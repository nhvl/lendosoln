namespace DataAccess
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Messaging;
    using System.Net;
    using System.Reflection;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Security.Cryptography;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.Schema;
    using CommonLib;
    using CommonProjectLib.Common;
    using ComplianceEase;
    using DataAccess.Core.Construction;
    using DataAccess.CounselingOrganization;
    using DataAccess.LoanValidation;
    using DataAccess.Utilities;
    using EDocs;
    using EDocs.Contents;
    using LendersOffice.Admin;
    using LendersOffice.Audit;
    using LendersOffice.BrokerAdmin;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.CreditReport;
    using LendersOffice.CreditReport.Mcl;
    using LendersOffice.Drivers.NetFramework;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.Events;
    using LendersOffice.HttpModule;
    using LendersOffice.Integration.Cenlar;
    using LendersOffice.Integration.TitleFramework;
    using LendersOffice.Integration.UcdDelivery;
    using LendersOffice.Integration.Underwriting;
    using LendersOffice.Integration.VOXFramework;
    using LendersOffice.Logging;
    using LendersOffice.Migration;
    using LendersOffice.ObjLib;
    using LendersOffice.ObjLib.AdjustmentsAndOtherCredits;
    using LendersOffice.ObjLib.CapitalMarkets;
    using LendersOffice.ObjLib.DatabaseMessageQueue;
    using LendersOffice.ObjLib.Hmda;
    using LendersOffice.ObjLib.Loan;
    using LendersOffice.ObjLib.LoanFileCache;
    using LendersOffice.ObjLib.MortgagePool;
    using LendersOffice.ObjLib.QuickPricer;
    using LendersOffice.ObjLib.Renovation;
    using LendersOffice.ObjLib.ServiceCredential;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.ObjLib.TPO;
    using LendersOffice.ObjLib.TRID2;
    using LendersOffice.ObjLib.UI.DataContainers;
    using LendersOffice.Reports;
    using LendersOffice.Roles;
    using LendersOffice.Security;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;
    using LqbGrammar.Drivers;
    using LqbGrammar.Drivers.ConversationLog;
    using LqbGrammar.Drivers.SecurityEventLogging;
    using LqbGrammar.Exceptions;
    using MeridianLink.CommonControls;
    using PdfRasterizerLib;
    using TotalScoreCard;

    public class YearMonthParser
    {

        private int m_years;
        private int m_months;

        public int NumberOfYears
        {
            get { return m_years; }
        }

        public int NumberOfMonths
        {
            get { return m_months; }
        }

        public bool ParseStr(string years, string months)
        {

            int y = 0;

            try { y = int.Parse(years); }
            catch { return false; }

            int m = 0;

            try { m = int.Parse(months); }
            catch { }

            Parse(y, m);
            return true;
        }

        public void Parse(int years, int months)
        {
            m_years = years;
            m_months = months;
        }

        public decimal YearsInDecimal
        {
            get
            {
                // 8/17/2004 dd - If months is greater than 12 months than convert to year.
                int years = m_years + m_months / 12;
                int months = m_months % 12;

                decimal[] fractions = { 0M, .08M, .17M, .25M, .33M, .42M, .5M, .58M, .67M, .75M, .83M, .92M };

                return (decimal)years + fractions[months];

            }
        }
        public void Parse(decimal dec)
        {
            Reset();

            m_years = (int)dec;
            m_months = (int)((dec - m_years) * 12 + 0.5M);
            if (m_months >= 12)
            {
                m_months = 0;
                ++m_years;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate">If not a date, "today" will be assumed</param>
        public void Parse(string startDate, string endDate)
        {
            Reset();
            DateTime startD, endD;
            try { startD = Convert.ToDateTime(startDate); }
            catch { return; }

            try { endD = Convert.ToDateTime(endDate); }
            catch { endD = DateTime.Now; }

            this.Parse(startD, endD);
        }

        public void Parse(DateTime startD, DateTime endD)
        {
            Reset();

            TimeSpan timespan = endD.Subtract(startD);

            int yrs = endD.Year - startD.Year;
            int mon = endD.Month - startD.Month;

            if (mon < 0)
            {
                --yrs;
                mon = 12 + mon;
            }

            if (yrs < 0)
            {
                return;
            }

            m_years = yrs;
            m_months = mon;
        }

        private void Reset()
        {
            m_years = 0;
            m_months = 0;
        }
        public YearMonthParser()
        {
            Reset();
        }

        public static YearMonthParser Create(string dec)
        {
            var parser = new YearMonthParser();
            parser.Parse(dec);
            return parser;
        }

        public static YearMonthParser operator +(YearMonthParser ym1, YearMonthParser ym2)
        {
            int years = ym1.NumberOfYears + ym2.NumberOfYears;
            int months = ym1.NumberOfMonths + ym2.NumberOfMonths;
            years += months / 12;
            months = months % 12;

            return new YearMonthParser
            {
                m_months = months,
                m_years = years
            };
        }

        public void Parse(string dec)
        {
            Reset();
            if (dec.Length == 0)
                return;
            try
            {
                Parse(decimal.Parse(dec));
            }
            catch
            {
                // FUTURE: Log error, but note that sometimes it's not really an error here.
                return;
            }
        }

    }


    public static class Tools
    {
        public static bool IsStringEmptyOrNegativeInt(string val)
        {
            int res;
            if (val == string.Empty || (int.TryParse(val, out res) && res <= 0))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Runs internal pricing validation on a loan.
        /// </summary>
        /// <param name="loan">
        /// The loan to run internal pricing.
        /// </param>
        /// <param name="principal">
        /// The user running internal pricing.
        /// </param>
        /// <returns>
        /// The validation result.
        /// </returns>
        public static LoanValidationResultCollection RunInternalPricingValidation(CPageData loan, AbstractUserPrincipal principal)
        {
            LoanValidationResultCollection resultList;

            if (loan.BrokerDB.IsNewPmlUIEnabled)
            {
                resultList = loan.ValidateForRunningInternalPricing(principal);
            }
            else
            {
                var employee = EmployeeDB.RetrieveById(loan.BrokerDB.BrokerID, principal.EmployeeId);
                resultList = employee.IsNewPmlUIEnabled ? loan.ValidateForRunningInternalPricing(principal) : loan.ValidateForRunningPricing(principal);
            }

            return resultList;
        }

        /// <summary>
        /// Retrieves a list of all custom reports for a lender.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the lender.
        /// </param>
        /// <returns>
        /// The list of all custom reports as Report Id - Report Name pairs.
        /// </returns>
        public static IEnumerable<KeyValuePair<Guid, string>> GetAllLenderCustomReports(Guid brokerId)
        {
            var reportList = new List<KeyValuePair<Guid, string>>();

            var procedureName = StoredProcedureName.Create("ListReportQueryDetails").ForceValue();
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId)
            };

            using (var connection = DbConnectionInfo.GetConnection(brokerId))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(connection, null, procedureName, parameters, TimeoutInSeconds.Thirty))
            {
                while (reader.Read())
                {
                    reportList.Add(new KeyValuePair<Guid, string>(
                        (Guid)reader["QueryId"],
                        (string)reader["QueryName"]));
                }
            }

            return reportList;
        }

        /// <summary>
        /// Escapes invalid filename characters with an underscore.
        /// </summary>
        /// <param name="filename">
        /// The input filename.
        /// </param>
        /// <returns>
        /// The scaped filename.
        /// </returns>
        public static string EscapeInvalidFilenameCharacters(string filename) => string.Join("_", filename.Split(Path.GetInvalidFileNameChars()));

        /// <summary>
        /// Retrieves the FileDB key - title pair for a custom form.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker for the form.
        /// </param>
        /// <param name="customFormId">
        /// The ID of the custom form.
        /// </param>
        /// <returns>
        /// The custom form info pair.
        /// </returns>
        public static KeyValuePair<Guid, string> GetCustomFormInfo(Guid brokerId, Guid customFormId)
        {
            var procedureName = StoredProcedureName.Create("RetrieveCustomForm").ForceValue();
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerID", brokerId),
                new SqlParameter("@CustomLetterID", customFormId)
            };

            using (var connection = DbConnectionInfo.GetConnection(brokerId))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(connection, null, procedureName, parameters, TimeoutInSeconds.Thirty))
            {
                if (reader.Read())
                {
                    return new KeyValuePair<Guid, string>(
                        (Guid)reader["FileDBKey"],
                        (string)reader["Title"]);
                }
                else
                {
                    throw new CBaseException(ErrorMessages.Generic, $"Unable to locate custom form with ID {customFormId} for broker {brokerId}");
                }
            }
        }

        public static void ValidateInternalUserPermission(string permissionStr, E_InternalUserPermissions permission)
        {
            InternalUserPermissions permissions = new InternalUserPermissions(permissionStr);
            if (!permissions.Can(permission))
            {
                throw new CBaseException("You do not have permission to do this action.", $"The user without {permission.ToString()} tried to perform a prohibited action.");
            }
        }

        /// <summary>
        /// Determines whether the given page is valid for CORS requests, as configured in ConstStage.CrossOriginUrls.
        /// </summary>
        /// <param name="pageUriString">The page uri to check.</param>
        /// <returns>Whether the page is enabled </returns>
        public static bool IsCorsPageEnabled(string pageUriString)
        {
            var allowedOriginUrls = ConstStage.CrossOriginUrls;
            Uri pageUri;
            if (Uri.TryCreate(pageUriString, UriKind.Absolute, out pageUri))
            {
                return pageUri.Scheme.Equals("https", StringComparison.OrdinalIgnoreCase)
                    && allowedOriginUrls.Any(allowedOrigin =>
                        Uri.Compare(pageUri, allowedOrigin, UriComponents.Scheme | UriComponents.HostAndPort, UriFormat.SafeUnescaped, StringComparison.OrdinalIgnoreCase) == 0);
            }

            return false;
        }

        /// <summary>
        /// Gets the string description for the specified <paramref name="party"/>.
        /// </summary>
        /// <param name="party">
        /// The party.
        /// </param>
        /// <returns>
        /// The string description.
        /// </returns>
        public static string GetPartyDescription(E_PartyT party)
        {
            switch (party)
            {
                case E_PartyT.Blank:
                    return string.Empty;
                case E_PartyT.Borrower:
                    return "Borrower";
                case E_PartyT.BorrowersEmployer:
                    return "Borrower's Employer";
                case E_PartyT.BorrowersFriend:
                    return "Borrower's Friend";
                case E_PartyT.BorrowersParent:
                    return "Borrower's Parent";
                case E_PartyT.BorrowersRelative:
                    return "Borrower's Relative";
                case E_PartyT.BuilderOrDeveloper:
                    return "Builder or Developer";
                case E_PartyT.FederalAgency:
                    return "Federal Agency";
                case E_PartyT.Lender:
                    return "Lender";
                case E_PartyT.Other:
                    return "Other";
                case E_PartyT.RealEstateAgent:
                    return "Real Estate Agent";
                case E_PartyT.Seller:
                    return "Seller";
                default:
                    throw new UnhandledEnumException(party);
            }
        }

        /// <summary>
        /// Will run the static constructors of our large static classes if they haven't been initialized already.
        /// </summary>
        public static void InitializeLargeStatics()
        {
            Stopwatch sw = Stopwatch.StartNew();

            var t = CFieldDbInfoList.TheOnlyInstance;
            var pmlPipeline = LoanReporting.DefaultSimplePipelineReportId;
            var nonPmlPipeline = LoanReporting.DefaultSimplePipelineNonPMLReportId;

            CFieldInfoTable.GetInstance();
            DbConnectionStringManager.GetConnectionString(DataSrc.LQBSecure);
            CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputNothing_NeedAllFields, inputFields: null);
            CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputNothing_NeedAllCachedFields, inputFields: null);
            ParameterReporting.RetrieveSystemParameters();
            LendersOfficeApp.los.RatePrice.CApplicantPrice.GetUserMsg(nameof(Tools));

            List<Type> statics = new List<Type>()
            {
                typeof(ConstStage),
                typeof(DbConnectionInfo),
                typeof(PageDataUtilities),
                typeof(CPageBase),
                typeof(CAppBase),
                typeof(WorkflowOperations),
                typeof(LendingQBExecutingEngine),
                typeof(LoanFileFieldValidator),
                typeof(BrokerUserPermissions),
                typeof(DependencyUtils),
                typeof(EnumUtilities),
                typeof(SerializationHelper),
            };

            foreach (var type in statics)
            {
                System.Runtime.CompilerServices.RuntimeHelpers.RunClassConstructor(type.TypeHandle);
            }

            sw.Stop();

            if (sw.ElapsedMilliseconds > 5)
            {
                Tools.LogInfo("InitializeLargeStatics executed in " + sw.ElapsedMilliseconds.ToString("#,#") + "ms.");
            }

            /* Below is what we want to do and it works. Too well.
             * The reflection code will pull every single class with a static constructor and call the constructor only if it hasn't been called before.
             * However, this also includes classes without a defined static constructor.
             * This is because as long as there is one static member in the class, a default static constructor is added.
             * This also does not find singleton classes, that we'd want to initialize on startup as well. 
             */

            /*
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            
            var typesWithStaticConstructors = assemblies.SelectMany(assembly => assembly.DefinedTypes.Where(type => type.DeclaredConstructors.Any(constructor => constructor.IsStatic)));

            foreach (var type in typesWithStaticConstructors)
            {
                System.Runtime.CompilerServices.RuntimeHelpers.RunClassConstructor(type.TypeHandle);
            }
            */
        }

        /// <summary>
        /// Constructs the id needed to pull the fields from the service item.
        /// </summary>
        /// <param name="prefix">The prefix to use.</param>
        /// <param name="id">The id of the field.</param>
        /// <returns>The key to use.</returns>
        public static string ConstructControlKey(string prefix, string id)
        {
            if (string.IsNullOrEmpty(prefix))
            {
                return id;
            }

            return prefix + id;
        }

        /// <summary>
        /// Generates a <see cref="DataPath"/> from the specified
        /// collection rule.
        /// </summary>
        /// <param name="rule">
        /// The collection rule.
        /// </param>
        /// <returns>
        /// The path for the rule, or null if the rule cannot be parsed.
        /// </returns>
        public static DataPath GenerateDataPathFromCollRule(string rule)
        {
            if (string.IsNullOrEmpty(rule))
            {
                return null;
            }

            var ruleParts = rule.Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
            if (ruleParts.Length != 3)
            {
                return null;
            }

            string collectionName;
            if (!PathCollectionMappings.TryGetValue(ruleParts[1], out collectionName))
            {
                // Workflow will save collection names for paths in lowercase.
                // Replicate that behavior here.
                collectionName = ruleParts[1].ToLower();
            }

            var path = $"{DataPath.PathHeader}{collectionName}[{DataPath.WholeCollectionIdentifier}].";

            var propertyName = Regex.Replace(ruleParts[2], "[_]rep$", string.Empty, RegexOptions.Singleline | RegexOptions.Compiled);
            if (propertyName.Contains("Payment-"))
            {
                path += $"Payments[{DataPath.WholeCollectionIdentifier}]." + propertyName.Replace("Payment-", string.Empty);
            }
            else if (string.Equals("PhoneOfCompanyminOccurs=0", propertyName, StringComparison.OrdinalIgnoreCase))
            {
                // Special case handling: there's a typo for the name of 
                // the agent company phone field. This cannot be fixed
                // due to existing workflow configurations having this
                // value in their XML, so adjust the property name here 
                // to ensure a valid path value.
                path += "PhoneOfCompany";
            }
            else
            {
                path += propertyName;
            }

            return DataPath.Create(path);
        }

        /// <summary>
        /// Maps between set names in coll rules and set path names.
        /// </summary>
        private static readonly IReadOnlyDictionary<string, string> PathCollectionMappings = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
        {
            // Rules not explicitly using the seller set will use the borrower set
            // in LoanFileFieldValidator.
            [nameof(CPageData.sClosingCostSet)] = "borrowerclosingcostset",
            [nameof(CPageData.sSellerResponsibleClosingCostSet)] = "sellerclosingcostset"
        };

        /// <summary>
        /// Create the list of broker DBs using the list of broker ids.
        /// </summary>
        /// <return>List of broker db objects.</return>
        public static List<BrokerDB> GetBrokerDBsFromBrokerIds(List<Guid> brokerIds)
        {
            List<BrokerDB> brokerDbs = new List<BrokerDB>();

            foreach (Guid brokerid in brokerIds)
            {
                brokerDbs.Add(BrokerDB.RetrieveById(brokerid));
            }

            return brokerDbs;
        }

        /// <summary>
        /// Function that gets all the feature ids from the Tracked_Feature table in the database.
        /// </summary>
        /// <returns>A hash set of enum values casted to integer.</returns>
        public static HashSet<int> GetFeatureIdsFromDB()
        {
            HashSet<int> featuresFromDB = new HashSet<int>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShare, "GetAllTrackedFeatureIds", null))
            {
                while (reader.Read())
                {
                    int featureId = (int)reader["FeatureId"];
                    featuresFromDB.Add(featureId);
                }
            }

            return featuresFromDB;
        }

        /// <summary>
        /// Return a enumeration of the guids of all the active brokers
        /// </summary>
        /// <param name="suiteType">
        /// The suite type of the active brokers, if specified.
        /// </param>
        /// <returns>All active brokers.</returns>
        public static IEnumerable<Guid> GetAllActiveBrokers(BrokerSuiteType? suiteType = null)
        {
            var brokerIdList = new List<Guid>();

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                SqlParameter[] parameters =
                {
                    new SqlParameter("@SuiteType", suiteType)
                };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "ListBrokerIds", parameters))
                {
                    while (reader.Read())
                    {
                        brokerIdList.Add((Guid)reader["BrokerId"]);
                    }
                }
            }

            return brokerIdList;
        }

        public static string RetrieveUserNameForAudit(AbstractUserPrincipal principal)
        {
            var userName = "System User";

            if (principal != null)
            {
                if (principal.InitialPrincipalType == InitialPrincipalTypeT.Internal)
                {
                    userName = $"LQB User (aliased as {principal.DisplayName})";
                }
                else
                {
                    userName = principal.DisplayName;
                }
            }

            return userName;
        }

        /// <summary>
        /// Determines whether explicit saving functionality has been enabled.
        /// </summary>
        /// <returns>True if the save button is enabled, false otherwise.</returns>
        public static bool IsSaveButtonEnabled(AbstractUserPrincipal principal)
        {
            if (principal == null)
            {
                return false;
            }

            TpoLoanEditorSaveButtonBrokerOptions startButtonOptions = principal.BrokerDB.TpoLoanEditorSaveButtonBrokerOptions;

            if (startButtonOptions.IsEnabledForAll)
            {
                return true;
            }

            bool isInEmployeeGroup = !string.IsNullOrWhiteSpace(startButtonOptions.EnabledEmployeeGroupName) && principal.IsInEmployeeGroup(startButtonOptions.EnabledEmployeeGroupName);
            bool isInOcGroup = !string.IsNullOrWhiteSpace(startButtonOptions.EnabledOcGroupName) && principal.IsInPmlBrokerGroup(startButtonOptions.EnabledOcGroupName);

            return isInEmployeeGroup || isInOcGroup;
        }

        /// <summary>
        /// Gets a value indicating whether the specified <paramref name="type"/>
        /// of TPO disclosure is enabled based on user and broker settings.
        /// </summary>
        /// <param name="broker">
        /// The broker for the user accessing the disclosures.
        /// </param>
        /// <param name="membershipGroups">
        /// The groups that the user's originating company or branch belongs to
        /// depending on the user's type.
        /// </param>
        /// <param name="type">
        /// The type of TPO disclosure.
        /// </param>
        /// <param name="alphaTestingGroupName">
        /// The name of the broker's alpha testing group. If not supplied,
        /// the name will be retrieved from the database.
        /// </param>
        /// <param name="betaTestingGroupName">
        /// The name of the broker's beta testing group. If not supplied,
        /// the name will be retrieved from the database.
        /// </param>
        /// <returns>
        /// True if the disclosure is enabled, false otherwise.
        /// </returns>
        public static bool IsTpoDisclosureEnabled(
            BrokerDB broker, 
            IEnumerable<MembershipGroup> membershipGroups, 
            TpoDisclosureType type,
            string alphaTestingGroupName = null,
            string betaTestingGroupName = null)
        {            
            var redisclosureAllowedGroup = broker.TpoRedisclosureAllowedGroupType;
            var initialClosingDisclosureAllowedGroup = broker.TpoInitialClosingDisclosureAllowedGroupType;

            if (type == TpoDisclosureType.InitialDisclosure ||
                type == TpoDisclosureType.Redisclosure && redisclosureAllowedGroup == NewTpoFeatureOcGroupAccess.All ||
                type == TpoDisclosureType.InitialClosingDisclosure && initialClosingDisclosureAllowedGroup == NewTpoFeatureOcGroupAccess.All)
            {
                return true;
            }

            if (type == TpoDisclosureType.Redisclosure && redisclosureAllowedGroup == NewTpoFeatureOcGroupAccess.None ||
                type == TpoDisclosureType.InitialClosingDisclosure && initialClosingDisclosureAllowedGroup == NewTpoFeatureOcGroupAccess.None)
            {
                return false;
            }

            if (!broker.TpoNewFeatureAlphaTestingGroup.HasValue &&
                !broker.TpoNewFeatureBetaTestingGroup.HasValue)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            alphaTestingGroupName = alphaTestingGroupName ?? GroupDB.GetGroupNameById(broker.BrokerID, broker.TpoNewFeatureAlphaTestingGroup.Value);
            betaTestingGroupName = betaTestingGroupName ?? GroupDB.GetGroupNameById(broker.BrokerID, broker.TpoNewFeatureBetaTestingGroup.Value);

            switch (type)
            {
                case TpoDisclosureType.Redisclosure:
                    var alphaRedisclosureEnabled = redisclosureAllowedGroup == NewTpoFeatureOcGroupAccess.Alpha;
                    var alphaAndBetaRedisclosureEnabled = redisclosureAllowedGroup == NewTpoFeatureOcGroupAccess.AlphaAndBeta;

                    foreach (var group in membershipGroups.Where(group => group.IsInGroup == 1))
                    {
                        var isInAlphaGroup = string.Equals(alphaTestingGroupName, group.GroupName, StringComparison.Ordinal);
                        var isInBetaGroup = string.Equals(betaTestingGroupName, group.GroupName, StringComparison.Ordinal);

                        var redisclosureEnabled = (isInAlphaGroup && alphaRedisclosureEnabled) ||
                            ((isInAlphaGroup || isInBetaGroup) && alphaAndBetaRedisclosureEnabled);

                        if (redisclosureEnabled)
                        {
                            return true;
                        }
                    }

                    return false;

                case TpoDisclosureType.InitialClosingDisclosure:
                    var alphaInitialClosingDisclosureEnabled = initialClosingDisclosureAllowedGroup == NewTpoFeatureOcGroupAccess.Alpha;
                    var alphaAndBetaInitialClosingDisclosureEnabled = initialClosingDisclosureAllowedGroup == NewTpoFeatureOcGroupAccess.AlphaAndBeta;

                    foreach (var group in membershipGroups.Where(group => group.IsInGroup == 1))
                    {
                        var isInAlphaGroup = string.Equals(alphaTestingGroupName, group.GroupName, StringComparison.Ordinal);
                        var isInBetaGroup = string.Equals(betaTestingGroupName, group.GroupName, StringComparison.Ordinal);

                        var initialClosingDisclosureEnabled = (isInAlphaGroup && alphaInitialClosingDisclosureEnabled) ||
                            ((isInAlphaGroup || isInBetaGroup) && alphaAndBetaInitialClosingDisclosureEnabled);

                        if (initialClosingDisclosureEnabled)
                        {
                            return true;
                        }
                    }

                    return false;
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        /// <summary>
        /// Determines whether the specified <paramref name="amount"/>
        /// is a whole-dollar amount.
        /// </summary>
        /// <param name="amount">
        /// The amount to check.
        /// </param>
        /// <returns>
        /// True if the amount is a whole dollar amount, false otherwise.
        /// </returns>
        /// <remarks>
        /// Adapted from https://stackoverflow.com/a/2751597 and
        /// https://stackoverflow.com/q/11781899. This solution
        /// prevents common floating point errors from altering 
        /// the results of the check.
        /// </remarks>
        public static bool IsWholeDollarAmount(decimal amount)
        {
            return Math.Abs(amount % 1) <= (ConstApp.DecimalEpsilon * 100);
        }

        /// <summary>
        /// A timezone representing Pacific Standard Time that doesn't change with daylight savings time.
        /// </summary>
        private static readonly TimeZoneInfo PacificStandardFixedOffset = TimeZoneInfo.CreateCustomTimeZone(
               id: "P Standard Time",
               baseUtcOffset: TimeSpan.FromHours(-8),
               displayName: "PST",
               standardDisplayName: "P Standard Time");

        /// <summary>
        /// A timezone representing Pacific Daylight Time that doesn't change with daylight savings time.
        /// </summary>
        private static readonly TimeZoneInfo PacificDaylightFixedOffset = TimeZoneInfo.CreateCustomTimeZone(
                id: "P Daylight Time",
                baseUtcOffset: TimeSpan.FromHours(-7),
                displayName: "PDT",
                standardDisplayName: "P Daylight Time");

        /// <summary>
        /// A timezone representing Pacific Standard *or* Daylight Time depending on the time.  This is what we're used to using. <para/>
        /// For a list of available timezones, see regedit as stated here: http://stackoverflow.com/a/1402640/420667.
        /// </summary>
        public static readonly TimeZoneInfo PacificStandardVariantOffset = TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time");

        /// <summary>
        /// A timezone representing Eastern Standard *or* Daylight Time depending on the time.  This is what we're used to using for Eastern times. <para/>
        /// For a list of available timezones, see regedit as stated here: http://stackoverflow.com/a/1402640/420667.
        /// </summary>
        private static readonly TimeZoneInfo EasternStandardVariantOffset = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");

        /// <summary>
        /// Checks if a workflow operation can be performed.
        /// </summary>
        /// <param name="principal">The principal.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="operation">The operation to check.</param>
        /// <returns>Tuple. Item1 is whether the operation can be performed. Item2 is the reason if it can't be performed.</returns>
        public static Tuple<bool, string> IsWorkflowOperationAuthorized(AbstractUserPrincipal principal, Guid loanId, WorkflowOperation operation)
        {
            return AreWorkflowOperationsAuthorized(principal, loanId, new WorkflowOperation[] { operation }).Values.First();
        }

        /// <summary>
        /// Checks if workflow permits the operations.
        /// </summary>
        /// <param name="principal">The principal of the user.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="operations">The operations to check.</param>
        /// <returns>Dictionary mapping the operation to the a tuple. Tuple-Item1 contains whether the operation can be performed. Item2 is the reason if it cannot be performed.</returns>
        public static Dictionary<WorkflowOperation, Tuple<bool, string>> AreWorkflowOperationsAuthorized(AbstractUserPrincipal principal, Guid loanId, IEnumerable<WorkflowOperation> operations)
        {
            LoanValueEvaluator valueEvaluator = new LoanValueEvaluator(principal.ConnectionInfo, principal.BrokerId, loanId, operations.ToArray());
            valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(principal));

            var operationResults = new Dictionary<WorkflowOperation, Tuple<bool, string>>();
            foreach (WorkflowOperation operation in operations)
            {
                var canPerform = LendingQBExecutingEngine.CanPerform(operation, valueEvaluator);
                string reasonCantPerform = string.Empty;
                if (!canPerform)
                {
                    reasonCantPerform = LendingQBExecutingEngine.GetUserFriendlyMessage(operation, valueEvaluator);
                }

                operationResults.Add(operation, new Tuple<bool, string>(canPerform, reasonCantPerform));
            }

            return operationResults;
        }

        /// <summary>
        /// Converts the E_sRAdjFloorBaseT enum to RateAdjFloorCalcT enum.
        /// </summary>
        /// <param name="baseType">The base type to convert.</param>
        /// <returns>The equivalent RateAdjFloorCalcT if applicable.</returns>
        public static RateAdjFloorCalcT? GetRateAdjFloorCalcFromRateAdjFloorBase(E_sRAdjFloorBaseT baseType)
        {
            if (baseType == E_sRAdjFloorBaseT.Margin)
            {
                return RateAdjFloorCalcT.MarginFixedPercent;
            }
            else if (baseType == E_sRAdjFloorBaseT.StartRate)
            {
                return RateAdjFloorCalcT.StartRateFixedPercent;
            }
            else if (baseType == E_sRAdjFloorBaseT.ZeroPercent)
            {
                return RateAdjFloorCalcT.ZeroFixedPercent;
            }

            return null;
        }

        public static bool IsLogLargeRequest(int length)
        {
            if (length <= ConstApp.LargeLogInBytesThreshold)
            {
                return true; // Length is less than threshold. We log.
            }

            return ConstSite.EnableLogFor3rdParty;
        }

        /// <summary>
        /// Merge two agent data sets together. It prevents duplication if agents have the same record ID.
        /// </summary>
        /// <param name="srcSet">The set from the source loan.</param>
        /// <param name="destSet">The set from the destination loan. This will be modified.</param>
        public static void MergeAgentDataSets(DataSet srcSet, DataSet destSet)
        {
            // Dodge the expensive operation if possible.
            if (destSet.Tables[CAgentFields.TableName].Rows.Count == 0)
            {
                destSet = srcSet;
                return;
            }

            var table = destSet.Tables[CAgentFields.TableName];
            table.PrimaryKey = new System.Data.DataColumn[] { table.Columns[CAgentFields.RecordIdColumnName] };
            destSet.Merge(srcSet, false, System.Data.MissingSchemaAction.Error);
        }

        /// <summary>
        /// Pass in Uri.UriSchemeXXX as params.
        /// Will return false if non accepted.
        /// </summary>
        /// <param name="acceptedSchemes"></param>
        /// <returns></returns>
        public static bool IsValidUrl(string urlString, params string[] acceptedSchemes)
        {
            Uri uri;
            return Uri.TryCreate(urlString, UriKind.Absolute, out uri) &&
                acceptedSchemes.Contains(uri.Scheme);
        }

        /// <summary>
        /// Should clear and attach the appropriate headers prior to calling this.  Shouldn't add anything to response after this.
        /// May wish to call Response.SuppressContent = true and HttpContext.Current.ApplicationInstance.CompleteRequest() after this.
        /// File at fullPath should exist.
        /// </summary>
        public static void WriteFileToResponseWithBuffer(HttpResponse response, string fullPath)
        {
            var buffer = new byte[ConstApp.SendingBufferLength];
            var byteCount = 0;
            using (FileStream fs = File.Open(fullPath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                while ((byteCount = fs.Read(buffer, 0, ConstApp.SendingBufferLength)) == ConstApp.SendingBufferLength)
                {
                    response.BinaryWrite(buffer);
                    response.Flush();
                }
                if (byteCount != 0)
                {
                    byte[] lastBuffer = new byte[byteCount];
                    Array.Copy(buffer, lastBuffer, byteCount);
                    response.BinaryWrite(lastBuffer);
                }
            }
            response.Flush();
        }

        /// <summary>
        /// Obtains a dictionary mapping <see cref="TpoRequestForInitialDisclosureGenerationEventType"/> to
        /// a boolean indicating if that status can be transitioned to from the <paramref name="currentStatus"/>.
        /// </summary>
        /// <param name="currentStatus">
        /// The current status.
        /// </param>
        /// <returns>
        /// A dictionary with the mappings.
        /// </returns>
        public static Dictionary<TpoRequestForInitialDisclosureGenerationEventType, bool> GetAvailableTransitionsForTpoInitialDisclosureRequestGeneration(TpoRequestForInitialDisclosureGenerationEventType currentStatus)
        {
            var resultDictionary = Enum.GetValues(typeof(TpoRequestForInitialDisclosureGenerationEventType)).Cast<TpoRequestForInitialDisclosureGenerationEventType>().ToDictionary(
                key => key,
                value => false);

            switch (currentStatus)
            {
                case TpoRequestForInitialDisclosureGenerationEventType.NoRequest:
                    resultDictionary[TpoRequestForInitialDisclosureGenerationEventType.Requested] = true;
                    break;
                case TpoRequestForInitialDisclosureGenerationEventType.Requested:
                    resultDictionary[TpoRequestForInitialDisclosureGenerationEventType.Completed] = true;
                    resultDictionary[TpoRequestForInitialDisclosureGenerationEventType.Cancelled] = true;
                    break;
                case TpoRequestForInitialDisclosureGenerationEventType.Cancelled:
                    resultDictionary[TpoRequestForInitialDisclosureGenerationEventType.Requested] = true;
                    resultDictionary[TpoRequestForInitialDisclosureGenerationEventType.Completed] = true;
                    break;
                case TpoRequestForInitialDisclosureGenerationEventType.Completed:
                    resultDictionary[TpoRequestForInitialDisclosureGenerationEventType.Requested] = true;
                    resultDictionary[TpoRequestForInitialDisclosureGenerationEventType.Cancelled] = true;
                    break;
                default:
                    throw new UnhandledEnumException(currentStatus);
            }

            return resultDictionary;
        }

        /// <summary>
        /// Obtains a dictionary mapping <see cref="TpoRequestForRedisclosureGenerationEventType"/> to
        /// a boolean indicating if that status can be transitioned to from the <paramref name="currentStatus"/>.
        /// </summary>
        /// <param name="currentStatus">
        /// The current status.
        /// </param>
        /// <returns>
        /// A dictionary with the mappings.
        /// </returns>
        public static Dictionary<TpoRequestForRedisclosureGenerationEventType, bool> GetAvailableTransitionsForTpoRedisclosureRequestGeneration(TpoRequestForRedisclosureGenerationEventType currentStatus)
        {
            var resultDictionary = Enum.GetValues(typeof(TpoRequestForRedisclosureGenerationEventType))
                .Cast<TpoRequestForRedisclosureGenerationEventType>()
                .ToDictionary(key => key, value => false);

            switch (currentStatus)
            {
                case TpoRequestForRedisclosureGenerationEventType.NoRequest:
                    resultDictionary[TpoRequestForRedisclosureGenerationEventType.Active] = true;
                    break;
                case TpoRequestForRedisclosureGenerationEventType.Active:
                    resultDictionary[TpoRequestForRedisclosureGenerationEventType.Completed] = true;
                    resultDictionary[TpoRequestForRedisclosureGenerationEventType.Cancelled] = true;
                    break;
                case TpoRequestForRedisclosureGenerationEventType.Cancelled:
                    resultDictionary[TpoRequestForRedisclosureGenerationEventType.Active] = true;
                    resultDictionary[TpoRequestForRedisclosureGenerationEventType.Completed] = true;
                    break;
                case TpoRequestForRedisclosureGenerationEventType.Completed:
                    resultDictionary[TpoRequestForRedisclosureGenerationEventType.Active] = true;
                    resultDictionary[TpoRequestForRedisclosureGenerationEventType.Cancelled] = true;
                    break;
                default:
                    throw new UnhandledEnumException(currentStatus);
            }

            return resultDictionary;
        }

        /// <summary>
        /// Obtains a dictionary mapping <see cref="TpoRequestForInitialClosingDisclosureGenerationEventType"/> to
        /// a boolean indicating if that status can be transitioned to from the <paramref name="currentStatus"/>.
        /// </summary>
        /// <param name="currentStatus">
        /// The current status.
        /// </param>
        /// <returns>
        /// A dictionary with the mappings.
        /// </returns>
        public static Dictionary<TpoRequestForInitialClosingDisclosureGenerationEventType, bool> GetAvailableTransitionsForTpoInitialClosingDisclosureRequestGeneration(TpoRequestForInitialClosingDisclosureGenerationEventType currentStatus)
        {
            var resultDictionary = Enum.GetValues(typeof(TpoRequestForInitialClosingDisclosureGenerationEventType))
                .Cast<TpoRequestForInitialClosingDisclosureGenerationEventType>()
                .ToDictionary(key => key, value => false);

            switch (currentStatus)
            {
                case TpoRequestForInitialClosingDisclosureGenerationEventType.NoRequest:
                    resultDictionary[TpoRequestForInitialClosingDisclosureGenerationEventType.Active] = true;
                    break;
                case TpoRequestForInitialClosingDisclosureGenerationEventType.Active:
                    resultDictionary[TpoRequestForInitialClosingDisclosureGenerationEventType.Completed] = true;
                    resultDictionary[TpoRequestForInitialClosingDisclosureGenerationEventType.Cancelled] = true;
                    break;
                case TpoRequestForInitialClosingDisclosureGenerationEventType.Cancelled:
                    resultDictionary[TpoRequestForInitialClosingDisclosureGenerationEventType.Active] = true;
                    resultDictionary[TpoRequestForInitialClosingDisclosureGenerationEventType.Completed] = true;
                    break;
                case TpoRequestForInitialClosingDisclosureGenerationEventType.Completed:
                    resultDictionary[TpoRequestForInitialClosingDisclosureGenerationEventType.Active] = true;
                    resultDictionary[TpoRequestForInitialClosingDisclosureGenerationEventType.Cancelled] = true;
                    break;
                default:
                    throw new UnhandledEnumException(currentStatus);
            }

            return resultDictionary;
        }

        /// <summary>
        /// Determines whether the Homeowner Counseling Organization list received in
        /// the specified <paramref name="jsonResponseFromCfpb"/> is complete, i.e.
        /// containing ten organizations that are data complete.
        /// </summary>
        /// <param name="jsonResponseFromCfpb">
        /// The response from the CFPB API.
        /// </param>
        /// <returns>
        /// True if the list is complete, false otherwise.
        /// </returns>
        public static bool IsHomeownerCounselingOrganizationListComplete(string jsonResponseFromCfpb)
        {
            if (string.IsNullOrWhiteSpace(jsonResponseFromCfpb))
            {
                return false;
            }

            var cfpbAgencyList = CounselingOrganizationUtilities.CreateAgencyListFromJson(jsonResponseFromCfpb);
            var counselingList = cfpbAgencyList.CounselingAgencies.Select(agency => CounselingOrganizationUtilities.CreateHomeownerCounselingOrgFromCfpbData(agency));
            return IsHomeownerCounselingOrganizationListComplete(counselingList);
        }

        /// <summary>
        /// Determines whether the specified <paramref name="organizationCollection"/>
        /// is complete, i.e. containing ten organizations that are data complete.
        /// </summary>
        /// <param name="organizationCollection">
        /// The collection to check.
        /// </param>
        /// <returns>
        /// True if the collection is complete, false otherwise.
        /// </returns>
        public static bool IsHomeownerCounselingOrganizationListComplete(IEnumerable<HomeownerCounselingOrganizations.HomeownerCounselingOrganization> organizationCollection)
        {
            if (organizationCollection.Count() != 10)
            {
                return false;
            }

            return organizationCollection.All(organization => organization.IsDataComplete());
        }

        /// <summary>
        /// Determines whether the universal loan identifier for a loan is
        /// unique for the loan's broker.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker.
        /// </param>
        /// <param name="loanId">
        /// The ID of the loan.
        /// </param>
        /// <param name="legalEntityIdentifier">
        /// The legal entity identifier for the loan's broker.
        /// </param>
        /// <param name="loanName">
        /// The name of the loan.
        /// </param>
        public static void CheckForUniqueUniversalLoanIdentifier(Guid brokerId, Guid loanId, string legalEntityIdentifier, string loanName)
        {
            var universalLoanIdentifier = GenerateUniversalLoanIdentifier(legalEntityIdentifier, loanName);
            CheckForUniqueUniversalLoanIdentifier(brokerId, loanId, universalLoanIdentifier);
        }

        /// <summary>
        /// Determines whether the universal loan identifier for a loan is
        /// unique for the loan's broker.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker.
        /// </param>
        /// <param name="loanId">
        /// The ID of the loan.
        /// </param>
        /// <param name="universalLoanIdentifier">
        /// The universal loan identifier for the loan.
        /// </param>
        public static void CheckForUniqueUniversalLoanIdentifier(Guid brokerId, Guid loanId, string universalLoanIdentifier)
        {
            if (string.IsNullOrWhiteSpace(universalLoanIdentifier))
            {
                return;
            }

            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@ThisLoanId", loanId),
                new SqlParameter("@UniversalLoanIdentifier", universalLoanIdentifier)
            };

            var procedureName = StoredProcedureName.Create("CheckForUniqueUniversalLoanIdentifier");
            if (!procedureName.HasValue)
            {
                throw new LqbGrammar.Exceptions.DeveloperException(ErrorMessage.SystemError);
            }

            using (var connection = DbAccessUtils.GetConnection(brokerId))
            using (var reader = LendersOffice.Drivers.SqlServerDB.StoredProcedureDriverHelper.ExecuteReader(connection, null, procedureName.Value, parameters, TimeoutInSeconds.Thirty))
            {
                if (reader.Read())
                {
                    throw new UniversalLoanIdentifierDuplicateException();
                }
            }
        }

        /// <summary>
        /// Generates a universal loan identifier (ULI) using the given legal
        /// entity identifier and loan name.
        /// </summary>
        /// <param name="legalEntityIdentifier">
        /// The legal entity identifier of the loan's broker.
        /// </param>
        /// <param name="loanName">
        /// The name of the loan.
        /// </param>
        /// <returns>
        /// The ULI for the loan.
        /// </returns>
        public static string GenerateUniversalLoanIdentifier(string legalEntityIdentifier, string loanName)
        {
            if (string.IsNullOrWhiteSpace(legalEntityIdentifier))
            {
                return string.Empty;
            }

            var factory = GenericLocator<IRegularExpressionDriverFactory>.Factory;
            var regexDriver = factory.Create();
            var isMatch = regexDriver.IsMatch(RegularExpressionString.HmdaLegalIdentityIdentifierValidLoanName, loanName);

            if (!isMatch)
            {
                return string.Empty;
            }

            var identifierAndLoanName = legalEntityIdentifier + loanName;

            var generator = new Mod97Radix10ChecksumGenerator();
            var checkDigits = generator.ComputeCheckDigits(identifierAndLoanName);

            return identifierAndLoanName + checkDigits;
        }

        /// <summary>
        /// Obtains the initialization parameters for <see cref="CFieldInfoTable"/> 
        /// using reflection.
        /// </summary>
        /// <param name="usePredefinedObsoletePropertyOptimization">
        /// True to use a predefined list of obsolete field names, false otherwise.
        /// </param>
        /// <param name="logAction">
        /// Action used to log event timings.
        /// </param>
        /// <returns>
        /// A <see cref="FieldInfoTableInitializationParameters"/> instance with the parameters.
        /// </returns>
        public static FieldInfoTableInitializationParameters GetFieldInfoInitializationParametersFromTypeInfo(
            bool usePredefinedObsoletePropertyOptimization,
            Action<string> logAction = null)
        {
            var result = new FieldInfoTableInitializationParameters();

            if (usePredefinedObsoletePropertyOptimization)
            {
                result.ObsoletePropertyNameList.AddRange(new[]
                {
                    "sBrokLicNum",
                    "sBrokRep",
                    "sProdAmortArm",
                    "sProdAmortFixed",
                    "sProdAmortGpm",
                    "sProdAmortIOnly",
                    "sTilPrepByNm",
                    "sTransmProThisMPmt",
                    "sTransmProThisMPmt_rep",
                    "sUwerNm"
                });
            }

            // These properties are located at the base type level and have no dependencies.
            var rootFields = new string[]
            {
                "sEncryptionKey",
                "sEncryptionMigrationVersion"
            };

            foreach(string field in rootFields)
            {
                result.DependsOnAttributes.Add(new KeyValuePair<string, DependsOnAttribute>(field, new DependsOnAttribute()));
            }

            foreach (var reflectedType in DependencyUtils.DependencyClasses.Select(type => new ReflectedType(type)))
            {
                foreach (PropertyInfo prop in reflectedType.GetProperties())
                {
                    if (usePredefinedObsoletePropertyOptimization)
                    {
                        var dependsOnAttribute = prop.GetCustomAttribute<DependsOnAttribute>(inherit: false);
                        if (dependsOnAttribute != null)
                        {
                            result.DependsOnAttributes.Add(new KeyValuePair<string, DependsOnAttribute>(prop.Name, dependsOnAttribute));
                        }
                    }
                    else
                    {
                        foreach (Attribute attr in prop.GetCustomAttributes(false))
                        {
                            DependsOnAttribute dependson = attr as DependsOnAttribute;

                            if (dependson != null)
                            {
                                result.DependsOnAttributes.Add(new KeyValuePair<string, DependsOnAttribute>(prop.Name, dependson));
                                continue;
                            }

                            if (attr is ObsoleteAttribute == true)
                            {
                                result.ObsoletePropertyNameList.Add(prop.Name);
                            }
                        }
                    }
                }

                logAction?.Invoke($"Property gathering for {reflectedType.ContainedType.Name} with optimization {usePredefinedObsoletePropertyOptimization}");

                foreach (MethodInfo method in reflectedType.GetMethods().Where(method => !reflectedType.IsPropertyGetOrSetMethod(method)))
                {
                    var dependsOn = method.GetCustomAttribute<DependsOnAttribute>();
                    if (dependsOn != null)
                    {
                        result.DependsOnAttributes.Add(new KeyValuePair<string, DependsOnAttribute>(method.Name, dependsOn));
                    }
                }

                logAction?.Invoke($"Method gathering for {reflectedType.ContainedType.Name}");
            }

            return result;
        }

        /// <summary>
        /// Gets the list of field names with <see cref="GfeAttribute"/> applied.
        /// </summary>
        /// <returns>
        /// The list of field names.
        /// </returns>
        public static StringList GetFieldNamesWithGfeAttribute()
        {
            var list = new StringList();

            list.AddRange(new[]
            {
                "s800U1F",
                "s800U2F",
                "s800U3F",
                "s800U4F",
                "s800U5F",
                "s900U1Pia",
                "s904Pia",
                "s1006Rsrv",
                "s1007Rsrv",
                "sAggregateAdjRsrv",
                "sApprF",
                "sAttorneyF",
                "sCountyRtc",
                "sCrF",
                "sDocPrepF",
                "sEscrowF",
                "sFloodCertificationF",
                "sFloodInsRsrv",
                "sGfeBrokerComp",
                "sGfeLOrigBrokerCreditF",
                "sGfeOriginatorCompF",
                "sHazInsPia",
                "sHazInsRsrv",
                "sInspectF",
                "sIPia",
                "sLDiscnt",
                "sLOrigF",
                "sMBrokF",
                "sMInsRsrv",
                "sMipPia",
                "sNotaryF",
                "sOwnerTitleInsF",
                "sPestInspectF",
                "sProcF",
                "sRealETxRsrv",
                "sRecF",
                "sSchoolTxRsrv",
                "sStateRtc",
                "sTitleInsF",
                "sTxServF",
                "sU1GovRtc",
                "sU1Sc",
                "sU1Tc",
                "sU2GovRtc",
                "sU2Sc",
                "sU2Tc",
                "sU3GovRtc",
                "sU3Rsrv",
                "sU3Sc",
                "sU3Tc",
                "sU4Rsrv",
                "sU4Sc",
                "sU4Tc",
                "sU5Sc",
                "sUwF",
                "sVaFf",
                "sVaFfProps",
                "sWireF"
            });

            return list;
        }

        /// <summary>
        /// Determines whether the specified <paramref name="request"/>
        /// is to a local developer server.
        /// </summary>
        /// <param name="request">
        /// An optional parameter specifying the current request. If not specified,
        /// the request from <see cref="HttpContext.Current"/> is used.
        /// </param>
        /// <returns>
        /// True if the request is to a local developer server, false otherwise.
        /// </returns>
        public static bool IsLocalServer(HttpRequest request = null)
        {
            request = request ?? HttpContext.Current?.Request;
            if (request == null)
            {
                return false;
            }

            return ConstStage.LocalServerNames.Contains(request.Url.Host);
        }

        public static string GetIntegratedDisclosureFriendlyName(E_IntegratedDisclosureSectionT sectionT)
        {
            switch(sectionT)
            {
                case E_IntegratedDisclosureSectionT.LeaveBlank:
                    return "None";
                case E_IntegratedDisclosureSectionT.SectionA:
                    return "A";
                case E_IntegratedDisclosureSectionT.SectionB:
                    return "B";
                case E_IntegratedDisclosureSectionT.SectionBorC:
                    return "B or C";
                case E_IntegratedDisclosureSectionT.SectionC:
                    return "C";
                case E_IntegratedDisclosureSectionT.SectionD:
                    return "D";
                case E_IntegratedDisclosureSectionT.SectionE:
                    return "E";
                case E_IntegratedDisclosureSectionT.SectionF:
                    return "F";
                case E_IntegratedDisclosureSectionT.SectionG:
                    return "G";
                case E_IntegratedDisclosureSectionT.SectionH:
                    return "H";
                default:
                    throw new UnhandledEnumException(sectionT);
            }
        }

        public static string UrlOptionToUrl(E_UrlOption option)
        {
            switch (option)
            {
                case E_UrlOption.Page_2010GFE:
                    return Tools.VRoot + "/newlos/Forms/GoodFaithEstimate2010.aspx";
                case E_UrlOption.Page_Certificate:
                    return Tools.VRoot + "/newlos/Underwriting/SavedCertificate.aspx";
                case E_UrlOption.BrokerToUnderwritingNotes:
                    return Tools.VRoot + "/newlos/underwriting/BrokerToUnderwritingNotes.aspx";
                case E_UrlOption.LoanProgramView:
                    return Tools.VRoot + "/los/Template/LoanProgramView.aspx";
                case E_UrlOption.Page_GSEDelivery:
                    return Tools.VRoot + "/newlos/Finance/GSEDelivery.aspx";

                case E_UrlOption.Page_StatusGeneral:
                    return Tools.VRoot + "/newlos/Status/General.aspx";

                case E_UrlOption.Page_SettlementCharges:
                    return Tools.VRoot + "/newlos/Closer/SettlementCharges.aspx";

                case E_UrlOption.Page_LoanInformation:
                    return Tools.VRoot + "/newlos/LoanInfo.aspx?pg=0";

                case E_UrlOption.Page_CreditScores:
                    return Tools.VRoot + "/newlos/Worksheets/CreditScores.aspx";

                case E_UrlOption.Page_1003_1:
                    return Tools.VRoot + "/newlos/Forms/Loan1003.aspx?pg=0";

                case E_UrlOption.Page_1003_3:
                    return Tools.VRoot + "/newlos/Forms/Loan1003.aspx?pg=3";

                case E_UrlOption.Page_1008_04:
                    return Tools.VRoot + "/newlos/Forms/Transmittal_04.aspx";

                case E_UrlOption.Page_1008_04Combined:
                    return Tools.VRoot + "/newlos/Forms/Transmittal_04_Combined.aspx";

                case E_UrlOption.Page_PropertyDetails:
                    return Tools.VRoot + "/newlos/PropertyInfo.aspx?pg=1";

                case E_UrlOption.Page_StatusHMDA:
                    return Tools.VRoot + "/newlos/Status/HMDA.aspx";

                case E_UrlOption.Page_StatusProcessing:
                    return Tools.VRoot + "/newlos/Status/Processing.aspx";

                case E_UrlOption.Page_Servicing:
                    return Tools.VRoot + "/newlos/Finance/Servicing.aspx";

                case E_UrlOption.Page_ARMProgramDisclosure:
                    return Tools.VRoot + "/newlos/Disclosure/ARMProgramDisclosure.aspx";

                case E_UrlOption.Page_TIL:
                    return Tools.VRoot + "/newlos/Forms/TruthInLending.aspx";

                case E_UrlOption.Page_FHAAddendum:
                    return Tools.VRoot + "/newlos/FHA/FHAAddendum.aspx";

                case E_UrlOption.Page_MortgageInsurancePolicy:
                    return Tools.VRoot + "/newlos/Finance/MortgageInsurancePolicy.aspx";

                case E_UrlOption.Page_StatusAgents:
                    return Tools.VRoot + "/newlos/Status/Agents.aspx";
                case E_UrlOption.Page_CheckEligibility:
                    return Tools.VRoot + "/newlos/Underwriting/CheckEligibility.aspx";
                case E_UrlOption.Page_PmlReviewInfo:
                    return Tools.VRoot + "/newlos/Underwriting/PmlReviewInfo.aspx";
                case E_UrlOption.Page_BorrowerMonthlyIncome:
                    return Tools.VRoot + "/newlos/BorrowerInfo.aspx?pg=3";
                case E_UrlOption.Page_PropertyInformation:
                    return Tools.VRoot + "/newlos/PropertyInfo.aspx?pg=0";
                case E_UrlOption.Page_PMLDefaultValues:
                    return Tools.VRoot + "/newlos/PmlDefaultValues.aspx";
                case E_UrlOption.Page_BorrowerInformation:
                    return Tools.VRoot + "/newlos/BorrowerInfo.aspx?pg=0";
                case E_UrlOption.Page_2015GFE:
                    return Tools.VRoot + "/newlos/Test/RActiveGFE.aspx";
                case E_UrlOption.Page_LoanEstimate:
                    return Tools.VRoot + "/newlos/Disclosure/LoanEstimate.aspx";
                case E_UrlOption.Page_InternalPricingValidationError:
                    return Tools.VRoot + "/newlos/InternalPricingValidationErrorPage.aspx";
                case E_UrlOption.Page_BrokerRateLock:
                    return Tools.VRoot + "/newlos/LockDesk/BrokerRateLock.aspx";
                case E_UrlOption.Page_InvestorRateLock:
                    return Tools.VRoot + "/newlos/LockDesk/InvestorRateLock.aspx";
                case E_UrlOption.Page_1003_2:
                    return Tools.VRoot + "/newlos/Forms/Loan1003.aspx?pg=1";
                case E_UrlOption.Page_FannieAddendum:
                    return Tools.VRoot + "/newlos/FannieAddendum/FannieAddendum.aspx";
                case E_UrlOption.Page_LoanTerms:
                    return Tools.VRoot + "/newlos/Forms/LoanTerms.aspx?pg=0";
                case E_UrlOption.Page_BorrowerInfoNoTab:
                    return Tools.VRoot + "/newlos/BorrowerInfo.aspx";
                case E_UrlOption.Page_SubmitToLPA:
                    return VRoot + "/newlos/FreddieExport.aspx";
                case E_UrlOption.Page_Liabilities:
                    return VRoot + "/newlos/BorrowerInfo.aspx?pg=4";
                case E_UrlOption.Page_Assets:
                    return VRoot + "/newlos/BorrowerInfo.aspx?pg=5";
            }

            return "";
        }

        /// <summary>
        /// Get the GetComplianceEaseStatusT enum from the string description.
        /// </summary>
        /// <param name="sComplianceEaseStatus">The string description of sComplianceEaseStatus.</param>
        /// <returns>The corresponding enum.</returns>
        public static E_sComplianceEaseStatusT GetComplianceEaseStatusT(string sComplianceEaseStatus)
        {
            if (string.IsNullOrEmpty(sComplianceEaseStatus))
            {
                return E_sComplianceEaseStatusT.Undefined;
            }
            switch (sComplianceEaseStatus)
            {
                case "Critical":
                    return E_sComplianceEaseStatusT.Critical;
                case "Elevated":
                    return E_sComplianceEaseStatusT.Elevated;
                case "ERRORS":
                    return E_sComplianceEaseStatusT.Errors;
                case "Errors":
                    Tools.LogInfo("[ComplianceEagleDebug] Loan still has sComplianceEaseStatus set to 'Errors'.");
                    return E_sComplianceEaseStatusT.Errors;
                case "Minimal":
                    return E_sComplianceEaseStatusT.Minimal;
                case "Moderate":
                    return E_sComplianceEaseStatusT.Moderate;
                case "Significant":
                    return E_sComplianceEaseStatusT.Significant;
                case "Updating":
                    return E_sComplianceEaseStatusT.Updating;
                case "Pass":
                    return E_sComplianceEaseStatusT.Pass;
                case "Fail":
                    return E_sComplianceEaseStatusT.Fail;
                case "FailException":
                    return E_sComplianceEaseStatusT.FailException;
                case "Alert":
                    return E_sComplianceEaseStatusT.Alert;
                case "Warning":
                    return E_sComplianceEaseStatusT.Warning;
                case "Review":
                    return E_sComplianceEaseStatusT.Review;
                case "UserInput":
                    return E_sComplianceEaseStatusT.UserInput;
                case "Delayed":
                    return E_sComplianceEaseStatusT.Delayed;
                case "Undefined":
                    return E_sComplianceEaseStatusT.Undefined;
                default:
                    Tools.LogErrorWithCriticalTracking("Did not recognize '" + sComplianceEaseStatus + "' as a compliance ease status.");
                    return E_sComplianceEaseStatusT.Undefined;
            }

        }

        /// <summary>
        /// Get the value of sComplianceEaseStatus for the chosen loan file.
        /// </summary>
        /// <param name="loanId">Loan identifier of the loan.</param>
        /// <returns>The value of sComplianceEaseStatus.</returns>
        public static string GetLoanComplianceEaseStatus(Guid loanId, Guid brokerId)
        {
            // For OPM 344250, this is a bypass of InitLoad() for performance of the
            // polling pages.  Be wary of following this model elsewhere.  We really 
            // do want to use the datalayer access objects instead in nearly all cases.
            SqlParameter[] parameters = 
            {
                new SqlParameter("@LoanId", loanId)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetComplianceEaseDataByLoanId", parameters))
            {
                if (reader.Read())
                {
                    return reader["sComplianceEaseStatus"].ToString();
                }
            }

            throw new GenericUserErrorMessageException("Could not load sComplianceEaseStatus for loanid:" + loanId);
        }

        /// <summary>
        /// Get the value of sFileVersion for the chosen loan file.
        /// </summary>
        /// <param name="loanId">Loan identifier of the loan.</param>
        /// <returns>The value of sFileVersion</returns>
        public static int GetLoanFileVersion(Guid loanId, Guid brokerid)
        {
            // For OPM 344250, this is a bypass of InitLoad() for performance of the
            // polling pages.  Be wary of following this model elsewhere.  We really 
            // do want to use the datalayer access objects instead in nearly all cases.
            SqlParameter[] parameters = 
            {
                new SqlParameter("@LoanId", loanId)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerid, "GetFileVersionByLoanId", parameters))
            {
                if (reader.Read())
                {
                    return (int) reader["sFileVersion"];
                }
            }

            throw new GenericUserErrorMessageException("Could not load sFileVersion for loanid:" + loanId);
        }

        public static PropertyInfo GetFieldProperty(string fieldId)
        {
            fieldId = DependencyUtils.NormalizeMemberName(fieldId?.Trim() ?? string.Empty);

            // OPM 466743 - Add support for the sStatusProgressT field, which does not actually exist,
            // but is used in loan reporting and workflow because it provides an ordered list of statuses.
            if (fieldId.Equals("sStatusProgressT", StringComparison.OrdinalIgnoreCase))
            {
                fieldId = "sStatusT";
            }

            PropertyInfo property = null;

            property = GetProperty(fieldId, typeof(CPageBase), typeof(CPageHelocBase));

            return property;
        }

        public static PropertyInfo GetProperty(string name, params Type[] typesToConsider)
        {
            foreach (var type in typesToConsider)
            {
                var propertyInfo = type.GetProperty(name, BindingFlags.IgnoreCase | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
                if (propertyInfo != null)
                {
                    return propertyInfo;
                }
            }

            return null;
        }

        /// <summary>
        /// Get the value of sIsAuditHistoryMigrated for the chosen loan file.
        /// </summary>
        /// <param name="loanId">Loan identifier of the loan.</param>
        /// <returns>The value of sIsAuditHistoryMigrated</returns>
        public static bool GetAuditHistoryMigrated(Guid loanId, Guid brokerid)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@LoanId", loanId)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerid, "GetAuditHistoryMigratedByLoanId", parameters))
            {
                if (reader.Read())
                {
                    return reader["sIsAuditHistoryMigrated"] is DBNull ? false : (bool)reader["sIsAuditHistoryMigrated"];
                }
            }

            throw new GenericUserErrorMessageException("Could not load sIsAuditHistoryMigrated for loanid:" + loanId);
        }

        /// <summary>
        /// Adds to queue which then adds to INTEGRATION_FILE_MODIFIED, but it won't do update that table for sandboxed or qp2 sandboxed loans.
        /// </summary>
        public static void TrackIntegrationModifiedFile(Guid sBrokerId, Guid sLId, string loanName)
        {
            if (sBrokerId == Guid.Empty)
            {
                Tools.LogWarning("Broker ID should not be Guid.Empty; unable to detect whether the lender has modified file tracking enabled. sLId=" + sLId);
                return;
            }

            BrokerDB brokerDB = BrokerDB.RetrieveById(sBrokerId);
            if (brokerDB.IsTrackModifiedFile)
            {
                if (string.IsNullOrEmpty(ConstStage.MSMQ_TrackIntegrationModifiedFile))
                {
                    Tools.LogWarning("MSMQ_TrackIntegrationModifiedFile is not defined, but the lender has IsTrackModifiedFile enabled.  BrokerId=" + sBrokerId + ", sLId=" + sLId);
                    return;
                }

                try
                {
                    XDocument xdoc = new XDocument();
                    XElement rootElement = new XElement("root");
                    rootElement.SetAttributeValue("sLId", sLId.ToString());
                    rootElement.SetAttributeValue("sBrokerId", sBrokerId.ToString());
                    rootElement.SetAttributeValue("loanName", loanName);

                    xdoc.Add(rootElement);

                    LendersOffice.Drivers.Gateways.MessageQueueHelper.SendXML(ConstStage.MSMQ_TrackIntegrationModifiedFile, null, xdoc);
                }
                catch (MessageQueueException exc)
                {
                    Tools.LogErrorWithCriticalTracking("Unable to send to MSMQ:" + ConstStage.MSMQ_TrackIntegrationModifiedFile, exc);
                }
            }
        }

        public static Dictionary<E_DocBarcodeFormatT, string> s_DocBarcodeFormatT_Map = new Dictionary<E_DocBarcodeFormatT, string>()
        {
            { E_DocBarcodeFormatT.DocMagic, "DocMagic"},
            { E_DocBarcodeFormatT.DocuTech, "DocuTech"},
            { E_DocBarcodeFormatT.IDS, "IDS"}
        };

        public static string GetFileDBKeyForPmlLogoWithLoanId(Guid pmlLenderSiteId, Guid sLId)
        {
            Guid sBranchId = Guid.Empty;
            if (pmlLenderSiteId == new Guid("6BD06D93-EF65-4CFE-AE5A-3FE28091E71A") /* REMN PML0200 */)
            {
                // 8/29/2012 dd - Logo are varies depends on Branch. Only work on REMN (PML0200)
                if (sLId != Guid.Empty)
                {
                    CPageData dataLoan = new CPageData(sLId, "PmlLogo", new string[] { "sBranchId" });
                    dataLoan.InitLoad();

                    sBranchId = dataLoan.sBranchId;
                }
            }
            return GetFileDBKeyForPmlLogo(pmlLenderSiteId, sBranchId);
        }
        public static string GetFileDBKeyForPmlLogo(Guid pmlLenderSiteId, Guid sBranchId)
        {
            string key = pmlLenderSiteId.ToString().ToLower();
            if (pmlLenderSiteId == new Guid("6BD06D93-EF65-4CFE-AE5A-3FE28091E71A") /*REMN PML0200 */)
            {
                if (sBranchId != Guid.Empty)
                {
                    key += "_" + sBranchId.ToString().ToLower();
                }
            }
            return key;
        }

        public static Address ParseSimpleAddress(string addressString)
        {
            var address = new Address();

            // Transform the address so that we can use whitespace as a seperator
            string transformedAddress = addressString.Replace(",", ", ")
                                                     .Replace("\n", "\n ");

            string[] whitespaceSplit = transformedAddress.Split(
                new char[] { ' ', '\t' },
                StringSplitOptions.RemoveEmptyEntries);

            if (whitespaceSplit.Length >= 4)
            {
                int cursor = whitespaceSplit.Length - 1;

                // Assume the last element is the zip code
                address.Zipcode = whitespaceSplit[cursor].TrimWhitespaceAndBOM();
                cursor--;

                // And the element before that would be the state
                address.State = whitespaceSplit[cursor].TrimWhitespaceAndBOM().Replace(",", "");
                cursor--;

                // For the city, we will have to find the comma or newline
                int lookbehind = cursor - 1;
                bool foundCity = false;
                while (false == foundCity && lookbehind >= 0)
                {
                    int seperatorLocation = whitespaceSplit[lookbehind].LastIndexOfAny(new char[] { ',', '\n' });
                    if (seperatorLocation >= 0)
                    {
                        foundCity = true;

                        string[] cityParts = new string[cursor - lookbehind];
                        Array.Copy(whitespaceSplit, lookbehind + 1, cityParts, 0, cursor - lookbehind);

                        // address.City = whitespaceSplit[lookbehind].Substring(commaLocation) + string.Join(" ", cityParts);
                        address.City = string.Join(" ", cityParts).Replace(",", "")
                                                                  .Replace("\n", "");
                    }
                    else
                    {
                        lookbehind--;
                    }
                }

                // Rest is street address
                if (true == foundCity)
                {
                    cursor = lookbehind;
                    string streetAddress = string.Join(" ", whitespaceSplit, 0, cursor + 1).TrimWhitespaceAndBOM();
                    if (streetAddress[streetAddress.Length - 1] == ',')
                    {
                        address.StreetAddress = streetAddress.Substring(0, streetAddress.Length - 1);
                    }
                    else
                    {
                        address.StreetAddress = streetAddress;
                    }
                }
                else
                {
                    // Fallback to using street address if we can't find the city
                    string streetAddress = string.Join(" ", whitespaceSplit, 0, cursor + 1).TrimWhitespaceAndBOM();
                    address.StreetAddress = streetAddress;
                }
            }
            else
            {
                // Fallback if it doesn't split nicely
                address.StreetAddress = addressString;
            }
            return address;
        }


        public static IEnumerable<KeyValuePair<Guid, string>> GetRolesExcluding(params string[] filters)
        {
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "GetAllRoles"))
            {
                while(reader.Read())
                {
                    if( filters.Contains( (string) reader["RoleDesc"] ) == false )
                    {
                        yield return new KeyValuePair<Guid, string>((Guid)reader["RoleId"], (string)reader["RoleModifiableDesc"]);
                    }
                }
            }
        }

        /// <summary>
        /// Gets a list of payments for renovation fees matching the specified <paramref name="mismoFeeType"/>
        /// that are paid at closing.
        /// </summary>
        /// <param name="feeSet">
        /// The closing cost fee set.
        /// </param>
        /// <param name="mismoFeeType">
        /// The MISMO type for the fee.
        /// </param>
        /// <returns>
        /// The list of payments.
        /// </returns>
        /// <remarks>
        /// A renovation fee payment that is marked as "Paid Outside of Closing" is considered to be
        /// zero for the purposes of renovation calculations.
        /// </remarks>
        public static IEnumerable<decimal> GetRenovationFeePaymentsMatchingMismoType(BaseClosingCostSet feeSet, E_ClosingCostFeeMismoFeeT mismoFeeType)
        {
            var feePayments = feeSet.GetFees(fee => fee.MismoFeeT == mismoFeeType).Cast<BorrowerClosingCostFee>().SelectMany(fee => fee.Payments);
            return feePayments.Select(payment => payment.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing ? 0M : payment.Amount);
        }

        /// <summary>
        /// SLOW. <para></para>
        /// Currently only works for archvies of E_ClosingCostArchiveType LoanEstimate and ClosingDisclosure. <para></para>
        /// It gets the fees that appear on either the closing disclosure page or the loan estimate page, depending on the archive type. <para></para>
        /// This is slow because it loads the loan and it's archives, then applies them.
        /// </summary>
        public static IEnumerable<ClosingCostFeeWithSection> GetFeeViewsForClosingCostArchive(Guid loanID, Guid archiveID)
        {
            var dataLoan = new CPageData(loanID, new string[] { "sClosingCostArchive", "sClosingCostSet", "sfApplyClosingCostArchive", "sSellerResponsibleClosingCostSet" });
            dataLoan.InitLoad();

            var archive = dataLoan.sClosingCostArchive.Where((a) => a.Id == archiveID).First();
            dataLoan.ApplyClosingCostArchive(archive);

            IEnumerable<BaseClosingCostFeeSection> feeSections;
            switch (archive.ClosingCostArchiveType)
            {
                case ClosingCostArchive.E_ClosingCostArchiveType.ClosingDisclosure:
                    feeSections = dataLoan.sClosingCostSet.GetViewBase(E_ClosingCostViewT.LoanClosingCost);
                    IEnumerable<ClosingCostFeeWithSection> borrowerFees = feeSections.SelectMany((closingCostFeeSection) => closingCostFeeSection.FilteredClosingCostFeeList.Select((fee) => new ClosingCostFeeWithSection() { Section = closingCostFeeSection.SectionType, Fee = (BorrowerClosingCostFee)fee.Clone() }));

                    IEnumerable<BaseClosingCostFeeSection> sellerSections = dataLoan.sSellerResponsibleClosingCostSet.GetViewBase(E_ClosingCostViewT.SellerResponsibleLenderTypeEstimate);
                    IEnumerable<ClosingCostFeeWithSection> sellerFees = sellerSections.SelectMany(closingCostFeeSection => closingCostFeeSection.FilteredClosingCostFeeList.Select(fee => new ClosingCostFeeWithSection() { Section = closingCostFeeSection.SectionType, Fee = (SellerClosingCostFee)fee.Clone() })).ToArray();

                    foreach(var sellerFee in sellerFees)
                    {
                        sellerFee.Fee.GfeResponsiblePartyT = E_GfeResponsiblePartyT.Seller;
                    }
                    return borrowerFees.Union(sellerFees).OrderBy((fee) => fee.Section);
                case ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate:
                    feeSections = dataLoan.sClosingCostSet.GetViewBase(E_ClosingCostViewT.LoanClosingCost);
                    break;
                default:
                    throw new UnhandledEnumException(archive.ClosingCostArchiveType);
            }

            return feeSections.SelectMany((closingCostFeeSection) => closingCostFeeSection.FilteredClosingCostFeeList.Select((fee) => new ClosingCostFeeWithSection() { Section = closingCostFeeSection.SectionType, Fee = (BorrowerClosingCostFee)fee.Clone() })).OrderBy((fee) => fee.Section);
        }

        /// <summary>
        /// Only goes up to days.  Update it if you need more.
        /// </summary>
        public static string TimeSpanAsString(TimeSpan ts)
        {
            return string.Format(
                "{0:00}d {1:00}h {2:00}m {3:00}.{4:000}s",
                ts.Days,
                ts.Hours,   // 1
                ts.Minutes, // 2
                ts.Seconds, // 3
                ts.Milliseconds  // 4
            );
        }

        public static long Time(Action action)
        {
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Reset();
            sw.Start();
            try
            {
                action();
            }
            finally
            {
                sw.Stop();
            }
            return sw.ElapsedMilliseconds;
        }

        public static void Time(string label, Action action)
        {
            long timeMs = Time(action);
            System.Diagnostics.Trace.WriteLine(label + " : " + timeMs);
            Tools.LogInfo(label + " : " + timeMs);
        }

        public static TimeSpan GetTimeSpan(Action action)
        {
            var stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();

            action();

            stopwatch.Stop();
            return stopwatch.Elapsed;
        }

        /// <summary>
        /// Determines whether two arrays are identical.
        /// </summary>
        /// <typeparam name="T">
        /// The type of items in the arrays.
        /// </typeparam>
        /// <param name="first">
        /// The first array.
        /// </param>
        /// <param name="second">
        /// The second array.
        /// </param>
        /// <returns>
        /// True if the arrays are identical, false otherwise.
        /// </returns>
        /// <remarks>
        /// Concept obtained from https://stackoverflow.com/a/14086048 .
        /// </remarks>
        public static bool AreArraysIdentical<T>(IStructuralEquatable first, IStructuralEquatable second)
        {
            return AreArraysIdentical(first, second, EqualityComparer<T>.Default);
        }

        /// <summary>
        /// Determines whether two arrays are identical.
        /// </summary>
        /// <typeparam name="T">
        /// The type of items in the arrays.
        /// </typeparam>
        /// <param name="first">
        /// The first array.
        /// </param>
        /// <param name="second">
        /// The second array.
        /// </param>
        /// <param name="comparer">
        /// The comparer for the items within the arrays.
        /// </param>
        /// <returns>
        /// True if the arrays are identical, false otherwise.
        /// </returns>
        /// <remarks>
        /// Concept obtained from https://stackoverflow.com/a/14086048 .
        /// </remarks>
        public static bool AreArraysIdentical<T>(IStructuralEquatable first, IStructuralEquatable second, EqualityComparer<T> comparer)
        {
            return first?.Equals(second, comparer) ?? second == null;
        }

        /// <summary>
        /// Do a binary comparison between 2 files
        /// </summary>
        /// <returns>True if the files are identical</returns>
        public static bool IsFileIdentical(string sFile1, string sFile2)
        {
            if (!File.Exists(sFile1) || !File.Exists(sFile2)) return false;

            const int BUFSIZE = 1024;
            byte[] buf1 = new byte[BUFSIZE];
            byte[] buf2 = new byte[BUFSIZE];

            using (FileStream fs1 = new FileStream(sFile1, FileMode.Open))
            {
                using (FileStream fs2 = new FileStream(sFile2, FileMode.Open))
                {
                    while (true)
                    {
                        int n1 = fs1.Read(buf1, 0, BUFSIZE);
                        int n2 = fs2.Read(buf2, 0, BUFSIZE);

                        if (n1 != n2) return false;
                        if (n1 == 0) return true;

                        for (int i = 0; i < n1; i++)
                            if (buf1[i] != buf2[i])
                                return false;
                    }
                }
            }

        }

        public static System.Web.UI.Control FindControlRecursively(System.Web.UI.Control rootControl, string controlID)
        {
            if (rootControl.ID == controlID) return rootControl;

            foreach (System.Web.UI.Control controlToSearch in rootControl.Controls)
            {
                System.Web.UI.Control controlToReturn =
                    FindControlRecursively(controlToSearch, controlID);
                if (controlToReturn != null) return controlToReturn;
            }
            return null;
        }

        /// <summary>
        /// Use this method instead of File.Copy so it will override the destFile even if it is read-only.
        /// </summary>
        /// <param name="srcFile"></param>
        /// <param name="destFile"></param>
        public static void FileCopy(string srcFile, string destFile)
        {
            TurnOffReadOnlyAttribute(destFile);
            File.Copy(srcFile, destFile, true);
        }

        public static void FileDelete(string sFileName)
        {
            TurnOffReadOnlyAttribute(sFileName);
            File.Delete(sFileName);
        }
        private static void TurnOffReadOnlyAttribute(string fileName)
        {
            if (File.Exists(fileName))
            {
                FileAttributes attributes = File.GetAttributes(fileName);
                if ((attributes & FileAttributes.ReadOnly) > 0)
                {
                    attributes ^= System.IO.FileAttributes.ReadOnly; // Turn off read-only bit
                    File.SetAttributes(fileName, attributes);
                }
            }
        }
        /// <summary>
        /// opm 72500.
        /// If it's valid at the loan level, use that.
        /// If not, if it's valid at the branch level, use that.
        /// If not, if it's valid at the broker level, use that.
        /// Otherwise, use blank.
        /// </summary>
        public static string GetFhaLenderIDFromLoanBranchOrBroker(Guid brokerId, Guid loanID, Guid branchID)
        {
            SqlParameter fhaLenderIDParam = new SqlParameter("@fhalenderidcode", System.Data.SqlDbType.VarChar, 10);
            fhaLenderIDParam.Direction = System.Data.ParameterDirection.Output;

            var parameters = new List<SqlParameter>()
            {
                 new SqlParameter("@LoanID", loanID)
                ,new SqlParameter("@BranchID", branchID)
                ,fhaLenderIDParam
            };
            var result = StoredProcedureHelper.ExecuteNonQuery(brokerId, "FhaLenderIDFromLoanOrBranchOrBroker", 0, parameters);

            return (string)fhaLenderIDParam.Value;
        }

        public static bool IsValidAndNonBlankFhaLenderID(string fhaLenderID, bool UseFHATOTALProductionAccount)
        {
            return Regex.IsMatch(fhaLenderID, @"\d{10}") && false == (UseFHATOTALProductionAccount && fhaLenderID.StartsWith("9"));
        }

        public static void SetupServicePointManager()
        {
            // 2016-11-11 - dd - Allow web request to initiate tls v 1.1 and v1.2.
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
        }
        /// <summary>
        /// Setups the certificate check callback.
        /// </summary>
        public static void SetupServicePointCallback()
        {
            ServicePointManager.DefaultConnectionLimit = 10;
            ServicePointManager.ServerCertificateValidationCallback = delegate(object sender, X509Certificate certificate, X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
            {

                if (sslPolicyErrors == System.Net.Security.SslPolicyErrors.None)    //If there are no errors then the certificate is valid
                {
                    return true;  //so approve
                }
                else if (sender is HttpWebRequest) //check to see if it was a web request
                {
                    var request = (HttpWebRequest)sender;

                    if (ContainsURL(request.RequestUri.AbsoluteUri))
                    {
                        return true;
                    }
                    Tools.LogError("CERTIFICATE_ERROR : " + request.RequestUri.AbsoluteUri + " failed Server certificate validation with " + sslPolicyErrors);
                }
                else if (sender is WebClient)
                {
                    var request = (WebClient)sender;
                    if (ContainsURL(request.BaseAddress))
                    {
                        return true;
                    }
                    Tools.LogError("CERTIFICATE_ERROR : " + request.BaseAddress + " failed Server certificate validation with " + sslPolicyErrors);
                }
                else
                {
                    Tools.LogError("CERTIFICATE_ERROR : " + sender.ToString() + " failed server certificate validation with " + sslPolicyErrors);
                }
                return false; //dont approve
            };

        }

        private static bool ContainsURL(string urlToLookFor)
        {
            string[] urls = ConstStage.CertificateCheckBypassUrl.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var url in urls)
            {
                if (urlToLookFor.StartsWith(url))
                {
                    return true;    //if the request url starts with any of the bypass urls approve the certificate
                }
            }

            return false;
        }

        /// <summary>
        /// Takes a guid and shrinks it into 22 characters.
        /// Taken from   http://www.singular.co.nz/blog/archive/2007/12/20/shortguid-a-shorter-and-url-friendly-guid-in-c-sharp.aspx
        /// </summary>
        /// <param name="value">The guid to shrink</param>
        /// <returns></returns>
        public static string ShortenGuid(Guid value)
        {
            string base64Guid = Convert.ToBase64String(value.ToByteArray());
            base64Guid = base64Guid.Replace("/", "_").Replace("+", "-");
            return base64Guid.Substring(0, 22);
        }

        /// <summary>
        /// Expands a shorten guid back to its original value
        /// Taken from http://www.singular.co.nz/blog/archive/2007/12/20/shortguid-a-shorter-and-url-friendly-guid-in-c-sharp.aspx
        /// </summary>
        /// <param name="shortenGuid">the shorten string returned from Tools.ShortenGuid</param>
        /// <returns>A guid</returns>
        public static Guid GetGuidFrom(string shortenGuid)
        {
            shortenGuid = shortenGuid
                .Replace("_", "/")
                .Replace("-", "+");

            byte[] buffer = Convert.FromBase64String(shortenGuid + "==");
            return new Guid(buffer);
        }

        /// <summary>
        /// Expands a shorten guid back to its original value
        /// Taken from http://www.singular.co.nz/blog/archive/2007/12/20/shortguid-a-shorter-and-url-friendly-guid-in-c-sharp.aspx
        /// </summary>
        /// <param name="shortenGuid">the shorten string returned from Tools.ShortenGuid</param>
        /// <param name="value">The guid that will be populated.</param>
        /// <returns>A guid</returns>
        public static bool TryGetGuidFrom(string shortenGuid, out Guid value)
        {
            value = Guid.Empty;
            try
            {
                value = Tools.GetGuidFrom(shortenGuid);
                return true;
            }
            catch (FormatException)
            {
            }
            catch (ArgumentException)
            {
            }
            return false;
        }

        /// <summary>
        /// Retrieves the given url and puts it in a string.
        /// </summary>
        /// <param name="url">The url of the document which should be retrieved.</param>
        /// <returns>String with the text</returns>
        public static string GetURL(string url)
        {
            HttpWebRequest request = HttpWebRequest.Create(url) as HttpWebRequest;

            string file = null;
            using (WebResponse response = request.GetResponse())
            {

                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    file = reader.ReadToEnd();
                }
            }
            return file;
        }

        /// <summary>
        /// Gets the description attribute (System.ComponentModel) of an Enum.
        /// </summary>
        /// <remarks>
        /// Copied from http://stackoverflow.com/questions/479410/enum-tostring-with-user-friendly-strings#answer-479417
        /// </remarks>
        /// <typeparam name="T">An Enum type</typeparam>
        /// <param name="enumerationValue">The Enum value to get description of</param>
        /// <returns>The Enum's description</returns>
        public static string GetDescription<T>(this T enumerationValue)
            where T : struct
        {
            Type type = enumerationValue.GetType();
            if (!type.IsEnum)
            {
                throw new ArgumentException("EnumerationValue must be of Enum type", "enumerationValue");
            }

            //Tries to find a DescriptionAttribute for a potential friendly name
            //for the enum
            MemberInfo[] memberInfo = type.GetMember(enumerationValue.ToString());
            if (memberInfo != null && memberInfo.Length > 0)
            {
                object[] attrs = memberInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attrs != null && attrs.Length > 0)
                {
                    //Pull out the description value
                    return ((DescriptionAttribute)attrs[0]).Description;
                }
            }
            //If we have no description attribute, just return the ToString of the enum
            return enumerationValue.ToString();
        }

        /// <summary>
        /// This method dumps the contents of a stream into a byte array. Note: It will most likely fail if the size of the stream is > int.max
        /// http://www.yoda.arachsys.com/csharp/readbinary.html
        /// It copies 32K at a time.
        /// </summary>
        /// <param name="input">input stream to read from</param>
        /// <param name="initialLength">length you think it is. stream.length is not implemented for all streams so it could not do that here.</param>
        /// <returns>read 32k at a time </returns>
        public static byte[] ReadFully(Stream input, int initialLength)
        {
            if (initialLength < 1)
            {
                initialLength = 32768; //32k
            }
            int read = 0;
            int chunk;
            byte[] buffer = new byte[initialLength];

            while ((chunk = input.Read(buffer, read, buffer.Length - read)) > 0)
            {
                read += chunk;
                if (read == buffer.Length) //we filled the buffer
                {
                    int nextByte = input.ReadByte();
                    if (nextByte == -1) //check to see if we are at the end
                    {
                        return buffer; //if so return
                    }

                    byte[] newBuffer = new byte[buffer.Length * 2]; //re create bigger buffer with twice the size
                    Array.Copy(buffer, newBuffer, buffer.Length);   //copy contents
                    newBuffer[read] = (byte)nextByte;  //set the buffer we read
                    buffer = newBuffer; //overwrite the old buffer
                    read++;  //we just added 1 more byte
                }
            }
            byte[] ret = new byte[read];  //the buffer is too large  so lets shrink
            Array.Copy(buffer, ret, read);
            return ret;
        }

        /// <summary>
        /// Calculate fully amortized payment.
        /// </summary>
        /// <param name="originalPrincipal">Original Loan Amount</param>
        /// <param name="interestRate">Annual Interest Rate</param>
        /// <param name="termInMonths">Term in months</param>
        /// <returns></returns>
        public static decimal CalculatePayment(decimal originalPrincipal, decimal interestRate, int termInMonths)
        {
            double i = (double)interestRate / 1200;
            double iPower = Math.Pow(1 + i, (double)(-1 * termInMonths));
            double payment = (double)originalPrincipal * i / (1 - iPower);

            return (decimal)Math.Round(payment, 2);

        }

        /// <summary>
        /// Calculates the qualifying rate given the specified <paramref name="options"/>.
        /// </summary>
        /// <param name="options">
        /// The options for the calculation.
        /// </param>
        /// <returns>
        /// The qualifying rate.
        /// </returns>
        public static decimal CalculateQualRate(QualRateCalculationOptions options)
        {
            if (!options.UseQualRate)
            {
                return 0M;
            }

            var firstCalculationValue = GetQualRateCalculationValue(options, options.CalculationField1, options.Adjustment1);
            if (options.CalculationType == QualRateCalculationT.FlatValue)
            {
                return firstCalculationValue;
            }

            var secondCalculationValue = GetQualRateCalculationValue(options, options.CalculationField2, options.Adjustment2);
            return Math.Max(firstCalculationValue, secondCalculationValue);
        }

        /// <summary>
        /// Gets the calculated value for the specified <paramref name="calculationField"/>.
        /// </summary>
        /// <param name="options">
        /// The options for the calculation.
        /// </param>
        /// <param name="calculationField">
        /// The rate field for the calculation.
        /// </param>
        /// <param name="calculationAdjustment">
        /// The adjustment for the rate field.
        /// </param>
        /// <returns>
        /// The calculated value.
        /// </returns>
        /// <remarks>
        /// Loans submitted with templates that use the legacy LPE Upload Value 
        /// calculation field should have their qualifying rate calculation locked
        /// to the value computed at submission and should therefore not reach this
        /// calculation method.
        /// </remarks>
        public static decimal GetQualRateCalculationValue(QualRateCalculationOptions options, QualRateCalculationFieldT calculationField, decimal calculationAdjustment)
        {
            switch (calculationField)
            {
                case QualRateCalculationFieldT.NoteRate:
                    return options.NoteRate + calculationAdjustment;
                case QualRateCalculationFieldT.FullyIndexedRate:
                    return options.FullyIndexedRate + calculationAdjustment;
                case QualRateCalculationFieldT.Index:
                    return options.IndexRate + calculationAdjustment;
                case QualRateCalculationFieldT.Margin:
                    return options.MarginRate + calculationAdjustment;
                case QualRateCalculationFieldT.LpeUploadValue:
                    return 0M;
                default:
                    throw new UnhandledEnumException(calculationField);
            }
        }

        public static decimal AlwaysRoundDown(decimal value, int decimals)
        {
            // Always round down base on significant digit.
            // Assume decimals=3.
            // 10.000 ==> 10.000
            // 10.0000001 ==> 10.000
            // 10.0009999 ==> 10.000
            // 10.0005000 ==> 10.000

            // -10.0000001 ==> -10.000
            // -10.0009999 ==> -10.000
            // -10.0005000 ==> -10.000
            decimal roundValue = decimal.Round(value, decimals);
            if ((value > 0 && roundValue < value) || (value < 0 && roundValue > value))
            {
                return roundValue; // This number is already round down.
            }
            else
            {
                if (value >= 0)
                {
                    if (value < roundValue)
                        return roundValue - (decimal)Math.Pow(10, -1 * decimals);
                    else
                        return roundValue;
                }
                else
                {
                    // Value is negative number.
                    if (roundValue < value)
                        return roundValue + (decimal)Math.Pow(10, -1 * decimals);
                    else
                        return roundValue;
                }
            }

        }
        public static decimal FannieMaeLtvRounding(decimal value)
        {
            // 8/30/2010 dd - OPM 50817 - Implement the way FannieMae round their LTV.
            //    Step 1 - Truncate the value to two decimal places.
            //    Step 2 - Round up to the next whole percent.
            //    i.e:  96.01 --> 97%
            //          80.001 --> 80%
            decimal truncateValue = Math.Truncate(value * 100);
            return Math.Ceiling(truncateValue / 100); // Round up.
        }
        public static decimal LtvRounding(decimal value)
        {
            // This method should only be use to display LTV, CLTV, and HCLTV only (HCLTV added in case 88297 -mp).
            // We will truncate value instead of rounding if the hundredth is modify due to rounding.
            // Example:
            //    80.00000012313 --> 80.001
            //    79.999923424   --> 79.999
            //    79.9980002     --> 79.999

            int original100th = (int)(value * 100) % 10;
            decimal roundValue = AlwaysRoundUp(value, 3);
            int round100th = (int)(roundValue * 100) % 10;

            if (original100th == round100th)
            {
                return roundValue;
            }
            else
            {
                // Truncate instead of round up.
                return roundValue - .001M;
            }

        }

        public static decimal EnsureValidRateValue(decimal value)
        {
            // 4/17/2012 dd - Ensure that rate value should be able to save to DB with no error.
            // Our database normally use decimal(9,3) as rate. Therefore large number will cause exception.

            var absVal = Math.Abs(value);
            if (absVal > 10000 || absVal < 0.000001M)
            {
                return 0;
            }
            return value;
        }


        /// <summary>
        /// If the value is below 0.000001M returns 0 if its greater than 10000 returns 10000;
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static decimal EnsureValidRateValueWithMax(decimal value)
        {
            // 4/17/2012 dd - Ensure that rate value should be able to save to DB with no error.
            // Our database normally use decimal(9,3) as rate. Therefore large number will cause exception.

            var absVal = Math.Abs(value);
            if (absVal >= 10000)
            {
                return 10000;
            }
            if (absVal < 0.000001M)
            {
                return 0;
            }
            return value;
        }
        /// <summary>
        /// Snap the number "toBeSnapped" to the closest gridline, or to the adjacent gridline depending on the rounding type.
        /// Example: 3.2 snaps to 3.25 when the gridlines are spaced every 0.25, unless round down is used.
        /// *Note: if it's a draw, it chooses the gridline that that is an even # of gridlines from 0.
        /// </summary>
        public static decimal SnapToGridLine(decimal toBeSnapped, decimal gridLineSpacing, E_sRAdjRoundT roundingType)
        {
            // SK - adding this method to make it unit testable.
            if (gridLineSpacing > 0)
            {
                decimal remainder = toBeSnapped % gridLineSpacing; // i.e. the amount beyond the lower grid line that the number is.
                if (remainder > 0)
                {
                    var gridLineLower = toBeSnapped - remainder;       // the gridline below the number.
                    var gridLineUpper = gridLineLower + gridLineSpacing;   // the gridline above the number.
                    switch (roundingType)
                    {
                        case E_sRAdjRoundT.Down:
                            return gridLineLower;
                        case E_sRAdjRoundT.Up:
                            return gridLineUpper;
                        case E_sRAdjRoundT.Normal:
                            if ((gridLineUpper - toBeSnapped) < (toBeSnapped - gridLineLower))  // if it's closer to the upper, return that.
                            {
                                return gridLineUpper;
                            }
                            else if ((gridLineUpper - toBeSnapped) > (toBeSnapped - gridLineLower)) // if it's closer to the lower grid line
                            {
                                return gridLineLower;
                            }
                            else // if it's exactly between the two grid line, return the one that is an even # of lines.
                            {
                                int numGridLinesBelow = (int)(gridLineLower / gridLineSpacing);
                                if (numGridLinesBelow % 2 == 0)
                                {
                                    return gridLineLower;
                                }
                                else
                                {
                                    return gridLineUpper;
                                }
                            }
                        default:
                            Tools.LogErrorWithCriticalTracking("rounding type " + roundingType + " not recognized");
                            return gridLineLower;
                    }
                }
            }
            return toBeSnapped;
        }

        public static decimal AlwaysRoundUp(decimal value, int decimals)
        {
            // Always round up base on significant digit.
            // Assume decimals=3.
            // 10.000 ==> 10.000
            // 10.0000001 ==> 10.001
            // 10.0009999 ==> 10.001
            // 10.0005000 ==> 10.001

            // -10.0000001 ==> -10.001
            // -10.0009999 ==> -10.001
            // -10.0005000 ==> -10.001

            decimal roundValue = decimal.Round(value, decimals);
            if ((value > 0 && roundValue > value) || (value < 0 && roundValue < value))
            {
                return roundValue; // This number is already round up.
            }
            else
            {
                if (value >= 0)
                {
                    if (value > roundValue)
                        return roundValue + (decimal)Math.Pow(10, -1 * decimals);
                    else
                        return roundValue;
                }
                else
                {
                    // Value is negative number.
                    if (roundValue > value)
                        return roundValue - (decimal)Math.Pow(10, -1 * decimals);
                    else
                        return roundValue;
                }
            }

        }
        public static string SafeStateCodeWithoutTerritories(string stateCode)
        {
            // 4/11/2012 dd - OPM 65673 - Add GU, PR, VI, AS, MP to property state.
            // 12/6/2013 gf - opm 89690 Pull state codes into ConstApp.cs to minimize duplication.

            if (null == stateCode)
                return "";

            stateCode = stateCode.ToUpper();

            foreach (string code in ConstApp.ValidStateCodesWithoutMilitaryMailCodes)
            {
                if (code == stateCode)
                    return code;
            }
            return "";

        }

        public static void BindPredefineLiabilityDescription(MeridianLink.CommonControls.ComboBox cb)
        {
            cb.Items.Add("1st Existing Lien");
            cb.Items.Add("2nd Existing Lien");
            cb.Items.Add("Home Equity Line of Credit");
            cb.Items.Add("Installment Loan");
            cb.Items.Add("Lease Payments");
            cb.Items.Add("Liens");
            cb.Items.Add("Mortgage");
            cb.Items.Add("Open, 30day Charge Acc");
            cb.Items.Add("Other Liability");
            cb.Items.Add("Other Existing Lien");
            cb.Items.Add("Revolving Charge");
            cb.Items.Add("Taxes");
        }

        public static void Bind_ManuallyEnteredHousingHistory(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("0x30", ManuallyEnteredHousingHistory._0x30));
            ddl.Items.Add(CreateEnumListItem("1x30", ManuallyEnteredHousingHistory._0x60));
            ddl.Items.Add(CreateEnumListItem("0x60", ManuallyEnteredHousingHistory._0x90));
            ddl.Items.Add(CreateEnumListItem("0x90", ManuallyEnteredHousingHistory._1x30));
            ddl.Items.Add(CreateEnumListItem("1x90+", ManuallyEnteredHousingHistory._1x90Plus));
        }

        public static void Bind_ManuallyEnteredHousingEvents(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("None", ManuallyEnteredHousingEvents.None));
            ddl.Items.Add(CreateEnumListItem("Within 12 Months", ManuallyEnteredHousingEvents.Within12Months));
            ddl.Items.Add(CreateEnumListItem("Within 24 Months", ManuallyEnteredHousingEvents.Within24Months));
            ddl.Items.Add(CreateEnumListItem("Within 36 Months", ManuallyEnteredHousingEvents.Within36Months));
            ddl.Items.Add(CreateEnumListItem("Within 48 Months", ManuallyEnteredHousingEvents.Within48Months));
        }

        public static void Bind_ManuallyEnteredBankruptcy(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("None", ManuallyEnteredBankruptcy.None));
            ddl.Items.Add(CreateEnumListItem("Within 12 Months", ManuallyEnteredBankruptcy.Within12Months));
            ddl.Items.Add(CreateEnumListItem("Within 24 Months", ManuallyEnteredBankruptcy.Within24Months));
            ddl.Items.Add(CreateEnumListItem("Within 36 Months", ManuallyEnteredBankruptcy.Within36Months));
            ddl.Items.Add(CreateEnumListItem("Within 48 Months", ManuallyEnteredBankruptcy.Within48Months));
        }

        public static void Bind_SecurityEventType(DropDownList ddl)
        {
            // Keep this in alphabetical order, except "All Events"
            ddl.Items.Add(CreateEnumListItem("All Events", SecurityEventType.AllEvents));
            ddl.Items.Add(CreateEnumListItem("Account Creation", SecurityEventType.AccountCreation));
            ddl.Items.Add(CreateEnumListItem("Account Lock", SecurityEventType.AccountLocking));
            ddl.Items.Add(CreateEnumListItem("Become User", SecurityEventType.BecomeUser));
            ddl.Items.Add(CreateEnumListItem("Changing User Settings", SecurityEventType.ChangingUserSettings));
            ddl.Items.Add(CreateEnumListItem("Failed Log In", SecurityEventType.FailedLogin));
            ddl.Items.Add(CreateEnumListItem("Failed Webservice Authentication", SecurityEventType.FailedWebserviceAuthentication));
            ddl.Items.Add(CreateEnumListItem("Log In", SecurityEventType.LogIn));
            ddl.Items.Add(CreateEnumListItem("Log Out", SecurityEventType.LogOut));
            ddl.Items.Add(CreateEnumListItem("Sensitive Report Export", SecurityEventType.SensitiveReportExport));
            ddl.Items.Add(CreateEnumListItem("Webservice Authentication", SecurityEventType.WebserviceAuthentication));
        }

        public static void Bind_StatesWithoutMilitaryMailCodes(DropDownList ddl)
        {
            Bind_StatesWithoutMilitaryMailCodes(ddl.Items);
        }

        public static void Bind_StatesWithoutMilitaryMailCodes(HtmlSelect ddl)
        {
            Bind_StatesWithoutMilitaryMailCodes(ddl.Items);
        }

        private static void Bind_StatesWithoutMilitaryMailCodes(ListItemCollection items)
        {
            foreach (var state in ConstApp.ValidStateCodesWithoutMilitaryMailCodes.OrderBy(s => s))
            {
                items.Add(new ListItem(state, state));
            }
        }

        /// <summary>
        /// Converts the phone number to the format we want in the DB.
        /// This will strip all non-numerical numbers from the string then feed it through LosConvert.
        /// </summary>
        /// <param name="phoneNumber">The phone number to format.</param>
        /// <param name="formattedNumber">The formatted phone number.</param>
        /// <returns>True if formatted. False otherwise.</returns>
        public static bool ToPhoneFormat(string phoneNumber, out string formattedNumber)
        {
            string strippedNumber = CheckAndStripPhoneNumber(phoneNumber);
            if (string.IsNullOrEmpty(strippedNumber))
            {
                formattedNumber = null;
                return false;
            }

            formattedNumber = new LosConvert().ToPhoneNumFormat(strippedNumber, FormatDirection.ToDb);
            return true;
        }

        /// <summary>
        /// Formats any phone number to a different format.
        /// The phone number will be stripped of all non-numeric characters before being formatted.
        /// (123)456-7890 will be stripped to 1234567890.
        /// </summary>
        /// <param name="phoneNumber">The phone number to format.</param>
        /// <param name="pattern">The pattern to match. This pattern should match against the phone number stripped of all non-numeric characters.</param>
        /// <param name="formatTarget">The target format.</param>
        /// <param name="formattedNumber">The output number.</param>
        /// <returns>True if the number was formatted. False otherwise.</returns>
        public static bool ToPhoneFormat(string phoneNumber, string pattern, string formatTarget, out string formattedNumber)
        {
            string strippedNumber = CheckAndStripPhoneNumber(phoneNumber);
            if (string.IsNullOrEmpty(strippedNumber))
            {
                formattedNumber = null;
                return false;
            }

            formattedNumber = Regex.Replace(strippedNumber, pattern, formatTarget);
            return true;
        }

        /// <summary>
        /// Takes a phone number, checks its validity, and then strips it down to just its numerals.
        /// </summary>
        /// <param name="phoneNumber">The phone number.</param>
        /// <returns>The stripped phone number if valid. Null otherwise.</returns>
        private static string CheckAndStripPhoneNumber(string phoneNumber)
        {
            string numberToFormat = phoneNumber?.Trim();
            PhoneNumber? validatedPhoneNumber = PhoneNumber.CreateWithValidation(numberToFormat);

            if (!validatedPhoneNumber.HasValue)
            {
                return null;
            }

            return Regex.Replace(validatedPhoneNumber.Value.ToString(), "[^0-9]", string.Empty, RegexOptions.Compiled);
        }

        /// <summary>
        /// This method will only return a valid upper case state code. If input is invalid value then empty string will return.
        /// </summary>
        /// <param name="stateCode"></param>
        /// <returns></returns>
        public static string SafeStateCode(string stateCode)
        {
            string[] valid_states = {"", "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "DC", "FL",
                                        "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME",
                                        "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NC", "ND", "NE",
                                        "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR",
                                        "PA", "PR", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "VI",
                                        "WA", "WV", "WI", "WY", "GU", "PR", "AA", "AP", "AE", "AS", "MP"};
            if (null == stateCode)
                return "";

            stateCode = stateCode.ToUpper();

            foreach (string code in valid_states)
            {
                if (code == stateCode)
                    return code;
            }
            return "";
        }
        public static string SecretHashForPml(Guid userID)
        {
            string saltFormat = ConstAppDavid.PmlSaltFormat;
            DateTime dt = DateTime.Now;
            string dd = dt.Day.ToString();
            string mm = dt.Month.ToString();
            string hh = dt.Hour.ToString();
            string salt = saltFormat.Replace("{dd}", dd).Replace("{mm}", mm).Replace("{hh}", hh);

            // Replace +, /, = with url friendly character ., _, -
            return HashOfPassword(userID.ToString() + salt).Replace('+','.').Replace('/','_').Replace('=', '-');
        }
        public static string HashOfPassword(string password)
        {
            SHA256 sha1 = new SHA256Managed();
            return Convert.ToBase64String(sha1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password.ToLower())));
        }

        public static bool IsPasswordValid(DbConnectionInfo connInfo, string loginName, Guid brokerPMLSiteId, string password)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@LoginNm", loginName),
                                            new SqlParameter("@BrokerPmlSiteId", brokerPMLSiteId)
                                        };

            try
            {
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "GetPasswordInfo", parameters))
                {
                    if (reader.Read())
                    {
                        string passwordHash = (string)reader["PasswordHash"];
                        string passwordSalt = (string)reader["PasswordSalt"];
                        bool isUpdatedPassword = (bool)reader["IsUpdatedPassword"];

                        if (isUpdatedPassword)
                        {
                            return EncryptionHelper.ValidatePBKDF2Hash(password, passwordHash, passwordSalt);
                        }
                        else
                        {
                            return passwordHash == HashOfPassword(password.ToLower());
                        }
                    }
                }
            }
            catch (NotFoundException)
            {
                return false;
            }
            catch (SqlException sqlExc)
            {
                Tools.LogError("Failed to retrieve password info for login " + loginName + ".", sqlExc);
                return false;
            }

            return false;
        }

        public static DateTime GetDBCurrentDateTime()
        {
            DateTime dt = DateTime.Now;

            string query = "select getdate()";

            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                if (reader.Read())
                    dt = (DateTime)reader[0];
            };

            DBSelectUtility.ProcessDBData(DataSrc.LOShareROnly, query, null, null, readHandler);

            return dt;
        }

        public static void CallDB()
        {
            string query = "select 1";

            Action<IDataReader> readHandler = delegate (IDataReader reader)
            {
                reader.Read();
            };

            DBSelectUtility.ProcessDBData(DataSrc.LOShareROnly, query, null, null, readHandler);
        }

        public const string EmptyGuid = "00000000-0000-0000-0000-000000000000";

        static public void NotifyImportAssignees(Guid loanId)
        {

            AbstractUserPrincipal p = Thread.CurrentPrincipal as AbstractUserPrincipal;

            string[] fields = new string[] {
											   "sEmployeeLoanRepId" ,
											   "sEmployeeUnderwriterId",
											   "sEmployeeProcessorId",
											   "sEmployeeLoanOpenerId" ,
											   "sEmployeeCallCenterAgentId",
											   "sEmployeeLenderAccExecId",
											   "sEmployeeLockDeskId",
											   "sEmployeeLenderAccExecId",
											   "sEmployeeRealEstateAgentId",
											   "sLNm", "sBranchId","sPrimBorrowerFullNm",};

            CPageData dataLoan = new CFullAccessPageData(loanId, "Notify Import Assignees", CSelectStatementProvider.GetProviderForTargets(fields, false));
            dataLoan.InitLoad();
            if (p == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "Thread.CurrentPrincipal is null. - Should be abstract user principal.");
            }
            if (dataLoan.sEmployeeUnderwriterId != Guid.Empty)
            {
                RoleChange.Mark(p.EmployeeId, p.BrokerId, dataLoan, Guid.Empty, dataLoan.sEmployeeUnderwriterId, CEmployeeFields.s_UnderwriterRoleId, E_RoleChangeT.Import);
            }
            if (dataLoan.sEmployeeLoanRepId != Guid.Empty)
            {
                RoleChange.Mark(p.EmployeeId, p.BrokerId, dataLoan, Guid.Empty, dataLoan.sEmployeeLoanRepId, CEmployeeFields.s_LoanRepRoleId, E_RoleChangeT.Import);
            }

            if (dataLoan.sEmployeeLoanOpenerId != Guid.Empty)
            {
                RoleChange.Mark(p.EmployeeId, p.BrokerId, dataLoan, Guid.Empty, dataLoan.sEmployeeLoanOpenerId, CEmployeeFields.s_LoanOpenerId, E_RoleChangeT.Import);
            }

            if (dataLoan.sEmployeeLenderAccExecId != Guid.Empty)
            {
                RoleChange.Mark(p.EmployeeId, p.BrokerId, dataLoan, Guid.Empty, dataLoan.sEmployeeLenderAccExecId, CEmployeeFields.s_LenderAccountExecRoleId, E_RoleChangeT.Import);
            }

            if (dataLoan.sEmployeeProcessorId != Guid.Empty)
            {
                RoleChange.Mark(p.EmployeeId, p.BrokerId, dataLoan, Guid.Empty, dataLoan.sEmployeeProcessorId, CEmployeeFields.s_ProcessorRoleId, E_RoleChangeT.Import);
            }

            if (dataLoan.sEmployeeCallCenterAgentId != Guid.Empty)
            {
                RoleChange.Mark(p.EmployeeId, p.BrokerId, dataLoan, Guid.Empty, dataLoan.sEmployeeCallCenterAgentId, CEmployeeFields.s_CallCenterAgentRoleId, E_RoleChangeT.Import);
            }

            if (dataLoan.sEmployeeLockDeskId != Guid.Empty)
            {
                RoleChange.Mark(p.EmployeeId, p.BrokerId, dataLoan, Guid.Empty, dataLoan.sEmployeeLockDeskId, CEmployeeFields.s_LockDeskRoleId, E_RoleChangeT.Import);
            }
            if (dataLoan.sEmployeeRealEstateAgentId != Guid.Empty)
            {
                RoleChange.Mark(p.EmployeeId, p.BrokerId, dataLoan, Guid.Empty, dataLoan.sEmployeeRealEstateAgentId, CEmployeeFields.s_RealEstateAgentRoleId, E_RoleChangeT.Import);
            }
        }

        public static Guid GetLoanIdByLoanName(Guid brokerId, string sLNm)
        {
            return GetLoanIdByLoanName(brokerId, sLNm, false);
        }

        public static Guid GetLoanIdByLoanName(Guid brokerId, string sLNm, bool includeInactive)
        {
            Guid sLId = Guid.Empty;
            if (!string.IsNullOrEmpty(sLNm) && brokerId != Guid.Empty)
            {

                SqlParameter[] parameters = {
                                                new SqlParameter("@sBrokerId", brokerId),
                                                new SqlParameter("@sLNm", sLNm),
                                                new SqlParameter("@CheckInvalid", includeInactive)
                                            };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetLoanIdByLoanName", parameters))
                {
                    if (reader.Read())
                    {
                        sLId = (Guid)reader["sLId"];
                    }
                }
            }

            return sLId;
        }

        public static Guid GetLoanIdByLoanReferenceNumber(Guid brokerId, string sLRefNm, bool includeInactive = false)
        {
            Guid sLId = Guid.Empty;
            if (!string.IsNullOrEmpty(sLRefNm) && brokerId != Guid.Empty)
            {

                SqlParameter[] parameters = {
                                                new SqlParameter("@sBrokerId", brokerId),
                                                new SqlParameter("@sLRefNm", sLRefNm),
                                                new SqlParameter("@IncludeInactive", includeInactive)
                                            };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetLoanIdByLoanReferenceNumber", parameters))
                {
                    if (reader.Read())
                    {
                        sLId = (Guid)reader["sLId"];
                    }
                }
            }

            return sLId;
        }

        /// <summary>
        /// Retrieves the loan name for the associated <paramref name="loanId"/>.
        /// </summary>
        /// <param name="brokerId">
        /// The <see cref="Guid"/> of the loan's broker.
        /// </param>
        /// <param name="loanId">
        /// The <see cref="Guid"/> of the loan.
        /// </param>
        /// <returns>
        /// The name of the loan with the associated <paramref name="loanId"/>
        /// or <see cref="string.Empty"/> if no loan exists with the specified
        /// <paramref name="loanId"/>.
        /// </returns>
        public static string GetLoanNameByLoanId(Guid brokerId, Guid loanId)
        {
            var loanName = string.Empty;

            SqlParameter[] parameters = { new SqlParameter("@LoanID", loanId) };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetLoanNumberByLoanID", parameters))
            {
                if (reader.Read())
                {
                    loanName = (string)reader["sLNm"];
                }
            }

            return loanName;
        }

        /// <summary>
        /// Retrieves the loan reference number for the associated <paramref name="loanId"/>.
        /// </summary>
        /// <param name="brokerId">
        /// The <see cref="Guid"/> of the loan's broker.
        /// </param>
        /// <param name="loanId">
        /// The <see cref="Guid"/> of the loan.
        /// </param>
        /// <returns>
        /// The reference number of the loan with the associated <paramref name="loanId"/>
        /// or <see cref="string.Empty"/> if no loan exists with the specified
        /// <paramref name="loanId"/>.
        /// </returns>
        public static string GetLoanRefNmByLoanId(Guid brokerId, Guid loanId)
        {
            string sLRefNm = string.Empty;

            SqlParameter[] parameters = { new SqlParameter("@LoanId", loanId) };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetLoanReferenceNumberByLoanID", parameters))
            {
                if (reader.Read())
                {
                    sLRefNm = (string)reader["sLRefNm"];
                }
            }

            return sLRefNm;
        }

        /// <summary>
        /// Retrieves the file type of the given loan.
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="loanId"></param>
        /// <returns></returns>
        public static E_sLoanFileT GetLoanFileType(Guid brokerId, Guid loanId)
        {
            SqlParameter[] parameters = { new SqlParameter("@LoanId", loanId) };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetLoanFileType", parameters))
            {
                if (reader.Read())
                {
                    return (E_sLoanFileT)reader["sLoanFileT"];
                }

                throw new NotFoundException($"No loan file was found matching the ID {loanId}.");
            }
        }

        /// <summary>
        /// Retrieves broker ID using a customer code.
        /// Throws NotFoundException if BrokerId not found.
        /// </summary>
        /// <param name="customerCode">Customer Code</param>
        /// <returns>Broker ID if broker found.</returns>
        public static Guid GetBrokerIdByCustomerCode(string customerCode)
        {
            Guid brokerId = Guid.Empty;

            SqlParameter[] parameters =
            {
                new SqlParameter("@CustomerCode", customerCode)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "RetrieveBrokerIdByCustomerCode", parameters))
            {
                if (reader.Read())
                {
                    brokerId = (Guid)reader["BrokerId"];
                }
                else
                {
                    throw new NotFoundException("Customer Code is not found.");
                }
            }

            return brokerId;
        }

        /// <summary>
        /// Retrieves broker ID using a customer code.
        /// Throws NotFoundException if BrokerId not found.
        /// </summary>
        /// <param name="sLId"></param>
        /// <returns>Broker ID if broker found.</returns>
        public static Guid GetBrokerIdByLoanID(Guid sLId)
        {
            Guid brokerId;
            DbConnectionInfo.GetConnectionInfoByLoanId(sLId, out brokerId);
            return brokerId;
        }

        /// <summary>
        /// Retrieves the value of <seealso cref="CPageData.sBranchId"/> for the specified loan.
        /// </summary>
        /// <param name="brokerId">The lender id for the loan.</param>
        /// <param name="loanId">The loan id.</param>
        /// <returns>The branch id for the loan, or <see cref="Guid.Empty"/> if it is not found.</returns>
        public static Guid GetLoanBranchIdByLoanId(Guid brokerId, Guid loanId)
        {
            SqlParameter[] parameters = { new SqlParameter("@LoanId", loanId) };
            using (IDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetBranchByLoanId", parameters))
            {
                if (reader.Read())
                {
                    return (Guid)reader["sBranchId"];
                }
            }

            return Guid.Empty;
        }

        /// <summary>
        /// Note this is not intended for the main apps since in the main apps you should use LinkLoans. <para></para>
        /// Even though this can return a reason it failed, it may also throw exceptions.
        /// </summary>
        /// <param name="loan1ID">The ID of the first loan to link.</param>
        /// <param name="loan2ID">The ID of the second loan to link.</param>
        /// <param name="scrapExistingLinkages">If true, deletes existing linkages for each loan.  If false, raises an error if either loan has existing linkages.</param>
        /// <returns>The reasons it failed, or empty.</returns>
        static public void LinkLoansForLOAdmin(Guid loan1ID, Guid loan2ID, bool scrapExistingLinkages)
        {

            Guid brokerID = Tools.GetBrokerIdByLoanID(loan1ID);
            Guid brokerID2 = Tools.GetBrokerIdByLoanID(loan2ID);

            if (brokerID != brokerID2)
            {
                string msg = string.Format(
                    "Loan {0} is at broker {1} which is not the same as {2}, the broker for loan {3}",
                    loan1ID,
                    BrokerDB.RetrieveById(brokerID).AsSimpleStringForLog(),
                    BrokerDB.RetrieveById(brokerID2).AsSimpleStringForLog(),
                    loan2ID);

                throw new CBaseException(msg, msg);
            }

            // OPM 459984 - Don't link loans if they are on different version numbers.
            LoanVersionT loan1Version = GetLoanVersion(brokerID, loan1ID);
            LoanVersionT loan2Version = GetLoanVersion(brokerID, loan2ID);

            if (loan1Version != loan2Version)
            {
                string msg = $"Cannot link loans not on the same version number. Loan {loan1ID} is on version {loan1Version.ToString("d")}. Loan {loan2ID} is on version {loan2Version.ToString("d")}.";
                throw new CBaseException(msg, msg);
            }

            // check no existing linkages or bail.
            if (false == scrapExistingLinkages)
            {
                if(HasLinkedLoan(brokerID, loan1ID))
                {
                    string msg = string.Format(
                        "Loan {0} at broker {1} has linked loans.",
                        loan2ID,
                        BrokerDB.RetrieveById(brokerID).AsSimpleStringForLog());

                    throw new CBaseException(msg, msg);
                }

                if(HasLinkedLoan(brokerID, loan2ID))
                {
                    string msg = string.Format(string.Format(
                        "Loan {0} at broker {1} has linked loans.",
                        loan2ID,
                        BrokerDB.RetrieveById(brokerID).AsSimpleStringForLog()));

                    throw new CBaseException(msg, msg);
                }
            }

            // at this point same broker and no existing linkages or user desires to delete existing linkages.
            Tools.LinkLoans(brokerID, loan1ID, loan2ID);
        }

        /// <summary>
        /// In addition to updating Loan_Link: deletes existing linkages and updates cached sLinkedLoanName.
        /// </summary>
        static public void LinkLoans(Guid brokerId, Guid loan1Id, Guid loan2Id)
        {
            SqlParameter[] pars = new SqlParameter[]
                {
                    new SqlParameter( "@sLId1", loan1Id ),
                    new SqlParameter( "@sLId2", loan2Id ),
                    new SqlParameter( "@brokerID", brokerId)
                };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "LinkLoans", 5, pars);

            string[] cacheFields = new string[] { "sLinkedLoanName" };
            Tools.UpdateCacheTable(loan1Id, cacheFields);
            Tools.UpdateCacheTable(loan2Id, cacheFields);
        }

        static public void UnlinkLoans(AbstractUserPrincipal userInfo, Guid sLId)
        {
            // 6/29/2005 kb - We now (case 2218) secure delete requests, so
            // get the calling user's credentials and check.

            if (userInfo == null)
            {
                throw new PageDataAccessDenied("Unable to unlink this loan.", new ArgumentException("Invalid user credentials."));
            }
            LoanValueEvaluator valueEvaluator = new LoanValueEvaluator(userInfo.ConnectionInfo, userInfo.BrokerId, sLId, WorkflowOperations.WriteLoanOrTemplate);
            valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(userInfo));


            bool canWrite = LendingQBExecutingEngine.CanPerform(WorkflowOperations.WriteLoanOrTemplate, valueEvaluator);
            if (canWrite == false)
            {
                throw new PageDataAccessDenied(new AccessBinding(userInfo.UserId, sLId, false, false), "Insufficient permissions", "Unable to unlink this loan.");
            }



            SqlParameter[] parameters = { new SqlParameter("@sLId", sLId) };

            Guid sLId1 = Guid.Empty, sLId2 = Guid.Empty;

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(userInfo.BrokerId, "UnlinkLoans", parameters))
            {
                if (reader.Read())
                {

                    if (reader["sLId1"] != DBNull.Value)
                    {
                        sLId1 = (Guid)reader["sLId1"];
                    }
                    if (reader["sLId2"] != DBNull.Value)
                    {
                        sLId2 = (Guid)reader["sLid2"];
                    }
                }
            }

            string[] cacheFields = new string[] { "sLinkedLoanName" };
            if (sLId1 != Guid.Empty)
            {
                Tools.UpdateCacheTable(sLId1, cacheFields);
            }
            if (sLId2 != Guid.Empty)
            {
                Tools.UpdateCacheTable(sLId2, cacheFields);
            }
        }

        static public Guid GetLinkedLoanId(Guid brokerId, Guid srcLoanId)
        {
            var linkedLoanID = GetLinkedLoanIdIfItExists(brokerId, srcLoanId);

            if (linkedLoanID.HasValue)
            {
                return linkedLoanID.Value;
            }

            throw new CBaseException(ErrorMessages.CannotAccessOldLinkedLoan,
                string.Format("Failed to retrieve linked loan id for loan id {0}", srcLoanId.ToString()));
        }

        static public bool HasLinkedLoan(Guid brokerId, Guid srcLoanId)
        {
            return GetLinkedLoanIdIfItExists(brokerId, srcLoanId) != null;
        }

        static private Guid? GetLinkedLoanIdIfItExists(Guid brokerId, Guid srcLoanId)
        {
            SqlParameter[] pars = new SqlParameter[]
            {
                new SqlParameter( "@srcLoanId", srcLoanId )
            };

            using (DbDataReader r = StoredProcedureHelper.ExecuteReader(brokerId, "RetrieveLinkedLoanId", pars))
            {
                if (r.Read())
                {
                    //select sLId1, sLId2, sLNm1, sLNm2
                    Guid sLId1 = new Guid(r["sLId1"].ToString());
                    Guid sLId2 = new Guid(r["sLId2"].ToString());

                    if (sLId1 == srcLoanId)
                    {
                        return sLId2;
                    }

                    if (sLId2 == srcLoanId)
                    {
                        return sLId1;
                    }

                    Tools.LogBug("RetrieveLinkedLoanId store proc is not doing the right thing.");
                    throw new CBaseException(
                        ErrorMessages.Generic,
                        "RetrieveLinkedLoanId did the wrong thing so the behavior of HasLinkedLoan is unspecified.");
                }
                else
                {
                    return null;
                }
            }
        }

        static private LoanVersionT GetLoanVersion(Guid brokerId, Guid loanId)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter( "@LoanId", loanId )
            };

            IStoredProcedureDriver driver = GenericLocator<IStoredProcedureDriverFactory>.Factory.Create(TimeoutInSeconds.Thirty);

            using (DbConnection connection = DbAccessUtils.GetConnection(brokerId))
            using (IDataReader reader = driver.ExecuteReader(connection, null, StoredProcedureName.Create("RetrieveLoanVersion").Value, parameters))
            {
                if (reader.Read())
                {
                    return (LoanVersionT)reader["sLoanVersionT"];
                }
            }

            // If we got here then we did not find the loan version for this loan in the DB.
            throw new CBaseException("Failed to retrieve Loan Version number.", $"Failed to retrieve Loan Version number for sLId=[{loanId}].");
        }

        public static string GetTaskDueDateDescription(Guid sLId, int TaskDueDateCalcDays, string TaskDueDateCalcField, bool sanitize)
        {
            return GetTaskDueDateDescription(sLId, TaskDueDateCalcDays, TaskDueDateCalcField, sanitize, null);
        }


        public static string GetTaskDueDateDescription(Guid sLId, int TaskDueDateCalcDays, string TaskDueDateCalcField, bool sanitize, Dictionary<string, string> loanDateCache)
        {
            string desc = GetTaskDueDateDescription(TaskDueDateCalcDays, TaskDueDateCalcField, sanitize);
            if (string.IsNullOrEmpty(Task.GetCalculatedDueDate(sLId, TaskDueDateCalcDays, TaskDueDateCalcField, loanDateCache))
                && !string.IsNullOrEmpty(desc))
            {
                return "TBD: " + desc;
            }
            else
            {
                return desc;
            }
        }

        public static string GetTaskDueDateDescription(int TaskDueDateCalcDays, string TaskDueDateCalcField, bool sanitize)
        {
            if (String.IsNullOrEmpty(TaskDueDateCalcField))
            {
                return String.Empty;
            }

            StringBuilder sb = new StringBuilder();

            if (TaskDueDateCalcDays != 0)
            {
                if (TaskDueDateCalcDays > 0)
                {
                    sb.Append(TaskDueDateCalcDays);
                }
                else
                {
                    sb.Append(TaskDueDateCalcDays * -1);
                }

                sb.Append(" business day");
                if (TaskDueDateCalcDays > 1 || TaskDueDateCalcDays < -1)
                {
                    sb.Append("s");
                }

                if (TaskDueDateCalcDays > 0)
                {
                    sb.Append(" after the ");
                }
                else
                {
                    sb.Append(" prior to the ");
                }
            }
            ParameterReporting parameterList = null;
            HttpContext context = HttpContext.Current;
            string key = "ParamReporting_1239";

            if (null != context)
            {
                parameterList = context.Items[key] as ParameterReporting;
            }

            if( null == parameterList) {
                parameterList = ParameterReporting.RetrieveSystemParameters();
                if( null != context )
                {
                    context.Items.Add(key, parameterList);
                }
            }
            //this may not be a very good idea...
            string field;
            try
            {
                field = "[" + parameterList[TaskDueDateCalcField].FriendlyName + "]";
            }
            catch (NullReferenceException)
            {
                field = "[" + TaskDueDateCalcField + "]";
            }
            if (sanitize)
            {
                field = TaskUtilities.SanitizeTaskText(field, PrincipalFactory.CurrentPrincipal);
            }
            sb.Append(field);
            return sb.ToString();
        }

        #region Prepare Loan for Resubmission methods
        public static string Get_sReasonNotAllowingResubmission_Action(E_sReasonNotAllowingResubmission reason)
        {
            switch (reason)
            {
                case E_sReasonNotAllowingResubmission.NotAStandAloneSecondLien:
                    return "Switch current loan to first lien position.";
                case E_sReasonNotAllowingResubmission.LoanLinked:
                    return "Delete linked loan.";
                case E_sReasonNotAllowingResubmission.LoanStatusNotOpen:
                    return "Change loan status to \"Open\".";
                case E_sReasonNotAllowingResubmission.RateStillLocked:
                    return JsMessages.BreakRateLock;
                case E_sReasonNotAllowingResubmission.LoanIsTemplate:
                    return "Cannot run pricing on template.";
                case E_sReasonNotAllowingResubmission.HasRateLockRequest:
                    return "Clear rate lock request and change loan status to \"Open\".";
                case E_sReasonNotAllowingResubmission.HasRegisteredLoanProgram:
                    return "Clear registered loan program.";
                default:
                    throw new UnhandledEnumException(reason);

            }
        }
        public static string Get_sReasonNotAllowingResubmission_Description(E_sReasonNotAllowingResubmission reason)
        {
            switch (reason)
            {
                case E_sReasonNotAllowingResubmission.NotAStandAloneSecondLien:
                    return "Current loan is not a stand alone 2nd.";
                case E_sReasonNotAllowingResubmission.LoanLinked:
                    return "There is a linked loan.";
                case E_sReasonNotAllowingResubmission.LoanStatusNotOpen:
                    return "Current loan is not in \"Open\" status.";
                case E_sReasonNotAllowingResubmission.RateStillLocked:
                    return "Rate is currently locked.";
                case E_sReasonNotAllowingResubmission.LoanIsTemplate:
                    return "Current file is a loan template.";
                case E_sReasonNotAllowingResubmission.HasRateLockRequest:
                    return "Current loan has a rate lock request pending.";
                case E_sReasonNotAllowingResubmission.HasRegisteredLoanProgram:
                    return "Current loan has registered loan program.";
                default:
                    throw new UnhandledEnumException(reason);

            }
        }
        public static void PrepareLoanForResubmission(Guid sLId)
        {
            CPageData dataLoan = new CReasonForNotAllowingResubmissionData(sLId);
            dataLoan.InitLoad();

            ArrayList reasons = dataLoan.sReasonForNotAllowingResubmission;
            bool bFixLienPosNotFirst = false;
            bool bFixLoanStatusNotOpen = false;


            foreach (E_sReasonNotAllowingResubmission reason in reasons)
            {
                switch (reason)
                {
                    case E_sReasonNotAllowingResubmission.NotAStandAloneSecondLien:
                        bFixLienPosNotFirst = true;
                        break;
                    case E_sReasonNotAllowingResubmission.LoanLinked:
                        PrepareLoan_LoanLinked(dataLoan);
                        break;
                    case E_sReasonNotAllowingResubmission.LoanStatusNotOpen:
                        bFixLoanStatusNotOpen = true;
                        break;
                    case E_sReasonNotAllowingResubmission.RateStillLocked:
                        PrepareLoan_RateStillLocked(sLId);
                        break;
                    case E_sReasonNotAllowingResubmission.LoanIsTemplate:
                        Tools.LogBug("Cannot prepare for resubmission on template.");
                        break;
                    case E_sReasonNotAllowingResubmission.HasRateLockRequest:
                        Prepare_ClearLockRequest(sLId);
                        bFixLoanStatusNotOpen = true;
                        break;
                    case E_sReasonNotAllowingResubmission.HasRegisteredLoanProgram:
                        // 4/8/2011 dd - Clear register loan program will happen in PrepareLoan_LienPosNotFirst_LoanStatusNotOpen
                        break;
                    default:
                        throw new UnhandledEnumException(reason);

                }
            }
            PrepareLoan_LienPosNotFirst_LoanStatusNotOpen(sLId, bFixLienPosNotFirst, bFixLoanStatusNotOpen);
        }

        /// <summary>
        /// 01/07/05 - Current method will unlink the loan and DELETE linked loan.
        /// </summary>
        /// <param name="sLId"></param>
        private static void PrepareLoan_LoanLinked(CPageData dataLoan)
        {
            // 6/29/2005 kb - We now (case 2218) secure delete requests, so
            // get the calling user's credentials and check.

            AbstractUserPrincipal userP = PrincipalFactory.CurrentPrincipal;

            if (userP != null)
            {
                // Get Linked Loan ID
                // Delete Linked Loan

                if (dataLoan.sLinkedLoanInfo.IsLoanLinked)
                {
                    try
                    {
                        DeclareLoanFileInvalid(userP, dataLoan.sLinkedLoanInfo.LinkedLId, true, true);
                    }
                    catch (PageDataAccessDenied exc)
                    {
                        throw new CBaseException(ErrorMessages.InsufficientPermissionToDeleteLinkedLoan, exc);
                    }
                }

            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, "Tools.PrepareLoan_LoanLinked( sLId ) need to have IUserPrincipal.");
            }
        }
        private static void PrepareLoan_LienPosNotFirst_LoanStatusNotOpen(Guid sLId, bool bFixLienPosNotFirst, bool bFixLoanStatusNotOpen)
        {
            CPageData dataLoan = new CPrepareLoanForResubmissionData(sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            if (dataLoan.sLpTemplateId != Guid.Empty)
            {
                dataLoan.sLpTemplateId = Guid.Empty;
            }
            if (!string.IsNullOrEmpty(dataLoan.sLpTemplateNm))
            {
                dataLoan.sLpTemplateNm = "";
            }
            if(!string.IsNullOrEmpty(dataLoan.sLpTemplateNmSubmitted))
            {
                dataLoan.sLpTemplateNmSubmitted = "";
            }
            if (bFixLienPosNotFirst)
                dataLoan.sLienPosT = E_sLienPosT.First;

            if (bFixLoanStatusNotOpen)
            {
                dataLoan.sStatusLckd = true;
                dataLoan.sStatusT = E_sStatusT.Loan_Open;
            }
            dataLoan.Save();
        }
        private static void Prepare_ClearLockRequest(Guid sLId)
        {
            CPageData dataLoan = new CPerformBreakRateLockData(sLId);

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.RemoveRequestedRate();
            dataLoan.Save();

        }
        private static void PrepareLoan_RateStillLocked(Guid sLId)
        {
            BrokerUserPrincipal currentUserPrincipal = BrokerUserPrincipal.CurrentPrincipal;

            if (null != currentUserPrincipal)
            {
                CPageData dataLoan = new CPerformBreakRateLockData(sLId);

                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                dataLoan.BreakRateLock(currentUserPrincipal.DisplayName, "Prepare loan for running PriceMyLoan.");
                dataLoan.Save();
            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, "Tools.PrepareLoan_RateStillLocked(sLId) need to have BrokerUserPrincipal.");
            }
        }

        #endregion

        /// <summary>
        /// Returns timezone + DT or ST depending on the timezone of the server and the passed in timezone character.
        /// </summary>
        /// <param name="dt">The date involved.</param>
        /// <param name="prefixZone">The default is Pacific Standard/Daylight Time.  </param>
        /// <returns>A value representing the timezone for the date.</returns>
        static public string GetTimeZonePart(DateTime dt, TimeZoneInfo timeZoneInfo)
        {
            if(timeZoneInfo == null)
            {
                timeZoneInfo = PacificStandardVariantOffset;
            }

            var endPart = timeZoneInfo.IsDaylightSavingTime(dt) ? "DT" : "ST";
            var result = GetTimezonePrefixByTimezone(timeZoneInfo) + endPart;

            return result;
        }

        /// <summary>
        /// Gets the date formatted  with the specified format in the timezone specified, with the appropriate abbreviation appended e.g. PST/PDT.
        /// </summary>
        /// <param name="dtUtc">The utc date we are getting the string for.</param>
        /// <param name="timeZoneOfViewer">The timezone info of the person viewing the date.</param>
        /// <param name="dateFormat">The format we'd like the non-time-zone part to appear as.</param>
        /// <returns>A string form of the date in the specified timezone with the specified format.</returns>
        static public string GetUtcDateTimeAsStringWithTimeZone(DateTime dtUtc, TimeZoneInfo timeZoneOfViewer, string dateFormat)
        {
            if (null == timeZoneOfViewer)
            {
                throw new ArgumentNullException(nameof(timeZoneOfViewer));
            }

            var dtInTzOfViewer = Tools.GetZonedTimeFromUtcTimeAndViewerTimeZone(dtUtc, timeZoneOfViewer);
            var dateString = dtInTzOfViewer.ToString(dateFormat) + " " + Tools.GetTimeZonePart(dtUtc, timeZoneOfViewer);
            return dateString;
        }

        /// <summary>
        /// Gets the appropriate first letter for the given time zone info.  E.g. 'P' for Pacific Standard / Daylight Time.
        /// </summary>
        /// <param name="timeZoneInfo">The timezone info of interest.</param>
        /// <returns>The first character of the abbreviated form of the timezone info.</returns>
        static public char GetTimezonePrefixByTimezone(TimeZoneInfo timeZoneInfo)
        {
            if(timeZoneInfo == null)
            {
                throw new ArgumentNullException(nameof(timeZoneInfo));
            }

            switch (timeZoneInfo.Id)
            {
                case "Pacific Standard Time":
                    return 'P';
                case "Eastern Standard Time":
                    return 'E';
                default:
                    throw new UnhandledCaseException(timeZoneInfo.DisplayName);
            }
        }

        /// <summary>
        /// Gets the UTC date in the timezone of the current date (accounts for daylight / standard) with the provided time zone. ,par/>
        /// Current date is passed in so that when run in a loop, current date is consistent and not reacquired. <para/>
        /// The resulting object has kind unspecified, as local is misleading.
        /// </summary>
        /// <param name="currentDate">A snapshot of a more or less current date.</param>
        /// <param name="currentTimeZone">The current time zone of interest.</param>
        /// <param name="utcDateOfInterest">The utc date the viewer to is viewing.</param>
        /// <returns>The time in a timezone corresponding to the current time (accounts for daylight).</returns>
        static public DateTime GetZonedTimeFromUtcTimeAndCurrentTime(DateTime currentDate, TimeZoneInfo currentTimeZone, DateTime utcDateOfInterest)
        {
            var targetZone = currentTimeZone.IsDaylightSavingTime(currentDate) ? PacificDaylightFixedOffset : PacificStandardFixedOffset;
            var resultDt = GetZonedTimeFromUtcTimeAndViewerTimeZone(utcDateOfInterest, targetZone);
            return resultDt;
        }

        /// <summary>
        /// Gets the utc date into the timezone provided. <para/>
        /// The resulting object has kind unspecified, as local is misleading.
        /// </summary>
        /// <param name="utcDateOfInterest">The utc date the viewer to is viewing.</param>
        /// <param name="currentTimeZone">The current time zone of interest.</param>
        /// <returns>The time in a timezone specified.</returns>
        static public DateTime GetZonedTimeFromUtcTimeAndViewerTimeZone(DateTime utcDateOfInterest, TimeZoneInfo currentTimeZone)
        {
            if (utcDateOfInterest.Kind != DateTimeKind.Utc)
            {
                throw new InvalidOperationException("utcDateOfInterest was not of kind utc, so cannot convert from it.");
            }

            var resultDt = TimeZoneInfo.ConvertTimeFromUtc(utcDateOfInterest, currentTimeZone);
            return resultDt;
        }

        static public string GetDateTimeNowString()
        {
            return GetDateTimeDescription(DateTime.Now);
        }

        static public string GetDateTimeDescription(DateTime dt)
        {
            return dt.ToString() + " " + GetTimeZonePart(dt, PacificStandardVariantOffset);
        }

        static public string GetDateTimeDescription(CDateTime dt)
        {
            return (dt == CDateTime.InvalidWrapValue) ? string.Empty
                : GetDateTimeDescription(dt.DateTimeForComputationWithTime);
        }

        static public string GetDateTimeDescription(DateTime dt, string format)
        {
            return dt.ToString(format) + " " + GetTimeZonePart(dt, PacificStandardVariantOffset);
        }

        static public string GetTimeDescription(DateTime dt)
        {
            return dt.ToString("hh:mm tt") + " " + GetTimeZonePart(dt, PacificStandardVariantOffset);
        }

        static public string GetDateTimeDescriptionFromExactString(string dts)
        {
            try
            {
                var dt = DateTime.Parse(dts);
                return dts.Replace("PST", "").Replace("PDT", "") + " " + GetTimeZonePart(dt, PacificStandardVariantOffset);
            }
            catch (FormatException)
            {
                return dts;
            }
        }

        static public string GetEasternTimeDescriptionFromExactString(string dts)
        {
            try
            {
                // 10/24/2007 dd - This method will append either EDT or EST at the end. The time must be in eastern time. This method is used to
                // interpret timezone for time return from FNMA connection. Notice how I use the CurrentTimeZone (Pacific) to determine if
                // time is daylight saving or not. At the moment, .NET does not allow you to switch to different timezone.
                var dt = DateTime.Parse(dts);
                return dts + " " + GetTimeZonePart(dt, EasternStandardVariantOffset);
            }
            catch
            {
                return dts;
            }
        }

        public static string GetEasternTimeDescription(DateTime dt)
        {
            return dt.ToString("G") + (dt.IsDaylightSavingTime() ? " EDT" : " EST");
        }

        static public int NumberOfMonthsBetween(DateTime startD, DateTime endD, int maxMonths)
        {
            if (0 == maxMonths)
                return 0;

            if (startD.CompareTo(endD) > 0)
            {
                Tools.Assert(false,
                    string.Format("Expecting startD={0} to be smaller than endD={1}",
                    startD.ToString(), endD.ToString()));

                return 0;
            }

            for (int i = 1; i <= maxMonths; ++i)
            {
                DateTime newD = startD.AddMonths(i);
                if (newD.CompareTo(endD) > 0)
                    return i - 1;
            }
            return maxMonths;
        }

        public static void Assert(bool bAssert, string msg)
        {
            if (!bAssert)
                Tools.LogBug("Assert:" + msg);
        }

        public static void AssertOrFail(bool bAssert, string usrMessage, string devMessage)
        {
            if (!bAssert)
            {
                throw new CBaseException(usrMessage, devMessage);
            }
        }

        public static bool IsStatusLead(E_sStatusT sStatusT)
        {
            switch (sStatusT)
            {
                case E_sStatusT.Lead_Canceled:
                case E_sStatusT.Lead_Declined:
                case E_sStatusT.Lead_New:
                case E_sStatusT.Lead_Other:
                    return true;
                case E_sStatusT.Loan_Open:
                case E_sStatusT.Loan_Prequal:
                case E_sStatusT.Loan_Preapproval:
                case E_sStatusT.Loan_Registered:
                case E_sStatusT.Loan_Approved:
                case E_sStatusT.Loan_Docs:
                case E_sStatusT.Loan_Funded:
                case E_sStatusT.Loan_OnHold:
                case E_sStatusT.Loan_Suspended:
                case E_sStatusT.Loan_Canceled:
                case E_sStatusT.Loan_Rejected:
                case E_sStatusT.Loan_Closed:
                case E_sStatusT.Loan_LoanSubmitted:
                case E_sStatusT.Loan_Underwriting:
                case E_sStatusT.Loan_WebConsumer:
                case E_sStatusT.Loan_Recorded:
                case E_sStatusT.Loan_Other:
                case E_sStatusT.Loan_Shipped:
                case E_sStatusT.Loan_ClearToClose:
                case E_sStatusT.Loan_FinalDocs:
                case E_sStatusT.Loan_FinalUnderwriting:
                case E_sStatusT.Loan_DocsBack:
                case E_sStatusT.Loan_Processing:
                case E_sStatusT.Loan_FundingConditions:
                case E_sStatusT.Loan_LoanPurchased:
                case E_sStatusT.Loan_PreProcessing:
                case E_sStatusT.Loan_DocumentCheck:
                case E_sStatusT.Loan_DocumentCheckFailed:
                case E_sStatusT.Loan_PreUnderwriting:
                case E_sStatusT.Loan_ConditionReview:
                case E_sStatusT.Loan_PreDocQC:
                case E_sStatusT.Loan_DocsOrdered:
                case E_sStatusT.Loan_DocsDrawn:
                case E_sStatusT.Loan_InvestorConditions:       // this is "promoting the date to a status". tied to sSuspendedByInvestorD
                case E_sStatusT.Loan_InvestorConditionsSent:   // this is "promoting the date to a status". tied to sCondSentToInvestorD
                case E_sStatusT.Loan_ReadyForSale:
                case E_sStatusT.Loan_SubmittedForPurchaseReview:
                case E_sStatusT.Loan_InPurchaseReview:
                case E_sStatusT.Loan_PrePurchaseConditions:
                case E_sStatusT.Loan_SubmittedForFinalPurchaseReview:
                case E_sStatusT.Loan_InFinalPurchaseReview:
                case E_sStatusT.Loan_ClearToPurchase:
                case E_sStatusT.Loan_Purchased:
                case E_sStatusT.Loan_CounterOffer:
                case E_sStatusT.Loan_Withdrawn:
                case E_sStatusT.Loan_Archived:
                    return false;
                default:
                    throw new UnhandledEnumException(sStatusT);
            }
        }

        public static bool IsInactiveStatus(E_sStatusT sStatusT)
        {
            switch (sStatusT)
            {
                case E_sStatusT.Loan_CounterOffer:
                case E_sStatusT.Loan_Withdrawn:
                case E_sStatusT.Loan_Archived:
                case E_sStatusT.Loan_Suspended:
                case E_sStatusT.Loan_Canceled:
                case E_sStatusT.Loan_Rejected:
                case E_sStatusT.Loan_OnHold:
                    return true;
                case E_sStatusT.Lead_Canceled:
                case E_sStatusT.Lead_Declined:
                case E_sStatusT.Lead_New:
                case E_sStatusT.Lead_Other:
                case E_sStatusT.Loan_Open:
                case E_sStatusT.Loan_Prequal:
                case E_sStatusT.Loan_Preapproval:
                case E_sStatusT.Loan_Registered:
                case E_sStatusT.Loan_Approved:
                case E_sStatusT.Loan_Docs:
                case E_sStatusT.Loan_Funded:
                case E_sStatusT.Loan_Closed:
                case E_sStatusT.Loan_LoanSubmitted:
                case E_sStatusT.Loan_Underwriting:
                case E_sStatusT.Loan_WebConsumer:
                case E_sStatusT.Loan_Recorded:
                case E_sStatusT.Loan_Other:
                case E_sStatusT.Loan_Shipped:
                case E_sStatusT.Loan_ClearToClose:
                case E_sStatusT.Loan_FinalDocs:
                case E_sStatusT.Loan_FinalUnderwriting:
                case E_sStatusT.Loan_DocsBack:
                case E_sStatusT.Loan_Processing:
                case E_sStatusT.Loan_FundingConditions:
                case E_sStatusT.Loan_LoanPurchased:
                case E_sStatusT.Loan_PreProcessing:
                case E_sStatusT.Loan_DocumentCheck:
                case E_sStatusT.Loan_DocumentCheckFailed:
                case E_sStatusT.Loan_PreUnderwriting:
                case E_sStatusT.Loan_ConditionReview:
                case E_sStatusT.Loan_PreDocQC:
                case E_sStatusT.Loan_DocsOrdered:
                case E_sStatusT.Loan_DocsDrawn:
                case E_sStatusT.Loan_InvestorConditions:       // this is "promoting the date to a status". tied to sSuspendedByInvestorD
                case E_sStatusT.Loan_InvestorConditionsSent:   // this is "promoting the date to a status". tied to sCondSentToInvestorD
                case E_sStatusT.Loan_ReadyForSale:
                case E_sStatusT.Loan_SubmittedForPurchaseReview:
                case E_sStatusT.Loan_InPurchaseReview:
                case E_sStatusT.Loan_PrePurchaseConditions:
                case E_sStatusT.Loan_SubmittedForFinalPurchaseReview:
                case E_sStatusT.Loan_InFinalPurchaseReview:
                case E_sStatusT.Loan_ClearToPurchase:
                case E_sStatusT.Loan_Purchased:
                    return false;
                default:
                    throw new UnhandledEnumException(sStatusT);
            }
        }

        public static bool IsDefunctStatus(E_sStatusT sStatusT)
        {
            switch (sStatusT)
            {
                case E_sStatusT.Loan_Other:
                    return true;
                case E_sStatusT.Lead_Canceled:
                case E_sStatusT.Lead_Declined:
                case E_sStatusT.Lead_New:
                case E_sStatusT.Lead_Other:
                case E_sStatusT.Loan_Open:
                case E_sStatusT.Loan_Prequal:
                case E_sStatusT.Loan_Preapproval:
                case E_sStatusT.Loan_Registered:
                case E_sStatusT.Loan_Approved:
                case E_sStatusT.Loan_Docs:
                case E_sStatusT.Loan_Funded:
                case E_sStatusT.Loan_OnHold:
                case E_sStatusT.Loan_Suspended:
                case E_sStatusT.Loan_Canceled:
                case E_sStatusT.Loan_Rejected:
                case E_sStatusT.Loan_Closed:
                case E_sStatusT.Loan_LoanSubmitted:
                case E_sStatusT.Loan_Underwriting:
                case E_sStatusT.Loan_WebConsumer:
                case E_sStatusT.Loan_Recorded:
                case E_sStatusT.Loan_Shipped:
                case E_sStatusT.Loan_ClearToClose:
                case E_sStatusT.Loan_FinalDocs:
                case E_sStatusT.Loan_FinalUnderwriting:
                case E_sStatusT.Loan_DocsBack:
                case E_sStatusT.Loan_Processing:
                case E_sStatusT.Loan_FundingConditions:
                case E_sStatusT.Loan_LoanPurchased:
                case E_sStatusT.Loan_PreProcessing:
                case E_sStatusT.Loan_DocumentCheck:
                case E_sStatusT.Loan_DocumentCheckFailed:
                case E_sStatusT.Loan_PreUnderwriting:
                case E_sStatusT.Loan_ConditionReview:
                case E_sStatusT.Loan_PreDocQC:
                case E_sStatusT.Loan_DocsOrdered:
                case E_sStatusT.Loan_DocsDrawn:
                case E_sStatusT.Loan_InvestorConditions:       // this is "promoting the date to a status". tied to sSuspendedByInvestorD
                case E_sStatusT.Loan_InvestorConditionsSent:   // this is "promoting the date to a status". tied to sCondSentToInvestorD
                case E_sStatusT.Loan_ReadyForSale:
                case E_sStatusT.Loan_SubmittedForPurchaseReview:
                case E_sStatusT.Loan_InPurchaseReview:
                case E_sStatusT.Loan_PrePurchaseConditions:
                case E_sStatusT.Loan_SubmittedForFinalPurchaseReview:
                case E_sStatusT.Loan_InFinalPurchaseReview:
                case E_sStatusT.Loan_ClearToPurchase:
                case E_sStatusT.Loan_Purchased:
                case E_sStatusT.Loan_CounterOffer:
                case E_sStatusT.Loan_Withdrawn:
                case E_sStatusT.Loan_Archived:
                    return false;
                default:
                        throw new UnhandledEnumException(sStatusT);
            }
        }

        /// <summary>
        /// Billable if the status is between Funded and Loan Sold inclusive, and status is not Loan Closed. opm 172868, 190747.
        /// </summary>
        public static bool IsBillableStatus(E_sStatusT sStatusT)
        {
            return E_sStatusT.Loan_Closed != sStatusT &&  // sk opm 190747
                MathUtilities.IsInclusivelyBetween(CPageBase.s_OrderByStatusT[sStatusT],
                CPageBase.s_OrderByStatusT[E_sStatusT.Loan_Funded],
                CPageBase.s_OrderByStatusT[E_sStatusT.Loan_LoanPurchased]);
        }

        /// <summary>
        /// Determine if the loan is a HELOC loan
        /// </summary>
        /// <param name="principal"></param>
        /// <param name="sLId"></param>
        /// <returns></returns>
        public static bool IsLoanAHELOC(AbstractUserPrincipal principal, Guid sLId)
        {
            // 10/21/2013 dd - A HELOC loan must sastify the following criterias
            //   1) Broker must have EnableHELOC feature
            //   2) sIsLineOfCredit is true
            Guid brokerId = Guid.Empty;
            DbConnectionInfo connInfo = null;
            BrokerDB broker = null;

            // 1/21/2015 dd - OPM 201419 - For performance reason, we are retrieving sIsLineOfCredit directly from DB.
            if (principal == null || principal.BrokerId == Guid.Empty)
            {
                // 10/26/2017 gf - Retrieve broker id from loan id when we don't have a
                // principal. This can happen for background processes.
                // 10/25/2013 dd - A process is run under System Principal.
                // Need to get BrokerId from loan directly.
                connInfo = DbConnectionInfo.GetConnectionInfoByLoanId(sLId, out brokerId);
                broker = BrokerDB.RetrieveById(brokerId);
            }
            else
            {
                brokerId = principal.BrokerId;
                connInfo = principal.ConnectionInfo;
                broker = principal.BrokerDB;
            }

            if (broker.IsEnableHELOC == false)
            {
                return false;
            }

            // 1/22/2015 dd - Currently this SQL get call twice per request. However I cannot cache per request since
            // there is a small change that sIsLineOfCredit can change during same request.
            SqlParameter[] parameters = {
                                            new SqlParameter("@sLId", sLId)
                                        };
            string sql = "SELECT sIsLineOfCredit FROM LOAN_FILE_CACHE_3 WHERE sLId=@sLId";

            bool? isLineOfCredit = null;
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                if (reader.Read())
                {
                    var o = reader["sIsLineOfCredit"];
                    if (o != DBNull.Value)
                    {
                        isLineOfCredit = (bool)o;
                    }
                }
            };

            DBSelectUtility.ProcessDBData(connInfo, sql, null, parameters, readHandler);
            if (isLineOfCredit != null) return isLineOfCredit.Value;

            return false;

        }
        /// <summary>
        /// Inclusive
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="nMonths"></param>
        /// <returns></returns>
        public static bool IsDateWithin(DateTime dt, int nMonths)
        {
            return IsDateWithin(dt, nMonths, DateTime.Now);
        }
        /// <summary>
        /// This method takes an object that has Implemented ISerializable and serializes it to binary then it
        /// turns it into a base 64 string. It then stores the string into the AutoExpiredTextCache for the given amount of time.
        /// </summary>
        /// <param name="obj">object to be serialized and store (not null) </param>
        /// <param name="life">the amount of time the object will last in the cache (+ a few minutes) </param>
        /// <exception cref="SerializationException"> throws when an error has occured in the serialization </exception>
        /// <returns>the key used to retrieve the item from the cache.</returns>
        public static string SerializeAndCacheFor(ISerializable obj, TimeSpan life)
        {
            byte[] bytes;
            BinaryFormatter bf = new BinaryFormatter();
            string encodedData;
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                bytes = ms.GetBuffer();
                //base 64  stuff might add some extra zeroes at the end. It uses A-Za-z so its ok to add : and digits
            }
            encodedData = bytes.Length + ":" + Convert.ToBase64String(bytes, 0, bytes.Length);
            return AutoExpiredTextCache.AddToCache(encodedData, life);

        }

        /// <summary>
        /// Fetches the serialized object from the cache if it exist. If the object doesn't exist method returns null.
        /// If the key belongs to an object that is not Serializable ( wasnt added to cache via SerializeAndCache ) it
        /// returns null. Throws an exception if it cant be deserialized.
        /// </summary>
        /// <param name="key">The key of the object returned by SerializeAndCacheFor</param>
        /// <param name="expireCacheNow">if true marks the item in the cache as expired</param>
        ///
        /// <returns>the object</returns>
        public static object DeserializeFromCache(string key, bool expireCacheNow)
        {
            if (key == null)
                throw new NullReferenceException("key is null");

            string data = AutoExpiredTextCache.GetFromCache(key);

            if (data == null)
            {
                return null;
            }

            int p = data.IndexOf(":");

            if (p == -1) //its not something we should deserialize since the length is missing!
            {
                return null;
            }

            int length = 0;
            try
            {
                length = Convert.ToInt32(data.Substring(0, p));
            }
            catch (FormatException)
            {
                return null;
            }

            byte[] memoryData = Convert.FromBase64String(data.Substring(p + 1));
            BinaryFormatter sf = new BinaryFormatter();
            object o;
            using (MemoryStream rs = new MemoryStream(memoryData, 0, length))
            {
                o = sf.Deserialize(rs);
            }
            if (expireCacheNow)
            {
                AutoExpiredTextCache.ExpireCache(key);
            }
            return o;
        }
        /// <summary>
        /// Inclusive
        /// </summary>
        /// <param name="dt1"></param>
        /// <param name="nMonths"></param>
        /// <param name="dt2"></param>
        /// <returns></returns>
        public static bool IsDateDiffWithin(DateTime dt1, int nMonths, DateTime dt2)
        {
            DateTime olderD, laterD;
            if (dt1.CompareTo(dt2) >= 0)
            {
                laterD = dt1;
                olderD = dt2;
            }
            else
            {
                laterD = dt2;
                olderD = dt1;
            }

            return IsDateWithin(olderD, nMonths, laterD);
        }

        /// <summary>
        /// Inclusive
        /// </summary>
        /// <param name="olderDate"></param>
        /// <param name="nMonths"></param>
        /// <param name="laterDate"></param>
        /// <returns></returns>
        public static bool IsDateWithin(DateTime olderDate, int nMonths, DateTime laterDate)
        {
            return olderDate.AddMonths(nMonths).CompareTo(laterDate) >= 0;
        }

        /// <summary>
        /// Throw if both are invalid, DateTime.MinValue and DateTime.MaxValue are both considered to be invalid.
        /// If one is invalid and the other is invalid, it returns the valid one. If both valid, it returns the later one.
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public static DateTime LatestValidDate(DateTime t1, DateTime t2)
        {
            bool isT1Valid = t1 != DateTime.MinValue && t1 != DateTime.MaxValue;
            bool isT2Valid = t2 != DateTime.MinValue && t2 != DateTime.MaxValue;

            if (!isT1Valid && !isT2Valid)
                throw new CBaseException(ErrorMessages.Generic, "Tools.MinValidDate encounters both parameters to be invalid");

            if (!isT1Valid)
                return t2;

            if (!isT2Valid)
                return t1;

            if (t1.CompareTo(t2) >= 0)
                return t1;
            return t2;
        }

        /// <summary>
        /// Return the Request.ApplicationPath with no trailing forward slash.
        /// </summary>
        public static string VRoot
        {
            get
            {
                if (HttpContext.Current != null)
                {
                    if ("/" == HttpContext.Current.Request.ApplicationPath)
                        return "";
                    else
                        return HttpContext.Current.Request.ApplicationPath;
                }
                else
                {
                    return "";
                }
            }
        }

        /// <summary>
        /// Returns a link prefix for display full size PNG of eDocs page.
        /// </summary>
        /// <returns>A link prefix for display full size PNG of eDocs page.</returns>
        public static string GetEDocsPngLink()
        {
            return GetEDocsPngLinkImpl(false);
        }

        /// <summary>
        /// Returns a link prefix for display thumb size PNG of eDocs page.
        /// </summary>
        /// <returns>A link prefix for display thumb size PNG of eDocs page.</returns>
        public static string GetEDocsThumbPngLink()
        {
            return GetEDocsPngLinkImpl(true);
        }

        private static string GetEDocsPngLinkImpl(bool isThumb)
        {
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;

            bool isAmazon = false;

            if (principal != null && principal.BrokerDB != null)
            {
                isAmazon = principal.BrokerDB.IsTestingAmazonEdocs;
            }

            if (isAmazon)
            {
                float scale = ConstAppDavid.PdfPngScaling;

                if (isThumb)
                {
                    scale = ConstAppDavid.PdfPngThumbRequeueScaling;
                }

                return ConstStage.AmazonEdocsClientUrl + "/edocpng.aspx?scale=" + scale;
            }
            else
            {
                // LendingQB host solution.
                string query = "mode=png&rand=" + Guid.NewGuid();

                if (isThumb)
                {
                    query += "&thumb=t";
                }

                return GetEDocsLink(VirtualPathUtility.ToAbsolute("~/edocpng.aspx?" + query));
            }
        }

        /// <summary>
        /// Returns a link to be used for EDocs uploads and downloads.
        /// This will use the EDocs subdomain (ConstSite.EDocsSite) if it exists and if login cookie sharing is enabled.
        /// </summary>
        /// <param name="rawUrl">The full root relative URL including querystring and extra path as a string. </param>
        public static string GetEDocsLink(string rawUrl)
        {
            return GetLink(ConstSite.EDocsSite, rawUrl);
        }
        /// <summary>
        /// This is intended to be used from a subdomain only in order to link back to LO.
        /// Returns a link to LO, using ConstSite.LOSite if it exists and if login cookie sharing is enabled.
        /// </summary>
        /// <param name="rawUrl">The full root relative URL including querystring and extra path as a string.</param>
        /// <returns></returns>
        public static string GetLOLink(string rawUrl)
        {
            return GetLink(ConstSite.LOSite, rawUrl);
        }

        private static string GetLink(string site, string rawUrl)
        {
            if (!string.IsNullOrEmpty(site) && ConstSite.ShareLoginCookieWithSubdomain)
            {
                if (!string.IsNullOrEmpty(rawUrl) && rawUrl[0] != '/')
                {
                    rawUrl = "/" + rawUrl;
                }
                return site + rawUrl;
            }
            return rawUrl;
        }

        public static string GetCurrentStackTrace()
        {
            System.Diagnostics.StackTrace stack = new System.Diagnostics.StackTrace();
            return stack.ToString();
        }
        /// <summary>
        /// Check to see if login name already exists.
        /// </summary>
        /// <param name="loginName"></param>
        /// <returns>true if this login is unique, false otherwise</returns>
        public static bool IsLoginUnique(string loginName, Guid excludeUserId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@LoginNm", loginName),
                                            new SqlParameter("@ExcludeUserId", excludeUserId)
                                        };

            int count = 0;

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "ALL_USER_SHARED_DB_CheckUniqueness", parameters))
            {
                if (reader.Read())
                {
                    count = (int)reader["Hits"];

                }
            }

            return count == 0;
        }
        public static bool IsPmlLoginUnique(BrokerDB brokerDB, string loginName, Guid excludeUserId)
        {
            bool isUnique = false;

            try
            {
                Guid brokerPmlSiteId = brokerDB.PmlSiteID;
                SqlParameter[] parameters = {
                                                new SqlParameter("@LoginName", loginName),
                                                new SqlParameter("@BrokerPmlSiteId", brokerPmlSiteId),
                                                new SqlParameter("@ExcludeUserId", excludeUserId)
                                            };
                int count = (int)StoredProcedureHelper.ExecuteScalar(brokerDB.BrokerID, "CheckLoginUniqueness", parameters);

                if (count == 0)
                {
                    isUnique = true;
                }
            }
            catch (SqlException)
            {
            }
            return isUnique;
        }

        /// <summary>
        /// Overloaded this method to take a cache table update RequestId in the case that this
        /// update request is coming from the result of a user updating a loan file.  The cache update
        /// request is saved in the LOAN_CACHE_UPDATE_REQUEST table first as part of the loan file
        /// update transaction, so that if something happens to the server before the cache file request
        /// can take place, it will not get lost, and can be re-issued.  Once the request successfully
        /// runs, it is removed from the table, unless it produces an exception, which will get saved
        /// into the table so that the request can be viewed, fixed, and re-run easily. -DB 9/15/06
        ///
        /// </summary>
        /// <param name="loanId"></param>
        /// <param name="dirtyFields">pass null to force everything to update</param>
        /// <param name="requestId" > The cache table update request ID for lookup in LOAN_CACHE_UPDATE_REQUEST table</param>
        public static void UpdateCacheTable(DbConnectionInfo connInfo, Guid loanId, ICollection dirtyFields, Guid requestId, bool rerunningCacheUpdateRequest)
        {
            using (PerformanceMonitorItem.Time("Tools.UpdateCacheTable"))
            {
                try
                {
                    DataAccess.LoanFileCacheData obj = (null == dirtyFields) ? new LoanFileCacheData(loanId, requestId, rerunningCacheUpdateRequest) : new LoanFileCacheData(loanId, dirtyFields, requestId, rerunningCacheUpdateRequest);
                    obj.UpdateCache();

                    // On success, remove entry in LOAN_CACHE_UPDATE_REQUEST table.
                    LoanCacheUpdateRequest.Delete(requestId, connInfo);
                }
                catch (Exception e)
                {
                    // Note: It is possible for this to be a LoanNotFoundException.
                    // This can happen due to inconsistencies between loan file tables in the database.
                    // For example, if something strange happened in file creation that left behind
                    // a loan that was valid but did not have a row in all of the loan-related 
                    // tables this could happen. We will not give those records special handling 
                    // here, we will just update the error message regarding the failure.
                    Tools.LogErrorWithCriticalTracking(string.Format("Failed to update cached data of loan file with id {0}. Need to manually update it now.", loanId.ToString()), e);

                    if (!(requestId.Equals(Guid.Empty)))
                    {
                        AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
                        string loginNm = principal == null ? "system" : principal.LoginNm;

                        LoanCacheUpdateRequest.Update(requestId, connInfo, e.ToString(), loginNm);
                    }
                }
            }
        }

        /// <summary>
        /// This method won't throw to ensure smooth saving operations around the calls to this method.
        /// </summary>
        /// <param name="loanId"></param>
        /// <param name="dirtyFields">pass null to force update everything</param>
        /// <param name="sendEmailOnFailure">True if the method should email if the update fails, false otherwise.</param>
        /// <returns>true if successful, false otherwise.</returns> OPM 185732
        public static bool UpdateCacheTable(Guid loanId, ICollection dirtyFields, bool sendEmailOnFailure = true)
        {
            using (PerformanceMonitorItem.Time("Tools.UpdateCacheTable"))
            {
                Exception firstExc = null;
                for (int i = 0; i < 5; ++i)
                {
                    try
                    {
                        DataAccess.LoanFileCacheData obj = (null == dirtyFields) ? new LoanFileCacheData(loanId) : new LoanFileCacheData(loanId, dirtyFields);
                        obj.UpdateCache();
                        return true;
                    }
                    catch (Exception exc)
                    {
                        if (firstExc == null)
                        {
                            firstExc = exc;
                        }

                        // Skip retrying when these exceptions are caught since
                        // subsequent attempts will continue to encounter these 
                        // errors.
                        if (exc is OverflowException || exc is LoanNotFoundException)
                        {
                            break;
                        }

                        Tools.LogWarning(string.Format("Failed to update cache data for file with id ={0}, retry counter = {1}", loanId.ToString(), i.ToString()), exc);
                    }
                }

                var message = string.Format("Failed to update cached data of loan file with id {0} after 5 tries. Need to manually update it now.", loanId.ToString());
                if (sendEmailOnFailure)
                {
                    Tools.LogErrorWithCriticalTracking(message, firstExc);
                }
                else
                {
                    Tools.LogError(message, firstExc);
                }

                return false;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="firstNm"></param>
        /// <param name="midNm"></param>
        /// <param name="lastNm"></param>
        /// <param name="suffix"></param>
        /// <returns>format: FirstName MiddleName LastName, Suffix</returns>
        static public string ComposeFullName(string firstNm, string midNm, string lastNm, string suffix)
        {
            StringBuilder strBuild = new StringBuilder(70);

            if ("" != firstNm)
                strBuild.Append(firstNm);

            if ("" != midNm)
            {
                if (strBuild.Length > 0)
                    strBuild.Append(" ");
                strBuild.Append(midNm);
            }

            if ("" != lastNm)
            {
                if (strBuild.Length > 0)
                    strBuild.Append(" ");
                strBuild.Append(lastNm);
            }

            if ("" != suffix)
                strBuild.Append(", " + suffix);

            return strBuild.ToString();
        }

        /// <summary>
        /// 7/28/2004 kb - We don't need this function during loan file
        /// creation because we merged the declaring of new loan files
        /// as valid into the creator class for better name handling
        /// control.  Batch delete within simple service still uses it,
        /// though, so we specialized it for it's purposes.
        ///
        /// 6/29/2005 kb - Securing this call so that noone abuses.
        ///
        /// ONLY USE THIS IF YOU DO NOT WANT A DELETE Loan PERMISSION CHECK
        /// </summary>
        /// <param name="userInfo"></param>
        /// <param name="loanId"></param>
        /// <param name="sendNotifications"></param>
        /// <param name="generateLinkLoanMsgEvent"></param>
        static public void SystemDeclareLoanFileInvalid(AbstractUserPrincipal userInfo, Guid loanId, bool sendNotifications, bool generateLinkLoanMsgEvent, bool isLeadToLoan = false)
        {
            DeclareLoanFileInvalid(userInfo, loanId, sendNotifications, generateLinkLoanMsgEvent, false, isLeadToLoan);
        }

        /// <summary>
        /// 7/28/2004 kb - We don't need this function during loan file
        /// creation because we merged the declaring of new loan files
        /// as valid into the creator class for better name handling
        /// control.  Batch delete within simple service still uses it,
        /// though, so we specialized it for it's purposes.
        ///
        /// 6/29/2005 kb - Securing this call so that noone abuses.
        /// </summary>
        /// <param name="userInfo"></param>
        /// <param name="loanId"></param>
        /// <param name="sendNotifications"></param>
        /// <param name="generateLinkLoanMsgEvent"></param>
        static public void DeclareLoanFileInvalid(AbstractUserPrincipal userInfo, Guid loanId, bool sendNotifications, bool generateLinkLoanMsgEvent)
        {
            DeclareLoanFileInvalid(userInfo, loanId, sendNotifications, generateLinkLoanMsgEvent, true);
        }

        static private void DeclareLoanFileInvalid(AbstractUserPrincipal userInfo, Guid loanId, bool sendNotifications, bool generateLinkLoanMsgEvent, bool checkDelete, bool isLeadToLoan = false)
        {
            // 6/29/2005 kb - We now (case 2218) secure delete requests, so
            // get the calling user's credentials and check.
            if (userInfo == null)
            {
                throw new LoanDeletedPermissionDenied(userInfo, loanId, "");
            }

            var dependencies = new List<string>
            {
                nameof(CPageData.sIntegrationT),
                nameof(CPageData.sLoanFileT),
                nameof(CPageData.sBranchChannelT)
            };
            
            LoanValueEvaluator evaluator = null;
            if (checkDelete)
            {
                var deleteLoanDependencies = LendingQBExecutingEngine.GetDependencyFieldsByOperation(
                    userInfo.BrokerId,
                    WorkflowOperations.DeleteLoan);
                dependencies.AddRange(deleteLoanDependencies);
            }

            evaluator = new LoanValueEvaluator(userInfo.ConnectionInfo, dependencies, loanId);
            evaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(userInfo));

            if (checkDelete && !LendingQBExecutingEngine.CanPerform(WorkflowOperations.DeleteLoan, evaluator))
            {
                throw new LoanDeletedPermissionDenied(userInfo, loanId, evaluator.sLNm);
            }

            // Manually get values from LoanValueEvaluator to avoid overhead of initializing a CPageData object.
            E_IntegrationT sIntegrationT = (E_IntegrationT)evaluator.GetSafeScalarInt("sIntegrationT", 0);
            E_sLoanFileT sLoanFileT = (E_sLoanFileT)evaluator.GetSafeScalarInt(nameof(CPageData.sLoanFileT), -1);
            E_BranchChannelT sBranchChannelT = (E_BranchChannelT)evaluator.GetSafeScalarInt(nameof(CPageData.sBranchChannelT), 0);
            bool addToNhcQueue = ShouldAddToNhcQueue(sIntegrationT, sLoanFileT, sBranchChannelT, userInfo.BrokerDB);

            try
            {
                #region loan send event OPM 3843
                ArrayList events = new ArrayList();
                ArrayList employees = new ArrayList();

                if (sendNotifications)
                {
                    LoanAssignmentContactTable loanAssignmentTable = new LoanAssignmentContactTable(userInfo.BrokerId, loanId);


                    foreach (var assignEmployee in loanAssignmentTable.Items)
                    {
                        if (assignEmployee.RoleT !=  E_RoleT.Administrator)
                        {
                            if (assignEmployee.EmployeeId != userInfo.EmployeeId && !employees.Contains(assignEmployee.EmployeeId))
                            {
                                employees.Add(assignEmployee.EmployeeId);
                                LoanDeleted ev = new LoanDeleted();
                                ev.Initialize(loanId, assignEmployee.EmployeeId, generateLinkLoanMsgEvent);
                                events.Add(ev);
                            }
                        }
                    }
                }
                #endregion

                SqlParameter[] parameters = {
                                                new SqlParameter("@sLId", loanId),
                                                new SqlParameter("@IsValid", false),
                                                new SqlParameter("@IsLeadToLoan", isLeadToLoan)
                                            };
                StoredProcedureHelper.ExecuteNonQuery(userInfo.BrokerId, "DeclareLoanFileValid", 5, parameters );

                LoanDeleteAuditItem audit = new LoanDeleteAuditItem(userInfo);
                AuditManager.RecordAudit(loanId, audit);

                if (ConstStage.TrackIntegrationDeletesWithoutQueue)
                {
                    // OPM 233251.  Deletes are rare compared to the other actions that
                    // trigger integration modified files.  So when this setting on, make
                    // the call directly for deletes in the same way that
                    // TrackIntegrationModifiedFileProcessor does it. Temporary.
                    var brokerId = userInfo.BrokerId;
                    if (brokerId == Guid.Empty)
                    {
                        Tools.LogInfo("[OPM_233251] Fail to get broker on delete of loan: " + loanId);
                    }
                    else
                    {
                        BrokerDB brokerDB = BrokerDB.RetrieveById(brokerId);
                        if (brokerDB.IsTrackModifiedFile)
                        {
                            SqlParameter[] trackParameters =
                            {
                                new SqlParameter("@LoanID", loanId),
                                new SqlParameter("@LoanName", loanId.ToString()), // After deletion, sLNm = sLId
                                new SqlParameter("@BrokerId", brokerId)
                            };

                            try
                            {
                                int rows = StoredProcedureHelper.ExecuteNonQuery(brokerId, "TrackIntegrationModifiedFile", 5, trackParameters);
                                Tools.LogInfo(String.Format("[OPM_233251] TrackIntegrationModifiedFile called after delete. Row count:{0}, loanId:{1}, Lender:{2} ({3}).", rows, loanId, brokerDB.CustomerCode, brokerDB.BrokerID));
                            }
                            catch (Exception exc)
                            {
                                Tools.LogError("[OPM_233251] Mark deleted loan as modified fails: " + loanId, exc);
                                throw;
                            }
                        }
                    }
                }
                else
                {
                    TrackIntegrationModifiedFile(userInfo.BrokerId, loanId, loanId.ToString()); // After deletion, sLNm = sLId
                }

                if (addToNhcQueue)
                {
                    DBMessageQueue queue = new DBMessageQueue(ConstMsg.NHCQueue);
                    queue.Send(loanId.ToString(), loanId.ToString());
                }

                #region loan send event OPM 3843
                foreach (LoanDeleted ev in events)
                {
                    try
                    {
                        ev.Send();
                    }
                    catch (Exception e)
                    {
                        Tools.LogError("Failed to send loan delete notification", e);
                    }
                }
                #endregion

            }
            catch (Exception exc)
            {
                Tools.LogErrorWithCriticalTracking(string.Format("Failed to declare loan file IN-valid for fileid = {0} after 5 tries.", loanId.ToString()), exc);
                throw;
            }

            Tools.UpdateCacheTable(loanId, null);
        }

        /// <summary>
        /// Determines whether a loan ID should be added to the NHC queue for LON.
        /// Accepts the necessary loan fields to decide whether the loan meets the requirements.
        /// </summary>
        /// <seealso cref="CPageData.ShouldAddToNhcQueue"/>
        /// <param name="sIntegrationT">The sIntegrationT field value.</param>
        /// <param name="sLoanFileT">The <see cref="CPageData.sLoanFileT"/> field value.</param>
        /// <param name="sBranchChannelT">The <see cref="CPageData.sBranchChannelT"/> field value.</param>
        /// <param name="broker">The BrokerDB/lender the loan file belongs to.</param>
        /// <returns>Whether or not to add the file to the NHC queue, based on the inputs.</returns>
        public static bool ShouldAddToNhcQueue(E_IntegrationT sIntegrationT, E_sLoanFileT sLoanFileT, E_BranchChannelT sBranchChannelT, BrokerDB broker)
        {
            return sIntegrationT.HasFlag(E_IntegrationT.NHC)
                && sLoanFileT == E_sLoanFileT.Loan
                && broker.IsSupportedBranchChannelForLonIntegration(sBranchChannelT);
        }

        public static decimal SumMoney(IEnumerable<decimal> unroundedDecimals)
        {
            return SumMoney(unroundedDecimals, MidpointRounding.ToEven);
        }

        public static decimal SumMoney(IEnumerable<decimal> unroundedDecimals, MidpointRounding roundingMode)
        {
            var total = 0.00M;

            foreach (var number in unroundedDecimals)
            {
                total += Math.Round(number, 2, roundingMode);
            }

            return total;
        }

        public static decimal MoneyDifference(decimal origAmount, decimal subtractAmount)
        {
            // As with SumMoney, we want to round before subtracting to avoid rounding issues
            // with the answer.
            return Math.Round(origAmount, 2) - Math.Round(subtractAmount, 2);
        }

        public static decimal SumMoney(IEnumerable <string> moneyReps)
        {
            LosConvert convert = new LosConvert();
            var parsedDecimals = moneyReps.Select(moneyRep => convert.ToMoney(moneyRep));
            return Tools.SumMoney(parsedDecimals);
        }

        static public ListItem CreateEnumListItem(string text, Enum value)
        {
            return new ListItem(text, value.ToString("D"));
        }
        static public ListItem CreateEnumListItem(string text, Enum value, bool selected)
        {
            ListItem item = CreateEnumListItem(text, value);
            item.Selected = selected;
            return item;
        }
        static public void SetDropDownListValue(DropDownList ddl, string value)
        {
            SetDropDownListByPartialVal(ddl, value, new char[] { });
        }

        static public void BindSuffix(MeridianLink.CommonControls.ComboBox cb)
        {
            cb.Items.AddRange(new string[] { "SR.", "JR.", "I", "II", "III", "IV" });
        }
        static public void BindLateDatesDropDown(DropDownList ddl)
        {
            for (int i = 0; i < 13; i++)
                ddl.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }

        static public void BindYearDropDown(DropDownList ddl)
        {
            ddl.Items.Add(new ListItem("", ""));
            // 7/1/2005 dd - Drop down list will be from 1996 to current year.
            int currentYear = DateTime.Now.Year;
            for (int i = 1996; i <= currentYear; i++)
            {
                ddl.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
        }
        static public void BindMonthDropDown(DropDownList ddl)
        {
            ddl.Items.Add(new ListItem("", ""));
            for (int i = 1; i < 13; i++)
            {
                ddl.Items.Add(new ListItem(string.Format("{0:0#}", i), i.ToString()));
            }
        }
        static public void BindMonthNamesDropDown(DropDownList ddl)
        {
            ddl.Items.Add(new ListItem("", "0"));
            string[] monthNames = {
                "Jan", "Feb", "Mar", "April", "May", "June",
                "July", "Aug", "Sept", "Oct", "Nov", "Dec"
            };
            for (int i = 1; i <= monthNames.Length; i++)
            {
                ddl.Items.Add(new ListItem(monthNames[i-1], i.ToString()));
            }
        }
        static public string GetMonthFromInt(int monthNum)
        {
            string[] monthNames = {
                "Jan", "Feb", "Mar", "April", "May", "June",
                "July", "Aug", "Sept", "Oct", "Nov", "Dec"
            };
            return monthNames[monthNum - 1];
        }
        static public int GetIntFromMonth(string monthName)
        {
            string[] monthNames = {
                "Jan", "Feb", "Mar", "April", "May", "June",
                "July", "Aug", "Sept", "Oct", "Nov", "Dec"
            };

            for(var i = 0; i < monthNames.Length; i++){
                if(monthNames[i].Equals(monthName.TrimWhitespaceAndBOM(), StringComparison.InvariantCultureIgnoreCase)) return i + 1;
            }

            return 0;
        }
        static public void SetDropDownListByPartialVal(DropDownList ddl, string value, char[] separator)
        {

            if (ddl.SelectedItem != null)
            {
                ddl.SelectedItem.Selected = false;
            }

            ListItem item = null;
            if (separator.Length == 0)
                item = ddl.Items.FindByValue(value);
            else
            {
                int count = ddl.Items.Count;
                for (int i = 0; i < count; ++i)
                {
                    ListItem candidate = ddl.Items[i];
                    string[] result = candidate.Value.Split(separator);
                    if (result.GetValue(0).ToString().ToLower() == value.ToLower())
                    {
                        item = candidate;
                        break;
                    }
                }
            }
            string selectedValue = "";
            if (item != null)
            {
                item.Selected = true;
                selectedValue = item.Value;
            }

            //opm 16821
            AbstractUserPrincipal p = (AbstractUserPrincipal)Thread.CurrentPrincipal;
            ArrayList hiddenFields = BrokerOptionConfiguration.Instance.GetOptionsFor(ddl.ID, p.BrokerId);

            foreach (string id in hiddenFields)
            {
                ListItem currOption = ddl.Items.FindByValue(id);
                if (currOption != null && currOption.Value != selectedValue)
                {
                    ddl.Items.Remove(currOption);
                }

                else if (currOption != null && currOption.Value == selectedValue)
                {
                    if (item != null)
                    {
                        item.Text += " (not available)";
                    }
                }
            }




        }

        static public void Bind_ConversationLogCategories(DropDownList ddl, List<Category> categories)
        {
            ddl.Items.Add(new ListItem(string.Empty, "-1")); // Blank value to default to

            foreach (var category in categories)
            {
                ddl.Items.Add(new ListItem(category.DisplayName.ToString(), category.Identity.Id.ToString()));
            }
        }

        static public void SetDropDownListValue(DropDownList ddl, int value)
        {
            SetDropDownListValue(ddl, value.ToString());
        }
        static public void SetDropDownListValue(DropDownList ddl, Enum value)
        {
            SetDropDownListValue(ddl, value.ToString("D"));
        }
        public static void SetDropDownListValue(DropDownList ddl, Guid value)
        {
            SetDropDownListValue(ddl, value.ToString());
        }

        static public void SetRadioButtonListValue(RadioButtonList rbl, int value)
        {
            SetRadioButtonListValue(rbl, value.ToString());
        }
        static public void SetRadioButtonListValue(RadioButtonList rbl, Enum value)
        {
            SetRadioButtonListValue(rbl, value.ToString("D"));
        }

        static public void SetRadioButtonListValue(RadioButtonList rbl, string value)
        {
            if (rbl.SelectedItem != null)
                rbl.SelectedItem.Selected = false;


            ListItem item = rbl.Items.FindByValue(value);
            if (null != item)
                item.Selected = true;
        }
        public static int GetRadioButtonListValue(RadioButtonList rbl)
        {
            return int.Parse(rbl.SelectedItem.Value);
        }
        static public int GetDropDownListValue(DropDownList ddl)
        {
            return int.Parse(ddl.SelectedItem.Value);
        }

        static public void Bind_IPair(DropDownList ddl, IPair[] items)
        {
            foreach (IPair o in items)
            {
                ddl.Items.Add(new ListItem(o.Val, o.Key));
            }
        }

        static public void Bind_ConditionCategoryDdl(E_UserTaskPermissionLevel permission, ConditionCategoriesWithPermissionInfo categoryTypes, DropDownList ddl, int? currentCategoryId, string currentCategoryName)
        {
            IEnumerable<ConditionCategory> categories = new List<ConditionCategory>();
            switch (permission)
            {
                case E_UserTaskPermissionLevel.WorkOn:
                    categories = categoryTypes.WorkOnCategories;
                    break;
                case E_UserTaskPermissionLevel.Close:
                    categories = categoryTypes.CloseCategories;
                    break;
                case E_UserTaskPermissionLevel.Manage:
                    categories = categoryTypes.ManageCategories;
                    break;
                case E_UserTaskPermissionLevel.None:
                    categories = categoryTypes.NoPermissionCategories;
                    break;
            }

            ddl.DataSource = categories;
            ddl.DataTextField = "Category";
            ddl.DataValueField = "Id";
            ddl.DataBind();

            if (currentCategoryId.HasValue && !categories.Any(category => category.Id == currentCategoryId))
            {
                ddl.Items.Add(new ListItem(currentCategoryName, currentCategoryId.Value.ToString()));
            }
        }

        #region Bind Enum to drop down list methods.

        /// <summary>
        /// A list of credit reporting companies on Freddie Mac's Loan Product Advisor - Credit Reporting Companies and Technical Affiliates list.
        /// Last updated from http://www.freddiemac.com/loanadvisorsuite/loanproductadvisor/crc.html on 10/26/2017.
        /// </summary>
        /// <remarks>
        /// We may want to look into scraping this list from http://www.freddiemac.com/cgi-bin/crc.cgi?sort=alpha or a similar source, in order to stay up-to-date.
        /// </remarks>
        public static readonly ReadOnlyDictionary<string,string> FreddieMacCreditReportingCompanyMapping = new ReadOnlyDictionary<string, string>( new Dictionary<string, string>{
            {"1", "CBCInnovis, Inc"},
            //{"4", "FIS Credit Services"}, // No longer available in LP after 09/14/2008.
            {"5", "Equifax Mortgage Solutions"},
            {"6", "Factual Data"},
            {"8", "CREDCO/Credstar/CBA"},
            //{"A", "LandAmerica"},
            //{"B", "LandSafe"}, // 2/6/2012 dd - No longer available in LP after 2/1/2012
        });

        /// <summary>
        /// A list of technical affiliates on Freddie Mac's Loan Product Advisor - Credit Reporting Companies and Technical Affiliates list.
        /// Last updated from http://www.freddiemac.com/loanadvisorsuite/loanproductadvisor/crc.html on 10/26/2017.
        /// </summary>
        /// <remarks>
        /// See <see cref="FreddieMacCreditReportingCompanyMapping"/> for a list of the main CRCs.
        /// </remarks>
        public static readonly ReadOnlyDictionary<string, string> FreddieMacTechnicalAffiliateMapping = new ReadOnlyDictionary<string, string>(new Dictionary<string, string>
        {
            {"5025", "1 Source Data"},
            {"5031", "ACRAnet"},
            {"5301", "Advantage Credit Bureau"},
            {"5226", "Advantage Credit Inc."},
            {"5036", "Advantage Credit International Group"},
            {"5138", "Advantage Plus Credit Reporting, Inc."},
            {"5275", "Alliance 2020, Inc"},
            //{"5276", "Amaesing Credit Solutions"},
            {"5016", "American Reporting Company LLC (via MeridianLink)"},
            {"5091", "Avantus LLC"},
            {"5076", "Birchwood Credit Services, Inc."},
            //{"5146", "Cal Coast Credit Reports"},
            //{"5304", "Premium Credit Bureau/CDC via MeridianLink)"},
            //{"5318", "Premium Credit Bureau/CDC via ISS"},
            //{"5175", "Premium Credit Bureau/CCIS"},
            //{"5214", "CDS Mortgage Reports"},
            {"5071", "Certified Credit Reporting (via MeridianLink)"},
            {"5305", "Certified Credit Reporting (via Sharper Lending)"},
            {"5128", "CIC Credit"},
            {"5022", "CIS, Inc."},
            {"5146", "Clear Choice Credit Corp." },
            {"5086", "Cisco"},
            //{"5130", "CogentRoad,Inc"},
            //{"5191", "Credit Bureau of San Luis Obispo & Santa Barbara Counties"},
            //{"5243", "Credit Bureau Services Inc., Ft. Laud., FL"},
            //{"5303", "Credit Facts Services, LLC"},
            //{"5084", "Credit Communications, Inc."},
            {"5229", "Credit Data Solutions"},
            {"5298", "Credit Information Systems"},
            {"5024", "Credit Link LLC" },
            {"5017", "Credit Plus, Inc. MD"},
            //{"5166", "Credit Service Company"},
            {"5181", "Credit Technologies, Inc."},
            {"5161", "CTI - Credit Technology, Inc."},
            //{"5224", "Credit Verifiers"},
            //{"5007", "CSC Mortgage Services"},
            {"5306", "Data Facts, Inc."},
            {"5311", "DataQuick/aka CoreLogic Credco" },
            //{"5062", "Equidata"},
            {"5313", "EGS Global Solutions, Inc." },
            //{"5211", "Financial Data Reports"},
            //{"5307", "Fiserv Lending Solutions/Credstar"},
            {"5130", "Funding Suite - CSD"},
            {"5042", "Funding Suite via Sharper Lending" },
            {"5308", "Informative Research"},
            {"5058", "Information Searching Company, LLC" },
            {"5309", "KCB Information Services "},
            //{"5010", "MCR of America"},
            //{"5311", "MDA Lending Solutions"},
            {"5078", "Merchants Credit Bureau/MCB"},
            //{"5048", "Merchants Information Solutions"},
            {"5277", "MFI Credit Solutions" },
            {"5029", "Midwest Mortgage Credit Services, Inc."},
            //{"5006", "NCO Credit Services (via Percentage)"},
            //{"5313", "NCO Credit Services (via Sharper Lending)"},
            {"5251", "Partners Credit & Verification Solutions"},
            //{"5040", "One Source Credit Reporting"},
            {"5314", "Online Information Services, Inc."},
            //{"5258", "Platinum Credit Services"},
            {"5294", "Premium Credit Bureau/PCB 1"},
            //{"5030", "Pyramid Credit "},
            //{"5073", "Rapid Credit Reports, Inc."},
            //{"5087", "Royal Mortgage Credit Reporting"},
            {"5302", "SARMA - via MeridianLink" },
            {"5043", "SARMA - via Sharper Lending" },
            {"5290", "Settlement One (via MeridianLink)"},
            {"5315", "Settlement One (via Sharper Lending)"},
            //{"5119", "Southwest Credit Services"},
            {"5075", "Strategic Information Resources, Inc."},
            {"5270", "Trans Union de Puerto Rico" },
            //{"5225", "Total Credit Services"},
            //{"5297", "Trak-1 Technology"},
            {"5063", "United One Resource, Inc. (via MeridianLink)"},
            //{"5316", "United One Resource, Inc. (via Sharper Lending)"},
            {"5127", "Universal Credit Services (via MeridianLink)"},
        });

        /// <summary>
        /// Helper function to get the { "value", "text" } pairs out of a Bind_* function.
        /// </summary>
        /// <param name="bindFunc"></param>
        /// <returns></returns>
        public static List<KeyValuePair<string, string>> ExtractDDLEntries(Action<DropDownList> bindFunc)
        {
            DropDownList ddl = new DropDownList();
            bindFunc(ddl);

            var items = new List<KeyValuePair<string, string>>();
            foreach (ListItem item in ddl.Items)
            {
                items.Add(new KeyValuePair<string, string>(item.Value, item.Text));
            }
            return items;
        }

        public static void Bind_VendorServiceT(DropDownList ddl, IEnumerable<VOXServiceT> serviceTypes)
        {
            foreach (VOXServiceT serviceT in serviceTypes)
            {
                ddl.Items.Add(CreateEnumListItem(VendorServiceTToString(serviceT), serviceT));
            }
        }

        public static string VendorServiceTToString(VOXServiceT serviceT)
        {
            switch (serviceT)
            {
                case VOXServiceT.VOA_VOD:
                    return "VOA/VOD";
                case VOXServiceT.VOE:
                    return "VOE/VOI";
                case VOXServiceT.SSA_89:
                    return "SSA-89";
                default:
                    throw new UnhandledEnumException(serviceT);
            }
        }

        public static void Bind_TitleTransmissionModelT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Ernst Synchronous", TitleTransmissionModelT.ErnstSynchronousModel));
        }

        public static void Bind_TitlePayloadFormatT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Ernst XML", TitlePayloadFormatT.ErnstXml));
        }

        public static void Bind_VOXTransmissionModelT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("MCL Polling Model", TransmissionModelT.MCLPolling));
            ddl.Items.Add(CreateEnumListItem("Equifax Verification Services OFX Polling Model", TransmissionModelT.EquifaxVerificationServicesOfxPollingModel));
        }

        public static void Bind_VOXPayloadFormatT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("LQB MISMO", PayloadFormatT.LQBMismo));
            ddl.Items.Add(CreateEnumListItem("MCL Smart API", PayloadFormatT.MCLSmartApi));
            ddl.Items.Add(CreateEnumListItem("Equifax Verification Services OFX XML", PayloadFormatT.EquifaxVerificationServicesOfxXml));
        }

        public static void Bind_TransmissionAuthenticationT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Basic Authentication", TransmissionAuthenticationT.BasicAuthentication));
            ddl.Items.Add(CreateEnumListItem("Digital Certificate", TransmissionAuthenticationT.DigitalCert));
            ddl.Items.Add(CreateEnumListItem("IP Address Validation", TransmissionAuthenticationT.IpAddressValidation));
        }

        public static void Bind_sTridTargetRegulationVersionT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem(string.Empty, TridTargetRegulationVersionT.Blank));
            ddl.Items.Add(CreateEnumListItem("2015 TRID", TridTargetRegulationVersionT.TRID2015));
            ddl.Items.Add(CreateEnumListItem("2017 TRID", TridTargetRegulationVersionT.TRID2017));
        }

        public static void Bind_sTridClosingDisclosureCalculateEscrowAccountFromT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("First payment date", TridClosingDisclosureCalculateEscrowAccountFromT.FirstPaymentDate));
            ddl.Items.Add(CreateEnumListItem("Consummation", TridClosingDisclosureCalculateEscrowAccountFromT.Consummation));
        }

        public static void Bind_LoanRescindableT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Rescindable", LoanRescindableT.Rescindable));
            ddl.Items.Add(CreateEnumListItem("Not Rescindable", LoanRescindableT.NotRescindable));
            // Blank isn't a valid option yet.
        }

        public static void Bind_sLenderCreditCalculationMethodT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Set Manually", E_sLenderCreditCalculationMethodT.SetManually));
            ddl.Items.Add(CreateEnumListItem("Calculate from Front-end Rate Lock", E_sLenderCreditCalculationMethodT.CalculateFromFrontEndRateLock));
        }

        public static void Bind_sLenderCreditMaxT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("No Credit", E_sLenderCreditMaxT.NoCredit));
            ddl.Items.Add(CreateEnumListItem("Origination Charges", E_sLenderCreditMaxT.OriginationCharges));
            ddl.Items.Add(CreateEnumListItem("Total Closing Costs", E_sLenderCreditMaxT.TotalClosingCosts));
            ddl.Items.Add(CreateEnumListItem("No Limit", E_sLenderCreditMaxT.NoLimit));
            ddl.Items.Add(CreateEnumListItem("Manual", E_sLenderCreditMaxT.Manual));
        }

        public static void Bind_LenderCreditDiscloseLocationT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Initial/GFE", E_LenderCreditDiscloseLocationT.InitialGfe));
            ddl.Items.Add(CreateEnumListItem("Closing/HUD-1", E_LenderCreditDiscloseLocationT.ClosingHud1));
        }

        public static void Bind_sToleranceCureCalculationT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Include", E_sToleranceCureCalculationT.Include));
            ddl.Items.Add(CreateEnumListItem("Exclude", E_sToleranceCureCalculationT.Exclude));
        }

        public static void Bind_sInitialRateT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_sInitialRateT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("No Discount", E_sInitialRateT.NoDiscount));
            ddl.Items.Add(CreateEnumListItem("Fixed Rate", E_sInitialRateT.FixedRate));
            ddl.Items.Add(CreateEnumListItem("Discount To Margin", E_sInitialRateT.DiscountToMargin));
        }
        public static void Bind_sHelocPmtBaseT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Line Amount", E_sHelocPmtBaseT.LineAmount));
            ddl.Items.Add(CreateEnumListItem("Initial Draw", E_sHelocPmtBaseT.InitialDraw));
        }
        public static void Bind_sHelocPmtPcBaseT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Qual Rate", E_sHelocPmtPcBaseT.QualRate));
            ddl.Items.Add(CreateEnumListItem("Note Rate", E_sHelocPmtPcBaseT.NoteRate));
            ddl.Items.Add(CreateEnumListItem("Fixed Percent", E_sHelocPmtPcBaseT.FixedPercent));
        }

        public static void Bind_sHelocPmtFormulaT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Interest Only", E_sHelocPmtFormulaT.InterestOnly));
            ddl.Items.Add(CreateEnumListItem("Amortizing", E_sHelocPmtFormulaT.Amortizing));
            ddl.Items.Add(CreateEnumListItem("Fixed Percent", E_sHelocPmtFormulaT.FixedPercent));
        }

        public static void Bind_sHelocPmtFormulaRateT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Qual Rate", E_sHelocPmtFormulaRateT.QualRate));
            ddl.Items.Add(CreateEnumListItem("Note Rate", E_sHelocPmtFormulaRateT.NoteRate));
        }

        public static void Bind_sHelocPmtAmortTermT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Draw", E_sHelocPmtAmortTermT.DrawMonths));
            ddl.Items.Add(CreateEnumListItem("Repay", E_sHelocPmtAmortTermT.RepayMonths));
            ddl.Items.Add(CreateEnumListItem("Term", E_sHelocPmtAmortTermT.TermMonths));
            ddl.Items.Add(CreateEnumListItem("Due", E_sHelocPmtAmortTermT.DueMonths));
        }

        public static void Bind_QualRateCalculationFieldT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Note Rate", QualRateCalculationFieldT.NoteRate));
            ddl.Items.Add(CreateEnumListItem("Fully Indexed Rate", QualRateCalculationFieldT.FullyIndexedRate));
            ddl.Items.Add(CreateEnumListItem("Index", QualRateCalculationFieldT.Index));
            ddl.Items.Add(CreateEnumListItem("Margin", QualRateCalculationFieldT.Margin));
        }

        public static void Bind_CreditCards(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Visa", CreditCardType.Visa));
            ddl.Items.Add(CreateEnumListItem("Mastercard", CreditCardType.MasterCard));
            ddl.Items.Add(CreateEnumListItem("American Express", CreditCardType.AmericanExpress));
            ddl.Items.Add(CreateEnumListItem("Discover", CreditCardType.Discover));
        }

        public static void Bind_sVaPriorLoanT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_sVaPriorLoanT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("FHA-Fixed", E_sVaPriorLoanT.FhaFixed));
            ddl.Items.Add(CreateEnumListItem("FHA-ARM/HARM", E_sVaPriorLoanT.FhaArm));
            ddl.Items.Add(CreateEnumListItem("Conventional-Fixed", E_sVaPriorLoanT.ConventionalFixed));
            ddl.Items.Add(CreateEnumListItem("Conventional-ARM/HARM", E_sVaPriorLoanT.ConventionalArm));
            ddl.Items.Add(CreateEnumListItem("Conventional-Interest Only", E_sVaPriorLoanT.ConventionalInterestOnly));
            ddl.Items.Add(CreateEnumListItem("VA-Fixed", E_sVaPriorLoanT.VaFixed));
            ddl.Items.Add(CreateEnumListItem("VA-ARM/HARM", E_sVaPriorLoanT.VaArm));
            ddl.Items.Add(CreateEnumListItem("Other", E_sVaPriorLoanT.Other));
        }
        public static void Bind_sFreddieConstructionT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_sFreddieConstructionT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Newly Built", E_sFreddieConstructionT.NewlyBuilt));
            ddl.Items.Add(CreateEnumListItem("Construction Conversion", E_sFreddieConstructionT.ConstructionConversion));
        }


        private static readonly KeyValuePair<string, string>[] FreddieMacCurrentOfferingMapping =
        {
            new KeyValuePair<string, string>("Home Possible", "241"),
            new KeyValuePair<string, string>("Home Possible NH Solution", "243"),
            new KeyValuePair<string, string>("Home Possible Advantage", "250"),
            new KeyValuePair<string, string>("Home Possible Advantage for HFA's", "251"),
            new KeyValuePair<string, string>("Relief Refinance - Open Access", "310"),
        };
        private static readonly KeyValuePair<string, string>[] FreddieMacOfferingMapping = new[]
        {
            // The mappings not in FreddieMacCurrentOfferingMapping are legacy mappings, i.e., they still get mapped but no longer appear in the UI
            new KeyValuePair<string, string>("Freddie Mac 100", "220"),
            new KeyValuePair<string, string>("Initial Int 10/20 Fixed", "230"),
            new KeyValuePair<string, string>("Initial Int 15/15 Fixed", "231"),
            new KeyValuePair<string, string>("Initial Int ARM", "232"),
            new KeyValuePair<string, string>("Initial Int ARM - 10 Year IO", "233"),
        }.Concat(FreddieMacCurrentOfferingMapping).ToArray();

        public static void Bind_sFredAffordProgId(MeridianLink.CommonControls.ComboBox comboBox)
        {
            foreach (KeyValuePair<string, string> mapping in FreddieMacCurrentOfferingMapping)
            {
                comboBox.Items.Add(mapping.Key);
            }
        }

        public static string sFredAffordProdIdToLoanProspectorId(string sFredAffordProdId)
        {
            if (sFredAffordProdId == null)
            {
                return string.Empty;
            }

            foreach (var mapping in FreddieMacOfferingMapping)
            {
                if (StringComparer.OrdinalIgnoreCase.Equals(sFredAffordProdId, mapping.Key))
                {
                    return mapping.Value;
                }
            }

            return sFredAffordProdId;
        }

        public static string LoanProspectorIdTosFredAffordProdId(string loanProspectorId)
        {
            if (loanProspectorId == null)
            {
                return string.Empty;
            }

            foreach (var mapping in FreddieMacOfferingMapping)
            {
                if (StringComparer.OrdinalIgnoreCase.Equals(loanProspectorId, mapping.Value))
                {
                    return mapping.Key;
                }
            }

            return loanProspectorId;
        }

        static public void Bind_sFHAHousingActSection(MeridianLink.CommonControls.ComboBox comboBox)
        {
            comboBox.Items.Add("184");
            comboBox.Items.Add("203(b)");
            comboBox.Items.Add("203(b)2");
            comboBox.Items.Add("203(b)/251");
            comboBox.Items.Add("203(h)");
            comboBox.Items.Add("203(k)");
            comboBox.Items.Add("203(k)/251");
            comboBox.Items.Add("234(c)");
            comboBox.Items.Add("234(c)/251");
            comboBox.Items.Add("248");
            comboBox.Items.Add("251");
            comboBox.Items.Add("255");
        }
        static public void Bind_sFHAConstructionT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Leave Blank", E_sFHAConstructionT.Blank));
            ddl.Items.Add(CreateEnumListItem("Existing", E_sFHAConstructionT.Existing));
            ddl.Items.Add(CreateEnumListItem("Proposed", E_sFHAConstructionT.Proposed));
            ddl.Items.Add(CreateEnumListItem("New (less than 1 year)", E_sFHAConstructionT.New));

        }
        static public void Bind_sProdSpT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("SFR", E_sProdSpT.SFR));
            ddl.Items.Add(CreateEnumListItem("2 Units", E_sProdSpT.TwoUnits));
            ddl.Items.Add(CreateEnumListItem("3 Units", E_sProdSpT.ThreeUnits));
            ddl.Items.Add(CreateEnumListItem("4 Units", E_sProdSpT.FourUnits));
            ddl.Items.Add(CreateEnumListItem("PUD", E_sProdSpT.PUD));
            ddl.Items.Add(CreateEnumListItem("Condo", E_sProdSpT.Condo));


            ddl.Items.Add(CreateEnumListItem("Co-Op", E_sProdSpT.CoOp));
            ddl.Items.Add(CreateEnumListItem("Manufactured", E_sProdSpT.Manufactured));
            #region OPM #2270
            //			ddl.Items.Add(CreateEnumListItem("Townhouse", E_sProdSpT.Townhouse));
            #endregion
            #region OPM #2263
            ddl.Items.Add(CreateEnumListItem("Rowhouse", E_sProdSpT.Rowhouse));
            ddl.Items.Add(CreateEnumListItem("Modular", E_sProdSpT.Modular));
            #endregion
            #region OPM #1644
            //			ddl.Items.Add(CreateEnumListItem("Commercial", E_sProdSpT.Commercial));
            //			ddl.Items.Add(CreateEnumListItem("Mixed Use", E_sProdSpT.MixedUse));
            #endregion
        }

        public static void Bind_sAprCalculationT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Actuarial Method: Ignore Irregular First Period", E_sAprCalculationT.Actuarial_IgnoreIrregularFirstPeriod));
            ddl.Items.Add(CreateEnumListItem("Actuarial Method: Account for Irregular First Period", E_sAprCalculationT.Actuarial_AccountForIrregularFirstPeriod));
        }

        public static void Bind_sDisclosureRegulationT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_sDisclosureRegulationT.Blank));
            ddl.Items.Add(CreateEnumListItem("GFE", E_sDisclosureRegulationT.GFE));
            ddl.Items.Add(CreateEnumListItem("TRID", E_sDisclosureRegulationT.TRID));
        }

        public static void Bind_GfeClosingCostFeePaymentTimingT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("At closing", E_GfeClosingCostFeePaymentTimingT.AtClosing));
            ddl.Items.Add(CreateEnumListItem("Outside of closing", E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing));
        }

        public static void Bind_ClosingCostFeePaymentPaidByT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("borr pd", E_ClosingCostFeePaymentPaidByT.Borrower));
            ddl.Items.Add(CreateEnumListItem("seller", E_ClosingCostFeePaymentPaidByT.Seller));
            ddl.Items.Add(CreateEnumListItem("borr fin", E_ClosingCostFeePaymentPaidByT.BorrowerFinance));
            ddl.Items.Add(CreateEnumListItem("lender", E_ClosingCostFeePaymentPaidByT.Lender));
            ddl.Items.Add(CreateEnumListItem("broker", E_ClosingCostFeePaymentPaidByT.Broker));
        }

        public static void Bind_HousingExpenseType(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Hazard Insurance", E_HousingExpenseTypeT.HazardInsurance));
            ddl.Items.Add(CreateEnumListItem("Flood Insurance", E_HousingExpenseTypeT.FloodInsurance));
            ddl.Items.Add(CreateEnumListItem("Windstorm Insurance", E_HousingExpenseTypeT.WindstormInsurance));
            ddl.Items.Add(CreateEnumListItem("Condo HO6 Insurance", E_HousingExpenseTypeT.CondoHO6Insurance));
            ddl.Items.Add(CreateEnumListItem("Real Estate Tax", E_HousingExpenseTypeT.RealEstateTaxes));
            ddl.Items.Add(CreateEnumListItem("School Tax", E_HousingExpenseTypeT.SchoolTaxes));
            ddl.Items.Add(CreateEnumListItem("Other Tax 1", E_HousingExpenseTypeT.OtherTaxes1));
            ddl.Items.Add(CreateEnumListItem("Other Tax 2", E_HousingExpenseTypeT.OtherTaxes2));
            ddl.Items.Add(CreateEnumListItem("Other Tax 3", E_HousingExpenseTypeT.OtherTaxes3));
            ddl.Items.Add(CreateEnumListItem("Other Tax 4", E_HousingExpenseTypeT.OtherTaxes4));
            ddl.Items.Add(CreateEnumListItem("HOA", E_HousingExpenseTypeT.HomeownersAsscDues));
            ddl.Items.Add(CreateEnumListItem("Ground Rent", E_HousingExpenseTypeT.GroundRent));
            ddl.Items.Add(CreateEnumListItem("Unassigned", E_HousingExpenseTypeT.Unassigned));
        }

        public static void Bind_AnnAmtCalcType(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Calculator", E_AnnualAmtCalcTypeT.LoanValues));
            ddl.Items.Add(CreateEnumListItem("Disbursements", E_AnnualAmtCalcTypeT.Disbursements));
        }

        public static void Bind_DisbursementReptIntervalT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Annual", E_DisbursementRepIntervalT.Annual));
            ddl.Items.Add(CreateEnumListItem("Annually in closing month", E_DisbursementRepIntervalT.AnnuallyInClosingMonth));
            ddl.Items.Add(CreateEnumListItem("Monthly", E_DisbursementRepIntervalT.Monthly));
        }

        public static void Bind_DisbursementTypeT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Projected", E_DisbursementTypeT.Projected));
            ddl.Items.Add(CreateEnumListItem("Actual", E_DisbursementTypeT.Actual));
        }

        public static void Bind_DisbursementPaidByT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Borrower", E_DisbursementPaidByT.Borrower));
            ddl.Items.Add(CreateEnumListItem("Lender", E_DisbursementPaidByT.Lender));
            ddl.Items.Add(CreateEnumListItem("Seller", E_DisbursementPaidByT.Seller));
            ddl.Items.Add(CreateEnumListItem("Escrow/Impounds", E_DisbursementPaidByT.EscrowImpounds));
        }

        public static void Bind_CustomExpenseLineNumberT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("None", E_CustomExpenseLineNumberT.None));
            ddl.Items.Add(CreateEnumListItem("1008", E_CustomExpenseLineNumberT.Line1008));
            ddl.Items.Add(CreateEnumListItem("1009", E_CustomExpenseLineNumberT.Line1009));
            ddl.Items.Add(CreateEnumListItem("1010", E_CustomExpenseLineNumberT.Line1010));
            ddl.Items.Add(CreateEnumListItem("1011", E_CustomExpenseLineNumberT.Line1011));
            ddl.Items.Add(CreateEnumListItem("Any", E_CustomExpenseLineNumberT.Any));

        }

        public static void Bind_DisbPaidDateType(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Before Closing", E_DisbPaidDateType.BeforeClosing));
            ddl.Items.Add(CreateEnumListItem("At Closing", E_DisbPaidDateType.AtClosing));
            ddl.Items.Add(CreateEnumListItem("After Closing", E_DisbPaidDateType.AfterClosing));
        }

		public static void Bind_DeliveryMethodT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_DeliveryMethodT.LeaveEmpty));
            ddl.Items.Add(CreateEnumListItem("Email", E_DeliveryMethodT.Email));
            ddl.Items.Add(CreateEnumListItem("Fax", E_DeliveryMethodT.Fax));
            ddl.Items.Add(CreateEnumListItem("In Person", E_DeliveryMethodT.InPerson));
            ddl.Items.Add(CreateEnumListItem("Mail", E_DeliveryMethodT.Mail));
            ddl.Items.Add(CreateEnumListItem("Overnight", E_DeliveryMethodT.Overnight));
        }

        public static void Bind_EscrowImpoundsCalcMethodT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Regulatory", E_EscrowImpoundsCalcMethodT.Regulatory));
            ddl.Items.Add(CreateEnumListItem("Customary", E_EscrowImpoundsCalcMethodT.Customary));
        }

        public static void Bind_CustomaryCalcMinT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("No Min", E_CustomaryEscrowImpoundsCalcMinT.NoMin));
            ddl.Items.Add(CreateEnumListItem("Zero (Adjust Escrow Total)", E_CustomaryEscrowImpoundsCalcMinT.Zero));
            ddl.Items.Add(CreateEnumListItem("Cushion (Adjust Escrow Total)", E_CustomaryEscrowImpoundsCalcMinT.Cushion));
            ddl.Items.Add(CreateEnumListItem("Zero (Adjust Aggregate)", E_CustomaryEscrowImpoundsCalcMinT.Zero_AdjustAggregate));
            ddl.Items.Add(CreateEnumListItem("Cushion (Adjust Aggregate)", E_CustomaryEscrowImpoundsCalcMinT.Cushion_AdjustAggregate));
        }

        public static void Bind_YesOrNo(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("No", YesOrNo.No));
            ddl.Items.Add(CreateEnumListItem("Yes", YesOrNo.Yes));
        }

        public static void Bind_AggregateEscrowCalcModeT(DropDownList ddl, bool addCustomary)
        {
            ddl.Items.Add(CreateEnumListItem("Legacy", E_AggregateEscrowCalculationModeT.Legacy));
            ddl.Items.Add(CreateEnumListItem("Regulatory", E_AggregateEscrowCalculationModeT.Regulatory));
            if (addCustomary)
            {
                ddl.Items.Add(CreateEnumListItem("Customary", E_AggregateEscrowCalculationModeT.Customary));
                ddl.Items.Add(CreateEnumListItem("Customary 2", E_AggregateEscrowCalculationModeT.Customary2));
            }
        }

        public static void Bind_PaidDateType(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("Before Closing", E_DisbPaidDateType.BeforeClosing));
            ddl.Items.Add(Tools.CreateEnumListItem("As part of closing", E_DisbPaidDateType.AtClosing));
            ddl.Items.Add(Tools.CreateEnumListItem("After Closing", E_DisbPaidDateType.AfterClosing));
        }

        public static void Bind_DisbPaidByType(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("Borrower", E_DisbursementPaidByT.Borrower));
            ddl.Items.Add(Tools.CreateEnumListItem("Lender", E_DisbursementPaidByT.Lender));
            ddl.Items.Add(Tools.CreateEnumListItem("Seller", E_DisbursementPaidByT.Seller));
            ddl.Items.Add(Tools.CreateEnumListItem("Escrow/Impounds", E_DisbursementPaidByT.EscrowImpounds));
        }

        public static E_sInsSettlementChargeT LineNumToSettlementChargeT(E_CustomExpenseLineNumberT lineNum)
        {
            switch(lineNum)
            {
                case E_CustomExpenseLineNumberT.None:
                case E_CustomExpenseLineNumberT.Any:
                    return E_sInsSettlementChargeT.None;
                case E_CustomExpenseLineNumberT.Line1008:
                    return E_sInsSettlementChargeT.Line1008;
                case E_CustomExpenseLineNumberT.Line1009:
                    return E_sInsSettlementChargeT.Line1009;
                case E_CustomExpenseLineNumberT.Line1010:
                    return E_sInsSettlementChargeT.Line1010;
                case E_CustomExpenseLineNumberT.Line1011:
                    return E_sInsSettlementChargeT.Line1011;
                default:
                    throw new UnhandledEnumException(lineNum);
            }
        }

        public static E_CustomExpenseLineNumberT SettlementChargeTToLineNum(E_sInsSettlementChargeT charge)
        {
            switch(charge)
            {
                case E_sInsSettlementChargeT.None:
                    return E_CustomExpenseLineNumberT.None;
                case E_sInsSettlementChargeT.Line1008:
                    return E_CustomExpenseLineNumberT.Line1008;
                case E_sInsSettlementChargeT.Line1009:
                    return E_CustomExpenseLineNumberT.Line1009;
                case E_sInsSettlementChargeT.Line1010:
                    return E_CustomExpenseLineNumberT.Line1010;
                case E_sInsSettlementChargeT.Line1011:
                    return E_CustomExpenseLineNumberT.Line1011;
                default:
                    throw new UnhandledEnumException(charge);
            }
        }

        public static E_TableSettlementDescT LineNumToSettlementDescT(E_CustomExpenseLineNumberT lineNum)
        {
            switch(lineNum)
            {
                case E_CustomExpenseLineNumberT.None:
                case E_CustomExpenseLineNumberT.Any:
                    return E_TableSettlementDescT.NoInfo;
                case E_CustomExpenseLineNumberT.Line1008:
                    return E_TableSettlementDescT.Use1008DescT;
                case E_CustomExpenseLineNumberT.Line1009:
                    return E_TableSettlementDescT.Use1009DescT;
                case E_CustomExpenseLineNumberT.Line1010:
                    return E_TableSettlementDescT.Use1010DescT;
                case E_CustomExpenseLineNumberT.Line1011:
                    return E_TableSettlementDescT.Use1011DescT;
                default:
                    throw new UnhandledEnumException(lineNum);
            }
        }

        public static E_CustomExpenseLineNumberT SettlementDescTToLineNum(E_TableSettlementDescT desc)
        {
            switch(desc)
            {
                case E_TableSettlementDescT.NoInfo:
                    return E_CustomExpenseLineNumberT.None;
                case E_TableSettlementDescT.Use1008DescT:
                    return E_CustomExpenseLineNumberT.Line1008;
                case E_TableSettlementDescT.Use1009DescT:
                    return E_CustomExpenseLineNumberT.Line1009;
                case E_TableSettlementDescT.Use1010DescT:
                    return E_CustomExpenseLineNumberT.Line1010;
                case E_TableSettlementDescT.Use1011DescT:
                    return E_CustomExpenseLineNumberT.Line1011;
                default:
                    throw new UnhandledEnumException(desc);
            }
        }

        public static string GetIntegratedDisclosureString(E_IntegratedDisclosureSectionT section)
        {
            switch(section)
            {
                case E_IntegratedDisclosureSectionT.SectionA:
                    return "A";
                case E_IntegratedDisclosureSectionT.SectionB:
                    return "B";
                case E_IntegratedDisclosureSectionT.SectionC:
                    return "C";
                case E_IntegratedDisclosureSectionT.SectionD:
                    return "D";
                case E_IntegratedDisclosureSectionT.SectionE:
                    return "E";
                case E_IntegratedDisclosureSectionT.SectionF:
                    return "F";
                case E_IntegratedDisclosureSectionT.SectionG:
                    return "G";
                case E_IntegratedDisclosureSectionT.SectionH:
                    return "H";
                case E_IntegratedDisclosureSectionT.LeaveBlank:
                case E_IntegratedDisclosureSectionT.SectionBorC:
                    return "Other";
                default:
                    throw new UnhandledEnumException(section);
            }
        }

        static public string GetsProdSpTDescription(E_sProdSpT sProdSpT)
        {
            switch (sProdSpT)
            {
                case E_sProdSpT.SFR:
                    return "SFR";
                case E_sProdSpT.PUD:
                    return "PUD";
                case E_sProdSpT.Condo:
                    return "Condo";
                case E_sProdSpT.CoOp:
                    return "Co-Op";
                case E_sProdSpT.Manufactured:
                    return "Manufactured";
                case E_sProdSpT.Townhouse:
                    return "Townhouse";
                case E_sProdSpT.Commercial:
                    return "Commercial";
                case E_sProdSpT.MixedUse:
                    return "Mixed Used";
                case E_sProdSpT.TwoUnits:
                    return "2 Units";
                case E_sProdSpT.ThreeUnits:
                    return "3 Units";
                case E_sProdSpT.FourUnits:
                    return "4 Units";
                case E_sProdSpT.Modular:
                    return "Modular";
                case E_sProdSpT.Rowhouse:
                    return "Rowhouse";
                default:
                    throw new UnhandledEnumException(sProdSpT);
            }
        }

        static public void Bind_sFannieCommunityLendingT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Leave Blank", E_sFannieCommunityLendingT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("MyCommunityMortgage", E_sFannieCommunityLendingT.MyCommunityMortgage)); // 10/5/2007 dd - There is no space between word. This word is trademark.
            ddl.Items.Add(CreateEnumListItem("HFA Preferred Risk Sharing", E_sFannieCommunityLendingT.HFAPreferredRiskSharing));
            ddl.Items.Add(CreateEnumListItem("HFA Preferred", E_sFannieCommunityLendingT.HFAPreferred));
            ddl.Items.Add(CreateEnumListItem("HomeReady", E_sFannieCommunityLendingT.HomeReady));
            ddl.Items.Add(CreateEnumListItem("Community Home Buyer Program (discontinued)", E_sFannieCommunityLendingT.CommunityHomeBuyer));
            ddl.Items.Add(CreateEnumListItem("Fannie 97 (discontinued)", E_sFannieCommunityLendingT.Fannie97));
            ddl.Items.Add(CreateEnumListItem("Fannie 3/2 (discontinued)", E_sFannieCommunityLendingT.Fannie32));

        }

        static public void Bind_sFannieCommunitySecondsRepaymentStructureT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Leave Blank", E_sFannieCommunitySecondsRepaymentStructureT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Any payment (including interest only, P&I, etc.) required within first 5 years", E_sFannieCommunitySecondsRepaymentStructureT.AnyPaymentWithin5Years)); // 10/5/2007 dd - There is no space between word. This word is trademark.
            ddl.Items.Add(CreateEnumListItem("Payments deferred 5 or more years and fully forgiven", E_sFannieCommunitySecondsRepaymentStructureT.PaymentsDeferredFullyForgiven));
            ddl.Items.Add(CreateEnumListItem("Payments deferred 5 or more years and not fully forgiven", E_sFannieCommunitySecondsRepaymentStructureT.PaymentsDeferredNotFullyForgiven));
        }

        static public void Bind_sApprT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Full", E_sApprT.Full));
            ddl.Items.Add(CreateEnumListItem("Drive By", E_sApprT.DriveBy));
            ddl.Items.Add(CreateEnumListItem("Statistical", E_sApprT.Statistical));
            ddl.Items.Add(CreateEnumListItem("Stated Value", E_sApprT.Stated));

        }

        static public void Bind_sTimeZoneT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_sTimeZoneT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Pacific Time", E_sTimeZoneT.PacificTime));
            ddl.Items.Add(CreateEnumListItem("Mountain Time", E_sTimeZoneT.MountainTime));
            ddl.Items.Add(CreateEnumListItem("Central Time", E_sTimeZoneT.CentralTime));
            ddl.Items.Add(CreateEnumListItem("Eastern Time", E_sTimeZoneT.EasternTime));
        }

        static public void Bind_sProdDocT(HtmlSelect ddl, BrokerDB broker, E_sProdDocT currentValue)
        {
            Bind_sProdDocT(ddl.Items, broker.RenameAlternativeDocType, broker.UsingLegacyDocTypes, !broker.HideEnhancedDocTypes, currentValue);
        }

        static public void Bind_sProdDocT(DropDownList ddl, BrokerDB broker, E_sProdDocT currentValue)
        {
            Bind_sProdDocT(ddl.Items, broker.RenameAlternativeDocType, broker.UsingLegacyDocTypes, !broker.HideEnhancedDocTypes, currentValue);
        }

        static public void Bind_sProdDocT(ListItemCollection ddlItems, bool renameAlternativeDocType, bool showLegacy, bool showEnhanced, E_sProdDocT currentValue)
        {
            ddlItems.Add(CreateEnumListItem("Full Document", E_sProdDocT.Full));

            // 1/21/2009 dd - I explicitly did not include E_sProdDocT.Streamline to dropdown for user to select. See OPM 27153
            var legacyDocumentationTypes = new[]
            {
                Tuple.Create(renameAlternativeDocType ? "Alt Doc" : "Alt - 12 months bank stmts", E_sProdDocT.Alt),
                Tuple.Create("Lite - 6 months bank stmts", E_sProdDocT.Light),
                Tuple.Create("NINA - No Income, No Assets", E_sProdDocT.NINA),
                Tuple.Create("NISA - No Income, Stated Assets", E_sProdDocT.NISA),
                Tuple.Create("No Doc (NINANE) - No Income, No Assets, No Empl", E_sProdDocT.NINANE),
                Tuple.Create("No Ratio (NIVA) - No Income, Verified Assets", E_sProdDocT.NIVA),
                Tuple.Create("NIV (SISA) - Stated Income, Stated Assets", E_sProdDocT.SISA),
                Tuple.Create("NIV (SIVA) - Stated Income, Verified Assets", E_sProdDocT.SIVA),
                Tuple.Create("VISA - Verified Income, Stated Assets", E_sProdDocT.VISA),
                Tuple.Create("NoDoc Verif Assets - No Income, Verified Assets, No Empl", E_sProdDocT.NIVANE),
                Tuple.Create("VINA - Verified Income, No Assets", E_sProdDocT.VINA)
            };

            var enhancedDocumentationTypes = new[]
            {
                Tuple.Create("12 Mo. Personal Bank Statements", E_sProdDocT._12MoPersonalBankStatements),
                Tuple.Create("24 Mo. Personal Bank Statements", E_sProdDocT._24MoPersonalBankStatements),
                Tuple.Create("12 Mo. Business Bank Statements", E_sProdDocT._12MoBusinessBankStatements),
                Tuple.Create("24 Mo. Business Bank Statements", E_sProdDocT._24MoBusinessBankStatements),
                Tuple.Create("Other Bank Statements", E_sProdDocT.OtherBankStatements),
                Tuple.Create("1 Yr. Tax Returns", E_sProdDocT._1YrTaxReturns),
                Tuple.Create("VOE", E_sProdDocT.Voe),
                Tuple.Create("Asset Utilization", E_sProdDocT.AssetUtilization),
                Tuple.Create("Debt Service Coverage (DSCR)", E_sProdDocT.DebtServiceCoverage),
                Tuple.Create("No Ratio", E_sProdDocT.NoIncome)
            };

            // Full documentation is always displayed in the dropdown.
            // Streamline is never displayed in the dropdown per OPM 27153.
            if (currentValue != E_sProdDocT.Full && currentValue != E_sProdDocT.Streamline)
            {
                var selectedValueTuple = legacyDocumentationTypes.FirstOrDefault(tuple => tuple.Item2 == currentValue)
                    ?? enhancedDocumentationTypes.First(tuple => tuple.Item2 == currentValue);

                ddlItems.Add(CreateEnumListItem(selectedValueTuple.Item1, selectedValueTuple.Item2));
            }

            if (showLegacy)
            {
                foreach (var tuple in legacyDocumentationTypes.Where(tuple => tuple.Item2 != currentValue))
                {
                    ddlItems.Add(CreateEnumListItem(tuple.Item1, tuple.Item2));
                }
            }

            if (showEnhanced)
            {
                foreach (var tuple in enhancedDocumentationTypes.Where(tuple => tuple.Item2 != currentValue))
                {
                    ddlItems.Add(CreateEnumListItem(tuple.Item1, tuple.Item2));
                }
            }
        }

        static public void Bind_PmiCompanyT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem(PmiCompanyT_map[E_PmiCompanyT.Arch], E_PmiCompanyT.Arch));
            ddl.Items.Add(CreateEnumListItem(PmiCompanyT_map[E_PmiCompanyT.Essent], E_PmiCompanyT.Essent));
            ddl.Items.Add(CreateEnumListItem(PmiCompanyT_map[E_PmiCompanyT.Genworth], E_PmiCompanyT.Genworth));
            ddl.Items.Add(CreateEnumListItem(PmiCompanyT_map[E_PmiCompanyT.MassHousing], E_PmiCompanyT.MassHousing));
            ddl.Items.Add(CreateEnumListItem(PmiCompanyT_map[E_PmiCompanyT.MGIC], E_PmiCompanyT.MGIC));
            ddl.Items.Add(CreateEnumListItem(PmiCompanyT_map[E_PmiCompanyT.NationalMI], E_PmiCompanyT.NationalMI));
            ddl.Items.Add(CreateEnumListItem(PmiCompanyT_map[E_PmiCompanyT.Radian], E_PmiCompanyT.Radian));
        }

        public static Dictionary<E_PmiCompanyT, string> PmiCompanyT_map = new Dictionary<E_PmiCompanyT, string>() {
            { E_PmiCompanyT.Genworth, "Genworth" },
            { E_PmiCompanyT.MGIC, "MGIC" },
            { E_PmiCompanyT.Radian, "Radian" },
            { E_PmiCompanyT.Essent, "Essent" },
            { E_PmiCompanyT.Arch, "Arch MI"  },
            { E_PmiCompanyT.NationalMI, "National MI" },
            { E_PmiCompanyT.MassHousing, "MassHousing" }
        };

        static public void Bind_PmiTypeT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem(PmiTypeT_map[E_PmiTypeT.BorrowerPaidMonthlyPremium], E_PmiTypeT.BorrowerPaidMonthlyPremium));
            ddl.Items.Add(CreateEnumListItem(PmiTypeT_map[E_PmiTypeT.BorrowerPaidSinglePremium], E_PmiTypeT.BorrowerPaidSinglePremium));
            ddl.Items.Add(CreateEnumListItem(PmiTypeT_map[E_PmiTypeT.BorrowerPaidSplitPremium], E_PmiTypeT.BorrowerPaidSplitPremium));
            ddl.Items.Add(CreateEnumListItem(PmiTypeT_map[E_PmiTypeT.LenderPaidSinglePremium], E_PmiTypeT.LenderPaidSinglePremium));
        }

        public static Dictionary<E_PmiTypeT, string> PmiTypeT_map = new Dictionary<E_PmiTypeT, string>() {
            { E_PmiTypeT.BorrowerPaidMonthlyPremium, "Borrower Paid - Monthly Premium" },
            { E_PmiTypeT.BorrowerPaidSinglePremium, "Borrower Paid - Single Premium" },
            { E_PmiTypeT.BorrowerPaidSplitPremium, "Borrower Paid - Split Premium" },
            { E_PmiTypeT.LenderPaidSinglePremium, "Lender Paid - Single Premium" },
            { E_PmiTypeT.MASTER, "MASTER" }
        };

        static public void Bind_sProdCrManualDerogRecentStatusT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Not satisfied", E_sProdCrManualDerogRecentStatusT.NotSatisfied));
            ddl.Items.Add(CreateEnumListItem("Discharged", E_sProdCrManualDerogRecentStatusT.Discharged));
            ddl.Items.Add(CreateEnumListItem("Dismissed", E_sProdCrManualDerogRecentStatusT.Dismissed));
        }
        //static public string GetDocumentTypeDescription(E_sProdDocT sProdDocT)
        //{
        //    switch (sProdDocT)
        //    {
        //        case E_sProdDocT.Full: return "Full Document";
        //        case E_sProdDocT.Alt: return "Alt - 12 months bank stmts";
        //        case E_sProdDocT.Light: return "Lite - 6 months bank stmts";
        //        case E_sProdDocT.NINA: return "NINA - No Income, No Assets";
        //        case E_sProdDocT.NIVA: return "No Ratio - No Income, Verified Assets";
        //        case E_sProdDocT.NISA: return "NISA - No Income, Stated Assets";
        //        case E_sProdDocT.SISA: return "NIV (SISA) - Stated Income, Stated Assets";
        //        case E_sProdDocT.SIVA: return "NIV (SIVA) - Stated Income, Verified Assets";
        //        case E_sProdDocT.VISA: return "VISA - Verified Income, Stated Assets";
        //        case E_sProdDocT.NINANE: return "No Doc - No Income, No Assets, No Empl";
        //        case E_sProdDocT.NIVANE: return "NoDoc Verif Assets - No Income, Verified Assets, No Empl";
        //        case E_sProdDocT.VINA: return "VINA - Verified Income, No Assets";
        //        case E_sProdDocT.Streamline: return "Streamline";
        //        default:
        //            Tools.LogBug("Unhandle sProdDocT=" + sProdDocT + " in Tools.GetDocumentTypeDescription()");
        //            break;

        //    }
        //    return "";
        //}
        static public string GetProd3rdPartyUwResultTDescription(E_sProd3rdPartyUwResultT sProd3rdPartyUwResultT)
        {
            switch (sProd3rdPartyUwResultT)
            {
                case E_sProd3rdPartyUwResultT.NA: return "None/Not Submitted";
                case E_sProd3rdPartyUwResultT.DU_ApproveEligible: return "DU Approve/Eligible";
                case E_sProd3rdPartyUwResultT.DU_ApproveIneligible: return "DU Approve/Ineligible";
                case E_sProd3rdPartyUwResultT.DU_ReferEligible: return "DU Refer/Eligible";
                case E_sProd3rdPartyUwResultT.DU_ReferIneligible: return "DU Refer/Ineligible";
                case E_sProd3rdPartyUwResultT.DU_ReferWCautionEligible: return "DU Refer with Caution/Eligible";
                case E_sProd3rdPartyUwResultT.DU_ReferWCautionIneligible: return "DU Refer with Caution/Ineligible";
                case E_sProd3rdPartyUwResultT.DU_EAIEligible: return "DU EA I/Eligible";
                case E_sProd3rdPartyUwResultT.DU_EAIIEligible: return "DU EA II/Eligible";
                case E_sProd3rdPartyUwResultT.DU_EAIIIEligible: return "DU EA III/Eligible";
                case E_sProd3rdPartyUwResultT.LP_AcceptEligible: return "LP Accept/Eligible";
                case E_sProd3rdPartyUwResultT.LP_AcceptIneligible: return "LP Accept/Ineligible";
                case E_sProd3rdPartyUwResultT.LP_CautionEligible: return "LP Caution/Eligible";
                case E_sProd3rdPartyUwResultT.LP_CautionIneligible: return "LP Caution/Ineligible";
                case E_sProd3rdPartyUwResultT.OutOfScope: return "Out of Scope";
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level1: return "LP A- Level 1";
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level2: return "LP A- Level 2";
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level3: return "LP A- Level 3";
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level4: return "LP A- Level 4";
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level5: return "LP A- Level 5";
                case E_sProd3rdPartyUwResultT.Lp_Refer: return "LP Refer";
                case E_sProd3rdPartyUwResultT.Total_ApproveEligible: return "TOTAL Approve/Eligible";
                case E_sProd3rdPartyUwResultT.Total_ApproveIneligible: return "TOTAL Approve/Ineligible";
                case E_sProd3rdPartyUwResultT.Total_ReferEligible: return "TOTAL Refer/Eligible";
                case E_sProd3rdPartyUwResultT.Total_ReferIneligible: return "TOTAL Refer/Ineligible";
                case E_sProd3rdPartyUwResultT.GUS_AcceptEligible: return "GUS Accept/Eligible";
                case E_sProd3rdPartyUwResultT.GUS_AcceptIneligible: return "GUS Accept/Ineligible";
                case E_sProd3rdPartyUwResultT.GUS_ReferEligible: return "GUS Refer/Eligible";
                case E_sProd3rdPartyUwResultT.GUS_ReferIneligible: return "GUS Refer/Ineligible";
                case E_sProd3rdPartyUwResultT.GUS_ReferWCautionEligible: return "GUS Refer with Caution/Eligible";
                case E_sProd3rdPartyUwResultT.GUS_ReferWCautionIneligible: return "GUS Refer with Caution/Ineligible";

                default:
                    Tools.LogBug("Unhandle sProd3rdPartyUwResultT=" + sProd3rdPartyUwResultT + " in Tools.GetProd3rdPartyUwResultTDescription()");
                    break;
            }
            return "";
        }

        static public string GetStructureTypeDescription(E_sProdSpStructureT sProdSpStructureT)
        {
            switch (sProdSpStructureT)
            {
                case E_sProdSpStructureT.Attached: return "Attached";
                case E_sProdSpStructureT.Detached: return "Detached";
                default:
                    Tools.LogBug("Unhandle sProdSpStructureT=" + sProdSpStructureT + " in Tools.GetStructureTypeDescription()");
                    break;
            }
            return "";
        }
        
        static public string GetSubFinTDescription(E_sSubFinT sSubFinT)
        {
            switch (sSubFinT)
            {
                case E_sSubFinT.CloseEnd:
                    return "Closed-End";

                case E_sSubFinT.Heloc:
                    return "HELOC";
                default:
                    throw new UnhandledEnumException(sSubFinT);
            }
        }

        static public void Bind_sApprPmtMethod(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem(" ", E_sApprPmtMethodT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("C.O.D.", E_sApprPmtMethodT.COD));
            ddl.Items.Add(CreateEnumListItem("Credit Card", E_sApprPmtMethodT.CreditCard));
            ddl.Items.Add(CreateEnumListItem("Invoice Client", E_sApprPmtMethodT.InvoiceClient));
            ddl.Items.Add(CreateEnumListItem("Bill", E_sApprPmtMethodT.Bill));
            ddl.Items.Add(CreateEnumListItem("Other", E_sApprPmtMethodT.Other));
        }

        static public void Bind_TridLoanPurposeType(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Purchase", TridLoanPurposeType.Purchase));
            ddl.Items.Add(CreateEnumListItem("Refinance", TridLoanPurposeType.Refinance));
            ddl.Items.Add(CreateEnumListItem("Construction", TridLoanPurposeType.Construction));
            ddl.Items.Add(CreateEnumListItem("Home Equity Loan", TridLoanPurposeType.HomeEquityLoan));
        }

        public static void Bind_sLPurposeT(DropDownList ddl)
        {
            Bind_sLPurposeT(ddl.Items);
        }

        public static void Bind_sLPurposeT(HtmlSelect ddl)
        {
            Bind_sLPurposeT(ddl.Items);
        }

        private static void Bind_sLPurposeT(ListItemCollection items)
        {
            items.Add(CreateEnumListItem("Purchase", E_sLPurposeT.Purchase));
            items.Add(CreateEnumListItem("Refi Rate/Term", E_sLPurposeT.Refin));
            items.Add(CreateEnumListItem("Refinance Cashout", E_sLPurposeT.RefinCashout));
            items.Add(CreateEnumListItem("Construction", E_sLPurposeT.Construct));
            items.Add(CreateEnumListItem("Construction Perm", E_sLPurposeT.ConstructPerm));
            items.Add(CreateEnumListItem("FHA Streamline Refi", E_sLPurposeT.FhaStreamlinedRefinance));
            items.Add(CreateEnumListItem("VA IRRRL", E_sLPurposeT.VaIrrrl));
            items.Add(CreateEnumListItem("Other", E_sLPurposeT.Other));

            // 9/9/2013 dd - OPM 136658 - Only display Home Equity option if broker settings is enable.
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            if (principal != null)
            {
                BrokerDB broker = principal.BrokerDB;
                if (broker.IsEnableHomeEquityInLoanPurpose)
                {
                    items.Add(CreateEnumListItem("Home Equity", E_sLPurposeT.HomeEquity));
                }

            }
        }

        static public void Bind_sLPurposeTPe(HtmlSelect ddl)
        {
            Bind_sLPurposeTPe(ddl.Items);
        }

        static public void Bind_sLPurposeTPe(DropDownList ddl)
        {
            Bind_sLPurposeTPe(ddl.Items);
        }

        static private void Bind_sLPurposeTPe(ListItemCollection ddlItems)
        {
            ddlItems.Add(CreateEnumListItem("Refi Rate/Term", E_sLPurposeT.Refin));
            ddlItems.Add(CreateEnumListItem("Refinance Cashout", E_sLPurposeT.RefinCashout));
            ddlItems.Add(CreateEnumListItem("FHA Streamline Refi", E_sLPurposeT.FhaStreamlinedRefinance));
            ddlItems.Add(CreateEnumListItem("VA IRRRL", E_sLPurposeT.VaIrrrl));
            // 9/9/2013 dd - OPM 136658 - Only display Home Equity option if broker settings is enable.
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            if (principal != null)
            {
                BrokerDB broker = principal.BrokerDB;
                if (broker.IsEnableHomeEquityInLoanPurpose)
                {
                    ddlItems.Add(CreateEnumListItem("Home Equity", E_sLPurposeT.HomeEquity));
                }

            }
        }

        static public void Bind_sSimpleLPurposeT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Purchase", E_sLPurposeT.Purchase));
            ddl.Items.Add(CreateEnumListItem("Refi Rate/Term", E_sLPurposeT.Refin));
            ddl.Items.Add(CreateEnumListItem("Refinance Cashout", E_sLPurposeT.RefinCashout));
            ddl.Items.Add(CreateEnumListItem("FHA Streamline Refi", E_sLPurposeT.FhaStreamlinedRefinance));
            ddl.Items.Add(CreateEnumListItem("VA IRRRL", E_sLPurposeT.VaIrrrl));
            // 9/9/2013 dd - OPM 136658 - Only display Home Equity option if broker settings is enable.
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            if (principal != null)
            {
                BrokerDB broker = principal.BrokerDB;
                if (broker.IsEnableHomeEquityInLoanPurpose)
                {
                    ddl.Items.Add(CreateEnumListItem("Home Equity", E_sLPurposeT.HomeEquity));
                }

            }
        }

        static public void Bind_sSellerCreditT(HtmlSelect ddl)
        {
            Bind_sSellerCreditT(ddl.Items);
        }

        static public void Bind_sSellerCreditT(DropDownList ddl)
        {
            Bind_sSellerCreditT(ddl.Items);
        }

        static private void Bind_sSellerCreditT(ListItemCollection ddlItems)
        {
            ddlItems.Add(CreateEnumListItem("0%", E_SellerCreditT.ZeroPct));
            ddlItems.Add(CreateEnumListItem("1%", E_SellerCreditT.OnePct));
            ddlItems.Add(CreateEnumListItem("2%", E_SellerCreditT.TwoPct));
            ddlItems.Add(CreateEnumListItem("3%", E_SellerCreditT.ThreePct));
            ddlItems.Add(CreateEnumListItem("4%", E_SellerCreditT.FourPct));
            ddlItems.Add(CreateEnumListItem("5%", E_SellerCreditT.FivePct));
            ddlItems.Add(CreateEnumListItem("6%", E_SellerCreditT.SixPct));
        }

        static public void Bind_sLienPosT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("1st Mortgage", E_sLienPosT.First));
            ddl.Items.Add(CreateEnumListItem("2nd Mortgage", E_sLienPosT.Second));
        }

        static public void Bind_ResponsibleLien(HtmlSelect select)
        {
            Bind_ResponsibleLienImpl(select.Items);
        }

        static public void Bind_ResponsibleLien(DropDownList ddl)
        {
            Bind_ResponsibleLienImpl(ddl.Items);
        }

        static private void Bind_ResponsibleLienImpl(ListItemCollection items)
        {
            items.Add(CreateEnumListItem("This Lien Transaction", ResponsibleLien.ThisLienTransaction));
            items.Add(CreateEnumListItem("Other Lien Transaction", ResponsibleLien.OtherLienTransaction));
        }

        static public void Bind_CounselingType(HtmlSelect select)
        {
            select.Items.Add(CreateEnumListItem("Counseling", CounselingType.Counseling));
            select.Items.Add(CreateEnumListItem("Education", CounselingType.Education));
        }

        static public void Bind_CounselingFormatType(HtmlSelect select)
        {
            select.Items.Add(CreateEnumListItem("Face to Face", CounselingFormatType.FaceToFace));
            select.Items.Add(CreateEnumListItem("Telephone", CounselingFormatType.Telephone));
            select.Items.Add(CreateEnumListItem("Internet", CounselingFormatType.Internet));
        }

        static public string ConvertsLtToString(E_sLT type)
        {
            switch (type)
            {
                case E_sLT.Conventional:
                    return "Conventional";
                case E_sLT.FHA:
                    return "FHA";
                case E_sLT.Other:
                    return "Other";
                case E_sLT.UsdaRural:
                    return "USDA/Rural Housing";
                case E_sLT.VA:
                    return "VA";
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        public static void Bind_sLT(DropDownList ddl)
        {
            Bind_sLT(ddl.Items);
        }

        public static void Bind_sLT(HtmlSelect ddl)
        {
            Bind_sLT(ddl.Items);
        }

        private static void Bind_sLT(ListItemCollection items)
        {
            items.Add(CreateEnumListItem(ConvertsLtToString(E_sLT.Conventional), E_sLT.Conventional));
            items.Add(CreateEnumListItem(ConvertsLtToString(E_sLT.FHA), E_sLT.FHA));
            items.Add(CreateEnumListItem(ConvertsLtToString(E_sLT.VA), E_sLT.VA));
            items.Add(CreateEnumListItem(ConvertsLtToString(E_sLT.UsdaRural), E_sLT.UsdaRural)); // After 1/1/2004 This is name will be used on 1003.
            items.Add(CreateEnumListItem(ConvertsLtToString(E_sLT.Other), E_sLT.Other));
        }

        static public void Bind_CustomFolder_LeadLoanStatus(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Not Applicable", LendersOffice.ObjLib.CustomFavoriteFolder.LeadLoanStatusT.DC));
            ddl.Items.Add(CreateEnumListItem("Loan", LendersOffice.ObjLib.CustomFavoriteFolder.LeadLoanStatusT.Loan));
            ddl.Items.Add(CreateEnumListItem("Lead", LendersOffice.ObjLib.CustomFavoriteFolder.LeadLoanStatusT.Lead));
        }

        /// <summary>
        /// This depends on how sLT is implemented.  See case 217824.
        /// </summary>
        static public bool ShouldBeReadonly_sLT(E_sLPurposeT loanPurposeT)
        {
            if (loanPurposeT == E_sLPurposeT.FhaStreamlinedRefinance)
            {
                return true;
            }
            else if (loanPurposeT == E_sLPurposeT.VaIrrrl)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Adding the NotEditable attribute so the background-color isn't lightgrey, but there is probably a better way to achieve this.
        /// </summary>
        static public void ReadonlifyDropDown(DropDownList ddl)
        {
            ddl.Attributes.Add("disabled", "disabled");
            ddl.Attributes.Add("NotEditable", "true");
        }

        static public void Bind_ConditionChoice_sLT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Any", E_ConditionChoice_sLT.Any));
            ddl.Items.Add(CreateEnumListItem("Conventional", E_ConditionChoice_sLT.Conventional));
            ddl.Items.Add(CreateEnumListItem("FHA", E_ConditionChoice_sLT.FHA));
            ddl.Items.Add(CreateEnumListItem("VA", E_ConditionChoice_sLT.VA));
            ddl.Items.Add(CreateEnumListItem("USDA/Rural Housing", E_ConditionChoice_sLT.UsdaRural)); // After 1/1/2004 This is name will be used on 1003.
            ddl.Items.Add(CreateEnumListItem("Other", E_ConditionChoice_sLT.Other));
        }

        static public void Bind_ConditionType(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem(E_ConditionType.Application.FriendlyValue(), E_ConditionType.Application));
            ddl.Items.Add(CreateEnumListItem(E_ConditionType.AppraisalProperty.FriendlyValue(), E_ConditionType.AppraisalProperty));
            ddl.Items.Add(CreateEnumListItem(E_ConditionType.Assets.FriendlyValue(), E_ConditionType.Assets));
            ddl.Items.Add(CreateEnumListItem(E_ConditionType.ComplianceDisclosure.FriendlyValue(), E_ConditionType.ComplianceDisclosure));
            ddl.Items.Add(CreateEnumListItem(E_ConditionType.Credit.FriendlyValue(), E_ConditionType.Credit));
            ddl.Items.Add(CreateEnumListItem(E_ConditionType.EmploymentIncome.FriendlyValue(), E_ConditionType.EmploymentIncome));
            ddl.Items.Add(CreateEnumListItem(E_ConditionType.Title.FriendlyValue(), E_ConditionType.Title));
            ddl.Items.Add(CreateEnumListItem(E_ConditionType.Insurance.FriendlyValue(), E_ConditionType.Insurance));
            ddl.Items.Add(CreateEnumListItem(E_ConditionType.Misc.FriendlyValue(), E_ConditionType.Misc));
        }

        static public void Bind_ConditionTypeString(DropDownList ddl)
        {
            ddl.Items.Add(new ListItem(E_ConditionType.Application.FriendlyValue(), E_ConditionType.Application.FriendlyValue()));
            ddl.Items.Add(new ListItem(E_ConditionType.AppraisalProperty.FriendlyValue(), E_ConditionType.AppraisalProperty.FriendlyValue()));
            ddl.Items.Add(new ListItem(E_ConditionType.Assets.FriendlyValue(), E_ConditionType.Assets.FriendlyValue()));
            ddl.Items.Add(new ListItem(E_ConditionType.ComplianceDisclosure.FriendlyValue(), E_ConditionType.ComplianceDisclosure.FriendlyValue()));
            ddl.Items.Add(new ListItem(E_ConditionType.Credit.FriendlyValue(), E_ConditionType.Credit.FriendlyValue()));
            ddl.Items.Add(new ListItem(E_ConditionType.EmploymentIncome.FriendlyValue(), E_ConditionType.EmploymentIncome.FriendlyValue()));
            ddl.Items.Add(new ListItem(E_ConditionType.Title.FriendlyValue(), E_ConditionType.Title.FriendlyValue()));
            ddl.Items.Add(new ListItem(E_ConditionType.Insurance.FriendlyValue(), E_ConditionType.Insurance.FriendlyValue()));
            ddl.Items.Add(new ListItem(E_ConditionType.Misc.FriendlyValue(), E_ConditionType.Misc.FriendlyValue()));
        }
        static public void Bind_aTypeT(DropDownList ddl, bool isTargeting2019Ulad)
        {
            ddl.Items.Add(CreateEnumListItem("Individual", E_aTypeT.Individual));
            ddl.Items.Add(CreateEnumListItem("Co-Signer", E_aTypeT.CoSigner));
            ddl.Items.Add(CreateEnumListItem("Title Only", E_aTypeT.TitleOnly));
            ddl.Items.Add(CreateEnumListItem("Non-Title Spouse", E_aTypeT.NonTitleSpouse));

            if (isTargeting2019Ulad)
            {
                ddl.Items.Add(CreateEnumListItem("Current Title Only", E_aTypeT.CurrentTitleOnly));
            }
        }
        public static void Bind_aOIDescT(ComboBox cb)
        {
            foreach (var pair in OtherIncome.GetTypeDescriptionMappings().OrderBy(pair => pair.Value))
            {
                var type = pair.Key;
                var description = pair.Value;

                if (type != E_aOIDescT.AlimonyChildSupport && type != E_aOIDescT.SocialSecurityDisability)
                {
                    // ejm opm 467848 - Removed in favor of the split types (Alimony, ChildSupport, SocialSecurity, Disability)
                   cb.Items.Add(description);
                }
            }
        }

        static public void Bind_aMaritalStatT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem(aMaritalStatT_rep(E_aBMaritalStatT.LeaveBlank), E_aBMaritalStatT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem(aMaritalStatT_rep(E_aBMaritalStatT.Married), E_aBMaritalStatT.Married));
            ddl.Items.Add(CreateEnumListItem(aMaritalStatT_rep(E_aBMaritalStatT.NotMarried), E_aBMaritalStatT.NotMarried, true));
            ddl.Items.Add(CreateEnumListItem(aMaritalStatT_rep(E_aBMaritalStatT.Separated), E_aBMaritalStatT.Separated));

        }
        public static string aMaritalStatT_rep(E_aBMaritalStatT stat)
        {
            switch (stat)
            {
                case E_aBMaritalStatT.Married:
                    return "Married";
                case E_aBMaritalStatT.NotMarried:
                    return "Not Married";
                case E_aBMaritalStatT.Separated:
                    return "Separated";
                case E_aBMaritalStatT.LeaveBlank:
                    return "";
                default:
                    throw new UnhandledEnumException(stat);
            }
        }

        public static void Bind_MclCreditReportType(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem(MclCreditReportType_rep(MclCreditReportType.Standard), MclCreditReportType.Standard));
            ddl.Items.Add(CreateEnumListItem(MclCreditReportType_rep(MclCreditReportType.MortageOnly), MclCreditReportType.MortageOnly));
            ddl.Items.Add(CreateEnumListItem(MclCreditReportType_rep(MclCreditReportType.MortgageOnlyWithCreditScores), MclCreditReportType.MortgageOnlyWithCreditScores));
            ddl.Items.Add(CreateEnumListItem(MclCreditReportType_rep(MclCreditReportType.MortgageOnlyWithScoresAndFactors), MclCreditReportType.MortgageOnlyWithScoresAndFactors));
        }

        public static string MclCreditReportType_rep(MclCreditReportType type)
        {
            switch (type)
            {
                case MclCreditReportType.Standard:
                    return "Standard";
                case MclCreditReportType.MortageOnly:
                    return "Mortgage Only";
                case MclCreditReportType.MortgageOnlyWithCreditScores:
                    return "Mortgage Only with Credit Scores";
                case MclCreditReportType.MortgageOnlyWithScoresAndFactors:
                    return "Mortgage Only with Scores and Factors";
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        public static string Get_sMiCompanyNmTFriendlyDisplay(E_sMiCompanyNmT sMiCompanyNmT)
        {
            switch (sMiCompanyNmT)
            {
                case E_sMiCompanyNmT.LeaveBlank: return "";
                case E_sMiCompanyNmT.CMG: return "CMG";
                case E_sMiCompanyNmT.Essent: return "Essent";
                case E_sMiCompanyNmT.Genworth: return "Genworth";
                case E_sMiCompanyNmT.MGIC: return "MGIC";
                case E_sMiCompanyNmT.Radian: return "Radian";
                case E_sMiCompanyNmT.UnitedGuaranty: return "United Guaranty";
                case E_sMiCompanyNmT.Amerin: return "Amerin";
                case E_sMiCompanyNmT.CAHLIF: return "CAHLIF";
                case E_sMiCompanyNmT.CMGPre94: return "CMG Pre-Sep 94";
                case E_sMiCompanyNmT.Commonwealth: return "Commonwealth";
                case E_sMiCompanyNmT.MDHousing: return "MD Housing";
                case E_sMiCompanyNmT.MIF: return "MIF";
                case E_sMiCompanyNmT.PMI: return "PMI";
                case E_sMiCompanyNmT.RMIC: return "RMIC";
                case E_sMiCompanyNmT.RMICNC: return "RMIC-NC";
                case E_sMiCompanyNmT.SONYMA: return "SONYMA";
                case E_sMiCompanyNmT.Triad: return "Triad";
                case E_sMiCompanyNmT.Verex: return "Verex";
                case E_sMiCompanyNmT.WiscMtgAssr: return "Wisc Mtg Assr";
                case E_sMiCompanyNmT.NationalMI: return "National MI";
                case E_sMiCompanyNmT.FHA: return "FHA";
                case E_sMiCompanyNmT.VA: return "VA";
                case E_sMiCompanyNmT.USDA: return "USDA";
                case E_sMiCompanyNmT.Arch: return "Arch MI";
                case E_sMiCompanyNmT.MassHousing: return "MassHousing";
                default:
                    throw new UnhandledEnumException(sMiCompanyNmT);
            }
        }

        static public void Bind_aDecisionCreditSourceT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Experian", E_aDecisionCreditSourceT.Experian));
            ddl.Items.Add(CreateEnumListItem("TransUnion", E_aDecisionCreditSourceT.TransUnion));
            ddl.Items.Add(CreateEnumListItem("Equifax", E_aDecisionCreditSourceT.Equifax));
            ddl.Items.Add(CreateEnumListItem("Other", E_aDecisionCreditSourceT.Other));
        }

        static public void Bind_cTitleInsurancePolicy(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Owner", E_cTitleInsurancePolicyT.Owner));
            ddl.Items.Add(CreateEnumListItem("Lender", E_cTitleInsurancePolicyT.Lender));
            ddl.Items.Add(CreateEnumListItem("Simultaneous", E_cTitleInsurancePolicyT.Simultaneous));
        }

        static public void Bind_cPricingEngineCostT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("801 Loan origination fee", E_cPricingEngineCostT._801LoanOriginationFee));
            ddl.Items.Add(CreateEnumListItem("802 Credit or Charge", E_cPricingEngineCostT._802CreditOrCharge));
            ddl.Items.Add(CreateEnumListItem("None", E_cPricingEngineCostT.None));
        }

        static public void Bind_cPricingEngineCreditT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("801 Loan origination fee", E_cPricingEngineCreditT._801LoanOriginationFee));
            ddl.Items.Add(CreateEnumListItem("802 Credit or Charge", E_cPricingEngineCreditT._802CreditOrCharge));
            ddl.Items.Add(CreateEnumListItem("None", E_cPricingEngineCreditT.None));
        }

        static public void Bind_cPricingEngineLimitCreditToT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Origination charges (Box A1)", E_cPricingEngineLimitCreditToT.OriginationCharges));
            ddl.Items.Add(CreateEnumListItem("Estimated closing cost", E_cPricingEngineLimitCreditToT.EstimatedClosingCosts));
            ddl.Items.Add(CreateEnumListItem("No limit", E_cPricingEngineLimitCreditToT.NoLimit));
        }

        static public void Bind_aAddrT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_aBAddrT.LeaveBlank, true));
            ddl.Items.Add(CreateEnumListItem("Own", E_aBAddrT.Own));
            ddl.Items.Add(CreateEnumListItem("Rent", E_aBAddrT.Rent));
            ddl.Items.Add(CreateEnumListItem("Living Rent Free", E_aBAddrT.LivingRentFree));
        }

        static public void Bind_aAddrMailSourceT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Present Address", E_aAddrMailSourceT.PresentAddress));
            ddl.Items.Add(CreateEnumListItem("Subject Property Address", E_aAddrMailSourceT.SubjectPropertyAddress));
            ddl.Items.Add(CreateEnumListItem("Other", E_aAddrMailSourceT.Other));
        }

        static public void Bind_aAddrPostSourceT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Present Address", E_aAddrPostSourceT.PresentAddress));
            ddl.Items.Add(CreateEnumListItem("Mailing Address", E_aAddrPostSourceT.MailingAddress));
            ddl.Items.Add(CreateEnumListItem("Subject Property Address", E_aAddrPostSourceT.SubjectPropertyAddress));
            ddl.Items.Add(CreateEnumListItem("Other", E_aAddrPostSourceT.Other));
        }

        public static void Bind_sQMStatusT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem(CPageBase.sQMStatusT_map_rep(E_sQMStatusT.Blank), E_sQMStatusT.Blank));
            ddl.Items.Add(CreateEnumListItem(CPageBase.sQMStatusT_map_rep(E_sQMStatusT.Eligible), E_sQMStatusT.Eligible));
            ddl.Items.Add(CreateEnumListItem(CPageBase.sQMStatusT_map_rep(E_sQMStatusT.Ineligible), E_sQMStatusT.Ineligible));
            ddl.Items.Add(CreateEnumListItem(CPageBase.sQMStatusT_map_rep(E_sQMStatusT.ProvisionallyEligible), E_sQMStatusT.ProvisionallyEligible));
        }

        public static void Bind_sQMLoanPurchaseAgency(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem(CPageBase.sQMLoanPurchaseAgency_map_rep(E_sQMLoanPurchaseAgency.Blank), E_sQMLoanPurchaseAgency.Blank));
            ddl.Items.Add(CreateEnumListItem(CPageBase.sQMLoanPurchaseAgency_map_rep(E_sQMLoanPurchaseAgency.FHA), E_sQMLoanPurchaseAgency.FHA));
            ddl.Items.Add(CreateEnumListItem(CPageBase.sQMLoanPurchaseAgency_map_rep(E_sQMLoanPurchaseAgency.VA), E_sQMLoanPurchaseAgency.VA));
            ddl.Items.Add(CreateEnumListItem(CPageBase.sQMLoanPurchaseAgency_map_rep(E_sQMLoanPurchaseAgency.USDA), E_sQMLoanPurchaseAgency.USDA));
            ddl.Items.Add(CreateEnumListItem(CPageBase.sQMLoanPurchaseAgency_map_rep(E_sQMLoanPurchaseAgency.FannieMae), E_sQMLoanPurchaseAgency.FannieMae));
            ddl.Items.Add(CreateEnumListItem(CPageBase.sQMLoanPurchaseAgency_map_rep(E_sQMLoanPurchaseAgency.FreddieMac), E_sQMLoanPurchaseAgency.FreddieMac));
        }

        public static void Bind_GiftFundSourceT(DropDownList ddl)
        {
            Bind_GiftFundSourceT(ddl.Items);
        }

        private static void Bind_GiftFundSourceT(ListItemCollection items)
        {
            items.Add(CreateEnumListItem(string.Empty, E_GiftFundSourceT.Blank));
            items.Add(CreateEnumListItem("Relative", E_GiftFundSourceT.Relative));
            items.Add(CreateEnumListItem("Employer", E_GiftFundSourceT.Employer));
            items.Add(CreateEnumListItem("Government", E_GiftFundSourceT.Government));
            items.Add(CreateEnumListItem("Nonprofit", E_GiftFundSourceT.Nonprofit));
            items.Add(CreateEnumListItem("Other", E_GiftFundSourceT.Other));
        }

        public static void Bind_ReoTypeT(DropDownList ddl)
        {
            Bind_ReoTypeT(ddl.Items);
        }

        public static void Bind_ReoTypeT(HtmlSelect ddl)
        {
            Bind_ReoTypeT(ddl.Items);
        }

        private static void Bind_ReoTypeT(ListItemCollection items)
        {
            items.Add(CreateEnumListItem("", E_ReoTypeT.LeaveBlank, true));
            items.Add(CreateEnumListItem("2-4Plx", E_ReoTypeT._2_4Plx));
            items.Add(CreateEnumListItem("Com-NR", E_ReoTypeT.ComNR));
            items.Add(CreateEnumListItem("Com-R", E_ReoTypeT.ComR));
            items.Add(CreateEnumListItem("Condo", E_ReoTypeT.Condo));
            items.Add(CreateEnumListItem("Coop", E_ReoTypeT.Coop));
            items.Add(CreateEnumListItem("Farm", E_ReoTypeT.Farm));
            items.Add(CreateEnumListItem("Land", E_ReoTypeT.Land));
            items.Add(CreateEnumListItem("Mixed", E_ReoTypeT.Mixed));
            items.Add(CreateEnumListItem("Mobil", E_ReoTypeT.Mobil));
            items.Add(CreateEnumListItem("Multi", E_ReoTypeT.Multi));
            items.Add(CreateEnumListItem("SFR", E_ReoTypeT.SFR));
            items.Add(CreateEnumListItem("Town", E_ReoTypeT.Town));
            items.Add(CreateEnumListItem("Other", E_ReoTypeT.Other));
        }

        static public void Bind_sTotalScoreRefiT(DropDownList ddl)
        {
            ddl.Items.Add((CreateEnumListItem("", E_sTotalScoreRefiT.LeaveBlank, true)));
            ddl.Items.Add((CreateEnumListItem("Conventional-to-FHA", E_sTotalScoreRefiT.ConventionalToFHA, false)));
            ddl.Items.Add((CreateEnumListItem("FHA-to-FHA", E_sTotalScoreRefiT.FHAToFHANonStreamline, false)));
            ddl.Items.Add((CreateEnumListItem("Unknown", E_sTotalScoreRefiT.Unknown, false)));
        }
        /// <summary>
        /// Special binding for the FHA Connection page
        /// </summary>
        static public void Bind_sTotalScoreRefiT_FHAConnection(DropDownList ddl)
        {
            ddl.Items.Add((CreateEnumListItem("<- Select a Refi Type ->", E_sTotalScoreRefiT.LeaveBlank)));
            ddl.Items.Add((CreateEnumListItem("Prior FHA", E_sTotalScoreRefiT.FHAToFHANonStreamline)));
            ddl.Items.Add((CreateEnumListItem("Conventional", E_sTotalScoreRefiT.ConventionalToFHA)));
        }

        static public void Bind_sFHAFieldOfficeCode(DropDownList ddl, string state)
        {
            // 2/18/2015 tj - 202809 - FHA Field offices' jurisdiction is limited to their state with the
            // exception of DC, which also has jurisdiction in Virginia and Maryland.
            Func<LendersOffice.ObjLib.FHAConnection.FHAFieldOffice, bool> isFHAFieldOfficeWithJurisdiction;
            if (state == "VA" || state == "MD")
            {
                isFHAFieldOfficeWithJurisdiction = office => office.State == state || office.State == "DC";
            }
            else if (state == "MO")
            {
                isFHAFieldOfficeWithJurisdiction = office => office.State == state || office.Code == "0716";
            }
            else
            {
                isFHAFieldOfficeWithJurisdiction = office => office.State == state;
            }

            IEnumerable<LendersOffice.ObjLib.FHAConnection.FHAFieldOffice>  offices =
                LendersOffice.ObjLib.FHAConnection.FhaConnectionUtil.FhaFieldOfficeList.Where(isFHAFieldOfficeWithJurisdiction);

            if (offices.Count() == 0)
            {
                offices = LendersOffice.ObjLib.FHAConnection.FhaConnectionUtil.FhaFieldOfficeList.ToList();
            }

            foreach (LendersOffice.ObjLib.FHAConnection.FHAFieldOffice office in offices)
            {
                ddl.Items.Add(new ListItem(office.City + ", " + office.State, office.Code));
            }
        }

        static public void Bind_sFHAProgramId(DropDownList ddl)
        {
            ddl.DataSource = LendersOffice.ObjLib.FHAConnection.FhaConnectionUtil.FhaProgramList;
            ddl.DataTextField = "Name";
            ddl.DataValueField = "Id";
            ddl.DataBind();
        }


        static public void Bind_sTotalScoreCurrentMortgageStatusT(DropDownList ddl)
        {
            ddl.Items.Add((CreateEnumListItem("", E_sTotalScoreCurrentMortgageStatusT.LeaveBlank, true)));
            ddl.Items.Add((CreateEnumListItem("Current", E_sTotalScoreCurrentMortgageStatusT.Current, false)));
            ddl.Items.Add((CreateEnumListItem("30 day late", E_sTotalScoreCurrentMortgageStatusT.Late30, false)));
            ddl.Items.Add((CreateEnumListItem("60 day late", E_sTotalScoreCurrentMortgageStatusT.Late60, false)));
            ddl.Items.Add((CreateEnumListItem("90 day late", E_sTotalScoreCurrentMortgageStatusT.Late90, false)));
            ddl.Items.Add((CreateEnumListItem("120+ day late", E_sTotalScoreCurrentMortgageStatusT.Late120Plus, false)));

        }

        static public void Bind_sTotalScoreFhaProductT(DropDownList ddl)
        {
            ddl.Items.Add((CreateEnumListItem("", E_sTotalScoreFhaProductT.LeaveBlank, true)));
            ddl.Items.Add((CreateEnumListItem("Standard - 203(b)/234(c)/251", E_sTotalScoreFhaProductT.Standard, false)));
            ddl.Items.Add((CreateEnumListItem("Rehabilitation - 203(k)/251", E_sTotalScoreFhaProductT.Rehabilitation, false)));
            ddl.Items.Add((CreateEnumListItem("Disaster Victims - 203(h)", E_sTotalScoreFhaProductT.DisasterVictims, false)));

        }


        static public void Bind_sInvestorLockCommitmentT(DropDownList ddl)
        {
            ddl.Items.Add((CreateEnumListItem("Best Efforts", E_sInvestorLockCommitmentT.BestEfforts)));
            ddl.Items.Add((CreateEnumListItem("Mandatory", E_sInvestorLockCommitmentT.Mandatory)));
            ddl.Items.Add((CreateEnumListItem("Hedged", E_sInvestorLockCommitmentT.Hedged)));
            ddl.Items.Add((CreateEnumListItem("Securitized", E_sInvestorLockCommitmentT.Securitized)));

        }

        static public void Bind_aBTotalScoreFhtbCounselingT_Legacy(DropDownList ddl)
        {
            ddl.Items.Add((CreateEnumListItem("", E_aTotalScoreFhtbCounselingT.LeaveBlank, true)));
            ddl.Items.Add((CreateEnumListItem("HUD Approved Counseling", E_aTotalScoreFhtbCounselingT.HudApprovedCounseling)));
            ddl.Items.Add((CreateEnumListItem("Not Counseled", E_aTotalScoreFhtbCounselingT.NotCounseled)));
            ddl.Items.Add((CreateEnumListItem("Not Applicable", E_aTotalScoreFhtbCounselingT.NotApplicable)));
        }

        static public void Bind_aBTotalScoreFhtbCounselingT(DropDownList ddl)
        {
            ddl.Items.Add((CreateEnumListItem("", E_aTotalScoreFhtbCounselingT.LeaveBlank, true)));
            ddl.Items.Add((CreateEnumListItem("HUD Approved Counseling", E_aTotalScoreFhtbCounselingT.HudApprovedCounseling)));
            ddl.Items.Add((CreateEnumListItem("No HUD-Approved Counseling", E_aTotalScoreFhtbCounselingT.NotCounseled)));
            ddl.Items.Add((CreateEnumListItem("Not Applicable", E_aTotalScoreFhtbCounselingT.NotApplicable)));
        }

        static public void Bind_sFHARefinanceTypeDesc(MeridianLink.CommonControls.ComboBox cb)
        {
            // 11/16/2007 dd - In FNMA 3.2 format these are the valid values: "Full Documentation", "Interest Rate Reduction Refinance Loan", "Streamline with Appraisal", "Streamline without Appraisal"
            //                 However their DO/DU only have "Full Documentation", "Streamline with Appraisal", "Streamline without Appraisal".
            //                 We currently decide to only make values support by DO/DU UI available to our user.
            cb.Items.Add("Full Documentation");
            cb.Items.Add("Streamline with Appraisal");
            cb.Items.Add("Streamline without Appraisal");
            //            sFHARefinanceTypeDesc.Items.Add("Regular Refinance");
            //            sFHARefinanceTypeDesc.Items.Add("Cash-Out Refinance");

        }

        public static void Bind_UnderwritingService(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("DU", UnderwritingService.DU));
            ddl.Items.Add(CreateEnumListItem("LPA", UnderwritingService.LP));
            ddl.Items.Add(CreateEnumListItem("TOTAL", UnderwritingService.TOTAL));
            ddl.Items.Add(CreateEnumListItem("GUS", UnderwritingService.GUS));
            ddl.Items.Add(CreateEnumListItem("Other", UnderwritingService.Other));
        }

        public static void Bind_DuRecommendation(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem(string.Empty, DuRecommendation.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Refer/Eligible", DuRecommendation.Refer_Eligible));
            ddl.Items.Add(CreateEnumListItem("Out of Scope", DuRecommendation.OutOfScope));
            ddl.Items.Add(CreateEnumListItem("Approve/Eligible", DuRecommendation.Approve_Eligible));
            ddl.Items.Add(CreateEnumListItem("Refer/Ineligible", DuRecommendation.Refer_Ineligible));
            ddl.Items.Add(CreateEnumListItem("Unknown", DuRecommendation.Unknown));
            ddl.Items.Add(CreateEnumListItem("Approve/Ineligible", DuRecommendation.Approve_Ineligible));
            ddl.Items.Add(CreateEnumListItem("Refer with Caution", DuRecommendation.ReferWithCaution));
            ddl.Items.Add(CreateEnumListItem("Refer", DuRecommendation.Refer));
            ddl.Items.Add(CreateEnumListItem("Deny", DuRecommendation.Deny));
            ddl.Items.Add(CreateEnumListItem("Error", DuRecommendation.Error));
            ddl.Items.Add(CreateEnumListItem("Complete", DuRecommendation.Complete));
            ddl.Items.Add(CreateEnumListItem("Approve", DuRecommendation.Approve));
            ddl.Items.Add(CreateEnumListItem("Ineligible", DuRecommendation.Ineligible));
            ddl.Items.Add(CreateEnumListItem("Complete with Warning", DuRecommendation.CompleteWithWarning));
            ddl.Items.Add(CreateEnumListItem("Refer W Caution/I", DuRecommendation.ReferWCaution_I));
            ddl.Items.Add(CreateEnumListItem("Refer W Caution/II", DuRecommendation.ReferWCaution_II));
            ddl.Items.Add(CreateEnumListItem("Refer W Caution/III", DuRecommendation.ReferWCaution_III));
            ddl.Items.Add(CreateEnumListItem("Refer W Caution/IV", DuRecommendation.ReferWCaution_IV));
            ddl.Items.Add(CreateEnumListItem("Resubmit", DuRecommendation.Resubmit));
            ddl.Items.Add(CreateEnumListItem("Expanded Approval/I", DuRecommendation.ExpandedApproval_I));
            ddl.Items.Add(CreateEnumListItem("Expanded Approval/II", DuRecommendation.ExpandedApproval_II));
            ddl.Items.Add(CreateEnumListItem("Expanded Approval/III", DuRecommendation.ExpandedApproval_III));
            ddl.Items.Add(CreateEnumListItem("Expanded Approval/IV", DuRecommendation.ExpandedApproval_IV));
            ddl.Items.Add(CreateEnumListItem("EA-I/Eligible", DuRecommendation.EAI_Eligible));
            ddl.Items.Add(CreateEnumListItem("EA-I/Ineligible", DuRecommendation.EAI_Ineligible));
            ddl.Items.Add(CreateEnumListItem("EA-II/Eligible", DuRecommendation.EAII_Eligible));
            ddl.Items.Add(CreateEnumListItem("EA-II/Ineligible", DuRecommendation.EAII_Ineligible));
            ddl.Items.Add(CreateEnumListItem("EA-III/Eligible", DuRecommendation.EAIII_Eligible));
            ddl.Items.Add(CreateEnumListItem("EA-III/Ineligible", DuRecommendation.EAIII_Ineligible));
            ddl.Items.Add(CreateEnumListItem("EA-IV/Eligible", DuRecommendation.EAIV_Eligible));
            ddl.Items.Add(CreateEnumListItem("EA-IV/Ineligible", DuRecommendation.EAIV_Ineligible));
            ddl.Items.Add(CreateEnumListItem("RWC-IV/Eligible", DuRecommendation.RWCIV_Eligible));
            ddl.Items.Add(CreateEnumListItem("RWC-IV/Ineligible", DuRecommendation.RWCIV_Ineligible));
        }

        public static void Bind_LpRiskClass(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem(string.Empty, LpRiskClass.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Accept", LpRiskClass.Accept));
            ddl.Items.Add(CreateEnumListItem("Refer", LpRiskClass.Refer));
            ddl.Items.Add(CreateEnumListItem("Caution", LpRiskClass.Caution));
            ddl.Items.Add(CreateEnumListItem("N/A", LpRiskClass.NA));
        }

        public static void Bind_LpPurchaseEligibility(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem(string.Empty, LpPurchaseEligibility.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("N/A", LpPurchaseEligibility.NA));
            ddl.Items.Add(CreateEnumListItem("000FreddieMacEligible", LpPurchaseEligibility._000FreddieMacEligible));
            ddl.Items.Add(CreateEnumListItem("000FreddieMacIneligible", LpPurchaseEligibility._000FreddieMacIneligible));
            ddl.Items.Add(CreateEnumListItem("500FreddieMacIneligibleLPAMinusOffering", LpPurchaseEligibility._500FreddieMacEligibleLPAMinusOffering));
        }

        public static void Bind_LpStatus(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem(string.Empty, LpStatus.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Invalid", LpStatus.Invalid));
            ddl.Items.Add(CreateEnumListItem("Ineligible", LpStatus.Ineligible));
            ddl.Items.Add(CreateEnumListItem("Incomplete", LpStatus.Incomplete));
            ddl.Items.Add(CreateEnumListItem("Complete", LpStatus.Complete));
        }

        public static void Bind_sTotalScoreSecondaryFinancingSrc(DropDownList ddl, E_TotalScorecardSecondaryFinancingSrc currentValue)
        {
            ddl.Items.Add(CreateEnumListItem(string.Empty, E_TotalScorecardSecondaryFinancingSrc.Undefined));
            ddl.Items.Add(CreateEnumListItem("N/A", E_TotalScorecardSecondaryFinancingSrc.NA));

            // This value has been deprecated by HUD, so we only want to show it if it's the current value on the loan file.
            if (currentValue == E_TotalScorecardSecondaryFinancingSrc.GovernmentNonprofitInstrumentality)
            {
                ddl.Items.Add(CreateEnumListItem("Government & Nonprofit Instrumentality of Government", E_TotalScorecardSecondaryFinancingSrc.GovernmentNonprofitInstrumentality));
            }

            ddl.Items.Add(CreateEnumListItem("Nonprofit (not Instrumentality of Government)", E_TotalScorecardSecondaryFinancingSrc.NonprofitNotInstrumentalityOfGovernment));
            ddl.Items.Add(CreateEnumListItem("Private Organizations/Eligible Individuals", E_TotalScorecardSecondaryFinancingSrc.PrivateOrganizations));
            ddl.Items.Add(CreateEnumListItem("Lender", E_TotalScorecardSecondaryFinancingSrc.Lender));
            ddl.Items.Add(CreateEnumListItem("Government - State or Local", E_TotalScorecardSecondaryFinancingSrc.GovernmentStateOrLocal));
            ddl.Items.Add(CreateEnumListItem("Nonprofit Instrumentality of Government", E_TotalScorecardSecondaryFinancingSrc.NonProfitInstrumentalityOfGovernment));
            ddl.Items.Add(CreateEnumListItem("Family/Relative", E_TotalScorecardSecondaryFinancingSrc.FamilyRelative));
            ddl.Items.Add(CreateEnumListItem("Federal Government", E_TotalScorecardSecondaryFinancingSrc.FederalGovernment));
        }

        public static void Bind_TotalRiskClass(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem(string.Empty, TotalRiskClass.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Unknown", TotalRiskClass.Unknown));
            ddl.Items.Add(CreateEnumListItem("Accept", TotalRiskClass.Accept));
            ddl.Items.Add(CreateEnumListItem("Refer", TotalRiskClass.Refer));
        }

        public static void Bind_TotalEligibilityAssessment(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem(string.Empty, TotalEligibilityAssessment.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Ineligible", TotalEligibilityAssessment.Ineligible));
            ddl.Items.Add(CreateEnumListItem("Eligible", TotalEligibilityAssessment.Eligible));
            ddl.Items.Add(CreateEnumListItem("Insufficient Info", TotalEligibilityAssessment.InsufficientInfo));
        }

        public static void Bind_GusRecommendation(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem(string.Empty, GusRecommendation.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Accept", GusRecommendation.Accept));
            ddl.Items.Add(CreateEnumListItem("Refer", GusRecommendation.Refer));
            ddl.Items.Add(CreateEnumListItem("Refer with Caution", GusRecommendation.ReferWithCaution));
            ddl.Items.Add(CreateEnumListItem("Ineligible", GusRecommendation.Ineligible));
            ddl.Items.Add(CreateEnumListItem("Unable to Determine", GusRecommendation.UnableToDetermine));
        }

        public static void Bind_GusRiskEvaluation(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem(string.Empty, GusRiskEvaluation.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Eligible", GusRiskEvaluation.Eligible));
            ddl.Items.Add(CreateEnumListItem("Ineligible", GusRiskEvaluation.Ineligible));
            ddl.Items.Add(CreateEnumListItem("Unable to Determine", GusRiskEvaluation.UnableToDetermine));
        }

        public static void Bind_IncomeType(HtmlSelect select)
        {
            var items = select.Items;

            items.Add(CreateEnumListItem("Base Income", IncomeType.BaseIncome));
            items.Add(CreateEnumListItem("Overtime", IncomeType.Overtime));
            items.Add(CreateEnumListItem("Bonuses", IncomeType.Bonuses));
            items.Add(CreateEnumListItem("Commission", IncomeType.Commission));
            items.Add(CreateEnumListItem("Dividends Or Interest", IncomeType.DividendsOrInterest));
            items.Add(CreateEnumListItem("Other", IncomeType.Other));
            items.Add(CreateEnumListItem("Military Base Pay", IncomeType.MilitaryBasePay));
            items.Add(CreateEnumListItem("Military Rations Allowance", IncomeType.MilitaryRationsAllowance));
            items.Add(CreateEnumListItem("Military Flight Pay", IncomeType.MilitaryFlightPay));
            items.Add(CreateEnumListItem("Military Hazard Pay", IncomeType.MilitaryHazardPay));
            items.Add(CreateEnumListItem("Military Clothes Allowance", IncomeType.MilitaryClothesAllowance));
            items.Add(CreateEnumListItem("Military Quarters Allowance", IncomeType.MilitaryQuartersAllowance));
            items.Add(CreateEnumListItem("Military Prop Pay", IncomeType.MilitaryPropPay));
            items.Add(CreateEnumListItem("Military Overseas Pay", IncomeType.MilitaryOverseasPay));
            items.Add(CreateEnumListItem("Military Combat Pay", IncomeType.MilitaryCombatPay));
            items.Add(CreateEnumListItem("Military Variable Housing Allowance", IncomeType.MilitaryVariableHousingAllowance));
            items.Add(CreateEnumListItem("Alimony / Child Support", IncomeType.AlimonyChildSupport));
            items.Add(CreateEnumListItem("Notes Receivable Installment", IncomeType.NotesReceivableInstallment));
            items.Add(CreateEnumListItem("Pension Retirement", IncomeType.PensionRetirement));
            items.Add(CreateEnumListItem("Social Security Disability", IncomeType.SocialSecurityDisability));
            items.Add(CreateEnumListItem("Real Estate Mortgage Differential", IncomeType.RealEstateMortgageDifferential));
            items.Add(CreateEnumListItem("Trust", IncomeType.Trust));
            items.Add(CreateEnumListItem("Unemployment / Welfare", IncomeType.UnemploymentWelfare));
            items.Add(CreateEnumListItem("Automobile Expense Account", IncomeType.AutomobileExpenseAccount));
            items.Add(CreateEnumListItem("Foster Care", IncomeType.FosterCare));
            items.Add(CreateEnumListItem("VA Benefits (non-education)", IncomeType.VABenefitsNonEducation));
            items.Add(CreateEnumListItem("Capital Gains", IncomeType.CapitalGains));
            items.Add(CreateEnumListItem("Employment Related Assets", IncomeType.EmploymentRelatedAssets));
            items.Add(CreateEnumListItem("Foreign Income", IncomeType.ForeignIncome));
            items.Add(CreateEnumListItem("Royalty Payment", IncomeType.RoyaltyPayment));
            items.Add(CreateEnumListItem("Seasonal Income", IncomeType.SeasonalIncome));
            items.Add(CreateEnumListItem("Temporary Leave", IncomeType.TemporaryLeave));
            items.Add(CreateEnumListItem("Tip Income", IncomeType.TipIncome));
            items.Add(CreateEnumListItem("Boarder Income", IncomeType.BoarderIncome));
            items.Add(CreateEnumListItem("Mortgage Credit Certificate", IncomeType.MortgageCreditCertificate));
            items.Add(CreateEnumListItem("Trailing Co-Borrower Income", IncomeType.TrailingCoBorrowerIncome));
            items.Add(CreateEnumListItem("Accessory Unit Income", IncomeType.AccessoryUnitIncome));
            items.Add(CreateEnumListItem("Non-Borrower Household Income", IncomeType.NonBorrowerHouseholdIncome));
            items.Add(CreateEnumListItem("Housing Choice Voucher", IncomeType.HousingChoiceVoucher));
            items.Add(CreateEnumListItem("Social Security", IncomeType.SocialSecurity));
            items.Add(CreateEnumListItem("Disability", IncomeType.Disability));
            items.Add(CreateEnumListItem("Alimony", IncomeType.Alimony));
            items.Add(CreateEnumListItem("Child Support", IncomeType.ChildSupport));
            items.Add(CreateEnumListItem("Contract Basis", IncomeType.ContractBasis));
            items.Add(CreateEnumListItem("Defined Contribution Plan", IncomeType.DefinedContributionPlan));
            items.Add(CreateEnumListItem("Housing Allowance", IncomeType.HousingAllowance));
            items.Add(CreateEnumListItem("Miscellaneous Income", IncomeType.MiscellaneousIncome));
            items.Add(CreateEnumListItem("Public Assistance", IncomeType.PublicAssistance));
            items.Add(CreateEnumListItem("Workers' Compensation", IncomeType.WorkersCompensation));
        }        

		static public void Bind_AssetT(DropDownList ddl, bool useUladAssets = false)
		{
            Bind_AssetT(ddl.Items, useUladAssets);
        }

        public static void Bind_AssetT(HtmlSelect select, bool useUladAssets)
        {
            Bind_AssetT(select.Items, useUladAssets);
        }

        private static void Bind_AssetT(ListItemCollection items, bool useUladAssets)
        {
            items.Add(CreateEnumListItem("Auto", E_AssetT.Auto));
            items.Add(CreateEnumListItem("Bonds", E_AssetT.Bonds));
            items.Add(CreateEnumListItem("Bridge Loan", E_AssetT.BridgeLoanNotDeposited));

            if (useUladAssets)
            {
                items.Add(CreateEnumListItem("Business", E_AssetT.Business));
            }

            if (useUladAssets)
            {
                items.Add(CreateEnumListItem("Cash Deposit", E_AssetT.CashDeposit));
            }

            items.Add(CreateEnumListItem("Certificate Of Deposit", E_AssetT.CertificateOfDeposit));
            items.Add(CreateEnumListItem("Checking", E_AssetT.Checking));

            if (useUladAssets)
            {
                items.Add(CreateEnumListItem("Employer Assistance", E_AssetT.EmployerAssistance));
            }

            items.Add(CreateEnumListItem("Gift Funds", E_AssetT.GiftFunds));
            items.Add(CreateEnumListItem("Gift Of Equity", E_AssetT.GiftEquity));

            if (useUladAssets)
            {
                items.Add(CreateEnumListItem("Grant", E_AssetT.Grant));
                items.Add(CreateEnumListItem("Individual Development Account", E_AssetT.IndividualDevelopmentAccount));
                items.Add(CreateEnumListItem("Life Insurance", E_AssetT.LifeInsurance));
            }

            items.Add(CreateEnumListItem("Money Market Fund", E_AssetT.MoneyMarketFund));
            items.Add(CreateEnumListItem("Mutual Funds", E_AssetT.MutualFunds));
            items.Add(CreateEnumListItem("Other Liquid Asset (other bank accounts, etc.)", E_AssetT.OtherLiquidAsset));
            items.Add(CreateEnumListItem("Other Non-liquid Asset (furniture, jewelry, etc.)", E_AssetT.OtherIlliquidAsset));

            if (useUladAssets)
            {
                items.Add(CreateEnumListItem("Other Purchase Credit", E_AssetT.OtherPurchaseCredit));
            }

            items.Add(CreateEnumListItem("Pending Net Sale Proceeds", E_AssetT.PendingNetSaleProceedsFromRealEstateAssets));

            if (useUladAssets)
            {
                items.Add(CreateEnumListItem("Pending Net Sale Proceeds Non-Real Estate", E_AssetT.ProceedsFromSaleOfNonRealEstateAsset));
                items.Add(CreateEnumListItem("Rent Credit", E_AssetT.LeasePurchaseCredit));
                items.Add(CreateEnumListItem("Retirement", E_AssetT.Retirement));
            }

            items.Add(CreateEnumListItem("Savings", E_AssetT.Savings));
            items.Add(CreateEnumListItem("Secured Borrowed Funds", E_AssetT.SecuredBorrowedFundsNotDeposit));

            if (useUladAssets)
            {
                items.Add(CreateEnumListItem("Stock Options", E_AssetT.StockOptions));
            }

            items.Add(CreateEnumListItem("Stocks", E_AssetT.Stocks));

            if (useUladAssets)
            {
                items.Add(CreateEnumListItem("Sweat Equity", E_AssetT.SweatEquity));
                items.Add(CreateEnumListItem("Trade Equity", E_AssetT.TradeEquityFromPropertySwap));
            }

            items.Add(CreateEnumListItem("Trust Funds", E_AssetT.TrustFunds));

            if (useUladAssets)
            {
                items.Add(CreateEnumListItem("Unsecured Borrowed Funds", E_AssetT.ProceedsFromUnsecuredLoan));
            }
        }

        static public void Bind_sOptionArmMinPayOptionT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Teaser Rate", E_sOptionArmMinPayOptionT.TeaserRate));
            ddl.Items.Add(CreateEnumListItem("Discount from Fully Amortizing Payment", E_sOptionArmMinPayOptionT.ByDiscountPmt));
            ddl.Items.Add(CreateEnumListItem("Discount from Note Rate", E_sOptionArmMinPayOptionT.ByDiscountNoteIR));
        }
        static public void Bind_aVaVestTitleT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_aVaVestTitleT.Blank));
            ddl.Items.Add(CreateEnumListItem("Veteran", E_aVaVestTitleT.Veteran));
            ddl.Items.Add(CreateEnumListItem("Veteran & Spouse", E_aVaVestTitleT.VeteranAndSpouse));
            ddl.Items.Add(CreateEnumListItem("Other (Specify)", E_aVaVestTitleT.Other));

        }
        static public void Bind_TriState(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_TriState.Blank));
            ddl.Items.Add(CreateEnumListItem("Yes", E_TriState.Yes));
            ddl.Items.Add(CreateEnumListItem("No", E_TriState.No));
        }
        static public void Bind_sVaLienPosT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_sVaLienPosT.Blank));
            ddl.Items.Add(CreateEnumListItem("First Realty Mortgage", E_sVaLienPosT.First));
            ddl.Items.Add(CreateEnumListItem("Second Realty Mortgage", E_sVaLienPosT.Second));
            ddl.Items.Add(CreateEnumListItem("First Chattel Mortgage", E_sVaLienPosT.FirstChattel));
            ddl.Items.Add(CreateEnumListItem("Unsecured", E_sVaLienPosT.Unsecured));
            ddl.Items.Add(CreateEnumListItem("Other -", E_sVaLienPosT.Other));
        }
        static public void Bind_sVaEstateHeldT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_sVaEstateHeldT.Blank));
            ddl.Items.Add(CreateEnumListItem("Fee Simple", E_sVaEstateHeldT.FeeSimple));
            ddl.Items.Add(CreateEnumListItem("Lease Hold", E_sVaEstateHeldT.LeaseHold));
            ddl.Items.Add(CreateEnumListItem("Other", E_sVaEstateHeldT.Other));
        }
        static public void Bind_sVaLProceedDepositT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_sVaLProceedDepositT.Blank));
            ddl.Items.Add(CreateEnumListItem("Escrow", E_sVaLProceedDepositT.Escrow));
            ddl.Items.Add(CreateEnumListItem("Earmarked Account", E_sVaLProceedDepositT.EarmarkedAcc));
        }
        static public void Bind_sRAdjRoundT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("normal", E_sRAdjRoundT.Normal));
            ddl.Items.Add(CreateEnumListItem("up", E_sRAdjRoundT.Up));
            ddl.Items.Add(CreateEnumListItem("down", E_sRAdjRoundT.Down));
        }
        static public void Bind_sRAdjFloorCalcT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Margin + Fixed Percent", RateAdjFloorCalcT.MarginFixedPercent));
            ddl.Items.Add(CreateEnumListItem("Start Rate + Fixed Percent", RateAdjFloorCalcT.StartRateFixedPercent));
            ddl.Items.Add(CreateEnumListItem("0% + Fixed Percent", RateAdjFloorCalcT.ZeroFixedPercent));
            ddl.Items.Add(CreateEnumListItem("Set Manually", RateAdjFloorCalcT.SetManually));
        }
        static public string GetsRAdjFloorCalcTLabel(RateAdjFloorCalcT value)
        {
            if (value == RateAdjFloorCalcT.MarginFixedPercent)
            {
                return "Margin";
            }
            else if (value == RateAdjFloorCalcT.StartRateFixedPercent)
            {
                return "Start Rate";
            }
            else if (value == RateAdjFloorCalcT.ZeroFixedPercent)
            {
                return "0%";
            }
            else
            {
                return string.Empty;
            }
        }
        static public void Bind_sRAdjFloorBaseT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Margin", E_sRAdjFloorBaseT.Margin));
            ddl.Items.Add(CreateEnumListItem("Start Rate", E_sRAdjFloorBaseT.StartRate));
            ddl.Items.Add(CreateEnumListItem("0%", E_sRAdjFloorBaseT.ZeroPercent));
        }
        static public void Bind_sArmIndexT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_sArmIndexT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Weekly Average CMT", E_sArmIndexT.WeeklyAvgCMT));
            ddl.Items.Add(CreateEnumListItem("Monthly Average CMT", E_sArmIndexT.MonthlyAvgCMT));
            ddl.Items.Add(CreateEnumListItem("Weekly Average TAAI", E_sArmIndexT.WeeklyAvgTAAI));
            ddl.Items.Add(CreateEnumListItem("Weekly Average TAABD", E_sArmIndexT.WeeklyAvgTAABD));
            ddl.Items.Add(CreateEnumListItem("Weekly Average SMTI", E_sArmIndexT.WeeklyAvgSMTI));
            ddl.Items.Add(CreateEnumListItem("Daily CD Rate", E_sArmIndexT.DailyCDRate));
            ddl.Items.Add(CreateEnumListItem("Weekly Average CD Rate", E_sArmIndexT.WeeklyAvgCDRate));
            ddl.Items.Add(CreateEnumListItem("Weekly Average Prime Rate", E_sArmIndexT.WeeklyAvgPrimeRate));
            ddl.Items.Add(CreateEnumListItem("T-Bill Daily Value", E_sArmIndexT.TBillDailyValue));
            ddl.Items.Add(CreateEnumListItem("11th District COF", E_sArmIndexT.EleventhDistrictCOF));
            ddl.Items.Add(CreateEnumListItem("National Monthly Median Cost of Funds", E_sArmIndexT.NationalMonthlyMedianCostOfFunds));
            ddl.Items.Add(CreateEnumListItem("Wall Street Journal LIBOR", E_sArmIndexT.WallStreetJournalLIBOR));
            ddl.Items.Add(CreateEnumListItem("Fannie Mae LIBOR", E_sArmIndexT.FannieMaeLIBOR));

        }
        static public void Bind_sFannieSpT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Leave Blank", E_sFannieSpT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Detached", E_sFannieSpT.Detached));
            ddl.Items.Add(CreateEnumListItem("Attached", E_sFannieSpT.Attached));
            ddl.Items.Add(CreateEnumListItem("Condo", E_sFannieSpT.Condo));
            ddl.Items.Add(CreateEnumListItem("PUD", E_sFannieSpT.PUD));
            ddl.Items.Add(CreateEnumListItem("Co-Op", E_sFannieSpT.CoOp));
            ddl.Items.Add(CreateEnumListItem("High Rise Condo", E_sFannieSpT.HighRiseCondo));
            ddl.Items.Add(CreateEnumListItem("Manufactured", E_sFannieSpT.Manufactured));
            ddl.Items.Add(CreateEnumListItem("Detached Condo", E_sFannieSpT.DetachedCondo));
            ddl.Items.Add(CreateEnumListItem("Manufactured home/condo/pud/coop", E_sFannieSpT.ManufacturedCondoPudCoop));

        }
        static public void Bind_sFannieDocT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Leave Blank", E_sFannieDocT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Alternative", E_sFannieDocT.Alternative));
            ddl.Items.Add(CreateEnumListItem("Full", E_sFannieDocT.Full));
            ddl.Items.Add(CreateEnumListItem("No Documentation", E_sFannieDocT.NoDocumentation));
            ddl.Items.Add(CreateEnumListItem("Reduced", E_sFannieDocT.Reduced));
            ddl.Items.Add(CreateEnumListItem("Streamlined Refinance", E_sFannieDocT.StreamlinedRefinanced));
            ddl.Items.Add(CreateEnumListItem("No Ratio", E_sFannieDocT.NoRatio));
            ddl.Items.Add(CreateEnumListItem("Limited Documentation", E_sFannieDocT.LimitedDocumentation));
            ddl.Items.Add(CreateEnumListItem("No Income, No Employment and No Assets on 1003", E_sFannieDocT.NoIncomeNoEmploymentNoAssets));
            ddl.Items.Add(CreateEnumListItem("No Income and No Assets on 1003", E_sFannieDocT.NoIncomeNoAssets));
            ddl.Items.Add(CreateEnumListItem("No Assets on 1003", E_sFannieDocT.NoAssets));
            ddl.Items.Add(CreateEnumListItem("No Income and No Employment on 1003", E_sFannieDocT.NoIncomeNoEmployment));
            ddl.Items.Add(CreateEnumListItem("No Income on 1003", E_sFannieDocT.NoIncome));
            ddl.Items.Add(CreateEnumListItem("No Verification of Stated Income, Employment or Assets", E_sFannieDocT.NoVerificationStatedIncomeEmploymentAssets));
            ddl.Items.Add(CreateEnumListItem("No Verification of Stated Income or Assets", E_sFannieDocT.NoVerificationStatedIncomeAssets));
            ddl.Items.Add(CreateEnumListItem("No Verification of Stated Assets", E_sFannieDocT.NoVerificationStatedAssets));
            ddl.Items.Add(CreateEnumListItem("No Verification of Stated Income or Employment", E_sFannieDocT.NoVerificationStatedIncomeEmployment));
            ddl.Items.Add(CreateEnumListItem("No Verification of Stated Income", E_sFannieDocT.NoVerificationStatedIncome));
            ddl.Items.Add(CreateEnumListItem("Verbal Verification of Employment (VVOE)", E_sFannieDocT.VerbalVOE));
            ddl.Items.Add(CreateEnumListItem("One paystub", E_sFannieDocT.OnePaystub));
            ddl.Items.Add(CreateEnumListItem("One paystub and VVOE", E_sFannieDocT.OnePaystubAndVerbalVOE));
            ddl.Items.Add(CreateEnumListItem("One paystub and one W-2 and VVOE or one yr 1040", E_sFannieDocT.OnePaystubOneW2VerbalVOE));
        }
        static public void Bind_sTemplatePublishLevelT(ListControl ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Individual - Only person assign to this template.", E_sTemplatePublishLevelT.Individual));
            ddl.Items.Add(CreateEnumListItem("Branch - Only person in the same branch.", E_sTemplatePublishLevelT.Branch));
            ddl.Items.Add(CreateEnumListItem("Corporate - Everyone.", E_sTemplatePublishLevelT.Corporate));
        }
        static public void Bind_sTemplateAssignT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Leave Blank", E_sTemplateAssignT.LeaveEmpty));
            ddl.Items.Add(CreateEnumListItem("Loan Creator", E_sTemplateAssignT.LoanCreator));
            ddl.Items.Add(CreateEnumListItem("Specific Person", E_sTemplateAssignT.SpecificPerson));
        }

        public static void Bind_AusOption(DropDownList ddl, bool skipSellerCredentials)
        {
            foreach (AusOption option in Enum.GetValues(typeof(AusOption)))
            {
                if (option == AusOption.LpaSellerCredential && skipSellerCredentials)
                {
                    continue;
                }

                ddl.Items.Add(CreateEnumListItem(option.GetDescription(), option));
            }
        }

        public static void Bind_UcdDeliveryTargetOption(DropDownList ddl, bool skipSellerCredentials)
        {
            foreach (UcdDeliveryTargetOption option in Enum.GetValues(typeof(UcdDeliveryTargetOption)))
            {
                if (option == UcdDeliveryTargetOption.LcaSellerCredential && skipSellerCredentials)
                {
                    continue;
                }

                ddl.Items.Add(CreateEnumListItem(option.GetDescription(), option));
            }
        }

        public static void Bind_DigitalMortgage(DropDownList ddl)
        {
            foreach (DigitalMortgageProvider option in Enum.GetValues(typeof(DigitalMortgageProvider)))
            {
                ddl.Items.Add(CreateEnumListItem(option.GetDescription(), option));
            }
        }

        static public void Bind_sSpCounty(string state, DropDownList county, bool addEmpty)
        {
            county.EnableViewState = false;
            county.DataSource = getCountiesList(state, addEmpty);
            county.DataBind();
        }

        static public void Bind_sSpCounty(string state, HtmlSelect county, bool addEmpty)
        {
            county.EnableViewState = false;
            county.DataSource = getCountiesList(state, addEmpty);
            county.DataBind();
        }

        static private ArrayList getCountiesList(string state, bool addEmpty)
        {
            if (state == null || state == string.Empty)
            {
                return new ArrayList();
            }

            ArrayList list = StateInfo.Instance.GetCountiesFor(state);
            if (addEmpty)
            {
                list.Insert(0, string.Empty);
        }

            return list;
        }

        /// <summary>
        /// Replaces only words (separated by spaces) in the string.  If the word doesn't match exactly, it doesn't replace it.
        /// Ex: ReplaceWord("this was a mistake", "MISTAKE", true, "good idea") returns "this was a good idea"
        /// </summary>
        public static string ReplaceWord(string initialString, string wordToBeReplaced, bool ignoreCase, string replacement)
        {
            var words = initialString.Split(' ');

            for (int i = 0; i < words.Length; i++)
            {
                if (string.Compare(words[i], wordToBeReplaced, ignoreCase) == 0)
                {
                    words[i] = replacement;
                }
            }
            return string.Join(" ", words);
        }

        /// <summary>
        /// Applies the metadata from <paramref name="document"/> to <paramref name="imgTag"/> for display in the <c>LendersOfficeApp</c> UI.
        /// </summary>
        /// <param name="imgTag">The &lt;img&gt; tag in the UI to display the metadata on.</param>
        /// <param name="document">The document to display.</param>
        /// <remarks>
        /// This was implemented for Case 463715, and is intended as a simple, reusable tooltip for displaying details about a document. In order
        /// to use it, ensure:
        ///   1. jQuery UI (js + css) are registered on the page (it was built against 1.11 on <c>GenerateUcd.aspx</c>).
        ///   2. The correct jQuery UI widget is registered ("widgets/jquery.docmetadatatooltip.js").
        ///   3. The start up js initializes the widget with <c>$('img[data-docmetadata]').docmetadatatooltip();</c>.
        /// </remarks>
        public static void ApplyDocMetaDataTooltip(HtmlImage imgTag, EDocument document)
        {
            if (document == null)
            {
                return;
            }

            ApplyDocMetaDataTooltip(imgTag, CreateDocMetaDataTooltipDataAttributeValue(document));
        }

        public static void ApplyDocMetaDataTooltip(HtmlImage imgTag, string edocsMetaData)
        {
            imgTag.Visible = true;
            imgTag.Src = ((BasePage)imgTag.Page).VirtualRoot + "/images/edocs/view.png";
            imgTag.Alt = "Info";
            imgTag.Attributes.Add("data-docmetadata", edocsMetaData);
        }

        /// <summary>
        /// Creates the string value for the <c>data-docmetadata</c> attribute for an document metadata tooltip in the <c>LendersOfficeApp</c> UI.
        /// </summary>
        /// <param name="document">The document to display.</param>
        /// <returns>The value to assign as the value of the the <c>data-docmetadata</c> attribute.</returns>
        /// <remarks>
        /// Ideally, this method would be swallowed into <see cref="ApplyDocMetaDataTooltip(HtmlImage, EDocument)"/>.  Unfortunately, however, we need
        /// to display this control on pages with dynamic sets of documents, loaded through Angular in this case, so our best option is to teach the
        /// page some of the guts of this widget.
        /// </remarks>
        public static string CreateDocMetaDataTooltipDataAttributeValue(EDocument document)
        {
            var docMetaDataToDisplay = new Dictionary<string, object>()
            {
                [string.Empty] = new { rawHtml = LendersOffice.AntiXss.AspxTools.HtmlControls(GetDocumentIcons(document)) },
                ["Status"] = new { rawHtml = LendersOffice.AntiXss.AspxTools.HtmlControl(GetDocumentStatusControl(document.DocStatus, document.StatusDescription)) },
                ["Folder"] = document.Folder.FolderNm,
                ["Doc Type"] = document.DocTypeName,
                ["Borrower"] = document.AppName,
                ["Description"] = document.PublicDescription,
                ["Comments"] = document.InternalDescription,
                ["Last Modified"] = document.LastModifiedDate.ToString()
            };
            return SerializationHelper.JsonNetAnonymousSerialize(docMetaDataToDisplay);
        }

        /// <summary>
        /// Gets the icons to display for the specified document in the <c>LendersOfficeApp</c> UI.
        /// </summary>
        /// <param name="document">The document to inspect to determine which icons to show.</param>
        /// <param name="genericDocumentLookup">A cache of possible linked EDocs to prevent having to hit the database to load the generic version.</param>
        /// <returns>A list of the icons to show for the specified document.</returns>
        public static IEnumerable<HtmlImage> GetDocumentIcons(EDocument document, IReadOnlyDictionary<Guid, GenericEDocument> genericDocumentLookup = null)
        {
            var icons = new List<HtmlImage>();
            if (document.IsESigned)
            {
                icons.Add(new HtmlImage() { Src = "~/images/edocs/SigIcon.png" });
            }

            GenericEDocument xmlDocument;
            if (genericDocumentLookup != null)
            {
                xmlDocument = genericDocumentLookup.GetValueOrNull(document.DocumentId);
            }
            else
            {
                xmlDocument = GenericEDocument.GetDocument(document.BrokerId, document.DocumentId);
            }

            if (xmlDocument != null)
            {
                icons.AddRange(GetDocumentIcons(xmlDocument));
            }

            if (document.HasInternalAnnotations)
            {
                var image = new HtmlImage() { Src = "~/images/edocs/NoteIcon2.png" };
                image.Attributes.Add("title", "This document contains internal notes. To access them select the action link, then open editor.");
                icons.Add(image);
            }

            return icons;
        }

        /// <summary>
        /// Gets the icons to display for the specified document in the <c>LendersOfficeApp</c> UI.
        /// </summary>
        /// <param name="xmlDoc">The document to inspect to determine which icons to show.</param>
        /// <returns>A list of the icons to show for the specified document.</returns>
        public static IEnumerable<HtmlImage> GetDocumentIcons(GenericEDocument xmlDoc)
        {
            var icons = new List<HtmlImage>();
            if (xmlDoc.FileType == E_FileType.UniformClosingDataset)
            {
                icons.Add(new HtmlImage() { Src = "~/images/edocs/UCDIcon.png" });
            }
            else
            {
                icons.Add(new HtmlImage() { Src = "~/images/edocs/XMLIcon.png" });
            }

            return icons;
        }

        /// <summary>
        /// Gets the control containing a document's status information to display in the <c>LendersOfficeApp</c> UI.
        /// </summary>
        /// <param name="status">The document's status.</param>
        /// <param name="description">The document status description.</param>
        /// <returns>A control indicating the status.</returns>
        public static HtmlControl GetDocumentStatusControl(E_EDocStatus status, string description)
        {
            var span = new HtmlGenericControl("span");
            span.Style.Add(HtmlTextWriterStyle.Color, GetDocumentStatusColor(status));
            var b = new HtmlGenericControl("b");
            b.Controls.Add(new EncodedLiteral()
            {
                Text = EDocument.StatusText(status)
            });
            if (status == E_EDocStatus.Rejected)
            {
                b.Controls.Add(new PassthroughLiteral() { Text = "<br />" });
                b.Controls.Add(new EncodedLiteral()
                {
                    Text = description
                });
            }

            span.Controls.Add(b);
            return span;
        }

        /// <summary>
        /// Gets the color of the document status indicator in the <c>LendersOfficeApp</c> UI for the specified status.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <returns>The color as a hex string for the UI.</returns>
        private static string GetDocumentStatusColor(E_EDocStatus status)
        {
            if (status == E_EDocStatus.Obsolete || status == E_EDocStatus.Rejected)
            {
                return "#FF0000";
            }
            else if (status == E_EDocStatus.Screened)
            {
                return "#004080";
            }

            return "#008000";
        }

        /// <summary>
        /// Binds all of an enum's values to a <see cref="ListControl"/>, most commonly a <see cref="DropDownList"/>.
        /// If the values of the Enum type <typeparamref name="T"/> have <see cref="OrderedDescriptionAttribute"/> or <see cref="DescriptionAttribute"/> attributes defined, 
        ///     those values will be used for the display name of the value.
        /// If no description is given, the name of the Enum value will be used.
        /// </summary>
        /// <typeparam name="T">The type of the enum to bind.</typeparam>
        /// <param name="ddl">The control to bind.</param>
        /// <param name="values">The specific values to bind in order. If not specified or null, binds all the values of the enum in default order.</param>
        public static void BindGenericEnum<T>(ListControl ddl, IEnumerable<T> values = null) where T : struct, IConvertible
        {
            if (!typeof(T).IsEnum)
            {
                throw new InvalidOperationException($"Type {typeof(T).FullName} is not an Enum type.");
            }

            IEnumerable<Enum> orderedValues = values?.Cast<Enum>() ?? Enum.GetValues(typeof(T)).Cast<Enum>();

            foreach (var value in orderedValues)
            {
                var type = value.GetType();
                var memInfo = type.GetMember(value.ToString());
                DescriptionAttribute descriptionAttr = 
                    (OrderedDescriptionAttribute)memInfo[0].GetCustomAttribute(typeof(OrderedDescriptionAttribute)) 
                    ?? (DescriptionAttribute)memInfo[0].GetCustomAttribute(typeof(DescriptionAttribute));
                string description = descriptionAttr?.Description ?? Enum.GetName(typeof(T), value);

                ddl.Items.Add(new ListItem(description, ((int)Convert.ChangeType(value, value.GetTypeCode())).ToString()));
            }
        }

        public static CommonProjectLib.Common.Lib.ReadOnlyDictionary<string, string> GetEnumAsDictionaryByName(Type tEnum)
        {
            if (tEnum == null)
            {
                throw new ArgumentNullException("Cannot convert null type to dictionary.");
            }

            if (!tEnum.IsEnum)
            {
                throw new ArgumentException(string.Format("Type {0} is not an enumerated type", tEnum.FullName));
            }

            var enumValues = Enum.GetValues(tEnum);
            var tempDict = new Dictionary<string, string>(enumValues.Length);

            foreach (var value in enumValues)
            {
                tempDict.Add(
                    Enum.Format(tEnum, value, "G"),
                    Enum.Format(tEnum, value, "D"));
            }

            return new CommonProjectLib.Common.Lib.ReadOnlyDictionary<string, string>(tempDict);
        }

        public static bool HasNonInheritedFlagsAttribute(Type t)
        {
            if (t == null)
            {
                return false;
            }

            return t.IsDefined(typeof(System.FlagsAttribute), false);
        }

        public static CommonProjectLib.Common.Lib.ReadOnlyDictionary<string, string> GetEnumAsDictionaryByValue(Type tEnum)
        {
            if (tEnum == null)
            {
                throw new ArgumentNullException("Cannot convert null type to dictionary.");
            }

            if (!tEnum.IsEnum)
            {
                throw new ArgumentException(string.Format("Type {0} is not an enumerated type", tEnum.FullName));
            }

            if (HasNonInheritedFlagsAttribute(tEnum))
            {
                throw new ArgumentException(string.Format("Type {0} has flags attribute so name by value doesn't make sense.", tEnum.FullName));
            }

            var enumValues = Enum.GetValues(tEnum);
            var tempDict = new Dictionary<string, string>(enumValues.Length);

            foreach (var value in enumValues)
            {
                tempDict.Add(
                    Enum.Format(tEnum, value, "D"),
                    Enum.Format(tEnum, value, "G"));
            }

            return new CommonProjectLib.Common.Lib.ReadOnlyDictionary<string, string>(tempDict);
        }

        static public void SetDropDownListCaseInsensitive(DropDownList ddl, string val)
        {
            SetDropDownListCaseInsensitiveImpl(ddl.Items, val);
        }

        static public void SetDropDownListCaseInsensitive(HtmlSelect select, string val)
            {
            SetDropDownListCaseInsensitiveImpl(select.Items, val);
        }

        static private void SetDropDownListCaseInsensitiveImpl(ListItemCollection options, string val)
        {
            foreach (ListItem item in options)
            {
                if (val.ToLower() == item.Value.ToLower())
                {
                    item.Selected = true;
                    break;
                }
            }
            return;
        }

        /// <summary>
        /// Tristate UI, Yes, No & Blank will be using Zero or One Checkboxlist. These are helper functions for bind/get/set CheckboxList
        /// </summary>
        /// <param name="cbl"></param>
        static public void Bind_TriState(CheckBoxList cbl)
        {
            cbl.Items.Add(CreateEnumListItem("Yes", E_TriState.Yes));
            cbl.Items.Add(CreateEnumListItem("No", E_TriState.No));
        }
        static public void Set_TriState(CheckBoxList cbl, E_TriState value)
        {
            SetCBUsingEnum(cbl, value);
        }

        public static void Set_ListControl(ListControl list, Enum value)
        {
            string v = value.ToString("D");
            foreach (ListItem o in list.Items)
            {
                if (o.Value == v)
                {
                    o.Selected = true;
                }
            }
        }
        static public E_TriState Get_TriState(ListControl cbl)
        {
            E_TriState ret = E_TriState.Blank;
            foreach (ListItem o in cbl.Items)
            {
                if (o.Selected)
                {
                    if (o.Value == E_TriState.Yes.ToString("D"))
                    {
                        ret = E_TriState.Yes;
                    }
                    else if (o.Value == E_TriState.No.ToString("D"))
                    {
                        ret = E_TriState.No;
                    }
                }
            }
            return ret;
        }

        public static void Bind_DebtT(DropDownList ddl)
        {
            Bind_DebtT(ddl.Items, useUladDebtTypes: false);
        }

        public static void Bind_DebtT(HtmlSelect ddl, bool useUladDebtTypes)
        {
            Bind_DebtT(ddl.Items, useUladDebtTypes);
        }

        private static void Bind_DebtT(ListItemCollection items, bool useUladDebtTypes)
        {
            if (useUladDebtTypes)
            {
                items.Add(CreateEnumListItem("Alimony", E_DebtT.Alimony));
                items.Add(CreateEnumListItem("Child Support", E_DebtT.ChildSupport));
                items.Add(CreateEnumListItem("Separate Maintenance", E_DebtT.SeparateMaintenance));
                items.Add(CreateEnumListItem("Job Expense", E_DebtT.JobRelatedExpense));
            }

            items.Add(CreateEnumListItem("Installment", E_DebtT.Installment));
            items.Add(CreateEnumListItem("Mortgage", E_DebtT.Mortgage));
            items.Add(CreateEnumListItem("Open", E_DebtT.Open));
            items.Add(CreateEnumListItem("Revolving", E_DebtT.Revolving));

            if (useUladDebtTypes)
            {
                items.Add(CreateEnumListItem("HELOC", E_DebtT.Heloc));
                items.Add(CreateEnumListItem("Lease", E_DebtT.Lease));
                items.Add(CreateEnumListItem("Other Debt Liability", E_DebtT.Other));
                items.Add(CreateEnumListItem("Other Expense Liability", E_DebtT.OtherExpenseLiability));
            }
            else
            {
                items.Add(CreateEnumListItem("Other", E_DebtT.Other));
            }
        }

        static public void Bind_EmplmtStat(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Current", E_EmplmtStat.Current));
            ddl.Items.Add(CreateEnumListItem("Previous", E_EmplmtStat.Previous));
        }

        static public void Bind_sMortgageLoanT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Government (FHA/VA/RHS)", E_MortgageLoanT.Government_FHA_VA_RHS));
            ddl.Items.Add(CreateEnumListItem("Prime Conforming", E_MortgageLoanT.PrimeConforming));
            ddl.Items.Add(CreateEnumListItem("Prime Non-Conforming", E_MortgageLoanT.PrimeNonConforming));
            ddl.Items.Add(CreateEnumListItem("Other 1-4 Unit Residential", E_MortgageLoanT.Other1To4UnitResidential));
            ddl.Items.Add(CreateEnumListItem("Closed-End Second", E_MortgageLoanT.ClosedEndSecond));
            ddl.Items.Add(CreateEnumListItem("Funded HELOC", E_MortgageLoanT.FundedHELOC));
            ddl.Items.Add(CreateEnumListItem("Construction", E_MortgageLoanT.Construction));
        }

        static public void Bind_sJumboT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Non-Jumbo", E_JumboT.NonJumbo));
            ddl.Items.Add(CreateEnumListItem("Jumbo", E_JumboT.Jumbo));
        }

        static public void Bind_sDocumentationT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Full Doc", E_DocumentationT.FullDoc));
            ddl.Items.Add(CreateEnumListItem("Alt Doc", E_DocumentationT.AltDoc));
        }

        static public void Bind_sNMLSLoanPurposeT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Purchase", E_NMLSLoanPurposeT.Purchase));
            ddl.Items.Add(CreateEnumListItem("Refinance Rate-Term", E_NMLSLoanPurposeT.RefinanceRateTerm));
            ddl.Items.Add(CreateEnumListItem("Refinance Cash-Out Refinance", E_NMLSLoanPurposeT.RefinanceCashOutRefinance));
            ddl.Items.Add(CreateEnumListItem("Refinance Restructure", E_NMLSLoanPurposeT.RefinanceRestructure));
            ddl.Items.Add(CreateEnumListItem("Refinance Other/Unknown", E_NMLSLoanPurposeT.RefinanceOtherUnknown));
        }

        static public void Bind_sOccT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Primary Residence", E_sOccT.PrimaryResidence));
            ddl.Items.Add(CreateEnumListItem("Secondary Residence", E_sOccT.SecondaryResidence));
            ddl.Items.Add(CreateEnumListItem("Investment", E_sOccT.Investment));
        }

        static public void Bind_sFHAPropAcquisitionT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("N/A", FHAPropAcquisitionT.NA));
            ddl.Items.Add(CreateEnumListItem("Less than 12 months prior to Case Number Assignment date", FHAPropAcquisitionT.LessThan12MonthsPriorToCaseNumberAssignmentDate));
            ddl.Items.Add(CreateEnumListItem("Greater than or equal to 12 months prior to Case Number Assignment date", FHAPropAcquisitionT.GreaterThanOrEqualTo12MonthsPriorToCaseNumberAssignmentDate));
            ddl.Items.Add(CreateEnumListItem("Less than 12 months through Inheritance or Gift from Family Member", FHAPropAcquisitionT.LessThan12MonthsThroughInheritanceOrGiftFromFamilyMember));
        }

        static public void Bind_sLoanSaleDispositionT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("<--NONE SELECTED-->", E_LoanSaleDispositionT.Blank));
            ddl.Items.Add(CreateEnumListItem("Sold to Secondary Market Agencies (FNMA, FHLMC, GNMA)", E_LoanSaleDispositionT.SoldToSecondaryMarketAgencies_FNMA_FHLMC_GNMA));
            ddl.Items.Add(CreateEnumListItem("Sold to Other (Non-Affiliate)", E_LoanSaleDispositionT.SoldToOther_NonAffiliate));
            ddl.Items.Add(CreateEnumListItem("Sold to Other (Affiliate)", E_LoanSaleDispositionT.SoldToOther_Affiliate));
            ddl.Items.Add(CreateEnumListItem("Kept in Portfolio/Held for Investment", E_LoanSaleDispositionT.KeptInPortfolio_HeldForInvestment));
            ddl.Items.Add(CreateEnumListItem("Sold through Non-Agency Securitization with Sale Treatment", E_LoanSaleDispositionT.SoldThroughNonAgencySecuritizationWithSaleTreatment));
            ddl.Items.Add(CreateEnumListItem("Sold through Non-Agency Securitization w/o Sale Treatment", E_LoanSaleDispositionT.SoldThroughNonAgencySecuritizationWithOutSaleTreatment));
        }

        static public void Bind_sLeadSrcDesc(BrokerDB broker, DropDownList ddl)
        {
            // 5/13/2014 gf - opm 172380, always include a blank option.
            ddl.Items.Add(new ListItem("", ""));

            foreach (LeadSource desc in broker.LeadSources)
            {
                ddl.Items.Add(new ListItem(desc.Name, desc.Name));
            }
        }

        static public void Bind_sFinMethT(HtmlSelect ddl)
        {
            Bind_sFinMethT(ddl.Items);
        }

        static public void Bind_sFinMethT(DropDownList ddl)
        {
            Bind_sFinMethT(ddl.Items);
        }

        static private void Bind_sFinMethT(ListItemCollection ddlItems)
        {
            ddlItems.Add(CreateEnumListItem("Fixed Rate", E_sFinMethT.Fixed));
            ddlItems.Add(CreateEnumListItem("ARM", E_sFinMethT.ARM));
            ddlItems.Add(CreateEnumListItem("Graduated", E_sFinMethT.Graduated));
        }
        static public void Bind_AmortizationT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Fixed Rate", E_sFinMethT.Fixed));
            ddl.Items.Add(CreateEnumListItem("ARM", E_sFinMethT.ARM));
        }
        static public void Bind_sProRealETxT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Loan Amount", E_PercentBaseT.LoanAmount));
            ddl.Items.Add(CreateEnumListItem("Purchase Price", E_PercentBaseT.SalesPrice));
            ddl.Items.Add(CreateEnumListItem("Appraisal Value", E_PercentBaseT.AppraisalValue));
            ddl.Items.Add(CreateEnumListItem("Original Cost", E_PercentBaseT.OriginalCost));
            ddl.Items.Add(CreateEnumListItem("Total Loan Amount", E_PercentBaseT.TotalLoanAmount));
        }
        static public void Bind_PercentBaseT(DropDownList ddl)
        {
            ddl.AddEnumItem("Loan Amount", E_PercentBaseT.LoanAmount);
            ddl.AddEnumItem("Purchase Price", E_PercentBaseT.SalesPrice);
            ddl.AddEnumItem("Appraisal Value", E_PercentBaseT.AppraisalValue);
            ddl.AddEnumItem("Total Loan Amount", E_PercentBaseT.TotalLoanAmount);
        }
        static public void Bind_TPOLandingPages(DropDownList ddl, IEnumerable<LendersOffice.ObjLib.TPO.TPOPortalConfig> configs)
        {
            ddl.Items.Add(new ListItem("<--None-->", "none"));
            foreach (var config in configs)
            {
                ddl.Items.Add(new ListItem(config.Name, config.ID.Value.ToString()));
            }
        }
        static public void Bind_PercentBaseLoanAmountsT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Loan Amount", E_PercentBaseT.LoanAmount));
            ddl.Items.Add(CreateEnumListItem("Total Loan Amount", E_PercentBaseT.TotalLoanAmount));
        }
        static public void Bind_FloodCertificationDeterminationT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Single Charge or Life of Loan", E_FloodCertificationDeterminationT.SingleChargeOrLifeOfLoan));
            ddl.Items.Add(CreateEnumListItem("Initial Fee", E_FloodCertificationDeterminationT.InitialFee));
        }
        static public void Bind_OrigiantorCompenationSetLevelT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Originating Company", E_OrigiantorCompenationLevelT.OriginatingCompany));
            ddl.Items.Add(CreateEnumListItem("Individual User", E_OrigiantorCompenationLevelT.IndividualUser));
        }
        static public void Bind_sGfeOriginatorCompFBaseT(DropDownList ddl)
        {
            ddl.AddEnumItem("Loan Amount", E_PercentBaseT.LoanAmount);
            ddl.AddEnumItem("Total Loan Amount", E_PercentBaseT.TotalLoanAmount);
            ddl.AddEnumItem("All YSP", E_PercentBaseT.AllYSP);

        }
        static public void Bind_sOriginatorCompensationPaymentSourceT(RadioButtonList rbl)
        {
            rbl.Items.Add(CreateEnumListItem("Borrower    ", E_sOriginatorCompensationPaymentSourceT.BorrowerPaid));
            rbl.Items.Add(CreateEnumListItem("Lender    ", E_sOriginatorCompensationPaymentSourceT.LenderPaid));
            rbl.Items.Add(CreateEnumListItem("Not specified", E_sOriginatorCompensationPaymentSourceT.SourceNotSpecified));
        }
        static public void Bind_DomesticRelationshipType(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem(string.Empty, DomesticRelationshipType.Blank));
            ddl.Items.Add(CreateEnumListItem("Civil Union", DomesticRelationshipType.CivilUnion));
            ddl.Items.Add(CreateEnumListItem("Domestic Partnership", DomesticRelationshipType.DomesticPartnership));
            ddl.Items.Add(CreateEnumListItem("Registered Reciprocal Beneficiary Relationship", DomesticRelationshipType.RegisteredReciprocalBeneficiaryRelationship));
            ddl.Items.Add(CreateEnumListItem("Other", DomesticRelationshipType.Other));
        }
        static public void Bind_booleanRadioButtonList(RadioButtonList rbl)
        {
            rbl.Items.Add(new ListItem("Yes", true.ToString()));
            rbl.Items.Add(new ListItem("No", false.ToString()));
        }
        static public void Bind_sEstateHeldT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Fee Simple", E_sEstateHeldT.FeeSimple));
            ddl.Items.Add(CreateEnumListItem("Lease Hold", E_sEstateHeldT.LeaseHold));
        }
        public static void Bind_sNativeAmericanLandsT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem(string.Empty, NativeAmericanLandsT.Blank));
            ddl.Items.Add(CreateEnumListItem("Fee Simple (On a Reservation)", NativeAmericanLandsT.FeeSimple));
            ddl.Items.Add(CreateEnumListItem("Individual Trust Land (Allotted/Restricted)", NativeAmericanLandsT.IndividualTrustLand));
            ddl.Items.Add(CreateEnumListItem("Tribal Trust Land (On a Reservation)", NativeAmericanLandsT.TribalTrustLandOnReservation));
            ddl.Items.Add(CreateEnumListItem("Tribal Trust Land (Off Reservation)", NativeAmericanLandsT.TribalTrustLandOffReservation));
            ddl.Items.Add(CreateEnumListItem("Alaska Native Corporation Land", NativeAmericanLandsT.AlaskaNativeCorporationLand));
        }
        public static void Bind_sTrustClassificationT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem(string.Empty, TrustClassificationT.Blank));
            ddl.Items.Add(CreateEnumListItem("Living Trust", TrustClassificationT.LivingTrust));
            ddl.Items.Add(CreateEnumListItem("Land Trust", TrustClassificationT.LandTrust));
        }
        static public void Bind_sSpImprovTimeFrameT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_sSpImprovTimeFrameT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("made", E_sSpImprovTimeFrameT.Made));
            ddl.Items.Add(CreateEnumListItem("to be made", E_sSpImprovTimeFrameT.ToBeMade));
        }
        static public void Bind_GenderT(DropDownList ddl, bool shouldIncludeBothOption)
        {
            ddl.Items.Add(CreateEnumListItem("", E_GenderT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Male", E_GenderT.Male));
            ddl.Items.Add(CreateEnumListItem("Female", E_GenderT.Female));
            ddl.Items.Add(CreateEnumListItem("Not Furnished", E_GenderT.Unfurnished));
            ddl.Items.Add(CreateEnumListItem("N/A", E_GenderT.NA));
            if (shouldIncludeBothOption)
            {
                ddl.Items.Add(CreateEnumListItem("Both Male and Female", E_GenderT.MaleAndFemale));
                ddl.Items.Add(CreateEnumListItem("Not Furnished but Male best guess", E_GenderT.MaleAndNotFurnished));
                ddl.Items.Add(CreateEnumListItem("Not Furnished but Female best guess", E_GenderT.FemaleAndNotFurnished));
                ddl.Items.Add(CreateEnumListItem("Not Furnished but Male and Female best guess", E_GenderT.MaleFemaleNotFurnished));
            }

        }
        static public void Bind_aRaceT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_aBRaceT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("American Indian/Alaskan Native", E_aBRaceT.AmericanIndian));
            ddl.Items.Add(CreateEnumListItem("Asian/Packific Islander", E_aBRaceT.Asian));
            ddl.Items.Add(CreateEnumListItem("Black (not Hispanic)", E_aBRaceT.Black));
            ddl.Items.Add(CreateEnumListItem("Hispanic", E_aBRaceT.Hispanic));
            ddl.Items.Add(CreateEnumListItem("White (not Hispanic)", E_aBRaceT.White));
            ddl.Items.Add(CreateEnumListItem("Other", E_aBRaceT.Other));
            ddl.Items.Add(CreateEnumListItem("Not Furnished", E_aBRaceT.NotFurnished));
            ddl.Items.Add(CreateEnumListItem("N/A", E_aBRaceT.NotApplicable));
        }

        static public void Bind_aVServiceT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_aVServiceT.Blank));
            ddl.Items.Add(CreateEnumListItem("Regular Military", E_aVServiceT.Veteran));
            ddl.Items.Add(CreateEnumListItem("Reservist/National Guard", E_aVServiceT.ReservistNationalGuard));
        }

        static public void Bind_aVEnt(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_aVEnt.Blank));
            ddl.Items.Add(CreateEnumListItem("First Time Use", E_aVEnt.FirstTimeUse));
            ddl.Items.Add(CreateEnumListItem("Second And Subsequent Use", E_aVEnt.SecondAndSubsequentUse));
        }

        static public void Bind_aProdBCitizenT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("US Citizen", E_aProdCitizenT.USCitizen));
            ddl.Items.Add(CreateEnumListItem("Permanent Resident", E_aProdCitizenT.PermanentResident));
            ddl.Items.Add(CreateEnumListItem("Non-permanent Resident", E_aProdCitizenT.NonpermanentResident));
            ddl.Items.Add(CreateEnumListItem("Non-Resident Alien (Foreign National)", E_aProdCitizenT.ForeignNational));

        }

        static public void Bind_aProdCCitizenT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("US Citizen", E_aProdCitizenT.USCitizen));
            ddl.Items.Add(CreateEnumListItem("Permanent Resident", E_aProdCitizenT.PermanentResident));
            ddl.Items.Add(CreateEnumListItem("Non-permanent Resident", E_aProdCitizenT.NonpermanentResident));
            ddl.Items.Add(CreateEnumListItem("Non-Resident Alien (Foreign National)", E_aProdCitizenT.ForeignNational));
        }
        static public void Bind_aHispanicT(DropDownList ddl, bool shouldIncludeBothOption)
        {
            ddl.Items.Add(CreateEnumListItem("", E_aHispanicT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Hispanic or Latino", E_aHispanicT.Hispanic));
            ddl.Items.Add(CreateEnumListItem("Not Hispanic or Latino", E_aHispanicT.NotHispanic));
            if (shouldIncludeBothOption)
            {
                ddl.Items.Add(CreateEnumListItem("Both Hispanic or Latino and Not Hispanic or Latino", E_aHispanicT.BothHispanicAndNotHispanic));
            }
        }

        static public void Bind_aOccT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Primary Residence", E_aOccT.PrimaryResidence));
            ddl.Items.Add(CreateEnumListItem("Secondary Residence", E_aOccT.SecondaryResidence));
            ddl.Items.Add(CreateEnumListItem("Investment", E_aOccT.Investment));
        }

        static public void Bind_aIntrvwrMethodT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_aIntrvwrMethodT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("face-to-face interview", E_aIntrvwrMethodT.FaceToFace));
            ddl.Items.Add(Tools.CreateEnumListItem("by mail", E_aIntrvwrMethodT.ByMail));
            ddl.Items.Add(Tools.CreateEnumListItem("by telephone", E_aIntrvwrMethodT.ByTelephone));
            ddl.Items.Add(Tools.CreateEnumListItem("by internet", E_aIntrvwrMethodT.Internet));
        }

        static public void Bind_aDecPastOwnedPropT(DropDownList ddl)
        {
            Bind_aDecPastOwnedPropT(ddl.Items, includeFhaSecondaryResidence: false);
        }

        public static void Bind_aDecPastOwnedPropT(HtmlSelect ddl, bool includeFhaSecondaryResidence)
        {
            Bind_aDecPastOwnedPropT(ddl.Items, includeFhaSecondaryResidence);
        }

        private static void Bind_aDecPastOwnedPropT(ListItemCollection items, bool includeFhaSecondaryResidence)
        {
            items.Add(Tools.CreateEnumListItem(" ", E_aBDecPastOwnedPropT.Empty));
            items.Add(Tools.CreateEnumListItem("PR", E_aBDecPastOwnedPropT.PR));

            if (includeFhaSecondaryResidence)
            {
                items.Add(Tools.CreateEnumListItem("SR", E_aBDecPastOwnedPropT.SR));
            }

            items.Add(Tools.CreateEnumListItem("SH", E_aBDecPastOwnedPropT.SH));
            items.Add(Tools.CreateEnumListItem("IP", E_aBDecPastOwnedPropT.IP));
        }

        static public void Bind_aDecPastOwnedPropTitleT(DropDownList ddl)
        {
            Bind_aDecPastOwnedPropTitleT(ddl.Items);
        }

        public static void Bind_aDecPastOwnedPropTitleT(HtmlSelect ddl)
        {
            Bind_aDecPastOwnedPropTitleT(ddl.Items);
        }

        private static void Bind_aDecPastOwnedPropTitleT(ListItemCollection items)
        {
            items.Add(Tools.CreateEnumListItem(" ", E_aBDecPastOwnedPropTitleT.Empty));
            items.Add(Tools.CreateEnumListItem("S", E_aBDecPastOwnedPropTitleT.S));
            items.Add(Tools.CreateEnumListItem("SP", E_aBDecPastOwnedPropTitleT.SP));
            items.Add(Tools.CreateEnumListItem("O", E_aBDecPastOwnedPropTitleT.O));
        }

        static public void Bind_sSpT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sSpT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Detached Housing", E_sSpT.DetachedHousing));
            ddl.Items.Add(Tools.CreateEnumListItem("Attached Housing", E_sSpT.AttachedHousing));
            ddl.Items.Add(Tools.CreateEnumListItem("Condominium", E_sSpT.Condominium));
            ddl.Items.Add(Tools.CreateEnumListItem("PUD", E_sSpT.PUD));
            ddl.Items.Add(Tools.CreateEnumListItem("CO-OP", E_sSpT.COOP));
        }
        public static void Bind_sHmdaPreapprovalT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sHmdaPreapprovalT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Preapproval was requested", E_sHmdaPreapprovalT.PreapprovalRequested));
            ddl.Items.Add(Tools.CreateEnumListItem("Preapproval was not requested", E_sHmdaPreapprovalT.PreapprovalNotRequested));
            ddl.Items.Add(Tools.CreateEnumListItem("Not applicable", E_sHmdaPreapprovalT.NotApplicable));
        }
        public static void Bind_sHmdaLienT(DropDownList ddl, bool includeNaAndNotSecured)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sHmdaLienT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("First Lien", E_sHmdaLienT.FirstLien));
            ddl.Items.Add(Tools.CreateEnumListItem("Subordinate Lien", E_sHmdaLienT.SubordinateLien));

            if (includeNaAndNotSecured)
            {
                ddl.Items.Add(Tools.CreateEnumListItem("Not secured by a lien", E_sHmdaLienT.NotSecuredByLien));
                ddl.Items.Add(Tools.CreateEnumListItem("Not applicable (purchase loan)", E_sHmdaLienT.NotApplicablePurchaseLoan));
            }
        }
        public static void Bind_sHmdaPropT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sHmdaPropT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("1 - 4 SFR", E_sHmdaPropT.OneTo4Family));
            ddl.Items.Add(Tools.CreateEnumListItem("Manufactured Housing", E_sHmdaPropT.ManufacturedHousing));
            ddl.Items.Add(Tools.CreateEnumListItem("Multifamily", E_sHmdaPropT.MultiFamiliy));
        }
        public static void Bind_sHmdaPurchaser2004T(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sHmdaPurchaser2004T.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Loan was not sold", E_sHmdaPurchaser2004T.LoanWasNotSold));
            ddl.Items.Add(Tools.CreateEnumListItem("FNMA", E_sHmdaPurchaser2004T.FNMA));
            ddl.Items.Add(Tools.CreateEnumListItem("GNMA", E_sHmdaPurchaser2004T.GNMA));
            ddl.Items.Add(Tools.CreateEnumListItem("FHLMC", E_sHmdaPurchaser2004T.FHLMC));
            ddl.Items.Add(Tools.CreateEnumListItem("Farmer Mac", E_sHmdaPurchaser2004T.FarmerMac));
            ddl.Items.Add(Tools.CreateEnumListItem("Private Securitization", E_sHmdaPurchaser2004T.PrivateSecuritization));
            ddl.Items.Add(Tools.CreateEnumListItem("Commercial bank, savings bank or savings association", E_sHmdaPurchaser2004T.CommercialBank));
            ddl.Items.Add(Tools.CreateEnumListItem("Life ins. company, credit union, mortgage bank, finance company", E_sHmdaPurchaser2004T.LifeInsCreditUnion));
            ddl.Items.Add(Tools.CreateEnumListItem("Affiliate Institution", E_sHmdaPurchaser2004T.AffiliateInstitution));
            ddl.Items.Add(Tools.CreateEnumListItem("Other type of purchaser", E_sHmdaPurchaser2004T.OtherType));

        }

        public static void Bind_sReliedOnCreditScoreModeT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem(string.Empty, ReliedOnCreditScoreMode.Blank));
            ddl.Items.Add(Tools.CreateEnumListItem("Applicant's Individual Score", ReliedOnCreditScoreMode.ApplicantIndividualScore));
            ddl.Items.Add(Tools.CreateEnumListItem("Single Score", ReliedOnCreditScoreMode.SingleScore));
        }

        public static void Bind_sReliedOnCreditScoreSourceT(DropDownList ddl, IEnumerable<CAppData> apps)
        {
            ddl.Items.Add(new ListItem(string.Empty, HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Blank]));
            ddl.Items.Add(new ListItem("Type 1", HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Type1]));
            ddl.Items.Add(new ListItem("Type 2", HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Type2]));
            ddl.Items.Add(new ListItem("Type 2 Soft", HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Type2Soft]));
            ddl.Items.Add(new ListItem("Type 3", HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Type3]));

            var appsList = apps.ToList();

            for (int i = 0; i < appsList.Count; i++)
            {
                int index = i + 1;
                if (appsList[i].aBIsValidHmdaApplication)
                {
                    ddl.Items.Add(new ListItem("Borrower " + index + " Decision Score", appsList[i].aAppId.ToString() + "_b"));
                }

                if (appsList[i].aCIsValidHmdaApplication)
                {
                    ddl.Items.Add(new ListItem("Coborrower " + index + " Decision Score", appsList[i].aAppId.ToString() + "_c"));
                }
            }
                        
            ddl.Items.Add(new ListItem("Other", HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Other]));
        }

        public static void Bind_sHmdaActionTaken(ComboBox combobox)
        {
            combobox.Items.Add("Loan originated");
            combobox.Items.Add("App approved but not accepted by applicant");
            combobox.Items.Add("Application denied");
            combobox.Items.Add("Application withdrawn");
            combobox.Items.Add("File closed for incompleteness");
            combobox.Items.Add("Loan purchased by your institution");
            combobox.Items.Add("Preapproval request denied");
            combobox.Items.Add("Preapproval request approved but not accepted");
        }

        /// <summary>
        /// Maps a HMDA action taken string to an enumerated value.
        /// </summary>
        /// <param name="value">The value to map.</param>
        /// <param name="defaultValue">
        /// Optional. The default value in the case that we cannot map the given value.
        /// If the default value is null and we can't map the given value, an exception
        /// will be thrown.
        /// </param>
        /// <returns>The enumerated action taken value.</returns>
        public static HmdaActionTaken MapStringToHmdaActionTaken(string value, HmdaActionTaken? defaultValue = null)
        {
            switch (value.ToLower())
            {
                case null:
                case "":
                    return HmdaActionTaken.Blank;
                case "loan originated":
                    return HmdaActionTaken.LoanOriginated;
                case "application approved but not accepted":
                case "app approved but not accepted by applicant":  // OLD VALUE - OBSOLETE - DO NOT USE IF POSSIBLE
                    return HmdaActionTaken.ApplicationApprovedButNotAccepted;
                case "application denied":
                    return HmdaActionTaken.ApplicationDenied;
                case "application withdrawn by applicant":
                case "application withdrawn":   // OLD VALUE - OBSOLETE - DO NOT USE IF POSSIBLE
                    return HmdaActionTaken.ApplicationWithdrawnByApplicant;
                case "file closed for incompleteness":
                    return HmdaActionTaken.FileClosedForIncompleteness;
                case "purchased loan":
                case "loan purchased by your institution":  // OLD VALUE - OBSOLETE - DO NOT USE IF POSSIBLE
                    return HmdaActionTaken.PurchasedLoan;
                case "preapproval request denied":
                    return HmdaActionTaken.PreapprovalRequestDenied;
                case "preapproval request approved but not accepted":
                    return HmdaActionTaken.PreapprovalRequestApprovedButNotAccepted;
                default:
                    if (defaultValue.HasValue)
                    {
                        return defaultValue.Value;
                    }
                    else
                    {
                        throw new UnhandledCaseException(value);
                    }
            }
        }

        public static string MapHmdaActionTakenToString(HmdaActionTaken value)
        {
            switch (value)
            {
                case HmdaActionTaken.Blank:
                    return string.Empty;
                case HmdaActionTaken.LoanOriginated:
                    return "Loan originated";
                case HmdaActionTaken.ApplicationApprovedButNotAccepted:
                    return "Application approved but not accepted";
                case HmdaActionTaken.ApplicationDenied:
                    return "Application denied";
                case HmdaActionTaken.ApplicationWithdrawnByApplicant:
                    return "Application withdrawn by applicant";
                case HmdaActionTaken.FileClosedForIncompleteness:
                    return "File closed for incompleteness";
                case HmdaActionTaken.PurchasedLoan:
                    return "Purchased loan";
                case HmdaActionTaken.PreapprovalRequestDenied:
                    return "Preapproval request denied";
                case HmdaActionTaken.PreapprovalRequestApprovedButNotAccepted:
                    return "Preapproval request approved but not accepted";
                default:
                    throw new UnhandledEnumException(value);
            }
        }

        public static void Bind_sHmdaActionTakenT(DropDownList ddl)
        {
            ddl.AddEnumItem(MapHmdaActionTakenToString(HmdaActionTaken.Blank), HmdaActionTaken.Blank);
            ddl.AddEnumItem(MapHmdaActionTakenToString(HmdaActionTaken.LoanOriginated), HmdaActionTaken.LoanOriginated);
            ddl.AddEnumItem(MapHmdaActionTakenToString(HmdaActionTaken.ApplicationApprovedButNotAccepted), HmdaActionTaken.ApplicationApprovedButNotAccepted);
            ddl.AddEnumItem(MapHmdaActionTakenToString(HmdaActionTaken.ApplicationDenied), HmdaActionTaken.ApplicationDenied);
            ddl.AddEnumItem(MapHmdaActionTakenToString(HmdaActionTaken.ApplicationWithdrawnByApplicant), HmdaActionTaken.ApplicationWithdrawnByApplicant);
            ddl.AddEnumItem(MapHmdaActionTakenToString(HmdaActionTaken.FileClosedForIncompleteness), HmdaActionTaken.FileClosedForIncompleteness);
            ddl.AddEnumItem(MapHmdaActionTakenToString(HmdaActionTaken.PurchasedLoan), HmdaActionTaken.PurchasedLoan);
            ddl.AddEnumItem(MapHmdaActionTakenToString(HmdaActionTaken.PreapprovalRequestDenied), HmdaActionTaken.PreapprovalRequestDenied);
            ddl.AddEnumItem(MapHmdaActionTakenToString(HmdaActionTaken.PreapprovalRequestApprovedButNotAccepted), HmdaActionTaken.PreapprovalRequestApprovedButNotAccepted);
        }

        // NOTE: The 2nd case for each enum (except blank) is an OBSOLETE string that should not be used with any loan with sLoanVersionT > V22
        public static HmdaDenialReason MapStringToHmdaDenialReason(string value)
        {
            switch (value.ToLower())
            {
                case null:
                case "":
                    return HmdaDenialReason.Blank;
                case "debt-to-income ratio":
                case "1 debt-to-income ratio":
                    return HmdaDenialReason.DebtToIncomeRatio;
                case "employment history":
                case "2 employment history":
                    return HmdaDenialReason.EmploymentHistory;
                case "credit history":
                case "3 credit history":
                    return HmdaDenialReason.CreditHistory;
                case "collateral":
                case "4 collateral":
                    return HmdaDenialReason.Collateral;
                case "insufficient cash (downpayment, closing costs)":
                case "5 insufficient cash":
                    return HmdaDenialReason.InsufficientCash;
                case "unverifiable information":
                case "6 unverifiable information":
                    return HmdaDenialReason.UnverifiableInformation;
                case "credit application incomplete":
                case "7 credit app incomplete":
                    return HmdaDenialReason.CreditApplicationIncomplete;
                case "mortgage insurance denied":
                case "8 mortgage ins denied":
                    return HmdaDenialReason.MortgageInsuranceDenied;
                case "other":
                case "9 other":
                    return HmdaDenialReason.Other;
                case "not applicable":
                    return HmdaDenialReason.NotApplicable;
                default:
                    throw new UnhandledCaseException(value);
            }
        }

        public static string MapHmdaDenialReasonToString(HmdaDenialReason value)
        {
            switch (value)
            {
                case HmdaDenialReason.Blank:
                    return string.Empty;
                case HmdaDenialReason.DebtToIncomeRatio:
                    return "Debt-to-income ratio";
                case HmdaDenialReason.EmploymentHistory:
                    return "Employment history";
                case HmdaDenialReason.CreditHistory:
                    return "Credit history";
                case HmdaDenialReason.Collateral:
                    return "Collateral";
                case HmdaDenialReason.InsufficientCash:
                    return "Insufficient cash (downpayment, closing costs)";
                case HmdaDenialReason.UnverifiableInformation:
                    return "Unverifiable information";
                case HmdaDenialReason.CreditApplicationIncomplete:
                    return "Credit application incomplete";
                case HmdaDenialReason.MortgageInsuranceDenied:
                    return "Mortgage insurance denied";
                case HmdaDenialReason.Other:
                    return "Other";
                case HmdaDenialReason.NotApplicable:
                    return "Not applicable";
                default:
                    throw new UnhandledEnumException(value);
            }
        }

        public static void Bind_sHmdaDenialReasonT(DropDownList ddl)
        {
            ddl.AddEnumItem(MapHmdaDenialReasonToString(HmdaDenialReason.Blank), HmdaDenialReason.Blank);
            ddl.AddEnumItem(MapHmdaDenialReasonToString(HmdaDenialReason.DebtToIncomeRatio), HmdaDenialReason.DebtToIncomeRatio);
            ddl.AddEnumItem(MapHmdaDenialReasonToString(HmdaDenialReason.CreditHistory), HmdaDenialReason.CreditHistory);
            ddl.AddEnumItem(MapHmdaDenialReasonToString(HmdaDenialReason.Collateral), HmdaDenialReason.Collateral);
            ddl.AddEnumItem(MapHmdaDenialReasonToString(HmdaDenialReason.InsufficientCash), HmdaDenialReason.InsufficientCash);
            ddl.AddEnumItem(MapHmdaDenialReasonToString(HmdaDenialReason.UnverifiableInformation), HmdaDenialReason.UnverifiableInformation);
            ddl.AddEnumItem(MapHmdaDenialReasonToString(HmdaDenialReason.CreditApplicationIncomplete), HmdaDenialReason.CreditApplicationIncomplete);
            ddl.AddEnumItem(MapHmdaDenialReasonToString(HmdaDenialReason.MortgageInsuranceDenied), HmdaDenialReason.MortgageInsuranceDenied);
            ddl.AddEnumItem(MapHmdaDenialReasonToString(HmdaDenialReason.Other), HmdaDenialReason.Other);
            ddl.AddEnumItem(MapHmdaDenialReasonToString(HmdaDenialReason.NotApplicable), HmdaDenialReason.NotApplicable);
        }
        
        public static void Bind_sHmdaLoanTypeT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem(string.Empty, sHmdaLoanTypeT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Conventional", sHmdaLoanTypeT.Conventional));
            ddl.Items.Add(Tools.CreateEnumListItem("FHA", sHmdaLoanTypeT.FHA));
            ddl.Items.Add(Tools.CreateEnumListItem("VA", sHmdaLoanTypeT.VA));
            ddl.Items.Add(Tools.CreateEnumListItem("USDA", sHmdaLoanTypeT.USDA));
        }

        public static void Bind_sHmdaLoanPurposeT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem(string.Empty, sHmdaLoanPurposeT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Home Purchase", sHmdaLoanPurposeT.HomePurchase));
            ddl.Items.Add(Tools.CreateEnumListItem("Home Improvement", sHmdaLoanPurposeT.HomeImprovement));
            ddl.Items.Add(Tools.CreateEnumListItem("Refinancing", sHmdaLoanPurposeT.Refinancing));
            ddl.Items.Add(Tools.CreateEnumListItem("Cash-Out Refinancing", sHmdaLoanPurposeT.CashOutRefinancing));
            ddl.Items.Add(Tools.CreateEnumListItem("Other Purpose", sHmdaLoanPurposeT.OtherPurpose));
        }

        public static void Bind_sHmdaConstructionMethT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem(string.Empty, sHmdaConstructionMethT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Site-Built", sHmdaConstructionMethT.SiteBuilt));
            ddl.Items.Add(Tools.CreateEnumListItem("Manufactured Home", sHmdaConstructionMethT.ManufacturedHome));
        }

        public static void Bind_sHmdaCoApplicantSourceT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("No Co-Applicant", sHmdaCoApplicantSourceT.NoCoApplicant));
            ddl.Items.Add(Tools.CreateEnumListItem("Co-Borrower On Primary App", sHmdaCoApplicantSourceT.CoborrowerOnPrimaryApp));
            ddl.Items.Add(Tools.CreateEnumListItem("Applicant On Secondary App", sHmdaCoApplicantSourceT.ApplicantOnSecondaryApp));
        }

        public static void Bind_sHmdaHoepaStatusT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem(string.Empty, sHmdaHoepaStatusT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("High-Cost", sHmdaHoepaStatusT.HighCost));
            ddl.Items.Add(Tools.CreateEnumListItem("Not High-Cost", sHmdaHoepaStatusT.NotHighCost));
            ddl.Items.Add(Tools.CreateEnumListItem("Not Applicable", sHmdaHoepaStatusT.NA));
        }

        public static void Bind_sHmdaBalloonPaymentT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem(string.Empty, sHmdaBalloonPaymentT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Balloon Payment", sHmdaBalloonPaymentT.BalloonPayment));
            ddl.Items.Add(Tools.CreateEnumListItem("No balloon payment", sHmdaBalloonPaymentT.NoBalloonPayment));
        }

        public static void Bind_sHmdaInterestOnlyPaymentT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem(string.Empty, sHmdaInterestOnlyPaymentT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Interest-only Payments", sHmdaInterestOnlyPaymentT.InterestOnlyPayments));
            ddl.Items.Add(Tools.CreateEnumListItem("No interest-only payments", sHmdaInterestOnlyPaymentT.NoInterestOnlyPayments));
        }

        public static void Bind_sHmdaNegativeAmortizationT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem(string.Empty, sHmdaNegativeAmortizationT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Negative amortization", sHmdaNegativeAmortizationT.NegativeAmortization));
            ddl.Items.Add(Tools.CreateEnumListItem("No negative amortization", sHmdaNegativeAmortizationT.NoNegativeAmortization));
        }

        public static void Bind_sHmdaOtherNonAmortFeatureT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem(string.Empty, sHmdaOtherNonAmortFeatureT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Other Non-Fully Amortizing Features", sHmdaOtherNonAmortFeatureT.OtherNonFullyAmortizingFeatures));
            ddl.Items.Add(Tools.CreateEnumListItem("No Other Non-Fully Amortizing Features", sHmdaOtherNonAmortFeatureT.NoOtherNonFullyAmortizingFeatures));

        }

        public static void Bind_sHmdaManufacturedTypeT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem(string.Empty, sHmdaManufacturedTypeT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Manufactured Home and Land", sHmdaManufacturedTypeT.ManufacturedHomeAndLand));
            ddl.Items.Add(Tools.CreateEnumListItem("Manufactured Home and Not Land", sHmdaManufacturedTypeT.ManufacturedHomeAndNotLand));
            ddl.Items.Add(Tools.CreateEnumListItem("Not Applicable", sHmdaManufacturedTypeT.NA));
        }

        public static void Bind_sHmdaManufacturedInterestT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem(string.Empty, sHmdaManufacturedInterestT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Direct Ownership", sHmdaManufacturedInterestT.DirectOwnership));
            ddl.Items.Add(Tools.CreateEnumListItem("Indirect Ownership", sHmdaManufacturedInterestT.IndirectOwnership));
            ddl.Items.Add(Tools.CreateEnumListItem("Paid Leasehold", sHmdaManufacturedInterestT.PaidLeasehold));
            ddl.Items.Add(Tools.CreateEnumListItem("Unpaid Leasehold", sHmdaManufacturedInterestT.UnpaidLeasehold));
            ddl.Items.Add(Tools.CreateEnumListItem("Not Applicable", sHmdaManufacturedInterestT.NA));
        }

        public static void Bind_sHmdaSubmissionApplicationT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem(string.Empty, sHmdaSubmissionApplicationT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Submitted Directly to Institution", sHmdaSubmissionApplicationT.SubmittedDirectlyToInstitution));
            ddl.Items.Add(Tools.CreateEnumListItem("Not Submitted Directly to Institution", sHmdaSubmissionApplicationT.NotSubmittedDirectlyToInstitution));
            ddl.Items.Add(Tools.CreateEnumListItem("Not Applicable", sHmdaSubmissionApplicationT.NA));
        }

        public static void Bind_sHmdaInitiallyPayableToInstitutionT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem(string.Empty, sHmdaInitiallyPayableToInstitutionT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Initially Payable to Institution", sHmdaInitiallyPayableToInstitutionT.InitiallyPayableToInstitution));
            ddl.Items.Add(Tools.CreateEnumListItem("Not Initially Payable to Institution", sHmdaInitiallyPayableToInstitutionT.NotInitiallyPayableToInstitution));
            ddl.Items.Add(Tools.CreateEnumListItem("Not Applicable", sHmdaInitiallyPayableToInstitutionT.NA));
        }

        public static void Bind_sHmdaReverseMortgageT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem(string.Empty, sHmdaReverseMortgageT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Reverse Mortgage", sHmdaReverseMortgageT.ReverseMortgage));
            ddl.Items.Add(Tools.CreateEnumListItem("Not Reverse Mortgage", sHmdaReverseMortgageT.NotReverseMortgage));
        }

        public static void Bind_sHmdaBusinessPurposeT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem(string.Empty, sHmdaBusinessPurposeT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Primarily for Business or Commercial", sHmdaBusinessPurposeT.PrimarilyForBusinessOrCommercial));
            ddl.Items.Add(Tools.CreateEnumListItem("Not Primarily for Business or Commercial", sHmdaBusinessPurposeT.NotPrimarilyForBusinessOrCommercial));
        }

        public static void BindHmdaCreditModel(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem(string.Empty, sHmdaCreditScoreModelT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Equifax Beacon 5.0", sHmdaCreditScoreModelT.EquifaxBeacon5));
            ddl.Items.Add(Tools.CreateEnumListItem("Experian Fair Isaac", sHmdaCreditScoreModelT.ExperianFairIsaac));
            ddl.Items.Add(Tools.CreateEnumListItem("FICO Risk Score Classic 04", sHmdaCreditScoreModelT.FicoRiskScoreClassic04));
            ddl.Items.Add(Tools.CreateEnumListItem("FICO Risk Score Classic 98", sHmdaCreditScoreModelT.FicoRiskScoreClassic98));
            ddl.Items.Add(Tools.CreateEnumListItem("VantageScore 2.0", sHmdaCreditScoreModelT.VantageScore2));
            ddl.Items.Add(Tools.CreateEnumListItem("VantageScore 3.0", sHmdaCreditScoreModelT.VantageScore3));
            ddl.Items.Add(Tools.CreateEnumListItem("More than one credit scoring model", sHmdaCreditScoreModelT.MoreThanOneScoringModel));
            ddl.Items.Add(Tools.CreateEnumListItem("Other credit scoring model", sHmdaCreditScoreModelT.Other));
            ddl.Items.Add(Tools.CreateEnumListItem("Not applicable", sHmdaCreditScoreModelT.NotApplicable));
            ddl.Items.Add(Tools.CreateEnumListItem("No co-applicant", sHmdaCreditScoreModelT.NoCoApplicant));
        }

        public static Dictionary<E_sHmdaPurchaser2004T, HmdaPurchaser2015T> HmdaPurchaserMapping2004To2015 = new Dictionary<E_sHmdaPurchaser2004T, HmdaPurchaser2015T>() {
            {E_sHmdaPurchaser2004T.LeaveBlank, HmdaPurchaser2015T.LeaveBlank },
            {E_sHmdaPurchaser2004T.LoanWasNotSold, HmdaPurchaser2015T.NA },
            {E_sHmdaPurchaser2004T.FNMA, HmdaPurchaser2015T.FannieMae },
            {E_sHmdaPurchaser2004T.GNMA, HmdaPurchaser2015T.GinnieMae },
            {E_sHmdaPurchaser2004T.FHLMC, HmdaPurchaser2015T.FreddieMac },
            {E_sHmdaPurchaser2004T.FarmerMac, HmdaPurchaser2015T.FarmerMac },
            {E_sHmdaPurchaser2004T.PrivateSecuritization, HmdaPurchaser2015T.PrivateSecuritizer },
            {E_sHmdaPurchaser2004T.CommercialBank, HmdaPurchaser2015T.CommercialBankSavingsBankSavingsAssociation },
            {E_sHmdaPurchaser2004T.LifeInsCreditUnion, HmdaPurchaser2015T.LifeInsCreditUnion },
            {E_sHmdaPurchaser2004T.AffiliateInstitution, HmdaPurchaser2015T.AffiliateInstitution },
            {E_sHmdaPurchaser2004T.OtherType, HmdaPurchaser2015T.Other }
        };

        public static Dictionary<HmdaPurchaser2015T, E_sHmdaPurchaser2004T> HmdaPurchaserMapping2015To2004 = new Dictionary<HmdaPurchaser2015T, E_sHmdaPurchaser2004T>() {
            {HmdaPurchaser2015T.LeaveBlank, E_sHmdaPurchaser2004T.LeaveBlank },
            {HmdaPurchaser2015T.NA, E_sHmdaPurchaser2004T.LoanWasNotSold },
            {HmdaPurchaser2015T.FannieMae, E_sHmdaPurchaser2004T.FNMA },
            {HmdaPurchaser2015T.GinnieMae, E_sHmdaPurchaser2004T.GNMA },
            {HmdaPurchaser2015T.FreddieMac, E_sHmdaPurchaser2004T.FHLMC },
            {HmdaPurchaser2015T.FarmerMac, E_sHmdaPurchaser2004T.FarmerMac },
            {HmdaPurchaser2015T.PrivateSecuritizer, E_sHmdaPurchaser2004T.PrivateSecuritization },
            {HmdaPurchaser2015T.CommercialBankSavingsBankSavingsAssociation, E_sHmdaPurchaser2004T.CommercialBank },
            {HmdaPurchaser2015T.LifeInsCreditUnion, E_sHmdaPurchaser2004T.LifeInsCreditUnion },
            {HmdaPurchaser2015T.CreditUnionMortgageCompanyFinanceCompany, E_sHmdaPurchaser2004T.LifeInsCreditUnion },
            {HmdaPurchaser2015T.LifeInsuranceCompany, E_sHmdaPurchaser2004T.LifeInsCreditUnion },
            {HmdaPurchaser2015T.AffiliateInstitution, E_sHmdaPurchaser2004T.AffiliateInstitution },
            {HmdaPurchaser2015T.Other, E_sHmdaPurchaser2004T.OtherType }
        };

        public static Dictionary<HmdaPurchaser2015T, string> HmdaPurchaser2015TDescriptionMapping = new Dictionary<HmdaPurchaser2015T, string>() {
            { HmdaPurchaser2015T.LeaveBlank, string.Empty},
            { HmdaPurchaser2015T.NA, "Not Applicable"},
            { HmdaPurchaser2015T.FannieMae, "FannieMae"},
            { HmdaPurchaser2015T.GinnieMae, "GinnieMae"},
            { HmdaPurchaser2015T.FreddieMac, "FreddieMac"},
            { HmdaPurchaser2015T.FarmerMac, "FarmerMac"},
            { HmdaPurchaser2015T.PrivateSecuritizer, "Private Securitizer"},
            { HmdaPurchaser2015T.LifeInsCreditUnion, "(Obsolete) Life Ins. Company, Credit Union, Mortgage Bank, Finance Company"},
            { HmdaPurchaser2015T.CommercialBankSavingsBankSavingsAssociation, "Commercial Bank, Savings Bank, or Savings Association"},
            { HmdaPurchaser2015T.CreditUnionMortgageCompanyFinanceCompany, "Credit Union, Mortgage Company, or Finance Company"},
            { HmdaPurchaser2015T.LifeInsuranceCompany, "Life Insurance Company"},
            { HmdaPurchaser2015T.AffiliateInstitution, "Affiliate Institution"},
            { HmdaPurchaser2015T.Other, "Other"},
        };

        public static string GetHmdaPurchaser2015TDescription(HmdaPurchaser2015T purchaserT)
        {
            string description;
            HmdaPurchaser2015TDescriptionMapping.TryGetValue(purchaserT, out description);
            return description;
        }

        public static void Bind_sHmdaPurchaser2015T(DropDownList ddl, HmdaPurchaser2015T currentValue = HmdaPurchaser2015T.NA)
        {
            ddl.Items.Add(Tools.CreateEnumListItem(string.Empty, HmdaPurchaser2015T.LeaveBlank));

            ddl.Items.Add(Tools.CreateEnumListItem(GetHmdaPurchaser2015TDescription(HmdaPurchaser2015T.NA), HmdaPurchaser2015T.NA));

            ddl.Items.Add(Tools.CreateEnumListItem(GetHmdaPurchaser2015TDescription(HmdaPurchaser2015T.FannieMae), HmdaPurchaser2015T.FannieMae));
            ddl.Items.Add(Tools.CreateEnumListItem(GetHmdaPurchaser2015TDescription(HmdaPurchaser2015T.FreddieMac), HmdaPurchaser2015T.FreddieMac));
            ddl.Items.Add(Tools.CreateEnumListItem(GetHmdaPurchaser2015TDescription(HmdaPurchaser2015T.GinnieMae), HmdaPurchaser2015T.GinnieMae));
            ddl.Items.Add(Tools.CreateEnumListItem(GetHmdaPurchaser2015TDescription(HmdaPurchaser2015T.FarmerMac), HmdaPurchaser2015T.FarmerMac));
            ddl.Items.Add(Tools.CreateEnumListItem(GetHmdaPurchaser2015TDescription(HmdaPurchaser2015T.PrivateSecuritizer), HmdaPurchaser2015T.PrivateSecuritizer));

            if(currentValue == HmdaPurchaser2015T.LifeInsCreditUnion)
            {
                ddl.Items.Add(Tools.CreateEnumListItem(GetHmdaPurchaser2015TDescription(HmdaPurchaser2015T.LifeInsCreditUnion), HmdaPurchaser2015T.LifeInsCreditUnion));
            }

            ddl.Items.Add(Tools.CreateEnumListItem(GetHmdaPurchaser2015TDescription(HmdaPurchaser2015T.CommercialBankSavingsBankSavingsAssociation), HmdaPurchaser2015T.CommercialBankSavingsBankSavingsAssociation));
            ddl.Items.Add(Tools.CreateEnumListItem(GetHmdaPurchaser2015TDescription(HmdaPurchaser2015T.CreditUnionMortgageCompanyFinanceCompany), HmdaPurchaser2015T.CreditUnionMortgageCompanyFinanceCompany));
            ddl.Items.Add(Tools.CreateEnumListItem(GetHmdaPurchaser2015TDescription(HmdaPurchaser2015T.LifeInsuranceCompany), HmdaPurchaser2015T.LifeInsuranceCompany));
            ddl.Items.Add(Tools.CreateEnumListItem(GetHmdaPurchaser2015TDescription(HmdaPurchaser2015T.AffiliateInstitution), HmdaPurchaser2015T.AffiliateInstitution));
            ddl.Items.Add(Tools.CreateEnumListItem(GetHmdaPurchaser2015TDescription(HmdaPurchaser2015T.Other), HmdaPurchaser2015T.Other));
        }

        public static void Bind_sClosingCostFeeVersionT(DropDownList ddl, bool hideLegacy)
        {
            if (hideLegacy == false)
            {
                ddl.Items.Add(Tools.CreateEnumListItem("Legacy", E_sClosingCostFeeVersionT.Legacy));
            }
            ddl.Items.Add(Tools.CreateEnumListItem("Migrated with legacy support", E_sClosingCostFeeVersionT.LegacyButMigrated));
            ddl.Items.Add(Tools.CreateEnumListItem("Closing cost fee 2015", E_sClosingCostFeeVersionT.ClosingCostFee2015));
        }

        public static void Bind_sLoanFileT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("Loan", E_sLoanFileT.Loan));
            ddl.Items.Add(Tools.CreateEnumListItem("Sandbox", E_sLoanFileT.Sandbox));
            ddl.Items.Add(Tools.CreateEnumListItem("QuickPricer2Sandboxed", E_sLoanFileT.QuickPricer2Sandboxed));
            ddl.Items.Add(Tools.CreateEnumListItem("Test", E_sLoanFileT.Test));
        }

        public static void BindOCStatusT(DropDownList ddl)
        {
            foreach (OCStatusType statusType in Enum.GetValues(typeof(OCStatusType)))
            {
                ddl.Items.Add(Tools.CreateEnumListItem(ConstApp.FriendlyOCStatusTypes[statusType], statusType));
            }
        }

        public static string GetDescription(E_sLqbCollectionT lqbCollectionT)
        {
            switch (lqbCollectionT)
            {
                case E_sLqbCollectionT.Legacy: return "Legacy";
                case E_sLqbCollectionT.UseLqbCollections: return "Use LQB Collections";
                case E_sLqbCollectionT.IncomeSourceCollection: return "Income Source Collection";
                case E_sLqbCollectionT.HousingHistoryEntriesCollection: return "Housing History Entries Collection";
                default: throw new UnhandledEnumException(lqbCollectionT);
            }
        }

        public static string GetDescription(E_BranchChannelT branchChannelT)
        {
            switch (branchChannelT)
            {
                case E_BranchChannelT.Blank:
                    return "";
                case E_BranchChannelT.Retail:
                    return "Retail";
                case E_BranchChannelT.Wholesale:
                    return "Wholesale";
                case E_BranchChannelT.Correspondent:
                    return "Correspondent";
                case E_BranchChannelT.Broker:
                    return "Broker";
                default:
                    throw new UnhandledEnumException(branchChannelT);
            }
        }
        public static string GetDescription(E_sCorrespondentProcessT correspondentProcess)
        {
            switch (correspondentProcess)
            {
                case E_sCorrespondentProcessT.Blank:
                    return string.Empty;
                case E_sCorrespondentProcessT.Delegated:
                    return "Delegated";
                case E_sCorrespondentProcessT.PriorApproved:
                    return "Prior Approved";
                case E_sCorrespondentProcessT.MiniCorrespondent:
                    return "Mini-Correspondent";
                case E_sCorrespondentProcessT.Bulk:
                    return "Bulk";
                case E_sCorrespondentProcessT.MiniBulk:
                    return "Mini-Bulk";
                default:
                    throw new UnhandledEnumException(correspondentProcess);
            }
        }
        public static void Bind_sBranchChannelT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_BranchChannelT.Blank));
            ddl.Items.Add(Tools.CreateEnumListItem("Retail", E_BranchChannelT.Retail));
            ddl.Items.Add(Tools.CreateEnumListItem("Wholesale", E_BranchChannelT.Wholesale));
            ddl.Items.Add(Tools.CreateEnumListItem("Correspondent", E_BranchChannelT.Correspondent));
            ddl.Items.Add(Tools.CreateEnumListItem("Broker", E_BranchChannelT.Broker));
        }

        public static void Bind_BranchStatus(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("Active", BranchStatus.Active));
            ddl.Items.Add(Tools.CreateEnumListItem("Inactive", BranchStatus.Inactive));
        }

        public static void Bind_sCorrespondentProcessT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sCorrespondentProcessT.Blank));
            ddl.Items.Add(Tools.CreateEnumListItem("Delegated", E_sCorrespondentProcessT.Delegated));
            ddl.Items.Add(Tools.CreateEnumListItem("Prior Approved", E_sCorrespondentProcessT.PriorApproved));
            ddl.Items.Add(Tools.CreateEnumListItem("Mini-Correspondent", E_sCorrespondentProcessT.MiniCorrespondent));
            ddl.Items.Add(Tools.CreateEnumListItem("Bulk", E_sCorrespondentProcessT.Bulk));
            ddl.Items.Add(Tools.CreateEnumListItem("Mini-Bulk", E_sCorrespondentProcessT.MiniBulk));
        }
        public static void Bind_sLT_LoanFind(DropDownList ddl)
        {
            ddl.Items.Add(new ListItem("<-- Any type -->", "-1"));
            Bind_sLT(ddl);
        }
        private static void Bind_sStatusT_Impl(DropDownList ddl, E_sStatusT[] list)
        {
            foreach (E_sStatusT sStatusT in list)
            {
                ddl.Items.Add(new ListItem(CPageBase.sStatusT_map_rep(sStatusT), sStatusT.ToString("D")));
            }
        }

        public static IEnumerable<E_sStatusT> sStatusTs_For_PmlSearchCriteria(Guid brokerID, E_PortalMode mode, E_sCorrespondentProcessT process, bool includeLeadStatuses = false)
        {
            var broker = BrokerDB.RetrieveById(brokerID);

            E_sStatusT maxStatus;

            if (mode.Equals(E_PortalMode.Broker))
            {
                maxStatus = E_sStatusT.Loan_Funded;
            }
            else
            {
                maxStatus = E_sStatusT.Loan_Purchased;
            }

            var orderedActiveList = (from s in CPageBase.s_sStatusT_Order
                                     where false == Tools.IsInactiveStatus(s)
                                     && CPageBase.s_OrderByStatusT[s] <= CPageBase.s_OrderByStatusT[maxStatus]
                                     select s);

            var orderedInactiveList = (from s in CPageBase.s_sStatusT_Order
                                       where Tools.IsInactiveStatus(s)
                                       select s);

            var orderedFilteredList = (from s in orderedActiveList.Concat(orderedInactiveList)
                                       where includeLeadStatuses || false == Tools.IsStatusLead(s)
                                       && s != E_sStatusT.Loan_WebConsumer
                                       select s);

            return orderedFilteredList;
        }

        //This will return list for the search criteria DDL
        public static IEnumerable<E_sStatusT> sStatusTs_ForDDL_PmlSearchCriteria(Guid brokerID, E_PortalMode mode, E_sCorrespondentProcessT process, bool IsLeadPipeline)
        {
            if (IsLeadPipeline)
            {
                return new List<E_sStatusT>() {
                    E_sStatusT.Lead_New,
                    E_sStatusT.Lead_Canceled,
                    E_sStatusT.Lead_Declined,
                    E_sStatusT.Lead_Other
                };
            }

            var broker = BrokerDB.RetrieveById(brokerID);

            E_sStatusT maxStatus;
            E_BranchChannelT branchT;

            if (mode.Equals(E_PortalMode.Broker))
            {
                maxStatus = E_sStatusT.Loan_Funded;
                branchT = E_BranchChannelT.Wholesale;
            }
            else if (mode.Equals(E_PortalMode.Retail))
            {
                maxStatus = E_sStatusT.Loan_LoanPurchased;
                branchT = E_BranchChannelT.Retail;
            }
            else
            {
                maxStatus = E_sStatusT.Loan_Purchased;
                branchT = E_BranchChannelT.Correspondent;
                if (mode.Equals(E_PortalMode.MiniCorrespondent))
                {
                    process = E_sCorrespondentProcessT.MiniCorrespondent;
                }
            }

            var orderedActiveList = (from s in CPageBase.s_sStatusT_Order
                                     where false == Tools.IsInactiveStatus(s)
                                     && CPageBase.s_OrderByStatusT[s] <= CPageBase.s_OrderByStatusT[maxStatus]
                                     select s);

            var orderedInactiveList = (from s in CPageBase.s_sStatusT_Order
                                       where Tools.IsInactiveStatus(s)
                                       select s);

            if (mode != E_PortalMode.Correspondent)
            {
                var orderedFilteredList = (from s in orderedActiveList.Concat(orderedInactiveList)
                                           where broker.GetEnabledStatusesByChannelAndProcessType(branchT, process).Contains(s)
                                           && false == Tools.IsStatusLead(s)
                                           && s != E_sStatusT.Loan_WebConsumer
                                           select s);

                return orderedFilteredList;
            }
            else
            {
                var orderedFilteredList = (from s in orderedActiveList.Concat(orderedInactiveList)
                                           where
                                           (broker.GetEnabledStatusesByChannelAndProcessType(branchT, E_sCorrespondentProcessT.Delegated).Contains(s) ||
                                           broker.GetEnabledStatusesByChannelAndProcessType(branchT, E_sCorrespondentProcessT.PriorApproved).Contains(s))
                                           && false == Tools.IsStatusLead(s)
                                           && s != E_sStatusT.Loan_WebConsumer
                                           select s);

                return orderedFilteredList;
            }
        }

        /// <summary>
        /// When a user first logs into PML, their portal mode will be Blank.  This method will determine the which portal mode is available to the user,
        /// and choose the first one.  If available, use the principal as the parameter instead for faster performance.
        /// </summary>
        /// <param name="employee">The employee to decide the portal mode for.</param>
        /// <returns>The portal mode.</returns>
        public static E_PortalMode DeterminePortalModeFirstLogin(EmployeeDB employee)
        {
            var principal = PrincipalFactory.Create(employee.BrokerID, employee.UserID, employee.UserType.ToString(), true, false);
            return Tools.DeterminePortalModeFirstLogin(principal);
        }

        /// <summary>
        /// When a user first logs into PML, their portal mode will be Blank.  This method will determine the which portal mode is available to the user,
        /// and choose the first one.  This is faster than the employee version since it doesn't recreate the principal.
        /// </summary>
        /// <param name="principal">The user's principal.</param>
        /// <returns>The first available portal mode.</returns>
        public static E_PortalMode DeterminePortalModeFirstLogin(AbstractUserPrincipal principal)
        {
            if (principal == null)
            {
                return E_PortalMode.Blank;
            }
            else if (string.Equals("B", principal.Type, StringComparison.OrdinalIgnoreCase))
            {
                return E_PortalMode.Retail;
            }
            else if (principal.HasPermission(Permission.AllowViewingWholesaleChannelLoans))
            {
                return E_PortalMode.Broker;
            }
            else if (principal.HasPermission(Permission.AllowViewingMiniCorrChannelLoans))
            {
                return E_PortalMode.MiniCorrespondent;
            }
            else if (principal.HasPermission(Permission.AllowViewingCorrChannelLoans))
            {
                return E_PortalMode.Correspondent;
            }

            return E_PortalMode.Blank;
        }

        public static void Bind_sStatusT_ForBatchImport(DropDownList ddl, Guid brokerID)
        {
            E_sStatusT[] list = {
                                    E_sStatusT.Loan_Open,
                                    E_sStatusT.Loan_Prequal,
                                    E_sStatusT.Loan_Registered,
                                    E_sStatusT.Loan_Processing,
                                    E_sStatusT.Loan_Preapproval,
                                    E_sStatusT.Loan_LoanSubmitted,
                                    E_sStatusT.Loan_Underwriting,
                                    E_sStatusT.Loan_Approved,
                                    E_sStatusT.Loan_FinalUnderwriting,
                                    E_sStatusT.Loan_ClearToClose,
                                    E_sStatusT.Loan_Docs,
                                    E_sStatusT.Loan_DocsBack,
                                    E_sStatusT.Loan_FundingConditions,
                                    E_sStatusT.Loan_Funded,
                                    E_sStatusT.Loan_Recorded,
                                    E_sStatusT.Loan_FinalDocs,
                                    E_sStatusT.Loan_Closed,
                                    E_sStatusT.Loan_Shipped,
                                    E_sStatusT.Loan_PreProcessing,    // start sk 1/6/2014 opm 145251
                                    E_sStatusT.Loan_DocumentCheck,
                                    E_sStatusT.Loan_DocumentCheckFailed,
                                    E_sStatusT.Loan_PreUnderwriting,
                                    E_sStatusT.Loan_ConditionReview,
                                    E_sStatusT.Loan_PreDocQC,
                                    E_sStatusT.Loan_DocsOrdered,
                                    E_sStatusT.Loan_DocsDrawn,
                                    E_sStatusT.Loan_InvestorConditions,       // this is "promoting the date to a status". tied to sSuspendedByInvestorD
                                    E_sStatusT.Loan_InvestorConditionsSent,   // this is "promoting the date to a status". tied to sCondSentToInvestorD
                                    E_sStatusT.Loan_ReadyForSale,
                                    E_sStatusT.Loan_SubmittedForPurchaseReview,
                                    E_sStatusT.Loan_InPurchaseReview,
                                    E_sStatusT.Loan_PrePurchaseConditions,
                                    E_sStatusT.Loan_SubmittedForFinalPurchaseReview,
                                    E_sStatusT.Loan_InFinalPurchaseReview,
                                    E_sStatusT.Loan_ClearToPurchase,
                                    E_sStatusT.Loan_Purchased,        // don't confuse with the old E_sStatusT.Loan_LoanPurchased, which is now Loan Sold in the UI.
                                    E_sStatusT.Loan_LoanPurchased
                                };
            var broker = BrokerDB.RetrieveById(brokerID);
            var orderedList = (from s in list
                               orderby CPageBase.s_OrderByStatusT[s]
                               where broker.HasEnabledInAnyChannel(s)
                               select s).ToList();

            orderedList.AddRange(
                                from s in new E_sStatusT[] {
                                    E_sStatusT.Loan_CounterOffer,
                                    E_sStatusT.Loan_OnHold,
                                    E_sStatusT.Loan_Canceled,
                                    E_sStatusT.Loan_Rejected,
                                    E_sStatusT.Loan_Suspended,
                                    E_sStatusT.Loan_Withdrawn,
                                    E_sStatusT.Loan_Archived,
                                }
                                where broker.HasEnabledInAnyChannel(s)
                                select s);

            orderedList.AddRange(new E_sStatusT[] {
                                    //E_sStatusT.Loan_Other,
                                    E_sStatusT.Lead_New,
                                    E_sStatusT.Lead_Canceled,
                                    E_sStatusT.Lead_Declined,
                                    E_sStatusT.Lead_Other,
                                });
            Bind_sStatusT_Impl(ddl, orderedList.ToArray());

        }

        public static void Bind_sLoanStatusT(DropDownList ddl, Guid brokerID, bool includeLoanOther)
        {
            E_sStatusT[] list = {
                                    E_sStatusT.Loan_Open,
                                    E_sStatusT.Loan_Prequal,
                                    E_sStatusT.Loan_Registered,
                                    E_sStatusT.Loan_Processing,
                                    E_sStatusT.Loan_Preapproval,
                                    E_sStatusT.Loan_LoanSubmitted,
                                    E_sStatusT.Loan_Underwriting,
                                    E_sStatusT.Loan_Approved,
                                    E_sStatusT.Loan_FinalUnderwriting,
                                    E_sStatusT.Loan_ClearToClose,
                                    E_sStatusT.Loan_Docs,
                                    E_sStatusT.Loan_DocsBack,
                                    E_sStatusT.Loan_FundingConditions,
                                    E_sStatusT.Loan_Funded,
                                    E_sStatusT.Loan_Recorded,
                                    E_sStatusT.Loan_FinalDocs,
                                    E_sStatusT.Loan_Closed,
                                    E_sStatusT.Loan_Shipped,
                                    E_sStatusT.Loan_LoanPurchased,
                                    E_sStatusT.Loan_OnHold,
                                    E_sStatusT.Loan_Canceled,
                                    E_sStatusT.Loan_Rejected,
                                    E_sStatusT.Loan_Suspended,
                                    E_sStatusT.Loan_PreProcessing,    // start sk 1/6/2014 opm 145251
                                    E_sStatusT.Loan_DocumentCheck,
                                    E_sStatusT.Loan_DocumentCheckFailed,
                                    E_sStatusT.Loan_PreUnderwriting,
                                    E_sStatusT.Loan_ConditionReview,
                                    E_sStatusT.Loan_PreDocQC,
                                    E_sStatusT.Loan_DocsOrdered,
                                    E_sStatusT.Loan_DocsDrawn,
                                    E_sStatusT.Loan_InvestorConditions,       // this is "promoting the date to a status". tied to sSuspendedByInvestorD
                                    E_sStatusT.Loan_InvestorConditionsSent,   // this is "promoting the date to a status". tied to sCondSentToInvestorD
                                    E_sStatusT.Loan_ReadyForSale,
                                    E_sStatusT.Loan_SubmittedForPurchaseReview,
                                    E_sStatusT.Loan_InPurchaseReview,
                                    E_sStatusT.Loan_PrePurchaseConditions,
                                    E_sStatusT.Loan_SubmittedForFinalPurchaseReview,
                                    E_sStatusT.Loan_InFinalPurchaseReview,
                                    E_sStatusT.Loan_ClearToPurchase,
                                    E_sStatusT.Loan_Purchased,        // don't confuse with the old E_sStatusT.Loan_LoanPurchased, which is now Loan Sold in the UI.
                                    E_sStatusT.Loan_CounterOffer,
                                    E_sStatusT.Loan_Withdrawn,
                                    E_sStatusT.Loan_Archived
                                };
            var broker = BrokerDB.RetrieveById(brokerID);
            var orderedList = (from s in list
                               orderby CPageBase.s_OrderByStatusT[s]
                               where broker.HasEnabledInAnyChannel(s)
                               select s).ToList();
            if (includeLoanOther)
            {
                orderedList.Add(E_sStatusT.Loan_Other);
            }
            Bind_sStatusT_Impl(ddl, orderedList.ToArray());

        }

        public static void Bind_sLeadStatusT(DropDownList ddl)
        {
            E_sStatusT[] list = {
                                    E_sStatusT.Lead_New,
                                    E_sStatusT.Lead_Canceled,
                                    E_sStatusT.Lead_Declined,
                                    E_sStatusT.Lead_Other,
                                };
            Bind_sStatusT_Impl(ddl, list);

        }

        public static void Bind_sStatusT_LoanFind(DropDownList ddl)
        {
            ddl.Items.Add(new ListItem("<-- Active Loan -->", "-2"));
            ddl.Items.Add(new ListItem("<-- Inactive Loan -->", "-3"));
            ddl.Items.Add(new ListItem("<-- Any Status -->", "-1"));
        }

        public static void Bind_sStatusT_LeadFind(DropDownList ddl)
        {

            ddl.Items.Add(new ListItem("<-- Active Lead -->", "-4"));
            ddl.Items.Add(new ListItem("<-- Inactive Lead -->", "-5"));
            ddl.Items.Add(new ListItem("<-- Any Status -->", "-1"));
        }

        public static void Bind_sStatusT_AssignLoans(DropDownList ddl)
        {
            var list = new List<E_sStatusT>();
            foreach (var status in CPageBase.s_sStatusT_Order)
            {
                if(!Tools.IsStatusLead(status) && !Tools.IsInactiveStatus(status))
                {
                    list.Add(status);
                }
            }
            list.Remove(E_sStatusT.Loan_WebConsumer);
            Bind_sStatusT_Impl(ddl, list.ToArray());
        }

        public static void Bind_sBuildingStatusT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sBuildingStatusT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Existing", E_sBuildingStatusT.Existing));
            ddl.Items.Add(Tools.CreateEnumListItem("Proposed", E_sBuildingStatusT.Proposed));
            ddl.Items.Add(Tools.CreateEnumListItem("Subject To Alter Improve Repair And Rehab", E_sBuildingStatusT.SubjectToAlterImproveRepairAndRehab));
            ddl.Items.Add(Tools.CreateEnumListItem("Substantially Rehabilitated", E_sBuildingStatusT.SubstantiallyRehabilitated));
            ddl.Items.Add(Tools.CreateEnumListItem("Under Construction", E_sBuildingStatusT.UnderConstruction));
        }
        public static void Bind_sBuydownContributorT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sBuydownContributorT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Borrower", E_sBuydownContributorT.Borrower));
            ddl.Items.Add(Tools.CreateEnumListItem("Other", E_sBuydownContributorT.Other));
            ddl.Items.Add(Tools.CreateEnumListItem("Lender Premium Financed", E_sBuydownContributorT.LenderPremiumFinanced));
            ddl.Items.Add(Tools.CreateEnumListItem("Seller", E_sBuydownContributorT.Seller));
            ddl.Items.Add(Tools.CreateEnumListItem("Builder", E_sBuydownContributorT.Builder));
            ddl.Items.Add(Tools.CreateEnumListItem("Unassigned", E_sBuydownContributorT.Unassigned));
            ddl.Items.Add(Tools.CreateEnumListItem("Parent", E_sBuydownContributorT.Parent));
            ddl.Items.Add(Tools.CreateEnumListItem("Non-Parent Relative", E_sBuydownContributorT.NonParentRelative));
            ddl.Items.Add(Tools.CreateEnumListItem("Unrelated Friend", E_sBuydownContributorT.UnrelatedFriend));
            ddl.Items.Add(Tools.CreateEnumListItem("Employer", E_sBuydownContributorT.Employer));
        }
        public static void Bind_sFreddieArmIndexT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sFreddieArmIndexT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("One Year Treasury", E_sFreddieArmIndexT.OneYearTreasury));
            ddl.Items.Add(Tools.CreateEnumListItem("Three Year Treasury", E_sFreddieArmIndexT.ThreeYearTreasury));
            ddl.Items.Add(Tools.CreateEnumListItem("Six Month Treasury", E_sFreddieArmIndexT.SixMonthTreasury));
            ddl.Items.Add(Tools.CreateEnumListItem("11th District Cost Of Funds", E_sFreddieArmIndexT.EleventhDistrictCostOfFunds));
            ddl.Items.Add(Tools.CreateEnumListItem("National Monthly Median Cost Of Funds", E_sFreddieArmIndexT.NationalMonthlyMedianCostOfFunds));
            ddl.Items.Add(Tools.CreateEnumListItem("LIBOR", E_sFreddieArmIndexT.LIBOR));
            ddl.Items.Add(Tools.CreateEnumListItem("Other", E_sFreddieArmIndexT.Other));
        }
        public static void Bind_sFreddieDocT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sFreddieDocT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Full Documentation", E_sFreddieDocT.FullDocumentation));
            ddl.Items.Add(Tools.CreateEnumListItem("No Deposit Verif", E_sFreddieDocT.NoDepositVerif));
            ddl.Items.Add(Tools.CreateEnumListItem("No Dep Emp Inc Verif", E_sFreddieDocT.NoDepEmpIncVerif));
            ddl.Items.Add(Tools.CreateEnumListItem("No Doc", E_sFreddieDocT.NoDoc));
            ddl.Items.Add(Tools.CreateEnumListItem("No Emp Inc Verif", E_sFreddieDocT.NoEmpIncVerif));
        }
        public static void Bind_sCondoProjClassT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sCondoProjClassT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("A/III Condominium", E_sCondoProjClassT.A_IIICondominium));
            ddl.Items.Add(Tools.CreateEnumListItem("B/II Condominium", E_sCondoProjClassT.B_IICondominium));
            ddl.Items.Add(Tools.CreateEnumListItem("C/I Condominium", E_sCondoProjClassT.C_ICondominium));
            ddl.Items.Add(Tools.CreateEnumListItem("Approved FHA/VA Condominium Project Or Spot Loan", E_sCondoProjClassT.ApprovedFHA_VACondominiumProjectOrSpotLoan));
        }
        public static void Bind_sGseRefPurposeT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sGseRefPurposeT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Cash Out Debt Consolidation", E_sGseRefPurposeT.CashOutDebtConsolidation));
            ddl.Items.Add(Tools.CreateEnumListItem("Cash Out Home Improvement", E_sGseRefPurposeT.CashOutHomeImprovement));
            ddl.Items.Add(Tools.CreateEnumListItem("Cash Out Limited", E_sGseRefPurposeT.CashOutLimited));
            ddl.Items.Add(Tools.CreateEnumListItem("Cash Out Other", E_sGseRefPurposeT.CashOutOther));
            ddl.Items.Add(Tools.CreateEnumListItem("No Cash Out FHA Streamlined Refinance", E_sGseRefPurposeT.NoCashOutFHAStreamlinedRefinance));
            ddl.Items.Add(Tools.CreateEnumListItem("No Cash Out FREOwned Refinance", E_sGseRefPurposeT.NoCashOutFREOwnedRefinance));
            ddl.Items.Add(Tools.CreateEnumListItem("No Cash Out Other", E_sGseRefPurposeT.NoCashOutOther));
            ddl.Items.Add(Tools.CreateEnumListItem("No Cash Out Streamlined Refinance", E_sGseRefPurposeT.NoCashOutStreamlinedRefinance));
            ddl.Items.Add(Tools.CreateEnumListItem("Change In Rate Term", E_sGseRefPurposeT.ChangeInRateTerm));
        }
        public static string Get_sGseSptFriendlyDisplay(E_sGseSpT value)
        {
            switch (value)
            {
                case E_sGseSpT.LeaveBlank:
                    return "";
                case E_sGseSpT.Attached:
                    return "Attached";
                case E_sGseSpT.Condominium:
                    return "Condominium";
                case E_sGseSpT.Cooperative:
                    return "Cooperative";
                case E_sGseSpT.Detached:
                    return "Detached";
                case E_sGseSpT.DetachedCondominium:
                    return "Detached Condominium";
                case E_sGseSpT.HighRiseCondominium:
                    return "High Rise Condominium";
                case E_sGseSpT.ManufacturedHomeCondominium:
                    return "Manufactured Home Condominium";
                case E_sGseSpT.ManufacturedHomeMultiwide:
                    return "Manufactured Home Multiwide";
                case E_sGseSpT.ManufacturedHousing:
                    return "Manufactured Housing";
                case E_sGseSpT.ManufacturedHousingSingleWide:
                    return "Manufactured Housing Single Wide";
                case E_sGseSpT.Modular:
                    return "Modular";
                case E_sGseSpT.PUD:
                    return "PUD";
                default:
                    throw new UnhandledEnumException(value);
            }
        }
        public static void Bind_sGseSpT(DropDownList ddl)
        {
            foreach (var option in Enum.GetValues(typeof(E_sGseSpT)))
            {
                ddl.Items.Add(Tools.CreateEnumListItem(Get_sGseSptFriendlyDisplay((E_sGseSpT)option), (E_sGseSpT)option));
            }
        }



        public static readonly IReadOnlyDictionary<E_sFredProcPointT, string> sFredProcPointT_map = new Dictionary<E_sFredProcPointT, string>
        {
            { E_sFredProcPointT.LeaveBlank, "" },
            { E_sFredProcPointT.Application, "Application" },
            { E_sFredProcPointT.FinalDisposition, "Final Disposition" },
            { E_sFredProcPointT.PostClosingQualityControl, "Post Closing Quality Control" },
            { E_sFredProcPointT.Prequalification, "Prequalification" },
            { E_sFredProcPointT.Underwriting, "Underwriting" },
        };

        public static IPair[] sFredProcPointT_pairs =
        {
            EnumRepMap.Create(Tools.sFredProcPointT_map, E_sFredProcPointT.LeaveBlank),
            EnumRepMap.Create(Tools.sFredProcPointT_map, E_sFredProcPointT.Application),
            EnumRepMap.Create(Tools.sFredProcPointT_map, E_sFredProcPointT.FinalDisposition),
            EnumRepMap.Create(Tools.sFredProcPointT_map, E_sFredProcPointT.PostClosingQualityControl),
            EnumRepMap.Create(Tools.sFredProcPointT_map, E_sFredProcPointT.Prequalification),
            EnumRepMap.Create(Tools.sFredProcPointT_map, E_sFredProcPointT.Underwriting),
        };

        public static void Bind_sNegAmortT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sNegAmortT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("No Negative Amort", E_sNegAmortT.NoNegAmort));
            ddl.Items.Add(Tools.CreateEnumListItem("Potential Negative Amort", E_sNegAmortT.PotentialNegAmort));
            ddl.Items.Add(Tools.CreateEnumListItem("Scheduled Negative Amort", E_sNegAmortT.ScheduledNegAmort));
        }

        public static void Bind_sSpProjClassT(DropDownList ddl)
        {

            ddl.Items.Add(Tools.CreateEnumListItem("", E_sSpProjClassT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("E PUD", E_sSpProjClassT.EPUD));
            ddl.Items.Add(Tools.CreateEnumListItem("F PUD", E_sSpProjClassT.FPUD));
            ddl.Items.Add(Tools.CreateEnumListItem("T PUD", E_sSpProjClassT.TPUD));
            ddl.Items.Add(Tools.CreateEnumListItem("1 CO-OP", E_sSpProjClassT.COOP1));
            ddl.Items.Add(Tools.CreateEnumListItem("2 CO-OP", E_sSpProjClassT.COOP2));
            ddl.Items.Add(Tools.CreateEnumListItem("T CO-OP", E_sSpProjClassT.TCoop));

            ddl.Items.Add(Tools.CreateEnumListItem("A/III Condo", E_sSpProjClassT.AIIICondo));
            ddl.Items.Add(Tools.CreateEnumListItem("B/II Condo", E_sSpProjClassT.BIICondo));
            ddl.Items.Add(Tools.CreateEnumListItem("C/I Condo", E_sSpProjClassT.CICondo));
            ddl.Items.Add(Tools.CreateEnumListItem("III PUD", E_sSpProjClassT.IIIPUD));

        }

        public static void Bind_sSpProjectStatusT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sSpProjectStatusT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Existing", E_sSpProjectStatusT.Existing));
            ddl.Items.Add(Tools.CreateEnumListItem("New", E_sSpProjectStatusT.New));
        }

        public static void Bind_sSpProjectAttachmentT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sSpProjectAttachmentT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Attached", E_sSpProjectAttachmentT.Attached));
            ddl.Items.Add(Tools.CreateEnumListItem("Detached", E_sSpProjectAttachmentT.Detached));
        }

        public static void Bind_sSpProjectDesignT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sSpProjectDesignT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Townhouse / Rowhouse", E_sSpProjectDesignT.Townhouse));
            ddl.Items.Add(Tools.CreateEnumListItem("Low-rise / Garden project", E_sSpProjectDesignT.LowRise));
            ddl.Items.Add(Tools.CreateEnumListItem("Mid-rise project", E_sSpProjectDesignT.MidRise));
            ddl.Items.Add(Tools.CreateEnumListItem("High-rise project", E_sSpProjectDesignT.HighRise));
            ddl.Items.Add(Tools.CreateEnumListItem("Other", E_sSpProjectDesignT.Other));
        }

        public static void Bind_sSpValuationMethodT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sSpValuationMethodT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Automated Valuation Model", E_sSpValuationMethodT.AutomatedValuationModel));
            ddl.Items.Add(Tools.CreateEnumListItem("Desktop Appraisal", E_sSpValuationMethodT.DesktopAppraisal));
            ddl.Items.Add(Tools.CreateEnumListItem("Drive By", E_sSpValuationMethodT.DriveBy));
            ddl.Items.Add(Tools.CreateEnumListItem("Full Appraisal", E_sSpValuationMethodT.FullAppraisal));
            ddl.Items.Add(Tools.CreateEnumListItem("Prior Appraisal Used", E_sSpValuationMethodT.PriorAppraisalUsed));
            ddl.Items.Add(Tools.CreateEnumListItem("Field Review", E_sSpValuationMethodT.FieldReview));
            ddl.Items.Add(Tools.CreateEnumListItem("Desk Review", E_sSpValuationMethodT.DeskReview));
            ddl.Items.Add(Tools.CreateEnumListItem("None", E_sSpValuationMethodT.None));
            ddl.Items.Add(Tools.CreateEnumListItem("Other", E_sSpValuationMethodT.Other));
        }

        public static void Bind_sSpAvmModelT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sSpAvmModelT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Automated Property Service", E_sSpAvmModelT.AutomatedPropertyService));
            ddl.Items.Add(Tools.CreateEnumListItem("Casa", E_sSpAvmModelT.Casa));
            ddl.Items.Add(Tools.CreateEnumListItem("Fidelity Hansen", E_sSpAvmModelT.FidelityHansen));
            ddl.Items.Add(Tools.CreateEnumListItem("Home Price Analyzer", E_sSpAvmModelT.HomePriceAnalyzer));
            ddl.Items.Add(Tools.CreateEnumListItem("Home Price Index", E_sSpAvmModelT.HomePriceIndex));
            ddl.Items.Add(Tools.CreateEnumListItem("Home Value Explorer", E_sSpAvmModelT.HomeValueExplorer));
            ddl.Items.Add(Tools.CreateEnumListItem("Indicator", E_sSpAvmModelT.Indicator));
            ddl.Items.Add(Tools.CreateEnumListItem("MTM", E_sSpAvmModelT.MTM));
            ddl.Items.Add(Tools.CreateEnumListItem("Net Value", E_sSpAvmModelT.NetValue));
            ddl.Items.Add(Tools.CreateEnumListItem("Pass", E_sSpAvmModelT.Pass));
            ddl.Items.Add(Tools.CreateEnumListItem("Property Survey Analysis Report", E_sSpAvmModelT.PropertySurveyAnalysisReport));
            ddl.Items.Add(Tools.CreateEnumListItem("Value Finder", E_sSpAvmModelT.ValueFinder));
            ddl.Items.Add(Tools.CreateEnumListItem("Value Point", E_sSpAvmModelT.ValuePoint));
            ddl.Items.Add(Tools.CreateEnumListItem("Value Point 4", E_sSpAvmModelT.ValuePoint4));
            ddl.Items.Add(Tools.CreateEnumListItem("Value Point Plus", E_sSpAvmModelT.ValuePointPlus));
            ddl.Items.Add(Tools.CreateEnumListItem("Value Sure", E_sSpAvmModelT.ValueSure));
            ddl.Items.Add(Tools.CreateEnumListItem("Value Wizard", E_sSpAvmModelT.ValueWizard));
            ddl.Items.Add(Tools.CreateEnumListItem("Value Wizard Plus", E_sSpAvmModelT.ValueWizardPlus));
            ddl.Items.Add(Tools.CreateEnumListItem("Vero Index Plus", E_sSpAvmModelT.VeroIndexPlus));
            ddl.Items.Add(Tools.CreateEnumListItem("Vero Value", E_sSpAvmModelT.VeroValue));
        }

        public static void Bind_sSpGseRefiProgramT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("None / Not Applicable", E_sSpGseRefiProgramT.None));
            ddl.Items.Add(Tools.CreateEnumListItem("DU Refi Plus", E_sSpGseRefiProgramT.DURefiPlus));
            ddl.Items.Add(Tools.CreateEnumListItem("Refi Plus", E_sSpGseRefiProgramT.RefiPlus));
            ddl.Items.Add(Tools.CreateEnumListItem("Relief Refinance � Open Access", E_sSpGseRefiProgramT.ReliefRefiOpenAccess));
            ddl.Items.Add(Tools.CreateEnumListItem("Relief Refinance � Same Servicer", E_sSpGseRefiProgramT.ReliefRefiSameServicer));
            ddl.Items.Add(Tools.CreateEnumListItem("FHLB Disaster Response", E_sSpGseRefiProgramT.FHLBDisasterResponse));
        }

        public static void Bind_sSpGseCollateralProgramT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("None / Not Applicable", E_sSpGseCollateralProgramT.None));
            ddl.Items.Add(Tools.CreateEnumListItem("DU Refi Plus Property Fieldwork Waiver", E_sSpGseCollateralProgramT.DURefiPlus));
            ddl.Items.Add(Tools.CreateEnumListItem("DU Property Inspection Report Form 2075", E_sSpGseCollateralProgramT.PropertyInspectionReportForm2075));
            ddl.Items.Add(Tools.CreateEnumListItem("LPA Property Inspection Report Form 2070", E_sSpGseCollateralProgramT.PropertyInspectionReportForm2070));
            ddl.Items.Add(Tools.CreateEnumListItem("Property Inspection Alternative", E_sSpGseCollateralProgramT.PropertyInspectionAlternative));
            ddl.Items.Add(Tools.CreateEnumListItem("Appraisal Waiver", E_sSpGseCollateralProgramT.PropertyInspectionWaiver));
        }

        public static void Bind_sMiInsuranceT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sMiInsuranceT.None));
            ddl.Items.Add(Tools.CreateEnumListItem("Borrower paid", E_sMiInsuranceT.BorrowerPaid));
            ddl.Items.Add(Tools.CreateEnumListItem("Lender paid", E_sMiInsuranceT.LenderPaid));
            ddl.Items.Add(Tools.CreateEnumListItem("Investor paid", E_sMiInsuranceT.InvestorPaid));
        }

        public static void Bind_sInsPaidBy(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("Borrower", E_sInsPaidByT.Borrower));
            ddl.Items.Add(Tools.CreateEnumListItem("Lender", E_sInsPaidByT.Lender));
        }

        public static void Bind_sInsPaidByWithHOA(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("Borrower", E_sInsPaidByT.Borrower));
            ddl.Items.Add(Tools.CreateEnumListItem("Lender", E_sInsPaidByT.Lender));
            ddl.Items.Add(Tools.CreateEnumListItem("HOA", E_sInsPaidByT.HOA));
        }

        public static void Bind_sMiReasonForAbsenceT(DropDownList ddl, bool includeBlankOption)
        {
            if (includeBlankOption)
            {
                ddl.Items.Add(Tools.CreateEnumListItem(string.Empty, E_sMiReasonForAbsenceT.None));
            }

            ddl.Items.Add(Tools.CreateEnumListItem("No MI based on original LTV", E_sMiReasonForAbsenceT.NoMiBasedOnOriginalLtv));
            ddl.Items.Add(Tools.CreateEnumListItem("No MI based on mortgage being refinanced", E_sMiReasonForAbsenceT.NoMIBasedOnMortgageBeingRefinanced));
            ddl.Items.Add(Tools.CreateEnumListItem("MI canceled based on current LTV", E_sMiReasonForAbsenceT.MiCanceledBasedOnCurrentLtv));
            ddl.Items.Add(Tools.CreateEnumListItem("Recourse in lieu of MI", E_sMiReasonForAbsenceT.RecourseInLieuOfMI));
            ddl.Items.Add(Tools.CreateEnumListItem("Indemnification in lieu of MI", E_sMiReasonForAbsenceT.IndemnificationInLieuOfMI));
            ddl.Items.Add(Tools.CreateEnumListItem("No MI based on investor requirements", E_sMiReasonForAbsenceT.NoMIBasedOnInvestorRequirements));
        }

        public static void Bind_sMiCompanyNmT(DropDownList ddl, E_sLT sLT)
        {
            var enumList = new List<E_sMiCompanyNmT>((IEnumerable<E_sMiCompanyNmT>)Enum.GetValues(typeof(E_sMiCompanyNmT)));
            if (sLT != E_sLT.FHA)
            {
                enumList.Remove(E_sMiCompanyNmT.FHA);
            }

            if (sLT != E_sLT.VA)
            {
                enumList.Remove(E_sMiCompanyNmT.VA);
            }

            if (sLT != E_sLT.UsdaRural)
            {
                enumList.Remove(E_sMiCompanyNmT.USDA);
            }

            foreach (E_sMiCompanyNmT sMiCompanyNmT in enumList)
            {
                ddl.Items.Add(Tools.CreateEnumListItem(Get_sMiCompanyNmTFriendlyDisplay(sMiCompanyNmT), sMiCompanyNmT));
            }
        }

        public static void Bind_sAppraisalNeededDate(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem(E_AppraisalNeededDateOptions.EnabledNotRequired.GetDescription(), E_AppraisalNeededDateOptions.EnabledNotRequired));
            ddl.Items.Add(Tools.CreateEnumListItem(E_AppraisalNeededDateOptions.Required.GetDescription(), E_AppraisalNeededDateOptions.Required));
            ddl.Items.Add(Tools.CreateEnumListItem(E_AppraisalNeededDateOptions.Hidden.GetDescription(), E_AppraisalNeededDateOptions.Hidden));
        }

        public static void Bind_sGseDeliveryRemittanceT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("Actual / Actual", E_sGseDeliveryRemittanceT.ActualActual));
            ddl.Items.Add(Tools.CreateEnumListItem("Scheduled / Actual", E_sGseDeliveryRemittanceT.ScheduledActual));
            ddl.Items.Add(Tools.CreateEnumListItem("Scheduled / Scheduled", E_sGseDeliveryRemittanceT.ScheduledScheduled));
        }

        public static void Bind_sGseDeliveryLoanDeliveryT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("Whole loan", E_sGseDeliveryLoanDeliveryT.WholeLoan));
            ddl.Items.Add(Tools.CreateEnumListItem("MBS", E_sGseDeliveryLoanDeliveryT.MBS));
        }

        public static void Bind_sGseDeliveryLoanDefaultLossPartyT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("Investor", E_sGseDeliveryLoanDefaultLossPartyT.Investor));
            ddl.Items.Add(Tools.CreateEnumListItem("Lender", E_sGseDeliveryLoanDefaultLossPartyT.Lender));
            ddl.Items.Add(Tools.CreateEnumListItem("Shared", E_sGseDeliveryLoanDefaultLossPartyT.Shared));
        }
        public static void Bind_sGseDeliveryTargetT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("Fannie Mae", E_sGseDeliveryTargetT.FannieMae));
            ddl.Items.Add(Tools.CreateEnumListItem("Freddie Mac", E_sGseDeliveryTargetT.FreddieMac));
            ddl.Items.Add(Tools.CreateEnumListItem("Ginnie Mae", E_sGseDeliveryTargetT.GinnieMae));
            ddl.Items.Add(Tools.CreateEnumListItem("Other/None", E_sGseDeliveryTargetT.Other_None));
        }
        public static void Bind_sGseDeliveryPayeeT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("Lender", E_sGseDeliveryPayeeT.Lender));
            ddl.Items.Add(Tools.CreateEnumListItem("Warehouse Lender", E_sGseDeliveryPayeeT.WarehouseLender));

        }

        public static void Bind_UcdDeliveredToEntity(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem(string.Empty, UcdDeliveredToEntity.Blank));
            ddl.Items.Add(Tools.CreateEnumListItem("Fannie Mae", UcdDeliveredToEntity.FannieMae));
            ddl.Items.Add(Tools.CreateEnumListItem("Freddie Mac", UcdDeliveredToEntity.FreddieMac));
        }

        /// <summary>
        /// Binds Freddie Mac products to a dropdown list, with obsolete products at the
        /// bottom and the remaining products alphabetized.
        /// </summary>
        /// <param name="ddl">The dropdown list to bind the data to.</param>
        public static void Bind_sGseFreddieMacProductT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sGseFreddieMacProductT.None));
            ddl.Items.Add(Tools.CreateEnumListItem("Alternate Requirements Desktop Underwriter", E_sGseFreddieMacProductT.AlternateRequirementsDesktopUnderwriter));
            ddl.Items.Add(Tools.CreateEnumListItem("Alternative Full Information", E_sGseFreddieMacProductT.AlternativeFullInformation));
            ddl.Items.Add(Tools.CreateEnumListItem("A-minus Mortgage", E_sGseFreddieMacProductT.AMinusMortgage));
            ddl.Items.Add(Tools.CreateEnumListItem("Builder or Developer Affiliated", E_sGseFreddieMacProductT.BuilderOrDeveloperAffiliated));
            ddl.Items.Add(Tools.CreateEnumListItem("Construction Conversion", E_sGseFreddieMacProductT.ConstructionConversion));
            ddl.Items.Add(Tools.CreateEnumListItem("Corr Advantage Loan", E_sGseFreddieMacProductT.CorrAdvantageLoan));
            ddl.Items.Add(Tools.CreateEnumListItem("Declining Balance Co-Ownership Initiative", E_sGseFreddieMacProductT.DecliningBalanceCoOwnershipInitiative));
            ddl.Items.Add(Tools.CreateEnumListItem("Disaster Relief Program", E_sGseFreddieMacProductT.DisasterReliefProgram));
            ddl.Items.Add(Tools.CreateEnumListItem("DreaMaker", E_sGseFreddieMacProductT.DreaMaker));
            ddl.Items.Add(Tools.CreateEnumListItem("Energy Conservation", E_sGseFreddieMacProductT.EnergyConservation));
            ddl.Items.Add(Tools.CreateEnumListItem("FRE-Owned Condo Project", E_sGseFreddieMacProductT.FREOwnedCondoProject));
            ddl.Items.Add(Tools.CreateEnumListItem("Home Opportunity", E_sGseFreddieMacProductT.HomeOpportunity));
            ddl.Items.Add(Tools.CreateEnumListItem("Home Possible Advantage", E_sGseFreddieMacProductT.HomePossibleAdvantage));
            ddl.Items.Add(Tools.CreateEnumListItem("Home Possible Advantage HFA", E_sGseFreddieMacProductT.HomePossibleAdvantageHFA));
            ddl.Items.Add(Tools.CreateEnumListItem("Home Possible Home Ready", E_sGseFreddieMacProductT.HomePossibleHomeReady));
            ddl.Items.Add(Tools.CreateEnumListItem("Home Possible MCM", E_sGseFreddieMacProductT.HomePossibleMCM));
            ddl.Items.Add(Tools.CreateEnumListItem("Home Possible MCMCS", E_sGseFreddieMacProductT.HomePossibleMCMCS));
            ddl.Items.Add(Tools.CreateEnumListItem("Home Possible Mortgage", E_sGseFreddieMacProductT.HomePossibleMortgage));
            ddl.Items.Add(Tools.CreateEnumListItem("Loans to Facilitate REO Sales", E_sGseFreddieMacProductT.LoansToFacilitateREOSales));
            ddl.Items.Add(Tools.CreateEnumListItem("Long Term Standby", E_sGseFreddieMacProductT.LongTermStandBy));
            ddl.Items.Add(Tools.CreateEnumListItem("Mortgage Revenue Bond", E_sGseFreddieMacProductT.MortgageRevenueBond));
            ddl.Items.Add(Tools.CreateEnumListItem("Mortgage Rewards Program", E_sGseFreddieMacProductT.MortgageRewardsProgram));
            ddl.Items.Add(Tools.CreateEnumListItem("Murabaha Mortgage", E_sGseFreddieMacProductT.MurabahaMortgage));
            ddl.Items.Add(Tools.CreateEnumListItem("Negotiated 97 Percent LTV Loan Program", E_sGseFreddieMacProductT.Negotiated97PercentLTVLoanProgram));
            ddl.Items.Add(Tools.CreateEnumListItem("Neighborhood Champions", E_sGseFreddieMacProductT.NeighborhoodChampions));
            ddl.Items.Add(Tools.CreateEnumListItem("No Fee Mortage Plus", E_sGseFreddieMacProductT.NoFeeMortgagePlus));
            ddl.Items.Add(Tools.CreateEnumListItem("Optimum Mortgage Program", E_sGseFreddieMacProductT.OptimumMortgageProgram));
            ddl.Items.Add(Tools.CreateEnumListItem("Recourse Guaranteed By Third Party", E_sGseFreddieMacProductT.RecourseGuaranteedByThirdParty));
            ddl.Items.Add(Tools.CreateEnumListItem("Renovation", E_sGseFreddieMacProductT.Renovation));
            ddl.Items.Add(Tools.CreateEnumListItem("Short Term Standby", E_sGseFreddieMacProductT.ShortTermStandBy));
            ddl.Items.Add(Tools.CreateEnumListItem("Solar Initiative", E_sGseFreddieMacProductT.SolarInitiative));
            ddl.Items.Add(Tools.CreateEnumListItem("Home Possible 97 (Obsolete)", E_sGseFreddieMacProductT.HomePossible97));
            ddl.Items.Add(Tools.CreateEnumListItem("Home Possible Neighborhood Solution 97 (Obsolete)", E_sGseFreddieMacProductT.HomePossibleNeighborhoodSolution97));
        }

        public static void Bind_sGseDeliveryREOMarketingPartyT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("Investor", E_sGseDeliveryREOMarketingPartyT.Investor));
            ddl.Items.Add(Tools.CreateEnumListItem("Lender", E_sGseDeliveryREOMarketingPartyT.Lender));
        }

        public static void Bind_aHomeOwnershipCounselingSourceT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_aHomeOwnershipCounselingSourceT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("No Borrower Counseling", E_aHomeOwnershipCounselingSourceT.NoBorrowerCounseling));
            ddl.Items.Add(Tools.CreateEnumListItem("Government Agency", E_aHomeOwnershipCounselingSourceT.GovernmentAgency));
            ddl.Items.Add(Tools.CreateEnumListItem("HUD Approved Counseling Agency", E_aHomeOwnershipCounselingSourceT.HUDApprovedCounselingAgency));
            ddl.Items.Add(Tools.CreateEnumListItem("Lender Trained Counseling", E_aHomeOwnershipCounselingSourceT.LenderTrainedCounseling));
            ddl.Items.Add(Tools.CreateEnumListItem("Mortgage Insurance Company", E_aHomeOwnershipCounselingSourceT.MortgageInsuranceCompany));
            ddl.Items.Add(Tools.CreateEnumListItem("Non-Profit Organization", E_aHomeOwnershipCounselingSourceT.NonProfitOrganization));
            ddl.Items.Add(Tools.CreateEnumListItem("Borrower Did Not Participate", E_aHomeOwnershipCounselingSourceT.BorrowerDidNotParticipate));
        }

        public static void Bind_aHomeOwnershipCounselingFormatT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_aHomeOwnershipCounselingFormatT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Borrower Education Not Required", E_aHomeOwnershipCounselingFormatT.BorrowerEducationNotRequired));
            ddl.Items.Add(Tools.CreateEnumListItem("Classroom", E_aHomeOwnershipCounselingFormatT.Classroom));
            ddl.Items.Add(Tools.CreateEnumListItem("Home Study", E_aHomeOwnershipCounselingFormatT.HomeStudy));
            ddl.Items.Add(Tools.CreateEnumListItem("Individual", E_aHomeOwnershipCounselingFormatT.Individual));
            ddl.Items.Add(Tools.CreateEnumListItem("Borrower Did Not Participate", E_aHomeOwnershipCounselingFormatT.BorrowerDidNotParticipate));
        }

        public static void Bind_sCommunityLendingDownPaymentSourceT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sCommunityLendingDownPaymentSourceT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Borrower", E_sCommunityLendingDownPaymentSourceT.Borrower));
            ddl.Items.Add(Tools.CreateEnumListItem("Community Non-Profit", E_sCommunityLendingDownPaymentSourceT.CommunityNonProfit));
            ddl.Items.Add(Tools.CreateEnumListItem("Employer", E_sCommunityLendingDownPaymentSourceT.Employer));
            ddl.Items.Add(Tools.CreateEnumListItem("Federal Agency", E_sCommunityLendingDownPaymentSourceT.FederalAgency));
            ddl.Items.Add(Tools.CreateEnumListItem("FHLB Affordable Housing Program", E_sCommunityLendingDownPaymentSourceT.FHLBAffordableHousingProgram));
            ddl.Items.Add(Tools.CreateEnumListItem("Local Agency", E_sCommunityLendingDownPaymentSourceT.LocalAgency));
            ddl.Items.Add(Tools.CreateEnumListItem("Relative", E_sCommunityLendingDownPaymentSourceT.Relative));
            ddl.Items.Add(Tools.CreateEnumListItem("Religious Non-Profit", E_sCommunityLendingDownPaymentSourceT.ReligiousNonProfit));
            ddl.Items.Add(Tools.CreateEnumListItem("State Agency", E_sCommunityLendingDownPaymentSourceT.StateAgency));
            ddl.Items.Add(Tools.CreateEnumListItem("USDA Rural Housing", E_sCommunityLendingDownPaymentSourceT.UsdaRuralHousing));
            ddl.Items.Add(Tools.CreateEnumListItem("Originating Lender", E_sCommunityLendingDownPaymentSourceT.OriginatingLender));
            ddl.Items.Add(Tools.CreateEnumListItem("Grant", E_sCommunityLendingDownPaymentSourceT.Grant));
        }

        public static void Bind_sCommunityLendingDownPaymentSourceTWithAggregate(DropDownList ddl)
        {
            Bind_sCommunityLendingDownPaymentSourceT(ddl);
            ddl.Items.Add(Tools.CreateEnumListItem("Aggregated Remaining Types", E_sCommunityLendingDownPaymentSourceT.AggregatedRemainingTypes));
        }

        public static void Bind_sCommunityLendingDownPaymentT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sCommunityLendingDownPaymentT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Bridge Loan", E_sCommunityLendingDownPaymentT.BridgeLoan));
            ddl.Items.Add(Tools.CreateEnumListItem("Cash On Hand", E_sCommunityLendingDownPaymentT.CashOnHand));
            ddl.Items.Add(Tools.CreateEnumListItem("Checking Savings", E_sCommunityLendingDownPaymentT.CheckingSavings));
            ddl.Items.Add(Tools.CreateEnumListItem("Gift Funds", E_sCommunityLendingDownPaymentT.GiftFunds));
            ddl.Items.Add(Tools.CreateEnumListItem("Secondary Financing - Closed End", E_sCommunityLendingDownPaymentT.SecondaryFinancingClosedEnd));
            ddl.Items.Add(Tools.CreateEnumListItem("Secondary Financing - HELOC ", E_sCommunityLendingDownPaymentT.SecondaryFinancingHELOC));
            ddl.Items.Add(Tools.CreateEnumListItem("Secured Borrowed Funds", E_sCommunityLendingDownPaymentT.SecuredBorrowedFunds));
            ddl.Items.Add(Tools.CreateEnumListItem("Sweat Equity", E_sCommunityLendingDownPaymentT.SweatEquity));
            ddl.Items.Add(Tools.CreateEnumListItem("Unsecured Borrowed Funds", E_sCommunityLendingDownPaymentT.UnsecuredBorrowedFunds));
            ddl.Items.Add(Tools.CreateEnumListItem("Life Insurance Cash Value", E_sCommunityLendingDownPaymentT.LifeInsuranceCashValue));
            ddl.Items.Add(Tools.CreateEnumListItem("Lot Equity", E_sCommunityLendingDownPaymentT.LotEquity));
            ddl.Items.Add(Tools.CreateEnumListItem("Grant", E_sCommunityLendingDownPaymentT.Grant));
            ddl.Items.Add(Tools.CreateEnumListItem("Rent With Option To Purchase", E_sCommunityLendingDownPaymentT.RentWithOptionToPurchase));
            ddl.Items.Add(Tools.CreateEnumListItem("Retirement funds", E_sCommunityLendingDownPaymentT.RetirementFunds));
            ddl.Items.Add(Tools.CreateEnumListItem("Sale of Chattel", E_sCommunityLendingDownPaymentT.SaleOfChattel));
            ddl.Items.Add(Tools.CreateEnumListItem("Stocks and Bonds", E_sCommunityLendingDownPaymentT.StocksAndBonds));
            ddl.Items.Add(Tools.CreateEnumListItem("Trade Equity", E_sCommunityLendingDownPaymentT.TradeEquity));
            ddl.Items.Add(Tools.CreateEnumListItem("Trust Funds", E_sCommunityLendingDownPaymentT.TrustFunds));
            ddl.Items.Add(Tools.CreateEnumListItem("Equity On Sold Property", E_sCommunityLendingDownPaymentT.EquityOnSoldProperty));
            ddl.Items.Add(Tools.CreateEnumListItem("Equity On Subject Property", E_sCommunityLendingDownPaymentT.EquityOnSubjetProperty));
            ddl.Items.Add(Tools.CreateEnumListItem("Forgivable Secured Loan", E_sCommunityLendingDownPaymentT.ForgivableSecuredLoan));

        }

        public static void Bind_sCommunityLendingDownPaymentTWithAggregate(DropDownList ddl)
        {
            Bind_sCommunityLendingDownPaymentT(ddl);
            ddl.Items.Add(Tools.CreateEnumListItem("Aggregated Remaining Types", E_sCommunityLendingDownPaymentT.AggregatedRemainingTypes));
        }

        public static void Bind_sCommunityLendingClosingCostSourceT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sCommunityLendingClosingCostSourceT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Borrower", E_sCommunityLendingClosingCostSourceT.Borrower));
            ddl.Items.Add(Tools.CreateEnumListItem("Community Non-Profit", E_sCommunityLendingClosingCostSourceT.CommunityNonProfit));
            ddl.Items.Add(Tools.CreateEnumListItem("Employer", E_sCommunityLendingClosingCostSourceT.Employer));
            ddl.Items.Add(Tools.CreateEnumListItem("Federal Agency", E_sCommunityLendingClosingCostSourceT.FederalAgency));
            ddl.Items.Add(Tools.CreateEnumListItem("FHLB Affordable Housing Program", E_sCommunityLendingClosingCostSourceT.FHLBAffordableHousingProgram));
            ddl.Items.Add(Tools.CreateEnumListItem("Lender", E_sCommunityLendingClosingCostSourceT.Lender));
            ddl.Items.Add(Tools.CreateEnumListItem("Local Agency", E_sCommunityLendingClosingCostSourceT.LocalAgency));
            ddl.Items.Add(Tools.CreateEnumListItem("Property Seller", E_sCommunityLendingClosingCostSourceT.PropertySeller));
            ddl.Items.Add(Tools.CreateEnumListItem("Relative", E_sCommunityLendingClosingCostSourceT.Relative));
            ddl.Items.Add(Tools.CreateEnumListItem("Religious Non-Profit", E_sCommunityLendingClosingCostSourceT.ReligiousNonProfit));
            ddl.Items.Add(Tools.CreateEnumListItem("State Agency", E_sCommunityLendingClosingCostSourceT.StateAgency));
            ddl.Items.Add(Tools.CreateEnumListItem("USDA Rural Housing", E_sCommunityLendingClosingCostSourceT.UsdaRuralHousing));
        }

        public static void Bind_sCommunityLendingClosingCostSourceTWithAggregate(DropDownList ddl)
        {
            Bind_sCommunityLendingClosingCostSourceT(ddl);
            ddl.Items.Add(Tools.CreateEnumListItem("Aggregated Remaining Types", E_sCommunityLendingClosingCostSourceT.AggregatedRemainingTypes));
        }

        public static void Bind_sCommunityLendingClosingCostT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sCommunityLendingClosingCostT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Bridge Loan", E_sCommunityLendingClosingCostT.BridgeLoan));
            ddl.Items.Add(Tools.CreateEnumListItem("Cash On Hand", E_sCommunityLendingClosingCostT.CashOnHand));
            ddl.Items.Add(Tools.CreateEnumListItem("Checking / Savings", E_sCommunityLendingClosingCostT.CheckingSavings));
            ddl.Items.Add(Tools.CreateEnumListItem("Contribution", E_sCommunityLendingClosingCostT.Contribution));
            ddl.Items.Add(Tools.CreateEnumListItem("Credit Card", E_sCommunityLendingClosingCostT.CreditCard));
            ddl.Items.Add(Tools.CreateEnumListItem("Gift Funds", E_sCommunityLendingClosingCostT.GiftFunds));
            ddl.Items.Add(Tools.CreateEnumListItem("Grant", E_sCommunityLendingClosingCostT.Grant));
            ddl.Items.Add(Tools.CreateEnumListItem("Premium Funds", E_sCommunityLendingClosingCostT.PremiumFunds));
            ddl.Items.Add(Tools.CreateEnumListItem("Secondary Financing - Closed End", E_sCommunityLendingClosingCostT.SecondaryClosedEnd));
            ddl.Items.Add(Tools.CreateEnumListItem("Secondary Financing - HELOC", E_sCommunityLendingClosingCostT.SecondaryHELOC));
            ddl.Items.Add(Tools.CreateEnumListItem("Secured Loan", E_sCommunityLendingClosingCostT.SecuredLoan));
            ddl.Items.Add(Tools.CreateEnumListItem("Sweat Equity", E_sCommunityLendingClosingCostT.SweatEquity));
            ddl.Items.Add(Tools.CreateEnumListItem("Unsecured Borrowed Funds", E_sCommunityLendingClosingCostT.UnsecuredBorrowedFunds));
            ddl.Items.Add(Tools.CreateEnumListItem("Equity On Sold Property", E_sCommunityLendingClosingCostT.EquityOnSoldProperty));
            ddl.Items.Add(Tools.CreateEnumListItem("Equity On Subject Property", E_sCommunityLendingClosingCostT.EquityOnSubjectProperty));
            ddl.Items.Add(Tools.CreateEnumListItem("Forgivable Secured Loan", E_sCommunityLendingClosingCostT.ForgivableSecuredLoan));
            ddl.Items.Add(Tools.CreateEnumListItem("Life Insurance Cash Value", E_sCommunityLendingClosingCostT.LifeInsuranceCashValue));
            ddl.Items.Add(Tools.CreateEnumListItem("Lot Equity", E_sCommunityLendingClosingCostT.LotEquity));
            ddl.Items.Add(Tools.CreateEnumListItem("Rent With Option To Purchase", E_sCommunityLendingClosingCostT.RentWithOptionToPurchase));
            ddl.Items.Add(Tools.CreateEnumListItem("Retirement Funds", E_sCommunityLendingClosingCostT.RetirementFunds));
            ddl.Items.Add(Tools.CreateEnumListItem("Sale of Chattel", E_sCommunityLendingClosingCostT.SaleOfChattel));
            ddl.Items.Add(Tools.CreateEnumListItem("Stocks and Bonds", E_sCommunityLendingClosingCostT.StocksAndBonds));
            ddl.Items.Add(Tools.CreateEnumListItem("Trade Equity", E_sCommunityLendingClosingCostT.TradeEquity));
            ddl.Items.Add(Tools.CreateEnumListItem("Trust Funds", E_sCommunityLendingClosingCostT.TrustFunds));
        }

        public static void Bind_sCommunityLendingClosingCostTWithAggregate(DropDownList ddl)
        {
            Bind_sCommunityLendingClosingCostT(ddl);
            ddl.Items.Add(Tools.CreateEnumListItem("Aggregated Remaining Types", E_sCommunityLendingClosingCostT.AggregatedRemainingTypes));
        }

        public static void Bind_sSpProjectClassFannieT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sSpProjectClassFannieT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("G Not in Project", E_sSpProjectClassFannieT.GNotInProject));
            ddl.Items.Add(Tools.CreateEnumListItem("P Limited Review New", E_sSpProjectClassFannieT.PLimitedReviewNew));
            ddl.Items.Add(Tools.CreateEnumListItem("Q Limited Review Est.", E_sSpProjectClassFannieT.QLimitedReviewEst));
            ddl.Items.Add(Tools.CreateEnumListItem("R Expedited Review New", E_sSpProjectClassFannieT.RExpeditedReviewNew));
            ddl.Items.Add(Tools.CreateEnumListItem("S Expedited Review Est.", E_sSpProjectClassFannieT.SExpeditedReviewEst));
            ddl.Items.Add(Tools.CreateEnumListItem("T Fannie Mae Review", E_sSpProjectClassFannieT.TFannieReview));
            ddl.Items.Add(Tools.CreateEnumListItem("U FHA-Approved", E_sSpProjectClassFannieT.UFhaApproved));
            ddl.Items.Add(Tools.CreateEnumListItem("V Refi Plus/Site Condo/LCOR w/o condo review", E_sSpProjectClassFannieT.VRefiPlus));
            ddl.Items.Add(Tools.CreateEnumListItem("E PUD", E_sSpProjectClassFannieT.EPud));
            ddl.Items.Add(Tools.CreateEnumListItem("F PUD", E_sSpProjectClassFannieT.FPud));
            ddl.Items.Add(Tools.CreateEnumListItem("T PUD", E_sSpProjectClassFannieT.TPud));
            ddl.Items.Add(Tools.CreateEnumListItem("1 Co-op", E_sSpProjectClassFannieT._1Coop));
            ddl.Items.Add(Tools.CreateEnumListItem("2 Co-op", E_sSpProjectClassFannieT._2Coop));
            ddl.Items.Add(Tools.CreateEnumListItem("T Co-op", E_sSpProjectClassFannieT.TCoop));

        }

        public static void Bind_sLoanBeingRefinancedAmortizationT(DropDownList ddl)
        {
            foreach (E_sLoanBeingRefinancedAmortizationT item in Enum.GetValues(typeof(E_sLoanBeingRefinancedAmortizationT)))
            {
                ddl.Items.Add(Tools.CreateEnumListItem(item.GetDescription(), item));
            }
        }

        public static void Bind_sLoanBeingRefinancedLienPosT(DropDownList ddl)
        {
            foreach (E_sLoanBeingRefinancedLienPosT item in Enum.GetValues(typeof(E_sLoanBeingRefinancedLienPosT)))
            {
                ddl.Items.Add(Tools.CreateEnumListItem(item.GetDescription(), item));
            }
        }

        public static void Bind_sSpProjectClassFreddieT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sSpProjectClassFreddieT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Streamlined Review", E_sSpProjectClassFreddieT.FreddieMacStreamlinedReview));
            ddl.Items.Add(Tools.CreateEnumListItem("Established Project", E_sSpProjectClassFreddieT.FreddieMacEstablishedProject));
            ddl.Items.Add(Tools.CreateEnumListItem("New Project", E_sSpProjectClassFreddieT.FreddieMacNewProject));
            ddl.Items.Add(Tools.CreateEnumListItem("Detached Project", E_sSpProjectClassFreddieT.FreddieMacDetachedProject));
            ddl.Items.Add(Tools.CreateEnumListItem("2- to 4-unit Project", E_sSpProjectClassFreddieT.FreddieMac2To4UnitProject));
            //ddl.Items.Add(Tools.CreateEnumListItem("Reciprocal Review", E_sSpProjectClassFreddieT.FreddieMacReciprocalReview));
            ddl.Items.Add(Tools.CreateEnumListItem("Reciprocal Review - CPM", E_sSpProjectClassFreddieT.FreddieMacReciprocalReviewCPM));
            ddl.Items.Add(Tools.CreateEnumListItem("Reciprocal Review - PERS", E_sSpProjectClassFreddieT.FreddieMacReciprocalReviewPERS));
            ddl.Items.Add(Tools.CreateEnumListItem("Reciprocal Review - FHA", E_sSpProjectClassFreddieT.FreddieMacReciprocalReviewFHA));
        }
        public static void Bind_sMOrigT(DropDownList ddl)
        {

            ddl.Items.Add(Tools.CreateEnumListItem("", E_sMOrigT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Seller", E_sMOrigT.Seller));
            ddl.Items.Add(Tools.CreateEnumListItem("Third Party", E_sMOrigT.ThirdParty));
        }
        public static void Bind_s1stMOwnerT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_s1stMOwnerT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Fannie Mae", E_s1stMOwnerT.FannieMae));
            ddl.Items.Add(Tools.CreateEnumListItem("Freddie Mac", E_s1stMOwnerT.FreddieMac));
            ddl.Items.Add(Tools.CreateEnumListItem("Seller / Other", E_s1stMOwnerT.SellerOrOther));
        }
        public static void Bind_sQualIRDeriveT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("Note Rate Dependent", E_sQualIRDeriveT.NoteRateDependent));
            ddl.Items.Add(Tools.CreateEnumListItem("Bought Down Rate", E_sQualIRDeriveT.BoughtDownRate));
            ddl.Items.Add(Tools.CreateEnumListItem("Other", E_sQualIRDeriveT.Other));
        }

        public static void Bind_QualTermCalculationType(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("Standard Term", QualTermCalculationType.Standard));
            ddl.Items.Add(Tools.CreateEnumListItem("Amortizing Term", QualTermCalculationType.Amortizing));
            ddl.Items.Add(Tools.CreateEnumListItem("Interest Only Term", QualTermCalculationType.InterestOnly));
            ddl.Items.Add(Tools.CreateEnumListItem("Manual", QualTermCalculationType.Manual));
        }

        public static void Bind_aVaEntitleCode(ComboBox cb)
        {
            cb.Items.Add("01");
            cb.Items.Add("02");
            cb.Items.Add("03");
            cb.Items.Add("04");
            cb.Items.Add("05");
            cb.Items.Add("06");
            cb.Items.Add("07");
            cb.Items.Add("08");
            cb.Items.Add("09");
            cb.Items.Add("10");
            cb.Items.Add("11");
        }

        public static void Bind_aVaServiceBranchT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_aVaServiceBranchT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Army", E_aVaServiceBranchT.Army));
            ddl.Items.Add(Tools.CreateEnumListItem("Navy", E_aVaServiceBranchT.Navy));
            ddl.Items.Add(Tools.CreateEnumListItem("Air Force", E_aVaServiceBranchT.AirForce));
            ddl.Items.Add(Tools.CreateEnumListItem("Marine Corps", E_aVaServiceBranchT.Marine));
            ddl.Items.Add(Tools.CreateEnumListItem("Coast Guard", E_aVaServiceBranchT.CoastGuard));
            ddl.Items.Add(Tools.CreateEnumListItem("Other", E_aVaServiceBranchT.Other));
        }
        public static void Bind_aVaMilitaryStatT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_aVaMilitaryStatT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Separated From Service", E_aVaMilitaryStatT.SeparatedFromService));
            ddl.Items.Add(Tools.CreateEnumListItem("In Service", E_aVaMilitaryStatT.InService));

        }

        public static void Bind_sVaLPurposeT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sVaLPurposeT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Home", E_sVaLPurposeT.Home));
            ddl.Items.Add(Tools.CreateEnumListItem("Manufactured Home", E_sVaLPurposeT.ManufacturedHome));
            ddl.Items.Add(Tools.CreateEnumListItem("Condominium", E_sVaLPurposeT.Condo));
            ddl.Items.Add(Tools.CreateEnumListItem("Alterations/Improvements", E_sVaLPurposeT.Alteration));
            ddl.Items.Add(Tools.CreateEnumListItem("Refinance", E_sVaLPurposeT.Refinance));
        }
        public static void Bind_sVaLCodeT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sVaLCodeT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Purchase", E_sVaLCodeT.Purchase));
            ddl.Items.Add(Tools.CreateEnumListItem("IRRRL", E_sVaLCodeT.Irrrl));
            ddl.Items.Add(Tools.CreateEnumListItem("Regular (\"Cash-out\") Refi", E_sVaLCodeT.CashoutRefin));
            ddl.Items.Add(Tools.CreateEnumListItem("Manufactured Home Refi", E_sVaLCodeT.ManufacturedHomeRefi));
            ddl.Items.Add(Tools.CreateEnumListItem("Refi of Construction Loan...", E_sVaLCodeT.RefiOver90Rv));
        }
        public static void Bind_sVaFinMethT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sVaFinMethT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Regular Fixed Payment", E_sVaFinMethT.RegularFixed));
            ddl.Items.Add(Tools.CreateEnumListItem("GPM-Never to exceed NOV", E_sVaFinMethT.GpmNeverExceedCrv));
            ddl.Items.Add(Tools.CreateEnumListItem("Other GPMs", E_sVaFinMethT.GpmOther));
            ddl.Items.Add(Tools.CreateEnumListItem("GEM", E_sVaFinMethT.Gem));
            ddl.Items.Add(Tools.CreateEnumListItem("Temporary Buydown", E_sVaFinMethT.TemporaryBuydown));
            ddl.Items.Add(Tools.CreateEnumListItem("Hybrid ARM", E_sVaFinMethT.HybridArm));
            ddl.Items.Add(Tools.CreateEnumListItem("ARM", E_sVaFinMethT.ARM));
        }
        public static void Bind_sVaHybridArmT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sVaHybridArmT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("3/1", E_sVaHybridArmT.ThreeOne));
            ddl.Items.Add(Tools.CreateEnumListItem("5/1", E_sVaHybridArmT.FiveOne));
            ddl.Items.Add(Tools.CreateEnumListItem("7/1", E_sVaHybridArmT.SevenEleven));
            ddl.Items.Add(Tools.CreateEnumListItem("10/1", E_sVaHybridArmT.TenOne));
        }
        public static void Bind_sVaOwnershipT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sVaOwnershipT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Sole Ownership", E_sVaOwnershipT.SoleOwnership));
            ddl.Items.Add(Tools.CreateEnumListItem("Joint - 2 or more veterans", E_sVaOwnershipT.JointWithOtherVets));
            ddl.Items.Add(Tools.CreateEnumListItem("Joint - Veteran/Non-Veteran", E_sVaOwnershipT.JointWithNonVet));
        }
        public static void Bind_sVaApprT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sVaApprT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("IND - Single Property", E_sVaApprT.SingleProp));
            ddl.Items.Add(Tools.CreateEnumListItem("ONE - Master CRV Case", E_sVaApprT.MasterCrvCase));
            ddl.Items.Add(Tools.CreateEnumListItem("LAPP - Lender Appraisal", E_sVaApprT.LappLenderAppr));
            ddl.Items.Add(Tools.CreateEnumListItem("MBL - Manufactured Home", E_sVaApprT.ManufacturedHome));
            ddl.Items.Add(Tools.CreateEnumListItem("HUD - Conversion", E_sVaApprT.HudVaConversion));
            ddl.Items.Add(Tools.CreateEnumListItem("PMC - Prop. Mgmt. Case", E_sVaApprT.PropMgmtCase));
        }
        public static void Bind_sVaStructureT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sVaStructureT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Conventional", E_sVaStructureT.Conventional));
            ddl.Items.Add(Tools.CreateEnumListItem("Single Wide M/H", E_sVaStructureT.SingleWideMobil));
            ddl.Items.Add(Tools.CreateEnumListItem("Double Wide M/H", E_sVaStructureT.DoubleWideMobil));
            ddl.Items.Add(Tools.CreateEnumListItem("M/H Lot Only", E_sVaStructureT.MobilLotOnly));
            ddl.Items.Add(Tools.CreateEnumListItem("Prefabricated Home", E_sVaStructureT.PrefabricatedHome));
            ddl.Items.Add(Tools.CreateEnumListItem("Condominium Conversion", E_sVaStructureT.CondoConversion));
        }
        public static void Bind_sVaPropDesignationT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sVaPropDesignationT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Existing or Used Home, Condo, M/H", E_sVaPropDesignationT.ExistingOcc));
            ddl.Items.Add(Tools.CreateEnumListItem("Appraised As Proposed Construction", E_sVaPropDesignationT.ProposedConstruction));
            ddl.Items.Add(Tools.CreateEnumListItem("New Existing - Never Occupied", E_sVaPropDesignationT.ExistingNew));
            ddl.Items.Add(Tools.CreateEnumListItem("Energy Improvements", E_sVaPropDesignationT.EnergyImprovement));
        }
        public static void Bind_sVaManufacturedHomeT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sVaManufacturedHomeT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Other - Not M/H", E_sVaManufacturedHomeT.NotMobilHome));
            ddl.Items.Add(Tools.CreateEnumListItem("M/H Only (Rented Space", E_sVaManufacturedHomeT.MobilHomeOnly));
            ddl.Items.Add(Tools.CreateEnumListItem("M/H Only (Veteran-Owned Lot)", E_sVaManufacturedHomeT.MobilHomeVetOwnedLot));
            ddl.Items.Add(Tools.CreateEnumListItem("M/H On Permanent Foundation", E_sVaManufacturedHomeT.MobilHomeOnPermanentFoundation));
        }
        public static void Bind_sVaAutoUnderwritingT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sVaAutoUnderwritingT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("LPA", E_sVaAutoUnderwritingT.Lp));
            ddl.Items.Add(Tools.CreateEnumListItem("DU", E_sVaAutoUnderwritingT.Du));
            ddl.Items.Add(Tools.CreateEnumListItem("PMI AURA", E_sVaAutoUnderwritingT.PmiAura));
            ddl.Items.Add(Tools.CreateEnumListItem("CLUES", E_sVaAutoUnderwritingT.Clues));
            ddl.Items.Add(Tools.CreateEnumListItem("ZIPPY", E_sVaAutoUnderwritingT.Zippy));
        }
        public static void Bind_sVaRiskT(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_sVaRiskT.LeaveBlank));
            ddl.Items.Add(Tools.CreateEnumListItem("Approve", E_sVaRiskT.Approve));
            ddl.Items.Add(Tools.CreateEnumListItem("Refer", E_sVaRiskT.Refer));
        }
        public static void Bind_sSpUtilT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_sSpUtilT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Public", E_sSpUtilT.Public));
            ddl.Items.Add(CreateEnumListItem("Community", E_sSpUtilT.Community));
            ddl.Items.Add(CreateEnumListItem("Individual", E_sSpUtilT.Individual));
        }
        public static void Bind_sSpMonthBuiltT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_sSpMonthBuiltT.Blank));
            ddl.Items.Add(CreateEnumListItem("Jan", E_sSpMonthBuiltT.January));
            ddl.Items.Add(CreateEnumListItem("Feb", E_sSpMonthBuiltT.February));
            ddl.Items.Add(CreateEnumListItem("Mar", E_sSpMonthBuiltT.March));
            ddl.Items.Add(CreateEnumListItem("Apr", E_sSpMonthBuiltT.April));
            ddl.Items.Add(CreateEnumListItem("May", E_sSpMonthBuiltT.May));
            ddl.Items.Add(CreateEnumListItem("Jun", E_sSpMonthBuiltT.June));
            ddl.Items.Add(CreateEnumListItem("July", E_sSpMonthBuiltT.July));
            ddl.Items.Add(CreateEnumListItem("Aug", E_sSpMonthBuiltT.August));
            ddl.Items.Add(CreateEnumListItem("Sept", E_sSpMonthBuiltT.September));
            ddl.Items.Add(CreateEnumListItem("Oct", E_sSpMonthBuiltT.October));
            ddl.Items.Add(CreateEnumListItem("Nov", E_sSpMonthBuiltT.November));
            ddl.Items.Add(CreateEnumListItem("Dec", E_sSpMonthBuiltT.December));
        }
        public static void Bind_sFHACaseT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("DE VA/CRV", E_sFHACaseT.DE_VACRV));
            ddl.Items.Add(CreateEnumListItem("HUD VA/CRV", E_sFHACaseT.HUD_VACRV));
            ddl.Items.Add(CreateEnumListItem("Irregular HUD", E_sFHACaseT.Irregular_HUD));
            ddl.Items.Add(CreateEnumListItem("Regular DE", E_sFHACaseT.Regular_DE));
            ddl.Items.Add(CreateEnumListItem("Regular HUD", E_sFHACaseT.Regular_HUD));
        }
        public static void Bind_sFHAConstCodeT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_sFHAConstCodeT.Unspecified));
            ddl.Items.Add(CreateEnumListItem("Existing Construction", E_sFHAConstCodeT.Existing));
            ddl.Items.Add(CreateEnumListItem("New (Less than 1 Year)", E_sFHAConstCodeT.New));
            ddl.Items.Add(CreateEnumListItem("Proposed Construction", E_sFHAConstCodeT.Proposed));
            ddl.Items.Add(CreateEnumListItem("Substantial Rehabilitation", E_sFHAConstCodeT.SubstantialRehab));
            ddl.Items.Add(CreateEnumListItem("Under Construction", E_sFHAConstCodeT.UnderConstruction));
        }
        public static void Bind_sFHAProcessingT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("N/A", E_sFHAProcessingT.NA));
            ddl.Items.Add(CreateEnumListItem("REO w/ Appraisal", E_sFHAProcessingT.REOWithAppraisal));
            ddl.Items.Add(CreateEnumListItem("REO w/o Appraisal", E_sFHAProcessingT.REOWithoutAppraisal));
            ddl.Items.Add(CreateEnumListItem("Coinsurance Conversion", E_sFHAProcessingT.CoinsuranceConversion));
            ddl.Items.Add(CreateEnumListItem("Coinsurance Endorsements", E_sFHAProcessingT.CoinsuranceEndorsements));
            ddl.Items.Add(CreateEnumListItem("Military Sales", E_sFHAProcessingT.MilitarySales));
        }
        public static void Bind_sFHAFinT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("N/A", E_sFHAFinT.NA));
            ddl.Items.Add(CreateEnumListItem("Private", E_sFHAFinT.Private));
            ddl.Items.Add(CreateEnumListItem("GNMA", E_sFHAFinT.GNMA));
            ddl.Items.Add(CreateEnumListItem("FNMA", E_sFHAFinT.FNMA));
        }
        public static void Bind_sFHAADPHousingProgramT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_sFHAADPHousingProgramT.Unspecified));
            ddl.Items.Add(CreateEnumListItem("FHA Standard Mortgage Program (203b)", E_sFHAADPHousingProgramT.FHAStandard203b));
            ddl.Items.Add(CreateEnumListItem("Condominium (203b)", E_sFHAADPHousingProgramT.Condominium203b));
            ddl.Items.Add(CreateEnumListItem("Improvements (203k)", E_sFHAADPHousingProgramT.Improvements203k));
            ddl.Items.Add(CreateEnumListItem("Urban Renewal (220)", E_sFHAADPHousingProgramT.UrbanRenewal220k));
            ddl.Items.Add(CreateEnumListItem("Other", E_sFHAADPHousingProgramT.Other));
        }
        public static void Bind_sFHAADPSpecialProgramT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("No Special Program", E_sFHAADPSpecialProgramT.NoSpecialProgram));
            ddl.Items.Add(CreateEnumListItem("Indian Lands", E_sFHAADPSpecialProgramT.IndianLands));
            ddl.Items.Add(CreateEnumListItem("Hawaiian Homelands", E_sFHAADPSpecialProgramT.HawaiianHomelands));
            ddl.Items.Add(CreateEnumListItem("Military Impact Area", E_sFHAADPSpecialProgramT.MilitaryImpactArea));
            ddl.Items.Add(CreateEnumListItem("223(e) Location Waiver", E_sFHAADPSpecialProgramT.LocationWaiver223e));
        }
        public static void Bind_sFHAADPPrincipalWriteDownT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("No", E_sFHAADPPrincipalWriteDownT.No));
            ddl.Items.Add(CreateEnumListItem(">= 10% of first existing lien", E_sFHAADPPrincipalWriteDownT.GreaterThanOrEqualToTenPercent));
            ddl.Items.Add(CreateEnumListItem("< 10% of first existing lien", E_sFHAADPPrincipalWriteDownT.LessThanTenPercent));
        }
        public static void Bind_sFHAPUDCondoT(DropDownList ddl, bool includeUnspecified)
        {
            ddl.Items.Add(CreateEnumListItem("NA", E_sFHAPUDCondoT.NA));

            if (includeUnspecified)
            {
                ddl.Items.Add(CreateEnumListItem("Unspecified", E_sFHAPUDCondoT.Unspecified));
            }

            ddl.Items.Add(CreateEnumListItem("PUD", E_sFHAPUDCondoT.PUD));
            ddl.Items.Add(CreateEnumListItem("Condominium", E_sFHAPUDCondoT.Condo));
            ddl.Items.Add(CreateEnumListItem("Subdivision", E_sFHAPUDCondoT.Subdivision));
        }
        public static void Bind_sFHAPUDSiteCondoT(DropDownList ddl, bool includeSpotLot)
        {
            ddl.Items.Add(CreateEnumListItem("NA", E_sFHAPUDSiteCondoT.NA));
            ddl.Items.Add(CreateEnumListItem("Site Condo", E_sFHAPUDSiteCondoT.SiteCondo));

            if (includeSpotLot)
            {
                ddl.Items.Add(CreateEnumListItem("Spot Lot", E_sFHAPUDSiteCondoT.SpotLot));
            }
        }
        public static void Bind_sFHAComplianceInspectionAssignmentT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("N/A", E_sFHAComplianceInspectionAssignmentT.NA));
            ddl.Items.Add(CreateEnumListItem("Roster", E_sFHAComplianceInspectionAssignmentT.Roster));
            ddl.Items.Add(CreateEnumListItem("Mortgagee", E_sFHAComplianceInspectionAssignmentT.Mortgagee));
            ddl.Items.Add(CreateEnumListItem("Lender Select", E_sFHAComplianceInspectionAssignmentT.LenderSelect));
        }

        public static void Bind_sFHA203kType(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("N/A", E_sFHA203kType.NA));
            ddl.Items.Add(CreateEnumListItem("Limited", E_sFHA203kType.Limited));
            ddl.Items.Add(CreateEnumListItem("Standard", E_sFHA203kType.Standard));
        }

        public static void Bind_sFHAAgencyT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("<-- Select an Agency -->", E_sFHAAgencyT.Unspecified));
            ddl.Items.Add(CreateEnumListItem("HUD - FHA Single Family", E_sFHAAgencyT.HUDFHASingleFamily));
            ddl.Items.Add(CreateEnumListItem("HUD - FHA Title I", E_sFHAAgencyT.HUDFHATitleI));
            ddl.Items.Add(CreateEnumListItem("HUD - Native American Programs", E_sFHAAgencyT.HUDNativeAmericanPrograms));
            ddl.Items.Add(CreateEnumListItem("USDA - Farm Services", E_sFHAAgencyT.USDAFarmServices));
            ddl.Items.Add(CreateEnumListItem("USDA - Rural Development", E_sFHAAgencyT.USDARuralDevelopment));
            ddl.Items.Add(CreateEnumListItem("USDA - Rural Housing", E_sFHAAgencyT.USDARuralHousing));
            ddl.Items.Add(CreateEnumListItem("Veterans' Affairs", E_sFHAAgencyT.VeteransAffairs));
        }
        public static void Bind_sVaBuildingStatusT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_sVaBuildingStatusT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Proposed", E_sVaBuildingStatusT.Proposed));
            ddl.Items.Add(CreateEnumListItem("Under Construction", E_sVaBuildingStatusT.UnderConstruction));
            ddl.Items.Add(CreateEnumListItem("Existing", E_sVaBuildingStatusT.Existing));
            ddl.Items.Add(CreateEnumListItem("Alterations/Improvement", E_sVaBuildingStatusT.Repair));

        }
        public static void Bind_sVaBuildingT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_sVaBuildingT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Detached", E_sVaBuildingT.Detached));
            ddl.Items.Add(CreateEnumListItem("Semi-Detached", E_sVaBuildingT.SemiDetached));
            ddl.Items.Add(CreateEnumListItem("ROW", E_sVaBuildingT.Row));
            ddl.Items.Add(CreateEnumListItem("APT Unit", E_sVaBuildingT.AptUnit));

        }
        public static void Bind_sVaSpOccT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_sVaSpOccT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Occupied By Owner", E_sVaSpOccT.OccByOwner));
            ddl.Items.Add(CreateEnumListItem("Never Occupied", E_sVaSpOccT.NeverOcc));
            ddl.Items.Add(CreateEnumListItem("Vacant", E_sVaSpOccT.Vacant));
            ddl.Items.Add(CreateEnumListItem("Occupied By Tenant", E_sVaSpOccT.Rental));

        }
        public static void Bind_sProdFinMethFilterT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Fixed and ARM", E_sProdFinMethFilterT.All));
            ddl.Items.Add(CreateEnumListItem("Fixed Only", E_sProdFinMethFilterT.FixedOnly));
            ddl.Items.Add(CreateEnumListItem("ARM Only", E_sProdFinMethFilterT.ArmOnly));
        }
        public static void Bind_sPpmtPenaltyMon(DropDownList ddl)
        {
            // 4/27/2005 dd - OPM #1707 - We are not supporting 4yrs prepay
            ddl.Items.Add(new ListItem("No PP", "0"));
            ddl.Items.Add(new ListItem("1 yr", "12"));
            ddl.Items.Add(new ListItem("2 yrs", "24"));
            ddl.Items.Add(new ListItem("3 yrs", "36"));
            ddl.Items.Add(new ListItem("5 yrs", "60"));
        }
        public static void Bind_sProdAvailReserveMonths(DropDownList ddl)
        {
            // 06/07/07 mf - OPM 15866 - We now allow a "Leave Blank" setting
            // 07/25/07 mf - OPM 17067, 15959, 17089.  We are adding
            // 9, 14, 18, 24, 36, and 48 to the reserve months options.
            ddl.Items.Add(new ListItem("Leave Blank", "-1"));
            ddl.Items.Add(new ListItem("0 month", "0"));
            ddl.Items.Add(new ListItem("1 month", "1"));
            ddl.Items.Add(new ListItem("2 months", "2"));
            ddl.Items.Add(new ListItem("3 months", "3"));
            ddl.Items.Add(new ListItem("4 months", "4"));
            ddl.Items.Add(new ListItem("5 months", "5"));
            ddl.Items.Add(new ListItem("6 months", "6"));
            ddl.Items.Add(new ListItem("8 months", "8"));
            ddl.Items.Add(new ListItem("9 months", "9"));
            ddl.Items.Add(new ListItem("12 months", "12"));
            ddl.Items.Add(new ListItem("14 months", "14"));
            ddl.Items.Add(new ListItem("18 months", "18"));
            ddl.Items.Add(new ListItem("24 months", "24"));
            ddl.Items.Add(new ListItem("36 months", "36"));
            ddl.Items.Add(new ListItem("48+ months", "48"));
        }
        public static void Bind_sPriorSalesPropertySeller(DropDownList ddl)
        {
            ddl.Items.Add(new ListItem("", ""));
            ddl.Items.Add(CreateEnumListItem("Private", E_sPriorSalesPropertySellerT.Private));
            ddl.Items.Add(CreateEnumListItem("Bank Reo", E_sPriorSalesPropertySellerT.BankREO));
            ddl.Items.Add(CreateEnumListItem("FNMA", E_sPriorSalesPropertySellerT.FNMA));
            ddl.Items.Add(CreateEnumListItem("FHLMC", E_sPriorSalesPropertySellerT.FHLMC));
            ddl.Items.Add(CreateEnumListItem("GNMA", E_sPriorSalesPropertySellerT.GNMA));
            ddl.Items.Add(CreateEnumListItem("HUD", E_sPriorSalesPropertySellerT.HUD));
            ddl.Items.Add(CreateEnumListItem("VA", E_sPriorSalesPropertySellerT.VA));
            ddl.Items.Add(CreateEnumListItem("Other Gov Entity", E_sPriorSalesPropertySellerT.OtherGovEntity));
            ddl.Items.Add(CreateEnumListItem("Inheritance", E_sPriorSalesPropertySellerT.Inheritance));
            ddl.Items.Add(CreateEnumListItem("USDA", E_sPriorSalesPropertySellerT.USDA));
            ddl.Items.Add(CreateEnumListItem("Corporation", E_sPriorSalesPropertySellerT.Corporation));
        }
        public static void Bind_sConvSplitMIRT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_sConvSplitMIRT.Blank));
            ddl.Items.Add(CreateEnumListItem("0.50%", E_sConvSplitMIRT.PointFivePercent));
            ddl.Items.Add(CreateEnumListItem("0.75%", E_sConvSplitMIRT.PointSevenFivePercent));
            ddl.Items.Add(CreateEnumListItem("1.00%", E_sConvSplitMIRT.OnePercent));
            ddl.Items.Add(CreateEnumListItem("1.25%", E_sConvSplitMIRT.OnePointTwoFivePercent));
            ddl.Items.Add(CreateEnumListItem("1.50%", E_sConvSplitMIRT.OnePointFivePercent));
            ddl.Items.Add(CreateEnumListItem("1.75%", E_sConvSplitMIRT.OnePointSevenFivePercent));
            ddl.Items.Add(CreateEnumListItem("2.00%", E_sConvSplitMIRT.TwoPercent));
            ddl.Items.Add(CreateEnumListItem("2.25%", E_sConvSplitMIRT.TwoPointTwoFivePercent));
        }

        public static decimal? Get_sConvSplitMIRT(E_sConvSplitMIRT sConvSplitMIRT)
        {
            switch (sConvSplitMIRT)
            {
                case E_sConvSplitMIRT.PointFivePercent: return  .5m;
                case E_sConvSplitMIRT.PointSevenFivePercent: return  .75m;
                case E_sConvSplitMIRT.OnePercent: return  1.0m;
                case E_sConvSplitMIRT.OnePointTwoFivePercent: return  1.25m;
                case E_sConvSplitMIRT.OnePointFivePercent: return 1.5m;
                case E_sConvSplitMIRT.OnePointSevenFivePercent: return 1.75m;
                case E_sConvSplitMIRT.TwoPercent: return 2.0m;
                case E_sConvSplitMIRT.TwoPointTwoFivePercent: return 2.25m;
                case E_sConvSplitMIRT.Blank: return null;
                default:
                    throw new UnhandledEnumException(sConvSplitMIRT);
            }
        }
        /// <summary>
        /// intended to be prefaced by "because"
        /// </summary>
        public static string Get_GFEArchivedReason_rep(E_GFEArchivedReasonT gfeArchivedReasonT)
        {
            switch(gfeArchivedReasonT)
            {
                case E_GFEArchivedReasonT.ChangeOfCircumstance:
                    return "a change of circumstance was filed.";
                case E_GFEArchivedReasonT.DisclosureSentToBorrower:
                    return "a disclosure was sent to a borrower.";
                case E_GFEArchivedReasonT.InitialDislosureDateSetViaGFE:
                    return "the gfe initial disclosure date was entered on the GFE page.";
                case E_GFEArchivedReasonT.InitialDislosureDateSetViaHMDA:
                    return "the gfe initial disclosure date was entered on the HMDA page.";
                case E_GFEArchivedReasonT.InitialDislosureDateSetViaLeadConversionLO:
                    return "the gfe initial disclosure date was transfered during lead conversion in the loan editor.";
                case E_GFEArchivedReasonT.InitialDislosureDateSetViaLeadConversionPML:
                    return "the gfe initial disclosure date was transfered during lead conversion in PML.";
                case E_GFEArchivedReasonT.ManuallyArchived:
                    return "it was manually recorded.";
                case E_GFEArchivedReasonT.NotYetDetermined:
                    return "of automation.";
                case E_GFEArchivedReasonT.ClosingCostMigration:
                    return "closing costs were migrated to gfe archive.";
                case E_GFEArchivedReasonT.InitialDisclosureDateSetViaLoanEstimate:
                    return "the initial loan estimate date was changed.";
                case E_GFEArchivedReasonT.InitialLoanEstimateGenerated:
                    return "an initial loan estimate was generated.";
                default:
                    throw new UnhandledEnumException(gfeArchivedReasonT);
            }
        }

        public static void Bind_sProdConvMIOptionT(DropDownList ddl)
        {
            Bind_sProdConvMIOptionT( ddl, true, true );
        }

        public static void Bind_sProdConvMIOptionT(DropDownList ddl, bool showBpmiSingle, bool showBpmiSplit)
        {
            ddl.Items.Add(CreateEnumListItem(Get_sProdConvMIOptionTFriendlyDisplay(E_sProdConvMIOptionT.Blank), E_sProdConvMIOptionT.Blank));
            ddl.Items.Add(CreateEnumListItem(Get_sProdConvMIOptionTFriendlyDisplay(E_sProdConvMIOptionT.BorrPaidMonPrem), E_sProdConvMIOptionT.BorrPaidMonPrem));

            if ( showBpmiSingle )
            {
                ddl.Items.Add(CreateEnumListItem(Get_sProdConvMIOptionTFriendlyDisplay(E_sProdConvMIOptionT.BorrPaidSinglePrem), E_sProdConvMIOptionT.BorrPaidSinglePrem));
            }

            if ( showBpmiSplit )
            {
                ddl.Items.Add(CreateEnumListItem(Get_sProdConvMIOptionTFriendlyDisplay(E_sProdConvMIOptionT.BorrPaidSplitPrem), E_sProdConvMIOptionT.BorrPaidSplitPrem));
            }

            ddl.Items.Add(CreateEnumListItem(Get_sProdConvMIOptionTFriendlyDisplay(E_sProdConvMIOptionT.LendPaidSinglePrem), E_sProdConvMIOptionT.LendPaidSinglePrem));
            ddl.Items.Add(CreateEnumListItem(Get_sProdConvMIOptionTFriendlyDisplay(E_sProdConvMIOptionT.NoMI), E_sProdConvMIOptionT.NoMI));
        }

        /// <summary>
        /// Converts the given MI option type to a user-friendly string.
        /// </summary>
        /// <param name="value">The MI option type to convert.</param>
        /// <returns>A string representation of the given MI option type.</returns>
        public static string Get_sProdConvMIOptionTFriendlyDisplay(E_sProdConvMIOptionT value)
        {
            switch (value)
            {
                case E_sProdConvMIOptionT.BorrPaidMonPrem:
                    return "Borrower Paid - Monthly Premium";
                case E_sProdConvMIOptionT.BorrPaidSinglePrem:
                    return "Borrower Paid - Single Premium";
                case E_sProdConvMIOptionT.BorrPaidSplitPrem:
                    return "Borrower Paid - Split Premium";
                case E_sProdConvMIOptionT.LendPaidSinglePrem:
                    return "Lender Paid - Single Premium";
                case E_sProdConvMIOptionT.NoMI:
                    return "No MI";
                case E_sProdConvMIOptionT.Blank:
                    return string.Empty;
                default:
                    throw new UnhandledEnumException(value);
            }
        }

        public static void Bind_sProd3rdPartyUwResultT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("None/Not Submitted", E_sProd3rdPartyUwResultT.NA));
            ddl.Items.Add(CreateEnumListItem("DU Approve/Eligible", E_sProd3rdPartyUwResultT.DU_ApproveEligible));
            ddl.Items.Add(CreateEnumListItem("DU Approve/Ineligible", E_sProd3rdPartyUwResultT.DU_ApproveIneligible));
            ddl.Items.Add(CreateEnumListItem("DU Refer/Eligible", E_sProd3rdPartyUwResultT.DU_ReferEligible));
            ddl.Items.Add(CreateEnumListItem("DU Refer/Ineligible", E_sProd3rdPartyUwResultT.DU_ReferIneligible));
            ddl.Items.Add(CreateEnumListItem("DU Refer with Caution/Eligible", E_sProd3rdPartyUwResultT.DU_ReferWCautionEligible));
            ddl.Items.Add(CreateEnumListItem("DU Refer with Caution/Ineligible", E_sProd3rdPartyUwResultT.DU_ReferWCautionIneligible));
            ddl.Items.Add(CreateEnumListItem("DU EA I/Eligible", E_sProd3rdPartyUwResultT.DU_EAIEligible));
            ddl.Items.Add(CreateEnumListItem("DU EA II/Eligible", E_sProd3rdPartyUwResultT.DU_EAIIEligible));
            ddl.Items.Add(CreateEnumListItem("DU EA III/Eligible", E_sProd3rdPartyUwResultT.DU_EAIIIEligible));
            ddl.Items.Add(CreateEnumListItem("LPA Accept/Eligible", E_sProd3rdPartyUwResultT.LP_AcceptEligible));
            ddl.Items.Add(CreateEnumListItem("LPA Accept/Ineligible", E_sProd3rdPartyUwResultT.LP_AcceptIneligible));
            ddl.Items.Add(CreateEnumListItem("LPA A- Level 1", E_sProd3rdPartyUwResultT.Lp_AMinus_Level1));
            ddl.Items.Add(CreateEnumListItem("LPA A- Level 2", E_sProd3rdPartyUwResultT.Lp_AMinus_Level2));
            ddl.Items.Add(CreateEnumListItem("LPA A- Level 3" , E_sProd3rdPartyUwResultT.Lp_AMinus_Level3));
            ddl.Items.Add(CreateEnumListItem("LPA A- Level 4", E_sProd3rdPartyUwResultT.Lp_AMinus_Level4));
            ddl.Items.Add(CreateEnumListItem("LPA A- Level 5", E_sProd3rdPartyUwResultT.Lp_AMinus_Level5));
            ddl.Items.Add(CreateEnumListItem("LPA Caution/Eligible", E_sProd3rdPartyUwResultT.LP_CautionEligible));
            ddl.Items.Add(CreateEnumListItem("LPA Caution/Ineligible", E_sProd3rdPartyUwResultT.LP_CautionIneligible));
            ddl.Items.Add(CreateEnumListItem("LPA Refer", E_sProd3rdPartyUwResultT.Lp_Refer));
            ddl.Items.Add(CreateEnumListItem("GUS Accept/Eligible", E_sProd3rdPartyUwResultT.GUS_AcceptEligible));
            ddl.Items.Add(CreateEnumListItem("GUS Accept/Ineligible", E_sProd3rdPartyUwResultT.GUS_AcceptIneligible));
            ddl.Items.Add(CreateEnumListItem("GUS Refer/Eligible", E_sProd3rdPartyUwResultT.GUS_ReferEligible));
            ddl.Items.Add(CreateEnumListItem("GUS Refer/Ineligible", E_sProd3rdPartyUwResultT.GUS_ReferIneligible));
            ddl.Items.Add(CreateEnumListItem("GUS Refer with Caution/Eligible", E_sProd3rdPartyUwResultT.GUS_ReferWCautionEligible));
            ddl.Items.Add(CreateEnumListItem("GUS Refer with Caution/Ineligible", E_sProd3rdPartyUwResultT.GUS_ReferWCautionIneligible));
            ddl.Items.Add(CreateEnumListItem("TOTAL Approve/Eligible", E_sProd3rdPartyUwResultT.Total_ApproveEligible));
            ddl.Items.Add(CreateEnumListItem("TOTAL Approve/Ineligible", E_sProd3rdPartyUwResultT.Total_ApproveIneligible));
            ddl.Items.Add(CreateEnumListItem("TOTAL Refer/Eligible", E_sProd3rdPartyUwResultT.Total_ReferEligible));
            ddl.Items.Add(CreateEnumListItem("TOTAL Refer/Ineligible", E_sProd3rdPartyUwResultT.Total_ReferIneligible));
            ddl.Items.Add(CreateEnumListItem("Out of Scope", E_sProd3rdPartyUwResultT.OutOfScope));


        }
        public static void Bind_sProdSpStructureT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Attached", E_sProdSpStructureT.Attached));
            ddl.Items.Add(CreateEnumListItem("Detached", E_sProdSpStructureT.Detached));
        }

        public static void Bind_sFhaCondoApprovalStatusT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem(" ", E_sFhaCondoApprovalStatusT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Approved", E_sFhaCondoApprovalStatusT.Approved));
            ddl.Items.Add(CreateEnumListItem("Rejected", E_sFhaCondoApprovalStatusT.Rejected));
            ddl.Items.Add(CreateEnumListItem("Pending", E_sFhaCondoApprovalStatusT.Pending));
            ddl.Items.Add(CreateEnumListItem("Withdrawn", E_sFhaCondoApprovalStatusT.Withdrawn));
        }

        public static void Bind_RespaFeePaidToType(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Other", E_RespaFeePaidToType.Other));
            ddl.Items.Add(CreateEnumListItem("Broker", E_RespaFeePaidToType.Broker));
            ddl.Items.Add(CreateEnumListItem("Lender", E_RespaFeePaidToType.Lender));
            ddl.Items.Add(CreateEnumListItem("Affiliate of Broker", E_RespaFeePaidToType.AffiliateOfBroker));
            ddl.Items.Add(CreateEnumListItem("Affiliate of Lender", E_RespaFeePaidToType.AffiliateOfLender));
        }

        public static void Bind_sProdMIOptionT(DropDownList ddl)
        {
            // OPM 109393 SAEs want these options renamed in Dropdown. LeaveBlank->NoMI, No/LPMI/Other->LPMI.
            ddl.Items.Add(CreateEnumListItem("No MI", E_sProdMIOptionT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("LPMI", E_sProdMIOptionT.NoPmi)); // 09/28/07 mf OPM 18255. Add LPMI/Other
            ddl.Items.Add(CreateEnumListItem("Borrower Paid MI", E_sProdMIOptionT.BorrowerPdPmi));
        }

        public static void Bind_aH4HNonOccInterestT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_aH4HNonOccInterestT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Non-occupant co-borrower", E_aH4HNonOccInterestT.NonOccupantCoBorrower));
            ddl.Items.Add(CreateEnumListItem("Non-occupant co-signer", E_aH4HNonOccInterestT.NonOccupantCoSigner));
        }

        public static void Bind_sHighPricedMortgageT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Unknown", E_HighPricedMortgageT.Unknown));
            ddl.Items.Add(CreateEnumListItem("None", E_HighPricedMortgageT.None));
            ddl.Items.Add(CreateEnumListItem("Higher-priced QM/HPML", E_HighPricedMortgageT.HigherPricedQm));
            ddl.Items.Add(CreateEnumListItem("HPML", E_HighPricedMortgageT.HPML));
            ddl.Items.Add(CreateEnumListItem("Higher-priced QM/not HPML", E_HighPricedMortgageT.HigherPricedQmNotHpml));
        }

        public static void Bind_sSpAppraisalFormT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_sSpAppraisalFormT.Blank));
            ddl.Items.Add(CreateEnumListItem("FNM 1004 / FRE 70 - Uniform Residential Appraisal Report", E_sSpAppraisalFormT.UniformResidentialAppraisalReport));
            ddl.Items.Add(CreateEnumListItem("FNM 1004C / FRE 70B - Manufactured Home Appraisal Report", E_sSpAppraisalFormT.ManufacturedHomeAppraisalReport));
            ddl.Items.Add(CreateEnumListItem("FNM 1004D / FRE 442 - Appraisal Update and/or Completion Report", E_sSpAppraisalFormT.AppraisalUpdateAndOrCompletionReport));
            ddl.Items.Add(CreateEnumListItem("FNM 1025 / FRE 72 - Small Residential Income Property Appraisal Report", E_sSpAppraisalFormT.SmallResidentialIncomePropertyAppraisalReport));
            ddl.Items.Add(CreateEnumListItem("FNM 1073 / FRE 465 - Individual Condominium Unit Appraisal Report", E_sSpAppraisalFormT.IndividualCondominiumUnitAppraisalReport));
            ddl.Items.Add(CreateEnumListItem("FNM 1075 / FRE 466 - Exterior-Only Inspection Individual Condominium Unit Appraisal Report", E_sSpAppraisalFormT.ExteriorOnlyInspectionIndividualCondominiumUnitAppraisalReport));
            ddl.Items.Add(CreateEnumListItem("FNM 2000 / FRE 1032 - One-Unit Residential Appraisal Field Review Report", E_sSpAppraisalFormT.OneUnitResidentialAppraisalFieldReviewReport));
            ddl.Items.Add(CreateEnumListItem("FNM 2000A / FRE 1072 - Two- to Four-Unit Residential Appraisal", E_sSpAppraisalFormT.TwoToFourUnitResidentialAppraisal));
            ddl.Items.Add(CreateEnumListItem("FNM 2055 / FRE 2055 -  Exterior-Only Inspection Residential Appraisal Report", E_sSpAppraisalFormT.ExteriorOnlyInspectionResidentialAppraisalReport));
            ddl.Items.Add(CreateEnumListItem("FNM 2090 - Individual Cooperative Interest Appraisal Report", E_sSpAppraisalFormT.IndividualCooperativeInterestAppraisalReport));
            ddl.Items.Add(CreateEnumListItem("FNM 2095 - Exterior-Only Individual Cooperative Interest Appraisal Report", E_sSpAppraisalFormT.ExteriorOnlyInspectionIndividualCooperativeInterestAppraisalReport));
            ddl.Items.Add(CreateEnumListItem("DU Form 2075 - Desktop Underwriter Property Inspection Report", E_sSpAppraisalFormT.DesktopUnderwriterPropertyInspectionReport));
            ddl.Items.Add(CreateEnumListItem("LPA Form 2070 - Loan Product Advisor Condition and Marketability Report", E_sSpAppraisalFormT.LoanProspectorConditionAndMarketability));
            ddl.Items.Add(CreateEnumListItem("Other", E_sSpAppraisalFormT.Other));
        }

        public static void Bind_CuRiskT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem(string.Empty, E_CuRiskT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Not Heightened Risk", E_CuRiskT.NotHeightenedRisk));
            ddl.Items.Add(CreateEnumListItem("Heightened Risk", E_CuRiskT.HeightenedRisk));
        }

        public static void Bind_sSpAppraisalFormTShort(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_sSpAppraisalFormT.Blank));
            ddl.Items.Add(CreateEnumListItem("FNM 1004 / FRE 70", E_sSpAppraisalFormT.UniformResidentialAppraisalReport));
            ddl.Items.Add(CreateEnumListItem("FNM 1004C / FRE 70B", E_sSpAppraisalFormT.ManufacturedHomeAppraisalReport));
            ddl.Items.Add(CreateEnumListItem("FNM 1004D / FRE 442", E_sSpAppraisalFormT.AppraisalUpdateAndOrCompletionReport));
            ddl.Items.Add(CreateEnumListItem("FNM 1025 / FRE 72", E_sSpAppraisalFormT.SmallResidentialIncomePropertyAppraisalReport));
            ddl.Items.Add(CreateEnumListItem("FNM 1073 / FRE 465", E_sSpAppraisalFormT.IndividualCondominiumUnitAppraisalReport));
            ddl.Items.Add(CreateEnumListItem("FNM 1075 / FRE 466", E_sSpAppraisalFormT.ExteriorOnlyInspectionIndividualCondominiumUnitAppraisalReport));
            ddl.Items.Add(CreateEnumListItem("FNM 2000 / FRE 1032", E_sSpAppraisalFormT.OneUnitResidentialAppraisalFieldReviewReport));
            ddl.Items.Add(CreateEnumListItem("FNM 2000A / FRE 1072", E_sSpAppraisalFormT.TwoToFourUnitResidentialAppraisal));
            ddl.Items.Add(CreateEnumListItem("FNM 2055 / FRE 2055", E_sSpAppraisalFormT.ExteriorOnlyInspectionResidentialAppraisalReport));
            ddl.Items.Add(CreateEnumListItem("FNM 2090", E_sSpAppraisalFormT.IndividualCooperativeInterestAppraisalReport));
            ddl.Items.Add(CreateEnumListItem("FNM 2095", E_sSpAppraisalFormT.ExteriorOnlyInspectionIndividualCooperativeInterestAppraisalReport));
            ddl.Items.Add(CreateEnumListItem("DU Form 2075", E_sSpAppraisalFormT.DesktopUnderwriterPropertyInspectionReport));
            ddl.Items.Add(CreateEnumListItem("LPA Form 2070", E_sSpAppraisalFormT.LoanProspectorConditionAndMarketability));
            ddl.Items.Add(CreateEnumListItem("Other", E_sSpAppraisalFormT.Other));
        }

        public static void Bind_sGfeCreditLenderPaidItemT(DropDownList ddl)
        {
            ddl.AddEnumItem("All lender paid items", E_CreditLenderPaidItemT.AllLenderPaidItems);
            ddl.AddEnumItem("Originator compensation only", E_CreditLenderPaidItemT.OriginatorCompensationOnly);
            ddl.AddEnumItem("None", E_CreditLenderPaidItemT.None);
        }
        public static void Bind_CoCArchives(DropDownList ddl, IEnumerable<ICoCArchive> CoCArchives)
        {
            ddl.Items.Clear();

            foreach (var coca in CoCArchives)
            {
                ddl.Items.Add(new ListItem(coca.DateArchived, coca.DateArchived));
            }
        }
        public static void Bind_selectedGFEArchives(DropDownList ddl, IEnumerable<IGFEArchive> GFEArchives)
        {
            ddl.Items.Clear();

            foreach (var gfea in GFEArchives)
            {
                ddl.Items.Add(new ListItem(gfea.DateArchived, gfea.DateArchived));
            }
        }
        public static void Bind_GFEArchives(DropDownList ddlGFEArchiveDates, IEnumerable<IGFEArchive> GFEArchives)
        {
            ddlGFEArchiveDates.Items.Clear();
            if (GFEArchives.Count() == 0)
            {
                ddlGFEArchiveDates.Items.Add(new ListItem("<-- NONE -->", "0"));
            }
            else
            {
                foreach (var gfea in GFEArchives)
                {
                    ddlGFEArchiveDates.Items.Add(new ListItem(gfea.DateArchived, gfea.DateArchived));
                }
            }
        }
        // OPM 46439
        public static string ServicingPaymentTransactionType_rep(E_ServicingTransactionT type)
        {
            switch (type)
            {
                case E_ServicingTransactionT.EscrowDisbursement:
                    return "Escrow Disbursement";
                case E_ServicingTransactionT.MonthlyPayment:
                    return "Monthly Payment";
                case E_ServicingTransactionT.LateFeeCharged:
                    return "Late Fee Charged";
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        public static void Bind_ServicingPaymentTransactionType(DropDownList ddl)
        {
            foreach (int i in Enum.GetValues(typeof(E_ServicingTransactionT)))
            {
                ddl.Items.Add(new ListItem(ServicingPaymentTransactionType_rep((E_ServicingTransactionT)i), i.ToString()));
            }
        }

        public static string ServicingPaymentType_rep(E_ServicingPaymentT type)
        {
            switch (type)
            {
                case E_ServicingPaymentT.FullyAmortizing:
                    return "Principal & Interest";
                case E_ServicingPaymentT.InterestOnly:
                    return "Interest Only";
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        public static void Bind_ServicingPaymentType(DropDownList ddl)
        {
            foreach (int i in Enum.GetValues(typeof(E_ServicingPaymentT)))
            {
                ddl.Items.Add(new ListItem(ServicingPaymentType_rep((E_ServicingPaymentT)i), i.ToString()));
            }
        }

        public static void Bind_sRedisclosureMethodT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_RedisclosureMethodT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Email", E_RedisclosureMethodT.Email));
            ddl.Items.Add(CreateEnumListItem("Fax", E_RedisclosureMethodT.Fax));
            ddl.Items.Add(CreateEnumListItem("In Person", E_RedisclosureMethodT.InPerson));
            ddl.Items.Add(CreateEnumListItem("Mail", E_RedisclosureMethodT.Mail));
            ddl.Items.Add(CreateEnumListItem("Overnight", E_RedisclosureMethodT.Overnight));
        }

        public static void Bind_aPreFundVoeTypeT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_aPreFundVoeTypeT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Verbal", E_aPreFundVoeTypeT.Verbal));
            ddl.Items.Add(CreateEnumListItem("Written", E_aPreFundVoeTypeT.Written));
        }

        public static void Bind_sHasESignedDocumentT(DropDownList ddl)
        {
            foreach (int i in Enum.GetValues(typeof(E_sHasESignedDocumentsT)))
            {
                ddl.Items.Add(new ListItem(sHasESignedDocumentsT_rep((E_sHasESignedDocumentsT)i), i.ToString()));
            }
        }
        public static string sHasESignedDocumentsT_rep(E_sHasESignedDocumentsT type)
        {
            switch (type)
            {
                case E_sHasESignedDocumentsT.Undetermined:
                    return "Undetermined";
                case E_sHasESignedDocumentsT.No:
                    return "No";
                case E_sHasESignedDocumentsT.Yes:
                    return "Yes";
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        public static string PurchaseAdviceServicingStatus_rep(E_ServicingStatus type)
        {
            switch (type)
            {
                case E_ServicingStatus.Released:
                    return "Released";
                case E_ServicingStatus.Retained:
                    return "Retained";
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        public static void Bind_PurchaseAdviceServicingStatus(DropDownList ddl)
        {
            foreach (int i in Enum.GetValues(typeof(E_ServicingStatus)))
            {
                ddl.Items.Add(new ListItem(PurchaseAdviceServicingStatus_rep((E_ServicingStatus)i), i.ToString()));
            }
        }

        //OPM 32441
        public static string MipCalcType_rep(E_PercentBaseT type)
        {
            switch (type)
            {
                case E_PercentBaseT.AppraisalValue:
                    return "Appraisal Value";
                case E_PercentBaseT.AverageOutstandingBalance:
                    return "Average Outstanding Balance";
                case E_PercentBaseT.LoanAmount:
                    return "Loan Amount";
                case E_PercentBaseT.SalesPrice:
                    return "Purchase Price";
                case E_PercentBaseT.TotalLoanAmount:
                    return "Total Loan Amount";
                case E_PercentBaseT.DecliningRenewalsAnnually:
                    return "Declining Renewals - Annually";
                case E_PercentBaseT.DecliningRenewalsMonthly:
                    return "Declining Renewals - Monthly";
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        public static void Bind_MipCalcType_rep(DropDownList ddl, E_sLT loanType)
        {
            if (loanType == E_sLT.FHA)
            {
                ddl.Items.Add(CreateEnumListItem(MipCalcType_rep(E_PercentBaseT.AverageOutstandingBalance), E_PercentBaseT.AverageOutstandingBalance));
                ddl.Items.Add(CreateEnumListItem(MipCalcType_rep(E_PercentBaseT.LoanAmount), E_PercentBaseT.LoanAmount));
            }
            else
            {
                ddl.Items.Add(CreateEnumListItem(MipCalcType_rep(E_PercentBaseT.AppraisalValue), E_PercentBaseT.AppraisalValue));
                ddl.Items.Add(CreateEnumListItem(MipCalcType_rep(E_PercentBaseT.AverageOutstandingBalance), E_PercentBaseT.AverageOutstandingBalance));
                ddl.Items.Add(CreateEnumListItem(MipCalcType_rep(E_PercentBaseT.LoanAmount), E_PercentBaseT.LoanAmount));
                ddl.Items.Add(CreateEnumListItem(MipCalcType_rep(E_PercentBaseT.SalesPrice), E_PercentBaseT.SalesPrice));
                ddl.Items.Add(CreateEnumListItem(MipCalcType_rep(E_PercentBaseT.TotalLoanAmount), E_PercentBaseT.TotalLoanAmount));
                ddl.Items.Add(CreateEnumListItem(MipCalcType_rep(E_PercentBaseT.DecliningRenewalsAnnually), E_PercentBaseT.DecliningRenewalsAnnually));
                ddl.Items.Add(CreateEnumListItem(MipCalcType_rep(E_PercentBaseT.DecliningRenewalsMonthly), E_PercentBaseT.DecliningRenewalsMonthly));

            }
        }

        public static void Bind_CreditPublicRecordType(DropDownList ddl)
        {
            foreach (int i in Enum.GetValues(typeof(E_CreditPublicRecordType)))
            {
                ddl.Items.Add(new ListItem(CreditPublicRecordType_rep((E_CreditPublicRecordType)i), i.ToString()));
            }
        }

        public static void Bind_CreditPublicRecordDispositionType(DropDownList ddl)
        {
            foreach (int i in Enum.GetValues(typeof(E_CreditPublicRecordDispositionType)))
            {
                ddl.Items.Add(new ListItem(CreditPublicRecordDispositionType_rep((E_CreditPublicRecordDispositionType)i), i.ToString()));
            }
        }

        public static void Bind_MBSTradeTypeA(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("TBA", E_TradeType.TBA));
            ddl.Items.Add(CreateEnumListItem("Call Option", E_TradeType.CallOption));
            ddl.Items.Add(CreateEnumListItem("Put Option", E_TradeType.PutOption));
        }

        public static void Bind_MBSTradeTypeB(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Buy", E_TradeDirection.Buy));
            ddl.Items.Add(CreateEnumListItem("Sell", E_TradeDirection.Sell));
        }

        public static string CreditPublicRecordType_rep(E_CreditPublicRecordType Type)
        {
            switch (Type)
            {
                case E_CreditPublicRecordType.Annulment: return "Annulment";
                case E_CreditPublicRecordType.Attachment: return "Attachment";
                case E_CreditPublicRecordType.BankruptcyChapter11: return "Bankruptcy, Chapter 11";
                case E_CreditPublicRecordType.BankruptcyChapter12: return "Bankruptcy, Chapter 12";
                case E_CreditPublicRecordType.BankruptcyChapter13: return "Bankruptcy, Chapter 13";
                case E_CreditPublicRecordType.BankruptcyChapter7: return "Bankruptcy, Ch 7";
                case E_CreditPublicRecordType.BankruptcyChapter7Involuntary: return "Bankruptcy, Ch 7 Involuntary";
                case E_CreditPublicRecordType.BankruptcyChapter7Voluntary: return "Bankruptcy, Ch 7 Voluntary";
                case E_CreditPublicRecordType.BankruptcyTypeUnknown: return "Unknown Bankruptcy";
                case E_CreditPublicRecordType.ChildSupport: return "Child Support";
                case E_CreditPublicRecordType.Collection: return "Collection";
                case E_CreditPublicRecordType.CustodyAgreement: return "Custody Agreement";
                case E_CreditPublicRecordType.DivorceDecree: return "Divorce Decree";
                case E_CreditPublicRecordType.FicticiousName: return "Ficticious Name";
                case E_CreditPublicRecordType.FinancialCounseling: return "Financial Counseling";
                case E_CreditPublicRecordType.FinancingStatement: return "Financial Statement";
                case E_CreditPublicRecordType.ForcibleDetainer: return "Forcible Detainer";
                case E_CreditPublicRecordType.Foreclosure: return "Forclosure";
                case E_CreditPublicRecordType.Garnishment: return "Garnishment";
                case E_CreditPublicRecordType.HOALien: return "HOA Lien";
                case E_CreditPublicRecordType.HospitalLien: return "Hospital Lien";
                case E_CreditPublicRecordType.Judgment: return "Judgment";
                case E_CreditPublicRecordType.JudicialLien: return "Judicial Lien";
                case E_CreditPublicRecordType.LawSuit: return "Lawsuit";
                case E_CreditPublicRecordType.Lien: return "Lien";
                case E_CreditPublicRecordType.MechanicLien: return "Mechanical Lien";
                case E_CreditPublicRecordType.NonResponsibility: return "Non-Responsibility";
                case E_CreditPublicRecordType.NoticeOfDefault: return "Notice Of Default";
                case E_CreditPublicRecordType.Other: return "Other";
                case E_CreditPublicRecordType.PavingAssessmentLien: return "Paving Assessment Lien";
                case E_CreditPublicRecordType.PublicSale: return "Public Sale";
                case E_CreditPublicRecordType.RealEstateLien: return "Real Estate Lien";
                case E_CreditPublicRecordType.RealEstateRecording: return "Real Estate Recording";
                case E_CreditPublicRecordType.Repossession: return "Repossession";
                case E_CreditPublicRecordType.SupportDebt: return "Support Debt";
                case E_CreditPublicRecordType.TaxLienCity: return "Tax Lien, City";
                case E_CreditPublicRecordType.TaxLienCounty: return "Tax Lien, County";
                case E_CreditPublicRecordType.TaxLienFederal: return "Tax Lien, Federal";
                case E_CreditPublicRecordType.TaxLienOther: return "Tax Lien, Other";
                case E_CreditPublicRecordType.TaxLienState: return "Tax Lien, State";
                case E_CreditPublicRecordType.Trusteeship: return "Trusteeship";
                case E_CreditPublicRecordType.Unknown: return "Unknown";
                case E_CreditPublicRecordType.UnlawfulDetainer: return "Unlawful Detainer";
                case E_CreditPublicRecordType.WaterAndSewerLien: return "Water and Sewer Lien";
                default:
                    Tools.LogBug("Unknown E_CreditPublicRecordType value: " + Type);
                    return "";
            }
        }

        public static string CreditPublicRecordDispositionType_rep(E_CreditPublicRecordDispositionType Type)
        {
            switch (Type)
            {
                case E_CreditPublicRecordDispositionType.Adjudicated: return "Adjudicated";
                case E_CreditPublicRecordDispositionType.Appealed: return "Appealed";
                case E_CreditPublicRecordDispositionType.Canceled: return "Canceled";
                case E_CreditPublicRecordDispositionType.Completed: return "Completed";
                case E_CreditPublicRecordDispositionType.Converted: return "Converted";
                case E_CreditPublicRecordDispositionType.Discharged: return "Discharged";
                case E_CreditPublicRecordDispositionType.Dismissed: return "Dismissed";
                case E_CreditPublicRecordDispositionType.Distributed: return "Distributed";
                case E_CreditPublicRecordDispositionType.Filed: return "Filed";
                case E_CreditPublicRecordDispositionType.Granted: return "Granted";
                case E_CreditPublicRecordDispositionType.InvoluntarilyDischarged: return "Involuntarily Discharged";
                case E_CreditPublicRecordDispositionType.Nonadjudicated: return "Non-adjudicated";
                case E_CreditPublicRecordDispositionType.Other: return "Other";
                case E_CreditPublicRecordDispositionType.Paid: return "Paid";
                case E_CreditPublicRecordDispositionType.PaidNotSatisfied: return "Paid - Not Satisfied";
                case E_CreditPublicRecordDispositionType.Pending: return "Pending";
                case E_CreditPublicRecordDispositionType.RealEstateSold: return "Real Estate Sold";
                case E_CreditPublicRecordDispositionType.Released: return "Released";
                case E_CreditPublicRecordDispositionType.RelievedInBankruptcy: return "Relieved In Bankruptcy";
                case E_CreditPublicRecordDispositionType.Rescinded: return "Rescinded";
                case E_CreditPublicRecordDispositionType.Revived: return "Revived";
                case E_CreditPublicRecordDispositionType.Satisfied: return "Satisfied";
                case E_CreditPublicRecordDispositionType.Settled: return "Settled";
                case E_CreditPublicRecordDispositionType.TerminatedUnsuccessfully: return "Terminated Unsuccessfully";
                case E_CreditPublicRecordDispositionType.Unknown: return "Unknown ";
                case E_CreditPublicRecordDispositionType.Unreleased: return "Unreleased";
                case E_CreditPublicRecordDispositionType.Unsatisfied: return "Unsatisfied";
                case E_CreditPublicRecordDispositionType.Vacated: return "Vacated";
                case E_CreditPublicRecordDispositionType.VacatedOrReversed: return "Vacated Or Reversed";
                case E_CreditPublicRecordDispositionType.VoluntarilyDischarged: return "Voluntarily Discharged";
                case E_CreditPublicRecordDispositionType.Voluntary: return "Voluntary";
                case E_CreditPublicRecordDispositionType.VoluntaryDismissal: return "Voluntary Dismissal";
                case E_CreditPublicRecordDispositionType.VoluntaryWithdrawal: return "Voluntary Withdrawal";
                case E_CreditPublicRecordDispositionType.Withdrawn: return "Withdrawn";
                default:
                    Tools.LogBug("Unknown E_CreditPublicRecordDispositionType value: " + Type);
                    return "";
            }
        }


        // 09/02/08 fs opm 22179 - For Rate Merge feature.
        static public void Bind_lLpProductType(DropDownList ddl)
        {
            ddl.Items.Add(new ListItem("", ""));
            ddl.Items.Add(new ListItem("AIO", "AIO")); // OPM 169704
            ddl.Items.Add(new ListItem("CONFORMING", "CONFORMING"));
            ddl.Items.Add(new ListItem("NONCONFORMING", "NONCONFORMING"));
            ddl.Items.Add(new ListItem("FHA", "FHA"));
            ddl.Items.Add(new ListItem("VA", "VA"));
            ddl.Items.Add(new ListItem("ALT-A", "ALT-A"));
            ddl.Items.Add(new ListItem("SUBPRIME", "SUBPRIME"));
            ddl.Items.Add(new ListItem("CES", "CES"));
            ddl.Items.Add(new ListItem("HELOC", "HELOC"));
            ddl.Items.Add(new ListItem("2ND", "2ND"));
            ddl.Items.Add(new ListItem("MANUAL UW", "MANUAL UW"));
            ddl.Items.Add(new ListItem("RURAL", "RURAL"));
            ddl.Items.Add(new ListItem("HOMEPATH", "HOMEPATH"));
            ddl.Items.Add(new ListItem("FHLMC RELIEF REFI", "FHLMC RELIEF REFI"));
            ddl.Items.Add(new ListItem("FHA 203(k)", "FHA 203(k)"));
            ddl.Items.Add(new ListItem("FHA 203(ks)", "FHA 203(ks)"));
            ddl.Items.Add(new ListItem("FLEX 97", "FLEX 97"));
            ddl.Items.Add(new ListItem("ALT 97", "ALT 97"));
            ddl.Items.Add(new ListItem("HOMEPATH RENOVATION", "HOMEPATH RENOVATION"));
            ddl.Items.Add(new ListItem("FHA $100 DOWN", "FHA $100 DOWN"));
            ddl.Items.Add(new ListItem("FHA 203(k) $100 DOWN", "FHA 203(k) $100 DOWN"));
            ddl.Items.Add(new ListItem("FHA 203(ks) $100 DOWN", "FHA 203(ks) $100 DOWN"));
            ddl.Items.Add(new ListItem("MY COMMUNITY", "MY COMMUNITY"));
            ddl.Items.Add(new ListItem("HOME POSSIBLE", "HOME POSSIBLE"));
            ddl.Items.Add(new ListItem("HOMESTYLE RENOVATION", "HOMESTYLE RENOVATION"));
            ddl.Items.Add(new ListItem("CalHFA FHA", "CalHFA FHA")); // OPM 112913 // added back opm 207787
            ddl.Items.Add(new ListItem("CalHFA CHDAP", "CalHFA CHDAP"));
            ddl.Items.Add(new ListItem("FHA 1xCLOSE", "FHA 1xCLOSE"));
            ddl.Items.Add(new ListItem("CHFA CONFORMING", "CHFA CONFORMING")); // OPM 112913
            ddl.Items.Add(new ListItem("CHFA FHA", "CHFA FHA"));
            ddl.Items.Add(new ListItem("CHFA FHA 203(k)", "CHFA FHA 203(k)"));
            ddl.Items.Add(new ListItem("CHFA VA", "CHFA VA"));
            ddl.Items.Add(new ListItem("GNMA", "GNMA"));
            ddl.Items.Add(new ListItem("CONFORMING VOD", "CONFORMING VOD"));
            ddl.Items.Add(new ListItem("CONFORMING SHORT SALE", "CONFORMING SHORT SALE"));
            ddl.Items.Add(new ListItem("CONFORMING PROPERTY FLIP", "CONFORMING PROPERTY FLIP"));
            ddl.Items.Add(new ListItem("CONF NON-OCC CO-BOR", "CONF NON-OCC CO-BOR"));
            ddl.Items.Add(new ListItem("CONF 5-10 FINANCED PROP", "CONF 5-10 FINANCED PROP"));
            ddl.Items.Add(new ListItem("HOME EQUITY", "HOME EQUITY"));

            // 8/22/2013 AV - 136162 Create loan product type drop downs for NFLP
            ddl.Items.Add(new ListItem("FHA MCC", "FHA MCC"));
            ddl.Items.Add(new ListItem("VA MCC", "VA MCC"));
            ddl.Items.Add(new ListItem("CONFORMING TVLB", "CONFORMING TVLB"));
            ddl.Items.Add(new ListItem("FHA TVLB", "FHA TVLB"));
            ddl.Items.Add(new ListItem("VA TVLB", "VA TVLB"));
            ddl.Items.Add(new ListItem("FHA REPAIR ESCROW", "FHA REPAIR ESCROW"));
            ddl.Items.Add(new ListItem("FHA PURCHASE 90", "FHA PURCHASE 90"));
            ddl.Items.Add(new ListItem("CONSTRUCTION PERM", "CONSTRUCTION PERM"));
            ddl.Items.Add(new ListItem("NONCONF LAGUNA", "NONCONF LAGUNA"));
            ddl.Items.Add(new ListItem("NONCONF MANHATTAN", "NONCONF MANHATTAN"));
            ddl.Items.Add(new ListItem("CONSTRUCTION I", "CONSTRUCTION I"));
            ddl.Items.Add(new ListItem("CONSTRUCTION II", "CONSTRUCTION II"));
            ddl.Items.Add(new ListItem("RE", "RE"));
            ddl.Items.Add(new ListItem("TDHCA BOND", "TDHCA BOND"));
            ddl.Items.Add(new ListItem("TSAHC BOND", "TSAHC BOND"));
            ddl.Items.Add(new ListItem("CONFORMING VOE ONLY", "CONFORMING VOE ONLY"));
            ddl.Items.Add(new ListItem("JMAC DIRECT", "JMAC DIRECT"));
            ddl.Items.Add(new ListItem("IMPERIAL ARM", "IMPERIAL ARM"));
            ddl.Items.Add(new ListItem("PACIFIC JUMBO", "PACIFIC JUMBO"));
            ddl.Items.Add(new ListItem("WSHFC BOND", "WSHFC BOND"));
            ddl.Items.Add(new ListItem("FLFHC BOND", "FLFHC BOND"));
            ddl.Items.Add(new ListItem("SONOMA JUMBO", "SONOMA JUMBO"));
            ddl.Items.Add(new ListItem("MALIBU JUMBO", "MALIBU JUMBO"));
            ddl.Items.Add(new ListItem("SUNSET JUMBO", "SUNSET JUMBO"));

            ddl.Items.Add(new ListItem("CONFORMING FTHB", "CONFORMING FTHB"));
            ddl.Items.Add(new ListItem("MALIBU EXPANDED T1", "MALIBU EXPANDED T1"));
            ddl.Items.Add(new ListItem("MALIBU EXPANDED T2", "MALIBU EXPANDED T2"));

            ddl.Items.Add(new ListItem("VHDA", "VHDA"));
            ddl.Items.Add(new ListItem("GA DREAM", "GA DREAM"));
            ddl.Items.Add(new ListItem("LAKE MICHIGAN", "LAKE MICHIGAN"));
            ddl.Items.Add(new ListItem("ALABAMA STEP UP", "ALABAMA STEP UP"));
            ddl.Items.Add(new ListItem("CARMEL JUMBO", "CARMEL JUMBO"));

            ddl.Items.Add(new ListItem("SUPERSAVER", "SUPERSAVER"));
            ddl.Items.Add(new ListItem("IMCU SPECIALTY PRODUCTS", "IMCU SPECIALTY PRODUCTS"));
            ddl.Items.Add(new ListItem("$0 CLOSING COST OPTION", "$0 CLOSING COST OPTION"));
            ddl.Items.Add(new ListItem("97%", "97%"));
            ddl.Items.Add(new ListItem("PMI WAIVER", "PMI WAIVER"));
            ddl.Items.Add(new ListItem("VENICE NON-QM", "VENICE NON-QM"));
            ddl.Items.Add(new ListItem("ADVANTAGE", "ADVANTAGE"));
            ddl.Items.Add(new ListItem("PATH", "PATH"));
            ddl.Items.Add(new ListItem("PLATINUM/SAPPHIRE", "PLATINUM/SAPPHIRE"));
            ddl.Items.Add(new ListItem("HOME IN FIVE", "HOME IN FIVE"));
            ddl.Items.Add(new ListItem("BELMONT NON-QM", "BELMONT NON-QM")); // ejm opm 212790 Add Loan product Type "BELMON NON-QM"
            ddl.Items.Add(new ListItem("MODIFICATIONS", "MODIFICATIONS"));

            ddl.Items.Add(new ListItem("AHFA BOND", "AHFA BOND")); // OPM 214139
            ddl.Items.Add(new ListItem("CONFORMING FTHB (BOND PROGRAM)", "CONFORMING FTHB (BOND PROGRAM)"));
            ddl.Items.Add(new ListItem("PORTFOLIO", "PORTFOLIO"));
            ddl.Items.Add(new ListItem("FHA PATH", "FHA PATH"));
            ddl.Items.Add(new ListItem("CONFORMING PATH", "CONFORMING PATH"));

            // ejm opm 221013
            ddl.Items.Add(new ListItem("SONYMA ACHIEVING THE DREAM", "SONYMA ACHIEVING THE DREAM"));
            ddl.Items.Add(new ListItem("SONYMA LOW INTEREST RATE", "SONYMA LOW INTEREST RATE"));
            ddl.Items.Add(new ListItem("MASS HOUSING", "MASS HOUSING"));

            ddl.Items.Add(new ListItem("ZERO INTEREST PROGRAM", "ZERO INTEREST PROGRAM"));

            ddl.Items.Add(new ListItem("CALHFA", "CALHFA")); // OPM 223614

            ddl.Items.Add(new ListItem("CALHFA CONF", "CALHFA CONF"));
            ddl.Items.Add(new ListItem("CONFORMING FTHB/VET (BOND PRG)", "CONFORMING FTHB/VET (BOND PRG)")); // ejm - opm 231936
            ddl.Items.Add(new ListItem("HOMEREADY", "HOMEREADY")); // ejm - opm 231760

            ddl.Items.Add(new ListItem("JUMBO", "JUMBO")); // je - opm 233932
            ddl.Items.Add(new ListItem("90% CASH OUT", "90% CASH OUT")); // je - opm 233932

            // OPM 235582, 12/29/2015, ML
            ddl.Items.Add(new ListItem("BOND", "BOND"));

            ddl.Items.Add(new ListItem("CONFORMING CEMA", "CONFORMING CEMA")); // ejm - opm 237297
            ddl.Items.Add(new ListItem("CONSTRUCTION END LOAN", "CONSTRUCTION END LOAN")); // ejm - opm 237297
            ddl.Items.Add(new ListItem("CONSTRUCTION DRAW", "CONSTRUCTION DRAW")); // ejm - opm 237297

            ddl.Items.Add(new ListItem("YES MORTGAGE", "YES MORTGAGE")); // ejm - opm 237622

            // OPM 239061, 3/11/2016, ML
            ddl.Items.Add(new ListItem("WESLEND SELECT", "WESLEND SELECT"));

            // OPM 240716, 3/18/2016, ML
            ddl.Items.Add(new ListItem("CONVENTIONAL BOND", "CONVENTIONAL BOND"));
            ddl.Items.Add(new ListItem("FHA BOND", "FHA BOND"));
            ddl.Items.Add(new ListItem("VA BOND", "VA BOND"));
            ddl.Items.Add(new ListItem("USDA BOND", "USDA BOND"));

            // OPM 242286, 4/13/2016, ML
            ddl.Items.Add(new ListItem("PLATINUM", "PLATINUM"));
            ddl.Items.Add(new ListItem("SAPPHIRE", "SAPPHIRE"));

            ddl.Items.Add(new ListItem("VA 1XCLOSE", "VA 1XCLOSE"));

            // OPM 245355, 6/16/2016, ML
            ddl.Items.Add(new ListItem("COMBO", "COMBO"));

            // OPM 244201, 6/30/2016, ML
            ddl.Items.Add(new ListItem("USDA STREAMLINE-ASSIST REFI", "USDA STREAMLINE-ASSIST REFI"));

            // ejm opm 246231
            ddl.Items.Add(new ListItem("FHA EEM", "FHA EEM"));
            ddl.Items.Add(new ListItem("VA EEM", "VA EEM"));

            // jl - opm 246584
            ddl.Items.Add(new ListItem("EXPANDED PLUS", "EXPANDED PLUS"));

            // jl - opm 247122
            ddl.Items.Add(new ListItem("JUMBO FLEX", "JUMBO FLEX"));

            // jl - opm 245595
            ddl.Items.Add(new ListItem("FHA 203(k) BOND", "FHA 203(k) BOND"));

            // jl - opm 247338
            ddl.Items.Add(new ListItem("ALT-A - PLATINUM", "ALT-A - PLATINUM"));
            ddl.Items.Add(new ListItem("ALT-A - DIAMOND", "ALT-A - DIAMOND"));
            ddl.Items.Add(new ListItem("ALT-A - DIAMOND PLUS", "ALT-A - DIAMOND PLUS"));
            ddl.Items.Add(new ListItem("ALT-A - ULTRA PRIME", "ALT-A - ULTRA PRIME"));

            // OPM 247564, 8/25/2016, ML
            ddl.Items.Add(new ListItem("CONV MCC", "CONV MCC"));

            // OPM 296619, 9/28/2016, ML
            ddl.Items.Add(new ListItem("NONCONF NEWPORT", "NONCONF NEWPORT"));

            // OPM 304215, 10/13/2016, ML
            ddl.Items.Add(new ListItem("FHA203H", "FHA203H"));

            // OPM 309843, 10/20/2016, SG
            ddl.Items.Add(new ListItem("DELEGATED JUMBO", "DELEGATED JUMBO"));

            // OPM 326408, 11/1/2016, ML
            ddl.Items.Add(new ListItem("NONPRIME BRIDGE RESIDENTIAL", "NONPRIME BRIDGE RESIDENTIAL"));
            ddl.Items.Add(new ListItem("NONPRIME BRIDGE COMMERCIAL", "NONPRIME BRIDGE COMMERCIAL"));
            ddl.Items.Add(new ListItem("NONPRIME BUSINESS RESIDENTIAL", "NONPRIME BUSINESS RESIDENTIAL"));
            ddl.Items.Add(new ListItem("NONPRIME BUSINESS COMMERCIAL", "NONPRIME BUSINESS COMMERCIAL"));
            ddl.Items.Add(new ListItem("NONPRIME CONSUMER", "NONPRIME CONSUMER"));

            // OPM 353206, 11/10/2016, SG
            ddl.Items.Add(new ListItem("HOMESTYLE ENERGY", "HOMESTYLE ENERGY"));

            // OPM 448952, 1/9/2017, ML
            ddl.Items.Add(new ListItem("SERVICE RETAINED", "SERVICE RETAINED"));

            // OPM 449334, 1/18/2017, JL
            ddl.Items.Add(new ListItem("ALT-A - SILVER", "ALT-A - SILVER"));

            // OPM 449775, 1/20/2017, JL
            ddl.Items.Add(new ListItem("6 MO SEASONING VA", "6 MO SEASONING VA"));

            // OPM 449918, 1/25/2017, JL
            ddl.Items.Add(new ListItem("CONV MEDICAL PROFESSIONAL", "CONV MEDICAL PROFESSIONAL"));

            // OPM 450971, 2/17/2017, JL
            ddl.Items.Add(new ListItem("1 SERVICE RETAINED", "1 SERVICE RETAINED"));

            // OPM 451257, 2/24/2017, ML
            ddl.Items.Add(new ListItem("HOME POSS ADV PLUS 1 PCT GRANT", "HOME POSS ADV PLUS 1 PCT GRANT"));
            ddl.Items.Add(new ListItem("HOME POSS ADV PLUS 2 PCT GRANT", "HOME POSS ADV PLUS 2 PCT GRANT"));

            // OPM 453219, 3/20/17, JL
            ddl.Items.Add(new ListItem("HOMESTYLE ENERGY IMPROVEMENTS", "HOMESTYLE ENERGY IMPROVEMENTS"));

            // OPM 454078, 3/30/17, JL
            ddl.Items.Add(new ListItem("CONFORMING DELUXE", "CONFORMING DELUXE"));

            // OPM 455252, 4/14/2017, JL
            ddl.Items.Add(new ListItem("SECTION 184", "SECTION 184"));

            // OPM 457778, 6/5/17, GF
            ddl.Items.Add(new ListItem("VA RENOVATION", "VA RENOVATION"));

            ddl.Items.Add(new ListItem("CONFORMING FTHB/VALOR (BOND PRG)", "CONFORMING FTHB/VALOR (BOND PRG)"));

            ddl.Items.Add(new ListItem("CALHFA EEM", "CALHFA EEM"));

            ddl.Items.Add(new ListItem("PIGGYBACK COMBO", "PIGGYBACK COMBO"));

            ddl.Items.Add(new ListItem("EDGE MORTGAGE", "EDGE MORTGAGE"));
            ddl.Items.Add(new ListItem("USDA 1XCLOSE", "USDA 1XCLOSE"));

            ddl.Items.Add(new ListItem("HOMEFRONT", "HOMEFRONT"));

            ddl.Items.Add(new ListItem("USDA REPAIR ESCROW", "USDA REPAIR ESCROW"));

            ddl.Items.Add(new ListItem("ELITE PLUS", "ELITE PLUS"));

            // OPM 469459, 05/03/2018, JE
            ddl.Items.Add(new ListItem("TX 50(A)(6)", "TX 50(A)(6)"));
            ddl.Items.Add(new ListItem("TX 50(A)(4)", "TX 50(A)(4)"));
            ddl.Items.Add(new ListItem("DPA ADVANTAGE", "DPA ADVANTAGE"));

            // OPM 470079, 06/01/2018, JE
            ddl.Items.Add(new ListItem("HARDMONEY COMMERCIAL", "HARDMONEY COMMERCIAL"));
            ddl.Items.Add(new ListItem("HARDMONEY RESIDENTIAL", "HARDMONEY RESIDENTIAL"));
            ddl.Items.Add(new ListItem("HARDMONEY BACK EAST RESIDENTIAL", "HARDMONEY BACK EAST RESIDENTIAL"));
            ddl.Items.Add(new ListItem("ALT-A BUSINESS RESIDENTIAL", "ALT-A BUSINESS RESIDENTIAL"));

            // OPM 470453, 06/11/2018, JK
            ddl.Items.Add(new ListItem("DPA ADVANTAGE 203(K)", "DPA ADVANTAGE 203(K)"));
            ddl.Items.Add(new ListItem("DPA ADVANTAGE 203(KS)", "DPA ADVANTAGE 203(KS)"));
            ddl.Items.Add(new ListItem("DPA ADVANTAGE REPAIR ESCROW", "DPA ADVANTAGE REPAIR ESCROW"));
            ddl.Items.Add(new ListItem("TX(A)(6) FHLMC", "TX(A)(6) FHLMC"));
            ddl.Items.Add(new ListItem("TX(A)(6) FNMA", "TX(A)(6) FNMA"));
            ddl.Items.Add(new ListItem("TX(A)(4) FHLMC", "TX(A)(4) FHLMC"));
            ddl.Items.Add(new ListItem("TX(A)(4) FNMA", "TX(A)(4) FNMA"));

            ddl.Items.Add(new ListItem("HOMEONE", "HOMEONE"));
            ddl.Items.Add(new ListItem("PIVOT", "PIVOT"));
            ddl.Items.Add(new ListItem("SIMPLE", "SIMPLE"));

            ddl.Items.Add(new ListItem("HB BISCAYNE", "HB BISCAYNE"));

            ddl.Items.Add(new ListItem("LOT LOAN", "LOT LOAN"));
            ddl.Items.Add(new ListItem("RENOVATION", "RENOVATION"));

            ddl.Items.Add(new ListItem("HIGH LTV REFI", "HIGH LTV REFI"));
            ddl.Items.Add(new ListItem("ENHANCED RELIEF REFI", "ENHANCED RELIEF REFI"));

            ddl.Items.Add(new ListItem("FTHB/VALOR HFA PREFERRED", "FTHB/VALOR HFA PREFERRED"));
            ddl.Items.Add(new ListItem("BROKERED", "BROKERED"));
            ddl.Items.Add(new ListItem("HFA PREFERRED", "HFA PREFERRED"));

            ddl.Items.Add(new ListItem("FHA DPA OTC", "FHA DPA OTC"));
        }

        //03/11/09 fs opm 25872
        static public void Bind_RuleQBCType(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Primary borrower (P)", E_sRuleQBCType.PrimaryBorrower));
            ddl.Items.Add(CreateEnumListItem("All borrowers (A)", E_sRuleQBCType.AllBorrowers));
            ddl.Items.Add(CreateEnumListItem("Only coborrowers (C)", E_sRuleQBCType.OnlyCoborrowers));
            ddl.Items.Add(CreateEnumListItem("Legacy (Default)", E_sRuleQBCType.Legacy, true));
        }

        static public void Bind_aBRelationshipTitleT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_aRelationshipTitleT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("A Married Man", E_aRelationshipTitleT.AMarriedMan));
            ddl.Items.Add(CreateEnumListItem("A Married Man as His Sole and Separate Property", E_aRelationshipTitleT.AMarriedManAsHisSoleAndSeparateProperty));
            ddl.Items.Add(CreateEnumListItem("A Married Man as to an Undivided 1/2 Interest", E_aRelationshipTitleT.AMarriedManAsToAnUndividedHalfInterest));
            ddl.Items.Add(CreateEnumListItem("A Married Woman", E_aRelationshipTitleT.AMarriedWoman));
            ddl.Items.Add(CreateEnumListItem("A Married Woman as Her Sole and Separate Property", E_aRelationshipTitleT.AMarriedWomanAsHerSoleAndSeparateProperty));
            ddl.Items.Add(CreateEnumListItem("A Married Woman as to an Undivided 1/2 Interest", E_aRelationshipTitleT.AMarriedWomanAsToAnUndividedHalfInterest));
            ddl.Items.Add(CreateEnumListItem("A Married Person", E_aRelationshipTitleT.AMarriedPerson));
            ddl.Items.Add(CreateEnumListItem("A Single Man", E_aRelationshipTitleT.ASingleMan));
            ddl.Items.Add(CreateEnumListItem("A Single Man as His Sole and Separate Property", E_aRelationshipTitleT.ASingleManAsHisSoleAndSeparateProperty));
            ddl.Items.Add(CreateEnumListItem("A Single Man as to an Undivided 1/2 Interest", E_aRelationshipTitleT.ASingleManAsToAnUndividedHalfInterest));
            ddl.Items.Add(CreateEnumListItem("A Single Woman", E_aRelationshipTitleT.ASingleWoman));
            ddl.Items.Add(CreateEnumListItem("A Single Woman as Her Sole and Separate Property", E_aRelationshipTitleT.ASingleWomanAsHerSoleAndSeparateProperty));
            ddl.Items.Add(CreateEnumListItem("A Single Woman as to an Undivided 1/2 Interest", E_aRelationshipTitleT.ASingleWomanAsToAnUndividedHalfInterest));
            ddl.Items.Add(CreateEnumListItem("A Single Person", E_aRelationshipTitleT.ASinglePerson));
            ddl.Items.Add(CreateEnumListItem("An Unmarried Man", E_aRelationshipTitleT.AnUnmarriedMan));
            ddl.Items.Add(CreateEnumListItem("An Unmarried Man as His Sole and Separate Property", E_aRelationshipTitleT.AnUnmarriedManAsHisSoleAndSeparateProperty));
            ddl.Items.Add(CreateEnumListItem("An Unmarried Man as to an Undivided 1/2 Interest", E_aRelationshipTitleT.AnUnmarriedManAsToAnUndividedHalfInterest));
            ddl.Items.Add(CreateEnumListItem("An Unmarried Woman", E_aRelationshipTitleT.AnUnmarriedWoman));
            ddl.Items.Add(CreateEnumListItem("An Unmarried Woman as Her Sole and Separate Property", E_aRelationshipTitleT.AnUnmarriedWomanAsHerSoleAndSeparateProperty));
            ddl.Items.Add(CreateEnumListItem("An Unmarried Woman as to an Undivided 1/2 Interest", E_aRelationshipTitleT.AnUnmarriedWomanAsToAnUndividedHalfInterest));
            ddl.Items.Add(CreateEnumListItem("An Unmarried Person", E_aRelationshipTitleT.AnUnmarriedPerson));
            ddl.Items.Add(CreateEnumListItem("A Widow", E_aRelationshipTitleT.AWidow));
            ddl.Items.Add(CreateEnumListItem("A Widower", E_aRelationshipTitleT.AWidower));
            ddl.Items.Add(CreateEnumListItem("Chief Executive Officer", E_aRelationshipTitleT.ChiefExecutiveOfficer));
            ddl.Items.Add(CreateEnumListItem("General Partner", E_aRelationshipTitleT.GeneralPartner));
            ddl.Items.Add(CreateEnumListItem("Domestic Partners", E_aRelationshipTitleT.DomesticPartners));
            ddl.Items.Add(CreateEnumListItem("Husband and Wife", E_aRelationshipTitleT.HusbandAndWife));
            ddl.Items.Add(CreateEnumListItem("Husband and Wife as Community Property", E_aRelationshipTitleT.HusbandAndWifeAsCommunityProperty));
            ddl.Items.Add(CreateEnumListItem("Husband and Wife as Community Property with Right of Survivorship", E_aRelationshipTitleT.HusbandAndWifeAsCommunityPropertyWithRightOfSurvivorship));
            ddl.Items.Add(CreateEnumListItem("Husband and Wife as Joint Tenants", E_aRelationshipTitleT.HusbandAndWifeAsJointTenants));
            ddl.Items.Add(CreateEnumListItem("Husband and Wife as Joint Tenants with Right of Survivorship", E_aRelationshipTitleT.HusbandAndWifeAsJointTenantsWithRightOfSurvivorship));
            ddl.Items.Add(CreateEnumListItem("Husband and Wife as Tenants in Common", E_aRelationshipTitleT.HusbandAndWifeAsTenantsInCommon));
            ddl.Items.Add(CreateEnumListItem("Husband and Wife as Tenants by the Entirety", E_aRelationshipTitleT.HusbandAndWifeAsTenantsByTheEntirety));
            ddl.Items.Add(CreateEnumListItem("Husband and Wife, Tenancy by the Entirety", E_aRelationshipTitleT.HusbandAndWifeTenancyByTheEntirety));
            ddl.Items.Add(CreateEnumListItem("Husband and Wife as to an Undivided 1/2 Interest", E_aRelationshipTitleT.HusbandAndWifeAsToAnUndividedHalfInterest));
            ddl.Items.Add(CreateEnumListItem("Personal Guarantor", E_aRelationshipTitleT.PersonalGuarantor));
            ddl.Items.Add(CreateEnumListItem("President", E_aRelationshipTitleT.President));
            ddl.Items.Add(CreateEnumListItem("Secretary", E_aRelationshipTitleT.Secretary));
            ddl.Items.Add(CreateEnumListItem("Tenancy by Entirety", E_aRelationshipTitleT.TenancyByEntirety));
            ddl.Items.Add(CreateEnumListItem("Tenants by the Entirety", E_aRelationshipTitleT.TenantsByTheEntirety));
            ddl.Items.Add(CreateEnumListItem("Treasurer", E_aRelationshipTitleT.Treasurer));
            ddl.Items.Add(CreateEnumListItem("Trustee", E_aRelationshipTitleT.Trustee));
            ddl.Items.Add(CreateEnumListItem("Vice-President", E_aRelationshipTitleT.VicePresident));
            ddl.Items.Add(CreateEnumListItem("Wife and Husband", E_aRelationshipTitleT.WifeAndHusband));
            ddl.Items.Add(CreateEnumListItem("Wife and Husband as Community Property", E_aRelationshipTitleT.WifeAndHusbandAsCommunityProperty));
            ddl.Items.Add(CreateEnumListItem("Wife and Husband as Joint Tenants", E_aRelationshipTitleT.WifeAndHusbandAsJointTenants));
            ddl.Items.Add(CreateEnumListItem("Wife and Husband as Tenants in Common", E_aRelationshipTitleT.WifeAndHusbandAsTenantsInCommon));
            ddl.Items.Add(CreateEnumListItem("A Husband and Wife", E_aRelationshipTitleT.AHusbandAndWife));
            ddl.Items.Add(CreateEnumListItem("A Wife and Husband", E_aRelationshipTitleT.AWifeAndHusband));
            ddl.Items.Add(CreateEnumListItem("Her Husband", E_aRelationshipTitleT.HerHusband));
            ddl.Items.Add(CreateEnumListItem("His Wife", E_aRelationshipTitleT.HisWife));
            ddl.Items.Add(CreateEnumListItem("Not Applicable", E_aRelationshipTitleT.NotApplicable));
            ddl.Items.Add(CreateEnumListItem("Other", E_aRelationshipTitleT.Other));
        }

        static public void Bind_sManufacturedHomeConditionT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_ManufacturedHomeConditionT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("New", E_ManufacturedHomeConditionT.New));
            ddl.Items.Add(CreateEnumListItem("Used", E_ManufacturedHomeConditionT.Used));
        }

        static public void Bind_sManufacturedHomeCertificateofTitleT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_ManufacturedHomeCertificateofTitleT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Home shall be covered by Certificate of title", E_ManufacturedHomeCertificateofTitleT.HomeShallBeCoveredByCertificateOfTitle));
            ddl.Items.Add(CreateEnumListItem("Title Certificate shall be eliminated", E_ManufacturedHomeCertificateofTitleT.TitleCertificateShallBeEliminated));
            ddl.Items.Add(CreateEnumListItem("Title Certificate has been eliminated", E_ManufacturedHomeCertificateofTitleT.TitleCertificateHasBeenEliminated));
            ddl.Items.Add(CreateEnumListItem("Manufacturer�s Certificate shall be eliminated", E_ManufacturedHomeCertificateofTitleT.ManufacturersCertificateShallBeEliminated));
            ddl.Items.Add(CreateEnumListItem("Manufacturer�s Certificate has been eliminated", E_ManufacturedHomeCertificateofTitleT.ManufacturersCertificateHasBeenEliminated));
            ddl.Items.Add(CreateEnumListItem("Home is not covered/Cert is attached", E_ManufacturedHomeCertificateofTitleT.HomeIsNotCoveredOrCertIsAttached));
            ddl.Items.Add(CreateEnumListItem("Home is not covered/Not able to produce Cert", E_ManufacturedHomeCertificateofTitleT.HomeIsNotCoveredOrNotAbleToProduceCert));
        }

        static public void Bind_PmiProviderRanking(DropDownList ddl)
        {
            ddl.Items.Add(new ListItem("Exclude", "-1"));
            ddl.Items.Add(new ListItem("1", "1"));
            ddl.Items.Add(new ListItem("2", "2"));
            ddl.Items.Add(new ListItem("3", "3"));
            ddl.Items.Add(new ListItem("4", "4"));
            ddl.Items.Add(new ListItem("5", "5"));
            ddl.Items.Add(new ListItem("6", "6"));
            ddl.Items.Add(new ListItem("7", "7"));
            ddl.Items.Add(new ListItem("8", "8"));
        }

        static public void Bind_sCooperativeFormOfOwnershipT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_CooperativeFormOfOwnershipT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Owner in Fee", E_CooperativeFormOfOwnershipT.OwnerInFee));
            ddl.Items.Add(CreateEnumListItem("Ground Tenant", E_CooperativeFormOfOwnershipT.GroundTenant));
        }
        static public void Bind_sSpInvestorCurrentLoanT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Unknown/Other", E_sSpInvestorCurrentLoanT.Unknown));
            ddl.Items.Add(CreateEnumListItem("Fannie Mae", E_sSpInvestorCurrentLoanT.FannieMae));
            ddl.Items.Add(CreateEnumListItem("Freddie Mac", E_sSpInvestorCurrentLoanT.FreddieMac));
        }

        static public void Bind_sLoanProceedsToT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_LoanProceedsToT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Borrower", E_LoanProceedsToT.Borrower));
            ddl.Items.Add(CreateEnumListItem("Closing Company", E_LoanProceedsToT.ClosingCompany));
            ddl.Items.Add(CreateEnumListItem("Title Company", E_LoanProceedsToT.TitleCompany));
        }

        static public void Bind_CenlarEnvironmentT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Beta", CenlarEnvironmentT.Beta));
            ddl.Items.Add(CreateEnumListItem("Production", CenlarEnvironmentT.Production));
        }

        static public void Bind_sFundingDisbursementMethodT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_sDisbursementMethodT.Blank));
            ddl.Items.Add(CreateEnumListItem("Wire", E_sDisbursementMethodT.Wire));
            ddl.Items.Add(CreateEnumListItem("Check", E_sDisbursementMethodT.Check));
            ddl.Items.Add(CreateEnumListItem("Hold", E_sDisbursementMethodT.Hold));
        }

        static public void Bind_sUsdaGuaranteeRefinancedLoanT(CheckBoxList cbl)
        {
            cbl.Items.Add(CreateEnumListItem("Guaranteed Loan", E_sUsdaGuaranteeRefinancedLoanT.GuaranteedLoan));
            cbl.Items.Add(CreateEnumListItem("Direct Loan", E_sUsdaGuaranteeRefinancedLoanT.DirectLoan));
        }
        static public void Set_sUsdaGuaranteeRefinancedLoanT(CheckBoxList cbl, E_sUsdaGuaranteeRefinancedLoanT value)
        {
            SetCBUsingEnum(cbl, value);
        }

        static public void Bind_aUsdaGuaranteeEmployeeRelationshipT(CheckBoxList cbl)
        {
            cbl.Items.Add(CreateEnumListItem("has", E_aUsdaGuaranteeEmployeeRelationshipT.Has));
            cbl.Items.Add(CreateEnumListItem("does not have", E_aUsdaGuaranteeEmployeeRelationshipT.DoesNot));
        }

        static public void Set_aUsdaGuaranteeEmployeeRelationshipT(CheckBoxList cbl, E_aUsdaGuaranteeEmployeeRelationshipT value)
        {
            SetCBUsingEnum(cbl, value);
        }

        static private void SetCBUsingEnum(CheckBoxList cbl, Enum value)
        {
            foreach (ListItem o in cbl.Items)
            {
                o.Selected = o.Value == value.ToString("D");
            }
        }

        static public string[] Options_aManner = 
        {
            "All as Joint Tenants",
            "All as Tenants in Common",
            "As Community Property",
            "As Her Sole and Separate Property",
            "As His Sole and Separate Property",
            "As Joint Tenants",
            "As Joint Tenants with Right of Survivorship",
            "As Tenants by the Entirety",
            "As Tenants in Common",
            "Each as to an Undivided One Half Interest",
            "Each as to an Undivided One Third Interest",
            "Each as to an Undivided One Fourth Interest",
            "Husband and Husband",
            "Husband and Wife",
            "Husband and Wife as Community Property with Right of Survivorship",
            "Husband and Wife as Joint Tenants",
            "Husband and Wife as Joint Tenants with Right of Survivorship",
            "Husband and Wife as Tenants in Common",
            "Married Man",
            "Married Woman",
            "Single Man",
            "Single Woman",
            "Spouse and Spouse",
            "Unmarried Man",
            "Unmarried Woman",
            "Wife and Husband",
            "Wife and Wife",
        };

        static public void BindComboBox_aManner(ComboBox cb)
        {
            cb.Items.Add("");
            cb.Items.AddRange(Options_aManner);
        }

        public static void BindComboBox_DownPaymentSource(ComboBox comboBox)
        {
            comboBox.Items.AddRange(Options_sDwnPmtSrc);
        }

        static public string[] Options_sDwnPmtSrc = 
        {
            "Checking/Savings",
            "Gift Funds",
            "Stocks & Bonds",
            "Lot Equity",
            "Bridge Loan",
            "Trust Funds",
            "Retirement Funds",
            "Life Insurance Cash Value",
            "Sale of Chattel",
            "Trade Equity",
            "Sweat Equity",
            "Cash on Hand",
            "Deposit on Sales Contract",
            "Equity from Pending Sale",
            "Equity from Subject Property",
            "Equity on Sold Property",
            "Other Type of Down Payment",
            "Rent with Option to Purchase",
            "Secured Borrowed Funds",
            "Unsecured Borrowed Funds",
            "Loan Proceeds", // 2/27/2004 dd - Request by customer kbagley 
            "FHA - Gift - Source N/A",
            "FHA - Gift - Source Relative",
            "FHA - Gift - Source Government Assistance",
            "FHA - Gift - Source Employer",
            "FHA - Gift - Source Nonprofit/Religious/Community - Non-Seller Funded",
            "Forgivable Secured Loan"
        };

        public static void BindComboBox_CreditModelName(ComboBox combobox)
        {
            combobox.Items.Add(string.Empty);
            combobox.Items.AddRange(Options_CreditModelName);
        }

        public static string[] Options_CreditModelName = new[]
        {
            "Equifax Beacon 5.0",
            "Experian Fair Isaac",
            "FICO Risk Score Classic 04",
            "FICO Risk Score Classic 98",
            "VantageScore 2.0",
            "VantageScore 3.0",
            "More than one credit scoring model",
        };

        public static void Bind_ConstructionDisclosureType(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", ConstructionDisclosureType.Blank));
            ddl.Items.Add(Tools.CreateEnumListItem("Combined", ConstructionDisclosureType.Combined));
            ddl.Items.Add(Tools.CreateEnumListItem("Separate", ConstructionDisclosureType.Separate));
        }

        public static void Bind_ConstructionIntCalcType(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", ConstructionIntCalcType.Blank));
            ddl.Items.Add(Tools.CreateEnumListItem("Full Commitment", ConstructionIntCalcType.FullCommitment));
            ddl.Items.Add(Tools.CreateEnumListItem("Half Commitment", ConstructionIntCalcType.HalfCommitment));
        }

        public static void Bind_ConstructionMethod(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", ConstructionMethod.Blank));
            ddl.Items.Add(Tools.CreateEnumListItem("Manufactured", ConstructionMethod.Manufactured));
            ddl.Items.Add(Tools.CreateEnumListItem("Mobile Home", ConstructionMethod.MobileHome));
            ddl.Items.Add(Tools.CreateEnumListItem("Modular", ConstructionMethod.Modular));
            ddl.Items.Add(Tools.CreateEnumListItem("On Frame Modular", ConstructionMethod.OnFrameModular));
            ddl.Items.Add(Tools.CreateEnumListItem("Other", ConstructionMethod.Other));
            ddl.Items.Add(Tools.CreateEnumListItem("Site Built", ConstructionMethod.SiteBuilt));
        }

        public static void Bind_ConstructionPhaseIntAccrual(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", ConstructionPhaseIntAccrual.Blank));
            ddl.Items.Add(Tools.CreateEnumListItem("Monthly (360/360)", ConstructionPhaseIntAccrual.Monthly_360_360));
            ddl.Items.Add(Tools.CreateEnumListItem("Actual Days (365/365)", ConstructionPhaseIntAccrual.ActualDays_365_365));
            ddl.Items.Add(Tools.CreateEnumListItem("Actual Days (365/360)", ConstructionPhaseIntAccrual.ActualDays_365_360));
        }

        public static void Bind_ConstructionPhaseIntPaymentFrequency(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", ConstructionPhaseIntPaymentFrequency.Blank));
            ddl.Items.Add(Tools.CreateEnumListItem("Biweekly", ConstructionPhaseIntPaymentFrequency.Biweekly));
            ddl.Items.Add(Tools.CreateEnumListItem("Monthly", ConstructionPhaseIntPaymentFrequency.Monthly));
            ddl.Items.Add(Tools.CreateEnumListItem("Other", ConstructionPhaseIntPaymentFrequency.Other));
        }

        public static void Bind_ConstructionPhaseIntPaymentTiming(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", ConstructionPhaseIntPaymentTiming.Blank));
            ddl.Items.Add(Tools.CreateEnumListItem("Interest Paid At End Of Construction", ConstructionPhaseIntPaymentTiming.InterestPaidAtEndOfConstruction));
            ddl.Items.Add(Tools.CreateEnumListItem("Interest Paid Periodically", ConstructionPhaseIntPaymentTiming.InterestPaidPeriodically));
            ddl.Items.Add(Tools.CreateEnumListItem("Other", ConstructionPhaseIntPaymentTiming.Other));
        }

        public static void Bind_ConstructionPurpose(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("Construction And Lot Purchase", ConstructionPurpose.ConstructionAndLotPurchase));
            ddl.Items.Add(Tools.CreateEnumListItem("Construction And Lot Refinance", ConstructionPurpose.ConstructionAndLotRefinance));
            ddl.Items.Add(Tools.CreateEnumListItem("Construction On Owned Lot", ConstructionPurpose.ConstructionOnOwnedLot));
        }

        public static void Bind_ConstructionToPermanentClosingType(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", ConstructionToPermanentClosingType.Blank));
            ddl.Items.Add(Tools.CreateEnumListItem("One Closing", ConstructionToPermanentClosingType.OneClosing));
            ddl.Items.Add(Tools.CreateEnumListItem("Two Closing", ConstructionToPermanentClosingType.TwoClosing));
            ddl.Items.Add(Tools.CreateEnumListItem("Other", ConstructionToPermanentClosingType.Other));
        }

        public static void Bind_LotOwnerType(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", LotOwnerType.Blank));
            var borrowerItem = Tools.CreateEnumListItem("Borrower", LotOwnerType.Borrower);
            borrowerItem.Attributes.Add("class", "borrower-item");
            ddl.Items.Add(borrowerItem);
            ddl.Items.Add(Tools.CreateEnumListItem("Builder", LotOwnerType.Builder));
            ddl.Items.Add(Tools.CreateEnumListItem("Other", LotOwnerType.Other));
        }

        public static void Bind_CreditModelT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_CreditScoreModelT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Beacon '09 Mortgage Industry Option", E_CreditScoreModelT.Beacon09MortgageIndustryOption));
            ddl.Items.Add(CreateEnumListItem("Equifax Bankruptcy Navigator Index 02871", E_CreditScoreModelT.EquifaxBankruptcyNavigatorIndex02781));
            ddl.Items.Add(CreateEnumListItem("Equifax Bankruptcy Navigator Index 02782", E_CreditScoreModelT.EquifaxBankruptcyNavigatorIndex02782));
            ddl.Items.Add(CreateEnumListItem("Equifax Bankruptcy Navigator Index 02783", E_CreditScoreModelT.EquifaxBankruptcyNavigatorIndex02783));
            ddl.Items.Add(CreateEnumListItem("Equifax Bankruptcy Navigator Index 02784", E_CreditScoreModelT.EquifaxBankruptcyNavigatorIndex02784));
            ddl.Items.Add(CreateEnumListItem("Equifax Beacon", E_CreditScoreModelT.EquifaxBeacon));
            ddl.Items.Add(CreateEnumListItem("Equifax Beacon 5.0", E_CreditScoreModelT.EquifaxBeacon5));
            ddl.Items.Add(CreateEnumListItem("Equifax Beacon 5.0 Auto", E_CreditScoreModelT.EquifaxBeacon5Auto));
            ddl.Items.Add(CreateEnumListItem("Equifax Beacon 5.0 Bankcard", E_CreditScoreModelT.EquifaxBeacon5BankCard));
            ddl.Items.Add(CreateEnumListItem("Equifax Beacon 5.0 Installment", E_CreditScoreModelT.EquifaxBeacon5Installment));
            ddl.Items.Add(CreateEnumListItem("Equifax Beacon 5.0 Personal Finance", E_CreditScoreModelT.EquifaxBeacon5PersonalFinance));
            ddl.Items.Add(CreateEnumListItem("Equifax Beacon Auto", E_CreditScoreModelT.EquifaxBeaconAuto));
            ddl.Items.Add(CreateEnumListItem("Equifax Beacon Bankcard", E_CreditScoreModelT.EquifaxBeaconBankcard));
            ddl.Items.Add(CreateEnumListItem("Equifax Beacon Installment", E_CreditScoreModelT.EquifaxBeaconInstallment));
            ddl.Items.Add(CreateEnumListItem("Equifax Beacon Personal Finance", E_CreditScoreModelT.EquifaxBeaconPersonalFinance));
            ddl.Items.Add(CreateEnumListItem("Equifax DAS", E_CreditScoreModelT.EquifaxDAS));
            ddl.Items.Add(CreateEnumListItem("Equifax Enhanced Beacon", E_CreditScoreModelT.EquifaxEnhancedBeacon));
            ddl.Items.Add(CreateEnumListItem("Equifax Enhanced DAS", E_CreditScoreModelT.EquifaxEnhancedDAS));
            ddl.Items.Add(CreateEnumListItem("Equifax Market Max", E_CreditScoreModelT.EquifaxMarketMax));
            ddl.Items.Add(CreateEnumListItem("Equifax Mortgage Score", E_CreditScoreModelT.EquifaxMortgageScore));
            ddl.Items.Add(CreateEnumListItem("Equifax Pinnacle", E_CreditScoreModelT.EquifaxPinnacle));
            ddl.Items.Add(CreateEnumListItem("Equifax Pinnacle 2.0", E_CreditScoreModelT.EquifaxPinnacle2));
            ddl.Items.Add(CreateEnumListItem("Equifax Vantage Score", E_CreditScoreModelT.EquifaxVantageScore));
            ddl.Items.Add(CreateEnumListItem("Equifax Vantage Score 3.0", E_CreditScoreModelT.EquifaxVantageScore3));
            ddl.Items.Add(CreateEnumListItem("Experian Fair Isaac", E_CreditScoreModelT.ExperianFairIsaac));
            ddl.Items.Add(CreateEnumListItem("Experian Fair Isaac Advanced", E_CreditScoreModelT.ExperianFairIsaacAdvanced));
            ddl.Items.Add(CreateEnumListItem("Experian Fair Isaac Advanced 2.0", E_CreditScoreModelT.ExperianFairIsaacAdvanced2));
            ddl.Items.Add(CreateEnumListItem("Experian Fair Isaac Auto", E_CreditScoreModelT.ExperianFairIsaacAuto));
            ddl.Items.Add(CreateEnumListItem("Experian Fair Isaac Bankcard", E_CreditScoreModelT.ExperianFairIsaacBankcard));
            ddl.Items.Add(CreateEnumListItem("Experian Fair Isaac Installment", E_CreditScoreModelT.ExperianFairIsaacInstallment));
            ddl.Items.Add(CreateEnumListItem("Experian Fair Isaac Personal Finance", E_CreditScoreModelT.ExperianFairIsaacPersonalFinance));
            ddl.Items.Add(CreateEnumListItem("Experian FICO Classic v3", E_CreditScoreModelT.ExperianFICOClassicV3));
            ddl.Items.Add(CreateEnumListItem("Experian MDS Bankruptcy II", E_CreditScoreModelT.ExperianMDSBankruptcyII));
            ddl.Items.Add(CreateEnumListItem("Experian New National Equivalency", E_CreditScoreModelT.ExperianNewNationalEquivalency));
            ddl.Items.Add(CreateEnumListItem("Experian New National Risk", E_CreditScoreModelT.ExperianNewNationalRisk));
            ddl.Items.Add(CreateEnumListItem("Experian Old National Risk", E_CreditScoreModelT.ExperianOldNationalRisk));
            ddl.Items.Add(CreateEnumListItem("Experian Scorex PLUS", E_CreditScoreModelT.ExperianScorexPLUS));
            ddl.Items.Add(CreateEnumListItem("Experian Vantage Score", E_CreditScoreModelT.ExperianVantageScore));
            ddl.Items.Add(CreateEnumListItem("Experian Vantage Score 3.0", E_CreditScoreModelT.ExperianVantageScore3));
            ddl.Items.Add(CreateEnumListItem("FICO Expansion Score", E_CreditScoreModelT.FICOExpansionScore));
            ddl.Items.Add(CreateEnumListItem("FICO Risk Score Classic '04", E_CreditScoreModelT.FICORiskScoreClassic04));
            ddl.Items.Add(CreateEnumListItem("FICO Risk Score Classic '98", E_CreditScoreModelT.FICORiskScoreClassic98));
            ddl.Items.Add(CreateEnumListItem("FICO Risk Score Classic Auto '98", E_CreditScoreModelT.FICORiskScoreClassicAuto98));
            ddl.Items.Add(CreateEnumListItem("FICO Risk Score Classic Bankcard '98", E_CreditScoreModelT.FICORiskScoreClassicBankcard98));
            ddl.Items.Add(CreateEnumListItem("FICO Risk Score Classic Installment Loan '98", E_CreditScoreModelT.FICORiskScoreClassicInstallmentLoan98));
            ddl.Items.Add(CreateEnumListItem("FICO Risk Score Classic Personal Finance '98", E_CreditScoreModelT.FICORiskScoreClassicPersonalFinance98));
            ddl.Items.Add(CreateEnumListItem("FICO Risk Score Next Gen '00", E_CreditScoreModelT.FICORiskScoreNextGen00));
            ddl.Items.Add(CreateEnumListItem("FICO Risk Score Next Gen '03", E_CreditScoreModelT.FICORiskScoreNextGen03));
            ddl.Items.Add(CreateEnumListItem("TransUnion Delphi", E_CreditScoreModelT.TransUnionDelphi));
            ddl.Items.Add(CreateEnumListItem("TransUnion Empirica", E_CreditScoreModelT.TransUnionEmpirica));
            ddl.Items.Add(CreateEnumListItem("TransUnion Empirica Auto", E_CreditScoreModelT.TransUnionEmpiricaAuto));
            ddl.Items.Add(CreateEnumListItem("TransUnion Empirica Bankcard", E_CreditScoreModelT.TransUnionEmpiricaBankcard));
            ddl.Items.Add(CreateEnumListItem("TransUnion Empirica Installment", E_CreditScoreModelT.TransUnionEmpiricaInstallment));
            ddl.Items.Add(CreateEnumListItem("TransUnion Empirica Personal Finance", E_CreditScoreModelT.TransUnionEmpiricaPersonalFinance));
            ddl.Items.Add(CreateEnumListItem("TransUnion New Delphi", E_CreditScoreModelT.TransUnionNewDelphi));
            ddl.Items.Add(CreateEnumListItem("TransUnion Precision", E_CreditScoreModelT.TransUnionPrecision));
            ddl.Items.Add(CreateEnumListItem("TransUnion Precision '03", E_CreditScoreModelT.TransUnionPrecision03));
            ddl.Items.Add(CreateEnumListItem("TransUnion VantageScore", E_CreditScoreModelT.TransUnionVantageScore));
            ddl.Items.Add(CreateEnumListItem("TransUnion VantageScore 3.0", E_CreditScoreModelT.TransUnionVantageScore30));
            ddl.Items.Add(CreateEnumListItem("VantageScore 2.0", E_CreditScoreModelT.VantageScore2));
            ddl.Items.Add(CreateEnumListItem("VantageScore 3.0", E_CreditScoreModelT.VantageScore3));
            ddl.Items.Add(CreateEnumListItem("More than one scoring model", E_CreditScoreModelT.MoreThanOneCreditScoringModel));
            ddl.Items.Add(CreateEnumListItem("Other", E_CreditScoreModelT.Other));
        }

        static public void Bind_LenderPaidOriginatorCompensationOptionT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Add originator compensation to pricing", E_LenderPaidOriginatorCompensationOptionT.AddOriginatorCompensationToPricing ) );
            ddl.Items.Add(CreateEnumListItem("Do not add originator compensation to pricing", E_LenderPaidOriginatorCompensationOptionT.DoNotAddOriginatorCompensation ) );
            ddl.Items.Add(CreateEnumListItem("Use setting in loan file", E_LenderPaidOriginatorCompensationOptionT.UseSettingInLoanFile) );
        }

        static public void Bind_sMiProrata(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_MiProrata.NoInfo));
            ddl.Items.Add(CreateEnumListItem("Partial Premium Due", E_MiProrata.PartialPremium));
            ddl.Items.Add(CreateEnumListItem("Full Month Due", E_MiProrata.FullMonth));
        }

        static public void Bind_sTaxTableT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_TaxTableTaxT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Borough", E_TaxTableTaxT.Borough));
            ddl.Items.Add(CreateEnumListItem("City", E_TaxTableTaxT.City));
            ddl.Items.Add(CreateEnumListItem("County", E_TaxTableTaxT.County));
            ddl.Items.Add(CreateEnumListItem("Fire District", E_TaxTableTaxT.FireDist));
            ddl.Items.Add(CreateEnumListItem("Local Improvement District", E_TaxTableTaxT.LocalImprovementDist));
            ddl.Items.Add(CreateEnumListItem("Miscellaneous", E_TaxTableTaxT.Miscellaneous));
            ddl.Items.Add(CreateEnumListItem("Municipal Utility District", E_TaxTableTaxT.MunicipalUtilDist));
            ddl.Items.Add(CreateEnumListItem("School", E_TaxTableTaxT.School));
            ddl.Items.Add(CreateEnumListItem("Special Assessment District", E_TaxTableTaxT.SpecialAssessmentDist));
            ddl.Items.Add(CreateEnumListItem("Town", E_TaxTableTaxT.Town));
            ddl.Items.Add(CreateEnumListItem("Township", E_TaxTableTaxT.Township));
            ddl.Items.Add(CreateEnumListItem("Utility", E_TaxTableTaxT.Utility));
            ddl.Items.Add(CreateEnumListItem("Village", E_TaxTableTaxT.Village));
            ddl.Items.Add(CreateEnumListItem("Waste Fee District", E_TaxTableTaxT.WasteFeeDist));
            ddl.Items.Add(CreateEnumListItem("Water/Irrigation", E_TaxTableTaxT.Water_Irrigation));
            ddl.Items.Add(CreateEnumListItem("Water/Sewer", E_TaxTableTaxT.Water_Sewer));
        }

        static public void Bind_sTaxTablePaidBy(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Borrower", E_TaxTablePaidBy.Borrower));
            ddl.Items.Add(CreateEnumListItem("Lender", E_TaxTablePaidBy.Lender));
        }
        static public void Bind_sServicingCurrentLateChargeT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_TaxLateChargeT.NoInfo));
            ddl.Items.Add(CreateEnumListItem("5% of PI/15 day, $5 min, no max", E_TaxLateChargeT.v5PctPI_15day_5min_nomax));
            ddl.Items.Add(CreateEnumListItem("Fixed amount/15 day", E_TaxLateChargeT.Fixed_15day));
            ddl.Items.Add(CreateEnumListItem("4% of PI/15 day, $5 min, no max", E_TaxLateChargeT.v4PctPI_15day_5min_nomax));
            ddl.Items.Add(CreateEnumListItem("3% of PI/15 day, $5 min, no max", E_TaxLateChargeT.v3PctPI_15day_5min_nomax));
            ddl.Items.Add(CreateEnumListItem("2% of PI/15 day, $5 min, no max", E_TaxLateChargeT.v2PctPI_15day_5min_nomax));
            ddl.Items.Add(CreateEnumListItem("5% of PI/10 day, $5 min, no max", E_TaxLateChargeT.v5PctPI_10day_5min_nomax));
            ddl.Items.Add(CreateEnumListItem("4% of PI/10 day, $5 min, no max", E_TaxLateChargeT.v4PctPI_10day_5min_nomax));
            ddl.Items.Add(CreateEnumListItem("3% of PI/10 day, $5 min, no max", E_TaxLateChargeT.v3PctPI_10day_5min_nomax));
            ddl.Items.Add(CreateEnumListItem("2% of PI/10 day, $5 min, no max", E_TaxLateChargeT.v2PctPI_10day_5min_nomax));
            ddl.Items.Add(CreateEnumListItem("10% of PI/15 day, $5 min, no max", E_TaxLateChargeT.v10PctPI_15day_5min_nomax));
            ddl.Items.Add(CreateEnumListItem("3% of PITI/15 day, $0 min, no max", E_TaxLateChargeT.v3PctPITI_15day_0min_nomax));
            ddl.Items.Add(CreateEnumListItem("4% of PITI/15 day, $0 min, no max", E_TaxLateChargeT.v4PctPITI_15day_0min_nomax));
            ddl.Items.Add(CreateEnumListItem("5% of PITI/15 day, $0 min, no max", E_TaxLateChargeT.v5PctPITI_15day_0min_nomax));
            ddl.Items.Add(CreateEnumListItem("None", E_TaxLateChargeT.None));
            ddl.Items.Add(CreateEnumListItem("1% of PI/15 day, $0 min, no max", E_TaxLateChargeT.v1PctPI_15day_0min_nomax));
            ddl.Items.Add(CreateEnumListItem("2% of PI/15 day, $0 min, no max", E_TaxLateChargeT.v2PctPI_15day_0min_nomax));
            ddl.Items.Add(CreateEnumListItem("3% of PI/15 day, $0 min, no max", E_TaxLateChargeT.v3PctPI_15day_0min_nomax));
            ddl.Items.Add(CreateEnumListItem("4% of PI/10 day, $0 min, no max", E_TaxLateChargeT.v4PctPI_10day_0min_nomax));
            ddl.Items.Add(CreateEnumListItem("4% of PI/15 day, $0 min, no max", E_TaxLateChargeT.v4PctPI_15day_0min_nomax));
            ddl.Items.Add(CreateEnumListItem("5% of PI/10 day, $0 min, no max", E_TaxLateChargeT.v5PctPI_10day_0min_nomax));
            ddl.Items.Add(CreateEnumListItem("5% of PI/10 day, $0 min, $15 max", E_TaxLateChargeT.v5PctPI_10day_0min_15max));
            ddl.Items.Add(CreateEnumListItem("5% of PI/15 day, $0 min, no max", E_TaxLateChargeT.v5PctPI_15day_0min_nomax));
            ddl.Items.Add(CreateEnumListItem("5% of PI/15 day, $0 min, $10 max", E_TaxLateChargeT.v5PctPI_15day_0min_10max));
            ddl.Items.Add(CreateEnumListItem("5% of PI/15 day, $0 min, $15 max", E_TaxLateChargeT.v5PctPI_15day_0min_15max));
            ddl.Items.Add(CreateEnumListItem("5% of PI/15 day, $0 min, $25 max", E_TaxLateChargeT.v5PctPI_15day_0min_25max));
            ddl.Items.Add(CreateEnumListItem("6% of PI/15 day, $0 min, no max", E_TaxLateChargeT.v6PctPI_15day_0min_nomax));
            ddl.Items.Add(CreateEnumListItem("5% of PI/15 day, $15 min, no max", E_TaxLateChargeT.v5PctPI_15day_15min_nomax));
            ddl.Items.Add(CreateEnumListItem("5% of PI/15 day, $18 min, $100 max", E_TaxLateChargeT.v5PctPI_15day_18min_100max));
        }

        static public void Bind_sFloodCertificationPreparerT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("CBC Innovis - FZDS", E_FloodCertificationPreparerT.CBCInnovis_FZDS)); // 11/20/2015 BS - Case 228701
            ddl.Items.Add(CreateEnumListItem("Certified Credit Reporting", E_FloodCertificationPreparerT.CertifiedCredit));
            ddl.Items.Add(CreateEnumListItem("Chicago Title", E_FloodCertificationPreparerT.ChicagoTitle));
            ddl.Items.Add(CreateEnumListItem("CoreLogic", E_FloodCertificationPreparerT.FirstAm_Corelogic));
            ddl.Items.Add(CreateEnumListItem("DataQuick", E_FloodCertificationPreparerT.DataQuick));
            ddl.Items.Add(CreateEnumListItem("Fidelity", E_FloodCertificationPreparerT.Fidelity));
            ddl.Items.Add(CreateEnumListItem("Kroll Factual Data", E_FloodCertificationPreparerT.KrollFactualData));
            ddl.Items.Add(CreateEnumListItem("LandSafe", E_FloodCertificationPreparerT.LandSafe));
            ddl.Items.Add(CreateEnumListItem("Lereta", E_FloodCertificationPreparerT.Leretta));
            // 10/16/2015 BS - Case 228700. Update LPS to ServiceLink
            ddl.Items.Add(CreateEnumListItem("ServiceLink", E_FloodCertificationPreparerT.ServiceLink));
            ddl.Items.Add(CreateEnumListItem("MDA", E_FloodCertificationPreparerT.MDA));
            ddl.Items.Add(CreateEnumListItem("US Flood", E_FloodCertificationPreparerT.USFlood));
            ddl.Items.Add(CreateEnumListItem("Other", E_FloodCertificationPreparerT.Other));
        }

        static public void Bind_sInsPolicyPaymentTypeT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Non-Escrowed", E_InsPolicyPaymentTypeT.NonEscrowed));
            ddl.Items.Add(CreateEnumListItem("Escrowed", E_InsPolicyPaymentTypeT.Escrowed));
        }

        static public void Bind_sLomaLomrT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("None", E_LomaLomrT.None));
            ddl.Items.Add(CreateEnumListItem("LOMA", E_LomaLomrT.LOMA));
            ddl.Items.Add(CreateEnumListItem("LOMR", E_LomaLomrT.LOMR));
        }

        static public void Bind_CustomPmlFieldType(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("", E_CustomPmlFieldType.Blank));
            ddl.Items.Add(CreateEnumListItem("Dropdown", E_CustomPmlFieldType.Dropdown));
            ddl.Items.Add(CreateEnumListItem("Numeric Field", E_CustomPmlFieldType.NumericGeneric));
            ddl.Items.Add(CreateEnumListItem("Numeric Field: $", E_CustomPmlFieldType.NumericMoney));
            ddl.Items.Add(CreateEnumListItem("Numeric Field: %", E_CustomPmlFieldType.NumericPercent));
        }

        static public void Bind_CustomPmlFieldVisibilityType(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("All Loans", E_CustomPmlFieldVisibilityT.AllLoans));
            ddl.Items.Add(CreateEnumListItem("Purchase Loans", E_CustomPmlFieldVisibilityT.PurchaseLoans));
            ddl.Items.Add(CreateEnumListItem("Refinance Loans", E_CustomPmlFieldVisibilityT.RefinanceLoans));
            ddl.Items.Add(CreateEnumListItem("Home Equity Loans", E_CustomPmlFieldVisibilityT.HomeEquityLoans));
        }

        static public void Bind_ShippingMethod(ComboBox cb)
        {
            cb.Items.Add("USPS");
            cb.Items.Add("UPS");
            cb.Items.Add("FedEx");
            cb.Items.Add("Electronic");
        }

        public static void Bind_FromDateDDL(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Today", E_FromDateT.Today));
            ddl.Items.Add(CreateEnumListItem("Yesterday", E_FromDateT.Yesterday));
            ddl.Items.Add(CreateEnumListItem("This Monday", E_FromDateT.This_Monday));
            ddl.Items.Add(CreateEnumListItem("Last Monday", E_FromDateT.Last_Monday));
            ddl.Items.Add(CreateEnumListItem("Next Monday", E_FromDateT.Next_Monday));
            ddl.Items.Add(CreateEnumListItem("Beginning of This Month", E_FromDateT.Beginning_This_Month));
            ddl.Items.Add(CreateEnumListItem("Beginning of Last Month", E_FromDateT.Beginning_Last_Month));
            ddl.Items.Add(CreateEnumListItem("Beginning of Next Month", E_FromDateT.Beginning_Next_Month));
            ddl.Items.Add(CreateEnumListItem("Start of Current Cal. Quarter", E_FromDateT.Start_Current_Calendar_Quarter));
            ddl.Items.Add(CreateEnumListItem("Start of Last Cal. Quarter", E_FromDateT.Start_Last_Calendar_Quarter));
            ddl.Items.Add(CreateEnumListItem("Beginning of This Year", E_FromDateT.Beginning_This_Year));
            ddl.Items.Add(CreateEnumListItem("Beginning of Last Year", E_FromDateT.Beginning_Last_Year));
        }

        public static void Bind_ToDateDDL(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Today", E_ToDateT.Today));
            ddl.Items.Add(CreateEnumListItem("Yesterday", E_ToDateT.Yesterday));
            ddl.Items.Add(CreateEnumListItem("This Friday", E_ToDateT.This_Friday));
            ddl.Items.Add(CreateEnumListItem("Last Friday", E_ToDateT.Last_Friday));
            ddl.Items.Add(CreateEnumListItem("Next Friday", E_ToDateT.Next_Friday));
            ddl.Items.Add(CreateEnumListItem("End of This Month", E_ToDateT.End_This_Month));
            ddl.Items.Add(CreateEnumListItem("End of Last Month", E_ToDateT.End_Last_Month));
            ddl.Items.Add(CreateEnumListItem("End of Next Month", E_ToDateT.End_Next_Month));
            ddl.Items.Add(CreateEnumListItem("End of Current Cal. Quarter", E_ToDateT.End_Current_Calendar_Quarter));
            ddl.Items.Add(CreateEnumListItem("End of Last Cal. Quarter", E_ToDateT.End_Last_Calendar_Quarter));
            ddl.Items.Add(CreateEnumListItem("End of This Year", E_ToDateT.End_This_Year));
            ddl.Items.Add(CreateEnumListItem("End of Last Year", E_ToDateT.End_Last_Year));
        }

        public static void Bind_sTxSecurityInstrumentPar27T(RadioButtonList rbl)
        {
            rbl.Items.Add(CreateEnumListItem("Purchase Money", E_sTxSecurityInstrumentPar27T.PurchaseMoney));
            rbl.Items.Add(CreateEnumListItem("Owelty of Partition", E_sTxSecurityInstrumentPar27T.OweltyOfPartition));
            rbl.Items.Add(CreateEnumListItem("Renewal and Extension of Liens", E_sTxSecurityInstrumentPar27T.RenewalAndExtensionOfLiens));
            rbl.Items.Add(CreateEnumListItem("None", E_sTxSecurityInstrumentPar27T.None));
        }

        public static void Bind_sNyPropertyStatementT(RadioButtonList rbl)
        {
            rbl.Items.Add(CreateEnumListItem("This Security Instrument covers real property improved, or to be improved, by a one or two family dwelling only.", E_sNyPropertyStatementT.RealPropertyImprovedOrToBeImprovedIndicator));
            rbl.Items.Add(CreateEnumListItem("This Security Instrument covers real property principally improved, or to be improved, by one or more structures containing, in the aggregate, not more than six residential units with each dwelling unit having its own separate cooking facilities.", E_sNyPropertyStatementT.MultipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicator));
            rbl.Items.Add(CreateEnumListItem("This Security Instrument does not cover real property described as above.", E_sNyPropertyStatementT.RealPropertyImprovementsNotCoveredIndicator));
        }

public static void Bind_sPayoffStatementInterestCalcT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Per-diem", E_sPayoffStatementInterestCalcT.PerDiem));
            ddl.Items.Add(CreateEnumListItem("Whole month", E_sPayoffStatementInterestCalcT.WholeMonth));

        }

        public static void Bind_sPayoffStatementRecipientT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Borrower Mailing Address", E_sPayoffStatementRecipientT.BorrowerMailingAddress));
            ddl.Items.Add(CreateEnumListItem("Subject Property Address", E_sPayoffStatementRecipientT.SubjectPropertyAddress));
            ddl.Items.Add(CreateEnumListItem("Investor Main Address", E_sPayoffStatementRecipientT.InvestorMainAddress));
            ddl.Items.Add(CreateEnumListItem("Investor Note Ship To Address", E_sPayoffStatementRecipientT.InvestorNoteShipToAddress));
            ddl.Items.Add(CreateEnumListItem("Other", E_sPayoffStatementRecipientT.Other));
        }

        public static void Bind_FeeTypeRequirementT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("For all new loans", E_FeeTypeRequirementT.ForAllNewLoans));
            ddl.Items.Add(CreateEnumListItem("If set in template", E_FeeTypeRequirementT.IfSetInTemplate));
            ddl.Items.Add(CreateEnumListItem("Never (Disable and hide)", E_FeeTypeRequirementT.Never));
        }

        public static void Bind_1098PointsCalcT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Set Manually", PointCalculation1098T.SetManually));
            ddl.Items.Add(CreateEnumListItem("Discount Points", PointCalculation1098T.DiscountPoints));
        }

        public static void Bind_1098FilingYearT(DropDownList ddl, int createdYear)
        {
            ddl.Items.Add(new ListItem(string.Empty, string.Empty));
            Bind_1098FilingYear(ddl, createdYear);
        }

        public static void Bind_1098FilingYear(DropDownList ddl, int createdYear)
        {
            for (int i = createdYear - 1; i <= DateTime.Today.Year; i++)
            {
                string year = i.ToString();
                ddl.Items.Add(new ListItem(year, year));
            }
        }

        static public void Bind_CustomFeeDesc(MeridianLink.CommonControls.ComboBox cb, Guid brokerId, string lineNumber)
        {
            string key = "feeTypes_" + brokerId + "_" + lineNumber;
            List<FeeType> feeTypes = (List<FeeType>) CurrentContextCache.Get(key);
            if (feeTypes == null)
            {
                feeTypes = FeeType.RetrieveAllByBrokerIdAndLineNumber(brokerId, lineNumber);
                CurrentContextCache.Set(key, feeTypes);
            }

            foreach (FeeType feeType in feeTypes)
            {
                cb.Items.Add(feeType.Description);
            }
            if (cb.Items.Count <= 0)
            {
                cb.Attributes.Add("HasNoItems", "true");
            }
        }

        static public void Bind_CustomFeeDescDDL(DropDownList list, Guid brokerId, string lineNumber)
        {
            string key = "feeTypes_" + brokerId + "_" + lineNumber;
            List<FeeType> feeTypes = (List<FeeType>)CurrentContextCache.Get(key);
            if (feeTypes == null)
            {
                feeTypes = FeeType.RetrieveAllByBrokerIdAndLineNumber(brokerId, lineNumber);
                CurrentContextCache.Set(key, feeTypes);
            }

            list.Items.Add(new ListItem("", ((char)8201).ToString()));
            foreach (FeeType feeType in feeTypes)
            {
                list.Items.Add(new ListItem(feeType.Description, feeType.Description));
            }
        }

        /// <summary>
        /// Binds the directions of the compass to a combo box.
        /// </summary>
        /// <param name="comboBox">The combo box control.</param>
        public static void Bind_CompassDirections(ComboBox comboBox)
        {
            comboBox.Items.Add(string.Empty);
            comboBox.Items.Add("N");
            comboBox.Items.Add("S");
            comboBox.Items.Add("E");
            comboBox.Items.Add("W");
            comboBox.Items.Add("NW");
            comboBox.Items.Add("SW");
            comboBox.Items.Add("NE");
            comboBox.Items.Add("SE");
        }

        /// <summary>
        /// Binds a list of USPS street types to a combo box.
        /// </summary>
        /// <param name="comboBox"></param>
        public static void Bind_UspsStreetTypes(ComboBox comboBox)
        {
            comboBox.Items.Add(string.Empty);
            comboBox.Items.AddRange(Address.UspsStreetTypesByMLStreetTypes.Values.ToArray());
        }

        #endregion

        public static Dictionary<Guid, PropertyInfoDetails> PopulatePropertyInfoDetails(CPageData dataLoan, DropDownList propertyAddressDDL, bool includeSubjPropInDDL)
        {
            var primaryApp = dataLoan.GetAppData(0);
            Dictionary<Guid, PropertyInfoDetails> propertyInfoDetails = new Dictionary<Guid, PropertyInfoDetails>();

            // Subject property as initial selection
            Guid subjPropGuid = Guid.Empty;
            propertyInfoDetails.Add(subjPropGuid, new PropertyInfoDetails(primaryApp.aAppId, primaryApp.aBNm, primaryApp.aBEmail, dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
            if (includeSubjPropInDDL)
            {
                var subjPropAddr = dataLoan.sSpAddr_SingleLine;
                propertyAddressDDL.Items.Add(new ListItem($"Subject Property: {subjPropAddr}", subjPropGuid.ToString()));
            }

            foreach (var app in dataLoan.Apps)
            {
                foreach (IRealEstateOwned reo in app.aReCollection.GetSubcollection(true, E_ReoGroupT.All))
                {
                    var address = Tools.FormatSingleLineAddress(reo.Addr, reo.City, reo.State, reo.Zip);
                    propertyAddressDDL.Items.Add(new ListItem($"REO: {address}", reo.RecordId.ToString()));

                    var borrowerName = reo.ReOwnerT == E_ReOwnerT.CoBorrower ? app.aCNm : app.aBNm;
                    var borrowerEmail = reo.ReOwnerT == E_ReOwnerT.CoBorrower ? app.aCEmail : app.aBEmail;
                    propertyInfoDetails.Add(reo.RecordId, new PropertyInfoDetails(app.aAppId, borrowerName, borrowerEmail, reo.Addr, reo.City, reo.State, reo.Zip));
                }
            }

            return propertyInfoDetails;
        }

        /// <summary>
        /// Attempts to map a credit model string to an enum.
        /// </summary>
        /// <param name="creditModel">The name of the credit model.</param>
        /// <returns>The corresponding credit model enum.</returns>
        public static E_CreditScoreModelT MapCreditModelStringToEnum(string creditModel)
        {
            if (string.IsNullOrWhiteSpace(creditModel))
            {
                return E_CreditScoreModelT.LeaveBlank;
            }

            var normalizedModel = creditModel.Trim().Replace(" ", string.Empty);

            E_CreditScoreModelT model;
            if (Enum.TryParse(normalizedModel, ignoreCase: true, result: out model) &&
                Enum.IsDefined(typeof(E_CreditScoreModelT), model))
            {
                return model;
            }

            // Handle special case models that may have different formatting for numbers or abbreviations.
            switch (normalizedModel.ToLower())
            {
                case "beacon09mortgageindustryoption":
                case "beacon'09mortgageindustryoption":
                case "beacon2009mortgageindustryoption":
                    return E_CreditScoreModelT.Beacon09MortgageIndustryOption;
                case "equifaxbeacon5":
                case "equifaxbeacon50":
                case "equifaxbeacon5.0":
                    return E_CreditScoreModelT.EquifaxBeacon5;
                case "equifaxbeacon5auto":
                case "equifaxbeacon50auto":
                case "equifaxbeacon5.0auto":
                    return E_CreditScoreModelT.EquifaxBeacon5Auto;
                case "equifaxbeacon5bankcard":
                case "equifaxbeacon50bankcard":
                case "equifaxbeacon5.0bankcard":
                    return E_CreditScoreModelT.EquifaxBeacon5BankCard;
                case "equifaxbeacon5installment":
                case "equifaxbeacon50installment":
                case "equifaxbeacon5.0installment":
                    return E_CreditScoreModelT.EquifaxBeacon5Installment;
                case "equifaxbeacon5personalfinance":
                case "equifaxbeacon50personalfinance":
                case "equifaxbeacon5.0personalfinance":
                    return E_CreditScoreModelT.EquifaxBeacon5PersonalFinance;
                case "equifaxpinnacle2":
                case "equifaxpinnacle20":
                case "equifaxpinnacle2.0":
                    return E_CreditScoreModelT.EquifaxPinnacle2;
                case "equifaxvantagescore3":
                case "equifaxvantagescore30":
                case "equifaxvantagescore3.0":
                    return E_CreditScoreModelT.EquifaxVantageScore3;
                case "experianfairisaacadvanced2":
                case "experianfairisaacadvanced20":
                case "experianfairisaacadvanced2.0":
                    return E_CreditScoreModelT.ExperianFairIsaacAdvanced2;
                case "experianficoclassic3":
                case "experianficoclassicv3":
                case "experianficoclassicversion3":
                    return E_CreditScoreModelT.ExperianFICOClassicV3;
                case "experianmdsbankruptcy2":
                case "experianmdsbankruptcyii":
                    return E_CreditScoreModelT.ExperianMDSBankruptcyII;
                case "experianvantagescore3":
                case "experianvantagescore30":
                case "experianvantagescore3.0":
                    return E_CreditScoreModelT.ExperianVantageScore3;
                case "ficoriskscoreclassic04":
                case "ficoriskscoreclassic'04":
                case "ficoriskscoreclassic2004":
                    return E_CreditScoreModelT.FICORiskScoreClassic04;
                case "ficoriskscoreclassic98":
                case "ficoriskscoreclassic'98":
                case "ficoriskscoreclassic1998":
                    return E_CreditScoreModelT.FICORiskScoreClassic98;
                case "ficoriskscoreclassicauto98":
                case "ficoriskscoreclassicauto'98":
                case "ficoriskscoreclassicauto1998":
                    return E_CreditScoreModelT.FICORiskScoreClassicAuto98;
                case "ficoriskscoreclassicbankcard98":
                case "ficoriskscoreclassicbankcard'98":
                case "ficoriskscoreclassicbankcard1998":
                    return E_CreditScoreModelT.FICORiskScoreClassicBankcard98;
                case "ficoriskscoreclassicinstallmentloan98":
                case "ficoriskscoreclassicinstallmentloan'98":
                case "ficoriskscoreclassicinstallmentloan1998":
                    return E_CreditScoreModelT.FICORiskScoreClassicInstallmentLoan98;
                case "ficoriskscoreclassicpersonalfinance98":
                case "ficoriskscoreclassicpersonalfinance'98":
                case "ficoriskscoreclassicpersonalfinance1998":
                    return E_CreditScoreModelT.FICORiskScoreClassicPersonalFinance98;
                case "ficoriskscorenextgen00":
                case "ficoriskscorenextgen'00":
                case "ficoriskscorenextgen2000":
                    return E_CreditScoreModelT.FICORiskScoreNextGen00;
                case "ficoriskscorenextgen03":
                case "ficoriskscorenextgen'03":
                case "ficoriskscorenextgen2003":
                    return E_CreditScoreModelT.FICORiskScoreNextGen03;
                case "transunionprecision03":
                case "transunionprecision'03":
                case "transunionprecision2003":
                    return E_CreditScoreModelT.TransUnionPrecision03;
                case "transunionvantagescore3":
                case "transunionvantagescore30":
                case "transunionvantagescore3.0":
                    return E_CreditScoreModelT.TransUnionVantageScore30;
                case "vantagescore2":
                case "vantagescore20":
                case "vantagescore2.0":
                    return E_CreditScoreModelT.VantageScore2;
                case "vantagescore3":
                case "vantagescore30":
                case "vantagescore3.0":
                    return E_CreditScoreModelT.VantageScore3;
                case "morethanone":
                case "morethanonecreditscoringmodel":
                case "multiple":
                    return E_CreditScoreModelT.MoreThanOneCreditScoringModel;
                case "other":
                case "othercreditscoringmodel":
                    return E_CreditScoreModelT.Other;
                default:
                    LogInfoWithStackTrace($"E_CreditModelT fell back to 'Other'. Original value: {creditModel} Normalized value: {normalizedModel}");
                    return E_CreditScoreModelT.Other;
            }
        }

        /// <summary>
        /// Maps the applicant's credit scoring model to a HMDA model value.
        /// </summary>
        /// <param name="creditModel">The applicant's credit scoring model.</param>
        /// <param name="otherDescription">The other description for the applicant's credit scoring model.</param>
        /// <returns>The corresponding HMDA credit model value.</returns>
        public static sHmdaCreditScoreModelT MapHmdaCreditScoreModelByType(E_CreditScoreModelT creditModel, string otherDescription)
        {
            if (creditModel == E_CreditScoreModelT.LeaveBlank)
            {
                return sHmdaCreditScoreModelT.LeaveBlank;
            }

            if (creditModel == E_CreditScoreModelT.Other)
            {
                return string.IsNullOrEmpty(otherDescription)
                    ? sHmdaCreditScoreModelT.LeaveBlank
                    : MapHmdaCreditScoreModelByName(otherDescription);
            }

            switch (creditModel)
            {
                case E_CreditScoreModelT.EquifaxBeacon5:
                    return sHmdaCreditScoreModelT.EquifaxBeacon5;
                case E_CreditScoreModelT.ExperianFairIsaac:
                    return sHmdaCreditScoreModelT.ExperianFairIsaac;
                case E_CreditScoreModelT.FICORiskScoreClassic04:
                    return sHmdaCreditScoreModelT.FicoRiskScoreClassic04;
                case E_CreditScoreModelT.FICORiskScoreClassic98:
                    return sHmdaCreditScoreModelT.FicoRiskScoreClassic98;
                case E_CreditScoreModelT.MoreThanOneCreditScoringModel:
                    return sHmdaCreditScoreModelT.MoreThanOneScoringModel;
                case E_CreditScoreModelT.VantageScore2:
                    return sHmdaCreditScoreModelT.VantageScore2;
                case E_CreditScoreModelT.VantageScore3:
                    return sHmdaCreditScoreModelT.VantageScore3;
                case E_CreditScoreModelT.Beacon09MortgageIndustryOption:
                case E_CreditScoreModelT.EquifaxBankruptcyNavigatorIndex02781:
                case E_CreditScoreModelT.EquifaxBankruptcyNavigatorIndex02782:
                case E_CreditScoreModelT.EquifaxBankruptcyNavigatorIndex02783:
                case E_CreditScoreModelT.EquifaxBankruptcyNavigatorIndex02784:
                case E_CreditScoreModelT.EquifaxBeacon:
                case E_CreditScoreModelT.EquifaxBeacon5Auto:
                case E_CreditScoreModelT.EquifaxBeacon5BankCard:
                case E_CreditScoreModelT.EquifaxBeacon5Installment:
                case E_CreditScoreModelT.EquifaxBeacon5PersonalFinance:
                case E_CreditScoreModelT.EquifaxBeaconAuto:
                case E_CreditScoreModelT.EquifaxBeaconBankcard:
                case E_CreditScoreModelT.EquifaxBeaconInstallment:
                case E_CreditScoreModelT.EquifaxBeaconPersonalFinance:
                case E_CreditScoreModelT.EquifaxDAS:
                case E_CreditScoreModelT.EquifaxEnhancedBeacon:
                case E_CreditScoreModelT.EquifaxEnhancedDAS:
                case E_CreditScoreModelT.EquifaxMarketMax:
                case E_CreditScoreModelT.EquifaxMortgageScore:
                case E_CreditScoreModelT.EquifaxPinnacle:
                case E_CreditScoreModelT.EquifaxPinnacle2:
                case E_CreditScoreModelT.EquifaxVantageScore:
                case E_CreditScoreModelT.EquifaxVantageScore3:
                case E_CreditScoreModelT.ExperianFairIsaacAdvanced:
                case E_CreditScoreModelT.ExperianFairIsaacAdvanced2:
                case E_CreditScoreModelT.ExperianFairIsaacAuto:
                case E_CreditScoreModelT.ExperianFairIsaacBankcard:
                case E_CreditScoreModelT.ExperianFairIsaacInstallment:
                case E_CreditScoreModelT.ExperianFairIsaacPersonalFinance:
                case E_CreditScoreModelT.ExperianFICOClassicV3:
                case E_CreditScoreModelT.ExperianMDSBankruptcyII:
                case E_CreditScoreModelT.ExperianNewNationalEquivalency:
                case E_CreditScoreModelT.ExperianNewNationalRisk:
                case E_CreditScoreModelT.ExperianOldNationalRisk:
                case E_CreditScoreModelT.ExperianScorexPLUS:
                case E_CreditScoreModelT.ExperianVantageScore:
                case E_CreditScoreModelT.ExperianVantageScore3:
                case E_CreditScoreModelT.FICOExpansionScore:
                case E_CreditScoreModelT.FICORiskScoreClassicAuto98:
                case E_CreditScoreModelT.FICORiskScoreClassicBankcard98:
                case E_CreditScoreModelT.FICORiskScoreClassicInstallmentLoan98:
                case E_CreditScoreModelT.FICORiskScoreClassicPersonalFinance98:
                case E_CreditScoreModelT.FICORiskScoreNextGen00:
                case E_CreditScoreModelT.FICORiskScoreNextGen03:
                case E_CreditScoreModelT.TransUnionDelphi:
                case E_CreditScoreModelT.TransUnionEmpirica:
                case E_CreditScoreModelT.TransUnionEmpiricaAuto:
                case E_CreditScoreModelT.TransUnionEmpiricaBankcard:
                case E_CreditScoreModelT.TransUnionEmpiricaInstallment:
                case E_CreditScoreModelT.TransUnionEmpiricaPersonalFinance:
                case E_CreditScoreModelT.TransUnionNewDelphi:
                case E_CreditScoreModelT.TransUnionPrecision:
                case E_CreditScoreModelT.TransUnionPrecision03:
                case E_CreditScoreModelT.TransUnionVantageScore:
                case E_CreditScoreModelT.TransUnionVantageScore30:
                    return sHmdaCreditScoreModelT.Other;
                default:
                    throw new UnhandledEnumException(creditModel);
            }
        }

        /// <summary>
        /// Maps the value specified in <paramref name="creditScoreModel"/>
        /// to a value for the <see cref="sHmdaCreditScoreModelT"/> enumeration.
        /// </summary>
        /// <param name="creditScoreModel">
        /// The model to map.
        /// </param>
        /// <returns>
        /// The mapped model.
        /// </returns>
        public static sHmdaCreditScoreModelT MapHmdaCreditScoreModelByName(string creditScoreModel)
        {
            if (string.IsNullOrWhiteSpace(creditScoreModel))
            {
                return sHmdaCreditScoreModelT.NotApplicable;
            }

            var normalizedModel = creditScoreModel.Trim().Replace(" ", string.Empty);

            sHmdaCreditScoreModelT model;
            if (Enum.TryParse(normalizedModel, ignoreCase: true, result: out model) &&
                Enum.IsDefined(typeof(sHmdaCreditScoreModelT), model))
            {
                return model;
            }

            // Handle special case models that may have different formatting for numbers or abbreviations.
            switch (normalizedModel.ToLower())
            {
                case "equifaxbeacon5":
                case "equifaxbeacon50":
                case "equifaxbeacon5.0":
                    return sHmdaCreditScoreModelT.EquifaxBeacon5;
                case "vantagescore2":
                case "vantagescore20":
                case "vantagescore2.0":
                    return sHmdaCreditScoreModelT.VantageScore2;
                case "vantagescore3":
                case "vantagescore30":
                case "vantagescore3.0":
                    return sHmdaCreditScoreModelT.VantageScore3;
                case "morethanone":
                case "morethanonecreditscoringmodel":
                case "multiple":
                    return sHmdaCreditScoreModelT.MoreThanOneScoringModel;
                case "other":
                case "othercreditscoringmodel":
                    return sHmdaCreditScoreModelT.Other;
                case "na":
                    return sHmdaCreditScoreModelT.NotApplicable;
                default:
                    Tools.LogInfoWithStackTrace("Credit score model fell back to 'Other'. Original value: " + creditScoreModel + " Normalized value: " + normalizedModel);
                    return sHmdaCreditScoreModelT.Other;
            }
        }

        static public E_FeeServiceFeePropertyT MapStringToFeeServiceFeePropertyT(string property)
        {
            switch(property.TrimWhitespaceAndBOM().ToUpper())
            {
                case "DESCRIPTION":
                    return E_FeeServiceFeePropertyT.Description;
                //case "GFE BOX":
                //    return E_FeeServiceFeePropertyT.GfeBox;
                case "APR":
                    return E_FeeServiceFeePropertyT.Apr;
                case "FHA":
                    return E_FeeServiceFeePropertyT.Fha;
                case "PAID TO":
                    return E_FeeServiceFeePropertyT.PaidTo;
                case "THIRD PARTY":
                    return E_FeeServiceFeePropertyT.ThirdParty;
                case "AFFILIATE":
                    return E_FeeServiceFeePropertyT.Affiliate;
                case "CAN SHOP":
                    return E_FeeServiceFeePropertyT.CanShop;
                case "PERCENT":
                    return E_FeeServiceFeePropertyT.Percent;
                case "BASE VALUE":
                    return E_FeeServiceFeePropertyT.BaseValue;
                case "AMOUNT":
                    return E_FeeServiceFeePropertyT.Amount;
                case "PAID BY":
                    return E_FeeServiceFeePropertyT.PaidBy;
                case "PAYABLE":
                    return E_FeeServiceFeePropertyT.Payable;
                case "REMOVE":
                    return E_FeeServiceFeePropertyT.Remove;
                case "DEDUCTED FROM LOAN PROCEEDS":
                    return E_FeeServiceFeePropertyT.Dflp;

                // OPM 217028 - New Property Types for Housing Expenses
                case "TAX TYPE":
                    return E_FeeServiceFeePropertyT.TaxType;
                case "CALCULATION SOURCE":
                    return E_FeeServiceFeePropertyT.CalculationSource;
                case "PREPAID":
                    return E_FeeServiceFeePropertyT.Prepaid;
                case "ESCROW":
                    return E_FeeServiceFeePropertyT.Escrow;
                case "PREPAID MONTHS":
                    return E_FeeServiceFeePropertyT.PrepaidMonths;
                case "RESERVE CUSHION":
                    return E_FeeServiceFeePropertyT.ReserveCushion;
                case "RESERVE MONTHS LOCKED":
                    return E_FeeServiceFeePropertyT.ReserveMonthsLocked;
                case "RESERVE MONTHS":
                    return E_FeeServiceFeePropertyT.ReserveMonths;
                case "PAYMENT REPEAT INTERVAL":
                    return E_FeeServiceFeePropertyT.PaymentRepeatInterval;
                case "DISBURSEMENT SCHEDULE":
                    return E_FeeServiceFeePropertyT.DisbursementSchedule;
                case "SETTLEMENT SERVICE PROVIDER":
                    return E_FeeServiceFeePropertyT.SettlementServiceProvider;
                default:
                    return E_FeeServiceFeePropertyT.None;
            }
        }

        public static string GetFriendlyMismoFeeType(E_ClosingCostFeeMismoFeeT type)
        {
            switch (type)
            {
                case E_ClosingCostFeeMismoFeeT.Undefined:
                    return "Blank";
                case E_ClosingCostFeeMismoFeeT._203KDiscountOnRepairs:
                    return "203K Discount On Repairs";
                case E_ClosingCostFeeMismoFeeT._203KPermits:
                    return "203K Permits";
                case E_ClosingCostFeeMismoFeeT._203KArchitecturalAndEngineeringFee:
                    return "203K Architectural And Engineering Fee";
                case E_ClosingCostFeeMismoFeeT._203KInspectionFee:
                    return "203K Inspection Fee";
                case E_ClosingCostFeeMismoFeeT._203KSupplementalOriginationFee:
                    return "203K Supplemental Origination Fee";
                case E_ClosingCostFeeMismoFeeT._203KConsultantFee:
                    return "203K Consultant Fee";
                case E_ClosingCostFeeMismoFeeT._203KTitleUpdate:
                    return "203K Title Update";
                case E_ClosingCostFeeMismoFeeT.AbstractOrTitleSearchFee:
                    return "Abstract Or Title Search Fee";
                case E_ClosingCostFeeMismoFeeT.AmortizationFee:
                    return "Amortization Fee";
                case E_ClosingCostFeeMismoFeeT.ApplicationFee:
                    return "Application Fee";
                case E_ClosingCostFeeMismoFeeT.AppraisalFee:
                    return "Appraisal Fee";
                case E_ClosingCostFeeMismoFeeT.AssignmentFee:
                    return "Assignment Fee";
                case E_ClosingCostFeeMismoFeeT.AssignmentRecordingFee:
                    return "Recording Fee For Assignment";
                case E_ClosingCostFeeMismoFeeT.AssumptionFee:
                   return "Assumption Fee";
                case E_ClosingCostFeeMismoFeeT.AttorneyFee:
                   return "Attorney Fee";
                case E_ClosingCostFeeMismoFeeT.BondReviewFee:
                   return "Bond Review Fee";
                case E_ClosingCostFeeMismoFeeT.CountyDeedTaxStampFee:
                   return "Tax Stamp For County Deed";
                case E_ClosingCostFeeMismoFeeT.CountyMortgageTaxStampFee:
                   return "Tax Stamp For County Mortgage";
                case E_ClosingCostFeeMismoFeeT.CLOAccessFee:
                   return "CLO Access Fee";
                case E_ClosingCostFeeMismoFeeT.CommitmentFee:
                   return "Commitment Fee";
                case E_ClosingCostFeeMismoFeeT.CopyFaxFee:
                   return "Copy Or Fax Fee";
                case E_ClosingCostFeeMismoFeeT.CourierFee:
                   return "Courier Fee";
                case E_ClosingCostFeeMismoFeeT.CreditReportFee:
                   return "Credit Report Fee";
                case E_ClosingCostFeeMismoFeeT.DeedRecordingFee:
                   return "Recording Fee For Deed";
                case E_ClosingCostFeeMismoFeeT.DocumentPreparationFee:
                   return "Document Preparation Fee";
                case E_ClosingCostFeeMismoFeeT.DocumentaryStampFee:
                    return "Documentary Stamp Fee";
                case E_ClosingCostFeeMismoFeeT.EscrowWaiverFee:
                    return "Escrow Waiver Fee";
                case E_ClosingCostFeeMismoFeeT.FloodCertification:
                    return "Flood Certification";
                case E_ClosingCostFeeMismoFeeT.GeneralCounselFee:
                    return "General Counsel Fee";
                case E_ClosingCostFeeMismoFeeT.InspectionFee:
                    return "Home Inspection Fee";
                case E_ClosingCostFeeMismoFeeT.LoanDiscountPoints:
                    return "Loan Discount Points";
                case E_ClosingCostFeeMismoFeeT.LoanOriginationFee:
                    return "Loan Origination Fee";
                case E_ClosingCostFeeMismoFeeT.ModificationFee:
                    return "Modification Fee";
                case E_ClosingCostFeeMismoFeeT.MortgageBrokerFee:
                    return "Mortgage Broker Fee";
                case E_ClosingCostFeeMismoFeeT.MortgageRecordingFee:
                    return "Recording Fee For Mortgage";
                case E_ClosingCostFeeMismoFeeT.MunicipalLienCertificateFee:
                    return "Municipal Lien Certificate Fee";
                case E_ClosingCostFeeMismoFeeT.MunicipalLienCertificateRecordingFee:
                    return "Recording Fee For Municipal Lien Certificate";
                case E_ClosingCostFeeMismoFeeT.NewLoanAdministrationFee:
                    return "New Loan Administration Fee";
                case E_ClosingCostFeeMismoFeeT.NotaryFee:
                    return "Notary Fee";
                case E_ClosingCostFeeMismoFeeT.Other:
                    return "Other Fee";
                case E_ClosingCostFeeMismoFeeT.PestInspectionFee:
                    return "Pest Inspection Fee";
                case E_ClosingCostFeeMismoFeeT.ProcessingFee:
                    return "Processing Fee";
                case E_ClosingCostFeeMismoFeeT.RedrawFee:
                    return "Redraw Fee";
                case E_ClosingCostFeeMismoFeeT.RealEstateCommission:
                    return "Real Estate Commission Buyers Broker";
                case E_ClosingCostFeeMismoFeeT.ReinspectionFee:
                    return "Reinspection Fee";
                case E_ClosingCostFeeMismoFeeT.ReleaseRecordingFee:
                    return "Recording Fee For Release";
                case E_ClosingCostFeeMismoFeeT.RuralHousingFee:
                    return "USDA Rural Development Guarantee Fee";
                case E_ClosingCostFeeMismoFeeT.SettlementOrClosingFee:
                    return "Settlement Fee";
                case E_ClosingCostFeeMismoFeeT.StateDeedTaxStampFee:
                    return "Tax Stamp For State Deed";
                case E_ClosingCostFeeMismoFeeT.StateMortgageTaxStampFee:
                    return "Tax Stamp For State Mortgage";
                case E_ClosingCostFeeMismoFeeT.SurveyFee:
                    return "Survey Fee";
                case E_ClosingCostFeeMismoFeeT.TaxRelatedServiceFee:
                    return "Tax Related Service Fee";
                case E_ClosingCostFeeMismoFeeT.TitleExaminationFee:
                    return "Title Examination Fee";
                case E_ClosingCostFeeMismoFeeT.TitleInsuranceBinderFee:
                    return "Title Insurance Binder Fee";
                case E_ClosingCostFeeMismoFeeT.TitleInsuranceFee:
                    return "Title Insurance Fee";
                case E_ClosingCostFeeMismoFeeT.UnderwritingFee:
                    return "Underwriting Fee";
                case E_ClosingCostFeeMismoFeeT.BankruptcyMonitoringFee:
                    return "Bankruptcy Monitoring Fee";
                case E_ClosingCostFeeMismoFeeT.BondFee:
                    return "Bond Fee";
                case E_ClosingCostFeeMismoFeeT.ClosingProtectionLetterFee:
                    return "Title Closing Protection Letter Fee";
                case E_ClosingCostFeeMismoFeeT.AppraisalDeskReviewFee:
                    return "Appraisal Desk Review Fee";
                case E_ClosingCostFeeMismoFeeT.ElectronicDocumentDeliveryFee:
                    return "Electronic Document Delivery Fee";
                case E_ClosingCostFeeMismoFeeT.EscrowServiceFee:
                    return "Escrow Service Fee";
                case E_ClosingCostFeeMismoFeeT.AppraisalFieldReviewFee:
                    return "Appraisal Field Review Fee";
                case E_ClosingCostFeeMismoFeeT.CertificationFee:
                    return "Certification Fee";
                case E_ClosingCostFeeMismoFeeT.MERSRegistrationFee:
                    return "MERS Registration Fee";
                case E_ClosingCostFeeMismoFeeT.PayoffRequestFee:
                    return "Payoff Request Fee";
                case E_ClosingCostFeeMismoFeeT.SigningAgentFee:
                    return "Signing Agent Fee";
                case E_ClosingCostFeeMismoFeeT.SubordinationFee:
                    return "Subordination Fee";
                case E_ClosingCostFeeMismoFeeT.TitleEndorsementFee:
                    return "Title Endorsement Fee";
                case E_ClosingCostFeeMismoFeeT.WireTransferFee:
                    return "Wire Transfer Fee";
                case E_ClosingCostFeeMismoFeeT.LoanOriginatorCompensation:
                    return "Loan Originator Compensation";
                case E_ClosingCostFeeMismoFeeT.TitleLendersCoveragePremium:
                    return "Title Lenders Coverage Premium";
                case E_ClosingCostFeeMismoFeeT.TitleOwnersCoveragePremium:
                    return "Title Owners Coverage Premium";
                case E_ClosingCostFeeMismoFeeT.CityDeedTaxStampFee:
                    return "Tax Stamp For City Deed";
                case E_ClosingCostFeeMismoFeeT.CityMortgageTaxStampFee:
                    return "Tax Stamp For City Mortgage";
                case E_ClosingCostFeeMismoFeeT.AppraisalManagementCompanyFee:
                    return "Appraisal Management Company Fee";
                case E_ClosingCostFeeMismoFeeT.AsbestosInspectionFee:
                    return "Asbestos Inspection Fee";
                case E_ClosingCostFeeMismoFeeT.AutomatedUnderwritingFee:
                    return "Automated Underwriting Fee";
                case E_ClosingCostFeeMismoFeeT.AVMFee:
                    return "AVM Fee";
                case E_ClosingCostFeeMismoFeeT.CondominiumAssociationDues:
                    return "Condominium Association Dues";
                case E_ClosingCostFeeMismoFeeT.CondominiumAssociationSpecialAssessment:
                    return "Condominium Association Special Assessment";
                case E_ClosingCostFeeMismoFeeT.CooperativeAssociationDues:
                    return "Cooperative Association Dues";
                case E_ClosingCostFeeMismoFeeT.CooperativeAssociationSpecialAssessment:
                    return "Cooperative Association Special Assessment";
                case E_ClosingCostFeeMismoFeeT.CreditDisabilityInsurancePremium:
                    return "Credit Disability Insurance Premium";
                case E_ClosingCostFeeMismoFeeT.CreditLifeInsurancePremium:
                    return "Credit Life Insurance Premium";
                case E_ClosingCostFeeMismoFeeT.CreditPropertyInsurancePremium:
                    return "Credit Property Insurance Premium";
                case E_ClosingCostFeeMismoFeeT.CreditUnemploymentInsurancePremium:
                    return "Credit Unemployment Insurance Premium";
                case E_ClosingCostFeeMismoFeeT.DebtCancellationInsurancePremium:
                    return "Debt Cancellation Insurance Premium";
                case E_ClosingCostFeeMismoFeeT.DeedPreparationFee:
                    return "Deed Preparation Fee";
                case E_ClosingCostFeeMismoFeeT.DisasterInspectionFee:
                    return "Disaster Inspection Fee";
                case E_ClosingCostFeeMismoFeeT.DryWallInspectionFee:
                    return "Dry Wall Inspection Fee";
                case E_ClosingCostFeeMismoFeeT.ElectricalInspectionFee:
                    return "Electrical Inspection Fee";
                case E_ClosingCostFeeMismoFeeT.EnvironmentalInspectionFee :
                    return "Environmental Inspection Fee";
                case E_ClosingCostFeeMismoFeeT.FilingFee:
                    return "Filing Fee";
                case E_ClosingCostFeeMismoFeeT.FoundationInspectionFee:
                    return "Foundation Inspection Fee";
                case E_ClosingCostFeeMismoFeeT.HeatingCoolingInspectionFee:
                    return "Heating Cooling Inspection Fee";
                case E_ClosingCostFeeMismoFeeT.HighCostMortgageCounselingFee:
                    return "High Cost Mortgage Counseling Fee";
                case E_ClosingCostFeeMismoFeeT.HomeownersAssociationDues:
                    return "Homeowners Association Dues";
                case E_ClosingCostFeeMismoFeeT.HomeownersAssociationSpecialAssessment:
                    return "Homeowners Association Special Assessment";
                case E_ClosingCostFeeMismoFeeT.HomeWarrantyFee:
                    return "Home Warranty Fee";
                case E_ClosingCostFeeMismoFeeT.LeadInspectionFee:
                    return "Lead Inspection Fee";
                case E_ClosingCostFeeMismoFeeT.LendersAttorneyFee:
                    return "Lenders Attorney Fee";
                case E_ClosingCostFeeMismoFeeT.LoanLevelPriceAdjustment:
                    return "Loan Level Price Adjustment";
                case E_ClosingCostFeeMismoFeeT.ManualUnderwritingFee:
                    return "Manual Underwriting Fee";
                case E_ClosingCostFeeMismoFeeT.ManufacturedHousingInspectionFee:
                    return "Manufactured Housing Inspection Fee";
                case E_ClosingCostFeeMismoFeeT.MIUpfrontPremium:
                    return "MI Upfront Premium";
                case E_ClosingCostFeeMismoFeeT.MoldInspectionFee:
                    return "Mold Inspection Fee";
                case E_ClosingCostFeeMismoFeeT.MortgageSurchargeCountyOrParish:
                    return "Mortgage Surcharge County Or Parish";
                case E_ClosingCostFeeMismoFeeT.MortgageSurchargeMunicipal:
                    return "Mortgage Surcharge Municipal";
                case E_ClosingCostFeeMismoFeeT.MortgageSurchargeState:
                    return "Mortgage Surcharge State";
                case E_ClosingCostFeeMismoFeeT.PlumbingInspectionFee:
                    return "Plumbing Inspection Fee";
                case E_ClosingCostFeeMismoFeeT.PowerOfAttorneyPreparationFee:
                    return "Power Of Attorney Preparation Fee";
                case E_ClosingCostFeeMismoFeeT.PowerOfAttorneyRecordingFee:
                    return "Power Of Attorney Recording Fee";
                case E_ClosingCostFeeMismoFeeT.PreclosingVerificationControlFee:
                    return "Preclosing Verification Control Fee";
                case E_ClosingCostFeeMismoFeeT.PropertyInspectionWaiverFee:
                    return "Property Inspection Waiver Fee";
                case E_ClosingCostFeeMismoFeeT.PropertyTaxStatusResearchFee:
                    return "Property Tax Status Research Fee";
                case E_ClosingCostFeeMismoFeeT.RadonInspectionFee:
                    return "Radon Inspection Fee";
                case E_ClosingCostFeeMismoFeeT.RateLockFee:
                    return "Rate Lock Fee";
                case E_ClosingCostFeeMismoFeeT.ReconveyanceFee:
                    return "Reconveyance Fee";
                case E_ClosingCostFeeMismoFeeT.RecordingFeeForSubordination:
                    return "Recording Fee For Subordination";
                case E_ClosingCostFeeMismoFeeT.RecordingFeeTotal:
                    return "Recording Fee Total";
                case E_ClosingCostFeeMismoFeeT.RepairsFee:
                    return "Repairs Fee";
                case E_ClosingCostFeeMismoFeeT.RoofInspectionFee:
                    return "Roof Inspection Fee";
                case E_ClosingCostFeeMismoFeeT.SepticInspectionFee:
                    return "Septic Inspection Fee";
                case E_ClosingCostFeeMismoFeeT.SmokeDetectorInspectionFee:
                    return "Smoke Detector Inspection Fee";
                case E_ClosingCostFeeMismoFeeT.StateTitleInsuranceFee:
                    return "State Title Insurance Fee";
                case E_ClosingCostFeeMismoFeeT.StructuralInspectionFee:
                    return "Structural Inspection Fee";
                case E_ClosingCostFeeMismoFeeT.TemporaryBuydownAdministrationFee:
                    return "Temporary Buydown Administration Fee";
                case E_ClosingCostFeeMismoFeeT.TemporaryBuydownPoints:
                    return "Temporary Buydown Points";
                case E_ClosingCostFeeMismoFeeT.TitleCertificationFee:
                    return "Title Certification Fee";
                case E_ClosingCostFeeMismoFeeT.TitleClosingFee:
                    return "Title Closing Fee";
                case E_ClosingCostFeeMismoFeeT.TitleDocumentPreparationFee:
                    return "Title Document Preparation Fee";
                case E_ClosingCostFeeMismoFeeT.TitleFinalPolicyShortFormFee:
                    return "Title Final Policy Short Form Fee";
                case E_ClosingCostFeeMismoFeeT.TitleNotaryFee:
                    return "Title Notary Fee";
                case E_ClosingCostFeeMismoFeeT.TitleServicesSalesTax:
                    return "Title Services Sales Tax";
                case E_ClosingCostFeeMismoFeeT.TitleUnderwritingIssueResolutionFee:
                    return "Title Underwriting Issue Resolution Fee";
                case E_ClosingCostFeeMismoFeeT.TransferTaxTotal:
                    return "Transfer Tax Total";
                case E_ClosingCostFeeMismoFeeT.VerificationOfAssetsFee:
                    return "Verification Of Assets Fee";
                case E_ClosingCostFeeMismoFeeT.VerificationOfEmploymentFee:
                    return "Verification Of Employment Fee";
                case E_ClosingCostFeeMismoFeeT.VerificationOfIncomeFee:
                    return "Verification Of Income Fee";
                case E_ClosingCostFeeMismoFeeT.VerificationOfResidencyStatusFee:
                    return "Verification Of Residency Status Fee";
                case E_ClosingCostFeeMismoFeeT.VerificationOfTaxpayerIdentificationFee:
                    return "Verification Of Taxpayer Identification Fee";
                case E_ClosingCostFeeMismoFeeT.VerificationOfTaxReturnFee:
                    return "Verification Of Tax Return Fee";
                case E_ClosingCostFeeMismoFeeT.WaterTestingFee:
                    return "Water Testing Fee";
                case E_ClosingCostFeeMismoFeeT.WellInspectionFee:
                    return "Well Inspection Fee";
                case E_ClosingCostFeeMismoFeeT.VAFundingFee:
                    return "VA Funding Fee";
                case E_ClosingCostFeeMismoFeeT.MIInitialPremium:
                    return "MI Initial Premium";
                case E_ClosingCostFeeMismoFeeT.ChosenInterestRateCreditOrChargeTotal:
                    return "Chosen Interest Rate Credit Or Charge Total";
                case E_ClosingCostFeeMismoFeeT.OurOriginationChargeTotal:
                    return "Our Origination Charge Total";
                case E_ClosingCostFeeMismoFeeT.TitleServicesFeeTotal:
                    return "Title Services Fee Total";
                case E_ClosingCostFeeMismoFeeT.RealEstateCommissionSellersBroker:
                    return "Real Estate Commission Sellers Broker";
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Unknown Mismo Fee Type: " + type.ToString());
            }
        }

        public static string GetFriendlyMismoFeeType(Mismo3Specification.Version4Schema.FeeBase mismo34FeeType)
        {
            switch (mismo34FeeType)
            {
                case Mismo3Specification.Version4Schema.FeeBase.Blank: return "Blank";
                case Mismo3Specification.Version4Schema.FeeBase.Item203KConsultantFee: return "203K Consultant Fee";
                case Mismo3Specification.Version4Schema.FeeBase.Item203KDiscountOnRepairs: return "203K Discount On Repairs";
                case Mismo3Specification.Version4Schema.FeeBase.Item203KInspectionFee: return "203K Inspection Fee";
                case Mismo3Specification.Version4Schema.FeeBase.Item203KPermits: return "203K Permits";
                case Mismo3Specification.Version4Schema.FeeBase.Item203KSupplementalOriginationFee: return "203K Supplemental Origination Fee";
                case Mismo3Specification.Version4Schema.FeeBase.Item203KTitleUpdate: return "203K Title Update";
                case Mismo3Specification.Version4Schema.FeeBase.ApplicationFee: return "Application Fee";
                case Mismo3Specification.Version4Schema.FeeBase.AppraisalDeskReviewFee: return "Appraisal Desk Review Fee";
                case Mismo3Specification.Version4Schema.FeeBase.AppraisalFee: return "Appraisal Fee";
                case Mismo3Specification.Version4Schema.FeeBase.AppraisalFieldReviewFee: return "Appraisal Field Review Fee";
                case Mismo3Specification.Version4Schema.FeeBase.AppraisalManagementCompanyFee: return "Appraisal Management Company Fee";
                case Mismo3Specification.Version4Schema.FeeBase.AsbestosInspectionFee: return "Asbestos Inspection Fee";
                case Mismo3Specification.Version4Schema.FeeBase.AssignmentPreparationFee: return "Assignment Preparation Fee";
                case Mismo3Specification.Version4Schema.FeeBase.AssumptionFee: return "Assumption Fee";
                case Mismo3Specification.Version4Schema.FeeBase.AttorneyFee: return "Attorney Fee";
                case Mismo3Specification.Version4Schema.FeeBase.AutomatedUnderwritingFee: return "Automated Underwriting Fee";
                case Mismo3Specification.Version4Schema.FeeBase.AVMFee: return "AVM Fee";
                case Mismo3Specification.Version4Schema.FeeBase.BankruptcyMonitoringFee: return "Bankruptcy Monitoring Fee";
                case Mismo3Specification.Version4Schema.FeeBase.BondFee: return "Bond Fee";
                case Mismo3Specification.Version4Schema.FeeBase.BondReviewFee: return "Bond Review Fee";
                case Mismo3Specification.Version4Schema.FeeBase.CertificationFee: return "Certification Fee";
                case Mismo3Specification.Version4Schema.FeeBase.ChosenInterestRateCreditOrChargeTotal: return "Chosen Interest Rate Credit Or Charge Total";
                case Mismo3Specification.Version4Schema.FeeBase.CommitmentFee: return "Commitment Fee";
                case Mismo3Specification.Version4Schema.FeeBase.CondominiumAssociationDues: return "Condominium Association Dues";
                case Mismo3Specification.Version4Schema.FeeBase.CondominiumAssociationSpecialAssessment: return "Condominium Association Special Assessment";
                case Mismo3Specification.Version4Schema.FeeBase.CooperativeAssociationDues: return "Cooperative Association Dues";
                case Mismo3Specification.Version4Schema.FeeBase.CooperativeAssociationSpecialAssessment: return "Cooperative Association Special Assessment";
                case Mismo3Specification.Version4Schema.FeeBase.CopyOrFaxFee: return "Copy Or Fax Fee";
                case Mismo3Specification.Version4Schema.FeeBase.CourierFee: return "Courier Fee";
                case Mismo3Specification.Version4Schema.FeeBase.CreditDisabilityInsurancePremium: return "Credit Disability Insurance Premium";
                case Mismo3Specification.Version4Schema.FeeBase.CreditLifeInsurancePremium: return "Credit Life Insurance Premium";
                case Mismo3Specification.Version4Schema.FeeBase.CreditPropertyInsurancePremium: return "Credit Property Insurance Premium";
                case Mismo3Specification.Version4Schema.FeeBase.CreditReportFee: return "Credit Report Fee";
                case Mismo3Specification.Version4Schema.FeeBase.CreditUnemploymentInsurancePremium: return "Credit Unemployment Insurance Premium";
                case Mismo3Specification.Version4Schema.FeeBase.DebtCancellationInsurancePremium: return "Debt Cancellation Insurance Premium";
                case Mismo3Specification.Version4Schema.FeeBase.DebtSuspensionInsurancePremium: return "Debt Suspension Insurance Premium";
                case Mismo3Specification.Version4Schema.FeeBase.DeedPreparationFee: return "Deed Preparation Fee";
                case Mismo3Specification.Version4Schema.FeeBase.DisasterInspectionFee: return "Disaster Inspection Fee";
                case Mismo3Specification.Version4Schema.FeeBase.DiscountOnRepairsFee: return "Discount On Repairs Fee";
                case Mismo3Specification.Version4Schema.FeeBase.DocumentaryStampFee: return "Documentary Stamp Fee";
                case Mismo3Specification.Version4Schema.FeeBase.DocumentPreparationFee: return "Document Preparation Fee";
                case Mismo3Specification.Version4Schema.FeeBase.DownPaymentProtectionFee: return "Down Payment Protection Fee";
                case Mismo3Specification.Version4Schema.FeeBase.DryWallInspectionFee: return "Dry Wall Inspection Fee";
                case Mismo3Specification.Version4Schema.FeeBase.ElectricalInspectionFee: return "Electrical Inspection Fee";
                case Mismo3Specification.Version4Schema.FeeBase.ElectronicDocumentDeliveryFee: return "Electronic Document Delivery Fee";
                case Mismo3Specification.Version4Schema.FeeBase.EnvironmentalInspectionFee: return "Environmental Inspection Fee";
                case Mismo3Specification.Version4Schema.FeeBase.EscrowHoldbackFee: return "Escrow Holdback Fee";
                case Mismo3Specification.Version4Schema.FeeBase.EscrowServiceFee: return "Escrow Service Fee";
                case Mismo3Specification.Version4Schema.FeeBase.EscrowWaiverFee: return "Escrow Waiver Fee";
                case Mismo3Specification.Version4Schema.FeeBase.FilingFee: return "Filing Fee";
                case Mismo3Specification.Version4Schema.FeeBase.FloodCertification: return "Flood Certification";
                case Mismo3Specification.Version4Schema.FeeBase.FoundationInspectionFee: return "Foundation Inspection Fee";
                case Mismo3Specification.Version4Schema.FeeBase.HeatingCoolingInspectionFee: return "Heating Cooling Inspection Fee";
                case Mismo3Specification.Version4Schema.FeeBase.HighCostMortgageCounselingFee: return "High Cost Mortgage Counseling Fee";
                case Mismo3Specification.Version4Schema.FeeBase.HomeInspectionFee: return "Home Inspection Fee";
                case Mismo3Specification.Version4Schema.FeeBase.HomeownersAssociationDues: return "Homeowners Association Dues";
                case Mismo3Specification.Version4Schema.FeeBase.HomeownersAssociationServiceFee: return "Homeowners Association Service Fee";
                case Mismo3Specification.Version4Schema.FeeBase.HomeownersAssociationSpecialAssessment: return "Homeowners Association Special Assessment";
                case Mismo3Specification.Version4Schema.FeeBase.HomeWarrantyFee: return "Home Warranty Fee";
                case Mismo3Specification.Version4Schema.FeeBase.LeadInspectionFee: return "Lead Inspection Fee";
                case Mismo3Specification.Version4Schema.FeeBase.LendersAttorneyFee: return "Lenders Attorney Fee";
                case Mismo3Specification.Version4Schema.FeeBase.LoanDiscountPoints: return "Loan Discount Points";
                case Mismo3Specification.Version4Schema.FeeBase.LoanLevelPriceAdjustment: return "Loan Level Price Adjustment";
                case Mismo3Specification.Version4Schema.FeeBase.LoanOriginationFee: return "Loan Origination Fee";
                case Mismo3Specification.Version4Schema.FeeBase.LoanOriginatorCompensation: return "Loan Originator Compensation";
                case Mismo3Specification.Version4Schema.FeeBase.ManualUnderwritingFee: return "Manual Underwriting Fee";
                case Mismo3Specification.Version4Schema.FeeBase.ManufacturedHousingInspectionFee: return "Manufactured Housing Inspection Fee";
                case Mismo3Specification.Version4Schema.FeeBase.ManufacturedHousingProcessingFee: return "Manufactured Housing Processing Fee";
                case Mismo3Specification.Version4Schema.FeeBase.MERSRegistrationFee: return "MERS Registration Fee";
                case Mismo3Specification.Version4Schema.FeeBase.MIInitialPremium: return "MI Initial Premium";
                case Mismo3Specification.Version4Schema.FeeBase.MIUpfrontPremium: return "MI Upfront Premium";
                case Mismo3Specification.Version4Schema.FeeBase.ModificationFee: return "Modification Fee";
                case Mismo3Specification.Version4Schema.FeeBase.MoldInspectionFee: return "Mold Inspection Fee";
                case Mismo3Specification.Version4Schema.FeeBase.MortgageBrokerFee: return "Mortgage Broker Fee";
                case Mismo3Specification.Version4Schema.FeeBase.MortgageSurchargeCountyOrParish: return "Mortgage Surcharge County Or Parish";
                case Mismo3Specification.Version4Schema.FeeBase.MortgageSurchargeMunicipal: return "Mortgage Surcharge Municipal";
                case Mismo3Specification.Version4Schema.FeeBase.MortgageSurchargeState: return "Mortgage Surcharge State";
                case Mismo3Specification.Version4Schema.FeeBase.MortgageTaxCreditServiceFee: return "Mortgage Tax Credit Service Fee";
                case Mismo3Specification.Version4Schema.FeeBase.MultipleLoansClosingFee: return "Multiple Loans Closing Fee";
                case Mismo3Specification.Version4Schema.FeeBase.MunicipalLienCertificateFee: return "Municipal Lien Certificate Fee";
                case Mismo3Specification.Version4Schema.FeeBase.NotaryFee: return "Notary Fee";
                case Mismo3Specification.Version4Schema.FeeBase.Other: return "Other";
                case Mismo3Specification.Version4Schema.FeeBase.OurOriginationChargeTotal: return "Our Origination Charge Total";
                case Mismo3Specification.Version4Schema.FeeBase.PayoffRequestFee: return "Payoff Request Fee";
                case Mismo3Specification.Version4Schema.FeeBase.PestInspectionFee: return "Pest Inspection Fee";
                case Mismo3Specification.Version4Schema.FeeBase.PlumbingInspectionFee: return "Plumbing Inspection Fee";
                case Mismo3Specification.Version4Schema.FeeBase.PowerOfAttorneyPreparationFee: return "Power Of Attorney Preparation Fee";
                case Mismo3Specification.Version4Schema.FeeBase.PowerOfAttorneyRecordingFee: return "Power Of Attorney Recording Fee";
                case Mismo3Specification.Version4Schema.FeeBase.PreclosingVerificationControlFee: return "Preclosing Verification Control Fee";
                case Mismo3Specification.Version4Schema.FeeBase.ProcessingFee: return "Processing Fee";
                case Mismo3Specification.Version4Schema.FeeBase.ProgramGuaranteeFee: return "Program Guarantee Fee";
                case Mismo3Specification.Version4Schema.FeeBase.PropertyInspectionWaiverFee: return "Property Inspection Waiver Fee";
                case Mismo3Specification.Version4Schema.FeeBase.RadonInspectionFee: return "Radon Inspection Fee";
                case Mismo3Specification.Version4Schema.FeeBase.RateLockFee: return "Rate Lock Fee";
                case Mismo3Specification.Version4Schema.FeeBase.RealEstateCommissionBuyersBroker: return "Real Estate Commission Buyers Broker";
                case Mismo3Specification.Version4Schema.FeeBase.RealEstateCommissionSellersBroker: return "Real Estate Commission Sellers Broker";
                case Mismo3Specification.Version4Schema.FeeBase.ReconveyanceFee: return "Reconveyance Fee";
                case Mismo3Specification.Version4Schema.FeeBase.ReconveyanceTrackingFee: return "Reconveyance Tracking Fee";
                case Mismo3Specification.Version4Schema.FeeBase.RecordingFeeForAssignment: return "Recording Fee For Assignment";
                case Mismo3Specification.Version4Schema.FeeBase.RecordingFeeForDeed: return "Recording Fee For Deed";
                case Mismo3Specification.Version4Schema.FeeBase.RecordingFeeForMortgage: return "Recording Fee For Mortgage";
                case Mismo3Specification.Version4Schema.FeeBase.RecordingFeeForMunicipalLienCertificate: return "Recording Fee For Municipal Lien Certificate";
                case Mismo3Specification.Version4Schema.FeeBase.RecordingFeeForOtherDocument: return "Recording Fee For Other Document";
                case Mismo3Specification.Version4Schema.FeeBase.RecordingFeeForRelease: return "Recording Fee For Release";
                case Mismo3Specification.Version4Schema.FeeBase.RecordingFeeForSubordination: return "Recording Fee For Subordination";
                case Mismo3Specification.Version4Schema.FeeBase.RecordingFeeTotal: return "Recording Fee Total";
                case Mismo3Specification.Version4Schema.FeeBase.RecordingServiceFee: return "Recording Service Fee";
                case Mismo3Specification.Version4Schema.FeeBase.RedrawFee: return "Redraw Fee";
                case Mismo3Specification.Version4Schema.FeeBase.ReinspectionFee: return "Reinspection Fee";
                case Mismo3Specification.Version4Schema.FeeBase.RenovationConsultantFee: return "Renovation Consultant Fee";
                case Mismo3Specification.Version4Schema.FeeBase.RepairsFee: return "Repairs Fee";
                case Mismo3Specification.Version4Schema.FeeBase.RoofInspectionFee: return "Roof Inspection Fee";
                case Mismo3Specification.Version4Schema.FeeBase.SepticInspectionFee: return "Septic Inspection Fee";
                case Mismo3Specification.Version4Schema.FeeBase.SettlementFee: return "Settlement Fee";
                case Mismo3Specification.Version4Schema.FeeBase.SigningAgentFee: return "Signing Agent Fee";
                case Mismo3Specification.Version4Schema.FeeBase.SmokeDetectorInspectionFee: return "Smoke Detector Inspection Fee";
                case Mismo3Specification.Version4Schema.FeeBase.StateTitleInsuranceFee: return "State Title Insurance Fee";
                case Mismo3Specification.Version4Schema.FeeBase.StructuralInspectionFee: return "Structural Inspection Fee";
                case Mismo3Specification.Version4Schema.FeeBase.SubordinationFee: return "Subordination Fee";
                case Mismo3Specification.Version4Schema.FeeBase.SurveyFee: return "Survey Fee";
                case Mismo3Specification.Version4Schema.FeeBase.TaxServiceFee: return "Tax Service Fee";
                case Mismo3Specification.Version4Schema.FeeBase.TaxStampForCityDeed: return "Tax Stamp For City Deed";
                case Mismo3Specification.Version4Schema.FeeBase.TaxStampForCityMortgage: return "Tax Stamp For City Mortgage";
                case Mismo3Specification.Version4Schema.FeeBase.TaxStampForCountyDeed: return "Tax Stamp For County Deed";
                case Mismo3Specification.Version4Schema.FeeBase.TaxStampForCountyMortgage: return "Tax Stamp For County Mortgage";
                case Mismo3Specification.Version4Schema.FeeBase.TaxStampForStateDeed: return "Tax Stamp For State Deed";
                case Mismo3Specification.Version4Schema.FeeBase.TaxStampForStateMortgage: return "Tax Stamp For State Mortgage";
                case Mismo3Specification.Version4Schema.FeeBase.TaxStatusResearchFee: return "Tax Status Research Fee";
                case Mismo3Specification.Version4Schema.FeeBase.TemporaryBuydownAdministrationFee: return "Temporary Buydown Administration Fee";
                case Mismo3Specification.Version4Schema.FeeBase.TemporaryBuydownPoints: return "Temporary Buydown Points";
                case Mismo3Specification.Version4Schema.FeeBase.TitleAbstractFee: return "Title Abstract Fee";
                case Mismo3Specification.Version4Schema.FeeBase.TitleBorrowerClosingProtectionLetterFee: return "Title Borrower Closing Protection Letter Fee";
                case Mismo3Specification.Version4Schema.FeeBase.TitleCertificationFee: return "Title Certification Fee";
                case Mismo3Specification.Version4Schema.FeeBase.TitleClosingCoordinationFee: return "Title Closing Coordination Fee";
                case Mismo3Specification.Version4Schema.FeeBase.TitleClosingFee: return "Title Closing Fee";
                case Mismo3Specification.Version4Schema.FeeBase.TitleClosingProtectionLetterFee: return "Title Closing Protection Letter Fee";
                case Mismo3Specification.Version4Schema.FeeBase.TitleDocumentPreparationFee: return "Title Document Preparation Fee";
                case Mismo3Specification.Version4Schema.FeeBase.TitleEndorsementFee: return "Title Endorsement Fee";
                case Mismo3Specification.Version4Schema.FeeBase.TitleExaminationFee: return "Title Examination Fee";
                case Mismo3Specification.Version4Schema.FeeBase.TitleFinalPolicyShortFormFee: return "Title Final Policy Short Form Fee";
                case Mismo3Specification.Version4Schema.FeeBase.TitleInsuranceBinderFee: return "Title Insurance Binder Fee";
                case Mismo3Specification.Version4Schema.FeeBase.TitleInsuranceFee: return "Title Insurance Fee";
                case Mismo3Specification.Version4Schema.FeeBase.TitleLendersCoveragePremium: return "Title Lenders Coverage Premium";
                case Mismo3Specification.Version4Schema.FeeBase.TitleNotaryFee: return "Title Notary Fee";
                case Mismo3Specification.Version4Schema.FeeBase.TitleOwnersCoveragePremium: return "Title Owners Coverage Premium";
                case Mismo3Specification.Version4Schema.FeeBase.TitleServicesFeeTotal: return "Title Services Fee Total";
                case Mismo3Specification.Version4Schema.FeeBase.TitleServicesSalesTax: return "Title Services Sales Tax";
                case Mismo3Specification.Version4Schema.FeeBase.TitleUnderwritingIssueResolutionFee: return "Title Underwriting Issue Resolution Fee";
                case Mismo3Specification.Version4Schema.FeeBase.TransferTaxTotal: return "Transfer Tax Total";
                case Mismo3Specification.Version4Schema.FeeBase.UnderwritingFee: return "Underwriting Fee";
                case Mismo3Specification.Version4Schema.FeeBase.USDARuralDevelopmentGuaranteeFee: return "USDA Rural Development Guarantee Fee";
                case Mismo3Specification.Version4Schema.FeeBase.VAFundingFee: return "VA Funding Fee";
                case Mismo3Specification.Version4Schema.FeeBase.VerificationOfAssetsFee: return "Verification Of Assets Fee";
                case Mismo3Specification.Version4Schema.FeeBase.VerificationOfEmploymentFee: return "Verification Of Employment Fee";
                case Mismo3Specification.Version4Schema.FeeBase.VerificationOfIncomeFee: return "Verification Of Income Fee";
                case Mismo3Specification.Version4Schema.FeeBase.VerificationOfResidencyStatusFee: return "Verification Of Residency Status Fee";
                case Mismo3Specification.Version4Schema.FeeBase.VerificationOfTaxpayerIdentificationFee: return "Verification Of Taxpayer Identification Fee";
                case Mismo3Specification.Version4Schema.FeeBase.VerificationOfTaxReturnFee: return "Verification Of Tax Return Fee";
                case Mismo3Specification.Version4Schema.FeeBase.WaterTestingFee: return "Water Testing Fee";
                case Mismo3Specification.Version4Schema.FeeBase.WellInspectionFee: return "Well Inspection Fee";
                case Mismo3Specification.Version4Schema.FeeBase.WireTransferFee: return "Wire Transfer Fee";
                default:
                    throw new UnhandledEnumException(mismo34FeeType);
            }
        }

        /// <summary>
        /// Scans document for viruses.
        /// </summary>
        /// <param name="documentContent">The uploaded document as a base 64 encoded string.</param>
        /// <exception cref="CBaseException">The PDF contains a virus.</exception>
        public static void ScanForVirus(string documentContent)
        {
            var documentBytes = Convert.FromBase64String(documentContent);
            ScanForVirus(documentBytes);
        }

        /// <summary>
        /// Scans document for viruses.
        /// </summary>
        /// <param name="documentBytes">The uploaded document as a byte array.</param>
        /// <exception cref="CBaseException">The PDF contains a virus.</exception>
        public static void ScanForVirus(byte[] documentBytes)
        {
            string path = TempFileUtils.NewTempFilePath();
            using (FileStream file = File.OpenWrite(path))
            {
                file.Write(documentBytes, 0, documentBytes.Length);
            }

            E_AntiVirusResult result = AntiVirusScanner.Scan(path);

            File.Delete(path);  // Clean up before throwing.

            if (result == E_AntiVirusResult.HasVirus)
            {
                throw new CBaseException("The document contains a virus.", "Virus found in document.");
            }
        }
        
        /// <summary>
        /// Splits a string containing multiple email addresses.
        /// </summary>
        /// <param name="emailList">A string containing multiple semicolon-delimited addresses.</param>
        /// <returns>A collection of email addresses.</returns>
        public static IEnumerable<string> SplitEmailList(string emailList)
        {
            if (string.IsNullOrEmpty(emailList))
            {
                return Enumerable.Empty<string>();
            }

            return emailList.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                            .Select(email => email.Trim());
        }

        /// <summary>
        /// Joins a collection of email addresses into a single string.
        /// </summary>
        /// <param name="emailList">A collection of email addresses.</param>
        /// <returns>The email addresses in a single semicolon-delimited string.</returns>
        public static string JoinEmailList(IEnumerable<string> emailList)
        {
            return string.Join(";", emailList);
        }

        /// <summary>
        /// Replace certain Unicode chars with similar ASCII equivalents.
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static string ReplaceInvalidUnicodeChars(string s)
        {
            StringBuilder sb = new StringBuilder(s.Length);
            foreach (char ch in s.ToCharArray())
            {
                switch (ch)
                {
                    case '`': case '�':
                    case '�': case '�':
                        sb.Append('\'');
                        break;
                    case '�': case '�':
                        sb.Append('\"');
                        break;
                    case '�':
                        sb.Append('-');
                        break;
                    case (char)0x00A0:
                        sb.Append(' ');
                        break;
                    default:
                        if (IsLegalXmlChar(ch))
                        {
                            sb.Append(ch);
                        }
                        break;
                }
            }
            return sb.ToString();
        }

        /// <summary>
        /// Remove all illegal XML characters from string. The list of allowable characters http://www.w3.org/TR/REC-xml/#charsets
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static string SantizeXmlString(string xml)
        {
            return LqbGrammar.Utils.XmlDataUtil.SantizeXmlString(xml);
        }

        public static bool IsLegalXmlChar(int ch)
        {
            return LqbGrammar.Utils.XmlDataUtil.IsLegalXmlChar(ch);
        }

        public static XmlDocument CreateXmlDoc(string xmlText)
        {
            try
            {
                return CreateXmlDocImpl(xmlText);
            }
            catch (XmlException)
            {
                xmlText = SantizeXmlString(xmlText);
                return CreateXmlDocImpl(xmlText);
            }
            throw CBaseException.GenericException("Should not get here Tools.CreateXmlDoc");
        }

        private static XmlDocument CreateXmlDocImpl(string xmlText)
        {
            XmlDocument xmlDoc = new XmlDocument();

            if (null == xmlText || xmlText.TrimWhitespaceAndBOM() == "")
                return xmlDoc;

            //System.IO.MemoryStream memStream = null;
            XmlTextReader readerData = null;

            try
            {
                /**********

                // Bug when xmlText contains some character such that its ascii code >= 0x80
                // http://opm/default.asp?4118

                System.Text.ASCIIEncoding e = new System.Text.ASCIIEncoding();
                memStream = new MemoryStream( e.GetBytes( xmlText ) );
                readerData = new XmlTextReader( memStream );
                *****/

                if (xmlText != null && xmlText.Length <= ConstAppDavid.ThresholdSizeForCreateXmlDoc)
                {
                    // it runs faster because it uses more memory.
                    readerData = new XmlTextReader(xmlText, XmlNodeType.Document, null);
                }
                else
                    readerData = new XmlTextReader(new StringReader(xmlText));

                // OPM 4796 : tell XmlDocument to not parse any DTD file.
                readerData.XmlResolver = null;

                xmlDoc.Load(readerData);

            }
            finally
            {
                if (null != readerData)
                    readerData.Close();

                /*
				if( null != memStream )
					memStream.Close();
                */
            }
            return xmlDoc;

        }

        public static Result<TXObject> TryParseXml<TXObject>(Func<TXObject> parseXml) where TXObject : XObject
        {
            try
            {
                return Result<TXObject>.Success(parseXml());
            }
            catch (XmlException exc)
            {
                return Result<TXObject>.Failure(exc);
            }
        }

        // 105887
        public static bool AreEnumerablesEqual<T>(bool orderMatters, bool repeatsMatter, IEnumerable<T> ie1, IEnumerable<T> ie2)
        {
            if (ie1 == null)
            {
                if (ie2 != null)
                {
                    return false;
                }
                if (ie2 == null)
                {
                    return true;
                }
            }
            else
            {
                if (ie2 == null)
                {
                    return false;
                }
            }

            if (orderMatters)
            {
                if (repeatsMatter)
                {
                    return AreEnumerablesEqualOrderAndRepeatsMatter(ie1, ie2);
                }
                else
                {
                    return AreEnumerablesEqualOrderMattersRepeatsDont(ie1,ie2);
                }
            }
            else
            {
                if (repeatsMatter)
                {
                    return AreEnumerablesEqualRepeatsMatterOrderDoesnt(ie1, ie2);
                }
                else
                {
                    return AreEnumerablesEqualOrderAndRepeatsDontMatter(ie1, ie2);
                }
            }
        }

        #region methods for AreEnumerablesEqual public method
        private static bool AreEnumerablesEqualOrderMattersRepeatsDont<T>(IEnumerable<T> ie1, IEnumerable<T> ie2)
        {
            // assume nullness already checked by AreEnumerablesEqual
            return AreEnumerablesEqualOrderAndRepeatsDontMatter(ie1.Distinct(), ie2.Distinct()); // assumes distinct preserves order.
        }

        private static bool AreEnumerablesEqualRepeatsMatterOrderDoesnt<T>(IEnumerable<T> ie1, IEnumerable<T> ie2)
        {
            // assume nullness already checked by AreEnumerablesEqual
            var list2 = ie2.ToList();
            foreach (var i in ie1)
            {
                if (list2.Contains(i))
                {
                    list2.Remove(i);
                }
                else
                {
                    return false;
                }
            }
            if (list2.Count > 0)
            {
               return false;
            }
            return true;

        }

        private static bool AreEnumerablesEqualOrderAndRepeatsDontMatter<T>(IEnumerable<T> ie1, IEnumerable<T> ie2)
        {
            // assume nullness already checked by AreEnumerablesEqual
            if (ie1.Except(ie2).Any() || ie2.Except(ie1).Any())
            {
                return false;
            }
            return true;
        }


        private static bool AreEnumerablesEqualOrderAndRepeatsMatter<T>(IEnumerable<T> ie1, IEnumerable<T> ie2)
        {
            // assume nullness already checked by AreEnumerablesEqual
            if(ie1.Count() != ie2.Count())
            {
                return false;
            }
            var tmp1 = ie1.ToArray();
            var tmp2 = ie2.ToArray();
            for(int i = 0; i < tmp1.Length; i++)
            {
                if(false == tmp1[i].Equals(tmp2[i]))
                {
                    return false;
                }
            }
            return true;


        }
        #endregion

        public static int GetListHashCode<T>(List<T> list)
        {
            if (list == null)
            {
                return 0;
            }

            int hash = 13;
            foreach (var item in list)
            {
                hash = hash * 7 + (item == null ? 0 : item.GetHashCode());
            }
            return hash;

        }

        public static string FormatSingleLineAddress(string address, string city, string state, string zipcode)
        {
            StringBuilder sb = new StringBuilder(60);
            string csz = CombineCityStateZip(city, state, zipcode);
            string A = address == null ? "" : address.TrimWhitespaceAndBOM();

            if ("" != A)
            {
                sb.Append(A);
            }

            if ("" != csz)
            {
                if ("" != A)
                    sb.Append(", ");

                sb.Append(csz);
            }
            return sb.ToString();

        }

        /// <summary>
        /// Construct multi-line address. First line is name, second line is address, third line is city, state, zipcode and fourth line is description.
        /// </summary>
        /// <param name="address"></param>
        /// <param name="city"></param>
        /// <param name="state"></param>
        /// <param name="zipcode"></param>
        /// <returns></returns>
        public static string FormatSigneLineAddressCustom(string name, string address, string city, string state, string zipcode, string phone)
        {
            StringBuilder fullAddress = new StringBuilder(60);
            StringBuilder addressLine2 = new StringBuilder(60);

            name = name == null ? "" : name.TrimWhitespaceAndBOM();
            address = address == null ? "" : address.TrimWhitespaceAndBOM();
            city = city == null ? "" : city.TrimWhitespaceAndBOM();
            state  = state == null ? "" : state.TrimWhitespaceAndBOM();
            zipcode =  zipcode == null ? "" : zipcode.TrimWhitespaceAndBOM();
            phone = phone == null ? "" : phone.TrimWhitespaceAndBOM();


            addressLine2.Append(city);  //we need to calculate how much padding to add so need to do address line 2 first
            if (false == string.IsNullOrEmpty(state) || false == string.IsNullOrEmpty(zipcode))
            {
                if(false== string.IsNullOrEmpty(city))
                {
                    addressLine2.Append(", ");
                }

                if( false == string.IsNullOrEmpty(state) )
                {
                    addressLine2.Append(state).Append(" ");
                }
                if (false == string.IsNullOrEmpty(zipcode))
                {
                    addressLine2.Append(zipcode);
                }
            }

            if (false == string.IsNullOrEmpty(phone))
            {
                //int addressLine2Padding = addressLine2.Length - address.Length;
                //int namePadding = name.Length - address.Length;
                //int paddingNeeded = Math.Max(addressLine2.Length, Math.Max(name.Length, address.Length)) + 5;


                name = name + "     Phone: " + phone;
                //address = address.PadRight(30) + phone;

            }

            if (false == string.IsNullOrEmpty(name))
            {
                fullAddress.AppendLine(name);
            }

            if (false == string.IsNullOrEmpty(address))
            {
                fullAddress.AppendLine(address);
            }

            fullAddress.Append(addressLine2.ToString());

            return fullAddress.ToString();
        }

        /// <summary>
        /// Construct multi-line address. First line is name, second line is address, third line is city, state, zipcode and fourth line is description.
        /// </summary>
        /// <param name="address"></param>
        /// <param name="city"></param>
        /// <param name="state"></param>
        /// <param name="zipcode"></param>
        /// <returns></returns>
        public static string FormatAddress(string name, string address, string city, string state, string zipcode, string description)
        {
            StringBuilder sBuild = new StringBuilder(60);
            string N = name == null ? "" : name.TrimWhitespaceAndBOM();
            string A = address == null ? "" : address.TrimWhitespaceAndBOM();
            string C = city == null ? "" : city.TrimWhitespaceAndBOM();
            string S = state == null ? "" : state.TrimWhitespaceAndBOM();
            string Z = zipcode == null ? "" : zipcode.TrimWhitespaceAndBOM();
            string D = description == null ? "" : description.TrimWhitespaceAndBOM();

            if ("" != N)
            {
                sBuild.Append(N).Append(Environment.NewLine);
            }
            if ("" != A)
            {
                sBuild.Append(A).Append(Environment.NewLine);
            }
            if ("" != C)
            {
                sBuild.Append(C);

                if ("" != (S + Z))
                    sBuild.Append(", ");
            }

            if ("" != S)
            {
                sBuild.Append(S);
                if ("" != Z)
                    sBuild.Append(" ");
            }

            sBuild.Append(Z);
            if ("" != D)
            {
                sBuild.Append(Environment.NewLine).Append(D);
            }
            return sBuild.ToString();

        }
        public static string FormatAddress(string name, string address, string city, string state, string zipcode)
        {
            return FormatAddress(name, address, city, state, zipcode, null);
        }
        /// <summary>
        /// Construct multi-line address. First line is address, and second line is city, state, zipcode.
        /// </summary>
        /// <param name="address"></param>
        /// <param name="city"></param>
        /// <param name="state"></param>
        /// <param name="zipcode"></param>
        /// <returns></returns>
        public static string FormatAddress(string address, string city, string state, string zipcode)
        {
            return FormatAddress(null, address, city, state, zipcode, null);
        }
        static public string CombineCityStateZip(string city, string state, string zip)
        {
            return FormatAddress(null, null, city, state, zip, null);
        }

        public static string GetCountyFromZipCode(string zipCode)
        {

            string county = string.Empty;
            string city = string.Empty;
            string state = string.Empty;

            if (GetCountyCityStateByZip(zipCode, out county, out city, out state) == true)
            {
                return county;
            }
            else
            {
                return string.Empty;
            }

        }

        public static bool GetCountyCityStateByZip(string zipCode, out string county, out string city, out string state)
        {
            county = string.Empty;
            city = string.Empty;
            state = string.Empty;
            SqlParameter[] parameters = {
                                            new SqlParameter("@Zipcode", zipCode)
                                        };
            bool isValid = false;

            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "GetCountyCityStateByZip", parameters ))
            {
                if (reader.Read())
                {
                    county = StateInfo.Instance.Sanitize(reader["County"].ToString());
                    city = reader["City"].ToString();
                    state = reader["State"].ToString();
                    isValid = true;
                }
            }

            return isValid;
        }

        public static string SortCommaDelimitedString(string input)
        {
            string[] array = input.Split(',');
            Array.Sort(array);
            StringBuilder output = new StringBuilder(input.Length + 2);
            foreach (string s in array)
            {
                output.Append(s);
                output.Append(", ");
            }
            return output.ToString().Substring(0, output.Length - 2);
        }

        public static string GenerateUniqueString(Exception exc)
        {
            if (exc == null)
            {
                return string.Empty;
            }
            if (exc is System.OutOfMemoryException)
            {
                // 3/5/2013 dd - For out of memory exception combine with server name to get unique key.
                return ConstAppDavid.ServerName + "::System.OutOfMemoryException";
            }
            if (exc is DataAccess.GenericSqlException)
            {
                // 7/8/2013 dd - EmailSubjectCode contains the stored procedure of the error.
                return ((DataAccess.GenericSqlException)exc).EmailSubjectCode;
            }

            // 1/2/2015 dd - Generate unique checksum base on the exception stack trace.
            // To avoid a different checksum generate due to a shift in line number, I stripped out all
            // the line number from stack trace.
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(exc.GetType().FullName);
            StripOutFileLocationInStackTrace(sb, exc.StackTrace);

            if (exc.InnerException != null)
            {

                if (exc.InnerException is System.OutOfMemoryException)
                {
                    // 3/5/2013 dd - For out of memory exception combine with server name to get unique key.
                    return ConstAppDavid.ServerName + "::System.OutOfMemoryException";

                }
                sb.Append(exc.InnerException.GetType().FullName);
                StripOutFileLocationInStackTrace(sb, exc.InnerException.StackTrace);

            }

            return EncryptionHelper.ComputeSHA256Hash(sb.ToString()).Replace("/", "-").Replace("+", "_").Replace("=", "");
        }

        /// <summary>
        /// Remove file line location from stack trace.
        ///
        /// Input:
        ///     at LendOSolnTest.Admin.BrokerDBTest.LendingLicenseTest() in C:\Programming\lendosoln\LendOSolnTest\Admin\BrokerDBTest.cs:line 24
        /// Output:
        ///     at LendOSolnTest.Admin.BrokerDBTest.LendingLicenseTest()
        /// </summary>
        /// <param name="sb"></param>
        /// <param name="stackTrace"></param>
        private static void StripOutFileLocationInStackTrace(StringBuilder sb, string stackTrace)
        {
            if (string.IsNullOrEmpty(stackTrace))
            {
                return;
            }

            foreach (var line in stackTrace.Split(new string[] { Environment.NewLine },StringSplitOptions.RemoveEmptyEntries))
            {
                int index = line.IndexOf(") in ");

                if (index > 0)
                {
                    sb.AppendLine(line.Substring(0, index + 1));
                }
                else
                {
                    sb.AppendLine(line);
                }
            }
        }

        private static string GetExceptionTypeInfo(Exception exc)
        {
            try
            {
                return String.Format("{0}{0}Exception type : {1}", Environment.NewLine, exc.GetType().FullName);
            }
            catch
            {
                return "";
            }

        }
        private static void LogExceptionImpl(StringBuilder sb, int indentLevel, Exception exc)
        {
            LogExceptionAppendLineImpl(sb, indentLevel, exc.GetType().FullName + ": " + exc.Message);

            if (string.IsNullOrEmpty(exc.Source) == false)
            {
                sb.AppendLine();
                LogExceptionAppendLineImpl(sb, indentLevel, "Source: " + exc.Source);
            }

            if (exc.Data != null && exc.Data.Count > 0)
            {
                sb.AppendLine();
                LogExceptionAppendLineImpl(sb, indentLevel, "Data:");
                foreach (var key in exc.Data.Keys)
                {
                    LogExceptionAppendLineImpl(sb, indentLevel, "   [" + key + "] - [" + exc.Data[key] + "]");
                }
            }

            if (exc.InnerException != null)
            {
                sb.AppendLine();
                LogExceptionAppendLineImpl(sb, indentLevel + 1, "--- Start inner exception ---");
                LogExceptionImpl(sb, indentLevel + 1, exc.InnerException);
                LogExceptionAppendLineImpl(sb, indentLevel + 1, "--- End inner exception ---");
            }

            string stackTrace = exc.StackTrace;

            if (string.IsNullOrEmpty(stackTrace) == false)
            {
                sb.AppendLine();
                LogExceptionAppendLineImpl(sb, indentLevel, "StackTrace:");
                LogExceptionAppendLineImpl(sb, indentLevel, stackTrace);
            }

        }
        private static void LogExceptionAppendLineImpl(StringBuilder sb, int indentLevel, string msg)
        {
            if (msg == null)
            {
                sb.AppendLine("<null>");
                return;
            }
            string[] lines = msg.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

            string padding = new string(' ', indentLevel * 3);

            foreach (var o in lines)
            {
                sb.AppendLine(padding + o);
            }
        }

        private static void LogErrorImpl(string msg, Exception exc)
        {
            if (string.IsNullOrEmpty(msg) && exc == null)
            {
                return; // Nothing to log.
            }


            StringBuilder sb = new StringBuilder();
            int indentLevel = 0;

            string category = "Error";
            string uniqueString = string.Empty;

            if (string.IsNullOrEmpty(msg) == false)
            {
                LogExceptionAppendLineImpl(sb, indentLevel, msg);
                sb.AppendLine();
            }

            if (exc != null)
            {
                LogExceptionImpl(sb, indentLevel, exc);

                category = exc.GetType().FullName;
                uniqueString = GenerateUniqueString(exc);
            }

            Dictionary<string, string> extraProperties = new Dictionary<string, string>();

            if (string.IsNullOrEmpty(uniqueString) == false)
            {
                sb.AppendLine();
                sb.AppendLine("UniqueString: [" + uniqueString + "]");

                extraProperties.Add("error_unique_id", uniqueString);
            }



            StackTrace stackTrace = new StackTrace(0, true); // Include caller stack information.
            sb.AppendLine();
            sb.AppendLine(stackTrace.ToString());

            PaulBunyanHelper.Log(LoggingLevel.Error, category, sb.ToString(), extraProperties);
        }

        public static void LogError(string msg, Exception exc)
        {
            LogErrorImpl(msg, exc);
        }

        public static void LogError(Exception exc)
        {
            LogErrorImpl(string.Empty, exc);
        }

        public static void LogError(string msg)
        {
            LogErrorImpl(msg, null);
        }

        public static void LogWarning(string msg)
        {
            LogWarningImpl(msg, null);
        }

        public static void LogWarning(string msg, Exception exc)
        {
            LogWarningImpl(msg, exc);
        }
        public static void LogWarning(Exception exc)
        {
            LogWarning(null, exc);
        }

        private static void LogWarningImpl(string msg, Exception exc)
        {
            StringBuilder sb = new StringBuilder();
            string category = string.Empty;

            if (string.IsNullOrEmpty(msg) == false)
            {
                sb.AppendLine(msg);
                sb.AppendLine();
            }

            if (exc != null)
            {
                category = exc.GetType().FullName;
                LogExceptionImpl(sb, 0, exc);
            }

            StackTrace stackTrace = new StackTrace(0, true); // Include caller stack information.
            sb.AppendLine();
            sb.AppendLine(stackTrace.ToString());
            PaulBunyanHelper.Log(LoggingLevel.Warn, category, sb.ToString(), null);
        }

        static public void LogErrorWithCriticalTracking(Exception exc)
        {
            LogErrorWithCriticalTracking(string.Empty, exc);
        }

        static public void LogErrorWithCriticalTracking(string msg, Exception exc)
        {
            // Tracking is for sending Critical Email. So skip tracking if exception is set to not email developer.
            if (exc == null ||
                (exc is DataAccess.CBaseException && !((DataAccess.CBaseException)exc).IsEmailDeveloper))
            {
                LogErrorImpl(msg, exc);
                return;
            }

            LogErrorWithTag(msg, exc);
            LendersOffice.Common.EmailUtilities.UpdateExceptionTracking(msg, exc);
        }

        static public void LogErrorWithCriticalTracking(string msg)
        {
            LogErrorWithTag(msg, null);
            LendersOffice.Common.EmailUtilities.UpdateExceptionTracking(msg);
        }

        /// <summary>
        /// Logs an exception.
        /// </summary>
        /// <param name="msg">Additional message to add to log.</param>
        /// <param name="exc">The Exception.</param>
        /// <remarks>
        /// If we ever need to expand this to use different tags, consider
        /// adding an enum parameter (instead of a string parameter) such that
        /// taggedMessage = $"[{tagT.ToString()}]{msg}";
        /// As using an enum allows us to restrict what strings can be used as tags.
        /// </remarks>
        static private void LogErrorWithTag(string msg, Exception exc)
        {
            string taggedMsg = "[LoException]" + msg;
            LogErrorImpl(taggedMsg, exc);
        }

        static public void LogErrorFeedback(string errorReferenceNumber, string userMessage, string developerMessage, string name, string phone, bool contactByPhone, string email, bool contactByEmail, bool recurringError, string errorOnPage, string lastClicked, string urlReferrer)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(string.Format("[{0}-LOException Feedback]ErrRef#: {1}", ConstAppDavid.ServerName, errorReferenceNumber)).Append(Environment.NewLine);
            sb.Append("[USER NAME]: ").Append(name).Append(Environment.NewLine);
            sb.Append("[CONTACT USER BY PHONE]: ").Append(contactByPhone ? "YES" : "NO").Append(Environment.NewLine);
            sb.Append("[USER PHONE]: ").Append(phone).Append(Environment.NewLine);
            sb.Append("[CONTACT USER BY EMAIL]: ").Append(contactByEmail ? "YES" : "NO").Append(Environment.NewLine);
            sb.Append("[USER EMAIL]: ").Append(email).Append(Environment.NewLine);

            var principal = PrincipalFactory.CurrentPrincipal;

            if (principal != null)
            {
                sb.Append("[USER LOGIN]: ").Append(principal.LoginNm).Append(Environment.NewLine);
                sb.Append("[USER ID]: ").Append(principal.UserId).Append(Environment.NewLine);
            }

            sb.Append("[ERROR OCCURED MORE THAN ONCE]: ").Append(recurringError ? "YES" : "NO").Append(Environment.NewLine);
            sb.Append("[PAGE WHERE ERROR OCCURED]: ").Append(errorOnPage).Append(Environment.NewLine);
            sb.Append("[BUTTON OR LINK LAST CLICKED]: ").Append(lastClicked).Append(Environment.NewLine);

            sb.Append(Environment.NewLine).Append(Environment.NewLine);
            sb.Append("[USER ERROR MESSAGE]: ").Append(userMessage).Append(Environment.NewLine);
            sb.Append("[DEVELOPER ERROR MESSAGE]: ").Append(developerMessage).Append(Environment.NewLine);
            sb.Append("[PRE POSTBACK URL REFERRER]: ").Append(urlReferrer).Append(Environment.NewLine);
            sb.AppendFormat("[APPLICATION]: {0}{1}", ConstAppDavid.ServerName, Environment.NewLine);

            Tools.LogError(sb.ToString());
        }

        static public void LogBug(string msg)
        {
            StackTrace trace = new StackTrace(true);
            PaulBunyanHelper.Log(LoggingLevel.Error, "BUG",msg + Environment.NewLine + "*** CALL STACK:" + Environment.NewLine + trace.ToString(), null);
        }

        public static void LogInfo(string msg)
        {
            PaulBunyanHelper.Log(LoggingLevel.Info, string.Empty /* category */, msg, null);
        }

        public static void LogInfo(string category, string message)
        {
            PaulBunyanHelper.Log(LoggingLevel.Info, category, message, null);
        }

        public static void LogInfo(string category, string message, int timeProcessingInMs)
        {
            var logProperties = new Dictionary<string, string>(1)
            {
                ["timing_processing"] = timeProcessingInMs.ToString()
            };

            PaulBunyanHelper.Log(LoggingLevel.Info, category, message, logProperties);
        }

        static public void LogInfoWithStackTrace(string msg)
        {
            StackTrace trace = new StackTrace(true);
            PaulBunyanHelper.Log(LoggingLevel.Info, string.Empty, msg + Environment.NewLine + "*** CALL STACK:" + Environment.NewLine + trace.ToString(), null);
        }
        /// <summary>
        /// pbSearchString should be more or less unique to allow for ease of lookup.
        /// Additionally logs the userid, or the callstack if no principal is available.
        /// </summary>
        static public void LogInfoWithUserInfo(string pbSearchString, string msg)
        {
            var principal = PrincipalFactory.CurrentPrincipal;
            if (principal == null)
            {
                LogInfoWithStackTrace(
                      pbSearchString + Environment.NewLine
                    + "Principal was null." + Environment.NewLine
                    + msg
                    );
            }
            else
            {
                var principalIDString = principal.UserId.ToString();
                LogInfo(
                      pbSearchString + Environment.NewLine
                    + "By userid: " + principalIDString + Environment.NewLine
                    + msg
                    );
            }
        }

        static public void LogVerbose(string msg)
        {
            PaulBunyanHelper.Log(LoggingLevel.Trace, string.Empty, msg, null);
        }

        static public void LogRegTest(string msg)
        {
            PaulBunyanHelper.Log(LoggingLevel.Info, "REGTEST", msg, null);
        }

        /// <summary>
        /// Convert Guid to base64. This base64 string is use as unique string in log.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string ConvertGuidToFriendlyBase64(Guid id)
        {
            if (id == Guid.Empty)
            {
                return string.Empty;
            }
            return Convert.ToBase64String(id.ToByteArray()).Replace("/", "-").Replace("+", "_").Replace("==", "");
        }

        /// <summary>
        /// Generate a new correlation id. Only call this method if a same thread is use for multiple run (i.e continuous service)
        /// </summary>
        public static void ResetLogCorrelationId()
        {
            SetCorrelationId(Guid.NewGuid());
        }

        public static Guid GetLogCorrelationId()
        {
            return Trace.CorrelationManager.ActivityId;
        }

        public static void SetCorrelationId(Guid correlationId)
        {
            Trace.CorrelationManager.ActivityId = correlationId;
        }

        public enum LogMadnessType
        {
            TestDependencyGraphError = 0, //user 0x8000
            TestDependencyGraphInfo = 1, //user 0x4000
            CachedDataMigrationInfo = 2, //user 0x2000
            TaskMonitorStatus = 3, //user 0x1000
            SecurityCheck = 4, //user 0x0800
            SecurityStatus = 5, //user 0x0400
            LpeAuthorInfo = 6, //user 0x0200
            RolePumpMsgQ = 7, //user 0x0100
        }

        public static void LogMadness(LogMadnessType type, string msg)
        {
            string category = string.Empty;
            LoggingLevel logLevel = LoggingLevel.Info;

            switch (type)
            {
                case LogMadnessType.TestDependencyGraphError:
                    category = "DependencyGraph";
                    logLevel = LoggingLevel.Error;
                    break;
                case LogMadnessType.TestDependencyGraphInfo:
                    category = "DependencyGraph";
                    logLevel = LoggingLevel.Info;
                    break;
                case LogMadnessType.CachedDataMigrationInfo:
                    category = "DataMigration";
                    logLevel = LoggingLevel.Info;
                    break;
                case LogMadnessType.TaskMonitorStatus:
                    category = "TaskMonitorStatus";
                    logLevel = LoggingLevel.Info;
                    break;

                case LogMadnessType.SecurityCheck:
                    category = "SecurityCheckError";
                    logLevel = LoggingLevel.Error;
                    break;

                case LogMadnessType.SecurityStatus:
                    category = "SecurityStatus";
                    logLevel = LoggingLevel.Error;
                    break;
                case LogMadnessType.LpeAuthorInfo:
                    category = "LpeAuthor";
                    logLevel = LoggingLevel.Info;
                    break;

                case LogMadnessType.RolePumpMsgQ:
                    category = "RolePumpMsgQ";
                    logLevel = LoggingLevel.Info;
                    break;

                default:
                    throw new UnhandledEnumException(type);
            }

            PaulBunyanHelper.Log(logLevel, category, msg, null);
        }

        static public void LogMadness(LogMadnessType type, Exception ex)
        {
            LogMadness(type, ex.ToString());
        }

        static public int CalcAgeForToday(DateTime dob)
        {

            DateTime now = DateTime.Now;
            return CalcAgeForDate(dob, now);
        }

        static public int CalcAgeForDate(DateTime dob, DateTime compareDate)
        {
            if (dob.CompareTo(compareDate) >= 0)
                throw new CBaseException("Date of birth must be before the comparing date.", "Date of birth must be before the comparing date.");

            int age = compareDate.Year - dob.Year;

            DateTime thisYearBirthday = dob.AddYears(compareDate.Year - dob.Year); // making it the same year so we can compare the month and day
            if (thisYearBirthday.CompareTo(compareDate) > 0)
                --age; // haven't got to this year birthday.
            return age;
        }

        /// <summary>
        /// Causes the current thread to sleep for an amount of time
        /// between <paramref name="msMinSleep"/> and <paramref name="msMaxSleep"/>.
        /// </summary>
        /// <param name="msMinSleep">in miliseconds</param>
        /// <param name="msMaxSleep">in miliseconds</param>
        /// <param name="logWarning">true if a warning should be logged, false otherwise</param>
        /// <returns>sleep duration in miliseconds</returns>
        static public int SleepAndWakeupRandomlyWithin(int msMinSleep, int msMaxSleep, bool logWarning = true)
        {
            int duration = 0;

            try
            {
                Tools.Assert(msMinSleep < msMaxSleep, "msMinSleep is expected to be smaller than msMaxSleep");

                Random rdm = new Random(unchecked((int)DateTime.Now.Ticks));

                duration = rdm.Next(msMinSleep, msMaxSleep);

                if (logWarning)
                {
                    Tools.LogWarning(string.Format("Sleeping for {0} msec", duration.ToString()));
                }

                LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(duration);
            }
            catch { }

            return duration;
        }

        public static void Bind_sWADisclosureVersionT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Leave Blank", E_sWADisclosureVersionT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Initial", E_sWADisclosureVersionT.Initial));
            ddl.Items.Add(CreateEnumListItem("Revised", E_sWADisclosureVersionT.Revised));
        }

        /// <summary>
        /// Precompiles every Type used in the given assembly.
        /// </summary>
        /// <param name="assembly"></param>
        public static void RunJITOn(params Assembly[] assemblies)
        {
            Stopwatch sw;

            foreach (Assembly assembly in assemblies)
            {
                sw = Stopwatch.StartNew();
                Type[] types = assembly.GetTypes();
                foreach (Type curType in types)
                {
                    MethodInfo[] methods = curType.GetMethods(
                            BindingFlags.DeclaredOnly |
                            BindingFlags.NonPublic |
                            BindingFlags.Public |
                            BindingFlags.Instance |
                            BindingFlags.Static);

                    foreach (MethodInfo curMethod in methods)
                    {
                        if (curMethod.IsAbstract ||
                            curMethod.ContainsGenericParameters)
                            continue;

                        System.Runtime.CompilerServices.RuntimeHelpers.PrepareMethod(curMethod.MethodHandle);
                    }
                }
                sw.Stop();
                Tools.LogInfo(assembly.GetName().Name + " took " + sw.ElapsedMilliseconds + "ms to precompile.");
            }
        }

        /// <summary>
        /// Returns the file path of the given name. This works when there is a context and there is
        /// no context.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetServerMapPath(string name)
        {
            string result;
            HttpContext httpContext = HttpContext.Current;
            if (null != httpContext)
            {
                result = httpContext.Server.MapPath(name);
            }
            else
            {
                // 8/23/2013 AV - 136577 Edoc Auto Saving Not Working for Pml Submitted Cert
                result = System.Web.Hosting.HostingEnvironment.MapPath(name) ?? string.Empty;
            }

            return result;
        }

        /// <summary>
        /// Based on the package type being drawn, send out different forms. UI PTM Spec 2.2.9
        ///
        /// </summary>
        /// <remarks>
        /// If you change this, MAKE SURE YOU CHANGE GetDocMagicExportOptionsForProcess(processPackageType, ...) AS WELL
        ///
        /// Changed in OPM 89894. Any package type that is not predisclosure,
        /// loan application, point of sale, or redisclosure should now send
        /// Settlement Charges and "Include GFE data from Lender's Office
        /// for GFE comparison on HUD-1".
        /// </remarks>
        /// <param name="auditPackageType"></param>
        /// <returns>
        /// True if we want to export from the GFE or Settlement Charges specifically.
        /// False if we want to load from the loan.</returns>
        public static bool GetDocMagicExportOptionsForAudit(DocMagic.DsiDocRequest.E_AuditPackageType auditPackageType, out E_SettlementChargesExportSource exportSource, out bool includeGfeDataForDocMagicComparison)
        {
            switch (auditPackageType)
            {
                case DocMagic.DsiDocRequest.E_AuditPackageType.Predisclosure:
                case DocMagic.DsiDocRequest.E_AuditPackageType.LoanApplication:
                case DocMagic.DsiDocRequest.E_AuditPackageType.PointOfSale:
                case DocMagic.DsiDocRequest.E_AuditPackageType.Redisclosure:
                case DocMagic.DsiDocRequest.E_AuditPackageType.Prequalification:
                    exportSource = E_SettlementChargesExportSource.GFE;
                    includeGfeDataForDocMagicComparison = false;
                    return true;

                case DocMagic.DsiDocRequest.E_AuditPackageType.Closing:
                case DocMagic.DsiDocRequest.E_AuditPackageType.ServicingTransfer:
                case DocMagic.DsiDocRequest.E_AuditPackageType.PostClosing:
                case DocMagic.DsiDocRequest.E_AuditPackageType.Adverse:
                case DocMagic.DsiDocRequest.E_AuditPackageType.FloodCertification:
                case DocMagic.DsiDocRequest.E_AuditPackageType.FormList:
                case DocMagic.DsiDocRequest.E_AuditPackageType.LoanModification:
                case DocMagic.DsiDocRequest.E_AuditPackageType.Preclosing:
                case DocMagic.DsiDocRequest.E_AuditPackageType.Processing:
                case DocMagic.DsiDocRequest.E_AuditPackageType.Undefined:
                case DocMagic.DsiDocRequest.E_AuditPackageType.Underwriting:
                    exportSource = E_SettlementChargesExportSource.SETTLEMENT;
                    includeGfeDataForDocMagicComparison = true;
                    return true;
                default:
                    throw new UnhandledEnumException(auditPackageType);
            }
        }

        /// <summary>
        /// Based on the package type being drawn, send out different forms. UI PTM Spec 2.2.9
        /// </summary>
        /// <remarks>
        /// If you change this, MAKE SURE YOU CHANGE GetDocMagicExportOptions(auditPackageType, ...) AS WELL
        ///
        /// Changed in OPM 89894. Any package type that is not predisclosure,
        /// loan application, point of sale, or redisclosure should now send
        /// Settlement Charges and "Include GFE data from Lender's Office
        /// for GFE comparison on HUD-1".
        /// </remarks>
        /// <param name="processPackageType"></param>
        /// <returns>
        /// True if we want to export from the GFE or Settlement Charges specifically.
        /// False if we want to load from the loan.</returns>
        public static bool GetDocMagicExportOptionsForProcess(DocMagic.DsiDocRequest.E_ProcessPackageType processPackageType, out E_SettlementChargesExportSource exportSource, out bool includeGfeDataForDocMagicComparison)
        {
            switch (processPackageType)
            {
                case DocMagic.DsiDocRequest.E_ProcessPackageType.Predisclosure:
                case DocMagic.DsiDocRequest.E_ProcessPackageType.LoanApplication:
                case DocMagic.DsiDocRequest.E_ProcessPackageType.PointOfSale:
                case DocMagic.DsiDocRequest.E_ProcessPackageType.Redisclosure:
                case DocMagic.DsiDocRequest.E_ProcessPackageType.Prequalification:
                    exportSource = E_SettlementChargesExportSource.GFE;
                    includeGfeDataForDocMagicComparison = false;
                    return true;

                case DocMagic.DsiDocRequest.E_ProcessPackageType.Closing:
                case DocMagic.DsiDocRequest.E_ProcessPackageType.ServicingTransfer:
                case DocMagic.DsiDocRequest.E_ProcessPackageType.PostClosing:
                case DocMagic.DsiDocRequest.E_ProcessPackageType.Adverse:
                case DocMagic.DsiDocRequest.E_ProcessPackageType.FloodCertification:
                case DocMagic.DsiDocRequest.E_ProcessPackageType.FormList:
                case DocMagic.DsiDocRequest.E_ProcessPackageType.LoanModification:
                case DocMagic.DsiDocRequest.E_ProcessPackageType.Preclosing:
                case DocMagic.DsiDocRequest.E_ProcessPackageType.Processing:
                case DocMagic.DsiDocRequest.E_ProcessPackageType.Undefined:
                case DocMagic.DsiDocRequest.E_ProcessPackageType.Underwriting:
                    exportSource = E_SettlementChargesExportSource.SETTLEMENT;
                    includeGfeDataForDocMagicComparison = true;
                    return true;
                default:
                    throw new UnhandledEnumException(processPackageType);
            }
        }

        private static System.Text.RegularExpressions.Regex inflate =
            new System.Text.RegularExpressions.Regex("([A-Z][^A-Z])", System.Text.RegularExpressions.RegexOptions.Compiled);

        public static void Bind_Generic(DropDownList ddl, Enum e)
        {
            var elements = Enum.GetNames(e.GetType());
            Bind_Generic(ddl, e, elements);
        }

        public static void Bind_Generic(DropDownList ddl, Enum e, IEnumerable<string> NamesToBind)
        {
            Bind_Generic(ddl, e, NamesToBind, a => a.Replace("LeaveBlank", ""));
        }

        public static void Bind_Generic(DropDownList ddl, Enum e, Func<string, string> NameModifier)
        {
            var elements = Enum.GetNames(e.GetType());
            Bind_Generic(ddl, e, elements, NameModifier);
        }

        public static void Bind_Generic(DropDownList ddl, Enum e, IEnumerable<string> NamesToBind, Func<string, string> NameModifier)
        {
            foreach (var name in NamesToBind)
            {
                bool selected = Enum.GetName(e.GetType(), e) == name;
                ddl.Items.Add(CreateEnumListItem(inflate.Replace(NameModifier(name), @" $1").TrimWhitespaceAndBOM(),
                                                 (Enum)Enum.Parse(e.GetType(), name), selected));
            }
        }

        public static void Bind_Generic(DropDownList ddl, IEnumerable<string> items, Func<string, bool> selected)
        {
            Bind_Generic<string>(ddl, items, selected, a => a, a => a);
        }

        public static void Bind_Generic<T>(DropDownList ddl, IEnumerable<T> items, Func<T, bool> selected, Func<T, string> name, Func<T, string> value)
        {
            foreach (var item in items)
            {
                var isSelected = selected(item);
                var li = new ListItem(name(item), value(item));
                li.Selected = isSelected;
                ddl.Items.Add(li);
            }
        }

        public static void Bind_SettlementType(DropDownList ddl, E_SettlementType selectedValue)
        {
            ddl.Items.Add(CreateEnumListItem(string.Empty, E_SettlementType.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Cash", E_SettlementType.Cash));
            ddl.Items.Add(CreateEnumListItem("GNMAI", E_SettlementType.GNMAI));
            ddl.Items.Add(CreateEnumListItem("GNMAII", E_SettlementType.GNMAII));
            ddl.Items.Add(CreateEnumListItem("FNMA", E_SettlementType.FNMA));
            ddl.Items.Add(CreateEnumListItem("FHLMC", E_SettlementType.FHLMC));
            ddl.Items.Add(CreateEnumListItem("UMBS", E_SettlementType.Umbs));
            ddl.Items.Add(CreateEnumListItem("Other", E_SettlementType.Other));

            ddl.SelectedValue = selectedValue.ToString("D");
        }

        public static void Bind_DeliveryType(DropDownList ddl, E_DeliveryType selectedValue)
        {
            ddl.Items.Add(CreateEnumListItem(string.Empty, E_DeliveryType.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Ginnie Mae Single Lender", E_DeliveryType.GinnieMaeSingleLender));
            ddl.Items.Add(CreateEnumListItem("Ginnie Mae Multiple Lender", E_DeliveryType.GinnieMaeMultipleLender));
            ddl.Items.Add(CreateEnumListItem("Fannie Majors", E_DeliveryType.FannieMajors));
            ddl.Items.Add(CreateEnumListItem("Fannie Mae", E_DeliveryType.FannieMae));
            ddl.Items.Add(CreateEnumListItem("Freddie Mac GoldPC", E_DeliveryType.FreddieMacGoldPC));
            ddl.Items.Add(CreateEnumListItem("Uniform MBS", E_DeliveryType.UniformMbs));
            ddl.Items.Add(CreateEnumListItem("Other", E_DeliveryType.Other));

            ddl.SelectedValue = selectedValue.ToString("D");
        }

        public static void Bind_PoolRoundingType(DropDownList ddl, E_PoolRoundingT val)
        {
            ddl.Items.Add(CreateEnumListItem("", E_PoolRoundingT.LeaveBlank));
            ddl.Items.Add(CreateEnumListItem("Nearest - 0.125", E_PoolRoundingT.Nearest125));
            ddl.Items.Add(CreateEnumListItem("Nearest - 0.25", E_PoolRoundingT.Nearest25));
            ddl.Items.Add(CreateEnumListItem("Up - 0.125", E_PoolRoundingT.Up125));
            ddl.Items.Add(CreateEnumListItem("Up - 0.25", E_PoolRoundingT.Up25));
            ddl.Items.Add(CreateEnumListItem("Down - 0.01", E_PoolRoundingT.Down01));
            ddl.Items.Add(CreateEnumListItem("Down - 0.125", E_PoolRoundingT.Down125));
            ddl.Items.Add(CreateEnumListItem("Down - 0.25", E_PoolRoundingT.Down25));
            ddl.Items.Add(CreateEnumListItem("No Rounding", E_PoolRoundingT.NoRounding));

            ddl.SelectedValue = val.ToString("D");
        }

        public static void Bind_LenderLockPolicy(DropDownList ddl, Guid brokerID)
        {
            var lps = LendersOffice.ObjLib.LockPolicies.LockPolicy.RetrieveAllForBroker(brokerID);
            foreach(var lp in lps)
            {
                ddl.Items.Add(new ListItem(lp.PolicyNm, lp.LockPolicyId.ToString()));
            }
        }

        public static void Bind_PricingPolicyFieldValueSourceForPmlUser(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Originating Company", E_PricingPolicyFieldValueSource.OriginatingCompanyOrBranch));
            ddl.Items.Add(CreateEnumListItem("Individual User", E_PricingPolicyFieldValueSource.IndividualUser));
        }

        public static void Bind_PricingPolicyFieldValueSourceForLqbUser(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("Branch", E_PricingPolicyFieldValueSource.OriginatingCompanyOrBranch));
            ddl.Items.Add(CreateEnumListItem("Individual User", E_PricingPolicyFieldValueSource.IndividualUser));
        }

        public static void Bind_sLenderFeeBuyoutRequestedT(DropDownList ddl)
        {
            ddl.Items.Add(CreateEnumListItem("No", E_sLenderFeeBuyoutRequestedT.No));
            ddl.Items.Add(CreateEnumListItem("Yes", E_sLenderFeeBuyoutRequestedT.Yes));
            ddl.Items.Add(CreateEnumListItem("Waived", E_sLenderFeeBuyoutRequestedT.Waived));
        }

        public static void BindUsePmlUserOCDropdown(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("from Originating Company", EmployeePopulationMethodT.OriginatingCompany));
            ddl.Items.Add(Tools.CreateEnumListItem("from Individual User", EmployeePopulationMethodT.IndividualUser));
        }

        public static void Bind_SelfOwnershipShare(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem(string.Empty, SelfOwnershipShare.Blank));
            ddl.Items.Add(Tools.CreateEnumListItem("Less than 25%", SelfOwnershipShare.LessThan25));
            ddl.Items.Add(Tools.CreateEnumListItem("25% or more", SelfOwnershipShare.GreaterOrEqualTo25));
        }

        public static void Bind_aProdCitizenT(DropDownList ddl, IEnumerable<E_aProdCitizenT> values)
        {
            var friendlyDescriptionByOption = new Dictionary<E_aProdCitizenT, string>
            {
                [E_aProdCitizenT.USCitizen] = "US Citizen",
                [E_aProdCitizenT.PermanentResident] = "Permanent Resident",
                [E_aProdCitizenT.NonpermanentResident] = "Non-permanent Resident",
                [E_aProdCitizenT.ForeignNational] = "Non-Resident Alien (Foreign National)"
            };

            Bind_Generic(
                ddl,
                values,
                option => false,
                option => friendlyDescriptionByOption[option],
                option => option.ToString("D"));

            ddl.Enabled = values.Count() > 1;
        }

        public static int Get_sProMInsMon(int termInMonths, decimal ltvR)
        {
            if (ltvR > 90.0M)
            {
                return Math.Min(30 * 12, termInMonths);
            }
            else
            {
                return Math.Min(11 * 12, termInMonths);
            }
        }

        public static decimal Get_sProMInsR(decimal defaultValue, decimal ltvR, int termInMonths, bool endorsedBeforeJune09)
        {
            if (ltvR <= 78.0M && termInMonths <= 15 * 12 && !endorsedBeforeJune09)
            {
                return 0.45M;
            }
            else
            {
                return defaultValue;
            }
        }

        public static string GetOrigCompPlanDesc(E_sOriginatorCompensationPlanT plan, string appliedBy, string sourceEmployeeName)
        {
            string desc = "Plan ";
            switch (plan)
            {
                case E_sOriginatorCompensationPlanT.Manual:
                    desc += " set manually by " + appliedBy;
                    break;
                case E_sOriginatorCompensationPlanT.FromEmployee:
                    if (string.IsNullOrEmpty(sourceEmployeeName))
                    {
                        // This situation should be RARE
                        desc += " set from the employee record, ";
                    }
                    else
                    {
                        desc += " set from the employee record of " + sourceEmployeeName;
                    }
                    break;
                default:
                    throw new UnhandledEnumException(plan);
            }

            return desc;
        }

        public static DateTime GetDateTimeForFromD(E_FromDateT from)
        {
            DateTime today = DateTime.Today;
            int quarterBeginMonth = 3 * ((today.Month - 1) / 3) + 1;
            DateTime dt;

            switch (from)
            {
                case E_FromDateT.Today:
                    return today;
                case E_FromDateT.Yesterday:
                    return today.AddDays(-1);
                case E_FromDateT.This_Monday:
                    dt = today;
                    while (dt.DayOfWeek != DayOfWeek.Monday)
                    {
                        dt = dt.AddDays(-1);
                    }
                    return dt;
                case E_FromDateT.Last_Monday:
                    dt = today.AddDays(-7);
                    while (dt.DayOfWeek != DayOfWeek.Monday)
                    {
                        dt = dt.AddDays(-1);
                    }
                    return dt;
                case E_FromDateT.Next_Monday:
                    dt = today.AddDays(1);
                    while (dt.DayOfWeek != DayOfWeek.Monday)
                    {
                        dt = dt.AddDays(1);
                    }
                    return dt;
                case E_FromDateT.Beginning_This_Month:
                    dt = today;
                    while (dt.Day != 1)
                    {
                        dt = dt.AddDays(-1);
                    }
                    return dt;
                case E_FromDateT.Beginning_Last_Month:
                    dt = today.AddMonths(-1);
                    while (dt.Day != 1)
                    {
                        dt = dt.AddDays(-1);
                    }
                    return dt;
                case E_FromDateT.Beginning_Next_Month:
                    dt = today.AddMonths(1);
                    while (dt.Day != 1)
                    {
                        dt = dt.AddDays(-1);
                    }
                    return dt;
                case E_FromDateT.Start_Current_Calendar_Quarter:
                    dt = today;
                    return new DateTime(dt.Year, quarterBeginMonth, 1);
                case E_FromDateT.Start_Last_Calendar_Quarter:
                    dt = today;
                    return new DateTime(dt.Year, quarterBeginMonth, 1).AddMonths(-3);
                case E_FromDateT.Beginning_This_Year:
                    dt = today;
                    while (dt.Month != 1)
                    {
                        dt = dt.AddMonths(-1);
                    }
                    while (dt.Day != 1)
                    {
                        dt = dt.AddDays(-1);
                    }
                    return dt;
                case E_FromDateT.Beginning_Last_Year:
                    dt = today;
                    while (dt.Month != 1)
                    {
                        dt = dt.AddMonths(-1);
                    }
                    while (dt.Day != 1)
                    {
                        dt = dt.AddDays(-1);
                    }
                    return dt.AddYears(-1);
                default:
                    throw new UnhandledEnumException(from);
            }
        }

        public static DateTime GetDateTimeForToD(E_ToDateT to)
        {
            DateTime today = DateTime.Today;
            int quarterBeginMonth = 3 * ((today.Month - 1) / 3) + 1;
            DateTime dt;

            switch (to)
            {
                case E_ToDateT.Today:
                    return today;
                case E_ToDateT.Yesterday:
                    return today.AddDays(-1);
                case E_ToDateT.This_Friday:
                    dt = today;
                    while (dt.DayOfWeek != DayOfWeek.Friday)
                    {
                        dt = dt.AddDays(1);
                    }
                    return dt;
                case E_ToDateT.Last_Friday:
                    dt = today.AddDays(-1);
                    while (dt.DayOfWeek != DayOfWeek.Friday)
                    {
                        dt = dt.AddDays(-1);
                    }
                    return dt;
                case E_ToDateT.Next_Friday:
                    dt = today.AddDays(7);
                    while (dt.DayOfWeek != DayOfWeek.Friday)
                    {
                        dt = dt.AddDays(1);
                    }
                    return dt;
                case E_ToDateT.End_This_Month:
                    dt = today.AddMonths(1);
                    while (dt.Day != 1)
                    {
                        dt = dt.AddDays(-1);
                    }
                    return dt.AddDays(-1);
                case E_ToDateT.End_Last_Month:
                    dt = today;
                    while (dt.Day != 1)
                    {
                        dt = dt.AddDays(-1);
                    }
                    return dt.AddDays(-1);
                case E_ToDateT.End_Next_Month:
                    dt = today.AddMonths(2);
                    while (dt.Day != 1)
                    {
                        dt = dt.AddDays(-1);
                    }
                    return dt.AddDays(-1);
                case E_ToDateT.End_Current_Calendar_Quarter:
                    dt = today;
                    return new DateTime(dt.Year, quarterBeginMonth, 1).AddMonths(3).AddDays(-1);
                case E_ToDateT.End_Last_Calendar_Quarter:
                    dt = today;
                    return new DateTime(dt.Year, quarterBeginMonth, 1).AddDays(-1);
                case E_ToDateT.End_This_Year:
                    dt = today.AddMonths(1);
                    while (dt.Month != 1)
                    {
                        dt = dt.AddMonths(1);
                    }
                    while (dt.Day != 1)
                    {
                        dt = dt.AddDays(-1);
                    }
                    return dt.AddDays(-1);
                case E_ToDateT.End_Last_Year:
                    dt = today;
                    while (dt.Month != 1)
                    {
                        dt = dt.AddMonths(-1);
                    }
                    while (dt.Day != 1)
                    {
                        dt = dt.AddDays(-1);
                    }
                    return dt.AddDays(-1);
                default:
                    throw new UnhandledEnumException(to);
            }
        }

        public static bool IsNonAuthorProduction
        {
            get
            { // OPM 107445
                // Certain fields/options will need to be hidden on production until working correctly.
                // This is a temporary hack for determining if the code is being executed on author.
                bool isLoAuthor = ConstSite.DisplayPolicyAndRuleIdOnLpeResult;
                bool isProduction = !isLoAuthor && (ConstAppDavid.CurrentServerLocation == ServerLocation.Production);
                return isProduction;
            }
        }



        /// <summary>
        /// Returns true iff the request is on loauth domain. <para></para>
        /// Note that we often really want to know just does the server host user traffic.  <para></para>
        /// This is probably less hacky then Tools.IsNonLoauthProduction, but still not quite ideal.
        /// </summary>
        /// <returns>True if the domain is determined to be loauth, false , or null if no request.</returns>
        public static bool? RequestIsOnLoauth()
        {
            if (HttpContext.Current == null)
            {
                return null;
            }

            string loauthPattern = ConstStage.JustinLoauthUrlPatternChecking;
            string[] patterns = (loauthPattern == string.Empty) ? new string[] { } : loauthPattern.Split(';');
            bool isLoauthPattern = false;
            string url = HttpContext.Current.Request.Url.Host.ToLower();
            foreach (string pattern in patterns)
            {
                if (url.StartsWith(pattern))
                {
                    isLoauthPattern = true;
                }
            }

            if (isLoauthPattern || HttpContext.Current.Request.Url.Host.ToLower().StartsWith("loauth"))
            {
                return true;
            }

            return false;
        }

        public static bool IsQP2LoanFile(Guid loanId, Guid brokerId)
        {
            try
            {
                var fileType = GetLoanFileType(brokerId, loanId);
                return fileType == E_sLoanFileT.QuickPricer2Sandboxed;
            }
            catch(NotFoundException)
            {
                return false;
            }
        }

        public static IEnumerable<SimpleSandboxLoanItem> ListSandboxLoans(Guid brokerId, Guid sSrcsLId)
        {
            List<SimpleSandboxLoanItem> list = new List<SimpleSandboxLoanItem>();

            SqlParameter[] parameters = {
                                            new SqlParameter("@sBrokerId", brokerId),
                                            new SqlParameter("@sSrcsLId", sSrcsLId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "ListSandboxLoans", parameters))
            {
                while (reader.Read())
                {
                    SimpleSandboxLoanItem item = new SimpleSandboxLoanItem();
                    item.sLId = (Guid)reader["sLId"];
                    item.sCreatedD = (DateTime)reader["sCreatedD"];
                    item.sLNm = (string)reader["sLNm"];

                    list.Add(item);
                }
            }
            return list;
        }

        public static bool CanUserUseQP2(BrokerDB theirBroker, bool haveQPAccess, bool haveNewPMLAccess, bool havePml2AsQpChecked, string userType)
        {
            // opm 185732
            return ReasonsUserIsntUsingQP2Impl(theirBroker, haveQPAccess, haveNewPMLAccess, havePml2AsQpChecked, userType).Count() == 0;
        }
        public static bool IsQP2UserCheckboxEnabled(BrokerDB theirBroker, bool haveQPAccess, bool haveNewPMLAccess, bool havePml2AsQpChecked, string userType)
        {
            // opm 185732
            var reasons = ReasonsUserIsntUsingQP2Impl(theirBroker, haveQPAccess, haveNewPMLAccess, havePml2AsQpChecked, userType).Except(
                new E_ReasonCantUseQP2[]{E_ReasonCantUseQP2.PerUserBitDisabled});
            return reasons.Count() == 0;
        }
        private static IEnumerable<E_ReasonCantUseQP2> ReasonsUserIsntUsingQP2Impl(BrokerDB theirBroker, bool haveQPAccess, bool haveNewPMLAccess, bool havePml2AsQpChecked, string userType)
        {
            // opm 185732
            var reasons = new List<E_ReasonCantUseQP2>();
            if (false == (haveNewPMLAccess || theirBroker.IsNewPmlUIEnabled))
            {
                reasons.Add(E_ReasonCantUseQP2.LackNewPML);
            }
            if (false == (haveQPAccess || theirBroker.IsQuickPricerEnable))
            {
                reasons.Add(E_ReasonCantUseQP2.LackQP);
            }
            if ("P" == userType && false == ConstStage.AllowQuickPricer2InTPOPortal)
            {
                reasons.Add(E_ReasonCantUseQP2.NotEnabledForTPOUsersInStageConfig);
            }
            switch (theirBroker.Pml2AsQuickPricerMode)
            {
                case E_Pml2AsQuickPricerMode.Disabled:
                    reasons.Add(E_ReasonCantUseQP2.BrokerDisabled);
                    break;
                case E_Pml2AsQuickPricerMode.PerUser:
                    if(false == havePml2AsQpChecked)
                    {
                        reasons.Add(E_ReasonCantUseQP2.PerUserBitDisabled);
                    }
                    break;
                case E_Pml2AsQuickPricerMode.Enabled:
                    break;
                default:
                    throw new UnhandledEnumException(theirBroker.Pml2AsQuickPricerMode);
            }
            return reasons;
        }
        private enum E_ReasonCantUseQP2
        {
            LackNewPML = 0,
            LackQP = 1,
            BrokerDisabled = 2,
            PerUserBitDisabled = 3,
            NotEnabledForTPOUsersInStageConfig = 4
        }
        private static Dictionary<E_ReasonCantUseQP2, string> s_friendlyQP2ReasonByReasonT = new Dictionary<E_ReasonCantUseQP2,string>()
        {
            {E_ReasonCantUseQP2.LackQP, "They do not have quickpricer enabled at either the user or the broker level."},
            {E_ReasonCantUseQP2.LackNewPML, "They do not have new pml enabled at either the user or the broker level."},
            {E_ReasonCantUseQP2.BrokerDisabled, "The broker has Pml2AsQuickPricerMode set to Disabled."},
            {E_ReasonCantUseQP2.PerUserBitDisabled, "The broker has Pml2AsQuickPricerMode set to PerUser but the feature is not enabled at the user level."},
            {E_ReasonCantUseQP2.NotEnabledForTPOUsersInStageConfig, "QuickPricer 2.0 is not enabled for Originator Portal users in stage config. (AllowQuickPricer2InTPOPortal)."}
        };
        public static IEnumerable<string> ReasonsUserIsntUsingQP2(BrokerDB theirBroker, bool haveQPAccess, bool haveNewPMLAccess, bool havePml2AsQpChecked, string userType)
        {
            // opm 185732
            foreach (var reason in ReasonsUserIsntUsingQP2Impl(theirBroker, haveQPAccess, haveNewPMLAccess, havePml2AsQpChecked, userType))
            {
                yield return s_friendlyQP2ReasonByReasonT[reason];
            }
        }

        /// <summary>
        /// Do not call this if the loan is a dummy / quickpricer2sandboxed loan.<para></para>
        /// Extracted from LoanFileCreation.cs <para></para>
        /// OPM 185732. <para></para>
        /// </summary>
        public static void CreateLoanCreationAudit(AbstractUserPrincipal principal, Guid sourceFileID, Guid createdFileID, E_LoanCreationSource creationSource, bool isLead)
        {
            string templateName = "";
            if (sourceFileID != Guid.Empty)
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@LoanID", sourceFileID)
                                            };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(principal.BrokerId, "GetLoanNumberByLoanID", parameters))
                {
                    if (reader.Read())
                    {
                        templateName = (string)reader["sLNm"];
                    }
                }
            }

            string auditDescription;
            if (isLead)
            {
                auditDescription = "Lead Creation";
            }
            else
            {
                auditDescription = "Loan Creation";
            }

            LendersOffice.Audit.AbstractAuditItem audit = new LendersOffice.Audit.LoanCreationAuditItem(principal, creationSource, templateName, auditDescription);
            LendersOffice.Audit.AuditManager.RecordAudit(createdFileID, audit);
        }

        /// <summary>
        /// Stolen from RunEmbeddedPml logic.  Now used in RunEmbeddedQuickpricer too for users with quickpricer 2 setup. <para></para>
        /// TODO: cache the result per session if possible.
        /// </summary>
        /// <returns>The url of the page to redirect to.</returns>
        public static string GetEmbeddedPmlUrl(BrokerUserPrincipal brokerUser, Guid loanID, bool isLead,
            ref bool isLoanTypeSupported, ref bool doBreakout)
        {
            var url = "QualifiedNotAvailable.aspx";
            var brokerID = brokerUser.BrokerId;
            var employeeID = brokerUser.EmployeeId;
            var virtualRoot = VRoot;

            CPageData dataLoan = new CCheckLpeRunModeData(loanID);
            dataLoan.AllowLoadWhileQP2Sandboxed = true;
            dataLoan.InitLoad();


            E_LpeRunModeT lpeRunModeT = dataLoan.lpeRunModeT;

            string oldEngineDebugInfo, newEngineDebugInfo;

            LoanValueEvaluator evaluator = new LoanValueEvaluator(brokerUser.ConnectionInfo, brokerUser.BrokerId, loanID, WorkflowOperations.UseOldSecurityModelForPml, WorkflowOperations.RunPmlForAllLoans);
            evaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(brokerUser));
            bool useOldEngine = LendingQBExecutingEngine.CanPerform(WorkflowOperations.UseOldSecurityModelForPml, evaluator);


            if (useOldEngine)
            {
                string oldEngineUrl = GetOldEngineRedirectUrl(loanID, brokerID, employeeID, lpeRunModeT, virtualRoot, isLead,
                    ref doBreakout, out oldEngineDebugInfo);
                url = oldEngineUrl;
            }
            else
            {
                // Other is not supported by the new UI
                switch (dataLoan.sLPurposeT)
                {
                    case E_sLPurposeT.Other:
                        isLoanTypeSupported = false;
                        break;
                    default:
                        string newEngineUrl = GetNewEngineRedirectUrl(brokerUser, loanID, brokerID, employeeID, virtualRoot, isLead,
                            ref doBreakout, out newEngineDebugInfo);
                        url = newEngineUrl;
                        break;
                }
            }

            if (dataLoan.sLoanFileT == E_sLoanFileT.QuickPricer2Sandboxed)
            {
                doBreakout = false;  // because then it was linked to directly without an outer frame.
            }
            return url;
        }

        private static string GetOldEngineRedirectUrl(Guid loanID, Guid brokerID, Guid employeeID,
            E_LpeRunModeT lpeRunModeT,
            string virtualRoot,
            bool isLead,
            ref bool doBreakout,
            out string debugInfo)
        {
            debugInfo = "lpeRunModeT: " + lpeRunModeT;
            switch (lpeRunModeT)
            {
                case E_LpeRunModeT.NotAllowed:
                    return "QualifiedNotAvailable.aspx?loanid=" + loanID;
                case E_LpeRunModeT.Full:
                case E_LpeRunModeT.OriginalProductOnly_Direct:
                    string url = virtualRoot + "/embeddedpml/main/agents.aspx?loanid=" + loanID;
                    doBreakout = false;

                    BrokerDB broker = BrokerDB.RetrieveById(brokerID);
                    bool isNewPmlUIEnabled = false;
                    if (broker.IsNewPmlUIEnabled)
                    {
                        isNewPmlUIEnabled = true;
                    }
                    else
                    {
                        EmployeeDB employee = EmployeeDB.RetrieveById(brokerID, employeeID);
                        if (employee.IsNewPmlUIEnabled)
                        {
                            isNewPmlUIEnabled = true;
                        }
                    }

                    if (isNewPmlUIEnabled)
                    {
                        url = virtualRoot + string.Format("/embeddedpml/webapp/pml.aspx?loanid={0}&source={1}", loanID, "LO");
                        if (isLead)
                        {
                            url += "&isLead=t";
                        }
                        doBreakout = true;
                    }
                    return url;
                default:
                    throw new UnhandledEnumException(lpeRunModeT);

            }
        }

        private static string GetNewEngineRedirectUrl(BrokerUserPrincipal brokerUser, Guid loanID, Guid brokerID, Guid employeeID,
            string virtualRoot,
            bool isLead,
            ref bool doBreakout,
            out string debugInfo)
        {
            LoanValueEvaluator evaluator = new LoanValueEvaluator(brokerUser.ConnectionInfo, brokerUser.BrokerId, loanID, WorkflowOperations.RunPmlForAllLoans, WorkflowOperations.RunPmlForRegisteredLoans, WorkflowOperations.RunPmlToStep3);
            evaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(brokerUser));

            bool canRunToStep3 = LendingQBExecutingEngine.CanPerform(WorkflowOperations.RunPmlToStep3, evaluator);
            bool canRunAll = LendingQBExecutingEngine.CanPerform(WorkflowOperations.RunPmlForAllLoans, evaluator);
            bool canRunOrig = LendingQBExecutingEngine.CanPerform(WorkflowOperations.RunPmlForRegisteredLoans, evaluator);

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("canRunToStep3: " + canRunToStep3);
            sb.AppendLine("canRunAll: " + canRunAll);
            sb.AppendLine("canRunOrig: " + canRunOrig);

            debugInfo = sb.ToString();

            string url = "QualifiedNotAvailable.aspx?loanid=" + loanID;

            if (canRunToStep3 || canRunAll || canRunOrig)
            {
                url = virtualRoot + "/embeddedpml/main/agents.aspx?loanid=" + loanID;
                doBreakout = false;

                BrokerDB broker = BrokerDB.RetrieveById(brokerID);
                bool isNewPmlUIEnabled = false;
                if (broker.IsNewPmlUIEnabled)
                {
                    isNewPmlUIEnabled = true;
                }
                else
                {
                    EmployeeDB employee = EmployeeDB.RetrieveById(brokerID, employeeID);
                    if (employee.IsNewPmlUIEnabled)
                    {
                        isNewPmlUIEnabled = true;
                    }
                }

                if (isNewPmlUIEnabled)
                {
                    url = virtualRoot + string.Format("/embeddedpml/webapp/pml.aspx?loanid={0}&source={1}", loanID, "LO");
                    if (isLead)
                    {
                        url += "&isLead=t";
                    }
                    doBreakout = true;
                }
            }

            return url;
        }

        public static CRA GetCraById(Guid craId, Guid brokerId)
        {
            if (craId == Guid.Empty)
            {
                throw new CBaseException("Require a valid credit report agency.", "comId is Guid.Empty");
            }

            CRA cra = MasterCRAList.FindById(craId, false);

            if (null == cra)
            {
                var proxy = CreditReportAccountProxy.Helper.RetrieveCreditProxyByProxyID(brokerId, craId);
                if (proxy != null)
                {
                    craId = proxy.CraId;
                }

                cra = MasterCRAList.FindById(craId, false);
                if (null == cra)
                {
                    throw new CBaseException("Require a valid credit report agency.", "comId cannot be found");
                }
            }

            return cra;
        }

        /// <summary>
        /// OrderCredit permission requires the use of a particular parameter.
        /// Use this method to ensure it is added in.
        /// </summary>
        public static bool CanOrderCredit(AbstractUserPrincipal principal, Guid loanId, CreditReportProtocol protocol, out string reasons)
        {
            var nonLoanValueEvaluator = new NonLoanValueEvaluator();
            nonLoanValueEvaluator.Add("OrderCredit_CreditReportProtocol", (int)protocol);

            var valueEvaluator = new LoanValueEvaluator(principal.BrokerId, loanId, WorkflowOperations.OrderCredit);
            valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(principal));
            valueEvaluator.SetNonLoanValues(nonLoanValueEvaluator);

            var canOrderCredit = LendingQBExecutingEngine.CanPerform(WorkflowOperations.OrderCredit, valueEvaluator);
            reasons = (!canOrderCredit) ?
                LendingQBExecutingEngine.GetUserFriendlyMessage(WorkflowOperations.OrderCredit, valueEvaluator)
                : string.Empty;

            return canOrderCredit;
        }

        /// <summary>
        /// OrderCredit permission requires the use of a particular parameter.
        /// Use this method to ensure it is added in.
        /// </summary>
        public static bool CanOrderCredit(AbstractUserPrincipal principal, Guid loanId, Guid craId, out string reasons)
        {
            var protocol = GetCraById(craId, principal.BrokerId).Protocol;
            return CanOrderCredit(principal, loanId, protocol, out reasons);
        }

        /// <summary>
        /// Transforms LO data to PML data. 
        /// Does run for quickpricer 2.0 loans.
        /// </summary>
        /// <param name="pageIsPostback">Should be true if page is nonexistant / calling from a library.</param>
        /// <returns></returns>
        public static int TransformData(Guid loanID, bool initNewLoanForPml, bool pageIsPostback, E_LpeRunModeT lpeRunModeT, bool bypassFieldSecurity)
        {
            CPageData dataLoan = new CPmlLoanSummary_TransformData(loanID);
            if (bypassFieldSecurity)
            {
                dataLoan.ByPassFieldSecurityCheck = bypassFieldSecurity;
            }
            dataLoan.AllowSaveWhileQP2Sandboxed = true;
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            if (false == pageIsPostback && E_sLoanFileT.QuickPricer2Sandboxed == dataLoan.sLoanFileT) // skip this for qp2.0 loans. 185732
            {
                return dataLoan.sFileVersion;
            }
            dataLoan.CalcModeT = E_CalcModeT.LendersOfficePageEditing;
            if (lpeRunModeT == E_LpeRunModeT.OriginalProductOnly_Direct)
            {
                dataLoan.TransformDataToPml(E_TransformToPmlT.RerunForRateLockRequest);
            }
            else
            {
                dataLoan.TransformDataToPml(E_TransformToPmlT.FromScratch);
            }
            if (initNewLoanForPml)
            {
                dataLoan.InitNewLoanForPml();
            }

            dataLoan.RecalculateClosingFeeConditions(false);

            if (dataLoan.sLoanFileT == E_sLoanFileT.QuickPricer2Sandboxed)
            {
                dataLoan.sFeeServiceApplicationHistoryXmlContent = string.Empty;
            }

            dataLoan.Save();
            return dataLoan.sFileVersion;
        }

        /// <summary>
        /// Gets the FileDBEntryInfo associated with the file db key type for the specified unique id.
        /// </summary>
        /// <remarks>
        ///  If you add it here, please consider updating SpecificallyLoanFileDBKeyTypes.
        /// </remarks>
        /// <param name="keyType">The type of file stored.</param>
        /// <param name="loanID">Typically this is the loan id.</param>
        /// <returns></returns>
        public static FileDBEntryInfo GetFileDBEntryInfoForLoan(E_FileDBKeyType keyType, Guid loanID)
        {
            var loanIDInNFormat = loanID.ToString("N");

            switch (keyType)
            {
                case E_FileDBKeyType.Normal_Loan: // has case E_FileDBKeyType.aLiaXmlContent: case E_FileDBKeyType.aAssetXmlContent: and sCondXmlContent sections based on those keys.
                    return new FileDBEntryInfo(E_FileDB.Normal, loanIDInNFormat);
                case E_FileDBKeyType.Normal_Loan_approval_pdf:
                    return new FileDBEntryInfo(E_FileDB.Normal, loanIDInNFormat + "_approval.pdf");
                case E_FileDBKeyType.Normal_Loan_COMPLIANCE_EASE_REPORT_PDF:
                    return new FileDBEntryInfo(E_FileDB.Normal, loanIDInNFormat + "_COMPLIANCE_EASE_REPORT_PDF");
                case E_FileDBKeyType.Normal_Loan_FNMA_DWEB_CASEFILEIMPORT:
                    return new FileDBEntryInfo(E_FileDB.Normal, loanIDInNFormat + "_FNMA_DWEB_CASEFILEIMPORT");
                case E_FileDBKeyType.Normal_Loan_FNMA_EARLYCHECK:
                    return new FileDBEntryInfo(E_FileDB.Normal, loanIDInNFormat + "_FNMA_EARLYCHECK");
                case E_FileDBKeyType.Normal_Loan_FNMA_GETCREDITAGENCIES:
                    return new FileDBEntryInfo(E_FileDB.Normal, loanIDInNFormat + "_FNMA_GETCREDITAGENCIES");
                case E_FileDBKeyType.Normal_Loan_GLOBAL_DMS_INFO_XML:
                    return new FileDBEntryInfo(E_FileDB.Normal, loanIDInNFormat + "_GLOBAL_DMS_INFO_XML");
                case E_FileDBKeyType.Normal_Loan_suspense_pdf:
                    return new FileDBEntryInfo(E_FileDB.Normal, loanIDInNFormat + "_suspense.pdf");
                case E_FileDBKeyType.Normal_Loan_ratelock_pdf:
                    return new FileDBEntryInfo(E_FileDB.Normal, loanIDInNFormat + "_ratelock.pdf");
                case E_FileDBKeyType.Normal_Loan_pmlsummary_pdf:
                    return new FileDBEntryInfo(E_FileDB.Normal, loanIDInNFormat + "_pmlsummary.pdf");
                case E_FileDBKeyType.Normal_Loan_audit:
                    return new FileDBEntryInfo(E_FileDB.Normal, "audit_" + loanID);
                case E_FileDBKeyType.Normal_Loan_CLOSING_COST_MIGRATION_ARCHIVE:
                    return new FileDBEntryInfo(E_FileDB.Normal, "CLOSING_COST_MIGRATION_ARCHIVE_" + loanID);
                case E_FileDBKeyType.Normal_Loan_coc_archive:
                    return new FileDBEntryInfo(E_FileDB.Normal, "coc_archive_" + loanID);
                case E_FileDBKeyType.Normal_Loan_COMPLIANCE_EAGLE_REPORT_PDF:
                    return new FileDBEntryInfo(E_FileDB.Normal, loanIDInNFormat + "_COMPLIANCE_EAGLE_REPORT_PDF"); // from ConstAppDavid.ComplianceEagle_Report_Tag;
                case E_FileDBKeyType.Normal_Loan_DU_FINDINGS_HTML:
                    return new FileDBEntryInfo(E_FileDB.Normal, "DU_FINDINGS_HTML_" + loanIDInNFormat);
                case E_FileDBKeyType.Normal_Loan_FNMA_CASEFILE_EXPORT:
                    return new FileDBEntryInfo(E_FileDB.Normal, "FNMA_CASEFILE_EXPORT_" + loanIDInNFormat);
                case E_FileDBKeyType.Normal_Loan_FNMA_CASEFILE_IMPORT:
                    return new FileDBEntryInfo(E_FileDB.Normal, "FNMA_CASEFILE_IMPORT_" + loanIDInNFormat);
                case E_FileDBKeyType.Normal_Loan_FNMA_DU_UNDERWRITE:
                    return new FileDBEntryInfo(E_FileDB.Normal, "FNMA_DU_UNDERWRITE_" + loanIDInNFormat);
                case E_FileDBKeyType.Normal_Loan_FNMA_DWEB_CASEFILESTATUS:
                    return new FileDBEntryInfo(E_FileDB.Normal, "FNMA_DWEB_CASEFILESTATUS_" + loanIDInNFormat);
                case E_FileDBKeyType.Normal_Loan_FREDDIE_XML:
                    return new FileDBEntryInfo(E_FileDB.Normal, "FREDDIE_XML_" + loanIDInNFormat);
                case E_FileDBKeyType.Normal_Loan_gfe_archive:
                    return new FileDBEntryInfo(E_FileDB.Normal, "gfe_archive_" + loanID);
                case E_FileDBKeyType.Normal_Loan_pmlactivity:
                    return new FileDBEntryInfo(E_FileDB.Normal, "pmlactivity_" + loanID);
                case E_FileDBKeyType.Normal_Loan_PricingStateXml:
                    return new FileDBEntryInfo(E_FileDB.Normal, loanID + "_PricingStateXml");
                case E_FileDBKeyType.Temp_Loan_backup_audit:
                    return new FileDBEntryInfo(E_FileDB.Temp, "backup_audit_" + loanID);
                case E_FileDBKeyType.Temp_Loan_COMPLIANCE_EAGLE_REPORT_PDF:
                    return new FileDBEntryInfo(E_FileDB.Temp, loanIDInNFormat + "_COMPLIANCE_EAGLE_REPORT_PDF"); // from ConstAppDavid.ComplianceEagle_Report_Tag;
                case E_FileDBKeyType.Temp_Loan_LPEFirstResult:
                    return new FileDBEntryInfo(E_FileDB.Temp, "LPEFirstResult_" + loanIDInNFormat);
                case E_FileDBKeyType.Temp_Loan_LPESecondResult:
                    return new FileDBEntryInfo(E_FileDB.Temp, "LPESecondResult_" + loanIDInNFormat);
                case E_FileDBKeyType.Normal_Loan_AutoDisclosureAuditHtml:
                    return new FileDBEntryInfo(E_FileDB.Normal, "AUTO_DISCLOSURE_AUDIT_HTML_" + loanID);
                case E_FileDBKeyType.Normal_Loan_sAgentXmlContent:
                    return new FileDBEntryInfo(E_FileDB.Normal, loanIDInNFormat + "_sAgentXmlContent");
                case E_FileDBKeyType.Normal_Loan_sPmlCertXmlContent:
                    return new FileDBEntryInfo(E_FileDB.Normal, loanIDInNFormat + "_sPmlCertXmlContent");
                case E_FileDBKeyType.Normal_Loan_sClosingCostSetJsonContent:
                    return new FileDBEntryInfo(E_FileDB.Normal, loanIDInNFormat + "_sClosingCostSetJsonContent");
                case E_FileDBKeyType.Normal_Loan_sTpoRequestForInitialDisclosureGenerationEventsJsonContent:
                    return new FileDBEntryInfo(E_FileDB.Normal, loanIDInNFormat + "_sTPORequestForInitialDisclosureGenerationEventsJsonContent");
                case E_FileDBKeyType.Normal_ClosingCostArchive:
                    throw new ArgumentException("Normal_ClosingCostArchive is not associated with a loan, but rather an archive.");
                case E_FileDBKeyType.Normal_LpePriceGroup_Content:
                    throw new ArgumentException("Normal_LpePriceGroup_Content is not associated with a loan, but rather a lpe price group.");
                case E_FileDBKeyType.Normal_CustomNonPdf_Form:
                    throw new ArgumentException("Normal_CustomNonPdf_Form is not associated with a loan, but rather a lender level custom document.");
                case E_FileDBKeyType.Edms_CustomPdf_Form:
                    throw new ArgumentException("Normal_CustomPdfForm is not associated with a loan, but rather a lender level custom pdf form.");
                case E_FileDBKeyType.Normal_FeeServiceRevision_File:
                    throw new ArgumentException("Normal_Broker_FeeServiceRevision is not associated with a loan, but rather a fee service file for a lender.");
                case E_FileDBKeyType.Normal_Loan_MigrationAuditHistoryKey:
                    throw new ArgumentException("Normal_Loan_MigrationAuditHistoryKey needs to come from loanid + sLoanDataMigrationAuditHistoryKey.");
                case E_FileDBKeyType.Normal_LenderClientCertificate_Notes:
                    throw new ArgumentException("Normal_LenderClientCertificate_Notes is per lender per certificate, not per loan.");
                case E_FileDBKeyType.Normal_Loan_sRawSelectedProductCodeFilterKey:
                    throw new ArgumentException("Normal_Loan_sRawSelectedProductCodeFilterKey needs to come from loanId + sRawSelectedProductCodeFilterKey");
                case E_FileDBKeyType.Normal_Loan_sDuThirdPartyProvidersJsonContent:
                    return new FileDBEntryInfo(E_FileDB.Normal, loanIDInNFormat + "_sDuThirdPartyProviders");
                case E_FileDBKeyType.Normal_Loan_sTpoRequestForRedisclosureGenerationEventsJsonContent:
                    return new FileDBEntryInfo(E_FileDB.Normal, loanIDInNFormat + "_sTpoRequestForRedisclosureGenerationEventsJsonContent");
                case E_FileDBKeyType.Normal_Loan_sTpoRequestForInitialClosingDisclosureGenerationEventsJsonContent:
                    return new FileDBEntryInfo(E_FileDB.Normal, loanIDInNFormat + "_sTpoRequestForInitialClosingDisclosureGenerationEventsJsonContent");
                case E_FileDBKeyType.Normal_Loan_sFHACaseQueryResultXmlContent:
                    return new FileDBEntryInfo(E_FileDB.Normal, loanIDInNFormat + "_sFHACaseQueryResultXmlContent");
                case E_FileDBKeyType.Normal_Loan_sFHACaseNumberResultXmlContent:
                    return new FileDBEntryInfo(E_FileDB.Normal, loanIDInNFormat + "_sFHACaseNumberResultXmlContent");
                case E_FileDBKeyType.Normal_Loan_sFHACAVIRSResultXmlContent:
                    return new FileDBEntryInfo(E_FileDB.Normal, loanIDInNFormat + "_sFHACAVIRSResultXmlContent");
                case E_FileDBKeyType.Normal_Loan_sProdPmlDataUsedForLastPricingXmlContent:
                    return new FileDBEntryInfo(E_FileDB.Normal, loanIDInNFormat + "_sProdPmlDataUsedForLastPricingXmlContent");
                case E_FileDBKeyType.Normal_Loan_sTotalScoreCertificateXmlContent:
                    return new FileDBEntryInfo(E_FileDB.Normal, loanIDInNFormat + "_sTotalScoreCertificateXmlContent");
                case E_FileDBKeyType.Normal_Loan_sTotalScoreTempCertificateXmlContent:
                    return new FileDBEntryInfo(E_FileDB.Normal, loanIDInNFormat + "_sTotalScoreTempCertificateXmlContent");
                case E_FileDBKeyType.Normal_Loan_sPreparerXmlContent:
                    return new FileDBEntryInfo(E_FileDB.Normal, loanIDInNFormat + "_sPreparerXmlContent");
                default:
                    throw new UnhandledEnumException(keyType);
            }
        }

        /// <summary>
        /// Contains only keys specifically tied to one loan object.  <para></para>
        /// Contrast with a closing cost archive which has its own filedb key type, but there can be many archives within a loan, so that filedbkeytype is not included in this enumeration.
        /// </summary>
        public static IEnumerable<E_FileDBKeyType> SpecificallyLoanFileDBKeyTypes = new System.Collections.ObjectModel.ReadOnlyCollection<E_FileDBKeyType>(new E_FileDBKeyType[]
        {
            E_FileDBKeyType.Normal_Loan,
            E_FileDBKeyType.Normal_Loan_approval_pdf,
            E_FileDBKeyType.Normal_Loan_audit,
            E_FileDBKeyType.Normal_Loan_AutoDisclosureAuditHtml,
            E_FileDBKeyType.Normal_Loan_CLOSING_COST_MIGRATION_ARCHIVE,
            E_FileDBKeyType.Normal_Loan_coc_archive,
            E_FileDBKeyType.Normal_Loan_COMPLIANCE_EAGLE_REPORT_PDF,
            E_FileDBKeyType.Normal_Loan_COMPLIANCE_EASE_REPORT_PDF,
            E_FileDBKeyType.Normal_Loan_DU_FINDINGS_HTML,
            E_FileDBKeyType.Normal_Loan_FNMA_CASEFILE_EXPORT,
            E_FileDBKeyType.Normal_Loan_FNMA_CASEFILE_IMPORT,
            E_FileDBKeyType.Normal_Loan_FNMA_DU_UNDERWRITE,
            E_FileDBKeyType.Normal_Loan_FNMA_DWEB_CASEFILEIMPORT,
            E_FileDBKeyType.Normal_Loan_FNMA_DWEB_CASEFILESTATUS,
            E_FileDBKeyType.Normal_Loan_FNMA_EARLYCHECK,
            E_FileDBKeyType.Normal_Loan_FNMA_GETCREDITAGENCIES,
            E_FileDBKeyType.Normal_Loan_FREDDIE_XML,
            E_FileDBKeyType.Normal_Loan_GLOBAL_DMS_INFO_XML,
            E_FileDBKeyType.Normal_Loan_gfe_archive,
            E_FileDBKeyType.Normal_Loan_pmlactivity,
            E_FileDBKeyType.Normal_Loan_pmlsummary_pdf,
            E_FileDBKeyType.Normal_Loan_PricingStateXml,
            E_FileDBKeyType.Normal_Loan_ratelock_pdf,
            E_FileDBKeyType.Normal_Loan_sAgentXmlContent,
            E_FileDBKeyType.Normal_Loan_sClosingCostSetJsonContent,
            E_FileDBKeyType.Normal_Loan_sPmlCertXmlContent,
            E_FileDBKeyType.Normal_Loan_suspense_pdf,
            E_FileDBKeyType.Normal_Loan_sTpoRequestForInitialDisclosureGenerationEventsJsonContent,
            E_FileDBKeyType.Temp_Loan_backup_audit,
            E_FileDBKeyType.Temp_Loan_COMPLIANCE_EAGLE_REPORT_PDF,
            E_FileDBKeyType.Temp_Loan_LPEFirstResult,
            E_FileDBKeyType.Temp_Loan_LPESecondResult,
            E_FileDBKeyType.Normal_Loan_sTpoRequestForRedisclosureGenerationEventsJsonContent,
            E_FileDBKeyType.Normal_Loan_sTpoRequestForInitialClosingDisclosureGenerationEventsJsonContent,
            E_FileDBKeyType.Normal_Loan_sFHACaseQueryResultXmlContent,
            E_FileDBKeyType.Normal_Loan_sFHACaseNumberResultXmlContent,
            E_FileDBKeyType.Normal_Loan_sFHACAVIRSResultXmlContent,
            E_FileDBKeyType.Normal_Loan_sProdPmlDataUsedForLastPricingXmlContent,
            E_FileDBKeyType.Normal_Loan_sTotalScoreCertificateXmlContent,
            E_FileDBKeyType.Normal_Loan_sTotalScoreTempCertificateXmlContent,
            E_FileDBKeyType.Normal_Loan_sPreparerXmlContent
        });

        /// <summary>
        /// Gets FileDBEntryInfos tied specificially to the loan, i.e. not including things tied to the loan closing cost archives, since for those, for instance
        /// one would need to load the archives, parse them, and then grab the filedbkeys associated with each.
        /// </summary>
        public static IEnumerable<FileDBEntryInfo> GetSpecificallyLoanFileDBEntryInfos(Guid loanID)
        {
            var fileDBEntryInfos = new List<FileDBEntryInfo>();
            foreach (E_FileDBKeyType loanKeyType in SpecificallyLoanFileDBKeyTypes)
            {
                fileDBEntryInfos.Add(GetFileDBEntryInfoForLoan(loanKeyType, loanID));
            }
            return fileDBEntryInfos;
        }

        public static bool TryGetUserIdByEmployeeId(Guid brokerID, Guid employeeID, out Guid? userID)
        {
            userID = null;

            var sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@EmployeeID", employeeID),
                new SqlParameter("@BrokerID", brokerID)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerID, "EMPLOYEE_GetUserIdByEmployeeId", sqlParameters))
            {
                if (reader.Read())
                {
                    userID = reader.AsNullableStruct<Guid>("UserID");
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool TryGetUserInfoByEmployeeId(Guid brokerID, Guid employeeID, out Guid? userID, out char? userType)
        {
            userID = null;
            userType = null;

            var sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@EmployeeID", employeeID),
                new SqlParameter("@BrokerID", brokerID)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerID, "EMPLOYEE_GetUserInfoByEmployeeId", sqlParameters))
            {
                if (reader.Read())
                {
                    userID = reader.AsNullableStruct<Guid>("UserID");
                    var userTypeAsString = reader.AsObject<string>("UserType");
                    if (userTypeAsString != null)
                    {
                        userType = userTypeAsString[0];
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static CPipelineView GetMainTabsSettingXmlContent(DbConnectionInfo connInfo, Guid userId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", userId)
                                        };
            string xmlContent = string.Empty;
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "RetrieveMainTabsSettingXmlContent", parameters))
            {
                if(reader.Read())
                {
                    xmlContent = reader["MainTabsSettingXmlContent"].ToString();
                }
            }

            CPipelineView tabs = null;
            if (!string.IsNullOrEmpty(xmlContent))
            {
                tabs = CPipelineView.DeserializeFromXml(xmlContent);
            }
            if (tabs == null)
            {
                tabs = new CPipelineView();
            }
            return tabs;
        }

        public static void UpdateMainTabsSettingXmlContent(DbConnectionInfo connInfo, Guid userId, CPipelineView tabSettings)
        {
            string xmlContent = tabSettings.ToXml();

            SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", userId),
                                            new SqlParameter("@XmlContent", xmlContent)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(connInfo, "UpdateMainTabsSettingXmlContent", 1, parameters);
        }

        /// <summary>
        /// Throws access denied if user lacks permissions / roles.  <para></para>
        /// </summary>
        public static void DenyIfUserLacksSufficientPermissionsOrRoles(
            IEnumerable<LendersOffice.Security.Permission> requiredPermissionsForLoadingPage,
            IEnumerable<LendersOffice.Security.E_RoleT> requiredRolesForLoadingPage)
        {
            var principal = LendersOffice.Security.PrincipalFactory.CurrentPrincipal;
            if (requiredPermissionsForLoadingPage.Count() > 0)
            {
                var requiredPermissionsThatUserLacks = new List<LendersOffice.Security.Permission>();
                foreach (var permission in requiredPermissionsForLoadingPage)
                {
                    if (principal.HasPermission(permission))
                    {
                        continue;
                    }
                    else
                    {
                        requiredPermissionsThatUserLacks.Add(permission);
                    }
                }

                if (requiredPermissionsThatUserLacks.Count() != 0)
                {
                    throw new LendersOffice.Security.AccessDenied(
                        "You do not have the necessary permissions to view this page.",
                        "User " + principal.ToUserDataString() + " lacks permissions: " +
                        string.Join(", ", requiredPermissionsThatUserLacks.Select(p => p.ToString()).ToArray()));
                }
                else
                {
                    return;
                }
            }

            if (requiredRolesForLoadingPage.Count() > 0)
            {
                var requiredRolesThatUserLacks = new List<LendersOffice.Security.E_RoleT>();
                foreach (var role in requiredRolesForLoadingPage)
                {
                    if (principal.HasRole(role))
                    {
                        continue;
                    }
                    else
                    {
                        requiredRolesThatUserLacks.Add(role);
                    }
                }

                if (requiredRolesThatUserLacks.Count() != 0)
                {
                    throw new LendersOffice.Security.AccessDenied(
                        "You do not have the necessary roles to view this page.",
                        "User " + principal.ToUserDataString() + " lacks roles: " +
                        string.Join(", ", requiredRolesThatUserLacks.Select(p => p.ToString()).ToArray()));
                }
                else
                {
                    return;
                }
            }

        }

        public static void GetAdditionalCredentialForBrokerUserPrincipal(
            DbConnectionInfo connInfo,
            Guid userID,
            Guid employeeID,
            Guid brokerID,
            string sessionId,
            out List<string> roles,
            out List<string> ipWhiteList,
            out HashSet<Guid> teamList,
            out bool isLoggedOut)
        {
            roles = new List<string>();
            ipWhiteList = new List<string>();
            teamList = new HashSet<Guid>();

            // 4/18/2014 dd - Using a single stored procedure to return multiple result set.
            SqlParameter[] param = {
                                                new SqlParameter("@BrokerId", brokerID),
                                                new SqlParameter("@UserId", userID),
                                                new SqlParameter("@EmployeeId", employeeID),
                                                new SqlParameter("@SessionId", sessionId)
                                            };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "GetAdditionalCredentialForBrokerUserPrincipal", param))
            {
                // 4/18/2014 dd - This stored procedure returns multiple result set.
                // The order of result set must match with T-SQL stored procedure.

                #region Get Role Information.
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Guid roleId = (Guid)reader["RoleId"];

                        Role role = Role.Get(roleId);
                        roles.Add(role.Desc);
                    }
                }
                #endregion
                reader.NextResult();

                #region Get Broker Global IP White List
                // 4/18/2014 dd - OPM 125242

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        ipWhiteList.Add((string)reader["IPAddress"]);
                    }
                }
                #endregion
                reader.NextResult();

                #region Get Team List

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        teamList.Add((Guid)reader["TeamId"]);
                    }
                }
                #endregion

                reader.NextResult();

                #region Get Session Logout

                // OPM 247103.  If this session has a logout row, it is not a valid session.
                isLoggedOut = reader.HasRows;
                
                #endregion

            }
        }

        /// <summary>
        /// Taken from BrokerInfoService.aspx.cs.
        /// </summary>
        public static bool UsesPrefix(string mersFormat)
        {
            return false == string.IsNullOrEmpty(GetCorporatePrefix(mersFormat));
        }

        public static string GetCorporatePrefix(string pattern)
        {
            pattern = pattern.Replace("{mersID}", "");

            if (pattern.IndexOf("{") != -1)
            {
                string prefix;

                prefix = pattern.Substring(0, pattern.IndexOf("{"));

                return prefix;
            }

            return "";
        }

        public static SqlResultTypes.GetLpePriceGroupIdByEmployeeIdResult GetLpePriceGroupIdByEmployeeId(Guid brokerID, Guid employeeID)
        {
            SqlParameter[] paramters = { new SqlParameter("@EmployeeId", employeeID)};

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerID, "GetLpePriceGroupIdByEmployeeId", paramters))
            {
                if (reader.Read())
                {
                    return new SqlResultTypes.GetLpePriceGroupIdByEmployeeIdResult(reader);
                }
                else
                {
                    return null;
                }
            }
        }

        public static Guid ComputePriceGroupOfUser(Guid employeeId, Guid brokerId, Guid branchId, E_OCRoles role)
        {
            Guid lpePriceGroupId = Guid.Empty;
            SqlResultTypes.GetLpePriceGroupIdByEmployeeIdResult lpePriceGroupIdsByEmployeeID = null;

            if (employeeId != Guid.Empty)
            {
                lpePriceGroupIdsByEmployeeID = Tools.GetLpePriceGroupIdByEmployeeId(brokerId, employeeId);
                if (lpePriceGroupIdsByEmployeeID != null)
                {
                    if (role == E_OCRoles.Broker)
                    {
                        // Fall through goes user pricegroup -> User's branch default pricegroup -> lender's default pricegroup
                        {
                            if (lpePriceGroupIdsByEmployeeID.LpePriceGroupId.HasValue)
                            {
                                lpePriceGroupId = lpePriceGroupIdsByEmployeeID.LpePriceGroupId.Value;
                            }

                            if (lpePriceGroupId == Guid.Empty && lpePriceGroupIdsByEmployeeID.BranchLpePriceGroupIdDefault.HasValue)
                            {
                                lpePriceGroupId = lpePriceGroupIdsByEmployeeID.BranchLpePriceGroupIdDefault.Value;
                            }
                        }
                    }
                    else
                    {
                        // Fall through order: User's (Mini)Correspondent pricegroup => Loan's branch default price group -> User's OC price group -> lender's default pricegroup
                        if (role == E_OCRoles.MiniCorrespondent)
                        {
                            if (lpePriceGroupIdsByEmployeeID.MiniCorrespondentPriceGroupID.HasValue)
                            {
                                lpePriceGroupId = lpePriceGroupIdsByEmployeeID.MiniCorrespondentPriceGroupID.Value;
                            }
                        }
                        else if (role == E_OCRoles.Correspondent)
                        {
                            if (lpePriceGroupIdsByEmployeeID.CorrespondentPriceGroupID.HasValue)
                            {
                                lpePriceGroupId = lpePriceGroupIdsByEmployeeID.CorrespondentPriceGroupID.Value;
                            }
                        }
                        else
                        {
                            throw new UnhandledEnumException(role);
                        }

                        // Default of this loan's branch
                        if (lpePriceGroupId == Guid.Empty)
                        {
                            SqlParameter[] parameters = {
                                                new SqlParameter("@BranchId", branchId)
                                            };
                            using (DbDataReader branchReader = StoredProcedureHelper.ExecuteReader(brokerId, "GetLpePriceGroupIdByBranchId", parameters))
                            {
                                if (branchReader.Read())
                                {
                                    if (!(branchReader["BranchLpePriceGroupIdDefault"] is DBNull))
                                    {
                                        lpePriceGroupId = (Guid)branchReader["BranchLpePriceGroupIdDefault"];
                                    }
                                    else if (!(branchReader["BrokerLpePriceGroupIdDefault"] is DBNull))
                                    {
                                        // Branch is using the corporate-level default
                                        lpePriceGroupId = (Guid)branchReader["BrokerLpePriceGroupIdDefault"];
                                    }
                                }
                            }
                        }

                        // Default of the user's OC
                        if (lpePriceGroupId == Guid.Empty)
                        {
                            SqlParameter[] parameters = {
                                                new SqlParameter("@PmlBrokerId", EmployeeDB.RetrieveById(brokerId, employeeId).PmlBrokerId)
                                            };
                            var columnName = role == E_OCRoles.MiniCorrespondent ? "MiniCorrespondentLpePriceGroupId" : "CorrespondentLpePriceGroupId";
                            using (DbDataReader ocReader = StoredProcedureHelper.ExecuteReader(brokerId, "GetLpePriceGroupIdByPmlBrokerId", parameters))
                            {
                                if (ocReader.Read())
                                {
                                    if (!(ocReader[columnName] is DBNull))
                                    {
                                        lpePriceGroupId = (Guid)ocReader[columnName];
                                    }
                                }
                            }
                        }
                    }

                    // Default at lender
                    if (lpePriceGroupId == Guid.Empty && lpePriceGroupIdsByEmployeeID.BrokerLpePriceGroupIdDefault.HasValue)
                    {
                        lpePriceGroupId = lpePriceGroupIdsByEmployeeID.BrokerLpePriceGroupIdDefault.Value;
                    }
                }
            }

            return lpePriceGroupId;
        }

        public static Guid ComputePriceGroupOfBranch(Guid branchId, Guid brokerId)
        {
            Guid lpePriceGroupId = Guid.Empty;
            SqlParameter[] parameters = {
                                                new SqlParameter("@BranchId", branchId)
                                            };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetLpePriceGroupIdByBranchId", parameters))
            {
                if (reader.Read())
                {
                    // No LpePriceGroupId Yet, get from branch.
                    if (!(reader["BranchLpePriceGroupIdDefault"] is DBNull))
                    {
                        lpePriceGroupId = (Guid)reader["BranchLpePriceGroupIdDefault"];
                    }
                    if (Guid.Empty == lpePriceGroupId)
                    {
                        // Still no LpePriceGroupId yet, get from broker.
                        if (!(reader["BrokerLpePriceGroupIdDefault"] is DBNull))
                        {
                            lpePriceGroupId = (Guid)reader["BrokerLpePriceGroupIdDefault"];
                        }
                    }
                }
            } // using (DbDataReader reader = StoredProcedureHelper.ExecuteReader("GetLpePriceGroupIdByBranchId", parameters))
            return lpePriceGroupId;
        }

        public static Guid ComputePriceGroupOfOriginatingCompany(PmlBroker pmlBroker, BrokerDB broker, E_OCRoles role)
        {
            if (pmlBroker == null)
            {
                throw new ArgumentNullException("pmlBroker");
            }

            Guid? oCPriceGroupId;
            switch (role)
            {
                case E_OCRoles.Broker:
                    oCPriceGroupId = pmlBroker.LpePriceGroupId;
                    break;
                case E_OCRoles.MiniCorrespondent:
                    oCPriceGroupId = pmlBroker.MiniCorrespondentLpePriceGroupId;
                    break;
                case E_OCRoles.Correspondent:
                    oCPriceGroupId = pmlBroker.CorrespondentLpePriceGroupId;
                    break;
                default:
                    throw new UnhandledEnumException(role);
            }

            if (oCPriceGroupId.HasValue && oCPriceGroupId.Value != Guid.Empty)
            {
                return oCPriceGroupId.Value;
            }

            if ( pmlBroker.BranchId.HasValue )
            {
                return ComputePriceGroupOfBranch( pmlBroker.BranchId.Value, pmlBroker.BrokerId);
            }

            // Fall Through to lender default.
            return broker.LpePriceGroupIdDefault;

        }

        private static void ExecuteBatchFile(string filePath)
        {
            if (string.IsNullOrEmpty(filePath))
            {
                Tools.LogWarning("ExecuteBatchFile: filePath cannot be empty.");
                return;
            }

            if (File.Exists(filePath) == false)
            {
                Tools.LogWarning("ExecuteBatchFile: file at [" + filePath + "] not found.");
                return;
            }

            ProcessStartInfo processInfo = new ProcessStartInfo(filePath);
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            processInfo.RedirectStandardError = true;
            processInfo.RedirectStandardOutput = true;

            Process process = Process.Start(processInfo);
            process.WaitForExit();

            // Log output
            int exitCode = process.ExitCode;
            string output = process.StandardOutput.ReadToEnd();
            string error = process.StandardError.ReadToEnd();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("ExecuteBatchFile: " + filePath);

            if (!string.IsNullOrEmpty(output))
            {
                sb.AppendLine("output: " + output);
            }

            if (!string.IsNullOrEmpty(error))
            {
                sb.AppendLine("error: " + error);
            }

            sb.AppendLine("ExitCode: " + exitCode.ToString());

            Tools.LogInfo(sb.ToString());

            process.Close();
        }

        public static void RestartPdfConversionProcess()
        {
            if (!string.IsNullOrEmpty(ConstStage.RestartPdfConversionProcessBatchFilePath))
            {
                ExecuteBatchFile(ConstStage.RestartPdfConversionProcessBatchFilePath);
            }
        }

        /// <summary>
        /// Takes a base url, html file path and conversion options to turn the given html page to pdf.
        /// </summary>
        /// <param name="url">The base url.</param>
        /// <param name="htmlFilePath">The html file in UTF8 encoding. </param>
        /// <param name="options">The options to convert.</param>
        /// <returns>The pdf file.</returns>
        public static string ConvertHtmlFileToPdfFile(string url, string htmlFilePath, PDFConversionOptions options)
        {
            IPdfRasterizer rasterizer = PdfRasterizerFactory.Create(ConstSite.PdfRasterizerHost);
            string file;
            try
            {
                file = rasterizer.ConvertHtmlToPdfFile(url, htmlFilePath, ConstStage.PdfConverterLicenseKey, options);
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                rasterizer = PdfRasterizerFactory.Restart(ConstSite.PdfRasterizerHost);
                file = rasterizer.ConvertHtmlToPdfFile(url, htmlFilePath, ConstStage.PdfConverterLicenseKey, options);
            }

            return file;
        }

        /// <summary>
        /// Checks to see if the PDF rasterizer service is running if not it tries to start it. If validateLicense is true it will also make sure the license is non empty. 
        /// We use an empty string license for development.
        /// </summary>
        /// <param name="validateLicense">Whether the license needs to have a value.</param>
        public static void EnsurePdfRasterizerIsConfiguredAndRunning(bool validateLicense)
        {
            if (validateLicense && string.IsNullOrEmpty(ConstStage.PdfConverterLicenseKey))
            {
                LendersOffice.Common.EmailUtilities.SendToCritical($"{ConstAppDavid.ServerName} Misconfiguration", "PdfConverterLicenseKey is not defined.");
                throw new InvalidOperationException("PdfConverterLicenseKey is not configured correctly.");
            }

            if (PdfRasterizerFactory.IsRunning(ConstSite.PdfRasterizerHost))
            {
                return;
            }

            PdfRasterizerFactory.Restart(ConstSite.PdfRasterizerHost);

            if (!PdfRasterizerFactory.IsRunning(ConstSite.PdfRasterizerHost))
            {
                LendersOffice.Common.EmailUtilities.SendToCritical($"{ConstAppDavid.ServerName} - Misconfiguration", "PdfRasterizer is either not installed or it cannot be started.");
                throw new InvalidOperationException("PdfRasterizer is not configured correctly.");
            }
        }

        /// <summary>
        /// Calls methodToCall and, if an exception is thrown, logs that exception
        /// before rethrowing. The type T should be the return type of the function
        /// to call.
        /// </summary>
        /// <remarks>
        /// This method is particularly handy when used with WebMethods, as those methods
        /// do not correctly log to our tools if called remotely. See PipelineService.aspx.cs
        /// for additional examples.
        /// </remarks>
        /// <typeparam name="T">The return type of methodToCall</typeparam>
        /// <param name="methodToCall"></param>
        /// <returns></returns>
        public static T LogAndThrowIfErrors<T>(Func<T> methodToCall)
        {
            try
            {
                return methodToCall();
            }
            catch (CBaseException)
            {
                throw;
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        /// <summary>
        /// Calls methodToCall and, if an exception is thrown, logs that exception
        /// before rethrowing. No value is returned.
        /// </summary>
        /// <remarks>
        /// This method is particularly handy when used with WebMethods, as those methods
        /// do not correctly log to our tools if called remotely. See PipelineService.aspx.cs
        /// for additional examples.
        /// </remarks>
        /// <param name="methodToCall"></param>
        public static void LogAndThrowIfErrors(Action methodToCall)
        {
            try
            {
                methodToCall();
            }
            catch (CBaseException)
            {
                throw;
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        /// <summary>
        /// Gets the rounded number of years from a total number of months.
        /// </summary>
        /// <param name="totalMonthsString">The total number of months.</param>
        /// <returns>The rounded number of years.</returns>
        public static int GetWholeYears(string totalMonthsString)
        {
            int totalMonths;
            if (!int.TryParse(totalMonthsString, out totalMonths))
            {
                totalMonths = 0;
            }

            return totalMonths / 12;
        }

        /// <summary>
        /// Gets the remaining amount of months from a total number of months
        /// after any full years are removed (remainder after division by 12).
        /// </summary>
        /// <param name="totalMonthsString">The total number of months.</param>
        /// <returns>The remainder number of months.</returns>
        public static int GetRemainderMonths(string totalMonthsString)
        {
            int totalMonths;
            if (!int.TryParse(totalMonthsString, out totalMonths))
            {
                totalMonths = 0;
            }

            return totalMonths % 12;
        }

        private static bool TryGenerateMersMinFromLoanNumber(BrokerDB broker, string sLNm, out string sMersMin, out string errorReasons)
        {
            sMersMin = string.Empty;
            errorReasons = string.Empty;

            bool hasParsingError = false;
            StringBuilder reasons = new StringBuilder();
            reasons.AppendLine("Cannot generate MERS MIN:");

            string trimmedName = sLNm.TrimWhitespaceAndBOM();

            if (trimmedName.Length > 10)
            {
                reasons.AppendLine("* Loan number must be 10 digits or fewer");
                hasParsingError = true;
            }

            Regex nonDigits = new Regex(@"[^0-9]");
            if (nonDigits.IsMatch(trimmedName))
            {
                reasons.AppendLine("* Loan number can only contain digits");
                hasParsingError = true;
            }

            //Note that we can't rely on long.TryParse to give us only digits, as it understands negative numbers.
            long parsedLoanNumber;
            if (!long.TryParse(trimmedName, out parsedLoanNumber))
            {
                //We don't want to berate people twice for not having a numerical loan number when they try to do this,
                //but just in case something is less than 10 digits long yet somehow can't be parsed...
                if (!hasParsingError)
                {
                    reasons.AppendLine("* Loan number can only contain digits");
                    Tools.LogWarning("Loan number: [" + sLNm + "] contained only digits but wasn't a long");
                    hasParsingError = true;
                }
            }

            if (hasParsingError)
            {
                // sMersMin is blank at this point.
                errorReasons = reasons.ToString();
                return false;
            }

            sMersMin = MersUtilities.GenerateMersNumber(broker.MersOrganizationId, parsedLoanNumber);
            return true;
        }

        /// <summary>
        /// Try to generate a mers min number from the given information.
        /// </summary>
        /// <param name="principal">A user principal.</param>
        /// <param name="sMersMin">Output mers min number.</param>
        /// <param name="errorReasons">Output reasons for failure, if any.</param>
        /// <returns>True, if mers generation successful. False, otherwise.</returns>
        public static bool TryGenerateMersMin(AbstractUserPrincipal principal, string sLNm, out string sMersMin, out string errorReasons)
        {
            try
            {
                sMersMin = string.Empty;
                errorReasons = string.Empty;

                CLoanFileNamer loanNamer = new CLoanFileNamer();

                // OPM 273220 - Generate a new MERS number if sequential numbering is used,
                // but blank out MERS MIN if MERS generation uses loan name and loan name is produced by removing prefix.
                if (string.IsNullOrEmpty(principal.BrokerDB.MersFormat))
                {
                    string unusedRefNm;
                    loanNamer.GetLoanAutoName(principal.BrokerId, principal.BranchId, false, false, true, out sMersMin, out unusedRefNm);
                }
                else
                {
                    return TryGenerateMersMinFromLoanNumber(principal.BrokerDB, sLNm, out sMersMin, out errorReasons);
                }
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                sMersMin = string.Empty;
                errorReasons = e.UserMessage;
            }

            return !string.IsNullOrWhiteSpace(sMersMin);
        }

        /// <summary>
        /// Determines whether the given refinance type matches the given loan purpose.
        /// </summary>
        /// <param name="isRefinanceLoan">Indicates whether a loan is a refinance loan.</param>
        /// <param name="purpose">A loan purpose.</param>
        /// <returns>True if the purpose matches the refinance type, false otherwise.</returns>
        public static bool PurposeMatchesTransactionType(bool isRefinanceLoan, E_sLPurposeT purpose)
        {
            if (isRefinanceLoan)
            {
                return purpose == E_sLPurposeT.Refin ||
                    purpose == E_sLPurposeT.RefinCashout ||
                    purpose == E_sLPurposeT.FhaStreamlinedRefinance ||
                    purpose == E_sLPurposeT.VaIrrrl ||
                    purpose == E_sLPurposeT.HomeEquity;
            }
            else
            {
                return purpose == E_sLPurposeT.Purchase;
            }
        }

        /// <summary>
        /// Replaces the value of any node found at the given xpath with asterisks.
        /// </summary>
        /// <param name="xmlString">The XML document in string format.</param>
        /// <param name="xpaths">The xpath to the node whose values you want redacted.</param>
        /// <returns>A redacted XML document in string format.</returns>
        /// <exception cref="XmlException">There is a load or parse error in the XML. In this case, the document remains empty.</exception>
        public static string RedactXmlUsingXPath(string xmlString, params string[] xpaths)
        {
            // Try to redact using XDocument.
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlString);

            foreach (string xpath in xpaths)
            {
                RedactXmlUsingXPath(xmlDoc, xpath);
            }

            return xmlDoc.OuterXml;
        }

        /// <summary>
        /// Replaces the value of any node found at the given xpath with asterisks.
        /// </summary>
        /// <param name="xmlDoc">The XML document to be redacted.</param>
        /// <param name="xpaths">The xpath to the node whose values you want redacted.</param>
        /// <returns>A redacted XML document in string format.</returns>
        /// <exception cref="XmlException">There is a load or parse error in the XML. In this case, the document remains empty.</exception>
        public static void RedactXmlUsingXPath(XmlDocument xmlDoc, string xpath)
        {
            foreach (XmlNode node in xmlDoc.SelectNodes(xpath))
            {
                node.Value = ConstAppDavid.FakePasswordDisplay;
            }
        }

        /// <summary>
        /// Replaces all substrings of the input string that match the specified pattern with asterisks.
        /// </summary>
        /// <param name="xmlString">The XML document in string format.</param>
        /// <param name="nodeName">The name of the node to be replaced.</param>
        /// <remarks>Redacts the values of any elements or attributes that match nodeName.</remarks>
        /// <returns>A redacted XML document in string format.</returns>
        public static string RedactXmlUsingRegex(string xmlString, string nodeName)
        {
            RegularExpressionString? elementRegEx = RegularExpressionString.Create("<" + nodeName + ">[^<]*</" + nodeName + ">");
            RegularExpressionString? attributeRegEx = RegularExpressionString.Create(nodeName + "=\"([^\"]*)\"");

            xmlString = RegularExpressionHelper.Replace(elementRegEx.Value, xmlString, "<" + nodeName + ">" + ConstAppDavid.FakePasswordDisplay + "</" + nodeName + ">");
            xmlString = RegularExpressionHelper.Replace(attributeRegEx.Value, xmlString, nodeName + "=\"" + ConstAppDavid.FakePasswordDisplay + "\"");

            return xmlString;
        }

        /// <summary>
        /// Retrieves the broker ID for the Pmldojo.
        /// </summary>
        /// <returns>The corresponding broker ID.</returns>
        /// <remarks>Per case 144774, we assume the Pmldojo is located on the main DB.</remarks>
        public static object GetPmlDojoBrokerId()
        {
            var args = new SqlParameter[]
            {
                new SqlParameter("@SampleCustomReportLoginNm", ConstStage.SampleCustomReportLoginNm)
            };

            return StoredProcedureHelper.ExecuteScalar(DataSrc.LOShare, "GetPmldojoBrokerId", args);
        }

        /// <summary>
        /// Retrieves the sample report query template ID for the Pmldojo.
        /// </summary>
        /// <returns>The corresponding report query template ID.</returns>
        /// <remarks>Per case 144774, we assume the Pmldojo is located on the main DB.</remarks>
        public static object GetPmlDojoBrokerReportQueryTemplateId()
        {
            var args = new SqlParameter[]
            {
                new SqlParameter("@SampleCustomReportLoginNm", ConstStage.SampleCustomReportLoginNm)
            };

            return StoredProcedureHelper.ExecuteScalar(DataSrc.LOShare, "GetPmldojoBrokerReportQueryTemplateId", args);
        }

        /// <summary>
        /// Checks if the xml string conforms to the MISMO 3.3 Schema.
        /// </summary>
        /// <param name="xml">XML doc string.</param>
        /// <returns>True is XML is valid MISMO 3.3 doc.</returns>
        public static bool IsValidMismo33Xml(string xml)
        {
            // Parse doc from string. Throws is string is not valid XML.
            var doc = XDocument.Parse(xml);

            return IsValidMismo33Xml(doc);
        }

        /// <summary>
        /// Checks if the xml conforms to the MISMO 3.3 Schema.
        /// </summary>
        /// <param name="xml">XML doc.</param>
        /// <returns>True is XML is valid MISMO 3.3 doc.</returns>
        public static bool IsValidMismo33Xml(XDocument doc)
        {
            // Check for namespace.
            string nameSpaceUrl = "http://www.mismo.org/residential/2009/schemas";
            if (doc.Root.GetDefaultNamespace().NamespaceName != nameSpaceUrl && string.IsNullOrEmpty(doc.Root.GetPrefixOfNamespace(nameSpaceUrl)))
            {
                Tools.LogError($"[IsValidMismo33Xml] MISMO XML Namespace not found. Cannot run validation. Returning false.");
                return false;
            }

            // Get Schemas from assembly.
            // NOTE: Order matters when adding schemas. Add schema dependencies first, or the call to XmlSchemaSet.Compile() will throw.
            var schemas = new XmlSchemaSet();
            Assembly assembly = Assembly.GetExecutingAssembly();
            schemas.Add(null, XmlReader.Create(assembly.GetManifestResourceStream("LendersOffice.ObjLib.Conversions.Mismo3.Version3.Schema.xml.xsd")));
            schemas.Add(null, XmlReader.Create(assembly.GetManifestResourceStream("LendersOffice.ObjLib.Conversions.Mismo3.Version3.Schema.xlink.xsd")));
            schemas.Add(null, XmlReader.Create(assembly.GetManifestResourceStream("LendersOffice.ObjLib.Conversions.Mismo3.Version3.Schema.xlinkMISMOB306.xsd")));
            schemas.Add(nameSpaceUrl, XmlReader.Create(assembly.GetManifestResourceStream("LendersOffice.ObjLib.Conversions.Mismo3.Version3.Schema.MISMOEnumeratedTypesB306.xsd")));
            schemas.Add(nameSpaceUrl, XmlReader.Create(assembly.GetManifestResourceStream("LendersOffice.ObjLib.Conversions.Mismo3.Version3.Schema.MISMODataTypesB306.xsd")));
            schemas.Add(nameSpaceUrl, XmlReader.Create(assembly.GetManifestResourceStream("LendersOffice.ObjLib.Conversions.Mismo3.Version3.Schema.MISMOExtensionDetailsB306.xsd")));
            schemas.Add(nameSpaceUrl, XmlReader.Create(assembly.GetManifestResourceStream("LendersOffice.ObjLib.Conversions.Mismo3.Version3.Schema.MISMOComplexTypeExtensionsB306.xsd")));
            schemas.Add(nameSpaceUrl, XmlReader.Create(assembly.GetManifestResourceStream("LendersOffice.ObjLib.Conversions.Mismo3.Version3.Schema.MISMOComplexTypesB306.xsd")));
            schemas.Add(nameSpaceUrl, XmlReader.Create(assembly.GetManifestResourceStream("LendersOffice.ObjLib.Conversions.Mismo3.Version3.Schema.MISMO_3.3.1_B306.xsd")));
            schemas.Compile();

            // Validate.
            StringBuilder errors = new StringBuilder();
            bool result = true;
            doc.Validate(
                schemas,
                (sender, e) =>
                {
                    if (errors.Length == 0)
                    {
                        errors.AppendLine("[IsValidMismo33Xml] MISMO validation failed with the following errors:");
                    }

                    errors.AppendLine("\t*" + e.Message);
                    result = false;
                });

            if (errors.Length > 0)
            {
                Tools.LogError(errors.ToString());
            }

            return result;
        }

        /// <summary>
        /// Computes the ordinal suffix for a number.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <returns>The suffix for the number.</returns>
        public static string ComputeOrdinal(int number)
        {
            if ((number % 100) / 10 == 1)
            {
                return "th";
            }

            var numModTen = number % 10;

            switch (numModTen)
            {
                case 0:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    return "th";
                case 1:
                    return "st";
                case 2:
                    return "nd";
                case 3:
                    return "rd";
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Unhandled value mod ten.");
            }
        }

        /// <summary>
        /// Truncates a string to given max length.
        /// </summary>
        /// <param name="input">The string to truncate.</param>
        /// <param name="maxLength">Desired max length.</param>
        /// <returns>Truncated string. Or original string if less than max length.</returns>
        public static string TruncateString(string input, int maxLength)
        {
            return input.Length > maxLength ? input.Substring(0, maxLength) : input;
        }

        /// <summary>
        /// Gets a CDateTime object from a data reader.
        /// </summary>
        /// <param name="reader"><see cref="IDataReader"/> to get value from.</param>
        /// <param name="columnName">Data column name.</param>
        /// <remarks>Handles null/DBNull values.</remarks>
        /// <returns></returns>
        public static CDateTime GetCDateTimeFromReader(IDataReader reader, string columnName)
        {
            object dateTime = reader[columnName];
            if (dateTime == null || dateTime == DBNull.Value)
            {
                return CDateTime.InvalidWrapValue;
            }

            return CDateTime.Create((DateTime)dateTime);
        }

        /// <summary>
        /// Retrieves the user ID corresponding to the given username.
        /// </summary>
        /// <param name="username">The username to search.</param>
        /// <param name="userType">The user type.</param>
        /// <param name="brokerId">The broker ID associated with the user.</param>
        /// <returns>The user ID corresponding to the username, or the empty Guid if no user was found.</returns>
        public static Guid GetUserIdByUsername(string username, string userType, Guid brokerId)
        {
            if (!userType.Equals("B") && !userType.Equals("P"))
            {
                throw new CBaseException(ErrorMessages.Generic, $"Invalid user type: {userType}");
            }

            SqlParameter[] parameters =
            {
                new SqlParameter("@LoginName", username),
                new SqlParameter("@Type", userType),
                new SqlParameter("@BrokerId", brokerId)
            };

            var userId = Guid.Empty;
            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetUserByLoginName", parameters))
            {
                if (reader.Read())
                {
                     userId = (Guid)reader["UserId"];
                }
            }

            return userId;
        }

        /// <summary>
        /// Retrieves the user login name corresponding to the given user ID.
        /// </summary>
        /// <param name="userId">The user ID to search.</param>
        /// <param name="brokerId">The broker ID associated with the user.</param>
        /// <returns>The user ID corresponding to the username, or the empty Guid if no user was found.</returns>
        public static string GetUserNameByUserId(Guid userId, Guid brokerId)
        {
            if (brokerId == Guid.Empty || brokerId == ConstAppDavid.SystemBrokerGuid)
            {
                return null;
            }

            SqlParameter[] parameters =
            {
                new SqlParameter("@UserId", userId),
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetUserByUserId", parameters))
            {
                if (reader.Read())
                {
                    return reader["LoginNm"] as string;   // Dynamic cast can return null (we want this).
                }
            }

            return null;
        }

        /// <summary>
        /// Determines whether the given principal is a PML user. Doesn't use an SQL query.
        /// </summary>
        /// <param name="principal">The user principal.</param>
        /// <returns>A boolean indicating whether the given principal is a PML user</returns>
        /// <remarks>Moved from Task.cs for wider use -- jl 6/2018</remarks>
        public static bool IsPmlUser(AbstractUserPrincipal principal)
        {
            //! test this.
            return principal != null && principal.Type == "P";
        }

        /// <summary>
        /// Uses an SQL query to determine if the user is a PML user.
        /// </summary>
        /// <exception cref="CBaseException"> occurs if there is not exactly one user with that id.</exception>
        /// <remarks>Moved from Task.cs for wider use -- jl 6/2018</remarks>
        public static bool IsPmlUser(Guid userId, Guid brokerId)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@UserId", userId)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "RetrieveUserInfoByUserId", parameters))
            {
                if (!reader.Read())
                {
                    throw new CBaseException("No users found with the given id.",
                        "No users found with id " + userId);
                }

                if ((string)reader["Type"] == "P")
                {
                    return true;
                }

                if (reader.Read())
                {
                    throw new CBaseException("more than one user with an id.",
                        "more than one user w/ id " + userId);
                }
            }

            return false;
        }

        /// <summary>
        /// Gets the LFF filed ID that corresponds to the legacy GFE field type.
        /// </summary>
        /// <param name="gfeFieldType">Legacy GFE Field Type.</param>
        /// <returns>String representing the legarcy GFE fee field ID.</returns>
        private static string LookupLegacyFeeId(E_LegacyGfeFieldT gfeFieldType)
        {
            switch (gfeFieldType)
            {
                case E_LegacyGfeFieldT.sEscrowF: return "sEscrowFMb";
                case E_LegacyGfeFieldT.sOwnerTitleInsF: return "sOwnerTitleInsFMb";
                case E_LegacyGfeFieldT.sTitleInsF: return "sTitleInsFMb";
                case E_LegacyGfeFieldT.sRecF: return "sRecFMb";
                case E_LegacyGfeFieldT.sCountyRtc: return "sCountyRtcMb";
                case E_LegacyGfeFieldT.sStateRtc: return "sStateRtcMb";
                case E_LegacyGfeFieldT.sNotaryF: return "sNotaryF";
                case E_LegacyGfeFieldT.sRecDeed: return "sRecDeed";
                case E_LegacyGfeFieldT.sRecMortgage: return "sRecMortgage";
                case E_LegacyGfeFieldT.sRecRelease: return "sRecRelease";
                default:
                    throw new UnhandledEnumException(gfeFieldType);
            }
        }
    }

    public class SimpleSandboxLoanItem
    {
        public Guid sLId { get; set; }
        public string sLNm { get; set; }
        public DateTime sCreatedD { get; set; }
    }
    static class ToolsExtension
    {
        public static void AddEnumItem(this DropDownList ddl, string text, Enum value, bool isSelected)
        {
            ListItem item = new ListItem(text, value.ToString("D"));
            item.Selected = isSelected;
            ddl.Items.Add(item);
        }
        public static void AddEnumItem(this DropDownList ddl, string text, Enum value)
        {
            AddEnumItem(ddl, text, value, false);
        }
    }

    public class LargeFieldStorage
    {
        static private string GetStringKey(Guid key)
        {
            return key.ToString().Replace("-", "").ToUpper();
        }

        private Guid m_loanId;
        private ListDictionary m_cachedValues;

        public LargeFieldStorage(Guid loanId)
        {
            m_loanId = loanId;
            m_cachedValues = new ListDictionary();
        }

        /// <summary>
        /// This method checks to see if the FileDB field is available in the supplied cache.  If it is, it returns from the cache, else FileDB.
        /// </summary>
        /// <param name="keySection">  The generated GUID for the field.</param>
        /// <param name="cache">  The cache to be checked for the GUID.</param>
        /// <returns></returns>
        public string GetTextCacheCheck(Guid keySection, Dictionary<Guid, string> cache)
        {
            string val = string.Empty;
            if (!cache.TryGetValue(keySection, out val))
            {
                val = GetText(keySection);
                cache.Add(keySection, val);
            }

            return val;
        }

        /// <summary>
        /// This is a temporary fix to delete old collection data in QP pool. Noticed it doesnt clean up collections causing a massive file to be loaded 
        /// multiple times from filedb. DO NOT CALL THIS ANYWHERE ELSE.
        /// </summary>
        /// <param name="sLId">The loan id.</param>
        public static void DO_NOT_USE_UnlessYouAreAntonio(Guid sLId)
        {
            FileDBTools.Delete(E_FileDB.Normal, GetStringKey(sLId));
        }

        private string FindKeySection(XmlDocument xmlDoc, Guid keySection)
        {
            if (xmlDoc.HasChildNodes)
            {
                XmlNodeList nodeList = xmlDoc.GetElementsByTagName(SECTION_ELEMENT);
                foreach (XmlNode node in nodeList)
                {
                    foreach (XmlAttribute attr in node.Attributes)
                    {
                        if (0 == attr.Name.CompareTo(SECTION_KEY_ATTRIBUTE))
                        {
                            if (0 == attr.Value.CompareTo(keySection.ToString()))
                            {
                                return node.InnerText;
                            }
                        }
                    }
                }
            }

            return string.Empty;
        }

        public string GetText(Guid keySection)
        {
            if (keySection == Guid.Empty)
            {
                return string.Empty;
            }

            if (m_cachedValues.Contains(keySection))
            {
                return m_cachedValues[keySection].ToString();
            }

            string result = string.Empty;

            try
            {
                XmlDocument xmlDoc = ReadXmlDocumentFromFileDB();

                if (xmlDoc.HasChildNodes)
                {
                    result = FindKeySection(xmlDoc, keySection);
                    SetText(keySection, result);
                }

            }
            catch (XmlException)
            {
                result = string.Empty;
            }

            return result;
        }

        private const string APP_ELEMENT = "LendersOffice";
        private const string FORMAT_VERSION_ATTRIBUTE = "FormatVersion";
        private const string FORMAT_VERSION = "1.0";
        private const string LOAN_ELEMENT = "Loan";
        private const string LOAN_ID_ATTRIBUTE = "LoanId";
        private const string SECTION_ELEMENT = "Section";
        private const string SECTION_KEY_ATTRIBUTE = "Key";

        public void SetText(Guid keySection, string sFileDBVal)
        {
            if (m_cachedValues.Contains(keySection))
                m_cachedValues[keySection] = sFileDBVal;
            else
                m_cachedValues.Add(keySection, sFileDBVal);
        }

        public int Count
        {
            get { return m_cachedValues.Count; }
        }

        private XmlDocument ReadXmlDocumentFromFileDB()
        {
            XmlDocument xmlDoc = new XmlDocument();

            Action<Stream> readHandler = delegate(Stream stream)
            {
                xmlDoc.Load(stream);
            };

            try
            {
                FileDBTools.ReadData(E_FileDB.Normal, GetStringKey(this.m_loanId), readHandler);
            }
            catch (FileNotFoundException)
            {
                xmlDoc = new XmlDocument();
            }

            return xmlDoc;
        }

        private void WriteXmlDocumentToFileDB(XmlDocument xmlDoc)
        {
            Action<Stream> writeHandler = delegate(Stream stream)
            {
                XmlWriterSettings writerSettings = new XmlWriterSettings();
                writerSettings.Encoding = new UTF8Encoding(false); // Skip BOM
                writerSettings.OmitXmlDeclaration = true;

                using (XmlWriter xmlWriter = XmlWriter.Create(stream, writerSettings))
                {
                    xmlDoc.WriteTo(xmlWriter);
                }

            };

            FileDBTools.WriteData(E_FileDB.Normal, GetStringKey(m_loanId), writeHandler);
        }

        private XmlDocument CreateDefaultXmlDocument()
        {
            XmlDocument xmlDoc = new XmlDocument(); // Getting a fresh one to avoid corrupted file.

            //<?xml version="1.0" standalone="yes"?>
            xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", "yes");

            //<LendersOffice FormatVersion="1.0">
            XmlAttribute newFormatAttr = xmlDoc.CreateAttribute(FORMAT_VERSION_ATTRIBUTE);
            newFormatAttr.Value = FORMAT_VERSION;
            XmlElement newAppElement = xmlDoc.CreateElement(APP_ELEMENT);
            newAppElement.Attributes.Append(newFormatAttr);

            //	<Loan sLId="000001">
            XmlAttribute newLoanIdAttr = xmlDoc.CreateAttribute(LOAN_ID_ATTRIBUTE);
            newLoanIdAttr.Value = m_loanId.ToString();
            XmlElement newLoanElement = xmlDoc.CreateElement(LOAN_ELEMENT);
            newLoanElement.Attributes.Append(newLoanIdAttr);

            newAppElement.AppendChild(newLoanElement);
            xmlDoc.AppendChild(newAppElement);

            return xmlDoc;
        }

        public void Flush()
        {
            if (m_cachedValues.Count == 0)
            {
                return;
            }

            // 8/22/2016 - dd - WARNING - Add new logic block to this method require extra attention to the bool isContentModified.
            //                If isContentModified DOES NOT set to true correctly then content will not be flush to FileDB.
            XmlDocument xmlDoc = ReadXmlDocumentFromFileDB();

            bool isContentModified = false;

            if (xmlDoc.HasChildNodes == false)
            {
                xmlDoc = CreateDefaultXmlDocument();
                isContentModified = true;
            }

            XmlNodeList nodeList = xmlDoc.GetElementsByTagName(SECTION_ELEMENT);

            IDictionaryEnumerator eUpdate = m_cachedValues.GetEnumerator();
            while (eUpdate.MoveNext())
            {
                Guid key = (Guid)eUpdate.Key;

                if (0 == key.ToString().CompareTo(Tools.EmptyGuid))
                {
                    throw new CBaseException(ErrorMessages.Generic, "Programming error:  Cannot write to FileDB with empty section key.");
                }

                string val = eUpdate.Value.ToString();

                // Does this get auto-reset?? YES
                XmlNode nodeFound = null;
                foreach (XmlNode node in nodeList)
                {
                    foreach (XmlAttribute attr in node.Attributes)
                    {
                        if (0 == attr.Name.CompareTo(SECTION_KEY_ATTRIBUTE))
                        {
                            if (0 == attr.Value.CompareTo(key.ToString()))
                            {
                                nodeFound = node;
                            }
                            break;
                        }
                    }

                    if (null != nodeFound)
                    {
                        break;
                    }
                }

                if (null == nodeFound)
                {
                    //<Section Key="abcdefg">
                    XmlAttribute newAttr = xmlDoc.CreateAttribute(SECTION_KEY_ATTRIBUTE);
                    newAttr.Value = key.ToString();
                    XmlElement newElement = xmlDoc.CreateElement(SECTION_ELEMENT);
                    newElement.Attributes.Append(newAttr);
                    newElement.InnerText = val;
                    XmlNodeList loanList = xmlDoc.GetElementsByTagName(LOAN_ELEMENT);
                    loanList[0].AppendChild(newElement);
                    isContentModified = true;
                }
                else
                {
                    if (nodeFound.InnerText != val)
                    {
                        nodeFound.InnerText = val;
                        isContentModified = true;
                    }
                }
            }

            if (isContentModified)
            {
                WriteXmlDocumentToFileDB(xmlDoc);
            }

            m_cachedValues.Clear();

        }
    }
}
