﻿/// <copyright file="DependencyUtils.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   12/3/2015
/// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Reflection.Emit;
    using System.Text;
    using System.Text.RegularExpressions;
    using DavidDao.Reflection;

    /// <summary>
    /// Provides a set of utility functions and fields for dependency generation.
    /// </summary>
    public static class DependencyUtils
    {
        /// <summary>
        /// A regular expression used to extract the name of a method from a 
        /// <see cref="MethodInfo"/> instance.
        /// </summary>
        public const string MethodNameExtractionRegex = @"^.*<(?<methodName>\w+)>.*$";

        /// <summary>
        /// The capture group containing the name of a method extracted using 
        /// <see cref="MethodNameExtractionRegex"/>.
        /// </summary>
        public const string MethodNameExtractionRegexCaptureGroupName = "methodName";

        /// <summary>
        /// A regular expression used to determine if a method's name corresponds to a 
        /// non-property getter or setter.
        /// </summary>
        public const string GetSetFieldRegex = @"^(Get|Set)\w+Field$";

        /// <summary>
        /// A list of all possible classes containing dependencies to check.
        /// </summary>
        public static readonly Type[] DependencyClasses = new Type[]
        {
            typeof(CPageBase),
            typeof(CAppBase),
            typeof(CPageHelocBase),
            typeof(LendingQB.Core.Data.AppDataConsumer),
            typeof(LendingQB.Core.Data.AppDataLegacyApplication),
        };

        /// <summary>
        /// A list of all possible prefixes for the dependency property of a method.
        /// </summary>
        public static readonly string[] MethodDependencyPropertyPrefixes = new string[] 
        { 
            "sf", 
            "sf_", 
            "af", 
            "af_" 
        };

        /// <summary>
        /// A list of op codes that represent non-lambda function calls or load instructions.
        /// </summary>
        public static readonly HashSet<OpCode> ValidNonLambdaOpCodes = new HashSet<OpCode>() 
        { 
            OpCodes.Call, 
            OpCodes.Calli, 
            OpCodes.Callvirt, 
            OpCodes.Ldstr
        };

        /// <summary>
        /// A list of op codes that represent lambda function calls.
        /// </summary>
        public static readonly HashSet<OpCode> ValidLambdaOpCodes = new HashSet<OpCode>()
        {
            OpCodes.Ldftn,
            OpCodes.Ldvirtftn
        };

        /// <summary>
        /// The default <see cref="BindingFlags"/> to use when obtaining a list of properties 
        /// and methods for a class. <code>BindingFlags.DeclaredOnly</code> is required to
        /// prevent duplicate errors between CPageBase and CPageHelocBase for inherited members.
        /// </summary>
        public static readonly BindingFlags BindingFlags = 
            BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly;

        /// <summary>
        /// Provides a cached list of the special dependencies from 
        /// CFieldInfoTable.x_SpecialDependencyFieldList.
        /// </summary>
        private static readonly Lazy<IEnumerable<string[]>> LazySpecialDependencyList = new Lazy<IEnumerable<string[]>>(
            () => CFieldInfoTable.GetSpecialDependencies(expandTriggerFieldsIfNeeded: true).Select(dependencyFieldList => dependencyFieldList.StringCollection.ToArray()).ToArray());

        /// <summary>
        /// A predicate used to determine if an <see cref="ILInstruction"/> represents
        /// a non-lambda method call or load string instruction.
        /// </summary>
        private static readonly Predicate<ILInstruction> ValidNonLambdaIl = inst =>
            ValidNonLambdaOpCodes.Contains(inst.OpCode);

        /// <summary>
        /// A predicate used to determine if an <see cref="ILInstruction"/> represents
        /// a lambda method call.
        /// </summary>
        private static readonly Predicate<ILInstruction> ValidLambdaIl = inst =>
            ValidLambdaOpCodes.Contains(inst.OpCode);

        /// <summary>
        /// Provides a cached list of the special dependencies from 
        /// CFieldInfoTable.x_SpecialDependencyFieldList.
        /// </summary>
        public static IEnumerable<string[]> SpecialDependencyList => LazySpecialDependencyList.Value;

        /// <summary>
        /// Normalizes the <paramref name="input"/> string by removing occurrences of
        /// <code>get_, set_, _rep</code>, and any trailing underscores.
        /// </summary>
        /// <param name="input">The input string to normalize.</param>
        /// <returns>
        /// A string with all occurrences of <code>get_, set_, _rep</code>, and any 
        /// trailing underscores removed.
        /// </returns>
        public static string NormalizeMemberName(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return input;
            }

            input = input.Trim();
            var resultBuilder = new StringBuilder(input, input.Length);

            if (input.StartsWith("get_") || input.StartsWith("set_"))
            {
                resultBuilder = resultBuilder.Remove(0, 4);
            }

            if (input.EndsWith("_rep"))
            {
                resultBuilder = resultBuilder.Remove(resultBuilder.Length - 4, 4);
            }

            // Fields that end with an underscore set the field without 
            // the ending underscore.
            if (resultBuilder.Length > 0 && resultBuilder[resultBuilder.Length - 1] == '_')
            {
                return resultBuilder.ToString(0, resultBuilder.Length - 1);
            }
            else
            {
                return resultBuilder.ToString();
            }
        }

        /// <summary>
        /// Obtains a list of <see cref="ILInstruction"/>s for the specified
        /// <paramref name="method"/>.
        /// </summary>
        /// <param name="declaringModule">
        /// The <see cref="Module"/> for the <paramref name="method"/>.
        /// </param>
        /// <param name="method">
        /// The <see cref="MethodBody"/> to obtain IL instructions.
        /// </param>
        /// <returns>
        /// A list of <see cref="ILInstruction"/>s for the specified <paramref name="method"/>.
        /// </returns>
        public static List<ILInstruction> GetILFromMethod(Module declaringModule, MethodBody method)
        {
            var allInstructions = MSILParser.Parse(method.GetILAsByteArray());

            var relevantInstructions = allInstructions.FindAll(inst => ValidNonLambdaIl(inst));

            relevantInstructions.AddRange(DependencyUtils.GetLambdaInstructions(
                declaringModule, 
                allInstructions.FindAll(inst => ValidLambdaIl(inst))));

            return relevantInstructions;
        }

        /// <summary>
        /// Obtains a list of <see cref="ILInstruction"/>s present in the
        /// specified <paramref name="lambdaInstructions"/>.
        /// </summary>
        /// <param name="declaringModule">
        /// The <see cref="Module"/> for the <paramref name="lambdaInstructions"/>.
        /// </param>
        /// <param name="lambdaInstructions">
        /// The <see cref="List{ILInstruction}"/> to obtain lambda IL instructions.
        /// </param>
        /// <returns>
        /// A list of <see cref="ILInstruction"/>s present in the specified 
        /// <paramref name="lambdaInstructions"/>.
        /// </returns>
        /// <remarks>
        /// Note that "lambda" is used loosely here.  It may include any use of a function as a first-class member, most notably
        /// passed as an argument to a function.  For example, in the expression <c>myList.Where(char.IsDigit)</c>, <c>IsDigit</c>
        /// will be considered as a lambda.
        /// </remarks>
        public static List<ILInstruction> GetLambdaInstructions(Module declaringModule, List<ILInstruction> lambdaInstructions)
        {
            var relevantInstructions = new List<ILInstruction>();

            foreach (var instruction in lambdaInstructions)
            {
                var lambdaMethod = declaringModule.ResolveMethod(instruction.MetadataToken);
                if (lambdaMethod.DeclaringType.Module != declaringModule)
                {
                    // ILInstructions are particular to a module, and can't cross into another module.  This is okay for us
                    // since LendersOfficeLib's dll dependencies can't contain members of the dependency graph.
                    continue;
                }

                var lambdaMethodBody = lambdaMethod.GetMethodBody();

                if (lambdaMethodBody != null)
                {
                    relevantInstructions.AddRange(DependencyUtils.GetILFromMethod(declaringModule, lambdaMethodBody));
                }
            }

            return relevantInstructions;
        }

        /// <summary>
        /// Determines whether the member with the specified <paramref name="memberName"/>
        /// is part of a special dependency.
        /// </summary>
        /// <param name="memberName">The name of the member for lookup.</param>
        /// <returns>True if the member is part of a special dependency, false otherwise.</returns>
        public static bool IsSpecialDependency(string memberName)
        {
            foreach (var dependencyGroup in DependencyUtils.SpecialDependencyList)
            {
                if (dependencyGroup.Any(dependencyGroupMember => dependencyGroupMember == memberName))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Determines whether the specified <paramref name="method"/> is a non-property field
        /// getter or setter.
        /// </summary>
        /// <param name="method">The <see cref="MethodBase"/> to check.</param>
        /// <returns>
        /// True if the specified <paramref name="method"/> is a non-property field
        /// getter or setter, false otherwise.
        /// </returns>
        public static bool IsGetSetFieldMethod(MethodBase method)
        {
            return Regex.IsMatch(method.Name, DependencyUtils.GetSetFieldRegex, RegexOptions.Compiled);
        }

        /// <summary>
        /// Compares the parameters of two methods to look for overload equivalence.
        /// </summary>
        /// <param name="m1">The first method.</param>
        /// <param name="m2">The second method.</param>
        /// <returns><see langword="true"/> if the parameters match; <see langword="false"/> otherwise.</returns>
        public static bool AreParametersEquivalent(MethodBase m1, MethodBase m2)
        {
            ParameterInfo[] p1 = m1.GetParameters();
            ParameterInfo[] p2 = m2.GetParameters();
            return p1.Length == p2.Length
                && Enumerable.Range(0, p1.Length).All(i => p1[i].ParameterType == p2[i].ParameterType && p1[i].IsOut == p2[i].IsOut);
        }
    }
}
