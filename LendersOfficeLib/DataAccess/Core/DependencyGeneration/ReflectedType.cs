﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    /// Simple container for caching reflected members.
    /// </summary>
    public class ReflectedType
    {
        /// <summary>
        /// The methods available in the type, grouped by name.
        /// </summary>
        private readonly ILookup<string, MethodInfo> methodInfo;

        /// <summary>
        /// A map from property name to property for the properties available in the type.
        /// </summary>
        private readonly Dictionary<string, PropertyInfo> propertyInfo;

        /// <summary>
        /// A set of all properties' get and set accessor methods.
        /// </summary>
        private readonly Lazy<HashSet<MethodInfo>> propertyGetAndSetMethods;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataAccess.ReflectedType"/> class.
        /// </summary>
        /// <param name="type">The type that will be reflected.</param>
        public ReflectedType(Type type)
        {
            this.ContainedType = type;

            this.methodInfo = this.ContainedType.GetMethods(DependencyUtils.BindingFlags).ToLookup(p => p.Name);

            this.propertyInfo = this.ContainedType.GetProperties(DependencyUtils.BindingFlags).ToDictionary(p => p.Name);

            this.propertyGetAndSetMethods = new Lazy<HashSet<MethodInfo>>(
                () => new HashSet<MethodInfo>(this.propertyInfo.Values
                    .SelectMany(property => new[]
                    {
                        property.GetGetMethod(nonPublic: true),
                        property.GetSetMethod(nonPublic: true),
                    })
                    .Where(accessor => accessor != null)));
        }

        /// <summary>
        /// Gets the type that will be reflected.
        /// </summary>
        /// <value>The type that will be reflected.</value>
        public Type ContainedType { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the type contains the given property.
        /// </summary>
        /// <param name="name">The name of the property.</param>
        /// <returns>True if the type contains the property. Otherwise, false.</returns>
        public bool HasProperty(string name)
        {
            return this.propertyInfo.ContainsKey(name);
        }

        /// <summary>
        /// Gets a property by name.
        /// </summary>
        /// <param name="name">The name of the property.</param>
        /// <returns>The property, if found. Otherwise, null.</returns>
        public PropertyInfo GetProperty(string name)
        {
            PropertyInfo property;
            this.propertyInfo.TryGetValue(name, out property);
            return property;
        }

        /// <summary>
        /// Gets a method by name.
        /// </summary>
        /// <param name="name">The name of the method.</param>
        /// <returns>The method, if found. Otherwise, null.</returns>
        public IEnumerable<MethodInfo> GetMethods(string name)
        {
            return this.methodInfo[name];
        }

        /// <summary>
        /// Gets all of the properties available in this type.
        /// </summary>
        /// <returns>The available properties.</returns>
        public IEnumerable<PropertyInfo> GetProperties()
        {
            return this.propertyInfo.Values;
        }

        /// <summary>
        /// Gets all of the methods available in this type.
        /// </summary>
        /// <returns>The available methods.</returns>
        public IEnumerable<MethodInfo> GetMethods()
        {
            return this.methodInfo.SelectMany(m => m);
        }

        /// <summary>
        /// Checks the properties' accessors on the file to verify if the method is an accessor or not.
        /// </summary>
        /// <param name="method">The method to check.</param>
        /// <returns><see langword="true"/> if the properties include <paramref name="method"/> as an accessor; <see langword="false"/> otherwise.</returns>
        public bool IsPropertyGetOrSetMethod(MethodInfo method)
        {
            return this.propertyGetAndSetMethods.Value.Contains(method);
        }
    }
}
