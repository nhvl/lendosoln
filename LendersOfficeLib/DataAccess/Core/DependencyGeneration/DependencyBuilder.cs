﻿/// <copyright file="DependencyBuilder.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   12/3/2015
/// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Reflection;
    using System.Reflection.Emit;
    using DavidDao.Reflection;

    /// <summary>
    /// Provides a means for generating the list of dependencies in a method
    /// or property.
    /// </summary>
    public class DependencyBuilder
    {
        /// <summary>
        /// Provides a set of members that can be skipped when considering dependencies.
        /// </summary>
        private readonly HashSet<string> skippableMemberSet;

        /// <summary>
        /// Initializes a new instance of the <see cref="DependencyBuilder"/> class
        /// that will obtain dependencies for the properties and methods in
        /// <see cref="DependencyUtils.DependencyClasses"/>.
        /// </summary>
        public DependencyBuilder()
        {
            this.ReflectedTypes = new ReadOnlyDictionary<Type, ReflectedType>(
                DependencyUtils.DependencyClasses.ToDictionary(t => t, t => new ReflectedType(t)));
            this.skippableMemberSet = this.GetSkipSet();
        }

        /// <summary>
        /// Gets a map from type name to reflected type.
        /// </summary>
        public IReadOnlyDictionary<Type, ReflectedType> ReflectedTypes { get; }

        /// <summary>
        /// Obtains the names for the dependencies listed in the DependsOn attribute 
        /// for the specified <paramref name="property"/>.
        /// </summary>
        /// <param name="property">
        /// The <see cref="PropertyInfo"/> object with a DependsOn attribute used
        /// to obtain dependencies.
        /// </param>
        /// <param name="specialIncludedDependencies">
        /// A list that will contain the names of special properties included from 
        /// CFieldInfoTable.x_SpecialDependencyFieldList that were not originally 
        /// present in the dependency attribute of <paramref name="property"/>.
        /// </param>
        /// <returns>
        /// A HashSet containing the string names of the dependencies listed in the 
        /// DependsOn attribute for the specified <paramref name="property"/>. 
        /// <paramref name="specialIncludedDependencies"/> will contain any special 
        /// dependencies required to prevent circular dependencies.
        /// </returns>
        public HashSet<string> GetDependenciesFromDependsOn(
            PropertyInfo property, 
            out HashSet<string> specialIncludedDependencies)
        {
            var dependenciesListedForProperty = new HashSet<string>();

            var dependsOnAttribute = property.GetCustomAttribute<DependsOnAttribute>();

            if (dependsOnAttribute == null)
            {
                specialIncludedDependencies = new HashSet<string>();

                return new HashSet<string>();
            }

            if (dependsOnAttribute.GetList().Any())
            {
                dependenciesListedForProperty.UnionWith(
                    from fieldSpec in dependsOnAttribute.GetList() 
                    select DependencyUtils.NormalizeMemberName(fieldSpec.Name));
            }
            else
            {
                // An empty DependsOn indicates a self-dependency.
                dependenciesListedForProperty.Add(property.Name);
            }

            this.AddSpecialDependencies(
                dependenciesListedForProperty, 
                out specialIncludedDependencies);

            return dependenciesListedForProperty;
        }

        /// <summary>
        /// Obtains the names for the dependencies listed in the DependsOn attribute for 
        /// the specified <paramref name="method"/>.
        /// </summary>
        /// <param name="method">
        /// The <see cref="MethodInfo"/> object with a DependsOn attribute used
        /// to obtain dependencies.
        /// </param>
        /// <param name="specialIncludedDependencies">
        /// A list that will contain the names of special properties included from 
        /// CFieldInfoTable.x_SpecialDependencyFieldList that were not originally 
        /// present in the dependency attribute of <paramref name="method"/>.
        /// </param>
        /// <returns>
        /// A HashSet containing the string names of the dependencies listed in the 
        /// DependsOn attribute for the specified <paramref name="method"/>.
        /// <paramref name="specialIncludedDependencies"/> will contain any special 
        /// dependencies required to prevent circular dependencies.
        /// </returns>
        public HashSet<string> GetDependenciesFromDependsOn(
            MethodInfo method,
            out HashSet<string> specialIncludedDependencies)
        {
            var dependenciesListedForMethod = new HashSet<string>();

            var dependsOnList = this.GetDependenciesInMethodDependsOn(
                method.DeclaringType,
                method);

            if (dependsOnList.Any())
            {
                dependenciesListedForMethod.UnionWith(dependsOnList);
            }
            else
            {
                var prefix = this.GetMethodDependencyPropertyPrefix(method);

                // An empty DependsOn indicates a self-dependency.
                dependenciesListedForMethod.Add(prefix + method.Name);
            }

            this.AddSpecialDependencies(
                dependenciesListedForMethod, 
                out specialIncludedDependencies);

            return dependenciesListedForMethod;
        }

        /// <summary>
        /// Obtains the list of names for the dependencies required for the specified 
        /// <paramref name="property"/>, optionally including dependencies that would 
        /// otherwise be skipped.
        /// </summary>
        /// <param name="property">
        /// The <see cref="PropertyInfo"/> object used to obtain dependencies.
        /// </param>
        /// <param name="includeSkippable">
        /// An optional value indicating whether dependencies normally skipped should be 
        /// included in the list returned.
        /// </param>
        /// <returns>
        /// A HashSet containing the string names of the dependencies needed in the 
        /// <paramref name="property"/>.
        /// </returns>
        public HashSet<string> GetAccessors(PropertyInfo property, bool includeSkippable = false)
        {
            var accessorList = new HashSet<string>();

            accessorList.UnionWith(this.GetDependenciesInPropertyAccessor(
                property.GetGetMethod(nonPublic: true), 
                includeSkippable));

            accessorList.UnionWith(this.GetDependenciesInPropertyAccessor(
                property.GetSetMethod(nonPublic: true), 
                includeSkippable));

            return accessorList;
        }

        /// <summary>
        /// Obtains the list of names for the dependencies required for the specified 
        /// <paramref name="method"/>, optionally including dependencies that would 
        /// otherwise be skipped.
        /// </summary>
        /// <param name="method">
        /// The <see cref="MethodInfo"/> object used to obtain dependencies.
        /// </param>
        /// <param name="includeSkippable">
        /// An optional value indicating whether dependencies normally skipped should be 
        /// included in the list returned.
        /// </param>
        /// <returns>
        /// A HashSet containing the string names of the dependencies needed in the 
        /// <paramref name="method"/>.
        /// </returns>
        public HashSet<string> GetAccessors(MethodInfo method, bool includeSkippable = false)
        {
            var methodBody = method.GetMethodBody();

            if (methodBody == null)
            {
                return new HashSet<string>();
            }

            return this.GetAccessedMembers(method, methodBody, includeSkippable);
        }

        /// <summary>
        /// Obtains the prefix for the dependency property of the specified <paramref name="method"/>.
        /// </summary>
        /// <param name="method">The <see cref="MethodInfo"/> object to check.</param>
        /// <returns>
        /// A member of <see cref="DependencyUtils.MethodDependencyPropertyPrefixes"/> if a dependency property exists,
        /// the empty string otherwise.
        /// </returns>
        public string GetMethodDependencyPropertyPrefix(MethodBase method)
        {
            foreach (var prefix in DependencyUtils.MethodDependencyPropertyPrefixes)
            {
                foreach (var reflectedType in this.ReflectedTypes.Values)
                {
                    var prop = reflectedType.GetProperty(prefix + method.Name);
                    if (prop != null && prop.GetCustomAttribute<DependsOnAttribute>() != null)
                    {
                        return prefix;
                    }
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Determines whether the field with the specified <paramref name="fieldId"/> 
        /// is a dependency that can be skipped.
        /// </summary>
        /// <param name="fieldId">The ID of the field to check.</param>
        /// <returns>
        /// True if the field with the specified <paramref name="fieldId"/> can be skipped, 
        /// false otherwise.
        /// </returns>
        public bool IsSkippable(string fieldId)
        {
            return this.skippableMemberSet.Contains(fieldId);
        }

        /// <summary>
        /// Determines whether the specified <paramref name="property"/> is a dependency
        /// that can be skipped.
        /// </summary>
        /// <param name="property">The <see cref="PropertyInfo"/> object to check.</param>
        /// <returns>
        /// True if the specified <paramref name="property"/> can be skipped, false otherwise.
        /// </returns>
        public bool IsSkippable(PropertyInfo property)
        {
            return this.skippableMemberSet.Contains(property.Name);
        }

        /// <summary>
        /// Determines whether the specified <paramref name="method"/> is a dependency
        /// that can be skipped.
        /// </summary>
        /// <param name="method">The <see cref="MethodBase"/> object to check.</param>
        /// <returns>
        /// True if the specified <paramref name="method"/> can be skipped, false otherwise.
        /// </returns>
        public bool IsSkippable(MethodBase method)
        {
            return this.skippableMemberSet.Contains(method.Name);
        }

        /// <summary>
        /// Obtains the hardcoded list of dependency names that can be skipped.
        /// </summary>
        /// <remarks>
        /// Add any dependencies that should be hardcoded to this list with a
        /// comment as to why they can be skipped.
        /// </remarks>
        /// <returns>
        /// A list of properties and methods that are hardcoded as able to be
        /// skipped.
        /// </returns>
        private static List<string> GetHardcodedSkipList()
        {
            return new List<string>()
            {
                //// *** These fields are always loaded from the database through 
                // CSelectStatementProvider.GenerateSelectStatementImpl() and 
                // have no special dependencies listed in the x_SpecialDependencyFieldList
                // from CFieldInfoTable.
                "sLId",
                "sBrokerId",
                "sIntegrationT",
                "sCustomField1D",
                "sFileVersion",
                "IsTemplate",
                "sAuditTrackedFieldChanges",
                "sComplianceEaseStatus",
                "sLNm",
                "sBranchId",
                "sLoanFileT",
                //// ***

                // This field is always available since it uses either the current 
                // principal's BrokerDB or BrokerDB.RetrieveById(sBrokerId).
                "BrokerDB",

                // This field is required by sGfeItems but is automatically initialized 
                // by CPageBase.
                "s_GfeFieldNames",

                // This field is required by sfForceUnlockedStatus but is automatically
                // initialized by CPageBase.
                "s_OrderByStatusT",

                // OPM 245161, 11/28/2016, ML
                // This method contains a lambda expression to order by the date value of
                // a loan's status, causing the VerifyAllMethodOverloadsIncludeDependsOnAttribute
                // test to fail since the lambda has the same name as the method with a DependsOn
                // and is thus treated as an overload. Due to there being no feasible way to 
                // differentiate lambdas in these kind of situations from the actual method with a
                // DependsOn while also preventing the filtering of methods we want to test, this 
                // method should be skipped during the dependency tests.
                "GetMostRecentStatusPriorTo"
            };
        }

        /// <summary>
        /// Determines whether any of the properties present in <paramref name="dependenciesListedInDependsOn"/> 
        /// are part of any special dependencies and, if so, adds the entries in that special dependency 
        /// to <paramref name="dependenciesListedInDependsOn"/>.
        /// </summary>
        /// <param name="dependenciesListedInDependsOn">
        /// The list of dependencies present for a property or method.
        /// </param>
        /// <param name="specialIncludedDependencies">
        /// A list that will contain any special dependencies not originally listed in the dependency attribute
        /// of a property or method.
        /// </param>
        private void AddSpecialDependencies(
            HashSet<string> dependenciesListedInDependsOn,
            out HashSet<string> specialIncludedDependencies)
        {
            specialIncludedDependencies = new HashSet<string>();

            foreach (var specialDependencyGroup in DependencyUtils.SpecialDependencyList)
            {
                if (specialDependencyGroup.Any(specialDependency =>
                        dependenciesListedInDependsOn.Contains(specialDependency)))
                {
                    // We want to include all special dependencies that were not originally
                    // present in the DependsOn of the property.
                    specialIncludedDependencies.UnionWith(
                        specialDependencyGroup.Except(dependenciesListedInDependsOn));

                    dependenciesListedInDependsOn.UnionWith(specialDependencyGroup);
                }
            }
        }

        /// <summary>
        /// Obtains the list of dependency names for the specified 
        /// <paramref name="method"/>.
        /// </summary>
        /// <param name="classType">The containing class of the method.</param>
        /// <param name="method">The method to check.</param>
        /// <returns>
        /// An IEnumerable containing the list of dependency names for the specified 
        /// <paramref name="method"/>.
        /// </returns>
        private IEnumerable<string> GetDependenciesInMethodDependsOn(
            Type classType, 
            MethodBase method)
        {
            if (!this.ReflectedTypes.ContainsKey(classType))
            {
                throw new ArgumentException("Unsupported type.", nameof(classType));
            }

            var dependencyPrefix = this.GetMethodDependencyPropertyPrefix(method);
            var methodDependsOnAttribute = method.GetCustomAttribute<DependsOnAttribute>();

            if (dependencyPrefix == string.Empty && methodDependsOnAttribute == null)
            {
                return new string[0];
            }

            var dependencyStubProperty = this.GetProperty(classType, dependencyPrefix + method.Name);

            if (dependencyStubProperty != null)
            {
                var dependsOnAttributes = dependencyStubProperty.GetCustomAttributes<DependsOnAttribute>();

                if (dependsOnAttributes.Any())
                {
                    if (methodDependsOnAttribute != null)
                    {
                        var error = string.Format(
                            "Method {0} specifies dependencies using a stub property ("
                            + "{1}) and using a method-level DependsOn attribute. "
                            + "All method dependencies must be specified using only one technique.",
                            method.Name,
                            dependencyStubProperty.Name);

                        throw new InvalidOperationException(error);
                    }

                    return dependsOnAttributes.First().GetList().Select(fieldSpec =>
                        DependencyUtils.NormalizeMemberName(fieldSpec.Name));
                }
            }
            else if (methodDependsOnAttribute != null)
            {
                return methodDependsOnAttribute.GetList()
                    .Select(fieldSpec => DependencyUtils.NormalizeMemberName(fieldSpec.Name));
            }

            return new string[0];
        }

        /// <summary>
        /// Obtains the list of dependency names for the getter or setter of a property.
        /// </summary>
        /// <param name="accessorMethodInfo">
        /// The <see cref="MethodInfo"/> for the property getter or setter used to obtain 
        /// dependencies.
        /// </param>
        /// <param name="includeSkippable">
        /// An optional value indicating whether dependencies normally skipped should be 
        /// included in the list returned.
        /// </param>
        /// <returns>
        /// A HashSet containing the names of dependencies required for the getter or setter
        /// of a property.
        /// </returns>
        private HashSet<string> GetDependenciesInPropertyAccessor(
            MethodInfo accessorMethodInfo, 
            bool includeSkippable)
        {
            if (accessorMethodInfo == null)
            {
                return new HashSet<string>();
            }

            var methodBody = accessorMethodInfo.GetMethodBody();

            if (methodBody == null)
            {
                return new HashSet<string>();
            }

            return this.GetAccessedMembers(accessorMethodInfo, methodBody, includeSkippable);
        }

        /// <summary>
        /// Obtains the list of dependency names for the specified <paramref name="method"/>.
        /// </summary>
        /// <param name="method">The <see cref="MethodInfo"/> used to obtain dependencies.</param>
        /// <param name="methodBody">The <see cref="MethodBody"/> for the specified 
        /// <paramref name="method"/></param>
        /// <param name="includeSkippable">
        /// An optional value indicating whether dependencies normally skipped should be 
        /// included in the list returned.
        /// </param>
        /// <returns>
        /// A HashSet containing the names of dependencies required for specified 
        /// <paramref name="method"/>.
        /// </returns>
        private HashSet<string> GetAccessedMembers(
            MethodInfo method,
            MethodBody methodBody,
            bool includeSkippable)
        {
            var accessorList = new HashSet<string>();

            string containingMethodName = DependencyUtils.NormalizeMemberName(method.Name);

            List<ILInstruction> instructions = DependencyUtils.GetILFromMethod(method.DeclaringType.Module, methodBody);

            for (int instIndex = 0; instIndex < instructions.Count; instIndex++)
            {
                if (instructions[instIndex].OpCode == OpCodes.Ldstr)
                {
                    // The load string instructions we keep for when we check
                    // non-property getters and setters for dependencies should 
                    // be skipped; otherwise, Module.ResolveMethod will throw an
                    // ArgumentException.
                    continue;
                }

                MethodBase resolvedMethod = method.DeclaringType.Module.ResolveMethod(
                    instructions[instIndex].MetadataToken,
                    method.DeclaringType.GetGenericArguments(),
                    method.GetGenericArguments());
                if (resolvedMethod.DeclaringType.IsInterface)
                {
                    resolvedMethod = this.ReflectedTypes.Values.FirstOrDefault(t => resolvedMethod.DeclaringType.IsAssignableFrom(t.ContainedType))
                        ?.GetMethods(resolvedMethod.Name).SingleOrDefault(m => DependencyUtils.AreParametersEquivalent(m, resolvedMethod)) ?? resolvedMethod;
                }

                if (this.ReflectedTypes.ContainsKey(resolvedMethod.DeclaringType))
                {
                    this.AddAccessorsForMethod(
                        resolvedMethod,
                        containingMethodName,
                        includeSkippable,
                        accessorList);
                }
                else if (DependencyUtils.IsGetSetFieldMethod(resolvedMethod))
                {
                    this.AddAccessorsForLoadedString(
                        instIndex,
                        instructions,
                        resolvedMethod,
                        containingMethodName,
                        includeSkippable,
                        accessorList);
                }
            }

            return accessorList;
        }

        /// <summary>
        /// Adds accessors for the specified <paramref name="resolvedMethod"/> to the 
        /// specified <paramref name="accessorList"/>.
        /// </summary>
        /// <param name="resolvedMethod">
        /// The method containing accessors.
        /// </param>
        /// <param name="containingMethodName">
        /// The name of the containing method for <paramref name="resolvedMethod"/>.
        /// </param>
        /// <param name="includeSkippable">
        /// A boolean indicating whether dependencies that are normally skipped should be
        /// included.
        /// </param>
        /// <param name="accessorList">
        /// The list of accessors for <paramref name="resolvedMethod"/>.
        /// </param>
        private void AddAccessorsForMethod(
            MethodBase resolvedMethod, 
            string containingMethodName, 
            bool includeSkippable, 
            HashSet<string> accessorList)
        {
            if (this.MethodHasDependsOn(resolvedMethod))
            {
                var normalizedName = DependencyUtils.NormalizeMemberName(resolvedMethod.Name);

                if (normalizedName != containingMethodName)
                {
                    var prefix = this.GetMethodDependencyPropertyPrefix(resolvedMethod);

                    var calledName = prefix + normalizedName;

                    if (includeSkippable || !this.IsSkippable(calledName))
                    {
                        accessorList.Add(calledName);
                    }
                }
            }
            else if (resolvedMethod.Name.Contains("get_") || resolvedMethod.Name.Contains("set_"))
            {
                var calledName = DependencyUtils.NormalizeMemberName(resolvedMethod.Name);

                if (calledName != containingMethodName &&
                    (includeSkippable || !this.IsSkippable(calledName)))
                {
                    accessorList.Add(calledName);
                }
            }
        }

        /// <summary>
        /// Adds accessors for the specified <paramref name="resolvedMethod"/> based
        /// on the string loaded by the specified <paramref name="resolvedMethod"/>.
        /// </summary>
        /// <param name="instIndex">
        /// The current index of <paramref name="resolvedMethod"/>, which is used to find 
        /// the last load string instruction before the call to <paramref name="resolvedMethod"/>.
        /// </param>
        /// <param name="instructions">
        /// The list of instructions for <paramref name="containingMethodName"/>.
        /// </param>
        /// <param name="resolvedMethod">
        /// The method containing accessors.
        /// </param>
        /// <param name="containingMethodName">
        /// The name of the containing method for <paramref name="resolvedMethod"/>.
        /// </param>
        /// <param name="includeSkippable">
        /// A boolean indicating whether dependencies that are normally skipped should be
        /// included.
        /// </param>
        /// <param name="accessorList">
        /// The list of accessors for <paramref name="resolvedMethod"/>.
        /// </param>
        private void AddAccessorsForLoadedString(
            int instIndex, 
            List<ILInstruction> instructions,
            MethodBase resolvedMethod,
            string containingMethodName, 
            bool includeSkippable,
            HashSet<string> accessorList)
        {
            string loadedString = null;

            // We want to find the load string instruction directly
            // before the method call.
            for (var loadStrIndex = instIndex - 1; loadStrIndex >= 0; loadStrIndex--)
            {
                if (instructions[loadStrIndex].OpCode == OpCodes.Ldstr)
                {
                    loadedString = resolvedMethod.DeclaringType.Module.ResolveString(instructions[loadStrIndex].MetadataToken);

                    break;
                }
            }

            if (this.IsValidLoadedString(loadedString, containingMethodName, includeSkippable))
            {
                accessorList.Add(loadedString);
            }
        }

        /// <summary>
        /// Determines whether the specified <paramref name="loadedString"/> contains the
        /// name of a property.
        /// </summary>
        /// <remarks>
        /// We only want to consider field names, so we'll filter based on if the loaded 
        /// string starts with "s" or "a".
        /// </remarks>
        /// <param name="loadedString">The loaded string to check.</param>
        /// <param name="containingMethodName">
        /// The name of the containing method for the method call that loads 
        /// <paramref name="loadedString"/>.
        /// </param>
        /// <param name="allowSkippable">
        /// A boolean indicating whether skip-able members should be included.
        /// </param>
        /// <returns>
        /// True if the specified <paramref name="loadedString"/> contains the name of
        /// a property, false otherwise.
        /// </returns>
        private bool IsValidLoadedString(string loadedString, string containingMethodName, bool allowSkippable)
        {
            return !string.IsNullOrWhiteSpace(loadedString) &&
                loadedString != containingMethodName &&
                (loadedString.StartsWith("s") || loadedString.StartsWith("a")) &&
                (allowSkippable || !this.skippableMemberSet.Contains(loadedString));
        }

        /// <summary>
        /// Gets a property with the given name from the given type.
        /// </summary>
        /// <param name="type">The target type.</param>
        /// <param name="name">The name of the property.</param>
        /// <returns>The property, if found. Otherwise, null.</returns>
        private PropertyInfo GetProperty(Type type, string name)
        {
            ReflectedType reflectedType;
            if (!this.ReflectedTypes.TryGetValue(type, out reflectedType))
            {
                throw new ArgumentException("Unsupported type: " + type.Name, nameof(type));
            }

            return reflectedType.GetProperty(name);
        }

        /// <summary>
        /// Obtains the list of dependency names that can be skipped.
        /// </summary>
        /// <returns>
        /// A HashSet containing the list of methods and properties that can be skipped.
        /// </returns>
        private HashSet<string> GetSkipSet()
        {
            var skipSet = new HashSet<string>(GetHardcodedSkipList());

            // We only signal a missing dependency if the property in question has a DependsOn attribute, 
            // as this is what usually indicates that a field belongs in the database. This means we
            // won't handle the scenario where a database field without a DependsOn attribute is 
            // missing, but this case is rare.
            foreach (ReflectedType dependencyClass in this.ReflectedTypes.Values)
            {
                skipSet.UnionWith(
                    from property in dependencyClass.GetProperties()
                    where property.GetCustomAttribute<DependsOnAttribute>() == null
                    select property.Name);
            }

            // To avoid testing auto-generated getter and setter methods, we ignore any
            // methods with a get_ or set_ prefix and we ignore any _rep methods. In
            // addition, we only signal a missing dependency if the class has a 
            // "af" or "sf" property with a DependsOn for the method.
            foreach (ReflectedType dependencyClass in this.ReflectedTypes.Values)
            {
                skipSet.UnionWith(
                    from method in dependencyClass.GetMethods()
                    where this.MethodIsSkippable(method)
                    select method.Name);
            }

            return skipSet;
        }

        /// <summary>
        /// A function used to determine if a method can be added the list of skip-able
        /// dependencies.
        /// </summary>
        /// <param name="method">The method to check.</param>
        /// <returns>True if the method can be skipped. Otherwise, false.</returns>
        private bool MethodIsSkippable(MethodInfo method)
        {
            return this.MethodRepresentsProperty(method.Name)
                || !this.MethodHasDependsOn(method);
        }

        /// <summary>
        /// A predicate used to determine if a <see cref="MethodInfo"/> represents
        /// a property getter, property setter, or rep field.
        /// </summary>
        /// <param name="methodName">The name of the method.</param>
        /// <returns>A value indicating whether the method appears to be a getter, setter, or rep field.</returns>
        private bool MethodRepresentsProperty(string methodName)
        {
            return methodName.Contains("get_")
                || methodName.Contains("set_")
                || methodName.Contains("_rep");
        }

        /// <summary>
        /// Determines whether the specified <paramref name="method"/> has a DependsOn attribute.
        /// </summary>
        /// <param name="method">The <see cref="MethodInfo"/> object to check.</param>
        /// <returns>
        /// True if the specified <paramref name="method"/> has a DependsOn attribute, false otherwise.
        /// </returns>
        private bool MethodHasDependsOn(MethodBase method)
        {
            return this.GetMethodDependencyPropertyPrefix(method) != string.Empty
                || method.GetCustomAttribute<DependsOnAttribute>() != null;
        }
    }
}
