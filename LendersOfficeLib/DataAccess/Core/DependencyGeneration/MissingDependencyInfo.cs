﻿﻿/// <copyright file="MissingDependencyInfo.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   12/10/2015
/// </summary>
namespace DataAccess
{
    using System.Collections.Generic;

    /// <summary>
    /// Provides a wrapper that captures information for 
    /// a missing dependency.
    /// </summary>
    public class MissingDependencyInfo
    {
        /// <summary>
        /// Gets or sets a value for the list of accessors.
        /// </summary>
        /// <value>
        /// An IEnumerable containing the string names of the accessors.
        /// </value>
        public HashSet<string> Accessors { get; set; }

        /// <summary>
        /// Gets or sets a value for the list of missing dependencies.
        /// </summary>
        /// <value>
        /// An IEnumerable containing the string names of the missing 
        /// dependencies.
        /// </value>
        public HashSet<string> MissingDependencies { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the property has special dependencies.
        /// </summary>
        /// <value>
        /// True if the property has special dependencies, false otherwise.
        /// </value>
        public bool HasSpecialIncludedDependencies { get; set; }
    }
}
