﻿namespace DataAccess
{
    using System;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Interface that encapsulates formatting and parsing of various LendingQB data types.
    /// </summary>
    internal interface ILosConvert
    {
        /// <summary>
        /// Gets the current format target value.
        /// </summary>
        FormatTarget FormatTargetCurrent { get; }

        /// <summary>
        /// Gets a string that represents an invalid count.
        /// </summary>
        string InvalidCountString { get; }

        /// <summary>
        /// Gets a string that represents an invalid date.
        /// </summary>
        string InvalidDateString { get; }

        /// <summary>
        /// Gets a string that represents an invalid decimal value.
        /// </summary>
        string InvalidDecimalString { get; }

        /// <summary>
        /// Gets a string that represents an invalid monetary value.
        /// </summary>
        string InvalidMoneyString { get; }

        /// <summary>
        /// Gets a string that represents an invalid rate.
        /// </summary>
        string InvalidRateString { get; }

        /// <summary>
        /// Convert a number of months to a number of years.
        /// </summary>
        /// <param name="months">The number of months.</param>
        /// <returns>The number of years.</returns>
        string ConvertMonthToYear(string months);

        /// <summary>
        /// Convert a number of years to a number of months.
        /// </summary>
        /// <param name="yrs">The number of years.</param>
        /// <returns>The number of months.</returns>
        string ConvertYearToMonth(string yrs);

        /// <summary>
        /// Sanitize a string representation of a decimal.
        /// </summary>
        /// <param name="str">The string representation of a decimal.</param>
        /// <returns>The sanitized version of the decimal representation.</returns>
        string SanitizedDecimalString(string str);

        /// <summary>
        /// Parse a string representation of a big count value.
        /// </summary>
        /// <param name="str">The string representation of a big count value.</param>
        /// <returns>The parsed big count value.</returns>
        long ToBigCount(string str);

        /// <summary>
        /// Format a big count value.
        /// </summary>
        /// <param name="count">The big count value.</param>
        /// <returns>The formatted big count value.</returns>
        string ToBigCountString(long count);

        /// <summary>
        /// Parse a string representation of a boolean value.
        /// </summary>
        /// <param name="str">The string representation of a boolean value.</param>
        /// <returns>The parsed boolean value.</returns>
        bool ToBit(string str);

        /// <summary>
        /// Format a boolean value.
        /// </summary>
        /// <param name="value">The boolean value.</param>
        /// <returns>The formatted boolean value.</returns>
        string ToBitString(bool value);

        /// <summary>
        /// Parse a string representation of a count value.
        /// </summary>
        /// <param name="str">The string representation of a count value.</param>
        /// <returns>The parsed count value.</returns>
        int ToCount(string str);

        /// <summary>
        /// Format a count value.
        /// </summary>
        /// <param name="count">The count value.</param>
        /// <returns>The formatted count value.</returns>
        string ToCountString(int count);

        /// <summary>
        /// Format a credit score value.
        /// </summary>
        /// <param name="value">The credit score value.</param>
        /// <param name="isRunningPricingEngine">True if this is called by the pricing engine, false otherwise.</param>
        /// <returns>The formatted credit score value.</returns>
        string ToCountStringCreditScore(int value, bool isRunningPricingEngine);

        /// <summary>
        /// Format a count value for HmdaNa.
        /// </summary>
        /// <param name="value">The count value.</param>
        /// <param name="preserveNotApplicableCode">True if the code specifying not applicable should be preserved, false otherwise.</param>
        /// <returns>The formatted count value.</returns>
        string ToCountStringWithHmdaNa(int? value, bool preserveNotApplicableCode = false);

        /// <summary>
        /// Format a count value for HmdaNa.
        /// </summary>
        /// <param name="value">The count value.</param>
        /// <param name="preserveNotApplicableCode">True if the code specifying not applicable should be preserved, false otherwise.</param>
        /// <returns>The formatted count value.</returns>
        string ToCountStringWithHmdaNaAndBlank(int? value, bool preserveNotApplicableCode = false);

        /// <summary>
        /// Format a count value.
        /// </summary>
        /// <param name="rawVal">The count value.</param>
        /// <returns>The formatted count value.</returns>
        string ToCountVarCharStringRep(string rawVal);

        /// <summary>
        /// Parse a string representation of a count value.
        /// </summary>
        /// <param name="value">The string representation of a count value.</param>
        /// <returns>The parsed count value, or null.</returns>
        int? ToCountWithHmdaNa(string value);

        /// <summary>
        /// Format a CDateTime value for HmdaNa.
        /// </summary>
        /// <param name="value">The CDateTime value.</param>
        /// <returns>The formatted CDateTime value.</returns>
        string ToDateStringWithHmdaNa(CDateTime value);

        /// <summary>
        /// Format a DateTime value via a delegate.
        /// </summary>
        /// <param name="value">The delegate.</param>
        /// <returns>The formatted DateTime value.</returns>
        string ToDateTimeString(LosConvert.DelegateDateValue value);

        /// <summary>
        /// Format a CDateTime value.
        /// </summary>
        /// <param name="dt">The CDateTime value.</param>
        /// <returns>The formatted CDateTime value.</returns>
        string ToDateTimeString(CDateTime dt);

        /// <summary>
        /// Format a DateTime value via a delegate.
        /// </summary>
        /// <param name="value">The delegate.</param>
        /// <param name="displayTime">True if the time should be included, false otherwise.</param>
        /// <returns>The formatted DateTime value.</returns>
        string ToDateTimeString(LosConvert.DelegateDateValue value, bool displayTime);

        /// <summary>
        /// Format a DateTimeOffset value.
        /// </summary>
        /// <param name="dtoSrc">The DateTimeOffset value.</param>
        /// <param name="displayTime">True if the time should be included, false otherwise.</param>
        /// <returns>The formatted DateTimeOffset value.</returns>
        string ToDateTimeString(DateTimeOffset dtoSrc, bool displayTime);

        /// <summary>
        /// Format a DateTime value.
        /// </summary>
        /// <param name="src">The DateTime value.</param>
        /// <param name="displayTime">True if the time should be included, false otherwise.</param>
        /// <returns>The formatted DateTime value.</returns>
        string ToDateTimeString(DateTime src, bool displayTime = false);

        /// <summary>
        /// Format a time portion of a DateTime value.
        /// </summary>
        /// <param name="src">The DateTime value.</param>
        /// <returns>The formatted time portion of a DateTime value.</returns>
        string ToDateTimeString_TimeOnly(DateTime src);

        /// <summary>
        /// Format a date value.
        /// </summary>
        /// <param name="src">The date value.</param>
        /// <returns>The formatted date value.</returns>
        string ToDateTimeVarCharString(string src);

        /// <summary>
        /// Parse a string representation of a CDateTime value for HmdaNa.
        /// </summary>
        /// <param name="value">The string representation of the CDateTime value.</param>
        /// <returns>The parsed CDateTime value.</returns>
        CDateTime ToDateWithHmdaNa(string value);

        /// <summary>
        /// Parse a string representation of a decimal value.
        /// </summary>
        /// <param name="str">The string representation of the decimal value.</param>
        /// <returns>The parsed decimal value.</returns>
        decimal ToDecimal(string str);

        /// <summary>
        /// Format a decimal value.
        /// </summary>
        /// <param name="val">The decimal value.</param>
        /// <param name="direction">The format direction.</param>
        /// <returns>The formatted decimal value.</returns>
        string ToDecimalString(decimal val, FormatDirection direction);

        /// <summary>
        /// Format a decimal value that is an SpCuScore.
        /// </summary>
        /// <param name="value">The decimal value.</param>
        /// <returns>The formatted decimal value.</returns>
        string ToDecimalString_sSpCuScore(decimal value);

        /// <summary>
        /// Format a employer TIN (aka EIN) value.
        /// </summary>
        /// <param name="ein">The EIN value.</param>
        /// <returns>The formatted EIN value.</returns>
        string ToEmployerTaxIdentificationNumberString(EmployerTaxIdentificationNumber? ein);

        /// <summary>
        /// Format a float value.
        /// </summary>
        /// <param name="f">The float value.</param>
        /// <returns>The formatted float value.</returns>
        string ToFloatString(float f);

        /// <summary>
        /// Parse a string representation of a Guid value.
        /// </summary>
        /// <param name="str">The string representation of the Guid value.</param>
        /// <returns>The parsed Guid value.</returns>
        Guid ToGuid(string str);

        /// <summary>
        /// Format a GUID value.
        /// </summary>
        /// <param name="g">The GUID value.</param>
        /// <returns>The formatted GUID value.</returns>
        string ToGuidString(Guid g);

        /// <summary>
        /// Parse a string representation of a monetary value.
        /// </summary>
        /// <param name="str">The string representation of the monetary value.</param>
        /// <returns>The parsed monetary value.</returns>
        decimal ToMoney(string str);

        /// <summary>
        /// Format a monetary value.
        /// </summary>
        /// <param name="money">The monetary value.</param>
        /// <param name="direction">The format direction value.</param>
        /// <returns>The formatted monetary value.</returns>
        string ToMoneyString(decimal money, FormatDirection direction);

        /// <summary>
        /// Format a monetary value with 4 digits precision.
        /// </summary>
        /// <param name="money">The monetary value.</param>
        /// <param name="direction">The format direction value.</param>
        /// <returns>The formatted monetary value.</returns>
        string ToMoneyString4DecimalDigits(decimal money, FormatDirection direction);

        /// <summary>
        /// Format a monetary value with 6 digits precision.
        /// </summary>
        /// <param name="money">The monetary value.</param>
        /// <param name="direction">The format direction value.</param>
        /// <returns>The formatted monetary value.</returns>
        string ToMoneyString6DecimalDigits(decimal money, FormatDirection direction);

        /// <summary>
        /// Format a monetary value with rounding away from zero.
        /// </summary>
        /// <param name="money">The monetary value.</param>
        /// <param name="direction">The format direction value.</param>
        /// <returns>The formatted monetary value.</returns>
        string ToMoneyStringRoundAwayFromZero(decimal money, FormatDirection direction);

        /// <summary>
        /// Format a monetary value for HmdaNa.
        /// </summary>
        /// <param name="value">The monetary value.</param>
        /// <returns>The formatted monetary value.</returns>
        string ToMoneyStringWithHmdaNa(decimal? value);

        /// <summary>
        /// Format a monetary value for HmdaNa.
        /// </summary>
        /// <param name="value">The monetary value.</param>
        /// <returns>The formatted monetary value.</returns>
        string ToMoneyStringWithHmdaNaAndBlank(decimal? value);

        /// <summary>
        /// Format a monetary value.
        /// </summary>
        /// <param name="rawVal">The monetary value.</param>
        /// <returns>The formatted monetary value.</returns>
        string ToMoneyVarCharStringRep(string rawVal);

        /// <summary>
        /// Format a big count value.
        /// </summary>
        /// <param name="count">The big count value.</param>
        /// <returns>The formatted big count value.</returns>
        string ToNullableBigCountString(long? count);

        /// <summary>
        /// Format a phone number.
        /// </summary>
        /// <param name="phoneNumberStrVal">The phone number.</param>
        /// <param name="direction">The format direction value.</param>
        /// <returns>The formatted phone number.</returns>
        string ToPhoneNumFormat(string phoneNumberStrVal, FormatDirection direction);

        /// <summary>
        /// Parse a string representation of a rate into a rate value.
        /// </summary>
        /// <param name="str">The string representation of a rate.</param>
        /// <returns>The parsed rate value.</returns>
        decimal ToRate(string str);

        /// <summary>
        /// Format a rate value.
        /// </summary>
        /// <param name="rate">The rate value.</param>
        /// <returns>The formatted rate value.</returns>
        string ToRateString(decimal? rate);

        /// <summary>
        /// Format a rate value.
        /// </summary>
        /// <param name="rate">The rate value.</param>
        /// <returns>The formatted rate value.</returns>
        string ToRateString(decimal rate);

        /// <summary>
        /// Format a rate with 4 digit precision.
        /// </summary>
        /// <param name="rate">The rate value.</param>
        /// <returns>The formatted rate value.</returns>
        string ToRateString4DecimalDigits(decimal rate);

        /// <summary>
        /// Format a rate with 6 digit precision.
        /// </summary>
        /// <param name="rate">The rate value.</param>
        /// <returns>The formatted rate value.</returns>
        string ToRateString6DecimalDigits(decimal rate);

        /// <summary>
        /// Format a rate with 9 digit precision.
        /// </summary>
        /// <param name="rate">The rate value.</param>
        /// <returns>The formatted rate value.</returns>
        string ToRateString9DecimalDigits(decimal rate);

        /// <summary>
        /// Format a rate that is not a percent.
        /// </summary>
        /// <param name="rate">The rate value.</param>
        /// <returns>The formatted rate value.</returns>
        string ToRateStringNoPercent(decimal rate);

        /// <summary>
        /// Format a rate for HmdaNa as a string.
        /// </summary>
        /// <param name="value">The rate value.</param>
        /// <returns>The formatted rate value.</returns>
        string ToRateStringWithHmdaNa(decimal? value);

        /// <summary>
        /// Format a social security number as a string.
        /// </summary>
        /// <param name="ssnStrVal">The social security number.</param>
        /// <param name="direction">The format direction value.</param>
        /// <returns>The formatted social security number.</returns>
        string ToSsnFormat(string ssnStrVal, FormatDirection direction);

        /// <summary>
        /// Format a HmdaNa value as a string.
        /// </summary>
        /// <param name="value">The HmdaNa value.</param>
        /// <returns>A string representation of the HmdaNa value.</returns>
        string ToStringWithHmdaNa(string value);

        /// <summary>
        /// Parse a string representation of an E_TriState value.
        /// </summary>
        /// <param name="str">The string representation of an E_TriState value.</param>
        /// <returns>The E_TriState value.</returns>
        E_TriState ToTri(string str);

        /// <summary>
        /// Format an E_TriState value as a string.
        /// </summary>
        /// <param name="v">The E_TriState value.</param>
        /// <returns>A string representation of the E_TriState value.</returns>
        string ToTriString(E_TriState v);
    }
}