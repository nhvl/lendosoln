﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Constants;
using LendersOffice.Common;

namespace DataAccess
{
    //this is used for creating the long error body message, same as before
    public class TheEmailBody
    {
        private StringBuilder sb;
        public TheEmailBody(Exception exc)
        {
            sb = new StringBuilder();
            sb.AppendFormat("Application: {0}{1}", ConstAppDavid.ServerName, Environment.NewLine);
            sb.AppendFormat("=============================={0}", Environment.NewLine);
            sb.Append("Exception Message: ");
            sb.Append(Environment.NewLine);
            sb.Append(exc.Message);
            sb.Append(Environment.NewLine);

            if (exc.InnerException != null)
            {
                // 10/13/2004 kb - We are getting more nested with our throwing,
                // so we need to dump all the errors.  It would also be nice to
                // pull out all the embedded error strings of each sql exception
                // we come across.

                Exception eInner = exc.InnerException;
                Int32 iLevel = 0;

                while (eInner != null)
                {
                    sb.Append(String.Format("[{0}] {1}", iLevel, eInner.Message) + Environment.NewLine);

                    eInner = eInner.InnerException;

                    ++iLevel;
                }
            }

            sb.Append("------------------------------" + Environment.NewLine);

            sb.Append("Exception Stack Trace:");
            sb.Append(Environment.NewLine);
            sb.Append(exc.StackTrace);
            sb.Append(Environment.NewLine);

            sb.Append("------------------------------" + Environment.NewLine);

            sb.Append("Inner Exception:");
            sb.Append(Environment.NewLine);
            sb.Append(exc.InnerException);
            sb.Append(Environment.NewLine);

            sb.Append("------------------------------" + Environment.NewLine);

            GenerateRequestInformation(sb);

        }

        private void GenerateRequestInformation(StringBuilder sb)
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;

            if (null != context)
            {
                // Display user request information.
                sb.Append(System.Environment.NewLine);
                sb.Append(System.Environment.NewLine);
                sb.Append(System.Environment.NewLine).Append("<-- Request Information -->");
                sb.Append(System.Environment.NewLine);
                sb.AppendFormat("TIME OF EXCEPTION = {0:G}{1}", DateTime.Now, Environment.NewLine);
                sb.AppendFormat("HTTP HOST         = {0}{1}", context.Request.Url, Environment.NewLine);
                sb.AppendFormat("URL REFERRER      = {0}{1}", context.Request.UrlReferrer, Environment.NewLine);
                sb.AppendFormat("HTTP METHOD       = {0}{1}", context.Request.HttpMethod, Environment.NewLine);
                sb.AppendFormat("APPLICATION PATH  = {0}{1}", context.Request.ApplicationPath, Environment.NewLine);
                sb.AppendFormat("HEADERS           = {0}{1}", context.Request.Headers.ToString(), Environment.NewLine);
                sb.AppendFormat("USER AGENT        = {0}{1}", context.Request.UserAgent, Environment.NewLine);
                sb.AppendFormat("USER PRINCIPAL    = {0}{1}", context.User, Environment.NewLine);
                sb.AppendFormat("USER IP           = {0}{1}", RequestHelper.ClientIP, Environment.NewLine);

                sb.Append("<-- Start request string values -->");
                sb.Append(System.Environment.NewLine);

                string[] keys = context.Request.QueryString.AllKeys;
                foreach (string k in keys)
                {
                    sb.AppendFormat("{0} = {1}{2}", k, RequestHelper.GetSafeQueryString(k), Environment.NewLine);
                }
                sb.Append("<-- End request string values -->");
                sb.Append(System.Environment.NewLine);

                sb.Append("<-- Start postback values -->");
                sb.Append(System.Environment.NewLine);
                keys = new string[] { "__EVENTTARGET", "__EVENTARGUMENT" };
                foreach (string k in keys)
                {
                    sb.AppendFormat("{0} = {1}", k, context.Request.Form[k]);
                    sb.Append(System.Environment.NewLine);
                }
                sb.Append("<-- End postback values -->");
                sb.Append(System.Environment.NewLine);

            }
        }


        public string getBodyString
        {
            get { return sb.ToString(); }
        }

    }
}
