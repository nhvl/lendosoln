using System;
using System.Reflection;
using LendersOffice.Common;
namespace DataAccess
{
	[Flags]
	internal enum E_GfeFlags
	{
		None		 = 0,
		McawPurch12b = 0x0001,
		//McawPurch12f = 0x0002,
		//McawPurch12c = 0x0004,
		RealCost	 = 0x0008,
		//Prepaid		 = 0x0010,
		//Reserve		 = 0x0020,
        //McawRefi10i  = 0x0040
	}

	[ AttributeUsage( AttributeTargets.Property, AllowMultiple=false, Inherited=true ) ]
    public class GfeAttribute : Attribute
	{
		private E_GfeFlags m_attrs;
		private string m_propsFieldName;

		internal GfeAttribute( string propsFieldName, E_GfeFlags gfeAttributes )
		{
			m_attrs				= gfeAttributes;
			m_propsFieldName	= propsFieldName;
		}

		internal E_GfeFlags GfeAttributes
		{
			get{ return m_attrs; }
		}

		internal string PropsFieldName
		{
			get { return m_propsFieldName; }
		}
	}

	internal class CGfeItem
	{
		private System.Reflection.PropertyInfo m_valuePropInfo = null;
		private System.Reflection.PropertyInfo m_userPropInfo = null;
        private Func<int> m_userPropGetter = null;

		private string m_fieldName = "";
		
	
		private CPageBase m_loanFile;

        public string FieldName
        {
            get { return m_fieldName; }
        }

		internal CGfeItem( string fieldName, CPageBase loanFile )
		{
			m_loanFile			= loanFile;
			m_fieldName			= fieldName;
		}

		internal bool			IsFhaAllowed				{ get { return LosConvert.GfeItemProps_FhaAllow( UserProps );}	}
		internal bool			IsPaidToBroker				{ get { return LosConvert.GfeItemProps_ToBr( UserProps ); }	}
		internal bool			IsAprItem					{ get { return LosConvert.GfeItemProps_Apr( UserProps ); }	}
		internal bool			IsPaidByBorrower			{ get { return 0 == LosConvert.GfeItemProps_Payer( UserProps ); }	}
		internal bool			IsPaidByBorrowerButFinanced { get { return 1 == LosConvert.GfeItemProps_Payer( UserProps ); }	}
		internal bool			IsPaidBySeller				{ get { return 2 == LosConvert.GfeItemProps_Payer( UserProps ); }	}
		internal bool			IsPaidByNeitherSellerNorBorrower { get { return 3 == LosConvert.GfeItemProps_Payer( UserProps ); }	}
        internal bool          IsPoc                        { get { return LosConvert.GfeItemProps_Poc( UserProps ); } }

		internal decimal Value
		{
			get 
			{
				try
				{
					return (decimal) ValuePropInfo.GetValue( m_loanFile, null );						
				}
				catch{ return 0.0M; }
			}
		}

		internal int UserProps
		{
			get 
			{
				try
				{
                    if (m_userPropGetter == null)
                    {
                        // OPM 215276. Perf. To avoid reflection at every access cache delegate of the property getter instead.
                        m_userPropGetter = (Func<int>) Delegate.CreateDelegate(
                            typeof(Func<int>), m_loanFile, UserPropInfo.GetGetMethod());
                    }

                    return m_userPropGetter();
				}
				catch{ return 0; }
			}
		}


		internal E_GfeFlags Flags
		{
			get{ return GfeStaticCustomAttrs.GfeAttributes; }
		}

		/// <summary>
		/// For status of the checkboxes in GFE for this given value
		/// </summary>
		private PropertyInfo UserPropInfo
		{
			get 
			{
				if( null == m_userPropInfo )
				{
                    m_userPropInfo = m_loanFile.GetType().GetProperty(GfeStaticCustomAttrs.PropsFieldName, BindingFlags.GetProperty | BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic);
				}
				return m_userPropInfo;
			}
		}


		private PropertyInfo ValuePropInfo
		{
			get 
			{
				if( null == m_valuePropInfo )
				{
					m_valuePropInfo = m_loanFile.GetType().GetProperty( m_fieldName, BindingFlags.GetProperty | BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic);
				}
                if (m_valuePropInfo == null)
                {
                    throw new CBaseException(ErrorMessages.Generic, m_fieldName + " did not has GfeItem attribute");
                }
				return m_valuePropInfo;
			}
		}

		/// <summary>
		/// Static attributes in the declarations
		/// </summary>
		private GfeAttribute GfeStaticCustomAttrs						
		{ 
			get 
			{ 
				foreach( Attribute attr in ValuePropInfo.GetCustomAttributes( typeof( GfeAttribute ), false ) )
				{
					return attr as GfeAttribute;
				}
                string errMsg = m_fieldName + " doesn't have GFE item attribute declaration when it should have";
				Tools.LogBug(errMsg);
				throw new CBaseException(ErrorMessages.Generic, errMsg);
			} 
		}

	}
}
 