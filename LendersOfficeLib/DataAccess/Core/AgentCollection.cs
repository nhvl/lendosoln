namespace DataAccess
{
    using System;
    using DataAccess.PathDispatch;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// Temporary class that will be converted to inherit from CXmlRecordCollection soon
    /// </summary>
    public class CAgentCollection : IPathResolvable
	{
		private CPageBase m_parent;
		public CAgentCollection( CPageBase parent )
		{
			m_parent = parent;
		}

		#region AGENTS

		public CAgentFields GetAgentOfRole( E_AgentRoleT role, DataAccess.E_ReturnOptionIfNotExist option )
		{
			return m_parent.GetAgentOfRole( role, option );
		}

		public int GetAgentRecordCount()
		{
			return m_parent.GetAgentRecordCount();
		}

		/// <summary>
		/// Pass -1 for iRecord to create new record.
		/// </summary>
		public CAgentFields GetAgentFields( int iRecord )
		{
			return m_parent.GetAgentFields( iRecord );
		}
		public CAgentFields GetAgentFields(Guid id) 
		{
			return m_parent.GetAgentFields( id );
		}

		public void sAgentDataSetClear()
		{
			m_parent.sAgentDataSetClear();
		}

		#endregion // AGENTS




        

        public System.Collections.Generic.IEnumerable<CAgentFields> GetAllAgents()
        {
            var agents = new System.Collections.Generic.List<CAgentFields>();
            for (var i = 0; i < GetAgentRecordCount(); i++)
            {
                agents.Add(GetAgentFields(i));   
            }
            return agents;
        }

        public object GetElement(IDataPathElement element)
        {
            if (!(element is DataPathSelectionElement))
            {
                throw new ArgumentException("PreparerCollection can only handle DataPathSelectionElements, but passed element of type " + element.GetType());
            }

            var selectorElement = (DataPathSelectionElement)element;

            E_AgentRoleT agentRole;

            if (Enum.TryParse(selectorElement.Name, out agentRole))
            {
                return this.m_parent.GetAgentOfRole(agentRole, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            }
            else
            {
                throw new ArgumentException("Could not parse agent argument to a valid agent type");
            }
        }
    }
}
