﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{
    public class PermissionException : CBaseException
    {
        public PermissionException(string sMessage)
            : base(sMessage, sMessage)
        {

        }
        public PermissionException(string sMessage, string sDevMsg) 
            : base(sMessage, sDevMsg)
        {

        }

        public override bool IsEmailDeveloper
        {
            get
            {
                return false;
            }
            set
            {
            }
        }
    
    }
}
