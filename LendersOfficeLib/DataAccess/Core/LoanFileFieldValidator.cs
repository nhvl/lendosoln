﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Text.RegularExpressions;

    using CommonProjectLib.Common.Lib;
    using ConfigSystem;
    using ConfigSystem.DataAccess;
    using DataAccess.PathDispatch;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes.PathDispatch;

    public class LoanFileFieldValidator : ILoanFileFieldValidator
    {
        /// <summary>
        /// A list of broken field protection rules.  This property is only used if <see cref="ReportAllWorkflowFailureMessages"/> is true.
        /// </summary>
        private HashSet<string> allBrokenRuleMessages = new HashSet<string>();
        private bool m_isValidationCheck = false;
        /// <summary>
        /// Has a list of fields that are defaulted when a collection record is created (CollectionName_FieldId). We use this to determine when a 
        /// collection record is new since a collection record is added with most empty values. Ideally, I should put default values and for some i can,
        /// others I cannot such as accnm which is borrower name.  Pretty much means the user can edit these fields as much as they want as long as nothing else is set.
        /// As soon as something else in the collection record is set the row will go through field protection.
        /// </summary>
        private static HashSet<string> x_knownSetCollectionItems = new HashSet<string>(StringComparer.OrdinalIgnoreCase) {

           "CEmpCollection_EmplmtStat",
           "CEmpCollection_IsSeeAttachment",

           "CLiaCollection_DebtT",
           "CLiaCollection_AccNm",
           "CLiaCollection_NotUsedInRatio",
           "CLiaCollection_WillBePdOff",
           "CLiaCollection_DebtJobExpenseT",

           "CAssetCollection_OwnerT",
           "CAssetCollection_AssetT",
           "CAssetCollection_AccNm",

           "CReCollection_OccR",
           "CReCollection_ReOwnerT"
        };

        /// <summary>
        /// Keep this in sync with FieldEnumerator.cs ~324
        /// </summary>
        private static HashSet<string> x_CAgentFieldSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
        {
            "coll|CAgentFields|RecordId",
            "coll|CAgentFields|AgentRoleT",
            "coll|CAgentFields|AgentName",
            "coll|CAgentFields|LicenseNum",
            "coll|CAgentFields|CaseNum",
            "coll|CAgentFields|CompanyName",
            "coll|CAgentFields|StreetAddr",
            "coll|CAgentFields|City",
            "coll|CAgentFields|State",
            "coll|CAgentFields|Zip",
            "coll|CAgentFields|Phone",
            "coll|CAgentFields|CellPhone",
            "coll|CAgentFields|PagerNum",
            "coll|CAgentFields|FaxNum",
            "coll|CAgentFields|EmployeeIDInCompany",
            "coll|CAgentFields|EmailAddr",
            "coll|CAgentFields|Notes",
            "coll|CAgentFields|OtherAgentRoleTDesc",
            "coll|CAgentFields|DepartmentName",
            "coll|CAgentFields|CommissionPointOfLoanAmount",
            "coll|CAgentFields|CommissionPointOfGrossProfit",
            "coll|CAgentFields|CommissionMinBase",
            "coll|CAgentFields|IsCommissionPaid",
            "coll|CAgentFields|PaymentNotes",
            "coll|CAgentFields|InvestorSoldDate",
            "coll|CAgentFields|InvestorBasisPoints",
            "coll|CAgentFields|IsListedInGFEProviderForm",
            "coll|CAgentFields|IsLenderAssociation",
            "coll|CAgentFields|IsLenderAffiliate",
            "coll|CAgentFields|IsLenderRelative",
            "coll|CAgentFields|HasLenderRelationship",
            "coll|CAgentFields|HasLenderAccountLast12Months",
            "coll|CAgentFields|IsUsedRepeatlyByLenderLast12Months",
            "coll|CAgentFields|ProviderItemNumber",
            "coll|CAgentFields|LicenseNumForCompany",
            "coll|CAgentFields|PhoneOfCompanyminOccurs=0",
            "coll|CAgentFields|FaxOfCompany",
            "coll|CAgentFields|IsNotifyWhenLoanStatusChange",
            "coll|CAgentFields|LendingLicenseXmlContent",
            "coll|CAgentFields|CompanyLendingLicenseXmlContent",
            "coll|CAgentFields|TaxId",
            "coll|CAgentFields|LoanOriginatorIdentifier",
            "coll|CAgentFields|CompanyLoanOriginatorIdentifier",
            "coll|CAgentFields|EmployeeId",
            "coll|CAgentFields|BranchName",
            "coll|CAgentFields|PayToBankName",
            "coll|CAgentFields|PayToBankCityState",
            "coll|CAgentFields|PayToABANumber",
            "coll|CAgentFields|PayToAccountNumber",
            "coll|CAgentFields|PayToAccountName",
            "coll|CAgentFields|FurtherCreditToAccountNumber",
            "coll|CAgentFields|FurtherCreditToAccountName",
            "coll|CAgentFields|AgentSourceT",
            "coll|CAgentFields|BrokerLevelAgentID",
        };

        private HashSet<string> m_fieldChanges;
        private Guid m_sLId;

        private static bool IsFieldSetOnInsertion(string collectionName, string fieldName)
        {
            return x_knownSetCollectionItems.Contains(collectionName + "_" + fieldName);
        }

        private SystemConfig m_global = null;
        private SystemConfig GlobalSystemConfig
        {
            get
            {
                if (m_global == null)
                {
                    IConfigRepository repository = ConfigHandler.GetRepository(ConstAppDavid.SystemBrokerGuid);
                    m_global = repository.LoadActiveRelease(ConstAppDavid.SystemBrokerGuid).Configuration;
                }
                return m_global;
            }
        }

        private FieldRule m_firstBrokenRule = null;
        private List<string> m_invalidChangedFieldIds;

        private HashSet<string> m_collectionsAllowedAdd;
        public bool m_canDeleteClosingCostPayments = false;

        private HashSet<E_sStatusT> allowedLoanStatusChanges = new HashSet<E_sStatusT>();

        public LoanFileFieldValidator(Guid sLId)
        {
            this.m_sLId = sLId;
            this.CanSave = true;
            CheckCollection = true;

            m_fieldProtectionSet = new Dictionary<string, IList<ProtectFieldRule>>(StringComparer.OrdinalIgnoreCase);
            m_fieldWriteSet = new Dictionary<string, WriteFieldRule>(StringComparer.OrdinalIgnoreCase);
            m_invalidChangedFieldIds = new List<string>();
            m_collectionsAllowedAdd = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            m_fieldChanges = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
        }

        public LoanFileFieldValidator(Guid sLId, Dictionary<string, WriteFieldRule> writeRules, Dictionary<string, IList<ProtectFieldRule>> protectionRules)
        {
            this.m_sLId = sLId;
            this.CanSave = true;
            CheckCollection = true;

            m_fieldProtectionSet = protectionRules;
            m_fieldWriteSet = writeRules;
            m_invalidChangedFieldIds = new List<string>();
            m_fieldChanges = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
        }

        internal LoanFileFieldValidator(Guid sLId, AbstractUserPrincipal principal, LoanValueEvaluator evaluator, bool userHasFullWritePrivlege) : this(sLId)
        {
            Principal = principal;
            LoadFieldPermissions(evaluator, userHasFullWritePrivlege);
            LoadAllowedLoanStatusChanges(evaluator);
        }

        /// <summary>
        /// Generate debug information for this class.
        /// </summary>
        /// <returns></returns>
        internal string GetDebugInfo()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("FieldProtectionSet");
            if (null != m_fieldProtectionSet)
            {
                foreach (var o in m_fieldProtectionSet.OrderBy(o => o.Key))
                {
                    sb.AppendLine("    [" + o.Key + "]");
                    foreach (var rule in o.Value)
                    {
                        sb.AppendLine("        " + rule.ToString());
                    }
                }
            }

            sb.AppendLine();
            sb.AppendLine("FieldWriteSet");
            if (null != m_fieldWriteSet)
            {
                foreach (var o in m_fieldWriteSet.OrderBy(o => o.Key))
                {
                    sb.AppendLine("    [" + o.Key + "] - " + o.Value);
                }
            }

            return sb.ToString();
        }

        public static IEnumerable<string> GetListOfCustomCollectionFields()
        {
            // For Borrower set
            foreach (string field in x_availableBorrowerClosingCostFields.Keys)
            {
                yield return string.Format("coll|{0}|{1}", "sClosingCostSet", field);
            }

            foreach (string field in x_availableBorrowerClosingCostProperties.Keys)
            {
                yield return string.Format("coll|{0}|{1}", "sClosingCostSet", field);
            }

            // For Seller set
            foreach (string field in x_availableSellerClosingCostFields.Keys)
            {
                yield return string.Format("coll|{0}|{1}", "sSellerResponsibleClosingCostSet", field);
            }

            foreach (string field in x_availableSellerClosingCostProperties.Keys)
            {
                yield return string.Format("coll|{0}|{1}", "sSellerResponsibleClosingCostSet", field);
            }

            foreach (string field in x_availableLoanEstimateDateFields.Keys)
            {
                yield return string.Format("coll|{0}|{1}", "sLoanEstimateDatesInfo", field);
            }

            foreach (string field in x_availableClosingDisclosureDateFields.Keys)
            {
                yield return string.Format("coll|{0}|{1}", "sClosingDisclosureDatesInfo", field);
            }

            yield return string.Format("coll|{0}|{1}", "sSelectedProductCodeFilter", "sSelectedProductCodeFilter");
        }

        // For BorrowerClosingCostSet
        private static Dictionary<string, FieldInfo> x_availableBorrowerClosingCostFields;
        private static Dictionary<string, MethodInfo> x_availableBorrowerClosingCostProperties;

        // For SellerClosingCostSet
        private static Dictionary<string, FieldInfo> x_availableSellerClosingCostFields;
        private static Dictionary<string, MethodInfo> x_availableSellerClosingCostProperties;

        /// <summary>
        /// List of fields and their accessors for the Loan Estimate Dates.
        /// </summary>
        private static Dictionary<string, FieldInfo> x_availableLoanEstimateDateFields;

        /// <summary>
        /// List of fields and their accessors for the Closing Disclosures Dates.
        /// </summary>
        private static Dictionary<string, FieldInfo> x_availableClosingDisclosureDateFields;

        private static bool CheckInfo(PropertyInfo p, Type attribute, string[] ignoreList)
        {
            if (Attribute.IsDefined(p, attribute) && !ignoreList.Contains(p.Name))
            {
                return true;
            }
            else
            {
                // This is an overriden property. Include it in since it probably doesn't have the Attribute on it.
                var getMethod = p.GetGetMethod();
                if (getMethod != null && getMethod.GetBaseDefinition() != getMethod)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        private static readonly Dictionary<string, Func<object, object, bool>> ExpenseSpecialComparisons = new Dictionary<string, Func<object, object, bool>>(StringComparer.OrdinalIgnoreCase)
        {
            {"disbursementScheduleMonths", (oldVal, newVal) => ((int[])oldVal).SequenceEqual((int[])newVal)}
        };

        private static Dictionary<string, MethodInfo> x_availableBaseHousingExpenseProperties = null;
        private static Dictionary<string, FieldInfo> x_availableBaseHousingExpenseFields = null;
        private static Dictionary<string, FieldInfo> x_availableSingleDisbursementFields = null;
        private static Dictionary<string, MethodInfo> x_availableSingleDisbursementProperties = null;
        private static readonly Dictionary<E_HousingExpenseTypeCalculatedT, Dictionary<string, string>> x_LegacyHousingExpenseMappings;

        static LoanFileFieldValidator()
        {
            string[] ignoreList = new string[] {
                "formula",
                "paymentList",
                "internalHudLine",
                "internalId",
                "Id",
                "IsSystemGenerated",
                "IsSystemLegacyFee",
            };

            Type dataMemberAttributeType = typeof(DataMemberAttribute);

            // For Borrower
            x_availableBorrowerClosingCostProperties = new Dictionary<string, MethodInfo>();
            x_availableBorrowerClosingCostFields = new Dictionary<string, FieldInfo>();

            Type borrowerCostFeeType = typeof(BorrowerClosingCostFee);
            Type borrowerCostPaymentType = typeof(BorrowerClosingCostFeePayment);


            var borrowerClosingProperties = borrowerCostFeeType.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy).Where(p => CheckInfo(p, dataMemberAttributeType, ignoreList));
            var borrowerPaymentProperties = borrowerCostPaymentType.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy).Where(p => CheckInfo(p, dataMemberAttributeType, ignoreList));

            var borrowerClosingFields = borrowerCostFeeType.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy).Where(p => Attribute.IsDefined(p, dataMemberAttributeType) && !ignoreList.Contains(p.Name));
            var borrowerPaymentFields = borrowerCostPaymentType.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy).Where(p => Attribute.IsDefined(p, dataMemberAttributeType) && !ignoreList.Contains(p.Name));

            foreach (var property in borrowerClosingProperties)
            {
                var method = property.GetGetMethod(true);
                if (method == null)
                {
                    Tools.LogInfo("Warning - No Get Method " + property.Name);
                }
                else
                {
                    x_availableBorrowerClosingCostProperties.Add(property.Name, method);
                }
            }

            foreach (var property in borrowerPaymentProperties)
            {
                var method = property.GetGetMethod(true);
                if (method == null)
                {
                    Tools.LogInfo("Warning - No Get Method for property: " + property.Name);
                }
                else
                {
                    string name = string.Concat("Payment-", property.Name);
                    x_availableBorrowerClosingCostProperties.Add(name, method);
                }
            }


            foreach (var property in borrowerClosingFields)
            {
                x_availableBorrowerClosingCostFields.Add(property.Name, property);
            }

            foreach (var property in borrowerPaymentFields)
            {
                string name = string.Concat("Payment-", property.Name);
                x_availableBorrowerClosingCostFields.Add(name, property);
            }

            // For Seller
            x_availableSellerClosingCostProperties = new Dictionary<string, MethodInfo>();
            x_availableSellerClosingCostFields = new Dictionary<string, FieldInfo>();

            Type sellerCostFeeType = typeof(SellerClosingCostFee);
            Type sellerCostPaymentType = typeof(SellerClosingCostFeePayment);

            var sellerClosingProperties = sellerCostFeeType.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy).Where(p => CheckInfo(p, dataMemberAttributeType, ignoreList));
            var sellerPaymentProperties = sellerCostPaymentType.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy).Where(p => CheckInfo(p, dataMemberAttributeType, ignoreList));

            var sellerClosingFields = sellerCostFeeType.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy).Where(p => Attribute.IsDefined(p, dataMemberAttributeType) && !ignoreList.Contains(p.Name));
            var sellerPaymentFields = sellerCostPaymentType.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy).Where(p => Attribute.IsDefined(p, dataMemberAttributeType) && !ignoreList.Contains(p.Name));

            foreach (var property in sellerClosingProperties)
            {
                var method = property.GetGetMethod(true);
                if (method == null)
                {
                    Tools.LogInfo("Warning - No Get Method " + property.Name);
                }
                else
                {
                    x_availableSellerClosingCostProperties.Add(property.Name, method);
                }
            }

            foreach (var property in sellerPaymentProperties)
            {
                var method = property.GetGetMethod(true);
                if (method == null)
                {
                    Tools.LogInfo("Warning - No Get Method for property: " + property.Name);
                }
                else
                {
                    string name = string.Concat("Payment-", property.Name);
                    x_availableSellerClosingCostProperties.Add(name, method);
                }
            }


            foreach (var property in sellerClosingFields)
            {
                x_availableSellerClosingCostFields.Add(property.Name, property);
            }

            foreach (var property in sellerPaymentFields)
            {
                string name = string.Concat("Payment-", property.Name);
                x_availableSellerClosingCostFields.Add(name, property);
            }

            // For Loan Estimate Dates
            Type loanEstimateDateType = typeof(LoanEstimateDates);
            var loanEstimateDateFields = loanEstimateDateType.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy).Where(p => Attribute.IsDefined(p, dataMemberAttributeType));
            x_availableLoanEstimateDateFields = new Dictionary<string, FieldInfo>();

            foreach (var loanEstimateDateField in loanEstimateDateFields)
            {
                x_availableLoanEstimateDateFields.Add(loanEstimateDateField.Name, loanEstimateDateField);
            }

            // For Closing Disclosure Dates
            Type closingDisclosureDateType = typeof(ClosingDisclosureDates);
            var closingDisclosureDateFields = closingDisclosureDateType.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy).Where(p => Attribute.IsDefined(p, dataMemberAttributeType));
            x_availableClosingDisclosureDateFields = new Dictionary<string, FieldInfo>();

            foreach (var closingDisclosureDateField in closingDisclosureDateFields)
            {
                x_availableClosingDisclosureDateFields.Add(closingDisclosureDateField.Name, closingDisclosureDateField);
            }

            // For Housing Expenses
            HashSet<string> expenseIgnoreList = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
            {
                "actualDisbursementList",
                "DisbId",
                "DisbursementType"
            };

            Type housingExpensesType = typeof(BaseHousingExpense);
            var housingExpenseProperties = housingExpensesType.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy).Where(p => Attribute.IsDefined(p, dataMemberAttributeType) && !expenseIgnoreList.Contains(p.Name));
            var housingExpenseFields = housingExpensesType.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy).Where(p => Attribute.IsDefined(p, dataMemberAttributeType) && !expenseIgnoreList.Contains(p.Name));

            x_availableBaseHousingExpenseProperties = new Dictionary<string, MethodInfo>();
            foreach (var housingExpenseProp in housingExpenseProperties)
            {
                var method = housingExpenseProp.GetGetMethod(true);
                if (method == null)
                {
                    Tools.LogInfo("Warning - No Get Method for property: " + housingExpenseProp.Name);
                }
                else
                {
                    x_availableBaseHousingExpenseProperties.Add(housingExpenseProp.Name, method);
                }
            }

            x_availableBaseHousingExpenseFields = new Dictionary<string, FieldInfo>();
            foreach (var housingExpenseField in housingExpenseFields)
            {
                x_availableBaseHousingExpenseFields.Add(housingExpenseField.Name, housingExpenseField);
            }

            Type singleDisbursementType = typeof(SingleDisbursement);
            var singleDisbursementProperties = singleDisbursementType.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy).Where(p => Attribute.IsDefined(p, dataMemberAttributeType) && !expenseIgnoreList.Contains(p.Name));
            var singleDisbursementFields = singleDisbursementType.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy).Where(p => Attribute.IsDefined(p, dataMemberAttributeType) && !expenseIgnoreList.Contains(p.Name));

            x_availableSingleDisbursementProperties = new Dictionary<string, MethodInfo>();
            foreach (var disbProp in singleDisbursementProperties)
            {
                var method = disbProp.GetGetMethod(true);
                if (method == null)
                {
                    Tools.LogInfo("Warning - No Get Method for property: " + disbProp.Name);
                }
                else
                {
                    x_availableSingleDisbursementProperties.Add(disbProp.Name, method);
                }
            }

            x_availableSingleDisbursementFields = new Dictionary<string, FieldInfo>();
            foreach (var disbField in singleDisbursementFields)
            {
                x_availableSingleDisbursementFields.Add(disbField.Name, disbField);
            }

            // Legacy mapping so that a write permission on a legacy field will be checked if one of the new properties has changed.
            Dictionary<E_HousingExpenseTypeCalculatedT, Dictionary<string, string>> legacyFieldMapping = new Dictionary<E_HousingExpenseTypeCalculatedT, Dictionary<string, string>>();
            legacyFieldMapping.Add(
                E_HousingExpenseTypeCalculatedT.HazardInsurance,
                new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase) {
                    { "MonthlyAmtFixedAmt", "sProHazInsMb" },
                    { "AnnualAmtCalcBasePerc", "sProHazInsR" },
                    { "AnnualAmtCalcBaseType", "sProHazInsT" },
                    { "prepaidMonths", "sHazInsPiaMon" },
                    { "reserveMonths", "sHazInsRsrvMon" },
                    { "ReserveMonthsLckd", "sHazInsRsrvMonLckd" },
                    { "IsEscrowedAtClosing", "sHazInsRsrvEscrowedTri" },
                    { "PolicyActivationD", "sHazInsPolicyActivationD" }
            });

            legacyFieldMapping.Add(E_HousingExpenseTypeCalculatedT.FloodInsurance,
                new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
                {
                    { "reserveMonths", "sFloodInsRsrvMon" },
                    { "ReserveMonthsLckd", "sFloodInsRsrvMonLckd" },
                    { "IsEscrowedAtClosing", "sFloodInsRsrvEscrowedTri" },
                    { "PolicyActivationD", "sFloodInsPolicyActivationD" },
                    { "AnnualAmtCalcBasePerc_Calc", "sProFloodIns"},
                    { "MonthlyAmtFixedAmt_Calc", "sProFloodIns" },
                    { "AnnualAmtCalcBaseType_Calc", "sProFloodIns" },
                    { "disbursementAmt_Disb", "sProFloodIns" }
                });

            legacyFieldMapping.Add(E_HousingExpenseTypeCalculatedT.RealEstateTaxes,
                new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
                {
                    { "MonthlyAmtFixedAmt", "sProRealETxMb" },
                    { "AnnualAmtCalcBasePerc", "sProRealETxR" },
                    { "AnnualAmtCalcBaseType", "sProRealETxT" },
                    { "reserveMonths", "sRealETxRsrvMon" },
                    { "ReserveMonthsLckd", "sRealETxRsrvMonLckd" },
                    { "IsEscrowedAtClosing", "sRealETxRsrvEscrowedTri" },
                    { "TaxType", "sTaxTableRealETxT" }
                });

            legacyFieldMapping.Add(E_HousingExpenseTypeCalculatedT.SchoolTaxes,
                new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
                {
                    { "reserveMonths", "sSchoolTxRsrvMon" },
                    { "ReserveMonthsLckd", "sSchoolTxRsrvMonLckd" },
                    { "IsEscrowedAtClosing", "sSchoolTxRsrvEscrowedTri" },
                    { "TaxType", "sTaxTableSchoolT" },
                    { "AnnualAmtCalcBasePerc_Calc", "sProSchoolTx"},
                    { "MonthlyAmtFixedAmt_Calc", "sProSchoolTx" },
                    { "AnnualAmtCalcBaseType_Calc", "sProSchoolTx" },
                    { "disbursementAmt_Disb", "sProSchoolTx" }
                });

            legacyFieldMapping.Add(E_HousingExpenseTypeCalculatedT.WindstormInsurance,
                new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
                {
                    { "PolicyActivationD", "sWindInsPolicyActivationD" }
                });

            legacyFieldMapping.Add(E_HousingExpenseTypeCalculatedT.CondoHO6Insurance,
                new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
                {
                    { "PolicyActivationD", "sCondoHO6InsPolicyActivationD" }
                });

            legacyFieldMapping.Add(E_HousingExpenseTypeCalculatedT.OtherTaxes1,
                new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
                {
                    { "TaxType", "sTaxTable1008T" }
                });

            legacyFieldMapping.Add(E_HousingExpenseTypeCalculatedT.OtherTaxes2,
                new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
                {
                    { "TaxType", "sTaxTable1009T" }
                });

            legacyFieldMapping.Add(E_HousingExpenseTypeCalculatedT.OtherTaxes3,
                new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
                {
                    { "TaxType", "sTaxTableU3T" }
                });

            legacyFieldMapping.Add(E_HousingExpenseTypeCalculatedT.OtherTaxes4,
                new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
                {
                    { "TaxType", "sTaxTableU4T" }
                });

            x_LegacyHousingExpenseMappings = legacyFieldMapping;
        }

        public bool IsDoingValidation { get { return m_isValidationCheck; } }

        /// <summary>
        /// A value determining how far the Field Protection evaluation will run.
        /// If true, the evaluation will continue to run until all of the protection rules have been evaluated.  It will also report
        /// all the access denied messages.
        /// If false, the evaluation will stop as soon as it encounters the first failure, and it will only report that single failure.
        /// </summary>
        public bool ReportAllWorkflowFailureMessages { get; set; }
        private Dictionary<string, WriteFieldRule> m_fieldWriteSet;
        private Dictionary<string, IList<ProtectFieldRule>> m_fieldProtectionSet;

        //following fields are excluded from field write check as these are set by the system
        private static readonly HashSet<string> s_fieldsExcludedFromWriteCheck = new HashSet<string>(
            new List<string> {
            "sIsCreditQualifying" /*always set to true in PML PropertyInfo*/,
            "CalcModeT",
            "sDriveXmlContent",
            // 9/11/2013 AV - 137627 Field Security Rules Ignored [sLtvROtherFinPe Old=[10.268%] New=[10.267%]] - Assign to SDE
            // PLEASE LOOK AT THE CASE.
            "sLtvROtherFinPe",
            });

        private SystemConfig m_systemConfig = null;
        private SystemConfig CurrentSystemConfig
        {
            get
            {
                if (m_systemConfig == null)
                {
                    IConfigRepository repository = ConfigHandler.GetRepository(Principal.BrokerId);
                    var activeRelease = repository.LoadActiveRelease(Principal.BrokerId);
                    if (activeRelease == null)
                    {
                        m_systemConfig = GlobalSystemConfig;
                    }
                    else
                    {

                        m_systemConfig = activeRelease.Configuration ?? GlobalSystemConfig;
                    }
                }
                return m_systemConfig;
            }
        }

        private void SetError(string fieldId, FieldRule rule)
        {
            m_firstBrokenRule = m_firstBrokenRule ?? rule;
            if (this.ReportAllWorkflowFailureMessages)
            {
                allBrokenRuleMessages.UnionWith(rule.FailureMessages);
            }

            m_invalidChangedFieldIds.Add(fieldId);
            this.CanSave = false;
        }

        private void SetError(string fieldId, FieldRule rule, object originalValue, object newValue)
        {
            m_firstBrokenRule = m_firstBrokenRule ?? rule;
            if (this.ReportAllWorkflowFailureMessages)
            {
                allBrokenRuleMessages.UnionWith(rule.FailureMessages);
            }

            m_invalidChangedFieldIds.Add(String.Format("{0} Old=[{1}] New=[{2}]", fieldId, originalValue, newValue));
            this.CanSave = false;
        }

        private bool ValidateCollectionDeletion(string collectionName)
        {
            if (this.HasFullWrite)
            {
                return true;
            }

            SetError(collectionName, null);

            return false;
        }

        /// <summary>
        /// You can only add new collection entries if you have full write or a field write in that collection data set. 
        /// </summary>
        /// <param name="collectionName"></param>
        private bool ValidateCollectionAddition(string collectionName)
        {
            if (CanAddToCollection(collectionName))
            {
                return true;
            }

            SetError(collectionName, null);
            return false;
        }

        private bool CanAddToCollection(string collection)
        {
            if (this.HasFullWrite)
            {
                return true;
            }

            if (m_collectionsAllowedAdd.Contains(collection))
            {
                return true;
            }

            return false;
        }

        private AbstractUserPrincipal Principal
        {
            get;
            set;
        }

        public bool HasFieldWritePermissions
        {
            get;
            private set;
        }

        public bool CheckCollection { get; set; }

        public bool CanSave { get; private set; }

        public bool HasFullWrite { get; private set; }

        public WriteFieldRule GetWriteFieldRule(string fieldId)
        {
            WriteFieldRule rule = null;

            if (null != this.m_fieldWriteSet)
            {
                if (this.m_fieldWriteSet.TryGetValue(fieldId, out rule) == false)
                {
                    rule = null;
                }
            }

            return rule;
        }

        public IEnumerable<ProtectFieldRule> GetProtectionFieldRule(string fieldId)
        {
            IList<ProtectFieldRule> rules = null;

            if (null != this.m_fieldProtectionSet)
            {
                if (this.m_fieldProtectionSet.TryGetValue(fieldId, out rules))
                {
                    rules = null;
                }
            }

            return rules;
        }

        public IEnumerable<string> GetProtectedFields()
        {
            HashSet<string> keys = new HashSet<string>();
            
            foreach (string key in m_fieldProtectionSet.Keys)
            {
                if (key.StartsWith(DataPath.PathHeader, StringComparison.OrdinalIgnoreCase))
                {
                    keys.UnionWith(PathResolver.GetPathDependencyFields(key));
                    continue;
                }

                if (x_CAgentFieldSet.Contains(key))
                {
                    keys.Add("sAgentXmlContent");
                }
                
                keys.Add(key);
            }

            return keys;
        }

        /// <summary>
        /// Gets the field protection rules for the validator.
        /// </summary>
        /// <returns>
        /// The field protection rules.
        /// </returns>
        public IEnumerable<ProtectFieldRule> GetProtectFieldRules() => this.m_fieldProtectionSet.SelectMany(kvp => kvp.Value);

        /// <summary>
        /// Gets the field-level write rules for the validator.
        /// </summary>
        /// <returns>
        /// The field-level write rules.
        /// </returns>
        public IEnumerable<WriteFieldRule> GetWriteFieldRules()
        {
            if (this.HasFullWrite)
            {
                return Enumerable.Empty<WriteFieldRule>();
            }

            return this.m_fieldWriteSet.Values;
        }

        public LoanFieldWritePermissionDenied GetException()
        {
            if (this.CanSave)
            {
                return null;
            }

            string msg = null;
            bool isFieldWriteFailure;
            if (this.ReportAllWorkflowFailureMessages)
            {
                if (m_invalidChangedFieldIds.Count > 0)
                {
                    isFieldWriteFailure = true;
                    foreach (var failureMessage in this.allBrokenRuleMessages)
                    {
                        msg += failureMessage + "\n";
                    }
                }
                else
                {
                    isFieldWriteFailure = false;
                }
            }
            else
            {
                if (null != m_firstBrokenRule)
                {
                    msg = m_firstBrokenRule.FailureMessages.FirstOrDefault();
                    isFieldWriteFailure = true;
                }
                else
                {
                    isFieldWriteFailure = m_firstBrokenRule is WriteFieldRule;
                }
            }

            if (string.IsNullOrEmpty(msg))
            {
                msg = ErrorMessages.GenericAccessDenied;
            }

            StringBuilder debugInfo = new StringBuilder();
            debugInfo.AppendFormat("Field Rule Violation for User {1} Loan {2}{0}Lender: {3}{0}FirstErrorRuleType: {4}{0}ChosenDenialMessage: {5}{0} Debug:{0}",
                Environment.NewLine, Principal.LoginNm, m_sLId, Principal.BrokerId, isFieldWriteFailure ? "FieldWrite" : "FieldProtection", msg);

            foreach (var entry in m_fieldWriteSet)
            {
                debugInfo.AppendLine(entry.Value.ToString());
            }

            foreach (var entry in m_fieldProtectionSet)
            {
                foreach (var secondEntry in entry.Value)
                {
                    debugInfo.AppendLine(secondEntry.ToString());
                }
            }

            debugInfo.AppendFormat("Invalid Field Changes ({0})", string.Join(",", m_invalidChangedFieldIds.ToArray()));

            Tools.LogWarning(debugInfo.ToString());

            return new LoanFieldWritePermissionDenied(isFieldWriteFailure, m_invalidChangedFieldIds, msg, debugInfo.ToString());
        }

        public void ValidateProductCodeFilters(string collectionName, Dictionary<string, bool> originalCodes, Dictionary<string, bool> currentCodes)
        {
            if (originalCodes == null || currentCodes == null)
            {
                return;
            }

            if (!this.CheckCollection)
            {
                return;
            }

            if (m_fieldProtectionSet.Count == 0 && m_fieldWriteSet.Count == 0)
            {
                return;
            }

            bool hasChanged = false;
            if (originalCodes.Count != currentCodes.Count)
            {
                hasChanged = true;
            }
            else
            {
                foreach (var oldPair in originalCodes)
                {
                    bool newVal;
                    if (!currentCodes.TryGetValue(oldPair.Key, out newVal) ||
                        newVal != oldPair.Value)
                    {
                        hasChanged = true;
                        break;
                    }
                }
            }

            if (hasChanged)
            {
                string newVal = SerializationHelper.JsonNetSerialize(currentCodes);
                if (currentCodes.Count == 0)
                {
                    newVal = string.Empty;
                }

                string fieldId = GetCollectionFieldName(collectionName, collectionName);
                ValidateFieldWrite(fieldId, newVal);
                ValidateFieldProtection(fieldId, newVal.GetType(), newVal);
            }
        }

        /// <summary>
        /// Validates the housing expenses for workflow write permissions.
        /// </summary>
        /// <param name="originalJson">The old housing expense JSON.</param>
        /// <param name="currentJson">The new housing expense JSON.</param>
        /// <remarks>
        /// This checks the housing expense properties as well as the disbursement properties.
        /// Also, this contains a mapping between certain loan file fields to specific expenses' properties (sProHazInsMb to HazardExpense.MonthlyAmountFixedAmount).
        /// This mapping allows write rules on sProHazInsMb to still work properly when modifying HazardExpense.MonthlyAmountFixedAmount.
        /// </remarks>
        public void ValidateHousingExpenses(string originalJson, string currentJson)
        {
            if (originalJson == null || currentJson == null)
            {
                return;
            }

            if (false == this.CheckCollection)
            {
                return;
            }

            if (m_fieldProtectionSet.Count == 0 && m_fieldWriteSet.Count == 0)
            {
                return;
            }

            HousingExpenses original = new HousingExpenses(originalJson, null);
            HousingExpenses current = new HousingExpenses(currentJson, null);

            Dictionary<E_HousingExpenseTypeCalculatedT, BaseHousingExpense> originalDictionary = original.ExpensesToUse.ToDictionary(exp => exp.CalculatedHousingExpenseType);
            Dictionary<E_HousingExpenseTypeCalculatedT, BaseHousingExpense> currentDictionary = current.ExpensesToUse.ToDictionary(exp => exp.CalculatedHousingExpenseType);

            bool validateAdd = false;
            foreach (KeyValuePair<E_HousingExpenseTypeCalculatedT, BaseHousingExpense> currentPair in currentDictionary)
            {
                BaseHousingExpense currentExpense = currentPair.Value;
                BaseHousingExpense originalExpense = null;

                if (originalDictionary.ContainsKey(currentPair.Key))
                {
                    originalExpense = originalDictionary[currentPair.Key];
                }

                if (originalExpense == null)
                {
                    validateAdd = true;
                    break;
                }

                foreach (var field in x_availableBaseHousingExpenseFields)
                {
                    object oldValue;
                    object newValue;

                    this.PopulateValuesFromFieldInfo(originalExpense, currentExpense, field.Value, out oldValue, out newValue);

                    Func<object, object, bool> specialComparer = null;
                    ExpenseSpecialComparisons.TryGetValue(field.Key, out specialComparer);
                    if (!DidObjectChange(oldValue, newValue, specialComparer))
                    {
                        continue;
                    }

                    this.ValidateHousingExpenseFieldChange(currentPair.Value, field.Key, field.Value.FieldType, newValue);
                }

                foreach (var property in x_availableBaseHousingExpenseProperties)
                {
                    object oldValue;
                    object newValue;

                    this.PopulateValuesFromMethodInfo(originalExpense, currentExpense, property.Value, out oldValue, out newValue);

                    Func<object, object, bool> specialComparer = null;
                    ExpenseSpecialComparisons.TryGetValue(property.Key, out specialComparer);
                    if (!DidObjectChange(oldValue, newValue, specialComparer))
                    {
                        continue;
                    }

                    this.ValidateHousingExpenseFieldChange(currentPair.Value, property.Key, property.Value.ReturnType, newValue);
                }

                // We only need to check disbursements in Disbursement mode since that's the only time they're editable.
                if (currentExpense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements &&
                    originalExpense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
                {
                    Dictionary<Guid, SingleDisbursement> currentDisbursements = currentExpense.ActualDisbursements;
                    Dictionary<Guid, SingleDisbursement> originalDisbursements = originalExpense.ActualDisbursements;
                    List<Guid> sharedDisbursements = new List<Guid>();
                    foreach (KeyValuePair<Guid, SingleDisbursement> currentDisbPair in currentDisbursements)
                    {
                        SingleDisbursement currentDisb = currentDisbPair.Value;
                        SingleDisbursement originalDisb = null;

                        if (originalDisbursements.ContainsKey(currentDisb.DisbId))
                        {
                            originalDisb = originalDisbursements[currentDisb.DisbId];
                            sharedDisbursements.Add(currentDisb.DisbId);
                        }

                        if (originalDisb == null)
                        {
                            validateAdd = true;
                            break;
                        }

                        foreach (var disbField in x_availableSingleDisbursementFields)
                        {
                            object oldValue;
                            object newValue;

                            this.PopulateValuesFromFieldInfo(currentDisb, originalDisb, disbField.Value, out oldValue, out newValue);

                            Func<object, object, bool> specialComparer = null;
                            ExpenseSpecialComparisons.TryGetValue(disbField.Key, out specialComparer);
                            if (!DidObjectChange(oldValue, newValue, specialComparer))
                            {
                                continue;
                            }

                            this.ValidateHousingExpenseFieldChange(currentPair.Value, disbField.Key, disbField.Value.FieldType, newValue);
                        }

                        foreach (var disbProp in x_availableSingleDisbursementProperties)
                        {
                            object oldValue;
                            object newValue;

                            this.PopulateValuesFromMethodInfo(currentDisb, originalDisb, disbProp.Value, out oldValue, out newValue);

                            Func<object, object, bool> specialComparer = null;
                            ExpenseSpecialComparisons.TryGetValue(disbProp.Key, out specialComparer);
                            if (!DidObjectChange(oldValue, newValue, specialComparer))
                            {
                                continue;
                            }

                            this.ValidateHousingExpenseFieldChange(currentPair.Value, disbProp.Key, disbProp.Value.ReturnType, newValue);
                        }
                    }
                }
            }

            if (validateAdd)
            {
                ValidateCollectionAddition("HousingExpenses");
            }
        }

        /// <summary>
        /// Populates the values using a field.
        /// </summary>
        /// <param name="oldObject">The old object to use.</param>
        /// <param name="newObject">The new object to use.</param>
        /// <param name="fieldInfo">The field to get the value of.</param>
        /// <param name="oldValue">The output old value.</param>
        /// <param name="newValue">The output new value.</param>
        private void PopulateValuesFromFieldInfo(object oldObject, object newObject, FieldInfo fieldInfo, out object oldValue, out object newValue)
        {
            if (fieldInfo.FieldType == typeof(string))
            {
                oldValue = (string)fieldInfo.GetValue(oldObject);
                if (string.IsNullOrEmpty((string)oldValue))
                {
                    oldValue = string.Empty;
                }

                newValue = (string)fieldInfo.GetValue(newObject);
                if (string.IsNullOrEmpty((string)newValue))
                {
                    newValue = string.Empty;
                }
            }
            else
            {
                oldValue = fieldInfo.GetValue(oldObject);
                newValue = fieldInfo.GetValue(newObject);
            }
        }

        /// <summary>
        /// Populates the values using a property.
        /// </summary>
        /// <param name="oldObject">The old object to use.</param>
        /// <param name="newObject">The new object to use.</param>
        /// <param name="methodInfo">The method to get the return value of.</param>
        /// <param name="oldValue">The output old value.</param>
        /// <param name="newValue">The output new value.</param>
        private void PopulateValuesFromMethodInfo(object oldObject, object newObject, MethodInfo methodInfo, out object oldValue, out object newValue)
        {
            if (methodInfo.ReturnType == typeof(string))
            {
                oldValue = (string)methodInfo.Invoke(oldObject, null);
                if (string.IsNullOrEmpty((string)oldValue))
                {
                    oldValue = string.Empty;
                }

                newValue = (string)methodInfo.Invoke(newObject, null);
                if (string.IsNullOrEmpty((string)newValue))
                {
                    newValue = string.Empty;
                }
            }
            else
            {
                oldValue = methodInfo.Invoke(oldObject, null);
                newValue = methodInfo.Invoke(newObject, null);
            }
        }

        /// <summary>
        /// Validates a change in a Housing Expense property/field. 
        /// Will map to a legacy loan file field if possible since those are the ones that can be set in workflow.
        /// </summary>
        /// <param name="calculatedType">The type of the housing expense.</param>
        /// <param name="fieldId">The field id.</param>
        /// <param name="baseType">The return type value.</param>
        /// <param name="newValue">The new value.</param>
        public void ValidateHousingExpenseFieldChange(BaseHousingExpense newExpense, string fieldId, Type baseType, object newValue)
        {
            E_HousingExpenseTypeCalculatedT calculatedType = newExpense.CalculatedHousingExpenseType;

            string fieldIdToUse = fieldId;
            string fieldIdToCheck = fieldId;

            if (newExpense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.LoanValues)
            {
                // These fields only map to a legacy field when the expense is in Calculator mode.
                if (string.Equals(fieldIdToCheck, "AnnualAmtCalcBasePerc", StringComparison.OrdinalIgnoreCase) ||
                    string.Equals(fieldIdToCheck, "MonthlyAmtFixedAmt", StringComparison.OrdinalIgnoreCase) ||
                    string.Equals(fieldIdToCheck, "AnnualAmtCalcBaseType", StringComparison.OrdinalIgnoreCase))
                {
                    fieldIdToCheck += "_Calc";
                }
            }
            else
            {
                // The disbursement amounts affect legacy fields if the expense is in disbursement mode.
                if (string.Equals(fieldIdToCheck, "disbursementAmt", StringComparison.OrdinalIgnoreCase))
                {
                    fieldIdToCheck += "_Disb";
                }
            }

            if (x_LegacyHousingExpenseMappings.ContainsKey(calculatedType) &&
                x_LegacyHousingExpenseMappings[calculatedType].ContainsKey(fieldIdToCheck))
            {
                fieldIdToUse = x_LegacyHousingExpenseMappings[calculatedType][fieldIdToCheck];
            }

            this.ValidateFieldWrite(fieldIdToUse, newValue);
            this.ValidateFieldProtection(fieldIdToUse, baseType, newValue);
        }

        public void ValidateLoanDateInfos(Guid brokerId, string originalJson, string currentJson)
        {
            // Run Path Rules.
            LoanEstimateDatesInfo originalDates = LoanEstimateDatesInfo.FromJson(originalJson, metadata: null) ?? LoanEstimateDatesInfo.Create();
            LoanEstimateDatesInfo updatedDates = LoanEstimateDatesInfo.FromJson(currentJson, metadata: null) ?? LoanEstimateDatesInfo.Create();
            ValidatePathResolvableResult results = ValidatePathResolvable("sLoanEstimateDatesInfo", originalDates, updatedDates, brokerId);

            // Run Closing Cost Rules.
            // NOTE: If there aren't any broken write field rules from path validation then all writes have been allowed
            // by path rules, so we can skip running path validation for coll rules.
            bool skipWriteFieldValidation = results.IsValid_WriteField;
            bool passedWriteFieldValidation;
            ValidateLoanDateInfos_CollRules(originalJson, currentJson, skipWriteFieldValidation, out passedWriteFieldValidation);

            // Add failed write rules from path validation only if "COLL" rules failed write validation.
            if (!passedWriteFieldValidation)
            {
                foreach (FailedRule failedRule in results.FailedWriteFieldRules)
                {
                    SetError(failedRule.FieldId, failedRule.Rule);
                }
            }

            // Always add any failed protect field rules from path validation.
            foreach (FailedRule failedRule in results.FailedProtectFieldRules)
            {
                SetError(failedRule.FieldId, failedRule.Rule);
            }
        }

        public void ValidateLoanDateInfos_CollRules(string originalJson, string currentJson, bool skipWriteFieldValidation, out bool passedWriteFieldValidation)
        {
            passedWriteFieldValidation = true;
            if (originalJson == null || currentJson == null)
            {
                return;
            }

            if (originalJson == string.Empty && currentJson == string.Empty)
            {
                return;
            }

            if (false == this.CheckCollection)
            {
                return;
            }

            if (m_fieldProtectionSet.Count == 0 && m_fieldWriteSet.Count == 0)
            {
                return;
            }

            string collectionName = "sLoanEstimateDatesInfo";

            // We will not provide a way to retrieve archives. All of the data used for workflow
            // should be stored in the DataMember fields/properties of the dates classes. gf opm 242144
            var originalDates = LoanEstimateDatesInfo.FromJson(originalJson, metadata: null)
                ?? LoanEstimateDatesInfo.Create();
            var updatedDates = LoanEstimateDatesInfo.FromJson(currentJson, metadata: null)
                ?? LoanEstimateDatesInfo.Create();

            Dictionary<Guid, LoanEstimateDates> originaDatesDictionary = originalDates.LoanEstimateDatesList.ToDictionary(d => d.UniqueId);
            Dictionary<Guid, LoanEstimateDates> updatedDatesDictionary = updatedDates.LoanEstimateDatesList.ToDictionary(d => d.UniqueId);

            bool validateAdd = false;

            foreach (KeyValuePair<Guid, LoanEstimateDates> updatedPair in updatedDatesDictionary)
            {
                LoanEstimateDates updatedDate = updatedPair.Value;
                LoanEstimateDates originalDate = null;

                if (originaDatesDictionary.ContainsKey(updatedPair.Key))
                {
                    originalDate = originaDatesDictionary[updatedPair.Key];
                }

                // If not in old set, then it was added, so validate add permissions.
                // OPM 460950 - also validate against a blank row
                if (originalDate == null)
                {
                    validateAdd = true;
                    originalDate = LoanEstimateDates.Create(metadata: null);
                }

                object oldValue, newValue;

                foreach (var field in x_availableLoanEstimateDateFields)
                {
                    // The UI seems to remove leading zeroes from the date.
                    if (field.Key.Equals("createdDate"))
                    {
                        oldValue = originalDate.CreatedDate;
                        newValue = updatedDate.CreatedDate;
                    }
                    if (field.Key.Equals("issuedDate"))
                    {
                        oldValue = originalDate.IssuedDate;
                        newValue = updatedDate.IssuedDate;
                    }
                    else if (field.Key.Equals("receivedDate"))
                    {
                        oldValue = originalDate.ReceivedDate;
                        newValue = updatedDate.ReceivedDate;
                    }
                    else if (field.Key.Equals("archiveDate"))
                    {
                        oldValue = originalDate.ArchiveDate;
                        newValue = updatedDate.ArchiveDate;
                    }
                    else
                    {
                        this.PopulateValuesFromFieldInfo(originalDate, updatedDate, field.Value, out oldValue, out newValue);
                    }

                    if (DidObjectChange(oldValue, newValue) == false)
                    {
                        continue; //its okay.
                    }
                    else if (field.Key.Equals("disclosedApr") && originalDates.Version < LoanEstimateDatesInfo.Version201708)
                    {
                        // The Disclosed APR field didn't exist for dates in version 0.
                        // Simply loading the collection can assign a value to the field,
                        // and we don't want to block users simply because a calculated
                        // field that formerly didn't exist now has a value.
                        // If the user changed the archive associated with the date or locked
                        // the disclosed APR then it is a manual modification. Otherwise, we
                        // can let it through.
                        if (originalDate.ArchiveId == updatedDate.ArchiveId
                            && !updatedDate.DisclosedAprLckd)
                        {
                            continue;
                        }
                    }

                    string fieldId = GetCollectionFieldName(collectionName, field.Key);

                    passedWriteFieldValidation &= ValidateFieldWrite(fieldId, newValue);

                    if (!skipWriteFieldValidation)
                    {
                        ValidateFieldProtection(fieldId, field.Value.FieldType, newValue);
                    }
                }
            }

            if (!skipWriteFieldValidation && validateAdd)
            {
                passedWriteFieldValidation &= ValidateCollectionAddition(collectionName);
            }
        }

        public void ValidateClosingDisclosureDates(Guid brokerId, string originalJson, string currentJson)
        {
            // Run Path Rules.
            ClosingDisclosureDatesInfo originalDates = ClosingDisclosureDatesInfo.FromJson(originalJson, metadata: null) ?? ClosingDisclosureDatesInfo.Create();
            ClosingDisclosureDatesInfo updatedDates = ClosingDisclosureDatesInfo.FromJson(currentJson, metadata: null) ?? ClosingDisclosureDatesInfo.Create();
            ValidatePathResolvableResult results = ValidatePathResolvable("sClosingDisclosureDatesInfo", originalDates, updatedDates, brokerId);

            // Run Closing Cost Rules.
            // NOTE: If there aren't any broken write field rules from path validation then all writes have been allowed
            // by path rules, so we can skip running path validation for coll rules.
            bool skipWriteFieldValidation = results.IsValid_WriteField;
            bool passedWriteFieldValidation;
            ValidateClosingDisclosureDates_CollRules(originalJson, currentJson, skipWriteFieldValidation, out passedWriteFieldValidation);

            // Add failed write rules from path validation only if "COLL" rules failed write validation.
            if (!passedWriteFieldValidation)
            {
                foreach (FailedRule failedRule in results.FailedWriteFieldRules)
                {
                    SetError(failedRule.FieldId, failedRule.Rule);
                }
            }

            // Always add any failed protect field rules from path validation.
            foreach (FailedRule failedRule in results.FailedProtectFieldRules)
            {
                SetError(failedRule.FieldId, failedRule.Rule);
            }
        }

        public void ValidateClosingDisclosureDates_CollRules(string originalJson, string currentJson, bool skipWriteFieldValidation, out bool passedWriteFieldValidation)
        {
            passedWriteFieldValidation = true;

            if (originalJson == null || currentJson == null)
            {
                return;
            }

            if (originalJson == string.Empty && currentJson == string.Empty)
            {
                return;
            }

            if (false == this.CheckCollection)
            {
                return;
            }

            if (m_fieldProtectionSet.Count == 0 && m_fieldWriteSet.Count == 0)
            {
                return;
            }

            string collectionName = "sClosingDisclosureDatesInfo";

            // We will not provide a way to retrieve archives. All of the data used for workflow
            // should be stored in the DataMember fields/properties of the dates classes. gf opm 242144
            var originalDates = ClosingDisclosureDatesInfo.FromJson(originalJson, metadata: null)
                ?? ClosingDisclosureDatesInfo.Create();
            var updatedDates = ClosingDisclosureDatesInfo.FromJson(currentJson, metadata: null)
                ?? ClosingDisclosureDatesInfo.Create();

            Dictionary<Guid, ClosingDisclosureDates> originalDatesDictionary = originalDates.ClosingDisclosureDatesList.ToDictionary(d => d.UniqueId);
            Dictionary<Guid, ClosingDisclosureDates> updatedDatesDictionary = updatedDates.ClosingDisclosureDatesList.ToDictionary(d => d.UniqueId);

            bool validateAdd = false;

            foreach (KeyValuePair<Guid, ClosingDisclosureDates> updatedPair in updatedDatesDictionary)
            {
                ClosingDisclosureDates updatedDate = updatedPair.Value;
                ClosingDisclosureDates originalDate = null;

                if (originalDatesDictionary.ContainsKey(updatedPair.Key))
                {
                    originalDate = originalDatesDictionary[updatedPair.Key];
                }

                // If not in the old dictionary, then it was added so check add permissions.
                // OPM 460950 - also validate against a blank row
                if (originalDate == null)
                {
                    validateAdd = true;
                    originalDate = ClosingDisclosureDates.Create(metadata: null);
                }

                object oldValue, newValue;

                foreach (var field in x_availableClosingDisclosureDateFields)
                {
                    // The UI seems to remove leading zeroes from the date.
                    if (field.Key.Equals("createdDate"))
                    {
                        oldValue = originalDate.CreatedDate;
                        newValue = updatedDate.CreatedDate;
                    }
                    else if (field.Key.Equals("issuedDate"))
                    {
                        oldValue = originalDate.IssuedDate;
                        newValue = updatedDate.IssuedDate;
                    }
                    else if (field.Key.Equals("receivedDate"))
                    {
                        oldValue = originalDate.ReceivedDate;
                        newValue = updatedDate.ReceivedDate;
                    }
                    else if (field.Key.Equals("archiveDate"))
                    {
                        oldValue = originalDate.ArchiveDate;
                        newValue = updatedDate.ArchiveDate;
                    }
                    else if (field.Key.Equals("postConsummationKnowledgeOfEventDate"))
                    {
                        oldValue = originalDate.PostConsummationKnowledgeOfEventDate;
                        newValue = updatedDate.PostConsummationKnowledgeOfEventDate;
                    }
                    else if (field.Key.Equals("postConsummationRedisclosureReasonDate"))
                    {
                        oldValue = originalDate.PostConsummationRedisclosureReasonDate;
                        newValue = updatedDate.PostConsummationRedisclosureReasonDate;
                    }
                    else
                    {
                        this.PopulateValuesFromFieldInfo(originalDate, updatedDate, field.Value, out oldValue, out newValue);
                    }

                    if (DidObjectChange(oldValue, newValue) == false)
                    {
                        continue; //its okay.
                    }
                    else if (field.Key.Equals("disclosedApr") && originalDates.Version < ClosingDisclosureDatesInfo.Version201708)
                    {
                        // The Disclosed APR field didn't exist for dates in version 0.
                        // Simply loading the collection can assign a value to the field,
                        // and we don't want to block users simply because a calculated
                        // field that formerly didn't exist now has a value.
                        // If the user changed the archive associated with the date or locked
                        // the disclosed APR then it is a manual modification. Otherwise, we
                        // can let it through.
                        if (originalDate.ArchiveId == updatedDate.ArchiveId
                            && !updatedDate.DisclosedAprLckd)
                        {
                            continue;
                        }
                    }

                    string fieldId = GetCollectionFieldName(collectionName, field.Key);

                    if (!skipWriteFieldValidation)
                    {
                        passedWriteFieldValidation &= ValidateFieldWrite(fieldId, newValue);
                    }

                    ValidateFieldProtection(fieldId, field.Value.FieldType, newValue);
                }
            }

            if (!skipWriteFieldValidation && validateAdd)
            {
                passedWriteFieldValidation &= ValidateCollectionAddition(collectionName);
            }
        }

        public void ValidateClosingCostSet(Guid brokerId, string collectionName, string originalJson, string currentJson)
        {
            // Run Path Rules.
            ValidatePathResolvableResult results;
            if (collectionName == "sClosingCostSet")
            {
                 results = ValidatePathResolvable("borrowerclosingcostset", new BorrowerClosingCostSet(originalJson), new BorrowerClosingCostSet(currentJson), brokerId);
            }
            else
            {
                results = ValidatePathResolvable("sellerclosingcostset", new SellerClosingCostSet(originalJson), new SellerClosingCostSet(currentJson), brokerId);
            }

            // Run Closing Cost Rules.
            // NOTE: If there aren't any broken write field rules from path validation then all writes have been allowed
            // by path rules, so we can skip running path validation for coll rules.
            bool skipWriteFieldValidation = results.IsValid_WriteField;
            bool passedWriteFieldValidation;
            ValidateClosingCostSet_CollRules(collectionName, originalJson, currentJson, skipWriteFieldValidation, out passedWriteFieldValidation);

            // Add failed write rules from path validation only if "COLL" rules failed write validation.
            if (!passedWriteFieldValidation)
            {
                foreach (FailedRule failedRule in results.FailedWriteFieldRules)
                {
                    SetError(failedRule.FieldId, failedRule.Rule);
                }
            }

            // Always add any failed protect field rules from path validation.
            foreach (FailedRule failedRule in results.FailedProtectFieldRules)
            {
                SetError(failedRule.FieldId, failedRule.Rule);
            }
        }

        public void ValidateClosingCostSet_CollRules(string collectionName, string originalJson, string currentJson, bool skipWriteFieldValidation, out bool passedWriteFieldValidation)
        {
            passedWriteFieldValidation = true;

            if (originalJson == null || currentJson == null)
            {
                return;
            }

            if (false == this.CheckCollection)
            {
                return;
            }

            if (m_fieldProtectionSet.Count == 0 && m_fieldWriteSet.Count == 0)
            {
                return;
            }

            LoanClosingCostSet originalSet;
            LoanClosingCostSet updatedSet;
            Func<BaseClosingCostFee, bool> setFilter;

            if (collectionName == "sClosingCostSet")
            {
                originalSet = new BorrowerClosingCostSet(originalJson);
                updatedSet = new BorrowerClosingCostSet(currentJson);

                // opm 451523 ejm - The Discount Points is not filtered anymore in the UI so it should be OK to not filter it here.
                setFilter = fee => ClosingCostSetUtils.PrepsSecGMIFilter((BorrowerClosingCostFee)fee) && fee.ClosingCostFeeTypeId != DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId;
            }
            else
            {
                setFilter = null;
                originalSet = new SellerClosingCostSet(originalJson);
                updatedSet = new SellerClosingCostSet(currentJson);
            }

            Dictionary<Guid, LoanClosingCostFee> originalFees = originalSet.GetFees(setFilter).Cast<LoanClosingCostFee>().ToDictionary(p => p.UniqueId);

            foreach (LoanClosingCostFee updatedFee in updatedSet.GetFees(setFilter))
            {
                LoanClosingCostFee originalFee;

                // the original set doesnt have this fee it is new.
                if (originalFees.TryGetValue(updatedFee.UniqueId, out originalFee))
                {
                    originalFees.Remove(updatedFee.UniqueId);
                }
                else
                {
                    if (!skipWriteFieldValidation)
                    {
                        passedWriteFieldValidation &= this.CanAddToCollection(collectionName);
                    }
                    continue;
                }

                if (collectionName == "sClosingCostSet")
                {
                    this.ValidateClosingCostFee(collectionName, originalFee, updatedFee, LoanFileFieldValidator.x_availableBorrowerClosingCostFields, LoanFileFieldValidator.x_availableBorrowerClosingCostProperties, skipWriteFieldValidation, ref passedWriteFieldValidation);
                }
                else
                {
                    this.ValidateClosingCostFee(collectionName, originalFee, updatedFee, LoanFileFieldValidator.x_availableSellerClosingCostFields, LoanFileFieldValidator.x_availableSellerClosingCostProperties, skipWriteFieldValidation, ref passedWriteFieldValidation);
                }
            }

            if (originalFees.Count > 0 && !skipWriteFieldValidation)
            {
                passedWriteFieldValidation &= ValidateCollectionDeletion(collectionName);
            }
        }

        private void ValidateClosingCostFee(string collectionName, LoanClosingCostFee originalFee, LoanClosingCostFee updatedFee, Dictionary<string, FieldInfo> fieldsToUse, Dictionary<string, MethodInfo> propertiesToUse, bool skipWriteFieldValidation, ref bool passedWriteFieldValidation)
        {
            List<KeyValuePair<string, MethodInfo>> paymentProperties = new List<KeyValuePair<string, MethodInfo>>();
            List<KeyValuePair<string, FieldInfo>> paymentFields = new List<KeyValuePair<string, FieldInfo>>();

            foreach (var entry in propertiesToUse)
            {
                if (entry.Key.StartsWith("Payment-"))
                {
                    paymentProperties.Add(entry);
                    continue;
                }

                object val1 = entry.Value.Invoke(originalFee, null);
                object val2 = entry.Value.Invoke(updatedFee, null);

                if (false == DidObjectChange(val1, val2))
                {
                    continue;  //cell is identical so its okay
                }

                string fieldId = GetCollectionFieldName(collectionName, entry.Key);
                fieldId = Regex.Replace(fieldId, "[_]rep$", "", RegexOptions.Singleline | RegexOptions.Compiled);

                if (!skipWriteFieldValidation)
                {
                    passedWriteFieldValidation &= ValidateFieldWrite(fieldId, val2);
                }

                ValidateFieldProtection(fieldId, entry.Value.ReturnType, val2);
            }

            foreach (var entry in fieldsToUse)
            {
                if (entry.Key.StartsWith("Payment-"))
                {
                    paymentFields.Add(entry);
                    continue;
                }

                object val1 = entry.Value.GetValue(originalFee);
                object val2 = entry.Value.GetValue(updatedFee);

                if (false == DidObjectChange(val1, val2))
                {
                    continue;  //cell is identical so its okay
                }

                string fieldId = GetCollectionFieldName(collectionName, entry.Key);
                fieldId = Regex.Replace(fieldId, "[_]rep$", "", RegexOptions.Singleline | RegexOptions.Compiled);

                if (!skipWriteFieldValidation)
                {
                    passedWriteFieldValidation &= ValidateFieldWrite(fieldId, val2);
                }

                ValidateFieldProtection(fieldId, entry.Value.GetType(), val2);
            }

            //ignore the system generated payments

            Dictionary<Guid, LoanClosingCostFeePayment> originalPayments = originalFee.Payments.Where(p => !p.IsSystemGenerated).ToDictionary(p => p.Id);
            Dictionary<Guid, LoanClosingCostFeePayment> existingPayments = updatedFee.Payments.Where(p => !p.IsSystemGenerated).ToDictionary(p => p.Id);


            foreach (var originalPayment in originalPayments)
            {

                LoanClosingCostFeePayment existingPayment = null;

                if (!existingPayments.TryGetValue(originalPayment.Key, out existingPayment))
                {
                    if (!HasFullWrite && !skipWriteFieldValidation && !m_canDeleteClosingCostPayments && !originalPayment.Value.IsSystemGenerated)
                    {
                        passedWriteFieldValidation = false;
                        SetError(collectionName + "|PaymentDelete", null);  //cannot delete payments
                    }
                    continue;
                }

                existingPayments.Remove(originalPayment.Key);

                if (existingPayment.IsSystemGenerated)
                {
                    continue;
                }

                foreach (var attribute in paymentProperties)
                {
                    object originalValue = attribute.Value.Invoke(originalPayment.Value, null);
                    object newValue = attribute.Value.Invoke(existingPayment, null);

                    if (false == DidObjectChange(originalValue, newValue))
                    {
                        continue;
                    }

                    string fieldId = GetCollectionFieldName(collectionName, attribute.Key);
                    ValidateFieldProtection(fieldId, attribute.Value.ReturnType, newValue);

                    if (!skipWriteFieldValidation)
                    {
                        passedWriteFieldValidation &= ValidateFieldWrite(fieldId, newValue);
                    }
                }

                foreach (var field in paymentFields)
                {
                    object originalValue = field.Value.GetValue(originalPayment.Value);
                    object newValue = field.Value.GetValue(existingPayment);

                    if (false == DidObjectChange(originalValue, newValue))
                    {
                        continue;
                    }

                    string fieldId = GetCollectionFieldName(collectionName, field.Key);
                    ValidateFieldProtection(fieldId, field.Value.GetType(), newValue);

                    if (!skipWriteFieldValidation)
                    {
                        passedWriteFieldValidation &= ValidateFieldWrite(fieldId, newValue);
                    }
                }
            }

            if (!HasFullWrite && !skipWriteFieldValidation && existingPayments.Count > 0 && !m_canDeleteClosingCostPayments)
            {
                passedWriteFieldValidation = false;
                SetError(collectionName + "PaymentAdd", null);
            }
        }

        /// <summary>
        /// Validates changes made to an IPathResolvable object using schema.
        /// </summary>
        /// <param name="path">Path to the element being checked.</param>
        /// <param name="original">The original state of the IPathResolvable object.</param>
        /// <param name="current">The current state of the IPathResolvable object.</param>
        /// <param name="brokerId">Broker ID.</param>
        private ValidatePathResolvableResult ValidatePathResolvable(string path, IPathResolvable original, IPathResolvable current, Guid brokerId)
        {
            // Add path header if missing.
            if (!path.StartsWith(DataPath.PathHeader, StringComparison.OrdinalIgnoreCase))
            {
                path = DataPath.PathHeader + path;
            }

            List<string> pathList = new List<string>();
            pathList.Add(path);
            return ValidatePathResolvableImpl(pathList, original, current, brokerId);
        }

        /// <summary>
        /// Validates changes made to an IPathResolvable object using schema.
        /// </summary>
        /// <param name="pathList">List of possible paths leading to the element being checked.</param>
        /// <param name="original">The original state of the IPathResolvable object.</param>
        /// <param name="current">The current state of the IPathResolvable object.</param>
        /// <param name="brokerId">Broker ID.</param>
        /// <returns><see cref="ValidatePathResolvableResult"/>.</returns>
        private ValidatePathResolvableResult ValidatePathResolvableImpl(List<string> pathList, IPathResolvable original, IPathResolvable current, Guid brokerId)
        {
            // Return empty (passing) validation results if validation is disabled.
            if (false == this.CheckCollection)
            {
                return new ValidatePathResolvableResult();
            }

            // Return empty (passing) validation results if there are no workflow rules to validate against.
            if (m_fieldProtectionSet.Count == 0 && m_fieldWriteSet.Count == 0)
            {
                return new ValidatePathResolvableResult();
            }

            IPathableRecordSchema schema = PathSchemaManager.GetSchemaByType(current.GetType());

            // If schema is not found, we called the wrong validator. Throw.
            if (schema == null)
            {
                throw new CBaseException(ErrorMessages.Generic, $"[ValidatePathResolvable] Could not find schema for type=[{current.GetType()}].");
            }

            
            if (schema is IPathableCollectionSchema)
            {
                return ValidatePathableCollection(pathList, (IPathableCollectionSchema)schema, original, current, brokerId);
            }
            else
            {
                return ValidatatePathableRecordProperties(pathList, schema, original, current, brokerId);
            }
        }

        /// <summary>
        /// Validates changes made to a pathable collection.
        /// </summary>
        /// <param name="pathList">List of possible paths leading to the collection being checked.</param>
        /// <param name="schema">The schema for the collection.</param>
        /// <param name="originalCollection">The original state of the IPathResolvable object.</param>
        /// <param name="currentCollection">The current state of the IPathResolvable object.</param>
        /// <param name="brokerId">Broker ID.</param>
        /// <returns><see cref="ValidatePathResolvableResult"/>.</returns>
        private ValidatePathResolvableResult ValidatePathableCollection(List<string> pathList, IPathableCollectionSchema schema, IPathResolvable originalCollection, IPathResolvable currentCollection, Guid brokerId)
        {
            ValidatePathResolvableResult results = new ValidatePathResolvableResult();

            // Get all records for original and current collection.
            IEnumerable<IPathResolvable> originalRecords = originalCollection == null ? Enumerable.Empty<IPathResolvable>() : schema.GetAllRecordsFunc(originalCollection);
            IEnumerable<IPathResolvable> currentRecords = schema.GetAllRecordsFunc(currentCollection);
            
            foreach (IPathResolvable currentRecord in currentRecords)
            {
                IPathResolvable originalRecord = originalRecords.FirstOrDefault(o => string.Equals(schema.RecordIdGetter(o), schema.RecordIdGetter(currentRecord), StringComparison.OrdinalIgnoreCase));

                // Check if record was added.
                if (originalRecord == null)
                {
                    // If so, check if adding record is allowed.
                    List<string> addPathList = GeneratePossiblePaths(pathList, schema, currentRecord, "add");
                    results.Merge(ValidatePathList(addPathList, value: null, testRuleExistanceOnly: true));

                    // Set original record to default, so we can compare default record values against the current record.
                    originalRecord = schema.GetDefaultRecord(brokerId, schema.RecordIdGetter(currentRecord), originalCollection);
                }

                // Validate individual record property values.
                results.Merge(ValidatatePathableRecordProperties(pathList, schema, originalRecord, currentRecord, brokerId));
            }

            // Check if records have been removed and if removing records is allowed.
            HashSet<string> currentIds = new HashSet<string>(currentRecords.Select(rec => schema.RecordIdGetter(rec)), StringComparer.OrdinalIgnoreCase);
            IEnumerable<IPathResolvable> removedRecords = originalRecords.Where(rec => !currentIds.Contains(schema.RecordIdGetter(rec)));
            foreach (IPathResolvable removed in removedRecords)
            {
                List<string> removePathList = GeneratePossiblePaths(pathList, schema, removed, "remove");
                results.Merge(ValidatePathList(removePathList, value: null, testRuleExistanceOnly: true));
            }

            return results;
        }

        /// <summary>
        /// Generates a list of possible paths to the passed in parameter by extending the list of possible paths to it's parent element (the passed in record).
        /// </summary>
        /// <param name="pathList">List of possible paths leading to the given record.</param>
        /// <param name="schema">The schema for the record.</param>
        /// <param name="record">The IPathResolvable record.</param>
        /// <param name="parameterName">The name of the parameter we are generating paths to.</param>
        /// <returns>A list of possible paths to the parameter.</returns>
        private List<string> GeneratePossiblePaths(List<string> pathList, IPathableRecordSchema schema, IPathResolvable record, string parameterName)
        {
            bool isCollection = schema is IPathableCollectionSchema;
            List<string> newPathList = new List<string>();
            foreach (string path in pathList)
            {
                if (isCollection)
                {
                    IPathableCollectionSchema collectionSchema = (IPathableCollectionSchema)schema;

                    // Generate collection level path.
                    newPathList.Add($"{path}[{DataPath.WholeCollectionIdentifier}].{parameterName}");

                    // Generate subset level paths.
                    IEnumerable<PathableSubsetInfo> subsets = collectionSchema.GetSubsets().Values.Where(info => info.Filter(record));
                    foreach (PathableSubsetInfo subset in subsets)
                    {
                        newPathList.Add($"{path}[{DataPath.SubsetMarker}{subset.Name}].{parameterName}");
                    }

                    // Generate record level path.
                    newPathList.Add($"{path}[{collectionSchema.RecordIdGetter(record)}].{parameterName}");
                }
                else
                {
                    // If the parent node in the path is not a collection, then there is only a single path to the given property.
                    newPathList.Add($"{path}.{parameterName}");
                }
            }

            return newPathList;
        }

        /// <summary>
        /// Validates changes made to a pathable record by validating any changes made to any of that record's properties.
        /// </summary>
        /// <param name="pathList">List of possible paths leading to the record.</param>
        /// <param name="schema">The schema for the record.</param>
        /// <param name="originalRecord">The original state of the IPathResolvable record.</param>
        /// <param name="currentRecord">The current state of the IPathResolvable record.</param>
        /// <param name="brokerId">Broker ID.</param>
        /// <returns><see cref="ValidatePathResolvableResult"/>.</returns>
        private ValidatePathResolvableResult ValidatatePathableRecordProperties(List<string> pathList, IPathableRecordSchema schema, IPathResolvable originalRecord, IPathResolvable currentRecord, Guid brokerId)
        {
            ValidatePathResolvableResult results = new ValidatePathResolvableResult();

            // Check each property for change.
            foreach (PathableFieldInfo fieldInfo in schema.GetFields().Values)
            {
                object originalProperty = fieldInfo.Getter(originalRecord);
                object currentProperty = fieldInfo.Getter(currentRecord);

                // If property is a pathable record or collection, call ValidatePathResolvable recursively.
                if (fieldInfo.ParameterType == SecurityParameter.SecurityParameterType.PathableRecord
                    || fieldInfo.ParameterType == SecurityParameter.SecurityParameterType.PathableCollection)
                {
                    List<string> recursePathList = GeneratePossiblePaths(pathList, schema, currentRecord, fieldInfo.Name);
                    results.Merge(ValidatePathResolvableImpl(recursePathList, (IPathResolvable)originalProperty, (IPathResolvable)currentProperty, brokerId));

                    continue;
                }

                // For all other properties, check if value changed.
                if (!DidObjectChange(originalProperty, currentProperty))
                {
                    continue;   // Value didn't change, so no need to validate.
                }

                // If property did change, then validate path to property.
                // NOTE: We only validate the list of paths once we reach a property that can't be expanted on further (ie. Isn't IPathResolvable).
                List<string> propPathList = GeneratePossiblePaths(pathList, schema, currentRecord, fieldInfo.Name);
                results.Merge(ValidatePathList(propPathList, value: currentProperty, testRuleExistanceOnly: false));
            }

            return results;
        }

        /// <summary>
        /// Validates a list of paths and the given value against workflow Write Field and Protect Field rules.
        /// </summary>
        /// <param name="pathList">The list of paths to validate.</param>
        /// <param name="value">The value found at the end of those paths.</param>
        /// <param name="testRuleExistanceOnly">If true, only validates against workflow rule existance, NOT against individual rule settings (e.g. CanBeCleared, etc.)</param>
        /// <returns><see cref="ValidatePathResolvableResult"/>.</returns>
        private ValidatePathResolvableResult ValidatePathList(List<string> pathList, object value, bool testRuleExistanceOnly)
        {
            ValidatePathResolvableResult results = new ValidatePathResolvableResult();

            bool skipWriteFieldValidation = false;
            foreach (string path in pathList)
            {
                ValidatePathResolvableResult pathResults = ValidatePath(path, value, testRuleExistanceOnly, skipWriteFieldValidation);

                // Write validation passes if it passes for any of the paths being validated.
                // And any prior broken validation rules from other paths can be considered invalid and should be cleared out.
                if (!skipWriteFieldValidation && pathResults.IsValid_WriteField)
                {
                    skipWriteFieldValidation = true;
                    results.FailedWriteFieldRules.Clear();
                }

                results.Merge(pathResults);
            }

            return results;
        }

        /// <summary>
        /// Validates a path and a given value against workflow Write Field and Protect Field rules.
        /// </summary>
        /// <param name="path">The path to validate.</param>
        /// <param name="value">The value found at that path.</param>
        /// <param name="testRuleExistanceOnly">If true, only validates against workflow rule existance, NOT against individual rule settings (e.g. CanBeCleared, etc.)</param>
        /// <param name="skipWriteFieldValidation">If true, does not validate against write field rules.</param>
        /// <returns><see cref="ValidatePathResolvableResult"/>.</returns>
        private ValidatePathResolvableResult ValidatePath(string path, object value, bool testRuleExistanceOnly, bool skipWriteFieldValidation)
        {
            ValidatePathResolvableResult results = new ValidatePathResolvableResult();

            string pathSoFar = string.Empty;
            string[] splitPath = path.Split('.');

            // Vaidate write and protect field rules for each step along the path.
            foreach(string pathNode in splitPath)
            {
                if (!string.IsNullOrEmpty(pathSoFar))
                {
                    pathSoFar += ".";
                }

                pathSoFar += pathNode;

                // If write field validation passes, we no longer need to check for validation further down the path.
                // And any prior broken validation rules (from higher up the path) can be considered invalid.
                // In otherwords, if we pass write field validation at ANY point along the path, we can consider the path valid for write field privleges.
                if (!skipWriteFieldValidation && ValidatePath_WriteField(pathSoFar, value, testRuleExistanceOnly, results.FailedWriteFieldRules))
                {
                    skipWriteFieldValidation = true;
                    results.FailedWriteFieldRules.Clear();
                }

                // Protect Field validation must be run for all points along the path. As opposed to Write Field validation,
                // Protect Field validation fails if validation fails at ANY point along the path.
                ValidatePath_ProtectField(pathSoFar, value, testRuleExistanceOnly, results.FailedProtectFieldRules);
            }

            return results;
        }

        /// <summary>
        /// Helper function used to check for workflow Write: Field Level rules for a given path and property value.
        /// </summary>
        /// <param name="path">The path used to look up workflow rules.</param>
        /// <param name="value">The value used to test against any found rules.</param>
        /// <param name="passOnRuleExistance">If true, validation passes if any write field rules for the path are found. Rules are not validated in this case.</param>
        /// <param name="failedRules">A list of faild rules to add the found rule to if it fails validation.</param>
        /// <returns>True, if valid workflow rule is found. Otherwise, false.</returns>
        private bool ValidatePath_WriteField(string path, object value, bool passOnRuleExistance, List<FailedRule> failedRules)
        {
            // If no write field rules exists, or if the user has full write access
            // then this check passes automatically. User has write access.
            if (this.HasFullWrite || m_fieldWriteSet.Count == 0)
            {
                return true;
            }

            WriteFieldRule writeFieldRule;
            if (m_fieldWriteSet.TryGetValue(path, out writeFieldRule))
            {
                if (passOnRuleExistance || ValidateValueAgainstWriteFieldRule(writeFieldRule, value))
                {
                    return true;
                }

                // If we got here, then the rule was not valid against the given value. Add to failure set.
                failedRules.Add(new FailedRule(writeFieldRule.FieldId, writeFieldRule));
                return false;
            }

            // If we got here, then no valid write rules were found, so we failed validation.
            // Add a dummy rule to the failed set (because no rule exists for this path).
            failedRules.Add(new FailedRule(path, m_fieldWriteSet.Values.First()));
            return false;
        }

        /// <summary>
        /// Helper function used to check for workflow Protect: Field Level rules for a given path and property value.
        /// </summary>
        /// <param name="path">The path used to look up workflow rules.</param>
        /// <param name="value">The value used to test against any found rules.</param>
        /// <param name="failOnRuleExistance">If true, validation fails if any protect field rules for the path are found. Rules are not validated in this case.</param>
        /// <param name="failedRules">A list of faild rules to add the found rule to if it fails validation.</param>
        private void ValidatePath_ProtectField(string path, object value, bool failOnRuleExistance, List<FailedRule> failedRules)
        {
            // If no protect field rules exists then this check passes automatically.
            if (m_fieldProtectionSet.Count == 0)
            {
                return;
            }

            IList<ProtectFieldRule> protectedFieldRules;
            if (m_fieldProtectionSet.TryGetValue(path, out protectedFieldRules))
            {
                foreach (ProtectFieldRule fieldRule in protectedFieldRules)
                {
                    if (failOnRuleExistance || !ValidateValueAgainstProtectFieldRule(fieldRule, value))
                    {
                        failedRules.Add(new FailedRule(fieldRule.FieldId, fieldRule));
                    }
                }
            }
        }

        /// <summary>
        /// Validates a value against a workflow Write: Field Level rule.
        /// </summary>
        /// <param name="writeFieldRule">Rule to validate against.</param>
        /// <param name="value">Value to validate.</param>
        /// <returns>True, if value is valid. Otherwise, false.</returns>
        private bool ValidateValueAgainstWriteFieldRule(WriteFieldRule writeFieldRule, object value)
        {
            if (!writeFieldRule.CanBeCleared && string.IsNullOrEmpty(value.ToString()))
            {
                return false;
            }

            if (writeFieldRule.SpecificValues.Count > 0 && value.GetType().IsEnum)
            {
                Enum enumValue = value as Enum;
                if (enumValue != null && !writeFieldRule.SpecificValues.Contains(enumValue.ToString("D")))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Validates a value against a workflow Protect: Field Level rule.
        /// </summary>
        /// <param name="writeFieldRule">Rule to validate against.</param>
        /// <param name="value">Value to validate.</param>
        /// <returns>True, if value is valid. Otherwise, false.</returns>
        private bool ValidateValueAgainstProtectFieldRule(ProtectFieldRule protectedFieldRule, object newValue)
        {
            if (protectedFieldRule.SpecificValues.Count > 0 && newValue.GetType().IsEnum)
            {
                Enum enumValue = newValue as Enum;
                if (enumValue != null && protectedFieldRule.SpecificValues.Contains(enumValue.ToString("D")))
                {
                    return false;
                }
            }
            else if (!protectedFieldRule.CanBeUpdated)
            {
                return false;
            }

            if (string.IsNullOrEmpty(newValue.ToString()))
            {
                return false;
            }

            return true;
        }

        private class FailedRule
        {
            public FailedRule(string fieldId, FieldRule rule)
            {
                this.FieldId = fieldId;
                this.Rule = rule;
            }

            public string FieldId { get; }
            public FieldRule Rule { get; }
        }

        private class ValidatePathResolvableResult
        {
            public List<FailedRule> FailedWriteFieldRules { get; } = new List<FailedRule>();
            public List<FailedRule> FailedProtectFieldRules { get; } = new List<FailedRule>();

            public bool IsValid_WriteField
            {
                get
                {
                    return !this.FailedWriteFieldRules.Any();
                }
            }

            public bool IsValid_ProtectField
            {
                get
                {
                    return !this.FailedProtectFieldRules.Any();
                }
            }

            public bool IsValid
            {
                get
                {
                    return this.IsValid_WriteField && this.IsValid_ProtectField;
                }
            }

            public void Merge(ValidatePathResolvableResult other)
            {
                this.FailedWriteFieldRules.AddRange(other.FailedWriteFieldRules);
                this.FailedProtectFieldRules.AddRange(other.FailedProtectFieldRules);
            }
        }

        public void UpdateCollection(string collectionName, DataSet originalDataSet, DataSet updatedDataSet)
        {
            if (false == CheckCollection)
            {
                return;
            }
            if (m_fieldProtectionSet.Count == 0 && m_fieldWriteSet.Count == 0 )
            {
                return;
            }

            if (updatedDataSet.Tables.Count > 1 || originalDataSet.Tables.Count > 1)
            {
                Tools.LogWarning("Collection data " + collectionName + " has multiple tables. Code assumes 1. Not doing field checks");
            }

            DataTable updatedTable = updatedDataSet.Tables[0];
            Dictionary<string, DataRow> origionalRows = new Dictionary<string, DataRow>(StringComparer.OrdinalIgnoreCase);

            foreach (DataRow row in originalDataSet.Tables[0].Rows)
            {
                object recordId = row["RecordId"];
                if (recordId == null)
                {
                    Tools.LogWarning("Invalid row no record id -- Skipping protection. " + collectionName);
                    continue;
                }

                origionalRows.Add(recordId.ToString(), row);
            }

            
            for (int rowIndex = 0; rowIndex < updatedTable.Rows.Count; rowIndex++)
            {
                DataRow newRow = updatedTable.Rows[rowIndex];
                object recordId = newRow["RecordId"];
                if (recordId == null)  //should not happen
                {
                    Tools.LogInfo("No record id for  row in " + collectionName);
                    return;
                }

                DataRow unmodifiedRow;
                if (false == origionalRows.TryGetValue(recordId.ToString(), out unmodifiedRow))
                {
                    ValidateCollectionAddition(collectionName);
                    continue; 
                }
                origionalRows.Remove(recordId.ToString());

                //check to see if the row being updated was new before and is being updated for the first time.
                //if so most fields will be empty    -- this check should only apply when you have field write or can full write 
                if (IsEmptyRow(collectionName, updatedTable.Columns, unmodifiedRow) && CanAddToCollection(collectionName))
                {
                    continue; 
                }
                foreach (DataColumn dc in updatedTable.Columns)
                {
                    if (false == DidObjectChange(unmodifiedRow[dc.ColumnName], newRow[dc.ColumnName]))
                    {
                        continue;  //cell is identical so its okay
                    }

                    string fieldId = GetCollectionFieldName(collectionName, dc.ColumnName);
                    ValidateFieldWrite(fieldId, newRow[dc.ColumnName]);
                    ValidateFieldProtection(fieldId, dc.DataType.BaseType, newRow[dc.ColumnName]);
                }
            }

            //some of the rows were deleted
            if (origionalRows.Values.Count > 0)
            {
                ValidateCollectionDeletion(collectionName);
            }
        }

        private bool IsEmptyRow(string collection, DataColumnCollection columns, DataRow unmodifiedRow)
        {
            foreach (DataColumn column in columns)
            {
                if (column.ColumnName.Equals("RecordId", StringComparison.OrdinalIgnoreCase) || 
                    column.ColumnName.Equals("OrderRankValue", StringComparison.OrdinalIgnoreCase) || 
                    IsFieldSetOnInsertion(collection, column.ColumnName))   //some fields are set when creating the records so these should not count 
                {
                    continue;
                }

                if (unmodifiedRow[column.ColumnName] == null || unmodifiedRow[column.ColumnName].ToString().TrimWhitespaceAndBOM() == "")
                {
                    continue;
                }

                //Tools.LogError(collection + "_" + column.ColumnName + " val " + unmodifiedRow[column.ColumnName] + " " + unmodifiedRow["RecordId"]);
                return false;
            }

            return true;
        }
        private bool DidObjectChange(object original, object newOb, Func<object, object, bool> specialComparer = null)
        {
            if (original == null && newOb == null)
            {
                return false;
            }

            if (original == null || newOb == null)
            {
                return true;
            }

            if (specialComparer != null)
            {
                if (specialComparer(original, newOb))
                {
                    return false;
                }
            }
            else
            {
                if (original.Equals(newOb))
                {
                    return false;
                }
            }

            if ((original == null || original.ToString().TrimWhitespaceAndBOM() == "") && (newOb == null || newOb.ToString().TrimWhitespaceAndBOM() == ""))
            {
                return false;
            }

            return true;
        }

        private string GetCollectionFieldName(string collectionName, string columnName)
        {
            return string.Format("coll|{0}|{1}", collectionName, columnName);
        }
        private bool ValidateFieldWrite(string fieldId, object newValue)
        {
            if (m_fieldWriteSet.Count == 0)
            {
                return true;
            }

            WriteFieldRule writeField;
            if (false == m_fieldWriteSet.TryGetValue(fieldId, out writeField))
            {
                //take the first field write rule as the reason.
                SetError(fieldId, m_fieldWriteSet.Values.First());
                return false;
            }

            if (false == writeField.CanBeCleared && string.IsNullOrEmpty(newValue.ToString()))
            {
                SetError(fieldId, writeField);
                return false;
            }

            if (writeField.SpecificValues.Count > 0)
            {
                Enum val = newValue as Enum;
                if (null != val && false == writeField.SpecificValues.Contains(val.ToString("D")))
                {
                    SetError(fieldId, writeField);
                    return false;
                }
            }

            return true;
        }

        private void ValidateFieldProtection(string fieldId, Type baseType, object newValue)
        {
            IList<ProtectFieldRule> protectedFieldRules;
            if (false == m_fieldProtectionSet.TryGetValue(fieldId, out protectedFieldRules))
            {
                return;
            }

            foreach (ProtectFieldRule fieldRule in protectedFieldRules)
            {
                if (fieldRule.SpecificValues.Count > 0 && baseType == typeof(Enum))
                {
                    Enum val = newValue as Enum;
                    if (null != val && fieldRule.SpecificValues.Contains(val.ToString("D")))
                    {
                        SetError(fieldId, fieldRule);
                        return;
                    }
                }
                else if (false == fieldRule.CanBeUpdated)
                {
                    SetError(fieldId, fieldRule);
                    return;
                }

                if (string.IsNullOrEmpty(newValue.ToString()))
                {
                    SetError(fieldId, fieldRule);
                    return;
                }
            }
        }

        private string GetDataBaseId(string field)
        {
            return Regex.Replace(field, "[_]rep$", "", RegexOptions.Singleline | RegexOptions.Compiled);

        }
        private static bool WasCleared(E_PageDataFieldType fieldType, string value)
        {
            switch (fieldType)
            {
                case E_PageDataFieldType.DateTime:
                case E_PageDataFieldType.String:
                case E_PageDataFieldType.Guid:
                case E_PageDataFieldType.Ssn:
                case E_PageDataFieldType.Bool:
                case E_PageDataFieldType.Enum:
                case E_PageDataFieldType.Unknown: // SK - there are none of these as of 5/7/2012
                case E_PageDataFieldType.Phone:
                    return string.IsNullOrEmpty(value);
                case E_PageDataFieldType.Decimal:
                    {
                        decimal result;
                        if (!Decimal.TryParse(value, out result))
                        {
                            Tools.LogBug("Couldn't parse " + value + " as " + E_PageDataFieldType.Decimal.ToString("G"));
                            return false;
                        }
                        return result == 0;
                    }
                case E_PageDataFieldType.Integer:
                    {
                        int result;
                        if (!Int32.TryParse(value, out result))
                        {
                            Tools.LogBug("Couldn't parse " + value + " as " + E_PageDataFieldType.Integer.ToString("G"));
                            return false;
                        }
                        return result == 0;
                    }
                case E_PageDataFieldType.Money:
                    {
                        decimal result;
                        if (!Decimal.TryParse(value.Replace("$", "").Replace("(", "").Replace(")", ""), out result))
                        {
                            Tools.LogBug("Couldn't parse " + value + " as " + E_PageDataFieldType.Money.ToString("G"));
                            return false;
                        }
                        return result == 0;
                    }                    
                case E_PageDataFieldType.Percent:
                    {
                        decimal result;
                        if (!Decimal.TryParse(value.Replace("%", ""), out result))
                        {
                            Tools.LogBug("Couldn't parse " + value + " as " + E_PageDataFieldType.Percent.ToString("G"));
                            return false;
                        }
                        return result == 0;
                    }                       
                default:
                    throw new UnhandledEnumException(fieldType);
            }           
        }
        public void CheckProtectedFields(IEnumerable<Tuple<string, FieldOriginalCurrentItem>> fieldChanges)
        {
            IList<ProtectFieldRule> rules;
            foreach (var fieldChange in fieldChanges)
            {
                if (m_fieldProtectionSet.TryGetValue(fieldChange.Item1, out rules) ||
                    m_fieldProtectionSet.TryGetValue(GetDataBaseId(fieldChange.Item1), out rules))
                {
                    foreach (ProtectFieldRule fieldRule in rules)
                    {
                        E_PageDataFieldType fieldType;
                        if (PageDataUtilities.GetFieldType(GetDataBaseId(fieldChange.Item1), out fieldType) && fieldType == E_PageDataFieldType.Bool)
                        {
                            List<string> newAllowedValues = new List<string>(2);
                            foreach (string val in fieldRule.SpecificValues)
                            {
                                if (val == "1")
                                {
                                    newAllowedValues.Add("Yes");
                                }
                                else if (val == "0")
                                {
                                    newAllowedValues.Add("No");
                                }
                            }

                            foreach (string val in newAllowedValues)
                            {
                                fieldRule.SpecificValues.Add(val);
                            }
                        }
          
                        if (fieldRule.SpecificValues.Contains(fieldChange.Item2.Current))
                        {
                            SetError(fieldChange.Item1, fieldRule, fieldChange.Item2.Original, fieldChange.Item2.Current);

                            if(!this.ReportAllWorkflowFailureMessages)
                            {
                                return;
                            }
                        }
                        else if (false == fieldRule.CanBeUpdated)
                        {
                            SetError(fieldChange.Item1, fieldRule, fieldChange.Item2.Original, fieldChange.Item2.Current);
                            if(!this.ReportAllWorkflowFailureMessages)
                            {
                                return;
                            }
                        }

                        // cause error if value is cleared.
                        if (WasCleared(fieldType, fieldChange.Item2.Current))
                        {
                            SetError(fieldChange.Item1, fieldRule, fieldChange.Item2.Original, fieldChange.Item2.Current);
                            if (!this.ReportAllWorkflowFailureMessages)
                            {
                                return;
                            }
                        }
                    }
                }
                //new way to protect agent fields
                else if (fieldChange.Item1.Equals("sAgentXmlContent", StringComparison.OrdinalIgnoreCase))
                {
                    foreach (string field in x_CAgentFieldSet)
                    {
                        if (!m_fieldProtectionSet.TryGetValue(field, out rules) || rules.Count == 0)
                        {
                            continue;
                        }

                        DataSet original =  CPageBase.GetDataSet(E_DataSetT.AgentXml, fieldChange.Item2.Original);
                        DataSet current = CPageBase.GetDataSet(E_DataSetT.AgentXml, fieldChange.Item2.Current);
                        UpdateCollection("CAgentFields", original, current);
                        break;
                    }
                }
            }
        }

        // 1/31/2012 dd - Provide a way to disable a way to field validator. 
        // One place that will disable field validator are Pricing Engine.
        public bool IsDisabled { get; set; }
        
        /// <summary>
        /// If your field is an object type, consider passing an IEqualityComparer for your specific type. <para></para>
        /// E.g. CAppData.cs > aBAliases and aCAliases.
        /// </summary>
        public void TryToWriteField<T>(string fieldId, Func<T> getter, Action<T> setter, T newValue)
        {
            TryToWriteField<T>(fieldId, getter, setter, newValue, null);
        }
        /// <summary>
        /// Do not call this unless you are absolutely sure that your IEqualityComparer works.  Most of the time you shouldn't need this.
        /// </summary>
        public void TryToWriteField<T>(string fieldId, Func<T> getter, Action<T> setter, T newValue, IEqualityComparer<T> comparer)
        {
            fieldId = GetDataBaseId(fieldId);
            m_fieldChanges.Add(fieldId);

            if (IsDisabled == true || (m_fieldProtectionSet.Count == 0 && m_fieldWriteSet.Count == 0))
            {
                setter(newValue);
                return;
            }

            if (comparer == null)
            {
                comparer = EqualityComparer<T>.Default;
            }

            T oldVal;
            try
            {
                //there are some pml fields that on get cause calculation updates to a bunch of a fields
                //for this is error prone because of the calculation assumption 
                //all fields are set  and then the calculation should happen       before any gets are called. 
                m_isValidationCheck = true;
                oldVal = getter();
            }
            catch (CBaseException e)
            {
                //could not retrieve the value so allow it to go through fine since it means that next time user tries to read it should fail. 
                Tools.LogWarning("Failed running getter for " + fieldId + " exception: " + e.DeveloperMessage);
                return;
            }
            finally
            {
                m_isValidationCheck = false;
            }
            setter(newValue); //set it because the setter may alter the value        

            T newVal;
            try
            {
                m_isValidationCheck = true;
                newVal = getter(); //pull it back out for the new and possibly formatted value.
            }
            finally
            {
                m_isValidationCheck = false;
            }
            //if the fieldid is in the exclusion list, do not check
            if (s_fieldsExcludedFromWriteCheck.Contains(fieldId)) return;

            if (comparer.Equals(oldVal, newVal)) //the value is the same no need to do any checks.
            {
                return;
            }

            if (typeof(DownPaymentGiftSet) == typeof(T))
            {
                DownPaymentGiftSet o = oldVal as DownPaymentGiftSet;
                DownPaymentGiftSet n = newVal as DownPaymentGiftSet;

                if (o.SerializeForStorage().Equals(n.SerializeForStorage()))
                {
                    return;
                }
            }
            else if (typeof(ClosingDisclosureDatesInfo) == typeof(T))
            {
                ClosingDisclosureDatesInfo o = oldVal as ClosingDisclosureDatesInfo;
                ClosingDisclosureDatesInfo n = newVal as ClosingDisclosureDatesInfo;

                if (o == null && n == null)
                {
                    return;
                }
                else if (o != null && n != null)
                {
                    if (o.ClosingDisclosureDatesList.Count == 0 && n.ClosingDisclosureDatesList.Count == 0)
                    {
                        // 4/12/2015 dd - Treat empty list as equals.
                        return;
                    }

                    if (o.IsEqual(n))
                    {
                        return;
                    }
                }


            }
            else if (typeof(LoanEstimateDatesInfo) == typeof(T))
            {
                LoanEstimateDatesInfo o = oldVal as LoanEstimateDatesInfo;
                LoanEstimateDatesInfo n = newVal as LoanEstimateDatesInfo;

                if (o == null && n == null)
                {
                    return;
                }
                else if (o != null && n != null)
                {
                    if (o.LoanEstimateDatesList.Count == 0 && n.LoanEstimateDatesList.Count == 0)
                    {
                        return;
                    }

                    if (o.IsEqual(n))
                    {
                        return;
                    }
                }
            }

            //if data layer passed in string do a case insensitve comparison.
            if(typeof(string) == typeof(T) && newVal.ToString().Equals(oldVal.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                return;
            }

            if (typeof(string) == typeof(T) && oldVal != null && newVal != null
                && string.Compare(
                    oldVal.ToString().TrimWhitespaceAndBOM().Replace("\r", "").Replace("\n", ""), 
                    newVal.ToString().TrimWhitespaceAndBOM().Replace("\r", "").Replace("\n", ""), 
                    StringComparison.OrdinalIgnoreCase) == 0)
            {   
                // opm 203926 - ie9 quirks / non standard makes \r\n out of \n.  also sometimes new value is a trimmed version of the old value.
                return;
            }

            //if  m_fieldWriteSet has values then the modified field HAS to be in the dictionary.
            if (m_fieldWriteSet.Count == 0)
            {
                return;
            }
            //field protection handled before save

            WriteFieldRule writeField;
            if (false == m_fieldWriteSet.TryGetValue(fieldId, out writeField))
            {
                //take the first field write rule as the reason.
                SetError(fieldId, m_fieldWriteSet.Values.First(), oldVal, newVal);
                return;
            }

            if (false == writeField.CanBeCleared && string.IsNullOrEmpty(newValue.ToString()))
            {
                SetError(fieldId, writeField, oldVal, newVal);
                return;
            }

            if (writeField.SpecificValues.Count > 0 && false == writeField.SpecificValues.Contains(newVal.ToString()))
            {
                Enum val = newVal as Enum;
                if (null != val && false == writeField.SpecificValues.Contains(val.ToString("D")))
                {
                    SetError(fieldId, writeField, oldVal, newVal);
                    return;
                }
            }

        }


        /// <summary>
        /// Loads the field permission from the loan value evaluator the evaluator must have been loaded with the correct 
        /// privileges. If user has full write there is no need to load the field write privleges only field protection.
        /// </summary>
        /// <param name="evaluator"></param>
        /// <param name="userHasFullWritePriv"></param>
        private void LoadFieldPermissions(LoanValueEvaluator evaluator, bool userHasFullWritePriv)
        {
            this.HasFullWrite = userHasFullWritePriv;
            SystemConfig globalConfig = GlobalSystemConfig;
            if (false == userHasFullWritePriv) //we only need to load this when the user does not have full write priv
            {
                Guid[] conditionIds = LendingQBExecutingEngine.GetAllPassingPrivilegeConditionIds(WorkflowOperations.WriteField, evaluator).ToArray();
                HasFieldWritePermissions = conditionIds.Length > 0;
                foreach (Guid id in conditionIds)
                {
                    AbstractConditionGroup conditionGroup = CurrentSystemConfig.Conditions.Get(id) ?? globalConfig.Conditions.Get(id);
                    SystemOperation writeOperation = conditionGroup.SysOpSet.Get(WorkflowOperations.WriteField);
                    foreach (KeyValuePair<string, OperationField> field in writeOperation.Fields)
                    {
                        WriteFieldRule fieldRule;
                        string specificValue = null;
                        string fieldName = field.Value.Id;

                        // Don't break into parts for path fileds.
                        if (!fieldName.StartsWith(DataPath.PathHeader, StringComparison.OrdinalIgnoreCase))
                        {
                            string[] parts = field.Value.Id.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                            fieldName = Regex.Replace(parts[0], "[_]rep$", "", RegexOptions.Singleline | RegexOptions.Compiled);

                            if (parts.Length == 2)
                            {
                                specificValue = parts[1];
                            }
                        }

                        if (fieldName.StartsWith("coll|", StringComparison.OrdinalIgnoreCase))
                        {
                            //we have a collection 
                            string[] colInfo = fieldName.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                            if (colInfo.Length == 3)
                            {
                                m_collectionsAllowedAdd.Add(colInfo[1]);
                            }
                            else
                            {
                                Tools.LogWarning("Invalid collection field inside of workflow " + fieldName);
                            }
                        }

                        if (false == m_fieldWriteSet.TryGetValue(fieldName, out fieldRule))
                        {
                            if (fieldName.Contains("|Payment-"))
                            {
                                this.m_canDeleteClosingCostPayments = true;
                            }

                            fieldRule = new WriteFieldRule(fieldName);
                            m_fieldWriteSet.Add(fieldName, fieldRule);
                        }

                        fieldRule.CanBeCleared = field.Value.Clearable;
                        if(!string.IsNullOrWhiteSpace(specificValue)) 
                        {
                            fieldRule.SpecificValues.Add(specificValue);
                        }

                        fieldRule.FailureMessages.Add(conditionGroup.FailureMessage);
                    }
                }
            }

            Guid[] protectionConditionIds = LendingQBExecutingEngine.GetAllPassingPrivilegeConditionIds(WorkflowOperations.ProtectField, evaluator).ToArray();

            foreach (Guid id in protectionConditionIds)
            {
                AbstractConditionGroup conditionGroup = CurrentSystemConfig.Conditions.Get(id) ?? globalConfig.Conditions.Get(id);
                SystemOperation writeOperation = conditionGroup.SysOpSet.Get(WorkflowOperations.ProtectField);

                Dictionary<string, ProtectFieldRule> protectedFieldSet = new Dictionary<string, ProtectFieldRule>();
                foreach (KeyValuePair<string, OperationField> field in writeOperation.Fields)
                {
                    ProtectFieldRule protectionRule;
                    string specificValue = null;
                    string fieldName = field.Value.Id;

                    // Don't break into parts for path fileds.
                    if (!fieldName.StartsWith(DataPath.PathHeader, StringComparison.OrdinalIgnoreCase))
                    {
                        string[] parts = field.Value.Id.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                        fieldName = Regex.Replace(parts[0], "[_]rep$", "", RegexOptions.Singleline | RegexOptions.Compiled);

                        if (parts.Length == 2)
                        {
                            specificValue = parts[1];
                        }
                    }

                    if (false == protectedFieldSet.TryGetValue(fieldName, out protectionRule))
                    {
                        protectionRule = new ProtectFieldRule(fieldName);
                        protectedFieldSet.Add(fieldName, protectionRule);
                    }

                    protectionRule.CanBeUpdated = field.Value.Updateable;

                    if (!string.IsNullOrWhiteSpace(specificValue))
                    {
                        protectionRule.SpecificValues.Add(specificValue);
                    }

                    protectionRule.FailureMessages.Add(conditionGroup.FailureMessage);
                }

                foreach (ProtectFieldRule rule in protectedFieldSet.Values)
                {
                    IList<ProtectFieldRule> rules;
                    if (false == m_fieldProtectionSet.TryGetValue(rule.FieldId, out rules))
                    {
                        rules = new List<ProtectFieldRule>();
                        m_fieldProtectionSet.Add(rule.FieldId, rules);
                    }

                    rules.Add(rule);
                }
            }
        }

        /// <summary>
        /// Loads the set of allowed loan status changes.
        /// </summary>
        /// <param name="evaluator">A loan value evaluator.</param>
        /// <remarks>
        /// Does not throw on parse failures do to invalid field values.
        /// Invalid values are simply logged before skipping to the next field.
        /// </remarks>
        private void LoadAllowedLoanStatusChanges(LoanValueEvaluator evaluator)
        {
            IEnumerable<Guid> restraintConditionIds = LendingQBExecutingEngine.GetAllPassingRestraintConditionIds(WorkflowOperations.LoanStatusChange, evaluator);
            IEnumerable<Guid> privilegeConditionIds = LendingQBExecutingEngine.GetAllPassingPrivilegeConditionIds(WorkflowOperations.LoanStatusChange, evaluator);

            HashSet<E_sStatusT> restrainedStatuses = ExtractLoanStatusesFromConditions(restraintConditionIds, true);
            HashSet<E_sStatusT> privilegedStatuses = ExtractLoanStatusesFromConditions(privilegeConditionIds, false);

            this.allowedLoanStatusChanges.UnionWith(privilegedStatuses.Except(restrainedStatuses));
        }

        private HashSet<E_sStatusT> ExtractLoanStatusesFromConditions(IEnumerable<Guid> loanStatusChangeConditionIds, bool forRestraints)
        {
            HashSet<E_sStatusT> statusSet = new HashSet<E_sStatusT>();
            foreach (Guid id in loanStatusChangeConditionIds)
            {
                AbstractConditionGroup conditionGroup;
                if (forRestraints)
                {
                    conditionGroup = CurrentSystemConfig.Restraints.Get(id) ?? GlobalSystemConfig.Restraints.Get(id);
                }
                else
                {
                    conditionGroup = CurrentSystemConfig.Conditions.Get(id) ?? GlobalSystemConfig.Conditions.Get(id);
                }

                SystemOperation loanStatusChangeOperation = conditionGroup.SysOpSet.Get(WorkflowOperations.LoanStatusChange);

                foreach (OperationField field in loanStatusChangeOperation.Fields.Values)
                {
                    string[] parts = field.Id.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                    if (parts.Length != 2)
                    {
                        Tools.LogWarning($"[LoanFileFieldValidator::LoadAllowedLoanStatusChanges] FieldId is in wrong format. Skipping. BrokerId=[{Principal.BrokerId}]. Condition ID=[{conditionGroup.Id}]. FieldId=[{field.Id}].");
                        continue;
                    }

                    E_sStatusT status;
                    if (Enum.TryParse(parts[1], out status) && Enum.IsDefined(typeof(E_sStatusT), status))
                    {
                        statusSet.Add(status);
                    }
                    else
                    {
                        Tools.LogWarning($"[LoanFileFieldValidator::LoadAllowedLoanStatusChanges] Invalid status value. Skipping. BrokerId=[{Principal.BrokerId}]. Condition ID=[{conditionGroup.Id}]. FieldId=[{field.Id}].");
                    }
                }
            }

            return statusSet;
        }

        /// <summary>
        /// Checks if loan status can be changed to passed in status.
        /// </summary>
        /// <param name="newStatus">Status you want to set.</param>
        /// <returns>True, if change to passed is status value is allowed. False otherwise.</returns>
        public bool CanChangeLoanStatus(E_sStatusT newStatus)
        {
            return this.allowedLoanStatusChanges.Contains(newStatus);
        }
    }

    #region field rule classes
    public class FieldRule
    {
        public string FieldId { get; private set; }
        public HashSet<string> SpecificValues { get; set; }
        public HashSet<string> FailureMessages { get; set; }

        public FieldRule(string fieldId)
        {
            FieldId = fieldId;
            SpecificValues = new HashSet<string>();
            FailureMessages = new HashSet<string>();
        }

        public override string ToString()
        {
            string a = "FieldId: [" + FieldId + "]";

            if (SpecificValues.Count > 0)
            {
                a += " Specific Values : {" + string.Join(",", SpecificValues.ToArray()) + "}";
            }

            return a;
        }
    }
    //combine                           
    public sealed class WriteFieldRule : FieldRule
    {
        //enum with value means you can only change it to those values 
        public bool CanBeCleared { get; set; }        //update but not clear
        public WriteFieldRule(string fieldId) : base(fieldId) { }

        public override string ToString()
        {
            return "WriteFieldRule " + base.ToString() + " CanBeCleared: " + CanBeCleared;
        }

    }

    //dont combine rules with same fields 
    public sealed class ProtectFieldRule : FieldRule  //
    {
        private bool m_canBeUpdated;
        //if enum has value it means that it cannot be changed to that value
        public bool CanBeUpdated { get { return SpecificValues.Count > 0 || m_canBeUpdated; } set { m_canBeUpdated = value; } }
        public ProtectFieldRule(string fieldId) : base(fieldId) { }

        public override string ToString()
        {
            return "WriteProtectionRule " + base.ToString() + " CanBeUpdated: " + CanBeUpdated;
        }
    }

    #endregion
}
