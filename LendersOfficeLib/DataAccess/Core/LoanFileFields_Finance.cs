﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOfficeApp.los.RatePrice;
using LendersOffice.Common;
using LendersOffice.ObjLib.Rolodex;
using LendersOffice.ObjLib.Conversions.ProvidentFunding;
using LendersOffice.ObjLib.CustomAttributes;
using LendersOffice.UI;
using LqbGrammar.DataTypes;

namespace DataAccess
{
    public partial class CPageBase
    {
        #region Servicing Fields

        private static Dictionary<E_ServicingPaymentT, string> sServicingPmtT_map = new Dictionary<E_ServicingPaymentT, string>()
        {
            { E_ServicingPaymentT.FullyAmortizing, "Principal & Interest"},
            { E_ServicingPaymentT.InterestOnly, "Interest Only"},
        };

        //NOTE: the order of these entries is important, if you change it, it will affect anything that indexes it, such as sServicingTransT_map[0]
        public static List<string> sServicingTransT_map = new List<string>() { "Escrow Disbursement", "Monthly Payment" , "Late fee charged"};

        public static List<string> sServicingTransTComboBox_map = new List<string>() { " ", sServicingTransT_map[0], sServicingTransT_map[1], sServicingTransT_map[2], };

        [DependsOn]
        private decimal sServicingOtherPmts
        {
            get { return GetRateField("sServicingOtherPmts"); }
            set { SetRateField("sServicingOtherPmts", value); }
        }
        public string sServicingOtherPmts_rep
        {
            get { return ToMoneyString(() => sServicingOtherPmts); }
            set { sServicingOtherPmts = ToMoney(value); }
        }
        
        [DependsOn(nameof(sIOnlyMon))]
        public E_ServicingPaymentT sServicingPmtT
        {
            get 
            {
                if (sIOnlyMon > 0)
                {
                    return E_ServicingPaymentT.InterestOnly;
                }
                else
                {
                    return E_ServicingPaymentT.FullyAmortizing;   
                }
            }
        }
        public static string sServicingPmtT_map_rep(E_ServicingPaymentT value)
        {
            return sServicingPmtT_map[value];
        }
        public string sServicingPmtT_rep
        {
            get { return sServicingPmtT_map_rep(sServicingPmtT); }
        }

        [DependsOn(nameof(sFinalLAmt), nameof(sServicingTotalDueFunds_Principal), nameof(sServicingPmtT))]
        private decimal sServicingPrincipalBasis
        {
            get
            {
                switch(sServicingPmtT)
                {
                    case E_ServicingPaymentT.FullyAmortizing:
                        return sFinalLAmt;
                    case E_ServicingPaymentT.InterestOnly: // Use the Unpaid Pricincipal Balance field (which is just the Principal, Total w/pmts due field from the top summary table)
                        return sServicingTotalDueFunds_Principal;
                    default:
                        throw new UnhandledEnumException(sServicingPmtT);
                }
            }
        }

        public string sServicingPrincipalBasis_rep
        {
            get { return ToMoneyString(() => sServicingPrincipalBasis); }
        }


        [DependsOn(nameof(sServicingCurrentPmtDueEscrow),nameof(sProMIns))]
        private decimal sServicingTaxInsuranceImpounds
        {
            get 
            {
                return sServicingCurrentPmtDueEscrow - sProMIns; 
            }
        }

        public string sServicingTaxInsuranceImpounds_rep
        {
            get { return ToMoneyString(() => sServicingTaxInsuranceImpounds); }
        }

        [DependsOn(nameof(sSettlementHazInsRsrv), nameof(sSettlementMInsRsrv), nameof(sSettlementRealETxRsrv), nameof(sSettlementSchoolTxRsrv),
            nameof(sSettlementFloodInsRsrv), nameof(sSettlementAggregateAdjRsrv), nameof(sSettlement1008Rsrv), nameof(sSettlement1009Rsrv),
            nameof(sSettlementU3Rsrv), nameof(sSettlementU4Rsrv), nameof(sGfeInitialImpoundDeposit), nameof(sClosingCostFeeVersionT))]
        private decimal sInitialDeposit_Escrow
        {
            get
            {
                if (sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
                {
                    return SumMoney(sSettlementHazInsRsrv, sSettlementMInsRsrv, sSettlementRealETxRsrv, sSettlementSchoolTxRsrv, sSettlementFloodInsRsrv, sSettlementAggregateAdjRsrv, sSettlement1008Rsrv, sSettlement1009Rsrv, sSettlementU3Rsrv, sSettlementU4Rsrv);
                }
                else
                {
                    // OPM 232171 - return sGfeInitialImpoundDeposit for migrated loans.
                    return sGfeInitialImpoundDeposit;
                }
            }
        }
        public string sInitialDeposit_Escrow_rep
        {
            get { return ToMoneyString(() => sInitialDeposit_Escrow); }
        }


        [DependsOn(nameof(sProThisMPmt), nameof(sServicingEscrowPmt), nameof(sServicingOtherPmts), nameof(sIsIOnly), nameof(sServicingInterestDue))]
        private decimal sServicingNextPmtAmt
        {
            get
            {
                if (sIsIOnly)
                {
                    // If this file is interest only, then this is just the sum of Interest Due + Escrow pmt + Other Pmts
                    return SumMoney(sServicingInterestDue, sServicingEscrowPmt, sServicingOtherPmts);
                }
                else
                {
                    // Use sProThisMPmt calculation and add onto it the escrow pmt and other pmts
                    return SumMoney(sProThisMPmt, sServicingEscrowPmt, sServicingOtherPmts);
                }
            }
        }
        public string sServicingNextPmtAmt_rep
        {
            get { return ToMoneyString(() => sServicingNextPmtAmt); }
        }

        [DependsOn(nameof(sProThisMPmt), nameof(sServicingInterestDue), nameof(sIsIOnly), nameof(sNoteIR))]
        public decimal sServicingNextPmtAmtPrincipal
        {
            // Used to get the principal portion of the next monthly payment
            // Error handling done specifically for Provident export, this may
            // not be appropriate for other applications.
            get
            {
                // If it is interest only, then no portion is principal
                if (sIsIOnly)
                    return 0;

                return decimal.Round(sProThisMPmt - sServicingInterestDue, 2);
            }
        }

        [DependsOn(nameof(sServicingNextPmtAmt), nameof(sNoteIR))]
        public decimal sProvExpServicingNextPmtAmt
        {
            get 
            {
                // If sNoteIR == 0, return 0
                if (sNoteIR == 0)
                    return 0;

                return decimal.Round(sServicingNextPmtAmt, 2);
            }
        }

        [DependsOn(nameof(sProThisMPmt))]
        public decimal sProvExpPIPmtAmt
        {
            get { return decimal.Round(sProThisMPmt, 2); }
        }

        [DependsOn(nameof(sServicingUnpaidPrincipalBalance), nameof(sNoteIR))]
        private decimal sServicingInterestDue
        {
            get
            {
                return (sServicingUnpaidPrincipalBalance * (sNoteIR / 100)) / 12;
            }
        }
        public string sServicingInterestDue_rep
        {
            get
            {
                return ToMoneyString(() => sServicingInterestDue); 
            }
        }

        [DependsOn(nameof(sServicingInterestDue))]
        public decimal sProvExpServicingInterestDue
        {
            get { return decimal.Round(sServicingInterestDue, 2); }
        }

        private DateTime m_sServicingNextPmtDue = CDateTime.InvalidDateTime;

        [DependsOn(nameof(sServicingPayments),nameof(CalculateNextPmtD))]
        public DateTime sServicingNextPmtDue
        {
            get 
            {
                if (m_sServicingNextPmtDue == CDateTime.InvalidDateTime)
                {
                    CalculateNextPmtD(sServicingPayments);
                }
                
                return m_sServicingNextPmtDue;
            }
            private set { m_sServicingNextPmtDue = value; }
        }

        [DependsOn(nameof(sSchedDueD1))]
        private void CalculateNextPmtD(List<CServicingPaymentFields> pmtFields)
        {
            DateTime lastDueDate = DateTime.MinValue;
            foreach (CServicingPaymentFields fields in pmtFields)
            {
                if ((fields.ServicingTransactionT.ToLower() == TRANSACTIONTYPE_MONTHLYPMT.ToLower()) && (fields.DueD > lastDueDate))
                {
                    lastDueDate = fields.DueD;
                }
            }

            m_sServicingNextPmtDue = lastDueDate;

            if (m_sServicingNextPmtDue != DateTime.MinValue)
            {
                m_sServicingNextPmtDue = m_sServicingNextPmtDue.AddMonths(1);
            }
            else
            {
                try
                {
                    m_sServicingNextPmtDue = sSchedDueD1.DateTimeForComputation;
                }
                catch
                {
                    m_sServicingNextPmtDue = lastDueDate;
                }
            }
        }

        public string sServicingNextPmtDue_rep
        {
            get
            {
                if (sServicingNextPmtDue == DateTime.MinValue)
                {
                    return "";
                }
                else
                {
                    return sServicingNextPmtDue.ToShortDateString();
                }
            }
            set  
            {
                try
                {
                    if (string.IsNullOrEmpty(value))
                    {
                        sServicingNextPmtDue = DateTime.MinValue;
                    }
                    else
                    {
                        sServicingNextPmtDue = DateTime.Parse(value);
                    }
                }
                catch (System.FormatException f)
                {
                    throw new CBaseException("Next Payment Due is an invalid Date value", f);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        [DependsOn(nameof(sInvSchedDueD1), nameof(sServicingNextPmtDue))]
        public bool sIsNextPaymentBefore1stPaymentDueInvestor
        {
            get
            {
                return !sInvSchedDueD1.IsValid || sServicingNextPmtDue < sInvSchedDueD1.DateTimeForComputation;
            }
        }

        public string sIsNextPaymentBefore1stPaymentDueInvestor_rep
        {
            get
            {
                return sIsNextPaymentBefore1stPaymentDueInvestor ? "Y" : "N";
            }
        }

        private CServicingPaymentFields m_sServicingLastPaymentMade;

        [DependsOn(nameof(sServicingPayments))]
        private CServicingPaymentFields sServicingLastPaymentMade
        {
            get
            {
                if (m_sServicingLastPaymentMade == null || String.IsNullOrEmpty(m_sServicingLastPaymentMade.PaymentD_rep))
                {
                    getLastPmt(sServicingPayments);
                }

                return m_sServicingLastPaymentMade;
            }
        }

        private void getLastPmt(List<CServicingPaymentFields> pmtFields)
        {
            m_sServicingLastPaymentMade = new CServicingPaymentFields();    // Sets DueD == DateTime.MinValue

            foreach (CServicingPaymentFields fields in pmtFields)
            {
                if (fields.ServicingTransactionT.ToLower() == TRANSACTIONTYPE_MONTHLYPMT.ToLower() && fields.PaymentD > m_sServicingLastPaymentMade.PaymentD)
                {
                    m_sServicingLastPaymentMade = fields;
                }
            }
        }

        [DependsOn(nameof(sServicingLastPaymentMade))]
        public DateTime sServicingLastPaymentMadeDueD
        {
            get
            {
                return sServicingLastPaymentMade.DueD;
            }
        }

        [DependsOn(nameof(sServicingLastPaymentMadeDueD))]
        public string sServicingLastPaymentMadeDueD_rep
        {
            get
            {
                return m_convertLos.ToDateTimeString(sServicingLastPaymentMadeDueD);
            }
        }

        [DependsOn(nameof(sServicingLastPaymentMade))]
        public decimal sServicingLastPaymentMadeDueAmt
        {
            get
            {
                return sServicingLastPaymentMade.DueAmt;
            }
        }

        [DependsOn(nameof(sServicingLastPaymentMadeDueAmt))]
        public string sServicingLastPaymentMadeDueAmt_rep
        {
            get
            {
                return ToMoneyString(() => sServicingLastPaymentMadeDueAmt);
            }
        }

        [DependsOn(nameof(sServicingLastPaymentMade))]
        public DateTime sServicingLastPaymentMadePaymentD
        {
            get
            {
                return sServicingLastPaymentMade.PaymentD;
            }
        }

        [DependsOn(nameof(sServicingLastPaymentMadePaymentD))]
        public string sServicingLastPaymentMadePaymentD_rep
        {
            get
            {
                return m_convertLos.ToDateTimeString(sServicingLastPaymentMadePaymentD);
            }
        }

        [DependsOn(nameof(sServicingLastPaymentMade))]
        public decimal sServicingLastPaymentMadePaymentAmt
        {
            get
            {
                return sServicingLastPaymentMade.PaymentAmt;
            }
        }

        [DependsOn(nameof(sServicingLastPaymentMadePaymentAmt))]
        public string sServicingLastPaymentMadePaymentAmt_rep
        {
            get
            {
                return ToMoneyString(() => sServicingLastPaymentMadePaymentAmt);
            }
        }

        [DependsOn(nameof(sServicingLastPaymentMade))]
        public decimal sServicingLastPaymentMadePrincipal
        {
            get
            {
                return sServicingLastPaymentMade.Principal;
            }
        }

        [DependsOn(nameof(sServicingLastPaymentMadePrincipal))]
        public string sServicingLastPaymentMadePrincipal_rep
        {
            get
            {
                return ToMoneyString(() => sServicingLastPaymentMadePrincipal);
            }
        }

        [DependsOn(nameof(sServicingLastPaymentMade))]
        public decimal sServicingLastPaymentMadeInterest
        {
            get
            {
                return sServicingLastPaymentMade.Interest;
            }
        }

        [DependsOn(nameof(sServicingLastPaymentMadeInterest))]
        public string sServicingLastPaymentMadeInterest_rep
        {
            get
            {
                return ToMoneyString(() => sServicingLastPaymentMadeInterest);
            }
        }

        [DependsOn(nameof(sServicingLastPaymentMade))]
        public decimal sServicingLastPaymentMadeEscrow
        {
            get
            {
                return sServicingLastPaymentMade.Escrow;
            }
        }

        [DependsOn(nameof(sServicingLastPaymentMadeEscrow))]
        public string sServicingLastPaymentMadeEscrow_rep
        {
            get
            {
                return ToMoneyString(() => sServicingLastPaymentMadeEscrow);
            }
        }

        [DependsOn(nameof(sServicingLastPaymentMade))]
        public decimal sServicingLastPaymentMadeOther
        {
            get
            {
                return sServicingLastPaymentMade.Other;
            }
        }

        [DependsOn(nameof(sServicingLastPaymentMadeOther))]
        public string sServicingLastPaymentMadeOther_rep
        {
            get
            {
                return ToMoneyString(() => sServicingLastPaymentMadeOther);
            }
        }

        [DependsOn(nameof(sServicingLastPaymentMade))]
        public string sServicingLastPaymentMadeNotes
        {
            get
            {
                return sServicingLastPaymentMade.Notes;
            }
        }

        private bool m_isAddingNewServicingPmtDue = false;

        [DependsOn(nameof(sServicingPayments))]
        public void AddNextServicingPaymentDue(List<CServicingPaymentFields> fieldList)
        {
            m_isAddingNewServicingPmtDue = true;
            // Call the setter for sServicingPayments so that the calculations can be performed for the summary table
            sServicingPayments = fieldList;
        }

        [DependsOn(nameof(sServicingNextPmtDue), nameof(sSchedDueD1), nameof(sServicingNextPmtAmt), nameof(sServicingInterestDue), nameof(sServicingEscrowPmt), nameof(sServicingOtherPmts))]
        private void SetNewServicingPaymentEntryFields(List<CServicingPaymentFields> fieldList)
        {
            // 1.	Create a new "Monthly payment" entry (MPE).
            // The UI is already doing this is and so we must just update the last entry in the CServicingPaymentFields list
            CServicingPaymentFields fields = fieldList[fieldList.Count - 1];

            // 2.	Copy "Next payment due" to MPE."Due Date".
            //      If “Next payment due” is blank, copy the value from “1st payment due” into “Next payment due”, and use that.
            if (string.IsNullOrEmpty(sServicingNextPmtDue_rep) && sSchedDueD1.IsValid)
            {
                sServicingNextPmtDue = sSchedDueD1.DateTimeForComputation;
            }
            fields.DueD_rep = sServicingNextPmtDue_rep;
            fields.PaymentD_rep = "";

            // 3.	Increase the "Next payment due" by a month.
            sServicingNextPmtDue = sServicingNextPmtDue.AddMonths(1);
            
            // 4.	Copy "Next payment amount" to MPE."Due".
            fields.DueAmt_rep = m_convertLos.ToMoneyString(sServicingNextPmtAmt, FormatDirection.ToRep);

            // 5.	Copy "Interest due" to MPE."Interest".
            fields.Interest_rep = m_convertLos.ToMoneyString(sServicingInterestDue, FormatDirection.ToRep);

            // 6.	Copy "Escrow pmt" to MPE."Escrow".
            fields.Escrow_rep = m_convertLos.ToMoneyString(sServicingEscrowPmt, FormatDirection.ToRep);
            // 7.	Copy "Other pmts" to MPE."Other".
            fields.Other_rep = sServicingOtherPmts_rep;

            // 8.	Set MPE."Principal" to MPE."Payment" (due) minus the sum of the other MPE fields.
            decimal others = SumMoney(fields.Interest + fields.Escrow + fields.Other);
            decimal newPrincipal = SumMoney(fields.DueAmt, (others * -1));
            fields.Principal_rep = m_convertLos.ToMoneyString(newPrincipal, FormatDirection.ToRep);

            // Set fields that don't have default values to 0 so they show up as $0.00 in the UI
            fields.PaymentAmt_rep = m_convertLos.ToMoneyString(0, FormatDirection.ToRep);
            fields.LateFee_rep = m_convertLos.ToMoneyString(0, FormatDirection.ToRep);

            // Set the description to "Monthly Payment"
            fields.ServicingTransactionT = TRANSACTIONTYPE_MONTHLYPMT;
        }

        /// <summary>
        /// List of servicing payments on the servicing page.
        /// </summary>
        [DependsOn]
        public string sServicingPmtXmlContent
        {
            get { return GetLongTextField("sServicingPmtXmlContent"); }
            set { SetLongTextField("sServicingPmtXmlContent", value); }
        }

        /// <summary>
        /// Used by the sServicingPayments property. XmlSerializer is thread safe.
        /// </summary>
        private static System.Xml.Serialization.XmlSerializer x_ServicingPaymentsSerializer = new System.Xml.Serialization.XmlSerializer(typeof(List<CServicingPaymentFields>));
        [DependsOn(nameof(sServicingPmtXmlContent), nameof(SetPaymentSummaryData))]
        public List<CServicingPaymentFields> sServicingPayments
        {
            get
            {
                string data = sServicingPmtXmlContent;
                if (string.IsNullOrEmpty(data))
                {
                    return new List<CServicingPaymentFields>();
                }

                using (System.IO.TextReader textReader = new System.IO.StringReader(data))
                {
                    return (List<CServicingPaymentFields>)x_ServicingPaymentsSerializer.Deserialize(textReader);
                }
            }
            set
            {
                SetPaymentSummaryData(value);
                
                using (var writer = new System.IO.StringWriter())
                {
                    x_ServicingPaymentsSerializer.Serialize(writer, value);
                    sServicingPmtXmlContent = writer.ToString();
                }
            }
        }

        [DependsOn(nameof(sServicingPayments))]
        public List<CServicingPaymentFields> sServicingPayments_rep
        {
            get
            {
                var servicingPayments = new List<CServicingPaymentFields>(sServicingPayments);
                foreach (var payment in servicingPayments)
                {
                    payment.DueAmt_rep = m_convertLos.ToMoneyString(payment.DueAmt, FormatDirection.ToRep);
                    payment.PaymentAmt_rep = m_convertLos.ToMoneyString(payment.PaymentAmt, FormatDirection.ToRep);
                    payment.Principal_rep = m_convertLos.ToMoneyString(payment.Principal, FormatDirection.ToRep);
                    payment.Interest_rep = m_convertLos.ToMoneyString(payment.Interest, FormatDirection.ToRep);
                    payment.Escrow_rep = m_convertLos.ToMoneyString(payment.Escrow, FormatDirection.ToRep);
                    payment.Other_rep = m_convertLos.ToMoneyString(payment.Other, FormatDirection.ToRep);
                    payment.LateFee_rep = m_convertLos.ToMoneyString(payment.LateFee, FormatDirection.ToRep);
                }
                return servicingPayments;
            }
        }

        /// <summary>
        /// To be used in PaymentHistoryPDF.cs
        /// </summary>
        [DependsOn(nameof(sServicingPayments))]
        public List<CServicingPaymentFields> sServicingTotalAll
        {
            get
            {
                decimal[] dueAmt = { 0, 0, 0, 0 };
                decimal[] paymentAmt = { 0, 0, 0, 0 };
                decimal[] principal = { 0, 0, 0, 0 };
                decimal[] interest = { 0, 0, 0, 0 };
                decimal[] escrow = { 0, 0, 0, 0 };
                decimal[] other = { 0, 0, 0, 0 };
                decimal[] lateFee = { 0, 0, 0, 0 };
                int[] numRows = {0, 0, 0, 0};

                foreach (var payment in sServicingPayments)
                {
                    int totalRow;
                    switch (payment.ServicingTransactionT)
                    {
                        case "Monthly Payment":
                            totalRow = 0;
                            break;
                        case "Escrow Disbursement":
                            totalRow = 1;
                            break;
                        case "Late fee charged":
                            totalRow = 2;
                            break;
                        default:
                            totalRow = 3;
                            break;
                    }
                    // Performance warning:
                    // All the data is actually stored in the _reps, so it is going to do ToDecimal each time.
                    dueAmt[totalRow] = SumMoney(payment.DueAmt, dueAmt[totalRow]);
                    paymentAmt[totalRow] = SumMoney(payment.PaymentAmt, paymentAmt[totalRow]);
                    principal[totalRow] = SumMoney(payment.Principal, principal[totalRow]);
                    interest[totalRow] = SumMoney(payment.Interest, interest[totalRow]);
                    escrow[totalRow] = SumMoney(payment.Escrow, escrow[totalRow]);
                    other[totalRow] = SumMoney(payment.Other, other[totalRow]);
                    lateFee[totalRow] = SumMoney(payment.LateFee, lateFee[totalRow]);
                    numRows[totalRow] += 1;
                }

                var totals = new List<CServicingPaymentFields>();
                for (int i = 0; i < 4; i++)
                {
                    totals.Add(new CServicingPaymentFields());
                    totals[i].DueAmt_rep = m_convertLos.ToMoneyString(dueAmt[i], FormatDirection.ToRep);
                    totals[i].PaymentAmt_rep = m_convertLos.ToMoneyString(paymentAmt[i], FormatDirection.ToRep);
                    totals[i].Principal_rep = m_convertLos.ToMoneyString(principal[i], FormatDirection.ToRep);
                    totals[i].Interest_rep = m_convertLos.ToMoneyString(interest[i], FormatDirection.ToRep);
                    totals[i].Escrow_rep = m_convertLos.ToMoneyString(escrow[i], FormatDirection.ToRep);
                    totals[i].Other_rep = m_convertLos.ToMoneyString(other[i], FormatDirection.ToRep);
                    totals[i].LateFee_rep = m_convertLos.ToMoneyString(lateFee[i], FormatDirection.ToRep);
                    totals[i].RowNum = numRows[i]; // Just use RowNum as the number of rows
                }
                totals[0].ServicingTransactionT = "Monthly Payment Totals";
                totals[0].Notes = "monthly payment"; // This is kind of hacky, but I don't want to change CServicingPaymentFields
                totals[1].ServicingTransactionT = "Escrow Disbursement Totals";
                totals[1].Notes = "escrow disbursement";
                totals[2].ServicingTransactionT = "Late Fee Totals";
                totals[2].Notes = "late fee charged";
                totals[3].ServicingTransactionT = "Other Payment Totals";
                totals[3].Notes = "other payment";
                return totals;
            }
        }

        private static void ValidatePmtFields(CServicingPaymentFields fields)
        {
            DateTime temp;
            if (string.IsNullOrEmpty(fields.PaymentD_rep) == false && DateTime.TryParse(fields.PaymentD_rep, out temp) == false)
            {
                CBaseException exc = new CBaseException("Payment Date contains an invalid Date value.", "Payment Date contains an invalid Date value.");
                exc.IsEmailDeveloper = false;
                throw exc;
            }

            if (string.IsNullOrEmpty(fields.DueD_rep) == false && DateTime.TryParse(fields.DueD_rep, out temp) == false)
            {
                CBaseException exc = new CBaseException("Due Date contains an invalid Date value.", "Due Date contains an invalid Date value.");
                exc.IsEmailDeveloper = false;
                throw exc;
            }
        }

        private bool m_areServicingPmtFieldsSet = false;
        public const int SVCPMT_DESCCOL = 0;
        public const int SVCPMT_DUEDCOL = 1;
        public const int SVCPMT_DUEAMTCOL = 2;
        public const int SVCPMT_PMTDCOL = 3;
        public const int SVCPMT_PAYMENTCOL = 4;
        public const int SVCPMT_PRINCIPALCOL = 5;
        public const int SCVPMT_INTERESTCOL = 6;
        public const int SCVPMT_ESCROWCOL = 7;
        public const int SCVPMT_OTHERCOL = 8;
        public const int SCVPMT_LATEFEECOL = 9;
        public const int SCVPMT_PMTRCVDDCOL = 10;

        public string TRANSACTIONTYPE_ESCROW = sServicingTransT_map[0];
        public string TRANSACTIONTYPE_MONTHLYPMT = sServicingTransT_map[1];
        public string TRANSACTIONTYPE_LATEFEE = sServicingTransT_map[2];

        [DependsOn(nameof(SetTopServicingSummaryFields), nameof(SetNewServicingPaymentEntryFields), nameof(CalculateNextPmtD),
            nameof(sServicingDueFunds_Principal), nameof(sServicingDueFunds_Interest), nameof(sServicingDueFunds_Escrow),
            nameof(sServicingDueFunds_Other), nameof(sServicingDueFunds_LateFees), nameof(sServicingCollectedFunds_Principal), nameof(sServicingCollectedFunds_Interest),
            nameof(sServicingCollectedFunds_Escrow), nameof(sServicingCollectedFunds_Other), nameof(sServicingCollectedFunds_LateFees), nameof(sServicingDisbursedFunds_Principal),
            nameof(sServicingDisbursedFunds_Interest), nameof(sServicingDisbursedFunds_Escrow), nameof(sServicingDisbursedFunds_Other), nameof(sServicingDisbursedFunds_LateFees))]
        private void SetPaymentSummaryData(List<CServicingPaymentFields> pmtFields)
        {
            m_areServicingPmtFieldsSet = true;

            for (int i = 0; i < pmtFields.Count; ++i)
            {
                CServicingPaymentFields fields = (CServicingPaymentFields)pmtFields[i];

                if (m_isAddingNewServicingPmtDue && (i == pmtFields.Count - 1))
                {
                    SetTopServicingSummaryFields();
                    SetNewServicingPaymentEntryFields(pmtFields);
                }

                // If there is no payment nor due date, throw an exception
                if (string.IsNullOrEmpty(fields.PaymentD_rep) && string.IsNullOrEmpty(fields.DueD_rep))
                {
                    // We will not throw an exception here because per our current model, we will allow saves even with invalid fields.
                    // The field validator will still be shown in the UI so the user will be aware something is wrong with their field.
                    continue;
                    //throw new CBaseException("Both Payment Date and Due Date are empty.", "Both Payment Date and Due Date are empty.");
                }

                CPageBase.ValidatePmtFields(fields);

                bool usePaymentAmount = (string.IsNullOrEmpty(fields.PaymentD_rep) == false);
                // If the user changed the payment or due amount column (or the dates), just update the principal value so that Payment = Principal + Interest + Escrow + Other
                if (fields.LastColumnUpdated == SVCPMT_PAYMENTCOL || fields.LastColumnUpdated == SVCPMT_DUEAMTCOL || fields.LastColumnUpdated == SVCPMT_DUEDCOL || fields.LastColumnUpdated == SVCPMT_PMTDCOL)
                {
                    decimal paymentAmount = (usePaymentAmount) ? fields.PaymentAmt : fields.DueAmt;
                    decimal principalOrEscrowVal = (fields.ServicingTransactionT.ToLower() == TRANSACTIONTYPE_ESCROW.ToLower()) ? fields.Principal : fields.Escrow;
                    decimal otherFieldsTotal = fields.Interest + principalOrEscrowVal + fields.Other;
                    decimal newPrincipal = paymentAmount - otherFieldsTotal;

                    if (fields.ServicingTransactionT.ToLower() == TRANSACTIONTYPE_ESCROW.ToLower())
                    {
                        // For escrow disbursement entries, the difference here is that instead of re-computing principal, re-compute the escrow balance instead.
                        fields.Escrow_rep = m_convertLos.ToMoneyString(newPrincipal, FormatDirection.ToRep);
                    }
                    else if (fields.ServicingTransactionT.ToLower() == TRANSACTIONTYPE_LATEFEE.ToLower())
                    {
                        fields.LateFee_rep = m_convertLos.ToMoneyString(newPrincipal, FormatDirection.ToRep);
                    }
                    else // For all other types, use the same logic as the monthly payment entries, which is to recompute the principal when the payment changes.
                    {
                        fields.Principal_rep = m_convertLos.ToMoneyString(newPrincipal, FormatDirection.ToRep);
                    }
                }

                // If the user changes any other field, leave all other fields fixed and re-compute Payment.
                if (fields.LastColumnUpdated == SVCPMT_PRINCIPALCOL || fields.LastColumnUpdated == SCVPMT_INTERESTCOL || fields.LastColumnUpdated == SCVPMT_ESCROWCOL || fields.LastColumnUpdated == SCVPMT_OTHERCOL || fields.LastColumnUpdated == SCVPMT_LATEFEECOL)
                {
                    decimal newTotal = fields.Principal + fields.Interest + fields.Escrow + fields.Other + fields.LateFee;
                    if (usePaymentAmount)
                    {
                        fields.PaymentAmt_rep = m_convertLos.ToMoneyString(newTotal, FormatDirection.ToRep);
                    }
                    else
                    {
                        fields.DueAmt_rep = m_convertLos.ToMoneyString(newTotal, FormatDirection.ToRep);
                    }
                }

                #region Update Summary Fields
                // If there's a due date and no payment date, update the due amount summary fields
                if (string.IsNullOrEmpty(fields.DueD_rep) == false && string.IsNullOrEmpty(fields.PaymentD_rep))
                {
                    sServicingDueFunds_Principal += fields.Principal;
                    sServicingDueFunds_Interest += fields.Interest;
                    sServicingDueFunds_Escrow += fields.Escrow;
                    sServicingDueFunds_Other += fields.Other;
                    sServicingDueFunds_LateFees += fields.LateFee;
                }
                else
                {
                    // If payment amount is positive, update the Collected summary field.  Otherwise, update the disbursed summary field.
                    if (fields.PaymentAmt >= 0)
                    {
                        sServicingCollectedFunds_Principal += fields.Principal;
                        sServicingCollectedFunds_Interest += fields.Interest;
                        sServicingCollectedFunds_Escrow += fields.Escrow;
                        sServicingCollectedFunds_Other += fields.Other;
                        sServicingCollectedFunds_LateFees += fields.LateFee;
                    }
                    else
                    {
                        sServicingDisbursedFunds_Principal += fields.Principal;
                        sServicingDisbursedFunds_Interest += fields.Interest;
                        sServicingDisbursedFunds_Escrow += fields.Escrow;
                        sServicingDisbursedFunds_Other += fields.Other;
                        sServicingDisbursedFunds_LateFees += fields.LateFee;
                    }
                }
                #endregion
            }
            
            SetTopServicingSummaryFields();
            
            if (m_isAddingNewServicingPmtDue == false)
            {
                CalculateNextPmtD(pmtFields); // Recalculate the next payment date, we might be deleting entries
            }
        }
        
        [DependsOn(nameof(sServicingClosingFunds_Interest), nameof(sInitialDeposit_Escrow), nameof(sServicingClosingFunds_Principal),
            nameof(sServicingCollectedFunds_Principal), nameof(sServicingDisbursedFunds_Principal), nameof(sServicingCurrentTotalFunds_Principal),
            nameof(sServicingCollectedFunds_Interest), nameof(sServicingDisbursedFunds_Interest), nameof(sServicingCurrentTotalFunds_Interest),
            nameof(sServicingCollectedFunds_Escrow), nameof(sServicingDisbursedFunds_Escrow), nameof(sServicingCurrentTotalFunds_Escrow), nameof(sServicingCollectedFunds_Other),
            nameof(sServicingDisbursedFunds_Other), nameof(sServicingCurrentTotalFunds_Other), nameof(sServicingCollectedFunds_LateFees), nameof(sServicingDisbursedFunds_LateFees),
            nameof(sServicingCurrentTotalFunds_LateFees), nameof(sServicingDueFunds_Principal), nameof(sServicingTotalDueFunds_Principal), nameof(sServicingDueFunds_Interest),
            nameof(sServicingTotalDueFunds_Interest), nameof(sServicingDueFunds_Escrow), nameof(sServicingTotalDueFunds_Escrow), nameof(sServicingDueFunds_Other),
            nameof(sServicingTotalDueFunds_Other), nameof(sServicingDueFunds_LateFees), nameof(sServicingTotalDueFunds_LateFees))]
        private void SetTopServicingSummaryFields()
        {
            // Current Total summary field is the sum of the 3 fields above it in the table (Total loan amount, collected, and disbursed)
            sServicingCurrentTotalFunds_Principal = SumMoney(sServicingClosingFunds_Principal, sServicingCollectedFunds_Principal, sServicingDisbursedFunds_Principal);
            sServicingCurrentTotalFunds_Interest = SumMoney(sServicingClosingFunds_Interest, sServicingCollectedFunds_Interest, sServicingDisbursedFunds_Interest);
            sServicingCurrentTotalFunds_Escrow = SumMoney(sInitialDeposit_Escrow, sServicingCollectedFunds_Escrow, sServicingDisbursedFunds_Escrow);
            sServicingCurrentTotalFunds_Other = SumMoney(sServicingClosingFunds_Other, sServicingCollectedFunds_Other, sServicingDisbursedFunds_Other);
            sServicingCurrentTotalFunds_LateFees = SumMoney(sServicingClosingFunds_LateFees, sServicingCollectedFunds_LateFees, sServicingDisbursedFunds_LateFees);

            // Total w/pmts due summary field is the sum of all fields above it (the only diff between this and the total summary field is that it has the due amounts in it)
            sServicingTotalDueFunds_Principal = SumMoney(sServicingCurrentTotalFunds_Principal, sServicingDueFunds_Principal);
            sServicingTotalDueFunds_Interest = SumMoney(sServicingCurrentTotalFunds_Interest, sServicingDueFunds_Interest);
            sServicingTotalDueFunds_Escrow = SumMoney(sServicingCurrentTotalFunds_Escrow, sServicingDueFunds_Escrow);
            sServicingTotalDueFunds_Other = SumMoney(sServicingCurrentTotalFunds_Other, sServicingDueFunds_Other);
            sServicingTotalDueFunds_LateFees = SumMoney(sServicingCurrentTotalFunds_LateFees, sServicingDueFunds_LateFees);   
        }

        #region Principal Calc fields
        private decimal m_sServicingCollectedFunds_Principal = 0;
        private decimal m_sServicingDisbursedFunds_Principal = 0;
        private decimal m_sServicingCurrentTotalFunds_Principal = 0;
        private decimal m_sServicingDueFunds_Principal = 0;
        private decimal m_sServicingTotalDueFunds_Principal = 0;
        
        [DependsOn(nameof(sFinalLAmt))]
        private decimal sServicingClosingFunds_Principal
        {
            get  { return sFinalLAmt * -1; }
        }
        public string sServicingClosingFunds_Principal_rep
        {
            get { return ToMoneyString(() => sServicingClosingFunds_Principal); }
        }

        [DependsOn(nameof(sFinalLAmt), nameof(sServicingCollectedFunds_Principal))]
        private decimal sUPBAmount
        {
            get
            {
                return sFinalLAmt - m_sServicingCollectedFunds_Principal;
            }
        }
        public string sUPBAmount_rep
        {
            get { return ToMoneyString(() => sUPBAmount); }
        }

        [DependsOn(nameof(sServicingPayments), nameof(SetPaymentSummaryData))]
        private decimal sServicingCollectedFunds_Principal
        {
            get 
            {
                if (m_areServicingPmtFieldsSet == false)
                {
                    SetPaymentSummaryData(sServicingPayments);
                }
                return m_sServicingCollectedFunds_Principal; 
            }
            set { m_sServicingCollectedFunds_Principal = value; }
        }
        public string sServicingCollectedFunds_Principal_rep
        {
            get { return ToMoneyString(() => sServicingCollectedFunds_Principal); }
        }

        [DependsOn(nameof(sServicingCollectedFunds_Principal), nameof(sServicingDisbursedFunds_Principal))]
        private decimal sServicingCollectedAndDisbursedFunds_Principal
        {
            get { return SumMoney(sServicingCollectedFunds_Principal, sServicingDisbursedFunds_Principal); }
        }

        public string sServicingCollectedAndDisbursedFunds_Principal_rep
        {
            get { return ToMoneyString(() => sServicingCollectedAndDisbursedFunds_Principal); }
        }

        [DependsOn(nameof(sServicingCollectedFunds_Principal), nameof(sServicingDisbursedFunds_Principal), nameof(sServicingDueFunds_Principal))]
        private decimal sServicingCollectedAndDisbursedDueFunds_Principal
        {
            get { return SumMoney(sServicingCollectedFunds_Principal, sServicingDisbursedFunds_Principal, sServicingDueFunds_Principal); }
        }

        public string sServicingCollectedAndDisbursedDueFunds_Principal_rep
        {
            get { return ToMoneyString(() => sServicingCollectedAndDisbursedDueFunds_Principal); }
        }

        [DependsOn(nameof(sServicingPayments), nameof(SetPaymentSummaryData))]
        private decimal sServicingDisbursedFunds_Principal
        {
            get 
            {
                if (m_areServicingPmtFieldsSet == false)
                {
                    SetPaymentSummaryData(sServicingPayments);
                }
                return m_sServicingDisbursedFunds_Principal; 
            }
            set { m_sServicingDisbursedFunds_Principal = value; }
        }
        public string sServicingDisbursedFunds_Principal_rep
        {
            get { return ToMoneyString(() => sServicingDisbursedFunds_Principal); }
        }

        [DependsOn(nameof(sServicingPayments), nameof(SetPaymentSummaryData))]
        private decimal sServicingCurrentTotalFunds_Principal
        {
            get 
            {
                if (m_areServicingPmtFieldsSet == false)
                {
                    SetPaymentSummaryData(sServicingPayments);
                }
                return m_sServicingCurrentTotalFunds_Principal; 
            }
            set { m_sServicingCurrentTotalFunds_Principal = value; }
        }
        public string sServicingCurrentTotalFunds_Principal_rep
        {
            get { return ToMoneyString(() => sServicingCurrentTotalFunds_Principal); }
        }

        [DependsOn(nameof(sServicingPayments), nameof(SetPaymentSummaryData))]
        private decimal sServicingDueFunds_Principal
        {
            get 
            {
                if (m_areServicingPmtFieldsSet == false)
                {
                    SetPaymentSummaryData(sServicingPayments);
                }
                return m_sServicingDueFunds_Principal; 
            }
            set { m_sServicingDueFunds_Principal = value; }
        }
        public string sServicingDueFunds_Principal_rep
        {
            get { return ToMoneyString(() => sServicingDueFunds_Principal); }
        }

        [DependsOn(nameof(sServicingPayments), nameof(SetPaymentSummaryData))]
        private decimal sServicingTotalDueFunds_Principal
        {
            get 
            {
                if (m_areServicingPmtFieldsSet == false)
                {
                    SetPaymentSummaryData(sServicingPayments);
                }
                
                return m_sServicingTotalDueFunds_Principal; 
            }
            set { m_sServicingTotalDueFunds_Principal = value; }
        }
        public string sServicingTotalDueFunds_Principal_rep
        {
            get { return ToMoneyString(() => sServicingTotalDueFunds_Principal); }
        }

        [DependsOn(nameof(sServicingTotalDueFunds_Principal))]
        private decimal sServicingTotalDueFunds_Principal_neg
        {
            get
            {
                return -1*sServicingTotalDueFunds_Principal;
            }
        }
        public string sServicingTotalDueFunds_Principal_neg_rep
        {
            get { return ToMoneyString(() => sServicingTotalDueFunds_Principal_neg); }
        }

        // OPM 47973 - unpaid principal balance should be positive while total due funds principal is negative
        [DependsOn(nameof(sServicingTotalDueFunds_Principal))]
        public decimal sServicingUnpaidPrincipalBalance
        {
            get
            {
                return sServicingTotalDueFunds_Principal * -1;
            }
        }
        public string sServicingUnpaidPrincipalBalance_rep
        {
            get { return ToMoneyString(() => sServicingUnpaidPrincipalBalance); }
        }

        [DependsOn(nameof(sServicingUnpaidPrincipalBalance))]
        public decimal sProvExpServicingUnpaidPrincipalBalance
        {
            get { return decimal.Round(sServicingUnpaidPrincipalBalance, 2); }
        }

        [DependsOn(nameof(sServicingPayments))]
        public decimal sServicingTotalDue
        {
            get 
            {
                decimal totalDue = 0;
                foreach (CServicingPaymentFields spField in sServicingPayments)
                {
                    totalDue += spField.DueAmt;
                }
                return totalDue;
            }
        }
        public string sServicingTotalDue_rep
        {
            get { return ToMoneyString(() => sServicingTotalDue); }
        }

        [DependsOn(nameof(sServicingPayments))]
        public decimal sServicingTotalPayments
        {
            get 
            {
                decimal totalAmount = 0;
                foreach (CServicingPaymentFields spField in sServicingPayments)
                {
                    totalAmount += spField.PaymentAmt;
                }
                return totalAmount;
            }
        }
        public string sServicingTotalPayments_rep
        {
            get { return ToMoneyString(() => sServicingTotalPayments); }
        }

        [DependsOn(nameof(sServicingPayments))]
        private CServicingPaymentFields GetMonthPaymentCurrentlyDue()
        {
            DateTime dueDate = DateTime.MaxValue;
            CServicingPaymentFields earliestPayment = new CServicingPaymentFields();
            foreach (CServicingPaymentFields paymentFields in sServicingPayments)
            {
                // Skip non-payments
                if (paymentFields.ServicingTransactionT != sServicingTransT_map[1])
                {
                    continue;
                }

                // Only select payments that have a Due date, but no payment date
                if (false == string.IsNullOrEmpty(paymentFields.DueD_rep) &&
                    true == string.IsNullOrEmpty(paymentFields.PaymentD_rep))
                {
                    // Of those, find the payment with the earliest due date
                    if ( dueDate > paymentFields.DueD)
                    {
                        dueDate = paymentFields.DueD;
                        earliestPayment = paymentFields;
                    }
                }   
            }

            return earliestPayment;
        }

        public string sServicingCurrentPmtDueD_rep
        {
            get { return sServicingCurrentPmtDueD.ToString(this.m_convertLos); }
        }

        [DependsOn(nameof(GetMonthPaymentCurrentlyDue))]
        private decimal sServicingCurrentPmtAmt
        {
            get { return GetMonthPaymentCurrentlyDue().PaymentAmt; }
        }

        public string sServicingCurrentPmtAmt_rep
        {
            get { return ToMoneyString(() => sServicingCurrentPmtAmt); } 
        }

        [DependsOn(nameof(GetMonthPaymentCurrentlyDue))]
        private CDateTime sServicingCurrentPmtDueD
        {
            get { return CDateTime.Create(GetMonthPaymentCurrentlyDue().DueD); }
        }

        [DependsOn(nameof(GetMonthPaymentCurrentlyDue))]
        private decimal sServicingCurrentPmtDueAmt
        {
            get { return GetMonthPaymentCurrentlyDue().DueAmt; }
        }

        public string sServicingCurrentPmtDueAmt_rep
        {
            get { return ToMoneyString(() => sServicingCurrentPmtDueAmt); }
        }

        [DependsOn(nameof(GetMonthPaymentCurrentlyDue))]
        private decimal sServicingCurrentPmtDueEscrow
        {
            get
            {
                return GetMonthPaymentCurrentlyDue().Escrow;
            }
        }

        public string sServicingCurrentPmtDueEscrow_rep
        {
            get { return ToMoneyString(() => sServicingCurrentPmtDueEscrow); }
        }

        [DependsOn(nameof(GetMonthPaymentCurrentlyDue))]
        private decimal sServicingCurrentPmtDueInterest
        {
            get
            {
                return GetMonthPaymentCurrentlyDue().Interest;
            }
        }

        public string sServicingCurrentPmtDueInterest_rep
        {
            get { return ToMoneyString(() => sServicingCurrentPmtDueInterest); }
        }

        [DependsOn(nameof(GetMonthPaymentCurrentlyDue))]
        public string sServicingCurrentPmtDueNotes
        {
            get { return GetMonthPaymentCurrentlyDue().Notes; }
        }

        [DependsOn(nameof(GetMonthPaymentCurrentlyDue))]
        private decimal sServicingCurrentPmtPrincipal
        {
            get { return GetMonthPaymentCurrentlyDue().Principal; }
        }

        public string sServicingCurrentPmtPrincipal_rep
        {
            get { return ToMoneyString(() => sServicingCurrentPmtPrincipal); }
        }


        [DependsOn(nameof(GetMonthPaymentCurrentlyDue))]
        private decimal sServicingCurrentPmtOther
        {
            get { return GetMonthPaymentCurrentlyDue().Other; }
        }

        public string sServicingCurrentPmtOther_rep
        {
            get { return ToMoneyString(() => sServicingCurrentPmtOther); }
        }

        [DependsOn(nameof(sServicingPayments))]
        private CServicingPaymentFields GetMonthPaymentLastDue()
        {
            DateTime dueDate = DateTime.MinValue;
            CServicingPaymentFields earliestPayment = new CServicingPaymentFields();
            foreach (CServicingPaymentFields paymentFields in sServicingPayments)
            {
                // Skip non-payments
                if (paymentFields.ServicingTransactionT != CPageBase.sServicingTransT_map[1])
                {
                    continue;
                }

                // Only select payments that have a Due date
                if (false == string.IsNullOrEmpty(paymentFields.DueD_rep))
                {
                    // Of those, find the payment with the latest due date
                    if (dueDate < paymentFields.DueD)
                    {
                        dueDate = paymentFields.DueD;
                        earliestPayment = paymentFields;
                    }
                }
            }

            return earliestPayment;
        }

        [DependsOn(nameof(GetMonthPaymentLastDue), nameof(sSchedDueD1))]
        private CDateTime sServicingLastPmtDueD
        {
            get
            {
                CServicingPaymentFields lastDuePayment = GetMonthPaymentLastDue();

                if (!string.IsNullOrEmpty(lastDuePayment.DueD_rep))
                {
                    return CDateTime.Create(lastDuePayment.DueD);
                }
                else if (sSchedDueD1.IsValid)
                {
                    return CDateTime.Create(sSchedDueD1.DateTimeForComputation.AddMonths(-1));
                }

                return CDateTime.InvalidWrapValue;
            }
        }

        public string sServicingLastPmtDueD_rep
        {
            get { return sServicingLastPmtDueD.ToString(this.m_convertLos); }
        }

        [DependsOn]
        private decimal sServicingCurrentArmIndexR
        {
            get { return GetRateField("sServicingCurrentArmIndexR"); }
            set { SetRateField("sServicingCurrentArmIndexR", value); }
        }

        public string sServicingCurrentArmIndexR_rep
        {
            get { return ToRateString(() => sServicingCurrentArmIndexR); }
            set { sServicingCurrentArmIndexR = ToRate(value); }
        }

        [DependsOn(nameof(sServicingCurrentArmIndexR))]
        public decimal sProvExpServicingCurrentArmIndexR
        {
            get { return decimal.Round((sServicingCurrentArmIndexR / 100), 5); }
        }

        [DependsOn]
        public E_TaxLateChargeT sServicingCurrentLateChargeT
        {
            get { return (E_TaxLateChargeT)GetTypeIndexField("sServicingCurrentLateChargeT"); }
            set { SetTypeIndexField("sServicingCurrentLateChargeT", (int)value); }
        }


        [DependsOn]
        public E_BillingMethodT sBillingMethodT
        {
            get { return (E_BillingMethodT)GetTypeIndexField<byte>("sBillingMethodT"); }
            set { SetTypeIndexField("sBillingMethodT", (int)value); }
        }


        [DependsOn]
        public E_BillingAccountLocationT sBillingAccountLocationT
        {
            get { return (E_BillingAccountLocationT)GetTypeIndexField<byte>("sBillingAccountLocationT"); }
            set { SetTypeIndexField("sBillingAccountLocationT", (int)value); }
        }


        [DependsOn]
        public Sensitive<string> sBillingABANum
        {
            get { return GetStringVarCharField("sBillingABANum"); }
            set { SetStringVarCharField("sBillingABANum", value.Value); }
        }


        [DependsOn]
        public Sensitive<string> sBillingAccountNum
        {
            get { return GetStringVarCharField("sBillingAccountNum"); }
            set { SetStringVarCharField("sBillingAccountNum", value.Value); }
        }


        [DependsOn]
        public string sBillingNameOnAccount
        {
            get { return GetStringVarCharField("sBillingNameOnAccount"); }
            set { SetStringVarCharField("sBillingNameOnAccount", value); }
        }


        [DependsOn]
        public E_BillingAccountT sBillingAccountT
        {
            get { return (E_BillingAccountT)GetTypeIndexField<byte>("sBillingAccountT"); }
            set { SetTypeIndexField("sBillingAccountT", (int)value); }
        }


        [DependsOn]
        public decimal sBillingAdditionalAmt
        {
            get { return GetRateField("sBillingAdditionalAmt"); }
            set { SetRateField("sBillingAdditionalAmt", value); }
        }

        public string sBillingAdditionalAmt_rep
        {
            get { return ToMoneyString(() => sBillingAdditionalAmt); }
            set { sBillingAdditionalAmt = ToMoney(value); }
        }

        [DependsOn(nameof(sTaxTable1008DescT), nameof(sIsHousingExpenseMigrated), nameof(sOtherTax1Expense))]
        public E_TableSettlementDescT sTaxTable1008DescT
        {
            get 
            {
                if (this.sIsHousingExpenseMigrated)
                {
                    return Tools.LineNumToSettlementDescT(this.sOtherTax1Expense.CustomExpenseLineNum);
                }
                else
                {
                    return (E_TableSettlementDescT)GetTypeIndexField("sTaxTable1008DescT");
                }
            }
            set 
            {
                if (this.sIsHousingExpenseMigrated)
                {
                    this.sOtherTax1Expense.SetCustomLineNumber(Tools.SettlementDescTToLineNum(value));
                }
                else
                {
                    SetCountField("sTaxTable1008DescT", (int)value);
                }
            }
        }

        [DependsOn(nameof(sTaxTable1009DescT), nameof(sIsHousingExpenseMigrated), nameof(sOtherTax2Expense))]
        public E_TableSettlementDescT sTaxTable1009DescT
        {
            get 
            {
                if (this.sIsHousingExpenseMigrated)
                {
                    return Tools.LineNumToSettlementDescT(this.sOtherTax2Expense.CustomExpenseLineNum);
                }
                else
                {
                    return (E_TableSettlementDescT)GetTypeIndexField("sTaxTable1009DescT");
                }
            }
            set 
            {
                if (this.sIsHousingExpenseMigrated)
                {
                    this.sOtherTax2Expense.SetCustomLineNumber(Tools.SettlementDescTToLineNum(value));
                }
                else
                {
                    SetTypeIndexField("sTaxTable1009DescT", (int)value);
                }
            }
        }

        [DependsOn(nameof(sTaxTableU3DescT), nameof(sIsHousingExpenseMigrated), nameof(sOtherTax3Expense))]
        public E_TableSettlementDescT sTaxTableU3DescT
        {
            get 
            {
                if (!BrokerDB.EnableAdditionalSection1000CustomFees)
                {
                    return E_TableSettlementDescT.NoInfo;
                }

                if (this.sIsHousingExpenseMigrated)
                {
                    return Tools.LineNumToSettlementDescT(this.sOtherTax3Expense.CustomExpenseLineNum);
                }
                else
                {
                    return (E_TableSettlementDescT)GetTypeIndexField("sTaxTableU3DescT");
                }
            }
            set 
            {
                if (this.sIsHousingExpenseMigrated)
                {
                    this.sOtherTax3Expense.SetCustomLineNumber(Tools.SettlementDescTToLineNum(value));
                }
                else
                {
                    SetTypeIndexField("sTaxTableU3DescT", (int)value);
                }
            }
        }

        [DependsOn(nameof(sTaxTableU4DescT), nameof(sIsHousingExpenseMigrated), nameof(sOtherTax4Expense))]
        public E_TableSettlementDescT sTaxTableU4DescT
        {
            get 
            {
                if (!BrokerDB.EnableAdditionalSection1000CustomFees)
                {
                    return E_TableSettlementDescT.NoInfo;
                }

                if (this.sIsHousingExpenseMigrated)
                {
                    return Tools.LineNumToSettlementDescT(this.sOtherTax4Expense.CustomExpenseLineNum);
                }
                else
                {
                    return (E_TableSettlementDescT)GetTypeIndexField("sTaxTableU4DescT");
                }
            }
            set 
            {
                if (this.sIsHousingExpenseMigrated)
                {
                    this.sOtherTax4Expense.SetCustomLineNumber(Tools.SettlementDescTToLineNum(value));
                }
                else
                {
                    SetTypeIndexField("sTaxTableU4DescT", (int)value);
                }
            }
        }

        [DependsOn(nameof(sTaxTableRealETxT), nameof(sIsHousingExpenseMigrated), nameof(sRealEstateTaxExpense))]
        public E_TaxTableTaxT sTaxTableRealETxT
        {
            get 
            {
                if (this.sIsHousingExpenseMigrated)
                {
                    return this.sRealEstateTaxExpense.TaxType;
                }
                else
                {
                    return (E_TaxTableTaxT)GetTypeIndexField("sTaxTableRealETxT");
                }
            }
            set 
            {
                if (this.sIsHousingExpenseMigrated)
                {
                    this.sRealEstateTaxExpense.TaxType = value;
                }
                else
                {
                    SetTypeIndexField("sTaxTableRealETxT", (int)value);
                }
            }
        }

        [DependsOn(nameof(sTaxTableSchoolT), nameof(sSchoolTaxExpense), nameof(sSchoolTaxExpense), nameof(sIsHousingExpenseMigrated))]
        public E_TaxTableTaxT sTaxTableSchoolT
        {
            get 
            { 
                if(this.sIsHousingExpenseMigrated)
                {
                    return this.sSchoolTaxExpense.TaxType;
                }
                else
                {
                    return (E_TaxTableTaxT)GetTypeIndexField("sTaxTableSchoolT"); 
                }
            }
            set 
            {
                if(this.sIsHousingExpenseMigrated)
                {
                    this.sSchoolTaxExpense.TaxType = value;
                }
                else
                {
                    SetTypeIndexField("sTaxTableSchoolT", (int)value); 
                }
            }
        }

        [DependsOn(nameof(sTaxTable1008T), nameof(sIsHousingExpenseMigrated), nameof(sOtherTax1Expense))]
        public E_TaxTableTaxT sTaxTable1008T
        {
            get 
            {
                if (this.sIsHousingExpenseMigrated)
                {
                    return this.sOtherTax1Expense.TaxType;
                }
                else
                {
                    return (E_TaxTableTaxT)GetTypeIndexField("sTaxTable1008T");
                }
            }
            set 
            {
                if (this.sIsHousingExpenseMigrated)
                {
                    this.sOtherTax1Expense.TaxType = value;
                }
                else
                {
                    SetTypeIndexField("sTaxTable1008T", (int)value);
                }
            }
        }
        
        [DependsOn(nameof(sTaxTable1009T), nameof(sIsHousingExpenseMigrated), nameof(sOtherTax2Expense))]
        public E_TaxTableTaxT sTaxTable1009T
        {
            get 
            {
                if (this.sIsHousingExpenseMigrated)
                {
                    return this.sOtherTax2Expense.TaxType;
                }
                else
                {
                    return (E_TaxTableTaxT)GetTypeIndexField("sTaxTable1009T");
                }
            }
            set 
            {
                if (this.sIsHousingExpenseMigrated)
                {
                    this.sOtherTax2Expense.TaxType = value;
                }
                else
                {
                    SetTypeIndexField("sTaxTable1009T", (int)value);
                }
            }
        }

        [DependsOn(nameof(sTaxTableU3T), nameof(sIsHousingExpenseMigrated), nameof(sOtherTax3Expense))]
        public E_TaxTableTaxT sTaxTableU3T
        {
            get 
            { 
                if (!BrokerDB.EnableAdditionalSection1000CustomFees)
                {
                    return E_TaxTableTaxT.LeaveBlank;
                }
                if (this.sIsHousingExpenseMigrated)
                {
                    return this.sOtherTax3Expense.TaxType;
                }
                else
                {
                    return (E_TaxTableTaxT)GetTypeIndexField("sTaxTableU3T");
                }
            }
            set 
            {
                if (this.sIsHousingExpenseMigrated)
                {
                    this.sOtherTax3Expense.TaxType = value;
                }
                else
                {
                    SetTypeIndexField("sTaxTableU3T", (int)value);
                }
            }
        }

        [DependsOn(nameof(sTaxTableU4T), nameof(sIsHousingExpenseMigrated), nameof(sOtherTax4Expense))]
        public E_TaxTableTaxT sTaxTableU4T
        {
            get 
            { 
                if (!BrokerDB.EnableAdditionalSection1000CustomFees)
                {
                    return E_TaxTableTaxT.LeaveBlank;
                }

                if (this.sIsHousingExpenseMigrated)
                {
                    return this.sOtherTax4Expense.TaxType;
                }
                else
                {
                    return (E_TaxTableTaxT)GetTypeIndexField("sTaxTableU4T");
                }
            }
            set 
            {
                if (this.sIsHousingExpenseMigrated)
                {
                    this.sOtherTax4Expense.TaxType = value;
                }
                else
                {
                    SetTypeIndexField("sTaxTableU4T", (int)value);
                }
            }
        }

        [DependsOn(nameof(sTaxTableRealETxPaidByT), nameof(sSettlementRealETxRsrvMon))]
        public E_TaxTablePaidBy sTaxTableRealETxPaidByT
        {
            get //OPM 84258
            {
                int typeIndex = GetTypeIndexField("sTaxTableRealETxPaidByT");
                if(typeIndex!=0)
                    return (E_TaxTablePaidBy)typeIndex;
                
                if (sSettlementRealETxRsrvMon > 0)
                    return E_TaxTablePaidBy.Lender;
                else
                    return E_TaxTablePaidBy.Borrower;
            }
            set { SetTypeIndexField("sTaxTableRealETxPaidByT", (int)value); }
        }

        [DependsOn(nameof(sTaxTableSchoolPaidByT), nameof(sSettlementSchoolTxRsrvMon))]
        public E_TaxTablePaidBy sTaxTableSchoolPaidByT
        {
            get //OPM 84258
            {
                int typeIndex = GetTypeIndexField("sTaxTableSchoolPaidByT");
                if (typeIndex != 0)
                    return (E_TaxTablePaidBy)typeIndex;

                if (sSettlementSchoolTxRsrvMon > 0)
                    return E_TaxTablePaidBy.Lender;
                else
                    return E_TaxTablePaidBy.Borrower;
            }
            set { SetTypeIndexField("sTaxTableSchoolPaidByT", (int)value); }
        }

        [DependsOn(nameof(sTaxTable1008PaidByT), nameof(sSettlement1008RsrvMon))]
        public E_TaxTablePaidBy sTaxTable1008PaidByT
        {
            get //OPM 84258
            {
                int typeIndex = GetTypeIndexField("sTaxTable1008PaidByT");
                if (typeIndex != 0)
                    return (E_TaxTablePaidBy)typeIndex;

                if (sSettlement1008RsrvMon > 0)
                    return E_TaxTablePaidBy.Lender;
                else
                    return E_TaxTablePaidBy.Borrower;
            }
            set { SetTypeIndexField("sTaxTable1008PaidByT", (int)value); }
        }

        [DependsOn(nameof(sTaxTable1009PaidByT), nameof(sSettlement1009RsrvMon))]
        public E_TaxTablePaidBy sTaxTable1009PaidByT
        {
            get //OPM 84258
            {
                int typeIndex = GetTypeIndexField("sTaxTable1009PaidByT");
                if (typeIndex != 0)
                    return (E_TaxTablePaidBy)typeIndex;

                if (sSettlement1009RsrvMon > 0)
                    return E_TaxTablePaidBy.Lender;
                else
                    return E_TaxTablePaidBy.Borrower;
            }
            set { SetTypeIndexField("sTaxTable1009PaidByT", (int)value); }
        }

        [DependsOn(nameof(sTaxTableU3PaidByT), nameof(sSettlementU3RsrvMon))]
        public E_TaxTablePaidBy sTaxTableU3PaidByT
        {
            get
            {
                int typeIndex = GetTypeIndexField("sTaxTableU3PaidByT");
                if (typeIndex != 0)
                    return (E_TaxTablePaidBy)typeIndex;

                if (sSettlementU3RsrvMon > 0)
                    return E_TaxTablePaidBy.Lender;
                else
                    return E_TaxTablePaidBy.Borrower;
            }
            set { SetTypeIndexField("sTaxTableU3PaidByT", (int)value); }
        }

        [DependsOn(nameof(sTaxTableU4PaidByT), nameof(sSettlementU4RsrvMon))]
        public E_TaxTablePaidBy sTaxTableU4PaidByT
        {
            get
            {
                int typeIndex = GetTypeIndexField("sTaxTableU4PaidByT");
                if (typeIndex != 0)
                    return (E_TaxTablePaidBy)typeIndex;

                if (sSettlementU4RsrvMon > 0)
                    return E_TaxTablePaidBy.Lender;
                else
                    return E_TaxTablePaidBy.Borrower;
            }
            set { SetTypeIndexField("sTaxTableU4PaidByT", (int)value); }
        }

        [DependsOn(nameof(sTaxTableRealETxPaidByT))]
        public E_TaxTablePaidBy sProvExpTaxItemPaidByT
        {
            get
            {
                // For now, default to using the info for real estate taxes,
                // as they only want one value for all taxes. - OPM 111115
                return sTaxTableRealETxPaidByT;
            }
        }

        [DependsOn]
        public string sTaxTableRealETxParcelNum
        {
            get { return GetStringVarCharField("sTaxTableRealETxParcelNum"); }
            set { SetStringVarCharField("sTaxTableRealETxParcelNum", value); }
        }

        [DependsOn]
        public string sTaxTableSchoolParcelNum
        {
            get { return GetStringVarCharField("sTaxTableSchoolParcelNum"); }
            set { SetStringVarCharField("sTaxTableSchoolParcelNum", value); }
        }

        [DependsOn]
        public string sTaxTable1008ParcelNum
        {
            get { return GetStringVarCharField("sTaxTable1008ParcelNum"); }
            set { SetStringVarCharField("sTaxTable1008ParcelNum", value); }
        }

        [DependsOn]
        public string sTaxTable1009ParcelNum
        {
            get { return GetStringVarCharField("sTaxTable1009ParcelNum"); }
            set { SetStringVarCharField("sTaxTable1009ParcelNum", value); }
        }

        [DependsOn]
        public string sTaxTableU3ParcelNum
        {
            get 
            { 
                if (!BrokerDB.EnableAdditionalSection1000CustomFees)
                {
                    return "";
                }
                return GetStringVarCharField("sTaxTableU3ParcelNum"); 
            }
            set { SetStringVarCharField("sTaxTableU3ParcelNum", value); }
        }

        [DependsOn]
        public string sTaxTableU4ParcelNum
        {
            get 
            { 
                if (!BrokerDB.EnableAdditionalSection1000CustomFees)
                {
                    return "";
                }
                return GetStringVarCharField("sTaxTableU4ParcelNum"); 
            }
            set { SetStringVarCharField("sTaxTableU4ParcelNum", value); }
        }

        [DependsOn]
        public string sTaxTableRealEtxPayeeNm
        {
            get { return GetStringVarCharField("sTaxTableRealEtxPayeeNm"); }
            set { SetStringVarCharField("sTaxTableRealEtxPayeeNm", value); }
        }

        [DependsOn]
        public string sTaxTableSchoolPayeeNm
        {
            get { return GetStringVarCharField("sTaxTableSchoolPayeeNm"); }
            set { SetStringVarCharField("sTaxTableSchoolPayeeNm", value); }
        }

        [DependsOn]
        public string sTaxTable1008PayeeNm
        {
            get { return GetStringVarCharField("sTaxTable1008PayeeNm"); }
            set { SetStringVarCharField("sTaxTable1008PayeeNm", value); }
        }

        [DependsOn]
        public string sTaxTable1009PayeeNm
        {
            get { return GetStringVarCharField("sTaxTable1009PayeeNm"); }
            set { SetStringVarCharField("sTaxTable1009PayeeNm", value); }
        }

        [DependsOn]
        public string sTaxTableU3PayeeNm
        {
            get 
            { 
                if (!BrokerDB.EnableAdditionalSection1000CustomFees)
                {
                    return "";
                }
                return GetStringVarCharField("sTaxTableU3PayeeNm"); 
            }
            set { SetStringVarCharField("sTaxTableU3PayeeNm", value); }
        }

        [DependsOn]
        public string sTaxTableU4PayeeNm
        {
            get 
            { 
                if (!BrokerDB.EnableAdditionalSection1000CustomFees)
                {
                    return "";
                }
                return GetStringVarCharField("sTaxTableU4PayeeNm"); 
            }
            set { SetStringVarCharField("sTaxTableU4PayeeNm", value); }
        }

        [DependsOn]
        public int sTaxTableRealEtxPayeeId
        {
            get { return GetCountField("sTaxTableRealEtxPayeeId"); }
            set { SetCountField("sTaxTableRealEtxPayeeId", value); }
        }

        public string sTaxTableRealEtxPayeeId_rep
        {
            get { return ToCountString(() => sTaxTableRealEtxPayeeId); }
            set { sTaxTableRealEtxPayeeId = ToCount(value); }
        }

        [DependsOn]
        public int sTaxTableSchoolPayeeId
        {
            get { return GetCountField("sTaxTableSchoolPayeeId"); }
            set { SetCountField("sTaxTableSchoolPayeeId", value); }
        }

        public string sTaxTableSchoolPayeeId_rep
        {
            get { return ToCountString(() => sTaxTableSchoolPayeeId); }
            set { sTaxTableSchoolPayeeId = ToCount(value); }
        }

        [DependsOn]
        public int sTaxTable1008PayeeId
        {
            get { return GetCountField("sTaxTable1008PayeeId"); }
            set { SetCountField("sTaxTable1008PayeeId", value); }
        }

        public string sTaxTable1008PayeeId_rep
        {
            get { return ToCountString(() => sTaxTable1008PayeeId); }
            set { sTaxTable1008PayeeId = ToCount(value); }
        }
        [DependsOn]
        public int sTaxTable1009PayeeId
        {
            get { return GetCountField("sTaxTable1009PayeeId"); }
            set { SetCountField("sTaxTable1009PayeeId", value); }
        }
        public string sTaxTable1009PayeeId_rep
        {
            get { return ToCountString(() => sTaxTable1009PayeeId); }
            set { sTaxTable1009PayeeId = ToCount(value); }
        }

        [DependsOn]
        public int sTaxTableU3PayeeId
        {
            get 
            { 
                if (!BrokerDB.EnableAdditionalSection1000CustomFees)
                {
                    return 0;
                }
                return GetCountField("sTaxTableU3PayeeId"); 
            }
            set { SetCountField("sTaxTableU3PayeeId", value); }
        }
        public string sTaxTableU3PayeeId_rep
        {
            get { return ToCountString(() => sTaxTableU3PayeeId); }
            set { sTaxTableU3PayeeId = ToCount(value); }
        }

        [DependsOn]
        public int sTaxTableU4PayeeId
        {
            get 
            {
                if (!BrokerDB.EnableAdditionalSection1000CustomFees)
                {
                    return 0;
                }
                return GetCountField("sTaxTableU4PayeeId"); 
            }
            set { SetCountField("sTaxTableU4PayeeId", value); }
        }
        public string sTaxTableU4PayeeId_rep
        {
            get { return ToCountString(() => sTaxTableU4PayeeId); }
            set { sTaxTableU4PayeeId = ToCount(value); }
        }

        [DependsOn]
        public string sTaxTableRealEtxNumOfPmt
        {
            get { return GetStringCharField("sTaxTableRealEtxNumOfPmt"); }
            set { SetStringCharField("sTaxTableRealEtxNumOfPmt", value); }
        }

        [DependsOn(nameof(sTaxTableRealEtxNumOfPmt))]
        public int sTaxTableRealEtxNumOfNextPmt
        {
            get
            {
                int pmtNum;
                if (int.TryParse(sTaxTableRealEtxNumOfPmt, out pmtNum))
                {
                    return pmtNum + 1;
                }

                return 0;
            }
        }

        [DependsOn]
        public string sTaxTableSchoolNumOfPmt
        {
            get { return GetStringCharField("sTaxTableSchoolNumOfPmt"); }
            set { SetStringCharField("sTaxTableSchoolNumOfPmt", value); }
        }

        [DependsOn(nameof(sTaxTableSchoolNumOfPmt))]
        public int sTaxTableSchoolNumOfNextPmt
        {
            get
            {
                int pmtNum;
                if (int.TryParse(sTaxTableSchoolNumOfPmt, out pmtNum))
                {
                    return pmtNum + 1;
                }
                return 0;
            }
        }

        [DependsOn]
        public string sTaxTable1008NumOfPmt
        {
            get { return GetStringCharField("sTaxTable1008NumOfPmt"); }
            set { SetStringCharField("sTaxTable1008NumOfPmt", value); }
        }

        [DependsOn(nameof(sTaxTable1008NumOfPmt))]
        public int sTaxTable1008NumOfNextPmt
        {
            get
            {
                int pmtNum;
                if (int.TryParse(sTaxTable1008NumOfPmt, out pmtNum))
                {
                    return pmtNum + 1;
                }
                
                return 0;
            }
        }


        [DependsOn]
        public string sTaxTable1009NumOfPmt
        {
            get { return GetStringCharField("sTaxTable1009NumOfPmt"); }
            set { SetStringCharField("sTaxTable1009NumOfPmt", value); }
        }

        [DependsOn(nameof(sTaxTable1009NumOfPmt))]
        public int sTaxTable1009NumOfNextPmt
        {
            get
            {
                int pmtNum;
                if (int.TryParse(sTaxTable1009NumOfPmt, out pmtNum))
                {
                    return pmtNum + 1;
                }

                return 0;
            }
        }

        [DependsOn]
        public string sTaxTableU3NumOfPmt
        {
            get
            {
                if (!BrokerDB.EnableAdditionalSection1000CustomFees)
                {
                    return "";
                }
                return GetStringCharField("sTaxTableU3NumOfPmt"); 
            }
            set { SetStringCharField("sTaxTableU3NumOfPmt", value); }
        }

        [DependsOn]
        public string sTaxTableU4NumOfPmt
        {
            get 
            {
                if (!BrokerDB.EnableAdditionalSection1000CustomFees)
                {
                    return "";
                }
                return GetStringCharField("sTaxTableU4NumOfPmt"); 
            }
            set { SetStringCharField("sTaxTableU4NumOfPmt", value); }
        }

        [DependsOn(nameof(sTaxTableRealEtxDueD), nameof(sTaxTableRealEtxDueDLckd), nameof(sRealETxScheduleItems))]
        public CDateTime sTaxTableRealEtxDueD
        {
            get
            {
                if (sTaxTableRealEtxDueDLckd)
                    return GetDateTimeField("sTaxTableRealEtxDueD");
                var taxScheduleItem = sRealETxScheduleItems.FirstOrDefault();
                return taxScheduleItem == null ? CDateTime.InvalidWrapValue
                    : CDateTime.Create(taxScheduleItem.DueD_rep, m_convertLos);
            }
            set { SetDateTimeField("sTaxTableRealEtxDueD", value); }
        }
        public string sTaxTableRealEtxDueD_rep
        {
            get { return sTaxTableRealEtxDueD.ToString(m_convertLos); }
            set { SetDateTimeField_rep("sTaxTableRealEtxDueD", value); }
        }
        [DependsOn]
        public bool sTaxTableRealEtxDueDLckd
        {
            get { return GetBoolField("sTaxTableRealEtxDueDLckd"); }
            set { SetBoolField("sTaxTableRealEtxDueDLckd", value); }
        }
        [DependsOn]
        public CDateTime sTaxTableRealEtxPaidD
        {
            get { return GetDateTimeField("sTaxTableRealEtxPaidD"); }
            set { SetDateTimeField("sTaxTableRealEtxPaidD", value); }
        }
        public string sTaxTableRealEtxPaidD_rep
        {
            get { return GetDateTimeField_rep("sTaxTableRealEtxPaidD"); }
            set { SetDateTimeField_rep("sTaxTableRealEtxPaidD", value); }
        }

        [DependsOn(nameof(sTaxTableSchoolDueD), nameof(sTaxTableSchoolDueDLckd), nameof(sSchoolTxScheduleItems))]
        public CDateTime sTaxTableSchoolDueD
        {
            get
            {
                if (sTaxTableSchoolDueDLckd)
                    return GetDateTimeField("sTaxTableSchoolDueD");
                var taxScheduleItem = sSchoolTxScheduleItems.FirstOrDefault();
                return taxScheduleItem == null ? CDateTime.InvalidWrapValue
                    : CDateTime.Create(taxScheduleItem.DueD_rep, m_convertLos);
            }
            set { SetDateTimeField("sTaxTableSchoolDueD", value); }
        }
        public string sTaxTableSchoolDueD_rep
        {
            get { return sTaxTableSchoolDueD.ToString(m_convertLos); }
            set { SetDateTimeField_rep("sTaxTableSchoolDueD", value); }
        }
        [DependsOn]
        public bool sTaxTableSchoolDueDLckd
        {
            get { return GetBoolField("sTaxTableSchoolDueDLckd"); }
            set { SetBoolField("sTaxTableSchoolDueDLckd", value); }
        }
        [DependsOn]
        public CDateTime sTaxTableSchoolPaidD
        {
            get { return GetDateTimeField("sTaxTableSchoolPaidD"); }
            set { SetDateTimeField("sTaxTableSchoolPaidD", value); }
        }
        public string sTaxTableSchoolPaidD_rep
        {
            get { return GetDateTimeField_rep("sTaxTableSchoolPaidD"); }
            set { SetDateTimeField_rep("sTaxTableSchoolPaidD", value); }
        }

        [DependsOn(nameof(sTaxTable1008DueD), nameof(sTaxTable1008DueDLckd), nameof(sTaxTableTaxDDL1ScheduleItems))]
        public CDateTime sTaxTable1008DueD
        {
            get
            {
                if (sTaxTable1008DueDLckd)
                    return GetDateTimeField("sTaxTable1008DueD");
                var taxScheduleItem = sTaxTableTaxDDL1ScheduleItems.FirstOrDefault();
                return taxScheduleItem == null ? CDateTime.InvalidWrapValue
                    : CDateTime.Create(taxScheduleItem.DueD_rep, m_convertLos);
            }
            set { SetDateTimeField("sTaxTable1008DueD", value); }
        }
        public string sTaxTable1008DueD_rep
        {
            get { return sTaxTable1008DueD.ToString(m_convertLos); }
            set { SetDateTimeField_rep("sTaxTable1008DueD", value); }
        }
        [DependsOn]
        public bool sTaxTable1008DueDLckd
        {
            get { return GetBoolField("sTaxTable1008DueDLckd"); }
            set { SetBoolField("sTaxTable1008DueDLckd", value); }
        }
        [DependsOn]
        public CDateTime sTaxTable1008PaidD
        {
            get { return GetDateTimeField("sTaxTable1008PaidD"); }
            set { SetDateTimeField("sTaxTable1008PaidD", value); }
        }
        public string sTaxTable1008PaidD_rep
        {
            get { return GetDateTimeField_rep("sTaxTable1008PaidD"); }
            set { SetDateTimeField_rep("sTaxTable1008PaidD", value); }
        }

        [DependsOn(nameof(sTaxTable1009DueD), nameof(sTaxTable1009DueDLckd), nameof(sTaxTableTaxDDL2ScheduleItems))]
        public CDateTime sTaxTable1009DueD
        {
            get
            {
                if (sTaxTable1009DueDLckd)
                    return GetDateTimeField("sTaxTable1009DueD");
                var taxScheduleItem = sTaxTableTaxDDL2ScheduleItems.FirstOrDefault();
                return taxScheduleItem == null ? CDateTime.InvalidWrapValue
                    : CDateTime.Create(taxScheduleItem.DueD_rep, m_convertLos);
            }
            set { SetDateTimeField("sTaxTable1009DueD", value); }
        }
        public string sTaxTable1009DueD_rep
        {
            get { return sTaxTable1009DueD.ToString(m_convertLos); }
            set { SetDateTimeField_rep("sTaxTable1009DueD", value); }
        }
        [DependsOn]
        public bool sTaxTable1009DueDLckd
        {
            get { return GetBoolField("sTaxTable1009DueDLckd"); }
            set { SetBoolField("sTaxTable1009DueDLckd", value); }
        }
        [DependsOn]
        public CDateTime sTaxTable1009PaidD
        {
            get { return GetDateTimeField("sTaxTable1009PaidD"); }
            set { SetDateTimeField("sTaxTable1009PaidD", value); }
        }
        public string sTaxTable1009PaidD_rep
        {
            get { return GetDateTimeField_rep("sTaxTable1009PaidD"); }
            set { SetDateTimeField_rep("sTaxTable1009PaidD", value); }
        }

        [DependsOn(nameof(sTaxTableU3DueD), nameof(sTaxTableU3DueDLckd), nameof(sU3TaxScheduleItems))]
        public CDateTime sTaxTableU3DueD
        {
            get
            {
                if (!BrokerDB.EnableAdditionalSection1000CustomFees)
                {
                    return CDateTime.InvalidWrapValue;
                }

                if (sTaxTableU3DueDLckd)
                    return GetDateTimeField("sTaxTableU3DueD");
                var taxScheduleItem = sU3TaxScheduleItems.FirstOrDefault();
                return taxScheduleItem == null ? CDateTime.InvalidWrapValue 
                    : CDateTime.Create(taxScheduleItem.DueD_rep, m_convertLos);
            }
            set { SetDateTimeField("sTaxTableU3DueD", value); }
        }
        public string sTaxTableU3DueD_rep
        {
            get { return sTaxTableU3DueD.ToString(m_convertLos); }
            set { SetDateTimeField_rep("sTaxTableU3DueD", value); }
        }
        [DependsOn]
        public bool sTaxTableU3DueDLckd
        {
            get 
            { 
                if (!BrokerDB.EnableAdditionalSection1000CustomFees)
                {
                    return false;
                }
                return GetBoolField("sTaxTableU3DueDLckd"); 
            }
            set { SetBoolField("sTaxTableU3DueDLckd", value); }
        }
        [DependsOn]
        public CDateTime sTaxTableU3PaidD
        {
            get 
            { 
                if (!BrokerDB.EnableAdditionalSection1000CustomFees)
                {
                    return CDateTime.InvalidWrapValue;
                }
                return GetDateTimeField("sTaxTableU3PaidD"); 
            }
            set { SetDateTimeField("sTaxTableU3PaidD", value); }
        }
        public string sTaxTableU3PaidD_rep
        {
            get { return GetDateTimeField_rep("sTaxTableU3PaidD"); }
            set { SetDateTimeField_rep("sTaxTableU3PaidD", value); }
        }

        [DependsOn(nameof(sTaxTableU4DueD), nameof(sTaxTableU4DueDLckd), nameof(sU4TaxScheduleItems))]
        public CDateTime sTaxTableU4DueD
        {
            get
            {
                if (!BrokerDB.EnableAdditionalSection1000CustomFees)
                {
                    return CDateTime.InvalidWrapValue;
                }

                if (sTaxTableU4DueDLckd)
                    return GetDateTimeField("sTaxTableU4DueD");
                var taxScheduleItem = sU4TaxScheduleItems.FirstOrDefault();
                return taxScheduleItem == null ? CDateTime.InvalidWrapValue
                    : CDateTime.Create(taxScheduleItem.DueD_rep, m_convertLos);
            }
            set { SetDateTimeField("sTaxTableU4DueD", value); }
        }
        public string sTaxTableU4DueD_rep
        {
            get { return sTaxTableU4DueD.ToString(m_convertLos); }
            set { SetDateTimeField_rep("sTaxTableU4DueD", value); }
        }
        [DependsOn]
        public bool sTaxTableU4DueDLckd
        {
            get 
            {
                if (!BrokerDB.EnableAdditionalSection1000CustomFees)
                {
                    return false;
                }
                return GetBoolField("sTaxTableU4DueDLckd"); 
            }
            set { SetBoolField("sTaxTableU4DueDLckd", value); }
        }
        [DependsOn]
        public CDateTime sTaxTableU4PaidD
        {
            get 
            { 
                if (!BrokerDB.EnableAdditionalSection1000CustomFees)
                {
                    return CDateTime.InvalidWrapValue;
                }

                return GetDateTimeField("sTaxTableU4PaidD"); 
            }
            set { SetDateTimeField("sTaxTableU4PaidD", value); }
        }
        public string sTaxTableU4PaidD_rep
        {
            get { return GetDateTimeField_rep("sTaxTableU4PaidD"); }
            set { SetDateTimeField_rep("sTaxTableU4PaidD", value); }
        }
        
        #endregion

        #region Interest Calc fields
        private decimal m_sServicingCollectedFunds_Interest = 0;
        private decimal m_sServicingDisbursedFunds_Interest = 0;
        private decimal m_sServicingCurrentTotalFunds_Interest = 0;
        private decimal m_sServicingDueFunds_Interest = 0;
        private decimal m_sServicingTotalDueFunds_Interest = 0;

        [DependsOn(nameof(sServicingPayments))]
        public int sServicingPmtMadeCount
        {
            get
            {
                int cnt = 0;
                foreach (CServicingPaymentFields spField in sServicingPayments)
                {
                    if (spField.ServicingTransactionT != "Monthly Payment" || string.IsNullOrEmpty(spField.PaymentD_rep) 
                        || string.IsNullOrEmpty(spField.PaymentAmt_rep)) {  }
                    else{ cnt += 1; }
                }
                return cnt;
            }
        }

        [DependsOn(nameof(sServicingPayments))]
        public string sServicingPmtMadeCount_rep
        {
            get
            {
                int madeCnt = sServicingPmtMadeCount;
                return madeCnt.ToString();
            }
        }

        [DependsOn(nameof(sServicingPayments))]
        public int sServicingPmtDueCount
        {
            get
            {
                int cnt = 0;
                foreach (CServicingPaymentFields spField in sServicingPayments)
                {
                    if (spField.ServicingTransactionT != "Monthly Payment" || string.IsNullOrEmpty(spField.DueD_rep) 
                        || string.IsNullOrEmpty(spField.DueAmt_rep)) {  }
                    else{ cnt += 1; }
                }
                return cnt;
            }
        }

        [DependsOn(nameof(sServicingPayments))]
        public string sServicingPmtDueCount_rep
        {
            get
            {
                int dueCnt = sServicingPmtDueCount;
                return dueCnt.ToString();
            }
        }

        [DependsOn(nameof(sSettlementIPia))]
        private decimal sServicingClosingFunds_Interest
        {
            get { return sSettlementIPia; }
        }
        public string sServicingClosingFunds_Interest_rep
        {
            get { return ToMoneyString(() => sServicingClosingFunds_Interest); }
        }

        [DependsOn(nameof(sServicingPayments), nameof(SetPaymentSummaryData))]
        private decimal sServicingCollectedFunds_Interest
        {
            get
            {
                if (m_areServicingPmtFieldsSet == false)
                {
                    SetPaymentSummaryData(sServicingPayments);
                }
                return m_sServicingCollectedFunds_Interest;
            }
            set { m_sServicingCollectedFunds_Interest = value; }
        }
        public string sServicingCollectedFunds_Interest_rep
        {
            get { return ToMoneyString(() => sServicingCollectedFunds_Interest); }
        }

        [DependsOn(nameof(sServicingCollectedFunds_Interest))]
        public decimal sProvExpServicingCollectedFunds_Interest
        {
            get { return decimal.Round(sServicingCollectedFunds_Interest, 2); }
        }

        [DependsOn(nameof(sServicingPayments), nameof(SetPaymentSummaryData))]
        private decimal sServicingDisbursedFunds_Interest
        {
            get
            {
                if (m_areServicingPmtFieldsSet == false)
                {
                    SetPaymentSummaryData(sServicingPayments);
                }
                return m_sServicingDisbursedFunds_Interest;
            }
            set { m_sServicingDisbursedFunds_Interest = value; }
        }
        public string sServicingDisbursedFunds_Interest_rep
        {
            get { return ToMoneyString(() => sServicingDisbursedFunds_Interest); }
        }

        [DependsOn(nameof(sServicingPayments), nameof(SetPaymentSummaryData))]
        private decimal sServicingCurrentTotalFunds_Interest
        {
            get
            {
                if (m_areServicingPmtFieldsSet == false)
                {
                    SetPaymentSummaryData(sServicingPayments);
                }
                return m_sServicingCurrentTotalFunds_Interest;
            }
            set { m_sServicingCurrentTotalFunds_Interest = value; }
        }
        public string sServicingCurrentTotalFunds_Interest_rep
        {
            get { return ToMoneyString(() => sServicingCurrentTotalFunds_Interest); }
        }

        [DependsOn(nameof(sServicingPayments), nameof(SetPaymentSummaryData))]
        private decimal sServicingDueFunds_Interest
        {
            get
            {
                if (m_areServicingPmtFieldsSet == false)
                {
                    SetPaymentSummaryData(sServicingPayments);
                }
                return m_sServicingDueFunds_Interest;
            }
            set { m_sServicingDueFunds_Interest = value; }
        }
        public string sServicingDueFunds_Interest_rep
        {
            get { return ToMoneyString(() => sServicingDueFunds_Interest); }
        }

        [DependsOn(nameof(sServicingPayments), nameof(SetPaymentSummaryData))]
        private decimal sServicingTotalDueFunds_Interest
        {
            get
            {
                if (m_areServicingPmtFieldsSet == false)
                {
                    SetPaymentSummaryData(sServicingPayments);
                }
                return m_sServicingTotalDueFunds_Interest;
            }
            set { m_sServicingTotalDueFunds_Interest = value; }
        }
        public string sServicingTotalDueFunds_Interest_rep
        {
            get { return ToMoneyString(() => sServicingTotalDueFunds_Interest); }
        }

        [DependsOn(nameof(sServicingCollectedFunds_Interest), nameof(sServicingDisbursedFunds_Interest))]
        private decimal sServicingCollectedAndDisbursedFunds_Interest
        {
            get { return SumMoney(sServicingCollectedFunds_Interest, sServicingDisbursedFunds_Interest); }
        }

        public string sServicingCollectedAndDisbursedFunds_Interest_rep
        {
            get { return ToMoneyString(() => sServicingCollectedAndDisbursedFunds_Interest); }
        }

        [DependsOn(nameof(sServicingCollectedFunds_Interest), nameof(sServicingDisbursedFunds_Interest), nameof(sServicingDueFunds_Interest))]
        private decimal sServicingCollectedAndDisbursedDueFunds_Interest
        {
            get { return SumMoney(sServicingCollectedFunds_Interest, sServicingDisbursedFunds_Interest, sServicingDueFunds_Interest); }
        }

        public string sServicingCollectedAndDisbursedDueFunds_Interest_rep
        {
            get { return ToMoneyString(() => sServicingCollectedAndDisbursedDueFunds_Interest); }
        }

        #endregion

        #region Escrow Calc fields
        private decimal m_sServicingCollectedFunds_Escrow = 0;
        private decimal m_sServicingDisbursedFunds_Escrow = 0;
        private decimal m_sServicingCurrentTotalFunds_Escrow = 0;
        private decimal m_sServicingDueFunds_Escrow = 0;
        private decimal m_sServicingTotalDueFunds_Escrow = 0;

        //opm 53156 fs 07/02/10
        [DependsOn(nameof(sProMIns), nameof(sServicingEscrowPmtNonMI), nameof(sfIsIncludeInEscrow))]
        private decimal sServicingEscrowPmt
        {
            get
            {
                //split the escrow calculation into sProMins and a new field
                List<decimal> list = new List<decimal>();
                list.Add(sServicingEscrowPmtNonMI);
                // 6/30/2014 AV - 185405 Always include sProMIns when calculating sServicingEscrowPmt
                list.Add(sProMIns);
                return SumMoney(list, MidpointRounding.AwayFromZero);
            }
        }
        public string sServicingEscrowPmt_rep
        {
            get { return ToMoneyStringRoundAwayFromZero(() => sServicingEscrowPmt); }
        }

        [DependsOn(nameof(sServicingEscrowPmt))]
        public decimal sProvExpServicingEscrowPmt
        {
            get { return decimal.Round(sServicingEscrowPmt, 2); }
        }

        [DependsOn(nameof(sfIsIncludeInEscrow), nameof(sSettlementMInsRsrvMon), nameof(sSettlementHazInsRsrvMon), nameof(sSettlementRealETxRsrvMon), nameof(sSettlementSchoolTxRsrvMon),
            nameof(sSettlementFloodInsRsrvMon), nameof(sSettlement1008RsrvMon), nameof(sSettlement1009RsrvMon), nameof(sProHazIns), nameof(sProRealETx), nameof(sProSchoolTx), nameof(sProFloodIns),
            nameof(s1006ProHExp), nameof(s1007ProHExp), nameof(sSettlementU3RsrvMon), nameof(sProU3Rsrv), nameof(sSettlementU4RsrvMon), nameof(sProU4Rsrv),nameof(sIsHousingExpenseMigrated),nameof(sHousingExpenses),
            nameof(sEscrowPmtNonMI),nameof(sEnableNewExpenseCalculations),nameof(sHazardExpense),nameof(sRealEstateTaxExpense),nameof(sSchoolTaxExpense),nameof(sFloodExpense),nameof(sLine1008Expense),
            nameof(sLine1009Expense), nameof(sLine1010Expense), nameof(sLine1011Expense))]
        private decimal sServicingEscrowPmtNonMI
        {
            get
            {
                if (this.sEnableNewExpenseCalculations)
                {
                    return this.sEscrowPmtNonMI;
                }
                else
                {
                    /// sum of reserve values from 1000 section of settlement charges page
                    /// From the PDE:
                    /// To determine the amount for the monthly escrow payment:
                    /// For each item in section 1000:
                    /// --If it is 1003 Mortgage Insurance
                    /// ----Include the monthly amount  (exclude the MI in this calculation, it will later be added when calculating sServicingEscrowPmt)
                    /// --Else if the number of months reserves is > 0
                    /// ----Include the monthly amount
                    /// --Else
                    /// ----Do not include the monthly amount
                    /// Sum the total.
                    /// The monthly amount is the same as the calculation for 1 month of reserves.

                    decimal InitialDepositEscrow = 0;
                    if (IsIncludeInEscrow(E_EscrowItemT.HazardInsurance)) // scrapping the 0 checks in 180983
                    {
                        if (this.sIsHousingExpenseMigrated && this.sHazardExpense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
                        {
                            InitialDepositEscrow = SumMoney(InitialDepositEscrow, this.sHazardExpense.MonthlyAmtServicing);
                        }
                        else
                        {
                            InitialDepositEscrow = SumMoney(InitialDepositEscrow, sProHazIns);
                        }
                    }

                    if (IsIncludeInEscrow(E_EscrowItemT.RealEstateTax))
                    {
                        if (this.sIsHousingExpenseMigrated && this.sRealEstateTaxExpense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
                        {
                            InitialDepositEscrow = SumMoney(InitialDepositEscrow, this.sRealEstateTaxExpense.MonthlyAmtServicing);
                        }
                        else
                        {
                            InitialDepositEscrow = SumMoney(InitialDepositEscrow, sProRealETx); // sProRealETx is normally multiplied by the number of months to get the reserve value, so we'll just use it alone here as that is the same as multiplying by 1 month
                        }
                    }

                    if (IsIncludeInEscrow(E_EscrowItemT.SchoolTax))
                    {
                        if (this.sIsHousingExpenseMigrated && this.sSchoolTaxExpense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
                        {
                            InitialDepositEscrow = SumMoney(InitialDepositEscrow, this.sSchoolTaxExpense.MonthlyAmtServicing);
                        }
                        else
                        {
                            InitialDepositEscrow = SumMoney(InitialDepositEscrow, sProSchoolTx);
                        }
                    }

                    if (IsIncludeInEscrow(E_EscrowItemT.FloodInsurance))
                    {
                        if (this.sIsHousingExpenseMigrated && this.sFloodExpense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
                        {
                            InitialDepositEscrow = SumMoney(InitialDepositEscrow, this.sFloodExpense.MonthlyAmtServicing);
                        }
                        else
                        {
                            InitialDepositEscrow = SumMoney(InitialDepositEscrow, sProFloodIns);
                        }
                    }

                    if (IsIncludeInEscrow(E_EscrowItemT.UserDefine1))
                    {
                        if (this.sIsHousingExpenseMigrated && this.sLine1008Expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
                        {
                            InitialDepositEscrow = SumMoney(InitialDepositEscrow, this.sLine1008Expense.MonthlyAmtServicing);
                        }
                        else
                        {
                            InitialDepositEscrow = SumMoney(InitialDepositEscrow, s1006ProHExp);
                        }
                    }

                    if (IsIncludeInEscrow(E_EscrowItemT.UserDefine2))
                    {
                        if (this.sIsHousingExpenseMigrated && this.sLine1009Expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
                        {
                            InitialDepositEscrow = SumMoney(InitialDepositEscrow, this.sLine1009Expense.MonthlyAmtServicing);
                        }
                        else
                        {
                            InitialDepositEscrow = SumMoney(InitialDepositEscrow, s1007ProHExp);
                        }
                    }

                    if (IsIncludeInEscrow(E_EscrowItemT.UserDefine3))
                    {
                        if (this.sIsHousingExpenseMigrated && this.sLine1010Expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
                        {
                            InitialDepositEscrow = SumMoney(InitialDepositEscrow, this.sLine1010Expense.MonthlyAmtServicing);
                        }
                        else
                        {
                            InitialDepositEscrow = SumMoney(InitialDepositEscrow, sProU3Rsrv);
                        }
                    }

                    if (IsIncludeInEscrow(E_EscrowItemT.UserDefine4))
                    {
                        if (this.sIsHousingExpenseMigrated && this.sLine1011Expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
                        {
                            InitialDepositEscrow = SumMoney(InitialDepositEscrow, this.sLine1011Expense.MonthlyAmtServicing);
                        }
                        else
                        {
                            InitialDepositEscrow = SumMoney(InitialDepositEscrow, sProU4Rsrv);
                        }
                    }

                    return InitialDepositEscrow;
                }
            }
        }

        public string sServicingEscrowPmtNonMI_rep
        {
            get { return ToMoneyStringRoundAwayFromZero(() => sServicingEscrowPmtNonMI); }
        }

        [DependsOn(nameof(sServicingPayments), nameof(SetPaymentSummaryData))]
        private decimal sServicingCollectedFunds_Escrow
        {
            get
            {
                if (m_areServicingPmtFieldsSet == false)
                {
                    SetPaymentSummaryData(sServicingPayments);
                }
                return m_sServicingCollectedFunds_Escrow;
            }
            set { m_sServicingCollectedFunds_Escrow = value; }
        }
        public string sServicingCollectedFunds_Escrow_rep
        {
            get { return ToMoneyString(() => sServicingCollectedFunds_Escrow); }
        }

        [DependsOn(nameof(sServicingPayments), nameof(SetPaymentSummaryData))]
        private decimal sServicingDisbursedFunds_Escrow
        {
            get
            {
                if (m_areServicingPmtFieldsSet == false)
                {
                    SetPaymentSummaryData(sServicingPayments);
                }
                return m_sServicingDisbursedFunds_Escrow;
            }
            set { m_sServicingDisbursedFunds_Escrow = value; }
        }
        public string sServicingDisbursedFunds_Escrow_rep
        {
            get { return ToMoneyString(() => sServicingDisbursedFunds_Escrow); }
        }

        [DependsOn(nameof(sServicingPayments), nameof(SetPaymentSummaryData))]
        private decimal sServicingCurrentTotalFunds_Escrow
        {
            get
            {
                if (m_areServicingPmtFieldsSet == false)
                {
                    SetPaymentSummaryData(sServicingPayments);
                }
                return m_sServicingCurrentTotalFunds_Escrow;
            }
            set { m_sServicingCurrentTotalFunds_Escrow = value; }
        }
        public string sServicingCurrentTotalFunds_Escrow_rep
        {
            get { return ToMoneyString(() => sServicingCurrentTotalFunds_Escrow); }
        }

        [DependsOn(nameof(sServicingCurrentTotalFunds_Escrow))]
        public decimal sProvExpServicingCurrentTotalFunds_Escrow
        {
            get { return decimal.Round(sServicingCurrentTotalFunds_Escrow, 2); }
        }

        [DependsOn(nameof(sServicingPayments), nameof(SetPaymentSummaryData))]
        private decimal sServicingDueFunds_Escrow
        {
            get
            {
                if (m_areServicingPmtFieldsSet == false)
                {
                    SetPaymentSummaryData(sServicingPayments);
                }
                return m_sServicingDueFunds_Escrow;
            }
            set { m_sServicingDueFunds_Escrow = value; }
        }
        public string sServicingDueFunds_Escrow_rep
        {
            get { return ToMoneyString(() => sServicingDueFunds_Escrow); }
        }

        [DependsOn(nameof(sServicingPayments), nameof(SetPaymentSummaryData))]
        private decimal sServicingTotalDueFunds_Escrow
        {
            get
            {
                if (m_areServicingPmtFieldsSet == false)
                {
                    SetPaymentSummaryData(sServicingPayments);
                }
                return m_sServicingTotalDueFunds_Escrow;
            }
            set { m_sServicingTotalDueFunds_Escrow = value; }
        }
        public string sServicingTotalDueFunds_Escrow_rep
        {
            get { return ToMoneyString(() => sServicingTotalDueFunds_Escrow); }
        }

        [DependsOn(nameof(sServicingCollectedFunds_Escrow), nameof(sServicingDisbursedFunds_Escrow))]
        private decimal sServicingCollectedAndDisbursedFunds_Escrow
        {
            get { return SumMoney(sServicingCollectedFunds_Escrow, sServicingDisbursedFunds_Escrow); }
        }

        public string sServicingCollectedAndDisbursedFunds_Escrow_rep
        {
            get { return ToMoneyString(() => sServicingCollectedAndDisbursedFunds_Escrow); }
        }

        [DependsOn(nameof(sServicingCollectedFunds_Escrow), nameof(sServicingDisbursedFunds_Escrow), nameof(sServicingDueFunds_Escrow))]
        private decimal sServicingCollectedAndDisbursedDueFunds_Escrow
        {
            get { return SumMoney(sServicingCollectedFunds_Escrow, sServicingDisbursedFunds_Escrow, sServicingDueFunds_Escrow); }
        }

        public string sServicingCollectedAndDisbursedDueFunds_Escrow_rep
        {
            get { return ToMoneyString(() => sServicingCollectedAndDisbursedDueFunds_Escrow); }
        }

        [DependsOn(nameof(sServicingCollectedFunds_LateFees), nameof(sServicingDisbursedFunds_LateFees), nameof(sServicingDueFunds_LateFees))]
        private decimal sServicingCollectedAndDisbursedDueFunds_LateFees
        {
            get { return SumMoney(sServicingCollectedFunds_LateFees, sServicingDisbursedFunds_LateFees, sServicingDueFunds_LateFees); }
        }

        public string sServicingCollectedAndDisbursedDueFunds_LateFees_rep
        {
            get { return ToMoneyString(() => sServicingCollectedAndDisbursedDueFunds_LateFees); }
        }

        #endregion

        #region Other Calc fields
        private decimal m_sServicingCollectedFunds_Other = 0;
        private decimal m_sServicingDisbursedFunds_Other = 0;
        private decimal m_sServicingCurrentTotalFunds_Other = 0;
        private decimal m_sServicingDueFunds_Other = 0;
        private decimal m_sServicingTotalDueFunds_Other = 0;

        private decimal sServicingClosingFunds_Other
        {
            get
            {
                return 0; // For now, we always return 0 because we're not using this field
            }
            set { }
        }
        public string sServicingClosingFunds_Other_rep
        {
            get { return ToMoneyString(() => sServicingClosingFunds_Other); }
        }

        [DependsOn(nameof(sServicingPayments), nameof(SetPaymentSummaryData))]
        private decimal sServicingCollectedFunds_Other
        {
            get
            {
                if (m_areServicingPmtFieldsSet == false)
                {
                    SetPaymentSummaryData(sServicingPayments);
                }
                return m_sServicingCollectedFunds_Other;
            }
            set { m_sServicingCollectedFunds_Other = value; }
        }
        public string sServicingCollectedFunds_Other_rep
        {
            get { return ToMoneyString(() => sServicingCollectedFunds_Other); }
        }

        [DependsOn(nameof(sServicingPayments), nameof(SetPaymentSummaryData))]
        private decimal sServicingDisbursedFunds_Other
        {
            get
            {
                if (m_areServicingPmtFieldsSet == false)
                {
                    SetPaymentSummaryData(sServicingPayments);
                }
                return m_sServicingDisbursedFunds_Other;
            }
            set { m_sServicingDisbursedFunds_Other = value; }
        }
        public string sServicingDisbursedFunds_Other_rep
        {
            get { return ToMoneyString(() => sServicingDisbursedFunds_Other); }
        }

        [DependsOn(nameof(sServicingPayments), nameof(SetPaymentSummaryData))]
        private decimal sServicingCurrentTotalFunds_Other
        {
            get
            {
                if (m_areServicingPmtFieldsSet == false)
                {
                    SetPaymentSummaryData(sServicingPayments);
                }
                return m_sServicingCurrentTotalFunds_Other;
            }
            set { m_sServicingCurrentTotalFunds_Other = value; }
        }
        public string sServicingCurrentTotalFunds_Other_rep
        {
            get { return ToMoneyString(() => sServicingCurrentTotalFunds_Other); }
        }

        [DependsOn(nameof(sServicingPayments), nameof(SetPaymentSummaryData))]
        private decimal sServicingDueFunds_Other
        {
            get
            {
                if (m_areServicingPmtFieldsSet == false)
                {
                    SetPaymentSummaryData(sServicingPayments);
                }
                return m_sServicingDueFunds_Other;
            }
            set { m_sServicingDueFunds_Other = value; }
        }
        public string sServicingDueFunds_Other_rep
        {
            get { return ToMoneyString(() => sServicingDueFunds_Other); }
        }

        [DependsOn(nameof(sServicingPayments), nameof(SetPaymentSummaryData))]
        private decimal sServicingTotalDueFunds_Other
        {
            get
            {
                if (m_areServicingPmtFieldsSet == false)
                {
                    SetPaymentSummaryData(sServicingPayments);
                }
                return m_sServicingTotalDueFunds_Other;
            }
            set { m_sServicingTotalDueFunds_Other = value; }
        }
        public string sServicingTotalDueFunds_Other_rep
        {
            get { return ToMoneyString(() => sServicingTotalDueFunds_Other); }
        }
        [DependsOn(nameof(sServicingCollectedFunds_Other), nameof(sServicingDisbursedFunds_Other))]
        private decimal sServicingCollectedAndDisbursedFunds_Other
        {
            get { return SumMoney(sServicingCollectedFunds_Other, sServicingDisbursedFunds_Other); }
        }

        public string sServicingCollectedAndDisbursedFunds_Other_rep
        {
            get { return ToMoneyString(() => sServicingCollectedAndDisbursedFunds_Other); }
        }

        [DependsOn(nameof(sServicingCollectedFunds_Other), nameof(sServicingDisbursedFunds_Other), nameof(sServicingDueFunds_Other))]
        private decimal sServicingCollectedAndDisbursedDueFunds_Other
        {
            get { return SumMoney(sServicingCollectedFunds_Other, sServicingDisbursedFunds_Other, sServicingDueFunds_Other); }
        }

        public string sServicingCollectedAndDisbursedDueFunds_Other_rep
        {
            get { return ToMoneyString(() => sServicingCollectedAndDisbursedDueFunds_Other); }
        }
        #endregion

        #region Late Fees Calc fields
        private decimal m_sServicingCollectedFunds_LateFees = 0;
        private decimal m_sServicingDisbursedFunds_LateFees = 0;
        private decimal m_sServicingCurrentTotalFunds_LateFees = 0;
        private decimal m_sServicingDueFunds_LateFees = 0;
        private decimal m_sServicingTotalDueFunds_LateFees = 0;

        private decimal sServicingClosingFunds_LateFees
        {
            get
            {
                return 0; // For now, we always return 0 because we're not using the field
            }
            set { }
        }
        public string sServicingClosingFunds_LateFees_rep
        {
            get { return ToMoneyString(() => sServicingClosingFunds_LateFees); }
        }

        [DependsOn(nameof(sServicingPayments), nameof(SetPaymentSummaryData))]
        private decimal sServicingCollectedFunds_LateFees
        {
            get
            {
                if (m_areServicingPmtFieldsSet == false)
                {
                    SetPaymentSummaryData(sServicingPayments);
                }
                return m_sServicingCollectedFunds_LateFees;
            }
            set { m_sServicingCollectedFunds_LateFees = value; }
        }
        public string sServicingCollectedFunds_LateFees_rep
        {
            get { return ToMoneyString(() => sServicingCollectedFunds_LateFees); }
        }

        [DependsOn(nameof(sServicingPayments), nameof(SetPaymentSummaryData))]
        private decimal sServicingDisbursedFunds_LateFees
        {
            get
            {
                if (m_areServicingPmtFieldsSet == false)
                {
                    SetPaymentSummaryData(sServicingPayments);
                }
                return m_sServicingDisbursedFunds_LateFees;
            }
            set { m_sServicingDisbursedFunds_LateFees = value; }
        }
        public string sServicingDisbursedFunds_LateFees_rep
        {
            get { return ToMoneyString(() => sServicingDisbursedFunds_LateFees); }
        }

        [DependsOn(nameof(sServicingPayments), nameof(SetPaymentSummaryData))]
        private decimal sServicingCurrentTotalFunds_LateFees
        {
            get
            {
                if (m_areServicingPmtFieldsSet == false)
                {
                    SetPaymentSummaryData(sServicingPayments);
                }
                return m_sServicingCurrentTotalFunds_LateFees;
            }
            set { m_sServicingCurrentTotalFunds_LateFees = value; }
        }
        public string sServicingCurrentTotalFunds_LateFees_rep
        {
            get { return ToMoneyString(() => sServicingCurrentTotalFunds_LateFees); }
        }

        [DependsOn(nameof(sServicingPayments), nameof(SetPaymentSummaryData))]
        private decimal sServicingDueFunds_LateFees
        {
            get
            {
                if (m_areServicingPmtFieldsSet == false)
                {
                    SetPaymentSummaryData(sServicingPayments);
                }
                return m_sServicingDueFunds_LateFees;
            }
            set { m_sServicingDueFunds_LateFees = value; }
        }
        public string sServicingDueFunds_LateFees_rep
        {
            get { return ToMoneyString(() => sServicingDueFunds_LateFees); }
        }

        [DependsOn(nameof(sServicingPayments), nameof(SetPaymentSummaryData))]
        private decimal sServicingTotalDueFunds_LateFees
        {
            get
            {
                if (m_areServicingPmtFieldsSet == false)
                {
                    SetPaymentSummaryData(sServicingPayments);
                }
                return m_sServicingTotalDueFunds_LateFees;
            }
            set { m_sServicingTotalDueFunds_LateFees = value; }
        }
        public string sServicingTotalDueFunds_LateFees_rep
        {
            get { return ToMoneyString(() => sServicingTotalDueFunds_LateFees); }
        }
        #endregion

        #endregion

        #region Transactions Fields
        
        /// <summary>
        /// List of transaction payments on the transactions page.
        /// </summary>
        [DependsOn]
        public string sTransactionsXmlContent
        {
            get { return GetLongTextField("sTransactionsXmlContent"); }
            set { SetLongTextField("sTransactionsXmlContent", value); }
        }

        /// <summary>
        /// Used by the sTransactions property. XmlSerializer is thread safe.
        /// </summary>
        private static System.Xml.Serialization.XmlSerializer x_TransactionsSerializer = new System.Xml.Serialization.XmlSerializer(typeof(List<CTransactionFields>));
        [DependsOn(nameof(sTransactionsXmlContent), nameof(PerformTransactionCalculations))]
        public List<CTransactionFields> sTransactions
        {
            get
            {
                string data = sTransactionsXmlContent;
                if (string.IsNullOrEmpty(data))
                {
                    return new List<CTransactionFields>();
                }

                List<CTransactionFields> transactionsList;

                using (System.IO.TextReader textReader = new System.IO.StringReader(data))
                {
                    transactionsList = (List<CTransactionFields>)x_TransactionsSerializer.Deserialize(textReader);
                }

                foreach (CTransactionFields fields in transactionsList)
                {
                    fields.SetConvertLos(this.m_convertLos);
                }

                return transactionsList;
            }
            set
            {
                PerformTransactionCalculations(value);

                using (var writer = new System.IO.StringWriter())
                {
                    x_TransactionsSerializer.Serialize(writer, value);
                    sTransactionsXmlContent = writer.ToString();
                }
            }
        }

        //NOTE: the order of these entries is important, if you change it, it will affect anything that indexes it, such as sTransaction_map[0]
        public static List<string> sTransaction_map = new List<string>() { "Received from", "Paid to" };

        private string TRANSDIR_RECVFROM = sTransaction_map[0];
        private string TRANSDIR_PAIDTO = sTransaction_map[1];

        [DependsOn(nameof(sMiPmtMadeD), nameof(sMiPmtDueD), nameof(sFfUfmip1003), nameof(sTransactionsTotPmtsRecvd), nameof(sTransactionsTotPmtsMade),
            nameof(sTransactionsTotPmtsReceivable), nameof(sTransactionsTotPmtsPayable))]
        private void PerformTransactionCalculations(List<CTransactionFields> transFields)
        {
            m_areTransactionFieldsSet = true;
            foreach (CTransactionFields field in transFields)
            {
                if (field.PmtMade)
                {
                    if (field.PmtDir_rep.ToLower().Equals(TRANSDIR_RECVFROM.ToLower()))
                    {
                        sTransactionsTotPmtsRecvd += field.TransactionAmt;
                    }
                    else if (field.PmtDir_rep.ToLower().Equals(TRANSDIR_PAIDTO.ToLower()))
                    {
                        sTransactionsTotPmtsMade += field.TransactionAmt;
                    }
                }
                else
                {
                    if (field.PmtDir_rep.ToLower().Equals(TRANSDIR_RECVFROM.ToLower()))
                    {
                        sTransactionsTotPmtsReceivable += field.TransactionAmt;
                    }
                    else if (field.PmtDir_rep.ToLower().Equals(TRANSDIR_PAIDTO.ToLower()))
                    {
                        sTransactionsTotPmtsPayable += field.TransactionAmt;
                    }
                }

            }
            
            if (!string.IsNullOrEmpty(sMiPmtDueD_rep) && string.IsNullOrEmpty(sMiPmtMadeD_rep))
            {
                m_sTransactionsTotPmtsPayable += sFfUfmip1003; // Transactions: Payable
            }

            m_sTransactionsNetPmtsToDate = m_sTransactionsTotPmtsRecvd - m_sTransactionsTotPmtsMade;
            m_sTransactionsNetPmtsDue = m_sTransactionsTotPmtsReceivable - m_sTransactionsTotPmtsPayable;


            if (!string.IsNullOrEmpty(sMiPmtMadeD_rep))
            {
                m_sTransactionsNetPmtsToDate -= sFfUfmip1003; // Transactions: Cash
            }
        }

        #region Transaction calc fields
        private decimal m_sTransactionsTotPmtsRecvd = 0;
        private decimal m_sTransactionsTotPmtsMade = 0;
        private decimal m_sTransactionsTotPmtsReceivable = 0;
        private decimal m_sTransactionsTotPmtsPayable = 0;
        private decimal m_sTransactionsNetPmtsToDate = 0;
        private decimal m_sTransactionsNetPmtsDue = 0;
        private bool m_areTransactionFieldsSet = false;

        [DependsOn(nameof(sTransactions), nameof(sTransactionsXmlContent), nameof(PerformTransactionCalculations))]
        private decimal sTransactionsTotPmtsRecvd
        {
            get 
            {
                if (m_areTransactionFieldsSet == false)
                {
                    PerformTransactionCalculations(sTransactions);
                }
                return m_sTransactionsTotPmtsRecvd; 
            }
            set { m_sTransactionsTotPmtsRecvd = value; }
        }
        public string sTransactionsTotPmtsRecvd_rep
        {
            get {
                return ToMoneyString(() => sTransactionsTotPmtsRecvd); }
        }
        [DependsOn(nameof(sTransactions), nameof(sTransactionsXmlContent), nameof(PerformTransactionCalculations))]
        private decimal sTransactionsTotPmtsMade
        {
            get 
            {
                if (m_areTransactionFieldsSet == false)
                {
                    PerformTransactionCalculations(sTransactions);
                }
                return m_sTransactionsTotPmtsMade; 
            }
            set { m_sTransactionsTotPmtsMade = value; }
        }
        public string sTransactionsTotPmtsMade_rep
        {
            get { return ToMoneyString(() => sTransactionsTotPmtsMade); }
        }
        [DependsOn(nameof(sTransactions), nameof(sTransactionsXmlContent), nameof(PerformTransactionCalculations))]
        public decimal sTransactionsTotPmtsReceivable
        {
            get 
            {
                if (m_areTransactionFieldsSet == false)
                {
                    PerformTransactionCalculations(sTransactions);
                }
                return m_sTransactionsTotPmtsReceivable; 
            }
            set { m_sTransactionsTotPmtsReceivable = value; }
        }
        public string sTransactionsTotPmtsReceivable_rep
        {
            get { return ToMoneyString(() => sTransactionsTotPmtsReceivable); }
        }
        [DependsOn(nameof(sTransactions), nameof(PerformTransactionCalculations))]
        public decimal sTransactionsTotPmtsPayable
        {
            get 
            {
                if (m_areTransactionFieldsSet == false)
                {
                    PerformTransactionCalculations(sTransactions);
                }
                return m_sTransactionsTotPmtsPayable; 
            }
            set { m_sTransactionsTotPmtsPayable = value; }
        }
        public string sTransactionsTotPmtsPayable_rep
        {
            get { return ToMoneyString(() => sTransactionsTotPmtsPayable); }
        }
        [DependsOn(nameof(sTransactions), nameof(sTransactionsXmlContent), nameof(PerformTransactionCalculations))]
        public decimal sTransactionsNetPmtsToDate
        {
            get 
            {
                if (m_areTransactionFieldsSet == false)
                {
                    PerformTransactionCalculations(sTransactions);
                }
                return m_sTransactionsNetPmtsToDate; 
            }
            set { m_sTransactionsNetPmtsToDate = value; }
        }
        public string sTransactionsNetPmtsToDate_rep
        {
            get { return ToMoneyString(() => sTransactionsNetPmtsToDate); }
        }

        [DependsOn(nameof(sTransactions), nameof(sTransactionsXmlContent), nameof(PerformTransactionCalculations))]
        public decimal sTransactionsNetPmtsDue
        {
            get 
            {
                if (m_areTransactionFieldsSet == false)
                {
                    PerformTransactionCalculations(sTransactions);
                }
                return m_sTransactionsNetPmtsDue; 
            }
            set { m_sTransactionsNetPmtsDue = value; }
        }
        public string sTransactionsNetPmtsDue_rep
        {
            get { return ToMoneyString(() => sTransactionsNetPmtsDue); }
        }
        
        #endregion

        #endregion

        #region Purchase Advice Fields

        [DependsOn(nameof(sInvSchedDueD1), nameof(sServicingNextPmtDue), nameof(sServicingTotalDueFunds_Principal), nameof(sDisbursementWarehouseLineInt), nameof(sPurchaseAdviceAmortCurtail),
            nameof(sInvestorLockBaseBrokComp1PcPrice), nameof(sPurchaseAdviceInterestReimbursement), nameof(sPurchaseAdviceBasePrice_Field3), nameof(sPurchaseAdviceAdjustments),
            nameof(sPurchaseAdviceBasePrice_Field1), nameof(sSettlementTotalEstimateSettlementCharge), nameof(sServicingTotalDueFunds_Escrow), nameof(sInvestorLockLpInvestorNm),
            nameof(sInvestorLockLpTemplateNm), nameof(sInvestorLockLoanNum), nameof(sInvestorInfo), nameof(sPurchaseAdviceBasePrice_AmountPriceCalcMode), nameof(sServicingCollectedFunds_Principal),
            nameof(sServicingDisbursedFunds_Principal), nameof(sServicingDueFunds_Principal), nameof(sPurchaseAdviceUnpaidPBal), nameof(sInitialDeposit_Escrow), nameof(sPurchaseAdviceEscrowTotCollAtClosing),
            nameof(sServicingCollectedFunds_Escrow), nameof(sServicingDueFunds_Escrow), nameof(sPurchaseAdviceEscrowDepDue), nameof(sServicingDisbursedFunds_Escrow), nameof(sPurchaseAdviceEscrowDisbursements),
            nameof(sPurchaseAdviceSummaryInvNm), nameof(sPurchaseAdviceSummaryInvId), nameof(sPurchaseAdviceSummaryInvPgNm), nameof(sPurchaseAdviceSummaryInvLoanNm), nameof(CopyAdjustmentsFromInvestorRateLockTable))]
        public void ApplyLoanDataToPurchaseAdvicePage()
        {
            sPurchaseAdviceBasePrice_AmountPriceCalcMode = E_AmountPriceCalcMode.Price;
            sPurchaseAdviceAmortCurtail = -1 * SumMoney(sServicingCollectedFunds_Principal, sServicingDisbursedFunds_Principal, sServicingDueFunds_Principal);
            sPurchaseAdviceBasePrice_Field1 = ToRate(sInvestorLockBaseBrokComp1PcPrice);
            sPurchaseAdviceBasePrice_Field2 = (sPurchaseAdviceBasePrice_Field3 / 100) * sPurchaseAdviceUnpaidPBal;
            //sPurchaseAdviceInterestReimbursement = sDisbursementWarehouseLineInt; // OPM 48129
            sPurchaseAdviceEscrowTotCollAtClosing = -1 * sInitialDeposit_Escrow;
            sPurchaseAdviceEscrowDepDue = (sServicingCollectedFunds_Escrow + sServicingDueFunds_Escrow) * -1;  // OPM 48129 
            sPurchaseAdviceEscrowDisbursements = -1 * sServicingDisbursedFunds_Escrow;
            sInvSchedDueD1 = CDateTime.Create(sServicingNextPmtDue);
            sPurchaseAdviceSummaryInvNm = sInvestorLockLpInvestorNm;
            if (this.sInvestorInfo.Id.HasValue && string.Equals(this.sInvestorInfo.InvestorName, this.sPurchaseAdviceSummaryInvNm, StringComparison.Ordinal))
            {
                this.sPurchaseAdviceSummaryInvId = this.sInvestorInfo.Id.Value;
            }

            sPurchaseAdviceSummaryInvPgNm = sInvestorLockLpTemplateNm;
            sPurchaseAdviceSummaryInvLoanNm = sInvestorLockLoanNum;
            CopyAdjustmentsFromInvestorRateLockTable();
        }

        public const int PURCHADV_ADJ_AMT = 1;
        public const int PURCHADV_ADJ_PC = 2;
        public const int PURCHADV_ADJ_OTHER = 3;

        [DependsOn(nameof(sInvestorLockAdjustments), nameof(sFinalLAmt), nameof(sPurchaseAdviceAdjustments))]
        private void CopyAdjustmentsFromInvestorRateLockTable()
        {
            int cCount = 0;
            List<CPurchaseAdviceAdjustmentsFields> purchaseAdjustments = new List<CPurchaseAdviceAdjustmentsFields>();

            foreach (PricingAdjustment adjustment in sInvestorLockAdjustments)
            {
                CPurchaseAdviceAdjustmentsFields field = new CPurchaseAdviceAdjustmentsFields();
                field.RowNum = cCount++;
                field.AdjDesc = adjustment.Description;
                field.AdjPc_rep = adjustment.Price;
                field.AdjAmt_rep = ToMoneyString((field.AdjPc / 100) * sFinalLAmt);
                purchaseAdjustments.Add(field);
            }

            sPurchaseAdviceAdjustments = purchaseAdjustments;
        }

        [DependsOn]
        public string sServicingNotes
        {
            get { return GetLongTextField("sServicingNotes"); }
            set { SetLongTextField("sServicingNotes", value); }
        }

        #region Loan Price fields
        // From the spec: sum of principal payments/adjustments from the servicing screen (negative means principal has been paid off).
        // This just means grab the value from the Principal summary column's "Total w/pmts due" field in the servicing page (sServicingTotalDueFunds_Principal).
        [DependsOn]
        private decimal sPurchaseAdviceAmortCurtail
        {
            get { return GetMoneyField("sPurchaseAdviceAmortCurtail"); }
            set { SetMoneyField("sPurchaseAdviceAmortCurtail", value); }
        }
        public string sPurchaseAdviceAmortCurtail_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceAmortCurtail); }
            set { sPurchaseAdviceAmortCurtail = ToMoney(value); }
        }

        [DependsOn(nameof(sFinalLAmt), nameof(sPurchaseAdviceAmortCurtail))]
        private decimal sPurchaseAdviceUnpaidPBal
        {
            get 
            {
                return SumMoney(sFinalLAmt, sPurchaseAdviceAmortCurtail);
            }
        }
        public string sPurchaseAdviceUnpaidPBal_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceUnpaidPBal); }
        }

        [DependsOn(nameof(sPurchaseAdviceUnpaidPBal))]
        public decimal sProvExpPurchaseAdviceUnpaidPBal
        {
            get { return decimal.Round(sPurchaseAdviceUnpaidPBal, 2); }
        }
        
        [DependsOn(nameof(sPurchaseAdviceBasePrice_AmountPriceCalcMode))]
        public E_AmountPriceCalcMode sPurchaseAdviceBasePrice_AmountPriceCalcMode
        {
            get { return (E_AmountPriceCalcMode)GetTypeIndexField("sPurchaseAdviceBasePrice_AmountPriceCalcMode");  }
            set { SetTypeIndexField("sPurchaseAdviceBasePrice_AmountPriceCalcMode", (int)value); }
        }



        [DependsOn(nameof(sPurchaseAdviceBasePrice_AmountPriceCalcMode), nameof(sPurchaseAdviceBasePrice_Field1), nameof(sPurchaseAdviceUnpaidPBal))]
        private decimal sPurchaseAdviceBasePrice_Field1
        {
            get 
            {
                switch (sPurchaseAdviceBasePrice_AmountPriceCalcMode)
                {
                    case E_AmountPriceCalcMode.Amount:
                        if (sPurchaseAdviceUnpaidPBal == 0)
                        {
                            return 100;
                        }
                        return  100 + 100 * (sPurchaseAdviceBasePrice_Field2 / sPurchaseAdviceUnpaidPBal); 
                    case E_AmountPriceCalcMode.Price:
                        return GetRateField("sPurchaseAdviceBasePrice_Field1"); 
                    default:
                        throw new UnhandledEnumException(sPurchaseAdviceBasePrice_AmountPriceCalcMode);
                }
                
            }
            set
            {
                SetRateField("sPurchaseAdviceBasePrice_Field1", value);
            }
        }
        
        public string sPurchaseAdviceBasePrice_Field1_rep
        {
            get { return ToRateString6DecimalDigits(() => sPurchaseAdviceBasePrice_Field1);  }
            set { sPurchaseAdviceBasePrice_Field1 = ToRate(value); }
        }

        [DependsOn(nameof(sPurchaseAdviceBasePrice_Field2), nameof(sPurchaseAdviceBasePrice_AmountPriceCalcMode), nameof(sPurchaseAdviceUnpaidPBal))]
        private decimal sPurchaseAdviceBasePrice_Field2
        {
            get {
                switch (sPurchaseAdviceBasePrice_AmountPriceCalcMode)
                {
                    case E_AmountPriceCalcMode.Amount:
                        return GetMoneyField("sPurchaseAdviceBasePrice_Field2"); 
                    case E_AmountPriceCalcMode.Price:
                        //should match field3 logic - I removed it to get rid of depdency
                        return ((sPurchaseAdviceBasePrice_Field1 - 100) / 100) * sPurchaseAdviceUnpaidPBal;
                    default:
                        throw new UnhandledEnumException(sPurchaseAdviceBasePrice_AmountPriceCalcMode);
                }
            }
            set
            {
                SetMoneyField("sPurchaseAdviceBasePrice_Field2", value);
            }
        }

        public string sPurchaseAdviceBasePrice_Field2_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceBasePrice_Field2); }
            set { sPurchaseAdviceBasePrice_Field2 = ToMoney(value); }
        }


        [DependsOn(nameof(sPurchaseAdviceBasePrice_AmountPriceCalcMode), nameof(sPurchaseAdviceBasePrice_Field1))]
        private decimal sPurchaseAdviceBasePrice_Field3
        {
            get 
            {
                return sPurchaseAdviceBasePrice_Field1 - 100; 
            }
        }
        public string sPurchaseAdviceBasePrice_Field3_rep
        {
            get { return ToRateString6DecimalDigits(() => sPurchaseAdviceBasePrice_Field3); }
    
        }


        [DependsOn(nameof(sPurchaseAdviceAdjustments_Field2), nameof(sPurchaseAdviceUnpaidPBal), nameof(sPurchaseAdviceBasePrice_AmountPriceCalcMode), nameof(sPurchaseAdviceAdjustmentsNonSRPTotalAmt))]
        private decimal sPurchaseAdviceAdjustments_Field2
        {
            get
            {
                switch (sPurchaseAdviceBasePrice_AmountPriceCalcMode)
                {
                    case E_AmountPriceCalcMode.Amount:
                        return sPurchaseAdviceAdjustmentsNonSRPTotalAmt;
                    case E_AmountPriceCalcMode.Price:
                        return (sPurchaseAdviceAdjustments_Field3 / 100) * sPurchaseAdviceUnpaidPBal;
                    default:
                        throw new UnhandledEnumException(sPurchaseAdviceBasePrice_AmountPriceCalcMode);
                }
            }
        }
        public string sPurchaseAdviceAdjustments_Field2_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceAdjustments_Field2); }
        }

        // Dependency for sPurchaseAdviceAdjustments_Field2 left off to avoid circular dependency. This is handled in FieldInfo.cs
        [DependsOn(nameof(sPurchaseAdviceAdjustments_Field3), nameof(sPurchaseAdviceAdjustmentsNonSRPTotal), nameof(sPurchaseAdviceBasePrice_AmountPriceCalcMode), nameof(sPurchaseAdviceUnpaidPBal))]
        private decimal sPurchaseAdviceAdjustments_Field3
        {
            get 
            { 
                switch(sPurchaseAdviceBasePrice_AmountPriceCalcMode)
                {
                    case E_AmountPriceCalcMode.Amount:
                        if (sPurchaseAdviceUnpaidPBal == 0)
                        {
                            return 0;
                        }
                        return (sPurchaseAdviceAdjustments_Field2 * 100) / sPurchaseAdviceUnpaidPBal;
                    case E_AmountPriceCalcMode.Price:
                        return sPurchaseAdviceAdjustmentsNonSRPTotal; 
                    default:
                        throw new UnhandledEnumException(sPurchaseAdviceBasePrice_AmountPriceCalcMode);
                }
            }
        }
        public string sPurchaseAdviceAdjustments_Field3_rep
        {
            get { return ToRateString6DecimalDigits(() => sPurchaseAdviceAdjustments_Field3); }
        }

        [DependsOn(nameof(sPurchaseAdviceBasePrice_Field1), nameof(sPurchaseAdviceAdjustments_Field3))]
        private decimal sPurchaseAdviceNetPrice_Field1
        {
            get { return sPurchaseAdviceBasePrice_Field1 + sPurchaseAdviceAdjustments_Field3; }
        }
        public string sPurchaseAdviceNetPrice_Field1_rep
        {
            get { return string.Format("{0:N6}", sPurchaseAdviceNetPrice_Field1); } 
        }

        [DependsOn(nameof(sPurchaseAdviceNetPrice_Field3), nameof(sPurchaseAdviceUnpaidPBal))]
        private decimal sPurchaseAdviceNetPrice_Field2
        {
            get
            {
                return (sPurchaseAdviceNetPrice_Field3 / 100) * sPurchaseAdviceUnpaidPBal;
            }
        }
        public string sPurchaseAdviceNetPrice_Field2_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceNetPrice_Field2); }
        }

        [DependsOn(nameof(sPurchaseAdviceBasePrice_Field3), nameof(sPurchaseAdviceAdjustments_Field3))]
        private decimal sPurchaseAdviceNetPrice_Field3
        {
            get
            {
                return sPurchaseAdviceBasePrice_Field3 + sPurchaseAdviceAdjustments_Field3;
            }
        }
        public string sPurchaseAdviceNetPrice_Field3_rep
        {
            get { return ToRateString6DecimalDigits(() => sPurchaseAdviceNetPrice_Field3); }
        }

        [DependsOn(nameof(sPurchaseAdviceUnpaidPBal), nameof(sPurchaseAdviceSRP_Field3), nameof(sPurchaseAdviceBasePrice_AmountPriceCalcMode), nameof(sPurchaseAdviceAdjustmentsSRPTotalAmt))]
        private decimal sPurchaseAdviceSRP_Field2
        {
            get
            {
                switch (sPurchaseAdviceBasePrice_AmountPriceCalcMode)
                {
                    case E_AmountPriceCalcMode.Amount:
                        return sPurchaseAdviceAdjustmentsSRPTotalAmt;
                    case E_AmountPriceCalcMode.Price:
                        return (sPurchaseAdviceSRP_Field3 / 100) * sPurchaseAdviceUnpaidPBal;
                    default:
                        throw new UnhandledEnumException(sPurchaseAdviceBasePrice_AmountPriceCalcMode);
                }
            }
        }
        public string sPurchaseAdviceSRP_Field2_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceSRP_Field2); }
        }

        // Dependency for sPurchaseAdviceSRP_Field2 left off to avoid circular dependency. This is handled in FieldInfo.cs
        [DependsOn(nameof(sPurchaseAdviceSRP_Field3), nameof(sPurchaseAdviceAdjustmentsSRPTotal), nameof(sPurchaseAdviceBasePrice_AmountPriceCalcMode), nameof(sPurchaseAdviceUnpaidPBal))]
        private decimal sPurchaseAdviceSRP_Field3
        {
            get 
            { 
                switch (sPurchaseAdviceBasePrice_AmountPriceCalcMode)
                {
                    case E_AmountPriceCalcMode.Amount:
                        if (sPurchaseAdviceUnpaidPBal == 0)
                        {
                            return 0;
                        }
                        return (sPurchaseAdviceSRP_Field2 * 100) / sPurchaseAdviceUnpaidPBal;
                    case E_AmountPriceCalcMode.Price:
                        return sPurchaseAdviceAdjustmentsSRPTotal;
                    default:
                        throw new UnhandledEnumException(sPurchaseAdviceBasePrice_AmountPriceCalcMode);
                }
                
            }
        }
        public string sPurchaseAdviceSRP_Field3_rep
        {
            get { return ToRateString6DecimalDigits(() => sPurchaseAdviceSRP_Field3); }
        }

        [DependsOn(nameof(sPurchaseAdviceNetPrice_Field1), nameof(sPurchaseAdviceSRP_Field3))]
        public decimal sPurchaseAdviceTotalPrice_Field1
        {
            get
            {
                return sPurchaseAdviceNetPrice_Field1 + sPurchaseAdviceSRP_Field3;
            }
        }
        public string sPurchaseAdviceTotalPrice_Field1_rep
        {
            get { return string.Format("{0:N6}", sPurchaseAdviceTotalPrice_Field1); } 
        }

        [DependsOn(nameof(sPurchaseAdviceTotalPrice_Field3), nameof(sPurchaseAdviceUnpaidPBal))]
        private decimal sPurchaseAdviceTotalPrice_Field2
        {
            get
            {
                return (sPurchaseAdviceTotalPrice_Field3 / 100) * sPurchaseAdviceUnpaidPBal;
            }
        }
        public string sPurchaseAdviceTotalPrice_Field2_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceTotalPrice_Field2); }
        }

        [DependsOn(nameof(sPurchaseAdviceNetPrice_Field3), nameof(sPurchaseAdviceSRP_Field3))]
        private decimal sPurchaseAdviceTotalPrice_Field3
        {
            get
            {
                return sPurchaseAdviceNetPrice_Field3 + sPurchaseAdviceSRP_Field3;
            }
        }
        public string sPurchaseAdviceTotalPrice_Field3_rep
        {
            get { return ToRateString6DecimalDigits(() => sPurchaseAdviceTotalPrice_Field3); }
        }

        #endregion

        #region Interest fields
        [DependsOn]
        private decimal sPurchaseAdviceInterestDueSellerOrInvestor // Per PDE, we will not calculate this field for now and will leave it blank until further notice.
        {
            get { return GetMoneyField("sPurchaseAdviceInterestDueSellerOrInvestor"); }
            set { SetMoneyField("sPurchaseAdviceInterestDueSellerOrInvestor", value); }
        }
        public string sPurchaseAdviceInterestDueSellerOrInvestor_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceInterestDueSellerOrInvestor); }
            set { sPurchaseAdviceInterestDueSellerOrInvestor = ToMoney(value); }
        }

        [DependsOn]
        private decimal sPurchaseAdviceInterestReimbursement
        {
            get { return GetMoneyField("sPurchaseAdviceInterestReimbursement"); }
            set { SetMoneyField("sPurchaseAdviceInterestReimbursement", value); }
        }
        public string sPurchaseAdviceInterestReimbursement_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceInterestReimbursement); }
            set { sPurchaseAdviceInterestReimbursement = ToMoney(value); }
        }

        [DependsOn(nameof(sPurchaseAdviceInterestDueSellerOrInvestor), nameof(sPurchaseAdviceInterestReimbursement))]
        private decimal sPurchaseAdviceInterestTotalAdj
        {
            get
            {
                return SumMoney(sPurchaseAdviceInterestDueSellerOrInvestor, sPurchaseAdviceInterestReimbursement);
            }
        }
        public string sPurchaseAdviceInterestTotalAdj_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceInterestTotalAdj); }
        }

        #endregion

        #region Escrow Balance to Investor
        [DependsOn]
        private decimal sPurchaseAdviceEscrowTotCollAtClosing
        {
            get { return GetMoneyField("sPurchaseAdviceEscrowTotCollAtClosing"); }
            set { SetMoneyField("sPurchaseAdviceEscrowTotCollAtClosing", value); }
        }
        public string sPurchaseAdviceEscrowTotCollAtClosing_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceEscrowTotCollAtClosing); }
            set { sPurchaseAdviceEscrowTotCollAtClosing = ToMoney(value); }
        }

        [DependsOn]
        private decimal sPurchaseAdviceEscrowDepDue
        {
            get { return GetMoneyField("sPurchaseAdviceEscrowDepDue"); }
            set { SetMoneyField("sPurchaseAdviceEscrowDepDue", value); }
        }
        public string sPurchaseAdviceEscrowDepDue_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceEscrowDepDue); }
            set { sPurchaseAdviceEscrowDepDue = ToMoney(value); }
        }

        [DependsOn]
        private decimal sPurchaseAdviceEscrowDisbursements
        {
            get { return GetMoneyField("sPurchaseAdviceEscrowDisbursements"); }
            set { SetMoneyField("sPurchaseAdviceEscrowDisbursements", value); }
        }
        public string sPurchaseAdviceEscrowDisbursements_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceEscrowDisbursements); }
            set { sPurchaseAdviceEscrowDisbursements = ToMoney(value); }
        }

        [DependsOn(nameof(sPurchaseAdviceEscrowTotCollAtClosing), nameof(sPurchaseAdviceEscrowDepDue), nameof(sPurchaseAdviceEscrowDisbursements))]
        private decimal sPurchaseAdviceEscrowAmtDueInv
        {
            get
            {
                return SumMoney(sPurchaseAdviceEscrowTotCollAtClosing, sPurchaseAdviceEscrowDepDue, sPurchaseAdviceEscrowDisbursements);
            }
        }
        public string sPurchaseAdviceEscrowAmtDueInv_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceEscrowAmtDueInv); }
        }


        #endregion

        #region Purchase Summary fields
        [DependsOn(nameof(sPurchaseAdviceUnpaidPBal), nameof(sPurchaseAdviceTotalPrice_Field2), nameof(sPurchaseAdviceInterestTotalAdj), nameof(sPurchaseAdviceEscrowAmtDueInv), nameof(sPurchaseAdviceFeesTotal))]
        private decimal sPurchaseAdviceSummarySubtotal
        {
            get
            {
                return SumMoney(sPurchaseAdviceUnpaidPBal, sPurchaseAdviceTotalPrice_Field2, sPurchaseAdviceInterestTotalAdj, sPurchaseAdviceEscrowAmtDueInv, sPurchaseAdviceFeesTotal);
            }
        }
        public string sPurchaseAdviceSummarySubtotal_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceSummarySubtotal); }
        }

        [DependsOn]
        public string sPurchaseAdviceSummaryOtherAdjDesc
        {
            get { return GetStringVarCharField("sPurchaseAdviceSummaryOtherAdjDesc"); }
            set { SetStringVarCharField("sPurchaseAdviceSummaryOtherAdjDesc", value); }
        }

        [DependsOn]
        private decimal sPurchaseAdviceSummaryOtherAdjVal
        {
            get { return GetMoneyField("sPurchaseAdviceSummaryOtherAdjVal"); }
            set { SetMoneyField("sPurchaseAdviceSummaryOtherAdjVal", value); }
        }
        public string sPurchaseAdviceSummaryOtherAdjVal_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceSummaryOtherAdjVal); }
            set { sPurchaseAdviceSummaryOtherAdjVal = ToMoney(value); }
        }

        [DependsOn(nameof(sPurchaseAdviceSummarySubtotal), nameof(sPurchaseAdviceSummaryOtherAdjVal))]
        private decimal sPurchaseAdviceSummaryTotalDueSeller
        {
            get { return SumMoney(sPurchaseAdviceSummarySubtotal, sPurchaseAdviceSummaryOtherAdjVal); }
        }
        public string sPurchaseAdviceSummaryTotalDueSeller_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceSummaryTotalDueSeller); }
        }
        
        [DependsOn]
        public string sPurchaseAdviceSummaryInvNm
        {
            get { return GetStringVarCharField("sPurchaseAdviceSummaryInvNm"); }
            set { SetStringVarCharField("sPurchaseAdviceSummaryInvNm", value); }
        }

        // gf OPM 95063
        // Stores the Investor Rolodex Id of the Purchase Advice Investor.
        // If the value is not found in the rolodex, this should be -1.
        [DependsOn]
        public int sPurchaseAdviceSummaryInvId
        {
            get { return GetCountField("sPurchaseAdviceSummaryInvId"); }
            set { SetCountField("sPurchaseAdviceSummaryInvId", value); }
        }
        public string sPurchaseAdviceSummaryInvId_rep
        {
            get { return ToCountString(sPurchaseAdviceSummaryInvId); }
            set { sPurchaseAdviceSummaryInvId = ToCount(value); }
        }

        private InvestorRolodexEntry m_purchaseAdviceInvestorRolodexEntry;
        [DependsOn(nameof(sPurchaseAdviceSummaryInvId))]
        [DevNote("Exposed to allow batch export of this field.")]
        public string sPurchaseAdviceSummaryInvMainAddr
        {
            get
            {
                if (m_purchaseAdviceInvestorRolodexEntry == null)
                {
                    m_purchaseAdviceInvestorRolodexEntry = InvestorRolodexEntry.Get(m_brokerDB.BrokerID, 
                                                                                    sPurchaseAdviceSummaryInvId);
                }
                string addr = Tools.FormatSingleLineAddress(m_purchaseAdviceInvestorRolodexEntry.MainAddress,
                                                            m_purchaseAdviceInvestorRolodexEntry.MainCity,
                                                            m_purchaseAdviceInvestorRolodexEntry.MainState,
                                                            m_purchaseAdviceInvestorRolodexEntry.MainZip);
                return GetStringForCacheTable(addr, "sPurchaseAdviceSummaryInvMainAddr");
            }
        }

        [DependsOn(nameof(sPurchaseAdviceSummaryInvId))]
        [DevNote("Exposed to allow batch export of this field.")]
        public string sPurchaseAdviceSummaryInvPmtAddr
        {
            get
            {
                if (m_purchaseAdviceInvestorRolodexEntry == null)
                {
                    m_purchaseAdviceInvestorRolodexEntry = InvestorRolodexEntry.Get(m_brokerDB.BrokerID,
                                                                                    sPurchaseAdviceSummaryInvId);
                }
                string addr = Tools.FormatSingleLineAddress(m_purchaseAdviceInvestorRolodexEntry.PaymentToAddress,
                                                            m_purchaseAdviceInvestorRolodexEntry.PaymentToCity,
                                                            m_purchaseAdviceInvestorRolodexEntry.PaymentToState,
                                                            m_purchaseAdviceInvestorRolodexEntry.PaymentToZip);
                return GetStringForCacheTable(addr, "sPurchaseAdviceSummaryInvPmtAddr");
            }
        }

        [DependsOn]
        public string sPurchaseAdviceSummaryInvPgNm
        {
            get { return GetStringVarCharField("sPurchaseAdviceSummaryInvPgNm"); }
            set { SetStringVarCharField("sPurchaseAdviceSummaryInvPgNm", value); }
        }

        [DependsOn]
        public string sPurchaseAdviceSummaryInvLoanNm
        {
            get { return GetStringVarCharField("sPurchaseAdviceSummaryInvLoanNm"); }
            set { SetStringVarCharField("sPurchaseAdviceSummaryInvLoanNm", value); }
        }

        [DependsOn]
        public string sPurchaseAdviceSummaryInvCommNm
        {
            get { return GetStringVarCharField("sPurchaseAdviceSummaryInvCommNm"); }
            set { SetStringVarCharField("sPurchaseAdviceSummaryInvCommNm", value); }
        }

        // TODO - try to use Enum instead (need UI to be on board)
        public static List<string> sPurchaseAdviceServicingStatus_map = new List<string>() { "Released", "Retained" };
        
        [DependsOn]
        [LqbInputModel(type: InputFieldType.DropDownList, enumType:typeof(E_ServicingStatus))]
        public string sPurchaseAdviceSummaryServicingStatus
        {
            get {
                string val = GetStringVarCharField("sPurchaseAdviceSummaryServicingStatus");

                // OPM 138506 - sPurchaseAdviceSummaryServicingStatus should default to "0" (Released)
                if (string.IsNullOrEmpty(val))
                    val = ((int)E_ServicingStatus.Released).ToString();

                return val;
            }
            set { SetStringVarCharField("sPurchaseAdviceSummaryServicingStatus", value); }
        }

        #endregion

        #region Purchase Advice Purchasing Fields (OPM 184557)
        [DependsOn][DevNote("OPM 184557 - Other Adjustments description for Purchase Advice - Purchasing page.")]
        public string sPurchaseAdviceSummaryOtherAdjDesc_Purchasing
        {
            get { return GetStringVarCharField("sPurchaseAdviceSummaryOtherAdjDesc_Purchasing"); }
            set { SetStringVarCharField("sPurchaseAdviceSummaryOtherAdjDesc_Purchasing", value); }
        }

        [DependsOn][DevNote("OPM 184557 - Other Adjustments for Purchase Advice - Purchasing page.")]
        private decimal sPurchaseAdviceSummaryOtherAdjVal_Purchasing
        {
            get { return GetMoneyField("sPurchaseAdviceSummaryOtherAdjVal_Purchasing"); }
            set { SetMoneyField("sPurchaseAdviceSummaryOtherAdjVal_Purchasing", value); }
        }
        public string sPurchaseAdviceSummaryOtherAdjVal_Purchasing_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceSummaryOtherAdjVal_Purchasing); }
            set { sPurchaseAdviceSummaryOtherAdjVal_Purchasing = ToMoney(value); }
        }

        [DependsOn(nameof(sPurchaseAdviceSummarySubtotal_Purchasing), nameof(sPurchaseAdviceSummaryOtherAdjVal_Purchasing))]
        [DevNote("OPM 184557 - Total Due Seller for Purchase Advice - Purchasing page.")]
        private decimal sPurchaseAdviceSummaryTotalDueSeller_Purchasing
        {
            get { return SumMoney(sPurchaseAdviceSummarySubtotal_Purchasing, sPurchaseAdviceSummaryOtherAdjVal_Purchasing); }
        }
        public string sPurchaseAdviceSummaryTotalDueSeller_Purchasing_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceSummaryTotalDueSeller_Purchasing); }
        }

        [DependsOn]
        [DevNote("OPM 184557 - Servicing Status for Purchase Advice - Purchasing page.")]
        public string sPurchaseAdviceSummaryServicingStatus_Purchasing
        {
            get
            {
                string val = GetStringVarCharField("sPurchaseAdviceSummaryServicingStatus_Purchasing");

                // OPM 138506 - sPurchaseAdviceSummaryServicingStatus_Purchasing should default to "0" (Released)
                if (string.IsNullOrEmpty(val))
                    val = ((int)E_ServicingStatus.Released).ToString();

                return val;
            }
            set { SetStringVarCharField("sPurchaseAdviceSummaryServicingStatus_Purchasing", value); }
        }

        // From the spec: sum of principal payments/adjustments from the servicing screen (negative means principal has been paid off).
        // This just means grab the value from the Principal summary column's "Total w/pmts due" field in the servicing page (sServicingTotalDueFunds_Principal).
        [DependsOn]
        [DevNote("OPM 184557 - Amortization/Curtailments amount for Purchase Advice - Purchasing page.")]
        private decimal sPurchaseAdviceAmortCurtail_Purchasing
        {
            get { return GetMoneyField("sPurchaseAdviceAmortCurtail_Purchasing"); }
            set { SetMoneyField("sPurchaseAdviceAmortCurtail_Purchasing", value); }
        }
        public string sPurchaseAdviceAmortCurtail_Purchasing_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceAmortCurtail_Purchasing); }
            set { sPurchaseAdviceAmortCurtail_Purchasing = ToMoney(value); }
        }

        [DependsOn(nameof(sFinalLAmt), nameof(sPurchaseAdviceAmortCurtail_Purchasing))]
        [DevNote("OPM 184557 - Unpaid Principle(?) Balance for Purchase Advice - Purchasing page.")]
        private decimal sPurchaseAdviceUnpaidPBal_Purchasing
        {
            get
            {
                return SumMoney(sFinalLAmt, sPurchaseAdviceAmortCurtail_Purchasing);
            }
        }
        public string sPurchaseAdviceUnpaidPBal_Purchasing_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceUnpaidPBal_Purchasing); }
        }

        [DependsOn(nameof(sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing))]
        [DevNote("OPM 184557 - Base Price calculation mode for Purchase Advice - Purchasing page.")]
        public E_AmountPriceCalcMode sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing
        {
            get { return (E_AmountPriceCalcMode)GetTypeIndexField("sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing"); }
            set { SetTypeIndexField("sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing", (int)value); }
        }

        [DependsOn(nameof(sPurchaseAdviceBasePrice_Field1_Purchasing), nameof(sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing), nameof(sPurchaseAdviceUnpaidPBal_Purchasing))]
        [DevNote("OPM 184557 - Base Price Rate for Purchase Advice - Purchasing page.")]
        private decimal sPurchaseAdviceBasePrice_Field1_Purchasing
        {
            get
            {
                switch (sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing)
                {
                    case E_AmountPriceCalcMode.Amount:
                        if (sPurchaseAdviceUnpaidPBal_Purchasing == 0)
                        {
                            return 100;
                        }
                        return 100 + 100 * (sPurchaseAdviceBasePrice_Field2_Purchasing / sPurchaseAdviceUnpaidPBal_Purchasing);
                    case E_AmountPriceCalcMode.Price:
                        return GetRateField("sPurchaseAdviceBasePrice_Field1_Purchasing");
                    default:
                        throw new UnhandledEnumException(sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing);
                }

            }
            set
            {
                SetRateField("sPurchaseAdviceBasePrice_Field1_Purchasing", value);
            }
        }

        public string sPurchaseAdviceBasePrice_Field1_Purchasing_rep
        {
            get { return ToRateString6DecimalDigits(() => sPurchaseAdviceBasePrice_Field1_Purchasing); }
            set { sPurchaseAdviceBasePrice_Field1_Purchasing = ToRate(value); }
        }

        [DependsOn(nameof(sPurchaseAdviceBasePrice_Field2_Purchasing), nameof(sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing), nameof(sPurchaseAdviceUnpaidPBal_Purchasing))]
        [DevNote("OPM 184557 - Base Price Amount for Purchase Advice - Purchasing page.")]
        private decimal sPurchaseAdviceBasePrice_Field2_Purchasing
        {
            get
            {
                switch (sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing)
                {
                    case E_AmountPriceCalcMode.Amount:
                        return GetMoneyField("sPurchaseAdviceBasePrice_Field2_Purchasing");
                    case E_AmountPriceCalcMode.Price:
                        //should match field3 logic - I removed it to get rid of depdency
                        return ((sPurchaseAdviceBasePrice_Field1_Purchasing - 100) / 100) * sPurchaseAdviceUnpaidPBal_Purchasing;
                    default:
                        throw new UnhandledEnumException(sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing);
                }
            }
            set
            {
                SetMoneyField("sPurchaseAdviceBasePrice_Field2_Purchasing", value);
            }
        }

        public string sPurchaseAdviceBasePrice_Field2_Purchasing_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceBasePrice_Field2_Purchasing); }
            set { sPurchaseAdviceBasePrice_Field2_Purchasing = ToMoney(value); }
        }


        [DependsOn(nameof(sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing), nameof(sPurchaseAdviceBasePrice_Field1_Purchasing))]
        [DevNote("OPM 184557 - Base Price rate Adjustment for Purchase Advice - Purchasing page.")]
        private decimal sPurchaseAdviceBasePrice_Field3_Purchasing
        {
            get
            {
                return sPurchaseAdviceBasePrice_Field1_Purchasing - 100;
            }
        }
        public string sPurchaseAdviceBasePrice_Field3_Purchasing_rep
        {
            get { return ToRateString6DecimalDigits(() => sPurchaseAdviceBasePrice_Field3_Purchasing); }

        }

        [DependsOn(nameof(sPurchaseAdviceAdjustments_Field2_Purchasing), nameof(sPurchaseAdviceUnpaidPBal_Purchasing), nameof(sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing), nameof(sPurchaseAdviceAdjustmentsNonSRPTotalAmt_Purchasing))]
        [DevNote("OPM 184557 - Purchase Advice Adjustments Amount for Purchase Advice - Purchasing page.")]
        private decimal sPurchaseAdviceAdjustments_Field2_Purchasing
        {
            get
            {
                switch (sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing)
                {
                    case E_AmountPriceCalcMode.Amount:
                        return sPurchaseAdviceAdjustmentsNonSRPTotalAmt_Purchasing;
                    case E_AmountPriceCalcMode.Price:
                        return (sPurchaseAdviceAdjustments_Field3_Purchasing / 100) * sPurchaseAdviceUnpaidPBal_Purchasing;
                    default:
                        throw new UnhandledEnumException(sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing);
                }
            }
        }
        public string sPurchaseAdviceAdjustments_Field2_Purchasing_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceAdjustments_Field2_Purchasing); }
        }

        // Dependency for sPurchaseAdviceAdjustments_Field2_Purchasing left off to avoid circular dependency. This is handled in FieldInfo.cs
        [DependsOn(nameof(sPurchaseAdviceAdjustments_Field3_Purchasing), nameof(sPurchaseAdviceAdjustmentsNonSRPTotal_Purchasing), nameof(sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing), nameof(sPurchaseAdviceUnpaidPBal_Purchasing))]
        [DevNote("OPM 184557 - Purchase Advice Adjustments rate adjustment for Purchase Advice - Purchasing page.")]
        private decimal sPurchaseAdviceAdjustments_Field3_Purchasing
        {
            get
            {
                switch (sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing)
                {
                    case E_AmountPriceCalcMode.Amount:
                        if (sPurchaseAdviceUnpaidPBal_Purchasing == 0)
                        {
                            return 0;
                        }
                        return (sPurchaseAdviceAdjustments_Field2_Purchasing * 100) / sPurchaseAdviceUnpaidPBal_Purchasing;
                    case E_AmountPriceCalcMode.Price:
                        return sPurchaseAdviceAdjustmentsNonSRPTotal_Purchasing;
                    default:
                        throw new UnhandledEnumException(sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing);
                }
            }
        }
        public string sPurchaseAdviceAdjustments_Field3_Purchasing_rep
        {
            get { return ToRateString6DecimalDigits(() => sPurchaseAdviceAdjustments_Field3_Purchasing); }
        }

        [DependsOn(nameof(sPurchaseAdviceBasePrice_Field1_Purchasing), nameof(sPurchaseAdviceAdjustments_Field3_Purchasing))]
        [DevNote("OPM 184557 - Purchase Advice Net Price rate for Purchase Advice - Purchasing page.")]
        private decimal sPurchaseAdviceNetPrice_Field1_Purchasing
        {
            get { return sPurchaseAdviceBasePrice_Field1_Purchasing + sPurchaseAdviceAdjustments_Field3_Purchasing; }
        }
        public string sPurchaseAdviceNetPrice_Field1_Purchasing_rep
        {
            get { return string.Format("{0:N6}", sPurchaseAdviceNetPrice_Field1_Purchasing); }
        }

        [DependsOn(nameof(sPurchaseAdviceNetPrice_Field3_Purchasing), nameof(sPurchaseAdviceUnpaidPBal_Purchasing))]
        [DevNote("OPM 184557 - Purchase Advice Net Price amount for Purchase Advice - Purchasing page.")]
        private decimal sPurchaseAdviceNetPrice_Field2_Purchasing
        {
            get
            {
                return (sPurchaseAdviceNetPrice_Field3_Purchasing / 100) * sPurchaseAdviceUnpaidPBal_Purchasing;
            }
        }
        public string sPurchaseAdviceNetPrice_Field2_Purchasing_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceNetPrice_Field2_Purchasing); }
        }

        [DependsOn(nameof(sPurchaseAdviceBasePrice_Field3_Purchasing), nameof(sPurchaseAdviceAdjustments_Field3_Purchasing))]
        [DevNote("OPM 184557 - Purchase Advice Net Price rate adjustment for Purchase Advice - Purchasing page.")]
        private decimal sPurchaseAdviceNetPrice_Field3_Purchasing
        {
            get
            {
                return sPurchaseAdviceBasePrice_Field3_Purchasing + sPurchaseAdviceAdjustments_Field3_Purchasing;
            }
        }
        public string sPurchaseAdviceNetPrice_Field3_Purchasing_rep
        {
            get { return ToRateString6DecimalDigits(() => sPurchaseAdviceNetPrice_Field3_Purchasing); }
        }

        [DependsOn(nameof(sPurchaseAdviceSRP_Field2_Purchasing), nameof(sPurchaseAdviceUnpaidPBal_Purchasing), nameof(sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing), nameof(sPurchaseAdviceAdjustmentsSRPTotalAmt_Purchasing))]
        [DevNote("OPM 184557 - Purchase Advice SRP amount for Purchase Advice - Purchasing page.")]
        private decimal sPurchaseAdviceSRP_Field2_Purchasing
        {
            get
            {
                switch (sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing)
                {
                    case E_AmountPriceCalcMode.Amount:
                        return sPurchaseAdviceAdjustmentsSRPTotalAmt_Purchasing;
                    case E_AmountPriceCalcMode.Price:
                        return (sPurchaseAdviceSRP_Field3_Purchasing / 100) * sPurchaseAdviceUnpaidPBal_Purchasing;
                    default:
                        throw new UnhandledEnumException(sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing);
                }
            }
        }
        public string sPurchaseAdviceSRP_Field2_Purchasing_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceSRP_Field2_Purchasing); }
        }

        // Dependency for sPurchaseAdviceSRP_Field2_Purchasing left off to avoid circular dependency. This is handled in FieldInfo.cs
        [DependsOn(nameof(sPurchaseAdviceSRP_Field3_Purchasing), nameof(sPurchaseAdviceAdjustmentsSRPTotal_Purchasing), nameof(sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing), nameof(sPurchaseAdviceUnpaidPBal_Purchasing))]
        [DevNote("OPM 184557 - Purchase Advice SRP rate adjustment for Purchase Advice - Purchasing page.")]
        private decimal sPurchaseAdviceSRP_Field3_Purchasing
        {
            get
            {
                switch (sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing)
                {
                    case E_AmountPriceCalcMode.Amount:
                        if (sPurchaseAdviceUnpaidPBal_Purchasing == 0)
                        {
                            return 0;
                        }
                        return (sPurchaseAdviceSRP_Field2_Purchasing * 100) / sPurchaseAdviceUnpaidPBal_Purchasing;
                    case E_AmountPriceCalcMode.Price:
                        return sPurchaseAdviceAdjustmentsSRPTotal_Purchasing;
                    default:
                        throw new UnhandledEnumException(sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing);
                }

            }
        }
        public string sPurchaseAdviceSRP_Field3_Purchasing_rep
        {
            get { return ToRateString6DecimalDigits(() => sPurchaseAdviceSRP_Field3_Purchasing); }
        }

        [DependsOn(nameof(sPurchaseAdviceNetPrice_Field1_Purchasing), nameof(sPurchaseAdviceSRP_Field3_Purchasing))]
        [DevNote("OPM 184557 - Purchase Advice Total Price rate for Purchase Advice - Purchasing page.")]
        public decimal sPurchaseAdviceTotalPrice_Field1_Purchasing
        {
            get
            {
                return sPurchaseAdviceNetPrice_Field1_Purchasing + sPurchaseAdviceSRP_Field3_Purchasing;
            }
        }
        public string sPurchaseAdviceTotalPrice_Field1_Purchasing_rep
        {
            get { return string.Format("{0:N6}", sPurchaseAdviceTotalPrice_Field1_Purchasing); }
        }

        [DependsOn(nameof(sPurchaseAdviceTotalPrice_Field3_Purchasing), nameof(sPurchaseAdviceUnpaidPBal_Purchasing))]
        [DevNote("OPM 184557 - Purchase Advice Total Price amount for Purchase Advice - Purchasing page.")]
        private decimal sPurchaseAdviceTotalPrice_Field2_Purchasing
        {
            get
            {
                return (sPurchaseAdviceTotalPrice_Field3_Purchasing / 100) * sPurchaseAdviceUnpaidPBal_Purchasing;
            }
        }
        public string sPurchaseAdviceTotalPrice_Field2_Purchasing_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceTotalPrice_Field2_Purchasing); }
        }

        [DependsOn(nameof(sPurchaseAdviceNetPrice_Field3_Purchasing), nameof(sPurchaseAdviceSRP_Field3_Purchasing))]
        [DevNote("OPM 184557 - Purchase Advice Total Price rate adjustment for Purchase Advice - Purchasing page.")]
        private decimal sPurchaseAdviceTotalPrice_Field3_Purchasing
        {
            get
            {
                return sPurchaseAdviceNetPrice_Field3_Purchasing + sPurchaseAdviceSRP_Field3_Purchasing;
            }
        }
        public string sPurchaseAdviceTotalPrice_Field3_Purchasing_rep
        {
            get { return ToRateString6DecimalDigits(() => sPurchaseAdviceTotalPrice_Field3_Purchasing); }
        }

        [DependsOn(nameof(sPurchaseAdviceUnpaidPBal_Purchasing), nameof(sPurchaseAdviceTotalPrice_Field2_Purchasing), nameof(sPurchaseAdviceInterestTotalAdj_Purchasing), nameof(sPurchaseAdviceEscrowAmtDueInv_Purchasing), nameof(sPurchaseAdviceFeesTotal_Purchasing))]
        [DevNote("OPM 184557 - Purchase Summary Subtotal for Purchase Advice - Purchasing page.")]
        private decimal sPurchaseAdviceSummarySubtotal_Purchasing
        {
            get
            {
                return SumMoney(sPurchaseAdviceUnpaidPBal_Purchasing, sPurchaseAdviceTotalPrice_Field2_Purchasing, sPurchaseAdviceInterestTotalAdj_Purchasing, sPurchaseAdviceEscrowAmtDueInv_Purchasing, sPurchaseAdviceFeesTotal_Purchasing);
            }
        }
        public string sPurchaseAdviceSummarySubtotal_Purchasing_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceSummarySubtotal_Purchasing); }
        }

        [DependsOn]
        [DevNote("OPM 184557 - Interest Due Seller or Investor - Purchasing page.")]
        private decimal sPurchaseAdviceInterestDueSellerOrInvestor_Purchasing // Per PDE, we will not calculate this field for now and will leave it blank until further notice.
        {
            get { return GetMoneyField("sPurchaseAdviceInterestDueSellerOrInvestor_Purchasing"); }
            set { SetMoneyField("sPurchaseAdviceInterestDueSellerOrInvestor_Purchasing", value); }
        }
        public string sPurchaseAdviceInterestDueSellerOrInvestor_Purchasing_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceInterestDueSellerOrInvestor_Purchasing); }
            set { sPurchaseAdviceInterestDueSellerOrInvestor_Purchasing = ToMoney(value); }
        }

        [DependsOn]
        [DevNote("OPM 184557 - Interest Reimbursement for Purchase Advice - Purchasing page.")]
        private decimal sPurchaseAdviceInterestReimbursement_Purchasing
        {
            get { return GetMoneyField("sPurchaseAdviceInterestReimbursement_Purchasing"); }
            set { SetMoneyField("sPurchaseAdviceInterestReimbursement_Purchasing", value); }
        }
        public string sPurchaseAdviceInterestReimbursement_Purchasing_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceInterestReimbursement_Purchasing); }
            set { sPurchaseAdviceInterestReimbursement_Purchasing = ToMoney(value); }
        }

        [DependsOn(nameof(sPurchaseAdviceInterestDueSellerOrInvestor_Purchasing), nameof(sPurchaseAdviceInterestReimbursement_Purchasing))]
        [DevNote("OPM 184557 - Total Interest Adjustments for Purchase Advice - Purchasing page.")]
        private decimal sPurchaseAdviceInterestTotalAdj_Purchasing
        {
            get
            {
                return SumMoney(sPurchaseAdviceInterestDueSellerOrInvestor_Purchasing, sPurchaseAdviceInterestReimbursement_Purchasing);
            }
        }
        public string sPurchaseAdviceInterestTotalAdj_Purchasing_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceInterestTotalAdj_Purchasing); }
        }

        [DependsOn]
        [DevNote("OPM 184557 - Total Escrow Collected At Closing for Purchase Advice - Purchasing page.")]
        private decimal sPurchaseAdviceEscrowTotCollAtClosing_Purchasing
        {
            get { return GetMoneyField("sPurchaseAdviceEscrowTotCollAtClosing_Purchasing"); }
            set { SetMoneyField("sPurchaseAdviceEscrowTotCollAtClosing_Purchasing", value); }
        }
        public string sPurchaseAdviceEscrowTotCollAtClosing_Purchasing_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceEscrowTotCollAtClosing_Purchasing); }
            set { sPurchaseAdviceEscrowTotCollAtClosing_Purchasing = ToMoney(value); }
        }

        [DependsOn]
        [DevNote("OPM 184557 - Escrow Deposit with Amortization Payment for Purchase Advice - Purchasing page.")]
        private decimal sPurchaseAdviceEscrowDepDue_Purchasing
        {
            get { return GetMoneyField("sPurchaseAdviceEscrowDepDue_Purchasing"); }
            set { SetMoneyField("sPurchaseAdviceEscrowDepDue_Purchasing", value); }
        }
        public string sPurchaseAdviceEscrowDepDue_Purchasing_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceEscrowDepDue_Purchasing); }
            set { sPurchaseAdviceEscrowDepDue_Purchasing = ToMoney(value); }
        }

        [DependsOn]
        [DevNote("OPM 184557 - Escrow Disbursements for Purchase Advice - Purchasing page.")]
        private decimal sPurchaseAdviceEscrowDisbursements_Purchasing
        {
            get { return GetMoneyField("sPurchaseAdviceEscrowDisbursements_Purchasing"); }
            set { SetMoneyField("sPurchaseAdviceEscrowDisbursements_Purchasing", value); }
        }
        public string sPurchaseAdviceEscrowDisbursements_Purchasing_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceEscrowDisbursements_Purchasing); }
            set { sPurchaseAdviceEscrowDisbursements_Purchasing = ToMoney(value); }
        }

        [DependsOn(nameof(sPurchaseAdviceEscrowTotCollAtClosing_Purchasing), nameof(sPurchaseAdviceEscrowDepDue_Purchasing), nameof(sPurchaseAdviceEscrowDisbursements_Purchasing))]
        [DevNote("OPM 184557 - Total Escrow Amount Due Investor for Purchase Advice - Purchasing page.")]
        private decimal sPurchaseAdviceEscrowAmtDueInv_Purchasing
        {
            get
            {
                return SumMoney(sPurchaseAdviceEscrowTotCollAtClosing_Purchasing, sPurchaseAdviceEscrowDepDue_Purchasing, sPurchaseAdviceEscrowDisbursements_Purchasing);
            }
        }
        public string sPurchaseAdviceEscrowAmtDueInv_Purchasing_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceEscrowAmtDueInv_Purchasing); }
        }

        private decimal m_sPurchaseAdviceAdjustmentsSRPTotal_Purchasing = 0;
        private decimal m_sPurchaseAdviceAdjustmentsNonSRPTotal_Purchasing = 0;
        private decimal m_sPurchaseAdviceAdjustmentsSRPTotalAmt_Purchasing = 0;
        private decimal m_sPurchaseAdviceAdjustmentsNonSRPTotalAmt_Purchasing = 0;
        private bool m_arePurchaseAdviceAdjFieldsSet_Purchasing = false;

        [DependsOn(nameof(sPurchaseAdviceAdjustments_Purchasing), nameof(SetPurchaseAdviceAdjustmentSummaryData_Purchasing))]
        [DevNote("OPM 184557 - Adjustments SRP Total for Purchase Advice - Purchasing page. Set when sPurchaseAdviceAdjustments_Purchasing is set.")]
        private decimal sPurchaseAdviceAdjustmentsSRPTotal_Purchasing
        {
            get
            {
                if (m_arePurchaseAdviceAdjFieldsSet_Purchasing == false)
                {
                    SetPurchaseAdviceAdjustmentSummaryData_Purchasing(sPurchaseAdviceAdjustments_Purchasing);
                }
                return m_sPurchaseAdviceAdjustmentsSRPTotal_Purchasing;
            }
        }
        public string sPurchaseAdviceAdjustmentsSRPTotal_Purchasing_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceAdjustmentsSRPTotal_Purchasing); }
        }

        [DependsOn(nameof(sPurchaseAdviceAdjustments_Purchasing), nameof(SetPurchaseAdviceAdjustmentSummaryData_Purchasing))]
        [DevNote("OPM 184557 - Adjustments SRP Total Amount for Purchase Advice - Purchasing page. Set when sPurchaseAdviceAdjustments_Purchasing is set.")]
        private decimal sPurchaseAdviceAdjustmentsSRPTotalAmt_Purchasing
        {
            get
            {
                if (m_arePurchaseAdviceAdjFieldsSet_Purchasing == false)
                {
                    SetPurchaseAdviceAdjustmentSummaryData_Purchasing(sPurchaseAdviceAdjustments_Purchasing);
                }
                return m_sPurchaseAdviceAdjustmentsSRPTotalAmt_Purchasing;
            }
        }
        public string sPurchaseAdviceAdjustmentsSRPTotalAmt_Purchasing_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceAdjustmentsSRPTotalAmt_Purchasing); }
        }

        [DependsOn(nameof(sPurchaseAdviceAdjustments_Purchasing), nameof(SetPurchaseAdviceAdjustmentSummaryData_Purchasing))]
        [DevNote("OPM 184557 - Adjustments Non-SRP Total for Purchase Advice - Purchasing page. Set when sPurchaseAdviceAdjustments_Purchasing is set.")]
        private decimal sPurchaseAdviceAdjustmentsNonSRPTotal_Purchasing
        {
            get
            {
                if (m_arePurchaseAdviceAdjFieldsSet_Purchasing == false)
                {
                    SetPurchaseAdviceAdjustmentSummaryData_Purchasing(sPurchaseAdviceAdjustments_Purchasing);
                }
                return m_sPurchaseAdviceAdjustmentsNonSRPTotal_Purchasing;
            }
        }
        public string sPurchaseAdviceAdjustmentsNonSRPTotal_Purchasing_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceAdjustmentsNonSRPTotal_Purchasing); }
        }

        [DependsOn(nameof(sPurchaseAdviceAdjustments_Purchasing), nameof(SetPurchaseAdviceAdjustmentSummaryData_Purchasing))]
        [DevNote("OPM 184557 - Adjustments Non-SRP Total Amount for Purchase Advice - Purchasing page. Set when sPurchaseAdviceAdjustments_Purchasing is set.")]
        private decimal sPurchaseAdviceAdjustmentsNonSRPTotalAmt_Purchasing
        {
            get
            {
                if (m_arePurchaseAdviceAdjFieldsSet_Purchasing == false)
                {
                    SetPurchaseAdviceAdjustmentSummaryData_Purchasing(sPurchaseAdviceAdjustments_Purchasing);
                }
                return m_sPurchaseAdviceAdjustmentsNonSRPTotalAmt_Purchasing;
            }
        }
        public string sPurchaseAdviceAdjustmentsNonSRPTotalAmt_Purchasing_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceAdjustmentsNonSRPTotalAmt_Purchasing); }
        }

        [DependsOn(nameof(sPurchaseAdviceAdjustments_Purchasing), nameof(SetPurchaseAdviceAdjustmentSummaryData_Purchasing))]
        [DevNote("OPM 184557 - Adjustments Total Amount for Purchase Advice - Purchasing page. Relies on sPurchaseAdviceAdjustments_Purchasing being set.")]
        private decimal sPurchaseAdviceAdjustmentsTotal_Purchasing
        {
            get
            {
                if (m_arePurchaseAdviceAdjFieldsSet_Purchasing == false)
                {
                    SetPurchaseAdviceAdjustmentSummaryData_Purchasing(sPurchaseAdviceAdjustments_Purchasing);
                }
                return m_sPurchaseAdviceAdjustmentsSRPTotal_Purchasing + m_sPurchaseAdviceAdjustmentsNonSRPTotal_Purchasing;
            }
        }
        public string sPurchaseAdviceAdjustmentsTotal_Purchasing_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceAdjustmentsTotal_Purchasing); }
        }

        [DependsOn(nameof(sPurchaseAdviceUnpaidPBal_Purchasing), nameof(sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing))]
        private void SetPurchaseAdviceAdjustmentSummaryData_Purchasing(List<CPurchaseAdviceAdjustmentsFields> adjustmentFields)
        {
            m_arePurchaseAdviceAdjFieldsSet_Purchasing = true;

            foreach (CPurchaseAdviceAdjustmentsFields field in adjustmentFields)
            {
                if (sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing == E_AmountPriceCalcMode.Amount)
                {
                    field.AdjPc_rep = ToRateString6DecimalDigits(() => (field.AdjAmt * 100) / sPurchaseAdviceUnpaidPBal_Purchasing);
                }
                else
                {
                    field.AdjAmt_rep = ToMoneyString((field.AdjPc / 100) * sPurchaseAdviceUnpaidPBal_Purchasing);
                }

                if (field.IsSRP)
                {
                    m_sPurchaseAdviceAdjustmentsSRPTotalAmt_Purchasing += field.AdjAmt;
                    m_sPurchaseAdviceAdjustmentsSRPTotal_Purchasing += field.AdjPc;
                }
                else
                {
                    m_sPurchaseAdviceAdjustmentsNonSRPTotalAmt_Purchasing += field.AdjAmt;
                    m_sPurchaseAdviceAdjustmentsNonSRPTotal_Purchasing += field.AdjPc;
                }
            }
        }

        /// <summary>
        /// List of fees on the purchase advice page.
        /// </summary>
        [DependsOn]
        [DevNote("OPM 184557 - Purchase Advice Adjustments XML as string for Purchasing Advice - Purchasing page")]
        public string sPurchaseAdviceAdjustmentsXmlContent_Purchasing
        {
            get { return GetLongTextField("sPurchaseAdviceAdjustmentsXmlContent_Purchasing"); }
            set { SetLongTextField("sPurchaseAdviceAdjustmentsXmlContent_Purchasing", value); }
        }

        /// <summary>
        /// Used by the sPurchaseAdviceAdjustments property. XmlSerializer is thread safe.
        /// </summary>
        private static System.Xml.Serialization.XmlSerializer x_PurchaseAdviceAdjustmentsSerializer_Purchasing = new System.Xml.Serialization.XmlSerializer(typeof(List<CPurchaseAdviceAdjustmentsFields>));
        [DependsOn(nameof(sPurchaseAdviceAdjustmentsXmlContent_Purchasing), nameof(SetPurchaseAdviceAdjustmentSummaryData_Purchasing))]
        [DevNote("OPM 184557 - Adjustments List for Purchasing Advice - Purchasing page")]
        public List<CPurchaseAdviceAdjustmentsFields> sPurchaseAdviceAdjustments_Purchasing
        {
            get
            {
                string data = sPurchaseAdviceAdjustmentsXmlContent_Purchasing;
                if (string.IsNullOrEmpty(data))
                {
                    return new List<CPurchaseAdviceAdjustmentsFields>();
                }

                using (System.IO.TextReader textReader = new System.IO.StringReader(data))
                {
                    return (List<CPurchaseAdviceAdjustmentsFields>)x_PurchaseAdviceAdjustmentsSerializer_Purchasing.Deserialize(textReader);
                }
            }
            set
            {
                SetPurchaseAdviceAdjustmentSummaryData_Purchasing(value);

                using (var writer = new System.IO.StringWriter())
                {
                    x_PurchaseAdviceAdjustmentsSerializer_Purchasing.Serialize(writer, value);
                    sPurchaseAdviceAdjustmentsXmlContent_Purchasing = writer.ToString();
                }
            }
        }

        private decimal m_sPurchaseAdviceFeesTotal_Purchasing = 0;

        [DependsOn(nameof(sPurchaseAdviceFees_Purchasing), nameof(SetPurchaseAdviceFeesSummaryData_Purchasing))]
        [DevNote("OPM 184557 - Total Fees for Purchasing Advice - Purchasing page")]
        private decimal sPurchaseAdviceFeesTotal_Purchasing
        {
            get
            {
                if (m_arePurchaseAdviceFieldsSet_Purchasing == false)
                {
                    SetPurchaseAdviceFeesSummaryData_Purchasing(sPurchaseAdviceFees_Purchasing);
                }
                return m_sPurchaseAdviceFeesTotal_Purchasing;
            }
            set { m_sPurchaseAdviceFeesTotal_Purchasing = value; }
        }
        public string sPurchaseAdviceFeesTotal_Purchasing_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceFeesTotal_Purchasing); }
        }

        /// <summary>
        /// List of fees on the purchase advice page.
        /// </summary>
        [DependsOn]
        [DevNote("OPM 184557 - Purchase Advice Fees XML as string for Purchasing Advice - Purchasing page")]
        public string sPurchaseAdviceFeesXmlContent_Purchasing
        {
            get { return GetLongTextField("sPurchaseAdviceFeesXmlContent_Purchasing"); }
            set { SetLongTextField("sPurchaseAdviceFeesXmlContent_Purchasing", value); }
        }

        private bool m_arePurchaseAdviceFieldsSet_Purchasing = false;

        /// <summary>
        /// Used by the sPurchaseAdviceFees property. XmlSerializer is thread safe.
        /// </summary>
        private static System.Xml.Serialization.XmlSerializer x_PurchaseAdviceFeesSerializer_Purchasing = new System.Xml.Serialization.XmlSerializer(typeof(List<CPurchaseAdviceFeesFields>));
        [DependsOn(nameof(sPurchaseAdviceFeesXmlContent_Purchasing),nameof(SetPurchaseAdviceFeesSummaryData_Purchasing))]
        [DevNote("OPM 184557 - Fees List for Purchasing Advice - Purchasing page")]
        public List<CPurchaseAdviceFeesFields> sPurchaseAdviceFees_Purchasing
        {
            get
            {
                string data = sPurchaseAdviceFeesXmlContent_Purchasing;
                if (string.IsNullOrEmpty(data))
                {
                    return new List<CPurchaseAdviceFeesFields>();
                }

                using (System.IO.TextReader textReader = new System.IO.StringReader(data))
                {
                    return (List<CPurchaseAdviceFeesFields>)x_PurchaseAdviceFeesSerializer_Purchasing.Deserialize(textReader);
                }
            }
            set
            {
                SetPurchaseAdviceFeesSummaryData_Purchasing(value);

                using (var writer = new System.IO.StringWriter())
                {
                    x_PurchaseAdviceFeesSerializer_Purchasing.Serialize(writer, value);
                    sPurchaseAdviceFeesXmlContent_Purchasing = writer.ToString();
                }
            }
        }

        [DependsOn(nameof(sPurchaseAdviceFeesTotal_Purchasing))]
        private void SetPurchaseAdviceFeesSummaryData_Purchasing(List<CPurchaseAdviceFeesFields> feesFields)
        {
            m_arePurchaseAdviceFieldsSet_Purchasing = true;

            foreach (CPurchaseAdviceFeesFields field in feesFields)
            {
                sPurchaseAdviceFeesTotal_Purchasing += field.FeeAmt;
            }
        }

        [DependsOn]
        [DevNote("OPM 184557 - 1st Payment Due Investor date for Purchasing Advice - Purchasing page")]
        public CDateTime sInvSchedDueD1_Purchasing
        {
            get { return GetDateTimeField("sInvSchedDueD1_Purchasing"); }
            set { SetDateTimeField("sInvSchedDueD1_Purchasing", value); }
        }

        public string sInvSchedDueD1_Purchasing_rep
        {
            get { return GetDateTimeField_rep("sInvSchedDueD1_Purchasing"); }
            set { SetDateTimeField_rep("sInvSchedDueD1_Purchasing", value); }
        }

        [DependsOn]
        [DevNote("OPM 184557 - Investor Name Lock for Purchasing Advice - Purchasing page")]
        public bool sPurchaseAdviceSummaryInvNm_PurchasingLckd
        {
            get { return GetBoolField("sPurchaseAdviceSummaryInvNm_PurchasingLckd"); }
            set { SetBoolField("sPurchaseAdviceSummaryInvNm_PurchasingLckd", value); }
        }

        [DependsOn(nameof(sPurchaseAdviceSummaryInvNm_Purchasing), nameof(sPurchaseAdviceSummaryInvNm_PurchasingLckd), nameof(Branch))]
        [DevNote("OPM 184557 - Investor Name for Purchasing Advice - Purchasing page")]
        public string sPurchaseAdviceSummaryInvNm_Purchasing
        {
            get
            {
                if (sPurchaseAdviceSummaryInvNm_PurchasingLckd)
                {
                    return GetStringVarCharField("sPurchaseAdviceSummaryInvNm_Purchasing");
                }

                return Branch.DisplayNm;
            }
            set { SetStringVarCharField("sPurchaseAdviceSummaryInvNm_Purchasing", value); }
        }

        [DependsOn]
        [DevNote("OPM 184557 - Investor Program Name Lock for Purchasing Advice - Purchasing page")]
        public bool sPurchaseAdviceSummaryInvPgNm_PurchasingLckd
        {
            get { return GetBoolField("sPurchaseAdviceSummaryInvPgNm_PurchasingLckd"); }
            set { SetBoolField("sPurchaseAdviceSummaryInvPgNm_PurchasingLckd", value); }
        }

        [DependsOn(nameof(sPurchaseAdviceSummaryInvPgNm_Purchasing), nameof(sPurchaseAdviceSummaryInvPgNm_PurchasingLckd), nameof(sLpTemplateNm))]
        [DevNote("OPM 184557 - Investor Program Name for Purchasing Advice - Purchasing page")]
        public string sPurchaseAdviceSummaryInvPgNm_Purchasing
        {
            get
            {
                if (sPurchaseAdviceSummaryInvPgNm_PurchasingLckd)
                {
                    return GetStringVarCharField("sPurchaseAdviceSummaryInvPgNm_Purchasing");
                }

                return sLpTemplateNm;
            }
            set { SetStringVarCharField("sPurchaseAdviceSummaryInvPgNm_Purchasing", value); }
        }

        [DependsOn]
        [DevNote("OPM 184557 - Investor Loan Number Lock for Purchasing Advice - Purchasing page")]
        public bool sPurchaseAdviceSummaryInvLoanNm_PurchasingLckd
        {
            get { return GetBoolField("sPurchaseAdviceSummaryInvLoanNm_PurchasingLckd"); }
            set { SetBoolField("sPurchaseAdviceSummaryInvLoanNm_PurchasingLckd", value); }
        }

        [DependsOn(nameof(sPurchaseAdviceSummaryInvLoanNm_PurchasingLckd), nameof(sPurchaseAdviceSummaryInvLoanNm_Purchasing), nameof(sLNm))]
        [DevNote("OPM 184557 - Investor Loan Number for Purchasing Advice - Purchasing page")]
        public string sPurchaseAdviceSummaryInvLoanNm_Purchasing
        {
            get
            {
                if (sPurchaseAdviceSummaryInvLoanNm_PurchasingLckd)
                {
                    return GetStringVarCharField("sPurchaseAdviceSummaryInvLoanNm_Purchasing");
                }

                return sLNm;
            }
            set { SetStringVarCharField("sPurchaseAdviceSummaryInvLoanNm_Purchasing", value); }
        }

        [DependsOn(nameof(sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing), nameof(sPurchaseAdviceBasePrice_Field1_Purchasing), nameof(sBrokerLockBaseBrokComp1PcPrice),
            nameof(sPurchaseAdviceBasePrice_Field2_Purchasing), nameof(sPurchaseAdviceBasePrice_Field3_Purchasing), nameof(sPurchaseAdviceUnpaidPBal_Purchasing), nameof(sPurchaseAdviceEscrowTotCollAtClosing_Purchasing),
            nameof(sInitialDeposit_Escrow), nameof(CopyAdjustmentsFromInvestorRateLockTable_Purchasing))]
        public void ApplyLoanDataToPurchaseAdvice_PurchasingPage()
        {
            sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing = E_AmountPriceCalcMode.Price;
            //sPurchaseAdviceAmortCurtail = -1 * SumMoney(sServicingCollectedFunds_Principal, sServicingDisbursedFunds_Principal, sServicingDueFunds_Principal);
            sPurchaseAdviceBasePrice_Field1_Purchasing = ToRate(sBrokerLockBaseBrokComp1PcPrice);
            sPurchaseAdviceBasePrice_Field2_Purchasing = (sPurchaseAdviceBasePrice_Field3_Purchasing / 100) * sPurchaseAdviceUnpaidPBal_Purchasing;

            sPurchaseAdviceEscrowTotCollAtClosing_Purchasing = -1 * sInitialDeposit_Escrow;
            //sPurchaseAdviceEscrowDepDue_Purchasing = (sServicingCollectedFunds_Escrow + sServicingDueFunds_Escrow) * -1;  // OPM 48129 
            //sPurchaseAdviceEscrowDisbursements_Purchasing = -1 * sServicingDisbursedFunds_Escrow;
            //sInvSchedDueD1_Purchasing = new CDateTime(sServicingNextPmtDue, m_convertLos);
            //sPurchaseAdviceSummaryInvNm_Purchasing = sInvestorLockLpInvestorNm;
            //sPurchaseAdviceSummaryInvPgNm_Purchasing = sInvestorLockLpTemplateNm;
            //sPurchaseAdviceSummaryInvLoanNm_Purchasing = sInvestorLockLoanNum;
            CopyAdjustmentsFromInvestorRateLockTable_Purchasing();
        }

        [DependsOn(nameof(sBrokerLockAdjustments), nameof(sPurchaseAdviceAdjustments_Purchasing), nameof(sFinalLAmt))]
        private void CopyAdjustmentsFromInvestorRateLockTable_Purchasing()
        {
            int cCount = 0;
            List<CPurchaseAdviceAdjustmentsFields> purchaseAdjustments = new List<CPurchaseAdviceAdjustmentsFields>();

            foreach (PricingAdjustment adjustment in sBrokerLockAdjustments)
            {
                CPurchaseAdviceAdjustmentsFields field = new CPurchaseAdviceAdjustmentsFields();
                field.RowNum = cCount++;
                field.AdjDesc = adjustment.Description;
                field.AdjPc_rep = adjustment.Price;
                field.AdjAmt_rep = ToMoneyString((field.AdjPc / 100) * sFinalLAmt);
                purchaseAdjustments.Add(field);
            }

            sPurchaseAdviceAdjustments_Purchasing = purchaseAdjustments;
        }
        #endregion

        #region Purchasing Disbursement Fields (OPM 184557)
        [DependsOn]
        [DevNote("OPM 184557 - Purchasing Disbursement Date")]
        public CDateTime sPurchasingDisbursementD
        {
            get { return GetDateTimeField("sPurchasingDisbursementD"); }
            set { SetDateTimeField("sPurchasingDisbursementD", value); }
        }

        public string sPurchasingDisbursementD_rep
        {
            get { return GetDateTimeField_rep("sPurchasingDisbursementD"); }
            set { SetDateTimeField_rep("sPurchasingDisbursementD", value); }
        }

        [DependsOn]
        [DevNote("OPM 184557 - Purchasing Disbursement Funds To")]
        public Sensitive<string> sPurchasingDisbursementFundsTo
        {
            get { return GetLongTextField("sPurchasingDisbursementFundsTo"); }
            set { SetLongTextField("sPurchasingDisbursementFundsTo", value.Value); }
        }

        [DependsOn]
        [DevNote("OPM 184557 - Purchasing Disbursement Notes")]
        public string sPurchasingDisbursementNotes
        {
            get { return GetLongTextField("sPurchasingDisbursementNotes"); }
            set { SetLongTextField("sPurchasingDisbursementNotes", value); }
        }

        [DependsOn]
        [DevNote("OPM 184557 - Purchasing Disbursement Agent Bank Name")]
        public string sPurchasingDisbursementBankName
        {
            get { return GetStringVarCharField("sPurchasingDisbursementBankName"); }
            set { SetStringVarCharField("sPurchasingDisbursementBankName", value); }
        }

        [DependsOn]
        [DevNote("OPM 184557 - Purchasing Disbursement Agent Bank City/State")]
        public string sPurchasingDisbursementBankCityState
        {
            get { return GetStringVarCharField("sPurchasingDisbursementBankCityState"); }
            set { SetStringVarCharField("sPurchasingDisbursementBankCityState", value); }
        }

        [DependsOn]
        [DevNote("OPM 184557 - Purchasing Disbursement Agent ABA Number")]
        public Sensitive<string> sPurchasingDisbursementABANumber
        {
            get { return GetStringVarCharField("sPurchasingDisbursementABANumber"); }
            set { SetStringVarCharField("sPurchasingDisbursementABANumber", value.Value); }
        }

        [DependsOn]
        [DevNote("OPM 184557 - Purchasing Disbursement Agent Account Name")]
        public string sPurchasingDisbursementAccountName
        {
            get { return GetStringVarCharField("sPurchasingDisbursementAccountName"); }
            set { SetStringVarCharField("sPurchasingDisbursementAccountName", value); }
        }

        [DependsOn]
        [DevNote("OPM 184557 - Purchasing Disbursement Agent Account Number")]
        public Sensitive<string> sPurchasingDisbursementAccountNumber
        {
            get { return GetStringVarCharField("sPurchasingDisbursementAccountNumber"); }
            set { SetStringVarCharField("sPurchasingDisbursementAccountNumber", value.Value); }
        }

        [DependsOn]
        [DevNote("OPM 184557 - Purchasing Disbursement Agent Further Credit To Account Name")]
        public string sPurchasingDisbursementFurtherCreditToAccountName
        {
            get { return GetStringVarCharField("sPurchasingDisbursementFurtherCreditToAccountName"); }
            set { SetStringVarCharField("sPurchasingDisbursementFurtherCreditToAccountName", value); }
        }

        [DependsOn]
        [DevNote("OPM 184557 - Purchasing Disbursement Agent Further Credit To Account Number")]
        public Sensitive<string> sPurchasingDisbursementFurtherCreditToAccountNumber
        {
            get { return GetStringVarCharField("sPurchasingDisbursementFurtherCreditToAccountNumber"); }
            set { SetStringVarCharField("sPurchasingDisbursementFurtherCreditToAccountNumber", value.Value); }
        }

        [DependsOn]
        [DevNote("OPM 184557 - Purchasing Disbursement Additional Instructions Line 1")]
        public Sensitive<string> sPurchasingDisbursementAdditionalInstructionsLine1
        {
            get { return GetStringVarCharField("sPurchasingDisbursementAdditionalInstructionsLine1"); }
            set { SetStringVarCharField("sPurchasingDisbursementAdditionalInstructionsLine1", value.Value); }
        }

        [DependsOn]
        [DevNote("OPM 184557 - Purchasing Disbursement Additional Instructions Line 2")]
        public Sensitive<string> sPurchasingDisbursementAdditionalInstructionsLine2
        {
            get { return GetStringVarCharField("sPurchasingDisbursementAdditionalInstructionsLine2"); }
            set { SetStringVarCharField("sPurchasingDisbursementAdditionalInstructionsLine2", value.Value); }
        }
        #endregion

        #region Adjustment Fields
        private decimal m_sPurchaseAdviceAdjustmentsSRPTotal = 0;
        private decimal m_sPurchaseAdviceAdjustmentsNonSRPTotal = 0;
        private decimal m_sPurchaseAdviceAdjustmentsSRPTotalAmt = 0;
        private decimal m_sPurchaseAdviceAdjustmentsNonSRPTotalAmt = 0;
        private bool m_arePurchaseAdviceAdjFieldsSet = false;

        [DependsOn(nameof(sPurchaseAdviceAdjustments), nameof(SetPurchaseAdviceAdjustmentSummaryData))]
        private decimal sPurchaseAdviceAdjustmentsSRPTotal
        {
            get 
            {
                if (m_arePurchaseAdviceAdjFieldsSet == false)
                {
                    SetPurchaseAdviceAdjustmentSummaryData(sPurchaseAdviceAdjustments);
                }
                return m_sPurchaseAdviceAdjustmentsSRPTotal;
            }
        }
        public string sPurchaseAdviceAdjustmentsSRPTotal_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceAdjustmentsSRPTotal); }
        }

        [DependsOn(nameof(sPurchaseAdviceAdjustments), nameof(SetPurchaseAdviceAdjustmentSummaryData))]
        private decimal sPurchaseAdviceAdjustmentsSRPTotalAmt
        {
            get
            {
                if (m_arePurchaseAdviceAdjFieldsSet == false)
                {
                    SetPurchaseAdviceAdjustmentSummaryData(sPurchaseAdviceAdjustments);
                }
                return m_sPurchaseAdviceAdjustmentsSRPTotalAmt;
            }
        }
        public string sPurchaseAdviceAdjustmentsSRPTotalAmt_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceAdjustmentsSRPTotalAmt); }
        }

        [DependsOn(nameof(sPurchaseAdviceAdjustments), nameof(SetPurchaseAdviceAdjustmentSummaryData))]
        private decimal sPurchaseAdviceAdjustmentsNonSRPTotal
        {
            get
            {
                if (m_arePurchaseAdviceAdjFieldsSet == false)
                {
                    SetPurchaseAdviceAdjustmentSummaryData(sPurchaseAdviceAdjustments);
                }
                return m_sPurchaseAdviceAdjustmentsNonSRPTotal;
            }
        }
        public string sPurchaseAdviceAdjustmentsNonSRPTotal_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceAdjustmentsNonSRPTotal); }
        }

        [DependsOn(nameof(sPurchaseAdviceAdjustments), nameof(SetPurchaseAdviceAdjustmentSummaryData))]
        private decimal sPurchaseAdviceAdjustmentsNonSRPTotalAmt
        {
            get
            {
                if (m_arePurchaseAdviceAdjFieldsSet == false)
                {
                    SetPurchaseAdviceAdjustmentSummaryData(sPurchaseAdviceAdjustments);
                }
                return m_sPurchaseAdviceAdjustmentsNonSRPTotalAmt;
            }
        }
        public string sPurchaseAdviceAdjustmentsNonSRPTotalAmt_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceAdjustmentsNonSRPTotalAmt); }
        }

        [DependsOn(nameof(sPurchaseAdviceAdjustments), nameof(SetPurchaseAdviceAdjustmentSummaryData))]
        private decimal sPurchaseAdviceAdjustmentsTotal
        {
            get
            {
                if (m_arePurchaseAdviceAdjFieldsSet == false)
                {
                    SetPurchaseAdviceAdjustmentSummaryData(sPurchaseAdviceAdjustments);
                }
                return m_sPurchaseAdviceAdjustmentsSRPTotal + m_sPurchaseAdviceAdjustmentsNonSRPTotal;
            }
        }
        public string sPurchaseAdviceAdjustmentsTotal_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceAdjustmentsTotal); }
        }

        [DependsOn(nameof(sPurchaseAdviceUnpaidPBal), nameof(sPurchaseAdviceBasePrice_AmountPriceCalcMode))]
        private void SetPurchaseAdviceAdjustmentSummaryData(List<CPurchaseAdviceAdjustmentsFields> adjustmentFields)
        {
            m_arePurchaseAdviceAdjFieldsSet = true;

            foreach (CPurchaseAdviceAdjustmentsFields field in adjustmentFields)
            {
                if (sPurchaseAdviceBasePrice_AmountPriceCalcMode == E_AmountPriceCalcMode.Amount)
                {
                    field.AdjPc_rep = ToRateString6DecimalDigits(() => (field.AdjAmt * 100) / sPurchaseAdviceUnpaidPBal);
                }
                else                  {
                    field.AdjAmt_rep = ToMoneyString((field.AdjPc / 100) * sPurchaseAdviceUnpaidPBal);
                }
                
                if (field.IsSRP)
                {
                    m_sPurchaseAdviceAdjustmentsSRPTotalAmt += field.AdjAmt;
                    m_sPurchaseAdviceAdjustmentsSRPTotal += field.AdjPc;
                }
                else
                {
                    m_sPurchaseAdviceAdjustmentsNonSRPTotalAmt += field.AdjAmt;
                    m_sPurchaseAdviceAdjustmentsNonSRPTotal += field.AdjPc;
                }
            }
        }

        /// <summary>
        /// List of fees on the purchase advice page.
        /// </summary>
        [DependsOn]
        public string sPurchaseAdviceAdjustmentsXmlContent
        {
            get { return GetLongTextField("sPurchaseAdviceAdjustmentsXmlContent"); }
            set { SetLongTextField("sPurchaseAdviceAdjustmentsXmlContent", value); }
        }

        /// <summary>
        /// Used by the sPurchaseAdviceAdjustments property. XmlSerializer is thread safe.
        /// </summary>
        private static System.Xml.Serialization.XmlSerializer x_PurchaseAdviceAdjustmentsSerializer = new System.Xml.Serialization.XmlSerializer(typeof(List<CPurchaseAdviceAdjustmentsFields>));
        [DependsOn(nameof(sPurchaseAdviceAdjustmentsXmlContent), nameof(SetPurchaseAdviceAdjustmentSummaryData))]
        public List<CPurchaseAdviceAdjustmentsFields> sPurchaseAdviceAdjustments
        {
            get
            {
                string data = sPurchaseAdviceAdjustmentsXmlContent;
                if (string.IsNullOrEmpty(data))
                {
                    return new List<CPurchaseAdviceAdjustmentsFields>();
                }

                using (System.IO.TextReader textReader = new System.IO.StringReader(data))
                {
                    return (List<CPurchaseAdviceAdjustmentsFields>)x_PurchaseAdviceAdjustmentsSerializer.Deserialize(textReader);
                }
            }
            set
            {
                SetPurchaseAdviceAdjustmentSummaryData(value);

                using (var writer = new System.IO.StringWriter())
                {
                    x_PurchaseAdviceAdjustmentsSerializer.Serialize(writer, value);
                    sPurchaseAdviceAdjustmentsXmlContent = writer.ToString();
                }
            }
        }

        #endregion

        #region Fees fields
        
        private decimal m_sPurchaseAdviceFeesTotal = 0;

        [DependsOn(nameof(sPurchaseAdviceFees), nameof(SetPurchaseAdviceFeesSummaryData))]
        private decimal sPurchaseAdviceFeesTotal
        {
            get
            {
                if (m_arePurchaseAdviceFieldsSet == false)
                {
                    SetPurchaseAdviceFeesSummaryData(sPurchaseAdviceFees);
                }
                return m_sPurchaseAdviceFeesTotal;
            }
            set { m_sPurchaseAdviceFeesTotal = value; }
        }
        public string sPurchaseAdviceFeesTotal_rep
        {
            get { return ToMoneyString(() => sPurchaseAdviceFeesTotal); }
        }

        /// <summary>
        /// List of fees on the purchase advice page.
        /// </summary>
        [DependsOn]
        public string sPurchaseAdviceFeesXmlContent
        {
            get { return GetLongTextField("sPurchaseAdviceFeesXmlContent"); }
            set { SetLongTextField("sPurchaseAdviceFeesXmlContent", value); }
        }

        private bool m_arePurchaseAdviceFieldsSet = false;

        /// <summary>
        /// Used by the sPurchaseAdviceFees property. XmlSerializer is thread safe.
        /// </summary>
        private static System.Xml.Serialization.XmlSerializer x_PurchaseAdviceFeesSerializer = new System.Xml.Serialization.XmlSerializer(typeof(List<CPurchaseAdviceFeesFields>));
        [DependsOn(nameof(sPurchaseAdviceFeesXmlContent), nameof(SetPurchaseAdviceFeesSummaryData))]
        public List<CPurchaseAdviceFeesFields> sPurchaseAdviceFees
        {
            get
            {
                string data = sPurchaseAdviceFeesXmlContent;
                if (string.IsNullOrEmpty(data))
                {
                    return new List<CPurchaseAdviceFeesFields>();
                }

                using (System.IO.TextReader textReader = new System.IO.StringReader(data))
                {
                    return (List<CPurchaseAdviceFeesFields>)x_PurchaseAdviceFeesSerializer.Deserialize(textReader);
                }
            }
            set
            {
                SetPurchaseAdviceFeesSummaryData(value);

                using (var writer = new System.IO.StringWriter())
                {
                    x_PurchaseAdviceFeesSerializer.Serialize(writer, value);
                    sPurchaseAdviceFeesXmlContent = writer.ToString();
                }
            }
        }

        [DependsOn(nameof(sPurchaseAdviceFeesTotal))]
        private void SetPurchaseAdviceFeesSummaryData(List<CPurchaseAdviceFeesFields> feesFields)
        {
            m_arePurchaseAdviceFieldsSet = true;

            foreach (CPurchaseAdviceFeesFields field in feesFields)
            {
                sPurchaseAdviceFeesTotal += field.FeeAmt;
            }
        }
        #endregion

        #endregion

        #region Disbursement Fields
        [DependsOn(nameof(sAmtFundFromWarehouseLine))]
        public decimal sDisbursementAmtFundFromWarehouseLine
        {
            get
            {
                return -1 * sAmtFundFromWarehouseLine;
            }
        }

        public string sDisbursementAmtFundFromWarehouseLine_rep
        {
            get { return ToMoneyString(() => sDisbursementAmtFundFromWarehouseLine); }
        }

        [DependsOn]
        private decimal sDisbursementWarehouseLineFees
        {
            get { return GetMoneyField("sDisbursementWarehouseLineFees"); }
            set { SetMoneyField("sDisbursementWarehouseLineFees", value); }
        }
        public string sDisbursementWarehouseLineFees_rep
        {
            get { return ToMoneyString(() => sDisbursementWarehouseLineFees); }
            set { sDisbursementWarehouseLineFees = ToMoney(value); }
        }

        [DependsOn]
        private decimal sDisbursementWarehousePerDiemInterest
        {
            get { return GetMoneyField("sDisbursementWarehousePerDiemInterest", 4); }
            set { SetMoneyField("sDisbursementWarehousePerDiemInterest", value, 4); }
        }
        public string sDisbursementWarehousePerDiemInterest_rep
        {
            get { return ToMoneyString4DecimalDigits(() => sDisbursementWarehousePerDiemInterest); }
            set { sDisbursementWarehousePerDiemInterest = ToMoney(value); }
        }

        [DependsOn]
        public bool sDisbursementDaysOfInterestLckd
        {
            get { return GetBoolField("sDisbursementDaysOfInterestLckd"); }
            set { SetBoolField("sDisbursementDaysOfInterestLckd", value); }
        }

        [DependsOn(nameof(sDisbursementDaysOfInterest), nameof(sDisbursementDaysOfInterestLckd), nameof(sFundD), nameof(sDisbursementD))]
        private int sDisbursementDaysOfInterest
        {
            get
            {
                if (sDisbursementDaysOfInterestLckd)
                {
                    return GetCountField("sDisbursementDaysOfInterest");
                }
                
                // Otherwise, this is the difference between Disbursement date and Fund date
                if (!sFundD.IsValid || !sDisbursementD.IsValid)
                {
                    return 0;
                }

                TimeSpan diff = sDisbursementD.DateTimeForComputation - sFundD.DateTimeForComputation;
                return (int)diff.TotalDays;
            }
            set { SetCountField("sDisbursementDaysOfInterest", value); }
        }
        public string sDisbursementDaysOfInterest_rep
        {
            get { return ToCountString(sDisbursementDaysOfInterest); }
            set { sDisbursementDaysOfInterest = ToCount(value); }
        }

        [DependsOn]
        private decimal sDisbursementOther
        {
            get { return GetMoneyField("sDisbursementOther"); }
            set { SetMoneyField("sDisbursementOther", value); }
        }
        public string sDisbursementOther_rep
        {
            get { return ToMoneyString(() => sDisbursementOther); }
            set { sDisbursementOther = ToMoney(value); }
        }

        [DependsOn]
        public bool sDisbursementWarehouseLineIntLckd
        {
            get { return GetBoolField("sDisbursementWarehouseLineIntLckd"); }
            set { SetBoolField("sDisbursementWarehouseLineIntLckd", value); }
        }

        [DependsOn(nameof(sDisbursementWarehouseLineInt), nameof(sDisbursementWarehouseLineIntLckd), nameof(sDisbursementDaysOfInterest), nameof(sDisbursementWarehousePerDiemInterest))]
        private decimal sDisbursementWarehouseLineInt
        {
            get
            {
                if (sDisbursementWarehouseLineIntLckd)
                {
                    return GetMoneyField("sDisbursementWarehouseLineInt");
                }

                return sDisbursementWarehousePerDiemInterest * sDisbursementDaysOfInterest;
            }
            set { SetMoneyField("sDisbursementWarehouseLineInt", value); }
        }
        public string sDisbursementWarehouseLineInt_rep
        {
            get { return ToMoneyString(() => sDisbursementWarehouseLineInt); }
            set { sDisbursementWarehouseLineInt = ToMoney(value); }
        }

        [DependsOn(nameof(sDisbursementWarehouseLineInt), nameof(sDisbursementAmtFundFromWarehouseLine), nameof(sDisbursementWarehouseLineFees), nameof(sDisbursementOther), nameof(sPurchaseAdviceSummaryTotalDueSeller))]
        public decimal sDisbursementNetReceived
        {
            get
            {
                return SumMoney(sDisbursementAmtFundFromWarehouseLine, sDisbursementWarehouseLineFees, sDisbursementOther, sDisbursementWarehouseLineInt, sPurchaseAdviceSummaryTotalDueSeller);
            }
        }
        public string sDisbursementNetReceived_rep
        {
            get  { return ToMoneyString(() => sDisbursementNetReceived); }
        }

        [DependsOn]
        public string sDisbursementNotes
        {
            get { return GetLongTextField("sDisbursementNotes"); }
            set { SetLongTextField("sDisbursementNotes", value); }
        }

        #endregion

        #region Secondary Status Fields

        [DependsOn]
        public bool sSecondStatusLckd
        {
            get { return GetBoolField("sSecondStatusLckd"); }
            set { SetBoolField("sSecondStatusLckd", value); }
        }
        [DependsOn(nameof(sSecondStatusT), nameof(sSecondStatusLckd), nameof(CalcLoan2ndStatusBasedOnEventDates))]
        public E_sSecondStatusT sSecondStatusT
        {
            get
            {
                if (sSecondStatusLckd)
                    return (E_sSecondStatusT)GetTypeIndexField("sSecondStatusT");

                return CalcLoan2ndStatusBasedOnEventDates();
            }
            set { SetTypeIndexField("sSecondStatusT", (int)value); }
        }

        [DependsOn]
        public CDateTime sDisbursementD
        {
            get { return GetDateTimeField("sDisbursementD"); }
            set { SetDateTimeField("sDisbursementD", value); }
        }

        public string sDisbursementD_rep
        {
            get { return GetDateTimeField_rep("sDisbursementD"); }
            set { SetDateTimeField_rep("sDisbursementD", value); }
        }

        [DependsOn]
        public CDateTime sReconciledD
        {
            get { return GetDateTimeField("sReconciledD"); }
            set { SetDateTimeField("sReconciledD", value); }
        }

        public string sReconciledD_rep
        {
            get { return GetDateTimeField_rep("sReconciledD"); }
            set { SetDateTimeField_rep("sReconciledD", value); }
        }
        
        [DependsOn(nameof(sFundD), nameof(sLPurchaseD), nameof(sDisbursementD), nameof(sReconciledD))]
        private E_sSecondStatusT CalcLoan2ndStatusBasedOnEventDates()
        {
            DateTime latestTime = DateTime.MinValue;
            E_sSecondStatusT latestStatus = E_sSecondStatusT.None;

            // This section must be at the end of this section
            // because the previous assigned data will be needed here.

            DateTime d;

            //Funded
            d = sFundD.GetSafeDateTimeForComputation(DateTime.MinValue);
            if (d != DateTime.MinValue && d.CompareTo(latestTime) >= 0)
            {
                latestTime = d;
                latestStatus = E_sSecondStatusT.Loan_Funded;
            }

            //Purchased
            d = sLPurchaseD.GetSafeDateTimeForComputation(DateTime.MinValue);
            if (d != DateTime.MinValue && d.CompareTo(latestTime) >= 0)
            {
                latestTime = d;
                latestStatus = E_sSecondStatusT.Loan_Purchased;
            }

            //Disbursement
            d = sDisbursementD.GetSafeDateTimeForComputation(DateTime.MinValue);
            if (d != DateTime.MinValue && d.CompareTo(latestTime) >= 0)
            {
                latestTime = d;
                latestStatus = E_sSecondStatusT.Loan_Disbursed;
            }

            //Reconciled
            d = sReconciledD.GetSafeDateTimeForComputation(DateTime.MinValue);
            if (d != DateTime.MinValue && d.CompareTo(latestTime) >= 0)
            {
                latestTime = d;
                latestStatus = E_sSecondStatusT.Loan_Reconciled;
            }

            return latestStatus;
        }

        private static Dictionary<E_sSecondStatusT, string> s_sSecondStatusT_Map = new Dictionary<E_sSecondStatusT, string>()
        {
            { E_sSecondStatusT.None, "None"},
            { E_sSecondStatusT.Loan_Funded, "Loan Funded"},
            { E_sSecondStatusT.Loan_Purchased, "Loan Sold"},
            { E_sSecondStatusT.Loan_Disbursed, "Investor Funds Disbursed"},
            { E_sSecondStatusT.Loan_Reconciled, "Loan Reconciled"}
        };

        public static string sSecondStatusT_map_rep(E_sSecondStatusT sSecondStatusT)
        {
            return s_sSecondStatusT_Map[sSecondStatusT];
        }

        public string sSecondStatusT_rep
        {
            get
            {
                return sSecondStatusT_map_rep(sSecondStatusT);
            }
            set
            {
                sSecondStatusT = E_sSecondStatusT.None;

                string v = value.ToLower();

                foreach (var pair in s_sSecondStatusT_Map)
                {
                    if (pair.Value.ToLower() == v)
                    {
                        sSecondStatusT = pair.Key;
                    }
                }
            }
        }

        [DependsOn(nameof(sSecondStatusT), nameof(sOpenedD), nameof(sFundD), nameof(sLPurchaseD), nameof(sDisbursementD), nameof(sReconciledD))]
        public CDateTime sSecondStatusD
        {
            get { return CDateTime.Create(sSecondStatusD_rep, m_convertLos); }
        }
        public string sSecondStatusD_rep
        {
            get
            {
                // This section must be at the end of this section
                // because the previous assigned data will be needed here.
                switch (sSecondStatusT)
                {
                    case E_sSecondStatusT.None: return "";
                    case E_sSecondStatusT.Loan_Funded: return sFundD_rep;
                    case E_sSecondStatusT.Loan_Purchased: return sLPurchaseD_rep;
                    case E_sSecondStatusT.Loan_Disbursed: return sDisbursementD_rep;
                    case E_sSecondStatusT.Loan_Reconciled: return sReconciledD_rep;
                    default:
                        throw new UnhandledEnumException(sSecondStatusT);
                }
            }
        }

        [DependsOn]
        public CDateTime sInvSchedDueD1
        {
            get { return GetDateTimeField("sInvSchedDueD1"); }
            set { SetDateTimeField("sInvSchedDueD1", value); }
        }

        public string sInvSchedDueD1_rep
        {
            get { return GetDateTimeField_rep("sInvSchedDueD1"); }
            set { SetDateTimeField_rep("sInvSchedDueD1", value); }
        }

        #endregion

        #region Commissions

        [DependsOn(nameof(sProfitGrossBor), nameof(sProfitRebate))]
        public decimal sGrossProfit
        {
            get { return sProfitGrossBor + sProfitRebate; }
        }
        public string sGrossProfit_rep
        {
            get { return ToMoneyString(() => sGrossProfit); }
        }

        /// <summary>
        /// Commission total from all agents
        /// </summary>

        [DependsOn(nameof(sfGetAgentsOfRole))]
        public CAgentCollection sAgentCollection
        {
            get { return new CAgentCollection(this); }
        }

        [DependsOn(nameof(sAgentCollection))]
        public decimal sCommissionTotalPointOfLoanAmount
        {
            get
            {
                decimal pnt = 0;
                CAgentCollection agentColl = sAgentCollection;
                int nAgents = agentColl.GetAgentRecordCount();
                for (int i = 0; i < nAgents; ++i)
                {
                    pnt += agentColl.GetAgentFields(i).CommissionPointOfLoanAmount;
                }
                return pnt;
            }
        }
        public string sCommissionTotalPointOfLoanAmount_rep
        {
            get { return ToMoneyString(() => sCommissionTotalPointOfLoanAmount); }
        }

        [DependsOn(nameof(sAgentCollection))]
        public decimal sCommissionTotalPointOfGrossProfit
        {
            get
            {
                decimal pnt = 0;
                CAgentCollection agentColl = sAgentCollection;
                int nAgents = agentColl.GetAgentRecordCount();
                for (int i = 0; i < nAgents; ++i)
                {
                    pnt += agentColl.GetAgentFields(i).CommissionPointOfGrossProfit;
                }
                return pnt;
            }
        }
        public string sCommissionTotalPointOfGrossProfit_rep
        {
            get { return ToMoneyString(() => sCommissionTotalPointOfGrossProfit); }
        }

        [DependsOn(nameof(sGrossProfit), nameof(sCommissionTotal))]
        public decimal sNetProfit
        {
            get
            {
                return sGrossProfit - sCommissionTotal;
            }
        }
        public string sNetProfit_rep
        {
            get { return ToMoneyString(() => sNetProfit); }
        }

        [DependsOn]
        public decimal sProfitGrossBorMb
        {
            get { return GetMoneyField("sProfitGrossBorMb"); }
            set { SetMoneyField("sProfitGrossBorMb", value); }
        }

        public string sProfitGrossBorMb_rep
        {
            get { return ToMoneyString(() => sProfitGrossBorMb); }
            set { sProfitGrossBorMb = ToMoney(value); }
        }

        [DependsOn]
        public decimal sProfitGrossBorPnt
        {
            get { return GetRateField("sProfitGrossBorPnt"); }
            set { SetRateField("sProfitGrossBorPnt", value); }
        }
        public string sProfitGrossBorPnt_rep
        {
            get { return ToRateString(() => sProfitGrossBorPnt); }
            set { sProfitGrossBorPnt = ToRate(value); }
        }

        [DependsOn(nameof(sProfitGrossBorMb), nameof(sProfitGrossBorPnt), nameof(sLAmtCalc))]
        public decimal sProfitGrossBor
        {
            get { return sProfitGrossBorMb + (sProfitGrossBorPnt * sLAmtCalc) / 100; }
        }
        public string sProfitGrossBor_rep
        {
            get { return ToMoneyString(() => sProfitGrossBor); }
        }

        [DependsOn(nameof(sProfitRebatePnt))]
        public decimal sProfitGrossLenderPnt
        {
            get { return -1 * sProfitRebatePnt; }
        }
        public string sProfitGrossLenderPnt_rep
        {
            get { return ToRateString(() => sProfitGrossLenderPnt); }
        }

        [DependsOn(nameof(sProfitRebateMb))]
        public decimal sProfitGrossLenderMb
        {
            get
            {
                return -1 * sProfitRebateMb;
            }
        }

        public string sProfitGrossLenderMb_rep
        {
            get { return ToMoneyString(() => sProfitGrossLenderMb); }
        }


        [DependsOn]
        public bool sIsBrokerCommissionPaid
        {
            get { return GetBoolField("sIsBrokerCommissionPaid"); }
            set { SetBoolField("sIsBrokerCommissionPaid", value); }
        }

        [DependsOn]
        public string sBrokerCommissionNotes
        {
            get { return GetStringVarCharField("sBrokerCommissionNotes"); }
            set { SetStringVarCharField("sBrokerCommissionNotes", value); }
        }

        [DependsOn]
        public E_AgentRoleT sBrokerCommissionAgentT
        {
            get
            {
                return (E_AgentRoleT)GetCountField("sBrokerCommissionAgentT");
            }
            set { SetCountField("sBrokerCommissionAgentT", (int)value); }
        }
        [DependsOn(nameof(sAgentCollection), nameof(sLAmtCalc), nameof(sGrossProfit), nameof(sSettlementBrokerComp), nameof(sSettlementLOrigBrokerCreditF), nameof(sOriginatorCompensationPaymentSourceT), nameof(sIsIncludeOriginatorCompensationToCommission), nameof(sGfeOriginatorCompF))]
        public decimal sCommissionTotal
        {
            get
            {
                decimal commission = 0;
                CAgentCollection agentColl = sAgentCollection;
                int nAgents = agentColl.GetAgentRecordCount();
                for (int i = 0; i < nAgents; ++i)
                {
                    commission += agentColl.GetAgentFields(i).Commission; // uses slamtcalc , sgrossprofit
                }
                //if (sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)
                //{
                //    if (sIsIncludeOriginatorCompensationToCommission)
                //    {
                //        commission += sGfeOriginatorCompF;
                //    }
                //}
                return commission;
            }
        }
        public string sCommissionTotal_rep
        {
            get { return ToMoneyString(() => sCommissionTotal); }
        }

        [DependsOn(nameof(sAgentCollection), nameof(sLAmtCalc), nameof(sGrossProfit))]
        public decimal sCommissionTotalPaid
        {
            get
            {
                decimal commission = 0;
                CAgentCollection agentColl = sAgentCollection;
                int nAgents = agentColl.GetAgentRecordCount();
                for (int i = 0; i < nAgents; ++i)
                {
                    if (agentColl.GetAgentFields(i).IsCommissionPaid)
                        commission += agentColl.GetAgentFields(i).Commission;
                }

                return commission;
            }
        }
        public string sCommissionTotalPaid_rep
        {
            get { return ToMoneyString(() => sCommissionTotalPaid); }
        }
        [DependsOn(nameof(sAgentCollection), nameof(sLAmtCalc), nameof(sGrossProfit))]
        public decimal sCommissionTotalOutstanding
        {
            get
            {
                decimal commission = 0;
                CAgentCollection agentColl = sAgentCollection;
                int nAgents = agentColl.GetAgentRecordCount();
                for (int i = 0; i < nAgents; ++i)
                {
                    if (!agentColl.GetAgentFields(i).IsCommissionPaid)
                        commission += agentColl.GetAgentFields(i).Commission; // uses slamtcalc , sgrossprofit
                }
                return commission;
            }
        }
        public string sCommissionTotalOutstanding_rep
        {
            get { return ToMoneyString(() => sCommissionTotalOutstanding); }
        }
        [DependsOn(nameof(sAgentCollection))]
        public decimal sCommissionTotalMb
        {
            get
            {
                decimal mb = 0;
                CAgentCollection agentColl = sAgentCollection;
                int nAgents = agentColl.GetAgentRecordCount();
                for (int i = 0; i < nAgents; ++i)
                {
                    mb += agentColl.GetAgentFields(i).CommissionMinBase;
                }
                return mb;
            }
        }
        public string sCommissionTotalMb_rep
        {
            get { return ToMoneyString(() => sCommissionTotalMb); }
        }

        [DependsOn(nameof(sSettlementBrokerComp), nameof(sSettlementBrokerCredit))]
        public decimal sBrokerCommissionsTotal
        {
            get { return (sSettlementBrokerComp + sSettlementBrokerCredit); }
        }

        public string sBrokerCommissionsTotal_rep
        {
            get { return ToMoneyString(() => sBrokerCommissionsTotal); }
        }
        #endregion

        #region Accounting
        
        //Funding
        [DependsOn(nameof(sFundD), nameof(sFundReqForShortfall), nameof(sChkDueFromClosingRcvd), nameof(sChkDueFromClosing))]
        private decimal sFundingCash
        {
            get
            {
                if (!string.IsNullOrEmpty(sFundD_rep))
                {
                    return (-sFundReqForShortfall + (sChkDueFromClosingRcvd ? sChkDueFromClosing : 0));
                }
                else
                {
                    return 0;
                }
            }
        }

        public string sFundingCash_rep
        {
            get { return ToMoneyString(() => sFundingCash); }
        }

        [DependsOn(nameof(sAmtFundFromWarehouseLine), nameof(sFundD))]
        private decimal sFundingPayables
        {
            get 
            {
                if (!string.IsNullOrEmpty(sFundD_rep))
                {
                    return (sAmtFundFromWarehouseLine);
                }
                else
                {
                    return 0;
                }
            }
        }

        public string sFundingPayables_rep
        {
            get { return ToMoneyString(() => sFundingPayables); }
        }

        [DependsOn(nameof(sChkDueFromClosingRcvd), nameof(sChkDueFromClosing), nameof(sFundD))]
        private decimal sFundingReceivables
        {
            get 
            {
                if (!string.IsNullOrEmpty(sFundD_rep))
                {
                    return (sChkDueFromClosingRcvd ? 0 : sChkDueFromClosing);
                }
                else
                {
                    return 0;
                }
            }
        }

        public string sFundingReceivables_rep
        {
            get { return ToMoneyString(() => sFundingReceivables); }
        }

        [DependsOn(nameof(sFundD), nameof(sFinalLAmt))]
        private decimal sFundingLHS
        {
            get
            {
                if (!string.IsNullOrEmpty(sFundD_rep))
                {
                    return sFinalLAmt;
                }
                else
                {
                    return 0;
                } 
            }
        }

        public string sFundingLHS_rep
        {
            get { return ToMoneyString(() => sFundingLHS); }
        }

        //Servicing
        [DependsOn(nameof(sServicingCollectedFunds_Principal), nameof(sServicingCollectedFunds_Interest), nameof(sServicingCollectedFunds_Escrow),
            nameof(sServicingCollectedFunds_Other), nameof(sServicingCollectedFunds_LateFees), nameof(sServicingDisbursedFunds_Principal),
            nameof(sServicingDisbursedFunds_Interest), nameof(sServicingDisbursedFunds_Escrow), nameof(sServicingDisbursedFunds_Other),
            nameof(sServicingDisbursedFunds_LateFees))]
        private decimal sServicingCash
        {
            get
            {
                return sServicingCollectedFunds_Principal + sServicingCollectedFunds_Interest + sServicingCollectedFunds_Escrow + sServicingCollectedFunds_Other + sServicingCollectedFunds_LateFees + sServicingDisbursedFunds_Principal + sServicingDisbursedFunds_Interest + sServicingDisbursedFunds_Escrow + sServicingDisbursedFunds_Other + sServicingDisbursedFunds_LateFees;
            }
        }

        public string sServicingCash_rep
        {
            get { return ToMoneyString(() => sServicingCash); }
        }

        [DependsOn(nameof(sServicingDueFunds_Principal), nameof(sServicingDueFunds_Interest), nameof(sServicingDueFunds_Escrow), nameof(sServicingDueFunds_Other), nameof(sServicingDueFunds_LateFees))]
        private decimal sServicingTotalStillDue
        {
            get 
            { 
                return sServicingDueFunds_Principal + sServicingDueFunds_Interest + sServicingDueFunds_Escrow + sServicingDueFunds_Other + sServicingDueFunds_LateFees;
            }
        }

        [DependsOn(nameof(sServicingTotalStillDue))]
        private decimal sServicingPayables
        {
            get { return (sServicingTotalStillDue > 0 ? 0 : Math.Abs(sServicingTotalStillDue)); }
        }

        public string sServicingPayables_rep
        {
            get { return ToMoneyString(() => sServicingPayables); }
        }

        [DependsOn(nameof(sServicingTotalStillDue))]
        private decimal sServicingReceivables
        {
            get { return (sServicingTotalStillDue > 0 ? sServicingTotalStillDue : 0); }
        }

        public string sServicingReceivables_rep
        {
            get { return ToMoneyString(() => sServicingReceivables); }
        }

        [DependsOn(nameof(sServicingCollectedFunds_Principal), nameof(sServicingDisbursedFunds_Principal), nameof(sServicingDueFunds_Principal))]
        private decimal sServicingLHS
        {
            get
            {
                return - (sServicingCollectedFunds_Principal + sServicingDisbursedFunds_Principal  + sServicingDueFunds_Principal);
            }
        }

        public string sServicingLHS_rep
        {
            get { return ToMoneyString(() => sServicingLHS); }
        }

        //Commissions
        [DependsOn(nameof(sCommissionTotalPaid))]
        private decimal sCommissionsCash
        {
            get { return -sCommissionTotalPaid; }
        }

        public string sCommissionsCash_rep
        {
            get { return ToMoneyString(() => sCommissionsCash); }
        }

        [DependsOn(nameof(sCommissionTotalOutstanding))]
        private decimal sCommissionsPayables
        {
            get { return (sCommissionTotalOutstanding); }
        }

        public string sCommissionsPayables_rep
        {
            get { return ToMoneyString(() => sCommissionsPayables); }
        }

        private decimal sCommissionsReceivables
        {
            get { return 0; }
        }

        public string sCommissionsReceivables_rep
        {
            get { return ToMoneyString(() => sCommissionsReceivables); }
        }
        [DependsOn]
        private decimal sCommissionsLHS
        {
            get
            {
                // 3/19/2010 dd - If modify this property and start to add DependsOn attribute, need to go to 
                // SelectStatementProvider.cs class and remove a s_placeHolders.Add() line.

                return 0;
            }
        }

        public string sCommissionsLHS_rep
        {
            get { return ToMoneyString(() => sCommissionsLHS); }
        }

        //Disbursement
        [DependsOn(nameof(sDisbursementD), nameof(sDisbursementNetReceived))]
        private decimal sDisbursementCash
        {
            get 
            {
                if (!string.IsNullOrEmpty(sDisbursementD_rep))
                {
                    return sDisbursementNetReceived;
                }
                else
                {
                    return 0;
                }
            }
        }

        public string sDisbursementCash_rep
        {
            get { return ToMoneyString(() => sDisbursementCash); }
        }

        [DependsOn(nameof(sDisbursementD), nameof(sLPurchaseD), nameof(sDisbursementNetReceived), nameof(sDisbursementAmtFundFromWarehouseLine))]
        private decimal sDisbursementPayables
        {
            get 
            {
                if (string.IsNullOrEmpty(sDisbursementD_rep))
                {
                    if (!string.IsNullOrEmpty(sLPurchaseD_rep) && sDisbursementNetReceived < 0)
                    {                        
                            return Math.Abs(sDisbursementNetReceived);                       
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return sDisbursementAmtFundFromWarehouseLine;
                }
            }
        }

        public string sDisbursementPayables_rep
        {
            get { return ToMoneyString(() => sDisbursementPayables); }
        }

        [DependsOn(nameof(sDisbursementD), nameof(sLPurchaseD), nameof(sDisbursementNetReceived))]
        private decimal sDisbursementReceivables
        {
            get 
            {
                if (string.IsNullOrEmpty(sDisbursementD_rep) && !string.IsNullOrEmpty(sLPurchaseD_rep) && sDisbursementNetReceived > 0)
                {
                    return Math.Abs(sDisbursementNetReceived);
                }
                else
                {
                    return 0;
                }
            }
        }

        public string sDisbursementReceivables_rep
        {
            get { return ToMoneyString(() => sDisbursementReceivables); }
        }

        [DependsOn(nameof(sDisbursementD), nameof(sFundingLHS), nameof(sServicingLHS))]
        private decimal sDisbursementLHS
        {
            get
            {
                if (!string.IsNullOrEmpty(sDisbursementD_rep))
                {
                    return -(sFundingLHS + sServicingLHS);
                }
                else
                {
                    return 0;
                }
            }
        }

        public string sDisbursementLHS_rep
        {
            get { return ToMoneyString(() => sDisbursementLHS); }
        }

        [DependsOn]
        private decimal sTrustAccountLHS
        {
            get
            {
                // 3/19/2010 dd - If modify this property and start to add DependsOn attribute, need to go to 
                // SelectStatementProvider.cs class and remove a s_placeHolders.Add() line.
                return 0;
            }
        }

        public string sTrustAccountLHS_rep
        {
            get { return ToMoneyString(() => sTrustAccountLHS); }
        }

        [DependsOn]
        private decimal sTransactionsLHS
        {
            get
            {
                // 3/19/2010 dd - If modify this property and start to add DependsOn attribute, need to go to 
                // SelectStatementProvider.cs class and remove a s_placeHolders.Add() line.

                return 0;
            }
        }

        public string sTransactionsLHS_rep
        {
            get { return ToMoneyString(() => sTransactionsLHS); }
        }

        [DependsOn(nameof(sTotalCash), nameof(sTotalPayables), nameof(sTotalReceivables), nameof(sTotalLHS))]
        public decimal sTotalGain
        {
            get { return sTotalCash - sTotalPayables + sTotalReceivables + sTotalLHS; }
        }

        public string sTotalGain_rep
        {
            get { return ToMoneyString(() => sTotalGain); }
        }

        [DependsOn(nameof(sFundingCash), nameof(sServicingCash), nameof(sTransactionsNetPmtsToDate), nameof(sCommissionsCash), nameof(sDisbursementCash), nameof(sTrustBalance))]
        public decimal sTotalCash
        {
            get { return sFundingCash + sServicingCash + sTransactionsNetPmtsToDate + sCommissionsCash + sDisbursementCash + sTrustBalance; }
        }

        public string sTotalCash_rep
        {
            get { return ToMoneyString(() => sTotalCash); }
        }

        [DependsOn(nameof(sFundingPayables), nameof(sServicingPayables), nameof(sCommissionsPayables), nameof(sDisbursementPayables), nameof(sTrustPayableTotal), nameof(sTransactionsTotPmtsPayable))]
        public decimal sTotalPayables
        {
            get { return (sFundingPayables + sServicingPayables + sTransactionsTotPmtsPayable + sCommissionsPayables + sDisbursementPayables + sTrustPayableTotal); }
        }

        public string sTotalPayables_rep
        {
            get { return ToMoneyString(() => sTotalPayables); }
        }

        [DependsOn(nameof(sFundingReceivables), nameof(sServicingReceivables), nameof(sTransactionsTotPmtsReceivable), nameof(sDisbursementReceivables), nameof(sTrustReceivableTotal))]
        public decimal sTotalReceivables
        {
            get { return sFundingReceivables + sServicingReceivables + sTransactionsTotPmtsReceivable + sDisbursementReceivables + sTrustReceivableTotal; }
        }

        public string sTotalReceivables_rep
        {
            get { return ToMoneyString(() => sTotalReceivables); }
        }
        [DependsOn(nameof(sFundingLHS), nameof(sServicingLHS), nameof(sTransactionsLHS), nameof(sCommissionsLHS), nameof(sDisbursementLHS), nameof(sTrustAccountLHS))]
        public decimal sTotalLHS
        {
            get { return sFundingLHS + sServicingLHS + sTransactionsLHS + sCommissionsLHS + sDisbursementLHS + sTrustAccountLHS; }
        }

        public string sTotalLHS_rep
        {
            get { return ToMoneyString(() => sTotalLHS); }
        }
        [DependsOn(nameof(sTotalCash), nameof(sTotalPayables), nameof(sTotalReceivables), nameof(sFinalLAmt), nameof(sTotalLHS))]
        public decimal sTotalGainPercent
        {
            get
            {
                if (sFinalLAmt > 0)
                {
                    return 100 * (sTotalCash - sTotalPayables + sTotalReceivables + sTotalLHS) / sFinalLAmt;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sTotalGainPercent_rep
        {
            get { return ToRateString(() => sTotalGainPercent); }
        }
        #endregion

        #region Payoff Statement
        [DependsOn]
        public CDateTime sPayoffStatementPayoffD
        {
            get { return GetDateTimeField("sPayoffStatementPayoffD"); }
            set { SetDateTimeField("sPayoffStatementPayoffD", value); }
        }

        public string sPayoffStatementPayoffD_rep
        {
            get { return GetDateTimeField_rep("sPayoffStatementPayoffD"); }
            set { SetDateTimeField_rep("sPayoffStatementPayoffD", value); }
        }

        [DependsOn]
        public E_sPayoffStatementInterestCalcT sPayoffStatementInterestCalcT
        {
            get { return (E_sPayoffStatementInterestCalcT)GetTypeIndexField("sPayoffStatementInterestCalcT"); }
            set { SetTypeIndexField("sPayoffStatementInterestCalcT", (int)value); }
        }

        [DependsOn(nameof(sPayoffStatementInterestCalcT), nameof(sPayoffStatementPayoffD), nameof(sServicingLastPmtDueD))]
        public int sPayoffStatementInterestWholeMonths
        {
            get
            {
                // Both dates must be valid for date subtraction to work
                if( !sPayoffStatementPayoffD.IsValid || !sServicingLastPmtDueD.IsValid )
                    return 0;

                DateTime startDate = sServicingLastPmtDueD.DateTimeForComputation;
                DateTime endDate = sPayoffStatementPayoffD.DateTimeForComputation;

                int years = endDate.Year - startDate.Year;
                int months = endDate.Month - startDate.Month;
                months += 12 * years;

                if (sPayoffStatementInterestCalcT == E_sPayoffStatementInterestCalcT.PerDiem) {
                    // Round Down (don't include partial month)
                    if( endDate.Day < startDate.Day )
                        return months - 1;
                }
                else { //sPayoffStatementInterestCalcT == E_sPayoffStatementInterestCalcT.WholeMonth
                    // Round Up (include partial month)
                    if (endDate.Day > startDate.Day)
                        return months + 1;
                }

                return months;
            }
        }

        public string sPayoffStatementInterestWholeMonths_rep
        {
            get { return ToCountString(() => sPayoffStatementInterestWholeMonths); }
        }

        [DependsOn(nameof(sPayoffStatementInterestCalcT), nameof(sPayoffStatementPayoffD), nameof(sServicingLastPmtDueD), nameof(sPayoffStatementInterestWholeMonths))]
        public int sPayoffStatementInterestOddDays
        {
            get
            {
                // Both dates must be valid for date subtraction to work
                if (!sPayoffStatementPayoffD.IsValid || !sServicingLastPmtDueD.IsValid)
                    return 0;

                if (sPayoffStatementInterestCalcT == E_sPayoffStatementInterestCalcT.PerDiem) {
                    DateTime startDate = sServicingLastPmtDueD.DateTimeForComputation;
                    DateTime endDate = sPayoffStatementPayoffD.DateTimeForComputation;

                    DateTime lastCompleteMonthD;
                    if (endDate.Day < startDate.Day)
                    {
                        DateTime temp = endDate.AddMonths(-1);
                        lastCompleteMonthD = new DateTime(temp.Year, temp.Month, startDate.Day);
                    }
                    else // endDate.Day >= startDate.Day
                    {
                        lastCompleteMonthD = new DateTime(endDate.Year, endDate.Month, startDate.Day);
                    }

                    return (endDate - lastCompleteMonthD).Days;
                }
                else {
                    return 0;
                }
            }
        }

        public string sPayoffStatementInterestOddDays_rep
        {
            get { return ToCountString(() => sPayoffStatementInterestOddDays); }
        }

        [DependsOn(nameof(sPayoffStatementInterestWholeMonths), nameof(sServicingUnpaidPrincipalBalance), nameof(sNoteIR), nameof(sPayoffStatementInterestOddDays), nameof(sSettlementIPerDay))]
        private decimal sPayoffStatementTotalInterestDueAmt
        {
            get
            {
                return (sPayoffStatementInterestWholeMonths * sServicingUnpaidPrincipalBalance * sNoteIR / 1200M) + (sPayoffStatementInterestOddDays * sSettlementIPerDay);
            }
        }

        public string sPayoffStatementTotalInterestDueAmt_rep
        {
            get { return ToMoneyString(() => sPayoffStatementTotalInterestDueAmt); }
        }

        [DependsOn(nameof(sProMIns), nameof(sPayoffStatementInterestWholeMonths), nameof(sPayoffStatementInterestOddDays))]
        private decimal sPayoffStatementMonthlyMiDueAmt
        {
            get
            {
                return sProMIns * (sPayoffStatementInterestWholeMonths + (sPayoffStatementInterestOddDays > 0 ? 1 : 0));
            }
        }

        public string sPayoffStatementMonthlyMiDueAmt_rep
        {
            get { return ToMoneyString(() => sPayoffStatementMonthlyMiDueAmt); }
        }

        [DependsOn(nameof(sServicingUnpaidPrincipalBalance), nameof(sPayoffStatementTotalInterestDueAmt), nameof(sPayoffStatementMonthlyMiDueAmt), nameof(sServicingTotalDueFunds_Escrow), nameof(sServicingTotalDueFunds_LateFees))]
        private decimal sPayoffStatementTotalDueAmt
        {
            get
            {
                return sServicingUnpaidPrincipalBalance + sPayoffStatementTotalInterestDueAmt + sPayoffStatementMonthlyMiDueAmt
                    - sServicingTotalDueFunds_Escrow + sServicingTotalDueFunds_LateFees;
            }
        }

        public string sPayoffStatementTotalDueAmt_rep
        {
            get { return ToMoneyString(() => sPayoffStatementTotalDueAmt); }
        }

        [DependsOn]
        public E_sPayoffStatementRecipientT sPayoffStatementRecipientT
        {
            get { return (E_sPayoffStatementRecipientT)GetTypeIndexField("sPayoffStatementRecipientT"); }
            set { SetTypeIndexField("sPayoffStatementRecipientT", (int)value); }
        }

        [DependsOn]
        public string sPayoffStatementNotesToRecipient
        {
            get { return GetLongTextField("sPayoffStatementNotesToRecipient"); }
            set { SetLongTextField("sPayoffStatementNotesToRecipient", value); }
        }

        [DependsOn(nameof(sPayoffStatementRecipientAddressee), nameof(sPayoffStatementRecipientT), nameof(sInvestorRolodexId), nameof(CAppBase.aBNm), nameof(CAppBase.aCNm), nameof(sfGetInvestorInfo))]
        public string sPayoffStatementRecipientAddressee
        {
            get
            {
                switch (sPayoffStatementRecipientT)
                {
                    case E_sPayoffStatementRecipientT.BorrowerMailingAddress:
                    case E_sPayoffStatementRecipientT.SubjectPropertyAddress:
                        {
                            var appData = GetAppData(0);
                            return appData.aBNm + " and " + appData.aCNm;
                        }
                    case E_sPayoffStatementRecipientT.InvestorMainAddress:
                    case E_sPayoffStatementRecipientT.InvestorNoteShipToAddress:
                        {
                            var investor = GetInvestorInfo();
                            return investor.CompanyName;
                        }
                    case E_sPayoffStatementRecipientT.Other:
                        {
                            return GetStringVarCharField("sPayoffStatementRecipientAddressee");
                        }
                    default:
                        throw new UnhandledEnumException(sPayoffStatementRecipientT);
                }
            }
            set { SetLongTextField("sPayoffStatementRecipientAddressee", value); }
        }

        [DependsOn(nameof(sPayoffStatementRecipientAttention), nameof(sPayoffStatementRecipientT), nameof(sInvestorRolodexId), nameof(sfGetInvestorInfo))]
        public string sPayoffStatementRecipientAttention
        {
            get
            {
                switch (sPayoffStatementRecipientT)
                {
                    case E_sPayoffStatementRecipientT.BorrowerMailingAddress:
                    case E_sPayoffStatementRecipientT.SubjectPropertyAddress:
                        {
                            return "";
                        }
                    case E_sPayoffStatementRecipientT.InvestorMainAddress:
                        {
                            var investor = GetInvestorInfo();
                            return investor.MainAttention;
                        }
                    case E_sPayoffStatementRecipientT.InvestorNoteShipToAddress:
                        {
                            var investor = GetInvestorInfo();
                            return investor.NoteShipToAttention;
                        }
                    case E_sPayoffStatementRecipientT.Other:
                        {
                            return GetStringVarCharField("sPayoffStatementRecipientAttention");
                        }
                    default:
                        throw new UnhandledEnumException(sPayoffStatementRecipientT);
                }
            }
            set { SetLongTextField("sPayoffStatementRecipientAttention", value); }
        }

        [DependsOn(nameof(sPayoffStatementRecipientStreetAddress), nameof(sPayoffStatementRecipientT), nameof(sInvestorRolodexId), nameof(CAppBase.aBAddrMail), nameof(sSpAddr), nameof(sfGetInvestorInfo))]
        public string sPayoffStatementRecipientStreetAddress
        {
            get
            {
                switch(sPayoffStatementRecipientT)
                {
                    case E_sPayoffStatementRecipientT.BorrowerMailingAddress:
                        {
                            var appData = GetAppData(0);
                            return appData.aBAddrMail;
                        }
                    case E_sPayoffStatementRecipientT.SubjectPropertyAddress:
                        {
                            return sSpAddr;
                        }
                    case E_sPayoffStatementRecipientT.InvestorMainAddress:
                        {
                            var investor = GetInvestorInfo();
                            return investor.MainAddress;
                        }
                    case E_sPayoffStatementRecipientT.InvestorNoteShipToAddress:
                        {
                            var investor = GetInvestorInfo();
                            return investor.NoteShipToAddress;
                        }
                    case E_sPayoffStatementRecipientT.Other:
                        {
                            return GetStringVarCharField("sPayoffStatementRecipientStreetAddress");
                        }
                    default:
                        throw new UnhandledEnumException(sPayoffStatementRecipientT);
                }
            }
            set { SetStringVarCharField("sPayoffStatementRecipientStreetAddress", value); }
        }

        [DependsOn(nameof(sPayoffStatementRecipientCity), nameof(sPayoffStatementRecipientT), nameof(sInvestorRolodexId), nameof(CAppBase.aBCityMail), nameof(sSpCity), nameof(sfGetInvestorInfo))]
        public string sPayoffStatementRecipientCity
        {
            get
            {
                switch (sPayoffStatementRecipientT)
                {
                    case E_sPayoffStatementRecipientT.BorrowerMailingAddress:
                        {
                            var appData = GetAppData(0);
                            return appData.aBCityMail;
                        }
                    case E_sPayoffStatementRecipientT.SubjectPropertyAddress:
                        {
                            return sSpCity;
                        }
                    case E_sPayoffStatementRecipientT.InvestorMainAddress:
                        {
                            var investor = GetInvestorInfo();
                            return investor.MainCity;
                        }
                    case E_sPayoffStatementRecipientT.InvestorNoteShipToAddress:
                        {
                            var investor = GetInvestorInfo();
                            return investor.NoteShipToCity;
                        }
                    case E_sPayoffStatementRecipientT.Other:
                        {
                            return GetStringVarCharField("sPayoffStatementRecipientCity");
                        }
                    default:
                        throw new UnhandledEnumException(sPayoffStatementRecipientT);
                }
            }
            set { SetStringVarCharField("sPayoffStatementRecipientCity", value); }
        }

        [DependsOn(nameof(sPayoffStatementRecipientState), nameof(sPayoffStatementRecipientT), nameof(sInvestorRolodexId), nameof(CAppBase.aBStateMail), nameof(sSpState), nameof(sfGetInvestorInfo))]
        public string sPayoffStatementRecipientState
        {
            get
            {
                switch (sPayoffStatementRecipientT)
                {
                    case E_sPayoffStatementRecipientT.BorrowerMailingAddress:
                        {
                            var appData = GetAppData(0);
                            return appData.aBStateMail;
                        }
                    case E_sPayoffStatementRecipientT.SubjectPropertyAddress:
                        {
                            return sSpState;
                        }
                    case E_sPayoffStatementRecipientT.InvestorMainAddress:
                        {
                            var investor = GetInvestorInfo();
                            return investor.MainState;
                        }
                    case E_sPayoffStatementRecipientT.InvestorNoteShipToAddress:
                        {
                            var investor = GetInvestorInfo();
                            return investor.NoteShipToState;
                        }
                    case E_sPayoffStatementRecipientT.Other:
                        {
                            return GetStringVarCharField("sPayoffStatementRecipientState");
                        }
                    default:
                        throw new UnhandledEnumException(sPayoffStatementRecipientT);
                }
            }
            set { SetStringVarCharField("sPayoffStatementRecipientState", value); }
        }

        [DependsOn(nameof(sPayoffStatementRecipientZip), nameof(sPayoffStatementRecipientT), nameof(sInvestorRolodexId), nameof(CAppBase.aBZipMail), nameof(sSpZip), nameof(sfGetInvestorInfo))]
        public string sPayoffStatementRecipientZip
        {
            get
            {
                switch (sPayoffStatementRecipientT)
                {
                    case E_sPayoffStatementRecipientT.BorrowerMailingAddress:
                        {
                            var appData = GetAppData(0);
                            return appData.aBZipMail;
                        }
                    case E_sPayoffStatementRecipientT.SubjectPropertyAddress:
                        {
                            return sSpZip;
                        }
                    case E_sPayoffStatementRecipientT.InvestorMainAddress:
                        {
                            var investor = GetInvestorInfo();
                            return investor.MainZip;
                        }
                    case E_sPayoffStatementRecipientT.InvestorNoteShipToAddress:
                        {
                            var investor = GetInvestorInfo();
                            return investor.NoteShipToZip;
                        }
                    case E_sPayoffStatementRecipientT.Other:
                        {
                            return GetStringVarCharField("sPayoffStatementRecipientZip");
                        }
                    default:
                        throw new UnhandledEnumException(sPayoffStatementRecipientT);
                }
            }
            set { SetStringVarCharField("sPayoffStatementRecipientZip", value); }
        }
        #endregion
    }
}
