using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using LendersOffice.Constants;
using LendersOffice.ObjLib.FieldInfoCache;
using LendersOffice.ObjLib.FieldInfoCache.Models;

namespace DataAccess
{
    /// <summary>
    /// We expose a simple field info lookup table
    /// with this class as a wrapper.
    /// </summary>
    public class CFieldInfoTable
    {
        // 1/18/2011 dd - This array contains a list of fields that if one of field is include then everything relate fields must be include.
        private static readonly string[][] x_SpecialDependencyFieldList =
        {
            new string[] { "sLT", "sProMInsT", "sGroundRentExpense" },
            new string[] { "sPurchaseAdviceBasePrice_Field1", "sPurchaseAdviceBasePrice_Field2" },
            new string[] {"sProdInvestorRLckdDays", "sProdInvestorRLckdExpiredD", "sProdInvestorRLckdModeT" },
            new string[] { "sProdSpT", "sProdSpStructureT", "sProdCondoStories", "sfTransformPmlPropertyTypeToLendersOffice" },
            new string[] { "sRefPurpose", "sGseRefPurposeT", "sLPurposeT" },
            new string[] { "sFHAPurposeIsStreamlineRefiWithAppr", "sFHAPurposeIsStreamlineRefiWithoutAppr", "sHasAppraisal" },
            new string[] {"sGseSpT", "sUnitsNum", "sFannieSpT", "sSpT", "sfTransformLoPropertyTypeToPml"},
            new string[] { "sProdImpound", "sWillEscrowBeWaived" },
            new string[] { "sProdDocT", "sFannieDocT","sProdAssetT","sProdIT","sLPurposeT" }, //opm 
            new string[] { "sDocMagicPlanCodeId", "sDocMagicPlanCodeNm" },
            new string[] { "sProdIsUsdaRuralHousingFeeFinanced", "sProdUSDAGuaranteeFee", "sfTransformUSDADataToLO" },
            new string[] { "sCustomPMLField1", "sCustomPMLField2", "sCustomPMLField3", "sCustomPMLField4",
                "sCustomPMLField5", "sCustomPMLField6", "sCustomPMLField7", "sCustomPMLField8", "sCustomPMLField9",
                "sCustomPMLField10", "sCustomPMLField11", "sCustomPMLField12", "sCustomPMLField13", "sCustomPMLField14",
                "sCustomPMLField15", "sCustomPMLField16", "sCustomPMLField17", "sCustomPMLField18", "sCustomPMLField19",
                "sCustomPMLField20","sfSetCustomPMLFieldFromRep", "sfGetCustomPMLFieldRep" },
            new string[] { "sLTotI", "aBTotI", "aCTotI", "aOtherIncomeList" },
            new string[] { "sLT", "sLPurposeT" , "sLienPosT", "sfUpdateNLMSCallReportData" }, // OPM 120312
            //new string[] { "sRLckdExpiredD", "sRateLockStatusT", "sfPopulateGFEWithLockDatesFromFrontEndLock" }, // opm 69449, 134973
            //new string[] { "sfPopulate_sGfeEstScAvillTillD_on_sGfeNoteIRAvailTillD_Change", "sGfeNoteIRAvailTillD" }, // opm 69449, 134973
            new string[] { "sStatusT", "sHmdaActionTaken", "sHmdaActionTakenT", "sLoanVersionT", "sHmdaActionD", "sFundD", "sApprovD", "sSuspendedD", "sPreApprovD", "sPreQualD", "sCanceledD", "sRejectD", "sWithdrawnD" }, // 10/17/2013 gf - opm 136405
            new string[] { "aBRetainedNegCf", "aCRetainedNegCf", "aLiaCollection" }, // 04/03/14 mf - OPM 174375
            new string[] { "sPurchaseAdviceBasePrice_Field1", "sPurchaseAdviceBasePrice_Field2"},
            new string[] { "sSubFinPe", "sCreditLineAmt" },
            new string[] { "aProdBCitizenT", "aBDecCitizen", "aBDecResidency", "aBDecForeignNational"},
            new string[] { "aProdCCitizenT", "aCDecCitizen", "aCDecResidency"},
            new string[] { "sPurchaseAdviceAdjustments_Field2", "sPurchaseAdviceAdjustments_Field3"},
            new string[] { "sPurchaseAdviceSRP_Field2", "sPurchaseAdviceSRP_Field3"},
            new string[]{"sHazInsRsrvEscrowedTri","sHazInsRsrvMon"},
            new string[]{"sMInsRsrvEscrowedTri","sMInsRsrvMon"},
            new string[]{"sRealETxRsrvEscrowedTri","sRealETxRsrvMon"},
            new string[]{"sSchoolTxRsrvEscrowedTri","sSchoolTxRsrvMon"},
            new string[]{"sFloodInsRsrvEscrowedTri","sFloodInsRsrvMon"},
            new string[]{"s1006RsrvEscrowedTri","s1006RsrvMon"},
            new string[]{"s1007RsrvEscrowedTri","s1007RsrvMon"},
            new string[]{"sU3RsrvEscrowedTri","sU3RsrvMon"},
            new string[]{"sU4RsrvEscrowedTri","sU4RsrvMon"},
            new string[]{"aBAddrMailUsePresentAddr","aBAddrMailSourceT"},
            new string[]{"aCAddrMailUsePresentAddr","aCAddrMailSourceT"},
            new string[] { "sProdCurrPIPmtLckd", "sProdCurrPIPmtCalcOld", "sProdCurrPIPmt" },
            new string[] { "sPmlBrokerId", "sfUpdatePmlBrokerId", "sfUpdateAffiliateOfficialContactValues", "sfAddOfficialContactForOriginatingCompany", "sfUpdatePmlBrokerIdWithSetterBypass" },
            new string[] { "sCustomPricingPolicySourceEmployeeId", "sCustomPricingPolicySourceOriginatingCompanyId", "sCustomPricingPolicySourceBranchId" },
            new string[] { "sLDiscnt","sLenderActualInitialCreditAmt"},
            new string[] { "sVaAutoUnderwritingT", "sIsDuUw", "sIsLpUw", "sIsOtherUw", "sOtherUwDesc", "sfUpdate_sVaAutoUnderwritingT_BasedOn1008CheckboxStates" }, // circular dependency created in opm 68873
            new string[] { "sFloodHazardBuilding", "sFloodHazardMobileHome", "sFloodCertificationIsInSpecialArea", "sfUpdatesFloodCertificationIsInSpecialArea" }, // circle created in opm 192347
            new string[] { "sStatusT", "sNMLSApplicationAmount", "sFinalLT"},
            new string[] {"sLenderActualInitialCreditAmt", "sLDiscnt", "sClosingCostSet", "sLDiscntPc", "sLDiscntFMb", "sTotLenderRsrv"},
            new string[] {"sAggregateEscrowAccount", "sAggregateAdjRsrv"}, // Circular dependency created in opm 217164
            new string[] { "sfMigrateOnTRID2015" }.Concat(ConstApp.TridAutoMigrationTriggerFieldIds).ToArray(),
            new string[] {"sClosingCostFeeVersionT","sClosingCostSet"},
            new string[] { "aOccT", "aOccTPeval" },
            new string[] { "sFHARatedAcceptedByTotalScorecard", "sFHARatedReferByTotalScorecard" },
            new string[] { "sFHARiskClassAA", "sFHARiskClassRefer" },
            new string[] { "sComplianceEaseChecksumValue", "sComplianceEaseLastStatus" },
            new string[] { "sToleranceTenPercentCure","sPdByBorrowerU6F" },
            new string[] { "sDisclosuresDueD", "sDisclosuresDueD_Old" },
            new string[] { "sPurchaseAdviceBasePrice_Field1_Purchasing", "sPurchaseAdviceBasePrice_Field2_Purchasing" },
            new string[] { "sPurchaseAdviceAdjustments_Field2_Purchasing", "sPurchaseAdviceAdjustments_Field3_Purchasing" },
            new string[] { "sPurchaseAdviceSRP_Field2_Purchasing", "sPurchaseAdviceSRP_Field3_Purchasing" },
            new string[] { "sEmployeeLoanRepId", "sfComputeLpePriceGroupId" },
            new string[] { "sProdIsVaFundingFinanced", "sfTransfromProdVaFieldsToLendersOffice" },
            new string[] { "sLtvROtherFin", "sProOFinBal" },
            new string[] { "sTotalScoreCertificateHtml", "sTotalScoreCertificateXmlContent" },
            new string[] { "sIsRenovationLoan", "sAltCostLckd" },
            new string[] { "aBSsn", "sEncryptionKey", "sEncryptionMigrationVersion" }, // OPM 467079: special dependencies for cache updates
            new string[] { "aBSsnEncrypted", "sEncryptionKey", "sEncryptionMigrationVersion" },
            new string[] { "aCSsn", "sEncryptionKey", "sEncryptionMigrationVersion" },
            new string[] { "aCSsnEncrypted", "sEncryptionKey", "sEncryptionMigrationVersion" }, // End OPM 467079
            new string[] { "sAdjustmentList", "UpdateLinkedServicingPayments" },
            new string[] { "sStatusT", "GetStatusDateRepFromStatus" }, // OPM 473263 - GetStatusDateRepFromStatus needed when sStatusT included because it's called from within Save when sStatusT is updated.
            new string [] { "sConstructionPurposeT", "sfUpdateNLMSCallReportData" },
        };

        private static readonly string[] TEMP_GFEArchiveFieldNamesFrom2013Q3Release_ExtraUnion = new string[] { "aBNm", "aCNm", "sfGetPreparerOfForm" };

        private static readonly string[][] x_TriggerCollections =
        {
            ConstApp.DisclosureFieldChangeDependencies.ToArray(), // OPM 105770 - dependencies for field change triggers
            ConstApp.DisclosureWorkflowFieldDependencies.ToArray(), // OPM 105770 - dependencies for RESPA workflow trigger
        };

        /// <summary>
        /// Gets the list of special dependencies. If expandTriggerFieldsIfNeeded it will return the 
        /// dependencies needed for trigger firing code. If false it will return only the circular dependencies.
        /// </summary>
        /// <param name="expandTriggerFieldsIfNeeded"></param>
        /// <returns>A list of fields that should always be loaded together if any one of them is loaded.</returns>
        public static IEnumerable<StringList> GetSpecialDependencies(bool expandTriggerFieldsIfNeeded)
        {
            expandTriggerFieldsIfNeeded = ConstStage.LoadTriggerFieldDependenciesAllTheTime || expandTriggerFieldsIfNeeded;

            if (expandTriggerFieldsIfNeeded)
            {
                return specialDependencyFieldListWithTriggers;
            }
            else
            {
                return specialDependencyFieldList;
            }
        }

        private static readonly StringList[] specialDependencyFieldList = x_SpecialDependencyFieldList.Select(fieldList => CreateStringList(fieldList, true)).ToArray();
        private static readonly StringList[] specialDependencyFieldListWithTriggers = specialDependencyFieldList.Concat(x_TriggerCollections.Select(fieldList => CreateStringList(fieldList, true))).ToArray();

        /// <summary>
        /// Creates a set of <see cref="FieldSpec"/> items from a list of string field ids, after dependency graph generation.
        /// </summary>
        /// <param name="fields">The list of field ids.</param>
        /// <param name="throwOnInvalidFields">Indicates whether to throw when an invalid field is found.</param>
        /// <returns>The set of validated fields.</returns>
        private static StringList CreateStringList(IEnumerable<string> fields, bool allowFieldInitialization = false)
        {
            var list = new StringList();
            foreach (string item in fields)
            {
                var fieldId = allowFieldInitialization ? FieldSpec.Create(item) : FieldSpec.RetrieveOrNull(item);
                if (fieldId != null)
                {
                    list.Add(fieldId);
                }
            }

            return list;
        }

        private static readonly FieldSpec GfeInitialDisclosureD = FieldSpec.Create(nameof(CPageBase.sGfeInitialDisclosureD));

        private static readonly string[] GfeArchiveFieldNamesWithExtraUnion = CPageBase.TEMP_GFEArchiveFieldNamesFrom2013Q3Release.Union(TEMP_GFEArchiveFieldNamesFrom2013Q3Release_ExtraUnion).ToArray();

        private static void ExpandFieldImpl(StringList set, FieldSpec fieldName, bool expandTriggerFieldsIfNeeded = false)
        {
            bool has_sGfeInitialDisclosureD = false;

            set.Add(fieldName);

            if (fieldName.Equals(GfeInitialDisclosureD))
            {
                has_sGfeInitialDisclosureD = true;
            }
            
            foreach (var dependencyList in GetSpecialDependencies(expandTriggerFieldsIfNeeded))
            {
                if (dependencyList.Contains(fieldName))
                {
                    has_sGfeInitialDisclosureD |= dependencyList.Contains(GfeInitialDisclosureD);
                    set.AddRange(dependencyList);
                }
            }

            if (has_sGfeInitialDisclosureD)
            {
                foreach (var o in GfeArchiveFieldNamesWithExtraUnion)
                {
                    set.Add(o);
                }                
            }
        }

        private static StringList ExpandSpecialDependencyFields(StringList inputList, bool expandTriggerFieldsIfNeeded = false)
        {
            StringList items = new StringList();

            if (ConstStage.EnableParallelFieldGathering && inputList.Count > 1)
            {
                var resultBag = new System.Collections.Concurrent.ConcurrentBag<StringList>();

                System.Threading.Tasks.Parallel.ForEach(
                    inputList.GetFieldSpecList(),
                    field =>
                    {
                        var fieldList = new StringList();
                        ExpandFieldImpl(fieldList, field, expandTriggerFieldsIfNeeded);
                        resultBag.Add(fieldList);
                    });

                foreach (var stringList in resultBag)
                {
                    items.AddRange(stringList);
                }
            }
            else
            {
                foreach (var field in inputList.GetFieldSpecList())
                {
                    ExpandFieldImpl(items, field, expandTriggerFieldsIfNeeded);
                }
            }

            return items;
        }

        /// <summary>
        /// Track the properties with quick lookup and provide
        /// a simple static singleton.
        /// </summary>

        private IReadOnlyDictionary<string, FieldProperties> m_Lookup;
        private IReadOnlyDictionary<FieldSpec, StringList> dependencyLookupForNonObsoleteFields;

        #region ( Singleton Interface )

        private static readonly Lazy<CFieldInfoTable> Instance = new Lazy<CFieldInfoTable>(
            () => new CFieldInfoTable(useCache: ConstStage.EnableFieldInfoCache),
            System.Threading.LazyThreadSafetyMode.PublicationOnly);

        public static CFieldInfoTable GetInstance() 
        {
            // Get our singleton.
            return CFieldInfoTable.Instance.Value;
        }

        /// <summary>
        /// Creates an instance of the <see cref="CFieldInfoTable"/>
        /// without attempting to retrieve cached data.
        /// </summary>
        /// <returns>
        /// A new instance of the field info table.
        /// </returns>
        /// <remarks>
        /// This method is intended to be used only to create
        /// the field info cache and should not be used outside
        /// of the <seealso cref="FieldInfoCacheManager"/>.
        /// </remarks>
        internal static CFieldInfoTable GetNoCacheInstance()
        {
            return new CFieldInfoTable(useCache: false);
        }
        #endregion
        
        private UpdateResult m_CachingSet;

        public UpdateResult CachingSet
        {
            get { return m_CachingSet; }
        }

        public FieldProperties this[ string sFieldId ]
        {
            get
            {
                FieldProperties ret = null;

                if (string.IsNullOrEmpty(sFieldId) == false)
                {
                    if (!m_Lookup.TryGetValue(sFieldId, out ret))
                    {
                        ret = null;
                    }
                }

                return ret;
            }
        }

        public UpdateResult GatherAffectedCacheFields_Obsolete(ICollection sSourceFieldNames)
        {
            // 10/21/2013 dd - This method is for legacy code. Technically we should use
            // the generic version which perform code-type check at compile time.

            List<string> list = new List<string>();
            foreach (string id in sSourceFieldNames)
            {
                list.Add(id);
            }
            return GatherAffectedCacheFields(list);
        }

        /// <summary>
        /// Round up the affected cached fields given the input
        /// field set, and determine the database fields required
        /// to fully recompute these cached fields.
        /// </summary>
        /// <param name="sFieldNames">
        /// List of input names.
        /// </param>
        /// <returns>
        /// Cache impact.
        /// </returns>

        public UpdateResult GatherAffectedCacheFields( IEnumerable<string> sSourceFieldNames )
        {
            // Gather the impact result.
            
            try
            {
                // Initialize an empty result and compute the merged
                // affected set, given all the inputs.

                UpdateResult result = new UpdateResult();
                result.UpdateFields.Add("sBrokerId");
                foreach( string id in sSourceFieldNames )
                {
                    // Get the properties and load up.

                    FieldProperties properties;
                    if (!m_Lookup.TryGetValue(id, out properties)) 
                    {
                        // Invalid field input, so flag as error.

                        continue;
                    }

                    if( properties.IsObsolete == true )
                    {
                        // Invalid field input, so flag warning.

                        continue;
                    }

                    foreach (FieldSpec item in properties.AffectedCacheFields.GetFieldSpecList() )
                    {
                        // Add this affected field to our set.
                        result.AddUpdateFields(item);

                    }
                }

                foreach( var id in result.UpdateFields.GetFieldSpecList() )
                {
                    // Get the properties and load up.

                    FieldProperties properties;
                    if (!m_Lookup.TryGetValue(id.Name, out properties))
                    {
                        continue;
                    }

                    foreach (FieldSpec item in properties.DependencyFields.GetFieldSpecList() )
                    {
                        // Add this dependency to our set.

                        FieldProperties dependency;
                        if (!m_Lookup.TryGetValue(item.Name, out dependency))
                        {
                            dependency = null;
                        }

                        if( dependency == null || dependency.IsObsolete == true )
                        {
                            continue;
                        }

                        result.AddDbFields( item );
                    }
                }

                return result;
            }
            catch( Exception e )
            {
                Tools.LogErrorWithCriticalTracking(e);
                throw;
            }
        }

        public DependResult GatherRequiredIndepFields_OBSOLETE(ICollection sTargetFieldNames, bool expandTriggerFieldsIfNeeded = false)
        {
            List<string> list = new List<string>();

            foreach (string o in sTargetFieldNames)
            {
                list.Add(o);
            }

            return GatherRequiredIndepFields(list, expandTriggerFieldsIfNeeded);
        }
        /// <summary>
        /// Round up the required independent fields given the
        /// input field set to fully recompute these fields.
        /// </summary>
        /// <param name="sFieldNames">
        /// List of input names.
        /// </param>
        /// <returns>
        /// Required fields.
        /// </returns>

        public DependResult GatherRequiredIndepFields(IEnumerable<string> sTargetFieldNames, bool expandTriggerFieldsIfNeeded = true)
        {
            // Gather the required result.

            var fieldList = ExpandSpecialDependencyFields(CreateStringList(sTargetFieldNames), expandTriggerFieldsIfNeeded);
            try
            {
                // Initialize an empty result and compute the merged
                // required set, given all the inputs.

                DependResult result = new DependResult();

                if (ConstStage.EnableParallelFieldGathering && fieldList.Count > 1)
                {
                    var resultBag = new System.Collections.Concurrent.ConcurrentBag<StringList>();

                    System.Threading.Tasks.Parallel.ForEach(
                        fieldList.GetFieldSpecList(),
                        field =>
                        {
                            StringList dependencies;
                            if (this.dependencyLookupForNonObsoleteFields.TryGetValue(field, out dependencies))
                            {
                                resultBag.Add(dependencies);
                            }
                        });

                    foreach (var stringList in resultBag)
                    {
                        result.AddDbFields(stringList);
                    }
                }
                else
                {
                    foreach (FieldSpec fieldId in fieldList.GetFieldSpecList())
                    {
                        StringList dependencies;
                        if (this.dependencyLookupForNonObsoleteFields.TryGetValue(fieldId, out dependencies))
                        {
                            result.AddDbFields(dependencies);
                        }
                    }
                }

                return result;
            }
            catch( Exception e )
            {
                Tools.LogErrorWithCriticalTracking(e);
                throw; // Bubble up.
            }
        }
        
        /// <summary>
        /// Repopulate the lookup table with the field info.
        /// </summary>

        private void InitializeInfos(bool useCache)
        {
            // Initialize our table.
            var logBuilder = new StringBuilder();
            var sw = Stopwatch.StartNew();

            Action<string> logAction = log => logBuilder.AppendLine($"{log} executed at {sw.ElapsedMilliseconds} ms.");

            var fieldDbInfoList = CFieldDbInfoList.TheOnlyInstance;

            logBuilder.AppendLine("Obtained field DB info list at " + sw.ElapsedMilliseconds + " ms.");

            if (useCache)
            {
                var cacheManager = new FieldInfoCacheManager();
                var cacheResult = cacheManager.GetCachedProperties();

                if (cacheResult != null)
                {
                    this.m_Lookup = cacheResult.MainLookup;
                    this.dependencyLookupForNonObsoleteFields = cacheResult.DependencyLookupForNonObsoleteFields;
                    this.m_CachingSet = cacheResult.CachingSet;

                    logBuilder.AppendLine("Obtained cached properties at " + sw.ElapsedMilliseconds + " ms.");
                    Tools.LogInfo("CFieldInfoTable::InitializeInfos using resource cache executed in " + sw.ElapsedMilliseconds + " ms. Timing breakdown: " + logBuilder.ToString());
                    return;
                }
                else
                {
                    logBuilder.AppendLine("Could not obtain cache result, falling back to reflection method at " + sw.ElapsedMilliseconds + " ms.");
                }
            }

            var initializationParameters = Tools.GetFieldInfoInitializationParametersFromTypeInfo(
                usePredefinedObsoletePropertyOptimization: ConstStage.UsePredefinedObsoleteListForFieldInfoInit,
                logAction: logAction);

            logBuilder.AppendLine("Obtained initialization parameters at " + sw.ElapsedMilliseconds + " ms.");

            // Start by gathering up the necessary support information
            // for each field.  We copy the results into our field
            // info repository.

            UpdateGraph updateGraph = new UpdateGraph();

            logBuilder.AppendLine("Setup accessor list at " + sw.ElapsedMilliseconds + " ms.");

            updateGraph.InitializeGraph(
                initializationParameters.DependsOnAttributes,
                useSpecSetOptimization: ConstStage.UseSpecSetContainsOptimization,
                logAction: logAction);

            logBuilder.AppendLine("Initialized update graph at " + sw.ElapsedMilliseconds + " ms.");

            // Use the dependency decorations as an indicator that the
            // property is a first-class data field.  We look up the
            // field in the other repositories to build the complete
            // infoset for all data fields.

            Dictionary<string, FieldProperties> fieldPropertyDictionary = new Dictionary<string, FieldProperties>(StringComparer.OrdinalIgnoreCase);
            Hashtable fl = new Hashtable();

            foreach (FieldSpec key in updateGraph.Keys)
            {
                FieldNode node = updateGraph[key];

                if (node == null)
                {
                    // This should never happen.

                    continue;
                }

                fieldPropertyDictionary.Add(key.Name, new FieldProperties(key));

                fl.Add(key.Name.ToLower(), key);
            }

            logBuilder.AppendLine("Setup initial field property dictionary at " + sw.ElapsedMilliseconds + " ms.");

            // Now, round up the database info objects for each
            // field that was declared in the previous block.

            foreach (string key in fieldPropertyDictionary.Keys)
            {
                FieldProperties properties = fieldPropertyDictionary[key];

                properties.DbInfo = fieldDbInfoList.Get(key);
            }

            logBuilder.AppendLine("Setup DbInfo properties at " + sw.ElapsedMilliseconds + " ms.");

            foreach (string key in fieldDbInfoList.FieldNameList)
            {
                if (fieldPropertyDictionary.ContainsKey(key) == false)
                {
                    // We expect db fields to be tagged.  If not, we
                    // create an entry and assume it's obsolete.

                    if (fl.Contains(key.ToLower()) == false)
                    {
                        FieldProperties obsolete = new FieldProperties(FieldSpec.Create(key));

                        obsolete.IsObsolete = true;

                        fieldPropertyDictionary.Add(key, obsolete);
                    }

                }
            }

            logBuilder.AppendLine("Added missing properties as obsolete at " + sw.ElapsedMilliseconds + " ms.");

            // We need the dependency information for each field
            // for updating a cache table and for updating a form
            // on the fly.

            List<int> removeList = new List<int>();
            foreach (string key in fieldPropertyDictionary.Keys)
            {
                FieldProperties properties;

                if (fieldPropertyDictionary.TryGetValue(key, out properties))
                {
                    // Populate our precalculated dependency results
                    // for this field.  We always add the cached field
                    // to its own affected cached field set.  We do
                    // this so that updates to this field properly
                    // propagate back to the cache table.

                    UpdateResult deps = updateGraph.LookupEntry(key, useSpecSetOptimization: ConstStage.UseSpecSetContainsOptimization);

                    properties.AffectedCacheFields = deps.UpdateFields;
                    properties.AffectedFields = deps.UpdateFields;
                    properties.DependencyFields = deps.DbFields;
                    removeList.Clear();

                    for (int i = 0; i < properties.AffectedCacheFields.Count; ++i)
                    {
                        FieldProperties that;
                        if (!fieldPropertyDictionary.TryGetValue(properties.AffectedCacheFields[i], out that))
                        {
                            continue;
                        }


                        if (that.IsCached == false)
                        {
                            removeList.Add(i);
                        }
                    }

                    properties.AffectedCacheFields.RemoveAt(removeList);
                    if (properties.IsCached == true)
                    {
                        properties.AffectedCacheFields.Add(key);
                    }
                }
            }

            logBuilder.AppendLine("Setup affected and dependency fields at " + sw.ElapsedMilliseconds + " ms.");

            // Tag all the obsolete fields as obsolete.  Note
            // that we do maintain them in the properties list.

            foreach (string key in initializationParameters.ObsoletePropertyNameList)
            {
                FieldProperties properties;
                if (fieldPropertyDictionary.TryGetValue(key, out properties))
                {
                    properties.IsObsolete = true;
                }
            }

            // Lock in our lookup table so that no table
            // is stored if we leave before this point.

            m_Lookup = fieldPropertyDictionary;

            HashSet<FieldSpec> obsoleteFieldIds = new HashSet<FieldSpec>();
            foreach (FieldProperties properties in fieldPropertyDictionary.Values)
            {
                if (properties.IsObsolete)
                {
                    obsoleteFieldIds.Add(properties.FieldSpec);
                }
            }

            logBuilder.AppendLine("Set IsObsolete property and obtained obsolete property IDs at " + sw.ElapsedMilliseconds + " ms.");

            // Need to finish complete evaluation of all obsolete fields before we can make lists sanitized of their values
            Dictionary<FieldSpec, StringList> dependencyLookup = new Dictionary<FieldSpec, StringList>(fieldPropertyDictionary.Count - obsoleteFieldIds.Count);
            foreach (FieldProperties properties in fieldPropertyDictionary.Values)
            {
                if (!properties.IsObsolete)
                {
                    StringList nonObsoleteDependencies = new StringList();
                    foreach (var fieldId in properties.DependencyFields.GetFieldSpecList())
                    {
                        if (!obsoleteFieldIds.Contains(fieldId))
                        {
                            nonObsoleteDependencies.Add(fieldId);
                        }
                    }

                    dependencyLookup.Add(properties.FieldSpec, nonObsoleteDependencies);
                }
            }

            this.dependencyLookupForNonObsoleteFields = dependencyLookup;

            logBuilder.AppendLine("Created non-obsolete field dependency lookup at " + sw.ElapsedMilliseconds + " ms.");

            // Initialize our static lookup table
            // with the dependencies we processed.

            List<string> id = new List<string>();

            foreach (FieldProperties properties in m_Lookup.Values)
            {
                if (properties.DbInfo != null && properties.DbInfo.IsCachedField == true)
                {
                    id.Add(properties.FieldName);
                }
            }
            m_CachingSet = GatherAffectedCacheFields(id);

            logBuilder.AppendLine("Setup caching set at " + sw.ElapsedMilliseconds + " ms.");

            Tools.LogInfo($"CFieldInfoTable.InitializeInfos({useCache}) executed in " + sw.ElapsedMilliseconds + " ms. Timing breakdown: " + logBuilder.ToString());
        }

        /// <summary>
        /// Expose enumeration interface for walking properties.
        /// </summary>
        /// <returns>
        /// Enumerator of properties.
        /// </returns>

        public IEnumerator GetEnumerator()
        {
            // Fetch values enumerator.

            return m_Lookup.Values.GetEnumerator();
        }

        /// <summary>
        /// Construct default.  We access the the loan file
        /// fields and application data to gather all the
        /// decorated fields.
        /// </summary>

        private CFieldInfoTable(bool useCache)
        {
            // Initialize members.
            InitializeInfos(useCache);
        }

        internal void Accept(FieldInfoCacheManager manager)
        {
            manager.CachingSet = this.CachingSet;
            manager.MainLookup = this.m_Lookup;
            manager.DependencyLookupForNonObsoleteFields = this.dependencyLookupForNonObsoleteFields;
        }
    }

    /// <summary>
    /// We stash all the decoration info that we associate
    /// with each field in this properties bag.
    /// </summary>

    public class FieldProperties
    {
        /// <summary>
        /// Track fields by their id and store the associated
        /// properties.  Add whatever you need.  As long as you
        /// pull it from the custom attribute decorations on
        /// the field, or from the db directly, then we should
        /// be okay.
        /// </summary>

        /// <summary>
        /// We track fields, first by their id, and secondly by
        /// their names and domains.
        /// </summary>


        // unique field identifier
        public string FieldName { get; private set; }

        internal FieldSpec FieldSpec { get; }

        /// <summary>
        /// We need to remember calculated lists of fields that
        /// are affected when this field changes, and the fields
        /// that are required (this field is dependent upon them)
        /// to compute the value of this field.
        /// </summary>

        // fields required to compute
        public StringList DependencyFields { get; set;}

        // cached affected by change
        public StringList AffectedCacheFields { get; set;}

        // any affected by change
        public StringList AffectedFields { get; set;}


        /// <summary>
        /// We track the database representation format also.
        /// The reporting uses this to auto-generate select
        /// statements to query the tables for results.
        /// </summary>

        public CFieldDbInfo DbInfo { get; set;}

        /// <summary>
        /// Each field is either regular/calculated, cached and
        /// regular/calculated, or regular/calculated and cached
        /// and reportable.  So all fields are regular or calculated,
        /// and some are reportable, which requires that it is
        /// a cached field as well.
        /// </summary>
        public bool IsCached
        {
            // Access member.

            get
            {
                if (DbInfo != null && DbInfo.IsCachedField == true)
                {
                    return true;
                }

                return false;
            }
        }

        public bool IsObsolete { get; set;}


        /// <summary>
        /// Construct field properties bag.
        /// </summary>
        /// <param name="fieldSpec">
        /// Unique field identifier.
        /// </param>
        public FieldProperties(FieldSpec fieldSpec)
        {
            // Set initial values.  Start with an id
            // and then initialize outside of the ctor
            // to prepare an entry.
            this.FieldSpec = fieldSpec;
            FieldName = fieldSpec.Name;

            DependencyFields    = new StringList();
            AffectedCacheFields = new StringList();
            AffectedFields      = new StringList();

            DbInfo = null;

            IsObsolete   = false;
        }

        public FieldProperties(FieldPropertiesModel model)
        {
            this.FieldSpec = FieldSpec.Create(model.FieldSpec.Name);
            this.FieldName = model.FieldName;

            this.DependencyFields = new StringList(model.DependencyFields);
            this.AffectedCacheFields = new StringList(model.AffectedCacheFields);
            this.AffectedFields = new StringList(model.AffectedFields);

            this.DbInfo = CFieldDbInfoList.TheOnlyInstance.Get(this.FieldName);
            this.IsObsolete = model.IsObsolete;
        }
    }

    /// <summary>
    /// Maintain list of strings in unique set
    /// for fast unique insertion.
    /// </summary>
    public class StringList
    {
        private List<FieldSpec> m_fieldSpecList;

        private BitArray m_bitArray;

        public StringList()
        {
            this.m_fieldSpecList = new List<FieldSpec>();
            this.m_bitArray = new BitArray(FieldSpec.MaxFieldId);
        }

        public StringList(StringList s)
        {
            this.m_fieldSpecList = new List<FieldSpec>(s.m_fieldSpecList);
            this.m_bitArray = new BitArray(s.m_bitArray);
        }

        public StringList(StringListModel model)
        {
            this.m_fieldSpecList = model.FieldSpecList.Select(fieldSpecModel => FieldSpec.Create(fieldSpecModel.Name)).ToList();
            this.m_bitArray = new BitArray(FieldSpec.MaxFieldId);
            this.m_fieldSpecList.ForEach(fieldSpec => this.m_bitArray.Set(fieldSpec.internalId, true));
        }

        private List<string> GetStringList()
        {
            // 10/4/2015 - Eventually I want to remove this method and only use GetFieldSpecList() method.
            List<string> list = new List<string>(m_fieldSpecList.Count);
            for (int i = 0; i < m_fieldSpecList.Count; i++)
            {
                list.Add(m_fieldSpecList[i].Name);
            }
            return list;
        }

        /// <summary>
        /// Get by position.
        /// </summary>
        public string this[int iIndex]
        {
            get { return m_fieldSpecList[iIndex].Name; }
        }

        public IEnumerable<string> StringCollection
        {
            get { return GetStringList(); }
        }

        public ICollection Collection
        {
            get { return GetStringList(); }
        }

        public int Count
        {
            get { return m_fieldSpecList.Count; }
        }

        public IEnumerable<FieldSpec> GetFieldSpecList()
        {
            return m_fieldSpecList;
        }

        /// <summary>
        /// Search for a match and report true if found.
        /// </summary>
        /// <param name="sItem">
        /// Key to match against.
        /// </param>
        /// <returns>
        /// True if found.
        /// </returns>
        public bool Has(string sItem)
        {
            FieldSpec spec = FieldSpec.Create(sItem);
            return m_bitArray.Get(spec.internalId);
        }

        public bool Contains(FieldSpec item)
        {
            return m_bitArray.Get(item.internalId);
        }

        public void Add(FieldSpec item)
        {
            if (m_bitArray.Get(item.internalId) == false)
            {
                m_bitArray.Set(item.internalId, true);
                this.m_fieldSpecList.Add(item);
            }
        }

        /// <summary>
        /// Add new item in order.  Using binary sort.
        /// We ignore duplicate entries.
        /// </summary>
        /// <param name="sItem">
        /// Item to insert.
        /// </param>
        public void Add(string sItem)
        {
            this.Add(FieldSpec.Create(sItem));
        }

        public void AddRange(IEnumerable<string> fields)
        {
            foreach (var field in fields)
            {
                this.Add(field);
            }
        }

        public void AddRange(StringList fields)
        {
            foreach (var field in fields.m_fieldSpecList)
            {
                this.Add(field);
            }
        }

        public void RemoveAt(int index)
        {
            FieldSpec spec = this.m_fieldSpecList[index];
            m_bitArray.Set(spec.internalId, false);

            this.m_fieldSpecList.RemoveAt(index);
        }

        public void RemoveAt(List<int> arrIndex)
        {
            if (arrIndex.Count != 0)
            {
                foreach (var index in arrIndex)
                {
                    FieldSpec spec = this.m_fieldSpecList[index];
                    m_bitArray.Set(spec.internalId, false);
                    m_fieldSpecList[index] = null;
                }

                m_fieldSpecList.RemoveAll(e => e == null);
            }
        }
    }

    /// <summary>
    /// Simple collection of exceptions.
    /// </summary>
    class ExceptionBucket : ArrayList
    {
        /// <summary>
        /// Overloaded streaming operator to simulate append.
        /// </summary>
        /// <param name="eB">
        /// Bucket to fill with exception.
        /// </param>
        /// <param name="eX">
        /// Exception to hold.
        /// </param>
        /// <returns>
        /// Appended bucket.
        /// </returns>

        public static ExceptionBucket operator+( ExceptionBucket b , Exception e )
        {
            // Append to the bucket.

            if( e != null )
            {
                b.Add( e );
            }

            return b;
        }

    }

}
