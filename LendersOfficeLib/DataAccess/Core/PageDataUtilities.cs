﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Reflection;
    using System.Reflection.Emit;
    using System.Text.RegularExpressions;
    using DavidDao.Reflection;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Rolodex;
    using LendersOffice.UI;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;

    public enum E_PageDataFieldType
    {
        Unknown,
        String,
        Money,
        Percent,
        Ssn,
        Phone,
        Integer,
        Bool,
        Enum,
        DateTime,
        Guid,
        Decimal,
        Array,
        SensitiveString
    }
    public enum E_PageDataClassType
    {
        CBasePage = 0,
        CAppBase = 1
    }
    public static class PageDataUtilities
    {
        /* // 5/26/2010 dd -
         *
         * Note: Format of Field Id
         *
         *     Regular field id:  sLAmtCalc, aBNm
         *     Enum Type that will return actual value:  sLPurposeT
         *     Enum Type that return Yes / No.  {FieldEnumId}:{EnumValue}. Example: sLPurposeT:5
         *     Preparer Field: GetPreparerOfForm[{FormType}].{PropertyName}. Example: GetPreparerOfForm[App1003Interviewer].PreparerName
         *
         */
        private static Regex s_fieldNameStructureRegEx = new Regex(@"^(?<fieldid>[a-zA-Z0-9_]+)(\[(?<arrayIndex>[a-zA-Z0-9_]+)\]\.?(?<subItem>[a-zA-Z0-9_]+)?)?(\:(?<enumValue>[a-zA-Z0-9_]+))?$");
        private class _FieldNameStructure
        {
            /// <summary>
            /// Format:  ActualId[ArrayIndex].SubItemName:EnumValue
            /// </summary>
            ///
            public _FieldNameStructure(string fieldId)
            {
                if (!string.IsNullOrEmpty(fieldId) && ValidFieldNameFormat(fieldId))
                {
                    Match match = s_fieldNameStructureRegEx.Match(fieldId);
                    ActualId = match.Groups["fieldid"].Value;
                    ArrayIndex = match.Groups["arrayIndex"].Value;
                    SubItemName = match.Groups["subItem"].Value;
                    EnumValue = match.Groups["enumValue"].Value;
                }
                else
                {
                    Tools.LogWarning(string.IsNullOrEmpty(fieldId) ? "Field ID Cannot Be Empty" : "Invalid Field Id. FieldId=[" + fieldId + "]");
                    ActualId = string.Empty;
                    ArrayIndex = string.Empty;
                    SubItemName = string.Empty;
                    EnumValue = string.Empty;
                }

            }
            public string ActualId { get; private set; }
            public string EnumValue { get; private set; }
            public string ArrayIndex { get; private set; }
            public string SubItemName { get; private set; }
        }



        private class _PageDataFieldItem
        {
            public E_PageDataFieldType FieldType { get; set; }
            public E_PageDataClassType ClassType { get; set; }
            public Type EnumerationType { get; set; }
            public MethodInfo InvokeGetMethod { get; set; }
            public MethodInfo InvokeSetMethod { get; set; }
            public LqbInputModel CustomInputModel { get; set; }
        }

        private static Dictionary<string, _PageDataFieldItem> s_fieldHash = null;
        private static Dictionary<string, Tuple<PropertyInfo, MethodInfo, MethodInfo>> s_agentHash = null;
        private static Dictionary<string, Tuple<PropertyInfo, MethodInfo, MethodInfo>> s_preparerHash = null;

        private static Dictionary<string, string> s_alternateFieldNameDictionary = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
        {
            {"sLAmt", "sLAmtCalc"}
            ,{"aBGenderT", "aBGender"}
            ,{"aCGenderT", "aCGender"}
            ,{"aFHADebtPmtTot2", "aFHADebtPmtTot"}
            ,{"sLAmt2","sLAmtCalc"}
            ,{"sNoteIR2","sNoteIR"}
            ,{"sNoteIR3", "sNoteIR"}
            ,{"sTerm2", "sTerm"}
            ,{"sUfCashPdzzz2", "sUfCashPd"}
            ,{"sFinalLAmt2", "sFinalLAmt"}
            , {"sFinalLAmt3", "sFinalLAmt"}
            ,{"sGfeCanRateIncrease2", "sGfeCanRateIncrease"}
            ,{"sGfeCanRateIncrease3","sGfeCanRateIncrease"}
            ,{"sGfeCanRateIncrease4","sGfeCanRateIncrease"}
            ,{"sLenderCustomCredit1Description2", "sLenderCustomCredit1Description"}
            ,{"sLenderCustomCredit1AmountAsCharge2", "sLenderCustomCredit1AmountAsCharge"}
            ,{"sLenderCustomCredit2Description2", "sLenderCustomCredit2Description"}
            ,{"sLenderCustomCredit2AmountAsCharge2", "sLenderCustomCredit2AmountAsCharge"}
            ,{"sLenderCustomCredit1Amount2", "sLenderCustomCredit1Amount"}
            ,{"sLenderCustomCredit2Amount2", "sLenderCustomCredit2Amount"}
            ,{"sLenderPaidBrokerCompFAsCreditAmt2", "sLenderPaidBrokerCompFAsCreditAmt"}
            ,{"sLenderTargetInitialCreditAmt2", "sLenderTargetInitialCreditAmt"}
            ,{"sLenderPaidBrokerCompPc2", "sLenderPaidBrokerCompPc"}
            ,{"sLenderPaidBrokerCompF2", "sLenderPaidBrokerCompF"}
            ,{"sLenderPaidBrokerCompF3", "sLenderPaidBrokerCompF"}
            ,{"sLenderPaidBrokerCompF4", "sLenderPaidBrokerCompF"}
            ,{"sSettlementTotalFundByLenderAtClosing2", "sSettlementTotalFundByLenderAtClosing"}
            ,{"sSettlementTotalFundByLenderAtClosing_Neg2", "sSettlementTotalFundByLenderAtClosing_Neg"}
            ,{"sLenderTargetInitialCreditAmt_Neg2", "sLenderTargetInitialCreditAmt_Neg"}
            ,{"sLenderActualInitialCreditAmt_Neg2", "sLenderActualInitialCreditAmt_Neg"}
            ,{"sBrokerLockFinalBrokComp1PcPrice2", "sBrokerLockFinalBrokComp1PcPrice"}
            ,{"sBrokerLockFinalBrokComp1PcAmt2", "sBrokerLockFinalBrokComp1PcAmt"}
            ,{"sBrokerLockOriginatorPriceBrokComp1PcPrice2", "sBrokerLockOriginatorPriceBrokComp1PcPrice"}
            ,{"sBrokerLockOriginatorPriceBrokComp1PcAmt2", "sBrokerLockOriginatorPriceBrokComp1PcAmt"}
            ,{"sLDiscntBaseT2", "sLDiscntBaseT"}
            ,{"sToleranceCure_Neg2", "sToleranceCure_Neg"}
            ,{"sLenderActualTotalCreditAmt_Neg2", "sLenderActualTotalCreditAmt_Neg"}
            ,{"sLenderPaidBrokerCompF_Neg2", "sLenderPaidBrokerCompF_Neg"}
            ,{"sTRIDClosingDisclosureTotalLoanCosts2","sTRIDClosingDisclosureTotalLoanCosts"}
            ,{"sLenderAdditionalCreditAtClosingAmt_Neg2","sLenderAdditionalCreditAtClosingAmt_Neg"}
            ,{"sConsummationD2","sConsummationD"}
            ,{"sConsummationD2Lckd","sConsummationDLckd"}
        };

        /// <summary>
        /// Gathers the fields that will be available through reflection.
        /// </summary>
        static PageDataUtilities()
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            s_fieldHash = new Dictionary<string, _PageDataFieldItem>(StringComparer.OrdinalIgnoreCase);
            AddSpecialFieldIdsForCollectionAccess(s_fieldHash);

            // 9/22/2015 - dd - Preload all the PropertyInfo from the classes we need to inspect.
            var propertyMapsByType = new[] { typeof(CBase), typeof(CPageBase), typeof(CAppBase), typeof(CPageData), typeof(CAppData) }
                .ToDictionary(t => t, t => t.GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public).ToDictionary(o => o.Name));

            foreach (E_PageDataClassType baseDataClassType in new[] { E_PageDataClassType.CBasePage, E_PageDataClassType.CAppBase })
            {
                Type baseType = GetType(baseDataClassType);
                Dictionary<string, PropertyInfo> baseTypePropertiesByName = propertyMapsByType[baseType];

                Type invokingType = GetInvokingType(baseType);
                Dictionary<string, PropertyInfo> invokingTypePropertiesByName = propertyMapsByType[invokingType];

                foreach (PropertyInfo baseTypeProperty in baseTypePropertiesByName.Values)
                {
                    if (baseTypeProperty.Name.EndsWith("_rep"))
                    {
                        continue;
                    }

                    _PageDataFieldItem fieldInfo = GetFieldInfo(baseDataClassType, baseTypeProperty, propertyMapsByType);
                    if (fieldInfo == null)
                    {
                        continue;
                    }

                    s_fieldHash.Add(baseTypeProperty.Name, fieldInfo);

                    if (fieldInfo.FieldType == E_PageDataFieldType.Enum)
                    {
                        _PageDataFieldItem enumRepFieldInfo = GetEnumRepFieldInfo(baseDataClassType, baseTypeProperty, invokingTypePropertiesByName);
                        if (enumRepFieldInfo != null)
                        {
                            s_fieldHash.Add(baseTypeProperty.Name + "_rep", enumRepFieldInfo);
                        }
                    }
                }
            }

            // This delegate creates dictionary for IPreparerFields and CAgentFields.
            // Key is property Name, value is Tuple<propertyInfo,GetMethodInfo,SetMethodInfo>.
            Func<Type, Dictionary<string, Tuple<PropertyInfo, MethodInfo, MethodInfo>>> agentPreparerGatherer =
                type => type.GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .Select(prop => new Tuple<PropertyInfo, MethodInfo, MethodInfo>(prop, prop.GetGetMethod(), prop.GetSetMethod()))
                .ToDictionary(tuple => tuple.Item1.Name, StringComparer.OrdinalIgnoreCase);

            s_agentHash = agentPreparerGatherer(typeof(CAgentFields));
            s_preparerHash = agentPreparerGatherer(typeof(IPreparerFields));

            stopwatch.Stop();
            Tools.LogInfo("PageDataUtilities Static Constructor executed in " + stopwatch.ElapsedMilliseconds.ToString("#,#") + "ms.");
        }

        /// <summary>
        /// Adds field items for accessing certain collections.
        /// </summary>
        /// <param name="fields">The fields that will be made available to client code.</param>
        private static void AddSpecialFieldIdsForCollectionAccess(Dictionary<string, _PageDataFieldItem> fields)
        {
            fields.Add("GetPreparerOfForm", new _PageDataFieldItem() { ClassType = E_PageDataClassType.CBasePage, FieldType = E_PageDataFieldType.String });
            fields.Add("GetAgentOfRole", new _PageDataFieldItem() { ClassType = E_PageDataClassType.CBasePage, FieldType = E_PageDataFieldType.String });
            fields.Add("GetInvestorInfo", new _PageDataFieldItem() { ClassType = E_PageDataClassType.CBasePage, FieldType = E_PageDataFieldType.String });
            fields.Add("GetSubservicerInfo", new _PageDataFieldItem() { ClassType = E_PageDataClassType.CBasePage, FieldType = E_PageDataFieldType.String });
            fields.Add("GetAgentOfRoleNoFallback", new _PageDataFieldItem() { ClassType = E_PageDataClassType.CBasePage, FieldType = E_PageDataFieldType.String });
        }

        /// <summary>
        /// Gets the field item for the _rep associated with an enum field. Only supports value retrieval.
        /// </summary>
        /// <param name="baseDataClassType">The base data class type.</param>
        /// <param name="baseTypeProperty">The base property.</param>
        /// <param name="invokingTypePropertiesByName">
        /// A map from field id to the properties that will be used ot access the properties in the base data class.
        /// </param>
        /// <returns>The field item for the associated _rep field. Null if it is not found.</returns>
        private static _PageDataFieldItem GetEnumRepFieldInfo(
            E_PageDataClassType baseDataClassType,
            PropertyInfo baseTypeProperty,
            Dictionary<string, PropertyInfo> invokingTypePropertiesByName)
        {
            // 6/1/2010 dd - Check to see if there is a _rep method for enum. If yes then we want to include in the main hash table.
            PropertyInfo enum_rep = null;
            if (invokingTypePropertiesByName.TryGetValue(baseTypeProperty.Name + "_rep", out enum_rep))
            {
                if (enum_rep != null && enum_rep.PropertyType == typeof(string))
                {
                    if (IsPublicGetProperty(enum_rep))
                    {
                        return new _PageDataFieldItem()
                        {
                            ClassType = baseDataClassType,
                            FieldType = E_PageDataFieldType.String,
                            InvokeGetMethod = enum_rep.GetGetMethod()
                        };
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Determines the input model from custom attributes defined on the given property.
        /// </summary>
        /// <param name="baseTypeProperty">The property to check.</param>
        /// <returns>The input model if it is defined. Null if not defined.</returns>
        private static LqbInputModel GetLqbInputModel(PropertyInfo baseTypeProperty)
        {
            // Jan/27/2016 huyn - Get input type from property attribute
            object[] attrs = baseTypeProperty.GetCustomAttributes(true);

            LqbInputModel customModel = null;
            foreach (object attr in attrs)
            {
                LqbInputModelAttribute inputModelAttr = attr as LqbInputModelAttribute;
                if (inputModelAttr != null)
                {
                    customModel = inputModelAttr.Model;
                }
            }

            return customModel;
        }

        /// <summary>
        /// Gets the type that corresponds to the given data class type.
        /// </summary>
        /// <param name="t">The page data class type.</param>
        /// <returns>The corresponding type.</returns>
        private static Type GetType(E_PageDataClassType t)
        {
            switch (t)
            {
                case E_PageDataClassType.CBasePage:
                    return typeof(CPageBase);
                case E_PageDataClassType.CAppBase:
                    return typeof(CAppBase);
                default:
                    throw new UnhandledEnumException(t);
            }
        }

        /// <summary>
        /// Gets the field item for the given property if it should be exposed through PageDataUtilities.
        /// Returns null if it should not be exposed.
        /// </summary>
        /// <remarks>
        /// Most of the logic deciding whether a field should be included is in GetFieldInfoFromBase.
        /// This method just locates the associated properties in the class that will be used to access
        /// the underlying base properties and ensures that the methods match what exists in the base
        /// class.
        /// </remarks>
        /// <param name="baseDataClassType">The base data class type.</param>
        /// <param name="baseTypeProperty">The property from the base type.</param>
        /// <param name="propertyMapsByType">
        /// A map from type to a map from field id to property info for that type.
        /// </param>
        /// <returns>
        /// The field item for the given property if it should be exposed through PageDataUtilities. 
        /// Returns null if it should not be exposed.
        /// </returns>
        private static _PageDataFieldItem GetFieldInfo(
            E_PageDataClassType baseDataClassType,
            PropertyInfo baseTypeProperty,
            Dictionary<Type, Dictionary<string, PropertyInfo>> propertyMapsByType)
        {
            _PageDataFieldItem baseFieldInfoItem = GetFieldInfoFromBase(baseDataClassType, baseTypeProperty, propertyMapsByType);
            if (baseFieldInfoItem == null)
            {
                return null;
            }

            // CPageData and CAppData are a proxy for the underlying data classes. We need to route
            // access through CPageData and CAppData to ensure workflow is enforced and to be consistent
            // with the way client code has access to loan and application data.
            PropertyInfo invokingTypeProperty = GetInvokingTypeProperty(baseTypeProperty, baseFieldInfoItem, propertyMapsByType);
            if (invokingTypeProperty == null)
            {
                return null;
            }

            MethodInfo invokingTypeGetMethod = GetPublicGetMethodFrom(invokingTypeProperty);
            MethodInfo invokingTypeSetMethod = GetPublicSetMethodFrom(invokingTypeProperty);

            if (!MethodsRoughlyEqual(invokingTypeGetMethod, baseFieldInfoItem.InvokeGetMethod)
                || !MethodsRoughlyEqual(invokingTypeSetMethod, baseFieldInfoItem.InvokeSetMethod))
            {
                return null;
            }

            return new _PageDataFieldItem()
            {
                ClassType = baseFieldInfoItem.ClassType,
                FieldType = baseFieldInfoItem.FieldType,
                EnumerationType = baseFieldInfoItem.EnumerationType,
                InvokeGetMethod = invokingTypeGetMethod,
                InvokeSetMethod = invokingTypeSetMethod,
                CustomInputModel = baseFieldInfoItem.CustomInputModel
            };
        }

        /// <summary>
        /// Gets the property that will be used to access the given base property. Returns null if
        /// it is not found.
        /// </summary>
        /// <param name="baseTypeProperty">The property from the base data class.</param>
        /// <param name="baseFieldInfoItem">The field info from the base data class.</param>
        /// <param name="propertyMapsByType">
        /// A map from type to a map from field id to property info for that type.
        /// </param>
        /// <returns>
        /// The property that will be used to access the given base property. Null if it is not found.
        /// </returns>
        private static PropertyInfo GetInvokingTypeProperty(
            PropertyInfo baseTypeProperty,
            _PageDataFieldItem baseFieldInfoItem,
            Dictionary<Type, Dictionary<string, PropertyInfo>> propertyMapsByType)
        {
            Type baseType = baseTypeProperty.DeclaringType;
            Type invokingType = GetInvokingType(baseType);

            Dictionary<string, PropertyInfo> invokingTypePropertiesByName = propertyMapsByType[invokingType];
            string targetInvokingTypePropertyName = baseFieldInfoItem.InvokeGetMethod.Name.EndsWith("_rep") 
                ? baseTypeProperty.Name + "_rep" 
                : baseTypeProperty.Name;

            PropertyInfo invokingTypeProperty;
            if (invokingTypePropertiesByName.TryGetValue(targetInvokingTypePropertyName, out invokingTypeProperty))
            {
                return invokingTypeProperty;
            }

            return null;
        }

        /// <summary>
        /// Gets the type that will be used to access members in the given base type.
        /// Throws an exception if an invoking type is not found.
        /// </summary>
        /// <param name="baseType">The type of the base data class.</param>
        /// <returns>
        /// The type that will be used to access members defined the the given base type.
        /// </returns>
        /// <exception cref="CBaseException">
        /// Thrown when the invoking type is not found.
        /// </exception>
        private static Type GetInvokingType(Type baseType)
        {
            if (baseType == typeof(CPageBase))
            {
                return typeof(CPageData);
            }
            else if (baseType == typeof(CAppBase))
            {
                return typeof(CAppData);
            }

            throw new CBaseException(ErrorMessages.Generic, "Base type " + baseType.Name + " is unexpected.");
        }

        private static bool MethodsRoughlyEqual(MethodInfo method1, MethodInfo method2)
        {
            if (method1 == null)
            {
                return method2 == null;
            }

            if (method1.ReturnType != method2.ReturnType)
            {
                return false;
            }

            if (method1.GetParameters().Count() != method2.GetParameters().Count())
            {
                return false;
            }

            if (method1.Name != method2.Name)
            {
                return false;
            }

            return true;
        }

        private static MethodInfo GetPublicSetMethodFrom(PropertyInfo prop)
        {
            if (null == prop || false == prop.CanRead)
            {
                return null;
            }

            MethodInfo m = prop.GetSetMethod();
            if (null != m && m.IsPublic)
            {
                return m;
            }

            return null;
        }

        private static MethodInfo GetPublicGetMethodFrom(PropertyInfo prop)
        {
            if (null == prop || false == prop.CanRead)
            {
                return null;
            }

            MethodInfo m = prop.GetGetMethod();
            if (null != m && m.IsPublic)
            {
                return m;
            }

            return null;
        }

        /// <summary>
        /// Gets the field info item for the given property if it should be exposed through
        /// PageDataUtilities. Returns null if it should not be exposed.
        /// </summary>
        /// <remarks>
        /// This method contains the bulk of the logic determining whether a field should be exposed.
        /// For ints, decimals, dates, and nullable primitives/structs a _rep field is required.
        /// Whether a standard property or _rep property is being used, the getter and setter will
        /// only be picked up if they are public.
        /// Fields are only included if we are able to determine the field type and there is a public
        /// getter.
        /// </remarks>
        /// <param name="dataClassType">The data class type.</param>
        /// <param name="baseTypeProperty">The property from the base data class.</param>
        /// <param name="propertyMapsByType">
        /// A map from type to a map from field id to property info for that type.
        /// </param>
        /// <returns>
        /// The field info item for the given property if it should be included. Returns null if the
        /// field should not be included.
        /// </returns>
        private static _PageDataFieldItem GetFieldInfoFromBase(
            E_PageDataClassType dataClassType,
            PropertyInfo baseTypeProperty,
            Dictionary<Type, Dictionary<string, PropertyInfo>> propertyMapsByType)
        {
            string fieldId = baseTypeProperty.Name;
            Dictionary<string, PropertyInfo> baseTypePropertiesByName = propertyMapsByType[baseTypeProperty.DeclaringType];

            E_PageDataFieldType fieldType = E_PageDataFieldType.Unknown;
            Type enumType = null;
            MethodInfo getMethod = null;
            MethodInfo setMethod = null;

            if (baseTypeProperty.PropertyType == typeof(string))
            {
                PropertyInfo _rep;
                PropertyInfo propertyInUse = IsStringFieldWithRep(fieldId) && baseTypePropertiesByName.TryGetValue(fieldId + "_rep", out _rep)
                    ? _rep
                    : baseTypeProperty;

                if (IsPublicGetProperty(propertyInUse))
                {
                    getMethod = propertyInUse.GetGetMethod();
                    fieldType = GetFieldTypeForStringField(getMethod);
                }

                if (IsPublicSetProperty(propertyInUse))
                {
                    setMethod = propertyInUse.GetSetMethod();
                }
            }
            else if (baseTypeProperty.PropertyType == typeof(Sensitive<string>))
            {
                if (IsPublicGetProperty(baseTypeProperty))
                {
                    fieldType = E_PageDataFieldType.SensitiveString;
                    getMethod = baseTypeProperty.GetGetMethod();
                }

                if (IsPublicSetProperty(baseTypeProperty))
                {
                    setMethod = baseTypeProperty.GetSetMethod();
                }
            }
            else if (baseTypeProperty.PropertyType == typeof(bool))
            {
                if (IsPublicGetProperty(baseTypeProperty))
                {
                    getMethod = baseTypeProperty.GetGetMethod();
                    fieldType = E_PageDataFieldType.Bool;
                }

                if (IsPublicSetProperty(baseTypeProperty))
                {
                    setMethod = baseTypeProperty.GetSetMethod();
                }
            }
            else if (baseTypeProperty.PropertyType.IsEnum)
            {
                if (IsPublicGetProperty(baseTypeProperty))
                {
                    getMethod = baseTypeProperty.GetGetMethod();
                    fieldType = E_PageDataFieldType.Enum;
                    enumType = baseTypeProperty.PropertyType;
                }

                if (IsPublicSetProperty(baseTypeProperty))
                {
                    setMethod = baseTypeProperty.GetSetMethod();
                }
            }
            else if (baseTypeProperty.PropertyType == typeof(int))
            {
                // 5/14/2010 dd - Only return id is found if there is a _rep.
                PropertyInfo _rep;
                if (baseTypePropertiesByName.TryGetValue(fieldId + "_rep", out _rep))
                {
                    if (IsPublicGetProperty(_rep))
                    {
                        fieldType = E_PageDataFieldType.Integer;
                        getMethod = _rep.GetGetMethod();
                    }

                    if (IsPublicSetProperty(_rep))
                    {
                        setMethod = _rep.GetSetMethod();
                    }
                }
            }
            else if (baseTypeProperty.PropertyType == typeof(decimal))
            {
                PropertyInfo _rep;
                if (baseTypePropertiesByName.TryGetValue(fieldId + "_rep", out _rep))
                {
                    if (IsPublicGetProperty(_rep))
                    {
                        getMethod = _rep.GetGetMethod();
                        fieldType = GetFieldTypeForDecimalField(fieldId, getMethod);
                    }

                    if (IsPublicSetProperty(_rep))
                    {
                        setMethod = _rep.GetSetMethod();
                    }
                }
            }
            else if (baseTypeProperty.PropertyType == typeof(CDateTime) || baseTypeProperty.PropertyType == typeof(DateTime))
            {
                PropertyInfo _rep = null;
                if (baseTypePropertiesByName.TryGetValue(fieldId + "_rep", out _rep))
                {
                    if (IsPublicGetProperty(_rep))
                    {
                        getMethod = _rep.GetGetMethod();
                        fieldType = E_PageDataFieldType.DateTime;
                    }

                    if (IsPublicSetProperty(_rep))
                    {
                        setMethod = _rep.GetSetMethod();
                    }
                }
            }
            else if (baseTypeProperty.PropertyType == typeof(Guid))
            {
                if (IsPublicGetProperty(baseTypeProperty))
                {
                    fieldType = E_PageDataFieldType.Guid;
                    getMethod = baseTypeProperty.GetGetMethod();
                }

                if (IsPublicSetProperty(baseTypeProperty))
                {
                    setMethod = baseTypeProperty.GetSetMethod();
                }
            }
            else if (baseTypeProperty.Name.EndsWith("Options") && baseTypeProperty.PropertyType.GetInterfaces().Contains(typeof(System.Collections.IEnumerable)))
            {
                if (IsPublicGetProperty(baseTypeProperty))
                {
                    fieldType = E_PageDataFieldType.Array;
                    getMethod = baseTypeProperty.GetGetMethod();
                }

                if (IsPublicSetProperty(baseTypeProperty))
                {
                    setMethod = baseTypeProperty.GetSetMethod();
                }
            }
            else if (baseTypeProperty.PropertyType.IsGenericType && baseTypeProperty.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                // 1/4/17 ml - For nullable value types, use the _rep property, which may have special handling.
                // If statement taken from https://msdn.microsoft.com/en-us/library/ms366789.aspx
                PropertyInfo _rep;
                if (baseTypePropertiesByName.TryGetValue(fieldId + "_rep", out _rep))
                {
                    if (IsPublicGetProperty(_rep))
                    {
                        fieldType = E_PageDataFieldType.String;
                        getMethod = _rep.GetGetMethod();
                    }

                    if (IsPublicSetProperty(_rep))
                    {
                        setMethod = _rep.GetSetMethod();
                    }
                }
            }

            if (getMethod == null || fieldType == E_PageDataFieldType.Unknown)
            {
                return null;
            }

            return new _PageDataFieldItem()
            {
                ClassType = dataClassType,
                FieldType = fieldType,
                EnumerationType = enumType,
                InvokeGetMethod = getMethod,
                InvokeSetMethod = setMethod,
                CustomInputModel = GetLqbInputModel(baseTypeProperty)
            };
        }

        private static bool IsStringFieldWithRep(string fieldId)
        {
            return fieldId == "sCustomPMLField1"
                || fieldId == "sCustomPMLField2"
                || fieldId == "sCustomPMLField3"
                || fieldId == "sCustomPMLField4"
                || fieldId == "sCustomPMLField5"
                || fieldId == "sCustomPMLField6"
                || fieldId == "sCustomPMLField7"
                || fieldId == "sCustomPMLField8"
                || fieldId == "sCustomPMLField9"
                || fieldId == "sCustomPMLField10"
                || fieldId == "sCustomPMLField11"
                || fieldId == "sCustomPMLField12"
                || fieldId == "sCustomPMLField13"
                || fieldId == "sCustomPMLField14"
                || fieldId == "sCustomPMLField15"
                || fieldId == "sCustomPMLField16"
                || fieldId == "sCustomPMLField17"
                || fieldId == "sCustomPMLField18"
                || fieldId == "sCustomPMLField19"
                || fieldId == "sCustomPMLField20";
        }

        /// <summary>
        /// Determines the type of a string field from the get method's IL.
        /// </summary>
        /// <param name="getMethod">The get method.</param>
        /// <returns>The type for the string field.</returns>
        private static E_PageDataFieldType GetFieldTypeForStringField(MethodInfo getMethod)
        {
            E_PageDataFieldType fieldType = E_PageDataFieldType.String;
            MethodBody methodBody = getMethod.GetMethodBody();
            if (null != methodBody)
            {
                var instructionList = MSILParser.Parse(methodBody.GetILAsByteArray());
                Module module = getMethod.DeclaringType.Module;
                foreach (var instruction in instructionList)
                {
                    if (instruction.OpCode == OpCodes.Call || instruction.OpCode == OpCodes.Callvirt)
                    {
                        MethodBase m = module.ResolveMethod(instruction.MetadataToken);
                        if (m.Name == "GetPhoneNumberField")
                        {
                            fieldType = E_PageDataFieldType.Phone;
                            break;
                        }
                        else if (m.Name == "GetSsnField" || m.Name == "GetEncryptedSsnField")
                        {
                            fieldType = E_PageDataFieldType.Ssn;
                            break;
                        }
                    }
                }
            }

            return fieldType;
        }

        /// <summary>
        /// Determines the field type for a decimal field based on the field id or the get method's IL.
        /// </summary>
        /// <param name="fieldId">The field id.</param>
        /// <param name="getMethod">The get method for the _rep.</param>
        /// <returns>The field type.</returns>
        /// <exception cref="CBaseException">
        /// Thrown when there is more than a single expected method call or there is an unsupported 
        /// method call.
        /// </exception>
        private static E_PageDataFieldType GetFieldTypeForDecimalField(string fieldId, MethodInfo getMethod)
        {
            E_PageDataFieldType fieldType = E_PageDataFieldType.Unknown;

            if (fieldId == "sPurchaseAdviceTotalPrice_Field1"
                || fieldId == "sPurchaseAdviceTotalPrice_Field1_Purchasing"
                || fieldId == "sPurchaseAdviceNetPrice_Field1"
                || fieldId == "sPurchaseAdviceNetPrice_Field1_Purchasing"
                || fieldId == "sPurchaseAdviceBasePrice_Field1"
                || fieldId == "sPurchaseAdviceBasePrice_Field1_Purchasing")
            {
                fieldType = E_PageDataFieldType.Decimal;
            }
            else if (fieldId == "sQMAveragePrimeOfferR" || fieldId == "sQMQualBottom")
            {
                fieldType = E_PageDataFieldType.Percent;
            }
            else if (fieldId == "sQMDiscntBuyDownR")
            {
                fieldType = E_PageDataFieldType.Percent;
            }
            else
            {
                MethodBody methodBody = getMethod.GetMethodBody();

                string[] ignoreMethodsName =
                {
                    "get_IsRunningPricingEngine",
                    "get_BorrowerModeT",
                    "get_sProdEstimatedResidualI",
                    "get_sPurchaseAdviceBasePrice_Field1",
                    "Format",
                    "get_sPurchaseAdviceNetPrice_Field1",
                    "get_sPurchaseAdviceTotalPrice_Field1",
                    "get_sPurchaseAdviceBasePrice_Field1_Purchasing",
                    "get_sPurchaseAdviceNetPrice_Field1_Purchasing",
                    "get_sPurchaseAdviceTotalPrice_Field1_Purchasing"
                };

                if (methodBody != null)
                {
                    var instructionList = MSILParser.Parse(methodBody.GetILAsByteArray());
                    Module module = getMethod.DeclaringType.Module;
                    bool hasPreviousMethod = false;
                    foreach (var instruction in instructionList)
                    {
                        // 5/14/2010 dd - The _rep method should only have one method call to
                        // convert the decimal to a string. If there are more than one method invoke
                        // then it will throw exception and we will look at the case.
                        if (instruction.OpCode == OpCodes.Call || instruction.OpCode == OpCodes.Callvirt)
                        {
                            if (hasPreviousMethod)
                            {
                                throw new CBaseException(ErrorMessages.Generic, fieldId + "_rep has more than 1 method call in them.");
                            }

                            hasPreviousMethod = true;
                            MethodBase m = module.ResolveMethod(instruction.MetadataToken);
                            if (m.Name == "ToRateStringNoPercent"
                                || m.Name == "ToRateString"
                                || m.Name == "ToRateString4DecimalDigits"
                                || m.Name == "ToRateString6DecimalDigits"
                                || m.Name == "ToRateString9DecimalDigits")
                            {
                                fieldType = E_PageDataFieldType.Percent;
                            }
                            else if (m.Name == "ToMoneyString"
                                || m.Name == "GetMoneyVarCharField_rep"
                                || m.Name == "ToMoneyString4DecimalDigits"
                                || m.Name == "ToMoneyString6DecimalDigits"
                                || m.Name == "ToMoneyStringRoundAwayFromZero")
                            {
                                fieldType = E_PageDataFieldType.Money;
                            }
                            else if (m.Name.StartsWith("ToDecimalString")
                                || m.Name == "get_sPurchaseAdviceNetPrice_Field1"
                                || m.Name == "get_sPurchaseAdviceNetPrice_Field1_Purchasing")
                            {
                                fieldType = E_PageDataFieldType.Decimal;
                            }
                            else if (ignoreMethodsName.Contains(m.Name))
                            {
                                hasPreviousMethod = false;
                            }
                            else
                            {
                                Console.WriteLine(fieldId + "_rep contains unhandle method call. Method=" + m.Name);
                                throw new CBaseException(ErrorMessages.Generic, fieldId + "_rep contains unhandle method call. Method=" + m.Name);
                            }
                        }
                    }
                }
            }

            return fieldType;
        }


        private static bool IsPublicGetProperty(PropertyInfo prop)
        {
            if (null == prop)
            {
                return false;
            }
            if (prop.CanRead)
            {
                MethodInfo m = prop.GetGetMethod();
                if (null != m)
                {
                    return m.IsPublic;
                }
            }
            return false;
        }
        private static bool IsPublicSetProperty(PropertyInfo prop)
        {
            if (null == prop)
            {
                return false;
            }
            if (prop.CanWrite)
            {
                MethodInfo m = prop.GetSetMethod();
                if (null != m)
                {
                    return m.IsPublic;
                }
            }
            return false;
        }
        public static bool GetFieldType(string fieldId, out E_PageDataFieldType fieldType)
        {
            fieldType = E_PageDataFieldType.Unknown;

            _PageDataFieldItem item = FindDataFieldItem(fieldId);

            if (item == null)
            {
                return false;
            }
            fieldType = item.FieldType;
            return true;
        }

        public static bool GetFieldEnumType(string fieldId, out Type enumType)
        {
            enumType = null;

            _PageDataFieldItem item = FindDataFieldItem(fieldId);

            if (item == null)
            {
                return false;
            }
            bool isFound = false;

            if (item.FieldType == E_PageDataFieldType.Enum)
            {
                isFound = true;
                enumType = item.EnumerationType;
            }
            return isFound;

        }

        public static bool GetPageDataClassType(string fieldId, out E_PageDataClassType dataClassType)
        {
            dataClassType = E_PageDataClassType.CBasePage;


            if (fieldId.StartsWith("today+", StringComparison.OrdinalIgnoreCase) || fieldId.StartsWith("today-", StringComparison.OrdinalIgnoreCase))
            {
                // 12/20/2011 dd - OPM 75432 We support the following special keyword  day+N  or day-N
                return false;
            }

            if (fieldId.StartsWith(DataPath.PathHeader, StringComparison.OrdinalIgnoreCase))
            {
                // 04/03/2018 je - OPM 468746 - Data paths aren't handled/parsed through page data utilities.
                return false;
            }

            _PageDataFieldItem item = FindDataFieldItem(fieldId);

            if (item == null)
            {
                return false;
            }
            dataClassType = item.ClassType;
            return true;
        }
        private static _PageDataFieldItem FindDataFieldItem(string fieldId)
        {
            if (string.IsNullOrEmpty(fieldId))
            {
                return null;
            }

            _PageDataFieldItem item = null;
            _FieldNameStructure fieldName = new _FieldNameStructure(fieldId);
            if (s_fieldHash.TryGetValue(fieldName.ActualId, out item) == false)
            {
                item = null;
            }
            return item;
        }

        public static bool ContainsField(string fieldId)
        {
            if (string.IsNullOrEmpty(fieldId))
            {
                return false;
            }

            _FieldNameStructure s = new _FieldNameStructure(fieldId);

            return s_fieldHash.ContainsKey(s.ActualId);
        }

        public static bool ContainsBooleanField(string fieldId)
        {
            E_PageDataFieldType fieldType;
            if (PageDataUtilities.GetFieldType(fieldId, out fieldType) && fieldType == E_PageDataFieldType.Bool)
            {
                return true;
            }

            _FieldNameStructure s = new _FieldNameStructure(fieldId);
            Tuple<PropertyInfo, MethodInfo, MethodInfo> property = null;
            if (s.ActualId.Equals("GetPreparerOfForm", StringComparison.OrdinalIgnoreCase))
            {
                return s_preparerHash.TryGetValue(s.SubItemName, out property) && property.Item1.PropertyType == typeof(bool);
            }

            if (s.ActualId.Equals("GetAgentOfRole", StringComparison.OrdinalIgnoreCase))
            {
                return s_agentHash.TryGetValue(s.SubItemName, out property) && property.Item1.PropertyType == typeof(bool);
            }

            return false;
        }

        public static bool ContainsEnumField(string fieldId)
        {
            E_PageDataFieldType fieldType;
            if (PageDataUtilities.GetFieldType(fieldId, out fieldType) && fieldType == E_PageDataFieldType.Enum)
            {
                return true;
            }

            _FieldNameStructure s = new _FieldNameStructure(fieldId);
            Tuple<PropertyInfo, MethodInfo, MethodInfo> property = null;
            if (s.ActualId.Equals("GetPreparerOfForm", StringComparison.OrdinalIgnoreCase))
            {
                return s_preparerHash.TryGetValue(s.SubItemName, out property) && property.Item1.PropertyType.IsEnum;
            }

            if (s.ActualId.Equals("GetAgentOfRole", StringComparison.OrdinalIgnoreCase))
            {
                return s_agentHash.TryGetValue(s.SubItemName, out property) && property.Item1.PropertyType.IsEnum;
            }

            return false;
        }

        public static string GetDependencyFieldIdName(string fieldId)
        {
            if (string.IsNullOrEmpty(fieldId))
            {
                return string.Empty;
            }
            _FieldNameStructure name = new _FieldNameStructure(fieldId);
            if (name.ActualId.EndsWith("_rep"))
            {
                return name.ActualId.Substring(0, name.ActualId.Length - 4);
            }
            else if (name.ActualId.Equals("GetInvestorInfo", StringComparison.OrdinalIgnoreCase))
            {
                return "sfGetInvestorInfo";
            }
            else{
                return name.ActualId;
            }
        }

        /// <summary>
        /// This method will return a valid field id as declare in data layer.
        /// Example: sLAmt will return sLAmtCalc because sLAmtCalc is how data layer define it.
        /// </summary>
        /// <param name="fieldId"></param>
        /// <returns>Return empty string if field id is invalid.</returns>
        public static string GetActualFieldId(string fieldId)
        {
            if (string.IsNullOrEmpty(fieldId)) {
                return string.Empty;
            }
            string ret = string.Empty;

            string actualFieldId;

            if (!s_alternateFieldNameDictionary.TryGetValue(fieldId, out actualFieldId))
            {
                // 5/21/2010 dd - There is no alternate name for this field id.
                actualFieldId = fieldId;
            }

            if (ContainsField(actualFieldId))
            {
                ret = actualFieldId;
            }
            return ret;
        }

        public static E_PageDataClassType? GetDataClassType(string fieldId)
        {
            _PageDataFieldItem item = null;
            if (s_fieldHash.TryGetValue(fieldId, out item))
            {
                return item.ClassType;
            }
            else
            {
                return null;
            }
        }

        public static string GetValue(CPageData dataLoan, CAppData dataApp, string fieldId)
        {
            if (string.IsNullOrEmpty(fieldId))
            {
                return "";
            }

            _FieldNameStructure s = new _FieldNameStructure(fieldId);

            _PageDataFieldItem item = null;
            if (s.ActualId.Equals("GetPreparerOfForm", StringComparison.OrdinalIgnoreCase))
            {
                E_PreparerFormT form = (E_PreparerFormT)Enum.Parse(typeof(E_PreparerFormT), s.ArrayIndex);
                IPreparerFields preparer = dataLoan.GetPreparerOfForm(form, E_ReturnOptionIfNotExist.ReturnEmptyObject);

                Tuple<PropertyInfo,MethodInfo,MethodInfo> property = null;
                //// Item2 is the property getter, should not be null
                if (s_preparerHash.TryGetValue(s.SubItemName, out property))
                {
                    PropertyInfo propInfo = property.Item1;
                    MethodInfo getMethod = property.Item2;
                    if (getMethod != null)
                    {
                        if (propInfo.PropertyType == typeof(string))
                        {
                            return (string)getMethod.Invoke(preparer, null);
                        }
                        else if (propInfo.PropertyType == typeof(E_AgentRoleT))
                        {
                            return GetEnumValue((Enum)getMethod.Invoke(preparer, null), s.EnumValue);
                        }
                        else if (propInfo.Name == nameof(preparer.IsLocked))
                        {
                            return GetBoolValue((bool)getMethod.Invoke(preparer, null), s.EnumValue, dataLoan.GetFormatTarget());
                        }
                    }
                }
                throw new CBaseException(ErrorMessages.Generic, fieldId + " not found.");

            }
            else if (s.ActualId.Equals("GetAgentOfRole", StringComparison.OrdinalIgnoreCase) || s.ActualId.Equals("GetAgentOfRoleNoFallback", StringComparison.OrdinalIgnoreCase))
            {
                E_AgentRoleT role = (E_AgentRoleT)Enum.Parse(typeof(E_AgentRoleT), s.ArrayIndex);
                E_ReturnOptionIfNotExist returnOption = E_ReturnOptionIfNotExist.ReturnEmptyObject;

                if (s.ActualId.Equals("GetAgentOfRoleNoFallback", StringComparison.OrdinalIgnoreCase))
                {
                    returnOption = E_ReturnOptionIfNotExist.CreateNewDoNotFallBack;
                }

                CAgentFields agent = dataLoan.GetAgentOfRole(role, returnOption);
                Tuple<PropertyInfo, MethodInfo, MethodInfo> property = null;
                //// Item2 is the property getter, should not be null
                if (s_agentHash.TryGetValue(s.SubItemName, out property))
                {
                    PropertyInfo propInfo = property.Item1;
                    MethodInfo getMethod = property.Item2;
                    if (getMethod != null)
                    {
                        if (propInfo.PropertyType == typeof(string))
                        {
                            return (string)getMethod.Invoke(agent, null);
                        }
                    }
                }

                throw new CBaseException(ErrorMessages.Generic, fieldId + " not found.");
            }
            else if (s.ActualId.Equals("GetInvestorInfo", StringComparison.OrdinalIgnoreCase))
            {
                return GetInvestorRolodexProperty(s.ArrayIndex, dataLoan.GetInvestorInfo(), fieldId);
            }
            else if (s.ActualId.Equals("GetSubservicerInfo", StringComparison.OrdinalIgnoreCase))
            {
                return GetInvestorRolodexProperty(s.ArrayIndex, dataLoan.GetSubservicerInfo(), fieldId);
            }
            else if (s_fieldHash.TryGetValue(s.ActualId, out item))
            {
                object obj = null;
                switch (item.ClassType)
                {
                    case E_PageDataClassType.CBasePage:
                        obj = dataLoan;
                        break;
                    case E_PageDataClassType.CAppBase:
                        obj = dataApp;
                        break;
                    default:
                        throw new UnhandledEnumException(item.ClassType);
                }
                object result = item.InvokeGetMethod.Invoke(obj, null);
                if (item.FieldType == E_PageDataFieldType.Enum)
                {
                    return GetEnumValue((Enum)result, s.EnumValue);
                }
                else if (item.FieldType == E_PageDataFieldType.Bool)
                {
                    return GetBoolValue((bool)result, s.EnumValue, dataLoan.GetFormatTarget());
                }
                else if (item.FieldType == E_PageDataFieldType.SensitiveString)
                {
                    var sensitive = (Sensitive<string>)result;
                    return sensitive.Value;
                }
                else
                {
                    //OPM 122256: return an empty string instead of throwing an NRE when result is null.
                    if (result == null) return "";
                    return result.ToString();
                }
            }

            throw new CBaseException(ErrorMessages.Generic, fieldId + " not found.");
        }

        private static string GetBoolValue(bool value, string enumValue, FormatTarget target)
        {
            if (target == FormatTarget.XsltExport)
            {
                return LendersOffice.XsltExportReport.XsltExportWorker.GetBoolString(value);
            }
            else if (target == FormatTarget.LqbDataService)
            {
                return value.ToString();
            }
            else if (!string.IsNullOrEmpty(enumValue))
            {
                return (value ? "1" : "0") == enumValue ? "Yes" : "No";
            }
            else
            {
                return value ? "Yes" : "No";
            }
        }

        private static string GetEnumValue(Enum result, string enumValue)
        {
            string value = result.ToString("D"); // 5/17/2010 dd - For enumeration we want to return the numeric value.

            if (!string.IsNullOrEmpty(enumValue))
            {
                return value == enumValue ? "Yes" : "No";
            }
            return value;
        }

        public static IEnumerable<KeyValuePair<string,string>> GetArrayValue(CPageData dataLoan, CAppData dataApp, string fieldId)
        {
            if (string.IsNullOrEmpty(fieldId))
            {
                return null;
            }

            _FieldNameStructure s = new _FieldNameStructure(fieldId);

            _PageDataFieldItem item = null;
            if (s_fieldHash.TryGetValue(s.ActualId, out item))
            {
                object obj = null;
                switch (item.ClassType)
                {
                    case E_PageDataClassType.CBasePage:
                        obj = dataLoan;
                        break;
                    case E_PageDataClassType.CAppBase:
                        obj = dataApp;
                        break;
                    default:
                        throw new UnhandledEnumException(item.ClassType);
                }
                object result = item.InvokeGetMethod.Invoke(obj, null);
                if (item.FieldType == E_PageDataFieldType.Array)
                {
                    IEnumerable<KeyValuePair<string, string>> parsedResult = null;
                    try
                    {
                        parsedResult = ((System.Collections.IEnumerable)result).Cast<Enum>().Select(en => EnumUtilities.KeyValuePairFromEnum(en));

                        if (parsedResult.Count() == 0) return null;
                    }
                    catch (InvalidCastException)
                    {
                        try
                        {
                            parsedResult = ((System.Collections.IEnumerable)result).Cast<string>().Select(x => new KeyValuePair<string, string>(x, null));
                            if (parsedResult.Count() == 0) return null;
                        }
                        catch (InvalidCastException)
                        {
                            return null;
                        }
                    }

                    return parsedResult;
                }
                else
                {
                    return null;
                }
            }

            throw new CBaseException(ErrorMessages.Generic, fieldId + " not found.");
        }

        /// <summary>
        /// This method will check if a loan field can be binded.
        /// </summary>
        /// <param name="fieldId">The id of the checking field</param>
        /// <returns>Return true if field id is bindable.</returns>
        public static bool IsBindable(string fieldId)
        {
            if (string.IsNullOrEmpty(fieldId))
            {
                return false;
            }

            _FieldNameStructure s = new _FieldNameStructure(fieldId);

            _PageDataFieldItem item = null;

            if (s_fieldHash.TryGetValue(s.ActualId, out item))
            {
                return item.InvokeSetMethod != null;
            }

            return false;
        }

        /// <summary>
        /// This method will check if a loan field is read only.
        /// </summary>
        /// <param name="dataLoan">Loan data object of the checking field</param>
        /// <param name="dataApp">Application data object of the checking field</param>
        /// <param name="fieldId">The id of the checking field</param>
        /// <returns>Return empty string if field id is invalid.</returns>
        public static bool IsReadOnly(CPageData dataLoan, CAppData dataApp, string fieldId)
        {
            if (string.IsNullOrEmpty(fieldId))
            {
                return false;
            }

            _FieldNameStructure s = new _FieldNameStructure(fieldId);

            _PageDataFieldItem item;
            if (!s_fieldHash.TryGetValue(s.ActualId, out item))
            {
                throw new CBaseException(ErrorMessages.Generic, fieldId + " not found.");
            }

            string checkingFieldId = s.ActualId;
            bool isBorOrCoborField = s.ActualId.StartsWith("aB") || s.ActualId.StartsWith("aC");

            checkingFieldId = !isBorOrCoborField ? s.ActualId : s.ActualId.Remove(1, 1);

            //If fieldId has no setter THEN fieldId is readonly
            if (item.InvokeSetMethod == null)
            {
                return true;
            }

            //If there is a {fieldid}ReadOnly AND {fieldId}ReadOnly == true THEN fieldId is readonly
            {
                var readOnlyFieldId =
                    fieldId == nameof(dataLoan.sMInsRsrvEscrowedTri)
                        ? nameof(dataLoan.sIssMInsRsrvEscrowedTriReadOnly)
                        : checkingFieldId + "ReadOnly";
                _PageDataFieldItem ignoredItem;
                if (s_fieldHash.TryGetValue(readOnlyFieldId, out ignoredItem) &&
                    GetValue(dataLoan, dataApp, readOnlyFieldId) == "Yes")
                {
                    return true;
                }
            }

            // If there is a {fieldid}Lckd AND {fieldid}Lckd == false THEN fieldId is readonly.
            {
                string lockedFieldId = GetLockedField(s.ActualId);
                _PageDataFieldItem ignoredItem;
                if (!string.IsNullOrEmpty(lockedFieldId) && s_fieldHash.TryGetValue(lockedFieldId, out ignoredItem) &&
                    GetValue(dataLoan, dataApp, lockedFieldId) == "No")
                {
                    return true;
                }
            }

            // Manual checking fieldId is readonly or not.
            {
                if (fieldId == nameof(dataLoan.sPurchPrice))
                {
                    if (!(new[]
                        {
                            E_sLPurposeT.Purchase,
                            E_sLPurposeT.Construct,
                            E_sLPurposeT.ConstructPerm,
                        }.Contains(dataLoan.sLPurposeT)))
                    {
                        return true;
                    }
                }
                else if (fieldId == nameof(dataLoan.sIsQRateIOnly))
                {
                    if (dataLoan.sQualIR <= 0)
                    {
                        return true;
                    }
                }
                else if (fieldId == nameof(dataLoan.sOptionArmTeaserR))
                {
                    if (dataLoan.sIsRateLocked || !dataLoan.sIsOptionArm)
                    {
                        return true;
                    }
                }
                else if (fieldId == nameof(dataLoan.sDownPmtPc) ||
                         fieldId == nameof(dataLoan.sEquityCalc))
                {
                    if (dataLoan.sLAmtLckd)
                    {
                        return true;
                    }
                }
                else if (fieldId == nameof(dataLoan.sApprPmtMethodDesc))
                {
                    if (!new[]{ E_sApprPmtMethodT.Bill, E_sApprPmtMethodT.Other }.Contains(dataLoan.sApprPmtMethodT))
                    {
                        return true;
                    }
                }
            }

            // 11/17/2015 - Perform field write protection.
            return dataLoan.HasWriteLoanPermission
                ? dataLoan.IsInFieldProtection(fieldId)
                : dataLoan.IsInFieldWrite(fieldId) == false;
        }

        /// <summary>
        /// This method will check if a loan field is not able to be shown.
        /// </summary>
        /// <param name="dataLoan">Loan data object of the checking field</param>
        /// <param name="dataApp">Application data object of the checking field</param>
        /// <param name="fieldId">The id of the checking field</param>
        /// <returns>Return true if field id is invisible. Otherwise return false.</returns>
        public static bool IsInvisible(CPageData dataLoan, CAppData dataApp, string fieldId)
        {
            if (string.IsNullOrEmpty(fieldId))
            {
                return false;
            }

            _FieldNameStructure s = new _FieldNameStructure(fieldId);

            _PageDataFieldItem item;

            if (!s_fieldHash.TryGetValue(s.ActualId, out item))
            {
                throw new CBaseException(ErrorMessages.Generic, fieldId + " not found.");
            }

            bool isBorOrCoborField = s.ActualId.StartsWith("aB") || s.ActualId.StartsWith("aC");
            string checkingFieldId = !isBorOrCoborField ? s.ActualId : s.ActualId.Remove(1, 1);

            //If fieldId  {fieldid}Visible and {fieldid}Visible == false, field is invisible.
            string visibleFieldId = checkingFieldId + "Visible";
            _PageDataFieldItem ignoredItem;
            if (s_fieldHash.TryGetValue(visibleFieldId, out ignoredItem) &&
                GetValue(dataLoan, dataApp, visibleFieldId) == "No")
            {
                return true;
            }

            if (fieldId == nameof(dataLoan.sOriginalAppraisedValue) &&
                !(dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance && dataLoan.sFHAPurposeIsStreamlineRefiWithoutAppr))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Check a field is locked.
        /// </summary>
        /// <param name="fieldId">Field Id input.</param>
        /// <returns>Return string if the field is lock type.</returns>
        public static bool IsLockField(string fieldId)
        {
            return  fieldId.EndsWith("Lckd") || (fieldId == "sTotCcPbsLocked");
        }

        /// <summary>
        /// Get locked field id.
        /// </summary>
        /// <param name="fieldId">Field Id input.</param>
        /// <returns>Locked field Id.</returns>
        private static string GetLockedField (string fieldId)
        {
            bool isBorOrCoborField = fieldId.StartsWith("aB") || fieldId.StartsWith("aC");

            if (isBorOrCoborField)
            {
                return fieldId.Remove(1, 1) + "Lckd";
            }
            else if ("sTotCcPbs" == fieldId)
            {
                return "sTotCcPbsLocked";
            }
            else if ("sOCredit1Amt" == fieldId)
            {
                return "sOCredit1Lckd";
            }
            else if ("sOCredit1Desc" == fieldId)
            {
                return "sOCredit1Lckd";
            }
            else if ("sLAmtCalc" == fieldId)
            {
                return "sLAmtLckd";
            }
            else if ("sTotEstCcNoDiscnt1003" == fieldId)
            {
                return "sTotEstCc1003Lckd";
            }
            // 12/8/2015 huyn: temporarily disable sSpGrossRentLckd because sSpGrossRentLckd is deprecated by case 73986.
            else if ("sSpGrossRent" == fieldId)
            {
                return string.Empty;
            }
            else if (fieldId == "sNMLSServicingIntentT")
            {
                return "sNMLSServicingIntentLckd";
            }
            else if (fieldId == "sNMLSPeriodForDaysDelinquentT")
            {
                return "sNMLSPeriodForDaysDelinquentLckd";
            }

            return fieldId + "Lckd";
        }

        private static string GetInvestorRolodexProperty(string sFieldName, InvestorRolodexEntry investorInfo, string sFieldId)
        {
            PropertyInfo prop = typeof(InvestorRolodexEntry).GetProperty(sFieldName, BindingFlags.Instance | BindingFlags.Public);

            if (null != prop && (prop.PropertyType == typeof(string)))
            {
                MethodInfo info = prop.GetGetMethod();
                if (info != null)
                {
                    return (string)info.Invoke(investorInfo, null);
                }
            }
            throw CBaseException.GenericException(sFieldId + " not found.");
        }

        public static void SetValue(CPageData dataLoan, CAppData dataApp, string fieldId, string value)
        {
            if (string.IsNullOrEmpty(fieldId))
            {
                return;
            }

            _FieldNameStructure s = new _FieldNameStructure(fieldId);

            _PageDataFieldItem item = null;
            if (s.ActualId.Equals("GetPreparerOfForm", StringComparison.OrdinalIgnoreCase))
            {

                E_PreparerFormT form = (E_PreparerFormT)Enum.Parse(typeof(E_PreparerFormT), s.ArrayIndex);
                IPreparerFields preparer = dataLoan.GetPreparerOfForm(form, E_ReturnOptionIfNotExist.CreateNew);

                Tuple<PropertyInfo, MethodInfo, MethodInfo> property = null;
                //// Item3 is the property setter, should not be null
                if (s_preparerHash.TryGetValue(s.SubItemName, out property))
                {
                    PropertyInfo propInfo = property.Item1;
                    MethodInfo setMethod = property.Item3;
                    if (setMethod != null)
                    {
                        if (propInfo.PropertyType == typeof(string))
                        {
                            setMethod.Invoke(preparer, new object[] { value });
                            preparer.Update();
                            return;
                        }
                        else if (propInfo.PropertyType == typeof(E_AgentRoleT))
                        {
                            setMethod.Invoke(preparer, new object[] { SetEnumValue(typeof(E_AgentRoleT), value, fieldId) });
                            preparer.Update();
                            return;
                        }
                        else if (propInfo.Name == nameof(preparer.IsLocked))
                        {
                            setMethod.Invoke(preparer, new object[] { SetBoolValue(value, fieldId) });
                            preparer.Update();
                            return;
                        }
                    }
                }
                throw new NotFoundException(ErrorMessages.Generic, fieldId + " not found.");

            }
            else if (s.ActualId.Equals("GetAgentOfRole", StringComparison.OrdinalIgnoreCase) || s.ActualId.Equals("GetAgentOfRoleNoFallback", StringComparison.OrdinalIgnoreCase))
            {
                E_AgentRoleT role = (E_AgentRoleT)Enum.Parse(typeof(E_AgentRoleT), s.ArrayIndex);
                E_ReturnOptionIfNotExist option = E_ReturnOptionIfNotExist.CreateNew;

                if (s.ActualId.Equals("GetAgentOfRoleNoFallback", StringComparison.OrdinalIgnoreCase))
                {
                    option = E_ReturnOptionIfNotExist.CreateNewDoNotFallBack;
                }

                CAgentFields agent = dataLoan.GetAgentOfRole(role, option);

                Tuple<PropertyInfo, MethodInfo, MethodInfo> property = null;
                //// Item3 is the property setter, should not be null
                if (s_agentHash.TryGetValue(s.SubItemName, out property))
                {
                    PropertyInfo propInfo = property.Item1;
                    MethodInfo setMethod = property.Item3;
                    if (setMethod != null && propInfo.PropertyType == typeof(string))
                    {
                        setMethod.Invoke(agent, new object[] { value });
                        agent.Update();
                        return;
                    }
                }
                throw new NotFoundException(ErrorMessages.Generic, fieldId + " not found.");

            }
            else if (s_fieldHash.TryGetValue(s.ActualId, out item))
            {
                object obj = null;
                switch (item.ClassType)
                {
                    case E_PageDataClassType.CBasePage:
                        obj = dataLoan;
                        break;
                    case E_PageDataClassType.CAppBase:
                        obj = dataApp;
                        break;
                    default:
                        throw new UnhandledEnumException(item.ClassType);
                }

                object[] parameters = new object[1];
                if (item.FieldType == E_PageDataFieldType.Enum)
                {
                    parameters[0] = SetEnumValue(item.EnumerationType, value, fieldId);
                }
                else if (item.FieldType == E_PageDataFieldType.Bool)
                {
                    parameters[0] = SetBoolValue(value, fieldId);    
                }
                else if (item.FieldType == E_PageDataFieldType.Guid) // 2/26/2014 IR - Parse string value to Guid
                {
                    try
                    {
                        parameters[0] = new Guid(value);
                    }
                    catch (FormatException)
                    {
                        throw new FieldInvalidValueException(fieldId, value);
                    }
                }
                else if (item.FieldType == E_PageDataFieldType.SensitiveString)
                {
                    // Invoking the set method via reflection will not perform
                    // the implicit case from T to Sensitive<T>.
                    parameters[0] = new Sensitive<string>(value);
                }
                else
                {
                    parameters[0] = value;
                }

                if (item.InvokeSetMethod != null && obj != null)
                {
                    item.InvokeSetMethod.Invoke(obj, parameters);
                }
                else
                {
                    throw new NotFoundException(ErrorMessages.Generic, fieldId + " not found.");
                }
            }
            else
            {

                throw new NotFoundException(ErrorMessages.Generic, fieldId + " not found.");
            }

        }

        private static bool SetBoolValue(string value, string fieldId)
        {
            // 28/8/2015, huyn: should be able to parse Yes/No string instead of only true/false because GetActualFieldId return Yes/No
            bool b;
            if (bool.TryParse(value, out b) == false)
            {
                if (value.ToLower() == "yes")
                {
                    b = true;
                }
                else if (value.ToLower() == "no")
                {
                    b = false;
                }
                else
                {
                    throw new FieldInvalidValueException(fieldId, value);
                }
            }
            return b;
        }

        private static object SetEnumValue(Type enumerationType, string value, string fieldId)
        {
            object parameter = null;
            try
            {
                parameter = Enum.Parse(enumerationType, value);
                if (Enum.IsDefined(enumerationType, parameter) == false)
                {
                    throw new FieldInvalidValueException(fieldId, value);
                }
            }
            catch (ArgumentException)
            {
                throw new FieldInvalidValueException(fieldId, value);
            }

            return parameter;
        }

        public static bool ValidFieldNameFormat(string fieldId)
        {
            if (string.IsNullOrEmpty(fieldId))
                return false;

            Match match = s_fieldNameStructureRegEx.Match(fieldId);
            return match.Success;
        }

        internal static PageDataFieldInfo GetFieldInfo(string fieldId)
        {
            if (string.IsNullOrEmpty(fieldId))
            {
                return null;
            }

            _PageDataFieldItem item = null;
            _FieldNameStructure fieldName = new _FieldNameStructure(fieldId);
            string key = fieldName.ActualId;
            if (s_fieldHash.TryGetValue(key, out item))
            {
                return CreatePageDataFieldInfo(key, item);
            }

            return null;
        }

        public static string[] GetSettableFieldsFromString(string fieldList)
        {
            if (string.IsNullOrEmpty(fieldList))
            {
                return new string[0];
            }
            else
            {
                string[] fields = fieldList.Split(new char[] { ',', ';', ' ', '\t', '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                Func<PageDataFieldInfo, bool> notNullAndHasSet = field => field != null && field.HasSetter;
                return new HashSet<string>(fields.Where(f => notNullAndHasSet(PageDataUtilities.GetFieldInfo(f))), StringComparer.OrdinalIgnoreCase).ToArray();
            }
        }

        public static IEnumerable<PageDataFieldInfo> GetAllFieldsFromPageData()
        {
            return s_fieldHash.Select(item => CreatePageDataFieldInfo(item.Key, item.Value));
        }

        private static PageDataFieldInfo CreatePageDataFieldInfo(string id, _PageDataFieldItem item)
        {
            return new PageDataFieldInfo(id, item.FieldType, item.EnumerationType, item.InvokeSetMethod != null, item.CustomInputModel);
        }
    }

    public class PageDataFieldInfo
    {
        public string Id { get; private set; }
        public E_PageDataFieldType FieldType { get; private set; }
        public Type EnumeratedType { get; private set; }
        public bool HasSetter { get; private set; }
        public LqbInputModel CustomInputModel { get; private set; }

        public PageDataFieldInfo(string id, E_PageDataFieldType fieldType, Type enumType, bool hasSetter, LqbInputModel customInputModel)
        {
            Id = id;
            FieldType = fieldType;
            EnumeratedType = enumType;
            HasSetter = hasSetter;
            CustomInputModel = customInputModel;
        }
    }


}
