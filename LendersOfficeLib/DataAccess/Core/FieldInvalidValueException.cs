﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.CustomFormField;

namespace DataAccess
{
    public class FieldInvalidValueException : CBaseException
    {
        public FieldInvalidValueException(string fieldId, string value)
            : base("", "'" + value + "' is not valid value for " + fieldId)
        {
            string friendlyName = fieldId;

            FormField formField = FormField.RetrieveById(fieldId);
            if (null != formField)
            {
                friendlyName = formField.FriendlyName;
            }
            UserMessage = "'" + value + "' is not valid value for " + friendlyName;
            this.IsEmailDeveloper = false;

        }
    }
}
