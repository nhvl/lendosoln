﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using DataAccess.PathDispatch;
    using LendersOffice.Admin;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.Integration.DocumentVendor.LQBESignUpdate;
    using LendersOffice.Migration;
    using LendersOffice.ObjLib.Audit;
    using LendersOffice.ObjLib.CustomAttributes;
    using LendersOffice.ObjLib.CustomCocField;
    using LendersOffice.ObjLib.DatabaseMessageQueue;
    using LendersOffice.ObjLib.Disclosure;
    using LendersOffice.ObjLib.DocMagicLib;
    using LendersOffice.ObjLib.DocumentGeneration;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// Copied from LoanFileFields.cs, this partial was the region with the label "Disclosure Compliance Tracking - OPM 105770".
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "*", Justification = "Legacy code copied from LoanFileFields.cs.")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.LayoutRules", "*", Justification = "Legacy code copied from LoanFileFields.cs.")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "*", Justification = "Legacy code copied from LoanFileFields.cs.")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "*", Justification = "Legacy code copied from LoanFileFields.cs.")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.OrderingRules", "*", Justification = "Legacy code copied from LoanFileFields.cs.")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.ReadabilityRules", "*", Justification = "Legacy code copied from LoanFileFields.cs.")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.SpacingRules", "*", Justification = "Legacy code copied from LoanFileFields.cs.")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("LendingQBStyleCop.LendingQBCustomRules", "*", Justification = "Legacy code copied from LoanFileFields.cs.")]
    public partial class CPageBase
    {
        /// <summary>
        /// Whether the loan needs initial disclosure.
        /// </summary>
        [DependsOn]
        public bool sNeedInitialDisc
        {
            get { return GetBoolField("sNeedInitialDisc"); }
            set { SetBoolField("sNeedInitialDisc", value); }
        }

        /// <summary>
        /// Whether the loan needs re-disclosure.
        /// </summary>
        [DependsOn]
        public bool sNeedRedisc
        {
            get { return GetBoolField("sNeedRedisc"); }
            set { SetBoolField("sNeedRedisc", value); }
        }

        /// <summary>
        /// The type of disclosure needed.
        /// </summary>
        [DependsOn]
        public E_sDisclosureNeededT sDisclosureNeededT
        {
            get { return (E_sDisclosureNeededT)GetTypeIndexField("sDisclosureNeededT"); }
            set { SetTypeIndexField("sDisclosureNeededT", (int)value); }
        }

        [DevNote("Custom description for disclosure needed type, for custom CoC fields.")]
        [DependsOn(nameof(sDisclosureNeededTDescription))]
        public string sDisclosureNeededTDescription
        {
            get { return GetStringVarCharField("sDisclosureNeededTDescription"); }
            set { this.SetStringVarCharField("sDisclosureNeededTDescription", value); }
        }

        [DevNote("Indicates whether e-consent montoring information is enabled for the file.")]
        [DependsOn(nameof(sBrokerId))]
        protected virtual bool sIsEnableEConsentMonitoringAutomation => this.m_brokerDB.EnableEConsentMonitoringAutomation;

        /// <summary>
        /// A brief description of the type of disclosure needed.
        /// </summary>
        /// <remarks>
        /// There are two different representations of the sDisclosureNeededT enum
        /// value for custom reports. This field is the brief description. The detailed
        /// description is taken care of the way enums are usually handled in
        /// custom reports.
        /// </remarks>
        [DependsOn(nameof(sDisclosureNeededT))]
        public string sDisclosureNeededBrief
        {
            get
            {
                switch (sDisclosureNeededT)
                {
                    case E_sDisclosureNeededT.InitDisc_RESPAAppReceived:
                    case E_sDisclosureNeededT.InitDisc_LoanRegistered:
                    case E_sDisclosureNeededT.EConsentDec_PaperDiscReqd:
                    case E_sDisclosureNeededT.EConsentNotReceived_PaperDiscReqd:
                    case E_sDisclosureNeededT.InitDisc_RESPAAppReceivedVerbally:
                        return "Initial Disclosures";
                    case E_sDisclosureNeededT.APROutOfTolerance_RediscReqd:
                        return "APR Redisclosure";
                    case E_sDisclosureNeededT.CC_LoanAmtChanged:
                    case E_sDisclosureNeededT.CC_LoanLocked:
                    case E_sDisclosureNeededT.CC_LockExtended:
                    case E_sDisclosureNeededT.CC_Relocked:
                    case E_sDisclosureNeededT.CC_FloatDownLock:
                    case E_sDisclosureNeededT.CC_GFEChargeDiscountChanged:
                    case E_sDisclosureNeededT.CC_PropertyValueChanged:
                    case E_sDisclosureNeededT.CC_UWCreditScoreChanged:
                    case E_sDisclosureNeededT.CC_ProgramChanged:
                    case E_sDisclosureNeededT.CC_ImpoundChanged:
                    case E_sDisclosureNeededT.CC_PurchasePriceChanged:
                    case E_sDisclosureNeededT.Custom:
                        return "Changed Circumstance";
                    case E_sDisclosureNeededT.None:
                    case E_sDisclosureNeededT.AwaitingEConsent:
                        return "";
                    default:
                        Tools.LogWarning("Unhandled enum value for sDisclosureNeededT");
                        return "";
                }
            }
        }

        /// <summary>
        /// The date disclosure is due.
        /// </summary>
        /// <remarks>
        /// Once this field has a valid value, it cannot be extended. It
        /// can only be reset to null.
        /// </remarks>
        [DependsOn(nameof(sDisclosuresDueD_Old))]
        private CDateTime sDisclosuresDueD_Old
        {
            get { return GetDateTimeField("sDisclosuresDueD"); }
            set
            {
                if (sDisclosuresDueD_Old.IsValid && value.IsValid)
                {
                    return;
                }

                SetDateTimeField("sDisclosuresDueD", value);
            }
        }
        private string sDisclosuresDueD_Old_rep
        {
            get { return GetDateTimeField_rep("sDisclosuresDueD"); }
            set
            {
                if (sDisclosuresDueD_Old.IsValid && !string.IsNullOrEmpty(value))
                {
                    return;
                }

                SetDateTimeField_rep("sDisclosuresDueD", value);
            }
        }

        [DependsOn(nameof(sDisclosuresDueD_Old), nameof(sDisclosuresDueD_New), nameof(sLoanVersionT))]
        private bool sfMigrate_sDisclosuresDueD { get { return false; } }
        public void Migrate_sDisclosuresDueD()
        {
            if (!sDisclosuresDueD_Old.IsValid)
            {
                Tools.LogWarning("[DisclosureDueDMigration] Tried to migrate loan without disclosures due date.");
                return;
            }

            sDisclosuresDueD_New = sDisclosuresDueD_Old.AddWorkingBankDays(1,
                !LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V28_DontRollSundayHolidaysToMonday));
            sDisclosuresDueD_Old_rep = "";
        }

        [DependsOn]
        private CDateTime sDisclosuresDueD_New
        {
            get { return GetDateTimeField("sDisclosuresDueD_New"); }
            set
            {
                if (sDisclosuresDueD_New.IsValid && value.IsValid)
                {
                    return;
                }

                SetDateTimeField("sDisclosuresDueD_New", value);
            }
        }
        private string sDisclosuresDueD_New_rep
        {
            get { return GetDateTimeField_rep("sDisclosuresDueD_New"); }
            set
            {
                if (sDisclosuresDueD_New.IsValid && !string.IsNullOrEmpty(value))
                {
                    return;
                }

                SetDateTimeField_rep("sDisclosuresDueD_New", value);
            }
        }

        [DependsOn(nameof(sDisclosuresDueD), nameof(sDisclosuresDueD_New))]
        public CDateTime sDisclosuresDueD
        {
            get
            {
                // If we still have a valid value for the original field,
                // then the loan has NOT been migrated.
                if (sDisclosuresDueD_Old.IsValid)
                {
                    return sDisclosuresDueD_Old;
                }
                return sDisclosuresDueD_New;
            }
            set
            {
                // When loan is disclosed, we can clear both the old and new dates.
                if (!value.IsValid)
                {
                    sDisclosuresDueD_Old_rep = "";
                    sDisclosuresDueD_New_rep = "";
                }
                else if (!sDisclosuresDueD.IsValid)
                {
                    sDisclosuresDueD_New = value;
                }
                // If either of the dates is valid, we don't want to do anything.
            }
        }
        public string sDisclosuresDueD_rep
        {
            get
            {
                return sDisclosuresDueD.ToString(this.m_convertLos);
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    sDisclosuresDueD_Old_rep = "";
                    sDisclosuresDueD_New_rep = "";
                }
                else if (!sDisclosuresDueD.IsValid)
                {
                    sDisclosuresDueD = CDateTime.Create(value, m_convertLos);
                }
                // If either of them are valid, we don't want to update.
            }
        }

        [DependsOn(nameof(sDisclosuresDueD), nameof(sDisclosuresDueD_New), nameof(sLoanVersionT))]
        private bool sfIsConsentWithinThreeBusinessDays { get { return false; } }
        private bool IsConsentWithinThreeBusinessDays(DateTime consentReceived)
        {
            int days = 0;
            if (!sDisclosuresDueD.IsValid)
            {
                return false;
            }
            else if (sDisclosuresDueD_Old.IsValid)
            {
                days = 2;
            }
            else if (sDisclosuresDueD_New.IsValid)
            {
                days = 1;
            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, "Need to handle all components of sDisclosuresDueD.");
            }


            CDateTime compareDate = sDisclosuresDueD.AddWorkingBankDays(days,
                !LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V28_DontRollSundayHolidaysToMonday));

            return consentReceived.CompareTo(compareDate.DateTimeForComputation) < 0;
        }

        /// <summary>
        /// Whether EConsent has been received for every borrower across
        /// all applications.
        /// </summary>
        [DependsOn(nameof(CAppBase.aBEConsentReceivedD), nameof(CAppBase.aCEConsentReceivedD), nameof(CAppBase.aCIsValidNameSsn), nameof(CAppBase.aBEConsentDeclinedD), nameof(CAppBase.aCEConsentDeclinedD))]
        public bool sEConsentCompleted
        {
            get
            {
                // Cycle through all of the applications and check that
                // borrowers/coborrowers have valid E-ConsentReceived date
                // that is after the declined date.
                for (int i = 0; i < m_dataApps.Length; i++)
                {
                    var dataApp = GetAppData(i);
                    var aBEConsentReceivedD = dataApp.aBEConsentReceivedD;
                    var aBEConsentDeclinedD = dataApp.aBEConsentDeclinedD;
                    var aCEConsentReceivedD = dataApp.aCEConsentReceivedD;
                    var aCEConsentDeclinedD = dataApp.aCEConsentDeclinedD;

                    var haveBorrowerEConsent = aBEConsentReceivedD.IsValid &&
                        (!aBEConsentDeclinedD.IsValid ||
                        aBEConsentReceivedD.DateTimeForComputationWithTime.CompareTo(aBEConsentDeclinedD.DateTimeForComputationWithTime) > 0);

                    var haveCoborrowerEConsent = aCEConsentReceivedD.IsValid &&
                        (!aCEConsentDeclinedD.IsValid ||
                        aCEConsentReceivedD.DateTimeForComputationWithTime.CompareTo(aCEConsentDeclinedD.DateTimeForComputationWithTime) > 0);

                    if (!haveBorrowerEConsent || (dataApp.aCIsValidNameSsn && !haveCoborrowerEConsent))
                    {
                        return false;
                    }
                }

                return true;
            }
        }
        /// <summary>
        /// Needs to be set when we upload documents to EDocs that are ESigned.
        /// Default for new loans is NO.
        /// Default for existing is Undetermined.
        /// </summary>
        [DependsOn(nameof(sHasESignedDocumentsT))]
        public E_sHasESignedDocumentsT sHasESignedDocumentsT
        {
            get
            {
                return (E_sHasESignedDocumentsT)GetTypeIndexField("sHasESignedDocumentsT");
            }
            set
            {
                SetTypeIndexField("sHasESignedDocumentsT", (int)value);
            }
        }

        [DependsOn]
        public CDateTime sLastDisclosedD
        {
            get { return GetDateTimeField("sLastDisclosedD"); }
            set
            {
                if (value.IsValid)
                {
                    SetDateTimeField("sLastDisclosedD", value.DateTimeForComputationWithTime);
                }
                else
                {
                    SetDateTimeField("sLastDisclosedD", value);
                }
            }
        }

        [DependsOn(nameof(sAlwaysRequireAllBorrowersToReceiveClosingDisclosure), nameof(sDisclosureRegulationT))]
        [DevNote("Indicator for whether all borrowers must receive a CD, used for CD received and signed date calculations.")]
        public bool sAlwaysRequireAllBorrowersToReceiveClosingDisclosure
        {
            get
            {
                if (this.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID)
                {
                    return false;
                }

                return this.GetBoolField(nameof(sAlwaysRequireAllBorrowersToReceiveClosingDisclosure), this.m_brokerDB.RequireAllBorrowersToReceiveCD);
            }
            set { this.SetNullableBoolField(nameof(sAlwaysRequireAllBorrowersToReceiveClosingDisclosure), value); }
        }

        // THIS FIELD IS NO LONGER USED
        /// <summary>
        /// The previous difference between the APR and the last disclosed APR
        /// that triggered an APR alert. The name suggests that this may be
        /// unsigned, however, it is the signed difference.
        /// </summary>
        //[DependsOn]
        //public decimal sPrevAprDelta
        //{
        //    get { return GetDecimalField("sPrevAprDelta", 3); }
        //    set { SetDecimalField("sPrevAprDelta", value, 3); }
        //}

        /// <summary>
        /// Indicates whether or not the loan is being marked as disclosed or having
        /// a disclosure cleared during this save period (between the InitSave() and
        /// Save() calls).
        /// </summary>
        /// <remarks>
        /// This should be used when we are trying to reset the disclosure fields
        /// to their default vaules. It will suppress disclosure triggers from
        /// firing.
        /// </remarks>
        private bool m_markingLoanAsDisclosed = false;

        /// <summary>
        /// Contrast with m_auditItem, which are not always recorded.
        /// List to keep track of audit items that will need to be
        /// recorded after a successful save, regardless of any status or other condition.
        /// </summary>
        private List<AbstractAuditItem> m_alwaysAuditItems = new List<AbstractAuditItem>();

        /// <summary>
        /// Adds an audit item to the list of audits that will be recorded after a successful save event.
        /// </summary>
        /// <param name="auditItem">The audit item to add.</param>
        public void RecordAuditOnSave(AbstractAuditItem auditItem)
        {
            if (auditItem != null)
            {
                this.m_alwaysAuditItems.Add(auditItem);
            }
        }

        private IEnumerable<string> x_disclosureFieldChangeTriggerFields = null;
        /// <summary>
        /// Fields that need to be tracked for disclosure compliance automation.
        /// Changes to any of these fields have the potential to fire a field
        /// change disclosure trigger.
        /// </summary>
        private IEnumerable<string> DisclosureFieldChangeTriggerFields
        {
            get
            {
                if (x_disclosureFieldChangeTriggerFields == null)
                {
                    var brokerId = Tools.GetBrokerIdByLoanID(this.sLId);
                    bool useCustomCocFields = BrokerDB.RetrieveUseCustomCocFieldListBit(brokerId);

                    if (!useCustomCocFields)
                    {
                        var set = new HashSet<string>()
                        {
                        "sStatusT",
                        "sFinalLAmt",
                        "sLDiscnt",
                        "sApprVal",
                        "sCreditScoreLpeQual",
                        "sLpTemplateNmSubmitted",
                        "sApr",
                        "sLastDiscApr",
                        "sMldsHasImpound",
                        "sPurchPrice",
                        "sSubmitD"
                        };

                        x_disclosureFieldChangeTriggerFields = set.ToList();
                    }
                    else
                    {
                        var customCocFields = CustomCocFieldListHelper.RetrieveByBrokerId(brokerId).Select(c => c.FieldId).ToList();

                        x_disclosureFieldChangeTriggerFields = customCocFields;
                    }                    
                }

                return x_disclosureFieldChangeTriggerFields;
            }
        }

        private IEnumerable<string> x_disclosureWorkflowTriggerFields = null;
        /// <summary>
        /// Fields that need to be tracked for disclosure compliance automation.
        /// Changes to any of these fields will cause the RESPA workflow trigger
        /// to be evaluated.
        /// </summary>
        private IEnumerable<string> DisclosureWorkflowTriggerFields
        {
            get
            {
                if (x_disclosureWorkflowTriggerFields == null)
                {
                    if (m_systemExecutingEngine == null)
                    {
                        Tools.LogError("[DisclosureWorkflow] Failed to load executing engine, cannot load disclosure workflow trigger fields.");
                        return null;
                    }

                    IEnumerable<string> varList = m_systemExecutingEngine.GetDependencyVariableListForTrigger(ConstStage.DisclosureWorkflowRESPATriggerName);
                    if (varList == null)
                    {
                        return null;
                    }

                    HashSet<string> disclosureWorkflowTriggerFields = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
                    foreach (string field in varList)
                    {
                        if (field.StartsWith(DataPath.PathHeader, StringComparison.OrdinalIgnoreCase))
                        {
                            disclosureWorkflowTriggerFields.UnionWith(PathResolver.GetPathDependencyFields(field));
                            continue;
                        }

                        disclosureWorkflowTriggerFields.Add(field);
                    }

                    // GetDependencyVariableListForTrigger will return null if the trigger is not found. It won't be found for LoansPQ.
                    if (disclosureWorkflowTriggerFields.Any())
                    {
                        disclosureWorkflowTriggerFields.Add("sDocumentCheckD");
                        x_disclosureWorkflowTriggerFields = disclosureWorkflowTriggerFields;
                    }
                }

                return x_disclosureWorkflowTriggerFields;
            }
        }

        [DependsOn(nameof(sDisclosureNeededT), nameof(sDisclosuresDueD))]
        private bool sfUpdateDisclosuresDueDForChangedCircumstance { get { return false; } }

        /// <summary>
        /// Sets the disclosures due date for a changed circumstance trigger. If
        /// the date is already valid it will not be extended UNLESS the current
        /// disclosure status is due to an APR change. In this case, it may extend
        /// the due date. Sets the disclosures due date to three business days
        /// from the current date.
        /// </summary>
        private void UpdateDisclosuresDueDForChangedCircumstance()
        {
            if (this.sDisclosureNeededT == E_sDisclosureNeededT.APROutOfTolerance_RediscReqd
                && this.sDisclosuresDueD.IsValid)
            {
                // 7/30/2014 gf - We need to clear the due date in order to set a new value.
                this.sDisclosuresDueD_rep = "";
            }

            this.sDisclosuresDueD = CDateTime.Create(DateTime.Today).AddBusinessDays(3, this.BrokerDB);
        }

        [DependsOn(nameof(sDisclosureNeededT), nameof(sDisclosuresDueD), nameof(sNeedInitialDisc), nameof(sConsumerPortalVerbalSubmissionD))]
        private bool sfUpdateDisclosuresDueDForChangedVerbalSubmissionD { get { return false; } }

        /// <summary>
        /// If the consumer portal verbal submission date changes and the initial disclosure triggered because of the verbal submission date being entered,
        /// we need to update the disclosure due date per opm 209851.  Otherwise this is no-op.
        /// </summary>
        private void UpdateDisclosuresDueDForChangedVerbalSubmissionD()
        {
            if (sNeedInitialDisc && sDisclosureNeededT == E_sDisclosureNeededT.InitDisc_RESPAAppReceivedVerbally && sConsumerPortalVerbalSubmissionD.IsValid)
            {
                if(this.sDisclosuresDueD.IsValid)
                {
                    // 7/30/2014 gf - We need to clear the due date in order to set a new value.
                    this.sDisclosuresDueD_rep = string.Empty;
                }
                sDisclosuresDueD = CDateTime.Create(sConsumerPortalVerbalSubmissionD.DateTimeForComputationWithTime).AddBusinessDays(3, this.BrokerDB);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether we should force initial
        /// disclosure trigger evaluation on save.
        /// </summary>
        /// <remarks>
        /// This value is used to force trigger evaluation. It will not skip
        /// evaluation if false.
        /// </remarks>
        [DevNote("If set to true, will force initial disclosure trigger evaluation when the file is saved.")]
        [DependsOn()]
        public bool ForceInitialDisclosureEvalOnSave { get; set; }

        /// <summary>
        /// Flag to indicate whether we've set the loan application date because
        /// of RESPA evaluation.
        /// </summary>
        private bool applicationDateWasUpdatedInRespaTrigger = false;

        /// <summary>
        /// If any one of the RESPA triggers is fired for the loan, then mark it
        /// as needing initial disclosure. If the loan has already been disclosed,
        /// the loan is not a retail channel loan, or the loan is already marked
        /// as needing initial disclosure then do not evaluate the trigger.
        /// </summary>
        /// <remarks>
        /// Currently, the way that workflow works is that triggers with the same
        /// name are logically OR'd together so if any of them returns true, so
        /// will the evaluation.
        /// The field dependencies for this method are taken care of in
        /// FieldInfo.cs and PageData.cs.
        /// evaluated.
        /// </remarks>
        private void EvaluateWorkflowTriggersForDisclosureAutomation()
        {
            if (m_markingLoanAsDisclosed)
            {
                return;
            }
            else if (m_systemExecutingEngine == null)
            {
                Tools.LogError("[DisclosureWorkflow] Could not retrieve system executing engine.");
                return;
            }
            else if (!m_systemExecutingEngine.HasCustomVariable(ConstStage.DisclosureWorkflowRESPATriggerName))
            {
                // This will happen for LPQ prod.
                return;
            }
            else if (DisclosureWorkflowTriggerFields == null)
            {
                Tools.LogError("[DisclosureWorkflow] Could not load field dependencies for disclosure workflow trigger.");
                return;
            }

            bool evaluateTrigger = this.ForceInitialDisclosureEvalOnSave;
            string fieldUpdated = null;

            if (!evaluateTrigger)
            {
                foreach (var field in DisclosureWorkflowTriggerFields)
                {
                    if (IsFieldModified(field) == true)
                    {
                        evaluateTrigger = true;
                        fieldUpdated = field;
                        break;
                    }
                }
            }

            bool isSkippingDueToChannel = false;

            if (sBranchChannelT != E_BranchChannelT.Retail && !this.BrokerDB.UseRespaForNonRetailInitialDisclosures)
            {
                isSkippingDueToChannel = true;
            }

            // Don't check is sLastDisclosedD is valid until this point becuase
            // we are not sure if it will be included in the dependency list
            // until we know that one of the fields the trigger depends on is
            // modified. Same for sNeedInitialDisc and sBranchChannelT.
            if (!evaluateTrigger || this.sNeedInitialDisc ||
                sLastDisclosedD.IsValid)
            {
                return;
            }
            else if (this.sBranchChannelT != E_BranchChannelT.Retail &&
                this.BrokerDB.ReqDocCheckDAndRespaForNonRetailInitialDisclosures &&
                !this.sDocumentCheckD.IsValid)
            {
                // If they require the Document Check date in addition to RESPA
                // and the date is not valid, we do not need to evaluate the trigger.
                return;
            }

            // if we are skipping setting the disclosure due date and not applying the comp plan
            // when the RESPA 6 are collected, we are done. Exit early.
            if (isSkippingDueToChannel && !this.BrokerDB.ShouldApplyCompPlan(E_LoanEvent.Respa6Collected, this.sHasOriginatorCompensationPlan))
            {
                return;
            }

            var oldCalcMode = this.CalcModeT;
            var oldBypassFieldSecurityCheck = this.ByPassFieldSecurityCheck;
            var oldFormatTarget = this.GetFormatTarget();

            this.CalcModeT = E_CalcModeT.LendersOfficePageEditing;
            this.ByPassFieldSecurityCheck = true;
            this.SetFormatTarget(FormatTarget.Webform);

            LoanValueEvaluator valueEvaluator = new LoanValueEvaluator(this.InternalCPageData, this.InternalCPageData.GetAppData(0));

            // Not requiring a principal to evaluate the system workflow trigger...
            bool initialDisclosureRequired = m_systemExecutingEngine.EvaluateCustomVar(ConstStage.DisclosureWorkflowRESPATriggerName, valueEvaluator);

            this.ByPassFieldSecurityCheck = oldBypassFieldSecurityCheck;
            this.CalcModeT = oldCalcMode;
            this.SetFormatTarget(oldFormatTarget);

            if (ConstStage.LogRespaTriggerEvaluation)
            {
                var valueDescriptions = new List<string>();

                foreach (var fieldId in this.DisclosureWorkflowTriggerFields)
                {
                    if (this.x_loanFileFieldDictionary.ContainsKey(fieldId))
                    {
                        var item = this.x_loanFileFieldDictionary[fieldId];
                        var log = string.Format(
                            "{0} - Old: ({1}), Current: ({2})",
                            fieldId,
                            item.Original,
                            item.Current);
                        valueDescriptions.Add(log);
                    }
                    else if (this.x_appDataFieldDictionary.Values.Any(d => d.ContainsKey(fieldId)))
                    {
                        foreach (var appId in this.x_appDataFieldDictionary.Keys)
                        {
                            if (this.x_appDataFieldDictionary[appId].ContainsKey(fieldId))
                            {
                                var item = this.x_appDataFieldDictionary[appId][fieldId];

                                string log = "";
                                if (fieldId.ToLower().TrimWhitespaceAndBOM() == "abssn")
                                {
                                    log = string.Format(
                                        "{0} {1} - Old length: ({2}), Current length: ({3})",
                                        appId,
                                        fieldId,
                                        string.IsNullOrEmpty(item.Original) ? 0 : item.Original.Length,
                                        string.IsNullOrEmpty(item.Current) ? 0 : item.Current.Length);
                                }
                                else
                                {
                                    log = string.Format(
                                        "{0} {1} - Old: ({2}), Current: ({3})",
                                        appId,
                                        fieldId,
                                        item.Original,
                                        item.Current);
                                }

                                valueDescriptions.Add(log);
                            }
                        }
                    }
                }

                var messageFormat = "[DisclosureWorkflow] Evaluating RESPA trigger " +
                    " for loan id {0} due to field update ({1}). RESPA trigger evaluated to ({2})." +
                    "{3}Field Values:{3}{4}";
                valueDescriptions.Sort(StringComparer.OrdinalIgnoreCase);
                var formattedFieldValues = string.Join(
                    Environment.NewLine,
                    valueDescriptions.ToArray());

                var respaLog = string.Format(
                    messageFormat,
                    this.sLId,
                    fieldUpdated,
                    initialDisclosureRequired,
                    Environment.NewLine,
                    formattedFieldValues);

                Tools.LogInfo(respaLog);
            }

            if (initialDisclosureRequired)
            {
                if (!isSkippingDueToChannel)
                {
                    this.sNeedInitialDisc = true;

                    // 7/8/2014 gf - opm 182647, don't include days the
                    // lender is not open for business when setting initial
                    // disclosure date.

                    DateTime disclosureDueDate;
                    // per opm 209851 if the initial disclosure is being caused by entering in the consumer portal verbal submission date, we need to use that date.
                    if (sConsumerPortalVerbalSubmissionD.IsValid
                        && sIsConsumerPortalCreatedButNotSubmitted
                        && IsFieldModified("sConsumerPortalVerbalSubmissionD")
                        && x_loanFileFieldDictionary["sConsumerPortalVerbalSubmissionD"].Original == string.Empty)
                    {
                        disclosureDueDate = sConsumerPortalVerbalSubmissionD.DateTimeForComputationWithTime;
                        sDisclosureNeededT = E_sDisclosureNeededT.InitDisc_RESPAAppReceivedVerbally;
                    }
                    else
                    {
                        disclosureDueDate = DateTime.Today;
                        sDisclosureNeededT = E_sDisclosureNeededT.InitDisc_RESPAAppReceived;
                    }

                    sDisclosuresDueD = CDateTime.Create(disclosureDueDate).AddBusinessDays(3, this.BrokerDB);
                    sTilGfeDueD = sDisclosuresDueD;

                    // 10/7/2013 gf - opm 125734 auto-populate 1003 interview date
                    // 3/14/2017 gf - opm 365605 stop auto-populating this date for files
                    // that are in new loan version. For files in older versions, this value
                    // will be accessed through the application-level a1003InterviewD field.
                    if (!LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V17_UpdateApplicationSubmittedCalculation))
                    {
                        var preparer1003 = GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.CreateNew);
                        if (!preparer1003.PrepareDate.IsValid)
                        {
                            preparer1003.PrepareDate = CDateTime.Create(disclosureDueDate);
                            preparer1003.Update();

                            // Need to set this flag so we can migrate file to new data layer if needed.
                            this.applicationDateWasUpdatedInRespaTrigger = true;
                        }
                    }

                    // Add audit to be recorded after save.
                    var auditMsg = "Application Received, RESPA 3 Day Disclosure Period Begins";
                    m_alwaysAuditItems.Add(new DisclosureESignAuditItem(Guid.Empty, "System Notification", auditMsg));
                }

                if (this.BrokerDB.ShouldApplyCompPlan(E_LoanEvent.Respa6Collected, this.sHasOriginatorCompensationPlan))
                {
                    ApplyCompensationFromCurrentOriginator();
                }
            }
        }

        /// <summary>
        /// Handle the disclosure triggers that are fired from field changes and are not
        /// handled by workflow triggers.
        /// </summary>
        /// <remarks>
        /// Redisclosure triggers should have no affects on fields while the loan needs
        /// initial disclosure, but will still create audit events. The field dependencies
        /// for this method are taken care of in and FieldInfo.cs.
        /// If a field change needs to be checked after a delay, also handle it in
        /// ProcessDelayedRedisclosureTrigger and in DisclosureQueueProcessor.cs.
        /// </remarks>
        // Dependency list can be found in x_disclosureFieldChangeDependencies in ConstApp.cs
        private void ProcessFieldChangeDisclosureTriggers()
        {
            if (m_markingLoanAsDisclosed)
            {
                return;
            }

            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            if (principal != null && principal.Type == "I")
            {
                principal = ((InternalUserPrincipal)principal).BecomeUserPrincipal;
            }

            var useCustomCocAndEnableHelocRedisclosureTuple = BrokerDB.RetrieveUseCustomCocFieldBitAndEnableRedisclosureForHelocBit(this.sBrokerId);

            List<CustomCocField> customCocFields = null;
            bool useCustomCocFields = false;
            bool disableHelocRedisclosure = false;
            
            if (!useCustomCocAndEnableHelocRedisclosureTuple.Item2 && this.sIsLineOfCredit)
            {
                disableHelocRedisclosure = true;
            }

            if (useCustomCocAndEnableHelocRedisclosureTuple.Item1)
            {
                customCocFields = CustomCocFieldListHelper.RetrieveByBrokerId(this.sBrokerId);
                useCustomCocFields = true;
            }

            var userId = (principal == null) ? Guid.Empty : principal.UserId;
            var displayName = (principal == null) ? "System Notification" : principal.DisplayName;

            var disclosureQueue = new DBMessageQueue(ConstMsg.DisclosureQueue);
            var processedAprTrigger = false;

            foreach (var field in DisclosureFieldChangeTriggerFields)
            {                
                if (!IsFieldModified(field) &&
                    !(field == "sStatusT" && this.ForceInitialDisclosureEvalOnSave))
                {
                    continue;
                }

                string auditMsg = null;

                if (useCustomCocFields && !disableHelocRedisclosure)
                {
                    var customField = customCocFields.FirstOrDefault(f => f.FieldId == field);

                    if (customField.IsInstant)
                    {
                        if (!sNeedInitialDisc && sLastDisclosedD.IsValid)
                        {
                            sNeedRedisc = true;
                            sDisclosureNeededT = E_sDisclosureNeededT.Custom;
                            auditMsg = "Changed Circumstance - " + (string.IsNullOrEmpty(customField.Description) ? ConstApp.CustomCocDefaultDescription : customField.Description);
                            this.sDisclosureNeededTDescription = auditMsg;
                            this.UpdateDisclosuresDueDForChangedCircumstance();
                            sTilGfeDueD = sDisclosuresDueD;
                        }
                    }
                    else
                    {
                        FieldOriginalCurrentItem item = null;
                        string oldValue = null;
                        if (x_loanFileFieldDictionary.TryGetValue(field, out item))
                        {
                            oldValue = item.Original;
                        }
                        else
                        {
                            Tools.LogError("Could not determine original value of modified field.");
                            continue;
                        }

                        var data = String.Format("brokerid={0};;userid={1};;field={2};;oldvalue={3};;displayname={4};;description={5}",
                            sBrokerId,
                            userId,
                            field,
                            oldValue,
                            displayName,
                            customField.Description);
                        disclosureQueue.Send(sLId.ToString(), data);
                    }
                }
                
                switch (field)
                {
                    case "sStatusT":
                        // This is the initial disclosure trigger for wholesale
                        // loans. It should not be processed if the loan is
                        // marked as needing initial disclosure or has already
                        // been disclosed.
                        if (!sNeedInitialDisc &&
                            sStatusT == E_sStatusT.Loan_Registered &&
                            sBranchChannelT != E_BranchChannelT.Retail &&
                            !sLastDisclosedD.IsValid &&
                            !this.BrokerDB.UseRespaForNonRetailInitialDisclosures)
                        {
                            sNeedInitialDisc = true;
                            sDisclosureNeededT = E_sDisclosureNeededT.InitDisc_LoanRegistered;
                            auditMsg = "Loan Registered - Disclosures Required";

                            // 7/8/2014 gf - opm 182647, don't include days the
                            // lender is not open for business when setting initial
                            // disclosure date.
                            sDisclosuresDueD = CDateTime.Create(DateTime.Today).AddBusinessDays(3, this.BrokerDB);
                            sTilGfeDueD = sDisclosuresDueD;
                        }
                        break;
                    case "sSubmitD":
                        // OPM 235349. Capture the submission when the lender has disabled registered status
                        // when registered status is bypassed. Use registered date is
                        // going from blank to a date as another way.
                        if (!sNeedInitialDisc &&
                            sBranchChannelT != E_BranchChannelT.Retail &&
                            !sLastDisclosedD.IsValid &&
                            !this.BrokerDB.UseRespaForNonRetailInitialDisclosures)
                        {
                            FieldOriginalCurrentItem originalCurrentSubmitD = null;
                            string oldSubmitD = null;
                            if (x_loanFileFieldDictionary.TryGetValue(field, out originalCurrentSubmitD))
                            {
                                oldSubmitD = originalCurrentSubmitD.Original;
                            }
                            else
                            {
                                Tools.LogError("Could not determine original value of modified field:" + field);
                                continue;
                            }

                            if (String.IsNullOrEmpty(oldSubmitD) &&
                                sSubmitD.IsValid)
                            {
                                // Submit date was set.
                                sNeedInitialDisc = true;
                                sDisclosureNeededT = E_sDisclosureNeededT.InitDisc_LoanRegistered;
                                auditMsg = "Loan Registered - Disclosures Required";

                                sDisclosuresDueD = CDateTime.Create(DateTime.Today).AddBusinessDays(3, this.BrokerDB);
                                sTilGfeDueD = sDisclosuresDueD;
                            }
                        }
                            break;
                    case "sLpTemplateNmSubmitted":
                        if (useCustomCocFields || disableHelocRedisclosure)
                        {
                            break;
                        }

                        if (!sNeedInitialDisc && sLastDisclosedD.IsValid)
                        {
                            sNeedRedisc = true;
                            sDisclosureNeededT = E_sDisclosureNeededT.CC_ProgramChanged;
                            auditMsg = "Changed Circumstance - Program Changed";

                            this.UpdateDisclosuresDueDForChangedCircumstance();
                            sTilGfeDueD = sDisclosuresDueD;
                        }
                        break;
                    case "sApr":
                    case "sLastDiscApr":
                        // OPM 236812 - Do not trigger redisclosure required due to APR change for TRID loans.
                        if (sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
                        {
                            continue;
                        }

                        if (sNeedInitialDisc || !sLastDisclosedD.IsValid
                            || sLastDiscAPR == 0 || processedAprTrigger
                            || this.sIsLineOfCredit)
                        {
                            continue;
                        }

                        processedAprTrigger = true;
                        var aprDiff = 0.0m;
                        try
                        {
                            aprDiff = sApr - sLastDiscAPR;
                        }
                        catch (CBaseException exc)
                        {
                            Tools.LogWarning("[Disclosure] Failed to look up sApr: " + exc.Message);
                            continue;
                        }

                        // 8/30/2013 gf - OPM 118941 Some brokers do not want to trigger
                        // the redisclosure notification for an APR decrease.
                        if (aprDiff < 0 && !BrokerDB.TriggerAprRediscNotifForAprDecrease) continue;

                        var aprDelta = Math.Abs(aprDiff);
                        if (sFinMethT == E_sFinMethT.ARM && aprDelta < 0.25m
                            || aprDelta < 0.125m)
                        {
                            continue;
                        }

                        if (!IsChangedCircumstance(sDisclosureNeededT))
                        {
                            sNeedRedisc = true;
                            sDisclosureNeededT = E_sDisclosureNeededT.APROutOfTolerance_RediscReqd;

                            // 7/30/2014 gf - This calculation needs to include Saturdays regardless
                            // of the lender's business hours. opm 182647
                            sDisclosuresDueD = CDateTime.Create(DateTime.Today).AddWorkingBankDays(3,
                                !LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V28_DontRollSundayHolidaysToMonday));
                            sTilGfeDueD = sDisclosuresDueD;
                        }

                        auditMsg = "APR Change Threshold Exceeded, Redisclosure Required";
                        break;
                    case "sFinalLAmt":
                    case "sLDiscnt":
                    case "sApprVal":
                    case "sCreditScoreLpeQual":
                    case "sMldsHasImpound":
                    case "sPurchPrice":
                        if (useCustomCocFields || disableHelocRedisclosure)
                        {
                            break;
                        }

                        // If delayed triggers expand into app-level fields then will need
                        // to check there too. For now, only checking loan should be fine.
                        FieldOriginalCurrentItem item = null;
                        string oldValue = null;
                        if (x_loanFileFieldDictionary.TryGetValue(field, out item))
                        {
                            oldValue = item.Original;
                        }
                        else
                        {
                            Tools.LogError("Could not determine original value of modified field.");
                            continue;
                        }

                        var data = String.Format("brokerid={0};;userid={1};;field={2};;oldvalue={3};;displayname={4}",
                            sBrokerId,
                            userId,
                            field,
                            oldValue,
                            displayName);
                        disclosureQueue.Send(sLId.ToString(), data);
                        continue;
                    default:
                        if (useCustomCocFields)
                        {
                            break;
                        }

                        Tools.LogWarning("No action associated with disclosure trigger field.");
                        return;
                }

                if (!String.IsNullOrEmpty(auditMsg))
                {
                    var auditItem = new DisclosureESignAuditItem(userId, displayName, auditMsg);
                    m_alwaysAuditItems.Add(auditItem);
                }
            }
        }

        [DependsOn(nameof(sNeedRedisc), nameof(sDisclosuresDueD), nameof(sTilGfeDueD), nameof(sDisclosureNeededT), nameof(sNeedInitialDisc), nameof(sLastDisclosedD), nameof(sfUpdateDisclosuresDueDForChangedCircumstance),
            nameof(sDisclosureNeededTDescription))]
        private int sfProcessDelayedRedisclosureTriggerTrigger
        {
            get { return 0; }
        }
        /// <summary>
        /// Process a delayed redisclosure trigger and add an audit item to be
        /// recorded after a successful save.
        /// </summary>
        /// <remarks>
        /// When certain fields are changed, the field changes are recorded in
        /// a message queue to be processed at a later time.
        /// Will not have any effect if the loan needs initial disclosure or has
        /// not been disclosed yet.
        /// </remarks>
        /// <param name="disclosureT">The type of disclosure that is needed.</param>
        /// <param name="userId">The user id of the user that changed the field.</param>
        /// <param name="auditDisplayName">The name to display in the audit event.</param>
        public void ProcessDelayedRedisclosureTrigger(E_sDisclosureNeededT disclosureT, Guid userId, string auditDisplayName, string customDescription)
        {
            if (sNeedInitialDisc || !sLastDisclosedD.IsValid)
            {
                return;
            }

            sNeedRedisc = true;
            this.UpdateDisclosuresDueDForChangedCircumstance();
            sTilGfeDueD = sDisclosuresDueD;
            sDisclosureNeededT = disclosureT;

            var auditMsg = "";
            switch (disclosureT)
            {
                case E_sDisclosureNeededT.CC_LoanAmtChanged:
                    auditMsg = "Changed Circumstance - Loan Amount";
                    break;
                case E_sDisclosureNeededT.CC_GFEChargeDiscountChanged:
                    auditMsg = "Changed Circumstance - GFE Charge/Discount Changed";
                    break;
                case E_sDisclosureNeededT.CC_PropertyValueChanged:
                    auditMsg = "Changed Circumstance - Property Value Changed";
                    break;
                case E_sDisclosureNeededT.CC_UWCreditScoreChanged:
                    auditMsg = "Changed Circumstance - U/W Credit Score Changed";
                    break;
                case E_sDisclosureNeededT.CC_ImpoundChanged:
                    auditMsg = "Changed Circumstance - Impound Required Field Changed";
                    break;
                case E_sDisclosureNeededT.CC_PurchasePriceChanged:
                    auditMsg = "Changed Circumstance - Purchase Price Changed";
                    break;
                case E_sDisclosureNeededT.Custom:
                    auditMsg = "Changed Circumstance - " + (string.IsNullOrEmpty(customDescription) ? ConstApp.CustomCocDefaultDescription : customDescription);
                    this.sDisclosureNeededTDescription = auditMsg;
                    break;
                default:
                    Tools.LogError("This delayed redisclosure trigger is not implemented: " +
                        disclosureT.ToString());
                    return;
            }

            var auditItem = new DisclosureESignAuditItem(userId, auditDisplayName, auditMsg);
            m_alwaysAuditItems.Add(auditItem);
        }

        [DependsOn(nameof(sNeedInitialDisc), nameof(sNeedRedisc), nameof(sDisclosureNeededT), nameof(sDisclosuresDueD), nameof(sTilGfeDueD), nameof(sLastDisclosedD), nameof(sfUpdateDisclosuresDueDForChangedCircumstance), nameof(sIsLineOfCredit))]
        private int sfProcessRateLockDisclosureTrigger
        {
            get { return 0; }
        }
        /// <summary>
        /// Handle a rate lock disclosure trigger. Will handle a loan lock, re-lock,
        /// lock extension, and float down lock. Adds an audit item to be recorded
        /// after a successful save.
        /// </summary>
        /// <remarks>
        /// Should not change any fields if the loan needs initial disclosure, has
        /// not been disclosed, or the loan is a HELOC and the loan's lender disables 
        /// CoC triggers on HELOCs.
        /// </remarks>
        public void ProcessRateLockDisclosureTrigger(E_RateLockDisclosureEventT rateLockEventT)
        {
            if (sNeedInitialDisc || 
                !sLastDisclosedD.IsValid || 
                (this.sIsLineOfCredit && !this.m_brokerDB.EnableCocRedisclosureTriggersForHelocLoans))
            {
                return;
            }

            sNeedRedisc = true;
            this.UpdateDisclosuresDueDForChangedCircumstance();
            sTilGfeDueD = sDisclosuresDueD;

            DisclosureESignAuditItem auditItem = null;
            string auditMsg;
            switch (rateLockEventT)
            {
                case E_RateLockDisclosureEventT.LoanLocked:
                    sDisclosureNeededT = E_sDisclosureNeededT.CC_LoanLocked;
                    auditMsg = "Changed Circumstance - Loan Locked";
                    break;
                case E_RateLockDisclosureEventT.LockExtension:
                    sDisclosureNeededT = E_sDisclosureNeededT.CC_LockExtended;
                    auditMsg = "Changed Circumstance - Loan Extended";
                    break;
                case E_RateLockDisclosureEventT.Relocked:
                    sDisclosureNeededT = E_sDisclosureNeededT.CC_Relocked;
                    auditMsg = "Changed Circumstance - Relocked";
                    break;
                case E_RateLockDisclosureEventT.FloatDownLock:
                    sDisclosureNeededT = E_sDisclosureNeededT.CC_FloatDownLock;
                    auditMsg = "Changed Circumstance - Float Down Lock";
                    break;
                default:
                    throw new UnhandledEnumException(rateLockEventT);
            }

            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            if (principal != null && principal.Type == "I")
            {
                principal = ((InternalUserPrincipal)principal).BecomeUserPrincipal;
            }
            var userId = (principal == null) ? Guid.Empty : principal.UserId;
            var displayName = (principal == null) ? "System Notification" : principal.DisplayName;

            auditItem = new DisclosureESignAuditItem(userId, displayName, auditMsg);
            m_alwaysAuditItems.Add(auditItem);
        }

        [DependsOn(nameof(UpdateDisclosureStatus), nameof(SetEConsentReceivedDates))]
        public bool UpdateDisclosureStatusForExternalInitialDisclosure(decimal? lastDisclosedApr, AbstractAuditItem auditItem)
        {
            this.SetEConsentReceivedDates();
            return this.UpdateDisclosureStatus(
                package: string.Empty,
                lastDisclosedApr: lastDisclosedApr,
                resultIsDisclosurePackage: true,
                resultIsClosingPackage: false,
                resultIsInitialDisclosurePackage: true,
                requestIsSendEDisclosures: true,
                requestIsEClosed: false,
                requestIsManualFulfillment: false,
                resultPackageType: string.Empty,
                replacementAudit: auditItem);
        }

        [DependsOn(nameof(UpdateDisclosureStatus))]
        public bool UpdateDisclosureStatus(string package, decimal? lastDisclosedApr, bool resultIsDisclosurePackage, bool resultIsClosingPackage, bool resultIsInitialDisclosurePackage,
                                            bool requestIsSendEDisclosures, bool requestIsEClosed, bool requestIsManualFulfillment, string resultPackageType, DocumentIntegrationPackageName packageTypeName)
        {
            return this.UpdateDisclosureStatus(package, lastDisclosedApr, resultIsDisclosurePackage, resultIsClosingPackage, resultIsInitialDisclosurePackage,
                                            requestIsSendEDisclosures, requestIsEClosed, requestIsManualFulfillment, resultPackageType, packageTypeName, replacementAudit: null);
        }

        [DependsOn(nameof(sDisclosureRegulationT), nameof(sIsConstructionLoanWithConstructionRefiData), nameof(sInitAPR), nameof(sApr), nameof(sLastDiscAPR),
                   nameof(GetLastDisclosedApr), nameof(sfProcessDocDisclosureTrigger), nameof(sCalculateInitAprAndLastDiscApr))]
        private bool UpdateDisclosureStatus(string package, decimal? lastDisclosedApr, bool resultIsDisclosurePackage, bool resultIsClosingPackage, bool resultIsInitialDisclosurePackage,
                                            bool requestIsSendEDisclosures, bool requestIsEClosed, bool requestIsManualFulfillment, string resultPackageType, DocumentIntegrationPackageName? packageTypeName = null, AbstractAuditItem replacementAudit = null)
        {
            var disclosureT = E_DisclosureT.None;

            // OPM 61926: if this is a disclosure package, save the current APR for posterity.
            if (resultIsDisclosurePackage
                || (this.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID && resultIsClosingPackage))
            {
                // OPM 108241: Even if the user isn't normally allowed to write to sInitAPR or sLastDiscAPR,
                // they should be allowed to right now.
                if (!this.sCalculateInitAprAndLastDiscApr
                    && !this.sIsConstructionLoanWithConstructionRefiData)
                {
                    this.CalcModeT = E_CalcModeT.LendersOfficePageEditing;

                    if (resultIsInitialDisclosurePackage && this.sInitAPR == 0)
                    {
                        this.sInitAPR = this.sApr;
                    }

                    // 2/20/14 gf - opm 169247, If broker has DocuTech packaged based redisclosure date population set,
                    // we do NOT want to update the last disclosed APR if it is a GFE redisclosure because this package
                    // doesn't actually disclose an APR.
                    if (!this.BrokerDB.IsEnableDTPkgBsdRedisclosureDatePop
                        || package != ConstAppDavid.DocuTech_Temp_GFE_Redisclosure_PackageID)
                    {
                        // If we are generating documents for a TRID file, the last disclosed
                        // APR should only be updated when we save the temporary archive.
                        if (this.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID
                            && lastDisclosedApr != null)
                        {
                            this.sLastDiscAPR = lastDisclosedApr.Value;
                        }
                        else if (this.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID)
                        {
                            this.sLastDiscAPR = this.GetLastDisclosedApr();
                        }
                    }
                }

                if (resultIsInitialDisclosurePackage)
                {
                    disclosureT = E_DisclosureT.InitialDisclosure;
                }
                else if (resultIsDisclosurePackage || (resultIsClosingPackage && ConstStage.IsTreatClosingDisclosureAsRedisclosure))
                {
                    disclosureT = E_DisclosureT.Redisclosure;
                }
            }

            // OPM 105770, 80437, 108156
            bool isEDisclosed = requestIsSendEDisclosures;
            bool isEClosed = requestIsEClosed;
            bool isManualFulfillment = requestIsManualFulfillment;

            this.ProcessDocDisclosureTrigger(
                disclosureT,
                isEDisclosed,
                isEClosed,
                isManualFulfillment,
                resultPackageType,
                packageTypeName,
                replacementAudit);

            return true;
        }

        [DependsOn(nameof(sDisclosureRegulationT), nameof(sLastDisclosedGFEArchiveD), nameof(LastDisclosedGFEArchive), nameof(sApr))]
        private decimal GetLastDisclosedApr()
        {
            if (this.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                throw new InvalidOperationException("Do not expect TRID files to pull last disclosed APR in this manner.");
            }

            if (!this.BrokerDB.IsGFEandCoCVersioningEnabled ||
                this.sLastDisclosedGFEArchiveD == null ||
                this.LastDisclosedGFEArchive == null)
            {
                return this.sApr;
            }

            var fieldDependencies =
                CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(DocumentAuditor))
                    .Union(TEMP_GFEArchiveFieldNamesFrom2013Q3Release);

            var dataLoanWithGFEArchiveApplied = new CFullAccessPageData(
                this.sLId,
                fieldDependencies);
            dataLoanWithGFEArchiveApplied.InitLoad();

            dataLoanWithGFEArchiveApplied.ApplyGFEArchiveExcludingFields(
                this.LastDisclosedGFEArchive,
                ConstApp.FieldsToExcludeWhenApplyingGFEArchiveForAprCalc);

            return dataLoanWithGFEArchiveApplied.sApr;
        }

        [DependsOn(nameof(sNeedInitialDisc), nameof(sNeedRedisc), nameof(sDisclosureNeededT), nameof(sDisclosuresDueD), nameof(sTilGfeOd), nameof(sLastDisclosedD), nameof(sEConsentCompleted), 
            nameof(sTilInitialDisclosureD), nameof(sGfeInitialDisclosureD), nameof(sfSet_sGfeInitialDisclosureD), nameof(sTilRedisclosureD), nameof(sGfeRedisclosureD), nameof(sRedisclosureMethodT), nameof(sfGetPreparerOfForm), nameof(sSpAddr), 
            nameof(sAutoDisclosureFailed), nameof(sDisclosureRegulationT), nameof(sClosingCostArchive), nameof(sClosingCostFeeVersionT), nameof(sIsEnableEConsentMonitoringAutomation))]
        private int sfProcessDocDisclosureTrigger
        {
            get { return 0; }
        }

        /// <summary>
        /// Used for testing only.
        /// </summary>
        public void ProcessDocDisclosureTrigger(E_DisclosureT disclosureT, bool isEDisclosed, bool isEClosed, bool isManualFulfillment, string packageType, DocumentIntegrationPackageName packageTypeName)
        {
            this.ProcessDocDisclosureTrigger(disclosureT, isEDisclosed, isEClosed, isManualFulfillment, packageType, packageTypeName, replacementAudit: null);
        }

        /// <summary>
        /// Handle disclosure triggers that result from document generation.
        /// See OPM 105770, 80437, 108156, 138661.
        /// </summary>
        /// <param name="disclosureT">Type of doc trigger.</param>
        /// <remarks>
        /// <paramref name="packageTypeName"/> and <paramref name="replacementAudit"/> are "mutually exclusive" parameters, where we expect one or
        /// the other to be null.  This happens when either the docs are generated in the system (meaning <paramref name="packageTypeName"/> is not null)
        /// or an external initial disclosure is recorded via web services (meaning <paramref name="replacementAudit"/> is not null and there is no package).
        /// </remarks>
        private void ProcessDocDisclosureTrigger(E_DisclosureT disclosureT,
                bool isEDisclosed, bool isEClosed, bool isManualFulfillment, string packageType, DocumentIntegrationPackageName? packageTypeName, AbstractAuditItem replacementAudit)
        {
            var today = CDateTime.Create(DateTime.Today);
            var now = CDateTime.Create(DateTime.Now);

            var isInitialDisclosure = disclosureT == E_DisclosureT.InitialDisclosure;
            var isRedisclosure = disclosureT == E_DisclosureT.Redisclosure;

            if (isInitialDisclosure || isRedisclosure)
            {
                sTilGfeOd = today;

                // 10/31/2013 gf - opm 141730 Don't autopopulate GFE/TIL prepared dates, it
                // was confusing users.
                //var preparer = GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.CreateNew);
                //preparer.PrepareDate = today;
                //preparer.Update();
                //preparer = GetPreparerOfForm(E_PreparerFormT.Til, E_ReturnOptionIfNotExist.CreateNew);
                //preparer.PrepareDate = today;
                //preparer.Update();

                if (sLastDisclosedD.IsValid)
                {
                    if (false == BrokerDB.IsEnableDTPkgBsdRedisclosureDatePop ||
                        (BrokerDB.IsEnableDTPkgBsdRedisclosureDatePop && packageType == "9458"))
                    {
                        sTilRedisclosureD = today;
                    }
                    // 10/29/2013 gf - opm 143074 for the GFE, do not populate the re-disclosure date if the
                    // initial disclosure date isn't valid.
                    // IMPORTANT: THIS CODE MUST COME BEFORE AUTOPOPULATION OF sGfeInitialDisclosureD
                    if (string.Compare(sSpAddr, "TBD", true) != 0 && sGfeInitialDisclosureD.IsValid) // opm 141755
                    {
                        if (false == BrokerDB.IsEnableDTPkgBsdRedisclosureDatePop ||
                        (BrokerDB.IsEnableDTPkgBsdRedisclosureDatePop && packageType == "203"))
                        {
                            sGfeRedisclosureD = today;
                        }
                    }

                    if (isEDisclosed || isEClosed)
                    {
                        sRedisclosureMethodT = E_RedisclosureMethodT.Email;
                    }
                }

                if (isInitialDisclosure)
                {
                    if (!sTilInitialDisclosureD.IsValid) sTilInitialDisclosureD = today;
                    if (!sGfeInitialDisclosureD.IsValid)
                    {
                        if (string.Compare(sSpAddr, "TBD", true) != 0) // opm 141755
                        {
                            if (isEDisclosed || isEClosed || isManualFulfillment)    // opm 169865
                            {
                                Set_sGfeInitialDisclosureD(today, E_GFEArchivedReasonT.DisclosureSentToBorrower);

                                if (this.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID)
                                {
                                    string lastDisclosedArchiveType = sClosingCostArchive != null && sClosingCostArchive.Count() >= 1 ? sClosingCostArchive.First().ClosingCostArchiveType.ToString() : "Either a legacy archive, or no archive was created.";

                                    Tools.LogInfo("Document generation has triggered the Initial Disclosure. An archive is being created with the following settings:"
                                        + " sDisclosureRegulationT: " + sDisclosureRegulationT.ToString()
                                        + ", sClosingCostFeeVersionT: " + sClosingCostFeeVersionT.ToString()
                                        + ", An Archive was created of type: " + lastDisclosedArchiveType
                                        );
                                }
                            }
                        }
                    }

                    // 9/17/2013 gf - opm 138661 Don't mark as disclosed if it
                    // is neither e-disclosed nor manually fulfilled. Also, no
                    // longer allow drawing initial disc docs in place of re-
                    // disc docs.
                    // 10/30/2013 gf - opm 143074 Re-allow drawing initial disclosure
                    // package to clear a need for re-disclosure. DocuTech uses
                    // it's initial disclosure package as something like a "full
                    // re-disclosure" package.
                    if (isEDisclosed || isEClosed || isManualFulfillment)
                    {
                        sNeedInitialDisc = false;
                        sNeedRedisc = false;
                        sLastDisclosedD = now;
                        m_markingLoanAsDisclosed = true; // flag so field change/workflow triggers don't undo.
                        sAutoDisclosureFailed = false;

                        if (!this.sIsEnableEConsentMonitoringAutomation || sEConsentCompleted || isManualFulfillment)
                        {
                            sDisclosureNeededT = E_sDisclosureNeededT.None;
                            sDisclosuresDueD_rep = "";
                        }
                        else if (isEDisclosed || isEClosed)
                        {
                            sDisclosureNeededT = E_sDisclosureNeededT.AwaitingEConsent;
                        }
                    }
                }
                else
                {
                    // 9/17/2013 gf - opm 138661 Don't mark as disclosed
                    // if the user doesn't e-disclose or manually fulfill. It is
                    // possible that a user will generate documents as a preview.
                    if (sNeedRedisc && (isEDisclosed || isEClosed || isManualFulfillment))
                    {
                        sNeedRedisc = false;
                        sDisclosureNeededT = E_sDisclosureNeededT.None;
                        sDisclosuresDueD_rep = "";
                        sLastDisclosedD = now;
                        m_markingLoanAsDisclosed = true; // flag so field change/workflow triggers don't undo.
                    }
                }
            }

            if (replacementAudit != null)
            {
                m_alwaysAuditItems.Add(replacementAudit);
            }
            else
            {
                string auditMsg = string.Empty;
                if (isInitialDisclosure)
                {
                    auditMsg = "Initial Disclosures Created";

                    // 10/30/2014 AV - 195604 Updated Audit History to show Manual Disclosures
                    if (isManualFulfillment)
                    {
                        auditMsg += " - Manual Fulfillment Requested";
                    }
                }
                else if  (isRedisclosure)
                {
                    // 1/17/2014 gf - opm 149899 - Provide more descriptive audit for DocuTech TIL redisclosure.
                    auditMsg = packageType == "9458" ? "TIL Redisclosure Created" : "Redisclosures Created";
                }
                else
                {
                    auditMsg = packageTypeName.ForceValue().Name + " Created"; // either packageTypeName or replacementAudit should not be null
                }

                // OPM 139582 - Mark E-Disclosure Sent in audits
                if (!string.IsNullOrEmpty(auditMsg))
                {
                    if (isEDisclosed)
                    {
                        auditMsg += " - E-Disclosure Sent";
                    }
                    else if (isEClosed)
                    {
                        auditMsg += " - E-Close Selected";
                    }
                }

                AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
                if (principal != null && principal.Type == "I")
                {
                    principal = ((InternalUserPrincipal)principal).BecomeUserPrincipal;
                }
                var userId = (principal == null) ? Guid.Empty : principal.UserId;
                var displayName = (principal == null) ? "System Notification" : principal.DisplayName;

                var auditItem = new DisclosureESignAuditItem(userId, displayName, auditMsg);
                m_alwaysAuditItems.Add(auditItem);
            }
        }

        [DependsOn(nameof(CAppBase.aBEConsentReceivedD), nameof(CAppBase.aCEConsentReceivedD), nameof(CAppBase.aBEConsentDeclinedD), nameof(CAppBase.aCEConsentDeclinedD), 
            nameof(sNeedInitialDisc), nameof(sNeedRedisc), nameof(sDisclosureNeededT), nameof(sTilGfeRd), nameof(sfGetApplicationAndBorrowerFromString), 
            nameof(sDisclosuresDueD), nameof(sDisclosuresMailedD), nameof(sEConsentCompleted), nameof(sfProcessManualDisclosureTrigger), 
            nameof(sLoanEstimateDatesInfo), nameof(sClosingDisclosureDatesInfo), nameof(sfIsConsentWithinThreeBusinessDays), 
            nameof(sfHasBorrowerEConsented), nameof(sCHARMBookletProvidedD), nameof(sHUDSpecialInfoBookletProvidedD), nameof(sLoanRescindableT), nameof(SetEConsentReceivedDates),
            nameof(CAppBase.aBTypeT), nameof(CAppBase.aCTypeT), nameof(ApplyDisclosureMetadataFromVendor), nameof(sIsEnableEConsentMonitoringAutomation))]
        private int sfProcessEDisclosureDisclosureTrigger
        {
            get { return 0; }
        }
        [DependsOn(nameof(CAppBase.aBEConsentReceivedD), nameof(CAppBase.aCEConsentReceivedD), nameof(CAppBase.aBEConsentDeclinedD), nameof(CAppBase.aCEConsentDeclinedD),
            nameof(CAppBase.aBIsValidNameSsn), nameof(CAppBase.aCIsValidNameSsn))]
        public void SetEConsentReceivedDates()
        {
            CAppBase dataApp = null;
            for (int i = 0; i < m_dataApps.Length; i++)
            {
                dataApp = GetAppData(i);

                var haveBorrowerEConsent = dataApp.aBEConsentReceivedD.IsValid &&
                (!dataApp.aBEConsentDeclinedD.IsValid ||
                dataApp.aBEConsentReceivedD.DateTimeForComputationWithTime.CompareTo(dataApp.aBEConsentDeclinedD.DateTimeForComputationWithTime) > 0);

                var haveCoborrowerEConsent = dataApp.aCEConsentReceivedD.IsValid &&
                    (!dataApp.aCEConsentDeclinedD.IsValid ||
                    dataApp.aCEConsentReceivedD.DateTimeForComputationWithTime.CompareTo(dataApp.aCEConsentDeclinedD.DateTimeForComputationWithTime) > 0);

                if (!haveBorrowerEConsent && dataApp.aBIsValidNameSsn)
                {
                    dataApp.aBEConsentReceivedD = CDateTime.Create(DateTime.Now);
                }

                if (!haveCoborrowerEConsent && dataApp.aCIsValidNameSsn)
                {
                    dataApp.aCEConsentReceivedD = CDateTime.Create(DateTime.Now);
                }
            }
        }

        private void UpdateLeCdDatesInfo(ESignNotificationDatesUpdateData eSignNotificationDatesUpdateData)
        {
            bool appliedDataFromDocumentVendor = ApplyDisclosureMetadataFromVendor(eSignNotificationDatesUpdateData.VendorProvidedDisclosureMetaData);
            bool inferredDataFromESignStatus = InferLeCdDatesFromESignEvent(eSignNotificationDatesUpdateData.TransactionId, 
                eSignNotificationDatesUpdateData.DisclosureType, 
                eSignNotificationDatesUpdateData.BorrowerName, 
                eSignNotificationDatesUpdateData.BorrowerType, 
                eSignNotificationDatesUpdateData.AppIdAndBorrowerType);

            if (appliedDataFromDocumentVendor || inferredDataFromESignStatus)
            {
                SaveLEandCDDates();
            }
        }

        private void CacheESignNotificationDatesUpdateData(ESignNotificationDatesUpdateData eSignNotificationDatesUpdateData)
        {
            var eSignNotificationDatesUpdateDataJson = AutoExpiredTextCache.GetFromCacheThenRemove(sLId + "_" + eSignNotificationDatesUpdateData.TransactionId);

            List<ESignNotificationDatesUpdateData> eSignNotificationDatesUpdateDataList = null;

            if (eSignNotificationDatesUpdateDataJson == null)
            {
                eSignNotificationDatesUpdateDataList = new List<ESignNotificationDatesUpdateData>();
            }
            else
            {
                eSignNotificationDatesUpdateDataList = SerializationHelper.JsonNetDeserializeWithTypeNameHandling<List<ESignNotificationDatesUpdateData>>(eSignNotificationDatesUpdateDataJson);
            }

            eSignNotificationDatesUpdateDataList.Add(eSignNotificationDatesUpdateData);

            eSignNotificationDatesUpdateDataJson = SerializationHelper.JsonNetSerializeWithTypeNameHandling<List<ESignNotificationDatesUpdateData>>(eSignNotificationDatesUpdateDataList);

            AutoExpiredTextCache.AddToCache(eSignNotificationDatesUpdateDataJson, TimeSpan.FromMinutes(30), sLId + "_" + eSignNotificationDatesUpdateData.TransactionId);
        }

        /// <summary>
        /// Processes EDisclosure triggers from the document framework.
        /// </summary>
        /// <param name="disclosureT">Type of EDisclosure trigger.</param>
        /// <param name="eSignNotification">The ESign notification POSTed to LQB.</param>
        public void ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT disclosureT, LQBESignUpdate eSignNotification)
        {
            string auditMsg = string.Empty;
            string borrowerName = null;
            E_aTypeT? borrowerType = null;
            CAppBase dataApp = null;
            Tuple<Guid, E_BorrowerModeT> appIdAndBorrowerT = null;

            switch (disclosureT)
            {
                case E_EDisclosureDisclosureEventT.EConsentReceived:
                    borrowerName = eSignNotification.ESignNotification.ToUpper().Replace("E-CONSENT RECEIVED FOR", string.Empty).Trim();
                    appIdAndBorrowerT = GetApplicationAndBorrowerFromString(borrowerName);
                    if (appIdAndBorrowerT != null)
                    {
                        dataApp = GetAppData(appIdAndBorrowerT.Item1);
                        if (appIdAndBorrowerT.Item2 == E_BorrowerModeT.Borrower)
                        {
                            dataApp.aBEConsentReceivedD = CDateTime.Create(DateTime.Now);
                        }
                        else
                        {
                            dataApp.aCEConsentReceivedD = CDateTime.Create(DateTime.Now);
                        }
                    }
                    // 11/11/2013 gf - opm 143983
                    if (sEConsentCompleted && sDisclosuresDueD.IsValid
                        && (sDisclosureNeededT == E_sDisclosureNeededT.EConsentNotReceived_PaperDiscReqd
                        || sDisclosureNeededT == E_sDisclosureNeededT.EConsentDec_PaperDiscReqd
                        || sDisclosureNeededT == E_sDisclosureNeededT.AwaitingEConsent))
                    {
                        // Allow any time on the third business day.
                        if (IsConsentWithinThreeBusinessDays(DateTime.Now))
                        {
                            ProcessManualDisclosureTrigger(false);
                        }
                    }
                    auditMsg = eSignNotification.ESignNotification;
                    break;
                case E_EDisclosureDisclosureEventT.EConsentDeclined:
                    borrowerName = eSignNotification.ESignNotification.ToUpper().Replace("E-CONSENT DECLINED BY", string.Empty).Trim();
                    appIdAndBorrowerT = GetApplicationAndBorrowerFromString(borrowerName);
                    if (appIdAndBorrowerT != null)
                    {
                        dataApp = GetAppData(appIdAndBorrowerT.Item1);
                        if (appIdAndBorrowerT.Item2 == E_BorrowerModeT.Borrower)
                        {
                            dataApp.aBEConsentDeclinedD = CDateTime.Create(DateTime.Now);
                        }
                        else
                        {
                            dataApp.aCEConsentDeclinedD = CDateTime.Create(DateTime.Now);
                        }
                    }
                    
                    if (this.sIsEnableEConsentMonitoringAutomation)
                    {
                        sNeedInitialDisc = true;
                        // Even if there was a re-disclosure trigger that fired during the
                        // waiting period, we reset to needing initial if a single borrower
                        // declines e-consent. This applies only to the document framework.
                        sNeedRedisc = false;
                        sDisclosureNeededT = E_sDisclosureNeededT.EConsentDec_PaperDiscReqd;
                    }

                    auditMsg = String.Format("{0} - Paper Disclosure Required",
                            eSignNotification.ESignNotification);
                    break;
                case E_EDisclosureDisclosureEventT.DocumentsReviewed:
                case E_EDisclosureDisclosureEventT.EConsentReceivedForAllParties:
                    // 07/28/2016 je - opm 240146 - Treat EConsentReceivedForAllParties the same as DocumentsReviewed.                    
                case E_EDisclosureDisclosureEventT.ESignCompleted:
                    // opm 455585 - ESignCompleted has exactly same code as EConsentReceivedForAllParties.
                    auditMsg = eSignNotification.ESignNotification;                    
                    // 7/11/2014 gf - opm 186787
                    // normal case, where IDS/DocuTech sends E-Consent Received first before E-Sign Completed
                    if (!this.sNeedInitialDisc && !this.sNeedRedisc &&
                        (this.sDisclosureNeededT == E_sDisclosureNeededT.AwaitingEConsent ||
                        this.sDisclosureNeededT == E_sDisclosureNeededT.None))
                    {
                        sDisclosuresDueD_rep = "";
                        sDisclosureNeededT = E_sDisclosureNeededT.None;
                    }
                    // special case, where IDS/DocuTech sends E-Sign Completed without E-Consent Received
                    else
                    {
                        // manually set E-Consent received dates if they haven't been set yet
                        SetEConsentReceivedDates();
                        if (sEConsentCompleted && sDisclosuresDueD.IsValid
                            && (sDisclosureNeededT == E_sDisclosureNeededT.EConsentNotReceived_PaperDiscReqd
                                || sDisclosureNeededT == E_sDisclosureNeededT.EConsentDec_PaperDiscReqd
                                || sDisclosureNeededT == E_sDisclosureNeededT.AwaitingEConsent))
                        {
                            // only clear the disclosure if it is received within the due date. opm 455585
                            if (DateTime.Today.CompareTo(sDisclosuresDueD.DateTimeForComputation) <= 0)
                            {
                                ProcessManualDisclosureTrigger(false);
                            }
                        }
                    }
                    sTilGfeRd = CDateTime.Create(DateTime.Now);
                    //sHasESignedDocumentsT = E_sHasESignedDocumentsT.Yes; // not putting this here b/c we need to check we got a pdf.
                    break;
                case E_EDisclosureDisclosureEventT.ManualFulfillmentInitiated:
                    auditMsg = eSignNotification.ESignNotification;
                    m_manualFulfillmentInitiatedByDocVendor = true;
                    sDisclosuresMailedD = CDateTime.Create(DateTime.Now);
                    m_manualFulfillmentInitiatedByDocVendor = false;
                    break;
                case E_EDisclosureDisclosureEventT.DocumentsSigned:
                    auditMsg = eSignNotification.ESignNotification;

                    borrowerName = eSignNotification.ESignNotification.ToUpper().Replace("DOCUMENTS SIGNED FOR", string.Empty).Trim();
                    appIdAndBorrowerT = GetApplicationAndBorrowerFromString(borrowerName);
                    if (appIdAndBorrowerT != null)
                    {
                        dataApp = GetAppData(appIdAndBorrowerT.Item1);
                        if (appIdAndBorrowerT.Item2 == E_BorrowerModeT.Borrower)
                        {
                            borrowerType = dataApp.aBTypeT;
                        }
                        else
                        {
                            borrowerType = dataApp.aCTypeT;
                        }
                    }

                    break;
                case E_EDisclosureDisclosureEventT.ReceivedLQBESignUpdate:

                    // 10/21/2013 gf - opm 142153 If we receive an ESignUpdate that wasn't
                    // expected, log it in the audit history.
                    auditMsg = eSignNotification.ESignNotification;
                    break;
                default:
                    throw new CBaseException(ErrorMessages.Generic, "This method " +
                        "only handles ESign triggers from the generic document framework." +
                        "Either unimplemented disclosure trigger or wrong method.");

            }

            // The TransactionID checks are to prevent future errors with DocMagic.
            bool validTransactionId = !string.IsNullOrEmpty(eSignNotification.TransactionID)
                && !eSignNotification.TransactionID.Equals(Guid.Empty.ToString(), StringComparison.OrdinalIgnoreCase)
                && !eSignNotification.TransactionID.Equals(this.sLId.ToString(), StringComparison.OrdinalIgnoreCase);

            if (validTransactionId)
            {
                var disclosureMetadata = VendorProvidedDisclosureMetadata.ParseLqbESignMetadata(eSignNotification, this.sLId);

                var eSignNotificationDatesUpdateData = new ESignNotificationDatesUpdateData
                {
                    VendorProvidedDisclosureMetaData = disclosureMetadata,
                    TransactionId = eSignNotification.TransactionID,
                    DisclosureType = disclosureT,
                    BorrowerName = borrowerName,
                    BorrowerType = borrowerType,
                    AppIdAndBorrowerType = appIdAndBorrowerT
                };

                if (ConstStage.EnableDelayedESignNotificationLeCdRowUpdates && !DoesLeOrCdDatesExistWithTransactionId(eSignNotification.TransactionID))
                {
                    CacheESignNotificationDatesUpdateData(eSignNotificationDatesUpdateData);
                }                                
                else
                {
                    UpdateLeCdDatesInfo(eSignNotificationDatesUpdateData);
                }
            }

            var auditItem = new DisclosureESignAuditItem(Guid.Empty,
                "System Notification", auditMsg);
            m_alwaysAuditItems.Add(auditItem);

            // OPM 288610 - DT OtherInserts.
            if (eSignNotification.OtherInserts != null && eSignNotification.OtherInserts.Length > 0)
            {
                if (!sCHARMBookletProvidedD.IsValid && eSignNotification.OtherInserts.Contains("CHARM Booklet Distributed", StringComparer.OrdinalIgnoreCase))
                {
                    sCHARMBookletProvidedD = sTodayD;
                }

                if (!sHUDSpecialInfoBookletProvidedD.IsValid && eSignNotification.OtherInserts.Contains("HUD Booklet Distributed", StringComparer.OrdinalIgnoreCase))
                {
                    sHUDSpecialInfoBookletProvidedD = sTodayD;
                }

                AbstractAuditItem otherInsertsAudit = new ESignOtherInsertsAuditItem(eSignNotification.OtherInserts);
                m_alwaysAuditItems.Add(otherInsertsAudit);
            }
        }

        private bool DoesLeOrCdDatesExistWithTransactionId(string transactionId)
        {
            var closingDisclosure = this.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList.FirstOrDefault(d => d.TransactionId != null && d.TransactionId.Equals(transactionId, StringComparison.OrdinalIgnoreCase));
            var loanEstimate = this.sLoanEstimateDatesInfo.LoanEstimateDatesList.FirstOrDefault(d => d.TransactionId != null && d.TransactionId.Equals(transactionId, StringComparison.OrdinalIgnoreCase));

            if (closingDisclosure != null || loanEstimate != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Applies a set of vendor provided disclosure metadata to the loan.
        /// </summary>
        /// <param name="disclosureMetadata">The disclosure metadata.</param>
        /// <returns>A boolean indicating whether changes were made to the loan file.</returns>
        [DependsOn(nameof(sClosingDisclosureDatesInfo), nameof(sCalculateInitialClosingDisclosure), nameof(sClosingCostArchive), nameof(GetBorrowerNamesByConsumerId), nameof(sLoanEstimateDatesInfo), nameof(sCalculateInitialLoanEstimate), nameof(ApplyBorrowerMetadataToClosingDisclosure), nameof(ApplyBorrowerMetadataToLoanEstimate))]
        private bool ApplyDisclosureMetadataFromVendor(VendorProvidedDisclosureMetadata disclosureMetadata)
        {
            if (disclosureMetadata == null)
            {
                return false;
            }

            if (disclosureMetadata.IsClosingDisclosure)
            {
                var closingDisclosure = this.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList
                    .SingleOrDefault(d => d.TransactionId.Equals(disclosureMetadata.TransactionId));

                if (closingDisclosure == null)
                {
                    Tools.LogInfo($"Document metadata could not be updated. No Closing Disclosure found with transaction ID {disclosureMetadata.TransactionId} for loan {this.sLId}.");
                    return false;
                }

                var cachedClosingDisclosure = closingDisclosure.DuplicatePreservingId();

                if (disclosureMetadata.IssuedDate != null)
                {
                    closingDisclosure.IssuedDate_rep = disclosureMetadata.IssuedDate;
                    closingDisclosure.IssuedDateLckd = true;
                }

                if (disclosureMetadata.DeliveryMethod != null)
                {
                    closingDisclosure.DeliveryMethod_rep = disclosureMetadata.DeliveryMethod;
                    closingDisclosure.DeliveryMethodLckd = true;
                }

                if (disclosureMetadata.ReceivedDate != null)
                {
                    closingDisclosure.ReceivedDate_rep = disclosureMetadata.ReceivedDate;
                    closingDisclosure.ReceivedDateLckd = true;
                }
                
                if (disclosureMetadata.IsPreview != null)
                {
                    closingDisclosure.IsPreview_rep = disclosureMetadata.IsPreview;
                }
                
                if (disclosureMetadata.IsFinal != null)
                {
                    closingDisclosure.IsFinal_rep = disclosureMetadata.IsFinal;
                }
                
                if (disclosureMetadata.LastDisclosedTRIDLoanProductDescription != null)
                {
                    closingDisclosure.LastDisclosedTRIDLoanProductDescription = disclosureMetadata.LastDisclosedTRIDLoanProductDescription;
                }
                
                if (disclosureMetadata.DocVendorApr != null)
                {
                    closingDisclosure.DocVendorApr_rep = disclosureMetadata.DocVendorApr;
                }
                
                if (disclosureMetadata.SignedDate != null)
                {
                    closingDisclosure.SignedDate_rep = disclosureMetadata.SignedDate;
                    closingDisclosure.SignedDateLckd = true;
                }
                
                if (disclosureMetadata.DocCode != null)
                {
                    closingDisclosure.DocCode = disclosureMetadata.DocCode;
                }
                
                if (disclosureMetadata.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower != null)
                {
                    closingDisclosure.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower_rep = disclosureMetadata.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower;
                }

                if (disclosureMetadata.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller != null)
                {
                    closingDisclosure.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller_rep = disclosureMetadata.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller;
                }
                
                if (disclosureMetadata.IsDisclosurePostClosingDueToNonNumericalClericalError != null)
                {
                    closingDisclosure.IsDisclosurePostClosingDueToNonNumericalClericalError_rep = disclosureMetadata.IsDisclosurePostClosingDueToNonNumericalClericalError;
                }

                if (disclosureMetadata.IsDisclosurePostClosingDueToCureForToleranceViolation != null)
                {
                    closingDisclosure.IsDisclosurePostClosingDueToCureForToleranceViolation_rep = disclosureMetadata.IsDisclosurePostClosingDueToCureForToleranceViolation;
                }
                
                if (disclosureMetadata.PostConsummationRedisclosureReasonDate != null)
                {
                    closingDisclosure.PostConsummationRedisclosureReasonDate_rep = disclosureMetadata.PostConsummationRedisclosureReasonDate;
                }
                
                if (disclosureMetadata.PostConsummationKnowledgeOfEventDate != null)
                {
                    closingDisclosure.PostConsummationKnowledgeOfEventDate_rep = disclosureMetadata.PostConsummationKnowledgeOfEventDate;
                }

                // Applied last, so recalculation (if applicable) can take into account the relevant vendor-provided dates.
                if (this.sCalculateInitialClosingDisclosure)
                {
                    this.sClosingDisclosureDatesInfo.CalculateInitialClosingDisclosure(this.sCalculateInitialClosingDisclosure, this.sClosingCostArchive);
                }
                else if (disclosureMetadata.IsInitial != null)
                {
                    closingDisclosure.IsInitial_rep = disclosureMetadata.IsInitial;
                    if (closingDisclosure.IsInitial)
                    {
                        foreach (var otherClosingDisclosure in this.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList)
                        {
                            if (otherClosingDisclosure.UniqueId != closingDisclosure.UniqueId)
                            {
                                otherClosingDisclosure.IsInitial = false;
                            }
                        }
                    }
                }

                // Only apply borrower-level metadata when the CD date is
                // tracking disclosure data at the borrower level.
                if (closingDisclosure.DisclosureDatesByConsumerId.Count != 0)
                {
                    foreach (var borrowerMetadata in disclosureMetadata.BorrowerMetadata)
                    {
                        this.ApplyBorrowerMetadataToClosingDisclosure(closingDisclosure, borrowerMetadata, disclosureMetadata.TransactionId);
                    }
                }

                var auditItem = DisclosureMetadataUpdateAuditHelper.CreateAuditEvent(
                    oldData: cachedClosingDisclosure,
                    newData: closingDisclosure,
                    borrowerNamesByConsumerId: this.GetBorrowerNamesByConsumerId());

                if (auditItem != null)
                {
                    m_alwaysAuditItems.Add(auditItem);
                }

                // Necessary to trigger serialization in the setter.
                this.sClosingDisclosureDatesInfo = this.sClosingDisclosureDatesInfo;
                return true;
            }
            else
            {
                var loanEstimate = this.sLoanEstimateDatesInfo.LoanEstimateDatesList
                    .SingleOrDefault(d => d.TransactionId.Equals(disclosureMetadata.TransactionId));

                if (loanEstimate == null)
                {
                    Tools.LogInfo($"Document metadata could not be updated. No Loan Estimate found with transaction ID {disclosureMetadata.TransactionId} for loan {this.sLId}.");
                    return false;
                }

                var cachedLoanEstimate = loanEstimate.DuplicatePreservingId();

                if (disclosureMetadata.IssuedDate != null)
                {
                    loanEstimate.IssuedDate_rep = disclosureMetadata.IssuedDate;
                    loanEstimate.IssuedDateLckd = true;
                }

                if (disclosureMetadata.DeliveryMethod != null)
                {
                    loanEstimate.DeliveryMethod_rep = disclosureMetadata.DeliveryMethod;
                    loanEstimate.DeliveryMethodLckd = true;
                }
                
                if (disclosureMetadata.ReceivedDate != null)
                {
                    loanEstimate.ReceivedDate_rep = disclosureMetadata.ReceivedDate;
                    loanEstimate.ReceivedDateLckd = true;
                }
                
                if (disclosureMetadata.LastDisclosedTRIDLoanProductDescription != null)
                {
                    loanEstimate.LastDisclosedTRIDLoanProductDescription = disclosureMetadata.LastDisclosedTRIDLoanProductDescription;
                }

                if (disclosureMetadata.DocVendorApr != null)
                {
                    loanEstimate.DocVendorApr_rep = disclosureMetadata.DocVendorApr;
                }
                
                if (disclosureMetadata.SignedDate != null)
                {
                    loanEstimate.SignedDate_rep = disclosureMetadata.SignedDate;
                    loanEstimate.SignedDateLckd = true;
                }

                // Applied last, so recalculation (if applicable) can take into account the relevant vendor-provided dates.
                if (this.sCalculateInitialLoanEstimate)
                {
                    this.sLoanEstimateDatesInfo.CalculateInitialLoanEstimate(this.sCalculateInitialLoanEstimate, this.sClosingCostArchive);
                }
                else if (disclosureMetadata.IsInitial != null)
                {
                    loanEstimate.IsInitial_rep = disclosureMetadata.IsInitial;
                    if (loanEstimate.IsInitial)
                    {
                        foreach (var otherLoanEstimate in this.sLoanEstimateDatesInfo.LoanEstimateDatesList)
                        {
                            if (otherLoanEstimate.UniqueId != loanEstimate.UniqueId)
                            {
                                otherLoanEstimate.IsInitial = false;
                            }
                        }
                    }
                }

                // Only apply borrower-level metadata when the LE date is
                // tracking disclosure data at the borrower level.
                if (loanEstimate.DisclosureDatesByConsumerId.Count != 0)
                {
                    foreach (var borrowerMetadata in disclosureMetadata.BorrowerMetadata)
                    {
                        this.ApplyBorrowerMetadataToLoanEstimate(loanEstimate, borrowerMetadata, disclosureMetadata.TransactionId);
                    }
                }

                var auditItem = DisclosureMetadataUpdateAuditHelper.CreateAuditEvent(
                    oldData: cachedLoanEstimate,
                    newData: loanEstimate,
                    borrowerNamesByConsumerId: this.GetBorrowerNamesByConsumerId());

                if (auditItem != null)
                {
                    m_alwaysAuditItems.Add(auditItem);
                }

                // Necessary to trigger serialization in the setter.
                this.sLoanEstimateDatesInfo = this.sLoanEstimateDatesInfo;
                return true;
            }
        }

        /// <summary>
        /// Applies metadata for a single borrower to an LE metadata row.
        /// </summary>
        /// <param name="loanEstimate">The LE row.</param>
        /// <param name="borrowerMetadata">The borrower metadata.</param>
        /// <param name="transactionId">The LE transaction ID.</param>
        [DependsOn(nameof(RetrieveConsumerIdByNameAndEmail))]
        private void ApplyBorrowerMetadataToLoanEstimate(LoanEstimateDates loanEstimate, VendorProvidedBorrowerMetadata borrowerMetadata, string transactionId)
        {
            Guid consumerId;
            if (borrowerMetadata.ConsumerId == null || !Guid.TryParse(borrowerMetadata.ConsumerId, out consumerId))
            {
                // No consumer ID included, fallback to routing using borrower information.
                consumerId = this.RetrieveConsumerIdByNameAndEmail(
                    borrowerMetadata.BorrFirstName,
                    borrowerMetadata.BorrMiddleName,
                    borrowerMetadata.BorrLastName,
                    borrowerMetadata.BorrSuffix,
                    borrowerMetadata.BorrFullName,
                    borrowerMetadata.BorrEmail);
            }

            if (consumerId != Guid.Empty)
            {
                var borrowerRowKvp = loanEstimate.DisclosureDatesByConsumerId.FirstOrDefault(d => d.Key == consumerId);

                if (borrowerRowKvp.Equals(default(KeyValuePair<Guid, BorrowerDisclosureDates>)))
                {
                    Tools.LogInfo($"Transaction {transactionId} does not contain borrower data for consumer ID {consumerId}");
                }
                else
                {
                    var borrowerRow = borrowerRowKvp.Value;

                    if (borrowerMetadata.DeliveryMethod != null)
                    {
                        borrowerRow.DeliveryMethod_rep = borrowerMetadata.DeliveryMethod;
                    }
                    
                    if (borrowerMetadata.IssuedDate != null)
                    {
                        borrowerRow.IssuedDate_rep = borrowerMetadata.IssuedDate;
                    }
                    
                    if (borrowerMetadata.ReceivedDate != null)
                    {
                        borrowerRow.ReceivedDate_rep = borrowerMetadata.ReceivedDate;
                    }
                    
                    if (borrowerMetadata.SignedDate != null)
                    {
                        borrowerRow.SignedDate_rep = borrowerMetadata.SignedDate;
                    }
                }
            }
        }

        /// <summary>
        /// Applies metadata for a single borrower to a CD metadata row.
        /// </summary>
        /// <param name="closingDisclosure">The CD row.</param>
        /// <param name="borrowerMetadata">The borrower metadata.</param>
        /// <param name="transactionId">The LE transaction ID.</param>
        [DependsOn(nameof(RetrieveConsumerIdByNameAndEmail))]
        private void ApplyBorrowerMetadataToClosingDisclosure(ClosingDisclosureDates closingDisclosure, VendorProvidedBorrowerMetadata borrowerMetadata, string transactionId)
        {
            Guid consumerId;
            if (borrowerMetadata.ConsumerId == null || !Guid.TryParse(borrowerMetadata.ConsumerId, out consumerId))
            {
                // No consumer ID included, fallback to routing using borrower information.
                consumerId = this.RetrieveConsumerIdByNameAndEmail(
                    borrowerMetadata.BorrFirstName,
                    borrowerMetadata.BorrMiddleName,
                    borrowerMetadata.BorrLastName,
                    borrowerMetadata.BorrSuffix,
                    borrowerMetadata.BorrFullName,
                    borrowerMetadata.BorrEmail);
            }

            if (consumerId != Guid.Empty)
            {
                var borrowerRowKvp = closingDisclosure.DisclosureDatesByConsumerId.FirstOrDefault(d => d.Key == consumerId);

                if (borrowerRowKvp.Equals(default(KeyValuePair<Guid, BorrowerDisclosureDates>)))
                {
                    Tools.LogInfo($"Transaction {transactionId} does not contain borrower data for consumer ID {consumerId}");
                }
                else
                {
                    var borrowerRow = borrowerRowKvp.Value;

                    if (borrowerMetadata.DeliveryMethod != null)
                    {
                        borrowerRow.DeliveryMethod_rep = borrowerMetadata.DeliveryMethod;
                    }

                    if (borrowerMetadata.IssuedDate != null)
                    {
                        borrowerRow.IssuedDate_rep = borrowerMetadata.IssuedDate;
                    }

                    if (borrowerMetadata.ReceivedDate != null)
                    {
                        borrowerRow.ReceivedDate_rep = borrowerMetadata.ReceivedDate;
                    }

                    if (borrowerMetadata.SignedDate != null)
                    {
                        borrowerRow.SignedDate_rep = borrowerMetadata.SignedDate;
                    }
                }
            }
        }

        /// <summary>
        /// Attempts to infer some disclosure dates based on the ESign event.
        /// </summary>
        /// <param name="transactionId">The transaction id of the ESign event..</param>
        /// <param name="disclosureT">The disclosure type.</param>
        /// <param name="borrowerName">The borrower name.</param>
        /// <param name="borrowerType">The borrower type.</param>
        /// <param name="appIdAndBorrowerT">A tuple containing the application ID and borrower type.</param>
        /// <returns>A boolean indicating whether changes were made to the disclosure dates.</returns>
        private bool InferLeCdDatesFromESignEvent(string transactionId, E_EDisclosureDisclosureEventT disclosureT, string borrowerName, E_aTypeT? borrowerType, Tuple<Guid, E_BorrowerModeT> appIdAndBorrowerT)
        {
            bool datesUpdated = false;
            Action<Func<LoanEstimateDates, DateTime>, Action<LoanEstimateDates>> updateAssociatedLeDateFieldIfBlank = (dateGetter, dateSetter) =>
            {
                var le = this.sLoanEstimateDatesInfo.LoanEstimateDatesList.FirstOrDefault(p => string.Equals(p.TransactionId, transactionId, StringComparison.OrdinalIgnoreCase));
                if (le != null && dateGetter(le) == DateTime.MinValue)
                {
                    dateSetter(le);
                    datesUpdated = true;
                }
            };
            Action<Func<ClosingDisclosureDates, DateTime>, Action<ClosingDisclosureDates>> updateAssociatedCdDateFieldIfBlank = (dateGetter, dateSetter) =>
            {
                var cd = this.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList.FirstOrDefault(p => string.Equals(p.TransactionId, transactionId, StringComparison.OrdinalIgnoreCase));
                if (cd != null && dateGetter(cd) == DateTime.MinValue)
                {
                    dateSetter(cd);
                    datesUpdated = true;
                }
            };

            if (BrokerDB.AutoSaveLeCdReceivedDate)
            {
                bool updateOnDocumentSigned = disclosureT == E_EDisclosureDisclosureEventT.DocumentsSigned &&
                    !string.IsNullOrWhiteSpace(borrowerName) && borrowerType.HasValue && borrowerType.Value.EqualsOneOf(E_aTypeT.Individual, E_aTypeT.CoSigner);

                if ((disclosureT == E_EDisclosureDisclosureEventT.DocumentsReviewed
                    || disclosureT == E_EDisclosureDisclosureEventT.EConsentReceivedForAllParties
                    || disclosureT == E_EDisclosureDisclosureEventT.ESignCompleted
                    || (disclosureT == E_EDisclosureDisclosureEventT.EConsentReceived && HasBorrowerEConsented(appIdAndBorrowerT))
                    || updateOnDocumentSigned))
                {
                    updateAssociatedLeDateFieldIfBlank(
                        le => le.ReceivedDate,
                        le => { le.ReceivedDate = DateTime.Today; le.ReceivedDateLckd = true; });
                }

                // OPM 236645 - Autopopulate CD receive date.
                if (!BrokerDB.DisableAutoPopulationOfCdReceivedDate)
                {
                    bool shouldCheckForEconsentReceived = !(BrokerDB.RequireAllBorrowersToReceiveCD || this.sLoanRescindableT == LoanRescindableT.Blank || this.sLoanRescindableT == LoanRescindableT.Rescindable);

                    if ((disclosureT == E_EDisclosureDisclosureEventT.DocumentsReviewed ||
                         disclosureT == E_EDisclosureDisclosureEventT.ESignCompleted ||
                         disclosureT == E_EDisclosureDisclosureEventT.EConsentReceivedForAllParties ||
                         (shouldCheckForEconsentReceived && disclosureT == E_EDisclosureDisclosureEventT.EConsentReceived && HasBorrowerEConsented(appIdAndBorrowerT))))
                    {
                        updateAssociatedCdDateFieldIfBlank(
                            cd => cd.ReceivedDate,
                            cd => { cd.ReceivedDate = DateTime.Today; cd.ReceivedDateLckd = true; });
                    }
                }
            }

            if (BrokerDB.AutoSaveLeCdSignedDate)
            {
                bool isESignCompleted = disclosureT == E_EDisclosureDisclosureEventT.ESignCompleted;
                bool updateOnDocumentSigned = disclosureT == E_EDisclosureDisclosureEventT.DocumentsSigned && BrokerDB.AutoSaveLeSignedDateWhenAnyBorrowerSigns &&
                    !string.IsNullOrWhiteSpace(borrowerName) && borrowerType.HasValue && borrowerType.Value.EqualsOneOf(E_aTypeT.Individual, E_aTypeT.CoSigner);

                if (isESignCompleted || updateOnDocumentSigned)
                {
                    updateAssociatedLeDateFieldIfBlank(
                        le => le.SignedDate,
                        le => { le.SignedDate = DateTime.Today; le.SignedDateLckd = true; });
                }

                if (isESignCompleted)
                {
                    updateAssociatedCdDateFieldIfBlank(
                        cd => cd.SignedDate,
                        cd => { cd.SignedDate = DateTime.Today; cd.SignedDateLckd = true; });
                }
            }

            return datesUpdated;
        }

        /// <summary>
        /// Attempts to map string name and email data to a specific borrower in a collection of applications.
        /// </summary>
        /// <param name="firstName">The borrower's first name.</param>
        /// <param name="middleName">The borrower's middle name.</param>
        /// <param name="lastName">The borrower's last name.</param>
        /// <param name="suffix">The borrower's name suffix.</param>
        /// <param name="fullName">The borrower's unparsed name.</param>
        /// <param name="email">The borrower's email.</param>
        /// <returns>
        /// The matching borrower's consumer ID, if one can be found. Otherwise the empty GUID if there is either no match,
        /// or more than one matching borrower.
        /// </returns>
        /// <remarks>
        /// This method is prone to ambiguous results and should be phased out once we get the document vendors to use the consumer ID.
        /// </remarks>
        [DependsOn(nameof(CAppBase.aFirstNm), nameof(CAppBase.aMidNm), nameof(CAppBase.aLastNm), nameof(CAppBase.aSuffix), nameof(CAppBase.aEmail), nameof(CAppBase.aNm), nameof(CAppBase.aIsValidNameSsn), nameof(CAppBase.aConsumerId))]
        private Guid RetrieveConsumerIdByNameAndEmail(string firstName, string middleName, string lastName, string suffix, string fullName, string email)
        {
            var consumerIds = new List<Guid>();
            foreach (var app in this.m_dataApps)
            {
                foreach (E_BorrowerModeT mode in Enum.GetValues(typeof(E_BorrowerModeT)))
                {
                    app.BorrowerModeT = mode;

                    if (!app.aIsValidNameSsn)
                    {
                        continue;
                    }

                    bool firstNameMatch = string.IsNullOrEmpty(firstName) || firstName.Equals(app.aFirstNm, StringComparison.OrdinalIgnoreCase);
                    bool middleNameMatch = string.IsNullOrEmpty(middleName) || middleName.Equals(app.aMidNm, StringComparison.OrdinalIgnoreCase);
                    bool lastNameMatch = string.IsNullOrEmpty(lastName) || lastName.Equals(app.aLastNm, StringComparison.OrdinalIgnoreCase);
                    bool suffixMatch = string.IsNullOrEmpty(suffix) || suffix.Equals(app.aSuffix, StringComparison.OrdinalIgnoreCase);

                    bool parsedNameMatch = firstNameMatch && middleNameMatch && lastNameMatch && suffixMatch;

                    bool unparsedNameMatch = string.IsNullOrEmpty(fullName) || fullName.Equals(app.aNm, StringComparison.OrdinalIgnoreCase);
                    bool emailMatch = string.IsNullOrEmpty(email) || email.Equals(app.aEmail, StringComparison.OrdinalIgnoreCase);
                    
                    bool borrowerMatch = parsedNameMatch && unparsedNameMatch && emailMatch;

                    if (borrowerMatch)
                    {
                        consumerIds.Add(app.aConsumerId);
                    }
                }
            }

            if (consumerIds.Count() == 1)
            {
                return consumerIds.First();
            }
            else if (consumerIds.Count() > 1)
            {
                Tools.LogInfo($"Could not route to correct borrower. Ambiguous name and email data provided.");
            }

            return Guid.Empty;
        }

        /// <summary>
        /// Builds a collection of borrower names keyed by consumer IDs.
        /// </summary>
        /// <returns>A dictionary of borrower names keyed by consumer IDs.</returns>
        [DependsOn(nameof(CAppBase.aIsValidNameSsn), nameof(CAppBase.aNm), nameof(CAppBase.aConsumerId))]
        private Dictionary<Guid, string> GetBorrowerNamesByConsumerId()
        {
            var namesByConsumerIds = new Dictionary<Guid, string>();
            foreach (var app in this.m_dataApps)
            {
                foreach (E_BorrowerModeT mode in Enum.GetValues(typeof(E_BorrowerModeT)))
                {
                    app.BorrowerModeT = mode;

                    if (!app.aIsValidNameSsn)
                    {
                        continue;
                    }

                    namesByConsumerIds[app.aConsumerId] = app.aNm;
                }
            }

            return namesByConsumerIds;
        }

        [DependsOn(nameof(CAppBase.aBTypeT), nameof(CAppBase.aCTypeT), nameof(sLoanRescindableT))]
        private int sfHasBorrowerEConsented
        {
            get { return 0; }
        }

        /// <summary>
        /// Given a disclosure event of type <see cref="E_EDisclosureDisclosureEventT.EConsentReceived"/> for a given borrower,
        /// validates if a borrower can E-Consent.
        /// </summary>
        /// <param name="appIdAndBorrowerT">The identifier to the borrower who gave E-Consent.</param>
        /// <returns>A boolean indicating whether the person consenting is a borrower on the loan who can E-Consent.</returns>
        private bool HasBorrowerEConsented(Tuple<Guid, E_BorrowerModeT> appIdAndBorrowerT)
        {
            if (appIdAndBorrowerT == null)
            {
                return false;
            }

            CAppBase app = this.GetAppData(appIdAndBorrowerT.Item1);
            if (app == null)
            {
                return false; // this is verifying that the app id specified was indeed valid.
            }

            if (this.sLoanRescindableT != LoanRescindableT.NotRescindable)
            {
                return true;
            }
            else if (appIdAndBorrowerT.Item2 == E_BorrowerModeT.Borrower && !app.aBTypeT.EqualsOneOf(E_aTypeT.TitleOnly, E_aTypeT.NonTitleSpouse, E_aTypeT.CurrentTitleOnly))
            {
                return true;
            }
            else if (appIdAndBorrowerT.Item2 == E_BorrowerModeT.Coborrower && !app.aCTypeT.EqualsOneOf(E_aTypeT.TitleOnly, E_aTypeT.NonTitleSpouse, E_aTypeT.CurrentTitleOnly))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Processes EDisclosure triggers resulting from DocMagic email notifications.
        /// </summary>
        /// <param name="disclosureT">Type of disclosure.</param>
        /// <param name="emailDetails">Details associated with the DM email.</param>
        /// <param name="dateReceived">The date the email was received.</param>
        public void ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT disclosureT,
            DocMagicEmailDetails emailDetails, DateTime dateReceived)
        {
            string auditMsg = String.Empty;
            Tuple<Guid, E_BorrowerModeT> appIdAndBorrowerT = null;
            switch (disclosureT)
            {
                case E_EDisclosureDisclosureEventT.DM_EConsentReceived:
                    appIdAndBorrowerT = GetApplicationAndBorrowerFromString(emailDetails.ReceivedFor);
                    if (appIdAndBorrowerT != null)
                    {
                        CAppBase dataApp = GetAppData(appIdAndBorrowerT.Item1);
                        if (appIdAndBorrowerT.Item2 == E_BorrowerModeT.Borrower)
                        {
                            dataApp.aBEConsentReceivedD = CDateTime.Create(dateReceived);
                        }
                        else
                        {
                            dataApp.aCEConsentReceivedD = CDateTime.Create(dateReceived);
                        }
                    }
                    // 11/11/2013 gf - opm 143983
                    if (sEConsentCompleted && sDisclosuresDueD.IsValid
                        && (sDisclosureNeededT == E_sDisclosureNeededT.EConsentNotReceived_PaperDiscReqd
                        || sDisclosureNeededT == E_sDisclosureNeededT.EConsentDec_PaperDiscReqd
                        || sDisclosureNeededT == E_sDisclosureNeededT.AwaitingEConsent))
                    {
                        // Allow any time on the third business day.
                        if (IsConsentWithinThreeBusinessDays(dateReceived))
                        {
                            ProcessManualDisclosureTrigger(false);
                        }
                    }
                    auditMsg = String.Format("E-Consent Received for {0}",
                        emailDetails.ReceivedFor);
                    break;
                case E_EDisclosureDisclosureEventT.DM_EConsentDeclined:
                    // Note: if this case block is ever updated to mirror the LQB e-sign update
                    // code that sets sNeedInitialDisc, sNeedRedisc, and sDisclosureNeededT when
                    // e-consent is declined, please make sure to check the value of 
                    // sIsEnableEConsentMonitoringAutomation as the e-sign update code does 
                    // to make sure we honor the value of the lender setting to enable/disable 
                    // compliance automation.
                    appIdAndBorrowerT = GetApplicationAndBorrowerFromString(emailDetails.ReceivedFor);
                    if (appIdAndBorrowerT != null)
                    {
                        CAppBase dataApp = GetAppData(appIdAndBorrowerT.Item1);
                        if (appIdAndBorrowerT.Item2 == E_BorrowerModeT.Borrower)
                        {
                            dataApp.aBEConsentDeclinedD = CDateTime.Create(dateReceived);
                        }
                        else
                        {
                            dataApp.aCEConsentDeclinedD = CDateTime.Create(dateReceived);
                        }
                    }
                    auditMsg = String.Format("E-Consent Declined by {0} - Paper Disclosure Required",
                        emailDetails.ReceivedFor);
                    break;
                case E_EDisclosureDisclosureEventT.DM_DocumentReviewCompleted:
                    auditMsg = "Documents Reviewed";
                    // 7/11/2014 gf - opm 186787
                    if (!this.sNeedInitialDisc && !this.sNeedRedisc &&
                        (this.sDisclosureNeededT == E_sDisclosureNeededT.AwaitingEConsent ||
                        this.sDisclosureNeededT == E_sDisclosureNeededT.None))
                    {
                        sDisclosuresDueD_rep = "";
                        sDisclosureNeededT = E_sDisclosureNeededT.None;
                    }
                    sTilGfeRd = CDateTime.Create(dateReceived);
                    break;
                case E_EDisclosureDisclosureEventT.DM_ESignCompleted:
                    auditMsg = "E-Sign Completed";
                    // 7/11/2014 gf - opm 186787
                    if (!this.sNeedInitialDisc && !this.sNeedRedisc &&
                        (this.sDisclosureNeededT == E_sDisclosureNeededT.AwaitingEConsent ||
                        this.sDisclosureNeededT == E_sDisclosureNeededT.None))
                    {
                        sDisclosuresDueD_rep = "";
                        sDisclosureNeededT = E_sDisclosureNeededT.None;
                    }
                    sTilGfeRd = CDateTime.Create(dateReceived);
                    break;
                default:
                    throw new CBaseException(ErrorMessages.Generic, "This method " +
                        "only handles DocMagic email notifications. Either unimplemented " +
                        "disclosure trigger or wrong method.");
            }

            var auditItem = new DisclosureESignAuditItem(Guid.Empty, "System Notification", auditMsg);
            m_alwaysAuditItems.Add(auditItem);
        }

        /// <summary>
        /// Handle generic e-disclosure disclosure triggers that are not directly
        /// tied to DocMagic or the Document Framework.
        /// </summary>
        /// <param name="disclosureT">The type of the disclosure trigger.</param>
        public void ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT disclosureT)
        {
            string auditMsg = String.Empty;

            switch(disclosureT)
            {
                case E_EDisclosureDisclosureEventT.EConsentExpired:
                    if (!this.sIsEnableEConsentMonitoringAutomation)
                    {
                        // Skip setting disclosure state values or auditing
                        // when automation is disabled.
                        return;
                    }

                    // 7/31/2014 gf - opm 182647, change this disclosure status
                    // to be associated with initial disclosure instead of re-
                    // disclosure.
                    sNeedInitialDisc = true;
                    sNeedRedisc = false;
                    sDisclosureNeededT = E_sDisclosureNeededT.EConsentNotReceived_PaperDiscReqd;
                    auditMsg = "E-Consent Not Received - Paper Disclosure Required";
                    break;
                default:
                    throw new CBaseException(ErrorMessages.Generic, "This method " +
                        "only handles generic e-disclosure triggers. Either unimplemented " +
                        "disclosure trigger or wrong method.");
            }

            var auditItem = new DisclosureESignAuditItem(Guid.Empty, "System Notification", auditMsg);
            m_alwaysAuditItems.Add(auditItem);
        }

        [DependsOn(nameof(CAppBase.aBNm), nameof(CAppBase.aCNm))]
        private int sfGetApplicationAndBorrowerFromString
        {
            get { return 0; }
        }
        /// <summary>
        /// Determines the borrower and application that the given name refers to.
        /// </summary>
        /// <remarks>
        /// We are expecting the borrower name to be of the format:
        ///     {First} {Middle} {Last}, {Suffix}
        /// </remarks>
        /// <param name="borrowerName">The name of the borrower.</param>
        /// <returns>
        /// A tuple containing the application id and the borrower that matches.
        /// Returns null if the application and borrower could not be determined.
        /// </returns>
        private Tuple<Guid, E_BorrowerModeT> GetApplicationAndBorrowerFromString(string borrowerName)
        {
            var nonLettersAndDigitsRegex = new Regex("[^A-Za-z0-9]"); // see opm 239771 for the inspiration for this sanitization format - we've had problems mainly with commas and periods
            Func<string, string> sanitizeEDisclosureName = name => nonLettersAndDigitsRegex.Replace(name, string.Empty);
            string borrowerNameForComparison = sanitizeEDisclosureName(borrowerName?.Trim() ?? string.Empty);
            if (string.IsNullOrEmpty(borrowerNameForComparison))
            {
                Tools.LogErrorWithCriticalTracking("[EDisclosure] Unable to determine application / borrower from empty borrower name.");
                return null;
            }

            string[] borrowersOnFile = new string[this.nApps * 2];
            for (int i = 0; i < this.nApps; i++)
            {
                CAppBase dataApp = this.GetAppData(i);
                var aBNm = dataApp.aBNm.Trim();
                var aCNm = dataApp.aCNm.Trim();

                if (sanitizeEDisclosureName(aBNm).Equals(borrowerNameForComparison, StringComparison.OrdinalIgnoreCase))
                {
                    return Tuple.Create(dataApp.aAppId, E_BorrowerModeT.Borrower);
                }
                else if (sanitizeEDisclosureName(aCNm).Equals(borrowerNameForComparison, StringComparison.OrdinalIgnoreCase))
                {
                    return Tuple.Create(dataApp.aAppId, E_BorrowerModeT.Coborrower);
                }

                borrowersOnFile[(i * 2)] = aBNm;
                borrowersOnFile[(i * 2) + 1] = aCNm;
            }

            Tools.LogWarning(
                "[EDisclosure] Unable to determine application / borrower based on supplied information." + Environment.NewLine
                + "sLId: " + this.sLId + Environment.NewLine
                + "borrower: " + borrowerName + Environment.NewLine
                + "borrowers on file: " + Environment.NewLine
                + " - " + string.Join(Environment.NewLine + " - ", borrowersOnFile) + Environment.NewLine
                + Environment.NewLine
                + "Borrowers were compared after first scrubbing extra characters via the regular expression " + nonLettersAndDigitsRegex);
            return null;
        }

        [DependsOn(nameof(sfRemoveDisclosure))]
        private int sfProcessManualDisclosureTrigger
        {
            get { return 0; }
        }

        public void ProcessManualDisclosureTrigger()
        {
            RemoveDisclosure(false, true);
        }

        public void ProcessManualDisclosureTrigger(bool includeAudit)
        {
            RemoveDisclosure(false, includeAudit);
        }

        private const string ClearedMessage = "Removed from Disclosure Pipeline";
        private const string MarkedMessage = "Marked as Manually Disclosed";
        /// <summary>
        /// Removes the (re)disclosure field information from a loan.
        /// OPM 173000, 8/13/2015, ML
        /// </summary>
        /// <param name="isClearingOnly">
        /// If we're not marking the loan as disclosed in addition to clearing it from
        /// the disclosure pipeline.
        /// </param>
        /// <param name="includeMarkingAudit">
        /// If we want to update the loan's audit history to indicate that we
        /// marked it as disclosed.
        /// </param>
        public void RemoveDisclosure(bool isClearingOnly, bool includeMarkingAudit)
        {
            sNeedInitialDisc = false;
            sNeedRedisc = false;
            sDisclosureNeededT = E_sDisclosureNeededT.None;
            sDisclosuresDueD_rep = "";
            sAutoDisclosureFailed = false;

            // We don't want to set the last disclosed date if we're just clearing the
            // loan from the pipeline:
            // 1. For loans that are in the initial disclosure state, this will flag the file
            //    to the automation that initial disclosures were completed and to begin tracking
            //    redisclosure triggers.
            // 2. For loans that are past the initial disclosure state, the last disclosure date
            //    should be preserved for audit purposes.
            if (!isClearingOnly)
            {
                sLastDisclosedD = CDateTime.Create(DateTime.Now);
            }

            if (isClearingOnly || includeMarkingAudit)
            {
                AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;

                string auditMessage = isClearingOnly ? ClearedMessage : MarkedMessage;

                var auditItem = new DisclosureESignAuditItem(principal.UserId,
                    principal.DisplayName, auditMessage);

                m_alwaysAuditItems.Add(auditItem);
            }

            // Make sure that processing field changes / workflow rules for
            // disclosure will not overwrite these changes.
            m_markingLoanAsDisclosed = true;
        }

        // OPM 173000, 8/13/2015, ML
        [DependsOn(nameof(sNeedInitialDisc), nameof(sNeedRedisc), nameof(sDisclosureNeededT), nameof(sDisclosuresDueD), nameof(sLastDisclosedD), nameof(sAutoDisclosureFailed))]
        private int sfRemoveDisclosure
        {
            get { return 0; }
        }

        /// <summary>
        /// Clears the disclosure from the pipeline without marking it as disclosed.
        /// </summary>
        [DependsOn(nameof(sfRemoveDisclosure))]
        public void ClearDisclosureWithoutMarking()
        {
            this.RemoveDisclosure(true, false);
        }

        [DependsOn(nameof(sNeedInitialDisc), nameof(sNeedRedisc), nameof(sDisclosureNeededT), nameof(sDisclosuresDueD), nameof(sAutoDisclosureFailed))]
        public void MigrateDisclosureStateOffEConsentMonitoringAutomation()
        {
            var disclosureNeedTypeAuditName = new Dictionary<E_sDisclosureNeededT, string>()
            {
                [E_sDisclosureNeededT.EConsentDec_PaperDiscReqd] = "E-Consent Declined - Paper Disclosure Required",
                [E_sDisclosureNeededT.EConsentNotReceived_PaperDiscReqd] = "E-Consent Not Received - Paper Disclosure Required",
                [E_sDisclosureNeededT.AwaitingEConsent] = "Awaiting E-Consent"
            };

            var fieldSnapshot = new List<Tuple<string, string, string>>(4)
            {
                Tuple.Create("Need Initial Disclosures", sNeedInitialDisc.ToString(), bool.FalseString),
                Tuple.Create("Need Re-Disclosures", sNeedRedisc.ToString(), bool.FalseString),
                Tuple.Create("Disclosure Needed Type", disclosureNeedTypeAuditName[sDisclosureNeededT], "None"),
                Tuple.Create("Disclosure Due Date", sDisclosuresDueD_rep, string.Empty),
            };
            this.m_alwaysAuditItems.Add(new DisableEConsentDisclosureMonitoringAuditItem(fieldSnapshot));

            sNeedInitialDisc = false;
            sNeedRedisc = false;
            sDisclosureNeededT = E_sDisclosureNeededT.None;
            sDisclosuresDueD_rep = "";
            sAutoDisclosureFailed = false;

            // Make sure that processing field changes / workflow rules for
            // disclosure will not overwrite these changes.
            m_markingLoanAsDisclosed = true;
        }

        /// <summary>
        /// Determine if the given disclosure is a Changed Circumstance type.
        /// </summary>
        /// <param name="disclosureT">Type of disclosure.</param>
        /// <returns>True if this is a changed circumstance trigger. Otherwise, false.</returns>
        private bool IsChangedCircumstance(E_sDisclosureNeededT disclosureT)
        {
            switch (disclosureT)
            {
                case E_sDisclosureNeededT.APROutOfTolerance_RediscReqd:
                case E_sDisclosureNeededT.EConsentDec_PaperDiscReqd:
                case E_sDisclosureNeededT.EConsentNotReceived_PaperDiscReqd:
                case E_sDisclosureNeededT.InitDisc_LoanRegistered:
                case E_sDisclosureNeededT.InitDisc_RESPAAppReceived:
                case E_sDisclosureNeededT.InitDisc_RESPAAppReceivedVerbally:
                case E_sDisclosureNeededT.AwaitingEConsent:
                case E_sDisclosureNeededT.None:
                    return false;
                case E_sDisclosureNeededT.CC_FloatDownLock:
                case E_sDisclosureNeededT.CC_GFEChargeDiscountChanged:
                case E_sDisclosureNeededT.CC_LoanAmtChanged:
                case E_sDisclosureNeededT.CC_LoanLocked:
                case E_sDisclosureNeededT.CC_LockExtended:
                case E_sDisclosureNeededT.CC_ProgramChanged:
                case E_sDisclosureNeededT.CC_PropertyValueChanged:
                case E_sDisclosureNeededT.CC_Relocked:
                case E_sDisclosureNeededT.CC_UWCreditScoreChanged:
                case E_sDisclosureNeededT.CC_ImpoundChanged:
                case E_sDisclosureNeededT.CC_PurchasePriceChanged:
                case E_sDisclosureNeededT.Custom:
                    return true;
                default:
                    Tools.LogWarning("Unhandled enum value for E_sDisclosureNeededT");
                    return false;
            }
        }
    }
}
