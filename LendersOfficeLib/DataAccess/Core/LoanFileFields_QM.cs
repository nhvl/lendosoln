using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOfficeApp.los.RatePrice;
using LendersOffice.Constants;
using LendersOffice.FFIEC;
using LendersOffice.ObjLib.CustomAttributes;
using LendersOffice.Common;

namespace DataAccess
{
    public partial class CPageBase : CBase, IGetSymbolTable, IAmortizationDataProvider
    {
        #region QM Calculations

        [DependsOn(nameof(sClosingCostFeeVersionT), nameof(sSettlementChargesExportSource))]
        private E_SettlementChargesExportSource sQmSettlementChargesExportSource
        {
            get
            {
                // OPM 198065 - When 2015 closing cost fees are enabled sQmSettlementChargesExportSource should
                // awlays return GFE.
                switch (sClosingCostFeeVersionT)
                {
                    case E_sClosingCostFeeVersionT.Legacy:
                        return sSettlementChargesExportSource;
                    case E_sClosingCostFeeVersionT.LegacyButMigrated:
                    case E_sClosingCostFeeVersionT.ClosingCostFee2015:
                        return E_SettlementChargesExportSource.GFE;
                    default:
                        throw new UnhandledEnumException(sClosingCostFeeVersionT);
                }
            }
        }

        [DependsOn]
        public bool sQMAveragePrimeOfferRLck
        {
            get { return GetBoolField("sQMAveragePrimeOfferRLck"); }
            set { SetBoolField("sQMAveragePrimeOfferRLck", value); }
        }


        /// <summary>
        /// Returns 0 if it cannot determine the apor
        /// </summary>
        [DependsOn(nameof(sQMAveragePrimeOfferR), nameof(sFinMethT), nameof(sInterestRateSetD), nameof(sDue), nameof(sRAdj1stCapMon), nameof(sQMAveragePrimeOfferRLck), nameof(sHmdaActionD))]
        public decimal sQMAveragePrimeOfferR
        {
            get
            {
                if (sQMAveragePrimeOfferRLck)
                {
                    return GetRateField("sQMAveragePrimeOfferR");
                }
                decimal apor = YieldTable.LookUp(sFinMethT, this.sInterestRateSetD_rep, sDue_rep, sRAdj1stCapMon_rep, sHmdaActionD);
                return apor == Decimal.MinValue ? 0 : apor;
            }
            set
            {
                SetRateField("sQMAveragePrimeOfferR", value);
            }
        }
        
        /// <summary>
        /// Returns 0 if it cannot determine the apor
        /// </summary>
        [DependsOn(nameof(sQMAveragePrimeOfferR), nameof(sFinMethT), nameof(sInterestRateSetD), nameof(sDue), nameof(sRAdj1stCapMon), nameof(sHmdaActionD))]
        public decimal sQMAveragePrimeOfferRCalc
        {
            get
            {
                decimal apor = YieldTable.LookUp(sFinMethT, this.sInterestRateSetD_rep, sDue_rep, sRAdj1stCapMon_rep, sHmdaActionD);
                return apor == Decimal.MinValue ? 0 : apor;
            }
        }
        
        public string sQMAveragePrimeOfferR_rep
        {
            get
            {
                if (sQMAveragePrimeOfferR == decimal.MinValue)
                {
                    return "N/A";
                }

                return ToRateString(() => sQMAveragePrimeOfferR);
            }

            set
            {
                sQMAveragePrimeOfferR = ToRate(value);
            }
        }

        [DependsOn]
        public decimal sQMParR
        {
            get { return GetRateField("sQMParR"); }
            private set { SetRateField("sQMParR", value); }
        }

        public string sQMParR_rep
        {
            get { return ToRateString(() => sQMParR); }
            set { sQMParR = ToRate(value); }
        }

        [DependsOn]
        public decimal sQMNonExcludableDiscountPointsPc
        {
            get { return GetRateField("sQMNonExcludableDiscountPointsPc"); }
            private set { SetRateField("sQMNonExcludableDiscountPointsPc", value); }
        }

        public string sQMNonExcludableDiscountPointsPc_rep
        {
            get { return ToRateString(() => sQMNonExcludableDiscountPointsPc); }
            set { sQMNonExcludableDiscountPointsPc = ToRate(value); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sQMNonExcludableDiscountPointsPc), nameof(sLDiscntBaseAmt), nameof(sSettlementLDiscntBaseAmt))]
        public decimal sQMNonExcludableDiscountPointsF
        {
            get
            {
                if( sQmSettlementChargesExportSource == E_SettlementChargesExportSource.GFE)
                    return sQMNonExcludableDiscountPointsPc / 100 * sLDiscntBaseAmt;
                else
                    return sQMNonExcludableDiscountPointsPc / 100 * sSettlementLDiscntBaseAmt;
            }
        }

        public string sQMNonExcludableDiscountPointsF_rep
        {
            get { return ToMoneyString(() => sQMNonExcludableDiscountPointsF); }
        }

        [DependsOn(nameof(sQMNonExcludableDiscountPointsF), nameof(sQMGfeDiscountPointF))]
        public decimal sQMDiscountPointsF_SansNonExcludable
        {
            get
            {
                return sQMGfeDiscountPointF - sQMNonExcludableDiscountPointsF;
            }
        }

        public string sQMDiscountPointsF_SansNonExcludable_rep
        {
            get { return ToMoneyString(() => sQMDiscountPointsF_SansNonExcludable); }
        }

        [DependsOn(nameof(sQMNonExcludableDiscountPointsPc), nameof(sLDiscntBaseAmt))]
        public decimal sQMNonExcludableDiscountPointsFComplianceEase
        {
            get
            {
                return sQMNonExcludableDiscountPointsPc / 100 * sLDiscntBaseAmt;
            }
        }

        public string sQMNonExcludableDiscountPointsFComplianceEase_rep
        {
            get { return ToMoneyString(() => sQMNonExcludableDiscountPointsFComplianceEase); }
        }

        [DependsOn(nameof(sGfeDiscountPointFComplianceEase), nameof(sQMNonExcludableDiscountPointsFComplianceEase))]
        public decimal sQMDiscountPointFComplianceEase_SansNonExcludable
        {
            get
            {
                return sGfeDiscountPointFComplianceEase - sQMNonExcludableDiscountPointsFComplianceEase;
            }
        }

        public string sQMDiscountPointFComplianceEase_SansNonExcludable_rep
        {
            get { return ToMoneyString(() => sQMDiscountPointFComplianceEase_SansNonExcludable); }
        }

        [DependsOn(nameof(sClosingCostFeeVersionT), nameof(sClosingCostSet), nameof(sfGetQMamount))]
        public decimal sQMTotFeeAmount
        {
            get
            {
                switch ( sClosingCostFeeVersionT )
                {
                    case E_sClosingCostFeeVersionT.Legacy:
                        return GetQMAmount(false);
                    case E_sClosingCostFeeVersionT.LegacyButMigrated:
                    case E_sClosingCostFeeVersionT.ClosingCostFee2015:
                        return SumClosingCostQMAmtTotals(false);
                    default:
                        throw new UnhandledEnumException(sClosingCostFeeVersionT);
                }
            }
        }

        public string sQMTotFeeAmount_rep
        {
            get { return ToMoneyString(() => sQMTotFeeAmount); }
        }

        [DependsOn(nameof(sClosingCostFeeVersionT), nameof(sClosingCostSet), nameof(sfGetQMamount))]
        public decimal sQMTotFinFeeAmount
        {
            get
            {
                switch (sClosingCostFeeVersionT)
                {
                    case E_sClosingCostFeeVersionT.Legacy:
                        return GetQMAmount(true);
                    case E_sClosingCostFeeVersionT.LegacyButMigrated:
                    case E_sClosingCostFeeVersionT.ClosingCostFee2015:
                        return SumClosingCostQMAmtTotals(true);
                    default:
                        throw new UnhandledEnumException(sClosingCostFeeVersionT);
                }
            }
        }

        public string sQMTotFinFeeAmount_rep
        {
            get { return ToMoneyString(() => sQMTotFinFeeAmount); }
        }

        [DependsOn]
        public bool sQMExclDiscntPntLck
        {
            get { return GetBoolField("sQMExclDiscntPntLck"); }
            set { SetBoolField("sQMExclDiscntPntLck", value); }
        }

        // OPM 198065 - Note: sGfeDiscountPointFProps is from old GFE fee, consider updating once 2015 fees are in effect
        [DependsOn(nameof(sQMExclDiscntPntLck), nameof(sQMExclDiscntPnt), nameof(sQMAveragePrimeOfferR), nameof(sQMParR), nameof(sGfeDiscountPointFProps))]
        public decimal sQMExclDiscntPnt
        {
            get
            {
                if (sQMExclDiscntPntLck)
                {
                    return GetRateField("sQMExclDiscntPnt");
                }

                if (!LosConvert.GfeItemProps_BF(sGfeDiscountPointFProps))
                {
                    return 0;
                }

                return GetExcludableDiscountPointsPc(sQMAveragePrimeOfferR, sQMParR);
            }
            set
            {
                SetRateField("sQMExclDiscntPnt", value);
            }
        }

        public string sQMExclDiscntPnt_rep
        {
            get
            {
                return ToRateString(() => sQMExclDiscntPnt);
            }
            set
            {
                sQMExclDiscntPnt = ToRate(value);
            }
        }

        public static decimal GetExcludableDiscountPointsPc(decimal APOR, decimal parRate)
        {
            decimal excludableDiscountPointsPc = 0.000M;

            if (parRate <= 0.000M || APOR == decimal.MinValue)
            {
                excludableDiscountPointsPc = 0.000M;
            }
            else if (parRate - APOR <= 1.000M)
            {
                excludableDiscountPointsPc = 2.000M;
            }
            else if (parRate - APOR <= 2.000M)
            {
                excludableDiscountPointsPc = 1.000M;
            }

            return excludableDiscountPointsPc;
        }

        public decimal GetExcessDiscountPointsPc(decimal discountPointsPc, decimal excludableDiscountPointsPc, bool isBonaFide)
        {
            if (isBonaFide)
            {
                return Math.Max(discountPointsPc - excludableDiscountPointsPc, 0.000M);
            }
            else
            {
                return discountPointsPc;
            }
        }

        [DependsOn(nameof(sQMFhaUfmipAtClosingLck))]
        public bool sQMFhaUfmipAtClosingLck
        {
            get { return GetBoolField("sQMFhaUfmipAtClosingLck"); }
            set { SetBoolField("sQMFhaUfmipAtClosingLck", value); }
        }
        [DependsOn(nameof(sQMFhaUfmipAtClosingLck), nameof(sQMFhaUfmipAtClosing), nameof(sDocMagicClosingD), nameof(sRLckdDays), nameof(sLPurposeT), nameof(sProdIsLoanEndorsedBeforeJune09), nameof(sLtvR), nameof(sTerm), nameof(sLAmt))]
        private decimal sQMFhaUfmipAtClosing
        {
            get
            {
                if (sQMFhaUfmipAtClosingLck)
                {
                    return GetDecimalField("sQMFhaUfmipAtClosing");
                }

                CDateTime closingD = sDocMagicClosingD;

                if (!closingD.IsValid)
                {
                    closingD = CDateTime.Create(DateTime.Today.AddDays(sRLckdDays));
                }
                return Calculate_FhaUfmip(closingD.DateTimeForComputation) * sLAmtCalc / 100;
            }
            set
            {
                SetDecimalField("sQMFhaUfmipAtClosing", value);
            }
        }

        public string sQMFhaUfmipAtClosing_rep
        {
            get { return ToMoneyString(() => sQMFhaUfmipAtClosing); }
            set { sQMFhaUfmipAtClosing = ToMoney(value); }
        }

        [DevNote("The maximum possible penalty for loan prepayment within 36 months.")]
        [DependsOn]
        public decimal sQMMaxPrePmntPenalty
        {
            get
            {
                return GetMoneyField("sQMMaxPrePmntPenalty");
            }
            set
            {
                SetMoneyField("sQMMaxPrePmntPenalty", value);
            }
        }

        public string sQMMaxPrePmntPenalty_rep
        {
            get { return ToMoneyString(() => sQMMaxPrePmntPenalty); }
            set { sQMMaxPrePmntPenalty = ToMoney(value); }
        }

        [DependsOn(nameof(sApr), nameof(sQMAveragePrimeOfferR))]
        private decimal sQMAprRSpread
        {
            get
            {
                return sApr - sQMAveragePrimeOfferR;
            }
        }

        public string sQMAprRSpread_rep
        {
            get { return ToRateString(() => sQMAprRSpread); }
        }

        [DependsOn(nameof(sQMParR), nameof(sQMAveragePrimeOfferR))]
        public string sQMParRSpreadCalcDesc
        {
            get
            {
                return string.Format("= {0} Par rate - {1} APOR", sQMParR_rep, sQMAveragePrimeOfferR_rep);
            }
        }

        [DependsOn(nameof(sApr), nameof(sQMAveragePrimeOfferR))]
        public string sQMAprRSpreadCalcDesc
        {
            get
            {
                return string.Format("= {0} APR - {1} APOR", sApr_rep, sQMAveragePrimeOfferR_rep);
            }
        }

        [DependsOn(nameof(sLT), nameof(sAprIncludedUfmip), nameof(sQMFhaUfmipAtClosing), nameof(sUfmipIsRefundableOnProRataBasis))]
        private decimal sQMExcessUpfrontMIP
        {
            get
            {
                decimal amount;
                if (sLT == E_sLT.Conventional || sLT == E_sLT.Other)
                {
                    if (sUfmipIsRefundableOnProRataBasis)
                    {
                        amount = Math.Max(this.sAprIncludedUfmip - sQMFhaUfmipAtClosing, 0);
                    }
                    else
                    {
                        amount = this.sAprIncludedUfmip;
                    }
                }
                else
                {
                    amount = 0;
                }

                return amount;
            }
        }

        [DependsOn(nameof(sQMExcessUpfrontMIP), nameof(sLT), nameof(sUfmipIsRefundableOnProRataBasis), nameof(sAprIncludedUfmip), nameof(sQMFhaUfmipAtClosing))]
        public string sQMExcessUpfrontMIPCalcDesc
        {
            get
            {
                if (sLT == E_sLT.Conventional || sLT == E_sLT.Other)
                {
                    if (sUfmipIsRefundableOnProRataBasis)
                    {
                        return string.Format("= {0} UFMIP (paid by borrower) - {1} FHA UFMIP at closing", this.sAprIncludedUfmip_rep, sQMFhaUfmipAtClosing_rep);
                    }
                    else
                    {
                        return "UFMIP is not refundable. Entire amount paid by borrower included.";
                    }
                }
                else
                {
                    return "FHA, VA, or USDA. Entire amount excluded.";
                }
            }
        }

        public string sQMExcessUpfrontMIP_rep
        {
            get
            {
                return ToMoneyString(() => sQMExcessUpfrontMIP);
            }
        }


        [DependsOn(nameof(sQMGfeDiscountPointFPc), nameof(sQMExclDiscntPnt), nameof(sQMNonExcludableDiscountPointsPc))]
        private decimal sQMExcessDiscntFPc
        {
            get
            {
                var qmNonExcludableDiscountPointsPc = sQMNonExcludableDiscountPointsPc;
                return Math.Max(sQMGfeDiscountPointFPc - qmNonExcludableDiscountPointsPc - sQMExclDiscntPnt, 0) + qmNonExcludableDiscountPointsPc;
            }
        }


        public string sQMExcessDiscntFPc_rep
        {
            get { return ToRateString(() => sQMExcessDiscntFPc); }
        }

        [DependsOn(nameof(sLDiscntFMb), nameof(sQMExcessDiscntFPc), nameof(sLDiscntBaseAmt))]
        private decimal sQMExcessDiscntF
        {
            get { return sLDiscntBaseAmt * sQMExcessDiscntFPc / 100; }
        }

        public string sQMExcessDiscntF_rep
        {
            get { return ToMoneyString(() => sQMExcessDiscntF); }
        }

        [DevNote("The discount point percentage for QM only if the discount point is paid by borrower.")]
        [DependsOn(nameof(sClosingCostFeeVersionT), nameof(sGfeDiscountPointFProps), nameof(sQMGfeDiscountPointF), nameof(sClosingCostSet), nameof(sLDiscntBaseAmt))]
        private decimal sQMBorrowerPaidDiscountPointFPc
        {
            get
            {
                decimal paymentTotal = 0;
                if (this.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.ClosingCostFee2015)
                {
                    // Paid by borrower
                    if (LosConvert.GfeItemProps_Payer(this.sGfeDiscountPointFProps) == LosConvert.BORR_PAID_OUTOFPOCKET || 
                        LosConvert.GfeItemProps_Payer(this.sGfeDiscountPointFProps) == LosConvert.BORR_PAID_FINANCED) 
                    {
                        paymentTotal = this.sQMGfeDiscountPointF;
                    }
                }
                else
                {
                    // Need payments marked as paid by borrower
                    paymentTotal = this.sClosingCostSet.Sum((fee) => fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId, (payment) => 
                        payment.PaidByT == E_ClosingCostFeePaymentPaidByT.Borrower || 
                        payment.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance);
                }

                return GetDiscountPointFPc(paymentTotal, this.sLDiscntBaseAmt);
            }
        }

        [DependsOn(nameof(sQMBorrowerPaidDiscountPointFPc))]
        private decimal sQMGfeDiscountPointFPc
        {
            get
            {
                return this.sQMBorrowerPaidDiscountPointFPc;
            }
        }
        
        public string sQMGfeDiscountPointFPc_rep
        {
            get { return ToRateString(() => sQMGfeDiscountPointFPc); }
        }

        [DependsOn(nameof(sQMGfeDiscountPointFPc), nameof(sQMExclDiscntPnt), nameof(sQMExcessDiscntFPc), nameof(sQMNonExcludableDiscountPointsPc))]
        public string sQMExcessDiscntFCalcDesc
        {
            get {
                string msg = "{0} Borrower paid discount points - {1} Excludable discount points";

                if (sQMNonExcludableDiscountPointsPc != 0)
                {
                    msg = "(" + msg + ")<br/>+ {2} Non-excludable discount points";
                    msg = string.Format(msg, ToRateString(() => sQMGfeDiscountPointFPc - sQMNonExcludableDiscountPointsPc),
                                        sQMExclDiscntPnt_rep, sQMNonExcludableDiscountPointsPc_rep);
                }
                else
                {
                    msg = string.Format(msg, sQMGfeDiscountPointFPc_rep, sQMExclDiscntPnt_rep);

                    if (sQMGfeDiscountPointFPc > 0 && sQMExcessDiscntFPc == 0)
                    {
                        msg += " (entire amount excluded)";
                    }
                }
                
                return msg;
            }
        }

        private decimal sQMParRSpread
        {
            get
            {
                return Math.Max(sQMParR - sQMAveragePrimeOfferR, 0);
            }
        }

        public string sQMParRSpread_rep
        {
            get { return ToRateString(() => sQMParRSpread); }
        }

        [DependsOn(nameof(sFinMethT), nameof(sNoteIR), nameof(sRAdj1stCapMon), nameof(sRAdjCapMon), nameof(sRAdj1stCapR), nameof(sRAdjLifeCapR), nameof(sRAdjCapR))]
        private decimal sQMQualR
        {
            get
            {
                if (sFinMethT == E_sFinMethT.Fixed || sFinMethT == E_sFinMethT.Graduated)
                {
                    return sNoteIR;
                }
                else if (sFinMethT == E_sFinMethT.ARM)
                {
                    if (sRAdj1stCapMon > 60)
                    {
                        return sNoteIR;
                    }
                    // 1/9/2014 AV - 148523 QM-QRATE Should be using max rate in first five years
                    int safe_sRAdjCapMon = sRAdjCapMon;

                    //assume 1 month recurring adjustments when sRAdjCapMon is incorrectly set to zero for an ARM.               
                    if (sRAdjCapMon <= 0)
                    {
                        safe_sRAdjCapMon = 1;
                    }

                    int n = (60 - sRAdj1stCapMon) / safe_sRAdjCapMon;
                    return Math.Min(sNoteIR + sRAdj1stCapR + n * sRAdjCapR, sNoteIR + sRAdjLifeCapR);
                }
                else
                {
                    throw new UnhandledEnumException(sFinMethT);
                }
            }
        }


        public string sQMQualR_rep
        {
            get { return ToRateString(() => sQMQualR); }
        }

        [DependsOn(nameof(sFinalLAmt), nameof(sQMQualR), nameof(sTerm))]
        private decimal sQMQPmt
        {
            get
            {
                double i = (double)sQMQualR / 1200;
                double iPower = Math.Pow(1 + i, (double)(-1 * sTerm));
                double payment = (double)sFinalLAmt * i / (1 - iPower);
                if (double.IsNaN(payment))
                {
                    return 0; 
                }
                return (decimal)Math.Round(payment, 2);
            }
        }

        [DependsOn(nameof(sLTotI), nameof(sQMQPmt), nameof(sTransmTotMonPmt), nameof(sProdDocT), nameof(sLPurposeTPe), nameof(sIsCreditQualifying), nameof(sHasNonOccupantCoborrower))]
        private decimal sQMQualBottom
        {
            get
            {
                if (sLTotI == 0 || sQMQPmt == 0)
                {
                    return Tools.EnsureValidRateValueWithMax(10000);
                }
                return Tools.EnsureValidRateValueWithMax(Calculate_sQualBottomR(sLTotI, sQMQPmt));
            }
        }

        public string sQMQualBottom_rep
        {
            get 
            {
                if (sQMQualBottom == 10000)
                {
                    return "N/A";
                }
                return ToRateString(() => sQMQualBottom); 
            }
        }


        [DependsOn(nameof(sQMMaxPointAndFeesAllowedAmt), nameof(sGfeOriginatorCompF), nameof(sQMLoanDoesNotHaveNegativeAmort), nameof(sQMLoanDoesNotHaveBalloonFeature), nameof(sQMLoanDoesNotHaveAmortTermOver30Yr), nameof(sQMLoanHasDtiLessThan43Pc), nameof(sQMGfeDiscountPointF), nameof(sLDiscntBaseAmt), nameof(sQMLAmt), nameof(sQMLAmtCalcDesc), nameof(sQMExcessUpfrontMIP), nameof(sGfeDiscountPointFProps), nameof(sQMExcessUpfrontMIPCalcDesc), nameof(sQMGfeDiscountPointFPc), nameof(sQMAveragePrimeOfferR), nameof(sQMQualR), nameof(sQMQualBottom), nameof(sQMTotFeeAmount), nameof(sQMTotFinFeeAmount), nameof(sQMFhaUfmipAtClosing), nameof(sQMMaxPrePmntPenalty), nameof(sQMExclDiscntPntLck), nameof(sQMExclDiscntPnt), nameof(sQMNonExcludableDiscountPointsPc), nameof(sLT), nameof(sProMInsR), nameof(sLienPosT), nameof(sIsConformingLAmt))]
        private int sf_GetQMPricingData{ get { return -1; } }


        public QMPricingFields GetQMPricingData()
        {
            using (PerformanceStopwatch.Start("GetQMPricingData"))
            {
                QMPricingFields qm = new QMPricingFields();
                qm.sQMAveragePrimeOfferR = sQMAveragePrimeOfferR;
                qm.sQMQualR = sQMQualR;
                qm.sQMQualBottom = sQMQualBottom;
                qm.sQMTotFeeAmount = sQMTotFeeAmount;
                qm.sQMTotFinFeeAmount = sQMTotFinFeeAmount;
                qm.sQMExclDiscntPntLck = sQMExclDiscntPntLck;
                qm.sQMExclDiscntPntNonCalc = sQMExclDiscntPnt;
                qm.sQMNonExcludableDiscountPointsPc = sQMNonExcludableDiscountPointsPc;
                qm.sQMFhaUfmipAtClosing = sQMFhaUfmipAtClosing;
                qm.sQMMaxPrePmntPenalty = sQMMaxPrePmntPenalty;
                qm.sGfeOriginatorCompF = sGfeOriginatorCompF;
                //qm.sQMAprRSpread = sQMAprRSpread_rep;
                //qm.sQMAprRSpreadCalcDesc = sQMAprRSpreadCalcDesc;
                qm.sQMGfeDiscountPointFPc = sQMGfeDiscountPointFPc;
                qm.sGfeDiscountPointFProps = sGfeDiscountPointFProps;
                qm.sQMLAmt = sQMLAmt;
                qm.sQMLAmtCalcDesc = sQMLAmtCalcDesc;
                qm.sQMGfeDiscountPointF = sQMGfeDiscountPointF;
                qm.sLDiscntBaseAmt = sLDiscntBaseAmt;
                qm.sQMLoanDoesNotHaveNegativeAmort = sQMLoanDoesNotHaveNegativeAmort;
                qm.sQMLoanDoesNotHaveBalloonFeature = sQMLoanDoesNotHaveBalloonFeature;
                qm.sQMLoanDoesNotHaveAmortTermOver30Yr = sQMLoanDoesNotHaveAmortTermOver30Yr;
                qm.sQMLoanHasDtiLessThan43Pc = sQMLoanHasDtiLessThan43Pc;
                qm.sQMMaxPointAndFeesAllowedAmt = sQMMaxPointAndFeesAllowedAmt;
                qm.sQMExcessUpfrontMIPCalcDesc = sQMExcessUpfrontMIPCalcDesc;
                qm.sQMExcessUpfrontMIP = sQMExcessUpfrontMIP;
                qm.sLT = sLT;
                qm.sProMInsR = sProMInsR;
                qm.sLienPosT = sLienPosT;
                qm.sIsConformingLAmt = sIsConformingLAmt;
                qm.sQMIsEligibleByLoanPurchaseAgency = BrokerDB.AssumePmlQmEligibleForGse;  // OPM 184815 - Set value based on broker bit.
                return qm;
            }
        }


        private static decimal CalcFullyIndexedRate(decimal sRAdjRoundToR, E_sRAdjRoundT sRAdjRoundT, decimal index, decimal margin, decimal initialRate, decimal lifetimeCap)
        {
            decimal fullyIndexedRate = Tools.SnapToGridLine(index + margin, sRAdjRoundToR, sRAdjRoundT);

            if (fullyIndexedRate > initialRate + lifetimeCap)
            {
                return initialRate + lifetimeCap;
            }
            else
            {
                return fullyIndexedRate;
            }
        }

        [DependsOn]
        public bool sUfmipIsRefundableOnProRataBasis
        {
            get { return GetBoolField("sUfmipIsRefundableOnProRataBasis"); }
            set { SetBoolField("sUfmipIsRefundableOnProRataBasis", value); }
        }


        [DependsOn(nameof(sQMGfeDiscountPointFPc), nameof(sQMParR), nameof(sNoteIR))]
        private decimal sQMDiscntBuyDownR
        {
            get
            {
                if (sQMGfeDiscountPointFPc <= 0)
                {
                    return decimal.MinValue;
                }
                else
                {
                    return (sQMParR - sNoteIR) / sQMGfeDiscountPointFPc;
                }
            }
        }

        public string sQMDiscntBuyDownR_rep
        {
            get
            {
                if (sQMDiscntBuyDownR == decimal.MinValue)
                {
                    return "N/A";
                }
                else
                {
                    return ToRateString(() => sQMDiscntBuyDownR) + " rate / point";
                }
            }
        }



        [DependsOn(nameof(sQMParR), nameof(sNoteIR), nameof(sQMGfeDiscountPointFPc))]
        public string sQMDiscntBuyDownRCalcDesc
        {
            get
            {
                return string.Format("= ({0} Par Rate - {1} Note Rate) / {2} Discount Points", sQMParR_rep, sNoteIR_rep, sQMGfeDiscountPointFPc_rep);
            }
        }

        [DependsOn(nameof(sFinancedAmt), nameof(sQMTotFinFeeAmount))]
        private decimal sQMLAmt
        {
            get
            {
                return sFinancedAmt - sQMTotFinFeeAmount;
            }
        }

        public string sQMLAmt_rep
        {
            get { return ToMoneyString(() => sQMLAmt); }
        }

        [DependsOn(nameof(sFinancedAmt), nameof(sQMTotFinFeeAmount))]
        public string sQMLAmtCalcDesc
        {
            get
            {
                return string.Format("= {0} Amount Financed - {1} Financed QM Fees", sFinancedAmt_rep, sQMTotFinFeeAmount_rep);
            }
        }

        // OPM 198065 - Note: sGfeOriginatorCompF is old GFE fee, consider updating once 2015 fees are in effect
        /// <summary>
        /// Only valid in LQB. 
        /// </summary>
        [DependsOn(nameof(sQMTotFeeAmount), nameof(sQMExcessUpfrontMIP), nameof(sQMMaxPrePmntPenalty), nameof(sQMExcessDiscntF), nameof(sGfeOriginatorCompF))]
        private decimal sQMTotFeeAmt
        {
            get { return SumMoney(sQMTotFeeAmount, sQMExcessUpfrontMIP, sQMMaxPrePmntPenalty, sQMExcessDiscntF, sGfeOriginatorCompF); }
        }

        public string sQMTotFeeAmt_rep
        {
            get { return ToMoneyString(() => sQMTotFeeAmt); }
        }

        [DependsOn(nameof(sQMTotFeeAmt), nameof(sQMLAmt))]
        private decimal sQMTotFeePc
        {
            get { return sQMTotFeeAmt / sQMLAmt * 100; }  
        }

        public string sQMTotFeePc_rep
        {
            get { return ToRateString(() => sQMTotFeePc); }
        }

        /// <summary>
        /// The "Amt" portion of these will be changed every year, the percent probably not.
        /// To update this calculation, add a new entry to the <see cref="ConstApp.QMPointsFeesByYear"/> dictionary. <para></para>
        /// We may need to be able to support the current year and the prior year at the same time, but not for the same loan file.
        /// </summary>
        private static decimal GetMaxPointsAndFees(decimal sFinalLAmt, decimal sQmTotalLoanAmt, CDateTime consummationDate)
        {
            decimal result = 0.00M;

            int consummationDateYear = consummationDate.DateTimeForComputation.Year;

            ConstApp.QMPointsFees currentYearPointsFees = null;

            if (consummationDateYear < 2015)
            {
                currentYearPointsFees = ConstApp.Pre2015QMPointsFees;
            }
            else if (!ConstApp.QMPointsFeesByYear.TryGetValue(consummationDateYear, out currentYearPointsFees))
            {
                // If the year is not found in our dictionary, we'll use the most
                // recent year.
                var latestYear = ConstApp.QMPointsFeesByYear.Keys.Max();

                currentYearPointsFees = ConstApp.QMPointsFeesByYear[latestYear];
            }

            if (sFinalLAmt >= currentYearPointsFees.tier1LAmt)
            {
                result = sQmTotalLoanAmt * currentYearPointsFees.tier1Pc / 100M;
            }
            else if ((sFinalLAmt >= currentYearPointsFees.tier2LAmt) && (sFinalLAmt < currentYearPointsFees.tier1LAmt))
            {
                result = currentYearPointsFees.tier2Amt;
            }
            else if ((sFinalLAmt >= currentYearPointsFees.tier3LAmt) && (sFinalLAmt < currentYearPointsFees.tier2LAmt))
            {
                result = sQmTotalLoanAmt * currentYearPointsFees.tier3Pc / 100M;
            }
            else if ((sFinalLAmt >= currentYearPointsFees.tier4LAmt) && (sFinalLAmt < currentYearPointsFees.tier3LAmt))
            {
                result = currentYearPointsFees.tier4Amt;
            }
            else if ((sFinalLAmt >= 0) && (sFinalLAmt < currentYearPointsFees.tier4LAmt))
            {
                result = sQmTotalLoanAmt * currentYearPointsFees.tier5Pc / 100M;
            }
            else
            {
                throw CBaseException.GenericException("Invalid loan amount.");
            }

            return Math.Max(result, 0);
        }

        public string sQMAllowablePointAndFeeDesc
        {
            get
            {
                var totAmt = sQMLAmt;
                var amt = GetMaxPointsAndFees(sFinalLAmt, totAmt, sQMConsummationD);
                var pc = amt / totAmt * 100;
                return string.Format("{0} {1}", ToMoneyString(() => amt), ToRateString(() => pc));
            }
        }

        [DependsOn(nameof(sDocMagicClosingD), nameof(sRLckdDays))]
        [DevNote("The field is set with the current date by the appraisal framework when the documents are received.")]
        private CDateTime sQMConsummationD
        {
            get
            {
                // OPM 191021: 
                // "Use the same date logic that sQMFhaUfmipAtClosing uses to determine what the closing date should be
                // (sDocMagicClosingD if it is available, otherwise Today + the rate lock period)" 
                CDateTime closingD = sDocMagicClosingD;

                if (!closingD.IsValid)
                {
                    closingD = CDateTime.Create(DateTime.Today.AddDays(sRLckdDays));
                }

                return closingD;
            }
        }

        [DependsOn(nameof(sFinalLAmt), nameof(sQMLAmt), nameof(sQMConsummationD))]
        private decimal sQMMaxPointAndFeesAllowedAmt
        {
            get
            {
                return GetMaxPointsAndFees(sFinalLAmt, sQMLAmt, sQMConsummationD);
            }
        }

        public string sQMMaxPointAndFeesAllowedAmt_rep
        {
            get { return ToMoneyString(() => sQMMaxPointAndFeesAllowedAmt); }
        }

        [DependsOn(nameof(sQMMaxPointAndFeesAllowedAmt), nameof(sQMLAmt))]
        private decimal sQMMaxPointAndFeesAllowedPc
        {
            get
            {
                return sQMMaxPointAndFeesAllowedAmt / sQMLAmt * 100;
            }
        }

        public string sQMMaxPointAndFeesAllowedPc_rep
        {
            get { return ToRateString(() => sQMMaxPointAndFeesAllowedPc); }
        }

        [DependsOn(nameof(sQMMaxPointAndFeesAllowedAmt), nameof(sQMTotFeeAmt))]
        public bool sQMLoanPassesPointAndFeesTest
        {
            get
            {
                return sQMMaxPointAndFeesAllowedAmt >= sQMTotFeeAmt;
            }
        }

        [DependsOn(nameof(sIsOptionArm), nameof(sIsIOnly))]
        public bool sQMLoanDoesNotHaveNegativeAmort
        {
            get { return !(sIsOptionArm || sIsIOnly); }
        }

        [DependsOn(nameof(sTerm), nameof(sDue))]
        public bool sQMLoanDoesNotHaveAmortTermOver30Yr
        {
            get { return !(sTerm > 360 || sDue > 360); }
        }

        [DependsOn(nameof(sIsBalloon))]
        public bool sQMLoanDoesNotHaveBalloonFeature
        {
            get { return !sIsBalloon; }
        }

        [DependsOn(nameof(sQMQualBottom))]
        public bool sQMLoanHasDtiLessThan43Pc
        {
            get { return sQMQualBottom <= 43.0M; }
        }

        [DependsOn]
        public bool sQMIsVerifiedIncomeAndAssets
        {
            get { return GetBoolField("sQMIsVerifiedIncomeAndAssets"); }
            set { SetBoolField("sQMIsVerifiedIncomeAndAssets", value); }
        }


        [DependsOn(nameof(sLT), nameof(sQMLoanPurchaseAgency))]
        public E_sQMLoanPurchaseAgency sQMLoanPurchaseAgency
        {
            get
            {
                switch (sLT)
                {
                    case E_sLT.FHA:
                        return E_sQMLoanPurchaseAgency.FHA;
                    case E_sLT.VA:
                        return E_sQMLoanPurchaseAgency.VA;
                    case E_sLT.UsdaRural:
                        return E_sQMLoanPurchaseAgency.USDA;
                    case E_sLT.Conventional:
                    case E_sLT.Other:
                        return (E_sQMLoanPurchaseAgency)GetTypeIndexField("sQMLoanPurchaseAgency");
                    default:
                        throw new UnhandledEnumException(sLT);
                }

            }
            set { SetTypeIndexField("sQMLoanPurchaseAgency", (int)value); }
        }

        private static Dictionary<E_sQMLoanPurchaseAgency, string> s_sQMLoanPurchaseAgency_Map = new Dictionary<E_sQMLoanPurchaseAgency, string>()
        {
            { E_sQMLoanPurchaseAgency.Blank, "" },
            { E_sQMLoanPurchaseAgency.FannieMae, "Fannie Mae" },
            { E_sQMLoanPurchaseAgency.FHA, "FHA" },
            { E_sQMLoanPurchaseAgency.FreddieMac, "Freddie Mac" },
            { E_sQMLoanPurchaseAgency.USDA, "USDA" },
            { E_sQMLoanPurchaseAgency.VA, "VA" }
        };

        public static string sQMLoanPurchaseAgency_map_rep(E_sQMLoanPurchaseAgency sQMLoanPurchaseAgency)
        {
            return s_sQMLoanPurchaseAgency_Map[sQMLoanPurchaseAgency];
        }

        [DependsOn(nameof(sLT))]
        public bool sQMLoanPurchaseAgencyIsDisabled
        {
            get { return !(sLT == E_sLT.Conventional || sLT == E_sLT.Other); }
        }

        [DependsOn(nameof(sQMIsEligibleByLoanPurchaseAgency))]
        public bool sQMIsEligibleByLoanPurchaseAgency
        {
            get { return GetBoolField("sQMIsEligibleByLoanPurchaseAgency"); }
            set { SetBoolField("sQMIsEligibleByLoanPurchaseAgency", value); }

        }
        [DependsOn(nameof(sComplianceEaseStatusT))]
        public bool sQMLoanHasComplianceChecksOnFile
        {
            //2/20/2014 av 170245 Disable the ComplianceEase QM check
            get { return true; }
            //get { return sComplianceEaseStatusT != E_sComplianceEaseStatusT.Updating && sComplianceEaseStatusT != E_sComplianceEaseStatusT.Undefined; }
        }

        [DependsOn(nameof(sComplianceEaseStatusT))]
        public bool sQMLoanPassedComplianceChecks
        {
            get
            {
                return true;
            }
        }

        [DependsOn(nameof(sLAmtCalc), nameof(sNoteIR), nameof(sIsLineOfCredit), nameof(sFinMethT), nameof(sQMLoanPassesPointAndFeesTest), nameof(sQMLoanDoesNotHaveNegativeAmort), nameof(sQMLoanDoesNotHaveBalloonFeature), nameof(sQMLoanDoesNotHaveAmortTermOver30Yr), nameof(sQMLoanHasDtiLessThan43Pc), nameof(sQMIsEligibleByLoanPurchaseAgency), nameof(sQMLoanHasComplianceChecksOnFile), nameof(sQMLoanPassedComplianceChecks), nameof(sQMIsVerifiedIncomeAndAssets))]
        public E_sQMStatusT sQMStatusT
        {
            get
            {
                if (sLAmtCalc == 0 || sNoteIR == 0 || sIsLineOfCredit || sFinMethT == E_sFinMethT.Graduated)
                {
                    return E_sQMStatusT.Blank;
                }

                bool[] checks = new bool[] { 
                    sQMLoanPassesPointAndFeesTest,
                    sQMLoanDoesNotHaveNegativeAmort,
                    sQMLoanDoesNotHaveBalloonFeature,
                    sQMLoanDoesNotHaveAmortTermOver30Yr
                };

                if (checks.Any(p => p == false))
                {
                    return E_sQMStatusT.Ineligible;
                }

                if (!sQMLoanHasDtiLessThan43Pc && !sQMIsEligibleByLoanPurchaseAgency)
                {
                    return E_sQMStatusT.Ineligible;
                }

                if (sQMLoanHasComplianceChecksOnFile && !sQMLoanPassedComplianceChecks)
                {
                    return E_sQMStatusT.Ineligible;
                }

                if (!sQMIsVerifiedIncomeAndAssets && !sQMIsEligibleByLoanPurchaseAgency)
                {
                    return E_sQMStatusT.ProvisionallyEligible;
                }

                if (!checks.Any(p => p == false))
                {
                    return E_sQMStatusT.Eligible;
                }
                if (sQMLoanHasDtiLessThan43Pc || sQMIsVerifiedIncomeAndAssets || sQMIsEligibleByLoanPurchaseAgency)
                {
                    return E_sQMStatusT.Eligible;
                }

                if (sQMLoanHasComplianceChecksOnFile && sQMLoanPassedComplianceChecks)
                {
                    return E_sQMStatusT.Eligible;
                }

                throw CBaseException.GenericException("Unhandled qm Status. Reached the end.");

            }
        }

        private static Dictionary<E_sQMStatusT, string> s_sQMStatusT_Map = new Dictionary<E_sQMStatusT, string>()
        {
            { E_sQMStatusT.Blank, "" },
            { E_sQMStatusT.Eligible, "Eligible" },
            { E_sQMStatusT.Ineligible, "Ineligible" },
            { E_sQMStatusT.ProvisionallyEligible, "Provisionally Eligible" }
        };

        public static string sQMStatusT_map_rep(E_sQMStatusT sQMStatusT)
        {
            return s_sQMStatusT_Map[sQMStatusT];
        }

        ///<summary>
        ///True if the system was unable to automatically determine third-party
        ///or affiliate status for one or more fee recipients.
        ///</summary>
        [DependsOn(nameof(sClosingCostSet), nameof(sQMShowWarning_Old), nameof(sOriginatorCompensationPaymentSourceT), nameof(sGfeIsTPOTransaction))]
        [DevNote("Show QM Warning on QM Calculations and Point and Fees page.")]
        public bool sQMShowWarning
        {
            get
            {
                switch (sClosingCostFeeVersionT)
                {
                    case E_sClosingCostFeeVersionT.Legacy:
                        return sQMShowWarning_Old;
                    case E_sClosingCostFeeVersionT.LegacyButMigrated:
                    case E_sClosingCostFeeVersionT.ClosingCostFee2015:
                        Func<BaseClosingCostFee, bool> bFilter;
                        if (this.sClosingCostSet.HasDataLoanAssociate)
                        {
                            bFilter = bFee => ClosingCostSetUtils.OrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostFee)bFee, this.sOriginatorCompensationPaymentSourceT, this.sDisclosureRegulationT, this.sGfeIsTPOTransaction);
                        }
                        else
                        {
                            bFilter = bFee => ClosingCostSetUtils.ExOrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostFee)bFee);
                        }

                        foreach (BorrowerClosingCostFee fee in sClosingCostSet.GetFees(bFilter))
                        {
                            if (fee.IsShowQmWarning)
                            {
                                return true;
                            }
                        }

                        return false;
                    default:
                        throw new UnhandledEnumException(sClosingCostFeeVersionT);
                }
            }
        }

        protected virtual decimal SumClosingCostQMAmtTotals(bool financed)
        {
            decimal totalFeeAmount = 0;
            Func<BaseClosingCostFee, bool> bFilter;
            if (this.sClosingCostSet.HasDataLoanAssociate)
            {
                bFilter = bFee => ClosingCostSetUtils.OrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostFee)bFee, this.sOriginatorCompensationPaymentSourceT, this.sDisclosureRegulationT, this.sGfeIsTPOTransaction);
            }
            else
            {
                bFilter = bFee => ClosingCostSetUtils.ExOrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostFee)bFee);
            }
            
            foreach (BorrowerClosingCostFee fee in sClosingCostSet.GetFees(bFilter))
            {
                totalFeeAmount += financed ? fee.FinancedQmAmount : fee.QmAmount;
            }
            return totalFeeAmount;
        }


        #endregion 

        #region OBSOLETE - PRE-QM2015 - DO NOT USE POST 2015 CLOSING COST FEE CHANGES
        #region OBSOLETE ACCESS FUNCTIONS
        private string GetQMField_Desc(E_SettlementChargesExportSource settlementSource, string gfeDesc, string scDesc)
        {
            if (settlementSource == E_SettlementChargesExportSource.SETTLEMENT)
                return scDesc;
            else
                return gfeDesc;
        }

        private decimal GetQMField(E_SettlementChargesExportSource settlementSource, decimal gfeField, decimal scField)
        {
            if (settlementSource == E_SettlementChargesExportSource.SETTLEMENT)
                return scField;
            else
                return gfeField;
        }

        private int GetQMField_Props(E_SettlementChargesExportSource settlementSource, int gfeProps, int scProps)
        {
            if (settlementSource == E_SettlementChargesExportSource.SETTLEMENT)
                return scProps;
            else
                return gfeProps;
        }

        private void SetQMField_Props(E_SettlementChargesExportSource settlementSource, Action<int> setGfeProps, Action<int> setScProps, int newProps)
        {
            if (settlementSource == E_SettlementChargesExportSource.SETTLEMENT)
                setScProps(newProps);
            else
                setGfeProps(newProps);
        }

        private decimal GetQMField_QMAmount(decimal field, int props, bool requiresApr)
        {
            // updated in opm 203073 https://lqbopm/default.asp?203073#BugEvent.1526080

            int payer = LosConvert.GfeItemProps_Payer(props);

            if (payer == 0 || payer == 1)
            {
                if (LosConvert.GfeItemProps_Apr(props))
                {
                    if (false == (LosConvert.GfeItemProps_PaidToThirdParty(props) && false == LosConvert.GfeItemProps_ThisPartyIsAffiliate(props)))
                    {
                        return field;
                    }
                }
            }

            if (false == requiresApr)
            {
                if (LosConvert.GfeItemProps_PaidToThirdParty(props) && LosConvert.GfeItemProps_ThisPartyIsAffiliate(props))
                {
                    return field;
                }
            }


            return 0;
        }
        #endregion

        #region OBSOLETE QM Calculations
        [DependsOn(nameof(sQMLOrigF), nameof(sQMLOrigFProps), nameof(sQMApprF), nameof(sQMApprFProps), nameof(sQMCrF), nameof(sQMCrFProps), nameof(sQMTxServF), nameof(sQMTxServFProps), nameof(sQMFloodCertificationF), nameof(sQMFloodCertificationFProps), nameof(sQMMBrokF), nameof(sQMMBrokFProps), nameof(sQMInspectF), nameof(sQMInspectFProps), nameof(sQMProcF), nameof(sQMProcFProps), nameof(sQMUwF), nameof(sQMUwFProps), nameof(sQMWireF), nameof(sQMWireFProps), nameof(sQM800U1F), nameof(sQM800U1FProps), nameof(sQM800U2F), nameof(sQM800U2FProps), nameof(sQM800U3F), nameof(sQM800U3FProps), nameof(sQM800U4F), nameof(sQM800U4FProps), nameof(sQM800U5F), nameof(sQM800U5FProps), nameof(sQMHazInsPia), nameof(sQMHazInsPiaProps), nameof(sQM904Pia), nameof(sQM904PiaProps), nameof(sQM900U1Pia), nameof(sQM900U1PiaProps), nameof(sQMHazInsRsrv), nameof(sQMHazInsRsrvProps), nameof(sQMMInsRsrv), nameof(sQMMInsRsrvProps), nameof(sQMRealETxRsrv), nameof(sQMRealETxRsrvProps), nameof(sQMSchoolTxRsrv), nameof(sQMSchoolTxRsrvProps), nameof(sQMFloodInsRsrv), nameof(sQMFloodInsRsrvProps), nameof(sQMAggregateAdjRsrv), nameof(sQMAggregateAdjRsrvProps), nameof(sQM1006Rsrv), nameof(sQM1006RsrvProps), nameof(sQM1007Rsrv), nameof(sQM1007RsrvProps), nameof(sQMEscrowF), nameof(sQMEscrowFProps), nameof(sQMOwnerTitleInsF), nameof(sQMOwnerTitleInsFProps), nameof(sQMTitleInsF), nameof(sQMTitleInsFProps), nameof(sQMDocPrepF), nameof(sQMDocPrepFProps), nameof(sQMNotaryF), nameof(sQMNotaryFProps), nameof(sQMAttorneyF), nameof(sQMAttorneyFProps), nameof(sQMU1Tc), nameof(sQMU1TcProps), nameof(sQMU2Tc), nameof(sQMU2TcProps), nameof(sQMU3Tc), nameof(sQMU3TcProps), nameof(sQMU4Tc), nameof(sQMU4TcProps), nameof(sQMRecF), nameof(sQMRecFProps), nameof(sQMCountyRtc), nameof(sQMCountyRtcProps), nameof(sQMStateRtc), nameof(sQMStateRtcProps), nameof(sQMU1GovRtc), nameof(sQMU1GovRtcProps), nameof(sQMU2GovRtc), nameof(sQMU2GovRtcProps), nameof(sQMU3GovRtc), nameof(sQMU3GovRtcProps), nameof(sQMPestInspectF), nameof(sQMPestInspectFProps), nameof(sQMU1Sc), nameof(sQMU1ScProps), nameof(sQMU2Sc), nameof(sQMU2ScProps), nameof(sQMU3Sc), nameof(sQMU3ScProps), nameof(sQMU4Sc), nameof(sQMU4ScProps), nameof(sQMU5Sc), nameof(sQMU5ScProps), nameof(sQMU3Rsrv), nameof(sQMU3RsrvProps), nameof(sQMU4Rsrv), nameof(sQMU4RsrvProps))]
        private int sfGetQMamount { get { return -1; } }

        /// <summary>
        /// Factors settlement charges into the equation
        /// </summary>
        /// <param name="financed"></param>
        /// <returns></returns>
        private decimal GetQMAmount(bool financed)
        {
            // the reason to use Funcs below is for performance, since you don't always need to get the QMAmount or the fee, don't always need to call the getter.
            var gfeLineItems = new[] {
 
                    #region gfe apr line items
                        new { QMFee = new Func<decimal>(() => sQMLOrigF), QMAmount = new Func<decimal>(() => sQMLOrigF_QMAmount), Props = new Func<int>(() => sQMLOrigFProps), RequiresAPR = true},
                        //new { QMFee = new Func<decimal>(() => sQMGfeOriginatorCompF), QMAmount = new Func<decimal>(() => sQMGfeOriginatorCompF_QMAmount), Props = new Func<int>(() => sQMGfeOriginatorCompFProps), RequiresAPR = true},
                        //new { QMFee = new Func<decimal>(() => sGfeLenderCreditF), QMAmount = new Func<decimal>(() => sGfeLenderCreditF_QMAmount), Props = new Func<int>(() => sGfeLenderCreditFProps), RequiresAPR = true},
                        //new { QMFee = new Func<decimal>(() => sQMGfeDiscountPointF), QMAmount = new Func<decimal>(() => sQMGfeDiscountPointF_QMAmount), Props = new Func<int>(() => sQMGfeDiscountPointFProps), RequiresAPR = true},
                        new { QMFee = new Func<decimal>(() => sQMApprF), QMAmount = new Func<decimal>(() => sQMApprF_QMAmount), Props = new Func<int>(() => sQMApprFProps), RequiresAPR = false},
                        new { QMFee = new Func<decimal>(() => sQMCrF), QMAmount = new Func<decimal>(() => sQMCrF_QMAmount), Props = new Func<int>(() => sQMCrFProps), RequiresAPR = false},
                        new { QMFee = new Func<decimal>(() => sQMTxServF), QMAmount = new Func<decimal>(() => sQMTxServF_QMAmount), Props = new Func<int>(() => sQMTxServFProps), RequiresAPR = true},
                        new { QMFee = new Func<decimal>(() => sQMFloodCertificationF), QMAmount = new Func<decimal>(() => sQMFloodCertificationF_QMAmount), Props = new Func<int>(() => sQMFloodCertificationFProps), RequiresAPR = false},
                        new { QMFee = new Func<decimal>(() => sQMMBrokF), QMAmount = new Func<decimal>(() => sQMMBrokF_QMAmount), Props = new Func<int>(() => sQMMBrokFProps), RequiresAPR = true},
                        new { QMFee = new Func<decimal>(() => sQMInspectF), QMAmount = new Func<decimal>(() => sQMInspectF_QMAmount), Props = new Func<int>(() => sQMInspectFProps), RequiresAPR = true},
                        new { QMFee = new Func<decimal>(() => sQMProcF), QMAmount = new Func<decimal>(() => sQMProcF_QMAmount), Props = new Func<int>(() => sQMProcFProps), RequiresAPR = true},
                        new { QMFee = new Func<decimal>(() => sQMUwF), QMAmount = new Func<decimal>(() => sQMUwF_QMAmount), Props = new Func<int>(() => sQMUwFProps), RequiresAPR = true},
                        new { QMFee = new Func<decimal>(() => sQMWireF), QMAmount = new Func<decimal>(() => sQMWireF_QMAmount), Props = new Func<int>(() => sQMWireFProps), RequiresAPR = true},
                        new { QMFee = new Func<decimal>(() => sQM800U1F), QMAmount = new Func<decimal>(() => sQM800U1F_QMAmount), Props = new Func<int>(() => sQM800U1FProps), RequiresAPR = true},
                        new { QMFee = new Func<decimal>(() => sQM800U2F), QMAmount = new Func<decimal>(() => sQM800U2F_QMAmount), Props = new Func<int>(() => sQM800U2FProps), RequiresAPR = true},
                        new { QMFee = new Func<decimal>(() => sQM800U3F), QMAmount = new Func<decimal>(() => sQM800U3F_QMAmount), Props = new Func<int>(() => sQM800U3FProps), RequiresAPR = true},
                        new { QMFee = new Func<decimal>(() => sQM800U4F), QMAmount = new Func<decimal>(() => sQM800U4F_QMAmount), Props = new Func<int>(() => sQM800U4FProps), RequiresAPR = true},
                        new { QMFee = new Func<decimal>(() => sQM800U5F), QMAmount = new Func<decimal>(() => sQM800U5F_QMAmount), Props = new Func<int>(() => sQM800U5FProps), RequiresAPR = true},

                        //900
                        //new { QMFee = new Func<decimal>(() => sQMIPia), QMAmount = new Func<decimal>(() => sQMIPia_QMAmount), Props = new Func<int>(() => sQMIPiaProps), RequiresAPR = true},
                        //new { QMFee = new Func<decimal>(() => sQMMipPia), QMAmount = new Func<decimal>(() => sQMMipPia_QMAmount), Props = new Func<int>(() => sQMMipPiaProps), RequiresAPR = true},
                        new { QMFee = new Func<decimal>(() => sQMHazInsPia), QMAmount = new Func<decimal>(() => sQMHazInsPia_QMAmount), Props = new Func<int>(() => sQMHazInsPiaProps), RequiresAPR = true},
                        new { QMFee = new Func<decimal>(() => sQM904Pia), QMAmount = new Func<decimal>(() => sQM904Pia_QMAmount), Props = new Func<int>(() => sQM904PiaProps), RequiresAPR = true},
                        //new { QMFee = new Func<decimal>(() => sQMVaFf), QMAmount = new Func<decimal>(() => sQMVaFf_QMAmount), Props = new Func<int>(() => sQMVaFfProps), RequiresAPR = true},
                        new { QMFee = new Func<decimal>(() => sQM900U1Pia), QMAmount = new Func<decimal>(() => sQM900U1Pia_QMAmount), Props = new Func<int>(() => sQM900U1PiaProps), RequiresAPR = true},

                        //1000 
                        new { QMFee = new Func<decimal>(() => sQMHazInsRsrv), QMAmount = new Func<decimal>(() => sQMHazInsRsrv_QMAmount), Props = new Func<int>(() => sQMHazInsRsrvProps), RequiresAPR = false},
                        new { QMFee = new Func<decimal>(() => sQMMInsRsrv), QMAmount = new Func<decimal>(() => sQMMInsRsrv_QMAmount), Props = new Func<int>(() => sQMMInsRsrvProps), RequiresAPR = false},
                        new { QMFee = new Func<decimal>(() => sQMRealETxRsrv), QMAmount = new Func<decimal>(() => sQMRealETxRsrv_QMAmount), Props = new Func<int>(() => sQMRealETxRsrvProps), RequiresAPR = true},
                        new { QMFee = new Func<decimal>(() => sQMSchoolTxRsrv), QMAmount = new Func<decimal>(() => sQMSchoolTxRsrv_QMAmount), Props = new Func<int>(() => sQMSchoolTxRsrvProps), RequiresAPR = true},
                        new { QMFee = new Func<decimal>(() => sQMFloodInsRsrv), QMAmount = new Func<decimal>(() => sQMFloodInsRsrv_QMAmount), Props = new Func<int>(() => sQMFloodInsRsrvProps), RequiresAPR = false},
                        new { QMFee = new Func<decimal>(() => sQMAggregateAdjRsrv), QMAmount = new Func<decimal>(() => sQMAggregateAdjRsrv_QMAmount), Props = new Func<int>(() => sQMAggregateAdjRsrvProps), RequiresAPR = true},
                        new { QMFee = new Func<decimal>(() => sQM1006Rsrv), QMAmount = new Func<decimal>(() => sQM1006Rsrv_QMAmount), Props = new Func<int>(() => sQM1006RsrvProps), RequiresAPR = false},
                        new { QMFee = new Func<decimal>(() => sQM1007Rsrv), QMAmount = new Func<decimal>(() => sQM1007Rsrv_QMAmount), Props = new Func<int>(() => sQM1007RsrvProps), RequiresAPR = false},
                        new { QMFee = new Func<decimal>(() => sQMU3Rsrv), QMAmount = new Func<decimal>(() => sQMU3Rsrv_QMAmount), Props = new Func<int>(() => sQMU3RsrvProps), RequiresAPR = false},
                        new { QMFee = new Func<decimal>(() => sQMU4Rsrv), QMAmount = new Func<decimal>(() => sQMU4Rsrv_QMAmount), Props = new Func<int>(() => sQMU4RsrvProps), RequiresAPR = false},

                        //1100 title charges
                        new { QMFee = new Func<decimal>(() => sQMEscrowF), QMAmount = new Func<decimal>(() => sQMEscrowF_QMAmount), Props = new Func<int>(() => sQMEscrowFProps), RequiresAPR = false},
                        new { QMFee = new Func<decimal>(() => sQMOwnerTitleInsF), QMAmount = new Func<decimal>(() => sQMOwnerTitleInsF_QMAmount), Props = new Func<int>(() => sQMOwnerTitleInsFProps), RequiresAPR = false},
                        new { QMFee = new Func<decimal>(() => sQMTitleInsF), QMAmount = new Func<decimal>(() => sQMTitleInsF_QMAmount), Props = new Func<int>(() => sQMTitleInsFProps), RequiresAPR = false},
                        new { QMFee = new Func<decimal>(() => sQMDocPrepF), QMAmount = new Func<decimal>(() => sQMDocPrepF_QMAmount), Props = new Func<int>(() => sQMDocPrepFProps), RequiresAPR = false},
                        new { QMFee = new Func<decimal>(() => sQMNotaryF), QMAmount = new Func<decimal>(() => sQMNotaryF_QMAmount), Props = new Func<int>(() => sQMNotaryFProps), RequiresAPR = false},
                        new { QMFee = new Func<decimal>(() => sQMAttorneyF), QMAmount = new Func<decimal>(() => sQMAttorneyF_QMAmount), Props = new Func<int>(() => sQMAttorneyFProps), RequiresAPR = false},
                        new { QMFee = new Func<decimal>(() => sQMU1Tc), QMAmount = new Func<decimal>(() => sQMU1Tc_QMAmount), Props = new Func<int>(() => sQMU1TcProps), RequiresAPR = false},
                        new { QMFee = new Func<decimal>(() => sQMU2Tc), QMAmount = new Func<decimal>(() => sQMU2Tc_QMAmount), Props = new Func<int>(() => sQMU2TcProps), RequiresAPR = false},
                        new { QMFee = new Func<decimal>(() => sQMU3Tc), QMAmount = new Func<decimal>(() => sQMU3Tc_QMAmount), Props = new Func<int>(() => sQMU3TcProps), RequiresAPR = false},
                        new { QMFee = new Func<decimal>(() => sQMU4Tc), QMAmount = new Func<decimal>(() => sQMU4Tc_QMAmount), Props = new Func<int>(() => sQMU4TcProps), RequiresAPR = false},

                        //1200
                        new { QMFee = new Func<decimal>(() => sQMRecF), QMAmount = new Func<decimal>(() => sQMRecF_QMAmount), Props = new Func<int>(() => sQMRecFProps), RequiresAPR = true},
                        new { QMFee = new Func<decimal>(() => sQMCountyRtc), QMAmount = new Func<decimal>(() => sQMCountyRtc_QMAmount), Props = new Func<int>(() => sQMCountyRtcProps), RequiresAPR = true},
                        new { QMFee = new Func<decimal>(() => sQMStateRtc), QMAmount = new Func<decimal>(() => sQMStateRtc_QMAmount), Props = new Func<int>(() => sQMStateRtcProps), RequiresAPR = true},
                        new { QMFee = new Func<decimal>(() => sQMU1GovRtc), QMAmount = new Func<decimal>(() => sQMU1GovRtc_QMAmount), Props = new Func<int>(() => sQMU1GovRtcProps), RequiresAPR = true},
                        new { QMFee = new Func<decimal>(() => sQMU2GovRtc), QMAmount = new Func<decimal>(() => sQMU2GovRtc_QMAmount), Props = new Func<int>(() => sQMU2GovRtcProps), RequiresAPR = true},
                        new { QMFee = new Func<decimal>(() => sQMU3GovRtc), QMAmount = new Func<decimal>(() => sQMU3GovRtc_QMAmount), Props = new Func<int>(() => sQMU3GovRtcProps), RequiresAPR = true},

                        //1300 
                        new { QMFee = new Func<decimal>(() => sQMPestInspectF), QMAmount = new Func<decimal>(() => sQMPestInspectF_QMAmount), Props = new Func<int>(() => sQMPestInspectFProps), RequiresAPR = false},
                        new { QMFee = new Func<decimal>(() => sQMU1Sc), QMAmount = new Func<decimal>(() => sQMU1Sc_QMAmount), Props = new Func<int>(() => sQMU1ScProps), RequiresAPR = false},
                        new { QMFee = new Func<decimal>(() => sQMU2Sc), QMAmount = new Func<decimal>(() => sQMU2Sc_QMAmount), Props = new Func<int>(() => sQMU2ScProps), RequiresAPR = false},
                        new { QMFee = new Func<decimal>(() => sQMU3Sc), QMAmount = new Func<decimal>(() => sQMU3Sc_QMAmount), Props = new Func<int>(() => sQMU3ScProps), RequiresAPR = false},
                        new { QMFee = new Func<decimal>(() => sQMU4Sc), QMAmount = new Func<decimal>(() => sQMU4Sc_QMAmount), Props = new Func<int>(() => sQMU4ScProps), RequiresAPR = false},
                        new { QMFee = new Func<decimal>(() => sQMU5Sc), QMAmount = new Func<decimal>(() => sQMU5Sc_QMAmount), Props = new Func<int>(() => sQMU5ScProps), RequiresAPR = false},

                    #endregion
                };

            decimal totalFeeAmount = 0;

            foreach (var lineItem in gfeLineItems)
            {
                int props = lineItem.Props();
                int payer = LosConvert.GfeItemProps_Payer(props);

                // logic updated per 203073.
                if (false == financed)
                {
                    totalFeeAmount += lineItem.QMAmount();
                }
                else // if financed == true.
                {
                    if(    payer == 1 // borr_fin
                        && false == LosConvert.GfeItemProps_Apr(props) 
                        && false == lineItem.RequiresAPR 
                        && LosConvert.GfeItemProps_PaidToThirdParty(props) && LosConvert.GfeItemProps_ThisPartyIsAffiliate(props))
                    {
                        totalFeeAmount += lineItem.QMFee();
                    }
                }
            }
            return totalFeeAmount;
        }

        /// <summary>
        /// True if the system was unable to automatically determine third-party
        /// or affiliate status for one or more fee recipients.
        /// </summary>
        [DependsOn(nameof(sQMLOrigFProps_ShowQmWarning), nameof(sQMGfeOriginatorCompFProps_ShowQmWarning), nameof(sQMApprFProps_ShowQmWarning),  
            nameof(sQMCrFProps_ShowQmWarning), nameof(sQMTxServFProps_ShowQmWarning), nameof(sQMFloodCertificationFProps_ShowQmWarning),  
            nameof(sQMMBrokFProps_ShowQmWarning), nameof(sQMInspectFProps_ShowQmWarning), nameof(sQMProcFProps_ShowQmWarning),  
            nameof(sQMUwFProps_ShowQmWarning), nameof(sQMWireFProps_ShowQmWarning), nameof(sQM800U1FProps_ShowQmWarning),  
            nameof(sQM800U2FProps_ShowQmWarning), nameof(sQM800U3FProps_ShowQmWarning), nameof(sQM800U4FProps_ShowQmWarning),  
            nameof(sQM800U5FProps_ShowQmWarning), nameof(sQMIPiaProps_ShowQmWarning), nameof(sQMHazInsPiaProps_ShowQmWarning),  
            nameof(sQM904PiaProps_ShowQmWarning), nameof(sQMVaFfProps_ShowQmWarning), nameof(sQM900U1PiaProps_ShowQmWarning),  
            nameof(sQMHazInsRsrvProps_ShowQmWarning), nameof(sQMMInsRsrvProps_ShowQmWarning), nameof(sQMRealETxRsrvProps_ShowQmWarning),  
            nameof(sQMSchoolTxRsrvProps_ShowQmWarning), nameof(sQMFloodInsRsrvProps_ShowQmWarning),  
            nameof(sQMAggregateAdjRsrvProps_ShowQmWarning), nameof(sQM1006RsrvProps_ShowQmWarning), nameof(sQM1007RsrvProps_ShowQmWarning),  
            nameof(sQMU3RsrvProps_ShowQmWarning), nameof(sQMU4RsrvProps_ShowQmWarning), nameof(sQMEscrowFProps_ShowQmWarning),  
            nameof(sQMOwnerTitleInsFProps_ShowQmWarning), nameof(sQMTitleInsFProps_ShowQmWarning), nameof(sQMDocPrepFProps_ShowQmWarning),  
            nameof(sQMNotaryFProps_ShowQmWarning), nameof(sQMAttorneyFProps_ShowQmWarning), nameof(sQMU1TcProps_ShowQmWarning),  
            nameof(sQMU2TcProps_ShowQmWarning), nameof(sQMU3TcProps_ShowQmWarning), nameof(sQMU4TcProps_ShowQmWarning),  
            nameof(sQMRecFProps_ShowQmWarning), nameof(sQMCountyRtcProps_ShowQmWarning), nameof(sQMStateRtcProps_ShowQmWarning),  
            nameof(sQMU1GovRtcProps_ShowQmWarning), nameof(sQMU2GovRtcProps_ShowQmWarning), nameof(sQMU3GovRtcProps_ShowQmWarning),  
            nameof(sQMPestInspectFProps_ShowQmWarning), nameof(sQMU1ScProps_ShowQmWarning), nameof(sQMU2ScProps_ShowQmWarning),  
            nameof(sQMU3ScProps_ShowQmWarning), nameof(sQMU4ScProps_ShowQmWarning), nameof(sQMU5ScProps_ShowQmWarning))]
        [DevNote("Show QM Warning on QM Calculations and Point and Fees page.")]
        private bool sQMShowWarning_Old
        {
            get
            {
                return //sQMLOrigFProps_ShowQmWarning                 // No PaidTo
                    //|| sQMGfeOriginatorCompFProps_ShowQmWarning     // No PaidTo
                    //|| sQMGfeDiscountPointFProps_ShowQmWarning      // No TP & AFF
                    /*||*/ sQMApprFProps_ShowQmWarning
                    || sQMCrFProps_ShowQmWarning
                    || sQMTxServFProps_ShowQmWarning
                    || sQMFloodCertificationFProps_ShowQmWarning
                    //|| sQMMBrokFProps_ShowQmWarning                 // No PaidTo
                    || sQMInspectFProps_ShowQmWarning
                    || sQMProcFProps_ShowQmWarning
                    || sQMUwFProps_ShowQmWarning
                    || sQMWireFProps_ShowQmWarning
                    || sQM800U1FProps_ShowQmWarning
                    || sQM800U2FProps_ShowQmWarning
                    || sQM800U3FProps_ShowQmWarning
                    || sQM800U4FProps_ShowQmWarning
                    || sQM800U5FProps_ShowQmWarning
                    //|| sQMIPiaProps_ShowQmWarning                   // No PaidTo
                    //|| sQMMipPiaProps_ShowQmWarning                 // No TP & AFF (BUT HAS PAID TO)
                    || sQMHazInsPiaProps_ShowQmWarning
                    //|| sQM904PiaProps_ShowQmWarning                 // No PaidTo
                    || sQMVaFfProps_ShowQmWarning
                    //|| sQM900U1PiaProps_ShowQmWarning               // No PaidTo
                    //|| sQMHazInsRsrvProps_ShowQmWarning             // No PaidTo
                    //|| sQMMInsRsrvProps_ShowQmWarning               // No PaidTo
                    //|| sQMRealETxRsrvProps_ShowQmWarning            // No PaidTo
                    //|| sQMSchoolTxRsrvProps_ShowQmWarning           // No PaidTo
                    //|| sQMFloodInsRsrvProps_ShowQmWarning           // No PaidTo
                    //|| sQMAggregateAdjRsrvProps_ShowQmWarning       // No PaidTo
                    //|| sQM1006RsrvProps_ShowQmWarning               // No PaidTo
                    //|| sQM1007RsrvProps_ShowQmWarning               // No PaidTo
                    //|| sQMU3RsrvProps_ShowQmWarning                 // No PaidTo
                    //|| sQMU4RsrvProps_ShowQmWarning                 // No PaidTo
                    || sQMEscrowFProps_ShowQmWarning
                    || sQMOwnerTitleInsFProps_ShowQmWarning
                    || sQMTitleInsFProps_ShowQmWarning
                    || sQMDocPrepFProps_ShowQmWarning
                    || sQMNotaryFProps_ShowQmWarning
                    || sQMAttorneyFProps_ShowQmWarning
                    || sQMU1TcProps_ShowQmWarning
                    || sQMU2TcProps_ShowQmWarning
                    || sQMU3TcProps_ShowQmWarning
                    || sQMU4TcProps_ShowQmWarning
                    || sQMRecFProps_ShowQmWarning
                    || sQMCountyRtcProps_ShowQmWarning
                    || sQMStateRtcProps_ShowQmWarning
                    || sQMU1GovRtcProps_ShowQmWarning
                    || sQMU2GovRtcProps_ShowQmWarning
                    || sQMU3GovRtcProps_ShowQmWarning
                    || sQMPestInspectFProps_ShowQmWarning
                    || sQMU1ScProps_ShowQmWarning
                    || sQMU2ScProps_ShowQmWarning
                    || sQMU3ScProps_ShowQmWarning
                    || sQMU4ScProps_ShowQmWarning
                    || sQMU5ScProps_ShowQmWarning;
            }
        }
        #endregion

        #region sQMLOrigF
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sLOrigF), nameof(sSettlementLOrigF))]
        public decimal sQMLOrigF
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sLOrigF, sSettlementLOrigF); }
        }

        public string sQMLOrigF_rep
        {
            get { return ToMoneyString(() => sQMLOrigF); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sLOrigFProps), nameof(sSettlementLOrigFProps))]
        public int sQMLOrigFProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sLOrigFProps, sSettlementLOrigFProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sLOrigFProps = t, t => sSettlementLOrigFProps = t, value); }
        }

        [DependsOn(nameof(sQMLOrigFProps))]
        public int sQMLOrigFProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMLOrigFProps); }
            set { sQMLOrigFProps = LosConvert.GfeItemProps_UpdatePayer(sQMLOrigFProps, value); }
        }

        [DependsOn(nameof(sQMLOrigFProps))]
        public bool sQMLOrigFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMLOrigFProps); }
            set { sQMLOrigFProps = LosConvert.GfeItemProps_UpdateApr(sQMLOrigFProps, value); }
        }

        [DependsOn(nameof(sQMLOrigFProps))]
        public bool sQMLOrigFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMLOrigFProps); }
            set { sQMLOrigFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMLOrigFProps, value); }
        }

        [DependsOn(nameof(sQMLOrigFProps))]
        public bool sQMLOrigFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMLOrigFProps); }
            set { sQMLOrigFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMLOrigFProps, value); }
        }

        [DependsOn(nameof(sQMLOrigFProps))]
        [DevNote("sQMLOrigFProps Show QM Warning bit.")]
        public bool sQMLOrigFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMLOrigFProps); }
            set { sQMLOrigFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMLOrigFProps, value); }
        }

        [DependsOn(nameof(sQMLOrigF), nameof(sQMLOrigFProps))]
        public decimal sQMLOrigF_QMAmount
        {
            get { return GetQMField_QMAmount(sQMLOrigF, sQMLOrigFProps, true); }
        }

        public string sQMLOrigF_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMLOrigF_QMAmount); }
        }
        #endregion
        #region sQMGfeOriginatorCompF
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sGfeOriginatorCompF))]
        public decimal sQMGfeOriginatorCompF
        {
            get { return sGfeOriginatorCompF; }
        }

        public string sQMGfeOriginatorCompF_rep
        {
            get { return ToMoneyString(() => sQMGfeOriginatorCompF); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sGfeOriginatorCompFProps))]
        public int sQMGfeOriginatorCompFProps
        {
            get { return sGfeOriginatorCompFProps; }
            set { sGfeOriginatorCompFProps = value; }
        }

        [DependsOn(nameof(sQMGfeOriginatorCompFProps))]
        public int sQMGfeOriginatorCompFProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMGfeOriginatorCompFProps); }
            set { sQMGfeOriginatorCompFProps = LosConvert.GfeItemProps_UpdatePayer(sQMGfeOriginatorCompFProps, value); }
        }

        [DependsOn(nameof(sQMGfeOriginatorCompFProps))]
        public bool sQMGfeOriginatorCompFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMGfeOriginatorCompFProps); }
            set { sQMGfeOriginatorCompFProps = LosConvert.GfeItemProps_UpdateApr(sQMGfeOriginatorCompFProps, value); }
        }

        [DependsOn(nameof(sQMGfeOriginatorCompFProps))]
        public bool sQMGfeOriginatorCompFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMGfeOriginatorCompFProps); }
            set { sQMGfeOriginatorCompFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMGfeOriginatorCompFProps, value); }
        }

        [DependsOn(nameof(sQMGfeOriginatorCompFProps))]
        public bool sQMGfeOriginatorCompFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMGfeOriginatorCompFProps); }
            set { sQMGfeOriginatorCompFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMGfeOriginatorCompFProps, value); }
        }

        [DependsOn(nameof(sQMGfeOriginatorCompFProps))]
        [DevNote("sQMGfeOriginatorCompFProps Show QM Warning bit.")]
        public bool sQMGfeOriginatorCompFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMGfeOriginatorCompFProps); }
            set { sQMGfeOriginatorCompFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMGfeOriginatorCompFProps, value); }
        }

        [DependsOn(nameof(sQMGfeOriginatorCompF), nameof(sQMGfeOriginatorCompFProps))]
        public decimal sQMGfeOriginatorCompF_QMAmount
        {
            get { return 0; }   // Excluded value. QM Amount is always $0.00
        }

        public string sQMGfeOriginatorCompF_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMGfeOriginatorCompF_QMAmount); }
        }
        #endregion
        #region sQMGfeDiscountPointF
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sGfeDiscountPointF), nameof(sSettlementDiscountPointF))]
        public decimal sQMGfeDiscountPointF
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sGfeDiscountPointF, sSettlementDiscountPointF); }
        }

        public string sQMGfeDiscountPointF_rep
        {
            get { return ToMoneyString(() => sQMGfeDiscountPointF); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sGfeDiscountPointFProps), nameof(sSettlementDiscountPointFProps))]
        public int sQMGfeDiscountPointFProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sGfeDiscountPointFProps, sSettlementDiscountPointFProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sGfeDiscountPointFProps = t, t => sSettlementDiscountPointFProps = t, value); }
        }

        [DependsOn(nameof(sQMGfeDiscountPointFProps))]
        public int sQMGfeDiscountPointFProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMGfeDiscountPointFProps); }
            set { sQMGfeDiscountPointFProps = LosConvert.GfeItemProps_UpdatePayer(sQMGfeDiscountPointFProps, value); }
        }

        [DependsOn(nameof(sQMGfeDiscountPointFProps))]
        public bool sQMGfeDiscountPointFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMGfeDiscountPointFProps); }
            set { sQMGfeDiscountPointFProps = LosConvert.GfeItemProps_UpdateApr(sQMGfeDiscountPointFProps, value); }
        }

        //[DependsOn(nameof(sQMGfeDiscountPointFProps))]
        //public bool sQMGfeDiscountPointFProps_PaidToThirdParty
        //{
        //    get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMGfeDiscountPointFProps); }
        //    set { sQMGfeDiscountPointFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMGfeDiscountPointFProps, value); }
        //}

        //[DependsOn(nameof(sQMGfeDiscountPointFProps))]
        //public bool sQMGfeDiscountPointFProps_ThisPartyIsAffiliate
        //{
        //    get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMGfeDiscountPointFProps); }
        //    set { sQMGfeDiscountPointFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMGfeDiscountPointFProps, value); }
        //}

        //[DependsOn(nameof(sQMGfeDiscountPointFProps))]
        //[DevNote("sQMGfeDiscountPointFProps Show QM Warning bit.")]
        //public bool sQMGfeDiscountPointFProps_ShowQmWarning
        //{
        //    get { return LosConvert.GfeItemProps_ShowQmWarning(sQMGfeDiscountPointFProps); }
        //    set { sQMGfeDiscountPointFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMGfeDiscountPointFProps, value); }
        //}


        [DependsOn(nameof(sQMGfeDiscountPointF), nameof(sQMGfeDiscountPointFProps))]
        public decimal sQMGfeDiscountPointF_QMAmount
        {
            get { return 0; }   // Excluded value. QM Amount is always $0.00
        }

        public string sQMGfeDiscountPointF_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMGfeDiscountPointF_QMAmount); }
        }
        #endregion
        #region sQMApprF
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sApprF), nameof(sSettlementApprF))]
        public decimal sQMApprF
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sApprF, sSettlementApprF); }
        }

        public string sQMApprF_rep
        {
            get { return ToMoneyString(() => sQMApprF); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sApprFProps), nameof(sSettlementApprFProps))]
        public int sQMApprFProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sApprFProps, sSettlementApprFProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sApprFProps = t, t => sSettlementApprFProps = t, value); }
        }

        [DependsOn(nameof(sQMApprFProps))]
        public int sQMApprFProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMApprFProps); }
            set { sQMApprFProps = LosConvert.GfeItemProps_UpdatePayer(sQMApprFProps, value); }
        }

        [DependsOn(nameof(sQMApprFProps))]
        public bool sQMApprFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMApprFProps); }
            set { sQMApprFProps = LosConvert.GfeItemProps_UpdateApr(sQMApprFProps, value); }
        }

        [DependsOn(nameof(sQMApprFProps))]
        public bool sQMApprFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMApprFProps); }
            set { sQMApprFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMApprFProps, value); }
        }

        [DependsOn(nameof(sQMApprFProps))]
        public bool sQMApprFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMApprFProps); }
            set { sQMApprFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMApprFProps, value); }
        }

        [DependsOn(nameof(sQMApprFProps))]
        [DevNote("sQMApprFProps Show QM Warning bit.")]
        public bool sQMApprFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMApprFProps); }
            set { sQMApprFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMApprFProps, value); }
        }

        [DependsOn(nameof(sQMApprF), nameof(sQMApprFProps))]
        public decimal sQMApprF_QMAmount
        {
            get { return GetQMField_QMAmount(sQMApprF, sQMApprFProps, false); }
        }

        public string sQMApprF_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMApprF_QMAmount); }
        }
        #endregion
        #region sQMCrF
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sCrF), nameof(sSettlementCrF))]
        public decimal sQMCrF
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sCrF, sSettlementCrF); }
        }

        public string sQMCrF_rep
        {
            get { return ToMoneyString(() => sQMCrF); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sCrFProps), nameof(sSettlementCrFProps))]
        public int sQMCrFProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sCrFProps, sSettlementCrFProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sCrFProps = t, t => sSettlementCrFProps = t, value); }
        }

        [DependsOn(nameof(sQMCrFProps))]
        public int sQMCrFProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMCrFProps); }
            set { sQMCrFProps = LosConvert.GfeItemProps_UpdatePayer(sQMCrFProps, value); }
        }

        [DependsOn(nameof(sQMCrFProps))]
        public bool sQMCrFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMCrFProps); }
            set { sQMCrFProps = LosConvert.GfeItemProps_UpdateApr(sQMCrFProps, value); }
        }

        [DependsOn(nameof(sQMCrFProps))]
        public bool sQMCrFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMCrFProps); }
            set { sQMCrFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMCrFProps, value); }
        }

        [DependsOn(nameof(sQMCrFProps))]
        public bool sQMCrFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMCrFProps); }
            set { sQMCrFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMCrFProps, value); }
        }

        [DependsOn(nameof(sQMCrFProps))]
        [DevNote("sQMCrFProps Show QM Warning bit.")]
        public bool sQMCrFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMCrFProps); }
            set { sQMCrFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMCrFProps, value); }
        }

        [DependsOn(nameof(sQMCrF), nameof(sQMCrFProps))]
        public decimal sQMCrF_QMAmount
        {
            get { return GetQMField_QMAmount(sQMCrF, sQMCrFProps, false); }
        }

        public string sQMCrF_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMCrF_QMAmount); }
        }
        #endregion
        #region sQMTxServF
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sTxServF), nameof(sSettlementTxServF))]
        public decimal sQMTxServF
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sTxServF, sSettlementTxServF); }
        }

        public string sQMTxServF_rep
        {
            get { return ToMoneyString(() => sQMTxServF); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sTxServFProps), nameof(sSettlementTxServFProps))]
        public int sQMTxServFProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sTxServFProps, sSettlementTxServFProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sTxServFProps = t, t => sSettlementTxServFProps = t, value); }
        }

        [DependsOn(nameof(sQMTxServFProps))]
        public int sQMTxServFProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMTxServFProps); }
            set { sQMTxServFProps = LosConvert.GfeItemProps_UpdatePayer(sQMTxServFProps, value); }
        }

        [DependsOn(nameof(sQMTxServFProps))]
        public bool sQMTxServFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMTxServFProps); }
            set { sQMTxServFProps = LosConvert.GfeItemProps_UpdateApr(sQMTxServFProps, value); }
        }

        [DependsOn(nameof(sQMTxServFProps))]
        public bool sQMTxServFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMTxServFProps); }
            set { sQMTxServFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMTxServFProps, value); }
        }

        [DependsOn(nameof(sQMTxServFProps))]
        public bool sQMTxServFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMTxServFProps); }
            set { sQMTxServFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMTxServFProps, value); }
        }

        [DependsOn(nameof(sQMTxServFProps))]
        [DevNote("sQMTxServFProps Show QM Warning bit.")]
        public bool sQMTxServFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMTxServFProps); }
            set { sQMTxServFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMTxServFProps, value); }
        }

        [DependsOn(nameof(sQMTxServF), nameof(sQMTxServFProps))]
        public decimal sQMTxServF_QMAmount
        {
            get { return GetQMField_QMAmount(sQMTxServF, sQMTxServFProps, true); }
        }

        public string sQMTxServF_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMTxServF_QMAmount); }
        }
        #endregion
        #region sQMFloodCertificationF
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sFloodCertificationF), nameof(sSettlementFloodCertificationF))]
        public decimal sQMFloodCertificationF
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sFloodCertificationF, sSettlementFloodCertificationF); }
        }

        public string sQMFloodCertificationF_rep
        {
            get { return ToMoneyString(() => sQMFloodCertificationF); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sFloodCertificationFProps), nameof(sSettlementFloodCertificationFProps))]
        public int sQMFloodCertificationFProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sFloodCertificationFProps, sSettlementFloodCertificationFProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sFloodCertificationFProps = t, t => sSettlementFloodCertificationFProps = t, value); }
        }

        [DependsOn(nameof(sQMFloodCertificationFProps))]
        public int sQMFloodCertificationFProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMFloodCertificationFProps); }
            set { sQMFloodCertificationFProps = LosConvert.GfeItemProps_UpdatePayer(sQMFloodCertificationFProps, value); }
        }

        [DependsOn(nameof(sQMFloodCertificationFProps))]
        public bool sQMFloodCertificationFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMFloodCertificationFProps); }
            set { sQMFloodCertificationFProps = LosConvert.GfeItemProps_UpdateApr(sQMFloodCertificationFProps, value); }
        }

        [DependsOn(nameof(sQMFloodCertificationFProps))]
        public bool sQMFloodCertificationFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMFloodCertificationFProps); }
            set { sQMFloodCertificationFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMFloodCertificationFProps, value); }
        }

        [DependsOn(nameof(sQMFloodCertificationFProps))]
        public bool sQMFloodCertificationFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMFloodCertificationFProps); }
            set { sQMFloodCertificationFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMFloodCertificationFProps, value); }
        }

        [DependsOn(nameof(sQMFloodCertificationFProps))]
        [DevNote("sQMFloodCertificationFProps Show QM Warning bit.")]
        public bool sQMFloodCertificationFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMFloodCertificationFProps); }
            set { sQMFloodCertificationFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMFloodCertificationFProps, value); }
        }

        [DependsOn(nameof(sQMFloodCertificationF), nameof(sQMFloodCertificationFProps))]
        public decimal sQMFloodCertificationF_QMAmount
        {
            get { return GetQMField_QMAmount(sQMFloodCertificationF, sQMFloodCertificationFProps, false); }
        }

        public string sQMFloodCertificationF_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMFloodCertificationF_QMAmount); }
        }
        #endregion
        #region sQMMBrokF
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sMBrokF), nameof(sSettlementMBrokF))]
        public decimal sQMMBrokF
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sMBrokF, sSettlementMBrokF); }
        }

        public string sQMMBrokF_rep
        {
            get { return ToMoneyString(() => sQMMBrokF); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sMBrokFProps), nameof(sSettlementMBrokFProps))]
        public int sQMMBrokFProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sMBrokFProps, sSettlementMBrokFProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sMBrokFProps = t, t => sSettlementMBrokFProps = t, value); }
        }

        [DependsOn(nameof(sQMMBrokFProps))]
        public int sQMMBrokFProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMMBrokFProps); }
            set { sQMMBrokFProps = LosConvert.GfeItemProps_UpdatePayer(sQMMBrokFProps, value); }
        }

        [DependsOn(nameof(sQMMBrokFProps))]
        public bool sQMMBrokFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMMBrokFProps); }
            set { sQMMBrokFProps = LosConvert.GfeItemProps_UpdateApr(sQMMBrokFProps, value); }
        }

        [DependsOn(nameof(sQMMBrokFProps))]
        public bool sQMMBrokFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMMBrokFProps); }
            set { sQMMBrokFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMMBrokFProps, value); }
        }

        [DependsOn(nameof(sQMMBrokFProps))]
        public bool sQMMBrokFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMMBrokFProps); }
            set { sQMMBrokFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMMBrokFProps, value); }
        }

        [DependsOn(nameof(sQMMBrokFProps))]
        [DevNote("sQMMBrokFProps Show QM Warning bit.")]
        public bool sQMMBrokFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMMBrokFProps); }
            set { sQMMBrokFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMMBrokFProps, value); }
        }

        [DependsOn(nameof(sQMMBrokF), nameof(sQMMBrokFProps))]
        public decimal sQMMBrokF_QMAmount
        {
            get { return GetQMField_QMAmount(sQMMBrokF, sQMMBrokFProps, true); }
        }

        public string sQMMBrokF_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMMBrokF_QMAmount); }
        }
        #endregion
        #region sQMInspectF
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sInspectF), nameof(sSettlementInspectF))]
        public decimal sQMInspectF
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sInspectF, sSettlementInspectF); }
        }

        public string sQMInspectF_rep
        {
            get { return ToMoneyString(() => sQMInspectF); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sInspectFProps), nameof(sSettlementInspectFProps))]
        public int sQMInspectFProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sInspectFProps, sSettlementInspectFProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sInspectFProps = t, t => sSettlementInspectFProps = t, value); }
        }

        [DependsOn(nameof(sQMInspectFProps))]
        public int sQMInspectFProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMInspectFProps); }
            set { sQMInspectFProps = LosConvert.GfeItemProps_UpdatePayer(sQMInspectFProps, value); }
        }

        [DependsOn(nameof(sQMInspectFProps))]
        public bool sQMInspectFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMInspectFProps); }
            set { sQMInspectFProps = LosConvert.GfeItemProps_UpdateApr(sQMInspectFProps, value); }
        }

        [DependsOn(nameof(sQMInspectFProps))]
        public bool sQMInspectFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMInspectFProps); }
            set { sQMInspectFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMInspectFProps, value); }
        }

        [DependsOn(nameof(sQMInspectFProps))]
        public bool sQMInspectFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMInspectFProps); }
            set { sQMInspectFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMInspectFProps, value); }
        }

        [DependsOn(nameof(sQMInspectFProps))]
        [DevNote("sQMInspectFProps Show QM Warning bit.")]
        public bool sQMInspectFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMInspectFProps); }
            set { sQMInspectFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMInspectFProps, value); }
        }

        [DependsOn(nameof(sQMInspectF), nameof(sQMInspectFProps))]
        public decimal sQMInspectF_QMAmount
        {
            get { return GetQMField_QMAmount(sQMInspectF, sQMInspectFProps, true); }
        }

        public string sQMInspectF_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMInspectF_QMAmount); }
        }
        #endregion
        #region sQMProcF
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sProcF), nameof(sSettlementProcF))]
        public decimal sQMProcF
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sProcF, sSettlementProcF); }
        }

        public string sQMProcF_rep
        {
            get { return ToMoneyString(() => sQMProcF); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sProcFProps), nameof(sSettlementProcFProps))]
        public int sQMProcFProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sProcFProps, sSettlementProcFProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sProcFProps = t, t => sSettlementProcFProps = t, value); }
        }

        [DependsOn(nameof(sQMProcFProps))]
        public int sQMProcFProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMProcFProps); }
            set { sQMProcFProps = LosConvert.GfeItemProps_UpdatePayer(sQMProcFProps, value); }
        }

        [DependsOn(nameof(sQMProcFProps))]
        public bool sQMProcFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMProcFProps); }
            set { sQMProcFProps = LosConvert.GfeItemProps_UpdateApr(sQMProcFProps, value); }
        }

        [DependsOn(nameof(sQMProcFProps))]
        public bool sQMProcFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMProcFProps); }
            set { sQMProcFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMProcFProps, value); }
        }

        [DependsOn(nameof(sQMProcFProps))]
        public bool sQMProcFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMProcFProps); }
            set { sQMProcFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMProcFProps, value); }
        }

        [DependsOn(nameof(sQMProcFProps))]
        [DevNote("sQMProcFProps Show QM Warning bit.")]
        public bool sQMProcFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMProcFProps); }
            set { sQMProcFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMProcFProps, value); }
        }

        [DependsOn(nameof(sQMProcF), nameof(sQMProcFProps))]
        public decimal sQMProcF_QMAmount
        {
            get { return GetQMField_QMAmount(sQMProcF, sQMProcFProps, true); }
        }

        public string sQMProcF_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMProcF_QMAmount); }
        }
        #endregion
        #region sQMUwF
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sUwF), nameof(sSettlementUwF))]
        public decimal sQMUwF
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sUwF, sSettlementUwF); }
        }

        public string sQMUwF_rep
        {
            get { return ToMoneyString(() => sQMUwF); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sUwFProps), nameof(sSettlementUwFProps))]
        public int sQMUwFProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sUwFProps, sSettlementUwFProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sUwFProps = t, t => sSettlementUwFProps = t, value); }
        }

        [DependsOn(nameof(sQMUwFProps))]
        public int sQMUwFProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMUwFProps); }
            set { sQMUwFProps = LosConvert.GfeItemProps_UpdatePayer(sQMUwFProps, value); }
        }

        [DependsOn(nameof(sQMUwFProps))]
        public bool sQMUwFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMUwFProps); }
            set { sQMUwFProps = LosConvert.GfeItemProps_UpdateApr(sQMUwFProps, value); }
        }

        [DependsOn(nameof(sQMUwFProps))]
        public bool sQMUwFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMUwFProps); }
            set { sQMUwFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMUwFProps, value); }
        }

        [DependsOn(nameof(sQMUwFProps))]
        public bool sQMUwFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMUwFProps); }
            set { sQMUwFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMUwFProps, value); }
        }

        [DependsOn(nameof(sQMUwFProps))]
        [DevNote("sQMUwFProps Show QM Warning bit.")]
        public bool sQMUwFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMUwFProps); }
            set { sQMUwFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMUwFProps, value); }
        }

        [DependsOn(nameof(sQMUwF), nameof(sQMUwFProps))]
        public decimal sQMUwF_QMAmount
        {
            get { return GetQMField_QMAmount(sQMUwF, sQMUwFProps, true); }
        }

        public string sQMUwF_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMUwF_QMAmount); }
        }
        #endregion
        #region sQMWireF
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sWireF), nameof(sSettlementWireF))]
        public decimal sQMWireF
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sWireF, sSettlementWireF); }
        }

        public string sQMWireF_rep
        {
            get { return ToMoneyString(() => sQMWireF); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sWireFProps), nameof(sSettlementWireFProps))]
        public int sQMWireFProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sWireFProps, sSettlementWireFProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sWireFProps = t, t => sSettlementWireFProps = t, value); }
        }

        [DependsOn(nameof(sQMWireFProps))]
        public int sQMWireFProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMWireFProps); }
            set { sQMWireFProps = LosConvert.GfeItemProps_UpdatePayer(sQMWireFProps, value); }
        }

        [DependsOn(nameof(sQMWireFProps))]
        public bool sQMWireFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMWireFProps); }
            set { sQMWireFProps = LosConvert.GfeItemProps_UpdateApr(sQMWireFProps, value); }
        }

        [DependsOn(nameof(sQMWireFProps))]
        public bool sQMWireFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMWireFProps); }
            set { sQMWireFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMWireFProps, value); }
        }

        [DependsOn(nameof(sQMWireFProps))]
        public bool sQMWireFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMWireFProps); }
            set { sQMWireFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMWireFProps, value); }
        }

        [DependsOn(nameof(sQMWireFProps))]
        [DevNote("sQMWireFProps Show QM Warning bit.")]
        public bool sQMWireFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMWireFProps); }
            set { sQMWireFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMWireFProps, value); }
        }

        [DependsOn(nameof(sQMWireF), nameof(sQMWireFProps))]
        public decimal sQMWireF_QMAmount
        {
            get { return GetQMField_QMAmount(sQMWireF, sQMWireFProps, true); }
        }

        public string sQMWireF_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMWireF_QMAmount); }
        }
        #endregion
        #region sQM800U1F
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(s800U1FDesc), nameof(sSettlement800U1FDesc))]
        public string sQM800U1FDesc
        {
            get { return GetQMField_Desc(sQmSettlementChargesExportSource, s800U1FDesc, sSettlement800U1FDesc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(s800U1F), nameof(sSettlement800U1F))]
        public decimal sQM800U1F
        {
            get { return GetQMField(sQmSettlementChargesExportSource, s800U1F, sSettlement800U1F); }
        }

        public string sQM800U1F_rep
        {
            get { return ToMoneyString(() => sQM800U1F); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(s800U1FProps), nameof(sSettlement800U1FProps))]
        public int sQM800U1FProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, s800U1FProps, sSettlement800U1FProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => s800U1FProps = t, t => sSettlement800U1FProps = t, value); }
        }

        [DependsOn(nameof(sQM800U1FProps))]
        public int sQM800U1FProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQM800U1FProps); }
            set { sQM800U1FProps = LosConvert.GfeItemProps_UpdatePayer(sQM800U1FProps, value); }
        }

        [DependsOn(nameof(sQM800U1FProps))]
        public bool sQM800U1FProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQM800U1FProps); }
            set { sQM800U1FProps = LosConvert.GfeItemProps_UpdateApr(sQM800U1FProps, value); }
        }

        [DependsOn(nameof(sQM800U1FProps))]
        public bool sQM800U1FProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQM800U1FProps); }
            set { sQM800U1FProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQM800U1FProps, value); }
        }

        [DependsOn(nameof(sQM800U1FProps))]
        public bool sQM800U1FProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQM800U1FProps); }
            set { sQM800U1FProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQM800U1FProps, value); }
        }

        [DependsOn(nameof(sQM800U1FProps))]
        [DevNote("sQM800U1FProps Show QM Warning bit.")]
        public bool sQM800U1FProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQM800U1FProps); }
            set { sQM800U1FProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQM800U1FProps, value); }
        }

        [DependsOn(nameof(sQM800U1F), nameof(sQM800U1FProps))]
        public decimal sQM800U1F_QMAmount
        {
            get { return GetQMField_QMAmount(sQM800U1F, sQM800U1FProps, true); }
        }

        public string sQM800U1F_QMAmount_rep
        {
            get { return ToMoneyString(() => sQM800U1F_QMAmount); }
        }
        #endregion
        #region sQM800U2F
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(s800U2FDesc), nameof(sSettlement800U2FDesc))]
        public string sQM800U2FDesc
        {
            get { return GetQMField_Desc(sQmSettlementChargesExportSource, s800U2FDesc, sSettlement800U2FDesc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(s800U2F), nameof(sSettlement800U2F))]
        public decimal sQM800U2F
        {
            get { return GetQMField(sQmSettlementChargesExportSource, s800U2F, sSettlement800U2F); }
        }

        public string sQM800U2F_rep
        {
            get { return ToMoneyString(() => sQM800U2F); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(s800U2FProps), nameof(sSettlement800U2FProps))]
        public int sQM800U2FProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, s800U2FProps, sSettlement800U2FProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => s800U2FProps = t, t => sSettlement800U2FProps = t, value); }
        }

        [DependsOn(nameof(sQM800U2FProps))]
        public int sQM800U2FProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQM800U2FProps); }
            set { sQM800U2FProps = LosConvert.GfeItemProps_UpdatePayer(sQM800U2FProps, value); }
        }

        [DependsOn(nameof(sQM800U2FProps))]
        public bool sQM800U2FProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQM800U2FProps); }
            set { sQM800U2FProps = LosConvert.GfeItemProps_UpdateApr(sQM800U2FProps, value); }
        }

        [DependsOn(nameof(sQM800U2FProps))]
        public bool sQM800U2FProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQM800U2FProps); }
            set { sQM800U2FProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQM800U2FProps, value); }
        }

        [DependsOn(nameof(sQM800U2FProps))]
        public bool sQM800U2FProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQM800U2FProps); }
            set { sQM800U2FProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQM800U2FProps, value); }
        }

        [DependsOn(nameof(sQM800U2FProps))]
        [DevNote("sQM800U2FProps Show QM Warning bit.")]
        public bool sQM800U2FProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQM800U2FProps); }
            set { sQM800U2FProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQM800U2FProps, value); }
        }

        [DependsOn(nameof(sQM800U2F), nameof(sQM800U2FProps))]
        public decimal sQM800U2F_QMAmount
        {
            get { return GetQMField_QMAmount(sQM800U2F, sQM800U2FProps, true); }
        }

        public string sQM800U2F_QMAmount_rep
        {
            get { return ToMoneyString(() => sQM800U2F_QMAmount); }
        }
        #endregion
        #region sQM800U3F
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(s800U3FDesc), nameof(sSettlement800U3FDesc))]
        public string sQM800U3FDesc
        {
            get { return GetQMField_Desc(sQmSettlementChargesExportSource, s800U3FDesc, sSettlement800U3FDesc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(s800U3F), nameof(sSettlement800U3F))]
        public decimal sQM800U3F
        {
            get { return GetQMField(sQmSettlementChargesExportSource, s800U3F, sSettlement800U3F); }
        }

        public string sQM800U3F_rep
        {
            get { return ToMoneyString(() => sQM800U3F); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(s800U3FProps), nameof(sSettlement800U3FProps))]
        public int sQM800U3FProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, s800U3FProps, sSettlement800U3FProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => s800U3FProps = t, t => sSettlement800U3FProps = t, value); }
        }

        [DependsOn(nameof(sQM800U3FProps))]
        public int sQM800U3FProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQM800U3FProps); }
            set { sQM800U3FProps = LosConvert.GfeItemProps_UpdatePayer(sQM800U3FProps, value); }
        }

        [DependsOn(nameof(sQM800U3FProps))]
        public bool sQM800U3FProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQM800U3FProps); }
            set { sQM800U3FProps = LosConvert.GfeItemProps_UpdateApr(sQM800U3FProps, value); }
        }

        [DependsOn(nameof(sQM800U3FProps))]
        public bool sQM800U3FProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQM800U3FProps); }
            set { sQM800U3FProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQM800U3FProps, value); }
        }

        [DependsOn(nameof(sQM800U3FProps))]
        public bool sQM800U3FProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQM800U3FProps); }
            set { sQM800U3FProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQM800U3FProps, value); }
        }

        [DependsOn(nameof(sQM800U3FProps))]
        [DevNote("sQM800U3FProps Show QM Warning bit.")]
        public bool sQM800U3FProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQM800U3FProps); }
            set { sQM800U3FProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQM800U3FProps, value); }
        }

        [DependsOn(nameof(sQM800U3F), nameof(sQM800U3FProps))]
        public decimal sQM800U3F_QMAmount
        {
            get { return GetQMField_QMAmount(sQM800U3F, sQM800U3FProps, true); }
        }

        public string sQM800U3F_QMAmount_rep
        {
            get { return ToMoneyString(() => sQM800U3F_QMAmount); }
        }
        #endregion
        #region sQM800U4F
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(s800U4FDesc), nameof(sSettlement800U4FDesc))]
        public string sQM800U4FDesc
        {
            get { return GetQMField_Desc(sQmSettlementChargesExportSource, s800U4FDesc, sSettlement800U4FDesc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(s800U4F), nameof(sSettlement800U4F))]
        public decimal sQM800U4F
        {
            get { return GetQMField(sQmSettlementChargesExportSource, s800U4F, sSettlement800U4F); }
        }

        public string sQM800U4F_rep
        {
            get { return ToMoneyString(() => sQM800U4F); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(s800U4FProps), nameof(sSettlement800U4FProps))]
        public int sQM800U4FProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, s800U4FProps, sSettlement800U4FProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => s800U4FProps = t, t => sSettlement800U4FProps = t, value); }
        }

        [DependsOn(nameof(sQM800U4FProps))]
        public int sQM800U4FProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQM800U4FProps); }
            set { sQM800U4FProps = LosConvert.GfeItemProps_UpdatePayer(sQM800U4FProps, value); }
        }

        [DependsOn(nameof(sQM800U4FProps))]
        public bool sQM800U4FProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQM800U4FProps); }
            set { sQM800U4FProps = LosConvert.GfeItemProps_UpdateApr(sQM800U4FProps, value); }
        }

        [DependsOn(nameof(sQM800U4FProps))]
        public bool sQM800U4FProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQM800U4FProps); }
            set { sQM800U4FProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQM800U4FProps, value); }
        }

        [DependsOn(nameof(sQM800U4FProps))]
        public bool sQM800U4FProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQM800U4FProps); }
            set { sQM800U4FProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQM800U4FProps, value); }
        }

        [DependsOn(nameof(sQM800U4FProps))]
        [DevNote("sQM800U4FProps Show QM Warning bit.")]
        public bool sQM800U4FProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQM800U4FProps); }
            set { sQM800U4FProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQM800U4FProps, value); }
        }

        [DependsOn(nameof(sQM800U4F), nameof(sQM800U4FProps))]
        public decimal sQM800U4F_QMAmount
        {
            get { return GetQMField_QMAmount(sQM800U4F, sQM800U4FProps, true); }
        }

        public string sQM800U4F_QMAmount_rep
        {
            get { return ToMoneyString(() => sQM800U4F_QMAmount); }
        }
        #endregion
        #region sQM800U5F
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(s800U5FDesc), nameof(sSettlement800U5FDesc))]
        public string sQM800U5FDesc
        {
            get { return GetQMField_Desc(sQmSettlementChargesExportSource, s800U5FDesc, sSettlement800U5FDesc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(s800U5F), nameof(sSettlement800U5F))]
        public decimal sQM800U5F
        {
            get { return GetQMField(sQmSettlementChargesExportSource, s800U5F, sSettlement800U5F); }
        }

        public string sQM800U5F_rep
        {
            get { return ToMoneyString(() => sQM800U5F); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(s800U5FProps), nameof(sSettlement800U5FProps))]
        public int sQM800U5FProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, s800U5FProps, sSettlement800U5FProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => s800U5FProps = t, t => sSettlement800U5FProps = t, value); }
        }

        [DependsOn(nameof(sQM800U5FProps))]
        public int sQM800U5FProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQM800U5FProps); }
            set { sQM800U5FProps = LosConvert.GfeItemProps_UpdatePayer(sQM800U5FProps, value); }
        }

        [DependsOn(nameof(sQM800U5FProps))]
        public bool sQM800U5FProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQM800U5FProps); }
            set { sQM800U5FProps = LosConvert.GfeItemProps_UpdateApr(sQM800U5FProps, value); }
        }

        [DependsOn(nameof(sQM800U5FProps))]
        public bool sQM800U5FProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQM800U5FProps); }
            set { sQM800U5FProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQM800U5FProps, value); }
        }

        [DependsOn(nameof(sQM800U5FProps))]
        public bool sQM800U5FProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQM800U5FProps); }
            set { sQM800U5FProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQM800U5FProps, value); }
        }

        [DependsOn(nameof(sQM800U5FProps))]
        [DevNote("sQM800U5FProps Show QM Warning bit.")]
        public bool sQM800U5FProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQM800U5FProps); }
            set { sQM800U5FProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQM800U5FProps, value); }
        }

        [DependsOn(nameof(sQM800U5F), nameof(sQM800U5FProps))]
        public decimal sQM800U5F_QMAmount
        {
            get { return GetQMField_QMAmount(sQM800U5F, sQM800U5FProps, true); }
        }

        public string sQM800U5F_QMAmount_rep
        {
            get { return ToMoneyString(() => sQM800U5F_QMAmount); }
        }
        #endregion
        #region sQMIPia
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sIPia), nameof(sSettlementIPia))]
        public decimal sQMIPia
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sIPia, sSettlementIPia); }
        }

        public string sQMIPia_rep
        {
            get { return ToMoneyString(() => sQMIPia); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sIPiaProps), nameof(sSettlementIPiaProps))]
        public int sQMIPiaProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sIPiaProps, sSettlementIPiaProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sIPiaProps = t, t => sSettlementIPiaProps = t, value); }
        }

        [DependsOn(nameof(sQMIPiaProps))]
        public int sQMIPiaProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMIPiaProps); }
            set { sQMIPiaProps = LosConvert.GfeItemProps_UpdatePayer(sQMIPiaProps, value); }
        }

        [DependsOn(nameof(sQMIPiaProps))]
        public bool sQMIPiaProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMIPiaProps); }
            set { sQMIPiaProps = LosConvert.GfeItemProps_UpdateApr(sQMIPiaProps, value); }
        }

        [DependsOn(nameof(sQMIPiaProps))]
        public bool sQMIPiaProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMIPiaProps); }
            set { sQMIPiaProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMIPiaProps, value); }
        }

        [DependsOn(nameof(sQMIPiaProps))]
        public bool sQMIPiaProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMIPiaProps); }
            set { sQMIPiaProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMIPiaProps, value); }
        }

        [DependsOn(nameof(sQMIPiaProps))]
        [DevNote("sQMIPiaProps Show QM Warning bit.")]
        public bool sQMIPiaProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMIPiaProps); }
            set { sQMIPiaProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMIPiaProps, value); }
        }

        [DependsOn(nameof(sQMIPia), nameof(sQMIPiaProps))]
        public decimal sQMIPia_QMAmount
        {
            get { return 0; }   // Excluded value. QM Amount is always $0.00
        }

        public string sQMIPia_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMIPia_QMAmount); }
        }
        #endregion
        #region sQMMipPia
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sMipPia))]
        public decimal sQMMipPia
        {
            get { return sMipPia; }
        }

        public string sQMMipPia_rep
        {
            get { return ToMoneyString(() => sQMMipPia); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sMipPiaProps))]
        public int sQMMipPiaProps
        {
            get { return sMipPiaProps; }
            set { sMipPiaProps = value; }
        }

        [DependsOn(nameof(sQMMipPiaProps))]
        public int sQMMipPiaProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMMipPiaProps); }
            set { sQMMipPiaProps = LosConvert.GfeItemProps_UpdatePayer(sQMMipPiaProps, value); }
        }

        [DependsOn(nameof(sQMMipPiaProps))]
        public bool sQMMipPiaProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMMipPiaProps); }
            set { sQMMipPiaProps = LosConvert.GfeItemProps_UpdateApr(sQMMipPiaProps, value); }
        }

        //[DependsOn(nameof(sQMMipPiaProps))]
        //public bool sQMMipPiaProps_PaidToThirdParty
        //{
        //    get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMMipPiaProps); }
        //    set { sQMMipPiaProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMMipPiaProps, value); }
        //}

        //[DependsOn(nameof(sQMMipPiaProps))]
        //public bool sQMMipPiaProps_ThisPartyIsAffiliate
        //{
        //    get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMMipPiaProps); }
        //    set { sQMMipPiaProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMMipPiaProps, value); }
        //}

        //[DependsOn(nameof(sQMMipPiaProps))]
        //[DevNote("sQMMipPiaProps Show QM Warning bit.")]
        //public bool sQMMipPiaProps_ShowQmWarning
        //{
        //    get { return LosConvert.GfeItemProps_ShowQmWarning(sQMMipPiaProps); }
        //    set { sQMMipPiaProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMMipPiaProps, value); }
        //}

        [DependsOn(nameof(sQMMipPia), nameof(sQMMipPiaProps))]
        public decimal sQMMipPia_QMAmount
        {
            get { return 0; }   // Excluded value. QM Amount is always $0.00
        }

        public string sQMMipPia_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMMipPia_QMAmount); }
        }
        #endregion
        #region sQMHazInsPia
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sHazInsPia), nameof(sSettlementHazInsPia))]
        public decimal sQMHazInsPia
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sHazInsPia, sSettlementHazInsPia); }
        }

        public string sQMHazInsPia_rep
        {
            get { return ToMoneyString(() => sQMHazInsPia); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sHazInsPiaProps), nameof(sSettlementHazInsPiaProps))]
        public int sQMHazInsPiaProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sHazInsPiaProps, sSettlementHazInsPiaProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sHazInsPiaProps = t, t => sSettlementHazInsPiaProps = t, value); }
        }

        [DependsOn(nameof(sQMHazInsPiaProps))]
        public int sQMHazInsPiaProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMHazInsPiaProps); }
            set { sQMHazInsPiaProps = LosConvert.GfeItemProps_UpdatePayer(sQMHazInsPiaProps, value); }
        }

        [DependsOn(nameof(sQMHazInsPiaProps))]
        public bool sQMHazInsPiaProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMHazInsPiaProps); }
            set { sQMHazInsPiaProps = LosConvert.GfeItemProps_UpdateApr(sQMHazInsPiaProps, value); }
        }

        [DependsOn(nameof(sQMHazInsPiaProps))]
        public bool sQMHazInsPiaProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMHazInsPiaProps); }
            set { sQMHazInsPiaProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMHazInsPiaProps, value); }
        }

        [DependsOn(nameof(sQMHazInsPiaProps))]
        public bool sQMHazInsPiaProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMHazInsPiaProps); }
            set { sQMHazInsPiaProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMHazInsPiaProps, value); }
        }

        [DependsOn(nameof(sQMHazInsPiaProps))]
        [DevNote("sQMHazInsPiaProps Show QM Warning bit.")]
        public bool sQMHazInsPiaProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMHazInsPiaProps); }
            set { sQMHazInsPiaProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMHazInsPiaProps, value); }
        }

        [DependsOn(nameof(sQMHazInsPia), nameof(sQMHazInsPiaProps))]
        public decimal sQMHazInsPia_QMAmount
        {
            get { return GetQMField_QMAmount(sQMHazInsPia, sQMHazInsPiaProps, true); }
        }

        public string sQMHazInsPia_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMHazInsPia_QMAmount); }
        }
        #endregion
        #region sQM904Pia
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(s904PiaDesc), nameof(sSettlement904PiaDesc))]
        public string sQM904PiaDesc
        {
            get { return GetQMField_Desc(sQmSettlementChargesExportSource, s904PiaDesc, sSettlement904PiaDesc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(s904Pia), nameof(sSettlement904Pia))]
        public decimal sQM904Pia
        {
            get { return GetQMField(sQmSettlementChargesExportSource, s904Pia, sSettlement904Pia); }
        }

        public string sQM904Pia_rep
        {
            get { return ToMoneyString(() => sQM904Pia); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(s904PiaProps), nameof(sSettlement904PiaProps))]
        public int sQM904PiaProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, s904PiaProps, sSettlement904PiaProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => s904PiaProps = t, t => sSettlement904PiaProps = t, value); }
        }

        [DependsOn(nameof(sQM904PiaProps))]
        public int sQM904PiaProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQM904PiaProps); }
            set { sQM904PiaProps = LosConvert.GfeItemProps_UpdatePayer(sQM904PiaProps, value); }
        }

        [DependsOn(nameof(sQM904PiaProps))]
        public bool sQM904PiaProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQM904PiaProps); }
            set { sQM904PiaProps = LosConvert.GfeItemProps_UpdateApr(sQM904PiaProps, value); }
        }

        [DependsOn(nameof(sQM904PiaProps))]
        public bool sQM904PiaProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQM904PiaProps); }
            set { sQM904PiaProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQM904PiaProps, value); }
        }

        [DependsOn(nameof(sQM904PiaProps))]
        public bool sQM904PiaProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQM904PiaProps); }
            set { sQM904PiaProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQM904PiaProps, value); }
        }

        [DependsOn(nameof(sQM904PiaProps))]
        [DevNote("sQM904PiaProps Show QM Warning bit.")]
        public bool sQM904PiaProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQM904PiaProps); }
            set { sQM904PiaProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQM904PiaProps, value); }
        }

        [DependsOn(nameof(sQM904Pia), nameof(sQM904PiaProps))]
        public decimal sQM904Pia_QMAmount
        {
            get { return GetQMField_QMAmount(sQM904Pia, sQM904PiaProps, true); }
        }

        public string sQM904Pia_QMAmount_rep
        {
            get { return ToMoneyString(() => sQM904Pia_QMAmount); }
        }
        #endregion
        #region sQMVaFf
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sVaFf))]
        public decimal sQMVaFf
        {
            get { return sVaFf; }
        }

        public string sQMVaFf_rep
        {
            get { return ToMoneyString(() => sQMVaFf); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sVaFfProps))]
        public int sQMVaFfProps
        {
            get { return sVaFfProps; }
            set { sVaFfProps = value; }
        }

        [DependsOn(nameof(sQMVaFfProps))]
        public int sQMVaFfProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMVaFfProps); }
            set { sQMVaFfProps = LosConvert.GfeItemProps_UpdatePayer(sQMVaFfProps, value); }
        }

        [DependsOn(nameof(sQMVaFfProps))]
        public bool sQMVaFfProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMVaFfProps); }
            set { sQMVaFfProps = LosConvert.GfeItemProps_UpdateApr(sQMVaFfProps, value); }
        }

        [DependsOn(nameof(sQMVaFfProps))]
        public bool sQMVaFfProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMVaFfProps); }
            set { sQMVaFfProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMVaFfProps, value); }
        }

        [DependsOn(nameof(sQMVaFfProps))]
        public bool sQMVaFfProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMVaFfProps); }
            set { sQMVaFfProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMVaFfProps, value); }
        }

        [DependsOn(nameof(sQMVaFfProps))]
        [DevNote("sQMVaFfProps Show QM Warning bit.")]
        public bool sQMVaFfProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMVaFfProps); }
            set { sQMVaFfProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMVaFfProps, value); }
        }

        [DependsOn(nameof(sQMVaFf), nameof(sQMVaFfProps))]
        public decimal sQMVaFf_QMAmount
        {
            get { return 0; }   // Excluded value. QM Amount is always $0.00
        }

        public string sQMVaFf_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMVaFf_QMAmount); }
        }
        #endregion
        #region sQM900U1Pia
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(s900U1PiaDesc), nameof(sSettlement900U1PiaDesc))]
        public string sQM900U1PiaDesc
        {
            get { return GetQMField_Desc(sQmSettlementChargesExportSource, s900U1PiaDesc, sSettlement900U1PiaDesc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(s900U1Pia), nameof(sSettlement900U1Pia))]
        public decimal sQM900U1Pia
        {
            get { return GetQMField(sQmSettlementChargesExportSource, s900U1Pia, sSettlement900U1Pia); }
        }

        public string sQM900U1Pia_rep
        {
            get { return ToMoneyString(() => sQM900U1Pia); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(s900U1PiaProps), nameof(sSettlement900U1PiaProps))]
        public int sQM900U1PiaProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, s900U1PiaProps, sSettlement900U1PiaProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => s900U1PiaProps = t, t => sSettlement900U1PiaProps = t, value); }
        }

        [DependsOn(nameof(sQM900U1PiaProps))]
        public int sQM900U1PiaProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQM900U1PiaProps); }
            set { sQM900U1PiaProps = LosConvert.GfeItemProps_UpdatePayer(sQM900U1PiaProps, value); }
        }

        [DependsOn(nameof(sQM900U1PiaProps))]
        public bool sQM900U1PiaProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQM900U1PiaProps); }
            set { sQM900U1PiaProps = LosConvert.GfeItemProps_UpdateApr(sQM900U1PiaProps, value); }
        }

        [DependsOn(nameof(sQM900U1PiaProps))]
        public bool sQM900U1PiaProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQM900U1PiaProps); }
            set { sQM900U1PiaProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQM900U1PiaProps, value); }
        }

        [DependsOn(nameof(sQM900U1PiaProps))]
        public bool sQM900U1PiaProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQM900U1PiaProps); }
            set { sQM900U1PiaProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQM900U1PiaProps, value); }
        }

        [DependsOn(nameof(sQM900U1PiaProps))]
        [DevNote("sQM900U1PiaProps Show QM Warning bit.")]
        public bool sQM900U1PiaProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQM900U1PiaProps); }
            set { sQM900U1PiaProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQM900U1PiaProps, value); }
        }

        [DependsOn(nameof(sQM900U1Pia), nameof(sQM900U1PiaProps))]
        public decimal sQM900U1Pia_QMAmount
        {
            get { return GetQMField_QMAmount(sQM900U1Pia, sQM900U1PiaProps, true); }
        }

        public string sQM900U1Pia_QMAmount_rep
        {
            get { return ToMoneyString(() => sQM900U1Pia_QMAmount); }
        }
        #endregion
        #region sQMHazInsRsrv
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sHazInsRsrv), nameof(sSettlementHazInsRsrv))]
        public decimal sQMHazInsRsrv
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sHazInsRsrv, sSettlementHazInsRsrv); }
        }

        public string sQMHazInsRsrv_rep
        {
            get { return ToMoneyString(() => sQMHazInsRsrv); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sHazInsRsrvProps), nameof(sSettlementHazInsRsrvProps))]
        public int sQMHazInsRsrvProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sHazInsRsrvProps, sSettlementHazInsRsrvProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sHazInsRsrvProps = t, t => sSettlementHazInsRsrvProps = t, value); }
        }

        [DependsOn(nameof(sQMHazInsRsrvProps))]
        public int sQMHazInsRsrvProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMHazInsRsrvProps); }
            set { sQMHazInsRsrvProps = LosConvert.GfeItemProps_UpdatePayer(sQMHazInsRsrvProps, value); }
        }

        [DependsOn(nameof(sQMHazInsRsrvProps))]
        public bool sQMHazInsRsrvProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMHazInsRsrvProps); }
            set { sQMHazInsRsrvProps = LosConvert.GfeItemProps_UpdateApr(sQMHazInsRsrvProps, value); }
        }

        [DependsOn(nameof(sQMHazInsRsrvProps))]
        public bool sQMHazInsRsrvProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMHazInsRsrvProps); }
            set { sQMHazInsRsrvProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMHazInsRsrvProps, value); }
        }

        [DependsOn(nameof(sQMHazInsRsrvProps))]
        public bool sQMHazInsRsrvProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMHazInsRsrvProps); }
            set { sQMHazInsRsrvProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMHazInsRsrvProps, value); }
        }

        [DependsOn(nameof(sQMHazInsRsrvProps))]
        [DevNote("sQMHazInsRsrvProps Show QM Warning bit.")]
        public bool sQMHazInsRsrvProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMHazInsRsrvProps); }
            set { sQMHazInsRsrvProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMHazInsRsrvProps, value); }
        }

        [DependsOn(nameof(sQMHazInsRsrv), nameof(sQMHazInsRsrvProps))]
        public decimal sQMHazInsRsrv_QMAmount
        {
            get { return GetQMField_QMAmount(sQMHazInsRsrv, sQMHazInsRsrvProps, false); }
        }

        public string sQMHazInsRsrv_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMHazInsRsrv_QMAmount); }
        }
        #endregion
        #region sQMMInsRsrv
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sMInsRsrv), nameof(sSettlementMInsRsrv))]
        public decimal sQMMInsRsrv
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sMInsRsrv, sSettlementMInsRsrv); }
        }

        public string sQMMInsRsrv_rep
        {
            get { return ToMoneyString(() => sQMMInsRsrv); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sMInsRsrvProps), nameof(sSettlementMInsRsrvProps))]
        public int sQMMInsRsrvProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sMInsRsrvProps, sSettlementMInsRsrvProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sMInsRsrvProps = t, t => sSettlementMInsRsrvProps = t, value); }
        }

        [DependsOn(nameof(sQMMInsRsrvProps))]
        public int sQMMInsRsrvProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMMInsRsrvProps); }
            set { sQMMInsRsrvProps = LosConvert.GfeItemProps_UpdatePayer(sQMMInsRsrvProps, value); }
        }

        [DependsOn(nameof(sQMMInsRsrvProps))]
        public bool sQMMInsRsrvProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMMInsRsrvProps); }
            set { sQMMInsRsrvProps = LosConvert.GfeItemProps_UpdateApr(sQMMInsRsrvProps, value); }
        }

        [DependsOn(nameof(sQMMInsRsrvProps))]
        public bool sQMMInsRsrvProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMMInsRsrvProps); }
            set { sQMMInsRsrvProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMMInsRsrvProps, value); }
        }

        [DependsOn(nameof(sQMMInsRsrvProps))]
        public bool sQMMInsRsrvProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMMInsRsrvProps); }
            set { sQMMInsRsrvProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMMInsRsrvProps, value); }
        }

        [DependsOn(nameof(sQMMInsRsrvProps))]
        [DevNote("sQMMInsRsrvProps Show QM Warning bit.")]
        public bool sQMMInsRsrvProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMMInsRsrvProps); }
            set { sQMMInsRsrvProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMMInsRsrvProps, value); }
        }

        [DependsOn(nameof(sQMMInsRsrv), nameof(sQMMInsRsrvProps))]
        public decimal sQMMInsRsrv_QMAmount
        {
            get { return GetQMField_QMAmount(sQMMInsRsrv, sQMMInsRsrvProps, false); }
        }

        public string sQMMInsRsrv_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMMInsRsrv_QMAmount); }
        }
        #endregion
        #region sQMRealETxRsrv
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sRealETxRsrv), nameof(sSettlementRealETxRsrv))]
        public decimal sQMRealETxRsrv
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sRealETxRsrv, sSettlementRealETxRsrv); }
        }

        public string sQMRealETxRsrv_rep
        {
            get { return ToMoneyString(() => sQMRealETxRsrv); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sRealETxRsrvProps), nameof(sSettlementRealETxRsrvProps))]
        public int sQMRealETxRsrvProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sRealETxRsrvProps, sSettlementRealETxRsrvProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sRealETxRsrvProps = t, t => sSettlementRealETxRsrvProps = t, value); }
        }

        [DependsOn(nameof(sQMRealETxRsrvProps))]
        public int sQMRealETxRsrvProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMRealETxRsrvProps); }
            set { sQMRealETxRsrvProps = LosConvert.GfeItemProps_UpdatePayer(sQMRealETxRsrvProps, value); }
        }

        [DependsOn(nameof(sQMRealETxRsrvProps))]
        public bool sQMRealETxRsrvProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMRealETxRsrvProps); }
            set { sQMRealETxRsrvProps = LosConvert.GfeItemProps_UpdateApr(sQMRealETxRsrvProps, value); }
        }

        [DependsOn(nameof(sQMRealETxRsrvProps))]
        public bool sQMRealETxRsrvProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMRealETxRsrvProps); }
            set { sQMRealETxRsrvProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMRealETxRsrvProps, value); }
        }

        [DependsOn(nameof(sQMRealETxRsrvProps))]
        public bool sQMRealETxRsrvProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMRealETxRsrvProps); }
            set { sQMRealETxRsrvProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMRealETxRsrvProps, value); }
        }

        [DependsOn(nameof(sQMRealETxRsrvProps))]
        [DevNote("sQMRealETxRsrvProps Show QM Warning bit.")]
        public bool sQMRealETxRsrvProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMRealETxRsrvProps); }
            set { sQMRealETxRsrvProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMRealETxRsrvProps, value); }
        }

        [DependsOn(nameof(sQMRealETxRsrv), nameof(sQMRealETxRsrvProps))]
        public decimal sQMRealETxRsrv_QMAmount
        {
            get { return GetQMField_QMAmount(sQMRealETxRsrv, sQMRealETxRsrvProps, true); }
        }

        public string sQMRealETxRsrv_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMRealETxRsrv_QMAmount); }
        }
        #endregion
        #region sQMSchoolTxRsrv
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sSchoolTxRsrv), nameof(sSettlementSchoolTxRsrv))]
        public decimal sQMSchoolTxRsrv
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sSchoolTxRsrv, sSettlementSchoolTxRsrv); }
        }

        public string sQMSchoolTxRsrv_rep
        {
            get { return ToMoneyString(() => sQMSchoolTxRsrv); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sSchoolTxRsrvProps), nameof(sSettlementSchoolTxRsrvProps))]
        public int sQMSchoolTxRsrvProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sSchoolTxRsrvProps, sSettlementSchoolTxRsrvProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sSchoolTxRsrvProps = t, t => sSettlementSchoolTxRsrvProps = t, value); }
        }

        [DependsOn(nameof(sQMSchoolTxRsrvProps))]
        public int sQMSchoolTxRsrvProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMSchoolTxRsrvProps); }
            set { sQMSchoolTxRsrvProps = LosConvert.GfeItemProps_UpdatePayer(sQMSchoolTxRsrvProps, value); }
        }

        [DependsOn(nameof(sQMSchoolTxRsrvProps))]
        public bool sQMSchoolTxRsrvProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMSchoolTxRsrvProps); }
            set { sQMSchoolTxRsrvProps = LosConvert.GfeItemProps_UpdateApr(sQMSchoolTxRsrvProps, value); }
        }

        [DependsOn(nameof(sQMSchoolTxRsrvProps))]
        public bool sQMSchoolTxRsrvProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMSchoolTxRsrvProps); }
            set { sQMSchoolTxRsrvProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMSchoolTxRsrvProps, value); }
        }

        [DependsOn(nameof(sQMSchoolTxRsrvProps))]
        public bool sQMSchoolTxRsrvProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMSchoolTxRsrvProps); }
            set { sQMSchoolTxRsrvProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMSchoolTxRsrvProps, value); }
        }

        [DependsOn(nameof(sQMSchoolTxRsrvProps))]
        [DevNote("sQMSchoolTxRsrvProps Show QM Warning bit.")]
        public bool sQMSchoolTxRsrvProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMSchoolTxRsrvProps); }
            set { sQMSchoolTxRsrvProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMSchoolTxRsrvProps, value); }
        }

        [DependsOn(nameof(sQMSchoolTxRsrv), nameof(sQMSchoolTxRsrvProps))]
        public decimal sQMSchoolTxRsrv_QMAmount
        {
            get { return GetQMField_QMAmount(sQMSchoolTxRsrv, sQMSchoolTxRsrvProps, true); }
        }

        public string sQMSchoolTxRsrv_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMSchoolTxRsrv_QMAmount); }
        }
        #endregion
        #region sQMFloodInsRsrv
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sFloodInsRsrv), nameof(sSettlementFloodInsRsrv))]
        public decimal sQMFloodInsRsrv
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sFloodInsRsrv, sSettlementFloodInsRsrv); }
        }

        public string sQMFloodInsRsrv_rep
        {
            get { return ToMoneyString(() => sQMFloodInsRsrv); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sFloodInsRsrvProps), nameof(sSettlementFloodInsRsrvProps))]
        public int sQMFloodInsRsrvProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sFloodInsRsrvProps, sSettlementFloodInsRsrvProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sFloodInsRsrvProps = t, t => sSettlementFloodInsRsrvProps = t, value); }
        }

        [DependsOn(nameof(sQMFloodInsRsrvProps))]
        public int sQMFloodInsRsrvProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMFloodInsRsrvProps); }
            set { sQMFloodInsRsrvProps = LosConvert.GfeItemProps_UpdatePayer(sQMFloodInsRsrvProps, value); }
        }

        [DependsOn(nameof(sQMFloodInsRsrvProps))]
        public bool sQMFloodInsRsrvProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMFloodInsRsrvProps); }
            set { sQMFloodInsRsrvProps = LosConvert.GfeItemProps_UpdateApr(sQMFloodInsRsrvProps, value); }
        }

        [DependsOn(nameof(sQMFloodInsRsrvProps))]
        public bool sQMFloodInsRsrvProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMFloodInsRsrvProps); }
            set { sQMFloodInsRsrvProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMFloodInsRsrvProps, value); }
        }

        [DependsOn(nameof(sQMFloodInsRsrvProps))]
        public bool sQMFloodInsRsrvProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMFloodInsRsrvProps); }
            set { sQMFloodInsRsrvProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMFloodInsRsrvProps, value); }
        }

        [DependsOn(nameof(sQMFloodInsRsrvProps))]
        [DevNote("sQMFloodInsRsrvProps Show QM Warning bit.")]
        public bool sQMFloodInsRsrvProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMFloodInsRsrvProps); }
            set { sQMFloodInsRsrvProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMFloodInsRsrvProps, value); }
        }

        [DependsOn(nameof(sQMFloodInsRsrv), nameof(sQMFloodInsRsrvProps))]
        public decimal sQMFloodInsRsrv_QMAmount
        {
            get { return GetQMField_QMAmount(sQMFloodInsRsrv, sQMFloodInsRsrvProps, false); }
        }

        public string sQMFloodInsRsrv_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMFloodInsRsrv_QMAmount); }
        }
        #endregion
        #region sQMAggregateAdjRsrv
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sAggregateAdjRsrv), nameof(sSettlementAggregateAdjRsrv))]
        public decimal sQMAggregateAdjRsrv
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sAggregateAdjRsrv, sSettlementAggregateAdjRsrv); }
        }

        public string sQMAggregateAdjRsrv_rep
        {
            get { return ToMoneyString(() => sQMAggregateAdjRsrv); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sAggregateAdjRsrvProps), nameof(sSettlementAggregateAdjRsrvProps))]
        public int sQMAggregateAdjRsrvProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sAggregateAdjRsrvProps, sSettlementAggregateAdjRsrvProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sAggregateAdjRsrvProps = t, t => sSettlementAggregateAdjRsrvProps = t, value); }
        }

        [DependsOn(nameof(sQMAggregateAdjRsrvProps))]
        public int sQMAggregateAdjRsrvProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMAggregateAdjRsrvProps); }
            set { sQMAggregateAdjRsrvProps = LosConvert.GfeItemProps_UpdatePayer(sQMAggregateAdjRsrvProps, value); }
        }

        [DependsOn(nameof(sQMAggregateAdjRsrvProps))]
        public bool sQMAggregateAdjRsrvProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMAggregateAdjRsrvProps); }
            set { sQMAggregateAdjRsrvProps = LosConvert.GfeItemProps_UpdateApr(sQMAggregateAdjRsrvProps, value); }
        }

        [DependsOn(nameof(sQMAggregateAdjRsrvProps))]
        public bool sQMAggregateAdjRsrvProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMAggregateAdjRsrvProps); }
            set { sQMAggregateAdjRsrvProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMAggregateAdjRsrvProps, value); }
        }

        [DependsOn(nameof(sQMAggregateAdjRsrvProps))]
        public bool sQMAggregateAdjRsrvProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMAggregateAdjRsrvProps); }
            set { sQMAggregateAdjRsrvProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMAggregateAdjRsrvProps, value); }
        }

        [DependsOn(nameof(sQMAggregateAdjRsrvProps))]
        [DevNote("sQMAggregateAdjRsrvProps Show QM Warning bit.")]
        public bool sQMAggregateAdjRsrvProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMAggregateAdjRsrvProps); }
            set { sQMAggregateAdjRsrvProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMAggregateAdjRsrvProps, value); }
        }

        [DependsOn(nameof(sQMAggregateAdjRsrv), nameof(sQMAggregateAdjRsrvProps))]
        public decimal sQMAggregateAdjRsrv_QMAmount
        {
            get { return GetQMField_QMAmount(sQMAggregateAdjRsrv, sQMAggregateAdjRsrvProps, true); }
        }

        public string sQMAggregateAdjRsrv_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMAggregateAdjRsrv_QMAmount); }
        }
        #endregion
        #region sQM1006Rsrv
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(s1006ProHExpDesc))]
        public string sQM1006ProHExpDesc
        {
            get { return s1006ProHExpDesc; }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(s1006Rsrv), nameof(sSettlement1008Rsrv))]
        public decimal sQM1006Rsrv
        {
            get { return GetQMField(sQmSettlementChargesExportSource, s1006Rsrv, sSettlement1008Rsrv); }
        }

        public string sQM1006Rsrv_rep
        {
            get { return ToMoneyString(() => sQM1006Rsrv); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(s1006RsrvProps), nameof(sSettlement1008RsrvProps))]
        public int sQM1006RsrvProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, s1006RsrvProps, sSettlement1008RsrvProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => s1006RsrvProps = t, t => sSettlement1008RsrvProps = t, value); }
        }

        [DependsOn(nameof(sQM1006RsrvProps))]
        public int sQM1006RsrvProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQM1006RsrvProps); }
            set { sQM1006RsrvProps = LosConvert.GfeItemProps_UpdatePayer(sQM1006RsrvProps, value); }
        }

        [DependsOn(nameof(sQM1006RsrvProps))]
        public bool sQM1006RsrvProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQM1006RsrvProps); }
            set { sQM1006RsrvProps = LosConvert.GfeItemProps_UpdateApr(sQM1006RsrvProps, value); }
        }

        [DependsOn(nameof(sQM1006RsrvProps))]
        public bool sQM1006RsrvProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQM1006RsrvProps); }
            set { sQM1006RsrvProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQM1006RsrvProps, value); }
        }

        [DependsOn(nameof(sQM1006RsrvProps))]
        public bool sQM1006RsrvProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQM1006RsrvProps); }
            set { sQM1006RsrvProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQM1006RsrvProps, value); }
        }

        [DependsOn(nameof(sQM1006RsrvProps))]
        [DevNote("sQM1006RsrvProps Show QM Warning bit.")]
        public bool sQM1006RsrvProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQM1006RsrvProps); }
            set { sQM1006RsrvProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQM1006RsrvProps, value); }
        }

        [DependsOn(nameof(sQM1006Rsrv), nameof(sQM1006RsrvProps))]
        public decimal sQM1006Rsrv_QMAmount
        {
            get { return GetQMField_QMAmount(sQM1006Rsrv, sQM1006RsrvProps, false); }
        }

        public string sQM1006Rsrv_QMAmount_rep
        {
            get { return ToMoneyString(() => sQM1006Rsrv_QMAmount); }
        }
        #endregion
        #region sQM1007Rsrv
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(s1007ProHExpDesc))]
        public string sQM1007ProHExpDesc
        {
            get { return s1007ProHExpDesc; }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(s1007Rsrv), nameof(sSettlement1009Rsrv))]
        public decimal sQM1007Rsrv
        {
            get { return GetQMField(sQmSettlementChargesExportSource, s1007Rsrv, sSettlement1009Rsrv); }
        }

        public string sQM1007Rsrv_rep
        {
            get { return ToMoneyString(() => sQM1007Rsrv); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(s1007RsrvProps), nameof(sSettlement1009RsrvProps))]
        public int sQM1007RsrvProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, s1007RsrvProps, sSettlement1009RsrvProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => s1007RsrvProps = t, t => sSettlement1009RsrvProps = t, value); }
        }

        [DependsOn(nameof(sQM1007RsrvProps))]
        public int sQM1007RsrvProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQM1007RsrvProps); }
            set { sQM1007RsrvProps = LosConvert.GfeItemProps_UpdatePayer(sQM1007RsrvProps, value); }
        }

        [DependsOn(nameof(sQM1007RsrvProps))]
        public bool sQM1007RsrvProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQM1007RsrvProps); }
            set { sQM1007RsrvProps = LosConvert.GfeItemProps_UpdateApr(sQM1007RsrvProps, value); }
        }

        [DependsOn(nameof(sQM1007RsrvProps))]
        public bool sQM1007RsrvProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQM1007RsrvProps); }
            set { sQM1007RsrvProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQM1007RsrvProps, value); }
        }

        [DependsOn(nameof(sQM1007RsrvProps))]
        public bool sQM1007RsrvProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQM1007RsrvProps); }
            set { sQM1007RsrvProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQM1007RsrvProps, value); }
        }

        [DependsOn(nameof(sQM1007RsrvProps))]
        [DevNote("sQM1007RsrvProps Show QM Warning bit.")]
        public bool sQM1007RsrvProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQM1007RsrvProps); }
            set { sQM1007RsrvProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQM1007RsrvProps, value); }
        }

        [DependsOn(nameof(sQM1007Rsrv), nameof(sQM1007RsrvProps))]
        public decimal sQM1007Rsrv_QMAmount
        {
            get { return GetQMField_QMAmount(sQM1007Rsrv, sQM1007RsrvProps, false); }
        }

        public string sQM1007Rsrv_QMAmount_rep
        {
            get { return ToMoneyString(() => sQM1007Rsrv_QMAmount); }
        }
        #endregion
        #region sQMU3Rsrv
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU3RsrvDesc))]
        public string sQMU3RsrvDesc
        {
            get { return sU3RsrvDesc; }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU3Rsrv), nameof(sSettlementU3Rsrv))]
        public decimal sQMU3Rsrv
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sU3Rsrv, sSettlementU3Rsrv); }
        }

        public string sQMU3Rsrv_rep
        {
            get { return ToMoneyString(() => sQMU3Rsrv); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU3RsrvProps), nameof(sSettlementU3RsrvProps))]
        public int sQMU3RsrvProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sU3RsrvProps, sSettlementU3RsrvProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sU3RsrvProps = t, t => sSettlementU3RsrvProps = t, value); }
        }

        [DependsOn(nameof(sQMU3RsrvProps))]
        public int sQMU3RsrvProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMU3RsrvProps); }
            set { sQMU3RsrvProps = LosConvert.GfeItemProps_UpdatePayer(sQMU3RsrvProps, value); }
        }

        [DependsOn(nameof(sQMU3RsrvProps))]
        public bool sQMU3RsrvProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMU3RsrvProps); }
            set { sQMU3RsrvProps = LosConvert.GfeItemProps_UpdateApr(sQMU3RsrvProps, value); }
        }

        [DependsOn(nameof(sQMU3RsrvProps))]
        public bool sQMU3RsrvProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMU3RsrvProps); }
            set { sQMU3RsrvProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMU3RsrvProps, value); }
        }

        [DependsOn(nameof(sQMU3RsrvProps))]
        public bool sQMU3RsrvProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMU3RsrvProps); }
            set { sQMU3RsrvProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMU3RsrvProps, value); }
        }

        [DependsOn(nameof(sQMU3RsrvProps))]
        [DevNote("sQMU3RsrvProps Show QM Warning bit.")]
        public bool sQMU3RsrvProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMU3RsrvProps); }
            set { sQMU3RsrvProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMU3RsrvProps, value); }
        }

        [DependsOn(nameof(sQMU3Rsrv), nameof(sQMU3RsrvProps))]
        public decimal sQMU3Rsrv_QMAmount
        {
            get { return GetQMField_QMAmount(sQMU3Rsrv, sQMU3RsrvProps, false); }
        }

        public string sQMU3Rsrv_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMU3Rsrv_QMAmount); }
        }
        #endregion
        #region sQMU4Rsrv
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU4RsrvDesc))]
        public string sQMU4RsrvDesc
        {
            get { return sU4RsrvDesc; }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU4Rsrv), nameof(sSettlementU4Rsrv))]
        public decimal sQMU4Rsrv
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sU4Rsrv, sSettlementU4Rsrv); }
        }

        public string sQMU4Rsrv_rep
        {
            get { return ToMoneyString(() => sQMU4Rsrv); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU4RsrvProps), nameof(sSettlementU4RsrvProps))]
        public int sQMU4RsrvProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sU4RsrvProps, sSettlementU4RsrvProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sU4RsrvProps = t, t => sSettlementU4RsrvProps = t, value); }
        }

        [DependsOn(nameof(sQMU4RsrvProps))]
        public int sQMU4RsrvProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMU4RsrvProps); }
            set { sQMU4RsrvProps = LosConvert.GfeItemProps_UpdatePayer(sQMU4RsrvProps, value); }
        }

        [DependsOn(nameof(sQMU4RsrvProps))]
        public bool sQMU4RsrvProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMU4RsrvProps); }
            set { sQMU4RsrvProps = LosConvert.GfeItemProps_UpdateApr(sQMU4RsrvProps, value); }
        }

        [DependsOn(nameof(sQMU4RsrvProps))]
        public bool sQMU4RsrvProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMU4RsrvProps); }
            set { sQMU4RsrvProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMU4RsrvProps, value); }
        }

        [DependsOn(nameof(sQMU4RsrvProps))]
        public bool sQMU4RsrvProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMU4RsrvProps); }
            set { sQMU4RsrvProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMU4RsrvProps, value); }
        }

        [DependsOn(nameof(sQMU4RsrvProps))]
        [DevNote("sQMU4RsrvProps Show QM Warning bit.")]
        public bool sQMU4RsrvProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMU4RsrvProps); }
            set { sQMU4RsrvProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMU4RsrvProps, value); }
        }

        [DependsOn(nameof(sQMU4Rsrv), nameof(sQMU4RsrvProps))]
        public decimal sQMU4Rsrv_QMAmount
        {
            get { return GetQMField_QMAmount(sQMU4Rsrv, sQMU4RsrvProps, false); }
        }

        public string sQMU4Rsrv_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMU4Rsrv_QMAmount); }
        }
        #endregion
        #region sQMEscrowF
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sEscrowF), nameof(sSettlementEscrowF))]
        public decimal sQMEscrowF
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sEscrowF, sSettlementEscrowF); }
        }

        public string sQMEscrowF_rep
        {
            get { return ToMoneyString(() => sQMEscrowF); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sEscrowFProps), nameof(sSettlementEscrowFProps))]
        public int sQMEscrowFProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sEscrowFProps, sSettlementEscrowFProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sEscrowFProps = t, t => sSettlementEscrowFProps = t, value); }
        }

        [DependsOn(nameof(sQMEscrowFProps))]
        public int sQMEscrowFProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMEscrowFProps); }
            set { sQMEscrowFProps = LosConvert.GfeItemProps_UpdatePayer(sQMEscrowFProps, value); }
        }

        [DependsOn(nameof(sQMEscrowFProps))]
        public bool sQMEscrowFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMEscrowFProps); }
            set { sQMEscrowFProps = LosConvert.GfeItemProps_UpdateApr(sQMEscrowFProps, value); }
        }

        [DependsOn(nameof(sQMEscrowFProps))]
        public bool sQMEscrowFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMEscrowFProps); }
            set { sQMEscrowFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMEscrowFProps, value); }
        }

        [DependsOn(nameof(sQMEscrowFProps))]
        public bool sQMEscrowFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMEscrowFProps); }
            set { sQMEscrowFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMEscrowFProps, value); }
        }

        [DependsOn(nameof(sQMEscrowFProps))]
        [DevNote("sQMEscrowFProps Show QM Warning bit.")]
        public bool sQMEscrowFProps_ShowQmWarning
        {
            get
            {
                return LosConvert.GfeItemProps_ShowQmWarning(sQMEscrowFProps);
            }
            
            set { sQMEscrowFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMEscrowFProps, value); }
        }

        [DependsOn(nameof(sQMEscrowF), nameof(sQMEscrowFProps))]
        public decimal sQMEscrowF_QMAmount
        {
            get { return GetQMField_QMAmount(sQMEscrowF, sQMEscrowFProps, false); }
        }

        public string sQMEscrowF_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMEscrowF_QMAmount); }
        }
        #endregion
        #region sQMOwnerTitleInsF
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sOwnerTitleInsF), nameof(sSettlementOwnerTitleInsF))]
        public decimal sQMOwnerTitleInsF
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sOwnerTitleInsF, sSettlementOwnerTitleInsF); }
        }

        public string sQMOwnerTitleInsF_rep
        {
            get { return ToMoneyString(() => sQMOwnerTitleInsF); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sOwnerTitleInsProps), nameof(sSettlementOwnerTitleInsFProps))]
        public int sQMOwnerTitleInsFProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sOwnerTitleInsProps, sSettlementOwnerTitleInsFProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sOwnerTitleInsProps = t, t => sSettlementOwnerTitleInsFProps = t, value); }
        }

        [DependsOn(nameof(sQMOwnerTitleInsFProps))]
        public int sQMOwnerTitleInsFProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMOwnerTitleInsFProps); }
            set { sQMOwnerTitleInsFProps = LosConvert.GfeItemProps_UpdatePayer(sQMOwnerTitleInsFProps, value); }
        }

        [DependsOn(nameof(sQMOwnerTitleInsFProps))]
        public bool sQMOwnerTitleInsFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMOwnerTitleInsFProps); }
            set { sQMOwnerTitleInsFProps = LosConvert.GfeItemProps_UpdateApr(sQMOwnerTitleInsFProps, value); }
        }

        [DependsOn(nameof(sQMOwnerTitleInsFProps))]
        public bool sQMOwnerTitleInsFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMOwnerTitleInsFProps); }
            set { sQMOwnerTitleInsFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMOwnerTitleInsFProps, value); }
        }

        [DependsOn(nameof(sQMOwnerTitleInsFProps))]
        public bool sQMOwnerTitleInsFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMOwnerTitleInsFProps); }
            set { sQMOwnerTitleInsFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMOwnerTitleInsFProps, value); }
        }

        [DependsOn(nameof(sQMOwnerTitleInsFProps))]
        [DevNote("sQMOwnerTitleInsFProps Show QM Warning bit.")]
        public bool sQMOwnerTitleInsFProps_ShowQmWarning
        {
            get 
            {
                return LosConvert.GfeItemProps_ShowQmWarning(sQMOwnerTitleInsFProps);
            }
            
            set { sQMOwnerTitleInsFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMOwnerTitleInsFProps, value); }
        }

        [DependsOn(nameof(sQMOwnerTitleInsF), nameof(sQMOwnerTitleInsFProps))]
        public decimal sQMOwnerTitleInsF_QMAmount
        {
            get { return GetQMField_QMAmount(sQMOwnerTitleInsF, sQMOwnerTitleInsFProps, false); }
        }

        public string sQMOwnerTitleInsF_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMOwnerTitleInsF_QMAmount); }
        }
        #endregion
        #region sQMTitleInsF
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sTitleInsF), nameof(sSettlementTitleInsF))]
        public decimal sQMTitleInsF
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sTitleInsF, sSettlementTitleInsF); }
        }

        public string sQMTitleInsF_rep
        {
            get { return ToMoneyString(() => sQMTitleInsF); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sTitleInsFProps), nameof(sSettlementTitleInsFProps))]
        public int sQMTitleInsFProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sTitleInsFProps, sSettlementTitleInsFProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sTitleInsFProps = t, t => sSettlementTitleInsFProps = t, value); }
        }

        [DependsOn(nameof(sQMTitleInsFProps))]
        public int sQMTitleInsFProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMTitleInsFProps); }
            set { sQMTitleInsFProps = LosConvert.GfeItemProps_UpdatePayer(sQMTitleInsFProps, value); }
        }

        [DependsOn(nameof(sQMTitleInsFProps))]
        public bool sQMTitleInsFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMTitleInsFProps); }
            set { sQMTitleInsFProps = LosConvert.GfeItemProps_UpdateApr(sQMTitleInsFProps, value); }
        }

        [DependsOn(nameof(sQMTitleInsFProps))]
        public bool sQMTitleInsFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMTitleInsFProps); }
            set { sQMTitleInsFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMTitleInsFProps, value); }
        }

        [DependsOn(nameof(sQMTitleInsFProps))]
        public bool sQMTitleInsFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMTitleInsFProps); }
            set { sQMTitleInsFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMTitleInsFProps, value); }
        }

        [DependsOn(nameof(sQMTitleInsFProps))]
        [DevNote("sQMTitleInsFProps Show QM Warning bit.")]
        public bool sQMTitleInsFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMTitleInsFProps); }
            set { sQMTitleInsFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMTitleInsFProps, value); }
        }

        [DependsOn(nameof(sQMTitleInsF), nameof(sQMTitleInsFProps))]
        public decimal sQMTitleInsF_QMAmount
        {
            get { return GetQMField_QMAmount(sQMTitleInsF, sQMTitleInsFProps, false); }
        }

        public string sQMTitleInsF_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMTitleInsF_QMAmount); }
        }
        #endregion
        #region sQMDocPrepF
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sDocPrepF), nameof(sSettlementDocPrepF))]
        public decimal sQMDocPrepF
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sDocPrepF, sSettlementDocPrepF); }
        }

        public string sQMDocPrepF_rep
        {
            get { return ToMoneyString(() => sQMDocPrepF); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sDocPrepFProps), nameof(sSettlementDocPrepFProps))]
        public int sQMDocPrepFProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sDocPrepFProps, sSettlementDocPrepFProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sDocPrepFProps = t, t => sSettlementDocPrepFProps = t, value); }
        }

        [DependsOn(nameof(sQMDocPrepFProps))]
        public int sQMDocPrepFProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMDocPrepFProps); }
            set { sQMDocPrepFProps = LosConvert.GfeItemProps_UpdatePayer(sQMDocPrepFProps, value); }
        }

        [DependsOn(nameof(sQMDocPrepFProps))]
        public bool sQMDocPrepFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMDocPrepFProps); }
            set { sQMDocPrepFProps = LosConvert.GfeItemProps_UpdateApr(sQMDocPrepFProps, value); }
        }

        [DependsOn(nameof(sQMDocPrepFProps))]
        public bool sQMDocPrepFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMDocPrepFProps); }
            set { sQMDocPrepFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMDocPrepFProps, value); }
        }

        [DependsOn(nameof(sQMDocPrepFProps))]
        public bool sQMDocPrepFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMDocPrepFProps); }
            set { sQMDocPrepFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMDocPrepFProps, value); }
        }

        [DependsOn(nameof(sQMDocPrepFProps))]
        [DevNote("sQMDocPrepFProps Show QM Warning bit.")]
        public bool sQMDocPrepFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMDocPrepFProps); }
            set { sQMDocPrepFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMDocPrepFProps, value); }
        }

        [DependsOn(nameof(sQMDocPrepF), nameof(sQMDocPrepFProps))]
        public decimal sQMDocPrepF_QMAmount
        {
            get { return GetQMField_QMAmount(sQMDocPrepF, sQMDocPrepFProps, false); }
        }

        public string sQMDocPrepF_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMDocPrepF_QMAmount); }
        }
        #endregion
        #region sQMNotaryF
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sNotaryF), nameof(sSettlementNotaryF))]
        public decimal sQMNotaryF
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sNotaryF, sSettlementNotaryF); }
        }

        public string sQMNotaryF_rep
        {
            get { return ToMoneyString(() => sQMNotaryF); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sNotaryFProps), nameof(sSettlementNotaryFProps))]
        public int sQMNotaryFProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sNotaryFProps, sSettlementNotaryFProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sNotaryFProps = t, t => sSettlementNotaryFProps = t, value); }
        }

        [DependsOn(nameof(sQMNotaryFProps))]
        public int sQMNotaryFProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMNotaryFProps); }
            set { sQMNotaryFProps = LosConvert.GfeItemProps_UpdatePayer(sQMNotaryFProps, value); }
        }

        [DependsOn(nameof(sQMNotaryFProps))]
        public bool sQMNotaryFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMNotaryFProps); }
            set { sQMNotaryFProps = LosConvert.GfeItemProps_UpdateApr(sQMNotaryFProps, value); }
        }

        [DependsOn(nameof(sQMNotaryFProps))]
        public bool sQMNotaryFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMNotaryFProps); }
            set { sQMNotaryFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMNotaryFProps, value); }
        }

        [DependsOn(nameof(sQMNotaryFProps))]
        public bool sQMNotaryFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMNotaryFProps); }
            set { sQMNotaryFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMNotaryFProps, value); }
        }

        [DependsOn(nameof(sQMNotaryFProps))]
        [DevNote("sQMNotaryFProps Show QM Warning bit.")]
        public bool sQMNotaryFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMNotaryFProps); }
            set { sQMNotaryFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMNotaryFProps, value); }
        }

        [DependsOn(nameof(sQMNotaryF), nameof(sQMNotaryFProps))]
        public decimal sQMNotaryF_QMAmount
        {
            get { return GetQMField_QMAmount(sQMNotaryF, sQMNotaryFProps, false); }
        }

        public string sQMNotaryF_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMNotaryF_QMAmount); }
        }
        #endregion
        #region sQMAttorneyF
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sAttorneyF), nameof(sSettlementAttorneyF))]
        public decimal sQMAttorneyF
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sAttorneyF, sSettlementAttorneyF); }
        }

        public string sQMAttorneyF_rep
        {
            get { return ToMoneyString(() => sQMAttorneyF); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sAttorneyFProps), nameof(sSettlementAttorneyFProps))]
        public int sQMAttorneyFProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sAttorneyFProps, sSettlementAttorneyFProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sAttorneyFProps = t, t => sSettlementAttorneyFProps = t, value); }
        }

        [DependsOn(nameof(sQMAttorneyFProps))]
        public int sQMAttorneyFProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMAttorneyFProps); }
            set { sQMAttorneyFProps = LosConvert.GfeItemProps_UpdatePayer(sQMAttorneyFProps, value); }
        }

        [DependsOn(nameof(sQMAttorneyFProps))]
        public bool sQMAttorneyFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMAttorneyFProps); }
            set { sQMAttorneyFProps = LosConvert.GfeItemProps_UpdateApr(sQMAttorneyFProps, value); }
        }

        [DependsOn(nameof(sQMAttorneyFProps))]
        public bool sQMAttorneyFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMAttorneyFProps); }
            set { sQMAttorneyFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMAttorneyFProps, value); }
        }

        [DependsOn(nameof(sQMAttorneyFProps))]
        public bool sQMAttorneyFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMAttorneyFProps); }
            set { sQMAttorneyFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMAttorneyFProps, value); }
        }

        [DependsOn(nameof(sQMAttorneyFProps))]
        [DevNote("sQMAttorneyFProps Show QM Warning bit.")]
        public bool sQMAttorneyFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMAttorneyFProps); }
            set { sQMAttorneyFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMAttorneyFProps, value); }
        }

        [DependsOn(nameof(sQMAttorneyF), nameof(sQMAttorneyFProps))]
        public decimal sQMAttorneyF_QMAmount
        {
            get { return GetQMField_QMAmount(sQMAttorneyF, sQMAttorneyFProps, false); }
        }

        public string sQMAttorneyF_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMAttorneyF_QMAmount); }
        }
        #endregion
        #region sQMU1Tc
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU1TcDesc), nameof(sSettlementU1TcDesc))]
        public string sQMU1TcDesc
        {
            get { return GetQMField_Desc(sQmSettlementChargesExportSource, sU1TcDesc, sSettlementU1TcDesc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU1Tc), nameof(sSettlementU1Tc))]
        public decimal sQMU1Tc
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sU1Tc, sSettlementU1Tc); }
        }

        public string sQMU1Tc_rep
        {
            get { return ToMoneyString(() => sQMU1Tc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU1TcProps), nameof(sSettlementU1TcProps))]
        public int sQMU1TcProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sU1TcProps, sSettlementU1TcProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sU1TcProps = t, t => sSettlementU1TcProps = t, value); }
        }

        [DependsOn(nameof(sQMU1TcProps))]
        public int sQMU1TcProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMU1TcProps); }
            set { sQMU1TcProps = LosConvert.GfeItemProps_UpdatePayer(sQMU1TcProps, value); }
        }

        [DependsOn(nameof(sQMU1TcProps))]
        public bool sQMU1TcProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMU1TcProps); }
            set { sQMU1TcProps = LosConvert.GfeItemProps_UpdateApr(sQMU1TcProps, value); }
        }

        [DependsOn(nameof(sQMU1TcProps))]
        public bool sQMU1TcProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMU1TcProps); }
            set { sQMU1TcProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMU1TcProps, value); }
        }

        [DependsOn(nameof(sQMU1TcProps))]
        public bool sQMU1TcProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMU1TcProps); }
            set { sQMU1TcProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMU1TcProps, value); }
        }

        [DependsOn(nameof(sQMU1TcProps))]
        [DevNote("sQMU1TcProps Show QM Warning bit.")]
        public bool sQMU1TcProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMU1TcProps); }
            set { sQMU1TcProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMU1TcProps, value); }
        }

        [DependsOn(nameof(sQMU1Tc), nameof(sQMU1TcProps))]
        public decimal sQMU1Tc_QMAmount
        {
            get { return GetQMField_QMAmount(sQMU1Tc, sQMU1TcProps, false); }
        }

        public string sQMU1Tc_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMU1Tc_QMAmount); }
        }
        #endregion
        #region sQMU2Tc
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU2TcDesc), nameof(sSettlementU2TcDesc))]
        public string sQMU2TcDesc
        {
            get { return GetQMField_Desc(sQmSettlementChargesExportSource, sU2TcDesc, sSettlementU2TcDesc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU2Tc), nameof(sSettlementU2Tc))]
        public decimal sQMU2Tc
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sU2Tc, sSettlementU2Tc); }
        }

        public string sQMU2Tc_rep
        {
            get { return ToMoneyString(() => sQMU2Tc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU2TcProps), nameof(sSettlementU2TcProps))]
        public int sQMU2TcProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sU2TcProps, sSettlementU2TcProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sU2TcProps = t, t => sSettlementU2TcProps = t, value); }
        }

        [DependsOn(nameof(sQMU2TcProps))]
        public int sQMU2TcProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMU2TcProps); }
            set { sQMU2TcProps = LosConvert.GfeItemProps_UpdatePayer(sQMU2TcProps, value); }
        }

        [DependsOn(nameof(sQMU2TcProps))]
        public bool sQMU2TcProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMU2TcProps); }
            set { sQMU2TcProps = LosConvert.GfeItemProps_UpdateApr(sQMU2TcProps, value); }
        }

        [DependsOn(nameof(sQMU2TcProps))]
        public bool sQMU2TcProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMU2TcProps); }
            set { sQMU2TcProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMU2TcProps, value); }
        }

        [DependsOn(nameof(sQMU2TcProps))]
        public bool sQMU2TcProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMU2TcProps); }
            set { sQMU2TcProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMU2TcProps, value); }
        }

        [DependsOn(nameof(sQMU2TcProps))]
        [DevNote("sQMU2TcProps Show QM Warning bit.")]
        public bool sQMU2TcProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMU2TcProps); }
            set { sQMU2TcProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMU2TcProps, value); }
        }

        [DependsOn(nameof(sQMU2Tc), nameof(sQMU2TcProps))]
        public decimal sQMU2Tc_QMAmount
        {
            get { return GetQMField_QMAmount(sQMU2Tc, sQMU2TcProps, false); }
        }

        public string sQMU2Tc_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMU2Tc_QMAmount); }
        }
        #endregion
        #region sQMU3Tc
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU3TcDesc), nameof(sSettlementU3TcDesc))]
        public string sQMU3TcDesc
        {
            get { return GetQMField_Desc(sQmSettlementChargesExportSource, sU3TcDesc, sSettlementU3TcDesc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU3Tc), nameof(sSettlementU3Tc))]
        public decimal sQMU3Tc
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sU3Tc, sSettlementU3Tc); }
        }

        public string sQMU3Tc_rep
        {
            get { return ToMoneyString(() => sQMU3Tc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU3TcProps), nameof(sSettlementU3TcProps))]
        public int sQMU3TcProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sU3TcProps, sSettlementU3TcProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sU3TcProps = t, t => sSettlementU3TcProps = t, value); }
        }

        [DependsOn(nameof(sQMU3TcProps))]
        public int sQMU3TcProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMU3TcProps); }
            set { sQMU3TcProps = LosConvert.GfeItemProps_UpdatePayer(sQMU3TcProps, value); }
        }

        [DependsOn(nameof(sQMU3TcProps))]
        public bool sQMU3TcProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMU3TcProps); }
            set { sQMU3TcProps = LosConvert.GfeItemProps_UpdateApr(sQMU3TcProps, value); }
        }

        [DependsOn(nameof(sQMU3TcProps))]
        public bool sQMU3TcProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMU3TcProps); }
            set { sQMU3TcProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMU3TcProps, value); }
        }

        [DependsOn(nameof(sQMU3TcProps))]
        public bool sQMU3TcProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMU3TcProps); }
            set { sQMU3TcProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMU3TcProps, value); }
        }

        [DependsOn(nameof(sQMU3TcProps))]
        [DevNote("sQMU3TcProps Show QM Warning bit.")]
        public bool sQMU3TcProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMU3TcProps); }
            set { sQMU3TcProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMU3TcProps, value); }
        }

        [DependsOn(nameof(sQMU3Tc), nameof(sQMU3TcProps))]
        public decimal sQMU3Tc_QMAmount
        {
            get { return GetQMField_QMAmount(sQMU3Tc, sQMU3TcProps, false); }
        }

        public string sQMU3Tc_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMU3Tc_QMAmount); }
        }
        #endregion
        #region sQMU4Tc
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU4TcDesc), nameof(sSettlementU4TcDesc))]
        public string sQMU4TcDesc
        {
            get { return GetQMField_Desc(sQmSettlementChargesExportSource, sU4TcDesc, sSettlementU4TcDesc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU4Tc), nameof(sSettlementU4Tc))]
        public decimal sQMU4Tc
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sU4Tc, sSettlementU4Tc); }
        }

        public string sQMU4Tc_rep
        {
            get { return ToMoneyString(() => sQMU4Tc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU4TcProps), nameof(sSettlementU4TcProps))]
        public int sQMU4TcProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sU4TcProps, sSettlementU4TcProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sU4TcProps = t, t => sSettlementU4TcProps = t, value); }
        }

        [DependsOn(nameof(sQMU4TcProps))]
        public int sQMU4TcProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMU4TcProps); }
            set { sQMU4TcProps = LosConvert.GfeItemProps_UpdatePayer(sQMU4TcProps, value); }
        }

        [DependsOn(nameof(sQMU4TcProps))]
        public bool sQMU4TcProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMU4TcProps); }
            set { sQMU4TcProps = LosConvert.GfeItemProps_UpdateApr(sQMU4TcProps, value); }
        }

        [DependsOn(nameof(sQMU4TcProps))]
        public bool sQMU4TcProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMU4TcProps); }
            set { sQMU4TcProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMU4TcProps, value); }
        }

        [DependsOn(nameof(sQMU4TcProps))]
        public bool sQMU4TcProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMU4TcProps); }
            set { sQMU4TcProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMU4TcProps, value); }
        }

        [DependsOn(nameof(sQMU4TcProps))]
        [DevNote("sQMU4TcProps Show QM Warning bit.")]
        public bool sQMU4TcProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMU4TcProps); }
            set { sQMU4TcProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMU4TcProps, value); }
        }

        [DependsOn(nameof(sQMU4Tc), nameof(sQMU4TcProps))]
        public decimal sQMU4Tc_QMAmount
        {
            get { return GetQMField_QMAmount(sQMU4Tc, sQMU4TcProps, false); }
        }

        public string sQMU4Tc_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMU4Tc_QMAmount); }
        }
        #endregion
        #region sQMRecF
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sRecF), nameof(sSettlementRecF))]
        public decimal sQMRecF
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sRecF, sSettlementRecF); }
        }

        public string sQMRecF_rep
        {
            get { return ToMoneyString(() => sQMRecF); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sRecFProps), nameof(sSettlementRecFProps))]
        public int sQMRecFProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sRecFProps, sSettlementRecFProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sRecFProps = t, t => sSettlementRecFProps = t, value); }
        }

        [DependsOn(nameof(sQMRecFProps))]
        public int sQMRecFProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMRecFProps); }
            set { sQMRecFProps = LosConvert.GfeItemProps_UpdatePayer(sQMRecFProps, value); }
        }

        [DependsOn(nameof(sQMRecFProps))]
        public bool sQMRecFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMRecFProps); }
            set { sQMRecFProps = LosConvert.GfeItemProps_UpdateApr(sQMRecFProps, value); }
        }

        [DependsOn(nameof(sQMRecFProps))]
        public bool sQMRecFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMRecFProps); }
            set { sQMRecFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMRecFProps, value); }
        }

        [DependsOn(nameof(sQMRecFProps))]
        public bool sQMRecFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMRecFProps); }
            set { sQMRecFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMRecFProps, value); }
        }

        [DependsOn(nameof(sQMRecFProps))]
        [DevNote("sQMRecFProps Show QM Warning bit.")]
        public bool sQMRecFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMRecFProps); }
            set { sQMRecFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMRecFProps, value); }
        }

        [DependsOn(nameof(sQMRecF), nameof(sQMRecFProps))]
        public decimal sQMRecF_QMAmount
        {
            get { return GetQMField_QMAmount(sQMRecF, sQMRecFProps, true); }
        }

        public string sQMRecF_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMRecF_QMAmount); }
        }
        #endregion
        #region sQMCountyRtc
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sCountyRtc), nameof(sSettlementCountyRtc))]
        public decimal sQMCountyRtc
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sCountyRtc, sSettlementCountyRtc); }
        }

        public string sQMCountyRtc_rep
        {
            get { return ToMoneyString(() => sQMCountyRtc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sCountyRtcProps), nameof(sSettlementCountyRtcProps))]
        public int sQMCountyRtcProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sCountyRtcProps, sSettlementCountyRtcProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sCountyRtcProps = t, t => sSettlementCountyRtcProps = t, value); }
        }

        [DependsOn(nameof(sQMCountyRtcProps))]
        public int sQMCountyRtcProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMCountyRtcProps); }
            set { sQMCountyRtcProps = LosConvert.GfeItemProps_UpdatePayer(sQMCountyRtcProps, value); }
        }

        [DependsOn(nameof(sQMCountyRtcProps))]
        public bool sQMCountyRtcProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMCountyRtcProps); }
            set { sQMCountyRtcProps = LosConvert.GfeItemProps_UpdateApr(sQMCountyRtcProps, value); }
        }

        [DependsOn(nameof(sQMCountyRtcProps))]
        public bool sQMCountyRtcProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMCountyRtcProps); }
            set { sQMCountyRtcProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMCountyRtcProps, value); }
        }

        [DependsOn(nameof(sQMCountyRtcProps))]
        public bool sQMCountyRtcProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMCountyRtcProps); }
            set { sQMCountyRtcProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMCountyRtcProps, value); }
        }

        [DependsOn(nameof(sQMCountyRtcProps))]
        [DevNote("sQMCountyRtcProps Show QM Warning bit.")]
        public bool sQMCountyRtcProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMCountyRtcProps); }
            set { sQMCountyRtcProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMCountyRtcProps, value); }
        }

        [DependsOn(nameof(sQMCountyRtc), nameof(sQMCountyRtcProps))]
        public decimal sQMCountyRtc_QMAmount
        {
            get { return GetQMField_QMAmount(sQMCountyRtc, sQMCountyRtcProps, true); }
        }

        public string sQMCountyRtc_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMCountyRtc_QMAmount); }
        }
        #endregion
        #region sQMStateRtc
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sStateRtc), nameof(sSettlementStateRtc))]
        public decimal sQMStateRtc
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sStateRtc, sSettlementStateRtc); }
        }

        public string sQMStateRtc_rep
        {
            get { return ToMoneyString(() => sQMStateRtc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sStateRtcProps), nameof(sSettlementStateRtcProps))]
        public int sQMStateRtcProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sStateRtcProps, sSettlementStateRtcProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sStateRtcProps = t, t => sSettlementStateRtcProps = t, value); }
        }

        [DependsOn(nameof(sQMStateRtcProps))]
        public int sQMStateRtcProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMStateRtcProps); }
            set { sQMStateRtcProps = LosConvert.GfeItemProps_UpdatePayer(sQMStateRtcProps, value); }
        }

        [DependsOn(nameof(sQMStateRtcProps))]
        public bool sQMStateRtcProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMStateRtcProps); }
            set { sQMStateRtcProps = LosConvert.GfeItemProps_UpdateApr(sQMStateRtcProps, value); }
        }

        [DependsOn(nameof(sQMStateRtcProps))]
        public bool sQMStateRtcProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMStateRtcProps); }
            set { sQMStateRtcProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMStateRtcProps, value); }
        }

        [DependsOn(nameof(sQMStateRtcProps))]
        public bool sQMStateRtcProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMStateRtcProps); }
            set { sQMStateRtcProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMStateRtcProps, value); }
        }

        [DependsOn(nameof(sQMStateRtcProps))]
        [DevNote("sQMStateRtcProps Show QM Warning bit.")]
        public bool sQMStateRtcProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMStateRtcProps); }
            set { sQMStateRtcProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMStateRtcProps, value); }
        }

        [DependsOn(nameof(sQMStateRtc), nameof(sQMStateRtcProps))]
        public decimal sQMStateRtc_QMAmount
        {
            get { return GetQMField_QMAmount(sQMStateRtc, sQMStateRtcProps, true); }
        }

        public string sQMStateRtc_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMStateRtc_QMAmount); }
        }
        #endregion
        #region sQMU1GovRtc
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU1GovRtcDesc), nameof(sSettlementU1GovRtcDesc))]
        public string sQMU1GovRtcDesc
        {
            get { return GetQMField_Desc(sQmSettlementChargesExportSource, sU1GovRtcDesc, sSettlementU1GovRtcDesc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU1GovRtc), nameof(sSettlementU1GovRtc))]
        public decimal sQMU1GovRtc
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sU1GovRtc, sSettlementU1GovRtc); }
        }

        public string sQMU1GovRtc_rep
        {
            get { return ToMoneyString(() => sQMU1GovRtc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU1GovRtcProps), nameof(sSettlementU1GovRtcProps))]
        public int sQMU1GovRtcProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sU1GovRtcProps, sSettlementU1GovRtcProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sU1GovRtcProps = t, t => sSettlementU1GovRtcProps = t, value); }
        }

        [DependsOn(nameof(sQMU1GovRtcProps))]
        public int sQMU1GovRtcProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMU1GovRtcProps); }
            set { sQMU1GovRtcProps = LosConvert.GfeItemProps_UpdatePayer(sQMU1GovRtcProps, value); }
        }

        [DependsOn(nameof(sQMU1GovRtcProps))]
        public bool sQMU1GovRtcProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMU1GovRtcProps); }
            set { sQMU1GovRtcProps = LosConvert.GfeItemProps_UpdateApr(sQMU1GovRtcProps, value); }
        }

        [DependsOn(nameof(sQMU1GovRtcProps))]
        public bool sQMU1GovRtcProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMU1GovRtcProps); }
            set { sQMU1GovRtcProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMU1GovRtcProps, value); }
        }

        [DependsOn(nameof(sQMU1GovRtcProps))]
        public bool sQMU1GovRtcProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMU1GovRtcProps); }
            set { sQMU1GovRtcProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMU1GovRtcProps, value); }
        }

        [DependsOn(nameof(sQMU1GovRtcProps))]
        [DevNote("sQMU1GovRtcProps Show QM Warning bit.")]
        public bool sQMU1GovRtcProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMU1GovRtcProps); }
            set { sQMU1GovRtcProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMU1GovRtcProps, value); }
        }

        [DependsOn(nameof(sQMU1GovRtc), nameof(sQMU1GovRtcProps))]
        public decimal sQMU1GovRtc_QMAmount
        {
            get { return GetQMField_QMAmount(sQMU1GovRtc, sQMU1GovRtcProps, true); }
        }

        public string sQMU1GovRtc_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMU1GovRtc_QMAmount); }
        }
        #endregion
        #region sQMU2GovRtc
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU2GovRtcDesc), nameof(sSettlementU2GovRtcDesc))]
        public string sQMU2GovRtcDesc
        {
            get { return GetQMField_Desc(sQmSettlementChargesExportSource, sU2GovRtcDesc, sSettlementU2GovRtcDesc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU2GovRtc), nameof(sSettlementU2GovRtc))]
        public decimal sQMU2GovRtc
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sU2GovRtc, sSettlementU2GovRtc); }
        }

        public string sQMU2GovRtc_rep
        {
            get { return ToMoneyString(() => sQMU2GovRtc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU2GovRtcProps), nameof(sSettlementU2GovRtcProps))]
        public int sQMU2GovRtcProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sU2GovRtcProps, sSettlementU2GovRtcProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sU2GovRtcProps = t, t => sSettlementU2GovRtcProps = t, value); }
        }

        [DependsOn(nameof(sQMU2GovRtcProps))]
        public int sQMU2GovRtcProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMU2GovRtcProps); }
            set { sQMU2GovRtcProps = LosConvert.GfeItemProps_UpdatePayer(sQMU2GovRtcProps, value); }
        }

        [DependsOn(nameof(sQMU2GovRtcProps))]
        public bool sQMU2GovRtcProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMU2GovRtcProps); }
            set { sQMU2GovRtcProps = LosConvert.GfeItemProps_UpdateApr(sQMU2GovRtcProps, value); }
        }

        [DependsOn(nameof(sQMU2GovRtcProps))]
        public bool sQMU2GovRtcProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMU2GovRtcProps); }
            set { sQMU2GovRtcProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMU2GovRtcProps, value); }
        }

        [DependsOn(nameof(sQMU2GovRtcProps))]
        public bool sQMU2GovRtcProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMU2GovRtcProps); }
            set { sQMU2GovRtcProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMU2GovRtcProps, value); }
        }

        [DependsOn(nameof(sQMU2GovRtcProps))]
        [DevNote("sQMU2GovRtcProps Show QM Warning bit.")]
        public bool sQMU2GovRtcProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMU2GovRtcProps); }
            set { sQMU2GovRtcProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMU2GovRtcProps, value); }
        }

        [DependsOn(nameof(sQMU2GovRtc), nameof(sQMU2GovRtcProps))]
        public decimal sQMU2GovRtc_QMAmount
        {
            get { return GetQMField_QMAmount(sQMU2GovRtc, sQMU2GovRtcProps, true); }
        }

        public string sQMU2GovRtc_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMU2GovRtc_QMAmount); }
        }
        #endregion
        #region sQMU3GovRtc
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU3GovRtcDesc), nameof(sSettlementU3GovRtcDesc))]
        public string sQMU3GovRtcDesc
        {
            get { return GetQMField_Desc(sQmSettlementChargesExportSource, sU3GovRtcDesc, sSettlementU3GovRtcDesc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU3GovRtc), nameof(sSettlementU3GovRtc))]
        public decimal sQMU3GovRtc
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sU3GovRtc, sSettlementU3GovRtc); }
        }

        public string sQMU3GovRtc_rep
        {
            get { return ToMoneyString(() => sQMU3GovRtc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU3GovRtcProps), nameof(sSettlementU3GovRtcProps))]
        public int sQMU3GovRtcProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sU3GovRtcProps, sSettlementU3GovRtcProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sU3GovRtcProps = t, t => sSettlementU3GovRtcProps = t, value); }
        }

        [DependsOn(nameof(sQMU3GovRtcProps))]
        public int sQMU3GovRtcProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMU3GovRtcProps); }
            set { sQMU3GovRtcProps = LosConvert.GfeItemProps_UpdatePayer(sQMU3GovRtcProps, value); }
        }

        [DependsOn(nameof(sQMU3GovRtcProps))]
        public bool sQMU3GovRtcProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMU3GovRtcProps); }
            set { sQMU3GovRtcProps = LosConvert.GfeItemProps_UpdateApr(sQMU3GovRtcProps, value); }
        }

        [DependsOn(nameof(sQMU3GovRtcProps))]
        public bool sQMU3GovRtcProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMU3GovRtcProps); }
            set { sQMU3GovRtcProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMU3GovRtcProps, value); }
        }

        [DependsOn(nameof(sQMU3GovRtcProps))]
        public bool sQMU3GovRtcProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMU3GovRtcProps); }
            set { sQMU3GovRtcProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMU3GovRtcProps, value); }
        }

        [DependsOn(nameof(sQMU3GovRtcProps))]
        [DevNote("sQMU3GovRtcProps Show QM Warning bit.")]
        public bool sQMU3GovRtcProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMU3GovRtcProps); }
            set { sQMU3GovRtcProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMU3GovRtcProps, value); }
        }

        [DependsOn(nameof(sQMU3GovRtc), nameof(sQMU3GovRtcProps))]
        public decimal sQMU3GovRtc_QMAmount
        {
            get { return GetQMField_QMAmount(sQMU3GovRtc, sQMU3GovRtcProps, true); }
        }

        public string sQMU3GovRtc_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMU3GovRtc_QMAmount); }
        }
        #endregion
        #region sQMPestInspectF
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sPestInspectF), nameof(sSettlementPestInspectF))]
        public decimal sQMPestInspectF
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sPestInspectF, sSettlementPestInspectF); }
        }

        public string sQMPestInspectF_rep
        {
            get { return ToMoneyString(() => sQMPestInspectF); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sPestInspectFProps), nameof(sSettlementPestInspectFProps))]
        public int sQMPestInspectFProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sPestInspectFProps, sSettlementPestInspectFProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sPestInspectFProps = t, t => sSettlementPestInspectFProps = t, value); }
        }

        [DependsOn(nameof(sQMPestInspectFProps))]
        public int sQMPestInspectFProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMPestInspectFProps); }
            set { sQMPestInspectFProps = LosConvert.GfeItemProps_UpdatePayer(sQMPestInspectFProps, value); }
        }

        [DependsOn(nameof(sQMPestInspectFProps))]
        public bool sQMPestInspectFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMPestInspectFProps); }
            set { sQMPestInspectFProps = LosConvert.GfeItemProps_UpdateApr(sQMPestInspectFProps, value); }
        }

        [DependsOn(nameof(sQMPestInspectFProps))]
        public bool sQMPestInspectFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMPestInspectFProps); }
            set { sQMPestInspectFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMPestInspectFProps, value); }
        }

        [DependsOn(nameof(sQMPestInspectFProps))]
        public bool sQMPestInspectFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMPestInspectFProps); }
            set { sQMPestInspectFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMPestInspectFProps, value); }
        }

        [DependsOn(nameof(sQMPestInspectFProps))]
        [DevNote("sQMPestInspectFProps Show QM Warning bit.")]
        public bool sQMPestInspectFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMPestInspectFProps); }
            set { sQMPestInspectFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMPestInspectFProps, value); }
        }

        [DependsOn(nameof(sQMPestInspectF), nameof(sQMPestInspectFProps))]
        public decimal sQMPestInspectF_QMAmount
        {
            get { return GetQMField_QMAmount(sQMPestInspectF, sQMPestInspectFProps, false); }
        }

        public string sQMPestInspectF_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMPestInspectF_QMAmount); }
        }
        #endregion
        #region sQMU1Sc
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU1ScDesc), nameof(sSettlementU1ScDesc))]
        public string sQMU1ScDesc
        {
            get { return GetQMField_Desc(sQmSettlementChargesExportSource, sU1ScDesc, sSettlementU1ScDesc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU1Sc), nameof(sSettlementU1Sc))]
        public decimal sQMU1Sc
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sU1Sc, sSettlementU1Sc); }
        }

        public string sQMU1Sc_rep
        {
            get { return ToMoneyString(() => sQMU1Sc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU1ScProps), nameof(sSettlementU1ScProps))]
        public int sQMU1ScProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sU1ScProps, sSettlementU1ScProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sU1ScProps = t, t => sSettlementU1ScProps = t, value); }
        }

        [DependsOn(nameof(sQMU1ScProps))]
        public int sQMU1ScProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMU1ScProps); }
            set { sQMU1ScProps = LosConvert.GfeItemProps_UpdatePayer(sQMU1ScProps, value); }
        }

        [DependsOn(nameof(sQMU1ScProps))]
        public bool sQMU1ScProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMU1ScProps); }
            set { sQMU1ScProps = LosConvert.GfeItemProps_UpdateApr(sQMU1ScProps, value); }
        }

        [DependsOn(nameof(sQMU1ScProps))]
        public bool sQMU1ScProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMU1ScProps); }
            set { sQMU1ScProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMU1ScProps, value); }
        }

        [DependsOn(nameof(sQMU1ScProps))]
        public bool sQMU1ScProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMU1ScProps); }
            set { sQMU1ScProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMU1ScProps, value); }
        }

        [DependsOn(nameof(sQMU1ScProps))]
        [DevNote("sQMU1ScProps Show QM Warning bit.")]
        public bool sQMU1ScProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMU1ScProps); }
            set { sQMU1ScProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMU1ScProps, value); }
        }

        [DependsOn(nameof(sQMU1Sc), nameof(sQMU1ScProps))]
        public decimal sQMU1Sc_QMAmount
        {
            get { return GetQMField_QMAmount(sQMU1Sc, sQMU1ScProps, false); }
        }

        public string sQMU1Sc_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMU1Sc_QMAmount); }
        }
        #endregion
        #region sQMU2Sc
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU2ScDesc), nameof(sSettlementU2ScDesc))]
        public string sQMU2ScDesc
        {
            get { return GetQMField_Desc(sQmSettlementChargesExportSource, sU2ScDesc, sSettlementU2ScDesc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU2Sc), nameof(sSettlementU2Sc))]
        public decimal sQMU2Sc
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sU2Sc, sSettlementU2Sc); }
        }

        public string sQMU2Sc_rep
        {
            get { return ToMoneyString(() => sQMU2Sc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU2ScProps), nameof(sSettlementU2ScProps))]
        public int sQMU2ScProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sU2ScProps, sSettlementU2ScProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sU2ScProps = t, t => sSettlementU2ScProps = t, value); }
        }

        [DependsOn(nameof(sQMU2ScProps))]
        public int sQMU2ScProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMU2ScProps); }
            set { sQMU2ScProps = LosConvert.GfeItemProps_UpdatePayer(sQMU2ScProps, value); }
        }

        [DependsOn(nameof(sQMU2ScProps))]
        public bool sQMU2ScProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMU2ScProps); }
            set { sQMU2ScProps = LosConvert.GfeItemProps_UpdateApr(sQMU2ScProps, value); }
        }

        [DependsOn(nameof(sQMU2ScProps))]
        public bool sQMU2ScProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMU2ScProps); }
            set { sQMU2ScProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMU2ScProps, value); }
        }

        [DependsOn(nameof(sQMU2ScProps))]
        public bool sQMU2ScProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMU2ScProps); }
            set { sQMU2ScProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMU2ScProps, value); }
        }

        [DependsOn(nameof(sQMU2ScProps))]
        [DevNote("sQMU2ScProps Show QM Warning bit.")]
        public bool sQMU2ScProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMU2ScProps); }
            set { sQMU2ScProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMU2ScProps, value); }
        }

        [DependsOn(nameof(sQMU2Sc), nameof(sQMU2ScProps))]
        public decimal sQMU2Sc_QMAmount
        {
            get { return GetQMField_QMAmount(sQMU2Sc, sQMU2ScProps, false); }
        }

        public string sQMU2Sc_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMU2Sc_QMAmount); }
        }
        #endregion
        #region sQMU3Sc
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU3ScDesc), nameof(sSettlementU3ScDesc))]
        public string sQMU3ScDesc
        {
            get { return GetQMField_Desc(sQmSettlementChargesExportSource, sU3ScDesc, sSettlementU3ScDesc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU3Sc), nameof(sSettlementU3Sc))]
        public decimal sQMU3Sc
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sU3Sc, sSettlementU3Sc); }
        }

        public string sQMU3Sc_rep
        {
            get { return ToMoneyString(() => sQMU3Sc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU3ScProps), nameof(sSettlementU3ScProps))]
        public int sQMU3ScProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sU3ScProps, sSettlementU3ScProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sU3ScProps = t, t => sSettlementU3ScProps = t, value); }
        }

        [DependsOn(nameof(sQMU3ScProps))]
        public int sQMU3ScProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMU3ScProps); }
            set { sQMU3ScProps = LosConvert.GfeItemProps_UpdatePayer(sQMU3ScProps, value); }
        }

        [DependsOn(nameof(sQMU3ScProps))]
        public bool sQMU3ScProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMU3ScProps); }
            set { sQMU3ScProps = LosConvert.GfeItemProps_UpdateApr(sQMU3ScProps, value); }
        }

        [DependsOn(nameof(sQMU3ScProps))]
        public bool sQMU3ScProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMU3ScProps); }
            set { sQMU3ScProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMU3ScProps, value); }
        }

        [DependsOn(nameof(sQMU3ScProps))]
        public bool sQMU3ScProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMU3ScProps); }
            set { sQMU3ScProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMU3ScProps, value); }
        }

        [DependsOn(nameof(sQMU3ScProps))]
        [DevNote("sQMU3ScProps Show QM Warning bit.")]
        public bool sQMU3ScProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMU3ScProps); }
            set { sQMU3ScProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMU3ScProps, value); }
        }

        [DependsOn(nameof(sQMU3Sc), nameof(sQMU3ScProps))]
        public decimal sQMU3Sc_QMAmount
        {
            get { return GetQMField_QMAmount(sQMU3Sc, sQMU3ScProps, false); }
        }

        public string sQMU3Sc_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMU3Sc_QMAmount); }
        }
        #endregion
        #region sQMU4Sc
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU4ScDesc), nameof(sSettlementU4ScDesc))]
        public string sQMU4ScDesc
        {
            get { return GetQMField_Desc(sQmSettlementChargesExportSource, sU4ScDesc, sSettlementU4ScDesc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU4Sc), nameof(sSettlementU4Sc))]
        public decimal sQMU4Sc
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sU4Sc, sSettlementU4Sc); }
        }

        public string sQMU4Sc_rep
        {
            get { return ToMoneyString(() => sQMU4Sc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU4ScProps), nameof(sSettlementU4ScProps))]
        public int sQMU4ScProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sU4ScProps, sSettlementU4ScProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sU4ScProps = t, t => sSettlementU4ScProps = t, value); }
        }

        [DependsOn(nameof(sQMU4ScProps))]
        public int sQMU4ScProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMU4ScProps); }
            set { sQMU4ScProps = LosConvert.GfeItemProps_UpdatePayer(sQMU4ScProps, value); }
        }

        [DependsOn(nameof(sQMU4ScProps))]
        public bool sQMU4ScProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMU4ScProps); }
            set { sQMU4ScProps = LosConvert.GfeItemProps_UpdateApr(sQMU4ScProps, value); }
        }

        [DependsOn(nameof(sQMU4ScProps))]
        public bool sQMU4ScProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMU4ScProps); }
            set { sQMU4ScProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMU4ScProps, value); }
        }

        [DependsOn(nameof(sQMU4ScProps))]
        public bool sQMU4ScProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMU4ScProps); }
            set { sQMU4ScProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMU4ScProps, value); }
        }

        [DependsOn(nameof(sQMU4ScProps))]
        [DevNote("sQMU4ScProps Show QM Warning bit.")]
        public bool sQMU4ScProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMU4ScProps); }
            set { sQMU4ScProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMU4ScProps, value); }
        }

        [DependsOn(nameof(sQMU4Sc), nameof(sQMU4ScProps))]
        public decimal sQMU4Sc_QMAmount
        {
            get { return GetQMField_QMAmount(sQMU4Sc, sQMU4ScProps, false); }
        }

        public string sQMU4Sc_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMU4Sc_QMAmount); }
        }
        #endregion
        #region sQMU5Sc
        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU5ScDesc), nameof(sSettlementU5ScDesc))]
        public string sQMU5ScDesc
        {
            get { return GetQMField_Desc(sQmSettlementChargesExportSource, sU5ScDesc, sSettlementU5ScDesc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU5Sc), nameof(sSettlementU5Sc))]
        public decimal sQMU5Sc
        {
            get { return GetQMField(sQmSettlementChargesExportSource, sU5Sc, sSettlementU5Sc); }
        }

        public string sQMU5Sc_rep
        {
            get { return ToMoneyString(() => sQMU5Sc); }
        }

        [DependsOn(nameof(sQmSettlementChargesExportSource), nameof(sU5ScProps), nameof(sSettlementU5ScProps))]
        public int sQMU5ScProps
        {
            get { return GetQMField_Props(sQmSettlementChargesExportSource, sU5ScProps, sSettlementU5ScProps); }
            set { SetQMField_Props(sQmSettlementChargesExportSource, t => sU5ScProps = t, t => sSettlementU5ScProps = t, value); }
        }

        [DependsOn(nameof(sQMU5ScProps))]
        public int sQMU5ScProps_Payer
        {
            get { return LosConvert.GfeItemProps_Payer(sQMU5ScProps); }
            set { sQMU5ScProps = LosConvert.GfeItemProps_UpdatePayer(sQMU5ScProps, value); }
        }

        [DependsOn(nameof(sQMU5ScProps))]
        public bool sQMU5ScProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sQMU5ScProps); }
            set { sQMU5ScProps = LosConvert.GfeItemProps_UpdateApr(sQMU5ScProps, value); }
        }

        [DependsOn(nameof(sQMU5ScProps))]
        public bool sQMU5ScProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sQMU5ScProps); }
            set { sQMU5ScProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sQMU5ScProps, value); }
        }

        [DependsOn(nameof(sQMU5ScProps))]
        public bool sQMU5ScProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sQMU5ScProps); }
            set { sQMU5ScProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sQMU5ScProps, value); }
        }

        [DependsOn(nameof(sQMU5ScProps))]
        [DevNote("sQMU5ScProps Show QM Warning bit.")]
        public bool sQMU5ScProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sQMU5ScProps); }
            set { sQMU5ScProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sQMU5ScProps, value); }
        }

        [DependsOn(nameof(sQMU5Sc), nameof(sQMU5ScProps))]
        public decimal sQMU5Sc_QMAmount
        {
            get { return GetQMField_QMAmount(sQMU5Sc, sQMU5ScProps, false); }
        }

        public string sQMU5Sc_QMAmount_rep
        {
            get { return ToMoneyString(() => sQMU5Sc_QMAmount); }
        }
        #endregion
        #endregion
    }
}