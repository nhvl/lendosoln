﻿namespace DataAccess
{
    /// <summary>
    /// Simple class to contain the updated data of a first lien after syncing
    /// with a second lien.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Mimic loan property names.")]
    public class UpdatedFirstLienInfo
    {
        /// <summary>
        /// Gets a value indicating whether the second lien is a line of credit within its draw period.
        /// </summary>
        /// <value>A value indicating whether the second lien is a line of credit within its draw period.</value>
        public bool sIsOFinCreditLineInDrawPeriod { get; internal set; }

        /// <summary>
        /// Gets a value indicating whether the second lien is interest only.
        /// </summary>
        /// <value>A value indicating whether the second lien is interest only.</value>
        public bool sIsIOnlyForSubFin { get; internal set; }

        /// <summary>
        /// Gets the original balance / line amount of the second lien.
        /// </summary>
        /// <value>The original balance / line amount of the second lien.</value>
        public string sSubFin { get; internal set; }

        /// <summary>
        /// Gets the current balance of the second lien.
        /// </summary>
        /// <value>The current balance of the second lien.</value>
        public string sConcurSubFin { get; internal set; }

        /// <summary>
        /// Gets the interest rate of the second lien.
        /// </summary>
        /// <value>The interest rate of the second lien.</value>
        public string sSubFinIR { get; internal set; }

        /// <summary>
        /// Gets the term of the second lien.
        /// </summary>
        /// <value>The term of the second lien.</value>
        public string sSubFinTerm { get; internal set; }

        /// <summary>
        /// Gets the payment base of the second lien.
        /// </summary>
        /// <value>The payment base of the second lien.</value>
        public string sSubFinMb { get; internal set; }

        /// <summary>
        /// Gets the second lien monthly payment.
        /// </summary>
        /// <value>The second lien monthly payment.</value>
        public string sSubFinPmt { get; internal set; }

        /// <summary>
        /// Gets a value indicating whether the second lien monthly payment is locked.
        /// </summary>
        /// <value>A value indicating whether the second lien monthly payment is locked.</value>
        public bool sSubFinPmtLckd { get; internal set; }

        /// <summary>
        /// Gets the financing method for the second lien.
        /// </summary>
        /// <value>The financing method for the second lien.</value>
        public E_sFinMethT sOtherLFinMethT { get; internal set; }
    }
}
