﻿namespace DataAccess
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.UI.WebControls;
    using MeridianLink.CommonControls;

    /// <summary>
    /// Provides methods to build rate lock history table markup.
    /// </summary>
    public static class RateLockHistoryTable
    {
        /// <summary>
        /// Builds an HTML table and returns the markup.
        /// </summary>
        /// <param name="xml">The investor rate lock history XML.</param>
        /// <returns>The HTML string for the table.</returns>
        public static string BuildInvestorRateLockHistoryTable(string xml)
        {
            Table tb = new Table();
            tb.CellPadding = 2;
            tb.CellSpacing = 0;
            tb.CssClass = "RateLockTable";

            var columnHeaders = new Literal[] 
            {
                new EncodedLiteral() { Text = "Date" },
                new EncodedLiteral() { Text = "Action" },
                new EncodedLiteral() { Text = "By" },
                new PassthroughLiteral() { Text = "Lock<br/>Expiration" },
                new EncodedLiteral() { Text = "Rate" },
                new EncodedLiteral() { Text = "Price" },
                new EncodedLiteral() { Text = "Fee" },
                new EncodedLiteral() { Text = "Margin" },
                new PassthroughLiteral() { Text = "Teaser<br/>Rate" },
                new PassthroughLiteral() { Text = "Lock<br/>Fee" },
                new EncodedLiteral() { Text = "Investor" },
                new PassthroughLiteral() { Text = "Program<br/>Name" },
                new EncodedLiteral() { Text = "Reason" },
                new PassthroughLiteral() { Text = "&nbsp;" }
            };

            TableRow row = GenerateRow(columnHeaders);
            row.CssClass = "RateLockHeader";
            tb.Rows.Add(row);

            var rateLockHistoryItems = InvestorRateLockHistoryItem.GetSortedRateHistory(xml);

            bool isOdd = true;

            string[] classes  = new string[] 
            {
                "date",
                "action",
                "by",
                "lockExpiration",
                "rate",
                "price",
                "fee",
                "margin",
                "trate",
                "lockfee",
                "investor",
                "programName",
                "reason",
                "misc"
            };

            for (var i = 0; i < row.Cells.Count; i++)
            {
                row.Cells[i].CssClass  = classes[i];
            }

            foreach (InvestorRateLockHistoryItem o in rateLockHistoryItems)
            {
                string link = GenerateLink(o);

                var data = new Literal[] 
                { 
                    new EncodedLiteral() { Text = o.EventDate },
                    new EncodedLiteral() { Text = o.Action },
                    new EncodedLiteral() { Text = o.DoneBy },
                    new EncodedLiteral() { Text = o.sInvestorLockRLckExpiredD },
                    new EncodedLiteral() { Text = o.sInvestorLockNoteIR },
                    new EncodedLiteral() { Text = o.sInvestorLockBrokComp1PcPrice },
                    new EncodedLiteral() { Text = o.sInvestorLockBrokComp1Pc },
                    new EncodedLiteral() { Text = o.sInvestorLockRAdjMarginR },
                    new EncodedLiteral() { Text = o.sInvestorLockOptionArmTeaserR },
                    new EncodedLiteral() { Text = o.sInvestorLockLockFee },
                    new EncodedLiteral() { Text = o.sInvestorLockLpInvestorNm },
                    new EncodedLiteral() { Text = o.sInvestorLockLpTemplateNm },
                    new PassthroughLiteral() { Text = LendersOffice.Common.Utilities.SafeHtmlString(o.sRLckBreakReasonDesc) },
                    new PassthroughLiteral() { Text = link }
                };

                row = GenerateRow(data);
                row.CssClass = isOdd ? "RateLockItem" : "RateLockAlternatingItem";

                isOdd = !isOdd;
                tb.Rows.Add(row);
            }

            return RenderControl(tb);
        }

        /// <summary>
        /// Builds an HTML table and returns the markup.
        /// </summary>
        /// <param name="xml">The broker rate lock history XML.</param>
        /// <returns>The HTML string for the table.</returns>
        public static string BuildBrokerRateLockHistoryTable(string xml)
        {
            return BuildBrokerRateLockHistoryTable(xml, true); 
        }

        /// <summary>
        /// Builds an HTML table and returns the markup.
        /// </summary>
        /// <param name="xml">The broker rate lock history XML.</param>
        /// <param name="generateDetailsLink">Indicates whether the details link should be included.</param>
        /// <returns>The HTML string for the table.</returns>
        public static string BuildBrokerRateLockHistoryTable(string xml, bool generateDetailsLink)
        {
            Table tb = new Table();
            tb.CellPadding = 0;
            tb.CellSpacing = 0;
            tb.CssClass = "RateLockTable";

            var columnHeaders = new List<Literal>()
            { 
                new EncodedLiteral() { Text = "Date" }, 
                new EncodedLiteral() { Text = "Action" }, 
                new EncodedLiteral() { Text = "By" }, 
                new EncodedLiteral() { Text = "Rate" }, 
                new EncodedLiteral() { Text = "Price" }, 
                new EncodedLiteral() { Text = "Fee" }, 
                new EncodedLiteral() { Text = "Margin" }, 
                new EncodedLiteral() { Text = "Teaser Rate" }, 
                new EncodedLiteral() { Text = "Program Name" }, 
                new EncodedLiteral() { Text = "Lock Date" }, 
                new EncodedLiteral() { Text = "Exp Date" }, 
                new EncodedLiteral() { Text = "Reason" }, 
            };

            if (generateDetailsLink)
            {
                columnHeaders.Add(new PassthroughLiteral() { Text = "&nbsp;" });
            }

            TableRow row = GenerateRow(columnHeaders);
            row.CssClass = "RateLockHeader";
            tb.Rows.Add(row);

            var cssClasses = new List<string>() 
            {
                "date",
                "action",
                "by",
                "rate",
                "price",
                "fee",
                "margin",
                "trate",
                "investor",
                "programName",
                "lockDate",
                "expDate",
                "reason",
            };

            if (generateDetailsLink)
            {
                cssClasses.Add("misc");
            }

            for (var i = 0; i < row.Cells.Count; i++)
            {
                row.Cells[i].CssClass = cssClasses[i];
            }

            var rateLockHistoryItems = RateLockHistoryItem.GetSortedRateHistory(xml);

            bool isOdd = true;
            foreach (RateLockHistoryItem o in rateLockHistoryItems)
            {
                string link = generateDetailsLink ? GenerateLink(o) : string.Empty;

                var data = new List<Literal>()
                {
                    new EncodedLiteral() { Text = o.EventDate },
                    new EncodedLiteral() { Text = o.Action },
                    new EncodedLiteral() { Text = o.DoneBy },
                    new EncodedLiteral() { Text = o.sNoteIR },
                    new EncodedLiteral() { Text = o.sBrokerLockFinalBrokComp1PcPrice },
                    new EncodedLiteral() { Text = o.sBrokComp1Pc },
                    new EncodedLiteral() { Text = o.sRAdjMarginR },
                    new EncodedLiteral() { Text = o.sOptionArmTeaserR },
                    new EncodedLiteral() { Text = o.sLpTemplateNm },
                    new EncodedLiteral() { Text = o.sRLckdD },
                    new EncodedLiteral() { Text = o.sRLckdExpiredD },
                    new PassthroughLiteral() { Text = LendersOffice.Common.Utilities.SafeHtmlString(o.sRLckBreakReasonDesc) }
                };

                if (generateDetailsLink)
                {
                    data.Add(new PassthroughLiteral() { Text = link });
                }

                row = GenerateRow(data);
                row.CssClass = isOdd ? "RateLockItem" : "RateLockAlternatingItem";

                isOdd = !isOdd;
                tb.Rows.Add(row);
            }

            return RenderControl(tb);
        }

        /// <summary>
        /// Makes a table row with the given data as column values.
        /// </summary>
        /// <param name="cellContent">The values for the columns.</param>
        /// <returns>The table row.</returns>
        private static TableRow GenerateRow(IEnumerable<Literal> cellContent)
        {
            TableRow row = new TableRow();
            foreach (Literal column in cellContent)
            {
                TableCell tc = new TableCell();
                tc.Controls.Add(column);
                row.Cells.Add(tc);
            }

            return row;
        }

        /// <summary>
        /// Writes the table control out to a string.
        /// </summary>
        /// <param name="ctrl">The table to write.</param>
        /// <returns>The HTML markup for the table.</returns>
        private static string RenderControl(Table ctrl)
        {
            StringBuilder sb = new StringBuilder();
            using (var tw = new System.IO.StringWriter(sb))
            using (var hw = new System.Web.UI.HtmlTextWriter(tw))
            {
                ctrl.RenderControl(hw);
                return sb.ToString();
            }
        }

        /// <summary>
        /// Generates HTML for a details link for a rate lock history item.
        /// </summary>
        /// <param name="item">The rate lock history item.</param>
        /// <returns>The HTML string for the details link.</returns>
        private static string GenerateLink(AbstractRateLockHistoryItem item)
        {
            string link = string.Empty;

            if (string.IsNullOrEmpty(item.eventId))
            {
                return link;
            }

            E_RateLockAuditActionT[] filteredEvents = new E_RateLockAuditActionT[] 
            { 
                E_RateLockAuditActionT.UNDEFINED,
                E_RateLockAuditActionT.BreakRateLock, 
                E_RateLockAuditActionT.ClearRateLockRequest 
            };

            if (filteredEvents.Contains(item.sRateLockAuditActionT))
            {
                return link;
            }

            link = @"<a href=""#"" onclick=""return f_openAuditItem(" + LendersOffice.AntiXss.AspxTools.JsString(item.eventId) + @")"";>details</a>";

            return link;
        }
    }
}
