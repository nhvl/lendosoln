﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Common;
using LendersOffice.Security;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data;
using LendersOffice.Constants;
using LqbGrammar.DataTypes;
using LendersOffice.Drivers.SqlServerDB;

namespace DataAccess
{
    public class LoanFileCache
    {
        private Dictionary<string, object> m_row = null;

        private ExecutingEnginePrincipal m_principal;
        private ExecutingEnginePrincipal CurrentPrincipal
        {
            get { return m_principal; }
        }


        /// <summary>
        /// Pass null for fieldNameTranslator if you have dataRow contains same column name as fieldList.
        /// </summary>
        /// <param name="principal"></param>
        /// <param name="sLId"></param>
        /// <param name="dataRow"></param>
        /// <param name="fieldList"></param>
        /// <param name="fieldNameTranslator"></param>
        public LoanFileCache(Guid sLId, DataRow dataRow, IEnumerable<string> fieldList, Func<string, string> fieldNameTranslator)
        {
            Initialize(sLId);
            LoadFromDataRow(dataRow, fieldList, fieldNameTranslator);
        }
        public LoanFileCache(Guid sLId, IEnumerable<string> fieldList)
        {
            Initialize(sLId);
            LoadFromDatabase(fieldList);
        }

        public LoanFileCache(DbConnectionInfo connInfo, Guid sLId, IEnumerable<string> fieldList)
        {
            Initialize(sLId);
            LoadFromDatabase(connInfo, fieldList);
        }

        private void Initialize(Guid sLId)
        {
            this.sLId = sLId;
            m_row = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
        }
        public void SetEvaluatingPrincipal(ExecutingEnginePrincipal principal)
        {
            if (null == principal)
            {
                throw CBaseException.GenericException("principal is null in LoanFileCache.SetEvaluatingPrincipal");
            }
            if (principal.BrokerId != sBrokerId)
            {
                throw new AccessDenied("Access denied. You are not logged in to this loan's broker.", "Access Denied due to being logged in to the wrong broker");
            }
            m_principal = principal;
        }

        public bool ContainsField(string fieldId)
        {
            return m_row.ContainsKey(fieldId);
        }

        // 12/27/2012 dd - List of fields in LOAN_FILE_CACHE that allow to be null due to
        // calculation error.
        // Example:  APR can be null if loan amount or interest rate is zero.
        private readonly string[] ALLOW_NULL_FIELD_LIST = {
                                                              "sApr", 
                                                              "sLtvR",
                                                              "sMaxR",
                                                              "sMonthlyPmt"
                                                          };
        private void LoadFromDataRow(DataRow dataRow, IEnumerable<string> fieldList, Func<string, string> fieldNameTranslator)
        {

            DataTable table = dataRow.Table;
            foreach (var fieldId in fieldList)
            {
                string columnName = fieldId;
                if (null != fieldNameTranslator)
                {
                    columnName = fieldNameTranslator(columnName);

                    if (table.Columns.Contains(columnName) == false)
                    {
                        // 1/26/2015 dd - Fall back to origin name without fieldNameTranslator.
                        columnName = fieldId;
                    }
                
                }
                if (table.Columns.Contains(columnName))
                {
                    object v = dataRow[columnName];
                    if (!(v is DBNull))
                    {
                        m_row[fieldId] = v;
                    }
                    else if (ALLOW_NULL_FIELD_LIST.Contains(fieldId, StringComparer.OrdinalIgnoreCase))
                    {
                        m_row[fieldId] = null;
                    }
                    else
                    {
                        Type t = table.Columns[columnName].DataType;
                        if (t == typeof(DateTime))
                        {
                            m_row[fieldId] = DateTime.MinValue;
                        }
                    }
                }
            }

        }

        private void LoadFromDatabase(IEnumerable<string> fieldList)
        {
            Guid brokerId = Guid.Empty;

            DbConnectionInfo connInfo = DbConnectionInfo.GetConnectionInfoByLoanId(sLId, out brokerId);

            LoadFromDatabase(connInfo, fieldList);
        }

        private void LoadFromDatabase(DbConnectionInfo connInfo, IEnumerable<string> fieldList)
        {

            if (connInfo == null)
            {
                Guid brokerId = Guid.Empty;

                connInfo = DbConnectionInfo.GetConnectionInfoByLoanId(sLId, out brokerId);
            }

            string sql = GetSelectSqlStatement(fieldList); // Only @sLId is needed, so OK

            SqlParameter[] parameters = {
                                            new SqlParameter("@sLId", sLId)
                                        };

            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                if (reader.Read())
                {
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        string fieldId = reader.GetName(i);
                        object v = reader.GetValue(i);

                        if (!(v is DBNull))
                        {
                            m_row[fieldId] = v;
                        }
                        else if (ALLOW_NULL_FIELD_LIST.Contains(fieldId, StringComparer.OrdinalIgnoreCase))
                        {
                            m_row[fieldId] = null; // sApr can be null if we could not compute its value.
                        }
                        else
                        {
                            Type t = reader.GetFieldType(i);
                            if (t == typeof(DateTime))
                            {
                                m_row[fieldId] = DateTime.MinValue;
                            }
                        }
                    }
                }
                else
                {
                    throw new LoanNotFoundException(sLId);
                }
            };

            DBSelectUtility.ProcessDBData(connInfo, sql, null, parameters, readHandler);
        }
        public Guid sLId { get; private set;}
        public Guid sBrokerId 
        {
            get
            {
                Guid brokerId;
                if (TryGetGuid("sBrokerId", out brokerId))
                {
                    return brokerId;
                }
                else
                {
                    throw CBaseException.GenericException("Required sBrokerId in LoanFileCache");
                }
            }
        }

        public string sLNm
        {
            get
            {
                if (m_row != null)
                {
                    string name;
                    if (TryGetString("sLNm", out name))
                    {
                        return name;
                    }
                }
                return string.Empty;
            }
        }
        /// <summary>
        /// Checks if the specified <paramref name="fieldId"/> can be loaded through Loan_File_Cache.
        /// </summary>
        /// <param name="fieldId">A possible field field ID in the table.</param>
        /// <returns>A value indicating whether the field is present in a LOAN_FILE_CACHE_X table.</returns>
        public static bool IsCachedField(string fieldId)
        {
            CFieldDbInfo field = CFieldDbInfoList.TheOnlyInstance.Get(fieldId);
            return field != null && field.IsCachedField;
        }
        private string GetSelectSqlStatement(IEnumerable<string> fieldList)
        {
            SortedList<string, bool> uniqueFields = new SortedList<string, bool>(StringComparer.OrdinalIgnoreCase);

            bool bNeedLoanFileCache2 = false;
            bool bNeedLoanFileCache3 = false;
            bool bNeedLoanFileCache4 = false;
            foreach (var fieldId in fieldList)
            {
                if (fieldId.Equals("sLId", StringComparison.OrdinalIgnoreCase))
                {
                    continue;
                }
                if (uniqueFields.ContainsKey(fieldId) == false)
                {
                    CFieldDbInfo field = CFieldDbInfoList.TheOnlyInstance.Get(fieldId);
                    if (null != field)
                    {
                        if (field.IsCachedField)
                        {
                            switch (field.m_cacheTable)
                            {
                                case E_DbCacheTable.Loan_Cache_1:
                                    break;
                                case E_DbCacheTable.Loan_Cache_2:
                                    bNeedLoanFileCache2 = true;
                                    break;
                                case E_DbCacheTable.Loan_Cache_3:
                                    bNeedLoanFileCache3 = true;
                                    break;
                                case E_DbCacheTable.Loan_Cache_4:
                                    bNeedLoanFileCache4 = true;
                                    break;
                                default:
                                    throw new UnhandledEnumException(field.m_cacheTable);
                            }
                            uniqueFields.Add(fieldId, false);
                        }
                    }

                }
            }

            StringBuilder sqlBuilder = new StringBuilder("SELECT sBrokerId, ");
            sqlBuilder.Append(string.Join(",", uniqueFields.Keys));
            if (bNeedLoanFileCache2)
            {
                sqlBuilder.Append(" FROM LOAN_FILE_CACHE AS C JOIN LOAN_FILE_CACHE_2 AS C2 ON C.sLId = c2.sLId ");
            }
            else 
            {
                sqlBuilder.Append(" FROM LOAN_FILE_CACHE AS C ");
            }
            if (bNeedLoanFileCache3)
            {
                sqlBuilder.Append(" JOIN LOAN_FILE_CACHE_3 AS c3 ON c.sLId = c3.sLId ");
            }
            if (bNeedLoanFileCache4)
            {
                sqlBuilder.Append(" JOIN LOAN_FILE_CACHE_4 AS c4 ON c.sLId = c4.sLId ");
            }

            if (PrincipalFactory.CurrentPrincipal != null && PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowLoadingDeletedLoans))
            {
                sqlBuilder.Append(" WHERE C.sLId = @sLId");
            }
            else
            {
                sqlBuilder.Append(" WHERE C.IsValid = 1 AND C.sLId = @sLId");
            }

            if (ConstStage.IsLoanForceOrderHintEnabled)
            {
                sqlBuilder.Append(" OPTION (FORCE ORDER)");
            }

            return sqlBuilder.ToString();
        }
        public bool TryGetFloat(string fieldName, out float value)
        {
            if (m_row == null)
            {
                throw new GenericUserErrorMessageException("Need to invoke InitLoad() first.");
            }
            value = float.NaN;

            object o = null;
            bool ret = m_row.TryGetValue(fieldName, out o);
            if (ret == true)
            {
                if (o == null)
                {
                    ret = false;
                }
                else
                {
                    value = Convert.ToSingle(o);
                }

            }
            return ret;
        }

        public bool TryGetInt(string fieldName, out int value)
        {
            if (m_row == null)
            {
                throw new GenericUserErrorMessageException("Need to invoke InitLoad() first.");
            }
            value = int.MinValue;

            object o = null;
            bool ret = m_row.TryGetValue(fieldName, out o);
            if (ret == true)
            {
                if (o == null)
                {
                    ret = false;
                }
                else
                {
                    value = Convert.ToInt32(o);
                }
            }
            return ret;

        }

        public bool TryGetGuid(string fieldName, out Guid value)
        {
            if (m_row == null)
            {
                throw new GenericUserErrorMessageException("Need to invoke InitLoad() first.");
            }

            value = Guid.Empty;
            object o = null;

            bool ret = m_row.TryGetValue(fieldName, out o);
            if (ret == true)
            {
                if (o == null)
                {
                    ret = false;
                }
                else
                {
                    value = (Guid)o;
                }
            }

            return ret;

        }

        public bool TryGetDateTime(string fieldName, out DateTime value)
        {
            if (m_row == null)
            {
                throw new GenericUserErrorMessageException("Need to invoke InitLoad() first.");
            }
            value = DateTime.MinValue;

            object o;

            bool ret = m_row.TryGetValue(fieldName, out o);
            
            if (ret == true)
            {
                value = (DateTime)o;

                //If we leave this comparing empty date to a date fails with exception in workflow.  (DD, AV, SK reviewed)
                //    if (value == DateTime.MinValue)
                //    {
                //        // 11/18/2011 dd - Consider this to be invalid value.
                //        ret = false;
                //    }
            }
            return ret;
        }

        public bool TryGetString(string fieldName, out string value)
        {
            if (m_row == null)
            {
                throw new GenericUserErrorMessageException("Need to invoke InitLoad() first.");
            }

            value = string.Empty;
            object o = null;

            bool ret = m_row.TryGetValue(fieldName, out o);
            if (ret == true)
            {
                if (o == null)
                {
                    ret = false;
                }
                else
                {
                    value = o.ToString();
                }
            }
            return ret;
        }

        public bool TryGetBool(string fieldName, out bool value)
        {
            if (m_row == null)
            {
                throw new GenericUserErrorMessageException("Need to invoke InitLoad() first.");
            }

            value = false;
            object o = null;

            bool ret = m_row.TryGetValue(fieldName, out o);
            if (ret == true)
            {
                value = (bool) o;
            }
            return ret;
        }

        /// <summary>
        /// Gets the string representation of the field.
        /// </summary>
        /// <param name="fieldName">The field ID of the field in the Cache table.</param>
        /// <param name="convertLos">The conversion information to use when formatting data.</param>
        /// <returns>The string representation of the field using LosConvert as possible, or the empty string.</returns>
        /// <exception cref="GenericUserErrorMessageException">The field was not a valid field on the cache table.</exception>
        public string GetStringRep(string fieldName, LosConvert convertLos)
        {
            string value;
            CFieldDbInfo field = CFieldDbInfoList.TheOnlyInstance.Get(fieldName);
            if (field == null || !field.IsCachedField)
            {
                throw new GenericUserErrorMessageException("Field not in cache tables: [" + fieldName + "]");
            }

            if (field.m_dbType == SqlDbType.DateTime
                || field.m_dbType == SqlDbType.DateTime2
                || field.m_dbType == SqlDbType.SmallDateTime)
            {
                DateTime dt;
                this.TryGetDateTime(fieldName, out dt);
                value = convertLos.ToDateTimeString(dt);
            }
            else if (field.m_dbType == SqlDbType.Bit)
            {
                bool bit;
                this.TryGetBool(fieldName, out bit);
                value = convertLos.ToBitString(bit);
            }
            else
            {
                this.TryGetString(fieldName, out value);
            }

            return value ?? string.Empty;
        }

        private string GetRoleAssignmentCacheFieldId(E_RoleT role)
        {
            string fieldId = string.Empty;
            switch (role)
            {
                case E_RoleT.CallCenterAgent:
                    fieldId = "sEmployeeCallCenterAgentId";
                    break;
                case E_RoleT.Closer:
                    fieldId = "sEmployeeCloserId";
                    break;
                case E_RoleT.LenderAccountExecutive:
                    fieldId = "sEmployeeLenderAccExecId";
                    break;
                case E_RoleT.LoanOfficer:
                    fieldId = "sEmployeeLoanRepId";
                    break;
                case E_RoleT.LoanOpener:
                    fieldId = "sEmployeeLoanOpenerId";
                    break;
                case E_RoleT.LockDesk:
                    fieldId = "sEmployeeLockDeskId";
                    break;
                case E_RoleT.Manager:
                    fieldId = "sEmployeeManagerId";
                    break;
                case E_RoleT.Processor:
                    fieldId = "sEmployeeProcessorId";
                    break;
                case E_RoleT.RealEstateAgent:
                    fieldId = "sEmployeeRealEstateAgentId";
                    break;
                case E_RoleT.Underwriter:
                    fieldId = "sEmployeeUnderwriterId";
                    break;
                case E_RoleT.Pml_LoanOfficer:
                    fieldId = "sEmployeeLoanRepId";
                    break;
                case E_RoleT.Pml_BrokerProcessor:
                    fieldId = "sEmployeeBrokerProcessorId";
                    break;
                case E_RoleT.Funder:
                    fieldId = "sEmployeeFunderId";
                    break;
                case E_RoleT.Shipper:
                    fieldId = "sEmployeeShipperId";
                    break;
                case E_RoleT.PostCloser:
                    fieldId = "sEmployeePostCloserId";
                    break;
                case E_RoleT.Insuring:
                    fieldId = "sEmployeeInsuringId";
                    break;
                case E_RoleT.CollateralAgent:
                    fieldId = "sEmployeeCollateralAgentId";
                    break;
                case E_RoleT.DocDrawer:
                    fieldId = "sEmployeeDocDrawerId";
                    break;
                case E_RoleT.CreditAuditor:
                    fieldId = "sEmployeeCreditAuditorId";
                    break;
                case E_RoleT.DisclosureDesk:
                    fieldId = "sEmployeeDisclosureDeskId";
                    break;
                case E_RoleT.JuniorProcessor:
                    fieldId = "sEmployeeJuniorProcessorId";
                    break;
                case E_RoleT.JuniorUnderwriter:
                    fieldId = "sEmployeeJuniorUnderwriterId";
                    break;
                case E_RoleT.LegalAuditor:
                    fieldId = "sEmployeeLegalAuditorId";
                    break;
                case E_RoleT.LoanOfficerAssistant:
                    fieldId = "sEmployeeLoanOfficerAssistantId";
                    break;
                case E_RoleT.Purchaser:
                    fieldId = "sEmployeePurchaserId";
                    break;
                case E_RoleT.QCCompliance:
                    fieldId = "sEmployeeQCComplianceId";
                    break;
                case E_RoleT.Secondary:
                    fieldId = "sEmployeeSecondaryId";
                    break;
                case E_RoleT.Servicing:
                    fieldId = "sEmployeeServicingId";
                    break;
                case E_RoleT.Pml_Secondary:
                    fieldId = "sEmployeeExternalSecondaryId";
                    break;
                case E_RoleT.Pml_PostCloser:
                    fieldId = "sEmployeeExternalPostCloserId";
                    break;
                case E_RoleT.Consumer:
                case E_RoleT.Accountant:
                case E_RoleT.Administrator:
                case E_RoleT.Pml_Administrator:
                    fieldId = string.Empty; // 9/7/2010 dd - These roles are non-assignable.
                    break;
                default:
                    throw new UnhandledEnumException(role);
            }

            return fieldId;
        }

        public E_ActAsRateLockedT ActAsRateLockedT
        {
            get
            {
                E_ActAsRateLockedT ret = E_ActAsRateLockedT.NoAffect;

                if (CurrentPrincipal.HasRole(E_RoleT.LenderAccountExecutive) && CurrentPrincipal.IsRateLockedAtSubmission)
                {
                    int v;
                    if (TryGetInt("sRateLockStatusT", out v))
                    {
                        E_sRateLockStatusT sRateLockStatusT = (E_sRateLockStatusT)v;

                        if (sRateLockStatusT == E_sRateLockStatusT.Locked)
                        {
                            ret = E_ActAsRateLockedT.AllowRead;
                        }
                        else
                        {
                            float sNoteIRSubmitted;
                            if (TryGetFloat("sNoteIRSubmitted", out sNoteIRSubmitted))
                            {
                                if (sNoteIRSubmitted > 0)
                                {

                                    if (TryGetInt("sStatusT", out v))
                                    {
                                        E_sStatusT sStatusT = (E_sStatusT)v;
                                        switch (sStatusT)
                                        {
                                            case E_sStatusT.Loan_Prequal:
                                            case E_sStatusT.Loan_Preapproval:
                                            case E_sStatusT.Loan_Registered:
                                            case E_sStatusT.Loan_Approved:
                                            case E_sStatusT.Loan_Docs:
                                            case E_sStatusT.Loan_OnHold:
                                            case E_sStatusT.Loan_Suspended:
                                            case E_sStatusT.Loan_Canceled:
                                            case E_sStatusT.Loan_Rejected:
                                            case E_sStatusT.Loan_WebConsumer:
                                            case E_sStatusT.Loan_Other:
                                            case E_sStatusT.Loan_ClearToClose:
                                                ret = E_ActAsRateLockedT.AllowRead;
                                                break;
                                            case E_sStatusT.Lead_New:
                                            case E_sStatusT.Lead_Canceled:
                                            case E_sStatusT.Lead_Declined:
                                            case E_sStatusT.Lead_Other:
                                            case E_sStatusT.Loan_Open:
                                                ret = E_ActAsRateLockedT.AllowWrite;
                                                break;



                                            case E_sStatusT.Loan_Funded:
                                            case E_sStatusT.Loan_Closed:
                                            case E_sStatusT.Loan_LoanSubmitted:
                                            case E_sStatusT.Loan_Underwriting:
                                            case E_sStatusT.Loan_Recorded:
                                            case E_sStatusT.Loan_Shipped:
                                            case E_sStatusT.Loan_PreProcessing:    // start sk 1/6/2014 opm 145251
                                            case E_sStatusT.Loan_DocumentCheck:
                                            case E_sStatusT.Loan_DocumentCheckFailed:
                                            case E_sStatusT.Loan_PreUnderwriting:
                                            case E_sStatusT.Loan_ConditionReview:
                                            case E_sStatusT.Loan_PreDocQC:
                                            case E_sStatusT.Loan_DocsOrdered:
                                            case E_sStatusT.Loan_DocsDrawn:
                                            case E_sStatusT.Loan_InvestorConditions:       // this is "promoting the date to a status". tied to sSuspendedByInvestorD
                                            case E_sStatusT.Loan_InvestorConditionsSent:   // this is "promoting the date to a status". tied to sCondSentToInvestorD
                                            case E_sStatusT.Loan_ReadyForSale:
                                            case E_sStatusT.Loan_SubmittedForPurchaseReview:
                                            case E_sStatusT.Loan_InPurchaseReview:
                                            case E_sStatusT.Loan_PrePurchaseConditions:
                                            case E_sStatusT.Loan_SubmittedForFinalPurchaseReview:
                                            case E_sStatusT.Loan_InFinalPurchaseReview:
                                            case E_sStatusT.Loan_ClearToPurchase:
                                            case E_sStatusT.Loan_Purchased:        // don't confuse with the old case E_sStatusT.Loan_LoanPurchased: which is now Loan Sold in the UI.
                                            case E_sStatusT.Loan_LoanPurchased:
                                            case E_sStatusT.Loan_CounterOffer:
                                            case E_sStatusT.Loan_Withdrawn:
                                            case E_sStatusT.Loan_Archived:
                                                ret = E_ActAsRateLockedT.NoAffect;
                                                break;
                                                
                                            default:
                                                throw new UnhandledEnumException(sStatusT);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        throw new CBaseException(ErrorMessages.Generic, "Missing sRateLockStatusT in LoanFileCache");
                    }
                }
                return ret;
            }
        }

        public bool sLpTemplateIdNonEmpty
        {
            get
            {
                Guid sLpTemplateId = Guid.Empty;

                if (TryGetGuid("sLpTemplateId", out sLpTemplateId) == false)
                {
                    sLpTemplateId = Guid.Empty;
                }
                return sLpTemplateId != Guid.Empty;
            }
        }
        public IEnumerable<E_RoleT> GetAssignAsRole()
        {
            List<E_RoleT> result = new List<E_RoleT>();
            foreach (E_RoleT role in CurrentPrincipal.GetRoles())
            {
                if (CurrentPrincipal.EmployeeId == GetAssignmentFor(role))
                {
                    result.Add(role);
                }
            }
            return result;
        }
        public bool IsSameBranchAsLoanFile
        {
            get
            {
                Guid loanBranchId;
                if (TryGetGuid("sBranchId", out loanBranchId))
                {
                    return loanBranchId == CurrentPrincipal.BranchId;
                }
                return false;
            }
        }
        public bool IsAssignedToCurrentUser
        {
            get
            {
                if (m_row == null)
                {
                    throw new GenericUserErrorMessageException("Need to invoke InitLoad() first.");
                }

                foreach (var role in CurrentPrincipal.GetRoles())
                {
                    string fieldId = GetRoleAssignmentCacheFieldId(role);
                    if (fieldId != string.Empty)
                    {
                        Guid assignedEmployeeId = Guid.Empty;

                        if (TryGetGuid(fieldId, out assignedEmployeeId) == true)
                        {
                            if (assignedEmployeeId == CurrentPrincipal.EmployeeId)
                            {
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
        }
        /// <summary>
        /// Return true if there is an underwriter that is not a current user assign to the loan.
        /// </summary>
        public bool HaveUnderwriterAssigned
        {
            get
            {
                return HaveAssignmentFor(E_RoleT.Underwriter) || HaveAssignmentFor(E_RoleT.JuniorUnderwriter);
            }
        }

        /// <summary>
        /// Return true if there is an processor that is not a current user assign to the loan.
        /// </summary>
        public bool HaveProcessorAssigned
        {
            get
            {
                return HaveAssignmentFor(E_RoleT.Processor) || HaveAssignmentFor(E_RoleT.JuniorProcessor);
            }
        }

        /// <summary>
        /// THis method is use in IsPmlAssigned variable for hack. Should limit the use of this function.
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public Guid GetAssignmentFor(E_RoleT role)
        {
            if (m_row == null)
            {
                throw new GenericUserErrorMessageException("Need to invoke InitLoad() first.");
            }
            Guid employeeId = Guid.Empty;
            string fieldId = GetRoleAssignmentCacheFieldId(role);
            if (fieldId != string.Empty)
            {
                TryGetGuid(fieldId, out employeeId);
            }
            return employeeId;
        }
        /// <summary>
        /// Return true if there is an assignment to the role that is not a current user.
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        /// 
        public bool HaveAssignmentFor(E_RoleT role)
        {
            if (m_row == null)
            {
                throw new GenericUserErrorMessageException("Need to invoke InitLoad() first.");
            }

            string fieldId = GetRoleAssignmentCacheFieldId(role);
            if (fieldId != string.Empty)
            {
                Guid assignedEmployeeId = Guid.Empty;

                if (TryGetGuid(fieldId, out assignedEmployeeId) == true)
                {
                    if (assignedEmployeeId != Guid.Empty)
                    {
                        if (assignedEmployeeId == CurrentPrincipal.EmployeeId)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                }
            }
            return false;

        }
        
        /// <summary>
        /// Returns true if a team in the list is assigned in a role.
        /// </summary>
        /// <param name="teamList"></param>
        /// <returns></returns>
        public bool IsTeamAssigned(HashSet<Guid> teamList)
        {
            Guid result;
            foreach (var teamId in teamList)
            {
                foreach (var fieldid in teamFields)
                {
                    if (TryGetGuid(fieldid, out result))
                    {
                        if (result == teamId)
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private readonly string[] teamFields = 
        {
            "sTeamLoanRepId",
            "sTeamManagerId",
            "sTeamProcessorId",
            "sTeamRealEstateAgentId",
            "sTeamCallCenterAgentId",
            "sTeamLenderAccExecId",
            "sTeamLockDeskId",
            "sTeamUnderwriterId",
            "sTeamLoanOpenerId",
            "sTeamCloserId",
            "sTeamShipperId",
            "sTeamFunderId",
            "sTeamPostCloserId",
            "sTeamInsuringId",
            "sTeamCollateralAgentId",
            "sTeamDocDrawerId",
            "sTeamCreditAuditorId",
            "sTeamDisclosureDeskId",
            "sTeamJuniorProcessorId",
            "sTeamJuniorUnderwriterId",
            "sTeamLegalAuditorId",
            "sTeamLoanOfficerAssistantId",
            "sTeamPurchaserId",
       };

    }
}
