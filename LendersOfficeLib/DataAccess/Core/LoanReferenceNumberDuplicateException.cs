// <copyright file="LoanReferenceNumberDuplicateException.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Jhairo Erazo
//    Date:   09/01/2016
// </summary>
namespace DataAccess
{
    using System;

    /// <summary>
    /// Thrown if a loan with a conflicting loan reference number tries to save.
    /// </summary>
    public class LoanReferenceNumberDuplicateException : CBaseException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoanReferenceNumberDuplicateException" /> class.
        /// </summary>
        /// <param name="exc">Original SQL uniqueness constraint exception.</param>
        public LoanReferenceNumberDuplicateException(Exception exc) : base(LendersOffice.Common.JsMessages.LoanInfo_DuplicateLoanReferenceNumber, exc)
        {
            this.IsEmailDeveloper = false;
        }
    }
}
