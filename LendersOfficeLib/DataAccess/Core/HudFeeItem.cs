﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{
    public enum E_HudFeeItemSectionT
    {
        LeaveBlank,
        _800_LoanFees,
        _900_LenderRequiredPaidInAdvance,
        _1000_ReservesDepositedWithLender,
        _1100_TitleCharges,
        _1200_RecordingAndTransferCharges,
        _1300_AdditionalSettlementCharges
    }
    public class HudFeeItem
    {
        public HudFeeItem()
        {
        }
        public HudFeeItem(string hudLineNumber, string description, decimal percentFee, E_PercentBaseT percentBaseT, decimal baseAmount, E_GfeSectionT gfeSectionT, int props)
        {
            HudLineNumber = hudLineNumber;
            Description = description;
            PercentFee = percentFee;
            PercentBaseT = percentBaseT;
            BaseAmountFee = baseAmount;
            GfeSectionT = gfeSectionT;
            IsApr = LosConvert.GfeItemProps_Apr(props);
            IsPoc = LosConvert.GfeItemProps_Poc(props);
            IsFhaAllowable = LosConvert.GfeItemProps_FhaAllow(props);
            IsPaidToBroker = LosConvert.GfeItemProps_ToBr(props);

            int hudNumber = 0;
            if (int.TryParse(HudLineNumber, out hudNumber))
            {
                if (hudNumber >= 800 && hudNumber <= 899)
                {
                    SectionT = E_HudFeeItemSectionT._800_LoanFees;
                } else if (hudNumber >= 900 && hudNumber <= 999) {
                    SectionT = E_HudFeeItemSectionT._900_LenderRequiredPaidInAdvance;
                }
                else if (hudNumber >= 1000 && hudNumber <= 1099)
                {
                    SectionT = E_HudFeeItemSectionT._1000_ReservesDepositedWithLender;
                }
                else if (hudNumber >= 1100 && hudNumber <= 1199)
                {
                    SectionT = E_HudFeeItemSectionT._1100_TitleCharges;
                }
                else if (hudNumber >= 1200 && hudNumber <= 1299)
                {
                    SectionT = E_HudFeeItemSectionT._1200_RecordingAndTransferCharges;
                }
                else if (hudNumber >= 1300 && hudNumber <= 1399)
                {
                    SectionT = E_HudFeeItemSectionT._1300_AdditionalSettlementCharges;
                }
            }
        }
        public string HudLineNumber { get; set; }
        public string Description { get; set; }
        public E_HudFeeItemSectionT SectionT { get; set; }
        public bool IsApr { get; set; }
        public bool IsFhaAllowable { get; set; }
        public bool IsPoc { get; set; }
        public bool IsPaidToBroker { get; set; }
        public E_GfeSectionT GfeSectionT { get; set; }
        public decimal PercentFee { get; set; }
        public E_PercentBaseT PercentBaseT { get; set; }
        public decimal BaseAmountFee { get; set; }

    }
}
