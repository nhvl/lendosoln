﻿// <copyright file="FileDbHttpRequestCache.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   8/21/2016
// </summary>
namespace DataAccess
{
    using System;
    using System.IO;
    using System.Web;
    using LendersOffice.Constants;

    /// <summary>
    /// This class will dedup the FileDB operation on the http request within certain time interval.
    /// If the context is not a http request then this class will do nothing.
    /// </summary>
    internal class FileDbHttpRequestCache
    {
        /// <summary>
        /// Represent an invalid cache instance.
        /// </summary>
        private static FileDbHttpRequestCache invalidRequestCache = new FileDbHttpRequestCache(false);

        /// <summary>
        /// Initializes a new instance of the <see cref="FileDbHttpRequestCache"/> class.
        /// </summary>
        /// <param name="isEnabled">Whether the cache is enable.</param>
        private FileDbHttpRequestCache(bool isEnabled)
        {
            this.IsEnabled = isEnabled;
            this.ExpirationInMs = ConstStage.FileDBExpirationInMs;
        }

        /// <summary>
        /// Gets a value indicating whether the cache is enable.
        /// </summary>
        public bool IsEnabled { get; private set; }

        /// <summary>
        /// Gets the expiration interval.
        /// </summary>
        public int ExpirationInMs { get; private set; }

        /// <summary>
        /// Gets the instance of FileDB cache.
        /// </summary>
        /// <returns>The instance of FileDB cache.</returns>
        public static FileDbHttpRequestCache GetCurrent()
        {
            HttpContext context = HttpContext.Current;

            if (context == null)
            {
                return invalidRequestCache;
            }
            else
            {
                string key = "FileDbHttpRequestCache_245BR";

                FileDbHttpRequestCache item = context.Items[key] as FileDbHttpRequestCache;

                if (item == null)
                {
                    item = new FileDbHttpRequestCache(true);
                    context.Items.Add(key, item);
                }

                return item;
            }
        }

        /// <summary>
        /// Check to see if the filedb key is exists in cache.
        /// </summary>
        /// <param name="database">FileDB database type.</param>
        /// <param name="key">The key of FileDB.</param>
        /// <param name="filePath">The file path of the cache content.</param>
        /// <returns>Indicate whether the key is exists in cache.</returns>
        public bool TryGet(E_FileDB database, string key, out string filePath)
        {
            filePath = string.Empty;

            if (!this.IsEnabled)
            {
                return false;
            }

            // 8/19/2016 - Based from discussion with Thinh, Antonio and Geoffrey, the cache will work as follow.
            //             For HttpRequest, we will check to see if the key already read previously and the key is not expire.
            //             If the filedb key still valid then return a local file path. Check for the local file path exists.
            //             If local file path does not exists then treat it as no key in memory.
            HttpContext context = HttpContext.Current;

            string cacheKey = this.GetKey(database, key);
            Tuple<string, DateTime> item = context.Items[cacheKey] as Tuple<string, DateTime>;

            if (item == null)
            {
                return false; // Not cache yet.
            }

            if (item.Item2 < DateTime.UtcNow)
            {
                context.Items.Remove(cacheKey);
                return false; // Item is expired.
            }

            FileInfo fileInfo = new FileInfo(item.Item1);

            if (!fileInfo.Exists)
            {
                context.Items.Remove(cacheKey);
                return false; // File is no longer available locally.
            }

            filePath = item.Item1;
            return true;
        }

        /// <summary>
        /// Add the filedb key to cache.
        /// </summary>
        /// <param name="database">FileDB database type.</param>
        /// <param name="key">The key of FileDB.</param>
        /// <param name="filePath">The file path of the content to add to cache.</param>
        public void Add(E_FileDB database, string key, string filePath)
        {
            if (!this.IsEnabled)
            {
                return;
            }

            string cacheKey = this.GetKey(database, key);

            HttpContext context = HttpContext.Current;

            Tuple<string, DateTime> item = context.Items[cacheKey] as Tuple<string, DateTime>;

            if (item == null || item.Item2 < DateTime.UtcNow)
            {
                // Not cache or item is expire.
                // In this scenario we want to create a new cache item with new expiration.
                string localFilePath = TempFileUtils.NewTempFilePath();

                DateTime expirationTime = DateTime.UtcNow.AddMilliseconds(this.ExpirationInMs);

                item = new Tuple<string, DateTime>(localFilePath, expirationTime);
                context.Items.Add(cacheKey, item);
            }
            else
            {
                // Item existed in cache. Re-use the expiration time but update the file path.
                string localFilePath = TempFileUtils.NewTempFilePath();

                DateTime expirationTime = item.Item2;

                item = new Tuple<string, DateTime>(localFilePath, expirationTime);
                context.Items[cacheKey] = item;
            }

            try
            {
                File.Move(filePath, item.Item1);
            }
            catch (IOException exc)
            {
                // Only log error, clear the cache and move on.
                Tools.LogError(exc);
                context.Items.Remove(cacheKey);
            }
            catch (UnauthorizedAccessException exc)
            {
                // Only log error, clear the cache and move on.
                Tools.LogError(exc);
                context.Items.Remove(cacheKey);
            }
        }

        /// <summary>
        /// Remove the filedb key from cache.
        /// </summary>
        /// <param name="database">FileDB database type.</param>
        /// <param name="key">The key of FileDB.</param>
        public void Remove(E_FileDB database, string key)
        {
            if (!this.IsEnabled)
            {
                return;
            }

            string cacheKey = this.GetKey(database, key);

            HttpContext context = HttpContext.Current;

            context.Items.Remove(cacheKey);
        }

        /// <summary>
        /// Generate the key base on database type and file name.
        /// </summary>
        /// <param name="database">FileDB database type.</param>
        /// <param name="key">The key of FileDB.</param>
        /// <returns>The key to use in dictionary.</returns>
        private string GetKey(E_FileDB database, string key)
        {
            return database.ToString("D") + "::" + key + "::GBWI03";
        }
    }
}