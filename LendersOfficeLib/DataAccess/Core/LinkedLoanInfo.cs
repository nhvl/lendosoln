using System;
using LendersOffice.Common;
using System.Data.Common;
using System.Data.SqlClient;

namespace DataAccess
{
	public class CLinkedLoanInfo
	{
		private readonly Guid linkedLId;
		private readonly string linkedLNm = string.Empty;
		private readonly bool isValid;

		public CLinkedLoanInfo(Guid linkedLId, string linkedLNm)
		{
			this.linkedLId = linkedLId;
			this.linkedLNm = linkedLNm;
			this.isValid = true;
		}

		public CLinkedLoanInfo()
		{
			this.isValid = false;
		}
		
		public bool IsLoanLinked
		{
			get{ return this.isValid; }
		}

        public Guid LinkedLId
        {
            get
            {
                if (this.isValid)
                {
                    return this.linkedLId;
                }

                throw new CBaseException(ErrorMessages.Generic, "CLinkedLoanInfo for unlinked loan does not have valid data");
            }
        }

        public string LinkedLNm
        {
            get { return this.linkedLNm; }
        }

        /// <summary>
        /// Retrieves information about a linked loan to the specified loan.
        /// </summary>
        /// <param name="loanId">The identifier of the loan to search for links</param>
        /// <param name="brokerId">The identifier of the broker who owns the specified loan.</param>
        /// <returns>A information about the linked loan or lack thereof, as clarified by <see cref="IsLoanLinked"/>.</returns>
        internal static CLinkedLoanInfo Retrieve(Guid loanId, Guid brokerId)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@sLId", loanId)
            };

            using (DbDataReader rd = StoredProcedureHelper.ExecuteReader(brokerId, "RetrieveLinkedLoan", parameters))
            {
                if (rd.Read())
                {
                    Guid loanId1 = new Guid(rd["sLId1"].ToString());
                    Guid loanId2 = new Guid(rd["sLId2"].ToString());

                    if (loanId == loanId1)
                    {
                        return new CLinkedLoanInfo(loanId2, rd["sLNm2"].ToString());
                    }
                    else if (loanId == loanId2)
                    {
                        return new CLinkedLoanInfo(loanId1, rd["sLNm1"].ToString());
                    }
                    else
                    {
                        Tools.Assert(loanId2 == loanId, "Unexpected result from RetrieveLinkedLoan store proc");
                    }
                }
            }

            return new CLinkedLoanInfo();
        }

	}
}