using System;
using System.Data;
using System.IO;
using System.Text;
using System.Collections;
using System.Globalization;
using LendersOffice.Common;

namespace DataAccess
{
	
	
	/// <summary>
	/// 
	/// </summary>
	public class CCompareDouble : IComparer
	{
		public CCompareDouble()
		{
			
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns>ValueCondition Less than zero x is less than y. Zero x equals y. Greater than zero x is greater than y.</returns>
		public int Compare ( System.Object x , System.Object y )
		{
			double result = (double) x - (double) y;
			if( 0 == result )
				return 0;
			if( result < 0 )
				return -1;
			return 1;

		}

	}
	
	public class CXmlRecordSubcollection: ISubcollection
    {
		private ICollectionItemBase[] m_subcoll;
		private CXmlRecordSubcollection(ICollectionItemBase[] subcoll )
		{
			m_subcoll = subcoll;
		}

        public static ISubcollection Create(ICollectionItemBase[] subcoll)
        {
            return new CXmlRecordSubcollection(subcoll);
        }

		public System.Collections.IEnumerator GetEnumerator()
		{
			return new CXmlRecordBaseEnumerator( this );
		}
		
		public ICollectionItemBase GetRecordAt( int pos )
		{
			return m_subcoll[pos];
		}
		public int Count
		{
			get { return m_subcoll.Length; }
		}

        private class CXmlRecordBaseEnumerator : IEnumerator
        {
            private int m_index = -1;
            private ISubcollection m_subcoll;

            public CXmlRecordBaseEnumerator(ISubcollection subcoll)
            {
                m_subcoll = subcoll;
            }

            public bool MoveNext()
            {
                ++m_index;
                if (m_index >= m_subcoll.Count)
                    return false;
                return true;

            }
            public void Reset()
            {
                m_index = 0;
            }
            public object Current
            {
                get
                {
                    return m_subcoll.GetRecordAt(m_index);
                }
            }
        }
    }


    /// <summary>
    /// Wrapper of the dataset which represents a list of xml records.
    /// </summary>
    abstract public class CXmlRecordCollection : IRecordCollection
    {
        public DataSet GetDataSet()
        {
            return m_ds;
        }

        public void EnsureDataSetUpToDate()
        {
            // This is a no-op for this class because the data set is already
            // our source of truth.
        }

		public ICollectionItemBase2 GetRegularRecordAt( int pos )
		{
			return (ICollectionItemBase2)m_sortedList.GetByIndex( pos );
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="recordId"></param>
		/// <returns></returns>
		public ICollectionItemBase2 GetRegRecordOf( Guid recordId )
		{
			foreach (ICollectionItemBase2 record in m_sortedList.Values )
			{
				if( recordId == record.RecordId )
					return record;
			}

            throw new CBaseException("Error: Record not found.", string.Format("XmlRecord is not found. Id = {0}", recordId.ToString()));
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="recordId">If it is Guid.Empty, new record with new id will be returned</param>
		/// <returns></returns>
		public ICollectionItemBase2 EnsureRegularRecordOf( Guid recordId )
		{
			if( Guid.Empty == recordId )
				return AddRegularRecord();
	
			foreach(ICollectionItemBase2 record in m_sortedList.Values )
			{
				if( recordId == record.RecordId )
					return record;
			}

            var newRec = AddRegularRecord();
			newRec.ChangeRecordId( recordId );
			return newRec;
		}

		/// <summary>
		/// Do not use this, use foreach on GetSubcollection() instead.
		/// </summary>
		/// <param name="i"></param>
		/// <returns></returns>
		public ICollectionItemBase2 GetSpecialRecordAt( int i )
		{
			return (ICollectionItemBase2)SpecialRecords[ i ];
		}

		public ICollectionItemBase2 AddRegularRecord()
		{
			return AddRegularRecordAt( CountRegular );
		}

        public ICollectionItemBase2 GetRecordOf(Guid recordId)
        {
            if (recordId == Guid.Empty)
            {
                return null;
            }
            foreach (ICollectionItemBase2 record in m_sortedList.Values)
            {
                if (recordId == record.RecordId)
                {
                    return record;
                }
                    
            }
            foreach (ICollectionItemBase2 record in SpecialRecords)
            {
                if (recordId == record.RecordId)
                {
                    return record;
                }
            }

            return null;
        }

		public bool HasRecordOf( Guid recordId )
		{
            return this.GetRecordOf(recordId) != null;
		}

		protected abstract ICollectionItemBase2 AddSpecialRecord( Enum type );
		
		/// <summary>
		/// Add new record, let collection object figure out special or regular
		/// and add the right one accordingly. Use this with extra care, as it
		/// can throw exception if the special records' fixed number of slots
		/// is all filled up.
		/// </summary>
		/// <returns></returns>
		public ICollectionItemBase2 AddRecord( Enum recordT ) // TODO: Should this be supported at all? it's so loose...
		{		
			if( IsTypeOfSpecialRecord( recordT ) )
				return AddSpecialRecord( recordT );
			var newRec = AddRegularRecordAt( CountRegular );
			newRec.KeyType = recordT;
			return newRec;
		}
		
		public ICollectionItemBase2 AddRegularRecordAt( int pos )
		{			
			var newRecord = CreateRegularRawRecord();
			InsertRegularRecordTo( pos, newRecord );
			return newRecord;
		}

		#region	PRIVATE DATA
		private SortedList _m_sortedList;
		#endregion // PRIVATE DATA
		#region PRIVATE METHODS AND PROPS
		
		protected enum E_ExistingRecordFaith
		{
			E_RegularOK = 0,
			E_RegularInvalid = 1,
			E_SpecialTaken = 2,
			E_SpecialInvalid = 3
		}
		abstract protected E_ExistingRecordFaith DetermineRawRecordDestiny(ICollectionItemBase2 record );
        static private void EnsureAddUniqueKey(SortedList sortedList, ICollectionItemBase2 record)
        {
            double key = record.OrderRankValue;
            if (sortedList.ContainsKey(key))
            {
                while (sortedList.ContainsKey(key))
                {
                    key += s_RankIncrement;
                }
                record.OrderRankValue = key;
            }
            sortedList.Add(key, record);
        }
        static private void EnsureAddUniqueKey(SortedList sortedList, ILiability record)
        {
            double key = record.OrderRankValue;
            if (sortedList.ContainsKey(key))
            {
                while (sortedList.ContainsKey(key))
                {
                    key += s_RankIncrement;
                }
                record.OrderRankValue = key;
            }
            sortedList.Add(key, record);
        }
        private void ConstructSortedList()
		{
			DataTable table = m_ds.Tables[0];
			DataRowCollection rows = table.Rows;
			int count = rows.Count;
			_m_sortedList = new SortedList( new CCompareDouble(), count );

			for( int iRow = count - 1; iRow >= 0; --iRow )
			{
				//Examine for empty row, occasionally we run into these guys, somehow dataset generates those weird row in the xml content
				if( System.DBNull.Value == rows[iRow]["RecordId"] )
				{
					rows.RemoveAt( iRow );
					continue;
				}

				var record = Reconstruct( iRow );
				if( record.IsOnDeathRow )
				{
					rows.RemoveAt( iRow );
					continue;
				}

				E_ExistingRecordFaith faith = DetermineRawRecordDestiny( record );
				switch( faith )
				{
					case E_ExistingRecordFaith.E_RegularInvalid:
					case E_ExistingRecordFaith.E_SpecialInvalid:
						rows.RemoveAt( iRow ); break;
					case E_ExistingRecordFaith.E_RegularOK:
						EnsureAddUniqueKey( _m_sortedList, record ); break;
					case E_ExistingRecordFaith.E_SpecialTaken:
						break; // Derived class manage this special record itself
					default:
                        throw new CBaseException(ErrorMessages.Generic, "Unhandled E_ExistingRecordFaith value in CXmlRecordCollection.ConstructSortedList()");
				}
			}
			table.AcceptChanges();
		}
		private void LogError( string sMsg )
		{
			Tools.LogError( string.Format( "sLId={0}. ", m_loanData.sLId.ToString() ) + sMsg );
		}
		private void LogError( Exception exc )
		{
			LogError( exc.Message + ". Call Stack:" + exc.StackTrace );
		}
		private DataSet GetDataSet( string xmlSchema, string xmlContent )
		{
            #if (DEBUG)
			Tools.LogInfo(xmlContent != "" ? xmlContent : "xmlContent is empty");
            #endif
			StreamReader readerData = null;
			StreamReader readerSchema = null;
			MemoryStream streamSchema = null;
			MemoryStream streamData = null;
			try
			{
				//ThienChange, org. code : System.Text.ASCIIEncoding e = new System.Text.ASCIIEncoding(); 
                System.Text.Encoding e = System.Text.Encoding.ASCII;
				DataSet ds = new DataSet( "XmlTable" );

				// Don't remove this block, we use this to retrieve the schema during debugging if it needs to be modified.
				bool RetrieveSchema = false;
				if( RetrieveSchema )
				{
					string contentMaster = "";
					string type = "Emplmt";
					switch( type )
					{
						case "Emplmt": 
							contentMaster = "<EmplmtXmlContent>    <EmplmtStartD />    <EmplmtEndD />    <EmplrNm />    <EmplrBusPhone />    <EmplrFax />    <EmplrAddr />    <EmplrCity />   <EmplrState />  <JobTitle />    <EmplrZip />    <VerifSentD />    <VerifRecvD />    <VerifExpD />    <IsSelfEmplmt />    <MonI />    <EmplmtStat />    <EmplmtLen />    <ProfLen /> <IsSpecialBorrowerEmployerRelationship /> <SelfOwnershipShare />  </EmplmtXmlContent>";
							break;
						case "Lia":
							contentMaster = "<LiaXmlContent><OwnerT /><DebtT/><ComNm /><ComAddr /><ComCity /><ComState /><ComZip /><ComPhone /><ComFax /><AccNum /><AccNm /><Bal /><Pmt /><R /><OrigTerm /><Due /><RemainMons /><WillBePdOff /><NotUsedInRatio /><IsPiggyBack /><Late30 /><Late60 /><Late90Plus /><IncInReposession /><IncInBankruptcy /><IncInForeclosure /><ExcFromUnderwriting /><VerifSentD /><VerifRecvD /><VerifExpD /></LiaXmlContent>";
							break;
						case "Asset":
							contentMaster = "<AssetXmlContent> <OwnerT /> <AssetT /> <Val /> <Desc /> <FaceVal /> <AccNum /> <AccNm /> <ComNm /> <StAddr /> <City /> <State /> <Zip /> <VerifSentD /> <VerifRecvD /> <VerifExpD /> </AssetXmlContent>";
							break;
						case "Re":
							break;
					}
				
					streamData = new MemoryStream( e.GetBytes( contentMaster ) );
					readerData = new StreamReader( streamData, System.Text.Encoding.ASCII, true, contentMaster.Length + 2 );				
					ds.ReadXml( readerData, XmlReadMode.Auto );
					streamData.Close();
					readerData.Close();
					if( RetrieveSchema )
					{
						string resultXml = ds.GetXml();
						string resultSchema = ds.GetXmlSchema();
					}
				}

				streamSchema = new MemoryStream( e.GetBytes( xmlSchema ) );
				readerSchema = new StreamReader( streamSchema, System.Text.Encoding.ASCII, true, xmlSchema.Length + 2 );
				ds.ReadXmlSchema( readerSchema );

				
				if( xmlContent.Length > 0 )
				{
					streamData = new MemoryStream( e.GetBytes( xmlContent ) );
					readerData = new StreamReader( streamData, System.Text.Encoding.ASCII, true, xmlContent.Length + 2 );				
					ds.ReadXml( readerData, XmlReadMode.Auto );
				}
				
				return ds;
			}
			catch( Exception ex )
			{
				LogError( ex );
			}
			finally
			{
				if( streamSchema != null )
					streamSchema.Close();

				if( streamData != null )
					streamData.Close();

				if( readerSchema != null )
					readerSchema.Close();

				if( readerData != null )
					readerData.Close();
			}
			return null;
		}
		#endregion //PRIVATE METHODS AND PROPS

		#region PROTECTED DATA
		protected DataSet	m_ds;
		protected CPageBase	m_loanData;
        protected CAppBase m_appData;
		#endregion // PROTECTED DATA
		#region PROTECTED METHODS AND PROPS
		abstract protected ICollectionItemBase2 Reconstruct( int iRow );
		protected SortedList m_sortedList
		{
			get 
			{ 
				if( null == _m_sortedList )
					ConstructSortedList();
				return _m_sortedList; 
			}
		}

        protected Action<DataSet> m_onflush;

        public void SetFlushFunction(Action<DataSet> onFlush)
        {
            m_onflush = onFlush;
        }

        protected CXmlRecordCollection(CPageBase loanData, CAppBase appData, string xmlSchema, string xmlContent)
		{
			m_loanData = loanData;
			m_appData = appData;
			m_ds = GetDataSet( xmlSchema, xmlContent );

			ConstructSortedList();
		}

        protected CXmlRecordCollection(CPageBase loanData, CAppBase appData, E_DataSetT dataSetT, string xmlContent)
        {
            m_loanData = loanData;
            m_appData = appData;
            m_ds = CBase.GetDataSet(dataSetT, xmlContent);

            ConstructSortedList();

        }
		
		
		
		static private double s_RankIncrement = 100;
		
		/// <summary>
		/// Use this for record that is currently on the list only.
		/// </summary>
		/// <param name="pos"></param>
		/// <param name="record"></param>
		protected void MoveRegularRecordTo( int pos, ICollectionItemBase2 record )
		{
			m_sortedList.Remove( record.OrderRankValue ); // removing it here to make the list index accurate, otherwise, we would have to do 2 cases, 1 for moving up and 1 for moving down
			InsertRegularRecordTo( pos, record );
		}

        protected void MoveRegularRecordTo(int pos, ILiability record)
        {
            m_sortedList.Remove(record.OrderRankValue); // removing it here to make the list index accurate, otherwise, we would have to do 2 cases, 1 for moving up and 1 for moving down
            InsertRegularRecordTo(pos, record);
        }

        /// <summary
        /// Use this for record that is currently not on the list.
        /// </summary>
        /// <param name="pos">if it equals to the number of record, record will be moved to the end</param>
        /// <param name="record"></param>
        protected void InsertRegularRecordTo(int pos, ICollectionItemBase2 record)
        {
            if (m_sortedList.Count == 0)
            {
                record.OrderRankValue = 0;
            }
            else if (m_sortedList.Count == pos)
            {
                var lastRecord = (ICollectionItemBase2)m_sortedList.GetByIndex(pos - 1);
                record.OrderRankValue = lastRecord.OrderRankValue + s_RankIncrement;
            }
            else
            {
                var afterRecord = (ICollectionItemBase2)m_sortedList.GetByIndex(pos);
                double after = afterRecord.OrderRankValue;

                double before;
                if (0 == pos)
                {
                    before = after - s_RankIncrement;
                }
                else
                {
                    var beforeRecord = (ICollectionItemBase2)m_sortedList.GetByIndex(pos - 1);
                    before = beforeRecord.OrderRankValue;
                }

                record.OrderRankValue = (after + before) / 2;
            }

            EnsureAddUniqueKey(m_sortedList, record);
        }

        /// <summary
        /// Use this for record that is currently not on the list.
        /// </summary>
        /// <param name="pos">if it equals to the number of record, record will be moved to the end</param>
        /// <param name="record"></param>
        protected void InsertRegularRecordTo(int pos, ILiability record)
        {
            if (m_sortedList.Count == 0)
            {
                record.OrderRankValue = 0;
            }
            else if (m_sortedList.Count == pos)
            {
                var lastRecord = (ICollectionItemBase2)m_sortedList.GetByIndex(pos - 1);
                record.OrderRankValue = lastRecord.OrderRankValue + s_RankIncrement;
            }
            else
            {
                var afterRecord = (ICollectionItemBase2)m_sortedList.GetByIndex(pos);
                double after = afterRecord.OrderRankValue;

                double before;
                if (0 == pos)
                {
                    before = after - s_RankIncrement;
                }
                else
                {
                    var beforeRecord = (ICollectionItemBase2)m_sortedList.GetByIndex(pos - 1);
                    before = beforeRecord.OrderRankValue;
                }

                record.OrderRankValue = (after + before) / 2;
            }

            EnsureAddUniqueKey(m_sortedList, record);
        }

        protected void MoveRegularRecordToEnd(ICollectionItemBase2 record )
		{
			MoveRegularRecordTo( m_sortedList.Count, record );
		}
		
		#endregion // PROTECTED METHODS AND PROPS

		protected bool BelongToTypeGroup( Enum type, Enum group )
		{
			int expVal = (int) Math.Pow( 2, int.Parse( type.ToString("D") ) );
			return 0 != ( (int) (int.Parse( group.ToString("D") ) & expVal ) );

		}

		
		protected ISubcollection GetSubcollectionCore( bool bInclusiveGroup, Enum group )
		{
			int countRegular = CountRegular;
			int countSpecial = CountSpecial;
			ArrayList subcoll = new ArrayList( countRegular + countSpecial );
			for( int i = 0; i < ( countRegular + countSpecial ); ++i )
			{
				var rec = ( i < countRegular ) ? GetRegularRecordAt( i ) : GetSpecialRecordAt( i - countRegular );
				bool bBelongToGroup = BelongToTypeGroup( rec.KeyType, group );
				if( bInclusiveGroup == bBelongToGroup )  // ( a and b ) or ( !a and !b ) is the same as ( a == b )
					subcoll.Add( rec );
			}
			return CXmlRecordSubcollection.Create( (ICollectionItemBase[]) subcoll.ToArray( typeof(ICollectionItemBase) ) );
		}

		
		protected abstract bool IsTypeOfSpecialRecord( Enum recordT );

		#region PUBLIC METHODS AND PROPS
		/// <summary>
		/// Count of regular records, not including special records
		/// </summary>
		public int CountRegular
		{
			get { return m_sortedList.Count; }
		}
		/// <summary>
		/// Do not use this method directly. Use MoveUp() from the individual record instead.
		/// </summary>
		/// <param name="record"></param>
		public void MoveRegularRecordUp(ICollectionItemBase2 record )
		{
			int curPos = m_sortedList.IndexOfKey( record.OrderRankValue ); 
			if( 0 == curPos )
				return;
			MoveRegularRecordTo( curPos - 1, record );			
		}
        /// <summary>
        /// Do not use this method directly. Use MoveDown() from the individual record instead.
        /// </summary>
        /// <param name="record"></param>
        public void MoveRegularRecordDown(ICollectionItemBase2 record )
		{
			SortedList list = m_sortedList; 
			int curPos = list.IndexOfKey( record.OrderRankValue );
			int maxPos = list.Count - 1;
			if( maxPos == curPos )
				return;

			MoveRegularRecordTo( curPos + 1, record );
		}
		

		/// <summary>
		/// Must call this to update the dataobject, before getting calculated values
		/// and save to db.
		/// </summary>
        virtual public void Flush()
        {
            if (m_onflush != null)
            {
                m_onflush(m_ds);
            }
        }
		abstract protected void DetachSpecialRecords();
		public void ClearAll()
		{
			m_sortedList.Clear();
			DetachSpecialRecords();
			m_ds.Clear();
			Flush();
		}
		
		/// <summary>
		/// See DataTable.Clone() and DataTable.Select( string filterExpression ) for full documentation
		/// </summary>
		/// <returns></returns>
		public DataTable CloneFilteredTable( string strFilterExpression )
		{
			DataTable table = m_ds.Tables[0];
			DataRow[] rows = table.Select( strFilterExpression );

			// Create a new empty LiaXmlContent table.
			DataTable fiteredTable = table.Clone();
			foreach (DataRow row in rows) 
			{
				fiteredTable.ImportRow(row);
			}

			return fiteredTable;
		}

		abstract protected ArrayList SpecialRecords
		{
			get;
		}
		/// <summary>
		/// Remove all records marked to die
		/// </summary>
		protected void ExecuteDeathRowRecords()
		{
			// Compute and cached some frequently used numbers and store them in the db.
			// Special records are not to be killed.
			int count = m_sortedList.Count;
			DataRowCollection rows = m_ds.Tables[0].Rows;

			for( int i = count - 1; i >= 0; --i )
			{
				var rec = (ICollectionItemBase2) m_sortedList.GetByIndex( i );
				if( rec.IsOnDeathRow )
				{
					m_sortedList.RemoveAt( i );
					rows.Remove( rec.DataRow );
				}
			}
		}

		
		internal class CMoneyStringComparer : IComparer
		{
			
			private bool m_ascDirection;
			internal CMoneyStringComparer( bool ascDirection )
			{
				m_ascDirection = ascDirection;
			}

			private bool CanBeMoney( string s )
			{
				try
				{
					if( "" == s )
						return false;
					decimal d = decimal.Parse( s, NumberStyles.Currency );
					return true;
				}
				catch
				{
					return false;
				}
			}

			private decimal ToMoney( string s )
			{
				if( "" == s )
					return 0;
				try
				{
					return decimal.Parse( s, NumberStyles.Currency );
				}
				catch
				{
					return 0;
				}
			}

			public int Compare( object x , object y )
			{
				int resNormal = CompareNormalDirection( x, y );
				if( m_ascDirection )
					return resNormal;
				return (-1) * resNormal;
			}

			private int CompareNormalDirection ( object x , object y )
			{
				if( x == y )
					return 0;
				if( null == x )
					return 1; // x < y
				if( null == y )
					return -1; // x > y
				string strX = x.ToString();
				string strY = y.ToString();

				// Give emtpy string smallest value
				if( "" == strX )
					return -1;
				if( "" == strY )
					return 1;

				bool isXMoney = CanBeMoney( strX );
				bool isYMoney = CanBeMoney( strY );
				if( isXMoney )
				{
					if( isYMoney )
					{
						decimal res = ToMoney( strX ) - ToMoney( strY );
						if( 0 == res )
							return 0;
						else
							return res < 0 ? -1 : 1;
					}
					return -1; // x < y meaning (currency < string )
				}
				else // x is not money
				{
					if( isYMoney )
						return 1;
					return strX.CompareTo( strY );
				}
			}
		}
		
		/// <summary>
		/// Capable of sorting mixed types (string and money)
		/// </summary>
		/// <param name="fieldName"></param>
		public void SortRegularRecordsByKey( string fieldName, bool directionAsc )
		{
			int count = m_sortedList.Count;	

			string[] keys = new string[count];
            ICollectionItemBase2[] records = new ICollectionItemBase2[count];
			for( int i = 0; i < count; ++i )
			{
				var r = (ICollectionItemBase2) m_sortedList.GetByIndex( i );
				records[i] = r;
				keys[i] = r.SortValueFor( fieldName );
			}
			
			Array.Sort( keys, records, new CMoneyStringComparer( directionAsc ) );
			
			m_sortedList.Clear();


			double orderVal = 0;
			for( int i = 0; i < count; ++i )
			{
				var r = records[i];
				r.OrderRankValue = orderVal;
				m_sortedList.Add( orderVal, r );
				
				orderVal += 100;
			}			
		}
	
		
		#endregion // PUBLIC METHODS AND PROPS

		
		/// <summary>
		/// Number of special records. Special records are not in the regular sorted list.
		/// </summary>
		public int CountSpecial
		{
			get { return SpecialRecords.Count;	}
		}

		protected abstract ICollectionItemBase2 CreateRegularRawRecord();
	}
	
	
	abstract public class CXmlRecordCollectionNoSpecialType : CXmlRecordCollection
	{
        protected CXmlRecordCollectionNoSpecialType(CPageBase loanData, CAppBase appData, string xmlSchema, string xmlContent)
			: base( loanData, appData, xmlSchema, xmlContent )
		{
		}

		protected override bool IsTypeOfSpecialRecord( Enum recordT )
		{
			return false;
		}

		override protected ArrayList SpecialRecords
		{
			get
			{				
				ArrayList list = new ArrayList( 0 );
				return list;
			}
		}
		override protected void DetachSpecialRecords()
		{
		}
		
		protected override ICollectionItemBase2 AddSpecialRecord( Enum type )
		{
            throw new CBaseException(ErrorMessages.Generic, "AddSpecialRecord is not supported for this collection.");
		}
	}
	
	
}
