﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.CreditReport;
using System.Data;
using System.Xml.Serialization;
using System.Globalization;

namespace DataAccess
{
    [XmlType]
    public sealed class CTransactionFields
    {
        [XmlIgnoreAttribute]
        public string TransactionD_rep 
        {
            get
            {
                return m_LosConvert.ToDateTimeString(TransactionD);
            }
            set
            {
                m_TransactionD_rep = value;
            }
        }

        [XmlIgnoreAttribute]
        public string TransactionAmt_rep
        {
            get
            {
                return m_LosConvert.ToMoneyString(TransactionAmt, FormatDirection.ToRep);
            }
            set
            {
                m_TransactionAmt_rep = value;
            }
        }

        [XmlElement]
        public string ItemDescription { get; set; }
        [XmlElement]
        public string PmtDir_rep { get; set; }
        [XmlElement]
        public string Entity { get; set; }
        [XmlElement]
        public string TransNum { get; set; }
        [XmlElement]
        public bool PmtMade { get; set; }
        [XmlElement]
        public string Notes { get; set; }
        [XmlElement]
        public int RowNum { get; set; }

        [XmlElement("TransactionD_rep")]
        public string m_TransactionD_rep { get; set; }
        [XmlElement("TransactionAmt_rep")]
        public string m_TransactionAmt_rep { get; set; }


        private LosConvert m_LosConvert { get; set; }

        public CTransactionFields()
        {
            m_LosConvert = new LosConvert();
        }

        public DateTime TransactionD
        {
            get
            {
                if (string.IsNullOrEmpty(m_TransactionD_rep))
                {
                    return DateTime.MinValue;
                }
                else
                {
                    try
                    {
                        return DateTime.Parse(m_TransactionD_rep);
                    }
                    catch (Exception e)
                    {
                        throw new CBaseException("Transaction Date is not a valid date", e);
                    }
                }
            }
        }

        public decimal TransactionAmt
        {
            get
            {
                if (string.IsNullOrEmpty(m_TransactionAmt_rep))
                {
                    return 0;
                }
                else
                {
                    try
                    {
                        return m_LosConvert.ToMoney(m_TransactionAmt_rep);
                    }
                    catch (Exception e)
                    {
                        throw new CBaseException("Transaction Amount is not a valid decimal", e);
                    }
                }
            }
        }

        public void SetConvertLos(LosConvert losConvert)
        {
            this.m_LosConvert = losConvert;
        }
    }
}
