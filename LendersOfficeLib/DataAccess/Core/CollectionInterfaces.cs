﻿namespace DataAccess
{
    using System;
    using System.Collections;
    using System.Data;

    /// <summary>
    /// Interface extracted from CXmlRecordBase.
    /// </summary>
    public interface ICollectionItemBase
    {
        /// <summary>
        /// Gets a value indicating whether the item is valid.
        /// </summary>
        bool IsValid { get; }

        /// <summary>
        /// Gets or sets a type for an enumeration that is used as a key.
        /// </summary>
        Enum KeyType { get; set; }

        /// <summary>
        /// Update the item.
        /// </summary>
        void Update();
    }

    /// <summary>
    /// Interface extracted from CXmlRecordBaseOld.
    /// </summary>
    public interface ICollectionItemBaseOld : ICollectionItemBase
    {
        /// <summary>
        /// Gets the identifier for this item.
        /// </summary>
        Guid RecordId { get; }
    }

    /// <summary>
    /// Interface extracted from CXmlRecordBase2.
    /// </summary>
    public interface ICollectionItemBase2 : ICollectionItemBase
    {
        /// <summary>
        /// Gets or sets a value indicating whether the item has been marked as deleted.
        /// </summary>
        bool IsOnDeathRow { get; set; }

        /// <summary>
        /// Gets or sets a value that can be used to order the items in a collection.
        /// </summary>
        double OrderRankValue { get; set; }

        /// <summary>
        /// Gets the identifier for this item.
        /// </summary>
        Guid RecordId { get; }

        /// <summary>
        /// Gets the position from within a containing collection that this item has.
        /// </summary>
        int RowPos { get; }

        /// <summary>
        /// Gets a DataRow with the same data as this item.
        /// </summary>
        DataRow DataRow { get; }

        /// <summary>
        /// Assign a new identifier to this item.
        /// </summary>
        /// <param name="newId">The new identifier.</param>
        void ChangeRecordId(Guid newId);

        /// <summary>
        /// Move the item down one position in the containing collection.
        /// </summary>
        void MoveDown();

        /// <summary>
        /// Move the item up one position in the containing position.
        /// </summary>
        void MoveUp();

        /// <summary>
        /// Let the item know that it is about to be saved, so any necessary cleanup work can be done.
        /// </summary>
        void PrepareToFlush();

        /// <summary>
        /// Retrieve a value based on the field name.
        /// </summary>
        /// <param name="fieldName">The field name used to locate the value.</param>
        /// <returns>The value associated with the field name.</returns>
        string SortValueFor(string fieldName);
    }

    /// <summary>
    /// Interface extracted from CXmlRecordSubcollection.
    /// </summary>
    public interface ISubcollection : IEnumerable
    {
        /// <summary>
        /// Gets the number of items in the sub collection.
        /// </summary>
        int Count { get; }

        /// <summary>
        /// Retrieve the record located at the input position.
        /// </summary>
        /// <param name="pos">The position for the desired record.</param>
        /// <returns>The record at the position, or throws an out of bounds exception.</returns>
        ICollectionItemBase GetRecordAt(int pos);
    }

    /// <summary>
    /// Interface extracted from CXmlRecordCollection.
    /// </summary>
    public interface IRecordCollection
    {
        /// <summary>
        /// Gets the number of regular records.
        /// </summary>
        int CountRegular { get; }

        /// <summary>
        /// Gets the number of special records.
        /// </summary>
        int CountSpecial { get; }

        /// <summary>
        /// Create and add a new record of the specified type.
        /// </summary>
        /// <param name="recordT">The type of record to create.</param>
        /// <returns>The new record.</returns>
        ICollectionItemBase2 AddRecord(Enum recordT);

        /// <summary>
        /// Create and add a new record.
        /// </summary>
        /// <returns>The new record.</returns>
        ICollectionItemBase2 AddRegularRecord();

        /// <summary>
        /// Create and add a new record at the specified position.
        /// </summary>
        /// <param name="pos">The position into which the record should be inserted.</param>
        /// <returns>The new record.</returns>
        ICollectionItemBase2 AddRegularRecordAt(int pos);

        /// <summary>
        /// Empty the collection.
        /// </summary>
        void ClearAll();

        /// <summary>
        /// Create a data table with the data filtered by the given expression.
        /// </summary>
        /// <param name="strFilterExpression">The filter expression.</param>
        /// <returns>A data table with the requested records.</returns>
        DataTable CloneFilteredTable(string strFilterExpression);

        /// <summary>
        /// If there is no record with the indicated identifier, create one.  Return the record with the indicated identifier.
        /// </summary>
        /// <param name="recordId">The identifier that the returned record should have.</param>
        /// <returns>The record with the specified identifier.</returns>
        ICollectionItemBase2 EnsureRegularRecordOf(Guid recordId);

        /// <summary>
        /// Set a function that will be called during the flush operation.
        /// </summary>
        /// <param name="onFlush">The function that will be called during the flush operation.</param>
        void SetFlushFunction(Action<DataSet> onFlush);

        /// <summary>
        /// Flush the changes and prepare for saving.
        /// </summary>
        void Flush();

        /// <summary>
        /// Get the data in the format of a DataSet.
        /// </summary>
        /// <returns>The data in the format of a DataSet.</returns>
        DataSet GetDataSet();

        /// <summary>
        /// Ensures that the publicly retrievable data set contains the most
        /// up to date data.
        /// </summary>
        void EnsureDataSetUpToDate();

        /// <summary>
        /// Retrieve the record with the specified identifier.
        /// </summary>
        /// <param name="recordId">The identifier for the desired record.</param>
        /// <returns>The desired record, or throw an exception.</returns>
        ICollectionItemBase2 GetRegRecordOf(Guid recordId);

        /// <summary>
        /// Retrieve the regular record at the specified position.
        /// </summary>
        /// <param name="pos">The position.</param>
        /// <returns>The record at the specified position, or throw an out of bounds exception.</returns>
        ICollectionItemBase2 GetRegularRecordAt(int pos);

        /// <summary>
        /// Retrieve the special record at the specified position.
        /// </summary>
        /// <param name="i">The position.</param>
        /// <returns>The record at the specified position, or throw an out of bounds exception.</returns>
        ICollectionItemBase2 GetSpecialRecordAt(int i);

        /// <summary>
        /// Gets a value indicating whether the collection contains a record with the specified identifier.
        /// </summary>
        /// <param name="recordId">The identifier.</param>
        /// <returns>True if the record exists, false otherwise.</returns>
        bool HasRecordOf(Guid recordId);

        /// <summary>
        /// Move a regular record up one spot in the order.
        /// </summary>
        /// <param name="record">The record that is to be moved.</param>
        void MoveRegularRecordUp(ICollectionItemBase2 record);

        /// <summary>
        /// Move a regular record down one spot in the order.
        /// </summary>
        /// <param name="record">The record that is to be moved.</param>
        void MoveRegularRecordDown(ICollectionItemBase2 record);

        /// <summary>
        /// Sort the records according to the specifed field name and direction.
        /// </summary>
        /// <param name="fieldName">The field to use for sorting.</param>
        /// <param name="directionAsc">True if using an ascending sort, false if using a descending sort.</param>
        void SortRegularRecordsByKey(string fieldName, bool directionAsc);
    }
}