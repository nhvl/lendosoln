using System;
using System.Collections.Specialized;
using System.Collections;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data;
using LendersOffice.Common;
using System.Security.Cryptography;
using System.Text;

namespace DataAccess
{
	/// <summary>
	/// Used to create Friendly File Id for PriceMyLoan.com
	/// </summary>
	public class CPmlFIdGenerator
	{
		private RNGCryptoServiceProvider m_rng;	// MSDN: Any public static members of this type are safe for multithreaded operations. Any instance members are not guaranteed to be thread safe.
		static private Hashtable s_convTable;	// MSDN: multi-thread safe for one writer and multiple readers.

		static CPmlFIdGenerator()
		{
			Hashtable convTable	= new Hashtable( 23 );
			//To make it friendly I'm going to skip 
			//0, O, Q
			//U, V, 
			//5, S,
			//I, 1,
			//2, Z
			//B, 8
			//they look similar making it user unfriendly.
			// 23^11 = 952809757913927 combinations
			convTable[0] = '3';
			convTable[1] = '4';
			convTable[2] = '6';
			convTable[3] = '7';
			convTable[4] = '9';
			convTable[5] = 'A';
			convTable[6] = 'C';
			convTable[7] = 'D';
			convTable[8] = 'E';
			convTable[9] = 'F';
			convTable[10] = 'G';
			convTable[11] = 'H';
			convTable[12] = 'J';
			convTable[13] = 'K';
			convTable[14] = 'L';
			convTable[15] = 'M';
			convTable[16] = 'N'; 
			convTable[17] = 'P';
			convTable[18] = 'R';
			convTable[19] = 'T';
			convTable[20] = 'W'; 
			convTable[21] = 'X';
			convTable[22] = 'Y';

			s_convTable = convTable;
		}


		public CPmlFIdGenerator()
		{
			m_rng = new RNGCryptoServiceProvider();
		}
		private static char FromByteToFriendlyChar( byte Byte )
		{
			return (char) s_convTable[ Byte % s_convTable.Count ]; // This won't make an even distribution but should be good enough.
		}
		public string GenerateNewFriendlyId()
		{
			byte[] random = new Byte[11];
			//RNGCryptoServiceProvider is an implementation of a random number generator.
			//System.Security.Cryptography.PasswordDeriveBytes pdb;
			
			m_rng.GetBytes(random); // The array is now filled with cryptographically strong random bytes.
			
			//char[] result = new char[11];
			StringBuilder strBuild = new StringBuilder( 11 );
			for( int i = 0; i < 11; ++i )
			{
				if( 3 == i || 7 == i )
					strBuild.Append( '-' );
				strBuild.Append( FromByteToFriendlyChar( random[i] ) );
			}
			return strBuild.ToString();
		}
			


	}
}
			