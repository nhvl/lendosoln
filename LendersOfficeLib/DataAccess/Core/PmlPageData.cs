using System;
using System.Collections;
using System.Data;
using DataAccess;
using LendersOffice.Security;
using System.Threading;
namespace DataAccess
{

    public class CPmlPageData : CPageData
    {
        public CPmlPageData(Guid fileId, string pageName, CSelectStatementProvider selectProvider)
            :base( fileId)
        {
            AbstractUserPrincipal principal = Thread.CurrentPrincipal as AbstractUserPrincipal;
            if (Tools.IsLoanAHELOC(principal, fileId))
            {
                m_pageBase = new CPmlBaseHelocPage(fileId, pageName, selectProvider);
            }
            else
            {


                m_pageBase = new CPmlBasePage(fileId, pageName, selectProvider);
            }
            m_pageBase.InternalCPageData = this;
        }
    }

	public class CPmlBasePage:  CPageBaseWrapped
	{

        public CPmlBasePage(Guid fileId, string pageName, CSelectStatementProvider selectProvider)
            : base(fileId, pageName, selectProvider)
		{
		}

		override public		void		InitLoad()
		{
			CalcModeT = E_CalcModeT.PriceMyLoan;
			base.InitLoad();
		}

		override public		void		InitSave(int expectedVersion)
		{
			CalcModeT = E_CalcModeT.PriceMyLoan;
            base.InitSave(expectedVersion);
		}
		
		override public		void		Save()
		{
			CalcModeT = E_CalcModeT.PriceMyLoan;
			base.Save();
		}
	}
    public class CPmlBaseHelocPage : CPageHelocBase
    {

        public CPmlBaseHelocPage(Guid fileId, string pageName, CSelectStatementProvider selectProvider)
            : base(fileId, pageName, selectProvider)
        {
        }

        override public void InitLoad()
        {
            CalcModeT = E_CalcModeT.PriceMyLoan;
            base.InitLoad();
        }

        override public void InitSave(int expectedVersion)
        {
            CalcModeT = E_CalcModeT.PriceMyLoan;
            base.InitSave(expectedVersion);
        }

        override public void Save()
        {
            CalcModeT = E_CalcModeT.PriceMyLoan;
            base.Save();
        }
    }

}