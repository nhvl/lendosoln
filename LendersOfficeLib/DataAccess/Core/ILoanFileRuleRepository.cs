namespace DataAccess
{
    using System.Collections.Generic;

    /// <summary>
    /// Provides an interface for retrieving write and protect
    /// field rules for a loan file.
    /// </summary>
    public interface ILoanFileRuleRepository
    {
        /// <summary>
        /// Gets a value indicating whether the user has full
        /// write access to the loan file.
        /// </summary>
        /// <value>
        /// True if the user has full write access to the loan,
        /// false otherwise.
        /// </value>
        bool HasFullWrite { get; }

        /// <summary>
        /// Obtains the list of field protect rules.
        /// </summary>
        /// <returns>
        /// The list of protect field rules.
        /// </returns>
        IEnumerable<ProtectFieldRule> GetProtectFieldRules();

        /// <summary>
        /// Obtains the list of field write rules.
        /// </summary>
        /// <returns>
        /// The list of field write rules.
        /// </returns>
        IEnumerable<WriteFieldRule> GetWriteFieldRules();

        /// <summary>
        /// Checks if loan status can be changed to passed in status.
        /// </summary>
        /// <param name="newStatus">Status you want to set.</param>
        /// <returns>True, if change to passed is status value is allowed. False otherwise.</returns>
        bool CanChangeLoanStatus(E_sStatusT newStatus);
    }
}