﻿/// <copyright file="ILoanFileFieldValidator.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   8/12/2016
/// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using CommonProjectLib.Common.Lib;

    /// <summary>
    /// Provides an interface for loan file field validators.
    /// </summary>
    public interface ILoanFileFieldValidator : ILoanFileRuleRepository
    {
        /// <summary>
        /// Gets a value indicating whether the validator permits
        /// saving.
        /// </summary>
        /// <value>
        /// True if the validator permits saving, false otherwise.
        /// </value>
        bool CanSave { get; }

        /// <summary>
        /// Gets or sets a value indicating whether the validator requires
        /// collections to be checked.
        /// </summary>
        /// <value>
        /// True if the validator required collections to be checked, 
        /// false otherwise.
        /// </value>
        bool CheckCollection { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the validator is disabled.
        /// </summary>
        /// <value>
        /// True if the validator is disabled, false otherwise.
        /// </value>
        bool IsDisabled { get; set; }

        /// <summary>
        /// Gets a value indicating whether the validator is currently
        /// performing validation.
        /// </summary>
        /// <value>
        /// True if the validator is currently performing validation, 
        /// false otherwise.
        /// </value>
        bool IsDoingValidation { get; }

        /// <summary>
        /// Gets a value indicating whether the user has field write permissions.
        /// </summary>
        /// <value>
        /// True if the user has field write permissions, false otherwise.
        /// </value>
        bool HasFieldWritePermissions { get; }

        /// <summary>
        /// Obtains the <see cref="WriteFieldRule"/> for the specified 
        /// <paramref name="fieldId"/>.
        /// </summary>
        /// <param name="fieldId">
        /// The id of the field.
        /// </param>
        /// <returns>
        /// The <see cref="WriteFieldRule"/> corresponding to the field id.
        /// </returns>
        WriteFieldRule GetWriteFieldRule(string fieldId);

        /// <summary>
        /// Obtains a list of <see cref="ProtectFieldRule"/> instances
        /// from the specified <paramref name="fieldId"/>.
        /// </summary>
        /// <param name="fieldId">
        /// The id of the field.
        /// </param>
        /// <returns>
        /// An <see cref="IEnumerable{T}"/> containing the rules.
        /// </returns>
        IEnumerable<ProtectFieldRule> GetProtectionFieldRule(string fieldId);

        /// <summary>
        /// Checks protected fields against a list of changes.
        /// </summary>
        /// <param name="fieldChanges">
        /// The <see cref="IEnumerable{T}"/> containing the list of changes.
        /// </param>
        void CheckProtectedFields(IEnumerable<Tuple<string, FieldOriginalCurrentItem>> fieldChanges);

        /// <summary>
        /// Obtains the <see cref="LoanFieldWritePermissionDenied"/> exception
        /// based on the result of validation, if any.
        /// </summary>
        /// <returns>
        /// The exception encountered when performing validation.
        /// </returns>
        LoanFieldWritePermissionDenied GetException();

        /// <summary>
        /// Obtains the list of protected fields for the validator.
        /// </summary>
        /// <returns>
        /// An <see cref="IEnumerable{T}"/> containing the list of fields.
        /// </returns>
        IEnumerable<string> GetProtectedFields();

        /// <summary>
        /// Attempts to write a new value to the specified <paramref name="fieldId"/>.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the field.
        /// </typeparam>
        /// <param name="fieldId">
        /// The id of the field.
        /// </param>
        /// <param name="getter">
        /// The getter for the field.
        /// </param>
        /// <param name="setter">
        /// The setter for the field.
        /// </param>
        /// <param name="newValue">
        /// The new value to be stored for the field.
        /// </param>
        void TryToWriteField<T>(string fieldId, Func<T> getter, Action<T> setter, T newValue);

        /// <summary>
        /// Attempts to write a new value to the specified <paramref name="fieldId"/>. A
        /// parameter specified the <see cref="IEqualityComparer{T}"/> to use.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the field.
        /// </typeparam>
        /// <param name="fieldId">
        /// The id of the field.
        /// </param>
        /// <param name="getter">
        /// The getter for the field.
        /// </param>
        /// <param name="setter">
        /// The setter for the field.
        /// </param>
        /// <param name="newValue">
        /// The new value to be stored for the field.
        /// </param>
        /// <param name="comparer">
        /// The <see cref="IEqualityComparer{T}"/> to use when determining if the field
        /// value has changed.
        /// </param>
        void TryToWriteField<T>(string fieldId, Func<T> getter, Action<T> setter, T newValue, IEqualityComparer<T> comparer);

        /// <summary>
        /// Validates the update to the specified <paramref name="originalDataSet"/>.
        /// </summary>
        /// <param name="collectionName">
        /// The name of the collection.
        /// </param>
        /// <param name="originalDataSet">
        /// The original values for the collection.
        /// </param>
        /// <param name="updatedDataSet">
        /// The new values for the collection.
        /// </param>
        void UpdateCollection(string collectionName, DataSet originalDataSet, DataSet updatedDataSet);

        /// <summary>
        /// Validates the updated values for the closing cost set json.
        /// </summary>
        /// <param name="brokerId">
        /// The Broker ID.
        /// </param>
        /// <param name="collectionName">
        /// The name of the collection.
        /// </param>
        /// <param name="originalJson">
        /// The original content for the collection.
        /// </param>
        /// <param name="currentJson">
        /// The updated content for the collection.
        /// </param>
        void ValidateClosingCostSet(Guid brokerId, string collectionName, string originalJson, string currentJson);

        /// <summary>
        /// Validates the updated values for the closing disclosure date json.
        /// </summary>
        /// <param name="brokerId">
        /// The Broker ID.
        /// </param>
        /// <param name="originalJson">
        /// The original content for the collection.
        /// </param>
        /// <param name="currentJson">
        /// The updated content for the collection.
        /// </param>
        void ValidateClosingDisclosureDates(Guid brokerId, string originalJson, string currentJson);

        /// <summary>
        /// Validates the updated values for the loan estimate date json.
        /// </summary>
        /// <param name="brokerId">
        /// The Broker ID.
        /// </param>
        /// <param name="originalJson">
        /// The original content for the collection.
        /// </param>
        /// <param name="currentJson">
        /// The updated content for the collection.
        /// </param>
        void ValidateLoanDateInfos(Guid brokerId, string originalJson, string currentJson);

        /// <summary>
        /// Validates the updated values for the housing expenses json.
        /// </summary>
        /// <param name="originalJson">
        /// The original content for the collection.
        /// </param>
        /// <param name="currentJson">
        /// The updated content for the collection.
        /// </param>
        void ValidateHousingExpenses(string originalJson, string currentJson);

        /// <summary>
        /// Validates the changes to the selected product code filter.
        /// </summary>
        /// <param name="collectionName">The collection name.</param>
        /// <param name="originalCodes">The original codes.</param>
        /// <param name="currentCodes">The new codes.</param>
        void ValidateProductCodeFilters(string collectionName, Dictionary<string, bool> originalCodes, Dictionary<string, bool> currentCodes);
    }
}