using System;
using System.Web;
using LendersOffice.Common;

namespace DataAccess
{
	/// <summary>
    /// 12/13/2004 dd - This class only contains the most basic stuff that I and Thinh discussed.
    /// The three essential informations are 'UserMessage', 'ErrorReferenceNumber', and 'DeveloperMessage'.
    /// 
	/// </summary>
	public class CBaseException : Exception
	{
		//soon too be absolete booleans
        private bool m_isEmailDeveloper = true;

        public static CBaseException GenericException(string devMessage)
        {
            return new CBaseException(ErrorMessages.Generic, devMessage);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CBaseException"/> class.
        /// </summary>
        /// <param name="userMessage">Choose one from LendersOffice.Common.ErrorMessages, create new one there if needed</param>
        /// <param name="developerMessage">Do not pass empty string</param>
        public CBaseException(string userMessage, string developerMessage) : base(userMessage)
        {
            Initialize(userMessage, developerMessage);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CBaseException"/> class.
        /// </summary>
        /// <param name="userMessage">Choose one from LendersOffice.Common.ErrorMessages</param>
        /// <param name="innerException">Do not pass empty string</param>
        /// 

        public CBaseException(string userMessage, Exception innerException) : base(userMessage, innerException)
        {
            string exceptionString = innerException.ToString();
            if (innerException is HttpException && exceptionString.Contains("The error code is "))
            {
                HttpException ex = innerException as HttpException;
                exceptionString = exceptionString.Replace("The error code is ", "The error code is " + ex.ErrorCode.ToString() + " - ");
            }
            Initialize(userMessage, exceptionString);
        }

        public virtual string EmailSubjectCode 
        {
            get { return ""; }
        }

        public override string Message 
        {
            get { return ToString(); }
        }

        public virtual bool IsEmailDeveloper 
        {
            get { return m_isEmailDeveloper; }
            set { m_isEmailDeveloper = value; }
        }

        /// <summary>
        /// This is the message that will display to user in error page. This message should be informative and not too technical.
        /// </summary>
        public string UserMessage { get; set; }

        /// <summary>
        /// This number will display on error screen and PB Log. This will help us keep track error message experience by user with
        /// error generate from PB Log.
        /// </summary>
        public string ErrorReferenceNumber { get; private set; }

        /// <summary>
        /// This message should include technical on why error failed, stack trace that will help developer to debug the problem.
        /// </summary>
        public string DeveloperMessage { get; set; }

        /// <summary>
        /// Gets a value indicating whether this exception is a workflow exception.
        /// </summary>
        /// <returns>True if the error is a workflow exception.</returns>
        public bool IsWorkflowError
        {
            get
            {
                return
                    this is LoanFieldWritePermissionDenied
                    || this is PageDataAccessDenied
                    || this is PageDataSaveDenied
                    || this is PageDataLoadDenied
                    || this is DeleteApplicationPermissionDenied
                    || this is DeleteSpousePermissionDenied
                    || this is LoanDeletedPermissionDenied
                    || this is SwapBorrowerAndSpousePermissionDenied;
            }
        }

		//intializes all the variables
        private void Initialize(string userMessage, string developerMessage) 
        {
            this.UserMessage = userMessage;
            this.DeveloperMessage = developerMessage;

            CPmlFIdGenerator generator = new CPmlFIdGenerator();
            this.ErrorReferenceNumber = generator.GenerateNewFriendlyId();
        }

        public override string ToString() 
        {
            return string.Format("Reference #: {1}{0}{0}UserMessage: {2}{0}{0}DeveloperMessage: {3}", Environment.NewLine, this.ErrorReferenceNumber, this.UserMessage, this.DeveloperMessage);
        }


        static public string GetVerboseMessage( Exception exc )
        {
            CBaseException baseExc = exc as CBaseException;
            return (baseExc != null) ? baseExc.DeveloperMessage : exc.Message;
        }
	}

}
