namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using LendersOffice.Admin;
    using LendersOffice.ObjLib.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// We must keep the implementation of this class as a "lean-mean-file-creating routine" at
    /// all time.  Anything that is not about creating a core loan file must be done out side
    /// of this class to avoid locking up db tables.  Talk to Thinh before modifying anything here.
    /// </summary>

    sealed internal class CFileCreatorCore
	{
        static public CFileCreatorCore GetCreator(Guid idBroker, Guid idBranch, Guid idUser, Guid idEmployee, string nmLogin)
        {
            return new CFileCreatorCore(idBroker, idBranch, idUser, idEmployee, nmLogin);
        }

        private CFileCreatorCore(Guid idBroker, Guid idBranch, Guid idUser, Guid idEmployee, string nmLogin)
        {
            this.BrokerId = idBroker;
            this.BranchId = idBranch;
            this.EmployeeId = idEmployee;
            this.UserId = idUser;
            this.LoginName = nmLogin;
            this.CreatedOn = DateTime.Now;
        }

		public string LoanName { get; private set;}

        internal void SetLoanName(string sLNm)
        {
            // 8/14/2013 dd - Allow loan name to be change from the default generated name.
            // Example: FirstTech will use their reserve loan number instead of our name.
            this.LoanName = sLNm;

        }

		public string LoginName { get; private set; }

		public Guid BrokerId { get; private set; }

		public Guid BranchId { get; private set; }

		public Guid EmployeeId { get; private set; }

		public Guid UserId { get; private set; }

		public Guid FileId { get; private set; }

		public Guid SourceFileId { get; private set; }

		public DateTime CreatedOn { get; private set; }

        /// <summary>
        /// Currently any creation from template where CreateDummyLoanNm is true is considered a dummy loan. <para></para>
        /// QuickPricer 2.0 Loans are dummy loans (until they are converted.) <para></para>
        /// opm 185732. <para></para>
        /// </summary>
        public bool IsDummyLoan { get; private set; }

        // OPM 28343 - entries in defaultValues must have the database field as their key
        /**
         * @param defaultValues - Keys in this IDictionary object must be the database field name.
         * Any entries in the defaultValues parameter must also be added to the CreateBlankLoanFile stored procedure.
         * */
        public Guid BeginCreateFile(
            bool isTemplate, 
            Guid branchIdToUse, 
            string loanNameToUse, 
            string sMersMin, 
            string sLRefNm, 
            EncryptionKeyIdentifier encryptionKey,
            EncryptionMigrationVersion encryptionMigrationVersion,
            IDictionary<string, object> defaultValues)
		{
            CPmlFIdGenerator idGenerator = new CPmlFIdGenerator();

			List<SqlParameter>   parameters = new List<SqlParameter>();

			parameters.Add(new SqlParameter( "@EmployeeID" ,  this.EmployeeId ));
			parameters.Add(new SqlParameter( "@BranchID"   , branchIdToUse ));
			parameters.Add(new SqlParameter( "@sLNm"       , loanNameToUse ));
            parameters.Add(new SqlParameter("@sMersMin", sMersMin));
            parameters.Add(new SqlParameter("@sLRefNm", sLRefNm));
            parameters.Add(new SqlParameter("@sEncryptionKey", encryptionKey.ToString()));
            parameters.Add(new SqlParameter("@sEncryptionMigrationVersion", encryptionMigrationVersion));
            parameters.Add(new SqlParameter("@sTotalScoreUniqueLoanId", idGenerator.GenerateNewFriendlyId())); // 10/15/2009 dd - FHA TOTAL Scorecard require id to be unique across all lenders.

			if( isTemplate )
			{
				parameters.Add(new SqlParameter("@IsTemplate", true));
			}

            if (defaultValues != null)
            {
                foreach (KeyValuePair<string, object> entry in defaultValues)
                {
                    parameters.Add(new SqlParameter("@" + entry.Key, entry.Value));
                }
            }

			SqlParameter loanIDParam = new SqlParameter("@LoanID", Guid.Empty);

			loanIDParam.Direction = ParameterDirection.Output;

			parameters.Add(loanIDParam);

            if (StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, "CreateBlankLoanFile", 0, parameters) > 0)
            {
                this.FileId = (Guid)loanIDParam.Value;
                this.BranchId = branchIdToUse;
                this.LoanName = loanNameToUse;
            }

			return this.FileId;
		}

        /**
         * @param defaultValues - Keys in this IDictionary object must be the database field name.
         * Any entries in the defaultValues parameter must also be added to the DuplicateLoanFile stored procedure.
         * */
        public Guid BeginCreateFileFromTemplate(
            bool bMakeTemplate, 
            Guid branchIdToUse, 
            string loanNameToUse, 
            Guid sourceFileId,
            string sMersMin, 
            string sLRefNm,
            EncryptionKeyIdentifier encryptionKey,
            EncryptionMigrationVersion encryptionMigrationVersion,
            IDictionary<string, object> defaultValues, 
            bool createDummyLoanNm, 
            bool sIsLead)
        {
            CPmlFIdGenerator idGenerator = new CPmlFIdGenerator();

			List<SqlParameter>   parameters = new List<SqlParameter>();

			this.SourceFileId = sourceFileId;


            parameters.Add(new SqlParameter("@SrcLoanID", sourceFileId ) );
			parameters.Add(new SqlParameter("@MakeTemplate", bMakeTemplate ) );

			parameters.Add(new SqlParameter( "@EmployeeID" ,  this.EmployeeId ));
			parameters.Add(new SqlParameter( "@BranchID"   , branchIdToUse ));
			parameters.Add(new SqlParameter( "@sLNm"       , loanNameToUse ));
            parameters.Add(new SqlParameter( "@sMersMin", sMersMin));
            parameters.Add(new SqlParameter( "@sLRefNm", sLRefNm));
            parameters.Add(new SqlParameter("@sEncryptionKey", encryptionKey.ToString()));
            parameters.Add(new SqlParameter("@sEncryptionMigrationVersion", encryptionMigrationVersion));
            parameters.Add(new SqlParameter( "@sTotalScoreUniqueLoanId", idGenerator.GenerateNewFriendlyId())); // 10/15/2009 dd - FHA TOTAL Scorecard require id to be unique across all lenders.
            parameters.Add(new SqlParameter( "@sIsLead", sIsLead));

            if (defaultValues != null)
            {
                foreach (KeyValuePair<string, object> entry in defaultValues)
                {
                    parameters.Add(new SqlParameter("@" + entry.Key, entry.Value));
                }
            }

			SqlParameter loanIDParam = new SqlParameter( "@LoanID" , Guid.Empty );
			loanIDParam.Direction = ParameterDirection.Output;
            SqlParameter createdLoanNameParam = new SqlParameter("@CreatedLoanName", SqlDbType.VarChar, 36);
            createdLoanNameParam.Direction = ParameterDirection.Output;
            
            parameters.Add( loanIDParam );

            if (createDummyLoanNm)
            {
                parameters.Add(new SqlParameter("@CreateDummyLoanNm", createDummyLoanNm));
                parameters.Add(createdLoanNameParam);
            }

            if (StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, "DuplicateLoanFile", 0, parameters) > 0)
            {
                this.FileId = (Guid)loanIDParam.Value;
                this.BranchId = branchIdToUse;
                this.LoanName = (createDummyLoanNm ? (string)createdLoanNameParam.Value : loanNameToUse);
                this.IsDummyLoan = createDummyLoanNm;
            }

			return this.FileId;
		}

        public Guid BeginCreateNewLeadFile(
            Guid branchIdToUse, 
            string loanNameToUse, 
            string sLRefNm,
            EncryptionKeyIdentifier encryptionKey,
            EncryptionMigrationVersion encryptionMigrationVersion,
            IDictionary<string, object> defaultValues)
		{
            CPmlFIdGenerator idGenerator = new CPmlFIdGenerator();

			List<SqlParameter>   parameters = new List<SqlParameter>();

			SqlParameter loanIDParam = new SqlParameter("@LoanID", Guid.Empty);

			loanIDParam.Direction = ParameterDirection.Output;
            
			parameters.Add( new SqlParameter("@EmployeeID" ,  this.EmployeeId ));
			parameters.Add( new SqlParameter("@BranchID"   , branchIdToUse ));
			parameters.Add( new SqlParameter("@sLNm"       , loanNameToUse ));
            parameters.Add( new SqlParameter("@BrokerId", BrokerId));
            parameters.Add( new SqlParameter("@sTotalScoreUniqueLoanId", idGenerator.GenerateNewFriendlyId())); // 10/15/2009 dd - FHA TOTAL Scorecard require id to be unique across all lenders.
            parameters.Add( new SqlParameter("@sUseGFEDataForSCFields", true));
            parameters.Add( new SqlParameter("@sLRefNm", sLRefNm));
            parameters.Add(new SqlParameter("@sEncryptionKey", encryptionKey.ToString()));
            parameters.Add(new SqlParameter("@sEncryptionMigrationVersion", encryptionMigrationVersion));

            // OPM 169207 - Set sIsRequireFeesFromDropDown to true based on broker setting.
            if (BrokerDB.RetrieveById(BrokerId).FeeTypeRequirementT != E_FeeTypeRequirementT.Never)
            {
                parameters.Add(new SqlParameter("@sIsRequireFeesFromDropDown", true));
            }

            if (defaultValues != null)
            {
                foreach (KeyValuePair<string, object> entry in defaultValues)
                {
                    parameters.Add(new SqlParameter("@" + entry.Key, entry.Value));
                }
            }

			parameters.Add(loanIDParam);

            if (StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, "CreateBlankLeadFile", 0, parameters) > 0)
            {
                this.FileId = (Guid)loanIDParam.Value;
                this.BranchId = branchIdToUse;
                this.LoanName = loanNameToUse;
            }

			return this.FileId;
		}

		/// <summary>
		/// Commit new loans and finalize the name.  The loan is presumed
		/// to be invalid in its current state.  Once we leave this call,
		/// the loan should be valid and available -- or we failed and
		/// all commit changes (name counter update, etc) are rolled back.
		/// If the new loan is a duplicate of another loan (not a template),
		/// pass in the loan id of the original loan.  We use this id to
		/// lookup the original's name and then use this name as a base
		/// for our new loan's name.
		/// </summary>
		/// <param name="origLoanId">
		/// Id of original loan.  Pass empty if the loan is novel.  Pass
		/// the id of the original if the new loan is a duplicate.
		/// </param>
		/// <param name="sLoanName">
		/// Prefer this name over other ones when saving.
		/// </param>

		public void CommitFileCreation()
		{
			Exception e = null;
			for( int i = 0; i < 5; ++i )
			{
				using( CStoredProcedureExec exec = new CStoredProcedureExec(this.BrokerId) )
				{
					try
					{
						exec.BeginTransactionForWrite();

						exec.ExecuteNonQuery( "DeclareLoanFileValid"
							, new SqlParameter( "@sLId"    , this.FileId )
							, new SqlParameter( "@IsValid" , true     )
							);

						exec.CommitTransaction();
						return;
					}
					catch( Exception exc )
					{
						try { exec.RollbackTransaction(); } 
						catch{}

                        if (null == e)
                        {
                            e = exc;
                        }
						Tools.LogWarning( string.Format( "Failure for {0}. Retry counter = {1}", this.LoginName, i.ToString() ), exc );

						Tools.SleepAndWakeupRandomlyWithin( 1000, 4500 );
					}
				}
			}
			
			Tools.LogErrorWithCriticalTracking( string.Format( "Failed to commit new loan file for {0} after 5 retries.", this.LoginName ), e );
			throw e;
		}
	}
}