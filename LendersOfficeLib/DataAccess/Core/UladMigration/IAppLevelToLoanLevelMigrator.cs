﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Basic interface for migrating app-level collections to loan-level.
    /// </summary>
    /// <typeparam name="TValueKind">The kind of the new entity.</typeparam>
    internal interface IAppLevelToLoanLevelMigrator<TValueKind>
        where TValueKind : DataObjectKind
    {
        /// <summary>
        /// Migrates the app-level records to the loan-level.
        /// </summary>
        /// <returns>A map of legacy record ids to new record ids.</returns>
        Dictionary<Guid, DataObjectIdentifier<TValueKind, Guid>> MigrateAppLevelRecordsToLoanLevel();
    }
}
