﻿namespace DataAccess
{
    using System;
    using LendersOffice.Common;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Migrates the app-level public records collection to the loan-level collection.
    /// </summary>
    internal class PublicRecordMigrator : AbstractAppRecordCollectionMigrator<IPublicRecordCollection, IPublicRecord, PublicRecord, DataObjectKind.PublicRecord>
    {
        /// <summary>
        /// The consumer id of the borrower.
        /// </summary>
        private DataObjectIdentifier<DataObjectKind.Consumer, Guid> borrowerId;

        /// <summary>
        /// The consumer id of the coborrower.
        /// </summary>
        private DataObjectIdentifier<DataObjectKind.Consumer, Guid>? coborrowerId;

        /// <summary>
        /// The loan collection container.
        /// </summary>
        private ILoanLqbCollectionContainer collectionContainer;

        /// <summary>
        /// Initializes a new instance of the <see cref="PublicRecordMigrator"/> class.
        /// </summary>
        /// <param name="app">The app data.</param>
        /// <param name="collectionContainer">The loan collection container.</param>
        public PublicRecordMigrator(IUladDataLayerMigrationAppDataProvider app, ILoanLqbCollectionContainer collectionContainer)
            : base(app.aPublicRecordCollection)
        {
            this.borrowerId = app.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
            this.coborrowerId = (app.aHasCoborrower ? app.aCConsumerId : default(Guid?))?.ToIdentifier<DataObjectKind.Consumer>();
            this.collectionContainer = collectionContainer;
        }

        /// <summary>
        /// Adds an public record to the loan-level collection with the appropriate ownership.
        /// </summary>
        /// <param name="legacyRecord">The old public record.</param>
        /// <param name="entity">The new public record.</param>
        /// <returns>The identifier of the new loan-level public record.</returns>
        protected override DataObjectIdentifier<DataObjectKind.PublicRecord, Guid> AddEntity(IPublicRecord legacyRecord, PublicRecord entity)
        {
            DataObjectIdentifier<DataObjectKind.Consumer, Guid>? additionalOwnerId;
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> primaryOwnerId = this.GetOwnerId(
                (Ownership)legacyRecord.OwnerT,
                this.borrowerId,
                this.coborrowerId,
                out additionalOwnerId);

            var recordId = this.collectionContainer.Add(primaryOwnerId, entity);

            if (additionalOwnerId.HasValue)
            {
                this.collectionContainer.AddOwnership(additionalOwnerId.Value, recordId);
            }

            return recordId;
        }

        /// <summary>
        /// Creates a new loan-level public record based on the provided app-level public record.
        /// </summary>
        /// <param name="legacyRecord">The app-level public record.</param>
        /// <returns>The new loan-level public record.</returns>
        protected override PublicRecord CreateEntity(IPublicRecord legacyRecord)
        {
            var entity = new PublicRecord();
            entity.AuditTrail = legacyRecord.AuditTrailItems;
            entity.BankruptcyLiabilitiesAmount = Money.Create(legacyRecord.BankruptcyLiabilitiesAmount);
            entity.BkFileDate = this.ToUnzonedDate(legacyRecord.BkFileD);
            entity.CourtName = EntityName.Create(legacyRecord.CourtName);
            entity.DispositionDate = this.ToUnzonedDate(legacyRecord.DispositionD);
            entity.DispositionType = legacyRecord.DispositionT;
            entity.IdFromCreditReport = ThirdPartyIdentifier.Create(legacyRecord.IdFromCreditReport);
            entity.IncludeInPricing = legacyRecord.IncludeInPricing;
            entity.LastEffectiveDate = this.ToUnzonedDate(legacyRecord.LastEffectiveD);
            entity.ReportedDate = this.ToUnzonedDate(legacyRecord.ReportedD);
            entity.Type = legacyRecord.Type;
            return entity;
        }
    }
}
