﻿namespace DataAccess
{
    using System;
    using LendersOffice.Common;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Migrates the app-level VA past loan collection to the loan-level collection.
    /// </summary>
    internal class VaPastLoanMigrator : AbstractAppRecordCollectionMigrator<IVaPastLoanCollection, IVaPastLoan, VaPreviousLoan, DataObjectKind.VaPreviousLoan>
    {
        /// <summary>
        /// The consumer id of the borrower.
        /// </summary>
        private DataObjectIdentifier<DataObjectKind.Consumer, Guid> borrowerId;

        /// <summary>
        /// The loan collection container.
        /// </summary>
        private ILoanLqbCollectionContainer collectionContainer;

        /// <summary>
        /// Initializes a new instance of the <see cref="VaPastLoanMigrator"/> class.
        /// </summary>
        /// <param name="app">The app data.</param>
        /// <param name="collectionContainer">The loan data.</param>
        public VaPastLoanMigrator(IUladDataLayerMigrationAppDataProvider app, ILoanLqbCollectionContainer collectionContainer)
            : base(app.aVaPastLCollection)
        {
            this.borrowerId = app.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
            this.collectionContainer = collectionContainer;
        }

        /// <summary>
        /// Adds an VA past loan to the loan-level collection with the appropriate ownership.
        /// </summary>
        /// <param name="legacyRecord">The old VA past loan record.</param>
        /// <param name="entity">The new VA past loan record.</param>
        /// <returns>The identifier of the new loan-level VA past loan.</returns>
        protected override DataObjectIdentifier<DataObjectKind.VaPreviousLoan, Guid> AddEntity(IVaPastLoan legacyRecord, VaPreviousLoan entity)
        {
            return this.collectionContainer.Add(this.borrowerId, entity);
        }

        /// <summary>
        /// Creates a new loan-level VA past loan based on the provided app-level VA past loan.
        /// </summary>
        /// <param name="legacyRecord">The app-level VA past loan.</param>
        /// <returns>The new loan-level VA past loan.</returns>
        protected override VaPreviousLoan CreateEntity(IVaPastLoan legacyRecord)
        {
            var entity = new VaPreviousLoan();
            entity.City = City.Create(legacyRecord.City);
            entity.CityState = CityState.Create(legacyRecord.CityState);
            entity.DateOfLoan = DescriptionField.Create(legacyRecord.DateOfLoan);
            entity.IsStillOwned = BooleanKleene.Create((byte)legacyRecord.IsStillOwned);
            entity.LoanTypeDesc = DescriptionField.Create(legacyRecord.LoanTypeDesc);
            entity.State = this.StringParser.TryParseUnitedStatesPostalState(legacyRecord.State);
            entity.Zip = this.StringParser.TryParseZipcode(legacyRecord.Zip);
            entity.LoanDate = this.ToUnzonedDate(legacyRecord.LoanD);
            entity.PropertySoldDate = this.ToUnzonedDate(legacyRecord.PropSoldD);
            entity.StreetAddress = StreetAddress.Create(legacyRecord.StAddr);
            entity.VaLoanNumber = VaLoanNumber.Create(legacyRecord.VaLoanNum);
            return entity;
        }
    }
}
