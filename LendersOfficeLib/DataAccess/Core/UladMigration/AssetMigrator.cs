﻿namespace DataAccess
{
    using System;
    using LendersOffice.Common;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Migrates the app-level assets collection to the loan-level assets collection.
    /// </summary>
    internal class AssetMigrator : AbstractAppRecordCollectionMigrator<IAssetCollection, IAsset, Asset, DataObjectKind.Asset>
    {
        /// <summary>
        /// The consumer id of the borrower.
        /// </summary>
        private DataObjectIdentifier<DataObjectKind.Consumer, Guid> borrowerId;

        /// <summary>
        /// The consumer id of the coborrower.
        /// </summary>
        private DataObjectIdentifier<DataObjectKind.Consumer, Guid>? coborrowerId;

        /// <summary>
        /// The collection container for the loan file.
        /// </summary>
        private ILoanLqbCollectionContainer collectionContainer;

        /// <summary>
        /// Initializes a new instance of the <see cref="AssetMigrator"/> class.
        /// </summary>
        /// <param name="app">The application data.</param>
        /// <param name="collectionContainer">The collection container.</param>
        public AssetMigrator(IUladDataLayerMigrationAppDataProvider app, ILoanLqbCollectionContainer collectionContainer)
            : base(app.aAssetCollection)
        {
            this.borrowerId = app.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
            this.coborrowerId = (app.aHasCoborrower ? app.aCConsumerId : default(Guid?))?.ToIdentifier<DataObjectKind.Consumer>();
            this.collectionContainer = collectionContainer;
        }

        /// <summary>
        /// Adds an asset to the loan-level collection with the appropriate ownership.
        /// </summary>
        /// <param name="legacyRecord">The old asset record.</param>
        /// <param name="entity">The new asset record.</param>
        /// <returns>The identifier of the new loan-level asset.</returns>
        protected override DataObjectIdentifier<DataObjectKind.Asset, Guid> AddEntity(IAsset legacyRecord, Asset entity)
        {
            DataObjectIdentifier<DataObjectKind.Consumer, Guid>? additionalOwnerId;
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> primaryOwnerId = this.GetOwnerId(
                (Ownership)legacyRecord.OwnerT,
                this.borrowerId,
                this.coborrowerId,
                out additionalOwnerId);

            var recordId = this.collectionContainer.Add(primaryOwnerId, entity);

            if (additionalOwnerId.HasValue)
            {
                this.collectionContainer.AddOwnership(additionalOwnerId.Value, recordId);
            }

            return recordId;
        }

        /// <summary>
        /// Creates a new loan-level asset based on the provided app-level asset.
        /// </summary>
        /// <param name="legacyRecord">The app-level asset.</param>
        /// <returns>The new loan-level asset.</returns>
        protected override Asset CreateEntity(IAsset legacyRecord)
        {
            var entity = new Asset();

            entity.AssetType = legacyRecord.AssetT;
            entity.Desc = DescriptionField.Create(legacyRecord.Desc);
            entity.IsEmptyCreated = legacyRecord.IsEmptyCreated;
            entity.Phone = this.StringParser.TryParsePhoneNumber(legacyRecord.PhoneNumber);
            entity.Value = Money.Create(legacyRecord.Val);

            var legacyAsRegular = legacyRecord as IAssetRegular;
            if (legacyAsRegular != null)
            {
                if (legacyAsRegular.VerifHasSignature)
                {
                    var newSignatureId = this.StringParser.TryParseDataObjectIdentifier<DataObjectKind.Signature, Guid>(legacyAsRegular.VerifSignatureImgId);
                    entity.SetVerifSignature(
                        legacyAsRegular.VerifSigningEmployeeId.ToIdentifier<DataObjectKind.Employee>(),
                        newSignatureId.Value);
                }

                entity.AccountName = DescriptionField.Create(legacyAsRegular.AccNm);
                entity.AccountNum = BankAccountNumber.Create(legacyAsRegular.AccNum.Value);
                entity.Attention = this.StringParser.TryParseEntityName(legacyAsRegular.Attention);
                entity.City = City.Create(legacyAsRegular.City);
                entity.CompanyName = EntityName.Create(legacyAsRegular.ComNm);
                entity.DepartmentName = DescriptionField.Create(legacyAsRegular.DepartmentName);
                entity.GiftSourceData = legacyAsRegular.GiftSource;
                entity.IsSeeAttachment = legacyAsRegular.IsSeeAttachment;
                entity.OtherTypeDesc = DescriptionField.Create(legacyAsRegular.OtherTypeDesc);
                entity.PrepDate = this.ToUnzonedDate(legacyAsRegular.PrepD);
                entity.State = this.StringParser.TryParseUnitedStatesPostalState(legacyAsRegular.State);
                entity.StreetAddress = StreetAddress.Create(legacyAsRegular.StAddr);
                entity.VerifExpiresDate = this.ToUnzonedDate(legacyAsRegular.VerifExpD);
                entity.VerifRecvDate = this.ToUnzonedDate(legacyAsRegular.VerifRecvD);
                entity.VerifReorderedDate = this.ToUnzonedDate(legacyAsRegular.VerifReorderedD);
                entity.VerifSentDate = this.ToUnzonedDate(legacyAsRegular.VerifSentD);
                entity.Zip = this.StringParser.TryParseZipcode(legacyAsRegular.Zip);
            }

            var legacyAsCashDeposit = legacyRecord as IAssetCashDeposit;
            if (legacyAsCashDeposit != null)
            {
                entity.AssetCashDepositType = legacyAsCashDeposit.AssetCashDepositT;
            }

            var legacyAsLifeInsurance = legacyRecord as IAssetLifeInsurance;
            if (legacyAsLifeInsurance != null)
            {
                entity.FaceValue = Money.Create(legacyAsLifeInsurance.FaceVal);
            }

            return entity;
        }
    }
}
