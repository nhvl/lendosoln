﻿namespace DataAccess
{
    using System;
    using LendersOffice.Common;
    using LendingQB.Core.Data;
    using LendingQB.Core.Mapping;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Migrates the app-level liability collection to the loan-level collection.
    /// </summary>
    internal class LiabilityMigrator : AbstractAppRecordCollectionMigrator<ILiaCollection, ILiability, Liability, DataObjectKind.Liability>
    {
        /// <summary>
        /// The consumer id of the borrower.
        /// </summary>
        private DataObjectIdentifier<DataObjectKind.Consumer, Guid> borrowerId;

        /// <summary>
        /// The consumer id of the coborrower.
        /// </summary>
        private DataObjectIdentifier<DataObjectKind.Consumer, Guid>? coborrowerId;

        /// <summary>
        /// The loan collection container.
        /// </summary>
        private ILoanLqbCollectionContainer collectionContainer;

        /// <summary>
        /// The liability default provider to use when creating new liabilities.
        /// </summary>
        private ILiabilityDefaultsProvider defaultsProvider;

        /// <summary>
        /// A mapper to use when copying the audit trail from the legacy records.
        /// </summary>
        private LiabilityPmlAuditTrailMapper auditTrailMapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="LiabilityMigrator"/> class.
        /// </summary>
        /// <param name="app">The app data.</param>
        /// <param name="collectionContainer">The loan collection container.</param>
        /// <param name="defaultsProvider">The defaults provider to use when creating new liabilities.</param>
        public LiabilityMigrator(IUladDataLayerMigrationAppDataProvider app, ILoanLqbCollectionContainer collectionContainer, ILiabilityDefaultsProvider defaultsProvider)
            : base(app.aLiaCollection)
        {
            this.borrowerId = app.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
            this.coborrowerId = (app.aHasCoborrower ? app.aCConsumerId : default(Guid?))?.ToIdentifier<DataObjectKind.Consumer>();
            this.collectionContainer = collectionContainer;
            this.defaultsProvider = defaultsProvider;
            this.auditTrailMapper = new LiabilityPmlAuditTrailMapper();
        }

        /// <summary>
        /// Adds an liability to the loan-level collection with the appropriate ownership.
        /// </summary>
        /// <param name="legacyRecord">The old liability record.</param>
        /// <param name="entity">The new liability record.</param>
        /// <returns>The identifier of the new loan-level liability.</returns>
        protected override DataObjectIdentifier<DataObjectKind.Liability, Guid> AddEntity(ILiability legacyRecord, Liability entity)
        {
            DataObjectIdentifier<DataObjectKind.Consumer, Guid>? additionalOwnerId;
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> primaryOwnerId = this.GetOwnerId(
                (Ownership)legacyRecord.OwnerT,
                this.borrowerId,
                this.coborrowerId,
                out additionalOwnerId);

            var recordId = this.collectionContainer.Add(primaryOwnerId, entity);

            if (additionalOwnerId.HasValue)
            {
                this.collectionContainer.AddOwnership(additionalOwnerId.Value, recordId);
            }

            return recordId;
        }

        /// <summary>
        /// Creates a new loan-level liability based on the provided app-level liability.
        /// </summary>
        /// <param name="legacyRecord">The app-level liability.</param>
        /// <returns>The new loan-level liability.</returns>
        protected override Liability CreateEntity(ILiability legacyRecord)
        {
            var entity = new Liability(this.defaultsProvider);
            entity.DebtType = legacyRecord.DebtT;
            entity.UsedInRatioData = !legacyRecord.NotUsedInRatio;

            try
            {
                entity.PmlAuditTrail = this.auditTrailMapper.Create(legacyRecord.PmlAuditTrailXmlContent);
            }
            catch (Exception e)
            {
                throw new CBaseException(ErrorMessages.Generic, e);
            }

            entity.Pmt = Money.Create(legacyRecord.Pmt);
            entity.RemainMon = LiabilityRemainingMonths.Create(legacyRecord.RemainMons_rep);
            entity.WillBePdOff = legacyRecord.WillBePdOff;

            var legacyAsRegular = legacyRecord as ILiabilityRegular;
            if (legacyAsRegular != null)
            {
                if (legacyAsRegular.VerifHasSignature)
                {
                    var newSignatureId = this.StringParser.TryParseDataObjectIdentifier<DataObjectKind.Signature, Guid>(legacyAsRegular.VerifSignatureImgId);
                    entity.SetVerifSignature(
                        legacyAsRegular.VerifSigningEmployeeId.ToIdentifier<DataObjectKind.Employee>(),
                        newSignatureId.Value);
                }

                entity.AccountName = DescriptionField.Create(legacyAsRegular.AccNm);
                entity.AccountNum = ThirdPartyIdentifier.Create(legacyAsRegular.AccNum.Value);
                entity.Attention = EntityName.Create(legacyAsRegular.Attention);
                entity.AutoYearMakeAsString = YearAsString.Create(legacyAsRegular.AutoYearMake);
                entity.Bal = Money.Create(legacyAsRegular.Bal);
                entity.CompanyAddress = StreetAddress.Create(legacyAsRegular.ComAddr);
                entity.CompanyCity = City.Create(legacyAsRegular.ComCity);
                entity.CompanyFax = PhoneNumber.Create(legacyAsRegular.ComFax);
                entity.CompanyName = EntityName.Create(legacyAsRegular.ComNm);
                entity.CompanyPhone = PhoneNumber.Create(legacyAsRegular.ComPhone);
                entity.CompanyState = this.StringParser.TryParseUnitedStatesPostalState(legacyAsRegular.ComState);
                entity.CompanyZip = Zipcode.Create(legacyAsRegular.ComZip);
                entity.Desc = DescriptionField.Create(legacyAsRegular.Desc);
                entity.DueInMonAsString = CountString.Create(legacyAsRegular.Due_rep);
                entity.ExcludeFromUw = legacyAsRegular.ExcFromUnderwriting;
                entity.FullyIndexedPitiPmt = legacyAsRegular.FullyIndexedPITIPayment;
                entity.IncludeInBk = legacyAsRegular.IncInBankruptcy;
                entity.IncludeInFc = legacyAsRegular.IncInForeclosure;
                entity.IncludeInReposession = legacyAsRegular.IncInReposession;
                entity.IsForAuto = legacyAsRegular.IsForAuto;
                entity.IsMtgFhaInsured = legacyAsRegular.IsMortFHAInsured;
                entity.IsPiggyBack = legacyAsRegular.IsPiggyBack;
                entity.IsSeeAttachment = legacyAsRegular.IsSeeAttachment;
                entity.IsSp1stMtgData = legacyAsRegular.IsSubjectProperty1stMortgage;
                entity.Late30AsString = CountString.Create(legacyAsRegular.Late30_rep);
                entity.Late60AsString = CountString.Create(legacyAsRegular.Late60_rep);
                entity.Late90PlusAsString = CountString.Create(legacyAsRegular.Late90Plus_rep);
                entity.OriginalDebtAmt = Money.Create(legacyAsRegular.OrigDebtAmt);
                entity.OriginalTermAsString = CountString.Create(legacyAsRegular.OrigTerm_rep);
                entity.PayoffAmtData = Money.Create(legacyAsRegular.PayoffAmt);
                entity.PayoffAmtLocked = legacyAsRegular.PayoffAmtLckd;
                entity.PayoffTimingData = (PayoffTiming)legacyAsRegular.PayoffTiming;
                entity.PayoffTimingLockedData = legacyAsRegular.PayoffTimingLckd;
                entity.PrepDate = this.ToUnzonedDate(legacyAsRegular.PrepD);
                entity.Rate = Percentage.Create(legacyAsRegular.R);
                entity.ReconcileStatusType = legacyAsRegular.ReconcileStatusT;
                entity.VerifExpiresDate = this.ToUnzonedDate(legacyAsRegular.VerifExpD);
                entity.VerifRecvDate = this.ToUnzonedDate(legacyAsRegular.VerifRecvD);
                entity.VerifReorderedDate = this.ToUnzonedDate(legacyAsRegular.VerifReorderedD);
                entity.VerifSentDate = this.ToUnzonedDate(legacyAsRegular.VerifSentD);
            }

            var legacyAsAlimony = legacyRecord as ILiabilityAlimony;
            if (legacyAsAlimony != null)
            {
                entity.OwedTo = PersonName.Create(legacyAsAlimony.OwedTo);
            }

            var legacyAsChildSupport = legacyRecord as ILiabilityChildSupport;
            if (legacyAsChildSupport != null)
            {
                entity.OwedTo = PersonName.Create(legacyAsChildSupport.OwedTo);
            }

            var legacyAsJobExpense = legacyRecord as ILiabilityJobExpense;
            if (legacyAsJobExpense != null)
            {
                entity.JobExpenseDesc = DescriptionField.Create(legacyAsJobExpense.ExpenseDesc);
            }

            return entity;
        }
    }
}
