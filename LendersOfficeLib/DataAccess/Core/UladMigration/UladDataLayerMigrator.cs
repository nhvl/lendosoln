﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Migrates a file onto the ULAD data layer.
    /// </summary>
    internal class UladDataLayerMigrator
    {
        /// <summary>
        /// The loan data.
        /// </summary>
        private IUladDataLayerMigrationLoanDataProvider loan;

        /// <summary>
        /// The loan collection container.
        /// </summary>
        private ILoanLqbCollectionContainer collectionContainer;

        /// <summary>
        /// Initializes a new instance of the <see cref="UladDataLayerMigrator"/> class.
        /// </summary>
        /// <param name="loan">The loan data.</param>
        /// <param name="collectionContainer">The loan collection container.</param>
        public UladDataLayerMigrator(
            IUladDataLayerMigrationLoanDataProvider loan,
            ILoanLqbCollectionContainer collectionContainer)
        {
            this.loan = loan;
            this.collectionContainer = collectionContainer;
        }

        /// <summary>
        /// Migrates app-level collections to loan-level, initializes the ULAD apps, and sets
        /// the flag to use the ULAD data layer.
        /// </summary>
        public void Migrate()
        {
            foreach (var app in this.loan.Apps)
            {
                var assetMigrator = this.GetAssetMigrator(app);
                assetMigrator.MigrateAppLevelRecordsToLoanLevel();

                var borrowerEmploymentMigrator = this.GetEmploymentMigrator(E_BorrowerModeT.Borrower, app);
                borrowerEmploymentMigrator.MigrateAppLevelRecordsToLoanLevel();
                
                var coborrowerEmploymentMigrator = this.GetEmploymentMigrator(E_BorrowerModeT.Coborrower, app);
                coborrowerEmploymentMigrator.MigrateAppLevelRecordsToLoanLevel();

                var publicRecordMigrator = this.GetPublicRecordMigrator(app);
                publicRecordMigrator.MigrateAppLevelRecordsToLoanLevel();

                var pastLoanMigrator = this.GetVaPastLoanMigrator(app);
                pastLoanMigrator.MigrateAppLevelRecordsToLoanLevel();
                
                var vorRecordMigrator = this.GetVorRecordMigrator(app);
                vorRecordMigrator.MigrateAppLevelRecordsToLoanLevel();
                
                var liabilityMigrator = this.GetLiabilityMigrator(app);
                var liabilityMap = liabilityMigrator.MigrateAppLevelRecordsToLoanLevel();
                
                var reoMigrator = this.GetReoMigrator(app);
                var reoMap = reoMigrator.MigrateAppLevelRecordsToLoanLevel();

                this.MigrateRealPropertyLiabilityAssociations(app, reoMap, liabilityMap);
            }

            this.collectionContainer.InitializeUladAppsToMatchLegacyApps();

            this.loan.sBorrowerApplicationCollectionT = E_sLqbCollectionT.UseLqbCollections;
        }
        
        /// <summary>
        /// Gets the asset migrator.
        /// </summary>
        /// <param name="app">The app data.</param>
        /// <returns>The migrator.</returns>
        protected virtual IAppLevelToLoanLevelMigrator<DataObjectKind.Asset> GetAssetMigrator(IUladDataLayerMigrationAppDataProvider app)
        {
            return new AssetMigrator(app, this.collectionContainer);
        }

        /// <summary>
        /// Gets the employment migrator.
        /// </summary>
        /// <param name="borrowerMode">The borrower to retrieve the migrator for.</param>
        /// <param name="app">The app data.</param>
        /// <returns>The migrator.</returns>
        protected virtual IAppLevelToLoanLevelMigrator<DataObjectKind.EmploymentRecord> GetEmploymentMigrator(
            E_BorrowerModeT borrowerMode,
            IUladDataLayerMigrationAppDataProvider app)
        {
            return new EmploymentMigrator(borrowerMode, app, this.collectionContainer, this.loan.CreateEmploymentRecordDefaultsProvider());
        }

        /// <summary>
        /// Gets the liability migrator.
        /// </summary>
        /// <param name="app">The app data.</param>
        /// <returns>The migrator.</returns>
        protected virtual IAppLevelToLoanLevelMigrator<DataObjectKind.Liability> GetLiabilityMigrator(IUladDataLayerMigrationAppDataProvider app)
        {
            var factory = new LiabilityDefaultsFactory();
            var defaultsProvider = factory.Create(this.loan);
            return new LiabilityMigrator(app, this.collectionContainer, defaultsProvider);
        }

        /// <summary>
        /// Gets the public record migrator.
        /// </summary>
        /// <param name="app">The app data.</param>
        /// <returns>The migrator.</returns>
        protected virtual IAppLevelToLoanLevelMigrator<DataObjectKind.PublicRecord> GetPublicRecordMigrator(IUladDataLayerMigrationAppDataProvider app)
        {
            return new PublicRecordMigrator(app, this.collectionContainer);
        }

        /// <summary>
        /// Gets the REO migrator.
        /// </summary>
        /// <param name="app">The app data.</param>
        /// <returns>The migrator.</returns>
        protected virtual IAppLevelToLoanLevelMigrator<DataObjectKind.RealProperty> GetReoMigrator(IUladDataLayerMigrationAppDataProvider app)
        {
            return new ReoMigrator(app, this.collectionContainer);
        }

        /// <summary>
        /// Gets the VA past loan migrator.
        /// </summary>
        /// <param name="app">The app data.</param>
        /// <returns>The migrator.</returns>
        protected virtual IAppLevelToLoanLevelMigrator<DataObjectKind.VaPreviousLoan> GetVaPastLoanMigrator(IUladDataLayerMigrationAppDataProvider app)
        {
            return new VaPastLoanMigrator(app, this.collectionContainer);
        }
        
        /// <summary>
        /// Gets the VOR migrator.
        /// </summary>
        /// <param name="app">The app data.</param>
        /// <returns>The migrator.</returns>
        protected virtual IAppLevelToLoanLevelMigrator<DataObjectKind.VorRecord> GetVorRecordMigrator(IUladDataLayerMigrationAppDataProvider app)
        {
            return new VorRecordMigrator(app, this.collectionContainer);
        }

        /// <summary>
        /// Migrates the associations from the app-level to the loan-level based on the new ids.
        /// </summary>
        /// <param name="app">The app data.</param>
        /// <param name="reoMap">The map from old id to new id for REOs.</param>
        /// <param name="liabilityMap">The map from old id to new id for liabilities.</param>
        private void MigrateRealPropertyLiabilityAssociations(
            IUladDataLayerMigrationAppDataProvider app,
            Dictionary<Guid, DataObjectIdentifier<DataObjectKind.RealProperty, Guid>> reoMap,
            Dictionary<Guid, DataObjectIdentifier<DataObjectKind.Liability, Guid>> liabilityMap)
        {
            var liabilities = app.aLiaCollection;
            var oldLiaIdToOldReoId = new Dictionary<Guid, Guid>();
            for (int i = 0; i < liabilities.CountRegular; ++i)
            {
                var liability = liabilities.GetRegularRecordAt(i);
                if (liability.MatchedReRecordId != Guid.Empty)
                {
                    oldLiaIdToOldReoId.Add(liability.RecordId, liability.MatchedReRecordId);
                }
            }

            foreach (var liaToReo in oldLiaIdToOldReoId)
            {
                var liabilityId = liabilityMap[liaToReo.Key];
                var realPropertyId = reoMap[liaToReo.Value];
                this.collectionContainer.AssociateLiabilityWithRealProperty(liabilityId, realPropertyId);
            }
        }
    }
}
