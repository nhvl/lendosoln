﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LendersOffice.Common;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Migrates the app-level VOR records collection to the loan-level collection.
    /// </summary>
    internal class VorRecordMigrator : IAppLevelToLoanLevelMigrator<DataObjectKind.VorRecord>
    {
        /// <summary>
        /// Legacy record ids that were associated with coborrower housing history.
        /// </summary>
        private static readonly Guid[] CoborrowerSpecialRecordIds = new[]
        {
            new Guid("44444444-4444-4444-4444-444444444444"),
            new Guid("55555555-5555-5555-5555-555555555555"),
            new Guid("66666666-6666-6666-6666-666666666666")
        };

        /// <summary>
        /// The consumer id of the borrower.
        /// </summary>
        private DataObjectIdentifier<DataObjectKind.Consumer, Guid> borrowerId;

        /// <summary>
        ///  The consumer id of the coborrower.
        /// </summary>
        private DataObjectIdentifier<DataObjectKind.Consumer, Guid>? coborrowerId;

        /// <summary>
        /// The legacy VOR record collection.
        /// </summary>
        private IVerificationOfRentCollection collection;

        /// <summary>
        /// The loan collection container.
        /// </summary>
        private ILoanLqbCollectionContainer collectionContainer;

        /// <summary>
        /// The string parser to convert strings to semantic types.
        /// </summary>
        private UladMigratorStringParser stringParser;

        /// <summary>
        /// Initializes a new instance of the <see cref="VorRecordMigrator"/> class.
        /// </summary>
        /// <param name="app">The app data.</param>
        /// <param name="collectionContainer">The loan collection container.</param>
        public VorRecordMigrator(IUladDataLayerMigrationAppDataProvider app, ILoanLqbCollectionContainer collectionContainer)
            : this(app, app.aVorCollection, collectionContainer)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VorRecordMigrator"/> class.
        /// </summary>
        /// <param name="app">The app data.</param>
        /// <param name="collection">The VOR collection to migrate.</param>
        /// <param name="collectionContainer">The loan collection container.</param>
        public VorRecordMigrator(IUladDataLayerMigrationAppDataProvider app, IVerificationOfRentCollection collection, ILoanLqbCollectionContainer collectionContainer)
        {
            this.borrowerId = app.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
            this.coborrowerId = (app.aHasCoborrower ? app.aCConsumerId : default(Guid?))?.ToIdentifier<DataObjectKind.Consumer>();
            this.collection = collection;
            this.collectionContainer = collectionContainer;
            this.stringParser = new UladMigratorStringParser();
        }

        /// <summary>
        /// Migrates the app-level records to the loan-level.
        /// </summary>
        /// <returns>A map from the legacy record ids to the new record ids.</returns>
        public Dictionary<Guid, DataObjectIdentifier<DataObjectKind.VorRecord, Guid>> MigrateAppLevelRecordsToLoanLevel()
        {
            var count = this.collection.Count();
            var idMap = new Dictionary<Guid, DataObjectIdentifier<DataObjectKind.VorRecord, Guid>>(count);
            
            for (int i = 0; i < count; ++i)
            {
                var legacyRecord = this.collection.GetRecordByIndex(i);
                var entity = this.CreateEntity(legacyRecord);
                var entityId = this.AddEntity(legacyRecord, entity);
                idMap.Add(legacyRecord.RecordId, entityId);
            }

            return idMap;
        }

        /// <summary>
        /// Creates a new loan-level VOR record based on the provided app-level VOR record.
        /// </summary>
        /// <param name="legacyRecord">The app-level VOR record.</param>
        /// <returns>The new loan-level VOR record.</returns>
        protected VorRecord CreateEntity(IVerificationOfRent legacyRecord)
        {
            var entity = new VorRecord();

            if (legacyRecord.VerifHasSignature)
            {
                var newSignatureId = this.stringParser.TryParseDataObjectIdentifier<DataObjectKind.Signature, Guid>(legacyRecord.VerifSignatureImgId);
                entity.SetVerifSignature(
                    legacyRecord.VerifSigningEmployeeId.ToIdentifier<DataObjectKind.Employee>(),
                    newSignatureId.Value);
            }

            if (VorRecord.LegacyRecordIdToSpecialType.ContainsKey(legacyRecord.RecordId))
            {
                entity.SetLegacyRecordSpecialType(VorRecord.LegacyRecordIdToSpecialType[legacyRecord.RecordId]);
            }

            entity.AccountName = DescriptionField.Create(legacyRecord.AccountName);
            entity.StreetAddressFor = StreetAddress.Create(legacyRecord.AddressFor);
            entity.StreetAddressTo = StreetAddress.Create(legacyRecord.AddressTo);
            entity.Attention = this.stringParser.TryParseEntityName(legacyRecord.Attention);
            entity.CityFor = City.Create(legacyRecord.CityFor);
            entity.CityTo = City.Create(legacyRecord.CityTo);
            entity.Description = DescriptionField.Create(legacyRecord.Description);
            entity.FaxTo = this.stringParser.TryParsePhoneNumber(legacyRecord.FaxTo);
            entity.IsSeeAttachment = legacyRecord.IsSeeAttachment;
            entity.LandlordCreditorName = EntityName.Create(legacyRecord.LandlordCreditorName);
            entity.PhoneTo = this.stringParser.TryParsePhoneNumber(legacyRecord.PhoneTo);
            entity.PrepDate = this.ToUnzonedDate(legacyRecord.PrepD);
            entity.StateFor = this.stringParser.TryParseUnitedStatesPostalState(legacyRecord.StateFor);
            entity.StateTo = this.stringParser.TryParseUnitedStatesPostalState(legacyRecord.StateTo);
            entity.VerifExpDate = this.ToUnzonedDate(legacyRecord.VerifExpD);
            entity.VerifRecvDate = this.ToUnzonedDate(legacyRecord.VerifRecvD);
            entity.VerifReorderedDate = this.ToUnzonedDate(legacyRecord.VerifReorderedD);
            entity.VerifSentDate = this.ToUnzonedDate(legacyRecord.VerifSentD);
            entity.VerifType = legacyRecord.VerifT;
            entity.ZipFor = this.stringParser.TryParseZipcode(legacyRecord.ZipFor);
            entity.ZipTo = this.stringParser.TryParseZipcode(legacyRecord.ZipTo);
            return entity;
        }

        /// <summary>
        /// Adds an VOR record to the loan-level collection with the appropriate ownership.
        /// </summary>
        /// <param name="legacyRecord">The old VOR record.</param>
        /// <param name="entity">The new VOR record.</param>
        /// <returns>The identifier of the new loan-level VOR record.</returns>
        private DataObjectIdentifier<DataObjectKind.VorRecord, Guid> AddEntity(IVerificationOfRent legacyRecord, VorRecord entity)
        {
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId;

            // This collection did some really bizarre stuff where it associated specific record ids with
            // specific borrowers / addresses. If the record does not have one of the hard-coded coborrower
            // ids, then we can just defualt to borrower.
            if (CoborrowerSpecialRecordIds.Contains(legacyRecord.RecordId))
            {
                if (this.coborrowerId == null)
                {
                    throw CBaseException.GenericException("Expected co-borrower id for special coborrower record: " + legacyRecord.RecordId.ToString());
                }

                consumerId = this.coborrowerId.Value;
            }
            else
            {
                consumerId = this.borrowerId;
            }

            return this.collectionContainer.Add(consumerId, entity);
        }

        /// <summary>
        /// Converts a CDateTime to an UnzonedDate.
        /// </summary>
        /// <param name="dateTime">The CDateTime.</param>
        /// <returns>An UnzonedDate for the given CDateTime.</returns>
        private UnzonedDate? ToUnzonedDate(CDateTime dateTime)
        {
            if (dateTime == null || !dateTime.IsValid)
            {
                return null;
            }

            return UnzonedDate.Create(dateTime.DateTimeForComputation);
        }
    }
}
