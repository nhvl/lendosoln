﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides skeleton for migrating an IRecordCollection.
    /// </summary>
    /// <typeparam name="TCollection">The legacy collection interface.</typeparam>
    /// <typeparam name="TRecord">The legacy record interface.</typeparam>
    /// <typeparam name="TEntity">The entity type.</typeparam>
    /// <typeparam name="TValueKind">The kind of the entity.</typeparam>
    internal abstract class AbstractAppRecordCollectionMigrator<TCollection, TRecord, TEntity, TValueKind> : IAppLevelToLoanLevelMigrator<TValueKind>
        where TCollection : IRecordCollection
        where TRecord : ICollectionItemBase2
        where TValueKind : DataObjectKind
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractAppRecordCollectionMigrator{TCollection, TRecord, TEntity, TValueKind}"/> class.
        /// </summary>
        /// <param name="collection">The legacy collection.</param>
        protected AbstractAppRecordCollectionMigrator(TCollection collection)
        {
            this.Collection = collection;
            this.StringParser = new UladMigratorStringParser();
        }

        /// <summary>
        /// Gets the legacy record collection.
        /// </summary>
        protected TCollection Collection { get; }

        /// <summary>
        /// Gets the parser for converting string values to semantic types.
        /// </summary>
        protected UladMigratorStringParser StringParser { get; }

        /// <summary>
        /// Migrates the app-level records to the loan-level collection.
        /// </summary>
        /// <returns>A map from legacy record ids to new record ids.</returns>
        public Dictionary<Guid, DataObjectIdentifier<TValueKind, Guid>> MigrateAppLevelRecordsToLoanLevel()
        {
            var legacyIdToEntityId = new Dictionary<Guid, DataObjectIdentifier<TValueKind, Guid>>();

            for (int i = 0; i < this.Collection.CountRegular; ++i)
            {
                var legacyRecord = (TRecord)this.Collection.GetRegularRecordAt(i);
                var legacyId = legacyRecord.RecordId;
                var entity = this.CreateEntity(legacyRecord);
                if (entity != null)
                {
                    var entityId = this.AddEntity(legacyRecord, entity);
                    legacyIdToEntityId.Add(legacyId, entityId);
                }
            }

            for (int i = 0; i < this.Collection.CountSpecial; ++i)
            {
                var legacyRecord = (TRecord)this.Collection.GetSpecialRecordAt(i);
                var legacyId = legacyRecord.RecordId;
                var entity = this.CreateEntity(legacyRecord);
                if (entity != null)
                {
                    var entityId = this.AddEntity(legacyRecord, entity);
                    legacyIdToEntityId.Add(legacyId, entityId);
                }
            }

            return legacyIdToEntityId;
        }

        /// <summary>
        /// Converts a CDateTime to an UnzonedDate.
        /// </summary>
        /// <param name="dateTime">The CDateTime.</param>
        /// <returns>An UnzonedDate for the given CDateTime.</returns>
        protected UnzonedDate? ToUnzonedDate(CDateTime dateTime)
        {
            if (dateTime == null || !dateTime.IsValid)
            {
                return null;
            }

            return UnzonedDate.Create(dateTime.DateTimeForComputation);
        }

        /// <summary>
        /// Determines which borrower should be the primary owner and if the coborrower should
        /// be included as an additional owner.
        /// </summary>
        /// <param name="ownership">The ownership of the record.</param>
        /// <param name="borrowerId">The borrower's consumer id.</param>
        /// <param name="coborrowerId">The coborrower's consumer id.</param>
        /// <param name="additionalOwnerId">The consumer id of an extra owner if it exists.</param>
        /// <returns>The primary owner id. The out parameter will be populated for joint ownership.</returns>
        protected DataObjectIdentifier<DataObjectKind.Consumer, Guid> GetOwnerId(
            Ownership ownership,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> borrowerId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid>? coborrowerId,
            out DataObjectIdentifier<DataObjectKind.Consumer, Guid>? additionalOwnerId)
        {
            if (ownership == Ownership.Borrower)
            {
                additionalOwnerId = null;
                return borrowerId;
            }
            else if (ownership == Ownership.Coborrower)
            {
                if (!coborrowerId.HasValue)
                {
                    throw CBaseException.GenericException("Expected co-borrower id for " + ownership + " ownership.");
                }

                additionalOwnerId = null;
                return coborrowerId.Value;
            }
            else if (ownership == Ownership.Joint)
            {
                if (!coborrowerId.HasValue)
                {
                    throw CBaseException.GenericException("Expected co-borrower id for " + ownership + " ownership.");
                }

                additionalOwnerId = coborrowerId.Value;
                return borrowerId;
            }
            else
            {
                throw new UnhandledEnumException(ownership);
            }
        }

        /// <summary>
        /// Creates a new loan-level entity based on the provided app-level entity. Return null to skip the legacy record.
        /// </summary>
        /// <param name="legacyRecord">The app-level entity.</param>
        /// <returns>The new loan-level entity or null if nothing should be added for the legacy record.</returns>
        protected abstract TEntity CreateEntity(TRecord legacyRecord);

        /// <summary>
        /// Adds an entity to the loan-level collection with the appropriate ownership.
        /// </summary>
        /// <param name="legacyRecord">The old entity record.</param>
        /// <param name="entity">The new entity record.</param>
        /// <returns>The identifier of the new loan-level entity.</returns>
        protected abstract DataObjectIdentifier<TValueKind, Guid> AddEntity(TRecord legacyRecord, TEntity entity);
    }
}
