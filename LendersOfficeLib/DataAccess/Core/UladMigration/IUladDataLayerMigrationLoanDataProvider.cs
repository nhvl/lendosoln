﻿namespace DataAccess
{
    using System.Collections.Generic;
    using LendingQB.Core.Data;

    /// <summary>
    /// The loan data needed by the ULAD data layer migrators.
    /// </summary>
    /// <remarks>
    /// This extends the collection container interface because that is dependend on by the liability
    /// defaults provider. We may want to reduce the number of fields pulled in here since the liability
    /// only care about a single field.
    /// </remarks>
    public interface IUladDataLayerMigrationLoanDataProvider : ILoanLqbCollectionContainerLoanDataProvider
    {
        /// <summary>
        /// Gets or sets the borrower application collection type. This is the field that indicates whether the
        /// ULAD data layer should be used.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        E_sLqbCollectionT sBorrowerApplicationCollectionT { get; set; }

        /// <summary>
        /// Gets the applications on the file.
        /// </summary>
        IEnumerable<IUladDataLayerMigrationAppDataProvider> Apps { get; }

        /// <summary>
        /// Creates a defaults provider for an employment record.
        /// </summary>
        /// <returns>A defaults provider for an employment record.</returns>
        IEmploymentRecordDefaultsProvider CreateEmploymentRecordDefaultsProvider();
    }
}
