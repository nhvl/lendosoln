﻿namespace DataAccess
{
    using System;

    /// <summary>
    /// The app data needed by the ULAD data layer migrators.
    /// </summary>
    public interface IUladDataLayerMigrationAppDataProvider
    {
        /// <summary>
        /// Gets the app id.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        Guid aAppId { get; }

        /// <summary>
        /// Gets the consumer id of the borrower.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        Guid aBConsumerId { get; }

        /// <summary>
        /// Gets the consumer id of the co-borrower.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        Guid aCConsumerId { get; }

        /// <summary>
        /// Gets a value indicating whether the legacy application has a co-borrower.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        bool aHasCoborrower { get; }

        /// <summary>
        /// Gets the asset collection.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        IAssetCollection aAssetCollection { get; }

        /// <summary>
        /// Gets the borrower employment collection.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        IEmpCollection aBEmpCollection { get; }

        /// <summary>
        /// Gets the co-borrower employment collection.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        IEmpCollection aCEmpCollection { get; }

        /// <summary>
        /// Gets the liability collection.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        ILiaCollection aLiaCollection { get; }

        /// <summary>
        /// Gets the public record collection.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        IPublicRecordCollection aPublicRecordCollection { get; }

        /// <summary>
        /// Gets the REO collection.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        IReCollection aReCollection { get; }

        /// <summary>
        /// Gets the VA past loan collection.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        IVaPastLoanCollection aVaPastLCollection { get; }

        /// <summary>
        /// Gets the VOR collection.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        IVerificationOfRentCollection aVorCollection { get; }
    }
}
