﻿namespace DataAccess
{
    using System;
    using LendersOffice.Common;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Migrates the app-level employment collection to the loan-level employment collection.
    /// </summary>
    internal class EmploymentMigrator : AbstractAppRecordCollectionMigrator<IEmpCollection, IEmploymentRecord, EmploymentRecord, DataObjectKind.EmploymentRecord>
    {
        /// <summary>
        /// Gets the borrower mode for the migrator.
        /// </summary>
        private readonly E_BorrowerModeT borrowerMode;

        /// <summary>
        /// Gets a value indicating whether the coborrower is defined on the application.
        /// </summary>
        private readonly bool hasCoborrower;

        /// <summary>
        /// The consumer id of the borrower.
        /// </summary>
        private readonly Lazy<DataObjectIdentifier<DataObjectKind.Consumer, Guid>> lazyConsumerId;

        /// <summary>
        /// The collection container of the loan.
        /// </summary>
        private ILoanLqbCollectionContainer collectionContainer;

        /// <summary>
        /// The defaults provider for employment records.
        /// </summary>
        private IEmploymentRecordDefaultsProvider defaultsProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmploymentMigrator"/> class.
        /// </summary>
        /// <param name="borrower">The borrower mode.</param>
        /// <param name="app">The app data.</param>
        /// <param name="collectionContainer">The loan collection container.</param>
        /// <param name="defaultsProvider">The employment record defaults.</param>
        public EmploymentMigrator(E_BorrowerModeT borrower, IUladDataLayerMigrationAppDataProvider app, ILoanLqbCollectionContainer collectionContainer, IEmploymentRecordDefaultsProvider defaultsProvider)
            : base(borrower == E_BorrowerModeT.Borrower ? app.aBEmpCollection : app.aCEmpCollection)
        {
            this.borrowerMode = borrower;
            this.hasCoborrower = app.aHasCoborrower;
            Guid borrConsumerId = app.aBConsumerId;
            Guid coboConsumerId = app.aCConsumerId;
            this.lazyConsumerId = new Lazy<DataObjectIdentifier<DataObjectKind.Consumer, Guid>>(() =>
            {
                if (!this.hasCoborrower && this.borrowerMode == E_BorrowerModeT.Coborrower)
                {
                    throw CBaseException.GenericException("Expected a valid co-borrower for co-borrower employment.");
                }

                return this.borrowerMode == E_BorrowerModeT.Borrower
                    ? borrConsumerId.ToIdentifier<DataObjectKind.Consumer>()
                    : coboConsumerId.ToIdentifier<DataObjectKind.Consumer>();
            });

            this.collectionContainer = collectionContainer;
            this.defaultsProvider = defaultsProvider;
        }

        /// <summary>
        /// Adds an employment record to the loan-level collection.
        /// </summary>
        /// <param name="legacyRecord">The old employment record.</param>
        /// <param name="entity">The new employment record.</param>
        /// <returns>The identifier of the new loan-level employment record.</returns>
        protected override DataObjectIdentifier<DataObjectKind.EmploymentRecord, Guid> AddEntity(IEmploymentRecord legacyRecord, EmploymentRecord entity)
        {
            return this.collectionContainer.Add(this.lazyConsumerId.Value, entity);
        }

        /// <summary>
        /// Creates a new loan-level employment record based on the provided app-level employment record.
        /// </summary>
        /// <param name="legacyRecord">The app-level employment record.</param>
        /// <returns>The new loan-level employment record.</returns>
        protected override EmploymentRecord CreateEntity(IEmploymentRecord legacyRecord)
        {
            if (this.borrowerMode == E_BorrowerModeT.Coborrower
                && !this.hasCoborrower
                && ((CEmpRec)legacyRecord).IsEmpty)
            {
                // Ignore empty coborrower records if the coborrower
                // is not defined. OPM 476897.
                return null;
            }

            var entity = new EmploymentRecord(this.defaultsProvider);

            if (legacyRecord.VerifHasSignature)
            {
                var newSignatureId = this.StringParser.TryParseDataObjectIdentifier<DataObjectKind.Signature, Guid>(legacyRecord.VerifSignatureImgId);
                entity.SetVerifSignature(
                    legacyRecord.VerifSigningEmployeeId.ToIdentifier<DataObjectKind.Employee>(),
                    newSignatureId.Value);
            }

            entity.EmploymentRecordVoeData = legacyRecord.EmploymentRecordVOEData;
            entity.Attention = this.StringParser.TryParseEntityName(legacyRecord.Attention);
            entity.EmployerZip = this.StringParser.TryParseZipcode(legacyRecord.EmplrZip);
            entity.IsCurrent = legacyRecord.IsCurrent;
            entity.IsSeeAttachment = legacyRecord.IsSeeAttachment;
            entity.JobTitle = DescriptionField.Create(legacyRecord.JobTitle);
            entity.PrepDate = this.ToUnzonedDate(legacyRecord.PrepD);
            entity.VerifExpiresDate = this.ToUnzonedDate(legacyRecord.VerifExpD);
            entity.VerifRecvDate = this.ToUnzonedDate(legacyRecord.VerifRecvD);
            entity.VerifReorderedDate = this.ToUnzonedDate(legacyRecord.VerifReorderedD);
            entity.VerifSentDate = this.ToUnzonedDate(legacyRecord.VerifSentD);
            entity.EmployerAddress = StreetAddress.Create(legacyRecord.EmplrAddr);
            entity.EmployerCity = City.Create(legacyRecord.EmplrCity);
            entity.EmployerFax = this.StringParser.TryParsePhoneNumber(legacyRecord.EmplrFax);
            entity.EmployerName = EntityName.Create(legacyRecord.EmplrNm);
            entity.EmployerPhone = this.StringParser.TryParsePhoneNumber(legacyRecord.EmplrBusPhone);
            entity.EmployerState = this.StringParser.TryParseUnitedStatesPostalState(legacyRecord.EmplrState);
            entity.EmploymentStatusType = legacyRecord.EmplmtStat;
            entity.IsSelfEmployed = legacyRecord.IsSelfEmplmt;

            var legacyAsRegular = legacyRecord as IRegularEmploymentRecord;
            if (legacyAsRegular != null)
            {
                entity.MonthlyIncome = this.StringParser.TryParseMoney(legacyAsRegular.MonI_rep);
                entity.EmploymentStartDate = this.StringParser.TryParseUnzonedDate(legacyAsRegular.EmplmtStartD_rep);
                entity.EmploymentEndDate = this.StringParser.TryParseUnzonedDate(legacyAsRegular.EmplmtEndD_rep);
            }

            var legacyAsPrimary = legacyRecord as IPrimaryEmploymentRecord;
            if (legacyAsPrimary != null)
            {
                entity.PrimaryEmploymentStartDate = this.StringParser.TryParseUnzonedDate(legacyAsPrimary.EmpltStartD_rep);
                entity.ProfessionStartDate = this.StringParser.TryParseUnzonedDate(legacyAsPrimary.ProfStartD_rep);
                entity.IsPrimary = true;
            }

            return entity;
        }
    }
}
