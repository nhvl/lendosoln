﻿namespace DataAccess
{
    using System;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides string parsing for types that enforce validation. If we receive a non-empty
    /// input and are unable to convert, this will throw.
    /// </summary>
    public class UladMigratorStringParser : ParseEntityFromString
    {
        /// <summary>
        /// If true, apply logic to repair semantically invalid data.
        /// </summary>
        /// <remarks>
        /// An examination of the output from the migration checker revealed a lot of data that
        /// was not correct but could be made correct with a bit of help.  In other words, the
        /// intended value is recognizable.  However, our legacy code may not be able to handle it
        /// and so in that case we may want to migrate to the value that the legacy code generates.
        /// This is because it may seem weird if previously suppressed bad data get resurrected
        /// after the migration.  Of course, the code below doesn't attempt to return a default value,
        /// but instead throws when parsing isn't possible.  That is because we haven't yet made a
        /// decision on what to do with bad data so we are just abandoning the migration for now.
        /// </remarks>
        private const bool AttemptFixBadData = true;

        /// <summary>
        /// Initializes a new instance of the <see cref="UladMigratorStringParser"/> class.
        /// </summary>
        public UladMigratorStringParser()
            : base(FormatTarget.Webform, FormatDirection.ToXmlFieldDb)
        {
        }

        /// <summary>
        /// Tries to parse a BooleanKleene from the value.
        /// </summary>
        /// <param name="value">The value to parse.</param>
        /// <returns>The parsed BooleanKleene or null.</returns>
        public override BooleanKleene? TryParseBooleanKleene(string value)
        {
            if (value != null)
            {
                if (value == "0")
                {
                    return BooleanKleene.Indeterminant;
                }
                else if (value == "1")
                {
                    return BooleanKleene.True;
                }
                else if (value == "2")
                {
                    return BooleanKleene.False;
                }
                else
                {
                    var parsed = base.TryParseBooleanKleene(value);
                    if (parsed != null)
                    {
                        return parsed;
                    }
                    else
                    {
                        throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Unable to parse BooleanKleene: " + value);
                    }
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Tries to parse a PhoneNumber from the value.
        /// </summary>
        /// <param name="value">The value to parse.</param>
        /// <returns>The parsed PhoneNumber or null.</returns>
        public override PhoneNumber? TryParsePhoneNumber(string value)
        {
            var parsed = base.TryParsePhoneNumber(value);
            if (!string.IsNullOrWhiteSpace(value) && parsed == null)
            {
                throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Unable to parse PhoneNumber: " + value);
            }

            return parsed;
        }

        /// <summary>
        /// Tries to parse a Zipcode from the value.
        /// </summary>
        /// <param name="value">The value to parse.</param>
        /// <returns>The parsed Zipcode or null.</returns>
        public override Zipcode? TryParseZipcode(string value)
        {
            var parsed = base.TryParseZipcode(value);
            if (!string.IsNullOrWhiteSpace(value) && parsed == null)
            {
                throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Unable to parse Zipcode: " + value);
            }

            return parsed;
        }

        /// <summary>
        /// Tries to parse a UnitedStatesPostalState from the value.
        /// </summary>
        /// <param name="value">The value to parse.</param>
        /// <returns>The parsed UnitedStatesPostalState or null.</returns>
        public override UnitedStatesPostalState? TryParseUnitedStatesPostalState(string value)
        {
            if (AttemptFixBadData)
            {
                if (value != null && value.Length == 2)
                {
                    value = value.Replace("0", "O");
                }
            }

            var parsed = base.TryParseUnitedStatesPostalState(value);
            if (!string.IsNullOrWhiteSpace(value) && parsed == null)
            {
                throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Unable to parse UnitedStatesPostalState: " + value);
            }

            return parsed;
        }

        /// <summary>
        /// Tries to parse a DataObjectIdentifier from the value.
        /// </summary>
        /// <param name="value">The value to parse.</param>
        /// <returns>The parsed DataObjectIdentifier or null.</returns>
        /// <typeparam name="TValueKind">The kind of entity to identify.</typeparam>
        /// <typeparam name="TIdValue">The type of the underlying id.</typeparam>
        public override DataObjectIdentifier<TValueKind, TIdValue>? TryParseDataObjectIdentifier<TValueKind, TIdValue>(string value)
        {
            var parsed = base.TryParseDataObjectIdentifier<TValueKind, TIdValue>(value);
            if (!string.IsNullOrWhiteSpace(value) && parsed == null)
            {
                throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Unable to parse DataObjectIdentifier: " + value);
            }

            return parsed;
        }

        /// <summary>
        /// Tries to parse an EntityName from the value.
        /// </summary>
        /// <param name="value">The value to parse.</param>
        /// <returns>The parsed EntityName or null.</returns>
        public override EntityName? TryParseEntityName(string value)
        {
            var parsed = base.TryParseEntityName(value);
            if (!string.IsNullOrWhiteSpace(value) && parsed == null)
            {
                throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Unable to parse EntityName: " + value);
            }

            return parsed;
        }

        /// <summary>
        /// Tries to parse a PersonName from the value.
        /// </summary>
        /// <param name="value">The value to parse.</param>
        /// <returns>The parsed PersonName or null.</returns>
        public override PersonName? TryParsePersonName(string value)
        {
            var parsed = base.TryParsePersonName(value);
            if (!string.IsNullOrWhiteSpace(value) && parsed == null)
            {
                throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Unable to parse PersonName: " + value);
            }

            return parsed;
        }

        /// <summary>
        /// Tries to parse a Money from the value.
        /// </summary>
        /// <param name="value">The value to parse.</param>
        /// <returns>The parsed Money or null.</returns>
        public override Money? TryParseMoney(string value)
        {
            var parsed = Money.Create(value);
            if (!string.IsNullOrWhiteSpace(value) && parsed == null)
            {
                throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Unable to parse Money: " + value);
            }

            return parsed;
        }

        /// <summary>
        /// Tries to parse a UnzonedDate from the value.
        /// </summary>
        /// <param name="value">The value to parse.</param>
        /// <returns>The parsed UnzonedDate or null.</returns>
        public override UnzonedDate? TryParseUnzonedDate(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return null;
            }

            var parsedUZD = base.TryParseUnzonedDate(value);
            if (parsedUZD != null)
            {
                return parsedUZD;
            }

            if (AttemptFixBadData)
            {
                parsedUZD = this.TryParseUnzonedDateWithRepair(value);
                if (parsedUZD != null)
                {
                    return parsedUZD;
                }
            }

            var parsed = CDateTime.Create(value, FormatTarget.Webform);
            if (parsed == null || !parsed.IsValid)
            {
                // Try format with special handling:
                parsed = CDateTime.Create(value, FormatTarget.FannieMaeMornetPlus);
            }

            if (!parsed.IsValid)
            {
                // We received non-null/empty input and cannot convert.
                throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Unable to parse UnzonedDate: " + value);
            }

            return UnzonedDate.Create(parsed.DateTimeForComputation);
        }

        /// <summary>
        /// Tries to parse an enumeration's value.
        /// </summary>
        /// <typeparam name="E">The type of enumeration to be parsed.</typeparam>
        /// <param name="value">The value to parse.</param>
        /// <returns>The parsed value, or null.</returns>
        public override E? TryParseEnum<E>(string value)
        {
            var parsed = base.TryParseEnum<E>(value);
            if (parsed != null)
            {
                return parsed;
            }

            if (!string.IsNullOrWhiteSpace(value))
            {
                if (AttemptFixBadData && typeof(E) == typeof(E_ReoStatusT))
                {
                    parsed = E_ReoStatusT.Residence as E?;
                }
                else if (AttemptFixBadData && typeof(E) == typeof(E_ReoTypeT))
                {
                    parsed = this.TryParseReoType(value) as E?;
                }

                if (parsed == null)
                {
                    // We received non-null/empty input and cannot convert.
                    throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, $"Unable to parse {typeof(E).Name}: {value}");
                }
            }

            return parsed;
        }

        /// <summary>
        /// Try to repair the input value prior to the attempt to parse it.
        /// </summary>
        /// <param name="value">The value to parse.</param>
        /// <returns>The parsed value, or null.</returns>
        private UnzonedDate? TryParseUnzonedDateWithRepair(string value)
        {
            // The following cleanup code is based upon oddities in the live values that can be dealt with
            value = value.Replace("-", "/");
            value = value.Replace("//", "/");
            value = value.Replace("`", string.Empty);
            if (value.StartsWith("/"))
            {
                value = value.Substring(1);
            }

            if (value.EndsWith("\"") || value.EndsWith("/"))
            {
                value = value.Substring(0, value.Length - 1);
            }

            value = this.FixEndOfMonth(value);

            if (value.Contains("/"))
            {
                DateTime dt;
                bool ok = DateTime.TryParse(value, out dt);
                if (ok)
                {
                    return UnzonedDate.Create(dt);
                }
            }

            return null;
        }

        /// <summary>
        /// If the date string is correctly formatted but the day is too large for the month, correct the day.
        /// </summary>
        /// <param name="value">The date string.</param>
        /// <returns>The date string with the correct day for end of month errors.</returns>
        private string FixEndOfMonth(string value)
        {
            // Don't use DateTime.Parse() because the data is expected to fail
            string[] pieces = value.Split('/');
            if (pieces.Length == 3)
            {
                int month;
                bool ok = int.TryParse(pieces[0], out month);
                if (ok && month >= 1 && month <= 12)
                {
                    int day;
                    ok = int.TryParse(pieces[1], out day);
                    if (ok && day >= 1 && day <= 31)
                    {
                        int year;
                        ok = int.TryParse(pieces[2], out year);
                        if (ok && year >= 1900 && year <= DateTime.Today.Year)
                        {
                            int maxDay = DateTime.DaysInMonth(year, month);
                            day = Math.Min(day, maxDay);

                            return $"{month.ToString()}/{day.ToString()}/{year.ToString()}";
                        }
                    }
                }
            }

            return value;
        }

        /// <summary>
        /// Tries to parse a string into an E_ReoTypeT value.
        /// </summary>
        /// <param name="value">The value to parse.</param>
        /// <returns>An E_ReoTypeT value, or null.</returns>
        private E_ReoTypeT? TryParseReoType(string value)
        {
            // values seen live are make up the switch cases
            switch (value)
            {
                case "0":
                    return E_ReoTypeT.LeaveBlank;
                case "1":
                case "2 UNITS":
                case "2-4 PLEX":
                case "2PLEX":
                case "2-PLEX":
                case "2-PLX":
                case "2UNIT":
                case "2UNITS":
                case "2-UNITS":
                case "2UNTS":
                case "2UTS":
                case "3 UNIT":
                case "3 UNITS":
                case "3-PLEX":
                case "3PLX":
                case "3-PLX":
                case "3UNIT":
                case "3UNITS":
                case "3-UNITS":
                case "3UNTS":
                case "4 PLEX":
                case "4- PLX":
                case "4 UNIT":
                case "4 UNITS":
                case "4-PLEX":
                case "4-PLX":
                case "4UNITS":
                case "4-UNITS":
                case "4UNITSN":
                case "DUPLES":
                case "DUPLEX":
                    return E_ReoTypeT._2_4Plx;
                case "2":
                    return E_ReoTypeT.ComNR;
                case "3":
                case "COM":
                case "COMM":
                case "COMMERCIAL":
                case "COMMERCIAL BUILDING":
                case "CR":
                    return E_ReoTypeT.ComR;
                case "4":
                case "CNDO":
                case "Condominium":
                    return E_ReoTypeT.Condo;
                case "5":
                    return E_ReoTypeT.Coop;
                case "6":
                case "usda_rd":
                case "USDA-RD":
                    return E_ReoTypeT.Farm;
                case "7":
                    return E_ReoTypeT.Land;
                case "8":
                    return E_ReoTypeT.Mixed;
                case "9":
                case "MOBILE":
                    return E_ReoTypeT.Mobil;
                case "10":
                case "10 UNITS":
                case "5MULTI":
                case "5UNITS":
                case "6 PLEX":
                case "6 UNITS":
                case "6-PLX":
                case "6UNITS":
                case "6UNTS":
                case "7UNITS":
                case "8 UNIT":
                case "8 UNITS":
                case "MPLEX":
                case "UNITS":
                case "UNTS":
                    return E_ReoTypeT.Multi;
                case "11":
                case "1-FAM":
                case "HOUSE":
                case "PR":
                case "S":
                case "SFE":
                case "Single Family":
                case "SINGLE FAMILY":
                case "SPR":
                case "SRF":
                    return E_ReoTypeT.SFR;
                case "12":
                case "TWNHS":
                    return E_ReoTypeT.Town;
                case "13":
                case "2NDHOME":
                case "92":
                case "93":
                case "94":
                case "96":
                case "97":
                case "98":
                case "APT":
                case "CABIN":
                case "CHURCH":
                case "conventional":
                case "Conventional":
                case "fha":
                case "FHA":
                case "MyType 2":
                case "MyType 3":
                case "NOT_DEFINED":
                case "PUD":
                case @"R:\":
                case "Rental":
                case "Secondary":
                case "VA":
                    return E_ReoTypeT.Other;
                default:
                    throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, $"Unrecognized E_ReoTypeT: {value}");
            }
        }
    }
}
