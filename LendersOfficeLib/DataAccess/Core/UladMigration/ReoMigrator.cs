﻿namespace DataAccess
{
    using System;
    using LendersOffice.Common;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Migrates the app-level REO collection to the loan-level collection.
    /// </summary>
    internal class ReoMigrator : AbstractAppRecordCollectionMigrator<IReCollection, IRealEstateOwned, RealProperty, DataObjectKind.RealProperty>
    {
        /// <summary>
        /// The consumer id of the borrower.
        /// </summary>
        private DataObjectIdentifier<DataObjectKind.Consumer, Guid> borrowerId;

        /// <summary>
        /// The consumer id of teh coborrower.
        /// </summary>
        private DataObjectIdentifier<DataObjectKind.Consumer, Guid>? coborrowerId;

        /// <summary>
        /// The loan collection container.
        /// </summary>
        private ILoanLqbCollectionContainer collectionContainer;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReoMigrator"/> class.
        /// </summary>
        /// <param name="app">The app data.</param>
        /// <param name="collectionContainer">The loan collection container.</param>
        public ReoMigrator(IUladDataLayerMigrationAppDataProvider app, ILoanLqbCollectionContainer collectionContainer)
            : base(app.aReCollection)
        {
            this.borrowerId = app.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
            this.coborrowerId = (app.aHasCoborrower ? app.aCConsumerId : default(Guid?))?.ToIdentifier<DataObjectKind.Consumer>();
            this.collectionContainer = collectionContainer;
        }

        /// <summary>
        /// Adds an REO to the loan-level collection with the appropriate ownership.
        /// </summary>
        /// <param name="legacyRecord">The old REO record.</param>
        /// <param name="entity">The new REO record.</param>
        /// <returns>The identifier of the new loan-level REO.</returns>
        protected override DataObjectIdentifier<DataObjectKind.RealProperty, Guid> AddEntity(IRealEstateOwned legacyRecord, RealProperty entity)
        {
            DataObjectIdentifier<DataObjectKind.Consumer, Guid>? additionalOwnerId;
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> primaryOwnerId = this.GetOwnerId(
                (Ownership)legacyRecord.ReOwnerT,
                this.borrowerId,
                this.coborrowerId,
                out additionalOwnerId);

            var recordId = this.collectionContainer.Add(primaryOwnerId, entity);

            if (additionalOwnerId.HasValue)
            {
                this.collectionContainer.AddOwnership(additionalOwnerId.Value, recordId);
            }

            return recordId;
        }

        /// <summary>
        /// Creates a new loan-level REO based on the provided app-level REO.
        /// </summary>
        /// <param name="legacyRecord">The app-level REO.</param>
        /// <returns>The new loan-level REO.</returns>
        protected override RealProperty CreateEntity(IRealEstateOwned legacyRecord)
        {
            var entity = new RealProperty();
            entity.StreetAddress = StreetAddress.Create(legacyRecord.Addr);
            entity.City = City.Create(legacyRecord.City);
            entity.GrossRentInc = Money.Create(legacyRecord.GrossRentI);
            entity.HExp = Money.Create(legacyRecord.HExp);
            entity.IsEmptyCreated = legacyRecord.IsEmptyCreated;
            entity.IsForceCalcNetRentalInc = legacyRecord.IsForceCalcNetRentalI;
            entity.IsPrimaryResidence = legacyRecord.IsPrimaryResidence;
            entity.IsSubjectProp = legacyRecord.IsSubjectProp;
            entity.MtgAmt = Money.Create(legacyRecord.MAmt);
            entity.MtgPmt = Money.Create(legacyRecord.MPmt);
            entity.NetRentIncData = Money.Create(legacyRecord.NetRentI);
            entity.NetRentIncLocked = legacyRecord.NetRentILckd;
            entity.OccR = Percentage.Create(legacyRecord.OccR);
            entity.Status = legacyRecord.StatT;
            entity.State = this.StringParser.TryParseUnitedStatesPostalState(legacyRecord.State);
            entity.Type = legacyRecord.TypeT;
            entity.MarketValue = Money.Create(legacyRecord.Val);
            entity.Zip = this.StringParser.TryParseZipcode(legacyRecord.Zip);
            return entity;
        }
    }
}
