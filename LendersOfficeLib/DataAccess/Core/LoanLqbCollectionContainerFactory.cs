﻿namespace DataAccess
{
    /// <summary>
    /// Factory for creating implementations of the ILoanLqbCollectionContainer interface.
    /// </summary>
    public struct LoanLqbCollectionContainerFactory
    {
        /// <summary>
        /// Factory method for creating implementers of the ILoanLqbCollectionContainer interface.
        /// </summary>
        /// <param name="collectionProvider">The data provider that loads/saves the data.</param>
        /// <param name="loanData">The loan data.</param>
        /// <param name="borrMgmt">Interface used to manage consumers and legacy applications.</param>
        /// <param name="observer">The observer for the container.</param>
        /// <returns>An implementation of the ILoanLqbCollectionContainer interface.</returns>
        public ILoanLqbCollectionContainer Create(
            ILoanLqbCollectionProvider collectionProvider,
            ILoanLqbCollectionContainerLoanDataProvider loanData,
            ILoanLqbCollectionBorrowerManagement borrMgmt,
            ILoanLqbCollectionContainerObserver observer)
        {
            return new LoanLqbCollectionContainer(collectionProvider, loanData, borrMgmt, observer);
        }
    }
}
