﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{
    public class PageDataValidationException : CBaseException
    {
        public PageDataValidationException(string userMessage)
            : base(userMessage, userMessage)
        {
        }
    }
}
