﻿/// <copyright file="ISelectStatementProvider.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   8/12/2016
/// </summary>
namespace DataAccess
{
    using System.Collections;
    using System.Collections.Generic;

    /// <summary>
    /// Provides an interface for select statement generators.
    /// </summary>
    public interface ISelectStatementProvider
    {
        /// <summary>
        /// Gets the value for the fields used by the generator.
        /// </summary>
        /// <value>
        /// A <see cref="IEnumerable{T}"/> containing the fields 
        /// used by the generator.
        /// </value>
        IEnumerable<string> FieldSet { get; }

        /// <summary>
        /// Gets the initial input fields for the select provider.
        /// </summary>
        /// <remarks>
        /// This property is intended primarily for use with the cache datalayer.
        /// </remarks>
        IEnumerable InputFieldList { get; }

        /// <summary>
        /// Generates an item for creating SQL select statement using <see cref="FieldSet"/>
        /// and <paramref name="additionalInputFields"/>.
        /// </summary>
        /// <param name="additionalInputFields">
        /// Any additional fields used to generate the select statement.
        /// </param>
        /// <returns>
        /// A <see cref="CSelectStatementProvider.SqlSelectStatementItem"/> to
        /// create the string SQL query.
        /// </returns>
        CSelectStatementProvider.SqlSelectStatementItem GenerateSelectStatement(HashSet<string> additionalInputFields);
    }
}