﻿namespace DataAccess
{
    /// <summary>
    /// Represents the data that will be transformed from a first lien to a second lien.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "We are using the same names as loan properties.")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "We are using the same names as loan properties.")]
    public class FirstLienDataFor2ndLienSwap
    {
        /// <summary>
        /// Gets or sets the value of <see cref="CPageBase.sLienPosTPe"/> from the first lien.
        /// </summary>
        /// <value>The value of <see cref="CPageBase.sLienPosTPe"/> from the first lien.</value>
        public E_sLienPosT sLienPosTPe { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether <see cref="CPageBase.sProdHasExistingOFin"/> was set on the first lien.
        /// </summary>
        /// <value>The value of <see cref="CPageBase.sProdHasExistingOFin"/> from the first lien.</value>
        public bool sProdHasExistingOFin { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether <see cref="CPageBase.sIsOFinNewPe"/> was set on the first lien.
        /// </summary>
        /// <value>The value of <see cref="CPageBase.sIsOFinNewPe"/> from the first lien.</value>
        public bool sIsOFinNewPe { get; set; }

        /// <summary>
        /// Gets or sets the value of <see cref="CPageBase.sLAmtCalcPe_rep"/> from the first lien.
        /// </summary>
        /// <value>The value of <see cref="CPageBase.sLAmtCalcPe_rep"/> from the first lien.</value>
        public string sLAmtCalcPe_rep { get; set; }

        /// <summary>
        /// Gets or sets the value of <see cref="CPageBase.sProOFinBalPe_rep"/> from the first lien.
        /// </summary>
        /// <value>The value of <see cref="CPageBase.sProOFinBalPe_rep"/> from the first lien.</value>
        public string sProOFinBalPe_rep { get; set; }

        /// <summary>
        /// Gets or sets the value of <see cref="CPageBase.sLtvRPe_rep"/> from the first lien.
        /// </summary>
        /// <value>The value of <see cref="CPageBase.sLtvRPe_rep"/> from the first lien.</value>
        public string sLtvRPe_rep { get; set; }

        /// <summary>
        /// Gets or sets the value of <see cref="CPageBase.sLtvROtherFinPe_rep"/> from the first lien.
        /// </summary>
        /// <value>The value of <see cref="CPageBase.sLtvROtherFinPe_rep"/> from the first lien.</value>
        public string sLtvROtherFinPe_rep { get; set; }

        /// <summary>
        /// Gets or sets the monthly payment from the first lien.
        /// </summary>
        /// <value>The value of the monthly payment from the first lien.</value>
        public decimal MonthlyPayment { get; set; }

        /// <summary>
        /// Gets or sets the value of <see cref="CPageBase.sNoteIR_rep"/> from the first lien.
        /// </summary>
        /// <value>The value of <see cref="CPageBase.sNoteIR_rep"/> from the first lien.</value>
        public string sNoteIR_rep { get; set; }

        /// <summary>
        /// Gets or sets the value of <see cref="CPageBase.sProHoAssocDues_rep"/> from the first lien.
        /// </summary>
        /// <value>The value of <see cref="CPageBase.sProHoAssocDues_rep"/> from the first lien.</value>
        public string sProHoAssocDues_rep { get; set; }

        /// <summary>
        /// Gets or sets the value of <see cref="CPageBase.sProRealETx_rep"/> from the first lien.
        /// </summary>
        /// <value>The value of <see cref="CPageBase.sProRealETx_rep"/> from the first lien.</value>
        public string sProRealETx_rep { get; set; }

        /// <summary>
        /// Gets or sets the value of <see cref="CPageBase.sProMIns"/> from the first lien.
        /// </summary>
        /// <value>The value of <see cref="CPageBase.sProMIns"/> from the first lien.</value>
        public decimal sProMIns { get; set; }

        /// <summary>
        /// Gets or sets the value of <see cref="CPageBase.sProMIns_rep"/> from the first lien.
        /// </summary>
        /// <value>The value of <see cref="CPageBase.sProMIns_rep"/> from the first lien.</value>
        public string sProMIns_rep { get; set; }

        /// <summary>
        /// Gets or sets the value of <see cref="CPageBase.sProHazIns_rep"/> from the first lien.
        /// </summary>
        /// <value>The value of <see cref="CPageBase.sProHazIns_rep"/> from the first lien.</value>
        public string sProHazIns_rep { get; set; }

        /// <summary>
        /// Gets or sets the value of <see cref="CPageBase.sProOHExpPe_rep"/> from the first lien.
        /// </summary>
        /// <value>The value of <see cref="CPageBase.sProOHExpPe_rep"/> from the first lien.</value>
        public string sProOHExpPe_rep { get; set; }

        /// <summary>
        /// Gets or sets the value of <see cref="CPageBase.sProdPpmtPenaltyMon2ndLien_rep"/> from the first lien.
        /// </summary>
        /// <value>The value of <see cref="CPageBase.sProdPpmtPenaltyMon2ndLien_rep"/> from the first lien.</value>
        public string sProdPpmtPenaltyMon2ndLien_rep { get; set; }

        /// <summary>
        /// Gets or sets the value of <see cref="CPageBase.sLpInvestorNm"/> from the first lien.
        /// </summary>
        /// <value>The value of <see cref="CPageBase.sLpInvestorNm"/> from the first lien.</value>
        public string sLpInvestorNm { get; set; }

        /// <summary>
        /// Gets or sets the value of <see cref="CPageBase.sONewFinCc"/> from the first lien.
        /// </summary>
        /// <value>The value of <see cref="CPageBase.sONewFinCc"/> from the first lien.</value>
        public string sONewFinCc { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether <see cref="CPageBase.sIsNegAmort"/> was set on the first lien.
        /// </summary>
        /// <value>The value of <see cref="CPageBase.sIsNegAmort"/> from the first lien.</value>
        public bool sIsNegAmort { get; set; }

        /// <summary>
        /// Gets or sets the value of <see cref="CPageBase.sProdRLckdDays"/> from the first lien.
        /// </summary>
        /// <value>The value of <see cref="CPageBase.sProdRLckdDays"/> from the first lien.</value>
        public int sProdRLckdDays { get; set; }

        /// <summary>
        /// Gets or sets the value of <see cref="CPageBase.sSubmitD"/> from the first lien.
        /// </summary>
        /// <value>The value of <see cref="CPageBase.sSubmitD"/> from the first lien.</value>
        public CDateTime sSubmitD { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether <see cref="CPageBase.sEstCloseDLckd"/> from the first lien is set.
        /// </summary>
        /// <value>The value of <see cref="CPageBase.sEstCloseDLckd"/> from the first lien.</value>
        public bool sEstCloseDLckd { get; set; }

        /// <summary>
        /// Gets or sets the value of the other financing closing costs from the first lien.
        /// </summary>
        /// <value>The value of other financing closing costs from the first lien.</value>
        public decimal OtherFinancingClosingCosts { get; set; }

        /// <summary>
        /// Gets or sets the value of <see cref="CPageBase.sFinMethT"/> from the first lien.
        /// </summary>
        /// <value>The value of <see cref="CPageBase.sFinMethT"/> from the first lien.</value>
        public E_sFinMethT sFinMethT { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the file is interest only from the first lien.
        /// </summary>
        /// <value>The value of whether the file is interest only from the first lien.</value>
        public bool IsInterestOnly { get; set; }

        /// <summary>
        /// Gets or sets the value of <see cref="CPageBase.sSubFin_rep"/> from the first lien.
        /// </summary>
        /// <value>The value of <see cref="CPageBase.sSubFin_rep"/> from the first lien.</value>
        public string sSubFin_rep { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether there is a subfinancing HELOC from the first lien.
        /// </summary>
        /// <value>The value of whether there is a subfinancing HELOC from the first lien.</value>
        public bool HasSubHeloc { get; set; }
    }
}
