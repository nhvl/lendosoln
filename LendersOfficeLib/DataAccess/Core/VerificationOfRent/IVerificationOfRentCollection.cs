﻿namespace DataAccess
{
    using System;
    using System.Data;

    /// <summary>
    /// Interface representing a collection of verification of rent records.
    /// </summary>
    public interface IVerificationOfRentCollection
    {
        /// <summary>
        /// Delete the record by id.
        /// </summary>
        /// <param name="recordId">The record id of the record to delete.</param>
        void DeleteByRecordId(Guid recordId);

        /// <summary>
        /// Clear all entries from the record collection.
        /// </summary>
        void Clear();

        /// <summary>
        /// Count the number of entries in the collection.
        /// </summary>
        /// <returns>The number of entries in the collection.</returns>
        int Count();

        /// <summary>
        /// Get the data in the format of a DataSet.
        /// </summary>
        /// <returns>The data in the format of a DataSet.</returns>
        DataSet GetDataSet();

        /// <summary>
        /// Ensures that the publicly retrievable data set accurately reflects the state of
        /// the collection shim.
        /// </summary>
        void EnsureDataSetUpToDate();

        /// <summary>
        /// Get the record by the the record id.
        /// </summary>
        /// <param name="recordId">The id of the record.</param>
        /// <returns>The record assoicated with the record id.</returns>
        IVerificationOfRent GetRecordByRecordId(Guid recordId);

        /// <summary>
        /// Get the record by the index.
        /// </summary>
        /// <param name="index">The index of the record.</param>
        /// <returns>The record assoicated with the record id.</returns>
        IVerificationOfRent GetRecordByIndex(int index);

        /// <summary>
        /// Update the changes in the collection back to the source.
        /// </summary>
        void Update();
    }
}
