﻿namespace DataAccess
{
    using System;
    using System.Data;

    /// <summary>
    /// Verification of rent or land contract.
    /// </summary>
    public interface IVerificationOfRent : ICollectionItemBaseOld
    {
        /// <summary>
        /// Gets or sets the name of the mortgage holder, credit union, or landlord.
        /// </summary>
        string LandlordCreditorName { get; set; }

        /// <summary>
        /// Gets or sets the Description of the verification.
        /// </summary>
        string Description { get; set; }

        /// <summary>
        /// Gets or sets the Address of the information to be verified.
        /// </summary>
        string AddressFor { get; set; }

        /// <summary>
        /// Gets or sets the City of the information to be verified.
        /// </summary>
        string CityFor { get; set; }

        /// <summary>
        /// Gets or sets the State of the information to be verified.
        /// </summary>
        string StateFor { get; set; }

        /// <summary>
        /// Gets or sets the Zip code of the information to be verified.
        /// </summary>
        string ZipFor { get; set; }

        /// <summary>
        /// Gets the single line address of the information to be verified.
        /// </summary>
        string AddressFor_SingleLine { get; }

        /// <summary>
        /// Gets or sets the Address of the mortgage holder, credit union, or landlord.
        /// </summary>
        string AddressTo { get; set; }

        /// <summary>
        /// Gets or sets the City of the mortgage holder, credit union, or landlord.
        /// </summary>
        string CityTo { get; set; }

        /// <summary>
        /// Gets or sets the State the mortgage holder, credit union, or landlord.
        /// </summary>
        string StateTo { get; set; }

        /// <summary>
        /// Gets or sets the Zip the mortgage holder, credit union, or landlord.
        /// </summary>
        string ZipTo { get; set; }

        /// <summary>
        /// Gets or sets the Phone of the mortgage holder, credit union, or landlord.
        /// </summary>
        string PhoneTo { get; set; }

        /// <summary>
        /// Gets or sets the Fax of the mortgage holder, credit union, or landlord.
        /// </summary>
        string FaxTo { get; set; }

        /// <summary>
        /// Gets or sets the AccountName of the information to be verified.
        /// </summary>
        string AccountName { get; set; }

        /// <summary>
        /// Gets or sets the ordered date of the verification.
        /// </summary>
        CDateTime VerifSentD { get; set; }

        /// <summary>
        /// Gets or sets the ordered date of the verification.
        /// </summary>
        string VerifSentD_rep { get; set; }

        /// <summary>
        /// Gets or sets the received date of the verification.
        /// </summary>
        CDateTime VerifRecvD { get; set; }

        /// <summary>
        /// Gets or sets the received date of the verification.
        /// </summary>
        string VerifRecvD_rep { get; set; }

        /// <summary>
        /// Gets or sets the expected date of the verification.
        /// </summary>
        CDateTime VerifExpD { get; set; }

        /// <summary>
        /// Gets or sets the expected date of the verification.
        /// </summary>
        string VerifExpD_rep { get; set; }

        /// <summary>
        /// Gets or sets thr re-ordered of the verification.
        /// </summary>
        CDateTime VerifReorderedD { get; set; }

        /// <summary>
        /// Gets or sets the re-ordered date of the verification.
        /// </summary>
        string VerifReorderedD_rep { get; set; }

        /// <summary>
        /// Gets or sets the type of the verification.
        /// </summary>
        /// <remarks>
        /// Note that while VorT has mortgage as a type, it is not used for 
        /// VORs.  Mortgage verifications are associated with the appropriate 
        /// mortgage liability instead.
        /// </remarks>
        E_VorT VerifT { get; set; }

        /// <summary>
        /// Gets or sets the Attention of the "To" address of the verification.
        /// </summary>
        string Attention { get; set; }

        /// <summary>
        /// Gets or sets the Prepared date of the verification.
        /// </summary>
        CDateTime PrepD { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to indicate to see 
        /// attachement for borrower signature on the documents.
        /// </summary>
        bool IsSeeAttachment { get; set; }

        /// <summary>
        /// Gets the Signing EmployeeId of the verification.
        /// </summary>
        Guid VerifSigningEmployeeId { get; }

        /// <summary>
        /// Gets the ID of the signature image of the verification.
        /// </summary>
        string VerifSignatureImgId { get; }

        /// <summary>
        /// Gets a value indicating whether the verification has a signature.
        /// </summary>
        bool VerifHasSignature { get; }

        /// <summary>
        /// Clears the signature associated with the verification.
        /// </summary>
        void ClearSignature();

        /// <summary>
        /// Applies the signature.
        /// </summary>
        /// <param name="employeeId">The employee Id.</param>
        /// <param name="imgId">The image id of the signature.</param>
        void ApplySignature(Guid employeeId, string imgId);

        /// <summary>
        /// Set the flush function.
        /// </summary>
        /// <param name="onFlush">The action to perform on flush.</param>
        void SetFlushFunction(Action<DataSet> onFlush);
    }
}
