﻿namespace DataAccess
{
    using System;
    using System.Data;

    /// <summary>
    /// Xml based implementation of a collection of rent records.
    /// </summary>
    public class VerificationOfRentCollectionXml : IVerificationOfRentCollection
    {
        /// <summary>
        /// The VorXmlSchema.
        /// </summary>
        private static string vorXmlSchema;

        /// <summary>
        /// The app base.
        /// </summary>
        private CAppBase appBase;

        /// <summary>
        /// The app data.
        /// </summary>
        private CAppData appData;

        /// <summary>
        /// The collection.
        /// </summary>
        private DataSet collectionData;

        /// <summary>
        /// Initializes static members of the <see cref="VerificationOfRentCollectionXml" /> class.
        /// </summary>
        static VerificationOfRentCollectionXml()
        {
            vorXmlSchema = SchemaFolder.RetrieveSchema("VorXmlSchema.xml.config");
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VerificationOfRentCollectionXml" /> class.
        /// </summary>
        /// <param name="application">The associated application.</param>
        public VerificationOfRentCollectionXml(CAppBase application)
        {
            this.appBase = application;

            var loanFile = this.appBase.LoanData.InternalCPageData;
            this.appData = loanFile.GetAppData(this.appBase.aAppId);

            this.collectionData = this.appBase.GetDataSet(vorXmlSchema, application.aVorXmlContent);
            this.appData.RegisterCollection("CVorFields", this.collectionData);
        }

        /// <summary>
        /// Clear all entries from the record collection.
        /// </summary>
        public void Clear()
        {
            this.collectionData.Clear();
            this.Update();
        }

        /// <summary>
        /// Count the number of entries in the collection.
        /// </summary>
        /// <returns>The number of entries in the collection.</returns>
        public int Count()
        {
            var dataTable = this.collectionData.Tables["VorXmlContent"];
            return dataTable.Rows.Count;
        }

        /// <summary>
        /// Delete the record by id.
        /// </summary>
        /// <param name="recordId">The record id of the record to delete.</param>
        public void DeleteByRecordId(Guid recordId)
        {
            var dataTable = this.collectionData.Tables["VorXmlContent"];
            DataRow rowToDelete = null;

            foreach (DataRow row in dataTable.Rows)
            {
                var thisRowId = Guid.Parse(row["recordId"].ToString());

                if (thisRowId == recordId)
                {
                    rowToDelete = row;
                    break;
                }
            }
             
            if (rowToDelete != null)
            {
                rowToDelete.Delete();
            }
        }

        /// <summary>
        /// Get the record by the index.
        /// </summary>
        /// <param name="index">The index of the record.</param>
        /// <returns>The record assoicated with the record id.</returns>
        public IVerificationOfRent GetRecordByIndex(int index)
        {
            var record = new CVorFields(this.appBase, this.collectionData, index);
            record.SetFlushFunction(dst => this.appData.UpdateCollectionData("CVorFields", "CVorFields", dst));
            return record;
        }

        /// <summary>
        /// Get the record by the the record id.
        /// </summary>
        /// <param name="recordId">The id of the record.</param>
        /// <returns>The record assoicated with the record id.</returns>
        public IVerificationOfRent GetRecordByRecordId(Guid recordId)
        {
            var record = new CVorFields(this.appBase, this.collectionData, recordId);
            record.SetFlushFunction(dst => this.appData.UpdateCollectionData("CVorFields", "CVorFields", dst));
            return record;
        }

        /// <summary>
        /// Update the changes in the collection back to the source.
        /// </summary>
        public void Update()
        {
            this.appData.UpdateCollectionData("CVorFields", "CVorFields", this.collectionData);
            this.appBase.aVorXmlContent = this.collectionData.GetXml();
        }

        /// <summary>
        /// Gets the DataSet that backs the collection.
        /// </summary>
        /// <returns>The DataSet that backs the collection.</returns>
        public DataSet GetDataSet()
        {
            return this.collectionData;
        }

        /// <summary>
        /// Ensures that the publicly retrievable data set accurately reflects the state of
        /// the collection shim.
        /// </summary>
        public void EnsureDataSetUpToDate()
        {
            // This is a no-op for this class, as the data set is the source of truth.
        }
    }
}