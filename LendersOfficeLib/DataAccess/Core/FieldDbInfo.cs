namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Linq;
    using System.Reflection;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Extensions;

    public enum E_DbSrcTable
	{
		Loan_A = 0,
		Loan_B = 1,
		Loan_C = 2,
		Loan_D = 3,
		Loan_E = 4,
		Loan_F = 5,
		App_A = 6,
		App_B = 7,
		Employee = 8,
		Trust_Account = 9,
		None = 10, // can be the case if a field is only in loan_file_cache
        Branch = 11, // 7/6/2009 dd - Derive from OPM 32079 - Remove the need to have branch information in LOAN_FILE table
        Team = 12
	}

	public enum E_DbCacheTable
	{
		None = 0,
		Loan_Cache_1 = 1,
		Loan_Cache_2 = 2,
        Loan_Cache_3 = 3,
        Loan_Cache_4 = 4
	}

	public class CFieldDbInfo
	{
        /// <summary>
        /// Represents a list of fields that are stored at the base
        /// level, rather than the loan datalayer level.
        /// </summary>
        /// <remarks>
        /// Fields such as those used for datalayer encryption are 
        /// stored at the base-type level to simplify and decouple
        /// the retrieval and saving of the sensitive fields from
        /// the actual backing storage. To prevent exposure of those
        /// fields to the loan and application datalayer, these fields
        /// will be retrieved directly from the base type.
        /// </remarks>
        private static readonly IEnumerable<string> LoanBaseTypeCacheFields = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
        {
            "sEncryptionKey",
            "sEncryptionMigrationVersion"
        };

		internal static CFieldDbInfo CreateInfoForField( string fieldNm, string tableSource, object data_type, object charMaxLen, object numPrecision, object numScale )
		{
			return new CFieldDbInfo( fieldNm, tableSource, data_type, charMaxLen, numPrecision, numScale );
		}

		private E_DbSrcTable		_m_srcTable;
		private E_DbCacheTable		_m_cacheTable;
		//private 
		private SqlDbType			_m_type;
		private int					_m_charMaxLen;
		private string				_m_fieldName;
		private bool				_m_bFieldOfApp;
		private decimal				_m_minRange = 0;
		private decimal				_m_maxRange = 0;
		private decimal				_m_defaultValue = 0;
		private string				_m_clusive;
		private object				_m_sInputs;
		private object				_m_numPrecision;
		private object				_m_numScale;

		private CFieldDbInfo( string fieldNm, string tableSource, object data_type, object charMaxLen, object numPrecision, object numScale )
		{
			switch( tableSource.ToLower() )
			{
				case "loan_file_a":		
					_m_srcTable = E_DbSrcTable.Loan_A; 
					_m_cacheTable = E_DbCacheTable.None; // we don't know it yet, setting none for now.
					_m_bFieldOfApp = false;
					break;
				case "loan_file_b":		
					_m_srcTable = E_DbSrcTable.Loan_B; 
					_m_cacheTable = E_DbCacheTable.None;
					_m_bFieldOfApp = false;
					break;
				case "loan_file_c":		
					_m_srcTable = E_DbSrcTable.Loan_C; 
					_m_cacheTable = E_DbCacheTable.None;
					_m_bFieldOfApp = false;
					break;
				case "loan_file_d":		
					_m_srcTable = E_DbSrcTable.Loan_D; 
					_m_cacheTable = E_DbCacheTable.None;
					_m_bFieldOfApp = false;
					break;
				case "loan_file_e":		
					_m_srcTable = E_DbSrcTable.Loan_E; 
					_m_cacheTable = E_DbCacheTable.None;
					_m_bFieldOfApp = false;
					break;
				case "loan_file_f":		
					_m_srcTable = E_DbSrcTable.Loan_F;
					_m_cacheTable = E_DbCacheTable.None;
					_m_bFieldOfApp = false;
					break;
				case "application_a":	
					_m_srcTable = E_DbSrcTable.App_A;
					_m_cacheTable = E_DbCacheTable.None;
					_m_bFieldOfApp = true;
					break;
				case "application_b":	
					_m_srcTable = E_DbSrcTable.App_B; 
					_m_cacheTable = E_DbCacheTable.None;
					_m_bFieldOfApp = true;
					break;
				case "trust_account":	
					_m_srcTable = E_DbSrcTable.Trust_Account; 
					_m_cacheTable = E_DbCacheTable.None;
					_m_bFieldOfApp = false;
					break;
				case "loan_file_cache":	
					_m_srcTable = E_DbSrcTable.None;
					_m_cacheTable = E_DbCacheTable.Loan_Cache_1;
					// best guess by the first letter: 's' means loan, and 'a' means app
					_m_bFieldOfApp = fieldNm[0] == 'a';
                    m_Cache_charMaxLen = ConvertToMaxCharLen(charMaxLen);
					break;
				case "loan_file_cache_2":
					_m_srcTable = E_DbSrcTable.None; 
					_m_cacheTable = E_DbCacheTable.Loan_Cache_2;
					_m_bFieldOfApp = fieldNm[0] == 'a';
                    m_Cache_charMaxLen = ConvertToMaxCharLen(charMaxLen);
					break;
                case "loan_file_cache_3":
                    _m_srcTable = E_DbSrcTable.None;
                    _m_cacheTable = E_DbCacheTable.Loan_Cache_3;
                    _m_bFieldOfApp = fieldNm[0] == 'a';
                    m_Cache_charMaxLen = ConvertToMaxCharLen(charMaxLen);
                    break;
                case "loan_file_cache_4":
                    _m_srcTable = E_DbSrcTable.None;
                    _m_cacheTable = E_DbCacheTable.Loan_Cache_4;
                    _m_bFieldOfApp = fieldNm[0] == 'a';
                    m_Cache_charMaxLen = ConvertToMaxCharLen(charMaxLen);
                    break;
                case "branch":
                    _m_srcTable = E_DbSrcTable.Branch;
                    _m_cacheTable = E_DbCacheTable.None;
                    _m_bFieldOfApp = false;
                    break;
				default: 
					throw new CBaseException( ErrorMessages.Generic,  
						string.Format( "CFieldDbInfo constructor doesn't handle this table source {0}.", tableSource.ToString() ) );
			}
			_m_type = ConvertToSqlDbType( data_type );
			_m_charMaxLen = ConvertToMaxCharLen( charMaxLen );
			_m_fieldName = fieldNm;
			_m_numPrecision = numPrecision;
			_m_numScale = numScale;
		}

		public object m_mumericPrecision { get { return _m_numPrecision; } }
		public object m_numericScale { get { return _m_numScale; } }

		public E_DbSrcTable m_dbTable
		{
			get { return _m_srcTable; }
		}
		public SqlDbType		m_dbType
		{
			get { return _m_type; }
		}

		public E_DbCacheTable m_cacheTable
		{
			get { return _m_cacheTable; }
		}
		public bool IsCachedField
		{
			get
			{ 
				switch(  _m_cacheTable )
				{
					case E_DbCacheTable.Loan_Cache_1:
					case E_DbCacheTable.Loan_Cache_2:
                    case E_DbCacheTable.Loan_Cache_3:
                    case E_DbCacheTable.Loan_Cache_4:
						return true;
					case E_DbCacheTable.None:
						return false;
					default:
                        throw new UnhandledEnumException(_m_cacheTable);
				}
			}
		}

        public bool IsAppField => this._m_bFieldOfApp;

		public void SetCacheTable( string tableName )
		{
			switch( tableName.ToLower() )
			{
				case "loan_file_cache": 
					_m_cacheTable = E_DbCacheTable.Loan_Cache_1;
					break;
				case "loan_file_cache_2":
					_m_cacheTable = E_DbCacheTable.Loan_Cache_2;
					break;
                case "loan_file_cache_3":
                    _m_cacheTable = E_DbCacheTable.Loan_Cache_3;
                    break;
                case "loan_file_cache_4":
                    _m_cacheTable = E_DbCacheTable.Loan_Cache_4;
                    break;
				default:
					throw new CBaseException( ErrorMessages.Generic, "Unhandled E_DbCacheTable enum value in CFieldDbInfo.SetCacheTable(). TableName=[" + tableName + "]");
			}
		}

		public int m_charMaxLen
		{
			get { return _m_charMaxLen; }
		}

		public decimal m_minRange
		{
			get { return _m_minRange; }
			set { _m_minRange = value; }
		}

		public decimal m_maxRange
		{
			get { return _m_maxRange; }
			set { _m_maxRange = value; }
		}

		public decimal m_defaultValue
		{
			get { return _m_defaultValue; }
			set { _m_defaultValue = value; }
		}

		public string m_clusive
		{
			get { return _m_clusive; }
			set { _m_clusive = value; }
		}

		public object m_sInputs
		{
			get { return _m_sInputs; }
			set { _m_sInputs = value; }
		}

        public int m_Source_charMaxLen { get; set; }
        public int m_Cache_charMaxLen { get; set; }
        public SqlDbType? m_Cache_dbType { get; set; } 

		internal void CheckCacheConsistency( string fieldName, object data_type, object charMaxLenOfCached )
		{
            switch( fieldName )
            {   // These are the odd ones.
                case "aBGender": 
                case "aCGender":
                    return;
            }
			int cachedFieldLen = ConvertToMaxCharLen( charMaxLenOfCached );
            if (m_charMaxLen != cachedFieldLen && m_dbType != SqlDbType.Text)
				Tools.LogBug( string.Format( "Field: {0} doesn't have consistency CHAR LENGTH between original table and the cached table. Original table len = {1}, cached table len = {2}", fieldName, m_charMaxLen.ToString(), cachedFieldLen.ToString()  ) );

            if (m_dbType != ConvertToSqlDbType(data_type))
            {
                string[] ignoreFields = { "sEscrowFTable", "sCanceledN", "sRejectN", "sSuspendedN", "sOpenedN", "sSubmitN", "sApprovN", "sDocsN", "sFundN", "sRecordedN", "sClosedN" };
                if (!ignoreFields.Contains(fieldName, StringComparer.OrdinalIgnoreCase))
                {
                    Tools.LogBug(string.Format("Field: {0} doesn't have consistency TYPE between original table and the cached table S {1} - C {2}", fieldName, m_dbType, ConvertToSqlDbType(data_type)));
                }
            }
			
		}

        internal static SqlDbType ConvertToSqlDbType( object data_type )
		{
			switch( data_type.ToString().ToLower() )
			{
				case "bit":				return SqlDbType.Bit;
				case "char":			return SqlDbType.Char;
				case "datetime":		return SqlDbType.DateTime;
				case "decimal" :		return SqlDbType.Decimal;
				case "int":				return SqlDbType.Int;
				case "money":			return SqlDbType.Money;
				case "smalldatetime":	return SqlDbType.SmallDateTime;
				case "text":			return SqlDbType.Text;
				case "tinyint":			return SqlDbType.TinyInt;
                case "smallint":        return SqlDbType.SmallInt;
				case "uniqueidentifier": return SqlDbType.UniqueIdentifier;
				case "varchar":			return SqlDbType.VarChar;
                case "bigint": return SqlDbType.BigInt;
                case "varbinary": return SqlDbType.VarBinary;
                case "datetime2": return SqlDbType.DateTime2;
				default:
					string errmsg = string.Format( "DbType {0} is not handled in the FieldDbInfo", data_type );
					Tools.LogErrorWithCriticalTracking( errmsg );
					throw new CBaseException( ErrorMessages.Generic, errmsg );

			}
		}

		internal void OverwriteMaxLength(object newMaxLength)
        {
            _m_charMaxLen = ConvertToMaxCharLen(newMaxLength);
        }

        private static int ConvertToMaxCharLen( object charMaxLen )
		{
			if( DBNull.Value == charMaxLen )
				return 0;
			return (int) charMaxLen;
		}

		public string m_fieldName
		{
			get { return _m_fieldName; }
		}

        private bool m_typeCastBack = false;

        private PropertyInfo m_propertyInfo = null;
        /// <summary>
        /// This works for cached fields only
        /// </summary>
        /// <param name="strFieldNm"></param>
        /// <returns></returns>
        public object GetValueOf(CPageBase loan)
        {
            if (_m_bFieldOfApp)
            {
                if (m_propertyInfo == null)
                {
                    m_propertyInfo = typeof(CAppBase).GetProperty(m_fieldName, BindingFlags.GetProperty | BindingFlags.Public | BindingFlags.Instance);
                    if (null == m_propertyInfo)
                    {
                        string msg = string.Format("Failed to get PropertyInfo from CAppBase for this field {0}.", m_fieldName);
                        Tools.LogBug(msg);
                        throw new CBaseException(ErrorMessages.Generic, msg);
                    }
                }

                try { return m_propertyInfo.GetValue(loan.GetAppData(0), null); }
                catch { return null; }
            }
            else
            {
                string fieldName = m_fieldName;

                if (m_propertyInfo == null)
                {
                    // 10/28/2014 AV - 194382 Banker's Rounding and Custom Reports fields don't match
                    // We need to get the rep and then convert its result back to decimal.
                    if (fieldName.Equals("sSettlementMBrokF", StringComparison.OrdinalIgnoreCase))
                    {
                        fieldName = "sSettlementMBrokF_rep";
                        m_typeCastBack = true;
                        m_propertyInfo = null;
                    }

                    var retrievalType = typeof(CPageBase);
                    if (LoanBaseTypeCacheFields.Contains(fieldName))
                    {
                        retrievalType = retrievalType.BaseType;
                    }

                    m_propertyInfo = retrievalType.GetProperty(fieldName, BindingFlags.GetProperty | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);

                    if (null == m_propertyInfo)
                    {
                        string msg = string.Format("Failed to get PropertyInfo from CPageBase for this field {0}.", m_fieldName);
                        Tools.LogBug(msg);
                        throw new CBaseException(ErrorMessages.Generic, msg);
                    }
                }
                try
                {
                    object value = m_propertyInfo.GetValue(loan, null);
                    if (m_typeCastBack)
                    {
                        value = loan.m_convertLos.ToMoney((string)value);
                    }
                    return value;
                }
                catch { return null; }
            }


		}
	}

	public class CFieldDbInfoList
	{
		private static readonly LqbSingleThreadInitializeLazy<CFieldDbInfoList> instance = 
            new LqbSingleThreadInitializeLazy<CFieldDbInfoList>(() => new CFieldDbInfoList());

		static public  CFieldDbInfoList TheOnlyInstance
		{
			get { return instance.Value; }
		}

        public IList<string> FieldNameList
        {
            get { return m_fieldNameList.Keys; }
        }

        /// <summary>
        /// Return null if fieldName is not a valid column in loan files table nor loan cache table.
        /// </summary>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public CFieldDbInfo Get(string fieldName)
        {
            CFieldDbInfo fieldInfo;
            if (m_fieldNameList.TryGetValue(fieldName, out fieldInfo) == false)
            {
                fieldInfo = null;
            }
            return fieldInfo;
        }
        private SortedList<string, CFieldDbInfo> m_fieldNameList = new SortedList<string, CFieldDbInfo>(StringComparer.OrdinalIgnoreCase);

		private CFieldDbInfoList()
		{

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "ListColumnsInLoanRelatedTables"))
            {
                while (reader.Read())
                {
                    string fieldName = (string) reader["column_name"];
                    switch (fieldName.ToLower())
                    {
                        case "slid":
                        case "aappid":
                            break;
                        default:
                            CFieldDbInfo o = CFieldDbInfo.CreateInfoForField(fieldName,
                                    (string) reader["table_name"], reader["data_type"], reader["character_maximum_length"], reader["numeric_precision"], reader["numeric_scale"]);
                            o.m_Cache_dbType = null;
                            if (o.m_dbType == SqlDbType.Text)
                            {
                                o.OverwriteMaxLength(int.MaxValue);
                                o.m_Source_charMaxLen = int.MaxValue;
                            }

                            o.m_Source_charMaxLen = reader.SafeInt("character_maximum_length");

                            try
                            {
                                m_fieldNameList.Add(fieldName, o);
                            }
                            catch (ArgumentException)
                            {
                                Tools.LogError("Failed to add " + fieldName);
                                throw;
                            }
                            break;

                    }
                }
            }
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "ListColumnsInLoanFileCacheTables")) 
            {
                while (reader.Read()) {
                    string fieldName = (string) reader["column_name"];
                    switch (fieldName.ToLower())
                    {
                        case "slid":
                        case "aappid":
                            break;
                        default:
                            CFieldDbInfo fieldInfo = null;

                            if (m_fieldNameList.TryGetValue(fieldName, out fieldInfo) == true)
                            {
                                fieldInfo.SetCacheTable((string)reader["table_name"]);
                                object length = reader["character_maximum_length"];
                                fieldInfo.CheckCacheConsistency(fieldName, reader["data_type"], reader["character_maximum_length"]);
                                fieldInfo.m_Cache_charMaxLen = reader.SafeInt("character_maximum_length");
                                
                                if (length != DBNull.Value && ((int)length) > 0)
                                {
                                    fieldInfo.OverwriteMaxLength(length);
                                }
                            }
                            else
                            {
                                fieldInfo = CFieldDbInfo.CreateInfoForField(fieldName,
                                    (string)reader["table_name"], reader["data_type"], reader["character_maximum_length"], reader["numeric_precision"], reader["numeric_scale"]);
                                m_fieldNameList.Add(fieldName, fieldInfo);
                            }

                            fieldInfo.m_Cache_dbType = CFieldDbInfo.ConvertToSqlDbType(reader["data_type"]);
                            break;
                    }
                }
            }

			// mb 7/11/2006 - Added the code to get InputRangeAttribute 
			// for each properties here. Needs to be added here because we
			// have to make sure the table is first populated.

			Type[] tClasses = { typeof( CAppBase ), typeof(CPageBase)};

			try
			{
				// gather all of the loan fields that contain
				// inputRange attributes

				foreach( Type type in tClasses )
				{
					foreach( PropertyInfo prop in type.GetProperties( BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance ) )
					{
						foreach( Attribute attr in prop.GetCustomAttributes( false ) )
						{
							// Get the next depends-on attribute from the
							// specified class set.

							InputRangeAttribute inputRange = attr as InputRangeAttribute;

							if( inputRange == null )
							{
								continue;
							}

							// mb 7/11/2006 - Grabs the simpleton instance of the dbtable
							// and then populates the min/max values.
							
							CFieldDbInfo p = Get(prop.Name);
							if (p != null) 
							{
								if ( inputRange.Min == 0 && inputRange.Max == 0 )
								{
									p.m_sInputs  = string.Empty;
								}
								else
								{
									p.m_minRange = inputRange.Min;
									p.m_maxRange = inputRange.Max;
									p.m_clusive  = inputRange.Clusive;
									p.m_defaultValue = inputRange.Default;
								}								
							}
						}
 
					}
				}
			}
			catch( Exception e )
			{
				Tools.LogError( "Initialize InputRange: Construction failed.", e );
			}

		}
	}
}
