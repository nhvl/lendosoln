﻿namespace DataAccess
{
    using System;
    using System.Data.Common;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides methods for handling managment of borrowers and associated legacy/ulad applications.
    /// </summary>
    /// <remarks>
    /// It is important to understand that these methods are called BY the LoanLqbCollectionContainer class and 
    /// called ON the CPageBase class.  Pre-existing methods on the CPageBase class that currently are not aware
    /// of the new collection classes may require modification to call up to similar methods on the LoanLqbCollectionContainer
    /// class which would then call back down to these methods.  If/when this is done, we need to take care that the
    /// current implementation of these methods aren't then causing an infinite loop of calls.<para/>
    /// Some methods on this class have a prefix <c>BorrowerManagementOnly_</c>.  This was added when public methods were added to
    /// CPageData/CPageBase with the same name.  We wanted to use explicit interface implementations which would hide the methods
    /// defined here.  The dependency graph, however, relies on only using the explicitly declared members of the class, so this
    /// failed.  Thus, a prefix (however horrible) is what we've got to work with.
    /// </remarks>
    public interface ILoanLqbCollectionBorrowerManagement
    {
        /// <summary>
        /// Retrieve the value of the flag that determines whether or not the coborrower is deemed as active.
        /// </summary>
        /// <param name="appId">The identifier for the application for which the coborrower flag is getting retrieved.</param>
        /// <returns>The value of the flag.</returns>
        bool GetCoborrowerActiveFlag(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId);

        /// <summary>
        /// Set the flag that determines whether or not the coborrower is deemed as active.
        /// </summary>
        /// <remarks>
        /// This should only be used by LoanLqbCollectionContainer.  Shims need to use AddCoborrowerToLegacyApplication.
        /// </remarks>
        /// <param name="appId">The identifier for the application for which the coborrower flag is getting set.</param>
        /// <param name="value">The value of the flag to set.</param>
        void SetCoborrowerActiveFlag(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId, bool value);

        /// <summary>
        /// Adds a co-borrower to an existing legacy application.
        /// </summary>
        /// <remarks>
        /// This will handle all the book keeping necessary for a coborrower to exist within the ULAD environment.
        /// </remarks>
        /// <param name="appId">The application id.</param>
        /// <returns>The consumer id of the created co-borrower.</returns>
        DataObjectIdentifier<DataObjectKind.Consumer, Guid> AddCoborrowerToLegacyApplication(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId);

        /// <summary>
        /// Add a legacy application to a loan.
        /// </summary>
        /// <remarks>
        /// This also creates a consumer.  Since all consumers must be associated with a ULAD
        /// application, this method also creates a ULAD application and the association.
        /// </remarks>
        /// <param name="connection">The database connection.</param>
        /// <param name="transaction">The database transaction.</param>
        /// <returns>The identifier for the new legacy application.</returns>
        DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> AddLegacyApplication(DbConnection connection, DbTransaction transaction);

        /// <summary>
        /// Update the loan file cache when necessary.
        /// </summary>
        /// <remarks>
        /// This method was separated out from the AddLegacyApplication implementation so the work isn't done
        /// while the transaction is held open, because this is can be an expensive database operation.
        /// </remarks>
        /// <param name="legacyAppId">The legacy application that was recently added, or null.</param>
        void OnSuccessfulApplicationCreation(Guid? legacyAppId);

        /// <summary>
        /// Remove the coborrower from the legacy application.
        /// </summary>
        /// <param name="appId">The identifier for the application from which the coborrower will be removed.</param>
        void BorrowerManagementOnly_RemoveCoborrowerFromLegacyApplication(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId);

        /// <summary>
        /// Remove the legacy application from the loan.
        /// </summary>
        /// <param name="appId">The identifier for the application that will be removed.</param>
        void BorrowerManagementOnly_RemoveLegacyApplication(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId);

        /// <summary>
        /// Save all current changes to the loan.
        /// </summary>
        void SaveAllChanges();

        /// <summary>
        /// Set the indicated legacy application to be the primary for the loan.
        /// </summary>
        /// <param name="appId">The identifier for the legacy application.</param>
        void BorrowerManagementOnly_SetLegacyApplicationAsPrimary(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId);

        /// <summary>
        /// Swap the borrower and coborrower for the indicated application.
        /// </summary>
        /// <param name="appId">The identifier for the relevant application.</param>
        void BorrowerManagementOnly_SwapBorrowers(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId);
    }
}
