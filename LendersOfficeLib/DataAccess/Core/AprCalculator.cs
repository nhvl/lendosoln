﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccess.Core.Construction;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Computes the APR from the given inputs.
    /// </summary>
    public static class AprCalculator
    {
        /// <summary>
        /// Finds the indicated value using the secant method with adjustments specific
        /// to net present value calculations.
        /// This method of APR calculation reduces the number of iterations needed by about 75%
        /// Formula from <see href="http://www.tarleton.edu/faculty/thron/simple_IRR_computation%28thron_moten%29Apr2012.pdf"/>
        /// (link dead, paywalled version of similar (but presumably improved) paper here:
        /// <see href="http://www.ceserp.com/cp-jour/index.php%3Fjournal%3Dijamas%26page%3Darticle%26op%3Dview%26path%5B%5D%3D1841"/>
        /// Original paper was "Efficient Estimators for Internal Rate of Return", Moten, J. &amp; Thron, C.
        /// <see href="http://www.yumpu.com/en/document/view/6654891/efficient-estimators-for-internal-rate-of-return-tarleton-state"/>
        /// and Doug T.
        /// </summary>
        /// <param name="rateGuess1">The first estimate of the value. Must be different than the second.</param>
        /// <param name="rateGuess2">The second estimate of the value. Must be different than the first.</param>
        /// <param name="pmtCount">The number of payments in the NPV payment stream.</param>
        /// <param name="maxTries">The maximum number of iterations to try before giving up.</param>
        /// <param name="f">The function whose result the targetValue should match.</param>
        /// <param name="targetValue">The result we are looking for.</param>
        /// <param name="targetInputError">How far off of the input are we willing to accept.</param>
        /// <param name="targetResultError">How far off of the result are we willing to accept.</param>
        /// <param name="detailsForLogs">A string to include in logs and error messages, since this is a static function.</param>
        /// <returns>A value r, such that f(r) is less than targetError from targetValue.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1407:ArithmeticExpressionsMustDeclarePrecedence", Justification = "Transcription of formula, and formula has same precedence rules as C#.")]
        public static double SolveForValueUsingNpvSecant(double rateGuess1, double rateGuess2, int pmtCount, int maxTries, Func<double, double> f, double targetValue, double targetInputError, double targetResultError, string detailsForLogs)
        {
            double r;
            int numTries = 0;

            double ri1 = rateGuess1;
            double ri2 = rateGuess2;

            // Formula values, which require r2 < r1.
            double r1;
            double r2;
            double k_corr; // Magic number

            double f_ri1 = 0.0;
            double f_ri2 = 0.0;
            double f_r1;
            double f_r2;

            f_ri1 = f(ri1);
            f_ri2 = f(ri2);

            // We only need to check the new values for overflow.
            while (!double.IsInfinity(ri2) && !double.IsNaN(ri2) && !double.IsInfinity(f_ri2) && !double.IsNaN(f_ri2) && numTries < maxTries)
            {
                if (ri1 < ri2)
                {
                    r1 = ri2;
                    r2 = ri1;
                    f_r1 = f_ri2;
                    f_r2 = f_ri1;
                }
                else
                {
                    r1 = ri1;
                    r2 = ri2;
                    f_r1 = f_ri1;
                    f_r2 = f_ri2;
                }

                // Magic numbers from algorithm formula
                if (targetValue > f_r2 && f_r2 > f_r1)
                {
                    k_corr = 1.4;
                }
                else if (f_r2 > f_ri2 && f_r1 > targetValue)
                {
                    k_corr = 0.5 * pmtCount * r1 + 1.4;
                }
                else if (f_r2 > targetValue && targetValue > f_r1)
                {
                    k_corr = 0.3 * pmtCount * r1 + 1.4;
                }
                else
                {
                    k_corr = 1.0; // ERROR! Should never be reached.
                }

                numTries++;
                r = r2 + (r1 - r2) * (f_r2 - targetValue) / (f_r2 - f_r1) * (1.0 - (f_r1 - targetValue) / (f_r1 - 3.0 * f_r2) * k_corr);

                double f_r = f(r);

                double zeroTest = targetValue - f_r;

                if (Math.Abs(zeroTest) < targetResultError && Math.Abs(r - ri1) < targetInputError)
                {
                    return r;
                }

                ri1 = ri2;
                ri2 = r;
                f_ri1 = f_ri2;
                f_ri2 = f_r;
            }

            throw new CBaseException(ErrorMessages.Generic, $"Failed to find root after {numTries} tries. NPV Secant. Details: {detailsForLogs}.");
        }

        /// <summary>
        /// Finds the indicated value using the bisection method for root finding.
        /// See <see cref="https://en.wikipedia.org/wiki/Bisection_method"/>.
        /// </summary>
        /// <param name="lowerBound">The initial lower bound.</param>
        /// <param name="upperBound">The initial upper bound.</param>
        /// <param name="maxTries">The maximum number of iterations to try before giving up.</param>
        /// <param name="f">The function whose result the targetValue should match.</param>
        /// <param name="targetValue">The result we are looking for.</param>
        /// <param name="targetInputError">How far off of input value we are willing to accept.</param>
        /// <param name="targetResultError">How far off of the result value are we willing to accept.</param>
        /// <param name="detailsForLogs">A string to include in logs and error messages, since this is a static function.</param>
        /// <returns>A value r, such that f(r) is less than targetError from targetValue.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1407:ArithmeticExpressionsMustDeclarePrecedence", Justification = "Transcription of formula, and formula has same precedence rules as C#.")]
        public static double SolveForValueUsingBisection(double lowerBound, double upperBound, int maxTries, Func<double, double> f, double targetValue, double targetInputError, double targetResultError, string detailsForLogs)
        {
            double a = lowerBound;
            double b = upperBound;

            // Naughty developer...
            if (a > b)
            {
                a = upperBound;
                b = lowerBound;
            }

            double f_a = f(a) - targetValue;
            double f_b = f(b) - targetValue;

            if (Math.Abs(f_a) < targetResultError && Math.Abs(b - a) < targetInputError)
            {
                return a;
            }

            if (Math.Abs(f_b) < targetResultError && Math.Abs(b - a) < targetInputError)
            {
                return b;
            }

            if ((f_a > 0 && f_b > 0) || (f_a < 0 && f_b < 0))
            {
                throw new CBaseException(ErrorMessages.Generic, $"Bisection bounds do not guarantee a solution will be found. f(A) - targetValue = {f_a}, f(B) - targetValue = {f_b}. Details: {detailsForLogs}");
            }

            int numTries = 0;

            while (a <= b && numTries < maxTries)
            {
                numTries++;
                double r = (a + b) / 2;
                double f_r = f(r) - targetValue;

                if (Math.Abs(f_r) < targetResultError && Math.Abs(b - a) < targetInputError)
                {
                    return r;
                }

                // We didn't need the check for f_B > 0 in sApr because in that case, we could be sure that f is a decreasing function.
                if ((f_r > 0 && f_b > 0) || (f_r < 0 && f_b < 0))
                {
                    f_b = f_r;
                    b = r;
                }
                else
                {
                    f_a = f_r;
                    a = r;
                }
            }

            throw new CBaseException(ErrorMessages.Generic, $"Failed to find root after {numTries} tries. Bisection. Details: {detailsForLogs}.");
        }

        /// <summary>
        /// Computes the net present value of the payment stream, given the discount rate r, using the
        /// method given in Appendix J to 12 CFR 1026.
        /// </summary>
        /// <param name="amountData">The sequence of amount data records.</param>
        /// <param name="daysPerUnitPeriod">The number of days per unit period.</param>
        /// <param name="r">The discount rate.</param>
        /// <returns>The net present value of the inputs.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1407:ArithmeticExpressionsMustDeclarePrecedence", Justification = "Transcription of formula, and formula has same precedence rules as C#.")]
        public static double ComputePresentNetValue(IEnumerable<AprAmountData> amountData, int daysPerUnitPeriod, double r)
        {
            double pnv = 0;

            foreach (var entry in amountData)
            {
                pnv += (double)entry.Amount / ((1 + entry.OddDays * r / daysPerUnitPeriod) * Math.Pow(1 + r, entry.UnitPeriod));
            }

            return pnv;
        }

        /// <summary>
        /// Computes the net present value of the payment stream, given the discount rate r, provided
        /// that the payment stream represents even periods (e.g. one payment each month with no skipped
        /// months), possibly with an irregular first period. The payment amounts need not be regular.
        /// Calculation is optimized for arithmetic efficiency.
        /// See also <see href="https://mlintranet/x/zjPiAQ#APRCalculation-Specificequation-singleadvance,regularpaymentloans"/>
        /// </summary>
        /// <param name="amounts">The sequence of amounts.</param>
        /// <param name="r">The discount rate.</param>
        /// <param name="oddDays">The number of odd days in the first period. Assumes 0 if omitted.</param>
        /// <param name="daysPerUnitPeriod">The number of days per unit period. Assumes 30 if omitted.</param>
        /// <param name="extraUnitPeriods">The number of extra unit periods in the first period. Assumes 0 if omitted.</param>
        /// <returns>The net present value of the inputs.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1407:ArithmeticExpressionsMustDeclarePrecedence", Justification = "Transcription of formula, and formula has same precedence rules as C#.")]
        public static double ComputePresentNetValueOfEvenPeriods(IEnumerable<decimal> amounts, double r, int oddDays = 0, int daysPerUnitPeriod = 30, int extraUnitPeriods = 0)
        {
            double pnv = 0;
            double powerBase = 1 + r;
            double powerAmt = 1.0;

            foreach (double amt in amounts)
            {
                powerAmt *= powerBase; // (1 + r)^(n+1) = (1 + r)^n * (1 + r) 
                pnv += amt / powerAmt;
            }

            return pnv * 1 / ((1 + oddDays * r / daysPerUnitPeriod) * Math.Pow(powerBase, extraUnitPeriods)); // These are the same for each term, and so can be factored out.
        }

        /// <summary>
        /// Computes the APR using the general APR equation as defined in Appendix J to 12 CFR 1026.
        /// See also <see href="https://mlintranet/x/zjPiAQ#APRCalculation-Generalequation"/>
        /// </summary>
        /// <param name="advances">The sequence of advances.</param>
        /// <param name="payments">The sequence of payments.</param>
        /// <param name="daysPerUnitPeriod">The number of "days" per unit period.</param>
        /// <param name="lowerBound">The lower bound as an annual rate to use (defaults to -50).</param>
        /// <param name="upperBound">The upper bound as an annual rate to use (defaults to 100).</param>
        /// <param name="unitPeriodsPerYear">The number of unit periods per year (defaults to 12).</param>
        /// <returns>The APR as a percentage (e.g. 4.385% would be 4.385, not 0.04385).</returns>
        /// <remarks>This method is the most stable, most likely to be correct, and slowest.</remarks>
        public static double CalculateUsingGeneralEquation(
            IEnumerable<AprAmountData> advances,
            IEnumerable<AprAmountData> payments,
            int daysPerUnitPeriod,
            double lowerBound = -50.0,
            double upperBound = 100.0,
            int unitPeriodsPerYear = 12)
        {
            return SolveForValueUsingBisection(
                lowerBound: lowerBound / (100 * unitPeriodsPerYear),
                upperBound: upperBound / (100 * unitPeriodsPerYear),
                maxTries: 100,
                f: r => ComputePresentNetValue(advances, daysPerUnitPeriod, r)
                        - ComputePresentNetValue(payments, daysPerUnitPeriod, r),
                targetValue: 0.0,
                targetInputError: 0.00001 / (100 * unitPeriodsPerYear),
                targetResultError: 0.01,
                detailsForLogs: "AprCalculator.CalculateUsingGeneralEquation") * 100 * unitPeriodsPerYear;
        }

        /// <summary>
        /// Computes the APR using the APR formula for a single-disbursement loan with even payment periods,
        /// except possibly an irregular first payment as defined in Appendix J to 12 CFR 1026.
        /// See also <see href="https://mlintranet/x/zjPiAQ#APRCalculation-Specificequation-singleadvance,regularpaymentloans"/>
        /// </summary>
        /// <param name="payments">The sequence of payments with even periods.</param>
        /// <param name="advanceAmt">The single advance amount (usually amount financed as defined in the regulation).</param>
        /// <param name="oddDays">The number of odd days.</param>
        /// <param name="daysPerUnitPeriod">The number of days per unit period.</param>
        /// <param name="extraUnitPeriods">The number of extra unit periods for the irregular first payment.</param>
        /// <param name="tryNpvSecantSolver">If true, try to use the optimized NPV secant solver first.</param>
        /// <param name="rateGuess">An initial guess for the rate (typically the note rate) when using the NPV secant solver.</param>
        /// <returns>The APR as a percentage (e.g. 4.385% would be 4.385, not 0.04385).</returns>
        public static double CalculateSingleAdvanceRegularRepaymentLoan(
            ICollection<decimal> payments,
            decimal advanceAmt,
            int oddDays = 0,
            int daysPerUnitPeriod = 30,
            int extraUnitPeriods = 0,
            bool tryNpvSecantSolver = false,
            double rateGuess = 6.0 / 1200)
        {
            if (tryNpvSecantSolver)
            {
                try
                {
                    return SolveForValueUsingNpvSecant(
                        rateGuess1: 0,
                        rateGuess2: rateGuess,
                        pmtCount: payments.Count,
                        maxTries: 100,
                        f: r => ComputePresentNetValueOfEvenPeriods(payments, r, oddDays, daysPerUnitPeriod, extraUnitPeriods),
                        targetValue: (double)advanceAmt,
                        targetInputError: 0.001 / 1200,
                        targetResultError: 0.01,
                        detailsForLogs: $"AprCalculator.CalculateSingleAdvanceRegularRepaymentLoan, rateGuess: {rateGuess}, 1st payment: {payments.FirstOrDefault()}") * 1200.0;
                }
                catch (CBaseException e)
                {
                    // Do not return early, just fall back to bisection.
                    Tools.LogWarning(e.DeveloperMessage);
                }
            }

            return SolveForValueUsingBisection(
                lowerBound: -50.0 / 1200.0,
                upperBound: 100.0 / 1200.0,
                maxTries: 100,
                f: r => ComputePresentNetValueOfEvenPeriods(payments, r, oddDays, daysPerUnitPeriod, extraUnitPeriods),
                targetValue: (double)advanceAmt,
                targetInputError: 0.00001 / 1200,
                targetResultError: 0.01,
                detailsForLogs: $"AprCalculator.CalculateSingleAdvanceRegularRepaymentLoan, rateGuess: {rateGuess}, 1st payment: {payments.FirstOrDefault()}") * 1200.0;
        }

        /// <summary>
        /// Computes the APR using the single advance, single payment forms given in Appendix J to 12 CFR 1026.
        /// For forms 1-3, this is the most computationally efficient formula.
        /// See also <see href="https://mlintranet/display/LPD/APR+Calculation#APRCalculation-Specificequations-singleadvance,singlepaymentloans"/>
        /// Note that the regulations require that, regardless of how interest is computed for the loan, a single
        /// advance, single repayment loan shall use years as the unit period. If another term other than whole
        /// years is used as the basis for interest accrual, that is used as the basis for the fractional periods.
        /// Note that as a consequence, if interest actually accrues monthly, the term of the loan *must* be in
        /// whole months--there is no way for odd days to be considered.
        /// </summary>
        /// <param name="advanceAmount">The amount of the single advance.</param>
        /// <param name="paymentAmount">The amount of the single payment.</param>
        /// <param name="wholeYears">The number of whole years between advance and payment.</param>
        /// <param name="fractionalPeriods">The number of fractional periods (usually months or days) between advance and payment.</param>
        /// <param name="fractionsPerUnitPeriod">The number of fractions that make up exactly one unit period.</param>
        /// <returns>The APR as a percentage (e.g. 4.385% would be 4.385, not 0.04385).</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1407:ArithmeticExpressionsMustDeclarePrecedence", Justification = "Transcription of formula, and formula has same precedence rules as C#.")]
        public static double CalculateSingleDrawSinglePaymentLoan(
            decimal advanceAmount,
            decimal paymentAmount,
            int wholeYears,
            int fractionalPeriods,
            int fractionsPerUnitPeriod)
        {
            double p = (double)paymentAmount;
            double a = (double)advanceAmount;
            int t = wholeYears;
            double f = (double)fractionalPeriods / fractionsPerUnitPeriod; // w = 1 / f;

            if (wholeYears < 1)
            {
                // Form 1: Term less than 1 year
                return (100 / f) * (p / a - 1);
            }
            else if (wholeYears >= 1 && wholeYears < 2 && fractionalPeriods > 0)
            {
                // Form 2: Term more than 1 year but less than 2 years
                return (50 / f) * (Math.Pow((1 + f) * (1 + f) + 4 * f * (p / a - 1), 0.5) - (1 + f));
            }
            else if (fractionalPeriods == 0)
            {
                // Form 3: Term equal to exactly a year or exact multiple of a year
                return 100 * (Math.Pow(p / a, 1.0 / t) - 1);
            }
            else
            {
                // Otherwise fall back to the general equation
                return CalculateUsingGeneralEquation(
                    advances: new List<AprAmountData>() { new AprAmountData(advanceAmount, 0, 0) },
                    payments: new List<AprAmountData>() { new AprAmountData(paymentAmount, wholeYears, fractionalPeriods) },
                    daysPerUnitPeriod: fractionsPerUnitPeriod,
                    unitPeriodsPerYear: 1);
            }
        }

        /// <summary>
        /// Computes the APR using the instructions given in Appendix D to 12 CFR 1026 for a conrstuction
        /// only loan (or the construction phase of a construction to perm loan disclosed separately).
        /// See also <see href="https://mlintranet/x/zjPiAQ#APRCalculation-Closed-endconstructiononly,ortheconstructionphaseofaconstruction-to-permanentloandisclosedseparatelywherethescheduleofadvancesisnotknown"/>
        /// </summary>
        /// <param name="commitmentAmount">The construction phase commitment amount.</param>
        /// <param name="prepaidFinanceCharges">The total of prepaid finance charges (APR-related fees).</param>
        /// <param name="estimatedConstructionPhaseInterest">The estimated construction phase interest.</param>
        /// <param name="constructionStartDate">When the construction phase begins.</param>
        /// <param name="constructionEndDate">When the construction phase ends.</param>
        /// <param name="interestAccrualType">The construction phase interest accrual type.</param>
        /// <param name="aprWinInputs">The AprWinInputs instance to dump debug info into.</param>
        /// <returns>The APR as a percentage (e.g. 4.385% would be 4.385, not 0.04385).</returns>
        public static double CalculateConstructionPhaseOnly(
            decimal commitmentAmount,
            decimal prepaidFinanceCharges,
            decimal estimatedConstructionPhaseInterest,
            DateTime constructionStartDate,
            DateTime constructionEndDate,
            ConstructionPhaseIntAccrual interestAccrualType,
            AprWinInputs.AprWin_6_2 aprWinInputs = null)
        {
            decimal workingCommitmentAmount = commitmentAmount / 2;
            var constructionPeriods = FedCalendarPeriods.ConstructionOnlyPeriodBetween(constructionStartDate, constructionEndDate, interestAccrualType);

            if (aprWinInputs != null)
            {
                aprWinInputs.ConstructionInputs.ConstructionSeparately.LoanDate = UnzonedDate.Create(constructionStartDate).Value;
                aprWinInputs.ConstructionInputs.ConstructionSeparately.ConstructionPeriodEndDate = UnzonedDate.Create(constructionEndDate).Value;

                if (interestAccrualType == ConstructionPhaseIntAccrual.Monthly_360_360)
                {
                    aprWinInputs.ConstructionInputs.ConstructionSeparately.NumberOfMonths = constructionPeriods.UnitPeriods * constructionPeriods.FractionsPerUnitPeriod;
                }
                else
                {
                    aprWinInputs.ConstructionInputs.ConstructionSeparately.NumberOfDays = constructionEndDate.Subtract(constructionStartDate).Days;
                    aprWinInputs.ConstructionInputs.ConstructionSeparately.NumberOfUnitPeriods = constructionPeriods.UnitPeriods;
                    aprWinInputs.ConstructionInputs.ConstructionSeparately.NumberOfOddDays = constructionPeriods.FractionalPeriods;
                }
            }

            return CalculateSingleDrawSinglePaymentLoan(
                advanceAmount: workingCommitmentAmount - prepaidFinanceCharges,
                paymentAmount: workingCommitmentAmount + estimatedConstructionPhaseInterest,
                wholeYears: constructionPeriods.UnitPeriods,
                fractionalPeriods: constructionPeriods.FractionalPeriods,
                fractionsPerUnitPeriod: constructionPeriods.FractionsPerUnitPeriod);
        }

        /// <summary>
        /// Computes the APR using the instructions given in Appendix D to 12 CFR 1026 for a construction
        /// to permanent loan with an unknown draw schedule, disclosed as on transaction.
        /// See also <see href="https://mlintranet/x/zjPiAQ#APRCalculation-Closed-endconstruction-to-permanentloandisclosedasonetransactionwherethescheduleofadvancesisnotknown"/>
        /// </summary>
        /// <param name="permPhasePayments">The sequence of payments with even periods of the permanent phase.</param>
        /// <param name="amountFinanced">The amount financed (final loan amount less APR-related fees).</param>
        /// <param name="estimatedConstructionPhaseInterest">The estimated construction phase interest per Appendix D of 12 CFR 1026.</param>
        /// <param name="constructionStartDate">When the construction phase begins.</param>
        /// <param name="constructionEndDate">When the construction phase ends.</param>
        /// <param name="firstPaymentDate">The first payment due date for the permanent phase.</param>
        /// <param name="tryNpvSecantSolver">If true, try to use the optimized NPV secant solver first.</param>
        /// <param name="rateGuess">An initial guess for the rate (typically the note rate) when using the NPV secant solver.</param>
        /// <param name="aprWinInputs">The AprWinInputs instance to dump debug info into.</param>
        /// <returns>The APR as a percentage (e.g. 4.385% would be 4.385, not 0.04385).</returns>
        public static double CalculateOneDisclosureConstructionToPermLoanWithUnknownAdvanceSchedule(
            ICollection<decimal> permPhasePayments,
            decimal amountFinanced,
            decimal estimatedConstructionPhaseInterest,
            DateTime constructionStartDate,
            DateTime constructionEndDate,
            DateTime firstPaymentDate,
            bool tryNpvSecantSolver = false,
            double rateGuess = 6.0 / 1200,
            AprWinInputs.AprWin_6_2 aprWinInputs = null)
        {
            var constructionPhasePeriods = FedCalendarPeriods.ConstructionPermPeriodsBetween(constructionStartDate, constructionEndDate);
            var periodsBetweenConstructionAndPermanent = FedCalendarPeriods.ConstructionPermPeriodsBetween(constructionEndDate, firstPaymentDate).AddUnitPeriods(-1);
            var adjustedHalfConstructionPhasePeriods = constructionPhasePeriods.HalvePeriods().AddFedCalendarPeriods(periodsBetweenConstructionAndPermanent);

            if (aprWinInputs != null)
            {
                var paymentStream = new List<AprAmountData>();

                int currentUnitPeriod = adjustedHalfConstructionPhasePeriods.UnitPeriods + 1; // APRWIN starts at 1, we start at 0.
                int currentOddDays = adjustedHalfConstructionPhasePeriods.FractionalPeriods;

                foreach (var amt in permPhasePayments)
                {
                    var pmt = new AprAmountData(amt, currentUnitPeriod, currentOddDays);
                    paymentStream.Add(pmt);

                    ++currentUnitPeriod;
                }

                aprWinInputs.ConstructionInputs.ConstructionAndPermanent.PaymentSchedule.LoanDate = UnzonedDate.Create(constructionStartDate).Value;
                aprWinInputs.ConstructionInputs.ConstructionAndPermanent.PaymentSchedule.ConstructionPeriodEndDate = UnzonedDate.Create(constructionEndDate).Value;
                aprWinInputs.ConstructionInputs.ConstructionAndPermanent.PaymentSchedule.AmortizationPaymentDate = UnzonedDate.Create(firstPaymentDate).Value;
                aprWinInputs.ConstructionInputs.ConstructionAndPermanent.PaymentSchedule.PaymentList = paymentStream;
            }

            return CalculateSingleAdvanceRegularRepaymentLoan(
                payments: permPhasePayments,
                advanceAmt: amountFinanced - estimatedConstructionPhaseInterest,
                oddDays: adjustedHalfConstructionPhasePeriods.FractionalPeriods,
                daysPerUnitPeriod: adjustedHalfConstructionPhasePeriods.FractionsPerUnitPeriod,
                extraUnitPeriods: adjustedHalfConstructionPhasePeriods.UnitPeriods,
                tryNpvSecantSolver: tryNpvSecantSolver,
                rateGuess: rateGuess);
        }
    }
}
