﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using LendersOffice.Admin;
    using LendersOffice.Common;


    public class CDateTime : IEquatable<CDateTime>
    {
        private static readonly CDateTime s_invalidValue = new CDateTime();

        private static DateTime s_MinDate = new DateTime(1900, 1, 1, 0, 0, 0);
        private static DateTime s_MaxDate = new DateTime(3000, 12, 31, 23, 59, 59);

        private static DateTime s_SqlSmallDateTimeMin = new DateTime(1900, 1, 1, 0, 0, 0);
        private static DateTime s_SqlSmallDateTimeMax = new DateTime(2079, 6, 6, 0, 0, 0);

        private readonly DateTime? innerDt;

        private CDateTime()
        {
            this.innerDt = null;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="dateTime"></param>
        private CDateTime(DateTime dateTime)
        {
            this.innerDt = null;

            if (0 != dateTime.CompareTo(InvalidDateTime))
            {
                // TryParse returns DateTimeKind.Unspecified when there is no time zone information
                // in the string being parsed. The old constructors generated a DateTime string that
                // did not have a time zone in its format, and so ensure behavior is the same, we
                // need to make sure to change the DateTimeKind to Unspecified as well, just to be
                // safe.
                this.innerDt = DateTime.SpecifyKind(dateTime, DateTimeKind.Unspecified);
            }
        }

        public static CDateTime InvalidWrapValue
        {
            get
            {
                return s_invalidValue;
            }
        }

        public static DateTime InvalidDateTime
        {
            get { return DateTime.MaxValue; }
        }

        public bool IsValid
        {
            get
            {
                if (this.innerDt.HasValue)
                {
                    return ValidateDateTimeRange(this.innerDt.Value);
                }

                return false;
            }
        }

        public bool IsValidSmallDateTime
        {
            get
            {
                if (this.innerDt.HasValue)
                {
                    if (this.innerDt.Value.CompareTo(s_SqlSmallDateTimeMin) > 0 && this.innerDt.Value.CompareTo(s_SqlSmallDateTimeMax) < 0)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        /// <summary>
        /// This property will throw if the datetime is invalid. It only returns dates, not times.
        /// Use DateTimeForComputationWithTime if you need the time.
        /// </summary>
        public DateTime DateTimeForComputation
        {
            get
            {
                DateTime dt = this.DateTimeForComputationWithTime;
                return new DateTime(dt.Year, dt.Month, dt.Day);
            }
        }

        public DateTime DateTimeForComputationWithTime
        {
            get
            {
                if (!this.innerDt.HasValue)
                {
                    throw new CBaseException(ErrorMessages.Generic, "Invalid date");
                }

                if (!ValidateDateTimeRange(this.innerDt.Value))
                {
                    throw new CBaseException(ErrorMessages.Generic, "Invalid date '" + this.innerDt.Value + "': Year must be between 1900 and 3000.");
                }

                return this.innerDt.Value;
            }
        }

        public object DateTimeForDBWriting
        {
            get
            {
                try
                {
                    if (this.innerDt.HasValue)
                    {
                        return this.DateTimeForComputationWithTime;
                    }
                    else
                    {
                        return DBNull.Value;
                    }
                }
                catch
                {
                    return DBNull.Value;
                }
            }
        }

        /// <summary>
        /// Factory method to create a CDateTime from a string and a converter.
        /// </summary>
        /// <param name="strValue">The string to parse.</param>
        /// <param name="converter">The converter to use.</param>
        /// <returns>The CDateTime that the string represents, or a CDateTime equal to CDateTime.InvalidWrapValue.</returns>
        public static CDateTime Create(string strValue, LosConvert converter)
        {
            var format = converter == null ? FormatTarget.Webform : converter.FormatTargetCurrent;
            return Create(strValue, format);
        }

        /// <summary>
        /// Factory method to create a CDateTime from a string and a converter.
        /// </summary>
        /// <param name="strValue">The string to parse.</param>
        /// <param name="format">The target format.</param>
        /// <returns>The CDateTime that the string represents, or a CDateTime equal to CDateTime.InvalidWrapValue.</returns>
        public static CDateTime Create(string strValue, FormatTarget format)
        {
            if (string.IsNullOrWhiteSpace(strValue))
            {
                return InvalidWrapValue;
            }

            string trimmedStrValue = strValue.TrimWhitespaceAndBOM();

            // If we are expecting yyyyMMdd and we find it, return.
            if (format == FormatTarget.FannieMaeMornetPlus
                    || format == FormatTarget.FreddieMacAUS
                    || format == FormatTarget.GinnieNet)
            {
                if (trimmedStrValue.Length == 8)
                {
                    DateTime parsedDateOfLength8;

                    if (DateTime.TryParseExact(trimmedStrValue, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out parsedDateOfLength8))
                    {
                        return Create(parsedDateOfLength8);
                    }
                }
            }

            DateTime parsedDate;

            // Accept any string that the .NET BCL will parse into a DateTime by default, plus MMddyy and MMddyyyy
            if (DateTime.TryParse(trimmedStrValue, out parsedDate))
            {
                return Create(parsedDate);
            }
            else if (DateTime.TryParseExact(trimmedStrValue, "MMddyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out parsedDate))
            {
                return Create(parsedDate);
            }
            else if (DateTime.TryParseExact(trimmedStrValue, "MMddyyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out parsedDate))
            {
                return Create(parsedDate);
            }

            // Could not parse, so return the invalid value.
            return InvalidWrapValue;
        }

        /// <summary>
        /// Factory method to create a CDateTime from a DateTime.
        /// </summary>
        /// <param name="dtValue">The DateTime to convert.</param>
        /// <returns>A CDateTime that wraps the DateTime, or CDateTime.InvalidWrapValue if the the DateTime is equal to CDateTime.InvalidDateTime.</returns>
        public static CDateTime Create(DateTime dtValue)
        {
            if (dtValue.Equals(InvalidDateTime) || !ValidateDateTimeRange(dtValue))
            {
                return InvalidWrapValue;
            }
            else
            {
                return new CDateTime(dtValue);
            }
        }

        /// <summary>
        /// Factory method to create a CDateTime from a nullable DateTime.
        /// </summary>
        /// <param name="dtValue">The nullable DateTime to convert.</param>
        /// <returns>A CDateTime that wraps the DateTime, or CDateTime.InvalidWrapValue if the the DateTime is null or equal to CDateTime.InvalidDateTime.</returns>
        public static CDateTime Create(DateTime? dtValue)
        {
            if (!dtValue.HasValue)
            {
                return InvalidWrapValue;
            }
            else
            {
                return Create(dtValue.Value);
            }
        }

        public static bool ValidateDateTimeRange(DateTime dt)
        {
            return (dt > s_MinDate && dt < s_MaxDate);
        }

        /// <summary>
        /// The == operator is defined here.
        /// </summary>
        /// <param name="lhs">The value on the left side of the operator.</param>
        /// <param name="rhs">The value on the right side of the operator.</param>
        /// <returns>True if the values are equal, false otherwise.</returns>
        public static bool operator ==(CDateTime lhs, CDateTime rhs)
        {
            if (object.ReferenceEquals(lhs, null))
            {
                // lhs == null == rhs => true.
                if (object.ReferenceEquals(rhs, null))
                {
                    return true;
                }

                // lhs == null != rhs => false.
                return false;
            }

            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The != operator is defined here.
        /// </summary>
        /// <param name="lhs">The value on the left side of the operator.</param>
        /// <param name="rhs">The value on the right side of the operator.</param>
        /// <returns>True if the values are not equal, false otherwise.</returns>
        public static bool operator !=(CDateTime lhs, CDateTime rhs)
        {
            return !(lhs == rhs);
        }

        // HIDING
        //[Obsolete("Do not use this method", false)]
        public override string ToString()
        {
            //return ToString( "M/dd/yyyy" );
            return this.ToStringWithDefaultFormatting();
        }

        /// <summary>
        /// Produces a string from the CDateTime with default formatting.
        /// </summary>
        /// <returns>The default-formatted CDateTime string.</returns>
        public string ToStringWithDefaultFormatting()
        {
            return this.ToString(default(LosConvert)); // This is the LosConvert version of null, to resolve ambiguity between that and a string-typed null.
        }

        public string ToString(string format)
        {
            if (!this.innerDt.HasValue)
            {
                return string.Empty;
            }

            try
            {
                return this.DateTimeForComputation.ToString(format); // Bad format string? You get an exception now!
            }
            catch (CBaseException)
            {
                return string.Empty;
            }
        }

        public string ToString(LosConvert losConvert)
        {
            if (!this.innerDt.HasValue)
            {
                return string.Empty;
            }

            try
            {
                if (null == losConvert)
                {
                    return this.ToString("M/dd/yyyy");
                }

                return losConvert.ToDateTimeString(this.DateTimeForComputation);
            }
            catch
            {
                return string.Empty;
            }
        }

        public string ToStringWithTime(string format)
        {
            if (!this.innerDt.HasValue)
            {
                return string.Empty;
            }

            try
            {
                return this.DateTimeForComputationWithTime.ToString(format);
            }
            catch (CBaseException)
            {
                return string.Empty;
            }
        }

        /// <summary>
        ///     This method returns whether the date is between the start and end dates entered.
        /// </summary>
        /// <param name="start">The start date of the range.</param>
        /// <param name="end">The end date of the range.</param>
        /// <returns></returns>
        public bool IsBetweenRange(DateTime start, DateTime end)
        {
            DateTime dateTimeForComputation = this.DateTimeForComputation;
            return dateTimeForComputation.CompareTo(start) >= 0 && dateTimeForComputation.CompareTo(end) <= 0;
        }

        /// <summary>
        /// Returns a boolean indicating whether a <see cref="CDateTime"/> object has same underlying date-time value (including time).
        /// </summary>
        /// <param name="dtB">The <see cref="CDateTime"/> object for comparison.</param>
        /// <returns>Boolean indicating whether a <see cref="CDateTime"/> object has same underlying date-time value (including time).</returns>
        public bool SameAs(CDateTime dtB)
        {
            return this.Equals(dtB);
        }

        /// <summary>
        /// Implement IEquatable.
        /// </summary>
        /// <param name="other">Instance that is checked for equality with this instance.</param>
        /// <returns>True if other is equal to this instance, false otherwise.</returns>
        public bool Equals(CDateTime other)
        {
            if (object.ReferenceEquals(other, null))
            {
                return false;
            }

            if (object.ReferenceEquals(this, other))
            {
                return true;
            }

            return this.innerDt.Equals(other.innerDt);
        }

        /// <summary>
        /// Override the Equals method inherited from the Object class.
        /// </summary>
        /// <param name="obj">Instance that is checked for equality with this instance.</param>
        /// <returns>True if the obj is equal to this instance, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as CDateTime);
        }

        /// <summary>
        /// Override of the GetHashCode method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash code for this instance.</returns>
        public override int GetHashCode()
        {
            return this.innerDt.GetHashCode();
        }

        /// <summary>
        /// Compares two CDateTimes, discarding the time component and only comparing the date portions:
        ///   - Less than zero: This instance is less than value.
        ///   - Zero: This instance is equal to value or both invalid.
        ///   - Greater than zero: This instance is greater than value, or value is null.
        /// </summary>
        /// <param name="rhsDate"></param>
        /// <param name="bIsInvalidSmallest">true:treat invalid as smallest, false: treat invalid as largest</param>
        /// <returns></returns>
        public int CompareTo(CDateTime rhsDate, bool bIsInvalidSmallest)
        {
            DateTime lhs = this.GetSafeDateTimeForComputation(bIsInvalidSmallest ? DateTime.MinValue : DateTime.MaxValue);
            DateTime rhs = rhsDate.GetSafeDateTimeForComputation(bIsInvalidSmallest ? DateTime.MinValue : DateTime.MaxValue);
            return lhs.CompareTo(rhs);
        }

        /// <summary>
        /// Compares two CDateTimes including the time component:
        ///   - Less than zero: This instance is less than value.
        ///   - Zero: This instance is equal to value or both invalid.
        ///   - Greater than zero: This instance is greater than value, or value is null.
        /// </summary>
        /// <param name="rhsDate"></param>
        /// <param name="bIsInvalidSmallest">true:treat invalid as smallest, false: treat invalid as largest</param>
        /// <returns></returns>
        public int CompareToWithTime(CDateTime rhsDate, bool bIsInvalidSmallest)
        {
            DateTime lhs = this.GetSafeDateTimeWithTimeForComputation(bIsInvalidSmallest ? DateTime.MinValue : DateTime.MaxValue);
            DateTime rhs = rhsDate.GetSafeDateTimeWithTimeForComputation(bIsInvalidSmallest ? DateTime.MinValue : DateTime.MaxValue);
            return lhs.CompareTo(rhs);
        }

        public CDateTime AddWorkingBankDays(int days, bool pushSundayHolidaysToMonday)
        {
            if (!this.IsValid)
            {
                return CDateTime.InvalidWrapValue;
            }

            DateTime dt = BankingHoliday.AddWorkingBankDays(this.DateTimeForComputation, days, pushSundayHolidaysToMonday);
            return Create(dt);
        }

        /// <summary>
        /// Adds the given number of business days to this instance. Skips over
        /// days the lender is not open for business.
        /// </summary>
        /// <param name="days">The number of business days to add.</param>
        /// <param name="brokerDB">The lender whose business days will be used.</param>
        /// <returns>A new CDateTime instance with the given number of business days added.</returns>
        /// <exception cref="CBaseException">Thrown when an entire year is added without encountering a business day.</exception>
        public CDateTime AddBusinessDays(byte days, BrokerDB brokerDB)
        {
            if (days < 0)
            {
                throw new ArgumentException("The days parameter cannot be negative.");
            }
            else if (brokerDB == null)
            {
                throw new ArgumentNullException("brokerDB");
            }
            else if (!this.IsValid)
            {
                return CDateTime.InvalidWrapValue;
            }

            var dateTime = this.DateTimeForComputationWithTime;

            int consecutiveDaysWithoutBusinessDay = 0;
            int businessDaysAdded = 0;

            while (businessDaysAdded < days)
            {
                dateTime = dateTime.AddDays(1);

                if (!brokerDB.IsBusinessDay(dateTime))
                {
                    consecutiveDaysWithoutBusinessDay++;

                    if (consecutiveDaysWithoutBusinessDay > 365)
                    {
                        var errorMessage = string.Format(
                            "Went an entire year without finding a business day. " +
                            "BrokerId: {0}.",
                            brokerDB.BrokerID);

                        throw CBaseException.GenericException(errorMessage);
                    }

                    continue;
                }

                consecutiveDaysWithoutBusinessDay = 0;
                businessDaysAdded++;
            }

            return Create(dateTime);
        }

        /// <summary>
        /// Subtract the indicated number of business days
        /// </summary>
        /// <param name="days">The number of days to subtract</param>
        /// <param name="brokerDB">Lender whose business days will be used.</param>
        /// <returns>A new CDateTime the indicated number of business days before this one.</returns>
        /// <exception cref="CBaseException">Thrown when an entire year of no business days occurs.</exception>
        public CDateTime SubtractBusinessDays(byte days, BrokerDB brokerDB)
        {
            if (days < 0)
            {
                throw new ArgumentException("The days parameter cannot be negative.");
            }
            else if (brokerDB == null)
            {
                throw new ArgumentNullException("brokerDB");
            }
            else if (!this.IsValid)
            {
                return CDateTime.InvalidWrapValue;
            }

            var dateTime = this.DateTimeForComputationWithTime;

            int consecutiveDaysWithoutBusinessDay = 0;
            int businessDaysSubtracted = 0;

            while (businessDaysSubtracted < days)
            {
                dateTime = dateTime.AddDays(-1);

                if (!brokerDB.IsBusinessDay(dateTime))
                {
                    consecutiveDaysWithoutBusinessDay++;

                    if (consecutiveDaysWithoutBusinessDay > 365)
                    {
                        var errorMessage = string.Format(
                            "Went an entire year without finding a business day. " +
                            "BrokerId: {0}.",
                            brokerDB.BrokerID);

                        throw CBaseException.GenericException(errorMessage);
                    }

                    continue;
                }

                consecutiveDaysWithoutBusinessDay = 0;
                businessDaysSubtracted++;
            }

            return Create(dateTime);
        }

        /// <summary>
        /// Never throws an exception
        /// </summary>
        /// <param name="sDate"></param>
        /// <param name="fallbackVal"></param>
        /// <returns></returns>
        public DateTime GetSafeDateTimeForComputation(DateTime fallbackVal)
        {
            try
            {
                if (!this.innerDt.HasValue)
                {
                    return fallbackVal;
                }

                return this.DateTimeForComputation;
            }
            catch
            {
                return fallbackVal;
            }
        }

        /// <summary>
        /// Never throws an exception
        /// </summary>
        /// <param name="sDate"></param>
        /// <param name="fallbackVal"></param>
        /// <returns></returns>
        public DateTime GetSafeDateTimeWithTimeForComputation(DateTime fallbackVal)
        {
            try
            {
                if (!this.innerDt.HasValue)
                {
                    return fallbackVal;
                }

                return this.DateTimeForComputationWithTime;
            }
            catch
            {
                return fallbackVal;
            }
        }
    }
}
