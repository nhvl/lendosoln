﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{
    /// <summary>
    /// 2011-09-16 - dd - Hopefully it will one day replace CGfeItem class
    /// </summary>
    internal class GfeFeeItem
    {
        public decimal Amount { get; private set; }

        public bool IsOriginatorComp { get; private set; }

        private int m_props;
        

        internal GfeFeeItem(decimal amount, int props)
        {
            Amount = amount;
            m_props = props;
           IsOriginatorComp = false;
        }

        internal GfeFeeItem(decimal amount, int props, bool isOriginatorComp) : this(amount, props)
        {
            IsOriginatorComp = true; 
        }

        public bool NotPocAndPdByLender
        {
            get { return LosConvert.GfeItemProps_NotPocAndPdByLender(m_props); }
        }

        public bool IsApr
        {
            get { return LosConvert.GfeItemProps_Apr(m_props); }
        }
        public bool IsPaidByLender
        {
            get { return LosConvert.GfeItemProps_PdByLenderOnly(m_props); }
        }

        public bool IsPaidBySeller
        {
            get { return LosConvert.GfeItemProps_PdBySellerOnly(m_props); }
        }

        public bool IsFhaAllowed
        {
            get { return LosConvert.GfeItemProps_FhaAllow(m_props); }
        }

        public bool IsPoc
        {
            get { return LosConvert.GfeItemProps_Poc(m_props); }
        }
    }
}
