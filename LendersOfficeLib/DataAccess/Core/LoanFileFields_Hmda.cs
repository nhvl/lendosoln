﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LendersOffice.Common;
    using LendersOffice.FFIEC;
    using LendersOffice.Migration;
    using LendersOffice.ObjLib.CustomAttributes;
    using LendersOffice.ObjLib.Hmda;
    using LendersOffice.UI;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;
    using LendersOffice.Integration.Underwriting;
    using LendersOffice.Constants;

    public partial class CPageBase
    {
        [DependsOn(nameof(sHmdaBalloonPaymentT), nameof(sGfeIsBalloon))]
        [DevNote("Calculated wrapper field for balloon payment type")]
        public sHmdaBalloonPaymentT sHmdaBalloonPaymentT
        {
            get
            {
                if (this.sGfeIsBalloon)
                {
                    return sHmdaBalloonPaymentT.BalloonPayment;
                }
                else
                {
                    return sHmdaBalloonPaymentT.NoBalloonPayment;
                }
            }
        }

        [DependsOn(nameof(sHmdaInterestOnlyPaymentT), nameof(sIsIOnly))]
        [DevNote("Calculated wrapper field for interest only payment")]
        public sHmdaInterestOnlyPaymentT sHmdaInterestOnlyPaymentT
        {
            get
            {
                if (this.sIsIOnly)
                {
                    return sHmdaInterestOnlyPaymentT.InterestOnlyPayments;
                }
                else
                {
                    return sHmdaInterestOnlyPaymentT.NoInterestOnlyPayments;
                }
            }
        }

        [DependsOn(nameof(sHmdaNegativeAmortizationT), nameof(sIsNegAmort))]
        [DevNote("Calculated wrapper field for negative amortization type")]
        public sHmdaNegativeAmortizationT sHmdaNegativeAmortizationT
        {
            get
            {
                if (this.sIsNegAmort)
                {
                    return sHmdaNegativeAmortizationT.NegativeAmortization;
                }
                else
                {
                    return sHmdaNegativeAmortizationT.NoNegativeAmortization;
                }
            }
        }

        [DependsOn(nameof(sHmdaLoanAmount), nameof(sHmdaLoanAmountLckd), nameof(sIsLineOfCredit), nameof(sFinalLAmt), nameof(sCreditLineAmt), nameof(sLoanVersionT), 
            nameof(sHmdaActionTaken), nameof(sHmdaActionTakenT), nameof(sNMLSApplicationAmount), nameof(sPurchaseAdviceUnpaidPBal_Purchasing))]
        [DevNote("Calculated wrapper field for hmda loan amount")]
        public decimal sHmdaLoanAmount
        {
            get
            {
                if (this.sHmdaLoanAmountLckd)
                {
                    return this.GetMoneyField("sHmdaLoanAmount", defaultValue: 0M);
                }

                if (this.sIsLineOfCredit)
                {
                    return this.sCreditLineAmt;
                }

                var isAtLeastV22 = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement);

                var hmdaActionTaken = this.sHmdaActionTaken;
                var hmdaActionTakenT = this.sHmdaActionTakenT;

                if (isAtLeastV22 && DeniedWithdrawnIncompleteActionsTakenT.Contains(hmdaActionTakenT))
                {
                    return this.sNMLSApplicationAmount;
                }
                else if (!isAtLeastV22 && DeniedWithdrawnIncompleteActionsTaken.Any(status => string.Equals(status, hmdaActionTaken, StringComparison.OrdinalIgnoreCase)))
                {
                    return this.sNMLSApplicationAmount;
                }

                if (isAtLeastV22 && hmdaActionTakenT == HmdaActionTaken.PurchasedLoan)
                {
                    return this.sPurchaseAdviceUnpaidPBal_Purchasing;
                }
                else if (!isAtLeastV22 && string.Equals(hmdaActionTaken, ConstApp.HmdaPurchasedActionTaken, StringComparison.OrdinalIgnoreCase))
                {
                    return this.sPurchaseAdviceUnpaidPBal_Purchasing;
                }

                return this.sFinalLAmt;
            }
            set { this.SetNullableMoneyField("sHmdaLoanAmount", value); }
        }

        [DependsOn]
        [DevNote("Gets a value indicating whether the loan amount under HMDA regulations is locked to user input.")]
        public bool sHmdaLoanAmountLckd
        {
            get { return this.GetBoolField("sHmdaLoanAmountLckd", defaultValue: false); }
            set { this.SetNullableBoolField("sHmdaLoanAmountLckd", value); }
        }

        public string sHmdaLoanAmount_rep
        {
            get { return ToMoneyString(() => sHmdaLoanAmount); }
            set { this.sHmdaLoanAmount = ToMoney(value); }
        }

        [DependsOn]
        [DevNote("Lockbox for sHmdaPreapprovalT")]
        public bool sHmdaPreapprovalTLckd
        {
            get
            {
                return this.GetBoolField("sHmdaPreapprovalTLckd");
            }

            set
            {
                this.SetBoolField("sHmdaPreapprovalTLckd", value);
            }
        }

        [DependsOn(nameof(sHmdaPreapprovalT), nameof(sHmdaPreapprovalTLckd), nameof(sLPurposeT), nameof(sHmdaLenderHasPreapprovalProgram), nameof(sHmdaBorrowerRequestedPreapproval), nameof(sHmdaActionD))]
        public E_sHmdaPreapprovalT sHmdaPreapprovalT
        {
            get
            {
                if (this.sHmdaPreapprovalTLckd)
                {
                    return (E_sHmdaPreapprovalT)GetTypeIndexField("sHmdaPreapprovalT");
                }

                if ((this.sLPurposeT == E_sLPurposeT.Purchase || this.sLPurposeT == E_sLPurposeT.ConstructPerm) &&
                     (this.sHmdaLenderHasPreapprovalProgram))
                {
                    if (this.sHmdaBorrowerRequestedPreapproval)
                    {
                        return E_sHmdaPreapprovalT.PreapprovalRequested;
                    }
                    else
                    {
                        return E_sHmdaPreapprovalT.PreapprovalNotRequested;
                    }
                }

                if (this.sHmdaActionD.IsValid && this.sHmdaActionD.DateTimeForComputation.Year < 2018)
                {
                    return E_sHmdaPreapprovalT.NotApplicable;
                }

                return E_sHmdaPreapprovalT.PreapprovalNotRequested;
            }

            set
            {
                SetTypeIndexField("sHmdaPreapprovalT", (int)value);
            }
        }


        private readonly string sLastDiscAPR_Description = "Last Disclosed APR";
        private readonly string sApr_Description = "APR";
        private readonly string InvalidAPR_Description = "Invalid_APR";

        [DependsOn(nameof(sHmdaAprRateSpreadLckd), nameof(sHmdaAprRateSpreadAprToUse), nameof(sInterestRateSetD), nameof(sFinMethT), nameof(sDue), nameof(sRAdj1stCapMon), nameof(sHmdaAprRateSpread), nameof(sHmdaActionD))]
        [DevNote("Formats a string that shows how sHmdaAprRateSpread was calculated.")]
        public string sHmdaAprRateSpreadCalcMessage
        {
            get
            {
                if (this.sHmdaAprRateSpreadLckd)
                {
                    return string.Empty;
                }

                var aprUsed = this.sHmdaAprRateSpreadAprToUse;
                string aprDesc = aprUsed.Item1;
                string aprVal = this.m_convertLos.ToRateString(aprUsed.Item2);

                string chosenDate = this.sInterestRateSetD_rep;
                decimal value = YieldTable.LookUp(sFinMethT, chosenDate, sDue_rep, sRAdj1stCapMon_rep, sHmdaActionD);
                string apor = value == decimal.MinValue ? string.Empty : this.m_convertLos.ToRateString(value);

                if (aprDesc == this.InvalidAPR_Description)
                {
                    aprDesc = "APR";
                    aprVal = string.Empty;
                }

                return $"{this.sHmdaAprRateSpread} APR Rate Spread = {aprVal} {aprDesc} - {apor} APOR";
            }
        }

        [DependsOn(nameof(sRLckdD), nameof(sSubmitD), nameof(sClosedD))]
        private string sHmdaAprRateSpreadChosenDate
        {
            get
            {
                CDateTime chosenDate = null;
                if (this.sRLckdD.IsValid)
                {
                    chosenDate = this.sRLckdD;
                }
                else if (this.sSubmitD.IsValid)
                {
                    chosenDate = this.sSubmitD;
                }
                else if (this.sClosedD.IsValid)
                {
                    chosenDate = this.sClosedD;
                }

                if (chosenDate == null)
                {
                    return string.Empty;
                }
                else
                {
                    return this.m_convertLos.ToDateTimeString(chosenDate.DateTimeForComputation, false);
                }
            }
        }

        [DependsOn(nameof(sLastDiscAPR), nameof(sDisclosureRegulationT), nameof(sUseGFEDataForSCFields), nameof(sSettlementChargesExportSource), nameof(sApr))]
        private Tuple<string, decimal> sHmdaAprRateSpreadAprToUse
        {
            get
            {
                try
                {
                    if (this.sLastDiscAPR > 0)
                    {
                        if (this.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID ||
                            (!this.sUseGFEDataForSCFields && this.sSettlementChargesExportSource == E_SettlementChargesExportSource.SETTLEMENT))
                        {
                            return new Tuple<string, decimal>(this.sLastDiscAPR_Description, this.sLastDiscAPR);
                        }
                        else
                        {
                            return new Tuple<string, decimal>(this.sApr_Description, this.sApr);
                        }
                    }
                    else
                    {
                        decimal apr = this.sApr;
                        if (apr > 0)
                        {
                            return new Tuple<string, decimal>(this.sApr_Description, apr);
                        }
                        else
                        {
                            return new Tuple<string, decimal>(this.InvalidAPR_Description, 0);
                        }
                    }
                }
                catch (CBaseException)
                {
                    // Return invalid if we can't calculate. Will most likely occur when calculating the APR.
                    return new Tuple<string, decimal>(this.InvalidAPR_Description, 0);
                }
            }
        }

        [DependsOn(nameof(sHmdaAprRateSpreadLckd), nameof(sHmdaAprRateSpread), nameof(sHmdaAprRateSpreadChosenDate), nameof(sFinMethT), nameof(sDue),
            nameof(sRAdj1stCapMon), nameof(sHmdaAprRateSpreadAprToUse), nameof(sHmdaActionTaken), nameof(sHmdaActionTakenT), nameof(sLoanVersionT), nameof(sHmdaActionD), nameof(sHmdaReverseMortgageT))]
        public string sHmdaAprRateSpread
        {
            get
            {
                if (sHmdaAprRateSpreadLckd)
                {
                    return GetStringVarCharField("sHmdaAprRateSpread");
                }

                if (this.sHmdaReverseMortgageT == sHmdaReverseMortgageT.ReverseMortgage)
                {
                    return LosConvert.HmdaNotApplicableString;
                }

                bool isAtLeastV22 = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement);
                // 242730 - Only report if action taken is loan originated. Still respect locked value.
                if ((isAtLeastV22 && this.sHmdaActionTakenT != HmdaActionTaken.LoanOriginated && this.sHmdaActionTakenT != HmdaActionTaken.ApplicationApprovedButNotAccepted)
                    || (!isAtLeastV22 && sHmdaActionTaken != "Loan originated" && sHmdaActionTaken != "App approved but not accepted by applicant"))
                {
                    return LosConvert.HmdaNotApplicableString;
                }
                else
                { 
                    // 9/13/2013 dd - OPM 67223
                    try
                    {
                        string chosenDate = this.sHmdaAprRateSpreadChosenDate;
                        decimal value = YieldTable.LookUp(sFinMethT, chosenDate, sDue_rep, sRAdj1stCapMon_rep, sHmdaActionD);

                        var aprToUse = this.sHmdaAprRateSpreadAprToUse;
                        if (value != decimal.MinValue && aprToUse.Item1 != this.InvalidAPR_Description)
                        {
                            return this.m_convertLos.ToRateString(aprToUse.Item2 - value);
                        }
                    }
                    catch (CBaseException)
                    {
                        // 9/13/2013 dd - Cannot compute. Just return empty string.
                    }
                }
                // Different HMDA action or value is decimal.MinValue or apr has invalid description or exception thrown
                return string.Empty;
            }
            set { SetStringVarCharField("sHmdaAprRateSpread", value); }
        }

        [DependsOn(nameof(sHmdaAprRateSpread))]
        public decimal sHmdaAprRateSpreadForULDDExport
        {
            get
            {
                return m_convertLos.ToDecimal(this.sHmdaAprRateSpread);
            }
        }

        [DependsOn(nameof(sHmdaAprRateSpread))]
        [DevNote("The HMDA APR rate spread as defined by FNMA guidelines.")]
        public string sHmdaAprRateSpreadFannie
        {
            get
            {
                // APR rate spread is valid for FNMA when the value 
                // is a decimal greater than or equal to 0.
                var aprRateSpread = this.sHmdaAprRateSpread;
                if (string.IsNullOrEmpty(aprRateSpread) ||
                    aprRateSpread == LosConvert.HmdaNotApplicableString ||
                    this.m_convertLos.ToRate(aprRateSpread) < 0M)
                {
                    return string.Empty;
                }

                return aprRateSpread;
            }
        }

        [DependsOn]
        public bool sHmdaAprRateSpreadLckd
        {
            get
            {

                return GetBoolField("sHmdaAprRateSpreadLckd");
            }
            set { SetBoolField("sHmdaAprRateSpreadLckd", value); }
        }

        [DependsOn]
        public bool sHmdaReportAsHoepaLoan
        {
            get { return GetBoolField("sHmdaReportAsHoepaLoan"); }
            set { SetBoolField("sHmdaReportAsHoepaLoan", value); }
        }

        [DependsOn(nameof(sHmdaLienT), nameof(sLienPosT), nameof(sHmdaActionTaken), nameof(sHmdaLienTLckd), nameof(sLoanVersionT), nameof(sHmdaActionD),
            nameof(sHmdaActionTakenT), nameof(sLoanVersionT))]
        public E_sHmdaLienT sHmdaLienT
        {
            get
            {
                if (sHmdaLienTLckd)
                {
                    return (E_sHmdaLienT)GetTypeIndexField("sHmdaLienT");
                }

                // OPM 451705 - Don't return Not Applicable for migrated loans after 01/01/2018.
                // BB - When correspondent support is added with a purchased loan indicator (~Q4 2013), look-up that indicator instead of the HMDA Action Taken
                DateTime dateToCompare = sHmdaActionD.IsValid ? sHmdaActionD.DateTimeForComputation : DateTime.Today;
                bool isAtLeastV22 = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement);
                if ((isAtLeastV22 && (dateToCompare < new DateTime(2018, 1, 1) && sHmdaActionTakenT == HmdaActionTaken.PurchasedLoan))
                    || (!isAtLeastV22 && string.Equals(sHmdaActionTaken, ConstApp.HmdaPurchasedActionTaken, StringComparison.OrdinalIgnoreCase)))
                {
                    return E_sHmdaLienT.NotApplicablePurchaseLoan;
                }

                switch (sLienPosT)
                {
                    case E_sLienPosT.First: return E_sHmdaLienT.FirstLien;
                    case E_sLienPosT.Second: return E_sHmdaLienT.SubordinateLien;
                    default:
                        throw new UnhandledEnumException(sLienPosT);
                }
            }
            set { SetTypeIndexField("sHmdaLienT", (int)value); }
        }
        [DependsOn]
        public bool sHmdaLienTLckd
        {
            get { return GetBoolField("sHmdaLienTLckd"); }
            set { SetBoolField("sHmdaLienTLckd", value); }
        }

        [DependsOn(nameof(sHmdaPropTLckd), nameof(sHmdaPropT), nameof(sGseSpT), nameof(sUnitsNum))]
        public E_sHmdaPropT sHmdaPropT
        {
            get
            {
                if (sHmdaPropTLckd)
                {
                    return (E_sHmdaPropT)GetTypeIndexField("sHmdaPropT");
                }
                else
                {
                    // 1/27/2010 dd - OPM 45117
                    // Automatically compute sHmdaPropT based on sGseSpT and sUnitNums
                    // If sGseSpT is anything but 4 manufactured types and sUnitNums <= 4  ==> sHmdaPropT = 1-4 SFR
                    // If sGseSpT is anything but 4 manufactured types and sUnitNums > 4 ==> sHmdaPropT = Multifamily
                    // If sGseSpT is 4 manufactured types ==> sHmdaPropT = ManufactureHousing
                    // If sGseSpT is Leave Blank ==> sHmdaPropT = Leave Blank.
                    switch (sGseSpT)
                    {
                        case E_sGseSpT.LeaveBlank:
                            return E_sHmdaPropT.LeaveBlank;
                        case E_sGseSpT.Attached:
                        case E_sGseSpT.Condominium:
                        case E_sGseSpT.Cooperative:
                        case E_sGseSpT.Detached:
                        case E_sGseSpT.DetachedCondominium:
                        case E_sGseSpT.HighRiseCondominium:
                        case E_sGseSpT.Modular:
                        case E_sGseSpT.PUD:
                            if (sUnitsNum <= 4)
                            {
                                return E_sHmdaPropT.OneTo4Family;
                            }
                            else
                            {
                                return E_sHmdaPropT.MultiFamiliy;
                            }
                        case E_sGseSpT.ManufacturedHomeCondominium:
                        case E_sGseSpT.ManufacturedHomeMultiwide:
                        case E_sGseSpT.ManufacturedHousing:
                        case E_sGseSpT.ManufacturedHousingSingleWide:
                            return E_sHmdaPropT.ManufacturedHousing;
                        default:
                            throw new UnhandledEnumException(sGseSpT);
                    }
                }

            }
            set { SetTypeIndexField("sHmdaPropT", (int)value); }
        }
        [DependsOn]
        public bool sHmdaPropTLckd
        {
            get { return GetBoolField("sHmdaPropTLckd"); }
            set { SetBoolField("sHmdaPropTLckd", value); }
        }
        [DependsOn(nameof(sHmdaPurchaser2004T), nameof(sHmdaPurchaser2004TLckd), nameof(sHmdaPurchaser2015T), nameof(sHmdaActionD), nameof(sLPurchaseD), nameof(sInvestorRolodexId),
            nameof(sfGetInvestorInfo), nameof(sHmdaActionTakenT), nameof(sLoanVersionT))]
        public E_sHmdaPurchaser2004T sHmdaPurchaser2004T
        {
            get
            {
                E_sHmdaPurchaser2004T returnVal;
                bool isAtLeastV22 = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement);
                if (this.sHmdaPurchaser2004TLckd)
                {
                    return (E_sHmdaPurchaser2004T)GetTypeIndexField("sHmdaPurchaser2004T");
                }
                else if ((isAtLeastV22 && this.sHmdaActionTakenT != HmdaActionTaken.LoanOriginated)
                        || (!isAtLeastV22 && sHmdaActionTaken != "Loan originated"))
                {
                    return E_sHmdaPurchaser2004T.LeaveBlank;
                }
                else if(!Tools.HmdaPurchaserMapping2015To2004.TryGetValue(this.sHmdaPurchaser2015T, out returnVal))
                {
                    Tools.LogError("Cannot convert the sHmdaPurchaser2015T enum value " + (int)this.sHmdaPurchaser2015T + "to sHmdaPurchaser2004T.");
                }

                return returnVal;
            }
            set { SetTypeIndexField("sHmdaPurchaser2004T", (int)value); }
        }

        [DependsOn]
        public bool sHmdaPurchaser2004TLckd
        {
            get { return GetBoolField("sHmdaPurchaser2004TLckd"); }
            set { SetBoolField("sHmdaPurchaser2004TLckd", value); }
        }

        [DependsOn(nameof(sHmdaPurchaser2015TLckd), nameof(sHmdaPurchaser2015T), nameof(sHmdaActionTaken), nameof(sHmdaActionD), nameof(sLPurchaseD), nameof(sfGetInvestorInfo),
            nameof(sHmdaActionTakenT), nameof(sLoanVersionT))]
        [DevNote("The HMDA purchaser type as defined by 2015 guidelines.")]
        public HmdaPurchaser2015T sHmdaPurchaser2015T
        {
            get
            {
                if(this.sHmdaPurchaser2015TLckd)
                {
                    return (HmdaPurchaser2015T)this.GetCountField("sHmdaPurchaser2015T");
                }
                else
                {
                    bool isAtLeastV22 = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement);
                    if ((isAtLeastV22 && this.sHmdaActionTakenT != HmdaActionTaken.LoanOriginated && this.sHmdaActionTakenT != HmdaActionTaken.PurchasedLoan)
                        || (!isAtLeastV22 && !(this.sHmdaActionTaken.Equals("Loan originated") || this.sHmdaActionTaken.Equals("Loan purchased by your institution"))))
                    {
                        return HmdaPurchaser2015T.NA;
                    }
                    else if(this.sHmdaActionD.IsValid && this.sLPurchaseD.IsValid && this.sHmdaActionD.DateTimeForComputation.Year == this.sLPurchaseD.DateTimeForComputation.Year)
                    {
                        return GetInvestorInfo().HmdaPurchaser2015T;
                    }
                }

                return HmdaPurchaser2015T.NA;
            }
            set
            {
                if(value != HmdaPurchaser2015T.LifeInsCreditUnion || this.sHmdaPurchaser2015T == value)
                {
                    SetCountField("sHmdaPurchaser2015T", (int)value);
                }
            }
        }

        [DependsOn]
        [DevNote("Indicates whether the 2015 HMDA purchaser type is calculated or from user input.")]
        public bool sHmdaPurchaser2015TLckd
        {
            get { return GetBoolField("sHmdaPurchaser2015TLckd"); }
            set { SetBoolField("sHmdaPurchaser2015TLckd", value); }
        }

        [DependsOn]
        public bool sHmdaExcludedFromReport
        {
            get { return GetBoolField("sHmdaExcludedFromReport"); }
            set { SetBoolField("sHmdaExcludedFromReport", value); }
        }

        [DependsOn]
        public bool sHmdaReportAsHomeImprov
        {
            get { return GetBoolField("sHmdaReportAsHomeImprov"); }
            set { SetBoolField("sHmdaReportAsHomeImprov", value); }
        }

        [DependsOn]
        public string sHmdaMsaNum
        {
            get { return GetStringVarCharField("sHmdaMsaNum"); }
            set { SetStringVarCharField("sHmdaMsaNum", value); }
        }

        [DependsOn]
        public string sHmdaCountyCode
        {
            get { return GetStringVarCharField("sHmdaCountyCode"); }
            set { SetStringVarCharField("sHmdaCountyCode", value); }
        }

        [DependsOn]
        public string sHmdaCensusTract
        {
            get { return GetStringVarCharField("sHmdaCensusTract"); }
            set { SetStringVarCharField("sHmdaCensusTract", value); }
        }

        [DependsOn]
        public string sHmdaPurchaser
        {
            get { return GetStringVarCharField("sHmdaPurchaser"); }
            set { SetStringVarCharField("sHmdaPurchaser", value); }
        }

        [DependsOn(nameof(sHmdaActionTaken), nameof(sHmdaActionTakenT), nameof(sLoanVersionT))]
        [LqbInputModel(type: InputFieldType.StringWithDropDown)]
        public string sHmdaActionTaken
        {
            get
            {
                if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement))
                {
                    return Tools.MapHmdaActionTakenToString(this.sHmdaActionTakenT);
                }

                return GetStringVarCharField("sHmdaActionTaken");
            }

            set
            {
                if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement))
                {
                    sHmdaActionTakenT = Tools.MapStringToHmdaActionTaken(value);    // Throws on invalid value.
                }

                SetStringVarCharField("sHmdaActionTaken", value);
            }
        }

        [DependsOn(nameof(sLoanVersionT), nameof(sHmdaActionTaken), nameof(sHmdaActionTakenT))]
        [DevNote("Enum backed HMDA action taken field.")]
        public HmdaActionTaken sHmdaActionTakenT
        {
            get
            {
                if (!LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement))
                {
                    return Tools.MapStringToHmdaActionTaken(this.sHmdaActionTaken, HmdaActionTaken.Blank);
                }

                return GetTypeIndexField("sHmdaActionTakenT", HmdaActionTaken.Blank);
            }
            set { SetTypeIndexField("sHmdaActionTakenT", (int)value); }
        }

        [DependsOn]
        public CDateTime sHmdaActionD
        {
            get { return GetDateTimeField("sHmdaActionD"); }
            set { SetDateTimeField("sHmdaActionD", value); }
        }
        public string sHmdaActionD_rep
        {
            get { return GetDateTimeField_rep("sHmdaActionD"); }
            set { SetDateTimeField_rep("sHmdaActionD", value); }
        }

        [DependsOn(nameof(sHmdaDenialReason1), nameof(sHmdaDenialReasonT1), nameof(sLoanVersionT))]
        [LqbInputModelAttribute(type: InputFieldType.StringWithDropDown)]
        public string sHmdaDenialReason1
        {
            get
            {
                if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement))
                {
                    return Tools.MapHmdaDenialReasonToString(this.sHmdaDenialReasonT1);
                }

                return GetStringVarCharField("sHmdaDenialReason1");
            }

            set
            {
                if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement))
                {
                    sHmdaDenialReasonT1 = Tools.MapStringToHmdaDenialReason(value);    // Throws if invalid value;
                }

                SetStringVarCharField("sHmdaDenialReason1", value);
            }
        }

        [DependsOn(nameof(sHmdaDenialReason2), nameof(sHmdaDenialReasonT2), nameof(sLoanVersionT))]
        [LqbInputModelAttribute(type: InputFieldType.StringWithDropDown)]
        public string sHmdaDenialReason2
        {
            get
            {
                if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement))
                {
                    return Tools.MapHmdaDenialReasonToString(this.sHmdaDenialReasonT2);
                }

                return GetStringVarCharField("sHmdaDenialReason2");
            }

            set
            {
                if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement))
                {
                    sHmdaDenialReasonT2 = Tools.MapStringToHmdaDenialReason(value);    // Throws if invalid value;
                }

                SetStringVarCharField("sHmdaDenialReason2", value);
            }
        }

        [DependsOn(nameof(sHmdaDenialReason3), nameof(sHmdaDenialReasonT3), nameof(sLoanVersionT))]
        [LqbInputModelAttribute(type: InputFieldType.StringWithDropDown)]
        public string sHmdaDenialReason3
        {
            get
            {
                if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement))
                {
                    return Tools.MapHmdaDenialReasonToString(this.sHmdaDenialReasonT3);
                }

                return GetStringVarCharField("sHmdaDenialReason3");
            }

            set
            {
                if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement))
                {
                    sHmdaDenialReasonT3 = Tools.MapStringToHmdaDenialReason(value);    // Throws if invalid value;
                }

                SetStringVarCharField("sHmdaDenialReason3", value);
            }
        }

        [DevNote("The fourth HMDA denial reason.")]
        [DependsOn(nameof(sHmdaDenialReason4), nameof(sHmdaDenialReasonT4), nameof(sLoanVersionT))]
        [LqbInputModelAttribute(type: InputFieldType.StringWithDropDown)]
        public string sHmdaDenialReason4
        {
            get
            {
                if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement))
                {
                    return Tools.MapHmdaDenialReasonToString(this.sHmdaDenialReasonT4);
                }

                return GetStringVarCharField("sHmdaDenialReason4");
            }

            set
            {
                if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement))
                {
                    sHmdaDenialReasonT4 = Tools.MapStringToHmdaDenialReason(value);    // Throws if invalid value;
                }

                SetStringVarCharField("sHmdaDenialReason4", value);
            }
        }

        [DependsOn]
        [DevNote("List of HMDA Denial Reason enum values stored as a string of comma separated int values.")]
        private string sHmdaDenialReasons
        {
            get { return GetStringVarCharField("sHmdaDenialReasons"); }

            set { SetStringVarCharField("sHmdaDenialReasons", value); }
        }


        private List<HmdaDenialReason> xHmdaDenialReasonsList = null;
        [DependsOn(nameof(sHmdaDenialReasons))]
        [DevNote("List of HMDA Denial Reason enum values.")]
        public List<HmdaDenialReason> sHmdaDenialReasonsList
        {
            get
            {
                if (xHmdaDenialReasonsList == null)
                {
                    xHmdaDenialReasonsList = new List<HmdaDenialReason>();
                    if (!string.IsNullOrWhiteSpace(sHmdaDenialReasons))
                    {
                        xHmdaDenialReasonsList.AddRange(sHmdaDenialReasons.Split(',').Select(s => (HmdaDenialReason)Enum.Parse(typeof(HmdaDenialReason), s)));
                    }

                    // List must have at least 4 elements so sHmdaDenialReason1-4 and sHmdaDenialReasonT1-4 can calculate their values.
                    while (xHmdaDenialReasonsList.Count < 4)
                    {
                        xHmdaDenialReasonsList.Add(HmdaDenialReason.Blank);
                    }
                }

                return xHmdaDenialReasonsList;
            }

            set
            {
                xHmdaDenialReasonsList = value;

                // List must have at least 4 elements so sHmdaDenialReason1-4 and sHmdaDenialReasonT1-4 can calculate their values.
                while (xHmdaDenialReasonsList.Count < 4)
                {
                    xHmdaDenialReasonsList.Add(HmdaDenialReason.Blank);
                }

                sHmdaDenialReasons = string.Join(",", xHmdaDenialReasonsList.Select(hdr => hdr.ToString("d")));
            }
        }

        [DependsOn(nameof(sHmdaDenialReasonsList), nameof(sHmdaActionTakenT))]
        [DevNote("Enum backed first HMDA denial reason.")]
        public HmdaDenialReason sHmdaDenialReasonT1
        {
            get
            {
                if(sHmdaActionTakenT != HmdaActionTaken.ApplicationDenied && sHmdaActionTakenT != HmdaActionTaken.PreapprovalRequestDenied)
                {
                    return HmdaDenialReason.NotApplicable;
                }

                return sHmdaDenialReasonsList[0];
            }

            set { sHmdaDenialReasonsList[0] = value; }
        }

        [DependsOn(nameof(sHmdaDenialReasonsList), nameof(sHmdaActionTakenT))]
        [DevNote("Enum backed second HMDA denial reason.")]
        public HmdaDenialReason sHmdaDenialReasonT2
        {
            get
            {
                if (sHmdaActionTakenT != HmdaActionTaken.ApplicationDenied && sHmdaActionTakenT != HmdaActionTaken.PreapprovalRequestDenied)
                {
                    return HmdaDenialReason.Blank;
                }

                return sHmdaDenialReasonsList[1];
            }

            set { sHmdaDenialReasonsList[1] = value; }
        }

        [DependsOn(nameof(sHmdaDenialReasonsList), nameof(sHmdaActionTakenT))]
        [DevNote("Enum backed third HMDA denial reason.")]
        public HmdaDenialReason sHmdaDenialReasonT3
        {
            get
            {
                if (sHmdaActionTakenT != HmdaActionTaken.ApplicationDenied && sHmdaActionTakenT != HmdaActionTaken.PreapprovalRequestDenied)
                {
                    return HmdaDenialReason.Blank;
                }

                return sHmdaDenialReasonsList[2];
            }

            set { sHmdaDenialReasonsList[2] = value; }
        }

        [DependsOn(nameof(sHmdaDenialReasonsList), nameof(sHmdaActionTakenT))]
        [DevNote("Enum backed fourth HMDA denial reason.")]
        public HmdaDenialReason sHmdaDenialReasonT4
        {
            get
            {
                if (sHmdaActionTakenT != HmdaActionTaken.ApplicationDenied && sHmdaActionTakenT != HmdaActionTaken.PreapprovalRequestDenied)
                {
                    return HmdaDenialReason.Blank;
                }

                return sHmdaDenialReasonsList[3];
            }

            set { sHmdaDenialReasonsList[3] = value; }
        }

        [DependsOn]
        [DevNote("User entered HMDA denial reason. Should only have a value if one of the sHmdaDenialReasonTX vars is set to Other.")]
        public string sHmdaDenialReasonOtherDescription
        {
            get { return GetStringVarCharField("sHmdaDenialReasonOtherDescription"); }
            set { SetStringVarCharField("sHmdaDenialReasonOtherDescription", value); }
        }

        [DependsOn]
        public bool sHmdaLoanDenied
        {
            get { return GetBoolField("sHmdaLoanDenied"); }
            set { SetBoolField("sHmdaLoanDenied", value); }
        }

        [DependsOn]
        public bool sHmdaDeniedFormDone
        {
            get { return GetBoolField("sHmdaDeniedFormDone"); }
            set { SetBoolField("sHmdaDeniedFormDone", value); }
        }

        [DependsOn]
        public bool sHmdaCounterOfferMade
        {
            get { return GetBoolField("sHmdaCounterOfferMade"); }
            set { SetBoolField("sHmdaCounterOfferMade", value); }
        }

        [DependsOn]
        public string sHmdaLoanDeniedBy
        {
            get { return GetStringVarCharField("sHmdaLoanDeniedBy"); }
            set { SetStringVarCharField("sHmdaLoanDeniedBy", value); }
        }

        [DependsOn]
        public CDateTime sHmdaDeniedFormDoneD
        {
            get { return GetDateTimeField("sHmdaDeniedFormDoneD"); }
        }
        public string sHmdaDeniedFormDoneD_rep
        {
            get { return GetDateTimeField_rep("sHmdaDeniedFormDoneD"); }
            set { SetDateTimeField_rep("sHmdaDeniedFormDoneD", value); }
        }

        [DependsOn]
        public string sHmdaDeniedFormDoneBy
        {
            get { return GetStringVarCharField("sHmdaDeniedFormDoneBy"); }
            set { SetStringVarCharField("sHmdaDeniedFormDoneBy", value); }
        }

        [DependsOn]
        public CDateTime sHmdaCounterOfferMadeD
        {
            get { return GetDateTimeField("sHmdaCounterOfferMadeD"); }
            set { SetDateTimeField("sHmdaCounterOfferMadeD", value); }
        }
        public string sHmdaCounterOfferMadeD_rep
        {
            get { return GetDateTimeField_rep("sHmdaCounterOfferMadeD"); }
            set { SetDateTimeField_rep("sHmdaCounterOfferMadeD", value); }
        }

        [DependsOn]
        public string sHmdaCounterOfferMadeBy
        {
            get { return GetStringVarCharField("sHmdaCounterOfferMadeBy"); }
            set { SetStringVarCharField("sHmdaCounterOfferMadeBy", value); }
        }

        [DependsOn]
        [LqbInputModelAttribute(type: InputFieldType.TextArea)]
        public string sHmdaCounterOfferDetails
        {
            get { return GetLongTextField("sHmdaCounterOfferDetails"); }
            set { SetLongTextField("sHmdaCounterOfferDetails", value); }
        }

        [DependsOn]
        [DevNote("Indicates whether the Lender has a preapproval Program as defined by HMDA")]
        public bool sHmdaLenderHasPreapprovalProgram
        {
            get
            {
                return this.GetBoolField("sHmdaLenderHasPreapprovalProgram");
            }

            set
            {
                this.SetBoolField("sHmdaLenderHasPreapprovalProgram", value);
            }
        }

        [DependsOn]
        [DevNote("Indicates whether the borrower requested preapproval")]
        public bool sHmdaBorrowerRequestedPreapproval
        {
            get
            {
                return this.GetBoolField("sHmdaBorrowerRequestedPreapproval");
            }

            set
            {
                this.SetBoolField("sHmdaBorrowerRequestedPreapproval", value);
            }
        }

        [DependsOn]
        public string sHmdaStateCode
        {
            get { return GetStringVarCharField("sHmdaStateCode"); }
            set { SetStringVarCharField("sHmdaStateCode", value); }
        }

        [DependsOn]
        [DevNote("The legal entity identifier (LEI) for the loan, populated from the broker's value.")]
        public string sLegalEntityIdentifier
        {
            get { return this.GetStringVarCharField(nameof(sLegalEntityIdentifier)); }
            set { this.SetStringVarCharField(nameof(sLegalEntityIdentifier), value); }
        }

        [DependsOn(nameof(sUniversalLoanIdentifier), nameof(sUniversalLoanIdentifierLckd), nameof(sLNm), nameof(sLegalEntityIdentifier))]
        [DevNote("The universal identifier for the loan.")]
        public string sUniversalLoanIdentifier
        {
            get
            {
                if (this.sUniversalLoanIdentifierLckd)
                {
                    return this.GetStringVarCharField(nameof(sUniversalLoanIdentifier));
                }

                return Tools.GenerateUniversalLoanIdentifier(this.sLegalEntityIdentifier, this.sLNm);
            }
            set { this.SetStringVarCharField(nameof(sUniversalLoanIdentifier), value); }
        }

        [DependsOn]
        [DevNote("Determines whether sUniversalLoanIdentifier is calculated or from user input.")]
        public bool sUniversalLoanIdentifierLckd
        {
            get { return this.GetBoolField(nameof(sUniversalLoanIdentifierLckd)); }
            set { this.SetNullableBoolField(nameof(sUniversalLoanIdentifierLckd), value); }
        }

        [DependsOn(nameof(sLegalEntityIdentifier),  nameof(sHmdaApplicationDate),  nameof(sHmdaLoanPurposeT),  nameof(sHmdaHoepaStatusT),
            nameof(sHmdaOtherNonAmortFeatureT),  nameof(sHmdaManufacturedTypeT),  nameof(sHmdaManufacturedInterestT),
            nameof(sHmdaReverseMortgageT), nameof(sHmdaBusinessPurposeT), nameof(sUniversalLoanIdentifier) )]
        [DevNote("Determines whether this loan is data-complete for 2018 HMDA reporting.")]
        public bool sHmdaIs2018DataComplete
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.sLegalEntityIdentifier) &&
                !string.IsNullOrWhiteSpace(this.sUniversalLoanIdentifier) &&
                this.sHmdaApplicationDate != CDateTime.InvalidWrapValue &&
                this.sHmdaLoanPurposeT != sHmdaLoanPurposeT.LeaveBlank &&
                this.sHmdaHoepaStatusT != sHmdaHoepaStatusT.LeaveBlank &&
                this.sHmdaOtherNonAmortFeatureT != sHmdaOtherNonAmortFeatureT.LeaveBlank &&
                this.sHmdaManufacturedTypeT != sHmdaManufacturedTypeT.LeaveBlank &&
                this.sHmdaManufacturedInterestT != sHmdaManufacturedInterestT.LeaveBlank &&
                this.sHmdaReverseMortgageT != sHmdaReverseMortgageT.LeaveBlank &&
                this.sHmdaBusinessPurposeT != sHmdaBusinessPurposeT.LeaveBlank;
            }
        }

        [DependsOn(nameof(sHmdaApplicationDate), nameof(sHmdaApplicationDateLckd), nameof(sAppSubmittedD))]
        [DevNote("The application date for the loan as defined by HMDA guidelines.")]
        private CDateTime sHmdaApplicationDate
        {
            get
            {
                if (this.sHmdaApplicationDateLckd)
                {
                    return this.GetDateTimeField(nameof(sHmdaApplicationDate));
                }

                var submittedDate = this.sAppSubmittedD;
                if (submittedDate.IsValid)
                {
                    return submittedDate;
                }

                return CDateTime.Create(LosConvert.HmdaNotApplicableDateTime);
            }
            set { this.SetDateTimeField(nameof(sHmdaApplicationDate), value); }
        }

        public string sHmdaApplicationDate_rep
        {
            get { return this.ToDateStringWithHmdaNa(this.sHmdaApplicationDate); }
            set { this.sHmdaApplicationDate = ToDateWithHmdaNa(value); }
        }

        [DependsOn]
        [DevNote("Determines whether sHmdaApplicationDate is calculated or from user input.")]
        public bool sHmdaApplicationDateLckd
        {
            get { return this.GetBoolField(nameof(sHmdaApplicationDateLckd)); }
            set { this.SetBoolField(nameof(sHmdaApplicationDateLckd), value); }
        }

        [DependsOn(nameof(sLT))]
        [DevNote("The type of the loan as defined by HMDA guidelines.")]
        public sHmdaLoanTypeT sHmdaLoanTypeT
        {
            get
            {
                switch (this.sLT)
                {
                    case E_sLT.Conventional:
                    case E_sLT.Other:
                        return sHmdaLoanTypeT.Conventional;
                    case E_sLT.FHA:
                        return sHmdaLoanTypeT.FHA;
                    case E_sLT.VA:
                        return sHmdaLoanTypeT.VA;
                    case E_sLT.UsdaRural:
                        return sHmdaLoanTypeT.USDA;
                    default:
                        throw new UnhandledEnumException(this.sLT);
                }
            }
        }

        [DependsOn()]
        [DevNote("Locked checkbox field for sHmdaLoanPurposeT")]
        public bool sHmdaLoanPurposeTLckd
        {
            get { return GetBoolField("sHmdaLoanPurposeTLckd", false); }
            set { SetNullableBoolField("sHmdaLoanPurposeTLckd", value); }
        }

        [DependsOn(nameof(sHmdaLoanPurposeT),  nameof(sHmdaReportAsHomeImprov), nameof(sLPurposeT), nameof(sHmdaLoanPurposeTLckd))]
        [DevNote("The purpose of the loan as defined by HMDA guidelines.")]
        public sHmdaLoanPurposeT sHmdaLoanPurposeT
        {
            get
            {
                if (sHmdaLoanPurposeTLckd)
                {
                    return GetTypeIndexField<sHmdaLoanPurposeT>(nameof(sHmdaLoanPurposeT), 0);
                }

                if (this.sHmdaReportAsHomeImprov)
                {
                    return sHmdaLoanPurposeT.HomeImprovement;
                }

                switch (this.sLPurposeT)
                {
                    case E_sLPurposeT.Purchase:
                    case E_sLPurposeT.Construct:
                    case E_sLPurposeT.ConstructPerm:
                        return sHmdaLoanPurposeT.HomePurchase;
                    case E_sLPurposeT.Refin:
                    case E_sLPurposeT.FhaStreamlinedRefinance:
                    case E_sLPurposeT.VaIrrrl:
                        return sHmdaLoanPurposeT.Refinancing;
                    case E_sLPurposeT.RefinCashout:
                        return sHmdaLoanPurposeT.CashOutRefinancing;
                    case E_sLPurposeT.Other:
                    case E_sLPurposeT.HomeEquity:
                        return sHmdaLoanPurposeT.OtherPurpose;
                    default:
                        throw new UnhandledEnumException(this.sLPurposeT);
                }
            }
            set
            {
                this.SetTypeIndexField("sHmdaLoanPurposeT", (int)value);
            }
        }

        [DependsOn(nameof(sGseSpT))]
        [DevNote("The construction method for the loan as defined by HMDA guidelines.")]
        public sHmdaConstructionMethT sHmdaConstructionMethT
        {
            get
            {
                if (this.sGseSpT == E_sGseSpT.ManufacturedHousing || this.sGseSpT == E_sGseSpT.ManufacturedHousingSingleWide || this.sGseSpT == E_sGseSpT.ManufacturedHomeMultiwide || this.sGseSpT == E_sGseSpT.ManufacturedHomeCondominium)
                {
                    return sHmdaConstructionMethT.ManufacturedHome;
                }

                return sHmdaConstructionMethT.SiteBuilt;
            }
        }

        [DependsOn(nameof(sSpAddr), nameof(sSpAddrTBD))]
        [DevNote("The subject property address as defined by HMDA guidelines.")]
        public string sHmdaSpAddr
        {
            get
            {
                if (sSpAddrTBD)
                {
                    return LosConvert.HmdaNotApplicableString;
                }
                else
                {
                    return this.m_convertLos.ToStringWithHmdaNa(this.sSpAddr);
                }
            }
        }

        [DependsOn(nameof(sSpCity), nameof(sSpAddrTBD))]
        [DevNote("The subject property city as defined by HMDA guidelines.")]
        public string sHmdaSpCity
        {
            get
            {
                if (sSpAddrTBD)
                {
                    return LosConvert.HmdaNotApplicableString;
                }
                else
                {
                    return this.m_convertLos.ToStringWithHmdaNa(this.sSpCity);
                }
            }
        }

        [DependsOn(nameof(sSpState), nameof(sSpAddrTBD))]
        [DevNote("The subject property state as defined by HMDA guidelines.")]
        public string sHmdaSpState
        {
            get
            {
                if (sSpAddrTBD)
                {
                    return LosConvert.HmdaNotApplicableString;
                }
                else
                {
                    return this.m_convertLos.ToStringWithHmdaNa(this.sSpState);
                }
            }
        }

        [DependsOn(nameof(sSpZip), nameof(sSpAddrTBD))]
        [DevNote("The subject property ZIP code as defined by HMDA guidelines.")]
        public string sHmdaSpZip
        {
            get
            {
                if (sSpAddrTBD)
                {
                    return LosConvert.HmdaNotApplicableString;
                }
                else
                {
                    return this.m_convertLos.ToStringWithHmdaNa(this.sSpZip);
                }
            }
        }
        
        [DependsOn(nameof(sHmdaStateCode), nameof(sHmdaCountyCode))]
        [DevNote("The subject property county code as defined by HMDA guidelines.")]
        public string sHmda2018CountyCode
        {
            get
            {
                return this.m_convertLos.ToStringWithHmdaNa(this.sHmdaStateCode + this.sHmdaCountyCode);
            }
        }

        [DependsOn(nameof(sHmdaStateCode), nameof(sHmdaCountyCode), nameof(sHmdaCensusTract))]
        [DevNote("The subject property census tract as defined by HMDA guidelines.")]
        public string sHmda2018CensusTract
        {
            get
            {
                return this.m_convertLos.ToStringWithHmdaNa(string.Join(string.Empty, (this.sHmdaStateCode + this.sHmdaCountyCode + this.sHmdaCensusTract).ToCharArray().Where(Char.IsDigit)));
            }
        }

        [DependsOn(nameof(sHmdaAprRateSpread))]
        [DevNote("The APR rate spread as defined by HMDA guidelines.")]
        public string sHmda2018AprRateSpread
        {
            get { return this.m_convertLos.ToStringWithHmdaNa(this.sHmdaAprRateSpread); }
        }

        [DependsOn(nameof(sHmdaCoApplicantSourceT), nameof(sHmdaCoApplicantSourceTLckd), nameof(CAppBase.aCIsValidHmdaApplication), nameof(GetCoApplicantSourceFromSecondaryApp))]
        [DevNote("The source of coapplicant data for HMDA reporting.")]
        public sHmdaCoApplicantSourceT sHmdaCoApplicantSourceT
        {
            get
            {
                if (this.sHmdaCoApplicantSourceTLckd)
                {
                    return GetTypeIndexField<sHmdaCoApplicantSourceT>(nameof(sHmdaCoApplicantSourceT));
                }

                if (this.GetAppData(0).aCIsValidHmdaApplication)
                {
                    return sHmdaCoApplicantSourceT.CoborrowerOnPrimaryApp;
                }

                if (string.IsNullOrEmpty(this.GetCoApplicantSourceFromSecondaryApp()))
                {
                    return sHmdaCoApplicantSourceT.NoCoApplicant;
                }

                return sHmdaCoApplicantSourceT.ApplicantOnSecondaryApp;
            }
            set { this.SetTypeIndexField(nameof(sHmdaCoApplicantSourceT), (int)value); }
        }

        [DependsOn]
        [DevNote("Determines whether sHmdaCoApplicantSourceT is calculated or from user input.")]
        public bool sHmdaCoApplicantSourceTLckd
        {
            get { return this.GetBoolField(nameof(sHmdaCoApplicantSourceTLckd)); }
            set { this.SetBoolField(nameof(sHmdaCoApplicantSourceTLckd), value); }
        }

        [DependsOn(nameof(sHmdaCoApplicantSourceSsn), nameof(sHmdaCoApplicantSourceSsnLckd), nameof(CAppBase.aBIsValidHmdaApplication), nameof(CAppBase.aCIsValidHmdaApplication), nameof(CAppBase.aBSsn), nameof(CAppBase.aCSsn), nameof(sfFindDataAppBySsn), nameof(sHmdaCoApplicantSourceSsnEncrypted), nameof(sLoanVersionT), nameof(GetCoApplicantSourceFromSecondaryApp), nameof(sHmdaCoApplicantSourceT), nameof(sHmdaCoApplicantSourceTLckd))]
        [DevNote("The SSN of the coapplicant used to populate data for HMDA reporting.")]
        public string sHmdaCoApplicantSourceSsn
        {
            get
            {
                if (this.sHmdaCoApplicantSourceSsnLckd)
                {
                    return this.GetEncryptedSsnField(nameof(sHmdaCoApplicantSourceSsn), nameof(sHmdaCoApplicantSourceSsnEncrypted));
                }

                var applicantSource = this.sHmdaCoApplicantSourceT;
                if (applicantSource == sHmdaCoApplicantSourceT.NoCoApplicant)
                {
                    return string.Empty;
                }

                if (applicantSource == sHmdaCoApplicantSourceT.ApplicantOnSecondaryApp)
                {
                    return this.GetCoApplicantSourceFromSecondaryApp();
                }

                var primaryApp = this.GetAppData(0);
                if (primaryApp.aCIsValidHmdaApplication)
                {
                    return primaryApp.aCSsn;
                }

                return string.Empty;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V27_RequireValidApplicantTinFormats) &&
                        !SocialSecurityNumber.Create(value).HasValue)
                    {
                        throw new InvalidTaxIdValueException(nameof(sHmdaCoApplicantSourceSsn));
                    }

                    var selectedApp = this.FindDataAppBySsn(value);
                    if (selectedApp == null || (!selectedApp.aBIsValidHmdaApplication && !selectedApp.aCIsValidHmdaApplication))
                    {
                        throw new ServerException(ErrorMessage.SystemError);
                    }
                }

                this.SetEncryptedSsnField(nameof(sHmdaCoApplicantSourceSsn), nameof(sHmdaCoApplicantSourceSsnEncrypted), value);
            }
        }

        [DependsOn(nameof(CAppBase.aBSsn), nameof(CAppBase.aCSsn), nameof(CAppBase.aBIsValidHmdaApplication), nameof(CAppBase.aCIsValidHmdaApplication))]
        private string GetCoApplicantSourceFromSecondaryApp()
        {
            for (int i = 1; i < this.nApps; i++)
            {
                var currentApp = this.GetAppData(i);

                if (currentApp.aBIsValidHmdaApplication)
                {
                    return currentApp.aBSsn;
                }

                if (currentApp.aCIsValidHmdaApplication)
                {
                    return currentApp.aCSsn;
                }
            }

            return string.Empty;
        }

        // Stub property for database column that will be picked up by the
        // dependency graph.
        [DependsOn]
        private string sHmdaCoApplicantSourceSsnEncrypted => null;

        [DependsOn]
        [DevNote("Determines whether sHmdaCoApplicantSourceSsn is calculated or from user input.")]
        public bool sHmdaCoApplicantSourceSsnLckd
        {
            get { return this.GetBoolField(nameof(sHmdaCoApplicantSourceSsnLckd)); }
            set { this.SetBoolField(nameof(sHmdaCoApplicantSourceSsnLckd), value); }
        }

        [DependsOn(nameof(CAppBase.aBIsValidHmdaApplication), nameof(CAppBase.aBDob), nameof(sHmdaApplicationDate), nameof(sHmdaActionTakenT))]
        [DevNote("The age of the primary borrower as defined by HMDA guidelines.")]
        private int sHmdaBAge
        {
            get
            {
                if (this.sHmdaActionTakenT == HmdaActionTaken.PurchasedLoan)
                {
                    return LosConvert.HmdaNotApplicableCode;
                }

                var primaryApp = this.GetAppData(0);
                
                if (!primaryApp.aBIsValidHmdaApplication)
                {
                    throw new CBaseException("HMDA Application is invalid", "HMDA Application is invalid");
                }

                if (sHmdaApplicationDate == CDateTime.Create(LosConvert.HmdaNotApplicableDateTime))
                {
                    return LosConvert.HmdaNotApplicableCode;
                }

                return Tools.CalcAgeForDate(primaryApp.aBDob.DateTimeForComputation, sHmdaApplicationDate.DateTimeForComputation);
            }
        }

        public string sHmdaBAge_rep
        {
            get { return this.ToCountStringWithHmdaNa(() => this.sHmdaBAge, preserveNotApplicableCode: true); }
        }

        [DependsOn(nameof(sHmdaCoApplicantSourceT), nameof(sHmdaCoApplicantSourceSsn), nameof(CAppBase.aBDob), nameof(CAppBase.aCDob), nameof(CAppBase.aBSsn),
            nameof(CAppBase.aCSsn), nameof(CAppBase.aBIsValidHmdaApplication), nameof(CAppBase.aCIsValidHmdaApplication), nameof(sfFindDataAppBySsn),
            nameof(sHmdaApplicationDate), nameof(sHmdaActionTakenT))]
        [DevNote("The age of the coapplicant as defined by HDMA guidelines.")]
        private int sHmdaCAge
        {
            get
            {
                if (this.sHmdaActionTakenT == HmdaActionTaken.PurchasedLoan)
                {
                    return LosConvert.HmdaNotApplicableCode;
                }

                switch (this.sHmdaCoApplicantSourceT)
                {
                    case sHmdaCoApplicantSourceT.NoCoApplicant:
                        return LosConvert.HmdaNoCoapplicantCode;
                    case sHmdaCoApplicantSourceT.CoborrowerOnPrimaryApp:
                        var primaryApp = this.GetAppData(0);

                        if (!primaryApp.aCIsValidHmdaApplication)
                        {
                            throw new CBaseException("HMDA Application is invalid", "HMDA Application is invalid");
                        }

                        if (sHmdaApplicationDate == CDateTime.Create(LosConvert.HmdaNotApplicableDateTime))
                        {
                            return LosConvert.HmdaNotApplicableCode;
                        }

                        return Tools.CalcAgeForDate(primaryApp.aCDob.DateTimeForComputation, sHmdaApplicationDate.DateTimeForComputation);
                    case sHmdaCoApplicantSourceT.ApplicantOnSecondaryApp:
                        var ssn = this.sHmdaCoApplicantSourceSsn;
                        var app = this.FindDataAppBySsn(ssn);

                        if (app == null)
                        {
                            throw new CBaseException("HMDA Application is invalid", "HMDA Application is invalid");
                        }

                        if (app.aBSsn == ssn)
                        {
                            if (!app.aBIsValidHmdaApplication)
                            {
                                throw new CBaseException("HMDA Application is invalid", "HMDA Application is invalid");
                            }

                            if (sHmdaApplicationDate == CDateTime.Create(LosConvert.HmdaNotApplicableDateTime))
                            {
                                return LosConvert.HmdaNotApplicableCode;
                            }

                            return Tools.CalcAgeForDate(app.aBDob.DateTimeForComputation, sHmdaApplicationDate.DateTimeForComputation);
                        }

                        if (app.aCSsn == ssn)
                        {
                            if (!app.aCIsValidHmdaApplication)
                            {
                                throw new CBaseException("HMDA Application is invalid", "HMDA Application is invalid");
                            }

                            if (sHmdaApplicationDate == CDateTime.Create(LosConvert.HmdaNotApplicableDateTime))
                            {
                                return LosConvert.HmdaNotApplicableCode;
                            }

                            return Tools.CalcAgeForDate(app.aCDob.DateTimeForComputation, sHmdaApplicationDate.DateTimeForComputation);
                        }

                        throw new CBaseException("Applicant on secondary app could not be found.", "Applicant on secondary app could not be found.");
                    default:
                        throw new UnhandledEnumException(this.sHmdaCoApplicantSourceT);
                }
            }
        }

        public string sHmdaCAge_rep
        {
            get { return this.ToCountStringWithHmdaNa(() => this.sHmdaCAge, preserveNotApplicableCode: true); }
        }

        [DependsOn(nameof(sReliedOnTotalIncome), nameof(sIsEmployeeLoan), nameof(sHmdaIsIncomeReliedOn), nameof(sHmdaPropT), nameof(sHmdaActionTakenT))]
        [DevNote("The total income for the loan as defined by HMDA guidelines.")]
        private decimal? sHmdaIncome
        {
            get
            {
                if (this.sHmdaActionTakenT == HmdaActionTaken.PurchasedLoan)
                {
                    return null;
                }

                if (sHmdaPropT == E_sHmdaPropT.MultiFamiliy)
                {
                    return null;
                }

                if (this.sIsEmployeeLoan || !this.sHmdaIsIncomeReliedOn)
                {
                    return null;
                }

                var totalIncome = this.sReliedOnTotalIncome;
                if (totalIncome != 0)
                {
                    return Math.Round(totalIncome / 1000, MidpointRounding.AwayFromZero);
                }

                return null;
            }
        }

        public string sHmdaIncome_rep
        {
            get
            {
                return this.ToCountStringWithHmdaNa(() => this.sHmdaIncome);
            }
        }

        [DependsOn]
        [DevNote("The HOEPA status of the loan as defined by HMDA guidelines.")]
        public sHmdaHoepaStatusT sHmdaHoepaStatusT
        {
            get { return this.GetTypeIndexField<sHmdaHoepaStatusT>(nameof(sHmdaHoepaStatusT)); }
            set { this.SetTypeIndexField(nameof(sHmdaHoepaStatusT), (int)value); }
        }

        [DependsOn(nameof(CAppBase.aBIsValidHmdaApplication), nameof(CAppBase.aBDecisionCreditScore), nameof(sReliedOnCreditScoreModeT), nameof(sReliedOnCreditScoreSingleHmdaReportAsT),
            nameof(sReliedOnCreditScore), nameof(sHmdaIsCreditScoreReliedOn), nameof(IsCreditScoreNotApplicableForActionTaken))]
        [DevNote("The credit score for the primary borrower as defined by HMDA guidelines.")]
        private int sHmdaBCreditScore
        {
            get
            {
                if (!this.sHmdaIsCreditScoreReliedOn || this.IsCreditScoreNotApplicableForActionTaken())
                {
                    return LosConvert.HmdaNotApplicableCode;
                }

                if (this.sReliedOnCreditScoreModeT == ReliedOnCreditScoreMode.Blank)
                {
                    return int.MinValue; // this is the sentinel value representing blank
                }
                else if (this.sReliedOnCreditScoreModeT == ReliedOnCreditScoreMode.ApplicantIndividualScore)
                {
                    // existing behavior before HMDA relied On
                    var primaryApp = this.GetAppData(0);
                    if (primaryApp.aBIsValidHmdaApplication)
                    {
                        return primaryApp.aBDecisionCreditScore;
                    }

                    return LosConvert.HmdaNotApplicableCode;
                }
                else // this.sReliedOnCreditScoreModeT == ReliedOnCreditScoreMode.SingleScore
                {
                    if (this.sReliedOnCreditScoreSingleHmdaReportAsT == sHmdaReportCreditScoreAsT.Borrower)
                    {
                        return this.sReliedOnCreditScore;
                    }
                    else if (this.sReliedOnCreditScoreSingleHmdaReportAsT == sHmdaReportCreditScoreAsT.Coborrower)
                    {
                        return LosConvert.HmdaNotApplicableCode;
                    }
                    else // Hmda Report as type has not been selected
                    {
                        return int.MinValue; // this is the sentinel value representing blank
                    }
                }
            }
        }

        public string sHmdaBCreditScore_rep
        {
            get { return this.ToCountStringWithHmdaNaAndBlank(() => this.sHmdaBCreditScore, preserveNotApplicableCode: true); }
        }

        [DependsOn(nameof(sHmdaCoApplicantSourceT), nameof(sHmdaCoApplicantSourceSsn), nameof(CAppBase.aBSsn), nameof(CAppBase.aCSsn), nameof(CAppBase.aCDecisionCreditScore), nameof(CAppBase.aBDecisionCreditScore), nameof(CAppBase.aBIsValidHmdaApplication), nameof(CAppBase.aCIsValidHmdaApplication), nameof(sfFindDataAppBySsn), 
            nameof(sHmdaIsCreditScoreReliedOn), nameof(sReliedOnCreditScoreModeT), nameof(sReliedOnCreditScoreSingleHmdaReportAsT), nameof(sReliedOnCreditScore), nameof(IsCreditScoreNotApplicableForActionTaken))]
        [DevNote("The credit score for the coapplicant as defined by HMDA guidelines.")]
        private int sHmdaCCreditScore
        {
            get
            {
                if (!this.sHmdaIsCreditScoreReliedOn || this.IsCreditScoreNotApplicableForActionTaken())
                {
                    return LosConvert.HmdaNotApplicableCode;
                }

                if (this.sHmdaCoApplicantSourceT == sHmdaCoApplicantSourceT.NoCoApplicant)
                {
                    return LosConvert.HmdaNoCoapplicantCode;
                }
                else
                {
                    if (this.sReliedOnCreditScoreModeT == ReliedOnCreditScoreMode.Blank)
                    {
                        return int.MinValue; // sentinel value representing blank
                    }
                    else if (this.sReliedOnCreditScoreModeT == ReliedOnCreditScoreMode.ApplicantIndividualScore)
                    {
                        // existing behavior before HMDA relied on, includes fix for opm 464013
                        if (this.sHmdaCoApplicantSourceT == sHmdaCoApplicantSourceT.CoborrowerOnPrimaryApp)
                        {
                            var primaryApp = this.GetAppData(0);
                            if (primaryApp.aCIsValidHmdaApplication)
                            {
                                return primaryApp.aCDecisionCreditScore;
                            }

                            return LosConvert.HmdaNotApplicableCode;
                        }
                        else if (this.sHmdaCoApplicantSourceT == sHmdaCoApplicantSourceT.ApplicantOnSecondaryApp)
                        {
                            var ssn = this.sHmdaCoApplicantSourceSsn;
                            var app = this.FindDataAppBySsn(ssn);

                            if (app == null)
                            {
                                return LosConvert.HmdaNotApplicableCode;
                            }

                            if (app.aBSsn == ssn)
                            {
                                if (app.aBIsValidHmdaApplication)
                                {
                                    return app.aBDecisionCreditScore;
                                }

                                return LosConvert.HmdaNotApplicableCode;
                            }

                            if (app.aCSsn == ssn)
                            {
                                if (app.aCIsValidHmdaApplication)
                                {
                                    return app.aCDecisionCreditScore;
                                }

                                return LosConvert.HmdaNotApplicableCode;
                            }

                            return LosConvert.HmdaNotApplicableCode;
                        }
                        else
                        {
                            throw new UnhandledEnumException(this.sHmdaCoApplicantSourceT);
                        }
                    }
                    else // this.sReliedOnCreditScoreModeT == ReliedOnCreditScoreMode.SingleScore
                    {
                        if (this.sReliedOnCreditScoreSingleHmdaReportAsT == sHmdaReportCreditScoreAsT.Borrower)
                        {
                            return LosConvert.HmdaNotApplicableCode;
                        }
                        else if (this.sReliedOnCreditScoreSingleHmdaReportAsT == sHmdaReportCreditScoreAsT.Coborrower)
                        {
                            return this.sReliedOnCreditScore;
                        }
                        else // Hmda Report as type has not been selected
                        {
                            return int.MinValue; // this is the sentinel value representing blank
                        }
                    }
                }
            }            
        }

        public string sHmdaCCreditScore_rep
        {
            get { return this.ToCountStringWithHmdaNaAndBlank(() => this.sHmdaCCreditScore, preserveNotApplicableCode: true); }
        }

        [DependsOn(nameof(sDisclosureRegulationT), nameof(sTRIDClosingDisclosureTotalLoanCosts), nameof(sQMTotFeeAmount),
            nameof(sLoanVersionT), nameof(sHmdaActionTakenT), nameof(sHmdaActionTaken), nameof(sIsInTRID2015Mode), nameof(sIsLineOfCredit),
            nameof(sHmdaReverseMortgageT), nameof(sHmdaBusinessPurposeT))]
        [DevNote("The total loan costs as defined by HMDA guidelines.")]
        private decimal? sHmdaTotalLoanCosts
        {
            get
            {
                if (this.sIsLineOfCredit || 
                    this.sHmdaReverseMortgageT == sHmdaReverseMortgageT.ReverseMortgage ||
                    this.sHmdaBusinessPurposeT == sHmdaBusinessPurposeT.PrimarilyForBusinessOrCommercial)
                {
                    return null;
                }

                bool isHmdaDataPointImprovement = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement);

                if (isHmdaDataPointImprovement)
                {
                    if (this.sHmdaActionTakenT != HmdaActionTaken.LoanOriginated && this.sHmdaActionTakenT != HmdaActionTaken.PurchasedLoan)
                    {
                        return null;
                    }
                    if (this.sHmdaActionTakenT == HmdaActionTaken.PurchasedLoan && !this.sIsInTRID2015Mode)
                    {
                        return null;
                    }
                }
                else
                {
                    if (this.sHmdaActionTaken != "Loan originated" && this.sHmdaActionTaken != ConstApp.HmdaPurchasedActionTaken)
                    {
                        return null;
                    }
                    if (this.sHmdaActionTaken == ConstApp.HmdaPurchasedActionTaken && !this.sIsInTRID2015Mode)                       
                    {
                        return null;
                    }
                }                

                if (this.sIsInTRID2015Mode)
                {
                    return sTRIDClosingDisclosureTotalLoanCosts;
                }

                return sQMTotFeeAmount;
            }
        }

        public string sHmdaTotalLoanCosts_rep
        {
            get { return this.ToMoneyStringWithHmdaNa(() => this.sHmdaTotalLoanCosts); }
        }

        [DependsOn(nameof(sDisclosureRegulationT), nameof(sClosingCostSet), nameof(sLoanVersionT), nameof(sHmdaActionTaken),
            nameof(sHmdaActionTakenT), nameof(sIsLineOfCredit), nameof(sHmdaReverseMortgageT), nameof(sHmdaBusinessPurposeT))]
        [DevNote("The origination charge for the loan as defined by HMDA guidelines.")]
        private decimal? sHmdaOriginationCharge
        {
            get
            {
                if (this.sIsLineOfCredit ||
                    this.sHmdaReverseMortgageT == sHmdaReverseMortgageT.ReverseMortgage ||
                    this.sHmdaBusinessPurposeT == sHmdaBusinessPurposeT.PrimarilyForBusinessOrCommercial)
                {
                    return null;
                }

                bool isHmdaDataPointImprovement = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement);

                if (isHmdaDataPointImprovement)
                {
                    if (this.sHmdaActionTakenT != HmdaActionTaken.LoanOriginated && this.sHmdaActionTakenT != HmdaActionTaken.PurchasedLoan)
                    {
                        return null;
                    }
                }
                else
                {
                    if (this.sHmdaActionTaken != "Loan originated" && this.sHmdaActionTaken != ConstApp.HmdaPurchasedActionTaken)
                    {
                        return null;
                    }
                }                

                if (this.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
                {
                    Func<LoanClosingCostFeePayment, bool> paymentFilter = payment => payment.PaidByT.EqualsOneOf(
                        E_ClosingCostFeePaymentPaidByT.Borrower,
                        E_ClosingCostFeePaymentPaidByT.BorrowerFinance);

                    return this.sClosingCostSet.Sum(ClosingCostSetUtils.SectionAFilter, paymentFilter);
                }

                return null;
            }
        }

        public string sHmdaOriginationCharge_rep
        {
            get { return this.ToMoneyStringWithHmdaNa(() => this.sHmdaOriginationCharge); }
        }

        [DependsOn(nameof(sDisclosureRegulationT), nameof(sGfeDiscountPointF), nameof(sLoanVersionT),
            nameof(sHmdaActionTakenT), nameof(sHmdaActionTaken), nameof(sIsLineOfCredit), nameof(sHmdaReverseMortgageT), nameof(sHmdaBusinessPurposeT))]
        [DevNote("The discount points for the loan as defined by HMDA guidelines.")]
        private decimal? sHmdaDiscountPoints
        {
            get
            {
                if (this.sIsLineOfCredit ||
                    this.sHmdaReverseMortgageT == sHmdaReverseMortgageT.ReverseMortgage ||
                    this.sHmdaBusinessPurposeT == sHmdaBusinessPurposeT.PrimarilyForBusinessOrCommercial)
                {
                    return null;
                }

                bool isHmdaDataPointImprovement = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement);

                if (isHmdaDataPointImprovement)
                {
                    if (this.sHmdaActionTakenT != HmdaActionTaken.LoanOriginated && this.sHmdaActionTakenT != HmdaActionTaken.PurchasedLoan)
                    {
                        return null;
                    }
                }
                else
                {
                    if (this.sHmdaActionTaken != "Loan originated" && this.sHmdaActionTaken != ConstApp.HmdaPurchasedActionTaken)
                    {
                        return null;
                    }
                }

                if (this.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
                {
                    return sGfeDiscountPointF;
                }

                return null;
            }
        }

        public string sHmdaDiscountPoints_rep
        {
            get { return this.ToMoneyStringWithHmdaNaAndBlank(() => this.sHmdaDiscountPoints); }
        }

        [DependsOn(nameof(sDisclosureRegulationT), nameof(sTRIDClosingDisclosureGeneralLenderCredits), 
            nameof(sLoanVersionT), nameof(sHmdaActionTaken), nameof(sHmdaActionTakenT), nameof(sIsLineOfCredit),
            nameof(sHmdaReverseMortgageT), nameof(sHmdaBusinessPurposeT))]
        [DevNote("The lender credits for the loan as defined by HMDA guidelines.")]
        private decimal? sHmdaLenderCredits
        {
            get
            {
                if (this.sIsLineOfCredit ||
                    this.sHmdaReverseMortgageT == sHmdaReverseMortgageT.ReverseMortgage ||
                    this.sHmdaBusinessPurposeT == sHmdaBusinessPurposeT.PrimarilyForBusinessOrCommercial)
                {
                    return null;
                }

                bool isHmdaDataPointImprovement = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement);

                if (isHmdaDataPointImprovement)
                {
                    if (this.sHmdaActionTakenT != HmdaActionTaken.LoanOriginated && this.sHmdaActionTakenT != HmdaActionTaken.PurchasedLoan)
                    {
                        return null;
                    }
                }
                else
                {
                    if (this.sHmdaActionTaken != "Loan originated" && this.sHmdaActionTaken != ConstApp.HmdaPurchasedActionTaken)
                    {
                        return null;
                    }

                }            

                if (this.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
                {
                    return this.sTRIDClosingDisclosureGeneralLenderCredits;
                }

                return null;
            }
        }

        public string sHmdaLenderCredits_rep
        {
            get { return this.ToMoneyStringWithHmdaNaAndBlank(() => this.sHmdaLenderCredits); }
        }

        private static readonly IEnumerable<string> DeniedWithdrawnIncompleteActionsTaken = new[]
        {
            ConstApp.HmdaApplicationDenied,
            ConstApp.HmdaApplicationWithdrawnByApplicant,
            ConstApp.HmdaFileClosedForIncompleteness,
            ConstApp.HmdaPreapprovalDenied
        };

        private static readonly HashSet<HmdaActionTaken> DeniedWithdrawnIncompleteActionsTakenT = new HashSet<HmdaActionTaken>()
        {
            HmdaActionTaken.ApplicationDenied,
            HmdaActionTaken.ApplicationWithdrawnByApplicant,
            HmdaActionTaken.FileClosedForIncompleteness,
            HmdaActionTaken.PreapprovalRequestDenied
        };

        [DependsOn(nameof(sHmdaActionTaken), nameof(sNoteIR), nameof(sHmdaActionTakenT), nameof(sLoanVersionT))]
        [DevNote("The interest rate for the loan as defined by HMDA guidelines.")]
        private decimal? sHmdaInterestRate
        {
            get
            {
                var hmdaActionTaken = this.sHmdaActionTaken;
                bool isAtLeastV22 = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement);

                if(isAtLeastV22 && DeniedWithdrawnIncompleteActionsTakenT.Contains(this.sHmdaActionTakenT))
                {
                    return null;
                }

                if (!isAtLeastV22 && DeniedWithdrawnIncompleteActionsTaken.Any(status => string.Equals(status, hmdaActionTaken, StringComparison.OrdinalIgnoreCase)))
                {
                    return null;
                }

                if (this.sNoteIR <= 0M)
                {
                    return null;
                }

                return this.sNoteIR;
            }
        }

        public string sHmdaInterestRate_rep
        {
            get { return this.ToRateStringWithHmdaNa(() => this.sHmdaInterestRate); }
        }

        [DependsOn(nameof(sHmdaReverseMortgageT), nameof(sHmdaBusinessPurposeT), nameof(sHmdaActionTaken), nameof(sHardPlusSoftPrepmtPeriodMonths), nameof(sPrepmtPeriodMonths),
            nameof(sSoftPrepmtPeriodMonths), nameof(sHmdaActionTakenT), nameof(sLoanVersionT))]
        [DevNote("The prepayment term for the loan as defined by HMDA guidelines.")]
        private int? sHmdaPrepaymentTerm
        {
            get
            {
                bool isAtLeastV22 = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement);

                var isNotApplicable =
                    this.sHmdaReverseMortgageT == sHmdaReverseMortgageT.ReverseMortgage ||
                    this.sHmdaBusinessPurposeT == sHmdaBusinessPurposeT.PrimarilyForBusinessOrCommercial ||
                    (isAtLeastV22 && this.sHmdaActionTakenT == HmdaActionTaken.PurchasedLoan) ||
                    (!isAtLeastV22 && string.Equals(this.sHmdaActionTaken, ConstApp.HmdaPurchasedActionTaken, StringComparison.OrdinalIgnoreCase)) ||
                    this.sHardPlusSoftPrepmtPeriodMonths == 0;

                if (isNotApplicable)
                {
                    return null;
                }

                return Math.Max(this.sPrepmtPeriodMonths, this.sSoftPrepmtPeriodMonths);
            }
        }

        public string sHmdaPrepaymentTerm_rep
        {
            get { return this.ToCountStringWithHmdaNa(() => this.sHmdaPrepaymentTerm); }
        }

        [DependsOn(nameof(IsNotApplicableForHmdaActionTaken), nameof(sHmdaActionTakenT), nameof(sLoanVersionT), nameof(sCreditDecisionD),
            nameof(sHmdaIsDebtRatioReliedOn), nameof(sReliedOnDebtRatio), nameof(sHmdaMultifamilyUnits))]
        [DevNote("The debt ratio for the loan as defined by HMDA guidelines.")]
        private decimal? sHmdaDebtRatio
        {
            get
            {
                if (!this.sCreditDecisionD.IsValid || !this.sHmdaIsDebtRatioReliedOn || this.sHmdaMultifamilyUnits.HasValue)
                {
                    return null;
                }

                bool isAtLeastV22 = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement);

                if (isAtLeastV22 && this.IsNotApplicableForHmdaActionTaken(
                    HmdaActionTaken.PurchasedLoan,
                    HmdaActionTaken.ApplicationWithdrawnByApplicant,
                    HmdaActionTaken.FileClosedForIncompleteness))
                {
                    return null;
                }

                if (!isAtLeastV22 && this.IsNotApplicableForHmdaActionTaken(
                    ConstApp.HmdaPurchasedActionTaken,
                    ConstApp.HmdaApplicationWithdrawnByApplicant,
                    ConstApp.HmdaFileClosedForIncompleteness))
                {
                    return null;
                }

                return sReliedOnDebtRatio;
            }
        }

        public string sHmdaDebtRatio_rep
        {
            get { return this.ToRateStringWithHmdaNa(() => this.sHmdaDebtRatio); }
        }

        [DependsOn(nameof(IsNotApplicableForHmdaActionTaken), nameof(sHmdaIsCombinedRatioReliedOn), nameof(sHmdaActionTakenT), nameof(sLoanVersionT), nameof(sCreditDecisionD), nameof(sReliedOnCombinedLTVRatio))]
        [DevNote("The combined debt ratio for the loan as defined by HMDA guidelines.")]
        private decimal? sHmdaCombinedRatio
        {
            get
            {
                if (!this.sCreditDecisionD.IsValid || !this.sHmdaIsCombinedRatioReliedOn)
                {
                    return null;
                }

                bool isAtLeastV22 = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement);

                if (isAtLeastV22 && this.IsNotApplicableForHmdaActionTaken(
                    HmdaActionTaken.PurchasedLoan, 
                    HmdaActionTaken.FileClosedForIncompleteness, 
                    HmdaActionTaken.ApplicationWithdrawnByApplicant))
                {
                    return null;
                }

                if (!isAtLeastV22 && this.IsNotApplicableForHmdaActionTaken(
                    ConstApp.HmdaPurchasedActionTaken, 
                    ConstApp.HmdaFileClosedForIncompleteness,
                    ConstApp.HmdaApplicationWithdrawnByApplicant))
                {
                    return null;
                }

                return this.sReliedOnCombinedLTVRatio;
            }
        }

        public string sHmdaCombinedRatio_rep
        {
            get
            {
                return this.ToRateStringWithHmdaNa(() => this.sHmdaCombinedRatio.HasValue ? (decimal?)(Tools.LtvRounding(this.sHmdaCombinedRatio.Value)) : this.sHmdaCombinedRatio);
            }
        }

        [DependsOn(nameof(sDue), nameof(sHmdaReverseMortgageT), nameof(sLPurposeT), nameof(sConstructionPeriodMon))]
        [DevNote("The scheduled number of months after which the legal obligation will mature or terminate or would have matured or terminated. " +
            "In LQB, this is represented by sDue, not sTerm, as the latter represents the amortization term and the former represents the maturity term.")]
        private int? sHmdaTerm
        {
            get
            {
                if (this.sHmdaReverseMortgageT == sHmdaReverseMortgageT.ReverseMortgage)
                {
                    return null;
                }

                if (this.sLPurposeT == E_sLPurposeT.ConstructPerm)
                {
                    return this.sDue + this.sConstructionPeriodMon;
                }

                if (this.sLPurposeT == E_sLPurposeT.Construct)
                {
                    return this.sConstructionPeriodMon;
                }

                return this.sDue;
            }
        }

        public string sHmdaTerm_rep
        {
            get { return this.ToCountStringWithHmdaNa(() => this.sHmdaTerm); }
        }

        [DependsOn(nameof(sFinMethT), nameof(sRAdj1stCapMon), nameof(sGradPmtYrs), nameof(sBuydwnMon1))]
        [DevNote("The introductory period for the loan as defined by HMDA guidelines.")]
        private int? sHmdaIntroductoryPeriod
        {
            get
            {
                if (this.sFinMethT == E_sFinMethT.Fixed)
                {
                    return null;
                }

                var min = this.NonZeroMin(this.sRAdj1stCapMon, this.sGradPmtYrs * 12, this.sBuydwnMon1);
                if (min != 0)
                {
                    return min;
                }

                return null;
            }
        }

        public string sHmdaIntroductoryPeriod_rep
        {
            get { return this.ToCountStringWithHmdaNa(() => this.sHmdaIntroductoryPeriod); }
        }

        [DependsOn]
        [DevNote("Determines whether the loan has a non-amortization feature as defined by HMDA guidelines.")]
        public sHmdaOtherNonAmortFeatureT sHmdaOtherNonAmortFeatureT
        {
            get { return this.GetTypeIndexField<sHmdaOtherNonAmortFeatureT>(nameof(sHmdaOtherNonAmortFeatureT)); }
            set { this.SetTypeIndexField(nameof(sHmdaOtherNonAmortFeatureT), (int)value); }
        }

        [DependsOn(nameof(sHmdaActionTaken), nameof(sReliedOnPropertyValue), nameof(sHmdaActionTakenT), nameof(sLoanVersionT), nameof(sCreditDecisionD), nameof(sHmdaIsPropertyValueReliedOn))]
        [DevNote("The property value for the loan as defined by HMDA guidelines.")]
        private decimal? sHmdaPropertyValue
        {
            get
            {
                if (!this.sCreditDecisionD.IsValid || !this.sHmdaIsPropertyValueReliedOn)
                {
                    return null;
                }

                bool isAtLeastV22 = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement);

                if (isAtLeastV22 && 
                    (this.sHmdaActionTakenT == HmdaActionTaken.FileClosedForIncompleteness ||
                    this.sHmdaActionTakenT == HmdaActionTaken.ApplicationWithdrawnByApplicant))
                {
                    return null;
                }

                if (!isAtLeastV22 && 
                    (string.Equals(this.sHmdaActionTaken, ConstApp.HmdaFileClosedForIncompleteness, StringComparison.OrdinalIgnoreCase) ||
                    string.Equals(this.sHmdaActionTaken, ConstApp.HmdaApplicationWithdrawnByApplicant, StringComparison.OrdinalIgnoreCase)))
                {
                    return null;
                }

                return this.sReliedOnPropertyValue;
            }
        }

        public string sHmdaPropertyValue_rep
        {
            get { return this.ToMoneyStringWithHmdaNa(() => this.sHmdaPropertyValue); }
        }

        [DependsOn]
        [DevNote("The manufactured home and land type for the loan as defined by HMDA guidelines.")]
        public sHmdaManufacturedTypeT sHmdaManufacturedTypeT
        {
            get { return this.GetTypeIndexField<sHmdaManufacturedTypeT>(nameof(sHmdaManufacturedTypeT)); }
            set { this.SetTypeIndexField(nameof(sHmdaManufacturedTypeT), (int)value); }
        }

        [DependsOn]
        [DevNote("The manufactured interest for the loan as defined by HMDA guidelines.")]
        public sHmdaManufacturedInterestT sHmdaManufacturedInterestT
        {
            get { return this.GetTypeIndexField<sHmdaManufacturedInterestT>(nameof(sHmdaManufacturedInterestT)); }
            set { this.SetTypeIndexField(nameof(sHmdaManufacturedInterestT), (int)value); }
        }

        [DependsOn(nameof(sHmdaMultifamilyUnits), nameof(sHmdaPropT))]
        [DevNote("The number of multi-family units for the loan as defined by HMDA guidelines.")]
        private int? sHmdaMultifamilyUnits
        {
            get
            {
                if (this.sHmdaPropT != E_sHmdaPropT.MultiFamiliy)
                {
                    return null;
                }

                var dbValue = this.GetCountField(nameof(sHmdaMultifamilyUnits));
                return dbValue < 0 ? (int?)null : dbValue;
            }
            set { this.SetCountField(nameof(sHmdaMultifamilyUnits), value ?? ConstApp.HmdaMultiFamilyUnitsNotApplicableValue); }
        }

        public string sHmdaMultifamilyUnits_rep
        {
            get { return this.ToCountStringWithHmdaNa(() => this.sHmdaMultifamilyUnits); }
            set { this.sHmdaMultifamilyUnits = this.ToCountWithHmdaNa(value); }
        }

        [DependsOn(nameof(sHmdaActionTaken), nameof(sBranchChannelT), nameof(sHmdaActionTakenT), nameof(sLoanVersionT))]
        [DevNote("The application submission type for the loan as defined by HMDA guidelines.")]
        public sHmdaSubmissionApplicationT sHmdaSubmissionApplicationT
        {
            get
            {
                bool isAtLeastV22 = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement);

                if (isAtLeastV22 && this.sHmdaActionTakenT == HmdaActionTaken.PurchasedLoan)
                {
                    return sHmdaSubmissionApplicationT.NA;
                }

                if (!isAtLeastV22 && string.Equals(this.sHmdaActionTaken, ConstApp.HmdaPurchasedActionTaken, StringComparison.OrdinalIgnoreCase))
                {
                    return sHmdaSubmissionApplicationT.NA;
                }

                switch (this.sBranchChannelT)
                {
                    case E_BranchChannelT.Retail:
                    case E_BranchChannelT.Broker:
                        return sHmdaSubmissionApplicationT.SubmittedDirectlyToInstitution;
                    case E_BranchChannelT.Wholesale:
                        return sHmdaSubmissionApplicationT.NotSubmittedDirectlyToInstitution;
                    case E_BranchChannelT.Correspondent:
                    case E_BranchChannelT.Blank:
                        return sHmdaSubmissionApplicationT.LeaveBlank; 
                    default:
                        throw new UnhandledEnumException(this.sBranchChannelT);
                }
            }
        }

        [DependsOn(nameof(sHmdaActionTaken), nameof(sBranchChannelT), nameof(sHmdaActionTakenT), nameof(sLoanVersionT))]
        [DevNote("Whether the loan is initially payable to the institution as defined by HMDA guidelines.")]
        public sHmdaInitiallyPayableToInstitutionT sHmdaInitiallyPayableToInstitutionT
        {
            get
            {
                bool isAtLeastV22 = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement);

                if (isAtLeastV22 && 
                    (this.sHmdaActionTakenT == HmdaActionTaken.PurchasedLoan ||
                    this.sHmdaActionTakenT == HmdaActionTaken.ApplicationWithdrawnByApplicant ||
                    this.sHmdaActionTakenT == HmdaActionTaken.ApplicationDenied || 
                    this.sHmdaActionTakenT == HmdaActionTaken.FileClosedForIncompleteness))
                {
                    return sHmdaInitiallyPayableToInstitutionT.NA;
                }

                if (!isAtLeastV22 && 
                    (string.Equals(this.sHmdaActionTaken, ConstApp.HmdaPurchasedActionTaken, StringComparison.OrdinalIgnoreCase) ||
                    string.Equals(this.sHmdaActionTaken, ConstApp.HmdaApplicationWithdrawnByApplicant, StringComparison.OrdinalIgnoreCase) ||
                    string.Equals(this.sHmdaActionTaken, ConstApp.HmdaApplicationDenied, StringComparison.OrdinalIgnoreCase) ||
                    string.Equals(this.sHmdaActionTaken, ConstApp.HmdaFileClosedForIncompleteness, StringComparison.OrdinalIgnoreCase)))
                {
                    return sHmdaInitiallyPayableToInstitutionT.NA;
                }

                switch (this.sBranchChannelT)
                {
                    case E_BranchChannelT.Retail:
                    case E_BranchChannelT.Wholesale:
                        return sHmdaInitiallyPayableToInstitutionT.InitiallyPayableToInstitution;
                    case E_BranchChannelT.Broker:
                        return sHmdaInitiallyPayableToInstitutionT.NotInitiallyPayableToInstitution;
                    case E_BranchChannelT.Correspondent:
                    case E_BranchChannelT.Blank:
                        return sHmdaInitiallyPayableToInstitutionT.LeaveBlank;
                    default:
                        throw new UnhandledEnumException(this.sBranchChannelT);
                }
            }
        }
        
        [DependsOn(nameof(sApp1003InterviewerLoanOriginatorIdentifier))]
        [DevNote("The loan originator entered on page 3 of the 1003 for the loan as defined by HMDA guidelines.")]
        public string sHmdaApp1003InterviewerLoanOriginatorIdentifier
        {
            get { return this.m_convertLos.ToStringWithHmdaNa(this.sApp1003InterviewerLoanOriginatorIdentifier); }
        }

        [DependsOn]
        [DevNote("Whether the loan is a reverse mortgage as defined by HMDA guidelines.")]
        public sHmdaReverseMortgageT sHmdaReverseMortgageT
        {
            get { return this.GetTypeIndexField<sHmdaReverseMortgageT>(nameof(sHmdaReverseMortgageT)); }
            set { this.SetTypeIndexField(nameof(sHmdaReverseMortgageT), (int)value); }
        }
        
        [DependsOn]
        [DevNote("The business purpose for the loan as defined by HMDA guidelines.")]
        public sHmdaBusinessPurposeT sHmdaBusinessPurposeT
        {
            get { return this.GetTypeIndexField<sHmdaBusinessPurposeT>(nameof(sHmdaBusinessPurposeT)); }
            set { this.SetTypeIndexField(nameof(sHmdaBusinessPurposeT), (int)value); }
        }
        
        [DependsOn]
        [DevNote("Whether the loan is secured by a dwelling as defined by HMDA guidelines.")]
        public bool sHmdaIsSecuredByDwelling
        {
            get { return this.GetBoolField(nameof(sHmdaIsSecuredByDwelling)); }
            set { this.SetBoolField(nameof(sHmdaIsSecuredByDwelling), value); }
        }

        [DependsOn(nameof(sLoanVersionT), nameof(sReliedOnCreditScoreModelT), nameof(sReliedOnCreditScoreModelTOtherDescription), nameof(sReliedOnCreditScoreModelName))]
        private sHmdaCreditScoreModelT GetHmdaCreditScoreModelMappedFromReliedOn()
        {
            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V26_ReplaceFreeformCreditModelsWithEnums))
            {
                return Tools.MapHmdaCreditScoreModelByType(this.sReliedOnCreditScoreModelT, this.sReliedOnCreditScoreModelTOtherDescription);
            }

            return Tools.MapHmdaCreditScoreModelByName(this.sReliedOnCreditScoreModelName);
        }

        [DependsOn(nameof(sReliedOnCreditScoreSingleHmdaReportAsT), nameof(sReliedOnCreditScoreSourceT), nameof(CAppData.aBDecisionCreditSourceT),
            nameof(CAppData.aCDecisionCreditSourceT), nameof(GetHmdaReportTypeFromReliedOnCreditScoreSourceT), nameof(GetHmdaCreditScoreModelMappedFromReliedOn))]
        public sHmdaCreditScoreModelT GetHmdaCreditScoreModelTForBorrowerType(sHmdaReportCreditScoreAsT borrowerType)
        {
            // if sReliedOnCreditScoreSourceT is of Type1, Type2, Type2Soft, or Type3, simply return the mapped model name.
            // for borrower/coborrower X, check the app's decision aBDecisionCreditSourceT
            // if source is from Other, return Other, else return mapped model name.
            if (this.sReliedOnCreditScoreSingleHmdaReportAsT == borrowerType)
            {
                if (this.sReliedOnCreditScoreSourceT == HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Type1] ||
                    this.sReliedOnCreditScoreSourceT == HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Type2] ||
                    this.sReliedOnCreditScoreSourceT == HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Type2Soft] ||
                    this.sReliedOnCreditScoreSourceT == HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Type3])
                {
                    return GetHmdaCreditScoreModelMappedFromReliedOn();
                }
                else
                {
                    string reliedCreditScoreSource = this.sReliedOnCreditScoreSourceT;

                    if (reliedCreditScoreSource.Length != 38)
                    {
                        return GetHmdaCreditScoreModelMappedFromReliedOn();
                    }
                    else
                    {
                        sHmdaReportCreditScoreAsT applicantType = GetHmdaReportTypeFromReliedOnCreditScoreSourceT();

                        if (applicantType == sHmdaReportCreditScoreAsT.Blank)
                        {
                            return GetHmdaCreditScoreModelMappedFromReliedOn();
                        }

                        Guid appGuid = ToGuid(reliedCreditScoreSource.Substring(0, 36));

                        if (applicantType == borrowerType)
                        {
                            foreach (CAppBase app in this.m_dataApps)
                            {
                                if (app.aAppId == appGuid)
                                {
                                    if (borrowerType == sHmdaReportCreditScoreAsT.Borrower)
                                    {
                                        if (app.aBDecisionCreditSourceT == E_aDecisionCreditSourceT.Other)
                                        {
                                            return sHmdaCreditScoreModelT.Other;
                                        }
                                    }
                                    else if (borrowerType == sHmdaReportCreditScoreAsT.Coborrower)
                                    {
                                        if (app.aCDecisionCreditSourceT == E_aDecisionCreditSourceT.Other)
                                        {
                                            return sHmdaCreditScoreModelT.Other;
                                        }
                                    }
                                }
                            }
                        }

                        return GetHmdaCreditScoreModelMappedFromReliedOn();
                    }
                }
            }
            else if (this.sReliedOnCreditScoreSingleHmdaReportAsT != borrowerType)
            {
                return sHmdaCreditScoreModelT.NotApplicable;
            }
            else // Hmda Report as type has not been selected
            {
                return sHmdaCreditScoreModelT.LeaveBlank;
            }
        }
    

        [DependsOn(nameof(sHmdaIsCreditScoreReliedOn), nameof(sReliedOnCreditScoreModeT), nameof(sLoanVersionT), nameof(GetCreditModelType), nameof(GetCreditModelName), nameof(GetHmdaCreditScoreModelTForBorrowerType), nameof(IsCreditScoreNotApplicableForActionTaken))]
        [DevNote("The primary borrower's credit score model as defined by HMDA guidelines.")]
        public sHmdaCreditScoreModelT sHmdaBCreditScoreModelT
        {
            get
            {
                if (!this.sHmdaIsCreditScoreReliedOn || this.IsCreditScoreNotApplicableForActionTaken())
                {
                    return sHmdaCreditScoreModelT.NotApplicable;
                }

                if (this.sReliedOnCreditScoreModeT == ReliedOnCreditScoreMode.Blank)
                {
                    return sHmdaCreditScoreModelT.LeaveBlank;               
                }
                else if (this.sReliedOnCreditScoreModeT == ReliedOnCreditScoreMode.ApplicantIndividualScore)
                {
                    // existing behavior before HMDA relied on
                    var primaryApp = this.GetAppData(0);

                    if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V26_ReplaceFreeformCreditModelsWithEnums))
                    {
                        string otherDescription;
                        var creditModel = this.GetCreditModelType(primaryApp, E_BorrowerModeT.Borrower, out otherDescription);
                        return Tools.MapHmdaCreditScoreModelByType(creditModel, otherDescription);
                    }

                    return Tools.MapHmdaCreditScoreModelByName(this.GetCreditModelName(primaryApp, E_BorrowerModeT.Borrower));
                }
                else // this.sReliedOnCreditScoreModeT == ReliedOnCreditScoreMode.SingleScore
                {
                    return GetHmdaCreditScoreModelTForBorrowerType(sHmdaReportCreditScoreAsT.Borrower);
                }
            }
        }

        [DependsOn(nameof(sHmdaIsCreditScoreReliedOn), nameof(sHmdaBCreditScoreModelT), nameof(sReliedOnCreditScoreModeT), nameof(sLoanVersionT), nameof(GetCreditModelType),
            nameof(GetCreditModelName), nameof(sReliedOnCreditScoreSingleHmdaReportAsT), nameof(sReliedOnCreditScoreModelName))]
        [DevNote("The description for the primary borrower's scoring model if marked as 'Other' as defined by HMDA guidelines.")]
        public string sHmdaBCreditScoreModelOtherDescription
        {
            get
            {
                if (!sHmdaIsCreditScoreReliedOn || this.sHmdaBCreditScoreModelT != sHmdaCreditScoreModelT.Other || this.sReliedOnCreditScoreModeT == ReliedOnCreditScoreMode.Blank)
                {
                    return string.Empty;
                }                
                else if (this.sReliedOnCreditScoreModeT == ReliedOnCreditScoreMode.ApplicantIndividualScore)
                {
                    // existing behavior before HMDA relied on
                    var primaryApp = this.GetAppData(0);

                    if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V26_ReplaceFreeformCreditModelsWithEnums))
                    {
                        string otherDescription;
                        var creditModel = this.GetCreditModelType(primaryApp, E_BorrowerModeT.Borrower, out otherDescription);
                        var hmdaCreditModelType = Tools.MapHmdaCreditScoreModelByType(creditModel, otherDescription);

                        if (!string.IsNullOrEmpty(otherDescription))
                        {
                            return otherDescription;
                        }
                        else if (creditModel != E_CreditScoreModelT.Other)
                        {
                            return EnumUtilities.GetDescription(creditModel);
                        }
                        else
                        {
                            return string.Empty;
                        }
                    }

                    return this.GetCreditModelName(primaryApp, E_BorrowerModeT.Borrower);
                }
                else // this.sReliedOnCreditScoreModeT == ReliedOnCreditScoreMode.SingleScore
                {
                    if (this.sReliedOnCreditScoreSingleHmdaReportAsT == sHmdaReportCreditScoreAsT.Borrower)
                    {
                        if (this.sHmdaBCreditScoreModelT == sHmdaCreditScoreModelT.Other)
                        {
                            return this.sReliedOnCreditScoreModelName;
                        }                        
                        else
                        {
                            return string.Empty;
                        }
                    }    
                    else // coborrower or neither selected
                    {
                        return string.Empty;
                    }                
                }
            }
        }

        [DependsOn(nameof(sHmdaCoApplicantSourceT), nameof(sHmdaIsCreditScoreReliedOn), nameof(sReliedOnCreditScoreModeT), nameof(sfFindDataAppBySsn),
            nameof(sHmdaCoApplicantSourceSsn), nameof(sLoanVersionT), nameof(GetCreditModelType), nameof(GetCreditModelName), nameof(GetHmdaCreditScoreModelTForBorrowerType), nameof(IsCreditScoreNotApplicableForActionTaken))]
        [DevNote("The credit score model for the coborrower as defined by HMDA guidelines.")]
        public sHmdaCreditScoreModelT sHmdaCCreditScoreModelT
        {
            get
            {
                if (!this.sHmdaIsCreditScoreReliedOn || this.IsCreditScoreNotApplicableForActionTaken())
                {
                    return sHmdaCreditScoreModelT.NotApplicable;
                }

                var coapplicantSource = this.sHmdaCoApplicantSourceT;
                if (coapplicantSource == sHmdaCoApplicantSourceT.NoCoApplicant)
                {
                    return sHmdaCreditScoreModelT.NoCoApplicant;
                }

                if (this.sReliedOnCreditScoreModeT == ReliedOnCreditScoreMode.Blank)
                {
                    return sHmdaCreditScoreModelT.LeaveBlank;
                }
                else if (this.sReliedOnCreditScoreModeT == ReliedOnCreditScoreMode.ApplicantIndividualScore)
                {
                    // existing behavior before HMDA reliedon
                    CAppBase application = null;
                    switch (coapplicantSource)
                    {
                        case sHmdaCoApplicantSourceT.CoborrowerOnPrimaryApp:
                            application = this.GetAppData(0);
                            break;
                        case sHmdaCoApplicantSourceT.ApplicantOnSecondaryApp:
                            application = this.FindDataAppBySsn(this.sHmdaCoApplicantSourceSsn);
                            break;
                        default:
                            throw new UnhandledEnumException(coapplicantSource);
                    }

                    E_BorrowerModeT borrowerMode;

                    if (coapplicantSource == sHmdaCoApplicantSourceT.CoborrowerOnPrimaryApp)
                    {
                        borrowerMode = E_BorrowerModeT.Coborrower;
                    }
                    else
                    {
                        // The applicant may be listed as a borrower on a different app so we
                        // need to determine the correct borrower mode by checking the SSN.
                        borrowerMode = application.aBSsn == this.sHmdaCoApplicantSourceSsn
                            ? E_BorrowerModeT.Borrower
                            : E_BorrowerModeT.Coborrower;
                    }                    

                    if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V26_ReplaceFreeformCreditModelsWithEnums))
                    {
                        string otherDescription;
                        var modelType = this.GetCreditModelType(application, borrowerMode, out otherDescription);
                        return Tools.MapHmdaCreditScoreModelByType(modelType, otherDescription);
                    }
                    else
                    {
                        var modelName = this.GetCreditModelName(application, borrowerMode);
                        return Tools.MapHmdaCreditScoreModelByName(modelName);
                    }
                }
                else // this.sReliedOnCreditScoreModeT == ReliedOnCreditScoreMode.SingleScore
                {
                    return GetHmdaCreditScoreModelTForBorrowerType(sHmdaReportCreditScoreAsT.Coborrower);
                }
            }
        }

        [DependsOn(nameof(sHmdaCoApplicantSourceT), nameof(sHmdaCCreditScoreModelT), nameof(sHmdaIsCreditScoreReliedOn), nameof(sReliedOnCreditScoreModeT),
            nameof(sfFindDataAppBySsn), nameof(sHmdaCoApplicantSourceSsn), nameof(sLoanVersionT), nameof(GetCreditModelType), nameof(GetCreditModelName),
            nameof(sReliedOnCreditScoreSingleHmdaReportAsT), nameof(sHmdaCCreditScoreModelT), nameof(sReliedOnCreditScoreModelName))]
        [DevNote("The description for the coborrower's scoring model if marked as 'Other' as defined by HMDA guidelines.")]
        public string sHmdaCCreditScoreModelOtherDescription
        {
            get
            {
                var coapplicantSource = this.sHmdaCoApplicantSourceT;
                if (coapplicantSource == sHmdaCoApplicantSourceT.NoCoApplicant ||
                    this.sHmdaCCreditScoreModelT != sHmdaCreditScoreModelT.Other)
                {
                    return string.Empty;
                }

                if (!this.sHmdaIsCreditScoreReliedOn || this.sReliedOnCreditScoreModeT == ReliedOnCreditScoreMode.Blank)
                {
                    return string.Empty;
                }
                else if (this.sReliedOnCreditScoreModeT == ReliedOnCreditScoreMode.ApplicantIndividualScore)
                {
                    // existing behavior before HMDA relied on
                    CAppBase application = null;
                    switch (coapplicantSource)
                    {
                        case sHmdaCoApplicantSourceT.CoborrowerOnPrimaryApp:
                            application = this.GetAppData(0);
                            break;
                        case sHmdaCoApplicantSourceT.ApplicantOnSecondaryApp:
                            application = this.FindDataAppBySsn(this.sHmdaCoApplicantSourceSsn);
                            break;
                        default:
                            throw new UnhandledEnumException(coapplicantSource);
                    }

                    // The applicant may be listed as a borrower on a different app so we
                    // need to determine the correct borrower mode by checking the SSN.
                    var borrowerMode = application.aBSsn == this.sHmdaCoApplicantSourceSsn
                        ? E_BorrowerModeT.Borrower
                        : E_BorrowerModeT.Coborrower;

                    if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V26_ReplaceFreeformCreditModelsWithEnums))
                    {
                        string otherDescription;
                        var creditModel = this.GetCreditModelType(application, borrowerMode, out otherDescription);
                        var hmdaCreditModelType = Tools.MapHmdaCreditScoreModelByType(creditModel, otherDescription);

                        if (!string.IsNullOrEmpty(otherDescription))
                        {
                            return otherDescription;
                        }
                        else if (creditModel != E_CreditScoreModelT.Other)
                        {
                            return EnumUtilities.GetDescription(creditModel);
                        }
                        else
                        {
                            return string.Empty;
                        }
                    }

                    return this.GetCreditModelName(application, borrowerMode);
                }
                else // this.sReliedOnCreditScoreModeT == ReliedOnCreditScoreMode.SingleScore
                {
                    if (this.sReliedOnCreditScoreSingleHmdaReportAsT == sHmdaReportCreditScoreAsT.Coborrower)
                    {
                        if (this.sHmdaCCreditScoreModelT == sHmdaCreditScoreModelT.Other)
                        {
                            return this.sReliedOnCreditScoreModelName;
                        }
                        else
                        {
                            return string.Empty;
                        }
                    }
                    else // borrower or neither selected
                    {
                        return string.Empty;
                    }
                }
            }
        }

        /// <summary>
        /// Clears <see cref="sHmdaCoApplicantSourceSsnLckd"/> if the specified <paramref name="ssn"/>
        /// matches the loan's <see cref="sHmdaCoApplicantSourceSsn"/>.
        /// </summary>
        /// <param name="ssn">
        /// The SSN to check.
        /// </param>
        [DependsOn(nameof(sHmdaCoApplicantSourceSsn), nameof(sHmdaCoApplicantSourceSsnLckd))]
        public void ClearCoApplicantSourceSsn(string ssn)
        {
            if (string.Equals(this.sHmdaCoApplicantSourceSsn, ssn, StringComparison.Ordinal))
            {
                this.sHmdaCoApplicantSourceSsnLckd = false;
            }
        }

        [DependsOn(nameof(sCreditDecisionD), nameof(sHmdaActionTakenT))]
        private bool IsCreditScoreNotApplicableForActionTaken()
        {
            var actionTaken = this.sHmdaActionTakenT;

            return actionTaken == HmdaActionTaken.PurchasedLoan ||
                actionTaken == HmdaActionTaken.FileClosedForIncompleteness ||
                actionTaken == HmdaActionTaken.ApplicationWithdrawnByApplicant || 
                !this.sCreditDecisionD.IsValid;
        }

        [DependsOn(nameof(sHmdaActionTakenT))]
        private bool IsNotApplicableForHmdaActionTaken(params HmdaActionTaken[] actionsTaken)
        {
            return this.sHmdaActionTakenT.EqualsOneOf(actionsTaken);
        }

        [DependsOn(nameof(sHmdaActionTaken))]
        private bool IsNotApplicableForHmdaActionTaken(params string[] actionsTaken)
        {
            var loanActionTaken = this.sHmdaActionTaken;
            return actionsTaken.Any(action => string.Equals(action, loanActionTaken, StringComparison.OrdinalIgnoreCase));
        }

        /// <summary>
        /// This method attempts to retrieve the application for the HMDA co-applicant.
        /// Note that <see cref="IsCoApplicantCoBorr"/> must be used with the resulting
        /// app if retrieval is successful to determine whether the borrower or co-borrower
        /// fields should be used.
        /// </summary>
        /// <param name="dataApp">
        /// The retrieved app or null if the retrieval was not successful.
        /// </param>
        /// <returns>
        /// True if the retrieval was successful, false otherwise.
        /// </returns>
        [DependsOn(nameof(sHmdaCoApplicantSourceT), nameof(sHmdaCoApplicantSourceSsn), nameof(sfFindDataAppBySsn))]
        private bool TryGetCoApplicantApp(out CAppBase dataApp)
        {
            switch (this.sHmdaCoApplicantSourceT)
            {
                case sHmdaCoApplicantSourceT.NoCoApplicant:
                    dataApp = null;
                    break;
                case sHmdaCoApplicantSourceT.CoborrowerOnPrimaryApp:
                    dataApp = this.GetAppData(0);
                    break;
                case sHmdaCoApplicantSourceT.ApplicantOnSecondaryApp:
                    var ssn = this.sHmdaCoApplicantSourceSsn;
                    dataApp = this.FindDataAppBySsn(ssn);
                    break;
                default:
                    throw new UnhandledEnumException(this.sHmdaCoApplicantSourceT);
            }

            return dataApp != null;
        }

        private bool? isCoAppCoBorr = null;
        [DependsOn(nameof(TryGetCoApplicantApp), nameof(CAppBase.aCSsn), nameof(sHmdaCoApplicantSourceSsn))]
        private bool IsCoApplicantCoBorr
        {
            get
            {
                if(!isCoAppCoBorr.HasValue)
                {
                    CAppBase dataApp;
                    if(!this.TryGetCoApplicantApp(out dataApp))
                    {
                        return false;
                    }

                    isCoAppCoBorr = dataApp.aCSsn == this.sHmdaCoApplicantSourceSsn;
                }

                return isCoAppCoBorr.Value;
            }
        }

        [DependsOn(nameof(CalculateEthnicities), nameof(TryGetCoApplicantApp), nameof(IsCoApplicantCoBorr), nameof(sHmdaCoApplicantSourceSsn),
            nameof(sHmdaCoApplicantEthnicities), nameof(sHmdaBorrowerEthnicities))]
        private Hmda_Ethnicity RetrieveApplicantEthnicity(int ethnicityIndex, bool isCoApplicant)
        {
            List<Hmda_Ethnicity> ethnicities = isCoApplicant ? this.sHmdaCoApplicantEthnicities : this.sHmdaBorrowerEthnicities;

            return ethnicities.Count > ethnicityIndex ? ethnicities[ethnicityIndex] : Hmda_Ethnicity.Blank;
        }

        private List<Hmda_Ethnicity> borrowerEthnicities;
        [DependsOn(nameof(CalculateEthnicities))]
        private List<Hmda_Ethnicity> sHmdaBorrowerEthnicities
        {
            get
            {
                if(borrowerEthnicities == null)
                {
                    borrowerEthnicities = CalculateEthnicities(this.GetAppData(0), false, false);
                }

                return borrowerEthnicities;
            }
        }

        private List<Hmda_Ethnicity> coApplicantEthnicities;
        [DependsOn(nameof(sHmdaActionTakenT), nameof(sHmdaCoApplicantSourceT), nameof(CalculateEthnicities), nameof(TryGetCoApplicantApp), nameof(IsCoApplicantCoBorr))]
        private List<Hmda_Ethnicity> sHmdaCoApplicantEthnicities
        {
            get
            {
                if(coApplicantEthnicities == null)
                {
                    CAppBase dataApp;
                    if (this.sHmdaActionTakenT == HmdaActionTaken.PurchasedLoan)
                    {
                        coApplicantEthnicities = new List<Hmda_Ethnicity>() { Hmda_Ethnicity.NotApplicable };
                    }
                    else if (this.sHmdaCoApplicantSourceT == sHmdaCoApplicantSourceT.NoCoApplicant)
                    {
                        coApplicantEthnicities = new List<Hmda_Ethnicity>() { Hmda_Ethnicity.NoCoApplicant };
                    }
                    else if (this.TryGetCoApplicantApp(out dataApp))
                    {
                        coApplicantEthnicities = CalculateEthnicities(dataApp, true, this.IsCoApplicantCoBorr);
                    }
                    else
                    {
                        coApplicantEthnicities = new List<Hmda_Ethnicity>() { Hmda_Ethnicity.Blank };
                    }
                }

                return coApplicantEthnicities;
            }
        }

        [DependsOn( nameof(CAppBase.aCHispanicT), nameof(CAppBase.aCIsOtherHispanicOrLatino), nameof(CAppBase.aCIsMexican), 
            nameof(CAppBase.aCIsPuertoRican), nameof(CAppBase.aCIsCuban), nameof(CAppBase.aBHispanicT), 
            nameof(CAppBase.aBIsOtherHispanicOrLatino), nameof(CAppBase.aBIsMexican), nameof(CAppBase.aBIsPuertoRican), 
            nameof(CAppBase.aBIsCuban)) ]
        private List<Hmda_Ethnicity> FillEthnicityFields(CAppBase dataApp, bool isCoBorr)
        {
            var hispanicT = isCoBorr ? dataApp.aCHispanicT : dataApp.aBHispanicT;
            var isHispanicOther = isCoBorr ? dataApp.aCIsOtherHispanicOrLatino : dataApp.aBIsOtherHispanicOrLatino;
            var isMexican = isCoBorr ? dataApp.aCIsMexican : dataApp.aBIsMexican;
            var isPuertoRican = isCoBorr ? dataApp.aCIsPuertoRican : dataApp.aBIsPuertoRican;
            var isCuban = isCoBorr ? dataApp.aCIsCuban : dataApp.aBIsCuban;

            var isHispanic = hispanicT == E_aHispanicT.Hispanic;
            List<Hmda_Ethnicity> ethnicities = new List<Hmda_Ethnicity>();

            if (hispanicT == E_aHispanicT.BothHispanicAndNotHispanic)
            {
                ethnicities.Add(Hmda_Ethnicity.HispanicOrLatino);
                ethnicities.Add(Hmda_Ethnicity.NotHispanicOrLatino);
            }
            else if (isHispanic)
            {
                ethnicities.Add(Hmda_Ethnicity.HispanicOrLatino);
            }
            else if (hispanicT == E_aHispanicT.NotHispanic)
            {
                ethnicities.Add(Hmda_Ethnicity.NotHispanicOrLatino);
            }

            if (isHispanicOther)
            {
                ethnicities.Add(Hmda_Ethnicity.OtherHispanicOrLatino);
            }

            if (isMexican)
            {
                ethnicities.Add(Hmda_Ethnicity.Mexican);
            }

            if (isPuertoRican)
            {
                ethnicities.Add(Hmda_Ethnicity.PuertoRican);
            }

            if (isCuban)
            {
                ethnicities.Add(Hmda_Ethnicity.Cuban);
            }

            return ethnicities;
        }

        [DependsOn(nameof(sHmdaActionTakenT), nameof(FillEthnicityFields),
            nameof(CAppBase.aCIsValidHmdaApplication), nameof(CAppBase.aCInterviewMethodT), nameof(CAppBase.aCDoesNotWishToProvideEthnicity),
            nameof(CAppBase.aBIsValidHmdaApplication), nameof(CAppBase.aBInterviewMethodT), nameof(CAppBase.aBDoesNotWishToProvideEthnicity) )]
        private List<Hmda_Ethnicity> CalculateEthnicities(CAppBase dataApp, bool isCoApplicant, bool isCoBorr)
        {
            List<Hmda_Ethnicity> ethnicities = new List<Hmda_Ethnicity>();
            if (!(isCoBorr ? dataApp.aCIsValidHmdaApplication : dataApp.aBIsValidHmdaApplication))
            {
                ethnicities.Add(Hmda_Ethnicity.Blank);
                return ethnicities;
            }
            
            var interviewMethodT = isCoBorr ? dataApp.aCInterviewMethodT : dataApp.aBInterviewMethodT;
            var doesNotWishToProvideEthnicity = isCoBorr ? dataApp.aCDoesNotWishToProvideEthnicity : dataApp.aBDoesNotWishToProvideEthnicity;

            if (this.sHmdaActionTakenT == HmdaActionTaken.PurchasedLoan)
            {
                ethnicities.Add(Hmda_Ethnicity.NotApplicable);
            }
            else if (interviewMethodT == E_aIntrvwrMethodT.LeaveBlank)
            {
                ethnicities.Add(Hmda_Ethnicity.Blank);
            }
            else if (interviewMethodT != E_aIntrvwrMethodT.FaceToFace && doesNotWishToProvideEthnicity)
            {
                ethnicities.Add(Hmda_Ethnicity.NotProvided);
            }
            else
            {
                ethnicities = FillEthnicityFields(dataApp, isCoBorr);
            }

            return ethnicities;
        }

        [DependsOn(nameof(RetrieveApplicantEthnicity))]
        [DevNote("The Borrowers's first ethnicity.")]
        public Hmda_Ethnicity sHmdaBEthnicity1T
        {
            get
            {
                return this.RetrieveApplicantEthnicity(0, false);
            }
        }

        [DependsOn(nameof(RetrieveApplicantEthnicity))]
        [DevNote("The Borrowers's second ethnicity.")]
        public Hmda_Ethnicity sHmdaBEthnicity2T
        {
            get
            {
                return this.RetrieveApplicantEthnicity(1, false);
            }
        }

        [DependsOn(nameof(RetrieveApplicantEthnicity))]
        [DevNote("The Borrowers's third ethnicity.")]
        public Hmda_Ethnicity sHmdaBEthnicity3T
        {
            get
            {
                return this.RetrieveApplicantEthnicity(2, false);
            }
        }

        [DependsOn(nameof(RetrieveApplicantEthnicity))]
        [DevNote("The Borrowers's fourth ethnicity.")]
        public Hmda_Ethnicity sHmdaBEthnicity4T
        {
            get
            {
                return this.RetrieveApplicantEthnicity(3, false);
            }
        }

        [DependsOn(nameof(RetrieveApplicantEthnicity))]
        [DevNote("The Borrowers's fifth ethnicity.")]
        public Hmda_Ethnicity sHmdaBEthnicity5T
        {
            get
            {
                return this.RetrieveApplicantEthnicity(4, false);
            }
        }

        [DependsOn(nameof(sHmdaActionTakenT), nameof(CAppBase.aBOtherHispanicOrLatinoDescription))]
        [DevNote("The Borrower's Ethnicity description if other is selected.")]
        public string sHmdaBEthnicityOtherDescription
        {
            get
            {
                if (this.sHmdaActionTakenT == HmdaActionTaken.PurchasedLoan)
                {
                    return string.Empty;
                }

                return this.GetAppData(0).aBOtherHispanicOrLatinoDescription;
            }
        }

        [DependsOn(nameof(sHmdaActionTakenT), nameof(CAppBase.aBEthnicityCollectedByObservationOrSurname), nameof(CAppBase.aBInterviewMethodT))]
        [DevNote("How the Borrower's ethnicity data was collected.")]
        public VisualObservationSurnameCollection sHmdaBEthnicityCollectedByObservationOrSurname
        {
            get
            {
                if (this.sHmdaActionTakenT == HmdaActionTaken.PurchasedLoan)
                {
                    return VisualObservationSurnameCollection.NotApplicable;
                }

                var dataApp = this.GetAppData(0);
                if (dataApp.aBInterviewMethodT.EqualsOneOf(E_aIntrvwrMethodT.ByTelephone, E_aIntrvwrMethodT.ByMail, E_aIntrvwrMethodT.Internet))
                {
                    return VisualObservationSurnameCollection.NotCollectedOnBasisOfVisualObservationOrSurname;
                }

                E_TriState isCollectedByObservationOrSurname = dataApp.aBEthnicityCollectedByObservationOrSurname;
                return DetermineCollectionFromTriState(isCollectedByObservationOrSurname);
            }
        }

        [DependsOn(nameof(RetrieveApplicantEthnicity))]
        [DevNote("The Co-Applicants's first ethnicity.")]
        public Hmda_Ethnicity sHmdaCEthnicity1T
        {
            get
            {
                return this.RetrieveApplicantEthnicity(0, true);
            }
        }

        [DependsOn(nameof(RetrieveApplicantEthnicity))]
        [DevNote("The Co-Applicants's second ethnicity.")]
        public Hmda_Ethnicity sHmdaCEthnicity2T
        {
            get
            {
                return this.RetrieveApplicantEthnicity(1, true);
            }
        }

        [DependsOn(nameof(RetrieveApplicantEthnicity))]
        [DevNote("The Co-Applicants's third ethnicity.")]
        public Hmda_Ethnicity sHmdaCEthnicity3T
        {
            get
            {
                return this.RetrieveApplicantEthnicity(2, true);
            }
        }

        [DependsOn(nameof(RetrieveApplicantEthnicity))]
        [DevNote("The Co-Applicants's fourth ethnicity.")]
        public Hmda_Ethnicity sHmdaCEthnicity4T
        {
            get
            {
                return this.RetrieveApplicantEthnicity(3, true);
            }
        }

        [DependsOn(nameof(RetrieveApplicantEthnicity))]
        [DevNote("The Co-Applicants's fifth ethnicity.")]
        public Hmda_Ethnicity sHmdaCEthnicity5T
        {
            get
            {
                return this.RetrieveApplicantEthnicity(4, true);
            }
        }

        [DependsOn(nameof(IsCoApplicantCoBorr), nameof(TryGetCoApplicantApp), nameof(sHmdaActionTakenT),
            nameof(CAppBase.aCOtherHispanicOrLatinoDescription), nameof(CAppBase.aBOtherHispanicOrLatinoDescription))]
        [DevNote("The Co-Applicants's ethnicity description if other is selected.")]
        public string sHmdaCEthnicityOtherDescription
        {
            get
            {
                CAppBase dataApp;
                if(!this.TryGetCoApplicantApp(out dataApp))
                {
                    return string.Empty;
                }
                else if (this.sHmdaActionTakenT == HmdaActionTaken.PurchasedLoan)
                {
                    return string.Empty;
                }


                return IsCoApplicantCoBorr ? dataApp?.aCOtherHispanicOrLatinoDescription : dataApp?.aBOtherHispanicOrLatinoDescription;
            }
        }

        [DependsOn(nameof(sHmdaActionTakenT), nameof(sHmdaCoApplicantSourceT), nameof(TryGetCoApplicantApp), 
            nameof(IsCoApplicantCoBorr),
            nameof(CAppBase.aCEthnicityCollectedByObservationOrSurname), nameof(CAppBase.aBEthnicityCollectedByObservationOrSurname), nameof(CAppBase.aCInterviewMethodT), nameof(CAppBase.aBInterviewMethodT))]
        [DevNote("How the Co-Applicant's ethnicity was collected.")]
        public VisualObservationSurnameCollection sHmdaCEthnicityCollectedByObservationOrSurname
        {
            get
            {
                if (this.sHmdaActionTakenT == HmdaActionTaken.PurchasedLoan)
                {
                    return VisualObservationSurnameCollection.NotApplicable;
                }

                if (this.sHmdaCoApplicantSourceT == sHmdaCoApplicantSourceT.NoCoApplicant)
                {
                    return VisualObservationSurnameCollection.NoCoApplicant;
                }

                CAppBase dataApp;
                if (!this.TryGetCoApplicantApp(out dataApp))
                {
                    return VisualObservationSurnameCollection.Blank;
                }

                var interviewMethod = this.IsCoApplicantCoBorr ? dataApp.aCInterviewMethodT : dataApp.aBInterviewMethodT;
                if (interviewMethod.EqualsOneOf(E_aIntrvwrMethodT.ByTelephone, E_aIntrvwrMethodT.ByMail, E_aIntrvwrMethodT.Internet))
                {
                    return VisualObservationSurnameCollection.NotCollectedOnBasisOfVisualObservationOrSurname;
                }

                E_TriState isCollectedByObservationOrSurname = IsCoApplicantCoBorr ? dataApp?.aCEthnicityCollectedByObservationOrSurname ?? E_TriState.Blank : dataApp?.aBEthnicityCollectedByObservationOrSurname ?? E_TriState.Blank;
                return DetermineCollectionFromTriState(isCollectedByObservationOrSurname);
            }
        }

        [DependsOn(nameof(CalculateRaces), nameof(TryGetCoApplicantApp),
            nameof(sHmdaCoApplicantSourceSsn), nameof(sHmdaCoApplicantRaces), nameof(sHmdaBorrowerRaces))]
        private Hmda_Race RetrieveApplicantRace(int raceIndex, bool isCoApplicant)
        {
            List<Hmda_Race> races = isCoApplicant ? this.sHmdaCoApplicantRaces : this.sHmdaBorrowerRaces;

            return races.Count > raceIndex ? races[raceIndex] : Hmda_Race.Blank;
        }

        private List<Hmda_Race> borrowerRaces;
        [DependsOn(nameof(CalculateRaces))]
        private List<Hmda_Race> sHmdaBorrowerRaces
        {
            get
            {
                if(borrowerRaces == null)
                {
                    borrowerRaces = CalculateRaces(this.GetAppData(0), false, false);
                }

                return borrowerRaces;
            }
        }

        private List<Hmda_Race> coApplicantRaces;
        [DependsOn(nameof(sHmdaActionTakenT), nameof(sHmdaCoApplicantSourceT), nameof(CalculateRaces), nameof(TryGetCoApplicantApp), nameof(IsCoApplicantCoBorr))]
        private List<Hmda_Race> sHmdaCoApplicantRaces
        {
            get
            {
                if (coApplicantRaces == null)
                {
                    CAppBase dataApp = null;

                    if (this.sHmdaActionTakenT == HmdaActionTaken.PurchasedLoan)
                    {
                        coApplicantRaces = new List<Hmda_Race>() { Hmda_Race.NotApplicable };
                    }
                    else if (this.sHmdaCoApplicantSourceT == sHmdaCoApplicantSourceT.NoCoApplicant)
                    {
                        coApplicantRaces = new List<Hmda_Race>() { Hmda_Race.NoCoApplicant };
                    }
                    else if (!this.TryGetCoApplicantApp(out dataApp))
                    {
                        coApplicantRaces = new List<Hmda_Race>() { Hmda_Race.Blank };
                    }
                    else
                    {
                        coApplicantRaces = CalculateRaces(dataApp, true, IsCoApplicantCoBorr);
                    }
                }

                return coApplicantRaces;
            }
        }

        [DependsOn( nameof(CAppBase.aCIsAmericanIndian), nameof(CAppBase.aCIsChinese), nameof(CAppBase.aCIsFilipino),
            nameof(CAppBase.aCIsJapanese), nameof(CAppBase.aCIsKorean), nameof(CAppBase.aCIsVietnamese), nameof(CAppBase.aCIsOtherAsian),
            nameof(CAppBase.aCIsNativeHawaiian), nameof(CAppBase.aCIsGuamanianOrChamorro), nameof(CAppBase.aCIsPacificIslander), 
            nameof(CAppBase.aCIsSamoan), nameof(CAppBase.aCIsOtherPacificIslander), nameof(CAppBase.aCIsSamoan),
            nameof(CAppBase.aCIsAsian), nameof(CAppBase.aCIsBlack), nameof(CAppBase.aCIsWhite),
            nameof(CAppBase.aBIsChinese), nameof(CAppBase.aCIsAsianIndian), nameof(CAppBase.aBIsFilipino), nameof(CAppBase.aBIsJapanese), nameof(CAppBase.aBIsKorean),
            nameof(CAppBase.aBIsVietnamese), nameof(CAppBase.aBIsOtherAsian), nameof(CAppBase.aBIsNativeHawaiian), nameof(CAppBase.aBIsGuamanianOrChamorro),
            nameof(CAppBase.aBIsOtherPacificIslander), nameof(CAppBase.aBIsSamoan), nameof(CAppBase.aBIsAmericanIndian), nameof(CAppBase.aBIsAsian),
            nameof(CAppBase.aBIsBlack), nameof(CAppBase.aBIsWhite), nameof(CAppBase.aBIsAsianIndian), nameof(CAppBase.aBIsPacificIslander), nameof(CAppBase.aCIsSamoan) )]
        private List<Hmda_Race> FillRaceFields(CAppBase dataApp, bool isCoBorr)
        {
            var isAsianIndian = isCoBorr ? dataApp.aCIsAsianIndian : dataApp.aBIsAsianIndian;
            var isChinese = isCoBorr ? dataApp.aCIsChinese : dataApp.aBIsChinese;
            var isFilipino = isCoBorr ? dataApp.aCIsFilipino : dataApp.aBIsFilipino;
            var isJapanese = isCoBorr ? dataApp.aCIsJapanese : dataApp.aBIsJapanese;
            var isKorean = isCoBorr ? dataApp.aCIsKorean : dataApp.aBIsKorean;
            var isVietnamese = isCoBorr ? dataApp.aCIsVietnamese : dataApp.aBIsVietnamese;
            var isOtherAsian = isCoBorr ? dataApp.aCIsOtherAsian : dataApp.aBIsOtherAsian;
            var isNativeHawaiian = isCoBorr ? dataApp.aCIsNativeHawaiian : dataApp.aBIsNativeHawaiian;
            var isGuamanianOrChamorro = isCoBorr ? dataApp.aCIsGuamanianOrChamorro : dataApp.aBIsGuamanianOrChamorro;
            var isOtherPacificIslander = isCoBorr ? dataApp.aCIsOtherPacificIslander : dataApp.aBIsOtherPacificIslander;
            var isPacificIslander = isCoBorr ? dataApp.aCIsPacificIslander : dataApp.aBIsPacificIslander;
            var isSamoan = isCoBorr ? dataApp.aCIsSamoan : dataApp.aCIsSamoan;

            var isAmericanIndianOrAlaskanNative = isCoBorr ? dataApp.aCIsAmericanIndian : dataApp.aBIsAmericanIndian;
            var isAsian = isCoBorr ? dataApp.aCIsAsian : dataApp.aBIsAsian;
            var isBlack = isCoBorr ? dataApp.aCIsBlack : dataApp.aBIsBlack;
            var isWhite = isCoBorr ? dataApp.aCIsWhite : dataApp.aBIsWhite;
            List<Hmda_Race> races = new List<Hmda_Race>();

            if(isAmericanIndianOrAlaskanNative)
            {
                races.Add(Hmda_Race.AmericanIndianOrAlaskanNative);
            }

            if(isAsian)
            {
                races.Add(Hmda_Race.Asian);
            }

            if(isBlack)
            {
                races.Add(Hmda_Race.BlackOrAfricanAmerican);
            }

            if(isPacificIslander)
            {
                races.Add(Hmda_Race.NativeHawaiianOrOtherPacificIslander);
            }

            if(isWhite)
            {
                races.Add(Hmda_Race.White);
            }

            if(isOtherAsian)
            {
                races.Add(Hmda_Race.OtherAsian);
            }

            if(isOtherPacificIslander)
            {
                races.Add(Hmda_Race.OtherPacificIslander);
            }

            if(isAsianIndian)
            {
                races.Add(Hmda_Race.AsianIndian);
            }

            if(isChinese)
            {
                races.Add(Hmda_Race.Chinese);
            }

            if(isFilipino)
            {
                races.Add(Hmda_Race.Filipino);
            }

            if(isJapanese)
            {
                races.Add(Hmda_Race.Japanese);
            }

            if(isKorean)
            {
                races.Add(Hmda_Race.Korean);
            }

            if(isVietnamese)
            {
                races.Add(Hmda_Race.Vietnamese);
            }

            if(isNativeHawaiian)
            {
                races.Add(Hmda_Race.NativeHawaiian);
            }

            if(isGuamanianOrChamorro)
            {
                races.Add(Hmda_Race.GuamanianOrChamorro);
            }

            if(isSamoan)
            {
                races.Add(Hmda_Race.Samoan);
            }

            return races;
        }

        [DependsOn(nameof(FillRaceFields), nameof(sHmdaCoApplicantSourceT), nameof(sHmdaActionTakenT),
            nameof(CAppBase.aCIsValidHmdaApplication), nameof(CAppBase.aCInterviewMethodT), nameof(CAppBase.aCDoesNotWishToProvideRace),
            nameof(CAppBase.aBIsValidHmdaApplication), nameof(CAppBase.aBInterviewMethodT), nameof(CAppBase.aBDoesNotWishToProvideRace),
            nameof(sHmdaCoApplicantSourceT))]
        private List<Hmda_Race> CalculateRaces(CAppBase dataApp, bool isCoApplicant, bool isCoBorr)
        {
            List<Hmda_Race> races = new List<Hmda_Race>();
            if (isCoApplicant && this.sHmdaCoApplicantSourceT == sHmdaCoApplicantSourceT.NoCoApplicant)
            {
                races.Add(Hmda_Race.NoCoApplicant);
                return races;
            }
            else if(!(isCoBorr ? dataApp.aCIsValidHmdaApplication : dataApp.aBIsValidHmdaApplication))
            {
                races.Add(Hmda_Race.Blank);
                return races;
            }

            var interviewMethodT = isCoBorr ? dataApp.aCInterviewMethodT : dataApp.aBInterviewMethodT;
            var doesNotWishToProvideRace = isCoBorr ? dataApp.aCDoesNotWishToProvideRace : dataApp.aBDoesNotWishToProvideRace;

            if (this.sHmdaActionTakenT == HmdaActionTaken.PurchasedLoan)
            {
                races.Add(Hmda_Race.NotApplicable);
            }
            else if (interviewMethodT == E_aIntrvwrMethodT.LeaveBlank)
            {
                races.Add(Hmda_Race.Blank);
            }
            else if (interviewMethodT != E_aIntrvwrMethodT.FaceToFace && doesNotWishToProvideRace)
            {
                races.Add(Hmda_Race.NotProvided);
            }
            else
            {
                races = FillRaceFields(dataApp, isCoBorr);
            }

            return races;
        }
        
        [DependsOn(nameof(RetrieveApplicantRace))]
        [DevNote("The Borrower's first race.")]
        public Hmda_Race sHmdaBRace1T
        {
            get
            {
                return RetrieveApplicantRace(0, false);
            }
        }
        
        [DependsOn(nameof(RetrieveApplicantRace))]
        [DevNote("The Borrower's second race.")]
        public Hmda_Race sHmdaBRace2T
        {
            get
            {
                return RetrieveApplicantRace(1, false);
            }
        }
        
        [DependsOn(nameof(RetrieveApplicantRace))]
        [DevNote("The Borrower's third race.")]
        public Hmda_Race sHmdaBRace3T
        {
            get
            {
                return RetrieveApplicantRace(2, false);
            }
        }
        
        [DependsOn(nameof(RetrieveApplicantRace))]
        [DevNote("The Borrower's fourth race.")]
        public Hmda_Race sHmdaBRace4T
        {
            get
            {
                return RetrieveApplicantRace(3, false);
            }
        }
        
        [DependsOn(nameof(RetrieveApplicantRace))]
        [DevNote("The Borrower's fifth race.")]
        public Hmda_Race sHmdaBRace5T
        {
            get
            {
                return RetrieveApplicantRace(4, false);
            }
        }

        [DependsOn(nameof(sHmdaActionTakenT), nameof(CAppBase.aBOtherAmericanIndianDescription))]
        [DevNote("The Borrower's other American Indian or Alaskan Native race description.")]
        public string sHmdaBEnrolledOrPrincipalTribe
        {
            get
            {
                var dataApp = this.GetAppData(0);
                return this.sHmdaActionTakenT == HmdaActionTaken.PurchasedLoan ? string.Empty : dataApp.aBOtherAmericanIndianDescription;
            }
        }

        [DependsOn(nameof(sHmdaActionTakenT), nameof(CAppBase.aBOtherAsianDescription))]
        [DevNote("The Borrower's other Asian race description.")]
        public string sHmdaBOtherAsianDescription
        {
            get
            {
                var dataApp = this.GetAppData(0);
                return this.sHmdaActionTakenT == HmdaActionTaken.PurchasedLoan ? string.Empty : dataApp.aBOtherAsianDescription;
            }
        }

        [DependsOn(nameof(sHmdaActionTakenT), nameof(CAppBase.aBOtherPacificIslanderDescription))]
        [DevNote("The Borrower's other Pacific Islander race description.")]
        public string sHmdaBOtherPacificIslanderDescription
        {
            get
            {
                var dataApp = this.GetAppData(0);
                return this.sHmdaActionTakenT == HmdaActionTaken.PurchasedLoan ? string.Empty : dataApp.aBOtherPacificIslanderDescription;
            }
        }

        [DependsOn(nameof(RetrieveApplicantRace))]
        [DevNote("The Co-Applicant's first race.")]
        public Hmda_Race sHmdaCRace1T
        {
            get
            {
                return RetrieveApplicantRace(0, true);
            }
        }

        [DependsOn(nameof(RetrieveApplicantRace))]
        [DevNote("The Co-Applicant's second race.")]
        public Hmda_Race sHmdaCRace2T
        {
            get
            {
                return RetrieveApplicantRace(1, true);
            }
        }

        [DependsOn(nameof(RetrieveApplicantRace))]
        [DevNote("The Co-Applicant's third race.")]
        public Hmda_Race sHmdaCRace3T
        {
            get
            {
                return RetrieveApplicantRace(2, true);
            }
        }

        [DependsOn(nameof(RetrieveApplicantRace))]
        [DevNote("The Co-Applicant's fourth race.")]
        public Hmda_Race sHmdaCRace4T
        {
            get
            {
                return RetrieveApplicantRace(3, true);
            }
        }

        [DependsOn(nameof(RetrieveApplicantRace))]
        [DevNote("The Co-Applicant's fifth race.")]
        public Hmda_Race sHmdaCRace5T
        {
            get
            {
                return RetrieveApplicantRace(4, true);
            }
        }

        [DependsOn(nameof(sHmdaCoApplicantRaces), nameof(IsCoApplicantCoBorr),
            nameof(CAppBase.aCOtherAmericanIndianDescription), nameof(CAppBase.aBOtherAmericanIndianDescription), nameof(TryGetCoApplicantApp), nameof(sHmdaActionTakenT)
            ) ]
        [DevNote("The Co-Applicant's other American Indian or Alaskan Native race description.")]
        public string sHmdaCEnrolledOrPrincipalTribe
        {
            get
            {
                CAppBase dataApp;
                if(!this.TryGetCoApplicantApp(out dataApp))
                {
                    return string.Empty;
                }
                else if (this.sHmdaActionTakenT == HmdaActionTaken.PurchasedLoan)
                {
                    return string.Empty;
                }

                return IsCoApplicantCoBorr ? dataApp.aCOtherAmericanIndianDescription : dataApp.aBOtherAmericanIndianDescription;
            }
        }

        [DependsOn(nameof(sHmdaCoApplicantRaces), nameof(IsCoApplicantCoBorr), nameof(CAppBase.aCOtherAsianDescription), nameof(CAppBase.aBOtherAsianDescription), nameof(TryGetCoApplicantApp), nameof(sHmdaActionTakenT)) ]
        [DevNote("The Co-Applicant's other Asian race description.")]
        public string sHmdaCOtherAsianDescription
        {
            get
            {
                CAppBase dataApp;
                if(!this.TryGetCoApplicantApp(out dataApp))
                {
                    return string.Empty;
                }
                else if (this.sHmdaActionTakenT == HmdaActionTaken.PurchasedLoan)
                {
                    return string.Empty;
                }

                return IsCoApplicantCoBorr ? dataApp.aCOtherAsianDescription : dataApp.aBOtherAsianDescription;
            }
        }

        [DependsOn(nameof(IsCoApplicantCoBorr), nameof(CAppBase.aCOtherPacificIslanderDescription), nameof(CAppBase.aBOtherPacificIslanderDescription), nameof(TryGetCoApplicantApp), nameof(sHmdaActionTakenT)) ]
        [DevNote("The Co-Applicant's other Pacific Islander race description.")]
        public string sHmdaCOtherPacificIslanderDescription
        {
            get
            {
                CAppBase dataApp;
                if (!this.TryGetCoApplicantApp(out dataApp))
                {
                    return string.Empty;
                }
                else if (this.sHmdaActionTakenT == HmdaActionTaken.PurchasedLoan)
                {
                    return string.Empty;
                }

                return IsCoApplicantCoBorr ? dataApp.aCOtherPacificIslanderDescription : dataApp.aBOtherPacificIslanderDescription;
            }
        }

        [DependsOn(nameof(sHmdaActionTakenT), nameof(CAppBase.aBRaceCollectedByObservationOrSurname), nameof(CAppBase.aBInterviewMethodT))]
        [DevNote("How the Borrower's Race info was collected.")]
        public VisualObservationSurnameCollection sHmdaBRaceCollectedByObservationOrSurname
        {
            get
            {
                if (this.sHmdaActionTakenT == HmdaActionTaken.PurchasedLoan)
                {
                    return VisualObservationSurnameCollection.NotApplicable;
                }

                var dataApp = this.GetAppData(0);
                if (dataApp.aBInterviewMethodT.EqualsOneOf(E_aIntrvwrMethodT.ByTelephone, E_aIntrvwrMethodT.ByMail, E_aIntrvwrMethodT.Internet))
                {
                    return VisualObservationSurnameCollection.NotCollectedOnBasisOfVisualObservationOrSurname;
                }

                var isCollectedByObservationOrSurname = dataApp.aBRaceCollectedByObservationOrSurname;
                return DetermineCollectionFromTriState(isCollectedByObservationOrSurname);
            }
        }

        [DependsOn(nameof(sHmdaActionTakenT), nameof(sHmdaCoApplicantSourceT), nameof(TryGetCoApplicantApp), nameof(IsCoApplicantCoBorr),
            nameof(CAppBase.aCRaceCollectedByObservationOrSurname), nameof(CAppBase.aBRaceCollectedByObservationOrSurname), nameof(CAppBase.aCInterviewMethodT), nameof(CAppBase.aBInterviewMethodT))]
        [DevNote("How the Co-Applicant's Race info was colelcted.")]
        public VisualObservationSurnameCollection sHmdaCRaceCollectedByObservationOrSurname
        {
            get
            {
                if (this.sHmdaActionTakenT == HmdaActionTaken.PurchasedLoan)
                {
                    return VisualObservationSurnameCollection.NotApplicable;
                }

                if (this.sHmdaCoApplicantSourceT == sHmdaCoApplicantSourceT.NoCoApplicant)
                {
                    return VisualObservationSurnameCollection.NoCoApplicant;
                }

                CAppBase dataApp;
                if (!TryGetCoApplicantApp(out dataApp))
                {
                    return VisualObservationSurnameCollection.Blank;
                }

                var interviewMethod = this.IsCoApplicantCoBorr ? dataApp.aCInterviewMethodT : dataApp.aBInterviewMethodT;
                if (interviewMethod.EqualsOneOf(E_aIntrvwrMethodT.ByTelephone, E_aIntrvwrMethodT.ByMail, E_aIntrvwrMethodT.Internet))
                {
                    return VisualObservationSurnameCollection.NotCollectedOnBasisOfVisualObservationOrSurname;
                }

                var isCollectedByObservationOrSurname = this.IsCoApplicantCoBorr ? dataApp.aCRaceCollectedByObservationOrSurname : dataApp.aBRaceCollectedByObservationOrSurname;
                return DetermineCollectionFromTriState(isCollectedByObservationOrSurname);
            }
        }

        private Dictionary<E_GenderT, Hmda_Sex> genderToSexMap = new Dictionary<E_GenderT, Hmda_Sex>() {
            {E_GenderT.Female, Hmda_Sex.Female },
            {E_GenderT.FemaleAndNotFurnished, Hmda_Sex.Female },
            {E_GenderT.LeaveBlank, Hmda_Sex.Blank },
            {E_GenderT.Male, Hmda_Sex.Male },
            {E_GenderT.MaleAndFemale, Hmda_Sex.BothMaleFemale },
            {E_GenderT.MaleAndNotFurnished, Hmda_Sex.Male },
            {E_GenderT.MaleFemaleNotFurnished, Hmda_Sex.BothMaleFemale },
            {E_GenderT.NA, Hmda_Sex.NotApplicable },
            {E_GenderT.Unfurnished, Hmda_Sex.NotProvided }
        };

        [DependsOn(nameof(sHmdaCoApplicantSourceT), nameof(sHmdaActionTakenT), nameof(TryGetCoApplicantApp), nameof(IsCoApplicantCoBorr),
            nameof(CAppBase.aCInterviewMethodT), nameof(CAppBase.aCGender),
            nameof(CAppBase.aBInterviewMethodT), nameof(CAppBase.aBGender))]
        private Hmda_Sex CalculateSex(bool isCoApplicant)
        {
            if (this.sHmdaActionTakenT == HmdaActionTaken.PurchasedLoan)
            {
                return Hmda_Sex.NotApplicable;
            }
            else if (isCoApplicant && this.sHmdaCoApplicantSourceT == sHmdaCoApplicantSourceT.NoCoApplicant)
            {
                return Hmda_Sex.NoCoApplicant;
            }

            CAppBase dataApp;
            if(isCoApplicant)
            {
                if(!this.TryGetCoApplicantApp(out dataApp))
                {
                    return Hmda_Sex.Blank;
                }
            }
            else
            {
                dataApp = this.GetAppData(0);
            }

            var interviewMethod = isCoApplicant && this.IsCoApplicantCoBorr ? dataApp.aCInterviewMethodT : dataApp.aBInterviewMethodT;
            var gender = isCoApplicant && IsCoApplicantCoBorr ? dataApp.aCGender : dataApp.aBGender;
            var doesNotWishToProvide = gender == E_GenderT.Unfurnished;

            if(interviewMethod == E_aIntrvwrMethodT.LeaveBlank)
            {
                return Hmda_Sex.Blank;
            }
            else if(interviewMethod != E_aIntrvwrMethodT.FaceToFace && doesNotWishToProvide)
            {
                return Hmda_Sex.NotProvided;
            }
            else
            {
                Hmda_Sex sex;
                this.genderToSexMap.TryGetValue(gender, out sex);
                return sex;
            }
        }

        [DependsOn(nameof(CalculateSex))]
        [DevNote("The borrower's sex type.")]
        public Hmda_Sex sHmdaBSexT
        {
            get
            {
                return CalculateSex(false);
            }
        }

        [DependsOn(nameof(CalculateSex))]
        [DevNote("The co-applicant's sex type.")]
        public Hmda_Sex sHmdaCSexT
        {
            get
            {
                return CalculateSex(true);
            }
        }

        [DependsOn(nameof(sHmdaActionTakenT), nameof(CAppBase.aBSexCollectedByObservationOrSurname), nameof(CAppBase.aBInterviewMethodT))]
        [DevNote("How the Borrower's sex info was collected.")]
        public VisualObservationSurnameCollection sHmdaBSexCollectedByObservationOrSurname
        {
            get
            {
                if(this.sHmdaActionTakenT == HmdaActionTaken.PurchasedLoan)
                {
                    return VisualObservationSurnameCollection.NotApplicable;
                }

                var dataApp = this.GetAppData(0);
                if (dataApp.aBInterviewMethodT.EqualsOneOf(E_aIntrvwrMethodT.ByTelephone, E_aIntrvwrMethodT.ByMail, E_aIntrvwrMethodT.Internet))
                {
                    return VisualObservationSurnameCollection.NotCollectedOnBasisOfVisualObservationOrSurname;
                }

                var isCollectedByObservationOrSurname = dataApp.aBSexCollectedByObservationOrSurname;
                return DetermineCollectionFromTriState(isCollectedByObservationOrSurname);
            }
        }

        [DependsOn(nameof(sHmdaActionTakenT), nameof(sHmdaCoApplicantSourceT), nameof(TryGetCoApplicantApp), nameof(IsCoApplicantCoBorr),
            nameof(CAppBase.aCSexCollectedByObservationOrSurname), nameof(CAppBase.aBSexCollectedByObservationOrSurname), nameof(CAppBase.aCInterviewMethodT), nameof(CAppBase.aBInterviewMethodT))]
        [DevNote("How the Co-Applicants's sex info was collected.")]
        public VisualObservationSurnameCollection sHmdaCSexCollectedByObservationOrSurname
        {
            get
            {
                if (this.sHmdaActionTakenT == HmdaActionTaken.PurchasedLoan)
                {
                    return VisualObservationSurnameCollection.NotApplicable;
                }

                if (this.sHmdaCoApplicantSourceT == sHmdaCoApplicantSourceT.NoCoApplicant)
                {
                    return VisualObservationSurnameCollection.NoCoApplicant;
                }

                CAppBase dataApp;
                if(!this.TryGetCoApplicantApp(out dataApp))
                {
                    return VisualObservationSurnameCollection.Blank;
                }

                var interviewMethod = this.IsCoApplicantCoBorr ? dataApp.aCInterviewMethodT : dataApp.aBInterviewMethodT;
                if (interviewMethod.EqualsOneOf(E_aIntrvwrMethodT.ByTelephone, E_aIntrvwrMethodT.ByMail, E_aIntrvwrMethodT.Internet))
                {
                    return VisualObservationSurnameCollection.NotCollectedOnBasisOfVisualObservationOrSurname;
                }

                var isCollectedByObservationOrSurname = this.IsCoApplicantCoBorr ? dataApp.aCSexCollectedByObservationOrSurname : dataApp.aBSexCollectedByObservationOrSurname;
                return DetermineCollectionFromTriState(isCollectedByObservationOrSurname);
            }
        }

        private VisualObservationSurnameCollection DetermineCollectionFromTriState(E_TriState triState)
        {
            return triState == E_TriState.Yes ? VisualObservationSurnameCollection.CollectedOnBasisOfVisualObservationOrSurname :
                                triState == E_TriState.No ? VisualObservationSurnameCollection.NotCollectedOnBasisOfVisualObservationOrSurname :
                                VisualObservationSurnameCollection.Blank;
        }

        public List<AusResult> reportableAusResults;
        [DependsOn(nameof(CalculateAusData))]
        [DevNote("Null stands for a not-applicable result.")]
        private List<AusResult> sHmdaReportableAusResults
        {
            get
            {
                if (reportableAusResults == null)
                {
                    reportableAusResults = CalculateAusData().ToList();
                }

                return reportableAusResults;
            }
        }

        private Dictionary<UnderwritingService, AUSType> underwritingServiceToAusTypeMap = new Dictionary<UnderwritingService, AUSType>() {
            { UnderwritingService.DU, AUSType.DesktopUnderwriter },
            { UnderwritingService.GUS, AUSType.GUS },
            { UnderwritingService.LP, AUSType.LoanProspector },
            { UnderwritingService.Other, AUSType.Other },
            { UnderwritingService.TOTAL, AUSType.TotalScorecard }
        };

        private AUSType MapUnderwritingServiceToAusType(UnderwritingService serviceType)
        {
            AUSType ausType;
            if (underwritingServiceToAusTypeMap.TryGetValue(serviceType, out ausType))
            {
                return ausType;
            }

            throw new UnhandledEnumException(serviceType);
        }

        [DependsOn(nameof(sHmdaActionTakenT), nameof(sCreditDecisionD), nameof(sBrokerId), nameof(sLT), nameof(sReportFhaGseAusRunsAsTotalRun), nameof(sHmdaPurchaser2015T), nameof(sHmdaReportLatestAusRunOnly))]
        private IEnumerable<AusResult> CalculateAusData()
        {
            var reportLatestAusRunOnly = this.sHmdaReportLatestAusRunOnly;

            if (!reportLatestAusRunOnly && 
                (this.sHmdaActionTakenT == HmdaActionTaken.PurchasedLoan ||
                !this.sCreditDecisionD.IsValid))
            {
                return new List<AusResult>() { null };
            }

            var ausOrders = AusResultHandler.RetrieveOrdersForLoan(this.sLId, this.sBrokerId);
            if (ausOrders.Count == 0)
            {
                return new List<AusResult>() { null };
            }

            if (reportLatestAusRunOnly)
            {
                // OPM 478526
                // Pull the latest order without the loan type or purchaser checks.
                // We can ignore these checks because loans where sHmdaReportLatestAusRunOnly
                // returns true were completed early in the process before being considered
                // for purchase. 
                return ausOrders.OrderByDescending(order => order.Timestamp).Take(1);
            }

            var loanTypeMatchedRuns = this.sLT == E_sLT.FHA ? ausOrders.Where(order => order.ReportAs(this.sReportFhaGseAusRunsAsTotalRun) == UnderwritingService.TOTAL) :
                this.sLT == E_sLT.UsdaRural ? ausOrders.Where(order => order.UnderwritingService == UnderwritingService.GUS) :
                null;
            if (loanTypeMatchedRuns != null && loanTypeMatchedRuns.Count() == 1)
            {
                return loanTypeMatchedRuns.Take(1);
            }

            var purchaserMatchedRuns = this.sHmdaPurchaser2015T == HmdaPurchaser2015T.FannieMae ? ausOrders.Where(order => order.UnderwritingService == UnderwritingService.DU) :
                this.sHmdaPurchaser2015T == HmdaPurchaser2015T.FreddieMac ? ausOrders.Where(order => order.UnderwritingService == UnderwritingService.LP) :
                null;
            if (purchaserMatchedRuns != null && purchaserMatchedRuns.Count() == 1)
            {
                return purchaserMatchedRuns.Take(1);
            }

            ausOrders = ausOrders.OrderBy(order => Math.Abs(order.Timestamp.Ticks - this.sCreditDecisionD.DateTimeForComputationWithTime.Ticks)).ToList();
            var closestResult = ausOrders.First();
            return ausOrders.Where(order => order.Timestamp == closestResult.Timestamp);
        }

        private Dictionary<DuRecommendation, AUSResult> DuResultToAUSResult = new Dictionary<DuRecommendation, AUSResult>()
        {
            {DuRecommendation.Approve_Eligible, AUSResult.ApproveEligible },
            {DuRecommendation.Approve_Ineligible, AUSResult.ApproveIneligible },
            {DuRecommendation.Refer_Eligible, AUSResult.ReferEligible },
            {DuRecommendation.Refer_Ineligible, AUSResult.ReferIneligible },
            {DuRecommendation.ReferWithCaution, AUSResult.ReferCaution },
            {DuRecommendation.OutOfScope, AUSResult.OutOfScope },
            {DuRecommendation.Error, AUSResult.Error },
            {DuRecommendation.Unknown, AUSResult.UnableToDetermine }
        };


        private Dictionary<LpStatus, AUSResult> LpStatusToAUSResult = new Dictionary<LpStatus, AUSResult>()
        {
            {LpStatus.Ineligible, AUSResult.Ineligible },
            {LpStatus.Incomplete, AUSResult.Incomplete },
            {LpStatus.Invalid, AUSResult.Invalid },
        };

        private Dictionary<LpRiskClass, AUSResult> LpRiskClassToAusResult = new Dictionary<LpRiskClass, AUSResult>()
        {
            {LpRiskClass.Accept, AUSResult.Accept },
            {LpRiskClass.Caution, AUSResult.Caution },
            {LpRiskClass.Refer, AUSResult.Refer }
        };

        private Dictionary<TotalRiskClass, AUSResult> TotalResultToAusResult = new Dictionary<TotalRiskClass, AUSResult>()
        {
            { TotalRiskClass.Accept, AUSResult.Accept },
            { TotalRiskClass.Refer, AUSResult.Refer }
        };

        private Dictionary<GusRecommendation, AUSResult> GusResultToAusResult = new Dictionary<GusRecommendation, AUSResult>()
        {
            {GusRecommendation.ReferWithCaution, AUSResult.ReferCaution },
            {GusRecommendation.Accept, AUSResult.Accept },
            {GusRecommendation.Ineligible, AUSResult.Ineligible },
            {GusRecommendation.Refer, AUSResult.Refer },
            {GusRecommendation.UnableToDetermine, AUSResult.UnableToDetermine },
        };

        [DependsOn(nameof(sReportFhaGseAusRunsAsTotalRun))]
        private AUSResult CalculateAusResult(AusResult ausResult)
        {
            if(ausResult == null)
            {
                return AUSResult.Blank;
            }

            AUSResult resultType = AUSResult.Blank;
            switch(ausResult.UnderwritingService)
            {
                case UnderwritingService.DU:
                    if (ausResult.ReportAs(this.sReportFhaGseAusRunsAsTotalRun) == UnderwritingService.TOTAL 
                        && (ausResult.DuRecommendation == DuRecommendation.Refer_Eligible
                        || ausResult.DuRecommendation == DuRecommendation.Refer_Ineligible
                        || ausResult.DuRecommendation == DuRecommendation.ReferWithCaution
                        || ausResult.DuRecommendation == DuRecommendation.Refer))
                    {
                        resultType = AUSResult.Refer;
                        break;
                    }
                    if (ausResult.ReportAs(this.sReportFhaGseAusRunsAsTotalRun) == UnderwritingService.TOTAL
                        && (ausResult.DuRecommendation == DuRecommendation.Approve_Eligible
                        || ausResult.DuRecommendation == DuRecommendation.Approve_Ineligible
                        || ausResult.DuRecommendation == DuRecommendation.Approve))
                    {
                        resultType = AUSResult.Accept;
                        break;
                    }
                    this.DuResultToAUSResult.TryGetValue(ausResult.DuRecommendation, out resultType);
                    break;
                case UnderwritingService.LP:
                    if (ausResult.ReportAs(this.sReportFhaGseAusRunsAsTotalRun) == UnderwritingService.TOTAL
                        || !this.LpStatusToAUSResult.TryGetValue(ausResult.LpStatus, out resultType))
                    {
                        this.LpRiskClassToAusResult.TryGetValue(ausResult.LpRiskClass, out resultType);
                    }
                    break;
                case UnderwritingService.TOTAL:
                    this.TotalResultToAusResult.TryGetValue(ausResult.TotalRiskClass, out resultType);
                    break;
                case UnderwritingService.GUS:
                    if(ausResult.GusRiskEvaluation == GusRiskEvaluation.Eligible)
                    {
                        return AUSResult.Eligible;
                    }

                    this.GusResultToAusResult.TryGetValue(ausResult.GusRecommendation, out resultType);
                    break;
                case UnderwritingService.Other:
                    resultType = AUSResult.Other;
                    break;
                default:
                    throw new UnhandledEnumException(ausResult.UnderwritingService);
            }

            return resultType;
        }

        [DependsOn(nameof(sHmdaReportableAusResults), nameof(sHmdaAutomatedUnderwritingSystem1T), nameof(CalculateAusResult))]
        public AUSResult RetrieveAusResult(int ausIndex)
        {
            if(ausIndex == 0 && this.sHmdaAutomatedUnderwritingSystem1T == AUSType.NotApplicable)
            {
                return AUSResult.NotApplicable;
            }

            if (this.sHmdaReportableAusResults.Count <= ausIndex)
            {
                return AUSResult.Blank;
            }

            var result = this.sHmdaReportableAusResults.ElementAt(ausIndex);
            return CalculateAusResult(result);
        }

        [DependsOn(nameof(sHmdaReportableAusResults), nameof(sReportFhaGseAusRunsAsTotalRun))]
        private AUSType RetrieveAusType(int ausIndex)
        {
            if(this.sHmdaReportableAusResults.Count <= ausIndex)
            {
                return AUSType.Blank;
            }

            var result = this.sHmdaReportableAusResults.ElementAt(ausIndex);
            return result == null ? AUSType.NotApplicable : this.MapUnderwritingServiceToAusType(result.ReportAs(this.sReportFhaGseAusRunsAsTotalRun));
        }

        [DependsOn(nameof(RetrieveAusType))]
        [DevNote("The first Aus type.")]
        public AUSType sHmdaAutomatedUnderwritingSystem1T
        {
            get
            {
                return this.RetrieveAusType(0);
            }
        }


        [DependsOn(nameof(RetrieveAusType))]
        [DevNote("The second Aus type.")]
        public AUSType sHmdaAutomatedUnderwritingSystem2T
        {
            get
            {
                return this.RetrieveAusType(1);
            }
        }


        [DependsOn(nameof(RetrieveAusType))]
        [DevNote("The third Aus type.")]
        public AUSType sHmdaAutomatedUnderwritingSystem3T
        {
            get
            {
                return this.RetrieveAusType(2);
            }
        }


        [DependsOn(nameof(RetrieveAusType))]
        [DevNote("The fourth Aus type.")]
        public AUSType sHmdaAutomatedUnderwritingSystem4T
        {
            get
            {
                return this.RetrieveAusType(3);
            }
        }


        [DependsOn(nameof(RetrieveAusType))]
        [DevNote("The fifth Aus type.")]
        public AUSType sHmdaAutomatedUnderwritingSystem5T
        {
            get
            {
                return this.RetrieveAusType(4);
            }
        }

        [DependsOn(nameof(sHmdaReportableAusResults))]
        [DevNote("The other Aus type description.")]
        public string sHmdaAutomatedUnderwritingSystemOtherDescription
        {
            get
            {
                var otherResult = sHmdaReportableAusResults.FirstOrDefault(result => result != null && result.UnderwritingService == UnderwritingService.Other);
                return otherResult?.UnderwritingServiceOtherDescription;
            }
        }

        [DependsOn(nameof(RetrieveAusResult))]
        [DevNote("The first AUS result type.")]
        public AUSResult sHmdaAutomatedUnderwritingSystemResult1T
        {
            get
            {
                return this.RetrieveAusResult(0);
            }
        }

        [DependsOn(nameof(RetrieveAusResult))]
        [DevNote("The second AUS result type.")]
        public AUSResult sHmdaAutomatedUnderwritingSystemResult2T
        {
            get
            {
                return this.RetrieveAusResult(1);
            }
        }

        [DependsOn(nameof(RetrieveAusResult))]
        [DevNote("The third AUS result type.")]
        public AUSResult sHmdaAutomatedUnderwritingSystemResult3T
        {
            get
            {
                return this.RetrieveAusResult(2);
            }
        }

        [DependsOn(nameof(RetrieveAusResult))]
        [DevNote("The fourth AUS result type.")]
        public AUSResult sHmdaAutomatedUnderwritingSystemResult4T
        {
            get
            {
                return this.RetrieveAusResult(3);
            }
        }

        [DependsOn(nameof(RetrieveAusResult))]
        [DevNote("The fifth AUS result type.")]
        public AUSResult sHmdaAutomatedUnderwritingSystemResult5T
        {
            get
            {
                return this.RetrieveAusResult(4);
            }
        }

        [DependsOn(nameof(sHmdaReportableAusResults))]
        [DevNote("The AUS result other description.")]
        public string sHmdaAutomatedUnderwritingSystemResultOtherDescription
        {
            get
            {
                var otherResult = sHmdaReportableAusResults.FirstOrDefault(result => result != null && result.UnderwritingService == UnderwritingService.Other);
                return otherResult?.ResultOtherDescription;
            }
        }

        [DependsOn(nameof(sCreditDecisionD), nameof(sLoanVersionT), nameof(sHmdaActionTakenT), nameof(sHmdaActionTaken))]
        private bool sHmdaReportLatestAusRunOnly
        {
            get
            {
                if (this.sCreditDecisionD.IsValid)
                {
                    return false;
                }

                var isAtLeastV22 = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement);
                if (isAtLeastV22)
                {
                    return this.sHmdaActionTakenT.EqualsOneOf(HmdaActionTaken.ApplicationWithdrawnByApplicant, HmdaActionTaken.FileClosedForIncompleteness);
                }

                var actionTaken = this.sHmdaActionTaken;
                return string.Equals(actionTaken, ConstApp.HmdaApplicationWithdrawnByApplicant, StringComparison.OrdinalIgnoreCase) ||
                    string.Equals(actionTaken, ConstApp.HmdaFileClosedForIncompleteness, StringComparison.OrdinalIgnoreCase);
            }
        }

        [DependsOn(nameof(sLTotI), nameof(sReliedOnTotalIncome), nameof(sReliedOnTotalIncomeLckd))]
        [DevNote("Relied on total annual income.")]
        public decimal sReliedOnTotalIncome
        {
            get
            {
                if (this.sReliedOnTotalIncomeLckd)
                {
                    return GetMoneyField("sReliedOnTotalIncome", 0M);
                }

                return this.sLTotI * 12;
            }
            set { SetNullableMoneyField("sReliedOnTotalIncome", value); }
        }

        [DependsOn(nameof(sReliedOnTotalIncome))]
        public string sReliedOnTotalIncome_rep
        {
            get { return ToMoneyString(() => sReliedOnTotalIncome); }
            set { this.sReliedOnTotalIncome = ToMoney(value); }
        }

        [DependsOn]
        [DevNote("Locked field for sReliedOnTotalIncome.")]
        public bool sReliedOnTotalIncomeLckd
        {
            get { return GetBoolField("sReliedOnTotalIncomeLckd", false); }
            set { SetNullableBoolField("sReliedOnTotalIncomeLckd", value); }
        }

        [DependsOn(nameof(sQualBottomR), nameof(sReliedOnDebtRatio), nameof(sReliedOnDebtRatioLckd))]
        [DevNote("Relied on debt-to-income ratio (DTI).")]
        public decimal sReliedOnDebtRatio
        {
            get
            {
                if (this.sReliedOnDebtRatioLckd)
                {
                    return GetRateField("sReliedOnDebtRatio", 0);
                }

                return this.sQualBottomR;
            }
            set { SetNullableRateField("sReliedOnDebtRatio", value); }
        }

        [DependsOn(nameof(sReliedOnDebtRatio))]
        public string sReliedOnDebtRatio_rep
        {
            get { return ToRateString(() => this.sReliedOnDebtRatio); }
            set { this.sReliedOnDebtRatio = ToRate(value); }
        }

        [DependsOn]
        [DevNote("Locked field for sReliedOnDebtRatio.")]
        public bool sReliedOnDebtRatioLckd
        {
            get { return GetBoolField("sReliedOnDebtRatioLckd", false); }
            set { SetNullableBoolField("sReliedOnDebtRatioLckd", value); }
        }

        [DependsOn(nameof(sCltvR), nameof(sReliedOnCombinedLTVRatio), nameof(sReliedOnCombinedLTVRatioLckd))]
        [DevNote("Relied on Combined-Loan_To_Value Ratio (CLTV).")]
        public decimal sReliedOnCombinedLTVRatio
        {
            get
            {
                if (this.sReliedOnCombinedLTVRatioLckd)
                {
                    return GetRateField("sReliedOnCombinedLTVRatio", 0);
                }

                return this.sCltvR;
            }
            set { SetNullableRateField("sReliedOnCombinedLTVRatio", value); }
        }

        [DependsOn(nameof(sReliedOnCombinedLTVRatio))]
        public string sReliedOnCombinedLTVRatio_rep
        {
            get { return ToRateString(() => Tools.LtvRounding(sReliedOnCombinedLTVRatio)); }
            set { this.sReliedOnCombinedLTVRatio = ToRate(value); }
        }

        [DependsOn]
        [DevNote("Locked field for sReliedOnCombinedLTVRatio.")]
        public bool sReliedOnCombinedLTVRatioLckd
        {
            get { return GetBoolField("sReliedOnCombinedLTVRatioLckd", false); }
            set { SetNullableBoolField("sReliedOnCombinedLTVRatioLckd", value); }
        }

        [DependsOn(nameof(sHouseVal), nameof(sReliedOnPropertyValue), nameof(sReliedOnPropertyValueLckd))]
        [DevNote("Relied on Property Value.")]
        public decimal sReliedOnPropertyValue
        {
            get
            {
                if (this.sReliedOnPropertyValueLckd)
                {
                    return GetMoneyField("sReliedOnPropertyValue", 0M);
                }

                return this.sHouseVal;
            }
            set { SetNullableMoneyField("sReliedOnPropertyValue", value); }
        }

        [DependsOn(nameof(sReliedOnPropertyValue))]
        public string sReliedOnPropertyValue_rep
        {
            get { return ToMoneyString(() => sReliedOnPropertyValue); }
            set { this.sReliedOnPropertyValue = ToMoney(value); }
        }
        [DevNote("Locked field for sReliedOnPropertyValue.")]
        [DependsOn]
        public bool sReliedOnPropertyValueLckd
        {
            get { return GetBoolField("sReliedOnPropertyValueLckd", false); }
            set { SetNullableBoolField("sReliedOnPropertyValueLckd", value); }
        }

        [DependsOn(nameof(sReliedOnCreditScoreSourceT), nameof(sReliedOnCreditScore), nameof(sReliedOnCreditScoreModeT), nameof(GetHmdaReportTypeFromReliedOnCreditScoreSourceT),
            nameof(sCreditScoreType1), nameof(sCreditScoreType2), nameof(sCreditScoreType2Soft), nameof(sCreditScoreType3), nameof(CAppBase.aBDecisionCreditScore), nameof(CAppBase.aCDecisionCreditScore))]
        [DevNote("Relied on Credit Score.")]
        public int sReliedOnCreditScore
        {
            get
            {
                switch (this.sReliedOnCreditScoreModeT)
                {
                    case ReliedOnCreditScoreMode.Blank:
                    case ReliedOnCreditScoreMode.ApplicantIndividualScore:
                        return int.MinValue; // this is the sentinel value representing blank
                    case ReliedOnCreditScoreMode.SingleScore:
                        if (this.sReliedOnCreditScoreSourceT == HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Blank])
                        {
                            return int.MinValue; // this is the sentinel value representing blank
                        }
                        else if (this.sReliedOnCreditScoreSourceT == HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Type1])
                        {
                            return this.sCreditScoreType1;
                        }
                        else if (this.sReliedOnCreditScoreSourceT == HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Type2])
                        {
                            return this.sCreditScoreType2;
                        }
                        else if (this.sReliedOnCreditScoreSourceT == HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Type2Soft])
                        {
                            return this.sCreditScoreType2Soft;
                        }
                        else if (this.sReliedOnCreditScoreSourceT == HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Type3])
                        {
                            try
                            {
                                return this.sCreditScoreType3;
                            }
                            catch (CBaseException)
                            {
                                return int.MinValue;
                            }
                        }
                        else if (this.sReliedOnCreditScoreSourceT == HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Other])
                        {
                            return GetCountField("sReliedOnCreditScore", 0);
                        }
                        else // case where relied on credit score comes from an application.
                        {
                            string reliedCreditScoreSource = this.sReliedOnCreditScoreSourceT;

                            if (reliedCreditScoreSource.Length != 38)
                            {
                                return int.MinValue; // this is the sentinel value representing blank
                            }
                            else
                            {
                                sHmdaReportCreditScoreAsT applicantType = GetHmdaReportTypeFromReliedOnCreditScoreSourceT();

                                if (applicantType == sHmdaReportCreditScoreAsT.Blank)
                                {
                                    return int.MinValue; // this is the sentinel value representing blank
                                }

                                Guid appGuid = ToGuid(reliedCreditScoreSource.Substring(0, 36));

                                if (applicantType == sHmdaReportCreditScoreAsT.Borrower)
                                {
                                    foreach (CAppBase app in this.m_dataApps)
                                    {
                                        if (app.aAppId == appGuid)
                                        {
                                            return app.aBDecisionCreditScore;
                                        }
                                    }

                                    return int.MinValue; // this is the sentinel value representing blank
                                }
                                else // coborrower
                                {
                                    foreach (CAppBase app in this.m_dataApps)
                                    {
                                        if (app.aAppId == appGuid)
                                        {
                                            return app.aCDecisionCreditScore;
                                        }
                                    }

                                    return int.MinValue; // this is the sentinel value representing blank
                                }
                            }
                        }
                    default:
                        throw new UnhandledEnumException(this.sReliedOnCreditScoreModeT);
                }
            }
            set { this.SetNullableCountField("sReliedOnCreditScore", (int)value); }
        }

        public string sReliedOnCreditScore_rep
        {
            get { return ToCountStringCreditScore(this.sReliedOnCreditScore, this.IsRunningPricingEngine); }
            set { this.sReliedOnCreditScore = this.ToCount(value); }
        }

        [DependsOn(nameof(sReliedOnCreditScoreSourceT))]
        private sHmdaReportCreditScoreAsT GetHmdaReportTypeFromReliedOnCreditScoreSourceT()
        {
            string reliedCreditScoreSource = this.sReliedOnCreditScoreSourceT;

            if (reliedCreditScoreSource.Length != 38)
            {
                return sHmdaReportCreditScoreAsT.Blank;
            }
            else
            {
                // parse the stored string and see                 
                if (reliedCreditScoreSource[37] == 'b')
                {
                    return sHmdaReportCreditScoreAsT.Borrower;
                }
                else if (reliedCreditScoreSource[37] == 'c')
                {
                    return sHmdaReportCreditScoreAsT.Coborrower;
                }
                else
                {
                    // if we reach here in the code, it's most likely that the application that the credit score used to belong to was deleted.
                    return sHmdaReportCreditScoreAsT.Blank;
                }
            }
        }

        [DependsOn]
        [DevNote("A bit that tells the user whether the income is relied on or not.")]
        public bool sHmdaIsIncomeReliedOn
        {
            get { return GetBoolField("sHmdaIsIncomeReliedOn", true); }
            set { SetNullableBoolField("sHmdaIsIncomeReliedOn", value); }
        }

        [DependsOn]
        [DevNote("A bit that tells the user whether the debt ratio is relied on or not.")]
        public bool sHmdaIsDebtRatioReliedOn
        {
            get { return GetBoolField("sHmdaIsDebtRatioReliedOn", true); }
            set { SetNullableBoolField("sHmdaIsDebtRatioReliedOn", value); }
        }

        [DependsOn]
        [DevNote("A bit that tells the user whether the combined loan to value ratio is relied on or not.")]
        public bool sHmdaIsCombinedRatioReliedOn
        {
            get { return GetBoolField("sHmdaIsCombinedRatioReliedOn", true); }
            set { SetNullableBoolField("sHmdaIsCombinedRatioReliedOn", value); }
        }

        [DependsOn]
        [DevNote("A bit that tells the user whether the property value is relied on or not.")]
        public bool sHmdaIsPropertyValueReliedOn
        {
            get { return GetBoolField("sHmdaIsPropertyValueReliedOn", true); }
            set { SetNullableBoolField("sHmdaIsPropertyValueReliedOn", value); }
        }

        [DependsOn]
        [DevNote("A bit that tells the user whether the credit score is relied on or not.")]
        public bool sHmdaIsCreditScoreReliedOn
        {
            get { return GetBoolField("sHmdaIsCreditScoreReliedOn", true); }
            set { SetNullableBoolField("sHmdaIsCreditScoreReliedOn", value); }
        }

        [DependsOn]
        [DevNote("Relied on credit score mode. Blank, Applicant's Individual Score, or Single Score.")]
        public ReliedOnCreditScoreMode sReliedOnCreditScoreModeT
        {
            get
            {
                return GetTypeIndexField("sReliedOnCreditScoreModeT", ReliedOnCreditScoreMode.Blank);
            }
            set { SetNullableCountField("sReliedOnCreditScoreModeT", (int)value); }
        }

        [DependsOn]
        [DevNote("Source of data for the selected relied on score mode.")]
        public string sReliedOnCreditScoreSourceT
        {
            get
            {
                return this.GetStringVarCharField("sReliedOnCreditScoreSourceT");
            }
            set { SetStringVarCharField("sReliedOnCreditScoreSourceT", value); }
        }

        [DependsOn]
        [DevNote("Relied on HMDA report as Borrower or Coborrower.")]
        public sHmdaReportCreditScoreAsT sReliedOnCreditScoreSingleHmdaReportAsT
        {
            get
            {
                return GetTypeIndexField("sReliedOnCreditScoreSingleHmdaReportAsT", sHmdaReportCreditScoreAsT.Blank);
            }
            set { SetNullableCountField("sReliedOnCreditScoreSingleHmdaReportAsT", (int)value); }
        }

        [DependsOn(nameof(sReliedOnCreditScoreSourceT), nameof(CalculateCreditScoreType1), nameof(CalculateCreditScoreType2), nameof(CalculateCreditScoreType2Soft), nameof(GetHmdaReportTypeFromReliedOnCreditScoreSourceT))]
        public sHmdaReportCreditScoreAsT GetDefaultReliedOnCreditScoreSingleHmdaReportAsT()
        {
            if (this.sReliedOnCreditScoreSourceT == HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Blank])
            {
                return sHmdaReportCreditScoreAsT.Blank;
            }
            else if (this.sReliedOnCreditScoreSourceT == HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Type1])
            {
                try
                {
                    return this.CalculateCreditScoreType1().HmdaReportAsT;
                }
                catch (CBaseException)
                {
                    LogError("Error calculating sCreditScoreType1");
                    return sHmdaReportCreditScoreAsT.Blank;
                }
            }
            else if (this.sReliedOnCreditScoreSourceT == HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Type2])
            {
                sHmdaReportCreditScoreAsT hmdaReportAsT;

                try
                {
                    hmdaReportAsT = this.CalculateCreditScoreType2().HmdaReportAsT;
                }
                catch (CBaseException)
                {
                    LogError("Error calculating sCreditScoreType2");
                    return sHmdaReportCreditScoreAsT.Blank;
                }

                return hmdaReportAsT;
            }
            else if (this.sReliedOnCreditScoreSourceT == HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Type2Soft])
            {
                sHmdaReportCreditScoreAsT hmdaReportAsT; 
                try
                {
                    hmdaReportAsT = this.CalculateCreditScoreType2Soft().HmdaReportAsT;
                }
                catch (CBaseException)
                {
                    LogError("Error calculating sCreditScoreType2Soft");
                    return sHmdaReportCreditScoreAsT.Blank;
                }

                return hmdaReportAsT;                
            }
            else if (this.sReliedOnCreditScoreSourceT == HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Type3] ||
                        this.sReliedOnCreditScoreSourceT == HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Other])
            {
                return sHmdaReportCreditScoreAsT.Borrower;
            }
            else // case where relied on credit score comes from an application.
            {
                string reliedCreditScoreSource = this.sReliedOnCreditScoreSourceT;

                if (reliedCreditScoreSource.Length != 38)
                {
                    return sHmdaReportCreditScoreAsT.Blank;
                }
                else
                {
                    sHmdaReportCreditScoreAsT applicantType = GetHmdaReportTypeFromReliedOnCreditScoreSourceT();

                    if (applicantType == sHmdaReportCreditScoreAsT.Blank)
                    {
                        return applicantType;
                    }

                    Guid appGuid = ToGuid(reliedCreditScoreSource.Substring(0, 36));

                    if (applicantType == sHmdaReportCreditScoreAsT.Borrower)
                    {
                        for (int i = 0; i < m_dataApps.Length; i++)
                        {
                            if (m_dataApps[i].aAppId == appGuid)
                            {
                                // unless you are the primary applicant's borrower, every other borrower/cobborwer is defaulted as coborrower
                                return i == 0 ? sHmdaReportCreditScoreAsT.Borrower : sHmdaReportCreditScoreAsT.Coborrower;
                            }
                        }

                        return sHmdaReportCreditScoreAsT.Blank;
                    }
                    else // coborrower
                    {
                        for (int i = 0; i < m_dataApps.Length; i++)
                        {
                            if (m_dataApps[i].aAppId == appGuid)
                            {
                                return sHmdaReportCreditScoreAsT.Coborrower;
                            }
                        }

                        return sHmdaReportCreditScoreAsT.Blank;
                    }
                }
            }
        }

        [DependsOn(nameof(CAppData.aDecisionCreditSourceT), nameof(CAppData.aExperianModelT), nameof(CAppData.aExperianModelTOtherDescription),
            nameof(CAppData.aTransUnionModelT), nameof(CAppData.aTransUnionModelTOtherDescription), nameof(CAppData.aEquifaxModelT),
            nameof(CAppData.aEquifaxModelTOtherDescription), nameof(CAppData.aOtherCreditModelName), nameof(CAppData.aExperianScore),
            nameof(CAppData.aTransUnionScore), nameof(CAppData.aEquifaxScore))]
        private E_CreditScoreModelT GetCreditModelType(CAppBase app, E_BorrowerModeT borrowerMode, out string otherDescription, bool allowOtherSource = true)
        {
            app.BorrowerModeT = borrowerMode;
            otherDescription = string.Empty;

            var decisionSource = allowOtherSource
                ? app.aDecisionCreditSourceT
                : app.CalculateDecisionCredit(app.aExperianScore, app.aTransUnionScore, app.aEquifaxScore);

            if (decisionSource == E_aDecisionCreditSourceT.Experian)
            {
                if (app.aExperianModelT == E_CreditScoreModelT.Other)
                {
                    otherDescription = app.aExperianModelTOtherDescription;
                }

                return app.aExperianModelT;
            }
            else if (decisionSource == E_aDecisionCreditSourceT.TransUnion)
            {
                if (app.aTransUnionModelT == E_CreditScoreModelT.Other)
                {
                    otherDescription = app.aTransUnionModelTOtherDescription;
                }

                return app.aTransUnionModelT;
            }
            else if (decisionSource == E_aDecisionCreditSourceT.Equifax)
            {
                if (app.aEquifaxModelT == E_CreditScoreModelT.Other)
                {
                    otherDescription = app.aEquifaxModelTOtherDescription;
                }

                return app.aEquifaxModelT;
            }
            else if (decisionSource == E_aDecisionCreditSourceT.Other)
            {
                if (allowOtherSource)
                {
                    otherDescription = app.aOtherCreditModelName;
                    return E_CreditScoreModelT.Other;
                }
            }

            throw new UnhandledEnumException(decisionSource);
        }

        [DependsOn(nameof(CAppData.aDecisionCreditSourceT), nameof(CAppData.aExperianModelName), nameof(CAppData.aTransUnionModelName), nameof(CAppData.aEquifaxModelName), nameof(CAppData.aOtherCreditModelName),
            nameof(CAppData.aExperianScore), nameof(CAppData.aTransUnionScore), nameof(CAppData.aEquifaxScore))]
        private string GetCreditModelName(CAppBase app, E_BorrowerModeT borrowerMode, bool allowOtherSource = true)
        {
            app.BorrowerModeT = borrowerMode;

            var decisionSource = allowOtherSource
                ? app.aDecisionCreditSourceT
                : app.CalculateDecisionCredit(app.aExperianScore, app.aTransUnionScore, app.aEquifaxScore);

            switch (decisionSource)
            {
                case E_aDecisionCreditSourceT.Experian:
                    return app.aExperianModelName;
                case E_aDecisionCreditSourceT.TransUnion:
                    return app.aTransUnionModelName;
                case E_aDecisionCreditSourceT.Equifax:
                    return app.aEquifaxModelName;
                case E_aDecisionCreditSourceT.Other:
                    if (allowOtherSource)
                    {
                        return app.aOtherCreditModelName;
                    }

                    throw new UnhandledEnumException(app.aDecisionCreditSourceT);
                default:
                    throw new UnhandledEnumException(app.aDecisionCreditSourceT);
            }
        }

        [DependsOn]
        [DevNote("Locked field for sReliedOnCreditScoreModelNameLckd.")]
        public bool sReliedOnCreditScoreModelNameLckd
        {
            get { return GetBoolField("sReliedOnCreditScoreModelNameLckd", false); }
            set { SetNullableBoolField("sReliedOnCreditScoreModelNameLckd", value); }
        }

        [DependsOn(nameof(sReliedOnCreditScoreModelT), nameof(sReliedOnCreditScoreModelTLckd), nameof(sfGetReliedOnCreditScoreModelT))]
        [DevNote("Gets the HMDA relied-on credit scoring model")]
        public E_CreditScoreModelT sReliedOnCreditScoreModelT
        {
            get
            {
                if (this.sReliedOnCreditScoreModelTLckd)
                {
                    return GetTypeIndexField(nameof(this.sReliedOnCreditScoreModelT), E_CreditScoreModelT.LeaveBlank);
                }

                string otherDescription;
                return this.GetReliedOnCreditScoreModelT(out otherDescription);
            }

            set { SetNullableCountField(nameof(this.sReliedOnCreditScoreModelT), (int)value); }
        }

        [DependsOn]
        [DevNote("Indicates whether the HMDA relied-on credit scoring model is locked.")]
        public bool sReliedOnCreditScoreModelTLckd
        {
            get { return GetBoolField("sReliedOnCreditScoreModelTLckd", false); }
            set { SetNullableBoolField("sReliedOnCreditScoreModelTLckd", value); }
        }

        [DependsOn(nameof(sReliedOnCreditScoreModelTOtherDescription), nameof(sReliedOnCreditScoreModelTLckd), nameof(sfGetReliedOnCreditScoreModelT))]
        [DevNote("Gets the other description for the HMDA relied-on credit scoring model.")]
        public string sReliedOnCreditScoreModelTOtherDescription
        {
            get
            {
                if (this.sReliedOnCreditScoreModelTLckd)
                {
                    return GetStringVarCharField(nameof(this.sReliedOnCreditScoreModelTOtherDescription));
                }

                string otherDescription;
                this.GetReliedOnCreditScoreModelT(out otherDescription);
                return otherDescription;
            }

            set { SetStringVarCharField(nameof(this.sReliedOnCreditScoreModelTOtherDescription), value); }
        }

        [DependsOn(nameof(sReliedOnCreditScoreModelT), nameof(GetCreditModelType), nameof(sReliedOnCreditScoreSourceT),
            nameof(CalculateCreditScoreType1), nameof(CalculateCreditScoreType2), nameof(CalculateCreditScoreType2Soft),
            nameof(GetHmdaReportTypeFromReliedOnCreditScoreSourceT), nameof(sReliedOnCreditScoreModelTLckd), nameof(sReliedOnCreditScoreModelTOtherDescription))]
        private int sfGetReliedOnCreditScoreModelT { set { } }

        private E_CreditScoreModelT GetReliedOnCreditScoreModelT(out string otherDescription)
        {
            if (this.sReliedOnCreditScoreSourceT == HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Blank])
            {
                otherDescription = string.Empty;
                return E_CreditScoreModelT.LeaveBlank;
            }
            else if (this.sReliedOnCreditScoreSourceT == HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Type1])
            {
                CreditScoreCalculationResult creditScoreSourceType1Info;
                try
                {
                    creditScoreSourceType1Info = this.CalculateCreditScoreType1();
                }
                catch (CBaseException)
                {
                    LogError("Error calculating sCreditScoreType1");
                    otherDescription = string.Empty;
                    return E_CreditScoreModelT.LeaveBlank;
                }

                CAppBase app = this.GetAppData(creditScoreSourceType1Info.ApplicationIndex);

                if (creditScoreSourceType1Info.HmdaReportAsT == sHmdaReportCreditScoreAsT.Borrower)
                {
                    return GetCreditModelType(app, E_BorrowerModeT.Borrower, out otherDescription, allowOtherSource: false);
                }
                else if (creditScoreSourceType1Info.HmdaReportAsT == sHmdaReportCreditScoreAsT.Coborrower)
                {
                    return GetCreditModelType(app, E_BorrowerModeT.Coborrower, out otherDescription, allowOtherSource: false);
                }
                else
                {
                    throw new CBaseException("sCreditScoreType1 calculation resulted in error", "sCreditScoreType1 calculation resulted in error");
                }
            }
            else if (this.sReliedOnCreditScoreSourceT == HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Type2])
            {
                CreditScoreCalculationResult creditScoreSourceType2Info;
                try
                {
                    creditScoreSourceType2Info = this.CalculateCreditScoreType2();
                }
                catch (CBaseException)
                {
                    LogError("Error calculating sCreditScoreType2");
                    otherDescription = string.Empty;
                    return E_CreditScoreModelT.LeaveBlank;
                }

                CAppBase app = this.GetAppData(creditScoreSourceType2Info.ApplicationIndex);

                if (creditScoreSourceType2Info.HmdaReportAsT == sHmdaReportCreditScoreAsT.Borrower)
                {
                    return GetCreditModelType(app, E_BorrowerModeT.Borrower, out otherDescription, allowOtherSource: false);
                }
                else if (creditScoreSourceType2Info.HmdaReportAsT == sHmdaReportCreditScoreAsT.Coborrower)
                {
                    return GetCreditModelType(app, E_BorrowerModeT.Coborrower, out otherDescription, allowOtherSource: false);
                }
                else
                {
                    // credit source does not belong to a specific borrower.
                    otherDescription = string.Empty;
                    return E_CreditScoreModelT.LeaveBlank;
                }
            }
            else if (this.sReliedOnCreditScoreSourceT == HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Type2Soft])
            {
                CreditScoreCalculationResult creditScoreSourceType2SoftInfo;
                try
                {
                    creditScoreSourceType2SoftInfo = this.CalculateCreditScoreType2Soft();
                }
                catch (CBaseException)
                {
                    LogError("Error calculating sCreditScoreType2Soft");
                    otherDescription = string.Empty;
                    return E_CreditScoreModelT.LeaveBlank;
                }

                CAppBase app = this.GetAppData(creditScoreSourceType2SoftInfo.ApplicationIndex);

                if (creditScoreSourceType2SoftInfo.HmdaReportAsT == sHmdaReportCreditScoreAsT.Borrower)
                {
                    return GetCreditModelType(app, E_BorrowerModeT.Borrower, out otherDescription, allowOtherSource: false);
                }
                else if (creditScoreSourceType2SoftInfo.HmdaReportAsT == sHmdaReportCreditScoreAsT.Coborrower)
                {
                    return GetCreditModelType(app, E_BorrowerModeT.Coborrower, out otherDescription, allowOtherSource: false);
                }
                else
                {
                    // credit source does not belong to a specific borrower.
                    otherDescription = string.Empty;
                    return E_CreditScoreModelT.LeaveBlank;
                }
            }
            else if (this.sReliedOnCreditScoreSourceT == HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Type3])
            {
                var primaryApp = this.GetAppData(0);
                return GetCreditModelType(primaryApp, E_BorrowerModeT.Borrower, out otherDescription, allowOtherSource: false);
            }
            else if (this.sReliedOnCreditScoreSourceT == HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Other])
            {
                otherDescription = GetStringVarCharField(nameof(this.sReliedOnCreditScoreModelTOtherDescription));
                return GetTypeIndexField(nameof(this.sReliedOnCreditScoreModelT), E_CreditScoreModelT.LeaveBlank);
            }
            else // case where relied on credit score comes from an application.
            {
                string reliedCreditScoreSource = this.sReliedOnCreditScoreSourceT;

                if (reliedCreditScoreSource.Length != 38)
                {
                    otherDescription = string.Empty;
                    return E_CreditScoreModelT.LeaveBlank;
                }
                else
                {
                    sHmdaReportCreditScoreAsT applicantType = GetHmdaReportTypeFromReliedOnCreditScoreSourceT();

                    if (applicantType == sHmdaReportCreditScoreAsT.Blank)
                    {
                        otherDescription = string.Empty;
                        return E_CreditScoreModelT.LeaveBlank;
                    }

                    Guid appGuid = ToGuid(reliedCreditScoreSource.Substring(0, 36));

                    if (applicantType == sHmdaReportCreditScoreAsT.Borrower)
                    {
                        foreach (CAppBase app in this.m_dataApps)
                        {
                            if (app.aAppId == appGuid)
                            {
                                return GetCreditModelType(app, E_BorrowerModeT.Borrower, out otherDescription);
                            }
                        }

                        otherDescription = string.Empty;
                        return E_CreditScoreModelT.LeaveBlank;
                    }
                    else // coborrower
                    {
                        foreach (CAppBase app in this.m_dataApps)
                        {
                            if (app.aAppId == appGuid)
                            {
                                return GetCreditModelType(app, E_BorrowerModeT.Coborrower, out otherDescription);
                            }
                        }

                        otherDescription = string.Empty;
                        return E_CreditScoreModelT.LeaveBlank;
                    }
                }
            }
        }

        [DependsOn(nameof(sReliedOnCreditScoreModelName), nameof(GetCreditModelName), nameof(sReliedOnCreditScoreSourceT),
            nameof(CalculateCreditScoreType1), nameof(CalculateCreditScoreType2), nameof(CalculateCreditScoreType2Soft), nameof(GetHmdaReportTypeFromReliedOnCreditScoreSourceT), nameof(sReliedOnCreditScoreModelNameLckd))]
        [DevNote("Relied on credit score model name.")]
        public string sReliedOnCreditScoreModelName
        {
            get
            {
                if (this.sReliedOnCreditScoreModelNameLckd)
                {
                    return GetStringVarCharField("sReliedOnCreditScoreModelName");
                }

                if (this.sReliedOnCreditScoreSourceT == HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Blank])
                {
                    return string.Empty;
                }
                else if (this.sReliedOnCreditScoreSourceT == HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Type1])
                {
                    CreditScoreCalculationResult creditScoreSourceType1Info;
                    try
                    {
                        creditScoreSourceType1Info = this.CalculateCreditScoreType1();
                    }
                    catch (CBaseException)
                    {
                        LogError("Error calculating sCreditScoreType1");
                        return string.Empty;
                    }                    

                    CAppBase app = this.GetAppData(creditScoreSourceType1Info.ApplicationIndex);

                    if (creditScoreSourceType1Info.HmdaReportAsT == sHmdaReportCreditScoreAsT.Borrower)
                    {
                        return GetCreditModelName(app, E_BorrowerModeT.Borrower, allowOtherSource: false);
                    }
                    else if (creditScoreSourceType1Info.HmdaReportAsT == sHmdaReportCreditScoreAsT.Coborrower)
                    {
                        return GetCreditModelName(app, E_BorrowerModeT.Coborrower, allowOtherSource: false);
                    }
                    else
                    {
                        throw new CBaseException("sCreditScoreType1 calculation resulted in error", "sCreditScoreType1 calculation resulted in error");
                    }
                }
                else if (this.sReliedOnCreditScoreSourceT == HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Type2])
                {
                    CreditScoreCalculationResult creditScoreSourceType2Info;
                    try
                    {
                        creditScoreSourceType2Info = this.CalculateCreditScoreType2();
                    }
                    catch (CBaseException)
                    {
                        LogError("Error calculating sCreditScoreType2");
                        return string.Empty;
                    }

                    CAppBase app = this.GetAppData(creditScoreSourceType2Info.ApplicationIndex);

                    if (creditScoreSourceType2Info.HmdaReportAsT == sHmdaReportCreditScoreAsT.Borrower)
                    {
                        return GetCreditModelName(app, E_BorrowerModeT.Borrower, allowOtherSource: false);
                    }
                    else if (creditScoreSourceType2Info.HmdaReportAsT == sHmdaReportCreditScoreAsT.Coborrower)
                    {
                        return GetCreditModelName(app, E_BorrowerModeT.Coborrower, allowOtherSource: false);
                    }
                    else
                    {
                        // credit source does not belong to a specific borrower.
                        return string.Empty;
                    }
                }
                else if (this.sReliedOnCreditScoreSourceT == HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Type2Soft])
                {
                    CreditScoreCalculationResult creditScoreSourceType2SoftInfo;
                    try
                    {
                        creditScoreSourceType2SoftInfo = this.CalculateCreditScoreType2Soft();
                    }
                    catch (CBaseException)
                    {
                        LogError("Error calculating sCreditScoreType2Soft");
                        return string.Empty;
                    }

                    CAppBase app = this.GetAppData(creditScoreSourceType2SoftInfo.ApplicationIndex);

                    if (creditScoreSourceType2SoftInfo.HmdaReportAsT == sHmdaReportCreditScoreAsT.Borrower)
                    {
                        return GetCreditModelName(app, E_BorrowerModeT.Borrower, allowOtherSource: false);
                    }
                    else if (creditScoreSourceType2SoftInfo.HmdaReportAsT == sHmdaReportCreditScoreAsT.Coborrower)
                    {
                        return GetCreditModelName(app, E_BorrowerModeT.Coborrower, allowOtherSource: false);
                    }
                    else
                    {
                        // credit source does not belong to a specific borrower.
                        return string.Empty;
                    }
                }
                else if (this.sReliedOnCreditScoreSourceT == HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Type3])
                {
                    var primaryApp = this.GetAppData(0);
                    return GetCreditModelName(primaryApp, E_BorrowerModeT.Borrower, allowOtherSource: false);
                }
                else if (this.sReliedOnCreditScoreSourceT == HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Other])
                {
                    return GetStringVarCharField("sReliedOnCreditScoreModelName");
                }
                else // case where relied on credit score comes from an application.
                {
                    string reliedCreditScoreSource = this.sReliedOnCreditScoreSourceT;

                    if (reliedCreditScoreSource.Length != 38)
                    {
                        return string.Empty;
                    }
                    else
                    {
                        sHmdaReportCreditScoreAsT applicantType = GetHmdaReportTypeFromReliedOnCreditScoreSourceT();

                        if (applicantType == sHmdaReportCreditScoreAsT.Blank)
                        {
                            return string.Empty;
                        }

                        Guid appGuid = ToGuid(reliedCreditScoreSource.Substring(0, 36));

                        if (applicantType == sHmdaReportCreditScoreAsT.Borrower)
                        {
                            foreach (CAppBase app in this.m_dataApps)
                            {
                                if (app.aAppId == appGuid)
                                {
                                    return GetCreditModelName(app, E_BorrowerModeT.Borrower);
                                }
                            }

                            return string.Empty;
                        }
                        else // coborrower
                        {
                            foreach (CAppBase app in this.m_dataApps)
                            {
                                if (app.aAppId == appGuid)
                                {
                                    return GetCreditModelName(app, E_BorrowerModeT.Coborrower);
                                }
                            }

                            return string.Empty;
                        }
                    }
                }
            }
            set { this.SetStringVarCharField(nameof(sReliedOnCreditScoreModelName), value); }
        }
    }
}
