using System;
using System.Collections;
using System.Reflection;
using System.ComponentModel;

namespace DataAccess
{
    [AttributeUsage(AttributeTargets.All)]
    public class OrderedDescriptionAttribute : DescriptionAttribute
    {
        public int Order { get; set; }

        public OrderedDescriptionAttribute(string description) : base(description)
        {
        }

    }

    public enum VorSpecialRecordType
    {
        BorrowerPresent = 0,
        BorrowerPrevious1 = 1,
        BorrowerPrevious2 = 2,
        CoborrowerPresent = 3,
        CoborrowerPrevious1 = 4,
        CoborrowerPrevious2 = 5
    }

    public enum PointCalculation1098T
    {
        SetManually = 0,
        DiscountPoints = 1
    }

    public enum SecondFinancingInclusionT
    {
        None = 0,
        New = 1,
        Existing = 2
    }

    public enum LoanRescindableT
    {
        Blank = 0,
        Rescindable = 1,
        NotRescindable = 2
    }

    public enum PricingResultsType
    {
        Current = 0,
        Historical = 1,
        WorstCase = 2
    }

    public enum RateAdjFloorCalcT
    {
        /// <summary>
        /// Set manually.
        /// </summary>
        SetManually = 0,

        /// <summary>
        /// Margin + Fixed Percent.
        /// </summary>
        [OrderedDescription("Margin + Fixed Percent")]
        MarginFixedPercent = 1,

        /// <summary>
        /// Start Rate + Fixed Percent.
        /// </summary>
        [OrderedDescription("Start Rate + Fixed Percent")]
        StartRateFixedPercent = 2,

        /// <summary>
        /// 0% + Fixed Percent.
        /// </summary>
        [OrderedDescription("0% + Fixed Percent")]
        ZeroFixedPercent = 3
    }

    public enum AccPeriodMonEnum
    {
        Blank = 0,
        January = 1,
        February = 2,
        March = 3,
        April = 4,
        May = 5,
        June = 6,
        July = 7,
        August = 8,
        September = 9,
        October = 10,
        November = 11,
        December = 12,
    }

    public enum AccPeriodYrEnum
    {
        [OrderedDescription("2001")]
        Year2001 = 2001,

        [OrderedDescription("2002")]
        Year2002 = 2002,

        [OrderedDescription("2003")]
        Year2003 = 2003,

        [OrderedDescription("2004")]
        Year2004 = 2004,

        [OrderedDescription("2005")]
        Year2005 = 2005,

        [OrderedDescription("2006")]
        Year2006 = 2006,

        [OrderedDescription("2007")]
        Year2007 = 2007,

        [OrderedDescription("2008")]
        Year2008 = 2008,

        [OrderedDescription("2009")]
        Year2009 = 2009,

        [OrderedDescription("2010")]
        Year2010 = 2010,
    }


    public enum E_sClosingCostSetType
    {
        /// <summary>
        /// Normal GFE Fees
        /// </summary>
        [OrderedDescription("Description for Fees", Order = 3)]
        Fees = 0,

        /// <summary>
        /// The fee setup for lender
        /// </summary>
        [OrderedDescription("Description for LenderFeeSetup", Order = 2)]
        LenderFeeSetup = 1,

        /// <summary>
        /// Seller Responsible fees.
        /// </summary>
        [OrderedDescription("Description for SellerResponsibleFees", Order = 1)]
        SellerResponsibleFees = 2,

        /// <summary>
        /// Both Borrower and Seller Responsible Fees
        /// </summary>
        [OrderedDescription("Description for BorrowerAndSellerFees", Order = 0)]
        BorrowerAndSellerFees = 3,
    }

    public enum E_CashToCloseCalcMethodT
    {
        Standard = 0,
        Alternative = 1
    }

    public enum E_sTRIDNegativeAmortizationT
    {
        Scheduled = 0,
        Potential = 1,
        NoNegativeAmortization = 2
    }

    public enum E_sTRIDPartialPaymentAcceptanceT
    {
        NotAccepted = 0,
        AcceptedAndApplied = 1,
        AcceptedButNotApplied = 2
    }

    public enum E_sNonMIHousingExpensesEscrowedReasonT
    {
        Unknown = 0,
        LenderRequired = 1,
        BorrowerRequested = 2
    }

    public enum E_sNonMIHousingExpensesNotEscrowedReasonT
    {

        Unknown = 0,
        BorrowerDeclined = 1,
        LenderDidNotOffer = 2,
    }

    public enum E_sPIAdjustmentCause
    {
        GraduatedPayment = 0,
        ARM = 1,
        IO = 2,
        TempBuydown = 3,
        None = 4,

    }

    public enum E_AmortizationScheduleT
    {
        Standard = 0,
        WorstCase = 1,
        BestCase = 2
    }

    public enum E_sToleranceCureCalculationT
    {
        Exclude = 0,
        Include = 1
    }

    public enum E_sLenderCreditCalculationMethodT
    {
        [OrderedDescription("Set Manually")]
        SetManually = 0,
        [OrderedDescription("Calculate from Front-end Rate Lock")]
        CalculateFromFrontEndRateLock = 1
    }

    public enum E_sLenderCreditMaxT
    {
        TotalClosingCosts = 0,
        OriginationCharges = 1,
        NoCredit = 2,
        NoLimit = 3,
        Manual = 4
    }

    public enum E_LenderCreditDiscloseLocationT
    {
        [OrderedDescription("Initial/GFE")]
        InitialGfe = 0,
        [OrderedDescription("Closing/HUD-1")]
        ClosingHud1 = 1
    }

    public enum E_sClosingCostFeeVersionT
    {
        /// <summary>
        /// For legacy loan file that use GFE fields from database field.
        /// </summary>
        Legacy = 0,

        /// <summary>
        /// For legacy loan file that already migrate GFE fields from database to 2015 ClosingCostFee class.
        /// However the loan cannot add/remove fee from closing cost fee. This is to ensure loan can interchange between old GFE fee fields and new structure.
        /// </summary>
        LegacyButMigrated = 1,

        /// <summary>
        /// For new loan that complete on the 2015 ClosingCostFee class.
        /// </summary>
        ClosingCostFee2015 = 2
    }

    public enum E_sDisclosureRegulationT
    {
        Blank = 0,
        GFE = 1,
        TRID = 2
    }

    /// <summary>
    /// Indicates the target GSE application type for the loan file.
    /// </summary>
    public enum GseTargetApplicationT
    {
        /// <summary>
        /// No target form specified.
        /// </summary>
        Blank = 0,

        /// <summary>
        /// The legacy 1003 form is targeted.
        /// </summary>
        Legacy = 1,

        /// <summary>
        /// The new ULAD/URLA form is targeted.
        /// </summary>
        [Description("ULAD 2019")]
        Ulad2019 = 2
    }

    /// <summary>
    /// Indicates which APR calculation method is used for this loan.
    /// </summary>
    public enum E_sAprCalculationT
    {
        /// <summary>
        /// Using the Actuarial APR calculation method, but disregarding irregularity
        /// in the first payment period.
        /// </summary>
        Actuarial_IgnoreIrregularFirstPeriod = 0,

        /// <summary>
        /// Using the Actuarial APR calculation method and accounting for irregularity
        /// in the first payment period.
        /// </summary>
        Actuarial_AccountForIrregularFirstPeriod = 1
    }

    public enum E_sTRIDLoanEstimateSetLockStatusMethodT
    {
        PopulateFromFrontEndRateLock = 0,
        SetManually = 1
    }

    public enum E_sLoanFileT
    {
        Loan = 0,
        Sandbox = 1,
        QuickPricer2Sandboxed = 2, // opm 185732
        Test = 3 // opm 217783
    }

    public enum E_sCustomPricingPolicySourceT
    {
        NotSet = 0,
        Manual = 1,
        OriginatingCompany = 2,
        LoanOfficer = 3,
        Branch = 4
    };

    public enum E_PermissionType : byte
    {
        HardEnable = 0,
        DeferToMoreGranularLevel = 1, // e.g. Stage defers to Broker level.
        HardDisable = 2, // Default
    }

    public enum E_FirstAmericanFeeImportSource
    {
        Normal = 0,
        Buyer = 1,
        Seller = 2
    }

    public enum E_CurrentClosingCostSourceT
    {
        GFE = 0,
        SettlementCharges = 1
    }
    public enum E_LastDisclosedClosingCostSourceT
    {
        GFEArchive = 0,
        GFE = 1,
        None = 2
    }
    public enum E_MIVendorBrokerSettingStatusT
    {
        Disabled = 0,
        Test = 1,
        Production = 2
    }
    public enum E_sQMStatusT
    {
        Blank = 0,
        Ineligible = 1,
        ProvisionallyEligible = 2,
        Eligible = 3
        // 1/13/2014 gf - If adding any values here, please update s_sQMStatusT_Map in CPageBase (LoanFileFields_QM).
    }
    public enum E_sQMLoanPurchaseAgency
    {
        Blank = 0,

        FHA = 1,

        VA = 2,

        USDA = 3,

        FannieMae = 4,

        FreddieMac = 5
        // 1/13/2014 gf - If adding any values here, please update s_sQMLoanPurchaseAgency_Map in CPageBase (LoanFileFields_QM).
    }

    public enum E_OriginatorCompPointSetting
    {
        UsePoint = 0,
        UsePointLessOriginatorComp = 1,
        UsePointWithOriginatorComp = 2,
    }
    public enum E_sInitialRateT
    {
        LeaveBlank = 0,
        NoDiscount = 1,
        FixedRate = 2,
        DiscountToMargin = 3
    }

    public enum E_sExecutionLocationSourceT
    {
        LeaveBlank = 0,
        PopulateFromContact = 1,
        SubjectPropertyAddress = 2,
        SetManually = 3
    }

    public enum E_sTxSecurityInstrumentPar27T
    {
        None = 0,
        PurchaseMoney = 1,
        OweltyOfPartition = 2,
        RenewalAndExtensionOfLiens = 3
    }

    public enum E_sNyPropertyStatementT
    {

        RealPropertyImprovementsNotCoveredIndicator = 0,
        RealPropertyImprovedOrToBeImprovedIndicator = 1,
        MultipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicator = 2

    }
    public enum E_sHelocPmtBaseT
    {
        LineAmount = 0,
        InitialDraw = 1
    }
    public enum E_sHelocPmtPcBaseT
    {
        QualRate = 0,
        NoteRate = 1,
        FixedPercent = 2
    }

    public enum E_sHelocPmtFormulaT
    {
        InterestOnly = 0,
        Amortizing = 1,
        FixedPercent = 2
    }

    public enum E_sHelocPmtFormulaRateT
    {
        QualRate = 0,
        NoteRate = 1
    }

    public enum E_sHelocPmtAmortTermT
    {
        DrawMonths = 0,
        RepayMonths = 1,
        TermMonths = 2,
        DueMonths = 3
    }

    public enum E_sSubFinT
    {
        CloseEnd = 0,
        Heloc = 1
    }

    public enum E_sComplianceEaseStatusT // opm 104590
    {
        Undefined = 0, // blank in the db
        Critical = 1,
        Elevated = 2,   // not on dev
        Errors = 3,
        Minimal = 4,    // not on dev
        Moderate = 5,
        Significant = 6, // not on dev
        Updating = 7,
        Pass = 8, // ComplianceEagle
        Fail = 9, // ComplianceEagle
        FailException = 10, // ComplianceEagle
        Alert = 11, // ComplianceEagle
        Warning = 12, // ComplianceEagle
        Review = 13, // ComplianceEagle
        UserInput = 14, // ComplianceEagle
        Delayed = 15 // ComplianceEagle
    }

    public enum E_EscrowItemT
    {
        RealEstateTax = 0,
        HazardInsurance = 1,
        MortgageInsurance = 2,
        FloodInsurance = 3,
        SchoolTax = 4,
        UserDefine1 = 5,
        UserDefine2 = 6,
        UserDefine3 = 7,
        UserDefine4 = 8
    }

    public enum E_sSpProjectStatusT
    {
        LeaveBlank = 0,
        Existing = 1,
        New = 2
    }

    public enum E_sSpProjectAttachmentT
    {
        LeaveBlank = 0,
        Attached = 1,
        Detached = 2
    }


    public enum E_sSpProjectDesignT
    {
        LeaveBlank = 0,

        [OrderedDescription("Townhouse / Rowhouse")]
        Townhouse = 1,

        [OrderedDescription("Low-rise / Garden project")]
        LowRise = 2,

        [OrderedDescription("Mid-rise project")]
        MidRise = 3,

        [OrderedDescription("High-rise project")]
        HighRise = 4,

        Other = 5
    }

    public enum E_sSpValuationMethodT
    {
        LeaveBlank = 0,

        AutomatedValuationModel = 1,

        DesktopAppraisal = 2,

        DriveBy = 3,

        FullAppraisal = 4,

        PriorAppraisalUsed = 5,

        None = 6,

        FieldReview = 7,

        Other = 8,

        DeskReview = 9,
    }

    public enum E_sSpAvmModelT
    {
        LeaveBlank = 0,
        AutomatedPropertyService = 1,
        Casa = 2,
        FidelityHansen = 3,
        HomePriceAnalyzer = 4,
        HomePriceIndex = 5,
        HomeValueExplorer = 6,
        Indicator = 7,
        MTM = 8,
        NetValue = 9,
        Pass = 10,
        PropertySurveyAnalysisReport = 11,
        ValueFinder = 12,
        ValuePoint = 13,
        ValuePoint4 = 14,
        ValuePointPlus = 15,
        ValueSure = 16,
        ValueWizard = 17,
        ValueWizardPlus = 18,
        VeroIndexPlus = 19,
        VeroValue = 20
    }
    public enum E_sSpGseRefiProgramT
    {
        [OrderedDescription("None / Not Applicable")]
        None = 0,

        [OrderedDescription("DU Refi Plus")]
        DURefiPlus = 1,

        [OrderedDescription("Refi Plus")]
        RefiPlus = 2,

        [OrderedDescription("Relief Refinance – Open Access")]
        ReliefRefiOpenAccess = 3,

        [OrderedDescription("Relief Refinance – Same Servicer")]
        ReliefRefiSameServicer = 4,

        [OrderedDescription("FHLB Disaster Response")]
        FHLBDisasterResponse = 5
    }

    public enum E_sSpGseCollateralProgramT
    {
        None = 0,
        DURefiPlus = 1,
        PropertyInspectionAlternative = 2,
        PropertyInspectionWaiver = 3,
        PropertyInspectionReportForm2075 = 4,
        PropertyInspectionReportForm2070 = 5
    }
    public enum E_sMiInsuranceT
    {
        [OrderedDescription("")]
        None = 0,
        BorrowerPaid = 1,
        LenderPaid = 2,
        InvestorPaid = 3
    }
    public enum E_sMiReasonForAbsenceT
    {
        NoMiBasedOnOriginalLtv = 0,
        NoMIBasedOnMortgageBeingRefinanced = 1,
        MiCanceledBasedOnCurrentLtv = 2,
        RecourseInLieuOfMI = 3,
        IndemnificationInLieuOfMI = 4,
        NoMIBasedOnInvestorRequirements = 5,
        None = 6
    }
    public enum E_PmiCompanyT
    {
        Genworth = 0,
        MGIC = 1,
        Radian = 2,
        UnitedGuaranty_DoNotUse = 3, // removed 09/14/18 - mf - OPM 473117.
                                     // add back 09/26/18 - dd - OPM 474717 - We need to support historical pricing snapshot where snapshot has old value.
                                     //Kinecta = 4, // removed 12/8/16 - je - OPM 366981.
        Essent = 5,
        //Quicken = 6, // removed 12/8/16 - je - OPM 366981.
        Arch = 7,
        NationalMI = 8,
        MassHousing = 9 // 12/8/16 - je - OPM 368085.
    }
    public enum E_PmiTypeT
    {
        MASTER = 0,
        BorrowerPaidMonthlyPremium = 1,
        BorrowerPaidSinglePremium = 2,
        BorrowerPaidSplitPremium = 3,
        LenderPaidSinglePremium = 4
    }
    public enum E_sMiCompanyNmT
    {
        LeaveBlank = 0,

        CMG = 1,

        Essent = 2,

        Genworth = 3,

        MGIC = 4,

        Radian = 5,

        UnitedGuaranty = 6,

        Amerin = 7,

        CAHLIF = 8,

        [OrderedDescription("CMG Pre-Sep 94")]
        CMGPre94 = 9,

        Commonwealth = 10,

        [OrderedDescription("MD Housing")]
        MDHousing = 11,

        MIF = 12,

        PMI = 13,

        RMIC = 14,

        RMICNC = 15,

        SONYMA = 16,

        Triad = 17,

        Verex = 18,

        WiscMtgAssr = 19,

        FHA = 20,

        VA = 21,

        USDA = 22,

        NationalMI = 23,

        [OrderedDescription("Arch MI")]
        Arch = 24,

        [OrderedDescription("MassHousing")]
        MassHousing = 25
    }

    /// <summary>
    /// Represents the type of request for the MI framework.
    /// </summary>
    public enum E_MiRequestType
    {
        User = 0,
        PricingEngine = 1
    }

    public enum E_sBillingPeriodT
    {
        RealEstateTax = 0,
        HazardInsurance = 1,
        MortgageInsurance = 2,
        FloodInsurance = 3,
        SchoolTaxes = 4,
        UserDefined1 = 5,
        UserDefined2 = 6,
        UserDefined3 = 7,
        UserDefined4 = 8,
    }
    public enum E_sInsPaidByT
    {
        None = 0,
        Lender = 1,
        Borrower = 2,
        HOA = 3
    }
    public enum E_sInsSettlementChargeT
    {
        None = 0,
        Line1008 = 1,
        Line1009 = 2,
        Line1010 = 3,
        Line1011 = 4
    }
    public enum E_10081009
    {
        Line1008 = 0,
        Line1009 = 1
    }
    public enum E_sGseDeliveryRemittanceT
    {
        ActualActual = 0,
        ScheduledActual = 1,
        ScheduledScheduled = 2
    }
    public enum E_sGseDeliveryLoanDeliveryT
    {
        WholeLoan = 0,
        MBS = 1
    }
    public enum E_sGseDeliveryLoanDefaultLossPartyT
    {
        Investor = 0,
        Lender = 1,
        Shared = 2
    }
    public enum E_sGseFreddieMacProductT
    {
        None = 0,
        AMinusMortgage = 1,
        ConstructionConversion = 2,
        DisasterReliefProgram = 3,
        HomePossible97 = 4, // obsolete
        HomePossibleNeighborhoodSolution97 = 5, // obsolete
        Renovation = 6,
        HomePossibleAdvantage = 7,
        AlternativeFullInformation = 8,
        AlternateRequirementsDesktopUnderwriter = 9,
        BuilderOrDeveloperAffiliated = 10,
        CorrAdvantageLoan = 11,
        DecliningBalanceCoOwnershipInitiative = 12,
        DreaMaker = 13,
        EnergyConservation = 14,
        FREOwnedCondoProject = 15,
        HomeOpportunity = 16,
        HomePossibleAdvantageHFA = 17,
        HomePossibleMCM = 18,
        HomePossibleMCMCS = 19,
        HomePossibleMortgage = 20,
        LoansToFacilitateREOSales = 21,
        LongTermStandBy = 22,
        MortgageRevenueBond = 23,
        MortgageRewardsProgram = 24,
        MurabahaMortgage = 25,
        Negotiated97PercentLTVLoanProgram = 26,
        NoFeeMortgagePlus = 27,
        NeighborhoodChampions = 28,
        OptimumMortgageProgram = 29,
        RecourseGuaranteedByThirdParty = 30,
        SolarInitiative = 31,
        ShortTermStandBy = 32,
        HomePossibleHomeReady = 33,
    }
    public enum E_sGseDeliveryREOMarketingPartyT
    {
        Investor = 0,
        Lender = 1
    }
    public enum E_sCommunityLendingDownPaymentSourceT
    {
        LeaveBlank = 0,
        Borrower = 1,
        CommunityNonProfit = 2,
        FederalAgency = 3,
        FHLBAffordableHousingProgram = 4,
        LocalAgency = 5,
        Relative = 6,
        ReligiousNonProfit = 7,
        StateAgency = 8,
        UsdaRuralHousing = 9,
        AggregatedRemainingTypes = 10,
        Employer = 11,
        OriginatingLender = 12,
        Grant = 13
    }
    public enum E_sCommunityLendingDownPaymentT
    {
        LeaveBlank = 0,
        CashOnHand = 1,
        CheckingSavings = 2,
        GiftFunds = 3,
        SecondaryFinancingClosedEnd = 4,
        SecondaryFinancingHELOC = 5,
        SecuredBorrowedFunds = 6,
        SweatEquity = 7,
        UnsecuredBorrowedFunds = 8,
        AggregatedRemainingTypes = 9,
        BridgeLoan = 10,
        LifeInsuranceCashValue = 11,
        LotEquity = 12,
        Grant = 13,
        RentWithOptionToPurchase = 14,
        RetirementFunds = 15,
        SaleOfChattel = 16,
        StocksAndBonds = 17,
        TradeEquity = 18,
        TrustFunds = 19,
        EquityOnSoldProperty = 20,
        EquityOnSubjetProperty = 21,
        ForgivableSecuredLoan = 22
    }
    public enum E_sCommunityLendingClosingCostSourceT
    {
        LeaveBlank = 0,
        Borrower = 1,
        CommunityNonProfit = 2,
        Employer = 13,// 3/11/14 tj 173436 - Make employer a distinct value (formerly 2)
        FederalAgency = 3,
        FHLBAffordableHousingProgram = 4,
        Lender = 5,
        LocalAgency = 6,
        PropertySeller = 7,
        Relative = 8,
        ReligiousNonProfit = 9,
        StateAgency = 10,
        UsdaRuralHousing = 11,
        AggregatedRemainingTypes = 12,
    }
    public enum E_sCommunityLendingClosingCostT
    {
        LeaveBlank = 0,
        BridgeLoan = 1,
        CashOnHand = 2,
        CheckingSavings = 3,
        Contribution = 4,
        CreditCard = 5,
        GiftFunds = 6,
        Grant = 7,
        PremiumFunds = 8,
        SecondaryClosedEnd = 9,
        SecondaryHELOC = 10,
        SecuredLoan = 11,
        SweatEquity = 12,
        UnsecuredBorrowedFunds = 13,
        AggregatedRemainingTypes = 14,
        EquityOnSoldProperty = 15,
        EquityOnSubjectProperty = 16,
        ForgivableSecuredLoan = 17,
        LifeInsuranceCashValue = 18,
        LotEquity = 19,
        RentWithOptionToPurchase = 20,
        RetirementFunds = 21,
        SaleOfChattel = 22,
        StocksAndBonds = 23,
        TradeEquity = 24,
        TrustFunds = 25
    }

    public enum E_sApprPmtMethodT
    {
        LeaveBlank = 0,
        COD = 1,
        CreditCard = 2,
        InvoiceClient = 3,
        Bill = 4,
        Other = 5
    }


    public enum E_aHomeOwnershipCounselingSourceT
    {
        LeaveBlank = 0,
        NoBorrowerCounseling = 1,
        GovernmentAgency = 2,
        HUDApprovedCounselingAgency = 3,
        LenderTrainedCounseling = 4,
        MortgageInsuranceCompany = 5,
        NonProfitOrganization = 6,
        BorrowerDidNotParticipate = 7
    }
    public enum E_aHomeOwnershipCounselingFormatT
    {
        LeaveBlank = 0,
        BorrowerEducationNotRequired = 1,
        Classroom = 2,
        HomeStudy = 3,
        Individual = 4,
        BorrowerDidNotParticipate = 5
    }

    public enum E_FannieHomebuyerEducation
    {
        [OrderedDescription("Leave Blank")]
        LeaveBlank = 0,
        [OrderedDescription("HomeBuyer Education complete")]
        EducationComplete = 1,
        [OrderedDescription("One-on-one counseling complete")]
        OneOnOneCounselingComplete = 2
    }

    public enum E_aVaOwnOtherVaLoanT
    {
        LeaveBlank = 0,
        Yes = 1,
        No = 2,
        NA = 3
    }
    public enum E_CreditLenderPaidItemT
    {
        None = 0,
        AllLenderPaidItems = 1,
        OriginatorCompensationOnly = 2
    }

    public enum E_sPriorSalesPropertySellerT
    {
        Blank = 0,
        Private = 1,
        BankREO = 2,
        FNMA = 3,
        FHLMC = 4,
        GNMA = 5,
        HUD = 6,
        VA = 7,
        OtherGovEntity = 8,
        Inheritance = 9,
        USDA = 10,
        Corporation = 11
    }

    public enum E_sFhaLenderIdT
    {
        RegularFhaLender = 0,
        SponsoredOriginatorEIN = 1,
        NoOriginatorId = 2
    }
    public enum E_aUsdaGuaranteeEmployeeRelationshipT
    {
        LeaveBlank = 0,
        Has = 1,
        DoesNot = 2
    }
    public enum E_sUsdaGuaranteeRefinancedLoanT
    {
        LeaveBlank = 0,
        GuaranteedLoan = 1,
        DirectLoan = 2
    }
    public enum E_Encompass360RequestT
    {
        BuysideLock = 0,
        SellsideLock = 1,
        UnderwriteOnly = 2,
        RegisterOrLock = 3,
        QuoteRequest = 4
    }
    public enum E_sOriginatorCompensationLenderFeeOptionT
    {
        IncludedInLenderFees = 0,
        InAdditionToLenderFees = 1
    }
    public enum E_LenderPaidOriginatorCompensationOptionT // OPM 67812
    {
        DoNotAddOriginatorCompensation = 0,     // aka "Included In"
        AddOriginatorCompensationToPricing = 1, // aka "In Addition To"
        UseSettingInLoanFile = 2
    }

    public enum E_sOriginatorCompensationPaymentSourceT
    {
        SourceNotSpecified = 0,
        BorrowerPaid = 1,
        LenderPaid = 2
    }
    public enum E_sOriginatorCompensationPlanT
    {
        Manual = 0,
        FromEmployee = 1
    }
    public enum E_sProdInvestorRLckdModeT
    {
        RateLockPeriod = 0,
        RateLockExpiredDate = 1
    }
    public enum E_sPricingModeT
    {
        Undefined = 0,
        RegularPricing = 1,
        InternalBrokerPricing = 2,
        InternalInvestorPricing = 3,
        EligibilityBrokerPricing = 4,
        EligibilityInvestorPricing = 5
    }
    public enum E_sHasESignedDocumentsT
    {
        Undetermined = 0,
        No = 1,
        Yes = 2
    }

    public enum YesOrNo
    {
        No = 0,
        Yes = 1
    }

    /// <summary>
    /// 3/30/12 altered by case opm 61315 -mp
    /// </summary>
    public enum E_aRelationshipTitleT
    {
        LeaveBlank, // MISMO
        AHusbandAndWife, // MISMO
        AMarriedMan, // MISMO
        AMarriedManAsHisSoleAndSeparateProperty,
        AMarriedManAsToAnUndividedHalfInterest,
        AMarriedPerson, // MISMO
        AMarriedWoman, // MISMO
        AMarriedWomanAsHerSoleAndSeparateProperty,
        AMarriedWomanAsToAnUndividedHalfInterest,
        AnUnmarriedMan, // MISMO
        AnUnmarriedManAsHisSoleAndSeparateProperty,
        AnUnmarriedManAsToAnUndividedHalfInterest,
        AnUnmarriedPerson, // MISMO
        AnUnmarriedWoman, // MISMO
        AnUnmarriedWomanAsHerSoleAndSeparateProperty,
        AnUnmarriedWomanAsToAnUndividedHalfInterest,
        ASingleMan, // MISMO
        ASingleManAsHisSoleAndSeparateProperty,
        ASingleManAsToAnUndividedHalfInterest,
        ASinglePerson, // MISMO
        ASingleWoman, // MISMO
        ASingleWomanAsHerSoleAndSeparateProperty,
        ASingleWomanAsToAnUndividedHalfInterest,
        AWidow, // MISMO
        AWidower, // MISMO
        AWifeAndHusband, // MISMO
        WifeAndHusbandAsCommunityProperty,
        WifeAndHusbandAsJointTenants,
        WifeAndHusbandAsTenantsInCommon,
        HerHusband, // MISMO
        HisWife, // MISMO
        HusbandAndWife, // MISMO
        HusbandAndWifeAsCommunityProperty,
        HusbandAndWifeAsCommunityPropertyWithRightOfSurvivorship,
        HusbandAndWifeAsJointTenants,
        HusbandAndWifeAsJointTenantsWithRightOfSurvivorship,
        HusbandAndWifeAsTenantsInCommon,
        HusbandAndWifeAsTenantsByTheEntirety,
        HusbandAndWifeTenancyByTheEntirety,
        HusbandAndWifeAsToAnUndividedHalfInterest,
        WifeAndHusband, // MISMO
        GeneralPartner,
        DomesticPartners,
        ChiefExecutiveOfficer,
        PersonalGuarantor,
        President,
        Secretary,
        TenancyByEntirety,
        TenantsByTheEntirety,
        Treasurer,
        Trustee,
        VicePresident,
        NotApplicable, // MISMO
        Other // MISMO
    }



    public enum E_aDecisionCreditSourceT
    {
        [OrderedDescription("Experian")]
        Experian = 0,

        [OrderedDescription("TransUnion")]
        TransUnion = 1,

        [OrderedDescription("Equifax")]
        Equifax = 2,

        [OrderedDescription("Other")]
        Other = 3
    }

    public enum E_CreditScoreModelT
    {
        [OrderedDescription("")]
        LeaveBlank = 0,

        [OrderedDescription("Beacon '09 Mortgage Industry Option")]
        Beacon09MortgageIndustryOption = 1,

        [OrderedDescription("Equifax Bankruptcy Navigator Index 02871")]
        EquifaxBankruptcyNavigatorIndex02781 = 2,

        [OrderedDescription("Equifax Bankruptcy Navigator Index 02782")]
        EquifaxBankruptcyNavigatorIndex02782 = 3,

        [OrderedDescription("Equifax Bankruptcy Navigator Index 02783")]
        EquifaxBankruptcyNavigatorIndex02783 = 4,

        [OrderedDescription("Equifax Bankruptcy Navigator Index 02784")]
        EquifaxBankruptcyNavigatorIndex02784 = 5,

        [OrderedDescription("Equifax Beacon")]
        EquifaxBeacon = 6,

        [OrderedDescription("Equifax Beacon 5.0")]
        EquifaxBeacon5 = 7,

        [OrderedDescription("Equifax Beacon 5.0 Auto")]
        EquifaxBeacon5Auto = 8,

        [OrderedDescription("Equifax Beacon 5.0 Bankcard")]
        EquifaxBeacon5BankCard = 9,

        [OrderedDescription("Equifax Beacon 5.0 Installment")]
        EquifaxBeacon5Installment = 10,

        [OrderedDescription("Equifax Beacon 5.0 Personal Finance")]
        EquifaxBeacon5PersonalFinance = 11,

        [OrderedDescription("Equifax Beacon Auto")]
        EquifaxBeaconAuto = 12,

        [OrderedDescription("Equifax Beacon Bankcard")]
        EquifaxBeaconBankcard = 13,

        [OrderedDescription("Equifax Beacon Installment")]
        EquifaxBeaconInstallment = 14,

        [OrderedDescription("Equifax Beacon Personal Finance")]
        EquifaxBeaconPersonalFinance = 15,

        [OrderedDescription("Equifax DAS")]
        EquifaxDAS = 16,

        [OrderedDescription("Equifax Enhanced Beacon")]
        EquifaxEnhancedBeacon = 17,

        [OrderedDescription("Equifax Enhanced DAS")]
        EquifaxEnhancedDAS = 18,

        [OrderedDescription("Equifax Market Max")]
        EquifaxMarketMax = 19,

        [OrderedDescription("Equifax Mortgage Score")]
        EquifaxMortgageScore = 20,

        [OrderedDescription("Equifax Pinnacle")]
        EquifaxPinnacle = 21,

        [OrderedDescription("Equifax Pinnacle 2.0")]
        EquifaxPinnacle2 = 22,

        [OrderedDescription("Equifax Vantage Score")]
        EquifaxVantageScore = 23,

        [OrderedDescription("Equifax Vantage Score 3.0")]
        EquifaxVantageScore3 = 24,

        [OrderedDescription("Experian Fair Isaac")]
        ExperianFairIsaac = 25,

        [OrderedDescription("Experian Fair Isaac Advanced")]
        ExperianFairIsaacAdvanced = 26,

        [OrderedDescription("Experian Fair Isaac Advanced 2.0")]
        ExperianFairIsaacAdvanced2 = 27,

        [OrderedDescription("Experian Fair Isaac Auto")]
        ExperianFairIsaacAuto = 28,

        [OrderedDescription("Experian Fair Isaac Bankcard")]
        ExperianFairIsaacBankcard = 29,

        [OrderedDescription("Experian Fair Isaac Installment")]
        ExperianFairIsaacInstallment = 30,

        [OrderedDescription("Experian Fair Isaac Personal Finance")]
        ExperianFairIsaacPersonalFinance = 31,

        [OrderedDescription("Experian FICO Classic v3")]
        ExperianFICOClassicV3 = 32,

        [OrderedDescription("Experian MDS Bankruptcy II")]
        ExperianMDSBankruptcyII = 33,

        [OrderedDescription("Experian New National Equivalency")]
        ExperianNewNationalEquivalency = 34,

        [OrderedDescription("Experian New National Risk")]
        ExperianNewNationalRisk = 35,

        [OrderedDescription("Experian Old National Risk")]
        ExperianOldNationalRisk = 36,

        [OrderedDescription("Experian Scorex PLUS")]
        ExperianScorexPLUS = 37,

        [OrderedDescription("Experian Vantage Score")]
        ExperianVantageScore = 38,

        [OrderedDescription("Experian Vantage Score 3.0")]
        ExperianVantageScore3 = 39,

        [OrderedDescription("FICO Expansion Score")]
        FICOExpansionScore = 40,

        [OrderedDescription("FICO Risk Score Classic '04")]
        FICORiskScoreClassic04 = 41,

        [OrderedDescription("FICO Risk Score Classic '98")]
        FICORiskScoreClassic98 = 42,

        [OrderedDescription("FICO Risk Score Classic Auto '98")]
        FICORiskScoreClassicAuto98 = 43,

        [OrderedDescription("FICO Risk Score Classic Bankcard '98")]
        FICORiskScoreClassicBankcard98 = 44,

        [OrderedDescription("FICO Risk Score Classic Installment Loan '98")]
        FICORiskScoreClassicInstallmentLoan98 = 45,

        [OrderedDescription("FICO Risk Score Classic Personal Finance '98")]
        FICORiskScoreClassicPersonalFinance98 = 46,

        [OrderedDescription("FICO Risk Score Next Gen '00")]
        FICORiskScoreNextGen00 = 47,

        [OrderedDescription("FICO Risk Score Next Gen '03")]
        FICORiskScoreNextGen03 = 48,

        [OrderedDescription("TransUnion Delphi")]
        TransUnionDelphi = 49,

        [OrderedDescription("TransUnion Empirica")]
        TransUnionEmpirica = 50,

        [OrderedDescription("TransUnion Empirica Auto")]
        TransUnionEmpiricaAuto = 51,

        [OrderedDescription("TransUnion Empirica Bankcard")]
        TransUnionEmpiricaBankcard = 52,

        [OrderedDescription("TransUnion Empirica Installment")]
        TransUnionEmpiricaInstallment = 53,

        [OrderedDescription("TransUnion Empirica Personal Finance")]
        TransUnionEmpiricaPersonalFinance = 54,

        [OrderedDescription("TransUnion New Delphi")]
        TransUnionNewDelphi = 55,

        [OrderedDescription("TransUnion Precision")]
        TransUnionPrecision = 56,

        [OrderedDescription("TransUnion Precision '03")]
        TransUnionPrecision03 = 57,

        [OrderedDescription("TransUnion VantageScore")]
        TransUnionVantageScore = 58,

        [OrderedDescription("TransUnion VantageScore 3.0")]
        TransUnionVantageScore30 = 59,

        [OrderedDescription("VantageScore 2.0")]
        VantageScore2 = 60,

        [OrderedDescription("VantageScore 3.0")]
        VantageScore3 = 61,

        [OrderedDescription("More than one scoring model")]
        MoreThanOneCreditScoringModel = 62,

        [OrderedDescription("Other")]
        Other = 63
    }

    public enum E_ActAsRateLockedT
    {
        NoAffect = 0,
        AllowRead = 1,
        AllowWrite = 2,
    }
    public enum E_FilterStatusDateT
    {
        Any = 0,
        Last7Days = 1,
        Last90Days = 2,
        ThisMonth = 3,
        PreviousMonth = 4,
        Previous2Months = 5,
        Previous3Months = 6,
        YearToDate = 7,
        PreviousYear = 8,
        OlderThan3Months = 9
    }
    public enum E_ScopeT
    {
        CrossBroker = 0,
        InScope = 1,
        NonDuty = 2
    }
    public enum E_aH4HNonOccInterestT
    {
        LeaveBlank = 0,
        NonOccupantCoBorrower = 1,
        NonOccupantCoSigner = 2
    }
    public enum E_sTimeZoneT
    {
        LeaveBlank = 0,
        PacificTime = 1,
        MountainTime = 2,
        CentralTime = 3,
        EasternTime = 4
    }
    public enum E_GFEArchivedReasonT
    {
        NotYetDetermined = 0, // Don't set it to this, we want to track why the archive was created.
                              // A callstack will be logged if gfe archived with this.
        ManuallyArchived = 1,
        DisclosureSentToBorrower = 2,
        ChangeOfCircumstance = 3,
        ClosingCostMigration = 4,
        InitialDislosureDateSetViaHMDA = 5,
        InitialDislosureDateSetViaGFE = 6,
        InitialDislosureDateSetViaLeadConversionLO = 7,
        InitialDislosureDateSetViaLeadConversionPML = 8,
        InitialDisclosureDateSetViaLoanEstimate = 9,
        InitialLoanEstimateGenerated = 10
    }
    public enum E_sFhaCondoApprovalStatusT
    {
        LeaveBlank = 0,
        Approved = 1,
        Rejected = 2,
        Pending = 3,
        Withdrawn = 4
    }
    public enum E_LiabilityReconcileStatusT
    {
        LoanFileOnly = 0,
        Matched = 1,
        CreditReportOnly = 2,
    }
    public enum E_sWADisclosureVersionT
    {
        LeaveBlank = 0,
        Initial = 1,
        Revised = 2
    }
    public enum E_GfeVersion
    {
        Gfe2009 = 0,
        Gfe2010 = 1
    }
    public enum E_GfeSectionT
    {
        LeaveBlank = 0,
        B1 = 1,
        B2 = 2,
        B3 = 3,
        B4 = 4,
        B5 = 5,
        B6 = 6,
        B7 = 7,
        B8 = 8,
        B9 = 9,
        B10 = 10,
        B11 = 11,
        NotApplicable = 12,
    }

    public enum E_GfeSectionTGroups
    {
        NAOnly = 0,
        B1Only = 1,
        B2Only = 2,
        B3Only = 3,
        B4Only = 4,
        B5Only = 5,
        B6Only = 6,
        B7Only = 7,
        B8Only = 8,
        B9Only = 9,
        B10Only = 10,
        B11Only = 11,
        B4andB6 = 12,
        None = 99
    }

    public enum E_IntegratedDisclosureSectionT
    {
        LeaveBlank = 0,
        SectionA = 1,
        SectionB = 2,
        SectionC = 3,
        SectionD = 4,
        SectionE = 5,
        SectionF = 6,
        SectionG = 7,
        SectionH = 8,
        SectionBorC = 9
    }

    public enum E_GfeProviderChoiceT
    {
        LeaveBlank = 0,
        Borrower = 1,
        Lender = 2,
        NotApplicable = 3
    }

    public enum E_GfeResponsiblePartyT
    {
        LeaveBlank = 0,
        Buyer = 1,
        Seller = 2,
        Lender = 3,
        Broker = 4
    }

    public enum E_GfeClosingCostFeePaymentTimingT
    {
        LeaveBlank = 0,
        AtClosing = 1,
        OutsideOfClosing = 2
    }

    public enum E_sTotalScoreLdpOrGsaExclusionT
    {
        Unknown = 0,
        NotExcluded = 1,
        Excluded = 2
    }
    public enum E_sTotalScoreCreditRiskResultT
    {
        Unknown = 0,
        Approve = 1,
        Refer = 2
    }
    public enum E_sTotalScoreRefiT
    {
        LeaveBlank = 0,
        ConventionalToFHA = 1,
        FHAToFHANonStreamline = 2,
        Streamline = 3,
        HOPEForHomeowners = 4,
        Unknown = 5
    }
    public enum E_sTotalScoreFhaProductT
    {
        LeaveBlank = 0,
        Standard = 1,
        Rehabilitation = 2,
        HOPEForHomeowners = 3,
        DisasterVictims = 4,
        Other = 5
    }
    public enum E_aTotalScoreFhtbCounselingT
    {
        [OrderedDescription(null)]
        LeaveBlank = 0,

        [OrderedDescription("HUD Approved Counseling")]
        HudApprovedCounseling = 1,

        [OrderedDescription("Not Counseled")]
        NotCounseled = 2,

        [OrderedDescription("Not Applicable")]
        NotApplicable = 3,
    }
    public enum E_sTotalScoreCurrentMortgageStatusT
    {
        LeaveBlank = 0,

        Current = 1,

        [OrderedDescription("30 day late")]
        Late30 = 2,

        [OrderedDescription("60 day late")]
        Late60 = 3,

        [OrderedDescription("90 day late")]
        Late90 = 4,

        [OrderedDescription("120+ day late")]
        Late120Plus = 5
    }
    public enum E_sGfeCreditChargeT
    {
        CreditChargeIncludeInOrigination = 0,
        ReceiveCredit = 1,
        PayCharge = 2
    }
    public enum E_sRateLockStatusT
    {
        NotLocked = 0,
        Locked = 1,
        LockRequested = 2,
        LockSuspended = 3
    }
    public enum E_sInvestorLockRateLockStatusT
    {
        NotLocked = 0,
        Locked = 1,
        LockSuspended = 2
    }
    public enum E_DataSetT
    {
        AgentXml,
        LiabilityXml,
        PreparerXml,
    }
    public enum E_sFreddieConstructionT
    {
        LeaveBlank = 0,
        NewlyBuilt = 1,
        ConstructionConversion = 2
    }
    public enum E_sProd3rdPartyUwResultT
    {
        NA = 0,
        DU_ApproveEligible = 1,
        DU_ApproveIneligible = 2,
        DU_ReferEligible = 3,
        DU_ReferIneligible = 4,
        DU_ReferWCautionEligible = 5,
        DU_ReferWCautionIneligible = 6,
        DU_EAIEligible = 7,
        DU_EAIIEligible = 8,
        DU_EAIIIEligible = 9,
        LP_AcceptEligible = 10,
        LP_AcceptIneligible = 11,
        LP_CautionEligible = 12,
        LP_CautionIneligible = 13,
        OutOfScope = 14,
        Lp_AMinus_Level1 = 15,
        Lp_AMinus_Level2 = 16,
        Lp_AMinus_Level3 = 17,
        Lp_AMinus_Level4 = 18,
        Lp_AMinus_Level5 = 19,
        Lp_Refer = 20,
        Total_ApproveEligible = 21,
        Total_ApproveIneligible = 22,
        Total_ReferEligible = 23,
        Total_ReferIneligible = 24,
        GUS_AcceptEligible = 25,
        GUS_AcceptIneligible = 26,
        GUS_ReferEligible = 27,
        GUS_ReferIneligible = 28,
        GUS_ReferWCautionEligible = 29,
        GUS_ReferWCautionIneligible = 30
    }
    public enum E_sProd3rdPartyUwProcessingT
    {
        Normal = 0,
        DU_MyCommunity = 1,
        LP_HomePossible = 2
    }

    public enum E_sVaPriorLoanT
    {
        LeaveBlank = 0,
        FhaFixed = 1,
        FhaArm = 2,
        ConventionalFixed = 3,
        ConventionalArm = 4,
        ConventionalInterestOnly = 5,
        VaFixed = 6,
        VaArm = 7,
        Other = 8
    }
    public enum E_sSpProjectClassFannieT
    {
        [OrderedDescription("Leave Blank", Order = 0)]
        LeaveBlank = 0,

        [OrderedDescription("P Limited Review New", Order = 2)]
        PLimitedReviewNew = 1,

        [OrderedDescription("Q Limited Review Est.", Order = 3)]
        QLimitedReviewEst = 2,

        [OrderedDescription("R Expedited Review New", Order = 4)]
        RExpeditedReviewNew = 3,

        [OrderedDescription("S Expedited Review Est.", Order = 5)]
        SExpeditedReviewEst = 4,

        [OrderedDescription("T Fannie Mae Review", Order = 6)]
        TFannieReview = 5,

        [OrderedDescription("U FHA-Approved", Order = 7)]
        UFhaApproved = 6,

        [OrderedDescription("V Refi Plus/Site Condo/LCOR w/o condo review", Order = 8)]
        VRefiPlus = 7,

        [OrderedDescription("E PUD", Order = 9)]
        EPud = 8, // 4/23/2012 dd - OPM 79164

        [OrderedDescription("F PUD", Order = 10)]
        FPud = 9, // 4/23/2012 dd - OPM 79164

        [OrderedDescription("T PUD", Order = 11)]
        TPud = 10,// 4/23/2012 dd - OPM 79164

        [OrderedDescription("1 Co-op", Order = 12)]
        _1Coop = 11,// 4/23/2012 dd - OPM 79164

        [OrderedDescription("2 Co-op", Order = 13)]
        _2Coop = 12,// 4/23/2012 dd - OPM 79164

        [OrderedDescription("T Co-op", Order = 14)]
        TCoop = 13,// 4/23/2012 dd - OPM 79164

        [OrderedDescription("G Not in Project", Order = 1)]
        GNotInProject = 14,
    }

    public enum E_sSpProjectClassFreddieT
    {
        LeaveBlank = 0,

        [OrderedDescription("Streamlined Review")]
        FreddieMacStreamlinedReview = 1, // 3/26/2009 dd - OPM 27432 - Update 1008 form.

        [OrderedDescription("Established Project")]
        FreddieMacEstablishedProject = 2, // 3/26/2009 dd - OPM 27432 - Update 1008 form.

        [OrderedDescription("New Project")]
        FreddieMacNewProject = 3, // 3/26/2009 dd - OPM 27432 - Update 1008 form.

        [OrderedDescription("Detached Project")]
        FreddieMacDetachedProject = 4, // 3/26/2009 dd - OPM 27432 - Update 1008 form.

        [OrderedDescription("2- to 4-unit Project")]
        FreddieMac2To4UnitProject = 5, // 3/26/2009 dd - OPM 27432 - Update 1008 form.
        // 4/23/2012 dd - FreddieMacReciprocalReview is obsolete per case 79164
        //FreddieMacReciprocalReview = 6 // 3/26/2009 dd - OPM 27432 - Update 1008 form.

        [OrderedDescription("Reciprocal Review - CPM")]
        FreddieMacReciprocalReviewCPM = 7, // 4/23/2012 dd - OPM 79164

        [OrderedDescription("Reciprocal Review - PERS")]
        FreddieMacReciprocalReviewPERS = 8, // 4/23/2012 dd - OPM 79164

        [OrderedDescription("Reciprocal Review - FHA")]
        FreddieMacReciprocalReviewFHA = 9// 4/23/2012 dd - OPM 79164

    }
    public enum E_sProdCrManualDerogRecentStatusT
    {
        NotSatisfied = 0,
        Discharged = 1,
        Dismissed = 2,
    }

    public enum E_BkMultipleT
    {
        None = 0,
        PossibleRollOver = 1,
        Multiple = 2
    }

    public enum E_sTemplateAssignT
    {
        [OrderedDescription("Loan Creator", Order = 1)]
        LoanCreator = 0,

        [OrderedDescription("Leave Blank", Order = 0)]
        LeaveEmpty = 1,

        [OrderedDescription("Specific Person", Order = 2)]
        SpecificPerson = 2,
    }

    /// <summary>
    /// See Opm# 1884.
    /// </summary>
    public enum E_sTemplatePublishLevelT
    {
        [OrderedDescription("Individual - Only person assign to this template.")]
        Individual = 0, // Only people assigned to the template can use it to create new loan

        [OrderedDescription("Branch - Only person in the same branch.")]
        Branch = 1,

        [OrderedDescription("Corporate - Everyone.")]
        Corporate = 2,
    }

    public enum E_sProdSpStructureT
    {
        // Blank = 0, Obsolete, Opm case # 1756. May 6th, 2006.
        Attached = 1,
        Detached = 2
    }
    public enum E_sProdMIOptionT
    {
        // 9/7/2006 dd - OPM 7285
        [Description("Description for LeaveBlank")]
        LeaveBlank = 0,
        [Description("Description for NoPmi")]
        NoPmi = 1,
        [Description("Description for BorrowerPdPmi")]
        BorrowerPdPmi = 2
        // LenderPdPmi = 3 // Only add this when we start using. Right we should only use NoPmi and BorrowerPdPmi
    }

    public enum E_sReasonNotAllowingResubmission
    {
        LoanStatusNotOpen = 0,
        LoanLinked = 1,
        RateStillLocked = 2,
        NotAStandAloneSecondLien = 3,
        LoanIsTemplate = 4,
        HasRateLockRequest = 5,
        HasRegisteredLoanProgram = 6
    }

    public enum E_sLienQualifyModeT
    {
        ThisLoan = 0,
        OtherLoan = 1
    }


    public enum E_sPmlSubmitStatusT
    {
        NeverSubmitted = 0,
        CurrentlySubmitted = 1
    }

    public enum E_TransformToPmlT
    {
        FromScratch = 0,
        FromPoint = 1,
        FromFannieMae = 2,
        RerunForRateLockRequest = 3,
    }
    public enum E_sProdCalcEntryT
    {
        DwnPmtPc = 0,
        Equity = 1,
        LtvTarget = 2,
        LAmt = 3,
        CltvTarget = 4,
        LtvTargetOtherFin = 5,
        LAmtOtherFin = 6,
        HouseVal = 7,
        LineAmountRatio = 8,
        HcltvTarget = 9,
        AsCompleted = 10
    }

    public enum E_CalcModeT
    {
        /// <summary>
        /// This is used for LendersOffice page editing
        /// </summary>
        LendersOfficePageEditing = 0,

        /// <summary>
        /// Editing in PriceMyLoan pages and running pricing engine
        /// </summary>
        PriceMyLoan = 1
    }

    /// <summary>
    /// // 9/4/2013 dd - Introduce by OPM 137164
    /// </summary>
    public enum E_RunPmlRequestSourceT
    {
        Regular = 0,
        ConsumerPortal = 1
    }

    public enum E_sVaBuildingT
    {
        LeaveBlank = 0,
        Detached = 1,
        SemiDetached = 2,
        Row = 3,
        AptUnit = 4,
    }
    public enum E_sVaBuildingStatusT
    {
        LeaveBlank = 0,
        Proposed = 1,
        Existing = 2,
        UnderConstruction = 3,
        Repair = 4,
    }

    public enum E_sVaConstructComplianceInspectionMadeByT
    {
        LeaveBlank = 0,
        Fha = 1,
        Va = 2,
        None = 3,
    }
    public enum E_sVaSpAvailableForInspectTimeT
    {
        LeaveBlank = 0,
        AM = 1,
        PM = 2,
        BothAmPm = 3,
    }
    public enum E_sSpUtilT
    {
        LeaveBlank = 0,
        Public = 1,
        Community = 2,
        Individual = 3
    }

    public enum E_sVaStructureT
    {
        LeaveBlank = 0,
        Conventional = 1,
        SingleWideMobil = 2,
        DoubleWideMobil = 3,
        MobilLotOnly = 4,
        PrefabricatedHome = 5,
        CondoConversion = 6
    }

    public enum E_sVaLCodeT
    {
        LeaveBlank = 0,
        Purchase = 1,
        Irrrl = 2,
        CashoutRefin = 3,
        ManufacturedHomeRefi = 4,
        RefiOver90Rv = 5
    }

    public enum E_aVaServiceBranchT
    {
        LeaveBlank = 0,
        Army = 1,
        Navy = 2,
        AirForce = 3,
        Marine = 4,
        CoastGuard = 5,
        Other = 6
    }
    public enum E_aVaMilitaryStatT
    {
        LeaveBlank = 0,
        SeparatedFromService = 1,
        InService = 2
    }

    public enum E_sVaRiskT
    {
        LeaveBlank = 0,
        Approve = 1,
        Refer = 2
    }

    public enum E_sVaAutoUnderwritingT
    {
        LeaveBlank = 0,
        Lp = 1,
        Du = 2,
        PmiAura = 3,
        Clues = 4,
        Zippy = 5
    }
    public enum E_sVaManufacturedHomeT
    {
        LeaveBlank = 0,
        NotMobilHome = 1,
        MobilHomeOnly = 2,
        MobilHomeVetOwnedLot = 3,
        MobilHomeOnPermanentFoundation = 4
    }

    public enum E_sVaPropDesignationT
    {
        LeaveBlank = 0,
        ExistingOcc = 1,
        ProposedConstruction = 2,
        ExistingNew = 3,
        EnergyImprovement = 4
    }

    public enum E_sVaApprT
    {
        LeaveBlank = 0,
        SingleProp = 1,
        MasterCrvCase = 2,
        LappLenderAppr = 3,
        ManufacturedHome = 4,
        HudVaConversion = 5,
        PropMgmtCase = 6
    }

    public enum E_sVaOwnershipT
    {
        LeaveBlank = 0,
        SoleOwnership = 1,
        JointWithOtherVets = 2,
        JointWithNonVet = 3
    }

    public enum E_sVaHybridArmT
    {
        LeaveBlank = 0,
        ThreeOne = 1,
        FiveOne = 2,
        SevenEleven = 3,
        TenOne = 4
    }

    public enum E_sVaFinMethT
    {
        LeaveBlank = 0,
        RegularFixed = 1,
        GpmNeverExceedCrv = 2,
        GpmOther = 3,
        Gem = 4,
        TemporaryBuydown = 5,
        HybridArm = 6,
        ARM = 7 // 7/31/2008 dd - New option for VA 26-0286 version Aug 2006.
    }
    public enum E_sVaLPurposeT
    {
        LeaveBlank = 0,
        Home = 1,
        ManufacturedHome = 2,
        Condo = 3,
        Alteration = 4,
        Refinance = 5
    }

    public enum E_sVaLProceedDepositT
    {
        Blank = 0,
        Escrow = 1,
        EarmarkedAcc = 2
    }

    public enum E_sBuydownContributorT
    {
        LeaveBlank = 0,
        Borrower = 1,
        Other = 2,
        LenderPremiumFinanced = 3,
        Seller = 4,
        Builder = 5,
        Unassigned = 6,
        Parent = 7,
        NonParentRelative = 8,
        UnrelatedFriend = 9,
        Employer = 10
    }

    public enum E_CountryMailT
    {
        UnitedStates = 0,
        LeaveBlank = 1,
        Other = 2,
        Argentina = 3,
        Bangladesh = 4,
        Brazil = 5,
        Burma_Myanmar = 6,
        Canada = 7,
        China = 8,
        Congo = 9,
        Egypt = 10,
        ElSalvador = 11,
        Ethiopia = 12,
        France = 13,
        Germany = 14,
        Honduras = 15,
        India = 16,
        Indonesia = 17,
        Iran = 18,
        Italy = 19,
        Japan = 20,
        Mexico = 21,
        Nigeria = 22,
        Pakistan = 23,
        Peru = 24,
        Philippines = 25,
        Russia = 26,
        Spain = 27,
        Thailand = 28,
        Turkey = 29,
        Ukraine = 30,
        UnitedKingdom = 31,
        Vietnam = 32
    }
    public enum E_sFreddieArmIndexT
    {
        LeaveBlank = 0,
        OneYearTreasury = 1,
        ThreeYearTreasury = 2,
        SixMonthTreasury = 3,
        EleventhDistrictCostOfFunds = 4,
        NationalMonthlyMedianCostOfFunds = 5,
        LIBOR = 6,
        Other = 7
    }


    public enum E_sNegAmortT
    {
        LeaveBlank = 0,
        NoNegAmort = 1,
        PotentialNegAmort = 2,
        ScheduledNegAmort = 3
    }


    public enum E_sFreddieDocT
    {
        LeaveBlank = 0,
        FullDocumentation = 1,
        NoDepositVerif = 2,
        NoDepEmpIncVerif = 3,
        NoDoc = 4,
        NoEmpIncVerif = 5
    }

    public enum E_sGseRefPurposeT
    {
        LeaveBlank = 0,

        CashOutDebtConsolidation = 1,

        CashOutHomeImprovement = 2,

        CashOutLimited = 3,

        CashOutOther = 4,

        NoCashOutFHAStreamlinedRefinance = 5,

        NoCashOutFREOwnedRefinance = 6,

        NoCashOutOther = 7,

        NoCashOutStreamlinedRefinance = 8,

        ChangeInRateTerm = 9
    }

    public enum E_sBuildingStatusT
    {
        LeaveBlank = 0,
        Existing = 1,
        Proposed = 2,
        SubjectToAlterImproveRepairAndRehab = 3,
        SubstantiallyRehabilitated = 4,
        UnderConstruction = 5
    }

    public enum E_sCondoProjClassT
    {
        LeaveBlank = 0,
        A_IIICondominium = 1,
        B_IICondominium = 2,
        C_ICondominium = 3,
        ApprovedFHA_VACondominiumProjectOrSpotLoan = 4
    }

    public enum E_sGseSpT
    {
        LeaveBlank = 0,
        Attached = 1,
        Condominium = 2,
        Cooperative = 3,
        Detached = 4,
        DetachedCondominium = 5,
        HighRiseCondominium = 6,
        ManufacturedHomeCondominium = 7,
        ManufacturedHomeMultiwide = 8,
        ManufacturedHousing = 9,
        ManufacturedHousingSingleWide = 10,
        Modular = 11,
        PUD = 12
    }

    public enum E_sFredProcPointT
    {
        [Description("")]
        LeaveBlank = 0,
        [Description("Application")]
        Application = 1,
        [Description("Final Disposition")]
        FinalDisposition = 2,
        [Description("Post-Closing Quality Control")]
        PostClosingQualityControl = 3,
        [Description("Prequalification")]
        Prequalification = 4,
        [Description("Underwriting")]
        Underwriting = 5
    }

    public enum E_aOIDescT
    {
        Other = 0,
        MilitaryBasePay = 1,
        MilitaryRationsAllowance = 2,
        MilitaryFlightPay = 3,
        MilitaryHazardPay = 4,
        MilitaryClothesAllowance = 5,
        MilitaryQuartersAllowance = 6,
        MilitaryPropPay = 7,
        MilitaryOverseasPay = 8,
        MilitaryCombatPay = 9,
        MilitaryVariableHousingAllowance = 10,
        AlimonyChildSupport = 11,
        NotesReceivableInstallment = 12,
        PensionRetirement = 13,
        SocialSecurityDisability = 14,
        RealEstateMortgageDifferential = 15,
        Trust = 16,
        UnemploymentWelfare = 17,
        AutomobileExpenseAccount = 18,
        FosterCare = 19,
        VABenefitsNonEducation = 20,
        SubjPropNetCashFlow = 21,
        CapitalGains = 22,
        EmploymentRelatedAssets = 23,
        ForeignIncome = 24,
        RoyaltyPayment = 25,
        SeasonalIncome = 26,
        TemporaryLeave = 27,
        TipIncome = 28,
        BoarderIncome = 29,
        MortgageCreditCertificate = 30,
        TrailingCoBorrowerIncome = 31,
        AccessoryUnitIncome = 32,
        NonBorrowerHouseholdIncome = 33,
        HousingChoiceVoucher = 34,
        SocialSecurity = 35,
        Disability = 36,
        Alimony = 37,
        ChildSupport = 38,
        ContractBasis = 39,
        DefinedContributionPlan = 40,
        HousingAllowance = 41,
        MiscellaneousIncome = 42,
        PublicAssistance = 43,
        WorkersCompensation = 44
    }

    public enum E_sHmdaPreapprovalT
    {
        LeaveBlank = 0, //""
        [OrderedDescription("Preapproval was requested")]
        PreapprovalRequested = 1, // "1"
        [OrderedDescription("Preapproval was not requested")]
        PreapprovalNotRequested = 2, //"2"
        NotApplicable = 3 //"3"
    }

    public enum E_sHmdaLienT
    {
        LeaveBlank = 0, //""
        FirstLien = 1, //"1"
        SubordinateLien = 2,//"2"
        [OrderedDescription("Not secured by a lien")]
        NotSecuredByLien = 3,//"3"
        [OrderedDescription("Not applicable (purchase loan)")]
        NotApplicablePurchaseLoan = 4
    }
    public enum E_sHmdaPropT
    {
        LeaveBlank = 0, // ""
        [OrderedDescription("1 - 4 SFR")]
        OneTo4Family = 1,//"1"
        ManufacturedHousing = 2,//"2"
        [OrderedDescription("Multifamily")]
        MultiFamiliy = 3 //"3"
    }

    public enum E_sHmdaPurchaser2004T
    {
        LeaveBlank = 0, //""
        [OrderedDescription("Loan was not sold")]
        LoanWasNotSold = 1,//"0"

        FNMA = 2,//"1"
        GNMA = 3,//"2"
        FHLMC = 4,
        FarmerMac = 5,
        PrivateSecuritization = 6,

        [OrderedDescription("Commercial bank, savings bank or savings association")]
        CommercialBank = 7,

        [OrderedDescription("Life ins. company, credit union, mortgage bank, finance company")]
        LifeInsCreditUnion = 8,
        AffiliateInstitution = 9,

        [OrderedDescription("Other type of purchaser")]
        OtherType = 10
    }

    public enum sHmdaBalloonPaymentT
    {
        LeaveBlank = 0,
        BalloonPayment = 1,
        NoBalloonPayment = 2
    }

    public enum sHmdaInterestOnlyPaymentT
    {
        LeaveBlank = 0,
        InterestOnlyPayments = 1,
        NoInterestOnlyPayments = 2
    }

    public enum sHmdaNegativeAmortizationT
    {
        LeaveBlank = 0,
        NegativeAmortization = 1,
        NoNegativeAmortization = 2
    }

    /// <summary>
    /// The 2015 of Hmda's Purchaser type.  OPM 451721.
    /// </summary>
    public enum HmdaPurchaser2015T
    {
        LeaveBlank = -1,
        NA = 0,
        FannieMae = 1,
        GinnieMae = 2,
        FreddieMac = 3,
        FarmerMac = 4,
        PrivateSecuritizer = 5,
        CommercialBankSavingsBankSavingsAssociation = 6,
        LifeInsCreditUnion = 7,
        CreditUnionMortgageCompanyFinanceCompany = 71,
        LifeInsuranceCompany = 72,
        AffiliateInstitution = 8,
        Other = 9
    }

    public enum sHmdaLoanTypeT
    {
        LeaveBlank = 0,
        Conventional = 1,
        FHA = 2,
        VA = 3,
        USDA = 4
    }

    public enum sHmdaLoanPurposeT
    {
        LeaveBlank = 0,
        HomePurchase = 1,
        HomeImprovement = 2,
        Refinancing = 31,
        CashOutRefinancing = 32,
        OtherPurpose = 4
    }

    public enum sHmdaConstructionMethT
    {
        LeaveBlank = 0,
        SiteBuilt = 1,
        ManufacturedHome = 2
    }

    public enum sHmdaCoApplicantSourceT
    {
        NoCoApplicant = 0,
        CoborrowerOnPrimaryApp = 1,
        ApplicantOnSecondaryApp = 2
    }

    public enum sHmdaReportCreditScoreAsT
    {
        Blank = 0,
        Borrower = 1,
        Coborrower = 2
    }

    public enum sHmdaHoepaStatusT
    {
        LeaveBlank = 0,
        HighCost = 1,
        NotHighCost = 2,
        NA = 3
    }

    public enum sHmdaOtherNonAmortFeatureT
    {
        LeaveBlank = 0,
        OtherNonFullyAmortizingFeatures = 1,
        NoOtherNonFullyAmortizingFeatures = 2
    }

    public enum sHmdaManufacturedTypeT
    {
        LeaveBlank = 0,
        ManufacturedHomeAndLand = 1,
        ManufacturedHomeAndNotLand = 2,
        NA = 3
    }

    public enum sHmdaManufacturedInterestT
    {
        LeaveBlank = 0,
        DirectOwnership = 1,
        IndirectOwnership = 2,
        PaidLeasehold = 3,
        UnpaidLeasehold = 4,
        NA = 5
    }

    public enum sHmdaSubmissionApplicationT
    {
        LeaveBlank = 0,
        SubmittedDirectlyToInstitution = 1,
        NotSubmittedDirectlyToInstitution = 2,
        NA = 3
    }

    public enum sHmdaInitiallyPayableToInstitutionT
    {
        LeaveBlank = 0,
        InitiallyPayableToInstitution = 1,
        NotInitiallyPayableToInstitution = 2,
        NA = 3
    }

    public enum sHmdaReverseMortgageT
    {
        LeaveBlank = 0,
        ReverseMortgage = 1,
        NotReverseMortgage = 2
    }

    public enum sHmdaBusinessPurposeT
    {
        LeaveBlank = 0,
        PrimarilyForBusinessOrCommercial = 1,
        NotPrimarilyForBusinessOrCommercial = 2
    }

    public enum sHmdaCreditScoreModelT
    {
        LeaveBlank = 0,
        EquifaxBeacon5 = 1,
        ExperianFairIsaac = 2,
        FicoRiskScoreClassic04 = 3,
        FicoRiskScoreClassic98 = 4,
        VantageScore2 = 5,
        VantageScore3 = 6,
        MoreThanOneScoringModel = 7,
        Other = 8,
        NotApplicable = 9,
        NoCoApplicant = 10
    }

    public enum E_sLoanBeingRefinancedAmortizationT
    {
        [Description("")]
        LeaveBlank = 0,

        [Description("Fixed")]
        Fixed = 1,

        [Description("Adjustable")]
        Adjustable = 2
    }

    public enum E_sLoanBeingRefinancedLienPosT
    {
        [Description("")]
        LeaveBlank = 0,

        [Description("1st Lien")]
        First,

        [Description("2nd Lien")]
        Second
    }

    public enum E_sFHAPurchPriceSrcT
    {
        PurchPrice = 0,
        From203kWsC3 = 1,
        Locked = 2
    }

    public enum E_sFHAInsEndorseCounselingT
    {
        Blank = 0,
        Lender = 1,
        None = 2,
        ThirdParty = 3
    }

    public enum E_sVaSpOccT
    {
        LeaveBlank = 0,
        OccByOwner = 1,
        NeverOcc = 2,
        Vacant = 3,
        Rental = 4,
    }
    public enum E_TriState
    {
        [OrderedDescription("0")]
        Blank = 0,

        [OrderedDescription("is")]
        Yes = 1,

        [OrderedDescription("is not")]
        No = 2

    }

    public enum E_aVaVestTitleT
    {
        Blank = 0,

        Veteran = 1,

        [OrderedDescription("Veteran & Spouse")]
        VeteranAndSpouse = 2,

        [OrderedDescription("Other (Specify)")]
        Other = 3
    }

    public enum E_sFHA203kBorrAcknowledgeT
    {
        Blank = 0,
        ToBorrower = 1,
        ToPrinciple = 2,
        Other = 3
    }

    public enum E_sFHA203kType
    {
        [OrderedDescription("N/A")]
        NA = 0,
        Limited = 1,
        Standard = 2
    }

    public enum E_sFHACommitStageT
    {
        Blank = 0,
        Conditional = 1,
        Firm = 2
    }

    public enum E_VorT
    {
        Mortgage = 0,
        LandContract = 1,
        Rental = 2,
        Other = 3
    }
    public enum E_sApprT
    {
        Full = 0,
        DriveBy = 1,
        Statistical = 2,
        Stated = 3
    }
    public enum E_ReoStatusT
    {
        [OrderedDescription("Retained", Order = 3)]
        Residence = 0,

        [OrderedDescription("Sold", Order = 0)]
        Sale = 1,

        PendingSale = 2,

        Rental = 3
    }
    public enum E_ReoTypeT
    {
        LeaveBlank = 0,

        [OrderedDescription("2-4Plx", Order = 1)]
        _2_4Plx = 1,

        [OrderedDescription("Com-NR", Order = 2)]
        ComNR = 2,

        [OrderedDescription("Com-R", Order = 3)]
        ComR = 3,

        [OrderedDescription("Condo", Order = 4)]
        Condo = 4,

        [OrderedDescription("Coop", Order = 5)]
        Coop = 5,

        [OrderedDescription("Farm", Order = 6)]
        Farm = 6,

        [OrderedDescription("Land", Order = 7)]
        Land = 7,

        [OrderedDescription("Mixed", Order = 8)]
        Mixed = 8,

        [OrderedDescription("Mobil", Order = 9)]
        Mobil = 9,

        [OrderedDescription("Multi", Order = 10)]
        Multi = 10,

        [OrderedDescription("SFR", Order = 11)]
        SFR = 11,

        [OrderedDescription("Town", Order = 12)]
        Town = 12,

        [OrderedDescription("Other", Order = 13)]
        Other = 13
    }
    public enum E_ReoGroupT
    {
        Residence = 0x0001,
        Sale = 0x0002,
        PendingSale = 0x0004,
        Rental = 0x0008,
        All = Residence | Sale | PendingSale | Rental
    }

    [FlagsAttribute]
    public enum E_IntegrationT
    {
        None = 0,
        NHC = 1 // Just add another integration type to this if we need to, it would have to be 2^n format though.
    }

    [Flags]
    public enum E_AmortChangeEventType
    {
        None = 0,
        RateChange = 0x01,
        IOExpired = 0x02,
        MITermination = 0x04,
        FinalBalloonPmt = 0x08,
        PIChange = 0x10,
        IndexChange = 0x20
    }

    public enum E_GenderT
    {
        //"M" male, "F" female, "N" n/a, "U" unfurnished, or " " leave blank

        /// <summary>
        /// Leave the gender blank.
        /// </summary>
        [OrderedDescription("")]
        LeaveBlank = 0,

        /// <summary>
        /// Male gender.
        /// </summary>
        Male = 1,

        /// <summary>
        /// Female gender.
        /// </summary>
        Female = 2,

        /// <summary>
        /// Not available for gender field.
        /// </summary>
        [OrderedDescription("N/A", Order = 4)]
        NA = 3,

        /// <summary>
        /// Gender is not furnished.
        /// </summary>
        [OrderedDescription("Not Furnished", Order = 3)]
        Unfurnished = 4,

        /// <summary>
        /// Both Male and Female options selected.
        /// </summary>
        MaleAndFemale = 5,

        /// <summary>
        /// Gender not furnished but male is best guess.
        /// </summary>
        MaleAndNotFurnished = 6,

        /// <summary>
        /// Gender not furnshed but female is best guess.
        /// </summary>
        FemaleAndNotFurnished = 7,

        /// <summary>
        /// Gender not furnished but male and female is best guess.
        /// </summary>
        MaleFemaleNotFurnished = 8
    }

    public enum E_ReturnOptionIfNotExist
    {
        ReturnEmptyObject = 0,
        CreateNew = 1,              // may fall back on another selection if the requested item doesn't exist
        CreateNewDoNotFallBack = 2
    }
    public enum E_sFHASecondaryFinancingSourceT
    {
        LeaveBlank = 0,
        Government = 1,
        NonProfit = 2,
        Family = 3,
        Other = 4
    }
    public enum E_PreparerFormT
    {
        None = 0,
        App1003Interviewer = 1,					// loan officer, done
        RequestOfAppraisal = 2,					// processor, done
        VerificationOfDeposit = 3,				// processor, done
        VerificationOfEmployment = 4,			// processor, done
        VerificationOfMortgage = 5,				// processor, done
        VerificationOfLoan = 6,					// processor, done
        CreditDenialStatement = 7,				// loan officer, done
        RequestOfInsurance = 8,					// processor, done
        RequestOfTitle = 9,						// processor, done
        VerificationOfRent = 10,				// processor, done
        VerificationOfLandContract = 11,		// processor, done
        FloridaLenderDisclosure = 12,	// none
        FloridaMortBrokerBusinessContract = 13, // loan officer, done
        AppraisalDisclosure = 14,				// loan officer, done
        BorrCertification = 15,					// loan officer
        BorrSignatureAuthorization = 16,		// loan officer
        CACompoundStatement = 17,				// loan officer
        ECOA = 18,								// loan officer
        GeorgiaDisclosure = 19,					// loan officer
        Gfe = 20,								// loan officer, also used for CA MLDS, FairLendingNotice, ItemizationOfAmountFinanced, GfeProviderRelationship
        LoanSubmission = 21,					// loan officer
        MortgageLoanOrigAgreement = 22,			// loan officer
        PrivacyPolicyDisclosure = 23,			// loan officer
        RealEstateDisclosure = 24,				// loan officer
        FhaAddendum = 25,						// none
        FHAAddendumLender = 26,					// loan officer
        FHAAddendumSponsor = 27,				// Lender
        FHAConditionalCommitment = 28,			// underwritter
        FHACondCommitMortgagee = 29,			// loan officer
        FHAInsEndorsementAppraiser = 30,		// appraiser
        FHAInsEndorsementUnderwriter = 31,		// underwriter
        FHAAnalysisAppraisalAppraiser = 32,     // appraiser
        FHAAnalysisAppraisalUnderwriter = 33,   // underwriter
        FHACreditAnalysisPurchaseUnderwriter = 34, // underwriter
        FHACreditAnalysisRefinanceUnderwriter = 35, // underwriter
        FHA203kWorksheetUnderwriter = 36,		// underwriter
        LockInConfirmation = 37,				// loan officer
        MortgageLoanCommitment = 38,			// loan officer
        TXMortgageBrokerDisclosure = 39,		// loan officer
        MortgageLoanCommitmentAlternateLender = 40, // 2/5/2004 dd - Use as alternate lender address in Loan Commitment printout.
        FloodHazardNotice = 41,                 // 2/9/2004 dd - Lender information in Flood Hazard Notice screen.
        FloodHazardDetermination = 42, // 2/9/2004 dd - Lender information in Standard Flood Hazard Determination.
        FloodHazardDeterminationPreparer = 43, // 2/9/2004 dd - Preparer information is section F of Standard Flood Hazard Determination.
        FHAAppraisedValueDisclosure = 44, // lender.  2/19/2004 dd - Lender information in FHA HUD Appraised Value Disclosure.
        TransmMOriginator = 45,
        VA26_1820 = 46, // 4/21/2004 dd - Use prepare date for form VA 26-1820. Certification of Loan Disbursement
        VA26_1820Agent1 = 47, // 4/21/2004 dd - Use in VA 26-1820 Authorized Agents section.
        VA26_1820Agent2 = 48, // 4/21/2004 dd - Use in VA 26-1820 Authorized Agents section.
        VA26_1820Agent3 = 49, // 4/21/2004 dd - Use in VA 26-1820 Authorized Agents section.
        VA26_1820Agent4 = 50, // 4/21/2004 dd - Use in VA 26-1820 Authorized Agents section.
        VA26_1820Agent5 = 51, // 4/21/2004 dd - Use in VA 26-1820 Authorized Agents section.
        VA26_1820Lender = 52, // 4/26/2004 dd - Use in VA 26-1820 Lender Information.
        VA26_8937Lender = 53, // 4/26/2004 dd - Use in VA 26-8937 (Verification of Benefit) Lender Information
        VA26_0285Lender = 54, // 5/12/2004 dd - Use in VA 26-0285 Transmittal List. Use Lender Information
        VA26_0285Broker = 55, // 5/12/2004 dd - Use in VA 26-0285 Transmittal List. Use Broker Information
        VA26_8928Lender = 56, // 5/12/2004 dd - Use in VA 26-8928 Refin Worksheet. Use Lender Information.
        FHAAddendumDulyAgent = 57, // 5/12/2004 dd - Use in FHA/VA Addendum. Duly Agent info.
        VA26_1805FirmMakingRequest = 58, // 5/12/2004 dd - Use in VA 26-1805 Question 5. Firm Making Request
        VA26_1805Broker = 59, // 5/12/2004 dd - Use in VA 26-1805 Question 21, 22. Should import from Broker Info or Loan Officer company info
        VA26_1805KeyAt = 60, // 5/12/2004 dd - Use in VA 26-1805 Question 24
        VA26_1805Builder = 61, // 5/12/2004 dd - Use in VA 26-1805 Question 29
        VA26_1805Warrantor = 62, // 5/12/2004 dd - Use in VA 26-1805 Question 29
        VA26_1805Authorize = 63, // 5/12/2004 dd - Use in VA 26-1805 Question 39 - 41
        VA26_1805Appraiser = 64, // 5/12/2004 dd - Use in VA 26-1805 Question 42, 43, Copy from Appraiser agent role.
        SurveyRequest_From = 65, // 1/31/2005 dd - Use in Survey Request, From
        SurveyRequest_To = 66, // 1/31/2005 dd - Use in Survey Request, To
        CreditExperian = 67, // 2/8/2005 dd - Use in Credit Score Disclosure
        CreditEquifax = 68, // 2/8/2005 dd - Use in Credit Score Disclosure
        CreditTransUnion = 69, // 2/8/2005 dd - Use in Credit Score Disclosure
        CreditDisclosureLender = 70, // 2/8/2005 dd - Use in Credit Score Disclosure. Should copy Broker Information.
        AggregateEscrowServicer = 71, // 1/17/2006 dd - Use in Aggregate Escrow Disclosure
        ServicingDisclosure = 72, // 7/7/2006 dd - Use in Servicing Disclosure form.
        FHA92900LtUnderwriter = 73,
        FHAAddendumUnderwriter = 74,
        FHAAddendumMortgagee = 75,
        Tax1098 = 76, // 9/2/2009 dd - OPM 18055
        Til = 77,
        WADisclosure = 78,
        SafeHarborARM = 79, // 3/24/2011 vm - OPM 63599
        SafeHarborFixed = 80, // 3/24/2011 vm - OPM 63599
        UsdaGuaranteeApprovedLender = 81, // 4/15/2011 dd - OPM 60397
        UsdaGuaranteeThirdPartyOriginator = 82, // 4/15/2011 dd - OPM 60397
        CreditDenialScoreContact = 83, // 7/20/2011 dd - OPM 68552
        PatriotActDisclosure = 84, // OPM 62448
        GoodbyeLetter = 85, // OPM 65213
        UsdaConditionalCommitment = 86, // OPM 132159
        TRIDClosingCosts_Lender = 87, //OPM 204864
        TRIDClosingCosts_LoanOfficer = 88, //OPM 204864
        TRIDClosingCosts_MortgageBroker = 89, //OPM 204864
        TRIDClosingCosts_SettlementAgent = 90, //OPM 212280
        TRIDClosingCosts_RealEstateBrokerBuyer = 91, //OPM 212280
        TRIDClosingCosts_RealEstateBrokerSeller = 92, //OPM 212280
        TRIDClosingCosts_PropertySeller = 93, //OPM 212280
        VA26_1805ApplicablePointOfContact = 94, // OPM 80904
        MortgageServicingRightsHolder = 95, // opm 457166
        PaymentStatementServicer = 96,

        LAST = PaymentStatementServicer
    }

    public enum E_sFHAConstructionT
    {
        Blank = 0,
        Existing = 1,
        Proposed = 2,
        New = 3 // 9/3/2008 dd - Added on 9/3/08 - For form HUD-92900-LT
    }

    public enum E_sAppliedProgramTransferedT
    {
        Blank = 0,
        RetainNone = 1,
        RetainSome = 2,
        RetainAll = 3
    }

    public enum E_sAbleToServiceAndDecisionT
    {
        Blank = 0,
        Will = 1,
        Willnot = 2,
        HaventDecide = 3
    }

    public enum E_sTransferPercentFirstLienLoanT
    {
        Blank = 0,
        _25 = 1, // 0 - 25%
        _50 = 2, // 25 - 50%
        _75 = 3, // 50 - 75%
        _100 = 4 // 75 - 100%
    }

    public enum E_sFirstLienLoanServiceEstimateT
    {
        Blank = 0,
        Does = 1,
        Doesnot = 2
    }

    public enum E_sFirstLienLoanDeclareServiceRecordIncludeT
    {
        Blank = 0,
        Does = 1,
        Doesnot = 2
    }
    public enum E_sRefundabilityOfFeeToLenderT
    {
        Blank = 0,
        Nonrefundable = 1,
        Refundable = 2,
        NA = 3
    }
    public enum E_sProdSpT
    {
        SFR = 0,
        PUD = 1,
        Condo = 2,
        CoOp = 3,
        Manufactured = 4,
        Townhouse = 5,
        Commercial = 6,
        MixedUse = 7,
        TwoUnits = 8,
        ThreeUnits = 9,
        FourUnits = 10,
        Modular = 11,
        Rowhouse = 12
    }


    public enum E_sProdIT
    {
        FullDoc = 0,
        ReducedDoc = 1,
        Stated = 2,
        Verify = 3,
        Light = 4,
        NotStated = 5
    }

    public enum E_sProdAssetT
    {
        FullDoc = 0,
        ReducedDoc = 1,
        Stated = 2,
        Verify = 3,
        NotStated = 4
    }

    public enum E_sProdDocT
    {   // Note that any category left out in the name of a doc type means "Verified".
        // So practically, SIVA could just be SI
        Full = 0,
        Alt = 1,
        Light = 2,
        SIVA = 3,
        VISA = 4,
        SISA = 5,
        NIVA = 6,
        NINA = 7,
        NISA = 8,
        NINANE = 9,
        NIVANE = 10,
        VINA = 11, // 7/23/2008 dd - Added VINA (OPM 23570)
        Streamline = 12, // 1/21/2009 dd - OPM 27153
        _12MoPersonalBankStatements = 13,
        _24MoPersonalBankStatements = 14,
        _12MoBusinessBankStatements = 15,
        _24MoBusinessBankStatements = 16,
        OtherBankStatements = 17,
        _1YrTaxReturns = 18,
        AssetUtilization = 19,
        DebtServiceCoverage = 20, // (DSCR)

        // 12/11 ML 476662 - This was renamed to "No Ratio" in the UI.
        // Since SAEs have already written rules that are included in
        // pricing snapshots, the constant's name has not been updated.
        NoIncome = 21, 
        Voe = 22
    }

    public enum E_sFannieSpT
    {
        [OrderedDescription("Leave Blank")]
        LeaveBlank = 0,

        Detached = 1,
        Attached = 2,
        Condo = 3,
        PUD = 4,

        [OrderedDescription("Co-Op")]
        CoOp = 5,

        HighRiseCondo = 6,

        Manufactured = 7,

        DetachedCondo = 8,

        [OrderedDescription("Manufactured home/condo/pud/coop")]
        ManufacturedCondoPudCoop = 9

    }
    /// <summary>
    /// This is basically for FannieMae format
    /// </summary>
    public enum E_sArmIndexT
    {
        LeaveBlank = 0,

        [OrderedDescription("Weekly Average CMT")]
        WeeklyAvgCMT = 1,

        [OrderedDescription("Monthly Average CMT")]
        MonthlyAvgCMT = 2,

        [OrderedDescription("Weekly Average TAAI")]
        WeeklyAvgTAAI = 3,

        [OrderedDescription("Weekly Average TAABD")]
        WeeklyAvgTAABD = 4,

        [OrderedDescription("Weekly Average SMTI")]
        WeeklyAvgSMTI = 5,

        [OrderedDescription("Daily CD Rate")]
        DailyCDRate = 6,

        [OrderedDescription("Weekly Average CD Rate")]
        WeeklyAvgCDRate = 7,

        [OrderedDescription("Weekly Average Prime Rate")]
        WeeklyAvgPrimeRate = 8,

        [OrderedDescription("T-Bill Daily Value")]
        TBillDailyValue = 9,

        [OrderedDescription("11th District COF")]
        EleventhDistrictCOF = 10,

        NationalMonthlyMedianCostOfFunds = 11,

        WallStreetJournalLIBOR = 12,

        FannieMaeLIBOR = 13
    }

    /// <summary>
    /// This is basically the list of PMLMASTER arm indices
    /// Harcoded because adding a new index is rare event.
    /// </summary>
    public enum E_MasterArmIndexT
    {
        OneMonthLibor = 0,
        OneYearCmt = 1,
        OneYearLibor = 2,
        TenYearCmt = 3,
        EleventhDistrictCofi = 4,
        TwoYearCmt = 5,
        ThreeMonthLibor = 6,
        ThreeYearCmt = 7,
        FiveYearCmt = 8,
        SixMonthCmt = 9,
        SixMonthLibor = 10,
        SevenYearCmt = 11,
        MTA = 12,
        PrimeRate = 13
    }

    public enum E_aProdCitizenT
    {
        [OrderedDescription("US Citizen")]
        USCitizen = 0,

        [OrderedDescription("Permanent Resident")]
        PermanentResident = 1,

        [OrderedDescription("Non-permanent Resident")]
        NonpermanentResident = 2,

        [OrderedDescription("Non-Resident Alien (Foreign National)")]
        ForeignNational = 3,
    }
    public enum E_sFannieDocT
    {
        [OrderedDescription("Leave Blank")]
        LeaveBlank = 0,

        Alternative = 1,
        Full = 2,

        NoDocumentation = 3,

        Reduced = 4,

        StreamlinedRefinanced = 5,

        NoRatio = 6,

        LimitedDocumentation = 7,

        [OrderedDescription("No Income, No Employment and No Assets on 1003")]
        NoIncomeNoEmploymentNoAssets = 8,

        [OrderedDescription("No Income and No Assets on 1003")]
        NoIncomeNoAssets = 9,

        [OrderedDescription("No Assets on 1003")]
        NoAssets = 10,

        [OrderedDescription("No Income and No Employment on 1003")]
        NoIncomeNoEmployment = 11,

        [OrderedDescription("No Income on 1003")]
        NoIncome = 12,

        [OrderedDescription("No Verification of Stated Income, Employment or Assets")]
        NoVerificationStatedIncomeEmploymentAssets = 13,

        [OrderedDescription("No Verification of Stated Income or Assets")]
        NoVerificationStatedIncomeAssets = 14,

        [OrderedDescription("No Verification of Stated Assets")]
        NoVerificationStatedAssets = 15,

        [OrderedDescription("No Verification of Stated Income or Employment")]
        NoVerificationStatedIncomeEmployment = 16,

        [OrderedDescription("No Verification of Stated Income")]
        NoVerificationStatedIncome = 17,

        [OrderedDescription("Verbal Verification of Employment (VVOE)")]
        VerbalVOE = 18,

        [OrderedDescription("One paystub")]
        OnePaystub = 19,

        [OrderedDescription("One paystub and VVOE")]
        OnePaystubAndVerbalVOE = 20,

        [OrderedDescription("One paystub and one W-2 and VVOE or one yr 1040")]
        OnePaystubOneW2VerbalVOE = 21

    }

    public enum PmtInfoType
    {
        InterestRate = 0,
        DueDate = 1,
        PmtNumber = 2,
        PmtAmount = 3,
        Bal = 4
    }

    public enum E_EmplmtStat
    {
        Current = 0, // NOTE: This should be named as Primary instead of Current. At most one Primary per person
        Previous = 1
    }

    public enum E_EmpGroupT
    {
        Current = 0x0001,
        Previous = 0x0002,
        All = Current + Previous,
        Regular = Previous,
        Special = Current
    }

    public enum E_AgentSourceT // where the (loan) agent came from. opm 137883
    {
        NotYetDefined = 0,
        ManuallyEntered = 1,
        CorporateList = 2
    }

    public enum E_AgentRoleT
    {
        Other = 0,
        Appraiser = 1,
        Escrow = 2,

        CreditReport = 3,
        Title = 4,

        MortgageInsurance = 5,

        ListingAgent = 6,

        SellingAgent = 7,
        Builder = 8,

        Underwriter = 9,
        Investor = 10,
        Servicing = 11,

        BuyerAttorney = 12,

        SellerAttorney = 13,

        HomeOwnerInsurance = 14,

        HazardInsurance = 15,

        MarketingLead = 16,
        Seller = 17,
        Surveyor = 18,

        LoanOfficer = 19,
        Bank = 20,
        Lender = 21,
        Processor = 22,

        CallCenterAgent = 23,

        LoanOpener = 24,
        Manager = 25,
        Broker = 26,

        BrokerRep = 27,

        ECOA = 28, // Equal Credit Opportunity Act
        Realtor = 29,
        Mortgagee = 30, // Use in Request for Title form.

        HomeOwnerAssociation = 31, // OPM 2443

        BuyerAgent = 32, // Request by Gordon Shaw @ LON

        ClosingAgent = 33, // OPM 2871

        FairHousingLending = 34, // OPM 4545

        HomeInspection = 35, // OPM 8934

        FloodProvider = 36, // OPM 33365

        [OrderedDescription("Credit Report Agency #2")]
        CreditReportAgency2 = 37, // OPM 40436

        [OrderedDescription("Credit Report Agency #3")]
        CreditReportAgency3 = 38, // OPM 40436
        Shipper = 39, // OPM 44150,

        BrokerProcessor = 40, //Brokr Processor spec
        Trustee = 41, // 12/6/2010 dd - Add for DocuTech export.
        Funder = 42, // OPM 108148 gf start

        [OrderedDescription("Post-Closer")]
        PostCloser = 43,

        Insuring = 44,

        CollateralAgent = 45,

        DocDrawer = 46, // OPM 108148 gf end

        PropertyManagement = 47, // 8/27/2013 GF - OPM 116599

        TitleUnderwriter = 48, // 11/18/2013 SK - OPM 130371

        CreditAuditor = 49, // 11/19/2013 gf - OPM 145015 start

        DisclosureDesk = 50,

        JuniorProcessor = 51,

        JuniorUnderwriter = 52,

        LegalAuditor = 53,

        LoanOfficerAssistant = 54,
        Purchaser = 55,

        [OrderedDescription("QC Compliance")]
        QCCompliance = 56,
        Secondary = 57, // 11/19/2013 gf - OPM 145015 end

        PestInspection = 58,
        Subservicer = 59, // 1/8/2014 gf - opm 130487

        [OrderedDescription("Secondary (External)")]
        ExternalSecondary = 60,

        [OrderedDescription("Post-Closer (External)")]
        ExternalPostCloser = 61,

        LoanPurchasePayee = 62, // 10/13/2014 - OPM 184557

        AppraisalManagementCompany = 63, // 5/5/2015 AV - 212765 Add contact type for Appraisal Management Company

        Referral = 64, // 10/25/17 JK - OPM 462103
    }

    public enum E_sBrokControlledFundT
    {
        LeaveBlank = 0,
        May = 1,
        Will = 2,
        WillNot = 3
    }

    public enum E_sMldsPpmtBaseT
    {
        UnpaidBalance = 0,
        OriginalBalance = 1
    }

    public enum E_sMldsPpmtT
    {
        LeaveBlank = 0,
        Other = 1,
        Penalty = 2,
        NoPenalty = 3
    }

    public enum E_sLT
    {
        Conventional = 0,
        FHA = 1,
        VA = 2,

        [OrderedDescription("USDA/Rural Housing")]
        UsdaRural = 3,

        Other = 4
    }

    public enum E_ConditionChoice_sLT
    {
        Conventional = 0,
        FHA = 1,
        VA = 2,
        UsdaRural = 3,
        Other = 4,
        Any = 100
    }

    public enum E_LpeRunModeT
    {
        NotAllowed = 0,
        OriginalProductOnly_Direct = 1, // don't even need to display pml's steps
        Full = 2
    }

    public enum E_sLienPosT
    {
        [OrderedDescription("1st Mortgage")]
        First = 0,

        [OrderedDescription("2nd Mortgage")]
        Second = 1
    }

    public enum E_sVaLienPosT
    {
        Blank = 0,

        [OrderedDescription("First Realty Mortgage")]
        First = 1,

        [OrderedDescription("Second Realty Mortgage")]
        Second = 2,

        [OrderedDescription("First Chattel Mortgage")]
        FirstChattel = 3,

        Unsecured = 4,

        [OrderedDescription("Other -")]
        Other = 5
    }

    public enum TridLoanPurposeType
    {
        Purchase = 0,
        Refinance = 1,
        Construction = 2,
        HomeEquityLoan = 3
    }

    public enum E_sLPurposeT
    {
        Purchase = 0,

        [OrderedDescription("Refi Rate/Term")]
        Refin = 1,

        [OrderedDescription("Refinance Cashout")]
        RefinCashout = 2,

        [OrderedDescription("Construction")]
        Construct = 3,

        [OrderedDescription("Construction Perm")]
        ConstructPerm = 4,

        [OrderedDescription("Other", Order = 3)]
        Other = 5,

        [OrderedDescription("FHA Streamline Refi", Order = 1)]
        FhaStreamlinedRefinance = 6,

        [OrderedDescription("VA IRRRL", Order = 2)]
        VaIrrrl = 7,

        [OrderedDescription("Home Equity")]
        HomeEquity = 8, // 9/9/2013 dd - OPM 136658 Add Home Equity Option.
    }
    public enum E_sLPurposeForConsumerPortalT
    {
        Purchase = 0,
        Refin = 1,
        RefinCashout = 2,
        HomeEquityLineOfCredit = 80,
        HomeEquityLoan = 81,
        LeaveBlank = 82 // 11/26/2013 dd - 145397

    }


    public enum E_VaPastLT
    {
        Regular = 0,
    }

    public enum E_VaPastLGroupT
    {
        Regular = 0x0001,
        ALL = Regular,
    }

    public enum E_AssetT
    {
        [OrderedDescription("Auto", Order = 0)]
        Auto = 0,

        [OrderedDescription("Bonds", Order = 1)]
        Bonds = 1,

        Business = 2,

        [OrderedDescription("Checking", Order = 4)]
        Checking = 3,

        [OrderedDescription("Gift Funds", Order = 5)]
        GiftFunds = 4,

        LifeInsurance = 5,

        Retirement = 6,

        [OrderedDescription("Savings", Order = 9)]
        Savings = 7,

        [OrderedDescription("Stocks", Order = 11)]
        Stocks = 8,

        [OrderedDescription("Other Non-liquid Asset (furniture, jewelry, etc.)", Order = 14)]
        OtherIlliquidAsset = 9,

        CashDeposit = 10,

        [OrderedDescription("Other Liquid Asset (other bank accounts, etc.)", Order = 15)]
        OtherLiquidAsset = 11,

        [OrderedDescription("Pending Net Sale Proceeds", Order = 13)]
        PendingNetSaleProceedsFromRealEstateAssets = 12, // 5/12/2008 dd - Add to support Loan Prospector integration.

        [OrderedDescription("Gift Of Equity", Order = 6)]
        GiftEquity = 13, // 8/8/2008 dd - OPM 23660 - Add "Gift of Equity" asset type
        // 11/1/10 vm - OPM 29578 - Added assets 14 to 19 to match with DU

        [OrderedDescription("Certificate Of Deposit", Order = 3)]
        CertificateOfDeposit = 14,

        [OrderedDescription("Money Market Fund", Order = 7)]
        MoneyMarketFund = 15,

        [OrderedDescription("Mutual Funds", Order = 8)]
        MutualFunds = 16,

        [OrderedDescription("Secured Borrowed Funds", Order = 10)]
        SecuredBorrowedFundsNotDeposit = 17,

        [OrderedDescription("Bridge Loan", Order = 2)]
        BridgeLoanNotDeposited = 18,

        [OrderedDescription("Trust Funds", Order = 12)]
        TrustFunds = 19,

        [OrderedDescription("Stock Options", Order = 20)]
        StockOptions = 20,

        [OrderedDescription("Employer Assistance", Order = 21)]
        EmployerAssistance = 21,

        [OrderedDescription("Individual Development Account", Order = 22)]
        IndividualDevelopmentAccount = 22,

        [OrderedDescription("Pending Net Sale Proceeds Non-Realestate", Order = 23)]
        ProceedsFromSaleOfNonRealEstateAsset = 23,

        [OrderedDescription("Unsecured Borrowed Funds", Order = 24)]
        ProceedsFromUnsecuredLoan = 24,

        [OrderedDescription("Grant", Order = 25)]
        Grant = 25,

        [OrderedDescription("Rent Credit", Order = 26)]
        LeasePurchaseCredit = 26,

        [OrderedDescription("Sweat Equity", Order = 27)]
        SweatEquity = 27,

        [OrderedDescription("Trade Equity", Order = 28)]
        TradeEquityFromPropertySwap = 28,

        [OrderedDescription("Other Purchase Credit", Order = 29)]
        OtherPurchaseCredit = 29,

        //BE SURE TO SEARCH AND ADD ANY NEW TYPES TO ALL SWITCH STATEMENTS - av 2 2011
        //THERES CODE THAT THROWS EXCEPTION IF IT ENCOUNTERS A NEW TYPE
    }

    public enum E_GiftFundSourceT
    {
        [OrderedDescription(null, Order = 0)]
        Blank,

        [OrderedDescription("Relative", Order = 1)]
        Relative,

        [OrderedDescription("Government", Order = 3)]
        Government,

        [OrderedDescription("Employer", Order = 2)]
        Employer,

        [OrderedDescription("Nonprofit", Order = 4)]
        Nonprofit,

        [OrderedDescription("Other", Order = 5)]
        Other,

        [OrderedDescription("Community Nonprofit", Order = 6)]
        CommunityNonProfit,

        [OrderedDescription("Federal Agency", Order = 7)]
        FederalAgency,

        [OrderedDescription("Local Agency", Order = 8)]
        LocalAgency,

        [OrderedDescription("Religious Nonprofit", Order = 9)]
        ReligiousNonProfit,

        [OrderedDescription("State Agency", Order = 10)]
        StateAgency,

        [OrderedDescription("Unmarried Partner", Order = 11)]
        UnmarriedPartner,

        [OrderedDescription("Seller", Order = 12)]
        Seller,
    }


    /// <summary>
    /// A subset of E_AssetT, it's the set of types where we display them in a list
    /// </summary>
    public enum E_AssetRegularT
    {
        [OrderedDescription("Auto", Order = 0)]
        Auto = E_AssetT.Auto,

        [OrderedDescription("Bonds", Order = 1)]
        Bonds = E_AssetT.Bonds,

        [OrderedDescription("Checking", Order = 4)]
        Checking = E_AssetT.Checking,

        [OrderedDescription("Gift Funds", Order = 5)]
        GiftFunds = E_AssetT.GiftFunds,

        [OrderedDescription("Savings", Order = 9)]
        Savings = E_AssetT.Savings,

        [OrderedDescription("Stocks", Order = 11)]
        Stocks = E_AssetT.Stocks,

        [OrderedDescription("Other Non-liquid Asset (furniture, jewelry, etc.)", Order = 14)]
        OtherIlliquidAsset = E_AssetT.OtherIlliquidAsset,

        [OrderedDescription("Other Liquid Asset (other bank accounts, etc.)", Order = 15)]
        OtherLiquidAsset = E_AssetT.OtherLiquidAsset,

        [OrderedDescription("Pending Net Sale Proceeds", Order = 13)]
        PendingNetSaleProceedsFromRealEstateAssets = E_AssetT.PendingNetSaleProceedsFromRealEstateAssets,

        [OrderedDescription("Gift Of Equity", Order = 6)]
        GiftEquity = E_AssetT.GiftEquity,

        [OrderedDescription("Certificate Of Deposit", Order = 3)]
        CertificateOfDeposit = E_AssetT.CertificateOfDeposit,

        [OrderedDescription("Money Market Fund", Order = 7)]
        MoneyMarketFund = E_AssetT.MoneyMarketFund,

        [OrderedDescription("Mutual Funds", Order = 8)]
        MutualFunds = E_AssetT.MutualFunds,

        [OrderedDescription("Secured Borrowed Funds", Order = 10)]
        SecuredBorrowedFundsNotDeposit = E_AssetT.SecuredBorrowedFundsNotDeposit,

        [OrderedDescription("Bridge Loan", Order = 2)]
        BridgeLoanNotDeposited = E_AssetT.BridgeLoanNotDeposited,

        [OrderedDescription("Trust Funds", Order = 12)]
        TrustFunds = E_AssetT.TrustFunds,

        [OrderedDescription("Stock Options", Order = 16)]
        StockOptions = E_AssetT.StockOptions,

        [OrderedDescription("Employer Assistance", Order = 17)]
        EmployerAssistance = E_AssetT.EmployerAssistance,

        [OrderedDescription("Individual Development Account", Order = 18)]
        IndividualDevelopmentAccount = E_AssetT.IndividualDevelopmentAccount,

        [OrderedDescription("Pending Net Sale Proceeds Non-Real Estate", Order = 19)]
        ProceedsFromSaleOfNonRealEstateAsset = E_AssetT.ProceedsFromSaleOfNonRealEstateAsset,

        [OrderedDescription("Unsecured Borrowed Funds", Order = 20)]
        ProceedsFromUnsecuredLoan = E_AssetT.ProceedsFromUnsecuredLoan,

        [OrderedDescription("Grant", Order = 21)]
        Grant = E_AssetT.Grant,

        [OrderedDescription("Rent Credit", Order = 22)]
        LeasePurchaseCredit = E_AssetT.LeasePurchaseCredit,

        [OrderedDescription("Sweat Equity", Order = 23)]
        SweatEquity = E_AssetT.SweatEquity,

        [OrderedDescription("Trade Equity", Order = 24)]
        TradeEquityFromPropertySwap = E_AssetT.TradeEquityFromPropertySwap,

        [OrderedDescription("Other Purchase Credit", Order = 25)]
        OtherPurchaseCredit = E_AssetT.OtherPurchaseCredit,

        //BE SURE TO SEARCH AND ADD ANY NEW TYPES TO ALL SWITCH STATEMENTS - av 2 2011
        //THERES CODE THAT THROWS EXCEPTION IF IT ENCOUNTERS A NEW TYPE
    }


    public enum E_AssetSpecialT
    {
        Business = E_AssetT.Business,
        LifeInsurance = E_AssetT.LifeInsurance,
        Retirement = E_AssetT.Retirement,
        CashDeposit = E_AssetT.CashDeposit,
        OtherIlliquidAsset = E_AssetT.OtherIlliquidAsset,
        OtherLiquidAsset = E_AssetT.OtherLiquidAsset
    }

    /// <summary>
    /// A subset of E_AssetT
    /// </summary>

    public enum E_AssetCashDepositT
    {
        CashDeposit1 = 0,
        CashDeposit2 = 1,
        MAX_VAL = CashDeposit2
    }

    public enum E_AssetGroupT
    {
        Auto = 0x0001,
        Bonds = 0x0002,			// liquid
        Business = 0x0004,
        Checking = 0x0008,		// liquid
        GiftFunds = 0x0010,		// liquid
        LifeInsurance = 0x0020, // liquid
        Retirement = 0x0040,
        Savings = 0x0080,		// liquid
        Stocks = 0x0100,		// liquid
        OtherIlliquidAsset = 0x0200,
        CashDeposit = 0x0400,	// liquid
        OtherLiquidAsset = 0x0800, // liquid
        PendingNetSaleProceedsFromRealEstateAssets = 0x1000,
        GiftEquity = 0x2000,
        CertificateOfDeposit = 0x4000,
        MoneyMarketFund = 0x8000,
        MutualFunds = 0x10000,
        SecuredBorrowedFundsNotDeposit = 0x20000,
        BridgeLoanNotDposited = 0x40000,
        TrustFunds = 0x80000,
        StockOptions = 0x100000,
        EmployerAssistance = 0x200000,
        IndividualDevelopmentAccount = 0x400000,
        ProceedsFromSaleOfNonRealEstateAsset = 0x800000,
        ProceedsFromUnsecuredLoan = 0x1000000,
        Grant = 0x2000000,
        LeasePurchaseCredit = 0x4000000,
        SweatEquity = 0x8000000,
        TradeEquityFromPropertySwap = 0x10000000,

        CheckingSavings = Checking | Savings,

        Other = OtherIlliquidAsset | OtherLiquidAsset,

        Liquid = Bonds | Checking | GiftFunds | LifeInsurance | Savings | Stocks | CashDeposit | OtherLiquidAsset | PendingNetSaleProceedsFromRealEstateAssets |
            CertificateOfDeposit | MoneyMarketFund | MutualFunds | SecuredBorrowedFundsNotDeposit | BridgeLoanNotDposited | TrustFunds |
            ProceedsFromSaleOfNonRealEstateAsset | ProceedsFromUnsecuredLoan | EmployerAssistance | StockOptions | IndividualDevelopmentAccount | Grant,

        Illiquid = Auto | Business | Retirement | OtherIlliquidAsset,

        Regular = Auto | Bonds | Checking | GiftFunds | Savings | Stocks | OtherIlliquidAsset | OtherLiquidAsset | GiftEquity |
            StockOptions | EmployerAssistance | IndividualDevelopmentAccount | ProceedsFromSaleOfNonRealEstateAsset | ProceedsFromUnsecuredLoan |
            Grant | LeasePurchaseCredit | SweatEquity | TradeEquityFromPropertySwap,

        Special = CashDeposit | LifeInsurance | Retirement | Business,

        DontNeedVOD = Auto | Bonds | Business | GiftFunds | LifeInsurance | Retirement | Stocks | OtherIlliquidAsset | CashDeposit |
            ProceedsFromSaleOfNonRealEstateAsset | ProceedsFromUnsecuredLoan | EmployerAssistance | LeasePurchaseCredit | SweatEquity | TradeEquityFromPropertySwap | Grant,

        All = Auto | Bonds | Business | Checking | GiftFunds | LifeInsurance | Retirement | Savings |
            Stocks | OtherIlliquidAsset | CashDeposit | OtherLiquidAsset | PendingNetSaleProceedsFromRealEstateAssets | GiftEquity | Liquid
    }

    public enum E_sFinMethT
    {
        [OrderedDescription("Fixed Rate")]
        Fixed = 0,

        ARM = 1,

        Graduated = 2
    }

    public enum E_sProdFinMethFilterT
    {
        All = 0,
        FixedOnly = 1,
        ArmOnly = 2
    }

    public enum E_sAssumeLT
    {
        LeaveBlank = 0,
        MayNot = 1,
        May = 2,
        MaySubjectToCondition = 3
    }

    public enum E_sProMInsDurationT
    {
        LTV = 0,
        NumberOfMonths = 1,
        Midpoint = 2
    }

    // 32441
    public enum E_sProMInsFinancedFieldChangedT
    {
        FinancedAmount = 0,
        PaidInCash = 1,
        NoChange = 2
    }
    public enum E_OrigiantorCompenationLevelT
    {
        OriginatingCompany = 0,
        IndividualUser
    };

    public enum E_PercentBaseT
    {
        LoanAmount = 0,

        [OrderedDescription("Purchase Price")]
        SalesPrice = 1,

        AppraisalValue = 2,

        OriginalCost = 3,

        TotalLoanAmount = 4,

        AverageOutstandingBalance = 5, // OPM 32441

        [OrderedDescription("All YSP")]
        AllYSP = 6, // 2/10/2011 dd - This enum add for handle sGfeOriginatorCompFBaseT

        [OrderedDescription("Declining Renewals - Monthly")]
        DecliningRenewalsMonthly = 7, // 8/30/2011 dd - OPM 70541

        [OrderedDescription("Declining Renewals - Annually")]
        DecliningRenewalsAnnually = 8, // 8/30/2011 dd - OPM 70541
    }

    public enum E_PercentBaseTForConstruction
    {
        LoanAmount = E_PercentBaseT.LoanAmount,

        [OrderedDescription("Purchase Price")]
        SalesPrice = E_PercentBaseT.SalesPrice,

        AppraisalValue = E_PercentBaseT.AppraisalValue,

        OriginalCost = E_PercentBaseT.OriginalCost,

        TotalLoanAmount = E_PercentBaseT.TotalLoanAmount,
    }

    public enum E_FloodCertificationDeterminationT
    {
        SingleChargeOrLifeOfLoan = 0,
        InitialFee = 1
    }

    public enum E_FloodCertificationPreparerT
    {
        Other = 0,
        ChicagoTitle = 1,
        Fidelity = 2,
        FirstAm_Corelogic = 3,
        Leretta = 4,
        USFlood = 5,
        LandSafe = 6,
        // 10/16/2015 BS - Case 228700. Update LPS to ServiceLink
        ServiceLink = 7,
        MDA = 8,
        CertifiedCredit = 9,
        CBCInnovis_FZDS = 10, // 11/20/2015 BS - CAse 228701 Update CBCInnovis to CBCInnovis - FZDS
        DataQuick = 11,
        KrollFactualData = 12
    }

    public enum E_sStatusLoanCategoryT
    {
        Loan_Active = 0,
        Loan_Inactive = 1,
        Not_Valid_Loan_Status = 2
    }

    /// <summary>
    /// When adding a new loan status to this enumeration, make sure to add the corresponding
    /// date field to <code>ConstApp.AlwaysAuditFieldIds</code> per OPM 236046.
    /// </summary>
    public enum E_sStatusT
    {
        Loan_Open = 0,

        Loan_Prequal = 1,

        Loan_Preapproval = 2,

        Loan_Registered = 3,

        Loan_Approved = 4,

        Loan_Docs = 5, // 11/22/2010 dd - OPM 30150 - Rename to Docs Out

        Loan_Funded = 6,

        Loan_OnHold = 7,

        Loan_Suspended = 8,

        Loan_Canceled = 9,

        Loan_Rejected = 10,  // sk 1/6/2014 opm 145251 renamed to Loan Denied

        Loan_Closed = 11,

        Lead_New = 12,

        Loan_Underwriting = 13,

        Loan_WebConsumer = 14, // Loan file created from scratch by a borrower (via consumer website) will have this status.

        Lead_Canceled = 15,

        Lead_Declined = 16,

        Lead_Other = 17,

        Loan_Other = 18,

        Loan_Recorded = 19,

        Loan_Shipped = 20,

        Loan_ClearToClose = 21, // 7/22/2009 dd - OPM 5663

        Loan_Processing = 22, // 11/22/2010 dd - OPM 30150

        Loan_FinalUnderwriting = 23, // 11/22/2010 dd - OPM 30150

        Loan_DocsBack = 24, // 11/22/2010 dd - OPM 30150

        Loan_FundingConditions = 25, // 11/22/2010 dd - OPM 30150

        Loan_FinalDocs = 26, // 11/22/2010 dd - OPM 30150

        Loan_LoanPurchased = 27, // 11/22/2010 dd - OPM 30150 // sk 1/6/2014 opm 145251 renamed to Loan Sold

        Loan_LoanSubmitted = 28, // 4/20/2012 dd - OPM 50130


        Loan_PreProcessing = 29,    // start sk 1/6/2014 opm 145251

        Loan_DocumentCheck = 30,

        Loan_DocumentCheckFailed = 31,

        Loan_PreUnderwriting = 32,

        Loan_ConditionReview = 33,

        Loan_PreDocQC = 34,

        Loan_DocsOrdered = 35,

        Loan_DocsDrawn = 36,

        Loan_InvestorConditions = 37,       // this is "promoting the date to a status". tied to sSuspendedByInvestorD

        Loan_InvestorConditionsSent = 38,   // this is "promoting the date to a status". tied to sCondSentToInvestorD

        Loan_ReadyForSale = 39,

        Loan_SubmittedForPurchaseReview = 40,

        Loan_InPurchaseReview = 41,

        Loan_PrePurchaseConditions = 42,

        Loan_SubmittedForFinalPurchaseReview = 43,

        Loan_InFinalPurchaseReview = 44,

        Loan_ClearToPurchase = 45,

        Loan_Purchased = 46,        // don't confuse with the old 		Loan_LoanPurchased, which is now Loan Sold in the UI.



        Loan_CounterOffer = 47,

        Loan_Withdrawn = 48,

        Loan_Archived = 49      // end sk 1/6/2014 opm 145251

        // 12/15/2010 dd - NOTE: READ - When adding new status enum please go and update the Stored procedure "RetrieveMCTData" and "RetrieveSIData"
        // 1/7/2014 sk - NOTE: READ  - Update LosUtils.cs :: IsStatusLead when adding new statuses.
        // 1/7/2014 NOTE: READ consider if it needs an audit.  (LFF.cs > RecordAuditEvent)
    }

    public enum E_sSecondStatusT
    {
        None = 0,
        Loan_Funded = 1,
        Loan_Purchased = 2,
        Loan_Disbursed = 3,
        Loan_Reconciled = 4
    }

    public enum E_sLpProductT
    {
        LeaveBlank = 0,
        Conforming = 1,
        Subprime = 2,
        Rural = 3,
        Fha = 4,
        CES = 5,
        NonConforming = 6,
        ManualUW = 7,
        HomePath = 8,
        VA = 9,
        AltA = 10,
        Heloc = 11,
        Second = 12,
        FhlmcReliefRefi = 13,
        Fha203k = 14,
        Flex97 = 15,
        Alt97 = 16,
        Fha203ks = 17,
        HomePathRenovation = 18,
        Fha100Down = 19,
        Fha203k100Down = 20,
        Fha203ks100Down = 21,
        MyCommunity = 22,
        HomePossible = 23,
        CalHfaFha = 24,
        CalHfaChdap = 25,
        Fha1xClose = 26,
        ChfaConforming = 27,
        ChfaFha = 28,
        ChfaFha203k = 29,
        ChfaVa = 30,
        HomestyleRenovation = 31,
        GNMA = 32,
        ConformingVOD = 33,
        ConformingShortSale = 34,
        ConformingPropertyFlip = 35,
        ConformingNonOccupyingCoBorrower = 36,
        Conforming510FinancedProperties = 37,
        HomeEquity = 38,
        FhaMcc = 39,
        VAMcc = 40,
        ConformingTVLB = 41,
        FhaTVLB = 42,
        VaTVLB = 43,
        FhaRepairEscrow = 44, // 9/18/2013 dd - OPM 136671
        FhaPurchase90 = 45, // 9/18/2013 dd - OPM 136671
        ConstructionPerm = 46, // 11/7/2013 AV - 141843 Add new Loan Product Type: "CONSTRUCTION PERM"
        NonConfLaguna = 47, // OPM 145074. JMAC-specific type
        NonConfManhattan = 48, // OPM 145074. JMAC-specific type
        ConstructionI = 49, // 1/20/2014 gf - opm 149671
        ConstructionII = 50, // 1/20/2014 gf - opm 149671
        Re = 51, // 1/20/2014 gf -  opm 149671
        TdhcaBond = 52,  // OPM 171138 av
        TsahcBond = 53, // OPM 171138 av
        AIO = 54, // OPM 169704 dd
        ConformingVoeOnly = 55, // OPM 173294 gf
        JMACDirect = 56,   // OPM 177704 gf
        ImperialARM = 57,  // OPM 177704 gf
        PacificJumbo = 58, // OPM 177704 gf
        WSHFCBond = 59, // OPM 178019 gf
        FlfhcBond = 60, // 5/12/2014 gf - opm 181229
        SonomaJumbo = 61, // 6/18/2014 AV - Add Custom Loan Product Type SONOMA JUMBO for JMAC
        MalibuJumbo = 62, // 8/19/2014 AV - 189751 Add New Custom Loan Product Types for JMAC
        SunsetJumbo = 63, // 8/19/2014 AV - 189751 Add New Custom Loan Product Types for JMAC
        ConformingFTHB = 64, // 11/4/2014 AV - 195895 Add new loan product type for WHEDA
        MalibuExpandedT1 = 65, // 11/4/2014 AV - 195975 Add Custom Loan Product Types for JMAC
        MalibuExpandedT2 = 66, // 11/4/2014 AV - 195975 Add Custom Loan Product Types for JMAC
        VHDA = 67, //// 11/24/2014 AV - 197176 Add Custom Loan Product Types for Fidelity
        GADream = 68, // 11/24/2014 AV - 197176 Add Custom Loan Product Types for Fidelity
        LakeMichigan = 69, // 11/24/2014 AV - 197176 Add Custom Loan Product Types for Fidelity
        AlabamaStepUp = 70, // 11/24/2014 AV - 197176 Add Custom Loan Product Types for Fidelity
        CarmelJumbo = 71, //11/24/2014 AV - 197335 Add Custom LPT for JMAC
        Supersaver = 72,
        IMCUSpecialtyProducts = 73,
        ZeroDollarClosingCostOption = 74,
        NinetySevenPercent = 75,
        PmiWaiver = 76,
        VeniceNonQM = 77,
        Advantage = 78, // sk opm 203665 "for sharonview PML0256"
        Path = 79,
        PlatinumSapphire = 80,
        HomeInFive = 81,
        BelmontNonQM = 82, // ejm opm 212790 Add Loan product Type "BELMON NON-QM"
        Modifications = 83,
        AHFABond = 84,   // OPM 214139
        ConformingFthbBondProgram = 85,
        Portfolio = 86, //em opm 218067
        FHAPath = 87, //IR - OPM 219488 separate LPT for Mountain West Financial Path Conforming and Path FHA. Reason is that they should not be considered together in QM calculations.
        ConformingPath = 88,
        SonymaAchievingTheDream = 89,   // ejm opm 221013
        SonymaLowInterestRate = 90,     // ejm opm 221013
        MassHousing = 91,               // ejm opm 221013
        ZeroInterestProgram = 92,
        CalHFA = 93, // je - opm 223614
        CalHfaConf = 94,
        ConformingFthbVetBondPrg = 95, // ejm - opm 231936
        HOMEREADY = 96, // ejm - opm 231760
        Jumbo = 97, // je - opm 233932
        CashOut_90PerCent = 98, // je - opm 233932
        Bond = 99, // ml - opm 235582
        ConformingCEMA = 100, // ejm - opm 237297
        ConstructionEndLoan = 101, // ejm - opm 237297
        ConstructionDraw = 102, // ejm - opm 237297
        YESMortgage = 103, // ejm - opm 237622
        WESLENDSELECT = 104, // ml - opm 239061
        ConventionalBond = 105, // ml - opm 240716
        FHABond = 106,
        VABond = 107,
        USDABond = 108, // end opm 240716
        Platinum = 109, // ml - opm 242286
        Sapphire = 110, // ml - opm 242286
        Va1xClose = 111, // gf - opm 244314
        COMBO = 112, // ml - opm 245355
        UsdaStreamlineAssistRefi = 113, // ml - opm 244201
        FHAEEM = 114, // ejm opm 246231
        VAEEM = 115, // ejm opm 246231
        ExpandedPlus = 116, // jl - opm 246584
        JumboFlex = 117, // jl - opm 247111
        Fha203kBond = 118, // jl - opm 245595
        AltAPlatinum = 119, // jl - opm 247338
        AltADiamond = 120,
        AltADiamondPlus = 121,
        AltAUltraPrime = 122,
        ConvMcc = 123,
        NonconfNewport = 124,
        Fha203h = 125,
        DelegatedJumbo = 126, // For Service First PML0223 - OPM 309843
        NonprimeBridgeResidential = 127,
        NonprimeBridgeCommercial = 128,
        NonprimeBusinessResidential = 129,
        NonprimeBusinessCommercial = 130,
        NonprimeConsumer = 131,
        HomestyleEnergy = 132,
        ServiceRetained = 133,
        AltASilver = 134,
        SixMoSeasoningVa = 135,
        ConvMedicalProfessional = 136,
        _1ServiceRetained = 137,
        HomePossAdvPlus1PctGrant = 138,
        HomePossAdvPlus2PctGrant = 139,
        HomestyleEnergyImprovements = 140,
        ConformingDeluxe = 141,
        Section184 = 142,
        VaRenovation = 143,
        ConfFthbValorBond = 144,
        CalhfaEem = 145,
        PiggybackCombo = 146,
        EdgeMortgage = 147,
        Usda1xClose = 148,
        HomeFront = 149,
        UsdaRepairEscrow = 150,
        ElitePlus = 151,
        Tx50A6 = 152,       // je - OPM 469459
        Tx50A4 = 153,       // je - OPM 469459
        DpaAdvantage = 154,  // je - OPM 469459
        HardmoneyCommercial = 155,  // je - OPM 470079
        HardmoneyResidential = 156, // je - OPM 470079
        HardmoneyBackEastResidential = 157, // je - OPM 470079
        AltABusinessResidential = 158,   // je - OPM 470079
        DpaAdvantage203k = 159, // jk - OPM 470453
        DpaAdvantage203ks = 160, // jk - OPM 470453
        DpaAdvantageRepairEscrow = 161, // jk - OPM 470453
        TxA6Fhlmc = 162, // jk - OPM 470453
        TxA6Fnma = 163, // jk - OPM 470453
        TxA4Fhlmc = 164, // jk - OPM 470453
        TxA4Fnma = 165, // jk - OPM 470453
        HomeOne = 166, // OPM 470586 start
        Pivot = 167,
        Simple = 168, // OPM 470586 end
        HbBiscayne = 169,
        LotLoan = 170,
        Renovation = 171,
        HighLtvRefi = 172,
        EnhancedReliefRefi = 173,
        FthbValorHfaPreferred = 174,
        HfaPreferred = 175,
        FhaDpaOtc = 176,
        Brokered = 177
    }

    public enum E_sPrepmtRefundT
    {
        LeaveBlank = 0,
        WillNot = 1,
        May = 2
    }

    public enum E_sPrepmtPenaltyT
    {
        LeaveBlank = 0,
        WillNot = 1,
        May = 2
    }

    public enum E_sRAdjRoundT
    {
        [OrderedDescription("normal")]
        Normal = 0,

        [OrderedDescription("up")]
        Up = 1,

        [OrderedDescription("down")]
        Down = 2
    }

    public enum E_sRAdjFloorBaseT
    {
        Margin = 0,

        StartRate = 1,

        [OrderedDescription("0%")]
        ZeroPercent = 2
    }

    public enum E_sEstateHeldT
    {
        FeeSimple = 0,

        LeaseHold = 1
    }

    public enum NativeAmericanLandsT
    {
        Blank = 0,
        FeeSimple = 1,
        IndividualTrustLand = 2,
        TribalTrustLandOnReservation = 3,
        TribalTrustLandOffReservation = 4,
        AlaskaNativeCorporationLand = 5,
    }

    public enum TrustClassificationT
    {
        Blank = 0,
        LivingTrust = 1,
        LandTrust = 2
    }

    public enum E_sVaEstateHeldT
    {
        Blank = 0,

        FeeSimple = 1,

        LeaseHold = 2,

        Other = 3
    }

    public enum E_aBAddrT
    {
        [OrderedDescription("Own", Order = 1)]
        Own = 0,

        [OrderedDescription("Rent", Order = 2)]
        Rent = 1,

        [OrderedDescription(null, Order = 0)]
        LeaveBlank = 2,

        [OrderedDescription("Living Rent Free", Order = 3)]
        LivingRentFree = 3
    }

    public enum E_aTypeT
    {

        Individual = 0,

        [OrderedDescription("Co-Signer")]
        CoSigner = 1,

        TitleOnly = 2,

        [OrderedDescription("Non-Title Spouse")]
        NonTitleSpouse = 3,
        CurrentTitleOnly = 4
    }

    public enum E_aBMaritalStatT
    {
        [OrderedDescription("Married", Order = 1)]
        Married = 0,

        [OrderedDescription("Not Married", Order = 2)]
        NotMarried = 1,

        [OrderedDescription("Separated", Order = 3)]
        Separated = 2,

        [OrderedDescription(null, Order = 0)]
        LeaveBlank = 3
    }

    public enum E_aCMaritalStatT
    {
        Married = 0,
        NotMarried = 1,
        Separated = 2,
        [OrderedDescription("", Order = 0)]
        LeaveBlank = 3,
    }

    public enum E_aBPrev1AddrT
    {
        Own = 0,
        Rent = 1,
        LeaveBlank = 2,
        LivingRentFree = 3
    }

    public enum E_aBPrev2AddrT
    {
        Own = 0,
        Rent = 1,
        LeaveBlank = 2,
        LivingRentFree = 3
    }

    public enum E_aCAddrT
    {
        Own = 0,
        Rent = 1,
        LeaveBlank = 2,
        LivingRentFree = 3
    }

    public enum E_aCPrev1AddrT
    {
        Own = 0,
        Rent = 1,
        LeaveBlank = 2,
        LivingRentFree = 3
    }

    public enum E_aCPrev2AddrT
    {
        Own = 0,
        Rent = 1,
        LeaveBlank = 2,
        LivingRentFree = 3
    }

    public enum E_aAddrMailSourceT
    {
        PresentAddress = 0,

        SubjectPropertyAddress = 1,

        Other = 2
    }

    public enum E_aAddrPostSourceT
    {
        PresentAddress = 0,

        MailingAddress = 1,

        SubjectPropertyAddress = 2,

        Other = 3
    }

    public enum E_aOccT
    {
        [Description("Primary Residence")]
        PrimaryResidence = 0,

        [Description("Secondary Residence")]
        SecondaryResidence = 1,

        [Description("Investment")]
        Investment = 2
    }

    public enum E_sOccT
    {
        PrimaryResidence = 0,

        SecondaryResidence = 1,

        Investment = 2
    }

    public enum E_aBDecPastOwnedPropT
    {
        Empty = 0,
        [OrderedDescription("Principal Residence")]
        PR = 1, // Own-principal residence
        [OrderedDescription("Second Home")]
        SH = 2,
        [OrderedDescription("Investment Property")]
        IP = 3,
        [OrderedDescription("FHA Secondary Residence")]
        SR = 4
    }

    public enum E_aCDecPastOwnedPropT
    {
        Empty = 0,
        [OrderedDescription("Principal Residence")]
        PR = 1, // Own-principal residence
        [OrderedDescription("Second Home")]
        SH = 2,
        [OrderedDescription("Investment Property")]
        IP = 3,
    }

    public enum E_aBDecPastOwnedPropTitleT
    {
        Empty = 0,
        [OrderedDescription("Solely by Yourself")]
        S = 1,
        [OrderedDescription("Jointly with Spouse")]
        SP = 2,
        [OrderedDescription("Jointly with Another Person")]
        O = 3,
    }

    public enum E_aCDecPastOwnedPropTitleT
    {
        Empty = 0,
        [OrderedDescription("Solely by Yourself")]
        S = 1,
        [OrderedDescription("Jointly with Spouse")]
        SP = 2,
        [OrderedDescription("Jointly with Another Person")]
        O = 3,
    }

    public enum E_aBRaceT
    {
        LeaveBlank = 0,
        AmericanIndian = 1,
        Asian = 2, // Asian/Pacific Islander
        Black = 3,
        Hispanic = 4,
        White = 5,
        Other = 6, //Obsolete in year 2004 version of 1003
        NotFurnished = 7,
        NotApplicable = 8
    }

    public enum E_aCRaceT
    {
        LeaveBlank = 0,
        AmericanIndian = 1,
        Asian = 2, // Asian/Pacific Islander
        Black = 3,
        Hispanic = 4,
        White = 5,
        Other = 6, //Obsolete in year 2004 version of 1003
        NotFurnished = 7,
        NotApplicable = 8
    }

    public enum E_aHispanicT
    {
        [OrderedDescription("")]
        LeaveBlank = 0,

        [OrderedDescription("Hispanic or Latino")]
        Hispanic = 1,

        [OrderedDescription("Not Hispanic or Latino")]
        NotHispanic = 2,

        /// <summary>
        /// Both Hispanic and Not Hispanic options are selected.
        /// </summary>
        BothHispanicAndNotHispanic = Hispanic | NotHispanic
    }

    public enum E_aIntrvwrMethodT
    {
        [OrderedDescription("")]
        LeaveBlank = 0,
        [OrderedDescription("face-to-face interview")]
        FaceToFace = 1,
        [OrderedDescription("by mail")]
        ByMail = 2,
        [OrderedDescription("by telephone")]
        ByTelephone = 3,
        [OrderedDescription("by internet")]
        Internet = 4
    }
    public enum E_DebtT
    {
        Alimony = 0,
        Installment = 1,

        [OrderedDescription("Job Expense")]
        JobRelatedExpense = 2,

        Mortgage = 3,
        Open = 4,
        Revolving = 5,
        Other = 6,

        [OrderedDescription("Child Support")]
        ChildSupport = 7,

        Lease = 8,

        [OrderedDescription("Other Expense Liability")]
        OtherExpenseLiability = 9,

        [OrderedDescription("Separate Maintenance")]
        SeparateMaintenance = 10,

        [OrderedDescription("HELOC")]
        Heloc = 11
    }

    /// <summary>
    /// A subset of E_DebtT
    /// </summary>

    public enum E_DebtRegularT
    {
        Installment = E_DebtT.Installment,
        Mortgage = E_DebtT.Mortgage,
        Open = E_DebtT.Open,
        Revolving = E_DebtT.Revolving,
        Other = E_DebtT.Other,
        Lease = E_DebtT.Lease,
        OtherExpenseLiability = E_DebtT.OtherExpenseLiability,
        SeparateMaintenance = E_DebtT.SeparateMaintenance,
        Heloc = E_DebtT.Heloc
    }

    /// <summary>
    /// A subset of E_DebtT
    /// </summary>

    public enum E_DebtSpecialT
    {
        Alimony = E_DebtT.Alimony,
        JobRelatedExpense = E_DebtT.JobRelatedExpense,
        ChildSupport = E_DebtT.ChildSupport
    }

    public enum E_DebtGroupT
    {
        Alimony = 0x0001,
        Installment = 0x0002,
        JobRelatedExpense = 0x0004,
        Mortgage = 0x0008,
        Open = 0x0010,
        Revolving = 0x0020,
        Other = 0x0040,
        ChildSupport = 0x0080,
        Lease = 0x0100,
        OtherExpenseLiability = 0x0200,
        SeparateMaintenance = 0x0400,
        Heloc = 0x0800,
        ALL = Alimony | Installment | JobRelatedExpense | Mortgage | Open | Revolving | Other | ChildSupport | Lease | OtherExpenseLiability | SeparateMaintenance | Heloc,
        Regular = Installment | Mortgage | Open | Revolving | Other | Lease | OtherExpenseLiability | SeparateMaintenance | Heloc,
        Special = Alimony | JobRelatedExpense | ChildSupport
    }

    public enum E_DebtJobExpenseT
    {
        JobExpense1 = 0,
        JobExpense2 = 1,
        MAX_VAL = JobExpense2
    }

    public enum E_sSpImprovTimeFrameT
    {
        [OrderedDescription(null)]
        LeaveBlank = 0,

        [OrderedDescription("made")]
        Made = 1,

        [OrderedDescription("to be made")]
        ToBeMade = 2
    }

    public enum E_sSpT
    {
        LeaveBlank = 0,
        DetachedHousing = 1,
        AttachedHousing = 2,
        Condominium = 3,
        PUD = 4,
        COOP = 5
    }
    public enum E_sLenCommitT
    {
        LeaveBlank = 0,
        Standard = 1,
        Negotiated = 2
    }
    public enum E_sQualIRDeriveT
    {
        NoteRateDependent = 0,

        BoughtDownRate = 1,

        Other = 2
    }

    public enum E_sSpProjClassT
    {
        LeaveBlank = 0,
        AIIICondo = 1,
        BIICondo = 2,
        CICondo = 3,
        EPUD = 4,
        FPUD = 5,
        IIIPUD = 6,
        COOP1 = 7,
        COOP2 = 8,
        TPUD = 9,
        TCoop = 10,
    }

    public enum E_sMOrigT
    {
        LeaveBlank = 0,
        Seller = 1,
        ThirdParty = 2
    }

    public enum E_s1stMOwnerT
    {
        LeaveBlank = 0,

        FannieMae = 1,

        FreddieMac = 2,

        [OrderedDescription("Seller / Other")]
        SellerOrOther = 3
    }

    public enum E_ReOwnerT
    {
        Borrower = 0,
        CoBorrower = 1,
        Joint = 2
    }

    public enum E_AssetOwnerT
    {
        Borrower = 0,
        CoBorrower = 1,
        Joint = 2
    }

    public enum E_LiaOwnerT
    {
        Borrower = 0,

        CoBorrower = 1,

        Joint = 2,
    }

    public enum E_PublicRecordOwnerT
    {
        Borrower = 0,
        CoBorrower = 1,
        Joint = 2
    }

    public enum Ownership
    {
        Borrower = 0,
        Coborrower = 1,
        Joint = 2
    }

    /// <summary>
    /// This option is to indicate how new loan submit from consumer online site
    /// notified a designated manager.
    /// </summary>

    public enum E_ManagerNewLoanEventNotifOptionT
    {
        AlwaysReceiveEmail = 0,
        SometimesReceiveEmail = 1, // Receive email when loan is not assigned, or loan officer doesn't have email.
        DontReceiveEmail = 2
    }

    /// <summary>
    /// This option is to indicate how new loan submit from consumer online site
    /// notified loan officer that is assigned to by consumer.
    /// </summary>

    public enum E_NewOnlineLoanEventNotifOptionT
    {
        ReceiveEmail = 0,
        DontReceiveEmail = 1
    }

    public enum E_TaskRelatedEmailOptionT
    {
        ReceiveEmail = 0,
        DontReceiveEmail = 1
    }

    /// <summary>
    /// This enumerated type is for use with inputrange.cs to set if the min/max
    /// are inclusive or exclusive.
    /// </summary>

    public enum E_clusive
    {
        Inclusive = 0,
        Exclusive = 1,
    }

    public enum E_sLpDPmtT
    {
        /// <summary>
        /// OBSOLETE - DO NOT USE!!! Retired in OPM 242351.
        /// </summary>
        UserSelection = 0,

        PAndI = 1, // Calculate P&I
        InterestOnly = 2,
        QRate = 3, // 10/17/2013 gf - opm 141171
        HELOCAndInterestOnly = 4 // 10/24/2013 dd - OPM 142243
    }

    public enum E_sLpQPmtT
    {
        /// <summary>
        /// OBSOLETE - DO NOT USE!!! Retired in OPM 242351.
        /// </summary>
        UserSelection = 0,

        PAndI = 1, // Calculate P&I
        InterestOnly = 2,
        HELOCAndInterestOnly = 3 // 10/24/2013 dd - OPM 142243

    }

    public enum E_sLtv80TestResultT
    {
        Undefined = 0,
        UnderOrEqual80 = 1,
        Over80 = 2
    }

    public enum E_sOptionArmMinPayOptionT
    {
        TeaserRate = 0,

        [OrderedDescription("Discount from Fully Amortizing Payment")]
        ByDiscountPmt = 1,

        [OrderedDescription("Discount from Note Rate")]
        ByDiscountNoteIR = 2
    }
    public enum E_sFannieCommunityLendingT
    {
        LeaveBlank = 0,
        CommunityHomeBuyer = 1,
        Fannie97 = 2,
        Fannie32 = 3,
        MyCommunityMortgage = 4,
        HFAPreferredRiskSharing = 5,
        HFAPreferred = 6,
        HomeReady = 7
    }
    public enum E_sFannieCommunitySecondsRepaymentStructureT
    {
        LeaveBlank = 0,
        AnyPaymentWithin5Years = 1,
        PaymentsDeferredFullyForgiven = 2,
        PaymentsDeferredNotFullyForgiven = 3
    }

    public enum E_sFinMethMldsT
    {
        Fixed = 0,
        Adjustable = 1,
        InitialFixed = 2,
        InitialAdjustable = 3
    }
    public enum E_sFHA92900LtFinMethT
    {
        Fixed = 0,
        Adjustable = 1,
        InterestRateBuydown = 2
    }

    public enum E_sLoanOfficerRateLockStatusT
    {
        NoRateSelected = 0,
        RateSelected = 1,
        RateLockBroken = 2,
    }

    //opm 25872 fs 03/11/09
    public enum E_sRuleQBCType
    {
        PrimaryBorrower = 0,
        AllBorrowers = 1,
        OnlyCoborrowers = 2,
        Legacy = 3
    }

    // 4/3/2009 dd - OPM 25872 - This enum will indicate how credit parsing work. Whether the credit parsing will only calculate
    // liabilities/public record belong to borrower only, coborrower only or both borrower and coborrower.
    public enum E_aBorrowerCreditModeT
    {
        Both // Will include all tradeline with B, C or J
        , Borrower // Will include tradeline with B or J
        , Coborrower // Will include tradeline with C or J


    }

    public enum E_ReportExtentScopeT
    {
        // Control scope of report result when running.
        Default = 0,
        Assign = 1,
        Branch = 2,
        Broker = 3,
        SpecificLoans = 4,
        Team = 5
    }

    public enum E_TabTypeT
    {
        PipelinePortlet = 0,
        DisclosurePortlet = 1,
        LoanPoolPortlet = 2,
        TradeMasterPortlet = 3,
        MyTasksPortlet = 4,
        RemindersPortlet = 5,
        DisctrackPortlet = 6,
        MyLoanRemindersPortlet = 7,
        //Not necessary once we let clients adjust to new tab system
        UnassignedLoanPortlet = 8,
        MyLeadPortlet = 9,
        UnassignLeadPortlet = 10,
        TestPortlet = 11
    }

    public enum E_sBrokerLockPriceRowLockedT
    {
        Base = 0,
        BrokerBase = 1, // aka. Origintor Base
        BrokerFinal = 2, // aka.  Final Price
        OriginatorPrice = 3

    }
    public enum E_sInvestorLockPriceRowLockedT
    {
        Base = 0,
        Adjusted = 1
    }

    public enum E_ServicingTransactionT
    {
        MonthlyPayment = 0,
        EscrowDisbursement = 1,
        LateFeeCharged = 2
    }

    public enum E_ServicingPaymentT
    {
        InterestOnly = 0,
        FullyAmortizing = 1
    }

    public enum E_RedisclosureMethodT
    {
        LeaveBlank = 0,
        Email = 1,
        Fax = 2,
        InPerson = 3,
        Mail = 4,
        Overnight = 5
    }

    public enum E_aPreFundVoeTypeT
    {
        LeaveBlank = 0,
        Verbal = 1,
        Written = 2
    }

    public enum E_ManufacturedHomeConditionT
    {
        LeaveBlank = 0,
        New = 1,
        Used = 2
    }

    public enum E_ManufacturedHomeCertificateofTitleT
    {
        LeaveBlank = 0,
        HomeShallBeCoveredByCertificateOfTitle = 1,
        TitleCertificateShallBeEliminated = 2,
        TitleCertificateHasBeenEliminated = 3,
        ManufacturersCertificateShallBeEliminated = 4,
        ManufacturersCertificateHasBeenEliminated = 5,
        HomeIsNotCoveredOrCertIsAttached = 6,
        HomeIsNotCoveredOrNotAbleToProduceCert = 7
    }

    public enum E_CooperativeFormOfOwnershipT
    {
        LeaveBlank = 0,
        OwnerInFee = 1,
        GroundTenant = 2
    }

    public enum E_sSpInvestorCurrentLoanT
    {
        [OrderedDescription("Unknown/Other")]
        Unknown = 0,

        FannieMae = 1,

        FreddieMac = 2
    }

    public enum E_LoanProceedsToT
    {
        LeaveBlank = 0,
        Borrower = 1,
        ClosingCompany = 2,
        TitleCompany = 3
    }

    public enum E_TransactionDir
    {
        ReceivedFrom = 0,
        PaidTo = 1
    }

    public enum E_ServicingStatus
    {
        [OrderedDescription("sell")]
        Released = 0,

        [OrderedDescription("retain")]
        Retained = 1
    }

    // OPM 32441
    public enum E_MipFrequency
    {
        SinglePayment = 0,
        AnnualRateXMonths = 1
    }

    public enum E_sInvestorLockCommitmentT
    {
        [OrderedDescription("Best Efforts")]
        BestEfforts = 0,

        Mandatory = 1,
        Hedged = 2,
        Securitized = 3
    }
    public enum E_sNonOccCoBorrOnOrigNoteT
    {
        No = 0,
        Yes = 1,
        Unknown = 2,
        Blank = 3

    }
    public enum E_sH4HOriginationReqMetT
    {
        No = 0,
        Yes = 1,
        Blank = 2
        // Why not a trisate? Because possibly a new value may be needed.
        // Better for keyword to have it start as enum.
    }
    public enum E_aHasFraudConvictionT
    {
        No = 0,
        Yes = 1,
        Unknown = 2,
        Blank = 3
    }

    /// <summary>
    /// Reason for an account being locked.  OPM 246520.
    /// </summary>
    public enum E_AccountLockedReason
    {
        UnsuccessfulLoginAttempts = 0,
        UnsuccessfulPasswordResetAttempts = 1
    }

    public enum E_EDocsEnabledStatusT // To determine how to enable EDocs in PML
    {
        No = 0,
        Yes = 1
        // UseOriginatingCompanySettings = 2 // OPM 46783 - will eventually implement this
    }

    // db - adding for Google integration
    public enum E_FeeT
    {
        Origination = 0,
        CreditOrCharge = 1,
        Appraisal = 2,
        CreditReport = 3,
        LendersInspection = 4,
        MortgageBroker = 5,
        TaxService = 6,
        Processing = 7,
        Underwriting = 8,
        WireTransfer = 9,
        Line813 = 10,
        Line814 = 11,
        Line815 = 12,
        Line816 = 13,
        Line817 = 14,
        Line904 = 15,
        Line906 = 16,
        AggregateAdjustment = 17,
        Escrow = 18,
        DocPrep = 19,
        Notary = 20,
        Attorney = 21,
        LenderTitleIns = 22,
        Line1112 = 23,
        Line1113 = 24,
        Line1114 = 25,
        Line1115 = 26,
        PestInspection = 27,
        Line1303 = 28,
        Line1304 = 29,
        Line1305 = 30,
        Line1306 = 31,
        Line1307 = 32
    }

    public enum E_ConsumerResponseStatusT : byte
    {
        WaitingForConsumer = 0,
        PendingResponse = 1,
        Accepted = 2
    }

    public enum E_eSignBorrowerMode
    {
        Borrower,
        Coborrower,
        Both
    }

    /// <summary>
    /// This enum indicates a LQB collection mode of an object, which 
    /// describes if and how new Lqb collections are accessed.
    /// </summary>
    public enum E_sLqbCollectionT
    {
        /// <summary>
        /// Legacy mode.
        /// </summary>
        Legacy = 0,

        /// <summary>
        /// Use of lqb collections.  This is the basic conversion of the legacy
        /// XML fields into modern/uniform collections.
        /// </summary>
        UseLqbCollections = 1,

        /// <summary>
        /// The income source collection, located at <see cref="CPageData.IncomeSources"/>.
        /// </summary>
        IncomeSourceCollection = 2,

        /// <summary>
        /// The housing history collection, located at <see cref="CPageData.HousingHistoryEntries"/>.
        /// </summary>
        HousingHistoryEntriesCollection = 3,
    }

    public enum E_PmlLoanLevelAccess
    {
        Individual = 0,
        Supervisor = 1,
        Corporate = 2
    }

    public enum E_sTILPDFMode
    {
        FixedRateNoIntOnly,
        FixedRateWithIntOnly,
        ARMNoIntOnly,
        ARMWithIntOnly,
        NegARMOption
    }
    public enum E_LpeTaskAllowedT
    {
        None, // All threads should stop processing
        GetResultNonSSRejectedOnly, // Only GetResult requests that have not been previously rejected.
        All, // Allowed to take anything, including submissions
    }

    public enum E_BranchChannelT
    {
        Blank = 0,
        Retail = 1,
        Wholesale = 2,
        Correspondent = 3,
        Broker = 4
    }

    public enum E_sCorrespondentProcessT
    {
        [OrderedDescription(null)]
        Blank = 0,

        [OrderedDescription("Delegated")]
        Delegated = 1,

        [OrderedDescription("Prior Approved")]
        PriorApproved = 2,

        [OrderedDescription("Mini-Correspondent")]
        MiniCorrespondent = 3,

        [OrderedDescription("Bulk")]
        Bulk = 4,

        [OrderedDescription("Mini-Bulk")]
        MiniBulk = 5
    }

    public enum E_ImageStatus
    {
        NoCachedImagesNotInQueue = 0,
        NoCachedImagesButInQueue = 1,
        HasCachedImages = 2,
        Error = 3
    }

    public enum E_EDocQueueT
    {
        Main = 0,
        PML = 1,
        Fax = 2
    }

    public enum E_EDocOrigin
    {
        LO = 0,
        PML = 1,
        Fax = 2,
        ConsumerPortal = 3
    }

    public enum E_SettlementChargesExportSource
    {
        GFE,
        SETTLEMENT
    }

    //OPM 62448
    public enum E_IdType
    {
        Blank = -1,
        DriversLicense = 0,
        Passport = 1,
        MilitaryId = 2,
        StateId = 3,
        GreenCard = 4,
        ImmigrationCard = 5,
        Visa = 6,
        OtherDocument = 7
    }

    public enum E_TypeOfAppraisal
    {
        None = 0,
        Full = 1,
        DriveBy = 2,
        MarketRentAnalysis = 3
    }

    public enum E_GseRequestAppraisal
    {
        Unset,
        Other,
        Fannie216,
        Fannie1004,
        Fannie1004C,
        Fannie1004D,
        Fannie1007,
        Fannie1025,
        Fannie1073,
        Fannie1075,
        Fannie2000,
        Fannie2000A,
        Fannie2055,
        Fannie2065,
        Fannie2075,
        Fannie2090,
        Fannie2095,
        Freddie70,
        Freddie70B,
        Freddie72,
        Freddie442,
        Freddie465,
        Freddie466,
        Freddie998,
        Freddie1032,
        Freddie1072,
        Freddie2055,
        Freddie2070,
        VA26_1805,
        VA26_8712
    }


    public enum E_DocMagicAuditStatus
    {
        Warning,
        Error
    }
    public enum E_BillingReason
    {
        ClosingDocs,
        LoanPutInBillableStatus
    }
    public enum E_DocMagicDocumentSavingOption : int
    {
        Single = 0,
        Split = 1,
        DocumentCapture = 2
    }

    public enum E_CaseProcessingRequestType
    {
        NewCaseNumberAssignment,
        UpdateAnExistingCase,
        HoldsTracking,
        CaseQuery,
        CAIVRSAuthorization
    }

    public enum E_sSpMonthBuiltT
    {
        Blank = 0,
        January = 1,
        February = 2,
        March = 3,
        April = 4,
        May = 5,
        June = 6,
        July = 7,
        August = 8,
        September = 9,
        October = 10,
        November = 11,
        December = 12
    }

    public enum E_sFHACaseT
    {
        DE_VACRV = 1,
        HUD_VACRV = 2,
        Irregular_HUD = 3,
        Regular_DE = 0,
        Regular_HUD = 4
    }

    public enum E_sFHAConstCodeT
    {
        Unspecified = 0,
        Existing = 1, //- IFF sFHAConstructionT is blank, this is the default value. See the XML Mapping document for further details.
        New = 2,
        Proposed = 3,
        SubstantialRehab = 4,
        UnderConstruction = 5
    }

    public enum E_sFHAProcessingT
    {
        NA = 0,
        REOWithAppraisal = 1,
        REOWithoutAppraisal = 2,
        CoinsuranceConversion = 3,
        CoinsuranceEndorsements = 4,
        MilitarySales = 5
    }

    public enum E_sFHAFinT
    {
        NA = 0,
        Private = 1,
        GNMA = 2,
        FNMA = 3
    }

    public enum E_sFHAADPSpecialProgramT
    {
        NoSpecialProgram = 0,
        IndianLands = 1,
        HawaiianHomelands = 2,
        MilitaryImpactArea = 3,
        LocationWaiver223e = 4
    }

    public enum E_sFHAADPHousingProgramT
    {
        Unspecified = 0,
        FHAStandard203b = 1,
        Condominium203b = 2,
        Improvements203k = 3,
        UrbanRenewal220k = 4,
        Other = 5
    }

    public enum E_sFHAADPPrincipalWriteDownT
    {
        No = 0,
        GreaterThanOrEqualToTenPercent = 1,
        LessThanTenPercent = 2
    }

    public enum E_sFHAPUDCondoT
    {
        Unspecified = 0,
        PUD = 1,
        Condo = 2,
        Subdivision = 3,
        NA = 4,
    }

    public enum E_sFHAPUDSiteCondoT
    {
        NA = 0,
        SiteCondo = 1,
        SpotLot = 2
    }

    public enum E_sFHAComplianceInspectionAssignmentT
    {
        NA = 0,
        Roster = 1,
        Mortgagee = 2,
        LenderSelect = 3

    }

    public enum E_sFHAAgencyT
    {
        Unspecified = 0,
        HUDFHASingleFamily = 1,
        HUDFHATitleI = 2,
        HUDNativeAmericanPrograms = 3,
        USDAFarmServices = 4,
        USDARuralDevelopment = 5,
        USDARuralHousing = 6,
        VeteransAffairs = 7
    }

    // If these values are ever changed, be sure to alter stored procedure
    //     FindPoolIdForBrokerIdAgencyPoolNum
    public enum E_MortgagePoolAgencyT
    {
        LeaveBlank = 0,
        FannieMae = 1,
        FreddieMac = 2,
        GinnieMae = 3
    }

    public enum E_MortgagePoolAmortMethT
    {
        ConcurrentDate = 0,
        InternalReserve = 1
    }

    public enum E_MortgagePoolIndexT
    {
        CMT = 0,
        LIBOR = 1
    }

    public enum E_MortgagePoolCertAndAgreementT
    {
        LimitedSecurityAgreement = 0,
        NoSecurityAgreement = 1
    }

    public enum E_MiProrata
    {
        NoInfo = 0,
        PartialPremium = 1,
        FullMonth = 2
    }

    public enum E_cPricingEngineCreditT
    {
        [OrderedDescription("None", Order = 2)]
        None = 0,

        [OrderedDescription("801 Loan origination fee", Order = 0)]
        _801LoanOriginationFee = 1,

        [OrderedDescription("802 Credit or Charge", Order = 1)]
        _802CreditOrCharge = 2,
    }

    public enum E_cPricingEngineLimitCreditToT
    {
        [OrderedDescription("Origination charges (Box A1)")]
        OriginationCharges = 0,

        [OrderedDescription("Estimated closing cost")]
        EstimatedClosingCosts = 1,

        [OrderedDescription("No limit")]
        NoLimit = 2,
    }

    public enum E_cPricingEngineCostT
    {
        [OrderedDescription("801 Loan origination fee")]
        _801LoanOriginationFee = 0,

        [OrderedDescription("802 Credit or Charge")]
        _802CreditOrCharge = 1,

        None = 3
    }

    public enum E_cTitleInsurancePolicyT
    {

        /// <remarks/>
        Owner = 0,

        /// <remarks/>
        Lender = 1,

        /// <remarks/>
        Simultaneous = 2
    }
    public enum E_TaxLateChargeT
    {
        NoInfo = 0,
        v5PctPI_15day_5min_nomax = 1,
        Fixed_15day = 2,
        v4PctPI_15day_5min_nomax = 3,
        v3PctPI_15day_5min_nomax = 4,
        v2PctPI_15day_5min_nomax = 5,
        v5PctPI_10day_5min_nomax = 6,
        v4PctPI_10day_5min_nomax = 7,
        v3PctPI_10day_5min_nomax = 8,
        v2PctPI_10day_5min_nomax = 9,
        v10PctPI_15day_5min_nomax = 10,
        v4PctPITI_15day_0min_nomax = 11,
        None = 12,
        v2PctPI_15day_0min_nomax = 13,
        v3PctPI_15day_0min_nomax = 14,
        v4PctPI_10day_0min_nomax = 15,
        v4PctPI_15day_0min_nomax = 16,
        v5PctPI_10day_0min_nomax = 17,
        v5PctPI_15day_0min_10max = 18,
        v5PctPI_15day_0min_25max = 19,
        v5PctPI_15day_0min_nomax = 20,
        v6PctPI_15day_0min_nomax = 21,
        v5PctPI_15day_15min_nomax = 22,
        v5PctPI_15day_18min_100max = 23,
        v3PctPITI_15day_0min_nomax = 24,
        v5PctPITI_15day_0min_nomax = 25,
        v5PctPI_10day_0min_15max = 26,
        v1PctPI_15day_0min_nomax = 27,
        v5PctPI_15day_0min_15max = 28
    }
    public enum E_TaxTableTaxT
    {
        LeaveBlank = 0,
        Borough = 1,
        City = 2,
        County = 3,
        FireDist = 4,
        LocalImprovementDist = 5,
        Miscellaneous = 6,
        MunicipalUtilDist = 7,
        School = 8,
        SpecialAssessmentDist = 9,
        Town = 10,
        Township = 11,
        Utility = 12,
        Village = 13,
        WasteFeeDist = 14,
        Water_Irrigation = 15,
        Water_Sewer = 16
    }

    public enum E_TaxTablePaidBy
    {
        NoInfo = 0,
        Borrower = 1,
        Lender = 2,
    }

    public enum E_TableSettlementDescT
    {
        NoInfo = 0,
        Use1008DescT = 1,
        Use1009DescT = 2,
        Use1010DescT = 3,
        Use1011DescT = 4
    }
    public enum E_sGseDeliveryTargetT
    {
        FannieMae = 0,
        FreddieMac = 1,
        GinnieMae = 2,
        Other_None = 3,
    }
    public enum E_sGseDeliveryPayeeT
    {
        Lender = 0,
        WarehouseLender = 1
    }

    //The numbers in this enum are what we get from FCT.
    public enum E_FCTDocType
    {
        None = 0,
        Other = 1,
        FinalHUD = 2,
        PreliminaryHUD = 3,
        Deed = 5,
        Contract = 7,
        TaxCertificateOrInformation = 9,
        PowerOfAttorney = 11,
        BuyerClosingStatement = 12,
        FinalClosingPackage = 16,
        TitleOrder = 20,
        SellerClosingStatement = 21,
        IncomingEMail = 28,
        Commitment = 99,
        PolicyPackage = 103,
        PayoffReport = 992,
        CombinedClosingStatement = 993,
        SigningConfirmation = 995,
        CurativeRequirementsOverview = 1000,
        GuaranteedRateCertificate = 1001,
        SignedClosingDocuments = 1002,
        CPL = 1003,
        Survey = 1004,
        WireInstructions = 1005,
        TitleCommitmentPackage = 1006,
        EAndO = 1007,
        Subordination = 1009,
        Loan1003 = 1012,
        ApprovedFinalHUD = 1014,
        DriverLicense = 1015,
        LenderDocumentPackage = 1016,
        LenderInstructions = 1017,
        BorrowerIDs = 1027,
        SingleAmendedDocumentforClosing = 1030,
        WireInstructionsFCT = 1031,
        WireInstructionsFlorida = 1032,
        WireInstructionsTexas = 1033,
        HOIBinder = 1034
    }

    public enum E_InsPolicyPaymentTypeT
    {
        NonEscrowed,
        Escrowed
    }

    public enum E_LomaLomrT
    {
        None,
        LOMA,
        LOMR
    }

    public enum E_ContactT
    {
        Work = 1,
        Home = 2,
        WorkFax = 4,
        Mobile = 5
    }

    public enum E_TaxTableTaxItemParcelT
    {
        RealETx = 0,
        SchoolTx = 1,
        FirstDDLTx = 2,
        SecondDDLTx = 3
    }

    public enum E_InsuranceT
    {
        MortgageInsurance = 0,
        HazardInsurance = 1,
        FloodInsurance = 2,
        WindstormInsurance = 3,
        CondoHO6Insurance = 4
    }

    public enum E_MailComponent
    {
        City = 0,
        State = 1,
        Street = 2,
        Zip = 3
    }

    public enum E_SellerCreditT
    {
        ZeroPct = 0,
        OnePct = 1,
        TwoPct = 2,
        ThreePct = 3,
        FourPct = 4,
        FivePct = 5,
        SixPct = 6
    }

    public enum E_sProdConvMIOptionT
    {
        BorrPaidMonPrem = 0,
        BorrPaidSinglePrem = 1,
        BorrPaidSplitPrem = 2,
        LendPaidSinglePrem = 3,
        NoMI = 4,
        Blank = 5
    }

    public enum E_sConvSplitMIRT
    {
        Blank = 0,
        PointFivePercent = 1,
        PointSevenFivePercent = 2,
        OnePercent = 3,
        OnePointTwoFivePercent = 4,
        OnePointFivePercent = 5,
        OnePointSevenFivePercent = 6,
        TwoPercent = 7,
        TwoPointTwoFivePercent = 8
    }

    public enum E_aVServiceT
    {
        Blank = 0,
        Veteran = 1,
        ReservistNationalGuard = 2
    }

    public enum E_aVEnt
    {
        Blank = 0,
        FirstTimeUse = 1,
        SecondAndSubsequentUse = 2
    }

    public enum E_CustomPmlFieldType
    {
        Blank = 0,
        Dropdown = 1,
        NumericGeneric = 2,
        NumericMoney = 3,
        NumericPercent = 4
    }

    public enum E_sTitleInsuranceCostT
    {
        EstimateHighest = 0,
        UseIntegration = 1,
        Manual = 2,
        ManualTitleRecTrans = 3
    }

    public enum E_sDisbursementMethodT
    {
        Blank = 0,
        Wire = 1,
        Check = 2,
        Hold = 3
    }

    public enum QualRateCalculationT
    {
        [OrderedDescription("Flat Value")]
        FlatValue = 0,

        [OrderedDescription("Max of")]
        MaxOf = 1
    }

    public enum QualRateCalculationFieldT
    {
        [OrderedDescription("LPE Upload Value")]
        LpeUploadValue = 0,

        [OrderedDescription("Note Rate")]
        NoteRate = 1,

        [OrderedDescription("Fully Indexed Rate")]
        FullyIndexedRate = 2,

        Index = 3,
        Margin = 4
    }

    public enum QualTermCalculationType
    {
        Standard = 0,
        Amortizing = 1,
        InterestOnly = 2,
        Manual = 3
    }

    public enum E_MortgageLoanT
    {
        [OrderedDescription("Government (FHA/VA/RHS)")]
        Government_FHA_VA_RHS = 0,

        PrimeConforming = 1,

        [OrderedDescription("Prime Non-Conforming")]
        PrimeNonConforming = 2,

        [OrderedDescription("Other 1-4 Unit Residential")]
        Other1To4UnitResidential = 3,

        [OrderedDescription("Closed-End Second")]
        ClosedEndSecond = 4,

        FundedHELOC = 5,

        Construction = 6
    }
    public enum E_JumboT
    {
        [OrderedDescription("Non-Jumbo")]
        NonJumbo = 0,

        Jumbo = 1
    }
    public enum E_DocumentationT
    {
        FullDoc = 0,

        AltDoc = 1
    }
    public enum E_NMLSLoanPurposeT
    {
        Purchase = 0,

        [OrderedDescription("Refinance Rate-Term")]
        RefinanceRateTerm = 1,

        [OrderedDescription("Refinance Cash-Out Refinance")]
        RefinanceCashOutRefinance = 2,

        RefinanceRestructure = 3,

        [OrderedDescription("Refinance Other/Unknown")]
        RefinanceOtherUnknown = 4
    }
    public enum E_LoanSaleDispositionT
    {
        [OrderedDescription("<--NONE SELECTED-->")]
        Blank = 0,

        [OrderedDescription("Sold to Secondary Market Agencies (FNMA, FHLMC, GNMA)")]
        SoldToSecondaryMarketAgencies_FNMA_FHLMC_GNMA = 1,

        [OrderedDescription("Sold to Other (Non-Affiliate)")]
        SoldToOther_NonAffiliate = 2,

        [OrderedDescription("Sold to Other (Affiliate)")]
        SoldToOther_Affiliate = 3,

        [OrderedDescription("Kept in Portfolio/Held for Investment")]
        KeptInPortfolio_HeldForInvestment = 4,

        [OrderedDescription("Sold through Non-Agency Securitization with Sale Treatment")]
        SoldThroughNonAgencySecuritizationWithSaleTreatment = 5,

        [OrderedDescription("Sold through Non-Agency Securitization w/o Sale Treatment")]
        SoldThroughNonAgencySecuritizationWithOutSaleTreatment = 6
    }
    public enum E_NCCallRprtResponseT
    {
        [OrderedDescription(null)]
        NA = 0,
        [OrderedDescription("No")]
        NO = 1,
        [OrderedDescription("Yes")]
        YES = 2
    }
    public enum E_DocBarcodeFormatT
    {
        DocMagic = 0,
        DocuTech = 1,
        IDS = 2,
        LendingQB = 3
    }
    public enum E_AuditItemCategoryT
    {
        FieldChange = 0,
        RoleAssignment = 1,
        RateLock = 2,
        LoanStatus = 3,
        Email = 4,
        CreditReport = 5,
        DisclosureESign = 6,
        Integration = 7,
        TrailingDocument = 8,
        GFEArchived = 9,
        LoanEstimateArchived = 10,
        ClosingDisclosureArchived = 11,
        DisclosuresArchived = 12,
        FeeService = 13,
        AgentContactRecordChange = 14,
    }
    public enum E_sDisclosureNeededT
    {
        None = 0,
        InitDisc_RESPAAppReceived = 1,
        InitDisc_LoanRegistered = 2,
        EConsentDec_PaperDiscReqd = 3,
        EConsentNotReceived_PaperDiscReqd = 4,
        APROutOfTolerance_RediscReqd = 5,
        CC_LoanAmtChanged = 6,
        CC_LoanLocked = 7,
        CC_LockExtended = 8,
        CC_Relocked = 9,
        CC_FloatDownLock = 10,
        CC_GFEChargeDiscountChanged = 11,
        CC_PropertyValueChanged = 12,
        CC_UWCreditScoreChanged = 13,
        CC_ProgramChanged = 14,
        AwaitingEConsent = 15,
        CC_ImpoundChanged = 16, // opm 205074
        CC_PurchasePriceChanged = 17,
        InitDisc_RESPAAppReceivedVerbally = 18, // opm 209851
        Custom = 19 // opm 471930
    }
    public enum E_RateLockDisclosureEventT
    {
        LoanLocked = 0,
        LockExtension = 1,
        Relocked = 2,
        FloatDownLock = 3
    }
    public enum E_DisclosureT
    {
        None = 0,
        InitialDisclosure = 1,
        Redisclosure = 2
    }
    public enum E_EDisclosureDisclosureEventT
    {
        DM_EConsentReceived = 0,
        DM_DocumentReviewCompleted = 1,
        EConsentReceived = 2,
        EConsentDeclined = 3,
        DocumentsReviewed = 4,
        ESignCompleted = 5,
        EConsentExpired = 6,
        DM_EConsentDeclined = 7,
        DM_ESignCompleted = 8,
        ManualFulfillmentInitiated = 9,
        ReceivedLQBESignUpdate = 10, // 10/21/2013 gf - opm 142153
        EConsentReceivedForAllParties = 11, // 02/08/2016
        DocumentsSigned = 12, // 02/06/2018 je - opm 462016
    }

    public enum E_sAutoDisclosureStatusT
    {
        NA = 0,
        Scheduled = 1,
        Failed = 2
    }

    public enum E_PoolRoundingT
    {
        LeaveBlank = 0,
        Nearest125,
        Nearest25,
        Up125,
        Up25,
        Down01,
        Down125,
        Down25,
        NoRounding
    }

    public enum E_PoolAccrualRateStructT
    {
        Stated = 0,
        WeightedAverage
    }

    public enum E_PoolStructureT
    {
        SingleLender = 0,
        LenderInitiatedMultipleLender,
        InvestorDefinedMultipleLender
    }

    public enum E_BillingMethodT
    {
        LeaveBlank = 0,
        MonthlyStatements,
        CouponBook,
        AutomaticDraft
    }

    public enum E_BillingAccountLocationT
    {
        External = 0,
        Internal
    }

    public enum E_BillingAccountT
    {
        LeaveBlank = 0,
        Checking,
        Savings
    }
    public enum E_AutoRateLockAction
    {
        Extend = 0,
        FloatDown = 1,
        ReLock = 2
    }
    public enum E_AutoSavePage
    {
        None = 0,
        PmlSummary = 1,
        ApprovalCert = 2,
        LockConf = 3,
        GeneratedDocs = 4,
        DODUFindings = 5,
        LPFindings = 6,
        MiscTitleDocuments = 7,
        CertSubmitted = 8,
        FHATotalScorecard = 9,
        FHACaseAssignment = 10,
        FHACaseQuery = 11,
        FHACAIVRSAuthorization = 12,
        CreditReport = 13,
        AppraisalDocs = 14,
        ComplianceEase = 15,
        DataVerifyDRIVE = 16,
        FannieMaeEarlyCheckResults = 17,
        Irs4506TDocuments = 18,
        SuspenseNotice = 19,
        MortgageInsurancePolicyDocuments = 20,
        CreditReportLqi = 21,
        MortgageInsuranceQuoteDocuments = 22,
        UniformClosingDataset = 23,
        ConsumerPortalCreditAuthorization = 24,
        VoaVodDocuments = 25,
        VoeVoiDocuments = 26,
        SSA89Documents = 27,
        UniformClosingDatasetFindings = 28,
        PaymentStatement = 29,

        /// <summary>
        /// The certificate of DocuSign eSignature.
        /// </summary>
        ESignedCertificate = 30,

        /// <summary>
        /// An inbox to receive DocuSign documents with eSignatures.
        /// </summary>
        ESignedDocument = 31,

        VerbalCreditAuthorization = 32,

        /// <summary>
        /// A supplemental credit document, usually ordered as an additional service.
        /// </summary>
        CreditSupplement = 33,

        /// <summary>
        /// A report of the billing for a service, originally added for credit reports.
        /// </summary>
        BillingReport = 34,
    }
    public enum E_FHAConnDocT
    {
        CaseAssignment = 0,
        CaseQuery,
        CAIVRSAuthorization
    }
    public enum E_FromDateT
    {
        Today = 0,
        Yesterday = 1,
        This_Monday = 2,
        Last_Monday = 3,
        Next_Monday = 4,
        Beginning_This_Month = 5,
        Beginning_Last_Month = 6,
        Beginning_Next_Month = 7,
        Start_Current_Calendar_Quarter = 8,
        Start_Last_Calendar_Quarter = 9,
        Beginning_This_Year = 10,
        Beginning_Last_Year = 11
    }
    public enum E_ToDateT
    {
        Today = 0,
        Yesterday = 1,
        This_Friday = 2,
        Last_Friday = 3,
        Next_Friday = 4,
        End_This_Month = 5,
        End_Last_Month = 6,
        End_Next_Month = 7,
        End_Current_Calendar_Quarter = 8,
        End_Last_Calendar_Quarter = 9,
        End_This_Year = 10,
        End_Last_Year = 11
    }
    public enum E_RateLockExpirationWeekendHolidayBehavior
    {
        PrecedingBusinessDay = 0,
        AllowWeekendHoliday = 1,
        FollowingBusinessDay = 2
    }
    public enum E_FeeServiceConditionT
    {
        Enumerated = 0,
        Range = 1,
        Nested = 2
    }

    public enum E_FeeServiceFeePropertyT
    {
        None = 0,
        Description = 1,
        //GfeBox = 2,
        Apr = 3,
        Fha = 4,
        PaidTo = 5,
        ThirdParty = 6,
        Affiliate = 7,
        CanShop = 8,
        Percent = 9,
        BaseValue = 10,
        Amount = 11,
        PaidBy = 12,
        Payable = 13,
        Remove = 14,
        Dflp = 15,

        // OPM 217028 - New Property Types for Housing Expenses
        TaxType = 16,
        CalculationSource = 17,
        Prepaid = 18,
        Escrow = 19,
        PrepaidMonths = 20,
        ReserveCushion = 21,
        ReserveMonthsLocked = 22,
        ReserveMonths = 23,
        PaymentRepeatInterval = 24,
        DisbursementSchedule = 25,
        SettlementServiceProvider = 26
    }

    public enum E_AmountPriceCalcMode
    {
        Amount = 0,
        Price = 1
    }

    public enum E_CustomPmlFieldVisibilityT
    {
        AllLoans = 0,
        PurchaseLoans = 1,
        RefinanceLoans = 2,
        HomeEquityLoans = 3
    }

    public enum E_ClosingFeeSource
    {
        FeeService = 0,
        ClosingCostTemplate = 1,
        LoanFile = 2,
        FirstAmerican = 3
    }

    public enum E_sPayoffStatementInterestCalcT
    {
        PerDiem = 0,
        WholeMonth = 1
    }

    public enum E_sPayoffStatementRecipientT
    {
        BorrowerMailingAddress = 0,
        SubjectPropertyAddress = 1,
        InvestorMainAddress = 2,
        InvestorNoteShipToAddress = 3,
        Other = 4,
    }

    public enum E_SellerType
    {
        Individual = 0,
        Entity = 1,
    }
    public enum E_SellerEntityType
    {
        [OrderedDescription("-- Select Entity Type --")]
        Undefined = 0,

        Company = 1,
        Corporation = 2,
        GeneralPartnership = 3,
        LimitedLiabilityCompany = 4,
        LimitedLiabilityCorporation = 5,
        LimitedPartnership = 6,
        SoleProprietor = 7,
    }

    public enum E_UrlOption
    {
        Page_2010GFE = 0,
        LeadMainFrame = 1,
        Page_Certificate = 2,
        BrokerToUnderwritingNotes = 3,
        LoanProgramView = 4,
        Page_GSEDelivery = 5,
        Page_StatusGeneral = 6,
        Page_SettlementCharges = 7,
        Page_LoanInformation = 8,
        Page_CreditScores = 9,
        Page_1003_1 = 10,
        Page_1003_3 = 11,
        Page_1008_04 = 12,
        Page_1008_04Combined = 13,
        Page_PropertyDetails = 14,
        Page_PropertyInformation = 15,
        Page_StatusHMDA = 16,
        Page_StatusProcessing = 17,
        Page_Servicing = 18,
        Page_ARMProgramDisclosure = 19,
        Page_TIL = 20,
        Page_FHAAddendum = 21,
        Page_MortgageInsurancePolicy = 22,
        Page_StatusAgents = 23,
        Page_CheckEligibility = 24,
        Page_BorrowerInformation = 25,
        Page_PmlReviewInfo = 26,
        Page_PMLDefaultValues = 27,
        Page_BorrowerMonthlyIncome = 28,
        Page_2015GFE = 29,
        Page_LoanEstimate = 30,
        Page_InternalPricingValidationError = 31,
        Page_BrokerRateLock = 32,
        Page_InvestorRateLock = 33,
        Page_1003_2 = 34,
        Page_FannieAddendum = 35,
        Page_LoanTerms = 36,
        Page_BorrowerInfoNoTab = 37,
        Page_SubmitToLPA = 38,
        Page_Liabilities = 39,
        Page_Assets = 40

    }

    public enum EditorStatus
    {
        Never = -1,
        Both = 0,
        Lead = 1,
        Loan = 2
    }

    public enum E_PricingPolicyFieldValueSource
    {
        OriginatingCompanyOrBranch = 0,
        IndividualUser = 1,
    }

    public enum E_Timing
    {
        Blank = 0,

        [OrderedDescription("Before Closing")]
        Before_Closing = 1,

        [OrderedDescription("At Closing")]
        At_Closing = 2,

        [OrderedDescription("After Closing")]
        After_Closing = 3
    }

    public enum E_HighPricedMortgageT
    {

        Unknown = 0,
        None = 1,

        [OrderedDescription("Higher-priced QM/HPML")]
        HigherPricedQm = 2,

        HPML = 3,


        [OrderedDescription("Higher-priced QM/not HPML")]
        HigherPricedQmNotHpml = 4,
    }

    public enum E_sSpAppraisalFormT
    {
        Blank = 0,

        [OrderedDescription("FNM 1004 / FRE 70 - Uniform Residential Appraisal Report")]
        UniformResidentialAppraisalReport = 1,

        [OrderedDescription("FNM 1004C / FRE 70B - Manufactured Home Appraisal Report")]
        ManufacturedHomeAppraisalReport = 2,

        [OrderedDescription("FNM 1004D / FRE 442 - Appraisal Update and/or Completion Report")]
        AppraisalUpdateAndOrCompletionReport = 3,

        [OrderedDescription("FNM 1025 / FRE 72 - Small Residential Income Property Appraisal Report")]
        SmallResidentialIncomePropertyAppraisalReport = 4,

        [OrderedDescription("FNM 1073 / FRE 465 - Individual Condominium Unit Appraisal Report")]
        IndividualCondominiumUnitAppraisalReport = 5,

        [OrderedDescription("FNM 1075 / FRE 466 - Exterior-Only Inspection Individual Condominium Unit Appraisal Report")]
        ExteriorOnlyInspectionIndividualCondominiumUnitAppraisalReport = 6,

        [OrderedDescription("FNM 2000 / FRE 1032 - One-Unit Residential Appraisal Field Review Report")]
        OneUnitResidentialAppraisalFieldReviewReport = 7,

        [OrderedDescription("FNM 2000A / FRE 1072 - Two- to Four-Unit Residential Appraisal")]
        TwoToFourUnitResidentialAppraisal = 8,

        [OrderedDescription("FNM 2055 / FRE 2055 - Exterior-Only Inspection Residential Appraisal Report")]
        ExteriorOnlyInspectionResidentialAppraisalReport = 9,

        [OrderedDescription("FNM 2090 - Individual Cooperative Interest Appraisal Report")]
        IndividualCooperativeInterestAppraisalReport = 10,

        [OrderedDescription("FNM 2095 - Exterior-Only Individual Cooperative Interest Appraisal Report")]
        ExteriorOnlyInspectionIndividualCooperativeInterestAppraisalReport = 11,

        [OrderedDescription("DU Form 2075 - Desktop Underwriter Property Inspection Report")]
        DesktopUnderwriterPropertyInspectionReport = 12,

        [OrderedDescription("LP Form 2070 - Loan Prospector Condition and Marketability Report")]
        LoanProspectorConditionAndMarketability = 13,

        Other = 14,
    }

    public enum E_CuRiskT
    {
        LeaveBlank = 0,
        NotHeightenedRisk = 1,
        HeightenedRisk = 2
    }

    public enum E_InvestorRolodexType
    {
        Investor = 0,
        Subservicer = 1,
    }

    public enum DocumentRequestType
    {
        SendDocumentToBorrower = 0,
        SendDocumentToBorrowerForSigning = 1,
        ReceiveDocumentFromBorrower = 2
    }

    public enum PendingDocumentRequestStatus
    {
        Ready = 0,
        Suspended = 1,
        Error = 2
    }

    public enum E_sLenderFeeBuyoutRequestedT
    {
        No = 0,
        Yes = 1,
        Waived = 2
    }

    public enum E_UserTheme
    {
        Light = 0,
        Dark = 1
    }

    public enum E_OCRoles
    {
        Broker = 0,
        MiniCorrespondent = 1,
        Correspondent = 2
    }

    public enum E_UnderwritingAuthority
    {
        PriorApproved = 0,
        Delegated = 1
    }

    /// <summary>
    /// When adding a new constant to this enumeration, make sure to
    /// update <code>ConstApp.FriendlyOCStatusTypes</code>.
    /// </summary>
    public enum OCStatusType
    {
        Blank = 0,
        Approved = 1,
        Suspended = 2,
        Inactive = 3,
        Terminated = 4,
        Pending = 5,
        Declined = 6,
        Watchlist = 7
    }

    public enum NewTpoFeatureOcGroupAccess
    {
        None = 0,
        Alpha = 1,
        AlphaAndBeta = 2,
        All = 3
    }

    public enum TpoRequestFormSource
    {
        CustomPdf = 0,
        Hosted = 1
    }

    public enum TpoDisclosureType
    {
        InitialDisclosure = 0,
        Redisclosure = 1,
        InitialClosingDisclosure = 2
    }

    public enum OrigPortalAntiSteeringDisclosureAccessType
    {
        Never = 0,
        LenderPaidOriginationOnly = 1,
        AllTpoTransactions = 2
    }

    public enum RelationshipSetType
    {
        LendingQB,
        Pml_Broker,
        Pml_Correspondent,
        Pml_MiniCorrespondent
    }

    public enum E_LegacyGfeFieldT
    {
        Undefined = 0,
        sLOrigF = 1,
        sLDiscnt = 2,
        sApprF = 3,
        sCrF = 4,
        sTxServF = 5,
        sFloodCertificationF = 6,
        sMBrokF = 7,
        sInspectF = 8,
        sProcF = 9,
        sUwF = 10,
        sWireF = 11,
        s800U1F = 12,
        s800U2F = 13,
        s800U3F = 14,
        s800U4F = 15,
        s800U5F = 16,
        sIPia = 17,
        // This should really be sRecurringMipPia but I didn't want to change this enum name.
        sMipPia = 18,
        sHazInsPia = 19,
        s904Pia = 20,
        sVaFf = 21,
        sHazInsRsrv = 22,
        sMInsRsrv = 23,
        sRealETxRsrv = 24,
        sSchoolTxRsrv = 25,
        sFloodInsRsrv = 26,
        sAggregateAdjRsrv = 27,
        s1006Rsrv = 28,
        s1007Rsrv = 29,
        sEscrowF = 30,
        sOwnerTitleInsF = 31,
        sTitleInsF = 32,
        sDocPrepF = 33,
        sNotaryF = 34,
        sAttorneyF = 35,
        sU1Tc = 36,
        sU2Tc = 37,
        sU3Tc = 38,
        sU4Tc = 39,
        sRecF = 40,
        sRecDeed = 41,
        sRecMortgage = 42,
        sRecRelease = 43,
        sCountyRtc = 44,
        sStateRtc = 45,
        sU1GovRtc = 46,
        sU2GovRtc = 47,
        sU3GovRtc = 48,
        sPestInspectF = 49,
        sU1Sc = 50,
        sU2Sc = 51,
        sU3Sc = 52,
        sU4Sc = 53,
        sU5Sc = 55,
        s900U1Pia = 56,
        sU3Rsrv = 57,
        sU4Rsrv = 58,
        sGfeOriginatorCompF = 59,
        sGfeOriginatorCompF_Manual = 60,
        sGfeOriginatorComp_BorrowerPaid = 61,
        sUpfrontMipPia = 62
    }

    public enum E_sConsumerPortalCreationT
    {
        NotApplicable = 0,
        FullApplication = 1,
        ShortApplication = 2,
        Respa6Only = 3
    }

    public enum E_DisbursementTypeT
    {
        Projected = 0,
        Actual = 1
    }

    public enum E_DisbursementPaidByT
    {
        Borrower = 0,
        Lender = 1,
        Seller = 2,
        EscrowImpounds = 3
    }

    public enum E_HousingExpenseTypeCalculatedT
    {
        HazardInsurance = 0,
        FloodInsurance = 1,
        WindstormInsurance = 2,
        CondoHO6Insurance = 3,
        RealEstateTaxes = 4,
        SchoolTaxes = 5,
        OtherTaxes1 = 6,
        OtherTaxes2 = 7,
        OtherTaxes3 = 8,
        OtherTaxes4 = 9,
        HomeownersAsscDues = 10,
        GroundRent = 11,
        Line1008Exp = 12,
        Line1009Exp = 13,
        Line1010Exp = 14,
        Line1011Exp = 15
    }

    public enum E_HousingExpenseTypeT
    {
        HazardInsurance = 0,
        FloodInsurance = 1,
        WindstormInsurance = 2,
        CondoHO6Insurance = 3,
        RealEstateTaxes = 4,
        SchoolTaxes = 5,
        OtherTaxes1 = 6,
        OtherTaxes2 = 7,
        OtherTaxes3 = 8,
        OtherTaxes4 = 9,
        HomeownersAsscDues = 10,
        GroundRent = 11,
        Unassigned = 13
    }

    public enum E_AnnualAmtCalcTypeT
    {
        LoanValues = 0,
        Disbursements = 1
    }

    public enum E_BypassReadPermissionCheckT
    {
        False = 0, // default is not to disable the read permission check.
        True = 1
    }

    public enum E_DisbursementRepIntervalT
    {
        [OrderedDescription("Annual", Order = 0)]
        Annual = 0,
        [OrderedDescription("Monthly", Order = 2)]
        Monthly = 1,
        [OrderedDescription("Annually in closing month", Order = 1)]
        AnnuallyInClosingMonth = 2
    }

    public enum E_CustomExpenseLineNumberT
    {
        None = 0,
        Line1008 = 1,
        Line1009 = 2,
        Line1010 = 3,
        Line1011 = 4,
        Any = 5
    }

    public enum E_DisbPaidDateType
    {
        BeforeClosing = 0,
        AtClosing = 1,
        AfterClosing = 2
    }

    public enum E_DeliveryMethodT
    {
        [Description("")]
        LeaveEmpty = 0,

        [Description("Email")]
        Email = 1,

        [Description("Fax")]
        Fax = 2,

        [Description("In Person")]
        InPerson = 3,

        [Description("Mail")]
        Mail = 4,

        [Description("Overnight")]
        Overnight = 5
    }

    public enum E_EscrowImpoundsCalcMethodT
    {
        Regulatory = 0,
        Customary = 1
    }

    public enum E_CustomaryEscrowImpoundsCalcMinT
    {
        Zero = 0,
        NoMin = 1,
        Cushion = 2,
        Zero_AdjustAggregate = 3,
        Cushion_AdjustAggregate = 4

    }

    public enum E_AggregateEscrowCalculationModeT
    {
        Legacy = 0,
        Regulatory = 1,
        Customary = 2,
        Customary2 = 3
    }

    public enum E_AskConsumerForSubjPropertyT
    {
        NoNever = 0,
        YesAlways = 1,
        YesRefiNoPurch = 2
    }

    public enum E_AskConsumerForSubjValue
    {
        NoNever = 0,
        YesAlways = 1,
        YesPurchNoRefi = 2
    }

    public enum E_SelectsLoanProgramInFullSubmission
    {
        NoNever = 0,
        YesAlways = 1,
        YesPurchNoRefi = 2
    }

    public enum E_EnforceFolderPermissions
    {
        True = 0, // default should be true.
        False = 1
    }

    public enum E_AppraisalNeededDateOptions
    {
        [Description("Enabled, Not Required")]
        EnabledNotRequired = 0,
        [Description("Enabled and Required")]
        Required = 1,
        [Description("Hidden")]
        Hidden = 2
    }

    /// <summary>
    /// Lists the document vendor platform types we recognize.
    /// </summary>
    public enum E_DocumentVendor
    {
        [OrderedDescription("Docutech")]
        DocuTech = 1,
        [OrderedDescription("Docs on Demand")]
        DocsOnDemand = 2,
        [OrderedDescription("IDS")]
        IDS = 3,
        [OrderedDescription("DocMagic")]
        DocMagic = 4,
        [OrderedDescription("Simplifile")]
        Simplifile = 5,
        [OrderedDescription("Closing Xpress")]
        ClosingXpress = 6,
        [OrderedDescription("MRG")]
        MRG = 7,
        [OrderedDescription("Signia Documents")]
        SigniaDocuments = 8,
        [OrderedDescription("")]
        UnknownOtherNone = 0
    }

    public enum E_sClosingCostAutomationUpdateT
    {
        UpdateOnConditionChange = 0,
        PreserveFeesOnLoan = 1,
        UpdateUnconditionally = 2
    }

    /// <summary>
    /// Enumerates program types for Government Bond Financing of mortgage pools/securities.
    /// </summary>
    public enum E_GovernmentBondFinancingProgramType
    {
        LeaveBlank = 0,
        BuilderBond = 1,
        ConsolidatedBond = 2,
        FinalBond = 3
    }

    public enum EmployeePopulationMethodT
    {
        OriginatingCompany = 0,
        IndividualUser = 1
    }

    public enum E_sPropertyInspectionT
    {
        ExteriorAndInterior = 0,
        ExteriorOnly = 1,
        None = 2,
        Other = 3,
    }

    public enum Hmda_Ethnicity
    {
        [Description("")]
        Blank = 0,
        [Description("Hispanic Or Latino")]
        HispanicOrLatino = 1,
        [Description("Mexican")]
        Mexican = 11,
        [Description("Puerto Rican")]
        PuertoRican = 12,
        [Description("Cuban")]
        Cuban = 13,
        [Description("Other Hispanic or Latino")]
        OtherHispanicOrLatino = 14,
        [Description("Not Hispanic or Latino")]
        NotHispanicOrLatino = 2,
        [Description("Information not provided by applicant in mail, internet, or telephone application")]
        NotProvided = 3,
        [Description("Not applicable")]
        NotApplicable = 4,
        [Description("No co-applicant")]
        NoCoApplicant = 5
    }

    public enum Hmda_Race
    {
        [Description("")]
        Blank = 0,
        [Description("American Indian or Alaskan Native")]
        AmericanIndianOrAlaskanNative = 1,
        [Description("Asian")]
        Asian = 2,
        [Description("Asian Indian")]
        AsianIndian = 21,
        [Description("Chinese")]
        Chinese = 22,
        [Description("Filipino")]
        Filipino = 23,
        [Description("Japanese")]
        Japanese = 24,
        [Description("Korean")]
        Korean = 25,
        [Description("Vietnamese")]
        Vietnamese = 26,
        [Description("Other Asian")]
        OtherAsian = 27,
        [Description("Black or African American")]
        BlackOrAfricanAmerican = 3,
        [Description("Native Hawaiian or Other Pacific Islander")]
        NativeHawaiianOrOtherPacificIslander = 4,
        [Description("Native Hawaiian")]
        NativeHawaiian = 41,
        [Description("Guamanian or Chamorro")]
        GuamanianOrChamorro = 42,
        [Description("Samoan")]
        Samoan = 43,
        [Description("Other Pacific Islander")]
        OtherPacificIslander = 44,
        [Description("White")]
        White = 5,
        [Description("Information not provided by applicant in mail, internet, or telephone application")]
        NotProvided = 6,
        [Description("Not applicable")]
        NotApplicable = 7,
        [Description("No co-applicant")]
        NoCoApplicant = 8
    }

    public enum Hmda_Sex
    {
        [Description("")]
        Blank = 0,
        [Description("Male")]
        Male = 1,
        [Description("Female")]
        Female = 2,
        [Description("Information not provided by applicant in mail, internet, or telephone application")]
        NotProvided = 3,
        [Description("Not applicable")]
        NotApplicable = 4,
        [Description("No co-applicant")]
        NoCoApplicant = 5,
        [Description("Applicant selected both male and female")]
        BothMaleFemale = 6
    }

    public enum VisualObservationSurnameCollection
    {
        [Description("")]
        Blank = 0,
        [Description("Collected on the basis of visual observation or surname")]
        CollectedOnBasisOfVisualObservationOrSurname = 1,
        [Description("Not collected on the basis of visual observation or surname")]
        NotCollectedOnBasisOfVisualObservationOrSurname = 2,
        [Description("Not applicable")]
        NotApplicable = 3,
        [Description("No co-applicant")]
        NoCoApplicant = 4
    }

    public enum AUSType
    {
        [Description("")]
        Blank = 0,
        [Description("Desktop Underwriter (DU)")]
        DesktopUnderwriter = 1,
        [Description("Loan Prospector")]
        LoanProspector = 2,
        [Description("Technology Open to Approved Lenders (TOTAL) Scorecard")]
        TotalScorecard = 3,
        [Description("Guaranteed Underwriting System (GUS)")]
        GUS = 4,
        [Description("Other")]
        Other = 5,
        [Description("Not applicable")]
        NotApplicable = 6
    }

    /// <summary>
    /// Borrower's preferred language. 
    /// </summary>
    public enum LanguagePrefType
    {
        English = 0,
        Chinese = 1,
        Korean = 2,
        Spanish = 3,
        Tagalog = 4,
        Vietnamese = 5,
        Other = 6,
        Blank = 7
    }

    /// <summary>
    /// Domestic Relationship Type.
    /// </summary>
    public enum DomesticRelationshipType
    {
        Blank = 0,
        CivilUnion = 1,
        DomesticPartnership = 2,
        RegisteredReciprocalBeneficiaryRelationship = 3,
        Other = 4
    }

    public enum CounselingType
    {
        Counseling = 0,
        Education = 1
    }

    public enum CounselingFormatType
    {
        FaceToFace = 0,
        Telephone = 1,
        Internet = 2
    }

public enum AUSResult
    {
        [Description("")]
        Blank = 0,
        [Description("Approve/Eligible")]
        ApproveEligible = 1,
        [Description("Approve/Ineligible")]
        ApproveIneligible = 2,
        [Description("Refer/Eligible")]
        ReferEligible = 3,
        [Description("Refer/Ineligible")]
        ReferIneligible = 4,
        [Description("Refer with Caution")]
        ReferCaution = 5,
        [Description("Out of Scope")]
        OutOfScope = 6,
        [Description("Error")]
        Error = 7,
        [Description("Accept")]
        Accept = 8,
        [Description("Caution")]
        Caution = 9,
        [Description("Ineligible")]
        Ineligible = 10,
        [Description("Incomplete")]
        Incomplete = 11,
        [Description("Invalid")]
        Invalid = 12,
        [Description("Refer")]
        Refer = 13,
        [Description("Eligible")]
        Eligible = 14,
        [Description("Unable to Determine")]
        UnableToDetermine = 15,
        [Description("Other")]
        Other = 16,
        [Description("Not applicable")]
        NotApplicable = 17
    }

    public enum SelfOwnershipShare
    {
        Blank = 0,
        LessThan25 = 1,
        GreaterOrEqualTo25 = 2
    }
}
