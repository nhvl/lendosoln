namespace DataAccess
{
    using System;
    using System.Collections;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading;
    using System.Web;
    using DataAccess.PathDispatch;
    using DavidDao.Reflection;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.HttpModule;
    using LendersOffice.ObjLib.Security;
    using LendersOffice.Security;
    using LendingQB.Core.Commands;
    using LqbGrammar.DataTypes.PathDispatch;

    public partial class CPageData : IPathResolvable, ILoanCommandHandler, IPathSettable, ILoanLqbCollectionContainerObserver, IHaveReadableDependencies
    {

        #region Smart Dependency Stuff

        public static List<string> GetRecentModificationUserName(Guid sLId, AbstractUserPrincipal principal)
        {
            List<string> list = new List<string>();

            bool bIsEnabled = false; // 8/5/2010 dd - Not ready for prime time yet.

            if (principal != null && principal.BrokerId != Guid.Empty)
            {
                BrokerDB broker = principal.BrokerDB;
                bIsEnabled = broker.IsEnforceVersionCheck; // 3/16/2011 dd - This feature is enable if lender has enforce version check on.
            }
            // 1/26/2011 dd - Turn the feature off.
            if (bIsEnabled)
            {
                SqlParameter[] parameters = {
                                            new SqlParameter("@sLId", sLId),
                                            new SqlParameter("@PeriodInMinutes", ConstApp.LoanRecentModificationWarningPeriodInMinutes),
                                            new SqlParameter("@ExcludeUserId", principal.UserId)
                                        };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOTransient, "LoanModification_GetRecentModification", parameters))
                {
                    while (reader.Read())
                    {
                        string userType = (string)reader["UserType"];
                        if (principal.Type == "P" && userType == "B")
                        {
                            // 8/2/2010 dd - Based on the spec we do not return "B" user modification to "P" user.
                            continue;
                        }
                        list.Add((string)reader["UserName"]);
                    }
                }
            }
            return list;
        }
        public static Guid RetrieveLoanIdByEncompassLoanId(Guid sEncompassLoanId, Guid sBrokerId)
        {
            Guid sLId = Guid.Empty;

            SqlParameter[] parameters = {
                                            new SqlParameter("@sEncompassLoanId", sEncompassLoanId),
                                            new SqlParameter("@sBrokerId", sBrokerId)
                                        };
            int count = 0;
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(sBrokerId, "RetrieveLoanIdByEncompassLoanId", parameters))
            {
                while (reader.Read())
                {
                    sLId = (Guid)reader["sLId"];
                    count++;
                }
            }
            if (count > 1)
            {
                // 7/7/2009 dd - The encompass loan id must be unique. If there are more than one encompass loan id throw exception.
                throw new CBaseException(ErrorMessages.Generic, "EncompassId=" + sEncompassLoanId + " is not unique.");
            }
            return sLId;
        }

        private static ConcurrentDictionary<Type, IReadOnlyCollection<string>> s_typeDependentFields = new ConcurrentDictionary<Type, IReadOnlyCollection<string>>(); 

        private static CSelectStatementProvider GetSelectStatementProviderByType(Type t)
        {
            if (null == t)
            {
                throw new CBaseException(ErrorMessages.Generic, "Type cannot be null.");
            }

            IEnumerable<string> dependentFields = GetCPageBaseAndCAppDataDependencyList(t, logFields: true);
            return CSelectStatementProvider.GetProviderForTargets(dependentFields, false);
        }

        private static CSelectStatementProvider GetSelectStatementProviderByType(Type t, IEnumerable<string> fieldIds)
        {
            if (null == t)
            {
                throw new CBaseException(ErrorMessages.Generic, "Type cannot be null.");
            }

            List<string> dependentFields = GetCPageBaseAndCAppDataDependencyList(t, logFields: true).ToList();
            dependentFields.AddRange(fieldIds);
            return CSelectStatementProvider.GetProviderForTargets(dependentFields, false);
        }

        /// <summary>
        /// DO NOT USE UNLESS YOU ABSOLUTELY HAVE TO
        /// </summary>
        /// <param name="sLId"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public static CFullAccessPageData CreateUsingSmartDependencyWithSecurityBypass(Guid sLId, Type t)
        {
            if (null == t)
            {
                throw new CBaseException(ErrorMessages.Generic, "Type cannot be null.");
            }

            return new CFullAccessPageData(sLId, t.FullName, GetSelectStatementProviderByType(t));
        }

        /// <summary>
        /// Same as <see cref="CreateUsingSmartDependencyWithSecurityBypass"/> but it also always adds in the passed in fields to the dependency list.
        /// </summary>
        /// <param name="sLId">The loan id.</param>
        /// <param name="t">The type of the class.</param>
        /// <param name="fieldIds">The field ids to always load up.</param>
        /// <returns>The data loan.</returns>
        public static CPageData CreateUsingSmartDependencyWithSecurityBypassAndGuaranteedFields(Guid sLId, Type t, IEnumerable<string> fieldIds)
        {
            if (null == t)
            {
                throw new CBaseException(ErrorMessages.Generic, "Type cannot be null.");
            }

            return new CFullAccessPageData(sLId, t.FullName, GetSelectStatementProviderByType(t, fieldIds));
        }

        public static CPageData CreateUsingSmartDependency(Guid sLId, Type t)
        {
            if (null == t)
            {
                throw new CBaseException(ErrorMessages.Generic, "Type cannot be null.");
            }

            return new CPageData(sLId, t.FullName, GetSelectStatementProviderByType(t));
        }


        /// <summary>
        /// Creates, sets dependencies on, initializes (by calling <see cref="InitLoad()"/>) a new smart loan object for loading data. 
        /// What a concept! All your functional programming dreams have come true.
        /// </summary>
        /// <param name="sLId">Loan identifier.</param>
        /// <param name="t">Type of the object that will use the new CPageData instance.</param>
        /// <returns>A new, InitLoad-ed data loan.</returns>
        public static CPageData CreateUsingSmartDependencyForLoad(Guid sLId, Type t)
        {
            var data = CreateUsingSmartDependency(sLId, t);
            data.InitLoad();
            return data;
        }

        public static CPageData CreatePmlPageDataUsingSmartDependency(Guid sLId, Type t)
        {
            if (null == t)
            {
                throw new CBaseException(ErrorMessages.Generic, "Type cannot be null.");
            }

            return new CPmlPageData(sLId, t.FullName, GetSelectStatementProviderByType(t));
        }

        private static bool IsFieldBasedDependencyMember(Type t)
        {
            return typeof(IHaveReadableDependencies).IsAssignableFrom(t);
        }

        private static string[] s_ignoreCPageBaseMethods = {
                                                               "InitSave", "InitLoad", "Save", "GetAppData","CreateUsingSmartDependency"
                                                               , "SetFormatTarget","GetCPageBaseAndCAppDataDependencyList"
                                                           };

        public static IReadOnlyCollection<string> GetCPageBaseAndCAppDataDependencyList(Type t, bool logFields = false)
        {
            return s_typeDependentFields.GetOrAdd(t, (key) => GetCPageBaseAndCAppDataDependencyListImpl(key, logFields));
        }

        private static IReadOnlyCollection<string> GetCPageBaseAndCAppDataDependencyListImpl(Type t, bool logFields)
        {
            HashSet<string> dependencyFields = GetCPageBaseAndCAppDataDependencyList(t, new HashSet<Type>());
            if (logFields)
            {
                Tools.LogInfo(t.FullName + "::\n" + String.Join(", ", dependencyFields));
            }

            return new List<string>(dependencyFields).AsReadOnly();
        }

        private static HashSet<string> GetCPageBaseAndCAppDataDependencyList(Type t, HashSet<Type> visitedTypes)
        {
            visitedTypes.Add(t);

            HashSet<string> set = new HashSet<string>();
            var fieldInfoTable = CFieldInfoTable.GetInstance();

            List<MethodBase> methodList = t.GetDependsOnList();
            foreach (MethodBase method in methodList)
            {
                Type declaringType = method.DeclaringType;
                if (IsFieldBasedDependencyMember(declaringType))
                {
                    if (s_ignoreCPageBaseMethods.Contains(method.Name))
                    {
                        continue;
                    }
                    string name = method.Name;
                    if (name.StartsWith("get_") || name.StartsWith("set_"))
                    {
                        // 5/15/2009 dd - This is regular property.
                        name = name.Substring(4);

                        // 5/28/2009 dd - CreditReportData use sfCreditReportData as a dependency field name.
                        if (name == "CreditReportData")
                        {
                            name = "sfCreditReportData";
                        }
                    }
                    else
                    {
                        // 5/15/2009 dd - This is method. We need to prefix with either sf or af depend on CPageBase or CAppData.
                        if (name == "ImportCreditScoresFromCreditReport")
                        {
                            // 6/5/2009 dd - This method belong to CAppData but its dependency fields is sfImportCreditScoresFromCreditReport.
                            name = "sfImportCreditScoresFromCreditReport";
                        }
                        else if (name == "ImportLiabilitiesFromCreditReport")
                        {
                            name = "sfImportLiabilitiesFromCreditReport";
                        }
                        else if (name == "ImportPublicRecordsFromCreditReport")
                        {
                            name = "sfImportPublicRecordsFromCreditReport";
                        }
                        else if (name == "LoadOptimisticCreditReportValue")
                        {
                            name = "sfLoadOptimisticCreditReportValue";
                        }
                        else if (declaringType.IsSubclassOf(typeof(DataAccess.CPageBase)) || declaringType.IsSubclassOf(typeof(DataAccess.CPageData)) || declaringType == typeof(DataAccess.CPageData) || declaringType == typeof(DataAccess.CPageBase))
                        {
                            if (fieldInfoTable[name] == null)
                            {
                                name = "sf" + name;
                            }
                        }
                        else if (declaringType == typeof(DataAccess.CAppData) || declaringType == typeof(CAppBase))
                        {
                            if (fieldInfoTable[name] == null)
                            {
                                name = "af" + name;
                            }
                        }
                        else if (typeof(LendingQB.Core.Data.IConsumer).IsAssignableFrom(declaringType)
                            || typeof(LendingQB.Core.Data.ILegacyApplication).IsAssignableFrom(declaringType))
                        {
                        }
                        else
                        {
                            throw new CBaseException(ErrorMessages.Generic, "Hmm.. Declaring type is not a recognized type with DependsOn information. Type=" + declaringType.FullName + ", Name=" + name);
                        }
                    }

                    name = DependencyUtils.NormalizeMemberName(name);

                    //keep in sync with FieldInfo.cs

                    //I'm not entirely sure why this is even needed here. The select statement provider also does the expansion. 
                    //foreach (string[] specialFieldList in CFieldInfoTable.GetSpecialDependencies(true))
                    //{
                    //    CheckFieldNameAndAddAll(list, name, specialFieldList);
                    //}


                    if(name == "sGfeInitialDisclosureD" && !set.Contains(name))
                    {
                        set.UnionWith(CPageBase.TEMP_GFEArchiveFieldNamesFrom2013Q3Release);
                        set.UnionWith(new string[]{"aBNm", "aCNm","sfGetPreparerOfForm"});
                    }
                    set.Add(name);
                }
                else if (declaringType.IsAbstract && t.IsSubclassOf(declaringType))
                {
                    // 6/15/2009 dd - This handle case where method itself is implement from abstract class.
                    // Example:
                    // public abstract class MyAbstract {
                    //    public abstract void Foo(CPageData data);
                    // }
                    // public class Hello : MyAbstract {
                    //    public override void Foo(CPageData data) { }
                    //    public void Save() {
                    //        CPageData data = CPageData.CreateUsingSmartDependency(loanId, typeof(Hello));
                    //        Foo(data);
                    //    }
                    // }
                    //
                    // Without this skip an exception will throw.
                }
                else if (declaringType == typeof(PageDataUtilities) && (method.Name == "GetValue" || method.Name == "SetValue"))
                {
                    // 5/18/2010 dd - We allow CPageBase and CAppData to pass to PageDataUtilities.GetValue and PageDataUtilities.SetValue method.
                }
                else if (declaringType.FullName == "LendersOffice.Common.AbstractPDFFile")
                {
                    if (method.Name == "set_DataLoan" || method.Name == "SetCurrentDataApp")
                    {
                        // 5/18/2010 dd - Skip LendersOffice.Common.AbstractPDFFile.DataLoan and LendersOffice.Common.AbstractPDFFile.SetCurrentDataApp()
                    }
                }
                else if (declaringType == typeof(List<CPageData>))
                {
                    // 8/29/2012 dd - Allow CPageData to add to List<CPageData>
                }
                else if (!visitedTypes.Contains(declaringType))
                {
                    ParameterInfo[] parameterList = method.GetParameters();

                    foreach (ParameterInfo parameter in parameterList)
                    {
                        if (IsFieldBasedDependencyMember(parameter.ParameterType))
                        {
                            HashSet<string> dependencyList = GetCPageBaseAndCAppDataDependencyList(declaringType, visitedTypes);
                            set.UnionWith(dependencyList);

                            Debug.Assert(visitedTypes.Contains(declaringType));
                            break;
                        }
                    }
                }
            }

            return set;
        }

        private static void CheckFieldNameAndAddRequired(List<string> currentDependencyList, string fieldName, IEnumerable<string> requireDependencyList)
        {
            if (!currentDependencyList.Contains(fieldName))
            {
                foreach (var o in requireDependencyList)
                {
                    if (!currentDependencyList.Contains(o))
                    {
                        currentDependencyList.Add(o);
                    }
                }
            }
        }
        
        private static void CheckFieldNameAndAddAll(List<string> currentDependencyList, string fieldName, string[] requireDependencyList)
        {
            if (requireDependencyList.Contains(fieldName))
            {
                foreach (var o in requireDependencyList)
                {
                    if (!currentDependencyList.Contains(o))
                    {
                        currentDependencyList.Add(o);
                    }
                }
            }
        }
        #endregion

        protected CPageBase m_pageBase;
        private Guid m_loanid;
        private bool m_tempDidUserMakeChanges = true;

        protected ILoanFileFieldValidator m_validator;

        /// <summary>
        /// Contains the original data sets for loan level collections that are still using the 
        /// legacy (coll|CollectionName|FieldName) workflow rules. These data sets are not updated
        /// as the objects change, they are just meant to indicate the initial state of the collection.
        /// </summary>
        protected readonly Dictionary<string, DataSet> originalCollectionSnapshotsForLegacyWorkflow = new Dictionary<string, DataSet>();

        /// <summary>
        /// Contains the legacy collection names that have been loaded and need to have an initial
        /// data set snapshot taken.
        /// </summary>
        /// <remarks>
        /// This is used because when we are initially loading the collections from the database, we don't
        /// have all of the data in place to take the initial data set snapshot. Instead, we keep track of
        /// which collections have been loaded and do the snapshot at the end of the data loading process.
        /// </remarks>
        private readonly HashSet<string> collectionsToTrackForLegacyWorkflowOnceLoaded = new HashSet<string>();

        private AbstractUserPrincipal Principal
        {
            get
            {
                return BrokerUserPrincipal.CurrentPrincipal ?? (Thread.CurrentPrincipal as AbstractUserPrincipal);
            }
        }

        public ILoanFileRuleRepository LoanFileWorkflowRules => this.m_validator;

        public CPageData(Guid loanId)
        {
            m_loanid = loanId;
            m_validator = new LoanFileFieldValidator(loanId);
        }

        public CPageData(Guid loanId, string sPageName, CSelectStatementProvider selectProvider)
            : this(loanId)
        {
            Initialize(loanId, sPageName, selectProvider);
        }

        protected CPageData(CPageBase pageBase, ISelectStatementProvider provider, ILoanFileFieldValidator validator)
        {
            this.m_pageBase = pageBase;
            this.m_loanid = pageBase.sLId;
            this.m_validator = validator;
            this.m_pageBase.InternalCPageData = this;
        }

        public CPageData(Guid loanId, string name, IEnumerable<string> dependencyFields) : this(loanId)
        {
            if (dependencyFields != null && dependencyFields.Count() == 1)
            {
                // 11/3/2015 - dd - I want to find out page data that only load one dependency field.
                name = name + "+" + dependencyFields.First();
            }

            Initialize(loanId, name, CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, dependencyFields.ToArray()));
        }

        public CPageData(Guid loanId, IEnumerable<string> dependencyFields)
            : this(loanId)
        {
            Initialize(loanId, string.Empty, CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, dependencyFields.ToArray()));
        }

        private void Initialize(Guid loanId, string sPageName, CSelectStatementProvider selectProvider)
        {
            if (Tools.IsLoanAHELOC(Principal, loanId))
            {
                m_pageBase = new CPageHelocBase(loanId, sPageName, selectProvider);
            }
            else
            {
                m_pageBase = new CPageBaseWrapped(loanId, sPageName, selectProvider);
            }
            m_pageBase.InternalCPageData = this;

        }
        /// <summary>
        /// FOr this to work it has to be set till after save so its enforce all or none. 
        /// </summary>
        public void DisableFieldEnforcementOnCollections()
        {
            m_validator.CheckCollection = false;
        }
        public void DisableFieldEnforcement()
        {
            m_validator.IsDisabled = true;
        }
        /// <summary>
        /// Maintain access check results.  Skip checking if the
        /// implementation can't handle the truth.
        /// </summary>
        protected virtual bool m_enforceAccessControl
        {
            // By default, all page data accessors must submit
            // to access control checking.  Override on a limited
            // case-by-case basis.

            get
            {
                return true;
            }
        }

        public bool EnforceAccessControl
        {
            get { return m_enforceAccessControl; }
        }

        public bool CanWrite
        {
            // Return current access control query results.  If
            // not defined, then assume access denied.

            get
            {
                if (m_enforceAccessControl == true)
                {
                    return x_canWrite;
                }

                return false;
            }
        }

        public bool CanRead
        {
            // Return current access control query results.  If
            // not defined, then assume access denied.

            get
            {
                if (m_enforceAccessControl == true)
                {
                    return x_canRead;
                }

                return false;
            }
        }

        /// <summary>
        /// Attempts to get the data in the location specified by the element.
        /// </summary>
        /// <param name="element">The location of the data being requested.</param>
        /// <returns>The data that was requested.</returns>
        public object GetElement(IDataPathElement element)
        {
            string name = element.Name;

            if (element is DataPathCollectionElement)
            {
                object generatedCollection;
                if (this.TryGetGeneratedCollection(name, out generatedCollection))
                {
                    return generatedCollection;
                }

                switch (name.ToLower())
                {
                    case "application":
                    case "app":
                        return new LoanAppCollection(this);
                    case "borrowerclosingcostset":
                        return this.sClosingCostSet;
                    case "getpreparerofform":
                    case "preparer":
                        return this.PreparerCollection;
                    case "getagentofrole":
                    case "getagentofrolenofallback":
                    case "agent":
                        return this.sAgentCollection;
                    case "consumers":
                        return this.Consumers;
                    case "legacyapplications":
                        return this.LegacyApplications;
                    case "legacyapplicationconsumers":
                        return this.LegacyApplicationConsumers;
                    case "getinvestorinfo":
                    case "investorinfo":
                        throw new NotImplementedException("Haven't added the wrapper for Investors yet.");
                    case "getsubservicerinfo":
                    case "subservicerinfo":
                        throw new NotImplementedException("Haven't added the wrapper for Subservicers yet.");
                    default:
                        throw new ArgumentException("No collection type exists in CPageData of name " + name);
                }
            }
            else if (element is DataPathBasicElement)
            {
                var classType = PageDataUtilities.GetDataClassType(name);

                if (classType.HasValue && classType.Value == E_PageDataClassType.CBasePage)
                {
                    return PageDataUtilities.GetValue(this, null, name);
                }
                else
                {
                    throw new ArgumentException("Could not find field '" + name + "' in CPageData");
                }
            }
            else
            {
                throw new ArgumentException("Unhandled path type: " + element.GetType());
            }
        }

        /// <summary>
        /// Sets the value indicated by the path to the given value.
        /// </summary>
        /// <param name="element">The path to the field.</param>
        /// <param name="value">The field value to set.</param>
        public void SetElement(IDataPathElement element, object value)
        {
            if (element == null)
            {
                throw new ArgumentNullException(nameof(element));
            }
            else if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }
            else if (!(value is string))
            {
                throw new CBaseException(ErrorMessages.Generic, "Loan IPathSettable only supports setting string values");
            }

            if (element is DataPathBasicElement)
            {
                string name = element.Name;
                var classType = PageDataUtilities.GetDataClassType(name);

                if (classType.HasValue && classType.Value == E_PageDataClassType.CBasePage)
                {
                    PageDataUtilities.SetValue(this, null, name, (string)value);
                }
                else
                {
                    throw new CBaseException(ErrorMessages.Generic, "Could not find field '" + name + "' in CPageData");
                }
            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, "Unhandled path type: " + element.GetType());
            }
        }

        /// <summary>
        /// Check if the field id is part of the field protection rule.
        /// </summary>
        /// <param name="fieldId">Field id to check.</param>
        /// <returns>Whether field id is part of field write protection rule.</returns>
        internal bool IsInFieldProtection(string fieldId)
        {
            if (m_enforceAccessControl == true)
            {
                if (m_validator != null)
                {
                    var rules = m_validator.GetProtectionFieldRule(fieldId);

                    if (rules != null)
                    {
                        return true;
                    }
                    return false;
                }
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fieldId"></param>
        /// <returns></returns>
        internal bool IsInFieldWrite(string fieldId)
        {
            if (m_enforceAccessControl == true)
            {
                if (m_validator != null)
                {
                    var rule = m_validator.GetWriteFieldRule(fieldId);

                    if (rule != null)
                    {
                        return true;
                    }
                    return false;
                }
            }

            return false;
        }
        /// <summary>
        /// Check user's credentials against loading.  User needs
        /// read access to pass.
        /// </summary>

        public virtual void InitLoad()
        {
            AbstractUserPrincipal p = null;

            if (m_enforceAccessControl == true)
            {
                p = Principal;
            }
            InitLoad(p);
        }

        public bool TemporaryDidUserMakeChanges
        {
            get { return m_tempDidUserMakeChanges; }
            set { m_tempDidUserMakeChanges = value; }
        }
        private bool x_canRead;
        private bool x_canWrite;

        public bool HasWriteLoanPermission { get; private set; }

        public string ReadOnlysLNm { get; private set; }

        /// <summary>
        /// Resolves permissions on the loan file object, refreshing
        /// the state of the loan field validator.
        /// </summary>
        /// <param name="principal">
        /// The user accessing the loan file object.
        /// </param>
        public void ResolvePermission(AbstractUserPrincipal principal)
        {
            if (this.m_enforceAccessControl)
            {
                this.ResolvePermission(principal, loadValidator: true);
            }
        }

        /// <summary>
        /// Set x_canRead and x_canWrite
        /// </summary>
        /// <param name="principal"></param>
        /// <param name="loadValidator"></param>
        private void ResolvePermission(AbstractUserPrincipal principal, bool loadValidator)
        {
            if (principal == null)
            {
                throw new GenericUserErrorMessageException("Principal cannot be null");
            }

            bool isNeedToEvaluateCanWrite = true;
            if (principal.IsOriginalPrincipalAConsumer)
            {
                principal = principal.OriginalPrincipalConsumer;

            }
            if (principal is ConsumerUserPrincipal)
            {
                LoanAccessInfo accessInfo = new LoanAccessInfo(sLId);
                AccessBinding accessBinding = ConsumerPortalAccessControlProcessor.Resolve((ConsumerUserPrincipal)principal, accessInfo);

                x_canRead = accessBinding.CanRead;
                x_canWrite = accessBinding.CanWrite;
                return;
            }
            else if (principal is ConsumerPortalUserPrincipal)
            {
                AccessBinding accessBinding = ConsumerPortalv2AccessControlProcessor.Resolve((ConsumerPortalUserPrincipal)principal, sLId);

                x_canRead = accessBinding.CanRead;
                x_canWrite = accessBinding.CanWrite;

                if (x_canRead == false && x_canWrite == false)
                {
                    return; // No need to evaluate further.
                }

                ((ConsumerPortalUserPrincipal)principal).SetLoanId(sLId);

                // Continue on and use our workflow engine to evaluate further read/write.
                if (x_canWrite == false)
                {
                    // 6/1/2013 dd - Don't evaluate the CanWrite permission using BrokerUserPrincipal.
                    isNeedToEvaluateCanWrite = false;
                }
            }
            bool isTemplate = false;
            Guid branchId = Guid.Empty;
            int currentFileVersion = 0;
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(principal.ConnectionInfo, "IsLoanTemplate",
                new SqlParameter[] { new SqlParameter("@LoanId", sLId)}))
            {
                if (reader.Read())
                {
                   isTemplate = (bool)reader["IsTemplate"];
                   branchId = (Guid)reader["sBranchId"];
                   currentFileVersion = (int)reader["sFileVersion"];
                }
            }
            //keep this in sync with lendingqbexecution engine
            if (isTemplate)
            {
                x_canRead = principal.HasPermission(Permission.AllowAccessToTemplatesOfAnyBranch) || branchId == principal.BranchId;
                x_canWrite = x_canRead && principal.HasPermission(Permission.CanEditLoanTemplates);
                return;
            }

            WorkflowOperation[] interestingOperations;
            if (loadValidator)
            {
                interestingOperations = new WorkflowOperation[] {
                    WorkflowOperations.ReadLoanOrTemplate,
                    WorkflowOperations.WriteLoanOrTemplate,
                    WorkflowOperations.WriteField,
                    WorkflowOperations.ProtectField,
                    WorkflowOperations.LoanStatusChange};
            }
            else
            {
                interestingOperations = new WorkflowOperation[] { 
                    WorkflowOperations.ReadLoanOrTemplate,
                    WorkflowOperations.WriteLoanOrTemplate };
            }
    

            Stopwatch sw = Stopwatch.StartNew();

            LoanValueEvaluator valueEvaluator = GetLoanValueEvaluatorCache(currentFileVersion);
            if (valueEvaluator == null)
            {
                valueEvaluator = new LoanValueEvaluator(principal.ConnectionInfo, principal.BrokerId, m_loanid, interestingOperations);
                valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(principal));
            }

            ReadOnlysLNm = valueEvaluator.GetSafeScalarString("sLNm", "");

            x_canRead = LendingQBExecutingEngine.CanPerform(WorkflowOperations.ReadLoanOrTemplate, valueEvaluator);

            if (isNeedToEvaluateCanWrite)
            {
                x_canWrite = LendingQBExecutingEngine.CanPerform(WorkflowOperations.WriteLoanOrTemplate, valueEvaluator);
                this.HasWriteLoanPermission = x_canWrite; // 11/17/2015 - We need a permission that rely on the OPERATION_WRITE_LOAN_OR_TEMPLATE.
            }

            if (loadValidator)
            {
                m_validator = new LoanFileFieldValidator(m_loanid, principal, valueEvaluator, x_canWrite);
                (m_validator as LoanFileFieldValidator).ReportAllWorkflowFailureMessages = this.ReportAllWorkflowFailureMessages;
                x_canWrite = x_canWrite || m_validator.HasFieldWritePermissions;
            }
            sw.Stop();

            PerformanceMonitorItem monitorItem = PerformanceMonitorItem.Current;
            if (null != monitorItem)
            {
                monitorItem.AddTimingDetail("CPageData.ResolvePermission()", sw.ElapsedMilliseconds);
            }
            // For the save method need to check dirty field.
        }

        private LoanValueEvaluator GetLoanValueEvaluatorCache(int fileVersion)
        {
            if (HttpContext.Current == null)
            {
                return null;
            }

            string key = LoanValueEvaluator.GetCacheKey(this.m_loanid, fileVersion);
            LoanValueEvaluator obj = HttpContext.Current.Items[key] as LoanValueEvaluator;

            return obj;
        }

        public virtual void InitLoad(AbstractUserPrincipal userP)
        {
            // Check if the page data implementation has turned off
            // access control checking.  If so, let the save pass
            // through as normal, unhindered.

            if (m_enforceAccessControl == true)
            {
                ResolvePermission(userP, true);

                if (x_canRead == false)
                {
                    AccessBinding accessBinding = null;

                    if (userP.IsOriginalPrincipalAConsumer)
                    {
                        // 10/28/2013 dd - Try not to expose too much information for consumer portal user.
                        accessBinding = new AccessBinding(Guid.Empty, Guid.Empty, false, false);
                    }
                    else
                    {
                        accessBinding = new AccessBinding(userP.UserId, this.sLId, false, false);
                        accessBinding.AddComment("User Login = " + userP.LoginNm);
                        accessBinding.AddComment("User Type = " + userP.Type);
                        accessBinding.AddComment("Loan ID=" + m_loanid);
                        accessBinding.AddComment("BrokerID=" + userP.BrokerId);
                    }
                    CBaseException exc = new PageDataLoadDenied(accessBinding);
                    exc.IsEmailDeveloper = false; // Don't email developer
                    throw exc;
                }
                else if (ConstStage.EnableSettingLoanFileFieldValidatorOnInitLoad)
                {
                    m_pageBase.SetLoanFileFieldValidator(m_validator);
                }
            }

            // Delegate to base implementation.

            m_pageBase.InitLoad();
        }

        public virtual void InitSave()
        {
            this.InitSave(ConstAppDavid.SkipVersionCheck);
        }

        /// <summary>
        /// Check user's credentials against saving.  User needs
        /// write access to pass.
        /// </summary>

        public virtual void InitSave(int expectedVersion)
        {
            // Check if the page data implementation has turned off
            // access control checking.  If so, let the save pass
            // through as normal, unhindered.

            if (m_enforceAccessControl == true)
            {
                AbstractUserPrincipal userP = Thread.CurrentPrincipal as AbstractUserPrincipal;
                if (BrokerUserPrincipal.CurrentPrincipal != null)
                {
                    userP = BrokerUserPrincipal.CurrentPrincipal;
                }

                ResolvePermission(userP, true);
                m_pageBase.SetLoanFileFieldValidator(m_validator);
                //m_pageBase.AddFieldsToTrack(m_validator.GetProtectedFields());
                if (x_canRead == false)
                {
                    AccessBinding accessBinding = new AccessBinding(userP.UserId, this.sLId, false, false);
                    accessBinding.AddComment("User Login = " + userP.LoginNm);
                    accessBinding.AddComment("User Type = " + userP.Type);
                    accessBinding.AddComment("Loan ID=" + m_loanid);
                    accessBinding.AddComment("BrokerID=" + userP.BrokerId);
                    CBaseException exc = new PageDataLoadDenied(accessBinding);
                    exc.IsEmailDeveloper = false; // Don't email developer
                    throw exc;
                }

            }
            else // if m_enforceAccessControl == false.
            {
                //AllowSaveWhileQP2Sandboxed = true; // opm 185732.  still too forgiving to allow this.
            }

            this.originalCollectionSnapshotsForLegacyWorkflow.Clear();
            this.collectionsToTrackForLegacyWorkflowOnceLoaded.Clear();

            m_pageBase.InitSave(expectedVersion);

            this.BeginTrackingCollectionsForLegacyWorkflow();
        }

        /// <summary>
        /// Gets or sets a value indicating whether the Loan File Validator will
        /// report all the workflow failure messages when saving.
        /// </summary>
        public bool ReportAllWorkflowFailureMessages { get; set; } = false;

        /// <summary>
        /// Override save and check access control of invoking
        /// user to see if save is allowed.
        /// </summary>
        public virtual void Save()
        {
            // Validates the housing expenses. Will throw an exception if it detects an error.
            m_pageBase.ValidateHousingExpenses();

            // Validate the sets first before flushing them. An exception should be thrown somewhere in these methods if a violation is detected.
            // Also, don't bother with validation if m_enforceAccessControl is false.
            m_pageBase.ValidateBorrowerClosingCostSet(m_enforceAccessControl && !BypassClosingCostSetValidation);
            m_pageBase.ValidateSellerClosingCostSet(m_enforceAccessControl && !BypassClosingCostSetValidation);

            // Validate the Loan Estimate Dates and Closing Disclosure Dates. 
            // Exceptions should get thrown if it doesn't validate.
            m_pageBase.ValidateLoanEstimateDatesInfo(m_enforceAccessControl);
            m_pageBase.ValidateClosingDisclosureDatesInfo(m_enforceAccessControl);

            // At this point, the two sets should be validated.
            m_pageBase.Flush();
            // Check if the page data implementation has turned off
            // access control checking.  If so, let the save pass
            // through as normal, unhindered.
            if (m_enforceAccessControl == false)
            {
                m_pageBase.Save();
                return;
            }

            // This field is funky. We have to check it at the very end because its written such that it takes operations to
            // sClosingCostSet and has no flush. It has to be called after m_pageBase.Flush though. Ideally it would be in pagedataimpl 
            // but I cannot override the flush
            ValidateCustomCollections();

            
            m_validator.CheckProtectedFields(m_pageBase.GetChangedFields());

            foreach (var collectionSnapshot in this.originalCollectionSnapshotsForLegacyWorkflow)
            {
                var updatedDataSet = this.GetLoanLevelDataSetForCollection(collectionSnapshot.Key);
                m_validator.UpdateCollection(collectionSnapshot.Key, collectionSnapshot.Value, updatedDataSet);
            }

            // a field protection rule was broken
            if (false == m_validator.CanSave)    //false when the field protection is broken or field write is broken
            {
                if (true == TemporaryDidUserMakeChanges)
                {
                    throw m_validator.GetException();
                }
                else
                {
                    var loanFieldWritePermissionDenied = m_validator.GetException();
                    StringBuilder info = new StringBuilder("Field Security Rules Ignored ["); 
                    info.Append(loanFieldWritePermissionDenied.InvalidFieldIdChangeList.FirstOrDefault() ?? "");
                    info.AppendLine("] - Assign to SDE"); 
                    info.AppendLine(loanFieldWritePermissionDenied.DeveloperMessage);
                    if (HttpContext.Current != null && HttpContext.Current.Request != null)
                    {
                        info.AppendFormat("\nURL = {0}\n", HttpContext.Current.Request.RawUrl);
                        info.AppendFormat("Referrer : {0}\n", HttpContext.Current.Request.UrlReferrer);
                        foreach (string  key in HttpContext.Current.Request.Form.AllKeys)
                        {
                            info.AppendLine("Key: -" + HttpContext.Current.Request.Form[key] + "-");
                        }
                    }

                    Tools.LogInfoWithStackTrace(info.ToString());
                }
            }

            if (x_canWrite )
            {
                m_pageBase.Save();
                return;
            }

            //check for dirty fields -- why call save if there are no dirty fields? 
            bool hasDirtyFields = false;
            if (m_pageBase.dirtyFields != null)
            {
                foreach (DictionaryEntry dirtyField in m_pageBase.dirtyFields)
                {
                    if (null == dirtyField.Value)
                    {
                        continue; // not meant to be saved to db
                    }
                    hasDirtyFields = true;
                    break;
                }
            }

            if (hasDirtyFields == true)
            {
                AccessBinding accessBinding = new AccessBinding(Principal.UserId, this.sLId, false, false);
                accessBinding.AddComment("User Login = " + Principal.LoginNm);
                accessBinding.AddComment("User Type = " + Principal.Type);
                accessBinding.AddComment("Loan ID=" + m_loanid);
                accessBinding.AddComment("BrokerID=" + Principal.BrokerId);
                CBaseException exc = new PageDataSaveDenied(accessBinding);
                exc.IsEmailDeveloper = false; // Don't email developer
                throw exc;
            }
          
            // Delegate to base implementation.
            m_pageBase.Save();
        }

        /// <summary>
        /// A method that is called when a collection is loaded from the database via the collection container.
        /// </summary>
        /// <param name="uladCollectionName">The name of the collection.</param>
        public void NotifyCollectionLoad(string uladCollectionName)
        {
            if (!UladCollectionNameToLegacyWorkflowCollectionName.ContainsKey(uladCollectionName))
            {
                return;
            }

            // We are in the process of loading the loan file, keep track of the collection
            // name for us to track when the load is complete.
            IEnumerable<string> legacyCollectionNamesToTrack = UladCollectionNameToLegacyWorkflowCollectionName[uladCollectionName];
            this.collectionsToTrackForLegacyWorkflowOnceLoaded.UnionWith(legacyCollectionNamesToTrack);
        }

        /// <summary>
        /// Begins tracking all of the collections that were loaded during the load process.<para/>
        /// Does nothing if the file is on the legacy data layer.
        /// </summary>
        protected void BeginTrackingCollectionsForLegacyWorkflow()
        {
            if (!this.m_enforceAccessControl)
            {
                return;
            }

            if (this.sBorrowerApplicationCollectionT == E_sLqbCollectionT.Legacy)
            {
                return;
            }

            foreach (var legacyWorkflowCollectionName in this.collectionsToTrackForLegacyWorkflowOnceLoaded)
            {
                if (this.originalCollectionSnapshotsForLegacyWorkflow.ContainsKey(legacyWorkflowCollectionName))
                {
                    continue;
                }

                DataSet dataSet = this.GetLoanLevelDataSetForCollection(legacyWorkflowCollectionName);
                this.originalCollectionSnapshotsForLegacyWorkflow.Add(legacyWorkflowCollectionName, dataSet);
            }
        }
    }

	/// <summary>
	/// Thrown when invoking user lacks permission to
	/// save the specified page data object.
	/// </summary>

	public class PageDataSaveDenied : PageDataAccessDenied
	{
		#region ( Constructors )

		/// <summary>
		/// Construct default.
		/// </summary>

		public PageDataSaveDenied( AccessBinding aB , Exception inner )
		: base( aB , "Page data save denied" , inner )
		{
		}

		/// <summary>
		/// Construct default.
		/// </summary>

		public PageDataSaveDenied( AccessBinding aB )
		: base( aB , "Page data save denied" )
		{
		}

		/// <summary>
		/// Construct default.
		/// </summary>

		public PageDataSaveDenied( Exception inner )
		: base( "Page data save denied" , inner )
		{
		}

        public PageDataSaveDenied(string devMessage)
            : base(devMessage)
        {
        }
		#endregion

	}

	/// <summary>
	/// Thrown when invoking user lacks permission to
	/// load the specified page data object.
	/// </summary>

	public class PageDataLoadDenied : PageDataAccessDenied
	{
		#region ( Constructors )

		/// <summary>
		/// Construct default.
		/// </summary>

		public PageDataLoadDenied( AccessBinding aB , Exception inner )
		: base( aB , "Page data load denied" , inner )
		{
		}

		/// <summary>
		/// Construct default.
		/// </summary>

		public PageDataLoadDenied( AccessBinding aB )
		: base( aB , "Page data load denied" )
		{
		}

		/// <summary>
		/// Construct default.
		/// </summary>

		public PageDataLoadDenied( Exception inner )
		: base( "Page data load denied" , inner )
		{
		}

        public PageDataLoadDenied(string devMessage) : base(devMessage)
        {

        }

		#endregion

	}

	/// <summary>
	/// Thrown when page data load or save fails because
	/// invoking user lacks permission.
	/// </summary>

	public class PageDataAccessDenied : CBaseException
	{
        private string m_stackTrace = "";
        public override string EmailSubjectCode 
        {
            get { return "LOAN_ACCESS_DENIED"; }
        }
		#region ( Constructors )

		/// <summary>
		/// Construct default.
		/// </summary>

		public PageDataAccessDenied( AccessBinding aB , String message , Exception inner )
		: base(ErrorMessages.GenericAccessDenied, message.TrimEnd( '.' ) + "\r\n\r\n" + inner.ToString() + "\r\n\r\n" + aB.ToString() )
		{

            m_stackTrace = Environment.StackTrace;
		}

        public override string StackTrace 
        {
            get { return m_stackTrace; }
        }
		/// <summary>
		/// Construct default.
		/// </summary>

		public PageDataAccessDenied( AccessBinding aB , String message )
		: base(ErrorMessages.GenericAccessDenied, message.TrimEnd( '.' ) + "\r\n\r\n" + aB.ToString())
		{
            m_stackTrace = Environment.StackTrace;
		}

		/// <summary>
		/// Construct default.
		/// </summary>

		public PageDataAccessDenied( String message , Exception inner )
		: base(ErrorMessages.GenericAccessDenied, message.TrimEnd( '.' ) + "." + "\r\n\r\n" + inner.ToString())
		{
            m_stackTrace = Environment.StackTrace;
		}

        public PageDataAccessDenied(string devMessage)
            : base(ErrorMessages.GenericAccessDenied, devMessage)
        {
        }

        public PageDataAccessDenied(AccessBinding aB, string userMsg, String message)
            : base(userMsg, message.TrimEnd('.') + "\r\n\r\n" + aB.ToString())
        {
            m_stackTrace = Environment.StackTrace;
        }
		#endregion

	}


    public class LoanDeletedPermissionDenied : CBaseException
    {
        public AbstractUserPrincipal Principal { get; private set; }
        public string sLNm { get; private set; }

        public LoanDeletedPermissionDenied(AbstractUserPrincipal principal, Guid sLId, string sLNm)
            : base("You do not have permission to delete loan " + sLNm,
                "You do not have permission to delete loan " + sLNm + ". Principal: " + principal)
        {
            this.sLNm = sLNm;
            this.Principal = principal;
            this.IsEmailDeveloper = false;
        }
    }

    public class DeleteApplicationPermissionDenied : CBaseException
    {
        public AbstractUserPrincipal Principal { get; private set; }
        public string sLNm { get; private set; }

        public DeleteApplicationPermissionDenied(AbstractUserPrincipal principal, Guid sLId, string sLNm)
            : base("You do not have permission to delete applications.",
                "User did not have permission to delete applications on loan file " + sLNm + ". Principal: " + principal)
        {
            this.sLNm = sLNm;
            this.Principal = principal;
            this.IsEmailDeveloper = false;
        }
    }

    public class DeleteSpousePermissionDenied : CBaseException
    {
        public DeleteSpousePermissionDenied()
            : base($"Cannot delete spouse.{Environment.NewLine}{Environment.NewLine}Write access to this loan is required to delete spouse.",
                  "User did not have write loan permission and attempted to delete spouse.")
        {
            this.IsEmailDeveloper = false;
        }
    }

    public class SwapBorrowerAndSpousePermissionDenied : CBaseException
    {
        public SwapBorrowerAndSpousePermissionDenied()
            : base($"Cannot swap borr & spouse.{Environment.NewLine}{Environment.NewLine}Write access to this loan is required to swap borr & spouse.",
                  "User did not have write loan permission and attempted to swap borr & spouse.")
        {
            this.IsEmailDeveloper = false;
        }
    }

    public class LoanFieldWritePermissionDenied : CBaseException
    {
        public IEnumerable<string> InvalidFieldIdChangeList { get; private set; }
        public bool IsFieldWriteFailure { get; private set; }

        //You do not have permission to write fields on this loan file. {Field Names}
        public LoanFieldWritePermissionDenied(bool isFieldWriteFailure, IEnumerable<string> fieldIds, string msg, string devMsg)
            : base(msg, devMsg)
        {
            IsFieldWriteFailure = IsFieldWriteFailure;
            InvalidFieldIdChangeList = fieldIds.ToList();
            IsEmailDeveloper = false; // 2/4/2013 dd - We should not email this denied exception to developer.
        }
    }
	/// <summary>
	/// This class should never be used unless you are 100% sure that  its okay to have access control turned off. 
	/// This should only be used when you are doing something on the systems'S behalf not the users.  ie event handling.
	/// </summary>
	public class CFullAccessPageData : CPageData 
	{
        public CFullAccessPageData(Guid loanId, IEnumerable<string> dependencyFields)
            : base(loanId, "CFP", dependencyFields)
        {
        }
		public CFullAccessPageData ( Guid loanId, string pageName, CSelectStatementProvider provider)  : base( loanId, pageName, provider) 
		{
			
		}
		override protected  bool m_enforceAccessControl
		{
			get {  return false ; }
		}
	}

    /// <summary>
    /// A marker interface for indicating types that have readable dependencies for resolving with smart dependency.
    /// </summary>
    public interface IHaveReadableDependencies
    {
    }
}
