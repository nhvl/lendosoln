﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{
    public class RespaFeeItem
    {
        private int m_props;

        public decimal? PricingDisplayOverrideValue { get; set; }
        public string PricingDescription { get; set; }  
 
        public RespaFeeItem(string hudline, string description, decimal fee, E_GfeSectionT gfeSectionT, int props, string paidTo)
        {
            DisclosureSectionT = E_IntegratedDisclosureSectionT.LeaveBlank;
            HudLine = hudline;
            Description = description;
            Fee = fee;
            GfeSectionT = gfeSectionT;
            PaidTo = paidTo;
            m_props = props;
            Source = E_ClosingFeeSource.LoanFile;
            PricingDescription = null;
            PricingDisplayOverrideValue = new decimal?();
        }

        public RespaFeeItem(string hudline, string description, decimal fee, E_GfeSectionT gfeSectionT, int props, string paidTo, E_IntegratedDisclosureSectionT ls)
        {
            DisclosureSectionT = ls;
            HudLine = hudline;
            Description = description;
            Fee = fee;
            GfeSectionT = gfeSectionT;
            PaidTo = paidTo;
            m_props = props;
            Source = E_ClosingFeeSource.LoanFile;
            PricingDescription = null;
            PricingDisplayOverrideValue = new decimal?();
        }

        public RespaFeeItem(string hudline, string description, decimal fee, E_GfeSectionT gfeSectionT, int props, string paidTo, E_IntegratedDisclosureSectionT ls, decimal borrowerFee):
            this(hudline, description, fee, gfeSectionT, props, paidTo, ls)
        {
            this.BorrowerFee = borrowerFee;
        }

        public RespaFeeItem(string hudline, string description, decimal fee, E_GfeSectionT gfeSectionT, int props, string paidTo, E_ClosingFeeSource source)
        {
            DisclosureSectionT = E_IntegratedDisclosureSectionT.SectionH;
            HudLine = hudline;
            Description = description;
            Fee = fee;
            GfeSectionT = gfeSectionT;
            PaidTo = paidTo;
            m_props = props;
            Source = source;
            PricingDescription = null;
            PricingDisplayOverrideValue = new decimal?();
        }

        public RespaFeeItem(string hudline, string description, decimal fee, E_GfeSectionT gfeSectionT, int props, string paidTo, E_ClosingFeeSource source, E_IntegratedDisclosureSectionT ls)
        {
            DisclosureSectionT = ls;
            HudLine = hudline;
            Description = description;
            Fee = fee;
            GfeSectionT = gfeSectionT;
            PaidTo = paidTo;
            m_props = props;
            Source = source;
            PricingDescription = null;
            PricingDisplayOverrideValue = new decimal?();
        }

        public string HudLine { get; private set; }
        public decimal Fee { get; private set; }
        public decimal BorrowerFee { get; private set; }
        public string Description { get; private set; }
        public E_GfeSectionT GfeSectionT { get; private set; }
        public string PaidTo { get; private set; }
        public int Props { get { return m_props; } }
        public E_ClosingFeeSource Source { get; set; }
        public E_IntegratedDisclosureSectionT DisclosureSectionT { get; set; }
        public string DescriptionSource
        {
            get
            {
                switch (Source)
                {
                    case E_ClosingFeeSource.FeeService: return "Fee Service";
                    case E_ClosingFeeSource.LoanFile: return "Loan file setting";
                    case E_ClosingFeeSource.FirstAmerican: return "First American Quote";
                    case E_ClosingFeeSource.ClosingCostTemplate: return "Closing Cost Template";
                    default:
                        throw new UnhandledEnumException(Source);
                }
            }
        }

        public string DescriptionPaidTo
        {
            get
            {
                string desc = this.Description;
                if (!string.IsNullOrEmpty(PaidTo))
                {
                    desc += " - " + PaidTo;
                }
                return desc;
            }
        }
        public bool IsPaidToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(m_props); }
        }
    }
}
