namespace DataAccess
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using LendersOffice.Constants;
    using LendersOffice.HttpModule;
    using LendersOffice.ObjLib.Extensions;

    public enum E_ReadDBScenarioType
	{
		InputTargetFields_NeedTargetFields = 0, // for displaying and printing forms. We can use this to do complete cache update
		InputDirtyFields_NeedCachedFields = 1, // cached fields is a subset of loan fields, use this would help improve perf
		// This would be used for updating cached for each save of a form
		InputNothing_NeedAllCachedFields = 2, // try to use CSelectStatementProvider instead
		InputNothing_NeedAllFields = 3, // the granddaddy of them all, use this with care
        InputDirtyFields_NeedTargetAndTriggerFields = 4, // Expand the list one more time including fields needed for any triggers that MAY be triggered by this field set.
	}

	public class CSelectStatementProvider : ISelectStatementProvider
    {
        private static readonly LqbSingleThreadInitializeLazy<CSelectStatementProvider> s_allCachedFieldsProvider;
		private static readonly LqbSingleThreadInitializeLazy<CSelectStatementProvider> s_allFieldsProvider;

		static CSelectStatementProvider()
		{
			s_allCachedFieldsProvider = new LqbSingleThreadInitializeLazy<CSelectStatementProvider>(
                () => new CSelectStatementProvider(CFieldInfoTable.GetInstance().CachingSet.UpdateFields.Collection, expandTriggersIfNeeded: false, retainInputList: false));

            // passing in all the leaves would be sufficient for all the targets (calc fields) as well.
            s_allFieldsProvider = new LqbSingleThreadInitializeLazy<CSelectStatementProvider>(
                () => new CSelectStatementProvider(CFieldDbInfoList.TheOnlyInstance.FieldNameList, expandTriggersIfNeeded: false, retainInputList: false));
		}

		public static CSelectStatementProvider GetProviderFor( E_ReadDBScenarioType scenario, ICollection inputFields )
		{
            using (PerformanceMonitorItem.Time($"CSelectStatementProvider.GetProviderFor({scenario}, #inputFields {inputFields?.Count ?? 0})"))
            {
                switch (scenario)
                {
                case E_ReadDBScenarioType.InputDirtyFields_NeedTargetAndTriggerFields:
                    return new CSelectStatementProvider(inputFields, expandTriggersIfNeeded: true, retainInputList: false);
                case E_ReadDBScenarioType.InputTargetFields_NeedTargetFields:
                    return new CSelectStatementProvider(inputFields, expandTriggersIfNeeded: false, retainInputList: false);
                case E_ReadDBScenarioType.InputDirtyFields_NeedCachedFields:
                    var targetFields = CFieldInfoTable.GetInstance().GatherAffectedCacheFields_Obsolete(inputFields).UpdateFields.StringCollection;
                    return new CSelectStatementProvider(targetFields, expandTriggersIfNeeded: false, retainInputList: ConstStage.UseAffectedCacheFieldListForInvalidatedCacheFields);
                case E_ReadDBScenarioType.InputNothing_NeedAllCachedFields:
                    return s_allCachedFieldsProvider.Value;
                case E_ReadDBScenarioType.InputNothing_NeedAllFields:
                    return s_allFieldsProvider.Value;
                default:
                    throw new UnhandledEnumException(scenario);
                }
            }
		}

        public static CSelectStatementProvider GetProviderForTargets( IEnumerable<string> inputFields, bool expandTriggersIfNeeded )
        {
            E_ReadDBScenarioType scenarioType = E_ReadDBScenarioType.InputTargetFields_NeedTargetFields;

            if (expandTriggersIfNeeded)
            {
                scenarioType = E_ReadDBScenarioType.InputDirtyFields_NeedTargetAndTriggerFields;
            }
            return GetProviderFor( scenarioType, inputFields.ToArray() );
        }

        private HashSet<string> m_fieldSet = null;

        private bool expandTriggersIfNeeded; 

        public IEnumerable<string> FieldSet
        {
            get { return m_fieldSet; }
        }

        public IEnumerable InputFieldList { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CSelectStatementProvider"/> class.
        /// </summary>
        /// <param name="targetFields">
        /// The list of target fields.
        /// </param>
        /// <param name="expandTriggersIfNeeded">
        /// True to expand trigger fields when necessary, false otherwise.
        /// </param>
        /// <param name="retainInputList">
        /// True to retain the list of input target fields, false otherwise.
        /// </param>
        private CSelectStatementProvider(ICollection targetFields, bool expandTriggersIfNeeded, bool retainInputList)
        {
            if (retainInputList)
            {
                this.InputFieldList = targetFields;
            }

            this.expandTriggersIfNeeded = expandTriggersIfNeeded;

			CFieldInfoTable fieldInfoTable = CFieldInfoTable.GetInstance();

            m_fieldSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

            DependResult dependResult = null;
            using (PerformanceMonitorItem.Time("CSelectStatementProvider call to GatherRequiredIndepFields_OBSOLETE with expandTriggersIfNeeded = " + expandTriggersIfNeeded))
            {
                dependResult = fieldInfoTable.GatherRequiredIndepFields_OBSOLETE(targetFields, expandTriggersIfNeeded);
            }

            foreach ( var fieldNm in dependResult.DbFields.GetFieldSpecList() )
			{
                m_fieldSet.Add(fieldNm.Name);
			}

		}

        /// <summary>
        /// Initializes a new instance of the <see cref="CSelectStatementProvider"/> class.
        /// </summary>
        /// <param name="targetFields">
        /// The list of target fields.
        /// </param>
        /// <param name="expandTriggersIfNeeded">
        /// True to expand trigger fields when necessary, false otherwise.
        /// </param>
        /// <param name="retainInputList">
        /// True to retain the list of input target fields, false otherwise.
        /// </param>
        private CSelectStatementProvider(IEnumerable<string> targetFields, bool expandTriggersIfNeeded, bool retainInputList)
        {
            if (retainInputList)
            {
                this.InputFieldList = targetFields;
            }

            this.expandTriggersIfNeeded = expandTriggersIfNeeded;

            CFieldInfoTable fieldInfoTable = CFieldInfoTable.GetInstance();

            m_fieldSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

            DependResult dependResult = null;
            
            using (PerformanceMonitorItem.Time("CSelectStatementProvider call to GatherRequiredIndepFields with expandTriggersIfNeeded = " + expandTriggersIfNeeded))
            {
                dependResult = fieldInfoTable.GatherRequiredIndepFields(targetFields, expandTriggersIfNeeded);
            }

            foreach (var item in dependResult.DbFields.GetFieldSpecList())
            {
                m_fieldSet.Add(item.Name);
            }
        }

        public class SqlSelectStatementItem
        {
            public string LoanSelectFormat { get; set; }
            public string AppSelectFormat { get; set; }
            public string AssignedEmployeeSelectFormat { get; set; }
            public string AssignedTeamSelectFormat { get; set; }

            public string SubmissionSnapshotSelectFormat { get; set; }

            public string Query_LoanFileA { get; set; }
            public string Query_LoanFileB { get; set; }
            public string Query_LoanFileC { get; set; }
            public string Query_LoanFileD { get; set; }
            public string Query_LoanFileE { get; set; }
            public string Query_LoanFileF { get; set; }

            /// <summary>
            /// OPM 457741 - If true, loan should load UcdDeliveryCollection during initialization.
            /// </summary>
            public bool IncludeUcdDeliveryCollection { get; set; }

            /// <summary>
            /// The LqbCollections that have been regsitered and will need to be available for loading and saving.
            /// </summary>
            public IEnumerable<string> RegisteredLqbCollections { get; set; }

            /// <summary>
            ///Branch table requires @BranchId command parameter from query F.
            /// </summary>
            public string Query_Branch { get; set; }

            public string Query_TrustedAccount { get; set; }
        }

        public SqlSelectStatementItem GenerateSelectStatement(HashSet<string> additionalInputFields) 
        {
            SqlSelectStatementItem sqlSelectItem = GenerateSelectStatementImpl(additionalInputFields); ;
            return sqlSelectItem;
        }

        /// <summary>
        /// Generates a query SELECT fields FROM TableName WHERE PrimaryKey = @PrimaryKey
        /// </summary>
        /// <param name="primaryKey"></param>
        /// <param name="tableName"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        private string GenerateQuery(string primaryKey, string tableName, IEnumerable<string> fields)
        {
            if (!fields.Any())
            {
                return null;
            }

            var sb = new StringBuilder("select ");
            sb.Append(this.GenerateFieldSelectList(fields));
            sb.AppendFormat(" FROM {0} WHERE {1} = @{1}", tableName, primaryKey);
            return sb.ToString();
        }

        /// <summary>
        /// Generates a comma-separated list of columns to select or * if the number of columns in
        /// <paramref name="fields"/> is greater than <see cref="ConstApp.MaxSqlQueryColumns"/>.
        /// </summary>
        /// <param name="fields">
        /// The fields to generate a select string.
        /// </param>
        /// <returns>
        /// The comma-separated list or * if the number of fields exceeds the max number of columns.
        /// </returns>
        private string GenerateFieldSelectList(IEnumerable<string> fields)
        {
            if (fields.Count() > ConstApp.MaxSqlQueryColumns)
            {
                Tools.LogWarning($"{nameof(CSelectStatementProvider)}:{nameof(GenerateFieldSelectList)} - Exceeded {ConstApp.MaxSqlQueryColumns} SQL query fields. Selected fields: " + string.Join(",", fields));
                return "*";
            }

            return string.Join(",", fields);
        }

        private SqlSelectStatementItem GenerateSelectStatementImpl(IEnumerable<string> additionalInputFields)
        {
            HashSet<string> hashSet = new HashSet<string>(FieldSet, StringComparer.OrdinalIgnoreCase); // 12/15/2011 dd - Need to create a clone, so we don't modify original set.

            if (additionalInputFields != null)
            {
                hashSet.UnionWith(additionalInputFields);
            }

            StringBuilder loanSelect = new StringBuilder(5000);
            StringBuilder appSelect = new StringBuilder(5000);

            var selectFields = SqlSelectStatementFields.Create();

            selectFields.LoanFileA.Add("sLId");
            selectFields.LoanFileE.Add("sBrokerId");
            selectFields.LoanFileA.Add("sIntegrationT");
            selectFields.LoanFileE.Add("sCustomField1D");
            selectFields.LoanFileA.Add("sFileVersion");
            selectFields.LoanFileA.Add("IsTemplate");
            selectFields.LoanFileA.Add("sAuditTrackedFieldChanges");
            selectFields.LoanFileD.Add("sComplianceEaseStatus");
            selectFields.LoanFileE.Add("sLNm");
            selectFields.LoanFileF.Add("sBranchId");
            selectFields.LoanFileB.Add("sLoanFileT");

            // Add encryption fields to LOAN_FILE_A-specific query
            // and general loan select query to ensure they are loaded
            // for InitLoad and InitSave.
            selectFields.LoanFileA.Add("sEncryptionKey");
            selectFields.LoanSelect.Add("sEncryptionKey");

            selectFields.LoanFileA.Add("sEncryptionMigrationVersion");
            selectFields.LoanSelect.Add("sEncryptionMigrationVersion");

            selectFields.LoanFileA.Add("sBorrowerApplicationCollectionT");
            selectFields.LoanSelect.Add("sBorrowerApplicationCollectionT");

            string loanAQuery = null;
            string loanBQuery = null;
            string loanCQuery = null;
            string loanDQuery = null;
            string loanEQuery = null;
            string loanFQuery = null;
            string loanBranchQuery = null;
            string loanTrustedAccountQuery = null;

            bool bNeedLoanA = true;// because sIntegrationT must always be needed in the save method
            bool bNeedLoanB = true;// because sLoanFileT must always be used in the save method. opm 185732
            bool bNeedLoanC = false;
            bool bNeedLoanD = false;
            bool bNeedLoanE = true;// because sBrokerId is now assumed to be loaded with every loan
            bool bNeedLoanF = true;
            bool bNeedTrustAccount = false;
            bool bNeedAppA = true;
            bool bNeedAppB = false;
            bool bNeedEmployee = false;
            bool bNeedBranch = false;
            bool bNeedTeam = false;
            bool bNeedSubmissionSnapshots = false;
            bool bIncludeUcdDeliveryCollection = false;
            Lazy<HashSet<string>> registeredLqbCollections = new Lazy<HashSet<string>>();

            CFieldInfoTable fieldInfoTable = CFieldInfoTable.GetInstance();

            DependResult dependResult = null;
            using (PerformanceMonitorItem.Time("GenerateSelectStatementImpl call to GatherRequiredIndepFields with numFields = " + hashSet.Count + " and expandTriggersIfNeeded " + this.expandTriggersIfNeeded))
            {
                dependResult = fieldInfoTable.GatherRequiredIndepFields(hashSet, this.expandTriggersIfNeeded);
            }

            foreach (var field in dependResult.DbFields.GetFieldSpecList())
            {
                string fieldNm = field.Name;
                FieldProperties props = fieldInfoTable[fieldNm];
                if (props.DbInfo == null)
                {
                    if (fieldNm == "TableEmployee")
                    {
                        bNeedEmployee = true;
                    }
                    else if (fieldNm == "TableTeam")
                    {
                        bNeedTeam = true;
                    }
                    else if (fieldNm == nameof(CPageBase.SubmissionSnapshotTable))
                    {
                        bNeedSubmissionSnapshots = true;
                    }
                    else if (fieldNm == nameof(CPageBase.sUcdDeliveryCollection))
                    {
                        bIncludeUcdDeliveryCollection = true;
                    }
                    else if (LoanLqbCollectionContainer.IsLoanLqbCollection(fieldNm))
                    {
                        registeredLqbCollections.Value.Add(fieldNm);
                    }

                    continue;
                }

                switch (props.DbInfo.m_dbTable)
                {
                    case E_DbSrcTable.Loan_A:
                        selectFields.LoanSelect.Add(fieldNm);
                        selectFields.LoanFileA.Add(fieldNm);

                        break;
                    case E_DbSrcTable.Loan_B:
                        bNeedLoanB = true;
                        selectFields.LoanSelect.Add(fieldNm);
                        selectFields.LoanFileB.Add(fieldNm);
                        break;
                    case E_DbSrcTable.Loan_C:
                        bNeedLoanC = true;
                        selectFields.LoanSelect.Add(fieldNm);
                        selectFields.LoanFileC.Add(fieldNm);
                        break;
                    case E_DbSrcTable.Loan_D:
                        bNeedLoanD = true;
                        selectFields.LoanSelect.Add(fieldNm);
                        selectFields.LoanFileD.Add(fieldNm);
                        break;
                    case E_DbSrcTable.Loan_E:
                        bNeedLoanE = true;
                        selectFields.LoanSelect.Add(fieldNm);
                        selectFields.LoanFileE.Add(fieldNm);
                        break;
                    case E_DbSrcTable.Loan_F:
                        bNeedLoanF = true;
                        selectFields.LoanSelect.Add(fieldNm);
                        selectFields.LoanFileF.Add(fieldNm);

                        break;
                    case E_DbSrcTable.Trust_Account:
                        bNeedTrustAccount = true;
                        selectFields.LoanSelect.Add(fieldNm);
                        selectFields.LoanFileTrust.Add(fieldNm);

                        break;
                    case E_DbSrcTable.App_A:
                        bNeedAppA = true;
                        selectFields.AppSelect.Add(fieldNm);
                        break;
                    case E_DbSrcTable.App_B:
                        bNeedAppB = true;
                        selectFields.AppSelect.Add(fieldNm);
                        break;
                    case E_DbSrcTable.Employee:
                        bNeedEmployee = true;
                        break;
                    case E_DbSrcTable.Branch:
                        bNeedBranch = true;
                        selectFields.LoanSelect.Add(fieldNm);
                        selectFields.LoanFileBranch.Add(fieldNm);
                        break;
                    case E_DbSrcTable.Team:
                        bNeedTeam = true;
                        break;
                    default:
                        if (props.DbInfo.m_dbTable == E_DbSrcTable.None &&
                            props.DbInfo.m_cacheTable != E_DbCacheTable.None)
                        {
                            // There are some dependencies not present in the loan file
                            // or loan app tables but rather are present in the loan file
                            // cache, such as sEmployeeLoanRepId. In those cases, we'll 
                            // continue the loop.
                            break;
                        }

                        var msg = "Field '" + fieldNm + "' has both m_dbTable and m_cacheTable set to 'None' and cannot be processed. " +
                            "This is most likely caused by an error with the dependency generation mechanism.";

                        throw new UnhandledEnumException(props.DbInfo.m_dbTable, msg);
                } // switch
            } // foreach field

            if (bNeedLoanA || bNeedLoanB || bNeedLoanC || bNeedLoanD || bNeedLoanE || bNeedLoanF || bNeedTrustAccount || bNeedBranch)
            {
                StringBuilder from = new StringBuilder(80);
                StringBuilder where = new StringBuilder(80);

                loanAQuery = GenerateQuery("sLId", "loan_file_a", selectFields.LoanFileA);
                loanBQuery = GenerateQuery("sLId", "loan_file_b", selectFields.LoanFileB);
                loanCQuery = GenerateQuery("sLId", "loan_file_c", selectFields.LoanFileC);
                loanDQuery = GenerateQuery("sLId", "loan_file_d", selectFields.LoanFileD);
                loanEQuery = GenerateQuery("sLId", "loan_file_e", selectFields.LoanFileE);
                loanFQuery = GenerateQuery("sLId", "loan_file_f", selectFields.LoanFileF);

                loanBranchQuery = GenerateQuery("BranchId", "branch", selectFields.LoanFileBranch);
                loanTrustedAccountQuery = GenerateQuery("sLId", "trust_account", selectFields.LoanFileTrust);

                from.Append(" from loan_file_a La ");
                where.Append(" where La.sLId = @sLid ");

                if (bNeedLoanB)
                {
                    from.Append(", loan_file_b Lb ");
                    where.Append(" and Lb.sLId = La.sLId ");
                }

                if (bNeedLoanC)
                {
                    from.Append(", loan_file_c Lc ");
                    where.Append(" and Lc.sLId = La.sLId ");
                }

                // Needed for sComplianceEaseStatus
                from.Append(", loan_file_d Ld ");
                where.Append(" and Ld.sLId = La.sLId ");

                if (bNeedLoanE)
                {
                    from.Append(", loan_file_e Le ");
                    where.Append(" and Le.sLId = La.sLId ");
                }

                if (bNeedLoanF || bNeedBranch)
                {
                    // 7/6/2009 dd - Branch table need to link with LOAN_FILE_F through sBranchId

                    from.Append(", loan_file_f Lf ");
                    where.Append(" and Lf.sLId = La.sLId ");
                }
                if (bNeedBranch)
                {
                    from.Append(", branch br ");
                    where.Append(" and Lf.sBranchId = br.BranchId ");
                }

                if (bNeedTrustAccount)
                {
                    from.Append(", trust_account trust ");
                    where.Append(" and trust.sLId = La.sLId ");
                }

                if (ConstStage.IsLoanForceOrderHintEnabled)
                {
                    where.Append(" OPTION (FORCE ORDER)");
                }
                
                loanSelect.Append("SELECT ");
                loanSelect.Append(this.GenerateFieldSelectList(selectFields.LoanSelect));
                loanSelect.Append(from);
                loanSelect.Append(where);
            }

            if (bNeedAppA || bNeedAppB)
            {
                StringBuilder from = new StringBuilder(80);
                StringBuilder where = new StringBuilder(80);

                from.Append(" from application_a Aa ");
                where.Append(" where Aa.sLId = @sLId ");

                if (bNeedAppB)
                {
                    from.Append(", application_b Ab ");
                    where.Append(" and Ab.aAppId = Aa.aAppId ");
                }

                appSelect.Append("SELECT ");
                appSelect.Append(this.GenerateFieldSelectList(selectFields.AppSelect));
                appSelect.Append(from);
                appSelect.Append(where);
                appSelect.Append(" order by PrimaryRankIndex ASC ");
            }

            string empSelect = string.Empty;
            if (bNeedEmployee)
            {
                empSelect = "select assignment.RoleId,assignment.sLId,employee.EmployeeId,employee.UserFirstNm,employee.UserLastNm,employee.Phone,employee.Fax,employee.Email,employee.EmployeeStartD,employee.EmployeeTerminationD," +
                            "pmlbroker.Name AS PmlBrokerName, pmlBroker.CompanyId as PmlBrokerCompanyId from LOAN_USER_ASSIGNMENT assignment JOIN EMPLOYEE employee ON assignment.EmployeeId=employee.EmployeeId LEFT JOIN Broker_User bu ON employee.EmployeeUserId = bu.UserId LEFT JOIN Pml_Broker pmlbroker ON bu.PmlBrokerId = pmlBroker.PmlBrokerId where assignment.sLId = @sLId";
            }

            string teamSelect = string.Empty;
            if (bNeedTeam)
            {
                teamSelect = "SELECT t.Id, t.Name, t.RoleId FROM TEAM t LEFT JOIN TEAM_LOAN_ASSIGNMENT tla ON t.Id = tla.TeamId WHERE tla.LoanId = @sLId";
            }

            string submissionSnapshotSelect = string.Empty;
            if (bNeedSubmissionSnapshots)
            {
                submissionSnapshotSelect = "SELECT Id, LoanId, BrokerId, SnapshotDataLastModifiedD, SnapshotFileKey, PriceGroupId, PriceGroupFileKey, FeeServiceRevisionId, SubmissionType, SubmissionDate FROM SUBMISSION_SNAPSHOT ss WHERE ss.LoanId = @sLId";
            }

            SqlSelectStatementItem sqlSelectStatementItem = new SqlSelectStatementItem();

            sqlSelectStatementItem.LoanSelectFormat = loanSelect.ToString();
            sqlSelectStatementItem.AppSelectFormat = appSelect.ToString();
            sqlSelectStatementItem.AssignedEmployeeSelectFormat = empSelect;
            sqlSelectStatementItem.AssignedTeamSelectFormat = teamSelect;
            sqlSelectStatementItem.SubmissionSnapshotSelectFormat = submissionSnapshotSelect;
            
            sqlSelectStatementItem.Query_Branch = loanBranchQuery;
            sqlSelectStatementItem.Query_LoanFileA = loanAQuery;
            sqlSelectStatementItem.Query_LoanFileB = loanBQuery;
            sqlSelectStatementItem.Query_LoanFileC = loanCQuery;
            sqlSelectStatementItem.Query_LoanFileD = loanDQuery;
            sqlSelectStatementItem.Query_LoanFileE = loanEQuery;
            sqlSelectStatementItem.Query_LoanFileF = loanFQuery;
            sqlSelectStatementItem.Query_TrustedAccount = loanTrustedAccountQuery;

            sqlSelectStatementItem.IncludeUcdDeliveryCollection = bIncludeUcdDeliveryCollection;
            sqlSelectStatementItem.RegisteredLqbCollections = registeredLqbCollections.IsValueCreated ? registeredLqbCollections.Value : Enumerable.Empty<string>();

            return sqlSelectStatementItem;
        }

        private class SqlSelectStatementFields
        {
            public static SqlSelectStatementFields Create()
            {
                if (ConstStage.UseSortedSetForSelectStatementProvider)
                {
                    var loanSelectFields = new SortedSet<string>(StringComparer.OrdinalIgnoreCase)
                    {
                        "La.sLId",
                        "sBrokerId",
                        "sIntegrationT",
                        "sCustomField1D",
                        "sFileVersion",
                        "IsTemplate",
                        "sAuditTrackedFieldChanges",
                        "sComplianceEaseStatus",
                        "sLNm",
                        "sBranchId",
                        "sLoanFileT"
                    };

                    var appSelectFields = new SortedSet<string>(StringComparer.OrdinalIgnoreCase)
                    {
                        "Aa.aAppId",
                        "aBConsumerId",
                        "aCConsumerId",
                        "aHasCoborrowerData"
                    };

                    return new SqlSelectStatementFields()
                    {
                        LoanFileA = new SortedSet<string>(StringComparer.OrdinalIgnoreCase),
                        LoanFileB = new SortedSet<string>(StringComparer.OrdinalIgnoreCase),
                        LoanFileC = new SortedSet<string>(StringComparer.OrdinalIgnoreCase),
                        LoanFileD = new SortedSet<string>(StringComparer.OrdinalIgnoreCase),
                        LoanFileE = new SortedSet<string>(StringComparer.OrdinalIgnoreCase),
                        LoanFileF = new SortedSet<string>(StringComparer.OrdinalIgnoreCase),
                        LoanFileBranch = new SortedSet<string>(StringComparer.OrdinalIgnoreCase),
                        LoanFileTrust = new SortedSet<string>(StringComparer.OrdinalIgnoreCase),
                        LoanSelect = loanSelectFields,
                        AppSelect = appSelectFields
                    };
                }
                else
                {
                    var loanSelectFields = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
                    {
                        "La.sLId",
                        "sBrokerId",
                        "sIntegrationT",
                        "sCustomField1D",
                        "sFileVersion",
                        "IsTemplate",
                        "sAuditTrackedFieldChanges",
                        "sComplianceEaseStatus",
                        "sLNm",
                        "sBranchId",
                        "sLoanFileT"
                    };

                    var appSelectFields = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
                    {
                        "Aa.aAppId",
                        "aBConsumerId",
                        "aCConsumerId",
                        "aHasCoborrowerData"
                    };

                    return new SqlSelectStatementFields()
                    {
                        LoanFileA = new HashSet<string>(),
                        LoanFileB = new HashSet<string>(),
                        LoanFileC = new HashSet<string>(),
                        LoanFileD = new HashSet<string>(),
                        LoanFileE = new HashSet<string>(),
                        LoanFileF = new HashSet<string>(),
                        LoanFileBranch = new HashSet<string>(),
                        LoanFileTrust = new HashSet<string>(),
                        LoanSelect = loanSelectFields,
                        AppSelect = appSelectFields
                    };
                }
            }

            public ICollection<string> LoanFileA { get; private set; }
            public ICollection<string> LoanFileB { get; private set; }
            public ICollection<string> LoanFileC { get; private set; }
            public ICollection<string> LoanFileD { get; private set; }
            public ICollection<string> LoanFileE { get; private set; }
            public ICollection<string> LoanFileF { get; private set; }
            public ICollection<string> LoanFileBranch { get; private set; }
            public ICollection<string> LoanFileTrust { get; private set; }
            public ICollection<string> LoanSelect { get; private set; }
            public ICollection<string> AppSelect { get; private set; }
        }
    }
}