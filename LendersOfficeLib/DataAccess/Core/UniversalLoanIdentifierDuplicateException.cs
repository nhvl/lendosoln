// <copyright file="UniversalLoanIdentifierDuplicateException.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Michael Leinweaver
//    Date:   01/23/2017
// </summary>
namespace DataAccess
{
    using LendersOffice.Common;

    /// <summary>
    /// Thrown if a loan with a conflicting Universal Loan Identifier (ULI) tries to save.
    /// </summary>
    public class UniversalLoanIdentifierDuplicateException : CBaseException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UniversalLoanIdentifierDuplicateException" /> class.
        /// </summary>
        public UniversalLoanIdentifierDuplicateException() 
            : base(JsMessages.LoanInfo_DuplicateUniversalLoanIdentifier, JsMessages.LoanInfo_DuplicateUniversalLoanIdentifier)
        {
            this.IsEmailDeveloper = false;
        }
    }
}
