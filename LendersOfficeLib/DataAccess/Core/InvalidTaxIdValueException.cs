﻿namespace DataAccess
{
    /// <summary>
    /// Specific exception that handles invalid tax id inputs from the user.
    /// </summary>
    public class InvalidTaxIdValueException : FieldInvalidValueException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidTaxIdValueException"/> class.
        /// </summary>
        /// <param name="fieldId">The ID of the field.</param>
        public InvalidTaxIdValueException(string fieldId)
            : base(fieldId, "Entered value")
        {
        }
    }
}
