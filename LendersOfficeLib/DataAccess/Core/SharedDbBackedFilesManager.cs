﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// A manager for files that do not change name or content with their meta data in the shared database and content in filedb.
    /// </summary>
    public static class SharedDbBackedFilesManager
    {
        /// <summary>
        /// Adds the file to the database and to filedb.<para></para>
        /// Only internal users and system users can create these files.
        /// </summary>
        /// <param name="principal">The principal adding the file to the filedb.</param>
        /// <param name="filePath">The filepath stored on the server where the file is originally stored.</param>
        /// <param name="uniqueName">The unique name for the file.  It will forever take this name.</param>
        /// <param name="fileType">The type of the file being stored.</param>
        /// <returns>The id associated with the file.</returns>
        public static int AddFileToDbAndFileDb(AbstractUserPrincipal principal, LocalFilePath filePath, string uniqueName, SharedDbBackedFileType fileType)
        {
            if (principal == null)
            {
                throw new ArgumentNullException(nameof(principal));
            }

            if (filePath == null)
            {
                throw new ArgumentNullException(nameof(filePath));
            }

            if (uniqueName == null)
            {
                throw new ArgumentNullException(nameof(uniqueName));
            }

            if (!(principal is InternalUserPrincipal || principal is SystemUserPrincipal))
            {
                throw new AccessDenied();
            }

            if (ExistsFileWithName(uniqueName, fileType))
            {
                throw new InvalidOperationException("You cannot upload a file whose name already exists.");
            }

            var fileKey = GetFileKey(fileType, uniqueName);

            if (FileDBTools.DoesFileExist(E_FileDB.Normal, fileKey))
            {
                throw new InvalidOperationException("This file already exists in filedb.");
            }

            FileDBTools.WriteFile(E_FileDB.Normal, fileKey, filePath.Value);

            return AddFileNameToDb(uniqueName, fileType);
        }        

        /// <summary>
        /// Gets all the LoXml file infos that have been saved.
        /// </summary>
        /// <returns>The list of LoXml file infos that have been saved.</returns>
        public static IEnumerable<SharedDbBackedFileInfo> GetAllLoXmlFileInfos()
        {
            return GetAllFileInfosOfFileType(SharedDbBackedFileType.LoXml);
        }

        /// <summary>
        /// Gets all the xsl file infos that have been saved.
        /// </summary>
        /// <returns>The list of xsl file infos that have been saved.</returns>
        public static IEnumerable<SharedDbBackedFileInfo> GetAllXslFiles()
        {
            return GetAllFileInfosOfFileType(SharedDbBackedFileType.Xsl);  
        }

        /// <summary>
        /// Gets the key to pull the file from filedb or disk.
        /// </summary>
        /// <param name="fileType">The type of file being requested.</param>
        /// <param name="name">The name of hte file.</param>
        /// <returns>The key to be used for retrieving from filedb or disk.</returns>
        public static string GetFileKey(SharedDbBackedFileType fileType, string name)
        {
            return GetDbNamePrefix(fileType) + "_" + name;
        }

        /// <summary>
        /// Adds the file name to the db for the specific type, or fails if the name already exists for that file type.
        /// </summary>
        /// <param name="uniqueName">The name that should be unique.</param>
        /// <param name="fileType">The type of file to add to the database.</param>
        /// <returns>The id of the file that was added to the database.</returns>
        private static int AddFileNameToDb(string uniqueName, SharedDbBackedFileType fileType)
        {
            if (uniqueName == null)
            {
                throw new ArgumentNullException(nameof(uniqueName));
            }

            var nameParameter = new SqlParameter("@Name", SqlDbType.VarChar, 255) { Value = uniqueName };
            var parameters = new List<SqlParameter>()
            {
                nameParameter
            };

            var sprocName = GetSprocName(fileType, "Add");
            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, sprocName, parameters))
            {
                if (reader.Read())
                {
                    return (int)reader["Id"];
                }
                else
                {
                    throw new InvalidOperationException($"Could not add {uniqueName} to the database.");
                }
            }
        }

        /// <summary>
        /// Checks if the file with the specified name and type exists in the database.
        /// </summary>
        /// <param name="name">The name to be checked.</param>
        /// <param name="fileType">The type of file to be checked.</param>
        /// <returns>True iff the filename already exists in the database.</returns>
        private static bool ExistsFileWithName(string name, SharedDbBackedFileType fileType)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }            

            var nameParameter = new SqlParameter("@Name", SqlDbType.VarChar, 255) { Value = name };
            var existsParameter = new SqlParameter("@FileExists", SqlDbType.Bit) { Direction = ParameterDirection.Output };
            var parameters = new List<SqlParameter>()
            {
                nameParameter,
                existsParameter
            };

            var sprocName = GetSprocName(fileType, "ExistsWithName");
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShareROnly, sprocName, 0, parameters);

            return (bool)existsParameter.Value;
        }

        /// <summary>
        /// The name of the sproc associated with that file type.
        /// </summary>
        /// <param name="fileType">The type of file of interest.</param>
        /// <param name="sprocSuffix">The sproc suffix.</param>
        /// <returns>The name of the sproc.</returns>
        private static string GetSprocName(SharedDbBackedFileType fileType, string sprocSuffix)
        {
            return GetDbNamePrefix(fileType) + "_FILE_" + sprocSuffix;
        }

        /// <summary>
        /// Gets the prefix name for the file type, which is used for constructing keys and sproc names.
        /// </summary>
        /// <param name="fileType">The type of file of interest.</param>
        /// <returns>The prefix associated with the file type.</returns>
        private static string GetDbNamePrefix(SharedDbBackedFileType fileType)
        {
            string prefix;
            switch (fileType)
            {
                case SharedDbBackedFileType.LoXml:
                    prefix = "LO_XML";
                    break;
                case SharedDbBackedFileType.Xsl:
                    prefix = "XSL";
                    break;
                default:
                    throw new UnhandledEnumException(fileType);
            }

            return prefix;
        }

        /// <summary>
        /// Gets all the file infos associated with that file type.
        /// </summary>
        /// <param name="fileType">The type of file of interest.</param>
        /// <returns>The list of file infos associated with that file type.</returns>
        private static IEnumerable<SharedDbBackedFileInfo> GetAllFileInfosOfFileType(SharedDbBackedFileType fileType)
        {
            var files = new List<SharedDbBackedFileInfo>();

            var sprocName = GetSprocName(fileType, "GetAll");
            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, sprocName))
            {
                while (reader.Read())
                {
                    var file = new SharedDbBackedFileInfo()
                    {
                        FileType = fileType,
                        FileName = (string)reader["Name"],
                        Id = (int)reader["Id"]
                    };

                    files.Add(file);
                }
            }

            return files;
        }
    }
}
