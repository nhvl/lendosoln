﻿using System;
using System.Collections.Generic;
using LendersOffice.Common;
using LendersOffice.Constants;

namespace DataAccess
{
    /// <summary>
    /// The algorithm for calculate FHA Premium calculation is from
    /// http://www.hud.gov/offices/hsg/comp/premiums/sfpcalc.cfm
    /// </summary>
    public class FhaPremiumCalculator
    {
        public static List<decimal> Calculate(decimal sLAmt, decimal sNoteIR, decimal annualMipRate, decimal upfrontFactor, decimal payment, int sDue)
        {
            List<decimal> list = ComputeAnnualAverageBalance(sLAmt, sNoteIR, payment, sDue);
            List<decimal> monthlyPremiumList = new List<decimal>(list.Count);
            foreach (var o in list)
            {
                decimal annualMip = Math.Round(o * annualMipRate / 100, 2, MidpointRounding.AwayFromZero);
                decimal a = Math.Round(annualMip / (1 + upfrontFactor / 100), 2, MidpointRounding.AwayFromZero);
                decimal monthly = Math.Round(a / 12, 2, MidpointRounding.AwayFromZero);
                monthlyPremiumList.Add(monthly);
            }

            return monthlyPremiumList;
        }

        private static List<decimal> ComputeAnnualAverageBalance(decimal outstandingBalance, decimal interestRate, decimal payment, int dueMonths)
        {
            if (outstandingBalance <= 0 || interestRate <= 0 || payment <= 0)
            {
                throw new CBaseException(ErrorMessages.Generic, "Invalid arguments for FhaPremiumCalculator. OutstandingBalance=" + outstandingBalance + ", InterestRate=" + interestRate + ", Payment=" + payment);
            }

            List<decimal> annualAverageBalanceList = new List<decimal>(30);

            try
            {
                // 5/4/2009 dd - The logic is from Computation of Annual Average Oustanding Balance.
                // http://www.hud.gov/offices/hsg/comp/premiums/sfpcalc.cfm.
                // a. Multiply previous balance times interest rate. Round the result to 2 decimal places based on value in 3rd decimal
                // b. Divide result by 1200.Round the result to 2 decimal places based on value in 3rd decimal.
                // c. Add previous balance.
                // d. Subtract p&i payment.

                decimal previousBalance = outstandingBalance;
                decimal totalAmount = previousBalance;
                int i = 1;
                int oddMonths = dueMonths % 12;
                int finalMonth = (oddMonths == 0) ? dueMonths : dueMonths + (12 - oddMonths); // Pad to a full year if there are odd months

                while (i <= finalMonth)
                {
                    decimal stepA = Math.Round(previousBalance * interestRate, 2, MidpointRounding.AwayFromZero);
                    decimal stepB = Math.Round(stepA / 1200, 2, MidpointRounding.AwayFromZero);
                    decimal stepC = previousBalance + stepB;
                    decimal stepD = stepC - payment;
                    if (stepD < 0)
                    {
                        stepD = 0;
                    }
                    previousBalance = stepD;
                    //Tools.LogError(i + " - D=" + stepD);
                    if (i % 12 == 0)
                    {
                        annualAverageBalanceList.Add(totalAmount / 12);
                        totalAmount = 0.0M;
                    }
                    totalAmount += stepD;

                    i++;
                }
            }
            catch (Exception e)
            {
                // opm 181056.  We want to get a feel for some of the inputs that cause errors.
                if (ConstStage.LogAnnualAverageBalanceComputationErrors)
                {
                    Tools.LogError(
                        "LogAnnualAverageBalanceComputationErrors const stage bit controls this logging.  "
                        + string.Format("FhaPremiumCalculator.cs > ComputeAnnualAverageBalance inputs were: outstandingBalance {0}, interestRate {1}, payment {2}"
                            , outstandingBalance // 0
                            , interestRate       // 1
                            , payment            // 2
                        )
                        , e
                    );
                }
                throw;
            }

            return annualAverageBalanceList;

        }
    }
}