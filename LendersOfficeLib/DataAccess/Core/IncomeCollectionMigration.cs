﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LendersOffice.Common;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Defines the methods necessary for migrating income on a loan file.
    /// </summary>
    public class IncomeCollectionMigration
    {
        /// <summary>
        /// Maps the income type to the corresponding other income description type.
        /// </summary>
        /// <param name="incomeType">The type of the income entry.</param>
        /// <returns>The other income description type to map it to.</returns>
        /// <remarks>This belongs to this class mainly because this is the reverse mapping to <see cref="MapToIncomeType(E_aOIDescT)"/>.</remarks>
        public static E_aOIDescT MapToOtherIncomeDescriptionType(IncomeType incomeType)
        {
            switch (incomeType)
            {
                case IncomeType.Other: return E_aOIDescT.Other;
                case IncomeType.MilitaryBasePay: return E_aOIDescT.MilitaryBasePay;
                case IncomeType.MilitaryRationsAllowance: return E_aOIDescT.MilitaryRationsAllowance;
                case IncomeType.MilitaryFlightPay: return E_aOIDescT.MilitaryFlightPay;
                case IncomeType.MilitaryHazardPay: return E_aOIDescT.MilitaryHazardPay;
                case IncomeType.MilitaryClothesAllowance: return E_aOIDescT.MilitaryClothesAllowance;
                case IncomeType.MilitaryQuartersAllowance: return E_aOIDescT.MilitaryQuartersAllowance;
                case IncomeType.MilitaryPropPay: return E_aOIDescT.MilitaryPropPay;
                case IncomeType.MilitaryOverseasPay: return E_aOIDescT.MilitaryOverseasPay;
                case IncomeType.MilitaryCombatPay: return E_aOIDescT.MilitaryCombatPay;
                case IncomeType.MilitaryVariableHousingAllowance: return E_aOIDescT.MilitaryVariableHousingAllowance;
                case IncomeType.AlimonyChildSupport: return E_aOIDescT.AlimonyChildSupport;
                case IncomeType.NotesReceivableInstallment: return E_aOIDescT.NotesReceivableInstallment;
                case IncomeType.PensionRetirement: return E_aOIDescT.PensionRetirement;
                case IncomeType.SocialSecurityDisability: return E_aOIDescT.SocialSecurityDisability;
                case IncomeType.RealEstateMortgageDifferential: return E_aOIDescT.RealEstateMortgageDifferential;
                case IncomeType.Trust: return E_aOIDescT.Trust;
                case IncomeType.UnemploymentWelfare: return E_aOIDescT.UnemploymentWelfare;
                case IncomeType.AutomobileExpenseAccount: return E_aOIDescT.AutomobileExpenseAccount;
                case IncomeType.FosterCare: return E_aOIDescT.FosterCare;
                case IncomeType.VABenefitsNonEducation: return E_aOIDescT.VABenefitsNonEducation;
                case IncomeType.CapitalGains: return E_aOIDescT.CapitalGains;
                case IncomeType.EmploymentRelatedAssets: return E_aOIDescT.EmploymentRelatedAssets;
                case IncomeType.ForeignIncome: return E_aOIDescT.ForeignIncome;
                case IncomeType.RoyaltyPayment: return E_aOIDescT.RoyaltyPayment;
                case IncomeType.SeasonalIncome: return E_aOIDescT.SeasonalIncome;
                case IncomeType.TemporaryLeave: return E_aOIDescT.TemporaryLeave;
                case IncomeType.TipIncome: return E_aOIDescT.TipIncome;
                case IncomeType.BoarderIncome: return E_aOIDescT.BoarderIncome;
                case IncomeType.MortgageCreditCertificate: return E_aOIDescT.MortgageCreditCertificate;
                case IncomeType.TrailingCoBorrowerIncome: return E_aOIDescT.TrailingCoBorrowerIncome;
                case IncomeType.AccessoryUnitIncome: return E_aOIDescT.AccessoryUnitIncome;
                case IncomeType.NonBorrowerHouseholdIncome: return E_aOIDescT.NonBorrowerHouseholdIncome;
                case IncomeType.HousingChoiceVoucher: return E_aOIDescT.HousingChoiceVoucher;
                case IncomeType.SocialSecurity: return E_aOIDescT.SocialSecurity;
                case IncomeType.Disability: return E_aOIDescT.Disability;
                case IncomeType.Alimony: return E_aOIDescT.Alimony;
                case IncomeType.ChildSupport: return E_aOIDescT.ChildSupport;
                case IncomeType.ContractBasis: return E_aOIDescT.ContractBasis;
                case IncomeType.DefinedContributionPlan: return E_aOIDescT.DefinedContributionPlan;
                case IncomeType.HousingAllowance: return E_aOIDescT.HousingAllowance;
                case IncomeType.MiscellaneousIncome: return E_aOIDescT.MiscellaneousIncome;
                case IncomeType.PublicAssistance: return E_aOIDescT.PublicAssistance;
                case IncomeType.WorkersCompensation: return E_aOIDescT.WorkersCompensation;
                case IncomeType.BaseIncome:
                case IncomeType.Overtime:
                case IncomeType.Bonuses:
                case IncomeType.Commission:
                case IncomeType.DividendsOrInterest: // These options are the old standard income types, and don't have other income types to map back to.
                default:
                    throw new UnhandledEnumException(incomeType);
            }
        }

        /// <summary>
        /// Gets the error message returned for a set of other income entries.
        /// </summary>
        /// <param name="otherIncomes">The other income entries to migrate.</param>
        /// <returns>The error message for these entries, or null if the migration should be successful.</returns>
        public static string GetErrorMessageForOtherIncome(IEnumerable<OtherIncome> otherIncomes)
        {
            if (otherIncomes.Any(i => OtherIncome.Get_aOIDescT(i.Desc) == E_aOIDescT.SubjPropNetCashFlow))
            {
                return "\"Subject Property Net Cash Flow\" is not permitted as an other income type.  Please remove this value from any other income entries and retry.";
            }

            return null;
        }

        /// <summary>
        /// Gets the migrated owners and income sources for the other income data specified.
        /// </summary>
        /// <param name="otherIncomes">The set of incomes.</param>
        /// <param name="borrowerConsumerId">This legacy application's primary borrower.</param>
        /// <param name="coborrowerConsumerId">This legacy application's secondary borrower.</param>
        /// <returns>The list of entries representing the owners and income source information.</returns>
        public static IEnumerable<Tuple<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, IncomeSource>> GetMigratedOtherIncome(IEnumerable<OtherIncome> otherIncomes, Guid borrowerConsumerId, Guid coborrowerConsumerId)
        {
            var result = new List<Tuple<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, IncomeSource>>();
            foreach (var otherIncome in otherIncomes)
            {
                IncomeType incomeType = MapToIncomeType(OtherIncome.Get_aOIDescT(otherIncome.Desc));
                string description = incomeType == IncomeType.Other ? otherIncome.Desc : null;
                Guid consumerId = otherIncome.IsForCoBorrower ? coborrowerConsumerId : borrowerConsumerId;
                result.AddIfNotNull(CreateIncomeSourcePair(incomeType, otherIncome.Amount, consumerId, description));
            }

            return result;
        }

        /// <summary>
        /// Gets the error message for the migration.
        /// </summary>
        /// <param name="loan">The loan to check the migration on.</param>
        /// <returns>The error message for the migration, or null if the migration should be successful.</returns>
        public static string GetErrorMessageForMigration(CPageData loan)
        {
            if (loan.sIsIncomeCollectionEnabled)
            {
                return "This loan is already using the new income data.";
            }
            else if (loan.sBorrowerApplicationCollectionT == E_sLqbCollectionT.Legacy)
            {
                return "This loan must be migrated to use the new LendingQB collection data before it can use collection-based income sources.";
            }
            else if (loan.sBorrowerApplicationCollectionT != E_sLqbCollectionT.UseLqbCollections)
            {
                throw CBaseException.GenericException("Only expected UseLqbCollections to be between Legacy and IncomeSourceCollection"); // this is a developer error, not something the user can fix
            }

            foreach (CAppData app in loan.Apps)
            {
                if (!app.aHasCoborrower && app.aCTotI != 0)
                {
                    return "Income is defined for a co-borrower not recognized by the system.  Please verify the co-borrower's basic information and income match and retry.";
                }

                string appLevelMessage = GetErrorMessageForOtherIncome(app.aOtherIncomeList);
                if (appLevelMessage != null)
                {
                    return appLevelMessage;
                }
            }

            return null;
        }

        /// <summary>
        /// Migrates a loan to use the new income collection data.
        /// </summary>
        /// <param name="loan">The loan to migrate.</param>
        internal static void MigrateLoan(CPageBase loan)
        {
            string migrationErrorMessage = GetErrorMessageForMigration(loan.InternalCPageData);
            if (!string.IsNullOrEmpty(migrationErrorMessage))
            {
                throw new CBaseException(migrationErrorMessage, "Unable to migrate loan");
            }

            loan.ClearIncomeSources(); // Existing income means an internal user put this loan back in an unmigrated state, so per Doug we want to clear everything that's already there.
            foreach (var consumerAndIncome in loan.Apps.SelectMany(app => ConvertToIncomeSourceCollection(app)))
            {
                DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId = consumerAndIncome.Item1;
                IncomeSource incomeSource = consumerAndIncome.Item2;
                loan.AddIncomeSource(consumerId, incomeSource);
            }
        }

        /// <summary>
        /// Converts a legacy application to a set of income sources with owners.
        /// </summary>
        /// <param name="legacyApp">The legacy application.</param>
        /// <returns>The set of income sources with owners.</returns>
        private static IReadOnlyList<Tuple<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, IncomeSource>> ConvertToIncomeSourceCollection(CAppBase legacyApp)
        {
            var list = new List<Tuple<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, IncomeSource>>();
            list.AddIfNotNull(CreateIncomeSourcePair(IncomeType.BaseIncome, legacyApp.aBBaseI, legacyApp.aBConsumerId));
            list.AddIfNotNull(CreateIncomeSourcePair(IncomeType.BaseIncome, legacyApp.aCBaseI, legacyApp.aCConsumerId));
            list.AddIfNotNull(CreateIncomeSourcePair(IncomeType.Overtime, legacyApp.aBOvertimeI, legacyApp.aBConsumerId));
            list.AddIfNotNull(CreateIncomeSourcePair(IncomeType.Overtime, legacyApp.aCOvertimeI, legacyApp.aCConsumerId));
            list.AddIfNotNull(CreateIncomeSourcePair(IncomeType.Bonuses, legacyApp.aBBonusesI, legacyApp.aBConsumerId));
            list.AddIfNotNull(CreateIncomeSourcePair(IncomeType.Bonuses, legacyApp.aCBonusesI, legacyApp.aCConsumerId));
            list.AddIfNotNull(CreateIncomeSourcePair(IncomeType.Commission, legacyApp.aBCommisionI, legacyApp.aBConsumerId));
            list.AddIfNotNull(CreateIncomeSourcePair(IncomeType.Commission, legacyApp.aCCommisionI, legacyApp.aCConsumerId));
            list.AddIfNotNull(CreateIncomeSourcePair(IncomeType.DividendsOrInterest, legacyApp.aBDividendI, legacyApp.aBConsumerId));
            list.AddIfNotNull(CreateIncomeSourcePair(IncomeType.DividendsOrInterest, legacyApp.aCDividendI, legacyApp.aCConsumerId));

            // Per Doug, we do not need to handle aBNetRentI/aCNetRentI
            foreach (var otherIncome in legacyApp.aOtherIncomeList)
            {
                IncomeType incomeType = MapToIncomeType(OtherIncome.Get_aOIDescT(otherIncome.Desc));
                string description = incomeType == IncomeType.Other ? otherIncome.Desc : null;
                Guid consumerId = otherIncome.IsForCoBorrower ? legacyApp.aCConsumerId : legacyApp.aBConsumerId;
                list.AddIfNotNull(CreateIncomeSourcePair(incomeType, otherIncome.Amount, consumerId, description));
            }

            return list;
        }

        /// <summary>
        /// Creates an income source and owner for a set of data.
        /// </summary>
        /// <param name="type">The type of the income.</param>
        /// <param name="monthlyAmount">The monthly amount of the income.</param>
        /// <param name="consumerId">The consumer who is receiving the income.</param>
        /// <param name="description">The description of the income, or null if no description is specified.</param>
        /// <returns>The income source and consumer identifier.</returns>
        private static Tuple<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, IncomeSource> CreateIncomeSourcePair(IncomeType type, decimal monthlyAmount, Guid consumerId, string description = null)
        {
            if (monthlyAmount == 0M)
            {
                return null;
            }
            else if (consumerId == Guid.Empty)
            {
                throw new CBaseException(ErrorMessages.Generic, "Invalid consumer id");
            }

            var source = new IncomeSource
            {
                IncomeType = type,
                Description = DescriptionField.Create(description),
                MonthlyAmountData = monthlyAmount,
            };
            return Tuple.Create(consumerId.ToIdentifier<DataObjectKind.Consumer>(), source);
        }

        /// <summary>
        /// Maps the other income description type to the newer income type.
        /// </summary>
        /// <param name="otherIncomeDescriptionType">The description type of the other income entry.</param>
        /// <returns>The income type to map it to.</returns>
        private static IncomeType MapToIncomeType(E_aOIDescT otherIncomeDescriptionType)
        {
            switch (otherIncomeDescriptionType)
            {
                case E_aOIDescT.Other: return IncomeType.Other;
                case E_aOIDescT.MilitaryBasePay: return IncomeType.MilitaryBasePay;
                case E_aOIDescT.MilitaryRationsAllowance: return IncomeType.MilitaryRationsAllowance;
                case E_aOIDescT.MilitaryFlightPay: return IncomeType.MilitaryFlightPay;
                case E_aOIDescT.MilitaryHazardPay: return IncomeType.MilitaryHazardPay;
                case E_aOIDescT.MilitaryClothesAllowance: return IncomeType.MilitaryClothesAllowance;
                case E_aOIDescT.MilitaryQuartersAllowance: return IncomeType.MilitaryQuartersAllowance;
                case E_aOIDescT.MilitaryPropPay: return IncomeType.MilitaryPropPay;
                case E_aOIDescT.MilitaryOverseasPay: return IncomeType.MilitaryOverseasPay;
                case E_aOIDescT.MilitaryCombatPay: return IncomeType.MilitaryCombatPay;
                case E_aOIDescT.MilitaryVariableHousingAllowance: return IncomeType.MilitaryVariableHousingAllowance;
                case E_aOIDescT.AlimonyChildSupport: return IncomeType.AlimonyChildSupport;
                case E_aOIDescT.NotesReceivableInstallment: return IncomeType.NotesReceivableInstallment;
                case E_aOIDescT.PensionRetirement: return IncomeType.PensionRetirement;
                case E_aOIDescT.SocialSecurityDisability: return IncomeType.SocialSecurityDisability;
                case E_aOIDescT.RealEstateMortgageDifferential: return IncomeType.RealEstateMortgageDifferential;
                case E_aOIDescT.Trust: return IncomeType.Trust;
                case E_aOIDescT.UnemploymentWelfare: return IncomeType.UnemploymentWelfare;
                case E_aOIDescT.AutomobileExpenseAccount: return IncomeType.AutomobileExpenseAccount;
                case E_aOIDescT.FosterCare: return IncomeType.FosterCare;
                case E_aOIDescT.VABenefitsNonEducation: return IncomeType.VABenefitsNonEducation;
                case E_aOIDescT.CapitalGains: return IncomeType.CapitalGains;
                case E_aOIDescT.EmploymentRelatedAssets: return IncomeType.EmploymentRelatedAssets;
                case E_aOIDescT.ForeignIncome: return IncomeType.ForeignIncome;
                case E_aOIDescT.RoyaltyPayment: return IncomeType.RoyaltyPayment;
                case E_aOIDescT.SeasonalIncome: return IncomeType.SeasonalIncome;
                case E_aOIDescT.TemporaryLeave: return IncomeType.TemporaryLeave;
                case E_aOIDescT.TipIncome: return IncomeType.TipIncome;
                case E_aOIDescT.BoarderIncome: return IncomeType.BoarderIncome;
                case E_aOIDescT.MortgageCreditCertificate: return IncomeType.MortgageCreditCertificate;
                case E_aOIDescT.TrailingCoBorrowerIncome: return IncomeType.TrailingCoBorrowerIncome;
                case E_aOIDescT.AccessoryUnitIncome: return IncomeType.AccessoryUnitIncome;
                case E_aOIDescT.NonBorrowerHouseholdIncome: return IncomeType.NonBorrowerHouseholdIncome;
                case E_aOIDescT.HousingChoiceVoucher: return IncomeType.HousingChoiceVoucher;
                case E_aOIDescT.SocialSecurity: return IncomeType.SocialSecurity;
                case E_aOIDescT.Disability: return IncomeType.Disability;
                case E_aOIDescT.Alimony: return IncomeType.Alimony;
                case E_aOIDescT.ChildSupport: return IncomeType.ChildSupport;
                case E_aOIDescT.ContractBasis: return IncomeType.ContractBasis;
                case E_aOIDescT.DefinedContributionPlan: return IncomeType.DefinedContributionPlan;
                case E_aOIDescT.HousingAllowance: return IncomeType.HousingAllowance;
                case E_aOIDescT.MiscellaneousIncome: return IncomeType.MiscellaneousIncome;
                case E_aOIDescT.PublicAssistance: return IncomeType.PublicAssistance;
                case E_aOIDescT.WorkersCompensation: return IncomeType.WorkersCompensation;
                case E_aOIDescT.SubjPropNetCashFlow:
                    // 2018-10-09 tj - Per conversation with Doug, we explicitly do not handle this value.
                    // Any code that gets here should have already checked this externally, so it is a bug
                    // that the migration code here was able to fire.
                default:
                    throw new UnhandledEnumException(otherIncomeDescriptionType);
            }
        }
    }
}
