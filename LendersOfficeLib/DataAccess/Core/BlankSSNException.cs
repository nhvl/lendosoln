﻿using System;

namespace DataAccess
{
    public class BlankSSNException : CBaseException
    {
        public BlankSSNException()
            : base("SSN cannot be blank.", "SSN cannot be blank.")
        { }
    }
}
