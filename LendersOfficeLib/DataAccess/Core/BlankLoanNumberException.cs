/// Author: David Dao

using System;

namespace DataAccess
{
	public class BlankLoanNumberException : CBaseException
	{
		public BlankLoanNumberException() : base("Loan number cannot be blank.", "Loan number cannot be blank.")
		{
		}
	}
}
