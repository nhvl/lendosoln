﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{
    /// <summary>
    /// // 10/21/2013 dd - In order to rush out the HELOC support for FirstTech (PML0223), we separate out the 
    /// HELOC calculation vs our normal loan calculation. When certain calculation for HELOC deviate from normal loan calculation 
    /// we should override here.
    /// 
    /// We use sIsLineOfCredit to determine whether to use this class or not. By the time this class instantiate sIsLineOfCredit should be true.
    /// </summary>
    public class CPageHelocBase : CPageBaseWrapped
    {
        public CPageHelocBase(Guid sLId, string namePage, CSelectStatementProvider sqlProvider)
            : base(sLId, namePage, sqlProvider)
        {
        }
        public CPageHelocBase(Guid fileId, string namePage, CSelectStatementProvider sqlProvider, Guid requestId, bool rerunningCacheUpdateRequest)
            : base(fileId, namePage, sqlProvider, requestId, rerunningCacheUpdateRequest)
        {
        }
        [DependsOn]
        protected override int sHelocDraw
        {
            get { return GetCountField("sHelocDraw");}
            set { SetCountField("sHelocDraw", value); }
        }

        [DependsOn(nameof(sDue), nameof(sHelocDraw))]
        protected override int sHelocRepay
        {
            get
            {
                return sDue - sHelocDraw;
            }
        }

        [DependsOn]
        public override E_sHelocPmtBaseT sHelocQualPmtBaseT
        {
            get
            {
                return (E_sHelocPmtBaseT)GetTypeIndexField("sHelocQualPmtBaseT");
            }
            set
            {
                SetTypeIndexField("sHelocQualPmtBaseT", (int)value);
            }
        }
        [DependsOn(nameof(sHelocQualPmtBaseT), nameof(sCreditLineAmt), nameof(sLAmtCalc))]
        protected override decimal sHelocQualPmtBaseAmt
        {
            get
            {
                switch (sHelocQualPmtBaseT)
                {
                    case E_sHelocPmtBaseT.LineAmount:
                        return sCreditLineAmt;
                    case E_sHelocPmtBaseT.InitialDraw:
                        return sLAmtCalc;
                    default:
                        throw new UnhandledEnumException(sHelocQualPmtBaseT);
                }
            }
        }

        [DependsOn(nameof(sHelocQualPmtPcBaseT), nameof(sQualIR))]
        public override E_sHelocPmtPcBaseT sHelocQualPmtPcBaseT
        {
            get
            {
                var selectedHelocQualPmtPcBaseT = GetTypeIndexField<E_sHelocPmtPcBaseT>("sHelocQualPmtPcBaseT");
                // 4/28/14 gf - opm 179668
                if (selectedHelocQualPmtPcBaseT == E_sHelocPmtPcBaseT.QualRate && sQualIR == 0)
                {
                    return E_sHelocPmtPcBaseT.NoteRate;
                }
                else
                {
                    return selectedHelocQualPmtPcBaseT;
                }
            }
            set
            {
                SetTypeIndexField("sHelocQualPmtPcBaseT", (int)value);
            }
        }

        [DependsOn(nameof(sHelocQualPmtPcBaseT))]
        public override bool sHelocQualPmtPcBaseReadOnly
        {
            get
            {
                return sHelocQualPmtPcBaseT != E_sHelocPmtPcBaseT.FixedPercent;
            }
        }

        [DependsOn(nameof(sHelocQualPmtFormulaT), nameof(sHelocQualPmtFormulaRateT), nameof(sQualIR), nameof(sNoteIR), nameof(sHelocQualPmtPcBase))]
        protected override decimal sHelocQualPmtPcBase
        {
            get
            {
                switch (sHelocQualPmtFormulaT)
                {
                    case E_sHelocPmtFormulaT.InterestOnly:
                    case E_sHelocPmtFormulaT.Amortizing:
                        return (sHelocQualPmtFormulaRateT == E_sHelocPmtFormulaRateT.QualRate) ? sQualIR : sNoteIR;
                    case E_sHelocPmtFormulaT.FixedPercent:
                        return GetRateField("sHelocQualPmtPcBase");
                    default:
                        throw new UnhandledEnumException(sHelocQualPmtFormulaT);
                }
            }
            set
            {
                SetRateField("sHelocQualPmtPcBase", value);
            }
        }
        [DependsOn]
        protected override decimal sHelocQualPmtMb
        {
            get { return GetMoneyField("sHelocQualPmtMb"); }
            set { SetMoneyField("sHelocQualPmtMb", value); }
        }











        [DependsOn]
        public override E_sHelocPmtBaseT sHelocPmtBaseT
        {
            get
            {
                return (E_sHelocPmtBaseT)GetTypeIndexField("sHelocPmtBaseT");
            }
            set
            {
                SetTypeIndexField("sHelocPmtBaseT", (int)value);
            }
        }
        [DependsOn(nameof(sHelocPmtBaseT), nameof(sCreditLineAmt), nameof(sLAmtCalc))]
        protected override decimal sHelocPmtBaseAmt
        {
            get
            {
                switch (sHelocPmtBaseT)
                {
                    case E_sHelocPmtBaseT.LineAmount:
                        return sCreditLineAmt;
                    case E_sHelocPmtBaseT.InitialDraw:
                        return sLAmtCalc;
                    default:
                        throw new UnhandledEnumException(sHelocPmtBaseT);
                }
            }
        }

        [DependsOn(nameof(sHelocPmtPcBaseT), nameof(sQualIR))]
        public override E_sHelocPmtPcBaseT sHelocPmtPcBaseT
        {
            get
            {
                var selectedHelocPmtPcBaseT = GetTypeIndexField<E_sHelocPmtPcBaseT>("sHelocPmtPcBaseT");
                // 4/28/14 gf - opm 179668
                if (selectedHelocPmtPcBaseT == E_sHelocPmtPcBaseT.QualRate && sQualIR == 0)
                {
                    return E_sHelocPmtPcBaseT.NoteRate;
                }
                else
                {
                    return selectedHelocPmtPcBaseT;
                }
            }
            set
            {
                SetTypeIndexField("sHelocPmtPcBaseT", (int)value);
            }
        }

        [DependsOn(nameof(sHelocPmtPcBaseT))]
        public override bool sHelocPmtPcBaseReadOnly
        {
            get
            {
                return sHelocPmtPcBaseT != E_sHelocPmtPcBaseT.FixedPercent;
            }
        }

        [DependsOn(nameof(sHelocPmtFormulaT), nameof(sQualIR), nameof(sNoteIR), nameof(sHelocPmtPcBase), nameof(sHelocPmtFormulaRateT))]
        protected override decimal sHelocPmtPcBase
        {
            get
            {
                switch (sHelocPmtFormulaT)
                {
                    case E_sHelocPmtFormulaT.InterestOnly:
                    case E_sHelocPmtFormulaT.Amortizing:
                        return (sHelocPmtFormulaRateT == E_sHelocPmtFormulaRateT.QualRate) ? sQualIR : sNoteIR;
                    case E_sHelocPmtFormulaT.FixedPercent:
                        return GetRateField("sHelocPmtPcBase");
                    default:
                        throw new UnhandledEnumException(sHelocPmtFormulaT);
                }
            }
            set
            {
                SetRateField("sHelocPmtPcBase", value);
            }
        }
        [DependsOn]
        protected override decimal sHelocPmtMb
        {
            get { return GetMoneyField("sHelocPmtMb"); }
            set { SetMoneyField("sHelocPmtMb", value); }
        }

        [DependsOn]
        public override bool sHelocPaidOutInitialDrawLckd
        {
            get { return GetBoolField("sHelocPaidOutInitialDrawLckd"); }
            set { SetBoolField("sHelocPaidOutInitialDrawLckd", value); }
        }

        [DependsOn(nameof(sHelocPaidOutInitialDrawLckd), nameof(sHelocPaidOutInitialDraw))]
        protected override decimal sHelocPaidOutInitialDraw
        {
            get
            {
                if (sHelocPaidOutInitialDrawLckd)
                {
                    return GetMoneyField("sHelocPaidOutInitialDraw");
                }
                // TODO;
                return base.sHelocPaidOutInitialDraw;
            }
            set
            {
                SetMoneyField("sHelocPaidOutInitialDraw", value);
            }
        }
        [DependsOn(nameof(sLAmtCalc), nameof(sHelocPaidOutInitialDraw))]
        protected override decimal sHelocCashToBorrower
        {
            get
            {
                return sLAmtCalc - sHelocPaidOutInitialDraw;
            }
        }

        [DependsOn(nameof(sHelocQualPmtFormulaT), nameof(sHelocQualPmtBaseAmt), nameof(sHelocQualPmtPcBase), nameof(sHelocQualPmtMb), nameof(sHelocQualPmtAmortTerm))]
        public override decimal sProThisMQual
        {
            get
            {
                decimal v;
                switch (sHelocQualPmtFormulaT)
                {
                    case E_sHelocPmtFormulaT.InterestOnly:
                        v = sHelocQualPmtBaseAmt * sHelocQualPmtPcBase / 1200 + sHelocQualPmtMb;
                        break;
                    case E_sHelocPmtFormulaT.Amortizing:
                        v = Tools.CalculatePayment(sHelocQualPmtBaseAmt, sHelocQualPmtPcBase, sHelocQualPmtAmortTerm) + sHelocQualPmtMb;
                        break;
                    case E_sHelocPmtFormulaT.FixedPercent:
                        v = sHelocQualPmtBaseAmt * sHelocQualPmtPcBase / 100 + sHelocQualPmtMb;
                        break;
                    default:
                        throw new UnhandledEnumException(sHelocQualPmtFormulaT);
                }

                // Tools.LogError("sProThisMQual  @ sNoteIR=" + sNoteIR + "/ sQualIR=" + sQualIR + " = " + v);
                return v;
            }
        }
        [DependsOn(nameof(sHelocPmtFormulaT), nameof(sHelocPmtBaseAmt), nameof(sHelocPmtPcBase), nameof(sHelocPmtMb), nameof(sHelocPmtAmortTerm))]
        public override decimal sProThisMPmt
        {
            get
            {
                decimal v;
                switch (sHelocPmtFormulaT)
                {
                    case E_sHelocPmtFormulaT.InterestOnly:
                        v = sHelocPmtBaseAmt * sHelocPmtPcBase / 1200 + sHelocPmtMb;
                        break;
                    case E_sHelocPmtFormulaT.Amortizing:
                        v = Tools.CalculatePayment(sHelocPmtBaseAmt, sHelocPmtPcBase, sHelocPmtAmortTerm) + sHelocPmtMb;
                        break;
                    case E_sHelocPmtFormulaT.FixedPercent:
                        v = sHelocPmtBaseAmt * sHelocPmtPcBase / 100 + sHelocPmtMb;
                        break;
                    default:
                        throw new UnhandledEnumException(sHelocPmtFormulaT);
                }

                // Tools.LogError("sProThisMPmt  @ sNoteIR=" + sNoteIR + "/ sQualIR=" + sQualIR + " = " + v);
                return v;
            }
        }

        [DependsOn]
        protected override decimal sHelocAnnualFee
        {
            get { return GetMoneyField("sHelocAnnualFee"); }
            set { SetMoneyField("sHelocAnnualFee", value); }
        }

        [DependsOn]
        protected override decimal sHelocDrawFee
        {
            get { return GetMoneyField("sHelocDrawFee"); }
            set { SetMoneyField("sHelocDrawFee", value); }
        }

        [DependsOn]
        protected override decimal sHelocMinimumAdvanceAmt
        {
            get { return GetMoneyField("sHelocMinimumAdvanceAmt"); }
            set { SetMoneyField("sHelocMinimumAdvanceAmt", value); }
        }

        [DependsOn]
        protected override decimal sHelocReturnedCheckFee
        {
            get { return GetMoneyField("sHelocReturnedCheckFee"); }
            set { SetMoneyField("sHelocReturnedCheckFee", value); }
        }

        [DependsOn]
        protected override decimal sHelocStopPaymentFee
        {
            get { return GetMoneyField("sHelocStopPaymentFee"); }
            set { SetMoneyField("sHelocStopPaymentFee", value); }
        }

        [DependsOn]
        protected override int sHelocTerminationFeePeriod
        {
            get { return GetCountField("sHelocTerminationFeePeriod"); }
            set { SetCountField("sHelocTerminationFeePeriod", value); }
        }

        [DependsOn]
        protected override decimal sHelocTerminationFee
        {
            get { return GetMoneyField("sHelocTerminationFee"); }
            set { SetMoneyField("sHelocTerminationFee", value); }
        }

        [DependsOn]
        protected override decimal sHelocMinimumPayment
        {
            get { return GetMoneyField("sHelocMinimumPayment"); }
            set { SetMoneyField("sHelocMinimumPayment", value); }
        }

        [DependsOn]
        protected override decimal sHelocMinimumPaymentPc
        {
            get { return GetRateField("sHelocMinimumPaymentPc"); }
            set { SetRateField("sHelocMinimumPaymentPc", value); }
        }

        [DependsOn]
        public override E_sHelocPmtFormulaT sHelocPmtFormulaT
        {
            get { return (E_sHelocPmtFormulaT)GetTypeIndexField("sHelocPmtFormulaT"); }
            set { SetTypeIndexField("sHelocPmtFormulaT", (int)value); }
        }

        [DependsOn]
        public override E_sHelocPmtFormulaRateT sHelocPmtFormulaRateT
        {
            get { return (E_sHelocPmtFormulaRateT)GetTypeIndexField("sHelocPmtFormulaRateT"); }
            set { SetTypeIndexField("sHelocPmtFormulaRateT", (int)value); }
        }

        [DependsOn]
        public override E_sHelocPmtAmortTermT sHelocPmtAmortTermT
        {
            get { return (E_sHelocPmtAmortTermT)GetTypeIndexField("sHelocPmtAmortTermT"); }
            set { SetTypeIndexField("sHelocPmtAmortTermT", (int)value); }
        }

        [DependsOn(nameof(sHelocDraw), nameof(sHelocRepay), nameof(sTerm), nameof(sDue), nameof(sHelocPmtAmortTermT))]
        public override int sHelocPmtAmortTerm
        {
            get
            {
                // OPM 245256. 
                switch (sHelocPmtAmortTermT)
                {
                    case E_sHelocPmtAmortTermT.DrawMonths:
                        return sHelocDraw;
                    case E_sHelocPmtAmortTermT.RepayMonths:
                        return sHelocRepay;
                    case E_sHelocPmtAmortTermT.TermMonths:
                        return sTerm;
                    case E_sHelocPmtAmortTermT.DueMonths:
                        return sDue;
                    default:
                        throw new UnhandledEnumException(sHelocPmtAmortTermT);
                }
            }
        }

        [DependsOn]
        public override E_sHelocPmtFormulaT sHelocQualPmtFormulaT
        {
            get { return (E_sHelocPmtFormulaT)GetTypeIndexField("sHelocQualPmtFormulaT"); }
            set { SetTypeIndexField("sHelocQualPmtFormulaT", (int)value); }
        }

        [DependsOn]
        public override E_sHelocPmtFormulaRateT sHelocQualPmtFormulaRateT
        {
            get { return (E_sHelocPmtFormulaRateT)GetTypeIndexField("sHelocQualPmtFormulaRateT"); }
            set { SetTypeIndexField("sHelocQualPmtFormulaRateT", (int)value); }
        }

        [DependsOn]
        public override E_sHelocPmtAmortTermT sHelocQualPmtAmortTermT
        {
            get { return (E_sHelocPmtAmortTermT)GetTypeIndexField("sHelocQualPmtAmortTermT"); }
            set { SetTypeIndexField("sHelocQualPmtAmortTermT", (int)value); }
        }

        [DependsOn(nameof(sHelocDraw), nameof(sHelocRepay), nameof(sTerm), nameof(sDue), nameof(sHelocQualPmtAmortTermT))]
        public override int sHelocQualPmtAmortTerm
        {
            get
            {
                // OPM 245256. 
                switch (sHelocQualPmtAmortTermT)
                {
                    case E_sHelocPmtAmortTermT.DrawMonths:
                        return sHelocDraw;
                    case E_sHelocPmtAmortTermT.RepayMonths:
                        return sHelocRepay;
                    case E_sHelocPmtAmortTermT.TermMonths:
                        return sTerm;
                    case E_sHelocPmtAmortTermT.DueMonths:
                        return sDue;
                    default:
                        throw new UnhandledEnumException(sHelocQualPmtAmortTermT);
                }
            }
        }

        [DependsOn(nameof(sRAdjIndexR), nameof(sRAdjMarginR))]
        public override decimal sFullyIndexedR
        {
            get
            {
                // 10/23/2013 dd - Always return the sum regardless of sFinMethT
                return sRAdjIndexR + sRAdjMarginR;
            }
        }
        [DependsOn]
        public override decimal sRAdjIndexR
        {
            get
            {
                return GetRateField("sRAdjIndexR");
            }
            set
            {
                base.sRAdjIndexR = value;
            }
        }

        public override decimal sRLifeCapR
        {
            get
            {
                return sNoteIR + sRAdjLifeCapR;
            }
            set
            {
                sRAdjLifeCapR = value - sNoteIR;
                base.sRLifeCapR = value;
            }
        }

        [DependsOn(nameof(sNoteIR))]
        public override decimal sApr
        {
            get
            {
                // 10/24/2013 dd - Per OPM 142318 - APR is just note rate for HELOC
                return sNoteIR;
            }
            set
            {
                
            }
        }

        public override AmortTable sAmortTable
        {
            get
            {
                throw new NotSupportedException();
            }
        }


        public override int sIPiaDy
        {
            // 10/25/2013 dd - Per OPM 142656 - HELOC does not have Pre-Paid Interest
            // 04/23/2018 je - OPM 469415 - If sHelocCalculatePrepaidInterest == true, then HELOC does have Pre-Paid Interest.
            get
            {
                if (sHelocCalculatePrepaidInterest)
                {
                    return base.sIPiaDy;
                }

                return 0;
            }
            set
            {
                if (sHelocCalculatePrepaidInterest)
                {
                    base.sIPiaDy = value;
                }
            }
        }

        public override E_sSubFinT sSubFinT
        {
            get
            {
                // 10/25/2013 dd - For HELOC loan always return HELOC.
                return E_sSubFinT.Heloc;
            }
            set
            {
            }
        }
        [DependsOn(nameof(sLienPosT), nameof(sCreditLineAmt))]
        public override decimal sSubFinPe
        {
            get
            {
                if (sLienPosT == E_sLienPosT.First)
                {
                    return sCreditLineAmt;
                }
                else
                {
                    return base.sSubFinPe;
                }
            }
            set
            {
                base.sSubFinPe = value;
            }
        }
        [DependsOn(nameof(sLienPosT), nameof(sSubFinPe), nameof(sProOFinBal), nameof(sfGetPropertyValue))]
        public override decimal sHCLTVRPe
        {
            get
            {
                decimal propertyValue = GetPropertyValue();
                if (propertyValue == 0M)
                {
                    return 0M;
                }

                if (sLienPosT == E_sLienPosT.First)
                {
                    return 100M * sSubFinPe  / propertyValue;
                }

                return 100M * (sSubFinPe + sProOFinBal) / propertyValue;
            }
        }

        public override bool sIs8020PricingScenario
        {
            // 10/29/2013 dd - Current not support combo loan if current loan is HELOC.
            get { return false; }
        }
    }
}
