/// Author: David Dao

using System;

namespace DataAccess
{
    public enum FormatTarget
    {
        Webform = 0,
        PrintoutNormalFields = 1,
        PrintoutImportantFields = 2,
        FannieMaeMornetPlus = 3,
        PointNative = 4,
        PriceEngineEval = 5,
        ReportResult = 6,
        FreddieMacAUS = 7,
        LON_Mismo = 8,
        DocMagic = 9,
        MismoClosing = 10,
        FannieMaeMornetPlusImportantFields = 11,
        DocuTech = 12,
        GinnieNet = 13,
        XsltExport = 14,
        LqbDataService = 15
    }

}
