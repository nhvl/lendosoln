﻿namespace DataAccess
{
    using System;

    /// <summary>
    /// Interface extracted from CVaPastLCollection.
    /// </summary>
    public interface IVaPastLoanCollection : IRecordCollection
    {
        /// <summary>
        /// Adds a record of the specified type.
        /// </summary>
        /// <param name="pastLT">The type of record to add.</param>
        /// <returns>The new record.</returns>
        IVaPastLoan AddRecord(E_VaPastLT pastLT);

        /// <summary>
        /// Adds a new VA past loan record.
        /// </summary>
        /// <returns>The new VA past loan record.</returns>
        new IVaPastLoan AddRegularRecord();

        /// <summary>
        /// Adds a regular record at the specified index.
        /// </summary>
        /// <param name="pos">The index.</param>
        /// <returns>The new record.</returns>
        new IVaPastLoan AddRegularRecordAt(int pos);

        /// <summary>
        /// Ensures that a record exists at the given index, creating a new record if
        /// one was not found.
        /// </summary>
        /// <param name="pos">The index.</param>
        /// <returns>The record associated with that index.</returns>
        IVaPastLoan EnsureRegularRecordAt(int pos);

        /// <summary>
        /// Gets the record associated with the given id.
        /// </summary>
        /// <param name="recordId">The record id.</param>
        /// <returns>The record associated with the id.</returns>
        new IVaPastLoan GetRegRecordOf(Guid recordId);

        /// <summary>
        /// Gets the record at the specified index.
        /// </summary>
        /// <param name="pos">The index.</param>
        /// <returns>The record for that index.</returns>
        new IVaPastLoan GetRegularRecordAt(int pos);

        /// <summary>
        /// Gets a subset of the records.
        /// </summary>
        /// <param name="inclusiveGroup">
        /// A value indicating whether the given group(s) should be included or excluded.
        /// </param>
        /// <param name="group">The record group types to either include or exclude.</param>
        /// <returns>The specified subset of records.</returns>
        ISubcollection GetSubcollection(bool inclusiveGroup, E_VaPastLGroupT group);
    }
}