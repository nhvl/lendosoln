﻿namespace DataAccess
{
    /// <summary>
    /// Defines an interface for observers of <see cref="ILoanLqbCollectionContainer"/> objects.
    /// </summary>
    public interface ILoanLqbCollectionContainerObserver
    {
        /// <summary>
        /// A method that is called when a collection is loaded from the database.
        /// </summary>
        /// <param name="collectionName">The name of the collection.</param>
        void NotifyCollectionLoad(string collectionName);
    }
}
