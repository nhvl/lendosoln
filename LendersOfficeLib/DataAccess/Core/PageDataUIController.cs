﻿// <copyright file="PageDataUIController.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: David Dao
//  Date:   8/14/2015
// </summary>

namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using LendersOffice.UI;

    /// <summary>
    /// Provide basic operations for loading, saving loan data object.
    /// </summary>
    public static class PageDataUIController
    {
        /// <summary>
        /// Load loan data with a given field list.
        /// </summary>
        /// <param name="loanId">A loan id of the file.</param>
        /// <param name="applicantId">An applicant id of the file.</param>
        /// <param name="fieldList">A list of fields.</param>
        /// <returns>An input field model.</returns>
        public static Dictionary<string, InputFieldModel> Load(Guid loanId, Guid applicantId, IEnumerable<string> fieldList)
        {
            CPageData dataLoan = new CPageData(loanId, fieldList);
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(applicantId);

            Dictionary<string, InputFieldModel> dictionary = new Dictionary<string, InputFieldModel>();

            foreach (var fieldId in fieldList)
            {
                if (PageDataUtilities.ContainsField(fieldId) == false)
                {
                    // Not a valid field id. Skip.
                    continue;
                }

                PageDataFieldInfo pageDataFieldInfo = PageDataUtilities.GetFieldInfo(fieldId);
                if (pageDataFieldInfo == null)
                {
                    continue;
                }

                InputFieldModel fieldModel = new InputFieldModel();

                fieldModel.Value = PageDataUtilities.GetValue(dataLoan, dataApp, fieldId);

                if (pageDataFieldInfo.HasSetter == false)
                {
                    fieldModel.IsReadOnly = true;
                }

                fieldModel.Type = Convert(pageDataFieldInfo.FieldType);

                if (fieldModel.IsReadOnly == false)
                {
                    // TODO: Need to do field protection check to see if need to disable this field.
                }

                dictionary.Add(fieldId, fieldModel);
            }

            //// Custom handling for special field should handle here.

            return dictionary;
        }

        /// <summary>
        /// Perform loan calculation.
        /// </summary>
        /// <param name="loanId">A loan id of the file.</param>
        /// <param name="applicantId">An applicant id of the file.</param>
        /// <param name="inputModel">A list of fields.</param>
        /// <returns>A result model.</returns>
        public static Dictionary<string, InputFieldModel> Calculate(Guid loanId, Guid applicantId, Dictionary<string, InputFieldModel> inputModel)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Perform loan save.
        /// </summary>
        /// <param name="loanId">A loan id of the file.</param>
        /// <param name="applicantId">An applicant id of the file.</param>
        /// <param name="inputModel">A list of fields.</param>
        /// <returns>A result model.</returns>
        public static Dictionary<string, InputFieldModel> Save(Guid loanId, Guid applicantId, Dictionary<string, InputFieldModel> inputModel)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Convert a page data field type to input field type.
        /// </summary>
        /// <param name="pageDataFieldType">A page data field type.</param>
        /// <returns>A input field type.</returns>
        private static InputFieldType Convert(E_PageDataFieldType pageDataFieldType)
        {
            switch (pageDataFieldType)
            {
                case E_PageDataFieldType.Unknown:
                    return InputFieldType.Unknown;
                case E_PageDataFieldType.String:
                    return InputFieldType.String;
                case E_PageDataFieldType.Money:
                    return InputFieldType.Money;
                case E_PageDataFieldType.Percent:
                    return InputFieldType.Percent;
                case E_PageDataFieldType.Ssn:
                    return InputFieldType.Ssn;
                case E_PageDataFieldType.Phone:
                    return InputFieldType.Phone;
                case E_PageDataFieldType.Integer:
                    return InputFieldType.Number;
                case E_PageDataFieldType.Bool:
                    return InputFieldType.Checkbox;
                case E_PageDataFieldType.Enum:
                    return InputFieldType.DropDownList;
                case E_PageDataFieldType.DateTime:
                    return InputFieldType.Date;
                case E_PageDataFieldType.Guid:
                    return InputFieldType.String;
                case E_PageDataFieldType.Decimal:
                    return InputFieldType.Number;
                case E_PageDataFieldType.Array:
                    return InputFieldType.DropDownList;
                default:
                    throw new UnhandledEnumException(pageDataFieldType);
            }
        }
    }
}