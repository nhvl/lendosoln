using System;
using System.Data;

namespace DataAccess
{
    public class CVaPastL : CXmlRecordBase2, IVaPastLoan
    {
        static internal IVaPastLoan Create(CAppBase parent, DataSet ds, CVaPastLCollection vaPastLColl)
        {
            return new CVaPastL(parent, ds, vaPastLColl);
        }

        static internal IVaPastLoan Recontruct(CAppBase parent, DataSet ds, CVaPastLCollection vaPastLColl, int iRow)
        {
            return new CVaPastL(parent, ds, vaPastLColl, iRow);
        }

        static internal IVaPastLoan Recontruct(CAppBase parent, DataSet ds, CVaPastLCollection vaPastLColl, Guid id)
        {
            return new CVaPastL(parent, ds, vaPastLColl, id);
        }


        // DO NOT USE!  CREATED JUST TO ENABLE SOME QUICK UNIT TESTING OF THE SHIM
        public CVaPastL(DataSet data, LosConvert converter, Guid recordId)
            : base(data, converter, recordId)
        {

        }

        /// <summary>
        /// Create new record
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="ds"></param>
        protected CVaPastL(CAppBase parent, DataSet ds, CVaPastLCollection vaPastLColl)
            : base(parent, ds, vaPastLColl, "VaPastLXmlContent")
        {
        }


        // To create new, call static method Create() instead
        protected CVaPastL(CAppBase parent, DataSet ds, CVaPastLCollection vaPastLColl, int iRow)
            : base(parent, ds, vaPastLColl, iRow, "VaPastLXmlContent")
        {
        }
        protected CVaPastL(CAppBase parent, DataSet ds, CVaPastLCollection vaPastLColl, Guid id)
            : base(parent, ds, vaPastLColl, id, "VaPastLXmlContent")
        {
        }

        public string LoanTypeDesc
        {
            get { return GetDescString("LoanTypeDesc"); }
            set { SetDescString("LoanTypeDesc", value); }
        }

        public string StAddr
        {
            get { return GetDescString("StAddr"); }
            set { SetDescString("StAddr", value); }
        }
        public string City
        {
            get { return GetDescString("City"); }
            set { SetDescString("City", value); }
        }
        public string State
        {
            get { return GetState("State"); }
            set { SetState("State", value); }
        }

        public string CityState
        {
            // 10/3/2011 dd - City & State field are obsolete. Starting from OPM 71905 we are using this single free text field.
            get { return GetDescString("CityState"); }
            set { SetDescString("CityState", value); }
        }
        public string DateOfLoan
        {
            get { return GetDescString("DateOfLoan"); }
            set { SetDescString("DateOfLoan", value); }
        }
        public string Zip
        {
            get { return GetZipCode("Zip"); }
            set { SetZipCode("Zip", value); }
        }

        public CDateTime LoanD
        {
            get { return GetDateTime("LoanD"); }
            set { SetDateTime("LoanD", value); }
        }

        public string LoanD_rep
        {
            get { return LoanD.ToString(m_convertLos); }
            set { LoanD = CDateTime.Create(value, m_convertLos); }
        }

        public E_TriState IsStillOwned
        {
            get { return (E_TriState)GetEnum("IsStillOwned", E_TriState.Blank); }
            set { SetEnum("IsStillOwned", value); }
        }

        public CDateTime PropSoldD
        {
            get { return GetDateTime("PropSoldD"); }
            set { SetDateTime("PropSoldD", value); }
        }

        public string PropSoldD_rep
        {
            get { return PropSoldD.ToString(m_convertLos); }
            set { PropSoldD = CDateTime.Create(value, m_convertLos); }
        }

        public string VaLoanNum
        {
            get { return GetDescString("VaLoanNum"); }
            set { SetDescString("VaLoanNum", value); }
        }

        public E_VaPastLT VaPastLT
        {
            get { return E_VaPastLT.Regular; }

        }
        /// <summary>
        /// This would be what subcollection of records will be based on.
        /// </summary>
        override public Enum KeyType
        {
            get { return VaPastLT; }
            set {; } // no op
        }

        override public void PrepareToFlush()
        {
            // no-op
        }
    }
}