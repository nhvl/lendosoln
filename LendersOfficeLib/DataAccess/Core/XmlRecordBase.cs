using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Data.Common;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using LendersOffice.Common;
using System.Linq;
using System.Linq.Expressions;
using LqbGrammar.DataTypes;

namespace DataAccess
{
    public enum E_XmlRecordBaseKeyType
    {
        Undefined = 0
    }

    public abstract class CXmlRecordBase : ICollectionItemBase
    {
        // ADDED TO ENABLE ADDING THE FOLLOWING CONSTRUCTOR
        public CXmlRecordBase()
        {

        }

        // DO NOT USE!  CREATED JUST TO ENABLE SOME QUICK UNIT TESTING OF THE SHIM
        private LosConvert m_converter;
        public CXmlRecordBase(LosConvert converter)
        {
            m_converter = converter;
        }

        public abstract void Update();
        public abstract bool IsValid { get; }
        protected abstract DataRow CurrentRow { get; }
        protected abstract CBase Parent { get; }

        protected abstract string TableName { get; }

        public abstract Enum KeyType { get; set; }

        protected string GetUniqueFieldName(string fieldNm)
        {
            if (this.IsValid)
            {
                return this.TableName + "." + this.KeyType + "." + fieldNm;
            }
            else
            {
                return string.Empty;
            }
        }

        protected LosConvert m_convertLos
        {
            get
            {
                if (m_converter != null) return m_converter;
                return (null == this.Parent || null == this.Parent.m_convertLos) ? new LosConvert() : this.Parent.m_convertLos;
            }
        }

        protected CDateTime GetDateTime(string strFieldNm)
        {
            if (this.IsValid)
            {
                // 6/1/2010 dd - Datetime store in xml is always mm/dd/yyyy
                return CDateTime.Create(this.GetString(strFieldNm), null);
            }

            return CDateTime.InvalidWrapValue;
        }

        protected void SetDateTime(string strFieldNm, CDateTime val)
        {
            if (IsValid)
            {
                if (null == val)
                {
                    this.SetString(strFieldNm, string.Empty);
                }
                else
                {
                    this.SetString(strFieldNm, val.ToString("MM/dd/yyyy"));
                }
            }
        }

        protected string GetPhoneNum(string strFieldNm)
        {
            if (this.IsValid)
            {
                return this.m_convertLos.ToPhoneNumFormat(this.GetString(strFieldNm), FormatDirection.ToRep);
            }

            return "";
        }

        protected void SetPhoneNum(string strFieldNm, string val)
        {
            if (this.IsValid)
            {
                this.SetString(strFieldNm, this.m_convertLos.ToPhoneNumFormat(val, FormatDirection.ToDb));
            }
        }

        protected int GetCount(string strFieldNm)
        {
            if (this.IsValid)
            {
                return this.m_convertLos.ToCount(this.GetString(strFieldNm));
            }

            return 0;
        }

        protected void SetCount(string strFieldNm, int val)
        {
            if (this.IsValid)
            {
                this.SetString(strFieldNm, this.m_convertLos.ToCountString(val));
            }
        }

        protected Guid GetGuid(string strFieldNm)
        {
            if (this.IsValid)
            {
                try
                {
                    string guidStr = this.GetString(strFieldNm);

                    return (guidStr.Length == 0) ? Guid.Empty : new Guid(guidStr);
                }
                catch {}
            }

            return Guid.Empty;
        }

        protected void SetGuid(string strFieldNm, Guid val)
        {
            if (this.IsValid)
            {
                this.SetString(strFieldNm, val.ToString());
            }
        }

        protected decimal GetMoney(string strFieldNm)
        {
            if (this.IsValid)
            {
                return this.m_convertLos.ToMoney(this.GetString(strFieldNm));
            }

            return 0;
        }

        protected void SetMoney(string strFieldNm, decimal val)
        {
            if (this.IsValid)
            {
                this.SetString(strFieldNm, this.m_convertLos.ToMoneyString(val, FormatDirection.ToXmlFieldDb));
            }
        }

        protected decimal GetRate(string strFieldNm)
        {
            if (this.IsValid)
            {
                return this.m_convertLos.ToRate(this.GetString(strFieldNm));
            }

            return 0;
        }

        protected void SetRate(string strFieldNm, decimal val)
        {
            if (this.IsValid)
            {
                this.SetString(strFieldNm, this.m_convertLos.ToRateString(val));
            }
        }

        protected Enum GetEnum(string strFieldNm, Enum fallbackReturnVal)
        {
            Enum ret = fallbackReturnVal;
            try
            {
                Type enumType = fallbackReturnVal.GetType();
                int rawVal = this.GetCount(strFieldNm);
                if (Enum.IsDefined(enumType, rawVal))
                {
                    ret = (Enum)Enum.Parse(enumType, rawVal.ToString());
                }
            }
            catch
            {
            }

            return ret;
        }

        protected void SetEnum(string strFieldNm, Enum val)
        {
            this.SetDescString(strFieldNm, val.ToString("D"));
        }

        protected string GetCountVarChar_rep(string strFieldNm)
        {
            try
            {
                if (this.IsValid)
                {
                    string raw = this.GetDescString(strFieldNm);
                    return this.m_convertLos.ToCountVarCharStringRep(raw);
                }
            }
            catch {}
            return string.Empty;
        }

        protected void SetCountVarChar_rep(string strFieldNm, string val)
        {
            this.SetDescString(strFieldNm, val);
        }

        protected string GetMoneyVarChar_rep(string strFieldNm)
        {
            try
            {
                if (this.IsValid)
                {
                    string raw = this.GetDescString(strFieldNm);
                    return this.m_convertLos.ToMoneyVarCharStringRep(raw);
                }
            }
            catch {}
            return string.Empty;
        }

        protected void SetMoneyVarChar_rep(string strFieldNm, string val)
        {
            this.SetDescString(strFieldNm, val);
        }

        protected string GetDatetimeVarChar_rep(string strFieldNm)
        {
            try
            {
                if (IsValid)
                {
                    string raw = this.GetDescString(strFieldNm);
                    return this.m_convertLos.ToDateTimeVarCharString(raw);
                }
            }
            catch {}
            return string.Empty;
        }

        protected void SetDatetimeVarChar_rep(string strFieldNm, string val)
        {
            this.SetDescString(strFieldNm, val);
        }
        protected string GetDescString(string strFieldNm)
        {
            if (this.IsValid)
            {
                return this.GetString(strFieldNm);
            }

            return string.Empty;
        }

        protected void SetDescString(string strFieldNm, Sensitive<string> val)
        {
            this.SetDescString(strFieldNm, val.Value);
        }
        protected void SetDescString(string strFieldNm, string val)
        {
            if (this.IsValid)
            {
                this.SetString(strFieldNm, val);
            }
        }
        protected bool GetBool(string strFieldNm)
        {
            if (!this.IsValid)
                return false;

            string s = this.GetString(strFieldNm).ToLower();
            return s == "true";
        }

        protected bool GetBool(string strFieldNm, bool bDefaultVal)
        {
            if (!this.IsValid)
            {
                return false;
            }

            string s = this.GetString(strFieldNm);

            if (string.IsNullOrEmpty(s))
            {
                return bDefaultVal;
            }

            switch (s.ToLower())
            {
                case "true": return true;
                case "false": return false;
                default: return bDefaultVal;
            }
        }

        protected void SetBool(string strFieldNm, bool val)
        {
            if (this.IsValid)
            {
                this.SetString(strFieldNm, val.ToString());
            }
        }

        protected string GetState(string strFieldNm)
        {
            if (this.IsValid)
            {
                return this.GetString(strFieldNm);
            }

            return string.Empty;
        }

        protected void SetState(string strFieldNm, string val)
        {
            if (this.IsValid)
            {
                if (null == val)
                {
                    Tools.LogBug("CXmlRecordBase.SetState() doesn't expect val to be null.");
                    val = "";
                }

                val = val.TrimWhitespaceAndBOM();
                val = val.Substring(0, Math.Min(2, val.Length));
                this.SetString(strFieldNm, val);
            }
        }

        protected string GetZipCode(string strFieldNm)
        {
            if (this.IsValid)
            {
                return this.GetString(strFieldNm);
            }

            return string.Empty;
        }

        protected void SetZipCode(string strFieldNm, string val)
        {
            if (this.IsValid)
            {
                if (null == val)
                {
                    Tools.LogBug("CXmlRecordBase.SetZipCode() doesn't expect val to be null.");
                    val = "";
                }
                else
                {
                    val = val.TrimWhitespaceAndBOM();
                    val = val.Substring(0, Math.Min(5, val.Length));
                }

                this.SetString(strFieldNm, val);
            }
        }

        protected T GetData<T>(string fieldName)
        {
            if (this.IsValid)
            {
                object data = this.CurrentRow[fieldName];
                if (data == null || data == DBNull.Value ||
                    data.GetType() != typeof(T))
                {
                    return default(T);
                }

                return (T)data;
            }

            return default(T);
        }

        protected void SetData<T>(string fieldName, T data)
        {
            if (this.IsValid)
            {
                this.CurrentRow[fieldName] = data;
            }
        }

        protected string GetString(string fieldName)
        {
            if (this.IsValid)
            {
                try
                {
                    object data = this.CurrentRow[fieldName];
                    if (null == data)
                    {
                        return string.Empty;
                    }

                    return data.ToString();
                }
                catch
                {
                    return string.Empty;
                }
            }

            return string.Empty;
        }

        protected virtual void SetString(string fieldName, string value)
        {
            if (this.IsValid)
            {
                if (null == value)
                {
                    value = string.Empty;
                }

                this.CurrentRow[fieldName] = value.TrimWhitespaceAndBOM();
            }
        }
    }

	abstract public class CXmlRecordBaseOld : CXmlRecordBase, ICollectionItemBaseOld
    {
		private DataRow m_row;
		protected DataSet m_ds; 
		private CBase m_parent;
		protected int		m_iRow;
		protected Guid		m_recordId;
		private string m_tableName;
		private bool m_bNewRecord;

		private bool m_bIsValid;

		
		protected CXmlRecordBaseOld()
		{
			m_bIsValid = false;	
		}
		// Pass -1 for rowApp to add new row.
		public CXmlRecordBaseOld( CBase parent, DataSet ds, int iRow, string tableName )
		{
			m_iRow = iRow;
			m_recordId = Guid.Empty;
			m_ds = ds;
			m_parent = parent;
			m_tableName = tableName;

			DataTable table = ds.Tables[m_tableName];
			if( iRow == -1 )
			{
				m_row = table.NewRow();
				m_row["RecordId"] = Guid.NewGuid().ToString();
				m_bNewRecord = true;
			}
			else
				m_row = table.Rows[iRow];

			m_bIsValid = true;
		}

        // DO NOT USE!  CREATED JUST TO ENABLE SOME QUICK UNIT TESTING OF THE SHIM
        protected CXmlRecordBaseOld(DataSet ds, LosConvert converter)
            : base(converter)
        {
            m_iRow = 0;
            m_recordId = Guid.Empty;
            m_ds = ds;
            m_parent = null;

            var table = m_ds.Tables[0];
            m_tableName = table.TableName;
            m_row = table.Rows[0];
            m_bIsValid = true;
        }

		public bool IsNewRecord
		{
			get { return m_bNewRecord; }
		}
			
		public override bool IsValid
		{
			get { return m_bIsValid; }
		}
        protected override string TableName 
        {
            get { return m_tableName; }
        }

        protected override DataRow CurrentRow 
        {
            get { return m_row; }
        }

        protected override CBase Parent 
        {
            get { return m_parent; }
        }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="ds"></param>
		/// <param name="recordId">Pass Guid.Empty to have a new record created</param>
		/// <param name="tableName"></param>
		public CXmlRecordBaseOld( CBase parent, DataSet ds, Guid recordId, string tableName )
		{
			m_iRow = 0;

			m_ds = ds;
			m_parent = parent;
			m_tableName = tableName;

			DataTable table = ds.Tables[m_tableName];

			bool bFoundRecord = false;
			
			if( Guid.Empty != recordId )
			{
				m_recordId = recordId;
				for( int i = 0; i < table.Rows.Count; ++i )
				{
					if( m_recordId.Equals( new Guid( table.Rows[i]["RecordId"].ToString() ) ) )
					{
						m_row = table.Rows[i];
						bFoundRecord = true;
						break;
					}
				}
			}

			if( !bFoundRecord )
			{
				m_row = table.NewRow();

				if( Guid.Empty == m_recordId )
					m_recordId = Guid.NewGuid();
				
				m_row["RecordId"] = m_recordId.ToString();
				m_bNewRecord = true; // This is true because in the background calculation feature, new id created
									 // for new record might be passed back and forth several times
									 // w/o it being saved into the database.
			}
			
			m_bIsValid = true;
		}


        /// <summary>
        /// Wipes everything except for the record id.   
        /// //TODO improve
        /// </summary>
        public void ClearExistingData()
        {
            DataRow row = CurrentRow;


            foreach (DataColumn entry in row.Table.Columns)
            {
                if (entry.ColumnName == RecordIdColumnName || entry.ColumnName == "AgentRoleT")
                {
                    continue;
                }

                row[entry.ColumnName] = "";
            }


        }

        public override void Update()
        {
            if (IsValid)
            {
                if (m_bNewRecord)
                {
                    m_ds.Tables[m_tableName].Rows.Add(m_row);
                }
            }
            else
            {
                Tools.LogError("Programming Error, CXmlRecordBase.Update() is called on an invalid CXmlRecordBase object.");
            }
        }

        /// <summary>
        /// The name of the column, <c>"RecordId"</c>, in the <see cref="System.Data.DataTable"/> containing the <see cref="RecordId"/>.
        /// </summary>
        public const string RecordIdColumnName = "RecordId";

        public Guid RecordId
        {
            get
            {
                if (!this.IsValid)
                {
                    return Guid.Empty;
                }

                string val = this.m_row[RecordIdColumnName].ToString();
                if (0 == val.Length)
                {
                    throw new CBaseException(ErrorMessages.Generic, "Encountered a record in " + this.m_tableName + " without id.");
                }

                return new Guid(val);
            }
        }
    }
}