﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Common;

namespace DataAccess
{
    public class VersionMismatchException : CBaseException
    {
        public VersionMismatchException()
            : base(ErrorMessages.AnotherUserMadeChangeToLoan, ErrorMessages.AnotherUserMadeChangeToLoan)
        {
        }
    }
}
