using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using LendersOffice.Common;
using LendersOffice.Security;
using LqbGrammar.DataTypes;

namespace DataAccess
{
    /// <summary>
    /// Summary description for AutoExpiredTextCache.
    /// </summary>

    // 05/09/06 mf - OPM 4668.  We can now use auto-expiring
    // cache.  Currently the implementation uses DB for smaller
    // cache and FileDB for larger.
    //
    // 05/11/06 mf - After discussion with Thinh, we decided to
    // use a guid with a single character storage hint to avoid
    // some of the database hits just to check the storage location of
    // a cache item.  For compatibility with storage that will
    // require a GUID, we allow it, but average cache retrievals
    // from a guid will be slower because of the lack of hint.
    //
    // 05/18/06 mf - UpdateCache was implemented.
    //
    // 11/08/06 mf - Moved cache store to new transient DB.
    // 06/16/10 dd - Add ability to cache byte[].

    public class AutoExpiredTextCache
    {
        public const int KEY_LENGTH_MAX = 100;
        const int DB_CACHE_MAX = 5500;
        const int MAX_EXPIRATION_SECONDS = 691299; // 8 days + 99 seconds.
        const int MIN_EXPIRATION_SECONDS = 60; // 1 Minute

        // Since we now allow for custom string keys we need a more unique
        // hint.  Hopefully callers would be unlikely to create keys that 
        // began in such a way.  These will be more readable.
        const string FILEDB_LOCATION_PREFIX = "_fdb_";
        const string DB_LOCATION_PREFIX = "_db_";

        const string LOCATION_FILEDB = "FileDB";
        const string LOCATION_DB = "DB";

        public static string AddToCache(byte[] content, TimeSpan life)
        {
            string cacheId = FILEDB_LOCATION_PREFIX + Guid.NewGuid().ToString();
            AddToCache(content, life, cacheId);
            return cacheId;
        }


        private static string GetPrincipalDependentTempKey(AbstractUserPrincipal principal, string randomKey)
        {
            return string.Concat(principal.UserId, "_", randomKey);
        }

        private static void CreateTempCacheEntryInDb(string cacheId, double totalSeconds, string location, string content)
        {
            SqlParameter[] parameters = { new SqlParameter( "@CacheId" , cacheId )
                                                , new SqlParameter( "@SecondsToLive" , totalSeconds )
                                                , new SqlParameter( "@CacheLocationType" , location )
                                                , new SqlParameter( "@CacheContent" , content )
                                            };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "CreateTempCache", 2, parameters);
        }

        private static void CheckLifeTime(string cacheId, double totalSeconds)
        {
            if (cacheId.Length > KEY_LENGTH_MAX)
            {
                throw new CBaseException(ErrorMessages.Generic,
                    String.Format("The key {0} has a greater length than the current maximum of {1}.", cacheId, KEY_LENGTH_MAX));
            }

            if (totalSeconds > MAX_EXPIRATION_SECONDS || totalSeconds < MIN_EXPIRATION_SECONDS)
            {
                throw new CBaseException(ErrorMessages.Generic,
                    String.Format("Cache lifespan {0} outside accepted range of {1} to {2} seconds.", totalSeconds.ToString(), MIN_EXPIRATION_SECONDS, MAX_EXPIRATION_SECONDS));
            }
        }

        public static void AddFileToCache(string path, TimeSpan life, string cacheId)
        {
            CheckLifeTime(cacheId, life.TotalSeconds);

            FileDBTools.WriteFile(E_FileDB.Temp, cacheId, path);
     
            try
            {
                CreateTempCacheEntryInDb(cacheId, life.TotalSeconds, LOCATION_FILEDB, string.Empty);
            }
            catch (Exception)
            {
                FileDbDelete(cacheId);
                throw;
            }
        }

        /// <summary>
        /// Store the arbitrary file to cache.
        /// </summary>
        /// <param name="content"></param>
        /// <param name="life"></param>
        /// <returns></returns>
        public static void AddToCache(byte[] content, TimeSpan life, string cacheId)
        {
            CheckLifeTime(cacheId, life.TotalSeconds);
            FileDbPut(cacheId, content);

            try
            {
                CreateTempCacheEntryInDb(cacheId, life.TotalSeconds, LOCATION_FILEDB, string.Empty);
            }
            catch (Exception)
            {
                FileDbDelete(cacheId);
                throw;
            }
        }

        /// <summary>
        /// Get content from cache. Return null if key is not found.
        /// </summary>
        /// <param name="cacheId">Key to locate cache content.</param>
        /// <returns>Content in cache as byte array.</returns>
        public static byte[] GetBytesFromCache(string cacheId)
        {
            try
            {
                return FileDBTools.ReadData(E_FileDB.Temp, cacheId);
            }
            catch (FileNotFoundException)
            {
                return null;
            }
        }

        /// <summary>
        /// Get content from cache and remove it. Return null if key is not found.
        /// </summary>
        /// <param name="cacheId">Key to locate cache content.</param>
        /// <returns>Content in cache as byte array.</returns>
        public static byte[] GetBytesFromCacheThenRemove(string cacheId)
        {
            var content = GetBytesFromCache(cacheId);
            RemoveFromCacheImmediately(cacheId);
            return content;
        }

        /// <summary>
        /// Insert a string into temporary cache.  Returns key.
        /// </summary>
        public static string AddToCache(string content, TimeSpan life)
        {
            // Callers to this method do not have a pre-determined key.
            // This is the preferred way to add to cache, so we can embed our
            // storage hint into the key.
            
            string newId;

            newId = (content.Length < DB_CACHE_MAX) ?  DB_LOCATION_PREFIX + Guid.NewGuid().ToString() : FILEDB_LOCATION_PREFIX + Guid.NewGuid().ToString();
            CheckLifeTime(newId, life.TotalSeconds);
            AddToCache(content, life, newId);
            return newId;
        }

        /// <summary>
        /// Add to cache with key generated by combining given cacheId and principal user ID.
        /// </summary>
        public static void AddToCacheByUser(AbstractUserPrincipal principal, string content, TimeSpan life, Guid cacheId)
        {
            string key = $"{principal.UserId}_{cacheId}";
            CheckLifeTime(key, life.TotalSeconds);
            AddToCache(content, life, key);
        }

        /// <summary>
        /// Add to cache with key generated by combining given cacheId and principal user ID.
        /// </summary>
        /// <param name="principal">The principal whose user ID to use.</param>
        /// <param name="content">The content to save.</param>
        /// <param name="life">The lifespan of the user string.</param>
        /// <param name="key">The string key to use along with the principal ID. Take care not to make the key too long, as it will be concatenated with the user ID guid.</param>
        public static void AddUserString(AbstractUserPrincipal principal, string content, TimeSpan life, string key)
        {
            string cacheKey = GetPrincipalDependentTempKey(principal, key);
            CheckLifeTime(cacheKey, life.TotalSeconds);
            AddToCache(content, life, cacheKey);
        }

        /// <summary>
        /// Add to cache with key.
        /// </summary>
        public static void AddToCache(string content, TimeSpan life, Guid cacheId)
        {
            CheckLifeTime(cacheId.ToString(), life.TotalSeconds);
            AddToCache(content, life, cacheId.ToString());
        }

        /// <summary>
        /// Add to cache with a string key.  Max length 100.
        /// </summary>
        public static void AddToCache(string content, TimeSpan life, string cacheId)
        {
         
            CheckLifeTime(cacheId.ToString(), life.TotalSeconds);

            if (content.Length < DB_CACHE_MAX)
            {
                // DB Cache - The content is stored with the expiration in the DB

                CreateTempCacheEntryInDb(cacheId, life.TotalSeconds, LOCATION_DB, content);
            }
            else
            {
                // FileDB Cache - We store in FileDB, and if successful, add an expiration
                // rule to DB

                FileDbPut(cacheId, content);

                try
                {
                    CreateTempCacheEntryInDb(cacheId, life.TotalSeconds, LOCATION_FILEDB, string.Empty);
                }
                catch (Exception)
                {
                    FileDbDelete(cacheId);
                }
            }
        }

        /// <summary>
        /// Caches user object by serializing to JSON using JavaScriptSerializer. Must use 
        /// GetUserObject to pull value out.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="principal"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string InsertUserObject<T>(AbstractUserPrincipal principal, T obj, TimeSpan timeSpan) where T : class
        {
            return AutoExpiredTextCache.InsertUserString(principal, ObsoleteSerializationHelper.JavascriptJsonSerialize(obj), timeSpan);

        }

        /// <summary>
        /// Inserts a string into the cache for a particular user, returning a key used for retrieval.
        /// </summary>
        /// <param name="principal">The principal to insert this key for.</param>
        /// <param name="content">The string content to insert.</param>
        /// <param name="timeSpan">The timespan to cache <paramref name="content"/>.</param>
        /// <returns>A partial key, from which the object can be retrieved.</returns>
        /// <remarks>Retrieval must go through one of the "Get User" methods.</remarks>
        public static string InsertUserString(AbstractUserPrincipal principal, string content, TimeSpan timeSpan)
        {
            string key = Tools.ShortenGuid(Guid.NewGuid());
            return InsertUserString(principal, key, content, timeSpan);
        }


        /// <summary>
        /// Inserts a string into the cache for a particular user, returning a key used for retrieval.
        /// </summary>
        /// <param name="principal">The principal to insert this key for.</param>
        /// <param name="key">The user-specific key to store the value under.</param>
        /// <param name="content">The string content to insert.</param>
        /// <param name="timeSpan">The timespan to cache <paramref name="content"/>.</param>
        /// <returns>A partial key, from which the object can be retrieved.</returns>
        /// <remarks>Retrieval must go through one of the "Get User" methods.</remarks>
        public static string InsertUserString(AbstractUserPrincipal principal, string key, string content, TimeSpan timeSpan)
        {
            AutoExpiredTextCache.AddToCache(content, timeSpan, GetPrincipalDependentTempKey(principal, key));
            return key;
        }

        /// <summary>
        /// Removes an entry from the cache for a particular user.
        /// </summary>
        /// <param name="principal">The principal to delete this key for.</param>
        /// <param name="key">The user-specific key under which the value is currently stored.</param>
        public static void RemoveUserEntry(AbstractUserPrincipal principal, string key)
        {
            RemoveFromCacheImmediately(GetPrincipalDependentTempKey(principal, key));
        }

        /// <summary>
        /// Gets a string from the cache for a particular user.
        /// </summary>
        /// <param name="principal">The user to retrieve this key for.</param>
        /// <param name="key">The key returned when inserting the value through the corresponding "Insert User" method.</param>
        /// <returns>The string stored at the location.</returns>
        public static string GetUserString(AbstractUserPrincipal principal, string key)
        {
            return AutoExpiredTextCache.GetFromCache(GetPrincipalDependentTempKey(principal, key));
        }

        public static T GetUserObject<T>(AbstractUserPrincipal principal, string key) where T : class
        {
            string item = AutoExpiredTextCache.GetUserString(principal, key);
            if (string.IsNullOrEmpty(item))
            {
                return default(T);
            }

            return ObsoleteSerializationHelper.JavascriptJsonDeserializer<T>(item);
        }

        public static string GetFromCache(Guid cacheId)
        {
            return GetFromCache(cacheId.ToString());
        }

        public static string GetFromCacheByUser(AbstractUserPrincipal principal, string cacheId)
        {
            string key = $"{principal.UserId}_{cacheId}";
            return GetFromCache(key);
        }

        /// <summary>
        /// Load string from cache.  Returns null if unsuccessful.
        /// </summary>
        public static string GetFromCache(string cacheId)
        {
            if (string.IsNullOrEmpty(cacheId))
            {
                return null;
            }

            if(cacheId.StartsWith(FILEDB_LOCATION_PREFIX))
            {
                // Try FileDB.  If it isn't there, it must be expired,
                // because we do not move from FileDB to DB.

                return FileDbGetText(cacheId);
            }

            // DB location hint or there is no hint.
            // Try DB first, and while we are hitting DB, we use the cache 
            // table to determine if it exists in FileDB, to avoid extra FDB hit.

            bool inFileDB = false;
            string cacheContent;
                
            if ((cacheContent = GetFromDB(cacheId, ref inFileDB)) != null)
                return cacheContent;
                
            return (inFileDB) ? FileDbGetText(cacheId) : null;
        }

        public static string GetFromCacheThenRemove(string cacheId)
        {
            var content = GetFromCache(cacheId);
            RemoveFromCacheImmediately(cacheId);
            return content;
        }

        public static bool TryGetStringOrFileHandlerFromCacheByUser(AbstractUserPrincipal principal, string cacheId, out string stringFromDB, out LocalFilePath filePath)
        {
            string key = $"{principal.UserId}_{cacheId}";
            return TryGetStringOrFileHandlerFromCache(key, out stringFromDB, out filePath);
        }

        public static bool TryGetStringOrFileHandlerFromCache(string cacheId, out string stringFromDB, out LocalFilePath filePath)
        {
            stringFromDB = null;
            filePath = LocalFilePath.Invalid;
            try
            {
                if (cacheId.StartsWith(FILEDB_LOCATION_PREFIX))
                {
                    // Try FileDB.  If it isn't there, it must be expired,
                    // because we do not move from FileDB to DB.
                    try
                    {
                        string tempPath = FileDBTools.CreateCopy(E_FileDB.Temp, cacheId);

                        LocalFilePath? o = LocalFilePath.Create(tempPath);
                        if (o != null)
                        {
                            filePath = o.Value;
                        }
                    }
                    catch (FileNotFoundException)
                    {
                        filePath = LocalFilePath.Invalid;
                    }
                }
                else
                {
                    // DB location hint or there is no hint.
                    // Try DB first, and while we are hitting DB, we use the cache 
                    // table to determine if it exists in FileDB, to avoid extra FDB hit.

                    bool inFileDB = false;

                    if ((stringFromDB = GetFromDB(cacheId, ref inFileDB)) != null)
                    {
                        return true;
                    }

                    if (inFileDB)
                    {
                        try
                        {
                            string tempPath = FileDBTools.CreateCopy(E_FileDB.Temp, cacheId);

                            LocalFilePath? o = LocalFilePath.Create(tempPath);
                            if (o != null)
                            {
                                filePath = o.Value;
                            }
                        }
                        catch (FileNotFoundException)
                        {
                            filePath = LocalFilePath.Invalid;
                        }
                    }
                }

                if (filePath != LocalFilePath.Invalid)
                {
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                Tools.LogErrorWithCriticalTracking(e);
                return false;
            }
        }


        // Attempt to pull cache from DB
        private static string GetFromDB (string cacheId, ref bool inFileDB)
        {
            string cacheContent = null;
            string cacheLocationType = null;

            using(DbDataReader reader = StoredProcedureHelper.ExecuteReader( DataSrc.LOTransient, "GetTempCache", new SqlParameter("@CacheId", cacheId)))
            {
                if (reader.Read())
                {
                    cacheContent = (string) reader["CacheContent"];
                    cacheLocationType = (string) reader["CacheLocationType"];
                }
                else  
                {
                    //No rows read
                    return null;
                }
            }
            
            if (cacheLocationType == "FileDB")
            {
                inFileDB = true;
                return null;
            }
            else
            {
                inFileDB = false;
            }

            return cacheContent;
        }


        /// <summary>
        /// Updates the cache expiration date for the given item.
        /// </summary>
        /// <param name="cacheId">The id of the cache item to update.</param>
        /// <param name="timeToExpand">
        /// A timespan indicating how long to extend the expiration from the
        /// current time. Note, this is not based off of the previous expiration
        /// date. This value must be less than or equal to 4 hours.
        /// </param>
        /// <remarks>
        /// The new expiration date is set by adding the number of minutes
        /// expressed by timeToExpand to the current date and time. Fractional
        /// minutes are rounded.
        /// </remarks>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Thrown when timeToExpand is greater than 4 hours.
        /// </exception>
        public static void RefreshCacheDuration(string cacheId, TimeSpan timeToExpand)
        {
            int totalSeconds = Convert.ToInt32(timeToExpand.TotalSeconds);

            if (totalSeconds > MAX_EXPIRATION_SECONDS)
            {
                throw new ArgumentOutOfRangeException("timeToExpand");
            }

            int totalMinutes = Convert.ToInt32(timeToExpand.TotalMinutes);

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@CacheId", cacheId),
                new SqlParameter("Minutes", totalMinutes)
            };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "RefreshCacheItem", 1, parameters);
        }

        /// <summary>
        /// Update existing cache item.
        /// </summary>
        public static bool UpdateCacheBytes(byte[] content, string cacheId)
        {
            // This update is tricky.  We have to handle if the updated value
            // is on the other side of the size threshold as the original.
            // 
            // Four Situations: 
            // A. small(DB-size) replaces small(in DB) - leave in DB.
            // B. big(FileDB-size) replaces small(in DB) - move from DB to FileDB -- to gain enough space.
            // C. small(DB-size) replaces big(in FileDB) - leave in FileDB
            // D. big(FileDB-size) replaces big(in FileDB) - leave in FileDB
            //
            // The storage hint doesn't help us when we update because we have
            // to hit the DB in both cases.

            bool success = false;
            string updateLocation = "FileDB";

            SqlParameter updateResultParam = new SqlParameter("@UpdateResult", SqlDbType.TinyInt);
            updateResultParam.Direction = ParameterDirection.Output;

            SqlParameter[] parameters = { new SqlParameter("@CacheId", cacheId)
                                            , new SqlParameter("@CacheContent", string.Empty)
                                            , new SqlParameter("@UpdateLocation", updateLocation)
                                            , updateResultParam
                                        };

            using (CStoredProcedureExec exec = new CStoredProcedureExec(DataSrc.LOTransient))
            {
                try
                {
                    exec.BeginTransactionForWriteSerializable();

                    exec.ExecuteNonQuery("UpdateTempCache", 2, parameters);

                    UpdateResult result = (UpdateResult)int.Parse(updateResultParam.Value.ToString());


                    switch (result)
                    {
                        case UpdateResult.NoRecordExists:
                            // This cache is probably expired and deleted.

                            success = false;
                            break;

                        case UpdateResult.DbCacheItemUpdated:
                            Tools.LogError("Update cache bytes option UpdateResult.DbCacheItemUpdated should not happen");

                            success = false;
                            break;

                        case UpdateResult.ConvertedDbToFileDb:
                            Tools.LogError("Update cache bytes option UpdateResult.ConvertedDbToFileDb should not happen");
                            success = false;
                            break;

                        case UpdateResult.FileDbRecordExists:
                            // The SP reports that the file is currently in FileDB already, so the DB row was untouched.
                            // We just need to update the FileDB entry.

                            if (ExistsInFileDb(cacheId))
                            {
                                FileDbPut(cacheId, content);
                                success = true;
                            }
                            else
                            {
                                // We cannot update an item that does not exist
                                success = false;
                            }
                            break;
                        default:
                            Tools.LogError(String.Format("UpdateTempCache returned {0} on the id {1}.  This is not a known value.", result.ToString(), cacheId));
                            success = false;
                            break;
                    }

                    exec.CommitTransaction();
                }
                catch (CBaseException exc)
                {
                    exec.RollbackTransaction();
                    Tools.LogError(exc);
                    success = false;
                }
                catch (Exception exc)
                {
                    exec.RollbackTransaction();
                    throw exc;
                }
            }

            return success;

        }

        /// <summary>
        /// Update existing cache item.
        /// </summary>
        public static bool UpdateCache(string content, string cacheId)
        {
            // This update is tricky.  We have to handle if the updated value
            // is on the other side of the size threshold as the original.
            // 
            // Four Situations: 
            // A. small(DB-size) replaces small(in DB) - leave in DB.
            // B. big(FileDB-size) replaces small(in DB) - move from DB to FileDB -- to gain enough space.
            // C. small(DB-size) replaces big(in FileDB) - leave in FileDB
            // D. big(FileDB-size) replaces big(in FileDB) - leave in FileDB
            //
            // The storage hint doesn't help us when we update because we have
            // to hit the DB in both cases.

            bool success = false;

            string updateContent;
            string updateLocation;

            if (content.Length < DB_CACHE_MAX)
            {
                updateContent = content;
                updateLocation = "DB";
            }
            else
            {
                updateContent = string.Empty;
                updateLocation = "FileDB";
            }


            SqlParameter updateResultParam = new SqlParameter("@UpdateResult", SqlDbType.TinyInt);
            updateResultParam.Direction = ParameterDirection.Output;

            SqlParameter[] parameters = { new SqlParameter("@CacheId", cacheId)
                                            , new SqlParameter("@CacheContent", updateContent)
                                            , new SqlParameter("@UpdateLocation", updateLocation)
                                            , updateResultParam
                                        };
            
            using (CStoredProcedureExec exec = new CStoredProcedureExec( DataSrc.LOTransient ))
            {
                try
                {
                    exec.BeginTransactionForWriteSerializable();

                    exec.ExecuteNonQuery("UpdateTempCache", 2, parameters);

                    UpdateResult result = (UpdateResult) int.Parse( updateResultParam.Value.ToString());

                        
                    switch(result)
                    {
                        case UpdateResult.NoRecordExists:
                            // This cache is probably expired and deleted.

                            success = false;
                            break;
                        
                        case UpdateResult.DbCacheItemUpdated:

                            success = true;
                            break;

                        case UpdateResult.ConvertedDbToFileDb:
                            // Because the contents were too large to fit in DB cache, the SP changed the record of a DB 
                            // cache item to point to FileDB.  We need to create the new FileDB entry with this id.

                            FileDbPut(cacheId, content);
                            
                            // Must throw and rollback DB changes if we failed the FileDB Put operation
                            if (!ExistsInFileDb(cacheId))
                                throw new CBaseException(ErrorMessages.Generic, "Update Failure in FileDB.");

                            success = true;
                            break;

                        case UpdateResult.FileDbRecordExists:
                            // The SP reports that the file is currently in FileDB already, so the DB row was untouched.
                            // We just need to update the FileDB entry.

                            if (ExistsInFileDb(cacheId))
                            {
                                FileDbPut(cacheId, content);
                                success = true;
                            }
                            else
                            {
                                // We cannot update an item that does not exist
                                success = false;
                            }
                            break;
                        default:
                            Tools.LogError(String.Format("UpdateTempCache returned {0} on the id {1}.  This is not a known value.", result.ToString(), cacheId));
                            success = false;
                            break;
                    }

                    exec.CommitTransaction();
                }
                catch(CBaseException exc)
                {
                    exec.RollbackTransaction();
                    Tools.LogError(exc);
                    success = false;
                }
                catch(Exception exc)
                {
                    exec.RollbackTransaction();
                    throw exc;
                }
            }

            return success;
        }

        private enum UpdateResult
        {
            NoRecordExists = 1,
            DbCacheItemUpdated = 2,
            ConvertedDbToFileDb = 3,
            FileDbRecordExists = 4
        }

        /// <summary>
        /// Resets the expiration marking the given Cache item as expired for the next round of clean up. 
        /// 1-31-08 av opm 19618
        /// </summary>
        public static void ExpireCache(string CacheId) 
        {

            SqlParameter[] parameters = { new SqlParameter("@CacheId", CacheId) };
            
            using (CStoredProcedureExec exec = new CStoredProcedureExec( DataSrc.LOTransient ))
            {
                try
                {
                    exec.BeginTransactionForWriteSerializable();
                    exec.ExecuteNonQuery("ExpireCacheItem", parameters);
                }
                catch( Exception e )
                {
                    exec.RollbackTransaction();
                    throw e;
                }

                exec.CommitTransaction(); 
            }

        }

        public static void RemoveFromCacheImmediately(string cacheId)
        {
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "DeleteCacheItem", 1, new SqlParameter("@CacheId", cacheId));
            FileDBTools.Delete(E_FileDB.Temp, cacheId);
        }

        /// <summary>
        /// Enforces expiration of cache files by deleting all expired cache.
        /// Usually called periodically by expiration enforcement executable.
        /// </summary>
        public static void EnforceExpiration(out int numberOfDbItems, out int numberOfFileDbItems)
        {
            numberOfDbItems = numberOfFileDbItems = 0;
            List<string> items = new List<string>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOTransient, "CleanupTempCache"))
            {
                reader.Read();
                numberOfDbItems = (int)reader[0];

                if (reader.NextResult())
                {
                    while (reader.Read())
                    {
                        items.Add(reader["CacheId"].ToString());
                    }
                }
            }

            foreach (string id in items)
            {
                FileDbDelete(id);

                if (FileDBTools.DoesFileExist(E_FileDB.Temp, id))
                {
                    Tools.LogError("Failed to delete id " + id + " from FileDB.");
                    continue;
                }

                try
                {
                    StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "RemoveTempCacheExpirations", 3, new SqlParameter("@CacheId", id));
                    numberOfFileDbItems++;
                }
                catch (SqlException e)
                {
                    Tools.LogError("Failed to delete cache entry", e);
                    throw;
                }
            }
        }

        #region FileDB Access Methods

        private static void FileDbPut(string key, byte[] content)
        {
            FileDBTools.WriteData(E_FileDB.Temp, key, content);
        }

        // Put the content into FileDB with the given key
        private static void FileDbPut(string key, string content)
        {
            byte[] data = System.Text.Encoding.UTF8.GetBytes(content);

            FileDbPut(key, data);
        }

        // Delete the content from FileDB
        private static void FileDbDelete(string key)
        {
            FileDBTools.Delete(E_FileDB.Temp, key);
        }

        // Load the content from FileDB
        // Returns null if unsuccessful
        private static string FileDbGetText(string key)
        {
            string cacheContent = null;

            try
            {
                FileDBTools.UseFile(E_FileDB.Temp, key, (fileInfo) =>
                {
                    using (StreamReader stream = new StreamReader(fileInfo.FullName))
                    {
                        cacheContent = stream.ReadToEnd();
                    }
                });
            }
            catch (FileNotFoundException)
            {
                cacheContent = null;
            }

            return cacheContent;
        }

        /// <summary>
        /// Delete the file after you use this.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetFileFromCache(string key)
        {
            return FileDBTools.CreateCopy(E_FileDB.Temp, key);
        }

        // Check if content exists in FileDB
        private static bool ExistsInFileDb(string key)
        {
            return FileDBTools.DoesFileExist(E_FileDB.Temp, key);
        }

        #endregion
    }
}