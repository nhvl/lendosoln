﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// This class allows callers to LoanValueEvaluator to define some non-loan
    /// data to be used in evaluation of special parameters.
    /// </summary>
    public class NonLoanValueEvaluator
    {
        /// <summary>
        /// A lazy-loaded dictionary that contains the string parameter values.
        /// </summary>
        private Lazy<Dictionary<string, IList<string>>> stringValues =
            new Lazy<Dictionary<string, IList<string>>>(() => new Dictionary<string, IList<string>>(StringComparer.InvariantCultureIgnoreCase));

        /// <summary>
        /// A lazy-loaded dictionary that contains the DateTime parameter values.
        /// </summary>
        private Lazy<Dictionary<string, IList<DateTime>>> dateTimeValues =
            new Lazy<Dictionary<string, IList<DateTime>>>(() => new Dictionary<string, IList<DateTime>>(StringComparer.InvariantCultureIgnoreCase));

        /// <summary>
        /// A lazy-loaded dictionary that contains the float parameter values.
        /// </summary>
        private Lazy<Dictionary<string, IList<float>>> floatValues =
            new Lazy<Dictionary<string, IList<float>>>(() => new Dictionary<string, IList<float>>(StringComparer.InvariantCultureIgnoreCase));

        /// <summary>
        /// A lazy-loaded dictionary that contains the integer parameter values.
        /// </summary>
        private Lazy<Dictionary<string, IList<int>>> intValues =
            new Lazy<Dictionary<string, IList<int>>>(() => new Dictionary<string, IList<int>>(StringComparer.InvariantCultureIgnoreCase));

        /// <summary>
        /// A lazy-loaded dictionary that contains the boolean parameter values.
        /// </summary>
        private Lazy<Dictionary<string, IList<bool>>> boolValues =
            new Lazy<Dictionary<string, IList<bool>>>(() => new Dictionary<string, IList<bool>>(StringComparer.InvariantCultureIgnoreCase));

        /// <summary>
        /// A dictionary that contains the string parameter values.
        /// </summary>
        private Dictionary<string, IList<string>> StringValues => this.stringValues.Value;

        /// <summary>
        /// A dictionary that contains the DateTime parameter values.
        /// </summary>
        private Dictionary<string, IList<DateTime>> DateTimeValues => this.dateTimeValues.Value;

        /// <summary>
        /// A dictionary that contains the float parameter values.
        /// </summary>
        private Dictionary<string, IList<float>> FloatValues => this.floatValues.Value;

        /// <summary>
        /// A dictionary that contains the integer parameter values.
        /// </summary>
        private Dictionary<string, IList<int>> IntValues => this.intValues.Value;

        /// <summary>
        /// A dictionary that contains the boolean parameter values.
        /// </summary>
        private Dictionary<string, IList<bool>> BoolValues => this.boolValues.Value;

        /// <summary>
        /// Add a single parameter value.
        /// </summary>
        /// <param name="parameterId">The parameter Id.</param>
        /// <param name="parameterValue">The value of of the parameter.</param>
        public void Add(string parameterId, string parameterValue)
        {
            this.StringValues.Add(parameterId, new List<string>(new string[] { parameterValue }));
        }

        /// <summary>
        /// Add parameter value list.
        /// </summary>
        /// <param name="parameterId">The parameter Id.</param>
        /// <param name="parameterValues">The value of of the parameter.</param>
        public void Add(string parameterId, IEnumerable<string> parameterValues)
        {
            this.StringValues.Add(parameterId, new List<string>(parameterValues));
        }

        /// <summary>
        /// Add a single parameter value.
        /// </summary>
        /// <param name="parameterId">The parameter Id.</param>
        /// <param name="parameterValue">The value of of the parameter.</param>
        public void Add(string parameterId, DateTime parameterValue)
        {
            this.DateTimeValues.Add(parameterId, new List<DateTime>(new DateTime[] { parameterValue }));
        }

        /// <summary>
        /// Add parameter value list.
        /// </summary>
        /// <param name="parameterId">The parameter Id.</param>
        /// <param name="parameterValues">The value of of the parameter.</param>
        public void Add(string parameterId, IEnumerable<DateTime> parameterValues)
        {
            this.DateTimeValues.Add(parameterId, new List<DateTime>(parameterValues));
        }

        /// <summary>
        /// Add a single parameter value.
        /// </summary>
        /// <param name="parameterId">The parameter Id.</param>
        /// <param name="parameterValue">The value of of the parameter.</param>
        public void Add(string parameterId, float parameterValue)
        {
            this.FloatValues.Add(parameterId, new List<float>(new float[] { parameterValue }));
        }

        /// <summary>
        /// Add parameter value list.
        /// </summary>
        /// <param name="parameterId">The parameter Id.</param>
        /// <param name="parameterValues">The value of of the parameter.</param>
        public void Add(string parameterId, IEnumerable<float> parameterValues)
        {
            this.FloatValues.Add(parameterId, new List<float>(parameterValues));
        }

        /// <summary>
        /// Add a single parameter value.
        /// </summary>
        /// <param name="parameterId">The parameter Id.</param>
        /// <param name="parameterValue">The value of of the parameter.</param>
        public void Add(string parameterId, int parameterValue)
        {
            this.IntValues.Add(parameterId, new List<int>(new int[] { parameterValue }));
        }

        /// <summary>
        /// Add parameter value list.
        /// </summary>
        /// <param name="parameterId">The parameter Id.</param>
        /// <param name="parameterValues">The value of of the parameter.</param>
        public void Add(string parameterId, IEnumerable<int> parameterValues)
        {
            this.IntValues.Add(parameterId, new List<int>(parameterValues));
        }

        /// <summary>
        /// Add a single parameter value.
        /// </summary>
        /// <param name="parameterId">The parameter Id.</param>
        /// <param name="parameterValue">The value of of the parameter.</param>
        public void Add(string parameterId, IList<bool> parameterValue)
        {
            this.BoolValues.Add(parameterId, parameterValue);
        }

        /// <summary>
        /// Checks if a non-loan parameter value is defined for the parameterId.
        /// </summary>
        /// <param name="parameterId">The parameter Id.</param>
        /// <returns>True if it is defined.</returns>
        public bool IsSpecialStringFieldName(string parameterId)
        {
            return this.StringValues.ContainsKey(parameterId);
        }

        /// <summary>
        /// Checks if a non-loan parameter value is defined for the parameterId.
        /// </summary>
        /// <param name="parameterId">The parameter Id.</param>
        /// <returns>True if it is defined.</returns>
        public bool IsSpecialDateTimeFieldName(string parameterId)
        {
            return this.DateTimeValues.ContainsKey(parameterId);
        }

        /// <summary>
        /// Checks if a non-loan parameter value is defined for the parameterId.
        /// </summary>
        /// <param name="parameterId">The parameter Id.</param>
        /// <returns>True if it is defined.</returns>
        public bool IsSpecialFloatFieldName(string parameterId)
        {
            return this.FloatValues.ContainsKey(parameterId);
        }

        /// <summary>
        /// Checks if a non-loan parameter value is defined for the parameterId.
        /// </summary>
        /// <param name="parameterId">The parameter Id.</param>
        /// <returns>True if it is defined.</returns>
        public bool IsSpecialIntFieldName(string parameterId)
        {
            return this.IntValues.ContainsKey(parameterId);
        }
        
        /// <summary>
        /// Checks if a non-loan parameter value is defined for the parameterId.
        /// </summary>
        /// <param name="parameterId">The parameter Id.</param>
        /// <returns>True if it is defined.</returns>
        public bool IsSpecialBoolFieldName(string parameterId)
        {
            return this.BoolValues.ContainsKey(parameterId);
        }
        
        /// <summary>
        /// Get values corresponding to parameter id.
        /// </summary>
        /// <param name="parameterId">The parameter id of the value.</param>
        /// <returns>The value corresponding the the parameter id.</returns>
        public IList<string> GetStringValues(string parameterId)
        {
            return this.StringValues[parameterId];
        }

        /// <summary>
        /// Get values corresponding to parameter id.
        /// </summary>
        /// <param name="parameterId">The parameter id of the value.</param>
        /// <returns>The value corresponding the the parameter id.</returns>
        public IList<DateTime> GetDateTimeValues(string parameterId)
        {
            return this.DateTimeValues[parameterId];
        }

        /// <summary>
        /// Get values corresponding to parameter id.
        /// </summary>
        /// <param name="parameterId">The parameter id of the value.</param>
        /// <returns>The value corresponding the the parameter id.</returns>
        public IList<int> GetIntValues(string parameterId)
        {
            return this.IntValues[parameterId];
        }

        /// <summary>
        /// Get values corresponding to parameter id.
        /// </summary>
        /// <param name="parameterId">The parameter id of the value.</param>
        /// <returns>The value corresponding the the parameter id.</returns>
        public IList<float> GetFloatValues(string parameterId)
        {
            return this.FloatValues[parameterId];
        }

        /// <summary>
        /// Get value corresponding to parameter id.
        /// </summary>
        /// <param name="parameterId">The parameter id of the value.</param>
        /// <returns>The value corresponding the the parameter id.</returns>
        public IList<bool> GetBoolValues(string parameterId)
        {
            return this.BoolValues[parameterId];
        }
    }
}
