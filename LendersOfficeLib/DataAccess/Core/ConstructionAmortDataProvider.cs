﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Core.Construction;

    /// <summary>
    /// A class that remaps non-construction loan terms to their construction
    /// equivalent, directly to CPageBase version, or to a default response if
    /// neither applies for construction loans (all as applicable per field).
    /// </summary>
    /// <remarks>
    /// This class was initially auto-generated to implement the interface via
    /// the pageBase field. Fields that need to be remapped were then modified
    /// manually as needed. A result of this is that auto-generated code casts
    /// the pageBase member into the <see cref="IAmortizationDataProvider"/>
    /// interface when retrieving a field directly from the pageBase member
    /// without any additional mapping. I've left these in to help quickly
    /// visually distinguish fields that are being remapped from ones that are
    /// being passed through as-is.
    /// </remarks>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Compatibility with existing violations.")]
    public class ConstructionAmortDataProvider : IAmortizationDataProvider
    {
        /// <summary>
        /// The <see cref="CPageBase"/> instance being wrapped and remapped.
        /// </summary>
        private CPageBase pageBase;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConstructionAmortDataProvider"/> class.
        /// </summary>
        /// <param name="pageBase">The <see cref="CPageBase"/> instance to wrap/remap.</param>
        public ConstructionAmortDataProvider(CPageBase pageBase)
        {
            this.pageBase = pageBase;
        }

        /// <inheritdoc />
        public LosConvert m_convertLos
        {
            get
            {
                return ((IAmortizationDataProvider)this.pageBase).m_convertLos;
            }
        }

        /// <inheritdoc />
        public int sBuydwnMon1
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public int sBuydwnMon2
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public int sBuydwnMon3
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public int sBuydwnMon4
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public int sBuydwnMon5
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public decimal sBuydwnR1
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public decimal sBuydwnR2
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public decimal sBuydwnR3
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public decimal sBuydwnR4
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public decimal sBuydwnR5
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public ConstructionIntCalcType sConstructionIntCalcT
        {
            get
            {
                return ((IAmortizationDataProvider)this.pageBase).sConstructionIntCalcT;
            }
        }

        /// <inheritdoc />
        public int sConstructionInitialOddDays
        {
            get
            {
                return ((IAmortizationDataProvider)this.pageBase).sConstructionInitialOddDays;
            }
        }

        /// <inheritdoc />
        public ConstructionPhaseIntAccrual sConstructionPhaseIntAccrualT
        {
            get
            {
                return ((IAmortizationDataProvider)this.pageBase).sConstructionPhaseIntAccrualT;
            }
        }

        /// <inheritdoc />
        public int sDue
        {
            get { return this.pageBase.sConstructionPeriodMon; }
        }

        /// <inheritdoc />
        public decimal sFfUfmipFinanced
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public bool sFfUfMipIsBeingFinanced
        {
            get { return false; }
        }

        /// <inheritdoc />
        public decimal sFfUfmipR
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public decimal sFinalLAmt
        {
            get
            {
                return ((IAmortizationDataProvider)this.pageBase).sFinalLAmt;
            }
        }

        /// <inheritdoc />
        public E_sFinMethT sFinMethT
        {
            get { return this.pageBase.sConstructionAmortT; }
        }

        /// <inheritdoc />
        public decimal sGradPmtR
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public int sGradPmtYrs
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public decimal sHouseVal
        {
            get
            {
                return ((IAmortizationDataProvider)this.pageBase).sHouseVal;
            }
        }

        /// <inheritdoc />
        public decimal sApprValFor_LTV_CLTV_HCLTV
        {
            get
            {
                return this.pageBase.sApprValFor_LTV_CLTV_HCLTV;
            }
        } 

        /// <inheritdoc />
        public int sIOnlyMon
        {
            get { return this.pageBase.sConstructionPeriodMon; }
        }

        /// <inheritdoc />
        public bool sIsFullAmortAfterRecast
        {
            get { return false; }
        }

        /// <inheritdoc />
        public bool sIsIntReserveRequired
        {
            get
            {
                return ((IAmortizationDataProvider)this.pageBase).sIsIntReserveRequired;
            }
        }

        /// <inheritdoc />
        public bool sIsOptionArm
        {
            get { return false; }
        }

        /// <inheritdoc />
        public decimal sLAmtCalc
        {
            get
            {
                return ((IAmortizationDataProvider)this.pageBase).sLAmtCalc;
            }
        }

        /// <inheritdoc />
        public E_sLT sLT
        {
            get
            {
                return ((IAmortizationDataProvider)this.pageBase).sLT;
            }
        }

        /// <inheritdoc />
        public decimal sNoteIR
        {
            get { return this.pageBase.sConstructionPeriodIR; }
        }

        /// <inheritdoc />
        public int sOptionArmInitialFixMinPmtPeriod
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public int sOptionArmIntroductoryPeriod
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public bool sOptionArmMinPayIsIOOnly
        {
            get { return false; }
        }

        /// <inheritdoc />
        public E_sOptionArmMinPayOptionT sOptionArmMinPayOptionT
        {
            get { return default(E_sOptionArmMinPayOptionT); }
        }

        /// <inheritdoc />
        public int sOptionArmMinPayPeriod
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public decimal sOptionArmNoteIRDiscount
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public decimal sOptionArmPmtDiscount
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public decimal sOptionArmTeaserR
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public int sPmtAdjCapMon
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public decimal sPmtAdjCapR
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public decimal sPmtAdjMaxBalPc
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public int sPmtAdjRecastPeriodMon
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public int sPmtAdjRecastStop
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public decimal sPpmtAmt
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public int sPpmtMon
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public decimal sPpmtOneAmt
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public int sPpmtOneMon
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public int sPpmtStartMon
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public decimal sProMIns
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public decimal sProMIns2
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public int sProMIns2Mon
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public decimal sProMInsCancelLtv
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public decimal sProMInsCancelAppraisalLtv
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public int sProMInsCancelMinPmts
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public bool sProMInsLckd
        {
            get { return false; }
        }

        /// <inheritdoc />
        public bool sProMInsMidptCancel
        {
            get { return false; }
        }

        /// <inheritdoc />
        public int sProMInsMon
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public decimal sProMInsR
        {
            get { return 0; }
        }

        /// <inheritdoc />
        public E_PercentBaseT sProMInsT
        {
            get { return default(E_PercentBaseT); }
        }

        /// <inheritdoc />
        public int sRAdj1stCapMon
        {
            get { return this.pageBase.sConstructionRAdj1stCapMon; }
        }

        /// <inheritdoc />
        public decimal sRAdj1stCapR
        {
            get { return this.pageBase.sConstructionRAdj1stCapR; }
        }

        /// <inheritdoc />
        public int sRAdjCapMon
        {
            get { return this.pageBase.sConstructionRAdjCapMon; }
        }

        /// <inheritdoc />
        public decimal sRAdjCapR
        {
            get { return this.pageBase.sConstructionRAdjCapR; }
        }

        /// <inheritdoc />
        public decimal sRAdjFloorR
        {
            get { return this.pageBase.sConstructionRAdjFloorR; }
        }

        /// <inheritdoc />
        public decimal sRAdjIndexR
        {
            get { return this.pageBase.sConstructionRAdjIndexR; }
        }

        /// <inheritdoc />
        public decimal sRAdjLifeCapR
        {
            get { return this.pageBase.sConstructionRAdjLifeCapR; }
        }

        /// <inheritdoc />
        public decimal sRAdjMarginR
        {
            get { return this.pageBase.sConstructionRAdjMarginR; }
        }

        /// <inheritdoc />
        public E_sRAdjRoundT sRAdjRoundT
        {
            get { return this.pageBase.sConstructionRAdjRoundT; }
        }

        /// <inheritdoc />
        public decimal sRAdjRoundToR
        {
            get { return this.pageBase.sConstructionRAdjRoundToR; }
        }

        /// <inheritdoc />
        public CDateTime sSchedDueD1
        {
            get { return this.pageBase.sConstructionFirstPaymentD; }
        }

        /// <inheritdoc />
        public int sTerm
        {
            get { return this.pageBase.sConstructionPeriodMon; }
        }

        /// <inheritdoc />
        public decimal Get_sProMIns_ByMonth(int months)
        {
            return 0;
        }
    }
}
