﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using DataAccess.PathDispatch;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// Class to manage preparer records for a CPageData instance. This requires its
    /// own class mainly to maintain the location of workflow checks.
    /// </summary>
    public class PreparerCollection : IPathResolvable
    {
        /// <summary>
        /// The base preparer collection to use.
        /// </summary>
        private PreparerCollectionBase collectionBase;

        /// <summary>
        /// A copy of the preparer data at a prior point in time, which is to be compared to the
        /// current preparer data in order to determine if there are any changes that workflow
        /// cares about.
        /// </summary>
        /// <remarks>
        /// Though this is the data set for all preparers, we only update it when getting the 
        /// 1003 interviewer and only check it when updating that preparer. This is of course a
        /// pretty silly thing to do, but it is existing silliness that it doesn't make sense to
        /// try and fix right now, since we're trying to gut and replace how this entire thing works.
        /// </remarks>
        private DataSet oldData;

        /// <summary>
        /// Initializes a new instance of the <see cref="PreparerCollection"/> class.
        /// </summary>
        /// <param name="validateUpdateCollection">The method to call to make sure an update passes workflow.</param>
        /// <param name="getAgentOfRole">The function used to get an agent of a particular role.</param>
        /// <param name="collectionBase">The base preparer collection instance to use.</param>
        public PreparerCollection(
            Action<string, DataSet, DataSet> validateUpdateCollection,
            Func<E_AgentRoleT, E_ReturnOptionIfNotExist, CAgentFields> getAgentOfRole,
            PreparerCollectionBase collectionBase)
        {
            this.ValidateUpdateCollection = validateUpdateCollection;
            this.collectionBase = collectionBase;
        }

        /// <summary>
        /// Gets the number of preparers in the collection.
        /// </summary>
        /// <value>The number of preparers in the collection.</value>
        public int Count
        {
            get { return this.collectionBase.Count; }
        }

        /// <summary>
        /// Gets all the preparer form types in the preparer collection.
        /// </summary>
        /// <value>All the preparer form types in the preparer collection.</value>
        public IEnumerable<E_PreparerFormT> Keys
        {
            get { return this.collectionBase.Keys; }
        }

        /// <summary>
        /// Gets all the preparers in the preparer collection.
        /// </summary>
        /// <value>All the preparers in the preparer collection.</value>
        public IEnumerable<IPreparerFields> Values
        {
            get { return this.collectionBase.Values; }
        }

        /// <summary>
        /// Gets the method that will be called to determine that an update passes workflow.
        /// </summary>
        private Action<string, DataSet, DataSet> ValidateUpdateCollection { get; }

        /// <summary>
        /// Gets the function that will be called to get an agent record of a particular role.
        /// </summary>
        /// <remarks>
        /// This is expected to be passed in from the <see cref="CPageData"/> object that
        /// this PreparerCollection instance is linked to.
        /// </remarks>
        /// <value>The function that will be called to get an agent record of a particular role.</value>
        private Func<E_AgentRoleT, E_ReturnOptionIfNotExist, CAgentFields> GetAgentOfRole { get; }

        /// <summary>
        /// Gets sets the indicated preparer.
        /// </summary>
        /// <param name="form">The preparer form to get.</param>
        /// <returns>The preparer of the indicated form.</returns>
        public IPreparerFields this[E_PreparerFormT form]
        {
            get { return this.collectionBase[form]; }
        }

        /// <summary>
        /// Determines if the collection contains the selected preparer form.
        /// </summary>
        /// <param name="form">The form to check.</param>
        /// <returns>True if the collection contains the preparer form, false otherwise.</returns>
        public bool ContainsKey(E_PreparerFormT form)
        {
            return this.collectionBase.ContainsKey(form);
        }

        /// <summary>
        /// Adds a preparer to the collection.
        /// </summary>
        /// <param name="preparer">The preparer to add.</param>
        /// <returns>The form of the preparer added.</returns>
        public E_PreparerFormT Add(IPreparerFields preparer)
        {
            return this.collectionBase.Add(preparer);
        }

        /// <summary>
        /// Copies a preparer into this collection. Typically used when copying data from one
        /// loan to another, such as in a lead to loan conversion.
        /// </summary>
        /// <param name="source">The preparer to copy.</param>
        public void AddCopyOfPreparer(IPreparerFields source)
        {
            this.collectionBase.AddCopyOfPreparer(source, this.GetPreparerOfForm);
        }

        /// <summary>
        /// Gets the preparer for the given form, or a new preparer based on the agent whose
        /// role is passed in.
        /// </summary>
        /// <param name="form">The form of preparer to get or create.</param>
        /// <param name="initializingAgentRole">The type of agent whose values will be populated to a new preparer if no preparer of the given form is found in the collection.</param>
        /// <param name="initLinked">Whether or not updates to the agent should automatically update the preparer.</param>
        /// <returns>The preparer for the given form, or a new preparer based on the agent whose role is passed in.</returns>
        public IPreparerFields GetPreparerOfForm(E_PreparerFormT form, E_AgentRoleT initializingAgentRole, bool initLinked)
        {
            return this.collectionBase.GetPreparerOfForm(form, initializingAgentRole, initLinked);
        }

        /// <summary>
        /// Gets the preparer for the given form, or the thing specified by the return option
        /// if that preparer does not yet exist.
        /// </summary>
        /// <param name="form">The form of preparer to get or create.</param>
        /// <param name="returnOption">What to return if no preparer of the given form is found in the collection.</param>
        /// <returns>The preparer for the given form, or the thing specified by the return option.</returns>
        public IPreparerFields GetPreparerOfForm(E_PreparerFormT form, E_ReturnOptionIfNotExist returnOption)
        {
            var ret = this.collectionBase.GetPreparerOfForm(form, returnOption, this.ValidateChange);

            if (form == E_PreparerFormT.App1003Interviewer)
            {
                if (ret.IsValid)
                {
                    this.oldData = this.collectionBase.GetCopyOfBaseData();
                }
            }

            return ret;
        }

        /// <summary>
        /// Returns the value specified by the element.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns>The value specified by the element.</returns>
        public object GetElement(IDataPathElement element)
        {
            if (!(element is DataPathSelectionElement))
            {
                throw new ArgumentException("PreparerCollection can only handle DataPathSelectionElements, but passed element of type " + element.GetType());
            }

            var selectorElement = (DataPathSelectionElement)element;

            E_PreparerFormT preparerRole;

            if (Enum.TryParse(selectorElement.SelectionExpression.ToString(), out preparerRole))
            {
                return this.GetPreparerOfForm(preparerRole, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            }
            else
            {
                throw new ArgumentException("Could not parse preparer argument to a valid preparer form type");
            }
        }

        /// <summary>
        /// Ensures that the changes made to the preparer passed in relative to the old data
        /// stored in the collection are allowed.
        /// </summary>
        /// <param name="newPreparer">The preparer whose changes should be checked.</param>
        private void ValidateChange(IPreparerFields newPreparer)
        {
            if (newPreparer is CPreparerFields)
            {
                CPreparerFields newCPreparer = (CPreparerFields)newPreparer;

                if (this.oldData != null)
                {
                    this.ValidateUpdateCollection("CPreparerFields", this.oldData, newCPreparer.PreparerDataSet);
                }
            }
        }
    }
}
