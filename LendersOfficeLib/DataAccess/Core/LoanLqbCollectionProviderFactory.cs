﻿namespace DataAccess
{
    using System;
    using System.Data.Common;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for creating implementations of the ILoanLqbCollectionProvider interface.
    /// </summary>
    public struct LoanLqbCollectionProviderFactory
    {
        /// <summary>
        /// Factory method for creating a data provider that pulls the data using stored procedures.
        /// </summary>
        /// <param name="loanId">The identifier of the loan.</param>
        /// <param name="storedProcedureDriver">Driver used to call stored procedures.</param>
        /// <param name="loanData">The loan data, typically used to retrieve default values for some properties.</param>
        /// <param name="connectionFactory">A factory method to retrieve database connections.</param>
        /// <param name="userDisplayName">Display name of the logged in user.</param>
        /// <returns>An implementation of the ILoanLqbCollectionProvider interface.</returns>
        public ILoanLqbCollectionProvider CreateStoredProcedureProvider(
            DataObjectIdentifier<DataObjectKind.Loan, Guid> loanId,
            IStoredProcedureDriver storedProcedureDriver,
            ILoanLqbCollectionContainerLoanDataProvider loanData,
            Func<DbConnection> connectionFactory,
            string userDisplayName)
        {
            return new LoanLqbCollectionProvider(loanId, storedProcedureDriver, loanData, connectionFactory, userDisplayName);
        }

        /// <summary>
        /// Factory method for creating a data provider that returns empty collections.
        /// </summary>
        /// <returns>An implementation of the ILoanLqbCollectionProvider interface.</returns>
        public ILoanLqbCollectionProvider CreateEmptyProvider()
        {
            return new LoanLqbEmptyCollectionProvider();
        }
    }
}
