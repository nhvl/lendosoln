﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// A class that represents at estimated data for the construction phase
    /// of a loan. May also contain the permanent phase of a construction to
    /// permanent loan.
    /// </summary>
    public class ConstructionEstAmortTable : AmortTable
    {
        /// <summary>
        /// Whether or not this table is destined to be combined with
        /// another table in the future. This primarily affects whether
        /// or not a final balloon payment is calculated.
        /// </summary>
        private bool isCombined;

        /// <summary>
        /// Whether or not to assume that the the entire commitment amount
        /// will be outstanding starting immediately after the first payment.
        /// This is required by the TRID Loan Terms section to simulate the
        /// worst-case scenario for the draw amount, but that is not how the
        /// Projected Payments table is to be computed.
        /// </summary>
        private bool isFullDrawAfterFirstPmt;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConstructionEstAmortTable"/> class.
        /// </summary>
        /// <param name="dataLoan">The loan (or equivalent) from which to get amortization terms.</param>
        /// <param name="scheduleType">Whether the schedule is standard, best-case, worst-case.</param>
        /// <param name="isGrouped">Whether or not to group the items in the table.</param>
        /// <param name="isCombined">Whether the table will be combined with another table.</param>
        /// <param name="isFullDrawAfterFirstPmt">Whether to assume a full draw starting with the second payment.</param>
        public ConstructionEstAmortTable(IAmortizationDataProvider dataLoan, E_AmortizationScheduleT scheduleType, bool isGrouped, bool isCombined, bool isFullDrawAfterFirstPmt)
            : base(dataLoan, scheduleType, isGrouped)
        {
            this.isCombined = isCombined;
            this.isFullDrawAfterFirstPmt = isFullDrawAfterFirstPmt;

            base.InitAmortTable(scheduleType, isGrouped);
        }

        /// <summary>
        /// Appends another <see cref="AmortTable"/> on to this one.
        /// </summary>
        /// <param name="followingTable">The table to append.</param>
        public void AppendAmortItems(AmortTable followingTable)
        {
            int beginAppendIndex = this.m_items.Length;
            AmortItem lastConstructionItem = this.m_items[this.m_items.Length - 1];

            this.m_items = m_items.Concat(followingTable.Items).ToArray();

            // Sadly, I'm pretty sure that these changes can leak out into sAmortTable, but
            // at the moment they should have no effect except on the TridProjectedPayment
            // class, which will be getting the combined table anyways.
            if (this.m_items[beginAppendIndex].Principal != 0)
            {
                this.m_items[beginAppendIndex].AddChangeEvent(E_AmortChangeEventType.IOExpired);
            }

            if (this.m_items[beginAppendIndex].Rate != lastConstructionItem.Rate)
            {
                this.m_items[beginAppendIndex].AddChangeEvent(E_AmortChangeEventType.RateChange);
            }

            if (this.m_items[beginAppendIndex].Payment != lastConstructionItem.Payment)
            {
                this.m_items[beginAppendIndex].AddChangeEvent(E_AmortChangeEventType.PIChange);
            }
        }

        /// <summary>
        /// Creates a new <see cref="AmortItem"/> instance with the given parameters.
        /// This override creates the construction estimate AmortItem version.
        /// </summary>
        /// <param name="index">The index of the item.</param>
        /// <param name="previousItem">The previous item in the table.</param>
        /// <param name="dataLoan">The data source for amortization terms.</param>
        /// <param name="scheduleType">Whether the schedule is standard, best-case, worst-case.</param>
        /// <returns>The <see cref="AmortItem"/> created.</returns>
        protected override AmortItem CreateAmortItem(int index, AmortItem previousItem, IAmortizationDataProvider dataLoan, E_AmortizationScheduleT scheduleType)
        {
            return new ConstructionEstAmortItem(index, previousItem, dataLoan, scheduleType, this.isCombined, this.isFullDrawAfterFirstPmt);
        }

        /// <summary>
        /// Nullifies the InitAmortTable(...) call in the constructor of the parent class
        /// so that the additional setup that occurs in this class's constructor has a
        /// chance to happen before the calculations occur.
        /// </summary>
        /// <param name="scheduleType">Whether the schedule is standard, best-case, worst-case.</param>
        /// <param name="isGrouped">Whether or not to group the items in the table.</param>
        protected override void InitAmortTable(E_AmortizationScheduleT scheduleType, bool isGrouped)
        {
            return;
        }
    }
}
