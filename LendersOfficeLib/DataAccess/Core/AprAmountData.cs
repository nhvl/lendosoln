﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the set of data that needs to be fed into the general
    /// APR equation for an individual advance or payment. 
    /// </summary>
    public struct AprAmountData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AprAmountData"/> struct.
        /// </summary>
        /// <param name="amount">The dollar amount of the advance or payment.</param>
        /// <param name="unitPeriod">The unit period of the advance or payment.</param>
        /// <param name="oddDays">The odd days of the advance or payment.</param>
        public AprAmountData(decimal amount, int unitPeriod, int oddDays)
            : this()
        {
            this.Amount = amount;
            this.UnitPeriod = unitPeriod;
            this.OddDays = oddDays;
        }

        /// <summary>
        /// Gets the dollar amount of the advance or payment.
        /// </summary>
        public decimal Amount { get; }

        /// <summary>
        /// Gets the number of unit periods for the advance or payment.
        /// </summary>
        public int UnitPeriod { get; }

        /// <summary>
        /// Gets the number of odd days for the advance or payment.
        /// </summary>
        public int OddDays { get; }
    }
}
