﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOfficeApp.los.RatePrice;
using System.Xml;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Common.SerializationTypes;
using LendersOffice.Migration;
using LendersOffice.Security;
using System.IO;
using LendersOffice.ObjLib.CustomAttributes;
using LqbGrammar.DataTypes;

namespace DataAccess
{
    public partial class CPageBase
    {
        #region Settlement Charges Fields

        #region OPM 150695 Settlement Charge => GFE Migration

        /// <summary>
        /// Indicates whether the Settlement Charge fields should use the GFE database fields
        /// or the original Settlement Charge database fields.
        /// </summary>
        [DependsOn]
        public bool sUseGFEDataForSCFields
        {
            get { return GetBoolField("sUseGFEDataForSCFields"); }
            private set { SetBoolField("sUseGFEDataForSCFields", value); }
        }

        [DependsOn(nameof(sUseGFEDataForSCFields), nameof(sClosingCostMigrationArchive))]
        public bool sHasClosingCostMigrationArchive
        {
            get
            {
                if (!sUseGFEDataForSCFields)
                {
                    return false;
                }

                return sClosingCostMigrationArchive != null;
            }
        }

        private ClosingCostMigrationArchive m_closingCostMigrationArchive = null;
        [DependsOn(nameof(sUseGFEDataForSCFields))]
        private ClosingCostMigrationArchive sClosingCostMigrationArchive
        {
            get
            {
                if (!sUseGFEDataForSCFields)
                {
                    throw new GenericUserErrorMessageException("Programming Error: Should not try to access the closing cost migration archive if the file isn't using GFE data for SC fields.");
                }

                if (m_closingCostMigrationArchive == null)
                {
                    m_closingCostMigrationArchive = RetrieveClosingCostMigrationArchive();
                }
                return m_closingCostMigrationArchive;
            }
        }

        [DependsOn(nameof(sHasClosingCostMigrationArchive), nameof(sClosingCostMigrationArchive))]
        public GFEArchive sClosingCostMigrationGFEArchive
        {
            get 
            {
                if (sHasClosingCostMigrationArchive)
                {
                    return sClosingCostMigrationArchive.GFEArchive; 
                }

                return null;
            }
        }

        [DependsOn(nameof(sHasClosingCostMigrationArchive), nameof(sClosingCostMigrationArchive))]
        private string sClosingCostMigrationArchiveSourceEmployeeNm
        {
            get
            {
                if (sHasClosingCostMigrationArchive)
                {
                    if (sClosingCostMigrationArchive.SourceEmployeeId != Guid.Empty)
                    {
                        var employee = new EmployeeDB(sClosingCostMigrationArchive.SourceEmployeeId, sBrokerId);
                        if (employee.Retrieve())
                        {
                            return employee.FirstName + " " + employee.LastName;
                        }
                    }
                    else
                    {
                        return "SYSTEM";
                    }
                }

                return "";
            }
        }

        [DependsOn(nameof(sHasClosingCostMigrationArchive), nameof(sClosingCostMigrationArchive), nameof(sClosingCostMigrationArchiveSourceEmployeeNm))]
        public string sCurrentClosingCostStructure
        {
            get
            {
                if (sHasClosingCostMigrationArchive)
                {
                    var migrationDate = sClosingCostMigrationArchive.GFEArchive.DateArchived;
                    return string.Format("Last Disclosed and Most Current (migrated {0} by {1})", migrationDate, sClosingCostMigrationArchiveSourceEmployeeNm);
                }
                else
                {
                    return "GFE and Settlement Charges (not migrated)";
                }
            }
        }

        /// <summary>
        /// The current closing cost source. If the file has been migrated, will return the value 
        /// that was used when file was migrated. Otherwise, will return default value.
        /// </summary>
        [DependsOn(nameof(sHasClosingCostMigrationArchive), nameof(sClosingCostMigrationArchive), nameof(sSettlementChargesExportSource))]
        public E_CurrentClosingCostSourceT sClosingCostMigrationCurrentClosingCostSource
        {
            get
            {
                if (sHasClosingCostMigrationArchive)
                {
                    return sClosingCostMigrationArchive.CurrentClosingCostSource;
                }
                else
                {
                    return (sSettlementChargesExportSource == E_SettlementChargesExportSource.GFE) ?
                        E_CurrentClosingCostSourceT.GFE : E_CurrentClosingCostSourceT.SettlementCharges;
                }
            }
        }

        /// <summary>
        /// The last disclosed closing cost source. If the file has been migrated, will return the 
        /// value that was used when file was migrated. Otherwise, will return default value.
        /// </summary>
        [DependsOn(nameof(sHasClosingCostMigrationArchive), nameof(sClosingCostMigrationArchive))]
        public E_LastDisclosedClosingCostSourceT sClosingCostMigrationLastDisclosedClosingCostSource
        {
            get
            {
                if (sHasClosingCostMigrationArchive)
                {
                    return sClosingCostMigrationArchive.LastDisclosedClosingCostSource;
                }
                else
                {
                    if (GFEArchives.Count() > 0)
                    {
                        return E_LastDisclosedClosingCostSourceT.GFEArchive;
                    }
                    else
                    {
                        return E_LastDisclosedClosingCostSourceT.GFE;
                    }
                }
            }
        }

        [DependsOn(nameof(sUseGFEDataForSCFields), nameof(sfAssignSettlementChargeFieldsToGFE), nameof(sfArchiveGFE), nameof(sfExtractGFEArchive), nameof(sfAssignSettlementChargeDflpPropToGFE))]
        private bool sfMigrateCurrentClosingCostDataToGFE { get { return false; } }
        /// <summary>
        /// Migrates the closing costs such that the GFE Archive will contain the last disclosed
        /// version of the closing costs and the GFE will contain the current closing data. Upon
        /// completion, the Settlement Charge fields will share the GFE data-layer.
        /// May add a GFE archive. May also assign Settlement Charge fields to GFE.
        /// </summary>
        /// <param name="currentClosingCostSource">The page with the current closing costs.</param>
        /// <param name="lastDisclosedClosingCostSource">The location of the last disclosed closing costs.</param>
        public void MigrateCurrentClosingCostDataToGFE(E_CurrentClosingCostSourceT currentClosingCostSource, E_LastDisclosedClosingCostSourceT lastDisclosedClosingCostSource)
        {
            if (!BrokerDB.IsGFEandCoCVersioningEnabled)
            {
                throw new GenericUserErrorMessageException("Programming Error: Should not call WriteSettlementChargeGFEMigrationArchiveToFileDB when GFE Archive is not enabled.");
            }
            else if (sUseGFEDataForSCFields)
            {
                throw new GenericUserErrorMessageException("Programming Error: Should not try to run the closing cost migration if the file is already using GFE data for SC fields.");
            }

            if (lastDisclosedClosingCostSource == E_LastDisclosedClosingCostSourceT.GFE)
            {
                try
                {
                    m_isMigratingClosingCost = true;
                    ArchiveGFE(true, E_GFEArchivedReasonT.ClosingCostMigration);
                }
                finally
                {
                    m_isMigratingClosingCost = false;
                }
            }

            var gfeArchive = (GFEArchive)ExtractGFEArchive(true);
            gfeArchive.IsClosingCostMigrationArchive = true;
            gfeArchive.DateArchived = Tools.GetDateTimeDescription(DateTime.Now, x_gfeArchivedDateFormat);

            var migrationArchive = new ClosingCostMigrationArchive()
            {
                GFEArchive = gfeArchive,
                SourceEmployeeId = (PrincipalFactory.CurrentPrincipal != null) ? PrincipalFactory.CurrentPrincipal.EmployeeId : Guid.Empty,
                CurrentClosingCostSource = currentClosingCostSource,
                LastDisclosedClosingCostSource = lastDisclosedClosingCostSource
            };
            WriteClosingCostMigrationArchiveToFileDB(migrationArchive);

            if (currentClosingCostSource == E_CurrentClosingCostSourceT.SettlementCharges)
            {
                AssignSettlementChargeFieldsToGFE();
            }
            else // if (currentClosingCostSource == E_CurrentClosingCostSourceT.GFE)
            {
                // OPM 180608 - Merge Settlement charges props DFLP field into GFE props
                AssignSettlementChargeDflpPropToGFE();
            }

            sUseGFEDataForSCFields = true;
        }

        private static int GenerateGFEPropsFromSettlementProps(int gfeProps, int scProps)
        {
            return LosConvert.GfeItemProps_Pack(
                LosConvert.GfeItemProps_Apr(gfeProps),
                LosConvert.GfeItemProps_ToBr(gfeProps), // To Broker is shared between GFE/SC pages
                LosConvert.GfeItemProps_Payer(scProps),
                LosConvert.GfeItemProps_FhaAllow(gfeProps),
                LosConvert.GfeItemProps_Poc(scProps),
                LosConvert.GfeItemProps_Dflp(scProps),
                LosConvert.GfeItemProps_GBF(gfeProps),
                LosConvert.GfeItemProps_BF(gfeProps),
                LosConvert.GfeItemProps_PaidToThirdParty(scProps),
                LosConvert.GfeItemProps_ThisPartyIsAffiliate(scProps),
                LosConvert.GfeItemProps_ShowQmWarning(scProps)
            );
        }

        #region Assigning SC DFLP Prop to GFE Props
        [DependsOn(nameof(sSettlementU3RsrvProps), nameof(sU3RsrvProps), nameof(sSettlementU4RsrvProps), 
            nameof(sU4RsrvProps), nameof(sSettlementDiscountPointFProps), nameof(sGfeDiscountPointFProps), nameof(sSettlementLOrigFProps), nameof(sLOrigFProps), 
            nameof(sSettlementLDiscntProps), nameof(sLDiscntProps), nameof(sSettlementApprFProps), nameof(sApprFProps), nameof(sSettlementCrFProps), nameof(sCrFProps), 
            nameof(sSettlementTxServFProps), nameof(sTxServFProps), nameof(sSettlementFloodCertificationFProps), nameof(sFloodCertificationFProps), nameof(sSettlementInspectFProps), 
            nameof(sInspectFProps), nameof(sSettlementProcFProps), nameof(sProcFProps), nameof(sSettlementUwFProps), nameof(sUwFProps), nameof(sSettlementWireFProps), 
            nameof(sWireFProps), nameof(sSettlement800U1FProps), nameof(s800U1FProps), nameof(sSettlement800U2FProps), nameof(s800U2FProps), nameof(sSettlement800U3FProps), 
            nameof(s800U3FProps), nameof(sSettlement800U4FProps), nameof(s800U4FProps), nameof(sSettlement800U5FProps), nameof(s800U5FProps), nameof(sSettlementIPiaProps), 
            nameof(sIPiaProps), nameof(sSettlementHazInsPiaProps), nameof(sHazInsPiaProps), nameof(sSettlement904PiaProps), nameof(s904PiaProps), nameof(sSettlement900U1PiaProps), 
            nameof(s900U1PiaProps), nameof(sSettlementHazInsRsrvProps), nameof(sHazInsRsrvProps), nameof(sSettlementMInsRsrvProps), nameof(sMInsRsrvProps), nameof(sSettlementRealETxRsrvProps), 
            nameof(sRealETxRsrvProps), nameof(sSettlementSchoolTxRsrvProps), nameof(sSchoolTxRsrvProps), nameof(sSettlementFloodInsRsrvProps), nameof(sFloodInsRsrvProps), 
            nameof(sSettlementAggregateAdjRsrvProps), nameof(sAggregateAdjRsrvProps), nameof(sSettlement1008RsrvProps), nameof(s1006RsrvProps), nameof(sSettlement1009RsrvProps), 
            nameof(s1007RsrvProps), nameof(sSettlementEscrowFProps), nameof(sEscrowFProps), nameof(sSettlementOwnerTitleInsFProps), nameof(sOwnerTitleInsProps), 
            nameof(sSettlementTitleInsFProps), nameof(sTitleInsFProps), nameof(sSettlementDocPrepFProps), nameof(sDocPrepFProps), nameof(sSettlementNotaryFProps), 
            nameof(sNotaryFProps), nameof(sSettlementAttorneyFProps), nameof(sAttorneyFProps), nameof(sSettlementU1TcProps), nameof(sU1TcProps), nameof(sSettlementU2TcProps), 
            nameof(sU2TcProps), nameof(sSettlementU3TcProps), nameof(sU3TcProps), nameof(sSettlementU4TcProps), nameof(sU4TcProps), nameof(sSettlementRecFProps), nameof(sRecFProps), 
            nameof(sSettlementCountyRtcProps), nameof(sCountyRtcProps), nameof(sSettlementStateRtcProps), nameof(sStateRtcProps), nameof(sSettlementU1GovRtcProps), 
            nameof(sU1GovRtcProps), nameof(sSettlementU2GovRtcProps), nameof(sU2GovRtcProps), nameof(sSettlementU3GovRtcProps), nameof(sU3GovRtcProps), nameof(sSettlementPestInspectFProps), 
            nameof(sPestInspectFProps), nameof(sSettlementU1ScProps), nameof(sU1ScProps), nameof(sSettlementU2ScProps), nameof(sU2ScProps), nameof(sSettlementU4ScProps), 
            nameof(sU4ScProps), nameof(sSettlementU3ScProps), nameof(sU3ScProps), nameof(sSettlementU5ScProps), nameof(sU5ScProps), nameof(sSettlementMBrokFProps), nameof(sMBrokFProps), 
            nameof(sfExtractStaticGFEArchive))]
        #endregion
        private bool sfAssignSettlementChargeDflpPropToGFE { get { return false; } }
        /// <summary>
        /// Assign all 800-1300 Dflp props from the Settlement Charges page which are
        /// not already shared in the data-layer with the GFE to the GFE.
        /// </summary>
        private void AssignSettlementChargeDflpPropToGFE()
        {
            #region Assign to GFE fields from SC props
            sU3RsrvProps                = LosConvert.GfeItemProps_UpdateDflp(sU3RsrvProps,              LosConvert.GfeItemProps_Dflp(sSettlementU3RsrvProps                 ));
            sU4RsrvProps                = LosConvert.GfeItemProps_UpdateDflp(sU4RsrvProps,              LosConvert.GfeItemProps_Dflp(sSettlementU4RsrvProps                 ));
            sGfeDiscountPointFProps     = LosConvert.GfeItemProps_UpdateDflp(sGfeDiscountPointFProps,   LosConvert.GfeItemProps_Dflp(sSettlementDiscountPointFProps         ));
            sLOrigFProps                = LosConvert.GfeItemProps_UpdateDflp(sLOrigFProps,              LosConvert.GfeItemProps_Dflp(sSettlementLOrigFProps                 ));
            sLDiscntProps               = LosConvert.GfeItemProps_UpdateDflp(sLDiscntProps,             LosConvert.GfeItemProps_Dflp(sSettlementLDiscntProps                ));
            sApprFProps                 = LosConvert.GfeItemProps_UpdateDflp(sApprFProps,               LosConvert.GfeItemProps_Dflp(sSettlementApprFProps                  ));
            sCrFProps                   = LosConvert.GfeItemProps_UpdateDflp(sCrFProps,                 LosConvert.GfeItemProps_Dflp(sSettlementCrFProps                    ));
            sTxServFProps               = LosConvert.GfeItemProps_UpdateDflp(sTxServFProps,             LosConvert.GfeItemProps_Dflp(sSettlementTxServFProps                ));
            sFloodCertificationFProps   = LosConvert.GfeItemProps_UpdateDflp(sFloodCertificationFProps, LosConvert.GfeItemProps_Dflp(sSettlementFloodCertificationFProps    ));
            sInspectFProps              = LosConvert.GfeItemProps_UpdateDflp(sInspectFProps,            LosConvert.GfeItemProps_Dflp(sSettlementInspectFProps               ));
            sProcFProps                 = LosConvert.GfeItemProps_UpdateDflp(sProcFProps,               LosConvert.GfeItemProps_Dflp(sSettlementProcFProps                  ));
            sUwFProps                   = LosConvert.GfeItemProps_UpdateDflp(sUwFProps,                 LosConvert.GfeItemProps_Dflp(sSettlementUwFProps                    ));
            sWireFProps                 = LosConvert.GfeItemProps_UpdateDflp(sWireFProps,               LosConvert.GfeItemProps_Dflp(sSettlementWireFProps                  ));
            s800U1FProps                = LosConvert.GfeItemProps_UpdateDflp(s800U1FProps,              LosConvert.GfeItemProps_Dflp(sSettlement800U1FProps                 ));
            s800U2FProps                = LosConvert.GfeItemProps_UpdateDflp(s800U2FProps,              LosConvert.GfeItemProps_Dflp(sSettlement800U2FProps                 ));
            s800U3FProps                = LosConvert.GfeItemProps_UpdateDflp(s800U3FProps,              LosConvert.GfeItemProps_Dflp(sSettlement800U3FProps                 ));
            s800U4FProps                = LosConvert.GfeItemProps_UpdateDflp(s800U4FProps,              LosConvert.GfeItemProps_Dflp(sSettlement800U4FProps                 ));
            s800U5FProps                = LosConvert.GfeItemProps_UpdateDflp(s800U5FProps,              LosConvert.GfeItemProps_Dflp(sSettlement800U5FProps                 ));
            sIPiaProps                  = LosConvert.GfeItemProps_UpdateDflp(sIPiaProps,                LosConvert.GfeItemProps_Dflp(sSettlementIPiaProps                   ));
            sHazInsPiaProps             = LosConvert.GfeItemProps_UpdateDflp(sHazInsPiaProps,           LosConvert.GfeItemProps_Dflp(sSettlementHazInsPiaProps              ));
            s904PiaProps                = LosConvert.GfeItemProps_UpdateDflp(s904PiaProps,              LosConvert.GfeItemProps_Dflp(sSettlement904PiaProps                 ));
            s900U1PiaProps              = LosConvert.GfeItemProps_UpdateDflp(s900U1PiaProps,            LosConvert.GfeItemProps_Dflp(sSettlement900U1PiaProps               ));
            sHazInsRsrvProps            = LosConvert.GfeItemProps_UpdateDflp(sHazInsRsrvProps,          LosConvert.GfeItemProps_Dflp(sSettlementHazInsRsrvProps             ));
            sMInsRsrvProps              = LosConvert.GfeItemProps_UpdateDflp(sMInsRsrvProps,            LosConvert.GfeItemProps_Dflp(sSettlementMInsRsrvProps               ));
            sRealETxRsrvProps           = LosConvert.GfeItemProps_UpdateDflp(sRealETxRsrvProps,         LosConvert.GfeItemProps_Dflp(sSettlementRealETxRsrvProps            ));
            sSchoolTxRsrvProps          = LosConvert.GfeItemProps_UpdateDflp(sSchoolTxRsrvProps,        LosConvert.GfeItemProps_Dflp(sSettlementSchoolTxRsrvProps           ));
            sFloodInsRsrvProps          = LosConvert.GfeItemProps_UpdateDflp(sFloodInsRsrvProps,        LosConvert.GfeItemProps_Dflp(sSettlementFloodInsRsrvProps           ));
            sAggregateAdjRsrvProps      = LosConvert.GfeItemProps_UpdateDflp(sAggregateAdjRsrvProps,    LosConvert.GfeItemProps_Dflp(sSettlementAggregateAdjRsrvProps       ));
            s1006RsrvProps              = LosConvert.GfeItemProps_UpdateDflp(s1006RsrvProps,            LosConvert.GfeItemProps_Dflp(sSettlement1008RsrvProps               ));
            s1007RsrvProps              = LosConvert.GfeItemProps_UpdateDflp(s1007RsrvProps,            LosConvert.GfeItemProps_Dflp(sSettlement1009RsrvProps               ));
            sEscrowFProps               = LosConvert.GfeItemProps_UpdateDflp(sEscrowFProps,             LosConvert.GfeItemProps_Dflp(sSettlementEscrowFProps                ));
            sOwnerTitleInsProps         = LosConvert.GfeItemProps_UpdateDflp(sOwnerTitleInsProps,       LosConvert.GfeItemProps_Dflp(sSettlementOwnerTitleInsFProps         ));
            sTitleInsFProps             = LosConvert.GfeItemProps_UpdateDflp(sTitleInsFProps,           LosConvert.GfeItemProps_Dflp(sSettlementTitleInsFProps              ));
            sDocPrepFProps              = LosConvert.GfeItemProps_UpdateDflp(sDocPrepFProps,            LosConvert.GfeItemProps_Dflp(sSettlementDocPrepFProps               ));
            sNotaryFProps               = LosConvert.GfeItemProps_UpdateDflp(sNotaryFProps,             LosConvert.GfeItemProps_Dflp(sSettlementNotaryFProps                ));
            sAttorneyFProps             = LosConvert.GfeItemProps_UpdateDflp(sAttorneyFProps,           LosConvert.GfeItemProps_Dflp(sSettlementAttorneyFProps              ));
            sU1TcProps                  = LosConvert.GfeItemProps_UpdateDflp(sU1TcProps,                LosConvert.GfeItemProps_Dflp(sSettlementU1TcProps                   ));
            sU2TcProps                  = LosConvert.GfeItemProps_UpdateDflp(sU2TcProps,                LosConvert.GfeItemProps_Dflp(sSettlementU2TcProps                   ));
            sU3TcProps                  = LosConvert.GfeItemProps_UpdateDflp(sU3TcProps,                LosConvert.GfeItemProps_Dflp(sSettlementU3TcProps                   ));
            sU4TcProps                  = LosConvert.GfeItemProps_UpdateDflp(sU4TcProps,                LosConvert.GfeItemProps_Dflp(sSettlementU4TcProps                   ));
            sRecFProps                  = LosConvert.GfeItemProps_UpdateDflp(sU4TcProps,                LosConvert.GfeItemProps_Dflp(sSettlementRecFProps                   ));
            sCountyRtcProps             = LosConvert.GfeItemProps_UpdateDflp(sU4TcProps,                LosConvert.GfeItemProps_Dflp(sSettlementCountyRtcProps              ));
            sStateRtcProps              = LosConvert.GfeItemProps_UpdateDflp(sU4TcProps,                LosConvert.GfeItemProps_Dflp(sSettlementStateRtcProps               ));
            sU1GovRtcProps              = LosConvert.GfeItemProps_UpdateDflp(sU4TcProps,                LosConvert.GfeItemProps_Dflp(sSettlementU1GovRtcProps               ));
            sU2GovRtcProps              = LosConvert.GfeItemProps_UpdateDflp(sU4TcProps,                LosConvert.GfeItemProps_Dflp(sSettlementU2GovRtcProps               ));
            sU3GovRtcProps              = LosConvert.GfeItemProps_UpdateDflp(sU4TcProps,                LosConvert.GfeItemProps_Dflp(sSettlementU3GovRtcProps               ));
            sPestInspectFProps          = LosConvert.GfeItemProps_UpdateDflp(sU4TcProps,                LosConvert.GfeItemProps_Dflp(sSettlementPestInspectFProps           ));
            sU1ScProps                  = LosConvert.GfeItemProps_UpdateDflp(sU4TcProps,                LosConvert.GfeItemProps_Dflp(sSettlementU1ScProps                   ));
            sU2ScProps                  = LosConvert.GfeItemProps_UpdateDflp(sU4TcProps,                LosConvert.GfeItemProps_Dflp(sSettlementU2ScProps                   ));
            sU3ScProps                  = LosConvert.GfeItemProps_UpdateDflp(sU3ScProps,                LosConvert.GfeItemProps_Dflp(sSettlementU3ScProps                   ));
            sU4ScProps                  = LosConvert.GfeItemProps_UpdateDflp(sU4TcProps,                LosConvert.GfeItemProps_Dflp(sSettlementU4ScProps                   ));
            sU5ScProps                  = LosConvert.GfeItemProps_UpdateDflp(sU5ScProps,                LosConvert.GfeItemProps_Dflp(sSettlementU5ScProps                   ));
            sMBrokFProps                = LosConvert.GfeItemProps_UpdateDflp(sMBrokFProps,              LosConvert.GfeItemProps_Dflp(sSettlementMBrokFProps                 ));
            #endregion Assign to GFE fields from SC fields
        }

        #region Assigning SC Fields to GFE Dependencies
        [DependsOn(nameof(sSettlementLOrigFPc), nameof(sLOrigFPc), nameof(sSettlementLOrigFMb), nameof(sLOrigFMb), nameof(sSettlementLDiscntPc), nameof(sLDiscntPc), nameof(sSettlementLDiscntBaseT), 
            nameof(sLDiscntBaseT), nameof(sSettlementLDiscntFMb), nameof(sLDiscntFMb), nameof(sSettlementCreditLenderPaidItemT), nameof(sGfeCreditLenderPaidItemT), 
            nameof(sSettlementApprF), nameof(sApprF), nameof(sSettlementCrF), nameof(sCrF), nameof(sSettlementTxServF), nameof(sTxServF), nameof(sSettlementFloodCertificationDeterminationT), 
            nameof(sFloodCertificationDeterminationT), nameof(sSettlementFloodCertificationF), nameof(sFloodCertificationF), nameof(sSettlementMBrokFPc), 
            nameof(sMBrokFPc), nameof(sSettlementMBrokFBaseT), nameof(sMBrokFBaseT), nameof(sSettlementMBrokFMb), nameof(sMBrokFMb), nameof(sSettlementInspectF), nameof(sInspectF), 
            nameof(sSettlementProcF), nameof(sProcF), nameof(sSettlementUwF), nameof(sUwF), nameof(sSettlementWireF), nameof(sWireF), nameof(sSettlement800U1FDesc), nameof(s800U1FDesc), 
            nameof(sSettlement800U1F), nameof(s800U1F), nameof(sSettlement800U2FDesc), nameof(s800U2FDesc), nameof(sSettlement800U2F), nameof(s800U2F), nameof(sSettlement800U3FDesc), 
            nameof(s800U3FDesc), nameof(sSettlement800U3F), nameof(s800U3F), nameof(sSettlement800U4FDesc), nameof(s800U4FDesc), nameof(sSettlement800U4F), nameof(s800U4F), nameof(sSettlement800U5FDesc), 
            nameof(s800U5FDesc), nameof(sSettlement800U5F), nameof(s800U5F), nameof(sSettlementIPiaDyLckd), nameof(sIPiaDyLckd), nameof(sSettlementIPiaDy), nameof(sIPiaDy), nameof(sSettlementIPerDayLckd), 
            nameof(sIPerDayLckd), nameof(sSettlementIPerDay), nameof(sIPerDay), nameof(sSettlementHazInsPiaMon), nameof(sHazInsPiaMon), nameof(sSettlement904PiaDesc), 
            nameof(s904PiaDesc), nameof(sSettlement904Pia), nameof(s904Pia), nameof(sSettlement900U1PiaDesc), nameof(s900U1PiaDesc), nameof(sSettlement900U1Pia), nameof(s900U1Pia), 
            nameof(sSettlementHazInsRsrvMon), nameof(sHazInsRsrvMon), nameof(sSettlementMInsRsrvMon), nameof(sMInsRsrvMon), nameof(sSettlementRealETxRsrvMon), 
            nameof(sRealETxRsrvMon), nameof(sSettlementSchoolTxRsrvMon), nameof(sSchoolTxRsrvMon), nameof(sSettlementFloodInsRsrvMon), nameof(sFloodInsRsrvMon), 
            nameof(sSettlementAggregateAdjRsrvLckd), nameof(sAggregateAdjRsrvLckd), nameof(sSettlementAggregateAdjRsrv), nameof(sAggregateAdjRsrv), nameof(sSettlementU3RsrvMon), 
            nameof(sU3RsrvMon), nameof(sSettlementU4RsrvMon), nameof(sU4RsrvMon), nameof(sSettlementEscrowF), nameof(sEscrowF), nameof(sSettlementOwnerTitleInsF), nameof(sOwnerTitleInsF), 
            nameof(sSettlementTitleInsF), nameof(sTitleInsF), nameof(sSettlementDocPrepF), nameof(sDocPrepF), nameof(sSettlementNotaryF), nameof(sNotaryF), nameof(sSettlementAttorneyF), 
            nameof(sAttorneyF), nameof(sSettlementU1TcDesc), nameof(sU1TcDesc), nameof(sSettlementU1Tc), nameof(sU1Tc), nameof(sSettlementU2TcDesc), nameof(sU2TcDesc), nameof(sSettlementU2Tc), 
            nameof(sU2Tc), nameof(sSettlementU3TcDesc), nameof(sU3TcDesc), nameof(sSettlementU3Tc), nameof(sU3Tc), nameof(sSettlementU4TcDesc), nameof(sU4TcDesc), nameof(sSettlementU4Tc), 
            nameof(sU4Tc), nameof(sSettlementRecFPc), nameof(sRecFPc), nameof(sSettlementRecBaseT), nameof(sRecBaseT), nameof(sSettlementRecFMb), nameof(sRecFMb), nameof(sSettlementRecDeed), nameof(sRecDeed), nameof(sSettlementRecMortgage), nameof(sRecMortgage), nameof(sSettlementRecRelease), nameof(sRecRelease), nameof(sRecFLckd), 
            nameof(sSettlementCountyRtcPc), nameof(sCountyRtcPc), nameof(sSettlementCountyRtcBaseT), nameof(sCountyRtcBaseT), nameof(sSettlementCountyRtcMb), nameof(sCountyRtcMb), nameof(sSettlementStateRtcPc), 
            nameof(sStateRtcPc), nameof(sSettlementStateRtcBaseT), nameof(sStateRtcBaseT), nameof(sSettlementStateRtcMb), nameof(sStateRtcMb), nameof(sSettlementU1GovRtcDesc), 
            nameof(sU1GovRtcDesc), nameof(sSettlementU1GovRtcPc), nameof(sU1GovRtcPc), nameof(sSettlementU1GovRtcBaseT), nameof(sU1GovRtcBaseT), nameof(sSettlementU1GovRtcMb), 
            nameof(sU1GovRtcMb), nameof(sSettlementU2GovRtcDesc), nameof(sU2GovRtcDesc), nameof(sSettlementU2GovRtcPc), nameof(sU2GovRtcPc), nameof(sSettlementU2GovRtcBaseT), 
            nameof(sU2GovRtcBaseT), nameof(sSettlementU2GovRtcMb), nameof(sU2GovRtcMb), nameof(sSettlementU3GovRtcDesc), nameof(sU3GovRtcDesc), nameof(sSettlementU3GovRtcPc), 
            nameof(sU3GovRtcPc), nameof(sSettlementU3GovRtcBaseT), nameof(sU3GovRtcBaseT), nameof(sSettlementU3GovRtcMb), nameof(sU3GovRtcMb), nameof(sSettlementPestInspectF), 
            nameof(sPestInspectF), nameof(sSettlementU1ScDesc), nameof(sU1ScDesc), nameof(sSettlementU1Sc), nameof(sU1Sc), nameof(sSettlementU2ScDesc), nameof(sU2ScDesc), nameof(sSettlementU2Sc), 
            nameof(sU2Sc), nameof(sSettlementU3ScDesc), nameof(sU3ScDesc), nameof(sSettlementU3Sc), nameof(sU3Sc), nameof(sSettlementU4ScDesc), nameof(sU4ScDesc), nameof(sSettlementU4Sc), 
            nameof(sU4Sc), nameof(sSettlementU5ScDesc), nameof(sU5ScDesc), nameof(sSettlementU5Sc), nameof(sU5Sc), nameof(s800U1FSettlementSection), nameof(s800U1FGfeSection), 
            nameof(s800U2FSettlementSection), nameof(s800U2FGfeSection), nameof(s800U3FSettlementSection), nameof(s800U3FGfeSection), nameof(s800U4FSettlementSection), 
            nameof(s800U4FGfeSection), nameof(s800U5FSettlementSection), nameof(s800U5FGfeSection), nameof(s904PiaSettlementSection), nameof(s904PiaGfeSection), 
            nameof(s900U1PiaSettlementSection), nameof(s900U1PiaGfeSection), nameof(sEscrowFSettlementSection), nameof(sEscrowFGfeSection), nameof(sDocPrepFSettlementSection), 
            nameof(sDocPrepFGfeSection), nameof(sNotaryFSettlementSection), nameof(sNotaryFGfeSection), nameof(sAttorneyFSettlementSection), nameof(sAttorneyFGfeSection), 
            nameof(sTitleInsFSettlementSection), nameof(sTitleInsFGfeSection), nameof(sU1TcSettlementSection), nameof(sU1TcGfeSection), nameof(sU2TcSettlementSection), 
            nameof(sU2TcGfeSection), nameof(sU3TcSettlementSection), nameof(sU3TcGfeSection), nameof(sU4TcSettlementSection), nameof(sU4TcGfeSection), 
            nameof(sU1GovRtcSettlementSection), nameof(sU1GovRtcGfeSection), nameof(sU2GovRtcSettlementSection), nameof(sU2GovRtcGfeSection), nameof(sU3GovRtcSettlementSection), nameof(sU3GovRtcGfeSection), nameof(sU1ScSettlementSection), 
            nameof(sU1ScGfeSection), nameof(sU2ScSettlementSection), nameof(sU2ScGfeSection), nameof(sU3ScSettlementSection), nameof(sU3ScGfeSection), nameof(sU4ScSettlementSection), 
            nameof(sU4ScGfeSection), nameof(sU5ScSettlementSection), nameof(sU5ScGfeSection), nameof(sSettlementU3RsrvProps), nameof(sU3RsrvProps), nameof(sSettlementU4RsrvProps), 
            nameof(sU4RsrvProps), nameof(sSettlementDiscountPointFProps), nameof(sGfeDiscountPointFProps), nameof(sSettlementLOrigFProps), nameof(sLOrigFProps), 
            nameof(sSettlementLDiscntProps), nameof(sLDiscntProps), nameof(sSettlementApprFProps), nameof(sApprFProps), nameof(sSettlementCrFProps), nameof(sCrFProps), 
            nameof(sSettlementTxServFProps), nameof(sTxServFProps), nameof(sSettlementFloodCertificationFProps), nameof(sFloodCertificationFProps), nameof(sSettlementInspectFProps), 
            nameof(sInspectFProps), nameof(sSettlementProcFProps), nameof(sProcFProps), nameof(sSettlementUwFProps), nameof(sUwFProps), nameof(sSettlementWireFProps), 
            nameof(sWireFProps), nameof(sSettlement800U1FProps), nameof(s800U1FProps), nameof(sSettlement800U2FProps), nameof(s800U2FProps), nameof(sSettlement800U3FProps), 
            nameof(s800U3FProps), nameof(sSettlement800U4FProps), nameof(s800U4FProps), nameof(sSettlement800U5FProps), nameof(s800U5FProps), nameof(sSettlementIPiaProps), 
            nameof(sIPiaProps), nameof(sSettlementHazInsPiaProps), nameof(sHazInsPiaProps), nameof(sSettlement904PiaProps), nameof(s904PiaProps), nameof(sSettlement900U1PiaProps), 
            nameof(s900U1PiaProps), nameof(sSettlementHazInsRsrvProps), nameof(sHazInsRsrvProps), nameof(sSettlementMInsRsrvProps), nameof(sMInsRsrvProps), nameof(sSettlementRealETxRsrvProps), 
            nameof(sRealETxRsrvProps), nameof(sSettlementSchoolTxRsrvProps), nameof(sSchoolTxRsrvProps), nameof(sSettlementFloodInsRsrvProps), nameof(sFloodInsRsrvProps), 
            nameof(sSettlementAggregateAdjRsrvProps), nameof(sAggregateAdjRsrvProps), nameof(sSettlement1008RsrvProps), nameof(s1006RsrvProps), nameof(sSettlement1009RsrvProps), 
            nameof(s1007RsrvProps), nameof(sSettlementEscrowFProps), nameof(sEscrowFProps), nameof(sSettlementOwnerTitleInsFProps), nameof(sOwnerTitleInsProps), 
            nameof(sSettlementTitleInsFProps), nameof(sTitleInsFProps), nameof(sSettlementDocPrepFProps), nameof(sDocPrepFProps), nameof(sSettlementNotaryFProps), 
            nameof(sNotaryFProps), nameof(sSettlementAttorneyFProps), nameof(sAttorneyFProps), nameof(sSettlementU1TcProps), nameof(sU1TcProps), nameof(sSettlementU2TcProps), 
            nameof(sU2TcProps), nameof(sSettlementU3TcProps), nameof(sU3TcProps), nameof(sSettlementU4TcProps), nameof(sU4TcProps), nameof(sSettlementRecFProps), nameof(sRecFProps), 
            nameof(sSettlementCountyRtcProps), nameof(sCountyRtcProps), nameof(sSettlementStateRtcProps), nameof(sStateRtcProps), nameof(sSettlementU1GovRtcProps), 
            nameof(sU1GovRtcProps), nameof(sSettlementU2GovRtcProps), nameof(sU2GovRtcProps), nameof(sSettlementU3GovRtcProps), nameof(sU3GovRtcProps), nameof(sSettlementPestInspectFProps), 
            nameof(sPestInspectFProps), nameof(sSettlementU1ScProps), nameof(sU1ScProps), nameof(sSettlementU2ScProps), nameof(sU2ScProps), nameof(sSettlementU4ScProps), 
            nameof(sU4ScProps), nameof(sSettlementU3ScProps), nameof(sU3ScProps), nameof(sSettlementU5ScProps), nameof(sU5ScProps), nameof(sSettlementMBrokFProps), nameof(sMBrokFProps), 
            nameof(sSettlement1008RsrvMon), nameof(s1006RsrvMon), nameof(sSettlement1009RsrvMon), nameof(s1007RsrvMon), nameof(sSettlementProcFPaid), nameof(sProcFPaid), 
            nameof(sSettlementCrFPaid), nameof(sCrFPaid), nameof(sSettlementApprFPaid), nameof(sApprFPaid), 
            nameof(sfExtractStaticGFEArchive), nameof(sSettlementRecFLckd))]
        #endregion
        private bool sfAssignSettlementChargeFieldsToGFE { get { return false; } }
        /// <summary>
        /// Assign all 800-1300 fields from the Settlement Charges page which are
        /// not already shared in the data-layer with the GFE to the GFE.
        /// </summary>
        private void AssignSettlementChargeFieldsToGFE()
        {
            #region Assign to GFE fields from SC fields
            sLOrigFPc = sSettlementLOrigFPc;
            sLOrigFMb = sSettlementLOrigFMb;
            sLDiscntPc = sSettlementLDiscntPc;
            sLDiscntBaseT = sSettlementLDiscntBaseT;
            sLDiscntFMb = sSettlementLDiscntFMb;
            sGfeCreditLenderPaidItemT = sSettlementCreditLenderPaidItemT;
            sApprF = sSettlementApprF;
            sCrF = sSettlementCrF;
            sTxServF = sSettlementTxServF;
            sFloodCertificationDeterminationT = sSettlementFloodCertificationDeterminationT;
            sFloodCertificationF = sSettlementFloodCertificationF;
            sMBrokFPc = sSettlementMBrokFPc;
            sMBrokFBaseT = sSettlementMBrokFBaseT;
            sMBrokFMb = sSettlementMBrokFMb;
            sInspectF = sSettlementInspectF;
            sProcF = sSettlementProcF;
            sUwF = sSettlementUwF;
            sWireF = sSettlementWireF;
            s800U1FDesc = sSettlement800U1FDesc;
            s800U1F = sSettlement800U1F;
            s800U2FDesc = sSettlement800U2FDesc;
            s800U2F = sSettlement800U2F;
            s800U3FDesc = sSettlement800U3FDesc;
            s800U3F = sSettlement800U3F;
            s800U4FDesc = sSettlement800U4FDesc;
            s800U4F = sSettlement800U4F;
            s800U5FDesc = sSettlement800U5FDesc;
            s800U5F = sSettlement800U5F;
            sIPiaDyLckd = sSettlementIPiaDyLckd;
            sIPiaDy = sSettlementIPiaDy;
            sIPerDayLckd = sSettlementIPerDayLckd;
            sIPerDay = sSettlementIPerDay;
            sHazInsPiaMon = sSettlementHazInsPiaMon;
            s904PiaDesc = sSettlement904PiaDesc;
            s904Pia = sSettlement904Pia;
            s900U1PiaDesc = sSettlement900U1PiaDesc;
            s900U1Pia = sSettlement900U1Pia;
            sHazInsRsrvMon = sSettlementHazInsRsrvMon;
            sMInsRsrvMon = sSettlementMInsRsrvMon;
            sRealETxRsrvMon = sSettlementRealETxRsrvMon;
            sSchoolTxRsrvMon = sSettlementSchoolTxRsrvMon;
            sFloodInsRsrvMon = sSettlementFloodInsRsrvMon;
            sAggregateAdjRsrvLckd = sSettlementAggregateAdjRsrvLckd;
            sAggregateAdjRsrv = sSettlementAggregateAdjRsrv;
            sU3RsrvMon = sSettlementU3RsrvMon;
            sU4RsrvMon = sSettlementU4RsrvMon;
            sEscrowF = sSettlementEscrowF;
            sOwnerTitleInsF = sSettlementOwnerTitleInsF;
            sTitleInsF = sSettlementTitleInsF;
            sDocPrepF = sSettlementDocPrepF;
            sNotaryF = sSettlementNotaryF;
            sAttorneyF = sSettlementAttorneyF;
            sU1TcDesc = sSettlementU1TcDesc;
            sU1Tc = sSettlementU1Tc;
            sU2TcDesc = sSettlementU2TcDesc;
            sU2Tc = sSettlementU2Tc;
            sU3TcDesc = sSettlementU3TcDesc;
            sU3Tc = sSettlementU3Tc;
            sU4TcDesc = sSettlementU4TcDesc;
            sU4Tc = sSettlementU4Tc;
            sRecFPc = sSettlementRecFPc;
            sRecBaseT = sSettlementRecBaseT;
            sRecFMb = sSettlementRecFMb;
            sRecDeed = sSettlementRecDeed;
            sRecMortgage = sSettlementRecMortgage;
            sRecRelease = sSettlementRecRelease;
            sRecFLckd = sSettlementRecFLckd;
            sCountyRtcPc = sSettlementCountyRtcPc;
            sCountyRtcBaseT = sSettlementCountyRtcBaseT;
            sCountyRtcMb = sSettlementCountyRtcMb;
            sStateRtcPc = sSettlementStateRtcPc;
            sStateRtcBaseT = sSettlementStateRtcBaseT;
            sStateRtcMb = sSettlementStateRtcMb;
            sU1GovRtcDesc = sSettlementU1GovRtcDesc;
            sU1GovRtcPc = sSettlementU1GovRtcPc;
            sU1GovRtcBaseT = sSettlementU1GovRtcBaseT;
            sU1GovRtcMb = sSettlementU1GovRtcMb;
            sU2GovRtcDesc = sSettlementU2GovRtcDesc;
            sU2GovRtcPc = sSettlementU2GovRtcPc;
            sU2GovRtcBaseT = sSettlementU2GovRtcBaseT;
            sU2GovRtcMb = sSettlementU2GovRtcMb;
            sU3GovRtcDesc = sSettlementU3GovRtcDesc;
            sU3GovRtcPc = sSettlementU3GovRtcPc;
            sU3GovRtcBaseT = sSettlementU3GovRtcBaseT;
            sU3GovRtcMb = sSettlementU3GovRtcMb;
            sPestInspectF = sSettlementPestInspectF;
            sU1ScDesc = sSettlementU1ScDesc;
            sU1Sc = sSettlementU1Sc;
            sU2ScDesc = sSettlementU2ScDesc;
            sU2Sc = sSettlementU2Sc;
            sU3ScDesc = sSettlementU3ScDesc;
            sU3Sc = sSettlementU3Sc;
            sU4ScDesc = sSettlementU4ScDesc;
            sU4Sc = sSettlementU4Sc;
            sU5ScDesc = sSettlementU5ScDesc;
            sU5Sc = sSettlementU5Sc;
            s800U1FGfeSection = s800U1FSettlementSection;
            s800U2FGfeSection = s800U2FSettlementSection;
            s800U3FGfeSection = s800U3FSettlementSection;
            s800U4FGfeSection = s800U4FSettlementSection;
            s800U5FGfeSection = s800U5FSettlementSection;
            s904PiaGfeSection = s904PiaSettlementSection;
            s900U1PiaGfeSection = s900U1PiaSettlementSection;
            sEscrowFGfeSection = sEscrowFSettlementSection;
            sDocPrepFGfeSection = sDocPrepFSettlementSection;
            sNotaryFGfeSection = sNotaryFSettlementSection;
            sAttorneyFGfeSection = sAttorneyFSettlementSection;
            sTitleInsFGfeSection = sTitleInsFSettlementSection;
            sU1TcGfeSection = sU1TcSettlementSection;
            sU2TcGfeSection = sU2TcSettlementSection;
            sU3TcGfeSection = sU3TcSettlementSection;
            sU4TcGfeSection = sU4TcSettlementSection;
            sU1GovRtcGfeSection = sU1GovRtcSettlementSection;
            sU2GovRtcGfeSection = sU2GovRtcSettlementSection;
            sU3GovRtcGfeSection = sU3GovRtcSettlementSection;
            sU1ScGfeSection = sU1ScSettlementSection;
            sU2ScGfeSection = sU2ScSettlementSection;
            sU3ScGfeSection = sU3ScSettlementSection;
            sU4ScGfeSection = sU4ScSettlementSection;
            sU5ScGfeSection = sU5ScSettlementSection;
            sU3RsrvProps = GenerateGFEPropsFromSettlementProps(sU3RsrvProps, sSettlementU3RsrvProps);
            sU4RsrvProps = GenerateGFEPropsFromSettlementProps(sU4RsrvProps, sSettlementU4RsrvProps);
            sGfeDiscountPointFProps = GenerateGFEPropsFromSettlementProps(sGfeDiscountPointFProps, sSettlementDiscountPointFProps);
            sLOrigFProps = GenerateGFEPropsFromSettlementProps(sLOrigFProps, sSettlementLOrigFProps);
            sLDiscntProps = GenerateGFEPropsFromSettlementProps(sLDiscntProps, sSettlementLDiscntProps);
            sApprFProps = GenerateGFEPropsFromSettlementProps(sApprFProps, sSettlementApprFProps);
            sCrFProps = GenerateGFEPropsFromSettlementProps(sCrFProps, sSettlementCrFProps);
            sTxServFProps = GenerateGFEPropsFromSettlementProps(sTxServFProps, sSettlementTxServFProps);
            sFloodCertificationFProps = GenerateGFEPropsFromSettlementProps(sFloodCertificationFProps, sSettlementFloodCertificationFProps);
            sInspectFProps = GenerateGFEPropsFromSettlementProps(sInspectFProps, sSettlementInspectFProps);
            sProcFProps = GenerateGFEPropsFromSettlementProps(sProcFProps, sSettlementProcFProps);
            sUwFProps = GenerateGFEPropsFromSettlementProps(sUwFProps, sSettlementUwFProps);
            sWireFProps = GenerateGFEPropsFromSettlementProps(sWireFProps, sSettlementWireFProps);
            s800U1FProps = GenerateGFEPropsFromSettlementProps(s800U1FProps, sSettlement800U1FProps);
            s800U2FProps = GenerateGFEPropsFromSettlementProps(s800U2FProps, sSettlement800U2FProps);
            s800U3FProps = GenerateGFEPropsFromSettlementProps(s800U3FProps, sSettlement800U3FProps);
            s800U4FProps = GenerateGFEPropsFromSettlementProps(s800U4FProps, sSettlement800U4FProps);
            s800U5FProps = GenerateGFEPropsFromSettlementProps(s800U5FProps, sSettlement800U5FProps);
            sIPiaProps = GenerateGFEPropsFromSettlementProps(sIPiaProps, sSettlementIPiaProps);
            sHazInsPiaProps = GenerateGFEPropsFromSettlementProps(sHazInsPiaProps, sSettlementHazInsPiaProps);
            s904PiaProps = GenerateGFEPropsFromSettlementProps(s904PiaProps, sSettlement904PiaProps);
            s900U1PiaProps = GenerateGFEPropsFromSettlementProps(s900U1PiaProps, sSettlement900U1PiaProps);
            sHazInsRsrvProps = GenerateGFEPropsFromSettlementProps(sHazInsRsrvProps, sSettlementHazInsRsrvProps);
            sMInsRsrvProps = GenerateGFEPropsFromSettlementProps(sMInsRsrvProps, sSettlementMInsRsrvProps);
            sRealETxRsrvProps = GenerateGFEPropsFromSettlementProps(sRealETxRsrvProps, sSettlementRealETxRsrvProps);
            sSchoolTxRsrvProps = GenerateGFEPropsFromSettlementProps(sSchoolTxRsrvProps, sSettlementSchoolTxRsrvProps);
            sFloodInsRsrvProps = GenerateGFEPropsFromSettlementProps(sFloodInsRsrvProps, sSettlementFloodInsRsrvProps);
            sAggregateAdjRsrvProps = GenerateGFEPropsFromSettlementProps(sAggregateAdjRsrvProps, sSettlementAggregateAdjRsrvProps);
            s1006RsrvProps = GenerateGFEPropsFromSettlementProps(s1006RsrvProps, sSettlement1008RsrvProps);
            s1007RsrvProps = GenerateGFEPropsFromSettlementProps(s1007RsrvProps, sSettlement1009RsrvProps);
            sEscrowFProps = GenerateGFEPropsFromSettlementProps(sEscrowFProps, sSettlementEscrowFProps);
            sOwnerTitleInsProps = GenerateGFEPropsFromSettlementProps(sOwnerTitleInsProps, sSettlementOwnerTitleInsFProps);
            sTitleInsFProps = GenerateGFEPropsFromSettlementProps(sTitleInsFProps, sSettlementTitleInsFProps);
            sDocPrepFProps = GenerateGFEPropsFromSettlementProps(sDocPrepFProps, sSettlementDocPrepFProps);
            sNotaryFProps = GenerateGFEPropsFromSettlementProps(sNotaryFProps, sSettlementNotaryFProps);
            sAttorneyFProps = GenerateGFEPropsFromSettlementProps(sAttorneyFProps, sSettlementAttorneyFProps);
            sU1TcProps = GenerateGFEPropsFromSettlementProps(sU1TcProps, sSettlementU1TcProps);
            sU2TcProps = GenerateGFEPropsFromSettlementProps(sU2TcProps, sSettlementU2TcProps);
            sU3TcProps = GenerateGFEPropsFromSettlementProps(sU3TcProps, sSettlementU3TcProps);
            sU4TcProps = GenerateGFEPropsFromSettlementProps(sU4TcProps, sSettlementU4TcProps);
            sRecFProps = GenerateGFEPropsFromSettlementProps(sRecFProps, sSettlementRecFProps);
            sCountyRtcProps = GenerateGFEPropsFromSettlementProps(sCountyRtcProps, sSettlementCountyRtcProps);
            sStateRtcProps = GenerateGFEPropsFromSettlementProps(sStateRtcProps, sSettlementStateRtcProps);
            sU1GovRtcProps = GenerateGFEPropsFromSettlementProps(sU1GovRtcProps, sSettlementU1GovRtcProps);
            sU2GovRtcProps = GenerateGFEPropsFromSettlementProps(sU2GovRtcProps, sSettlementU2GovRtcProps);
            sU3GovRtcProps = GenerateGFEPropsFromSettlementProps(sU3GovRtcProps, sSettlementU3GovRtcProps);
            sPestInspectFProps = GenerateGFEPropsFromSettlementProps(sPestInspectFProps, sSettlementPestInspectFProps);
            sU1ScProps = GenerateGFEPropsFromSettlementProps(sU1ScProps, sSettlementU1ScProps);
            sU2ScProps = GenerateGFEPropsFromSettlementProps(sU2ScProps, sSettlementU2ScProps);
            sU4ScProps = GenerateGFEPropsFromSettlementProps(sU4ScProps, sSettlementU4ScProps);
            sU3ScProps = GenerateGFEPropsFromSettlementProps(sU3ScProps, sSettlementU3ScProps);
            sU5ScProps = GenerateGFEPropsFromSettlementProps(sU5ScProps, sSettlementU5ScProps);
            sMBrokFProps = GenerateGFEPropsFromSettlementProps(sMBrokFProps, sSettlementMBrokFProps);
            s1006RsrvMon = sSettlement1008RsrvMon;
            s1007RsrvMon = sSettlement1009RsrvMon;
            sProcFPaid = sSettlementProcFPaid;
            sCrFPaid = sSettlementCrFPaid;
            sApprFPaid = sSettlementApprFPaid;
            #endregion Assign to GFE fields from SC fields
        }

        private void WriteClosingCostMigrationArchiveToFileDB(ClosingCostMigrationArchive migrationArchive)
        {
            if (migrationArchive == null)
            {
                throw new GenericUserErrorMessageException("Programming Error: Should not try to save null closing cost migration archive.");
            }

            var serializedArchive = ObsoleteSerializationHelper.JavascriptJsonSerialize(migrationArchive);
            FileDBTools.WriteData(E_FileDB.Normal, x_ClosingCostMigrationBackupKey, serializedArchive);
        }

        /// <summary>
        /// Load the migration archive from FileDB.
        /// </summary>
        /// <returns>The migration archive.</returns>
        private ClosingCostMigrationArchive RetrieveClosingCostMigrationArchive()
        {
            string serializedArchive = null;
            try
            {
                serializedArchive = FileDBTools.ReadDataText(E_FileDB.Normal, x_ClosingCostMigrationBackupKey);
            }
            catch (FileNotFoundException)
            {
                return null;
            }
            return ObsoleteSerializationHelper.JavascriptJsonDeserializer<ClosingCostMigrationArchive>(serializedArchive);
        }

        [DependsOn(nameof(sfRestoreGFEFieldsFromMigrationArchive), nameof(sUseGFEDataForSCFields))]
        private bool sfRestoreGFEFromSettlementChargeGFEMigrationArchive { get { return false; } }
        /// <summary>
        /// Point SC fields back to SC data. If the settlement charge fields were copied to 
        /// the GFE as part of the migration, restore the GFE fields to their original state.
        /// Removes any associated GFE Archives.
        /// </summary>
        public void RestoreClosingCostDataFromMigrationArchive()
        {
            if (!sUseGFEDataForSCFields)
            {
                throw new GenericUserErrorMessageException("Programming Error: Should not call RestoreClosingCostDataFromMigrationArchive when loan is not using GFE data for SC fields.");
            }

            sUseGFEDataForSCFields = false;

            // Can't use sClosingCostMigrationArchive property if sUseGFEDataForSCFields is false.
            var closingCostMigrationArchive = RetrieveClosingCostMigrationArchive();
            RestoreGFEFieldsFromMigrationArchive(closingCostMigrationArchive.GFEArchive);

            DeleteClosingCostMigrationGFEArchive();
        }

        /// <summary>
        /// If there is a GFE Archive on the file which was the result of the migration, delete it.
        /// </summary>
        private void DeleteClosingCostMigrationGFEArchive()
        {
            if (!BrokerDB.IsGFEandCoCVersioningEnabled)
            {
                throw new GenericUserErrorMessageException("Programming Error: Should not call SaveGFEArchivesToFileDBIfArchived when IsGFEandCoCVersioningEnabled is false");
            }

            if (GFEArchives.Any(gfea => gfea.IsClosingCostMigrationArchive))
            {
                var gfeArchives = GFEArchives.Where(gfea => !gfea.IsClosingCostMigrationArchive);
                m_GFEArchives = gfeArchives;

                // We only need to update the last disclosed GFE archive date when there
                // are no longer any GFE archives on file.
                // Reasoning: If there was an archive on file when the migration was run,
                // then we do not create an additional GFE archive.
                // If we added a GFE archive to the file as part of the migration, it will
                // ALWAYS be the first GFE archive (unless it was deleted).
                // If there are any GFE archives added after the migration, the last disclosed
                // archive date will reflect the change.
                if (m_GFEArchives.Count() == 0)
                {
                    // If there are no archives, set the last disclosed GFE archive value to null.
                    sLastDisclosedGFEArchiveD = DateTime.MinValue;
                }

                Tools.LogInfo("Removing GFE Archive from Closing Cost Migration for sLId " + sLId + ". Old GFE Archives content: \r\n" + m_oldGFEArchives);

                var js = new System.Web.Script.Serialization.JavaScriptSerializer();

                string serializedArchives = js.Serialize(gfeArchives);

                FileDBTools.WriteData(E_FileDB.Normal, x_GFEArchiveKey, serializedArchives);
            }
        }

        #region Restore GFE Fields From Migration Dependencies
        [DependsOn(nameof(sLOrigFPc), nameof(sLOrigFMb), nameof(sLDiscntPc), nameof(sLDiscntBaseT), nameof(sLDiscntFMb), nameof(sGfeCreditLenderPaidItemT), nameof(sApprF), nameof(sCrF), nameof(sTxServF), 
            nameof(sFloodCertificationDeterminationT), nameof(sFloodCertificationF), nameof(sMBrokFPc), nameof(sMBrokFBaseT), nameof(sMBrokFMb), nameof(sInspectF), nameof(sProcF), 
            nameof(sUwF), nameof(sWireF), nameof(s800U1FDesc), nameof(s800U1F), nameof(s800U2FDesc), nameof(s800U2F), nameof(s800U3FDesc), nameof(s800U3F), nameof(s800U4FDesc), nameof(s800U4F), nameof(s800U5FDesc), 
            nameof(s800U5F), nameof(sIPiaDyLckd), nameof(sIPiaDy), nameof(sIPerDayLckd), nameof(sIPerDay), nameof(sHazInsPiaMon), nameof(s904PiaDesc), nameof(s904Pia), nameof(s900U1PiaDesc), nameof(s900U1Pia), 
            nameof(sHazInsRsrvMon), nameof(sMInsRsrvMon), nameof(sRealETxRsrvMon), nameof(sSchoolTxRsrvMon), nameof(sFloodInsRsrvMon), nameof(sAggregateAdjRsrvLckd), nameof(sAggregateAdjRsrv), 
            nameof(sU3RsrvMon), nameof(sU4RsrvMon), nameof(sEscrowF), nameof(sOwnerTitleInsF), nameof(sTitleInsF), nameof(sDocPrepF), nameof(sNotaryF), nameof(sAttorneyF), nameof(sU1TcDesc), nameof(sU1Tc), 
            nameof(sU2TcDesc), nameof(sU2Tc), nameof(sU3TcDesc), nameof(sU3Tc), nameof(sU4TcDesc), nameof(sU4Tc), nameof(sRecFPc), nameof(sRecBaseT), nameof(sRecFMb), nameof(sRecDeed), nameof(sRecMortgage), nameof(sRecRelease), nameof(sRecFLckd), nameof(sCountyRtcPc), nameof(sCountyRtcBaseT), 
            nameof(sCountyRtcMb), nameof(sStateRtcPc), nameof(sStateRtcBaseT), nameof(sStateRtcMb), nameof(sU1GovRtcDesc), nameof(sU1GovRtcPc), nameof(sU1GovRtcBaseT), nameof(sU1GovRtcMb), 
            nameof(sU2GovRtcDesc), nameof(sU2GovRtcPc), nameof(sU2GovRtcBaseT), nameof(sU2GovRtcMb), nameof(sU3GovRtcDesc), nameof(sU3GovRtcPc), nameof(sU3GovRtcBaseT), nameof(sU3GovRtcMb), 
            nameof(sPestInspectF), nameof(sU1ScDesc), nameof(sU1Sc), nameof(sU2ScDesc), nameof(sU2Sc), nameof(sU3ScDesc), nameof(sU3Sc), nameof(sU4ScDesc), nameof(sU4Sc), nameof(sU5ScDesc), nameof(sU5Sc), nameof(s800U1FGfeSection), 
            nameof(s800U2FGfeSection), nameof(s800U3FGfeSection), nameof(s800U4FGfeSection), nameof(s800U5FGfeSection), nameof(s904PiaGfeSection), nameof(s900U1PiaGfeSection), 
            nameof(sEscrowFGfeSection), nameof(sDocPrepFGfeSection), nameof(sNotaryFGfeSection), nameof(sAttorneyFGfeSection), nameof(sU1TcGfeSection), nameof(sU2TcGfeSection), 
            nameof(sU3TcGfeSection), nameof(sU4TcGfeSection), nameof(sU1GovRtcGfeSection), nameof(sU2GovRtcGfeSection), nameof(sU3GovRtcGfeSection), nameof(sU1ScGfeSection), nameof(sU2ScGfeSection), nameof(sU3ScGfeSection), nameof(sU4ScGfeSection), nameof(sU5ScGfeSection), 
            nameof(sU3RsrvProps), nameof(sU4RsrvProps), nameof(sGfeDiscountPointFProps), nameof(sLOrigFProps), nameof(sApprFProps), nameof(sCrFProps), nameof(sTxServFProps), nameof(sFloodCertificationFProps), 
            nameof(sInspectFProps), nameof(sProcFProps), nameof(sUwFProps), nameof(sWireFProps), nameof(s800U1FProps), nameof(s800U2FProps), nameof(s800U3FProps), nameof(s800U4FProps), nameof(s800U5FProps), 
            nameof(sIPiaProps), nameof(sHazInsPiaProps), nameof(s904PiaProps), nameof(s900U1PiaProps), nameof(sHazInsRsrvProps), nameof(sMInsRsrvProps), nameof(sRealETxRsrvProps), 
            nameof(sSchoolTxRsrvProps), nameof(sFloodInsRsrvProps), nameof(sAggregateAdjRsrvProps), nameof(s1006RsrvProps), nameof(s1007RsrvProps), nameof(sEscrowFProps), 
            nameof(sOwnerTitleInsProps), nameof(sTitleInsFProps), nameof(sDocPrepFProps), nameof(sNotaryFProps), nameof(sAttorneyFProps), nameof(sU1TcProps), nameof(sU2TcProps), 
            nameof(sU3TcProps), nameof(sU4TcProps), nameof(sRecFProps), nameof(sCountyRtcProps), nameof(sStateRtcProps), nameof(sU1GovRtcProps), nameof(sU2GovRtcProps), nameof(sU3GovRtcProps), 
            nameof(sPestInspectFProps), nameof(sU1ScProps), nameof(sU2ScProps), nameof(sU4ScProps), nameof(sU3ScProps), nameof(sU5ScProps), nameof(sMBrokFProps), nameof(s1006RsrvMon), nameof(s1007RsrvMon), 
            nameof(sProcFPaid), nameof(sCrFPaid), nameof(sApprFPaid), 
            nameof(s1007ProHExp), nameof(s1007ProHExpDesc), nameof(s1006ProHExp), nameof(s1006ProHExpDesc), nameof(sProU3Rsrv), nameof(sU3RsrvDesc), nameof(sProU4Rsrv), nameof(sU4RsrvDesc), 
            nameof(sProHazInsT), nameof(sProHazInsR), nameof(sProHazInsMb), nameof(sProFloodIns), nameof(sProRealETxMb), nameof(sProRealETxT), nameof(sProRealETxR), nameof(sProSchoolTx), 
            nameof(sMipPiaProps), nameof(sVaFfProps), nameof(sGfeOriginatorCompFProps), nameof(sGfeIsTPOTransaction), nameof(sTitleInsFTable), nameof(sEscrowFTable), nameof(sStateRtcDesc), 
            nameof(sCountyRtcDesc), nameof(sRecFDesc), nameof(sApprFPaidTo), nameof(sCrFPaidTo), nameof(sTxServFPaidTo), nameof(sFloodCertificationFPaidTo), nameof(sInspectFPaidTo), 
            nameof(sProcFPaidTo), nameof(sUwFPaidTo), nameof(sWireFPaidTo), nameof(s800U1FPaidTo), nameof(s800U2FPaidTo), nameof(s800U3FPaidTo), nameof(s800U4FPaidTo), nameof(s800U5FPaidTo), 
            nameof(sOwnerTitleInsPaidTo), nameof(sDocPrepFPaidTo), nameof(sNotaryFPaidTo), nameof(sAttorneyFPaidTo), nameof(sU1TcPaidTo), nameof(sU2TcPaidTo), nameof(sU3TcPaidTo), 
            nameof(sU4TcPaidTo), nameof(sU1GovRtcPaidTo), nameof(sU2GovRtcPaidTo), nameof(sU3GovRtcPaidTo), nameof(sPestInspectPaidTo), nameof(sU1ScPaidTo), nameof(sU2ScPaidTo), 
            nameof(sU3ScPaidTo), nameof(sU4ScPaidTo), nameof(sU5ScPaidTo), nameof(sHazInsPiaPaidTo), nameof(sMipPiaPaidTo), nameof(sVaFfPaidTo), 
            nameof(sHazInsRsrvMonLckd), nameof(sMInsRsrvMonLckd), nameof(sRealETxRsrvMonLckd), nameof(sSchoolTxRsrvMonLckd), nameof(sFloodInsRsrvMonLckd), 
            nameof(s1006RsrvMonLckd), nameof(s1007RsrvMonLckd), nameof(sU3RsrvMonLckd), nameof(sU4RsrvMonLckd))]
        #endregion
        private bool sfRestoreGFEFieldsFromMigrationArchive { get { return false; } }
        /// <summary>
        /// Assign the 800-1300 fields from the archive back to the GFE fields.
        /// This will restore fields which were not necessarily overridden by the 
        /// Settlement Charges version (fields which were already shared in the data-layer).
        /// The idea is that the fees are being restored to their pre-migration state.
        /// </summary>
        /// <param name="gfeArchive"></param>
        private void RestoreGFEFieldsFromMigrationArchive(GFEArchive gfeArchive)
        {
            #region Reassign fields from the Archive => GFE
            sLOrigFPc_rep = gfeArchive.sLOrigFPc_rep;
            sLOrigFMb_rep = gfeArchive.sLOrigFMb_rep;
            sLDiscntPc_rep = gfeArchive.sLDiscntPc_rep;
            sLDiscntBaseT = gfeArchive.sLDiscntBaseT;
            sLDiscntFMb_rep = gfeArchive.sLDiscntFMb_rep;
            sGfeCreditLenderPaidItemT = gfeArchive.sGfeCreditLenderPaidItemT;
            sApprF_rep = gfeArchive.sApprF_rep;
            sCrF_rep = gfeArchive.sCrF_rep;
            sTxServF_rep = gfeArchive.sTxServF_rep;
            sFloodCertificationDeterminationT = gfeArchive.sFloodCertificationDeterminationT;
            sFloodCertificationF_rep = gfeArchive.sFloodCertificationF_rep;
            sMBrokFPc_rep = gfeArchive.sMBrokFPc_rep;
            sMBrokFBaseT = gfeArchive.sMBrokFBaseT;
            sMBrokFMb_rep = gfeArchive.sMBrokFMb_rep;
            sInspectF_rep = gfeArchive.sInspectF_rep;
            sProcF_rep = gfeArchive.sProcF_rep;
            sUwF_rep = gfeArchive.sUwF_rep;
            sWireF_rep = gfeArchive.sWireF_rep;
            s800U1FDesc = gfeArchive.s800U1FDesc;
            s800U1F_rep = gfeArchive.s800U1F_rep;
            s800U2FDesc = gfeArchive.s800U2FDesc;
            s800U2F_rep = gfeArchive.s800U2F_rep;
            s800U3FDesc = gfeArchive.s800U3FDesc;
            s800U3F_rep = gfeArchive.s800U3F_rep;
            s800U4FDesc = gfeArchive.s800U4FDesc;
            s800U4F_rep = gfeArchive.s800U4F_rep;
            s800U5FDesc = gfeArchive.s800U5FDesc;
            s800U5F_rep = gfeArchive.s800U5F_rep;
            sIPiaDyLckd = gfeArchive.sIPiaDyLckd;
            sIPiaDy_rep = gfeArchive.sIPiaDy_rep;
            sIPerDayLckd = gfeArchive.sIPerDayLckd;
            sIPerDay_rep = gfeArchive.sIPerDay_rep;
            sHazInsPiaMon_rep = gfeArchive.sHazInsPiaMon_rep;
            s904PiaDesc = gfeArchive.s904PiaDesc;
            s904Pia_rep = gfeArchive.s904Pia_rep;
            s900U1PiaDesc = gfeArchive.s900U1PiaDesc;
            s900U1Pia_rep = gfeArchive.s900U1Pia_rep;
            sHazInsRsrvMonLckd = gfeArchive.sHazInsRsrvMonLckd; // start opm 180983
            sMInsRsrvMonLckd = gfeArchive.sMInsRsrvMonLckd;
            sRealETxRsrvMonLckd = gfeArchive.sRealETxRsrvMonLckd;
            sSchoolTxRsrvMonLckd = gfeArchive.sSchoolTxRsrvMonLckd;
            sFloodInsRsrvMonLckd = gfeArchive.sFloodInsRsrvMonLckd;
            s1006RsrvMonLckd = gfeArchive.s1006RsrvMonLckd;
            s1007RsrvMonLckd = gfeArchive.s1007RsrvMonLckd; 
            sU3RsrvMonLckd = gfeArchive.sU3RsrvMonLckd;
            sU4RsrvMonLckd = gfeArchive.sU4RsrvMonLckd; // end opm 180983
            sHazInsRsrvMon_rep = gfeArchive.sHazInsRsrvMon_rep;
            sMInsRsrvMon_rep = gfeArchive.sMInsRsrvMon_rep;
            sRealETxRsrvMon_rep = gfeArchive.sRealETxRsrvMon_rep;
            sSchoolTxRsrvMon_rep = gfeArchive.sSchoolTxRsrvMon_rep;
            sFloodInsRsrvMon_rep = gfeArchive.sFloodInsRsrvMon_rep;
            sAggregateAdjRsrvLckd = gfeArchive.sAggregateAdjRsrvLckd;
            sAggregateAdjRsrv_rep = gfeArchive.sAggregateAdjRsrv_rep;
            sU3RsrvMon_rep = gfeArchive.sU3RsrvMon_rep;
            sU4RsrvMon_rep = gfeArchive.sU4RsrvMon_rep;
            sEscrowF_rep = gfeArchive.sEscrowF_rep;
            sOwnerTitleInsF_rep = gfeArchive.sOwnerTitleInsF_rep;
            sTitleInsF_rep = gfeArchive.sTitleInsF_rep;
            sDocPrepF_rep = gfeArchive.sDocPrepF_rep;
            sNotaryF_rep = gfeArchive.sNotaryF_rep;
            sAttorneyF_rep = gfeArchive.sAttorneyF_rep;
            sU1TcDesc = gfeArchive.sU1TcDesc;
            sU1Tc_rep = gfeArchive.sU1Tc_rep;
            sU2TcDesc = gfeArchive.sU2TcDesc;
            sU2Tc_rep = gfeArchive.sU2Tc_rep;
            sU3TcDesc = gfeArchive.sU3TcDesc;
            sU3Tc_rep = gfeArchive.sU3Tc_rep;
            sU4TcDesc = gfeArchive.sU4TcDesc;
            sU4Tc_rep = gfeArchive.sU4Tc_rep;
            sRecFPc_rep = gfeArchive.sRecFPc_rep;
            sRecBaseT = gfeArchive.sRecBaseT;
            sRecFMb_rep = gfeArchive.sRecFMb_rep;
            sRecDeed_rep = gfeArchive.sRecDeed_rep;
            sRecMortgage_rep = gfeArchive.sRecMortgage_rep;
            sRecRelease_rep = gfeArchive.sRecRelease_rep;
            sRecFLckd = gfeArchive.sRecFLckd;
            sCountyRtcPc_rep = gfeArchive.sCountyRtcPc_rep;
            sCountyRtcBaseT = gfeArchive.sCountyRtcBaseT;
            sCountyRtcMb_rep = gfeArchive.sCountyRtcMb_rep;
            sStateRtcPc_rep = gfeArchive.sStateRtcPc_rep;
            sStateRtcBaseT = gfeArchive.sStateRtcBaseT;
            sStateRtcMb_rep = gfeArchive.sStateRtcMb_rep;
            sU1GovRtcDesc = gfeArchive.sU1GovRtcDesc;
            sU1GovRtcPc_rep = gfeArchive.sU1GovRtcPc_rep;
            sU1GovRtcBaseT = gfeArchive.sU1GovRtcBaseT;
            sU1GovRtcMb_rep = gfeArchive.sU1GovRtcMb_rep;
            sU2GovRtcDesc = gfeArchive.sU2GovRtcDesc;
            sU2GovRtcPc_rep = gfeArchive.sU2GovRtcPc_rep;
            sU2GovRtcBaseT = gfeArchive.sU2GovRtcBaseT;
            sU2GovRtcMb_rep = gfeArchive.sU2GovRtcMb_rep;
            sU3GovRtcDesc = gfeArchive.sU3GovRtcDesc;
            sU3GovRtcPc_rep = gfeArchive.sU3GovRtcPc_rep;
            sU3GovRtcBaseT = gfeArchive.sU3GovRtcBaseT;
            sU3GovRtcMb_rep = gfeArchive.sU3GovRtcMb_rep;
            sPestInspectF_rep = gfeArchive.sPestInspectF_rep;
            sU1ScDesc = gfeArchive.sU1ScDesc;
            sU1Sc_rep = gfeArchive.sU1Sc_rep;
            sU2ScDesc = gfeArchive.sU2ScDesc;
            sU2Sc_rep = gfeArchive.sU2Sc_rep;
            sU3ScDesc = gfeArchive.sU3ScDesc;
            sU3Sc_rep = gfeArchive.sU3Sc_rep;
            sU4ScDesc = gfeArchive.sU4ScDesc;
            sU4Sc_rep = gfeArchive.sU4Sc_rep;
            sU5ScDesc = gfeArchive.sU5ScDesc;
            sU5Sc_rep = gfeArchive.sU5Sc_rep;
            s800U1FGfeSection = gfeArchive.s800U1FGfeSection;
            s800U2FGfeSection = gfeArchive.s800U2FGfeSection;
            s800U3FGfeSection = gfeArchive.s800U3FGfeSection;
            s800U4FGfeSection = gfeArchive.s800U4FGfeSection;
            s800U5FGfeSection = gfeArchive.s800U5FGfeSection;
            s904PiaGfeSection = gfeArchive.s904PiaGfeSection;
            s900U1PiaGfeSection = gfeArchive.s900U1PiaGfeSection;
            sEscrowFGfeSection = gfeArchive.sEscrowFGfeSection;
            sDocPrepFGfeSection = gfeArchive.sDocPrepFGfeSection;
            sNotaryFGfeSection = gfeArchive.sNotaryFGfeSection;
            sAttorneyFGfeSection = gfeArchive.sAttorneyFGfeSection;
            //sTitleInsFGfeSection = gfeArchive.sTitleInsFGfeSection; DEPRECATED.
            sU1TcGfeSection = gfeArchive.sU1TcGfeSection;
            sU2TcGfeSection = gfeArchive.sU2TcGfeSection;
            sU3TcGfeSection = gfeArchive.sU3TcGfeSection;
            sU4TcGfeSection = gfeArchive.sU4TcGfeSection;
            sU1GovRtcGfeSection = gfeArchive.sU1GovRtcGfeSection;
            sU2GovRtcGfeSection = gfeArchive.sU2GovRtcGfeSection;
            sU3GovRtcGfeSection = gfeArchive.sU3GovRtcGfeSection;
            sU1ScGfeSection = gfeArchive.sU1ScGfeSection;
            sU2ScGfeSection = gfeArchive.sU2ScGfeSection;
            sU3ScGfeSection = gfeArchive.sU3ScGfeSection;
            sU4ScGfeSection = gfeArchive.sU4ScGfeSection;
            sU5ScGfeSection = gfeArchive.sU5ScGfeSection;
            sU3RsrvProps = gfeArchive.sU3RsrvProps;
            sU4RsrvProps = gfeArchive.sU4RsrvProps;
            sGfeDiscountPointFProps = gfeArchive.sGfeDiscountPointFProps;
            sLOrigFProps = gfeArchive.sLOrigFProps;
            //sLDiscntProps = gfeArchive.sLDiscntProps; DEPRECATED.
            sApprFProps = gfeArchive.sApprFProps;
            sCrFProps = gfeArchive.sCrFProps;
            sTxServFProps = gfeArchive.sTxServFProps;
            sFloodCertificationFProps = gfeArchive.sFloodCertificationFProps;
            sInspectFProps = gfeArchive.sInspectFProps;
            sProcFProps = gfeArchive.sProcFProps;
            sUwFProps = gfeArchive.sUwFProps;
            sWireFProps = gfeArchive.sWireFProps;
            s800U1FProps = gfeArchive.s800U1FProps;
            s800U2FProps = gfeArchive.s800U2FProps;
            s800U3FProps = gfeArchive.s800U3FProps;
            s800U4FProps = gfeArchive.s800U4FProps;
            s800U5FProps = gfeArchive.s800U5FProps;
            sIPiaProps = gfeArchive.sIPiaProps;
            sHazInsPiaProps = gfeArchive.sHazInsPiaProps;
            s904PiaProps = gfeArchive.s904PiaProps;
            s900U1PiaProps = gfeArchive.s900U1PiaProps;
            sHazInsRsrvProps = gfeArchive.sHazInsRsrvProps;
            sMInsRsrvProps = gfeArchive.sMInsRsrvProps;
            sRealETxRsrvProps = gfeArchive.sRealETxRsrvProps;
            sSchoolTxRsrvProps = gfeArchive.sSchoolTxRsrvProps;
            sFloodInsRsrvProps = gfeArchive.sFloodInsRsrvProps;
            sAggregateAdjRsrvProps = gfeArchive.sAggregateAdjRsrvProps;
            s1006RsrvProps = gfeArchive.s1006RsrvProps;
            s1007RsrvProps = gfeArchive.s1007RsrvProps;
            sEscrowFProps = gfeArchive.sEscrowFProps;
            sOwnerTitleInsProps = gfeArchive.sOwnerTitleInsProps;
            sTitleInsFProps = gfeArchive.sTitleInsFProps;
            sDocPrepFProps = gfeArchive.sDocPrepFProps;
            sNotaryFProps = gfeArchive.sNotaryFProps;
            sAttorneyFProps = gfeArchive.sAttorneyFProps;
            sU1TcProps = gfeArchive.sU1TcProps;
            sU2TcProps = gfeArchive.sU2TcProps;
            sU3TcProps = gfeArchive.sU3TcProps;
            sU4TcProps = gfeArchive.sU4TcProps;
            sRecFProps = gfeArchive.sRecFProps;
            sCountyRtcProps = gfeArchive.sCountyRtcProps;
            sStateRtcProps = gfeArchive.sStateRtcProps;
            sU1GovRtcProps = gfeArchive.sU1GovRtcProps;
            sU2GovRtcProps = gfeArchive.sU2GovRtcProps;
            sU3GovRtcProps = gfeArchive.sU3GovRtcProps;
            sPestInspectFProps = gfeArchive.sPestInspectFProps;
            sU1ScProps = gfeArchive.sU1ScProps;
            sU2ScProps = gfeArchive.sU2ScProps;
            sU4ScProps = gfeArchive.sU4ScProps;
            sU3ScProps = gfeArchive.sU3ScProps;
            sU5ScProps = gfeArchive.sU5ScProps;
            sMBrokFProps = gfeArchive.sMBrokFProps;
            s1006RsrvMon_rep = gfeArchive.s1006RsrvMon_rep;
            s1007RsrvMon_rep = gfeArchive.s1007RsrvMon_rep;
            sProcFPaid = gfeArchive.sProcFPaid;
            sCrFPaid = gfeArchive.sCrFPaid;
            sApprFPaid = gfeArchive.sApprFPaid;

            #region Fields that are shared in data-layer, but should still be restored.
            s1007ProHExp_rep = gfeArchive.s1007ProHExp_rep;
            s1007ProHExpDesc = gfeArchive.s1007ProHExpDesc;
            s1006ProHExp_rep = gfeArchive.s1006ProHExp_rep;
            s1006ProHExpDesc = gfeArchive.s1006ProHExpDesc;
            sProU3Rsrv_rep = gfeArchive.sProU3Rsrv_rep;
            sU3RsrvDesc = gfeArchive.sU3RsrvDesc;
            sProU4Rsrv_rep = gfeArchive.sProU4Rsrv_rep;
            sU4RsrvDesc = gfeArchive.sU4RsrvDesc;
            sProHazInsT = gfeArchive.sProHazInsT;
            sProHazInsR_rep = gfeArchive.sProHazInsR_rep;
            sProHazInsMb_rep = gfeArchive.sProHazInsMb_rep;
            sProFloodIns_rep = gfeArchive.sProFloodIns_rep;
            sProRealETxMb_rep = gfeArchive.sProRealETxMb_rep;
            sProRealETxT = gfeArchive.sProRealETxT;
            sProRealETxR_rep = gfeArchive.sProRealETxR_rep;
            sProSchoolTx_rep = gfeArchive.sProSchoolTx_rep;
            sMipPiaProps = gfeArchive.sMipPiaProps;
            sVaFfProps = gfeArchive.sVaFfProps;
            sGfeOriginatorCompFProps = gfeArchive.sGfeOriginatorCompFProps;
            sGfeIsTPOTransaction = gfeArchive.sGfeIsTPOTransaction;
            sTitleInsFTable = gfeArchive.sTitleInsFTable;
            sEscrowFTable = gfeArchive.sEscrowFTable;   // paid to tb
            sStateRtcDesc = gfeArchive.sStateRtcDesc;   // paid to tb
            sCountyRtcDesc = gfeArchive.sCountyRtcDesc; // paid to tb
            sRecFDesc = gfeArchive.sRecFDesc;           // paid to tb
            sApprFPaidTo = gfeArchive.sApprFPaidTo;
            sCrFPaidTo = gfeArchive.sCrFPaidTo;
            sTxServFPaidTo = gfeArchive.sTxServFPaidTo;
            sFloodCertificationFPaidTo = gfeArchive.sFloodCertificationFPaidTo;
            sInspectFPaidTo = gfeArchive.sInspectFPaidTo;
            sProcFPaidTo = gfeArchive.sProcFPaidTo;
            sUwFPaidTo = gfeArchive.sUwFPaidTo;
            sWireFPaidTo = gfeArchive.sWireFPaidTo;
            s800U1FPaidTo = gfeArchive.s800U1FPaidTo;
            s800U2FPaidTo = gfeArchive.s800U2FPaidTo;
            s800U3FPaidTo = gfeArchive.s800U3FPaidTo;
            s800U4FPaidTo = gfeArchive.s800U4FPaidTo;
            s800U5FPaidTo = gfeArchive.s800U5FPaidTo;
            sOwnerTitleInsPaidTo = gfeArchive.sOwnerTitleInsPaidTo;
            sDocPrepFPaidTo = gfeArchive.sDocPrepFPaidTo;
            sNotaryFPaidTo = gfeArchive.sNotaryFPaidTo;
            sAttorneyFPaidTo = gfeArchive.sAttorneyFPaidTo;
            sU1TcPaidTo = gfeArchive.sU1TcPaidTo;
            sU2TcPaidTo = gfeArchive.sU2TcPaidTo;
            sU3TcPaidTo = gfeArchive.sU3TcPaidTo;
            sU4TcPaidTo = gfeArchive.sU4TcPaidTo;
            sU1GovRtcPaidTo = gfeArchive.sU1GovRtcPaidTo;
            sU2GovRtcPaidTo = gfeArchive.sU2GovRtcPaidTo;
            sU3GovRtcPaidTo = gfeArchive.sU3GovRtcPaidTo;
            sPestInspectPaidTo = gfeArchive.sPestInspectPaidTo;
            sU1ScPaidTo = gfeArchive.sU1ScPaidTo;
            sU2ScPaidTo = gfeArchive.sU2ScPaidTo;
            sU3ScPaidTo = gfeArchive.sU3ScPaidTo;
            sU4ScPaidTo = gfeArchive.sU4ScPaidTo;
            sU5ScPaidTo = gfeArchive.sU5ScPaidTo;
            sHazInsPiaPaidTo = gfeArchive.sHazInsPiaPaidTo;
            sMipPiaPaidTo = gfeArchive.sMipPiaPaidTo;
            sVaFfPaidTo = gfeArchive.sVaFfPaidTo;
            #endregion

            #endregion
        }

        [DependsOn(nameof(sUseGFEDataForSCFields))]
        private bool sfTemporarilyUseSCDataForSCFields { get { return false; } }
        /// <summary>
        /// Applies the archive associated with the migration to the file
        /// and tells the file to use the SC data for SC fields (as opposed to GFE).
        /// Disables saving.
        /// </summary>
        public void TemporarilyUseSCDataForSCFields()
        {
            sUseGFEDataForSCFields = false;
            // We apply the GFE archive to restore the "snapshot" of the fees
            // at the time of the migration. If we didn't do this, it is possible that
            // we could show data that is more recent than when the migration occurred. 
            // Certain fields were already shared between GFE/SC, and could have 
            // been updated.
            ApplyGFEArchive(RetrieveClosingCostMigrationArchive().GFEArchive);
        }

        /// <summary>
        /// Duplicates the closing cost migration archive, if it exists. DO NOT CALL THIS FOR TEMPLATES.
        /// </summary>
        /// <param name="srcLoanId"></param>
        /// <param name="destLoanId"></param>
        public static void DuplicateClosingCostMigrationArchive(Guid srcLoanId, Guid destLoanId)
        {
            string serializedArchive = LoadFromFileDB(s_ClosingCostMigrationArchivePrefix + srcLoanId);
            if (!string.IsNullOrEmpty(serializedArchive))
            {
                SaveToFileDB(s_ClosingCostMigrationArchivePrefix + destLoanId, serializedArchive);
            }
        }

        private string x_ClosingCostMigrationBackupKey
        {
            get { return s_ClosingCostMigrationArchivePrefix + sLId; }
        }

        private static readonly string s_ClosingCostMigrationArchivePrefix = "CLOSING_COST_MIGRATION_ARCHIVE_";

        #region Special Settlement Charges Getters
        private decimal GetSCField(Func<decimal> settlementChargeGetter, Func<decimal> gfeGetter, bool useGFEDataForSCFields)
        {
            return useGFEDataForSCFields ? gfeGetter() : settlementChargeGetter();
        }
        private string GetSCField(Func<string> settlementChargeGetter, Func<string> gfeGetter, bool useGFEDataForSCFields)
        {
            return useGFEDataForSCFields ? gfeGetter() : settlementChargeGetter();
        }
        private int GetSCField(Func<int> settlementChargeGetter, Func<int> gfeGetter, bool useGFEDataForSCFields)
        {
            return useGFEDataForSCFields ? gfeGetter() : settlementChargeGetter();
        }
        private bool GetSCField(Func<bool> settlementChargeGetter, Func<bool> gfeGetter, bool useGFEDataForSCFields)
        {
            return useGFEDataForSCFields ? gfeGetter() : settlementChargeGetter();
        }
        private void SetSCField(Action settlementChargeSetter, Action gfeSetter, bool useGFEDataForSCFields)
        {
            if (useGFEDataForSCFields)
            {
                gfeSetter();
            }
            else
            {
                settlementChargeSetter();
            }
        }

        #endregion

        #endregion

        #region Section 800 - ITEMS PAYABLE IN CONNECTION WITH LOAN

        #region Line 801
        [DependsOn(nameof(sSettlementLOrigFPc), nameof(sLOrigFPc), nameof(sUseGFEDataForSCFields))]
        public decimal sSettlementLOrigFPc
        {
            get { return GetSCField(() => GetRateField("sSettlementLOrigFPc"), () => sLOrigFPc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetRateField("sSettlementLOrigFPc", value), () => sLOrigFPc = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementLOrigFPc_rep
        {
            get { return ToRateString(() => sSettlementLOrigFPc); }
            set { sSettlementLOrigFPc = ToRate(value); }
        }

        [DependsOn(nameof(sSettlementLOrigFMb), nameof(sLOrigFMb), nameof(sUseGFEDataForSCFields))]
        public decimal sSettlementLOrigFMb
        {
            get { return GetSCField(() => GetMoneyField("sSettlementLOrigFMb"), () => sLOrigFMb, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementLOrigFMb", value), () => sLOrigFMb = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementLOrigFMb_rep
        {
            get { return ToMoneyString(() => sSettlementLOrigFMb); }
            set { sSettlementLOrigFMb = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementLOrigFPc), nameof(sLAmtCalc), nameof(sSettlementLOrigFMb), nameof(sLT), nameof(sFinalLAmt))]
        public decimal sSettlementLOrigF
        {
            get
            {
                // FHA and VA treat this one differently.
                decimal loanAmt = (E_sLT.VA == sLT) ? sFinalLAmt : sLAmtCalc;
                return (sSettlementLOrigFPc * loanAmt) / 100.00M + sSettlementLOrigFMb;
            }
        }
        public string sSettlementLOrigF_rep
        {
            get { return ToMoneyString(() => sSettlementLOrigF); }
        }

        [DependsOn(nameof(sSettlementLOrigFProps), nameof(sLOrigFProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementLOrigFProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementLOrigFProps"), () => sLOrigFProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementLOrigFProps", value), () => sLOrigFProps = value, sUseGFEDataForSCFields); }
        }


        public string sSettlementLOrigFProps_rep
        {
            get { return ToPropsString(() => sSettlementLOrigFProps); }
            set { sSettlementLOrigFProps = ToProps(value); }
        }

        [DependsOn(nameof(sSettlementLDiscntPc))]
        private decimal sSettlementBrokerCompPc
        {
            get
            {
                if (sSettlementLDiscntPc < 0)
                {
                    return -1 * sSettlementLDiscntPc;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementBrokerCompPc_rep
        {
            get { return ToRateString(() => sSettlementBrokerCompPc); }
        }

        /// <summary>
        /// DEPRECATED
        /// </summary>
        [DependsOn(nameof(sSettlementLDiscnt))]
        private decimal sSettlementBrokerComp
        {
            get
            {
                if (sSettlementLDiscnt < 0)
                {
                    return -1 * sSettlementLDiscnt;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// DEPRECATED
        /// </summary>
        public string sSettlementBrokerComp_rep
        {
            get { return ToMoneyString(() => sSettlementBrokerComp); }
        }

        /// <summary>
        /// DEPRECATED
        /// </summary>
        [DependsOn(nameof(sSettlementBrokerComp), nameof(sSettlementLOrigBrokerCreditF))]
        public decimal sSettlementBrokTotalCommission
        {
            get { return sSettlementBrokerComp + sSettlementLOrigBrokerCreditF; }
        }

        /// <summary>
        /// DEPRECATED
        /// </summary>
        public string sSettlementBrokTotalCommission_rep
        {
            get { return ToMoneyString(() => sSettlementBrokTotalCommission); }
        }

        /// <summary>
        /// DEPRECATED
        /// </summary>
        [DependsOn]
        public int sSettlementBrokerCompProps
        {
            get { return Get32BitPropsField("sSettlementBrokerCompProps"); }
            set { Set32BitPropsField("sSettlementBrokerCompProps", value); }
        }

        /// <summary>
        /// DEPRECATED
        /// </summary>
        [DependsOn]
        private decimal sSettlementBrokerCredit
        {
            get { return GetMoneyField("sSettlementBrokerCredit"); }
            set { SetMoneyField("sSettlementBrokerCredit", value); }
        }

        /// <summary>
        /// DEPRECATED
        /// </summary>
        public string sSettlementBrokerCredit_rep
        {
            get { return ToMoneyString(() => sSettlementBrokerCredit); }
            set { sSettlementBrokerCredit = ToMoney(value); }
        }

        /// <summary>
        /// DEPRECATED
        /// </summary>
        [DependsOn(nameof(sSettlementBrokerCredit))]
        private decimal sSettlementLOrigBrokerCreditF
        {
            get { return -1 * sSettlementBrokerCredit; }
        }

        /// <summary>
        /// DEPRECATED
        /// </summary>
        public string sSettlementLOrigBrokerCreditF_rep
        {
            get { return ToMoneyString(() => sSettlementLOrigBrokerCreditF); }
        }

        /// <summary>
        /// DEPRECATED
        /// </summary>
        [DependsOn]
        public int sSettlementLOrigBrokerCreditFProps
        {
            get { return Get32BitPropsField("sSettlementLOrigBrokerCreditFProps"); }
            set { Set32BitPropsField("sSettlementLOrigBrokerCreditFProps", value); }
        }

        #endregion

        #region Line 802

        [DependsOn(nameof(sSettlementLDiscntPc), nameof(sLDiscntPc), nameof(sUseGFEDataForSCFields))]
        public decimal sSettlementLDiscntPc
        {
            get
            {
                return GetSCField(() => GetRateField("sSettlementLDiscntPc"), () => sLDiscntPc, sUseGFEDataForSCFields);
            }

            set
            {
                SetSCField(() => SetRateField("sSettlementLDiscntPc", value), () => sLDiscntPc = value, sUseGFEDataForSCFields);
            }
        }
        public string sSettlementLDiscntPc_rep
        {
            get { return ToRateString(() => sSettlementLDiscntPc); }
            set { sSettlementLDiscntPc = ToRate(value); }
        }

        [DependsOn(nameof(sSettlementLDiscntBaseT), nameof(sLDiscntBaseT), nameof(sUseGFEDataForSCFields))]
        public E_PercentBaseT sSettlementLDiscntBaseT
        {
            get { return (E_PercentBaseT)GetSCField(() => GetTypeIndexField("sSettlementLDiscntBaseT"), () => (int)sLDiscntBaseT, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetTypeIndexField("sSettlementLDiscntBaseT", (int)value), () => sLDiscntBaseT = value, sUseGFEDataForSCFields); }
        }
        [DependsOn(nameof(sSettlementLDiscntBaseT), nameof(sLAmtCalc), nameof(sFinalLAmt))]
        private decimal sSettlementLDiscntBaseAmt
        {
            get
            {
                switch (sSettlementLDiscntBaseT)
                {
                    case E_PercentBaseT.LoanAmount:
                        return sLAmtCalc;
                    case E_PercentBaseT.TotalLoanAmount:
                        return sFinalLAmt;
                    default:
                        throw new UnhandledEnumException(sSettlementLDiscntBaseT);
                }
            }
        }
        [DependsOn(nameof(sSettlementLDiscntFMb), nameof(sSettlementLDiscntPc), nameof(sSettlementLDiscntBaseAmt), nameof(sUseGFEDataForSCFields), nameof(sLenderCreditCalculationMethodT), nameof(sLDiscnt))]
        public decimal sSettlementLDiscnt
        {
            get
            {

                if (this.sUseGFEDataForSCFields && this.sLenderCreditCalculationMethodT == E_sLenderCreditCalculationMethodT.CalculateFromFrontEndRateLock)
                {
                    return this.sLDiscnt;
                }
                else
                {
                    return sSettlementLDiscntFMb + (sSettlementLDiscntPc * sSettlementLDiscntBaseAmt) / 100.00M;
                }
            }
        }
        public string sSettlementLDiscnt_rep
        {
            get { return ToMoneyString(() => sSettlementLDiscnt); }
        }

        [DependsOn(nameof(sSettlementLDiscntFMb), nameof(sLDiscntFMb), nameof(sUseGFEDataForSCFields))]
        public decimal sSettlementLDiscntFMb
        {
            get { return GetSCField(() => GetMoneyField("sSettlementLDiscntFMb"), () => sLDiscntFMb, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementLDiscntFMb", value), () => sLDiscntFMb = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementLDiscntFMb_rep
        {
            get { return ToMoneyString(() => sSettlementLDiscntFMb); }
            set { sSettlementLDiscntFMb = ToMoney(value); }
        }

        // sSettlementLDiscntProps: This is deprecated
        [DependsOn(nameof(sSettlementLDiscntProps), nameof(sLDiscntProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementLDiscntProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementLDiscntProps"), () => sLDiscntProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementLDiscntProps", value), () => sLDiscntProps = value, sUseGFEDataForSCFields); }
        }

        [DependsOn(nameof(sSettlementCreditLenderPaidItemT), nameof(sGfeCreditLenderPaidItemT), nameof(sUseGFEDataForSCFields))]
        public E_CreditLenderPaidItemT sSettlementCreditLenderPaidItemT
        {
            get { return (E_CreditLenderPaidItemT)GetSCField(() => GetTypeIndexField("sSettlementCreditLenderPaidItemT"), () => (int)sGfeCreditLenderPaidItemT, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetTypeIndexField("sSettlementCreditLenderPaidItemT", (int)value), () => sGfeCreditLenderPaidItemT = value, sUseGFEDataForSCFields); }
        }

        [DependsOn(nameof(sSettlementCreditLenderPaidItemT), nameof(sSettlementTotalFeesPaidByLender), nameof(sGfeOriginatorCompF), nameof(sOriginatorCompensationPaymentSourceT))]
        private decimal sSettlementCreditLenderPaidItemF
        {
            get
            {
                return GetCreditLenderPaidItemF(sSettlementCreditLenderPaidItemT, () => sSettlementTotalFeesPaidByLender, () => sGfeOriginatorCompF, sOriginatorCompensationPaymentSourceT);
            }
        }
        public string sSettlementCreditLenderPaidItemF_rep
        {
            get { return ToMoneyString(() => sSettlementCreditLenderPaidItemF); }
        }

        [DependsOn(nameof(sSettlementLenderCreditF), nameof(sSettlementLDiscntBaseAmt))]
        private decimal sSettlementLenderCreditFPc
        {
            get
            {
                return GetLenderCreditFPc(sSettlementLenderCreditF, sSettlementLDiscntBaseAmt);
            }
        }
        public string sSettlementLenderCreditFPc_rep
        {
            get { return ToRateString(() => sSettlementLenderCreditFPc); }
        }

        [DependsOn(nameof(sSettlementLDiscnt), nameof(sSettlementCreditLenderPaidItemF))]
        public decimal sSettlementLenderCreditF
        {
            get
            {
                return GetLenderCreditF(sSettlementLDiscnt, sSettlementCreditLenderPaidItemF);
            }
        }
        public string sSettlementLenderCreditF_rep
        {
            get { return ToMoneyString(() => sSettlementLenderCreditF); }
        }

        [DependsOn(nameof(sSettlementDiscountPointF), nameof(sSettlementLDiscntBaseAmt))]
        private decimal sSettlementDiscountPointFPc
        {
            get
            {
                return GetDiscountPointFPc(sSettlementDiscountPointF, sSettlementLDiscntBaseAmt);
            }
        }
        public string sSettlementDiscountPointFPc_rep
        {
            get { return ToRateString(() => sSettlementDiscountPointFPc); }
        }

        [DependsOn(nameof(sSettlementDiscountPointF), nameof(sSettlementDiscountPointFPc), nameof(sFinalLAmt))]
        private decimal sSettlementDiscountPointFRoundingError
        {
            get
            {
                decimal pct = Math.Round(sSettlementDiscountPointFPc, 3, MidpointRounding.AwayFromZero) / 100;
                //decimal amount = sSettlementDiscountPointF - (pct * sFinalLAmt);
                decimal amount = sSettlementDiscountPointF - Math.Round(pct * sFinalLAmt, 2, MidpointRounding.AwayFromZero);  // opm 130248
                return amount;
            }
        }

        public string sSettlementDiscountPointFRoundingError_rep
        {
            get { return ToMoneyString(() => sSettlementDiscountPointFRoundingError); }
        }

        [DependsOn(nameof(sSettlementLDiscnt), nameof(sSettlementCreditLenderPaidItemF), nameof(sUseGFEDataForSCFields), nameof(sLDiscntFPdByBorr))]
        public decimal sSettlementDiscountPointF
        {
            get
            {
                if (sUseGFEDataForSCFields)
                {
                    return sLDiscntFPdByBorr;
                }

                return GetDiscountPointF(sSettlementLDiscnt, sSettlementCreditLenderPaidItemF);
            }
        }
        public string sSettlementDiscountPointF_rep
        {
            get { return ToMoneyString(()=> sSettlementDiscountPointF);}
        }
        
        [DependsOn(nameof(sSettlementDiscountPointFProps), nameof(sGfeDiscountPointFProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementDiscountPointFProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementDiscountPointFProps"), () => sGfeDiscountPointFProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementDiscountPointFProps", value), () => sGfeDiscountPointFProps = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementDiscountPointFProps_rep
        {
            get { return ToPropsString(()=>sSettlementDiscountPointFProps); }
            set { sSettlementDiscountPointFProps = ToProps(value); }
        }

        #endregion

        #region Line 804
        [DependsOn(nameof(sSettlementApprF), nameof(sApprF), nameof(sUseGFEDataForSCFields))]
        public decimal sSettlementApprF
        {
            get { return GetSCField(() => GetMoneyField("sSettlementApprF"), () => sApprF, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementApprF", value), () => sApprF = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementApprF_rep
        {
            get { return ToMoneyString(() => sSettlementApprF); }
            set { sSettlementApprF = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementApprFPaid), nameof(sApprFPaid), nameof(sUseGFEDataForSCFields))]
        public bool sSettlementApprFPaid
        {
            get { return GetSCField(() => GetBoolField("sSettlementApprFPaid"), () => sApprFPaid, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetBoolField("sSettlementApprFPaid", value), () => sApprFPaid = value, sUseGFEDataForSCFields); }
        }

        [DependsOn(nameof(sSettlementApprFProps), nameof(sApprFProps), nameof(sUseGFEDataForSCFields), nameof(sfGetPropsUpdatedWithLinkedProps))]
        public int sSettlementApprFProps
        {
            get { return GetSCField(() =>
                {
                    int props = Get32BitPropsField("sSettlementApprFProps");

                    props = GetPropsUpdatedWithLinkedProps(props, E_AgentRoleT.Appraiser);

                    return props;
                }
                , () => sApprFProps, sUseGFEDataForSCFields); }
            set
            {
                SetSCField(() => Set32BitPropsField("sSettlementApprFProps", value), () => sApprFProps = value, sUseGFEDataForSCFields);
            }
        }

        public string sSettlementApprFProps_rep
        {
            get { return ToPropsString(() => sSettlementApprFProps); }
            set { sSettlementApprFProps = ToProps(value); }
        }

        #endregion

        #region Line 805
        [DependsOn(nameof(sSettlementCrF), nameof(sCrF), nameof(sUseGFEDataForSCFields))]
        public decimal sSettlementCrF
        {
            get { return GetSCField(() => GetMoneyField("sSettlementCrF"), () => sCrF, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementCrF", value), () => sCrF = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementCrF_rep
        {
            get { return ToMoneyString(() => sSettlementCrF); }
            set { sSettlementCrF = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementCrFProps), nameof(sCrFProps), nameof(sUseGFEDataForSCFields), nameof(sfGetPropsUpdatedWithLinkedProps))]
        public int sSettlementCrFProps
        {
            get { return GetSCField(() =>
                {
                    int props = Get32BitPropsField("sSettlementCrFProps");

                    props = GetPropsUpdatedWithLinkedProps(props, E_AgentRoleT.CreditReport);

                    return props;
                }
                , () => sCrFProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementCrFProps", value), () => sCrFProps = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementCrFProps_rep
        {
            get { return ToPropsString(() => sSettlementCrFProps); }
            set { sSettlementCrFProps = ToProps(value); }
        }

        [DependsOn(nameof(sSettlementCrFPaid), nameof(sCrFPaid), nameof(sUseGFEDataForSCFields))]
        public bool sSettlementCrFPaid
        {
            get { return GetSCField(() => GetBoolField("sSettlementCrFPaid"), () => sCrFPaid, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetBoolField("sSettlementCrFPaid", value), () => sCrFPaid = value, sUseGFEDataForSCFields); }
        }

        #endregion

        #region Line 806

        [DependsOn(nameof(sSettlementTxServF), nameof(sTxServF), nameof(sUseGFEDataForSCFields))]
        public decimal sSettlementTxServF
        {
            get { return GetSCField(() => GetMoneyField("sSettlementTxServF"), () => sTxServF, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementTxServF", value), () => sTxServF = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementTxServF_rep
        {
            get { return ToMoneyString(() => sSettlementTxServF); }
            set { sSettlementTxServF = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementTxServFProps), nameof(sTxServFProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementTxServFProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementTxServFProps"), () => sTxServFProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementTxServFProps", value), () => sTxServFProps = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementTxServFProps_rep
        {
            get { return ToPropsString(() => sSettlementTxServFProps); }
            set { sSettlementTxServFProps = ToProps(value); }
        }

        #endregion

        #region Line 807
        [DependsOn(nameof(sSettlementFloodCertificationDeterminationT), nameof(sFloodCertificationDeterminationT), nameof(sUseGFEDataForSCFields))]
        public E_FloodCertificationDeterminationT sSettlementFloodCertificationDeterminationT
        {
            get { return (E_FloodCertificationDeterminationT)GetSCField(() => GetTypeIndexField("sSettlementFloodCertificationDeterminationT"), () => (int)sFloodCertificationDeterminationT, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetTypeIndexField("sSettlementFloodCertificationDeterminationT", (int)value), () => sFloodCertificationDeterminationT = value, sUseGFEDataForSCFields); }
        }

        [DependsOn(nameof(sSettlementFloodCertificationF), nameof(sFloodCertificationF), nameof(sUseGFEDataForSCFields))]
        private decimal sSettlementFloodCertificationF
        {
            get { return GetSCField(() => GetMoneyField("sSettlementFloodCertificationF"), () => sFloodCertificationF, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementFloodCertificationF", value), () => sFloodCertificationF = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementFloodCertificationF_rep
        {
            get { return ToMoneyString(() => sSettlementFloodCertificationF); }
            set { sSettlementFloodCertificationF = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementFloodCertificationFProps), nameof(sFloodCertificationFProps), nameof(sUseGFEDataForSCFields), nameof(sfGetPropsUpdatedWithLinkedProps))]
        public int sSettlementFloodCertificationFProps
        {
            get { return GetSCField(() =>
                {
                    int props = GetCountField("sSettlementFloodCertificationFProps");

                    props = GetPropsUpdatedWithLinkedProps(props, E_AgentRoleT.FloodProvider);

                    return props;
                }, () => sFloodCertificationFProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetCountField("sSettlementFloodCertificationFProps", value), () => sFloodCertificationFProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementFloodCertificationFProps_rep
        {
            get { return ToPropsString(() => sSettlementFloodCertificationFProps); }
            set { sSettlementFloodCertificationFProps = ToProps(value); }
        }

        #endregion

        #region Line 808
        [DependsOn(nameof(sSettlementMBrokFBaseT), nameof(sMBrokFBaseT), nameof(sUseGFEDataForSCFields))]
        public E_PercentBaseT sSettlementMBrokFBaseT
        {
            get { return (E_PercentBaseT)GetSCField(() => GetTypeIndexField("sSettlementMBrokFBaseT"), () => (int)sMBrokFBaseT, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetTypeIndexField("sSettlementMBrokFBaseT", (int)value), () => sMBrokFBaseT = value, sUseGFEDataForSCFields); }
        }
        [DependsOn(nameof(sSettlementMBrokFBaseT), nameof(sLAmtCalc), nameof(sFinalLAmt))]
        private decimal sSettlementMBrokFBaseAmt
        {
            get
            {
                switch (sSettlementMBrokFBaseT)
                {
                    case E_PercentBaseT.LoanAmount:
                        return sLAmtCalc;
                    case E_PercentBaseT.TotalLoanAmount:
                        return sFinalLAmt;
                    default:
                        throw new UnhandledEnumException(sSettlementMBrokFBaseT);
                }
            }
        }
        [DependsOn(nameof(sSettlementMBrokFPc), nameof(sSettlementMBrokFBaseAmt), nameof(sSettlementMBrokFMb))]
        public decimal sSettlementMBrokF
        {
            get { return (sSettlementMBrokFPc * sSettlementMBrokFBaseAmt) / 100.00M + sSettlementMBrokFMb; }
        }
        public string sSettlementMBrokF_rep
        {
            get { return ToMoneyString(() => sSettlementMBrokF); }
        }

        [DependsOn(nameof(sSettlementMBrokFPc), nameof(sMBrokFPc), nameof(sUseGFEDataForSCFields))]
        public decimal sSettlementMBrokFPc
        {
            get { return GetSCField(() => GetRateField("sSettlementMBrokFPc"), () => sMBrokFPc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetRateField("sSettlementMBrokFPc", value), () => sMBrokFPc = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementMBrokFPc_rep
        {
            get { return ToRateString(() => sSettlementMBrokFPc); }
            set { sSettlementMBrokFPc = ToRate(value); }
        }

        [DependsOn(nameof(sSettlementMBrokFMb), nameof(sMBrokFMb), nameof(sUseGFEDataForSCFields))]
        public decimal sSettlementMBrokFMb
        {
            get { return GetSCField(() => GetMoneyField("sSettlementMBrokFMb"), () => sMBrokFMb, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementMBrokFMb", value), () => sMBrokFMb = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementMBrokFMb_rep
        {
            get { return ToMoneyString(() => sSettlementMBrokFMb); }
            set { sSettlementMBrokFMb = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementMBrokFProps), nameof(sMBrokFProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementMBrokFProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementMBrokFProps"), () => sMBrokFProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementMBrokFProps", value), () => sMBrokFProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementMBrokFProps_rep
        {
            get { return ToPropsString(() => sSettlementMBrokFProps); }
            set { sSettlementMBrokFProps = ToProps(value); }
        }

        #endregion

        #region Line 809
        [DependsOn(nameof(sSettlementInspectF), nameof(sInspectF), nameof(sUseGFEDataForSCFields))]
        public decimal sSettlementInspectF
        {
            get { return GetSCField(() => GetMoneyField("sSettlementInspectF"), () => sInspectF, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementInspectF", value), () => sInspectF = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementInspectF_rep
        {
            get { return ToMoneyString(() => sSettlementInspectF); }
            set { sSettlementInspectF = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementInspectFProps), nameof(sInspectFProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementInspectFProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementInspectFProps"), () => sInspectFProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementInspectFProps", value), () => sInspectFProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementInspectFProps_rep
        {
            get { return ToPropsString(() => sSettlementInspectFProps); }
            set { sSettlementInspectFProps = ToProps(value); }
        }

        #endregion

        #region Line 810
        [DependsOn(nameof(sSettlementProcF), nameof(sProcF), nameof(sUseGFEDataForSCFields))]
        private decimal sSettlementProcF
        {
            get { return GetSCField(() => GetMoneyField("sSettlementProcF"), () => sProcF, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementProcF", value), () => sProcF = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementProcF_rep
        {
            get { return ToMoneyString(() => sSettlementProcF); }
            set { sSettlementProcF = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementProcFPaid), nameof(sProcFPaid), nameof(sUseGFEDataForSCFields))]
        public bool sSettlementProcFPaid
        {
            get { return GetSCField(() => GetBoolField("sSettlementProcFPaid"), () => sProcFPaid, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetBoolField("sSettlementProcFPaid", value), () => sProcFPaid = value, sUseGFEDataForSCFields); }
        }

        [DependsOn(nameof(sSettlementProcFProps), nameof(sProcFProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementProcFProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementProcFProps"), () => sProcFProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementProcFProps", value), () => sProcFProps = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementProcFProps_rep
        {
            get { return ToPropsString(() => sSettlementProcFProps); }
            set { sSettlementProcFProps = ToProps(value); }
        }

        #endregion

        #region  Line 811
        [DependsOn(nameof(sSettlementUwF), nameof(sUwF), nameof(sUseGFEDataForSCFields))]
        private decimal sSettlementUwF
        {
            get { return GetSCField(() => GetMoneyField("sSettlementUwF"), () => sUwF, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementUwF", value), () => sUwF = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementUwF_rep
        {
            get { return ToMoneyString(() => sSettlementUwF); }
            set { sSettlementUwF = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementUwFProps), nameof(sUwFProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementUwFProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementUwFProps"), () => sUwFProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementUwFProps", value), () => sUwFProps = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementUwFProps_rep
        {
            get { return ToPropsString(() => sSettlementUwFProps); }
            set { sSettlementUwFProps = ToProps(value); }
        }

        #endregion

        #region Line 812
        [DependsOn(nameof(sSettlementWireF), nameof(sWireF), nameof(sUseGFEDataForSCFields))]
        private decimal sSettlementWireF
        {
            get { return GetSCField(() => GetMoneyField("sSettlementWireF"), () => sWireF, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementWireF", value), () => sWireF = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementWireF_rep
        {
            get { return ToMoneyString(() => sSettlementWireF); }
            set { sSettlementWireF = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementWireFProps), nameof(sWireFProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementWireFProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementWireFProps"), () => sWireFProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementWireFProps", value), () => sWireFProps = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementWireFProps_rep
        {
            get { return ToPropsString(() => sSettlementWireFProps); }
            set { sSettlementWireFProps = ToProps(value); }
        }

        #endregion

        #region Line 813
        [DependsOn(nameof(sSettlement800U1F), nameof(s800U1F), nameof(sUseGFEDataForSCFields))]
        private decimal sSettlement800U1F
        {
            get { return GetSCField(() => GetMoneyField("sSettlement800U1F"), () => s800U1F, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlement800U1F", value), () => s800U1F = value, sUseGFEDataForSCFields); }
        }

        public string sSettlement800U1F_rep
        {
            get { return ToMoneyString(() => sSettlement800U1F); }
            set { sSettlement800U1F = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlement800U1FProps), nameof(s800U1FProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlement800U1FProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlement800U1FProps"), () => s800U1FProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlement800U1FProps", value), () => s800U1FProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlement800U1FProps_rep
        {
            get { return ToPropsString(() => sSettlement800U1FProps); }
            set { sSettlement800U1FProps = ToProps(value); }
        }

        [DependsOn(nameof(sSettlement800U1FDesc), nameof(s800U1FDesc), nameof(sUseGFEDataForSCFields))]
        public string sSettlement800U1FDesc
        {
            get { return GetSCField(() => GetLongTextField("sSettlement800U1FDesc"), () => s800U1FDesc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetLongTextField("sSettlement800U1FDesc", value), () => s800U1FDesc = value, sUseGFEDataForSCFields); }
        }
        #endregion

        #region Line 814
        [DependsOn(nameof(sSettlement800U2F), nameof(s800U2F), nameof(sUseGFEDataForSCFields))]
        private decimal sSettlement800U2F
        {
            get { return GetSCField(() => GetMoneyField("sSettlement800U2F"), () => s800U2F, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlement800U2F", value), () => s800U2F = value, sUseGFEDataForSCFields); }
        }

        public string sSettlement800U2F_rep
        {
            get { return ToMoneyString(() => sSettlement800U2F); }
            set { sSettlement800U2F = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlement800U2FProps), nameof(s800U2FProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlement800U2FProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlement800U2FProps"), () => s800U2FProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlement800U2FProps", value), () => s800U2FProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlement800U2FProps_rep
        {
            get { return ToPropsString(() => sSettlement800U2FProps); }
            set { sSettlement800U2FProps = ToProps(value); }
        }

        [DependsOn(nameof(sSettlement800U2FDesc), nameof(s800U2FDesc), nameof(sUseGFEDataForSCFields))]
        public string sSettlement800U2FDesc
        {
            get { return GetSCField(() => GetLongTextField("sSettlement800U2FDesc"), () => s800U2FDesc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetLongTextField("sSettlement800U2FDesc", value), () => s800U2FDesc = value, sUseGFEDataForSCFields); }
        }
        #endregion

        #region Line 815
        [DependsOn(nameof(sSettlement800U3F), nameof(s800U3F), nameof(sUseGFEDataForSCFields))]
        private decimal sSettlement800U3F
        {
            get { return GetSCField(() => GetMoneyField("sSettlement800U3F"), () => s800U3F, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlement800U3F", value), () => s800U3F = value, sUseGFEDataForSCFields); }
        }

        public string sSettlement800U3F_rep
        {
            get { return ToMoneyString(() => sSettlement800U3F); }
            set { sSettlement800U3F = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlement800U3FProps), nameof(s800U3FProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlement800U3FProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlement800U3FProps"), () => s800U3FProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlement800U3FProps", value), () => s800U3FProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlement800U3FProps_rep
        {
            get { return ToPropsString(() => sSettlement800U3FProps); }
            set { sSettlement800U3FProps = ToProps(value); }
        }

        [DependsOn(nameof(sSettlement800U3FDesc), nameof(s800U3FDesc), nameof(sUseGFEDataForSCFields))]
        public string sSettlement800U3FDesc
        {
            get { return GetSCField(() => GetLongTextField("sSettlement800U3FDesc"), () => s800U3FDesc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetLongTextField("sSettlement800U3FDesc", value), () => s800U3FDesc = value, sUseGFEDataForSCFields); }
        }

        #endregion

        #region Line 816
        [DependsOn(nameof(sSettlement800U4F), nameof(s800U4F), nameof(sUseGFEDataForSCFields))]
        private decimal sSettlement800U4F
        {
            get { return GetSCField(() => GetMoneyField("sSettlement800U4F"), () => s800U4F, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlement800U4F", value), () => s800U4F = value, sUseGFEDataForSCFields); }
        }

        public string sSettlement800U4F_rep
        {
            get { return ToMoneyString(() => sSettlement800U4F); }
            set { sSettlement800U4F = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlement800U4FProps), nameof(s800U4FProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlement800U4FProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlement800U4FProps"), () => s800U4FProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlement800U4FProps", value), () => s800U4FProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlement800U4FProps_rep
        {
            get { return ToPropsString(() => sSettlement800U4FProps); }
            set { sSettlement800U4FProps = ToProps(value); }
        }

        [DependsOn(nameof(sSettlement800U4FDesc), nameof(s800U4FDesc), nameof(sUseGFEDataForSCFields))]
        public string sSettlement800U4FDesc
        {
            get { return GetSCField(() => GetLongTextField("sSettlement800U4FDesc"), () => s800U4FDesc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetLongTextField("sSettlement800U4FDesc", value), () => s800U4FDesc = value, sUseGFEDataForSCFields); }
        }

        #endregion

        #region Line 817
        [DependsOn(nameof(sSettlement800U5F), nameof(s800U5F), nameof(sUseGFEDataForSCFields))]
        private decimal sSettlement800U5F
        {
            get { return GetSCField(() => GetMoneyField("sSettlement800U5F"), () => s800U5F, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlement800U5F", value), () => s800U5F = value, sUseGFEDataForSCFields); }
        }

        public string sSettlement800U5F_rep
        {
            get { return ToMoneyString(() => sSettlement800U5F); }
            set { sSettlement800U5F = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlement800U5FProps), nameof(s800U5FProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlement800U5FProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlement800U5FProps"), () => s800U5FProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlement800U5FProps", value), () => s800U5FProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlement800U5FProps_rep
        {
            get { return ToPropsString(() => sSettlement800U5FProps); }
            set { sSettlement800U5FProps = ToProps(value); }
        }

        [DependsOn(nameof(sSettlement800U5FDesc), nameof(s800U5FDesc), nameof(sUseGFEDataForSCFields))]
        public string sSettlement800U5FDesc
        {
            get { return GetSCField(() => GetLongTextField("sSettlement800U5FDesc"), () => s800U5FDesc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetLongTextField("sSettlement800U5FDesc", value), () => s800U5FDesc = value, sUseGFEDataForSCFields); }
        }

        #endregion

        #endregion

        #region Section 900 - ITEMS REQUIRED BY LENDER TO BE PAID IN ADVANCE

        #region Line 901
        [DependsOn(nameof(sSettlementIPiaDyLckd), nameof(sSettlementIPiaDy), nameof(sSchedFundD), nameof(sIPiaDy), nameof(sUseGFEDataForSCFields), nameof(sSchedDueD1))]
        private int sSettlementIPiaDy
        {
            get
            {
                if (sSettlementIPiaDyLckd)
                {
                    return GetSCField(() => GetCountField("sSettlementIPiaDy"), () => sIPiaDy, sUseGFEDataForSCFields);
                }
                else if (sUseGFEDataForSCFields)
                {
                    return sIPiaDy;
                }
                else
                {
                    // Change the calculation of sSettlementIPiaDy. See how sIPiaDy implement the calculation. 
                    // The only different is that sSettlementIPiaDy use sSchedDueD1 and sSchedFundD (<- per PDE, use sSchedFundD not sFundD)
                    return Calculate_IPiaDy(sSchedDueD1, sSchedFundD);
                }
            }
            set { SetSCField(() => SetCountField("sSettlementIPiaDy", value), () => sIPiaDy = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementIPiaDy_rep
        {
            get { return ToCountString(() => sSettlementIPiaDy); }
            set { sSettlementIPiaDy = m_convertLos.ToCount(value); }
        }

        [DependsOn(nameof(sSettlementIPiaDyLckd), nameof(sIPiaDyLckd), nameof(sUseGFEDataForSCFields))]
        public bool sSettlementIPiaDyLckd
        {
            get { return GetSCField(() => GetBoolField("sSettlementIPiaDyLckd"), () => sIPiaDyLckd, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetBoolField("sSettlementIPiaDyLckd", value), () => sIPiaDyLckd = value, sUseGFEDataForSCFields); }
        }

        [DependsOn(nameof(sSettlementIPerDayLckd), nameof(sIPerDayLckd), nameof(sUseGFEDataForSCFields))]
        public bool sSettlementIPerDayLckd
        {
            get { return GetSCField(() => GetBoolField("sSettlementIPerDayLckd"), () => sIPerDayLckd, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetBoolField("sSettlementIPerDayLckd", value), () => sIPerDayLckd = value, sUseGFEDataForSCFields); }
        }

        [DependsOn(nameof(sSettlementIPerDay), nameof(sDaysInYr), nameof(sNoteIR), nameof(sFinalLAmt), nameof(sSettlementIPerDayLckd), nameof(sIPerDay), nameof(sUseGFEDataForSCFields))]
        private decimal sSettlementIPerDay
        {
            get
            {
                if (sSettlementIPerDayLckd)
                {
                    return GetSCField(() => GetMoneyField("sSettlementIPerDay", 6), () => sIPerDay, sUseGFEDataForSCFields);
                }
                else
                {
                    if (sDaysInYr == 0)
                    {
                        LogError("sDaysInYr (number of days per year) is 0; Cannot compute sSettlementIPerDay correctly. Return 0 for sSettlementIPerDay (Settlement Charges Page)");
                        return 0;
                    }
                    return (sNoteIR / 100.00M) * sFinalLAmt / sDaysInYr;
                }
            }
            set { SetSCField(() => SetMoneyField("sSettlementIPerDay", value, 6), () => sIPerDay = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementIPerDay_rep
        {
            get { return ToMoneyString6DecimalDigits(() => sSettlementIPerDay); }
            set { sSettlementIPerDay = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementIPerDay), nameof(sSettlementIPiaDy))]
        public decimal sSettlementIPia
        {
            get { return Math.Round(sSettlementIPiaDy * sSettlementIPerDay, 2, MidpointRounding.AwayFromZero); }
        }
        public string sSettlementIPia_rep
        {
            get { return ToMoneyString(() => sSettlementIPia); }
        }

        [DependsOn(nameof(sSettlementIPiaProps), nameof(sIPiaProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementIPiaProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementIPiaProps"), () => sIPiaProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementIPiaProps", value), () => sIPiaProps = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementIPiaProps_rep
        {
            get { return ToPropsString(() => sSettlementIPiaProps); }
            set { sSettlementIPiaProps = ToProps(value); }
        }

        #endregion

        #region Line 902 - DIRECT FROM GFE
        // PER THE SPEC, WE WILL USE THE VALUES DIRECTLY FROM THE GFE
        /*[DependsOn(nameof(sFfUfmip1003), nameof(sLT))]
        public decimal sSettlementMipPia
        {
            get
            {
                switch (sLT)
                {
                    case E_sLT.VA:
                        return 0;
                    default:
                        return sFfUfmip1003;
                }
            }
        }
        public string sSettlementMipPia_rep
        {
            get { return ToMoneyString(() => sSettlementMipPia); }
        }

        [DependsOn]
        public int sSettlementMipPiaProps
        {
            get { return Get32BitPropsField("sSettlementMipPiaProps"); }
            set
            {
                Set32BitPropsField("sSettlementMipPiaProps", value);
            }
        }*/
        #endregion

        #region Line 903

        /*[DependsOn]
        private decimal sSettlementProHazInsMb
        {
            get { return GetMoneyField("sSettlementProHazInsMb"); }
            set { SetMoneyField("sSettlementProHazInsMb", value); }
        }
        public string sSettlementProHazInsMb_rep
        {
            get { return ToMoneyString(() => sSettlementProHazInsMb); }
            set { sSettlementProHazInsMb = ToMoney(value); }
        }*/

        /*[DependsOn]
        private decimal sSettlementProHazInsR
        {
            get { return GetRateField("sSettlementProHazInsR"); }
            set { SetRateField("sSettlementProHazInsR", value); }
        }

        public string sSettlementProHazInsR_rep
        {
            get { return ToRateString(() => sSettlementProHazInsR); }
            set { sSettlementProHazInsR = ToRate(value); }
        }*/

        /*[DependsOn]
        public E_PercentBaseT sSettlementProHazInsT
        {
            get { return (E_PercentBaseT)GetTypeIndexField("sSettlementProHazInsT"); }
            set { SetTypeIndexField("sSettlementProHazInsT", (int)value); }
        }*/

        /*[DependsOn(nameof(sSettlementProHazInsT), nameof(sLAmtCalc), nameof(sPurchPrice), nameof(sApprVal), nameof(sFinalLAmt))]
        public decimal sSettlementProHazInsBaseAmt
        {
            get
            {
                switch (sSettlementProHazInsT)
                {
                    case E_PercentBaseT.LoanAmount: // Loan amount
                        return sLAmtCalc;
                    case E_PercentBaseT.SalesPrice: // sPurchPrice
                        return sPurchPrice;
                    case E_PercentBaseT.AppraisalValue: // Appraisal value
                        return sApprVal;
                    case E_PercentBaseT.TotalLoanAmount:
                        return sFinalLAmt;
                    default:
                        Throw("Error processing sSettlementProHazInsT value");
                        return 0; // To make compiler happy.
                }
            }
        }
        public string sSettlementProHazInsBaseAmt_rep
        {
            get { return ToMoneyString(() => sSettlementProHazInsBaseAmt); }
        }*/

        [DependsOn(nameof(sSettlementHazInsPiaMon), nameof(sHazInsPiaMon), nameof(sUseGFEDataForSCFields))]
        private int sSettlementHazInsPiaMon
        {
            get { return GetSCField(() => GetCountField("sSettlementHazInsPiaMon"), () => sHazInsPiaMon, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetCountField("sSettlementHazInsPiaMon", value), () => sHazInsPiaMon = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementHazInsPiaMon_rep
        {
            get { return ToCountString(() => sSettlementHazInsPiaMon); }
            set { sSettlementHazInsPiaMon = m_convertLos.ToCount(value); }
        }

        [DependsOn(nameof(sSettlementHazInsPiaMon), nameof(sProHazIns))]
        private decimal sSettlementHazInsPia
        {
            get { return sSettlementHazInsPiaMon * sProHazIns; }
        }
        public string sSettlementHazInsPia_rep
        {
            get { return ToMoneyString(() => sSettlementHazInsPia); }
        }

        [DependsOn(nameof(sSettlementHazInsPiaProps), nameof(sHazInsPiaProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementHazInsPiaProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementHazInsPiaProps"), () => sHazInsPiaProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementHazInsPiaProps", value), () => sHazInsPiaProps = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementHazInsPiaProps_rep
        {
            get { return ToPropsString(() => sSettlementHazInsPiaProps); }
            set { sSettlementHazInsPiaProps = ToProps(value); }
        }

        #endregion

        #region Line 904
        [DependsOn(nameof(sSettlement904PiaDesc), nameof(s904PiaDesc), nameof(sUseGFEDataForSCFields))]
        public string sSettlement904PiaDesc
        {
            get { return GetSCField(() => GetLongTextField("sSettlement904PiaDesc"), () => s904PiaDesc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetLongTextField("sSettlement904PiaDesc", value), () => s904PiaDesc = value, sUseGFEDataForSCFields); }
        }

        [DependsOn(nameof(sSettlement904Pia), nameof(s904Pia), nameof(sUseGFEDataForSCFields))]
        private decimal sSettlement904Pia
        {
            get { return GetSCField(() => GetMoneyField("sSettlement904Pia"), () => s904Pia, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlement904Pia", value), () => s904Pia = value, sUseGFEDataForSCFields); }
        }

        public string sSettlement904Pia_rep
        {
            get { return ToMoneyString(() => sSettlement904Pia); }
            set { sSettlement904Pia = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlement904PiaProps), nameof(s904PiaProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlement904PiaProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlement904PiaProps"), () => s904PiaProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlement904PiaProps", value), () => s904PiaProps = value, sUseGFEDataForSCFields); }
        }

        public string sSettlement904PiaProps_rep
        {
            get { return ToPropsString(() => sSettlement904PiaProps); }
            set { sSettlement904PiaProps = ToProps(value); }
        }

        #endregion

        #region Line 905 - DIRECT FROM GFE
        // PER THE SPEC, WE WILL USE THE VALUES DIRECTLY FROM THE GFE
        /*[DependsOn]
        private decimal sSettlementVaFf
        {
            get { return GetMoneyField("sSettlementVaFf"); }
            set { SetMoneyField("sSettlementVaFf", value); }
        }

        public string sSettlementVaFf_rep
        {
            get { return ToMoneyString(() => sSettlementVaFf); }
            set { sSettlementVaFf = ToMoney(value); }
        }

        [DependsOn]
        public int sSettlementVaFfProps
        {
            get { return Get32BitPropsField("sSettlementVaFfProps"); }
            set { Set32BitPropsField("sSettlementVaFfProps", value); }
        }*/

        #endregion

        #region Line 906
        [DependsOn(nameof(sSettlement900U1PiaDesc), nameof(s900U1PiaDesc), nameof(sUseGFEDataForSCFields))]
        public string sSettlement900U1PiaDesc
        {
            get { return GetSCField(() => GetLongTextField("sSettlement900U1PiaDesc"), () => s900U1PiaDesc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetLongTextField("sSettlement900U1PiaDesc", value), () => s900U1PiaDesc = value, sUseGFEDataForSCFields); }
        }

        [DependsOn(nameof(sSettlement900U1Pia), nameof(s900U1Pia), nameof(sUseGFEDataForSCFields))]
        private decimal sSettlement900U1Pia
        {
            get { return GetSCField(() => GetMoneyField("sSettlement900U1Pia"), () => s900U1Pia, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlement900U1Pia", value), () => s900U1Pia = value, sUseGFEDataForSCFields); }
        }

        public string sSettlement900U1Pia_rep
        {
            get { return ToMoneyString(() => sSettlement900U1Pia); }
            set { sSettlement900U1Pia = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlement900U1PiaProps), nameof(s900U1PiaProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlement900U1PiaProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlement900U1PiaProps"), () => s900U1PiaProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlement900U1PiaProps", value), () => s900U1PiaProps = value, sUseGFEDataForSCFields); }
        }

        public string sSettlement900U1PiaProps_rep
        {
            get { return ToPropsString(() => sSettlement900U1PiaProps); }
            set { sSettlement900U1PiaProps = ToProps(value); }
        }

        #endregion

        #endregion

        #region Section 1000 - RESERVES DEPOSITED WITH LENDER

        #region Line 1002
        /*[DependsOn(nameof(sSettlementProHazInsMb), nameof(sSettlementProHazInsR), nameof(sSettlementProHazInsBaseAmt))]
        private decimal sSettlementProHazIns // Per PDE, we won't use this for now, and will use the actual GFE field instead
        {
            get
            {
                return sSettlementProHazInsMb + (sSettlementProHazInsR * sSettlementProHazInsBaseAmt / 1200.00M);
            }
        }
        private string sSettlementProHazIns_rep
        {
            get { return ToMoneyString(() => sSettlementProHazIns); }
        }*/

        // Per PDE, we will use the GFE sProHazIns for this so that the settlement charges page will be in sync with the GFE
        [DependsOn(nameof(sSettlementHazInsRsrvMon), nameof(sProHazIns))]
        public decimal sSettlementHazInsRsrv
        {
            get { return sSettlementHazInsRsrvMon * Math.Round(sProHazIns, 2, MidpointRounding.AwayFromZero); }
        }
        public string sSettlementHazInsRsrv_rep
        {
            get { return ToMoneyString(() => sSettlementHazInsRsrv); }
        }

        [DependsOn(nameof(sSettlementHazInsRsrvMon), nameof(sHazInsRsrvMon), nameof(sUseGFEDataForSCFields))]
        public int sSettlementHazInsRsrvMon
        {
            get { return GetSCField(() => GetCountField("sSettlementHazInsRsrvMon"), () => sHazInsRsrvMon, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetCountField("sSettlementHazInsRsrvMon", value), () => sHazInsRsrvMon = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementHazInsRsrvMon_rep
        {
            get { return ToCountString(() => sSettlementHazInsRsrvMon); }
            set { sSettlementHazInsRsrvMon = m_convertLos.ToCount(value); }
        }

        [DependsOn(nameof(sSettlementHazInsRsrvProps), nameof(sHazInsRsrvProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementHazInsRsrvProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementHazInsRsrvProps"), () => sHazInsRsrvProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementHazInsRsrvProps", value), () => sHazInsRsrvProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementHazInsRsrvProps_rep
        {
            get { return ToPropsString(() => sSettlementHazInsRsrvProps); }
            set { sSettlementHazInsRsrvProps = ToProps(value); }
        }


        #endregion

        #region Line 1003
        // Per PDE, we will use the GFE field on the settlement page for now instead of this field, to keep the settlement charges page in sync with the GFE
        /*[DependsOn(nameof(sSettlementProMInsMb), nameof(sLT))]
        private decimal sSettlementProMInsMb
        {
            get
            {
                if (sLT == E_sLT.FHA)
                {
                    return 0;
                }
                else
                {
                    return GetMoneyField("sSettlementProMInsMb");
                }
            }
            set { SetMoneyField("sSettlementProMInsMb", value); }
        }
        private string sSettlementProMInsMb_rep
        {
            get { return ToMoneyString(() => sSettlementProMInsMb); }
            set { sSettlementProMInsMb = ToMoney(value); }
        }*/

        // Per PDE, we will use the GFE field on the settlement page for now instead of this field, to keep the settlement charges page in sync with the GFE
        /*[DependsOn]
        private decimal sSettlementProMInsR
        {
            get { return GetRateField("sSettlementProMInsR"); }
            set { SetRateField("sSettlementProMInsR", value); }
        }
        private string sSettlementProMInsR_rep
        {
            get { return ToRateString(() => sSettlementProMInsR); }
            set { sSettlementProMInsR = ToRate(value); }
        }*/

        // Per PDE, we will use the GFE field on the settlement page for now instead of this field, to keep the settlement charges page in sync with the GFE
        /* [DependsOn]
         private E_PercentBaseT sSettlementProMInsT
         {
             get { return (E_PercentBaseT)GetTypeIndexField("sSettlementProMInsT"); }
             set { SetTypeIndexField("sSettlementProMInsT", (int)value); }
         }

         [DependsOn]
         private bool sSettlementProMInsLckd
         {
             get { return GetBoolField("sSettlementProMInsLckd"); }
             set { SetBoolField("sSettlementProMInsLckd", value); }
         }*/

        /*private string x_fhaMonthlyMipListKey_Settlement = null;
        private List<decimal> x_cacheFhaMonthlyMipList_Settlement = null;
        [DependsOn(nameof(sLAmtCalc), nameof(sNoteIR), nameof(sTerm), nameof(sSettlementProMInsR), nameof(sFfUfmipR), nameof(sFfUfmipFinanced), nameof(sFinalLAmt))]
        private int sfGetFhaMonthlyMipList_Settlement
        {
            set { }
        }
        private List<decimal> GetFhaMonthlyMipList_Settlement()
        {
            string key = sLAmtCalc + "::" + sNoteIR + "::" + sTerm + "::" + sSettlementProMInsR + "::" + sFfUfmipR;
            if (key != x_fhaMonthlyMipListKey_Settlement || x_cacheFhaMonthlyMipList_Settlement == null)
            {
                decimal payment = Tools.CalculatePayment(sFinalLAmt, sNoteIR, sTerm);
                decimal upfrontFactor = Math.Round(sFfUfmipFinanced, 2) > 0 ? sFfUfmipR : 0;
                x_cacheFhaMonthlyMipList_Settlement = FhaPremiumCalculator.Calculate(sFinalLAmt, sNoteIR, sSettlementProMInsR, upfrontFactor, payment);
                x_fhaMonthlyMipListKey_Settlement = key;
            }
            return x_cacheFhaMonthlyMipList_Settlement;
        }*/

        /*private string x_fhaManualMonthlyMipListKey_Settlement = null;
        private List<decimal> x_cacheFhaManualMonthlyMipList_Settlement = null;
        private List<decimal> GetFhaManuallyMipList_Settlement()
        {
            string key = sLAmtCalc + "::" + sNoteIR + "::" + sTerm + "::" + sSettlementProMInsR + "::" + sFfUfmipR;

            if (key != x_fhaManualMonthlyMipListKey_Settlement || x_cacheFhaManualMonthlyMipList_Settlement == null)
            {
                // 5/15/2009 dd - OPM 30544 - Since not all investor support the average annual balance calculation define by FHA, we
                // have to support scenario where user override the first year MI. For subsequence year, we recalculate MI based on the 
                // remaining mortgage balance exclude the upfront finance.
                decimal payment = Tools.CalculatePayment(sLAmtCalc, sNoteIR, sTerm);

                if (sLAmtCalc <= 0 || sNoteIR <= 0)
                {
                    throw new CBaseException(ErrorMessages.Generic, "Invalid loan amount or note rate while calculate GetFhaManuallyMipList_Settlement()");
                }
                List<decimal> annualBalance = new List<decimal>();
                int month = 0;
                decimal previousBalance = sLAmtCalc;
                while (previousBalance > 0 && month < 1800)
                {
                    if (month % 12 == 0)
                    {
                        annualBalance.Add(previousBalance);
                    }
                    decimal stepA = Math.Round(previousBalance * sNoteIR, 2);
                    decimal stepB = Math.Round(stepA / 1200, 2);
                    decimal stepC = previousBalance + stepB;
                    previousBalance = stepC - payment;
                    month++;

                }
                x_cacheFhaManualMonthlyMipList_Settlement = new List<decimal>();
                foreach (decimal balance in annualBalance)
                {
                    x_cacheFhaManualMonthlyMipList_Settlement.Add(Math.Round(Math.Round(balance * sSettlementProMInsR, 2) / 1200, 2));
                }

            }
            return x_cacheFhaManualMonthlyMipList_Settlement;
        }*/

        // dd - I have to exclude 'sProMIns'. Otherwise there will be a cycle.
        /*[DependsOn(nameof(sLT), nameof(sfGetFhaMonthlyMipList), nameof(sSettlementProMInsLckd))]
        private int sfGet_sSettlementProMIns_ByMonth
        {
            set { }
        }
        public decimal Get_sSettlementProMIns_ByMonth(int month)
        {
            if (sLT == E_sLT.FHA)
            {
                int yr = month / 12;

                if (sSettlementProMInsLckd)
                {
                    // 5/15/2009 dd - User override sProMIns. This indicate to use user does not want the annual average outstanding balance method.
                    if (sSettlementProMInsR == 0)
                    {
                        return sSettlementProMIns;
                    }
                    else
                    {
                        if (yr == 0)
                        {
                            return sSettlementProMIns; // For 1st year always return value enter by user.
                        }
                        else
                        {
                            // TODO - verify this calculation
                            return GetFhaManuallyMipList_Settlement()[yr];
                        }
                    }
                }
                else
                {
                    // TODO - verify this calculation
                    // 5/15/2009 dd - User annual average outstanding balance calculation.
                    return GetFhaMonthlyMipList_Settlement()[yr];
                }
            }
            else
            {
                return sSettlementProMIns;
            }
        }*/

        // Per PDE, we will use the GFE field on the settlement page for now instead of this field, to keep the settlement charges page in sync with the GFE
        /*[DependsOn(nameof(sSettlementProMIns), nameof(sSettlementProMInsMb), nameof(sSettlementProMInsR), nameof(sSettlementProMInsBaseAmt), nameof(sLAmtCalc), nameof(sNoteIR), nameof(sFfUfmipR), nameof(sLT), nameof(sTerm), nameof(sfGet_sSettlementProMIns_ByMonth), nameof(sSettlementProMInsLckd))]
        private decimal sSettlementProMIns
        {
            get
            {
                if (IsRunningPricingEngine)
                {
                    Tools.LogBug("Unexpected use of sSettlementProMIns field in Pricing Engine (this field should only be used in the Settlement Charges page).");
                    return 0; // value has been pml-transformed into sProOHExpPeval
                }

                if (sSettlementProMInsLckd) // TODO - can we remove this field, it doesn't seem to be used?
                {
                    return GetMoneyField("sSettlementProMIns");
                }
                else
                {
                    if (sLT == E_sLT.FHA)
                    {
                        // 5/4/2009 dd - OPM 28818 For FHA loan the Monthly Mortgage Insurance is calculated based on formula describe by FHA.
                        try
                        {
                            return Get_sSettlementProMIns_ByMonth(0);
                        }
                        catch (PermissionException)
                        {
                            throw;
                        }
                        catch
                        {
                            // 5/11/2009 dd - Get_sProMIns_ByMonth could throw exception when loan amount or rate is invalid.
                            return 0;
                        }
                    }
                    else
                    {
                        return sSettlementProMInsMb + (sSettlementProMInsR * sSettlementProMInsBaseAmt / 1200.00M);
                    }
                }
            }
            set
            {
                SetMoneyField("sSettlementProMIns", value);
            }
        }

        private string sSettlementProMIns_rep
        {
            get { return ToMoneyString(() => sSettlementProMIns); }
            set { sSettlementProMIns = ToMoney(value); }
        }*/

        /*[DependsOn(nameof(sSettlementProMInsT), nameof(sLAmtCalc), nameof(sPurchPrice), nameof(sApprVal), nameof(sFinalLAmt))]
        private decimal sSettlementProMInsBaseAmt
        {
            get
            {
                switch (sSettlementProMInsT)
                {
                    case E_PercentBaseT.LoanAmount: // Loan amount
                        return sLAmtCalc;
                    case E_PercentBaseT.SalesPrice: // sPurchPrice
                        return sPurchPrice;
                    case E_PercentBaseT.AppraisalValue: // Appraisal value
                        return sApprVal;
                    case E_PercentBaseT.TotalLoanAmount:
                        return sFinalLAmt;
                    default:
                        Throw("Error processing sSettlementProMInsT value");
                        return 0; // To make compiler happy.
                }
            }
        }

        private string sSettlementProMInsBaseAmt_rep
        {
            get { return ToMoneyString(() => sSettlementProMInsBaseAmt); }
        }*/

        [DependsOn(nameof(sSettlementMInsRsrvProps), nameof(sMInsRsrvProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementMInsRsrvProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementMInsRsrvProps"), () => sMInsRsrvProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementMInsRsrvProps", value), () => sMInsRsrvProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementMInsRsrvProps_rep
        {
            get { return ToPropsString(() => sSettlementMInsRsrvProps); }
            set { sSettlementMInsRsrvProps = ToProps(value); }
        }

        [DependsOn(nameof(sSettlementMInsRsrvMon), nameof(sProMIns))]
        private decimal sSettlementMInsRsrv
        {
            get { return sSettlementMInsRsrvMon * sProMIns; }
        }
        public string sSettlementMInsRsrv_rep
        {
            get { return ToMoneyString(() => sSettlementMInsRsrv); }
        }

        [DependsOn(nameof(sSettlementMInsRsrvMon), nameof(sMInsRsrvMon), nameof(sUseGFEDataForSCFields))]
        private int sSettlementMInsRsrvMon
        {
            get { return GetSCField(() => GetCountField("sSettlementMInsRsrvMon"), () => sMInsRsrvMon, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetCountField("sSettlementMInsRsrvMon", value), () => sMInsRsrvMon = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementMInsRsrvMon_rep
        {
            get { return ToCountString(() => sSettlementMInsRsrvMon); }
            set { sSettlementMInsRsrvMon = m_convertLos.ToCount(value); }
        }
        #endregion

        #region Line 1004

        // Per PDE, we will use the GFE field on the settlement page for now instead of this field, to keep the settlement charges page in sync with the GFE
        /*[DependsOn(nameof(sSettlementProRealETxR), nameof(sProRealETxBaseAmt), nameof(sSettlementProRealETxMb), nameof(sProRealETxPeval))]
        private decimal sSettlementProRealETx
        {
            get
            {
                if (E_CalcModeT.PriceMyLoan == CalcModeT)
                    return sProRealETxPeval;
                return (sSettlementProRealETxR * sProRealETxBaseAmt / 1200.00M) + sSettlementProRealETxMb;
            }
        }
        private string sSettlementProRealETx_rep
        {
            get { return ToMoneyString(() => sSettlementProRealETx); }
        }*/

        // Per PDE, we will use the GFE field on the settlement page for now instead of this field, to keep the settlement charges page in sync with the GFE
        /*[DependsOn]
        private decimal sSettlementProRealETxR
        {
            get { return GetRateField("sSettlementProRealETxR"); }
            set { SetRateField("sSettlementProRealETxR", value); }
        }

        private string sSettlementProRealETxR_rep
        {
            get { return ToRateString(() => sSettlementProRealETxR); }
            set { sSettlementProRealETxR = ToRate(value); }
        }*/

        // Per PDE, we will use the GFE field on the settlement page for now instead of this field, to keep the settlement charges page in sync with the GFE
        /*[DependsOn]
        private E_PercentBaseT sSettlementProRealETxT
        {
            get { return (E_PercentBaseT)GetTypeIndexField("sSettlementProRealETxT"); }
            set { SetTypeIndexField("sSettlementProRealETxT", (int)value); }
        }

        // Per PDE, we will use the GFE field on the settlement page for now instead of this field, to keep the settlement charges page in sync with the GFE
        [DependsOn]
        private decimal sSettlementProRealETxMb
        {
            get { return GetMoneyField("sSettlementProRealETxMb"); }
            set { SetMoneyField("sSettlementProRealETxMb", value); }
        }
        private string sSettlementProRealETxMb_rep
        {
            get { return ToMoneyString(() => sSettlementProRealETxMb); }
            set { sSettlementProRealETxMb = ToMoney(value); }
        }*/

        [DependsOn(nameof(sSettlementRealETxRsrvMon), nameof(sProRealETx))]
        private decimal sSettlementRealETxRsrv
        {
            get { return sSettlementRealETxRsrvMon * Math.Round(sProRealETx, 2, MidpointRounding.AwayFromZero); }
        }
        public string sSettlementRealETxRsrv_rep
        {
            get { return ToMoneyString(() => sSettlementRealETxRsrv); }
        }

        [DependsOn(nameof(sSettlementRealETxRsrv), nameof(sSettlementRealETxRsrvMon))]
        public decimal sProvExpRealETxInitMoAmt
        {
            get 
            {
                if (sSettlementRealETxRsrvMon == 0)
                    return 0;

                return decimal.Round((sSettlementRealETxRsrv / sSettlementRealETxRsrvMon), 2);
            }
        }

        [DependsOn(nameof(sSettlementRealETxRsrvMon), nameof(sRealETxRsrvMon), nameof(sUseGFEDataForSCFields))]
        private int sSettlementRealETxRsrvMon
        {
            get { return GetSCField(() => GetCountField("sSettlementRealETxRsrvMon"), () => sRealETxRsrvMon, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetCountField("sSettlementRealETxRsrvMon", value), () => sRealETxRsrvMon = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementRealETxRsrvMon_rep
        {
            get { return ToCountString(() => sSettlementRealETxRsrvMon); }
            set { sSettlementRealETxRsrvMon = m_convertLos.ToCount(value); }
        }

        [DependsOn(nameof(sSettlementRealETxRsrvProps), nameof(sRealETxRsrvProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementRealETxRsrvProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementRealETxRsrvProps"), () => sRealETxRsrvProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementRealETxRsrvProps", value), () => sRealETxRsrvProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementRealETxRsrvProps_rep
        {
            get { return ToPropsString(() => sSettlementRealETxRsrvProps); }
            set { sSettlementRealETxRsrvProps = ToProps(value); }
        }

        [DependsOn(nameof(sSettlementRealETxRsrvMon), nameof(sfIsIncludeInEscrow), nameof(sProRealETx))]
        public decimal sSettlementRealETxImpoundAmt
        {
            get
            {
                if (IsIncludeInEscrow(E_EscrowItemT.RealEstateTax)) // opm 180983
                {
                    return decimal.Round(sProRealETx, 2);
                }
                return 0;
            }
        }

        #endregion

        #region Line 1005
        [DependsOn(nameof(sSettlementSchoolTxRsrvMon), nameof(sProSchoolTx))]
        private decimal sSettlementSchoolTxRsrv
        {
            get { return sSettlementSchoolTxRsrvMon * sProSchoolTx; }
        }
        public string sSettlementSchoolTxRsrv_rep
        {
            get { return ToMoneyString(() => sSettlementSchoolTxRsrv); }
        }

        // Per PDE, we will use the GFE field on the settlement page for now instead of this field, to keep the settlement charges page in sync with the GFE
        /*[DependsOn]
        private decimal sSettlementProSchoolTx
        {
            get { return GetMoneyField("sSettlementProSchoolTx"); }
            set { SetMoneyField("sSettlementProSchoolTx", value); }
        }
        private string sSettlementProSchoolTx_rep
        {
            get { return ToMoneyString(() => sSettlementProSchoolTx); }
            set { sSettlementProSchoolTx = ToMoney(value); }
        }*/

        [DependsOn(nameof(sSettlementSchoolTxRsrvMon), nameof(sSchoolTxRsrvMon), nameof(sUseGFEDataForSCFields))]
        private int sSettlementSchoolTxRsrvMon
        {
            get { return GetSCField(() => GetCountField("sSettlementSchoolTxRsrvMon"), () => sSchoolTxRsrvMon, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetCountField("sSettlementSchoolTxRsrvMon", value), () => sSchoolTxRsrvMon = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementSchoolTxRsrvMon_rep
        {
            get { return ToCountString(() => sSettlementSchoolTxRsrvMon); }
            set { sSettlementSchoolTxRsrvMon = m_convertLos.ToCount(value); }
        }

        [DependsOn(nameof(sSettlementSchoolTxRsrvProps), nameof(sSchoolTxRsrvProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementSchoolTxRsrvProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementSchoolTxRsrvProps"), () => sSchoolTxRsrvProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementSchoolTxRsrvProps", value), () => sSchoolTxRsrvProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementSchoolTxRsrvProps_rep
        {
            get { return ToPropsString(() => sSettlementSchoolTxRsrvProps); }
            set { sSettlementSchoolTxRsrvProps = ToProps(value); }
        }

        [DependsOn(nameof(sSettlementSchoolTxRsrvMon), nameof(sfIsIncludeInEscrow), nameof(sProSchoolTx))]
        public decimal sSettlementSchoolTxImpoundAmt
        {
            get
            {
                if (IsIncludeInEscrow(E_EscrowItemT.SchoolTax)) // opm 180983
                {
                    return decimal.Round(sProSchoolTx, 2);
                }
                return 0;
            }
        }

        #endregion

        #region Line 1006

        [DependsOn(nameof(sSettlementFloodInsRsrvMon), nameof(sProFloodIns))]
        private decimal sSettlementFloodInsRsrv
        {
            get { return sSettlementFloodInsRsrvMon * sProFloodIns; }
        }
        public string sSettlementFloodInsRsrv_rep
        {
            get { return ToMoneyString(() => sSettlementFloodInsRsrv); }
        }

        // Per PDE, we will use the GFE field on the settlement page for now instead of this field, to keep the settlement charges page in sync with the GFE
        /*[DependsOn]
        private decimal sSettlementProFloodIns
        {
            get { return GetMoneyField("sSettlementProFloodIns"); }
            set { SetMoneyField("sSettlementProFloodIns", value); }
        }
        private string sSettlementProFloodIns_rep
        {
            get { return ToMoneyString(() => sSettlementProFloodIns); }
            set { sSettlementProFloodIns = ToMoney(value); }
        }*/

        [DependsOn(nameof(sSettlementFloodInsRsrvMon), nameof(sFloodInsRsrvMon), nameof(sUseGFEDataForSCFields))]
        private int sSettlementFloodInsRsrvMon
        {
            get { return GetSCField(() => GetCountField("sSettlementFloodInsRsrvMon"), () => sFloodInsRsrvMon, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetCountField("sSettlementFloodInsRsrvMon", value), () => sFloodInsRsrvMon = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementFloodInsRsrvMon_rep
        {
            get { return ToCountString(() => sSettlementFloodInsRsrvMon); }
            set { sSettlementFloodInsRsrvMon = m_convertLos.ToCount(value); }
        }

        [DependsOn(nameof(sSettlementFloodInsRsrvProps), nameof(sFloodInsRsrvProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementFloodInsRsrvProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementFloodInsRsrvProps"), () => sFloodInsRsrvProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementFloodInsRsrvProps", value), () => sFloodInsRsrvProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementFloodInsRsrvProps_rep
        {
            get { return ToPropsString(() => sSettlementFloodInsRsrvProps); }
            set { sSettlementFloodInsRsrvProps = ToProps(value); }
        }

        #endregion

        #region Line 1007
        /// <summary>
        /// Aggregate Adjustment Reserve
        /// From the spec:
        /// 1007 Aggregate adjustment
        /// This needs to be recalculated based on the (potentially changed) 
        /// numbers from the Settlement Charges page instead of the ones on the GFE. 
        /// If the lender has locked the value, honor the lock.
        /// </summary>
        [DependsOn(nameof(sSettlementAggregateAdjRsrv), nameof(sAggregateEscrowAccount), nameof(sSettlementAggregateAdjRsrvLckd), nameof(sAggregateAdjRsrv), nameof(sUseGFEDataForSCFields), nameof(sSettlementAggregateEscrowAccount))]
        private decimal sSettlementAggregateAdjRsrv
        {
            get
            {
                if (sSettlementAggregateAdjRsrvLckd || sUseGFEDataForSCFields)
                {
                    return GetSCField(() => GetMoneyField("sSettlementAggregateAdjRsrv"), () => sAggregateAdjRsrv, sUseGFEDataForSCFields);
                }
                else
                {
                    return sSettlementAggregateEscrowAccount.AggregateEscrowAdjustment;
                }
            }
            set { SetSCField(() => SetMoneyField("sSettlementAggregateAdjRsrv", value), () => sAggregateAdjRsrv = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementAggregateAdjRsrv_rep
        {
            get { return ToMoneyString(() => sSettlementAggregateAdjRsrv); }
            set { sSettlementAggregateAdjRsrv = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementAggregateAdjRsrv))]
        public decimal sProvExpSettlementAggregateAdjRsrv
        {
            get { return decimal.Round(sSettlementAggregateAdjRsrv, 2); }
        }

        [DependsOn(nameof(sSettlementAggregateAdjRsrvLckd), nameof(sAggregateAdjRsrvLckd), nameof(sUseGFEDataForSCFields))]
        public bool sSettlementAggregateAdjRsrvLckd
        {
            get { return GetSCField(() => GetBoolField("sSettlementAggregateAdjRsrvLckd"), () => sAggregateAdjRsrvLckd, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetBoolField("sSettlementAggregateAdjRsrvLckd", value), () => sAggregateAdjRsrvLckd = value, sUseGFEDataForSCFields); }
        }


        [DependsOn(nameof(sSettlementAggregateAdjRsrvProps), nameof(sAggregateAdjRsrvProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementAggregateAdjRsrvProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementAggregateAdjRsrvProps"), () => sAggregateAdjRsrvProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementAggregateAdjRsrvProps", value), () => sAggregateAdjRsrvProps = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementAggregateAdjRsrvProps_rep
        {
            get { return ToPropsString(() => sSettlementAggregateAdjRsrvProps); }
            set { sSettlementAggregateAdjRsrvProps = ToProps(value); }
        }

        #endregion

        #region Line 1008

        // Per PDE, we will use the GFE field on the settlement page for now instead of this field, to keep the settlement charges page in sync with the GFE
        /*[DependsOn]
        private string sSettlement1008ProHExpDesc
        {
            get { return GetStringVarCharField("sSettlement1008ProHExpDesc"); }
            set { SetStringVarCharField("sSettlement1008ProHExpDesc", value); }
        }*/

        // Per PDE, we will use the GFE s1006ProHExp field in this calculation so that the data will be in sync with the gfe
        [DependsOn(nameof(sSettlement1008RsrvMon), nameof(s1006ProHExp))]
        private decimal sSettlement1008Rsrv
        {
            get { return sSettlement1008RsrvMon * s1006ProHExp; }
        }
        public string sSettlement1008Rsrv_rep
        {
            get { return ToMoneyString(() => sSettlement1008Rsrv); }
        }

        [DependsOn(nameof(sSettlement1008RsrvMon), nameof(s1006RsrvMon), nameof(sUseGFEDataForSCFields))]
        private int sSettlement1008RsrvMon
        {
            get { return GetSCField(() => GetCountField("sSettlement1008RsrvMon"), () => s1006RsrvMon, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetCountField("sSettlement1008RsrvMon", value), () => s1006RsrvMon = value, sUseGFEDataForSCFields); }
        }
        public string sSettlement1008RsrvMon_rep
        {
            get { return ToCountString(() => sSettlement1008RsrvMon); }
            set { sSettlement1008RsrvMon = m_convertLos.ToCount(value); }
        }

        // Per PDE, we will use the GFE field on the settlement page for now instead of this field, to keep the settlement charges page in sync with the GFE
        /*[DependsOn]
        private decimal sSettlement1008ProHExp
        {
            get { return GetMoneyField("sSettlement1008ProHExp"); }
            set { SetMoneyField("sSettlement1008ProHExp", value); }
        }
        private string sSettlement1008ProHExp_rep
        {
            get { return ToMoneyString(() => sSettlement1008ProHExp); }
            set { sSettlement1008ProHExp = ToMoney(value); }
        }*/

        [DependsOn(nameof(sSettlement1008RsrvProps), nameof(s1006RsrvProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlement1008RsrvProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlement1008RsrvProps"), () => s1006RsrvProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlement1008RsrvProps", value), () => s1006RsrvProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlement1008RsrvProps_rep
        {
            get { return ToPropsString(() => sSettlement1008RsrvProps); }
            set { sSettlement1008RsrvProps = ToProps(value); }
        }

        [DependsOn(nameof(sTaxTable1008DescT), nameof(sSettlement1008RsrvMon), nameof(sSettlement1009RsrvMon), nameof(s1006ProHExp), nameof(s1007ProHExp), nameof(sfIsIncludeInEscrow), nameof(sSettlementU3RsrvMon), nameof(sProU3Rsrv), nameof(sSettlementU4RsrvMon), nameof(sProU4Rsrv))]
        public decimal sTaxTableTaxDDL1ImpoundAmt
        {
            get
            {
                switch (sTaxTable1008DescT)
                {
                    case E_TableSettlementDescT.Use1008DescT:
                        if (IsIncludeInEscrow(E_EscrowItemT.UserDefine1)) // scrapping the 0 checks in 180983
                        {
                            return decimal.Round(s1006ProHExp, 2);
                        }
                        return 0;
                    case E_TableSettlementDescT.Use1009DescT:
                        if (IsIncludeInEscrow(E_EscrowItemT.UserDefine2))
                        {
                            return decimal.Round(s1007ProHExp, 2);
                        }
                        return 0;
                    case E_TableSettlementDescT.Use1010DescT:
                        if (IsIncludeInEscrow(E_EscrowItemT.UserDefine3))
                        {
                            return decimal.Round(sProU3Rsrv, 2);
                        }
                        return 0;
                    case E_TableSettlementDescT.Use1011DescT:
                        if (IsIncludeInEscrow(E_EscrowItemT.UserDefine4))
                        {
                            return decimal.Round(sProU4Rsrv, 2);
                        }
                        return 0;
                    case E_TableSettlementDescT.NoInfo:
                    default: return 0;
                    
                }
            }
        }

        [DependsOn(nameof(sTaxTable1008DescT), nameof(s1006ProHExp), nameof(s1007ProHExp), nameof(sProU3Rsrv), nameof(sProU4Rsrv))]
        public decimal sTaxTableTaxDDL1InitMonAmt
        {
            get
            {
                switch (sTaxTable1008DescT)
                {
                    case E_TableSettlementDescT.Use1008DescT: return decimal.Round(s1006ProHExp, 2);
                    case E_TableSettlementDescT.Use1009DescT: return decimal.Round(s1007ProHExp, 2);
                    case E_TableSettlementDescT.Use1010DescT: return decimal.Round(sProU3Rsrv, 2);
                    case E_TableSettlementDescT.Use1011DescT: return decimal.Round(sProU4Rsrv, 2);
                    case E_TableSettlementDescT.NoInfo:
                    default: return 0;

                }
            }
        }

        [DependsOn(nameof(sTaxTable1008DescT), nameof(sSettlement1009RsrvMon), nameof(sSettlement1008RsrvMon), nameof(sSettlementU3RsrvMon), nameof(sSettlementU4RsrvMon))]
        public int sTaxTableTaxDDL1InitMonInEscrow
        {
            get
            {
                switch (sTaxTable1008DescT)
                {
                    case E_TableSettlementDescT.Use1008DescT: return sSettlement1008RsrvMon;
                    case E_TableSettlementDescT.Use1009DescT: return sSettlement1009RsrvMon;
                    case E_TableSettlementDescT.Use1010DescT: return sSettlementU3RsrvMon;
                    case E_TableSettlementDescT.Use1011DescT: return sSettlementU4RsrvMon;
                    case E_TableSettlementDescT.NoInfo:
                    default: return 0;
                }
            }
        }

        #endregion

        #region Line 1009

        // Per PDE, we will use the GFE field on the settlement page for now instead of this field, to keep the settlement charges page in sync with the GFE
        /*[DependsOn]
        private string sSettlement1009ProHExpDesc
        {
            get { return GetStringVarCharField("sSettlement1009ProHExpDesc"); }
            set { SetStringVarCharField("sSettlement1009ProHExpDesc", value); }
        }*/

        [DependsOn(nameof(sSettlement1009RsrvMon), nameof(s1007ProHExp))]
        private decimal sSettlement1009Rsrv
        {
            get { return sSettlement1009RsrvMon * s1007ProHExp; }
        }
        public string sSettlement1009Rsrv_rep
        {
            get { return ToMoneyString(() => sSettlement1009Rsrv); }
        }

        [DependsOn(nameof(sSettlement1009RsrvMon), nameof(s1007RsrvMon), nameof(sUseGFEDataForSCFields))]
        private int sSettlement1009RsrvMon
        {
            get { return GetSCField(() => GetCountField("sSettlement1009RsrvMon"), () => s1007RsrvMon, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetCountField("sSettlement1009RsrvMon", value), () => s1007RsrvMon = value, sUseGFEDataForSCFields); }
        }
        public string sSettlement1009RsrvMon_rep
        {
            get { return ToCountString(() => sSettlement1009RsrvMon); }
            set { sSettlement1009RsrvMon = m_convertLos.ToCount(value); }
        }

        // Per PDE, we will use the GFE field on the settlement page for now instead of this field, to keep the settlement charges page in sync with the GFE
        /*[DependsOn]
        private decimal sSettlement1009ProHExp
        {
            get { return GetMoneyField("sSettlement1009ProHExp"); }
            set { SetMoneyField("sSettlement1009ProHExp", value); }
        }
        private string sSettlement1009ProHExp_rep
        {
            get { return ToMoneyString(() => sSettlement1009ProHExp); }
            set { sSettlement1009ProHExp = ToMoney(value); }
        }*/

        [DependsOn(nameof(sSettlement1009RsrvProps), nameof(s1007RsrvProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlement1009RsrvProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlement1009RsrvProps"), () => s1007RsrvProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlement1009RsrvProps", value), () => s1007RsrvProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlement1009RsrvProps_rep
        {
            get { return ToPropsString(() => sSettlement1009RsrvProps); }
            set { sSettlement1009RsrvProps = ToProps(value); }
        }

        [DependsOn(nameof(sTaxTable1009DescT), nameof(sSettlement1008RsrvMon), nameof(sSettlement1009RsrvMon), nameof(s1006ProHExp), nameof(s1007ProHExp), nameof(sfIsIncludeInEscrow), nameof(sSettlementU3RsrvMon), nameof(sProU3Rsrv), nameof(sSettlementU4RsrvMon), nameof(sProU4Rsrv))]
        public decimal sTaxTableTaxDDL2ImpoundAmt
        {
            get
            {
                switch (sTaxTable1009DescT)
                {
                    case E_TableSettlementDescT.Use1008DescT:
                        if (IsIncludeInEscrow(E_EscrowItemT.UserDefine1)) // scrapping the 0 checks in 180983
                        {
                            return decimal.Round(s1006ProHExp, 2);
                        }
                        return 0;
                    case E_TableSettlementDescT.Use1009DescT:
                        if (IsIncludeInEscrow(E_EscrowItemT.UserDefine2))
                        {
                            return decimal.Round(s1007ProHExp, 2);
                        }
                        return 0;
                    case E_TableSettlementDescT.Use1010DescT:
                        if (IsIncludeInEscrow(E_EscrowItemT.UserDefine3))
                        {
                            return decimal.Round(sProU3Rsrv, 2);
                        }
                        return 0;
                    case E_TableSettlementDescT.Use1011DescT:
                        if (IsIncludeInEscrow(E_EscrowItemT.UserDefine4))
                        {
                            return decimal.Round(sProU4Rsrv, 2);
                        }
                        return 0;
                    case E_TableSettlementDescT.NoInfo:
                    default: return 0;

                }
            }
        }

        [DependsOn(nameof(sTaxTable1009DescT), nameof(s1006ProHExp), nameof(s1007ProHExp), nameof(sProU3Rsrv), nameof(sProU4Rsrv))]
        public decimal sTaxTableTaxDDL2InitMonAmt
        {
            get
            {
                switch (sTaxTable1009DescT)
                {
                    case E_TableSettlementDescT.Use1008DescT: return decimal.Round(s1006ProHExp, 2);
                    case E_TableSettlementDescT.Use1009DescT: return decimal.Round(s1007ProHExp, 2);
                    case E_TableSettlementDescT.Use1010DescT: return decimal.Round(sProU3Rsrv, 2);
                    case E_TableSettlementDescT.Use1011DescT: return decimal.Round(sProU4Rsrv, 2);
                    case E_TableSettlementDescT.NoInfo:
                    default: return 0;
                }
            }
        }

        [DependsOn(nameof(sTaxTable1009DescT), nameof(sSettlement1009RsrvMon), nameof(sSettlement1008RsrvMon), nameof(sSettlementU3RsrvMon), nameof(sSettlementU4RsrvMon))]
        public int sTaxTableTaxDDL2InitMonInEscrow
        {
            get
            {
                switch (sTaxTable1009DescT)
                {
                    case E_TableSettlementDescT.Use1008DescT: return sSettlement1008RsrvMon;
                    case E_TableSettlementDescT.Use1009DescT: return sSettlement1009RsrvMon;
                    case E_TableSettlementDescT.Use1010DescT: return sSettlementU3RsrvMon;
                    case E_TableSettlementDescT.Use1011DescT: return sSettlementU4RsrvMon;
                    case E_TableSettlementDescT.NoInfo:
                    default: return 0;
                }
            }
        }

        #endregion

        #region Line 1010
        [DependsOn(nameof(sSettlementU3RsrvMon), nameof(sProU3Rsrv))]
        private decimal sSettlementU3Rsrv
        {
            get { return sSettlementU3RsrvMon * sProU3Rsrv; }
        }
        public string sSettlementU3Rsrv_rep
        {
            get { return ToMoneyString(() => sSettlementU3Rsrv); }
        }

        [DependsOn(nameof(sSettlementU3RsrvMon), nameof(sU3RsrvMon), nameof(sUseGFEDataForSCFields))]
        private int sSettlementU3RsrvMon
        {
            get { return GetSCField(() => BrokerDB.EnableAdditionalSection1000CustomFees ? GetCountField("sSettlementU3RsrvMon") : 0, () => sU3RsrvMon, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetCountField("sSettlementU3RsrvMon", value), () => sU3RsrvMon = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementU3RsrvMon_rep
        {
            get { return ToCountString(() => sSettlementU3RsrvMon); }
            set { sSettlementU3RsrvMon = m_convertLos.ToCount(value); }
        }

        [DependsOn(nameof(sSettlementU3RsrvProps), nameof(sU3RsrvProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementU3RsrvProps
        {
            get { return GetSCField(() => BrokerDB.EnableAdditionalSection1000CustomFees ? Get32BitPropsField("sSettlementU3RsrvProps") : 0, () => sU3RsrvProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementU3RsrvProps", value), () => sU3RsrvProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementU3RsrvProps_rep
        {
            get { return ToPropsString(() => sSettlementU3RsrvProps); }
            set { sSettlementU3RsrvProps = ToProps(value); }
        }
        #endregion

        #region Line 1011
        [DependsOn(nameof(sSettlementU4RsrvMon), nameof(sProU4Rsrv))]
        private decimal sSettlementU4Rsrv
        {
            get { return sSettlementU4RsrvMon * sProU4Rsrv; }
        }
        public string sSettlementU4Rsrv_rep
        {
            get { return ToMoneyString(() => sSettlementU4Rsrv); }
        }

        [DependsOn(nameof(sSettlementU4RsrvMon), nameof(sU4RsrvMon), nameof(sUseGFEDataForSCFields))]
        private int sSettlementU4RsrvMon
        {
            get { return GetSCField(() => BrokerDB.EnableAdditionalSection1000CustomFees ? GetCountField("sSettlementU4RsrvMon") : 0, () => sU4RsrvMon, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetCountField("sSettlementU4RsrvMon", value), () => sU4RsrvMon = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementU4RsrvMon_rep
        {
            get { return ToCountString(() => sSettlementU4RsrvMon); }
            set { sSettlementU4RsrvMon = m_convertLos.ToCount(value); }
        }

        [DependsOn(nameof(sSettlementU4RsrvProps), nameof(sU4RsrvProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementU4RsrvProps
        {
            get { return GetSCField(() => BrokerDB.EnableAdditionalSection1000CustomFees ? Get32BitPropsField("sSettlementU4RsrvProps") : 0, () => sU4RsrvProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementU4RsrvProps", value), () => sU4RsrvProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementU4RsrvProps_rep
        {
            get { return ToPropsString(() => sSettlementU4RsrvProps); }
            set { sSettlementU4RsrvProps = ToProps(value); }
        }
        #endregion

        #endregion

        #region Section 1100 - TITLE CHARGES

        #region Line 1102
        [DependsOn(nameof(sSettlementEscrowF), nameof(sEscrowF), nameof(sUseGFEDataForSCFields))]
        private decimal sSettlementEscrowF
        {
            get { return GetSCField(() => GetMoneyField("sSettlementEscrowF"), () => sEscrowF, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementEscrowF", value), () => sEscrowF = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementEscrowF_rep
        {
            get { return ToMoneyString(() => sSettlementEscrowF); }
            set { sSettlementEscrowF = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementEscrowFProps), nameof(sEscrowFProps), nameof(sUseGFEDataForSCFields), nameof(sfGetAgentOfRole), nameof(sfGetPropsUpdatedWithLinkedProps))]
        public int sSettlementEscrowFProps
        {
            get { return GetSCField(() =>
            {
                int props = Get32BitPropsField("sSettlementEscrowFProps");

                // OPM 122786 - Use closing agent if there is no escrow agent on file.
                var agentRoleT = GetAgentOfRole(
                    E_AgentRoleT.Escrow,
                    E_ReturnOptionIfNotExist.ReturnEmptyObject) == CAgentFields.Empty ?
                    E_AgentRoleT.ClosingAgent : E_AgentRoleT.Escrow;

                props = GetPropsUpdatedWithLinkedProps(props, agentRoleT);

                return props;
            }, () => sEscrowFProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementEscrowFProps", value), () => sEscrowFProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementEscrowFProps_rep
        {
            get { return ToPropsString(() => sSettlementEscrowFProps); }
            set { sSettlementEscrowFProps = ToProps(value); }
        }

        #endregion

        #region Line 1103
        [DependsOn(nameof(sSettlementOwnerTitleInsF), nameof(sOwnerTitleInsF), nameof(sUseGFEDataForSCFields))]
        private decimal sSettlementOwnerTitleInsF
        {
            get { return GetSCField(() => GetMoneyField("sSettlementOwnerTitleInsF"), () => sOwnerTitleInsF, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementOwnerTitleInsF", value), () => sOwnerTitleInsF = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementOwnerTitleInsF_rep
        {
            get { return ToMoneyString(() => sSettlementOwnerTitleInsF); }
            set { sSettlementOwnerTitleInsF = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementOwnerTitleInsFProps), nameof(sOwnerTitleInsProps), nameof(sUseGFEDataForSCFields), nameof(sLPurposeT), nameof(sfGetPropsUpdatedWithLinkedProps))]
        public int sSettlementOwnerTitleInsFProps
        {
            get { return GetSCField(() => 
                {
                    var dbProps = Get32BitPropsField("sSettlementOwnerTitleInsFProps");
                    // OPM 122786 - Mimic line 1104 for purchase loans.
                    if (E_sLPurposeT.Purchase == sLPurposeT)
                    {
                        dbProps = GetPropsUpdatedWithLinkedProps(dbProps, E_AgentRoleT.Title);
                    }
                    return dbProps;
                }
            , () => sOwnerTitleInsProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementOwnerTitleInsFProps", value), () => sOwnerTitleInsProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementOwnerTitleInsFProps_rep
        {
            get { return ToPropsString(() => sSettlementOwnerTitleInsFProps); }
            set { sSettlementOwnerTitleInsFProps = ToProps(value); }
        }

        #endregion

        #region Line 1104
        [DependsOn(nameof(sSettlementTitleInsF), nameof(sTitleInsF), nameof(sUseGFEDataForSCFields))]
        private decimal sSettlementTitleInsF
        {
            get { return GetSCField(() => GetMoneyField("sSettlementTitleInsF"), () => sTitleInsF, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementTitleInsF", value), () => sTitleInsF = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementTitleInsF_rep
        {
            get { return ToMoneyString(() => sSettlementTitleInsF); }
            set { sSettlementTitleInsF = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementTitleInsFProps), nameof(sTitleInsFProps), nameof(sUseGFEDataForSCFields), nameof(sfGetPropsUpdatedWithLinkedProps))]
        public int sSettlementTitleInsFProps
        {
            get { return GetSCField(() =>
                {
                    int props =Get32BitPropsField("sSettlementTitleInsFProps");
                    
                    props = GetPropsUpdatedWithLinkedProps(props, E_AgentRoleT.Title);

                    return props;
                }
                , () => sTitleInsFProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementTitleInsFProps", value), () => sTitleInsFProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementTitleInsFProps_rep
        {
            get { return ToPropsString(() => sSettlementTitleInsFProps); }
            set { sSettlementTitleInsFProps = ToProps(value); }
        }

        #endregion

        #region Line 1109
        [DependsOn(nameof(sSettlementDocPrepF), nameof(sDocPrepF), nameof(sUseGFEDataForSCFields))]
        private decimal sSettlementDocPrepF
        {
            get { return GetSCField(() => GetMoneyField("sSettlementDocPrepF"), () => sDocPrepF, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementDocPrepF", value), () => sDocPrepF = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementDocPrepF_rep
        {
            get { return ToMoneyString(() => sSettlementDocPrepF); }
            set { sSettlementDocPrepF = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementDocPrepFProps), nameof(sDocPrepFProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementDocPrepFProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementDocPrepFProps"), () => sDocPrepFProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementDocPrepFProps", value), () => sDocPrepFProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementDocPrepFProps_rep
        {
            get { return ToPropsString(() => sSettlementDocPrepFProps); }
            set { sSettlementDocPrepFProps = ToProps(value); }
        }

        #endregion

        #region Line 1110
        [DependsOn(nameof(sSettlementNotaryF), nameof(sNotaryF), nameof(sUseGFEDataForSCFields))]
        private decimal sSettlementNotaryF
        {
            get { return GetSCField(() => GetMoneyField("sSettlementNotaryF"), () => sNotaryF, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementNotaryF", value), () => sNotaryF = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementNotaryF_rep
        {
            get { return ToMoneyString(() => sSettlementNotaryF); }
            set { sSettlementNotaryF = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementNotaryFProps), nameof(sNotaryFProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementNotaryFProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementNotaryFProps"), () => sNotaryFProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementNotaryFProps", value), () => sNotaryFProps = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementNotaryFProps_rep
        {
            get { return ToPropsString(() => sSettlementNotaryFProps); }
            set { sSettlementNotaryFProps = ToProps(value); }
        }

        #endregion

        #region Line 1111
        [DependsOn(nameof(sSettlementAttorneyF), nameof(sAttorneyF), nameof(sUseGFEDataForSCFields))]
        private decimal sSettlementAttorneyF
        {
            get { return GetSCField(() => GetMoneyField("sSettlementAttorneyF"), () => sAttorneyF, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementAttorneyF", value), () => sAttorneyF = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementAttorneyF_rep
        {
            get { return ToMoneyString(() => sSettlementAttorneyF); }
            set { sSettlementAttorneyF = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementAttorneyFProps), nameof(sAttorneyFProps), nameof(sUseGFEDataForSCFields), nameof(sfGetPropsUpdatedWithLinkedProps))]
        public int sSettlementAttorneyFProps
        {
            get { return GetSCField(() => 
                {
                    int props = Get32BitPropsField("sSettlementAttorneyFProps");
                    
                    props = GetPropsUpdatedWithLinkedProps(props, E_AgentRoleT.BuyerAttorney);

                    return props;
                }
                , () => sAttorneyFProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementAttorneyFProps", value), () => sAttorneyFProps = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementAttorneyFProps_rep
        {
            get { return ToPropsString(() => sSettlementAttorneyFProps); }
            set { sSettlementAttorneyFProps = ToProps(value); }
        }

        #endregion

        #region Line 1112
        [DependsOn(nameof(sSettlementU1Tc), nameof(sU1Tc), nameof(sUseGFEDataForSCFields))]
        private decimal sSettlementU1Tc
        {
            get { return GetSCField(() => GetMoneyField("sSettlementU1Tc"), () => sU1Tc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementU1Tc", value), () => sU1Tc = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementU1Tc_rep
        {
            get { return ToMoneyString(() => sSettlementU1Tc); }
            set { sSettlementU1Tc = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementU1TcProps), nameof(sU1TcProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementU1TcProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementU1TcProps"), () => sU1TcProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementU1TcProps", value), () => sU1TcProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementU1TcProps_rep
        {
            get { return ToPropsString(() => sSettlementU1TcProps); }
            set { sSettlementU1TcProps = ToProps(value); }
        }

        [DependsOn(nameof(sSettlementU1TcDesc), nameof(sU1TcDesc), nameof(sUseGFEDataForSCFields))]
        public string sSettlementU1TcDesc
        {
            get { return GetSCField(() => GetLongTextField("sSettlementU1TcDesc"), () => sU1TcDesc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetLongTextField("sSettlementU1TcDesc", value), () => sU1TcDesc = value, sUseGFEDataForSCFields); }
        }

        #endregion

        #region Line 1113
        [DependsOn(nameof(sSettlementU2Tc), nameof(sU2Tc), nameof(sUseGFEDataForSCFields))]
        private decimal sSettlementU2Tc
        {
            get { return GetSCField(() => GetMoneyField("sSettlementU2Tc"), () => sU2Tc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementU2Tc", value), () => sU2Tc = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementU2Tc_rep
        {
            get { return ToMoneyString(() => sSettlementU2Tc); }
            set { sSettlementU2Tc = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementU2TcProps), nameof(sU2TcProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementU2TcProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementU2TcProps"), () => sU2TcProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementU2TcProps", value), () => sU2TcProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementU2TcProps_rep
        {
            get { return ToPropsString(() => sSettlementU2TcProps); }
            set { sSettlementU2TcProps = ToProps(value); }
        }

        [DependsOn(nameof(sSettlementU2TcDesc), nameof(sU2TcDesc), nameof(sUseGFEDataForSCFields))]
        public string sSettlementU2TcDesc
        {
            get { return GetSCField(() => GetLongTextField("sSettlementU2TcDesc"), () => sU2TcDesc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetLongTextField("sSettlementU2TcDesc", value), () => sU2TcDesc = value, sUseGFEDataForSCFields); }
        }

        #endregion

        #region Line 1114
        [DependsOn(nameof(sSettlementU3Tc), nameof(sU3Tc), nameof(sUseGFEDataForSCFields))]
        private decimal sSettlementU3Tc
        {
            get { return GetSCField(() => GetMoneyField("sSettlementU3Tc"), () => sU3Tc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementU3Tc", value), () => sU3Tc = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementU3Tc_rep
        {
            get { return ToMoneyString(() => sSettlementU3Tc); }
            set { sSettlementU3Tc = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementU3TcProps), nameof(sU3TcProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementU3TcProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementU3TcProps"), () => sU3TcProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementU3TcProps", value), () => sU3TcProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementU3TcProps_rep
        {
            get { return ToPropsString(() => sSettlementU3TcProps); }
            set { sSettlementU3TcProps = ToProps(value); }
        }

        [DependsOn(nameof(sSettlementU3TcDesc), nameof(sU3TcDesc), nameof(sUseGFEDataForSCFields))]
        public string sSettlementU3TcDesc
        {
            get { return GetSCField(() => GetLongTextField("sSettlementU3TcDesc"), () => sU3TcDesc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetLongTextField("sSettlementU3TcDesc", value), () => sU3TcDesc = value, sUseGFEDataForSCFields); }
        }

        #endregion

        #region Line 1115
        [DependsOn(nameof(sSettlementU4Tc), nameof(sU4Tc), nameof(sUseGFEDataForSCFields))]
        private decimal sSettlementU4Tc
        {
            get { return GetSCField(() => GetMoneyField("sSettlementU4Tc"), () => sU4Tc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementU4Tc", value), () => sU4Tc = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementU4Tc_rep
        {
            get { return ToMoneyString(() => sSettlementU4Tc); }
            set { sSettlementU4Tc = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementU4TcProps), nameof(sU4TcProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementU4TcProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementU4TcProps"), () => sU4TcProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementU4TcProps", value), () => sU4TcProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementU4TcProps_rep
        {
            get { return ToPropsString(() => sSettlementU4TcProps); }
            set { sSettlementU4TcProps = ToProps(value); }
        }

        [DependsOn(nameof(sSettlementU4TcDesc), nameof(sU4TcDesc), nameof(sUseGFEDataForSCFields))]
        public string sSettlementU4TcDesc
        {
            get { return GetSCField(() => GetLongTextField("sSettlementU4TcDesc"), () => sU4TcDesc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetLongTextField("sSettlementU4TcDesc", value), () => sU4TcDesc = value, sUseGFEDataForSCFields); }
        }

        #endregion

        [DependsOn(nameof(sSettlementEscrowF), nameof(sSettlementOwnerTitleInsF), nameof(sSettlementTitleInsF), nameof(sSettlementDocPrepF), nameof(sSettlementNotaryF), nameof(sSettlementAttorneyF), nameof(sSettlementU1Tc), nameof(sSettlementU2Tc), nameof(sSettlementU3Tc), nameof(sSettlementU4Tc))]
        private decimal sSettlementTotalTitleCharges
        {
            get
            {
                return SumMoney(sSettlementEscrowF,sSettlementOwnerTitleInsF,sSettlementTitleInsF,sSettlementDocPrepF,sSettlementNotaryF,sSettlementAttorneyF,sSettlementU1Tc,sSettlementU2Tc,sSettlementU3Tc,sSettlementU4Tc);
            }
        }

        public string sSettlementTotalTitleCharges_rep
        {
            get { return ToMoneyString(() => sSettlementTotalTitleCharges); }
        }

        #endregion

        #region Section 1200 - GOVERNMENT RECORDING & TRANSFER CHARGES

        #region Line 1201
        [DependsOn(nameof(sSettlementRecBaseT), nameof(sSettlementRecFPc), nameof(sSettlementRecFMb), nameof(sfCalculateTotalAmount), nameof(sSettlementRecDeed), nameof(sSettlementRecMortgage), nameof(sSettlementRecRelease), nameof(sSettlementRecFLckd), nameof(sU1GovRtcSettlementSection), nameof(sU2GovRtcSettlementSection), nameof(sU3GovRtcSettlementSection), nameof(sSettlementU1GovRtc), nameof(sSettlementU2GovRtc), nameof(sSettlementU3GovRtc))]
        public decimal sSettlementRecF
        {
            get
            {
                if (sSettlementRecFLckd)
                {
                    return CalculateTotalAmountForCC(sSettlementRecFPc, sSettlementRecBaseT, sSettlementRecFMb);
                }

                decimal sum = sSettlementRecDeed + sSettlementRecMortgage + sSettlementRecRelease;
                //if (sU1GovRtcSettlementSection == E_GfeSectionT.B7) sum += sSettlementU1GovRtc;
                //if (sU2GovRtcSettlementSection == E_GfeSectionT.B7) sum += sSettlementU2GovRtc;
                //if (sU3GovRtcSettlementSection == E_GfeSectionT.B7) sum += sSettlementU3GovRtc;

                return sum;
            }
        }
        public string sSettlementRecF_rep
        {
            get { return ToMoneyString(() => sSettlementRecF); }
        }

        [DependsOn(nameof(sSettlementRecFMb), nameof(sRecFMb), nameof(sUseGFEDataForSCFields))]
        public decimal sSettlementRecFMb
        {
            get { return GetSCField(() => GetMoneyField("sSettlementRecFMb"), () => sRecFMb, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementRecFMb", value), () => sRecFMb = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementRecFMb_rep
        {
            get { return ToMoneyString(() => sSettlementRecFMb); }
            set { sSettlementRecFMb = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementRecFPc), nameof(sRecFPc), nameof(sUseGFEDataForSCFields))]
        public decimal sSettlementRecFPc
        {
            get { return GetSCField(() => GetRateField("sSettlementRecFPc"), () => sRecFPc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetRateField("sSettlementRecFPc", value), () => sRecFPc = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementRecFPc_rep
        {
            get { return ToRateString(() => sSettlementRecFPc); }
            set { sSettlementRecFPc = ToRate(value); }
        }

        [DependsOn(nameof(sSettlementRecBaseT), nameof(sRecBaseT), nameof(sUseGFEDataForSCFields))]
        public E_PercentBaseT sSettlementRecBaseT
        {
            get { return (E_PercentBaseT)GetSCField(() => GetTypeIndexField("sSettlementRecBaseT"), () => (int)sRecBaseT, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetTypeIndexField("sSettlementRecBaseT", (int)value), () => sRecBaseT = value, sUseGFEDataForSCFields); }
        }

        [DependsOn(nameof(sSettlementRecFProps), nameof(sRecFProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementRecFProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementRecFProps"), () => sRecFProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementRecFProps", value), () => sRecFProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementRecFProps_rep
        {
            get { return ToPropsString(() => sSettlementRecFProps); }
            set { sSettlementRecFProps = ToProps(value); }
        }

        [DependsOn(nameof(sSettlementRecDeed), nameof(sRecDeed), nameof(sUseGFEDataForSCFields))]
        [DevNote("Settlement charges deed recording fee.")]
        public decimal sSettlementRecDeed
        {
            get { return GetSCField(() => GetMoneyField("sSettlementRecDeed"), () => sRecDeed, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementRecDeed", value), () => sRecDeed = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementRecDeed_rep
        {
            get { return ToMoneyString(() => sSettlementRecDeed); }
            set { sSettlementRecDeed = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementRecMortgage), nameof(sRecMortgage), nameof(sUseGFEDataForSCFields))]
        [DevNote("Settlement charges mortgage recording fee.")]
        public decimal sSettlementRecMortgage
        {
            get { return GetSCField(() => GetMoneyField("sSettlementRecMortgage"), () => sRecMortgage, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementRecMortgage", value), () => sRecMortgage = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementRecMortgage_rep
        {
            get { return ToMoneyString(() => sSettlementRecMortgage); }
            set { sSettlementRecMortgage = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementRecRelease), nameof(sRecRelease), nameof(sUseGFEDataForSCFields))]
        [DevNote("Settlement charges release recording fee.")]
        public decimal sSettlementRecRelease
        {
            get { return GetSCField(() => GetMoneyField("sSettlementRecRelease"), () => sRecRelease, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementRecRelease", value), () => sRecRelease = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementRecRelease_rep
        {
            get { return ToMoneyString(() => sSettlementRecRelease); }
            set { sSettlementRecRelease = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementRecFLckd), nameof(sRecFLckd), nameof(sUseGFEDataForSCFields))]
        [DevNote("Settlement charges recording fee lock.")]
        public bool sSettlementRecFLckd
        {
            get { return GetSCField(() => GetBoolField("sSettlementRecFLckd"), () => sRecFLckd, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetBoolField("sSettlementRecFLckd", value), () => sRecFLckd = value, sUseGFEDataForSCFields); }
        }
        #endregion

        #region Line 1204

        [DependsOn(nameof(sSettlementCountyRtcPc), nameof(sCountyRtcPc), nameof(sUseGFEDataForSCFields))]
        public decimal sSettlementCountyRtcPc
        {
            get { return GetSCField(() => GetRateField("sSettlementCountyRtcPc"), () => sCountyRtcPc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetRateField("sSettlementCountyRtcPc", value), () => sCountyRtcPc = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementCountyRtcPc_rep
        {
            get { return ToRateString(() => sSettlementCountyRtcPc); }
            set { sSettlementCountyRtcPc = ToRate(value); }
        }

        [DependsOn(nameof(sSettlementCountyRtcBaseT), nameof(sSettlementCountyRtcPc), nameof(sSettlementCountyRtcMb), nameof(sfCalculateTotalAmount))]
        public decimal sSettlementCountyRtc
        {
            get { return CalculateTotalAmountForCC(sSettlementCountyRtcPc, sSettlementCountyRtcBaseT, sSettlementCountyRtcMb); }
        }
        public string sSettlementCountyRtc_rep
        {
            get { return ToMoneyString(() => sSettlementCountyRtc); }
        }

        [DependsOn(nameof(sSettlementCountyRtcMb), nameof(sCountyRtcMb), nameof(sUseGFEDataForSCFields))]
        public decimal sSettlementCountyRtcMb
        {
            get { return GetSCField(() => GetMoneyField("sSettlementCountyRtcMb"), () => sCountyRtcMb, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementCountyRtcMb", value), () => sCountyRtcMb = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementCountyRtcMb_rep
        {
            get { return ToMoneyString(() => sSettlementCountyRtcMb); }
            set { sSettlementCountyRtcMb = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementCountyRtcBaseT), nameof(sCountyRtcBaseT), nameof(sUseGFEDataForSCFields))]
        public E_PercentBaseT sSettlementCountyRtcBaseT
        {
            get { return (E_PercentBaseT)GetSCField(() => GetTypeIndexField("sSettlementCountyRtcBaseT"), () => (int)sCountyRtcBaseT, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetTypeIndexField("sSettlementCountyRtcBaseT", (int)value), () => sCountyRtcBaseT = value, sUseGFEDataForSCFields); }
        }

        [DependsOn(nameof(sSettlementCountyRtcProps), nameof(sCountyRtcProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementCountyRtcProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementCountyRtcProps"), () => sCountyRtcProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementCountyRtcProps", value), () => sCountyRtcProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementCountyRtcProps_rep
        {
            get { return ToPropsString(() => sSettlementCountyRtcProps); }
            set { sSettlementCountyRtcProps = ToProps(value); }
        }

        #endregion

        #region Line 1205

        [DependsOn(nameof(sSettlementStateRtcPc), nameof(sStateRtcPc), nameof(sUseGFEDataForSCFields))]
        public decimal sSettlementStateRtcPc
        {
            get { return GetSCField(() => GetRateField("sSettlementStateRtcPc"), () => sStateRtcPc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetRateField("sSettlementStateRtcPc", value), () => sStateRtcPc = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementStateRtcPc_rep
        {
            get { return ToRateString(() => sSettlementStateRtcPc); }
            set { sSettlementStateRtcPc = ToRate(value); }
        }

        [DependsOn(nameof(sSettlementStateRtcBaseT), nameof(sSettlementStateRtcPc), nameof(sSettlementStateRtcMb), nameof(sfCalculateTotalAmount))]
        public decimal sSettlementStateRtc
        {
            get { return CalculateTotalAmountForCC(sSettlementStateRtcPc, sSettlementStateRtcBaseT, sSettlementStateRtcMb); }
        }
        public string sSettlementStateRtc_rep
        {
            get { return ToMoneyString(() => sSettlementStateRtc); }
        }

        [DependsOn(nameof(sSettlementStateRtcMb), nameof(sStateRtcMb), nameof(sUseGFEDataForSCFields))]
        public decimal sSettlementStateRtcMb
        {
            get { return GetSCField(() => GetMoneyField("sSettlementStateRtcMb"), () => sStateRtcMb, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementStateRtcMb", value), () => sStateRtcMb = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementStateRtcMb_rep
        {
            get { return ToMoneyString(() => sSettlementStateRtcMb); }
            set { sSettlementStateRtcMb = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementStateRtcProps), nameof(sStateRtcProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementStateRtcProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementStateRtcProps"), () => sStateRtcProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementStateRtcProps", value), () => sStateRtcProps = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementStateRtcProps_rep
        {
            get { return ToPropsString(() => sSettlementStateRtcProps); }
            set { sSettlementStateRtcProps = ToProps(value); }
        }

        [DependsOn(nameof(sSettlementStateRtcBaseT), nameof(sStateRtcBaseT), nameof(sUseGFEDataForSCFields))]
        public E_PercentBaseT sSettlementStateRtcBaseT
        {
            get { return (E_PercentBaseT)GetSCField(() => GetTypeIndexField("sSettlementStateRtcBaseT"), () => (int)sStateRtcBaseT, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetTypeIndexField("sSettlementStateRtcBaseT", (int)value), () => sStateRtcBaseT = value, sUseGFEDataForSCFields); }
        }
        #endregion

        #region Line 1206

        [DependsOn(nameof(sSettlementU1GovRtcPc), nameof(sU1GovRtcPc), nameof(sUseGFEDataForSCFields))]
        public decimal sSettlementU1GovRtcPc
        {
            get { return GetSCField(() => GetRateField("sSettlementU1GovRtcPc"), () => sU1GovRtcPc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetRateField("sSettlementU1GovRtcPc", value), () => sU1GovRtcPc = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementU1GovRtcPc_rep
        {
            get { return ToRateString(() => sSettlementU1GovRtcPc); }
            set { sSettlementU1GovRtcPc = ToRate(value); }
        }

        [DependsOn(nameof(sSettlementU1GovRtcDesc), nameof(sU1GovRtcDesc), nameof(sUseGFEDataForSCFields))]
        public string sSettlementU1GovRtcDesc
        {
            get { return GetSCField(() => GetStringVarCharField("sSettlementU1GovRtcDesc"), () => sU1GovRtcDesc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetStringVarCharField("sSettlementU1GovRtcDesc", value), () => sU1GovRtcDesc = value, sUseGFEDataForSCFields); }
        }

        [DependsOn(nameof(sSettlementU1GovRtcProps), nameof(sU1GovRtcProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementU1GovRtcProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementU1GovRtcProps"), () => sU1GovRtcProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementU1GovRtcProps", value), () => sU1GovRtcProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementU1GovRtcProps_rep
        {
            get { return ToPropsString(() => sSettlementU1GovRtcProps); }
            set { sSettlementU1GovRtcProps = ToProps(value); }
        }

        [DependsOn(nameof(sSettlementU1GovRtcMb), nameof(sU1GovRtcMb), nameof(sUseGFEDataForSCFields))]
        public decimal sSettlementU1GovRtcMb
        {
            get { return GetSCField(() => GetMoneyField("sSettlementU1GovRtcMb"), () => sU1GovRtcMb, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementU1GovRtcMb", value), () => sU1GovRtcMb = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementU1GovRtcMb_rep
        {
            get { return ToMoneyString(() => sSettlementU1GovRtcMb); }
            set { sSettlementU1GovRtcMb = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementU1GovRtcBaseT), nameof(sU1GovRtcBaseT), nameof(sUseGFEDataForSCFields))]
        public E_PercentBaseT sSettlementU1GovRtcBaseT
        {
            get { return (E_PercentBaseT)GetSCField(() => GetTypeIndexField("sSettlementU1GovRtcBaseT"), () => (int)sU1GovRtcBaseT, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetTypeIndexField("sSettlementU1GovRtcBaseT", (int)value), () => sU1GovRtcBaseT = value, sUseGFEDataForSCFields); }
        }

        [DependsOn(nameof(sSettlementU1GovRtcBaseT), nameof(sSettlementU1GovRtcPc), nameof(sSettlementU1GovRtcMb), nameof(sfCalculateTotalAmount))]
        public decimal sSettlementU1GovRtc
        {
            get { return CalculateTotalAmountForCC(sSettlementU1GovRtcPc, sSettlementU1GovRtcBaseT, sSettlementU1GovRtcMb); }
        }
        public string sSettlementU1GovRtc_rep
        {
            get { return ToMoneyString(() => sSettlementU1GovRtc); }
        }
        #endregion

        #region Line 1207

        [DependsOn(nameof(sSettlementU2GovRtcPc), nameof(sU2GovRtcPc), nameof(sUseGFEDataForSCFields))]
        public decimal sSettlementU2GovRtcPc
        {
            get { return GetSCField(() => GetRateField("sSettlementU2GovRtcPc"), () => sU2GovRtcPc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetRateField("sSettlementU2GovRtcPc", value), () => sU2GovRtcPc = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementU2GovRtcPc_rep
        {
            get { return ToRateString(() => sSettlementU2GovRtcPc); }
            set { sSettlementU2GovRtcPc = ToRate(value); }
        }

        [DependsOn(nameof(sSettlementU2GovRtcDesc), nameof(sU2GovRtcDesc), nameof(sUseGFEDataForSCFields))]
        public string sSettlementU2GovRtcDesc
        {
            get { return GetSCField(() => GetStringVarCharField("sSettlementU2GovRtcDesc"), () => sU2GovRtcDesc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetStringVarCharField("sSettlementU2GovRtcDesc", value), () => sU2GovRtcDesc = value, sUseGFEDataForSCFields); }
        }

        [DependsOn(nameof(sSettlementU2GovRtcProps), nameof(sU2GovRtcProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementU2GovRtcProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementU2GovRtcProps"), () => sU2GovRtcProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementU2GovRtcProps", value), () => sU2GovRtcProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementU2GovRtcProps_rep
        {
            get { return ToPropsString(() => sSettlementU2GovRtcProps); }
            set { sSettlementU2GovRtcProps = ToProps(value); }
        }

        [DependsOn(nameof(sSettlementU2GovRtcMb), nameof(sU2GovRtcMb), nameof(sUseGFEDataForSCFields))]
        public decimal sSettlementU2GovRtcMb
        {
            get { return GetSCField(() => GetMoneyField("sSettlementU2GovRtcMb"), () => sU2GovRtcMb, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementU2GovRtcMb", value), () => sU2GovRtcMb = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementU2GovRtcMb_rep
        {
            get { return ToMoneyString(() => sSettlementU2GovRtcMb); }
            set { sSettlementU2GovRtcMb = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementU2GovRtcBaseT), nameof(sU2GovRtcBaseT), nameof(sUseGFEDataForSCFields))]
        public E_PercentBaseT sSettlementU2GovRtcBaseT
        {
            get { return (E_PercentBaseT)GetSCField(() => GetTypeIndexField("sSettlementU2GovRtcBaseT"), () => (int)sU2GovRtcBaseT, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetTypeIndexField("sSettlementU2GovRtcBaseT", (int)value), () => sU2GovRtcBaseT = value, sUseGFEDataForSCFields); }
        }

        [DependsOn(nameof(sSettlementU2GovRtcBaseT), nameof(sSettlementU2GovRtcPc), nameof(sSettlementU2GovRtcMb), nameof(sfCalculateTotalAmount))]
        public decimal sSettlementU2GovRtc
        {
            get { return CalculateTotalAmountForCC(sSettlementU2GovRtcPc, sSettlementU2GovRtcBaseT, sSettlementU2GovRtcMb); }
        }
        public string sSettlementU2GovRtc_rep
        {
            get { return ToMoneyString(() => sSettlementU2GovRtc); }
        }
        #endregion

        #region Line 1208
        [DependsOn(nameof(sSettlementU3GovRtcPc), nameof(sU3GovRtcPc), nameof(sUseGFEDataForSCFields))]
        public decimal sSettlementU3GovRtcPc
        {
            get { return GetSCField(() => GetRateField("sSettlementU3GovRtcPc"), () => sU3GovRtcPc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetRateField("sSettlementU3GovRtcPc", value), () => sU3GovRtcPc = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementU3GovRtcPc_rep
        {
            get { return ToRateString(() => sSettlementU3GovRtcPc); }
            set { sSettlementU3GovRtcPc = ToRate(value); }
        }

        [DependsOn(nameof(sSettlementU3GovRtcDesc), nameof(sU3GovRtcDesc), nameof(sUseGFEDataForSCFields))]
        public string sSettlementU3GovRtcDesc
        {
            get { return GetSCField(() => GetStringVarCharField("sSettlementU3GovRtcDesc"), () => sU3GovRtcDesc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetStringVarCharField("sSettlementU3GovRtcDesc", value), () => sU3GovRtcDesc = value, sUseGFEDataForSCFields); }
        }

        [DependsOn(nameof(sSettlementU3GovRtcProps), nameof(sU3GovRtcProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementU3GovRtcProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementU3GovRtcProps"), () => sU3GovRtcProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementU3GovRtcProps", value), () => sU3GovRtcProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementU3GovRtcProps_rep
        {
            get { return ToPropsString(() => sSettlementU3GovRtcProps); }
            set { sSettlementU3GovRtcProps = ToProps(value); }
        }

        [DependsOn(nameof(sSettlementU3GovRtcMb), nameof(sU3GovRtcMb), nameof(sUseGFEDataForSCFields))]
        public decimal sSettlementU3GovRtcMb
        {
            get { return GetSCField(() => GetMoneyField("sSettlementU3GovRtcMb"), () => sU3GovRtcMb, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementU3GovRtcMb", value), () => sU3GovRtcMb = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementU3GovRtcMb_rep
        {
            get { return ToMoneyString(() => sSettlementU3GovRtcMb); }
            set { sSettlementU3GovRtcMb = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementU3GovRtcBaseT), nameof(sU3GovRtcBaseT), nameof(sUseGFEDataForSCFields))]
        public E_PercentBaseT sSettlementU3GovRtcBaseT
        {
            get { return (E_PercentBaseT)GetSCField(() => GetTypeIndexField("sSettlementU3GovRtcBaseT"), () => (int)sU3GovRtcBaseT, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetTypeIndexField("sSettlementU3GovRtcBaseT", (int)value), () => sU3GovRtcBaseT = value, sUseGFEDataForSCFields); }
        }

        [DependsOn(nameof(sSettlementU3GovRtcBaseT), nameof(sSettlementU3GovRtcPc), nameof(sSettlementU3GovRtcMb), nameof(sfCalculateTotalAmount))]
        public decimal sSettlementU3GovRtc
        {
            get { return CalculateTotalAmountForCC(sSettlementU3GovRtcPc, sSettlementU3GovRtcBaseT, sSettlementU3GovRtcMb); }
        }
        public string sSettlementU3GovRtc_rep
        {
            get { return ToMoneyString(() => sSettlementU3GovRtc); }
        }
        #endregion

        [DependsOn(nameof(sSettlementRecF), nameof(sSettlementCountyRtc), nameof(sSettlementStateRtc), nameof(sSettlementU1GovRtc), nameof(sSettlementU2GovRtc), nameof(sSettlementU3GovRtc))]
        private decimal sSettlementTotalGovernmentRecordingAndTransferCharges
        {
            get
            {
                return SumMoney(sSettlementRecF, sSettlementCountyRtc, sSettlementStateRtc, sSettlementU1GovRtc, sSettlementU2GovRtc, sSettlementU3GovRtc);
            }
        }

        public string sSettlementTotalGovernmentRecordingAndTransferCharges_rep
        {
            get { return ToMoneyString(() => sSettlementTotalGovernmentRecordingAndTransferCharges); }
        }

        #endregion

        #region Section 1300 - ADDITIONAL SETTLEMENT CHARGES

        #region Line 1302
        [DependsOn(nameof(sSettlementPestInspectF), nameof(sPestInspectF), nameof(sUseGFEDataForSCFields))]
        private decimal sSettlementPestInspectF
        {
            get { return GetSCField(() => GetMoneyField("sSettlementPestInspectF"), () => sPestInspectF, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementPestInspectF", value), () => sPestInspectF = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementPestInspectF_rep
        {
            get { return ToMoneyString(() => sSettlementPestInspectF); }
            set { sSettlementPestInspectF = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementPestInspectFProps), nameof(sPestInspectFProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementPestInspectFProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementPestInspectFProps"), () => sPestInspectFProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementPestInspectFProps", value), () => sPestInspectFProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementPestInspectFProps_rep
        {
            get { return ToPropsString(() => sSettlementPestInspectFProps); }
            set { sSettlementPestInspectFProps = ToProps(value); }
        }

        #endregion

        #region Line 1303
        [DependsOn(nameof(sSettlementU1Sc), nameof(sU1Sc), nameof(sUseGFEDataForSCFields))]
        private decimal sSettlementU1Sc
        {
            get { return GetSCField(() => GetMoneyField("sSettlementU1Sc"), () => sU1Sc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementU1Sc", value), () => sU1Sc = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementU1Sc_rep
        {
            get { return ToMoneyString(() => sSettlementU1Sc); }
            set { sSettlementU1Sc = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementU1ScProps), nameof(sU1ScProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementU1ScProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementU1ScProps"), () => sU1ScProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementU1ScProps", value), () => sU1ScProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementU1ScProps_rep
        {
            get { return ToPropsString(() => sSettlementU1ScProps); }
            set { sSettlementU1ScProps = ToProps(value); }
        }

        [DependsOn(nameof(sSettlementU1ScDesc), nameof(sU1ScDesc), nameof(sUseGFEDataForSCFields))]
        public string sSettlementU1ScDesc
        {
            get { return GetSCField(() => GetLongTextField("sSettlementU1ScDesc"), () => sU1ScDesc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetLongTextField("sSettlementU1ScDesc", value), () => sU1ScDesc = value, sUseGFEDataForSCFields); }
        }
        #endregion

        #region Line 1304
        [DependsOn(nameof(sSettlementU2Sc), nameof(sU2Sc), nameof(sUseGFEDataForSCFields))]
        private decimal sSettlementU2Sc
        {
            get { return GetSCField(() => GetMoneyField("sSettlementU2Sc"), () => sU2Sc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementU2Sc", value), () => sU2Sc = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementU2Sc_rep
        {
            get { return ToMoneyString(() => sSettlementU2Sc); }
            set { sSettlementU2Sc = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementU2ScProps), nameof(sU2ScProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementU2ScProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementU2ScProps"), () => sU2ScProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementU2ScProps", value), () => sU2ScProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementU2ScProps_rep
        {
            get { return ToPropsString(() => sSettlementU2ScProps); }
            set { sSettlementU2ScProps = ToProps(value); }
        }

        [DependsOn(nameof(sSettlementU2ScDesc), nameof(sU2ScDesc), nameof(sUseGFEDataForSCFields))]
        public string sSettlementU2ScDesc
        {
            get { return GetSCField(() => GetLongTextField("sSettlementU2ScDesc"), () => sU2ScDesc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetLongTextField("sSettlementU2ScDesc", value), () => sU2ScDesc = value, sUseGFEDataForSCFields); }
        }

        #endregion

        #region Line 1305
        [DependsOn(nameof(sSettlementU3Sc), nameof(sU3Sc), nameof(sUseGFEDataForSCFields))]
        private decimal sSettlementU3Sc
        {
            get { return GetSCField(() => GetMoneyField("sSettlementU3Sc"), () => sU3Sc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementU3Sc", value), () => sU3Sc = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementU3Sc_rep
        {
            get { return ToMoneyString(() => sSettlementU3Sc); }
            set { sSettlementU3Sc = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementU3ScProps), nameof(sU3ScProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementU3ScProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementU3ScProps"), () => sU3ScProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementU3ScProps", value), () => sU3ScProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementU3ScProps_rep
        {
            get { return ToPropsString(() => sSettlementU3ScProps); }
            set { sSettlementU3ScProps = ToProps(value); }
        }

        [DependsOn(nameof(sSettlementU3ScDesc), nameof(sU3ScDesc), nameof(sUseGFEDataForSCFields))]
        public string sSettlementU3ScDesc
        {
            get { return GetSCField(() => GetLongTextField("sSettlementU3ScDesc"), () => sU3ScDesc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetLongTextField("sSettlementU3ScDesc", value), () => sU3ScDesc = value, sUseGFEDataForSCFields); }
        }

        #endregion

        #region Line 1306
        [DependsOn(nameof(sSettlementU4Sc), nameof(sU4Sc), nameof(sUseGFEDataForSCFields))]
        private decimal sSettlementU4Sc
        {
            get { return GetSCField(() => GetMoneyField("sSettlementU4Sc"), () => sU4Sc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementU4Sc", value), () => sU4Sc = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementU4Sc_rep
        {
            get { return ToMoneyString(() => sSettlementU4Sc); }
            set { sSettlementU4Sc = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementU4ScProps), nameof(sU4ScProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementU4ScProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementU4ScProps"), () => sU4ScProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementU4ScProps", value), () => sU4ScProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementU4ScProps_rep
        {
            get { return ToPropsString(() => sSettlementU4ScProps); }
            set { sSettlementU4ScProps = ToProps(value); }
        }

        [DependsOn(nameof(sSettlementU4ScDesc), nameof(sU4ScDesc), nameof(sUseGFEDataForSCFields))]
        public string sSettlementU4ScDesc
        {
            get { return GetSCField(() => GetLongTextField("sSettlementU4ScDesc"), () => sU4ScDesc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetLongTextField("sSettlementU4ScDesc", value), () => sU4ScDesc = value, sUseGFEDataForSCFields); }
        }

        #endregion

        #region Line 1307
        [DependsOn(nameof(sSettlementU5Sc), nameof(sU5Sc), nameof(sUseGFEDataForSCFields))]
        private decimal sSettlementU5Sc
        {
            get { return GetSCField(() => GetMoneyField("sSettlementU5Sc"), () => sU5Sc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetMoneyField("sSettlementU5Sc", value), () => sU5Sc = value, sUseGFEDataForSCFields); }
        }

        public string sSettlementU5Sc_rep
        {
            get { return ToMoneyString(() => sSettlementU5Sc); }
            set { sSettlementU5Sc = ToMoney(value); }
        }

        [DependsOn(nameof(sSettlementU5ScProps), nameof(sU5ScProps), nameof(sUseGFEDataForSCFields))]
        public int sSettlementU5ScProps
        {
            get { return GetSCField(() => Get32BitPropsField("sSettlementU5ScProps"), () => sU5ScProps, sUseGFEDataForSCFields); }
            set { SetSCField(() => Set32BitPropsField("sSettlementU5ScProps", value), () => sU5ScProps = value, sUseGFEDataForSCFields); }
        }
        public string sSettlementU5ScProps_rep
        {
            get { return ToPropsString(() => sSettlementU5ScProps); }
            set { sSettlementU5ScProps = ToProps(value); }
        }

        [DependsOn(nameof(sSettlementU5ScDesc), nameof(sU5ScDesc), nameof(sUseGFEDataForSCFields))]
        public string sSettlementU5ScDesc
        {
            get { return GetSCField(() => GetLongTextField("sSettlementU5ScDesc"), () => sU5ScDesc, sUseGFEDataForSCFields); }
            set { SetSCField(() => SetLongTextField("sSettlementU5ScDesc", value), () => sU5ScDesc = value, sUseGFEDataForSCFields); }
        }

        [DependsOn(nameof(sSettlementPestInspectF), nameof(sSettlementU1Sc), nameof(sSettlementU2Sc), nameof(sSettlementU3Sc), nameof(sSettlementU4Sc), nameof(sSettlementU5Sc))]
        private decimal sSettlementTotalAdditionalSettlementCharges
        {
            get
            {
                return SumMoney(sSettlementPestInspectF, sSettlementU1Sc, sSettlementU2Sc, sSettlementU3Sc, sSettlementU4Sc, sSettlementU5Sc);
            }
        }

        public string sSettlementTotalAdditionalSettlementCharges_rep
        {
            get { return ToMoneyString(() => sSettlementTotalAdditionalSettlementCharges); }
        }

        #endregion

        #endregion

        #region TOTALS
        [DependsOn(nameof(sSettlementAdjOriginationCharge), nameof(sSettlementTotalOtherSettlementServiceFee))]
        private decimal sSettlementTotalEstimateSettlementCharge
        {
            get
            {
                return SumMoney(sSettlementAdjOriginationCharge, sSettlementTotalOtherSettlementServiceFee);
            }
        }
        public string sSettlementTotalEstimateSettlementCharge_rep
        {
            get { return ToMoneyString(() => sSettlementTotalEstimateSettlementCharge); }
        }

        [DependsOn(nameof(sSettlementOriginationF), nameof(sSettlementLDiscnt))]
        private decimal sSettlementAdjOriginationCharge
        {
            get
            {
                return sSettlementOriginationF + sSettlementLDiscnt;
            }
        }
        public string sSettlementAdjOriginationCharge_rep
        {
            get { return ToMoneyString(() => sSettlementAdjOriginationCharge); }
        }

        [DependsOn(nameof(sSettlementLOrigF), nameof(sSettlementMBrokF), nameof(sSettlementInspectF), nameof(sSettlementProcF), 
            nameof(sSettlementUwF), nameof(sSettlementWireF), nameof(sSettlement800U1F), nameof(s800U1FSettlementSection), nameof(s800U2FSettlementSection), 
            nameof(sSettlement800U2F), nameof(s800U3FSettlementSection), nameof(sSettlement800U3F), nameof(s800U4FSettlementSection), nameof(sSettlement800U4F), 
            nameof(s800U5FSettlementSection), nameof(sSettlement800U5F), nameof(sSettlementLOrigBrokerCreditF), nameof(sGfeOriginatorCompF))]
        private decimal sSettlementOriginationF
        {
            get
            {
                List<decimal> list = new List<decimal>();
                list.Add(sSettlementLOrigF);
                list.Add(sSettlementMBrokF);
                list.Add(sSettlementInspectF);
                list.Add(sSettlementProcF);
                list.Add(sSettlementUwF);
                list.Add(sSettlementWireF);

                if (s800U1FSettlementSection == E_GfeSectionT.B1)
                {
                    list.Add(sSettlement800U1F);
                }
                if (s800U2FSettlementSection == E_GfeSectionT.B1)
                {
                    list.Add(sSettlement800U2F);
                }

                if (s800U3FSettlementSection == E_GfeSectionT.B1)
                {
                    list.Add(sSettlement800U3F);
                }
                if (s800U4FSettlementSection == E_GfeSectionT.B1)
                {
                    list.Add(sSettlement800U4F);
                }

                if (s800U5FSettlementSection == E_GfeSectionT.B1)
                {
                    list.Add(sSettlement800U5F);
                }

                // 1/28/2010 dd - OPM 44498.
                //list.Add(sSettlementBrokerComp);
                //list.Add(sSettlementLOrigBrokerCreditF);
                list.Add(sGfeOriginatorCompF);

                return SumMoney(list);
            }
        }
        public string sSettlementOriginationF_rep
        {
            get { return ToMoneyString(() => sSettlementOriginationF); }
        }

        #region GFE Sections
        
        [DependsOn(nameof(sSettlementLenderCreditF), nameof(sSettlementCreditLenderPaidItemF), nameof(sGfeOriginatorCompF), nameof(sSettlementCreditLenderPaidItemT), nameof(sOriginatorCompensationPaymentSourceT))]
        public decimal sSettlementLenderCreditYieldSpreadDifferentialAmount
        {
            get
            {
                if (sSettlementCreditLenderPaidItemT == E_CreditLenderPaidItemT.AllLenderPaidItems
                    && sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)
                {
                    decimal amount = -(sSettlementLenderCreditF + sSettlementCreditLenderPaidItemF + sGfeOriginatorCompF);
                    return amount;
                }
                else if (sSettlementCreditLenderPaidItemT == E_CreditLenderPaidItemT.AllLenderPaidItems
                    && sOriginatorCompensationPaymentSourceT != E_sOriginatorCompensationPaymentSourceT.LenderPaid)
                {
                    decimal amount = -(sSettlementLenderCreditF + sSettlementCreditLenderPaidItemF);
                    return amount;
                }
                else
                {
                    return -sSettlementLenderCreditF;
                }
            }
        }
        
        public string sSettlementLenderCreditYieldSpreadDifferentialAmount_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementLenderCreditYieldSpreadDifferentialAmount);
            }
        }

        [DependsOn(nameof(s800U1FSettlementSection), nameof(s800U1FGfeSection), nameof(sUseGFEDataForSCFields))]
        public E_GfeSectionT s800U1FSettlementSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetSCField(() => GetTypeIndexField("s800U1FSettlementSection"), () => (int)s800U1FGfeSection, sUseGFEDataForSCFields);
                if (v == E_GfeSectionT.B1 || v == E_GfeSectionT.B3 || v == E_GfeSectionT.NotApplicable)
                {
                    return v;
                }
                else
                {
                    return E_GfeSectionT.B1;
                }
            }
            set { SetSCField(() => SetTypeIndexField("s800U1FSettlementSection", (int)value), () => s800U1FGfeSection = value, sUseGFEDataForSCFields); }
        }

        [DependsOn(nameof(s800U2FSettlementSection), nameof(s800U2FGfeSection), nameof(sUseGFEDataForSCFields))]
        public E_GfeSectionT s800U2FSettlementSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetSCField(() => GetTypeIndexField("s800U2FSettlementSection"), () => (int)s800U2FGfeSection, sUseGFEDataForSCFields);
                if (v == E_GfeSectionT.B1 || v == E_GfeSectionT.B3 || v == E_GfeSectionT.NotApplicable)
                {
                    return v;
                }
                else
                {
                    return E_GfeSectionT.B1;
                }
            }
            set { SetSCField(() => SetTypeIndexField("s800U2FSettlementSection", (int)value), () => s800U2FGfeSection = value, sUseGFEDataForSCFields); }
        }

        [DependsOn(nameof(s800U3FSettlementSection), nameof(s800U3FGfeSection), nameof(sUseGFEDataForSCFields))]
        public E_GfeSectionT s800U3FSettlementSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetSCField(() => GetTypeIndexField("s800U3FSettlementSection"), () => (int)s800U3FGfeSection, sUseGFEDataForSCFields);
                if (v == E_GfeSectionT.B1 || v == E_GfeSectionT.B3 || v == E_GfeSectionT.NotApplicable)
                {
                    return v;
                }
                else
                {
                    return E_GfeSectionT.B1;
                }
            }
            set { SetSCField(() => SetTypeIndexField("s800U3FSettlementSection", (int)value), () => s800U3FGfeSection = value, sUseGFEDataForSCFields); }
        }

        [DependsOn(nameof(s800U4FSettlementSection), nameof(s800U4FGfeSection), nameof(sUseGFEDataForSCFields))]
        public E_GfeSectionT s800U4FSettlementSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetSCField(() => GetTypeIndexField("s800U4FSettlementSection"), () => (int)s800U4FGfeSection, sUseGFEDataForSCFields);
                if (v == E_GfeSectionT.B1 || v == E_GfeSectionT.B3 || v == E_GfeSectionT.NotApplicable)
                {
                    return v;
                }
                else
                {
                    return E_GfeSectionT.B1;
                }
            }
            set { SetSCField(() => SetTypeIndexField("s800U4FSettlementSection", (int)value), () => s800U4FGfeSection = value, sUseGFEDataForSCFields); }
        }

        [DependsOn(nameof(s800U5FSettlementSection), nameof(s800U5FGfeSection), nameof(sUseGFEDataForSCFields))]
        public E_GfeSectionT s800U5FSettlementSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetSCField(() => GetTypeIndexField("s800U5FSettlementSection"), () => (int)s800U5FGfeSection, sUseGFEDataForSCFields);
                if (v == E_GfeSectionT.B1 || v == E_GfeSectionT.B3 || v == E_GfeSectionT.NotApplicable)
                {
                    return v;
                }
                else
                {
                    return E_GfeSectionT.B1;
                }
            }
            set { SetSCField(() => SetTypeIndexField("s800U5FSettlementSection", (int)value), () => s800U5FGfeSection = value, sUseGFEDataForSCFields); }
        }

        [DependsOn(nameof(s904PiaSettlementSection), nameof(s904PiaGfeSection), nameof(sUseGFEDataForSCFields))]
        public E_GfeSectionT s904PiaSettlementSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetSCField(() => GetTypeIndexField("s904PiaSettlementSection"), () => (int)s904PiaGfeSection, sUseGFEDataForSCFields);
                if (v == E_GfeSectionT.B3 || v == E_GfeSectionT.B11 || v == E_GfeSectionT.NotApplicable)
                {
                    return v;
                }
                else
                {
                    return E_GfeSectionT.B3;
                }
            }
            set { SetSCField(() => SetTypeIndexField("s904PiaSettlementSection", (int)value), () => s904PiaGfeSection = value, sUseGFEDataForSCFields); }
        }

        [DependsOn(nameof(s900U1PiaSettlementSection), nameof(s900U1PiaGfeSection), nameof(sUseGFEDataForSCFields))]
        public E_GfeSectionT s900U1PiaSettlementSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetSCField(() => GetTypeIndexField("s900U1PiaSettlementSection"), () => (int)s900U1PiaGfeSection, sUseGFEDataForSCFields);
                if (v == E_GfeSectionT.B3 || v == E_GfeSectionT.B11 || v == E_GfeSectionT.NotApplicable)
                {
                    return v;
                }
                else
                {
                    return E_GfeSectionT.B3;
                }
            }
            set { SetSCField(() => SetTypeIndexField("s900U1PiaSettlementSection", (int)value), () => s900U1PiaGfeSection = value, sUseGFEDataForSCFields); }
        }

        [DependsOn(nameof(sEscrowFSettlementSection), nameof(sEscrowFGfeSection), nameof(sUseGFEDataForSCFields))]
        public E_GfeSectionT sEscrowFSettlementSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetSCField(() => GetTypeIndexField("sEscrowFSettlementSection"), () => (int)sEscrowFGfeSection, sUseGFEDataForSCFields);
                if (v == E_GfeSectionT.B4 || v == E_GfeSectionT.B6)
                {
                    // v is unchanged for now.
                }
                else
                {
                    v = E_GfeSectionT.B4;
                }


                if (v == E_GfeSectionT.B6 && BrokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    v = E_GfeSectionT.B4;
                }
                return v;
            }
            set 
            {
                var valueAttempted = value;
                if (valueAttempted == E_GfeSectionT.B6 && BrokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    Tools.LogError("Programming error.  Attempted to set to set GfeSectionT to B6 when IsB4AndB6DisabledIn1100And1300OfGfe.  Setting it to B4.", new Exception());
                    valueAttempted = E_GfeSectionT.B4;
                }
                SetSCField(() => SetTypeIndexField("sEscrowFSettlementSection", (int)valueAttempted), () => sEscrowFGfeSection = value, sUseGFEDataForSCFields); 
            }
        }

        [DependsOn(nameof(sDocPrepFSettlementSection), nameof(sDocPrepFGfeSection), nameof(sUseGFEDataForSCFields))]
        public E_GfeSectionT sDocPrepFSettlementSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetSCField(() => GetTypeIndexField("sDocPrepFSettlementSection"), () => (int)sDocPrepFGfeSection, sUseGFEDataForSCFields);
                if (v == E_GfeSectionT.B4 || v == E_GfeSectionT.B6)
                {
                    // v is unchanged for now.
                }
                else
                {
                    v = E_GfeSectionT.B4;
                }

                if (v == E_GfeSectionT.B6 && BrokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    v = E_GfeSectionT.B4;
                }
                return v;
            }
            set 
            {
                var valueAttempted = value;
                if (valueAttempted == E_GfeSectionT.B6 && BrokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    Tools.LogError("Programming error.  Attempted to set to set GfeSectionT to B6 when IsB4AndB6DisabledIn1100And1300OfGfe.  Setting it to B4.", new Exception());
                    valueAttempted = E_GfeSectionT.B4;
                }
                SetSCField(() => SetTypeIndexField("sDocPrepFSettlementSection", (int)valueAttempted), () => sDocPrepFGfeSection = value, sUseGFEDataForSCFields); 
            }
        }

        [DependsOn(nameof(sNotaryFSettlementSection), nameof(sNotaryFGfeSection), nameof(sUseGFEDataForSCFields))]
        public E_GfeSectionT sNotaryFSettlementSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetSCField(() => GetTypeIndexField("sNotaryFSettlementSection"), () => (int)sNotaryFGfeSection, sUseGFEDataForSCFields);
                if (v == E_GfeSectionT.B4 || v == E_GfeSectionT.B6)
                {
                    // v is unchanged for now.
                }
                else
                {
                    v = E_GfeSectionT.B4;
                }
                if (v == E_GfeSectionT.B6 && BrokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    v = E_GfeSectionT.B4;
                }
                return v;
            }
            set 
            {
                var valueAttempted = value;
                if (valueAttempted == E_GfeSectionT.B6 && BrokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    Tools.LogError("Programming error.  Attempted to set to set GfeSectionT to B6 when IsB4AndB6DisabledIn1100And1300OfGfe.  Setting it to B4.", new Exception());
                    valueAttempted = E_GfeSectionT.B4;
                }
                SetSCField(() => SetTypeIndexField("sNotaryFSettlementSection", (int)valueAttempted), () => sNotaryFGfeSection = value, sUseGFEDataForSCFields);
            }
        }

        [DependsOn(nameof(sAttorneyFSettlementSection), nameof(sAttorneyFGfeSection), nameof(sUseGFEDataForSCFields))]
        public E_GfeSectionT sAttorneyFSettlementSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetSCField(() => GetTypeIndexField("sAttorneyFSettlementSection"), () => (int)sAttorneyFGfeSection, sUseGFEDataForSCFields);
                if (v == E_GfeSectionT.B4 || v == E_GfeSectionT.B6)
                {
                    // v is unchanged for now.
                }
                else
                {
                    v = E_GfeSectionT.B4;
                }

                if (v == E_GfeSectionT.B6 && BrokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    v = E_GfeSectionT.B4;
                }
                return v;
            }
            set
            {
                var valueAttempted = value;
                if (valueAttempted == E_GfeSectionT.B6 && BrokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    Tools.LogError("Programming error.  Attempted to set to set GfeSectionT to B6 when IsB4AndB6DisabledIn1100And1300OfGfe.  Setting it to B4.", new Exception());
                    valueAttempted = E_GfeSectionT.B4;
                }
                SetSCField(() => SetTypeIndexField("sAttorneyFSettlementSection", (int)valueAttempted), () => sAttorneyFGfeSection = value, sUseGFEDataForSCFields);
            }        
        }

        [DependsOn(nameof(sTitleInsFSettlementSection), nameof(sTitleInsFGfeSection), nameof(sUseGFEDataForSCFields))]
        public E_GfeSectionT sTitleInsFSettlementSection
        {
            get
            {

                //requested per OPM 9jj 
                return (E_GfeSectionT)GetSCField(() => (int)E_GfeSectionT.B4, () => (int)sTitleInsFGfeSection, sUseGFEDataForSCFields);
            }
            set { SetSCField(() => SetTypeIndexField("sTitleInsFSettlementSection", (int)value), () => sTitleInsFGfeSection = value, sUseGFEDataForSCFields); }
        }

        [DependsOn(nameof(sU1TcSettlementSection), nameof(sU1TcGfeSection), nameof(sUseGFEDataForSCFields))]
        public E_GfeSectionT sU1TcSettlementSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetSCField(() => GetTypeIndexField("sU1TcSettlementSection"), () => (int)sU1TcGfeSection, sUseGFEDataForSCFields);
                if (v == E_GfeSectionT.B4 || v == E_GfeSectionT.B6 || v == E_GfeSectionT.NotApplicable)
                {
                    // v is unchanged for now.
                }
                else
                {
                    v = E_GfeSectionT.B4;
                }

                if (v == E_GfeSectionT.B6 && BrokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    v = E_GfeSectionT.B4;
                }
                return v;
            }
            set
            {
                var valueAttempted = value;
                if (valueAttempted == E_GfeSectionT.B6 && BrokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    Tools.LogError("Programming error.  Attempted to set to set GfeSectionT to B6 when IsB4AndB6DisabledIn1100And1300OfGfe.  Setting it to B4.", new Exception());
                    valueAttempted = E_GfeSectionT.B4;
                }
                SetSCField(() => SetTypeIndexField("sU1TcSettlementSection", (int)valueAttempted), () => sU1TcGfeSection = value, sUseGFEDataForSCFields);
            }
        }

        [DependsOn(nameof(sU2TcSettlementSection), nameof(sU2TcGfeSection), nameof(sUseGFEDataForSCFields))]
        public E_GfeSectionT sU2TcSettlementSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetSCField(() => GetTypeIndexField("sU2TcSettlementSection"), () => (int)sU2TcGfeSection, sUseGFEDataForSCFields);
                if (v == E_GfeSectionT.B4 || v == E_GfeSectionT.B6 || v == E_GfeSectionT.NotApplicable)
                {
                    // v is unchanged for now.
                }
                else
                {
                    v = E_GfeSectionT.B4;
                }

                if (v == E_GfeSectionT.B6 && BrokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    v = E_GfeSectionT.B4;
                }
                return v;
            }
            set
            {
                var valueAttempted = value;
                if (valueAttempted == E_GfeSectionT.B6 && BrokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    Tools.LogError("Programming error.  Attempted to set to set GfeSectionT to B6 when IsB4AndB6DisabledIn1100And1300OfGfe.  Setting it to B4.", new Exception());
                    valueAttempted = E_GfeSectionT.B4;
                }
                SetSCField(() => SetTypeIndexField("sU2TcSettlementSection", (int)valueAttempted), () => sU2TcGfeSection = value, sUseGFEDataForSCFields);
            }
        }

        [DependsOn(nameof(sU3TcSettlementSection), nameof(sU3TcGfeSection), nameof(sUseGFEDataForSCFields))]
        public E_GfeSectionT sU3TcSettlementSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetSCField(() => GetTypeIndexField("sU3TcSettlementSection"), () => (int)sU3TcGfeSection, sUseGFEDataForSCFields);
                if (v == E_GfeSectionT.B4 || v == E_GfeSectionT.B6 || v == E_GfeSectionT.NotApplicable)
                {
                    // v is unchanged for now.
                }
                else
                {
                    v = E_GfeSectionT.B4;
                }

                if (v == E_GfeSectionT.B6 && BrokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    v = E_GfeSectionT.B4;
                }
                return v;
            }
            set
            {
                var valueAttempted = value;
                if (valueAttempted == E_GfeSectionT.B6 && BrokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    Tools.LogError("Programming error.  Attempted to set to set GfeSectionT to B6 when IsB4AndB6DisabledIn1100And1300OfGfe.  Setting it to B4.", new Exception());
                    valueAttempted = E_GfeSectionT.B4;
                }
                SetSCField(() => SetTypeIndexField("sU3TcSettlementSection", (int)valueAttempted), () => sU3TcGfeSection = value, sUseGFEDataForSCFields);
            }
        }

        [DependsOn(nameof(sU4TcSettlementSection), nameof(sU4TcGfeSection), nameof(sUseGFEDataForSCFields))]
        public E_GfeSectionT sU4TcSettlementSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetSCField(() => GetTypeIndexField("sU4TcSettlementSection"), () => (int)sU4TcGfeSection, sUseGFEDataForSCFields);
                if (v == E_GfeSectionT.B4 || v == E_GfeSectionT.B6 || v == E_GfeSectionT.NotApplicable)
                {
                    // v is unchanged for now.
                }
                else
                {
                    v = E_GfeSectionT.B4;
                }

                if (v == E_GfeSectionT.B6 && BrokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    v = E_GfeSectionT.B4;
                }
                return v;
            }
            set
            {
                var valueAttempted = value;
                if (valueAttempted == E_GfeSectionT.B6 && BrokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    Tools.LogError("Programming error.  Attempted to set to set GfeSectionT to B6 when IsB4AndB6DisabledIn1100And1300OfGfe.  Setting it to B4.", new Exception());
                    valueAttempted = E_GfeSectionT.B4;
                }
                SetSCField(() => SetTypeIndexField("sU4TcSettlementSection", (int)valueAttempted), () => sU4TcGfeSection = value, sUseGFEDataForSCFields);
            }
        }

        [DependsOn(nameof(sU1GovRtcSettlementSection), nameof(sU1GovRtcGfeSection), nameof(sUseGFEDataForSCFields))]
        public E_GfeSectionT sU1GovRtcSettlementSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetSCField(() => GetTypeIndexField("sU1GovRtcSettlementSection"), () => (int)sU1GovRtcGfeSection, sUseGFEDataForSCFields);
                if (v == E_GfeSectionT.B7 || v == E_GfeSectionT.B8)
                {
                    return v;
                }
                else
                {
                    return E_GfeSectionT.B8;
                }
            }
            set { SetSCField(() => SetTypeIndexField("sU1GovRtcSettlementSection", (int)value), () => sU1GovRtcGfeSection = value, sUseGFEDataForSCFields); }
        }

        [DependsOn(nameof(sU2GovRtcSettlementSection), nameof(sU2GovRtcGfeSection), nameof(sUseGFEDataForSCFields))]
        public E_GfeSectionT sU2GovRtcSettlementSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetSCField(() => GetTypeIndexField("sU2GovRtcSettlementSection"), () => (int)sU2GovRtcGfeSection, sUseGFEDataForSCFields);
                if (v == E_GfeSectionT.B7 || v == E_GfeSectionT.B8)
                {
                    return v;
                }
                else
                {
                    return E_GfeSectionT.B8;
                }
            }
            set { SetSCField(() => SetTypeIndexField("sU2GovRtcSettlementSection", (int)value), () => sU2GovRtcGfeSection = value, sUseGFEDataForSCFields); }
        }

        [DependsOn(nameof(sU3GovRtcSettlementSection), nameof(sU3GovRtcGfeSection), nameof(sUseGFEDataForSCFields))]
        public E_GfeSectionT sU3GovRtcSettlementSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetSCField(() => GetTypeIndexField("sU3GovRtcSettlementSection"), () => (int)sU3GovRtcGfeSection, sUseGFEDataForSCFields);
                if (v == E_GfeSectionT.B7 || v == E_GfeSectionT.B8)
                {
                    return v;
                }
                else
                {
                    return E_GfeSectionT.B8;
                }
            }
            set { SetSCField(() => SetTypeIndexField("sU3GovRtcSettlementSection", (int)value), () => sU3GovRtcGfeSection = value, sUseGFEDataForSCFields); }
        }

        [DependsOn(nameof(sU1ScSettlementSection), nameof(sU1ScGfeSection), nameof(sUseGFEDataForSCFields))]
        public E_GfeSectionT sU1ScSettlementSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetSCField(() => GetTypeIndexField("sU1ScSettlementSection"), () => (int)sU1ScGfeSection, sUseGFEDataForSCFields);
                if (v == E_GfeSectionT.B4 || v == E_GfeSectionT.B6 || v == E_GfeSectionT.NotApplicable)
                {
                    // v is unchanged for now.
                }
                else
                {
                    v = E_GfeSectionT.B4;
                }
                if (v == E_GfeSectionT.B4 && BrokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    v = E_GfeSectionT.B6;
                }
                return v;
            }
            set
            {
                var valueAttempted = value;
                if (valueAttempted == E_GfeSectionT.B4 && BrokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    Tools.LogError("Programming error.  Attempted to set to set GfeSectionT to B4 when IsB4AndB6DisabledIn1100And1300OfGfe.  Setting it to B6.", new Exception());
                    valueAttempted = E_GfeSectionT.B6;
                }
                SetSCField(() => SetTypeIndexField("sU1ScSettlementSection", (int)valueAttempted), () => sU1ScGfeSection = value, sUseGFEDataForSCFields);
            }
        }

        [DependsOn(nameof(sU2ScSettlementSection), nameof(sU2ScGfeSection), nameof(sUseGFEDataForSCFields))]
        public E_GfeSectionT sU2ScSettlementSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetSCField(() => GetTypeIndexField("sU2ScSettlementSection"), () => (int)sU2ScGfeSection, sUseGFEDataForSCFields);
                if (v == E_GfeSectionT.B4 || v == E_GfeSectionT.B6 || v == E_GfeSectionT.NotApplicable)
                {
                    // v is unchanged for now.
                }
                else
                {
                    v = E_GfeSectionT.B4;
                }
                if (v == E_GfeSectionT.B4 && BrokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    v = E_GfeSectionT.B6;
                }
                return v;
            }
            set
            {
                var valueAttempted = value;
                if (valueAttempted == E_GfeSectionT.B4 && BrokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    Tools.LogError("Programming error.  Attempted to set to set GfeSectionT to B4 when IsB4AndB6DisabledIn1100And1300OfGfe.  Setting it to B6.", new Exception());
                    valueAttempted = E_GfeSectionT.B6;
                }
                SetSCField(() => SetTypeIndexField("sU2ScSettlementSection", (int)valueAttempted), () => sU2ScGfeSection = value, sUseGFEDataForSCFields); 
            }
        }

        [DependsOn(nameof(sU3ScSettlementSection), nameof(sU3ScGfeSection), nameof(sUseGFEDataForSCFields))]
        public E_GfeSectionT sU3ScSettlementSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetSCField(() => GetTypeIndexField("sU3ScSettlementSection"), () => (int)sU3ScGfeSection, sUseGFEDataForSCFields);
                if (v == E_GfeSectionT.B4 || v == E_GfeSectionT.B6 || v == E_GfeSectionT.NotApplicable)
                {
                    // v is unchanged for now.
                }
                else
                {
                    v = E_GfeSectionT.B4;
                }
                if (v == E_GfeSectionT.B4 && BrokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    v = E_GfeSectionT.B6;
                }
                return v;
            }
            set
            {
                var valueAttempted = value;
                if (valueAttempted == E_GfeSectionT.B4 && BrokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    Tools.LogError("Programming error.  Attempted to set to set GfeSectionT to B4 when IsB4AndB6DisabledIn1100And1300OfGfe.  Setting it to B6.", new Exception());
                    valueAttempted = E_GfeSectionT.B6;
                }
                SetSCField(() => SetTypeIndexField("sU3ScSettlementSection", (int)valueAttempted), () => sU3ScGfeSection = value, sUseGFEDataForSCFields);
            }
        }

        [DependsOn(nameof(sU4ScSettlementSection), nameof(sU4ScGfeSection), nameof(sUseGFEDataForSCFields))]
        public E_GfeSectionT sU4ScSettlementSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetSCField(() => GetTypeIndexField("sU4ScSettlementSection"), () => (int)sU4ScGfeSection, sUseGFEDataForSCFields);
                if (v == E_GfeSectionT.B4 || v == E_GfeSectionT.B6 || v == E_GfeSectionT.NotApplicable)
                {
                    // v is unchanged for now.
                }
                else
                {
                    v = E_GfeSectionT.B4;
                }
                if (v == E_GfeSectionT.B4 && BrokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    v = E_GfeSectionT.B6;
                }
                return v;
            }
            set
            {
                var valueAttempted = value;
                if (valueAttempted == E_GfeSectionT.B4 && BrokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    Tools.LogError("Programming error.  Attempted to set to set GfeSectionT to B4 when IsB4AndB6DisabledIn1100And1300OfGfe.  Setting it to B6.", new Exception());
                    valueAttempted = E_GfeSectionT.B6;
                }
                SetSCField(() => SetTypeIndexField("sU4ScSettlementSection", (int)valueAttempted), () => sU4ScGfeSection = value, sUseGFEDataForSCFields);
            }
        }

        [DependsOn(nameof(sU5ScSettlementSection), nameof(sU5ScGfeSection), nameof(sUseGFEDataForSCFields))]
        public E_GfeSectionT sU5ScSettlementSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetSCField(() => GetTypeIndexField("sU5ScSettlementSection"), () => (int)sU5ScGfeSection, sUseGFEDataForSCFields);
                if (v == E_GfeSectionT.B4 || v == E_GfeSectionT.B6 || v == E_GfeSectionT.NotApplicable)
                {
                    // v is unchanged for now.
                }
                else
                {
                    v = E_GfeSectionT.B4;
                }
                if (v == E_GfeSectionT.B4 && BrokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    v = E_GfeSectionT.B6;
                }
                return v;

            }
            set
            {
                var valueAttempted = value;
                if (valueAttempted == E_GfeSectionT.B4 && BrokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    Tools.LogError("Programming error.  Attempted to set to set GfeSectionT to B4 when IsB4AndB6DisabledIn1100And1300OfGfe.  Setting it to B6.", new Exception());
                    valueAttempted = E_GfeSectionT.B6;
                }
                SetSCField(() => SetTypeIndexField("sU5ScSettlementSection", (int)valueAttempted), () => sU5ScGfeSection = value, sUseGFEDataForSCFields);
            }
        }

        #endregion

        [DependsOn(nameof(sSettlementRequiredServicesTotalFee), nameof(sSettlementLenderTitleTotalFee), nameof(sSettlementOwnerTitleTotalFee), nameof(sSettlementServicesYouShopTotalFee), nameof(sSettlementGovtRecTotalFee), nameof(sSettlementTransferTaxTotalFee), nameof(sSettlementInitialImpoundDeposit), nameof(sSettlementDailyInterestTotalFee), nameof(sSettlementHomeOwnerInsuranceTotalFee))]
        private decimal sSettlementTotalOtherSettlementServiceFee
        {
            get
            {
                return SumMoney(sSettlementRequiredServicesTotalFee, sSettlementLenderTitleTotalFee, sSettlementOwnerTitleTotalFee,
                    sSettlementServicesYouShopTotalFee, sSettlementGovtRecTotalFee, sSettlementTransferTaxTotalFee,
                    sSettlementInitialImpoundDeposit, sSettlementDailyInterestTotalFee, sSettlementHomeOwnerInsuranceTotalFee);
            }
        }
        public string sSettlementTotalOtherSettlementServiceFee_rep
        {
            get { return ToMoneyString(() => sSettlementTotalOtherSettlementServiceFee); }
        }

        [DependsOn(nameof(sSettlementHazInsPia), nameof(sSettlement904Pia), nameof(s904PiaSettlementSection), nameof(sSettlement900U1Pia), nameof(s900U1PiaSettlementSection), 
            nameof(sClosingCostFeeVersionT), nameof(sClosingCostSet))]
        [DevNote("The total of the closing costs that go to GFE Box 11.")]
        private decimal sSettlementHomeOwnerInsuranceTotalFee
        {
            get
            {
                if (this.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.ClosingCostFee2015)
                {
                    return this.sClosingCostSet.Sum(fee => fee.GfeSectionT == E_GfeSectionT.B11);
                }

                var list = new[] {
                    new { Fee = sSettlementHazInsPia, GfeSection = E_GfeSectionT.B11},
                    new { Fee = sSettlement904Pia, GfeSection = s904PiaSettlementSection},
                    new { Fee = sSettlement900U1Pia, GfeSection = s900U1PiaSettlementSection},
                };
                decimal total = 0;
                foreach (var o in list)
                {
                    if (o.Fee > 0 && o.GfeSection == E_GfeSectionT.B11)
                    {
                        total = SumMoney(total, o.Fee);
                    }
                }
                return total;
            }
        }
        public string sSettlementHomeOwnerInsuranceTotalFee_rep
        {
            get { return ToMoneyString(() => sSettlementHomeOwnerInsuranceTotalFee); }
        }

        [DependsOn(nameof(sSettlementIPia))]
        private decimal sSettlementDailyInterestTotalFee
        {
            get
            {
                return sSettlementIPia;
            }
        }
        public string sSettlementDailyInterestTotalFee_rep
        {
            get { return ToMoneyString(() => sSettlementDailyInterestTotalFee); }
        }

        [DependsOn(nameof(sSettlementAggregateAdjRsrv), nameof(sSettlementHazInsRsrv), nameof(sSettlementHazInsRsrvProps), nameof(sSettlementMInsRsrv), nameof(sSettlementMInsRsrvProps), nameof(sSettlementSchoolTxRsrv), nameof(sSettlementSchoolTxRsrvProps), nameof(sSettlementRealETxRsrv), nameof(sSettlementRealETxRsrvProps), nameof(sSettlementFloodInsRsrv), nameof(sSettlementFloodInsRsrvProps), nameof(sSettlement1008Rsrv), nameof(sSettlement1008RsrvProps), nameof(sSettlement1009Rsrv), nameof(sSettlement1009RsrvProps), nameof(sSettlementU3Rsrv), nameof(sSettlementU4Rsrv))]
        private decimal sSettlementInitialImpoundDeposit
        {
            get
            {
                var list = new[]{
                               new { Fee = sSettlementHazInsRsrv},
                               new { Fee = sSettlementMInsRsrv},
                               new { Fee = sSettlementSchoolTxRsrv},
                               new { Fee = sSettlementRealETxRsrv},
                               new { Fee = sSettlementFloodInsRsrv},
                               new { Fee = sSettlement1008Rsrv},
                               new { Fee = sSettlement1009Rsrv},
                               new { Fee = sSettlementU3Rsrv},
                               new { Fee = sSettlementU4Rsrv},
                               new { Fee = sSettlementAggregateAdjRsrv},

                           };

                decimal total = 0;
                foreach (var o in list)
                {
                    if (o.Fee != 0)
                    {
                        total = SumMoney(total, o.Fee);
                    }
                }
                return total;
            }
        }
        public string sSettlementInitialImpoundDeposit_rep
        {
            get { return ToMoneyString(() => sSettlementInitialImpoundDeposit); }
        }

        [DependsOn(nameof(sSettlementCountyRtc), nameof(sSettlementStateRtc), nameof(sSettlementU1GovRtc), nameof(sSettlementU2GovRtc), nameof(sSettlementU3GovRtc))]
        private decimal sSettlementTransferTaxTotalFee
        {
            get
            {
                var list = new[]{
                               new { Fee = sSettlementCountyRtc},
                               new { Fee = sSettlementStateRtc},
                               new { Fee = sSettlementU1GovRtc},
                               new { Fee = sSettlementU2GovRtc},
                               new { Fee = sSettlementU3GovRtc},

                           };

                decimal total = 0;
                foreach (var o in list)
                {
                    if (o.Fee != 0)
                    {
                        total = SumMoney(total, o.Fee);
                    }
                }
                return total;
            }
        }
        public string sSettlementTransferTaxTotalFee_rep
        {
            get { return ToMoneyString(() => sSettlementTransferTaxTotalFee); }
        }

        [DependsOn(nameof(sSettlementRecF))]
        private decimal sSettlementGovtRecTotalFee
        {
            get
            {
                return sSettlementRecF;
            }
        }
        public string sSettlementGovtRecTotalFee_rep
        {
            get { return ToMoneyString(() => sSettlementGovtRecTotalFee); }
        }

        [DependsOn(nameof(sSettlementPestInspectF), nameof(sSettlementDocPrepF), nameof(sDocPrepFSettlementSection), nameof(sSettlementNotaryF), nameof(sNotaryFSettlementSection), nameof(sSettlementAttorneyF), nameof(sAttorneyFSettlementSection), nameof(sSettlementTitleInsF), nameof(sTitleInsFSettlementSection), nameof(sSettlementU1Tc), nameof(sU1TcSettlementSection), 
            nameof(sSettlementU2Tc), nameof(sU2TcSettlementSection), nameof(sSettlementU3Tc), nameof(sU3TcSettlementSection), nameof(sSettlementU4Tc), nameof(sU4TcSettlementSection), nameof(sSettlementU1Sc), nameof(sU1ScSettlementSection), nameof(sSettlementU2Sc), nameof(sU2ScSettlementSection), 
            nameof(sSettlementU3Sc), nameof(sU3ScSettlementSection), nameof(sSettlementU4Sc), nameof(sU4ScSettlementSection), nameof(sSettlementU5Sc), nameof(sU5ScSettlementSection), nameof(sSettlementEscrowF), nameof(sEscrowFSettlementSection))]
        private decimal sSettlementServicesYouShopTotalFee
        {
            get
            {

                var list = new[]{
                               new { Fee = sSettlementPestInspectF, GfeSection = E_GfeSectionT.B6},
                               new { Fee = sSettlementEscrowF, GfeSection = sEscrowFSettlementSection},
                               new { Fee = sSettlementDocPrepF, GfeSection = sDocPrepFSettlementSection},
                               new { Fee = sSettlementNotaryF, GfeSection = sNotaryFSettlementSection},
                               new { Fee = sSettlementAttorneyF, GfeSection = sAttorneyFSettlementSection},
                               new { Fee = sSettlementTitleInsF, GfeSection = sTitleInsFSettlementSection},
                               new { Fee = sSettlementU1Tc, GfeSection = sU1TcSettlementSection},
                               new { Fee = sSettlementU2Tc, GfeSection = sU2TcSettlementSection},
                               new { Fee = sSettlementU3Tc, GfeSection = sU3TcSettlementSection},
                               new { Fee = sSettlementU4Tc, GfeSection = sU4TcSettlementSection},
                               new { Fee = sSettlementU1Sc, GfeSection = sU1ScSettlementSection},
                               new { Fee = sSettlementU2Sc, GfeSection = sU2ScSettlementSection},
                               new { Fee = sSettlementU3Sc, GfeSection = sU3ScSettlementSection},
                               new { Fee = sSettlementU4Sc, GfeSection = sU4ScSettlementSection},
                               new { Fee = sSettlementU5Sc, GfeSection = sU5ScSettlementSection},

                           };


                decimal total = 0;
                foreach (var o in list)
                {
                    if (o.Fee > 0 && o.GfeSection == E_GfeSectionT.B6)
                    {
                        total = SumMoney(total, o.Fee);
                    }
                }
                return total;
            }
        }
        public string sSettlementServicesYouShopTotalFee_rep
        {
            get { return ToMoneyString(() => sSettlementServicesYouShopTotalFee); }
        }

        [DependsOn(nameof(sSettlementOwnerTitleInsF), nameof(sSettlementOwnerTitleInsFProps))]
        private decimal sSettlementOwnerTitleTotalFee
        {
            get
            {
                if (sSettlementOwnerTitleInsF > 0)
                {
                    return sSettlementOwnerTitleInsF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementOwnerTitleTotalFee_rep
        {
            get { return ToMoneyString(() => sSettlementOwnerTitleTotalFee); }
        }

        [DependsOn(nameof(sSettlementEscrowF), nameof(sEscrowFSettlementSection), nameof(sSettlementDocPrepF), nameof(sDocPrepFSettlementSection), nameof(sSettlementNotaryF), nameof(sNotaryFSettlementSection), 
            nameof(sSettlementAttorneyF), nameof(sAttorneyFSettlementSection), nameof(sSettlementTitleInsF), nameof(sTitleInsFSettlementSection), nameof(sSettlementU1Tc), nameof(sU1TcSettlementSection), 
            nameof(sSettlementU2Tc), nameof(sU2TcSettlementSection), nameof(sSettlementU3Tc), nameof(sU3TcSettlementSection), nameof(sSettlementU4Tc), nameof(sU4TcSettlementSection), nameof(sSettlementU1Sc), nameof(sU1ScSettlementSection), nameof(sSettlementU2Sc), nameof(sU2ScSettlementSection), 
            nameof(sSettlementU3Sc), nameof(sU3ScSettlementSection), nameof(sSettlementU4Sc), nameof(sU4ScSettlementSection), nameof(sSettlementU5Sc), nameof(sU5ScSettlementSection))]
        private decimal sSettlementLenderTitleTotalFee
        {
            get
            {
                var list = new[]{
                               new { Fee = sSettlementEscrowF, GfeSection = sEscrowFSettlementSection},
                               new { Fee = sSettlementDocPrepF, GfeSection = sDocPrepFSettlementSection},
                               new { Fee = sSettlementNotaryF, GfeSection = sNotaryFSettlementSection},
                               new { Fee = sSettlementAttorneyF, GfeSection = sAttorneyFSettlementSection},
                               new { Fee = sSettlementTitleInsF, GfeSection = sTitleInsFSettlementSection},
                               new { Fee = sSettlementU1Tc, GfeSection = sU1TcSettlementSection},
                               new { Fee = sSettlementU2Tc, GfeSection = sU2TcSettlementSection},
                               new { Fee = sSettlementU3Tc, GfeSection = sU3TcSettlementSection},
                               new { Fee = sSettlementU4Tc, GfeSection = sU4TcSettlementSection},
                               new { Fee = sSettlementU1Sc, GfeSection = sU1ScSettlementSection},
                               new { Fee = sSettlementU2Sc, GfeSection = sU2ScSettlementSection},
                               new { Fee = sSettlementU3Sc, GfeSection = sU3ScSettlementSection},
                               new { Fee = sSettlementU4Sc, GfeSection = sU4ScSettlementSection},
                               new { Fee = sSettlementU5Sc, GfeSection = sU5ScSettlementSection},

                           };


                decimal total = 0;
                foreach (var o in list)
                {
                    if (o.Fee > 0 && o.GfeSection == E_GfeSectionT.B4)
                    {
                        total = SumMoney(total, o.Fee);
                    }
                }
                return total;
            }
        }
        public string sSettlementLenderTitleTotalFee_rep
        {
            get { return ToMoneyString(() => sSettlementLenderTitleTotalFee); }
        }

        [DependsOn(nameof(sSettlementApprF), nameof(sSettlementApprFProps), nameof(sSettlementCrF), nameof(sSettlementCrFProps), nameof(sSettlementInspectF), nameof(sSettlementInspectFProps), nameof(sSettlementMBrokF), nameof(sSettlementMBrokFProps), nameof(sSettlementTxServF), nameof(sSettlementTxServFProps), 
            nameof(sSettlementProcF), nameof(sSettlementProcFProps), nameof(sSettlementUwF), nameof(sSettlementUwFProps), nameof(sSettlementWireF), nameof(sSettlementWireFProps), nameof(sSettlementFloodCertificationF), 
            nameof(sSettlementFloodCertificationFProps), nameof(sSettlement800U1F), nameof(sSettlement800U1FProps), nameof(sSettlement800U2F), nameof(sSettlement800U2FProps), nameof(sSettlement800U3F), nameof(sSettlement800U3FProps), nameof(sSettlement800U4F), nameof(sSettlement800U4FProps), nameof(sSettlement800U5F), nameof(sSettlement800U5FProps), 
            nameof(s800U1FSettlementSection), nameof(s800U2FSettlementSection), nameof(s800U3FSettlementSection), nameof(s800U5FSettlementSection), nameof(sMipPia), nameof(sSettlement904Pia), nameof(sVaFf), nameof(sSettlement900U1Pia), nameof(s904PiaSettlementSection), nameof(s900U1PiaSettlementSection), 
            nameof(sClosingCostFeeVersionT), nameof(sClosingCostSet), nameof(s800U4FSettlementSection))]
        private decimal sSettlementRequiredServicesTotalFee
        {
            get
            {
                if (this.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.ClosingCostFee2015)
                {
                    return this.sClosingCostSet.Sum(fee => fee.GfeSectionT == E_GfeSectionT.B3);
                }

                var list = new[]{
                               new { Fee = sSettlementApprF, GfeSection = E_GfeSectionT.B3},
                               new { Fee = sSettlementCrF, GfeSection = E_GfeSectionT.B3}, 
                               new { Fee = sSettlementTxServF, GfeSection = E_GfeSectionT.B3},
                               new { Fee = sSettlementFloodCertificationF, GfeSection = E_GfeSectionT.B3}, 
                               new { Fee = sSettlement800U1F, GfeSection = s800U1FSettlementSection},
                               new { Fee = sSettlement800U2F, GfeSection = s800U2FSettlementSection},
                               new { Fee = sSettlement800U3F, GfeSection = s800U3FSettlementSection},
                               new { Fee = sSettlement800U4F, GfeSection = s800U4FSettlementSection},
                               new { Fee = sSettlement800U5F, GfeSection = s800U5FSettlementSection},
                               new { Fee = sMipPia, GfeSection = E_GfeSectionT.B3}, // Per the spec, use the original GFE field
                               new { Fee = sSettlement904Pia, GfeSection = s904PiaSettlementSection},
                               new { Fee = sVaFf, GfeSection = E_GfeSectionT.B3}, // Per the spec, use the original GFE field
                               new { Fee = sSettlement900U1Pia, GfeSection = s900U1PiaSettlementSection},
                           };

                decimal total = 0;
                foreach (var o in list)
                {
                    if (o.Fee > 0 && o.GfeSection == E_GfeSectionT.B3)
                    {
                        total = SumMoney(total, o.Fee);
                    }
                }
                return total;
            }
        }
        public string sSettlementRequiredServicesTotalFee_rep
        {
            get { return ToMoneyString(() => sSettlementRequiredServicesTotalFee); }
        }

        [DependsOn(nameof(sSettlementLOrigF), nameof(sSettlementLOrigFProps), nameof(sGfeOriginatorCompF), nameof(sGfeOriginatorCompFProps), nameof(sSettlementLOrigBrokerCreditF), nameof(sSettlementLOrigBrokerCreditFProps), nameof(sSettlementApprF), nameof(sSettlementApprFProps), 
        nameof(sSettlementCrF), nameof(sSettlementCrFProps), nameof(sSettlementTxServF), nameof(sSettlementTxServFProps), nameof(sSettlementFloodCertificationF), nameof(sSettlementFloodCertificationFProps), nameof(sSettlementMBrokF), nameof(sSettlementMBrokFProps), 
        nameof(sSettlementInspectF), nameof(sSettlementInspectFProps), nameof(sSettlementProcF), nameof(sSettlementProcFProps), nameof(sSettlementUwF), nameof(sSettlementUwFProps), nameof(sSettlementWireF), nameof(sSettlementWireFProps), nameof(sSettlement800U1F), nameof(sSettlement800U1FProps), nameof(sSettlement800U2F), nameof(sSettlement800U2FProps), nameof(sSettlement800U3F), nameof(sSettlement800U3FProps), nameof(sSettlement800U4F), nameof(sSettlement800U4FProps), 
        nameof(sSettlement800U5F), nameof(sSettlement800U5FProps), nameof(sSettlementIPia), nameof(sSettlementIPiaProps), nameof(sMipPia), nameof(sMipPiaProps), nameof(sSettlementHazInsPia), nameof(sSettlementHazInsPiaProps), nameof(sSettlement904Pia), nameof(sSettlement904PiaProps), nameof(sVaFf), nameof(sVaFfProps), nameof(sSettlement900U1Pia), nameof(sSettlement900U1PiaProps), 
        nameof(sSettlementHazInsRsrv), nameof(sSettlementHazInsRsrvProps), nameof(sSettlementMInsRsrv), nameof(sSettlementMInsRsrvProps), nameof(sSettlementRealETxRsrv), nameof(sSettlementRealETxRsrvProps), nameof(sSettlementSchoolTxRsrv), nameof(sSettlementSchoolTxRsrvProps), nameof(sSettlementFloodInsRsrv), nameof(sSettlementFloodInsRsrvProps), 
        nameof(sSettlementAggregateAdjRsrv), nameof(sSettlementAggregateAdjRsrvProps), nameof(sSettlement1008Rsrv), nameof(sSettlement1008RsrvProps), nameof(sSettlement1009Rsrv), nameof(sSettlement1009RsrvProps), nameof(sSettlementEscrowF), nameof(sSettlementEscrowFProps), nameof(sSettlementOwnerTitleInsF), nameof(sSettlementOwnerTitleInsFProps), 
        nameof(sSettlementTitleInsF), nameof(sSettlementTitleInsFProps), nameof(sSettlementDocPrepF), nameof(sSettlementDocPrepFProps), nameof(sSettlementNotaryF), nameof(sSettlementNotaryFProps), nameof(sSettlementAttorneyF), nameof(sSettlementAttorneyFProps), nameof(sSettlementU1Tc), nameof(sSettlementU1TcProps), nameof(sSettlementU2Tc), nameof(sSettlementU2TcProps), 
        nameof(sSettlementU3Tc), nameof(sSettlementU3TcProps), nameof(sSettlementU4Tc), nameof(sSettlementU4TcProps), nameof(sSettlementRecF), nameof(sSettlementRecFProps), 
        nameof(sSettlementCountyRtc), nameof(sSettlementCountyRtcProps), nameof(sSettlementStateRtc), nameof(sSettlementStateRtcProps), nameof(sSettlementU1GovRtc), nameof(sSettlementU1GovRtcProps), nameof(sSettlementU2GovRtc), nameof(sSettlementU2GovRtcProps), 
        nameof(sSettlementU3GovRtc), nameof(sSettlementU3GovRtcProps), nameof(sSettlementPestInspectF), nameof(sSettlementPestInspectFProps), nameof(sSettlementU1Sc), nameof(sSettlementU1ScProps), nameof(sSettlementU2Sc), nameof(sSettlementU2ScProps), nameof(sSettlementU3Sc), nameof(sSettlementU3ScProps), nameof(sSettlementU4Sc), nameof(sSettlementU4ScProps), nameof(sSettlementU5Sc), nameof(sSettlementU5ScProps), 
        nameof(sSettlementCrFPaid), nameof(sSettlementApprFPaid), nameof(sSettlementProcFPaid), nameof(sSettlementBrokerCredit), nameof(sSettlementLDiscnt), nameof(sSettlementLDiscntProps), nameof(sBrokerId), nameof(sSettlementU3Rsrv), nameof(sSettlementU3RsrvProps), nameof(sSettlementU4Rsrv), nameof(sSettlementU4RsrvProps), 
        nameof(sUseGFEDataForSCFields), nameof(sClosingCostFeeVersionT), nameof(sGfeTotalFundByLender))]
        [DevNote("The total of all fees paid by the lender using the final numbers for the fees. Includes POC items if lender-paid.")]
        private decimal sSettlementTotalFeesPaidByLender
        {
            get
            {
                if (sUseGFEDataForSCFields && sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.ClosingCostFee2015)
                {
                    return sGfeTotalFundByLender;
                }

                List<decimal> toBeAdded = new List<decimal>();

                // 800
                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementLOrigFProps) == true)
                    toBeAdded.Add(sSettlementLOrigF);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sGfeOriginatorCompFProps) == true)
                    toBeAdded.Add(sGfeOriginatorCompF);

                // No Discount points

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementApprFProps) == true)
                    toBeAdded.Add(sSettlementApprF);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementCrFProps) == true)
                    toBeAdded.Add(sSettlementCrF);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementTxServFProps) == true)
                    toBeAdded.Add(sSettlementTxServF);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementFloodCertificationFProps) == true)
                    toBeAdded.Add(sSettlementFloodCertificationF);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementMBrokFProps) == true)
                    toBeAdded.Add(sSettlementMBrokF);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementInspectFProps) == true)
                    toBeAdded.Add(sSettlementInspectF);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementProcFProps) == true)
                    toBeAdded.Add(sSettlementProcF);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementUwFProps) == true)
                    toBeAdded.Add(sSettlementUwF);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementWireFProps) == true)
                    toBeAdded.Add(sSettlementWireF);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlement800U1FProps) == true)
                    toBeAdded.Add(sSettlement800U1F);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlement800U2FProps) == true)
                    toBeAdded.Add(sSettlement800U2F);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlement800U3FProps) == true)
                    toBeAdded.Add(sSettlement800U3F);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlement800U4FProps) == true)
                    toBeAdded.Add(sSettlement800U4F);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlement800U5FProps) == true)
                    toBeAdded.Add(sSettlement800U5F);

                // 900
                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementIPiaProps) == true)
                    toBeAdded.Add(sSettlementIPia);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sMipPiaProps) == true)
                    toBeAdded.Add(sMipPia);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementHazInsPiaProps) == true)
                    toBeAdded.Add(sSettlementHazInsPia);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlement904PiaProps) == true)
                    toBeAdded.Add(sSettlement904Pia);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sVaFfProps) == true)
                    toBeAdded.Add(sVaFf);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlement900U1PiaProps) == true)
                    toBeAdded.Add(sSettlement900U1Pia);

                // 1000
                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementHazInsRsrvProps) == true)
                    toBeAdded.Add(sSettlementHazInsRsrv);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementMInsRsrvProps) == true)
                    toBeAdded.Add(sSettlementMInsRsrv);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementRealETxRsrvProps) == true)
                    toBeAdded.Add(sSettlementRealETxRsrv);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementSchoolTxRsrvProps) == true)
                    toBeAdded.Add(sSettlementSchoolTxRsrv);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementFloodInsRsrvProps) == true)
                    toBeAdded.Add(sSettlementFloodInsRsrv);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementAggregateAdjRsrvProps) == true)
                    toBeAdded.Add(sSettlementAggregateAdjRsrv);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlement1008RsrvProps) == true)
                    toBeAdded.Add(sSettlement1008Rsrv);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlement1009RsrvProps) == true)
                    toBeAdded.Add(sSettlement1009Rsrv);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementU3RsrvProps) == true)
                    toBeAdded.Add(sSettlementU3Rsrv);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementU4RsrvProps) == true)
                    toBeAdded.Add(sSettlementU4Rsrv);

                // 1100
                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementEscrowFProps) == true)
                    toBeAdded.Add(sSettlementEscrowF);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementOwnerTitleInsFProps) == true)
                    toBeAdded.Add(sSettlementOwnerTitleInsF);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementTitleInsFProps) == true)
                    toBeAdded.Add(sSettlementTitleInsF);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementDocPrepFProps) == true)
                    toBeAdded.Add(sSettlementDocPrepF);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementNotaryFProps) == true)
                    toBeAdded.Add(sSettlementNotaryF);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementAttorneyFProps) == true)
                    toBeAdded.Add(sSettlementAttorneyF);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementU1TcProps) == true)
                    toBeAdded.Add(sSettlementU1Tc);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementU2TcProps) == true)
                    toBeAdded.Add(sSettlementU2Tc);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementU3TcProps) == true)
                    toBeAdded.Add(sSettlementU3Tc);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementU4TcProps) == true)
                    toBeAdded.Add(sSettlementU4Tc);

                // 1200
                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementRecFProps) == true)
                    toBeAdded.Add(sSettlementRecF);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementCountyRtcProps) == true)
                    toBeAdded.Add(sSettlementCountyRtc);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementStateRtcProps) == true)
                    toBeAdded.Add(sSettlementStateRtc);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementU1GovRtcProps) == true)
                    toBeAdded.Add(sSettlementU1GovRtc);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementU2GovRtcProps) == true)
                    toBeAdded.Add(sSettlementU2GovRtc);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementU3GovRtcProps) == true)
                    toBeAdded.Add(sSettlementU3GovRtc);

                // 1300
                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementPestInspectFProps) == true)
                    toBeAdded.Add(sSettlementPestInspectF);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementU1ScProps) == true)
                    toBeAdded.Add(sSettlementU1Sc);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementU2ScProps) == true)
                    toBeAdded.Add(sSettlementU2Sc);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementU3ScProps) == true)
                    toBeAdded.Add(sSettlementU3Sc);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementU4ScProps) == true)
                    toBeAdded.Add(sSettlementU4Sc);

                if (LosConvert.GfeItemProps_PdByLenderOnly(sSettlementU5ScProps) == true)
                    toBeAdded.Add(sSettlementU5Sc);

                return SumMoney(toBeAdded);
            }
        }

        [DependsOn(nameof(sSettlementLOrigF), nameof(sSettlementLOrigFProps), nameof(sGfeOriginatorCompF), nameof(sGfeOriginatorCompFProps), nameof(sSettlementLOrigBrokerCreditF), nameof(sSettlementLOrigBrokerCreditFProps), nameof(sSettlementApprF), nameof(sSettlementApprFProps), 
        nameof(sSettlementCrF), nameof(sSettlementCrFProps), nameof(sSettlementTxServF), nameof(sSettlementTxServFProps), nameof(sSettlementFloodCertificationF), nameof(sSettlementFloodCertificationFProps), nameof(sSettlementMBrokF), nameof(sSettlementMBrokFProps),  
        nameof(sSettlementInspectF), nameof(sSettlementInspectFProps), nameof(sSettlementProcF), nameof(sSettlementProcFProps), nameof(sSettlementUwF), nameof(sSettlementUwFProps), nameof(sSettlementWireF), nameof(sSettlementWireFProps), nameof(sSettlement800U1F), nameof(sSettlement800U1FProps), nameof(sSettlement800U2F), nameof(sSettlement800U2FProps), nameof(sSettlement800U3F), nameof(sSettlement800U3FProps), nameof(sSettlement800U4F), nameof(sSettlement800U4FProps), 
        nameof(sSettlement800U5F), nameof(sSettlement800U5FProps), nameof(sSettlementIPia), nameof(sSettlementIPiaProps), nameof(sMipPia), nameof(sMipPiaProps), nameof(sSettlementHazInsPia), nameof(sSettlementHazInsPiaProps), nameof(sSettlement904Pia), nameof(sSettlement904PiaProps), nameof(sVaFf), nameof(sVaFfProps), nameof(sSettlement900U1Pia), nameof(sSettlement900U1PiaProps), 
        nameof(sSettlementHazInsRsrv), nameof(sSettlementHazInsRsrvProps), nameof(sSettlementMInsRsrv), nameof(sSettlementMInsRsrvProps), nameof(sSettlementRealETxRsrv), nameof(sSettlementRealETxRsrvProps), nameof(sSettlementSchoolTxRsrv), nameof(sSettlementSchoolTxRsrvProps), nameof(sSettlementFloodInsRsrv), nameof(sSettlementFloodInsRsrvProps), 
        nameof(sSettlementAggregateAdjRsrv), nameof(sSettlementAggregateAdjRsrvProps), nameof(sSettlement1008Rsrv), nameof(sSettlement1008RsrvProps), nameof(sSettlement1009Rsrv), nameof(sSettlement1009RsrvProps), nameof(sSettlementEscrowF), nameof(sSettlementEscrowFProps), nameof(sSettlementOwnerTitleInsF), nameof(sSettlementOwnerTitleInsFProps), 
        nameof(sSettlementTitleInsF), nameof(sSettlementTitleInsFProps), nameof(sSettlementDocPrepF), nameof(sSettlementDocPrepFProps), nameof(sSettlementNotaryF), nameof(sSettlementNotaryFProps), nameof(sSettlementAttorneyF), nameof(sSettlementAttorneyFProps), nameof(sSettlementU1Tc), nameof(sSettlementU1TcProps), nameof(sSettlementU2Tc), nameof(sSettlementU2TcProps),  
        nameof(sSettlementU3Tc), nameof(sSettlementU3TcProps), nameof(sSettlementU4Tc), nameof(sSettlementU4TcProps), nameof(sSettlementRecF), nameof(sSettlementRecFProps), 
        nameof(sSettlementCountyRtc), nameof(sSettlementCountyRtcProps), nameof(sSettlementStateRtc), nameof(sSettlementStateRtcProps), nameof(sSettlementU1GovRtc), nameof(sSettlementU1GovRtcProps), nameof(sSettlementU2GovRtc), nameof(sSettlementU2GovRtcProps),  
        nameof(sSettlementU3GovRtc), nameof(sSettlementU3GovRtcProps), nameof(sSettlementPestInspectF), nameof(sSettlementPestInspectFProps), nameof(sSettlementU1Sc), nameof(sSettlementU1ScProps), nameof(sSettlementU2Sc), nameof(sSettlementU2ScProps), nameof(sSettlementU3Sc), nameof(sSettlementU3ScProps), nameof(sSettlementU4Sc), nameof(sSettlementU4ScProps), nameof(sSettlementU5Sc), nameof(sSettlementU5ScProps), 
        nameof(sSettlementCrFPaid), nameof(sSettlementApprFPaid), nameof(sSettlementProcFPaid), nameof(sSettlementBrokerCredit), nameof(sSettlementLDiscnt), nameof(sSettlementLDiscntProps), nameof(sBrokerId), nameof(sSettlementU3Rsrv), nameof(sSettlementU3RsrvProps), nameof(sSettlementU4Rsrv), nameof(sSettlementU4RsrvProps), 
        nameof(sClosingCostFeeVersionT), nameof(sDisclosureRegulationT), nameof(sClosingCostSet), nameof(sUseGFEDataForSCFields), nameof(sfSumClosingCostFeeTotalsMatching))]
        [DevNote("The total of fees paid by the lender at closing. Does not include POC fees, misc lender credits, nor tolerance cures--sSettlementTotalFundByLenderAtClosing includes those.")]
        private decimal sSettlementTotalFundByLender
        {
            get
            {
                if (sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.ClosingCostFee2015)
                {
                    Func<LoanClosingCostFee, bool> feeFilter;

                    if (sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
                    {
                        feeFilter = (fee) =>
                        fee.ClosingCostFeeTypeId != DefaultSystemClosingCostFee.Hud800CreditOrChargeFeeTypeId &&
                        fee.ClosingCostFeeTypeId != DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId &&
                        fee.ClosingCostFeeTypeId != DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId;
                    }
                    else
                    {
                        feeFilter = (fee) =>
                        fee.ClosingCostFeeTypeId != DefaultSystemClosingCostFee.Hud800CreditOrChargeFeeTypeId &&
                        fee.ClosingCostFeeTypeId != DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId;
                    }

                    Func<LoanClosingCostFeePayment, bool> paymentFilter = (payment) =>
                        payment.PaidByT == E_ClosingCostFeePaymentPaidByT.Lender &&
                        payment.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.AtClosing;

                    return this.SumClosingCostFeeTotalsMatching(feeFilter, paymentFilter);
                }

                List<decimal> toBeAdded = new List<decimal>();

                // 800
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementLOrigFProps) == true)
                    toBeAdded.Add(sSettlementLOrigF);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sGfeOriginatorCompFProps) == true)
                    toBeAdded.Add(sGfeOriginatorCompF);

                // No Discount points

                if (sSettlementApprFPaid == false && LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementApprFProps) == true)
                    toBeAdded.Add(sSettlementApprF);

                if (sSettlementCrFPaid == false && LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementCrFProps) == true)
                    toBeAdded.Add(sSettlementCrF);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementTxServFProps) == true)
                    toBeAdded.Add(sSettlementTxServF);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementFloodCertificationFProps) == true)
                    toBeAdded.Add(sSettlementFloodCertificationF);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementMBrokFProps) == true)
                    toBeAdded.Add(sSettlementMBrokF);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementInspectFProps) == true)
                    toBeAdded.Add(sSettlementInspectF);

                if (sSettlementProcFPaid == false && LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementProcFProps) == true)
                    toBeAdded.Add(sSettlementProcF);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementUwFProps) == true)
                    toBeAdded.Add(sSettlementUwF);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementWireFProps) == true)
                    toBeAdded.Add(sSettlementWireF);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlement800U1FProps) == true)
                    toBeAdded.Add(sSettlement800U1F);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlement800U2FProps) == true)
                    toBeAdded.Add(sSettlement800U2F);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlement800U3FProps) == true)
                    toBeAdded.Add(sSettlement800U3F);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlement800U4FProps) == true)
                    toBeAdded.Add(sSettlement800U4F);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlement800U5FProps) == true)
                    toBeAdded.Add(sSettlement800U5F);

                // 900
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementIPiaProps) == true)
                    toBeAdded.Add(sSettlementIPia);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sMipPiaProps) == true)
                    toBeAdded.Add(sMipPia);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementHazInsPiaProps) == true)
                    toBeAdded.Add(sSettlementHazInsPia);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlement904PiaProps) == true)
                    toBeAdded.Add(sSettlement904Pia);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sVaFfProps) == true)
                    toBeAdded.Add(sVaFf);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlement900U1PiaProps) == true)
                    toBeAdded.Add(sSettlement900U1Pia);

                // 1000
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementHazInsRsrvProps) == true)
                    toBeAdded.Add(sSettlementHazInsRsrv);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementMInsRsrvProps) == true)
                    toBeAdded.Add(sSettlementMInsRsrv);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementRealETxRsrvProps) == true)
                    toBeAdded.Add(sSettlementRealETxRsrv);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementSchoolTxRsrvProps) == true)
                    toBeAdded.Add(sSettlementSchoolTxRsrv);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementFloodInsRsrvProps) == true)
                    toBeAdded.Add(sSettlementFloodInsRsrv);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementAggregateAdjRsrvProps) == true)
                    toBeAdded.Add(sSettlementAggregateAdjRsrv);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlement1008RsrvProps) == true)
                    toBeAdded.Add(sSettlement1008Rsrv);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlement1009RsrvProps) == true)
                    toBeAdded.Add(sSettlement1009Rsrv);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementU3RsrvProps) == true)
                    toBeAdded.Add(sSettlementU3Rsrv);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementU4RsrvProps) == true)
                    toBeAdded.Add(sSettlementU4Rsrv);

                // 1100
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementEscrowFProps) == true)
                    toBeAdded.Add(sSettlementEscrowF);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementOwnerTitleInsFProps) == true)
                    toBeAdded.Add(sSettlementOwnerTitleInsF);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementTitleInsFProps) == true)
                    toBeAdded.Add(sSettlementTitleInsF);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementDocPrepFProps) == true)
                    toBeAdded.Add(sSettlementDocPrepF);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementNotaryFProps) == true)
                    toBeAdded.Add(sSettlementNotaryF);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementAttorneyFProps) == true)
                    toBeAdded.Add(sSettlementAttorneyF);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementU1TcProps) == true)
                    toBeAdded.Add(sSettlementU1Tc);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementU2TcProps) == true)
                    toBeAdded.Add(sSettlementU2Tc);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementU3TcProps) == true)
                    toBeAdded.Add(sSettlementU3Tc);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementU4TcProps) == true)
                    toBeAdded.Add(sSettlementU4Tc);

                // 1200
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementRecFProps) == true)
                    toBeAdded.Add(sSettlementRecF);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementCountyRtcProps) == true)
                    toBeAdded.Add(sSettlementCountyRtc);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementStateRtcProps) == true)
                    toBeAdded.Add(sSettlementStateRtc);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementU1GovRtcProps) == true)
                    toBeAdded.Add(sSettlementU1GovRtc);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementU2GovRtcProps) == true)
                    toBeAdded.Add(sSettlementU2GovRtc);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementU3GovRtcProps) == true)
                    toBeAdded.Add(sSettlementU3GovRtc);

                // 1300
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementPestInspectFProps) == true)
                    toBeAdded.Add(sSettlementPestInspectF);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementU1ScProps) == true)
                    toBeAdded.Add(sSettlementU1Sc);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementU2ScProps) == true)
                    toBeAdded.Add(sSettlementU2Sc);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementU3ScProps) == true)
                    toBeAdded.Add(sSettlementU3Sc);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementU4ScProps) == true)
                    toBeAdded.Add(sSettlementU4Sc);

                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementU5ScProps) == true)
                    toBeAdded.Add(sSettlementU5Sc);

                return SumMoney(toBeAdded);
            }
        }

        [DependsOn(nameof(sUseGFEDataForSCFields), nameof(sDisclosureRegulationT), nameof(sLenderActualTotalCreditAmt), nameof(sLenderPaidBrokerCompF), nameof(sSettlementTotalFeesPaidByLender), nameof(sSettlementLenderCreditF), 
                   nameof(sIsAllowExcludeToleranceCure), nameof(sToleranceCureCalculationT), nameof(sToleranceCure), nameof(sLenderAdditionalCreditAtClosingManualAmt))]
        [DevNote("The total amount of all credits that the lender is making. Includes misc credits, tolerance cures, and lender-paid fees (including lender-paid POC fees).")]
        public decimal sSettlementTotalPaidByLender
        {
            get
            {
                // If GFE/SC data-sets are combined, follow the flow of the lender credit UI
                if (sUseGFEDataForSCFields)
                {
                    if (sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
                    {
                        return SumMoney(sLenderActualTotalCreditAmt, sLenderPaidBrokerCompF);
                    }
                    else
                    {
                        return sLenderActualTotalCreditAmt;
                    }
                }
                // If the GFE/SC data-sets are separate, add the additional credits to the SC lender-paid total
                else
                {
                    decimal v = SumMoney(sSettlementTotalFeesPaidByLender, -sSettlementLenderCreditF);

                    decimal toleranceCure = 0;
                    if (sIsAllowExcludeToleranceCure && sToleranceCureCalculationT == E_sToleranceCureCalculationT.Exclude)
                    {
                        toleranceCure = 0;
                    }
                    else
                    {
                        toleranceCure = sToleranceCure;
                    }
                    return SumMoney(v, toleranceCure, sLenderAdditionalCreditAtClosingManualAmt);
                }
            }
        }

        public string sSettlementTotalPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementTotalPaidByLender); }
        }

        [DevNote("Negative value of sSettlementTotalPaidByLender. Used to display (-) for credit and (+) for charge.")]
        [DependsOn(nameof(sSettlementTotalPaidByLender))]
        private decimal sSettlementTotalPaidByLender_Neg
        {
            get { return -1 * sSettlementTotalPaidByLender; }
        }

        public string sSettlementTotalPaidByLender_Neg_rep
        {
            get { return ToMoneyString(() => sSettlementTotalPaidByLender_Neg); }
        }

        [DependsOn(nameof(sUseGFEDataForSCFields), nameof(sSettlementTotalPaidByLender), nameof(sGfeTotalFundByLenderPoc), nameof(sGfeDiscountPointF), 
                   nameof(sSettlementTotalFundByLender), nameof(sSettlementLenderCreditF), nameof(sIsAllowExcludeToleranceCure), 
                   nameof(sToleranceCureCalculationT), nameof(sToleranceCure), nameof(sLenderAdditionalCreditAtClosingAmt), 
                   nameof(sLenderAdditionalCreditAtClosingManualAmt), nameof(sLenderPaidBrokerCompFPoc))]
        [DevNote("The final, total amount of all credits that the lender needs to make to closing. Excludes lender-paid fees that are POC.")]
        public decimal sSettlementTotalFundByLenderAtClosing
        {
            get 
            {
                if (sUseGFEDataForSCFields)
                {
                    // sGfeDiscountPoints is a fee amount, so need to negate it to convert to credit amount
                    // return sSettlementTotalPaidByLender - sGfeTotalFundByLenderPoc - (-sGfeDiscountPointF);
                    return SumMoney(sSettlementTotalPaidByLender, -sGfeTotalFundByLenderPoc, -sLenderPaidBrokerCompFPoc, sGfeDiscountPointF);
                }
                else
                {
                    decimal v = SumMoney(sSettlementTotalFundByLender, -sSettlementLenderCreditF);

                    decimal toleranceCure = 0;
                    if (sIsAllowExcludeToleranceCure && sToleranceCureCalculationT == E_sToleranceCureCalculationT.Exclude)
                    {
                        toleranceCure = 0;
                    } 
                    else 
                    {
                        toleranceCure = sToleranceCure;
                    }
                    return SumMoney(v, toleranceCure, sLenderAdditionalCreditAtClosingManualAmt);

                }
            }
        }

        public string sSettlementTotalFundByLenderAtClosing_rep
        {
            get { return ToMoneyString(() => sSettlementTotalFundByLenderAtClosing); }
        }

        [DevNote("OPM 209739 - Negative value of sSettlementTotalFundByLenderAtClosing. Used to display (-) for credit and (+) for charge.")]
        [DependsOn(nameof(sSettlementTotalFundByLenderAtClosing))]
        private decimal sSettlementTotalFundByLenderAtClosing_Neg
        {
            get { return -1 * sSettlementTotalFundByLenderAtClosing; }
        }

        public string sSettlementTotalFundByLenderAtClosing_Neg_rep
        {
            get { return ToMoneyString(() => sSettlementTotalFundByLenderAtClosing_Neg); }
        }

        [DependsOn(nameof(sDisclosureRegulationT), nameof(sLenderActualInitialCreditAmt), nameof(sLenderAdditionalCreditAtClosingAmt), nameof(sLenderPaidBrokerCompCreditF), nameof(sGfeDiscountPointF), nameof(sLenderInitialCreditToBorrowerAmt), 
            nameof(sLenderAdditionalCreditAtClosingManualAmt), nameof(sToleranceCure), nameof(sIsAllowExcludeToleranceCure), nameof(sToleranceCureCalculationT), nameof(sLenderCreditCalculationMethodT))]
        [DevNote("Total credit to borrower. Does not include lender-paid broker compensation (if any) in TRID mode")]
        public decimal sLenderActualTotalCreditAmt
        {
            get
            {
                if (sLenderCreditCalculationMethodT == E_sLenderCreditCalculationMethodT.SetManually)
                {
                    decimal toleranceCure = 0;
                    if (sIsAllowExcludeToleranceCure && sToleranceCureCalculationT == E_sToleranceCureCalculationT.Exclude)
                    {
                        toleranceCure = 0;
                    }
                    else
                    {
                        toleranceCure = sToleranceCure;
                    }
                    return SumMoney(sLenderPaidBrokerCompCreditF, -sGfeDiscountPointF, sLenderInitialCreditToBorrowerAmt, sLenderAdditionalCreditAtClosingManualAmt, toleranceCure);
                }
                else
                {
                    return SumMoney(sLenderActualInitialCreditAmt, sLenderAdditionalCreditAtClosingAmt, sToleranceCure);
                }
            }
        }

        public string sLenderActualTotalCreditAmt_rep
        {
            get { return ToMoneyString(() => sLenderActualTotalCreditAmt); }
        }

        [DevNote("OPM 209739 - Negative value of sLenderActualTotalCreditAmt. Used to display (-) for credit and (+) for charge.")]
        [DependsOn(nameof(sLenderActualTotalCreditAmt))]
        private decimal sLenderActualTotalCreditAmt_Neg
        {
            get { return -1 * sLenderActualTotalCreditAmt; }
        }

        public string sLenderActualTotalCreditAmt_Neg_rep
        {
            get { return ToMoneyString(() => sLenderActualTotalCreditAmt_Neg); }
        }

        [DevNote("Settlement total deducted from loan proceeds.")]
        [DependsOn(nameof(sf_sSettlementTotalDedFromLoanProc_LegacyCalc), nameof(sClosingCostFeeVersionT), nameof(CalculateSettlementTotalDedFromLoanProc), nameof(sLoanVersionT))]
        public decimal sSettlementTotalDedFromLoanProc
        {
            get
            {
                switch (sClosingCostFeeVersionT)
                {
                    case E_sClosingCostFeeVersionT.Legacy:
                        return sSettlementTotalDedFromLoanProc_LegacyCalc();
                    case E_sClosingCostFeeVersionT.LegacyButMigrated:
                        if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V33_IncludeNonBorrowerResponsibleFeesInFundingWorksheet))
                        {
                            return CalculateSettlementTotalDedFromLoanProc();
                        }

                        return sSettlementTotalDedFromLoanProc_LegacyCalc();
                    case E_sClosingCostFeeVersionT.ClosingCostFee2015:
                        return CalculateSettlementTotalDedFromLoanProc();
                    default:
                        throw new UnhandledEnumException(sClosingCostFeeVersionT);
                }
            }
        }

        [DependsOn(nameof(sLoanVersionT), nameof(sSellerResponsibleClosingCostSet), nameof(sfSumClosingCostFeeTotalsMatching))]
        private decimal CalculateSettlementTotalDedFromLoanProc()
        {
            decimal borrowerResponsibleSettlementTotalDflp = this.SumClosingCostFeeTotalsMatching(f => f.Dflp, p => p.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.AtClosing);
            decimal nonBorrowerResponsibleSettlementTotalDflp = 0M;

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V33_IncludeNonBorrowerResponsibleFeesInFundingWorksheet))
            {
                nonBorrowerResponsibleSettlementTotalDflp = this.sSellerResponsibleClosingCostSet.Sum(f => f.Dflp, p => p.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.AtClosing);
            }

            return this.SumMoney(borrowerResponsibleSettlementTotalDflp, nonBorrowerResponsibleSettlementTotalDflp);
        }

        public string sSettlementTotalDedFromLoanProc_rep
        {
            get { return ToMoneyString(() => sSettlementTotalDedFromLoanProc); }
        }

        [DependsOn(nameof(sSettlementLOrigF), nameof(sSettlementLOrigFProps), nameof(sSettlementLDiscnt), nameof(sSettlementLDiscntProps), nameof(sSettlementDiscountPointF), nameof(sSettlementDiscountPointFProps), nameof(sSettlementApprF), nameof(sSettlementApprFProps), 
        nameof(sSettlementCrF), nameof(sSettlementCrFProps), nameof(sSettlementTxServF), nameof(sSettlementTxServFProps), nameof(sSettlementFloodCertificationF), nameof(sSettlementFloodCertificationFProps), nameof(sSettlementMBrokF), nameof(sSettlementMBrokFProps), nameof(sSettlementInspectF), nameof(sSettlementInspectFProps), nameof(sSettlementProcF), nameof(sSettlementProcFProps), 
        nameof(sSettlementUwF), nameof(sSettlementUwFProps), nameof(sSettlementWireF), nameof(sSettlementWireFProps), nameof(sSettlement800U1F), nameof(sSettlement800U1FProps), nameof(sSettlement800U2F), nameof(sSettlement800U2FProps), nameof(sSettlement800U3F), nameof(sSettlement800U3FProps), nameof(sSettlement800U4F), nameof(sSettlement800U4FProps), 
        nameof(sSettlement800U5F), nameof(sSettlement800U5FProps), nameof(sSettlementIPia), nameof(sSettlementIPiaProps), nameof(sMipPia), nameof(sMipPiaProps), nameof(sSettlementHazInsPia), nameof(sSettlementHazInsPiaProps), nameof(sSettlement904Pia), nameof(sSettlement904PiaProps), nameof(sVaFf), nameof(sVaFfProps), 
        nameof(sSettlement900U1Pia), nameof(sSettlement900U1PiaProps), nameof(sSettlementHazInsRsrv), nameof(sSettlementHazInsRsrvProps), nameof(sSettlementMInsRsrv), nameof(sSettlementMInsRsrvProps), nameof(sSettlementRealETxRsrv), nameof(sSettlementRealETxRsrvProps), nameof(sSettlementSchoolTxRsrv), nameof(sSettlementSchoolTxRsrvProps), nameof(sSettlementFloodInsRsrv), nameof(sSettlementFloodInsRsrvProps), 
        nameof(sSettlementAggregateAdjRsrv), nameof(sSettlementAggregateAdjRsrvProps), nameof(sSettlement1008Rsrv), nameof(sSettlement1008RsrvProps), nameof(sSettlement1009Rsrv), nameof(sSettlement1009RsrvProps), nameof(sSettlementEscrowF), nameof(sSettlementEscrowFProps), nameof(sSettlementOwnerTitleInsF), nameof(sSettlementOwnerTitleInsFProps), 
        nameof(sSettlementTitleInsF), nameof(sSettlementTitleInsFProps), nameof(sSettlementDocPrepF), nameof(sSettlementDocPrepFProps), nameof(sSettlementNotaryF), nameof(sSettlementNotaryFProps), 
        nameof(sSettlementAttorneyF), nameof(sSettlementAttorneyFProps), nameof(sSettlementU1Tc), nameof(sSettlementU1TcProps), nameof(sSettlementU2Tc), nameof(sSettlementU2TcProps), nameof(sSettlementU3Tc), nameof(sSettlementU3TcProps), nameof(sSettlementU4Tc), nameof(sSettlementU4TcProps), nameof(sSettlementRecF), nameof(sSettlementRecFProps), 
        nameof(sSettlementCountyRtc), nameof(sSettlementCountyRtcProps), nameof(sSettlementStateRtc), nameof(sSettlementStateRtcProps), nameof(sSettlementU1GovRtc), nameof(sSettlementU1GovRtcProps), nameof(sSettlementU2GovRtc), nameof(sSettlementU2GovRtcProps), nameof(sSettlementU3GovRtc), nameof(sSettlementU3GovRtcProps), 
        nameof(sSettlementPestInspectF), nameof(sSettlementPestInspectFProps), nameof(sSettlementU1Sc), nameof(sSettlementU1ScProps), nameof(sSettlementU2Sc), nameof(sSettlementU2ScProps), nameof(sSettlementU3Sc), nameof(sSettlementU3ScProps), 
        nameof(sSettlementU4Sc), nameof(sSettlementU4ScProps), nameof(sSettlementU5Sc), nameof(sSettlementU5ScProps), nameof(sSettlementU3Rsrv), nameof(sSettlementU3RsrvProps), nameof(sSettlementU4Rsrv), nameof(sSettlementU4RsrvProps), nameof(sGfeOriginatorCompFProps), nameof(sGfeOriginatorCompF), nameof(sLoanVersionT))]
        private int sf_sSettlementTotalDedFromLoanProc_LegacyCalc { get { return -1; } }

        private decimal sSettlementTotalDedFromLoanProc_LegacyCalc()
        {
            List<decimal> toBeAdded = new List<decimal>();

            // 800
            if (LosConvert.GfeItemProps_Dflp(sSettlementLOrigFProps) == true)
                toBeAdded.Add(sSettlementLOrigF);

            if (LosConvert.GfeItemProps_Dflp(sGfeOriginatorCompFProps) == true)
                toBeAdded.Add(sGfeOriginatorCompF);

            if (LosConvert.GfeItemProps_Dflp(sSettlementDiscountPointFProps) == true)
                toBeAdded.Add(sSettlementDiscountPointF);

            if (LosConvert.GfeItemProps_Dflp(sSettlementApprFProps) == true)
                toBeAdded.Add(sSettlementApprF);

            if (LosConvert.GfeItemProps_Dflp(sSettlementCrFProps) == true)
                toBeAdded.Add(sSettlementCrF);

            if (LosConvert.GfeItemProps_Dflp(sSettlementTxServFProps) == true)
                toBeAdded.Add(sSettlementTxServF);

            if (LosConvert.GfeItemProps_Dflp(sSettlementFloodCertificationFProps) == true)
                toBeAdded.Add(sSettlementFloodCertificationF);

            if (LosConvert.GfeItemProps_Dflp(sSettlementMBrokFProps) == true)
                toBeAdded.Add(sSettlementMBrokF);

            if (LosConvert.GfeItemProps_Dflp(sSettlementInspectFProps) == true)
                toBeAdded.Add(sSettlementInspectF);

            if (LosConvert.GfeItemProps_Dflp(sSettlementProcFProps) == true)
                toBeAdded.Add(sSettlementProcF);

            if (LosConvert.GfeItemProps_Dflp(sSettlementUwFProps) == true)
                toBeAdded.Add(sSettlementUwF);

            if (LosConvert.GfeItemProps_Dflp(sSettlementWireFProps) == true)
                toBeAdded.Add(sSettlementWireF);

            if (LosConvert.GfeItemProps_Dflp(sSettlement800U1FProps) == true)
                toBeAdded.Add(sSettlement800U1F);

            if (LosConvert.GfeItemProps_Dflp(sSettlement800U2FProps) == true)
                toBeAdded.Add(sSettlement800U2F);

            if (LosConvert.GfeItemProps_Dflp(sSettlement800U3FProps) == true)
                toBeAdded.Add(sSettlement800U3F);

            if (LosConvert.GfeItemProps_Dflp(sSettlement800U4FProps) == true)
                toBeAdded.Add(sSettlement800U4F);

            if (LosConvert.GfeItemProps_Dflp(sSettlement800U5FProps) == true)
                toBeAdded.Add(sSettlement800U5F);

            // 900
            if (LosConvert.GfeItemProps_Dflp(sSettlementIPiaProps) == true)
                toBeAdded.Add(sSettlementIPia);

            if (LosConvert.GfeItemProps_Dflp(sMipPiaProps) == true)
                toBeAdded.Add(sMipPia);

            if (LosConvert.GfeItemProps_Dflp(sSettlementHazInsPiaProps) == true)
                toBeAdded.Add(sSettlementHazInsPia);

            if (LosConvert.GfeItemProps_Dflp(sSettlement904PiaProps) == true)
                toBeAdded.Add(sSettlement904Pia);

            if (LosConvert.GfeItemProps_Dflp(sVaFfProps) == true)
                toBeAdded.Add(sVaFf);

            if (LosConvert.GfeItemProps_Dflp(sSettlement900U1PiaProps) == true)
                toBeAdded.Add(sSettlement900U1Pia);

            if (LosConvert.GfeItemProps_Dflp(sSettlementHazInsRsrvProps) == true)
                toBeAdded.Add(sSettlementHazInsRsrv);

            // 1000
            if (LosConvert.GfeItemProps_Dflp(sSettlementMInsRsrvProps) == true)
                toBeAdded.Add(sSettlementMInsRsrv);

            if (LosConvert.GfeItemProps_Dflp(sSettlementRealETxRsrvProps) == true)
                toBeAdded.Add(sSettlementRealETxRsrv);

            if (LosConvert.GfeItemProps_Dflp(sSettlementSchoolTxRsrvProps) == true)
                toBeAdded.Add(sSettlementSchoolTxRsrv);

            if (LosConvert.GfeItemProps_Dflp(sSettlementFloodInsRsrvProps) == true)
                toBeAdded.Add(sSettlementFloodInsRsrv);

            if (LosConvert.GfeItemProps_Dflp(sSettlementAggregateAdjRsrvProps) == true)
                toBeAdded.Add(sSettlementAggregateAdjRsrv);

            if (LosConvert.GfeItemProps_Dflp(sSettlement1008RsrvProps) == true)
                toBeAdded.Add(sSettlement1008Rsrv);

            if (LosConvert.GfeItemProps_Dflp(sSettlement1009RsrvProps) == true)
                toBeAdded.Add(sSettlement1009Rsrv);

            if (LosConvert.GfeItemProps_Dflp(sSettlementU3RsrvProps) == true)
                toBeAdded.Add(sSettlementU3Rsrv);

            if (LosConvert.GfeItemProps_Dflp(sSettlementU4RsrvProps) == true)
                toBeAdded.Add(sSettlementU4Rsrv);

            // 1100
            if (LosConvert.GfeItemProps_Dflp(sSettlementEscrowFProps) == true)
                toBeAdded.Add(sSettlementEscrowF);

            if (LosConvert.GfeItemProps_Dflp(sSettlementOwnerTitleInsFProps) == true)
                toBeAdded.Add(sSettlementOwnerTitleInsF);

            if (LosConvert.GfeItemProps_Dflp(sSettlementTitleInsFProps) == true)
                toBeAdded.Add(sSettlementTitleInsF);

            if (LosConvert.GfeItemProps_Dflp(sSettlementDocPrepFProps) == true)
                toBeAdded.Add(sSettlementDocPrepF);

            if (LosConvert.GfeItemProps_Dflp(sSettlementNotaryFProps) == true)
                toBeAdded.Add(sSettlementNotaryF);

            if (LosConvert.GfeItemProps_Dflp(sSettlementAttorneyFProps) == true)
                toBeAdded.Add(sSettlementAttorneyF);

            if (LosConvert.GfeItemProps_Dflp(sSettlementU1TcProps) == true)
                toBeAdded.Add(sSettlementU1Tc);

            if (LosConvert.GfeItemProps_Dflp(sSettlementU2TcProps) == true)
                toBeAdded.Add(sSettlementU2Tc);

            if (LosConvert.GfeItemProps_Dflp(sSettlementU3TcProps) == true)
                toBeAdded.Add(sSettlementU3Tc);

            if (LosConvert.GfeItemProps_Dflp(sSettlementU4TcProps) == true)
                toBeAdded.Add(sSettlementU4Tc);

            // 1200
            if (LosConvert.GfeItemProps_Dflp(sSettlementRecFProps) == true)
                toBeAdded.Add(sSettlementRecF);

            if (LosConvert.GfeItemProps_Dflp(sSettlementCountyRtcProps) == true)
                toBeAdded.Add(sSettlementCountyRtc);

            if (LosConvert.GfeItemProps_Dflp(sSettlementStateRtcProps) == true)
                toBeAdded.Add(sSettlementStateRtc);

            if (LosConvert.GfeItemProps_Dflp(sSettlementU1GovRtcProps) == true)
                toBeAdded.Add(sSettlementU1GovRtc);

            if (LosConvert.GfeItemProps_Dflp(sSettlementU2GovRtcProps) == true)
                toBeAdded.Add(sSettlementU2GovRtc);

            if (LosConvert.GfeItemProps_Dflp(sSettlementU3GovRtcProps) == true)
                toBeAdded.Add(sSettlementU3GovRtc);

            // 1300
            if (LosConvert.GfeItemProps_Dflp(sSettlementPestInspectFProps) == true)
                toBeAdded.Add(sSettlementPestInspectF);

            if (LosConvert.GfeItemProps_Dflp(sSettlementU1ScProps) == true)
                toBeAdded.Add(sSettlementU1Sc);

            if (LosConvert.GfeItemProps_Dflp(sSettlementU2ScProps) == true)
                toBeAdded.Add(sSettlementU2Sc);

            if (LosConvert.GfeItemProps_Dflp(sSettlementU3ScProps) == true)
                toBeAdded.Add(sSettlementU3Sc);

            if (LosConvert.GfeItemProps_Dflp(sSettlementU4ScProps) == true)
                toBeAdded.Add(sSettlementU4Sc);

            if (LosConvert.GfeItemProps_Dflp(sSettlementU5ScProps) == true)
                toBeAdded.Add(sSettlementU5Sc);


            return SumMoney(toBeAdded);
        }

        #endregion

        #region Fields required for other calculations

        /*[DependsOn(nameof(sSettlementProHazIns), nameof(sSettlementProMIns), nameof(sSettlementProSchoolTx), nameof(sSettlementProRealETx), nameof(sSettlementProFloodIns), nameof(sSettlement1008ProHExp), nameof(sSettlement1009ProHExp))]
        private decimal sSettlementEscrowPmt
        {
            get
            {
                return sProHazIns + sProMIns + sProSchoolTx + sProRealETx + sProFloodIns + s1006ProHExp + s1007ProHExp;
            }
        }

        [DependsOn(nameof(sSettlementEscrowPmt))]
        private string sSettlementEscrowPmt_rep
        {
            get { return ToMoneyString(() => sSettlementEscrowPmt); }

        }*/

        /*[DependsOn(nameof(sInitialEscrowAcc), nameof(sSettlementProRealETx), nameof(sSettlementProHazIns), nameof(sSettlementProMIns), nameof(sSettlementProFloodIns), nameof(sSettlementProSchoolTx), nameof(sSettlement1008ProHExp), nameof(sSettlement1009ProHExp))]
        private decimal sSettlementAggrEscrowCushionRequired
        {
            get
            {
                int[,] initialEscrowTable = sInitialEscrowAcc; //TODO - verify this is the correct field and doesn't need a "settlement" version
                return sSettlementProRealETx * initialEscrowTable[0, 0] + sSettlementProHazIns * initialEscrowTable[0, 1] +
                    sSettlementProMIns * initialEscrowTable[0, 2] + sSettlementProFloodIns * initialEscrowTable[0, 3] +
                    sSettlementProSchoolTx * initialEscrowTable[0, 4] + sSettlement1008ProHExp * initialEscrowTable[0, 5] +
                    sSettlement1009ProHExp * initialEscrowTable[0, 6];
            }
        }*/

        [DependsOn(nameof(sSettlementRealETxRsrv), nameof(sSettlementHazInsRsrv), nameof(sSettlementMInsRsrv), nameof(sSettlementFloodInsRsrv), nameof(sSettlementSchoolTxRsrv), nameof(sSettlement1008Rsrv), nameof(sSettlement1009Rsrv), nameof(sSettlementU3Rsrv), nameof(sSettlementU4Rsrv))]
        public decimal sSettlementTotLenderRsrv
        {
            get
            {
                return sSettlementRealETxRsrv + sSettlementHazInsRsrv + sSettlementMInsRsrv + sSettlementFloodInsRsrv + 
                    sSettlementSchoolTxRsrv + sSettlement1008Rsrv + sSettlement1009Rsrv + sSettlementU3Rsrv + sSettlementU4Rsrv;
            }
        }

        [DependsOn(nameof(sSettlementTotLenderRsrv))]
        public string sSettlementTotLenderRsrv_rep
        {
            get { return ToMoneyString(() => sSettlementTotLenderRsrv); }
        }

        // Removed from depends on list:
        // sSettlement1008ProHExp
        // sSettlement1009ProHExp
        // sSettlement1009ProHExpDesc
        // sSettlement1008ProHExpDesc
        [DependsOn(nameof(sSchedDueD1), nameof(sEscrowPmt), nameof(sInitialEscrowAcc), nameof(sTotLenderRsrv), nameof(sAggrEscrowCushionRequired), nameof(sProRealETx), 
             nameof(sProHazIns), nameof(sProMIns), nameof(sProFloodIns), nameof(sProSchoolTx), nameof(s1007ProHExp), nameof(s1006ProHExp), nameof(s1006ProHExpDesc), nameof(s1007ProHExpDesc), 
             nameof(sProU3Rsrv), nameof(sProU4Rsrv), nameof(sU3RsrvDesc), nameof(sU4RsrvDesc), nameof(sHazInsRsrvMonLckd), nameof(sMInsRsrvMonLckd), nameof(sRealETxRsrvMonLckd), 
             nameof(sSchoolTxRsrvMonLckd), nameof(sFloodInsRsrvMonLckd), nameof(s1006RsrvMonLckd), nameof(s1007RsrvMonLckd), nameof(sU3RsrvMonLckd), nameof(sU4RsrvMonLckd), nameof(sSettlementTotLenderRsrv))]
        public AggregateEscrowAccount sSettlementAggregateEscrowAccount
        {
            get
            {
                return new AggregateEscrowAccount_Settlement(this);
            }
        }

        #endregion

        private bool m_IsGfeToSettlementCCTemplateMissing = false;

        public bool IsGfeToSettlementCCTemplateMissing
        {
            get { return m_IsGfeToSettlementCCTemplateMissing; }
            set { m_IsGfeToSettlementCCTemplateMissing = value; }
        }

        #region DependsOn
        // Removed from dependson list:
        // sSettlementProSchoolTx
        // sSettlementProFloodIns
        // sSettlement1008ProHExp
        // sSettlement1009ProHExpDesc
        // sSettlementProHazInsMb
        // sSettlementProHazInsR
        // sSettlementProHazInsT
        // sSettlementProMInsMb
        // sSettlementProMInsR
        // sSettlementProMInsT
        // sSettlementProMInsLckd
        // sSettlementProRealETxR
        // sSettlementProRealETxT
        // sSettlementProRealETxMb
        // sSettlementProMIns
        // sSettlement1009ProHExp
        // sSettlement1008ProHExpDesc
        [DependsOn(nameof(sAggregateAdjRsrv), nameof(sAggregateAdjRsrvLckd), nameof(sCcTemplateId), nameof(sIPiaDyLckd), nameof(sSettlementLOrigFPc), nameof(sLOrigFPc), nameof(sSettlementLOrigFMb), nameof(sLOrigFMb), nameof(sSettlementLOrigFProps), nameof(sLOrigFProps), nameof(sSettlementBrokerCredit), nameof(sGfeBrokerCredit),  
            nameof(sSettlementLDiscntPc), nameof(sLDiscntPc), nameof(sSettlementLDiscntFMb), nameof(sLDiscntFMb), nameof(sSettlementLDiscntProps), nameof(sLDiscntProps),  nameof(sSettlementCreditLenderPaidItemT), 
            nameof(sGfeCreditLenderPaidItemT), nameof(sSettlementDiscountPointFProps), nameof(sGfeDiscountPointFProps),  
            nameof(sSettlementApprF), nameof(sApprF),  
            nameof(sSettlementApprFProps), nameof(sApprFProps), nameof(sSettlementCrF), nameof(sCrF), nameof(sSettlementCrFProps), nameof(sCrFProps), nameof(sSettlementTxServF), nameof(sTxServF),  
            nameof(sSettlementTxServFProps), nameof(sTxServFProps), nameof(sSettlementFloodCertificationF), nameof(sFloodCertificationF), nameof(sSettlementFloodCertificationFProps), nameof(sFloodCertificationFProps), nameof(sSettlementMBrokFPc), nameof(sMBrokFPc),  
            nameof(sSettlementMBrokFMb), nameof(sMBrokFMb), nameof(sSettlementMBrokFProps), nameof(sMBrokFProps), nameof(sSettlementInspectF), nameof(sInspectF), nameof(sSettlementInspectFProps), nameof(sInspectFProps), nameof(sSettlementProcF), nameof(sProcF), nameof(sSettlementProcFProps), nameof(sProcFProps),  
            nameof(sSettlementUwF), nameof(sUwF), nameof(sSettlementUwFProps), nameof(sUwFProps), nameof(sSettlementWireF), nameof(sWireF), nameof(sSettlementWireFProps), nameof(sWireFProps), nameof(sSettlement800U1F), nameof(s800U1F),  
            nameof(sSettlement800U1FProps), nameof(s800U1FProps), nameof(sSettlement800U1FDesc), nameof(s800U1FDesc), nameof(sSettlement800U2F), nameof(s800U2F),  
            nameof(sSettlement800U2FProps), nameof(s800U2FProps), nameof(sSettlement800U2FDesc), nameof(s800U2FDesc), nameof(sSettlement800U3F), nameof(s800U3F),  
            nameof(sSettlement800U3FProps), nameof(s800U3FProps), nameof(sSettlement800U3FDesc), nameof(s800U3FDesc), nameof(sSettlement800U4F), nameof(s800U4F),  
            nameof(sSettlement800U4FProps), nameof(s800U4FProps), nameof(sSettlement800U4FDesc), nameof(s800U4FDesc), nameof(sSettlement800U5F), nameof(s800U5F),  
            nameof(sSettlement800U5FProps), nameof(s800U5FProps), nameof(sSettlement800U5FDesc), nameof(s800U5FDesc), nameof(sSettlementCrFPaid), nameof(sCrFPaid), 
            nameof(sSettlementApprFPaid), nameof(sApprFPaid), nameof(sSettlementProcFPaid), nameof(sProcFPaid), nameof(sSettlementIPiaDy), nameof(sIPiaDy), nameof(sSettlementIPerDay), nameof(sIPerDay), nameof(sSettlementIPerDayLckd), nameof(sIPerDayLckd),  
            nameof(sSettlementIPiaProps), nameof(sIPiaProps), nameof(sProHazInsMb), nameof(sProHazInsR), nameof(sProHazInsT), nameof(sSettlementHazInsPiaMon), nameof(sHazInsPiaMon), nameof(sSettlementHazInsPiaProps), nameof(sHazInsPiaProps), nameof(sSettlement904PiaDesc), nameof(s904PiaDesc), nameof(sSettlement904Pia), nameof(s904Pia), nameof(sSettlement904PiaProps), nameof(s904PiaProps), nameof(sSettlement900U1PiaDesc), nameof(s900U1PiaDesc),  
            nameof(sSettlement900U1Pia), nameof(s900U1Pia), nameof(sSettlement900U1PiaProps), nameof(s900U1PiaProps), nameof(sSettlementHazInsRsrvMon), nameof(sHazInsRsrvMon), nameof(sProMInsMb), nameof(sProMInsR), nameof(sProMInsT), nameof(sProMInsLckd), nameof(sProMIns),  
            nameof(sSettlementMInsRsrvProps), nameof(sMInsRsrvProps), nameof(sSettlementMInsRsrvMon), nameof(sMInsRsrvMon), nameof(sSettlementHazInsRsrvProps), nameof(sHazInsRsrvProps), nameof(sProRealETxR), nameof(sProRealETxT), nameof(sProRealETxMb), nameof(sSettlementRealETxRsrvMon), nameof(sRealETxRsrvMon), nameof(sSettlementRealETxRsrvProps), nameof(sRealETxRsrvProps),  
            nameof(sProSchoolTx), nameof(sSettlementSchoolTxRsrvMon), nameof(sSchoolTxRsrvMon), nameof(sSettlementSchoolTxRsrvProps), nameof(sSchoolTxRsrvProps), nameof(sProFloodIns), nameof(sSettlementFloodInsRsrvMon), nameof(sFloodInsRsrvMon), nameof(sSettlementFloodInsRsrvProps), nameof(sFloodInsRsrvProps), nameof(sSettlementAggregateAdjRsrvProps), nameof(sAggregateAdjRsrvProps), nameof(s1006ProHExpDesc),  
            nameof(sSettlement1008RsrvMon), nameof(s1006RsrvMon), nameof(s1006ProHExp), nameof(sSettlement1008RsrvProps), nameof(s1006RsrvProps), nameof(s1007ProHExpDesc), nameof(sSettlement1009RsrvMon), nameof(s1007RsrvMon), nameof(s1007ProHExp), nameof(sSettlement1009RsrvProps), nameof(s1007RsrvProps), nameof(sSettlementEscrowF), nameof(sEscrowF),  
            nameof(sSettlementEscrowFProps), nameof(sEscrowFProps), nameof(sSettlementOwnerTitleInsF), nameof(sOwnerTitleInsF), nameof(sSettlementOwnerTitleInsFProps), nameof(sOwnerTitleInsProps), nameof(sSettlementTitleInsF), nameof(sTitleInsF), nameof(sSettlementTitleInsFProps), nameof(sTitleInsFProps), nameof(sSettlementDocPrepF), nameof(sDocPrepF), nameof(sSettlementDocPrepFProps), nameof(sDocPrepFProps), nameof(sSettlementNotaryF), nameof(sNotaryF),  
            nameof(sSettlementNotaryFProps), nameof(sNotaryFProps), nameof(sSettlementAttorneyF), nameof(sAttorneyF), nameof(sSettlementAttorneyFProps), nameof(sAttorneyFProps), nameof(sSettlementU1Tc), nameof(sU1Tc), nameof(sSettlementU1TcProps), nameof(sU1TcProps), nameof(sSettlementU1TcDesc), nameof(sU1TcDesc), nameof(sSettlementU2Tc), nameof(sU2Tc), nameof(sSettlementU2TcProps), nameof(sU2TcProps), nameof(sSettlementU2TcDesc), nameof(sU2TcDesc), nameof(sSettlementU3Tc), nameof(sU3Tc),  
            nameof(sSettlementU3TcProps), nameof(sU3TcProps), nameof(sSettlementU3TcDesc), nameof(sU3TcDesc), nameof(sSettlementU4Tc), nameof(sU4Tc), nameof(sSettlementU4TcProps), nameof(sU4TcProps), nameof(sSettlementU4TcDesc), nameof(sU4TcDesc), nameof(sSettlementRecFMb), nameof(sRecFMb), nameof(sSettlementRecFPc), nameof(sRecFPc), nameof(sRecDeed), nameof(sSettlementRecDeed), nameof(sRecMortgage), nameof(sSettlementRecMortgage), nameof(sRecRelease), nameof(sSettlementRecRelease), nameof(sRecFLckd), nameof(sSettlementRecFLckd),  
            nameof(sSettlementRecBaseT), nameof(sRecBaseT), nameof(sSettlementRecFProps), nameof(sRecFProps), nameof(sSettlementCountyRtcPc), nameof(sCountyRtcPc), nameof(sSettlementCountyRtcMb), nameof(sCountyRtcMb), nameof(sSettlementCountyRtcBaseT), nameof(sCountyRtcBaseT), nameof(sSettlementCountyRtcProps), nameof(sCountyRtcProps),  
            nameof(sSettlementStateRtcPc), nameof(sStateRtcPc), nameof(sSettlementStateRtcMb), nameof(sStateRtcMb), 
            nameof(sSettlementStateRtcProps), nameof(sStateRtcProps), nameof(sSettlementStateRtcBaseT), nameof(sStateRtcBaseT), nameof(sSettlementU1GovRtcPc), nameof(sU1GovRtcPc), nameof(sSettlementU1GovRtcDesc), nameof(sU1GovRtcDesc),  
            nameof(sSettlementU1GovRtcProps), nameof(sU1GovRtcProps), nameof(sSettlementU1GovRtcMb), nameof(sU1GovRtcMb), nameof(sSettlementU1GovRtcBaseT), nameof(sU1GovRtcBaseT), nameof(sSettlementU2GovRtcPc), nameof(sU2GovRtcPc), nameof(sSettlementU2GovRtcDesc), nameof(sU2GovRtcDesc), nameof(sSettlementU2GovRtcProps), nameof(sU2GovRtcProps),  
            nameof(sSettlementU2GovRtcMb), nameof(sU2GovRtcMb), nameof(sSettlementU2GovRtcBaseT), nameof(sU2GovRtcBaseT), nameof(sSettlementU3GovRtcPc), nameof(sU3GovRtcPc), nameof(sSettlementU3GovRtcDesc), nameof(sU3GovRtcDesc), nameof(sSettlementU3GovRtcProps), nameof(sU3GovRtcProps), nameof(sSettlementU3GovRtcMb), nameof(sU3GovRtcMb), nameof(sSettlementU3GovRtcBaseT), nameof(sU3GovRtcBaseT),  
            nameof(sSettlementPestInspectF), nameof(sPestInspectF), nameof(sSettlementPestInspectFProps), nameof(sPestInspectFProps), nameof(sSettlementU1Sc), nameof(sU1Sc), nameof(sSettlementU1ScProps), nameof(sU1ScProps), nameof(sSettlementU1ScDesc), nameof(sU1ScDesc), nameof(sSettlementU2Sc), nameof(sU2Sc), nameof(sSettlementU2ScProps), nameof(sU2ScProps), nameof(sSettlementU2ScDesc), nameof(sU2ScDesc),  
            nameof(sSettlementU3Sc), nameof(sU3Sc), nameof(sSettlementU3ScProps), nameof(sU3ScProps), nameof(sSettlementU3ScDesc), nameof(sU3ScDesc), nameof(sSettlementU4Sc), nameof(sU4Sc), nameof(sSettlementU4ScProps), nameof(sU4ScProps), nameof(sSettlementU4ScDesc), nameof(sU4ScDesc), nameof(sSettlementU5Sc), nameof(sU5Sc), nameof(sSettlementU5ScProps), nameof(sU5ScProps),  
            nameof(sSettlementU5ScDesc), nameof(sU5ScDesc), nameof(s800U1FSettlementSection), nameof(s800U1FGfeSection), nameof(s800U2FSettlementSection), nameof(s800U2FGfeSection), nameof(s800U3FSettlementSection), nameof(s800U3FGfeSection), nameof(s800U4FSettlementSection), nameof(s800U4FGfeSection), nameof(s800U5FSettlementSection), nameof(s800U5FGfeSection), nameof(s904PiaSettlementSection), nameof(s904PiaGfeSection), nameof(s900U1PiaSettlementSection), nameof(s900U1PiaGfeSection),  
            nameof(sEscrowFSettlementSection), nameof(sEscrowFGfeSection), nameof(sDocPrepFSettlementSection), nameof(sDocPrepFGfeSection), nameof(sNotaryFSettlementSection), nameof(sNotaryFGfeSection), nameof(sAttorneyFSettlementSection), nameof(sAttorneyFGfeSection), nameof(sTitleInsFSettlementSection), nameof(sTitleInsFGfeSection), nameof(sU1TcSettlementSection), nameof(sU1TcGfeSection), nameof(sU2TcSettlementSection), nameof(sU2TcGfeSection), nameof(sU3TcSettlementSection), nameof(sU3TcGfeSection),  
            nameof(sU4TcSettlementSection), nameof(sU4TcGfeSection), nameof(sU1GovRtcSettlementSection), nameof(sU1GovRtcGfeSection), nameof(sU2GovRtcSettlementSection), nameof(sU2GovRtcGfeSection), nameof(sU3GovRtcSettlementSection), nameof(sU3GovRtcGfeSection), nameof(sU1ScSettlementSection), nameof(sU1ScGfeSection), nameof(sU2ScSettlementSection), nameof(sU2ScGfeSection), nameof(sU3ScSettlementSection), nameof(sU3ScGfeSection), nameof(sU4ScSettlementSection), nameof(sU4ScGfeSection),  
            nameof(sU5ScSettlementSection), nameof(sU5ScGfeSection), nameof(sSettlementOwnerTitleInsFProps), nameof(sOwnerTitleInsProps), nameof(sSchedFundD), nameof(sConsummationD), nameof(sSettlementChargesExportSource),  
            nameof(sSettlementU3RsrvMon), nameof(sU3RsrvMon), nameof(sSettlementU3RsrvProps), nameof(sU3RsrvProps),  
            nameof(sSettlementU4RsrvMon), nameof(sU4RsrvMon), nameof(sSettlementU4RsrvProps), nameof(sU4RsrvProps), nameof(sSettlementIPiaDyLckd), nameof(sSettlementAggregateAdjRsrvLckd), nameof(sSettlementAggregateAdjRsrv)
            // nameof(cLOrigFProps), nameof(cLDiscntProps), nameof(InspectFProps), nameof(cMBrokFProps), nameof(cFloodCertificationFProps), nameof(cTxServFProps), nameof(cCrFProps), nameof(cApprFProps), nameof(cProcFProps), nameof(cUwFProps), nameof(cWireFProps), nameof(c800U1FProps), nameof(c800U2FProps), nameof(c800U3FProps), nameof(c800U4FProps), nameof(c800U5FProps), nameof(cHazInsPiaProps), nameof(c904PiaProps), nameof(c900U1PiaProps), nameof(cMInsRsrvProps), nameof(cHazInsRsrvProps), nameof(cRealETxRsrvProps), nameof(cFloodInsRsrvProps), nameof(cAggregateAdjRsrvProps), nameof(c1007RsrvProps), 
            // nameof(cEscrowFProp), nameof(cOwnerTitleInsProps), nameof(cTitleInsFProps), nameof(cDocPrepFProps), nameof(cNotaryFProps), nameof(cAttorneyFProps), nameof(cU1TcProps), nameof(cU2TcProps), nameof(cU3TcProps), nameof(cU4TcProps), nameof(cRecFProps), nameof(cCountyRtcProps), nameof(cStateRtcProps), nameof(cU1GovRtcProps), nameof(cU2GovRtcProps), nameof(cU3GovRtcProps), nameof(cPestInspectFProps), nameof(cU1ScProps), nameof(cU2ScProps), nameof(cU3ScProps), nameof(cU4ScProps), nameof(cU5ScProps)
        )]
        #endregion
        private bool sfApplyGfeDataToSettlementPage
        {
            set
            {
            }
        }
        private static int GenerateSettlementPropsFromGfeProps(int origSettlementValue, int gfeValue, int dflpProps )
        {
            return LosConvert.GfeItemProps_Pack(
                  LosConvert.GfeItemProps_Apr(gfeValue)
                , LosConvert.GfeItemProps_ToBr(gfeValue)
                , LosConvert.GfeItemProps_Payer(gfeValue)
                , LosConvert.GfeItemProps_FhaAllow(gfeValue)
                , LosConvert.GfeItemProps_Poc(gfeValue)
                , LosConvert.GfeItemProps_Dflp(dflpProps)
                , LosConvert.GfeItemProps_GBF(origSettlementValue)  // no-op
                , LosConvert.GfeItemProps_BF(origSettlementValue)   // no-op
                , LosConvert.GfeItemProps_Borr(gfeValue)
                , LosConvert.GfeItemProps_PaidToThirdParty(gfeValue)
                , LosConvert.GfeItemProps_ThisPartyIsAffiliate(gfeValue)
                , LosConvert.GfeItemProps_ShowQmWarning(gfeValue)
            );
        }

        public void ApplyGfeDataToSettlementPage()
        {
            CCcTemplateData ccTemplate = null;
            if (sCcTemplateId != Guid.Empty)
            {
                ccTemplate = new CCcTemplateData(this.sBrokerId, sCcTemplateId);
                try
                {
                    ccTemplate.InitLoad();
                }
                catch (System.IndexOutOfRangeException)
                {
                    // OPM 57095. CC Template does not exist,
                    // but we still import, just not setting DFLP values
                    IsGfeToSettlementCCTemplateMissing = true;
                    ccTemplate = null;
                }
            }

            int dflpProps = 0;
            if (sSchedFundD.IsValid == false) // OPM 48134
            {
                sSchedFundD = sConsummationD;
            }
            // 3/15/2012 dd - OPM 59454 - Switch sSettlementChargesExportSource when copy values from GFE to settlement charges page.
            sSettlementChargesExportSource = E_SettlementChargesExportSource.SETTLEMENT;
            // 800 
            sSettlementLOrigFPc = sLOrigFPc;
            sSettlementLOrigFMb = sLOrigFMb;
            dflpProps = (ccTemplate == null) ? sSettlementLOrigFProps : ccTemplate.cLOrigFProps;
            sSettlementLOrigFProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sLOrigFProps), LosConvert.GfeItemProps_ToBr(sLOrigFProps), LosConvert.GfeItemProps_Payer(sLOrigFProps), LosConvert.GfeItemProps_FhaAllow(sLOrigFProps), LosConvert.GfeItemProps_Poc(sLOrigFProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(sLOrigFProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(sLOrigFProps), LosConvert.GfeItemProps_ShowQmWarning(sLOrigFProps));
            
            sSettlementBrokerCredit = sGfeBrokerCredit;
            
            sSettlementLDiscntPc = sLDiscntPc;
            sSettlementLDiscntFMb = sLDiscntFMb;
            dflpProps = (ccTemplate == null) ? sSettlementLDiscntProps : ccTemplate.cLDiscntProps;
            sSettlementLDiscntProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sLDiscntProps), LosConvert.GfeItemProps_ToBr(sLDiscntProps), LosConvert.GfeItemProps_Payer(sLDiscntProps), LosConvert.GfeItemProps_FhaAllow(sLDiscntProps), LosConvert.GfeItemProps_Poc(sLDiscntProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(sLDiscntProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(sLDiscntProps), LosConvert.GfeItemProps_ShowQmWarning(sLDiscntProps)); // deprecated

            sSettlementCreditLenderPaidItemT = sGfeCreditLenderPaidItemT;
            //sSettlementCreditLenderPaidItemF = sGfeCreditLenderPaidItemF;
            //sSettlementLenderCreditFPc = sGfeLenderCreditFPc;
            //sSettlementLenderCreditF = sGfeLenderCreditF;
            //sSettlementDiscountPointFPc = sGfeDiscountPointFPc;
            //sSettlementDiscountPointF = sGfeDiscountPointF;
            //dflpProps = (ccTemplate == null) ? sSettlementDiscountPointFProps : ccTemplate.cDiscountPointFProps; // ???
            dflpProps = sSettlementDiscountPointFProps;
            var payerProps = LosConvert.GfeItemProps_PdByLenderOnly(sGfeDiscountPointFProps) ? 0 : LosConvert.GfeItemProps_Payer(sGfeDiscountPointFProps); // Discount points does not make sense when paid by lender
            sSettlementDiscountPointFProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sGfeDiscountPointFProps), LosConvert.GfeItemProps_ToBr(sGfeDiscountPointFProps), payerProps, LosConvert.GfeItemProps_FhaAllow(sGfeDiscountPointFProps), LosConvert.GfeItemProps_Poc(sGfeDiscountPointFProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(sGfeDiscountPointFProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(sGfeDiscountPointFProps), LosConvert.GfeItemProps_ShowQmWarning(sGfeDiscountPointFProps));

            sSettlementApprF = sApprF;
            dflpProps = (ccTemplate == null) ? sSettlementApprFProps : ccTemplate.cApprFProps;
            sSettlementApprFProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sApprFProps), LosConvert.GfeItemProps_ToBr(sApprFProps), LosConvert.GfeItemProps_Payer(sApprFProps), LosConvert.GfeItemProps_FhaAllow(sApprFProps), LosConvert.GfeItemProps_Poc(sApprFProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(sApprFProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(sApprFProps), LosConvert.GfeItemProps_ShowQmWarning(sApprFProps));
            sSettlementCrF = sCrF;
            dflpProps = (ccTemplate == null) ? sSettlementCrFProps : ccTemplate.cCrFProps;
            sSettlementCrFProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sCrFProps), LosConvert.GfeItemProps_ToBr(sCrFProps), LosConvert.GfeItemProps_Payer(sCrFProps), LosConvert.GfeItemProps_FhaAllow(sCrFProps), LosConvert.GfeItemProps_Poc(sCrFProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(sCrFProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(sCrFProps), LosConvert.GfeItemProps_ShowQmWarning(sCrFProps));
            sSettlementTxServF = sTxServF;
            dflpProps = (ccTemplate == null) ? sSettlementTxServFProps : ccTemplate.cTxServFProps;
            sSettlementTxServFProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sTxServFProps), LosConvert.GfeItemProps_ToBr(sTxServFProps), LosConvert.GfeItemProps_Payer(sTxServFProps), LosConvert.GfeItemProps_FhaAllow(sTxServFProps), LosConvert.GfeItemProps_Poc(sTxServFProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(sTxServFProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(sTxServFProps), LosConvert.GfeItemProps_ShowQmWarning(sTxServFProps));
            sSettlementFloodCertificationF = sFloodCertificationF;
            dflpProps = (ccTemplate == null) ? sSettlementFloodCertificationFProps : ccTemplate.cFloodCertificationFProps;
            sSettlementFloodCertificationFProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sFloodCertificationFProps), LosConvert.GfeItemProps_ToBr(sFloodCertificationFProps), LosConvert.GfeItemProps_Payer(sFloodCertificationFProps), LosConvert.GfeItemProps_FhaAllow(sFloodCertificationFProps), LosConvert.GfeItemProps_Poc(sFloodCertificationFProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(sFloodCertificationFProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(sFloodCertificationFProps), LosConvert.GfeItemProps_ShowQmWarning(sFloodCertificationFProps));
            sSettlementMBrokFPc = sMBrokFPc;
            sSettlementMBrokFMb = sMBrokFMb;
            dflpProps = (ccTemplate == null) ? sSettlementMBrokFProps : ccTemplate.cMBrokFProps;
            sSettlementMBrokFProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sMBrokFProps), LosConvert.GfeItemProps_ToBr(sMBrokFProps), LosConvert.GfeItemProps_Payer(sMBrokFProps), LosConvert.GfeItemProps_FhaAllow(sMBrokFProps), LosConvert.GfeItemProps_Poc(sMBrokFProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(sMBrokFProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(sMBrokFProps), LosConvert.GfeItemProps_ShowQmWarning(sMBrokFProps));
            sSettlementInspectF = sInspectF;
            dflpProps = (ccTemplate == null) ? sSettlementInspectFProps : ccTemplate.cInspectFProps;
            sSettlementInspectFProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sInspectFProps), LosConvert.GfeItemProps_ToBr(sInspectFProps), LosConvert.GfeItemProps_Payer(sInspectFProps), LosConvert.GfeItemProps_FhaAllow(sInspectFProps), LosConvert.GfeItemProps_Poc(sInspectFProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(sInspectFProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(sInspectFProps), LosConvert.GfeItemProps_ShowQmWarning(sInspectFProps));
            sSettlementProcF = sProcF;
            dflpProps = (ccTemplate == null) ? sSettlementProcFProps : ccTemplate.cProcFProps;
            sSettlementProcFProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sProcFProps), LosConvert.GfeItemProps_ToBr(sProcFProps), LosConvert.GfeItemProps_Payer(sProcFProps), LosConvert.GfeItemProps_FhaAllow(sProcFProps), LosConvert.GfeItemProps_Poc(sProcFProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(sProcFProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(sProcFProps), LosConvert.GfeItemProps_ShowQmWarning(sProcFProps));
            sSettlementUwF = sUwF;
            dflpProps = (ccTemplate == null) ? sSettlementUwFProps : ccTemplate.cUwFProps;
            sSettlementUwFProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sUwFProps), LosConvert.GfeItemProps_ToBr(sUwFProps), LosConvert.GfeItemProps_Payer(sUwFProps), LosConvert.GfeItemProps_FhaAllow(sUwFProps), LosConvert.GfeItemProps_Poc(sUwFProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(sUwFProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(sUwFProps), LosConvert.GfeItemProps_ShowQmWarning(sUwFProps));
            sSettlementWireF = sWireF;
            dflpProps = (ccTemplate == null) ? sSettlementWireFProps : ccTemplate.cWireFProps;
            sSettlementWireFProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sWireFProps), LosConvert.GfeItemProps_ToBr(sWireFProps), LosConvert.GfeItemProps_Payer(sWireFProps), LosConvert.GfeItemProps_FhaAllow(sWireFProps), LosConvert.GfeItemProps_Poc(sWireFProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(sWireFProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(sWireFProps), LosConvert.GfeItemProps_ShowQmWarning(sWireFProps));
            sSettlement800U1F = s800U1F;
            dflpProps = (ccTemplate == null) ? sSettlement800U1FProps : ccTemplate.c800U1FProps;
            sSettlement800U1FProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(s800U1FProps), LosConvert.GfeItemProps_ToBr(s800U1FProps), LosConvert.GfeItemProps_Payer(s800U1FProps), LosConvert.GfeItemProps_FhaAllow(s800U1FProps), LosConvert.GfeItemProps_Poc(s800U1FProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(s800U1FProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(s800U1FProps), LosConvert.GfeItemProps_ShowQmWarning(s800U1FProps));
            sSettlement800U1FDesc = s800U1FDesc;
            sSettlement800U2F = s800U2F;
            dflpProps = (ccTemplate == null) ? sSettlement800U2FProps : ccTemplate.c800U2FProps;
            sSettlement800U2FProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(s800U2FProps), LosConvert.GfeItemProps_ToBr(s800U2FProps), LosConvert.GfeItemProps_Payer(s800U2FProps), LosConvert.GfeItemProps_FhaAllow(s800U2FProps), LosConvert.GfeItemProps_Poc(s800U2FProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(s800U2FProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(s800U2FProps), LosConvert.GfeItemProps_ShowQmWarning(s800U2FProps));
            sSettlement800U2FDesc = s800U2FDesc;
            sSettlement800U3F = s800U3F;
            dflpProps = (ccTemplate == null) ? sSettlement800U3FProps : ccTemplate.c800U3FProps;
            sSettlement800U3FProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(s800U3FProps), LosConvert.GfeItemProps_ToBr(s800U3FProps), LosConvert.GfeItemProps_Payer(s800U3FProps), LosConvert.GfeItemProps_FhaAllow(s800U3FProps), LosConvert.GfeItemProps_Poc(s800U3FProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(s800U3FProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(s800U3FProps), LosConvert.GfeItemProps_ShowQmWarning(s800U3FProps));
            sSettlement800U3FDesc = s800U3FDesc;
            sSettlement800U4F = s800U4F;
            dflpProps = (ccTemplate == null) ? sSettlement800U4FProps : ccTemplate.c800U4FProps;
            sSettlement800U4FProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(s800U4FProps), LosConvert.GfeItemProps_ToBr(s800U4FProps), LosConvert.GfeItemProps_Payer(s800U4FProps), LosConvert.GfeItemProps_FhaAllow(s800U4FProps), LosConvert.GfeItemProps_Poc(s800U4FProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(s800U4FProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(s800U4FProps), LosConvert.GfeItemProps_ShowQmWarning(s800U4FProps));
            sSettlement800U4FDesc = s800U4FDesc;
            sSettlement800U5F = s800U5F;
            dflpProps = (ccTemplate == null) ? sSettlement800U5FProps : ccTemplate.c800U5FProps;
            sSettlement800U5FProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(s800U5FProps), LosConvert.GfeItemProps_ToBr(s800U5FProps), LosConvert.GfeItemProps_Payer(s800U5FProps), LosConvert.GfeItemProps_FhaAllow(s800U5FProps), LosConvert.GfeItemProps_Poc(s800U5FProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(s800U5FProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(s800U5FProps), LosConvert.GfeItemProps_ShowQmWarning(s800U5FProps));
            sSettlement800U5FDesc = s800U5FDesc;
            sSettlementCrFPaid = sCrFPaid;
            sSettlementApprFPaid = sApprFPaid;
            sSettlementProcFPaid = sProcFPaid;
            
            // 900
            sSettlementIPiaDy = sIPiaDy;
            sSettlementIPerDay = sIPerDay;
            sSettlementIPerDayLckd = sIPerDayLckd;
            //sSettlementIPiaProps = sIPiaProps;
            dflpProps = (ccTemplate == null) ? sSettlementIPiaProps : ccTemplate.cIPiaProps;
            sSettlementIPiaProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sIPiaProps), LosConvert.GfeItemProps_ToBr(sIPiaProps), LosConvert.GfeItemProps_Payer(sIPiaProps), LosConvert.GfeItemProps_FhaAllow(sIPiaProps), LosConvert.GfeItemProps_Poc(sIPiaProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(sIPiaProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(sIPiaProps), LosConvert.GfeItemProps_ShowQmWarning(sIPiaProps));

            sSettlementIPiaDyLckd = sIPiaDyLckd;
            //sSettlementProHazInsMb = sProHazInsMb;
            //sSettlementProHazInsR = sProHazInsR;
            //sSettlementProHazInsT = sProHazInsT;
            sSettlementHazInsPiaMon = sHazInsPiaMon;
            dflpProps = (ccTemplate == null) ? sSettlementHazInsPiaProps : ccTemplate.cHazInsPiaProps;
            sSettlementHazInsPiaProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sHazInsPiaProps), LosConvert.GfeItemProps_ToBr(sHazInsPiaProps), LosConvert.GfeItemProps_Payer(sHazInsPiaProps), LosConvert.GfeItemProps_FhaAllow(sHazInsPiaProps), LosConvert.GfeItemProps_Poc(sHazInsPiaProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(sHazInsPiaProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(sHazInsPiaProps), LosConvert.GfeItemProps_ShowQmWarning(sHazInsPiaProps));
            sSettlement904PiaDesc = s904PiaDesc;
            sSettlement904Pia = s904Pia;
            dflpProps = (ccTemplate == null) ? sSettlement904PiaProps : ccTemplate.c904PiaProps;
            sSettlement904PiaProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(s904PiaProps), LosConvert.GfeItemProps_ToBr(s904PiaProps), LosConvert.GfeItemProps_Payer(s904PiaProps), LosConvert.GfeItemProps_FhaAllow(s904PiaProps), LosConvert.GfeItemProps_Poc(s904PiaProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(s904PiaProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(s904PiaProps), LosConvert.GfeItemProps_ShowQmWarning(s904PiaProps));
            sSettlement900U1PiaDesc = s900U1PiaDesc;
            sSettlement900U1Pia = s900U1Pia;
            dflpProps = (ccTemplate == null) ? sSettlement900U1PiaProps : ccTemplate.c900U1PiaProps;
            sSettlement900U1PiaProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(s900U1PiaProps), LosConvert.GfeItemProps_ToBr(s900U1PiaProps), LosConvert.GfeItemProps_Payer(s900U1PiaProps), LosConvert.GfeItemProps_FhaAllow(s900U1PiaProps), LosConvert.GfeItemProps_Poc(s900U1PiaProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(s900U1PiaProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(s900U1PiaProps), LosConvert.GfeItemProps_ShowQmWarning(s900U1PiaProps));
            
            // 1000
            sSettlementHazInsRsrvMon = sHazInsRsrvMon;
            //sSettlementProMInsMb = sProMInsMb;
            //sSettlementProMInsR = sProMInsR;
            //sSettlementProMInsT = sProMInsT;
            //sSettlementProMInsLckd = sProMInsLckd;
            //sSettlementProMIns = sProMIns;
            dflpProps = (ccTemplate == null) ? sSettlementMInsRsrvProps : ccTemplate.cMInsRsrvProps;

            sSettlementMInsRsrvProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sMInsRsrvProps), LosConvert.GfeItemProps_ToBr(sMInsRsrvProps), LosConvert.GfeItemProps_Payer(sMInsRsrvProps), LosConvert.GfeItemProps_FhaAllow(sMInsRsrvProps), LosConvert.GfeItemProps_Poc(sMInsRsrvProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(sMInsRsrvProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(sMInsRsrvProps), LosConvert.GfeItemProps_ShowQmWarning(sMInsRsrvProps));
            sSettlementMInsRsrvMon = sMInsRsrvMon;
            dflpProps = (ccTemplate == null) ? sSettlementHazInsRsrvProps : ccTemplate.cHazInsRsrvProps;
            sSettlementHazInsRsrvProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sHazInsRsrvProps), LosConvert.GfeItemProps_ToBr(sHazInsRsrvProps), LosConvert.GfeItemProps_Payer(sHazInsRsrvProps), LosConvert.GfeItemProps_FhaAllow(sHazInsRsrvProps), LosConvert.GfeItemProps_Poc(sHazInsRsrvProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(sHazInsRsrvProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(sHazInsRsrvProps), LosConvert.GfeItemProps_ShowQmWarning(sHazInsRsrvProps));
            //sSettlementProRealETxR = sProRealETxR;
            //sSettlementProRealETxT = sProRealETxT;
            //sSettlementProRealETxMb = sProRealETxMb;
            sSettlementRealETxRsrvMon = sRealETxRsrvMon;
            dflpProps = (ccTemplate == null) ? sSettlementRealETxRsrvProps : ccTemplate.cRealETxRsrvProps;
            sSettlementRealETxRsrvProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sRealETxRsrvProps), LosConvert.GfeItemProps_ToBr(sRealETxRsrvProps), LosConvert.GfeItemProps_Payer(sRealETxRsrvProps), LosConvert.GfeItemProps_FhaAllow(sRealETxRsrvProps), LosConvert.GfeItemProps_Poc(sRealETxRsrvProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(sRealETxRsrvProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(sRealETxRsrvProps), LosConvert.GfeItemProps_ShowQmWarning(sRealETxRsrvProps));
            //sSettlementProSchoolTx = sProSchoolTx;
            sSettlementSchoolTxRsrvMon = sSchoolTxRsrvMon;
            dflpProps = (ccTemplate == null) ? sSettlementSchoolTxRsrvProps : ccTemplate.cSchoolTxRsrvProps;
            sSettlementSchoolTxRsrvProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sSchoolTxRsrvProps), LosConvert.GfeItemProps_ToBr(sSchoolTxRsrvProps), LosConvert.GfeItemProps_Payer(sSchoolTxRsrvProps), LosConvert.GfeItemProps_FhaAllow(sSchoolTxRsrvProps), LosConvert.GfeItemProps_Poc(sSchoolTxRsrvProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(sSchoolTxRsrvProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(sSchoolTxRsrvProps), LosConvert.GfeItemProps_ShowQmWarning(sSchoolTxRsrvProps));
            //sSettlementProFloodIns = sProFloodIns;
            sSettlementFloodInsRsrvMon = sFloodInsRsrvMon;
            dflpProps = (ccTemplate == null) ? sSettlementFloodInsRsrvProps : ccTemplate.cFloodInsRsrvProps;
            sSettlementFloodInsRsrvProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sFloodInsRsrvProps), LosConvert.GfeItemProps_ToBr(sFloodInsRsrvProps), LosConvert.GfeItemProps_Payer(sFloodInsRsrvProps), LosConvert.GfeItemProps_FhaAllow(sFloodInsRsrvProps), LosConvert.GfeItemProps_Poc(sFloodInsRsrvProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(sFloodInsRsrvProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(sFloodInsRsrvProps), LosConvert.GfeItemProps_ShowQmWarning(sFloodInsRsrvProps));
            dflpProps = (ccTemplate == null) ? sSettlementAggregateAdjRsrvProps : ccTemplate.cAggregateAdjRsrvProps;
            sSettlementAggregateAdjRsrvProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sAggregateAdjRsrvProps), LosConvert.GfeItemProps_ToBr(sAggregateAdjRsrvProps), LosConvert.GfeItemProps_Payer(sAggregateAdjRsrvProps), LosConvert.GfeItemProps_FhaAllow(sAggregateAdjRsrvProps), LosConvert.GfeItemProps_Poc(sAggregateAdjRsrvProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(sAggregateAdjRsrvProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(sAggregateAdjRsrvProps), LosConvert.GfeItemProps_ShowQmWarning(sAggregateAdjRsrvProps));
            sSettlementAggregateAdjRsrvLckd = sAggregateAdjRsrvLckd;
            if (sAggregateAdjRsrvLckd)
            {
                sSettlementAggregateAdjRsrv = sAggregateAdjRsrv;
            }

            //sSettlement1008ProHExpDesc = s1006ProHExpDesc;
            sSettlement1008RsrvMon = s1006RsrvMon;
            //sSettlement1008ProHExp_rep = s1006ProHExp_rep;

            //sSettlement1008RsrvProps = s1006RsrvProps;
            dflpProps = (ccTemplate == null) ? sSettlement1008RsrvProps : ccTemplate.c1006RsrvProps;
            sSettlement1008RsrvProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(s1006RsrvProps), LosConvert.GfeItemProps_ToBr(s1006RsrvProps), LosConvert.GfeItemProps_Payer(s1006RsrvProps), LosConvert.GfeItemProps_FhaAllow(s1006RsrvProps), LosConvert.GfeItemProps_Poc(s1006RsrvProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(s1006RsrvProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(s1006RsrvProps), LosConvert.GfeItemProps_ShowQmWarning(s1006RsrvProps));
          

            //sSettlement1009ProHExpDesc = s1007ProHExpDesc;
            sSettlement1009RsrvMon = s1007RsrvMon;
            //sSettlement1009ProHExp = s1007ProHExp;
            dflpProps = (ccTemplate == null) ? sSettlement1009RsrvProps : ccTemplate.c1007RsrvProps;
            sSettlement1009RsrvProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(s1007RsrvProps), LosConvert.GfeItemProps_ToBr(s1007RsrvProps), LosConvert.GfeItemProps_Payer(s1007RsrvProps), LosConvert.GfeItemProps_FhaAllow(s1007RsrvProps), LosConvert.GfeItemProps_Poc(s1007RsrvProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(s1007RsrvProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(s1007RsrvProps), LosConvert.GfeItemProps_ShowQmWarning(s1007RsrvProps));

            sSettlementU3RsrvMon = sU3RsrvMon;
            dflpProps = (ccTemplate == null) ? sSettlementU3RsrvProps : ccTemplate.cU3RsrvProps;
            sSettlementU3RsrvProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sU3RsrvProps), LosConvert.GfeItemProps_ToBr(sU3RsrvProps), LosConvert.GfeItemProps_Payer(sU3RsrvProps), LosConvert.GfeItemProps_FhaAllow(sU3RsrvProps), LosConvert.GfeItemProps_Poc(sU3RsrvProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(sU3RsrvProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(sU3RsrvProps), LosConvert.GfeItemProps_ShowQmWarning(sU3RsrvProps));

            sSettlementU4RsrvMon = sU4RsrvMon;
            dflpProps = (ccTemplate == null) ? sSettlementU4RsrvProps : ccTemplate.cU4RsrvProps;
            sSettlementU4RsrvProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sU4RsrvProps), LosConvert.GfeItemProps_ToBr(sU4RsrvProps), LosConvert.GfeItemProps_Payer(sU4RsrvProps), LosConvert.GfeItemProps_FhaAllow(sU4RsrvProps), LosConvert.GfeItemProps_Poc(sU4RsrvProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(sU4RsrvProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(sU4RsrvProps), LosConvert.GfeItemProps_ShowQmWarning(sU4RsrvProps));
            
            // 1100
            sSettlementEscrowF = sEscrowF;
            dflpProps = (ccTemplate == null) ? sSettlementEscrowFProps : ccTemplate.cEscrowFProps;
            sSettlementEscrowFProps = GenerateSettlementPropsFromGfeProps(sSettlementEscrowFProps, sEscrowFProps ,dflpProps);
            sSettlementOwnerTitleInsF = sOwnerTitleInsF;
            dflpProps = (ccTemplate == null) ? sSettlementOwnerTitleInsFProps : ccTemplate.cOwnerTitleInsProps;
            sSettlementOwnerTitleInsFProps = GenerateSettlementPropsFromGfeProps(sSettlementOwnerTitleInsFProps, sOwnerTitleInsProps, dflpProps);//LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sOwnerTitleInsProps), LosConvert.GfeItemProps_ToBr(sOwnerTitleInsProps), LosConvert.GfeItemProps_Payer(sOwnerTitleInsProps), LosConvert.GfeItemProps_FhaAllow(sOwnerTitleInsProps), LosConvert.GfeItemProps_Poc(sOwnerTitleInsProps), LosConvert.GfeItemProps_Dflp(dflpProps));
            sSettlementTitleInsF = sTitleInsF;
            dflpProps = (ccTemplate == null) ? sSettlementTitleInsFProps : ccTemplate.cTitleInsFProps;
            sSettlementTitleInsFProps =  GenerateSettlementPropsFromGfeProps(sSettlementTitleInsFProps, sTitleInsFProps, dflpProps); //LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sTitleInsFProps), LosConvert.GfeItemProps_ToBr(sTitleInsFProps), LosConvert.GfeItemProps_Payer(sTitleInsFProps), LosConvert.GfeItemProps_FhaAllow(sTitleInsFProps), LosConvert.GfeItemProps_Poc(sTitleInsFProps), LosConvert.GfeItemProps_Dflp(dflpProps));
            sSettlementDocPrepF = sDocPrepF;
            dflpProps = (ccTemplate == null) ? sSettlementDocPrepFProps : ccTemplate.cDocPrepFProps;
            sSettlementDocPrepFProps =  GenerateSettlementPropsFromGfeProps(sSettlementDocPrepFProps, sDocPrepFProps, dflpProps); //LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sDocPrepFProps), LosConvert.GfeItemProps_ToBr(sDocPrepFProps), LosConvert.GfeItemProps_Payer(sDocPrepFProps), LosConvert.GfeItemProps_FhaAllow(sDocPrepFProps), LosConvert.GfeItemProps_Poc(sDocPrepFProps), LosConvert.GfeItemProps_Dflp(dflpProps));
            sSettlementNotaryF = sNotaryF;
            dflpProps = (ccTemplate == null) ? sSettlementNotaryFProps : ccTemplate.cNotaryFProps;
            sSettlementNotaryFProps =  GenerateSettlementPropsFromGfeProps(sSettlementNotaryFProps, sNotaryFProps, dflpProps); //LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sNotaryFProps), LosConvert.GfeItemProps_ToBr(sNotaryFProps), LosConvert.GfeItemProps_Payer(sNotaryFProps), LosConvert.GfeItemProps_FhaAllow(sNotaryFProps), LosConvert.GfeItemProps_Poc(sNotaryFProps), LosConvert.GfeItemProps_Dflp(dflpProps));
            sSettlementAttorneyF = sAttorneyF;
            dflpProps = (ccTemplate == null) ? sSettlementAttorneyFProps : ccTemplate.cAttorneyFProps;
            sSettlementAttorneyFProps = GenerateSettlementPropsFromGfeProps(sSettlementAttorneyFProps, sAttorneyFProps, dflpProps);   //LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sAttorneyFProps), LosConvert.GfeItemProps_ToBr(sAttorneyFProps), LosConvert.GfeItemProps_Payer(sAttorneyFProps), LosConvert.GfeItemProps_FhaAllow(sAttorneyFProps), LosConvert.GfeItemProps_Poc(sAttorneyFProps), LosConvert.GfeItemProps_Dflp(dflpProps));
            sSettlementU1Tc = sU1Tc;
            dflpProps = (ccTemplate == null) ? sSettlementU1TcProps : ccTemplate.cU1TcProps;
            sSettlementU1TcProps = GenerateSettlementPropsFromGfeProps(sSettlementU1TcProps, sU1TcProps, dflpProps); //LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sU1TcProps), LosConvert.GfeItemProps_ToBr(sU1TcProps), LosConvert.GfeItemProps_Payer(sU1TcProps), LosConvert.GfeItemProps_FhaAllow(sU1TcProps), LosConvert.GfeItemProps_Poc(sU1TcProps), LosConvert.GfeItemProps_Dflp(dflpProps));
            sSettlementU1TcDesc = sU1TcDesc;
            sSettlementU2Tc = sU2Tc;
            dflpProps = (ccTemplate == null) ? sSettlementU2TcProps : ccTemplate.cU2TcProps;
            sSettlementU2TcProps =  GenerateSettlementPropsFromGfeProps(sSettlementU2TcProps, sU2TcProps, dflpProps); //LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sU2TcProps), LosConvert.GfeItemProps_ToBr(sU2TcProps), LosConvert.GfeItemProps_Payer(sU2TcProps), LosConvert.GfeItemProps_FhaAllow(sU2TcProps), LosConvert.GfeItemProps_Poc(sU2TcProps), LosConvert.GfeItemProps_Dflp(dflpProps));
            sSettlementU2TcDesc = sU2TcDesc;
            sSettlementU3Tc = sU3Tc;
            dflpProps = (ccTemplate == null) ? sSettlementU3TcProps : ccTemplate.cU3TcProps;
            sSettlementU3TcProps =  GenerateSettlementPropsFromGfeProps(sSettlementU3TcProps, sU3TcProps, dflpProps); //LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sU3TcProps), LosConvert.GfeItemProps_ToBr(sU3TcProps), LosConvert.GfeItemProps_Payer(sU3TcProps), LosConvert.GfeItemProps_FhaAllow(sU3TcProps), LosConvert.GfeItemProps_Poc(sU3TcProps), LosConvert.GfeItemProps_Dflp(dflpProps));
            sSettlementU3TcDesc = sU3TcDesc;
            sSettlementU4Tc = sU4Tc;
            dflpProps = (ccTemplate == null) ? sSettlementU4TcProps : ccTemplate.cU4TcProps;
            sSettlementU4TcProps =  GenerateSettlementPropsFromGfeProps(sSettlementU4TcProps, sU4TcProps, dflpProps); //LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sU4TcProps), LosConvert.GfeItemProps_ToBr(sU4TcProps), LosConvert.GfeItemProps_Payer(sU4TcProps), LosConvert.GfeItemProps_FhaAllow(sU4TcProps), LosConvert.GfeItemProps_Poc(sU4TcProps), LosConvert.GfeItemProps_Dflp(dflpProps));
            sSettlementU4TcDesc = sU4TcDesc;
            
            // 1200
            sSettlementRecFMb = sRecFMb;
            sSettlementRecFPc = sRecFPc;
            sSettlementRecBaseT = sRecBaseT;
            sSettlementRecFLckd = sRecFLckd;
            sSettlementRecDeed = sRecDeed;
            sSettlementRecMortgage = sRecMortgage;
            sSettlementRecRelease = sRecRelease;
            dflpProps = (ccTemplate == null) ? sSettlementRecFProps : ccTemplate.cRecFProps;
            sSettlementRecFProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sRecFProps), LosConvert.GfeItemProps_ToBr(sRecFProps), LosConvert.GfeItemProps_Payer(sRecFProps), LosConvert.GfeItemProps_FhaAllow(sRecFProps), LosConvert.GfeItemProps_Poc(sRecFProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(sRecFProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(sRecFProps), LosConvert.GfeItemProps_ShowQmWarning(sRecFProps));
            sSettlementCountyRtcPc = sCountyRtcPc;
            sSettlementCountyRtcMb = sCountyRtcMb;
            sSettlementCountyRtcBaseT = sCountyRtcBaseT;
            dflpProps = (ccTemplate == null) ? sSettlementCountyRtcProps : ccTemplate.cCountyRtcProps;
            sSettlementCountyRtcProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sCountyRtcProps), LosConvert.GfeItemProps_ToBr(sCountyRtcProps), LosConvert.GfeItemProps_Payer(sCountyRtcProps), LosConvert.GfeItemProps_FhaAllow(sCountyRtcProps), LosConvert.GfeItemProps_Poc(sCountyRtcProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(sCountyRtcProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(sCountyRtcProps), LosConvert.GfeItemProps_ShowQmWarning(sCountyRtcProps));
            sSettlementStateRtcPc = sStateRtcPc;
            sSettlementStateRtcMb = sStateRtcMb;
            dflpProps = (ccTemplate == null) ? sSettlementStateRtcProps : ccTemplate.cStateRtcProps;
            sSettlementStateRtcProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sStateRtcProps), LosConvert.GfeItemProps_ToBr(sStateRtcProps), LosConvert.GfeItemProps_Payer(sStateRtcProps), LosConvert.GfeItemProps_FhaAllow(sStateRtcProps), LosConvert.GfeItemProps_Poc(sStateRtcProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(sStateRtcProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(sStateRtcProps), LosConvert.GfeItemProps_ShowQmWarning(sStateRtcProps));
            sSettlementStateRtcBaseT = sStateRtcBaseT;
            sSettlementU1GovRtcPc = sU1GovRtcPc;
            sSettlementU1GovRtcDesc = sU1GovRtcDesc;
            dflpProps = (ccTemplate == null) ? sSettlementU1GovRtcProps : ccTemplate.cU1GovRtcProps;
            sSettlementU1GovRtcProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sU1GovRtcProps), LosConvert.GfeItemProps_ToBr(sU1GovRtcProps), LosConvert.GfeItemProps_Payer(sU1GovRtcProps), LosConvert.GfeItemProps_FhaAllow(sU1GovRtcProps), LosConvert.GfeItemProps_Poc(sU1GovRtcProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(sU1GovRtcProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(sU1GovRtcProps), LosConvert.GfeItemProps_ShowQmWarning(sU1GovRtcProps));
            sSettlementU1GovRtcMb = sU1GovRtcMb;
            sSettlementU1GovRtcBaseT = sU1GovRtcBaseT;
            sSettlementU2GovRtcPc = sU2GovRtcPc;
            sSettlementU2GovRtcDesc = sU2GovRtcDesc;
            dflpProps = (ccTemplate == null) ? sSettlementU2GovRtcProps : ccTemplate.cU2GovRtcProps;
            sSettlementU2GovRtcProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sU2GovRtcProps), LosConvert.GfeItemProps_ToBr(sU2GovRtcProps), LosConvert.GfeItemProps_Payer(sU2GovRtcProps), LosConvert.GfeItemProps_FhaAllow(sU2GovRtcProps), LosConvert.GfeItemProps_Poc(sU2GovRtcProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(sU2GovRtcProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(sU2GovRtcProps), LosConvert.GfeItemProps_ShowQmWarning(sU2GovRtcProps));
            sSettlementU2GovRtcMb = sU2GovRtcMb;
            sSettlementU2GovRtcBaseT = sU2GovRtcBaseT;
            sSettlementU3GovRtcPc = sU3GovRtcPc;
            sSettlementU3GovRtcDesc = sU3GovRtcDesc;
            dflpProps = (ccTemplate == null) ? sSettlementU3GovRtcProps : ccTemplate.cU3GovRtcProps;
            sSettlementU3GovRtcProps = LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sU3GovRtcProps), LosConvert.GfeItemProps_ToBr(sU3GovRtcProps), LosConvert.GfeItemProps_Payer(sU3GovRtcProps), LosConvert.GfeItemProps_FhaAllow(sU3GovRtcProps), LosConvert.GfeItemProps_Poc(sU3GovRtcProps), LosConvert.GfeItemProps_Dflp(dflpProps), LosConvert.GfeItemProps_PaidToThirdParty(sU3GovRtcProps), LosConvert.GfeItemProps_ThisPartyIsAffiliate(sU3GovRtcProps), LosConvert.GfeItemProps_ShowQmWarning(sU3GovRtcProps));
            sSettlementU3GovRtcMb = sU3GovRtcMb;
            sSettlementU3GovRtcBaseT = sU3GovRtcBaseT;
            
            // 1300
            sSettlementPestInspectF = sPestInspectF;
            dflpProps = (ccTemplate == null) ? sSettlementPestInspectFProps : ccTemplate.cPestInspectFProps;
            sSettlementPestInspectFProps =  GenerateSettlementPropsFromGfeProps(sSettlementPestInspectFProps, sPestInspectFProps, dflpProps); //LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sPestInspectFProps), LosConvert.GfeItemProps_ToBr(sPestInspectFProps), LosConvert.GfeItemProps_Payer(sPestInspectFProps), LosConvert.GfeItemProps_FhaAllow(sPestInspectFProps), LosConvert.GfeItemProps_Poc(sPestInspectFProps), LosConvert.GfeItemProps_Dflp(dflpProps));
            sSettlementU1Sc = sU1Sc;
            dflpProps = (ccTemplate == null) ? sSettlementU1ScProps : ccTemplate.cU1ScProps;
            sSettlementU1ScProps =  GenerateSettlementPropsFromGfeProps(sSettlementU1ScProps, sU1ScProps, dflpProps); //LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sU1ScProps), LosConvert.GfeItemProps_ToBr(sU1ScProps), LosConvert.GfeItemProps_Payer(sU1ScProps), LosConvert.GfeItemProps_FhaAllow(sU1ScProps), LosConvert.GfeItemProps_Poc(sU1ScProps), LosConvert.GfeItemProps_Dflp(dflpProps));
            sSettlementU1ScDesc = sU1ScDesc;
            sSettlementU2Sc = sU2Sc;
            dflpProps = (ccTemplate == null) ? sSettlementU2ScProps : ccTemplate.cU2ScProps;
            sSettlementU2ScProps =  GenerateSettlementPropsFromGfeProps(sSettlementU2ScProps, sU2ScProps, dflpProps); //LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sU2ScProps), LosConvert.GfeItemProps_ToBr(sU2ScProps), LosConvert.GfeItemProps_Payer(sU2ScProps), LosConvert.GfeItemProps_FhaAllow(sU2ScProps), LosConvert.GfeItemProps_Poc(sU2ScProps), LosConvert.GfeItemProps_Dflp(dflpProps));
            sSettlementU2ScDesc = sU2ScDesc;
            sSettlementU3Sc = sU3Sc;
            dflpProps = (ccTemplate == null) ? sSettlementU3ScProps : ccTemplate.cU3ScProps;
            sSettlementU3ScProps =  GenerateSettlementPropsFromGfeProps(sSettlementU3ScProps, sU3ScProps, dflpProps); //LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sU3ScProps), LosConvert.GfeItemProps_ToBr(sU3ScProps), LosConvert.GfeItemProps_Payer(sU3ScProps), LosConvert.GfeItemProps_FhaAllow(sU3ScProps), LosConvert.GfeItemProps_Poc(sU3ScProps), LosConvert.GfeItemProps_Dflp(dflpProps));
            sSettlementU3ScDesc = sU3ScDesc;
            sSettlementU4Sc = sU4Sc;
            dflpProps = (ccTemplate == null) ? sSettlementU4ScProps : ccTemplate.cU4ScProps;
            sSettlementU4ScProps =  GenerateSettlementPropsFromGfeProps(sSettlementU4ScProps, sU4ScProps, dflpProps); //LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sU4ScProps), LosConvert.GfeItemProps_ToBr(sU4ScProps), LosConvert.GfeItemProps_Payer(sU4ScProps), LosConvert.GfeItemProps_FhaAllow(sU4ScProps), LosConvert.GfeItemProps_Poc(sU4ScProps), LosConvert.GfeItemProps_Dflp(dflpProps));
            sSettlementU4ScDesc = sU4ScDesc;
            sSettlementU5Sc = sU5Sc;
            dflpProps = (ccTemplate == null) ? sSettlementU5ScProps : ccTemplate.cU5ScProps;
            sSettlementU5ScProps =   GenerateSettlementPropsFromGfeProps(sSettlementU5ScProps, sU5ScProps, dflpProps); //LosConvert.GfeItemProps_Pack(LosConvert.GfeItemProps_Apr(sU5ScProps), LosConvert.GfeItemProps_ToBr(sU5ScProps), LosConvert.GfeItemProps_Payer(sU5ScProps), LosConvert.GfeItemProps_FhaAllow(sU5ScProps), LosConvert.GfeItemProps_Poc(sU5ScProps), LosConvert.GfeItemProps_Dflp(dflpProps));
            sSettlementU5ScDesc = sU5ScDesc;
            
            s800U1FSettlementSection = s800U1FGfeSection;
            s800U2FSettlementSection = s800U2FGfeSection;
            s800U3FSettlementSection = s800U3FGfeSection;
            s800U4FSettlementSection = s800U4FGfeSection;
            s800U5FSettlementSection = s800U5FGfeSection;
            s904PiaSettlementSection = s904PiaGfeSection;
            s900U1PiaSettlementSection = s900U1PiaGfeSection;
            sEscrowFSettlementSection = sEscrowFGfeSection;
            sDocPrepFSettlementSection = sDocPrepFGfeSection;
            sNotaryFSettlementSection = sNotaryFGfeSection;
            sAttorneyFSettlementSection = sAttorneyFGfeSection;
            sTitleInsFSettlementSection = sTitleInsFGfeSection;
            sU1TcSettlementSection = sU1TcGfeSection;
            sU2TcSettlementSection = sU2TcGfeSection;
            sU3TcSettlementSection = sU3TcGfeSection;
            sU4TcSettlementSection = sU4TcGfeSection;
            sU1GovRtcSettlementSection = sU1GovRtcGfeSection;
            sU2GovRtcSettlementSection = sU2GovRtcGfeSection;
            sU3GovRtcSettlementSection = sU3GovRtcGfeSection;
            sU1ScSettlementSection = sU1ScGfeSection;
            sU2ScSettlementSection = sU2ScGfeSection;
            sU3ScSettlementSection = sU3ScGfeSection;
            sU4ScSettlementSection = sU4ScGfeSection;
            sU5ScSettlementSection = sU5ScGfeSection;
        }

        #endregion

        #region Funding Fields

        [DependsOn]
        private decimal sWarehouseMaxFundPc
        {
            get { return GetRateField("sWarehouseMaxFundPc"); }
            set { SetRateField("sWarehouseMaxFundPc", value); }
        }

        public string sWarehouseMaxFundPc_rep
        {
            get { return ToRateString(() => sWarehouseMaxFundPc); }
            set { sWarehouseMaxFundPc = ToRate(value); }
        }

        [DependsOn(nameof(sFinalLAmt), nameof(sWarehouseMaxFundPc))]
        private decimal sWarehouseMaxFundAmt
        {
            get
            {
                return sFinalLAmt * (sWarehouseMaxFundPc / 100);
            }
        }

        public string sWarehouseMaxFundAmt_rep
        {
            get { return ToMoneyString(() => sWarehouseMaxFundAmt); }
        }

        [DependsOn(nameof(sSettlementTotalDedFromLoanProc))]
        private decimal sSettlementChargesDedFromLoanProc
        {
            get
            {
                return -1 * sSettlementTotalDedFromLoanProc;
            }
        }

        public string sSettlementChargesDedFromLoanProc_rep
        {
            get { return ToMoneyString(() => sSettlementChargesDedFromLoanProc); }
        }

        [DependsOn]
        private decimal sOtherFundAdj
        {
            get { return GetMoneyField("sOtherFundAdj"); }
            set { SetMoneyField("sOtherFundAdj", value); }
        }

        public string sOtherFundAdj_rep
        {
            get { return ToMoneyString(() => sOtherFundAdj); }
            set { sOtherFundAdj = ToMoney(value); }
        }

        [DependsOn(nameof(sFinalLAmt), nameof(sSettlementChargesDedFromLoanProc), nameof(sOtherFundAdj), nameof(sSettlementTotalFundByLenderAtClosing), nameof(sAdjustmentsOtherCreditsDeductedFromLoanProceeds), nameof(sAdjustmentsOtherCreditsFundedByLenderAtClosing))]
        protected virtual decimal sAmtReqToFund
        {
            get
            {
                return SumMoney(
                    sFinalLAmt,
                    sSettlementChargesDedFromLoanProc,
                    sOtherFundAdj,
                    sSettlementTotalFundByLenderAtClosing,
                    sAdjustmentsOtherCreditsDeductedFromLoanProceeds,
                    sAdjustmentsOtherCreditsFundedByLenderAtClosing);
            }
        }

        public string sAmtReqToFund_rep
        {
            get { return ToMoneyString(() => sAmtReqToFund); }
        }

        [DependsOn]
        public bool sAmtFundFromWarehouseLineLckd
        {
            get { return GetBoolField("sAmtFundFromWarehouseLineLckd"); }
            set { SetBoolField("sAmtFundFromWarehouseLineLckd", value); }
        }

        [DependsOn(nameof(sAmtReqToFund), nameof(sWarehouseMaxFundAmt), nameof(sAmtFundFromWarehouseLineLckd), nameof(sAmtFundFromWarehouseLine))]
        public decimal sAmtFundFromWarehouseLine
        {
            get
            {
                if (sAmtFundFromWarehouseLineLckd)
                {
                    return GetMoneyField("sAmtFundFromWarehouseLine", 4);
                }
                else
                {
                    return Math.Min(sAmtReqToFund, sWarehouseMaxFundAmt);
                }
            }
            set
            {
                SetMoneyField("sAmtFundFromWarehouseLine", value);
            }
        }

        public string sAmtFundFromWarehouseLine_rep
        {
            get { return ToMoneyString(() => sAmtFundFromWarehouseLine); }
            set { sAmtFundFromWarehouseLine = ToMoney(value); }
        }

        [DependsOn(nameof(sAmtFundFromWarehouseLine), nameof(sAmtReqToFund))]
        public decimal sTotalAmtFund
        {
            get { return Math.Max(sAmtReqToFund, sAmtFundFromWarehouseLine); }
        }

        public string sTotalAmtFund_rep
        {
            get { return ToMoneyString(() => sTotalAmtFund); }
        }


        [DependsOn(nameof(sAmtReqToFund), nameof(sAmtFundFromWarehouseLine))]
        public decimal sFundReqForShortfall
        {
            get
            {
                return Math.Max((sAmtReqToFund - sAmtFundFromWarehouseLine), 0);
            }
        }

        public string sFundReqForShortfall_rep
        {
            get { return ToMoneyString(() => sFundReqForShortfall); }
        }


        [DependsOn(nameof(sAmtFundFromWarehouseLine), nameof(sAmtReqToFund))]
        public decimal sChkDueFromClosing
        {
            get
            {
                return Math.Max(sAmtFundFromWarehouseLine - sAmtReqToFund, 0);
            }
        }

        public string sChkDueFromClosing_rep
        {
            get { return ToMoneyString(() => sChkDueFromClosing); }
        }

        public bool sChkDueFromClosingIsZero
        {
            get { return sChkDueFromClosing == 0; }
        }

        [DependsOn(nameof(sChkDueFromClosing), nameof(sChkDueFromClosingRcvd))]
        public bool sChkDueFromClosingRcvd
        {
            get
            {
                if (sChkDueFromClosing == 0)
                {
                    return false;
                }
                else
                {
                    return GetBoolField("sChkDueFromClosingRcvd");
                }
            }
            set { SetBoolField("sChkDueFromClosingRcvd", value); }
        }

        [DependsOn]
        public CDateTime sFundsOrderedD
        {
            get { return GetDateTimeField("sFundsOrderedD"); }
            set { SetDateTimeField("sFundsOrderedD", value); }
        }
        public string sFundsOrderedD_rep
        {
            get { return GetDateTimeField_rep("sFundsOrderedD"); }
            set { SetDateTimeField_rep("sFundsOrderedD", value); }
        }

        [DependsOn]
        public CDateTime sShippedToWarehouseD
        {
            get { return GetDateTimeField("sShippedToWarehouseD"); }
            set { SetDateTimeField("sShippedToWarehouseD", value); }
        }
        public string sShippedToWarehouseD_rep
        {
            get { return GetDateTimeField_rep("sShippedToWarehouseD"); }
            set { SetDateTimeField_rep("sShippedToWarehouseD", value); }
        }

        [DependsOn]
        public string sShippedToWarehouseN
        {
            get { return GetLongTextField("sShippedToWarehouseN"); }
            set { SetLongTextField("sShippedToWarehouseN", value); }
        }

        [DependsOn]
        public string sTrackingN
        {
            get { return GetStringVarCharField("sTrackingN"); }
            set { SetStringVarCharField("sTrackingN", value); }
        }

        [DependsOn]
        public string sFundNotes
        {
            get { return GetLongTextField("sFundNotes"); }
            set { SetLongTextField("sFundNotes", value); }
        }

        [DependsOn(nameof(sFundingCfmAgentRoleT), nameof(sfGetAgentOfRole))]
        public E_AgentRoleT sFundingCfmAgentRoleT
        {
            get { return (E_AgentRoleT)GetTypeIndexField("sFundingCfmAgentRoleT"); }
            set
            {
                SetTypeIndexField("sFundingCfmAgentRoleT", (int)value);
                x_sFundingCfmAgentFields = GetAgentOfRole((E_AgentRoleT)value, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            }
        }

        [DependsOn]
        public bool sFundingCfmIsLocked
        {
            get { return GetBoolField("sFundingCfmIsLocked"); }
            set { SetBoolField("sFundingCfmIsLocked", value); }
        }

        private CAgentFields x_sFundingCfmAgentFields;

        [DependsOn(nameof(sFundingCfmAgentRoleT), nameof(sfGetAgentOfRole))]
        private CAgentFields sFundingCfmAgentFields
        {
            get
            {
                if(x_sFundingCfmAgentFields == null)
                    x_sFundingCfmAgentFields = GetAgentOfRole(sFundingCfmAgentRoleT, E_ReturnOptionIfNotExist.ReturnEmptyObject);

                return x_sFundingCfmAgentFields;
            }
        }

        [DependsOn(nameof(sFundingCompanyName), nameof(sFundingCfmIsLocked), nameof(sFundingCfmAgentFields))]
        public string sFundingCompanyName
        {
            get
            {
                if (sFundingCfmIsLocked)
                {
                    return GetStringVarCharField("sFundingCompanyName");
                }
                else
                {
                    string data = sFundingCfmAgentFields.CompanyName;
                    if (IsCachingUtilityObj)
                    {
                        data = GetStringForCacheTable(data, "sFundingCompanyName");
                    }
                    return data;
                }
            }
            set { SetStringVarCharField("sFundingCompanyName", value); }
        }

        [DependsOn(nameof(sFundingBranchName), nameof(sFundingCfmIsLocked), nameof(sFundingCfmAgentFields))]
        public string sFundingBranchName
        {
            get
            {
                if (sFundingCfmIsLocked)
                {
                    return GetStringVarCharField("sFundingBranchName");
                }
                else
                {
                    string data = sFundingCfmAgentFields.BranchName;

                    if (IsCachingUtilityObj)
                    {
                        data = GetStringForCacheTable(data, "sFundingBranchName");
                    }

                    return data;
                }
            }
            set { SetStringVarCharField("sFundingBranchName", value); }
        }

        [DependsOn(nameof(sFundingCompanyAddr), nameof(sFundingCfmIsLocked), nameof(sFundingCfmAgentFields))]
        public string sFundingCompanyAddr
        {
            get
            {
                if (sFundingCfmIsLocked)
                {
                    return GetStringVarCharField("sFundingCompanyAddr");
                }
                else
                {
                    string data = sFundingCfmAgentFields.StreetAddr;

                    if (IsCachingUtilityObj)
                    {
                        data = GetStringForCacheTable(data, "sFundingCompanyAddr");
                    }
                    return data;
                }
            }
            set { SetStringVarCharField("sFundingCompanyAddr", value); }
        }

        [DependsOn(nameof(sFundingCompanyCity), nameof(sFundingCfmIsLocked), nameof(sFundingCfmAgentFields))]
        public string sFundingCompanyCity
        {
            get
            {
                if (sFundingCfmIsLocked)
                {
                    return GetStringVarCharField("sFundingCompanyCity");
                }
                else
                {
                    string data = sFundingCfmAgentFields.City;

                    if (IsCachingUtilityObj)
                    {
                        data = GetStringForCacheTable(data, "sFundingCompanyCity");
                    }

                    return data;
                }
            }
            set { SetStringVarCharField("sFundingCompanyCity", value); }
        }

        [DependsOn(nameof(sFundingCompanyState), nameof(sFundingCfmIsLocked), nameof(sFundingCfmAgentFields))]
        public string sFundingCompanyState
        {
            get
            {
                if (sFundingCfmIsLocked)
                {
                    return GetStringCharField("sFundingCompanyState");
                }
                else
                {
                    string data = sFundingCfmAgentFields.State;

                    if (IsCachingUtilityObj)
                    {
                        data = GetStringForCacheTable(data, "sFundingCompanyState");
                    }

                    return data;
                }
            }
            set { SetStringCharField("sFundingCompanyState", value); }
        }

        [DependsOn(nameof(sFundingCompanyZip), nameof(sFundingCfmIsLocked), nameof(sFundingCfmAgentFields))]
        public string sFundingCompanyZip
        {
            get
            {
                if (sFundingCfmIsLocked)
                {
                    return GetStringCharField("sFundingCompanyZip");
                }
                else
                {
                    string data = sFundingCfmAgentFields.Zip;

                    if (IsCachingUtilityObj)
                    {
                        data = GetStringForCacheTable(data, "sFundingCompanyZip");
                    }

                    return data;
                }
            }
            set { SetStringCharField("sFundingCompanyZip", value); }
        }

        [DependsOn(nameof(sFundingCompanyPhone), nameof(sFundingCfmIsLocked), nameof(sFundingCfmAgentFields))]
        public string sFundingCompanyPhone
        {
            get
            {
                if (sFundingCfmIsLocked)
                {
                    return GetStringVarCharField("sFundingCompanyPhone");
                }
                else
                {
                    string data = sFundingCfmAgentFields.PhoneOfCompany;

                    if (IsCachingUtilityObj)
                    {
                        data = GetStringForCacheTable(data, "sFundingCompanyPhone");
                    }

                    return data;
                }
            }
            set { SetStringVarCharField("sFundingCompanyPhone", value); }
        }

        [DependsOn(nameof(sFundingCompanyFax), nameof(sFundingCfmIsLocked), nameof(sFundingCfmAgentFields))]
        public string sFundingCompanyFax
        {
            get
            {
                if (sFundingCfmIsLocked)
                {
                    return GetStringVarCharField("sFundingCompanyFax");
                }
                else
                {
                    string data = sFundingCfmAgentFields.FaxOfCompany;

                    if (IsCachingUtilityObj)
                    {
                        data = GetStringForCacheTable(data, "sFundingCompanyFax");
                    }

                    return data;
                }
            }
            set { SetStringVarCharField("sFundingCompanyFax", value); }
        }

        [DependsOn(nameof(sFundingContactName), nameof(sFundingCfmIsLocked), nameof(sFundingCfmAgentFields))]
        public string sFundingContactName
        {
            get
            {
                if (sFundingCfmIsLocked)
                {
                    return GetStringVarCharField("sFundingContactName");
                }
                else
                {
                    string data = sFundingCfmAgentFields.AgentName;

                    if (IsCachingUtilityObj)
                    {
                        data = GetStringForCacheTable(data, "sFundingContactName");
                    }

                    return data;
                }
            }
            set { SetStringVarCharField("sFundingContactName", value); }
        }

        [DependsOn(nameof(sFundingBankName), nameof(sFundingCfmIsLocked), nameof(sFundingCfmAgentFields))]
        public string sFundingBankName
        {
            get
            {
                if (sFundingCfmIsLocked)
                {
                    return GetStringVarCharField("sFundingBankName");
                }
                else
                {
                    string data = sFundingCfmAgentFields.PayToBankName;

                    if (IsCachingUtilityObj)
                    {
                        data = GetStringForCacheTable(data, "sFundingBankName");
                    }

                    return data;
                }
            }
            set { SetStringVarCharField("sFundingBankName", value); }
        }
        [DependsOn(nameof(sFundingBankCityState), nameof(sFundingCfmIsLocked), nameof(sFundingCfmAgentFields))]
        public string sFundingBankCityState
        {
            get
            {
                if (sFundingCfmIsLocked)
                {
                    return GetStringVarCharField("sFundingBankCityState");
                }
                else
                {
                    string data = sFundingCfmAgentFields.PayToBankCityState;

                    if (IsCachingUtilityObj)
                    {
                        data = GetStringForCacheTable(data, "sFundingBankCityState");
                    }

                    return data;

                }
            }
        
            set { SetStringVarCharField("sFundingBankCityState", value); }
        }
        [DependsOn(nameof(sFundingABANumber), nameof(sFundingCfmIsLocked), nameof(sFundingCfmAgentFields))]
        public Sensitive<string> sFundingABANumber
        {
            get
            {
                if (sFundingCfmIsLocked)
                {
                    return GetStringVarCharField("sFundingABANumber");
                }
                else
                {
                    string data = sFundingCfmAgentFields.PayToABANumber.Value;

                    if (IsCachingUtilityObj)
                    {
                        data = GetStringForCacheTable(data, "sFundingABANumber");
                    }

                    return data;
                }
            }
            set { SetStringVarCharField("sFundingABANumber", value.Value); }
        }
        [DependsOn(nameof(sFundingAccountNumber), nameof(sFundingCfmIsLocked), nameof(sFundingCfmAgentFields))]
        public Sensitive<string> sFundingAccountNumber
        {
            get
            {
                if (sFundingCfmIsLocked)
                {
                    return GetStringVarCharField("sFundingAccountNumber");
                }
                else
                {
                    string data = sFundingCfmAgentFields.PayToAccountNumber.Value;

                    if (IsCachingUtilityObj)
                    {
                        data = GetStringForCacheTable(data, "sFundingAccountNumber");
                    }

                    return data;
                }

            }
            set { SetStringVarCharField("sFundingAccountNumber", value.Value); }
        }
        [DependsOn(nameof(sFundingAccountName), nameof(sFundingCfmIsLocked), nameof(sFundingCfmAgentFields))]
        public string sFundingAccountName
        {
            get
            {
                if (sFundingCfmIsLocked)
                {
                    return GetStringVarCharField("sFundingAccountName");
                }
                else
                {
                    string data = sFundingCfmAgentFields.PayToAccountName;

                    if (IsCachingUtilityObj)
                    {
                        data = GetStringForCacheTable(data, "sFundingAccountName");
                    }

                    return data;
                }
            }
            set { SetStringVarCharField("sFundingAccountName", value); }
        }

        [DependsOn(nameof(sFundingFurtherCreditToAccountName), nameof(sFundingCfmIsLocked), nameof(sFundingCfmAgentFields))]
        public string sFundingFurtherCreditToAccountName
        {
            get
            {
                if (sFundingCfmIsLocked)
                {
                    return GetStringVarCharField("sFundingFurtherCreditToAccountName");
                }
                else
                {
                    string data = sFundingCfmAgentFields.FurtherCreditToAccountName;

                    if (IsCachingUtilityObj)
                    {
                        data = GetStringForCacheTable(data, "sFundingFurtherCreditToAccountName");
                    }
                    return data;
                }
            }
            set { SetStringVarCharField("sFundingFurtherCreditToAccountName", value); }
        }

        [DependsOn(nameof(sFundingFurtherCreditToAccountNumber), nameof(sFundingCfmIsLocked), nameof(sFundingCfmAgentFields))]
        public Sensitive<string> sFundingFurtherCreditToAccountNumber
        {
            get
            {
                if (sFundingCfmIsLocked)
                {
                    return GetStringVarCharField("sFundingFurtherCreditToAccountNumber");
                }
                else
                {
                    string data = sFundingCfmAgentFields.FurtherCreditToAccountNumber.Value;

                    if (IsCachingUtilityObj)
                    {
                        data = GetStringForCacheTable(data, "sFundingFurtherCreditToAccountNumber");
                    }

                    return data;
                }
            }
            set { SetStringVarCharField("sFundingFurtherCreditToAccountNumber", value.Value); }
        }

        [DependsOn]
        public string sFundingRequestBatchNumber
        {
            get { return GetStringVarCharField("sFundingRequestBatchNumber"); }
            set { SetStringVarCharField("sFundingRequestBatchNumber", value); }
        }

        [DependsOn]
        public Sensitive<string> sFundingAdditionalInstructionsLine1
        {
            get { return GetStringVarCharField("sFundingAdditionalInstructionsLine1"); }
            set { SetStringVarCharField("sFundingAdditionalInstructionsLine1", value.Value); }
        }
        [DependsOn]
        public Sensitive<string> sFundingAdditionalInstructionsLine2
        {
            get { return GetStringVarCharField("sFundingAdditionalInstructionsLine2"); }
            set { SetStringVarCharField("sFundingAdditionalInstructionsLine2", value.Value); }
        }

        #endregion



        [DependsOn(nameof(sSettlementChargesExportSource), nameof(sClosingCostFeeVersionT))]
        public E_SettlementChargesExportSource sSettlementChargesExportSource
        {
            get { return (E_SettlementChargesExportSource)GetTypeIndexField("sSettlementChargesExportSource"); }
            set
            {
                SetTypeIndexField("sSettlementChargesExportSource", (int)value);
            }
        }

        [DependsOn]
        public bool sIncludeGfeDataForDocMagicComparison
        {
            get { return GetBoolField("sIncludeGfeDataForDocMagicComparison"); }
            set { SetBoolField("sIncludeGfeDataForDocMagicComparison", value); }
        }

        [DependsOn(nameof(sSettlementLOrigF), nameof(sSettlementLOrigFProps))]
        private decimal sSettlementLOrigFPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementLOrigFProps))
                {
                    return sSettlementLOrigF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementLOrigFPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementLOrigFPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementLOrigF), nameof(sSettlementLOrigFProps))]
        private decimal sSettlementLOrigFPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementLOrigFProps))
                {
                    return sSettlementLOrigF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementLOrigFPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementLOrigFPaidByLender); }
        }

        [DependsOn(nameof(sSettlementLDiscnt), nameof(sSettlementLDiscntProps))]
        private decimal sSettlementLDiscntPaidToLender
        {
            get
            {

                if (sSettlementLDiscnt > 0)
                {
                    if (LosConvert.GfeItemProps_Dflp(sSettlementLDiscntProps))
                    {
                        return sSettlementLDiscnt;
                    }
                }
                return 0;
            }
        }
        public string sSettlementLDiscntPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementLDiscntPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementLDiscnt), nameof(sSettlementLDiscntProps))]
        private decimal sSettlementLDiscntPaidByLender
        {
            get
            {
                return 0;
            }
        }
        public string sSettlementLDiscntPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementLDiscntPaidByLender); }
        }

        /// <summary>
        /// DEPRECATED
        /// </summary>
        [DependsOn(nameof(sSettlementBrokerComp), nameof(sSettlementBrokerCompProps))]
        private decimal sSettlementBrokerCompPaidToLender
        {
            get
            {
                return 0;
            }
        }
        /// <summary>
        /// DEPRECATED
        /// </summary>
        public string sSettlementBrokerCompPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementBrokerCompPaidToLender);
            }
        }

        [DependsOn(nameof(sSettlementBrokerComp), nameof(sSettlementBrokerCompProps), nameof(sSettlementLDiscnt), nameof(sOriginatorCompensationPaymentSourceT), nameof(sGfeOriginatorCompF))]
        private decimal sSettlementBrokerCompPaidByLender
        {
            get
            {
                if (sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)
                {
                    return sGfeOriginatorCompF;
                }
                else
                {
                    return 0;
                }
            }
        }

        public string sSettlementBrokerCompPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementBrokerCompPaidByLender); }
        }

        /// <summary>
        /// DEPRECATED
        /// </summary>
        [DependsOn(nameof(sSettlementLOrigBrokerCreditF), nameof(sSettlementLOrigBrokerCreditFProps))]
        private decimal sSettlementLOrigBrokerCreditFPaidToLender
        {
            get
            {
                return 0;
            }
        }

        /// <summary>
        /// DEPRECATED
        /// </summary>
        public string sSettlementLOrigBrokerCreditFPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementLOrigBrokerCreditFPaidToLender);
            }
        }

        /// <summary>
        /// DEPRECATED
        /// </summary>
        [DependsOn(nameof(sSettlementLOrigBrokerCreditF), nameof(sSettlementLOrigBrokerCreditFProps), nameof(sSettlementLDiscnt), nameof(sSettlementBrokerCompProps))]
        private decimal sSettlementLOrigBrokerCreditFPaidByLender
        {
            get
            {
                // 12/22/2010 dd - If 802 < 0 then populate net credit to borrower as paid by lender
                if (sSettlementLDiscnt == 0 || LosConvert.GfeItemProps_Poc(sSettlementBrokerCompProps))
                {
                    return -1 * sSettlementLOrigBrokerCreditF;
                }
                return 0;
            }
        }

        /// <summary>
        /// DEPRECATED
        /// </summary>
        public string sSettlementLOrigBrokerCreditFPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementLOrigBrokerCreditFPaidByLender); }
        }

        [DependsOn(nameof(sSettlementApprF), nameof(sSettlementApprFProps))]
        private decimal sSettlementApprFPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementApprFProps))
                {
                    return sSettlementApprF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementApprFPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementApprFPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementApprF), nameof(sSettlementApprFProps))]
        private decimal sSettlementApprFPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementApprFProps))
                {
                    return sSettlementApprF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementApprFPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementApprFPaidByLender); }
        }

        [DependsOn(nameof(sSettlementCrF), nameof(sSettlementCrFProps))]
        private decimal sSettlementCrFPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementCrFProps))
                {
                    return sSettlementCrF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementCrFPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementCrFPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementCrF), nameof(sSettlementCrFProps))]
        private decimal sSettlementCrFPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementCrFProps))
                {
                    return sSettlementCrF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementCrFPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementCrFPaidByLender); }
        }

        [DependsOn(nameof(sSettlementTxServF), nameof(sSettlementTxServFProps))]
        private decimal sSettlementTxServFPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementTxServFProps))
                {
                    return sSettlementTxServF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementTxServFPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementTxServFPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementTxServF), nameof(sSettlementTxServFProps))]
        private decimal sSettlementTxServFPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementTxServFProps))
                {
                    return sSettlementTxServF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementTxServFPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementTxServFPaidByLender); }
        }

        [DependsOn(nameof(sSettlementFloodCertificationF), nameof(sSettlementFloodCertificationFProps))]
        private decimal sSettlementFloodCertificationFPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementFloodCertificationFProps))
                {
                    return sSettlementFloodCertificationF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementFloodCertificationFPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementFloodCertificationFPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementFloodCertificationF), nameof(sSettlementFloodCertificationFProps))]
        private decimal sSettlementFloodCertificationFPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementFloodCertificationFProps))
                {
                    return sSettlementFloodCertificationF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementFloodCertificationFPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementFloodCertificationFPaidByLender); }
        }

        [DependsOn(nameof(sSettlementMBrokF), nameof(sSettlementMBrokFProps))]
        private decimal sSettlementMBrokFPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementMBrokFProps))
                {
                    return sSettlementMBrokF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementMBrokFPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementMBrokFPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementMBrokF), nameof(sSettlementMBrokFProps))]
        private decimal sSettlementMBrokFPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementMBrokFProps))
                {
                    return sSettlementMBrokF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementMBrokFPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementMBrokFPaidByLender); }
        }

        [DependsOn(nameof(sSettlementInspectF), nameof(sSettlementInspectFProps))]
        private decimal sSettlementInspectFPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementInspectFProps))
                {
                    return sSettlementInspectF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementInspectFPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementInspectFPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementInspectF), nameof(sSettlementInspectFProps))]
        private decimal sSettlementInspectFPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementInspectFProps))
                {
                    return sSettlementInspectF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementInspectFPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementInspectFPaidByLender); }
        }

        [DependsOn(nameof(sSettlementProcF), nameof(sSettlementProcFProps))]
        private decimal sSettlementProcFPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementProcFProps))
                {
                    return sSettlementProcF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementProcFPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementProcFPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementProcF), nameof(sSettlementProcFProps))]
        private decimal sSettlementProcFPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementProcFProps))
                {
                    return sSettlementProcF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementProcFPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementProcFPaidByLender); }
        }

        [DependsOn(nameof(sSettlementUwF), nameof(sSettlementUwFProps))]
        private decimal sSettlementUwFPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementUwFProps))
                {
                    return sSettlementUwF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementUwFPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementUwFPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementUwF), nameof(sSettlementUwFProps))]
        private decimal sSettlementUwFPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementUwFProps))
                {
                    return sSettlementUwF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementUwFPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementUwFPaidByLender); }
        }

        [DependsOn(nameof(sSettlementWireF), nameof(sSettlementWireFProps))]
        private decimal sSettlementWireFPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementWireFProps))
                {
                    return sSettlementWireF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementWireFPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementWireFPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementWireF), nameof(sSettlementWireFProps))]
        private decimal sSettlementWireFPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementWireFProps))
                {
                    return sSettlementWireF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementWireFPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementWireFPaidByLender); }
        }

        [DependsOn(nameof(sSettlement800U1F), nameof(sSettlement800U1FProps))]
        private decimal sSettlement800U1FPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlement800U1FProps))
                {
                    return sSettlement800U1F;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlement800U1FPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlement800U1FPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlement800U1F), nameof(sSettlement800U1FProps))]
        private decimal sSettlement800U1FPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlement800U1FProps))
                {
                    return sSettlement800U1F;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlement800U1FPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlement800U1FPaidByLender); }
        }

        [DependsOn(nameof(sSettlement800U2F), nameof(sSettlement800U2FProps))]
        private decimal sSettlement800U2FPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlement800U2FProps))
                {
                    return sSettlement800U2F;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlement800U2FPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlement800U2FPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlement800U2F), nameof(sSettlement800U2FProps))]
        private decimal sSettlement800U2FPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlement800U2FProps))
                {
                    return sSettlement800U2F;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlement800U2FPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlement800U2FPaidByLender); }
        }

        [DependsOn(nameof(sSettlement800U3F), nameof(sSettlement800U3FProps))]
        private decimal sSettlement800U3FPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlement800U3FProps))
                {
                    return sSettlement800U3F;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlement800U3FPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlement800U3FPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlement800U3F), nameof(sSettlement800U3FProps))]
        private decimal sSettlement800U3FPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlement800U3FProps))
                {
                    return sSettlement800U3F;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlement800U3FPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlement800U3FPaidByLender); }
        }

        [DependsOn(nameof(sSettlement800U4F), nameof(sSettlement800U4FProps))]
        private decimal sSettlement800U4FPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlement800U4FProps))
                {
                    return sSettlement800U4F;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlement800U4FPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlement800U4FPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlement800U4F), nameof(sSettlement800U4FProps))]
        private decimal sSettlement800U4FPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlement800U4FProps))
                {
                    return sSettlement800U4F;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlement800U4FPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlement800U4FPaidByLender); }
        }

        [DependsOn(nameof(sSettlement800U5F), nameof(sSettlement800U5FProps))]
        private decimal sSettlement800U5FPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlement800U5FProps))
                {
                    return sSettlement800U5F;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlement800U5FPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlement800U5FPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlement800U5F), nameof(sSettlement800U5FProps))]
        private decimal sSettlement800U5FPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlement800U5FProps))
                {
                    return sSettlement800U5F;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlement800U5FPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlement800U5FPaidByLender); }
        }

        [DependsOn(nameof(sSettlementIPia), nameof(sSettlementIPiaProps))]
        private decimal sSettlementIPiaPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementIPiaProps))
                {
                    return sSettlementIPia;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementIPiaPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementIPiaPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementIPia), nameof(sSettlementIPiaProps))]
        private decimal sSettlementIPiaPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementIPiaProps))
                {
                    return sSettlementIPia;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementIPiaPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementIPiaPaidByLender); }
        }

        [DependsOn(nameof(sMipPia), nameof(sMipPiaProps))]
        private decimal sMipPiaPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sMipPiaProps))
                {
                    return sMipPia;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sMipPiaPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sMipPiaPaidToLender);
            }
        }
        [DependsOn(nameof(sMipPia), nameof(sMipPiaProps))]
        private decimal sMipPiaPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sMipPiaProps))
                {
                    return sMipPia;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sMipPiaPaidByLender_rep
        {
            get { return ToMoneyString(() => sMipPiaPaidByLender); }
        }

        [DependsOn(nameof(sSettlementHazInsPia), nameof(sSettlementHazInsPiaProps))]
        private decimal sSettlementHazInsPiaPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementHazInsPiaProps))
                {
                    return sSettlementHazInsPia;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementHazInsPiaPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementHazInsPiaPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementHazInsPia), nameof(sSettlementHazInsPiaProps))]
        private decimal sSettlementHazInsPiaPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementHazInsPiaProps))
                {
                    return sSettlementHazInsPia;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementHazInsPiaPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementHazInsPiaPaidByLender); }
        }

        [DependsOn(nameof(sSettlement904Pia), nameof(sSettlement904PiaProps))]
        private decimal sSettlement904PiaPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlement904PiaProps))
                {
                    return sSettlement904Pia;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlement904PiaPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlement904PiaPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlement904Pia), nameof(sSettlement904PiaProps))]
        private decimal sSettlement904PiaPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlement904PiaProps))
                {
                    return sSettlement904Pia;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlement904PiaPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlement904PiaPaidByLender); }
        }

        [DependsOn(nameof(sVaFf), nameof(sVaFfProps))]
        private decimal sVaFfPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sVaFfProps))
                {
                    return sVaFf;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sVaFfPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sVaFfPaidToLender);
            }
        }
        [DependsOn(nameof(sVaFf), nameof(sVaFfProps))]
        private decimal sVaFfPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sVaFfProps))
                {
                    return sVaFf;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sVaFfPaidByLender_rep
        {
            get { return ToMoneyString(() => sVaFfPaidByLender); }
        }

        [DependsOn(nameof(sSettlement900U1Pia), nameof(sSettlement900U1PiaProps))]
        private decimal sSettlement900U1PiaPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlement900U1PiaProps))
                {
                    return sSettlement900U1Pia;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlement900U1PiaPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlement900U1PiaPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlement900U1Pia), nameof(sSettlement900U1PiaProps))]
        private decimal sSettlement900U1PiaPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlement900U1PiaProps))
                {
                    return sSettlement900U1Pia;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlement900U1PiaPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlement900U1PiaPaidByLender); }
        }

        [DependsOn(nameof(sSettlementHazInsRsrv), nameof(sSettlementHazInsRsrvProps))]
        private decimal sSettlementHazInsRsrvPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementHazInsRsrvProps))
                {
                    return sSettlementHazInsRsrv;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementHazInsRsrvPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementHazInsRsrvPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementHazInsRsrv), nameof(sSettlementHazInsRsrvProps))]
        private decimal sSettlementHazInsRsrvPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementHazInsRsrvProps))
                {
                    return sSettlementHazInsRsrv;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementHazInsRsrvPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementHazInsRsrvPaidByLender); }
        }

        [DependsOn(nameof(sSettlementMInsRsrv), nameof(sSettlementMInsRsrvProps))]
        private decimal sSettlementMInsRsrvPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementMInsRsrvProps))
                {
                    return sSettlementMInsRsrv;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementMInsRsrvPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementMInsRsrvPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementMInsRsrv), nameof(sSettlementMInsRsrvProps))]
        private decimal sSettlementMInsRsrvPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementMInsRsrvProps))
                {
                    return sSettlementMInsRsrv;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementMInsRsrvPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementMInsRsrvPaidByLender); }
        }

        [DependsOn(nameof(sSettlementRealETxRsrv), nameof(sSettlementRealETxRsrvProps))]
        private decimal sSettlementRealETxRsrvPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementRealETxRsrvProps))
                {
                    return sSettlementRealETxRsrv;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementRealETxRsrvPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementRealETxRsrvPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementRealETxRsrv), nameof(sSettlementRealETxRsrvProps))]
        private decimal sSettlementRealETxRsrvPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementRealETxRsrvProps))
                {
                    return sSettlementRealETxRsrv;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementRealETxRsrvPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementRealETxRsrvPaidByLender); }
        }

        [DependsOn(nameof(sSettlementSchoolTxRsrv), nameof(sSettlementSchoolTxRsrvProps))]
        private decimal sSettlementSchoolTxRsrvPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementSchoolTxRsrvProps))
                {
                    return sSettlementSchoolTxRsrv;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementSchoolTxRsrvPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementSchoolTxRsrvPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementSchoolTxRsrv), nameof(sSettlementSchoolTxRsrvProps))]
        private decimal sSettlementSchoolTxRsrvPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementSchoolTxRsrvProps))
                {
                    return sSettlementSchoolTxRsrv;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementSchoolTxRsrvPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementSchoolTxRsrvPaidByLender); }
        }

        [DependsOn(nameof(sSettlementFloodInsRsrv), nameof(sSettlementFloodInsRsrvProps))]
        private decimal sSettlementFloodInsRsrvPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementFloodInsRsrvProps))
                {
                    return sSettlementFloodInsRsrv;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementFloodInsRsrvPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementFloodInsRsrvPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementFloodInsRsrv), nameof(sSettlementFloodInsRsrvProps))]
        private decimal sSettlementFloodInsRsrvPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementFloodInsRsrvProps))
                {
                    return sSettlementFloodInsRsrv;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementFloodInsRsrvPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementFloodInsRsrvPaidByLender); }
        }

        [DependsOn(nameof(sSettlementAggregateAdjRsrv), nameof(sSettlementAggregateAdjRsrvProps))]
        private decimal sSettlementAggregateAdjRsrvPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementAggregateAdjRsrvProps))
                {
                    return sSettlementAggregateAdjRsrv;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementAggregateAdjRsrvPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementAggregateAdjRsrvPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementAggregateAdjRsrv), nameof(sSettlementAggregateAdjRsrvProps))]
        private decimal sSettlementAggregateAdjRsrvPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementAggregateAdjRsrvProps))
                {
                    return sSettlementAggregateAdjRsrv;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementAggregateAdjRsrvPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementAggregateAdjRsrvPaidByLender); }
        }

        [DependsOn(nameof(sSettlement1008Rsrv), nameof(sSettlement1008RsrvProps))]
        private decimal sSettlement1008RsrvPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlement1008RsrvProps))
                {
                    return sSettlement1008Rsrv;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlement1008RsrvPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlement1008RsrvPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlement1008Rsrv), nameof(sSettlement1008RsrvProps))]
        private decimal sSettlement1008RsrvPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlement1008RsrvProps))
                {
                    return sSettlement1008Rsrv;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlement1008RsrvPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlement1008RsrvPaidByLender); }
        }

        [DependsOn(nameof(sSettlement1009Rsrv), nameof(sSettlement1009RsrvProps))]
        private decimal sSettlement1009RsrvPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlement1009RsrvProps))
                {
                    return sSettlement1009Rsrv;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlement1009RsrvPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlement1009RsrvPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlement1009Rsrv), nameof(sSettlement1009RsrvProps))]
        private decimal sSettlement1009RsrvPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlement1009RsrvProps))
                {
                    return sSettlement1009Rsrv;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlement1009RsrvPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlement1009RsrvPaidByLender); }
        }
        [DependsOn(nameof(sSettlementU3Rsrv), nameof(sSettlementU3RsrvProps))]
        private decimal sSettlementU3RsrvPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementU3RsrvProps))
                {
                    return sSettlementU3Rsrv;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementU3RsrvPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementU3RsrvPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementU3Rsrv), nameof(sSettlementU3RsrvProps))]
        private decimal sSettlementU3RsrvPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementU3RsrvProps))
                {
                    return sSettlementU3Rsrv;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementU3RsrvPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementU3RsrvPaidByLender); }
        }

        [DependsOn(nameof(sSettlementU4Rsrv), nameof(sSettlementU4RsrvProps))]
        private decimal sSettlementU4RsrvPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementU4RsrvProps))
                {
                    return sSettlementU4Rsrv;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementU4RsrvPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementU4RsrvPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementU4Rsrv), nameof(sSettlementU4RsrvProps))]
        private decimal sSettlementU4RsrvPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementU4RsrvProps))
                {
                    return sSettlementU4Rsrv;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementU4RsrvPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementU4RsrvPaidByLender); }
        }
        [DependsOn(nameof(sSettlementEscrowF), nameof(sSettlementEscrowFProps))]
        private decimal sSettlementEscrowFPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementEscrowFProps))
                {
                    return sSettlementEscrowF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementEscrowFPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementEscrowFPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementEscrowF), nameof(sSettlementEscrowFProps))]
        private decimal sSettlementEscrowFPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementEscrowFProps))
                {
                    return sSettlementEscrowF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementEscrowFPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementEscrowFPaidByLender); }
        }

        [DependsOn(nameof(sSettlementOwnerTitleInsF), nameof(sSettlementOwnerTitleInsFProps))]
        private decimal sSettlementOwnerTitleInsFPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementOwnerTitleInsFProps))
                {
                    return sSettlementOwnerTitleInsF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementOwnerTitleInsFPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementOwnerTitleInsFPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementOwnerTitleInsF), nameof(sSettlementOwnerTitleInsFProps))]
        private decimal sSettlementOwnerTitleInsFPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementOwnerTitleInsFProps))
                {
                    return sSettlementOwnerTitleInsF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementOwnerTitleInsFPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementOwnerTitleInsFPaidByLender); }
        }

        [DependsOn(nameof(sSettlementTitleInsF), nameof(sSettlementTitleInsFProps))]
        private decimal sSettlementTitleInsFPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementTitleInsFProps))
                {
                    return sSettlementTitleInsF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementTitleInsFPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementTitleInsFPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementTitleInsF), nameof(sSettlementTitleInsFProps))]
        private decimal sSettlementTitleInsFPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementTitleInsFProps))
                {
                    return sSettlementTitleInsF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementTitleInsFPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementTitleInsFPaidByLender); }
        }

        [DependsOn(nameof(sSettlementDocPrepF), nameof(sSettlementDocPrepFProps))]
        private decimal sSettlementDocPrepFPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementDocPrepFProps))
                {
                    return sSettlementDocPrepF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementDocPrepFPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementDocPrepFPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementDocPrepF), nameof(sSettlementDocPrepFProps))]
        private decimal sSettlementDocPrepFPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementDocPrepFProps))
                {
                    return sSettlementDocPrepF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementDocPrepFPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementDocPrepFPaidByLender); }
        }

        [DependsOn(nameof(sSettlementNotaryF), nameof(sSettlementNotaryFProps))]
        private decimal sSettlementNotaryFPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementNotaryFProps))
                {
                    return sSettlementNotaryF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementNotaryFPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementNotaryFPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementNotaryF), nameof(sSettlementNotaryFProps))]
        private decimal sSettlementNotaryFPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementNotaryFProps))
                {
                    return sSettlementNotaryF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementNotaryFPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementNotaryFPaidByLender); }
        }

        [DependsOn(nameof(sSettlementAttorneyF), nameof(sSettlementAttorneyFProps))]
        private decimal sSettlementAttorneyFPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementAttorneyFProps))
                {
                    return sSettlementAttorneyF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementAttorneyFPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementAttorneyFPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementAttorneyF), nameof(sSettlementAttorneyFProps))]
        private decimal sSettlementAttorneyFPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementAttorneyFProps))
                {
                    return sSettlementAttorneyF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementAttorneyFPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementAttorneyFPaidByLender); }
        }

        [DependsOn(nameof(sSettlementU1Tc), nameof(sSettlementU1TcProps))]
        private decimal sSettlementU1TcPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementU1TcProps))
                {
                    return sSettlementU1Tc;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementU1TcPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementU1TcPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementU1Tc), nameof(sSettlementU1TcProps))]
        private decimal sSettlementU1TcPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementU1TcProps))
                {
                    return sSettlementU1Tc;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementU1TcPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementU1TcPaidByLender); }
        }

        [DependsOn(nameof(sSettlementU2Tc), nameof(sSettlementU2TcProps))]
        private decimal sSettlementU2TcPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementU2TcProps))
                {
                    return sSettlementU2Tc;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementU2TcPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementU2TcPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementU2Tc), nameof(sSettlementU2TcProps))]
        private decimal sSettlementU2TcPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementU2TcProps))
                {
                    return sSettlementU2Tc;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementU2TcPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementU2TcPaidByLender); }
        }

        [DependsOn(nameof(sSettlementU3Tc), nameof(sSettlementU3TcProps))]
        private decimal sSettlementU3TcPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementU3TcProps))
                {
                    return sSettlementU3Tc;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementU3TcPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementU3TcPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementU3Tc), nameof(sSettlementU3TcProps))]
        private decimal sSettlementU3TcPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementU3TcProps))
                {
                    return sSettlementU3Tc;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementU3TcPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementU3TcPaidByLender); }
        }

        [DependsOn(nameof(sSettlementU4Tc), nameof(sSettlementU4TcProps))]
        private decimal sSettlementU4TcPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementU4TcProps))
                {
                    return sSettlementU4Tc;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementU4TcPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementU4TcPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementU4Tc), nameof(sSettlementU4TcProps))]
        private decimal sSettlementU4TcPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementU4TcProps))
                {
                    return sSettlementU4Tc;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementU4TcPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementU4TcPaidByLender); }
        }

        [DependsOn(nameof(sSettlementRecF), nameof(sSettlementRecFProps))]
        private decimal sSettlementRecFPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementRecFProps))
                {
                    return sSettlementRecF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementRecFPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementRecFPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementRecF), nameof(sSettlementRecFProps))]
        private decimal sSettlementRecFPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementRecFProps))
                {
                    return sSettlementRecF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementRecFPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementRecFPaidByLender); }
        }

        [DependsOn(nameof(sSettlementCountyRtc), nameof(sSettlementCountyRtcProps))]
        private decimal sSettlementCountyRtcPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementCountyRtcProps))
                {
                    return sSettlementCountyRtc;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementCountyRtcPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementCountyRtcPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementCountyRtc), nameof(sSettlementCountyRtcProps))]
        private decimal sSettlementCountyRtcPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementCountyRtcProps))
                {
                    return sSettlementCountyRtc;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementCountyRtcPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementCountyRtcPaidByLender); }
        }

        [DependsOn(nameof(sSettlementStateRtc), nameof(sSettlementStateRtcProps))]
        private decimal sSettlementStateRtcPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementStateRtcProps))
                {
                    return sSettlementStateRtc;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementStateRtcPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementStateRtcPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementStateRtc), nameof(sSettlementStateRtcProps))]
        private decimal sSettlementStateRtcPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementStateRtcProps))
                {
                    return sSettlementStateRtc;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementStateRtcPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementStateRtcPaidByLender); }
        }

        [DependsOn(nameof(sSettlementU1GovRtc), nameof(sSettlementU1GovRtcProps))]
        private decimal sSettlementU1GovRtcPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementU1GovRtcProps))
                {
                    return sSettlementU1GovRtc;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementU1GovRtcPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementU1GovRtcPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementU1GovRtc), nameof(sSettlementU1GovRtcProps))]
        private decimal sSettlementU1GovRtcPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementU1GovRtcProps))
                {
                    return sSettlementU1GovRtc;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementU1GovRtcPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementU1GovRtcPaidByLender); }
        }

        [DependsOn(nameof(sSettlementU2GovRtc), nameof(sSettlementU2GovRtcProps))]
        private decimal sSettlementU2GovRtcPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementU2GovRtcProps))
                {
                    return sSettlementU2GovRtc;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementU2GovRtcPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementU2GovRtcPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementU2GovRtc), nameof(sSettlementU2GovRtcProps))]
        private decimal sSettlementU2GovRtcPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementU2GovRtcProps))
                {
                    return sSettlementU2GovRtc;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementU2GovRtcPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementU2GovRtcPaidByLender); }
        }

        [DependsOn(nameof(sSettlementU3GovRtc), nameof(sSettlementU3GovRtcProps))]
        private decimal sSettlementU3GovRtcPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementU3GovRtcProps))
                {
                    return sSettlementU3GovRtc;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementU3GovRtcPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementU3GovRtcPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementU3GovRtc), nameof(sSettlementU3GovRtcProps))]
        private decimal sSettlementU3GovRtcPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementU3GovRtcProps))
                {
                    return sSettlementU3GovRtc;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementU3GovRtcPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementU3GovRtcPaidByLender); }
        }

        [DependsOn(nameof(sSettlementPestInspectF), nameof(sSettlementPestInspectFProps))]
        private decimal sSettlementPestInspectFPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementPestInspectFProps))
                {
                    return sSettlementPestInspectF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementPestInspectFPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementPestInspectFPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementPestInspectF), nameof(sSettlementPestInspectFProps))]
        private decimal sSettlementPestInspectFPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementPestInspectFProps))
                {
                    return sSettlementPestInspectF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementPestInspectFPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementPestInspectFPaidByLender); }
        }

        [DependsOn(nameof(sSettlementU1Sc), nameof(sSettlementU1ScProps))]
        private decimal sSettlementU1ScPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementU1ScProps))
                {
                    return sSettlementU1Sc;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementU1ScPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementU1ScPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementU1Sc), nameof(sSettlementU1ScProps))]
        private decimal sSettlementU1ScPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementU1ScProps))
                {
                    return sSettlementU1Sc;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementU1ScPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementU1ScPaidByLender); }
        }

        [DependsOn(nameof(sSettlementU2Sc), nameof(sSettlementU2ScProps))]
        private decimal sSettlementU2ScPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementU2ScProps))
                {
                    return sSettlementU2Sc;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementU2ScPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementU2ScPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementU2Sc), nameof(sSettlementU2ScProps))]
        private decimal sSettlementU2ScPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementU2ScProps))
                {
                    return sSettlementU2Sc;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementU2ScPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementU2ScPaidByLender); }
        }

        [DependsOn(nameof(sSettlementU3Sc), nameof(sSettlementU3ScProps))]
        private decimal sSettlementU3ScPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementU3ScProps))
                {
                    return sSettlementU3Sc;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementU3ScPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementU3ScPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementU3Sc), nameof(sSettlementU3ScProps))]
        private decimal sSettlementU3ScPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementU3ScProps))
                {
                    return sSettlementU3Sc;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementU3ScPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementU3ScPaidByLender); }
        }

        [DependsOn(nameof(sSettlementU4Sc), nameof(sSettlementU4ScProps))]
        private decimal sSettlementU4ScPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementU4ScProps))
                {
                    return sSettlementU4Sc;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementU4ScPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementU4ScPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementU4Sc), nameof(sSettlementU4ScProps))]
        private decimal sSettlementU4ScPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementU4ScProps))
                {
                    return sSettlementU4Sc;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementU4ScPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementU4ScPaidByLender); }
        }

        [DependsOn(nameof(sSettlementU5Sc), nameof(sSettlementU5ScProps))]
        private decimal sSettlementU5ScPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementU5ScProps))
                {
                    return sSettlementU5Sc;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementU5ScPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementU5ScPaidToLender);
            }
        }
        [DependsOn(nameof(sSettlementU5Sc), nameof(sSettlementU5ScProps))]
        private decimal sSettlementU5ScPaidByLender
        {
            get
            {
                if (LosConvert.GfeItemProps_NotPocAndPdByLender(sSettlementU5ScProps))
                {
                    return sSettlementU5Sc;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementU5ScPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementU5ScPaidByLender); }
        }
        
        private decimal sSettlementLenderCreditFPaidToLender
        {
            get
            {
                return 0;
            }
        }
        public string sSettlementLenderCreditFPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementLenderCreditFPaidToLender);
            }
        }

        [DependsOn(nameof(sSettlementLenderCreditF))]
        private decimal sSettlementLenderCreditFPaidByLender
        {
            get
            {
                return -1 * sSettlementLenderCreditF;
            }
        }
        public string sSettlementLenderCreditFPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementLenderCreditFPaidByLender); }
        }

        [DependsOn(nameof(sSettlementDiscountPointFProps), nameof(sSettlementDiscountPointF))]
        private decimal sSettlementDiscountPointFPaidToLender
        {
            get
            {
                if (LosConvert.GfeItemProps_Dflp(sSettlementDiscountPointFProps))
                {
                    return sSettlementDiscountPointF;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string sSettlementDiscountPointFPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementDiscountPointFPaidToLender);
            }
        }

        [DependsOn(nameof(sSettlementDiscountPointF))]
        private decimal sSettlementDiscountPointFPaidByLender
        {
            get
            {
                return 0;
            }
        }
        public string sSettlementDiscountPointFPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementDiscountPointFPaidByLender); }
        }

        private decimal sSettlementCreditLenderPaidItemFPaidToLender
        {
            get
            {
                return 0;
            }
        }
        public string sSettlementCreditLenderPaidItemFPaidToLender_rep
        {
            get
            {
                return ToMoneyString(() => sSettlementCreditLenderPaidItemFPaidToLender);
            }
        }

        private decimal sSettlementCreditLenderPaidItemFPaidByLender
        {
            get
            {
                return 0;
            }
        }
        public string sSettlementCreditLenderPaidItemFPaidByLender_rep
        {
            get { return ToMoneyString(() => sSettlementCreditLenderPaidItemFPaidByLender); }
        }

        [DependsOn(nameof(sSettlementDiscountPointFPaidToLender), nameof(sSettlementLOrigFPaidToLender), nameof(sSettlementLDiscntPaidToLender), nameof(sSettlementBrokerCompPaidToLender), nameof(sSettlementLOrigBrokerCreditFPaidToLender), nameof(sSettlementApprFPaidToLender), nameof(sSettlementCrFPaidToLender), nameof(sSettlementTxServFPaidToLender), nameof(sSettlementFloodCertificationFPaidToLender), nameof(sSettlementMBrokFPaidToLender), nameof(sSettlementInspectFPaidToLender), nameof(sSettlementProcFPaidToLender), nameof(sSettlementUwFPaidToLender), nameof(sSettlementWireFPaidToLender), nameof(sSettlement800U1FPaidToLender), nameof(sSettlement800U2FPaidToLender), nameof(sSettlement800U3FPaidToLender), nameof(sSettlement800U4FPaidToLender), nameof(sSettlement800U5FPaidToLender))]
        private decimal sSettlement800PaidToLenderTotal
        {
            get
            {
                return SumMoney(sSettlementLOrigFPaidToLender,
sSettlementLDiscntPaidToLender,
sSettlementBrokerCompPaidToLender,
sSettlementLOrigBrokerCreditFPaidToLender,
sSettlementApprFPaidToLender,
sSettlementCrFPaidToLender,
sSettlementTxServFPaidToLender,
sSettlementFloodCertificationFPaidToLender,
sSettlementMBrokFPaidToLender,
sSettlementInspectFPaidToLender,
sSettlementProcFPaidToLender,
sSettlementUwFPaidToLender,
sSettlementWireFPaidToLender,
sSettlement800U1FPaidToLender,
sSettlement800U2FPaidToLender,
sSettlement800U3FPaidToLender,
sSettlement800U4FPaidToLender,
sSettlement800U5FPaidToLender,
sSettlementCreditLenderPaidItemFPaidToLender,
sSettlementDiscountPointFPaidToLender,
sSettlementLenderCreditFPaidToLender);
            }
        }

        public string sSettlement800PaidToLenderTotal_rep
        {
            get { return ToMoneyString(() => sSettlement800PaidToLenderTotal); }
        }

        [DependsOn(nameof(sSettlementDiscountPointFPaidByLender), nameof(sSettlementLenderCreditFPaidByLender), nameof(sSettlementLOrigFPaidByLender), nameof(sSettlementLDiscntPaidByLender), nameof(sSettlementBrokerCompPaidByLender), nameof(sSettlementLOrigBrokerCreditFPaidByLender), nameof(sSettlementApprFPaidByLender), nameof(sSettlementCrFPaidByLender), nameof(sSettlementTxServFPaidByLender), nameof(sSettlementFloodCertificationFPaidByLender), nameof(sSettlementMBrokFPaidByLender), nameof(sSettlementInspectFPaidByLender), nameof(sSettlementProcFPaidByLender), nameof(sSettlementUwFPaidByLender), nameof(sSettlementWireFPaidByLender), nameof(sSettlement800U1FPaidByLender), nameof(sSettlement800U2FPaidByLender), nameof(sSettlement800U3FPaidByLender), nameof(sSettlement800U4FPaidByLender), nameof(sSettlement800U5FPaidByLender))]
        private decimal sSettlement800PaidByLenderTotal
        {
            get
            {
                return SumMoney(sSettlementLOrigFPaidByLender,
sSettlementLDiscntPaidByLender,
sSettlementBrokerCompPaidByLender,
sSettlementLOrigBrokerCreditFPaidByLender,
sSettlementApprFPaidByLender,
sSettlementCrFPaidByLender,
sSettlementTxServFPaidByLender,
sSettlementFloodCertificationFPaidByLender,
sSettlementMBrokFPaidByLender,
sSettlementInspectFPaidByLender,
sSettlementProcFPaidByLender,
sSettlementUwFPaidByLender,
sSettlementWireFPaidByLender,
sSettlement800U1FPaidByLender,
sSettlement800U2FPaidByLender,
sSettlement800U3FPaidByLender,
sSettlement800U4FPaidByLender,
sSettlement800U5FPaidByLender,
sSettlementCreditLenderPaidItemFPaidByLender,
sSettlementDiscountPointFPaidByLender,
sSettlementLenderCreditFPaidByLender);
            }
        }

        public string sSettlement800PaidByLenderTotal_rep
        {
            get { return ToMoneyString(() => sSettlement800PaidByLenderTotal); }
        }

        [DependsOn(nameof(sSettlementIPiaPaidToLender), nameof(sMipPiaPaidToLender), nameof(sSettlementHazInsPiaPaidToLender), nameof(sSettlement904PiaPaidToLender), nameof(sVaFfPaidToLender), nameof(sSettlement900U1PiaPaidToLender))]
        private decimal sSettlement900PaidToLenderTotal
        {
            get
            {
                return SumMoney(sSettlementIPiaPaidToLender,
sMipPiaPaidToLender,
sSettlementHazInsPiaPaidToLender,
sSettlement904PiaPaidToLender,
sVaFfPaidToLender,
sSettlement900U1PiaPaidToLender);
            }
        }
        public string sSettlement900PaidToLenderTotal_rep
        {
            get { return ToMoneyString(() => sSettlement900PaidToLenderTotal); }
        }

        [DependsOn(nameof(sSettlementIPiaPaidByLender), nameof(sMipPiaPaidByLender), nameof(sSettlementHazInsPiaPaidByLender), nameof(sSettlement904PiaPaidByLender), nameof(sVaFfPaidByLender), nameof(sSettlement900U1PiaPaidByLender))]
        private decimal sSettlement900PaidByLenderTotal
        {
            get
            {
                return SumMoney(sSettlementIPiaPaidByLender,
sMipPiaPaidByLender,
sSettlementHazInsPiaPaidByLender,
sSettlement904PiaPaidByLender,
sVaFfPaidByLender,
sSettlement900U1PiaPaidByLender);
            }
        }
        public string sSettlement900PaidByLenderTotal_rep
        {
            get { return ToMoneyString(() => sSettlement900PaidByLenderTotal); }
        }
        [DependsOn(nameof(sSettlementHazInsRsrvPaidToLender), nameof(sSettlementMInsRsrvPaidToLender), nameof(sSettlementRealETxRsrvPaidToLender), 
            nameof(sSettlementSchoolTxRsrvPaidToLender), nameof(sSettlementFloodInsRsrvPaidToLender), nameof(sSettlementAggregateAdjRsrvPaidToLender), 
            nameof(sSettlement1008RsrvPaidToLender), nameof(sSettlement1009RsrvPaidToLender), nameof(sSettlementU3RsrvPaidToLender), nameof(sSettlementU4RsrvPaidToLender))]
        private decimal sSettlement1000PaidToLenderTotal
        {
            get
            {
                return SumMoney(sSettlementHazInsRsrvPaidToLender,
                    sSettlementMInsRsrvPaidToLender,
                    sSettlementRealETxRsrvPaidToLender,
                    sSettlementSchoolTxRsrvPaidToLender,
                    sSettlementFloodInsRsrvPaidToLender,
                    sSettlementAggregateAdjRsrvPaidToLender,
                    sSettlement1008RsrvPaidToLender,
                    sSettlement1009RsrvPaidToLender,
                    sSettlementU3RsrvPaidToLender,
                    sSettlementU4RsrvPaidToLender);
            }
        }
        public string sSettlement1000PaidToLenderTotal_rep
        {
            get { return ToMoneyString(() => sSettlement1000PaidToLenderTotal); }
        }

        [DependsOn(nameof(sSettlementHazInsRsrvPaidByLender), nameof(sSettlementMInsRsrvPaidByLender), nameof(sSettlementRealETxRsrvPaidByLender), 
            nameof(sSettlementSchoolTxRsrvPaidByLender), nameof(sSettlementFloodInsRsrvPaidByLender), nameof(sSettlementAggregateAdjRsrvPaidByLender), 
            nameof(sSettlement1008RsrvPaidByLender), nameof(sSettlement1009RsrvPaidByLender), nameof(sSettlementU3RsrvPaidByLender), nameof(sSettlementU4RsrvPaidByLender))]
        private decimal sSettlement1000PaidByLenderTotal
        {
            get
            {
                return SumMoney(sSettlementHazInsRsrvPaidByLender,
                    sSettlementMInsRsrvPaidByLender,
                    sSettlementRealETxRsrvPaidByLender,
                    sSettlementSchoolTxRsrvPaidByLender,
                    sSettlementFloodInsRsrvPaidByLender,
                    sSettlementAggregateAdjRsrvPaidByLender,
                    sSettlement1008RsrvPaidByLender,
                    sSettlement1009RsrvPaidByLender,
                    sSettlementU3RsrvPaidByLender,
                    sSettlementU4RsrvPaidByLender);
            }
        }
        public string sSettlement1000PaidByLenderTotal_rep
        {
            get { return ToMoneyString(() => sSettlement1000PaidByLenderTotal); }
        }
        [DependsOn(nameof(sSettlementEscrowFPaidToLender), nameof(sSettlementOwnerTitleInsFPaidToLender), nameof(sSettlementTitleInsFPaidToLender), nameof(sSettlementDocPrepFPaidToLender), nameof(sSettlementNotaryFPaidToLender), nameof(sSettlementAttorneyFPaidToLender), nameof(sSettlementU1TcPaidToLender), nameof(sSettlementU2TcPaidToLender), nameof(sSettlementU3TcPaidToLender), nameof(sSettlementU4TcPaidToLender))]
        private decimal sSettlement1100PaidToLenderTotal
        {
            get
            {
                return SumMoney(sSettlementEscrowFPaidToLender,
sSettlementOwnerTitleInsFPaidToLender,
sSettlementTitleInsFPaidToLender,
sSettlementDocPrepFPaidToLender,
sSettlementNotaryFPaidToLender,
sSettlementAttorneyFPaidToLender,
sSettlementU1TcPaidToLender,
sSettlementU2TcPaidToLender,
sSettlementU3TcPaidToLender,
sSettlementU4TcPaidToLender);
            }
        }
        public string sSettlement1100PaidToLenderTotal_rep
        {
            get { return ToMoneyString(() => sSettlement1100PaidToLenderTotal); }
        }

        [DependsOn(nameof(sSettlementEscrowFPaidByLender), nameof(sSettlementOwnerTitleInsFPaidByLender), nameof(sSettlementTitleInsFPaidByLender), nameof(sSettlementDocPrepFPaidByLender), nameof(sSettlementNotaryFPaidByLender), nameof(sSettlementAttorneyFPaidByLender), nameof(sSettlementU1TcPaidByLender), nameof(sSettlementU2TcPaidByLender), nameof(sSettlementU3TcPaidByLender), nameof(sSettlementU4TcPaidByLender))]
        private decimal sSettlement1100PaidByLenderTotal
        {
            get
            {
                return SumMoney(sSettlementEscrowFPaidByLender,
sSettlementOwnerTitleInsFPaidByLender,
sSettlementTitleInsFPaidByLender,
sSettlementDocPrepFPaidByLender,
sSettlementNotaryFPaidByLender,
sSettlementAttorneyFPaidByLender,
sSettlementU1TcPaidByLender,
sSettlementU2TcPaidByLender,
sSettlementU3TcPaidByLender,
sSettlementU4TcPaidByLender);
            }
        }
        public string sSettlement1100PaidByLenderTotal_rep
        {
            get { return ToMoneyString(() => sSettlement1100PaidByLenderTotal); }
        }
        [DependsOn(nameof(sSettlementRecFPaidToLender), nameof(sSettlementCountyRtcPaidToLender), nameof(sSettlementStateRtcPaidToLender), nameof(sSettlementU1GovRtcPaidToLender), nameof(sSettlementU2GovRtcPaidToLender), nameof(sSettlementU3GovRtcPaidToLender))]
        private decimal sSettlement1200PaidToLenderTotal
        {
            get
            {
                return SumMoney(sSettlementRecFPaidToLender,
sSettlementCountyRtcPaidToLender,
sSettlementStateRtcPaidToLender,
sSettlementU1GovRtcPaidToLender,
sSettlementU2GovRtcPaidToLender,
sSettlementU3GovRtcPaidToLender);
            }
        }
        public string sSettlement1200PaidToLenderTotal_rep
        {
            get { return ToMoneyString(() => sSettlement1200PaidToLenderTotal); }
        }

        [DependsOn(nameof(sSettlementRecFPaidByLender), nameof(sSettlementCountyRtcPaidByLender), nameof(sSettlementStateRtcPaidByLender), nameof(sSettlementU1GovRtcPaidByLender), nameof(sSettlementU2GovRtcPaidByLender), nameof(sSettlementU3GovRtcPaidByLender))]
        private decimal sSettlement1200PaidByLenderTotal
        {
            get
            {
                return SumMoney(sSettlementRecFPaidByLender,
sSettlementCountyRtcPaidByLender,
sSettlementStateRtcPaidByLender,
sSettlementU1GovRtcPaidByLender,
sSettlementU2GovRtcPaidByLender,
sSettlementU3GovRtcPaidByLender);
            }
        }
        public string sSettlement1200PaidByLenderTotal_rep
        {
            get { return ToMoneyString(() => sSettlement1200PaidByLenderTotal); }
        }

        [DependsOn(nameof(sSettlementPestInspectFPaidToLender), nameof(sSettlementU1ScPaidToLender), nameof(sSettlementU2ScPaidToLender), nameof(sSettlementU3ScPaidToLender), nameof(sSettlementU4ScPaidToLender), nameof(sSettlementU5ScPaidToLender))]
        private decimal sSettlement1300PaidToLenderTotal
        {
            get
            {
                return SumMoney(sSettlementPestInspectFPaidToLender,
sSettlementU1ScPaidToLender,
sSettlementU2ScPaidToLender,
sSettlementU3ScPaidToLender,
sSettlementU4ScPaidToLender,
sSettlementU5ScPaidToLender);
            }
        }
        public string sSettlement1300PaidToLenderTotal_rep
        {
            get { return ToMoneyString(() => sSettlement1300PaidToLenderTotal); }
        }

        [DependsOn(nameof(sSettlementPestInspectFPaidByLender), nameof(sSettlementU1ScPaidByLender), nameof(sSettlementU2ScPaidByLender), nameof(sSettlementU3ScPaidByLender), nameof(sSettlementU4ScPaidByLender), nameof(sSettlementU5ScPaidByLender))]
        private decimal sSettlement1300PaidByLenderTotal
        {
            get
            {
                return SumMoney(sSettlementPestInspectFPaidByLender,
sSettlementU1ScPaidByLender,
sSettlementU2ScPaidByLender,
sSettlementU3ScPaidByLender,
sSettlementU4ScPaidByLender,
sSettlementU5ScPaidByLender);
            }
        }
        public string sSettlement1300PaidByLenderTotal_rep
        {
            get { return ToMoneyString(() => sSettlement1300PaidByLenderTotal); }
        }
    }
}
