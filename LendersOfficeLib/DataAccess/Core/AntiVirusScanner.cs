﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.symantec.scanengine.api;
using LendersOffice.Constants;
using System.IO;
using LendersOffice.Common;

namespace DataAccess
{
    /// <summary>
    /// Possible enumeration of results from Anti Virus Scan Engine
    /// </summary>
    public enum E_AntiVirusResult
    {
        /// <summary>
        /// No Virus was detected
        /// </summary>
        NoVirus,
        /// <summary>
        /// There was no file to scan
        /// </summary>
        NoFileToScan,
        /// <summary>
        /// The file is infected with a virus
        /// </summary>
        HasVirus,
        /// <summary>
        /// No valid Anti Virus Scan Engine exists
        /// </summary>
        NoValidScanEngines,
        /// <summary>
        /// Scan could not be performed
        /// </summary>
        ScanFailed,
        /// <summary>
        /// Transmission to Anti Virus Scan Engine failed
        /// </summary>
        TransmissionFailed,
        /// <summary>
        /// Anti Virus Scan Engine's License is 
        /// </summary>
        BadLicense,
        /// <summary>
        /// There was an unexpected exception from Anit Virus Scan Engine.
        /// </summary>
        UnexpectedScanException
    }

    /// <summary>
    /// Scans files for virus via network. 
    /// </summary>
    public static class AntiVirusScanner
    {
        private const int ANTI_VIRUS_ENGINE_PORT = 1344;
        /// <summary>
        /// Default pulled from the sdk
        /// </summary>
        private const int ANTI_VIRUS_READ_WRITE_TIMEOUT = 20000;

        /// <summary>
        /// Default pulled from sdk 
        /// </summary>
        private const int ANTI_VIRUS_RETRY_TIME = 20; 

        /// <summary>
        /// Instantiates the scanning objects
        /// </summary>
        /// <returns>A stream ready for file upload</returns>
        private static StreamScanRequest GetScanRequestStream()
        {
            List<ScanEngineInfo> scanEngines = new List<ScanEngineInfo>();
            foreach (string ip in ConstStage.AntiVirusScannerIPs)
            {
                if (!string.IsNullOrEmpty(ip))
                {
                    scanEngines.Add(new ScanEngineInfo(ip, ANTI_VIRUS_ENGINE_PORT));
                }
            }

            if (scanEngines.Count == 0)
            {
                return null;
            }

            ScanRequestManager requestManager = new ScanRequestManager();
            requestManager.PrepareForScan(scanEngines, ANTI_VIRUS_READ_WRITE_TIMEOUT, ANTI_VIRUS_RETRY_TIME);

            StreamScanRequest test = requestManager.CreateStreamScanRequest(Policy.SCAN);
            return test;
        }
        /// <summary>
        /// Returns the error message that should be shown to the user from AntiVirusResult. The error message will be
        /// empty, if there was no error, or the file could not be scanned
        /// </summary>
        /// <param name="result"></param>
        /// <returns>A string representing the error message that should be shown to the user, or empty if the result is OK.</returns>
        public static string GetUserErrorMessage(E_AntiVirusResult result)
        {
            string sResult = string.Empty;
            switch (result)
            {
                case E_AntiVirusResult.HasVirus:
                    sResult = "File a virus.";
                    break;
                case E_AntiVirusResult.NoFileToScan:
                    sResult = "There was no file to scan.";
                    break;
                case E_AntiVirusResult.NoVirus:
                    break;
                // Configuration Issues - Do not issue an error to the user, since we don't want to prevent the users from
                // Uploading the files if we have a configuration problem.
                case E_AntiVirusResult.BadLicense:
                    break;
                case E_AntiVirusResult.NoValidScanEngines:
                    break;
                // Anti Virus Scan could not perform its operation - Do not issue an error to the user, since we don't 
                // want to prevent the users from uploading the files if anti virus scan could not work properly.
                case E_AntiVirusResult.ScanFailed:
                case E_AntiVirusResult.TransmissionFailed:
                case E_AntiVirusResult.UnexpectedScanException:
                    break;
            }
            return sResult;
        }

        public static E_AntiVirusResult Scan(string path)
        {
            using (PerformanceStopwatch.Start("AntiVirusScanner.Scan"))
            {
                return ScanImpl(path);
            }
        }

        private static E_AntiVirusResult ScanImpl(string path)
        {
            FileInfo fileInfo = new FileInfo(path);
            if (!fileInfo.Exists)
            {
                return  E_AntiVirusResult.NoFileToScan;
            }

            try
            {
                StreamScanRequest scanRequest = GetScanRequestStream();
                if (scanRequest == null)
                {
                    return E_AntiVirusResult.NoValidScanEngines;
                }

                scanRequest.Start(fileInfo.Name, fileInfo.Name);

                int bufferSize = 65536;
                byte[] buffer = new byte[bufferSize];
                int chunk;
                long read = 0;
                
                using (FileStream fs = fileInfo.OpenRead())
                {
                    while ((chunk = fs.Read(buffer, 0, bufferSize)) > 0)
                    {
                        read += chunk;
                        byte[] sendBuffer = buffer;

                        if (chunk != bufferSize)
                        {
                            sendBuffer = new byte[chunk];
                            Array.Copy(buffer, sendBuffer, chunk);
                        }

                        if (!scanRequest.Send(sendBuffer))
                        {
                            return E_AntiVirusResult.TransmissionFailed;
                        }
                    }
                }

                Tools.Assert(read.Equals(fileInfo.Length), "File sent to virus scanner size doesnt match file size.");

                // Finish the Scanning and write the result to the steam. 
                // Since we are not asking for repairs we dont need to use any resources
                ScanResult sc = scanRequest.Finish(Stream.Null);

                if (sc.fileStatus == FileScanStatus.CLEAN && sc.threat.Count == 0 && sc.totalInfection == 0)
                {
                    return E_AntiVirusResult.NoVirus;
                }
                else if (sc.fileStatus == FileScanStatus.INFECTED || sc.fileStatus == FileScanStatus.UNREPAIRABLE || sc.threat.Count > 0 || sc.totalInfection > 0)
                {
                    return E_AntiVirusResult.HasVirus;
                }
                else
                {
                    if (sc.fileStatus == FileScanStatus.LICENSE_NA)
                    {
                        return E_AntiVirusResult.BadLicense;
                    }
                    else
                    {
                        return E_AntiVirusResult.ScanFailed;
                    }
                }
            }
            catch (ScanException ex)
            {
                Tools.LogErrorWithCriticalTracking(ex);
                if (ex.Message == "ERR_INVALID_HOST_ERROR")
                {
                    return E_AntiVirusResult.NoValidScanEngines;
                }
                else
                {
                    return E_AntiVirusResult.ScanFailed;
                }
            }
            catch (CBaseException ex)
            {
                Tools.LogErrorWithCriticalTracking(ex);
                return E_AntiVirusResult.UnexpectedScanException;
            }
        }
    }
}
