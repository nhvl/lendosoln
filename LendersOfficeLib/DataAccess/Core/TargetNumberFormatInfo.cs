﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// An internal class to hold the combinations of NumberFormatInfo instances needed
    /// to format different FormatTarget types. These are being rolled into their own
    /// class to facilitate re-using instances, since these remain constant per format.
    /// </summary>
    internal sealed class TargetNumberFormatInfo
    {
        /// <summary>
        /// Backing field for the static WebForm property. Initialized lazily.
        /// </summary>
        private static TargetNumberFormatInfo webForm;

        /// <summary>
        /// Backing field for the static Printout property. Initialized lazily.
        /// </summary>
        private static TargetNumberFormatInfo printout;

        /// <summary>
        /// Backing field for the static Xml property. Initialized lazily.
        /// </summary>
        private static TargetNumberFormatInfo dataTransfer;

        /// <summary>
        /// Backing field for the static PricingEngineEval property. Initialized lazily.
        /// </summary>
        private static TargetNumberFormatInfo pricingEngineEval;

        /// <summary>
        /// Backing field for the static PointNative property. Initialized lazily.
        /// </summary>
        private static TargetNumberFormatInfo pointNative;

        /// <summary>
        /// Backing field for the static ReportResult property. Initialized lazily.
        /// </summary>
        private static TargetNumberFormatInfo reportResult;

        /// <summary>
        /// Format for counts.
        /// </summary>
        private NumberFormatInfo formatCount;

        /// <summary>
        /// Format for decimals.
        /// </summary>
        private NumberFormatInfo formatDecimal;

        /// <summary>
        /// Format for money.
        /// </summary>
        private NumberFormatInfo formatMoney;

        /// <summary>
        /// Format for money that needs 4 decimal places of precision.
        /// </summary>
        private NumberFormatInfo formatMoney4DecimalDigits;

        /// <summary>
        /// Format for money that needs 6 decimal places of precision.
        /// </summary>
        private NumberFormatInfo formatMoney6DecimalDigits;

        /// <summary>
        /// Format for rates.
        /// </summary>
        private NumberFormatInfo formatRate;

        /// <summary>
        /// Format for rates that need 4 decimal places of precision.
        /// </summary>
        private NumberFormatInfo formatRate4DecimalDigits;

        /// <summary>
        /// Format for rates that need 6 decimal places of precision.
        /// </summary>
        private NumberFormatInfo formatRate6DecimalDigits;

        /// <summary>
        /// Format for rates that need 9 decimal places of precision.
        /// </summary>
        private NumberFormatInfo formatRate9DecimalDigits;

        /// <summary>
        /// Prevents a default instance of the <see cref="TargetNumberFormatInfo"/> class from being created.
        /// Sort of.
        /// </summary>
        private TargetNumberFormatInfo()
        {
            this.formatCount = new NumberFormatInfo();
            this.formatDecimal = new NumberFormatInfo();
            this.formatMoney = new NumberFormatInfo();
            this.formatMoney4DecimalDigits = new NumberFormatInfo();
            this.formatMoney6DecimalDigits = new NumberFormatInfo();
            this.formatRate = new NumberFormatInfo();
            this.formatRate4DecimalDigits = new NumberFormatInfo();
            this.formatRate6DecimalDigits = new NumberFormatInfo();
            this.formatRate9DecimalDigits = new NumberFormatInfo();
        }

        /// <summary>
        /// Gets the TargetNumberFormatInfo instance for WebForm formatting.
        /// </summary>
        /// <value>The TargetNumberFormatInfo instance for WebForm formatting.</value>
        public static TargetNumberFormatInfo WebForm
        {
            get
            {
                if (webForm == null)
                {
                    var temp = new TargetNumberFormatInfo();
                    temp.SetupWebform();
                    temp.LockFormats();
                    webForm = temp;
                }

                return webForm;
            }
        }

        /// <summary>
        /// Gets the TargetNumberFormatInfo instance for Printout formatting.
        /// </summary>
        /// <value>The TargetNumberFormatInfo instance for Printout formatting.</value>
        public static TargetNumberFormatInfo Printout
        {
            get
            {
                if (printout == null)
                {
                    var temp = new TargetNumberFormatInfo();
                    temp.SetupPrintout();
                    temp.LockFormats();
                    printout = temp;
                }

                return printout;
            }
        }

        /// <summary>
        /// Gets the TargetNumberFormatInfo instance for general data transfer formatting.
        /// </summary>
        /// <value>The TargetNumberFormatInfo instance for general data transfer formatting.</value>
        public static TargetNumberFormatInfo DataTransfer
        {
            get
            {
                if (dataTransfer == null)
                {
                    var temp = new TargetNumberFormatInfo();
                    temp.SetupDataTransfer();
                    temp.LockFormats();
                    dataTransfer = temp;
                }

                return dataTransfer;
            }
        }

        /// <summary>
        /// Gets the TargetNumberFormatInfo instance for PricingEngineEval formatting.
        /// </summary>
        /// <value>The TargetNumberFormatInfo instance for PricingEngineEval formatting.</value>
        public static TargetNumberFormatInfo PricingEngineEval
        {
            get
            {
                if (pricingEngineEval == null)
                {
                    var temp = new TargetNumberFormatInfo();
                    temp.SetupPricingEngineEval();
                    temp.LockFormats();
                    pricingEngineEval = temp;
                }

                return pricingEngineEval;
            }
        }

        /// <summary>
        /// Gets the TargetNumberFormatInfo instance for PointNative formatting.
        /// </summary>
        /// <value>The TargetNumberFormatInfo instance for PointNative formatting.</value>
        public static TargetNumberFormatInfo PointNative
        {
            get
            {
                if (pointNative == null)
                {
                    var temp = new TargetNumberFormatInfo();
                    temp.SetupPointNative();
                    temp.LockFormats();
                    pointNative = temp;
                }

                return pointNative;
            }
        }

        /// <summary>
        /// Gets the TargetNumberFormatInfo instance for ReportResult formatting.
        /// </summary>
        /// <value>The TargetNumberFormatInfo instance for ReportResult formatting.</value>
        public static TargetNumberFormatInfo ReportResult
        {
            get
            {
                if (reportResult == null)
                {
                    var temp = new TargetNumberFormatInfo();
                    temp.SetupReportResult();
                    temp.LockFormats();
                    reportResult = temp;
                }

                return reportResult;
            }
        }

        /// <summary>
        /// Gets the NumberFormatInfo instance to use for formatting counts.
        /// </summary>
        /// <value>The NumberFormatInfo instance to use for formatting counts.</value>
        public NumberFormatInfo FormatCount
        {
            get { return this.formatCount; }
        }

        /// <summary>
        /// Gets the NumberFormatInfo instance to use for formatting general decimals.
        /// </summary>
        /// <value>The NumberFormatInfo instance to use for formatting general decimals.</value>
        public NumberFormatInfo FormatDecimal
        {
            get { return this.formatDecimal; }
        }

        /// <summary>
        /// Gets the NumberFormatInfo instance to use for formatting general money amounts.
        /// </summary>
        /// <value>The NumberFormatInfo instance to use for formatting general money amonts.</value>
        public NumberFormatInfo FormatMoney
        {
            get { return this.formatMoney; }
        }

        /// <summary>
        /// Gets the NumberFormatInfo instance to use for formatting money amounts to 4 decimal places.
        /// </summary>
        /// <value>The NumberFormatInfo instance to use for formatting money amounts to 4 decimal places.</value>
        public NumberFormatInfo FormatMoney4DecimalDigits
        {
            get { return this.formatMoney4DecimalDigits; }
        }

        /// <summary>
        /// Gets the NumberFormatInfo instance to use for formatting money amounts to 6 decimal places.
        /// </summary>
        /// <value>The NumberFormatInfo instance to use for formatting money amounts to 6 decimal places.</value>
        public NumberFormatInfo FormatMoney6DecimalDigits
        {
            get { return this.formatMoney6DecimalDigits; }
        }

        /// <summary>
        /// Gets the NumberFormatInfo instance to use for formatting general rates.
        /// </summary>
        /// <value>The NumberFormatInfo instance to use for formatting general rates.</value>
        public NumberFormatInfo FormatRate
        {
            get { return this.formatRate; }
        }

        /// <summary>
        /// Gets the NumberFormatInfo instance to use for formatting rates to 4 decimal places.
        /// </summary>
        /// <value>The NumberFormatInfo instance to use for formatting rates to 4 decimal places.</value>
        public NumberFormatInfo FormatRate4DecimalDigits
        {
            get { return this.formatRate4DecimalDigits; }
        }

        /// <summary>
        /// Gets the NumberFormatInfo instance to use for formatting rates to 6 decimal places.
        /// </summary>
        /// <value>The NumberFormatInfo instance to use for formatting rates to 6 decimal places.</value>
        public NumberFormatInfo FormatRate6DecimalDigits
        {
            get { return this.formatRate6DecimalDigits; }
        }

        /// <summary>
        /// Gets the NumberFormatInfo instance to use for formatting rates to 9 decimal places.
        /// </summary>
        /// <value>The NumberFormatInfo instance to use for formatting rates to 9 decimal places.</value>
        public NumberFormatInfo FormatRate9DecimalDigits
        {
            get { return this.formatRate9DecimalDigits; }
        }

        /// <summary>
        /// Gets the appropriate TargetNumberFormatInfo instance for the given FormatTarget.
        /// </summary>
        /// <param name="formatTarget">The FormatTarget.</param>
        /// <returns>The TargetNumberFormatInfo instance.</returns>
        public static TargetNumberFormatInfo Create(FormatTarget formatTarget)
        {
            switch (formatTarget)
            {
                case FormatTarget.Webform:
                case FormatTarget.LqbDataService:
                    return TargetNumberFormatInfo.WebForm;
                case FormatTarget.PrintoutNormalFields:
                case FormatTarget.PrintoutImportantFields:
                    return TargetNumberFormatInfo.Printout;
                case FormatTarget.FreddieMacAUS:
                case FormatTarget.FannieMaeMornetPlus:
                case FormatTarget.LON_Mismo:
                case FormatTarget.DocMagic:
                case FormatTarget.MismoClosing:
                case FormatTarget.FannieMaeMornetPlusImportantFields:
                case FormatTarget.DocuTech:
                case FormatTarget.GinnieNet:
                case FormatTarget.XsltExport:
                    return TargetNumberFormatInfo.DataTransfer;
                case FormatTarget.PriceEngineEval:
                    return TargetNumberFormatInfo.PricingEngineEval;
                case FormatTarget.PointNative:
                    return TargetNumberFormatInfo.PointNative;
                case FormatTarget.ReportResult:
                    return TargetNumberFormatInfo.ReportResult;
                default:
                    throw new UnhandledEnumException(formatTarget);
            }
        }

        /// <summary>
        /// Prevent further changes to the NumberFormatInfo instances for this object.
        /// </summary>
        private void LockFormats()
        {
            this.formatCount = NumberFormatInfo.ReadOnly(this.formatCount);
            this.formatDecimal = NumberFormatInfo.ReadOnly(this.formatDecimal);
            this.formatMoney = NumberFormatInfo.ReadOnly(this.formatMoney);
            this.formatMoney4DecimalDigits = NumberFormatInfo.ReadOnly(this.formatMoney4DecimalDigits);
            this.formatMoney6DecimalDigits = NumberFormatInfo.ReadOnly(this.formatMoney6DecimalDigits);
            this.formatRate = NumberFormatInfo.ReadOnly(this.formatRate);
            this.formatRate4DecimalDigits = NumberFormatInfo.ReadOnly(this.formatRate4DecimalDigits);
            this.formatRate6DecimalDigits = NumberFormatInfo.ReadOnly(this.formatRate6DecimalDigits);
            this.formatRate9DecimalDigits = NumberFormatInfo.ReadOnly(this.formatRate9DecimalDigits);
        }

        /// <summary>
        /// Set up the NumberFormatInfo instances for Webform formatting.
        /// </summary>
        private void SetupWebform()
        {
            this.formatCount.NumberDecimalDigits = 0;

            this.formatDecimal.NumberDecimalDigits = 2;

            this.formatMoney.CurrencyDecimalDigits = 2;
            this.formatMoney.CurrencyNegativePattern = 0; // Parentheses: ($n)
            this.formatMoney.CurrencySymbol = "$";

            this.formatMoney4DecimalDigits.CurrencyDecimalDigits = 4;
            this.formatMoney4DecimalDigits.CurrencyNegativePattern = 0; // Parentheses: ($n)
            this.formatMoney4DecimalDigits.CurrencySymbol = "$";

            this.formatMoney6DecimalDigits.CurrencyDecimalDigits = 6;
            this.formatMoney6DecimalDigits.CurrencyNegativePattern = 0; // Parentheses: ($n)
            this.formatMoney6DecimalDigits.CurrencySymbol = "$";

            this.formatRate.NumberNegativePattern = 0; // Parentheses: (n)
            this.formatRate.NumberDecimalDigits = 3;

            this.formatRate4DecimalDigits.NumberNegativePattern = 0; // Parentheses: (n)
            this.formatRate4DecimalDigits.NumberDecimalDigits = 4;

            this.formatRate6DecimalDigits.NumberNegativePattern = 0; // Parentheses: (n)
            this.formatRate6DecimalDigits.NumberDecimalDigits = 6;

            this.formatRate9DecimalDigits.NumberNegativePattern = 0; // Parentheses: (n)
            this.formatRate9DecimalDigits.NumberDecimalDigits = 9;
        }

        /// <summary>
        /// Set up the NumberFormatInfo instances for Printout formatting.
        /// </summary>
        private void SetupPrintout()
        {
            this.formatCount.NumberDecimalDigits = 0;

            this.formatDecimal.NumberDecimalDigits = 2;

            this.formatMoney.CurrencyDecimalDigits = 2;
            this.formatMoney.CurrencyNegativePattern = 0; // Parentheses: (n)
            this.formatMoney.CurrencySymbol = string.Empty;

            this.formatMoney4DecimalDigits.CurrencyDecimalDigits = 4;
            this.formatMoney4DecimalDigits.CurrencyNegativePattern = 0; // Parentheses: (n)
            this.formatMoney4DecimalDigits.CurrencySymbol = string.Empty;

            this.formatMoney6DecimalDigits.CurrencyDecimalDigits = 6;
            this.formatMoney6DecimalDigits.CurrencyNegativePattern = 0; // Parentheses: (n)
            this.formatMoney6DecimalDigits.CurrencySymbol = string.Empty;

            this.formatRate.NumberNegativePattern = 0; // Parentheses: (n)
            this.formatRate.NumberDecimalDigits = 3;

            this.formatRate4DecimalDigits.NumberNegativePattern = 0; // Parentheses: (n)
            this.formatRate4DecimalDigits.NumberDecimalDigits = 4;

            this.formatRate6DecimalDigits.NumberNegativePattern = 0; // Parentheses: (n)
            this.formatRate6DecimalDigits.NumberDecimalDigits = 6;

            this.formatRate9DecimalDigits.NumberNegativePattern = 0; // Parentheses: (n)
            this.formatRate9DecimalDigits.NumberDecimalDigits = 9;
        }

        /// <summary>
        /// Set up the NumberFormatInfo instances for DataTransfer formatting.
        /// </summary>
        private void SetupDataTransfer()
        {
            this.formatCount.NumberDecimalDigits = 0;
            this.formatCount.NumberGroupSeparator = string.Empty;

            this.formatDecimal.NumberDecimalDigits = 2;
            this.formatDecimal.NumberGroupSeparator = string.Empty;

            this.formatMoney.CurrencyDecimalDigits = 2;
            this.formatMoney.CurrencyNegativePattern = 2; // Sign: $-n (or it would be if the currency symbol was set...)
            this.formatMoney.CurrencySymbol = string.Empty;
            this.formatMoney.CurrencyGroupSeparator = string.Empty;

            this.formatMoney4DecimalDigits.CurrencyDecimalDigits = 4;
            this.formatMoney4DecimalDigits.CurrencyNegativePattern = 2; // Sign: $-n (or it would be if the currency symbol was set...)
            this.formatMoney4DecimalDigits.CurrencySymbol = string.Empty;
            this.formatMoney4DecimalDigits.CurrencyGroupSeparator = string.Empty;

            this.formatMoney6DecimalDigits.CurrencyDecimalDigits = 6;
            this.formatMoney6DecimalDigits.CurrencyNegativePattern = 2; // Sign: $-n (or it would be if the currency symbol was set...)
            this.formatMoney6DecimalDigits.CurrencySymbol = string.Empty;
            this.formatMoney6DecimalDigits.CurrencyGroupSeparator = string.Empty;

            this.formatRate.NumberNegativePattern = 1; // Sign: -n
            this.formatRate.NumberDecimalDigits = 3;
            this.formatRate.NumberGroupSeparator = string.Empty;

            this.formatRate4DecimalDigits.NumberNegativePattern = 1; // Sign: -n
            this.formatRate4DecimalDigits.NumberDecimalDigits = 4;
            this.formatRate4DecimalDigits.NumberGroupSeparator = string.Empty;

            this.formatRate6DecimalDigits.NumberNegativePattern = 1; // Sign: -n
            this.formatRate6DecimalDigits.NumberDecimalDigits = 6;
            this.formatRate6DecimalDigits.NumberGroupSeparator = string.Empty;

            this.formatRate9DecimalDigits.NumberNegativePattern = 1; // Sign: -n
            this.formatRate9DecimalDigits.NumberDecimalDigits = 9;
            this.formatRate9DecimalDigits.NumberGroupSeparator = string.Empty;
        }

        /// <summary>
        /// Set up the NumberFormatInfo instances for PricingEngineEval formatting.
        /// </summary>
        private void SetupPricingEngineEval()
        {
            this.formatCount.NumberDecimalDigits = 0;
            this.formatCount.NumberGroupSeparator = string.Empty;

            this.formatDecimal.NumberDecimalDigits = 2;
            this.formatDecimal.NumberGroupSeparator = string.Empty;

            this.formatMoney.CurrencyDecimalDigits = 2;
            this.formatMoney.CurrencyNegativePattern = 2; // Sign: $-n (or it would be if the currency symbol was set...)
            this.formatMoney.CurrencySymbol = string.Empty;
            this.formatMoney.CurrencyGroupSeparator = string.Empty;

            this.formatMoney4DecimalDigits.CurrencyDecimalDigits = 4;
            this.formatMoney4DecimalDigits.CurrencyNegativePattern = 2; // Sign: $-n (or it would be if the currency symbol was set...)
            this.formatMoney4DecimalDigits.CurrencySymbol = string.Empty;
            this.formatMoney4DecimalDigits.CurrencyGroupSeparator = string.Empty;

            this.formatMoney6DecimalDigits.CurrencyDecimalDigits = 6;
            this.formatMoney6DecimalDigits.CurrencyNegativePattern = 2; // Sign: $-n (or it would be if the currency symbol was set...)
            this.formatMoney6DecimalDigits.CurrencySymbol = string.Empty;
            this.formatMoney6DecimalDigits.CurrencyGroupSeparator = string.Empty;

            this.formatRate.NumberNegativePattern = 1; // Sign: -n
            this.formatRate.NumberDecimalDigits = 3;
            this.formatRate.NumberGroupSeparator = string.Empty;

            this.formatRate4DecimalDigits.NumberNegativePattern = 1; // Sign: -n
            this.formatRate4DecimalDigits.NumberDecimalDigits = 4;
            this.formatRate4DecimalDigits.NumberGroupSeparator = string.Empty;

            this.formatRate6DecimalDigits.NumberNegativePattern = 1; // Sign: -n
            this.formatRate6DecimalDigits.NumberDecimalDigits = 6;
            this.formatRate6DecimalDigits.NumberGroupSeparator = string.Empty;

            this.formatRate9DecimalDigits.NumberNegativePattern = 1; // Sign: -n
            this.formatRate9DecimalDigits.NumberDecimalDigits = 9;
            this.formatRate9DecimalDigits.NumberGroupSeparator = string.Empty;
        }

        /// <summary>
        /// Set up the NumberFormatInfo instances for PointNative formatting.
        /// </summary>
        private void SetupPointNative()
        {
            this.formatCount.NumberDecimalDigits = 0;
            this.formatCount.NumberGroupSeparator = string.Empty;
            this.formatCount.NumberNegativePattern = 1; // Sign: -n

            this.formatDecimal.NumberDecimalDigits = 2;
            this.formatDecimal.NumberGroupSeparator = string.Empty;
            this.formatDecimal.NumberNegativePattern = 1; // Sign: -n

            this.formatMoney.CurrencyDecimalDigits = 2;
            this.formatMoney.CurrencyGroupSeparator = string.Empty;
            this.formatMoney.CurrencyNegativePattern = 1; // Sign: -$n (or would be if the currency symbol was set...)
            this.formatMoney.CurrencySymbol = string.Empty;

            this.formatMoney4DecimalDigits.CurrencyDecimalDigits = 4;
            this.formatMoney4DecimalDigits.CurrencyGroupSeparator = string.Empty;
            this.formatMoney4DecimalDigits.CurrencyNegativePattern = 1; // Sign: -$n (or would be if the currency symbol was set...)
            this.formatMoney4DecimalDigits.CurrencySymbol = string.Empty;

            this.formatMoney6DecimalDigits.CurrencyDecimalDigits = 6;
            this.formatMoney6DecimalDigits.CurrencyGroupSeparator = string.Empty;
            this.formatMoney6DecimalDigits.CurrencyNegativePattern = 1; // Sign: -$n (or would be if the currency symbol was set...)
            this.formatMoney6DecimalDigits.CurrencySymbol = string.Empty;

            this.formatRate.NumberDecimalDigits = 3;
            this.formatRate.NumberGroupSeparator = string.Empty;
            this.formatRate.NumberNegativePattern = 1; // Sign: -n

            this.formatRate4DecimalDigits.NumberDecimalDigits = 4;
            this.formatRate4DecimalDigits.NumberGroupSeparator = string.Empty;
            this.formatRate4DecimalDigits.NumberNegativePattern = 1; // Sign: -n

            this.formatRate6DecimalDigits.NumberDecimalDigits = 6;
            this.formatRate6DecimalDigits.NumberGroupSeparator = string.Empty;
            this.formatRate6DecimalDigits.NumberNegativePattern = 1; // Sign: -n

            this.formatRate9DecimalDigits.NumberDecimalDigits = 9;
            this.formatRate9DecimalDigits.NumberGroupSeparator = string.Empty;
            this.formatRate9DecimalDigits.NumberNegativePattern = 1; // Sign: -n
        }

        /// <summary>
        /// Set up the NumberFormatInfo instances for ReportResult formatting.
        /// </summary>
        private void SetupReportResult()
        {
            this.formatCount.NumberDecimalDigits = 0;

            this.formatDecimal.NumberDecimalDigits = 2;

            this.formatMoney.CurrencyDecimalDigits = 2;
            this.formatMoney.CurrencyNegativePattern = 1; // Sign: -$n (or would be if the currency symbol was set...)
            this.formatMoney.CurrencySymbol = string.Empty;

            this.formatMoney4DecimalDigits.CurrencyDecimalDigits = 4;
            this.formatMoney4DecimalDigits.CurrencyNegativePattern = 1; // Sign: -$n (or would be if the currency symbol was set...)
            this.formatMoney4DecimalDigits.CurrencySymbol = string.Empty;

            this.formatMoney6DecimalDigits.CurrencyDecimalDigits = 6;
            this.formatMoney6DecimalDigits.CurrencyNegativePattern = 1; // Sign: -$n (or would be if the currency symbol was set...)
            this.formatMoney6DecimalDigits.CurrencySymbol = string.Empty;

            this.formatRate.NumberNegativePattern = 0; // Parentheses: (n)
            this.formatRate.NumberDecimalDigits = 3;

            this.formatRate4DecimalDigits.NumberNegativePattern = 0; // Parentheses: (n)
            this.formatRate4DecimalDigits.NumberDecimalDigits = 4;

            this.formatRate6DecimalDigits.NumberNegativePattern = 0; // Parentheses: (n)
            this.formatRate6DecimalDigits.NumberDecimalDigits = 6;

            this.formatRate9DecimalDigits.NumberNegativePattern = 0; // Parentheses: (n)
            this.formatRate9DecimalDigits.NumberDecimalDigits = 9;
        }
    }
}
