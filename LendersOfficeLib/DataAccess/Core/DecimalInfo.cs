﻿namespace DataAccess
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;

    /// <summary>
    /// Provides a way to determine minimum and maximum values for a given precision and scale.
    /// </summary>
    public static class DecimalInfo
    {
        /// <summary>
        /// A map from precision, scale to the corresponding minimum and maximum values.
        /// </summary>
        /// <remarks>
        /// This dictionary should be hit the majority of the time. It was defined based
        /// on the precision and scale of fields that were used in the loan data layer.
        /// </remarks>
        private static readonly IReadOnlyDictionary<string, Tuple<decimal, decimal>> PredefinedMinMaxValuesForPrecisionAndScale = new Dictionary<string, Tuple<decimal, decimal>>()
        {
            [GetKey(3, 0)] = GenerateMinMaxTuple(3, 0),
            [GetKey(4, 1)] = GenerateMinMaxTuple(4, 1),
            [GetKey(5, 0)] = GenerateMinMaxTuple(5, 0),
            [GetKey(9, 3)] = GenerateMinMaxTuple(9, 3),
            [GetKey(9, 6)] = GenerateMinMaxTuple(9, 6),
            [GetKey(10, 0)] = GenerateMinMaxTuple(10, 0),
            [GetKey(10, 4)] = GenerateMinMaxTuple(10, 4),
            [GetKey(12, 6)] = GenerateMinMaxTuple(12, 6),
            [GetKey(15, 4)] = GenerateMinMaxTuple(15, 4),
            [GetKey(18, 0)] = GenerateMinMaxTuple(18, 0),
            [GetKey(19, 0)] = GenerateMinMaxTuple(19, 0),
            [GetKey(19, 4)] = GenerateMinMaxTuple(19, 4),
            [GetKey(21, 6)] = GenerateMinMaxTuple(21, 6),
        };

        /// <summary>
        /// A map from precision scale to the corresponding minimum and maximum values.
        /// This will be used for precision, scale pairs that are not in the predefined
        /// dictionary.
        /// </summary>
        /// <remarks>
        /// Precision, scale pairs that consistently end up in here should likely be added
        /// to the predefined dictionary to avoid the performance overhead of a concurrent
        /// dictionary.
        /// </remarks>
        private static readonly ConcurrentDictionary<string, Tuple<decimal, decimal>> RuntimeMinMaxValuesForPrecisionAndScale = new ConcurrentDictionary<string, Tuple<decimal, decimal>>();

        /// <summary>
        /// Gets the minimum value for the given precision and scale.
        /// </summary>
        /// <param name="precision">The precision.</param>
        /// <param name="scale">The scale.</param>
        /// <returns>The minimum value.</returns>
        public static decimal MinValue(int precision, int scale)
        {
            return GetMinMaxTuple(precision, scale).Item1;
        }

        /// <summary>
        /// Gets the maximum value for the given precision and scale.
        /// </summary>
        /// <param name="precision">The precision.</param>
        /// <param name="scale">The scale.</param>
        /// <returns>The maximum value.</returns>
        public static decimal MaxValue(int precision, int scale)
        {
            return GetMinMaxTuple(precision, scale).Item2;
        }

        /// <summary>
        /// Gets a value indicating whether a decimal is the minimum or maximum value
        /// that will fit in a field of the given precision and scale.
        /// </summary>
        /// <param name="value">The value to check.</param>
        /// <param name="precision">The precision.</param>
        /// <param name="scale">The scale.</param>
        /// <returns>
        /// True if the field is the minimum or maximum value that would fit in the
        /// given precision and scale.
        /// </returns>
        public static bool IsSentinelValue(decimal value, int precision, int scale)
        {
            var minMax = GetMinMaxTuple(precision, scale);
            return value == minMax.Item1 || value == minMax.Item2;
        }

        /// <summary>
        /// Retrieves a min-max tuple.
        /// </summary>
        /// <param name="precision">The precision.</param>
        /// <param name="scale">The scale.</param>
        /// <returns>The min-max tuple.</returns>
        public static Tuple<decimal, decimal> GetMinMaxTuple(int precision, int scale)
        {
            var key = GetKey(precision, scale);
            Tuple<decimal, decimal> minMaxValue = null;

            if (!PredefinedMinMaxValuesForPrecisionAndScale.TryGetValue(key, out minMaxValue))
            {
                minMaxValue = RuntimeMinMaxValuesForPrecisionAndScale.GetOrAdd(
                    key,
                    entryKey =>
                    {
                        Tools.LogWarning($"Generating min-max tuple for precision ({precision}), scale ({scale}). "
                            + "Consider adding this to the predefined dictionary in DecimalInfo.cs.");

                        return GenerateMinMaxTuple(precision, scale);
                    });
            }

            return minMaxValue;
        }

        /// <summary>
        /// Generates a min-max tuple.
        /// </summary>
        /// <param name="precision">The precision.</param>
        /// <param name="scale">The scale.</param>
        /// <returns>The min-max tuple.</returns>
        private static Tuple<decimal, decimal> GenerateMinMaxTuple(int precision, int scale)
        {
            if (precision < scale)
            {
                throw new ArgumentException($"Precision ({precision}) cannot be less than scale ({scale}).");
            }

            var max = decimal.Parse($"{new string('9', precision - scale)}.{new string('9', scale)}");
            var min = -1 * max;
            return Tuple.Create(min, max);
        }
        
        /// <summary>
        /// Gets the dictionary key.
        /// </summary>
        /// <param name="precision">The precision.</param>
        /// <param name="scale">The scale.</param>
        /// <returns>The key for the dictionary.</returns>
        private static string GetKey(int precision, int scale)
        {
            return precision.ToString() + ":" + scale.ToString();
        }
    }
}
