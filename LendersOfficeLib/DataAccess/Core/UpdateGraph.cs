using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using CommonProjectLib.Common.Lib;
using LendersOffice.Constants;
using LendersOffice.ObjLib.FieldInfoCache.Models;

namespace DataAccess
{
    public enum KindOfDependsOn { LendersOffice, PML };

    /// <summary>
    /// Provide attribute sticky note that we can attach to
    /// a particular calculated field in the loan.  When we
    /// construct our loan base class, we'll create a static
    /// dependency graph and walk the loan type and gather
    /// up all these dependency notes.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Method)]
    public class DependsOnAttribute : Attribute
    {
        private FieldSpecList m_list = new FieldSpecList();

        public IEnumerable<FieldSpec> GetList()
        {
            return m_list.GetList();
        }

        /// <summary>
        /// Construct default.
        /// </summary>
        public DependsOnAttribute() { }

        public DependsOnAttribute(params string[] sList)
            : this((IEnumerable<string>)sList)
        {
        }

        public DependsOnAttribute(IEnumerable<string> sList)
        {
            Initialize(sList);
        }

        private void Initialize(IEnumerable<string> sList)
        {
            foreach (var dependencyString in sList)
            {
                m_list.Add(FieldSpec.Create(dependencyString));
            }
        }
    }

    /// <summary>
    /// Provide attribute sticky note that we can attach to
    /// a particular calculated field in the loan.  When we
    /// construct our loan base class, we'll create a static
    /// dependency graph and walk the loan type and gather
    /// up all these dependency notes.
    /// </summary>

    public class UpdateGraph
    {
        /// <summary>
        /// We maintain two lists in the lookup table for each
        /// aspect of the dependency list (what it depends on
        /// when reading and what is affects when writing).
        /// </summary>

        private FriendlyDictionary<FieldSpec, FieldNode> m_Lookup = new FriendlyDictionary<FieldSpec, FieldNode>();    // depends-on lookup table

        public FieldNode this[FieldSpec fieldSpecName]
        {
            get
            {
                FieldNode node;
                if (m_Lookup.TryGetValue(fieldSpecName, out node) == false)
                {
                    node = null;
                }

                return node;
            }
        }

        public ICollection<FieldSpec> Keys
        {
            get { return m_Lookup.Keys; }
        }

        /// <summary>
        /// Calculate the update result according to our calculated graph.
        /// </summary>
        /// <param name="fieldId">
        /// The ID of the field.
        /// </param>
        /// <param name="useSpecSetOptimization">
        /// True to use optimization for spec set contains check, false otherwise.
        /// </param>
        /// <returns>
        /// The result.
        /// </returns>
        public UpdateResult LookupEntry(string fieldId, bool useSpecSetOptimization)
        {
            UpdateResult result = new UpdateResult();
            FieldSpec fieldSpec = FieldSpec.RetrieveOrNull(fieldId);
            if (fieldSpec != null)
            {
                // Lookup entry and add it to our result.
                FieldNode node = null;
                if (m_Lookup.TryGetValue(fieldSpec, out node))
                {
                    foreach (FieldSpec spec in node.Affecting.GetList())
                    {
                        result.AddUpdateFields(spec);
                    }

                    foreach (FieldSpec spec in node.DependsOn.GetList())
                    {
                        result.AddDbFields(spec);
                    }

                    if (node.DependsOn.Count == 0 || node.DependsOn.Has(node.FieldSpec, useSpecSetOptimization) == true)
                    {
                        result.AddDbFields(node.FieldSpec);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Construct graph using depends-on custom attribute hints
        /// embedded within the class.
        /// </summary>
        /// <param name="dependsOnList">
        /// The list of property/method name - depends on attribute pairs.
        /// </param>
        /// <param name="useSpecSetOptimization">
        /// True to use optimization for spec set contains check, false otherwise.
        /// </param>
        /// <param name="logAction">
        /// Action used to log event timings.
        /// </param>
        private void InitializeGraph_Legacy(
            IEnumerable<KeyValuePair<string, DependsOnAttribute>> dependsOnList,
            bool useSpecSetOptimization,
            Action<string> logAction = null)
        {
            // Initialize our graph.

            // Walk the custom attributes and process the
            // depends-on hints.  We fully calculate the
            // closure for each field.  To keep things sane,
            // we gather the dependency relationships first.

            List<FieldNode> nodes = new List<FieldNode>();

            foreach (var item in dependsOnList)
            {
                DependsOnAttribute dependson = item.Value;
                FieldSpec name = FieldSpec.Create(item.Key);

                // Create a starting node in our lookup pool.
                FieldNode node = null;

                if (m_Lookup.TryGetValue(name, out node) == false)
                {
                    node = new FieldNode(name);
                    m_Lookup.Add(name, node);
                }

                foreach (FieldSpec fieldSpec in dependson.GetList())
                {
                    node.DependsOn.Add(fieldSpec);
                }

                // 2018-11-16 tj - this line should be moved up to the if block that initializes
                // the FieldNode entries, since having it here results in more than one entry
                // in nodes for every field with two DependsOn attributes, e.g. sApr appears in both
                // CPageBase and CPageHelocBase.  Moving it up to there removes this duplication, and
                // prevents the double expansion of these fields.  Unfortunately, my testing revealed
                // that the second expansion resulted in several of these fields receiving additional
                // dependencies.  As such, this line should only be moved with serious caution, and firm
                // grasp of how the dependency graph is changing itself.
                nodes.Add(node);
            }

            logAction?.Invoke("Initialized update graph node list");

            // We build up the dependency set of each calculated and
            // cached field to contain the data-bound fields of our
            // domain.  If the field doesn't depend on anything, then
            // it is a leaf, data-bound field.  We filter out calculated
            // fields, but keep locked fields in the list for each.

            foreach (FieldNode node in nodes)
            {
                int i;

                for (i = 0; i < node.DependsOn.Count; ++i)
                {
                    // Get the next dependency for our current
                    // node and absorb its children.

                    FieldSpec dependency = node.DependsOn.ElementAt(i);

                    if (dependency == null || dependency.Equals(node.FieldSpec))
                    {
                        continue;
                    }

                    // Find the dependency and add it if not currently
                    // part of our set.

                    FieldNode that;

                    if (m_Lookup.TryGetValue(dependency, out that) == false)
                    {
                        throw new Exception("Update graph: Lacked entry for [" + dependency.Name + "].");
                    }

                    // Append the dependencies of this dependency to the end of the dependency list,
                    // which effectively queues them up for later processing since we are recomputing
                    // the count of node.DependsOn each iteration of the loop.
                    node.DependsOn.Add(that.DependsOn);
                }

                while (--i >= 0)
                {
                    // Filter out non-leaf entries.  We lookup the dependency
                    // and remove it if it is calculated.

                    FieldSpec dependency = node.DependsOn.ElementAt(i);

                    if (dependency == null)
                    {
                        node.DependsOn.RemoveAt(i);

                        continue;
                    }

                    // Get the node and scrutinize it for calculated type.

                    FieldNode that = null;

                    if (m_Lookup.TryGetValue(dependency, out that) == false)
                    {
                        that = null;
                    }
                    if (that == null || that.DependsOn.Count == 0)
                    {
                        continue;
                    }

                    if (that.DependsOn.Has(that.FieldSpec, useSpecSetOptimization) == true)
                    {
                        continue;
                    }

                    node.DependsOn.RemoveAt(i);

                }
            }

            logAction?.Invoke("Generated node dependency sets and pruned non-leaf dependencies with optimization " + useSpecSetOptimization);

            // We need to build up the affected sets for all fields that
            // make up the dependency set for some field.  We use the
            // lookup table, which already contains precalculated lists
            // for each leaf field.

            foreach (FieldNode node in m_Lookup.Values)
            {
                foreach (FieldSpec dependency in node.DependsOn.GetList())
                {
                    // Add this node to the dependency's affected set.

                    FieldNode that = null;

                    if (m_Lookup.TryGetValue(dependency, out that) == false)
                    {
                        // This shouldn't happen.

                        continue;
                    }

                    if (that.DependsOn.Count == 0 || that.DependsOn.Has(that.FieldSpec, useSpecSetOptimization) == true)
                    {
                        that.Affecting.Add(node.FieldSpec);
                    }
                }
            }

            logAction?.Invoke("Initialized node affecting sets with optimization " + useSpecSetOptimization);
        }

        public void InitializeGraph_UsingSCC(
            IEnumerable<KeyValuePair<string, DependsOnAttribute>> dependsOnList,
            bool useSpecSetOptimization,
            Action<string> logAction = null)
        {
            var sw = System.Diagnostics.Stopwatch.StartNew();
            var dependencyByField = new FriendlyDictionary<FieldSpec, FieldNode>();    // "directly depends-on" lookup table
            m_Lookup = null;

            // Initialize our graph.

            // Walk the custom attributes and process the
            // depends-on hints.  We fully calculate the
            // closure for each field.  To keep things sane,
            // we gather the dependency relationships first.

            int maxId = -1;
            foreach (var item in dependsOnList)
            {
                DependsOnAttribute dependson = item.Value;
                FieldSpec name = FieldSpec.Create(item.Key);

                // Create a starting node in our lookup pool.
                FieldNode node = null;

                // 2018-11-16 tj pointed out some entry appears in multiple classes. For example sApr appears in both CPageBase and CPageHelocBase.  
                if (dependencyByField.TryGetValue(name, out node) == false)
                {
                    node = new FieldNode(name);
                    dependencyByField.Add(name, node);
                    maxId = Math.Max(maxId, node.FieldSpec.internalId);
                }
 
                foreach (FieldSpec fieldSpec in dependson.GetList())
                {
                    node.DependsOn.Add(fieldSpec);
                }
            }

            int numVertices = maxId + 1;
            FieldNode[] fields = new FieldNode[numVertices];
            foreach (FieldNode node in dependencyByField.Values)
            {
                fields[node.FieldSpec.internalId] = node;
            }

            HashSet<FieldSpec> calculatedFieldSet = new HashSet<FieldSpec>();
            foreach (FieldNode node in fields)
            {
                if ( node == null || node.DependsOn.Count == 0)
                {
                    continue;
                }

                if (!node.DependsOn.Has(node.FieldSpec, useSpecSetOptimization))
                {
                    calculatedFieldSet.Add(node.FieldSpec);
                }

                //// validate node.DependsOn
                for (int i = 0; i < node.DependsOn.Count; ++i)
                {
                    FieldSpec dependency = node.DependsOn.ElementAt(i);
                    if (dependency == null)
                    {
                        throw new Exception($"Update graph: the entry [{node.FieldSpec.Name} contains invalid dependency information.");
                    }

                    if ( fields[dependency.internalId] == null)
                    {
                        throw new Exception("Update graph: Lacked entry for [" + dependency.Name + "].");
                    }
                }
            }

            logAction?.Invoke("Initialized update graph node list");

            // Tarjan's strongly connected components (SCC) algorithm
            // https://en.wikipedia.org/wiki/Tarjan%27s_strongly_connected_components_algorithm


            List<List<int>> components = FindStronglyConnectedComponents(fields);
            logAction?.Invoke($"#Strong components: {components.Count}.");

            // Now start to remove "circular dependencies". 
            // With each particular SCC, select 1st member as represent and its other members as leaves
            //
            // Step 1: with each SCC, create its represent

            var id2comp = new Dictionary<int, int>();
            var repNodes = new List<FieldNode>();

            for (int c=0; c < components.Count; c++)
            {
                var component = components[c];
                repNodes.Add(new FieldNode(fields[component[0]].FieldSpec));

                foreach (int w in component)
                {
                    id2comp.Add(w, c);
                }
            }


            // Notation: uppercase is member of some SCC

            // Step 2: each SCC gets union of their directly dependencies.
            //
            // Assume SCC group A contains 4 members  A1, A2, A3, A4. 
            //    A's represent = A1
            //
            //    Moreover assume their union directly dependencies U = A1, A2, A3, A4, B1, B2, C4, e1, e2, e3
            //    foreach element in U
            //        if "element" belongs different SCC      
            //              replace "element" by their represents
            //
            //     => U = A1, A2, A3, A4, represent(B1), represent(C4), e1, e2, e3
            
            for (int c=0; c < components.Count; c++)
            {
                var repNode = repNodes[c];

                foreach (var id in components[c])
                {
                    var node = fields[id];
                    for (int i = 0; i < node.DependsOn.Count; ++i)
                    {
                        FieldSpec dependency = node.DependsOn.ElementAt(i);

                        int component;
                        if ( id2comp.TryGetValue(dependency.internalId, out component) && component != c)
                        {
                            // dependency belongs other SCC => add other SCC'represent
                            repNode.DependsOn.Add(repNodes[component].FieldSpec);
                        }
                        else
                        {
                            repNode.DependsOn.Add(dependency);
                        }
                    }
                }                
            }

            // Step 3: update SCC members to lookup
            //      i. Scc's represent -> step 2's union dependencies 
            //     ii. Scc's other members become leaves (ie dependen on nothing)
            Dictionary<FieldSpec, FieldNode> internalLookup = dependencyByField;
            for (int c = 0; c < components.Count; c++)
            {
                var repNode = repNodes[c];
                bool isFirst = true;

                foreach (var id in components[c])
                {
                    if(isFirst)
                    {
                        // Scc's represent -> repNode
                        internalLookup[fields[id].FieldSpec] = repNode;
                        isFirst = false;
                    }
                    else
                    {
                        // other non-calculated members become leaves (ie dependen on nothing)
                        internalLookup[fields[id].FieldSpec] = new FieldNode(fields[id].FieldSpec);
                    }
                }
            }

            // Step 4: process fields those don't belong any SCC.
            //        If dependency belongs some SCC, LOGICALLY replace" it by SCC's represent
            foreach( var node in dependencyByField.Values)
            {
                int component;

                if (id2comp.TryGetValue(node.FieldSpec.internalId, out component))
                {
                    // previous step already processed SCC members
                    continue;
                }

                for (int i = 0; i < node.DependsOn.Count; ++i)
                {
                    FieldSpec dependency = node.DependsOn.ElementAt(i);

                    if (id2comp.TryGetValue(dependency.internalId, out component))
                    {
                        node.DependsOn.Add(repNodes[component].FieldSpec);
                    }
                }
            }

            // Step 5: using DFS (depth first seach) to find all directly/indirectly dependencies for each field.
            FriendlyDictionary<FieldSpec, FieldNode> allDependLookup = GetAllDependencies(dependencyByField, calculatedFieldSet);

            // Step 6: With each scc member, set its dependencies same as scc's represent 
            for (int c = 0; c < components.Count; c++)
            {
                var repNode = repNodes[c];

                var component = components[c];
                for (int i=1; i < component.Count;  i++)
                {
                    var fieldSpec = fields[component[i]].FieldSpec;

                    FieldNode node = allDependLookup[fieldSpec];
                    Tools.Assert(node.DependsOn.Count == 0, "Error: expect DependsOn.Count == 0 for SCC's normal member");
                    node.DependsOn.Add(repNode.DependsOn);
                }
            }

            logAction?.Invoke("Generated node dependency sets and pruned non-leaf dependencies with optimization " + useSpecSetOptimization);

            // We need to build up the affected sets for all fields that
            // make up the dependency set for some field.  We use the
            // lookup table, which already contains precalculated lists
            // for each leaf field.

            foreach (FieldNode node in allDependLookup.Values)
            {
                foreach (FieldSpec dependency in node.DependsOn.GetList())
                {
                    // Add this node to the dependency's affected set.

                    FieldNode that = null;

                    if (allDependLookup.TryGetValue(dependency, out that) == false)
                    {
                        // This shouldn't happen.

                        continue;
                    }

                    if (!calculatedFieldSet.Contains(that.FieldSpec))
                    {
                        that.Affecting.Add(node.FieldSpec);
                    }
                }
            }

            m_Lookup = allDependLookup;
            logAction?.Invoke("Initialized node affecting sets with optimization " + useSpecSetOptimization + $"\r\nInitializeGraph()'s duration = {sw.ElapsedMilliseconds} ms ");
        }

        public void InitializeGraph(
            IEnumerable<KeyValuePair<string, DependsOnAttribute>> dependsOnList,
            bool useSpecSetOptimization,
            Action<string> logAction = null)
        {
            if (ConstStage.UseUpdateGraphLegacy)
            {
                InitializeGraph_Legacy(dependsOnList, useSpecSetOptimization, logAction);
            }
            else
            {
                InitializeGraph_UsingSCC(dependsOnList, useSpecSetOptimization, logAction);
            }
        }

        public UpdateGraph()
        {
        }

        internal string Dump()
        {
            using (var stream = new StringWriter())
            {
                stream.Write($"#Fields {m_Lookup.Count}" + Environment.NewLine + Environment.NewLine);
                foreach (FieldNode node in m_Lookup.Values.OrderBy(i => i.FieldSpec.Name))
                {
                    stream.Write(node.FieldSpec.Name + $": ({node.DependsOn.Count}) " + string.Join(", ", node.DependsOn.GetList().Select(d => d.Name).OrderBy(n => n)) + Environment.NewLine);
                    stream.Write($"    Affecting: ({node.Affecting.Count}) " + string.Join(", ", node.Affecting.GetList().Select(d => d.Name).OrderBy(n => n)) + Environment.NewLine);
                    stream.Write(Environment.NewLine);
                }

                return stream.ToString();
            }
        }

        private static List<List<int>> FindStronglyConnectedComponents(FieldNode[] fields)
        {
            // Tarjan's strongly connected components (SCC) algorithm
            // https://en.wikipedia.org/wiki/Tarjan%27s_strongly_connected_components_algorithm
            // https://www.geeksforgeeks.org/tarjan-algorithm-find-strongly-connected-components/

            int numVertices = fields.Length; // num vertex
            int time = 0;
            int[] disc = new int[numVertices]; // discovery times of visited vertices
            int[] low = new int[numVertices]; // earliest visited vertex (the vertex with minimum discovery time) that can be reached from subtree 
                                    // rooted with current vertex 
            Stack<int> stack = new Stack<int>();
            HashSet<int> stackMember = new HashSet<int>();

            List<List<int>> components = new List<List<int>>();


            Action<int> strongConnect = null;
            strongConnect = (int v /* vertex v */) =>
            {
                // Initialize discovery time and low value
                disc[v] = low[v] = ++time;
                var node = fields[v];

                if (node.IsLeaf())
                {
                    return;
                }

                stack.Push(v);
                stackMember.Add(v);

                // Go through all vertices adjacent to this 
                for (int i = 0; i < node.DependsOn.Count; ++i)
                {
                    FieldSpec dependency = node.DependsOn.ElementAt(i);
                    if (dependency == null || dependency.Equals(node.FieldSpec))
                    {
                        continue;
                    }

                    int w = dependency.internalId;  // w is current adjacent of 'v' 

                    if (disc[w] == 0)
                    {
                        // Successor w has not yet been visited; recurse on it
                        strongConnect(w);

                        // Check if the subtree rooted with 'v' has a 
                        // connection to one of the ancestors of 'u' 
                        // Case 1 (per above discussion on Disc and Low value) 
                        low[v] = Math.Min(low[v], low[w]);
                    }

                    // Update low value of 'u' only of 'v' is still in stack 
                    // (i.e. it's a back edge, not cross edge). 
                    // Case 2 (per above discussion on Disc and Low value) 
                    else if (stackMember.Contains(w))
                    {
                        low[v] = Math.Min(low[v], disc[w]);
                    }
                }

                // If v is a root node, pop the stack and generate an SCC
                if (disc[v] == low[v])
                {

                    // start a new strongly connected component
                    var component = new List<int>();
                    while (true)
                    {
                        int w = stack.Pop();
                        stackMember.Remove(w);
                        component.Add(w);

                        if (w == v)
                        {
                            break;
                        }
                    }

                    if (component.Count > 1)
                    {
                        component.Reverse();
                        components.Add(component);
                    }
                }

            };

            // Call the recursive helper function to find strongly connected components in DFS tree with vertex 'v' 
            for (int v = 0; v < numVertices; v++)
            {
                if (disc[v] == 0 && fields[v] != null)
                    strongConnect(v);
            }

            return components;
        }

        // Get all dependencies including directly/indirectly dependencies.
        // Input require: no circular dependency.
        private static FriendlyDictionary<FieldSpec, FieldNode> GetAllDependencies(FriendlyDictionary<FieldSpec, FieldNode> dependencyByField, HashSet<FieldSpec> calculatedFieldSet)
        {
            FriendlyDictionary<FieldSpec, FieldNode> allDependLookup = new FriendlyDictionary<FieldSpec, FieldNode>();
            HashSet<int> stackMember = new HashSet<int>();

            foreach (var key in dependencyByField.Keys)
            {
                if (!allDependLookup.ContainsKey(key))
                {
                    GetAllDependencyHelper(key, dependencyByField, calculatedFieldSet, allDependLookup, stackMember);
                }
            }

            return allDependLookup;
        }

        private static SpecSet GetAllDependencyHelper(FieldSpec key, FriendlyDictionary<FieldSpec, FieldNode> dependencyByField, HashSet<FieldSpec> calculatedFieldSet, FriendlyDictionary<FieldSpec, FieldNode> allDependLookup, HashSet<int> stackMember)
        {
            FieldNode result;
            if (allDependLookup.TryGetValue(key, out result))
            {
                return result.DependsOn;
            }

            if (stackMember.Contains(key.internalId))
            {
                throw new Exception("Dectect circurlar dependencies.");
            }

            stackMember.Add(key.internalId);

            result = new FieldNode(key);
            var node = dependencyByField[key];

            for (int i = 0; i < node.DependsOn.Count; ++i)
            {
                FieldSpec dependency = node.DependsOn.ElementAt(i);
                if (dependency.internalId == key.internalId)
                {
                    continue;
                }

                node.DependsOn.Add(GetAllDependencyHelper(dependency, dependencyByField, calculatedFieldSet, allDependLookup, stackMember));
            }

            for (int i = node.DependsOn.Count - 1; i >= 0; i--)
            {
                FieldSpec dependency = node.DependsOn.ElementAt(i);
                if (calculatedFieldSet.Contains(dependency))
                {
                    node.DependsOn.RemoveAt(i);
                    continue;
                }
            }

            stackMember.Remove(key.internalId);
            allDependLookup.Add(key, node);

            return node.DependsOn;
        }
    }

    /// <summary>
    /// Track fields according to their name and class.
    /// MICRO-PERFORMANCE: I made the FieldSpec become immutable class and each class will have a unique internal id that will be use to speed up Equals operation.
    /// </summary>
    public sealed class FieldSpec
    {
        /// <summary>
        /// Specify the maximum number of FieldSpec item can exist in the system. 
        /// Since I used this value to construct BitArray, therefore for memory usage consideration keep this number as small as possible.
        /// </summary>
        public const int MaxFieldId = 16000;

        private static int StaticInteralUseOnlyCounter = 0;

        // Implement Object Pool concept. There will be only 1 FieldSpec class for each name (i.e: sLNm, slnm will result to one class in memory.)
        private static ConcurrentDictionary<string, FieldSpec> s_staticDictionary = new ConcurrentDictionary<string, FieldSpec>(StringComparer.OrdinalIgnoreCase);

        public static FieldSpec Create(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name cannot be null or empty.");
            }
            // 9/24/2015 - dd - GetOrAdd will return existing object from dictionary, if not found then create new object.
            FieldSpec spec = s_staticDictionary.GetOrAdd(name, o => new FieldSpec(o));

            return spec;
        }

        /// <summary>
        /// Retrieve the <see cref="FieldSpec"/> for the specified name.  This is intended for code after the dependency graph generation
        /// so caller input does not add newly defined <see cref="FieldSpec"/> instances.
        /// </summary>
        /// <param name="name">The name of the field.</param>
        /// <returns>The <see cref="FieldSpec"/> identified by <paramref name="name"/>, or null if no match is found.</returns>
        public static FieldSpec RetrieveOrNull(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }

            FieldSpec value;
            s_staticDictionary.TryGetValue(name, out value);
            return value;
        }

        /// <summary>
        /// MICRO-PERFORMANCE: This internalId will be use to make a faster Equals operation.
        /// Use internal id as an optimize way compare if two FieldSpec objects are the same.
        /// Why do I use this instead of compare the Name? Because string.GetHashCode() is slower than int.GetHashCode(), and we are calling this A LOT.
        /// </summary>
        internal readonly int internalId;

        public string Name { get; private set; }

        /// <summary>
        /// Construct spec from field.
        /// </summary>
        /// <param name="sField">
        /// Field name from list.
        /// </param>

        private FieldSpec(string sField )
        {
            this.internalId = Interlocked.Increment(ref StaticInteralUseOnlyCounter);
            
            if (this.internalId > MaxFieldId)
            {
                throw CBaseException.GenericException("Exceed MaxFieldId=" + MaxFieldId + ". Increase the hard code value.");
            }

            Name = sField;
        }

        public override int GetHashCode()
        {
            return this.internalId;
        }

        public override bool Equals(object obj)
        {
            if (object.ReferenceEquals(this, obj))
            {
                return true;
            }

            FieldSpec o = obj as FieldSpec;
            if (o == null)
            {
                return false;
            }

            return this.internalId == o.internalId;
        }
    }

    public class SpecSet
    {
        private BitArray m_bitArray = new BitArray(FieldSpec.MaxFieldId);
        private List<FieldSpec> m_list = new List<FieldSpec>();

        public IEnumerable<FieldSpec> GetList()
        {
            return m_list;
        }

        public int Count
        {
            get { return m_list.Count; }
        }

        public FieldSpec ElementAt(int index)
        {
            return m_list[index];
        }

        public void RemoveAt(int index)
        {
            var specToRemove = this.ElementAt(index);

            this.m_list.RemoveAt(index);
            this.m_bitArray.Set(specToRemove.internalId, false);
        }

        /// <summary>
        /// Look for matching entry.
        /// </summary>
        /// <param name="item">
        /// The item to search for.
        /// </param>
        /// <param name="useOptimization">
        /// True to use the bit array check optimization,
        /// false otherwise.
        /// </param>
        /// <returns>
        /// True if found.
        /// </returns>
        public bool Has(FieldSpec item, bool useOptimization)
        {
            if (item == null)
            {
                return false;
            }

            if (useOptimization)
            {
                return this.m_bitArray.Get(item.internalId);
            }
            else
            {
                foreach (FieldSpec spec in this.m_list)
                {
                    if (spec.Equals(item))
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        public void Add(SpecSet item)
        {
            if (item == null)
            {
                return;
            }

            int count = item.m_list.Count;
            List<FieldSpec> tempList = item.m_list;

            // MICRO-PERFORMANCE: This for block is extremely time consuming. 40% of CFieldInfoTable.InitializeInfos occur in this block.
            // As of 9/24/2015 the m_set.Contains get call 149,605,487 times and only 1,398,897 of those calls return false.
            // Therefore any strange performance optimization in m_set.Contains will help.
            for (int i = 0; i < count; i++)
            {
                FieldSpec link = tempList[i];
                // Add the dependency's child because we depend on what
                // our dependencies depend upon.

                if (m_bitArray.Get(link.internalId) == false)
                {
                    m_bitArray.Set(link.internalId, true);
                    this.m_list.Add(link);
                }
            }

        }

        /// <summary>
        /// Append unique spec to this set.
        /// </summary>
        /// <param name="sField">
        /// Spec to add to this set.
        /// </param>
        public void Add(FieldSpec sField)
        {
            if (m_bitArray.Get(sField.internalId) == false)
            {
                m_bitArray.Set(sField.internalId, true);
                m_list.Add(sField);
            }
        }
    }

    /// <summary>
    /// Hold the depends-on details during gathering.
    /// </summary>
    public class FieldNode
    {
        /// <summary>
        /// Details contain the dependency information for
        /// the current field.
        /// </summary>

        public readonly SpecSet DependsOn = new SpecSet(); // depends-on list for reads
        public readonly SpecSet Affecting = new SpecSet(); // affecting list for writes

        public FieldSpec FieldSpec { get; }

        /// <summary>
        /// Construct node using spec.
        /// </summary>
        /// <param name="fieldSpec">
        /// Field spec from list.
        /// </param>
        public FieldNode(FieldSpec fieldSpec)
        {
            this.FieldSpec = fieldSpec;
        }

        public bool IsLeaf()
        {
            return this.DependsOn.Count == 0 ||
                   (this.DependsOn.Count == 1 && this.FieldSpec.Equals(this.DependsOn.ElementAt(0)));
        }

    }

    /// <summary>
    /// Maintain set of fields with corresponding scopes
    /// as we discovered them in our dependency graph.
    /// </summary>
    public class DependResult
    {
        public DependResult()
        {
        }

        public DependResult(ResultModel model)
        {
            this.m_DbFields = new StringList(model.DbFields);
        }

        /// <summary>
        /// Maintain set of fields with corresponding scopes
        /// as we discovered them in our dependency graph.
        /// </summary>
        private StringList m_DbFields = new StringList();  // working set of database fields

        public StringList DbFields
        {
            get { return m_DbFields; }
        }

        public void AddDbFields( FieldSpec item )
        {
            m_DbFields.Add( item );
        }

        public void AddDbFields(StringList items)
        {
            this.m_DbFields.AddRange(items);
        }
    }

    /// <summary>
    /// Maintain set of fields with corresponding scopes
    /// as we discovered them in our dependency graph.  We
    /// provide the affected field set too.
    /// </summary>

    public class UpdateResult : DependResult
    {
        public UpdateResult()
        {
        }

        public UpdateResult(ResultModel model) 
            : base(model)
        {
            this.m_UpdateFields = new StringList(model.UpdateFields);
        }

        /// <summary>
        /// Maintain set of fields with corresponding scopes
        /// as we discovered them in our dependency graph.
        /// We provide the affected field set too.
        /// </summary>

        private StringList m_UpdateFields = new StringList(); // list of affected fields

        public StringList UpdateFields
        {
            get { return m_UpdateFields; }
        }

        public void AddUpdateFields(FieldSpec item)
        {
            m_UpdateFields.Add(item);
        }
    }

    /// <summary>
    /// Represent a list of unique FieldSpec class.
    /// </summary>
    internal class FieldSpecList
    {
        private BitArray m_bitArray = new BitArray(FieldSpec.MaxFieldId);
        private List<FieldSpec> m_list = new List<FieldSpec>();

        internal IEnumerable<FieldSpec> GetList()
        {
            return m_list;
        }

        public void Add(FieldSpec item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("Cannot insert null");
            }

            if (this.m_bitArray.Get(item.internalId) == false)
            {
                m_bitArray.Set(item.internalId, true);
                m_list.Add(item);
            }
        }
    }
}