﻿namespace DataAccess
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;

    /// <summary>
    /// Class to manage preparer records for a CPageBase instance.
    /// </summary>
    public class PreparerCollectionBase
    {
        /// <summary>
        /// The <see cref="CPageBase"/> instance that will be passed into <see cref="CPreparerFields"/>
        /// instances on creation.
        /// </summary>
        private CPageBase pageBase;

        /// <summary>
        /// The data set used to hold the preparer records.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Preserving legacy naming.")]
        private DataSet sPreparerDataSet;

        /// <summary>
        /// Initializes a new instance of the <see cref="PreparerCollectionBase"/> class.
        /// </summary>
        /// <param name="pageBase">The <see cref="CPageBase"/> instance to pass to <see cref="CPreparerFields"/> instances when constructing.</param>
        /// <param name="preparerDataSet">The <see cref="DataSet"/> that will hold the data preparer for all preparers in the collection.</param>
        /// <param name="getAgentOfRole">The function used to get an agent of a particular role.</param>
        /// <param name="logError">The method to call when logging errors.</param>
        public PreparerCollectionBase(CPageBase pageBase, DataSet preparerDataSet, Func<E_AgentRoleT, E_ReturnOptionIfNotExist, CAgentFields> getAgentOfRole, Action<string> logError)
        {
            this.pageBase = pageBase;
            this.sPreparerDataSet = preparerDataSet;
            this.GetAgentOfRole = getAgentOfRole;
            this.LogError = logError;
        }

        /// <summary>
        /// Gets the number of preparers in the collection.
        /// </summary>
        /// <value>The number of preparers in the collection.</value>
        public int Count
        {
            get
            {
                DataTable table = this.sPreparerDataSet.Tables["PreparerXmlContent"];
                return table.Rows.Count;
            }
        }

        /// <summary>
        /// Gets all the preparer form types in the preparer collection.
        /// </summary>
        /// <value>All the preparer form types in the preparer collection.</value>
        public IEnumerable<E_PreparerFormT> Keys
        {
            get
            {
                foreach (var preparer in this.Values)
                {
                    yield return preparer.PreparerFormT;
                }
            }
        }

        /// <summary>
        /// Gets all the preparers in the preparer collection.
        /// </summary>
        /// <value>All the preparers in the preparer collection.</value>
        public IEnumerable<IPreparerFields> Values
        {
            get
            {
                int preparerCount = this.Count;
                for (int i = 0; i < preparerCount; ++i)
                {
                    yield return this.GetPreparerFields(i);
                }
            }
        }

        /// <summary>
        /// Gets the function that will be called to get an agent record of a particular role.
        /// </summary>
        /// <remarks>
        /// This is expected to be passed in from the <see cref="CPageBase"/> object that
        /// this PreparerCollectionBase instance is linked to.
        /// </remarks>
        /// <value>The function that will be called to get an agent record of a particular role.</value>
        private Func<E_AgentRoleT, E_ReturnOptionIfNotExist, CAgentFields> GetAgentOfRole { get; }

        /// <summary>
        /// Gets the method that will be called when logging errors.
        /// </summary>
        /// <value>The method that will be called when logging errors.</value>
        private Action<string> LogError { get; }

        /// <summary>
        /// Gets sets the indicated preparer.
        /// </summary>
        /// <param name="form">The preparer form to get.</param>
        /// <returns>The preparer of the indicated form.</returns>
        public IPreparerFields this[E_PreparerFormT form]
        {
            get
            {
                IPreparerFields preparer = this.GetPreparerOfForm(form, null);

                if (preparer != null)
                {
                    return preparer;
                }
                else
                {
                    throw new KeyNotFoundException($"Preparer for form '{form}' not found.");
                }
            }
        }

        /// <summary>
        /// From the preparer form, determines what agent to associate with the preparer
        /// and whether or not to copy the data from the agent record into the preparer.
        /// </summary>
        /// <param name="form">The preparer form.</param>
        /// <param name="sourceAgentRoleT">Outputs the agent role to associate with a new preparer of the given form.</param>
        /// <param name="isCopyFromOfficialContact">Outputs whether or not to copy the data from the associated agent record when creating a new preparer of the given form.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("LendingQBStyleCop.LendingQBCustomRules", "LB1001:DefaultSwitchCaseMustThrowException", Justification = "Copied legacy code out of whitelisted class.")]
        public static void GetDefaultAgentCopyInfoForPreparerFormT(E_PreparerFormT form, out E_AgentRoleT sourceAgentRoleT, out bool isCopyFromOfficialContact)
        {
            switch (form)
            {
                case E_PreparerFormT.BorrCertification:
                case E_PreparerFormT.BorrSignatureAuthorization:
                case E_PreparerFormT.CACompoundStatement:
                case E_PreparerFormT.ECOA:
                case E_PreparerFormT.GeorgiaDisclosure:
                case E_PreparerFormT.Gfe:
                case E_PreparerFormT.LoanSubmission:
                case E_PreparerFormT.MortgageLoanOrigAgreement:
                case E_PreparerFormT.PrivacyPolicyDisclosure:
                case E_PreparerFormT.RealEstateDisclosure:
                case E_PreparerFormT.App1003Interviewer:
                case E_PreparerFormT.FloridaMortBrokerBusinessContract:
                case E_PreparerFormT.AppraisalDisclosure:
                case E_PreparerFormT.FHAAddendumLender:
                case E_PreparerFormT.TXMortgageBrokerDisclosure:
                case E_PreparerFormT.LockInConfirmation:
                case E_PreparerFormT.MortgageLoanCommitment:
                case E_PreparerFormT.ServicingDisclosure:
                case E_PreparerFormT.CreditDisclosureLender:
                case E_PreparerFormT.FloodHazardNotice:
                case E_PreparerFormT.PatriotActDisclosure:
                    sourceAgentRoleT = E_AgentRoleT.LoanOfficer;
                    isCopyFromOfficialContact = true;
                    break;
                case E_PreparerFormT.FloodHazardDeterminationPreparer: // db - OPM 33365
                    sourceAgentRoleT = E_AgentRoleT.FloodProvider;
                    isCopyFromOfficialContact = true;
                    break;
                case E_PreparerFormT.VerificationOfDeposit:
                case E_PreparerFormT.VerificationOfEmployment:
                case E_PreparerFormT.VerificationOfLandContract:
                case E_PreparerFormT.VerificationOfLoan:
                case E_PreparerFormT.VerificationOfMortgage:
                case E_PreparerFormT.VerificationOfRent:
                case E_PreparerFormT.RequestOfAppraisal:
                case E_PreparerFormT.RequestOfInsurance:
                case E_PreparerFormT.RequestOfTitle:
                    sourceAgentRoleT = E_AgentRoleT.Processor;
                    isCopyFromOfficialContact = true;
                    break;
                case E_PreparerFormT.Til:
                case E_PreparerFormT.FHAAppraisedValueDisclosure:
                case E_PreparerFormT.FHAAddendumSponsor: // lender that loans get submitted to.
                case E_PreparerFormT.VA26_1820Lender:
                case E_PreparerFormT.VA26_8937Lender:
                case E_PreparerFormT.UsdaConditionalCommitment:
                case E_PreparerFormT.FHACondCommitMortgagee:
                case E_PreparerFormT.CreditDenialStatement:
                    sourceAgentRoleT = E_AgentRoleT.Lender;
                    isCopyFromOfficialContact = true;
                    break;
                case E_PreparerFormT.FHAInsEndorsementUnderwriter:
                case E_PreparerFormT.FHAConditionalCommitment:
                case E_PreparerFormT.FHAAnalysisAppraisalUnderwriter:
                case E_PreparerFormT.FHACreditAnalysisPurchaseUnderwriter:
                case E_PreparerFormT.FHACreditAnalysisRefinanceUnderwriter:
                case E_PreparerFormT.FHA203kWorksheetUnderwriter:
                case E_PreparerFormT.FHA92900LtUnderwriter:
                    sourceAgentRoleT = E_AgentRoleT.Underwriter;
                    isCopyFromOfficialContact = true;
                    break;
                case E_PreparerFormT.FHAInsEndorsementAppraiser:
                case E_PreparerFormT.FHAAnalysisAppraisalAppraiser:
                    sourceAgentRoleT = E_AgentRoleT.Appraiser;
                    isCopyFromOfficialContact = true;
                    break;
                case E_PreparerFormT.PaymentStatementServicer:
                    sourceAgentRoleT = E_AgentRoleT.Servicing;
                    isCopyFromOfficialContact = true;
                    break;
                default:
                    sourceAgentRoleT = E_AgentRoleT.LoanOfficer; // to make compiler happy.
                    isCopyFromOfficialContact = false;
                    break;
            }
        }

        /// <summary>
        /// Determines if the collection contains the selected preparer form.
        /// </summary>
        /// <param name="form">The form to check.</param>
        /// <returns>True if the collection contains the preparer form, false otherwise.</returns>
        public bool ContainsKey(E_PreparerFormT form)
        {
            return this.GetPreparerOfForm(form, null) != null;
        }

        /// <summary>
        /// Adds a preparer to the collection.
        /// </summary>
        /// <param name="preparer">The preparer to add.</param>
        /// <returns>The form of the preparer added.</returns>
        public E_PreparerFormT Add(IPreparerFields preparer)
        {
            E_PreparerFormT form = preparer.PreparerFormT;

            if (this.ContainsKey(form))
            {
                throw new ArgumentException($"Key {form} already exists.");
            }

            this.AddCopyOfPreparer(preparer, this.GetPreparerOfForm);

            return form;
        }

        /// <summary>
        /// Copies a preparer from another collection to this one, stomping over the
        /// corresponding preparer (if any) in this collection.
        /// </summary>
        /// <param name="source">The source preparer.</param>
        /// <param name="getPreparerOfForm">The function to use to get the preparer (mostly to check workflow).</param>
        public void AddCopyOfPreparer(IPreparerFields source, Func<E_PreparerFormT, E_ReturnOptionIfNotExist, IPreparerFields> getPreparerOfForm)
        {
            if (!source.IsValid)
            {
                return;
            }

            // Will need to divert to another implementation once we start shimming...
            CPreparerFields dest = (CPreparerFields)getPreparerOfForm(source.PreparerFormT, E_ReturnOptionIfNotExist.CreateNew);
            dest.CopyInfoFrom(source);
            dest.Update();
        }

        /// <summary>
        /// Returns the preparer of the given form from the collection, or the default value parameter if
        /// the form is not found.
        /// </summary>
        /// <param name="form">The preparer form to find.</param>
        /// <param name="defaultValue">The value to return if the form is not found.</param>
        /// <param name="changeNotifier">The method to call (if any) when the preparer instance is updated.</param>
        /// <returns>The preparer from the collection for the given form, or the default value parameter.</returns>
        public IPreparerFields GetPreparerOfForm(E_PreparerFormT form, IPreparerFields defaultValue = null, Action<IPreparerFields> changeNotifier = null)
        {
            IPreparerFields preparer;
            int preparerCount = this.Count;
            for (int i = 0; i < preparerCount; ++i)
            {
                preparer = this.GetPreparerFields(i, changeNotifier);

                if (form == preparer.PreparerFormT)
                {
                    // OPM 29731 - removing: if (!preparer.IsEmpty)
                    // If the preparer field exists, but it's blank, this means that the user cleared out all the fields
                    // explicitly, so we will just return the blank object instead of falling back to the data
                    // that currently exists in the Agent record (since if they cleared the data explicitly, they
                    // are intentionally clearing the field and do not want the Agent data populating there).
                    return preparer;
                }
            }

            return defaultValue;
        }

        /// <summary>
        /// Gets the preparer for the given form, or a new preparer based on the agent whose
        /// role is passed in.
        /// </summary>
        /// <param name="form">The form of preparer to get or create.</param>
        /// <param name="initializingAgentRole">The type of agent whose values will be populated to a new preparer if no preparer of the given form is found in the collection.</param>
        /// <param name="initLinked">Whether or not updates to the agent should automatically update the preparer.</param>
        /// <returns>The preparer for the given form, or a new preparer based on the agent whose role is passed in.</returns>
        public IPreparerFields GetPreparerOfForm(E_PreparerFormT form, E_AgentRoleT initializingAgentRole, bool initLinked)
        {
            IPreparerFields preparer = this.GetPreparerOfForm(form, E_ReturnOptionIfNotExist.CreateNew);

            // Below block is for CPreparerFields only
            if (preparer.IsNewRecord)
            {
                CAgentFields initializingAgent = this.GetAgentOfRole(initializingAgentRole, E_ReturnOptionIfNotExist.ReturnEmptyObject);

                if (initializingAgent.IsValid)
                {
                    preparer.SyncFromAgent(initializingAgent);
                    preparer.AgentRoleT = initializingAgentRole;
                    preparer.IsLocked = !initLinked;
                    preparer.Update();

                    preparer = this.GetPreparerOfForm(form); // Need to reload to clear IsNewRecord and thus prevent a subsequent Update() from trying to add the same row twice.
                }
            }

            return preparer;
        }

        /// <summary>
        /// Gets a preparer for the given form, or the thing specified by the return option.
        /// </summary>
        /// <param name="form">The form of the preparer to create.</param>
        /// <param name="returnOption">Instructions about what to return if no preparer of the given form is found in the collection.</param>
        /// <returns>The preparer for the given form, or the thing specified by the return option.</returns>
        public IPreparerFields GetPreparerOfForm(E_PreparerFormT form, E_ReturnOptionIfNotExist returnOption)
        {
            return this.GetPreparerOfForm(form, returnOption, null);
        }

        /// <summary>
        /// Gets a preparer for the given form, or the thing specified by the return option.
        /// </summary>
        /// <param name="form">The form of the preparer to create.</param>
        /// <param name="returnOption">Instructions about what to return if no preparer of the given form is found in the collection.</param>
        /// <param name="changeNotifier">The method to call (if any) when the preparer instance is updated.</param>
        /// <returns>The preparer for the given form, or the thing specified by the return option.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("LendingQBStyleCop.LendingQBCustomRules", "LB1001:DefaultSwitchCaseMustThrowException", Justification = "Copied legacy code out of whitelisted class.")]
        public IPreparerFields GetPreparerOfForm(E_PreparerFormT form, E_ReturnOptionIfNotExist returnOption, Action<IPreparerFields> changeNotifier)
        {
            IPreparerFields preparer = this.GetPreparerOfForm(form, null, changeNotifier);

            if (preparer != null)
            {
                return preparer;
            }

            E_AgentRoleT sourceAgentRoleT;
            bool isCopyFromOfficialContact = false;
            GetDefaultAgentCopyInfoForPreparerFormT(form, out sourceAgentRoleT, out isCopyFromOfficialContact);

            if (isCopyFromOfficialContact)
            {
                CAgentFields sourceAgent = this.GetAgentOfRole(sourceAgentRoleT, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                if (sourceAgent.IsValid)
                {
                    // Since we don't force an update to the new preparer record here, it would introduce a interesting behavior
                    // that users probably want: the preparer of the form can pick up
                    // new processor assigned to the loan, until they edit and (probably save) the Verficiation of Deposit.
                    preparer = this.GetNewPreparerFields(form);

                    if (form == E_PreparerFormT.App1003Interviewer || form == E_PreparerFormT.Gfe || form == E_PreparerFormT.Til)
                    {
                        preparer.IsLocked = false;
                        preparer.AgentRoleT = sourceAgentRoleT;
                    }
                    else
                    {
                        // 5/22/2009 dd - Since I did not enable UI to lock preparer info with official contact list, I cannot
                        // default these preparer to automatically use official contact list. Once we enable all preparer UI
                        // we should remove this line.
                        preparer.SyncFromAgent(sourceAgent);
                    }

                    return preparer;
                }
            }

            switch (returnOption)
            {
                case E_ReturnOptionIfNotExist.CreateNew:
                case E_ReturnOptionIfNotExist.CreateNewDoNotFallBack:
                    preparer = this.GetNewPreparerFields(form);
                    return preparer;
                case E_ReturnOptionIfNotExist.ReturnEmptyObject:
                    return CPreparerFields.Empty;
                default:
                    this.LogError("Unhandled return option enum value in GetPreparerOfForm method");
                    return CPreparerFields.Empty;
            }
        }

        /// <summary>
        /// Get a copy of the <see cref="DataSet"/> that this instance uses to store all the
        /// preparer data. Only the <see cref="PreparerCollection"/> class should use this, and 
        /// then only until workflow checks are made more sensible.
        /// </summary>
        /// <returns>A copy of the <see cref="DataSet"/> that this instance uses to store all the preparer data.</returns>
        internal DataSet GetCopyOfBaseData()
        {
            return this.sPreparerDataSet.Copy();
        }

        /// <summary>
        /// Get a new preparer for the given form.
        /// </summary>
        /// <param name="form">The form of the new preparer.</param>
        /// <returns>The new preparer for the given form.</returns>
        private IPreparerFields GetNewPreparerFields(E_PreparerFormT form)
        {
            return new CPreparerFields(this.pageBase, this.sPreparerDataSet, form);
        }

        /// <summary>
        /// Gets the preparer from the collection at the given index.
        /// </summary>
        /// <param name="iRecord">The index of the preparer to retrieve.</param>
        /// <param name="changeNotifier">The method to call (if any) when the preparer instance is updated.</param>
        /// <returns>The preparer from the collection at the given index.</returns>
        private IPreparerFields GetPreparerFields(int iRecord, Action<IPreparerFields> changeNotifier = null)
        {
            if (iRecord < 0)
            {
                throw new ArgumentException("Dude; don't pass -1 to GetPreparerFields to create a new record, use GetNewPreparerFields.");
            }

            return new CPreparerFields(this.pageBase, this.sPreparerDataSet, iRecord, changeNotifier);
        }
    }
}
