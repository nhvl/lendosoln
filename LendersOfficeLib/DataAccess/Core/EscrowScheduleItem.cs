﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.ObjLib.Conversions.ProvidentFunding;

namespace DataAccess
{

    public class EscrowScheduleItem
    {
        public E_EscrowItemT EscrowItemT { get; private set; }
        public E_TaxTableTaxT TaxTableT { get; private set; }
        public string ParcelNum { get; private set; }
        public string PayeeNm { get; private set; }
        public string NumOfPmt { get; private set; }
        public E_TaxTablePaidBy PaidByT { get; private set; }
        public IEnumerable<TaxScheduleItem> TaxScheduleItemList { get; private set; }


        internal EscrowScheduleItem(E_EscrowItemT escrowItemT, E_TaxTableTaxT taxTableT,
            string parcelNum, string payeeNm, string numOfPmt, E_TaxTablePaidBy paidByT,
            IEnumerable<TaxScheduleItem> taxScheduleItemList)
        {
            this.EscrowItemT = escrowItemT;
            this.TaxTableT = taxTableT;
            this.ParcelNum = parcelNum;
            this.PayeeNm = payeeNm;
            this.NumOfPmt = numOfPmt;
            this.PaidByT = paidByT;
            this.TaxScheduleItemList = taxScheduleItemList;
        }
    }
}
