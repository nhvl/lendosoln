using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Admin;
using LendersOffice.Constants;
using LendersOffice.Security;

namespace DataAccess
{
    public class LoanFileCacheData
    {
        private CPageBase m_internalPageBase = null;

        private AbstractUserPrincipal Principal
        {
            get { return System.Threading.Thread.CurrentPrincipal as AbstractUserPrincipal; }
        }
        public LoanFileCacheData(Guid sLId)
        {
            if (Tools.IsLoanAHELOC(Principal, sLId))
            {
                m_internalPageBase = new CCalcCacheHelocData(sLId);
            }
            else
            {
                m_internalPageBase = new CCalcCacheData(sLId);

            }
        }

        public LoanFileCacheData(Guid sLId, ICollection dirtyFields)
        {
            if (Tools.IsLoanAHELOC(Principal, sLId))
            {
                m_internalPageBase = new CCalcCacheHelocData(sLId, dirtyFields);
            }
            else
            {
                m_internalPageBase = new CCalcCacheData(sLId, dirtyFields);

            }
        }

        public LoanFileCacheData(Guid sLId, ICollection dirtyFields, Guid requestId, bool rerunningCacheUpdateRequest)
        {
            if (Tools.IsLoanAHELOC(Principal, sLId))
            {
                m_internalPageBase = new CCalcCacheHelocData(sLId, dirtyFields, requestId, rerunningCacheUpdateRequest);
            }
            else
            {
                m_internalPageBase = new CCalcCacheData(sLId, dirtyFields, requestId, rerunningCacheUpdateRequest);

            }
        }
        public LoanFileCacheData(Guid sLId, Guid requestId, bool rerunningCacheUpdateRequest)
        {
            if (Tools.IsLoanAHELOC(Principal, sLId))
            {
                m_internalPageBase = new CCalcCacheHelocData(sLId, requestId, rerunningCacheUpdateRequest);
            }
            else
            {
                m_internalPageBase = new CCalcCacheData(sLId, requestId, rerunningCacheUpdateRequest);

            }
        }

        public void UpdateCache()
        {
            // 11/5/2004 kb - Now that we have consolidated our
            // access control checking in the save call of all
            // page data implementations, we have run into a
            // snag where the loan started and writeable, was
            // written to, and became no longer writable after
            // the changes.  The cache update call takes the
            // final state and transfers it to the cache table.
            // This data object will fail with access denied
            // right here, unless we bypass the security check.
            // Ideally, this data object would inherit the access
            // binding rights that the toplevel loan data object
            // discovered when it started its save.  Updating
            // the cache is a special operation that we will
            // always allow -- the worst that happens is a user
            // transfers to the cache what they had permission
            // to write in the first place.

            m_internalPageBase.CalcModeT = E_CalcModeT.LendersOfficePageEditing;
            // 3/15/2010 dd - By pass per-field security.
            m_internalPageBase.ByPassFieldSecurityCheck = true;
            m_internalPageBase.InitSave(ConstAppDavid.SkipVersionCheck);
            m_internalPageBase.Save();
            m_internalPageBase.ByPassFieldSecurityCheck = false; // 3/15/2010 dd - Enable per-field security back.
        }
    }

	/// <summary>
	/// Use an object of this class to update cached data in the loan_file_cache table
	/// TODO: Hide public methods of CPageBase as we don't need to expose them from this class.
	/// </summary>
	public class CCalcCacheData : CPageBaseWrapped
	{
		public	CCalcCacheData(Guid fileId, ICollection dirtyFields )
			: base( fileId, "CalcCacheData", 
				CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputDirtyFields_NeedCachedFields, dirtyFields  ) )
		{
            IEnumerable invalidatedFields = null;
            if (ConstStage.UseAffectedCacheFieldListForInvalidatedCacheFields)
            {
                invalidatedFields = this.m_selectProvider.InputFieldList;
            }
            else
            {
                invalidatedFields = CFieldInfoTable.GetInstance().GatherAffectedCacheFields_Obsolete(dirtyFields).UpdateFields.Collection;
            }

			this.InvalidatedCachedFields = invalidatedFields;
            this.m_enableCaching = ConstStage.EnableCachingInCalcCacheData;
		}

		/// <summary>
		/// Use an object of this class to update cached data in the loan_file_cache table
		/// This constructor is called when re-running a cache table update request
		/// </summary>
		public	CCalcCacheData(Guid fileId, ICollection dirtyFields, Guid requestId, bool rerunningCacheUpdateRequest )
			: base( fileId, "CalcCacheData", 
			CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputDirtyFields_NeedCachedFields, dirtyFields  ), requestId, rerunningCacheUpdateRequest )
		{
            IEnumerable invalidatedFields = null;
            if (ConstStage.UseAffectedCacheFieldListForInvalidatedCacheFields)
            {
                invalidatedFields = this.m_selectProvider.InputFieldList;
            }
            else
            {
                invalidatedFields = CFieldInfoTable.GetInstance().GatherAffectedCacheFields_Obsolete(dirtyFields).UpdateFields.Collection;
            }

            this.InvalidatedCachedFields = invalidatedFields;
            this.m_enableCaching = ConstStage.EnableCachingInCalcCacheData;
		}

		/// <summary>
		/// This contructor will generate an object that will update all cached fields
		/// </summary>
		public CCalcCacheData( Guid fileId )
			: base( fileId, "All Cache Data", CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputNothing_NeedAllCachedFields, null  ) )
		{
			InvalidatedCachedFields = CFieldInfoTable.GetInstance().CachingSet.UpdateFields.Collection;
            this.m_enableCaching = ConstStage.EnableCachingInCalcCacheData;
		}
	
		/// <summary>
		/// This contructor will generate an object that will update all cached fields and is called when a
		/// cache table update request is re-run
		/// </summary>
		public CCalcCacheData( Guid fileId, Guid requestId, bool rerunningCacheUpdateRequest )
			: base( fileId, "All Cache Data", CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputNothing_NeedAllCachedFields, null  ), requestId, rerunningCacheUpdateRequest )
		{
			InvalidatedCachedFields = CFieldInfoTable.GetInstance().CachingSet.UpdateFields.Collection;
            this.m_enableCaching = ConstStage.EnableCachingInCalcCacheData;
		}

		override protected bool IsCachingUtilityObj
		{
			get { return true; }
		}

        private bool m_enableCaching = true;

        private Dictionary<string, decimal> m_cacheDecimalDictionary = new Dictionary<string, decimal>();
        private Dictionary<string, int> m_cacheIntDictionary = new Dictionary<string, int>();
        private IEnumerable<GfeFeeItem> m_cacheGfeFeeItemExclude802List = null;

        private decimal GetDecimalCache(string key, Func<decimal> getter)
        {
            if (m_enableCaching == false)
            {
                return getter();
            }
            else
            {
                decimal v = 0;

                bool hasKey = m_cacheDecimalDictionary.TryGetValue(key, out v);
                if (hasKey == false)
                {
                    v = getter();
                    m_cacheDecimalDictionary[key] = v;
                }

                return v;
            }
        }

        private int GetIntCache(string key, Func<int> getter)
        {
            if (m_enableCaching == false)
            {
                return getter();
            }
            else
            {
                int v = 0;

                bool hasKey = m_cacheIntDictionary.TryGetValue(key, out v);
                if (hasKey == false)
                {
                    v = getter();
                    m_cacheIntDictionary[key] = v;
                }

                return v;
            }
        }

        public override void InvalidateCache()
        {
            base.InvalidateCache();

            m_cacheDecimalDictionary = new Dictionary<string, decimal>();
            m_cacheIntDictionary = new Dictionary<string, int>();
            m_cacheGfeFeeItemExclude802List = null;
        }

        internal override IEnumerable<GfeFeeItem> GfeFeeItemExclude802List
        {
            get
            {
                if (m_enableCaching == false)
                {
                    return base.GfeFeeItemExclude802List;
                }

                if (m_cacheGfeFeeItemExclude802List == null)
                {
                    m_cacheGfeFeeItemExclude802List = base.GfeFeeItemExclude802List;
                }

                return m_cacheGfeFeeItemExclude802List;
            }
        }
        protected override decimal sAmtReqToFund
        {
            get { return this.GetDecimalCache("sAmtReqToFund", () => base.sAmtReqToFund); }
        }
        protected override int GetGfe32BitPropsField(string dbFieldName, E_LegacyGfeFieldT type)
        {
            return this.GetIntCache(dbFieldName + "_" + type, () => base.GetGfe32BitPropsField(dbFieldName, type));
        }

        public override decimal sTransNetCash
        {
            get
            {
                return this.GetDecimalCache("sTransNetCash", () => base.sTransNetCash);
            }
        }

        public override decimal sAprIncludedCc
        {
            get
            {
                return this.GetDecimalCache("sAprIncludedCc", () => base.sAprIncludedCc);
            }
        }

        public override decimal sFinCharge
        {
            get
            {
                return this.GetDecimalCache("sFinCharge", () => base.sFinCharge);
            }
        }

        public override decimal sMonthlyPmt
        {
            get
            {
                return this.GetDecimalCache("sMonthlyPmt", () => base.sMonthlyPmt);
            }
        }

        protected override decimal sLenderPaidFeesAmt
        {
            get
            {
                return this.GetDecimalCache("sLenderPaidFeesAmt", () => base.sLenderPaidFeesAmt);
            }
        }

        [Gfe("sLDiscntProps", E_GfeFlags.RealCost)]
        public override decimal sLDiscnt
        {
            get
            {
                return this.GetDecimalCache("sLDiscnt", () => base.sLDiscnt);
            }
        }

        public override decimal sTotEstCCPoc
        {
            get
            {
                return this.GetDecimalCache("sTotEstCCPoc", () => base.sTotEstCCPoc);
            }
        }

        protected override decimal sLenderInitialCreditAmt
        {
            get
            {
                return this.GetDecimalCache("sLenderInitialCreditAmt", () => base.sLenderInitialCreditAmt);
            }
        }

        public override decimal sFinalLAmt
        {
            get
            {
                return this.GetDecimalCache("sFinalLAmt", () => base.sFinalLAmt);
            }
        }

        public override decimal sFfUfmipFinanced
        {
            get
            {
                return this.GetDecimalCache("sFfUfmipFinanced", () => base.sFfUfmipFinanced);
            }
        }

        public override decimal sLAmtCalc
        {
            get
            {
                return this.GetDecimalCache("sLAmtCalc", () => base.sLAmtCalc);
            }
            set
            {
                base.sLAmtCalc = value;
                this.InvalidateCache();
            }
        }

        protected override decimal sGfeCreditLenderPaidItemF
        {
            get
            {
                return this.GetDecimalCache("sGfeCreditLenderPaidItemF", () => base.sGfeCreditLenderPaidItemF);
            }
        }

        public override decimal sGfeTotalFundByLender
        {
            get
            {
                return this.GetDecimalCache("sGfeTotalFundByLender", () => base.sGfeTotalFundByLender);
            }
        }

        [Gfe("sGfeOriginatorCompFProps", E_GfeFlags.RealCost)]
        public override decimal sGfeOriginatorCompF
        {
            get
            {
                return this.GetDecimalCache("sGfeOriginatorCompF", () => base.sGfeOriginatorCompF);
            }
        }

        protected override decimal sLenderTargetInitialCreditAmt
        {
            get
            {
                return this.GetDecimalCache("sLenderTargetInitialCreditAmt", () => base.sLenderTargetInitialCreditAmt);
            }
        }

        public override decimal sRecurringMipPia
        {
            get
            {
                return this.GetDecimalCache("sRecurringMipPia", () => base.sRecurringMipPia);
            }
        }

        public override decimal sUpfrontMipPia
        {
            get
            {
                return this.GetDecimalCache("sUpfrontMipPia", () => base.sUpfrontMipPia);
            }
        }

        public override decimal sVaFf
        {
            get
            {
                return this.GetDecimalCache("sVaFf", () => base.sVaFf);
            }
        }

        public override decimal sProHazInsMb
        {
            get
            {
                return this.GetDecimalCache("sProHazInsMb", () => base.sProHazInsMb);
            }
        }

        public override decimal sProHazInsR
        {
            get
            {
                return this.GetDecimalCache("sProHazInsR", () => base.sProHazInsR);
            }
        }

        public override decimal sIPerDay
        {
            get
            {
                return this.GetDecimalCache("sIPerDay", () => base.sIPerDay);
            }
        }

        public override decimal sProHazIns
        {
            get
            {
                return this.GetDecimalCache("sProHazIns", () => base.sProHazIns);
            }
        }

        public override decimal sProMIns
        {
            get
            {
                return this.GetDecimalCache("sProMIns", () => base.sProMIns);
            }
        }

        public override decimal sProRealETxMb
        {
            get
            {
                return this.GetDecimalCache("sProRealETxMb", () => base.sProRealETxMb);
            }
        }

        public override decimal sProSchoolTx
        {
            get
            {
                return this.GetDecimalCache("sProSchoolTx", () => base.sProSchoolTx);
            }
        }

        public override decimal sProFloodIns
        {
            get
            {
                return this.GetDecimalCache("sProFloodIns", () => base.sProFloodIns);
            }
        }


        [Gfe("sAggregateAdjRsrvProps", E_GfeFlags.McawPurch12b)]
        public override decimal sAggregateAdjRsrv
        {
            get
            {
                return this.GetDecimalCache("sAggregateAdjRsrv", () => base.sAggregateAdjRsrv);
            }
        }


        public override decimal s1006ProHExp
        {
            get
            {
                return this.GetDecimalCache("s1006ProHExp", () => base.s1006ProHExp);
            }
        }

        public override decimal s1007ProHExp
        {
            get
            {
                return this.GetDecimalCache("s1007ProHExp", () => base.s1007ProHExp);
            }
        }

        public override decimal sProRealETxR
        {
            get
            {
                return this.GetDecimalCache("sProRealETxR", () => base.sProRealETxR);
            }
        }

        public override decimal sGfeDiscountPointF
        {
            get
            {
                return this.GetDecimalCache("sGfeDiscountPointF", () => base.sGfeDiscountPointF);
            }
        }


        [Gfe("sMInsRsrvProps", E_GfeFlags.McawPurch12b)]
        public override decimal sMInsRsrv
        {
            get
            {
                return this.GetDecimalCache("sMInsRsrv", () => base.sMInsRsrv);
            }
        }


        public override decimal sOriginatorCompensationTotalAmount
        {
            get
            {
                return this.GetDecimalCache("sOriginatorCompensationTotalAmount", () => base.sOriginatorCompensationTotalAmount);
            }
        }

	}

    /// <summary>
    /// // 11/1/2013 dd - I DO NOT LIKE THE FACT that I need to separate the CCalcCacheData. However there is nothing I can do.
    /// TODO: Hide public methods of CPageBase as we don't need to expose them from this class.
    /// </summary>
    public class CCalcCacheHelocData : CPageHelocBase
    {
        public CCalcCacheHelocData(Guid fileId, ICollection dirtyFields)
            : base(fileId, "CCalcCacheHelocData",
                CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputDirtyFields_NeedCachedFields, dirtyFields))
        {
            IEnumerable invalidatedFields = null;
            if (ConstStage.UseAffectedCacheFieldListForInvalidatedCacheFields)
            {
                invalidatedFields = this.m_selectProvider.InputFieldList;
            }
            else
            {
                invalidatedFields = CFieldInfoTable.GetInstance().GatherAffectedCacheFields_Obsolete(dirtyFields).UpdateFields.Collection;
            }

            InvalidatedCachedFields = invalidatedFields;
        }

        /// <summary>
        /// Use an object of this class to update cached data in the loan_file_cache table
        /// This constructor is called when re-running a cache table update request
        /// </summary>
        public CCalcCacheHelocData(Guid fileId, ICollection dirtyFields, Guid requestId, bool rerunningCacheUpdateRequest)
            : base(fileId, "CalcCacheData",
            CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputDirtyFields_NeedCachedFields, dirtyFields), requestId, rerunningCacheUpdateRequest)
        {
            IEnumerable invalidatedFields = null;
            if (ConstStage.UseAffectedCacheFieldListForInvalidatedCacheFields)
            {
                invalidatedFields = this.m_selectProvider.InputFieldList;
            }
            else
            {
                invalidatedFields = CFieldInfoTable.GetInstance().GatherAffectedCacheFields_Obsolete(dirtyFields).UpdateFields.Collection;
            }

            InvalidatedCachedFields = invalidatedFields;
        }

        /// <summary>
        /// This contructor will generate an object that will update all cached fields
        /// </summary>
        public CCalcCacheHelocData(Guid fileId)
            : base(fileId, "All Cache Data", CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputNothing_NeedAllCachedFields, null))
        {
            InvalidatedCachedFields = CFieldInfoTable.GetInstance().CachingSet.UpdateFields.Collection;
        }

        /// <summary>
        /// This contructor will generate an object that will update all cached fields and is called when a
        /// cache table update request is re-run
        /// </summary>
        public CCalcCacheHelocData(Guid fileId, Guid requestId, bool rerunningCacheUpdateRequest)
            : base(fileId, "All Cache Data", CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputNothing_NeedAllCachedFields, null), requestId, rerunningCacheUpdateRequest)
        {
            InvalidatedCachedFields = CFieldInfoTable.GetInstance().CachingSet.UpdateFields.Collection;
        }

        override protected bool IsCachingUtilityObj
        {
            get { return true; }
        }
    }

}