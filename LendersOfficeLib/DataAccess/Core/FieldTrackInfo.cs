/// Author: David Dao

using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Xml.Schema;
using System.Xml;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Security;

namespace DataAccess
{
    public enum E_FieldTrackInfoTypeT
    {
        Normal = 0,
        New = 1,
        Obsolete = 2
    }

    public class FieldDescription
    {
        public const string Category_Credit = "Step 1 - Credit";
        public const string Category_Applicant = "Step 2 - Applicant";
        public const string Category_Property = "Step 3 - Property & Loan";
        private class __
        {
            public string Description { get; set; }
            public string Category { get; set; }
        }
        private static Dictionary<string, __> s_hash = new Dictionary<string, __>()
        {
            {"sProdCrManualNonRolling30MortLateCount", new __() { Description = "Non-Rolling Mortgage x30", Category=Category_Credit}}
            , {"sProdCrManual60MortLateCount", new __() { Description="Non-Rolling Mortgage x60", Category=Category_Credit}}
            , {"sProdCrManual90MortLateCount",  new __() { Description="Non-Rolling Mortgage x90", Category=Category_Credit}}
            , {"sProdCrManual120MortLateCount",  new __() { Description="Non-Rolling Mortgage x120", Category=Category_Credit}}
            , {"sProdCrManual150MortLateCount",  new __() { Description="Non-Rolling Mortgage x150", Category=Category_Credit}}
            , {"sProdCrManual30MortLateCount",  new __() { Description="Rolling Mortgage x30", Category=Category_Credit}}
            , {"sProdCrManualRolling60MortLateCount",  new __() { Description="Rolling Mortgage x60", Category=Category_Credit}}
            , {"sProdCrManualRolling90MortLateCount",  new __() { Description="Rolling Mortgage x90", Category=Category_Credit}}
            , {"sProdCrManualForeclosureHas",  new __() { Description="Has Foreclosure/NOD", Category=Category_Credit}}
            , {"sProdCrManualForeclosureRecentFileMon",  new __() { Description="Foreclosure File Month", Category=Category_Credit}}
            , {"sProdCrManualForeclosureRecentFileYr",  new __() { Description="Foreclosure File Year", Category=Category_Credit}}
            , {"sProdCrManualForeclosureRecentStatusT",  new __() { Description="Foreclosure Status", Category=Category_Credit}}
            , {"sProdCrManualForeclosureRecentSatisfiedMon",  new __() { Description="Foreclosure Satisfied Month", Category=Category_Credit}}
            , {"sProdCrManualForeclosureRecentSatisfiedYr",  new __() { Description="Foreclosure Satisfied Year", Category=Category_Credit}}
            , {"sProdCrManualBk7Has",  new __() { Description="Has Chapter 7", Category=Category_Credit}}
            , {"sProdCrManualBk7RecentFileMon",  new __() { Description="Chapter 7 File Month", Category=Category_Credit}}
            , {"sProdCrManualBk7RecentFileYr",  new __() { Description="Chapter 7 File Year", Category=Category_Credit}}
            , {"sProdCrManualBk7RecentStatusT",  new __() { Description="Chapter 7 Status", Category=Category_Credit}}
            , {"sProdCrManualBk7RecentSatisfiedMon",  new __() { Description="Chapter 7 Satisfied Month", Category=Category_Credit}}
            , {"sProdCrManualBk7RecentSatisfiedYr",  new __() { Description="Chapter 7 Satisfied Year", Category=Category_Credit}}
            , {"sProdCrManualBk13Has",  new __() { Description="Has Chapter 13", Category=Category_Credit}}
            , {"sProdCrManualBk13RecentFileMon",  new __() { Description="Chapter 13 File Month", Category=Category_Credit}}
            , {"sProdCrManualBk13RecentFileYr",  new __() { Description="Chapter 13 File Year", Category=Category_Credit}}
            , {"sProdCrManualBk13RecentStatusT",  new __() { Description="Chapter 13 Status", Category=Category_Credit}}
            , {"sProdCrManualBk13RecentSatisfiedMon",  new __() { Description="Chapter 13 Satisfied Month", Category=Category_Credit}}
            , {"sProdCrManualBk13RecentSatisfiedYr",  new __() { Description="Chapter 13 Satisfied Year", Category=Category_Credit}}

            , {"aProdCrManualNonRolling30MortLateCount", new __() { Description = "Non-Rolling Mortgage x30", Category=Category_Credit}}
            , {"aProdCrManual60MortLateCount", new __() { Description="Non-Rolling Mortgage x60", Category=Category_Credit}}
            , {"aProdCrManual90MortLateCount",  new __() { Description="Non-Rolling Mortgage x90", Category=Category_Credit}}
            , {"aProdCrManual120MortLateCount",  new __() { Description="Non-Rolling Mortgage x120", Category=Category_Credit}}
            , {"aProdCrManual150MortLateCount",  new __() { Description="Non-Rolling Mortgage x150", Category=Category_Credit}}
            , {"aProdCrManual30MortLateCount",  new __() { Description="Rolling Mortgage x30", Category=Category_Credit}}
            , {"aProdCrManualRolling60MortLateCount",  new __() { Description="Rolling Mortgage x60", Category=Category_Credit}}
            , {"aProdCrManualRolling90MortLateCount",  new __() { Description="Rolling Mortgage x90", Category=Category_Credit}}
            , {"aProdCrManualForeclosureHas",  new __() { Description="Has Foreclosure/NOD", Category=Category_Credit}}
            , {"aProdCrManualForeclosureRecentFileMon",  new __() { Description="Foreclosure File Month", Category=Category_Credit}}
            , {"aProdCrManualForeclosureRecentFileYr",  new __() { Description="Foreclosure File Year", Category=Category_Credit}}
            , {"aProdCrManualForeclosureRecentStatusT",  new __() { Description="Foreclosure Status", Category=Category_Credit}}
            , {"aProdCrManualForeclosureRecentSatisfiedMon",  new __() { Description="Foreclosure Satisfied Month", Category=Category_Credit}}
            , {"aProdCrManualForeclosureRecentSatisfiedYr",  new __() { Description="Foreclosure Satisfied Year", Category=Category_Credit}}
            , {"aProdCrManualBk7Has",  new __() { Description="Has Chapter 7", Category=Category_Credit}}
            , {"aProdCrManualBk7RecentFileMon",  new __() { Description="Chapter 7 File Month", Category=Category_Credit}}
            , {"aProdCrManualBk7RecentFileYr",  new __() { Description="Chapter 7 File Year", Category=Category_Credit}}
            , {"aProdCrManualBk7RecentStatusT",  new __() { Description="Chapter 7 Status", Category=Category_Credit}}
            , {"aProdCrManualBk7RecentSatisfiedMon",  new __() { Description="Chapter 7 Satisfied Month", Category=Category_Credit}}
            , {"aProdCrManualBk7RecentSatisfiedYr",  new __() { Description="Chapter 7 Satisfied Year", Category=Category_Credit}}
            , {"aProdCrManualBk13Has",  new __() { Description="Has Chapter 13", Category=Category_Credit}}
            , {"aProdCrManualBk13RecentFileMon",  new __() { Description="Chapter 13 File Month", Category=Category_Credit}}
            , {"aProdCrManualBk13RecentFileYr",  new __() { Description="Chapter 13 File Year", Category=Category_Credit}}
            , {"aProdCrManualBk13RecentStatusT",  new __() { Description="Chapter 13 Status", Category=Category_Credit}}
            , {"aProdCrManualBk13RecentSatisfiedMon",  new __() { Description="Chapter 13 Satisfied Month", Category=Category_Credit}}
            , {"aProdCrManualBk13RecentSatisfiedYr",  new __() { Description="Chapter 13 Satisfied Year", Category=Category_Credit}}
                        , {"aTransmOMonPmtPe",  new __() { Description="Total Payment", Category=Category_Applicant}}
                                    , {"aOpNegCfPe",  new __() { Description="Negative Cashflow From Other Properties", Category=Category_Applicant}}


            , {"aCreditReportFileDbKey",  new __() { Description="Credit Report", Category=Category_Credit}}
            , {"aBFirstNm",  new __() { Description="Applicant First Name", Category=Category_Applicant}}
            , {"aBMidNm",  new __() { Description="Applicant Middle Name", Category=Category_Applicant}}
            , {"aBLastNm",  new __() { Description="Applicant Last Name", Category=Category_Applicant}}
            , {"aBSuffix",  new __() { Description="Applicant Suffix", Category=Category_Applicant}}
            , {"aBSsn",  new __() { Description="Applicant SSN", Category=Category_Applicant}}
            , {"aBExperianScorePe",  new __() { Description="Applicant Experian Score", Category=Category_Applicant}}
            , {"aBTransUnionScorePe",  new __() { Description="Applicant TransUnion Score", Category=Category_Applicant}}
            , {"aBEquifaxScorePe",  new __() { Description="Applicant Equifax Score", Category=Category_Applicant}}
            , {"aProdBCitizenT",  new __() { Description="Applicant Citizenship", Category=Category_Applicant}}
            , {"aProdCCitizenT",  new __() { Description="Co-Applicant Citizenship", Category=Category_Applicant}}

            , {"aBHasSpouse",  new __() { Description="Has Co-Applicant", Category=Category_Applicant}}
            , {"aCFirstNm",  new __() { Description="Co-Applicant First Name", Category=Category_Applicant}}
            , {"aCMidNm",  new __() { Description="Co-Applicant Middle Name", Category=Category_Applicant}}
            , {"aCLastNm",  new __() { Description="Co-Applicant Last Name", Category=Category_Applicant}}
            , {"aCSuffix",  new __() { Description="Co-Applicant Suffix", Category=Category_Applicant}}
            , {"aCSsn",  new __() { Description="Co-Applicant SSN", Category=Category_Applicant}}
            , {"aCExperianScorePe",  new __() { Description="Co-Applicant Experian Score", Category=Category_Applicant}}
            , {"aCTransUnionScorePe",  new __() { Description="Co-Applicant TransUnion Score", Category=Category_Applicant}}
            , {"aCEquifaxScorePe",  new __() { Description="Co-Applicant Equifax Score", Category=Category_Applicant}}
            , {"aIsBorrSpousePrimaryWageEarner",  new __() { Description="Co-Applicant is Primary Wage Earner", Category=Category_Applicant}}
            , {"sIsSelfEmployed",  new __() { Description="Is Self-Employed", Category=Category_Applicant}}
            , {"aBIsSelfEmplmt",  new __() { Description="Applicant Is Self-Employed", Category=Category_Applicant}}
            , {"aCIsSelfEmplmt",  new __() { Description="Co-Applicant Is Self-Employed", Category=Category_Applicant}}

            , {"sTransmOMonPmtPe",  new __() { Description="Total Payment", Category=Category_Applicant}}
            , {"sOpNegCfPe",  new __() { Description="Negative Cashflow From Other Properties", Category=Category_Applicant}}
            , {"aPublicRecordXmlContent",  new __() { Description="Public Records", Category=Category_Applicant}}
            , {"sSpAddr",  new __() { Description="Property Address", Category=Category_Property}}
            , {"sSpCity",  new __() { Description="Property City", Category=Category_Property}}
            , {"sSpStatePe",  new __() { Description="Property State", Category=Category_Property}}
            , {"sSpZip",  new __() { Description="Property Zipcode", Category=Category_Property}}
            , {"sSpCounty",  new __() { Description="Property County", Category=Category_Property}}
            , {"sOccTPe",  new __() { Description="Property Use", Category=Category_Property}}
            , {"sProRealETxPe",  new __() { Description="Proposed Property Tax", Category=Category_Property}}
            , {"sProOHExpPe",  new __() { Description="Other Proposed Housing Expenses", Category=Category_Property}}
            , {"sOccRPe",  new __() { Description="Occupancy Rate (%)", Category=Category_Property}}
            , {"sSpGrossRentPe",  new __() { Description="Gross Rent", Category=Category_Property}}
            , {"sProdSpT",  new __() { Description="Property Type", Category=Category_Property}}
            , {"sProdSpStructureT",  new __() { Description="Structure Type", Category=Category_Property}}
            , {"sProdCondoStories",  new __() { Description="Condo Stories", Category=Category_Property}}
            , {"sProdIsSpInRuralArea",  new __() { Description="Is In Rural Area", Category=Category_Property}}
            , {"sProdIsCondotel",  new __() { Description="Is Condotel", Category=Category_Property}}
            , {"sProdIsNonwarrantableProj",  new __() { Description="Is Non-Warrantable Proj", Category=Category_Property}}
            , {"aPresOHExpPe",  new __() { Description="Present Housing Expense", Category=Category_Property}}
            , {"sLPurposeTPe",  new __() { Description="Loan Purpose", Category=Category_Property}}
            , {"sProdCashoutAmt",  new __() { Description="Cashout Amount", Category=Category_Property}}
            , {"sHas1stTimeBuyerPe",  new __() { Description="First Time Home Buyer", Category=Category_Property}}
            , {"sProdHasHousingHistory",  new __() { Description="Has Housing History", Category=Category_Property}}
            , {"sIsIOnlyPe",  new __() { Description="Interest Only (1st Lien)", Category=Category_Property}}
            , {"sProdImpound",  new __() { Description="Impound", Category=Category_Property}}
            , {"sProdDocT",  new __() { Description="Doc Type", Category=Category_Property}}
            , {"sPrimAppTotNonspIPe",  new __() { Description="Total Income", Category=Category_Property}}
            , {"sProdEstimatedResidualI",  new __() { Description="Estimated Residual Income", Category=Category_Property}}
            , {"sProdFinMethFilterT",  new __() { Description="Amort Type", Category=Category_Property}}
            , {"sProdPpmtPenaltyMon",  new __() { Description="Prepay Penalty", Category=Category_Property}}
            , {"sProdRLckdDays",  new __() { Description="Rate Lock Period", Category=Category_Property}}
            , {"sProdAvailReserveMonths",  new __() { Description="Reserves Available", Category=Category_Property}}
            , {"sProd3rdPartyUwResultT",  new __() { Description="AU Response", Category=Category_Property}}
            , {"sHouseValPe",  new __() { Description="Sales Price/House Value", Category=Category_Property}}
            , {"sDownPmtPcPe",  new __() { Description="Down Payment/Equity Percent", Category=Category_Property}}
            , {"sEquityPe",  new __() { Description="Down Payment/Equity", Category=Category_Property}}
            , {"sLtvRPe",  new __() { Description="Target LTV (1st Lien)", Category=Category_Property}}
            , {"sLAmtCalcPe",  new __() { Description="Loan Amount (1st Lien)", Category=Category_Property}}
            , {"sCltvRPe",  new __() { Description="Target CLTV", Category=Category_Property}}
            , {"sLtvROtherFinPe",  new __() { Description="2nd Financing LTV", Category=Category_Property}}
            , {"sProOFinBalPe",  new __() { Description="2nd Financing Amount", Category=Category_Property}}
            , {"sProdPpmtPenaltyMon2ndLien",  new __() { Description="2nd Lien Prepay Penalty", Category=Category_Property}}
            , {"sProdMIOptionT",  new __() { Description="Mortgage Insurance", Category=Category_Property}}
            , {"sProdFilterDue10Yrs",  new __() { Description="Filter Due in 10 yrs", Category=Category_Property}}
            , {"sProdFilterDue15Yrs",  new __() { Description="Filter Due in 15 yrs", Category=Category_Property}}
            , {"sProdFilterDue20Yrs",  new __() { Description="Filter Due in 20 yrs", Category=Category_Property}}
            , {"sProdFilterDue25Yrs",  new __() { Description="Filter Due in 25 yrs", Category=Category_Property}}
            , {"sProdFilterDue30Yrs",  new __() { Description="Filter Due in 30 yrs", Category=Category_Property}}            
            , {"sProdFilterDueOther",  new __() { Description="Filter Due Other", Category=Category_Property}}
            , {"sProdFilterFinMethFixed",  new __() { Description="Filter Amort Type - Fixed", Category=Category_Property}}
            , {"sProdFilterFinMethOptionArm",  new __() { Description="Filter Amort Type - Option ARM", Category=Category_Property}}            
            , {"sProdFilterFinMeth3YrsArm",  new __() { Description="Filter Amort Type - 3 Yrs ARM", Category=Category_Property}}
            , {"sProdFilterFinMeth5YrsArm",  new __() { Description="Filter Amort Type - 5 Yrs ARM", Category=Category_Property}}
            , {"sProdFilterFinMeth7YrsArm",  new __() { Description="Filter Amort Type - 7 Yrs ARM", Category=Category_Property}}
            , {"sProdFilterFinMeth10YrsArm",  new __() { Description="Filter Amort Type - 10 Yrs ARM", Category=Category_Property}}
            , {"sProdFilterFinMethOther",  new __() { Description="Filter Amort Type - Other", Category=Category_Property}}
            , {"sProdFilterPmtTPI",  new __() { Description="Filter Payment Type - P&I", Category=Category_Property}}
            , {"sProdFilterPmtTIOnly",  new __() { Description="Filter Payment Type - I/O", Category=Category_Property}}
            , {"sProd3rdPartyUwProcessingT",  new __() { Description="AU Processing", Category=Category_Property}}
            , {"sLpIsNegAmortOtherLien",  new __() { Description="1st Lien has Negative Amort.", Category=Category_Property}}
            , {"sOtherLFinMethT",  new __() { Description="1st Lien Amortization Type", Category=Category_Property}}
            , {"sProOFinPmtPe",  new __() { Description="1st Lien Payment", Category=Category_Property}}
            , {"sProdIncludeMyCommunityProc",  new __() { Description="My Community AU Processing", Category=Category_Property}}
            , {"sProdIncludeHomePossibleProc",  new __() { Description="Home Possible AU Processing", Category=Category_Property}}
            , {"sProdIncludeNormalProc",  new __() { Description="Conventional AU Processing", Category=Category_Property}}
            , {"sProdIncludeFHATotalProc",  new __() { Description="FHA Total AU Processing", Category=Category_Property}}
            , {"sProdIncludeVAProc",  new __() { Description="VA AU Processing", Category=Category_Property}}

            , {"aAppId", new __() { Description="Applicant", Category = Category_Applicant}}
            , {"aLiaCollection", new __() { Description="Tradeline", Category=Category_Applicant}}
            
            , {"aLiaCollection_ComNm", new __() { Description="Company Name", Category=Category_Applicant}}
            , {"aLiaCollection_Bal", new __() { Description="Balance", Category=Category_Applicant}}
            , {"aLiaCollection_Pmt", new __() { Description="Payment", Category=Category_Applicant}}
            , {"aLiaCollection_WillBePdOff", new __() { Description="Pd Off", Category=Category_Applicant}}
            , {"aLiaCollection_UsedInRatio", new __() { Description="Used In Ratio", Category=Category_Applicant}}

        };
        public static string GetDescription(string fieldId)
        {
            __ o;
            if (!s_hash.TryGetValue(fieldId, out o))
            {
                Tools.LogBug("Unhandle description for field Id=" + fieldId);
                return string.Empty;
            }
            return o.Description;
        }
        public static string GetCategory(string fieldId)
        {
            __ o;
            if (!s_hash.TryGetValue(fieldId, out o))
            {
                Tools.LogBug("Unhandle category for field Id=" + fieldId);
                return string.Empty;
            }
            return o.Category;

        }
    }

    public class FieldTrackInfo
    {
        #region Enum Mapping
        private static Dictionary<E_sProdMIOptionT, string> sProdMIOptionT_Map = new Dictionary<E_sProdMIOptionT, string>()
        {
            {E_sProdMIOptionT.BorrowerPdPmi, "Borrower Paid MI"}
            , {E_sProdMIOptionT.LeaveBlank, "Leave Blank"}
            , {E_sProdMIOptionT.NoPmi, "No MI / LPMI / Other"}
        };

        private static Dictionary<E_aProdCitizenT, string> aProdCitizenT_Map = new Dictionary<E_aProdCitizenT, string>()
        {
            { E_aProdCitizenT.USCitizen, "US Citizen"}
            , { E_aProdCitizenT.PermanentResident, "Permanent Resident"}
            , { E_aProdCitizenT.NonpermanentResident, "Non-permanent Resident"}
            , { E_aProdCitizenT.ForeignNational, "Non-Resident Alien (Foreign National)"}
        };
        private static Dictionary<E_sOccT, string> sOccT_Map = new Dictionary<E_sOccT, string>()
        {
            { E_sOccT.PrimaryResidence, "Primary Residence"}
            , { E_sOccT.SecondaryResidence, "Secondary Residence"}
            , { E_sOccT.Investment, "Investment"}
        };
        private static Dictionary<E_sProdSpT, string> sProdSpT_Map = new Dictionary<E_sProdSpT, string>()
        {
            { E_sProdSpT.Commercial, "Commercial"}
            , { E_sProdSpT.Condo, "Condo"}
            , { E_sProdSpT.CoOp, "Co-Op"}
            , { E_sProdSpT.FourUnits, "4 Units"}
            , { E_sProdSpT.Manufactured, "Manufactured"}
            , { E_sProdSpT.MixedUse, "Mixed Use"}
            , { E_sProdSpT.Modular, "Modular"}
            , { E_sProdSpT.PUD, "PUD"}
            , { E_sProdSpT.Rowhouse, "Rowhouse"}
            , { E_sProdSpT.SFR, "SFR"}
            , { E_sProdSpT.ThreeUnits, "3 Units"}
            , { E_sProdSpT.Townhouse, "Townhouse"}
            , { E_sProdSpT.TwoUnits, "2 Units"}
        };

        private static Dictionary<E_sProdSpStructureT, string> sProdSpStructureT_Map = new Dictionary<E_sProdSpStructureT, string>()
        {
            { E_sProdSpStructureT.Attached, "Attached"}
            , { E_sProdSpStructureT.Detached, "Detached"}
        };

        private static Dictionary<E_sLPurposeT, string> sLPurposeT_Map = new Dictionary<E_sLPurposeT, string>()
        {
            { E_sLPurposeT.Construct, "Construction"}
            , { E_sLPurposeT.ConstructPerm, "Construction Perm"}
            , { E_sLPurposeT.FhaStreamlinedRefinance, "FHA Streamline Refi"}
            , { E_sLPurposeT.Other, "Other"}
            , { E_sLPurposeT.Purchase, "Purchase"}
            , { E_sLPurposeT.Refin, "Refi Rate/Term"}
            , { E_sLPurposeT.RefinCashout, "RefinCashout"}
            , { E_sLPurposeT.VaIrrrl, "VA IRRRL"}
            , { E_sLPurposeT.HomeEquity, "Home Equity"}
        };
        private static Dictionary<E_sProdDocT, string> GetsProdDocT_Map() 
        {
            string altDoc =  "Alt - 12 months bank stmts";

            if (PrincipalFactory.CurrentPrincipal != null)
            {
                BrokerDB db = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);
                if (db.RenameAlternativeDocType)
                {
                    altDoc = "Alt Doc";
                }
            }

            Dictionary<E_sProdDocT, string> items = new Dictionary<E_sProdDocT, string>()
             {{ E_sProdDocT.Alt, altDoc}
            , { E_sProdDocT.Full, "Full Document"}
            , { E_sProdDocT.Light, "Lite - 6 months bank stmts"}
            , { E_sProdDocT.NINA, "NINA - No Income, No Assets"}
            , { E_sProdDocT.NINANE, "No Doc - No Income, No Assets, No Empl"}
            , { E_sProdDocT.NISA, "NISA - No Income, Stated Assets"}
            , { E_sProdDocT.NIVA, "No Ratio - No Income, Verified Assets"}
            , { E_sProdDocT.NIVANE, "NoDoc Verif Assets - No Income, Verified Assets, No Empl"}
            , { E_sProdDocT.SISA, "NIV (SISA) - Stated Income, Stated Assets"}
            , { E_sProdDocT.SIVA, "NIV (SIVA) - Stated Income, Verified Assets"}
            , { E_sProdDocT.Streamline, "FHA Streamline Refinance"}
            , { E_sProdDocT.VINA, "VINA - Verified Income, No Assets"}
            , { E_sProdDocT.VISA, "VISA - Verified Income, Stated Assets"}
            , { E_sProdDocT._12MoPersonalBankStatements, "12 Mo. Personal Bank Statements" }
            , { E_sProdDocT._24MoPersonalBankStatements, "24 Mo. Personal Bank Statements" }
            , { E_sProdDocT._12MoBusinessBankStatements, "12 Mo. Business Bank Statements" }
            , { E_sProdDocT._24MoBusinessBankStatements, "24 Mo. Business Bank Statements" }
            , { E_sProdDocT.OtherBankStatements, "Other Bank Statements" }
            , { E_sProdDocT._1YrTaxReturns, "1 Yr. Tax Returns" }
            , { E_sProdDocT.Voe, "VOE" }
            , { E_sProdDocT.AssetUtilization, "Asset Utilization" }
            , { E_sProdDocT.DebtServiceCoverage, "Debt Service Coverage (DSCR)" }
            , { E_sProdDocT.NoIncome, "No Ratio" }};
            return items;
        }
        private static Dictionary<E_sProdFinMethFilterT, string> sProdFinMethFilterT_Map = new Dictionary<E_sProdFinMethFilterT, string>()
        {
            { E_sProdFinMethFilterT.All, "Fixed and ARM"}
            , { E_sProdFinMethFilterT.ArmOnly, "ARM Only"}
            , { E_sProdFinMethFilterT.FixedOnly, "Fixed Only"}

        };
        private static Dictionary<E_sProd3rdPartyUwResultT, string> sProd3rdPartyUwResultT_Map = new Dictionary<E_sProd3rdPartyUwResultT, string>()
        {
            { E_sProd3rdPartyUwResultT.DU_ApproveEligible, "DU Approve/Eligible"}
            , { E_sProd3rdPartyUwResultT.DU_ApproveIneligible, "DU Approve/Ineligible"}
            , { E_sProd3rdPartyUwResultT.DU_EAIEligible, "DU EA I/Eligible"}
            , { E_sProd3rdPartyUwResultT.DU_EAIIEligible, "DU EA II/Eligible"}
            , { E_sProd3rdPartyUwResultT.DU_EAIIIEligible, "DU EA III/Eligible"}
            , { E_sProd3rdPartyUwResultT.DU_ReferEligible, "DU Refer/Eligible"}
            , { E_sProd3rdPartyUwResultT.DU_ReferIneligible, "DU Refer/Ineligible"}
            , { E_sProd3rdPartyUwResultT.DU_ReferWCautionEligible, "DU Refer with Caution/Eligible"}
            , { E_sProd3rdPartyUwResultT.DU_ReferWCautionIneligible, "DU Refer with Caution/Ineligible"}
            , { E_sProd3rdPartyUwResultT.LP_AcceptEligible, "LP Accept/Eligible"}
            , { E_sProd3rdPartyUwResultT.LP_AcceptIneligible, "LP Accept/Ineligible"}
            , { E_sProd3rdPartyUwResultT.Lp_AMinus_Level1, "LP A- Level 1"}
            , { E_sProd3rdPartyUwResultT.Lp_AMinus_Level2, "LP A- Level 2"}
            , { E_sProd3rdPartyUwResultT.Lp_AMinus_Level3, "LP A- Level 3"}
            , { E_sProd3rdPartyUwResultT.Lp_AMinus_Level4, "LP A- Level 4"}
            , { E_sProd3rdPartyUwResultT.Lp_AMinus_Level5, "LP A- Level 5"}
            , { E_sProd3rdPartyUwResultT.LP_CautionEligible, "LP Caution/Eligible"}
            , { E_sProd3rdPartyUwResultT.LP_CautionIneligible, "LP Caution/Ineligible"}
            , { E_sProd3rdPartyUwResultT.Lp_Refer, "LP Refer"}
            , { E_sProd3rdPartyUwResultT.NA, "None/Not Submitted"}
            , { E_sProd3rdPartyUwResultT.OutOfScope, "Out of Scope"}
            , { E_sProd3rdPartyUwResultT.Total_ApproveEligible, "TOTAL Approve/Eligible"}
            , { E_sProd3rdPartyUwResultT.Total_ApproveIneligible, "TOTAL Approve/Ineligible"}
            , { E_sProd3rdPartyUwResultT.Total_ReferEligible, "TOTAL Refer/Eligible"}
            , { E_sProd3rdPartyUwResultT.Total_ReferIneligible, "TOTAL Refer/Ineligible"}

        };

        private static Dictionary<E_sProd3rdPartyUwProcessingT, string> sProd3rdPartyUwProcessingT_Map = new Dictionary<E_sProd3rdPartyUwProcessingT, string>()
        {
            { E_sProd3rdPartyUwProcessingT.DU_MyCommunity, "My Community"}
            , { E_sProd3rdPartyUwProcessingT.LP_HomePossible, "Home Possible"}
            , { E_sProd3rdPartyUwProcessingT.Normal, "Normal"}
        };

        private static Dictionary<E_sProdCrManualDerogRecentStatusT, string> sProdCrManualDerogRecentStatusT_Map = new Dictionary<E_sProdCrManualDerogRecentStatusT, string>()
        {
            { E_sProdCrManualDerogRecentStatusT.Discharged, "Discharged"}
            , { E_sProdCrManualDerogRecentStatusT.Dismissed, "Dismissed"}
            , { E_sProdCrManualDerogRecentStatusT.NotSatisfied, "Not Satisfied"}
        };

        private static Dictionary<E_sFinMethT, string> sFinMethT_Map = new Dictionary<E_sFinMethT, string>()
        {
            { E_sFinMethT.ARM, "ARM"}
            , { E_sFinMethT.Fixed, "Fixed"}
            , { E_sFinMethT.Graduated, "Graduated"}
        };
        #endregion
        private string GetDescription<T>(string value, Dictionary<T, string> map)
        {
            if (string.IsNullOrEmpty(value))
                return string.Empty;

            string ret;

            try
            {
                T _key = (T)Enum.Parse(typeof(T), value);
                if (!map.TryGetValue(_key, out ret))
                {
                    Tools.LogBug("Unhandle Mapping of Value=" + _key);
                    ret = value;
                }
            }
            catch
            {
                Tools.LogBug("Unable to GetDescription for key=" + value);
                ret = value;
            }
            return ret;
        }
        public int RecordIndex { get; set; }
        public string FieldId { get; private set; }
        public string Description
        {
            get 
            {
                string prefix = string.Empty;
                // 5/1/2009 dd - Record Index is currently record Application Index.
                if (RecordIndex > 0)
                {
                    prefix = "[App #" + RecordIndex + "] ";
                }
                return prefix + FieldDescription.GetDescription(FieldId); 
            }
        }

        public string Category
        {
            get { return FieldDescription.GetCategory(FieldId); }
        }

        private string MaskSsn(string ssn)
        {
            if (string.IsNullOrEmpty(ssn))
            {
                return "";
            }
            if (ssn.Length >= 11)
            {
                return "***-**-" + ssn.Substring(7);
            }
            return ssn;
        }
        private string GetDescription_sProdPpmtPenaltyMon(string value)
        {
            switch (value)
            {
                case "0": return "No PP";
                case "12": return "1 yr";
                case "24": return "2 yrs";
                case "36": return "3 yrs";
                case "60": return "5 yrs";
                default:
                    return value;
            }
        }
        private string GetDescription_sProdAvailReserveMonths(string value)
        {
            switch (value)
            {
                case "-1": return "Leave Blank";
                case "0": return "0 month";
                case "1": return "1 month";
                case "2": return "2 months";
                case "3": return "3 months";
                case "4": return "4 months";
                case "5": return "5 months";
                case "6": return "6 months";
                case "8": return "8 months";
                case "9": return "9 months";
                case "12": return "12 months";
                case "14": return "14 months";
                case "18": return "18 months";
                case "24": return "24 months";
                case "36": return "36 months";
                case "48": return "48+ months";
                default:
                    return value;
            }
        }
        private string GetValue(string raw)
        {
            switch (FieldId)
            {
                case "sProdMIOptionT": return GetDescription(raw, sProdMIOptionT_Map);
                case "aProdBCitizenT": return GetDescription(raw, aProdCitizenT_Map);
                case "aProdCCitizenT": return GetDescription(raw, aProdCitizenT_Map);
                case "sOccTPe": return GetDescription(raw, sOccT_Map);
                case "sProdSpT": return GetDescription(raw, sProdSpT_Map);
                case "sProdSpStructureT": return GetDescription(raw, sProdSpStructureT_Map);
                case "sLPurposeTPe": return GetDescription(raw, sLPurposeT_Map);
                case "sProdDocT": return GetDescription(raw, GetsProdDocT_Map());
                case "sProdFinMethFilterT": return GetDescription(raw, sProdFinMethFilterT_Map);
                case "sProd3rdPartyUwResultT": return GetDescription(raw, sProd3rdPartyUwResultT_Map);
                case "sProd3rdPartyUwProcessingT": return GetDescription(raw, sProd3rdPartyUwProcessingT_Map);
                case "sProdCrManualForeclosureRecentStatusT": return GetDescription(raw, sProdCrManualDerogRecentStatusT_Map);
                case "sProdCrManualBk7RecentStatusT": return GetDescription(raw, sProdCrManualDerogRecentStatusT_Map);
                case "sProdCrManualBk13RecentStatusT": return GetDescription(raw, sProdCrManualDerogRecentStatusT_Map);
                case "aBSsn": return MaskSsn(raw);
                case "aCSsn": return MaskSsn(raw);
                case "sProdPpmtPenaltyMon": return GetDescription_sProdPpmtPenaltyMon(raw);
                case "sProdAvailReserveMonths": return GetDescription_sProdAvailReserveMonths(raw);
                default:
                    return raw;
            }
        }
        public string OldValue {
            get
            {
                if (FieldId == "aLiaCollection")
                {
                    return GetLiaCollectionOldValue();
                }
                else if (FieldId == "aPublicRecordXmlContent")
                {
                    return "Please see public record list.";
                }
                else if (FieldId == "aCreditReportFileDbKey")
                {
                    if (m_rawNewValue == Guid.Empty.ToString())
                    {
                        return "Credit report has been removed from this file.";
                    }
                    else
                    {
                        return "Credit report was re-issued. Please verify credit report.";
                    }
                }
                else if (FieldId == "aAppId")
                {
                    switch (FieldTracKInfoTypeT)
                    {
                        case E_FieldTrackInfoTypeT.New: return m_rawNewValue + " is added.";
                        case E_FieldTrackInfoTypeT.Obsolete: return m_rawOldValue + " is removed.";
                        case E_FieldTrackInfoTypeT.Normal: return string.Empty;
                        default:
                            throw new CBaseException(ErrorMessages.Generic, "Unhandled FieldTrackInfoTypeT=" + FieldTracKInfoTypeT);
                    }
                }
                else
                {
                    return GetValue(m_rawOldValue);
                }
            }
        }
        public string NewValue
        {
            get
            {
                switch (FieldId) 
                {
                    case "aLiaCollection": return GetLiaCollectionNewValue();
                    case "aPublicRecordXmlContent":
                    case "aCreditReportFileDbKey":
                    case "aAppId":
                        return string.Empty; // These are special fields and description only return in OldValue.
                    default:
                        return GetValue(m_rawNewValue);
                }
            }
        }
        private string GetLiaCollectionOldValue()
        {
            if (this.FieldTracKInfoTypeT == E_FieldTrackInfoTypeT.New)
            {
                return m_rawNewValue + " is added.";
            }
            else if (this.FieldTracKInfoTypeT == E_FieldTrackInfoTypeT.Obsolete)
            {
                return m_rawOldValue + " is removed.";
            }
            else
            {
                string ret = "";
                foreach (var o in List)
                {
                    ret += string.Format("{0}={1}{2}", FieldDescription.GetDescription(o.FieldId), o.OldValue, Environment.NewLine);
                }
                return ret;
            }
        }
        private string GetLiaCollectionNewValue()
        {
            if (this.FieldTracKInfoTypeT == E_FieldTrackInfoTypeT.New || this.FieldTracKInfoTypeT == E_FieldTrackInfoTypeT.Obsolete)
            {
                return string.Empty;
            }
            else
            {
                string ret = "";
                foreach (var o in List)
                {
                    ret += string.Format("{0}={1}{2}", FieldDescription.GetDescription(o.FieldId), o.NewValue, Environment.NewLine);
                }
                return ret;
            }
        }

        private string m_rawOldValue = string.Empty;
        private string m_rawNewValue = string.Empty;

        public E_FieldTrackInfoTypeT FieldTracKInfoTypeT { get; private set; }

        public FieldTrackInfo(string fieldId, string oldValue, string newValue, E_FieldTrackInfoTypeT fieldTrackInfoTypeT, int recordIndex)
        {
            FieldTracKInfoTypeT = fieldTrackInfoTypeT;
            FieldId = fieldId;
            m_rawOldValue = oldValue;
            m_rawNewValue = newValue;
            RecordIndex = recordIndex;
        }

        private List<FieldTrackInfo> m_list = new List<FieldTrackInfo>();
        public List<FieldTrackInfo> List
        {
            get { return m_list; }
        }
    }

}
