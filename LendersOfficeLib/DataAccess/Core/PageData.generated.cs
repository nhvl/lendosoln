﻿namespace DataAccess
{
    using System;
    using System.Data;
    using System.Linq;
    using System.Collections.Generic;
    using LendersOffice.ObjLib.CustomAttributes;
    using LendingQB.Core;
    using LendingQB.Core.Data;
    using LendingQB.Core.Commands;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// The auto-generated portion of CPageData.
    /// </summary>
    public partial class CPageData
    {
        /// <summary>
        /// Gets the Collection of Asset records.
        /// </summary>
        public IReadOnlyOrderedLqbCollection<DataObjectKind.Asset, Guid, Asset> Assets => this.m_pageBase.Assets;

        /// <summary>
        /// Gets the Collection of CounselingEvent records.
        /// </summary>
        public IReadOnlyOrderedLqbCollection<DataObjectKind.CounselingEvent, Guid, CounselingEvent> CounselingEvents => this.m_pageBase.CounselingEvents;

        /// <summary>
        /// Gets the Collection of EmploymentRecord records.
        /// </summary>
        public IReadOnlyOrderedLqbCollection<DataObjectKind.EmploymentRecord, Guid, EmploymentRecord> EmploymentRecords => this.m_pageBase.EmploymentRecords;

        /// <summary>
        /// Gets the Collection of IncomeSource records.
        /// </summary>
        public IReadOnlyOrderedLqbCollection<DataObjectKind.IncomeSource, Guid, IncomeSource> IncomeSources => this.m_pageBase.IncomeSources;

        /// <summary>
        /// Gets the Collection of HousingHistoryEntry records.
        /// </summary>
        public IReadOnlyLqbCollection<DataObjectKind.HousingHistoryEntry, Guid, HousingHistoryEntry> HousingHistoryEntries => this.m_pageBase.HousingHistoryEntries;

        /// <summary>
        /// Gets the Collection of Liability records.
        /// </summary>
        public IReadOnlyOrderedLqbCollection<DataObjectKind.Liability, Guid, Liability> Liabilities => this.m_pageBase.Liabilities;

        /// <summary>
        /// Gets the Collection of PublicRecord records.
        /// </summary>
        public IReadOnlyOrderedLqbCollection<DataObjectKind.PublicRecord, Guid, PublicRecord> PublicRecords => this.m_pageBase.PublicRecords;

        /// <summary>
        /// Gets the Collection of RealProperty records.
        /// </summary>
        public IReadOnlyOrderedLqbCollection<DataObjectKind.RealProperty, Guid, RealProperty> RealProperties => this.m_pageBase.RealProperties;

        /// <summary>
        /// Gets the Collection of UladApplication records.
        /// </summary>
        public IReadOnlyOrderedLqbCollection<DataObjectKind.UladApplication, Guid, UladApplication> UladApplications => this.m_pageBase.UladApplications;

        /// <summary>
        /// Gets the Collection of VaPreviousLoan records.
        /// </summary>
        public IReadOnlyOrderedLqbCollection<DataObjectKind.VaPreviousLoan, Guid, VaPreviousLoan> VaPreviousLoans => this.m_pageBase.VaPreviousLoans;

        /// <summary>
        /// Gets the Collection of VorRecord records.
        /// </summary>
        public IReadOnlyOrderedLqbCollection<DataObjectKind.VorRecord, Guid, VorRecord> VorRecords => this.m_pageBase.VorRecords;

        /// <summary>
        /// Gets the association of Consumers to Assets.
        /// </summary>
        public IReadOnlyLqbAssociationSet<DataObjectKind.ConsumerAssetAssociation, Guid, ConsumerAssetAssociation> ConsumerAssets => this.m_pageBase.ConsumerAssets;

        /// <summary>
        /// Gets the association of Consumers to IncomeSources.
        /// </summary>
        public IReadOnlyLqbAssociationSet<DataObjectKind.ConsumerIncomeSourceAssociation, Guid, ConsumerIncomeSourceAssociation> ConsumerIncomeSources => this.m_pageBase.ConsumerIncomeSources;

        /// <summary>
        /// Gets the association of Consumers to Liabilities.
        /// </summary>
        public IReadOnlyLqbAssociationSet<DataObjectKind.ConsumerLiabilityAssociation, Guid, ConsumerLiabilityAssociation> ConsumerLiabilities => this.m_pageBase.ConsumerLiabilities;

        /// <summary>
        /// Gets the association of Consumers to PublicRecords.
        /// </summary>
        public IReadOnlyLqbAssociationSet<DataObjectKind.ConsumerPublicRecordAssociation, Guid, ConsumerPublicRecordAssociation> ConsumerPublicRecords => this.m_pageBase.ConsumerPublicRecords;

        /// <summary>
        /// Gets the association of Consumers to RealProperties.
        /// </summary>
        public IReadOnlyLqbAssociationSet<DataObjectKind.ConsumerRealPropertyAssociation, Guid, ConsumerRealPropertyAssociation> ConsumerRealProperties => this.m_pageBase.ConsumerRealProperties;

        /// <summary>
        /// Gets the association of Consumers to VaPreviousLoans.
        /// </summary>
        public IReadOnlyLqbAssociationSet<DataObjectKind.ConsumerVaPreviousLoanAssociation, Guid, ConsumerVaPreviousLoanAssociation> ConsumerVaPreviousLoans => this.m_pageBase.ConsumerVaPreviousLoans;

        /// <summary>
        /// Gets the association of Consumers to VorRecords.
        /// </summary>
        public IReadOnlyLqbAssociationSet<DataObjectKind.ConsumerVorRecordAssociation, Guid, ConsumerVorRecordAssociation> ConsumerVorRecords => this.m_pageBase.ConsumerVorRecords;

        /// <summary>
        /// Gets the association of Consumers to CounselingEvents.
        /// </summary>
        public IReadOnlyLqbAssociationSet<DataObjectKind.CounselingEventAttendance, Guid, CounselingEventAttendance> CounselingEventAttendances => this.m_pageBase.CounselingEventAttendances;

        /// <summary>
        /// Gets the association of IncomeSources to EmploymentRecords.
        /// </summary>
        public IReadOnlyLqbAssociationSet<DataObjectKind.IncomeSourceEmploymentRecordAssociation, Guid, IncomeSourceEmploymentRecordAssociation> IncomeSourceEmploymentRecords => this.m_pageBase.IncomeSourceEmploymentRecords;

        /// <summary>
        /// Gets the association of RealProperties to Liabilities.
        /// </summary>
        public IReadOnlyLqbAssociationSet<DataObjectKind.RealPropertyLiabilityAssociation, Guid, RealPropertyLiabilityAssociation> RealPropertyLiabilities => this.m_pageBase.RealPropertyLiabilities;

        /// <summary>
        /// Gets the association of UladApplications to Consumers.
        /// </summary>
        public IReadOnlyLqbAssociationSet<DataObjectKind.UladApplicationConsumerAssociation, Guid, UladApplicationConsumerAssociation> UladApplicationConsumers => this.m_pageBase.UladApplicationConsumers;

        public IEnumerable<KeyValuePair<DataObjectIdentifier<DataObjectKind.Asset, Guid>, Asset>> GetAssetsForConsumer(DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId)
        {
            return this.m_pageBase.GetAssetsForConsumer(consumerId);
        }

        public DataObjectIdentifier<DataObjectKind.Asset, Guid> AddAsset(DataObjectIdentifier<DataObjectKind.Consumer, Guid> ownerId,  Asset asset)
        {
            return this.m_pageBase.AddAsset(ownerId, asset);
        }

        public void RemoveAsset(DataObjectIdentifier<DataObjectKind.Asset, Guid> id)
        {
            this.m_pageBase.RemoveAsset(id);
        }

        public void ClearAssets()
        {
            this.m_pageBase.ClearAssets();
        }

        public IEnumerable<KeyValuePair<DataObjectIdentifier<DataObjectKind.CounselingEvent, Guid>, CounselingEvent>> GetCounselingEventsForConsumer(DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId)
        {
            return this.m_pageBase.GetCounselingEventsForConsumer(consumerId);
        }

        public DataObjectIdentifier<DataObjectKind.CounselingEvent, Guid> AddCounselingEvent(DataObjectIdentifier<DataObjectKind.Consumer, Guid> ownerId,  CounselingEvent counselingEvent)
        {
            return this.m_pageBase.AddCounselingEvent(ownerId, counselingEvent);
        }

        public void RemoveCounselingEvent(DataObjectIdentifier<DataObjectKind.CounselingEvent, Guid> id)
        {
            this.m_pageBase.RemoveCounselingEvent(id);
        }

        public void ClearCounselingEvents()
        {
            this.m_pageBase.ClearCounselingEvents();
        }

        public IEmploymentRecordDefaultsProvider CreateEmploymentRecordDefaultsProvider() => this.m_pageBase.CreateEmploymentRecordDefaultsProvider();

        public IEnumerable<KeyValuePair<DataObjectIdentifier<DataObjectKind.EmploymentRecord, Guid>, EmploymentRecord>> GetEmploymentRecordsForConsumer(DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId)
        {
            return this.m_pageBase.GetEmploymentRecordsForConsumer(consumerId);
        }

        public DataObjectIdentifier<DataObjectKind.EmploymentRecord, Guid> AddEmploymentRecord(DataObjectIdentifier<DataObjectKind.Consumer, Guid> ownerId,  EmploymentRecord employmentRecord)
        {
            return this.m_pageBase.AddEmploymentRecord(ownerId, employmentRecord);
        }

        public void RemoveEmploymentRecord(DataObjectIdentifier<DataObjectKind.EmploymentRecord, Guid> id)
        {
            this.m_pageBase.RemoveEmploymentRecord(id);
        }

        public void ClearEmploymentRecords()
        {
            this.m_pageBase.ClearEmploymentRecords();
        }

        public IEnumerable<KeyValuePair<DataObjectIdentifier<DataObjectKind.IncomeSource, Guid>, IncomeSource>> GetIncomeSourcesForConsumer(DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId)
        {
            return this.m_pageBase.GetIncomeSourcesForConsumer(consumerId);
        }

        public DataObjectIdentifier<DataObjectKind.IncomeSource, Guid> AddIncomeSource(DataObjectIdentifier<DataObjectKind.Consumer, Guid> ownerId,  IncomeSource incomeSource)
        {
            return this.m_pageBase.AddIncomeSource(ownerId, incomeSource);
        }

        public void RemoveIncomeSource(DataObjectIdentifier<DataObjectKind.IncomeSource, Guid> id)
        {
            this.m_pageBase.RemoveIncomeSource(id);
        }

        public void ClearIncomeSources()
        {
            this.m_pageBase.ClearIncomeSources();
        }

        public IEnumerable<KeyValuePair<DataObjectIdentifier<DataObjectKind.HousingHistoryEntry, Guid>, HousingHistoryEntry>> GetHousingHistoryEntriesForConsumer(DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId)
        {
            return this.m_pageBase.GetHousingHistoryEntriesForConsumer(consumerId);
        }

        public DataObjectIdentifier<DataObjectKind.HousingHistoryEntry, Guid> AddHousingHistoryEntry(DataObjectIdentifier<DataObjectKind.Consumer, Guid> ownerId,  HousingHistoryEntry housingHistoryEntry)
        {
            return this.m_pageBase.AddHousingHistoryEntry(ownerId, housingHistoryEntry);
        }

        public void RemoveHousingHistoryEntry(DataObjectIdentifier<DataObjectKind.HousingHistoryEntry, Guid> id)
        {
            this.m_pageBase.RemoveHousingHistoryEntry(id);
        }

        public void ClearHousingHistoryEntries()
        {
            this.m_pageBase.ClearHousingHistoryEntries();
        }

        public ILiabilityDefaultsProvider CreateLiabilityDefaultsProvider() => this.m_pageBase.CreateLiabilityDefaultsProvider();

        public IEnumerable<KeyValuePair<DataObjectIdentifier<DataObjectKind.Liability, Guid>, Liability>> GetLiabilitiesForConsumer(DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId)
        {
            return this.m_pageBase.GetLiabilitiesForConsumer(consumerId);
        }

        public DataObjectIdentifier<DataObjectKind.Liability, Guid> AddLiability(DataObjectIdentifier<DataObjectKind.Consumer, Guid> ownerId,  Liability liability)
        {
            return this.m_pageBase.AddLiability(ownerId, liability);
        }

        public void RemoveLiability(DataObjectIdentifier<DataObjectKind.Liability, Guid> id)
        {
            this.m_pageBase.RemoveLiability(id);
        }

        public void ClearLiabilities()
        {
            this.m_pageBase.ClearLiabilities();
        }

        public IEnumerable<KeyValuePair<DataObjectIdentifier<DataObjectKind.PublicRecord, Guid>, PublicRecord>> GetPublicRecordsForConsumer(DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId)
        {
            return this.m_pageBase.GetPublicRecordsForConsumer(consumerId);
        }

        public DataObjectIdentifier<DataObjectKind.PublicRecord, Guid> AddPublicRecord(DataObjectIdentifier<DataObjectKind.Consumer, Guid> ownerId,  PublicRecord publicRecord)
        {
            return this.m_pageBase.AddPublicRecord(ownerId, publicRecord);
        }

        public void RemovePublicRecord(DataObjectIdentifier<DataObjectKind.PublicRecord, Guid> id)
        {
            this.m_pageBase.RemovePublicRecord(id);
        }

        public void ClearPublicRecords()
        {
            this.m_pageBase.ClearPublicRecords();
        }

        public IEnumerable<KeyValuePair<DataObjectIdentifier<DataObjectKind.RealProperty, Guid>, RealProperty>> GetRealPropertiesForConsumer(DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId)
        {
            return this.m_pageBase.GetRealPropertiesForConsumer(consumerId);
        }

        public DataObjectIdentifier<DataObjectKind.RealProperty, Guid> AddRealProperty(DataObjectIdentifier<DataObjectKind.Consumer, Guid> ownerId,  RealProperty realProperty)
        {
            return this.m_pageBase.AddRealProperty(ownerId, realProperty);
        }

        public void RemoveRealProperty(DataObjectIdentifier<DataObjectKind.RealProperty, Guid> id)
        {
            this.m_pageBase.RemoveRealProperty(id);
        }

        public void ClearRealProperties()
        {
            this.m_pageBase.ClearRealProperties();
        }

        public IEnumerable<KeyValuePair<DataObjectIdentifier<DataObjectKind.VaPreviousLoan, Guid>, VaPreviousLoan>> GetVaPreviousLoansForConsumer(DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId)
        {
            return this.m_pageBase.GetVaPreviousLoansForConsumer(consumerId);
        }

        public DataObjectIdentifier<DataObjectKind.VaPreviousLoan, Guid> AddVaPreviousLoan(DataObjectIdentifier<DataObjectKind.Consumer, Guid> ownerId,  VaPreviousLoan vaPreviousLoan)
        {
            return this.m_pageBase.AddVaPreviousLoan(ownerId, vaPreviousLoan);
        }

        public void RemoveVaPreviousLoan(DataObjectIdentifier<DataObjectKind.VaPreviousLoan, Guid> id)
        {
            this.m_pageBase.RemoveVaPreviousLoan(id);
        }

        public void ClearVaPreviousLoans()
        {
            this.m_pageBase.ClearVaPreviousLoans();
        }

        public IEnumerable<KeyValuePair<DataObjectIdentifier<DataObjectKind.VorRecord, Guid>, VorRecord>> GetVorRecordsForConsumer(DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId)
        {
            return this.m_pageBase.GetVorRecordsForConsumer(consumerId);
        }

        public DataObjectIdentifier<DataObjectKind.VorRecord, Guid> AddVorRecord(DataObjectIdentifier<DataObjectKind.Consumer, Guid> ownerId,  VorRecord vorRecord)
        {
            return this.m_pageBase.AddVorRecord(ownerId, vorRecord);
        }

        public void RemoveVorRecord(DataObjectIdentifier<DataObjectKind.VorRecord, Guid> id)
        {
            this.m_pageBase.RemoveVorRecord(id);
        }

        public void ClearVorRecords()
        {
            this.m_pageBase.ClearVorRecords();
        }

        public DataObjectIdentifier<DataObjectKind.ConsumerAssetAssociation, Guid> AddAssetOwnership(
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> ownerId,
            DataObjectIdentifier<DataObjectKind.Asset, Guid> recordId)
        {
            return this.m_pageBase.AddAssetOwnership(ownerId, recordId);
        }

        public void SetAssetOwnership(
            DataObjectIdentifier<DataObjectKind.Asset, Guid> recordId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> primaryOwnerId,
            IEnumerable<DataObjectIdentifier<DataObjectKind.Consumer, Guid>> additionalOwnerIds)
        {
            this.m_pageBase.SetAssetOwnership(recordId, primaryOwnerId, additionalOwnerIds);
        }

        public DataObjectIdentifier<DataObjectKind.ConsumerIncomeSourceAssociation, Guid> AddIncomeSourceOwnership(
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> ownerId,
            DataObjectIdentifier<DataObjectKind.IncomeSource, Guid> recordId)
        {
            return this.m_pageBase.AddIncomeSourceOwnership(ownerId, recordId);
        }

        public void SetIncomeSourceOwnership(
            DataObjectIdentifier<DataObjectKind.IncomeSource, Guid> recordId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> primaryOwnerId,
            IEnumerable<DataObjectIdentifier<DataObjectKind.Consumer, Guid>> additionalOwnerIds)
        {
            this.m_pageBase.SetIncomeSourceOwnership(recordId, primaryOwnerId, additionalOwnerIds);
        }

        public DataObjectIdentifier<DataObjectKind.ConsumerLiabilityAssociation, Guid> AddLiabilityOwnership(
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> ownerId,
            DataObjectIdentifier<DataObjectKind.Liability, Guid> recordId)
        {
            return this.m_pageBase.AddLiabilityOwnership(ownerId, recordId);
        }

        public void SetLiabilityOwnership(
            DataObjectIdentifier<DataObjectKind.Liability, Guid> recordId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> primaryOwnerId,
            IEnumerable<DataObjectIdentifier<DataObjectKind.Consumer, Guid>> additionalOwnerIds)
        {
            this.m_pageBase.SetLiabilityOwnership(recordId, primaryOwnerId, additionalOwnerIds);
        }

        public DataObjectIdentifier<DataObjectKind.ConsumerPublicRecordAssociation, Guid> AddPublicRecordOwnership(
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> ownerId,
            DataObjectIdentifier<DataObjectKind.PublicRecord, Guid> recordId)
        {
            return this.m_pageBase.AddPublicRecordOwnership(ownerId, recordId);
        }

        public void SetPublicRecordOwnership(
            DataObjectIdentifier<DataObjectKind.PublicRecord, Guid> recordId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> primaryOwnerId,
            IEnumerable<DataObjectIdentifier<DataObjectKind.Consumer, Guid>> additionalOwnerIds)
        {
            this.m_pageBase.SetPublicRecordOwnership(recordId, primaryOwnerId, additionalOwnerIds);
        }

        public DataObjectIdentifier<DataObjectKind.ConsumerRealPropertyAssociation, Guid> AddRealPropertyOwnership(
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> ownerId,
            DataObjectIdentifier<DataObjectKind.RealProperty, Guid> recordId)
        {
            return this.m_pageBase.AddRealPropertyOwnership(ownerId, recordId);
        }

        public void SetRealPropertyOwnership(
            DataObjectIdentifier<DataObjectKind.RealProperty, Guid> recordId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> primaryOwnerId,
            IEnumerable<DataObjectIdentifier<DataObjectKind.Consumer, Guid>> additionalOwnerIds)
        {
            this.m_pageBase.SetRealPropertyOwnership(recordId, primaryOwnerId, additionalOwnerIds);
        }

        public DataObjectIdentifier<DataObjectKind.ConsumerVaPreviousLoanAssociation, Guid> AddVaPreviousLoanOwnership(
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> ownerId,
            DataObjectIdentifier<DataObjectKind.VaPreviousLoan, Guid> recordId)
        {
            return this.m_pageBase.AddVaPreviousLoanOwnership(ownerId, recordId);
        }

        public void SetVaPreviousLoanOwnership(
            DataObjectIdentifier<DataObjectKind.VaPreviousLoan, Guid> recordId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> primaryOwnerId,
            IEnumerable<DataObjectIdentifier<DataObjectKind.Consumer, Guid>> additionalOwnerIds)
        {
            this.m_pageBase.SetVaPreviousLoanOwnership(recordId, primaryOwnerId, additionalOwnerIds);
        }

        public DataObjectIdentifier<DataObjectKind.ConsumerVorRecordAssociation, Guid> AddVorRecordOwnership(
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> ownerId,
            DataObjectIdentifier<DataObjectKind.VorRecord, Guid> recordId)
        {
            return this.m_pageBase.AddVorRecordOwnership(ownerId, recordId);
        }

        public void SetVorRecordOwnership(
            DataObjectIdentifier<DataObjectKind.VorRecord, Guid> recordId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> primaryOwnerId,
            IEnumerable<DataObjectIdentifier<DataObjectKind.Consumer, Guid>> additionalOwnerIds)
        {
            this.m_pageBase.SetVorRecordOwnership(recordId, primaryOwnerId, additionalOwnerIds);
        }

        public DataObjectIdentifier<DataObjectKind.CounselingEventAttendance, Guid> AddCounselingEventOwnership(
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> ownerId,
            DataObjectIdentifier<DataObjectKind.CounselingEvent, Guid> recordId)
        {
            return this.m_pageBase.AddCounselingEventOwnership(ownerId, recordId);
        }

        public void SetCounselingEventOwnership(
            DataObjectIdentifier<DataObjectKind.CounselingEvent, Guid> recordId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> primaryOwnerId,
            IEnumerable<DataObjectIdentifier<DataObjectKind.Consumer, Guid>> additionalOwnerIds)
        {
            this.m_pageBase.SetCounselingEventOwnership(recordId, primaryOwnerId, additionalOwnerIds);
        }

        public DataObjectIdentifier<DataObjectKind.IncomeSourceEmploymentRecordAssociation, Guid> AddIncomeSourceEmploymentRecordAssociation(IncomeSourceEmploymentRecordAssociation association)
        {
            return this.m_pageBase.AddIncomeSourceEmploymentRecordAssociation(association);
        }

        public DataObjectIdentifier<DataObjectKind.RealPropertyLiabilityAssociation, Guid> AddRealPropertyLiabilityAssociation(RealPropertyLiabilityAssociation association)
        {
            return this.m_pageBase.AddRealPropertyLiabilityAssociation(association);
        }

        /// <summary>
        /// Maps the new data layer collection names to the legacy collection name used by workflow.
        /// Only contains new data layer collections where the associated legacy collection supported
        /// workflow.
        /// </summary>
        private static readonly IReadOnlyDictionary<string, IEnumerable<string>> UladCollectionNameToLegacyWorkflowCollectionName = new Dictionary<string, IEnumerable<string>>()
        {
            ["Assets"] = new[] { "CAssetCollection" },
            ["EmploymentRecords"] = new[] { "CEmpCollection" },
            ["Liabilities"] = new[] { "CLiaCollection" },
            ["RealProperties"] = new[] { "CReCollection" },
            ["VaPreviousLoans"] = new[] { "CVaPastLCollection" },
            ["VorRecords"] = new[] { "CVorFields" },
            ["ConsumerAssets"] = new[] { "CAssetCollection" },
            ["ConsumerEmployments"] = new[] { "CEmpCollection" },
            ["ConsumerLiabilities"] = new[] { "CLiaCollection" },
            ["ConsumerRealProperties"] = new[] { "CReCollection" },
            ["ConsumerVaPreviousLoans"] = new[] { "CVaPastLCollection" },
            ["ConsumerVorRecords"] = new[] { "CVorFields" },
            ["IncomeSourceEmploymentRecords"] = new[] { "CEmpCollection" },
            ["RealPropertyLiabilities"] = new[] { "CReCollection", "CLiaCollection" },
        };

        protected virtual DataSet GetLoanLevelDataSetForCollection(string collectionName)
        {
            switch (collectionName)
            {
                case "CAssetCollection":
                    return this.GetLoanLevelAssetsDataSet();
                case "CEmpCollection":
                    return this.GetLoanLevelEmploymentRecordsDataSet();
                case "CLiaCollection":
                    return this.GetLoanLevelLiabilitiesDataSet();
                case "CReCollection":
                    return this.GetLoanLevelRealPropertiesDataSet();
                case "CVaPastLCollection":
                    return this.GetLoanLevelVaPreviousLoansDataSet();
                case "CVorFields":
                    return this.GetLoanLevelVorRecordsDataSet();
                default:
                    throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Unhandled collection: " + collectionName);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the specified collection name was found.
        /// Sets the out parameter to the collection value.
        /// </summary>
        /// <param name="collectionName">The collection name.</param>
        /// <param name="collection">The collection.</param>
        /// <returns>True if the collection was found. False otherwise.</returns>
        private bool TryGetGeneratedCollection(string collectionName, out object collection)
        {
            switch (collectionName.ToLower())
            {
                case "assets": 
                    collection = this.Assets;
                    return true;
                case "counselingevents": 
                    collection = this.CounselingEvents;
                    return true;
                case "employmentrecords": 
                    collection = this.EmploymentRecords;
                    return true;
                case "incomesources": 
                    collection = this.IncomeSources;
                    return true;
                case "housinghistoryentries": 
                    collection = this.HousingHistoryEntries;
                    return true;
                case "liabilities": 
                    collection = this.Liabilities;
                    return true;
                case "publicrecords": 
                    collection = this.PublicRecords;
                    return true;
                case "realproperties": 
                    collection = this.RealProperties;
                    return true;
                case "uladapplications": 
                    collection = this.UladApplications;
                    return true;
                case "vapreviousloans": 
                    collection = this.VaPreviousLoans;
                    return true;
                case "vorrecords": 
                    collection = this.VorRecords;
                    return true;
                case "consumerassets": 
                    collection = this.ConsumerAssets;
                    return true;
                case "consumerincomesources": 
                    collection = this.ConsumerIncomeSources;
                    return true;
                case "consumerliabilities": 
                    collection = this.ConsumerLiabilities;
                    return true;
                case "consumerpublicrecords": 
                    collection = this.ConsumerPublicRecords;
                    return true;
                case "consumerrealproperties": 
                    collection = this.ConsumerRealProperties;
                    return true;
                case "consumervapreviousloans": 
                    collection = this.ConsumerVaPreviousLoans;
                    return true;
                case "consumervorrecords": 
                    collection = this.ConsumerVorRecords;
                    return true;
                case "counselingeventattendances": 
                    collection = this.CounselingEventAttendances;
                    return true;
                case "incomesourceemploymentrecords": 
                    collection = this.IncomeSourceEmploymentRecords;
                    return true;
                case "realpropertyliabilities": 
                    collection = this.RealPropertyLiabilities;
                    return true;
                case "uladapplicationconsumers": 
                    collection = this.UladApplicationConsumers;
                    return true;
            }

            collection = null;
            return false;
        }

        private DataSet GetLoanLevelAssetsDataSet()
        {
            var appLevelCollections = this.m_pageBase.Apps.Select(a => a.aAssetCollection);

            foreach (var appLevelCollection in appLevelCollections)
            {
                appLevelCollection.EnsureDataSetUpToDate();
            }

            var appLevelDataSets = appLevelCollections.Select(c => c.GetDataSet().Copy());

            var loanLevelDataSet = appLevelDataSets.First();
            foreach (var additionalAppLevelDataSet in appLevelDataSets.Skip(1))
            {
                loanLevelDataSet.Merge(additionalAppLevelDataSet);
            }

            return loanLevelDataSet;
        }

        private DataSet GetLoanLevelEmploymentRecordsDataSet()
        {
            var appLevelCollections = this.m_pageBase.Apps.Select(a => a.aBEmpCollection);
            appLevelCollections = appLevelCollections.Concat(this.m_pageBase.Apps.Select(a => a.aCEmpCollection));

            foreach (var appLevelCollection in appLevelCollections)
            {
                appLevelCollection.EnsureDataSetUpToDate();
            }

            var appLevelDataSets = appLevelCollections.Select(c => c.GetDataSet().Copy());

            var loanLevelDataSet = appLevelDataSets.First();
            foreach (var additionalAppLevelDataSet in appLevelDataSets.Skip(1))
            {
                loanLevelDataSet.Merge(additionalAppLevelDataSet);
            }

            return loanLevelDataSet;
        }

        private DataSet GetLoanLevelLiabilitiesDataSet()
        {
            var appLevelCollections = this.m_pageBase.Apps.Select(a => a.aLiaCollection);

            foreach (var appLevelCollection in appLevelCollections)
            {
                appLevelCollection.EnsureDataSetUpToDate();
            }

            var appLevelDataSets = appLevelCollections.Select(c => c.GetDataSet().Copy());

            var loanLevelDataSet = appLevelDataSets.First();
            foreach (var additionalAppLevelDataSet in appLevelDataSets.Skip(1))
            {
                loanLevelDataSet.Merge(additionalAppLevelDataSet);
            }

            return loanLevelDataSet;
        }

        private DataSet GetLoanLevelRealPropertiesDataSet()
        {
            var appLevelCollections = this.m_pageBase.Apps.Select(a => a.aReCollection);

            foreach (var appLevelCollection in appLevelCollections)
            {
                appLevelCollection.EnsureDataSetUpToDate();
            }

            var appLevelDataSets = appLevelCollections.Select(c => c.GetDataSet().Copy());

            var loanLevelDataSet = appLevelDataSets.First();
            foreach (var additionalAppLevelDataSet in appLevelDataSets.Skip(1))
            {
                loanLevelDataSet.Merge(additionalAppLevelDataSet);
            }

            return loanLevelDataSet;
        }

        private DataSet GetLoanLevelVaPreviousLoansDataSet()
        {
            var appLevelCollections = this.m_pageBase.Apps.Select(a => a.aVaPastLCollection);

            foreach (var appLevelCollection in appLevelCollections)
            {
                appLevelCollection.EnsureDataSetUpToDate();
            }

            var appLevelDataSets = appLevelCollections.Select(c => c.GetDataSet().Copy());

            var loanLevelDataSet = appLevelDataSets.First();
            foreach (var additionalAppLevelDataSet in appLevelDataSets.Skip(1))
            {
                loanLevelDataSet.Merge(additionalAppLevelDataSet);
            }

            return loanLevelDataSet;
        }

        private DataSet GetLoanLevelVorRecordsDataSet()
        {
            var appLevelCollections = this.m_pageBase.Apps.Select(a => a.aVorCollection);

            foreach (var appLevelCollection in appLevelCollections)
            {
                appLevelCollection.EnsureDataSetUpToDate();
            }

            var appLevelDataSets = appLevelCollections.Select(c => c.GetDataSet().Copy());

            var loanLevelDataSet = appLevelDataSets.First();
            foreach (var additionalAppLevelDataSet in appLevelDataSets.Skip(1))
            {
                loanLevelDataSet.Merge(additionalAppLevelDataSet);
            }

            return loanLevelDataSet;
        }
    }
}
