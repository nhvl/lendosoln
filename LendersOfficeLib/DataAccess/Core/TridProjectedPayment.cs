﻿// <copyright file="TridProjectedPayment.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Douglas Telfer
//    Date:   6/17/2015
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LendersOffice.Common;
    using Newtonsoft.Json;

    /// <summary>
    /// Container for columns in the Projected Payments table on the TRID disclosures.
    /// Objects are intended to be immutable--all data points should be passed in to the constructor.
    /// </summary>
    [Serializable]
    [JsonObject(MemberSerialization.OptIn)]
    public class TridProjectedPayment
    {
        /// <summary>
        /// The maximum numbers of payments that will be produced.
        /// </summary>
        private const int MaxPayments = 4;

        /// <summary>
        /// This is used by the <code>_rep</code> fields to convert data to the desired format.
        /// </summary>
        private LosConvert formatConverter;

        /// <summary>
        /// Initializes a new instance of the <see cref="TridProjectedPayment" /> class when there a range of payments needs to be disclosed.
        /// </summary>
        /// <param name="startYear">The starting year for the projected payment.</param>
        /// <param name="endYear">The ending year for the projected payment.</param>
        /// <param name="minPI">The minimum principal and interest payment in the range.</param>
        /// <param name="maxPI">The maximum principal and interest payment in the range.</param>
        /// <param name="mi">The monthly mortgage insurance payment. Use 0 if there is none.</param>
        /// <param name="escrow">The monthly escrow payment, excluding mortgage insurance. Use 0 if there is none.</param>
        /// <param name="hasIO">Whether or not the projected payment includes payments that are interest-only.</param>
        /// <param name="isFinalBalloonPayment">Whether or not the projected payment is the final balloon payment.</param>
        /// <param name="converter">The <see cref="LosConvert"/> object to use when referencing payment data.</param>
        public TridProjectedPayment(int startYear, int endYear, decimal minPI, decimal maxPI, decimal mi, decimal escrow, bool hasIO, bool isFinalBalloonPayment, LosConvert converter)
        {
            this.StartYear = startYear;
            this.EndYear = endYear;
            this.PIMin = minPI;
            this.PIMax = maxPI;
            this.MI = mi;
            this.Escrow = escrow;
            this.HasInterestOnly = hasIO;
            this.IsFinalBalloonPayment = isFinalBalloonPayment;
            this.formatConverter = converter ?? new LosConvert();
        }

        /// <summary>
        /// Gets or sets a value indicating whether the money rep fields should
        /// be included in serialization.
        /// </summary>
        /// <value>
        /// False by default. If set to true, money field reps will be included
        /// in serialization.
        /// </value>
        public bool SerializeMoneyRepFields { get; set; }

        /// <summary>
        /// Gets the starting year of the projected payment.
        /// </summary>
        /// <value>The starting year of the projected payment.</value>
        [JsonProperty]
        public int StartYear { get; private set; }

        /// <summary>
        /// Gets the starting year of the projected payment.
        /// </summary>
        /// <value>The starting year of the projected payment.</value>
        public string StartYear_rep
        {
            get
            {
                return this.formatConverter.ToCountString(this.StartYear);
            }
        }

        /// <summary>
        /// Gets the ending year of the projected payment.
        /// </summary>
        /// <value>The ending year of the projected payment.</value>
        [JsonProperty]
        public int EndYear { get; private set; }

        /// <summary>
        /// Gets the ending year of the projected payment.
        /// </summary>
        /// <value>The ending year of the projected payment.</value>
        public string EndYear_rep
        {
            get
            {
                return this.formatConverter.ToCountString(this.EndYear);
            }
        }

        /// <summary>
        /// Gets the minimum principal and interest payment.
        /// Is the same as PIMax if there is not a payment range.
        /// </summary>
        /// <value>The minimum principal and interest payment.</value>
        [JsonProperty]
        public decimal PIMin { get; private set; }

        /// <summary>
        /// Gets the minimum principal and interest payment.
        /// Is the same as PIMax if there is not a payment range.
        /// </summary>
        /// <value>The minimum principal and interest payment.</value>
        [JsonProperty]
        public string PIMin_rep
        {
            get
            {
                return this.formatConverter.ToMoneyString(this.PIMin, FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// Gets the maximum principal and interest payment.
        /// Is the same as PIMin if there is not a payment range.
        /// </summary>
        /// <value>The maximum principal and interest payment.</value>
        [JsonProperty]
        public decimal PIMax { get; private set; }

        /// <summary>
        /// Gets the maximum principal and interest payment.
        /// Is the same as PIMin if there is not a payment range.
        /// </summary>
        /// <value>The maximum principal and interest payment.</value>
        [JsonProperty]
        public string PIMax_rep
        {
            get
            {
                return this.formatConverter.ToMoneyString(this.PIMax, FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// Gets the MI payment amount.
        /// </summary>
        /// <value>The MI payment amount.</value>
        [JsonProperty]
        public decimal MI { get; private set; }

        /// <summary>
        /// Gets the MI payment amount.
        /// </summary>
        /// <value>The MI payment amount.</value>
        [JsonProperty]
        public string MI_rep
        {
            get
            {
                return this.formatConverter.ToMoneyString(this.MI, FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// Gets the monthly escrow payment amount (excluding MI).
        /// </summary>
        /// <value>The monthly escrow payment amount (excluding MI).</value>
        [JsonProperty]
        public decimal Escrow { get; private set; }

        /// <summary>
        /// Gets the monthly escrow payment amount (excluding MI).
        /// </summary>
        /// <value>The monthly escrow payment amount (excluding MI).</value>
        [JsonProperty]
        public string Escrow_rep
        {
            get
            {
                return this.formatConverter.ToMoneyString(this.Escrow, FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// Gets a value indicating whether or not the projected payment includes an interest-only payment.
        /// </summary>
        /// <value>Whether or not the projected payment includes an interest-only payment.</value>
        [JsonProperty]
        public bool HasInterestOnly { get; private set; }

        /// <summary>
        /// Gets a value indicating whether or not the projected payment is the final balloon payment.
        /// </summary>
        /// <value>Whether or not the projected payment is the final balloon payment.</value>
        [JsonProperty]
        public bool IsFinalBalloonPayment { get; private set; }

        /// <summary>
        /// Gets the minimum total monthly payment.
        /// </summary>
        /// <value>The minimum total monthly payment.</value>
        public decimal PaymentMin
        {
            get { return this.PIMin + this.MI + this.Escrow; }
        }

        /// <summary>
        /// Gets the minimum total monthly payment.
        /// </summary>
        /// <value>The minimum total monthly payment.</value>
        [JsonProperty]
        public string PaymentMin_rep
        {
            get
            {
                return this.formatConverter.ToMoneyString(this.PaymentMin, FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// Gets the maximum total monthly payment.
        /// </summary>
        /// <value>The maximum total monthly payment.</value>
        public decimal PaymentMax
        {
            get { return this.PIMax + this.MI + this.Escrow; }
        }

        /// <summary>
        /// Gets the maximum total monthly payment.
        /// </summary>
        /// <value>The maximum total monthly payment.</value>
        [JsonProperty]
        public string PaymentMax_rep
        {
            get
            {
                return this.formatConverter.ToMoneyString(this.PaymentMax, FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// Returns a string representation of the amount range using a custom formatting string.
        /// If there is not a range (i.e. the min and max amounts are equal), returns just the amount string.
        /// </summary>
        /// <param name="rangeFormatStr">The String.Format format string for the range. Needs exactly 2 string parameters.</param>
        /// <param name="min_rep">The string representation of the minimum amount.</param>
        /// <param name="max_rep">The string representation of the maximum amount.</param>
        /// <returns>The string representation of the amount range using the custom formatting string passed.</returns>
        public static string FormattedRange(string rangeFormatStr, string min_rep, string max_rep)
        {
            if (min_rep.Equals(max_rep))
            {
                return min_rep;
            }
            else
            {
                return string.Format(rangeFormatStr, min_rep, max_rep);
            }
        }

        /// <summary>
        /// Returns the <code>TridProjectedPayment</code> list that would be produced from the inputs.
        /// </summary>
        /// <param name="amortTable">The ungrouped amortization table using the current index.</param>
        /// <param name="bestCase">The ungrouped amortization table assuming the worst-case index value.</param>
        /// <param name="worstCase">The ungrouped amortization table assuming the best-case index value.</param>
        /// <param name="escrowPmtNoMi">The monthly escrow impounds payment, exclusive of MI.</param>
        /// <param name="converter">The <see cref="LosConvert"/> object to use when referencing payment data.</param>
        /// <param name="offset">The number of months to offset the index, primarily for combined construction loans.</param>
        /// <returns>The <code>TridProjectedPayment</code> list that would be produced from the inputs.</returns>
        public static List<TridProjectedPayment> GenerateProjectedPayments(AmortTable amortTable, AmortTable bestCase, AmortTable worstCase, decimal escrowPmtNoMi, LosConvert converter, int offset = 0)
        {
            if (amortTable == null ||
                bestCase == null ||
                worstCase == null)
            {
                return null;
            }

            if (amortTable.IsGrouped || bestCase.IsGrouped || worstCase.IsGrouped)
            {
                Tools.Assert(false, " TridProjectedPayment.GenerateProjectedPayments requires ungrouped amortization tables.");
                return null;
            }

            if (amortTable.IncludesPrepayment || bestCase.IncludesPrepayment || worstCase.IncludesPrepayment)
            {
                Tools.Assert(false, " TridProjectedPayment.GenerateProjectedPayments cannot use amortizations that include prepayment of principal.");
                return null;
            }

            List<TridProjectedPayment> tridPaymentList = new List<TridProjectedPayment>();

            // Gather the changes from the amortTable
            // * The primary outputs of this process
            int yearOfMITermination = 0;
            int yearOfInterestOnlyExpiration = 0;
            int finalYear = (amortTable.Items.Length + 11) / 12;
            bool hasFinalBalloonPmt = false;
            List<PeriodOfPIChange> initialPeriodsOfPIChange = new List<PeriodOfPIChange>(MaxPayments);

            // * But not this
            bool isNewPeriodNeededNextYear = false;

            // * GO!!
            foreach (AmortItem amortItem in amortTable.Items)
            {
                int index = amortItem.Index + offset;

                int currentYear = (index / 12) + 1;
                int nextFullYear = ((index + 11) / 12) + 1; // The current year if we are at the year boundary, otherwise the following year.

                if (index == 0)
                {
                    // ^ Always include the first payment
                    initialPeriodsOfPIChange.Add(new PeriodOfPIChange(startYear: currentYear, endYear: finalYear) { IsFirstPeriod = true });
                    continue;
                }

                bool hasReportableChange =
                    (amortItem.HasChangeEvent(E_AmortChangeEventType.RateChange) ||
                     amortItem.HasChangeEvent(E_AmortChangeEventType.IOExpired) ||
                     amortItem.HasChangeEvent(E_AmortChangeEventType.PIChange)) &&
                    (bestCase.Items[index - 1].PaymentWOMI > bestCase.Items[index].PaymentWOMI ||
                     worstCase.Items[index - 1].PaymentWOMI < worstCase.Items[index].PaymentWOMI);

                if (index % 12 == 0 && (isNewPeriodNeededNextYear || hasReportableChange))
                {
                    // ^ If we are not already working on the last possible period...
                    if (initialPeriodsOfPIChange.Count < MaxPayments)
                    {
                        // ^ If there is enough room to add another record, add it.
                        initialPeriodsOfPIChange.Last().EndYear = currentYear - 1;

                        initialPeriodsOfPIChange.Add(new PeriodOfPIChange(startYear: currentYear, endYear: finalYear));
                    }
                    else
                    {
                        // ^ Else if we are out of room, mark the existing last record as combined.
                        initialPeriodsOfPIChange.Last().IsCombined = true;
                    }

                    isNewPeriodNeededNextYear = false;
                }
                else
                {
                    // ^ Else this is a mid-year period.
                    if (hasReportableChange)
                    {
                        isNewPeriodNeededNextYear = true;
                    }
                }

                // Credit any changes to the current period.
                if (hasReportableChange)
                {
                    initialPeriodsOfPIChange.Last().NumberOfChanges++;
                }

                if (amortItem.HasChangeEvent(E_AmortChangeEventType.IndexChange))
                {
                    initialPeriodsOfPIChange.Last().HasArmIndexChange = true;
                }

                // Capture the first (and hopefully only) IO expiration month
                if (yearOfInterestOnlyExpiration == 0 && amortItem.HasChangeEvent(E_AmortChangeEventType.IOExpired))
                {
                    yearOfInterestOnlyExpiration = nextFullYear; // Per regulations, if IO expires mid-year, start reporting it in the next full year.
                }

                if (amortItem.HasChangeEvent(E_AmortChangeEventType.MITermination))
                {
                    // If we don't already have an MI termination year, stash it.
                    if (yearOfMITermination == 0)
                    {
                        yearOfMITermination = nextFullYear; // Per regulation, if MI terminates mid-year, start reporting it in the next full year.
                    }
                }

                if (amortItem.HasChangeEvent(E_AmortChangeEventType.FinalBalloonPmt))
                {
                    hasFinalBalloonPmt = true;
                }
            }

            // Add and/or remove periods to fit in the available space, per the priorities set by the regulation.
            List<PeriodOfPIChange> finalPeriodsOfChanges = new List<PeriodOfPIChange>(MaxPayments);
            int availablePIChanges = MaxPayments; // The number of slots available for P&I changes

            // * Cut PI change events if needed to include the final balloon payment
            if (hasFinalBalloonPmt)
            {
                availablePIChanges--;

                if (availablePIChanges < initialPeriodsOfPIChange.Count)
                {
                    initialPeriodsOfPIChange.RemoveRange(availablePIChanges, initialPeriodsOfPIChange.Count - availablePIChanges);
                    initialPeriodsOfPIChange.Last().EndYear = finalYear; // Fix up the end year for the newly designated last period.
                    initialPeriodsOfPIChange.Last().IsCombined = true; // We just combined changes together into the last one.
                }
            }

            // * See if there is enough room to insert the MI Term event
            bool hasEnoughRoomForMITerm = initialPeriodsOfPIChange.Count < availablePIChanges;

            // * Copy the PI changes to the new list, inserting the MI Term if there is room and it is needed.
            foreach (PeriodOfPIChange change in initialPeriodsOfPIChange)
            {
                finalPeriodsOfChanges.Add(change);

                // If there is room, and this is the appropriate place, insert the MI termination year.
                if (hasEnoughRoomForMITerm && change.StartYear < yearOfMITermination && yearOfMITermination < change.EndYear)
                {
                    // Reset the end year of the prior output record and insert the new MI change record.
                    finalPeriodsOfChanges.Add(new PeriodOfPIChange(startYear: yearOfMITermination, endYear: change.EndYear));
                    change.EndYear = yearOfMITermination - 1;
                }
            }

            // Generate the TridProjectedPaymentList from the reduced year set
            foreach (PeriodOfPIChange currentPeriodInfo in finalPeriodsOfChanges)
            {
                int yearStart = currentPeriodInfo.StartYear;
                int yearEnd = currentPeriodInfo.EndYear;

                tridPaymentList.Add(
                    new TridProjectedPayment(
                        yearStart,
                        yearEnd,
                        GetPIMinForYears(bestCase, yearStart, yearEnd, currentPeriodInfo.IsIncludeRange),
                        GetPIMaxForYears(worstCase, yearStart, yearEnd, currentPeriodInfo.IsIncludeRange),
                        GetMaxMIForYear(amortTable, yearStart, yearEnd),
                        escrowPmtNoMi,
                        yearStart < yearOfInterestOnlyExpiration,
                        false,
                        converter));
            }

            // Tack the final balloon payment on the end if there is one
            if (hasFinalBalloonPmt)
            {
                int finalPmtIdx = amortTable.Items.Length - 1;
                tridPaymentList.Add(
                    new TridProjectedPayment(
                        0,
                        0,
                        amortTable.Items[finalPmtIdx].PaymentWOMI,
                        amortTable.Items[finalPmtIdx].PaymentWOMI,
                        0.00M, // Overriding this to 0 for now to match samples.
                        0.00M,
                        false,
                        true,
                        converter));
            }

            return tridPaymentList;
        }

        /// <summary>
        /// Load the projected payments from the given <code>json</code> string.
        /// </summary>
        /// <param name="projectedPaymentsJsonContent">A <code>json</code> string containing the projected payments from a loan estimate or closing disclosure archive.</param>
        /// <param name="converter">The <see cref="LosConvert" /> object to be used when serializing the payments.</param>
        /// <returns>A list of projected payments.</returns>
        public static List<TridProjectedPayment> GetProjectedPaymentsFromArchive(string projectedPaymentsJsonContent, LosConvert converter)
        {
            List<TridProjectedPayment> payments = SerializationHelper.JsonNetDeserialize<List<TridProjectedPayment>>(projectedPaymentsJsonContent);

            foreach (TridProjectedPayment payment in payments ?? new List<TridProjectedPayment>())
            {
                payment.formatConverter = converter;
            }

            return payments;
        }

        /// <summary>
        /// Returns a string representation of the object.
        /// </summary>
        /// <returns>A string representation of the object.</returns>
        public override string ToString()
        {
            return
                "TridProjectedPayment:" + Environment.NewLine +
                "  StartYear = " + this.StartYear + Environment.NewLine +
                "  EndYear = " + this.EndYear + Environment.NewLine +
                "  PIMin = " + this.PIMin + Environment.NewLine +
                "  PIMax = " + this.PIMax + Environment.NewLine +
                "  MI = " + this.MI + Environment.NewLine +
                "  Escrow = " + this.Escrow + Environment.NewLine +
                "  PaymentMin = " + this.PaymentMin + Environment.NewLine +
                "  PaymentMax = " + this.PaymentMax + Environment.NewLine +
                "  HasIO = " + this.HasInterestOnly + Environment.NewLine +
                "  IsFinalBalloonPayment = " + this.IsFinalBalloonPayment;
        }

        /// <summary>
        /// Determine whether the PIMin_rep field should be serialized by JSON.NET.
        /// </summary>
        /// <returns>True if the property should be serialized. Otherwise, false.</returns>
        public bool ShouldSerializePIMin_rep()
        {
            return this.SerializeMoneyRepFields;
        }

        /// <summary>
        /// Determine whether the PIMax_rep field should be serialized by JSON.NET.
        /// </summary>
        /// <returns>True if the property should be serialized. Otherwise, false.</returns>
        public bool ShouldSerializePIMax_rep()
        {
            return this.SerializeMoneyRepFields;
        }

        /// <summary>
        /// Determine whether the MI_rep field should be serialized by JSON.NET.
        /// </summary>
        /// <returns>True if the property should be serialized. Otherwise, false.</returns>
        public bool ShouldSerializeMI_rep()
        {
            return this.SerializeMoneyRepFields;
        }

        /// <summary>
        /// Determine whether the Escrow_rep field should be serialized by JSON.NET.
        /// </summary>
        /// <returns>True if the property should be serialized. Otherwise, false.</returns>
        public bool ShouldSerializeEscrow_rep()
        {
            return this.SerializeMoneyRepFields;
        }

        /// <summary>
        /// Determine whether the PaymentMin_rep field should be serialized by JSON.NET.
        /// </summary>
        /// <returns>True if the property should be serialized. Otherwise, false.</returns>
        public bool ShouldSerializePaymentMin_rep()
        {
            return this.SerializeMoneyRepFields;
        }

        /// <summary>
        /// Determine whether the PaymentMax_rep field should be serialized by JSON.NET.
        /// </summary>
        /// <returns>True if the property should be serialized. Otherwise, false.</returns>
        public bool ShouldSerializePaymentMax_rep()
        {
            return this.SerializeMoneyRepFields;
        }

        /// <summary>
        /// Returns the index of the first month of a given year.
        /// </summary>
        /// <param name="year">The given year.</param>
        /// <returns>The index of the first month.</returns>
        private static int GetMonthFromYear(int year)
        {
            return (year - 1) * 12;
        }

        /// <summary>
        /// The minimum principal and interest payment between two years.
        /// </summary>
        /// <param name="bestCase">The amortization table to use (HINT: should be the best case).</param>
        /// <param name="startYear">The start year.</param>
        /// <param name="endYear">The end year.</param>
        /// <param name="isIncludeRange">If true, iterate over the period. If false, use the first month.</param>
        /// <returns>The minimum principal and interest payment.</returns>
        private static decimal GetPIMinForYears(AmortTable bestCase, int startYear, int endYear, bool isIncludeRange)
        {
            if (startYear > endYear)
            {
                Tools.LogBug("GetPIMinForYear called with a startYear > endYear. This should not happen.");
                return 0;
            }

            int startMonth = GetMonthFromYear(startYear);
            int endMonth = GetMonthFromYear(endYear + 1);

            decimal minPI = bestCase.Items[startMonth].PaymentWOMI;

            if (!isIncludeRange)
            {
                return minPI;
            }

            for (int idx = startMonth; idx < endMonth && idx < bestCase.Items.Length - 1; ++idx)
            {
                if (!bestCase.Items[idx].HasChangeEvent(E_AmortChangeEventType.FinalBalloonPmt))
                {
                    minPI = System.Math.Min(minPI, bestCase.Items[idx].PaymentWOMI);
                }
            }

            return minPI;
        }

        /// <summary>
        /// The maximum principal and interest payment between two years.
        /// </summary>
        /// <param name="worstCase">The amortization table to use (HINT: should be the worst case).</param>
        /// <param name="startYear">The start year.</param>
        /// <param name="endYear">The end year.</param>
        /// <param name="isIncludeRange">If true, iterate over the period. If false, use the first month.</param>
        /// <returns>The maximum principal and interest payment.</returns>
        private static decimal GetPIMaxForYears(AmortTable worstCase, int startYear, int endYear, bool isIncludeRange)
        {
            if (startYear > endYear)
            {
                Tools.LogBug("GetPIMaxForYear called with a startYear > endYear. This should not happen.");
                return 0;
            }

            int startMonth = GetMonthFromYear(startYear);
            int endMonth = GetMonthFromYear(endYear + 1);

            decimal maxPI = worstCase.Items[startMonth].PaymentWOMI;

            if (!isIncludeRange)
            {
                return maxPI;
            }

            for (int idx = startMonth + 1; idx < endMonth && idx < worstCase.Items.Length - 1; ++idx)
            {
                if (!worstCase.Items[idx].HasChangeEvent(E_AmortChangeEventType.FinalBalloonPmt))
                {
                    maxPI = System.Math.Max(maxPI, worstCase.Items[idx].PaymentWOMI);
                }
            }

            return maxPI;
        }

        /// <summary>
        /// The maximum mortgage insurance payment between two years (exclusive of the ending year).
        /// </summary>
        /// <param name="worstCase">The amortization table to use (HINT: should be the worst case).</param>
        /// <param name="startYear">The start year.</param>
        /// <param name="endYear">The end year.</param>
        /// <returns>The maximum mortgage insurance payment.</returns>
        private static decimal GetMaxMIForYear(AmortTable worstCase, int startYear, int endYear)
        {
            if (startYear > endYear)
            {
                Tools.LogBug("GetMaxMIForYear called with a startYear > endYear. This should not happen.");
                return 0;
            }

            int startMonth = GetMonthFromYear(startYear);
            int endMonth = GetMonthFromYear(endYear + 1);

            decimal maxMI = worstCase.Items[startMonth].MI;

            for (int idx = startMonth + 1; idx < endMonth && idx < worstCase.Items.Length - 1; ++idx)
            {
                if (!worstCase.Items[idx].HasChangeEvent(E_AmortChangeEventType.FinalBalloonPmt))
                {
                    maxMI = System.Math.Max(maxMI, worstCase.Items[idx].MI);
                }
            }

            return maxMI;
        }

        /// <summary>
        /// Encapsulates data about the year where a P and I payment change happened.
        /// </summary>
        private class PeriodOfPIChange
        {
            /// <summary>
            /// Initializes a new instance of the PeriodOfPIChange class.
            /// </summary>
            /// <param name="startYear">The year the period starts.</param>
            /// <param name="endYear">The year the period ends.</param>
            public PeriodOfPIChange(int startYear, int endYear)
            {
                this.StartYear = startYear;
                this.EndYear = endYear;
            }

            /// <summary>
            /// Gets or sets the number of the year that the period starts.
            /// </summary>
            public int StartYear { get; set; }

            /// <summary>
            /// Gets or sets the number of the year that the period ends.
            /// </summary>
            public int EndYear { get; set; }

            /// <summary>
            /// Gets or sets the number of changes that occurred during the period.
            /// </summary>
            public int NumberOfChanges { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether or not there were ARM index changes during the period.
            /// </summary>
            public bool HasArmIndexChange { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether or not this is the record for the first period.
            /// </summary>
            public bool IsFirstPeriod { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether or not this is the record for a combination of multiple periods.
            /// </summary>
            public bool IsCombined { get; set; }

            /// <summary>
            /// Gets a value indicating whether or not the year of change should disclose a range of payments.
            /// </summary>
            public bool IsIncludeRange
            {
                get { return this.IsFirstPeriod || this.IsCombined || this.HasArmIndexChange || this.NumberOfChanges > 1; }
            }
        }
    }
}