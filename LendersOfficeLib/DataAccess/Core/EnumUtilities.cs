// <copyright file="EnumUtilities.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
// Author: huyn
// Date: 9/3/2015
// </summary>

namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.Caching;
    using System.Text.RegularExpressions;
    using CommonProjectLib.Common.Lib;
    using LendersOffice.Migration;
    using LendersOffice.ObjLib.DocMagicLib;
    using LendersOffice.ObjLib.Hmda;
    using LendersOffice.Security;

    /// <summary>
    ///  This class is a tool for parsing enumerated type.
    /// </summary>
    public static class EnumUtilities
    {
        /// <summary>
        /// List of states of US.
        /// </summary>
        private static List<string> states;

        /// <summary>
        /// List of name suffixes.
        /// </summary>
        private static string[] suffixOptionArray = new string[] { "SR.", "JR.", "I", "II", "III", "IV" };

        /// <summary>
        /// Initializes static members of the EnumUtilities class. Update US states.
        /// </summary>
        static EnumUtilities()
        {
            states = new List<string>(50);
            states.Add("AL");
            states.Add("AK");
            states.Add("AZ");
            states.Add("AR");
            states.Add("CA");
            states.Add("CO");
            states.Add("CT");
            states.Add("DE");
            states.Add("DC");
            states.Add("FL");
            states.Add("GA");
            states.Add("HI");
            states.Add("ID");
            states.Add("IL");
            states.Add("IN");
            states.Add("IA");
            states.Add("KS");
            states.Add("KY");
            states.Add("LA");
            states.Add("ME");
            states.Add("MD");
            states.Add("MA");
            states.Add("MI");
            states.Add("MN");
            states.Add("MS");
            states.Add("MO");
            states.Add("MT");
            states.Add("NE");
            states.Add("NV");
            states.Add("NH");
            states.Add("NJ");
            states.Add("NM");
            states.Add("NY");
            states.Add("NC");
            states.Add("ND");
            states.Add("OH");
            states.Add("OK");
            states.Add("OR");
            states.Add("PA");
            states.Add("RI");
            states.Add("SC");
            states.Add("SD");
            states.Add("TN");
            states.Add("TX");
            states.Add("UT");
            states.Add("VT");
            states.Add("VA");
            states.Add("WA");
            states.Add("WV");
            states.Add("WI");
            states.Add("WY");
        }

        /// <summary>
        /// Return enum values of a loan field.
        /// </summary>
        /// <param name="fieldId">Field id name.</param>
        /// <param name="principal">Abstract user principal.</param>
        /// <param name="loanData">Data of current loan.</param>
        /// <returns>A list of key pair (key,value) of the loan field.
        /// Return null if field id is not enum type.
        /// </returns>
        public static List<KeyValuePair<string, string>> GetValuesFromField(string fieldId, AbstractUserPrincipal principal, CPageData loanData = null)
        {
            var keyValsPairs = GetValuesFromSpecialField(fieldId, principal, loanData);

            if (keyValsPairs != null)
            {
                return keyValsPairs.ToList();
            }

            ObjectCache cache = MemoryCache.Default;

            if (cache[fieldId] == null)
            {
                if (PageDataUtilities.ContainsField(fieldId) == true)
                {
                    PageDataFieldInfo pageDataFieldInfo = PageDataUtilities.GetFieldInfo(fieldId);

                    if (pageDataFieldInfo.FieldType == E_PageDataFieldType.Enum)
                    {
                        keyValsPairs = GetValuesFromType(pageDataFieldInfo.EnumeratedType);
                    }
                }

                if (keyValsPairs == null)
                {
                    return null;
                }

                cache[fieldId] = keyValsPairs;
            }

            return cache[fieldId] as List<KeyValuePair<string, string>>;
        }

        /// <summary>
        /// Return enum values of a loan field.
        /// </summary>
        /// <param name="fieldId">Field id name.</param>
        /// <returns>A list of key pair (key,value) of the loan field.
        /// Return null if field id is not enum type.
        /// </returns>
        public static IEnumerable<Enum> GetEnumValuesFromField(string fieldId)
        {
            ObjectCache cache = MemoryCache.Default;

            if (cache[fieldId] == null)
            {
                IEnumerable<Enum> keyValsPairs = null;

                if (PageDataUtilities.ContainsField(fieldId) == true)
                {
                    PageDataFieldInfo pageDataFieldInfo = PageDataUtilities.GetFieldInfo(fieldId);

                    if (pageDataFieldInfo.FieldType == E_PageDataFieldType.Enum)
                    {
                        keyValsPairs = GetEnumValuesFromType(pageDataFieldInfo.EnumeratedType);
                    }
                }

                if (keyValsPairs == null)
                {
                    return null;
                }

                cache[fieldId] = keyValsPairs;
            }

            return cache[fieldId] as IEnumerable<Enum>;
        }

        /// <summary>
        /// Return enum values of a special field, which can get from reflection.
        /// </summary>
        /// <param name="fieldId">Field id name.</param>
        /// <param name="principal">Abstract User Principal.</param>
        /// <param name="loanData">Data of current loan.</param>
        /// <returns>A list of key pair (key,value) of the loan field.
        /// Return null if field id is not a special one.
        /// </returns>
        public static IEnumerable<KeyValuePair<string, string>> GetValuesFromSpecialField(string fieldId, AbstractUserPrincipal principal, CPageData loanData = null)
        {
            List<KeyValuePair<string, string>> keyValPairs = new List<KeyValuePair<string, string>>();
            if ((new string[] { "sProHazInsT", "sProRealETxT" }).Contains(fieldId))
            {
                keyValPairs.Add(KeyValuePairFromEnum(E_PercentBaseT.LoanAmount));
                keyValPairs.Add(KeyValuePairFromEnum(E_PercentBaseT.SalesPrice));
                keyValPairs.Add(KeyValuePairFromEnum(E_PercentBaseT.AppraisalValue));
                keyValPairs.Add(KeyValuePairFromEnum(E_PercentBaseT.TotalLoanAmount));
                return keyValPairs;
            }
            else if (fieldId == "sProMInsT")
            {
                if (loanData != null && loanData.sLT == E_sLT.FHA)
                {
                    keyValPairs.Add(KeyValuePairFromEnum(E_PercentBaseT.AverageOutstandingBalance));
                    keyValPairs.Add(KeyValuePairFromEnum(E_PercentBaseT.LoanAmount));
                }
                else
                {
                    keyValPairs.Add(KeyValuePairFromEnum(E_PercentBaseT.AppraisalValue));
                    keyValPairs.Add(KeyValuePairFromEnum(E_PercentBaseT.AverageOutstandingBalance));
                    keyValPairs.Add(KeyValuePairFromEnum(E_PercentBaseT.LoanAmount));
                    keyValPairs.Add(KeyValuePairFromEnum(E_PercentBaseT.SalesPrice));
                    keyValPairs.Add(KeyValuePairFromEnum(E_PercentBaseT.TotalLoanAmount));
                    keyValPairs.Add(KeyValuePairFromEnum(E_PercentBaseT.DecliningRenewalsAnnually));
                    keyValPairs.Add(KeyValuePairFromEnum(E_PercentBaseT.DecliningRenewalsMonthly));
                }

                return keyValPairs;
            }
            else if (fieldId == nameof(CPageData.sProMInsT))
            {
                keyValPairs.Add(KeyValuePairFromEnum(E_PercentBaseT.AppraisalValue));
                keyValPairs.Add(KeyValuePairFromEnum(E_PercentBaseT.AverageOutstandingBalance));
                keyValPairs.Add(KeyValuePairFromEnum(E_PercentBaseT.LoanAmount));
                keyValPairs.Add(KeyValuePairFromEnum(E_PercentBaseT.SalesPrice));
                keyValPairs.Add(KeyValuePairFromEnum(E_PercentBaseT.TotalLoanAmount));
                keyValPairs.Add(KeyValuePairFromEnum(E_PercentBaseT.DecliningRenewalsMonthly));
                keyValPairs.Add(KeyValuePairFromEnum(E_PercentBaseT.DecliningRenewalsAnnually));

                return keyValPairs;
            }
            else if (fieldId == "aManner")
            {
                return Tools.Options_aManner.Select(o => new KeyValuePair<string, string>(o, null)).ToList();
            }
            else if (fieldId == "sDwnPmtSrc")
            {
                return Tools.Options_sDwnPmtSrc.Select(o => new KeyValuePair<string, string>(o, null)).ToList();
            }
            else if (fieldId == "sLeadSrcDesc")
            {
                keyValPairs.Add(new KeyValuePair<string, string>(string.Empty, null));
                foreach (var desc in BrokerUserPrincipal.CurrentPrincipal.BrokerDB.LeadSources)
                {
                    keyValPairs.Add(new KeyValuePair<string, string>(desc.Name, null));
                }

                return keyValPairs;
            }
            else if (fieldId == "sLPurposeT")
            {
                var options = EnumUtilities.GetValuesFromType(typeof(E_sLPurposeT));

                if (!principal.BrokerDB.IsEnableHomeEquityInLoanPurpose)
                {
                    options = options.Where(x => x.Key != E_sLPurposeT.HomeEquity.ToString("D")).ToList();
                }

                return options;
            }
            else if (fieldId == "sDocMagicAlternateLenderId")
            {
                keyValPairs.Add(new KeyValuePair<string, string>(Guid.Empty.ToString(), string.Empty));
                List<Tuple<string, Guid>> alternateLenderList = DocMagicAlternateLender.GetAlternateLenderListByBroker(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
                keyValPairs.AddRange(alternateLenderList.Select(lender => new KeyValuePair<string, string>(lender.Item2.ToString(), lender.Item1)));
                return keyValPairs;
            }
            else if (fieldId == "sTitleBorrowers.State" || fieldId == "state" || fieldId == "aBStateMail")
            {
                foreach (string state in states)
                {
                    keyValPairs.Add(new KeyValuePair<string, string>(state, null));
                }

                return keyValPairs;
            }
            else if (fieldId.EndsWith("County"))
            {
                var stateFieldId = fieldId.Replace("County", "State");
                if (!PageDataUtilities.ContainsField(stateFieldId))
                {
                    return null;
                }

                var stateValue = PageDataUtilities.GetValue(loanData, null, stateFieldId);
                var counties = StateInfo.Instance.GetCountiesFor(stateValue);
                return counties.Cast<string>().Select(x => new KeyValuePair<string, string>(x, null)).ToList();
            }
            else if (Regex.Match(fieldId, @"sOCredit\dDesc").Success)
            {
                keyValPairs.Add(new KeyValuePair<string, string>("Cash Deposit on sales contract", null));
                keyValPairs.Add(new KeyValuePair<string, string>("Seller Credit", null));
                keyValPairs.Add(new KeyValuePair<string, string>("Lender Credit", null));
                keyValPairs.Add(new KeyValuePair<string, string>("Relocation Funds", null));
                keyValPairs.Add(new KeyValuePair<string, string>("Employer Assisted Housing", null));
                keyValPairs.Add(new KeyValuePair<string, string>("Lease Purchase Fund", null));
                keyValPairs.Add(new KeyValuePair<string, string>("Borrower Paid Fees", null));
                keyValPairs.Add(new KeyValuePair<string, string>("Paid Outside of Closing", null));
                keyValPairs.Add(new KeyValuePair<string, string>("Broker Credit", null));
                return keyValPairs;
            }
            else if (fieldId == "sLpDocClass")
            {
                keyValPairs.Add(new KeyValuePair<string, string>("Accept Plus", null));
                keyValPairs.Add(new KeyValuePair<string, string>("Streamlined Accept", null));
                keyValPairs.Add(new KeyValuePair<string, string>("Standard", null));
                keyValPairs.Add(new KeyValuePair<string, string>("Accept", null));
                keyValPairs.Add(new KeyValuePair<string, string>("Refer", null));
                return keyValPairs;
            }
            else if (fieldId == "sHmdaActionTaken")
            {
                // Note: This returns new string values by default if loanData is null.
                if (loanData == null || LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(loanData.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement))
                {
                    keyValPairs.Add(new KeyValuePair<string, string>(Tools.MapHmdaActionTakenToString(HmdaActionTaken.Blank), null));
                    keyValPairs.Add(new KeyValuePair<string, string>(Tools.MapHmdaActionTakenToString(HmdaActionTaken.LoanOriginated), null));
                    keyValPairs.Add(new KeyValuePair<string, string>(Tools.MapHmdaActionTakenToString(HmdaActionTaken.ApplicationApprovedButNotAccepted), null));
                    keyValPairs.Add(new KeyValuePair<string, string>(Tools.MapHmdaActionTakenToString(HmdaActionTaken.ApplicationDenied), null));
                    keyValPairs.Add(new KeyValuePair<string, string>(Tools.MapHmdaActionTakenToString(HmdaActionTaken.ApplicationWithdrawnByApplicant), null));
                    keyValPairs.Add(new KeyValuePair<string, string>(Tools.MapHmdaActionTakenToString(HmdaActionTaken.FileClosedForIncompleteness), null));
                    keyValPairs.Add(new KeyValuePair<string, string>(Tools.MapHmdaActionTakenToString(HmdaActionTaken.PurchasedLoan), null));
                    keyValPairs.Add(new KeyValuePair<string, string>(Tools.MapHmdaActionTakenToString(HmdaActionTaken.PreapprovalRequestDenied), null));
                    keyValPairs.Add(new KeyValuePair<string, string>(Tools.MapHmdaActionTakenToString(HmdaActionTaken.PreapprovalRequestApprovedButNotAccepted), null));
                }
                else
                {
                    keyValPairs.Add(new KeyValuePair<string, string>("Loan originated", null));
                    keyValPairs.Add(new KeyValuePair<string, string>("App approved but not accepted by applicant", null));
                    keyValPairs.Add(new KeyValuePair<string, string>("Application denied", null));
                    keyValPairs.Add(new KeyValuePair<string, string>("Application withdrawn", null));
                    keyValPairs.Add(new KeyValuePair<string, string>("File closed for incompleteness", null));
                    keyValPairs.Add(new KeyValuePair<string, string>("Loan purchased by your institution", null));
                    keyValPairs.Add(new KeyValuePair<string, string>("Preapproval request denied", null));
                    keyValPairs.Add(new KeyValuePair<string, string>("Preapproval request approved but not accepted", null));
                }
                
                return keyValPairs;
            }
            else if (Regex.Match(fieldId, @"sHmdaDenialReason\d").Success)
            {
                // Note: This returns new string values by default if loanData is null.
                if (loanData == null || LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(loanData.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement))
                {
                    keyValPairs.Add(new KeyValuePair<string, string>(Tools.MapHmdaDenialReasonToString(HmdaDenialReason.Blank), null));
                    keyValPairs.Add(new KeyValuePair<string, string>(Tools.MapHmdaDenialReasonToString(HmdaDenialReason.DebtToIncomeRatio), null));
                    keyValPairs.Add(new KeyValuePair<string, string>(Tools.MapHmdaDenialReasonToString(HmdaDenialReason.EmploymentHistory), null));
                    keyValPairs.Add(new KeyValuePair<string, string>(Tools.MapHmdaDenialReasonToString(HmdaDenialReason.CreditHistory), null));
                    keyValPairs.Add(new KeyValuePair<string, string>(Tools.MapHmdaDenialReasonToString(HmdaDenialReason.Collateral), null));
                    keyValPairs.Add(new KeyValuePair<string, string>(Tools.MapHmdaDenialReasonToString(HmdaDenialReason.InsufficientCash), null));
                    keyValPairs.Add(new KeyValuePair<string, string>(Tools.MapHmdaDenialReasonToString(HmdaDenialReason.UnverifiableInformation), null));
                    keyValPairs.Add(new KeyValuePair<string, string>(Tools.MapHmdaDenialReasonToString(HmdaDenialReason.CreditApplicationIncomplete), null));
                    keyValPairs.Add(new KeyValuePair<string, string>(Tools.MapHmdaDenialReasonToString(HmdaDenialReason.MortgageInsuranceDenied), null));
                    keyValPairs.Add(new KeyValuePair<string, string>(Tools.MapHmdaDenialReasonToString(HmdaDenialReason.Other), null));
                    keyValPairs.Add(new KeyValuePair<string, string>(Tools.MapHmdaDenialReasonToString(HmdaDenialReason.NotApplicable), null));
                }
                else
                {
                    keyValPairs.Add(new KeyValuePair<string, string>("1 Debt-to-income ratio", null));
                    keyValPairs.Add(new KeyValuePair<string, string>("2 Employment history", null));
                    keyValPairs.Add(new KeyValuePair<string, string>("3 Credit history", null));
                    keyValPairs.Add(new KeyValuePair<string, string>("4 Collateral", null));
                    keyValPairs.Add(new KeyValuePair<string, string>("5 Insufficient cash", null));
                    keyValPairs.Add(new KeyValuePair<string, string>("6 Unverifiable information", null));
                    keyValPairs.Add(new KeyValuePair<string, string>("7 Credit app incomplete", null));
                    keyValPairs.Add(new KeyValuePair<string, string>("8 Mortgage ins denied", null));
                    keyValPairs.Add(new KeyValuePair<string, string>("9 Other", null));
                }

                return keyValPairs;
            }
            else if (fieldId == "aBSuffix" || fieldId == "aCSuffix")
            {
                return suffixOptionArray.Select(x => new KeyValuePair<string, string>(x, null)).ToList();
            }
            else if (fieldId == "sPurchaseAdviceSummaryServicingStatus")
            {
                keyValPairs.Add(new KeyValuePair<string, string>(E_ServicingStatus.Released.ToString("D"), "Released"));
                keyValPairs.Add(new KeyValuePair<string, string>(E_ServicingStatus.Retained.ToString("D"), "Retained"));
                return keyValPairs;
            }
            else if (fieldId == "sCorrespondentProcessT")
            {
                keyValPairs = GetValuesFromType(typeof(E_sCorrespondentProcessT));
                var broker = PrincipalFactory.CurrentPrincipal.BrokerDB;
                if (!broker.ExposeBulkMiniBulkCorrProcessTypeEnums)
                {
                    if (loanData.sCorrespondentProcessT != E_sCorrespondentProcessT.Bulk)
                    {
                        keyValPairs.RemoveAll(x => x.Key == E_sCorrespondentProcessT.Bulk.ToString("D"));
                    }

                    if (loanData.sCorrespondentProcessT != E_sCorrespondentProcessT.MiniBulk)
                    {
                        keyValPairs.RemoveAll(x => x.Key == E_sCorrespondentProcessT.MiniBulk.ToString("D"));
                    }
                }

                return keyValPairs;
            }
            else if ((new string[] { "s1006ProHExpDesc", "s1007ProHExpDesc", "sU3RsrvDesc", "sU4RsrvDesc" }).Contains(fieldId))
            {
                return FeeType.RetrieveAllByBrokerIdAndLineNumber(BrokerUserPrincipal.CurrentPrincipal.BrokerId, "1000").Select(x => new KeyValuePair<string, string>(x.Description, null));
            }
            else if (fieldId == "sGLNewServNm")
            {
                keyValPairs.AddRange(Utilities.GoodbyeLetter.GetInvestorNames().ToList());
                return keyValPairs;
            }
            else if (fieldId == "sLDiscntBaseT")
            {
                keyValPairs.Add(KeyValuePairFromEnum(E_PercentBaseT.LoanAmount));
                keyValPairs.Add(KeyValuePairFromEnum(E_PercentBaseT.TotalLoanAmount));

                return keyValPairs;
            }
            else if (fieldId == "sMiCompanyNmT")
            {
                var options = new List<E_sMiCompanyNmT>(EnumUtilities.GetEnumValuesFromField("sMiCompanyNmT").Select(en => (E_sMiCompanyNmT)(object)en));
                if ((loanData.sLT != E_sLT.FHA) && (loanData.sLT != E_sLT.VA) && (loanData.sLT != E_sLT.UsdaRural))
                {
                    options.RemoveAll(x => (x == E_sMiCompanyNmT.FHA)
                    || (x == E_sMiCompanyNmT.VA)
                    || (x == E_sMiCompanyNmT.USDA));
                }
                else
                {
                    options.RemoveAll(x => (x != loanData.sMiCompanyNmT));
                }

                return options.Select(x => KeyValuePairFromEnum(x));
            }
            else if (fieldId == "sRefPurpose")
            {
                var options = new List<string>()
                {
                    "No Cash-Out Rate/Term",
                    "Limited Cash-Out Rate/Term",
                    "Cash-Out/Home Improvement",
                    "Cash-Out/Debt Consolidation",
                    "Cash-Out/Other"
                };

                if ((loanData.sLT == E_sLT.Conventional) && loanData.sRefPurpose != "No Cash-Out Rate/Term")
                {
                    options.RemoveAll(x => x == "No Cash-Out Rate/Term");
                }

                return options.Select(x => new KeyValuePair<string, string>(x, null));
            }
            else if ((new string[] { nameof(CPageData.s1006ProHExpDesc), nameof(CPageData.s1007ProHExpDesc), nameof(CPageData.sU3RsrvDesc), nameof(CPageData.sU4RsrvDesc) }).Contains(fieldId))
            {
                return FeeType.RetrieveAllByBrokerIdAndLineNumber(principal.BrokerId, "1000").Select(x => new KeyValuePair<string, string>(x.Description, null));
            }

            return null;
        }

        /// <summary>
        /// Return enum values of an enum type.
        /// </summary>
        /// <param name="enumType">Enum type.</param>
        /// <returns>A list of key pair (key,description) of the enum type.
        /// Return null if type is not enum.
        /// </returns>
        public static List<KeyValuePair<string, string>> GetValuesFromType(Type enumType)
        {
            if (!enumType.IsEnum)
            {
                return null;
            }

            return Enum.GetValues(enumType).OfType<Enum>()
                .OrderBy(GetOrder)
                .GroupBy(Convert.ToInt32)
                .Select(z => KeyValuePairFromEnum(z.First()))
                .Where(x => x.Value != null)
                .ToList();
        }

        /// <summary>
        /// Get Key value pair from an enum.
        /// </summary>
        /// <param name="en">The Enumeration.</param>
        /// <returns>A string representing the friendly name.</returns>
        public static KeyValuePair<string, string> KeyValuePairFromEnum(Enum en)
        {
            return new KeyValuePair<string, string>(Convert.ToInt32(en).ToString(), GetDescription(en));
        }

        /// <summary>
        /// Retrieve the description on the enum.
        /// Then when you pass in the enum, it will retrieve the description.
        /// </summary>
        /// <param name="en">The Enumeration.</param>
        /// <returns>A string representing the friendly name.</returns>
        public static string GetDescription(Enum en)
        {
            Type type = en.GetType();

            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(OrderedDescriptionAttribute), false);

                if (attrs != null && attrs.Length > 0)
                {
                    return ((OrderedDescriptionAttribute)attrs[0]).Description;
                }
            }

            string desc = en.ToString();
            if (desc == "LeaveBlank" || desc == "Blank")
            {
                desc = string.Empty;
            }

            desc = Regex.Replace(desc, "([a-z])([A-Z])", "$1 $2");
            desc = desc.Replace("_", " ");
            return desc;
        }

        /// <summary>
        /// Return enum values of an enum type.
        /// </summary>
        /// <param name="enumType">Enum type.</param>
        /// <returns>A list of key pair (key,description) of the enum type.
        /// Return null if type is not enum.
        /// </returns>
        private static IEnumerable<Enum> GetEnumValuesFromType(Type enumType)
        {
            if (!enumType.IsEnum)
            {
                return null;
            }

            Comparison<Enum> enumComparison = (x, y) => GetOrder(x).CompareTo(GetOrder(y));
            var listOfValues = Enum.GetValues(enumType).OfType<Enum>().ToList().OrderBy(x => GetOrder(x)).GroupBy(y => Convert.ToInt32(y)).Select(z => z.First());

            return listOfValues;
        }

        /// <summary>
        /// Retrieve the order on the enum.
        /// Then when you pass in the enum, it will order the description.
        /// </summary>
        /// <param name="en">The Enumeration.</param>
        /// <returns>The order number of the enum.</returns>
        private static int GetOrder(Enum en)
        {
            Type type = en.GetType();

            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(OrderedDescriptionAttribute), false);

                if (attrs != null && attrs.Length > 0)
                {
                    return ((OrderedDescriptionAttribute)attrs[0]).Order;
                }
            }

            return 0;
        }
    }
}
