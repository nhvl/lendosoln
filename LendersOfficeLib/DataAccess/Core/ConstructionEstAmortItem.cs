﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Core.Construction;

    /// <summary>
    /// A class that represents an item in the amortization table for
    /// the construction phase of a loan.
    /// </summary>
    public class ConstructionEstAmortItem : AmortItem
    {
        /// <summary>
        /// When reporting the Index, this number should be added to
        /// ensure that the Index forms an increasing sequence with
        /// whatever other amortization table this one is joined to.
        /// This is genereally required to avoid having to adjust the
        /// index for the main AmortTable, which would blow up the
        /// ARM calculations.
        /// </summary>
        private int offset;

        /// <summary>
        /// Whether or not this table is destined to be combined with
        /// another table in the future. This primarily affects whether
        /// or not a final balloon payment is calculated.
        /// </summary>
        private bool isCombined;

        /// <summary>
        /// Whether or not to assume that the the entire commitment amount
        /// will be outstanding starting immediately after the first payment.
        /// This is required by the TRID Loan Terms section to simulate the
        /// worst-case scenario for the draw amount, but that is not how the
        /// Projected Payments table is to be computed.
        /// </summary>
        private bool isFullDrawAfterFirstPmt;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConstructionEstAmortItem"/> class.
        /// </summary>
        /// <param name="index">The index of the new item.</param>
        /// <param name="previousItem">The previous item.</param>
        /// <param name="dataLoan">The loan (or equivalent) from which to get amortization terms.</param>
        /// <param name="scheduleType">Whether the schedule is standard, best-case, worst-case.</param>
        /// <param name="isCombined">Whether the item belongs to a table that will be combined with another table.</param>
        /// <param name="isFullDrawAfterFirstPmt">Whether to assume a full draw starting with the second payment.</param>
        public ConstructionEstAmortItem(int index, AmortItem previousItem, IAmortizationDataProvider dataLoan, E_AmortizationScheduleT scheduleType, bool isCombined, bool isFullDrawAfterFirstPmt)
            : base(index, previousItem, dataLoan, scheduleType)
        {
            this.offset = isCombined ? -1 * m_dataLoan.sTerm : 0;
            this.isCombined = isCombined;
            this.isFullDrawAfterFirstPmt = isFullDrawAfterFirstPmt;
            base.Calculate();
        }

        /// <summary>
        /// The index of the payment in the table, net of any offset.
        /// </summary>
        public override int Index
        {
            get { return base.Index + this.offset; }
        }

        /// <summary>
        /// Gets a value representing the amount of estimated interest due for the item's period.
        /// </summary>
        public override decimal Interest
        {
            get
            {
                decimal interestAccruedInPeriod;
                decimal constructionCommitmentBalance;

                // We can't do this mapping in the data provider because we need to
                // calculate a balloon payment with the full amount.
                if (this.isFullDrawAfterFirstPmt && base.Index > 0)
                {
                    constructionCommitmentBalance = this.m_dataLoan.sLAmtCalc;
                }
                else
                {
                    constructionCommitmentBalance = this.m_dataLoan.sLAmtCalc * (this.m_dataLoan.sConstructionIntCalcT == ConstructionIntCalcType.HalfCommitment ? 0.5M : 1.0M);
                }

                bool ignoreActualNumberOfDays = true; // IDS currently ignores the actual days; we shall as well for now.

                // Uncomment if we ever stop ignoring the actual days for everything.
                ////if (this.m_dataLoan.sConstructionPhaseIntAccrualT == ConstructionPhaseIntAccrual.Monthly_360_360)
                ////{
                ////    ignoreActualNumberOfDays = true;
                ////}

                decimal dailyRateForPeriod = CPageBase.CalcConstructionDailyRate(this.Rate, this.m_dataLoan.sConstructionPhaseIntAccrualT);
                decimal estimatedNumberOfDays = CPageBase.CalcConstructionEstimatedDaysPerMonth(this.m_dataLoan.sConstructionPhaseIntAccrualT);

                decimal numberOfDaysToUse;

                // Leaving the code to compute using the actual days in place because
                // using estimated days in the disclosures is allowed, but not required.
                if (ignoreActualNumberOfDays)
                {
                    numberOfDaysToUse = estimatedNumberOfDays;
                }
                else
                {
                    numberOfDaysToUse = DaysInTrailingMonth(this.DueDate);
                }

                // If there are odd days at the beginning of the construction period,
                // handle them here.
                if (base.Index == 0)
                {
                    numberOfDaysToUse += this.m_dataLoan.sConstructionInitialOddDays;
                }

                interestAccruedInPeriod = constructionCommitmentBalance * dailyRateForPeriod * numberOfDaysToUse;

                if (this.m_dataLoan.sIsIntReserveRequired)
                {
                    interestAccruedInPeriod = (constructionCommitmentBalance + (interestAccruedInPeriod / 2)) * dailyRateForPeriod * numberOfDaysToUse;
                }

                return Math.Round(interestAccruedInPeriod, 2, MidpointRounding.AwayFromZero);
            }
        }

        /// <summary>
        /// Gets a value representing the amount of the estimated payment exclusive of MI.
        /// </summary>
        public override decimal PaymentWOMI
        {
            get
            {
                decimal result = this.Interest;

                if (base.Index == (m_dataLoan.sDue - 1) && !this.isCombined)
                {
                    // Last Payment
                    result += this.PreviousBalance;
                    this.AddChangeEvent(E_AmortChangeEventType.FinalBalloonPmt);
                    this.AddChangeEvent(E_AmortChangeEventType.IOExpired);
                }

                return Math.Round(result, 2, MidpointRounding.AwayFromZero);
            }
        }

        /// <summary>
        /// Nullifies the Calculate() call in the constructor of the parent class
        /// so that the additional setup that occurs in this class's constructor
        /// has a chance to happen before the calculations occur.
        /// </summary>
        protected override void Calculate()
        {
            return;
        }

        /// <summary>
        /// Computes the number of days in the month before the given due date.
        /// </summary>
        /// <param name="dueDate">The due date.</param>
        /// <returns>The number of days in the month before the given due date.</returns>
        private static int DaysInTrailingMonth(CDateTime dueDate)
        {
            DateTime due = dueDate.DateTimeForComputation;
            return due.Subtract(due.AddMonths(-1)).Days;
        }
    }
}
