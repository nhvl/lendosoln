﻿namespace DataAccess
{
    using LendersOffice.Common;

    /// <summary>
    /// For when there's a part of a switch statement that hasn't been dealt with yet.
    /// </summary>
    public class UnhandledCaseException : CBaseException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UnhandledCaseException"/> class.
        /// </summary>
        /// <param name="v">The string that wasn't accounted for in the switch statement.</param>
        public UnhandledCaseException(string v)
            : base(ErrorMessages.Generic, "Unhandled switch case where value was: " + v)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UnhandledCaseException"/> class.
        /// </summary>
        /// <param name="v">The string that wasn't accounted for in the switch statement.</param>
        /// <param name="message">Any additional message the developer wishes to add.</param>
        public UnhandledCaseException(string v, string message)
            : base(ErrorMessages.Generic, "Unhandled switch case where value was: " + v + ". " + message)
        {
        }
    }
}
