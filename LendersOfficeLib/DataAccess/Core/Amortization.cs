#undef TRACE

using System;
using System.Collections.Generic;
using System.Globalization;
using DataAccess.Core.Construction;
using LendersOffice.Common;


namespace DataAccess
{
    public class AmortTableInvalidArgumentException : CBaseException
    {
        public AmortTableInvalidArgumentException(string msg) : base(ErrorMessages.Generic, msg)
        {
            IsEmailDeveloper = false;
        }
    }

    public class AmortTable
    {
        protected AmortItem[] m_items;
        private E_AmortizationScheduleT scheduleType;

        private IAmortizationDataProvider m_dataLoan;

        public AmortTable(CPageBase dataLoan, E_AmortizationScheduleT scheduleType, bool bGroup)
        {
            if (dataLoan.IsRunningPricingEngine)
            {
                // OPM 108042. Use special cache object for pricing.
                // Benchmarking indicated ~30% faster than reading from loan data object.
                m_dataLoan = new AmortDataLoanCache(dataLoan);
            }
            else
            {
                m_dataLoan = dataLoan;
            }

            InitAmortTable(scheduleType, bGroup);
        }

        public AmortTable(IAmortizationDataProvider amortTableDataStub, E_AmortizationScheduleT scheduleType, bool bGroup)
        {
            m_dataLoan = amortTableDataStub;

            InitAmortTable(scheduleType, bGroup);
        }

        protected virtual AmortItem CreateAmortItem(int index, AmortItem previousItem, IAmortizationDataProvider dataLoan, E_AmortizationScheduleT scheduleType)
        {
            return new AmortItem(index, previousItem, dataLoan, scheduleType);
        }

        protected virtual void InitAmortTable(E_AmortizationScheduleT scheduleType, bool bGroup)
        {
            if (m_dataLoan.sNoteIR < 0 || m_dataLoan.sFinalLAmt <= 0 || m_dataLoan.sTerm <= 0 || m_dataLoan.sDue <= 0)
            {
                throw new AmortTableInvalidArgumentException("Loan amount, term, or due is zero or negative, or note rate is negative.");
            }

            this.scheduleType = scheduleType;

            int nTerm = m_dataLoan.sTerm;
            int nDue = m_dataLoan.sDue;
            if (nDue <= 0 || nDue > nTerm)
                nDue = nTerm;

            m_items = new AmortItem[nDue];
            AmortItem prevItem = null;
            for (int i = 0; i < m_items.Length; i++)
            {
                AmortItem item = this.CreateAmortItem(i, prevItem, m_dataLoan, this.scheduleType);

                m_items[i] = item;
                prevItem = item;
            }

            CalculateBuydownRate();


            // Set aside the first pmt value before messing it up with the prepayment stuff.
            FirstPmt = (0 < nDue) ? (Items[0].Pmt - Items[0].MI) : 0;

            this.IncludesPrepayment = false;
            CalculatePrepayment();

            this.IsGrouped = bGroup;
            if (bGroup)
                Group();
        }

        private void CalculatePrepayment()
        {

            int sPpmtMon = m_dataLoan.sPpmtMon;
            decimal sPpmtAmt = m_dataLoan.sPpmtAmt;
            decimal sPpmtOneAmt = m_dataLoan.sPpmtOneAmt;
            if (sPpmtMon < 0 || sPpmtAmt < 0 || sPpmtOneAmt < 0)
            {
                throw new CBaseException(ErrorMessages.Generic, "sPpmtMon or sPpmtAmt or sPpmtOneAmt is less than zero.");
            }

            if ((sPpmtMon > 0 && sPpmtAmt > 0) || sPpmtOneAmt > 0)
            {
                this.IncludesPrepayment = true;
                int sPpmtStartMon = m_dataLoan.sPpmtStartMon;
                int sPpmtOneMon = m_dataLoan.sPpmtOneMon;
                if (sPpmtStartMon < 0 || sPpmtOneMon < 0)
                {
                    throw new CBaseException(ErrorMessages.Generic, "sPpmtStartMon or sPpmtOneMon is less than zero.");
                }

                decimal extra = 0;
                decimal balReduced = 0;
                decimal balPrev = m_dataLoan.sFinalLAmt;
                int count = sSchedPmtNumTot;

                // Recycled objs
                decimal newIntAmt = 0;

                int mon = Math.Min(sPpmtStartMon, sPpmtOneMon);
                for (; mon < count; ++mon)
                {
                    extra = 0;
                    if (sPpmtMon > 0 && (mon >= sPpmtStartMon) && ((mon - sPpmtStartMon) % sPpmtMon) == 0)
                        extra += sPpmtAmt;
                    if (mon == sPpmtOneMon)
                        extra += sPpmtOneAmt;

                    AmortItem item = Items[mon];

                    newIntAmt = (item.Rate / 1200) * balPrev;
                    decimal intAmtReduced = item.Interest - newIntAmt;
                    balReduced += intAmtReduced + extra;

                    item.UpdateInterestPdManually(newIntAmt);

                    if (item.Bal <= balReduced)
                    {
                        item.UpdateEqManually(balPrev);
                        //                        item.Pmt = item.Eq + item.InterestPd;
                        item.UpdatePmtWOMIManually(item.Principal + item.Interest);
                        //                        item.Bal = 0;
                        item.UpdateBalanceManually(0);
                        mon++;
                        break;
                    }
                    else
                    {
                        //                        item.Pmt.Value += extra;
                        item.UpdatePmtWOMIManually(item.Pmt + extra - item.MI);
                        //                        item.Eq.Value = item.Pmt.Value - item.InterestPd.Value;
                        item.UpdateEqManually(item.Pmt - item.Interest - item.MI);
                        //                        item.Bal.Value -= balReduced;
                        item.UpdateBalanceManually(item.Bal - balReduced);
                    }

                    balPrev = item.Bal;
                }
                AmortItem[] tmp = new AmortItem[mon];
                for (int i = 0; i < mon; i++)
                    tmp[i] = Items[i];

                m_items = tmp;
                //
                //                if( mon < sSchedPmtNumTot ) 
                //                {
                //                    sSchedPmtNumTot_ = mon + 1;
                //                    ClearExtraPayments();
                //                }

            }

        }
        private void CalculateBuydownRate()
        {

            int monBuydwn1 = m_dataLoan.sBuydwnMon1;

            bool bDoBuydwn = true;
            if (monBuydwn1 <= 0)
                bDoBuydwn = false;
            else if (m_dataLoan.sIOnlyMon > 0)
                bDoBuydwn = false;
            else
            {
                switch (m_dataLoan.sFinMethT)
                {
                    case E_sFinMethT.ARM:
                        if (m_dataLoan.sPmtAdjCapMon > 0)
                            bDoBuydwn = false;
                        break;
                }
            }

            if (bDoBuydwn)
            {
                // Rate buydown periods
                int monBuydwn2 = monBuydwn1 + m_dataLoan.sBuydwnMon2;
                int monBuydwn3 = monBuydwn2 + m_dataLoan.sBuydwnMon3;
                int monBuydwn4 = monBuydwn3 + m_dataLoan.sBuydwnMon4;
                int monBuydwn5 = monBuydwn4 + m_dataLoan.sBuydwnMon5;

                // Recycled objects.
                decimal IR_bd = 0;
                decimal Pmt = 0;
                decimal Bal_before = m_dataLoan.sFinalLAmt;

                bool bRecalcPmt = false; // recalculate pmt reduction

                int count = sSchedPmtNumTot;
                int mon;

                for (mon = 0; mon < count && mon < monBuydwn5; ++mon)
                {
                    AmortItem item = Items[mon];

                    //bRecalcPmt = item.RecalcPmt;

                    IR_bd = item.Rate;

                    if (mon >= monBuydwn4)
                    {
                        IR_bd = item.Rate - m_dataLoan.sBuydwnR5;
                        if (mon == monBuydwn4)
                            bRecalcPmt = true;
                    }
                    else if (mon >= monBuydwn3)
                    {
                        IR_bd = item.Rate - m_dataLoan.sBuydwnR4;
                        if (mon == monBuydwn3)
                            bRecalcPmt = true;
                    }
                    else if (mon >= monBuydwn2)
                    {
                        IR_bd = item.Rate - m_dataLoan.sBuydwnR3;
                        if (mon == monBuydwn2)
                            bRecalcPmt = true;
                    }
                    else if (mon >= monBuydwn1)
                    {
                        IR_bd = item.Rate - m_dataLoan.sBuydwnR2;
                        if (mon == monBuydwn1)
                            bRecalcPmt = true;
                    }
                    else if (mon >= 0)
                    {
                        IR_bd = item.Rate - m_dataLoan.sBuydwnR1;
                        if (mon == 0)
                            bRecalcPmt = true;
                    }

                    // Calculate Actual pmt

                    if (bRecalcPmt)
                    {
                        if (mon < (12 * m_dataLoan.sGradPmtYrs) && m_dataLoan.sFinMethT == E_sFinMethT.Graduated)
                        {
                            Pmt = AmortItem.CalculateFirstGPMPmt(IR_bd, m_dataLoan.sFinalLAmt, m_dataLoan.sGradPmtR, m_dataLoan.sGradPmtYrs, m_dataLoan.sTerm);
                        }
                        else
                        {
                            // Uncomment the "-mon" to always calculate buydown rate based remaining loan bal. Comment
                            // it out to calculate it based on original unpaid loan amount.
                            Pmt = AmortItem.CalculateFullPayment(Bal_before, IR_bd, m_dataLoan.sTerm /*- mon*/);
                        }
                    }

                    item.UpdateRateManually(IR_bd);
                    item.UpdatePmtWOMIManually(Pmt);
                    item.UpdateInterestPdManually(Pmt - item.Principal);

                    // Uncomment this to always calculate buydown rate based remaining loan bal. Comment
                    // it out to calculate it based on original unpaid loan amount.
                    //Bal_before.Value = item.Bal.Value; // buydown rate or not, balance is always the same.
                } // for

                // Clean up after the last change
                if (bRecalcPmt)
                {
                    AmortItem item = this.Items[mon];

                    item.UpdateRateManually(item.Rate);
                    item.UpdatePmtWOMIManually(item.PaymentWOMI);
                }

            } // if( bDoBuydwn )
        }

        private void Group()
        {
            if (m_items.Length == 0)
            {
                return;
            }

            AmortItem prevItem = null;
            List<AmortItem> list = new List<AmortItem>();

            foreach (AmortItem item in m_items)
            {
                if (prevItem != null)
                {
                    if (prevItem.Pmt_rep == item.Pmt_rep && prevItem.Rate_rep == item.Rate_rep)
                    {
                        prevItem.Count++;
                        prevItem.UpdateBalanceManually(item.Balance);
                        continue;
                    }
                }
                prevItem = item;
                list.Add(item);
            }

            m_items = list.ToArray();
        }

        public bool IsGrouped { private set; get; }
        public bool IncludesPrepayment { private set; get; }

        private decimal m_firstPmt = 0;
        public decimal FirstPmt
        {
            get { return m_firstPmt; }
            set { m_firstPmt = value; }
        }
        public AmortItem[] Items
        {
            get { return m_items; }
        }
        public int nRows
        {
            get { return m_items.Length; }
        }

        /// <summary>
        /// Returns the items interest amount.  This only works for non-grouped tables.
        /// </summary>
        public decimal sSchedInterestTot
        {
            get
            {
                decimal tot = 0;
                for (int i = 0; i < nRows; ++i)
                {
                    AmortItem item = Items[i];
                    tot += item.Interest;
                }
                return tot;
            }
        }

        public decimal sSchedPmtTot
        {
            get
            {
                decimal tot = 0;
                for (int i = 0; i < nRows; ++i)
                {
                    AmortItem item = Items[i];
                    for (int j = 0; j < item.Count; ++j)
                        tot += item.Pmt;
                }
                return tot;
            }
        }
        public int sSchedPmtNumTot
        {
            get
            {

                int total = 0;
                foreach (AmortItem item in m_items)
                {
                    total += item.Count;
                }
                return total;

            }
        }
        public int nRateChange
        {
            get
            {
                int count = 0;
                decimal currentRate = decimal.MinValue;
                foreach (AmortItem item in Items)
                {
                    if (null == item)
                        break;
                    if (decimal.MinValue == currentRate)
                        currentRate = item.Rate;

                    if (item.Rate != currentRate)
                    {
                        currentRate = item.Rate;
                        ++count;
                    }
                }
                return count;
            }
        }

        public void CalculateHorizon(int nMonths, out decimal principalPaid, out decimal financePaid, out decimal remainingBalance)
        {
            principalPaid = financePaid = remainingBalance = 0;

            for (int i = 0; i < Items.Length && i < nMonths; i++)
            {
                principalPaid += Items[i].Principal;
                financePaid += Items[i].Interest + Items[i].MI;
                remainingBalance = Items[i].Bal;
            }
        }
    }

    public class AmortItem
    {
        private NumberFormatInfo m_formatRate;
        private NumberFormatInfo m_formatMoney;

        public AmortItem(
            int index,
            AmortItem previousItem,
            IAmortizationDataProvider dataLoan,
            E_AmortizationScheduleT scheduleType)
        {
            m_formatRate = new NumberFormatInfo();
            m_formatRate.NumberNegativePattern = 1;
            m_formatRate.NumberDecimalDigits = 3;

            m_formatMoney = new NumberFormatInfo();
            m_formatMoney.NumberNegativePattern = 1;
            m_formatMoney.NumberDecimalDigits = 2;

            m_index = index;
            m_previousItem = previousItem;
            m_dataLoan = dataLoan;

            m_changeEventType = E_AmortChangeEventType.None;

            this.scheduleType = scheduleType;

            sProMIns = dataLoan.Get_sProMIns_ByMonth(index);
            sProMInsMon = dataLoan.sProMInsMon;
            sProMIns2Mon = dataLoan.sProMIns2Mon;
            sProMIns2 = dataLoan.sProMIns2;
            sProMInsCancelLtv = dataLoan.sProMInsCancelLtv;
            sProMInsCancelAppraisalLtv = dataLoan.sProMInsCancelAppraisalLtv;
            sProMInsMidptCancel = dataLoan.sProMInsMidptCancel;
            sProMInsCancelMinPmts = dataLoan.sProMInsCancelMinPmts;
            sFfUfmipR = dataLoan.sFfUfmipR;
            sFfUfMipIsBeingFinanced = dataLoan.sFfUfMipIsBeingFinanced;
            sLT = dataLoan.sLT;

            if (sProMIns < 0 || sProMInsMon < 0 || sProMIns2Mon < 0 || sProMIns2 < 0 || sProMInsCancelLtv < 0 || sProMInsCancelMinPmts < 0)
            {
                throw new CBaseException(ErrorMessages.Generic, "(sProMIns < 0 || sProMInsMon < 0 || sProMIns2Mon < 0 || sProMIns2 < 0 || sProMInsCancelLtv < 0 || sProMInsCancelMinPmts < 0 )");
            }
            Calculate();
        }

        private E_AmortizationScheduleT scheduleType;

        private decimal sProMIns;
        private decimal sProMInsMon;
        private decimal sProMIns2Mon;
        private decimal sProMInsCancelLtv;
        private decimal sProMInsCancelAppraisalLtv;
        private decimal sProMIns2;
        private bool sProMInsMidptCancel;
        private int sProMInsCancelMinPmts;
        private decimal sFfUfmipR;
        private bool sFfUfMipIsBeingFinanced;
        private E_sLT sLT;

        private bool IsCalcPmi
        {
            get
            {
                bool bPmiCalc = true;

                if (sProMIns <= 0.0M || sProMInsMon < 0 || sProMIns2Mon < 0 || sProMIns2 < 0.0M)
                    bPmiCalc = false;

                if (bPmiCalc)
                {
                    //sProMInsMon == 0 means users want to set some auto-cancel (via midpoint or ltv)
                    if (sProMInsMon == 0 || sProMInsMon > m_dataLoan.sDue)
                    {
                        sProMInsMon = m_dataLoan.sDue;
                        // If first MIns info is not valid, just do MIns entire loan
                        sProMIns2Mon = 0;
                        sProMIns2 = 0.0M;
                    }
                    else if (sProMIns2 <= 0 || 0 == sProMIns2Mon)
                    {
                        sProMIns2Mon = 0;
                        sProMIns2 = 0.0M;
                    }
                    else if (sProMInsMon + sProMIns2Mon > m_dataLoan.sDue)
                    {
                        sProMIns2Mon = m_dataLoan.sDue - sProMInsMon;
                    }

                    if (sProMInsCancelLtv < 0)
                        sProMInsCancelLtv = 0;

                    if (sProMInsCancelAppraisalLtv < 0)
                        sProMInsCancelAppraisalLtv = 0;

                    if (sProMInsCancelMinPmts < 0)
                        sProMInsCancelMinPmts = 0;

                }
                return bPmiCalc;
            }
        }
        protected IAmortizationDataProvider m_dataLoan;
        private AmortItem m_previousItem = null;
        private int m_index;
        public virtual int Index
        {
            get { return m_index; }
        }

        public string PmtIndex_rep
        {
            get { return m_dataLoan.m_convertLos.ToCountString(Index + 1); }
        }

        public decimal RoundMoney(decimal value)
        {
            decimal rounded;
            if (m_formatMoney.NumberDecimalDigits > 0)
                rounded = Math.Round(value, m_formatMoney.NumberDecimalDigits);
            else
                rounded = value;

            if (m_dataLoan is AmortDataLoanCache)
                return rounded; // OPM 108042. Pricing do this.

            return Convert.ToDecimal(rounded.ToString("F", m_formatMoney));
            //            return Math.Round(value, 2);
        }
        public decimal RoundRate(decimal value)
        {
            decimal rounded;
            if (m_formatRate.NumberDecimalDigits > 0)
                rounded = Math.Round(value, m_formatRate.NumberDecimalDigits);
            else
                rounded = value;

            if (m_dataLoan is AmortDataLoanCache)
                return rounded; // OPM 108042. Pricing do this.

            return Convert.ToDecimal(rounded.ToString("F", m_formatRate));
            //            return Math.Round(value, 3);
        }

        public decimal CurrentIndexRate
        {
            get 
            { 
                switch (this.scheduleType)
                {
                    case E_AmortizationScheduleT.Standard:
                        return this.m_dataLoan.sRAdjIndexR;
                    case E_AmortizationScheduleT.WorstCase:
                        return 200.00M;
                    case E_AmortizationScheduleT.BestCase:
                        return -200.00M;
                    default:
                        throw new UnhandledEnumException(this.scheduleType);
                }
            }
        }
        private decimal PreviousRate
        {
            get { return m_previousItem != null ? m_previousItem.Rate : m_dataLoan.sNoteIR; }
        }
        public decimal PreviousBalance
        {
            get { return RoundMoney(null != m_previousItem ? m_previousItem.Balance : m_dataLoan.sFinalLAmt); }
        }
        private decimal PreviousPayment
        {
            get { return m_previousItem != null ? m_previousItem.Payment : 0; }
        }
        private decimal PreviousPaymentWOMI
        {
            get { return m_previousItem != null ? m_previousItem.PaymentWOMI : 0; }
        }
        private decimal PreviousMI
        {
            get { return m_previousItem != null ? m_previousItem.MI : 0; }
        }

        private bool IsRateChanged
        {
            get { return PreviousRate != Rate; }
        }

        private bool IsRecalcMinimumPayment
        {
            get
            {
                return (Index == 0) ||
                    (m_dataLoan.sIsOptionArm && (Index <= m_dataLoan.sOptionArmMinPayPeriod) && m_dataLoan.sPmtAdjCapMon > 0 && Index >= m_dataLoan.sOptionArmInitialFixMinPmtPeriod && (Index - m_dataLoan.sOptionArmInitialFixMinPmtPeriod) % m_dataLoan.sPmtAdjCapMon == 0);
            }
        }
        private bool IsRecalcPayment
        {
            get
            {
                return (Index == 0) || // First Payment
                    (m_dataLoan.sIOnlyMon > 0 && Index == m_dataLoan.sIOnlyMon) || // Interest Only Period Over.
                    (!m_dataLoan.sIsOptionArm && m_dataLoan.sPmtAdjCapMon > 0 && (Index % m_dataLoan.sPmtAdjCapMon) == 0) || // Payment Adjustment hit. Recalculate payment
                    (!m_dataLoan.sIsOptionArm && m_dataLoan.sGradPmtYrs > 0 && Index % 12 == 0 && Index <= (m_dataLoan.sGradPmtYrs * 12)) || // Graduate Payment
                    IsRecalcMinimumPayment
                    || IsRecastPeriod; //|| // Recast Period
                
            }

        }
        private int NumberOfMonthsLeft
        {
            get { return m_dataLoan.sTerm - Index; }

        }
        private bool IsCalculateIOPayment
        {
            get
            {
                return (!(m_dataLoan.sIsOptionArm && Index < m_dataLoan.sOptionArmMinPayPeriod) && m_dataLoan.sIOnlyMon > 0 && Index < m_dataLoan.sIOnlyMon) ||
                    (IsNegArmRecast && !m_dataLoan.sIsFullAmortAfterRecast && (m_dataLoan.sOptionArmMinPayPeriod > 0 && Index < m_dataLoan.sOptionArmMinPayPeriod)) ||
                    (m_dataLoan.sIsOptionArm && Index < m_dataLoan.sOptionArmMinPayPeriod && m_dataLoan.sOptionArmMinPayIsIOOnly
                       && ((Index < m_dataLoan.sPmtAdjRecastPeriodMon && m_dataLoan.sPmtAdjRecastPeriodMon > 0) || m_dataLoan.sPmtAdjRecastPeriodMon == 0)
                    )
                    ;
            }
        }


        private decimal MaxBalance
        {
            get
            {
                if (m_dataLoan.sPmtAdjMaxBalPc > 0)
                {
                    return m_dataLoan.sFinalLAmt * m_dataLoan.sPmtAdjMaxBalPc / 100;
                }
                else
                {
                    return decimal.MaxValue;
                }
            }
        }

        private decimal x_rate = decimal.MinValue;
        public decimal Rate
        {
            get
            {
                if (x_rate == decimal.MinValue)
                {
                    decimal rate = PreviousRate;
                    if (m_dataLoan.sIsOptionArm && m_dataLoan.sOptionArmIntroductoryPeriod > 0 && Index < m_dataLoan.sOptionArmIntroductoryPeriod)
                    {
                        // OptionARM Introductory Period.
                        if (m_dataLoan.sOptionArmMinPayOptionT == E_sOptionArmMinPayOptionT.TeaserRate)
                            rate = m_dataLoan.sOptionArmTeaserR;
                        else
                            rate = PreviousRate;
                    }
                    else if (m_dataLoan.sIsOptionArm && m_dataLoan.sOptionArmIntroductoryPeriod > 0 && Index == m_dataLoan.sOptionArmIntroductoryPeriod &&
                        !(m_dataLoan.sFinMethT == E_sFinMethT.ARM && m_dataLoan.sOptionArmIntroductoryPeriod == m_dataLoan.sRAdj1stCapMon) /* Do not reverse back to old rate if Min period same with 1st adj period */)
                    {
                        rate = m_dataLoan.sNoteIR;
                    }
                    else
                    {
                        if (m_dataLoan.sFinMethT == E_sFinMethT.ARM)
                        {
                            bool isRateAdjustmentItem = false;

                            if (m_dataLoan.sRAdj1stCapMon > 0 && m_dataLoan.sRAdj1stCapMon == Index)
                            {
                                isRateAdjustmentItem = true;
                                // First Adjustment period
                                decimal newRate = m_dataLoan.sRAdjMarginR + CurrentIndexRate;
                                newRate = RoundRateAdjRoundTo(newRate);
                                if (newRate > rate + m_dataLoan.sRAdj1stCapR)
                                    rate = rate + m_dataLoan.sRAdj1stCapR;
                                else if (newRate < rate - m_dataLoan.sRAdj1stCapR)
                                    rate = rate - m_dataLoan.sRAdj1stCapR;
                                else
                                    rate = newRate;
                            }
                            else if (m_dataLoan.sRAdjCapMon > 0 && (Index > m_dataLoan.sRAdj1stCapMon) && (Index - m_dataLoan.sRAdj1stCapMon) % m_dataLoan.sRAdjCapMon == 0)
                            {
                                isRateAdjustmentItem = true;
                                // Adjustment Period.
                                decimal newRate = m_dataLoan.sRAdjMarginR + CurrentIndexRate;
                                newRate = RoundRateAdjRoundTo(newRate);
                                if (newRate > rate + m_dataLoan.sRAdjCapR)
                                    rate = rate + m_dataLoan.sRAdjCapR;
                                else if (newRate < rate - m_dataLoan.sRAdjCapR)
                                    rate = rate - m_dataLoan.sRAdjCapR;
                                else
                                    rate = newRate;
                            }

                            // Check to make sure it does not exceed the lifetime cap and rate floor.
                            if (m_dataLoan.sRAdjLifeCapR > 0 && (rate - m_dataLoan.sNoteIR) > m_dataLoan.sRAdjLifeCapR)
                            {
                                rate = m_dataLoan.sNoteIR + m_dataLoan.sRAdjLifeCapR;
                            }
                            
                            // Only apply the floor rate when we would otherwise be adjusting the rate.
                            // In other words, don't apply the floor to the initial note rate.
                            if (rate < m_dataLoan.sRAdjFloorR && isRateAdjustmentItem)
                            {
                                rate = m_dataLoan.sRAdjFloorR;
                            }

                            if (isRateAdjustmentItem)
                            {
                                AddChangeEvent(E_AmortChangeEventType.RateChange);
                                AddChangeEvent(E_AmortChangeEventType.IndexChange);
                            }
                        }
                    }

                    x_rate = RoundRate(rate);

                }

                return x_rate;
            }
        }

        public void UpdateRateManually(decimal rate)
        {
            x_rate = rate;

            if (x_rate != this.PreviousRate)
            {
                this.AddChangeEvent(E_AmortChangeEventType.RateChange);
            }
        }

        /// <summary>
        /// Snap the rate to the closest rate increment (given by sRAdjRoundToR above the whole # rate). 
        /// or to the one determined by the rate rounding type (up or down or normal from sRAdjRoundT)
        /// </summary>
        private decimal RoundRateAdjRoundTo(decimal rate)
        {
            // TODO: Need to round rate.
            return Tools.SnapToGridLine(rate, m_dataLoan.sRAdjRoundToR, m_dataLoan.sRAdjRoundT);

        }


        public string Rate_rep
        {
            get { return m_dataLoan.m_convertLos.ToRateString(Rate); }
        }

        public decimal Payment
        {
            get 
            {
                return PaymentWOMI + MI;
            }
        }

        private decimal? x_paymentWOMI = null; // payment without the MI included;
        public virtual decimal PaymentWOMI
        {
            get
            {
                //                if (Index > 59) 
                //                {
                //                    Tools.LogError("Index=" + Index + ", IsRecalcPayment=" + IsRecalcPayment);
                //                }
                if (x_paymentWOMI == null)
                {
                    decimal paymentWOMI = 0;
                    if (Index == (m_dataLoan.sDue - 1))
                    {
                        // Last Payment
                        paymentWOMI = Interest + PreviousBalance;
                    }
                    else if (IsRecalcPayment || IsRateChanged)
                    {
                        if (m_dataLoan.sFinMethT == E_sFinMethT.Graduated && m_dataLoan.sGradPmtR > 0 && m_dataLoan.sGradPmtYrs > 0)
                        {
                            // Graduate PMT
                            if (Index == 0)
                            {
                                paymentWOMI = CalculateFirstGPMPmt(Rate, m_dataLoan.sFinalLAmt, m_dataLoan.sGradPmtR, m_dataLoan.sGradPmtYrs, m_dataLoan.sTerm);
                            }
                            else
                            {
                                paymentWOMI = PreviousPaymentWOMI * (1 + m_dataLoan.sGradPmtR / 100);
                            }
                        }
                        else
                        {
                            if (m_dataLoan.sIsOptionArm && Index < m_dataLoan.sOptionArmMinPayPeriod && !IsRecastPeriod)
                            {
                                // In Option ARM.
                                if (IsRecalcMinimumPayment)
                                {
                                    switch (m_dataLoan.sOptionArmMinPayOptionT)
                                    {
                                        case E_sOptionArmMinPayOptionT.TeaserRate:
                                            if (IsCalculateIOPayment)
                                            {
                                                paymentWOMI = PreviousBalance * Rate / 1200;
                                            }
                                            else
                                            {
                                                if (NumberOfMonthsLeft == 0)
                                                {
                                                    throw new AmortTableInvalidArgumentException("NumberOfMonthsLeft has become zero unexpectedly.");
                                                }
                                                paymentWOMI = CalculateFullPayment(PreviousBalance, Rate, NumberOfMonthsLeft);
                                            }
                                            break;
                                        case E_sOptionArmMinPayOptionT.ByDiscountNoteIR:
                                            if (IsCalculateIOPayment)
                                            {
                                                paymentWOMI = PreviousBalance * (Rate - m_dataLoan.sOptionArmNoteIRDiscount) / 1200;
                                            }
                                            else
                                            {
                                                if (Rate - m_dataLoan.sOptionArmNoteIRDiscount == 0.0M || NumberOfMonthsLeft == 0)
                                                {
                                                    throw new AmortTableInvalidArgumentException("Min payment rate or NumberOfMonthsLeft has become zero unexpectedly.");
                                                }
                                                paymentWOMI = CalculateFullPayment(PreviousBalance, Rate - m_dataLoan.sOptionArmNoteIRDiscount, NumberOfMonthsLeft);
                                            }
                                            break;
                                        case E_sOptionArmMinPayOptionT.ByDiscountPmt:
                                            if (IsCalculateIOPayment)
                                            {
                                                paymentWOMI = (PreviousBalance * Rate / 1200) * m_dataLoan.sOptionArmPmtDiscount / 100;
                                            }
                                            else
                                            {
                                                if (NumberOfMonthsLeft == 0)
                                                {
                                                    throw new AmortTableInvalidArgumentException("NumberOfMonthsLeft has become zero unexpectedly.");
                                                }
                                                paymentWOMI = CalculateFullPayment(PreviousBalance, Rate, NumberOfMonthsLeft) * m_dataLoan.sOptionArmPmtDiscount / 100;
                                            }
                                            break;
                                    }

                                    // 2/14/2007 dd - After recast, minimum paymentWOMI must be greater than previous paymentWOMI.
                                    if (m_dataLoan.sPmtAdjRecastPeriodMon > 0 && Index > m_dataLoan.sPmtAdjRecastPeriodMon && paymentWOMI < PreviousPaymentWOMI)
                                        paymentWOMI = PreviousPaymentWOMI;
                                }
                                else
                                {
                                    paymentWOMI = PreviousPaymentWOMI;
                                }


                            }
                            else
                            {
                                if (IsCalculateIOPayment)
                                {
                                    paymentWOMI = PreviousBalance * Rate / 1200;
                                }
                                else
                                {
                                    if (NumberOfMonthsLeft == 0)
                                    {
                                        throw new AmortTableInvalidArgumentException("NumberOfMonthsLeft has become zero unexpectedly.");
                                    }
                                    paymentWOMI = CalculateFullPayment(PreviousBalance, Rate, NumberOfMonthsLeft);
                                }
                            }

                            if (!IsRecastPeriod)
                            {
                                // Make sure paymentWOMI does not exceed paymentWOMI adjustment cap. Payment cap is only honor when it is not recasting.

                                if (m_dataLoan.sPmtAdjCapR > 0 && m_previousItem != null)
                                {
                                    decimal maxPayment = PreviousPaymentWOMI * (1 + m_dataLoan.sPmtAdjCapR / 100);
                                    if (paymentWOMI > maxPayment)
                                        paymentWOMI = maxPayment;
                                }
                            }

                        }

                    }
                    else
                    {
                        paymentWOMI = PreviousPaymentWOMI;
                    }

                    x_paymentWOMI = RoundMoney(paymentWOMI);

                    if (x_paymentWOMI.Value != PreviousPaymentWOMI)
                    {
                        this.AddChangeEvent(E_AmortChangeEventType.PIChange);
                    }
                }

                return x_paymentWOMI.Value;
            }
        }

        public void UpdatePmtWOMIManually(decimal paymentWOMI)
        {
            x_paymentWOMI = RoundMoney(paymentWOMI);

            if (x_paymentWOMI.Value != PreviousPaymentWOMI)
            {
                this.AddChangeEvent(E_AmortChangeEventType.PIChange);
            }
        }
        public decimal Pmt
        {
            get { return Payment; }
        }
        public string Pmt_rep
        {
            get { return m_dataLoan.m_convertLos.ToMoneyString(Payment, FormatDirection.ToRep); }
        }


        private bool x_isNegArmRecast = false;
        private bool IsNegArmRecast
        {
            get
            {
                return ((m_dataLoan.sPmtAdjMaxBalPc > 0 && PreviousBalance > MaxBalance) &&
                    (m_dataLoan.sPmtAdjRecastPeriodMon > 0 && Index < m_dataLoan.sPmtAdjRecastPeriodMon)) || x_isNegArmRecast;
            }
        }
        private bool IsScheduleRecast
        {
            get
            {
                if (m_dataLoan.sPmtAdjRecastStop > 0 && Index > m_dataLoan.sPmtAdjRecastStop && m_dataLoan.sPmtAdjRecastStop >= m_dataLoan.sOptionArmMinPayPeriod)
                    return false; // Recast Period exceed.
                else
                    return ((m_dataLoan.sPmtAdjRecastPeriodMon > 0 && Index > 0 && (Index % m_dataLoan.sPmtAdjRecastPeriodMon == 0)) ||
                        (m_dataLoan.sIsOptionArm && m_dataLoan.sOptionArmMinPayPeriod > 0 && Index == m_dataLoan.sOptionArmMinPayPeriod));


            }
        }


        private bool IsRecastPeriod
        {
            get { return IsNegArmRecast || IsScheduleRecast; }
        }



        private decimal m_balance;
        public decimal Balance
        {
            get { return m_balance; }
        }
        public void UpdateBalanceManually(decimal bal)
        {
            m_balance = bal;
        }
        public decimal Bal
        {
            get { return Balance; }
        }
        public string Bal_rep
        {
            get { return m_dataLoan.m_convertLos.ToMoneyString(Balance, FormatDirection.ToRep); }
        }

        public decimal BalancePercent
        {
            get { return Balance / m_dataLoan.sFinalLAmt * 100; }

        }
        public string BalancePercent_rep
        {
            get { return m_dataLoan.m_convertLos.ToRateString(BalancePercent); }
        }

        private CDateTime x_dueDate = null;
        public CDateTime DueDate
        {
            get
            {
                if (x_dueDate == null)
                {
                    if (null == m_previousItem)
                    {
                        if (m_dataLoan.sSchedDueD1 != null && m_dataLoan.sSchedDueD1.IsValid)
                        {
                            x_dueDate = m_dataLoan.sSchedDueD1;
                        }
                        else
                            x_dueDate = CDateTime.InvalidWrapValue;

                    }
                    else
                    {
                        CDateTime dt = m_previousItem.DueDate;
                        if (dt != CDateTime.InvalidWrapValue)
                        {
                            x_dueDate = CDateTime.Create(dt.DateTimeForComputation.AddMonths(1));
                        }
                        else
                        {
                            x_dueDate = CDateTime.InvalidWrapValue;
                        }

                    }

                }
                return x_dueDate;
            }
        }
        public void UpdateDueDate(CDateTime dueDate)
        {
            x_dueDate = dueDate;
        }
        public string DueDate_rep
        {
            get
            {
                try
                {
                    return DueDate.ToString(m_dataLoan.m_convertLos);
                }
                catch
                {
                    return "";
                }

            }
        }

        private int m_count = 1;
        public int Count
        {
            get { return m_count; }
            set { m_count = value; }
        }
        private decimal m_principal;
        public decimal Principal
        {
            get { return m_principal; }
        }

        public void UpdateEqManually(decimal principal)
        {
            m_principal = principal;
        }

        public string Eq_rep
        {
            get { return m_dataLoan.m_convertLos.ToMoneyString(Principal, FormatDirection.ToRep); }
        }

        private decimal m_interest;
        public virtual decimal Interest
        {
            get { return m_interest; }
        }

        public void UpdateInterestPdManually(decimal interestPd)
        {
            m_interest = interestPd;
        }
        public string InterestPd_rep
        {
            get { return m_dataLoan.m_convertLos.ToMoneyString(Interest, FormatDirection.ToRep); }
        }

        private E_AmortChangeEventType m_changeEventType;
        public E_AmortChangeEventType ChangeEventType
        {
            get { return m_changeEventType; }
        }
        public bool HasChangeEvent(E_AmortChangeEventType changeType)
        {
            return (m_changeEventType & changeType) != 0;
        }
        public void AddChangeEvent(E_AmortChangeEventType changeType)
        {
            m_changeEventType |= changeType;
        }
        // We shouldn't need to clear one of these flags, so no ClearChangeEvent method.

        decimal? x_mi = null;
        public decimal MI
        {
            get
            {
                if (x_mi.HasValue == false || !(m_dataLoan is AmortDataLoanCache))
                {
                    // Step 5: Calculate PMI
                    // When does this value get displayed? This value is displayed together as a sum
                    // with the regular pmt and it will be included as part of the APR calculation, but not
                    // included in the "popular" 1st mtg payment.
                    decimal mi = 0.0M;

                    if (IsCalcPmi)
                    {
                        // At this point, we know that the specified numbers ( sProMInsMon, sProMIns2Mon etc ) 
                        // have been fixed up to be within the valid ranges.

                        //NOTE: Midpoint and LTV cancel come into effect as an OR condition. Which ever happens
                        // first.

                        // 3 types of stops
                        // 1) Months specificed ran out (sProMInsMon & sProMIns2Mon )
                        bool bPmiCalcStopped = Index >= (sProMInsMon + sProMIns2Mon);

                        // 11/23/09 mf. Per OPM 34095, adding a new MI setting: Minimum Number of MI payments.
                        // MI cancellation will occur only if it is cancelled by LTV or Midpoint *and* the minimum number of payments have occured.
                        // So, if we didn't stop MI above because we passed the specified MI term, and we are within the minimum MI months,
                        // we calculate MI even if LTV or midpoint cancellation would have occured.  Minimum MI payments is honored first.
                        if (!bPmiCalcStopped && Index >= sProMInsCancelMinPmts)
                        {
                            // 2) sProMInsMidptCancel and month passes the mid point.
                            bPmiCalcStopped = bPmiCalcStopped || (sProMInsMidptCancel && Index > (m_dataLoan.sTerm / 2));

                            // 3) sProMInsCancelLtv > 0 and ltv has been reached.
                            if (!bPmiCalcStopped && (sProMInsCancelLtv > 0 || sProMInsCancelAppraisalLtv > 0))
                            {
                                // 7/25/2011 dd - OPM 68401 - When loan is FHA and UFMIP is finance then we need
                                // to modify the cancellation differently.

                                decimal baseFactor = 1.0M; // This is a standard calculation.

                                if (sLT == E_sLT.FHA && sFfUfMipIsBeingFinanced)
                                {
                                    baseFactor = 1 + sFfUfmipR / 100;
                                }

                                var propertyValue = m_dataLoan.sHouseVal;
                                var appraisalValue = m_dataLoan.sApprValFor_LTV_CLTV_HCLTV;

                                if (0 == baseFactor || (0 == propertyValue && appraisalValue == 0)) // 7/31/2012 sk opm89398 - prevent divide by zero errors.
                                {
                                    bPmiCalcStopped = false;
                                }
                                else
                                {
                                    decimal currentLtv = propertyValue == 0 ? 0 : (100.0M * PreviousBalance) / baseFactor / propertyValue;
                                    decimal currentAppraisalLtv = appraisalValue == 0 ? 0 : (100.0M * PreviousBalance) / baseFactor / appraisalValue;

                                    bPmiCalcStopped = currentLtv < sProMInsCancelLtv || currentAppraisalLtv < sProMInsCancelAppraisalLtv;
                                }
                            }
                        }

                        if (!bPmiCalcStopped)
                        {
                            // NOTE: does PMI value gets adjusted to base on the new balance?
                            // Point and we both calculate sProMIns2 is calculated based on the original base value.
                            // The option in Point's mortgage insurance's dialog about calculating
                            // based on remaining balance or doesn't seem to affect anything.
                            if (Index < sProMInsMon)
                                mi = sProMIns;
                            else if (Index < (sProMInsMon + sProMIns2Mon))
                                mi = sProMIns2;
                        }

                    }

                    x_mi = RoundMoney(mi);
                }

                return x_mi.Value;

            }
        }
        public bool IsMIChanged
        {
            get { return m_previousItem != null ? m_previousItem.MI != this.MI : false; }
        }
        public string MI_rep
        {
            get { return m_dataLoan.m_convertLos.ToMoneyString(MI, FormatDirection.ToRep); }
        }

        protected virtual void Calculate()
        {
            m_interest = RoundMoney(PreviousBalance * Rate / 1200);
            m_principal = (Payment - MI) - Interest;
            m_balance = PreviousBalance - m_principal;

            if (m_balance > MaxBalance)
            {
                // Recalculate everything.
                x_isNegArmRecast = true;
                x_rate = decimal.MinValue;
                x_paymentWOMI = null;
                m_interest = RoundMoney(PreviousBalance * Rate / 1200);
                m_principal = (Payment - MI) - Interest;
                m_balance = PreviousBalance - m_principal;

            }

            // Rate flag was handled in the Rate property calculation
            // Check for IO expiration
            if (m_dataLoan.sIOnlyMon != 0 && m_dataLoan.sIOnlyMon == Index) AddChangeEvent(E_AmortChangeEventType.IOExpired);
            // Check for MI termination
            if (this.PreviousMI > 0 && this.MI == 0) AddChangeEvent(E_AmortChangeEventType.MITermination);
            // Check for final balloon payment (defined as twice or more of "normal")
            if (Index == (m_dataLoan.sDue - 1) && PaymentWOMI > PreviousPaymentWOMI*2) AddChangeEvent(E_AmortChangeEventType.FinalBalloonPmt);
        }

        public static decimal CalculateFullPayment(decimal balance, decimal rate, decimal numberOfPayments)
        {
            if ( rate == 0 )
            {
                if (numberOfPayments == 0 )
                {
                    return 0;
                }

                return balance / numberOfPayments;
            }

            decimal monthlyRate = rate / 1200;
            decimal jpower = (decimal)Math.Pow((double)(1 + monthlyRate), (double)(-1 * numberOfPayments));
            return balance * (monthlyRate / (1 - jpower));
        }

        public static decimal CalculateFirstGPMPmt(decimal rate, decimal finalLAmt, decimal gradPmtR, int gradPmtYrs, int term)
        {
            double g = (double)gradPmtR / 100;
            double t = (double)gradPmtYrs;
            double i = (double)rate / 1200;
            double n = (double)term;

            // a = { [( 1 + g )^(t+1)  / (1 + i)^((t+1)*12)] - [(1+g) / (1+i)^12 ] }
            //      	*
            //	     [(1 + i )^12  ] / [ (1+g) - (1+i)^12 ]

            double x = Math.Pow(1 + i, 12);
            double a = ((Math.Pow(1 + g, t + 1) / Math.Pow(1 + i, 12 * (t + 1)))
                - ((1 + g) / x))
                * (x / ((1 + g) - x));

            // b = [ (1+i)^12 - 1] / [ i *(1+ i)^12  ]
            double b = (x - 1) / (i * x);

            // c = (1+g)^t * [ (1+i)^(n-12*t)  -  1 ] /  [ i * ( 1 + i )^n ]
            double _m = Math.Pow(1 + g, t);
            double _r = Math.Pow(1 + i, n - 12 - 12 * t) - 1; // -12 is what I added to "fix" the formula.
            double _p = i * Math.Pow(1 + i, n);
            double c = _m * (_r / _p);
            //double c = Math.Pow( 1 + g, t ) * ( Math.Pow( 1 + i, n - 12 - 12*t ) - 1 ) / ( i * Math.Pow( 1 + i, n ) );

            // GPM initial pmt = sFinalLAmt / ( 1+a)*b + c
            return (decimal)((double)finalLAmt / ((1 + a) * b + c));
        }

        public override string ToString()
        {
            decimal balPercent = Balance / m_dataLoan.sFinalLAmt;
            decimal _rate = Rate;

            if (m_formatRate.NumberDecimalDigits > 0)
                _rate = Math.Round(_rate, m_formatRate.NumberDecimalDigits);
            return string.Format("[{0,3} - Rate={1,8}, Payment={2,15}, Interest={3,10}, Principal={4,15}, Balance={5,15}, Balance %={6,8}]", Index + 1, _rate.ToString("F", m_formatRate), Payment.ToString("C"), Interest.ToString("C"), Principal.ToString("C"), Balance.ToString("C"), balPercent.ToString("P"));
        }
    }

    /// <summary>
    /// The data required for all amortization table calculations.
    /// </summary>
    public interface IAmortizationBaseDataProvider
    {
        decimal sNoteIR { get; }
        int sTerm { get; }
        int sDue { get; }
        decimal sFinalLAmt { get; }
        E_sFinMethT sFinMethT { get; }
        LosConvert m_convertLos { get; }
        CDateTime sSchedDueD1 { get; }
        bool sIsOptionArm { get; }
        int sIOnlyMon { get; }
    }

    /// <summary>
    /// The data required for applying a regular borrower prepayment plan.
    /// </summary>
    public interface IAmortizationPrepaymentDataProvider
    {
        int sPpmtMon { get; }
        decimal sPpmtAmt { get; }
        decimal sPpmtOneAmt { get; }
        int sPpmtStartMon { get; }
        int sPpmtOneMon { get; }
    }

    /// <summary>
    /// The data required for temporary buydown loans. 
    /// </summary>
    public interface IAmortizationBuydownPaymentDataProvider
    {
        int sBuydwnMon1 { get; }
        int sBuydwnMon2 { get; }
        int sBuydwnMon3 { get; }
        int sBuydwnMon4 { get; }
        int sBuydwnMon5 { get; }
        decimal sBuydwnR1 { get; }
        decimal sBuydwnR2 { get; }
        decimal sBuydwnR3 { get; }
        decimal sBuydwnR4 { get; }
        decimal sBuydwnR5 { get; }
    }

    /// <summary>
    /// The data required to calculate monthly MI for the entire amortization schedule.
    /// </summary>
    public interface IAmortizationMiDataProvider
    {
        int sProMInsMon { get; }
        int sProMIns2Mon { get; }
        decimal sProMIns2 { get; }
        decimal sProMInsCancelLtv { get; }
        decimal sProMInsCancelAppraisalLtv { get; }
        bool sProMInsMidptCancel { get; }
        int sProMInsCancelMinPmts { get; }
        decimal sFfUfmipR { get; }
        bool sFfUfMipIsBeingFinanced { get; }
        decimal sProMInsR { get; }
        decimal sFfUfmipFinanced { get; }
        E_PercentBaseT sProMInsT { get; }
        bool sProMInsLckd { get; }
        decimal sProMIns { get; }
        decimal Get_sProMIns_ByMonth(int months);

        decimal sHouseVal { get; }
        decimal sApprValFor_LTV_CLTV_HCLTV { get; }
        decimal sLAmtCalc { get; }
        E_sLT sLT { get; }
    }

    /// <summary>
    /// The data required to calculate payments for a Graduated Payment Mortgage (GPM).
    /// </summary>
    public interface IAmortizationGraduatedPmtDataProvider
    {
        int sGradPmtYrs { get; }
        decimal sGradPmtR { get; }
    }

    /// <summary>
    /// The data required to calculate an Adjustable Rate Mortgage (ARM).
    /// </summary>
    public interface IAmortizationArmDataProvider
    {
        decimal sRAdj1stCapR { get; }
        int sRAdj1stCapMon { get; }
        decimal sRAdjCapR { get; }
        int sRAdjCapMon { get; }
        decimal sRAdjLifeCapR { get; }
        decimal sRAdjIndexR { get; }
        decimal sRAdjMarginR { get; }
        decimal sRAdjFloorR { get; }
        decimal sRAdjRoundToR { get; }
        E_sRAdjRoundT sRAdjRoundT { get; }
    }

    /// <summary>
    /// The data required to calculate a negative amortization loan.
    /// </summary>
    public interface IAmortizationNegAmDataProvider
    {
        decimal sPmtAdjCapR { get; }
        int sPmtAdjCapMon { get; }
        int sPmtAdjRecastPeriodMon { get; }
        int sPmtAdjRecastStop { get; }
        decimal sPmtAdjMaxBalPc { get; }

        int sOptionArmMinPayPeriod { get; }
        int sOptionArmInitialFixMinPmtPeriod { get; }
        int sOptionArmIntroductoryPeriod { get; }
        bool sOptionArmMinPayIsIOOnly { get; }
        bool sIsFullAmortAfterRecast { get; }

        E_sOptionArmMinPayOptionT sOptionArmMinPayOptionT { get; }
        decimal sOptionArmTeaserR { get; }
        decimal sOptionArmNoteIRDiscount { get; }
        decimal sOptionArmPmtDiscount { get; }
    }

    /// <summary>
    /// The extra data requried to calculate the payments for the construction phase of a loan.
    /// </summary>
    public interface IAmortizationConstructionDataProvider
    {
        bool sIsIntReserveRequired { get; }
        ConstructionIntCalcType sConstructionIntCalcT { get; }
        ConstructionPhaseIntAccrual sConstructionPhaseIntAccrualT { get; }
        int sConstructionInitialOddDays { get; }
    }

    public interface IAmortizationDataProvider :
        IAmortizationBaseDataProvider,
        IAmortizationPrepaymentDataProvider,
        IAmortizationBuydownPaymentDataProvider,
        IAmortizationMiDataProvider,
        IAmortizationGraduatedPmtDataProvider,
        IAmortizationArmDataProvider,
        IAmortizationNegAmDataProvider,
        IAmortizationConstructionDataProvider
    {
    }

    public class AmortTableDataStub : IAmortizationDataProvider
    {
        public AmortTableDataStub(LosConvert m_convertLos)
        {
            this.m_convertLos = m_convertLos;
        }

        public LosConvert m_convertLos { get; set; }

        public decimal Get_sProMIns_ByMonth(int month)
        {
            // It would be nice to be able to include AOB in the tests...
            return this.sProMIns;
        }

        public int sTerm { set; get; }
        public int sDue { set; get; }
        public bool sIsConstructionLoanWithConstructionRefiData { set; get; }
        public int sPpmtMon { set; get; }
        public decimal sPpmtAmt { set; get; }
        public decimal sPpmtOneAmt { set; get; }
        public int sPpmtStartMon { set; get; }
        public int sPpmtOneMon { set; get; }
        public int sBuydwnMon1 { set; get; }
        public int sBuydwnMon2 { set; get; }
        public int sBuydwnMon3 { set; get; }
        public int sBuydwnMon4 { set; get; }
        public int sBuydwnMon5 { set; get; }
        public decimal sBuydwnR1 { set; get; }
        public decimal sBuydwnR2 { set; get; }
        public decimal sBuydwnR3 { set; get; }
        public decimal sBuydwnR4 { set; get; }
        public decimal sBuydwnR5 { set; get; }

        public int sIOnlyMon { set; get; }
        public decimal sFinalLAmt { set; get; }
        public E_sFinMethT sFinMethT { set; get; }
        public int sPmtAdjCapMon { set; get; }
        public int sGradPmtYrs { set; get; }
        public int sProMInsMon { set; get; }
        public int sProMIns2Mon { set; get; }
        public decimal sProMIns2 { set; get; }
        public decimal sProMInsCancelLtv { set; get; }
        public decimal sProMInsCancelAppraisalLtv { set; get; }
        public bool sProMInsMidptCancel { set; get; }
        public int sProMInsCancelMinPmts { set; get; }
        public decimal sFfUfmipR { set; get; }
        public bool sFfUfMipIsBeingFinanced { set; get; }
        public E_sLT sLT { set; get; }
        public bool sRAdjWorstIndex { set; get; }
        public decimal sRAdjIndexR { set; get; }
        public decimal sNoteIR { set; get; }
        public bool sIsOptionArm { set; get; }
        public int sOptionArmMinPayPeriod { set; get; }
        public int sOptionArmInitialFixMinPmtPeriod { set; get; }
        public bool sIsFullAmortAfterRecast { set; get; }
        public bool sOptionArmMinPayIsIOOnly { set; get; }
        public int sPmtAdjRecastPeriodMon { set; get; }
        public decimal sPmtAdjMaxBalPc { set; get; }
        public int sOptionArmIntroductoryPeriod { set; get; }
        public E_sOptionArmMinPayOptionT sOptionArmMinPayOptionT { set; get; }
        public decimal sOptionArmTeaserR { set; get; }
        public int sRAdj1stCapMon { set; get; }
        public decimal sRAdjMarginR { set; get; }

        public decimal sRAdj1stCapR { set; get; }
        public int sRAdjCapMon { set; get; }
        public decimal sRAdjCapR { set; get; }
        public decimal sRAdjLifeCapR { set; get; }
        public decimal sRAdjFloorR { set; get; }

        public decimal sRAdjRoundToR { set; get; }
        public E_sRAdjRoundT sRAdjRoundT { set; get; }

        public decimal sGradPmtR { set; get; }
        public decimal sOptionArmNoteIRDiscount { set; get; }
        public decimal sOptionArmPmtDiscount { set; get; }
        public decimal sPmtAdjCapR { set; get; }
        public int sPmtAdjRecastStop { set; get; }
        public CDateTime sSchedDueD1 { set; get; }

        public decimal sHouseVal { set; get; }
        public decimal sApprValFor_LTV_CLTV_HCLTV { get; set; } 
        public decimal sLAmtCalc { set; get; }

        public decimal sProMInsR { set; get; }
        public decimal sFfUfmipFinanced { set; get; }
        public E_PercentBaseT sProMInsT { set; get; }
        public bool sProMInsLckd { set; get; }
        public decimal sProMIns { set; get; }

        public bool sIsIntReserveRequired { set; get; }
        public ConstructionIntCalcType sConstructionIntCalcT { set; get; }
        public ConstructionPhaseIntAccrual sConstructionPhaseIntAccrualT { set; get; }
        public int sConstructionInitialOddDays { set; get; }
    }

    public class AmortDataLoanCache : IAmortizationDataProvider
    {
        private CPageBase m_loanData;

        public AmortDataLoanCache(CPageBase dataLoan)
        {

            Tools.Assert(dataLoan.IsRunningPricingEngine, "Should only use new amortization calc in pricing");

            // Should not be needed once we cache out 
            m_loanData = dataLoan;

            #region Populate Fields
            sTerm = dataLoan.sTerm;
            sIsConstructionLoanWithConstructionRefiData = dataLoan.sIsConstructionLoanWithConstructionRefiData;
            sDue = dataLoan.sDue;
            sPpmtMon = dataLoan.sPpmtMon;
            sPpmtAmt = dataLoan.sPpmtAmt;
            sPpmtOneAmt = dataLoan.sPpmtOneAmt;
            sPpmtStartMon = dataLoan.sPpmtStartMon;
            sPpmtOneMon = dataLoan.sPpmtOneMon;
            sBuydwnMon1 = dataLoan.sBuydwnMon1;
            sBuydwnMon2 = dataLoan.sBuydwnMon2;
            sBuydwnMon3 = dataLoan.sBuydwnMon3;
            sBuydwnMon4 = dataLoan.sBuydwnMon4;
            sBuydwnMon5 = dataLoan.sBuydwnMon5;
            sBuydwnR1 = dataLoan.sBuydwnR1;
            sBuydwnR2 = dataLoan.sBuydwnR2;
            sBuydwnR3 = dataLoan.sBuydwnR3;
            sBuydwnR4 = dataLoan.sBuydwnR4;
            sBuydwnR5 = dataLoan.sBuydwnR5;

            sIOnlyMon = dataLoan.sIOnlyMon;
            sFinalLAmt = dataLoan.sFinalLAmt;
            sFinMethT = dataLoan.sFinMethT;
            sPmtAdjCapMon = dataLoan.sPmtAdjCapMon;
            sGradPmtYrs = dataLoan.sGradPmtYrs;
            sProMInsMon = dataLoan.sProMInsMon;
            sProMIns2Mon = dataLoan.sProMIns2Mon;
            sProMIns2 = dataLoan.sProMIns2;
            sProMInsCancelLtv = dataLoan.sProMInsCancelLtv;
            sProMInsCancelAppraisalLtv = dataLoan.sProMInsCancelAppraisalLtv;
            sProMInsMidptCancel = dataLoan.sProMInsMidptCancel;
            sProMInsCancelMinPmts = dataLoan.sProMInsCancelMinPmts;
            sFfUfmipR = dataLoan.sFfUfmipR;
            sFfUfMipIsBeingFinanced = dataLoan.sFfUfMipIsBeingFinanced;
            sLT = dataLoan.sLT;
            sRAdjWorstIndex = dataLoan.sRAdjWorstIndex;
            sRAdjIndexR = dataLoan.sRAdjIndexR;
            sNoteIR = dataLoan.sNoteIR;
            sIsOptionArm = dataLoan.sIsOptionArm;
            sOptionArmMinPayPeriod = dataLoan.sOptionArmMinPayPeriod;
            sOptionArmInitialFixMinPmtPeriod = dataLoan.sOptionArmInitialFixMinPmtPeriod;
            sIsFullAmortAfterRecast = dataLoan.sIsFullAmortAfterRecast;
            sOptionArmMinPayIsIOOnly = dataLoan.sOptionArmMinPayIsIOOnly;
            sPmtAdjRecastPeriodMon = dataLoan.sPmtAdjRecastPeriodMon;
            sPmtAdjMaxBalPc = dataLoan.sPmtAdjMaxBalPc;
            sOptionArmIntroductoryPeriod = dataLoan.sOptionArmIntroductoryPeriod;
            sOptionArmMinPayOptionT = dataLoan.sOptionArmMinPayOptionT;
            sOptionArmTeaserR = dataLoan.sOptionArmTeaserR;
            sRAdj1stCapMon = dataLoan.sRAdj1stCapMon;
            sRAdjMarginR = dataLoan.sRAdjMarginR;

            sRAdj1stCapR = dataLoan.sRAdj1stCapR;
            sRAdjCapMon = dataLoan.sRAdjCapMon;
            sRAdjCapR = dataLoan.sRAdjCapR;
            sRAdjLifeCapR = dataLoan.sRAdjLifeCapR;
            sRAdjFloorR = dataLoan.sRAdjFloorR;

            sRAdjRoundToR = dataLoan.sRAdjRoundToR;
            sRAdjRoundT = dataLoan.sRAdjRoundT;

            sGradPmtR = dataLoan.sGradPmtR;
            sOptionArmNoteIRDiscount = dataLoan.sOptionArmNoteIRDiscount;
            sOptionArmPmtDiscount = dataLoan.sOptionArmPmtDiscount;
            sPmtAdjCapR = dataLoan.sPmtAdjCapR;
            sPmtAdjRecastStop = dataLoan.sPmtAdjRecastStop;
            sSchedDueD1 = dataLoan.sSchedDueD1;
            sSchedDueD1_rep = dataLoan.sSchedDueD1_rep;

            sApprValFor_LTV_CLTV_HCLTV = dataLoan.sApprValFor_LTV_CLTV_HCLTV;
            sHouseVal = dataLoan.sHouseVal_PricingAmort;

            sLAmtCalc = dataLoan.sLAmtCalc;

            sProMInsR = dataLoan.sProMInsR;
            sFfUfmipFinanced = dataLoan.sFfUfmipFinanced;
            sProMInsT = dataLoan.sProMInsT;
            sProMInsLckd = dataLoan.sProMInsLckd;
            sProMIns = dataLoan.sProMIns;

            if (dataLoan.sIsConstructionLoan)
            {
                sIsIntReserveRequired = dataLoan.sIsIntReserveRequired;
                sConstructionIntCalcT = dataLoan.sConstructionIntCalcT;
                sConstructionPhaseIntAccrualT = dataLoan.sConstructionPhaseIntAccrualT;
                sConstructionInitialOddDays = dataLoan.sConstructionInitialOddDays;
            }
            #endregion
        }

        public LosConvert m_convertLos
        {
            get { return m_loanData.m_convertLos; }
        }
        public decimal Get_sProMIns_ByMonth(int month)
        {
            return m_loanData.Get_sProMIns_ByMonth(month, this);
        }

        public int sTerm { private set; get; }
        public int sDue { private set; get; }
        public bool sIsConstructionLoanWithConstructionRefiData { private set; get; }
        public int sPpmtMon { private set; get; }
        public decimal sPpmtAmt { private set; get; }
        public decimal sPpmtOneAmt { private set; get; }
        public int sPpmtStartMon { private set; get; }
        public int sPpmtOneMon { private set; get; }
        public int sBuydwnMon1 { private set; get; }
        public int sBuydwnMon2 { private set; get; }
        public int sBuydwnMon3 { private set; get; }
        public int sBuydwnMon4 { private set; get; }
        public int sBuydwnMon5 { private set; get; }
        public decimal sBuydwnR1 { private set; get; }
        public decimal sBuydwnR2 { private set; get; }
        public decimal sBuydwnR3 { private set; get; }
        public decimal sBuydwnR4 { private set; get; }
        public decimal sBuydwnR5 { private set; get; }

        public int sIOnlyMon { private set; get; }
        public decimal sFinalLAmt { private set; get; }
        public E_sFinMethT sFinMethT { private set; get; }
        public int sPmtAdjCapMon { private set; get; }
        public int sGradPmtYrs { private set; get; }
        public int sProMInsMon { private set; get; }
        public int sProMIns2Mon { private set; get; }
        public decimal sProMIns2 { private set; get; }
        public decimal sProMInsCancelLtv { private set; get; }
        public decimal sProMInsCancelAppraisalLtv { private set; get; }
        public bool sProMInsMidptCancel { private set; get; }
        public int sProMInsCancelMinPmts { private set; get; }
        public decimal sFfUfmipR { private set; get; }
        public bool sFfUfMipIsBeingFinanced { private set; get; }
        public E_sLT sLT { private set; get; }
        public bool sRAdjWorstIndex { private set; get; }
        public decimal sRAdjIndexR { private set; get; }
        public decimal sNoteIR { private set; get; }
        public bool sIsOptionArm { private set; get; }
        public int sOptionArmMinPayPeriod { private set; get; }
        public int sOptionArmInitialFixMinPmtPeriod { private set; get; }
        public bool sIsFullAmortAfterRecast { private set; get; }
        public bool sOptionArmMinPayIsIOOnly { private set; get; }
        public int sPmtAdjRecastPeriodMon { private set; get; }
        public decimal sPmtAdjMaxBalPc { private set; get; }
        public int sOptionArmIntroductoryPeriod { private set; get; }
        public E_sOptionArmMinPayOptionT sOptionArmMinPayOptionT { private set; get; }
        public decimal sOptionArmTeaserR { private set; get; }
        public int sRAdj1stCapMon { private set; get; }
        public decimal sRAdjMarginR { private set; get; }

        public decimal sRAdj1stCapR { private set; get; }
        public int sRAdjCapMon { private set; get; }
        public decimal sRAdjCapR { private set; get; }
        public decimal sRAdjLifeCapR { private set; get; }
        public decimal sRAdjFloorR { private set; get; }

        public decimal sRAdjRoundToR { private set; get; }
        public E_sRAdjRoundT sRAdjRoundT { private set; get; }

        public decimal sGradPmtR { private set; get; }
        public decimal sOptionArmNoteIRDiscount { private set; get; }
        public decimal sOptionArmPmtDiscount { private set; get; }
        public decimal sPmtAdjCapR { private set; get; }
        public int sPmtAdjRecastStop { private set; get; }
        public CDateTime sSchedDueD1 { private set; get; }
        public string sSchedDueD1_rep { private set; get; }

        public decimal sHouseVal { private set; get; }
        public decimal sApprValFor_LTV_CLTV_HCLTV { private set; get; }
        public decimal sLAmtCalc { private set; get; }

        public decimal sProMInsR { private set; get; }
        public decimal sFfUfmipFinanced { private set; get; }
        public E_PercentBaseT sProMInsT { private set; get; }
        public bool sProMInsLckd { private set; get; }
        public decimal sProMIns { private set; get; }
        public bool sIsRenovationLoan { get; set; }
        public decimal sApprVal { get; set; }
        public E_sLPurposeT sLPurposeT { get; set; }
        public decimal sPurchPrice { get; set; }
        public decimal sTotalRenovationCostsPeVal { get; set; }

        public bool sIsIntReserveRequired { set; get; }
        public ConstructionIntCalcType sConstructionIntCalcT { set; get; }
        public ConstructionPhaseIntAccrual sConstructionPhaseIntAccrualT { set; get; }
        public int sConstructionInitialOddDays { set; get; }

        private string GetProps()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            Action<string, string> addLog = (name, content) =>
                sb.AppendLine(name + ":" + content);

            addLog("sNoteIR", sNoteIR.ToString());
            addLog("sHouseVal", sHouseVal.ToString());
            addLog("sTerm", sTerm.ToString());
            addLog("sDue", sDue.ToString());
            addLog("sIsConstructionLoanWithConstructionRefiData", sIsConstructionLoanWithConstructionRefiData.ToString());
            addLog("sPpmtMon", sPpmtMon.ToString());
            addLog("sPpmtAmt", sPpmtAmt.ToString());
            addLog("sPpmtOneAmt", sPpmtOneAmt.ToString());
            addLog("sPpmtStartMon", sPpmtStartMon.ToString());
            addLog("sPpmtOneMon", sPpmtOneMon.ToString());
            addLog("sBuydwnMon1", sBuydwnMon1.ToString());
            addLog("sBuydwnMon2", sBuydwnMon2.ToString());
            addLog("sBuydwnMon3", sBuydwnMon3.ToString());
            addLog("sBuydwnMon4", sBuydwnMon4.ToString());
            addLog("sBuydwnMon5", sBuydwnMon5.ToString());
            addLog("sBuydwnR1", sBuydwnR1.ToString());
            addLog("sBuydwnR2", sBuydwnR2.ToString());
            addLog("sBuydwnR3", sBuydwnR3.ToString());
            addLog("sBuydwnR4", sBuydwnR4.ToString());
            addLog("sBuydwnR5", sBuydwnR5.ToString());

            addLog("sIOnlyMon", sIOnlyMon.ToString());
            addLog("sFinalLAmt", sFinalLAmt.ToString());
            addLog("sFinMethT", sFinMethT.ToString());
            addLog("sPmtAdjCapMon", sPmtAdjCapMon.ToString());
            addLog("sGradPmtYrs", sGradPmtYrs.ToString());
            addLog("sProMInsMon", sProMInsMon.ToString());
            addLog("sProMIns2Mon", sProMIns2Mon.ToString());
            addLog("sProMIns2", sProMIns2.ToString());
            addLog("sProMInsCancelLtv", sProMInsCancelLtv.ToString());
            addLog("sProMInsCancelAppraisalLtv", sProMInsCancelAppraisalLtv.ToString());
            addLog("sProMInsMidptCancel", sProMInsMidptCancel.ToString());
            addLog("sProMInsCancelMinPmts", sProMInsCancelMinPmts.ToString());
            addLog("sFfUfmipR", sFfUfmipR.ToString());
            addLog("sFfUfMipIsBeingFinanced", sFfUfMipIsBeingFinanced.ToString());
            addLog("sLT", sLT.ToString());
            addLog("sRAdjWorstIndex", sRAdjWorstIndex.ToString());
            addLog("sRAdjIndexR", sRAdjIndexR.ToString());
            addLog("sIsOptionArm", sIsOptionArm.ToString());
            addLog("sOptionArmMinPayPeriod", sOptionArmMinPayPeriod.ToString());
            addLog("sOptionArmInitialFixMinPmtPeriod", sOptionArmInitialFixMinPmtPeriod.ToString());
            addLog("sIsFullAmortAfterRecast", sIsFullAmortAfterRecast.ToString());
            addLog("sOptionArmMinPayIsIOOnly", sOptionArmMinPayIsIOOnly.ToString());
            addLog("sPmtAdjRecastPeriodMon", sPmtAdjRecastPeriodMon.ToString());
            addLog("sPmtAdjMaxBalPc", sPmtAdjMaxBalPc.ToString());
            addLog("sOptionArmIntroductoryPeriod", sOptionArmIntroductoryPeriod.ToString());
            addLog("sOptionArmMinPayOptionT", sOptionArmMinPayOptionT.ToString());
            addLog("sOptionArmTeaserR", sOptionArmTeaserR.ToString());
            addLog("sRAdj1stCapMon", sRAdj1stCapMon.ToString());
            addLog("sRAdjMarginR", sRAdjMarginR.ToString());

            addLog("sRAdj1stCapR", sRAdj1stCapR.ToString());
            addLog("sRAdjCapMon", sRAdjCapMon.ToString());
            addLog("sRAdjCapR", sRAdjCapR.ToString());
            addLog("sRAdjLifeCapR", sRAdjLifeCapR.ToString());
            addLog("sRAdjFloorR", sRAdjFloorR.ToString());

            addLog("sRAdjRoundToR", sRAdjRoundToR.ToString());
            addLog("sRAdjRoundT", sRAdjRoundT.ToString());

            addLog("sGradPmtR", sGradPmtR.ToString());
            addLog("sOptionArmNoteIRDiscount", sOptionArmNoteIRDiscount.ToString());
            addLog("sOptionArmPmtDiscount", sOptionArmPmtDiscount.ToString());
            addLog("sPmtAdjCapR", sPmtAdjCapR.ToString());
            addLog("sPmtAdjRecastStop", sPmtAdjRecastStop.ToString());
            addLog("sSchedDueD1", sSchedDueD1.ToStringWithDefaultFormatting());
            addLog("sSchedDueD1_rep", sSchedDueD1_rep.ToString());

            addLog("sLAmtCalc", sLAmtCalc.ToString());

            addLog("sProMInsR", sProMInsR.ToString());
            addLog("sFfUfmipFinanced", sFfUfmipFinanced.ToString());
            addLog("sProMInsT", sProMInsT.ToString());
            addLog("sProMInsLckd", sProMInsLckd.ToString());
            addLog("sProMIns", sProMIns.ToString());

            addLog("sIsRenovationLoan", this.sIsRenovationLoan.ToString());
            addLog("sApprVal", this.sApprVal.ToString());
            addLog("sLPurposeT", this.sLPurposeT.ToString());
            addLog("sPurchPrice", this.sPurchPrice.ToString());
            addLog("sTotalRenovationCostsPeVal", this.sTotalRenovationCostsPeVal.ToString());

            return sb.ToString();
        }

    }
}