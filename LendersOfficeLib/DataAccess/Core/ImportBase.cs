namespace DataAccess
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using LendersOffice.Security;

    public class CImportPageData : CPageData
    {
        public CImportPageData(string pageName, Guid fileId, CSelectStatementProvider provider)
            : base(fileId)
        {
            m_pageBase = new CImportPageBase(provider, fileId, pageName);
            // 5/14/2014 gf - opm 181293, assign to InternalCPageData to 
            // enable field tracking for the disclosure events.
            m_pageBase.InternalCPageData = this;
        }

        /// <summary>
        /// Special Behavior when Importing from fannie and point
        /// </summary>
        class CImportPageBase : CPageBaseWrapped
        {
            private bool m_useTList = false;
            private Hashtable m_tList = new Hashtable();

            // 1/6/2014 dd - OPM 148640 - Always get sTerm / sDue from import file.
            // 4/22/14 gf - opm 179500 - always get sUnitsNum from import file.
            // 7/6/2016 ml - opm 245481 - convert to hashset, always get declarations from import file
            private readonly HashSet<string> AlwaysGetFromExternalFields = new HashSet<string>()
            {
                "aBEmplmtXmlContent",
                "aCEmplmtXmlContent",
                "sPreparerXmlContent",
                "sAgentXmlContent",
                "sCondXmlContent",
                "aVorXmlContent",
                "aLiaXmlContent",
                "aVaPastLXmlContent",
                "aReXmlContent",
                "aPublicRecordXmlContent",
                "aAssetXmlContent",
                "aReTotNetRentI",
                "aReTotHExp",
                "aReTotMPmt",
                "aReTotGrossRentI",
                "aReTotMAmt",
                "aReTotVal",
                "aLiaPdOffTot",
                "aLiaMonTot",
                "aLiaBalTot",
                "aAsstLiqTot",
                "aAsstNonReSolidTot",
                "aPresTotHExpLckd",
                "sBranchId",
                "sLNm",
                "aO1I",
                "aO1IDesc",
                "aO2I",
                "aO2IDesc",
                "aO3I",
                "aO3IDesc",
                "sLPurposeT",
                "sTerm",
                "sDue",
                "sUnitsNum",
                "sProdSpStructureT",
                "sLenderCaseNum",
                "sAdjustmentListJson",
                "sEstCloseD",
                "sSchedDueD1",
                "sHousingExpenseJsonContent",
                "aBDecJudgment",
                "aCDecJudgment",
                "aBDecBankrupt",
                "aCDecBankrupt",
                "aBDecForeclosure",
                "aCDecForeclosure",
                "aBDecLawsuit",
                "aCDecLawsuit",
                "aBDecObligated",
                "aCDecObligated",
                "aBDecDelinquent",
                "aCDecDelinquent",
                "aBDecAlimony",
                "aCDecAlimony",
                "aBDecBorrowing",
                "aCDecBorrowing",
                "aBDecEndorser",
                "aCDecEndorser",
                "aBDecCitizen",
                "aCDecCitizen",
                "aBDecResidency",
                "aCDecResidency",
                "aBDecOcc",
                "aCDecOcc",
                "aBDecPastOwnership",
                "aCDecPastOwnership",
                "aBDecPastOwnedPropT",
                "aCDecPastOwnedPropT",
                "aBDecPastOwnedPropTitleT",
                "aCDecPastOwnedPropTitleT",
                "sNumFinancedProperties",
                "aBIsAsian",
                "aCIsAsian",
                "aBIsAsianIndian",
                "aCIsAsianIndian",
                "aBIsChinese",
                "aCIsChinese",
                "aBIsFilipino",
                "aCIsFilipino",
                "aBIsJapanese",
                "aCIsJapanese",
                "aBIsKorean",
                "aCIsKorean",
                "aBIsVietnamese",
                "aCIsVietnamese",
                "aBIsOtherAsian",
                "aCIsOtherAsian",
                "aBOtherAsianDescription",
                "aCOtherAsianDescription",
                "aBIsWhite",
                "aCIsWhite",
                "aBIsBlack",
                "aCIsBlack",
                "aBIsAmericanIndian",
                "aCIsAmericanIndian",
                "aBOtherAmericanIndianDescription",
                "aCOtherAmericanIndianDescription",
                "aBIsPacificIslander",
                "aCIsPacificIslander",
                "aBIsNativeHawaiian",
                "aCIsNativeHawaiian",
                "aBIsGuamanianOrChamorro",
                "aCIsGuamanianOrChamorro",
                "aBIsSamoan",
                "aCIsSamoan",
                "aBIsOtherPacificIslander",
                "aCIsOtherPacificIslander",
                "aBOtherPacificIslanderDescription",
                "aCOtherPacificIslanderDescription",
                "aBHispanicT",
                "aCHispanicT",
                "aBIsMexican",
                "aCIsMexican",
                "aBIsPuertoRican",
                "aCIsPuertoRican",
                "aBIsCuban",
                "aCIsCuban",
                "aBIsOtherHispanicOrLatino",
                "aCIsOtherHispanicOrLatino",
                "aBOtherHispanicOrLatinoDescription",
                "aCOtherHispanicOrLatinoDescription",
                "aBDoesNotWishToProvideEthnicity",
                "aCDoesNotWishToProvideEthnicity",
                "aBDoesNotWishToProvideRace",
                "aCDoesNotWishToProvideRace",
                "aBNoFurnish",
                "aBNoFurnishLckd",
                "aCNoFurnish",
                "aCNoFurnishLckd",
                "aBGender",
                "aCGender",
                "aBInterviewMethodT",
                "aCInterviewMethodT",
                "aBEthnicityCollectedByObservationOrSurname",
                "aCEthnicityCollectedByObservationOrSurname",
                "aBSexCollectedByObservationOrSurname",
                "aCSexCollectedByObservationOrSurname",
                "aBRaceCollectedByObservationOrSurname",
                "aCRaceCollectedByObservationOrSurname",
                "aOtherIncomeXmlContent",

                // OPM 469839
                // Property type and MH advantage indicator should
                // pull from the import file. Updating sFannieSpT
                // will also update sGseSpT, so both must be present
                // in this list.
                "sFannieSpT",
                "sGseSpT",
                "sHomeIsMhAdvantageTri",
            };

            public CImportPageBase(CSelectStatementProvider selectProvider, Guid loanId, string sPageName)
                : base(loanId, sPageName, selectProvider)
            {

                
                m_useTList = LendersOffice.Admin.BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId).ForceTemplateOnImport;
            }

            // 5/14/2014 gf - opm 181293, override to prevent logging audits during 
            // the import process prior to save.
            protected override bool RecordAuditEvent()
            {
                return sAuditTrackedFieldChanges;
            }

            #region Logic functions

            /// <summary>
            /// Returns true if the field should be set and false if it should not.
            /// For now, if we're set to use the t-list, we check if the field exists
            /// in the t-list. If it does, we return false. If it does not, we check
            /// the current value of the field. If there's data, we return false. Else,
            /// we return true. If we are not set to use the t-list, we simply return
            /// true.
            /// </summary>
            /// <param name="fieldName"></param>
            /// <returns></returns>
            private bool SharedLogic(string fieldName, bool templateDataIsBlank)
            {
                bool overwriteTemplateDataWithExternalData = true;

                if (m_useTList)
                {
                    if (this.AlwaysGetFromExternalFields.Contains(fieldName))
                    {
                        overwriteTemplateDataWithExternalData = true;
                    }
                    else if (m_tList.ContainsKey(fieldName))
                        overwriteTemplateDataWithExternalData = false;
                    else if (templateDataIsBlank)
                        overwriteTemplateDataWithExternalData = true;
                    else
                        overwriteTemplateDataWithExternalData = false;
                }
                return overwriteTemplateDataWithExternalData;
            }


            protected override bool IsRequireFieldSet(string strFieldNm, string oldValue, string newValue)
            {
                bool overwrite = 0 != oldValue.CompareTo(newValue);
                if (overwrite)
                    overwrite = Logic(strFieldNm, oldValue);
                return overwrite;
            }
            private bool Logic(string fieldName, string oldValue)
            {
                bool templateDataIsBlank = (oldValue == null || oldValue == "");
                return SharedLogic(fieldName, templateDataIsBlank);
            }


            protected override bool IsRequireFieldSet(string strFieldNm, int oldValue, int newValue)
            {
                bool overwrite = oldValue != newValue;
                if (overwrite)
                    overwrite = Logic(strFieldNm, (int?)oldValue);
                return overwrite;
            }

            protected override bool IsRequireFieldSet(string strFieldNm, int? oldValue, int? newValue)
            {
                bool overwrite = oldValue != newValue;
                if (overwrite)
                    overwrite = Logic(strFieldNm, oldValue);
                return overwrite;
            }

            private bool Logic(string fieldName, int? oldValue)
            {
                // 6/1/2009 dd - OPM 30665 - aBMaritalStatT, aCMaritalStatT, aBAddrT, aCAddrT default enum values are non-zero.
                bool templateDataIsBlank = false;

                if (fieldName == "aBMaritalStatT" || fieldName == "aCMaritalStatT")
                {
                    templateDataIsBlank = oldValue == (int)E_aBMaritalStatT.LeaveBlank;
                }
                else if (fieldName == "aBAddrT" || fieldName == "aCAddrT" || fieldName == "aBPrev1AddrT" || fieldName == "aBPrev2AddrT" ||
                    fieldName == "aCPrev1AddrT" || fieldName == "aCPrev2AddrT")
                {
                    templateDataIsBlank = oldValue == (int)E_aBAddrT.LeaveBlank;
                }
                else
                {
                    templateDataIsBlank = oldValue == 0 || oldValue == null;
                }


                return SharedLogic(fieldName, templateDataIsBlank);
            }


            protected override bool IsRequireFieldSet(string strFieldNm, decimal oldValue, decimal newValue)
            {
                bool overwrite = oldValue != newValue;
                if (overwrite)
                    overwrite = Logic(strFieldNm, (decimal?)oldValue);
                return overwrite;
            }
            protected override bool IsRequireFieldSet(string strFieldNm, decimal? oldValue, decimal? newValue)
            {
                bool overwrite = oldValue != newValue;
                if (overwrite)
                    overwrite = Logic(strFieldNm, oldValue);
                return overwrite;
            }
            private bool Logic(string fieldName, decimal? oldValue)
            {
                bool templateDataIsBlank = !oldValue.HasValue || (oldValue == 0m);

                return SharedLogic(fieldName, templateDataIsBlank);
            }


            protected override bool IsRequireFieldSet(string strFieldNm, long oldValue, long newValue)
            {
                bool overwrite = oldValue != newValue;
                if (overwrite)
                    overwrite = Logic(strFieldNm, oldValue);
                return overwrite;
            }
            private bool Logic(string fieldName, long oldValue)
            {
                bool templateDataIsBlank = (oldValue == 0);

                return SharedLogic(fieldName, templateDataIsBlank);
            }


            protected override bool IsRequireFieldSet(string strFieldNm, DateTime oldValue, DateTime newValue)
            {
                bool overwrite = 0 != oldValue.CompareTo(newValue);
                if (overwrite)
                    overwrite = Logic(strFieldNm, oldValue);
                return overwrite;
            }
            private bool Logic(string fieldName, DateTime oldValue)
            {
                bool templateDataIsBlank = (oldValue.Ticks == 0);

                return SharedLogic(fieldName, templateDataIsBlank);
            }


            protected override bool IsRequireFieldSet(string strFieldNm, CDateTime oldValue, CDateTime newValue)
            {
                bool overwrite = !oldValue.SameAs(newValue);
                if (overwrite)
                    overwrite = Logic(strFieldNm, oldValue);
                return overwrite;
            }
            private bool Logic(string fieldName, CDateTime oldValue)
            {
                bool templateDataIsBlank = (oldValue == null || !oldValue.IsValid);
                return SharedLogic(fieldName, templateDataIsBlank);
            }


            protected override bool IsRequireFieldSet(string strFieldNm, bool oldValue, bool newValue)
            {
                bool overwrite = oldValue != newValue;
                if (overwrite)
                    overwrite = Logic(strFieldNm, oldValue);
                return overwrite;
            }
            private bool Logic(string fieldName, bool oldValue)
            {
                bool templateDataIsBlank = (!oldValue);

                return SharedLogic(fieldName, templateDataIsBlank);
            }


            protected override bool IsRequireFieldSet(string strFieldNm, Guid oldValue, Guid newValue)
            {
                bool overwrite = 0 != oldValue.CompareTo(newValue);
                if (overwrite)
                    overwrite = Logic(strFieldNm, oldValue);
                return overwrite;
            }
            private bool Logic(string fieldName, Guid oldValue)
            {
                bool templateDataIsBlank = (0 == oldValue.CompareTo(Guid.Empty));

                return SharedLogic(fieldName, templateDataIsBlank);
            }


            protected override bool IsRequireFieldSet(string strFieldNm, Enum oldValue, Enum newValue)
            {
                bool overwrite = oldValue != newValue;
                if (overwrite)
                    overwrite = Logic(strFieldNm, oldValue);
                return overwrite;
            }
            private bool Logic(string fieldName, Enum oldValue)
            {
                bool templateDataIsBlank = oldValue == null || (oldValue.ToString("D") == "0");

                return SharedLogic(fieldName, templateDataIsBlank);
            }

            protected override bool IsRequireFieldSet(string fieldName, byte[] oldValue, byte[] newValue)
            {
                var overwrite = !Tools.AreArraysIdentical<byte>(oldValue, newValue);
                if (overwrite)
                {
                    overwrite = Logic(fieldName, oldValue);
                }
                return overwrite;
            }

            private bool Logic(string fieldName, byte[] oldValue)
            {
                var templateDataBlank = oldValue == null;
                return SharedLogic(fieldName, templateDataBlank);
            }

            #endregion

            protected override IPreparerFields GetPreparerFields(int iRecord)
            {
                return new CPreparerFieldsForImport(this, sPreparerDataSet, iRecord);
            }
            public override CAgentFields GetAgentFields(int iRecord)
            {
                return new CAgentFieldsForImport(this, sAgentDataSet, iRecord);
            }
            public override CAgentFields GetAgentFields(Guid id)
            {
                return new CAgentFieldsForImport(this, sAgentDataSet, id);
            }
        }

    }
}