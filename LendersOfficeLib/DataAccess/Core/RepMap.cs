/// Author: David Dao

using System;
using LendersOffice.Common;

namespace DataAccess
{
    /// <summary>
    /// A representation mapping container is needed for properly translating internal field values into their ui-readable form.
    /// </summary>
    internal abstract class AbstractRepMap : IPair 
    {
        /// <summary>
        /// Binary mapping
        /// 
        /// Each enum field needs a corresponding string representation to show in the ui.  We maintain
        /// lists of mappings for ui drop down init and
        /// report query condition exchange.
        /// </summary>
        private string m_key;
        private string m_val;
        public string Key 
        {
            get { return m_key; }
        }
        public string Val 
        {
            get { return m_val; }
        }
        protected AbstractRepMap(string key, string val) 
        {
            m_key = key;
            m_val = val;
        }
    }

    internal class EnumRepMap : AbstractRepMap
    {
        private Enum m_eCode;

        public Enum EnumCode
        {
            get { return m_eCode; }
        }

        /// <summary>
        /// Construct enum mapping entry.  We take the integer form
        /// of the enum member and cast it to a string.  We use this
        /// pairing to maintain a relationship between an enum field
        /// and the corresponding ui representation of the field.
        /// </summary>
        /// <param name="eCode">
        /// Enum member.
        /// </param>
        /// <param name="sDescription">
        /// Enum description
        /// </param>
        public EnumRepMap( Enum eCode , string sDescription ) : base(eCode.ToString("D"), sDescription)
        {
            m_eCode = eCode;
        }

        public static EnumRepMap Create<TEnum>(System.Collections.Generic.IReadOnlyDictionary<TEnum, string> descriptionLookup, TEnum enumValue) where TEnum : struct, IConvertible
        {
            // boxing is forced here due to the usage of System.Enum.
            // per http://stackoverflow.com/a/40952937/2946652 the C# spec says that this conversion always boxes the value
            return new EnumRepMap((Enum)(object)enumValue, descriptionLookup[enumValue]);
        }
    }

    internal class IntegerRepMap : AbstractRepMap
    {
        /// <summary>
        /// Construct int mapping entry so we can cast it to a string.
        /// We use this pairing to maintain a relationship between an
        /// integer encoding and the corresponding ui representation
        /// of the flag.
        /// </summary>
        /// <param name="iCode">
        /// Integer value.
        /// </param>
        /// <param name="sDescription">
        /// Integer description
        /// </param>
        public IntegerRepMap( int iCode , string sDescription ) : base(iCode.ToString("D"), sDescription)
        {
        }

    }

    internal class BoolRepMap : AbstractRepMap
    {
        /// <summary>
        /// Construct bool mapping entry.  We take the integer form
        /// of the bool member and cast it to a string.  We use this
        /// pairing to maintain a relationship between a boolean flag
        /// and the corresponding ui representation of the flag.  I
        /// expect that the mapping should always consist of two
        /// entries in the set.
        /// </summary>
        /// <param name="bCode">
        /// Boolean value.
        /// </param>
        /// <param name="sDescription">
        /// Bool description
        /// </param>
        public BoolRepMap(bool bCode , string sDescription) : base(bCode ? "1" : "0", sDescription)
        {
        }

    }

}
