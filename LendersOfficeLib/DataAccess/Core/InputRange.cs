using System;
using System.Data;
using System.Collections;
using System.Reflection;


namespace DataAccess
{
	/// <summary>
	/// 7/06/2006 mb - Attribute that can be added to 
	/// a particular field in the loan.  It should
	/// allow for a min and max value per field and
	/// any values outside should give an appropriate
	/// error message through javascript.
	/// </summary>
	
	[ AttributeUsage( AttributeTargets.Property )
	]
	public class InputRangeAttribute : Attribute
	{
		public decimal Min { get; private set;}

		public decimal Max { get; private set;}

		public decimal Default { get; private set;}
 
		public string Clusive { get; private set;}

		/// <summary>
		/// Constructor including inclusive or exclusive value.
		/// </summary>
		
		public InputRangeAttribute( string sMin, string sMax, E_clusive clusive )
		{
			Min = Decimal.Parse(sMin);
			Max = Decimal.Parse(sMax);
			Default = Min;

			if ( clusive == E_clusive.Inclusive )
			{
				Clusive = "inc";
			}
			else if ( clusive == E_clusive.Exclusive )
			{
				Clusive = "exc";
				if (Min < Max)
					Default++;
			}
			
			if ( Clusive == null )
			{
				Tools.LogError("Invalid Enumerated Type within InputRangeAttribute constructor.");
			}
		}

		/// <summary>
		/// Constructor including inclusive or exclusive value and default value
		/// </summary>
		
		public InputRangeAttribute( string sMin, string sMax, E_clusive clusive, string sDefault )
		{
			Min = Decimal.Parse(sMin);
			Max = Decimal.Parse(sMax);
			Default = Decimal.Parse(sDefault);

			if ( clusive == E_clusive.Inclusive )
			{
				Clusive = "inc";
			}
			else if ( clusive == E_clusive.Exclusive )
			{
				Clusive = "exc";
			}

			if ( Clusive == null )
			{
				Tools.LogError("Invalid Enumerated Type within InputRangeAttribute constructor.");
			}
		}
		
	}
}
