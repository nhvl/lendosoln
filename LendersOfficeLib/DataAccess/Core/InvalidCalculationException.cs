﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{
    public class InvalidCalculationException : CBaseException
    {
        public InvalidCalculationException(string userMessage, string developerMessage) :
            base(userMessage, developerMessage)
        {
            this.IsEmailDeveloper = false;
        }
    }
}
