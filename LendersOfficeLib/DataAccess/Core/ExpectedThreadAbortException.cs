﻿// <copyright file="ExpectedThreadAbortException.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Scott Kibler
//    Date:   8/27/2014
// </summary>
namespace DataAccess
{
    /// <summary>
    /// For Response.Redirect(someUrl, true) which throws a ThreadAbortException, until we have a way to turn off page event handlers.
    /// </summary>
    public class ExpectedThreadAbortException : CBaseException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExpectedThreadAbortException" /> class.
        /// </summary>
        /// <param name="usrMessage">The message for the user.</param>
        /// <param name="innerException">The inner exception.</param>
        public ExpectedThreadAbortException(string usrMessage, System.Exception innerException)
            : base(usrMessage, innerException)
        {
            this.IsEmailDeveloper = false;
        }
    }
}
