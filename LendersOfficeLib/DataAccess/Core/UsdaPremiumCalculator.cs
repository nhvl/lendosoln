﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Common;

namespace DataAccess
{
    /// <summary>
    /// The algorithm for calculate annual fee for USDA is at OPM 63549.
    /// </summary>
    public class UsdaPremiumCalculator
    {

        public static List<decimal> Calculate(decimal sLAmt, decimal sNoteIR, decimal annualMipRate, decimal payment, int sDue)
        {

            List<decimal> list = ComputeAnnualAverageBalance(sLAmt, sNoteIR, payment, sDue);
            List<decimal> monthlyPremiumList = new List<decimal>(list.Count);
            foreach (var o in list)
            {
                decimal annualMip = Math.Round(o * annualMipRate / 100, 2, MidpointRounding.AwayFromZero);
                decimal monthly = Math.Round(annualMip / 12, 2, MidpointRounding.AwayFromZero);
                monthlyPremiumList.Add(monthly);
            }
            return monthlyPremiumList;
        }
        public static List<decimal> ComputeAnnualAverageBalance(decimal outstandingBalance, decimal interestRate, decimal payment, int dueMonths)
        {
            if (outstandingBalance <= 0 || interestRate <= 0 || payment <= 0)
            {
                throw new CBaseException(ErrorMessages.Generic, "Invalid arguments for UsdaPremiumCalculator. OutstandingBalance=" + outstandingBalance + ", InterestRate=" + interestRate + ", Payment=" + payment);
            }

            List<decimal> annualAverageBalanceList = new List<decimal>(30);

            // 5/4/2009 dd - The logic is from Computation of Annual Average Oustanding Balance.
            // https://usdalinc.sc.egov.usda.gov/docs/rd/sfh/annualfee/Guaranteed_Annual_Fee_Calculation.pdf
            // a. Multiply previous balance times interest rate. Round the result to 2 decimal places based on value in 3rd decimal
            // b. Divide result by 1200.Round the result to 2 decimal places based on value in 3rd decimal.
            // c. Add previous balance.
            // d. Subtract p&i payment.

            decimal previousBalance = outstandingBalance;
            decimal totalAmount = previousBalance;
            int iMonthCount = 1;
            int oddMonths = dueMonths % 12;
            int finalMonth = (oddMonths == 0) ? dueMonths : dueMonths + (12 - oddMonths); // Pad to a full year if there are odd months

            while (iMonthCount <= finalMonth)
            {
                decimal stepA = previousBalance * interestRate;
                decimal stepB = Math.Round(stepA / 1200, 2, MidpointRounding.AwayFromZero);
                decimal stepC = previousBalance + stepB;
                decimal stepD = Math.Max(stepC - payment, 0.0M);
                previousBalance = stepD;
                if (iMonthCount % 12 == 0)
                {
                    annualAverageBalanceList.Add(totalAmount / 12);
                    totalAmount = 0.0M;
                }
                totalAmount += stepD;

                iMonthCount++;
            }

            return annualAverageBalanceList;

        }

    }
}
