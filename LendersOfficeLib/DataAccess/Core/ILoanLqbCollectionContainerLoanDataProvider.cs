﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using LendersOffice.Migration;
    using LendingQB.Core;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides access to the loan data that needs to be accessed by the aggregate.
    /// </summary>
    public interface ILoanLqbCollectionContainerLoanDataProvider
    {
        /// <summary>
        /// Gets a value indicating whether the loan is a refi.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        bool sIsRefinancing { get; }

        /// <summary>
        /// Gets a value indicating whether a cash deposit should be included in the assets totals.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        bool sIncludeCashDepositInAssetTotals { get; }

        /// <summary>
        /// Gets a value indicating whether the ulad and legacy applications should be kept in sync with respect to the attached borrowers.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field naming convention.")]
        bool sSyncUladAndLegacyApps { get; }

        /// <summary>
        /// Gets a value indicating whether ULAD applications are in use.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field naming convention.")]
        bool sUsingUladApplications { get; }

        /// <summary>
        /// Gets the consumer identifiers that are active (i.e., borrowers and cobborrower when marked as defined).
        /// </summary>
        IEnumerable<DataObjectIdentifier<DataObjectKind.Consumer, Guid>> ActiveConsumerIdentifiers { get; }

        /// <summary>
        /// Gets the legacy applications.
        /// </summary>
        IReadOnlyLqbCollection<DataObjectKind.LegacyApplication, Guid, ILegacyApplication> LegacyApplications { get; }

        /// <summary>
        /// Gets the legacy application to consumer associations.
        /// </summary>
        IReadOnlyLqbAssociationSet<DataObjectKind.LegacyApplicationConsumerAssociation, Guid, ILegacyApplicationConsumerAssociation> LegacyApplicationConsumers { get; }

        /// <summary>
        /// Gets the legacy application to consumer associations for consumers that do not formally exist, but
        /// are on the legacy application due to the two-consumer object design.
        /// </summary>
        IReadOnlyList<ILegacyApplicationConsumerAssociation> LegacyApplicationInvalidConsumers { get; }

        /// <summary>
        /// Gets the consumers for the loan.
        /// </summary>
        IReadOnlyOrderedLqbCollection<DataObjectKind.Consumer, Guid, IConsumer> Consumers { get; }

        /// <summary>
        /// Gets the consumers for a legacy application.
        /// </summary>
        /// <param name="appId">The identifier for the legacy application.</param>
        /// <param name="borrowerId">Out parameter, the identifier for the borrower.</param>
        /// <param name="coborrowerId">Out parameter, the identifier for the coborrower.</param>
        /// <param name="isCoborrowerDefined">Out parameter, true if the coborrower is defined, false otherwise.</param>
        void GetConsumersForLegacyApplication(
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId,
            out DataObjectIdentifier<DataObjectKind.Consumer, Guid> borrowerId,
            out DataObjectIdentifier<DataObjectKind.Consumer, Guid> coborrowerId,
            out bool isCoborrowerDefined);

        /// <summary>
        /// When ULAD collections change, the cache machinery needs to be notified.
        /// </summary>
        /// <param name="collectionName">The name of the collection.</param>
        void NotifyCollectionChange(string collectionName);
    }
}
