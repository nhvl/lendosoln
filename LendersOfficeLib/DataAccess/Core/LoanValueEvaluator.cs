﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Globalization;
    using System.Linq;
    using System.Text.RegularExpressions;
    using DataAccess.PathDispatch;
    using ConfigSystem.Engine;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes.PathDispatch;
    using System.Diagnostics;
    using LendersOffice.HttpModule;
    using LqbGrammar.Drivers.SecurityEventLogging;

    public class LoanValueEvaluator : IEngineValueEvaluator
    {
        private static string[] s_specialFields = {
                                                      "isextassigned", // Need to refactor this value.
                                                      "editassignedunderwriter",
                                                      "isloan", // Need to change the name to isTemplate
                                                      "CanWriteNonAssignedLoan",
                                                      "EditAssignedProcessor",
                                                      "AccessClosedLoan",
                                                      "LimitTemplateToBranch",
                                                      "PriceMyLoan",
                                                      "Scope",
                                                      "IsAssigned",
                                                      "HaveUnderwriter",
                                                      "HaveProcessor",
                                                      "Role",
                                                      "Lien",
                                                      "actasratelocked",

                                                      "IsPmlAssigned", // New variable for Hack rule
                                                      "IsPmlExternalAssigned", // New variable for Hack rule
                                                      "IsSameBranchAsLoanFile", // New variable for Hack Rule
                                                      "IsBranchAllowLOEditLoanUntilUW", // new variable for Hack Rule.
                                                      "AlwaysTrue", // Always return True.
                                                      "GetAssignAsRole",
                                                      "sLpTemplateIdNonEmpty",
                                                      "OCGroup",
                                                      "BranchGroup",
                                                      "UserGroup"
                                                  };


        private SortedList<string, bool> m_loanFieldList = new SortedList<string, bool>(StringComparer.OrdinalIgnoreCase);
        private LoanFileCache m_loanFileCache = null;
        private CPageData m_dataLoan = null;
        private CAppData m_dataApp = null;
        private ExecutingEnginePrincipal m_principal;
        private NonLoanValueEvaluator m_nonLoanValues = new NonLoanValueEvaluator();
        private bool logLoanValueEvaluatorGets = ConstStage.LogLoanValueEvaluatorGets;

        public static string GetCacheKey(Guid loanId, int version)
        {
            return "LoanValueEvaluator_" + loanId + "_" + version;
        }

        public ExecutingEnginePrincipal CurrentPrincipal
        {
            get { return m_principal; }
        }

        public void SetEvaluatingPrincipal(ExecutingEnginePrincipal principal)
        {
            if (null == principal)
            {
                throw CBaseException.GenericException("principal is null in LoanValueEvaluator.SetEvaluatingPrincipal");
            }

            m_principal = principal;
            if (m_loanFileCache != null)
            {
                m_loanFileCache.SetEvaluatingPrincipal(principal);
            }
        }

        public void SetNonLoanValues(NonLoanValueEvaluator nonLoanValues)
        {
            m_nonLoanValues = nonLoanValues;
        }

        public string sLNm
        {
            get
            {
                if (m_loanFileCache != null)
                {
                    return m_loanFileCache.sLNm;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Pass null for fieldNameTranslator if you have dataRow contains same column name as fieldList.
        /// </summary>
        /// <param name="principal"></param>
        /// <param name="sLId"></param>
        /// <param name="dataRow"></param>
        /// <param name="fieldList"></param>
        /// <param name="fieldNameTranslator"></param>
        public LoanValueEvaluator(Guid sLId, DataRow dataRow, IEnumerable<string> fieldList, Func<string, string> fieldNameTranslator)
        {
            m_loanFileCache = new LoanFileCache(sLId, dataRow, fieldList, fieldNameTranslator);
            InitLoad();
        }

        public LoanValueEvaluator(Guid brokerId, Guid sLId, params WorkflowOperation[] operationList)
        {
            IEnumerable<string> fieldList = LendingQBExecutingEngine.GetDependencyFieldsByOperation(brokerId, operationList);
            Initialize(null, sLId, fieldList);
        }

        public LoanValueEvaluator(DbConnectionInfo connInfo, Guid brokerId, Guid sLId, params WorkflowOperation[] operationList)
        {
            IEnumerable<string> fieldList = LendingQBExecutingEngine.GetDependencyFieldsByOperation(brokerId, operationList);
            Initialize(connInfo, sLId, fieldList);
        }

        public LoanValueEvaluator(IEnumerable<string> fieldList, Guid sLId)
        {
            Initialize(null, sLId, fieldList);
        }

        public LoanValueEvaluator(DbConnectionInfo connInfo, IEnumerable<string> fieldList, Guid sLId)
        {
            Initialize(connInfo, sLId, fieldList);
        }

        /// <summary>
        /// Evaluates worklow values on existing loan and application references.
        /// </summary>
        /// <param name="dataLoan">
        /// The loan to evaluate values.
        /// </param>
        /// <param name="dataApp">
        /// The app to evaluate values.
        /// </param>
        /// <remarks>
        /// Use caution when employing this constructor for workflow evaluation.
        /// Both references must contain all the field dependencies required 
        /// for evaluation, and the loan should bypass field security checks and 
        /// have the format target of <see cref="FormatTarget.Webform"/> for
        /// consistent value formats.
        /// </remarks>
        internal LoanValueEvaluator(CPageData dataLoan, CAppData dataApp)
        {
            m_dataLoan = dataLoan;
            m_dataApp = dataApp;
            m_loanFileCache = null;
            m_loanFieldList = null;
        }

        private void InitLoad()
        {
            if (m_dataLoan != null)
            {
                m_dataLoan.SetFormatTarget(FormatTarget.Webform);
                m_dataLoan.AllowLoadWhileQP2Sandboxed = true;
                m_dataLoan.ByPassFieldSecurityCheck = true;
                m_dataLoan.InitLoad();
                m_dataApp = m_dataLoan.GetAppData(0); // 9/8/2010 dd - Only get primary app only.
            }
        }

        private void Initialize(DbConnectionInfo connInfo, Guid sLId, IEnumerable<string> fieldList)
        {
            SortedList<string, bool> loanFileCacheFieldList = new SortedList<string, bool>(StringComparer.OrdinalIgnoreCase);
            foreach (var fieldName in fieldList)
            {
                string fieldNameWrite = fieldName;
                if (s_specialFields.Contains(fieldNameWrite, StringComparer.OrdinalIgnoreCase) == true)
                {
                    // 9/8/2010 dd - Skip special field.
                    continue;
                }
                if (fieldNameWrite.StartsWith("today+", StringComparison.OrdinalIgnoreCase) || fieldNameWrite.StartsWith("today-", StringComparison.OrdinalIgnoreCase))
                {
                    // 12/20/2011 dd - OPM 75432 We support the following special keyword  day+N  or day-N
                    continue;
                }
                if (fieldNameWrite.Equals("sLId", StringComparison.OrdinalIgnoreCase))
                {
                    continue;
                }

                if (PathSchemaManager.IsSchemaDependency(fieldNameWrite) && !m_loanFieldList.ContainsKey(fieldNameWrite))
                {
                    m_loanFieldList.Add(fieldNameWrite, true);
                    continue;
                }

                //Remap to lead source id.
                if (fieldNameWrite.Equals(ParameterReporting.LEAD_SOURCE_ENUM_FIELD_ID, StringComparison.OrdinalIgnoreCase))
                {
                    fieldNameWrite = ParameterReporting.LEAD_SOURCE_ENUM_REMAP_ID;
                }

                //Remap status progress field.
                if (fieldNameWrite.Equals(ParameterReporting.STATUS_PROGRESS_ENUM_FIELD_ID, StringComparison.OrdinalIgnoreCase))
                {
                    fieldNameWrite = ParameterReporting.STATUS_PROGRESS_ENUM_REMAP_ID;
                }

                CFieldDbInfo dbField = CFieldDbInfoList.TheOnlyInstance.Get(fieldNameWrite);
                if (null != dbField && dbField.IsCachedField)
                {
                    if (loanFileCacheFieldList.ContainsKey(fieldNameWrite) == false)
                    {
                        loanFileCacheFieldList.Add(fieldNameWrite, true);
                    }
                    continue;
                }
                if (PageDataUtilities.ContainsField(fieldNameWrite))
                {
                    if (m_loanFieldList.ContainsKey(fieldNameWrite) == false)
                    {
                        m_loanFieldList.Add(fieldNameWrite, true);
                    }
                    continue;
                }
                else if (fieldName.StartsWith("sf") || fieldName.StartsWith("af"))
                {
                    // 12/2/2011 dd - This will allow field name such as sfGetDistinctConditionCategory to
                    // include in dependency list.
                    if (m_loanFieldList.ContainsKey(fieldNameWrite) == false)
                    {
                        m_loanFieldList.Add(fieldNameWrite, true);
                    }
                    continue;
                }

                throw new NotFoundException(ErrorMessages.Generic, fieldName + " is not valid field in LoanValueEvaluator");

            }

            if (loanFileCacheFieldList.Count > 0)
            {
                if (null == connInfo)
                {

                    m_loanFileCache = new LoanFileCache(sLId, loanFileCacheFieldList.Keys);
                }
                else
                {
                    m_loanFileCache = new LoanFileCache(connInfo, sLId, fieldList);
                }
            }

            if (m_loanFieldList.Count > 0)
            {
                m_dataLoan = new NotEnforceAccessControlPageData(sLId, m_loanFieldList.Keys);
            }

            InitLoad();
        }

        #region Implement Get Values
        public IList<bool> GetBoolValues(string fieldName)
        {
            using (PerformanceMonitorItem.Time($"GetBoolValues:{fieldName}", this.logLoanValueEvaluatorGets))
            {
                List<bool> list = new List<bool>();

                if (string.IsNullOrEmpty(fieldName) == true)
                {
                    return list;
                }

                if (m_nonLoanValues.IsSpecialBoolFieldName(fieldName))
                {
                    list.AddRange(m_nonLoanValues.GetBoolValues(fieldName));
                    return list;
                }

                fieldName = fieldName.ToLower();
                if (fieldName == "alwaystrue")
                {
                    list.Add(true);
                }
                else if (fieldName == "slptemplateidnonempty")
                {
                    list.Add(m_loanFileCache != null && m_loanFileCache.sLpTemplateIdNonEmpty);
                }
                else if (fieldName == "editassignedprocessor")
                {
                    list.Add(CurrentPrincipal.IsLOAllowedToEditProcessorAssignedFile);
                }
                else if (fieldName == "isextassigned" || fieldName == "ispmlexternalassigned")
                {
                    bool bValue = false;
                    if (CurrentPrincipal.ApplicationType == E_ApplicationT.PriceMyLoan)
                    {

                        switch (CurrentPrincipal.PmlLevelAccess)
                        {
                            case E_PmlLoanLevelAccess.Individual:
                                bValue = false;
                                break;
                            case E_PmlLoanLevelAccess.Supervisor:
                                bValue = IsPmlUserManagerOfLoan();
                                break;
                            case E_PmlLoanLevelAccess.Corporate:
                                bValue = IsUserIsInSamePmlBroker();
                                break;
                            default:
                                break;
                        }
                    }

                    list.Add(bValue);
                }
                else if (fieldName == "isbranchallowloeditloanuntiluw")
                {
                    string cacheKey = "BranchIsAllowLOEditLoanUntilUnderwriting_" + m_principal.BranchId;
                    string allowLoOfficerEdit = CurrentContextCache.Get(cacheKey) as string;

                    if (null == allowLoOfficerEdit)
                    {
                        bool bTemp = false;
                        SqlParameter[] parameters = {
                                                    new SqlParameter("@BranchId", m_principal.BranchId)
                                                };
                        using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(m_principal.ConnectionInfo, "RetrieveBranchIsAllowLOEditLoanUntilUnderwriting", parameters))
                        {
                            if (reader.Read())
                                bTemp = true;
                            else
                                bTemp = false;
                        }
                        allowLoOfficerEdit = bTemp ? "T" : "F";
                        CurrentContextCache.Set(cacheKey, allowLoOfficerEdit);
                    }

                    list.Add(allowLoOfficerEdit == "T");
                }
                else if (fieldName == "issamebranchasloanfile")
                {
                    list.Add(m_loanFileCache != null && m_loanFileCache.IsSameBranchAsLoanFile);
                }
                else if (fieldName == "editassignedunderwriter")
                {
                    list.Add(CurrentPrincipal.IsOthersAllowedToEditUnderwriterAssignedFile);
                }
                else if (fieldName == "canwritenonassignedloan")
                {
                    list.Add(CurrentPrincipal.HasPermission(Permission.CanWriteNonAssignedLoan));
                }
                else if (fieldName == "accessclosedloan")
                {
                    list.Add(CurrentPrincipal.HasPermission(Permission.CanAccessClosedLoans));
                }
                else if (fieldName == "limittemplatetobranch")
                {
                    list.Add(CurrentPrincipal.HasPermission(Permission.AllowAccessToTemplatesOfAnyBranch) == false);
                }
                else if (fieldName == "pricemyloan")
                {
                    list.Add(CurrentPrincipal.HasFeatures(E_BrokerFeatureT.PriceMyLoan));
                }
                else if (fieldName == "ispmlassigned")
                {
                    bool bValue = false;
                    if (m_loanFileCache != null)
                    {
                        bValue = IsUserIsInSamePmlBroker() &&
                            (m_loanFileCache.GetAssignmentFor(E_RoleT.Pml_BrokerProcessor) == CurrentPrincipal.EmployeeId ||
                            m_loanFileCache.GetAssignmentFor(E_RoleT.Pml_LoanOfficer) == CurrentPrincipal.EmployeeId ||
                            m_loanFileCache.GetAssignmentFor(E_RoleT.Pml_Secondary) == CurrentPrincipal.EmployeeId ||
                            m_loanFileCache.GetAssignmentFor(E_RoleT.Pml_PostCloser) == CurrentPrincipal.EmployeeId);
                    }

                    list.Add(bValue);
                }
                else if (fieldName == "isassigned")
                {
                    list.Add(m_loanFileCache != null && this.CurrentPrincipal.GetRoles().Any(role => m_loanFileCache.GetAssignmentFor(role) == CurrentPrincipal.EmployeeId));
                }
                else if (fieldName == "haveunderwriter")
                {
                    list.Add(m_loanFileCache != null && m_loanFileCache.HaveUnderwriterAssigned);
                }
                else if (fieldName == "haveprocessor")
                {
                    list.Add(m_loanFileCache != null && m_loanFileCache.HaveProcessorAssigned);
                }
                else if (fieldName.StartsWith(DataPath.PathHeader, StringComparison.OrdinalIgnoreCase))
                {
                    DataPath path = DataPath.Create(fieldName);
                    IEnumerable<object> results = PathResolver.GetUsingSchema(m_dataLoan.sBrokerId, m_dataLoan, path);

                    foreach (object obj in results)
                    {
                        if (obj is string)
                        {
                            bool value;
                            string str = (string)obj;
                            if (bool.TryParse(str, out value))
                            {
                                list.Add(value);
                            }
                            else
                            {
                                list.Add(str.Equals("Yes", StringComparison.OrdinalIgnoreCase));
                            }
                        }
                        else
                        {
                            list.Add((bool)obj); // Will throw if wrong type.
                        }
                    }
                }
                else if (m_loanFileCache != null && m_loanFileCache.ContainsField(fieldName))
                {
                    bool bValue;
                    m_loanFileCache.TryGetBool(fieldName, out bValue);
                    list.Add(bValue);
                }
                else if (m_loanFieldList != null && m_loanFieldList.ContainsKey(fieldName))
                {
                    string str = PageDataUtilities.GetValue(m_dataLoan, m_dataApp, fieldName);
                    list.Add(str.Equals("Yes", StringComparison.OrdinalIgnoreCase));
                }
                else if (m_loanFileCache == null && m_loanFieldList == null && m_dataLoan != null && m_dataApp != null)
                {
                    // 12/22/2011 dd - m_loanFileCache and m_loanFieldList are null only when we pass in CPagedata and CAppData directly.
                    string str = PageDataUtilities.GetValue(m_dataLoan, m_dataApp, fieldName);
                    list.Add(str.Equals("Yes", StringComparison.OrdinalIgnoreCase));

                }
                else
                {
                    throw new CBaseException(ErrorMessages.Generic, "Unhandle " + fieldName + " in GetBoolValues()");
                }

                return list;
            }
        }

        private bool IsPmlUserAssignedToLoan()
        {
            return this.CurrentPrincipal.EmployeeId == this.m_loanFileCache.GetAssignmentFor(E_RoleT.Pml_LoanOfficer)
                || this.CurrentPrincipal.EmployeeId == this.m_loanFileCache.GetAssignmentFor(E_RoleT.Pml_BrokerProcessor)
                || this.CurrentPrincipal.EmployeeId == this.m_loanFileCache.GetAssignmentFor(E_RoleT.Pml_Secondary)
                || this.CurrentPrincipal.EmployeeId == this.m_loanFileCache.GetAssignmentFor(E_RoleT.Pml_PostCloser);
        }

        private bool IsPmlUserManagerOfLoan()
        {
            Guid pmlExternalBrokerProcessorManagerEmployeeId;
            Guid externalManagerId;

            Guid externalSecondaryManagerEmployeeId;
            Guid externalPostCloserManagerEmployeeId;

            m_loanFileCache.TryGetGuid("PmlExternalBrokerProcessorManagerEmployeeId", out pmlExternalBrokerProcessorManagerEmployeeId);
            m_loanFileCache.TryGetGuid("PmlExternalManagerEmployeeId", out externalManagerId);
            m_loanFileCache.TryGetGuid("PmlExternalSecondaryManagerEmployeeId", out externalSecondaryManagerEmployeeId);
            m_loanFileCache.TryGetGuid("PmlExternalPostCloserManagerEmployeeId", out externalPostCloserManagerEmployeeId);

            return pmlExternalBrokerProcessorManagerEmployeeId == CurrentPrincipal.EmployeeId || 
                externalManagerId == CurrentPrincipal.EmployeeId ||
                externalSecondaryManagerEmployeeId == CurrentPrincipal.EmployeeId ||
                externalPostCloserManagerEmployeeId == CurrentPrincipal.EmployeeId;
        }

        private bool IsUserIsInSamePmlBroker()
        {
            Guid sPmlBrokerId;
            m_loanFileCache.TryGetGuid("sPmlBrokerId", out sPmlBrokerId);
            return CurrentPrincipal.PmlBrokerId == sPmlBrokerId;
        }

        public IList<int> GetIntValues(string fieldName)
        {
            return GetIntValuesImpl(fieldName, true);
        }

        public int GetSafeScalarInt(string fieldName, int defaultValue)
        {
            IList<int> list = GetIntValuesImpl(fieldName, false);
            if (list.Count == 0)
            {
                return defaultValue;
            }
            else
            {
                return list[0];
            }
        }

        private IList<int> GetIntValuesImpl(string fieldName, bool isThrowExceptionWhenNotFound)
        {
            using (PerformanceMonitorItem.Time($"GetIntValuesImpl:{fieldName}", this.logLoanValueEvaluatorGets))
            {
                List<int> list = new List<int>();

                if (string.IsNullOrEmpty(fieldName) == true)
                {
                    return list;
                }

                if (m_nonLoanValues.IsSpecialIntFieldName(fieldName))
                {
                    return m_nonLoanValues.GetIntValues(fieldName);
                }

                if (fieldName.Equals(ParameterReporting.LEAD_SOURCE_ENUM_FIELD_ID, StringComparison.OrdinalIgnoreCase))
                {
                    fieldName = ParameterReporting.LEAD_SOURCE_ENUM_REMAP_ID;
                }

                if (fieldName.Equals(ParameterReporting.STATUS_PROGRESS_ENUM_FIELD_ID, StringComparison.OrdinalIgnoreCase))
                {
                    fieldName = ParameterReporting.STATUS_PROGRESS_ENUM_REMAP_ID;
                }

                if (fieldName.Equals("Role", StringComparison.OrdinalIgnoreCase))
                {
                    // OPM 241675 - 03/23/2017 - je - Evaluate all roles in single IN operation.
                    list.AddRange(this.CurrentPrincipal.GetRoles().Cast<int>());
                }
                else if (fieldName.Equals("GetAssignAsRole", StringComparison.OrdinalIgnoreCase))
                {
                    foreach (var role in m_loanFileCache.GetAssignAsRole())
                    {
                        list.Add((int)role);
                    }
                }
                else if (fieldName.Equals("actasratelocked", StringComparison.OrdinalIgnoreCase))
                {
                    list.Add((int)m_loanFileCache.ActAsRateLockedT);
                }
                else if (fieldName.Equals("Lien", StringComparison.OrdinalIgnoreCase))
                {
                    // 9/14/2010 dd - A stand-along second is consider to be first lien.
                    int value;
                    E_sLienPosT lienPosT = E_sLienPosT.Second;
                    if (m_loanFileCache.TryGetInt("sLienPosT", out value))
                    {
                        lienPosT = (E_sLienPosT)value;
                        if (lienPosT == E_sLienPosT.Second)
                        {
                            Guid loanBrokerId = Guid.Empty;
                            DbConnectionInfo connInfo = DbConnectionInfo.GetConnectionInfoByLoanId(m_loanFileCache.sLId, out loanBrokerId);

                            SqlParameter[] parameters = {
                                                        new SqlParameter("@srcLoanId", m_loanFileCache.sLId)
                                                    };

                            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "RetrieveLinkedLoanId", parameters))
                            {
                                if (reader.Read() == false)
                                {
                                    lienPosT = E_sLienPosT.First; // Change to First for stand alone 2nd.
                                }
                            }
                        }
                    }
                    list.Add((int)lienPosT);

                }
                else if (fieldName.Equals("Scope", StringComparison.OrdinalIgnoreCase))
                {
                    E_ScopeT scope = E_ScopeT.CrossBroker;

                    Guid loanBrokerId = Guid.Empty;
                    m_loanFileCache.TryGetGuid("sBrokerId", out loanBrokerId);

                    if (loanBrokerId == CurrentPrincipal.BrokerId)
                    {
                        // Key the check for P-User scope off of the user type, not the application type,
                        // as B-Users running embedded PML will have a PriceMyLoan application type.
                        if (string.Equals(this.CurrentPrincipal.UserType, "P", StringComparison.OrdinalIgnoreCase))
                        {
                            scope = this.GetPUserScope();
                        }
                        else
                        {
                            scope = this.GetBUserScope();
                        }
                    }
                    list.Add((int)scope);
                }
                else if (fieldName.StartsWith(DataPath.PathHeader, StringComparison.OrdinalIgnoreCase))
                {
                    DataPath path = DataPath.Create(fieldName);
                    IEnumerable<object> results = PathResolver.GetUsingSchema(m_dataLoan.sBrokerId, m_dataLoan, path);

                    foreach (object obj in results)
                    {
                        if (obj is string)
                        {
                            string str = (string)obj;

                            str = str.Replace(",", "").Replace("$", "").Replace("%", "");
                            int value;

                            if (int.TryParse(str, out value))
                            {
                                list.Add(value);
                            }
                        }
                        else
                        {
                            list.Add(Convert.ToInt32(obj)); // Will throw if wrong type.
                        }
                    }
                }
                else if (m_loanFileCache != null && m_loanFileCache.ContainsField(fieldName))
                {
                    int value;
                    if (m_loanFileCache.TryGetInt(fieldName, out value))
                    {
                        list.Add(value);
                    }
                }
                else if (m_loanFieldList != null && m_loanFieldList.ContainsKey(fieldName))
                {
                    string str = PageDataUtilities.GetValue(m_dataLoan, m_dataApp, fieldName);

                    str = str.Replace(",", "").Replace("$", "").Replace("%", "");
                    int value;

                    if (int.TryParse(str, out value))
                    {
                        list.Add(value);
                    }
                }
                else if (m_loanFileCache == null && m_loanFieldList == null && m_dataLoan != null && m_dataApp != null)
                {
                    // 12/22/2011 dd - m_loanFileCache and m_loanFieldList are null only when we pass in CPagedata and CAppData directly.
                    string str = PageDataUtilities.GetValue(m_dataLoan, m_dataApp, fieldName);

                    str = str.Replace(",", "").Replace("$", "").Replace("%", "");
                    int value;

                    if (int.TryParse(str, out value))
                    {
                        list.Add(value);
                    }

                }

                return list;
            }
        }

        private E_ScopeT GetPUserScope()
        {
            var isAssignedToLoan = this.IsPmlUserAssignedToLoan();
            if (this.CurrentPrincipal.PmlLevelAccess == E_PmlLoanLevelAccess.Individual && isAssignedToLoan)
            {
                return E_ScopeT.InScope;
            }

            var isManagerOfLoan = this.IsPmlUserManagerOfLoan();
            if (this.CurrentPrincipal.PmlLevelAccess == E_PmlLoanLevelAccess.Supervisor && (isAssignedToLoan || isManagerOfLoan))
            {
                return E_ScopeT.InScope;
            }

            if (this.CurrentPrincipal.PmlLevelAccess == E_PmlLoanLevelAccess.Corporate && (isAssignedToLoan || isManagerOfLoan || this.IsUserIsInSamePmlBroker()))
            {
                return E_ScopeT.InScope;
            }

            return E_ScopeT.NonDuty;
        }

        private E_ScopeT GetBUserScope()
        {
            if (CurrentPrincipal.HasPermission(Permission.BrokerLevelAccess))
            {
                return E_ScopeT.InScope;
            }

            Guid loanBranchId = Guid.Empty;
            m_loanFileCache.TryGetGuid("sBranchId", out loanBranchId);
            if (CurrentPrincipal.HasPermission(Permission.BranchLevelAccess) && loanBranchId == CurrentPrincipal.BranchId)
            {
                return E_ScopeT.InScope;
            }

            if ((CurrentPrincipal.HasPermission(Permission.TeamLevelAccess) || CurrentPrincipal.HasPermission(Permission.BranchLevelAccess))
                && m_loanFileCache.IsTeamAssigned(CurrentPrincipal.GetTeams()))
            {
                return E_ScopeT.InScope;
            }

            if (this.CurrentPrincipal.GetRoles().Any(role => m_loanFileCache.GetAssignmentFor(role) == CurrentPrincipal.EmployeeId))
            {
                return E_ScopeT.InScope;
            }

            return E_ScopeT.NonDuty;
        }

        public bool HasValues(params string[] fieldNames)
        {
            return fieldNames.All(a => m_loanFileCache.ContainsField(a));
        }

        public float? GetSafeFloatValue(string fieldName)
        {
            try
            {
                var list = GetFloatValues(fieldName);
                if (list != null && list.Any())
                {
                    return list.First();
                }
            }
            catch (CBaseException cbe)
            {
                Tools.LogWarning("Tried to get a field that doesn't exist", cbe);
            }

            return null;
        }

        public IList<float> GetFloatValues(string fieldName)
        {
            using (PerformanceMonitorItem.Time($"GetFloatValues:{fieldName}", this.logLoanValueEvaluatorGets))
            {
                List<float> list = new List<float>();

                if (string.IsNullOrEmpty(fieldName) == true)
                {
                    return list;
                }

                if (m_nonLoanValues.IsSpecialFloatFieldName(fieldName))
                {
                    return m_nonLoanValues.GetFloatValues(fieldName);
                }

                if (fieldName.StartsWith(DataPath.PathHeader, StringComparison.OrdinalIgnoreCase))
                {
                    DataPath path = DataPath.Create(fieldName);
                    IEnumerable<object> results = PathResolver.GetUsingSchema(m_dataLoan.sBrokerId, m_dataLoan, path);

                    foreach (object obj in results)
                    {
                        if (obj is string)
                        {
                            string str = (string)obj;

                            str = str.Replace(",", "").Replace("$", "").Replace("%", "");
                            float value;

                            if (float.TryParse(str, out value))
                            {
                                list.Add(value);
                            }
                        }
                        else
                        {
                            list.Add(Convert.ToSingle(obj)); // Will throw if wrong type.
                        }
                    }
                }
                else if (m_loanFileCache != null && m_loanFileCache.ContainsField(fieldName))
                {
                    float value;
                    if (m_loanFileCache.TryGetFloat(fieldName, out value))
                    {
                        list.Add(value);
                    }
                }
                else if (m_loanFieldList != null && m_loanFieldList.ContainsKey(fieldName))
                {
                    string str = PageDataUtilities.GetValue(m_dataLoan, m_dataApp, fieldName);

                    str = str.Replace(",", "").Replace("$", "").Replace("%", "");
                    float value;

                    if (float.TryParse(str, out value))
                    {
                        list.Add(value);
                    }
                }
                else if (m_loanFileCache == null && m_loanFieldList == null && m_dataLoan != null && m_dataApp != null)
                {
                    // 12/22/2011 dd - m_loanFileCache and m_loanFieldList are null only when we pass in CPagedata and CAppData directly.
                    string str = PageDataUtilities.GetValue(m_dataLoan, m_dataApp, fieldName);

                    str = str.Replace(",", "").Replace("$", "").Replace("%", "");
                    float value;
                    // copied defaults for Single.TryParse using reflector. 

                    NumberStyles styles = NumberStyles.Float | NumberStyles.AllowThousands | NumberStyles.AllowParentheses;
                    if (float.TryParse(str, styles, NumberFormatInfo.CurrentInfo, out value))
                    {
                        list.Add(value);
                    }
                }
                else
                {
                    // 9/18/2014 AV - 192326 [185] [LoException]System.Exception-Unhandle [squalbottomr] in LoanValueEval...
                    if (fieldName.Equals("squalbottomr", StringComparison.OrdinalIgnoreCase))
                    {
                        list.Add(99999);
                    }
                }

                return list;
            }
        }
        public IList<DateTime> GetDateTimeValues(string fieldName)
        {
            using (PerformanceMonitorItem.Time($"GetDateTimeValues:{fieldName}", this.logLoanValueEvaluatorGets))
            {
                List<DateTime> list = new List<DateTime>();

                if (string.IsNullOrEmpty(fieldName) == true)
                {
                    return list;
                }

                if (m_nonLoanValues.IsSpecialDateTimeFieldName(fieldName))
                {
                    return m_nonLoanValues.GetDateTimeValues(fieldName);
                }

                if (fieldName.StartsWith("today+", StringComparison.OrdinalIgnoreCase))
                {
                    int nDays = 0;
                    if (int.TryParse(fieldName.Substring(6), out nDays) == false)
                    {
                        Tools.LogErrorWithCriticalTracking("Unhandle for [" + fieldName + "] in LoanValueEvaluator.GetDateTimeValues " + sLNm);
                        throw new CBaseException(ErrorMessages.Generic, "Unhandle " + fieldName + " in GetDateTimeValues()");
                    }
                    list.Add(DateTime.Today.AddDays(nDays));
                }
                else if (fieldName.StartsWith("today-", StringComparison.OrdinalIgnoreCase))
                {
                    int nDays = 0;
                    if (int.TryParse(fieldName.Substring(6), out nDays) == false)
                    {
                        Tools.LogErrorWithCriticalTracking("Unhandle for [" + fieldName + "] in LoanValueEvaluator.GetDateTimeValues " + sLNm);
                        throw new CBaseException(ErrorMessages.Generic, "Unhandle " + fieldName + " in GetDateTimeValues()");
                    }
                    list.Add(DateTime.Today.AddDays(-1 * nDays));
                }
                else if (fieldName.StartsWith(DataPath.PathHeader, StringComparison.OrdinalIgnoreCase))
                {
                    DataPath path = DataPath.Create(fieldName);
                    IEnumerable<object> results = PathResolver.GetUsingSchema(m_dataLoan.sBrokerId, m_dataLoan, path);

                    foreach (object obj in results)
                    {
                        string str = obj.ToString(); // Should be a valid date string if object is DateTime or CDateTime or a date's _rep value.

                        DateTime value;
                        if (string.IsNullOrEmpty(str))
                        {
                            list.Add(DateTime.MinValue);
                        }
                        else if (DateTime.TryParse(str, out value))
                        {
                            list.Add(value.Date);
                        }
                        else
                        {
                            Tools.LogBug(String.Format("Could not parse date {0} value {1}", fieldName, str));
                        }
                    }
                }
                else if (m_loanFileCache != null && m_loanFileCache.ContainsField(fieldName))
                {
                    DateTime value;
                    if (m_loanFileCache.TryGetDateTime(fieldName, out value))
                    {
                        list.Add(value.Date);
                    }
                }
                else if (m_loanFieldList != null && m_loanFieldList.ContainsKey(fieldName))
                {
                    DateTime value;
                    string str = PageDataUtilities.GetValue(m_dataLoan, m_dataApp, fieldName);

                    if (string.IsNullOrEmpty(str))
                    {
                        list.Add(DateTime.MinValue);
                    }
                    else if (DateTime.TryParse(str, out value))
                    {
                        list.Add(value.Date);
                    }
                    else
                    {
                        Tools.LogBug(String.Format("Could not parse date {0} value {1}", fieldName, str));
                    }
                }
                else if (m_loanFileCache == null && m_loanFieldList == null && m_dataLoan != null && m_dataApp != null)
                {
                    // 12/22/2011 dd - m_loanFileCache and m_loanFieldList are null only when we pass in CPagedata and CAppData directly.
                    DateTime value;
                    string str = PageDataUtilities.GetValue(m_dataLoan, m_dataApp, fieldName);

                    if (DateTime.TryParse(str, out value))
                    {
                        list.Add(value.Date);
                    }

                }

                return list;
            }
        }

        private bool GetGuidFromLoanFileCacheWithDataLoanFallback(string fieldName, Func<Guid> dataLoanGetter, out Guid value)
        {
            using (PerformanceMonitorItem.Time($"GetGuidFromLoanFileCacheWithDataLoanFallback:{fieldName}", this.logLoanValueEvaluatorGets))
            {
                value = Guid.Empty;
                if (m_loanFileCache != null && m_loanFileCache.TryGetGuid(fieldName, out value))
                {
                    return true;
                }
                else if (m_dataLoan != null)
                {
                    value = dataLoanGetter();
                    return true;
                }

                return false;
            }
        }

        public string GetSafeScalarString(string fieldName, string defaultValue)
        {
            IList<string> list = GetStringValuesImpl(fieldName, false);

            if (list.Count == 0)
            {
                return defaultValue;
            }
            else
            {
                return list[0];
            }
        }


        public IList<string> GetStringValues(string fieldName)
        {
            return GetStringValuesImpl(fieldName, true);
        }

        private IList<string> GetStringValuesImpl(string fieldName, bool isThrowExceptionWhenNotFound)
        {
            using (PerformanceMonitorItem.Time($"GetStringValuesImpl:{fieldName}", this.logLoanValueEvaluatorGets))
            {
                List<string> list = new List<string>();

                if (string.IsNullOrEmpty(fieldName) == true)
                {
                    return list;
                }

                if (fieldName.StartsWith(ParameterReporting.GenerateDocsVendorPackageIdPrefix, StringComparison.OrdinalIgnoreCase))
                {
                    fieldName = ParameterReporting.GenerateDocsVendorPackageIdFieldId;
                }

                if (m_nonLoanValues.IsSpecialStringFieldName(fieldName))
                {
                    return m_nonLoanValues.GetStringValues(fieldName);
                }

                if (m_loanFileCache != null && m_loanFileCache.ContainsField(fieldName))
                {
                    string value;
                    if (m_loanFileCache.TryGetString(fieldName, out value))
                    {
                        list.Add(value);
                    }
                }
                else if (m_loanFieldList != null && m_loanFieldList.ContainsKey(fieldName))
                {
                    list.Add(PageDataUtilities.GetValue(m_dataLoan, m_dataApp, fieldName));
                }
                else if (fieldName.Equals("UserGroup", StringComparison.OrdinalIgnoreCase))
                {
                    foreach (var group in GroupDB.ListInclusiveGroupForEmployee(m_principal.BrokerId, m_principal.EmployeeId))
                    {
                        list.Add(group.Name);
                    }
                }
                else if (fieldName.Equals("OCGroup", StringComparison.OrdinalIgnoreCase))
                {
                    // 4/9/2012 dd - OPM 81685 - The OC Group is base on loan level.
                    Guid pmlBrokerId;
                    bool isPmlBrokerIdRetrieved = GetGuidFromLoanFileCacheWithDataLoanFallback("sPmlBrokerId", () => m_dataLoan.sPmlBrokerId, out pmlBrokerId);
                    if (isPmlBrokerIdRetrieved)
                    {
                        foreach (var group in GroupDB.ListInclusiveGroupForPmlBroker(m_principal.BrokerId, pmlBrokerId))
                        {
                            list.Add(group.Name);
                        }
                    }
                }
                else if (fieldName.Equals("BranchGroup", StringComparison.OrdinalIgnoreCase))
                {
                    // 4/9/2012 dd - OPM 81685 - The BranchGroup is based on loan level.
                    Guid branchId;
                    bool isBranchIdRetrieved = GetGuidFromLoanFileCacheWithDataLoanFallback("sBranchId", () => m_dataLoan.sBranchId, out branchId);
                    if (isBranchIdRetrieved)
                    {
                        foreach (var group in GroupDB.ListInclusiveGroupForBranch(m_principal.BrokerId, branchId))
                        {
                            list.Add(group.Name);
                        }
                    }
                }
                else if (fieldName.Equals("AreAllConditionsClosed", StringComparison.OrdinalIgnoreCase))
                {
                    list.AddRange(m_dataLoan.GetDistinctConditionCategoryWhereAllConditionsAreClosed());
                }
                else if (fieldName.Equals("DoesConditionExist", StringComparison.OrdinalIgnoreCase))
                {
                    list.AddRange(m_dataLoan.GetDistinctConditionCategory());
                }
                else if (fieldName.Equals("DoesActiveConditionExist", StringComparison.OrdinalIgnoreCase))
                {
                    list.AddRange(m_dataLoan.GetDistinctConditionCategoryWithActiveCondition());
                }
                else if (fieldName.StartsWith(DataPath.PathHeader, StringComparison.OrdinalIgnoreCase))
                {
                    DataPath path = DataPath.Create(fieldName);
                    list.AddRange(PathResolver.GetUsingSchema(m_dataLoan.sBrokerId, m_dataLoan, path).Select(o => o.ToString()));
                }
                else if (m_loanFileCache == null && m_loanFieldList == null && m_dataLoan != null && m_dataApp != null)
                {
                    // 12/22/2011 dd - m_loanFileCache and m_loanFieldList are null only when we pass in CPagedata and CAppData directly.
                    list.Add(PageDataUtilities.GetValue(m_dataLoan, m_dataApp, fieldName));

                }

                // av - 222777 Workflow Bug - Blank Subj Prop State Not Evaluating Correctly
                if (fieldName.Equals("sSpState", StringComparison.OrdinalIgnoreCase))
                {
                    Regex blankSpaceRegex = new Regex(@"^\s{1,2}$");

                    for (int i = 0; i < list.Count; i++)
                    {
                        list[i] = blankSpaceRegex.Replace(list[i], string.Empty);
                    }
                }

                return list;
            }
        }

        #endregion
        public bool IsDataModified(string name)
        {
            if (m_dataLoan != null && m_dataApp != null)
            {
                string originalValue = string.Empty;

                if (m_dataLoan.TryGetOriginalValue(name, out originalValue))
                {
                    string currentValue = PageDataUtilities.GetValue(m_dataLoan, m_dataApp, name);

                    return originalValue != currentValue;

                }
            }
            return false;
        }
    }
}
