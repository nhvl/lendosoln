using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using CommonLib;
using ConfigSystem.DataAccess;
using ConfigSystem.Engine;
using LendersOffice.Admin;
using LendersOffice.Audit;
using LendersOffice.Common;
using LendersOffice.Common.SerializationTypes;
using LendersOffice.ConfigSystem;
using LendersOffice.Constants;
using LendersOffice.Migration;
using LendersOffice.ObjLib.Relationships;
using LendersOffice.ObjLib.Security;
using LendersOffice.ObjLib.StatusEvents;
using LendersOffice.ObjLib.Task;
using LendersOffice.Reminders;
using LendersOffice.Security;
using LqbGrammar;
using LqbGrammar.DataTypes;
using LqbGrammar.Drivers;
using LendersOffice.ObjLib;
using LendersOffice.ObjLib.TRID2;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace DataAccess
{
    /// <summary>
    /// Wrap errors with details about the loan file and who is creating it.
    /// </summary>

    public class LoanFileCreatorException : CBaseException
    {
        public LoanFileCreatorException(string sErrorMsg, CLoanFileCreator lfCreator, Exception e)
            : base(
                sErrorMsg + " ( " + lfCreator.LoginName + " :: " + lfCreator.LoanName + " :: " + lfCreator.CreatedOn + " )",
                e)
        {
        }
    }

    /// <summary>
    /// Facilitate creation of new loans.  All new loans *must* be
    /// initialized and committed through this implementation.
    /// </summary>
    public class CLoanFileCreator
    {
        private const int prefixLength = 7;
        private const string DuplicateRefNmPrefixRegEx = "^D-[0-9A-Z]{4}:";
        private const string TestRefNmPrefixRegEx = "^T-[0-9A-Z]{4}:";
        private const string SandboxRefNmPrefixRegEx = "^S-[0-9A-Z]{4}:";

        static public CLoanFileCreator GetCreator(AbstractUserPrincipal principal, E_LoanCreationSource loanCreationSource)
        {
            return new CLoanFileCreator(principal, principal.BranchId, loanCreationSource);
        }

        static public CLoanFileCreator GetCreator(AbstractUserPrincipal principal, Guid branchId, E_LoanCreationSource loanCreationSource)
        {
            return new CLoanFileCreator(principal, branchId, loanCreationSource);
        }

        /// <summary>
        /// Add new options as needed.  We cache these values during
        /// initialization inside of construction.
        /// </summary>

        private bool m_NameDuplicateAsNew = false;
        private bool m_NameSecondAsNew = false;
        private bool m_CannotCreateLoanTemplate = true;
        private Guid createdLenderContactRecordId = Guid.Empty;

        /// <summary>
        /// Cache a pointer to our main workhorse.  All calls to make
        /// a new loan file use this mechanism.
        /// </summary>

        private CFileCreatorCore m_Core = null;
        private AbstractUserPrincipal m_principal = null;
        private E_LoanCreationSource m_loanCreationSource = E_LoanCreationSource.UserCreateFromBlank;

        private BrokerDB m_broker = null;

        /// <summary>
        /// DO NOT USE THIS OUTSIDE OF TESTING!! It does not correctly initialize the object.
        /// </summary>
        protected CLoanFileCreator()
        {
        }

        private CLoanFileCreator(AbstractUserPrincipal principal, Guid branchId, E_LoanCreationSource loanCreationSource)
        {
            // 9/3/2004 kb - Load up our embedded creator.  We now
            // create loans using basic operations.  The variant
            // behavior is handled in this level outside of the
            // begin and commit transactions.  Now, if we fail
            // after a core beginning, and before performing a
            // core commit, then we just don't commit, and the
            // begun file will linger in the db.

            // 9/27/2005 dd - Use the long constructor so that it will not hit database to pull out employee detail.
            m_principal = principal;
            m_loanCreationSource = loanCreationSource;
            m_Core = CFileCreatorCore.GetCreator(principal.BrokerId, branchId, principal.UserId, principal.EmployeeId, principal.LoginNm);

            InitializeOptions();
        }

        /// <summary>
        /// Initialize broker options we use during loan file creation.
        /// </summary>

        private void InitializeOptions()
        {
            // Get broker options.  Note that these are cached for the
            // lifetime of this creator.

            // 1/26/2005 kb - Get broker options that affect loan file
            // creation.  Load up the bits you need here (add to this
            // set as needed).  We always assume that the loan file
            // creator was created just before use.  If we use the
            // creator long after we cache these bits, then we could
            // be out of sync.

            m_broker = BrokerDB.RetrieveById(m_Core.BrokerId);

            m_NameDuplicateAsNew = m_broker.IsDuplicateLoanCreatedWAutonameBit;
            m_NameSecondAsNew = m_broker.Is2ndLoanCreatedWAutonameBit;

            // 3/1/2005 kb - Also fetch the employee's details and
            // see if they can create a loan file template.

            BrokerUserPermissions bUP = new BrokerUserPermissions(m_Core.BrokerId, m_Core.EmployeeId);

            if (bUP.HasPermission(Permission.CanCreateLoanTemplate) == true)
            {
                m_CannotCreateLoanTemplate = false;
            }
            else
            {
                m_CannotCreateLoanTemplate = true;
            }
        }

        public string LoginName
        {
            get { return m_Core.LoginName; }
        }

        public string LoanName
        {
            get { return m_Core.LoanName; }
        }
        public Guid sLId
        {
            get { return m_Core.FileId; }
        }
        public DateTime CreatedOn
        {
            get { return m_Core.CreatedOn; }
        }

        /// <summary>
        /// Gets or sets the driver for creating encryption keys.
        /// </summary>
        /// <remarks>
        /// This property is made protected to allow for substituting the
        /// driver by a derived class for testing purposes in the future.
        /// </remarks>
        protected IEncryptionKeyDriver EncryptionKeyDriver { get; set; } = GenericLocator<IEncryptionKeyDriverFactory>.Factory.Create();

        #region Creation utilities

        /// <summary>
        /// Generates a new loan name based on the source loan name, the given suffix,
        /// and a random character string.
        /// </summary>
        /// <remarks>
        /// If the generated loan name is longer than 36 characters, this will truncate
        /// the source loan name to accomodate the suffix and extra generated characters.
        /// </remarks>
        /// <param name="sourceFileId">The loan id of the source file.</param>
        /// <param name="separator">The separator to use between the base suffix and the random characters.</param>
        /// <param name="baseSuffix">The base suffix to append -- additional characters will also be appended.</param>
        /// <returns>The copy loan's loan name.</returns>
        protected string GetLoanCopyName(Guid sourceFileId, string separator, string baseSuffix)
        {
            string sourceLoanName = this.GetLoanNameByLoanId(sourceFileId);
            string friendlyId = this.GenerateFriendlyIdForCopyName(baseSuffix);

            string fullSuffix = string.IsNullOrEmpty(baseSuffix) ?
                separator + friendlyId
                : separator + baseSuffix + separator + friendlyId;
            string copyLoanName = sourceLoanName + fullSuffix;

            if (copyLoanName.Length > 36)
            {
                copyLoanName = sourceLoanName.Substring(0, sourceLoanName.Length - (copyLoanName.Length - 35)) + "+" + fullSuffix;
            }

            return copyLoanName;
        }

        protected virtual string GetLoanNameByLoanId(Guid sourceFileId)
        {
            return Tools.GetLoanNameByLoanId(m_Core.BrokerId, sourceFileId);
        }

        protected virtual string GenerateFriendlyIdForCopyName(string baseSuffix)
        {
            string friendlyId;
            var idGenerator = new CPmlFIdGenerator();

            friendlyId = idGenerator.GenerateNewFriendlyId().Replace("-", "");

            if (friendlyId.Length < 4)
            {
                throw new CBaseException(ErrorMessages.Generic, "Friendly tag for copy name is too short.");
            }

            if (baseSuffix.Length > 0)
            {
                friendlyId = friendlyId.Substring(0, 2);
            }
            else
            {
                friendlyId = friendlyId.Substring(0, 4);
            }

            return friendlyId;
        }

        private enum LoanCopyType
        {
            Sandbox = 0,
            Test = 1,
            Duplicate = 2
        }

        private string GetLoanCopyRefNm(Guid sourceFileId, LoanCopyType loanCopyType)
        {
            string sourceRefNm = Tools.GetLoanRefNmByLoanId(m_Core.BrokerId, sourceFileId);

            // Determin which prefix to use.
            bool removePrefix = false;
            string sPrefix = string.Empty;
            switch (loanCopyType)
            {
                case LoanCopyType.Sandbox:
                    // Replace sandbox prefixes. We don't need to check for Duplicate or Test Prefixes.
                    if (System.Text.RegularExpressions.Regex.IsMatch(sourceRefNm, SandboxRefNmPrefixRegEx))
                    {
                        removePrefix = true;
                    }

                    sPrefix = "S";
                    break;
                case LoanCopyType.Test:
                    // Replace Test or duplicate prefixes. We don't need to check for Sandbox prefix since
                    // it should not be possible to create a test loan from a sandbox loan (at least through UI).
                    if (System.Text.RegularExpressions.Regex.IsMatch(sourceRefNm, TestRefNmPrefixRegEx)
                        || System.Text.RegularExpressions.Regex.IsMatch(sourceRefNm, DuplicateRefNmPrefixRegEx))
                    {
                        removePrefix = true;
                    }

                    sPrefix = "T";
                    break;
                case LoanCopyType.Duplicate:
                    if (System.Text.RegularExpressions.Regex.IsMatch(sourceRefNm, SandboxRefNmPrefixRegEx))
                    {
                        // If source starts with sandbox prefix, replace with new sandbox prefix.
                        removePrefix = true;
                        sPrefix = "S";
                    }
                    else if (System.Text.RegularExpressions.Regex.IsMatch(sourceRefNm, TestRefNmPrefixRegEx))
                    {
                        // If source starts with test prefix, replace with new test prefix.
                        removePrefix = true;
                        sPrefix = "T";
                    }
                    else
                    {
                        // If source starts with duplicate prefix, replace with new duplicate prefix.
                        if (System.Text.RegularExpressions.Regex.IsMatch(sourceRefNm, DuplicateRefNmPrefixRegEx))
                        {
                            removePrefix = true;
                        }

                        sPrefix = "D"; // Default to duplicate prefix if no other prefix found.
                    }
                    break;
                default:
                    throw new UnhandledEnumException(loanCopyType);
            }

            // Remove old prefix if necessary.
            if (removePrefix)
            {
                sourceRefNm = sourceRefNm.Substring(prefixLength);
            }

            string sFTag = GetRandom4CharsForPrefix();

            return sPrefix + "-" + sFTag + ":" + sourceRefNm;
        }

        // Generate random 4 character alphanumeric code for refNm copy prefix.
        private static string GetRandom4CharsForPrefix()
        {
            CPmlFIdGenerator idG = new CPmlFIdGenerator();

            string sFTag = idG.GenerateNewFriendlyId().Replace("-", "");

            if (sFTag.Length < 4)
            {
                throw new CBaseException(ErrorMessages.Generic, "Friendly tag for copy reference number is too short.");
            }

            return sFTag.Substring(0, 4); ;
        }

        private string GetLoanAutoName(Guid brokerId, bool useLeadCounter, bool useTestCounter, Guid branchId, string sPrefix, bool enableMersGeneration, out string sMersMin, out string sLRefNm)
        {
            // 12/29/2004 kb - Get the auto naming strategy for this
            // broker.  If random, just use our friendly approach and
            // return.
            //
            // 1/26/2005 kb - Refactored auto loan naming to our
            // naming logic class.

            CLoanFileNamer loanNamer = new CLoanFileNamer();

            string sName = loanNamer.GetLoanAutoNameAddPrefix(brokerId, useLeadCounter, useTestCounter, branchId, sPrefix, enableMersGeneration, out sMersMin, out sLRefNm);

            return sName;
        }

        /// <summary>
        /// Calls SQL to write initial loan team assignments.
        /// </summary>
        /// <param name="principal">Abstract User Principal.</param>
        /// <param name="sourceLoanId">Source Loan ID.</param>
        /// <param name="destLoanID">Destination Loan ID.</param>
        /// <param name="copySourceLoanAssignments">Should source loan assignments be copied over to the destination loan.</param>
        private static void WriteInitialLoanTeamAssignments(AbstractUserPrincipal principal, Guid sourceLoanId, Guid destLoanID, bool copySourceLoanAssignments)
        {
            if (copySourceLoanAssignments)
            {
                try
                {
                    SqlParameter[] parameters =
                        {
                            new SqlParameter("@LoanId", destLoanID),
                            new SqlParameter("@SrcLoanIdForAssignmentDuplication", sourceLoanId)
                        };

                    StoredProcedureHelper.ExecuteNonQuery(principal.BrokerId, "InitLoanFileTeamAssignments", 5, parameters);

                    return;
                }
                catch (Exception e)
                {
                    throw new CBaseException("Failed to write initial assignments.", e);
                }
            }
            else
            {
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@LoanId", destLoanID),
                        new SqlParameter("@EmployeeId", principal.EmployeeId)
                    };
                StoredProcedureHelper.ExecuteNonQuery(principal.BrokerId, "InitLoanFileTeamAssignments", 5, parameters);

                return;
            }
        }

        /// <summary>
        /// Calls SQL to write initial loan assignments.
        /// </summary>
        /// <param name="principal">Abstract User Principal.</param>
        /// <param name="sourceLoanId">Source Loan ID.</param>
        /// <param name="destLoanID">Destination Loan ID.</param>
        /// <param name="copySourceLoanAssignments">Should source loan assignments be copied over to the destination loan.</param>
        /// <returns>PML Broker ID.</returns>
        private static Guid WriteInitialLoanAssignments(AbstractUserPrincipal principal, Guid sourceLoanId, Guid destLoanID, bool copySourceLoanAssignments)
        {
            // 9/3/2004 kb - We save out initial assignments for new
            // loans (unless we skipped it because of importing, or something goofy).
            try
            {
                Guid pmlBrokerID = Guid.Empty;

                // Keep transaction overhead light.
                if (copySourceLoanAssignments == true)
                {
                    var sourceFilePmlBrokerIdParam = new SqlParameter("@SourceFilePmlBrokerId", SqlDbType.UniqueIdentifier);
                    sourceFilePmlBrokerIdParam.Direction = ParameterDirection.Output;

                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@CreatorEmployeeId", principal.EmployeeId),
                        new SqlParameter("@LoanId", destLoanID),
                        new SqlParameter("@SrcLoanIdForAssignmentDuplication", sourceLoanId),
                        sourceFilePmlBrokerIdParam
                    };

                    StoredProcedureHelper.ExecuteNonQuery(principal.BrokerId, "InitLoanFileAssignments", 5, parameters);

                    if (!Convert.IsDBNull(sourceFilePmlBrokerIdParam.Value))
                    {
                        pmlBrokerID = (Guid)sourceFilePmlBrokerIdParam.Value;
                    }
                }
                else
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@CreatorEmployeeId", principal.EmployeeId),
                        new SqlParameter("@LoanId", destLoanID),
                        new SqlParameter("@PortalMode", principal.PortalMode)
                    };

                    StoredProcedureHelper.ExecuteNonQuery(principal.BrokerId, "InitLoanFileAssignments", 5, parameters);

                    pmlBrokerID = principal.PmlBrokerId;
                }

                return pmlBrokerID;
            }
            catch (Exception e)
            {
                throw new CBaseException("Failed to write initial assignments.", e);
            }
        }

        /// <summary>
        /// Copy Employee Commission to agents.
        /// </summary>
        /// <param name="dataLoan">The loan data.</param>
        /// <param name="employeeRoleForCommission">Employee role for commission.</param>
        /// <param name="employeeDictionary">Employee dictionary.</param>
        private static void CopyEmployeeCommissionToAgents(CPageData dataLoan, Dictionary<Guid, E_AgentRoleT> employeeRoleForCommission, Dictionary<Guid, EmployeeDB> employeeDictionary)
        {
            if (employeeRoleForCommission.Count == 0)
            {
                return;
            }

            Guid brokerID = dataLoan.sBrokerId;
            foreach (Guid employeeId in employeeRoleForCommission.Keys)
            {
                CAgentFields agent = dataLoan.GetAgentOfRole(employeeRoleForCommission[employeeId], E_ReturnOptionIfNotExist.ReturnEmptyObject);
                if (agent != CAgentFields.Empty)
                {
                    EmployeeDB employee = null;
                    if (employeeDictionary.TryGetValue(employeeId, out employee) == false)
                    {
                        employee = EmployeeDB.RetrieveById(brokerID, employeeId);
                    }

                    agent.CommissionPointOfLoanAmount = employee.CommissionPointOfLoanAmount;
                    agent.CommissionPointOfGrossProfit = employee.CommissionPointOfGrossProfit;
                    agent.CommissionMinBase = employee.CommissionMinBase;

                    agent.Update();
                }
            }
        }

        /// <summary>
        /// Write initial official agent list.
        /// </summary>
        /// <param name="dataLoan">The data loan.</param>
        /// <param name="principal">Abstract user principal.</param>
        /// <param name="branchAssignment">Determines if branch should be assigned.</param>
        /// <param name="createdLenderContactRecordId">Output parameter that returns lender contact record Id to caller.</param>
        private static void WriteInitialOfficialAgentListImpl(CPageData dataLoan, AbstractUserPrincipal principal, BranchAssignmentOption branchAssignment, out Guid createdLenderContactRecordId)
        {
            createdLenderContactRecordId = Guid.Empty;
            LoanAssignmentContactTable loanRole = new LoanAssignmentContactTable(principal.BrokerId, dataLoan.sLId);

            bool skipLoanOfficerAgent = false;

            Dictionary<Guid, E_AgentRoleT> employeeRoleForCommission = new Dictionary<Guid, E_AgentRoleT>();
            Dictionary<Guid, EmployeeDB> employeeDictionary = new Dictionary<Guid, EmployeeDB>();

            bool userHasAeInfo = false;
            if (principal.BrokerDB.IsAEAsOfficialLoanOfficer)
            {
                string agentName = string.Empty;
                Guid accountExecutiveEmployeeId = Guid.Empty;
                Guid brokerId = principal.BrokerId;
                if (principal.Type == "B")
                {
                    if (principal.HasRole(E_RoleT.LenderAccountExecutive))
                    {
                        agentName = principal.DisplayName;

                        // Retrieve additional employee data that are not in principal object.
                        SqlParameter[] parameters =
                            {
                                new SqlParameter("@EmployeeId", principal.EmployeeId)
                            };

                        using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetEmployeeDetailsByEmployeeId", parameters))
                        {
                            if (reader.Read())
                            {
                                accountExecutiveEmployeeId = principal.EmployeeId;
                                userHasAeInfo = true;
                            }
                        }
                    }
                }
                else if (principal.Type == "P")
                {
                    // Retrieve AE Associate 
                    skipLoanOfficerAgent = true; // If no AE associate with this "P" user then DO NOT create Loan Officer entry.
                    SqlParameter[] parameters =
                        {
                            new SqlParameter("@EmployeeId", principal.EmployeeId)
                        };

                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetLenderAccExecDetailsByEmployeeId", parameters))
                    {
                        if (reader.Read())
                        {
                            accountExecutiveEmployeeId = SafeConvert.ToGuid(reader["EmployeeId"]);
                            userHasAeInfo = true;
                            skipLoanOfficerAgent = false;
                        }
                    }
                }

                if (userHasAeInfo)
                {
                    CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);

                    if (agent.IsNewRecord)
                    {
                        EmployeeDB employeeDB = null;
                        if (employeeDictionary.TryGetValue(accountExecutiveEmployeeId, out employeeDB) == false)
                        {
                            employeeDB = EmployeeDB.RetrieveById(principal.BrokerDB.BrokerID, accountExecutiveEmployeeId);
                            employeeDictionary.Add(accountExecutiveEmployeeId, employeeDB);
                        }

                        CommonFunctions.CopyEmployeeInfoToAgent(principal.BrokerDB, agent, employeeDB, bCopyCommissions: false);
                        employeeRoleForCommission[accountExecutiveEmployeeId] = E_AgentRoleT.LoanOfficer;
                    }

                    agent.Update();
                }
            }

            foreach (EmployeeLoanAssignment employee in loanRole.Items)
            {
                // 6/17/2005 kb - Map the role to the agent descriptor.  We use
                // the db's non-cached data because the cache hasn't been set
                // yet, but we assume the final assignments are in place.
                E_AgentRoleT roleCode = Role.GetAgentRoleT(employee.Role.RoleT);

                if (roleCode != E_AgentRoleT.Other)
                {
                    // We have found a matching agent role.  If no agent of
                    // this type currently exists, we make a new one and put
                    // this employee's data into it.
                    if (roleCode == E_AgentRoleT.LoanOfficer && skipLoanOfficerAgent)
                    {
                        continue; // DO NOT ADD LOAN OFFICER AGENT.
                    }

                    // Update the loan officer record if it has been copied over from AE, OPM 51609
                    CAgentFields agent = dataLoan.GetAgentOfRole(roleCode, E_ReturnOptionIfNotExist.CreateNew);

                    if (agent.IsNewRecord
                        || (roleCode == E_AgentRoleT.LoanOfficer && principal.BrokerDB.IsAEAsOfficialLoanOfficer && userHasAeInfo))
                    {
                        EmployeeDB employeeDB = null;
                        if (employeeDictionary.TryGetValue(employee.EmployeeId, out employeeDB) == false)
                        {
                            employeeDB = EmployeeDB.RetrieveById(principal.BrokerId, employee.EmployeeId);
                            employeeDictionary.Add(employee.EmployeeId, employeeDB);
                        }

                        CommonFunctions.CopyEmployeeInfoToAgent(principal.BrokerDB, agent, employeeDB, false);
                        agent.Update();

                        if (agent.AgentRoleT == E_AgentRoleT.Lender)
                        {
                            createdLenderContactRecordId = agent.RecordId;
                        }

                        if (!employeeRoleForCommission.ContainsKey(employee.EmployeeId) || CommonFunctions.RoleHasPrecedenceForCommission(roleCode, employeeRoleForCommission[employee.EmployeeId]))
                        {
                            employeeRoleForCommission[employee.EmployeeId] = roleCode;
                        }
                    }
                    else
                    {
                        continue;
                    }
                }

                if (roleCode == E_AgentRoleT.LoanOfficer && branchAssignment == BranchAssignmentOption.Allow)
                {
                    dataLoan.AssignBranch(employee.BranchID);
                }
            }

            CopyEmployeeCommissionToAgents(dataLoan, employeeRoleForCommission, employeeDictionary);
        }

        public static void WriteInitialOfficialAgentList(Guid fileId, BrokerDB broker, BranchAssignmentOption branchAssignment)
        {
            // 3/15/2005 kb - As the employee can be added to all the
            // loan's assignments (for whatever roles they maintain),
            // so too can an employee populate the official agent
            // list of the loan.  We pull a match from the rolodex,
            // because we want all the details to carry over.  If
            // no match, then add a minimal entry to the set.
            //
            // 3/15/2005 kb - We assume that the official agent list
            // is already populated with the source loan's content if
            // we're creating from a template.

            for (int nAttempts = 0, nMaxAttempts = 5; nAttempts < nMaxAttempts;)
            {
                ++nAttempts;

                try
                {
                    // Use data object to access official agents.
                    // Beware: this accessor needs to bypass security
                    // to prevent blowing up.

                    CPageData loanData = CPageData.CreateUsingSmartDependencyWithSecurityBypass(fileId, typeof(CLoanFileCreator));
                    loanData.AllowSaveWhileQP2Sandboxed = true;
                    loanData.InitSave(ConstAppDavid.SkipVersionCheck);

                    Guid createdLenderContactRecordId;
                    WriteInitialOfficialAgentListImpl(loanData, PrincipalFactory.CurrentPrincipal, branchAssignment, out createdLenderContactRecordId);

                    AddOriginatingCompanyContact(loanData, createdLenderContactRecordId);

                    loanData.Save();

                    return;
                }
                catch (Exception e)
                {
                    Tools.LogWarning("Write initial official agents: Attempt #" + nAttempts + " failed.", e);

                    if (nAttempts == nMaxAttempts)
                    {
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Adds contact for originating company.
        /// </summary>
        /// <param name="loan">The loan file.</param>
        /// <param name="createdLenderContactRecordId">Existing lender contact record ID.</param>
        /// <remarks>If createdLenderContactRecordId is not Guid.Empty, the contact with that ID will be deleted.</remarks>
        private static void AddOriginatingCompanyContact(CPageData loan, Guid createdLenderContactRecordId)
        {
            bool isWholesale = loan.sBranchChannelT == E_BranchChannelT.Wholesale;
            bool isCorrespondent = loan.sBranchChannelT == E_BranchChannelT.Correspondent;

            if (isWholesale || isCorrespondent)
            {
                // If it's a correspondent file, we're supposed to use the OC contact
                // as the Lender contact, so we want to delete any lender contact we
                // created up to this point.
                if (isCorrespondent && createdLenderContactRecordId != Guid.Empty)
                {
                    loan.DeleteAgent(createdLenderContactRecordId);
                }

                loan.AddOfficialContactForOriginatingCompany();
            }
        }

        /// <summary>
        /// Assigns employees from the creating user's relationships.
        /// </summary>
        /// <param name="principal">Abstract user principal.</param>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="underwritingAuthority">Underwriting authority.</param>
        /// <remarks>Loan save in call to relationshipAssignmentHandler.AssignCreationRelationships.</remarks>
        private static void AssignEmployeesFromRelationships(AbstractUserPrincipal principal, Guid loanId, E_UnderwritingAuthority underwritingAuthority)
        {
            try
            {
                E_BranchChannelT branchChannel;
                E_sCorrespondentProcessT correspondentProcess;
                E_PortalMode portalMode = principal.PortalMode;
                switch (portalMode)
                {
                    case E_PortalMode.Broker:
                        branchChannel = E_BranchChannelT.Wholesale;
                        correspondentProcess = E_sCorrespondentProcessT.Blank;
                        break;
                    case E_PortalMode.MiniCorrespondent:
                        branchChannel = E_BranchChannelT.Correspondent;
                        correspondentProcess = E_sCorrespondentProcessT.MiniCorrespondent;
                        break;
                    case E_PortalMode.Correspondent:
                        branchChannel = E_BranchChannelT.Correspondent;

                        if (underwritingAuthority.Equals(E_UnderwritingAuthority.Delegated))
                        {
                            correspondentProcess = E_sCorrespondentProcessT.Delegated;
                        }
                        else
                        {
                            correspondentProcess = E_sCorrespondentProcessT.PriorApproved;
                        }

                        break;
                    case E_PortalMode.Retail:
                        branchChannel = E_BranchChannelT.Retail;
                        correspondentProcess = E_sCorrespondentProcessT.Blank;
                        break;
                    case E_PortalMode.Blank:
                        branchChannel = E_BranchChannelT.Blank;
                        correspondentProcess = E_sCorrespondentProcessT.Blank;
                        break;
                    default:
                        throw new UnhandledEnumException(portalMode);
                }

                var relationshipAssignmentHandler = new RelationshipAssignmentHandler(
                    principal.BrokerId,
                    loanId,
                    principal.EmployeeId);

                // +1 Loan save.
                relationshipAssignmentHandler.AssignCreationRelationships(
                    branchChannel,
                    correspondentProcess);
            }
            catch (Exception e)
            {
                Tools.LogWarning("Assigning from relationships failed.", e);
                throw new CBaseException("Failed to assignment from relationships.", e);
            }
        }

        private void DeepCopyLoanFile(bool bMakeTemplate, CPageData sourceLoan, bool isMakingSecond)
        {
            // 8/24/2004 kb - Duplicate the archived records associated with the
            // source loan file.  We moved this from the utils code so that we
            // would have a cohesive, single source for loan file creation logic,
            // and because it needed to be reengineered to use a shared
            // transaction object.
            //
            // 9/7/2004 kb - TODO: Setup retry loop, but make sure we minimize
            // clutter with each retry.
            //
            // 10/16/2018 je - Do NOT duplicate closing cost archives when making a 2nd lien.

            try
            {
                // Copy Xml FileDB contains Liability and Assets.
                // If someone called this function without initializing
                // the proper parameters, we just skip.  Afterwards, we
                // load up credit report and transfer them.

                if (m_Core.SourceFileId != Guid.Empty && m_Core.FileId != Guid.Empty)
                {
                    // Transfer the assets and liabilities.
                    var srcKey = m_Core.SourceFileId.ToString().Replace("-", "").ToUpper();
                    var destKey = m_Core.FileId.ToString().Replace("-", "").ToUpper();

                    try
                    {
                        FileDBTools.Copy(E_FileDB.Normal, srcKey, destKey);
                    }
                    catch (FileNotFoundException)
                    {
                        // 2016-11-17 - It is okay if the srcKey is missing. SrcKey could be missing if the loan is create from blank and there is no assets, liabilities.
                    }

                    if (sourceLoan.IsTemplate)
                    {
                        this.CopyCorrectBorrowerClosingCostSetData(sourceLoan);
                        // opm 236602 ejm - Ignore sClosingCostSet since we've already taken care of it in the call above this one.
                        DuplicateFileDBFieldText(m_Core.SourceFileId, m_Core.FileId, new HashSet<E_FileDBKeyType>() { E_FileDBKeyType.Normal_Loan_sClosingCostSetJsonContent });
                    }
                    else
                    {
                        DuplicateFileDBFieldText(m_Core.SourceFileId, m_Core.FileId, null); // 11/12/2015 dd - Duplicate the FileDB Text Field.
                    }

                    if( !ConstStage.DisableStatusEventFeature && !sourceLoan.IsTemplate && !isMakingSecond)
                    {
                        LendersOffice.ObjLib.StatusEvents.StatusEvent.DuplicateStatusEvents(m_Core.SourceFileId, m_Core.FileId, m_Core.BrokerId);
                    }

                    // 6/17/10 mf. OPM 52705. If the resulting file of this creating is a template
                    // (Either from duplicating a template, or creating a  template from a loan file),
                    // do not copy the credit report to the template.
                    if (bMakeTemplate == false)
                    {
                        DuplicateCreditReport(m_Core.BrokerId, m_Core.UserId, m_Core.SourceFileId, m_Core.FileId);
                    }
                    DuplicateDuFindings(m_Core.SourceFileId, m_Core.FileId);

                    // 2/14/2014 gf - opm 150695, we NEVER want to copy the closing cost migration
                    // archive from a template.
                    // 10/16/2018 je - opm 462948 - Don't copy archives when making a 2nd lien.
                    if (!sourceLoan.IsTemplate && !isMakingSecond)
                    {
                        DuplicateClosingCostMigrationArchive(m_Core.SourceFileId, m_Core.FileId);
                    }

                    DuplicateSubmissionSnapshots(this.m_Core.BrokerId, this.m_Core.SourceFileId, this.m_Core.FileId);

                    // This logic is used to determine if we need to load up and save data to an instance of the loan
                    // in order to deep copy certain collections.
                    bool shouldCopyLoanDataMigrationHistory = !sourceLoan.IsTemplate && sourceLoan.sLoanDataMigrationAuditHistory.Any();
                    bool shouldDuplicateClosingCostArchiveData = !isMakingSecond && !sourceLoan.IsTemplate &&
                        (sourceLoan.sClosingCostArchive.Any() || sourceLoan.sLoanEstimateDatesInfo.LoanEstimateDatesList.Any() || sourceLoan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList.Any());
                    bool shouldDuplicateSelectedLoanProgramFilter = sourceLoan.sRawSelectedProductCodeFilter.Any();
                    bool shouldMigrateLqbCollections = sourceLoan.sBorrowerApplicationCollectionT != E_sLqbCollectionT.Legacy;

                    if (shouldCopyLoanDataMigrationHistory || shouldDuplicateClosingCostArchiveData || shouldDuplicateSelectedLoanProgramFilter || shouldMigrateLqbCollections)
                    {
                        var destLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(m_Core.FileId, typeof(CLoanFileCreator));
                        destLoan.AllowSaveWhileQP2Sandboxed = true;
                        destLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                        if (shouldCopyLoanDataMigrationHistory)
                        {
                            // opm 236635 ejm - We want to copy the loan data migration audit history if not a template.
                            CopyLoanDataMigrationAuditHistory(sourceLoan, destLoan);
                        }

                        if (shouldDuplicateClosingCostArchiveData)
                        {
                            DuplicateClosingCostArchiveData(sourceLoan, destLoan);
                        }

                        if (shouldDuplicateSelectedLoanProgramFilter)
                        {
                            DuplicateSelectedLoanProgramFilter(sourceLoan, destLoan);
                        }

                        if (shouldMigrateLqbCollections)
                        {
                            DuplicateLoanLqbCollectionData(sourceLoan, destLoan);
                        }

                        destLoan.Save();
                    }
                }
            }
            catch (Exception e)
            {
                throw new LoanFileCreatorException("Failed to deep copy loan data.", this, e);
            }
        }

        private static void DuplicateSubmissionSnapshots(Guid brokerId, Guid sourceLoanId, Guid newLoanId)
        {
            var parameters = new[]
            {
                new SqlParameter("SourceLoanId", sourceLoanId),
                new SqlParameter("NewLoanId", newLoanId),
            };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "SUBMISSION_SNAPSHOT_Duplicate", nRetry: 0, parameters: parameters);
        }

        /// <summary>
        /// Will take the source loan's borrower fee set, strip out the hidden fees, and save the JSON to file db.
        /// Should only be used if the source loan is a template!
        /// </summary>
        /// <param name="sourceLoan">The source loan. Should be a template.</param>
        private void CopyCorrectBorrowerClosingCostSetData(CPageData sourceLoan)
        {
            string newFeeJSON = sourceLoan.sClosingCostSet.ToJsonPruneHiddenFees();

            // We don't care about what the template has in file db for the closing cost set at this point since we're using the JSON created in the data layer.
            var destDbEntryInfo = Tools.GetFileDBEntryInfoForLoan(E_FileDBKeyType.Normal_Loan_sClosingCostSetJsonContent, m_Core.FileId);
            FileDBTools.WriteData(destDbEntryInfo.FileDBType, destDbEntryInfo.Key, newFeeJSON);
        }

        private void DuplicateSelectedLoanProgramFilter(CPageData sourceLoan, CPageData destLoan)
        {
            if (sourceLoan.sRawSelectedProductCodeFilter.Any())
            {
                destLoan.sRawSelectedProductCodeFilter = sourceLoan.sRawSelectedProductCodeFilter;
            }
        }

        private void CopyLoanDataMigrationAuditHistory(CPageData sourceLoan, CPageData destLoan)
        {
            if (sourceLoan.IsTemplate)
            {
                return;
            }

            if (!sourceLoan.sLoanDataMigrationAuditHistory.Any())
            {
                return;
            }

            destLoan.ManuallySetLoanDataMigrationAuditHistory(sourceLoan.sLoanDataMigrationAuditHistory);
        }

        /// <summary>
        /// Copies the closing cost archives from the source file to the destination file.
        /// </summary>
        /// <remarks>
        /// We are doing this in code as opposed to the database because we need to make sure
        /// that the archives have a different ID and FileDBKey in the created loan. Preserves
        /// the ten percent tolerance archive from the source file.
        /// </remarks>
        /// <param name="sourceFileClosingCostArchives">The source file archives.</param>
        private void DuplicateClosingCostArchiveData(CPageData sourceLoan, CPageData destLoan)
        {
            if (sourceLoan.IsTemplate)
            {
                return;
            }

            if (!(sourceLoan.sClosingCostArchive.Any() || sourceLoan.sLoanEstimateDatesInfo.LoanEstimateDatesList.Any() || sourceLoan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList.Any()))
            {
                return;
            }

            var duplicateArchives = new List<ClosingCostArchive>();
            Guid newFileToleranceArchiveId = Guid.Empty;
            Dictionary<Guid, Guid> idMap = new Dictionary<Guid, Guid>();
            var linkedArchiveIds = new Dictionary<Guid, Guid>();
            foreach (var archive in sourceLoan.sClosingCostArchive)
            {
                var duplicate = archive.Duplicate();

                if (archive.Id == sourceLoan.sTolerance10BasisLEArchiveId)
                {
                    newFileToleranceArchiveId = duplicate.Id;
                }

                if (archive.LinkedArchiveId.HasValue)
                {
                    linkedArchiveIds.Add(duplicate.Id, archive.LinkedArchiveId.Value);
                }

                duplicateArchives.Add(duplicate);
                idMap.Add(archive.Id, duplicate.Id);
            }

            if (linkedArchiveIds.Count != 0)
            {
                foreach (var duplicate in duplicateArchives)
                {
                    Guid originalLinkedId, newLinkedId;
                    if (linkedArchiveIds.TryGetValue(duplicate.Id, out originalLinkedId) &&
                        idMap.TryGetValue(originalLinkedId, out newLinkedId))
                    {
                        duplicate.LinkedArchiveId = newLinkedId;
                    }
                }
            }

            // OPM 235223 - Copy disclosure dates on loan duplication.
            // NOTE: This is done here so that we can properly map the associated
            // ArchiveIds with the IDs belonging to the duplicated archives.
            LoanEstimateDatesInfo duplicateLedInfo = LoanEstimateDatesInfo.Create();
            foreach (LoanEstimateDates orig in sourceLoan.sLoanEstimateDatesInfo.LoanEstimateDatesList)
            {
                LoanEstimateDates duplicate = orig.DuplicateAssigningNewId(destLoan.GetClosingCostArchiveById);

                // Update archive id.
                if (orig.ArchiveId != Guid.Empty)
                {
                    duplicate.ArchiveId = idMap[orig.ArchiveId];
                }

                duplicateLedInfo.AddDates(duplicate);
            }

            ClosingDisclosureDatesInfo duplicateCddInfo = ClosingDisclosureDatesInfo.Create();
            foreach (ClosingDisclosureDates orig in sourceLoan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList)
            {
                ClosingDisclosureDates duplicate = orig.DuplicateAssigningNewId(destLoan.GetClosingCostArchiveById);

                // Update archive id.
                if (orig.ArchiveId != Guid.Empty)
                {
                    duplicate.ArchiveId = idMap[orig.ArchiveId];    // Update archive id.
                }

                duplicateCddInfo.AddDates(duplicate);
            }

            destLoan.UpdateClosingCostArchiveForFileDuplication(duplicateArchives);
            if (newFileToleranceArchiveId != Guid.Empty)
            {
                destLoan.sTolerance10BasisLEArchiveId = newFileToleranceArchiveId;
            }

            destLoan.sLoanEstimateDatesInfo = duplicateLedInfo;
            destLoan.sClosingDisclosureDatesInfo = duplicateCddInfo;
            destLoan.SaveLEandCDDates();
        }

        private static void DuplicateClosingCostMigrationArchive(Guid srcLoanId, Guid destLoanId)
        {
            CPageBase.DuplicateClosingCostMigrationArchive(srcLoanId, destLoanId);
        }

        private static void DuplicateDuFindings(Guid srcLoanId, Guid destLoanId)
        {
            CPageBase.DuplicateDuFindings(srcLoanId, destLoanId);
        }

        private static void DuplicateFileDBFieldText(Guid srcLoanId, Guid destLoanId, HashSet<E_FileDBKeyType> keyTypesToIgnore)
        {
            CBase.DuplicateFileDBTextField(srcLoanId, destLoanId, keyTypesToIgnore);
        }

        public static void DuplicateCreditReport(Guid brokerId, Guid userId, Guid srcLoanId, Guid destLoanId)
        {
            // 6/17/2005 dd - Duplicate the credit report manually.
            List<Guid> src_aAppIds = new List<Guid>();
            List<Guid> dest_aAppIds = new List<Guid>();

            SqlParameter[] parameters = {
                                             new SqlParameter("@srcLoanID", srcLoanId),
                                            new SqlParameter("@destLoanID", destLoanId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "RetrieveListOfOldAndNewAppId", parameters))
            {
                while (reader.Read())
                {
                    src_aAppIds.Add((Guid)reader["src_aAppId"]);
                    dest_aAppIds.Add((Guid)reader["dest_aAppId"]);
                }
            }

            int nApps = src_aAppIds.Count;
            for (int i = 0; i < nApps; i++)
            {
                Guid srcAppId = src_aAppIds[i];
                Guid destAppId = dest_aAppIds[i];

                Guid srcDbFileKey = Guid.Empty;
                string externalFileId = "";
                Guid comId = Guid.Empty;
                Guid crAccProxyId = Guid.Empty;

                parameters = new SqlParameter[] {
                    new SqlParameter("@ApplicationID", srcAppId)
                };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "RetrieveCreditReport", parameters))
                {
                    if (reader.Read())
                    {
                        srcDbFileKey = (Guid)reader["DbFileKey"];
                        externalFileId = (string)reader["ExternalFileId"];
                        comId = (Guid)reader["ComId"];
                        if (reader["CrAccProxyId"] != DBNull.Value)
                        {
                            crAccProxyId = (Guid)reader["CrAccProxyId"];
                        }
                    }
                }
                if (srcDbFileKey != Guid.Empty)
                {
                    Guid destDbFileKey = Guid.NewGuid();

                    try
                    {
                        FileDBTools.Copy(E_FileDB.Normal, srcDbFileKey.ToString(), destDbFileKey.ToString());

                        // 6/17/2005 dd - Insert entry to SERVICE_FILE
                        parameters = new SqlParameter[]{
                                                    new SqlParameter("@Owner", destAppId),
                                                    new SqlParameter("@ExternalFileId", externalFileId),
                                                    new SqlParameter("@DbFileKey", destDbFileKey),
                                                    new SqlParameter("@ComId", comId),
                                                    new SqlParameter("@UserId", userId),
                                                    new SqlParameter("@CrAccProxyId", crAccProxyId == Guid.Empty ? (object) DBNull.Value : (object) crAccProxyId),
                                                    new SqlParameter("@HowDidItGetHere", ConstAppDavid.ServiceFileDuplication)
                                                };
                        StoredProcedureHelper.ExecuteNonQuery(brokerId, "InsertCreditReportFile", 0, parameters);
                    }
                    catch (FileNotFoundException)
                    {
                        // NO-OP.
                    }
                }
            }
        }

        public static void DuplicateLoanTasks(string sInitialMessage, Guid brokerId, Guid srcLoanId, Guid destLoanId, string destLoanNm, Guid userId, string loginNm, DateTime createdOn)
        {
            // Load up copies of all the source file's tasks (we assume that the
            // source is a template, but it doesn't have to be one).  Once loaded,
            // we create new ones with the same details and associate them with
            // the new loan file.  We do assume that the new loans need to be
            // created from scratch.
            //
            // Once we have the list of task ids to copy, we copy each one at a
            // time.  If any one of them fails, we make a note of it in the log
            // and keep going.  Once we get this working, we should revisit
            // the error tracking and perhaps tell the user when all the tasks
            // don't make it over.
            //
            // 9/7/2004 kb - TODO: Setup retry loop that actually looks for
            // tasks that are already duplicated.  Not sure if task name
            // is enough; may need to keep track of progress in an array of
            // new task ids as we attempt to duplicate.
            //
            // 10/5/2004 kb - Added is for condition and is valid options
            // to task listing so we don't try to duplicate the conditions
            // that happen to be associated with the source file.
            //
            // 10/13/2004 kb - Putting in simple retry loop with memory of
            // which tasks were already copied.  If initial fetch fails, we
            // punt.  If you feel the fetch could fail, then loop that guy
            // too.  For now, we shall focus on the writing portion.

            try
            {
                DataSet dS = new DataSet();
                int iC = 0;

                SqlParameter[] parameters = {
                                                new SqlParameter( "@LoanId"         , srcLoanId )
                                                , new SqlParameter( "@IsValid"        , true                )
                                            };
                DataSetHelper.Fill(dS, brokerId, "ListDiscLogsByLoanId", parameters);


                for (int nAttempts = 0, nMaxAttempts = 3; nAttempts < nMaxAttempts;)
                {
                    ++nAttempts;

                    try
                    {
                        int iP = 0;

                        foreach (DataRow row in dS.Tables[0].Rows)
                        {
                            // Get each task for this loan and duplicate.  We should
                            // really duplicate from this in memory instance, not the
                            // db, but for here, it is moot.
                            //
                            // 5/3/2005 kb - We don't need the participants to be
                            // loaded, because we re-pull during replication of the
                            // notifs.  If this changes, then flip the retrieve
                            // flag to true.

                            CDiscNotif disc = new CDiscNotif(brokerId, Guid.Empty, (Guid)row["DiscLogId"]);

                            if (iP < iC)
                            {
                                continue;
                            }

                            if (disc.Retrieve(false, false) == false)
                            {
                                throw new CBaseException(ErrorMessages.Generic, "Failed to retrieve task from loan " + srcLoanId + " for user " + loginNm + ".");
                            }

                            disc.DiscCreatorLoginNm = loginNm;
                            disc.DiscCreatorUserId = userId;

                            disc.NewMsg = sInitialMessage;

                            disc.DiscRefObjId = destLoanId;
                            disc.DiscRefObjNm1 = destLoanNm;

                            disc.DiscRefObjType = "Loan_File";

                            if (disc.Duplicate(loginNm, true, createdOn) == false)
                            {
                                throw new CBaseException(ErrorMessages.Generic, "Failed from loan " + srcLoanId + " for user " + loginNm + ".");
                            }

                            ++iC;
                            ++iP;
                        }

                        return;
                    }
                    catch (Exception e)
                    {
                        Tools.LogWarning("Duplicating loan tasks: Attempt #" + nAttempts + " failed.", e);

                        if (nAttempts == nMaxAttempts)
                        {
                            throw;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new CBaseException(ErrorMessages.Generic, string.Format("Failed to duplicate loan tasks.  ({0} :: {1} :: {2})  " + e.ToString(), loginNm, destLoanNm, createdOn));
            }
        }

        private void DuplicateLoanConditions()
        {
            // 10/5/2004 kb - With the new conditions-as-tasks option for
            // brokers, we should copy this kind of conditions whenever
            // we duplicate a loan file.

            try
            {
                LoanConditionSetObsolete conditions = new LoanConditionSetObsolete();
                conditions.RetrieveAll(m_Core.SourceFileId, true);

                int iC = 0;

                for (int nAttempts = 0, nMaxAttempts = 3; nAttempts < nMaxAttempts;)
                {
                    ++nAttempts;

                    try
                    {
                        int iP = 0;

                        foreach (CLoanConditionObsolete condition in conditions)
                        {
                            // Get the condition task, assuming there are any, and
                            // duplicate.
                            //
                            // 5/3/2005 kb - We don't need the participants for this
                            // because the duplicate will re-pull from the db.  We
                            // should really use the participant list that's in
                            // memory for this and set the state of each notif to
                            // its default.

                            if (iP < iC)
                            {
                                continue;
                            }
                            if (condition.CondIsValid == false)
                            {
                                // 8/23/2010 dd - OPM We do not duplicate hidden condition to new file.
                                ++iC;
                                ++iP;
                                continue;
                            }
                            condition.LoanId = m_Core.FileId;

                            if (condition.Duplicate() == false)
                            {
                                throw new CBaseException(ErrorMessages.Generic, "Failed from loan " + m_Core.SourceFileId + " for user " + m_Core.LoginName + ".");
                            }

                            ++iC;
                            ++iP;
                        }

                        return;
                    }
                    catch (Exception e)
                    {
                        Tools.LogWarning("Duplicating loan conditions: Attempt #" + nAttempts + " failed.", e);

                        if (nAttempts == nMaxAttempts)
                        {
                            throw;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new LoanFileCreatorException("Failed to duplicate loan conditions.", this, e);
            }
        }

        private void RefreshLoanTasks()
        {
            // 10/5/2004 kb - Go through and update all the associated tasks
            // (and they could be conditions too!) with whatever the cache
            // table says is latest and greatest for a given loan. **Note:
            // for best effect, do this after committing the loan and marking
            // it as valid.  If this sub-op fails, so be it -- it's not a
            // critical aspect of creating new loans.

            try
            {
                // Invoke refresh directly.  If the new loan is from a blank
                // template, or one without tasks, then this call should have
                // no effect.
                SqlParameter[] parameters = {
                                                new SqlParameter( "@BrokerId" , m_Core.BrokerId )
                                                , new SqlParameter( "@LoanId"   , m_Core.FileId   )
                                            };
                StoredProcedureHelper.ExecuteNonQuery(m_Core.BrokerId, "RefreshDiscussionLogs", 3, parameters);
            }
            catch (Exception e)
            {
                throw new LoanFileCreatorException("Failed to refresh loan tasks.", this, e);
            }
        }

        private string RetryFileCreationOnRefNmConflict(string sLNm, string sLRefNm, Action<string> fileCreationMethod)
        {
            // OPM 244087 - We don't want to fail loan creation on reference number errors.
            // If loan file creation fails due to conflict with reference number, then try
            // again with new reference number.
            for (int refErrorCounter = 0; refErrorCounter <= 6; refErrorCounter++)
            {
                try
                {
                    fileCreationMethod.Invoke(sLRefNm);
                }
                catch (Exception e)
                {
                    if (refErrorCounter == 6 || e.Message.IndexOf("Constraint_UniqueLoanReferenceNumberPerBroker") == -1)
                    {
                        throw;
                    }

                    // Attempt to use legacy reference number on first failure.
                    if (refErrorCounter == 0)
                    {
                        sLRefNm = "L:" + sLNm;
                    }
                    else
                    {
                        // Try using friendly identifier for remaining 5 attempts.
                        CPmlFIdGenerator generator = new CPmlFIdGenerator();
                        sLRefNm = "L:" + generator.GenerateNewFriendlyId();
                    }

                    continue;
                }

                // BeginCreateFile worked! Break out of loop.
                break;
            }

            return sLRefNm;
        }

        /// <summary>
        /// Create initial loan assignments for a loan that is
        /// not being derived from another loan (e.g. via duplication
        /// or from a template).
        /// </summary>
        /// <remarks>
        /// 7/27/2017 dt - For employee roles, we set in this order, overwriting earlier data with later:
        ///   1. if (setInitialEmployeesRoles) The principal user's roles (except Admin, Accountant, and Closer?! [see the InitLoanFileAssignments SP])
        ///   2. if (assignEmployeesFromRelationships) The roles as defined in the principal user's relationships, per the channel of the loan file
        /// </remarks>
        /// <param name="principal">Abstract User Principal.</param>
        /// <param name="loanId">Destination Loan ID.</param>
        /// <param name="setInitialEmployeeRoles">Should internal emploee roles should be set.</param>
        /// <param name="assignEmployeesFromRelationships">Should employees be assigned from relationships.</param>
        /// <param name="addEmployeeAsOfficialAgent">Should employee be added as official agent.</param>
        /// <param name="setPreparerContent">Should preparer content be set.</param>
        /// <param name="underwritingAuthority">The underwriting authority.</param>
        /// <param name="lenderContactRecordId">Lender contact record ID. Passed in by reference.</param>
        /// <param name="assignmentOption">Whether or not the branch should be assigned from the loan officer's banch when writing the roles to the official contacts.</param>
        private static void LoanAssignment(AbstractUserPrincipal principal, Guid loanId, bool setInitialEmployeeRoles, 
            bool assignEmployeesFromRelationships, bool addEmployeeAsOfficialAgent, bool setPreparerContent, 
            E_UnderwritingAuthority underwritingAuthority, ref Guid lenderContactRecordId, BranchAssignmentOption assignmentOption)
        {
            LoanAssignment(principal, Guid.Empty, loanId, setInitialEmployeeRoles, assignEmployeesFromRelationships, 
                addEmployeeAsOfficialAgent, false, setPreparerContent, underwritingAuthority, ref lenderContactRecordId, assignmentOption);
        }

        /// <summary>
        /// Create initial loan assignments.  When we duplicate
        /// a loan, we either setup default initial loan assignments,
        /// or we duplicate the source file's assignments.
        /// </summary>
        /// <remarks>
        /// 7/27/2017 dt - For employee roles, we set in this order, overwriting earlier data with later:
        ///   1. if (setInitialEmployeeRoles)
        ///      a. If (duplicateUserAssignment) The role assignments from the source loan
        ///      b. If (!duplicateUserAssignment) The principal user's roles (except Admin, Accountant, and Closer?! [see the InitLoanFileAssignments SP])
        ///   2. if (assignEmployeesFromRelationships) The roles as defined in the principal user's relationships, per the channel of the loan file
        /// </remarks>
        /// <param name="principal">Abstract User Principal.</param>
        /// <param name="sourceLoanId">Source Loan ID.</param>
        /// <param name="destLoanId">Destination Loan ID.</param>
        /// <param name="setInitialEmployeeRoles">Should internal emploee roles should be set.</param>
        /// <param name="assignEmployeesFromRelationships">Should employee roles be assigned from relationships.</param>
        /// <param name="addEmployeeAsOfficialAgent">Should employee be added as official agent.</param>
        /// <param name="duplicateUserAssignment">Should user assignment be duplicated from the source loan.</param>
        /// <param name="setPreparerContent">Should preparer content be set.</param>
        /// <param name="underwritingAuthority">The underwriting authority.</param>
        /// <param name="lenderContactRecordId">Lender contact record ID. Passed in by reference.</param>
        /// <param name="assignmentOption">Whether or not the branch should be assigned from the loan officer's branch when creating official contacts.</param>
        private static void LoanAssignment(AbstractUserPrincipal principal, Guid sourceLoanId, Guid destLoanId, bool setInitialEmployeeRoles, 
            bool assignEmployeesFromRelationships, bool addEmployeeAsOfficialAgent, bool duplicateUserAssignment, 
            bool setPreparerContent, E_UnderwritingAuthority underwritingAuthority, ref Guid lenderContactRecordId, BranchAssignmentOption assignmentOption)
        {
            Guid pmlBrokerId = Guid.Empty;
            if (setInitialEmployeeRoles)
            {
                pmlBrokerId = WriteInitialLoanAssignments(principal, sourceLoanId, destLoanId, duplicateUserAssignment); 
                WriteInitialLoanTeamAssignments(principal, sourceLoanId, destLoanId, duplicateUserAssignment); 
            }

            if (assignEmployeesFromRelationships)
            {
                AssignEmployeesFromRelationships(principal, destLoanId, underwritingAuthority);   // +1 Loan Save
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(destLoanId, typeof(CLoanFileCreator));
            dataLoan.AllowSaveWhileQP2Sandboxed = true;
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            if (pmlBrokerId != Guid.Empty)
            {
                // We do not want to create an originating company contact at this point
                // because the branch channel may still change before we're done creating
                // the file.
                dataLoan.UpdatePmlBrokerId(pmlBrokerId, preventContactCreation: true);
            }

            //// Set up the official agents after we've finalized the assigned
            //// employee set.

            //// Make sure the final assignments are used to form the
            //// official agent list.
            if (setInitialEmployeeRoles == true && addEmployeeAsOfficialAgent == true)
            {
                WriteInitialOfficialAgentListImpl(dataLoan, principal, assignmentOption, out lenderContactRecordId);
            }

            // OPM 32373
            if (setPreparerContent == true)
            {
                Link1003InterviewerToLoanOfficer(dataLoan, principal);
            }

            // 10/30/2014 je - OPM 190542 - Initial Originating Company Tier Assignment
            dataLoan.sPmlCompanyTierId = dataLoan.ComputePmlCompanyTierId();

            dataLoan.Save();
        }

        /// <summary>
        /// From OPM 32373 - 
        /// On loan creation, set the "To be Completed by Interviewer" section of page 3 of the 1003 to be "Same as Official Contact" w/Loan Officer as the default.
        /// This will ensure that the data propagation intended by our Licenses feature will be the default behavior.  The lender can still override this via the "Set Manually" bullet if they choose to do so.        
        /// </summary>
        /// <param name="dataLoan">The loan data.</param>
        /// <param name="principal">Abstract user principal.</param>
        private static void Link1003InterviewerToLoanOfficer(CPageData dataLoan, AbstractUserPrincipal principal)
        {
            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.CreateNew);

            if (principal != null && (principal.PortalMode == E_PortalMode.MiniCorrespondent || principal.PortalMode == E_PortalMode.Correspondent))
            {
                preparer.IsLocked = true;
            }
            else
            {
                preparer.IsLocked = false;
            }

            preparer.AgentRoleT = E_AgentRoleT.LoanOfficer;
            preparer.Update();
        }

        private void Copy1003InterviewerFromLoanOfficer(CPageData dataLoan)
        {
            // The interviewer info should usually update when the loan officer info updates.
            // The exception to this rule is when we are creating correspondent loans.
            bool linkPreparerToAgent = m_principal == null || (m_principal.PortalMode != E_PortalMode.MiniCorrespondent && m_principal.PortalMode != E_PortalMode.Correspondent);

            IPreparerFields preparer = dataLoan.PreparerCollection.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_AgentRoleT.LoanOfficer, linkPreparerToAgent);
        }

        /// <summary>
        /// Duplicates encrypted fields for a new loan file.
        /// </summary>
        /// <param name="sourceLoan">
        /// The source loan.
        /// </param>
        /// <param name="newLoan">
        /// The new loan.
        /// </param>
        private void DuplicateEncryptedFields(CPageData sourceLoan, CPageData newLoan)
        {
            for (var i = 0; i < sourceLoan.nApps; ++i)
            {
                var sourceApp = sourceLoan.GetAppData(i);
                var newApp = newLoan.GetAppData(i);

                newApp.aBSsn = sourceApp.aBSsn;
                newApp.aCSsn = sourceApp.aCSsn;

                newApp.aVaServ1Ssn = sourceApp.aVaServ1Ssn;
                newApp.aVaServ2Ssn = sourceApp.aVaServ2Ssn;
                newApp.aVaServ3Ssn = sourceApp.aVaServ3Ssn;
                newApp.aVaServ4Ssn = sourceApp.aVaServ4Ssn;

                if (!string.Equals(sourceApp.aBSsn, sourceApp.aB4506TSsnTinEin, StringComparison.Ordinal))
                {
                    newApp.aB4506TSsnTinEin = sourceApp.aB4506TSsnTinEin;
                }

                if (!string.Equals(sourceApp.aCSsn, sourceApp.aC4506TSsnTinEin, StringComparison.Ordinal))
                {
                    newApp.aC4506TSsnTinEin = sourceApp.aC4506TSsnTinEin;
                }
            }

            if (sourceLoan.sHmdaCoApplicantSourceSsnLckd)
            {
                newLoan.sHmdaCoApplicantSourceSsn = sourceLoan.sHmdaCoApplicantSourceSsn;
            }

            if (sourceLoan.sCombinedBorInfoLckd)
            {
                newLoan.sCombinedBorSsn = sourceLoan.sCombinedBorSsn;
                newLoan.sCombinedCoborSsn = sourceLoan.sCombinedCoborSsn;
            }

            if (sourceLoan.sTax1098PayerSsnLckd)
            {
                newLoan.sTax1098PayerSsn = sourceLoan.sTax1098PayerSsn;
            }

            if (sourceLoan.sFhaSponsoredOriginatorEinLckd)
            {
                newLoan.sFHASponsoredOriginatorEIN = sourceLoan.sFHASponsoredOriginatorEIN;
            }
        }

        /// <summary>
        /// Duplicates the new ULAD data layer entities and associations.
        /// </summary>
        /// <param name="sourceLoan">The source file.</param>
        /// <param name="destinationLoan">The new file.</param>
        private void DuplicateLoanLqbCollectionData(CPageData sourceLoan, CPageData destinationLoan)
        {
            // If the source file is using the new infrastructure, make sure to copy
            // all of the data over.
            if (sourceLoan.sBorrowerApplicationCollectionT != E_sLqbCollectionT.Legacy)
            {
                destinationLoan.DuplicateLqbCollectionDataFrom(sourceLoan);
            }
        }

        /// <summary>
        /// Updates data on the duplicated loan that uses consumer
        /// identifiers from the source loan.
        /// </summary>
        /// <param name="sourceLoan">
        /// The source loan.
        /// </param>
        /// <param name="newLoan">
        /// The new loan.
        /// </param>
        private void UpdateConsumerIdentifierDependentData(CPageData sourceLoan, CPageData newLoan)
        {
            var oldConsumerIdToNewConsumerIdMappings = this.GetOldConsumerIdToNewConsumerIdMappings(sourceLoan, newLoan);

            foreach (var loanEstimateDate in newLoan.sLoanEstimateDatesInfo.LoanEstimateDatesList)
            {
                loanEstimateDate.UpdateConsumerIds(oldConsumerIdToNewConsumerIdMappings);
            }

            foreach (var closingDisclosureDate in newLoan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList)
            {
                closingDisclosureDate.UpdateConsumerIds(oldConsumerIdToNewConsumerIdMappings);
            }

            // Serialize and store new values for the dates.
            newLoan.sLoanEstimateDatesInfo = newLoan.sLoanEstimateDatesInfo;
            newLoan.sClosingDisclosureDatesInfo = newLoan.sClosingDisclosureDatesInfo;
        }

        /// <summary>
        /// Retrieves the mappings between old consumer IDs and new consumer IDs.
        /// </summary>
        /// <param name="sourceLoan">
        /// The source loan.
        /// </param>
        /// <param name="newLoan">
        /// The new loan.
        /// </param>
        /// <returns>
        /// The mappings.
        /// </returns>
        private Dictionary<Guid, Guid> GetOldConsumerIdToNewConsumerIdMappings(
            CPageData sourceLoan, 
            CPageData newLoan)
        {
            var dictionary = new Dictionary<Guid, Guid>(sourceLoan.nApps * 2);

            for (int i = 0; i < sourceLoan.nApps; ++i)
            {
                var sourceLoanApp = sourceLoan.GetAppData(i);
                var newLoanApp = newLoan.GetAppData(i);

                dictionary.Add(sourceLoanApp.aBConsumerId, newLoanApp.aBConsumerId);
                dictionary.Add(sourceLoanApp.aCConsumerId, newLoanApp.aCConsumerId);
            }

            return dictionary;
        }

        private void RunTriggers()
        {
            HashSet<string> triggerList = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            Dictionary<string, IEnumerable<string>> triggerDependencyFieldDictionary = new Dictionary<string, IEnumerable<string>>();
            foreach (var o in this.m_broker.GetTaskTriggerTemplateIdToCreationTriggerNameMap())
            {
                triggerList.Add(o.Value);
            }

            if (triggerList.Count == 0)
            {
                return;
            }

            IConfigRepository configRepository = ConfigHandler.GetRepository(m_principal.BrokerId);
            ExecutingEngine engine = ExecutingEngine.GetEngineByBrokerId(configRepository, m_principal.BrokerId);
            ExecutingEngine systemEngine = engine.SystemExecutingEngine;

            foreach (var trigger in triggerList)
            {
                var set = LendingQBExecutingEngine.GetDependencyFieldsByTrigger(m_principal.BrokerId, trigger);

                if (set.Any())
                {
                    triggerDependencyFieldDictionary.Add(trigger, set);
                }
            }

            HashSet<string> list = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            foreach (var set in triggerDependencyFieldDictionary.Values)
            {
                list.UnionWith(set);
            }

            CPageData data = new NotEnforceAccessControlPageData(m_Core.FileId, list);
            data.AllowLoadWhileQP2Sandboxed = true;
            data.ByPassFieldSecurityCheck = true;
            data.SetFormatTarget(FormatTarget.Webform);
            data.InitLoad();
            LoanValueEvaluator evaluator = new LoanValueEvaluator(data, data.GetAppData(0));
            // We must set the principal because the LoanValueEvaluator will fail for certain
            // values without it, including branch/originating company/user groups.
            // gf opm 450535
            evaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(this.m_principal));

            HashSet<string> hitTriggerSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            foreach (var trigger in triggerDependencyFieldDictionary.Keys)
            {
                bool bEval = false;
                if (engine.HasCustomVariable(trigger))
                {
                    bEval = engine.EvaluateCustomVar(trigger, evaluator);
                }
                else
                {
                    bEval = systemEngine.EvaluateCustomVar(trigger, evaluator);
                }
                if (bEval)
                {
                    hitTriggerSet.Add(trigger);
                }

            }

            if (hitTriggerSet.Count > 0)
            {

                Guid aAppId = data.GetAppData(0).aAppId; // 12/27/2011 dd - Use primary app for now.
                foreach (var o in this.m_broker.GetTaskTriggerTemplateIdToCreationTriggerNameMap())
                {
                    if (hitTriggerSet.Contains(o.Value))
                    {
                        TaskTriggerQueueItem item = new TaskTriggerQueueItem(o.Key, o.Value, this.m_Core.BrokerId, m_Core.FileId, aAppId, Guid.Empty);

                        TaskTriggerQueue.Send(item);
                    }
                }
            }

        }

        private void EnqueueTasksDueDateUpdate()
        {
            // 9/12/2011 dd - Update task count after commit file.
            TaskUtilities.EnqueueTasksDueDateUpdate(m_Core.BrokerId, m_Core.FileId);
        }

        /// <summary>
        /// Won't run if: no source file or isForGoogle <para></para>
        /// OPM 185732, 70088.
        /// </summary>
        private void DuplicateTasksToLoan()
        {
            //OPM 70088. Moved the task duplication here when a loan is created from template so that all role assigments saved before tasks are imported from template. Task assignment and ownership depends on loan role assignements
            //av dont bother creating task for google
            if (null == m_Core.SourceFileId || Guid.Empty == m_Core.SourceFileId)
            {
                return;
            }

            Task.DuplicateTasksToLoan(m_Core.BrokerId, m_Core.SourceFileId, m_Core.FileId, m_Core.EmployeeId, false, false /* Do not check permission of src file */, true /*copy sort order from template*/, false);
        }

        /// <summary>
        /// Deletes and reinitialize loan assignments by using the creating user's
        /// relationships.
        /// </summary>
        /// <remarks>
        /// 9/3/2004 kb - We always lay down a starting set.  It
        /// may be that we skip assigning employees from relationships,
        /// in which case this write will remain.
        /// 6/27/2017 dt - The above comment is probably now completely
        /// wrong, but at this point I don't have time to correct it.
        /// FIX THIS COMMENT.
        /// </remarks>
        /// <param name="principal">Abstract User Principal.</param>
        /// <param name="loanId">Destination Loan ID.</param>
        /// <param name="underwritingAuthority">The underwriting authority.</param>
        public static void RefreshLoanAssignments(AbstractUserPrincipal principal, Guid loanId, E_UnderwritingAuthority underwritingAuthority)
        {
            // Delete assignments in DB.
            DeleteLoanAssignments(principal, loanId, false);
            DeleteLoanTeamAssignments(principal, loanId);

            // Recreate Assignments in DB.
            Guid pmlBrokerId = WriteInitialLoanAssignments(principal, Guid.Empty, loanId, false);
            WriteInitialLoanTeamAssignments(principal, Guid.Empty, loanId, false);

            AssignEmployeesFromRelationships(principal, loanId, underwritingAuthority);   // +1 Loan Save

            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(CLoanFileCreator));
            dataLoan.AllowSaveWhileQP2Sandboxed = true;
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            // Clear Agents and Preparers.
            dataLoan.sAgentDataSetClear();
            dataLoan.sPreparerDataSetClear();
            dataLoan.UpdatePmlBrokerId(Guid.Empty, preventContactCreation: true);

            if (pmlBrokerId != Guid.Empty)
            {
                dataLoan.UpdatePmlBrokerId(pmlBrokerId, preventContactCreation: true);
            }

            Guid lenderContactRecordId = Guid.Empty;
            WriteInitialOfficialAgentListImpl(dataLoan, principal, BranchAssignmentOption.Allow, out lenderContactRecordId);

            Link1003InterviewerToLoanOfficer(dataLoan, principal);

            // opm 453388 ejm - We need to recompute the price group id in case the price groups have changed at the branch/broker level.
            dataLoan.sProdLpePriceGroupId = dataLoan.ComputeLpePriceGroupId();
            dataLoan.sPmlCompanyTierId = dataLoan.ComputePmlCompanyTierId();

            AddOriginatingCompanyContact(dataLoan, lenderContactRecordId);

            // Save loan.
            dataLoan.Save();
        }

        /// <summary>
        /// Calls SQL to delete initial loan team assignments.
        /// </summary>
        /// <param name="principal">Abstract User Principal.</param>
        /// <param name="loanID">The Loan ID.</param>
        private static void DeleteLoanTeamAssignments(AbstractUserPrincipal principal, Guid loanID)
        {
            SqlParameter[] parameters =
                {
                    new SqlParameter("@LoanId", loanID)
                };
            StoredProcedureHelper.ExecuteNonQuery(principal.BrokerId, "DeleteLoanFileTeamAssignments", 5, parameters);
        }

        /// <summary>
        /// Calls SQL to delete initial loan assignments.
        /// </summary>
        /// <param name="principal">Abstract User Principal.</param>
        /// <param name="loanId">The Loan ID.</param>
        /// <param name="updatePmlBroker">If true, PmlBrokerId is updated to Guid.Empty (adds and extra loan load/save).</param>
        private static void DeleteLoanAssignments(AbstractUserPrincipal principal, Guid loanID, bool updatePmlBroker)
        {
            try
            {
                SqlParameter[] parameters =
                {
                    new SqlParameter("@LoanId", loanID)
                };

                StoredProcedureHelper.ExecuteNonQuery(principal.BrokerId, "DeleteLoanFileAssignments", 5, parameters);

                if (updatePmlBroker)
                {
                    // Clear out PML Broker.
                    var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                        loanID,
                        typeof(CLoanFileCreator));
                    dataLoan.AllowSaveWhileQP2Sandboxed = true;
                    dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                    // We do not want to create an originating company contact at this point
                    // because the branch channel may still change before we're done creating
                    // the file.
                    dataLoan.UpdatePmlBrokerId(Guid.Empty, preventContactCreation: true);
                    dataLoan.Save();
                }
            }
            catch (Exception e)
            {
                throw new CBaseException("Failed to delete assignments.", e);
            }
        }

        #endregion Creation utilities

        #region Auto-committing creation methods

        /// <summary>
        /// Used to create a new blank loan template.
        /// </summary>
        /// <returns>Loan ID for the new template.</returns>
        public Guid CreateBlankLoanTemplate()
        {
            Guid templateId = BeginCreateFile(
                isTemplate: true,
                isTest: false,
                setInitalEmployeeRoles: true,
                addEmployeeAsOfficialAgent: false,
                assignEmployeesFromRelationships: false,
                defaultValues: null);

            CommitFileCreation(false);

            return templateId;
        }

        /// <summary>
        /// Used to create a new test loan from a template.
        /// </summary>
        /// <param name="templateId">The template to use.</param>
        /// <returns>Loan ID for new test file.</returns>
        public Guid CreateNewTestFile(Guid templateId = default(Guid))
        {
            return this.CreateNewTestFile(templateId: templateId, branchIdOverride: null, sourceUser: null);
        }

        /// <summary>
        /// Used to create a new test loan from a template.
        /// </summary>
        /// <param name="templateId">The template to use.</param>
        /// <param name="branchIdOverride">The branch to use when creating the loan file. Will determine loan number and will be the first branch set on the file.</param>
        /// <param name="sourceUser">The user that will be used to determine some initial data for the file. Will determine initial role assignments and final branch and branch channel assignment.</param>
        /// <returns>Loan ID for new test file.</returns>
        public Guid CreateNewTestFile(Guid templateId, Guid? branchIdOverride, AbstractUserPrincipal sourceUser)
        {
            Guid testLoanId;
            bool refreshLoanTasks;

            if (templateId == Guid.Empty)
            {
                testLoanId = BeginCreateFile(
                    isTemplate: false,
                    isTest: true,
                    setInitalEmployeeRoles: true,
                    addEmployeeAsOfficialAgent: true,
                    assignEmployeesFromRelationships: true,
                    defaultValues: null,
                    branchIdOverride: branchIdOverride,
                    sourceUser: sourceUser);
                refreshLoanTasks = false;
            }
            else
            {
                testLoanId = BeginCreateFileFromSourceLoan(
                    bMakeTemplate: false,
                    sourceFileId: templateId,
                    isCopyLoan: false,
                    setInitalEmployeeRoles: true,
                    addEmployeeAsOfficialAgent: true,
                    assignEmployeesFromRelationships: true,
                    duplicateUserAssignment: false,
                    duplicateLoanConditions: true,
                    nameAsSecond: false,
                    mustUseBranchOfSrcFile: false,
                    defaultValues: null,
                    createDummyLoanNm: false,
                    sIsLead: false,
                    branchIdToUse: branchIdOverride ?? Guid.Empty,
                    isTest: true,
                    sourceUser: sourceUser,
                    dontStompBranchOnLoanAssignment: branchIdOverride.HasValue);
                refreshLoanTasks = true;
            }

            CommitFileCreation(refreshLoanTasks);

            return testLoanId;
        }

        /// <summary>
        /// Used to create a test loan from an existing loan file.
        /// </summary>
        /// <param name="sourceLoanId">The loan file to use as a template.</param>
        /// <returns>Loan ID for new test file.</returns>
        public Guid DuplicateLoanIntoTestFile(Guid sourceLoanId)
        {
            Guid testLoanId;

            testLoanId = BeginCreateFileFromSourceLoan(
                bMakeTemplate: false,
                sourceFileId: sourceLoanId,
                isCopyLoan: true,
                setInitalEmployeeRoles: true,
                addEmployeeAsOfficialAgent: true,
                assignEmployeesFromRelationships: true,
                duplicateUserAssignment: true,
                duplicateLoanConditions: true,
                nameAsSecond: false,
                mustUseBranchOfSrcFile: false,
                defaultValues: null,
                createDummyLoanNm: false,
                sIsLead: false,
                branchIdToUse: Guid.Empty,
                isTest: true);

            CommitFileCreation(true);

            return testLoanId;
        }

        /// <summary>
        /// Create and commit a new lead file without any additional changes.
        /// </summary>
        /// <param name="templateId">The template to copy from. Pass Guid.Empty to start from a blank lead.</param>
        /// <returns>The ID of the newly created and valid lead.</returns>
        public Guid CreateLead(Guid templateId)
        {
            return this.CreateLead(templateId: templateId, branchIdOverride: null, sourceUser: null);
        }

        /// <summary>
        /// Create and commit a new lead file without any additional changes.
        /// </summary>
        /// <param name="templateId">The template to copy from. Pass Guid.Empty to start from a blank lead.</param>
        /// <param name="branchIdOverride">The branch to use when creating the loan file. Will determine loan number and will be the first branch set on the file.</param>
        /// <param name="sourceUser">The user that will be used to determine some initial data for the file. Will determine initial role assignments and final branch and branch channel assignment.</param>
        /// <returns>The ID of the newly created and valid lead.</returns>
        public Guid CreateLead(Guid templateId, Guid? branchIdOverride, AbstractUserPrincipal sourceUser)
        {
            Guid leadId = this.BeginCreateLead(templateId, branchIdOverride: branchIdOverride, sourceUser: sourceUser);
            this.CommitFileCreation(false);
            return leadId;
        }

        /// <summary>
        /// Creates a blank lead file using the new ULAD data layer.
        /// </summary>
        /// <returns>The created lead id.</returns>
        public Guid CreateBlankUladLead()
        {
            Guid leadId = this.BeginCreateNewLeadFile(useUladDataLayer: true);
            this.CommitFileCreation(false);
            return leadId;
        }

        /// <summary>
        /// Creates a blank loan set up to use the ULAD data layer.
        /// </summary>
        /// <returns>The loan id.</returns>
        public Guid CreateBlankUladLoanFile()
        {
            return CreateBlankLoanFile(branchIdOverride: null, sourceUser: null, useUladDataLayer: true);
        }

        /// <summary>
        /// Used to create a new blank loan file.
        /// </summary>
        /// <returns>Loan ID for the new loan.</returns>
        public Guid CreateBlankLoanFile()
        {
            return CreateBlankLoanFile(branchIdOverride: null, sourceUser: null);
        }

        /// <summary>
        /// Used to create a new blank loan file with some initial information.
        /// </summary>
        /// <param name="branchIdOverride">The branch to use when creating the loan file. Will determine loan number and will be the first branch set on the file.</param>
        /// <param name="sourceUser">The user that will be used to determine some initial data for the file. At the moment, this user's relationships will be used to populate the role assignments.</param>
        /// <returns>Loan ID for the new loan.</returns>
        public Guid CreateBlankLoanFile(Guid? branchIdOverride, AbstractUserPrincipal sourceUser)
        {
            return CreateBlankLoanFile(branchIdOverride, sourceUser, useUladDataLayer: false);
        }

        /// <summary>
        /// Creates and commits a new loan file from a template.
        /// </summary>
        /// <param name="templateId">The Guid of the template being used.</param>
        /// <returns>The Guid of the newly-created loan file.</returns>
        public Guid CreateLoanFromTemplate(Guid templateId)
        {
            return CreateLoanFromTemplate(templateId: templateId, branchIdOverride: null, sourceUser: null);
        }

        /// <summary>
        /// Creates and commits a new loan file from a template.
        /// </summary>
        /// <param name="templateId">The Guid of the template being used.</param>
        /// <param name="branchIdOverride">The branch to use when creating the loan file. Will determine loan number and will be the first branch set on the file.</param>
        /// <param name="sourceUser">The user that will be used to determine some initial data for the file. Will determine initial role assignments and final branch and branch channel assignment.</param>
        /// <returns>The Guid of the newly-created loan file.</returns>
        public Guid CreateLoanFromTemplate(Guid templateId, Guid? branchIdOverride, AbstractUserPrincipal sourceUser)
        {
            Guid loanId;

            loanId = BeginCreateFileFromSourceLoan(
                bMakeTemplate: false,
                sourceFileId: templateId,
                isCopyLoan: false,
                setInitalEmployeeRoles: true,
                addEmployeeAsOfficialAgent: true,
                assignEmployeesFromRelationships: true,
                duplicateUserAssignment: false,
                duplicateLoanConditions: true,
                nameAsSecond: false,
                mustUseBranchOfSrcFile: false,
                defaultValues: null,
                createDummyLoanNm: false,
                sIsLead: false,
                branchIdToUse: branchIdOverride ?? Guid.Empty,
                isTest: false,
                sourceUser: sourceUser,
                dontStompBranchOnLoanAssignment: branchIdOverride.HasValue);

            this.CommitFileCreation(false);

            return loanId;
        }

        /// <summary>
        /// Creates and commits a new loan template from an existing loan file.
        /// </summary>
        /// <param name="sourceLoanId">The Guid of the existing loan file being used.</param>
        /// <returns>The Guid of the newly-created loan file.</returns>
        public Guid CreateTemplateFromLoan(Guid sourceLoanId)
        {
            Guid loanId;

            loanId = BeginCreateFileFromSourceLoan(
                bMakeTemplate: true,
                sourceFileId: sourceLoanId,
                isCopyLoan: true,
                setInitalEmployeeRoles: true,
                addEmployeeAsOfficialAgent: false,
                assignEmployeesFromRelationships: false,
                duplicateUserAssignment: false,
                duplicateLoanConditions: true,
                nameAsSecond: false,
                mustUseBranchOfSrcFile: false,
                defaultValues: null,
                createDummyLoanNm: false, 
                sIsLead: false,
                branchIdToUse: Guid.Empty,
                isTest: false);

            this.CommitFileCreation(true);

            return loanId;
        }

        /// <summary>
        /// Creates and commits a duplicate loan file from an existing loan file.
        /// </summary>
        /// <param name="sourceLoanId">The Guid of the existing loan file being used.</param>
        /// <returns>The Guid of the newly-created loan file.</returns>
        public Guid DuplicateLoanFile(Guid sourceLoanId)
        {
            Guid loanId;

            loanId = BeginCreateFileFromSourceLoan(
                bMakeTemplate: false,
                sourceFileId: sourceLoanId, 
                isCopyLoan: true,
                setInitalEmployeeRoles: true,
                addEmployeeAsOfficialAgent: true,
                assignEmployeesFromRelationships: true,
                duplicateUserAssignment: true,
                duplicateLoanConditions: true,
                nameAsSecond: false,
                mustUseBranchOfSrcFile: false,
                defaultValues: null,
                createDummyLoanNm: false,
                sIsLead: false,
                branchIdToUse: Guid.Empty,
                isTest: false);

            this.CommitFileCreation(true); // Duplicated == duplicated tasks may need to be refreshed

            return loanId;
        }

        /// <summary>
        /// Creates a new 2nd lien loan file in a combo using data from the 1st lien.
        /// </summary>
        /// <param name="firstLoanId">The Guid of the existing 1st lien loan file.</param>
        /// <param name="isLead">Whether or not the 1st lien is a lead.</param>
        /// <param name="isTest">Whether or not the 1st lien is a test loan.</param>
        /// <returns>The Guid of the newly-created 2nd lien loan file.</returns>
        public Guid Derive2ndLienFrom1st(Guid firstLoanId, bool isLead, bool isTest)
        {
            Guid loanId;

            loanId = BeginCreateFileFromSourceLoan(
                bMakeTemplate: false,
                sourceFileId: firstLoanId,
                isCopyLoan: true,
                setInitalEmployeeRoles: true,
                addEmployeeAsOfficialAgent: true,
                assignEmployeesFromRelationships: false, // Different than straight duplication
                duplicateUserAssignment: true,
                duplicateLoanConditions: false, // Different: Changed it to false, don't expect any worth copying conditions (blame PageDataImpl.cs for why)
                nameAsSecond: true, // Different
                mustUseBranchOfSrcFile: true, // see OPM case 3309
                defaultValues: null,
                createDummyLoanNm: false,
                sIsLead: isLead,
                branchIdToUse: Guid.Empty,
                isTest: isTest);

            this.CommitFileCreation(false);

            return loanId;
        }

        /// <summary>
        /// Creates and commits a duplicate loan template from an existing loan template.
        /// </summary>
        /// <param name="sourceTemplateId">The Guid of the existing loan template being used.</param>
        /// <returns>The Guid of the newly-created loan template.</returns>
        public Guid DuplicateLoanTemplate(Guid sourceTemplateId)
        {
            Guid loanId;

            loanId = BeginCreateFileFromSourceLoan(
                bMakeTemplate: true,
                sourceFileId: sourceTemplateId,
                isCopyLoan: true,
                setInitalEmployeeRoles: true,
                addEmployeeAsOfficialAgent: true,
                assignEmployeesFromRelationships: true,
                duplicateUserAssignment: true,
                duplicateLoanConditions: true,
                nameAsSecond: false,
                mustUseBranchOfSrcFile: false,
                defaultValues: null,
                createDummyLoanNm: false,
                sIsLead: false,
                branchIdToUse: Guid.Empty,
                isTest: false);

            this.CommitFileCreation(true);

            return loanId;
        }

        /// <summary>
        /// Duplicates an existing loan file into a new sandbox loan file.
        /// </summary>
        /// <param name="sLId">The Loan ID of the loan to duplicate.</param>
        /// <returns>Loan ID of the newly created sandbox loan.</returns>
        public Guid CreateSandboxFile(Guid sLId)
        {
            Dictionary<string, object> defaults = new Dictionary<string, object>();
            defaults.Add("sLoanFileT", (int)E_sLoanFileT.Sandbox);

            Guid loanId = BeginCreateFileFromSourceLoan(
                bMakeTemplate: false,
                sourceFileId: sLId,
                isCopyLoan: true,
                setInitalEmployeeRoles: true,
                addEmployeeAsOfficialAgent: true,
                assignEmployeesFromRelationships: true,
                duplicateUserAssignment: true,
                duplicateLoanConditions: true,
                nameAsSecond: false,
                mustUseBranchOfSrcFile: false,
                defaultValues: defaults,
                createDummyLoanNm: false,
                sIsLead: false,
                branchIdToUse: Guid.Empty,
                isTest: false);

            this.CommitFileCreation(true);

            return loanId;
        }

        /// <summary>
        /// Duplicates a new file from the QuickPricerTemplateId for use as a QP1 pool file.
        /// </summary>
        /// <param name="quickPricerTemplateID">The QuickPricer template to use.</param>
        /// <returns>The Guid of the newly-created QP1 sandbox file.</returns>
        public Guid CreateQP1SandboxFile(Guid quickPricerTemplateID)
        {
            Guid loanId = BeginCreateFileFromSourceLoan(
                bMakeTemplate: false,
                sourceFileId: quickPricerTemplateID,
                isCopyLoan: false,
                setInitalEmployeeRoles: true,
                addEmployeeAsOfficialAgent: true,
                assignEmployeesFromRelationships: true,
                duplicateUserAssignment: false,
                duplicateLoanConditions: false,
                nameAsSecond: false,
                mustUseBranchOfSrcFile: false,
                defaultValues: null,
                createDummyLoanNm: true,
                sIsLead: true,
                branchIdToUse: m_Core.BranchId, // the principal's branch.
                isTest: false);

            this.CommitFileCreation(false);

            return loanId;
        }

        /// <summary>
        /// Intended to be used only by QuickPricer2LoanPoolManager because once the loan is created it needs to be added to the pool. <para></para>
        /// We want this to be as if they clicked create loan and selected the quickpricer template (LoanCreate > CreateLoanFromTemplate), except: <para></para>
        ///     - It has sLoanFileT QuickPricer2Sandboxed <para></para>
        ///     - A dummy loan name will be created. <para></para>
        ///     - No mersmin <para></para>
        ///     - Branch comes from core's principal. <para></para>
        ///     - It is in lead status. <para></para>
        ///     <para></para>
        /// OPM 185732. <para></para>
        /// </summary>
        /// <param name="quickPricerTemplateID"></param>
        /// <returns></returns>
        public Guid CreateQP2SandboxFile(Guid quickPricerTemplateID)
        {
            Dictionary<string, object> defaults = new Dictionary<string, object>();
            defaults.Add("sLoanFileT", (int)E_sLoanFileT.QuickPricer2Sandboxed);

            Guid loanId = BeginCreateFileFromSourceLoan(
                bMakeTemplate: false,
                sourceFileId: quickPricerTemplateID,
                isCopyLoan: false,
                setInitalEmployeeRoles: true,
                addEmployeeAsOfficialAgent: true,
                assignEmployeesFromRelationships: true,
                duplicateUserAssignment: false,
                duplicateLoanConditions: true,
                nameAsSecond: false,
                mustUseBranchOfSrcFile: false,
                defaultValues: defaults,
                createDummyLoanNm: true,
                sIsLead: true,
                branchIdToUse: m_Core.BranchId, // the principal's branch.
                isTest: false);

            this.CommitFileCreation(false);

            return loanId;
        }

        /// <summary>
        /// Create a new loan file for population from LoansPQ.
        /// </summary>
        /// <param name="templateId">The template to copy from.</param>
        /// <param name="defaultValues">The default values to apply.</param>
        /// <returns>The ID of the newly-created loan file.</returns>
        public Guid CreateNewLoanFileForLoansPQ(Guid templateId, IDictionary<string, object> defaultValues)
        {
            Guid loanId;

            if (templateId == Guid.Empty)
            {
                loanId = this.BeginCreateFile(
                    isTemplate: false,
                    isTest: false,
                    setInitalEmployeeRoles: true,
                    addEmployeeAsOfficialAgent: true,
                    assignEmployeesFromRelationships: true,
                    defaultValues: defaultValues);
            }
            else
            {
                loanId = this.BeginCreateFileFromSourceLoan(
                    bMakeTemplate: false,
                    sourceFileId: templateId,
                    isCopyLoan: false,
                    setInitalEmployeeRoles: true,
                    addEmployeeAsOfficialAgent: false, // Not sure why we do this for no template, but don't when there is a template.
                    assignEmployeesFromRelationships: true,
                    duplicateUserAssignment: false,
                    duplicateLoanConditions: true,
                    nameAsSecond: false,
                    mustUseBranchOfSrcFile: false,
                    defaultValues: defaultValues,
                    createDummyLoanNm: false,
                    sIsLead: false,
                    branchIdToUse: Guid.Empty,
                    isTest: false);
            }

            this.CommitFileCreation(false);

            return loanId;
        }

        #endregion Auto-committing creation methods

        #region Manual committing creation methods

        /// <summary>
        /// Creates a base loan into which import data is written.
        /// </summary>
        /// <param name="sourceFileId">The template (or other loan) to copy from.</param>
        /// <param name="setInitialEmployeeRoles">Bit to control if the creating user is assigned to all available roles in the loan.</param>
        /// <param name="addEmployeeOfficialAgent">Bit to control if offical contacts are created for assigned employees.</param>
        /// <param name="assignEmployeesFromRelationships">Bit to control if relationships of the creating user are assigned to their roles in the loan.</param>
        /// <param name="branchIdToUse">The ID of the Branch for the initial assignment to use (see comments in method for significan caveats...).</param>
        /// <param name="isLead">Bit to control if the new loan is a lead.</param>
        /// <returns>The ID of the (still invalid) loan file created.</returns>
        public Guid BeginCreateImportBaseLoanFile(Guid sourceFileId, bool setInitialEmployeeRoles, bool addEmployeeOfficialAgent, bool assignEmployeesFromRelationships, Guid branchIdToUse, bool isLead = false)
        {
            Guid loanId;

            // 6/27/2017 dt - What BranchId gets used in the existing code is strange.
            // In the BeginCreateLoanFile path, the BranchId used is the one passed
            // in to the factory method (which defaults to the one from the principal if
            // one is not passed in). In the BeginCreateFileFromSourceLoan path, the
            // BranchId is set from the first good result from:
            //   - The source loan file
            //   - The assigned loan officer
            //   - The one passed in to the factory method
            // To maintain the existing behavior, it should suffice to pass in the same
            // BranchId that is used in the BeginCreateFileFromSourceLoan call. This will
            // be rationalized in a later phase of the refactor (hello SDE from 2021!
            // How much RAM do you have in your machine? I hope it is more than 16GB...).
            if (isLead)
            {
                loanId = this.BeginCreateLead(
                    sourceFileId: sourceFileId,
                    setInitialEmployeeRoles: setInitialEmployeeRoles,
                    addEmployeeAsOfficialAgent: addEmployeeOfficialAgent,
                    assignEmployeesFromRelationships: assignEmployeesFromRelationships);
            }
            else if (sourceFileId == Guid.Empty)
            {
                loanId = this.BeginCreateFile(
                isTemplate: false,
                isTest: false,
                setInitalEmployeeRoles: setInitialEmployeeRoles,
                addEmployeeAsOfficialAgent: addEmployeeOfficialAgent,
                assignEmployeesFromRelationships: assignEmployeesFromRelationships,
                defaultValues: null);
            }
            else
            {
                loanId = this.BeginCreateFileFromSourceLoan(
                    bMakeTemplate: false,
                    sourceFileId: sourceFileId,
                    isCopyLoan: false,
                    setInitalEmployeeRoles: setInitialEmployeeRoles,
                    addEmployeeAsOfficialAgent: addEmployeeOfficialAgent,
                    assignEmployeesFromRelationships: assignEmployeesFromRelationships,
                    duplicateUserAssignment: false,
                    duplicateLoanConditions: true,
                    nameAsSecond: false,
                    mustUseBranchOfSrcFile: false,
                    defaultValues: null,
                    createDummyLoanNm: false,
                    sIsLead: false,
                    branchIdToUse: branchIdToUse,
                    isTest: false);
            }

            // Insert pre-commit function execution code here.
            return loanId;
        }

        /// <summary>
        /// Begin creating a new lead file that will have additional changes applied before committing.
        /// </summary>
        /// <param name="sourceFileId">The template or other loan file to copy. Pass Guid.Empty to start from a blank lead.</param>
        /// <param name="branchIdOverride">The branch to use when creating the loan file. Will determine loan number and will be the first branch set on the file.</param>
        /// <param name="sourceUser">The user that will be used to determine some initial data for the file. Will determine initial role assignments and final branch and branch channel assignment.</param>
        /// <returns>The ID of the newly created (but still invalid) lead.</returns>
        public Guid BeginCreateLead(Guid sourceFileId, bool setInitialEmployeeRoles = true, bool addEmployeeAsOfficialAgent = true, bool assignEmployeesFromRelationships = true,
            Guid? branchIdOverride = null, AbstractUserPrincipal sourceUser = null)
        {
            Guid leadId;

            if (sourceFileId == Guid.Empty)
            {
                leadId = BeginCreateNewLeadFile(branchIdOverride, sourceUser);
            }
            else
            {
                leadId = BeginCreateFileFromSourceLoan(
                    bMakeTemplate: false,
                    sourceFileId: sourceFileId,
                    isCopyLoan: false,
                    setInitalEmployeeRoles: setInitialEmployeeRoles,
                    addEmployeeAsOfficialAgent: addEmployeeAsOfficialAgent,
                    assignEmployeesFromRelationships: assignEmployeesFromRelationships,
                    duplicateUserAssignment: false,
                    duplicateLoanConditions: true,
                    nameAsSecond: false,
                    mustUseBranchOfSrcFile: false,
                    defaultValues: null,
                    createDummyLoanNm: false,
                    sIsLead: true,
                    branchIdToUse: branchIdOverride ?? Guid.Empty,
                    isTest: false,
                    sourceUser: sourceUser,
                    dontStompBranchOnLoanAssignment: branchIdOverride.HasValue);
            }

            return leadId;
        }

        /// <summary>
        /// Begin creating a new loan file from a lead file, for the purposes of lead->loan conversion.
        /// </summary>
        /// <param name="sourceLeadId">The ID of the lead to start converting.</param>
        /// <param name="defaultValues">The default values to apply.</param>
        /// <param name="isLead">Whether or not the new file should be a lead (I'm as mystified as you about why this exists).</param>
        /// <returns>The ID if the newly created (but still invalid) loan created from the lead.</returns>
        public Guid BeginCreateNewLoanFromLead(Guid sourceLeadId, IDictionary<string, object> defaultValues, bool isLead)
        {
            return this.BeginCreateFileFromSourceLoan(
                bMakeTemplate: false,
                sourceFileId: sourceLeadId,
                isCopyLoan: false,
                setInitalEmployeeRoles: false,
                addEmployeeAsOfficialAgent: false,
                assignEmployeesFromRelationships: false,
                duplicateUserAssignment: true,
                duplicateLoanConditions: true,
                nameAsSecond: false,
                mustUseBranchOfSrcFile: false,
                defaultValues: defaultValues,
                createDummyLoanNm: false,
                sIsLead: isLead,
                branchIdToUse: Guid.Empty,
                isTest: false);
        }

        /// <summary>
        /// Intended to be used only by PMLService::ConvertLeadToLoanImpl when converting a QP loan to lead or loan.
        /// </summary>
        /// <param name="quickPricerLoanID">Original QP loan ID.</param>
        /// <returns></returns>
        public Guid DuplicateQP2SandboxFileToLoan(Guid quickPricerLoanID, bool convertToLead)
        {
            Dictionary<string, object> defaults = new Dictionary<string, object>();
            defaults.Add("sLoanFileT", (int)E_sLoanFileT.Loan);

            return BeginCreateFileFromSourceLoan(
                false, // bMakeTemplate
                quickPricerLoanID,
                false, // isCopyLoan
                true, // setInitialEmployeeRoles
                true, // addEmployeeAsOfficialAgent
                true, // assignEmployeesFromRelationships
                false, // duplicateuserAssignment
                false, // duplicateLoanConditions
                false, // nameAsSecond
                false, // mustUseBranchOfSrcFile
                defaults,
                false, // createDummyLoanNm
                convertToLead, // sIsLead
                m_Core.BranchId, // branchIdToUse // the principal's branch.
                false // isTest
            );
        }

        #endregion Manual committing creation methods

        #region Creation methods to retain as private

        /// <summary>
        /// Used to create a new blank loan file with some initial information.
        /// </summary>
        /// <param name="branchIdOverride">The branch to use when creating the loan file. Will determine loan number and will be the first branch set on the file.</param>
        /// <param name="sourceUser">The user that will be used to determine some initial data for the file. At the moment, this user's relationships will be used to populate the role assignments.</param>
        /// <param name="useUladDataLayer">A value indicating whether the file should be created to use the new ULAD data layer. Passing true will result in the creation of a ULAD application.</param>
        /// <returns>Loan ID for the new loan.</returns>
        private Guid CreateBlankLoanFile(Guid? branchIdOverride, AbstractUserPrincipal sourceUser, bool useUladDataLayer)
        {
            Guid loanId = BeginCreateFile(
                isTemplate: false,
                isTest: false,
                setInitalEmployeeRoles: true,
                addEmployeeAsOfficialAgent: true,
                assignEmployeesFromRelationships: true,
                defaultValues: null,
                branchIdOverride: branchIdOverride,
                sourceUser: sourceUser,
                useUladDataLayer: useUladDataLayer);

            this.CommitFileCreation(false);

            return loanId;
        }

        /// <summary>
        /// Create new loan file according to specified options. 
        /// </summary>
        /// <param name="branchIdOverride">The branch to use when creating the loan file. Will determine loan number and will be the first branch set on the file.</param>
        /// <param name="sourceUser">The user that will be used to determine some initial data for the file. Will determine initial role assignments and final branch and branch channel assignment.</param>
        /// <param name="useUladDataLayer">A value indicating whether the file should be created on the ULAD data layer. This will result in the creation of a ULAD application for the file.</param>
        /// <returns>
        /// Id of newly created loan.
        /// </returns>
        private Guid BeginCreateFile(bool isTemplate, bool isTest, bool setInitalEmployeeRoles, bool addEmployeeAsOfficialAgent, bool assignEmployeesFromRelationships, IDictionary<string, object> defaultValues, 
            Guid? branchIdOverride = null, AbstractUserPrincipal sourceUser = null, bool useUladDataLayer = false)
        {
            if (useUladDataLayer && ConstStage.DisableBorrowerApplicationCollectionTUpdates)
            {
                throw new CBaseException(ErrorMessages.Generic, "Cannot create file on ULAD data layer because it is disabled for this environment.");
            }

            // 9/3/2004 kb - Start by creating a new loan file using the new
            // embedded core creation helper.  After that, we set loan assignments
            // from relationships.
            try
            {
                // Check if invoking user has permission to create loan
                // templates.  If creating a loan template (and this is
                // the only way to do it) and the user lacks permission,
                // then we punt in a loud way.

                if (isTemplate == true && m_CannotCreateLoanTemplate == true)
                {
                    throw new CBaseException
                        ("Unable to create new loan template.  Permission denied."
                        , "Unable to create new loan template.  Permission denied."
                        );
                }
                if (m_broker == null)
                {
                    throw new CBaseException(ErrorMessages.Generic, "Broker object should already initialize");
                }
                // Begin new loan file and save it as invalid in the
                // database.  Subsequent transactions should expect
                // this loan to be in its row on success.

                bool isUsingMersNamingScheme = m_broker.BrokerNamingScheme == E_BrokerNamingSchemeT.MersNaming;
                for (int i = 1; i <= 5; ++i)
                {
                    try
                    {
                        //OPM 51224 : Creator's branch is the default branch Id
                        Guid branchIdToUse = branchIdOverride ?? m_Core.BranchId;

                        // 12/29/2004 kb - Get the loan's name.  At one point,
                        // we used a temporary name at this stage and then set
                        // the final name (usually an auto-incrementing pattern
                        // name) after we created the rows.  This was in place
                        // to guarantee that we didn't increment the auto-name
                        // broker counter until we were sure we had a valid
                        // loan on the books.  No one cares about gaps in the
                        // sequential naming anymore, so we name the loan at
                        // the same time we create it to minimize row locking.
                        // As a result, it is no longer optional to use auto-
                        // naming for new blank loans -- we always name in
                        // this fashion (remember that friendly id labeling
                        // of loans is a kind-of auto-naming strategy).  If
                        // the name collides, then we'll fail on begin create
                        // file and try again with a fresh name.  If naming
                        // is ok, but we fail because of something else, we
                        // will suck up as many loan names from the broker's
                        // auto counter as we had trips through the retry.

                        string loanNameToUse;
                        string sMersMin = string.Empty;
                        string sLRefNm = string.Empty;

                        string prefix = "";
                        if (isTest)
                        {
                            prefix = "TEST";
                        }
                        else if (isTemplate)
                        {
                            prefix = "Template";
                        }

                        bool enableMersGeneration = isTemplate || isTest ? false : true;

                        loanNameToUse = GetLoanAutoName(m_Core.BrokerId, false, isTest, branchIdToUse, prefix, enableMersGeneration, out sMersMin, out sLRefNm);

                        if (isTest)
                        {
                            sLRefNm = "T-" + GetRandom4CharsForPrefix() + ":L:" + loanNameToUse;
                        }

                        // 2/14/2014 gf - opm 150695, honor the broker setting 
                        // for combining GFE/SC data sets when creating blank loan file.
                        if (defaultValues == null)
                        {
                            defaultValues = new Dictionary<string, object>();
                        }

                        // OPM 217783 - if isTest, the need to set value of sLoanFileT
                        if (isTest)
                        {
                            if (defaultValues.ContainsKey("sLoanFileT"))
                            {
                                defaultValues["sLoanFileT"] = (int)E_sLoanFileT.Test;
                            }
                            else
                            {
                                defaultValues.Add("sLoanFileT", (int)E_sLoanFileT.Test);
                            }
                        }

                        // Even if sUseGFEDataForSCFields wasn't passed in, it 
                        // is possible that the key will exist if the creation
                        // fails and it loops back to try again.
                        if (!defaultValues.ContainsKey("sUseGFEDataForSCFields"))
                        {
                            defaultValues.Add("sUseGFEDataForSCFields", true);
                        }

                        // OPM 169207 - Set sIsRequireFeesFromDropDown to true based on broker setting.
                        if (m_broker.FeeTypeRequirementT != E_FeeTypeRequirementT.Never && !defaultValues.ContainsKey("sIsRequireFeesFromDropDown"))
                        {
                            defaultValues.Add("sIsRequireFeesFromDropDown", true);
                        }

                        if (!defaultValues.ContainsKey("sClosingCostFeeVersionT"))
                        {
                            defaultValues.Add("sClosingCostFeeVersionT", m_broker.MinimumDefaultClosingCostDataSet);
                            if (!defaultValues.ContainsKey("sIsHousingExpenseMigrated") && m_broker.MinimumDefaultClosingCostDataSet == E_sClosingCostFeeVersionT.Legacy)
                            {
                                defaultValues.Add("sIsHousingExpenseMigrated", false);
                            }
                            else
                            {
                                defaultValues.Add("sIsHousingExpenseMigrated", true);
                            }
                        }

                        if (!defaultValues.ContainsKey("sLoanVersionT"))
                        {
                            defaultValues.Add("sLoanVersionT", LoanDataMigrationUtils.GetLatestVersion());
                        }

                        if (!defaultValues.ContainsKey("sUse2016ComboAndProrationUpdatesTo1003Details"))
                        {
                            defaultValues.Add("sUse2016ComboAndProrationUpdatesTo1003Details", m_broker.Force2016ComboAndProrationUpdatesTo1003Details);
                        }

                        if (!defaultValues.ContainsKey("sAprCalculationT"))
                        {
                            var broker = BrokerDB.RetrieveById(m_Core.BrokerId);
                            var defaultAprCalculationMethod = broker.IsUseIrregularFirstPeriodAprCalc
                                ? E_sAprCalculationT.Actuarial_AccountForIrregularFirstPeriod
                                : E_sAprCalculationT.Actuarial_IgnoreIrregularFirstPeriod;

                            defaultValues.Add("sAprCalculationT", defaultAprCalculationMethod);
                        }

                        if (!defaultValues.ContainsKey("sCalculateInitialLoanEstimate"))
                        {
                            defaultValues.Add("sCalculateInitialLoanEstimate", true);
                        }

                        if (!defaultValues.ContainsKey("sCalculateInitialClosingDisclosure"))
                        {
                            defaultValues.Add("sCalculateInitialClosingDisclosure", true);
                        }

                        if ((m_broker.CreateBlankFilesOnUladDataLayer || useUladDataLayer) 
                            && !defaultValues.ContainsKey("sBorrowerApplicationCollectionT"))
                        {
                            defaultValues.Add("sBorrowerApplicationCollectionT", EnumHelper.MaxValue<E_sLqbCollectionT>());
                        }

                        if (!defaultValues.ContainsKey("sAlwaysRequireAllBorrowersToReceiveClosingDisclosure"))
                        {
                            defaultValues.Add("sAlwaysRequireAllBorrowersToReceiveClosingDisclosure", this.m_broker.RequireAllBorrowersToReceiveCD);
                        }

                        var encryptionMigrationVersion = EncryptionMigrationVersionUtils.GetLatestVersion();
                        var encryptionKeyPair = this.EncryptionKeyDriver.GenerateKey();

                        sLRefNm = RetryFileCreationOnRefNmConflict(
                            loanNameToUse, 
                            sLRefNm, 
                            s => m_Core.BeginCreateFile(isTemplate, branchIdToUse, loanNameToUse, sMersMin, s, encryptionKeyPair.Key, encryptionMigrationVersion, defaultValues));

                        // If we got here, then we break out of the retry
                        // loop and continue because we have a valid file.

                        break;
                    }
                    catch (Exception e)
                    {
                        if (i == 5)
                        {
                            // 12/29/2004 kb - We moved the retry loop to
                            // this level...  If the counter is maxed out,
                            // then we failed for the last time.

                            Tools.LogErrorWithCriticalTracking(string.Format("After {3} retries, failed to create loan file for loginName = {0}, employeeId = {1}, isTemplate = {2}."
                                , m_Core.LoginName
                                , m_Core.EmployeeId
                                , isTemplate
                                , i
                                ), e
                                );

                            throw new CBaseException(ErrorMessages.Generic, "Failed to begin. " + e.ToString());
                        }
                        else
                        {
                            // 12/29/2004 kb - One more time!  We previously
                            // looped at the core level.  We now do it here
                            // and delay between retries.

                            Tools.LogWarning(string.Format("Core create file failure for {0} on attempt #{1}.", m_Core.LoginName, i));

                            Tools.SleepAndWakeupRandomlyWithin(1000, 4500);
                        }
                    }
                }

                try
                {
                    // ejm opm 466492 - For new files, if there is a branch id override, we don't want to assign the branch from the loan officer, so we'll suppress branch assignment.
                    // Branch changes due to portal mode and the like after this are still valid though.
                    BranchAssignmentOption assignmentOption = branchIdOverride.HasValue ? BranchAssignmentOption.Suppress : BranchAssignmentOption.Allow;
                    LoanAssignment(sourceUser ?? m_principal, m_Core.FileId, setInitalEmployeeRoles, 
                        assignEmployeesFromRelationships,  addEmployeeAsOfficialAgent, true /*setPreparerContent*/, 
                        E_UnderwritingAuthority.PriorApproved, ref createdLenderContactRecordId, assignmentOption);
                }
                catch (CBaseException e)
                {
                    throw new LoanFileCreatorException(e.UserMessage, this, e);
                }

                var principalForBranchAssignment = sourceUser ?? m_principal;
                if (principalForBranchAssignment.Type == "P")
                {
                    CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(m_Core.FileId, typeof(CLoanFileCreator));
                    dataLoan.AllowSaveWhileQP2Sandboxed = true;
                    dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                    EmployeeDB empDB = EmployeeDB.RetrieveById(principalForBranchAssignment.BrokerId, principalForBranchAssignment.EmployeeId);
                    switch (principalForBranchAssignment.PortalMode)
                    {
                        case E_PortalMode.Broker:
                            dataLoan.sIsBranchActAsOriginatorForFileTri = E_TriState.No;
                            dataLoan.sIsBranchActAsLenderForFileTri = E_TriState.Yes;

                            dataLoan.sCorrespondentProcessT = E_sCorrespondentProcessT.Blank;

                            dataLoan.sGfeIsTPOTransaction = true;
                            dataLoan.AssignBranch(empDB.BranchID);
                            break;
                        case E_PortalMode.MiniCorrespondent:
                            dataLoan.sIsBranchActAsOriginatorForFileTri = E_TriState.No;
                            dataLoan.sIsBranchActAsLenderForFileTri = E_TriState.No;
                            dataLoan.sCorrespondentProcessT = E_sCorrespondentProcessT.MiniCorrespondent;

                            dataLoan.sLenderCaseNumLckd = true;

                            IPreparerFields interviewer = dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.CreateNew);
                            interviewer.IsLocked = true;
                            interviewer.Update();
                            dataLoan.AssignBranch(empDB.MiniCorrespondentBranchID);
                            break;
                        case E_PortalMode.Correspondent:
                            dataLoan.sIsBranchActAsOriginatorForFileTri = E_TriState.No;
                            dataLoan.sIsBranchActAsLenderForFileTri = E_TriState.No;

                            PmlBroker pB = PmlBroker.RetrievePmlBrokerById(principalForBranchAssignment.PmlBrokerId, principalForBranchAssignment.BrokerId);

                            if (pB.UnderwritingAuthority.Equals(E_UnderwritingAuthority.Delegated))
                            {
                                dataLoan.sCorrespondentProcessT = E_sCorrespondentProcessT.Delegated;
                            }
                            else
                            {
                                dataLoan.sCorrespondentProcessT = E_sCorrespondentProcessT.PriorApproved;
                            }
                            dataLoan.sLenderCaseNumLckd = true;
                            IPreparerFields interviewerCorr = dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.CreateNew);
                            interviewerCorr.IsLocked = true;
                            interviewerCorr.Update();
                            dataLoan.AssignBranch(empDB.CorrespondentBranchID);
                            break;
                        case E_PortalMode.Retail:
                            dataLoan.sIsBranchActAsOriginatorForFileTri = E_TriState.Yes;
                            dataLoan.sIsBranchActAsLenderForFileTri = E_TriState.Yes;

                            dataLoan.sCorrespondentProcessT = E_sCorrespondentProcessT.Blank;

                            dataLoan.sGfeIsTPOTransaction = false;
                            dataLoan.AssignBranch(empDB.BranchID);
                            break;
                        case E_PortalMode.Blank:
                            break;
                        default:
                            throw new UnhandledEnumException(PrincipalFactory.CurrentPrincipal.PortalMode);

                    }

                    dataLoan.Save();
                }

                // Return whatever is the current state of the created
                // file.  If we failed, we should have thrown out of
                // here.

                if (!isTemplate)
                {
                    StatusEvent.AddStatusEvent(m_Core.FileId, m_broker.BrokerID, E_sStatusT.Loan_Open, DateTime.Now.ToString("g"), Tools.RetrieveUserNameForAudit(principalForBranchAssignment));
                }

                return m_Core.FileId;
            }
            catch (CBaseException e)
            {
                Tools.LogErrorWithCriticalTracking(e);

                throw;
            }
            catch (Exception e)
            {
                e = new CBaseException
                    (ErrorMessages.Generic
                    , string.Format
                    ("BeginCreateFile( isTemplate = {0} , setInitalEmployeeRoles = {1} , assignEmployeesFromRelationships = {2}) "
                    , isTemplate // 0
                    , setInitalEmployeeRoles // 1
                    , assignEmployeesFromRelationships // 2
                    )
                    + e.ToString()
                    );

                Tools.LogErrorWithCriticalTracking(e);

                throw;
            }
        }

        /// <summary>
        /// Copy the contents of existing an loan into a new loan. <para></para> 
        /// @param defaultValues - Keys in this IDictionary object must be the database field name.  <para></para>
        /// Any entries in the defaultValues parameter must also be added to the DuplicateLoanFile stored procedure.  <para></para>
        /// Uses branchIdToUse unless mustUseBranchOfSrcFile is true.  If branchIdToUse is Guid.Empty, it then tries other things.  <para></para>
        /// </summary>
        /// <param name="sourceUser">The user that will be used to determine some initial data for the file. Will determine initial role assignments and final branch and branch channel assignment.</param>
        /// <param name="dontStompBranchOnLoanAssignment">Whether we want to assign the branch during initial loan assignments. Set this to Suppress if you don't want the initial loan officer's branch to be the loan's branch.</param>
        private Guid BeginCreateFileFromSourceLoan(
            bool bMakeTemplate,
            Guid sourceFileId,
            bool isCopyLoan,
            bool setInitalEmployeeRoles,
            bool addEmployeeAsOfficialAgent,
            bool assignEmployeesFromRelationships,
            bool duplicateUserAssignment,
            bool duplicateLoanConditions,
            bool nameAsSecond,
            bool mustUseBranchOfSrcFile,
            IDictionary<string, object> defaultValues,
            bool createDummyLoanNm,
            bool sIsLead,
            Guid branchIdToUse,
            bool isTest,
            AbstractUserPrincipal sourceUser = null,
            bool dontStompBranchOnLoanAssignment = false)
        {
            // 9/3/2004 kb - Start by creating a new loan file using the new
            // embedded core creation helper.  After that, we assign employees
            // from relationships.
            if (m_broker == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "Broker object should already initialize");
            }
            try
            {
                // Begin new loan file and save it as invalid in the
                // database.  Subsequent transactions should expect
                // this loan to be in its row on success.
                CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sourceFileId, typeof(CLoanFileCreator));
                dataLoan.AllowLoadWhileQP2Sandboxed = true;
                dataLoan.InitLoad();

                for (int i = 1; i <= 5; ++i)
                {
                    try
                    {
                        // 9/3/2004 kb - Delegate to embedded creator to
                        // establish new loan (though invalid) with minimal
                        // locking of the tables.
                        //
                        // 12/28/2004 kb - We now pass in the branch we want
                        // to use up front.  This should be the final branch
                        // setting during this phase of loan cration.

                        if (mustUseBranchOfSrcFile)
                        {
                            SqlParameter[] parameters = {
                                                            new SqlParameter( "@LoanId", sourceFileId )
                                                        };
                            using (DbDataReader r = StoredProcedureHelper.ExecuteReader(m_Core.BrokerId, "GetBranchByLoanId", parameters))
                            {
                                if (r.Read())
                                    branchIdToUse = new Guid(r["sBranchId"].ToString());
                                else
                                    throw new CBaseException("Error. Cannot create new file because system cannot retrieve the source loan file's branch info at this moment.",
                                        string.Format("GetBranchByLoanId storeproc fails to retrieve branch id of loan {0}", sourceFileId));
                            }
                        }
                        else
                        {
                            // 5/16/2007 nw - OPM 4977 - if template has a default branch assignment, use that instead of the user's branch
                            if (branchIdToUse == Guid.Empty)
                            {
                                SqlParameter[] parameters = {
                                                                new SqlParameter("@TemplateId", sourceFileId)
                                                            };

                                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(m_Core.BrokerId, "GetBranchIdForNewFileByTemplateId", parameters))
                                {
                                    if (reader.Read())
                                        if (reader["sBranchIdForNewFileFromTemplate"] != System.DBNull.Value)
                                            branchIdToUse = new Guid(reader["sBranchIdForNewFileFromTemplate"].ToString());
                                }
                            }
                            if (branchIdToUse == Guid.Empty)
                            {
                                if (dataLoan.sEmployeeLoanRepId != Guid.Empty
                                    && !bMakeTemplate && isCopyLoan // OPM 135227 - 8/2013 pa - only do this for loan duplication
                                    )
                                {
                                    // OPM 126311 - 7/2013 pa - pull branch from existing loan officer before using current principal's branch
                                    var empDb = EmployeeDB.RetrieveById(m_broker.BrokerID, dataLoan.sEmployeeLoanRepId);
                                    branchIdToUse = empDb.BranchID;
                                }
                                else
                                {
                                    branchIdToUse = m_Core.BranchId;
                                }
                            }
                        }

                        // 12/29/2004 kb - Get the loan's name.  At one point,
                        // we used a temporary name at this stage and then set
                        // the final name (usually an auto-incrementing pattern
                        // name) after we created the rows.  This was in place
                        // to guarantee that we didn't increment the auto-name
                        // broker counter until we were sure we had a valid
                        // loan on the books.  No one cares about gaps in the
                        // sequential naming anymore, so we name the loan at
                        // the same time we create it to minimize row locking.
                        // As a result, it is no longer optional to use auto-
                        // naming for new blank loans -- we always name in
                        // this fashion (remember that friendly id labeling
                        // of loans is a kind-of auto-naming strategy).  If
                        // the name collides, then we'll fail on begin create
                        // file and try again with a fresh name.  If naming
                        // is ok, but we fail because of something else, we
                        // will suck up as many loan names from the broker's
                        // auto counter as we had trips through the retry.

                        string loanNameToUse;
                        string sMersMin = string.Empty;
                        string sLRefNm = string.Empty;

                        if (createDummyLoanNm)
                        {
                            // sLNm will be created on duplication.
                            // sMersMin should be blank.
                            // sLRefNm should be blank.
                            loanNameToUse = string.Empty;
                        }
                        else
                        {
                            if (isCopyLoan == true && m_loanCreationSource == E_LoanCreationSource.Sandbox)
                            {
                                loanNameToUse = GetLoanCopyName(sourceFileId, separator: "-", baseSuffix: "Sandbox");
                                sLRefNm = GetLoanCopyRefNm(sourceFileId, LoanCopyType.Sandbox);
                            }
                            else if (isCopyLoan == true && m_loanCreationSource == E_LoanCreationSource.CreateTest)
                            {
                                loanNameToUse = GetLoanCopyName(sourceFileId, separator: "-", baseSuffix: "Test");
                                sLRefNm = GetLoanCopyRefNm(sourceFileId, LoanCopyType.Test);
                            }
                            else if (isCopyLoan == true && (!nameAsSecond && !m_NameDuplicateAsNew || nameAsSecond && !m_NameSecondAsNew))
                            {
                                if (nameAsSecond)
                                {
                                    loanNameToUse = GetLoanCopyName(sourceFileId, separator: "x", baseSuffix: "2nd");
                                }
                                else
                                {
                                    loanNameToUse = GetLoanCopyName(sourceFileId, separator: "-", baseSuffix: "");
                                }

                                sLRefNm = GetLoanCopyRefNm(sourceFileId, LoanCopyType.Duplicate);

                                // If we're creating a second lien and the lender is using the LendingQB default scheme
                                // for naming subfinancing files, then we should still generate a MERS Min. Case 252520
                                // NOTE: This will burn a loan number counter.
                                if (nameAsSecond && !m_NameSecondAsNew && m_broker.IsAutoGenerateMersMin)
                                {
                                    string errorReasons;
                                    if (!Tools.TryGenerateMersMin(m_principal, loanNameToUse, out sMersMin, out errorReasons))
                                    {
                                        var errorMessage = "Unable to generate MERS Min for subfinancing file. "
                                            + $"{Environment.NewLine}{errorReasons}"
                                            + $"{Environment.NewLine}Source loan: {dataLoan.sLNm}({sourceFileId}), lender: {m_broker.CustomerCode}({m_principal.BrokerId})";
                                        Tools.LogError(errorMessage);
                                    }
                                }
                            }
                            else
                            {
                                string prefix = string.Empty;
                                if (isTest)
                                {
                                    prefix = "TEST";
                                }
                                else if (sIsLead)
                                {
                                    prefix = this.m_broker.LeadPrefix;
                                }

                                bool enableMersGeneration = (bMakeTemplate == true || isTest == true) ? false : true;

                                loanNameToUse = GetLoanAutoName(m_Core.BrokerId, sIsLead, isTest, branchIdToUse, prefix, enableMersGeneration, out sMersMin, out sLRefNm);

                                if (isTest)
                                {
                                    sLRefNm = "T-" + GetRandom4CharsForPrefix() + ":L:" + loanNameToUse;
                                }
                            }
                            if (isCopyLoan && !m_NameDuplicateAsNew && !nameAsSecond)
                            {
                                //OPM 4337: If we are copying a loan, and we are naming the duplicate after the old loan, copy the MersMin as well.
                                // OPM 232342/208438 - Don't copy MersMin for 2nd Lien Loans.
                                sMersMin = dataLoan.sMersMin;
                            }
                        }

                        // OPM 220166 - If the loan is created from a legacy template, but the broker minimum setting is greater than legacy
                        // then we need run loan migration after data is copied over from template.
                        bool bMigrateLoanToMinimumBrokerDefault = false;

                        if (defaultValues == null)
                        {
                            defaultValues = new Dictionary<string, object>();
                        }

                        if (!isCopyLoan && !defaultValues.ContainsKey(nameof(CPageData.sIsStatusEventMigrated)))
                        {
                            defaultValues.Add(nameof(CPageData.sIsStatusEventMigrated), true);
                        }

                        if (!isCopyLoan && !defaultValues.ContainsKey(nameof(CPageData.sStatusEventMigrationVersion)))
                        {
                            defaultValues.Add(nameof(CPageData.sStatusEventMigrationVersion), StatusEventMigrationVersion.MigrationWithUserName);
                        }

                        var encryptionMigrationVersion = EncryptionMigrationVersionUtils.GetLatestVersion();
                        var encryptionKeyPair = this.EncryptionKeyDriver.GenerateKey();

                        // OPM 217783 - if isTest, the need to set value of sLoanFileT
                        if (isTest)
                        {
                            if (defaultValues.ContainsKey("sLoanFileT"))
                            {
                                defaultValues["sLoanFileT"] = (int)E_sLoanFileT.Test;
                            }
                            else
                            {
                                defaultValues.Add("sLoanFileT", (int)E_sLoanFileT.Test);
                            }
                        }

                        if (nameAsSecond && !defaultValues.ContainsKey("copyFeeServiceAppHistory"))
                        {
                            defaultValues.Add("copyFeeServiceAppHistory", false);
                        }

                        // 2/14/2014 gf - opm 150695, honor the broker setting for combining GFE/SC data sets
                        // when creating loan from template. If we are duplicating a loan, we want the value
                        // to be copied from the source (non-template) file.
                        if (dataLoan.IsTemplate)
                        {
                            // Even if sUseGFEDataForSCFields wasn't passed in, it 
                            // is possible that the key will exist if the creation
                            // fails and it loops back to try again.
                            if (!defaultValues.ContainsKey("sUseGFEDataForSCFields"))
                            {
                                defaultValues.Add("sUseGFEDataForSCFields", true);
                            }

                            // OPM 169207 - Set sIsRequireFeesFromDropDown to true based on broker setting.
                            if (m_broker.FeeTypeRequirementT == E_FeeTypeRequirementT.ForAllNewLoans && !defaultValues.ContainsKey("sIsRequireFeesFromDropDown"))
                            {
                                defaultValues.Add("sIsRequireFeesFromDropDown", true);
                            }

                            // 3/26/2015 ejm - OPM 198510: If from a template, then check if broker setting is "greater". If so, then set that as default.
                            if (!defaultValues.ContainsKey("sClosingCostFeeVersionT"))
                            {
                                E_sClosingCostFeeVersionT version;
                                if (m_broker.MinimumDefaultClosingCostDataSet > dataLoan.sClosingCostFeeVersionT)
                                {
                                    if (dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
                                    {
                                        // 7/20/15 - je - Create legacy loan and migrate after loan duplication.
                                        bMigrateLoanToMinimumBrokerDefault = true;
                                        version = E_sClosingCostFeeVersionT.Legacy;
                                    }
                                    else
                                    {
                                        version = m_broker.MinimumDefaultClosingCostDataSet;
                                        defaultValues.Add("sClosingCostFeeVersionT", version);
                                    }
                                }
                                else
                                {
                                    version = dataLoan.sClosingCostFeeVersionT;
                                }

                                if (!defaultValues.ContainsKey("sIsHousingExpenseMigrated"))
                                {
                                    if (version == E_sClosingCostFeeVersionT.Legacy)
                                    {
                                        defaultValues.Add("sIsHousingExpenseMigrated", false);
                                    }
                                    else
                                    {
                                        defaultValues.Add("sIsHousingExpenseMigrated", true);
                                    }
                                }
                            }

                            if (!defaultValues.ContainsKey("sUse2016ComboAndProrationUpdatesTo1003Details"))
                            {
                                defaultValues.Add("sUse2016ComboAndProrationUpdatesTo1003Details", m_broker.Force2016ComboAndProrationUpdatesTo1003Details);
                            }

                            if (!defaultValues.ContainsKey("copyFeeServiceAppHistory") && (!defaultValues.ContainsKey("sLoanFileT") || (E_sLoanFileT)defaultValues["sLoanFileT"] == E_sLoanFileT.QuickPricer2Sandboxed || (E_sLoanFileT)defaultValues["sLoanFileT"] == E_sLoanFileT.Loan))
                            {
                                defaultValues.Add("copyFeeServiceAppHistory", false);
                            }
                        }

                        sLRefNm = RetryFileCreationOnRefNmConflict(
                            loanNameToUse, 
                            sLRefNm, 
                            s => m_Core.BeginCreateFileFromTemplate(bMakeTemplate, branchIdToUse, loanNameToUse, sourceFileId, sMersMin, s, encryptionKeyPair.Key, encryptionMigrationVersion, defaultValues, createDummyLoanNm, sIsLead));

                        // 7/20/15 - je - OPM 220166 - Run CFPB migration on non-legacy loans created from legacy template
                        if (bMigrateLoanToMinimumBrokerDefault)
                        {
                            ClosingCostFeeMigration.Migrate(m_principal, m_Core.FileId, m_broker.MinimumDefaultClosingCostDataSet);
                        }

                        // If we got here, then we break out of the retry
                        // loop and continue because we have a valid file.

                        break;
                    }
                    catch (Exception e)
                    {
                        if (i == 5)
                        {
                            // 12/29/2004 kb - We moved the retry loop to
                            // this level...  If the counter is maxed out,
                            // then we failed for the last time.

                            Tools.LogErrorWithCriticalTracking(string.Format("After {4} retries, failed to create loan file from template for loginName = {0}, employeeId = {1}, bMakeTemplate = {2}, isCopyLoan = {3}."
                                , m_Core.LoginName
                                , m_Core.EmployeeId
                                , bMakeTemplate
                                , isCopyLoan
                                , i
                                ), e
                                );

                            throw new CBaseException(ErrorMessages.Generic, "Failed to begin. " + e.ToString());
                        }
                        else
                        {
                            // 12/29/2004 kb - One more time!  We previously
                            // looped at the core level.  We now do it here
                            // and delay between retries.

                            Tools.LogWarning(string.Format("Core create file from template failure for {0} on attempt #{1}.", m_Core.LoginName, i));

                            Tools.SleepAndWakeupRandomlyWithin(1000, 4500);
                        }
                    }
                }

                // Deep copy the source file's attributes.  We also copy
                // many detached files that we store elsewhere.
                DeepCopyLoanFile(bMakeTemplate, dataLoan, nameAsSecond);

                try
                {
                    BranchAssignmentOption assignmentOption = dontStompBranchOnLoanAssignment ? BranchAssignmentOption.Suppress : BranchAssignmentOption.Allow;
                    LoanAssignment(sourceUser ?? m_principal, m_Core.SourceFileId, m_Core.FileId, setInitalEmployeeRoles, 
                        assignEmployeesFromRelationships, addEmployeeAsOfficialAgent, duplicateUserAssignment, 
                        false /*setPreparerContent*/, E_UnderwritingAuthority.PriorApproved, ref createdLenderContactRecordId, assignmentOption);
                }
                catch (CBaseException e)
                {
                    throw new LoanFileCreatorException(e.UserMessage, this, e);
                }

                CPageData newLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(m_Core.FileId, typeof(CLoanFileCreator));
                newLoan.AllowSaveWhileQP2Sandboxed = true;
                newLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                DuplicateEncryptedFields(dataLoan, newLoan);
                UpdateConsumerIdentifierDependentData(dataLoan, newLoan);

                if (nameAsSecond)
                {
                    dataLoan.CreateOrUpdateSubordinateFinancingLiens(newLoan);
                }

                //OPM 43665 - If preparer information is absent from the template, assign loan officer as the preparer
                IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.CreateNew);
                if (preparer == CPreparerFields.Empty)
                {
                    Copy1003InterviewerFromLoanOfficer(newLoan);
                }

                var principalForBranchAssignment = sourceUser ?? m_principal;
                //assign the branchchannel and correspondent process here
                if (principalForBranchAssignment.PortalMode != E_PortalMode.Blank)
                {
                    EmployeeDB empDB = EmployeeDB.RetrieveById(principalForBranchAssignment.BrokerId, principalForBranchAssignment.EmployeeId);
                    switch (principalForBranchAssignment.PortalMode)
                    {
                        case E_PortalMode.Broker:
                            newLoan.sIsBranchActAsOriginatorForFileTri = E_TriState.No;
                            newLoan.sIsBranchActAsLenderForFileTri = E_TriState.Yes;

                            newLoan.sCorrespondentProcessT = E_sCorrespondentProcessT.Blank;

                            newLoan.sGfeIsTPOTransaction = true;
                            newLoan.AssignBranch(empDB.BranchID);
                            break;
                        case E_PortalMode.MiniCorrespondent:
                            newLoan.sIsBranchActAsOriginatorForFileTri = E_TriState.No;
                            newLoan.sIsBranchActAsLenderForFileTri = E_TriState.No;
                            newLoan.sCorrespondentProcessT = E_sCorrespondentProcessT.MiniCorrespondent;

                            newLoan.sLenderCaseNumLckd = true;

                            IPreparerFields interviewer = newLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.CreateNew);
                            interviewer.IsLocked = true;
                            interviewer.Update();
                            newLoan.AssignBranch(empDB.MiniCorrespondentBranchID);
                            break;
                        case E_PortalMode.Correspondent:
                            newLoan.sIsBranchActAsOriginatorForFileTri = E_TriState.No;
                            newLoan.sIsBranchActAsLenderForFileTri = E_TriState.No;

                            PmlBroker pB = PmlBroker.RetrievePmlBrokerById(principalForBranchAssignment.PmlBrokerId, principalForBranchAssignment.BrokerId);

                            if (pB.UnderwritingAuthority.Equals(E_UnderwritingAuthority.Delegated))
                            {
                                newLoan.sCorrespondentProcessT = E_sCorrespondentProcessT.Delegated;
                            }
                            else
                            {
                                newLoan.sCorrespondentProcessT = E_sCorrespondentProcessT.PriorApproved;
                            }
                            newLoan.sLenderCaseNumLckd = true;
                            newLoan.AssignBranch(empDB.CorrespondentBranchID);
                            IPreparerFields interviewerCorr = newLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.CreateNew);
                            interviewerCorr.IsLocked = true;
                            interviewerCorr.Update();
                            break;
                        case E_PortalMode.Retail:
                            newLoan.sIsBranchActAsOriginatorForFileTri = E_TriState.Yes;
                            newLoan.sIsBranchActAsLenderForFileTri = E_TriState.Yes;

                            newLoan.sCorrespondentProcessT = E_sCorrespondentProcessT.Blank;

                            newLoan.sGfeIsTPOTransaction = false;
                            newLoan.AssignBranch(empDB.BranchID);
                            break;
                        case E_PortalMode.Blank:
                            break;
                        default:
                            throw new UnhandledEnumException(PrincipalFactory.CurrentPrincipal.PortalMode);

                    }
                }

                IPreparerFields addendumUnderwriter = newLoan.GetPreparerOfForm(E_PreparerFormT.FHAAddendumUnderwriter, E_ReturnOptionIfNotExist.CreateNew);

                addendumUnderwriter.overrideNameLocked(false);
                addendumUnderwriter.overrideLicenseLocked(false);
                addendumUnderwriter.HadInitialCheck = true;
                addendumUnderwriter.Update();

                IPreparerFields addendumMortgagee = newLoan.GetPreparerOfForm(E_PreparerFormT.FHAAddendumMortgagee, E_ReturnOptionIfNotExist.CreateNew);
                addendumMortgagee.overrideNameLocked(false);
                addendumMortgagee.HadInitialCheck = true;
                addendumMortgagee.Update();

                IPreparerFields Underwriter92900 = newLoan.GetPreparerOfForm(E_PreparerFormT.FHA92900LtUnderwriter, E_ReturnOptionIfNotExist.CreateNew);
                Underwriter92900.overrideLicenseLocked(false);
                Underwriter92900.HadInitialCheck = true;
                Underwriter92900.Update();

                IPreparerFields Underwriter203k = newLoan.GetPreparerOfForm(E_PreparerFormT.FHA203kWorksheetUnderwriter, E_ReturnOptionIfNotExist.CreateNew);
                Underwriter203k.overrideLicenseLocked(false);
                Underwriter203k.overrideNameLocked(false);
                Underwriter203k.HadInitialCheck = true;
                Underwriter203k.Update();

                IPreparerFields analysisAppraisalUnderwriter = newLoan.GetPreparerOfForm(E_PreparerFormT.FHAAnalysisAppraisalUnderwriter, E_ReturnOptionIfNotExist.CreateNew);
                analysisAppraisalUnderwriter.overrideLicenseLocked(false);
                analysisAppraisalUnderwriter.overrideNameLocked(false);
                analysisAppraisalUnderwriter.HadInitialCheck = true;
                analysisAppraisalUnderwriter.Update();

                if (this.m_broker.DefaultBorrPaidOrigCompSourceToTotalLoanAmount)
                {
                    if (nameAsSecond || bMakeTemplate || isCopyLoan || isTest || this.m_loanCreationSource == E_LoanCreationSource.Sandbox)
                    {
                        newLoan.sOriginatorCompensationBorrPaidBaseT = dataLoan.sOriginatorCompensationBorrPaidBaseT;
                    }
                    else
                    {
                        newLoan.sOriginatorCompensationBorrPaidBaseT = E_PercentBaseT.TotalLoanAmount;
                    }
                }

                newLoan.Save();

                if(!isCopyLoan && !bMakeTemplate)
                {
                    StatusEvent.AddStatusEvent(newLoan.sLId, newLoan.sBrokerId, newLoan.sStatusT, DateTime.Now.ToString("g"), Tools.RetrieveUserNameForAudit(principalForBranchAssignment));
                }

                // Copy the source loan's tasks if the new file is a copy of
                // an existing template that contains tasks.  Remember:  We
                // need to have a valid cache line for this new file before
                // duplicating loans' tasks.  Only duplicate tasks if copying
                // loan from a template.
                //
                // 9/28/2004 kb - We now copy tasks of templates when making
                // new templates from the source.

                try
                {
                    if (newLoan.sLoanFileT != E_sLoanFileT.QuickPricer2Sandboxed && (isCopyLoan == false || bMakeTemplate == true))
                    {
                        DuplicateLoanTasks("Starting new task.  Task created from template.", m_Core.BrokerId, m_Core.SourceFileId, m_Core.FileId, m_Core.LoanName, m_Core.UserId, m_Core.LoginName, m_Core.CreatedOn);
                    }
                }
                catch (Exception e)
                {
                    // Eat this error because we don't want to lose a decent
                    // loan because tasks didn't transfer.

                    Tools.LogErrorWithCriticalTracking(e);
                }

                // 10/5/2004 kb - Copy conditions that are stored as tasks
                // according to the new broker option.  If the loan has
                // none, then this operation is harmless.

                try
                {
                    if (duplicateLoanConditions == true)
                    {
                        DuplicateLoanConditions();
                    }
                }
                catch (Exception e)
                {
                    // Eat this error because we don't want to lose a decent
                    // loan because tasks didn't transfer.

                    Tools.LogErrorWithCriticalTracking(e);
                }

                // Return whatever is the current state of the created
                // file.  If we failed, we should have thrown out of
                // here.

                return m_Core.FileId;
            }
            catch (CBaseException e)
            {
                Tools.LogErrorWithCriticalTracking(e);

                throw;
            }
            catch (Exception e)
            {
                // Oops!

                e = new CBaseException
                    (ErrorMessages.Generic
                    , string.Format
                    ("BeginCreateFileFromTemplate( bMakeTemplate = {0} , sourceFileId = {1} , isCopyLoan = {2} , setInitalEmployeeRoles = {3} , assignEmployeesFromRelationships = {4} , duplicateUserAssignment = {5} , duplicateLoanConditions = {6} , nameAsSecond = {7}, mustUseBranchOfSrcFile = {8}, createDummyLoanNm = {9},  addEmployeeAsOfficialAgent = {10}). "
                    , bMakeTemplate  // 0
                    , sourceFileId // 1
                    , isCopyLoan // 2
                    , setInitalEmployeeRoles // 3
                    , assignEmployeesFromRelationships // 4
                    , duplicateUserAssignment //5
                    , duplicateLoanConditions //6
                    , nameAsSecond //7
                    , mustUseBranchOfSrcFile //8
                    , createDummyLoanNm // 9  // opm 185732
                    , addEmployeeAsOfficialAgent // 10
                    )
                    + e.ToString()
                    );

                Tools.LogErrorWithCriticalTracking(e);

                throw;
            }
        }

        /// <summary>
        /// Begin to create a new blank lead file.
        /// </summary>
        /// <param name="branchIdOverride">The branch to use when creating the loan file. Will determine loan number and will be the first branch set on the file.</param>
        /// <param name="sourceUser">The user that will be used to determine some initial data for the file. Will determine initial role assignments and final branch and branch channel assignment.</param>
        /// <param name="useUladDataLayer">
        /// A value indicating whether the file should be created on the ULAD data layer. Does not apply when the
        /// lender is set up to create leads from the QuickPricer template. This will result in the creation of a
        /// ULAD application for the file.
        /// </param>
        private Guid BeginCreateNewLeadFile(Guid? branchIdOverride = null, AbstractUserPrincipal sourceUser = null, bool useUladDataLayer = false)
        {
            if (useUladDataLayer && ConstStage.DisableBorrowerApplicationCollectionTUpdates)
            {
                throw new CBaseException(ErrorMessages.Generic, "Cannot create file on ULAD data layer because it is disabled for this environment.");
            }

            bool setInitalEmployeeRoles = true;
            bool addEmployeeAsOfficialAgent = true;
            bool assignEmployeesFromRelationships = true;
            // 9/3/2004 kb - Start by creating a new loan file using the new
            // embedded core creation helper.  After that, we assign employees
            // from relationships.

            try
            {
                // Begin new loan file and save it as invalid in the
                // database.  Subsequent transactions should expect
                // this loan to be in its row on success.

                for (int i = 1; i <= 5; ++i)
                {
                    try
                    {

                        //OPM 51224 : Creator's branch is the default branch Id
                        Guid branchIdToUse = branchIdOverride ?? m_Core.BranchId;

                        // 12/29/2004 kb - Get the loan's name.  At one point,
                        // we used a temporary name at this stage and then set
                        // the final name (usually an auto-incrementing pattern
                        // name) after we created the rows.  This was in place
                        // to guarantee that we didn't increment the auto-name
                        // broker counter until we were sure we had a valid
                        // loan on the books.  No one cares about gaps in the
                        // sequential naming anymore, so we name the loan at
                        // the same time we create it to minimize row locking.
                        // As a result, it is no longer optional to use auto-
                        // naming for new blank loans -- we always name in
                        // this fashion (remember that friendly id labeling
                        // of loans is a kind-of auto-naming strategy).  If
                        // the name collides, then we'll fail on begin create
                        // file and try again with a fresh name.  If naming
                        // is ok, but we fail because of something else, we
                        // will suck up as many loan names from the broker's
                        // auto counter as we had trips through the retry.

                        string sMersMin = string.Empty;
                        string sLRefNm = string.Empty;
                        string loanNameToUse = GetLoanAutoName(m_Core.BrokerId, true, false, branchIdToUse, this.m_broker.LeadPrefix, false /*enableMersGeneration */, out sMersMin, out sLRefNm);

                        Dictionary<string, object> keys = new Dictionary<string, object>();

                        var encryptionMigrationVersion = EncryptionMigrationVersionUtils.GetLatestVersion();
                        var encryptionKeyPair = this.EncryptionKeyDriver.GenerateKey();

                        // 9/3/2004 kb - Delegate to embedded creator to
                        // establish new loan (though invalid) with minimal
                        // locking of the tables.
                        if (this.m_broker.CreateLeadFilesFromQuickpricerTemplate && (!this.m_broker.IsEditLeadsInFullLoanEditor || this.m_broker.IsBlankTemplateInvisibleForFileCreation))
                        {
                            keys.Add("sUseGfeDataForSCFields", true);

                            // OPM 169207 - Set sIsRequireFeesFromDropDown to true based on broker setting.
                            if (m_broker.FeeTypeRequirementT != E_FeeTypeRequirementT.Never)
                            {
                                keys.Add("sIsRequireFeesFromDropDown", true);
                            }

                            // 1/5/2016 ejm opm 235454 - When creating from a QP template, use the larger of the two values.
                            CPageData qpTemplate = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.m_broker.QuickPricerTemplateId, typeof(CLoanFileCreator));
                            qpTemplate.AllowLoadWhileQP2Sandboxed = true;
                            qpTemplate.InitLoad();
                            bool bMigrateLoanToMinimumBrokerDefault = false;

                            if (!keys.ContainsKey("sClosingCostFeeVersionT"))
                            {
                                E_sClosingCostFeeVersionT version;
                                if (m_broker.MinimumDefaultClosingCostDataSet > qpTemplate.sClosingCostFeeVersionT)
                                {
                                    if (qpTemplate.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
                                    {
                                        bMigrateLoanToMinimumBrokerDefault = true;
                                        version = E_sClosingCostFeeVersionT.Legacy;
                                    }
                                    else
                                    {
                                        version = m_broker.MinimumDefaultClosingCostDataSet;
                                        keys.Add("sClosingCostFeeVersionT", version);
                                    }
                                }
                                else
                                {
                                    version = qpTemplate.sClosingCostFeeVersionT;
                                }

                                if (!keys.ContainsKey("sIsHousingExpenseMigrated") && version == E_sClosingCostFeeVersionT.Legacy)
                                {
                                    keys.Add("sIsHousingExpenseMigrated", false);
                                }
                                else
                                {
                                    keys.Add("sIsHousingExpenseMigrated", true);
                                }
                            }

                            if (!keys.ContainsKey("sUse2016ComboAndProrationUpdatesTo1003Details"))
                            {
                                keys.Add("sUse2016ComboAndProrationUpdatesTo1003Details", m_broker.Force2016ComboAndProrationUpdatesTo1003Details);
                            }

                            if (!keys.ContainsKey(nameof(CPageData.sIsStatusEventMigrated)))
                            {
                                keys.Add(nameof(CPageData.sIsStatusEventMigrated), true);
                            }

                            if (!keys.ContainsKey(nameof(CPageData.sStatusEventMigrationVersion)))
                            {
                                keys.Add(nameof(CPageData.sStatusEventMigrationVersion), StatusEventMigrationVersion.MigrationWithUserName);
                            }

                            m_Core.BeginCreateFileFromTemplate(
                                false, 
                                branchIdToUse, 
                                loanNameToUse, 
                                this.m_broker.QuickPricerTemplateId, 
                                sMersMin, 
                                sLRefNm,
                                encryptionKeyPair.Key,
                                encryptionMigrationVersion, 
                                keys, 
                                false, 
                                true);

                            // 1/5/2016 ejm opm 235454 - This needs to happen to ensure that values are correctly transferred from Legacy templates.
                            if (bMigrateLoanToMinimumBrokerDefault)
                            {
                                ClosingCostFeeMigration.Migrate(m_principal, m_Core.FileId, m_broker.MinimumDefaultClosingCostDataSet);
                            }

                            // Since this doesn't go through the standard code path for creation from a template,
                            // there are several things that will not be copied.
                            // I'm assuming this hasn't come up before because people aren't templating things
                            // that wind up in FileDB in their QuickPricer template. However, since ULAD 
                            // applications are being copied in the data layer, we're going to need to copy those
                            // here. Ultimately, we should get this to go through the code path that all other files
                            // created from a template go through.
                            // This will add another save to this codepath, but the codepath to correctly copy the
                            // data from the template does too.
                            if (qpTemplate.sBorrowerApplicationCollectionT != E_sLqbCollectionT.Legacy)
                            {
                                var newLead = CPageData.CreateUsingSmartDependencyWithSecurityBypass(m_Core.FileId, typeof(CLoanFileCreator));
                                newLead.InitSave(ConstAppDavid.SkipVersionCheck);
                                this.DuplicateLoanLqbCollectionData(qpTemplate, newLead);
                                newLead.Save();
                            }
                        }
                        else
                        {
                            // 5/1/2015 ejm - Add broker-level default minimum closing cost fee version (which in turn sets housing expenses).
                            // 1/5/2016 ejm opm 235454 - Use the minimum default closing cost version when making a completely blank lead file.
                            if (!keys.ContainsKey("sClosingCostFeeVersionT"))
                            {
                                keys.Add("sClosingCostFeeVersionT", m_broker.MinimumDefaultClosingCostDataSet);
                                if (!keys.ContainsKey("sIsHousingExpenseMigrated") && m_broker.MinimumDefaultClosingCostDataSet == E_sClosingCostFeeVersionT.Legacy)
                                {
                                    keys.Add("sIsHousingExpenseMigrated", false);
                                }
                                else
                                {
                                    keys.Add("sIsHousingExpenseMigrated", true);
                                }
                            }

                            if (!keys.ContainsKey("sLoanVersionT"))
                            {
                                keys.Add("sLoanVersionT", LoanDataMigrationUtils.GetLatestVersion());
                            }

                            if (!keys.ContainsKey("sUse2016ComboAndProrationUpdatesTo1003Details"))
                            {
                                keys.Add("sUse2016ComboAndProrationUpdatesTo1003Details", m_broker.Force2016ComboAndProrationUpdatesTo1003Details);
                            }

                            if (!keys.ContainsKey("sAprCalculationT"))
                            {
                                var broker = BrokerDB.RetrieveById(m_Core.BrokerId);
                                var defaultAprCalculationMethod = broker.IsUseIrregularFirstPeriodAprCalc
                                    ? E_sAprCalculationT.Actuarial_AccountForIrregularFirstPeriod
                                    : E_sAprCalculationT.Actuarial_IgnoreIrregularFirstPeriod;

                                keys.Add("sAprCalculationT", defaultAprCalculationMethod);
                            }

                            if ((m_broker.CreateBlankFilesOnUladDataLayer || useUladDataLayer) 
                                && !keys.ContainsKey("sBorrowerApplicationCollectionT"))
                            {
                                keys.Add("sBorrowerApplicationCollectionT", EnumHelper.MaxValue<E_sLqbCollectionT>());
                            }

                            if (!keys.ContainsKey("sAlwaysRequireAllBorrowersToReceiveClosingDisclosure"))
                            {
                                keys.Add("sAlwaysRequireAllBorrowersToReceiveClosingDisclosure", this.m_broker.RequireAllBorrowersToReceiveCD);
                            }

                            sLRefNm = RetryFileCreationOnRefNmConflict(
                                loanNameToUse, 
                                sLRefNm, 
                                s => m_Core.BeginCreateNewLeadFile(branchIdToUse, loanNameToUse, s, encryptionKeyPair.Key, encryptionMigrationVersion, keys));
                        }

                        // If we got here, then we break out of the retry
                        // loop and continue because we have a valid file.

                        break;
                    }
                    catch (Exception e)
                    {
                        if (i == 5)
                        {
                            // 12/29/2004 kb - We moved the retry loop to
                            // this level...  If the counter is maxed out,
                            // then we failed for the last time.

                            Tools.LogErrorWithCriticalTracking(string.Format("After {2} retries, failed to create lead file for loginName = {0}, employeeId = {1}."
                                , m_Core.LoginName
                                , m_Core.EmployeeId
                                , i
                                ), e
                                );
                            throw;
                        }
                        else
                        {
                            // 12/29/2004 kb - One more time!  We previously
                            // looped at the core level.  We now do it here
                            // and delay between retries.

                            Tools.LogWarning(string.Format("Core create lead failure for {0} on attempt #{1}.", m_Core.LoginName, i));

                            Tools.SleepAndWakeupRandomlyWithin(1000, 4500);
                        }
                    }
                }

                try
                {
                    // ejm opm 466492 - For new files, if there is a branch id override, we don't want to assign the branch from the loan officer, so we'll suppress branch assignment.
                    BranchAssignmentOption assignmentOption = branchIdOverride.HasValue ? BranchAssignmentOption.Suppress : BranchAssignmentOption.Allow;
                    LoanAssignment(sourceUser ?? m_principal, m_Core.FileId, setInitalEmployeeRoles, assignEmployeesFromRelationships,
                        addEmployeeAsOfficialAgent, true /*setPreparerContent*/, E_UnderwritingAuthority.PriorApproved, ref createdLenderContactRecordId, assignmentOption);
                }
                catch (CBaseException e)
                {
                    throw new LoanFileCreatorException(e.UserMessage, this, e);
                }

                // Return whatever is the current state of the created
                // file.  If we failed, we should have thrown out of
                // here.

                StatusEvent.AddStatusEvent(m_Core.FileId, m_broker.BrokerID, E_sStatusT.Lead_New, DateTime.Now.ToString("g"), Tools.RetrieveUserNameForAudit((sourceUser ?? m_principal)));

                return m_Core.FileId;
            }
            catch (CBaseException e)
            {
                Tools.LogErrorWithCriticalTracking(e);

                throw;
            }
            catch (Exception e)
            {
                e = new CBaseException
                    (ErrorMessages.Generic
                    , string.Format
                    ("BeginCreateNewLeadFile( setInitalEmployeeRoles = {0} , assignEmployeesFromRelationships = {1} , sPrefix = {2} ) "
                    , setInitalEmployeeRoles
                    , assignEmployeesFromRelationships
                    , "--look in db--"
                    )
                    + e.ToString()
                    );

                Tools.LogErrorWithCriticalTracking(e);

                throw;
            }
        }

        #endregion Creation methods to retain as private

        /// <summary>
        /// Commit new loans and finalize the name.  The loan is presumed
        /// to be invalid in its current state.  Once we leave this call,
        /// the loan should be valid and available -- or we failed and
        /// all commit changes (name counter update, etc) are rolled back.
        /// If the new loan is a duplicate of another loan (not a template),
        /// pass in the loan id of the original loan.  We use this id to
        /// lookup the original's name and then use this name as a base
        /// for our new loan's name.
        /// 
        /// 9/7/2004 kb - Now, we just flip the valid bit.  Naming is
        /// performed in the begin calls.  All state changes that occur
        /// in a loan after begin() and before commit() are final.
        /// </summary>
        public void CommitFileCreation(bool refreshLoanTasks)
        {                        
            bool isQp; // OPM 236514 - Prevent task creation for QP loans.

            // 9/7/2004 kb - Commit the creator's loan by flipping the
            // valid bit to true (that's it).  Note that we removed
            // many parameters that dealt with naming

            try
            {
                bool isLead = false;
                // Update the cache entry for this loan.  This should
                // put latest and greatest in the cache.  TODO: Figure
                // out how we want to handle errors in updating the
                // cache table.  Right now, we fail silently.  We need
                // to retry a couple of times.

                Tools.UpdateCacheTable(m_Core.FileId, null);

                // Put the final touch on this loan by flipping the valid
                // bit to true.  Once valid, the file is visible to all
                // readers of the loan file tables (like the pipeline).

                try
                {
                    // 9/7/2004 kb - Call into our embedded mechanism and
                    // flip the valid bit on.  This commit should retry.
                    m_Core.CommitFileCreation();

                    // 10/13/2010 dd - Update TPO Loan Bit.
                    CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(m_Core.FileId, typeof(CLoanFileCreator));
                    dataLoan.AllowSaveWhileQP2Sandboxed = true;
                    dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                    dataLoan.RecalculateTpoValue();

                    // OPM 236514 - No Tasks for QP loans.
                    isQp = dataLoan.sLoanFileT == E_sLoanFileT.QuickPricer2Sandboxed;
                    if (!isQp)
                    {
                        DuplicateTasksToLoan();
                        EnqueueTasksDueDateUpdate();
                    }

                    isLead = Tools.IsStatusLead(dataLoan.sStatusT);

                    // 10/30/2014 AV - 195360 User's branch channel not populating when channel is not defined
                    bool allowBranchChannelUpdate = this.m_loanCreationSource !=
                        E_LoanCreationSource.LeadConvertToLoanUsingTemplate && this.m_loanCreationSource != E_LoanCreationSource.FileDuplication
                         && this.m_loanCreationSource != E_LoanCreationSource.CreateTest;   // je - Treat create test as file duplication.

                    dataLoan.UpdateLoanFileWithBranchData(allowBranchChannelUpdate);

                    // 6/13/2013 dd - For FirstTech client we use the loan number from a pool. OPM 124658.
                    if (m_Core.BrokerId == new Guid("E9F006C9-CB83-4531-8925-08FCCF4ADD63")   // First Tech Fed (PML0223)
                        && false == m_Core.IsDummyLoan && ConstAppDavid.CurrentServerLocation != ServerLocation.Beta) // opm 185732
                    {
                        // 6/13/2013 dd - Only use the reserved loan number if the branch is not TESTING BRANCH.
                        if (dataLoan.IsTemplate == false && dataLoan.sStatusT != E_sStatusT.Lead_New &&
                            this.m_loanCreationSource != E_LoanCreationSource.LeadCreate)
                        {
                            dataLoan.SetsLNmWithPermissionBypass(HardcodeIdentitySequence.GetNextAvailableIdentity(m_Core.BrokerId, "OSI"));

                            // 8/14/2013 dd - Set the new name back to core object.
                            m_Core.SetLoanName(dataLoan.sLNm);
                        }
                    }

                    dataLoan.ForceInitialDisclosureEvalOnSave = true;

                    // Need to create a contact based on the originating company information and
                    // the channel of the file. I've put this at the end of commit file creation
                    // because of how often we're modifying the branch info (as this can affect
                    // the channel).
                    AddOriginatingCompanyContact(dataLoan, this.createdLenderContactRecordId);

                    if (this.m_Core.SourceFileId == Guid.Empty &&
                        this.m_broker.DefaultBorrPaidOrigCompSourceToTotalLoanAmount)
                    {
                        dataLoan.sOriginatorCompensationBorrPaidBaseT = E_PercentBaseT.TotalLoanAmount;
                    }

                    // OPM 464704 - Set LegalEntityIdentifier. Do this last, since it's value depends on loan branch.
                    // NOTE: This also affects UniversalLoanIdentifier which is calculated form LEI and loan name.
                    dataLoan.sLegalEntityIdentifier = dataLoan.Branch.LegalEntityIdentifier;

                    Tools.CheckForUniqueUniversalLoanIdentifier(
                                this.m_broker.BrokerID,
                                this.m_Core.FileId,
                                dataLoan.sLegalEntityIdentifier,
                                this.m_Core.LoanName);


                    dataLoan.Save();
                }
                catch (CBaseException e)
                {
                    Tools.LogError(e);

                    // Preserve exception settings that were set by whoever threw this as a CBaseException,
                    // so that things like IsEmailDeveloper is preserved.
                    throw;
                }
                catch (Exception e)
                {
                    // Adding logging here since I want the details of the exception logged, but preserving existing behavior of throwing a new exception.
                    Tools.LogError(e);

                    throw new CBaseException(ErrorMessages.Generic, "Failed to commit." + e.ToString());
                }


                // 10/13/2010 dd - OPM 
                // 10/5/2004 kb - Once the cache table is updated, we
                // should run through and refresh all task objects that
                // reference this loan with whatever is latest and
                // greatest in the cache line.  If we are unable to
                // commit, then we punt on this and let stale loan
                // names and borrower names in the task rows just lie.

                try
                {
                    // Update loan names and borrower names so we get
                    // up-to-date info in the grid views of all the tasks.

                    if (refreshLoanTasks == true)
                    {
                        RefreshLoanTasks();
                    }
                }
                catch (Exception e)
                {
                    // Eat this error because we don't want to lose a decent
                    // loan because tasks didn't transfer.

                    Tools.LogErrorWithCriticalTracking(e);
                }

                try
                {
                    if (false == m_Core.IsDummyLoan)
                    {
                        Tools.CreateLoanCreationAudit(m_principal, m_Core.SourceFileId, m_Core.FileId, m_loanCreationSource, isLead);
                    }

                    try
                    {
                        // OPM 236514 - No Tasks for QP loans.
                        if (!isQp)
                        {
                            RunTriggers();
                        }
                    }
                    catch (Exception e)
                    {
                        Tools.LogErrorWithCriticalTracking(e);
                    }
                }
                catch (Exception exc)
                {
                    Tools.LogError("Error while audit loan creation.", exc);
                }
            }
            catch (Exception e)
            {
                Tools.LogErrorWithCriticalTracking(e);

                // 8/8/2016, OPM 246341 
                // To prevent a loan in a bad state from being considered valid,
                // we'll declare the loan file invalid before leaving this catch
                // block.
                Tools.SystemDeclareLoanFileInvalid(this.m_principal, this.sLId, sendNotifications: false, generateLinkLoanMsgEvent: false);

                throw;
            }
        }
    }

    public enum BranchAssignmentOption
    {
        Allow = 0,
        Suppress = 1
    }
}
