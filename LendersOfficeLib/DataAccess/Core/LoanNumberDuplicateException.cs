using System;

namespace DataAccess
{
	public class CLoanNumberDuplicateException : CBaseException
	{
        public CLoanNumberDuplicateException(Exception exc) : base(LendersOffice.Common.JsMessages.LoanInfo_DuplicateLoanNumber, exc) 
        {
            IsEmailDeveloper = false;
        }


	}
}
