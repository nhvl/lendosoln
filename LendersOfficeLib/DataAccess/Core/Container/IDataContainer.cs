﻿// <copyright file="IDataContainer.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   3/15/2016 9:58:19 AM 
// </summary>
namespace DataAccess
{
    /// <summary>
    /// Defines methods to access data.
    /// </summary>
    public interface IDataContainer
    {
        /// <summary>
        /// Gets the number of items in the data container.
        /// </summary>
        /// <value>The number of items in the container.</value>
        int Size { get; }

        /// <summary>
        /// Gets or sets the data with the given key.
        /// </summary>
        /// <value>The data with the given key.</value>
        /// <param name="key">The key to lookup.</param>
        /// <returns>The data with the specified key.</returns>
        object this[string key] { get; set; }

        /// <summary>
        /// Gets a value indicating whether the given column is present.
        /// </summary>
        /// <param name="key">The key to check for existence.</param>
        /// <returns>A value indicating whether the data with the given key exists.</returns>
        bool Contains(string key);
    }
}
