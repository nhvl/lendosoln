﻿namespace DataAccess
{
    using System.Collections.Generic;

    /// <summary>
    /// Provides an interface for a data container that keeps track of updated values.
    /// </summary>
    public interface IChangeTrackingDataContainer : IDataContainer
    {
        /// <summary>
        /// Gets a value indicating whether or not there are changes.
        /// </summary>
        bool HasChanges { get; }

        /// <summary>
        /// Gets the updated fields and values.
        /// </summary>
        /// <returns>The updated fields and values. If no updates, empty (but not null).</returns>
        IEnumerable<KeyValuePair<string, object>> GetChanges();
    }
}
