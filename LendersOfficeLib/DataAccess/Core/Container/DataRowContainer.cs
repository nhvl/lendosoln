﻿// <copyright file="DataRowContainer.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   3/15/2016 9:58:19 AM 
// </summary>
namespace DataAccess
{
    using System.Data;

    /// <summary>
    /// A container for data that is backed by a datarow.
    /// </summary>
    public class DataRowContainer : IDataContainer
    {
        /// <summary>
        /// The data row with the data.
        /// </summary>
        private DataRow row;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataRowContainer" /> class. 
        /// </summary>
        /// <param name="row">The data row with the data.</param>
        public DataRowContainer(DataRow row)
        {
            this.row = row;
        }

        /// <summary>
        /// Gets the backing data row.
        /// </summary>
        /// <value>The data row.</value>
        public DataRow DataRow
        {
            get
            {
                return this.row;
            }
        }

        /// <summary>
        /// Gets the number of items head by the container.
        /// </summary>
        /// <value>The number of items in the container.</value>
        public int Size
        {
            get
            {
                return this.row.Table.Columns.Count;
            }
        }

        /// <summary>
        /// Gets or sets the data with the given key.
        /// </summary>
        /// <value>The data with the given key.</value>
        /// <param name="key">The key to lookup.</param>
        /// <returns>The data with the specified key.</returns>
        public object this[string key]
        {
            get
            {
                return this.row[key];
            }

            set
            {
                this.row[key] = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the given column is present.
        /// </summary>
        /// <param name="key">The key to check for existance.</param>
        /// <returns>A value indicating wheter the data with the given key exists.</returns>
        public bool Contains(string key)
        {
            return this.row.Table.Columns.Contains(key);
        }
    }
}
