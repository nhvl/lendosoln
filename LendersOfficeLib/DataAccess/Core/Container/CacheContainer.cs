﻿// <copyright file="CacheContainer.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   3/15/2016 9:58:19 AM 
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// A decorator that caches calls to a IDataContainer with a dictionary for faster access.
    /// </summary>
    internal class CacheContainer : IDataContainer
    {
        /// <summary>
        /// The base container with the data.
        /// </summary>
        private IDataContainer container;

        /// <summary>
        /// The caching dictionary for faster acess.
        /// </summary>
        private IDictionary<string, object> dictionary;

        /// <summary>
        /// Initializes a new instance of the <see cref="CacheContainer" /> class. 
        /// </summary>
        /// <param name="container">The base container with the data.</param>
        internal CacheContainer(IDataContainer container)
        {
            this.container = container;
            this.dictionary = new Dictionary<string, object>(container.Size);
        }

        /// <summary>
        /// Gets the number of items in the data.
        /// </summary>
        public int Size
        {
            get
            {
                return this.container.Size;
            }
        }

        /// <summary>
        /// Gets or sets the data with the given key.
        /// </summary>
        /// <value>The data with the given key.</value>
        /// <param name="key">The key to lookup.</param>
        /// <returns>The data with the specified key.</returns>
        public object this[string key]
        {
            get
            {
                object data;

                if (!this.dictionary.TryGetValue(key, out data))
                {
                    data = this.container[key];
                    this.dictionary.Add(key, data);
                }

                return data;
            }

            set
            {
                this.dictionary[key] = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the given column is present.
        /// </summary>
        /// <param name="key">The key to check for existance.</param>
        /// <returns>A value indicating wheter the data with the given key exists.</returns>
        public bool Contains(string key)
        {
            if (this.dictionary.ContainsKey(key))
            {
                return true;
            }

            if (this.container.Contains(key))
            {
                this.dictionary[key] = this.container[key];
                return true;
            }

            return false;
        }
    }
}
