﻿// <copyright file="DictionaryContainer.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   3/15/2016 9:58:19 AM 
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;

    /// <summary>
    /// A container for data that is backed by a dictionary.
    /// </summary>
    public class DictionaryContainer : IDataContainer
    {
        /// <summary>
        /// The holding dictionary with the data.
        /// </summary>
        private IDictionary<string, object> dictionary;

        /// <summary>
        /// Initializes a new instance of the <see cref="DictionaryContainer" /> class. 
        /// </summary>
        /// <param name="dictionary">The dictionary with the data.</param>
        public DictionaryContainer(IDictionary<string, object> dictionary)
        {
            this.dictionary = dictionary;
        }

        /// <summary>
        /// Gets the number of items head by the container.
        /// </summary>
        public int Size
        {
            get
            {
                return this.dictionary.Count;
            }
        }

        /// <summary>
        /// Gets or sets the data with the given key.
        /// </summary>
        /// <value>The data with the given key.</value>
        /// <param name="key">The key to lookup.</param>
        /// <returns>The data with the specified key.</returns>
        public object this[string key]
        {
            get
            {
                object data;

                if (this.dictionary.TryGetValue(key, out data))
                {
                    return data;
                }

                throw new ArgumentException(key + " has not been loaded into the dictionary.");
            }

            set
            {
                this.dictionary[key] = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the given column is present.
        /// </summary>
        /// <param name="key">The key to check for existence.</param>
        /// <returns>A value indicating whether the data with the given key exists.</returns>
        public bool Contains(string key)
        {
            return this.dictionary.ContainsKey(key);
        }
    }
}
