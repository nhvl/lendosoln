﻿namespace DataAccess
{
    using System.Collections.Generic;

    /// <summary>
    /// A data container that can indicate which fields have been updated.
    /// </summary>
    public class ChangeTrackingDataContainer : IChangeTrackingDataContainer
    {
        /// <summary>
        /// The wrapped, base data container.
        /// </summary>
        private IDataContainer originalData;

        /// <summary>
        /// The updated fields and values.
        /// </summary>
        private IDictionary<string, object> updatedData;

        /// <summary>
        /// Initializes a new instance of the <see cref="ChangeTrackingDataContainer"/> class.
        /// </summary>
        /// <param name="originalData">The original data.</param>
        public ChangeTrackingDataContainer(IDataContainer originalData)
        {
            this.originalData = originalData;
            this.updatedData = new Dictionary<string, object>();
        }

        /// <summary>
        /// Gets the number of fields in the container.
        /// </summary>
        public int Size => this.originalData.Size;

        /// <summary>
        /// Gets a value indicating whether or not there are changes.
        /// </summary>
        public bool HasChanges
        {
            get { return this.updatedData.Count > 0; }
        }

        /// <summary>
        /// Gets or sets the value at the corresponding key.
        /// </summary>
        /// <param name="key">The key for the value.</param>
        /// <returns>The value.</returns>
        /// <exception cref="KeyNotFoundException">Thrown when the key for the get or set is not found.</exception>
        public object this[string key]
        {
            get
            {
                object updatedValue;
                if (this.updatedData.TryGetValue(key, out updatedValue))
                {
                    return updatedValue;
                }
                else if (!this.originalData.Contains(key))
                {
                    throw new KeyNotFoundException("Unable to find key: " + key);
                }

                return this.originalData[key];
            }

            set
            {
                if (!this.originalData.Contains(key))
                {
                    throw new KeyNotFoundException("Unable to find key: " + key);
                }

                bool originalValueUpdated = (this.originalData[key] == null && value != null)
                    || (this.originalData[key] != null && value == null)
                    || (this.originalData[key] != null && !this.originalData[key].Equals(value));

                bool previouslyUpdated = this.updatedData.ContainsKey(key);

                if (originalValueUpdated)
                {
                    this.updatedData[key] = value;
                }
                else if (previouslyUpdated)
                {
                    this.updatedData.Remove(key);
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether the key exists in the container.
        /// </summary>
        /// <param name="key">The key to check.</param>
        /// <returns>True if the key is found. False otherwise.</returns>
        public bool Contains(string key) => this.originalData.Contains(key);

        /// <summary>
        /// Gets the updated fields and values.
        /// </summary>
        /// <returns>The updated fields and values. If no updates, empty (but not null).</returns>
        public IEnumerable<KeyValuePair<string, object>> GetChanges()
        {
            return this.updatedData;
        }
    }
}
