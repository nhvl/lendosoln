#undef TRACE

namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.IO;
    using System.Text;
    using System.Xml;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.CustomAttributes;
    using LendersOffice.ObjLib.Security;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    public enum E_DataObjectType
	{
		LoanSharedData = 0,
		AppData = 1,
		CCTemplate = 2,
		LoanProgramTemplate = 3,
		ReportQuery = 4
	}
	abstract public class CBase
	{
        protected delegate decimal DelegateDecimalValue();
        protected delegate int DelegateIntValue();
        protected delegate decimal? DelegateNullableDecimalValue();
        protected delegate int? DelegateNullableIntValue();

        private readonly bool forcePlaintextValueforEncryptedFields = ConstStage.ForcePlaintextValueforEncryptedLoanFields;
        private EncryptionKeyIdentifier? encryptionKeyId;

        private Guid? sEncryptionKey
        {
            get { return this.encryptionKeyId?.Value; }
        }

        private EncryptionMigrationVersion? sEncryptionMigrationVersion { get; set; }

		protected		Guid								m_fileId;
		protected		string								m_namePage;
		private			CBase								m_parent;
		private			E_DataObjectType					m_objType;
        protected abstract IDataContainer GetDataRow();

        /// <summary>
        /// A value indicating whether the per-field security checks should be enforced as data
        /// points are accessed from the data container.
        /// </summary>
        /// <remarks>
        /// The field protection is enforced by <see cref="LoanFieldSecurityManager"/>, and the
        /// fields are sourced from the LOAN_FIELD_SECURITY table.
        /// </remarks>
        public bool ByPassFieldSecurityCheck = false;
        public bool ByPassFieldsBrokerLockAdjustXmlContent = false;
        public bool LeadConversionBypassForsIsManuallySetThirdPartyAffiliateProps = false;
        public bool BypassClosingCostSetValidation = false;
        public bool BypassClosingCostSetDFLPValidation = false;

        public virtual LosConvert m_convertLos
		{
			get { return m_parent.m_convertLos; }
		}

        public virtual void SetEncryptionData()
        {
            if (this.m_parent != null)
            {
                this.m_parent.SetEncryptionDataForChild(this);
            }
            else
            {
                var keyId = this.GetGuidField("sEncryptionKey");
                this.encryptionKeyId = EncryptionKeyIdentifier.Create(keyId);

                this.sEncryptionMigrationVersion = this.GetTypeIndexFieldGeneric(
                    "sEncryptionMigrationVersion",
                    EncryptionMigrationVersion.Unmigrated);
            }
        }

        private void SetEncryptionDataForChild(CBase child)
        {
            child.encryptionKeyId = this.encryptionKeyId;
            child.sEncryptionMigrationVersion = this.sEncryptionMigrationVersion;
        }

        virtual protected bool HasIntegrationType( E_IntegrationT  t )
		{
			try
			{
				if( null != m_parent )
					return m_parent.HasIntegrationType( t );
			}
			catch( Exception ex )
			{
				Tools.LogError( ex );
			}
			return false;
		}

        protected bool LogTriggerInfo { get; set; }
        protected virtual string GetTriggerLog()
        {
            return "";
        }

		private void Init( Guid fileId, string namePage, CBase parent, E_DataObjectType objType )
		{
			m_fileId =  fileId;
            m_namePage = namePage ?? "";
			m_parent = parent;
			m_objType = objType;
            LogTriggerInfo = false;
		}

		protected CBase( Guid fileId, string namePage, E_DataObjectType objType )
		{
			Init( fileId, namePage, null, objType );
		}

		protected CBase( Guid fileId, string namePage, CBase parent, E_DataObjectType objType )
		{
			Init( fileId, namePage, parent, objType );
		}

        protected string ToDateString(DateTime value)
        {
            return m_convertLos.ToDateTimeString(value).ToString();
        }

        protected string ToDateStringWithHmdaNa(CDateTime value)
        {
            return this.m_convertLos.ToDateStringWithHmdaNa(value);
        }

        protected string ToMoneyString(decimal value)
        {
            return m_convertLos.ToMoneyString(value, FormatDirection.ToRep);
        }
        protected string ToMoneyString(DelegateDecimalValue value)
        {
            try
            {
                return m_convertLos.ToMoneyString(value.Invoke(), FormatDirection.ToRep);
            }
            catch (PermissionException)
            {
                throw;
            }
            catch
            {
                return m_convertLos.InvalidMoneyString;
            }
        }
        protected string ToMoneyString4DecimalDigits(DelegateDecimalValue value)
        {
            try
            {
                return m_convertLos.ToMoneyString4DecimalDigits(value.Invoke(), FormatDirection.ToRep);
            }
            catch (PermissionException)
            {
                throw;
            }
            catch
            {
                return m_convertLos.InvalidRateString;
            }
        }
        protected string ToMoneyString6DecimalDigits(DelegateDecimalValue value)
        {
            try
            {
                return m_convertLos.ToMoneyString6DecimalDigits(value.Invoke(), FormatDirection.ToRep);
            }
            catch (PermissionException)
            {
                throw;
            }
            catch
            {
                return m_convertLos.InvalidRateString;
            }
        }

        protected string ToMoneyStringRoundAwayFromZero(DelegateDecimalValue value)
        {
            try
            {
                return m_convertLos.ToMoneyStringRoundAwayFromZero(value.Invoke(), FormatDirection.ToRep);
            }
            catch (PermissionException)
            {
                throw;
            }
        }

        protected string ToMoneyStringWithHmdaNaAndBlank(DelegateNullableDecimalValue value)
        {
            try
            {
                return this.m_convertLos.ToMoneyStringWithHmdaNaAndBlank(value.Invoke());
            }
            catch (PermissionException)
            {
                throw;
            }
            catch
            {
                return this.m_convertLos.InvalidMoneyString;
            }
        }

        protected string ToMoneyStringWithHmdaNa(DelegateNullableDecimalValue value)
        {
            try
            {
                return this.m_convertLos.ToMoneyStringWithHmdaNa(value.Invoke());
            }
            catch (PermissionException)
            {
                throw;
            }
            catch
            {
                return this.m_convertLos.InvalidMoneyString;
            }
        }

        protected decimal ToMoney(string value)
        {
            return m_convertLos.ToMoney(value);
        }

        protected Guid ToGuid(string value)
        {
            return m_convertLos.ToGuid(value);
        }

        /// <summary>
        /// Converts a decimal to a decimal string. We don't really need
        /// convertLos for this, but we want to stay consistent.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected string ToDecimalString(decimal value)
        {
            return m_convertLos.ToDecimalString(value, FormatDirection.ToRep);
        }

        protected string ToDecimalString_sSpCuScore(DelegateDecimalValue value)
        {
            return this.m_convertLos.ToDecimalString_sSpCuScore(value.Invoke());
        }

        protected string ToDecimalString(DelegateDecimalValue value)
        {
            try
            {
                return m_convertLos.ToDecimalString(value.Invoke(), FormatDirection.ToRep);
            }
            catch (PermissionException)
            {
                throw;
            }
            catch
            {
                return m_convertLos.InvalidDecimalString;
            }
        }

        protected decimal ToDecimal(string value)
        {
            return m_convertLos.ToDecimal(value);
        }

        protected string ToRateStringNoPercent(DelegateDecimalValue value)
        {
            try
            {
                return m_convertLos.ToRateStringNoPercent(value.Invoke());
            }
            catch (PermissionException)
            {
                throw;
            }
            catch
            {
                return m_convertLos.InvalidRateString;
            }
        }
        protected string ToRateString(decimal value)
        {
            return m_convertLos.ToRateString(value);
        }
        protected string ToRateString(DelegateDecimalValue value)
        {
            // 5/18/2009 dd - ValueFunctionPointer may throw exception.
            try
            {
                return m_convertLos.ToRateString(value.Invoke());
            }
            catch (PermissionException)
            {
                throw;
            }
            catch
            {
                return m_convertLos.InvalidRateString;
            }
        }
        protected string ToRateString4DecimalDigits(DelegateDecimalValue value)
        {
            try
            {
                return m_convertLos.ToRateString4DecimalDigits(value.Invoke());
            }
            catch (PermissionException)
            {
                throw;
            }
            catch
            {
                return m_convertLos.InvalidRateString;
            }
        }
        protected string ToRateString6DecimalDigits(DelegateDecimalValue value)
        {
            try
            {
                return m_convertLos.ToRateString6DecimalDigits(value.Invoke());
            }
            catch (PermissionException)
            {
                throw;
            }
            catch
            {
                return m_convertLos.InvalidRateString;
            }
        }
        protected string ToRateString9DecimalDigits(DelegateDecimalValue value)
        {
            try
            {
                return m_convertLos.ToRateString9DecimalDigits(value.Invoke());
            }
            catch (PermissionException)
            {
                throw;
            }
            catch
            {
                return m_convertLos.InvalidRateString;
            }
        }

        protected string ToRateStringWithHmdaNa(DelegateNullableDecimalValue value)
        {
            try
            {
                return this.m_convertLos.ToRateStringWithHmdaNa(value.Invoke());
            }
            catch (PermissionException)
            {
                throw;
            }
            catch
            {
                return this.m_convertLos.InvalidRateString;
            }
        }

        /// <summary>
        /// For percentages
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected decimal ToRate(string value) 
        {
            return m_convertLos.ToRate(value);
        }

        protected string ToPropsString(DelegateIntValue value)
        {
            try
            {
                return value.Invoke().ToString();
            }
            catch (PermissionException)
            {
                throw;
            }
        }
        protected int ToProps(string value)
        {
            int ret = 0;

            if (int.TryParse(value, out ret) == false)
            {
                ret = 0;
            }
            return ret;
        }
        protected string ToCountString(int value)
        {
            return m_convertLos.ToCountString(value);
        }
        protected string ToCountString(DelegateIntValue value)
        {
            try
            {
                return m_convertLos.ToCountString(value.Invoke());
            }
            catch (PermissionException)
            {
                throw;
            }
            catch
            {
                return m_convertLos.InvalidCountString;
            }
        }
        protected string ToCountStringCreditScore(int value, bool isRunningPricingEngine)
        {
            try
            {
                return m_convertLos.ToCountStringCreditScore(value, isRunningPricingEngine);
            }
            catch (PermissionException)
            {
                throw;
            }
            catch
            {
                return m_convertLos.InvalidCountString;
            }
        }
        protected string ToCountStringWithHmdaNaAndBlank(DelegateNullableIntValue value, bool preserveNotApplicableCode = false)
        {
            try
            {
                return this.m_convertLos.ToCountStringWithHmdaNaAndBlank(value.Invoke(), preserveNotApplicableCode);
            }
            catch (PermissionException)
            {
                throw;
            }
            catch
            {
                return this.m_convertLos.InvalidCountString;
            }
        }

        protected string ToCountStringWithHmdaNa(DelegateNullableIntValue value, bool preserveNotApplicableCode = false)
        {
            try
            {
                return this.m_convertLos.ToCountStringWithHmdaNa(value.Invoke(), preserveNotApplicableCode);
            }
            catch (PermissionException)
            {
                throw;
            }
            catch
            {
                return this.m_convertLos.InvalidCountString;
            }
        }

        protected string ToCountStringWithHmdaNa(DelegateNullableDecimalValue value, bool preserveNotApplicableCode = false)
        {
            try
            {
                int? intValue = null;
                decimal? decValue = value.Invoke();
                if (decValue.HasValue)
                {
                    intValue = decimal.ToInt32(decValue.Value);
                }

                return this.m_convertLos.ToCountStringWithHmdaNa(intValue, preserveNotApplicableCode);
            }
            catch (PermissionException)
            {
                throw;
            }
            catch
            {
                return this.m_convertLos.InvalidCountString;
            }
        }

        protected int ToCount(string value)
        {
            return m_convertLos.ToCount(value);
        }

        protected int? ToCountWithHmdaNa(string value)
        {
            return this.m_convertLos.ToCountWithHmdaNa(value);
        }

        protected CDateTime ToDateWithHmdaNa(string value)
        {
            return this.m_convertLos.ToDateWithHmdaNa(value);
        }

        protected virtual LargeFieldStorage LargeFieldStorer
		{
			get // Class without parent must override this method.
			{
				return m_parent.LargeFieldStorer; 
			}
		}
        protected object ReadFromDataRow(string fieldName)
        {
            return ReadFromDataRow(fieldName, false);
        }
        private object ReadFromDataRow(string fieldName, bool bIsAccessForWrite)
        {
            try
            {
                if (!ByPassFieldSecurityCheck)
                {
                    if (fieldName == "sBrokerLockAdjustXmlContent" && ByPassFieldsBrokerLockAdjustXmlContent)
                    {
                        // Don't do anything. Skips check for this field only.
                    }
                    else
                    {
                        //If a secure field, check for permission : OPM 46433
                        if (bIsAccessForWrite)
                        {
                            LoanFieldSecurityManager.CheckFieldWritePermission(fieldName);
                        }
                        else
                        {
                            LoanFieldSecurityManager.CheckFieldReadPermission(fieldName);
                        }
                    }
                }



                try
                {
                    return GetDataRow()[fieldName];
                }
                catch (ArgumentException)
                {
                    Tools.LogErrorWithCriticalTracking("The field [" + fieldName + "] was not loaded, ensure its in the dependency list.");
                    if (LogTriggerInfo)
                    {
                        string trilog = GetTriggerLog();
                        if (!string.IsNullOrEmpty(trilog))
                        {
                            Tools.LogError(trilog);
                        }
                    }
                    throw;
                }
                catch (NullReferenceException)
                {
                    Tools.LogError("Did you forget to call initSave or InitLoad?");
                    throw;
                }
            }
            catch (PermissionException ex)
            {
                LogErrorWithStackTrace(ex.DeveloperMessage);
                throw;
            }
            catch
            {
                LogMissingField(fieldName);
                throw;
            }
        }

		private void LogMissingField( string strFieldNm )
		{
            LogErrorWithStackTrace("Fails to read " + strFieldNm);
		}        
		private bool m_bIsLoanNameChanged = false;

		protected bool IsLoanNameChanged
		{
			get { return m_bIsLoanNameChanged; }
			set { m_bIsLoanNameChanged = value; }
		}

        #region Logging Methods
        protected void LogErrorWithStackTrace(string sMsg)
        {
            System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(); string s = "";

            for (int i = 0; i < st.FrameCount; ++i)
            {
                System.Diagnostics.StackFrame sf = st.GetFrame(i);

                if (s != "")
                {
                    s += " <- ";
                }

                s += sf.GetMethod().Name;
            }
            LogError(sMsg + ", call stack: " + s);
        }
        protected void LogError( string msg )
		{
			Tools.LogError( m_namePage + ": " + msg + " In file id:" + m_fileId.ToString() );
		}
		protected void LogError( string msg, Exception exc )
		{
			Tools.LogError( m_namePage + ": " + msg + " In file id:" + m_fileId.ToString(), exc );
		}
		protected void LogErrorAndSendMail( string msg, Exception exc )
		{
			Tools.LogErrorWithCriticalTracking( m_namePage + ": " + msg + " In file id:" + m_fileId.ToString(), exc );
		}

		protected void LogErrorException( Exception ex )
		{
			if( ex is System.Data.SqlClient.SqlException )
			{
				SqlException e = ex as System.Data.SqlClient.SqlException;
				StringBuilder errorMessages = new StringBuilder( 200 );

				for (int i=0; i < e.Errors.Count; i++)
				{
					errorMessages.Append ( "Index #" + i + "\n" +
						"Message: " + e.Errors[i].Message + "\n" +
						"LineNumber: " + e.Errors[i].LineNumber + "\n" +
						"Source: " + e.Errors[i].Source + "\n" +
						"Procedure: " + e.Errors[i].Procedure + "\n" );
				}
                LogError(errorMessages.ToString());
			}
			else
			{
                LogError(ex.Message);
			}

		}
		protected void LogInfo( string msg )
		{
			Tools.LogInfo( m_namePage + ": " + msg + " In file id:" + m_fileId.ToString() );
		}

        protected void LogWarning(string msg)
        {
            Tools.LogWarning(m_namePage + ": " + msg + " In file id:" + m_fileId.ToString());
        }
        protected void LogWarning(string msg, Exception exc)
        {
            Tools.LogWarning(m_namePage + ": " + msg + " In file id:" + m_fileId.ToString(), exc);
        }

		protected void LogBug( string msg )
		{
			Tools.LogBug( m_namePage + ": " + msg + " In file id:" + m_fileId.ToString() );
        }
        #endregion

        /// <summary>
        /// This method rounds each argument to 2 decimal digits and then sum of the total.
        /// This will prevent problem of you try to add: 12.344 + 12.304 = 24.648. Since we only display
        /// 2 decimal digits for money, user will see $12.34 + $12.30 = $24.65 (if we add and then round).
        /// So in order for user to see $12.34 + $12.30 = $24.64 we must round 12.344, 12.304 first and then sum up the total.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        protected decimal SumMoney(params decimal[] args)
        {
            return Tools.SumMoney(args);
        }
        protected decimal SumMoney(IEnumerable<decimal> args)
        {
            return Tools.SumMoney(args);
        }
        protected decimal SumMoney(MidpointRounding roundingMode, params decimal[] args)
        {
            return Tools.SumMoney(args, roundingMode);
        }
        protected decimal SumMoney(IEnumerable<decimal> args, MidpointRounding roundingMode)
        {
            return Tools.SumMoney(args, roundingMode);
        }
        #region Get/Set Field From Database Methods
        protected string GetDateAndTime_rep(string strFieldNm, string defaultValue)
        {
            object value = ReadFromDataRow(strFieldNm);
            // 3/30/2010 dd - For MismoClosing format, we change defaultValue to empty string.
            if (m_convertLos.FormatTargetCurrent == FormatTarget.MismoClosing)
            {
                defaultValue = string.Empty;
            }
            try
            {
                if (value == DBNull.Value)
                {
                    return defaultValue;
                }

                DateTime dt = Convert.ToDateTime(value);
                if (dt == DateTime.MinValue || dt == DateTime.MaxValue)
                {
                    return defaultValue;
                }

                switch (m_convertLos.FormatTargetCurrent)
                {
                    case FormatTarget.Webform:
                    case FormatTarget.PrintoutNormalFields:
                    case FormatTarget.PrintoutImportantFields:
                    case FormatTarget.FannieMaeMornetPlus:
                    case FormatTarget.PointNative:
                    case FormatTarget.PriceEngineEval:
                    case FormatTarget.ReportResult:
                    case FormatTarget.FreddieMacAUS:
                    case FormatTarget.LON_Mismo:
                    case FormatTarget.DocMagic:
                    case FormatTarget.FannieMaeMornetPlusImportantFields:
                        return dt.ToString("MM/dd/yyyy hh:mm tt");
                    case FormatTarget.MismoClosing:
                        return dt.ToString("yyyy-MM-ddTHH:mm");
                    case FormatTarget.XsltExport:
                        return dt.ToString("MM/dd/yyyy");
                    default:
                        throw new UnhandledEnumException(m_convertLos.FormatTargetCurrent);
                }

            }
            catch (UnhandledEnumException)
            {
                throw;
            }
            catch
            {
                return defaultValue;
            }
        }
        protected CDateTime GetDateTimeField( string strFieldNm )
		{
            object value = ReadFromDataRow(strFieldNm);
            try
            {
                if( value is System.DBNull )
                    return CDateTime.InvalidWrapValue;

                return CDateTime.Create(Convert.ToDateTime(value));
            }
            catch
            {
                return CDateTime.InvalidWrapValue;
            }

		}
        protected string GetDateTimeField_rep(string strFieldNm)
        {
            return GetDateTimeField_rep(strFieldNm, false);
        }
        protected string GetDateTimeField_rep(DateTime dt, bool displayTime)
        {
         
            var defaultReturnValue = string.Empty;
            try
            {
                if (DateTime.MinValue == dt)
                {
                    return defaultReturnValue;
                }
                return m_convertLos.ToDateTimeString(dt, displayTime);
            }
            catch
            {
                return defaultReturnValue;
            }
        }

        /// <summary>
        /// Takes a DateTimeOffset structure and returns its string representation
        /// </summary>
        /// <param name="dto">The DateTimeOffset stucture.</param>
        /// <param name="displayTime">Boolean indicating whether to display time in string representation.</param>
        /// <returns>String representation of DateTimeOffset structure.</returns>
        protected string GetDateTimeField_rep(DateTimeOffset dto, bool displayTime)
        {

            var defaultReturnValue = string.Empty;
            try
            {
                if (DateTimeOffset.MinValue == dto)
                {
                    return defaultReturnValue;
                }
                return m_convertLos.ToDateTimeString(dto, displayTime);
            }
            catch
            {
                return defaultReturnValue;
            }
        }
        protected string GetDateTimeField_rep(string strFieldNm, bool displayTime)
        {
            object value = ReadFromDataRow(strFieldNm);
            try
            {
                if (value == DBNull.Value)
                    return string.Empty;

                DateTime dt = Convert.ToDateTime(value);

                return m_convertLos.ToDateTimeString(dt, displayTime);
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Get the time component of the given datetime field.
        /// </summary>
        /// <param name="strFieldNm">The field name of the datetime field.</param>
        /// <returns>The time component of the given datetime as a string.</returns>
        protected string GetDateTimeField_TimeOnly_rep(string strFieldNm)
        {
            return this.GetDateTimeField_TimeOnly_rep(strFieldNm, true);
        }

        /// <summary>
        /// Get the time component of the given datetime field.
        /// </summary>
        /// <param name="strFieldNm">The field name of the datetime field.</param>
        /// <param name="assumeMidnightIsKnown">True if it should be assumed that 00:00:00 is actually midnight. False will result in an empty return value for 00:00:00.</param>
        /// <returns>The time component of the given datetime as a string.</returns>
        protected string GetDateTimeField_TimeOnly_rep(string strFieldNm, bool assumeMidnightIsKnown)
        {
            object value = ReadFromDataRow(strFieldNm);
            try
            {
                if (value == DBNull.Value)
                    return string.Empty;

                DateTime dt = Convert.ToDateTime(value);

                if (!assumeMidnightIsKnown && dt.TimeOfDay.Equals(TimeSpan.Zero))
                {
                    return string.Empty;
                }

                return m_convertLos.ToDateTimeString_TimeOnly(dt);
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// This would return a higher precision (in seconds) then via the method returning the CDateTime type
        /// </summary>
        /// <param name="strFieldNm"></param>
        /// <param name="fallbackDate"></param>
        /// <returns></returns>
        protected DateTime GetDateTimeField(string strFieldNm, DateTime fallbackDate)
        {
            object value = ReadFromDataRow(strFieldNm);

            if (value is DBNull)
            {
                return fallbackDate;
            }
            try
            {
                return Convert.ToDateTime(value);
            }
            catch
            {
                return fallbackDate;
            }
        }
        /// <summary>
        /// This would allow saving a more accurate datetime value (in seconds) than the one accepting CDateTime
        /// </summary>
        /// <param name="strFieldNm"></param>
        /// <param name="newDateTime">Passing DateTime.MinValue to save an invalid date</param>
        /// <param name="format"></param>
        protected void SetDateTimeField(string strFieldNm, DateTime newDateTime)
        {
            object value = ReadFromDataRow(strFieldNm, true);
            DateTime oldDt = DateTime.MinValue;
            try
            {
                if (DBNull.Value != value)
                    oldDt = Convert.ToDateTime(value);
            }
            catch
            { }

            if (IsRequireFieldSet(strFieldNm, oldDt, newDateTime))
            {
                string oldDStr = string.Format("{0} {1}", oldDt.ToShortDateString(), oldDt.ToShortTimeString());
                string newDStr = string.Format("{0} {1}", newDateTime.ToShortDateString(), oldDt.ToShortTimeString());

                OnFieldChange(strFieldNm, SqlDbType.DateTime, oldDStr, newDStr);
                IDataContainer row = GetDataRow();
                if (newDateTime == DateTime.MinValue)
                {
                    row[strFieldNm] = DBNull.Value;
                }
                else
                {
                    row[strFieldNm] = newDateTime;
                }
            }
        }

		protected void SetDateTimeField( string strFieldNm, CDateTime newDateTime )
		{
			// TODO: the rep version will convert to CDateTime before saving. 
			// SetDateTimeField_rep should call this instead.
            SetDateTimeField_rep(strFieldNm, newDateTime.ToStringWithTime("M/dd/yyyy HH:mm:ss"));
		}

		protected virtual void SetDateTimeField_rep( string strFieldNm, string newStrVal )
		{
            object value = ReadFromDataRow(strFieldNm, true);

            string newVal = newStrVal;
            if (null == newVal)
            {
                newVal = "";
            }

            newVal = newVal.TrimWhitespaceAndBOM();
            CDateTime newDt = CDateTime.Create(newVal, m_convertLos);

            if ((DBNull.Value == value) && (newVal.Length == 0))
                return; // Same


            CDateTime oldDt = null;
            try
            {
                if (DBNull.Value != value)
                    oldDt = CDateTime.Create((DateTime)value);
            }
            catch
            { }
            if (null == oldDt)
                oldDt = CDateTime.InvalidWrapValue;

            if (IsRequireFieldSet(strFieldNm, oldDt, newDt))
            {
                if (string.IsNullOrEmpty(newVal) == false)
                {
                    CFieldDbInfo fieldInfo = CFieldDbInfoList.TheOnlyInstance.Get(strFieldNm);
                    // 8/18/2010 dd - OPM 55789 - If fieldInfo is empty then assume the datatype is SmallDateTime.

                    if (fieldInfo != null && fieldInfo.m_dbType == SqlDbType.DateTime && newDt.IsValid == false)
                    {
                        throw new FieldInvalidValueException(strFieldNm, newStrVal);

                    }
                    else if (newDt.IsValidSmallDateTime == false && (fieldInfo == null || fieldInfo.m_dbType == SqlDbType.SmallDateTime))
                    {
                        throw new FieldInvalidValueException(strFieldNm, newStrVal);
                    }
                }

                OnFieldChange(strFieldNm, SqlDbType.DateTime, oldDt.ToString(m_convertLos), newDt.ToString(m_convertLos));
                IDataContainer row = GetDataRow();

                row[strFieldNm] = newDt.DateTimeForDBWriting;
            }
		}

        /// <summary>
        /// If the field represented by strFieldNm has changed, then add the current time to that field.
        /// If the new value already has a time component, it is discarded in favor of the current time.
        /// </summary>
        protected virtual void SetDateTimeFieldWithCurrentTime_rep(string strFieldNm, string newStrVal)
        {
            object oldValue = ReadFromDataRow(strFieldNm, true);
            CDateTime oldCDt_withTime = null;
            CDateTime oldCDt_withoutTime = null;
            try
            {
                if (DBNull.Value != oldValue)
                {
                    DateTime oldDt_withTime = (DateTime)oldValue;
                    oldCDt_withTime = CDateTime.Create(oldDt_withTime);

                    // We want to be able to ignore hours/minutes for now
                    DateTime oldDt_withoutTime = new DateTime(oldDt_withTime.Year, oldDt_withTime.Month, oldDt_withTime.Day);
                    oldCDt_withoutTime = CDateTime.Create(oldDt_withoutTime);
                }
            }
            catch
            { }

            if (null == oldCDt_withTime)
                oldCDt_withTime = CDateTime.InvalidWrapValue;

            if (null == oldCDt_withoutTime)
                oldCDt_withoutTime = CDateTime.InvalidWrapValue;

            string newVal = newStrVal;
            if (null == newVal)
            {
                newVal = "";
            }
            newVal = newVal.TrimWhitespaceAndBOM();

            // First, check if they're both null / empty
            if ((DBNull.Value == oldValue) && (newVal.Length == 0))
                return; // Same

            // Convert to a CDateTime
            CDateTime newCDt = CDateTime.Create(newVal, m_convertLos);

            // See if we can append a time to this thing
            string newVal_withTime = string.Empty;
            if (newCDt.IsValidSmallDateTime)
            {
                // 5/30/2014 gf - If there is already a time appended to the end,
                // we want to remove it becuase we are going to append the current
                // time. We've enforced that the value isn't null, so this is safe.
                // LoanEditor new UI uses ISO 8601 date string format: 
                // date string may contains 'T': YYYY-MM-DDThh:mm:ss.sTZD
                // newVal_withTime = newVal.Split(new[] { 'T', ' ' })[0] + " " + DateTime.Now.ToShortTimeString();
                newVal_withTime = newCDt.DateTimeForComputation.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
                
            }
            CDateTime newCDt_withTime = CDateTime.Create(newVal_withTime, m_convertLos);

            // Check if the old value is different from the new one
            if (IsRequireFieldSet(strFieldNm, oldCDt_withoutTime, newCDt))
            {
                if (string.IsNullOrEmpty(newVal) == false)
                {
                    CFieldDbInfo fieldInfo = CFieldDbInfoList.TheOnlyInstance.Get(strFieldNm);
                    // 8/18/2010 dd - OPM 55789 - If fieldInfo is empty then assume the datatype is SmallDateTime.

                    if (fieldInfo != null && fieldInfo.m_dbType == SqlDbType.DateTime && newCDt.IsValid == false)
                    {
                        throw new FieldInvalidValueException(strFieldNm, newStrVal);

                    }
                    else if (newCDt.IsValidSmallDateTime == false && (fieldInfo == null || fieldInfo.m_dbType == SqlDbType.SmallDateTime))
                    {
                        throw new FieldInvalidValueException(strFieldNm, newStrVal);
                    }
                }

                OnFieldChange(strFieldNm, SqlDbType.DateTime, oldCDt_withTime.ToString(m_convertLos), newCDt_withTime.ToString(m_convertLos));
                IDataContainer row = GetDataRow();

                row[strFieldNm] = newCDt_withTime.DateTimeForDBWriting;
            }
        }

        private Dictionary<Guid, string> fileDBCache = new Dictionary<Guid, string>();

        public virtual string GetFileDBField(string strFieldNm)
        {
            return LargeFieldStorer.GetTextCacheCheck(GetGuidField(strFieldNm), fileDBCache);
        }
        public virtual void SetFileDBField(string strFieldNm, string newStrVal)
        {
            string newVal = newStrVal;
            if (null == newVal)
            {
                newVal = "";
            }

            Guid key = GetGuidField(strFieldNm, true);

            bool bNeedKey = (0 == key.ToString().CompareTo(Tools.EmptyGuid));

            newVal = newVal.TrimWhitespaceAndBOM();

            if (bNeedKey || (0 != GetFileDBField(strFieldNm).CompareTo(newVal)))
            {
                if (bNeedKey)
                {
                    key = Guid.NewGuid();
                    OnFieldChange(strFieldNm, SqlDbType.UniqueIdentifier, null, newVal);
                    GetDataRow()[strFieldNm] = key;
                }

                OnFileDbFieldChange(strFieldNm, key, newVal);

                // Update the fileDBCache with the new values.
                if (fileDBCache.ContainsKey(key))
                {
                    fileDBCache[key] = newVal;
                }
                else
                {
                    fileDBCache.Add(key, newVal);
                }
            }
        }

        //NOTE: Throws if DB value is null.
		protected bool GetBoolField( string strFieldNm )
		{
            return (bool)ReadFromDataRow(strFieldNm);
		}

        protected bool GetBoolField(string strFieldNm, bool defaultValue)
        {
            object data = ReadFromDataRow(strFieldNm);
            if (data == DBNull.Value)
            {
                return defaultValue;
            }

            return (bool)data;
        }

        protected bool? GetNullableBoolField(string strFieldName)
        {
            object data = ReadFromDataRow(strFieldName);
            if (data == DBNull.Value)
            {
                return null;
            }

            return (bool)data;
        }

        protected virtual void SetBoolField( string strFieldNm, bool newVal )
        {
            object value = ReadFromDataRow(strFieldNm, true);
            bool oldVal = Convert.ToBoolean(value);
            if (IsRequireFieldSet(strFieldNm, oldVal, newVal))
            {
                OnFieldChange(strFieldNm, SqlDbType.Bit, oldVal, newVal);
                IDataContainer row = GetDataRow();
                row[strFieldNm] = newVal;
            }

        }

        protected void SetNullableBoolField(string strFieldNm, bool? newVal)
        {
            object value = ReadFromDataRow(strFieldNm, true);
            bool? oldVal = null;
            if (value != DBNull.Value)
            {
                oldVal = Convert.ToBoolean(value);
            }

            if (IsRequireFieldSet(strFieldNm, oldVal, newVal))
            {
                OnFieldChange(strFieldNm, SqlDbType.Bit, oldVal, newVal);
                IDataContainer row = GetDataRow();
                if (newVal.HasValue)
                    row[strFieldNm] = newVal;
                else
                    row[strFieldNm] = DBNull.Value;
            }
        }

        protected E_TriState GetTriStateField(string strFieldNm)
        {
            object value = ReadFromDataRow(strFieldNm);
            return (E_TriState)Convert.ToByte(value);
        }
        protected E_TriState GetTriStateField(string strFieldNm, E_TriState defaultValue)
        {
            object value = ReadFromDataRow(strFieldNm);
            if (value == DBNull.Value)
            {
                return defaultValue;
            }

            return (E_TriState)Convert.ToByte(value);
        }
        protected virtual void SetTriStateField(string strFieldNm, E_TriState newVal)
        {
            object value = ReadFromDataRow(strFieldNm, true);
            E_TriState oldVal = (E_TriState)Convert.ToByte(value);
            if (IsRequireFieldSet(strFieldNm, oldVal, newVal))
            {
                OnFieldChange(strFieldNm, SqlDbType.TinyInt, oldVal, newVal);
                IDataContainer row = GetDataRow();
                row[strFieldNm] = newVal;
            }
        }

        protected void SetNullableTriStateField(string strFieldNm, E_TriState newVal)
        {
            object value = ReadFromDataRow(strFieldNm, true);
            E_TriState? oldVal = null;
            if (value != DBNull.Value)
            {
                oldVal = (E_TriState)Convert.ToByte(value);
            }

            if (IsRequireFieldSet(strFieldNm, oldVal, newVal))
            {
                OnFieldChange(strFieldNm, SqlDbType.TinyInt, oldVal, newVal);
                IDataContainer row = GetDataRow();
                row[strFieldNm] = Convert.ToByte(newVal);
            }
        }

        protected int GetCountField(string strFieldNm)
        {
            return (int)ReadFromDataRow(strFieldNm);
        }
        protected int GetCountField(string strFieldNm, int defaultValue)
        {
            object data = ReadFromDataRow(strFieldNm);
            if (data == DBNull.Value)
            {
                return defaultValue;
            }

            return (int)data;
        }
        protected int GetSmallCountField(string strFieldNm, int defaultValue)
        {
            object data = ReadFromDataRow(strFieldNm);
            if (data == DBNull.Value)
            {
                return defaultValue;
            }

            return Convert.ToInt32(data);
        }
        protected int? GetNullableSmallCountField(string strFieldNm)
        {
            object data = ReadFromDataRow(strFieldNm);
            if (data == DBNull.Value)
            {
                return null;
            }

            return Convert.ToInt32(data);
        }
        protected int? GetNullableCountField(string strFieldName)
        {
            object data = ReadFromDataRow(strFieldName);
            if (data == DBNull.Value)
            {
                return null;
            }

            return (int)data;
        }

        protected void SetCountField(string strFieldNm, int newVal)
        {
            object value = ReadFromDataRow(strFieldNm, true);
            int oldVal = Convert.ToInt32(value);
            if (IsRequireFieldSet(strFieldNm, oldVal, newVal))
            {
                OnFieldChange(strFieldNm, SqlDbType.Int, oldVal, newVal);
                IDataContainer row = GetDataRow();
                row[strFieldNm] = newVal;
            }
        }

        protected void SetNullableCountField(string strFieldNm, int? newVal)
        {
            object value = ReadFromDataRow(strFieldNm, true);
            int? oldVal = null;
            if (value != DBNull.Value)
            {
                oldVal = Convert.ToInt32(value);
        }
            if (IsRequireFieldSet(strFieldNm, oldVal, newVal))
            {
                OnFieldChange(strFieldNm, SqlDbType.Int, oldVal, newVal);
                IDataContainer row = GetDataRow();
                if (newVal.HasValue)
                {
                    row[strFieldNm] = newVal;
                }
                else
                {
                    row[strFieldNm] = DBNull.Value;
                }
            }
        }

        protected void SetNullableSmallCountField(string strFieldNm, int? newVal)
        {
            if (newVal.HasValue && (newVal.Value > Int16.MaxValue || newVal.Value < Int16.MinValue))
            {
                throw new CBaseException("Error trying to set value outside of Int16 to a SqlInt16 in the DB", "Error trying to set value outside of Int16 to a SqlInt16 in the DB");
            }
            else
            {
                SetNullableCountField(strFieldNm, newVal);
            }
        }

        protected long GetBigCountField(string strFieldNm)
        {
            return (long)ReadFromDataRow(strFieldNm);
        }
        protected void SetBigCountField(string strFieldNm, long newVal)
        {
            object value = ReadFromDataRow(strFieldNm, true);
            long oldVal = Convert.ToInt64(value);
            if (IsRequireFieldSet(strFieldNm, oldVal, newVal))
            {
                OnFieldChange(strFieldNm, SqlDbType.BigInt, oldVal, newVal);
                IDataContainer row = GetDataRow();
                row[strFieldNm] = newVal;
            }

        }

        protected long? GetNullableBigCountField(string strFieldNm)
        {
            var result = ReadFromDataRow(strFieldNm);
            return (result == DBNull.Value) ? null : (long?)result;
        }

        protected string GetNullableBigCountField_rep(string strFieldNm)
        {
            long? value = GetNullableBigCountField(strFieldNm);
            return m_convertLos.ToNullableBigCountString(value);
        }

        protected void SetNullableBigCountField(string strFieldNm, long? newVal)
        {
            object value = ReadFromDataRow(strFieldNm, true);
            long? oldVal = null;
            if (value != DBNull.Value)
            {
                try
                {
                    oldVal = Convert.ToInt64(value);
                }
                catch (InvalidCastException) { }
            }

            if (IsRequireFieldSet(strFieldNm, oldVal, newVal))
            {
                OnFieldChange(strFieldNm, SqlDbType.BigInt, oldVal, newVal);
                IDataContainer row = GetDataRow();
                if (newVal.HasValue) row[strFieldNm] = newVal;
                else row[strFieldNm] = DBNull.Value;

            }
        }

        protected int Get32BitPropsField(string strFieldNm)
        {
            return (int)ReadFromDataRow(strFieldNm);
        }
        protected void Set32BitPropsField(string strFieldNm, int newVal)
        {
            object value = ReadFromDataRow(strFieldNm, true);
            int oldVal = Convert.ToInt32(value);
            if (IsRequireFieldSet(strFieldNm, oldVal, newVal))
            {
                OnFieldChange(strFieldNm, SqlDbType.Int, oldVal, newVal);
                IDataContainer row = GetDataRow();
                row[strFieldNm] = newVal;
            }
        }

        protected int GetTypeIndexField(string strFieldNm)
        {
            return (int)ReadFromDataRow(strFieldNm);
        }

        protected T GetTypeIndexField<T>(string strFieldNm)
        {
            return (T)ReadFromDataRow(strFieldNm);
        }

        protected T GetTypeIndexField<T>(string strFieldNm, T defaultValue)
        {
            object data = ReadFromDataRow(strFieldNm);
            if (data == DBNull.Value)
            {
                return defaultValue;
            }

            return (T)data;
        }

        protected T GetTypeIndexFieldGeneric<T>(string strFieldNm, T defaultValue)
        {
            object data = ReadFromDataRow(strFieldNm);
            if (data == DBNull.Value)
            {
                return defaultValue;
            }

            return (T)Enum.ToObject(typeof(T), data);
        }

        protected void SetTypeIndexField<TEnum>(string strFieldNm, TEnum newVal)
        {
            this.SetTypeIndexField(strFieldNm, (int)(object)newVal);
        }

        protected void SetTypeIndexField(string strFieldNm, int newVal)
        {
            object value = ReadFromDataRow(strFieldNm, true);

            int? oldVal = null;
            if (value != DBNull.Value)
            {
                oldVal = Convert.ToInt32(value);
            }
                
            if (IsRequireFieldSet(strFieldNm, oldVal, newVal))
            {
                IDataContainer row = GetDataRow();

                OnFieldChange(strFieldNm, SqlDbType.Int, oldVal, newVal);
                row[strFieldNm] = newVal;
            }
        }

        /// <summary>
        /// Used for getting rate values, percentages, and points.
        /// </summary>
        /// <param name="strFieldNm"></param>
        /// <returns></returns>
        protected decimal GetRateField(string strFieldNm)
        {
            object value = ReadFromDataRow(strFieldNm);
            return Convert.ToDecimal(value);
        }
        protected decimal GetRateField(string strFieldNm, decimal defaultValue)
        {
            object data = ReadFromDataRow(strFieldNm);
            if (data == DBNull.Value)
            {
                return defaultValue;
            }

            return Convert.ToDecimal(data);
        }
        protected void SetRateField(string strFieldNm, decimal newVal)
        {
            object value = ReadFromDataRow(strFieldNm, true);
            decimal oldVal = Convert.ToDecimal(value);
            if (IsRequireFieldSet(strFieldNm, oldVal, newVal))
            {
                IDataContainer row = GetDataRow();

                OnFieldChange(strFieldNm, SqlDbType.Decimal, oldVal, newVal);
                row[strFieldNm] = newVal;
            }
        }
        protected void SetNullableRateField(string strFieldNm, decimal? newVal)
        {
            object value = ReadFromDataRow(strFieldNm, true);
            decimal? oldVal = null;
            if (value != DBNull.Value)
            {
                oldVal = Convert.ToDecimal(value);
            }
            if (IsRequireFieldSet(strFieldNm, oldVal, newVal))
            {
                OnFieldChange(strFieldNm, SqlDbType.Decimal, oldVal, newVal);
                IDataContainer row = GetDataRow();
                if (newVal.HasValue)
                {
                    row[strFieldNm] = newVal;
                }
                else
                {
                    row[strFieldNm] = DBNull.Value;
                }
            }
        }
    
        /// <summary>
        /// Force it to be a non-negative decimal.
        /// </summary>
        /// <param name="strFieldNm"></param>
        /// <param name="newVal"></param>
        protected void SetPositiveRateField(string strFieldNm, decimal newVal)
        {
            if (newVal < 0)
            {
                newVal = 0.0M;
            }

            SetRateField(strFieldNm, newVal);
        }
        protected decimal GetRate2DecimalDigitsField(string strFieldNm)
        {
            object value = ReadFromDataRow(strFieldNm);
            return Convert.ToDecimal(value);
        }
        protected void SetRate2DecimalDigitsField(string strFieldNm, decimal newVal)
        {
            object value = ReadFromDataRow(strFieldNm, true);
            decimal oldVal = Convert.ToDecimal(value);
            if (IsRequireFieldSet(strFieldNm, oldVal, newVal))
            {
                OnFieldChange(strFieldNm, SqlDbType.Decimal, oldVal, newVal);
                IDataContainer row = GetDataRow();
                row[strFieldNm] = newVal;
            }
        }

        protected decimal GetMoneyField(string strFieldNm)
        {
            return GetMoneyField(strFieldNm, 2);
        }
        protected decimal GetMoneyField(string strFieldNm, decimal defaultValue)
        {
            return GetMoneyField(strFieldNm, 2, defaultValue);
        }
        protected decimal GetMoneyField(string strFieldNm, int decimalDigits, decimal defaultValue)
        {
            object data = ReadFromDataRow(strFieldNm);
            if (data == DBNull.Value)
            {
                return defaultValue;
            }

            return Math.Round((decimal)data, decimalDigits);
        }
        protected decimal GetMoneyField(string strFieldNm, int decimalDigits)
        {
            object value = ReadFromDataRow(strFieldNm);
            decimal rawResult = (decimal)value;
            return Math.Round(rawResult, decimalDigits);
        }
        /// <summary>
        /// Force it to be a non-negative value.
        /// </summary>
        /// <param name="strFieldNm"></param>
        /// <param name="newVal"></param>
        protected void SetPositiveMoneyField(string strFieldNm, decimal newVal)
        {
            if (newVal < 0)
            {
                newVal = 0.0M;
            }
            SetMoneyField(strFieldNm, newVal, 2);
        }
        protected void SetMoneyField(string strFieldNm, decimal newVal)
        {
            SetMoneyField(strFieldNm, newVal, 2);
        }
        protected void SetMoneyField(string strFieldNm, decimal newVal, int decimalDigits)
        {
            object value = ReadFromDataRow(strFieldNm, true);
            decimal oldVal = Math.Round(Convert.ToDecimal(value), decimalDigits);
            newVal = Math.Round(newVal, decimalDigits);
            if (IsRequireFieldSet(strFieldNm, oldVal, newVal))
            {
                // OPM 458322.  DB datatype Money has scale of 4, so we have use decimal
                // when digits after the decimal exceed this value.
                var dataType = decimalDigits <= 4 ? SqlDbType.Money : SqlDbType.Decimal;
                OnFieldChange(strFieldNm, dataType, oldVal, newVal);
                IDataContainer row = GetDataRow();
                row[strFieldNm] = newVal;
            }
        }
        protected void SetNullableMoneyField(string strFieldNm, decimal? newVal)
        {
            SetNullableMoneyField(strFieldNm, newVal, 2);
        }
        protected void SetNullableMoneyField(string strFieldNm, decimal? newVal, int decimalDigits)
        {
            object value = ReadFromDataRow(strFieldNm, true);
            decimal? oldVal = null;
            if (value != DBNull.Value)
            {
                oldVal = Math.Round(Convert.ToDecimal(value), decimalDigits);
            }
            if (newVal.HasValue)
            {
                newVal = Math.Round(newVal.Value, decimalDigits);
            }
            if (IsRequireFieldSet(strFieldNm, oldVal, newVal))
            {
                // OPM 458322.  DB datatype Money has scale of 4, so we have use decimal
                // when digits after the decimal exceed this value.
                var dataType = decimalDigits <= 4 ? SqlDbType.Money : SqlDbType.Decimal;
                OnFieldChange(strFieldNm, dataType, oldVal, newVal);
                IDataContainer row = GetDataRow();
                row[strFieldNm] = newVal;
            }
        }

        /// <summary>
        /// Not to be confused with GetMoneyField or GetRateField, this is used for decimals that are NOT money or percentages.
        /// They're pretty much the same, though.
        /// </summary>
        /// <param name="strFieldNm"></param>
        protected decimal GetDecimalField(string strFieldNm, decimal? defaultValue = null)
        {
            return GetDecimalField(strFieldNm, 2, defaultValue);
        }
        protected decimal GetDecimalField(string strFieldNm, int decimalDigits, decimal? defaultValue = null)
        {
            object value = ReadFromDataRow(strFieldNm);

            if (value == DBNull.Value && defaultValue.HasValue)
            {
                return Math.Round(defaultValue.Value, decimalDigits);
            }

            decimal rawResult = (decimal)value;
            return Math.Round(rawResult, decimalDigits);
        }
        protected void SetDecimalField(string strFieldNm, decimal newVal)
        {
            SetDecimalField(strFieldNm, newVal, 2);
        }
        protected void SetDecimalField(string strFieldNm, decimal newVal, int decimalDigits)
        {
            object value = ReadFromDataRow(strFieldNm, true);

            decimal? oldVal = null;
            if (!Convert.IsDBNull(value))
            {
                oldVal = Math.Round(Convert.ToDecimal(value), decimalDigits);
            }

            newVal = Math.Round(newVal, decimalDigits);
            if (IsRequireFieldSet(strFieldNm, oldVal, newVal))
            {
                OnFieldChange(strFieldNm, SqlDbType.Money, oldVal, newVal);
                IDataContainer row = GetDataRow();
                row[strFieldNm] = newVal;
            }
        }


        /// <summary>
        /// For fields that is typically money but can accept text input like ("INCL").
        /// If the text user input isn't money then just return 0
        /// </summary>
        /// <param name="strFieldNm"></param>
        /// <returns></returns>
        protected decimal GetMoneyVarCharField(string strFieldNm)
        {
            try
            {
                return m_convertLos.ToMoney(GetMoneyVarCharField_rep(strFieldNm));
            }
            catch { return 0; }
        }
        /// <summary>
        /// For fields that is typically money but can accept text input like ("INCL")
        /// </summary>
        /// <param name="strFieldNm"></param>
        /// <returns></returns>
        protected string GetMoneyVarCharField_rep(string strFieldNm)
        {
            string val = GetStringVarCharField(strFieldNm);
            return m_convertLos.ToMoneyVarCharStringRep(val);
        }
        protected void SetMoneyVarCharField_rep(string strFieldNm, string newStrVal)
        {
            object value = ReadFromDataRow(strFieldNm, true);
            string newVal = newStrVal;
            if (null == newVal)
            {
                newVal = "";
            }

            newVal = newVal.TrimWhitespaceAndBOM();

            string oldVal = value.ToString().TrimWhitespaceAndBOM();
            if (IsRequireFieldSet(strFieldNm, oldVal, newVal))
            {
                SetStringVarCharField(strFieldNm, newVal);
            }

        }

        protected string GetPhoneNumberField(string strFieldNm)
        {
            return m_convertLos.ToPhoneNumFormat(GetStringVarCharField(strFieldNm), FormatDirection.ToRep);
        }
        protected void SetPhoneNumberField(string strFieldNm, string newVal)
        {
            SetStringVarCharField(strFieldNm, m_convertLos.ToPhoneNumFormat(newVal, FormatDirection.ToDb));
        }

        protected string GetSsnField(string strFieldNm)
        {
            return m_convertLos.ToSsnFormat(GetStringCharField(strFieldNm), FormatDirection.ToRep); ;
        }

        /// <summary>
        /// Gets the value of an SSN field that can be encrypted.
        /// </summary>
        /// <param name="plaintextFieldName">
        /// The name of the plaintext field.
        /// </param>
        /// <param name="encryptedFieldName">
        /// The name of the encrypted field.
        /// </param>
        /// <param name="minimumVersionForEncryption">
        /// The minimum version necessary to use the encrypted value.
        /// </param>
        /// <returns>
        /// The value of the SSN field.
        /// </returns>
        protected string GetEncryptedSsnField(
            string plaintextFieldName, 
            string encryptedFieldName, 
            EncryptionMigrationVersion minimumVersionForEncryption = EncryptionMigrationVersion.SocialSecurityNumbers)
        {
            if (!this.sEncryptionMigrationVersion.HasValue && !this.forcePlaintextValueforEncryptedFields)
            {
                throw new InvalidOperationException("Cannot determine the encryption migration version for the loan.");
            }

            if (!EncryptionMigrationVersionUtils.IsOnOrBeyondEncryptionVersion(this.sEncryptionMigrationVersion.Value, minimumVersionForEncryption) ||
                this.forcePlaintextValueforEncryptedFields)
            {
                return this.GetSsnField(plaintextFieldName);
            }

            if (this.encryptionKeyId == null || this.encryptionKeyId == EncryptionKeyIdentifier.BadIdentifier)
            {
                throw new ArgumentException("For migrated loans, encryption key identifier must be valid.", nameof(this.encryptionKeyId));
            }

            var encryptedValue = this.GetBinaryField(encryptedFieldName);
            var decryptedValue = LendersOffice.Drivers.Encryption.EncryptionHelper.DecryptString(this.encryptionKeyId.Value, encryptedValue);
            return this.m_convertLos.ToSsnFormat(decryptedValue, FormatDirection.ToRep);
        }

        protected void SetSsnField(string strFieldNm, string newVal)
        {
            SetStringCharField(strFieldNm, m_convertLos.ToSsnFormat(newVal.Replace(" ", ""), FormatDirection.ToDb));
        }

        /// <summary>
        /// Sets the value of an encryptable field.
        /// </summary>
        /// <param name="plaintextFieldName">
        /// The name of the plaintext field.
        /// </param>
        /// <param name="encryptedFieldName">
        /// The name of the encrypted field.
        /// </param>
        /// <param name="newValue">
        /// The new value.
        /// </param>
        /// <param name="minimumVersionForEncryption">
        /// The minimum version necessary to use the encrypted value.
        /// </param>
        protected void SetEncryptedSsnField(
            string plaintextFieldName, 
            string encryptedFieldName, 
            string newValue,
            EncryptionMigrationVersion minimumVersionForEncryption = EncryptionMigrationVersion.SocialSecurityNumbers)
        {
            if (!this.sEncryptionMigrationVersion.HasValue && !this.forcePlaintextValueforEncryptedFields)
            {
                throw new InvalidOperationException("Cannot determine the encryption migration version for the loan.");
            }

            var oldValue = this.GetEncryptedSsnField(plaintextFieldName, encryptedFieldName, minimumVersionForEncryption);
            if (!this.IsSsnFieldSet(oldValue, newValue))
            {
                return;
            }

            var formattedValue = newValue;
            if (this.sEncryptionMigrationVersion != EncryptionMigrationVersion.Unmigrated)
            {
                // The plaintext setter for the new SSN value will format the new value
                // using the current loan's format target. Replicate that behavior for
                // the value we're going to encrypt.
                formattedValue = this.m_convertLos.ToSsnFormat(newValue, FormatDirection.ToDb);
            }

            if (!EncryptionMigrationVersionUtils.IsOnOrBeyondEncryptionVersion(this.sEncryptionMigrationVersion.Value, minimumVersionForEncryption) ||
                this.forcePlaintextValueforEncryptedFields)
            {
                this.SetSsnField(plaintextFieldName, formattedValue);
                return;
            }

            if (this.encryptionKeyId == null || this.encryptionKeyId == EncryptionKeyIdentifier.BadIdentifier)
            {
                throw new ArgumentException("For migrated loans, encryption key identifier must be valid.", nameof(this.encryptionKeyId));
            }

            var encryptedValue = LendersOffice.Drivers.Encryption.EncryptionHelper.EncryptString(this.encryptionKeyId.Value, formattedValue);
            this.SetBinaryField(encryptedFieldName, encryptedValue);
        }

        protected SHA256Checksum GetSHA256ChecksumField(string fieldName)
        {
            string s = this.GetStringVarCharField(fieldName);

            if (string.IsNullOrEmpty(s))
            {
                return SHA256Checksum.Invalid;
            }

            var v = SHA256Checksum.Create(s);

            if (v == null)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            return v.Value;
        }

        protected void SetSHA256ChecksumField(string fieldName, SHA256Checksum value)
        {
            this.SetStringVarCharField(fieldName, value.Value);
        }

        protected string GetStringVarCharField(string strFieldNm) // For all string types such FullName, PartName...
        {
            object data = ReadFromDataRow(strFieldNm);
            if (data == DBNull.Value)
            {
                return string.Empty;
            }

            return data.ToString();
        }

        /// <summary>
        /// Gets the value of an string field that can be encrypted.
        /// </summary>
        /// <param name="plaintextFieldName">
        /// The name of the plaintext field.
        /// </param>
        /// <param name="encryptedFieldName">
        /// The name of the encrypted field.
        /// </param>
        /// <param name="minimumVersionForEncryption">
        /// The minimum version necessary to use the encrypted value.
        /// </param>
        /// <returns>
        /// The value of the string field.
        /// </returns>
        protected string GetEncryptedVarCharField(
            string plaintextFieldName,
            string encryptedFieldName,
            EncryptionMigrationVersion minimumVersionForEncryption = EncryptionMigrationVersion.SocialSecurityNumbers)
        {
            if (!this.sEncryptionMigrationVersion.HasValue && !this.forcePlaintextValueforEncryptedFields)
            {
                throw new InvalidOperationException("Cannot determine the encryption migration version for the loan.");
            }

            if (!EncryptionMigrationVersionUtils.IsOnOrBeyondEncryptionVersion(this.sEncryptionMigrationVersion.Value, minimumVersionForEncryption) ||
                this.forcePlaintextValueforEncryptedFields)
            {
                return this.GetStringVarCharField(plaintextFieldName);
            }

            if (this.encryptionKeyId == null || this.encryptionKeyId == EncryptionKeyIdentifier.BadIdentifier)
            {
                throw new ArgumentException("For migrated loans, encryption key identifier must be valid.", nameof(this.encryptionKeyId));
            }

            var encryptedValue = this.GetBinaryField(encryptedFieldName);
            return LendersOffice.Drivers.Encryption.EncryptionHelper.DecryptString(this.encryptionKeyId.Value, encryptedValue);
        }

        protected void SetStringVarCharField(string strFieldNm, string newStrVal) // For all string types such FullName, PartName...
        {
            object value = ReadFromDataRow(strFieldNm, true);
            string newVal = newStrVal;
            if (null == newVal)
            {
                newVal = "";
            }

            newVal = newVal.TrimWhitespaceAndBOM();

            string oldVal = value.ToString().TrimWhitespaceAndBOM();
            if (IsRequireFieldSet(strFieldNm, oldVal, newVal))
            {
                int maxLen = GetColumnMaxLength(strFieldNm);
                if (maxLen >= 0 && newVal.Length > maxLen)
                {
                    string s = newVal;
                    newVal = newVal.Substring(0, maxLen);
                    LogWarning("Value:" + s + " is too big for field " + strFieldNm + " which has max length of " + maxLen.ToString() + ". Truncate it to " + newVal);
                }

                OnFieldChange(strFieldNm, SqlDbType.VarChar, oldVal, newVal);
                IDataContainer row = GetDataRow();

                row[strFieldNm] = newVal;

                if (strFieldNm == "sLNm")
                {
                    IsLoanNameChanged = true;
                }
            }
        }

        /// <summary>
        /// Sets the value of an encryptable string field.
        /// </summary>
        /// <param name="plaintextFieldName">
        /// The name of the plaintext field.
        /// </param>
        /// <param name="encryptedFieldName">
        /// The name of the encrypted field.
        /// </param>
        /// <param name="newValue">
        /// The new value.
        /// </param>
        /// <param name="minimumVersionForEncryption">
        /// The minimum version necessary to use the encrypted value.
        /// </param>
        protected void SetEncryptedVarCharField(
            string plaintextFieldName, 
            string encryptedFieldName, 
            string newValue,
            EncryptionMigrationVersion minimumVersionForEncryption = EncryptionMigrationVersion.SocialSecurityNumbers)
        {
            if (!this.sEncryptionMigrationVersion.HasValue && !this.forcePlaintextValueforEncryptedFields)
            {
                throw new InvalidOperationException("Cannot determine the encryption migration version for the loan.");
            }

            if (!EncryptionMigrationVersionUtils.IsOnOrBeyondEncryptionVersion(this.sEncryptionMigrationVersion.Value, minimumVersionForEncryption) ||
                this.forcePlaintextValueforEncryptedFields)
            {
                this.SetStringVarCharField(plaintextFieldName, newValue);
                return;
            }

            if (this.encryptionKeyId == null || this.encryptionKeyId == EncryptionKeyIdentifier.BadIdentifier)
            {
                throw new ArgumentException("For migrated loans, encryption key identifier must be valid.", nameof(this.encryptionKeyId));
            }

            // Use SetStringVarCharField behavior of trimming and coalescing with empty.
            var normalizedValue = newValue?.Trim() ?? string.Empty;
            var encryptedValue = LendersOffice.Drivers.Encryption.EncryptionHelper.EncryptString(this.encryptionKeyId.Value, normalizedValue);
            this.SetBinaryField(encryptedFieldName, encryptedValue);
        }

        private Dictionary<string, FileDBTextField> m_fileDBTextFieldDictionary = new Dictionary<string, FileDBTextField>();
        private static Dictionary<string, E_FileDBKeyType> masterListOfFileDBKeyDictionary = new Dictionary<string, E_FileDBKeyType>()
        {
            { "sAgentXmlContent", E_FileDBKeyType.Normal_Loan_sAgentXmlContent},
            { "sPmlCertXmlContent", E_FileDBKeyType.Normal_Loan_sPmlCertXmlContent},
            { "sClosingCostSetJsonContent", E_FileDBKeyType.Normal_Loan_sClosingCostSetJsonContent},
            { "sTpoRequestForInitialDisclosureGenerationEventsJsonContent", E_FileDBKeyType.Normal_Loan_sTpoRequestForInitialDisclosureGenerationEventsJsonContent },
            { "sDuThirdPartyProviders", E_FileDBKeyType.Normal_Loan_sDuThirdPartyProvidersJsonContent},
            { "sTpoRequestForRedisclosureGenerationEventsJsonContent", E_FileDBKeyType.Normal_Loan_sTpoRequestForRedisclosureGenerationEventsJsonContent},
            { "sTpoRequestForInitialClosingDisclosureGenerationEventsJsonContent", E_FileDBKeyType.Normal_Loan_sTpoRequestForInitialClosingDisclosureGenerationEventsJsonContent },
            { "sFHACaseQueryResultXmlContent", E_FileDBKeyType.Normal_Loan_sFHACaseQueryResultXmlContent },
            { "sFHACaseNumberResultXmlContent", E_FileDBKeyType.Normal_Loan_sFHACaseNumberResultXmlContent },
            { "sFHACAVIRSResultXmlContent", E_FileDBKeyType.Normal_Loan_sFHACAVIRSResultXmlContent },
            { "sProdPmlDataUsedForLastPricingXmlContent", E_FileDBKeyType.Normal_Loan_sProdPmlDataUsedForLastPricingXmlContent },
            { "sTotalScoreCertificateXmlContent", E_FileDBKeyType.Normal_Loan_sTotalScoreCertificateXmlContent },
            { "sTotalScoreTempCertificateXmlContent", E_FileDBKeyType.Normal_Loan_sTotalScoreTempCertificateXmlContent },
            { "sPreparerXmlContent", E_FileDBKeyType.Normal_Loan_sPreparerXmlContent }
        };

        /// <summary>
        /// Gets the FileDBTextFields whose values have been read.
        /// </summary>
        /// <returns>An enumerable of FileDBTextFields whose values have been read.</returns>
        protected IEnumerable<FileDBTextField> GetModifiedFileDBTextField()
        {
            List<FileDBTextField> list = new List<FileDBTextField>();

            foreach (var o in m_fileDBTextFieldDictionary)
            {
                list.Add(o.Value);
            }

            return list;
        }

        protected virtual string GetFileDBTextField(string strFieldNm, bool requireOnDemandMigration = false)
        {
            FileDBTextField item = null;

            if (m_fileDBTextFieldDictionary.TryGetValue(strFieldNm, out item) == false)
            {
                E_FileDBKeyType keyType;

                if (masterListOfFileDBKeyDictionary.TryGetValue(strFieldNm, out keyType) == false)
                {
                    throw CBaseException.GenericException("[" + strFieldNm + "] is not a valid FileDBTextField.");
                }

                var dbEntryInfo = Tools.GetFileDBEntryInfoForLoan(keyType, this.m_fileId);
                item = new FileDBTextField(dbEntryInfo.FileDBType, strFieldNm, dbEntryInfo.Key);

                if (requireOnDemandMigration == true)
                {
                    if (item.IsExistedInFileDB == false)
                    {
                        item.Content = GetLongTextField(strFieldNm);
                    }
                }

                m_fileDBTextFieldDictionary.Add(strFieldNm, item);
            }

            return item.Content;
        }

        protected virtual void SetFileDBTextField(string fieldName, string value)
        {
            FileDBTextField item = null;

            string newVal = value ?? string.Empty;
            string oldVal;

            if (m_fileDBTextFieldDictionary.TryGetValue(fieldName, out item) == false)
            {
                E_FileDBKeyType keyType;

                if (masterListOfFileDBKeyDictionary.TryGetValue(fieldName, out keyType) == false)
                {
                    throw CBaseException.GenericException("[" + fieldName + "] is not a valid FileDBTextField.");
                }

                var dbEntryInfo = Tools.GetFileDBEntryInfoForLoan(keyType, this.m_fileId);
                item = new FileDBTextField(dbEntryInfo.FileDBType, fieldName, dbEntryInfo.Key);

                m_fileDBTextFieldDictionary.Add(fieldName, item);

                oldVal = null;
            }
            else
            {
                oldVal = item.Content;
            }

            if (IsRequireFieldSet(fieldName, oldVal, newVal))
            {
                OnFieldChange(fieldName);
                item.Content = value;
            }
            
        }

        protected virtual byte[] GetBinaryField(string fieldName)
        {
            var obj = this.ReadFromDataRow(fieldName);
            if (obj == DBNull.Value)
            {
                return null;
            }

            return (byte[])obj;
        }

        protected virtual void SetBinaryField(string fieldName, byte[] newVal)
        {
            var oldVal = this.GetBinaryField(fieldName);
            if (IsRequireFieldSet(fieldName, oldVal, newVal))
            {
                // Use SqlDbType.Image instead of SqlDbType.VarBinary,
                // as VarBinary is capped at 8000 bytes.
                OnFieldChange(fieldName, SqlDbType.Image, oldVal, newVal);

                var row = this.GetDataRow();
                if (newVal == null)
                {
                    row[fieldName] = DBNull.Value;
                }
                else
                {
                    row[fieldName] = newVal;
                }
            }
        }

        internal static void DuplicateFileDBTextField(Guid sourceFileId, Guid destFileId, HashSet<E_FileDBKeyType> keyTypesToIgnore)
        {
            foreach (var o in masterListOfFileDBKeyDictionary)
            {
                if (keyTypesToIgnore != null && keyTypesToIgnore.Contains(o.Value))
                {
                    continue;
                }

                try
                {
                    var sourceDbEntryInfo = Tools.GetFileDBEntryInfoForLoan(o.Value, sourceFileId);
                    var destDbEntryInfo = Tools.GetFileDBEntryInfoForLoan(o.Value, destFileId);

                    FileDBTools.Copy(sourceDbEntryInfo.FileDBType, sourceDbEntryInfo.Key, destDbEntryInfo.Key);
                }
                catch (FileNotFoundException)
                {
                    // 11/12/2015 - dd - OK to ignore if source file is missing.
                }
            }
        }

        protected string GetLongTextField(string strFieldNm) // For all string types such FullName, PartName...
        {
            return ReadFromDataRow(strFieldNm).ToString();
        }
        protected void SetLongTextField(string strFieldNm, string newStrVal)
        {
            object value = ReadFromDataRow(strFieldNm, true);
            string newVal = newStrVal;
            if (null == newVal)
            {
                newVal = "";
            }

            newVal = newVal.TrimWhitespaceAndBOM();

            string oldVal = value.ToString();
            if (IsRequireFieldSet(strFieldNm, oldVal, newVal))
            {
                OnFieldChange(strFieldNm, SqlDbType.Text, oldVal, newVal);
                IDataContainer row = GetDataRow();

                row[strFieldNm] = newVal;
            }
        }

        protected string GetStringCharField(string strFieldNm) // For all string types such FullName, PartName...
        {
            object data = ReadFromDataRow(strFieldNm);
            return data.ToString().TrimWhitespaceAndBOM();
        }
        protected void SetStringCharField(string strFieldNm, string newStrVal) // For all string types such FullName, PartName...
        {
            object value = ReadFromDataRow(strFieldNm, true);
            string newVal = newStrVal;
            if (null == newVal)
            {
                newVal = "";
            }

            newVal = newVal.TrimWhitespaceAndBOM();

            string oldVal = value.ToString().TrimWhitespaceAndBOM();
            if (IsRequireFieldSet(strFieldNm, oldVal, newVal))
            {
                int maxLen = GetColumnMaxLength(strFieldNm);
                if (maxLen >= 0 && newVal.Length > maxLen)
                {
                    string s = newVal;
                    newVal = newVal.Substring(0, maxLen);
                    LogWarning("Value:" + s + " is too big for field " + strFieldNm + " which has max length of " + maxLen.ToString() + ". Truncate it to " + newVal);
                }

                OnFieldChange(strFieldNm, SqlDbType.Char, oldVal, newVal);
                IDataContainer row = GetDataRow();

                row[strFieldNm] = newVal;
            }
        }

        protected Guid GetGuidField(string strFieldNm)
        { 
            return GetGuidField(strFieldNm, false);
        }
        protected Guid GetGuidField(string strFieldNm, bool bIsAccessForWrite)
        {
            object value = ReadFromDataRow(strFieldNm, bIsAccessForWrite);

            if (value == DBNull.Value)
            {
                return Guid.Empty;
            }

            try { return (Guid)value; }
            catch { return Guid.Empty; }
        }

        protected void SetGuidFieldPreserveEmptyGuid(string strFieldNm, Guid newVal)
        {
            object value = ReadFromDataRow(strFieldNm, true);
            Guid oldVal = GetGuidField(strFieldNm);
            if (IsRequireFieldSet(strFieldNm, oldVal, newVal))
            {
                string oldValDesc = oldVal.ToString();
                OnFieldChange(strFieldNm, SqlDbType.UniqueIdentifier, oldVal, newVal);
                IDataContainer row = GetDataRow();

                row[strFieldNm] = newVal;
            }
        }

        protected void SetGuidField(string strFieldNm, Guid newVal)
        {
            object value = ReadFromDataRow(strFieldNm, true);
            Guid oldVal = GetGuidField(strFieldNm);
            if (IsRequireFieldSet(strFieldNm, oldVal, newVal))
            {
                string oldValDesc = (Guid.Empty == oldVal) ? "" : oldVal.ToString();
                IDataContainer row = GetDataRow();

                if (Guid.Empty == newVal)
                {
                    OnFieldChange(strFieldNm, SqlDbType.UniqueIdentifier, oldVal, newVal);
                    row[strFieldNm] = DBNull.Value;
                }
                else
                {
                    OnFieldChange(strFieldNm, SqlDbType.UniqueIdentifier, oldVal, newVal);
                    row[strFieldNm] = newVal;
                }
            }
        }

        #endregion
        protected virtual bool IsRequireFieldSet(string strFieldNm, bool oldValue, bool newValue) 
        {
            if (null != m_parent)
                return m_parent.IsRequireFieldSet(strFieldNm, oldValue, newValue);

            return oldValue != newValue;
        }
        protected virtual bool IsRequireFieldSet(string strFieldNm, Enum oldValue, Enum newValue) 
        {
            if (null != m_parent)
                return m_parent.IsRequireFieldSet(strFieldNm, oldValue, newValue);

            return oldValue != newValue;
        }
        protected virtual bool IsRequireFieldSet(string strFieldNm, int oldValue, int newValue) 
        {
			if (null != m_parent)
			{
				return m_parent.IsRequireFieldSet(strFieldNm, oldValue, newValue);
			}
			return oldValue != newValue;
        }
        protected virtual bool IsRequireFieldSet(string strFieldNm, int? oldValue, int? newValue)
        {
            if (null != m_parent)
            {
                return m_parent.IsRequireFieldSet(strFieldNm, oldValue, newValue);
            }
            return oldValue != newValue;
        }
        protected virtual bool IsRequireFieldSet(string strFieldNm, long oldValue, long newValue) 
        {
            if (null != m_parent)
                return m_parent.IsRequireFieldSet(strFieldNm, oldValue, newValue);

            return oldValue != newValue;
        }
        protected virtual bool IsRequireFieldSet(string strFieldNm, long? oldValue, long? newValue)
        {
            if (null != m_parent)
                return m_parent.IsRequireFieldSet(strFieldNm, oldValue, newValue);

            return oldValue != newValue;
        }

        protected virtual bool IsRequireFieldSet(string strFieldNm, bool? oldValue, bool? newValue)
        {
            if (null != m_parent)
                return m_parent.IsRequireFieldSet(strFieldNm, oldValue, newValue);

            return oldValue != newValue;
        }

        protected virtual bool IsRequireFieldSet(string strFieldNm, decimal oldValue, decimal newValue) 
        {
            if (null != m_parent)
                return m_parent.IsRequireFieldSet(strFieldNm, oldValue, newValue);

            return oldValue != newValue;
        }

        protected virtual bool IsRequireFieldSet(string strFieldNm, decimal? oldValue, decimal? newValue)
        {
            if (null != m_parent)
                return m_parent.IsRequireFieldSet(strFieldNm, oldValue, newValue);

            return oldValue != newValue;
        }
        protected virtual bool IsRequireFieldSet(string strFieldNm, string oldValue, string newValue) 
        {
            if (null != m_parent)
                return m_parent.IsRequireFieldSet(strFieldNm, oldValue, newValue);

            return 0 != string.Compare(oldValue, newValue);
        }
        protected virtual bool IsRequireFieldSet(string strFieldNm, Guid oldValue, Guid newValue) 
        {
            if (null != m_parent)
                return m_parent.IsRequireFieldSet(strFieldNm, oldValue, newValue);

            return 0 != oldValue.CompareTo(newValue);
        }
        protected virtual bool IsRequireFieldSet(string strFieldNm, DateTime oldValue, DateTime newValue) 
        {
            if (null != m_parent)
                return m_parent.IsRequireFieldSet(strFieldNm, oldValue, newValue);

            return 0 != oldValue.CompareTo(newValue);
        }
        protected virtual bool IsRequireFieldSet(string strFieldNm, CDateTime oldValue, CDateTime newValue) 
        {
            if (null != m_parent)
                return m_parent.IsRequireFieldSet(strFieldNm, oldValue, newValue);

            return !oldValue.SameAs(newValue);
        }

        protected virtual bool IsRequireFieldSet(string fieldName, byte[] oldValue, byte[] newValue)
        {
            if (this.m_parent != null)
            {
                return this.m_parent.IsRequireFieldSet(fieldName, oldValue, newValue);
            }

            return !Tools.AreArraysIdentical<byte>(oldValue, newValue);
        }

        /// <summary>
        /// Determines whether an SSN field has been set to a new value.
        /// </summary>
        /// <param name="oldValue">
        /// The old SSN value.
        /// </param>
        /// <param name="newValue">
        /// The new SSN value.
        /// </param>
        /// <returns>
        /// True if the field has been set to a new value,
        /// false otherwise.
        /// </returns>
        /// <remarks>
        /// Compare the old value against the new value formatted for SSN
        /// to prevent changes due to the new value being the same but not 
        /// properly formatted.
        /// </remarks>
        protected bool IsSsnFieldSet(string oldValue, string newValue)
        {
            // Check for both null, as ToSsnFormat will coalesce null to the empty string.
            if (oldValue == null && newValue == null)
            {
                return false;
            }

            return !string.Equals(oldValue, this.m_convertLos.ToSsnFormat(newValue, FormatDirection.ToRep), StringComparison.Ordinal);
        }

        internal bool IsRequireFieldSetForXmlRecord(string fieldNm, string oldValue, string newValue)
        {
            return IsRequireFieldSet(fieldNm, oldValue, newValue);
        }

		/// <summary>
		/// Returns -1 if cannot be determined.
		/// </summary>
		/// <param name="strFieldNm"></param>
		/// <param name="newVal"></param>
		/// <returns></returns>
		protected virtual int GetColumnMaxLength( string strFieldNm )
		{
			// The parent has all data to handle this task.
			return m_parent.GetColumnMaxLength( strFieldNm );
		}

		protected virtual  void OnFileDbFieldChange( Guid keySection, string sFileDBVal )
		{
			m_parent.OnFileDbFieldChange( keySection, sFileDBVal );
		}

        protected virtual void OnFileDbFieldChange(string strFieldNm, Guid keySection, string sFileDBVal)
        {
            m_parent.OnFileDbFieldChange(strFieldNm, keySection, sFileDBVal);
        }

		protected	virtual	void OnFieldChange( string strFieldNm, SqlDbType dbType, object oldValue, object newValue )
		{
			// The parent has all data to handle this event and do the update later.
			m_parent.OnFieldChange( strFieldNm, dbType, oldValue, newValue );
		}

        protected abstract void OnFieldChange(string fieldName);

		protected void Throw( string sErrMsg )
		{
            Tools.LogError("DataAccess:" + m_namePage + ":: " + sErrMsg + ". Data access. FileId = " + m_fileId.ToString());
			throw new CBaseException( ErrorMessages.Generic, sErrMsg + ".  Data access.  FileId = " + m_fileId.ToString() );
		}

        public static DataSet GetDataSet(E_DataSetT dataSetT, string xmlContent)
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            DataSet ds = null;
            switch (dataSetT)
            {
                case E_DataSetT.AgentXml:
                    ds = new LendersOfficeApp.ObjLib.Schema.AgentXmlTable();
                    break;
                case E_DataSetT.LiabilityXml:
                    ds = new LendersOfficeApp.ObjLib.Schema.LiabilitySchemaDataSet();
                    break;
                case E_DataSetT.PreparerXml:
                    ds = new LendersOfficeApp.ObjLib.Schema.PreparerXmlTable();
                    break;
                default:
                    throw new UnhandledEnumException(dataSetT);
            }

            if (!string.IsNullOrEmpty(xmlContent))
            {
                using (StringReader reader = new StringReader(xmlContent))
                {
                    ds.ReadXml(reader, XmlReadMode.IgnoreSchema);
                }
            }

            watch.Stop();
#if (DEBUG)
            Tools.LogInfo("GetDataSet " + dataSetT + " [" + xmlContent + "] executed in " + watch.ElapsedMilliseconds + "ms");
#endif
            return ds;
        }
		public  DataSet GetDataSet( string xmlSchema, string xmlContent )
		{

			StreamReader readerData = null;
			StreamReader readerSchema = null;
			MemoryStream streamSchema = null;
			MemoryStream streamData = null;
			try
			{
				//ThienChange,  org. code : System.Text.ASCIIEncoding e = new System.Text.ASCIIEncoding(); 
                System.Text.Encoding e = System.Text.Encoding.ASCII;

				DataSet ds = new DataSet( "XmlTable" );

				// Don't remove this block, we use this to retrieve the schema during debugging if it needs to be modified.
				bool RetrieveSchema = false;
				if( RetrieveSchema )
				{
					string contentMaster = "";
					string type = "Emplmt";
					switch( type )
					{
						case "Emplmt": 
							contentMaster = "<EmplmtXmlContent>    <EmplmtStartD />    <EmplmtEndD />    <EmplrNm />    <EmplrBusPhone />    <EmplrFax />    <EmplrAddr />    <EmplrCity />   <EmplrState />  <JobTitle />    <EmplrZip />    <VerifSentD />    <VerifRecvD />    <VerifExpD />    <IsSelfEmplmt />    <MonI />    <EmplmtStat />    <EmplmtLen />    <ProfLen /> <IsSpecialBorrowerEmployerRelationship /> <SelfOwnershipShareT />  </EmplmtXmlContent>";
							break;
						case "Lia":
							contentMaster = "<LiaXmlContent><OwnerT /><DebtT/><ComNm /><ComAddr /><ComCity /><ComState /><ComZip /><ComPhone /><ComFax /><AccNum /><AccNm /><Bal /><Pmt /><R /><OrigTerm /><Due /><RemainMons /><WillBePdOff /><NotUsedInRatio /><IsPiggyBack /><Late30 /><Late60 /><Late90Plus /><IncInReposession /><IncInBankruptcy /><IncInForeclosure /><ExcFromUnderwriting /><VerifSentD /><VerifRecvD /><VerifExpD /></LiaXmlContent>";
							break;
						case "Asset":
							contentMaster = "<AssetXmlContent> <OwnerT /> <AssetT /> <Val /> <Desc /> <FaceVal /> <AccNum /> <AccNm /> <ComNm /> <StAddr /> <City /> <State /> <Zip /> <VerifSentD /> <VerifRecvD /> <VerifExpD /> </AssetXmlContent>";
							break;
						case "Re":
							break;
					}
				
					streamData = new MemoryStream( e.GetBytes( contentMaster ) );
					readerData = new StreamReader( streamData, System.Text.Encoding.ASCII, true, contentMaster.Length + 2 );				
					ds.ReadXml( readerData, XmlReadMode.Auto );
					streamData.Close();
					readerData.Close();
					if( RetrieveSchema )
					{
						string resultXml = ds.GetXml();
						string resultSchema = ds.GetXmlSchema();
					}
				}

				streamSchema = new MemoryStream( e.GetBytes( xmlSchema ) );
				readerSchema = new StreamReader( streamSchema, System.Text.Encoding.ASCII, true, xmlSchema.Length + 2 );
				ds.ReadXmlSchema( readerSchema );

				
				if( xmlContent.Length > 0 )
				{
					streamData = new MemoryStream( e.GetBytes( xmlContent ) );
					readerData = new StreamReader( streamData, System.Text.Encoding.ASCII, true, xmlContent.Length + 2 );				
					ds.ReadXml( readerData, XmlReadMode.Auto );
				}
				
				return ds;
			}
			catch( Exception ex )
			{
				LogError( ex.Message );
			}
			finally
			{
				if( streamSchema != null )
					streamSchema.Close();

				if( streamData != null )
					streamData.Close();

				if( readerSchema != null )
					readerSchema.Close();

				if( readerData != null )
					readerData.Close();
			}
			return null;
		}

        protected string GetStringForCacheTable(string value, string fieldId) 
        {
            if (null == value)
                return "";


            // 7/10/2006 dd - Trim the value to match the max length in Loan_File_Cache table.
            CFieldInfoTable table = CFieldInfoTable.GetInstance();
            FieldProperties field = table[fieldId];
            if (null != field && null != field.DbInfo) 
            {
                int maxLength = field.DbInfo.m_charMaxLen;
                return value.Substring(0, Math.Min(value.Length, maxLength));
            }

            return value; // 7/10/2006 dd - Unable to find info in DbFileInfo. Just return original value.
        }

	}
}
