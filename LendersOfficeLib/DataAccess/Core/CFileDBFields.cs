﻿using System;
using System.IO;
using System.Text;
using System.Xml;

namespace DataAccess
{
    /// <summary>
    /// Currently supporting only one field per file, will extend it if needed.
    /// </summary>
    public class CFileDBFields
    {
        static public string GetStringKey(Guid key)
        {
            return key.ToString().Replace("-", "").ToUpper();
        }

        private string m_fileDbKey;

        public CFileDBFields(Guid keyField)
        {
            m_fileDbKey = GetStringKey(keyField);
        }

        public void Save(string sectionName, string newVal)
        {
            Action<Stream> writeHandler = delegate(Stream fileStream)
            {
                XmlWriterSettings writerSettings = new XmlWriterSettings();
                writerSettings.Encoding = new UTF8Encoding(false); // Skip BOM
                writerSettings.OmitXmlDeclaration = true;

                using (XmlWriter xmlWriter = XmlWriter.Create(fileStream, writerSettings))
                {
                    xmlWriter.WriteStartDocument(true);
                    xmlWriter.WriteStartElement("CFileDBFields");
                    xmlWriter.WriteElementString(sectionName, newVal);
                    xmlWriter.WriteEndElement(); //"CFileDBFields"
                    xmlWriter.WriteEndDocument();
                }
            };

            FileDBTools.WriteData(E_FileDB.Normal, this.m_fileDbKey, writeHandler);
        }

        public string Load(string sectionName)
        {
            string result = string.Empty;

            Action<Stream> readHandler = delegate(Stream fs)
            {
                XmlTextReader reader = new XmlTextReader(fs);

                try
                {
                    while (!reader.EOF)
                    {
                        if (reader.MoveToContent() == XmlNodeType.Element && reader.Name == sectionName)
                        {
                            result = reader.ReadElementString(sectionName);
                            return;
                        }
                        reader.Read();
                    }
                }
                finally
                {
                    reader.Close();
                }
            };

            FileDBTools.ReadData(E_FileDB.Normal, this.m_fileDbKey, readHandler);

            return result;
        }
    }
}