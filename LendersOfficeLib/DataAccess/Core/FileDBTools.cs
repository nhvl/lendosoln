﻿namespace DataAccess
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Text;
    using System.Xml;
    using FileDB3;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.HttpModule;
    using LqbGrammar.DataTypes;
    using Newtonsoft.Json;

    public enum E_FileDB
    {
        Normal,
        EDMS,
        Temp,
        EdmsPdf, // Only stored non-destructive pdf and png.
        PricingSnapshot // dd 6/12/2018 - 470633 - Only stored pricing snapshot.
    }

    /// <summary>
    /// Keep track of at least all loan filedb keys here. <para></para>
    /// Format: [DB]_Loan_[restofkeystring]<para></para>
    /// Note, trying to keep this alphabetical for readability.  <para></para>
    /// For that reason, do not ever save it to the DB.<para></para>
    /// </summary>
    public enum E_FileDBKeyType
    {
        /// <summary>
        /// Though a set of archives is associated with a loan, it's more closely tied to just an archive.
        /// </summary>
        Normal_ClosingCostArchive,

        /// <summary>
        /// Stores the lpe price group and its associated products.
        /// </summary>
        Normal_LpePriceGroup_Content,

        /// <summary>
        /// Stores the document for the custom form, typically word or xml.  Associated with FileDBKey of Custom_letter table.
        /// </summary>
        Normal_CustomNonPdf_Form,

        /// <summary>
        /// Stores the custom pdf form.  Associated with formid of Lo_form table.
        /// </summary>
        Edms_CustomPdf_Form,
        
        /// <summary>
        /// associated with the fee service files for a broker.
        /// </summary>
        Normal_FeeServiceRevision_File,

        /// <summary>
        /// Per lender certificate notes file. <see cref="LendersOffice.ObjLib.Security.LenderClientCertificate"/> Notes and GetFullNotesFileDbKey
        /// </summary>
        Normal_LenderClientCertificate_Notes,

        /// <summary>
        /// has aLiaXmlContent, aAssetXmlContent, and sCondXmlContent sections based on those keys.
        /// </summary>
        Normal_Loan,
        Normal_Loan_approval_pdf,
        Normal_Loan_audit,
        Normal_Loan_AutoDisclosureAuditHtml,
        Normal_Loan_CLOSING_COST_MIGRATION_ARCHIVE,
        Normal_Loan_coc_archive,

        /// <summary>
        /// Comes from ConstAppDavid.ComplianceEaseReportTag.
        /// </summary>
        Normal_Loan_COMPLIANCE_EAGLE_REPORT_PDF,
        Normal_Loan_COMPLIANCE_EASE_REPORT_PDF,
        Normal_Loan_DU_FINDINGS_HTML,
        Normal_Loan_FNMA_CASEFILE_EXPORT,
        Normal_Loan_FNMA_CASEFILE_IMPORT,
        Normal_Loan_FNMA_DU_UNDERWRITE,
        Normal_Loan_FNMA_DWEB_CASEFILEIMPORT,
        Normal_Loan_FNMA_DWEB_CASEFILESTATUS,
        Normal_Loan_FNMA_EARLYCHECK,
        Normal_Loan_FNMA_GETCREDITAGENCIES,
        Normal_Loan_FREDDIE_XML,
        Normal_Loan_GLOBAL_DMS_INFO_XML,
        Normal_Loan_gfe_archive,

        /// <summary>
        /// Comes from sLoanDataMigrationAuditHistoryKey string.
        /// </summary>
        Normal_Loan_MigrationAuditHistoryKey,
        Normal_Loan_pmlactivity,
        Normal_Loan_pmlsummary_pdf,
        Normal_Loan_PricingStateXml,
        Normal_Loan_ratelock_pdf,
        Normal_Loan_sAgentXmlContent,
        Normal_Loan_sClosingCostSetJsonContent,
        Normal_Loan_sPmlCertXmlContent,
        Normal_Loan_suspense_pdf,
        Temp_Loan_backup_audit,
        Temp_Loan_COMPLIANCE_EAGLE_REPORT_PDF,
        Temp_Loan_LPEFirstResult,
        Temp_Loan_LPESecondResult,

        Normal_Loan_sRawSelectedProductCodeFilterKey,
        Normal_Loan_sTpoRequestForInitialDisclosureGenerationEventsJsonContent,
        Normal_Loan_sDuThirdPartyProvidersJsonContent,
        Normal_Loan_sTpoRequestForRedisclosureGenerationEventsJsonContent,
        Normal_Loan_sTpoRequestForInitialClosingDisclosureGenerationEventsJsonContent,

        Normal_Loan_sFHACaseQueryResultXmlContent,
        Normal_Loan_sFHACaseNumberResultXmlContent,
        Normal_Loan_sFHACAVIRSResultXmlContent,
        Normal_Loan_sProdPmlDataUsedForLastPricingXmlContent,
        Normal_Loan_sTotalScoreCertificateXmlContent,
        Normal_Loan_sTotalScoreTempCertificateXmlContent,
        Normal_Loan_sPreparerXmlContent
    }

    public static class FileDBTools
    {
        public static string GetDatabaseName(this E_FileDB database)
        {
            switch (database)
            {
                case E_FileDB.Normal:
                    return ConstStage.FileDBSiteCode;
                case E_FileDB.EDMS:
                    return ConstStage.FileDBSiteCodeForEdms;
                case E_FileDB.EdmsPdf:
                    return ConstStage.FileDBSiteCodeForEdmsPdf;
                case E_FileDB.PricingSnapshot:
                    return ConstStage.FileDBSiteCodeForPricingSnapshot;
                case E_FileDB.Temp:
                    return ConstStage.FileDBSiteCodeForTempFile;
                default:
                    throw new UnhandledEnumException(database);
            }
        }

        public static E_FileDB GetFileDBType(this string database)
        {
            if (database.Equals(ConstStage.FileDBSiteCode, StringComparison.OrdinalIgnoreCase))
            {
                return E_FileDB.Normal;
            }
            else if (database.Equals(ConstStage.FileDBSiteCodeForEdms, StringComparison.OrdinalIgnoreCase))
            {
                return E_FileDB.EDMS;
            }
            else if (database.Equals(ConstStage.FileDBSiteCodeForTempFile, StringComparison.OrdinalIgnoreCase))
            {
                return E_FileDB.Temp;
            }
            else if (database.Equals(ConstStage.FileDBSiteCodeForEdmsPdf, StringComparison.OrdinalIgnoreCase))
            {
                return E_FileDB.EdmsPdf;
            }
            else if (database.Equals(ConstStage.FileDBSiteCodeForPricingSnapshot, StringComparison.OrdinalIgnoreCase))
            {
                return E_FileDB.PricingSnapshot;
            }
            else
            {
                throw CBaseException.GenericException("Invalid file db " + database);
            }
        }


        /// <summary>
        /// Retrieves the file specified by the database and key and copies it to a temp file returning its path.
        /// Exceptions from FileOperationHelper.Copy are possible.
        /// </summary>
        /// <param name="database">The FileDB database to retrieve the contents of the file from.</param>
        /// <param name="key">The unique key of the file.</param>
        /// <returns>The path in the temp folder.</returns>
        /// <exception cref="FileNotFoundException">When the key is not found in filedb.</exception>
        public static string CreateCopy(E_FileDB database, string key)
        {
            string dstFileName = TempFileUtils.NewTempFilePath();
            ReadFileImp(database, key, srcFileName => FileOperationHelper.Copy(srcFileName, dstFileName, true));
            return dstFileName;
        }

        /// <summary>
        /// Copy the file in database to another location in database.
        /// </summary>
        /// <param name="database">The FileDB database to copy the file.</param>
        /// <param name="sourceKey">The unique key of the source file.</param>
        /// <param name="destKey">The unique key of the destination file.</param>
        /// <exception cref="FileNotFoundException">When the source key is not found in filedb.</exception>
        /// <exception cref="ArgumentException">When the source key is equal to dest key.</exception>
        public static void Copy(E_FileDB database, string sourceKey, string destKey)
        {
            if (sourceKey == destKey)
            {
                throw new ArgumentException("SourceKey cannot be same as DestKey");
            }

            Action<string> readHandler = delegate(string tempFileName)
            {
                WriteFile(database, destKey, tempFileName);
            };

            ReadFileImp(database, sourceKey, readHandler);
        }

        /// <summary>
        /// Deletes the file at the specified key in the filedb.  Does not throw exception if the file is not found.
        /// </summary>
        /// <param name="database">The FileDB database to retrieve the contents of the file from.</param>
        /// <param name="key">The unique key of the file.</param>
        public static void Delete(E_FileDB database, string key)
        {
            if (ConstStage.UseNewFileDb)
            {
                DeleteNEW(database, key);
            }
            else
            {
                DeleteOLD(database, key);
            }
        }

        /// <summary>
        /// If the tempFileName exists in the temp directory, it does nothing.<para></para>
        /// If not, it copies the file from the filedb and key to the temp folder with the specified name. <para></para>
        /// This is intended for use with files that do not change their name nor their contents, for reducing traffic to filedb.
        /// </summary>
        /// <param name="database">The filedb where the file is stored.</param>
        /// <param name="key">The key where the file is stored.</param>
        /// <param name="tempFileName">The file name on the server in the temp file location.</param>
        public static void EnsureDiskHasCopy(E_FileDB database, string key, string tempFileName)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }

            if (tempFileName == null)
            {
                throw new ArgumentNullException(nameof(tempFileName));
            }

            string destFileName = TempFileUtils.Name2Path(tempFileName);
            if (File.Exists(destFileName))
            {
                return;
            }

            ReadFileImp(database, key, srcFileName => FileOperationHelper.Copy(srcFileName, destFileName, true));
        }

        /// <summary>
        /// Deletes the file at the specified key in the filedb.  Does not throw exception if the file is not found.
        /// </summary>
        /// <param name="database">The FileDB database to retrieve the contents of the file from.</param>
        /// <param name="key">The unique key of the file.</param>
        private static void DeleteOLD(E_FileDB database, string key)
        {
            string databaseName = GetDatabaseName(database);
            Stopwatch sw = Stopwatch.StartNew();
            FileDB fileDB = new FileDB();
            fileDB.DeleteFile(databaseName, key);
            sw.Stop();
            PerformanceMonitorItem.AddTimingDetailsToCurrent("FileDB.DeleteFile", sw.ElapsedMilliseconds);

            FileDbHttpRequestCache cache = FileDbHttpRequestCache.GetCurrent();
            cache.Remove(database, key);
        }

        private static void DeleteNEW(E_FileDB database, string key)
        {
            string databaseName = GetDatabaseName(database);

            var location = FileStorageIdentifier.TryParse(databaseName);

            if (location == null)
            {
                throw new ArgumentNullException("Bad Database name");
            }

            var fileId = FileIdentifier.TryParse(key);

            if (fileId == null)
            {
                throw new ArgumentNullException("Bad key");
            }

            LendersOffice.Drivers.FileDB.FileDbHelper.DeleteFile(location.Value, fileId.Value);
        }

        /// <summary>
        /// Throws overflow exception if the file is too large. Throws File not found exception if they key is not in filedb. 
        /// </summary>
        /// <param name="database">The FileDB database to retrieve the contents of the file from.</param>
        /// <param name="key">The unique key of the file.</param>
        /// <returns>byte array of the file</returns>
        public static byte[] ReadData(E_FileDB database, string key)
        {
            byte[] buffer = null;
            ReadFileImp(database, key, filePath => buffer = BinaryFileHelper.ReadAllBytes(filePath));
            return buffer;
        }

        /// <summary>
        /// Read file from specified db with specified key and call void method that will read from the filestream. 
        /// </summary>
        /// <param name="database">The FileDB database to retrieve the contents of the file from.</param>
        /// <param name="key">The unique key of the file.</param>
        /// <param name="readFrom">a delegate to call with the opened stream for reading. Once the delegate completes the stream is disposed.</param>
        public static void ReadData(E_FileDB database, string key, Action<Stream> readFrom)
        {
            ReadFileImp(database, key, readFrom);
        }

        /// <summary>
        /// Reads a string from file db. Uses Unicode Encoding.
        /// </summary>
        /// <param name="database">The FileDB database to retrieve the contents of the file from.</param>
        /// <param name="key">The unique key of the file.</param>
        /// <returns>Unicode string</returns>
        public static string ReadDataText(E_FileDB database, string key)
        {
            byte[] data = ReadData(database, key);
            return Encoding.Unicode.GetString(data);
        }

        /// <summary>
        /// Reads from an XML file stored in file db. It passes the opened xml read stream ready for 
        /// reading to the specified <paramref name="readerAction"/>.
        /// </summary>
        /// <param name="database">The FileDB database to retrieve the contents of the file from.</param>
        /// <param name="key">The unique key of the file.</param>
        /// <param name="readerAction">The reading action to perform.</param>
        public static void ReadXmlData(E_FileDB database, string key, Action<XmlReader> readerAction)
        {
            ReadXmlData(database, key, new XmlReaderSettings(), readerAction);
        }

        /// <summary>
        /// Reads from an XML file stored in file db. It passes the opened xml read stream ready for 
        /// reading with the specified <paramref name="settings"/> to the specified <paramref name="readerAction"/>.
        /// </summary>
        /// <param name="database">The FileDB database to retrieve the contents of the file from.</param>
        /// <param name="key">The unique key of the file.</param>
        /// <param name="settings">The settings for the reader to use.</param>
        /// <param name="readerAction">The reading action to perform.</param>
        public static void ReadXmlData(E_FileDB database, string key, XmlReaderSettings settings, Action<XmlReader> readerAction)
        {
            Action<string> readerHandler = filePath =>
            {
                using (var reader = XmlReader.Create(filePath, settings))
                {
                    readerAction(reader);
                }
            };

            ReadFileImp(database, key, readerHandler);
        }

        /// <summary>
        /// Writes the given string into FileDB in the specified database with the given key. It uses Encoding.Unicode (utf-16).
        /// </summary>
        /// <param name="database">The FileDB database to retrieve the contents of the file from.</param>
        /// <param name="key">The unique key of the file.</param>
        /// <param name="data">non null data to write</param>
        /// <exception cref="CBaseException">Thrown when data is null</exception>
        public static void WriteData(E_FileDB database, string key, string data)
        {
            if (data == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "Trying to write a null to " + database + " with key " + key);
            }

            byte[] byteData = Encoding.Unicode.GetBytes(data);
            WriteData(database, key, byteData);
        }

        /// <summary>
        /// Writes the given byte data into file DB in the specified database with the specified key.
        /// </summary>
        /// <param name="database">The FileDB database to retrieve the contents of the file from.</param>
        /// <param name="key">The unique key of the file.</param>
        /// <param name="data">byte data</param>
        public static void WriteData(E_FileDB database, string key, byte[] data)
        {
            if (data == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "data cannot be null.");
            }

            WriteFileImp(database, key, filePath => BinaryFileHelper.WriteAllBytes(filePath, data));
        }

        /// <summary>
        /// Writes a file to file db. It passes the opened stream ready for writing to the delegate. The delgate should write all the data into the stream.
        /// </summary>
        /// <param name="database">The FileDB database to retrieve the contents of the file from.</param>
        /// <param name="key">The unique key of the file.</param>
        /// <param name="updateFile"></param>
        public static void WriteData(E_FileDB database, string key, Action<Stream> dstStreamForSaveHandler)
        {
            WriteFileImp(database, key, stream => dstStreamForSaveHandler(stream));
        }

        /// <summary>
        /// Gets a bool indicating whether the given file exist in the database.
        /// </summary>
        /// <param name="database">The FileDB location to check.</param>
        /// <param name="key">The file key.</param>
        /// <returns>True if the file exist false otherwise.</returns>
        public static bool DoesFileExist(E_FileDB database, string key)
        {
            if (ConstStage.UseNewFileDb)
            {
                return DoesFileExistNEW(database, key);
            }
            else
            {
                return DoesFileExistOLD(database, key);
            }
        }

        private static bool DoesFileExistOLD(E_FileDB database, string key)
        {
            FileDB3.FileDB f = new FileDB();
            return f.FileExists(database.GetDatabaseName(), key);
        }

        private static bool DoesFileExistNEW(E_FileDB database, string key)
        {
            string databaseName = GetDatabaseName(database);

            var location = FileStorageIdentifier.TryParse(databaseName);

            if (location == null)
            {
                throw new ArgumentNullException("Bad Database name");
            }

            var fileId = FileIdentifier.TryParse(key);

            if (fileId == null)
            {
                throw new ArgumentNullException("Bad key");
            }

            return LendersOffice.Drivers.FileDB.FileDbHelper.FileExists(location.Value, fileId.Value);
        }
        /// <summary>
        /// Writes an Object as json string to file DB. This will stream write it to the file to avoid building a large in memory string.
        /// </summary>
        /// <typeparam name="T">An object that can be instantiated.</typeparam>
        /// <param name="database">The database to save it to.</param>
        /// <param name="key">The key to save it under.</param>
        /// <param name="obj">The object to serialize.</param>
        public static void WriteJsonNetObject<T>(E_FileDB database, string key, T obj) where T : class, new()
        {
            Action<Stream> saveHandler = delegate(Stream fs)
            {
                using (StreamWriter sw = new StreamWriter(fs))
                using (JsonWriter jw = new JsonTextWriter(sw))
                {
                    var serializer = new Newtonsoft.Json.JsonSerializer();
                    serializer.Serialize(jw, obj);
                }
            };

            WriteFileImp(database, key, saveHandler);
        }

        public static void WriteJsonNetObjectWithTypeHandling<T>(E_FileDB database, string key, T obj) where T : class
        {
            Action<Stream> saveHandler = delegate (Stream fs)
            {
                using (StreamWriter sw = new StreamWriter(fs))
                using (JsonWriter jw = new JsonTextWriter(sw))
                {
                    JsonSerializerSettings settings = SerializationHelper.GetJsonNetSerializerSettings();
                    settings.TypeNameHandling = TypeNameHandling.All;
                    var serializer = Newtonsoft.Json.JsonSerializer.Create(settings);
                    serializer.Serialize(jw, obj);
                }
            };

            WriteFileImp(database, key, saveHandler);
        }

        /// <summary>
        /// Read a file written by <see cref="WriteJsonNetObject"/>. 
        /// </summary>
        /// <typeparam name="T">An object that can be instantiated.</typeparam>
        /// <param name="database">The db that has the file.</param>
        /// <param name="key">The key it was stored under.</param>
        /// <returns>The object populated.</returns>
        public static T ReadJsonNetObject<T>(E_FileDB database, string key) where T : class
        {
            T obj = null;
            Action<Stream> readHandler = delegate(Stream fs)
            {
                var serializer = new Newtonsoft.Json.JsonSerializer();
                using (StreamReader sr = new StreamReader(fs))
                using (JsonTextReader jr = new JsonTextReader(sr))
                {
                    obj = serializer.Deserialize<T>(jr);
                }
            };
            ReadFileImp(database, key, readHandler);
            return obj;
        }

        public static T ReadJsonNetObjectWithTypeHandling<T>(E_FileDB database, string key) where T : class
        {
            T obj = null;
            Action<Stream> readHandler = delegate (Stream fs)
            {
                JsonSerializerSettings settings = SerializationHelper.GetJsonNetSerializerSettings();
                settings.TypeNameHandling = TypeNameHandling.All;
                var serializer = Newtonsoft.Json.JsonSerializer.Create(settings);
                using (StreamReader sr = new StreamReader(fs))
                using (JsonTextReader jr = new JsonTextReader(sr))
                {
                    obj = serializer.Deserialize<T>(jr);
                }
            };
            ReadFileImp(database, key, readHandler);
            return obj;
        }


        /// <summary>
        /// Writes a xml object to file db. It passes the opened xml writer stream ready for writing to the delegate. The delegate should write all the data into the stream.
        /// </summary>
        /// <param name="database">The FileDB database to retrieve the contents of the file from.</param>
        /// <param name="key">The unique key of the file.</param>
        /// <param name="dstStreamForSaveHandler"></param>
        public static void WriteXmlData(E_FileDB database, string key, Action<XmlWriter> dstStreamForSaveHandler)
        {
            WriteXmlFileImpl(database, key, stream => dstStreamForSaveHandler(stream));
        }

        /// <summary>
        /// Copies the file @ srcFileName into database key in filedb.
        /// </summary>
        /// <param name="database">The FileDB database to retrieve the contents of the file from.</param>
        /// <param name="key">The unique key of the file.</param>
        /// <param name="srcFileName">The path of the file to copy into filedb </param>
        /// <exception cref="FileNotFoundException">Thrown when file is not found.</exception>
        /// <exception cref="CBaseException">Thrown when srcFileName is null or empty</exception>
        public static void WriteFile(E_FileDB database, string key, string srcFileName)
        {
            if (string.IsNullOrEmpty(srcFileName))
            {
                throw new CBaseException(ErrorMessages.Generic, "srcFileName cannot be null or empty");
            }

            WriteFileImp(database, key, dstFileName => FileOperationHelper.Copy(srcFileName, dstFileName, true));
        }

        /// <summary>
        /// Allows you to handle the file as a file on disk, useful for some things like eDocs.
        /// Changes to the file will not be saved.
        /// </summary>
        /// <param name="database">The FileDB database to retrieve from</param>
        /// <param name="key">The file's key</param>
        /// <param name="handler">An action to take with the file</param>
        public static T UseFile<T>(E_FileDB database, string key, Func<FileInfo, T> handler)
        {
            if (ConstStage.UseNewFileDb)
            {
                return UseFileNEW(database, key, handler);
            }
            else
            {
                return UseFileOLD(database, key, handler);
            }
        }

        private static T UseFileOLD<T>(E_FileDB database, string key, Func<FileInfo, T> handler)
        {
            FileDB fileDB = new FileDB();
            string databaseName = GetDatabaseName(database);
            Stopwatch sw = Stopwatch.StartNew();

            using (FileHandle fileHandle = fileDB.GetFile(databaseName, key))
            {
                if (fileHandle == null)
                {
                    var exc = new FileNotFoundException("File not in filedb", key);
                    exc.Data.Add("FileDBName", databaseName);
                    exc.Data.Add("FileDBKey", key);
                    // dd 4/17/2018 - OPM 469286 - Add this log to easily parse out the FIleDB key.
                    Tools.LogInfo("FileDBKeyNotFound", $"{databaseName}::{key}");
                    throw exc;
                }

                PerformanceMonitorItem.AddFileDBGetToCurrent(
                    key,
                    databaseName,
                    fileHandle.LocalFileName,
                    sw.ElapsedMilliseconds,
                    "FileDBTools::UseFile");

                return handler(new FileInfo(fileHandle.LocalFileName));
            }
        }

        private static T UseFileNEW<T>(E_FileDB database, string key, Func<FileInfo, T> handler)
        {
            string databaseName = GetDatabaseName(database);

            var location = FileStorageIdentifier.TryParse(databaseName);

            if (location == null)
            {
                throw new ArgumentNullException("Bad Database name");
            }

            var fileId = FileIdentifier.TryParse(key);

            if (fileId == null)
            {
                throw new ArgumentNullException("Bad key");
            }

            T ret = default(T);

            Action<LocalFilePath> readHandler = delegate (LocalFilePath path)
            {
                ret = handler(new FileInfo(path.Value));
            };

            LendersOffice.Drivers.FileDB.FileDbHelper.RetrieveFile(location.Value, fileId.Value, readHandler);

            return ret;
        }

        /// <summary>
        /// Allows you to handle the file as a file on disk, useful for some things like eDocs.
        /// Changes to the file will not be saved.
        /// </summary>
        /// <param name="database">The FileDB database to retrieve from</param>
        /// <param name="key">The file's key</param>
        /// <param name="handler">An action to take with the file</param>
        public static void UseFile(E_FileDB database, string key, Action<FileInfo> handler)
        {
            UseFile(database, key, (info) => { handler(info); return true; });
        }

        /// <summary>
        /// The only method that will call functions from filedb. All calls to filedb go through this.
        /// </summary>
        /// <param name="database">The FileDB database to retrieve the contents of the file from.</param>
        /// <param name="key">The unique key of the file.</param>
        /// <param name="fh">the delgate will be given the path and it will have access to the file until it returns</param>
        /// <exception cref="FileNotFoundException">Thrown when the file is not found at the given location.</exception>
        private static void ReadFileImp(E_FileDB database, string key, Action<string> dstFileNameHandler)
        {
            if (ConstStage.UseNewFileDb)
            {
                ReadFileImpNEW(database, key, dstFileNameHandler);
            }
            else
            {
                ReadFileImpOLD(database, key, dstFileNameHandler);
            }
        }

        private static void ReadFileImpOLD(E_FileDB database, string key, Action<string> dstFileNameHandler)
        {
            FileDbHttpRequestCache cache = FileDbHttpRequestCache.GetCurrent();

            Stopwatch sw = Stopwatch.StartNew();

            string filePath = string.Empty;
            string databaseName = GetDatabaseName(database);

            if (cache.TryGet(database, key, out filePath))
            {
                sw.Stop();
                PerformanceMonitorItem.AddTimingDetailsToCurrent("FileDBTools::ReadFileImp ReadFromCache [" + key + "] [" + databaseName + "]", sw.ElapsedMilliseconds);
                dstFileNameHandler(filePath);
            }
            else
            {
                FileDB fileDB = new FileDB();

                using (FileHandle fileHandle = fileDB.GetFile(databaseName, key))
                {
                    sw.Stop();

                    if (fileHandle == null)
                    {
                        var exc = new FileNotFoundException("File not in filedb", key);
                        exc.Data.Add("FileDBName", databaseName);
                        exc.Data.Add("FileDBKey", key);
                        // dd 4/17/2018 - OPM 469286 - Add this log to easily parse out the FIleDB key.
                        Tools.LogInfo("FileDBKeyNotFound", $"{databaseName}::{key}");
                        throw exc;
                    }

                    PerformanceMonitorItem.AddFileDBGetToCurrent(
                        key,
                        databaseName,
                        fileHandle.LocalFileName,
                        sw.ElapsedMilliseconds,
                        "FileDBTools::ReadFileImp");
                    dstFileNameHandler(fileHandle.LocalFileName);

                    cache.Add(database, key, fileHandle.LocalFileName);
                }
            }
        }

        private static void ReadFileImpNEW(E_FileDB database, string key, Action<string> dstFileNameHandler)
        {
            FileDbHttpRequestCache cache = FileDbHttpRequestCache.GetCurrent();

            Stopwatch sw = Stopwatch.StartNew();

            string filePath = string.Empty;
            string databaseName = GetDatabaseName(database);

            if (cache.TryGet(database, key, out filePath))
            {
                sw.Stop();
                PerformanceMonitorItem.AddTimingDetailsToCurrent("FileDBTools::ReadFileImp ReadFromCache [" + key + "] [" + databaseName + "]", sw.ElapsedMilliseconds);
                dstFileNameHandler(filePath);
            }
            else
            {
                var location = FileStorageIdentifier.TryParse(databaseName);

                if (location == null)
                {
                    throw new ArgumentNullException("Bad Database name");
                }

                var fileId = FileIdentifier.TryParse(key);

                if (fileId == null)
                {
                    throw new ArgumentNullException("Bad key");
                }

                Action<LocalFilePath> readHandler = delegate (LocalFilePath path)
                {
                    dstFileNameHandler(path.Value);

                    cache.Add(database, key, path.Value);
                };

                LendersOffice.Drivers.FileDB.FileDbHelper.RetrieveFile(location.Value, fileId.Value, readHandler);
            }
        }

        /// <summary>
        /// Reads a file from fildb. It creates a stream and calls the handler with the given stream.
        /// </summary>
        /// <param name="database">The FileDB database to retrieve the contents of the file from.</param>
        /// <param name="key">The unique key of the file.</param>
        /// <param name="dstStreamHandler">byte data</param>
        private static void ReadFileImp(E_FileDB database, string key, Action<Stream> dstStreamHandler)
        {
            Action<string> pathToStream = delegate(string path)
            {
                Action<BinaryFileHelper.LqbBinaryStream> readHandler = delegate(BinaryFileHelper.LqbBinaryStream filestream)
                {
                    dstStreamHandler(filestream.Stream);
                };

                BinaryFileHelper.OpenRead(path, readHandler);
            };

            ReadFileImp(database, key, pathToStream);
        }

        const int BYTES_TO_READ = sizeof(Int64);
        const int VERIFY_TIMEOUT_MS = 300000; //5 minutes
        const int VERIFY_SLEEP_MS = 10000; //10 seconds

        private static bool AreFilesTheSame(string path1, string path2)
        {
            FileInfo file1 = new FileInfo(path1);
            FileInfo file2 = new FileInfo(path2);

            if (file1.Length != file2.Length)
            {
                return false;
            }

            int iterations = (int)Math.Ceiling((double)file1.Length / BYTES_TO_READ);

            using (FileStream fs1 = file1.OpenRead())
            using (FileStream fs2 = file2.OpenRead())
            {
                byte[] one = new byte[BYTES_TO_READ];
                byte[] two = new byte[BYTES_TO_READ];

                for (int i = 0; i < iterations; i++)
                {
                    fs1.Read(one, 0, BYTES_TO_READ);
                    fs2.Read(two, 0, BYTES_TO_READ);

                    if (BitConverter.ToInt64(one, 0) != BitConverter.ToInt64(two, 0))
                        return false;
                }
            }

            return true;
        }


        /// <summary>
        /// Writes a file to file db. 
        /// </summary>
        /// <param name="database">The FILEDB db to use</param>
        /// <param name="key">The unique key to store data into</param>
        /// <param name="srcFileNameForSaveHandler">A handler that deals with the string</param>
        private static void WriteFileImp(E_FileDB database, string key, Action<string> srcFileNameForSaveHandler)
        {
            if (ConstStage.UseNewFileDb)
            {
                WriteFileImpNEW(database, key, srcFileNameForSaveHandler);
            }
            else
            {
                WriteFileImpOLD(database, key, srcFileNameForSaveHandler);
            }
        }

        private static void WriteFileImpOLD(E_FileDB database, string key, Action<string> srcFileNameForSaveHandler)
        {
            if (string.IsNullOrEmpty(key))
            {
                throw new CBaseException(ErrorMessages.Generic, "Key FileDBTools.WriteFileImp cannot be null or empty");
            }
            FileDB fileDB = new FileDB();
            Exception sourceException = null;
            string databaseName = GetDatabaseName(database);
            FileDbHttpRequestCache cache = FileDbHttpRequestCache.GetCurrent();
            cache.Remove(database, key);

            using (FileHandle fh = fileDB.NewFileHandle())
            {
                srcFileNameForSaveHandler(fh.LocalFileName);
                Stopwatch sw = Stopwatch.StartNew();
                try
                {
                    fileDB.PutFile(databaseName, key, fh);
                }
                catch (IOException e)
                {
                    sourceException = e;
                }
                sw.Stop();
                PerformanceMonitorItem.AddFileDBPutToCurrent(
                    key,
                    databaseName,
                    fh.LocalFileName,
                    sw.ElapsedMilliseconds,
                    "FileDBTools::WriteFileImp");
                if (sourceException != null)
                {
                    PerformanceStopwatch.TimeAndRun("FileDB.PutFile.VerifyContentsInFileDB", () => VerifyContentsInFileDB(sourceException, database, key, fh.LocalFileName));
                }
                else
                {
                    cache.Add(database, key, fh.LocalFileName);
                }

            }
        }

        private static void WriteFileImpNEW(E_FileDB database, string key, Action<string> srcFileNameForSaveHandler)
        {
            if (string.IsNullOrEmpty(key))
            {
                throw new CBaseException(ErrorMessages.Generic, "Key FileDBTools.WriteFileImp cannot be null or empty");
            }
            FileDB fileDB = new FileDB();

            string databaseName = GetDatabaseName(database);
            FileDbHttpRequestCache cache = FileDbHttpRequestCache.GetCurrent();
            cache.Remove(database, key);

            var location = FileStorageIdentifier.TryParse(databaseName);

            if (location == null)
            {
                throw new ArgumentNullException("Bad Database name");
            }

            var fileId = FileIdentifier.TryParse(key);

            if (fileId == null)
            {
                throw new ArgumentNullException("Bad key");
            }

            Action<LocalFilePath> saveHandler = delegate (LocalFilePath path)
            {
                srcFileNameForSaveHandler(path.Value);
            };

            LendersOffice.Drivers.FileDB.FileDbHelper.SaveNewFile(location.Value, fileId.Value, saveHandler);

            cache.Remove(database, key); // TODO: Replace with cache.Add(database, key, tempFileHandler)
        }

        private static void VerifyContentsInFileDB(Exception sourceException, E_FileDB fileDBName, string fileDBKey, string path1)
        {
            bool success = false;
            Stopwatch sw = new Stopwatch();
            sw.Start();

            do
            {
                try
                {
                    string path2 = FileDBTools.CreateCopy(fileDBName, fileDBKey);
                    PerformanceStopwatch.TimeAndRun("AreFilesTheSame", () => success = FileDBTools.AreFilesTheSame(path1, path2));
                    break;
                }
                catch (IOException e)
                {
                    Tools.LogWarning("[VerifyContentsInFileDB]", e);
                    LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(VERIFY_SLEEP_MS);
                }
            }
            while (sw.ElapsedMilliseconds <= VERIFY_TIMEOUT_MS); 

            if (!success)
            {
                throw sourceException;
            }
        }

        /// <summary>
        ///  Writes a file to file db. It creates a filestream with the file.
        /// </summary>
        /// <param name="database">The FILEDB db to use</param>
        /// <param name="key">The unique key to store data into</param>
        /// <param name="srcStreamForSaveHandler">A handler that does something with the stream</param>
        private static void WriteFileImp(E_FileDB database, string key, Action<Stream> srcStreamForSaveHandler)
        {
            Action<string> saveHandler = delegate(string dstFilePath)
            {
                Action<BinaryFileHelper.LqbBinaryStream> writeHandler = delegate (BinaryFileHelper.LqbBinaryStream fs)
                {
                    srcStreamForSaveHandler(fs.Stream);
                };

                BinaryFileHelper.OpenNew(dstFilePath, writeHandler);
            };

            WriteFileImp(database, key, saveHandler);
        }


        private static void WriteXmlFileImpl(E_FileDB database, string key, Action<XmlWriter> srcStreamForSaveHandler)
        {
            Action<string> saveHandler = delegate(string dstFilePath)
            {
                using (XmlWriter writer = XmlWriter.Create(dstFilePath))
                {
                    srcStreamForSaveHandler(writer);
                }
            };

            WriteFileImp(database, key, saveHandler);
        }
    }
}