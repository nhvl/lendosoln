﻿namespace DataAccess
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Text;
    using DataAccess.PathDispatch;
    using LendersOffice.Common;
    using LendersOffice.Common.Comparers;
    using LendersOffice.CreditReport;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.UI.DataContainers;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;

    public partial class CAppData : IPathResolvable, IHaveReadableDependencies
    {
        private CAppBase m_app;
        private ILoanFileFieldValidator m_validator;
        private readonly bool m_enforceAccessControl = true;

        internal CAppData(CAppBase app, ILoanFileFieldValidator validator, bool enforceAccessControl)
        {
            m_app = app;
            m_validator = validator;
            this.m_enforceAccessControl = enforceAccessControl;
        }

        internal static string GetAppBaseMemberDataName()
        {
            return nameof(CAppData.m_app);
        }

        #region properties

        public bool aRanPricingAfterCreatingApp
        {
            get { return m_app.aRanPricingAfterCreatingApp; }
        }

        public CPageBase LoanData
        {
            get { return m_app.LoanData; }
        }

        public bool ByPassFieldSecurityCheck
        {
            get { return m_app.ByPassFieldSecurityCheck; }
            set { m_app.ByPassFieldSecurityCheck = value; }
        }

        public bool aHasMortgageLia
        {
            get { return m_app.aHasMortgageLia; }
        }

        public string aBCreditAuthorizationD_rep
        {
            get { return this.m_app.aBCreditAuthorizationD_rep; }
            set { this.m_validator.TryToWriteField(nameof(CAppBase.aBCreditAuthorizationD_rep), () => this.m_app.aBCreditAuthorizationD_rep, t => this.m_app.aBCreditAuthorizationD_rep = t, value); }
        }

        public string aCCreditAuthorizationD_rep
        {
            get { return this.m_app.aCCreditAuthorizationD_rep; }
            set { this.m_validator.TryToWriteField(nameof(CAppBase.aCCreditAuthorizationD_rep), () => this.m_app.aCCreditAuthorizationD_rep, t => this.m_app.aCCreditAuthorizationD_rep = t, value); }
        }

        public bool aBCreditReportAuthorizationProvidedVerbally
        {
            get { return this.m_app.aBCreditReportAuthorizationProvidedVerbally; }
            set
            {
                this.m_validator.TryToWriteField(nameof(aBCreditReportAuthorizationProvidedVerbally), () => this.m_app.aBCreditReportAuthorizationProvidedVerbally, t => this.m_app.aBCreditReportAuthorizationProvidedVerbally = t, value);
            }
        }

        public bool aCCreditReportAuthorizationProvidedVerbally
        {
            get { return this.m_app.aCCreditReportAuthorizationProvidedVerbally; }
            set
            {
                this.m_validator.TryToWriteField(nameof(aCCreditReportAuthorizationProvidedVerbally), () => this.m_app.aCCreditReportAuthorizationProvidedVerbally, t => this.m_app.aCCreditReportAuthorizationProvidedVerbally = t, value);
            }
        }

        public bool aBDecisionCreditSourceTLckd
        {
            get { return m_app.aBDecisionCreditSourceTLckd; }
            set
            {
                m_validator.TryToWriteField("aBDecisionCreditSourceTLckd", () => m_app.aBDecisionCreditSourceTLckd, t => m_app.aBDecisionCreditSourceTLckd = t, value);
            }
        }


        public bool aCDecisionCreditSourceTLckd
        {
            get { return m_app.aCDecisionCreditSourceTLckd; }
            set
            {
                m_validator.TryToWriteField("aCDecisionCreditSourceTLckd", () => m_app.aCDecisionCreditSourceTLckd, t => m_app.aCDecisionCreditSourceTLckd = t, value);
            }
        }


        public E_aDecisionCreditSourceT aBDecisionCreditSourceT
        {
            get { return m_app.aBDecisionCreditSourceT; }
            set
            {
                m_validator.TryToWriteField("aBDecisionCreditSourceT", () => m_app.aBDecisionCreditSourceT, t => m_app.aBDecisionCreditSourceT = t, value);
            }
        }


        public E_aDecisionCreditSourceT aCDecisionCreditSourceT
        {
            get { return m_app.aCDecisionCreditSourceT; }
            set
            {
                m_validator.TryToWriteField("aCDecisionCreditSourceT", () => m_app.aCDecisionCreditSourceT, t => m_app.aCDecisionCreditSourceT = t, value);
            }
        }


        public string aBExperianPercentile_rep
        {
            get { return m_app.aBExperianPercentile_rep; }
            set
            {
                m_validator.TryToWriteField("aBExperianPercentile_rep", () => m_app.aBExperianPercentile_rep, t => m_app.aBExperianPercentile_rep = t, value);
            }
        }


        public string aBTransUnionPercentile_rep
        {
            get { return m_app.aBTransUnionPercentile_rep; }
            set
            {
                m_validator.TryToWriteField("aBTransUnionPercentile_rep", () => m_app.aBTransUnionPercentile_rep, t => m_app.aBTransUnionPercentile_rep = t, value);
            }
        }


        public string aBEquifaxPercentile_rep
        {
            get { return m_app.aBEquifaxPercentile_rep; }
            set
            {
                m_validator.TryToWriteField("aBEquifaxPercentile_rep", () => m_app.aBEquifaxPercentile_rep, t => m_app.aBEquifaxPercentile_rep = t, value);
            }
        }

        public E_CreditScoreModelT aBExperianModelT
        {
            get { return this.m_app.aBExperianModelT; }
            set { this.m_validator.TryToWriteField(nameof(aBExperianModelT), () => this.m_app.aBExperianModelT, t => this.m_app.aBExperianModelT = t, value); }
        }

        public string aBExperianModelTOtherDescription
        {
            get { return this.m_app.aBExperianModelTOtherDescription; }
            set { this.m_validator.TryToWriteField(nameof(aBExperianModelTOtherDescription), () => this.m_app.aBExperianModelTOtherDescription, t => this.m_app.aBExperianModelTOtherDescription = t, value); }
        }

        public string aBExperianModelName
        {
            get { return this.m_app.aBExperianModelName; }
            set { this.m_validator.TryToWriteField(nameof(aBExperianModelName), () => this.m_app.aBExperianModelName, t => this.m_app.aBExperianModelName = t, value); }
        }

        public E_CreditScoreModelT aBTransUnionModelT
        {
            get { return this.m_app.aBTransUnionModelT; }
            set { this.m_validator.TryToWriteField(nameof(aBTransUnionModelT), () => this.m_app.aBTransUnionModelT, t => this.m_app.aBTransUnionModelT = t, value); }
        }

        public string aBTransUnionModelTOtherDescription
        {
            get { return this.m_app.aBTransUnionModelTOtherDescription; }
            set { this.m_validator.TryToWriteField(nameof(aBTransUnionModelTOtherDescription), () => this.m_app.aBTransUnionModelTOtherDescription, t => this.m_app.aBTransUnionModelTOtherDescription = t, value); }
        }

        public string aBTransUnionModelName
        {
            get { return this.m_app.aBTransUnionModelName; }
            set { this.m_validator.TryToWriteField(nameof(aBTransUnionModelName), () => this.m_app.aBTransUnionModelName, t => this.m_app.aBTransUnionModelName = t, value); }
        }

        public E_CreditScoreModelT aBEquifaxModelT
        {
            get { return this.m_app.aBEquifaxModelT; }
            set { this.m_validator.TryToWriteField(nameof(aBEquifaxModelT), () => this.m_app.aBEquifaxModelT, t => this.m_app.aBEquifaxModelT = t, value); }
        }

        public string aBEquifaxModelTOtherDescription
        {
            get { return this.m_app.aBEquifaxModelTOtherDescription; }
            set { this.m_validator.TryToWriteField(nameof(aBEquifaxModelTOtherDescription), () => this.m_app.aBEquifaxModelTOtherDescription, t => this.m_app.aBEquifaxModelTOtherDescription = t, value); }
        }

        public string aBEquifaxModelName
        {
            get { return this.m_app.aBEquifaxModelName; }
            set { this.m_validator.TryToWriteField(nameof(aBEquifaxModelName), () => this.m_app.aBEquifaxModelName, t => this.m_app.aBEquifaxModelName = t, value); }
        }


        public string aBOtherCreditModelName
        {
            get { return this.m_app.aBOtherCreditModelName; }
            set
            {
                this.m_validator.TryToWriteField(nameof(aBOtherCreditModelName), () => this.m_app.aBOtherCreditModelName, t => this.m_app.aBOtherCreditModelName = t, value);
            }
        }


        public string aEquifaxPercentile_rep
        {
            get { return m_app.aEquifaxPercentile_rep; }
            set
            {
                m_validator.TryToWriteField("aEquifaxPercentile_rep", () => m_app.aEquifaxPercentile_rep, t => m_app.aEquifaxPercentile_rep = t, value);
            }
        }


        public string aExperianPercentile_rep
        {
            get { return m_app.aExperianPercentile_rep; }
            set
            {
                m_validator.TryToWriteField("aExperianPercentile_rep", () => m_app.aExperianPercentile_rep, t => m_app.aExperianPercentile_rep = t, value);
            }
        }


        public string aTransUnionPercentile_rep
        {
            get { return m_app.aTransUnionPercentile_rep; }
            set
            {
                m_validator.TryToWriteField("aTransUnionPercentile_rep", () => m_app.aTransUnionPercentile_rep, t => m_app.aTransUnionPercentile_rep = t, value);
            }
        }


        public string aBDecisionCreditPercentile_rep
        {
            get { return m_app.aBDecisionCreditPercentile_rep; }
        }


        public string aCDecisionCreditPercentile_rep
        {
            get { return m_app.aCDecisionCreditPercentile_rep; }
        }


        public string aDecisionCreditPercentile_rep
        {
            get { return m_app.aDecisionCreditPercentile_rep; }
        }


        public string aCExperianPercentile_rep
        {
            get { return m_app.aCExperianPercentile_rep; }
            set
            {
                m_validator.TryToWriteField("aCExperianPercentile_rep", () => m_app.aCExperianPercentile_rep, t => m_app.aCExperianPercentile_rep = t, value);
            }
        }


        public string aCTransUnionPercentile_rep
        {
            get { return m_app.aCTransUnionPercentile_rep; }
            set
            {
                m_validator.TryToWriteField("aCTransUnionPercentile_rep", () => m_app.aCTransUnionPercentile_rep, t => m_app.aCTransUnionPercentile_rep = t, value);
            }
        }


        public string aCEquifaxPercentile_rep
        {
            get { return m_app.aCEquifaxPercentile_rep; }
            set
            {
                m_validator.TryToWriteField("aCEquifaxPercentile_rep", () => m_app.aCEquifaxPercentile_rep, t => m_app.aCEquifaxPercentile_rep = t, value);
            }
        }

        public E_CreditScoreModelT aCExperianModelT
        {
            get { return this.m_app.aCExperianModelT; }
            set { this.m_validator.TryToWriteField(nameof(aCExperianModelT), () => this.m_app.aCExperianModelT, t => this.m_app.aCExperianModelT = t, value); }
        }

        public string aCExperianModelTOtherDescription
        {
            get { return this.m_app.aCExperianModelTOtherDescription; }
            set { this.m_validator.TryToWriteField(nameof(aCExperianModelTOtherDescription), () => this.m_app.aCExperianModelTOtherDescription, t => this.m_app.aCExperianModelTOtherDescription = t, value); }
        }

        public string aCExperianModelName
        {
            get { return this.m_app.aCExperianModelName; }
            set { this.m_validator.TryToWriteField(nameof(aCExperianModelName), () => this.m_app.aCExperianModelName, t => this.m_app.aCExperianModelName = t, value); }
        }

        public E_CreditScoreModelT aCTransUnionModelT
        {
            get { return this.m_app.aCTransUnionModelT; }
            set { this.m_validator.TryToWriteField(nameof(aCTransUnionModelT), () => this.m_app.aCTransUnionModelT, t => this.m_app.aCTransUnionModelT = t, value); }
        }

        public string aCTransUnionModelTOtherDescription
        {
            get { return this.m_app.aCTransUnionModelTOtherDescription; }
            set { this.m_validator.TryToWriteField(nameof(aCTransUnionModelTOtherDescription), () => this.m_app.aCTransUnionModelTOtherDescription, t => this.m_app.aCTransUnionModelTOtherDescription = t, value); }
        }

        public string aCTransUnionModelName
        {
            get { return this.m_app.aCTransUnionModelName; }
            set { this.m_validator.TryToWriteField(nameof(aCTransUnionModelName), () => this.m_app.aCTransUnionModelName, t => this.m_app.aCTransUnionModelName = t, value); }
        }

        public E_CreditScoreModelT aCEquifaxModelT
        {
            get { return this.m_app.aCEquifaxModelT; }
            set { this.m_validator.TryToWriteField(nameof(aCEquifaxModelT), () => this.m_app.aCEquifaxModelT, t => this.m_app.aCEquifaxModelT = t, value); }
        }

        public string aCEquifaxModelTOtherDescription
        {
            get { return this.m_app.aCEquifaxModelTOtherDescription; }
            set { this.m_validator.TryToWriteField(nameof(aCEquifaxModelTOtherDescription), () => this.m_app.aCEquifaxModelTOtherDescription, t => this.m_app.aCEquifaxModelTOtherDescription = t, value); }
        }

        public string aCEquifaxModelName
        {
            get { return this.m_app.aCEquifaxModelName; }
            set { this.m_validator.TryToWriteField(nameof(aCEquifaxModelName), () => this.m_app.aCEquifaxModelName, t => this.m_app.aCEquifaxModelName = t, value); }
        }


        public string aCOtherCreditModelName
        {
            get { return this.m_app.aCOtherCreditModelName; }
            set
            {
                this.m_validator.TryToWriteField(nameof(aCOtherCreditModelName), () => this.m_app.aCOtherCreditModelName, t => this.m_app.aCOtherCreditModelName = t, value);
            }
        }


        public CDateTime aBExperianCreatedD
        {
            get { return m_app.aBExperianCreatedD; }
            set
            {
                m_validator.TryToWriteField("aBExperianCreatedD", () => m_app.aBExperianCreatedD, t => m_app.aBExperianCreatedD = t, value);
            }
        }


        public string aBExperianCreatedD_rep
        {
            get { return m_app.aBExperianCreatedD_rep; }
            set
            {
                m_validator.TryToWriteField("aBExperianCreatedD_rep", () => m_app.aBExperianCreatedD_rep, t => m_app.aBExperianCreatedD_rep = t, value);
            }
        }


        public CDateTime aBTransUnionCreatedD
        {
            get { return m_app.aBTransUnionCreatedD; }
            set
            {
                m_validator.TryToWriteField("aBTransUnionCreatedD", () => m_app.aBTransUnionCreatedD, t => m_app.aBTransUnionCreatedD = t, value);
            }
        }


        public string aBTransUnionCreatedD_rep
        {
            get { return m_app.aBTransUnionCreatedD_rep; }
            set
            {
                m_validator.TryToWriteField("aBTransUnionCreatedD_rep", () => m_app.aBTransUnionCreatedD_rep, t => m_app.aBTransUnionCreatedD_rep = t, value);
            }
        }


        public CDateTime aBEquifaxCreatedD
        {
            get { return m_app.aBEquifaxCreatedD; }
            set
            {
                m_validator.TryToWriteField("aBEquifaxCreatedD", () => m_app.aBEquifaxCreatedD, t => m_app.aBEquifaxCreatedD = t, value);
            }
        }


        public string aBEquifaxCreatedD_rep
        {
            get { return m_app.aBEquifaxCreatedD_rep; }
            set
            {
                m_validator.TryToWriteField("aBEquifaxCreatedD_rep", () => m_app.aBEquifaxCreatedD_rep, t => m_app.aBEquifaxCreatedD_rep = t, value);
            }
        }


        public string aBDecisionCreditCreatedD_rep
        {
            get { return m_app.aBDecisionCreditCreatedD_rep; }
        }


        public string aCDecisionCreditCreatedD_rep
        {
            get { return m_app.aCDecisionCreditCreatedD_rep; }
        }


        public string aDecisionCreditCreatedD_rep
        {
            get { return m_app.aDecisionCreditCreatedD_rep; }
        }


        public CDateTime aCExperianCreatedD
        {
            get { return m_app.aCExperianCreatedD; }
            set
            {
                m_validator.TryToWriteField("aCExperianCreatedD", () => m_app.aCExperianCreatedD, t => m_app.aCExperianCreatedD = t, value);
            }
        }


        public string aCExperianCreatedD_rep
        {
            get { return m_app.aCExperianCreatedD_rep; }
            set
            {
                m_validator.TryToWriteField("aCExperianCreatedD_rep", () => m_app.aCExperianCreatedD_rep, t => m_app.aCExperianCreatedD_rep = t, value);
            }
        }


        public CDateTime aCTransUnionCreatedD
        {
            get { return m_app.aCTransUnionCreatedD; }
            set
            {
                m_validator.TryToWriteField("aCTransUnionCreatedD", () => m_app.aCTransUnionCreatedD, t => m_app.aCTransUnionCreatedD = t, value);
            }
        }


        public string aCTransUnionCreatedD_rep
        {
            get { return m_app.aCTransUnionCreatedD_rep; }
            set
            {
                m_validator.TryToWriteField("aCTransUnionCreatedD_rep", () => m_app.aCTransUnionCreatedD_rep, t => m_app.aCTransUnionCreatedD_rep = t, value);
            }
        }


        public CDateTime aCEquifaxCreatedD
        {
            get { return m_app.aCEquifaxCreatedD; }
            set
            {
                m_validator.TryToWriteField("aCEquifaxCreatedD", () => m_app.aCEquifaxCreatedD, t => m_app.aCEquifaxCreatedD = t, value);
            }
        }


        public string aCEquifaxCreatedD_rep
        {
            get { return m_app.aCEquifaxCreatedD_rep; }
            set
            {
                m_validator.TryToWriteField("aCEquifaxCreatedD_rep", () => m_app.aCEquifaxCreatedD_rep, t => m_app.aCEquifaxCreatedD_rep = t, value);
            }
        }


        public string aBExperianFactors
        {
            get { return m_app.aBExperianFactors; }
            set
            {
                m_validator.TryToWriteField("aBExperianFactors", () => m_app.aBExperianFactors, t => m_app.aBExperianFactors = t, value);
            }
        }


        public string aBTransUnionFactors
        {
            get { return m_app.aBTransUnionFactors; }
            set
            {
                m_validator.TryToWriteField("aBTransUnionFactors", () => m_app.aBTransUnionFactors, t => m_app.aBTransUnionFactors = t, value);
            }
        }


        public string aBEquifaxFactors
        {
            get { return m_app.aBEquifaxFactors; }
            set
            {
                m_validator.TryToWriteField("aBEquifaxFactors", () => m_app.aBEquifaxFactors, t => m_app.aBEquifaxFactors = t, value);
            }
        }


        public string aBDecisionCreditFactors
        {
            get { return m_app.aBDecisionCreditFactors; }
        }


        public string aCDecisionCreditFactors
        {
            get { return m_app.aCDecisionCreditFactors; }
        }


        public string aDecisionCreditFactors
        {
            get { return m_app.aDecisionCreditFactors; }
        }


        public string aCExperianFactors
        {
            get { return m_app.aCExperianFactors; }
            set
            {
                m_validator.TryToWriteField("aCExperianFactors", () => m_app.aCExperianFactors, t => m_app.aCExperianFactors = t, value);
            }
        }


        public string aCTransUnionFactors
        {
            get { return m_app.aCTransUnionFactors; }
            set
            {
                m_validator.TryToWriteField("aCTransUnionFactors", () => m_app.aCTransUnionFactors, t => m_app.aCTransUnionFactors = t, value);
            }
        }


        public string aCEquifaxFactors
        {
            get { return m_app.aCEquifaxFactors; }
            set
            {
                m_validator.TryToWriteField("aCEquifaxFactors", () => m_app.aCEquifaxFactors, t => m_app.aCEquifaxFactors = t, value);
            }
        }


        public bool aIsCreditReportOnFile
        {
            get { return m_app.aIsCreditReportOnFile; }
        }


        public bool aIsCreditScoresDifferentWithCreditReport
        {
            get { return m_app.aIsCreditScoresDifferentWithCreditReport; }
        }


        public int aCTransUnionScorePe
        {
            get { return m_app.aCTransUnionScorePe; }
            set
            {
                m_validator.TryToWriteField("aCTransUnionScorePe", () => m_app.aCTransUnionScorePe, t => m_app.aCTransUnionScorePe = t, value);
            }
        }


        public string aCTransUnionScorePe_rep
        {
            get { return m_app.aCTransUnionScorePe_rep; }
            set
            {
                m_validator.TryToWriteField("aCTransUnionScorePe_rep", () => m_app.aCTransUnionScorePe_rep, t => m_app.aCTransUnionScorePe_rep = t, value);
            }
        }


        public int aCExperianScorePe
        {
            get { return m_app.aCExperianScorePe; }
            set
            {
                m_validator.TryToWriteField("aCExperianScorePe", () => m_app.aCExperianScorePe, t => m_app.aCExperianScorePe = t, value);
            }
        }


        public string aCExperianScorePe_rep
        {
            get { return m_app.aCExperianScorePe_rep; }
            set
            {
                m_validator.TryToWriteField("aCExperianScorePe_rep", () => m_app.aCExperianScorePe_rep, t => m_app.aCExperianScorePe_rep = t, value);
            }
        }


        public int aCEquifaxScorePe
        {
            get { return m_app.aCEquifaxScorePe; }
            set
            {
                m_validator.TryToWriteField("aCEquifaxScorePe", () => m_app.aCEquifaxScorePe, t => m_app.aCEquifaxScorePe = t, value);
            }
        }


        public string aCEquifaxScorePe_rep
        {
            get { return m_app.aCEquifaxScorePe_rep; }
            set
            {
                m_validator.TryToWriteField("aCEquifaxScorePe_rep", () => m_app.aCEquifaxScorePe_rep, t => m_app.aCEquifaxScorePe_rep = t, value);
            }
        }


        public int aBTransUnionScorePe
        {
            get { return m_app.aBTransUnionScorePe; }
            set
            {
                m_validator.TryToWriteField("aBTransUnionScorePe", () => m_app.aBTransUnionScorePe, t => m_app.aBTransUnionScorePe = t, value);
            }
        }


        public string aBTransUnionScorePe_rep
        {
            get { return m_app.aBTransUnionScorePe_rep; }
            set
            {
                m_validator.TryToWriteField("aBTransUnionScorePe_rep", () => m_app.aBTransUnionScorePe_rep, t => m_app.aBTransUnionScorePe_rep = t, value);
            }
        }


        public int aBExperianScorePe
        {
            get { return m_app.aBExperianScorePe; }
            set
            {
                m_validator.TryToWriteField("aBExperianScorePe", () => m_app.aBExperianScorePe, t => m_app.aBExperianScorePe = t, value);
            }
        }


        public string aBExperianScorePe_rep
        {
            get { return m_app.aBExperianScorePe_rep; }
            set
            {
                m_validator.TryToWriteField("aBExperianScorePe_rep", () => m_app.aBExperianScorePe_rep, t => m_app.aBExperianScorePe_rep = t, value);
            }
        }


        public int aBEquifaxScorePe
        {
            get { return m_app.aBEquifaxScorePe; }
            set
            {
                m_validator.TryToWriteField("aBEquifaxScorePe", () => m_app.aBEquifaxScorePe, t => m_app.aBEquifaxScorePe = t, value);
            }
        }


        public string aBEquifaxScorePe_rep
        {
            get { return m_app.aBEquifaxScorePe_rep; }
            set
            {
                m_validator.TryToWriteField("aBEquifaxScorePe_rep", () => m_app.aBEquifaxScorePe_rep, t => m_app.aBEquifaxScorePe_rep = t, value);
            }
        }

        public string aBDecisionCreditScore_rep
        {
            get { return m_app.aBDecisionCreditScore_rep; }
            set
            {
                this.m_validator.TryToWriteField(nameof(aBDecisionCreditScore_rep), () => this.m_app.aBDecisionCreditScore_rep, t => this.m_app.aBDecisionCreditScore_rep = t, value);
            }
        }

        public string aBDecisionCreditModelName
        {
            get { return m_app.aBDecisionCreditModelName; }
        }

        public bool aBIsDecisionCreditModelAValidHmdaModel
        {
            get { return m_app.aBIsDecisionCreditModelAValidHmdaModel; }
        }

        public string aCDecisionCreditScore_rep
        {
            get { return m_app.aCDecisionCreditScore_rep; }
            set
            {
                this.m_validator.TryToWriteField(nameof(aCDecisionCreditScore_rep), () => this.m_app.aCDecisionCreditScore_rep, t => this.m_app.aCDecisionCreditScore_rep = t, value);
            }
        }

        public string aCDecisionCreditModelName
        {
            get { return m_app.aCDecisionCreditModelName; }
        }

        public bool aCIsDecisionCreditModelAValidHmdaModel
        {
            get { return m_app.aCIsDecisionCreditModelAValidHmdaModel; }
        }

        public string aDecisionCreditScore_rep
        {
            get { return m_app.aDecisionCreditScore_rep; }
        }

        public string aDecisionCreditModelName
        {
            get { return m_app.aDecisionCreditModelName; }
        }

        public bool aIsDecisionCreditModelAValidHmdaModel
        {
            get { return m_app.aIsDecisionCreditModelAValidHmdaModel; }
        }

        public string aBDecisionCreditScoreFrom_rep
        {
            get { return m_app.aBDecisionCreditScoreFrom_rep; }
        }


        public string aCDecisionCreditScoreFrom_rep
        {
            get { return m_app.aCDecisionCreditScoreFrom_rep; }
        }


        public string aDecisionCreditScoreFrom_rep
        {
            get { return m_app.aDecisionCreditScoreFrom_rep; }
        }


        public string aBDecisionCreditScoreTo_rep
        {
            get { return m_app.aBDecisionCreditScoreTo_rep; }
        }


        public string aCDecisionCreditScoreTo_rep
        {
            get { return m_app.aCDecisionCreditScoreTo_rep; }
        }


        public string aDecisionCreditScoreTo_rep
        {
            get { return m_app.aDecisionCreditScoreTo_rep; }
        }


        public IPreparerFields aBDecisionCraPreparerField
        {
            get { return m_app.aBDecisionCraPreparerField; }
        }


        public IPreparerFields aCDecisionCraPreparerField
        {
            get { return m_app.aCDecisionCraPreparerField; }
        }


        public IPreparerFields aDecisionCraPreparerField
        {
            get { return m_app.aDecisionCraPreparerField; }
        }


        public string aBDecisionCreditSourceName
        {
            get { return m_app.aBDecisionCreditSourceName; }
        }


        public string aCDecisionCreditSourceName
        {
            get { return m_app.aCDecisionCreditSourceName; }
        }

        public E_aDecisionCreditSourceT aDecisionCreditSourceT
        {
            get { return m_app.aDecisionCreditSourceT; }
        }

        public string aDecisionCreditSourceName
        {
            get { return m_app.aDecisionCreditSourceName; }
        }

        public int aProdCrManualNonRolling30MortLateCount
        {
            get { return m_app.aProdCrManualNonRolling30MortLateCount; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualNonRolling30MortLateCount", () => m_app.aProdCrManualNonRolling30MortLateCount, t => m_app.aProdCrManualNonRolling30MortLateCount = t, value);
            }
        }


        public string aProdCrManualNonRolling30MortLateCount_rep
        {
            get { return m_app.aProdCrManualNonRolling30MortLateCount_rep; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualNonRolling30MortLateCount_rep", () => m_app.aProdCrManualNonRolling30MortLateCount_rep, t => m_app.aProdCrManualNonRolling30MortLateCount_rep = t, value);
            }
        }


        public int aProdCrManual60MortLateCount
        {
            get { return m_app.aProdCrManual60MortLateCount; }
            set
            {
                m_validator.TryToWriteField("aProdCrManual60MortLateCount", () => m_app.aProdCrManual60MortLateCount, t => m_app.aProdCrManual60MortLateCount = t, value);
            }
        }


        public string aProdCrManual60MortLateCount_rep
        {
            get { return m_app.aProdCrManual60MortLateCount_rep; }
            set
            {
                m_validator.TryToWriteField("aProdCrManual60MortLateCount_rep", () => m_app.aProdCrManual60MortLateCount_rep, t => m_app.aProdCrManual60MortLateCount_rep = t, value);
            }
        }


        public int aProdCrManual90MortLateCount
        {
            get { return m_app.aProdCrManual90MortLateCount; }
            set
            {
                m_validator.TryToWriteField("aProdCrManual90MortLateCount", () => m_app.aProdCrManual90MortLateCount, t => m_app.aProdCrManual90MortLateCount = t, value);
            }
        }


        public string aProdCrManual90MortLateCount_rep
        {
            get { return m_app.aProdCrManual90MortLateCount_rep; }
            set
            {
                m_validator.TryToWriteField("aProdCrManual90MortLateCount_rep", () => m_app.aProdCrManual90MortLateCount_rep, t => m_app.aProdCrManual90MortLateCount_rep = t, value);
            }
        }


        public int aProdCrManual120MortLateCount
        {
            get { return m_app.aProdCrManual120MortLateCount; }
            set
            {
                m_validator.TryToWriteField("aProdCrManual120MortLateCount", () => m_app.aProdCrManual120MortLateCount, t => m_app.aProdCrManual120MortLateCount = t, value);
            }
        }


        public string aProdCrManual120MortLateCount_rep
        {
            get { return m_app.aProdCrManual120MortLateCount_rep; }
            set
            {
                m_validator.TryToWriteField("aProdCrManual120MortLateCount_rep", () => m_app.aProdCrManual120MortLateCount_rep, t => m_app.aProdCrManual120MortLateCount_rep = t, value);
            }
        }


        public int aProdCrManual150MortLateCount
        {
            get { return m_app.aProdCrManual150MortLateCount; }
            set
            {
                m_validator.TryToWriteField("aProdCrManual150MortLateCount", () => m_app.aProdCrManual150MortLateCount, t => m_app.aProdCrManual150MortLateCount = t, value);
            }
        }


        public string aProdCrManual150MortLateCount_rep
        {
            get { return m_app.aProdCrManual150MortLateCount_rep; }
            set
            {
                m_validator.TryToWriteField("aProdCrManual150MortLateCount_rep", () => m_app.aProdCrManual150MortLateCount_rep, t => m_app.aProdCrManual150MortLateCount_rep = t, value);
            }
        }


        public int aProdCrManual30MortLateCount
        {
            get { return m_app.aProdCrManual30MortLateCount; }
            set
            {
                m_validator.TryToWriteField("aProdCrManual30MortLateCount", () => m_app.aProdCrManual30MortLateCount, t => m_app.aProdCrManual30MortLateCount = t, value);
            }
        }


        public string aProdCrManual30MortLateCount_rep
        {
            get { return m_app.aProdCrManual30MortLateCount_rep; }
            set
            {
                m_validator.TryToWriteField("aProdCrManual30MortLateCount_rep", () => m_app.aProdCrManual30MortLateCount_rep, t => m_app.aProdCrManual30MortLateCount_rep = t, value);
            }
        }


        public int aProdCrManualRolling60MortLateCount
        {
            get { return m_app.aProdCrManualRolling60MortLateCount; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualRolling60MortLateCount", () => m_app.aProdCrManualRolling60MortLateCount, t => m_app.aProdCrManualRolling60MortLateCount = t, value);
            }
        }


        public string aProdCrManualRolling60MortLateCount_rep
        {
            get { return m_app.aProdCrManualRolling60MortLateCount_rep; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualRolling60MortLateCount_rep", () => m_app.aProdCrManualRolling60MortLateCount_rep, t => m_app.aProdCrManualRolling60MortLateCount_rep = t, value);
            }
        }


        public int aProdCrManualRolling90MortLateCount
        {
            get { return m_app.aProdCrManualRolling90MortLateCount; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualRolling90MortLateCount", () => m_app.aProdCrManualRolling90MortLateCount, t => m_app.aProdCrManualRolling90MortLateCount = t, value);
            }
        }


        public string aProdCrManualRolling90MortLateCount_rep
        {
            get { return m_app.aProdCrManualRolling90MortLateCount_rep; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualRolling90MortLateCount_rep", () => m_app.aProdCrManualRolling90MortLateCount_rep, t => m_app.aProdCrManualRolling90MortLateCount_rep = t, value);
            }
        }


        public bool aProdCrManualForeclosureHas
        {
            get { return m_app.aProdCrManualForeclosureHas; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualForeclosureHas", () => m_app.aProdCrManualForeclosureHas, t => m_app.aProdCrManualForeclosureHas = t, value);
            }
        }


        public int aProdCrManualForeclosureRecentFileMon
        {
            get { return m_app.aProdCrManualForeclosureRecentFileMon; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualForeclosureRecentFileMon", () => m_app.aProdCrManualForeclosureRecentFileMon, t => m_app.aProdCrManualForeclosureRecentFileMon = t, value);
            }
        }


        public string aProdCrManualForeclosureRecentFileMon_rep
        {
            get { return m_app.aProdCrManualForeclosureRecentFileMon_rep; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualForeclosureRecentFileMon_rep", () => m_app.aProdCrManualForeclosureRecentFileMon_rep, t => m_app.aProdCrManualForeclosureRecentFileMon_rep = t, value);
            }
        }


        public int aProdCrManualForeclosureRecentFileYr
        {
            get { return m_app.aProdCrManualForeclosureRecentFileYr; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualForeclosureRecentFileYr", () => m_app.aProdCrManualForeclosureRecentFileYr, t => m_app.aProdCrManualForeclosureRecentFileYr = t, value);
            }
        }


        public string aProdCrManualForeclosureRecentFileYr_rep
        {
            get { return m_app.aProdCrManualForeclosureRecentFileYr_rep; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualForeclosureRecentFileYr_rep", () => m_app.aProdCrManualForeclosureRecentFileYr_rep, t => m_app.aProdCrManualForeclosureRecentFileYr_rep = t, value);
            }
        }


        public E_sProdCrManualDerogRecentStatusT aProdCrManualForeclosureRecentStatusT
        {
            get { return m_app.aProdCrManualForeclosureRecentStatusT; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualForeclosureRecentStatusT", () => m_app.aProdCrManualForeclosureRecentStatusT, t => m_app.aProdCrManualForeclosureRecentStatusT = t, value);
            }
        }


        public int aProdCrManualForeclosureRecentSatisfiedMon
        {
            get { return m_app.aProdCrManualForeclosureRecentSatisfiedMon; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualForeclosureRecentSatisfiedMon", () => m_app.aProdCrManualForeclosureRecentSatisfiedMon, t => m_app.aProdCrManualForeclosureRecentSatisfiedMon = t, value);
            }
        }


        public string aProdCrManualForeclosureRecentSatisfiedMon_rep
        {
            get { return m_app.aProdCrManualForeclosureRecentSatisfiedMon_rep; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualForeclosureRecentSatisfiedMon_rep", () => m_app.aProdCrManualForeclosureRecentSatisfiedMon_rep, t => m_app.aProdCrManualForeclosureRecentSatisfiedMon_rep = t, value);
            }
        }


        public int aProdCrManualForeclosureRecentSatisfiedYr
        {
            get { return m_app.aProdCrManualForeclosureRecentSatisfiedYr; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualForeclosureRecentSatisfiedYr", () => m_app.aProdCrManualForeclosureRecentSatisfiedYr, t => m_app.aProdCrManualForeclosureRecentSatisfiedYr = t, value);
            }
        }


        public string aProdCrManualForeclosureRecentSatisfiedYr_rep
        {
            get { return m_app.aProdCrManualForeclosureRecentSatisfiedYr_rep; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualForeclosureRecentSatisfiedYr_rep", () => m_app.aProdCrManualForeclosureRecentSatisfiedYr_rep, t => m_app.aProdCrManualForeclosureRecentSatisfiedYr_rep = t, value);
            }
        }


        public bool aProdCrManualBk7Has
        {
            get { return m_app.aProdCrManualBk7Has; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualBk7Has", () => m_app.aProdCrManualBk7Has, t => m_app.aProdCrManualBk7Has = t, value);
            }
        }


        public int aProdCrManualBk7RecentFileMon
        {
            get { return m_app.aProdCrManualBk7RecentFileMon; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualBk7RecentFileMon", () => m_app.aProdCrManualBk7RecentFileMon, t => m_app.aProdCrManualBk7RecentFileMon = t, value);
            }
        }


        public string aProdCrManualBk7RecentFileMon_rep
        {
            get { return m_app.aProdCrManualBk7RecentFileMon_rep; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualBk7RecentFileMon_rep", () => m_app.aProdCrManualBk7RecentFileMon_rep, t => m_app.aProdCrManualBk7RecentFileMon_rep = t, value);
            }
        }


        public int aProdCrManualBk7RecentFileYr
        {
            get { return m_app.aProdCrManualBk7RecentFileYr; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualBk7RecentFileYr", () => m_app.aProdCrManualBk7RecentFileYr, t => m_app.aProdCrManualBk7RecentFileYr = t, value);
            }
        }


        public string aProdCrManualBk7RecentFileYr_rep
        {
            get { return m_app.aProdCrManualBk7RecentFileYr_rep; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualBk7RecentFileYr_rep", () => m_app.aProdCrManualBk7RecentFileYr_rep, t => m_app.aProdCrManualBk7RecentFileYr_rep = t, value);
            }
        }


        public E_sProdCrManualDerogRecentStatusT aProdCrManualBk7RecentStatusT
        {
            get { return m_app.aProdCrManualBk7RecentStatusT; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualBk7RecentStatusT", () => m_app.aProdCrManualBk7RecentStatusT, t => m_app.aProdCrManualBk7RecentStatusT = t, value);
            }
        }


        public int aProdCrManualBk7RecentSatisfiedMon
        {
            get { return m_app.aProdCrManualBk7RecentSatisfiedMon; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualBk7RecentSatisfiedMon", () => m_app.aProdCrManualBk7RecentSatisfiedMon, t => m_app.aProdCrManualBk7RecentSatisfiedMon = t, value);
            }
        }


        public string aProdCrManualBk7RecentSatisfiedMon_rep
        {
            get { return m_app.aProdCrManualBk7RecentSatisfiedMon_rep; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualBk7RecentSatisfiedMon_rep", () => m_app.aProdCrManualBk7RecentSatisfiedMon_rep, t => m_app.aProdCrManualBk7RecentSatisfiedMon_rep = t, value);
            }
        }


        public int aProdCrManualBk7RecentSatisfiedYr
        {
            get { return m_app.aProdCrManualBk7RecentSatisfiedYr; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualBk7RecentSatisfiedYr", () => m_app.aProdCrManualBk7RecentSatisfiedYr, t => m_app.aProdCrManualBk7RecentSatisfiedYr = t, value);
            }
        }


        public string aProdCrManualBk7RecentSatisfiedYr_rep
        {
            get { return m_app.aProdCrManualBk7RecentSatisfiedYr_rep; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualBk7RecentSatisfiedYr_rep", () => m_app.aProdCrManualBk7RecentSatisfiedYr_rep, t => m_app.aProdCrManualBk7RecentSatisfiedYr_rep = t, value);
            }
        }


        public bool aProdCrManualBk13Has
        {
            get { return m_app.aProdCrManualBk13Has; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualBk13Has", () => m_app.aProdCrManualBk13Has, t => m_app.aProdCrManualBk13Has = t, value);
            }
        }


        public int aProdCrManualBk13RecentFileMon
        {
            get { return m_app.aProdCrManualBk13RecentFileMon; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualBk13RecentFileMon", () => m_app.aProdCrManualBk13RecentFileMon, t => m_app.aProdCrManualBk13RecentFileMon = t, value);
            }
        }


        public string aProdCrManualBk13RecentFileMon_rep
        {
            get { return m_app.aProdCrManualBk13RecentFileMon_rep; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualBk13RecentFileMon_rep", () => m_app.aProdCrManualBk13RecentFileMon_rep, t => m_app.aProdCrManualBk13RecentFileMon_rep = t, value);
            }
        }


        public int aProdCrManualBk13RecentFileYr
        {
            get { return m_app.aProdCrManualBk13RecentFileYr; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualBk13RecentFileYr", () => m_app.aProdCrManualBk13RecentFileYr, t => m_app.aProdCrManualBk13RecentFileYr = t, value);
            }
        }


        public string aProdCrManualBk13RecentFileYr_rep
        {
            get { return m_app.aProdCrManualBk13RecentFileYr_rep; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualBk13RecentFileYr_rep", () => m_app.aProdCrManualBk13RecentFileYr_rep, t => m_app.aProdCrManualBk13RecentFileYr_rep = t, value);
            }
        }


        public E_sProdCrManualDerogRecentStatusT aProdCrManualBk13RecentStatusT
        {
            get { return m_app.aProdCrManualBk13RecentStatusT; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualBk13RecentStatusT", () => m_app.aProdCrManualBk13RecentStatusT, t => m_app.aProdCrManualBk13RecentStatusT = t, value);
            }
        }


        public int aProdCrManualBk13RecentSatisfiedMon
        {
            get { return m_app.aProdCrManualBk13RecentSatisfiedMon; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualBk13RecentSatisfiedMon", () => m_app.aProdCrManualBk13RecentSatisfiedMon, t => m_app.aProdCrManualBk13RecentSatisfiedMon = t, value);
            }
        }


        public string aProdCrManualBk13RecentSatisfiedMon_rep
        {
            get { return m_app.aProdCrManualBk13RecentSatisfiedMon_rep; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualBk13RecentSatisfiedMon_rep", () => m_app.aProdCrManualBk13RecentSatisfiedMon_rep, t => m_app.aProdCrManualBk13RecentSatisfiedMon_rep = t, value);
            }
        }


        public int aProdCrManualBk13RecentSatisfiedYr
        {
            get { return m_app.aProdCrManualBk13RecentSatisfiedYr; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualBk13RecentSatisfiedYr", () => m_app.aProdCrManualBk13RecentSatisfiedYr, t => m_app.aProdCrManualBk13RecentSatisfiedYr = t, value);
            }
        }


        public string aProdCrManualBk13RecentSatisfiedYr_rep
        {
            get { return m_app.aProdCrManualBk13RecentSatisfiedYr_rep; }
            set
            {
                m_validator.TryToWriteField("aProdCrManualBk13RecentSatisfiedYr_rep", () => m_app.aProdCrManualBk13RecentSatisfiedYr_rep, t => m_app.aProdCrManualBk13RecentSatisfiedYr_rep = t, value);
            }
        }


        public E_BkType aProdCrManualMostRecentBkT
        {
            get { return m_app.aProdCrManualMostRecentBkT; }
        }


        public string aProdCrManualMostRecentFileMon_rep
        {
            get { return m_app.aProdCrManualMostRecentFileMon_rep; }
        }


        public string aProdCrManualMostRecentFileYr_rep
        {
            get { return m_app.aProdCrManualMostRecentFileYr_rep; }
        }


        public E_sProdCrManualDerogRecentStatusT aProdCrManualMostRecentStatusT
        {
            get { return m_app.aProdCrManualMostRecentStatusT; }
        }


        public string aProdCrManualMostRecentSatisfiedMon_rep
        {
            get { return m_app.aProdCrManualMostRecentSatisfiedMon_rep; }
        }


        public string aProdCrManualMostRecentSatisfiedYr_rep
        {
            get { return m_app.aProdCrManualMostRecentSatisfiedYr_rep; }
        }


        public decimal aTransmOMonPmtPeval
        {
            get { return m_app.aTransmOMonPmtPeval; }
            set
            {
                m_validator.TryToWriteField("aTransmOMonPmtPeval", () => m_app.aTransmOMonPmtPeval, t => m_app.aTransmOMonPmtPeval = t, value);
            }
        }


        public decimal aTransmOMonPmtPe
        {
            get { return m_app.aTransmOMonPmtPe; }
            set
            {
                m_validator.TryToWriteField("aTransmOMonPmtPe", () => m_app.aTransmOMonPmtPe, t => m_app.aTransmOMonPmtPe = t, value);
            }
        }


        public string aTransmOMonPmtPe_rep
        {
            get { return m_app.aTransmOMonPmtPe_rep; }
            set
            {
                m_validator.TryToWriteField("aTransmOMonPmtPe_rep", () => m_app.aTransmOMonPmtPe_rep, t => m_app.aTransmOMonPmtPe_rep = t, value);
            }
        }


        public decimal aOpNegCfPeval
        {
            get { return m_app.aOpNegCfPeval; }
            set
            {
                m_validator.TryToWriteField("aOpNegCfPeval", () => m_app.aOpNegCfPeval, t => m_app.aOpNegCfPeval = t, value);
            }
        }


        public decimal aOpNegCfPe
        {
            get { return m_app.aOpNegCfPe; }
            set
            {
                m_validator.TryToWriteField("aOpNegCfPe", () => m_app.aOpNegCfPe, t => m_app.aOpNegCfPe = t, value);
            }
        }


        public string aOpNegCfPe_rep
        {
            get { return m_app.aOpNegCfPe_rep; }
            set
            {
                m_validator.TryToWriteField("aOpNegCfPe_rep", () => m_app.aOpNegCfPe_rep, t => m_app.aOpNegCfPe_rep = t, value);
            }
        }

        public bool aLiquidAssetsExists
        {
            get { return m_app.aLiquidAssetsExists; }
            set
            {
                m_validator.TryToWriteField("aLiquidAssetsExists", () => m_app.aLiquidAssetsExists, t => m_app.aLiquidAssetsExists = t, value);
            }
        }

        public decimal aLiquidAssetsPe
        {
            get { return m_app.aLiquidAssetsPe; }
            set
            {
                m_validator.TryToWriteField("aLiquidAssetsPe", () => m_app.aLiquidAssetsPe, t => m_app.aLiquidAssetsPe = t, value);
            }
        }


        public string aLiquidAssetsPe_rep
        {
            get { return m_app.aLiquidAssetsPe_rep; }
            set
            {
                m_validator.TryToWriteField("aLiquidAssetsPe_rep", () => m_app.aLiquidAssetsPe_rep, t => m_app.aLiquidAssetsPe_rep = t, value);
            }
        }


        public decimal aPresOHExpPeval
        {
            get { return m_app.aPresOHExpPeval; }
            set
            {
                m_validator.TryToWriteField("aPresOHExpPeval", () => m_app.aPresOHExpPeval, t => m_app.aPresOHExpPeval = t, value);
            }
        }


        public decimal aPresOHExpPe
        {
            get { return m_app.aPresOHExpPe; }
            set
            {
                m_validator.TryToWriteField("aPresOHExpPe", () => m_app.aPresOHExpPe, t => m_app.aPresOHExpPe = t, value);
            }
        }


        public string aPresOHExpPe_rep
        {
            get { return m_app.aPresOHExpPe_rep; }
            set
            {
                m_validator.TryToWriteField("aPresOHExpPe_rep", () => m_app.aPresOHExpPe_rep, t => m_app.aPresOHExpPe_rep = t, value);
            }
        }


        public bool aIsBorrSpousePrimaryWageEarnerPelckd
        {
            get { return m_app.aIsBorrSpousePrimaryWageEarnerPelckd; }
            set
            {
                m_validator.TryToWriteField("aIsBorrSpousePrimaryWageEarnerPelckd", () => m_app.aIsBorrSpousePrimaryWageEarnerPelckd, t => m_app.aIsBorrSpousePrimaryWageEarnerPelckd = t, value);
            }
        }


        public bool aIsBorrSpousePrimaryWageEarnerPe
        {
            get { return m_app.aIsBorrSpousePrimaryWageEarnerPe; }
            set
            {
                m_validator.TryToWriteField("aIsBorrSpousePrimaryWageEarnerPe", () => m_app.aIsBorrSpousePrimaryWageEarnerPe, t => m_app.aIsBorrSpousePrimaryWageEarnerPe = t, value);
            }
        }


        public bool aOccTPelckd
        {
            get { return m_app.aOccTPelckd; }
        }


        public E_TriState aFHABorrCertOwnOrSoldOtherFHAPropTri
        {
            get { return m_app.aFHABorrCertOwnOrSoldOtherFHAPropTri; }
            set
            {
                m_validator.TryToWriteField("aFHABorrCertOwnOrSoldOtherFHAPropTri", () => m_app.aFHABorrCertOwnOrSoldOtherFHAPropTri, t => m_app.aFHABorrCertOwnOrSoldOtherFHAPropTri = t, value);
            }
        }


        public E_TriState aVaBorrCertHadVaLoanTri
        {
            get { return m_app.aVaBorrCertHadVaLoanTri; }
            set
            {
                m_validator.TryToWriteField("aVaBorrCertHadVaLoanTri", () => m_app.aVaBorrCertHadVaLoanTri, t => m_app.aVaBorrCertHadVaLoanTri = t, value);
            }
        }


        public bool aFHABorrCertOccIsAsHome
        {
            get { return m_app.aFHABorrCertOccIsAsHome; }
            set
            {
                m_validator.TryToWriteField("aFHABorrCertOccIsAsHome", () => m_app.aFHABorrCertOccIsAsHome, t => m_app.aFHABorrCertOccIsAsHome = t, value);
            }
        }


        public bool aFHABorrCertOccIsAsHomeForActiveSpouse
        {
            get { return m_app.aFHABorrCertOccIsAsHomeForActiveSpouse; }
            set
            {
                m_validator.TryToWriteField("aFHABorrCertOccIsAsHomeForActiveSpouse", () => m_app.aFHABorrCertOccIsAsHomeForActiveSpouse, t => m_app.aFHABorrCertOccIsAsHomeForActiveSpouse = t, value);
            }
        }


        public bool aFHABorrCertOccIsAsHomePrev
        {
            get { return m_app.aFHABorrCertOccIsAsHomePrev; }
            set
            {
                m_validator.TryToWriteField("aFHABorrCertOccIsAsHomePrev", () => m_app.aFHABorrCertOccIsAsHomePrev, t => m_app.aFHABorrCertOccIsAsHomePrev = t, value);
            }
        }


        public bool aFHABorrCertOccIsAsHomePrevForActiveSpouse
        {
            get { return m_app.aFHABorrCertOccIsAsHomePrevForActiveSpouse; }
            set
            {
                m_validator.TryToWriteField("aFHABorrCertOccIsAsHomePrevForActiveSpouse", () => m_app.aFHABorrCertOccIsAsHomePrevForActiveSpouse, t => m_app.aFHABorrCertOccIsAsHomePrevForActiveSpouse = t, value);
            }
        }

        public bool aFHABorrCertOccIsChildAsHome
        {
            get { return m_app.aFHABorrCertOccIsChildAsHome; }
            set
            {
                m_validator.TryToWriteField("aFHABorrCertOccIsChildAsHome", () => m_app.aFHABorrCertOccIsChildAsHome, t => m_app.aFHABorrCertOccIsChildAsHome = t, value);
            }
        }

        public bool aFHABorrCertOccIsChildAsHomePrev
        {
            get { return m_app.aFHABorrCertOccIsChildAsHomePrev; }
            set
            {
                m_validator.TryToWriteField("aFHABorrCertOccIsChildAsHomePrev", () => m_app.aFHABorrCertOccIsChildAsHomePrev, t => m_app.aFHABorrCertOccIsChildAsHomePrev = t, value);
            }
        }


        public E_TriState aHas1stTimeBuyerTri
        {
            get { return m_app.aHas1stTimeBuyerTri; }
            set
            {
                m_validator.TryToWriteField("aHas1stTimeBuyerTri", () => m_app.aHas1stTimeBuyerTri, t => m_app.aHas1stTimeBuyerTri = t, value);
            }
        }


        public E_TriState aFHABorrCertOtherPropToBeSoldTri
        {
            get { return m_app.aFHABorrCertOtherPropToBeSoldTri; }
            set
            {
                m_validator.TryToWriteField("aFHABorrCertOtherPropToBeSoldTri", () => m_app.aFHABorrCertOtherPropToBeSoldTri, t => m_app.aFHABorrCertOtherPropToBeSoldTri = t, value);
            }
        }

        public bool aFhaBorrCertOtherProptoBeSoldNA
        {
            get { return m_app.aFhaBorrCertOtherPropToBeSoldNA; }
        }

        public E_TriState aFhaBorrCertWillOccupyFor1YearTri
        {
            get { return m_app.aFhaBorrCertWillOccupyFor1YearTri; }
        }


        public decimal aFHABorrCertOtherPropSalesPrice
        {
            get { return m_app.aFHABorrCertOtherPropSalesPrice; }
            set
            {
                m_validator.TryToWriteField("aFHABorrCertOtherPropSalesPrice", () => m_app.aFHABorrCertOtherPropSalesPrice, t => m_app.aFHABorrCertOtherPropSalesPrice = t, value);
            }
        }


        public string aFHABorrCertOtherPropSalesPrice_rep
        {
            get { return m_app.aFHABorrCertOtherPropSalesPrice_rep; }
            set
            {
                m_validator.TryToWriteField("aFHABorrCertOtherPropSalesPrice_rep", () => m_app.aFHABorrCertOtherPropSalesPrice_rep, t => m_app.aFHABorrCertOtherPropSalesPrice_rep = t, value);
            }
        }


        public decimal aFHABorrCertOtherPropOrigMAmt
        {
            get { return m_app.aFHABorrCertOtherPropOrigMAmt; }
            set
            {
                m_validator.TryToWriteField("aFHABorrCertOtherPropOrigMAmt", () => m_app.aFHABorrCertOtherPropOrigMAmt, t => m_app.aFHABorrCertOtherPropOrigMAmt = t, value);
            }
        }


        public string aFHABorrCertOtherPropOrigMAmt_rep
        {
            get { return m_app.aFHABorrCertOtherPropOrigMAmt_rep; }
            set
            {
                m_validator.TryToWriteField("aFHABorrCertOtherPropOrigMAmt_rep", () => m_app.aFHABorrCertOtherPropOrigMAmt_rep, t => m_app.aFHABorrCertOtherPropOrigMAmt_rep = t, value);
            }
        }


        public string aFHABorrCertOtherPropStAddr
        {
            get { return m_app.aFHABorrCertOtherPropStAddr; }
            set
            {
                m_validator.TryToWriteField("aFHABorrCertOtherPropStAddr", () => m_app.aFHABorrCertOtherPropStAddr, t => m_app.aFHABorrCertOtherPropStAddr = t, value);
            }
        }


        public string aFHABorrCertOtherPropCity
        {
            get { return m_app.aFHABorrCertOtherPropCity; }
            set
            {
                m_validator.TryToWriteField("aFHABorrCertOtherPropCity", () => m_app.aFHABorrCertOtherPropCity, t => m_app.aFHABorrCertOtherPropCity = t, value);
            }
        }


        public string aFHABorrCertOtherPropState
        {
            get { return m_app.aFHABorrCertOtherPropState; }
            set
            {
                m_validator.TryToWriteField("aFHABorrCertOtherPropState", () => m_app.aFHABorrCertOtherPropState, t => m_app.aFHABorrCertOtherPropState = t, value);
            }
        }


        public string aFHABorrCertOtherPropZip
        {
            get { return m_app.aFHABorrCertOtherPropZip; }
            set
            {
                m_validator.TryToWriteField("aFHABorrCertOtherPropZip", () => m_app.aFHABorrCertOtherPropZip, t => m_app.aFHABorrCertOtherPropZip = t, value);
            }
        }


        public E_TriState aFHABorrCertOtherPropCoveredByThisLoanTri
        {
            get { return m_app.aFHABorrCertOtherPropCoveredByThisLoanTri; }
            set
            {
                m_validator.TryToWriteField("aFHABorrCertOtherPropCoveredByThisLoanTri", () => m_app.aFHABorrCertOtherPropCoveredByThisLoanTri, t => m_app.aFHABorrCertOtherPropCoveredByThisLoanTri = t, value);
            }
        }


        public E_TriState aFHABorrCertOwnMoreThan4DwellingsTri
        {
            get { return m_app.aFHABorrCertOwnMoreThan4DwellingsTri; }
            set
            {
                m_validator.TryToWriteField("aFHABorrCertOwnMoreThan4DwellingsTri", () => m_app.aFHABorrCertOwnMoreThan4DwellingsTri, t => m_app.aFHABorrCertOwnMoreThan4DwellingsTri = t, value);
            }
        }


        public decimal aFHABorrCertInformedPropVal
        {
            get { return m_app.aFHABorrCertInformedPropVal; }
            set
            {
                m_validator.TryToWriteField("aFHABorrCertInformedPropVal", () => m_app.aFHABorrCertInformedPropVal, t => m_app.aFHABorrCertInformedPropVal = t, value);
            }
        }


        public bool aFHABorrCertInformedPropValDeterminedByVA
        {
            get { return m_app.aFHABorrCertInformedPropValDeterminedByVA; }
            set
            {
                m_validator.TryToWriteField("aFHABorrCertInformedPropValDeterminedByVA", () => m_app.aFHABorrCertInformedPropValDeterminedByVA, t => m_app.aFHABorrCertInformedPropValDeterminedByVA = t, value);
            }
        }


        public bool aFHABorrCertInformedPropValDeterminedByHUD
        {
            get { return m_app.aFHABorrCertInformedPropValDeterminedByHUD; }
            set
            {
                m_validator.TryToWriteField("aFHABorrCertInformedPropValDeterminedByHUD", () => m_app.aFHABorrCertInformedPropValDeterminedByHUD, t => m_app.aFHABorrCertInformedPropValDeterminedByHUD = t, value);
            }
        }


        public string aFHABorrCertInformedPropVal_rep
        {
            get { return m_app.aFHABorrCertInformedPropVal_rep; }
            set
            {
                m_validator.TryToWriteField("aFHABorrCertInformedPropVal_rep", () => m_app.aFHABorrCertInformedPropVal_rep, t => m_app.aFHABorrCertInformedPropVal_rep = t, value);
            }
        }


        public bool aFHABorrCertInformedPropValAwareAtContractSigning
        {
            get { return m_app.aFHABorrCertInformedPropValAwareAtContractSigning; }
            set
            {
                m_validator.TryToWriteField("aFHABorrCertInformedPropValAwareAtContractSigning", () => m_app.aFHABorrCertInformedPropValAwareAtContractSigning, t => m_app.aFHABorrCertInformedPropValAwareAtContractSigning = t, value);
            }
        }


        public bool aFHABorrCertInformedPropValNotAwareAtContractSigning
        {
            get { return m_app.aFHABorrCertInformedPropValNotAwareAtContractSigning; }
            set
            {
                m_validator.TryToWriteField("aFHABorrCertInformedPropValNotAwareAtContractSigning", () => m_app.aFHABorrCertInformedPropValNotAwareAtContractSigning, t => m_app.aFHABorrCertInformedPropValNotAwareAtContractSigning = t, value);
            }
        }


        public E_TriState aFHABorrCertReceivedLeadPaintPoisonInfoTri
        {
            get { return m_app.aFHABorrCertReceivedLeadPaintPoisonInfoTri; }
            set
            {
                m_validator.TryToWriteField("aFHABorrCertReceivedLeadPaintPoisonInfoTri", () => m_app.aFHABorrCertReceivedLeadPaintPoisonInfoTri, t => m_app.aFHABorrCertReceivedLeadPaintPoisonInfoTri = t, value);
            }
        }


        public string aVaClaimFolderNum
        {
            get { return m_app.aVaClaimFolderNum; }
            set
            {
                m_validator.TryToWriteField("aVaClaimFolderNum", () => m_app.aVaClaimFolderNum, t => m_app.aVaClaimFolderNum = t, value);
            }
        }


        public string aVaServiceNum
        {
            get { return m_app.aVaServiceNum; }
            set
            {
                m_validator.TryToWriteField("aVaServiceNum", () => m_app.aVaServiceNum, t => m_app.aVaServiceNum = t, value);
            }
        }


        public E_TriState aVaIndebtCertifyTri
        {
            get { return m_app.aVaIndebtCertifyTri; }
            set
            {
                m_validator.TryToWriteField("aVaIndebtCertifyTri", () => m_app.aVaIndebtCertifyTri, t => m_app.aVaIndebtCertifyTri = t, value);
            }
        }


        public string aVaEntitleCode
        {
            get { return m_app.aVaEntitleCode; }
            set
            {
                m_validator.TryToWriteField("aVaEntitleCode", () => m_app.aVaEntitleCode, t => m_app.aVaEntitleCode = t, value);
            }
        }


        public decimal aVaEntitleAmt
        {
            get { return m_app.aVaEntitleAmt; }
            set
            {
                m_validator.TryToWriteField("aVaEntitleAmt", () => m_app.aVaEntitleAmt, t => m_app.aVaEntitleAmt = t, value);
            }
        }


        public string aVaEntitleAmt_rep
        {
            get { return m_app.aVaEntitleAmt_rep; }
            set
            {
                m_validator.TryToWriteField("aVaEntitleAmt_rep", () => m_app.aVaEntitleAmt_rep, t => m_app.aVaEntitleAmt_rep = t, value);
            }
        }


        public E_aVaServiceBranchT aVaServiceBranchT
        {
            get { return m_app.aVaServiceBranchT; }
            set
            {
                m_validator.TryToWriteField("aVaServiceBranchT", () => m_app.aVaServiceBranchT, t => m_app.aVaServiceBranchT = t, value);
            }
        }


        public E_aVaMilitaryStatT aVaMilitaryStatT
        {
            get { return m_app.aVaMilitaryStatT; }
            set
            {
                m_validator.TryToWriteField("aVaMilitaryStatT", () => m_app.aVaMilitaryStatT, t => m_app.aVaMilitaryStatT = t, value);
            }
        }


        public E_TriState aVaIsVeteranFirstTimeBuyerTri
        {
            get { return m_app.aVaIsVeteranFirstTimeBuyerTri; }
            set
            {
                m_validator.TryToWriteField("aVaIsVeteranFirstTimeBuyerTri", () => m_app.aVaIsVeteranFirstTimeBuyerTri, t => m_app.aVaIsVeteranFirstTimeBuyerTri = t, value);
            }
        }


        public decimal aVaTotMonGrossI
        {
            get { return m_app.aVaTotMonGrossI; }
        }


        public string aVaTotMonGrossI_rep
        {
            get { return m_app.aVaTotMonGrossI_rep; }
        }


        public bool aVaIsCoborIConsidered
        {
            get { return m_app.aVaIsCoborIConsidered; }
        }


        public decimal aVaCEmplmtI
        {
            get { return m_app.aVaCEmplmtI; }
            set
            {
                m_validator.TryToWriteField("aVaCEmplmtI", () => m_app.aVaCEmplmtI, t => m_app.aVaCEmplmtI = t, value);
            }
        }


        public string aVaCEmplmtI_rep
        {
            get { return m_app.aVaCEmplmtI_rep; }
            set
            {
                m_validator.TryToWriteField("aVaCEmplmtI_rep", () => m_app.aVaCEmplmtI_rep, t => m_app.aVaCEmplmtI_rep = t, value);
            }
        }


        public bool aVaCEmplmtILckd
        {
            get { return m_app.aVaCEmplmtILckd; }
            set
            {
                m_validator.TryToWriteField("aVaCEmplmtILckd", () => m_app.aVaCEmplmtILckd, t => m_app.aVaCEmplmtILckd = t, value);
            }
        }


        public decimal aVaCFedITax
        {
            get { return m_app.aVaCFedITax; }
            set
            {
                m_validator.TryToWriteField("aVaCFedITax", () => m_app.aVaCFedITax, t => m_app.aVaCFedITax = t, value);
            }
        }


        public string aVaCFedITax_rep
        {
            get { return m_app.aVaCFedITax_rep; }
            set
            {
                m_validator.TryToWriteField("aVaCFedITax_rep", () => m_app.aVaCFedITax_rep, t => m_app.aVaCFedITax_rep = t, value);
            }
        }


        public decimal aVaCStateITax
        {
            get { return m_app.aVaCStateITax; }
            set
            {
                m_validator.TryToWriteField("aVaCStateITax", () => m_app.aVaCStateITax, t => m_app.aVaCStateITax = t, value);
            }
        }


        public string aVaCStateITax_rep
        {
            get { return m_app.aVaCStateITax_rep; }
            set
            {
                m_validator.TryToWriteField("aVaCStateITax_rep", () => m_app.aVaCStateITax_rep, t => m_app.aVaCStateITax_rep = t, value);
            }
        }


        public decimal aVaCSsnTax
        {
            get { return m_app.aVaCSsnTax; }
            set
            {
                m_validator.TryToWriteField("aVaCSsnTax", () => m_app.aVaCSsnTax, t => m_app.aVaCSsnTax = t, value);
            }
        }


        public string aVaCSsnTax_rep
        {
            get { return m_app.aVaCSsnTax_rep; }
            set
            {
                m_validator.TryToWriteField("aVaCSsnTax_rep", () => m_app.aVaCSsnTax_rep, t => m_app.aVaCSsnTax_rep = t, value);
            }
        }


        public decimal aVaCOITax
        {
            get { return m_app.aVaCOITax; }
            set
            {
                m_validator.TryToWriteField("aVaCOITax", () => m_app.aVaCOITax, t => m_app.aVaCOITax = t, value);
            }
        }


        public string aVaCOITax_rep
        {
            get { return m_app.aVaCOITax_rep; }
            set
            {
                m_validator.TryToWriteField("aVaCOITax_rep", () => m_app.aVaCOITax_rep, t => m_app.aVaCOITax_rep = t, value);
            }
        }


        public decimal aVaCTotITax
        {
            get { return m_app.aVaCTotITax; }
        }


        public string aVaCTotITax_rep
        {
            get { return m_app.aVaCTotITax_rep; }
        }


        public decimal aVaCTakehomeEmplmtI
        {
            get { return m_app.aVaCTakehomeEmplmtI; }
        }


        public string aVaCTakehomeEmplmtI_rep
        {
            get { return m_app.aVaCTakehomeEmplmtI_rep; }
        }


        public decimal aVaCONetI
        {
            get { return m_app.aVaCONetI; }
            set
            {
                m_validator.TryToWriteField("aVaCONetI", () => m_app.aVaCONetI, t => m_app.aVaCONetI = t, value);
            }
        }


        public string aVaCONetI_rep
        {
            get { return m_app.aVaCONetI_rep; }
            set
            {
                m_validator.TryToWriteField("aVaCONetI_rep", () => m_app.aVaCONetI_rep, t => m_app.aVaCONetI_rep = t, value);
            }
        }


        public bool aVaCONetILckd
        {
            get { return m_app.aVaCONetILckd; }
            set
            {
                m_validator.TryToWriteField("aVaCONetILckd", () => m_app.aVaCONetILckd, t => m_app.aVaCONetILckd = t, value);
            }
        }


        public string aVaONetIDesc
        {
            get { return m_app.aVaONetIDesc; }
            set
            {
                m_validator.TryToWriteField("aVaONetIDesc", () => m_app.aVaONetIDesc, t => m_app.aVaONetIDesc = t, value);
            }
        }


        public decimal aVaCNetI
        {
            get { return m_app.aVaCNetI; }
        }


        public string aVaCNetI_rep
        {
            get { return m_app.aVaCNetI_rep; }
        }


        public decimal aVaBEmplmtI
        {
            get { return m_app.aVaBEmplmtI; }
            set
            {
                m_validator.TryToWriteField("aVaBEmplmtI", () => m_app.aVaBEmplmtI, t => m_app.aVaBEmplmtI = t, value);
            }
        }


        public string aVaBEmplmtI_rep
        {
            get { return m_app.aVaBEmplmtI_rep; }
            set
            {
                m_validator.TryToWriteField("aVaBEmplmtI_rep", () => m_app.aVaBEmplmtI_rep, t => m_app.aVaBEmplmtI_rep = t, value);
            }
        }


        public bool aVaBEmplmtILckd
        {
            get { return m_app.aVaBEmplmtILckd; }
            set
            {
                m_validator.TryToWriteField("aVaBEmplmtILckd", () => m_app.aVaBEmplmtILckd, t => m_app.aVaBEmplmtILckd = t, value);
            }
        }


        public decimal aVaBFedITax
        {
            get { return m_app.aVaBFedITax; }
            set
            {
                m_validator.TryToWriteField("aVaBFedITax", () => m_app.aVaBFedITax, t => m_app.aVaBFedITax = t, value);
            }
        }


        public string aVaBFedITax_rep
        {
            get { return m_app.aVaBFedITax_rep; }
            set
            {
                m_validator.TryToWriteField("aVaBFedITax_rep", () => m_app.aVaBFedITax_rep, t => m_app.aVaBFedITax_rep = t, value);
            }
        }


        public decimal aVaBStateITax
        {
            get { return m_app.aVaBStateITax; }
            set
            {
                m_validator.TryToWriteField("aVaBStateITax", () => m_app.aVaBStateITax, t => m_app.aVaBStateITax = t, value);
            }
        }


        public string aVaBStateITax_rep
        {
            get { return m_app.aVaBStateITax_rep; }
            set
            {
                m_validator.TryToWriteField("aVaBStateITax_rep", () => m_app.aVaBStateITax_rep, t => m_app.aVaBStateITax_rep = t, value);
            }
        }


        public decimal aVaBSsnTax
        {
            get { return m_app.aVaBSsnTax; }
            set
            {
                m_validator.TryToWriteField("aVaBSsnTax", () => m_app.aVaBSsnTax, t => m_app.aVaBSsnTax = t, value);
            }
        }


        public string aVaBSsnTax_rep
        {
            get { return m_app.aVaBSsnTax_rep; }
            set
            {
                m_validator.TryToWriteField("aVaBSsnTax_rep", () => m_app.aVaBSsnTax_rep, t => m_app.aVaBSsnTax_rep = t, value);
            }
        }


        public decimal aVaBOITax
        {
            get { return m_app.aVaBOITax; }
            set
            {
                m_validator.TryToWriteField("aVaBOITax", () => m_app.aVaBOITax, t => m_app.aVaBOITax = t, value);
            }
        }


        public string aVaBOITax_rep
        {
            get { return m_app.aVaBOITax_rep; }
            set
            {
                m_validator.TryToWriteField("aVaBOITax_rep", () => m_app.aVaBOITax_rep, t => m_app.aVaBOITax_rep = t, value);
            }
        }


        public decimal aVaBTotITax
        {
            get { return m_app.aVaBTotITax; }
        }


        public string aVaBTotITax_rep
        {
            get { return m_app.aVaBTotITax_rep; }
        }


        public decimal aVaBTakehomeEmplmtI
        {
            get { return m_app.aVaBTakehomeEmplmtI; }
        }


        public string aVaBTakehomeEmplmtI_rep
        {
            get { return m_app.aVaBTakehomeEmplmtI_rep; }
        }


        public decimal aVaBONetI
        {
            get { return m_app.aVaBONetI; }
            set
            {
                m_validator.TryToWriteField("aVaBONetI", () => m_app.aVaBONetI, t => m_app.aVaBONetI = t, value);
            }
        }


        public string aVaBONetI_rep
        {
            get { return m_app.aVaBONetI_rep; }
            set
            {
                m_validator.TryToWriteField("aVaBONetI_rep", () => m_app.aVaBONetI_rep, t => m_app.aVaBONetI_rep = t, value);
            }
        }


        public bool aVaBONetILckd
        {
            get { return m_app.aVaBONetILckd; }
            set
            {
                m_validator.TryToWriteField("aVaBONetILckd", () => m_app.aVaBONetILckd, t => m_app.aVaBONetILckd = t, value);
            }
        }


        public decimal aVaBNetI
        {
            get { return m_app.aVaBNetI; }
        }


        public string aVaBNetI_rep
        {
            get { return m_app.aVaBNetI_rep; }
        }


        public decimal aVaTotEmplmtI
        {
            get { return m_app.aVaTotEmplmtI; }
        }


        public string aVaTotEmplmtI_rep
        {
            get { return m_app.aVaTotEmplmtI_rep; }
        }


        public decimal aVaTotITax
        {
            get { return m_app.aVaTotITax; }
        }


        public string aVaTotITax_rep
        {
            get { return m_app.aVaTotITax_rep; }
        }


        public decimal aVaTakehomeEmplmtI
        {
            get { return m_app.aVaTakehomeEmplmtI; }
        }


        public string aVaTakehomeEmplmtI_rep
        {
            get { return m_app.aVaTakehomeEmplmtI_rep; }
        }


        public decimal aVaONetI
        {
            get { return m_app.aVaONetI; }
        }


        public string aVaONetI_rep
        {
            get { return m_app.aVaONetI_rep; }
        }


        public string aVaLiaMonTotDeducted_rep
        {
            get { return m_app.aVaLiaMonTotDeducted_rep; }
        }


        public string aVaLiaMonTotExisting_rep
        {
            get { return m_app.aVaLiaMonTotExisting_rep; }
        }


        public string aVaLiaMonTotExistingAndOpNegCf_rep
        {
            get { return m_app.aVaLiaMonTotExistingAndOpNegCf_rep; }
        }


        public decimal aVaLiaBalTotExisting
        {
            get { return m_app.aVaLiaBalTotExisting; }
        }


        public string aVaLiaBalTotExisting_rep
        {
            get { return m_app.aVaLiaBalTotExisting_rep; }
        }


        public decimal aVaNetI
        {
            get { return m_app.aVaNetI; }
        }


        public string aVaNetI_rep
        {
            get { return m_app.aVaNetI_rep; }
        }


        public decimal aVaNetEffectiveI
        {
            get { return m_app.aVaNetEffectiveI; }
        }


        public string aVaNetEffectiveI_rep
        {
            get { return m_app.aVaNetEffectiveI_rep; }
        }


        public E_TriState aVaUtilityIncludedTri
        {
            get { return m_app.aVaUtilityIncludedTri; }
            set
            {
                m_validator.TryToWriteField("aVaUtilityIncludedTri", () => m_app.aVaUtilityIncludedTri, t => m_app.aVaUtilityIncludedTri = t, value);
            }
        }


        public decimal aVaFamilySupportBal
        {
            get { return m_app.aVaFamilySupportBal; }
        }


        public string aVaFamilySupportBal_rep
        {
            get { return m_app.aVaFamilySupportBal_rep; }
        }


        public decimal aVaFamilySuportGuidelineAmt
        {
            get { return m_app.aVaFamilySuportGuidelineAmt; }
            set
            {
                m_validator.TryToWriteField("aVaFamilySuportGuidelineAmt", () => m_app.aVaFamilySuportGuidelineAmt, t => m_app.aVaFamilySuportGuidelineAmt = t, value);
            }
        }


        public string aVaFamilySuportGuidelineAmt_rep
        {
            get { return m_app.aVaFamilySuportGuidelineAmt_rep; }
            set
            {
                m_validator.TryToWriteField("aVaFamilySuportGuidelineAmt_rep", () => m_app.aVaFamilySuportGuidelineAmt_rep, t => m_app.aVaFamilySuportGuidelineAmt_rep = t, value);
            }
        }


        public decimal aVaRatio
        {
            get { return m_app.aVaRatio; }
        }


        public string aVaRatio_rep
        {
            get { return m_app.aVaRatio_rep; }
        }


        public E_TriState aVaCrRecordSatisfyTri
        {
            get { return m_app.aVaCrRecordSatisfyTri; }
            set
            {
                m_validator.TryToWriteField("aVaCrRecordSatisfyTri", () => m_app.aVaCrRecordSatisfyTri, t => m_app.aVaCrRecordSatisfyTri = t, value);
            }
        }


        public E_TriState aVaLMeetCrStandardTri
        {
            get { return m_app.aVaLMeetCrStandardTri; }
            set
            {
                m_validator.TryToWriteField("aVaLMeetCrStandardTri", () => m_app.aVaLMeetCrStandardTri, t => m_app.aVaLMeetCrStandardTri = t, value);
            }
        }


        public string aVaLAnalysisN
        {
            get { return m_app.aVaLAnalysisN; }
            set
            {
                m_validator.TryToWriteField("aVaLAnalysisN", () => m_app.aVaLAnalysisN, t => m_app.aVaLAnalysisN = t, value);
            }
        }


        public E_TriState aActiveMilitaryStatTri
        {
            get { return m_app.aActiveMilitaryStatTri; }
            set
            {
                m_validator.TryToWriteField("aActiveMilitaryStatTri", () => m_app.aActiveMilitaryStatTri, t => m_app.aActiveMilitaryStatTri = t, value);
            }
        }


        public E_TriState aWereActiveMilitaryDutyDayAfterTri
        {
            get { return m_app.aWereActiveMilitaryDutyDayAfterTri; }
            set
            {
                m_validator.TryToWriteField("aWereActiveMilitaryDutyDayAfterTri", () => m_app.aWereActiveMilitaryDutyDayAfterTri, t => m_app.aWereActiveMilitaryDutyDayAfterTri = t, value);
            }
        }

        public E_aVaVestTitleT aVaVestTitleT
        {
            get { return m_app.aVaVestTitleT; }
            set
            {
                m_validator.TryToWriteField("aVaVestTitleT", () => m_app.aVaVestTitleT, t => m_app.aVaVestTitleT = t, value);
            }
        }


        public string aVaVestTitleODesc
        {
            get { return m_app.aVaVestTitleODesc; }
            set
            {
                m_validator.TryToWriteField("aVaVestTitleODesc", () => m_app.aVaVestTitleODesc, t => m_app.aVaVestTitleODesc = t, value);
            }
        }


        public bool aVaNotDischargedOrReleasedSinceCertOfEligibility
        {
            get { return m_app.aVaNotDischargedOrReleasedSinceCertOfEligibility; }
            set { m_validator.TryToWriteField("aVaNotDischargedOrReleasedSinceCertOfEligibility", () => m_app.aVaNotDischargedOrReleasedSinceCertOfEligibility, t => m_app.aVaNotDischargedOrReleasedSinceCertOfEligibility = t, value); }
        }


        public CDateTime aVaServ1StartD
        {
            get { return m_app.aVaServ1StartD; }
            set
            {
                m_validator.TryToWriteField("aVaServ1StartD", () => m_app.aVaServ1StartD, t => m_app.aVaServ1StartD = t, value);
            }
        }


        public string aVaServ1StartD_rep
        {
            get { return m_app.aVaServ1StartD_rep; }
            set
            {
                m_validator.TryToWriteField("aVaServ1StartD_rep", () => m_app.aVaServ1StartD_rep, t => m_app.aVaServ1StartD_rep = t, value);
            }
        }


        public CDateTime aVaServ2StartD
        {
            get { return m_app.aVaServ2StartD; }
            set
            {
                m_validator.TryToWriteField("aVaServ2StartD", () => m_app.aVaServ2StartD, t => m_app.aVaServ2StartD = t, value);
            }
        }


        public string aVaServ2StartD_rep
        {
            get { return m_app.aVaServ2StartD_rep; }
            set
            {
                m_validator.TryToWriteField("aVaServ2StartD_rep", () => m_app.aVaServ2StartD_rep, t => m_app.aVaServ2StartD_rep = t, value);
            }
        }


        public CDateTime aVaServ3StartD
        {
            get { return m_app.aVaServ3StartD; }
            set
            {
                m_validator.TryToWriteField("aVaServ3StartD", () => m_app.aVaServ3StartD, t => m_app.aVaServ3StartD = t, value);
            }
        }


        public string aVaServ3StartD_rep
        {
            get { return m_app.aVaServ3StartD_rep; }
            set
            {
                m_validator.TryToWriteField("aVaServ3StartD_rep", () => m_app.aVaServ3StartD_rep, t => m_app.aVaServ3StartD_rep = t, value);
            }
        }


        public CDateTime aVaServ4StartD
        {
            get { return m_app.aVaServ4StartD; }
            set
            {
                m_validator.TryToWriteField("aVaServ4StartD", () => m_app.aVaServ4StartD, t => m_app.aVaServ4StartD = t, value);
            }
        }


        public string aVaServ4StartD_rep
        {
            get { return m_app.aVaServ4StartD_rep; }
            set
            {
                m_validator.TryToWriteField("aVaServ4StartD_rep", () => m_app.aVaServ4StartD_rep, t => m_app.aVaServ4StartD_rep = t, value);
            }
        }


        public CDateTime aVaServ1EndD
        {
            get { return m_app.aVaServ1EndD; }
            set
            {
                m_validator.TryToWriteField("aVaServ1EndD", () => m_app.aVaServ1EndD, t => m_app.aVaServ1EndD = t, value);
            }
        }


        public string aVaServ1EndD_rep
        {
            get { return m_app.aVaServ1EndD_rep; }
            set
            {
                m_validator.TryToWriteField("aVaServ1EndD_rep", () => m_app.aVaServ1EndD_rep, t => m_app.aVaServ1EndD_rep = t, value);
            }
        }


        public CDateTime aVaServ2EndD
        {
            get { return m_app.aVaServ2EndD; }
            set
            {
                m_validator.TryToWriteField("aVaServ2EndD", () => m_app.aVaServ2EndD, t => m_app.aVaServ2EndD = t, value);
            }
        }


        public string aVaServ2EndD_rep
        {
            get { return m_app.aVaServ2EndD_rep; }
            set
            {
                m_validator.TryToWriteField("aVaServ2EndD_rep", () => m_app.aVaServ2EndD_rep, t => m_app.aVaServ2EndD_rep = t, value);
            }
        }


        public CDateTime aVaServ3EndD
        {
            get { return m_app.aVaServ3EndD; }
            set
            {
                m_validator.TryToWriteField("aVaServ3EndD", () => m_app.aVaServ3EndD, t => m_app.aVaServ3EndD = t, value);
            }
        }


        public string aVaServ3EndD_rep
        {
            get { return m_app.aVaServ3EndD_rep; }
            set
            {
                m_validator.TryToWriteField("aVaServ3EndD_rep", () => m_app.aVaServ3EndD_rep, t => m_app.aVaServ3EndD_rep = t, value);
            }
        }


        public CDateTime aVaServ4EndD
        {
            get { return m_app.aVaServ4EndD; }
            set
            {
                m_validator.TryToWriteField("aVaServ4EndD", () => m_app.aVaServ4EndD, t => m_app.aVaServ4EndD = t, value);
            }
        }


        public string aVaServ4EndD_rep
        {
            get { return m_app.aVaServ4EndD_rep; }
            set
            {
                m_validator.TryToWriteField("aVaServ4EndD_rep", () => m_app.aVaServ4EndD_rep, t => m_app.aVaServ4EndD_rep = t, value);
            }
        }


        public string aVaServ1FullNm
        {
            get { return m_app.aVaServ1FullNm; }
            set
            {
                m_validator.TryToWriteField("aVaServ1FullNm", () => m_app.aVaServ1FullNm, t => m_app.aVaServ1FullNm = t, value);
            }
        }


        public string aVaServ2FullNm
        {
            get { return m_app.aVaServ2FullNm; }
            set
            {
                m_validator.TryToWriteField("aVaServ2FullNm", () => m_app.aVaServ2FullNm, t => m_app.aVaServ2FullNm = t, value);
            }
        }


        public string aVaServ3FullNm
        {
            get { return m_app.aVaServ3FullNm; }
            set
            {
                m_validator.TryToWriteField("aVaServ3FullNm", () => m_app.aVaServ3FullNm, t => m_app.aVaServ3FullNm = t, value);
            }
        }


        public string aVaServ4FullNm
        {
            get { return m_app.aVaServ4FullNm; }
            set
            {
                m_validator.TryToWriteField("aVaServ4FullNm", () => m_app.aVaServ4FullNm, t => m_app.aVaServ4FullNm = t, value);
            }
        }


        public string aVaServ1Ssn
        {
            get { return m_app.aVaServ1Ssn; }
            set
            {
                m_validator.TryToWriteField("aVaServ1Ssn", () => m_app.aVaServ1Ssn, t => m_app.aVaServ1Ssn = t, value);
            }
        }


        public string aVaServ2Ssn
        {
            get { return m_app.aVaServ2Ssn; }
            set
            {
                m_validator.TryToWriteField("aVaServ2Ssn", () => m_app.aVaServ2Ssn, t => m_app.aVaServ2Ssn = t, value);
            }
        }


        public string aVaServ3Ssn
        {
            get { return m_app.aVaServ3Ssn; }
            set
            {
                m_validator.TryToWriteField("aVaServ3Ssn", () => m_app.aVaServ3Ssn, t => m_app.aVaServ3Ssn = t, value);
            }
        }


        public string aVaServ4Ssn
        {
            get { return m_app.aVaServ4Ssn; }
            set
            {
                m_validator.TryToWriteField("aVaServ4Ssn", () => m_app.aVaServ4Ssn, t => m_app.aVaServ4Ssn = t, value);
            }
        }


        public string aVaServ1Num
        {
            get { return m_app.aVaServ1Num; }
            set
            {
                m_validator.TryToWriteField("aVaServ1Num", () => m_app.aVaServ1Num, t => m_app.aVaServ1Num = t, value);
            }
        }


        public string aVaServ2Num
        {
            get { return m_app.aVaServ2Num; }
            set
            {
                m_validator.TryToWriteField("aVaServ2Num", () => m_app.aVaServ2Num, t => m_app.aVaServ2Num = t, value);
            }
        }


        public string aVaServ3Num
        {
            get { return m_app.aVaServ3Num; }
            set
            {
                m_validator.TryToWriteField("aVaServ3Num", () => m_app.aVaServ3Num, t => m_app.aVaServ3Num = t, value);
            }
        }


        public string aVaServ4Num
        {
            get { return m_app.aVaServ4Num; }
            set
            {
                m_validator.TryToWriteField("aVaServ4Num", () => m_app.aVaServ4Num, t => m_app.aVaServ4Num = t, value);
            }
        }


        public string aVaServ1BranchNum
        {
            get { return m_app.aVaServ1BranchNum; }
            set
            {
                m_validator.TryToWriteField("aVaServ1BranchNum", () => m_app.aVaServ1BranchNum, t => m_app.aVaServ1BranchNum = t, value);
            }
        }


        public string aVaServ2BranchNum
        {
            get { return m_app.aVaServ2BranchNum; }
            set
            {
                m_validator.TryToWriteField("aVaServ2BranchNum", () => m_app.aVaServ2BranchNum, t => m_app.aVaServ2BranchNum = t, value);
            }
        }


        public string aVaServ3BranchNum
        {
            get { return m_app.aVaServ3BranchNum; }
            set
            {
                m_validator.TryToWriteField("aVaServ3BranchNum", () => m_app.aVaServ3BranchNum, t => m_app.aVaServ3BranchNum = t, value);
            }
        }


        public string aVaServ4BranchNum
        {
            get { return m_app.aVaServ4BranchNum; }
            set
            {
                m_validator.TryToWriteField("aVaServ4BranchNum", () => m_app.aVaServ4BranchNum, t => m_app.aVaServ4BranchNum = t, value);
            }
        }


        public bool aVaDischargedDisabilityIs
        {
            get { return m_app.aVaDischargedDisabilityIs; }
            set
            {
                m_validator.TryToWriteField("aVaDischargedDisabilityIs", () => m_app.aVaDischargedDisabilityIs, t => m_app.aVaDischargedDisabilityIs = t, value);
            }
        }


        public string aVaClaimNum
        {
            get { return m_app.aVaClaimNum; }
            set
            {
                m_validator.TryToWriteField("aVaClaimNum", () => m_app.aVaClaimNum, t => m_app.aVaClaimNum = t, value);
            }
        }


        public bool HasBEmpCollection
        {
            get { return m_app.HasBEmpCollection; }
        }




        public bool sfMaxEmploymentGapWithin
        {
            get { return m_app.sfMaxEmploymentGapWithin; }
        }


        public int YearsOnJob
        {
            get { return m_app.YearsOnJob; }
        }


        public int YearsInProfession
        {
            get { return m_app.YearsInProfession; }
        }


        public bool aBEmpCollectionValid
        {
            get { return m_app.aBEmpCollectionValid; }
        }


        public bool aCEmpCollectionValid
        {
            get { return m_app.aCEmpCollectionValid; }
        }


        public bool aIsSelfEmployed
        {
            get { return m_app.aIsSelfEmployed; }
        }


        public bool aBIsSelfEmplmt
        {
            get { return m_app.aBIsSelfEmplmt; }
            set
            {
                m_validator.TryToWriteField("aBIsSelfEmplmt", () => m_app.aBIsSelfEmplmt, t => m_app.aBIsSelfEmplmt = t, value);
            }
        }


        public bool aCIsSelfEmplmt
        {
            get { return m_app.aCIsSelfEmplmt; }
            set
            {
                m_validator.TryToWriteField("aCIsSelfEmplmt", () => m_app.aCIsSelfEmplmt, t => m_app.aCIsSelfEmplmt = t, value);
            }
        }


        public string aBEmplrNm
        {
            get { return m_app.aBEmplrNm; }
        }


        public string aBEmplrAddr
        {
            get { return m_app.aBEmplrAddr; }
        }


        public string aBEmplrCity
        {
            get { return m_app.aBEmplrCity; }
        }


        public string aBEmplrState
        {
            get { return m_app.aBEmplrState; }
        }


        public string aBEmplrZip
        {
            get { return m_app.aBEmplrZip; }
        }


        public string aBEmplrBusPhone
        {
            get { return m_app.aBEmplrBusPhone; }
        }


        public string aBEmplrJobTitle
        {
            get { return m_app.aBEmplrJobTitle; }
        }


        public string aBEmplrJobTitleTotalScorecardWarning
        {
            get { return m_app.aBEmplrJobTitleTotalScorecardWarning; }
        }


        public string aBEmplrJobTitlePmlImportWarning
        {
            get { return m_app.aBEmplrJobTitlePmlImportWarning; }
        }


        public string aBEmpCollectionTotalScorecardWarning
        {
            get { return m_app.aBEmpCollectionTotalScorecardWarning; }
        }


        public string aCEmpCollectionTotalScorecardWarning
        {
            get { return m_app.aCEmpCollectionTotalScorecardWarning; }
        }


        public CDateTime aBEmplVerifSent
        {
            get { return m_app.aBEmplVerifSent; }
        }


        public string aBEmplVerifSent_rep
        {
            get { return m_app.aBEmplVerifSent_rep; }
        }


        public CDateTime aBEmplVerifRecv
        {
            get { return m_app.aBEmplVerifRecv; }
        }


        public string aBEmplVerifRecv_rep
        {
            get { return m_app.aBEmplVerifRecv_rep; }
        }


        public string aCEmplrNm
        {
            get { return m_app.aCEmplrNm; }
        }


        public string aCEmplrAddr
        {
            get { return m_app.aCEmplrAddr; }
        }


        public string aCEmplrCity
        {
            get { return m_app.aCEmplrCity; }
        }


        public string aCEmplrState
        {
            get { return m_app.aCEmplrState; }
        }


        public string aCEmplrZip
        {
            get { return m_app.aCEmplrZip; }
        }


        public string aCEmplrBusPhone
        {
            get { return m_app.aCEmplrBusPhone; }
        }


        public string aCEmplrJobTitle
        {
            get { return m_app.aCEmplrJobTitle; }
        }


        public string aCEmplrJobTitleTotalScorecardWarning
        {
            get { return m_app.aCEmplrJobTitleTotalScorecardWarning; }
        }


        public string aCEmplrJobTitlePmlImportWarning
        {
            get { return m_app.aCEmplrJobTitlePmlImportWarning; }
        }


        public bool aBEmplrBusPhoneLckd
        {
            get { return m_app.aBEmplrBusPhoneLckd; }
            set
            {
                m_validator.TryToWriteField("aBEmplrBusPhoneLckd", () => m_app.aBEmplrBusPhoneLckd, t => m_app.aBEmplrBusPhoneLckd = t, value);
            }
        }


        public bool aCEmplrBusPhoneLckd
        {
            get { return m_app.aCEmplrBusPhoneLckd; }
            set
            {
                m_validator.TryToWriteField("aCEmplrBusPhoneLckd", () => m_app.aCEmplrBusPhoneLckd, t => m_app.aCEmplrBusPhoneLckd = t, value);
            }
        }


        public bool HasCEmpCollection
        {
            get { return m_app.HasCEmpCollection; }
        }



        public decimal aPresPersistentHExp
        {
            get { return m_app.aPresPersistentHExp; }
        }


        public E_CountryMailT aBCountryMailT
        {
            get { return m_app.aBCountryMailT; }
            set
            {
                m_validator.TryToWriteField("aBCountryMailT", () => m_app.aBCountryMailT, t => m_app.aBCountryMailT = t, value);
            }
        }


        public E_CountryMailT aCCountryMailT
        {
            get { return m_app.aCCountryMailT; }
            set
            {
                m_validator.TryToWriteField("aCCountryMailT", () => m_app.aCCountryMailT, t => m_app.aCCountryMailT = t, value);
            }
        }


        public string aTitleNm1
        {
            get { return m_app.aTitleNm1; }
            set
            {
                m_validator.TryToWriteField("aTitleNm1", () => m_app.aTitleNm1, t => m_app.aTitleNm1 = t, value);
            }
        }

        public bool aTitleNm1Lckd
        {
            get { return m_app.aTitleNm1Lckd; }
            set
            {
                m_validator.TryToWriteField("aTitleNm1Lckd", () => m_app.aTitleNm1Lckd, t => m_app.aTitleNm1Lckd = t, value);
            }
        }

        public string aTitleNm2
        {
            get { return m_app.aTitleNm2; }
            set
            {
                m_validator.TryToWriteField("aTitleNm2", () => m_app.aTitleNm2, t => m_app.aTitleNm2 = t, value);
            }
        }

        public bool aTitleNm2Lckd
        {
            get { return m_app.aTitleNm2Lckd; }
            set
            {
                m_validator.TryToWriteField("aTitleNm2Lckd", () => m_app.aTitleNm2Lckd, t => m_app.aTitleNm2Lckd = t, value);
            }
        }

        public E_TriState aBIsCurrentlyOnTitle
        {
            get { return this.m_app.aBIsCurrentlyOnTitle; }
            set
            {
                this.m_validator.TryToWriteField("aBIsCurrentlyOnTitle", () => m_app.aBIsCurrentlyOnTitle, t => m_app.aBIsCurrentlyOnTitle = t, value);
            }
        }

        public E_TriState aCIsCurrentlyOnTitle
        {
            get { return this.m_app.aCIsCurrentlyOnTitle; }
            set
            {
                this.m_validator.TryToWriteField("aCIsCurrentlyOnTitle", () => m_app.aCIsCurrentlyOnTitle, t => m_app.aCIsCurrentlyOnTitle = t, value);
            }
        }

        public string aBCurrentTitleName
        {
            get { return this.m_app.aBCurrentTitleName; }
            set
            {
                this.m_validator.TryToWriteField("aBCurrentTitleName", () => m_app.aBCurrentTitleName, t => m_app.aBCurrentTitleName = t, value);
            }
        }

        public bool aBCurrentTitleNameLckd
        {
            get { return this.m_app.aBCurrentTitleNameLckd; }
            set
            {
                this.m_validator.TryToWriteField("aBCurrentTitleNameLckd", () => m_app.aBCurrentTitleNameLckd, t => m_app.aBCurrentTitleNameLckd = t, value);
            }
        }

        public string aCCurrentTitleName
        {
            get { return this.m_app.aCCurrentTitleName; }
            set
            {
                this.m_validator.TryToWriteField("aCCurrentTitleName", () => m_app.aCCurrentTitleName, t => m_app.aCCurrentTitleName = t, value);
            }
        }

        public bool aCCurrentTitleNameLckd
        {
            get { return this.m_app.aCCurrentTitleNameLckd; }
            set
            {
                this.m_validator.TryToWriteField("aCCurrentTitleNameLckd", () => m_app.aCCurrentTitleNameLckd, t => m_app.aCCurrentTitleNameLckd = t, value);
            }
        }

        public string aManner
        {
            get { return m_app.aManner; }
            set
            {
                m_validator.TryToWriteField("aManner", () => m_app.aManner, t => m_app.aManner = t, value);
            }
        }


        public string aFHACreditRating
        {
            get { return m_app.aFHACreditRating; }
            set
            {
                m_validator.TryToWriteField("aFHACreditRating", () => m_app.aFHACreditRating, t => m_app.aFHACreditRating = t, value);
            }
        }


        public string aFHARatingIAdequacy
        {
            get { return m_app.aFHARatingIAdequacy; }
            set
            {
                m_validator.TryToWriteField("aFHARatingIAdequacy", () => m_app.aFHARatingIAdequacy, t => m_app.aFHARatingIAdequacy = t, value);
            }
        }


        public string aFHARatingIStability
        {
            get { return m_app.aFHARatingIStability; }
            set
            {
                m_validator.TryToWriteField("aFHARatingIStability", () => m_app.aFHARatingIStability, t => m_app.aFHARatingIStability = t, value);
            }
        }


        public string aFHARatingAssetAdequacy
        {
            get { return m_app.aFHARatingAssetAdequacy; }
            set
            {
                m_validator.TryToWriteField("aFHARatingAssetAdequacy", () => m_app.aFHARatingAssetAdequacy, t => m_app.aFHARatingAssetAdequacy = t, value);
            }
        }


        public decimal aFHAGrossMonI
        {
            get { return m_app.aFHAGrossMonI; }
        }


        public string aFHAGrossMonI_rep
        {
            get { return m_app.aFHAGrossMonI_rep; }
        }


        public decimal aFHAFixedPmtToIRatio
        {
            get { return m_app.aFHAFixedPmtToIRatio; }
        }


        public string aFHAFixedPmtToIRatio_rep
        {
            get { return m_app.aFHAFixedPmtToIRatio_rep; }
        }


        public decimal aFHANegCfRentalI
        {
            get { return m_app.aFHANegCfRentalI; }
            set
            {
                m_validator.TryToWriteField("aFHANegCfRentalI", () => m_app.aFHANegCfRentalI, t => m_app.aFHANegCfRentalI = t, value);
            }
        }


        public string aFHANegCfRentalI_rep
        {
            get { return m_app.aFHANegCfRentalI_rep; }
            set
            {
                m_validator.TryToWriteField("aFHANegCfRentalI_rep", () => m_app.aFHANegCfRentalI_rep, t => m_app.aFHANegCfRentalI_rep = t, value);
            }
        }


        public string aFHABCaivrsNum
        {
            get { return m_app.aFHABCaivrsNum; }
            set
            {
                m_validator.TryToWriteField("aFHABCaivrsNum", () => m_app.aFHABCaivrsNum, t => m_app.aFHABCaivrsNum = t, value);
            }
        }


        public string aFHACCaivrsNum
        {
            get { return m_app.aFHACCaivrsNum; }
            set
            {
                m_validator.TryToWriteField("aFHACCaivrsNum", () => m_app.aFHACCaivrsNum, t => m_app.aFHACCaivrsNum = t, value);
            }
        }


        public E_TriState aFHABLdpGsaTri
        {
            get { return m_app.aFHABLdpGsaTri; }
            set
            {
                m_validator.TryToWriteField("aFHABLdpGsaTri", () => m_app.aFHABLdpGsaTri, t => m_app.aFHABLdpGsaTri = t, value);
            }
        }


        public E_TriState aFHACLdpGsaTri
        {
            get { return m_app.aFHACLdpGsaTri; }
            set
            {
                m_validator.TryToWriteField("aFHACLdpGsaTri", () => m_app.aFHACLdpGsaTri, t => m_app.aFHACLdpGsaTri = t, value);
            }
        }


        public string aFHABLpdGsa
        {
            get { return m_app.aFHABLpdGsa; }
            set
            {
                m_validator.TryToWriteField("aFHABLpdGsa", () => m_app.aFHABLpdGsa, t => m_app.aFHABLpdGsa = t, value);
            }
        }


        public string aFHACLpdGsa
        {
            get { return m_app.aFHACLpdGsa; }
            set
            {
                m_validator.TryToWriteField("aFHACLpdGsa", () => m_app.aFHACLpdGsa, t => m_app.aFHACLpdGsa = t, value);
            }
        }


        public decimal aFHAMPmtToIRatio
        {
            get { return m_app.aFHAMPmtToIRatio; }
        }


        public string aFHAMPmtToIRatio_rep
        {
            get { return m_app.aFHAMPmtToIRatio_rep; }
        }


        public decimal aFHAPmtFixedTot
        {
            get { return m_app.aFHAPmtFixedTot; }
        }


        public string aFHAPmtFixedTot_rep
        {
            get { return m_app.aFHAPmtFixedTot_rep; }
        }


        public decimal aFHADebtPmtTot
        {
            get { return m_app.aFHADebtPmtTot; }
        }


        public string aFHADebtPmtTot_rep
        {
            get { return m_app.aFHADebtPmtTot_rep; }
        }


        public decimal aFHACBaseI
        {
            get { return m_app.aFHACBaseI; }
            set
            {
                m_validator.TryToWriteField("aFHACBaseI", () => m_app.aFHACBaseI, t => m_app.aFHACBaseI = t, value);
            }
        }


        public string aFHACBaseI_rep
        {
            get { return m_app.aFHACBaseI_rep; }
            set
            {
                m_validator.TryToWriteField("aFHACBaseI_rep", () => m_app.aFHACBaseI_rep, t => m_app.aFHACBaseI_rep = t, value);
            }
        }


        public decimal aFHANetRentalI
        {
            get { return m_app.aFHANetRentalI; }
            set
            {
                m_validator.TryToWriteField("aFHANetRentalI", () => m_app.aFHANetRentalI, t => m_app.aFHANetRentalI = t, value);
            }
        }


        public string aFHANetRentalI_rep
        {
            get { return m_app.aFHANetRentalI_rep; }
            set
            {
                m_validator.TryToWriteField("aFHANetRentalI_rep", () => m_app.aFHANetRentalI_rep, t => m_app.aFHANetRentalI_rep = t, value);
            }
        }


        public decimal aFHACOI
        {
            get { return m_app.aFHACOI; }
            set
            {
                m_validator.TryToWriteField("aFHACOI", () => m_app.aFHACOI, t => m_app.aFHACOI = t, value);
            }
        }


        public string aFHACOI_rep
        {
            get { return m_app.aFHACOI_rep; }
            set
            {
                m_validator.TryToWriteField("aFHACOI_rep", () => m_app.aFHACOI_rep, t => m_app.aFHACOI_rep = t, value);
            }
        }


        public decimal aFHABOI
        {
            get { return m_app.aFHABOI; }
            set
            {
                m_validator.TryToWriteField("aFHABOI", () => m_app.aFHABOI, t => m_app.aFHABOI = t, value);
            }
        }


        public string aFHABOI_rep
        {
            get { return m_app.aFHABOI_rep; }
            set
            {
                m_validator.TryToWriteField("aFHABOI_rep", () => m_app.aFHABOI_rep, t => m_app.aFHABOI_rep = t, value);
            }
        }


        public decimal aFHABTotI
        {
            get { return m_app.aFHABTotI; }
        }


        public string aFHABTotI_rep
        {
            get { return m_app.aFHABTotI_rep; }
        }


        public decimal aFHACTotI
        {
            get { return m_app.aFHACTotI; }
        }


        public string aFHACTotI_rep
        {
            get { return m_app.aFHACTotI_rep; }
        }


        public decimal aFHATotOI
        {
            get { return m_app.aFHATotOI; }
        }


        public string aFHATotOI_rep
        {
            get { return m_app.aFHATotOI_rep; }
        }


        public decimal aFHABBaseI
        {
            get { return m_app.aFHABBaseI; }
            set
            {
                m_validator.TryToWriteField("aFHABBaseI", () => m_app.aFHABBaseI, t => m_app.aFHABBaseI = t, value);
            }
        }


        public string aFHABBaseI_rep
        {
            get { return m_app.aFHABBaseI_rep; }
            set
            {
                m_validator.TryToWriteField("aFHABBaseI_rep", () => m_app.aFHABBaseI_rep, t => m_app.aFHABBaseI_rep = t, value);
            }
        }


        public decimal aFHATotBaseI
        {
            get { return m_app.aFHATotBaseI; }
        }


        public string aFHATotBaseI_rep
        {
            get { return m_app.aFHATotBaseI_rep; }
        }


        public decimal aFHADebtInstallBal
        {
            get { return m_app.aFHADebtInstallBal; }
            set
            {
                m_validator.TryToWriteField("aFHADebtInstallBal", () => m_app.aFHADebtInstallBal, t => m_app.aFHADebtInstallBal = t, value);
            }
        }


        public string aFHADebtInstallBal_rep
        {
            get { return m_app.aFHADebtInstallBal_rep; }
            set
            {
                m_validator.TryToWriteField("aFHADebtInstallBal_rep", () => m_app.aFHADebtInstallBal_rep, t => m_app.aFHADebtInstallBal_rep = t, value);
            }
        }


        public decimal aFHAOtherDebtBal
        {
            get { return m_app.aFHAOtherDebtBal; }
            set
            {
                m_validator.TryToWriteField("aFHAOtherDebtBal", () => m_app.aFHAOtherDebtBal, t => m_app.aFHAOtherDebtBal = t, value);
            }
        }


        public string aFHAOtherDebtBal_rep
        {
            get { return m_app.aFHAOtherDebtBal_rep; }
            set
            {
                m_validator.TryToWriteField("aFHAOtherDebtBal_rep", () => m_app.aFHAOtherDebtBal_rep, t => m_app.aFHAOtherDebtBal_rep = t, value);
            }
        }


        public decimal aFHAOtherDebtPmt
        {
            get { return m_app.aFHAOtherDebtPmt; }
            set
            {
                m_validator.TryToWriteField("aFHAOtherDebtPmt", () => m_app.aFHAOtherDebtPmt, t => m_app.aFHAOtherDebtPmt = t, value);
            }
        }


        public string aFHAOtherDebtPmt_rep
        {
            get { return m_app.aFHAOtherDebtPmt_rep; }
            set
            {
                m_validator.TryToWriteField("aFHAOtherDebtPmt_rep", () => m_app.aFHAOtherDebtPmt_rep, t => m_app.aFHAOtherDebtPmt_rep = t, value);
            }
        }


        public decimal aFHAOtherDebtPmtMCAWPrinting
        {
            get { return m_app.aFHAOtherDebtPmtMCAWPrinting; }
        }


        public string aFHAOtherDebtPmtMCAWPrinting_rep
        {
            get { return m_app.aFHAOtherDebtPmtMCAWPrinting_rep; }
        }


        public decimal aFHAChildSupportPmt
        {
            get { return m_app.aFHAChildSupportPmt; }
            set
            {
                m_validator.TryToWriteField("aFHAChildSupportPmt", () => m_app.aFHAChildSupportPmt, t => m_app.aFHAChildSupportPmt = t, value);
            }
        }


        public string aFHAChildSupportPmt_rep
        {
            get { return m_app.aFHAChildSupportPmt_rep; }
            set
            {
                m_validator.TryToWriteField("aFHAChildSupportPmt_rep", () => m_app.aFHAChildSupportPmt_rep, t => m_app.aFHAChildSupportPmt_rep = t, value);
            }
        }


        public decimal aFHADebtInstallPmt
        {
            get { return m_app.aFHADebtInstallPmt; }
            set
            {
                m_validator.TryToWriteField("aFHADebtInstallPmt", () => m_app.aFHADebtInstallPmt, t => m_app.aFHADebtInstallPmt = t, value);
            }
        }


        public string aFHADebtInstallPmt_rep
        {
            get { return m_app.aFHADebtInstallPmt_rep; }
            set
            {
                m_validator.TryToWriteField("aFHADebtInstallPmt_rep", () => m_app.aFHADebtInstallPmt_rep, t => m_app.aFHADebtInstallPmt_rep = t, value);
            }
        }


        public decimal aFHAAssetAvail
        {
            get { return m_app.aFHAAssetAvail; }
            set
            {
                m_validator.TryToWriteField("aFHAAssetAvail", () => m_app.aFHAAssetAvail, t => m_app.aFHAAssetAvail = t, value);
            }
        }


        public string aFHAAssetAvail_rep
        {
            get { return m_app.aFHAAssetAvail_rep; }
            set
            {
                m_validator.TryToWriteField("aFHAAssetAvail_rep", () => m_app.aFHAAssetAvail_rep, t => m_app.aFHAAssetAvail_rep = t, value);
            }
        }


        public string aFHAGiftFundSrc
        {
            get { return m_app.aFHAGiftFundSrc; }
            set
            {
                m_validator.TryToWriteField("aFHAGiftFundSrc", () => m_app.aFHAGiftFundSrc, t => m_app.aFHAGiftFundSrc = t, value);
            }
        }


        public decimal aFHAGiftFundAmt
        {
            get { return m_app.aFHAGiftFundAmt; }
            set
            {
                m_validator.TryToWriteField("aFHAGiftFundAmt", () => m_app.aFHAGiftFundAmt, t => m_app.aFHAGiftFundAmt = t, value);
            }
        }


        public string aFHAGiftFundAmt_rep
        {
            get { return m_app.aFHAGiftFundAmt_rep; }
            set
            {
                m_validator.TryToWriteField("aFHAGiftFundAmt_rep", () => m_app.aFHAGiftFundAmt_rep, t => m_app.aFHAGiftFundAmt_rep = t, value);
            }
        }


        public bool aBAddrMailUsePresentAddr
        {
            get { return m_app.aBAddrMailUsePresentAddr; }
            set
            {
                m_validator.TryToWriteField("aBAddrMailUsePresentAddr", () => m_app.aBAddrMailUsePresentAddr, t => m_app.aBAddrMailUsePresentAddr = t, value);
            }
        }


        public bool aCAddrMailUsePresentAddr
        {
            get { return m_app.aCAddrMailUsePresentAddr; }
            set
            {
                m_validator.TryToWriteField("aCAddrMailUsePresentAddr", () => m_app.aCAddrMailUsePresentAddr, t => m_app.aCAddrMailUsePresentAddr = t, value);
            }
        }

        public E_aAddrMailSourceT aBAddrMailSourceT
        {
            get { return m_app.aBAddrMailSourceT; }
            set
            {
                m_validator.TryToWriteField("aBAddrMailSourceT", () => m_app.aBAddrMailSourceT, t => m_app.aBAddrMailSourceT = t, value);
            }
        }

        public E_aAddrMailSourceT aCAddrMailSourceT
        {
            get { return m_app.aCAddrMailSourceT; }
            set
            {
                m_validator.TryToWriteField("aCAddrMailSourceT", () => m_app.aCAddrMailSourceT, t => m_app.aCAddrMailSourceT = t, value);
            }
        }

        public E_aAddrMailSourceT aAddrMailSourceT
        {
            get { return m_app.aAddrMailSourceT; }
            set
            {
                m_validator.TryToWriteField("aAddrMailSourceT", () => m_app.aAddrMailSourceT, t => m_app.aAddrMailSourceT = t, value);
            }
        }

        public PostalAddress aBMailingAddress
        {
            get { return this.m_app.aBMailingAddress; }
        }

        public PostalAddress aCMailingAddress
        {
            get { return this.m_app.aCMailingAddress; }
        }

        public PostalAddress aBMailingAddressData
        {
            get { return this.m_app.aBMailingAddressData; }
            set { this.m_validator.TryToWriteField(nameof(aBMailingAddressData), () => this.m_app.aBMailingAddressData, t => this.m_app.aBMailingAddressData = t, value); }
        }

        public PostalAddress aCMailingAddressData
        {
            get { return this.m_app.aCMailingAddressData; }
            set { this.m_validator.TryToWriteField(nameof(aCMailingAddressData), () => this.m_app.aCMailingAddressData, t => this.m_app.aCMailingAddressData = t, value); }
        }

        public string aBAddrMail
        {
            get { return m_app.aBAddrMail; }
            set
            {
                m_validator.TryToWriteField("aBAddrMail", () => m_app.aBAddrMail, t => m_app.aBAddrMail = t, value);
            }
        }

        public string aCAddrMail
        {
            get { return m_app.aCAddrMail; }
            set
            {
                m_validator.TryToWriteField("aCAddrMail", () => m_app.aCAddrMail, t => m_app.aCAddrMail = t, value);
            }
        }

        public string aBCityMail
        {
            get { return m_app.aBCityMail; }
            set
            {
                m_validator.TryToWriteField("aBCityMail", () => m_app.aBCityMail, t => m_app.aBCityMail = t, value);
            }
        }

        public string aCCityMail
        {
            get { return m_app.aCCityMail; }
            set
            {
                m_validator.TryToWriteField("aCCityMail", () => m_app.aCCityMail, t => m_app.aCCityMail = t, value);
            }
        }

        public string aBStateMail
        {
            get { return m_app.aBStateMail; }
            set
            {
                m_validator.TryToWriteField("aBStateMail", () => m_app.aBStateMail, t => m_app.aBStateMail = t, value);
            }
        }

        public string aCStateMail
        {
            get { return m_app.aCStateMail; }
            set
            {
                m_validator.TryToWriteField("aCStateMail", () => m_app.aCStateMail, t => m_app.aCStateMail = t, value);
            }
        }

        public string aBAddrMail_MultiLine
        {
            get { return m_app.aBAddrMail_MultiLine; }
        }

        public string aBAddrMail_SingleLine
        {
            get { return m_app.aBAddrMail_SingleLine; }
        }

        public string aCAddrMail_MultiLine
        {
            get { return m_app.aCAddrMail_MultiLine; }
        }

        public string aCAddrMail_SingleLine
        {
            get { return m_app.aCAddrMail_SingleLine; }
        }

        public string aBZipMail
        {
            get { return m_app.aBZipMail; }
            set
            {
                m_validator.TryToWriteField("aBZipMail", () => m_app.aBZipMail, t => m_app.aBZipMail = t, value);
            }
        }

        public string aCZipMail
        {
            get { return m_app.aCZipMail; }
            set
            {
                m_validator.TryToWriteField("aCZipMail", () => m_app.aCZipMail, t => m_app.aCZipMail = t, value);
            }
        }

        public bool aBAddrPostSourceTLckd
        {
            get { return m_app.aBAddrPostSourceTLckd; }
            set
            {
                m_validator.TryToWriteField("aBAddrPostSourceTLckd", () => m_app.aBAddrPostSourceTLckd, t => m_app.aBAddrPostSourceTLckd = t, value);
            }
        }

        public bool aCAddrPostSourceTLckd
        {
            get { return m_app.aCAddrPostSourceTLckd; }
            set
            {
                m_validator.TryToWriteField("aCAddrPostSourceTLckd", () => m_app.aCAddrPostSourceTLckd, t => m_app.aCAddrPostSourceTLckd = t, value);
            }
        }

        public E_aAddrPostSourceT aBAddrPostSourceT
        {
            get { return m_app.aBAddrPostSourceT; }
            set
            {
                m_validator.TryToWriteField("aBAddrPostSourceT", () => m_app.aBAddrPostSourceT, t => m_app.aBAddrPostSourceT = t, value);
            }
        }

        public E_aAddrPostSourceT aCAddrPostSourceT
        {
            get { return m_app.aCAddrPostSourceT; }
            set
            {
                m_validator.TryToWriteField("aCAddrPostSourceT", () => m_app.aCAddrPostSourceT, t => m_app.aCAddrPostSourceT = t, value);
            }
        }

        public PostalAddress aBPostClosingAddress
        {
            get { return this.m_app.aBPostClosingAddress; }
        }

        public PostalAddress aCPostClosingAddress
        {
            get { return this.m_app.aCPostClosingAddress; }
        }

        public PostalAddress aBPostClosingAddressData
        {
            get { return this.m_app.aBPostClosingAddressData; }
            set { this.m_validator.TryToWriteField(nameof(aBPostClosingAddressData), () => this.m_app.aBPostClosingAddressData, t => this.m_app.aBPostClosingAddressData = t, value); }
        }

        public PostalAddress aCPostClosingAddressData
        {
            get { return this.m_app.aCPostClosingAddressData; }
            set { this.m_validator.TryToWriteField(nameof(aCPostClosingAddressData), () => this.m_app.aCPostClosingAddressData, t => this.m_app.aCPostClosingAddressData = t, value); }
        }

        public string aBAddrPost
        {
            get { return m_app.aBAddrPost; }
            set
            {
                m_validator.TryToWriteField("aBAddrPost", () => m_app.aBAddrPost, t => m_app.aBAddrPost = t, value);
            }
        }


        public string aCAddrPost
        {
            get { return m_app.aCAddrPost; }
            set
            {
                m_validator.TryToWriteField("aCAddrPost", () => m_app.aCAddrPost, t => m_app.aCAddrPost = t, value);
            }
        }


        public string aBCityPost
        {
            get { return m_app.aBCityPost; }
            set
            {
                m_validator.TryToWriteField("aBCityPost", () => m_app.aBCityPost, t => m_app.aBCityPost = t, value);
            }
        }


        public string aCCityPost
        {
            get { return m_app.aCCityPost; }
            set
            {
                m_validator.TryToWriteField("aCCityPost", () => m_app.aCCityPost, t => m_app.aCCityPost = t, value);
            }
        }


        public string aBStatePost
        {
            get { return m_app.aBStatePost; }
            set
            {
                m_validator.TryToWriteField("aBStatePost", () => m_app.aBStatePost, t => m_app.aBStatePost = t, value);
            }
        }


        public string aCStatePost
        {
            get { return m_app.aCStatePost; }
            set
            {
                m_validator.TryToWriteField("aCStatePost", () => m_app.aCStatePost, t => m_app.aCStatePost = t, value);
            }
        }


        public string aBAddrPost_MultiLine
        {
            get { return m_app.aBAddrPost_MultiLine; }
        }


        public string aBAddrPost_SingleLine
        {
            get { return m_app.aBAddrPost_SingleLine; }
        }


        public string aCAddrPost_MultiLine
        {
            get { return m_app.aCAddrPost_MultiLine; }
        }


        public string aCAddrPost_SingleLine
        {
            get { return m_app.aCAddrPost_SingleLine; }
        }


        public string aBZipPost
        {
            get { return m_app.aBZipPost; }
            set
            {
                m_validator.TryToWriteField("aBZipPost", () => m_app.aBZipPost, t => m_app.aBZipPost = t, value);
            }
        }


        public string aCZipPost
        {
            get { return m_app.aCZipPost; }
            set
            {
                m_validator.TryToWriteField("aCZipPost", () => m_app.aCZipPost, t => m_app.aCZipPost = t, value);
            }
        }

        public string aBMidNm
        {
            get { return m_app.aBMidNm; }
            set
            {
                m_validator.TryToWriteField("aBMidNm", () => m_app.aBMidNm, t => m_app.aBMidNm = t, value);
            }
        }


        public string aCMidNm
        {
            get { return m_app.aCMidNm; }
            set
            {
                m_validator.TryToWriteField("aCMidNm", () => m_app.aCMidNm, t => m_app.aCMidNm = t, value);
            }
        }


        public string aBSuffix
        {
            get { return m_app.aBSuffix; }
            set
            {
                m_validator.TryToWriteField("aBSuffix", () => m_app.aBSuffix, t => m_app.aBSuffix = t, value);
            }
        }


        public string aCSuffix
        {
            get { return m_app.aCSuffix; }
            set
            {
                m_validator.TryToWriteField("aCSuffix", () => m_app.aCSuffix, t => m_app.aCSuffix = t, value);
            }
        }


        public string aBCellPhone
        {
            get { return m_app.aBCellPhone; }
            set
            {
                m_validator.TryToWriteField("aBCellPhone", () => m_app.aBCellPhone, t => m_app.aBCellPhone = t, value);
            }
        }


        public string aCCellPhone
        {
            get { return m_app.aCCellPhone; }
            set
            {
                m_validator.TryToWriteField("aCCellPhone", () => m_app.aCCellPhone, t => m_app.aCCellPhone = t, value);
            }
        }


        public bool aBIsPrimaryWageEarner
        {
            get { return m_app.aBIsPrimaryWageEarner; }
            set
            {
                m_validator.TryToWriteField("aBIsPrimaryWageEarner", () => m_app.aBIsPrimaryWageEarner, t => m_app.aBIsPrimaryWageEarner = t, value);
            }
        }


        public bool aCIsPrimaryWageEarner
        {
            get { return m_app.aCIsPrimaryWageEarner; }
            set
            {
                m_validator.TryToWriteField("aCIsPrimaryWageEarner", () => m_app.aCIsPrimaryWageEarner, t => m_app.aCIsPrimaryWageEarner = t, value);
            }
        }

        public bool aIsAsian
        {
            get { return m_app.aIsAsian; }
            set
            {
                m_validator.TryToWriteField("aIsAsian", () => m_app.aIsAsian, t => m_app.aIsAsian = t, value);
            }
        }


        public bool aBIsAsian
        {
            get { return m_app.aBIsAsian; }
            set
            {
                m_validator.TryToWriteField("aBIsAsian", () => m_app.aBIsAsian, t => m_app.aBIsAsian = t, value);
            }
        }

        public bool aCIsAsian
        {
            get
            {
                return m_app.aCIsAsian;
            }
            set
            {
                m_validator.TryToWriteField("aCIsAsian", () => m_app.aCIsAsian, t => m_app.aCIsAsian = t, value);
            }
        }

        public bool aIsAsianIndian
        {
            get
            {
                return this.m_app.aIsAsianIndian;
            }
            set
            {
                this.m_validator.TryToWriteField("aIsAsianIndian", () => this.m_app.aIsAsianIndian, t => this.m_app.aIsAsianIndian = t, value);
            }
        }

        public bool aBIsAsianIndian
        {
            get
            {
                return this.m_app.aBIsAsianIndian;
            }
            set
            {
                this.m_validator.TryToWriteField("aBIsAsianIndian", () => this.m_app.aBIsAsianIndian, t => this.m_app.aBIsAsianIndian = t, value);
            }
        }

        public bool aCIsAsianIndian
        {
            get
            {
                return this.m_app.aCIsAsianIndian;
            }
            set
            {
                this.m_validator.TryToWriteField("aCIsAsianIndian", () => this.m_app.aCIsAsianIndian, t => this.m_app.aCIsAsianIndian = t, value);
            }
        }

        public bool aIsChinese
        {
            get
            {
                return this.m_app.aIsChinese;
            }
            set
            {
                this.m_validator.TryToWriteField("aIsChinese", () => this.m_app.aIsChinese, t => this.m_app.aIsChinese = t, value);
            }
        }

        public bool aBIsChinese
        {
            get
            {
                return this.m_app.aBIsChinese;
            }
            set
            {
                this.m_validator.TryToWriteField("aBIsChinese", () => this.m_app.aBIsChinese, t => this.m_app.aBIsChinese = t, value);
            }
        }

        public bool aCIsChinese
        {
            get
            {
                return this.m_app.aCIsChinese;
            }
            set
            {
                this.m_validator.TryToWriteField("aCIsChinese", () => this.m_app.aCIsChinese, t => this.m_app.aCIsChinese = t, value);
            }
        }

        public bool aIsFilipino
        {
            get
            {
                return this.m_app.aIsFilipino;
            }
            set
            {
                this.m_validator.TryToWriteField("aIsFilipino", () => this.m_app.aIsFilipino, t => this.m_app.aIsFilipino = t, value);
            }
        }

        public bool aBIsFilipino
        {
            get
            {
                return this.m_app.aBIsFilipino;
            }
            set
            {
                this.m_validator.TryToWriteField("aBIsFilipino", () => this.m_app.aBIsFilipino, t => this.m_app.aBIsFilipino = t, value);
            }
        }

        public bool aCIsFilipino
        {
            get
            {
                return this.m_app.aCIsFilipino;
            }
            set
            {
                this.m_validator.TryToWriteField("aCIsFilipino", () => this.m_app.aCIsFilipino, t => this.m_app.aCIsFilipino = t, value);
            }
        }

        public bool aIsJapanese
        {
            get
            {
                return this.m_app.aIsJapanese;
            }
            set
            {
                this.m_validator.TryToWriteField("aIsJapanese", () => this.m_app.aIsJapanese, t => this.m_app.aIsJapanese = t, value);
            }
        }

        public bool aBIsJapanese
        {
            get
            {
                return this.m_app.aBIsJapanese;
            }
            set
            {
                this.m_validator.TryToWriteField("aBIsJapanese", () => this.m_app.aBIsJapanese, t => this.m_app.aBIsJapanese = t, value);
            }
        }

        public bool aCIsJapanese
        {
            get
            {
                return this.m_app.aCIsJapanese;
            }
            set
            {
                this.m_validator.TryToWriteField("aCIsJapanese", () => this.m_app.aCIsJapanese, t => this.m_app.aCIsJapanese = t, value);
            }
        }

        public bool aIsKorean
        {
            get
            {
                return this.m_app.aIsKorean;
            }
            set
            {
                this.m_validator.TryToWriteField("aIsKorean", () => this.m_app.aIsKorean, t => this.m_app.aIsKorean = t, value);
            }
        }

        public bool aBIsKorean
        {
            get
            {
                return this.m_app.aBIsKorean;
            }
            set
            {
                this.m_validator.TryToWriteField("aBIsKorean", () => this.m_app.aBIsKorean, t => this.m_app.aBIsKorean = t, value);
            }
        }

        public bool aCIsKorean
        {
            get
            {
                return this.m_app.aCIsKorean;
            }
            set
            {
                this.m_validator.TryToWriteField("aCIsKorean", () => this.m_app.aCIsKorean, t => this.m_app.aCIsKorean = t, value);
            }
        }

        public bool aIsVietnamese
        {
            get
            {
                return this.m_app.aIsVietnamese;
            }
            set
            {
                this.m_validator.TryToWriteField("aIsVietnamese", () => this.m_app.aIsVietnamese, t => this.m_app.aIsVietnamese = t, value);
            }
        }

        public bool aBIsVietnamese
        {
            get
            {
                return this.m_app.aBIsVietnamese;
            }
            set
            {
                this.m_validator.TryToWriteField("aBIsVietnamese", () => this.m_app.aBIsVietnamese, t => this.m_app.aBIsVietnamese = t, value);
            }
        }

        public bool aCIsVietnamese
        {
            get
            {
                return this.m_app.aCIsVietnamese;
            }
            set
            {
                this.m_validator.TryToWriteField("aCIsVietnamese", () => this.m_app.aCIsVietnamese, t => this.m_app.aCIsVietnamese = t, value);
            }
        }

        public bool aIsOtherAsian
        {
            get
            {
                return this.m_app.aIsOtherAsian;
            }
            set
            {
                this.m_validator.TryToWriteField("aIsOtherAsian", () => this.m_app.aIsOtherAsian, t => this.m_app.aIsOtherAsian = t, value);
            }
        }

        public bool aBIsOtherAsian
        {
            get
            {
                return this.m_app.aBIsOtherAsian;
            }
            set
            {
                this.m_validator.TryToWriteField("aBIsOtherAsian", () => this.m_app.aBIsOtherAsian, t => this.m_app.aBIsOtherAsian = t, value);
            }
        }

        public bool aCIsOtherAsian
        {
            get
            {
                return this.m_app.aCIsOtherAsian;
            }
            set
            {
                this.m_validator.TryToWriteField("aCIsOtherAsian", () => this.m_app.aCIsOtherAsian, t => this.m_app.aCIsOtherAsian = t, value);
            }
        }

        public string aOtherAsianDescription
        {
            get
            {
                return this.m_app.aOtherAsianDescription;
            }

            set
            {
                this.m_validator.TryToWriteField("aOtherAsianDescription", () => this.m_app.aOtherAsianDescription, t => this.m_app.aOtherAsianDescription = t, value);
            }
        }

        public string aBOtherAsianDescription
        {
            get
            {
                return this.m_app.aBOtherAsianDescription;
            }

            set
            {
                this.m_validator.TryToWriteField("aBOtherAsianDescription", () => this.m_app.aBOtherAsianDescription, t => this.m_app.aBOtherAsianDescription = t, value);
            }
        }

        public string aCOtherAsianDescription
        {
            get
            {
                return this.m_app.aCOtherAsianDescription;
            }

            set
            {
                this.m_validator.TryToWriteField("aCOtherAsianDescription", () => this.m_app.aCOtherAsianDescription, t => this.m_app.aCOtherAsianDescription = t, value);
            }
        }

        public bool aIsWhite
        {
            get { return m_app.aIsWhite; }
            set
            {
                m_validator.TryToWriteField("aIsWhite", () => m_app.aIsWhite, t => m_app.aIsWhite = t, value);
            }
        }

        public bool aBIsWhite
        {
            get { return m_app.aBIsWhite; }
            set
            {
                m_validator.TryToWriteField("aBIsWhite", () => m_app.aBIsWhite, t => m_app.aBIsWhite = t, value);
            }
        }

        public bool aCIsWhite
        {
            get { return m_app.aCIsWhite; }
            set
            {
                m_validator.TryToWriteField("aCIsWhite", () => m_app.aCIsWhite, t => m_app.aCIsWhite = t, value);
            }
        }

        public bool aIsBlack
        {
            get { return m_app.aIsBlack; }
            set
            {
                m_validator.TryToWriteField("aIsBlack", () => m_app.aIsBlack, t => m_app.aIsBlack = t, value);
            }
        }

        public bool aBIsBlack
        {
            get { return m_app.aBIsBlack; }
            set
            {
                m_validator.TryToWriteField("aBIsBlack", () => m_app.aBIsBlack, t => m_app.aBIsBlack = t, value);
            }
        }

        public bool aCIsBlack
        {
            get { return m_app.aCIsBlack; }
            set
            {
                m_validator.TryToWriteField("aCIsBlack", () => m_app.aCIsBlack, t => m_app.aCIsBlack = t, value);
            }
        }

        public bool aIsAmericanIndian
        {
            get
            {
                return m_app.aIsAmericanIndian;
            }
            set
            {
                m_validator.TryToWriteField("aIsAmericanIndian", () => m_app.aIsAmericanIndian, t => m_app.aIsAmericanIndian = t, value);
            }
        }

        public bool aBIsAmericanIndian
        {
            get
            {
                return m_app.aBIsAmericanIndian;
            }
            set
            {
                m_validator.TryToWriteField("aBIsAmericanIndian", () => m_app.aBIsAmericanIndian, t => m_app.aBIsAmericanIndian = t, value);
            }
        }

        public bool aCIsAmericanIndian
        {
            get
            {
                return m_app.aCIsAmericanIndian;
            }
            set
            {
                m_validator.TryToWriteField("aCIsAmericanIndian", () => m_app.aCIsAmericanIndian, t => m_app.aCIsAmericanIndian = t, value);
            }
        }

        public string aOtherAmericanIndianDescription
        {
            get
            {
                return this.m_app.aOtherAmericanIndianDescription;
            }

            set
            {
                this.m_validator.TryToWriteField("aOtherAmericanIndianDescription", () => m_app.aOtherAmericanIndianDescription, t => m_app.aOtherAmericanIndianDescription = t, value);
            }
        }

        public string aBOtherAmericanIndianDescription
        {
            get
            {
                return this.m_app.aBOtherAmericanIndianDescription;
            }

            set
            {
                this.m_validator.TryToWriteField("aBOtherAmericanIndianDescription", () => m_app.aBOtherAmericanIndianDescription, t => m_app.aBOtherAmericanIndianDescription = t, value);
            }
        }

        public string aCOtherAmericanIndianDescription
        {
            get
            {
                return this.m_app.aCOtherAmericanIndianDescription;
            }

            set
            {
                this.m_validator.TryToWriteField("aCOtherAmericanIndianDescription", () => m_app.aCOtherAmericanIndianDescription, t => m_app.aCOtherAmericanIndianDescription = t, value);
            }
        }

        public bool aIsPacificIslander
        {
            get
            {
                return m_app.aIsPacificIslander;
            }
            set
            {
                m_validator.TryToWriteField("aIsPacificIslander", () => m_app.aIsPacificIslander, t => m_app.aIsPacificIslander = t, value);
            }
        }

        public bool aBIsPacificIslander
        {
            get
            {
                return m_app.aBIsPacificIslander;
            }
            set
            {
                m_validator.TryToWriteField("aBIsPacificIslander", () => m_app.aBIsPacificIslander, t => m_app.aBIsPacificIslander = t, value);
            }
        }

        public bool aCIsPacificIslander
        {
            get { return m_app.aCIsPacificIslander; }
            set
            {
                m_validator.TryToWriteField("aCIsPacificIslander", () => m_app.aCIsPacificIslander, t => m_app.aCIsPacificIslander = t, value);
            }
        }

        public bool aIsNativeHawaiian
        {
            get
            {
                return this.m_app.aIsNativeHawaiian;
            }
            set
            {
                this.m_validator.TryToWriteField("aIsNativeHawaiian", () => this.m_app.aIsNativeHawaiian, t => this.m_app.aIsNativeHawaiian = t, value);
            }
        }

        public bool aBIsNativeHawaiian
        {
            get
            {
                return this.m_app.aBIsNativeHawaiian;
            }
            set
            {
                this.m_validator.TryToWriteField("aBIsNativeHawaiian", () => this.m_app.aBIsNativeHawaiian, t => this.m_app.aBIsNativeHawaiian = t, value);
            }
        }

        public bool aCIsNativeHawaiian
        {
            get
            {
                return this.m_app.aCIsNativeHawaiian;
            }
            set
            {
                this.m_validator.TryToWriteField("aCIsNativeHawaiian", () => this.m_app.aCIsNativeHawaiian, t => this.m_app.aCIsNativeHawaiian = t, value);
            }
        }

        public bool aIsGuamanianOrChamorro
        {
            get
            {
                return this.m_app.aIsGuamanianOrChamorro;
            }
            set
            {
                this.m_validator.TryToWriteField("aIsGuamanianOrChamorro", () => this.m_app.aIsGuamanianOrChamorro, t => this.m_app.aIsGuamanianOrChamorro = t, value);
            }
        }

        public bool aBIsGuamanianOrChamorro
        {
            get
            {
                return this.m_app.aBIsGuamanianOrChamorro;
            }
            set
            {
                this.m_validator.TryToWriteField("aBIsGuamanianOrChamorro", () => this.m_app.aBIsGuamanianOrChamorro, t => this.m_app.aBIsGuamanianOrChamorro = t, value);
            }
        }

        public bool aCIsGuamanianOrChamorro
        {
            get
            {
                return this.m_app.aCIsGuamanianOrChamorro;
            }
            set
            {
                this.m_validator.TryToWriteField("aCIsGuamanianOrChamorro", () => this.m_app.aCIsGuamanianOrChamorro, t => this.m_app.aCIsGuamanianOrChamorro = t, value);
            }
        }

        public bool aIsSamoan
        {
            get
            {
                return this.m_app.aIsSamoan;
            }
            set
            {
                this.m_validator.TryToWriteField("aIsSamoan", () => this.m_app.aIsSamoan, t => this.m_app.aIsSamoan = t, value);
            }
        }

        public bool aBIsSamoan
        {
            get
            {
                return this.m_app.aBIsSamoan;
            }
            set
            {
                this.m_validator.TryToWriteField("aBIsSamoan", () => this.m_app.aBIsSamoan, t => this.m_app.aBIsSamoan = t, value);
            }
        }

        public bool aCIsSamoan
        {
            get
            {
                return this.m_app.aCIsSamoan;
            }
            set
            {
                this.m_validator.TryToWriteField("aCIsSamoan", () => this.m_app.aCIsSamoan, t => this.m_app.aCIsSamoan = t, value);
            }
        }

        public bool aIsOtherPacificIslander
        {
            get
            {
                return this.m_app.aIsOtherPacificIslander;
            }
            set
            {
                this.m_validator.TryToWriteField("aIsOtherPacificIslander", () => this.m_app.aIsOtherPacificIslander, t => this.m_app.aIsOtherPacificIslander = t, value);
            }
        }

        public bool aBIsOtherPacificIslander
        {
            get
            {
                return this.m_app.aBIsOtherPacificIslander;
            }
            set
            {
                this.m_validator.TryToWriteField("aBIsOtherPacificIslander", () => this.m_app.aBIsOtherPacificIslander, t => this.m_app.aBIsOtherPacificIslander = t, value);
            }
        }

        public bool aCIsOtherPacificIslander
        {
            get
            {
                return this.m_app.aCIsOtherPacificIslander;
            }
            set
            {
                this.m_validator.TryToWriteField("aCIsOtherPacificIslander", () => this.m_app.aCIsOtherPacificIslander, t => this.m_app.aCIsOtherPacificIslander = t, value);
            }
        }

        public string aOtherPacificIslanderDescription
        {
            get
            {
                return this.m_app.aOtherPacificIslanderDescription;
            }

            set
            {
                this.m_validator.TryToWriteField("aOtherPacificIslanderDescription", () => this.m_app.aOtherPacificIslanderDescription, t => this.m_app.aOtherPacificIslanderDescription = t, value);
            }
        }

        public string aBOtherPacificIslanderDescription
        {
            get
            {
                return this.m_app.aBOtherPacificIslanderDescription;
            }

            set
            {
                this.m_validator.TryToWriteField("aBOtherPacificIslanderDescription", () => this.m_app.aBOtherPacificIslanderDescription, t => this.m_app.aBOtherPacificIslanderDescription = t, value);
            }
        }

        public string aCOtherPacificIslanderDescription
        {
            get
            {
                return this.m_app.aCOtherPacificIslanderDescription;
            }

            set
            {
                this.m_validator.TryToWriteField("aCOtherPacificIslanderDescription", () => this.m_app.aCOtherPacificIslanderDescription, t => this.m_app.aCOtherPacificIslanderDescription = t, value);
            }
        }

        public E_aHispanicT aBHispanicT
        {
            get { return m_app.aBHispanicT; }
            set
            {
                m_validator.TryToWriteField("aBHispanicT", () => m_app.aBHispanicT, t => m_app.aBHispanicT = t, value);
            }
        }

        public E_aHispanicT aBHispanicTFallback
        {
            get
            {
                return m_app.aBHispanicTFallback;
            }

            set
            {
                this.m_validator.TryToWriteField("aBHispanicTFallback", () => this.m_app.aBHispanicTFallback, t => this.m_app.aBHispanicTFallback = t, value);
            }
        }

        public E_aHispanicT aCHispanicT
        {
            get { return m_app.aCHispanicT; }
            set
            {
                m_validator.TryToWriteField("aCHispanicT", () => m_app.aCHispanicT, t => m_app.aCHispanicT = t, value);
            }
        }

        public E_aHispanicT aCHispanicTFallback
        {
            get
            {
                return this.m_app.aCHispanicTFallback;
            }

            set
            {
                this.m_validator.TryToWriteField("aCHispanicTFallback", () => this.m_app.aCHispanicTFallback, t => this.m_app.aCHispanicTFallback = t, value);
            }
        }

        public bool aIsMexican
        {
            get
            {
                return this.m_app.aIsMexican;
            }
            set
            {
                this.m_validator.TryToWriteField("aIsMexican", () => this.m_app.aIsMexican, t => this.m_app.aIsMexican = t, value);
            }
        }

        public bool aBIsMexican
        {
            get
            {
                return this.m_app.aBIsMexican;
            }
            set
            {
                this.m_validator.TryToWriteField("aBIsMexican", () => this.m_app.aBIsMexican, t => this.m_app.aBIsMexican = t, value);
            }
        }

        public bool aCIsMexican
        {
            get
            {
                return this.m_app.aCIsMexican;
            }
            set
            {
                this.m_validator.TryToWriteField("aCIsMexican", () => this.m_app.aCIsMexican, t => this.m_app.aCIsMexican = t, value);
            }
        }

        public bool aIsPuertoRican
        {
            get
            {
                return this.m_app.aIsPuertoRican;
            }
            set
            {
                this.m_validator.TryToWriteField("aIsPuertoRican", () => this.m_app.aIsPuertoRican, t => this.m_app.aIsPuertoRican = t, value);
            }
        }

        public bool aBIsPuertoRican
        {
            get
            {
                return this.m_app.aBIsPuertoRican;
            }
            set
            {
                this.m_validator.TryToWriteField("aBIsPuertoRican", () => this.m_app.aBIsPuertoRican, t => this.m_app.aBIsPuertoRican = t, value);
            }
        }

        public bool aCIsPuertoRican
        {
            get
            {
                return this.m_app.aCIsPuertoRican;
            }
            set
            {
                this.m_validator.TryToWriteField("aCIsPuertoRican", () => this.m_app.aCIsPuertoRican, t => this.m_app.aCIsPuertoRican = t, value);
            }
        }

        public bool aIsCuban
        {
            get
            {
                return this.m_app.aIsCuban;
            }
            set
            {
                this.m_validator.TryToWriteField("aIsCuban", () => this.m_app.aIsCuban, t => this.m_app.aIsCuban = t, value);
            }
        }

        public bool aBIsCuban
        {
            get
            {
                return this.m_app.aBIsCuban;
            }
            set
            {
                this.m_validator.TryToWriteField("aBIsCuban", () => this.m_app.aBIsCuban, t => this.m_app.aBIsCuban = t, value);
            }
        }

        public bool aCIsCuban
        {
            get
            {
                return this.m_app.aCIsCuban;
            }
            set
            {
                this.m_validator.TryToWriteField("aCIsCuban", () => this.m_app.aCIsCuban, t => this.m_app.aCIsCuban = t, value);
            }
        }

        public bool aIsOtherHispanicOrLatino
        {
            get
            {
                return this.m_app.aIsOtherHispanicOrLatino;
            }
            set
            {
                this.m_validator.TryToWriteField("aIsOtherHispanicOrLatino", () => this.m_app.aIsOtherHispanicOrLatino, t => this.m_app.aIsOtherHispanicOrLatino = t, value);
            }
        }

        public bool aBIsOtherHispanicOrLatino
        {
            get
            {
                return this.m_app.aBIsOtherHispanicOrLatino;
            }
            set
            {
                this.m_validator.TryToWriteField("aBIsOtherHispanicOrLatino", () => this.m_app.aBIsOtherHispanicOrLatino, t => this.m_app.aBIsOtherHispanicOrLatino = t, value);
            }
        }

        public bool aCIsOtherHispanicOrLatino
        {
            get
            {
                return this.m_app.aCIsOtherHispanicOrLatino;
            }
            set
            {
                this.m_validator.TryToWriteField("aCIsOtherHispanicOrLatino", () => this.m_app.aCIsOtherHispanicOrLatino, t => this.m_app.aCIsOtherHispanicOrLatino = t, value);
            }
        }

        public string aOtherHispanicOrLatinoDescription
        {
            get
            {
                return this.m_app.aOtherHispanicOrLatinoDescription;
            }

            set
            {
                this.m_validator.TryToWriteField("aOtherHispanicOrLatinoDescription", () => this.m_app.aOtherHispanicOrLatinoDescription, t => this.m_app.aOtherHispanicOrLatinoDescription = t, value);
            }
        }

        public string aBOtherHispanicOrLatinoDescription
        {
            get
            {
                return this.m_app.aBOtherHispanicOrLatinoDescription;
            }

            set
            {
                this.m_validator.TryToWriteField("aBOtherHispanicOrLatinoDescription", () => this.m_app.aBOtherHispanicOrLatinoDescription, t => this.m_app.aBOtherHispanicOrLatinoDescription = t, value);
            }
        }

        public string aCOtherHispanicOrLatinoDescription
        {
            get
            {
                return this.m_app.aCOtherHispanicOrLatinoDescription;
            }

            set
            {
                this.m_validator.TryToWriteField("aCOtherHispanicOrLatinoDescription", () => this.m_app.aCOtherHispanicOrLatinoDescription, t => this.m_app.aCOtherHispanicOrLatinoDescription = t, value);
            }
        }

        public bool aDoesNotWishToProvideEthnicity
        {
            get
            {
                return this.m_app.aDoesNotWishToProvideEthnicity;
            }
            set
            {
                this.m_validator.TryToWriteField("aDoesNotWishToProvideEthnicity", () => this.m_app.aDoesNotWishToProvideEthnicity, t => this.m_app.aDoesNotWishToProvideEthnicity = t, value);
            }
        }

        public bool aBDoesNotWishToProvideEthnicity
        {
            get
            {
                return this.m_app.aBDoesNotWishToProvideEthnicity;
            }
            set
            {
                this.m_validator.TryToWriteField("aBDoesNotWishToProvideEthnicity", () => this.m_app.aBDoesNotWishToProvideEthnicity, t => this.m_app.aBDoesNotWishToProvideEthnicity = t, value);
            }
        }

        public bool aCDoesNotWishToProvideEthnicity
        {
            get
            {
                return this.m_app.aCDoesNotWishToProvideEthnicity;
            }
            set
            {
                this.m_validator.TryToWriteField("aCDoesNotWishToProvideEthnicity", () => this.m_app.aCDoesNotWishToProvideEthnicity, t => this.m_app.aCDoesNotWishToProvideEthnicity = t, value);
            }
        }

        public bool aDoesNotWishToProvideRace
        {
            get
            {
                return this.m_app.aDoesNotWishToProvideRace;
            }
            set
            {
                this.m_validator.TryToWriteField("aDoesNotWishToProvideRace", () => this.m_app.aDoesNotWishToProvideRace, t => this.m_app.aDoesNotWishToProvideRace = t, value);
            }
        }

        public bool aBDoesNotWishToProvideRace
        {
            get
            {
                return this.m_app.aBDoesNotWishToProvideRace;
            }
            set
            {
                this.m_validator.TryToWriteField("aBDoesNotWishToProvideRace", () => this.m_app.aBDoesNotWishToProvideRace, t => this.m_app.aBDoesNotWishToProvideRace = t, value);
            }
        }

        public bool aCDoesNotWishToProvideRace
        {
            get
            {
                return this.m_app.aCDoesNotWishToProvideRace;
            }
            set
            {
                this.m_validator.TryToWriteField("aCDoesNotWishToProvideRace", () => this.m_app.aCDoesNotWishToProvideRace, t => this.m_app.aCDoesNotWishToProvideRace = t, value);
            }
        }

        public CDateTime aBAssetsVerifSent
        {
            get { return m_app.aBAssetsVerifSent; }
        }


        public string aBAssetsVerifSent_rep
        {
            get { return m_app.aBAssetsVerifSent_rep; }
        }


        public CDateTime aBAssetsVerifRecv
        {
            get { return m_app.aBAssetsVerifRecv; }
        }


        public string aBAssetsVerifRecv_rep
        {
            get { return m_app.aBAssetsVerifRecv_rep; }
        }


        public decimal aBIlliquidAssets
        {
            get { return m_app.aBIlliquidAssets; }
        }


        public string aBIlliquidAssets_rep
        {
            get { return m_app.aBIlliquidAssets_rep; }
        }


        public decimal aCIlliquidAssets
        {
            get { return m_app.aCIlliquidAssets; }
        }


        public string aCIlliquidAssets_rep
        {
            get { return m_app.aCIlliquidAssets_rep; }
        }


        public decimal aBLiquidAssetsNotIncludingGift
        {
            get { return m_app.aBLiquidAssetsNotIncludingGift; }
        }


        public string aBLiquidAssetsNotIncludingGift_rep
        {
            get { return m_app.aBLiquidAssetsNotIncludingGift_rep; }
        }


        public decimal aCLiquidAssetsNotIncludingGift
        {
            get { return m_app.aCLiquidAssetsNotIncludingGift; }
        }


        public string aCLiquidAssetsNotIncludingGift_rep
        {
            get { return m_app.aCLiquidAssetsNotIncludingGift_rep; }
        }


        public decimal aBOmittedLiaMonPmt
        {
            get { return m_app.aBOmittedLiaMonPmt; }
        }


        public string aBOmittedLiaMonPmt_rep
        {
            get { return m_app.aBOmittedLiaMonPmt_rep; }
        }


        public decimal aCOmittedLiaMonPmt
        {
            get { return m_app.aCOmittedLiaMonPmt; }
        }


        public string aCOmittedLiaMonPmt_rep
        {
            get { return m_app.aCOmittedLiaMonPmt_rep; }
        }


        public decimal aBOmittedLiaBal
        {
            get { return m_app.aBOmittedLiaBal; }
        }


        public string aBOmittedLiaBal_rep
        {
            get { return m_app.aBOmittedLiaBal_rep; }
        }


        public decimal aCOmittedLiaBal
        {
            get { return m_app.aCOmittedLiaBal; }
        }


        public string aCOmittedLiaBal_rep
        {
            get { return m_app.aCOmittedLiaBal_rep; }
        }


        public decimal aBLiaBal
        {
            get { return m_app.aBLiaBal; }
        }


        public string aBLiaBal_rep
        {
            get { return m_app.aBLiaBal_rep; }
        }


        public decimal aCLiaBal
        {
            get { return m_app.aCLiaBal; }
        }


        public string aCLiaBal_rep
        {
            get { return m_app.aCLiaBal_rep; }
        }


        public decimal aBLiaMonTot
        {
            get { return m_app.aBLiaMonTot; }
        }


        public string aBLiaMonTot_rep
        {
            get { return m_app.aBLiaMonTot_rep; }
        }


        public decimal aCLiaMonTot
        {
            get { return m_app.aCLiaMonTot; }
        }


        public string aCLiaMonTot_rep
        {
            get { return m_app.aCLiaMonTot_rep; }
        }

        public string aLiaMonTot_Borr_rep
        {
            get
            {
                return m_app.aLiaMonTot_Borr_rep;
            }
        }

        public decimal aBMonthlyINotIncludingNetRentalI
        {
            get { return m_app.aBMonthlyINotIncludingNetRentalI; }
        }


        public string aBMonthlyINotIncludingNetRentalI_rep
        {
            get { return m_app.aBMonthlyINotIncludingNetRentalI_rep; }
        }


        public decimal aCMonthlyINotIncludingNetRentalI
        {
            get { return m_app.aCMonthlyINotIncludingNetRentalI; }
        }


        public string aCMonthlyINotIncludingNetRentalI_rep
        {
            get { return m_app.aCMonthlyINotIncludingNetRentalI_rep; }
        }


        public CDateTime aBDob
        {
            get { return m_app.aBDob; }
            set
            {
                m_validator.TryToWriteField("aBDob", () => m_app.aBDob, t => m_app.aBDob = t, value);
            }
        }


        public string aBDob_rep
        {
            get { return m_app.aBDob_rep; }
            set
            {
                m_validator.TryToWriteField("aBDob_rep", () => m_app.aBDob_rep, t => m_app.aBDob_rep = t, value);
            }
        }


        public CDateTime aCDob
        {
            get { return m_app.aCDob; }
            set
            {
                m_validator.TryToWriteField("aCDob", () => m_app.aCDob, t => m_app.aCDob = t, value);
            }
        }


        public string aCDob_rep
        {
            get { return m_app.aCDob_rep; }
            set
            {
                m_validator.TryToWriteField("aCDob_rep", () => m_app.aCDob_rep, t => m_app.aCDob_rep = t, value);
            }
        }


        public bool aDenialNoCreditFile
        {
            get { return m_app.aDenialNoCreditFile; }
            set
            {
                m_validator.TryToWriteField("aDenialNoCreditFile", () => m_app.aDenialNoCreditFile, t => m_app.aDenialNoCreditFile = t, value);
            }
        }


        public bool aDenialInsufficientCreditRef
        {
            get { return m_app.aDenialInsufficientCreditRef; }
            set
            {
                m_validator.TryToWriteField("aDenialInsufficientCreditRef", () => m_app.aDenialInsufficientCreditRef, t => m_app.aDenialInsufficientCreditRef = t, value);
            }
        }


        public bool aDenialInsufficientCreditFile
        {
            get { return m_app.aDenialInsufficientCreditFile; }
            set
            {
                m_validator.TryToWriteField("aDenialInsufficientCreditFile", () => m_app.aDenialInsufficientCreditFile, t => m_app.aDenialInsufficientCreditFile = t, value);
            }
        }


        public bool aDenialUnableVerifyCreditRef
        {
            get { return m_app.aDenialUnableVerifyCreditRef; }
            set
            {
                m_validator.TryToWriteField("aDenialUnableVerifyCreditRef", () => m_app.aDenialUnableVerifyCreditRef, t => m_app.aDenialUnableVerifyCreditRef = t, value);
            }
        }


        public bool aDenialGarnishment
        {
            get { return m_app.aDenialGarnishment; }
            set
            {
                m_validator.TryToWriteField("aDenialGarnishment", () => m_app.aDenialGarnishment, t => m_app.aDenialGarnishment = t, value);
            }
        }


        public bool aDenialExcessiveObligations
        {
            get { return m_app.aDenialExcessiveObligations; }
            set
            {
                m_validator.TryToWriteField("aDenialExcessiveObligations", () => m_app.aDenialExcessiveObligations, t => m_app.aDenialExcessiveObligations = t, value);
            }
        }


        public bool aDenialInsufficientIncome
        {
            get { return m_app.aDenialInsufficientIncome; }
            set
            {
                m_validator.TryToWriteField("aDenialInsufficientIncome", () => m_app.aDenialInsufficientIncome, t => m_app.aDenialInsufficientIncome = t, value);
            }
        }


        public bool aDenialUnacceptablePmtRecord
        {
            get { return m_app.aDenialUnacceptablePmtRecord; }
            set
            {
                m_validator.TryToWriteField("aDenialUnacceptablePmtRecord", () => m_app.aDenialUnacceptablePmtRecord, t => m_app.aDenialUnacceptablePmtRecord = t, value);
            }
        }


        public bool aDenialLackOfCashReserves
        {
            get { return m_app.aDenialLackOfCashReserves; }
            set
            {
                m_validator.TryToWriteField("aDenialLackOfCashReserves", () => m_app.aDenialLackOfCashReserves, t => m_app.aDenialLackOfCashReserves = t, value);
            }
        }


        public bool aDenialDeliquentCreditObligations
        {
            get { return m_app.aDenialDeliquentCreditObligations; }
            set
            {
                m_validator.TryToWriteField("aDenialDeliquentCreditObligations", () => m_app.aDenialDeliquentCreditObligations, t => m_app.aDenialDeliquentCreditObligations = t, value);
            }
        }


        public bool aDenialBankruptcy
        {
            get { return m_app.aDenialBankruptcy; }
            set
            {
                m_validator.TryToWriteField("aDenialBankruptcy", () => m_app.aDenialBankruptcy, t => m_app.aDenialBankruptcy = t, value);
            }
        }


        public bool aDenialInfoFromConsumerReportAgency
        {
            get { return m_app.aDenialInfoFromConsumerReportAgency; }
            set
            {
                m_validator.TryToWriteField("aDenialInfoFromConsumerReportAgency", () => m_app.aDenialInfoFromConsumerReportAgency, t => m_app.aDenialInfoFromConsumerReportAgency = t, value);
            }
        }


        public bool aDenialUnableVerifyEmployment
        {
            get { return m_app.aDenialUnableVerifyEmployment; }
            set
            {
                m_validator.TryToWriteField("aDenialUnableVerifyEmployment", () => m_app.aDenialUnableVerifyEmployment, t => m_app.aDenialUnableVerifyEmployment = t, value);
            }
        }


        public bool aDenialLenOfEmployment
        {
            get { return m_app.aDenialLenOfEmployment; }
            set
            {
                m_validator.TryToWriteField("aDenialLenOfEmployment", () => m_app.aDenialLenOfEmployment, t => m_app.aDenialLenOfEmployment = t, value);
            }
        }


        public bool aDenialTemporaryEmployment
        {
            get { return m_app.aDenialTemporaryEmployment; }
            set
            {
                m_validator.TryToWriteField("aDenialTemporaryEmployment", () => m_app.aDenialTemporaryEmployment, t => m_app.aDenialTemporaryEmployment = t, value);
            }
        }


        public bool aDenialInsufficientIncomeForMortgagePmt
        {
            get { return m_app.aDenialInsufficientIncomeForMortgagePmt; }
            set
            {
                m_validator.TryToWriteField("aDenialInsufficientIncomeForMortgagePmt", () => m_app.aDenialInsufficientIncomeForMortgagePmt, t => m_app.aDenialInsufficientIncomeForMortgagePmt = t, value);
            }
        }


        public bool aDenialUnableVerifyIncome
        {
            get { return m_app.aDenialUnableVerifyIncome; }
            set
            {
                m_validator.TryToWriteField("aDenialUnableVerifyIncome", () => m_app.aDenialUnableVerifyIncome, t => m_app.aDenialUnableVerifyIncome = t, value);
            }
        }


        public bool aDenialTempResidence
        {
            get { return m_app.aDenialTempResidence; }
            set
            {
                m_validator.TryToWriteField("aDenialTempResidence", () => m_app.aDenialTempResidence, t => m_app.aDenialTempResidence = t, value);
            }
        }


        public bool aDenialShortResidencePeriod
        {
            get { return m_app.aDenialShortResidencePeriod; }
            set
            {
                m_validator.TryToWriteField("aDenialShortResidencePeriod", () => m_app.aDenialShortResidencePeriod, t => m_app.aDenialShortResidencePeriod = t, value);
            }
        }


        public bool aDenialUnableVerifyResidence
        {
            get { return m_app.aDenialUnableVerifyResidence; }
            set
            {
                m_validator.TryToWriteField("aDenialUnableVerifyResidence", () => m_app.aDenialUnableVerifyResidence, t => m_app.aDenialUnableVerifyResidence = t, value);
            }
        }


        public bool aDenialByHUD
        {
            get { return m_app.aDenialByHUD; }
            set
            {
                m_validator.TryToWriteField("aDenialByHUD", () => m_app.aDenialByHUD, t => m_app.aDenialByHUD = t, value);
            }
        }


        public bool aDenialByVA
        {
            get { return m_app.aDenialByVA; }
            set
            {
                m_validator.TryToWriteField("aDenialByVA", () => m_app.aDenialByVA, t => m_app.aDenialByVA = t, value);
            }
        }


        public bool aDenialByFedNationalMortAssoc
        {
            get { return m_app.aDenialByFedNationalMortAssoc; }
            set
            {
                m_validator.TryToWriteField("aDenialByFedNationalMortAssoc", () => m_app.aDenialByFedNationalMortAssoc, t => m_app.aDenialByFedNationalMortAssoc = t, value);
            }
        }


        public bool aDenialByFedHomeLoanMortCorp
        {
            get { return m_app.aDenialByFedHomeLoanMortCorp; }
            set
            {
                m_validator.TryToWriteField("aDenialByFedHomeLoanMortCorp", () => m_app.aDenialByFedHomeLoanMortCorp, t => m_app.aDenialByFedHomeLoanMortCorp = t, value);
            }
        }


        public bool aDenialByOther
        {
            get { return m_app.aDenialByOther; }
            set
            {
                m_validator.TryToWriteField("aDenialByOther", () => m_app.aDenialByOther, t => m_app.aDenialByOther = t, value);
            }
        }


        public string aDenialByOtherDesc
        {
            get { return m_app.aDenialByOtherDesc; }
            set
            {
                m_validator.TryToWriteField("aDenialByOtherDesc", () => m_app.aDenialByOtherDesc, t => m_app.aDenialByOtherDesc = t, value);
            }
        }


        public bool aDenialInsufficientFundsToClose
        {
            get { return m_app.aDenialInsufficientFundsToClose; }
            set
            {
                m_validator.TryToWriteField("aDenialInsufficientFundsToClose", () => m_app.aDenialInsufficientFundsToClose, t => m_app.aDenialInsufficientFundsToClose = t, value);
            }
        }


        public bool aDenialCreditAppIncomplete
        {
            get { return m_app.aDenialCreditAppIncomplete; }
            set
            {
                m_validator.TryToWriteField("aDenialCreditAppIncomplete", () => m_app.aDenialCreditAppIncomplete, t => m_app.aDenialCreditAppIncomplete = t, value);
            }
        }


        public bool aDenialInadequateCollateral
        {
            get { return m_app.aDenialInadequateCollateral; }
            set
            {
                m_validator.TryToWriteField("aDenialInadequateCollateral", () => m_app.aDenialInadequateCollateral, t => m_app.aDenialInadequateCollateral = t, value);
            }
        }


        public bool aDenialUnacceptableProp
        {
            get { return m_app.aDenialUnacceptableProp; }
            set
            {
                m_validator.TryToWriteField("aDenialUnacceptableProp", () => m_app.aDenialUnacceptableProp, t => m_app.aDenialUnacceptableProp = t, value);
            }
        }


        public bool aDenialInsufficientPropData
        {
            get { return m_app.aDenialInsufficientPropData; }
            set
            {
                m_validator.TryToWriteField("aDenialInsufficientPropData", () => m_app.aDenialInsufficientPropData, t => m_app.aDenialInsufficientPropData = t, value);
            }
        }


        public bool aDenialUnacceptableAppraisal
        {
            get { return m_app.aDenialUnacceptableAppraisal; }
            set
            {
                m_validator.TryToWriteField("aDenialUnacceptableAppraisal", () => m_app.aDenialUnacceptableAppraisal, t => m_app.aDenialUnacceptableAppraisal = t, value);
            }
        }


        public bool aDenialUnacceptableLeasehold
        {
            get { return m_app.aDenialUnacceptableLeasehold; }
            set
            {
                m_validator.TryToWriteField("aDenialUnacceptableLeasehold", () => m_app.aDenialUnacceptableLeasehold, t => m_app.aDenialUnacceptableLeasehold = t, value);
            }
        }


        public bool aDenialNotGrantCreditToAnyAppOnSuchTermsAndConds
        {
            get { return m_app.aDenialNotGrantCreditToAnyAppOnSuchTermsAndConds; }
            set
            {
                m_validator.TryToWriteField("aDenialNotGrantCreditToAnyAppOnSuchTermsAndConds", () => m_app.aDenialNotGrantCreditToAnyAppOnSuchTermsAndConds, t => m_app.aDenialNotGrantCreditToAnyAppOnSuchTermsAndConds = t, value);
            }
        }


        public bool aDenialWithdrawnByApp
        {
            get { return m_app.aDenialWithdrawnByApp; }
            set
            {
                m_validator.TryToWriteField("aDenialWithdrawnByApp", () => m_app.aDenialWithdrawnByApp, t => m_app.aDenialWithdrawnByApp = t, value);
            }
        }


        public bool aDenialOtherReason1
        {
            get { return m_app.aDenialOtherReason1; }
            set
            {
                m_validator.TryToWriteField("aDenialOtherReason1", () => m_app.aDenialOtherReason1, t => m_app.aDenialOtherReason1 = t, value);
            }
        }


        public string aDenialOtherReason1Desc
        {
            get { return m_app.aDenialOtherReason1Desc; }
            set
            {
                m_validator.TryToWriteField("aDenialOtherReason1Desc", () => m_app.aDenialOtherReason1Desc, t => m_app.aDenialOtherReason1Desc = t, value);
            }
        }


        public bool aDenialOtherReason2
        {
            get { return m_app.aDenialOtherReason2; }
            set
            {
                m_validator.TryToWriteField("aDenialOtherReason2", () => m_app.aDenialOtherReason2, t => m_app.aDenialOtherReason2 = t, value);
            }
        }


        public string aDenialOtherReason2Desc
        {
            get { return m_app.aDenialOtherReason2Desc; }
            set
            {
                m_validator.TryToWriteField("aDenialOtherReason2Desc", () => m_app.aDenialOtherReason2Desc, t => m_app.aDenialOtherReason2Desc = t, value);
            }
        }


        public bool aDenialDecisionBasedOnReportAgency
        {
            get { return m_app.aDenialDecisionBasedOnReportAgency; }
            set
            {
                m_validator.TryToWriteField("aDenialDecisionBasedOnReportAgency", () => m_app.aDenialDecisionBasedOnReportAgency, t => m_app.aDenialDecisionBasedOnReportAgency = t, value);
            }
        }


        public bool aDenialDecisionBasedOnCRA
        {
            get { return m_app.aDenialDecisionBasedOnCRA; }
            set
            {
                m_validator.TryToWriteField("aDenialDecisionBasedOnCRA", () => m_app.aDenialDecisionBasedOnCRA, t => m_app.aDenialDecisionBasedOnCRA = t, value);
            }
        }


        public bool aDenialHasAdditionalStatement
        {
            get { return m_app.aDenialHasAdditionalStatement; }
            set
            {
                m_validator.TryToWriteField("aDenialHasAdditionalStatement", () => m_app.aDenialHasAdditionalStatement, t => m_app.aDenialHasAdditionalStatement = t, value);
            }
        }


        public string aDenialAdditionalStatement
        {
            get { return m_app.aDenialAdditionalStatement; }
            set
            {
                m_validator.TryToWriteField("aDenialAdditionalStatement", () => m_app.aDenialAdditionalStatement, t => m_app.aDenialAdditionalStatement = t, value);
            }
        }


        public Sensitive<ICreditReportView> CreditReportView
        {
            get { return m_app.CreditReportView; }
        }

        public Sensitive<ICreditReportView> LqiCreditReportView
        {
            get { return m_app.LqiCreditReportView; }
        }

        public string aCreditReportId
        {
            get { return this.m_app.aCreditReportId; }
            set
            {
                this.m_validator.TryToWriteField("aCreditReportId", () => this.m_app.aCreditReportId, t => this.m_app.aCreditReportId = t, value);
            }
        }

        public bool aCreditReportIdLckd
        {
            get { return this.m_app.aCreditReportIdLckd; }
            set
            {
                this.m_validator.TryToWriteField("aCreditReportIdLckd", () => this.m_app.aCreditReportIdLckd, t => this.m_app.aCreditReportIdLckd = t, value);
            }
        }

        public Sensitive<string> aCreditReportRawXml
        {
            get { return m_app.aCreditReportRawXml; }
        }


        public Sensitive<string> aCreditReportRawFnmaFormat
        {
            get { return m_app.aCreditReportRawFnmaFormat; }
        }


        public Guid aCreditReportFileDbKey
        {
            get { return m_app.aCreditReportFileDbKey; }
        }


        public E_aBorrowerCreditModeT aBorrowerCreditModeT
        {
            get { return m_app.aBorrowerCreditModeT; }
            set
            {
                m_validator.TryToWriteField("aBorrowerCreditModeT", () => m_app.aBorrowerCreditModeT, t => m_app.aBorrowerCreditModeT = t, value);
            }
        }


        public Sensitive<ICreditReport> CreditReportData
        {
            get { return m_app.CreditReportData; }
        }


        public int aMortgageLates30In12MonthsPmlClientReport
        {
            get { return m_app.aMortgageLates30In12MonthsPmlClientReport; }
        }


        public string aMortgageLates30In12MonthsPmlClientReport_rep
        {
            get { return m_app.aMortgageLates30In12MonthsPmlClientReport_rep; }
        }


        public int aMortgageLates60In12MonthsPmlClientReport
        {
            get { return m_app.aMortgageLates60In12MonthsPmlClientReport; }
        }


        public string aMortgageLates60In12MonthsPmlClientReport_rep
        {
            get { return m_app.aMortgageLates60In12MonthsPmlClientReport_rep; }
        }


        public int aMortgageLates90In12MonthsPmlClientReport
        {
            get { return m_app.aMortgageLates90In12MonthsPmlClientReport; }
        }


        public string aMortgageLates90In12MonthsPmlClientReport_rep
        {
            get { return m_app.aMortgageLates90In12MonthsPmlClientReport_rep; }
        }


        public int aMortgageLates120In12MonthsPmlClientReport
        {
            get { return m_app.aMortgageLates120In12MonthsPmlClientReport; }
        }


        public string aMortgageLates120In12MonthsPmlClientReport_rep
        {
            get { return m_app.aMortgageLates120In12MonthsPmlClientReport_rep; }
        }


        public bool aIsCreditReportManual
        {
            get { return m_app.aIsCreditReportManual; }
        }


        public CDateTime aFileDOfLastForeclosure
        {
            get { return m_app.aFileDOfLastForeclosure; }
        }


        public string aFileDOfLastForeclosure_rep
        {
            get { return m_app.aFileDOfLastForeclosure_rep; }
        }


        public CDateTime aDischargedDOfLastForeclosesure
        {
            get { return m_app.aDischargedDOfLastForeclosesure; }
        }


        public string aDischargedDOfLastForeclosesure_rep
        {
            get { return m_app.aDischargedDOfLastForeclosesure_rep; }
        }


        public CDateTime aFileDOfLastBk7
        {
            get { return m_app.aFileDOfLastBk7; }
        }


        public string aFileDOfLastBk7_rep
        {
            get { return m_app.aFileDOfLastBk7_rep; }
        }


        public CDateTime aDischargedDOfLastBk7
        {
            get { return m_app.aDischargedDOfLastBk7; }
        }


        public string aDischargedDOfLastBk7_rep
        {
            get { return m_app.aDischargedDOfLastBk7_rep; }
        }


        public CDateTime aFileDOfLastBk13
        {
            get { return m_app.aFileDOfLastBk13; }
        }


        public string aFileDOfLastBk13_rep
        {
            get { return m_app.aFileDOfLastBk13_rep; }
        }


        public CDateTime aDischargedDOfLastBk13
        {
            get { return m_app.aDischargedDOfLastBk13; }
        }


        public string aDischargedDOfLastBk13_rep
        {
            get { return m_app.aDischargedDOfLastBk13_rep; }
        }


        public bool aIsBorrSpousePrimaryWageEarner
        {
            get { return m_app.aIsBorrSpousePrimaryWageEarner; }
            set
            {
                m_validator.TryToWriteField("aIsBorrSpousePrimaryWageEarner", () => m_app.aIsBorrSpousePrimaryWageEarner, t => m_app.aIsBorrSpousePrimaryWageEarner = t, value);
            }
        }


        public int aBHighestScore
        {
            get { return m_app.aBHighestScore; }
        }


        public string aBHighestScore_rep
        {
            get { return m_app.aBHighestScore_rep; }
        }


        public bool aBHasHighestScore
        {
            get { return m_app.aBHasHighestScore; }
        }


        public int aBSecondHighestScore
        {
            get { return m_app.aBSecondHighestScore; }
        }


        public string aBSecondHighestScoreAgency
        {
            get { return m_app.aBSecondHighestScoreAgency; }
        }

        public int aBFicoScore
        {
            get { return m_app.aBFicoScore; }
        }
        public string aBFicoScore_rep
        {
            get { return m_app.aBFicoScore_rep; }
        }

        public int aCHighestScore
        {
            get { return m_app.aCHighestScore; }
        }


        public string aCHighestScore_rep
        {
            get { return m_app.aCHighestScore_rep; }
        }


        public bool aCHasHighestScore
        {
            get { return m_app.aCHasHighestScore; }
        }


        public int aCSecondHighestScore
        {
            get { return m_app.aCSecondHighestScore; }
        }


        public string aCSecondHighestScoreAgency
        {
            get { return m_app.aCSecondHighestScoreAgency; }
        }

        public int aCFicoScore
        {
            get { return m_app.aCFicoScore; }
        }
        public string aCFicoScore_rep
        {
            get { return m_app.aCFicoScore_rep; }
        }
        public int aBAvgScore
        {
            get { return m_app.aBAvgScore; }
        }


        public string aAltNm1
        {
            get { return m_app.aAltNm1; }
            set
            {
                m_validator.TryToWriteField("aAltNm1", () => m_app.aAltNm1, t => m_app.aAltNm1 = t, value);
            }
        }


        public string aAltNm1CreditorNm
        {
            get { return m_app.aAltNm1CreditorNm; }
            set
            {
                m_validator.TryToWriteField("aAltNm1CreditorNm", () => m_app.aAltNm1CreditorNm, t => m_app.aAltNm1CreditorNm = t, value);
            }
        }


        public Sensitive<string> aAltNm1AccNum
        {
            get { return m_app.aAltNm1AccNum; }
            set
            {
                m_validator.TryToWriteField("aAltNm1AccNum", () => m_app.aAltNm1AccNum, t => m_app.aAltNm1AccNum = t, value);
            }
        }


        public string aAltNm2
        {
            get { return m_app.aAltNm2; }
            set
            {
                m_validator.TryToWriteField("aAltNm2", () => m_app.aAltNm2, t => m_app.aAltNm2 = t, value);
            }
        }


        public string aAltNm2CreditorNm
        {
            get { return m_app.aAltNm2CreditorNm; }
            set
            {
                m_validator.TryToWriteField("aAltNm2CreditorNm", () => m_app.aAltNm2CreditorNm, t => m_app.aAltNm2CreditorNm = t, value);
            }
        }


        public Sensitive<string> aAltNm2AccNum
        {
            get { return m_app.aAltNm2AccNum; }
            set
            {
                m_validator.TryToWriteField("aAltNm2AccNum", () => m_app.aAltNm2AccNum, t => m_app.aAltNm2AccNum = t, value);
            }
        }


        public string aBEmplmtXmlContent
        {
            get { return m_app.aBEmplmtXmlContent; }
            set
            {
                m_validator.TryToWriteField("aBEmplmtXmlContent", () => m_app.aBEmplmtXmlContent, t => m_app.aBEmplmtXmlContent = t, value);
            }
        }


        public string aCEmplmtXmlContent
        {
            get { return m_app.aCEmplmtXmlContent; }
            set
            {
                m_validator.TryToWriteField("aCEmplmtXmlContent", () => m_app.aCEmplmtXmlContent, t => m_app.aCEmplmtXmlContent = t, value);
            }
        }

        public string aBH4HAssetTotal_rep
        {
            get { return m_app.aBH4HAssetTotal_rep; }
        }


        public string aCH4HAssetTotal_rep
        {
            get { return m_app.aCH4HAssetTotal_rep; }
        }


        public string aBH4HAssetRetirementTotal_rep
        {
            get { return m_app.aBH4HAssetRetirementTotal_rep; }
        }


        public string aCH4HAssetRetirementTotal_rep
        {
            get { return m_app.aCH4HAssetRetirementTotal_rep; }
        }


        public string aBH4HAssetLessRetirement_rep
        {
            get { return m_app.aBH4HAssetLessRetirement_rep; }
        }


        public string aCH4HAssetLessRetirement_rep
        {
            get { return m_app.aCH4HAssetLessRetirement_rep; }
        }


        public string aBH4HLiabilityTotal_rep
        {
            get { return m_app.aBH4HLiabilityTotal_rep; }
        }


        public string aCH4HLiabilityTotal_rep
        {
            get { return m_app.aCH4HLiabilityTotal_rep; }
        }


        public string aBH4HLiabilityRetirementTotal_rep
        {
            get { return m_app.aBH4HLiabilityRetirementTotal_rep; }
        }


        public string aCH4HLiabilityRetirementTotal_rep
        {
            get { return m_app.aCH4HLiabilityRetirementTotal_rep; }
        }


        public string aBH4HLiabilityLessRetirement_rep
        {
            get { return m_app.aBH4HLiabilityLessRetirement_rep; }
        }


        public string aCH4HLiabilityLessRetirement_rep
        {
            get { return m_app.aCH4HLiabilityLessRetirement_rep; }
        }


        public string aBH4HNetworth_rep
        {
            get { return m_app.aBH4HNetworth_rep; }
        }


        public string aCH4HNetworth_rep
        {
            get { return m_app.aCH4HNetworth_rep; }
        }


        public string aBH4HNetworthLessRetirement_rep
        {
            get { return m_app.aBH4HNetworthLessRetirement_rep; }
        }


        public string aCH4HNetworthLessRetirement_rep
        {
            get { return m_app.aCH4HNetworthLessRetirement_rep; }
        }


        public decimal aH4HNetworthLessRetirementKeyword
        {
            get { return m_app.aH4HNetworthLessRetirementKeyword; }
        }


        public E_aHasFraudConvictionT aBHasFraudConvictionT
        {
            get { return m_app.aBHasFraudConvictionT; }
            set
            {
                m_validator.TryToWriteField("aBHasFraudConvictionT", () => m_app.aBHasFraudConvictionT, t => m_app.aBHasFraudConvictionT = t, value);
            }
        }


        public E_aHasFraudConvictionT aCHasFraudConvictionT
        {
            get { return m_app.aCHasFraudConvictionT; }
            set
            {
                m_validator.TryToWriteField("aCHasFraudConvictionT", () => m_app.aCHasFraudConvictionT, t => m_app.aCHasFraudConvictionT = t, value);
            }
        }


        public E_aHasFraudConvictionT aHasFraudConvictionT
        {
            get { return m_app.aHasFraudConvictionT; }
        }


        public bool aH4HHasUnmatchedLiabilities
        {
            get { return m_app.aH4HHasUnmatchedLiabilities; }
        }





        public decimal aAsstLiqTot
        {
            get { return m_app.aAsstLiqTot; }
        }


        public string aAsstLiqTot_rep
        {
            get { return m_app.aAsstLiqTot_rep; }
        }


        public decimal aAsstNonReSolidTot
        {
            get { return m_app.aAsstNonReSolidTot; }
        }


        public string aAsstNonReSolidTot_rep
        {
            get { return m_app.aAsstNonReSolidTot_rep; }
        }


        public decimal aAsstValTot
        {
            get { return m_app.aAsstValTot; }
        }


        public string aAsstValTot_rep
        {
            get { return m_app.aAsstValTot_rep; }
        }


        public IPublicRecordCollection aPublicRecordCollection
        {
            get { return m_app.aPublicRecordCollection; }
        }


        public bool aIsPublicRecordAlter
        {
            get { return m_app.aIsPublicRecordAlter; }
        }




        public decimal aReTotVal
        {
            get { return m_app.aReTotVal; }
        }


        public string aReTotVal_rep
        {
            get { return m_app.aReTotVal_rep; }
        }


        public decimal aReTotMAmt
        {
            get { return m_app.aReTotMAmt; }
        }


        public string aReTotMAmt_rep
        {
            get { return m_app.aReTotMAmt_rep; }
        }


        public decimal aReTotGrossRentI
        {
            get { return m_app.aReTotGrossRentI; }
        }


        public string aReTotGrossRentI_rep
        {
            get { return m_app.aReTotGrossRentI_rep; }
        }


        public decimal aReTotMPmt
        {
            get { return m_app.aReTotMPmt; }
        }


        public string aReTotMPmt_rep
        {
            get { return m_app.aReTotMPmt_rep; }
        }


        public decimal aReTotHExp
        {
            get { return m_app.aReTotHExp; }
        }


        public string aReTotHExp_rep
        {
            get { return m_app.aReTotHExp_rep; }
        }


        public decimal aReTotNetRentI
        {
            get { return m_app.aReTotNetRentI; }
        }


        public string aReTotNetRentI_rep
        {
            get { return m_app.aReTotNetRentI_rep; }
        }





        public decimal aBBaseI
        {
            get { return m_app.aBBaseI; }
            set
            {
                m_validator.TryToWriteField("aBBaseI", () => m_app.aBBaseI, t => m_app.aBBaseI = t, value);
            }
        }


        public string aBBaseI_rep
        {
            get { return m_app.aBBaseI_rep; }
            set
            {
                m_validator.TryToWriteField("aBBaseI_rep", () => m_app.aBBaseI_rep, t => m_app.aBBaseI_rep = t, value);
            }
        }

        public decimal aBTotIPe
        {
            get { return m_app.aBTotIPe; }
            set
            {
                m_validator.TryToWriteField("aBTotIPe", () => m_app.aBTotIPe, t => m_app.aBTotIPe = t, value);
            }
        }


        public string aBTotIPe_rep
        {
            get { return m_app.aBTotIPe_rep; }
            set
            {
                m_validator.TryToWriteField("aBTotIPe_rep", () => m_app.aBTotIPe_rep, t => m_app.aBTotIPe_rep = t, value);
            }
        }

        public decimal aCTotIPe
        {
            get { return m_app.aCTotIPe; }
            set
            {
                m_validator.TryToWriteField("aCTotIPe", () => m_app.aCTotIPe, t => m_app.aCTotIPe = t, value);
            }
        }


        public string aCTotIPe_rep
        {
            get { return m_app.aCTotIPe_rep; }
            set
            {
                m_validator.TryToWriteField("aCTotIPe_rep", () => m_app.aCTotIPe_rep, t => m_app.aCTotIPe_rep = t, value);
            }
        }

        public decimal aBOvertimeI
        {
            get { return m_app.aBOvertimeI; }
            set
            {
                m_validator.TryToWriteField("aBOvertimeI", () => m_app.aBOvertimeI, t => m_app.aBOvertimeI = t, value);
            }
        }


        public string aBOvertimeI_rep
        {
            get { return m_app.aBOvertimeI_rep; }
            set
            {
                m_validator.TryToWriteField("aBOvertimeI_rep", () => m_app.aBOvertimeI_rep, t => m_app.aBOvertimeI_rep = t, value);
            }
        }


        public decimal aBBonusesI
        {
            get { return m_app.aBBonusesI; }
            set
            {
                m_validator.TryToWriteField("aBBonusesI", () => m_app.aBBonusesI, t => m_app.aBBonusesI = t, value);
            }
        }


        public string aBBonusesI_rep
        {
            get { return m_app.aBBonusesI_rep; }
            set
            {
                m_validator.TryToWriteField("aBBonusesI_rep", () => m_app.aBBonusesI_rep, t => m_app.aBBonusesI_rep = t, value);
            }
        }


        public decimal aBCommisionI
        {
            get { return m_app.aBCommisionI; }
            set
            {
                m_validator.TryToWriteField("aBCommisionI", () => m_app.aBCommisionI, t => m_app.aBCommisionI = t, value);
            }
        }


        public string aBCommisionI_rep
        {
            get { return m_app.aBCommisionI_rep; }
            set
            {
                m_validator.TryToWriteField("aBCommisionI_rep", () => m_app.aBCommisionI_rep, t => m_app.aBCommisionI_rep = t, value);
            }
        }


        public decimal aBDividendI
        {
            get { return m_app.aBDividendI; }
            set
            {
                m_validator.TryToWriteField("aBDividendI", () => m_app.aBDividendI, t => m_app.aBDividendI = t, value);
            }
        }


        public string aBDividendI_rep
        {
            get { return m_app.aBDividendI_rep; }
            set
            {
                m_validator.TryToWriteField("aBDividendI_rep", () => m_app.aBDividendI_rep, t => m_app.aBDividendI_rep = t, value);
            }
        }


        public decimal aBNetRentI
        {
            get { return m_app.aBNetRentI; }
        }


        public string aBNetRentI_rep
        {
            get { return m_app.aBNetRentI_rep; }
        }


        public decimal aBNetRentI1003
        {
            get { return m_app.aBNetRentI1003; }
            set
            {
                m_validator.TryToWriteField("aBNetRentI1003", () => m_app.aBNetRentI1003, t => m_app.aBNetRentI1003 = t, value);
            }
        }


        public string aBNetRentI1003_rep
        {
            get { return m_app.aBNetRentI1003_rep; }
            set
            {
                m_validator.TryToWriteField("aBNetRentI1003_rep", () => m_app.aBNetRentI1003_rep, t => m_app.aBNetRentI1003_rep = t, value);
            }
        }


        public decimal aCNetRentI1003
        {
            get { return m_app.aCNetRentI1003; }
            set
            {
                m_validator.TryToWriteField("aCNetRentI1003", () => m_app.aCNetRentI1003, t => m_app.aCNetRentI1003 = t, value);
            }
        }


        public string aCNetRentI1003_rep
        {
            get { return m_app.aCNetRentI1003_rep; }
            set
            {
                m_validator.TryToWriteField("aCNetRentI1003_rep", () => m_app.aCNetRentI1003_rep, t => m_app.aCNetRentI1003_rep = t, value);
            }
        }


        public decimal aTotNetRentI1003
        {
            get { return m_app.aTotNetRentI1003; }
        }


        public string aTotNetRentI1003_rep
        {
            get { return m_app.aTotNetRentI1003_rep; }
        }


        public decimal aTransmBTotI
        {
            get { return m_app.aTransmBTotI; }
        }


        public string aTransmBTotI_rep
        {
            get { return m_app.aTransmBTotI_rep; }
        }


        public decimal aTransmCTotI
        {
            get { return m_app.aTransmCTotI; }
        }


        public string aTransmCTotI_rep
        {
            get { return m_app.aTransmCTotI_rep; }
        }


        public decimal aTransmTotI
        {
            get { return m_app.aTransmTotI; }
        }


        public string aTransmTotI_rep
        {
            get { return m_app.aTransmTotI_rep; }
        }


        public decimal aQualTopR
        {
            get { return m_app.aQualTopR; }
        }


        public string aQualTopR_rep
        {
            get { return m_app.aQualTopR_rep; }
        }


        public decimal aQualBottomR
        {
            get { return m_app.aQualBottomR; }
        }


        public string aQualBottomR_rep
        {
            get { return m_app.aQualBottomR_rep; }
        }


        public decimal aCNetNegCf
        {
            get { return m_app.aCNetNegCf; }
        }


        public string aCNetNegCf_rep
        {
            get { return m_app.aCNetNegCf_rep; }
        }


        public decimal aBNetNegCf
        {
            get { return m_app.aBNetNegCf; }
        }


        public string aBNetNegCf_rep
        {
            get { return m_app.aBNetNegCf_rep; }
        }

        public decimal aBRetainedNegCf
        {
            get { return m_app.aBRetainedNegCf; }
        }


        public string aBRetainedNegCf_rep
        {
            get { return m_app.aBRetainedNegCf_rep; }
        }


        public decimal aCRetainedNegCf
        {
            get { return m_app.aCRetainedNegCf; }
        }


        public string aCRetainedNegCf_rep
        {
            get { return m_app.aCRetainedNegCf_rep; }
        }

        public decimal aCBaseI
        {
            get { return m_app.aCBaseI; }
            set
            {
                m_validator.TryToWriteField("aCBaseI", () => m_app.aCBaseI, t => m_app.aCBaseI = t, value);
            }
        }


        public string aCBaseI_rep
        {
            get { return m_app.aCBaseI_rep; }
            set
            {
                m_validator.TryToWriteField("aCBaseI_rep", () => m_app.aCBaseI_rep, t => m_app.aCBaseI_rep = t, value);
            }
        }

        public decimal aBSpPosCf
        {
            get { return m_app.aBSpPosCf; }
            set
            {
                m_validator.TryToWriteField("aBSpPosCf", () => m_app.aBSpPosCf, t => m_app.aBSpPosCf = t, value);
            }
        }


        public string aBSpPosCf_rep
        {
            get { return m_app.aBSpPosCf_rep; }
            set
            {
                m_validator.TryToWriteField("aBSpPosCf_rep", () => m_app.aBSpPosCf_rep, t => m_app.aBSpPosCf_rep = t, value);
            }
        }


        public decimal aBNonbaseI
        {
            get { return m_app.aBNonbaseI; }
        }


        public string aBNonbaseI_rep
        {
            get { return m_app.aBNonbaseI_rep; }
        }


        public decimal aTotNonbaseI
        {
            get { return m_app.aTotNonbaseI; }
        }


        public string aTotNonbaseI_rep
        {
            get { return m_app.aTotNonbaseI_rep; }
        }


        public decimal aTotSpPosCf
        {
            get { return m_app.aTotSpPosCf; }
        }


        public string aTotSpPosCf_rep
        {
            get { return m_app.aTotSpPosCf_rep; }
        }


        public decimal aTransmProRent
        {
            get { return m_app.aTransmProRent; }
        }


        public string aTransmProRent_rep
        {
            get { return m_app.aTransmProRent_rep; }
        }


        public decimal aTransmPro1stMPmt
        {
            get { return m_app.aTransmPro1stMPmt; }
        }


        public string aTransmPro1stMPmt_rep
        {
            get { return m_app.aTransmPro1stMPmt_rep; }
        }


        public decimal aTransmPro2ndMPmt
        {
            get { return m_app.aTransmPro2ndMPmt; }
        }


        public string aTransmPro2ndMPmt_rep
        {
            get { return m_app.aTransmPro2ndMPmt_rep; }
        }


        public decimal aTransmProHazIns
        {
            get { return m_app.aTransmProHazIns; }
        }


        public string aTransmProHazIns_rep
        {
            get { return m_app.aTransmProHazIns_rep; }
        }


        public decimal aTransmProRealETx
        {
            get { return m_app.aTransmProRealETx; }
        }


        public string aTransmProRealETx_rep
        {
            get { return m_app.aTransmProRealETx_rep; }
        }


        public decimal aTransmProMIns
        {
            get { return m_app.aTransmProMIns; }
        }


        public string aTransmProMIns_rep
        {
            get { return m_app.aTransmProMIns_rep; }
        }


        public decimal aTransmProHoAssocDues
        {
            get { return m_app.aTransmProHoAssocDues; }
        }


        public string aTransmProHoAssocDues_rep
        {
            get { return m_app.aTransmProHoAssocDues_rep; }
        }


        public decimal aTransmOtherProHExp
        {
            get { return m_app.aTransmOtherProHExp; }
        }


        public string aTransmOtherProHExp_rep
        {
            get { return m_app.aTransmOtherProHExp_rep; }
        }


        public decimal aTransmProTotHExp
        {
            get { return m_app.aTransmProTotHExp; }
        }


        public string aTransmProTotHExp_rep
        {
            get { return m_app.aTransmProTotHExp_rep; }
        }


        public decimal aSpNegCfReo
        {
            get { return m_app.aSpNegCfReo; }
        }


        public string aSpNegCfReo_rep
        {
            get { return m_app.aSpNegCfReo_rep; }
        }


        public decimal aSpNegCf
        {
            get { return m_app.aSpNegCf; }
            set
            {
                m_validator.TryToWriteField("aSpNegCf", () => m_app.aSpNegCf, t => m_app.aSpNegCf = t, value);
            }
        }


        public string aSpNegCf_rep
        {
            get { return m_app.aSpNegCf_rep; }
            set
            {
                m_validator.TryToWriteField("aSpNegCf_rep", () => m_app.aSpNegCf_rep, t => m_app.aSpNegCf_rep = t, value);
            }
        }


        public decimal aOpNegCf
        {
            get { return m_app.aOpNegCf; }
            set
            {
                m_validator.TryToWriteField("aOpNegCf", () => m_app.aOpNegCf, t => m_app.aOpNegCf = t, value);
            }
        }


        public string aOpNegCf_rep
        {
            get { return m_app.aOpNegCf_rep; }
            set
            {
                m_validator.TryToWriteField("aOpNegCf_rep", () => m_app.aOpNegCf_rep, t => m_app.aOpNegCf_rep = t, value);
            }
        }


        public bool aSpNegCfLckd
        {
            get { return m_app.aSpNegCfLckd; }
            set
            {
                m_validator.TryToWriteField("aSpNegCfLckd", () => m_app.aSpNegCfLckd, t => m_app.aSpNegCfLckd = t, value);
            }
        }


        public bool aOpNegCfLckd
        {
            get { return m_app.aOpNegCfLckd; }
            set
            {
                m_validator.TryToWriteField("aOpNegCfLckd", () => m_app.aOpNegCfLckd, t => m_app.aOpNegCfLckd = t, value);
            }
        }


        public decimal aTransmOMonPmt
        {
            get { return m_app.aTransmOMonPmt; }
        }


        public string aTransmOMonPmt_rep
        {
            get { return m_app.aTransmOMonPmt_rep; }
        }


        public decimal aTransmTotMonPmt
        {
            get { return m_app.aTransmTotMonPmt; }
        }


        public string aTransmTotMonPmt_rep
        {
            get { return m_app.aTransmTotMonPmt_rep; }
        }


        public decimal aCNonbaseI
        {
            get { return m_app.aCNonbaseI; }
        }


        public string aCNonbaseI_rep
        {
            get { return m_app.aCNonbaseI_rep; }
        }


        public decimal aCOvertimeI
        {
            get { return m_app.aCOvertimeI; }
            set
            {
                m_validator.TryToWriteField("aCOvertimeI", () => m_app.aCOvertimeI, t => m_app.aCOvertimeI = t, value);
            }
        }


        public string aCOvertimeI_rep
        {
            get { return m_app.aCOvertimeI_rep; }
            set
            {
                m_validator.TryToWriteField("aCOvertimeI_rep", () => m_app.aCOvertimeI_rep, t => m_app.aCOvertimeI_rep = t, value);
            }
        }


        public decimal aCBonusesI
        {
            get { return m_app.aCBonusesI; }
            set
            {
                m_validator.TryToWriteField("aCBonusesI", () => m_app.aCBonusesI, t => m_app.aCBonusesI = t, value);
            }
        }


        public string aCBonusesI_rep
        {
            get { return m_app.aCBonusesI_rep; }
            set
            {
                m_validator.TryToWriteField("aCBonusesI_rep", () => m_app.aCBonusesI_rep, t => m_app.aCBonusesI_rep = t, value);
            }
        }


        public decimal aCCommisionI
        {
            get { return m_app.aCCommisionI; }
            set
            {
                m_validator.TryToWriteField("aCCommisionI", () => m_app.aCCommisionI, t => m_app.aCCommisionI = t, value);
            }
        }


        public string aCCommisionI_rep
        {
            get { return m_app.aCCommisionI_rep; }
            set
            {
                m_validator.TryToWriteField("aCCommisionI_rep", () => m_app.aCCommisionI_rep, t => m_app.aCCommisionI_rep = t, value);
            }
        }


        public decimal aCDividendI
        {
            get { return m_app.aCDividendI; }
            set
            {
                m_validator.TryToWriteField("aCDividendI", () => m_app.aCDividendI, t => m_app.aCDividendI = t, value);
            }
        }


        public string aCDividendI_rep
        {
            get { return m_app.aCDividendI_rep; }
            set
            {
                m_validator.TryToWriteField("aCDividendI_rep", () => m_app.aCDividendI_rep, t => m_app.aCDividendI_rep = t, value);
            }
        }


        public decimal aCSpPosCf
        {
            get { return m_app.aCSpPosCf; }
            set
            {
                m_validator.TryToWriteField("aCSpPosCf", () => m_app.aCSpPosCf, t => m_app.aCSpPosCf = t, value);
            }
        }


        public string aCSpPosCf_rep
        {
            get { return m_app.aCSpPosCf_rep; }
            set
            {
                m_validator.TryToWriteField("aCSpPosCf_rep", () => m_app.aCSpPosCf_rep, t => m_app.aCSpPosCf_rep = t, value);
            }
        }


        public decimal aCNetRentI
        {
            get { return m_app.aCNetRentI; }
        }


        public string aCNetRentI_rep
        {
            get { return m_app.aCNetRentI_rep; }
        }


        public string aOIFrom1008Desc
        {
            get { return m_app.aOIFrom1008Desc; }
            set
            {
                m_validator.TryToWriteField("aOIFrom1008Desc", () => m_app.aOIFrom1008Desc, t => m_app.aOIFrom1008Desc = t, value);
            }
        }


        public decimal aBOIFrom1008
        {
            get { return m_app.aBOIFrom1008; }
            set
            {
                m_validator.TryToWriteField("aBOIFrom1008", () => m_app.aBOIFrom1008, t => m_app.aBOIFrom1008 = t, value);
            }
        }


        public string aBOIFrom1008_rep
        {
            get { return m_app.aBOIFrom1008_rep; }
            set
            {
                m_validator.TryToWriteField("aBOIFrom1008_rep", () => m_app.aBOIFrom1008_rep, t => m_app.aBOIFrom1008_rep = t, value);
            }
        }


        public decimal aCOIFrom1008
        {
            get { return m_app.aCOIFrom1008; }
            set
            {
                m_validator.TryToWriteField("aCOIFrom1008", () => m_app.aCOIFrom1008, t => m_app.aCOIFrom1008 = t, value);
            }
        }


        public string aCOIFrom1008_rep
        {
            get { return m_app.aCOIFrom1008_rep; }
            set
            {
                m_validator.TryToWriteField("aCOIFrom1008_rep", () => m_app.aCOIFrom1008_rep, t => m_app.aCOIFrom1008_rep = t, value);
            }
        }


        public decimal aTotOIFrom1008
        {
            get { return m_app.aTotOIFrom1008; }
        }


        public string aTotOIFrom1008_rep
        {
            get { return m_app.aTotOIFrom1008_rep; }
        }

        public string aBTotOI_print_rep
        {
            get { return m_app.aBTotOI_print_rep; }
        }

        public decimal aBTotOI
        {
            get { return m_app.aBTotOI; }
        }


        public string aBTotOI_rep
        {
            get { return m_app.aBTotOI_rep; }
        }

        public string aCTotOI_print_rep
        {
            get { return m_app.aCTotOI_print_rep; }
        }

        public decimal aCTotOI
        {
            get { return m_app.aCTotOI; }
        }


        public string aCTotOI_rep
        {
            get { return m_app.aCTotOI_rep; }
        }

        public void ClearOtherIncomeSources()
        {
            this.m_app.ClearOtherIncomeSources();
        }

        public List<OtherIncome> aOtherIncomeList
        {
            get { return m_app.aOtherIncomeList; }
            set
            {
                m_validator.TryToWriteField("aOtherIncomeList", () => m_app.aOtherIncomeList, t => m_app.aOtherIncomeList = t, value);
            }
        }

        public bool aIsPrimary
        {
            get { return m_app.aIsPrimary; }
            set
            {
                m_validator.TryToWriteField("aIsPrimary", () => m_app.aIsPrimary, t => m_app.aIsPrimary = t, value);
            }
        }


        public string aBLastNm
        {
            get { return m_app.aBLastNm; }
            set
            {
                m_validator.TryToWriteField("aBLastNm", () => m_app.aBLastNm, t => m_app.aBLastNm = t, value);
            }
        }


        public string aBFirstNm
        {
            get { return m_app.aBFirstNm; }
            set
            {
                m_validator.TryToWriteField("aBFirstNm", () => m_app.aBFirstNm, t => m_app.aBFirstNm = t, value);
            }
        }

        public string aBPreferredNm
        {
            get { return m_app.aBPreferredNm; }
            set
            {
                m_validator.TryToWriteField("aBPreferredNm", () => m_app.aBPreferredNm, t => m_app.aBPreferredNm = t, value);
            }
        }

        public bool aBPreferredNmLckd
        {
            get { return m_app.aBPreferredNmLckd; }
            set
            {
                m_validator.TryToWriteField("aBPreferredNmLckd", () => m_app.aBPreferredNmLckd, t => m_app.aBPreferredNmLckd = t, value);
            }
        }

        public string aBLastFirstNm
        {
            get { return m_app.aBLastFirstNm; }
        }


        public string aBNm
        {
            get { return m_app.aBNm; }
        }


        public string aBTotalScoreFormatNm
        {
            get { return m_app.aBTotalScoreFormatNm; }
        }


        public string aBNm_aCNm
        {
            get { return m_app.aBNm_aCNm; }
        }

        public bool aBIsValidHmdaApplication
        {
            get { return this.m_app.aBIsValidHmdaApplication; }
        }

        public bool aCIsValidHmdaApplication
        {
            get { return this.m_app.aCIsValidHmdaApplication; }
        }

        public string aBAddr
        {
            get { return m_app.aBAddr; }
            set
            {
                m_validator.TryToWriteField("aBAddr", () => m_app.aBAddr, t => m_app.aBAddr = t, value);
            }
        }


        public string aBAddr_MultiLine
        {
            get { return m_app.aBAddr_MultiLine; }
        }


        public string aBAddr_SingleLine
        {
            get { return m_app.aBAddr_SingleLine; }
        }


        public string aCAddr_MultiLine
        {
            get { return m_app.aCAddr_MultiLine; }
        }


        public string aCAddr_SingleLine
        {
            get { return m_app.aCAddr_SingleLine; }
        }


        public E_aBAddrT aBAddrT
        {
            get { return m_app.aBAddrT; }
            set
            {
                m_validator.TryToWriteField("aBAddrT", () => m_app.aBAddrT, t => m_app.aBAddrT = t, value);
            }
        }


        public string aBAddrYrs
        {
            get { return m_app.aBAddrYrs; }
            set
            {
                m_validator.TryToWriteField("aBAddrYrs", () => m_app.aBAddrYrs, t => m_app.aBAddrYrs = t, value);
            }
        }


        public string aBAddrTotalMonths
        {
            get { return m_app.aBAddrTotalMonths; }
            set
            {
                m_validator.TryToWriteField("aBAddrTotalMonths", () => m_app.aBAddrTotalMonths, t => m_app.aBAddrTotalMonths = t, value);
            }
        }


        public int aBAge
        {
            get { return m_app.aBAge; }
            set
            {
                m_validator.TryToWriteField("aBAge", () => m_app.aBAge, t => m_app.aBAge = t, value);
            }
        }


        public string aBAge_rep
        {
            get { return m_app.aBAge_rep; }
            set
            {
                m_validator.TryToWriteField("aBAge_rep", () => m_app.aBAge_rep, t => m_app.aBAge_rep = t, value);
            }
        }


        public int aBSchoolYrs
        {
            get { return m_app.aBSchoolYrs; }
            set
            {
                m_validator.TryToWriteField("aBSchoolYrs", () => m_app.aBSchoolYrs, t => m_app.aBSchoolYrs = t, value);
            }
        }


        public string aBSchoolYrs_rep
        {
            get { return m_app.aBSchoolYrs_rep; }
            set
            {
                m_validator.TryToWriteField("aBSchoolYrs_rep", () => m_app.aBSchoolYrs_rep, t => m_app.aBSchoolYrs_rep = t, value);
            }
        }


        public string aBSchoolTotalMonths_rep
        {
            get { return m_app.aBSchoolTotalMonths_rep; }
            set
            {
                m_validator.TryToWriteField("aBSchoolTotalMonths_rep", () => m_app.aBSchoolTotalMonths_rep, t => m_app.aBSchoolTotalMonths_rep = t, value);
            }
        }


        public string aBBusPhone
        {
            get { return m_app.aBBusPhone; }
            set
            {
                m_validator.TryToWriteField("aBBusPhone", () => m_app.aBBusPhone, t => m_app.aBBusPhone = t, value);
            }
        }


        public string aBCity
        {
            get { return m_app.aBCity; }
            set
            {
                m_validator.TryToWriteField("aBCity", () => m_app.aBCity, t => m_app.aBCity = t, value);
            }
        }


        public string aBDependAges
        {
            get { return m_app.aBDependAges; }
            set
            {
                m_validator.TryToWriteField("aBDependAges", () => m_app.aBDependAges, t => m_app.aBDependAges = t, value);
            }
        }


        public int aBDependNum
        {
            get { return m_app.aBDependNum; }
            set
            {
                m_validator.TryToWriteField("aBDependNum", () => m_app.aBDependNum, t => m_app.aBDependNum = t, value);
            }
        }


        public string aBDependNum_rep
        {
            get { return m_app.aBDependNum_rep; }
            set
            {
                m_validator.TryToWriteField("aBDependNum_rep", () => m_app.aBDependNum_rep, t => m_app.aBDependNum_rep = t, value);
            }
        }


        public string aBEmail
        {
            get { return m_app.aBEmail; }
            set
            {
                m_validator.TryToWriteField("aBEmail", () => m_app.aBEmail, t => m_app.aBEmail = t, value);
            }
        }

        public E_aTypeT aBTypeT
        {
            get { return m_app.aBTypeT; }
            set
            {
                m_validator.TryToWriteField("aBTypeT", () => m_app.aBTypeT, t => m_app.aBTypeT = t, value);
            }
        }

        /// <summary>
        /// The borrower's id in a bank's core system
        /// </summary>
        public string aBCoreSystemId
        {
            get { return m_app.aBCoreSystemId; }
            set
            {
                m_validator.TryToWriteField("aBCoreSystemId", () => m_app.aBCoreSystemId, t => m_app.aBCoreSystemId = t, value);
            }
        }

        /// <summary>
        /// The coborrower's id in a bank's core system
        /// </summary>
        public string aCCoreSystemId
        {
            get { return m_app.aCCoreSystemId; }
            set
            {
                m_validator.TryToWriteField("aCCoreSystemId", () => m_app.aCCoreSystemId, t => m_app.aCCoreSystemId = t, value);
            }
        }

        public string aBHPhone
        {
            get { return m_app.aBHPhone; }
            set
            {
                m_validator.TryToWriteField("aBHPhone", () => m_app.aBHPhone, t => m_app.aBHPhone = t, value);
            }
        }


        public string aBFax
        {
            get { return m_app.aBFax; }
            set
            {
                m_validator.TryToWriteField("aBFax", () => m_app.aBFax, t => m_app.aBFax = t, value);
            }
        }


        public string aCFax
        {
            get { return m_app.aCFax; }
            set
            {
                m_validator.TryToWriteField("aCFax", () => m_app.aCFax, t => m_app.aCFax = t, value);
            }
        }


        public E_aBMaritalStatT aBMaritalStatT
        {
            get { return m_app.aBMaritalStatT; }
            set
            {
                m_validator.TryToWriteField("aBMaritalStatT", () => m_app.aBMaritalStatT, t => m_app.aBMaritalStatT = t, value);
            }
        }


        public string aBMaritalStatTDescription
        {
            get { return m_app.aBMaritalStatTDescription; }
        }


        public string aBPrev1Addr
        {
            get { return m_app.aBPrev1Addr; }
            set
            {
                m_validator.TryToWriteField("aBPrev1Addr", () => m_app.aBPrev1Addr, t => m_app.aBPrev1Addr = t, value);
            }
        }


        public string aBPrev1Addr_SingleLine
        {
            get { return m_app.aBPrev1Addr_SingleLine; }
        }


        public E_aBPrev1AddrT aBPrev1AddrT
        {
            get { return m_app.aBPrev1AddrT; }
            set
            {
                m_validator.TryToWriteField("aBPrev1AddrT", () => m_app.aBPrev1AddrT, t => m_app.aBPrev1AddrT = t, value);
            }
        }


        public string aBPrev1AddrYrs
        {
            get { return m_app.aBPrev1AddrYrs; }
            set
            {
                m_validator.TryToWriteField("aBPrev1AddrYrs", () => m_app.aBPrev1AddrYrs, t => m_app.aBPrev1AddrYrs = t, value);
            }
        }


        public string aBPrev1AddrTotalMonths
        {
            get { return m_app.aBPrev1AddrTotalMonths; }
            set
            {
                m_validator.TryToWriteField("aBPrev1AddrTotalMonths", () => m_app.aBPrev1AddrTotalMonths, t => m_app.aBPrev1AddrTotalMonths = t, value);
            }
        }


        public string aBPrev1City
        {
            get { return m_app.aBPrev1City; }
            set
            {
                m_validator.TryToWriteField("aBPrev1City", () => m_app.aBPrev1City, t => m_app.aBPrev1City = t, value);
            }
        }


        public string aBPrev1State
        {
            get { return m_app.aBPrev1State; }
            set
            {
                m_validator.TryToWriteField("aBPrev1State", () => m_app.aBPrev1State, t => m_app.aBPrev1State = t, value);
            }
        }


        public string aBPrev1Zip
        {
            get { return m_app.aBPrev1Zip; }
            set
            {
                m_validator.TryToWriteField("aBPrev1Zip", () => m_app.aBPrev1Zip, t => m_app.aBPrev1Zip = t, value);
            }
        }


        public string aBPrev2Addr
        {
            get { return m_app.aBPrev2Addr; }
            set
            {
                m_validator.TryToWriteField("aBPrev2Addr", () => m_app.aBPrev2Addr, t => m_app.aBPrev2Addr = t, value);
            }
        }


        public string aBPrev2Addr_SingleLine
        {
            get { return m_app.aBPrev2Addr_SingleLine; }
        }


        public E_aBPrev2AddrT aBPrev2AddrT
        {
            get { return m_app.aBPrev2AddrT; }
            set
            {
                m_validator.TryToWriteField("aBPrev2AddrT", () => m_app.aBPrev2AddrT, t => m_app.aBPrev2AddrT = t, value);
            }
        }


        public string aBPrev2AddrYrs
        {
            get { return m_app.aBPrev2AddrYrs; }
            set
            {
                m_validator.TryToWriteField("aBPrev2AddrYrs", () => m_app.aBPrev2AddrYrs, t => m_app.aBPrev2AddrYrs = t, value);
            }
        }


        public string aBPrev2AddrTotalMonths
        {
            get { return m_app.aBPrev2AddrTotalMonths; }
            set
            {
                m_validator.TryToWriteField("aBPrev2AddrTotalMonths", () => m_app.aBPrev2AddrTotalMonths, t => m_app.aBPrev2AddrTotalMonths = t, value);
            }
        }


        public string aBPrev2City
        {
            get { return m_app.aBPrev2City; }
            set
            {
                m_validator.TryToWriteField("aBPrev2City", () => m_app.aBPrev2City, t => m_app.aBPrev2City = t, value);
            }
        }


        public string aBPrev2State
        {
            get { return m_app.aBPrev2State; }
            set
            {
                m_validator.TryToWriteField("aBPrev2State", () => m_app.aBPrev2State, t => m_app.aBPrev2State = t, value);
            }
        }


        public string aBPrev2Zip
        {
            get { return m_app.aBPrev2Zip; }
            set
            {
                m_validator.TryToWriteField("aBPrev2Zip", () => m_app.aBPrev2Zip, t => m_app.aBPrev2Zip = t, value);
            }
        }

        public DataObjectIdentifier<DataObjectKind.Consumer, Guid> aBConsumerIdentifier
        {
            get { return this.m_app.aBConsumerIdentifier; }
        }

        public Guid aBConsumerId
        {
            get { return m_app.aBConsumerId; }
        }

        public string aBConsumerId_rep
        {
            get { return m_app.aBConsumerId_rep; }
        }

        public DataObjectIdentifier<DataObjectKind.Consumer, Guid> aCConsumerIdentifier
        {
            get { return this.m_app.aCConsumerIdentifier; }
        }

        public Guid aCConsumerId
        {
            get { return m_app.aCConsumerId; }
        }

        public string aCConsumerId_rep
        {
            get { return m_app.aCConsumerId_rep; }
        }

        public Guid aConsumerId
        {
            get { return m_app.aConsumerId; }
        }

        public string aConsumerId_rep
        {
            get { return m_app.aConsumerId_rep; }
        }

        public string aBSsn
        {
            get { return m_app.aBSsn; }
            set
            {
                m_validator.TryToWriteField("aBSsn", () => m_app.aBSsn, t => m_app.aBSsn = t, value);
            }
        }

        public string aBSsnLastFour
        {
            get { return m_app.aBSsnLastFour; }
        }

        public string aCSsnLastFour
        {
            get { return m_app.aCSsnLastFour; }
        }

        public string aBSsnMasked
        {
            get { return m_app.aBSsnMasked; }
        }

        public string aBNmAndSsnForVerifications
        {
            get { return m_app.aBNmAndSsnForVerifications; }
        }

        public string aBSsnTotalScorecardWarning
        {
            get { return m_app.aBSsnTotalScorecardWarning; }
        }


        public string aBSsnPmlImportWarning
        {
            get { return m_app.aBSsnPmlImportWarning; }
        }


        public string aBState
        {
            get { return m_app.aBState; }
            set
            {
                m_validator.TryToWriteField("aBState", () => m_app.aBState, t => m_app.aBState = t, value);
            }
        }


        public string aBZip
        {
            get { return m_app.aBZip; }
            set
            {
                m_validator.TryToWriteField("aBZip", () => m_app.aBZip, t => m_app.aBZip = t, value);
            }
        }


        public string aCLastNm
        {
            get { return m_app.aCLastNm; }
            set
            {
                m_validator.TryToWriteField("aCLastNm", () => m_app.aCLastNm, t => m_app.aCLastNm = t, value);
            }
        }


        public string aCFirstNm
        {
            get { return m_app.aCFirstNm; }
            set
            {
                m_validator.TryToWriteField("aCFirstNm", () => m_app.aCFirstNm, t => m_app.aCFirstNm = t, value);
            }
        }

        public string aCPreferredNm
        {
            get { return m_app.aCPreferredNm; }
            set
            {
                m_validator.TryToWriteField("aCPreferredNm", () => m_app.aCPreferredNm, t => m_app.aCPreferredNm = t, value);
            }
        }

        public bool aCPreferredNmLckd
        {
            get { return m_app.aCPreferredNmLckd; }
            set
            {
                m_validator.TryToWriteField("aCPreferredNmLckd", () => m_app.aCPreferredNmLckd, t => m_app.aCPreferredNmLckd = t, value);
            }
        }

        public string aCLastFirstNm
        {
            get { return m_app.aCLastFirstNm; }
        }


        public string aCNm
        {
            get { return m_app.aCNm; }
        }


        public string aCTotalScoreFormatNm
        {
            get { return m_app.aCTotalScoreFormatNm; }
        }


        public string aTotalScoreFormatNm
        {
            get { return m_app.aTotalScoreFormatNm; }
        }


        public string aCAddr
        {
            get { return m_app.aCAddr; }
            set
            {
                m_validator.TryToWriteField("aCAddr", () => m_app.aCAddr, t => m_app.aCAddr = t, value);
            }
        }


        public E_aCAddrT aCAddrT
        {
            get { return m_app.aCAddrT; }
            set
            {
                m_validator.TryToWriteField("aCAddrT", () => m_app.aCAddrT, t => m_app.aCAddrT = t, value);
            }
        }


        public string aCAddrYrs
        {
            get { return m_app.aCAddrYrs; }
            set
            {
                m_validator.TryToWriteField("aCAddrYrs", () => m_app.aCAddrYrs, t => m_app.aCAddrYrs = t, value);
            }
        }


        public string aCAddrTotalMonths
        {
            get { return m_app.aCAddrTotalMonths; }
            set
            {
                m_validator.TryToWriteField("aCAddrTotalMonths", () => m_app.aCAddrTotalMonths, t => m_app.aCAddrTotalMonths = t, value);
            }
        }


        public int aCAge
        {
            get { return m_app.aCAge; }
            set
            {
                m_validator.TryToWriteField("aCAge", () => m_app.aCAge, t => m_app.aCAge = t, value);
            }
        }


        public string aCAge_rep
        {
            get { return m_app.aCAge_rep; }
            set
            {
                m_validator.TryToWriteField("aCAge_rep", () => m_app.aCAge_rep, t => m_app.aCAge_rep = t, value);
            }
        }


        public int aCSchoolYrs
        {
            get { return m_app.aCSchoolYrs; }
            set
            {
                m_validator.TryToWriteField("aCSchoolYrs", () => m_app.aCSchoolYrs, t => m_app.aCSchoolYrs = t, value);
            }
        }


        public string aCSchoolYrs_rep
        {
            get { return m_app.aCSchoolYrs_rep; }
            set
            {
                m_validator.TryToWriteField("aCSchoolYrs_rep", () => m_app.aCSchoolYrs_rep, t => m_app.aCSchoolYrs_rep = t, value);
            }
        }


        public string aCSchoolTotalMonths_rep
        {
            get { return m_app.aCSchoolTotalMonths_rep; }
            set
            {
                m_validator.TryToWriteField("aCSchoolTotalMonths_rep", () => m_app.aCSchoolTotalMonths_rep, t => m_app.aCSchoolTotalMonths_rep = t, value);
            }
        }


        public string aCBusPhone
        {
            get { return m_app.aCBusPhone; }
            set
            {
                m_validator.TryToWriteField("aCBusPhone", () => m_app.aCBusPhone, t => m_app.aCBusPhone = t, value);
            }
        }


        public string aCCity
        {
            get { return m_app.aCCity; }
            set
            {
                m_validator.TryToWriteField("aCCity", () => m_app.aCCity, t => m_app.aCCity = t, value);
            }
        }


        public string aCDependAges
        {
            get { return m_app.aCDependAges; }
            set
            {
                m_validator.TryToWriteField("aCDependAges", () => m_app.aCDependAges, t => m_app.aCDependAges = t, value);
            }
        }


        public int aCDependNum
        {
            get { return m_app.aCDependNum; }
            set
            {
                m_validator.TryToWriteField("aCDependNum", () => m_app.aCDependNum, t => m_app.aCDependNum = t, value);
            }
        }


        public string aCDependNum_rep
        {
            get { return m_app.aCDependNum_rep; }
            set
            {
                m_validator.TryToWriteField("aCDependNum_rep", () => m_app.aCDependNum_rep, t => m_app.aCDependNum_rep = t, value);
            }
        }


        public string aCEmail
        {
            get { return m_app.aCEmail; }
            set
            {
                m_validator.TryToWriteField("aCEmail", () => m_app.aCEmail, t => m_app.aCEmail = t, value);
            }
        }

        public E_aTypeT aCTypeT
        {
            get { return m_app.aCTypeT; }
            set
            {
                m_validator.TryToWriteField("aCTypeT", () => m_app.aCTypeT, t => m_app.aCTypeT = t, value);
            }
        }

        public string aCHPhone
        {
            get { return m_app.aCHPhone; }
            set
            {
                m_validator.TryToWriteField("aCHPhone", () => m_app.aCHPhone, t => m_app.aCHPhone = t, value);
            }
        }


        public E_aCMaritalStatT aCMaritalStatT
        {
            get { return m_app.aCMaritalStatT; }
            set
            {
                m_validator.TryToWriteField("aCMaritalStatT", () => m_app.aCMaritalStatT, t => m_app.aCMaritalStatT = t, value);
            }
        }


        public string aCMaritalStatTDescription
        {
            get { return m_app.aCMaritalStatTDescription; }
        }


        public string aCPrev1Addr
        {
            get { return m_app.aCPrev1Addr; }
            set
            {
                m_validator.TryToWriteField("aCPrev1Addr", () => m_app.aCPrev1Addr, t => m_app.aCPrev1Addr = t, value);
            }
        }


        public string aCPrev1Addr_SingleLine
        {
            get { return m_app.aCPrev1Addr_SingleLine; }
        }


        public E_aCPrev1AddrT aCPrev1AddrT
        {
            get { return m_app.aCPrev1AddrT; }
            set
            {
                m_validator.TryToWriteField("aCPrev1AddrT", () => m_app.aCPrev1AddrT, t => m_app.aCPrev1AddrT = t, value);
            }
        }


        public string aCPrev1AddrYrs
        {
            get { return m_app.aCPrev1AddrYrs; }
            set
            {
                m_validator.TryToWriteField("aCPrev1AddrYrs", () => m_app.aCPrev1AddrYrs, t => m_app.aCPrev1AddrYrs = t, value);
            }
        }


        public string aCPrev1AddrTotalMonths
        {
            get { return m_app.aCPrev1AddrTotalMonths; }
            set
            {
                m_validator.TryToWriteField("aCPrev1AddrTotalMonths", () => m_app.aCPrev1AddrTotalMonths, t => m_app.aCPrev1AddrTotalMonths = t, value);
            }
        }


        public string aCPrev1City
        {
            get { return m_app.aCPrev1City; }
            set
            {
                m_validator.TryToWriteField("aCPrev1City", () => m_app.aCPrev1City, t => m_app.aCPrev1City = t, value);
            }
        }


        public string aCPrev1State
        {
            get { return m_app.aCPrev1State; }
            set
            {
                m_validator.TryToWriteField("aCPrev1State", () => m_app.aCPrev1State, t => m_app.aCPrev1State = t, value);
            }
        }


        public string aCPrev1Zip
        {
            get { return m_app.aCPrev1Zip; }
            set
            {
                m_validator.TryToWriteField("aCPrev1Zip", () => m_app.aCPrev1Zip, t => m_app.aCPrev1Zip = t, value);
            }
        }


        public string aCPrev2Addr
        {
            get { return m_app.aCPrev2Addr; }
            set
            {
                m_validator.TryToWriteField("aCPrev2Addr", () => m_app.aCPrev2Addr, t => m_app.aCPrev2Addr = t, value);
            }
        }


        public string aCPrev2Addr_SingleLine
        {
            get { return m_app.aCPrev2Addr_SingleLine; }
        }


        public E_aCPrev2AddrT aCPrev2AddrT
        {
            get { return m_app.aCPrev2AddrT; }
            set
            {
                m_validator.TryToWriteField("aCPrev2AddrT", () => m_app.aCPrev2AddrT, t => m_app.aCPrev2AddrT = t, value);
            }
        }


        public string aCPrev2AddrYrs
        {
            get { return m_app.aCPrev2AddrYrs; }
            set
            {
                m_validator.TryToWriteField("aCPrev2AddrYrs", () => m_app.aCPrev2AddrYrs, t => m_app.aCPrev2AddrYrs = t, value);
            }
        }


        public string aCPrev2AddrTotalMonths
        {
            get { return m_app.aCPrev2AddrTotalMonths; }
            set
            {
                m_validator.TryToWriteField("aCPrev2AddrTotalMonths", () => m_app.aCPrev2AddrTotalMonths, t => m_app.aCPrev2AddrTotalMonths = t, value);
            }
        }


        public string aCPrev2City
        {
            get { return m_app.aCPrev2City; }
            set
            {
                m_validator.TryToWriteField("aCPrev2City", () => m_app.aCPrev2City, t => m_app.aCPrev2City = t, value);
            }
        }


        public string aCPrev2State
        {
            get { return m_app.aCPrev2State; }
            set
            {
                m_validator.TryToWriteField("aCPrev2State", () => m_app.aCPrev2State, t => m_app.aCPrev2State = t, value);
            }
        }


        public string aCPrev2Zip
        {
            get { return m_app.aCPrev2Zip; }
            set
            {
                m_validator.TryToWriteField("aCPrev2Zip", () => m_app.aCPrev2Zip, t => m_app.aCPrev2Zip = t, value);
            }
        }


        public bool aHasExtraFormerAddress
        {
            get { return m_app.aHasExtraFormerAddress; }
        }

        public bool aHasCoborrower
        {
            get { return m_app.aHasCoborrower; }
        }

        public bool aHasCoborrowerData
        {
            get { return m_app.aHasCoborrowerData; }
            internal set
            {
                // For unit tests, only call this method if attempting to generate invalid state
                m_validator.TryToWriteField(nameof(aHasCoborrowerData), () => m_app.aHasCoborrowerData, t => m_app.aHasCoborrowerData = t, value);
            }
        }

        public bool aHasCoborrowerCalc
        {
            get { return m_app.aHasCoborrowerCalc; }
        }

        public bool aCIsDefined
        {
            get { return m_app.aCIsDefined; }
        }

        public bool aBIsValidApplicant // opm 110302
        {
            get { return m_app.aBIsValidApplicant; }
        }

        public bool aCIsValidApplicant // opm 110302
        {
            get { return m_app.aCIsValidApplicant; }
        }

        public bool aHasValidApplicant // opm 110302
        {
            get { return m_app.aHasValidApplicant; }
        }


        public bool aBHasSpouse
        {
            get { return m_app.aBHasSpouse; }
        }


        public string aCSsn
        {
            get { return m_app.aCSsn; }
            set
            {
                m_validator.TryToWriteField("aCSsn", () => m_app.aCSsn, t => m_app.aCSsn = t, value);
            }
        }


        public string aCSsnMasked
        {
            get { return m_app.aCSsnMasked; }
        }

        public string aCNmAndSsnForVerifications
        {
            get { return m_app.aCNmAndSsnForVerifications; }
        }

        public string aCSsnTotalScorecardWarning
        {
            get { return m_app.aCSsnTotalScorecardWarning; }
        }


        public string aCSsnPmlImportWarning
        {
            get { return m_app.aCSsnPmlImportWarning; }
        }


        public string aCState
        {
            get { return m_app.aCState; }
            set
            {
                m_validator.TryToWriteField("aCState", () => m_app.aCState, t => m_app.aCState = t, value);
            }
        }


        public string aCZip
        {
            get { return m_app.aCZip; }
            set
            {
                m_validator.TryToWriteField("aCZip", () => m_app.aCZip, t => m_app.aCZip = t, value);
            }
        }


        public bool aNetRentI1003Lckd
        {
            get { return m_app.aNetRentI1003Lckd; }
            set
            {
                m_validator.TryToWriteField("aNetRentI1003Lckd", () => m_app.aNetRentI1003Lckd, t => m_app.aNetRentI1003Lckd = t, value);
            }
        }


        public Guid aAppId
        {
            get { return m_app.aAppId; }
        }

        public DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> aAppIdentifier
        {
            get { return this.m_app.aAppIdentifier; }
        }

        public string aAppNm
        {
            get { return m_app.aAppNm; }
        }


        public string aAppCPortalFullNm
        {
            get { return m_app.aAppCPortalFullNm; }
        }


        public string aAppFullNm
        {
            get { return m_app.aAppFullNm; }
        }


        public decimal aBTotI
        {
            get { return m_app.aBTotI; }
        }


        public string aBTotI_rep
        {
            get { return m_app.aBTotI_rep; }
        }


        public decimal aCTotI
        {
            get { return m_app.aCTotI; }
        }


        public string aCTotI_rep
        {
            get { return m_app.aCTotI_rep; }
        }


        public decimal aTotI
        {
            get { return m_app.aTotI; }
        }


        public string aTotI_rep
        {
            get { return m_app.aTotI_rep; }
        }

        public decimal aTotOI
        {
            get { return m_app.aTotOI; }
        }


        public string aTotOI_rep
        {
            get { return m_app.aTotOI_rep; }
        }

        public string aTotOI_print_rep
        {
            get { return m_app.aTotOI_print_rep; }
        }


        public decimal aTotNetRentI
        {
            get { return m_app.aTotNetRentI; }
        }


        public string aTotNetRentI_rep
        {
            get { return m_app.aTotNetRentI_rep; }
        }


        public decimal aTotDividendI
        {
            get { return m_app.aTotDividendI; }
        }


        public string aTotDividendI_rep
        {
            get { return m_app.aTotDividendI_rep; }
        }


        public decimal aTotCommisionI
        {
            get { return m_app.aTotCommisionI; }
        }


        public string aTotCommisionI_rep
        {
            get { return m_app.aTotCommisionI_rep; }
        }


        public decimal aTotBonusesI
        {
            get { return m_app.aTotBonusesI; }
        }


        public string aTotBonusesI_rep
        {
            get { return m_app.aTotBonusesI_rep; }
        }


        public decimal aTotBaseI
        {
            get { return m_app.aTotBaseI; }
        }


        public string aTotBaseI_rep
        {
            get { return m_app.aTotBaseI_rep; }
        }


        public decimal aTotOvertimeI
        {
            get { return m_app.aTotOvertimeI; }
        }


        public string aTotOvertimeI_rep
        {
            get { return m_app.aTotOvertimeI_rep; }
        }


        public decimal aPres1stM
        {
            get { return m_app.aPres1stM; }
            set
            {
                m_validator.TryToWriteField("aPres1stM", () => m_app.aPres1stM, t => m_app.aPres1stM = t, value);
            }
        }


        public string aPres1stM_rep
        {
            get { return m_app.aPres1stM_rep; }
            set
            {
                m_validator.TryToWriteField("aPres1stM_rep", () => m_app.aPres1stM_rep, t => m_app.aPres1stM_rep = t, value);
            }
        }


        public decimal aPresRent
        {
            get { return m_app.aPresRent; }
            set
            {
                m_validator.TryToWriteField("aPresRent", () => m_app.aPresRent, t => m_app.aPresRent = t, value);
            }
        }


        public string aPresRent_rep
        {
            get { return m_app.aPresRent_rep; }
            set
            {
                m_validator.TryToWriteField("aPresRent_rep", () => m_app.aPresRent_rep, t => m_app.aPresRent_rep = t, value);
            }
        }


        public decimal aPresOFin
        {
            get { return m_app.aPresOFin; }
            set
            {
                m_validator.TryToWriteField("aPresOFin", () => m_app.aPresOFin, t => m_app.aPresOFin = t, value);
            }
        }


        public string aPresOFin_rep
        {
            get { return m_app.aPresOFin_rep; }
            set
            {
                m_validator.TryToWriteField("aPresOFin_rep", () => m_app.aPresOFin_rep, t => m_app.aPresOFin_rep = t, value);
            }
        }


        public decimal aPresHazIns
        {
            get { return m_app.aPresHazIns; }
        }


        public string aPresHazIns_rep
        {
            get { return m_app.aPresHazIns_rep; }
            set
            {
                m_validator.TryToWriteField("aPresHazIns_rep", () => m_app.aPresHazIns_rep, t => m_app.aPresHazIns_rep = t, value);
            }
        }


        public decimal aPresRealETx
        {
            get { return m_app.aPresRealETx; }
        }


        public string aPresRealETx_rep
        {
            get { return m_app.aPresRealETx_rep; }
            set
            {
                m_validator.TryToWriteField("aPresRealETx_rep", () => m_app.aPresRealETx_rep, t => m_app.aPresRealETx_rep = t, value);
            }
        }


        public decimal aPresMIns
        {
            get { return m_app.aPresMIns; }
        }


        public string aPresMIns_rep
        {
            get { return m_app.aPresMIns_rep; }
            set
            {
                m_validator.TryToWriteField("aPresMIns_rep", () => m_app.aPresMIns_rep, t => m_app.aPresMIns_rep = t, value);
            }
        }


        public decimal aPresHoAssocDues
        {
            get { return m_app.aPresHoAssocDues; }
            set
            {
                m_validator.TryToWriteField("aPresHoAssocDues", () => m_app.aPresHoAssocDues, t => m_app.aPresHoAssocDues = t, value);
            }
        }


        public string aPresHoAssocDues_rep
        {
            get { return m_app.aPresHoAssocDues_rep; }
            set
            {
                m_validator.TryToWriteField("aPresHoAssocDues_rep", () => m_app.aPresHoAssocDues_rep, t => m_app.aPresHoAssocDues_rep = t, value);
            }
        }


        public string aPresOHExpDesc
        {
            get { return m_app.aPresOHExpDesc; }
            set
            {
                m_validator.TryToWriteField("aPresOHExpDesc", () => m_app.aPresOHExpDesc, t => m_app.aPresOHExpDesc = t, value);
            }
        }


        public decimal aPresOHExp
        {
            get { return m_app.aPresOHExp; }
            set
            {
                m_validator.TryToWriteField("aPresOHExp", () => m_app.aPresOHExp, t => m_app.aPresOHExp = t, value);
            }
        }


        public string aPresOHExp_rep
        {
            get { return m_app.aPresOHExp_rep; }
            set
            {
                m_validator.TryToWriteField("aPresOHExp_rep", () => m_app.aPresOHExp_rep, t => m_app.aPresOHExp_rep = t, value);
            }
        }


        public decimal aPresTotHExpCalc
        {
            get { return m_app.aPresTotHExpCalc; }
        }


        public string aPresTotHExpCalc_rep
        {
            get { return m_app.aPresTotHExpCalc_rep; }
            set
            {
                m_validator.TryToWriteField("aPresTotHExpCalc_rep", () => m_app.aPresTotHExpCalc_rep, t => m_app.aPresTotHExpCalc_rep = t, value);
            }
        }


        public decimal aPresTotHExp
        {
            get { return m_app.aPresTotHExp; }
        }


        public string aPresTotHExp_rep
        {
            get { return m_app.aPresTotHExp_rep; }
        }


        public bool aPresTotHExpLckd
        {
            get { return m_app.aPresTotHExpLckd; }
            set
            {
                m_validator.TryToWriteField("aPresTotHExpLckd", () => m_app.aPresTotHExpLckd, t => m_app.aPresTotHExpLckd = t, value);
            }
        }


        public string aPresTotHExpDesc
        {
            get { return m_app.aPresTotHExpDesc; }
            set
            {
                m_validator.TryToWriteField("aPresTotHExpDesc", () => m_app.aPresTotHExpDesc, t => m_app.aPresTotHExpDesc = t, value);
            }
        }

        public E_aOccT aOccT
        {
            get { return m_app.aOccT; }
            set
            {
                m_validator.TryToWriteField("aOccT", () => m_app.aOccT, t => m_app.aOccT = t, value);
            }
        }


        public string aOccT_rep
        {
            get { return m_app.aOccT_rep; }
        }


        public decimal aLiaBalTot
        {
            get { return m_app.aLiaBalTot; }
        }


        public string aLiaBalTot_rep
        {
            get { return m_app.aLiaBalTot_rep; }
        }


        public decimal aLiaMonTot
        {
            get { return m_app.aLiaMonTot; }
        }


        public string aLiaMonTot_rep
        {
            get { return m_app.aLiaMonTot_rep; }
        }


        public decimal aLiaMonTotRegardlessOfPdOff
        {
            get { return m_app.aLiaMonTotRegardlessOfPdOff; }
        }


        public string aLiaMonTotRegardlessOfPdOff_rep
        {
            get { return m_app.aLiaMonTotRegardlessOfPdOff_rep; }
        }


        public decimal aLiaPdOffTot
        {
            get { return m_app.aLiaPdOffTot; }
        }


        public string aLiaPdOffTot_rep
        {
            get { return m_app.aLiaPdOffTot_rep; }
        }


        public string aU1DocStatDesc
        {
            get { return m_app.aU1DocStatDesc; }
            set
            {
                m_validator.TryToWriteField("aU1DocStatDesc", () => m_app.aU1DocStatDesc, t => m_app.aU1DocStatDesc = t, value);
            }
        }


        public CDateTime aU1DocStatOd
        {
            get { return m_app.aU1DocStatOd; }
            set
            {
                m_validator.TryToWriteField("aU1DocStatOd", () => m_app.aU1DocStatOd, t => m_app.aU1DocStatOd = t, value);
            }
        }


        public string aU1DocStatOd_rep
        {
            get { return m_app.aU1DocStatOd_rep; }
            set
            {
                m_validator.TryToWriteField("aU1DocStatOd_rep", () => m_app.aU1DocStatOd_rep, t => m_app.aU1DocStatOd_rep = t, value);
            }
        }


        public CDateTime aU1DocStatRd
        {
            get { return m_app.aU1DocStatRd; }
            set
            {
                m_validator.TryToWriteField("aU1DocStatRd", () => m_app.aU1DocStatRd, t => m_app.aU1DocStatRd = t, value);
            }
        }


        public string aU1DocStatRd_rep
        {
            get { return m_app.aU1DocStatRd_rep; }
            set
            {
                m_validator.TryToWriteField("aU1DocStatRd_rep", () => m_app.aU1DocStatRd_rep, t => m_app.aU1DocStatRd_rep = t, value);
            }
        }


        public string aU1DocStatN
        {
            get { return m_app.aU1DocStatN; }
            set
            {
                m_validator.TryToWriteField("aU1DocStatN", () => m_app.aU1DocStatN, t => m_app.aU1DocStatN = t, value);
            }
        }


        public string aU2DocStatDesc
        {
            get { return m_app.aU2DocStatDesc; }
            set
            {
                m_validator.TryToWriteField("aU2DocStatDesc", () => m_app.aU2DocStatDesc, t => m_app.aU2DocStatDesc = t, value);
            }
        }


        public CDateTime aU2DocStatOd
        {
            get { return m_app.aU2DocStatOd; }
            set
            {
                m_validator.TryToWriteField("aU2DocStatOd", () => m_app.aU2DocStatOd, t => m_app.aU2DocStatOd = t, value);
            }
        }


        public string aU2DocStatOd_rep
        {
            get { return m_app.aU2DocStatOd_rep; }
            set
            {
                m_validator.TryToWriteField("aU2DocStatOd_rep", () => m_app.aU2DocStatOd_rep, t => m_app.aU2DocStatOd_rep = t, value);
            }
        }


        public CDateTime aU2DocStatRd
        {
            get { return m_app.aU2DocStatRd; }
        }


        public string aU2DocStatRd_rep
        {
            get { return m_app.aU2DocStatRd_rep; }
            set
            {
                m_validator.TryToWriteField("aU2DocStatRd_rep", () => m_app.aU2DocStatRd_rep, t => m_app.aU2DocStatRd_rep = t, value);
            }
        }


        public string aU2DocStatN
        {
            get { return m_app.aU2DocStatN; }
            set
            {
                m_validator.TryToWriteField("aU2DocStatN", () => m_app.aU2DocStatN, t => m_app.aU2DocStatN = t, value);
            }
        }

        public E_aPreFundVoeTypeT aBPreFundVoeTypeT
        {
            get { return m_app.aBPreFundVoeTypeT; }
            set
            {
                m_validator.TryToWriteField("aBPreFundVoeTypeT", () => m_app.aBPreFundVoeTypeT, t => m_app.aBPreFundVoeTypeT = t, value);
            }
        }
        public CDateTime aBPreFundVoeOd
        {
            get { return m_app.aBPreFundVoeOd; }
            set
            {
                m_validator.TryToWriteField("aBPreFundVoeOd", () => m_app.aBPreFundVoeOd, t => m_app.aBPreFundVoeOd = t, value);
            }
        }
        public string aBPreFundVoeOd_rep
        {
            get { return m_app.aBPreFundVoeOd_rep; }
            set
            {
                m_validator.TryToWriteField("aBPreFundVoeOd_rep", () => m_app.aBPreFundVoeOd_rep, t => m_app.aBPreFundVoeOd_rep = t, value);
            }
        }
        public CDateTime aBPreFundVoeDueD
        {
            get { return m_app.aBPreFundVoeDueD; }
            set
            {
                m_validator.TryToWriteField("aBPreFundVoeDueD", () => m_app.aBPreFundVoeDueD, t => m_app.aBPreFundVoeDueD = t, value);
            }
        }
        public string aBPreFundVoeDueD_rep
        {
            get { return m_app.aBPreFundVoeDueD_rep; }
            set
            {
                m_validator.TryToWriteField("aBPreFundVoeDueD_rep", () => m_app.aBPreFundVoeDueD_rep, t => m_app.aBPreFundVoeDueD_rep = t, value);
            }
        }
        public CDateTime aBPreFundVoeRd
        {
            get { return m_app.aBPreFundVoeRd; }
            set
            {
                m_validator.TryToWriteField("aBPreFundVoeRd", () => m_app.aBPreFundVoeRd, t => m_app.aBPreFundVoeRd = t, value);
            }
        }
        public string aBPreFundVoeRd_rep
        {
            get { return m_app.aBPreFundVoeRd_rep; }
            set
            {
                m_validator.TryToWriteField("aBPreFundVoeRd_rep", () => m_app.aBPreFundVoeRd_rep, t => m_app.aBPreFundVoeRd_rep = t, value);
            }
        }
        public string aBPreFundVoeN
        {
            get { return m_app.aBPreFundVoeN; }
            set
            {
                m_validator.TryToWriteField("aBPreFundVoeN", () => m_app.aBPreFundVoeN, t => m_app.aBPreFundVoeN = t, value);
            }
        }

        public E_aPreFundVoeTypeT aCPreFundVoeTypeT
        {
            get { return m_app.aCPreFundVoeTypeT; }
            set
            {
                m_validator.TryToWriteField("aCPreFundVoeTypeT", () => m_app.aCPreFundVoeTypeT, t => m_app.aCPreFundVoeTypeT = t, value);
            }
        }
        public CDateTime aCPreFundVoeOd
        {
            get { return m_app.aCPreFundVoeOd; }
            set
            {
                m_validator.TryToWriteField("aCPreFundVoeOd", () => m_app.aCPreFundVoeOd, t => m_app.aCPreFundVoeOd = t, value);
            }
        }
        public string aCPreFundVoeOd_rep
        {
            get { return m_app.aCPreFundVoeOd_rep; }
            set
            {
                m_validator.TryToWriteField("aCPreFundVoeOd_rep", () => m_app.aCPreFundVoeOd_rep, t => m_app.aCPreFundVoeOd_rep = t, value);
            }
        }
        public CDateTime aCPreFundVoeDueD
        {
            get { return m_app.aCPreFundVoeDueD; }
            set
            {
                m_validator.TryToWriteField("aCPreFundVoeDueD", () => m_app.aCPreFundVoeDueD, t => m_app.aCPreFundVoeDueD = t, value);
            }
        }
        public string aCPreFundVoeDueD_rep
        {
            get { return m_app.aCPreFundVoeDueD_rep; }
            set
            {
                m_validator.TryToWriteField("aCPreFundVoeDueD_rep", () => m_app.aCPreFundVoeDueD_rep, t => m_app.aCPreFundVoeDueD_rep = t, value);
            }
        }
        public CDateTime aCPreFundVoeRd
        {
            get { return m_app.aCPreFundVoeRd; }
            set
            {
                m_validator.TryToWriteField("aCPreFundVoeRd", () => m_app.aCPreFundVoeRd, t => m_app.aCPreFundVoeRd = t, value);
            }
        }
        public string aCPreFundVoeRd_rep
        {
            get { return m_app.aCPreFundVoeRd_rep; }
            set
            {
                m_validator.TryToWriteField("aCPreFundVoeRd_rep", () => m_app.aCPreFundVoeRd_rep, t => m_app.aCPreFundVoeRd_rep = t, value);
            }
        }
        public string aCPreFundVoeN
        {
            get { return m_app.aCPreFundVoeN; }
            set
            {
                m_validator.TryToWriteField("aCPreFundVoeN", () => m_app.aCPreFundVoeN, t => m_app.aCPreFundVoeN = t, value);
            }
        }

        public CDateTime aCrOd
        {
            get { return m_app.aCrOd; }
            set
            {
                m_validator.TryToWriteField("aCrOd", () => m_app.aCrOd, t => m_app.aCrOd = t, value);
            }
        }


        public string aCrOd_rep
        {
            get { return m_app.aCrOd_rep; }
            set
            {
                m_validator.TryToWriteField("aCrOd_rep", () => m_app.aCrOd_rep, t => m_app.aCrOd_rep = t, value);
            }
        }


        public CDateTime aCrRd
        {
            get { return m_app.aCrRd; }
            set
            {
                m_validator.TryToWriteField("aCrRd", () => m_app.aCrRd, t => m_app.aCrRd = t, value);
            }
        }


        public string aCrRd_rep
        {
            get { return m_app.aCrRd_rep; }
            set
            {
                m_validator.TryToWriteField("aCrRd_rep", () => m_app.aCrRd_rep, t => m_app.aCrRd_rep = t, value);
            }
        }


        public string aCrN
        {
            get { return m_app.aCrN; }
            set
            {
                m_validator.TryToWriteField("aCrN", () => m_app.aCrN, t => m_app.aCrN = t, value);
            }
        }

        public CDateTime aLqiCrOd
        {
            get { return m_app.aLqiCrOd; }
            set
            {
                m_validator.TryToWriteField("aLqiCrOd", () => m_app.aLqiCrOd, t => m_app.aLqiCrOd = t, value);
            }
        }

        public string aLqiCrOd_rep
        {
            get { return m_app.aLqiCrOd_rep; }
            set
            {
                m_validator.TryToWriteField("aLqiCrOd_rep", () => m_app.aLqiCrOd_rep, t => m_app.aLqiCrOd_rep = t, value);
            }
        }

        public CDateTime aLqiCrDueD
        {
            get { return m_app.aLqiCrDueD; }
            set
            {
                m_validator.TryToWriteField("aLqiCrDueD", () => m_app.aLqiCrDueD, t => m_app.aLqiCrDueD = t, value);
            }
        }

        public string aLqiCrDueD_rep
        {
            get { return m_app.aLqiCrDueD_rep; }
            set
            {
                m_validator.TryToWriteField("aLqiCrDueD_rep", () => m_app.aLqiCrDueD_rep, t => m_app.aLqiCrDueD_rep = t, value);
            }
        }

        public CDateTime aLqiCrRd
        {
            get { return m_app.aLqiCrRd; }
            set
            {
                m_validator.TryToWriteField("aLqiCrRd", () => m_app.aLqiCrRd, t => m_app.aLqiCrRd = t, value);
            }
        }

        public string aLqiCrRd_rep
        {
            get { return m_app.aLqiCrRd_rep; }
            set
            {
                m_validator.TryToWriteField("aLqiCrRd_rep", () => m_app.aLqiCrRd_rep, t => m_app.aLqiCrRd_rep = t, value);
            }
        }

        public string aLqiCrN
        {
            get { return m_app.aLqiCrN; }
            set
            {
                m_validator.TryToWriteField("aLqiCrN", () => m_app.aLqiCrN, t => m_app.aLqiCrN = t, value);
            }
        }

        public CDateTime aBusCrOd
        {
            get { return m_app.aBusCrOd; }
            set
            {
                m_validator.TryToWriteField("aBusCrOd", () => m_app.aBusCrOd, t => m_app.aBusCrOd = t, value);
            }
        }


        public string aBusCrOd_rep
        {
            get { return m_app.aBusCrOd_rep; }
            set
            {
                m_validator.TryToWriteField("aBusCrOd_rep", () => m_app.aBusCrOd_rep, t => m_app.aBusCrOd_rep = t, value);
            }
        }


        public CDateTime aBusCrRd
        {
            get { return m_app.aBusCrRd; }
            set
            {
                m_validator.TryToWriteField("aBusCrRd", () => m_app.aBusCrRd, t => m_app.aBusCrRd = t, value);
            }
        }


        public string aBusCrRd_rep
        {
            get { return m_app.aBusCrRd_rep; }
            set
            {
                m_validator.TryToWriteField("aBusCrRd_rep", () => m_app.aBusCrRd_rep, t => m_app.aBusCrRd_rep = t, value);
            }
        }


        public string aBusCrN
        {
            get { return m_app.aBusCrN; }
            set
            {
                m_validator.TryToWriteField("aBusCrN", () => m_app.aBusCrN, t => m_app.aBusCrN = t, value);
            }
        }


        public CDateTime aCrDueD
        {
            get { return m_app.aCrDueD; }
            set
            {
                m_validator.TryToWriteField("aCrDueD", () => m_app.aCrDueD, t => m_app.aCrDueD = t, value);
            }
        }


        public string aCrDueD_rep
        {
            get { return m_app.aCrDueD_rep; }
            set
            {
                m_validator.TryToWriteField("aCrDueD_rep", () => m_app.aCrDueD_rep, t => m_app.aCrDueD_rep = t, value);
            }
        }


        public CDateTime aBusCrDueD
        {
            get { return m_app.aBusCrDueD; }
            set
            {
                m_validator.TryToWriteField("aBusCrDueD", () => m_app.aBusCrDueD, t => m_app.aBusCrDueD = t, value);
            }
        }


        public string aBusCrDueD_rep
        {
            get { return m_app.aBusCrDueD_rep; }
            set
            {
                m_validator.TryToWriteField("aBusCrDueD_rep", () => m_app.aBusCrDueD_rep, t => m_app.aBusCrDueD_rep = t, value);
            }
        }


        public CDateTime aU1DocStatDueD
        {
            get { return m_app.aU1DocStatDueD; }
        }


        public string aU1DocStatDueD_rep
        {
            get { return m_app.aU1DocStatDueD_rep; }
            set
            {
                m_validator.TryToWriteField("aU1DocStatDueD_rep", () => m_app.aU1DocStatDueD_rep, t => m_app.aU1DocStatDueD_rep = t, value);
            }
        }


        public CDateTime aU2DocStatDueD
        {
            get { return m_app.aU2DocStatDueD; }
            set
            {
                m_validator.TryToWriteField("aU2DocStatDueD", () => m_app.aU2DocStatDueD, t => m_app.aU2DocStatDueD = t, value);
            }
        }


        public string aU2DocStatDueD_rep
        {
            get { return m_app.aU2DocStatDueD_rep; }
            set
            {
                m_validator.TryToWriteField("aU2DocStatDueD_rep", () => m_app.aU2DocStatDueD_rep, t => m_app.aU2DocStatDueD_rep = t, value);
            }
        }

        public CDateTime aUDNOrderedD
        {
            get { return m_app.aUDNOrderedD; }
            set
            {
                m_validator.TryToWriteField("aUDNOrderedD", () => m_app.aUDNOrderedD, t => m_app.aUDNOrderedD = t, value);
            }
        }

        public string aUDNOrderedD_rep
        {
            get { return m_app.aUDNOrderedD_rep; }
            set
            {
                m_validator.TryToWriteField("aUDNOrderedD_rep", () => m_app.aUDNOrderedD_rep, t => m_app.aUDNOrderedD_rep = t, value);
            }
        }

        public CDateTime aUDNDeactivatedD
        {
            get { return m_app.aUDNDeactivatedD; }
            set
            {
                m_validator.TryToWriteField("aUDNDeactivatedD", () => m_app.aUDNDeactivatedD, t => m_app.aUDNDeactivatedD = t, value);
            }
        }

        public string aUDNDeactivatedD_rep
        {
            get { return m_app.aUDNDeactivatedD_rep; }
            set
            {
                m_validator.TryToWriteField("aUDNDeactivatedD_rep", () => m_app.aUDNDeactivatedD_rep, t => m_app.aUDNDeactivatedD_rep = t, value);
            }
        }

        public string aUdnOrderId
        {
            get
            {
                return m_app.aUdnOrderId;
            }
            set
            {
                m_validator.TryToWriteField("aUdnOrderId", () => m_app.aUdnOrderId, t => m_app.aUdnOrderId = t, value);
            }
        }

        public bool aSpouseIExcl
        {
            get { return m_app.aSpouseIExcl; }
            set
            {
                m_validator.TryToWriteField("aSpouseIExcl", () => m_app.aSpouseIExcl, t => m_app.aSpouseIExcl = t, value);
            }
        }


        public decimal aNetWorth
        {
            get { return m_app.aNetWorth; }
        }


        public string aNetWorth_rep
        {
            get { return m_app.aNetWorth_rep; }
        }

        public E_TriState aBDecIsPrimaryResidenceUlad
        {
            get { return this.m_app.aBDecIsPrimaryResidenceUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecIsPrimaryResidenceUlad",
                  () => this.m_app.aBDecIsPrimaryResidenceUlad,
                  t => this.m_app.aBDecIsPrimaryResidenceUlad = t,
                  value);
            }
        }

        public E_TriState aCDecIsPrimaryResidenceUlad
        {
            get { return this.m_app.aCDecIsPrimaryResidenceUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecIsPrimaryResidenceUlad",
                  () => this.m_app.aCDecIsPrimaryResidenceUlad,
                  t => this.m_app.aCDecIsPrimaryResidenceUlad = t,
                  value);
            }
        }

        public E_TriState aDecIsPrimaryResidenceUlad
        {
            get { return this.m_app.aDecIsPrimaryResidenceUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecIsPrimaryResidenceUlad",
                  () => this.m_app.aDecIsPrimaryResidenceUlad,
                  t => this.m_app.aDecIsPrimaryResidenceUlad = t,
                  value);
            }
        }

        public string aBDecIsPrimaryResidenceExplanationUlad
        {
            get { return this.m_app.aBDecIsPrimaryResidenceExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecIsPrimaryResidenceExplanationUlad",
                  () => this.m_app.aBDecIsPrimaryResidenceExplanationUlad,
                  t => this.m_app.aBDecIsPrimaryResidenceExplanationUlad = t,
                  value);
            }
        }

        public string aCDecIsPrimaryResidenceExplanationUlad
        {
            get { return this.m_app.aCDecIsPrimaryResidenceExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecIsPrimaryResidenceExplanationUlad",
                  () => this.m_app.aCDecIsPrimaryResidenceExplanationUlad,
                  t => this.m_app.aCDecIsPrimaryResidenceExplanationUlad = t,
                  value);
            }
        }

        public string aDecIsPrimaryResidenceExplanationUlad
        {
            get { return this.m_app.aDecIsPrimaryResidenceExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecIsPrimaryResidenceExplanationUlad",
                  () => this.m_app.aDecIsPrimaryResidenceExplanationUlad,
                  t => this.m_app.aDecIsPrimaryResidenceExplanationUlad = t,
                  value);
            }
        }

        public E_TriState aBDecHasOwnershipIntOtherPropLastThreeYearsUlad
        {
            get { return this.m_app.aBDecHasOwnershipIntOtherPropLastThreeYearsUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecHasOwnershipIntOtherPropLastThreeYearsUlad",
                  () => this.m_app.aBDecHasOwnershipIntOtherPropLastThreeYearsUlad,
                  t => this.m_app.aBDecHasOwnershipIntOtherPropLastThreeYearsUlad = t,
                  value);
            }
        }

        public E_TriState aCDecHasOwnershipIntOtherPropLastThreeYearsUlad
        {
            get { return this.m_app.aCDecHasOwnershipIntOtherPropLastThreeYearsUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecHasOwnershipIntOtherPropLastThreeYearsUlad",
                  () => this.m_app.aCDecHasOwnershipIntOtherPropLastThreeYearsUlad,
                  t => this.m_app.aCDecHasOwnershipIntOtherPropLastThreeYearsUlad = t,
                  value);
            }
        }

        public E_TriState aDecHasOwnershipIntOtherPropLastThreeYearsUlad
        {
            get { return this.m_app.aDecHasOwnershipIntOtherPropLastThreeYearsUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecHasOwnershipIntOtherPropLastThreeYearsUlad",
                  () => this.m_app.aDecHasOwnershipIntOtherPropLastThreeYearsUlad,
                  t => this.m_app.aDecHasOwnershipIntOtherPropLastThreeYearsUlad = t,
                  value);
            }
        }

        public string aBDecHasOwnershipIntOtherPropLastThreeYearsExplanationUlad
        {
            get { return this.m_app.aBDecHasOwnershipIntOtherPropLastThreeYearsExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecHasOwnershipIntOtherPropLastThreeYearsExplanationUlad",
                  () => this.m_app.aBDecHasOwnershipIntOtherPropLastThreeYearsExplanationUlad,
                  t => this.m_app.aBDecHasOwnershipIntOtherPropLastThreeYearsExplanationUlad = t,
                  value);
            }
        }

        public string aCDecHasOwnershipIntOtherPropLastThreeYearsExplanationUlad
        {
            get { return this.m_app.aCDecHasOwnershipIntOtherPropLastThreeYearsExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecHasOwnershipIntOtherPropLastThreeYearsExplanationUlad",
                  () => this.m_app.aCDecHasOwnershipIntOtherPropLastThreeYearsExplanationUlad,
                  t => this.m_app.aCDecHasOwnershipIntOtherPropLastThreeYearsExplanationUlad = t,
                  value);
            }
        }

        public string aDecHasOwnershipIntOtherPropLastThreeYearsExplanationUlad
        {
            get { return this.m_app.aDecHasOwnershipIntOtherPropLastThreeYearsExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecHasOwnershipIntOtherPropLastThreeYearsExplanationUlad",
                  () => this.m_app.aDecHasOwnershipIntOtherPropLastThreeYearsExplanationUlad,
                  t => this.m_app.aDecHasOwnershipIntOtherPropLastThreeYearsExplanationUlad = t,
                  value);
            }
        }

        public E_aBDecPastOwnedPropT aBDecOwnershipIntOtherPropLastThreeYearsPropTypeUlad
        {
            get { return this.m_app.aBDecOwnershipIntOtherPropLastThreeYearsPropTypeUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecOwnershipIntOtherPropLastThreeYearsPropTypeUlad",
                  () => this.m_app.aBDecOwnershipIntOtherPropLastThreeYearsPropTypeUlad,
                  t => this.m_app.aBDecOwnershipIntOtherPropLastThreeYearsPropTypeUlad = t,
                  value);
            }
        }

        public E_aBDecPastOwnedPropT aCDecOwnershipIntOtherPropLastThreeYearsPropTypeUlad
        {
            get { return this.m_app.aCDecOwnershipIntOtherPropLastThreeYearsPropTypeUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecOwnershipIntOtherPropLastThreeYearsPropTypeUlad",
                  () => this.m_app.aCDecOwnershipIntOtherPropLastThreeYearsPropTypeUlad,
                  t => this.m_app.aCDecOwnershipIntOtherPropLastThreeYearsPropTypeUlad = t,
                  value);
            }
        }

        public E_aBDecPastOwnedPropT aDecOwnershipIntOtherPropLastThreeYearsPropTypeUlad
        {
            get { return this.m_app.aDecOwnershipIntOtherPropLastThreeYearsPropTypeUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecOwnershipIntOtherPropLastThreeYearsPropTypeUlad",
                  () => this.m_app.aDecOwnershipIntOtherPropLastThreeYearsPropTypeUlad,
                  t => this.m_app.aDecOwnershipIntOtherPropLastThreeYearsPropTypeUlad = t,
                  value);
            }
        }

        public E_aBDecPastOwnedPropTitleT aBDecOwnershipIntOtherPropLastThreeYearsTitleHoldTypeUlad
        {
            get { return this.m_app.aBDecOwnershipIntOtherPropLastThreeYearsTitleHoldTypeUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecOwnershipIntOtherPropLastThreeYearsTitleHoldTypeUlad",
                  () => this.m_app.aBDecOwnershipIntOtherPropLastThreeYearsTitleHoldTypeUlad,
                  t => this.m_app.aBDecOwnershipIntOtherPropLastThreeYearsTitleHoldTypeUlad = t,
                  value);
            }
        }

        public E_aBDecPastOwnedPropTitleT aCDecOwnershipIntOtherPropLastThreeYearsTitleHoldTypeUlad
        {
            get { return this.m_app.aCDecOwnershipIntOtherPropLastThreeYearsTitleHoldTypeUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecOwnershipIntOtherPropLastThreeYearsTitleHoldTypeUlad",
                  () => this.m_app.aCDecOwnershipIntOtherPropLastThreeYearsTitleHoldTypeUlad,
                  t => this.m_app.aCDecOwnershipIntOtherPropLastThreeYearsTitleHoldTypeUlad = t,
                  value);
            }
        }

        public E_aBDecPastOwnedPropTitleT aDecOwnershipIntOtherPropLastThreeYearsTitleHoldTypeUlad
        {
            get { return this.m_app.aDecOwnershipIntOtherPropLastThreeYearsTitleHoldTypeUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecOwnershipIntOtherPropLastThreeYearsTitleHoldTypeUlad",
                  () => this.m_app.aDecOwnershipIntOtherPropLastThreeYearsTitleHoldTypeUlad,
                  t => this.m_app.aDecOwnershipIntOtherPropLastThreeYearsTitleHoldTypeUlad = t,
                  value);
            }
        }

        public E_TriState aBDecIsFamilyOrBusAffiliateOfSellerUlad
        {
            get { return this.m_app.aBDecIsFamilyOrBusAffiliateOfSellerUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecIsFamilyOrBusAffiliateOfSellerUlad",
                  () => this.m_app.aBDecIsFamilyOrBusAffiliateOfSellerUlad,
                  t => this.m_app.aBDecIsFamilyOrBusAffiliateOfSellerUlad = t,
                  value);
            }
        }

        public E_TriState aCDecIsFamilyOrBusAffiliateOfSellerUlad
        {
            get { return this.m_app.aCDecIsFamilyOrBusAffiliateOfSellerUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecIsFamilyOrBusAffiliateOfSellerUlad",
                  () => this.m_app.aCDecIsFamilyOrBusAffiliateOfSellerUlad,
                  t => this.m_app.aCDecIsFamilyOrBusAffiliateOfSellerUlad = t,
                  value);
            }
        }

        public E_TriState aDecIsFamilyOrBusAffiliateOfSellerUlad
        {
            get { return this.m_app.aDecIsFamilyOrBusAffiliateOfSellerUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecIsFamilyOrBusAffiliateOfSellerUlad",
                  () => this.m_app.aDecIsFamilyOrBusAffiliateOfSellerUlad,
                  t => this.m_app.aDecIsFamilyOrBusAffiliateOfSellerUlad = t,
                  value);
            }
        }

        public string aBDecIsFamilyOrBusAffiliateOfSellerExplanationUlad
        {
            get { return this.m_app.aBDecIsFamilyOrBusAffiliateOfSellerExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecIsFamilyOrBusAffiliateOfSellerExplanationUlad",
                  () => this.m_app.aBDecIsFamilyOrBusAffiliateOfSellerExplanationUlad,
                  t => this.m_app.aBDecIsFamilyOrBusAffiliateOfSellerExplanationUlad = t,
                  value);
            }
        }

        public string aCDecIsFamilyOrBusAffiliateOfSellerExplanationUlad
        {
            get { return this.m_app.aCDecIsFamilyOrBusAffiliateOfSellerExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecIsFamilyOrBusAffiliateOfSellerExplanationUlad",
                  () => this.m_app.aCDecIsFamilyOrBusAffiliateOfSellerExplanationUlad,
                  t => this.m_app.aCDecIsFamilyOrBusAffiliateOfSellerExplanationUlad = t,
                  value);
            }
        }

        public string aDecIsFamilyOrBusAffiliateOfSellerExplanationUlad
        {
            get { return this.m_app.aDecIsFamilyOrBusAffiliateOfSellerExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecIsFamilyOrBusAffiliateOfSellerExplanationUlad",
                  () => this.m_app.aDecIsFamilyOrBusAffiliateOfSellerExplanationUlad,
                  t => this.m_app.aDecIsFamilyOrBusAffiliateOfSellerExplanationUlad = t,
                  value);
            }
        }

        public E_TriState aBDecIsBorrowingFromOthersForTransUlad
        {
            get { return this.m_app.aBDecIsBorrowingFromOthersForTransUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecIsBorrowingFromOthersForTransUlad",
                  () => this.m_app.aBDecIsBorrowingFromOthersForTransUlad,
                  t => this.m_app.aBDecIsBorrowingFromOthersForTransUlad = t,
                  value);
            }
        }

        public E_TriState aCDecIsBorrowingFromOthersForTransUlad
        {
            get { return this.m_app.aCDecIsBorrowingFromOthersForTransUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecIsBorrowingFromOthersForTransUlad",
                  () => this.m_app.aCDecIsBorrowingFromOthersForTransUlad,
                  t => this.m_app.aCDecIsBorrowingFromOthersForTransUlad = t,
                  value);
            }
        }

        public E_TriState aDecIsBorrowingFromOthersForTransUlad
        {
            get { return this.m_app.aDecIsBorrowingFromOthersForTransUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecIsBorrowingFromOthersForTransUlad",
                  () => this.m_app.aDecIsBorrowingFromOthersForTransUlad,
                  t => this.m_app.aDecIsBorrowingFromOthersForTransUlad = t,
                  value);
            }
        }

        public string aBDecIsBorrowingFromOthersForTransExplanationUlad
        {
            get { return this.m_app.aBDecIsBorrowingFromOthersForTransExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecIsBorrowingFromOthersForTransExplanationUlad",
                  () => this.m_app.aBDecIsBorrowingFromOthersForTransExplanationUlad,
                  t => this.m_app.aBDecIsBorrowingFromOthersForTransExplanationUlad = t,
                  value);
            }
        }

        public string aCDecIsBorrowingFromOthersForTransExplanationUlad
        {
            get { return this.m_app.aCDecIsBorrowingFromOthersForTransExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecIsBorrowingFromOthersForTransExplanationUlad",
                  () => this.m_app.aCDecIsBorrowingFromOthersForTransExplanationUlad,
                  t => this.m_app.aCDecIsBorrowingFromOthersForTransExplanationUlad = t,
                  value);
            }
        }

        public string aDecIsBorrowingFromOthersForTransExplanationUlad
        {
            get { return this.m_app.aDecIsBorrowingFromOthersForTransExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecIsBorrowingFromOthersForTransExplanationUlad",
                  () => this.m_app.aDecIsBorrowingFromOthersForTransExplanationUlad,
                  t => this.m_app.aDecIsBorrowingFromOthersForTransExplanationUlad = t,
                  value);
            }
        }

        public decimal aBDecAmtBorrowedFromOthersForTransUlad
        {
            get { return this.m_app.aBDecAmtBorrowedFromOthersForTransUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecAmtBorrowedFromOthersForTransUlad",
                  () => this.m_app.aBDecAmtBorrowedFromOthersForTransUlad,
                  t => this.m_app.aBDecAmtBorrowedFromOthersForTransUlad = t,
                  value);
            }
        }

        public decimal aCDecAmtBorrowedFromOthersForTransUlad
        {
            get { return this.m_app.aCDecAmtBorrowedFromOthersForTransUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecAmtBorrowedFromOthersForTransUlad",
                  () => this.m_app.aCDecAmtBorrowedFromOthersForTransUlad,
                  t => this.m_app.aCDecAmtBorrowedFromOthersForTransUlad = t,
                  value);
            }
        }

        public decimal aDecAmtBorrowedFromOthersForTransUlad
        {
            get { return this.m_app.aDecAmtBorrowedFromOthersForTransUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecAmtBorrowedFromOthersForTransUlad",
                  () => this.m_app.aDecAmtBorrowedFromOthersForTransUlad,
                  t => this.m_app.aDecAmtBorrowedFromOthersForTransUlad = t,
                  value);
            }
        }

        public E_TriState aBDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscUlad
        {
            get { return this.m_app.aBDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscUlad",
                  () => this.m_app.aBDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscUlad,
                  t => this.m_app.aBDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscUlad = t,
                  value);
            }
        }

        public E_TriState aCDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscUlad
        {
            get { return this.m_app.aCDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscUlad",
                  () => this.m_app.aCDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscUlad,
                  t => this.m_app.aCDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscUlad = t,
                  value);
            }
        }

        public E_TriState aDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscUlad
        {
            get { return this.m_app.aDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscUlad",
                  () => this.m_app.aDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscUlad,
                  t => this.m_app.aDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscUlad = t,
                  value);
            }
        }

        public string aBDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscExplanationUlad
        {
            get { return this.m_app.aBDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscExplanationUlad",
                  () => this.m_app.aBDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscExplanationUlad,
                  t => this.m_app.aBDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscExplanationUlad = t,
                  value);
            }
        }

        public string aCDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscExplanationUlad
        {
            get { return this.m_app.aCDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscExplanationUlad",
                  () => this.m_app.aCDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscExplanationUlad,
                  t => this.m_app.aCDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscExplanationUlad = t,
                  value);
            }
        }

        public string aDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscExplanationUlad
        {
            get { return this.m_app.aDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscExplanationUlad",
                  () => this.m_app.aDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscExplanationUlad,
                  t => this.m_app.aDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscExplanationUlad = t,
                  value);
            }
        }

        public E_TriState aBDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscUlad
        {
            get { return this.m_app.aBDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscUlad",
                  () => this.m_app.aBDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscUlad,
                  t => this.m_app.aBDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscUlad = t,
                  value);
            }
        }

        public E_TriState aCDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscUlad
        {
            get { return this.m_app.aCDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscUlad",
                  () => this.m_app.aCDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscUlad,
                  t => this.m_app.aCDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscUlad = t,
                  value);
            }
        }

        public E_TriState aDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscUlad
        {
            get { return this.m_app.aDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscUlad",
                  () => this.m_app.aDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscUlad,
                  t => this.m_app.aDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscUlad = t,
                  value);
            }
        }

        public string aBDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscExplanationUlad
        {
            get { return this.m_app.aBDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscExplanationUlad",
                  () => this.m_app.aBDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscExplanationUlad,
                  t => this.m_app.aBDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscExplanationUlad = t,
                  value);
            }
        }

        public string aCDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscExplanationUlad
        {
            get { return this.m_app.aCDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscExplanationUlad",
                  () => this.m_app.aCDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscExplanationUlad,
                  t => this.m_app.aCDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscExplanationUlad = t,
                  value);
            }
        }

        public string aDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscExplanationUlad
        {
            get { return this.m_app.aDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscExplanationUlad",
                  () => this.m_app.aDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscExplanationUlad,
                  t => this.m_app.aDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscExplanationUlad = t,
                  value);
            }
        }

        public E_TriState aBDecPropSubjectToHigherPriorityLienThanFirstMtgUlad
        {
            get { return this.m_app.aBDecPropSubjectToHigherPriorityLienThanFirstMtgUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecPropSubjectToHigherPriorityLienThanFirstMtgUlad",
                  () => this.m_app.aBDecPropSubjectToHigherPriorityLienThanFirstMtgUlad,
                  t => this.m_app.aBDecPropSubjectToHigherPriorityLienThanFirstMtgUlad = t,
                  value);
            }
        }

        public E_TriState aCDecPropSubjectToHigherPriorityLienThanFirstMtgUlad
        {
            get { return this.m_app.aCDecPropSubjectToHigherPriorityLienThanFirstMtgUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecPropSubjectToHigherPriorityLienThanFirstMtgUlad",
                  () => this.m_app.aCDecPropSubjectToHigherPriorityLienThanFirstMtgUlad,
                  t => this.m_app.aCDecPropSubjectToHigherPriorityLienThanFirstMtgUlad = t,
                  value);
            }
        }

        public E_TriState aDecPropSubjectToHigherPriorityLienThanFirstMtgUlad
        {
            get { return this.m_app.aDecPropSubjectToHigherPriorityLienThanFirstMtgUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecPropSubjectToHigherPriorityLienThanFirstMtgUlad",
                  () => this.m_app.aDecPropSubjectToHigherPriorityLienThanFirstMtgUlad,
                  t => this.m_app.aDecPropSubjectToHigherPriorityLienThanFirstMtgUlad = t,
                  value);
            }
        }

        public string aBDecPropSubjectToHigherPriorityLienThanFirstMtgExplanationUlad
        {
            get { return this.m_app.aBDecPropSubjectToHigherPriorityLienThanFirstMtgExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecPropSubjectToHigherPriorityLienThanFirstMtgExplanationUlad",
                  () => this.m_app.aBDecPropSubjectToHigherPriorityLienThanFirstMtgExplanationUlad,
                  t => this.m_app.aBDecPropSubjectToHigherPriorityLienThanFirstMtgExplanationUlad = t,
                  value);
            }
        }

        public string aCDecPropSubjectToHigherPriorityLienThanFirstMtgExplanationUlad
        {
            get { return this.m_app.aCDecPropSubjectToHigherPriorityLienThanFirstMtgExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecPropSubjectToHigherPriorityLienThanFirstMtgExplanationUlad",
                  () => this.m_app.aCDecPropSubjectToHigherPriorityLienThanFirstMtgExplanationUlad,
                  t => this.m_app.aCDecPropSubjectToHigherPriorityLienThanFirstMtgExplanationUlad = t,
                  value);
            }
        }

        public string aDecPropSubjectToHigherPriorityLienThanFirstMtgExplanationUlad
        {
            get { return this.m_app.aDecPropSubjectToHigherPriorityLienThanFirstMtgExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecPropSubjectToHigherPriorityLienThanFirstMtgExplanationUlad",
                  () => this.m_app.aDecPropSubjectToHigherPriorityLienThanFirstMtgExplanationUlad,
                  t => this.m_app.aDecPropSubjectToHigherPriorityLienThanFirstMtgExplanationUlad = t,
                  value);
            }
        }

        public E_TriState aBDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedUlad
        {
            get { return this.m_app.aBDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedUlad",
                  () => this.m_app.aBDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedUlad,
                  t => this.m_app.aBDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedUlad = t,
                  value);
            }
        }

        public E_TriState aCDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedUlad
        {
            get { return this.m_app.aCDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedUlad",
                  () => this.m_app.aCDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedUlad,
                  t => this.m_app.aCDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedUlad = t,
                  value);
            }
        }

        public E_TriState aDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedUlad
        {
            get { return this.m_app.aDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedUlad",
                  () => this.m_app.aDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedUlad,
                  t => this.m_app.aDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedUlad = t,
                  value);
            }
        }

        public string aBDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedExplanationUlad
        {
            get { return this.m_app.aBDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedExplanationUlad",
                  () => this.m_app.aBDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedExplanationUlad,
                  t => this.m_app.aBDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedExplanationUlad = t,
                  value);
            }
        }

        public string aCDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedExplanationUlad
        {
            get { return this.m_app.aCDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedExplanationUlad",
                  () => this.m_app.aCDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedExplanationUlad,
                  t => this.m_app.aCDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedExplanationUlad = t,
                  value);
            }
        }

        public string aDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedExplanationUlad
        {
            get { return this.m_app.aDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedExplanationUlad",
                  () => this.m_app.aDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedExplanationUlad,
                  t => this.m_app.aDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedExplanationUlad = t,
                  value);
            }
        }

        public E_TriState aBDecHasOutstandingJudgmentsUlad
        {
            get { return this.m_app.aBDecHasOutstandingJudgmentsUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecHasOutstandingJudgmentsUlad",
                  () => this.m_app.aBDecHasOutstandingJudgmentsUlad,
                  t => this.m_app.aBDecHasOutstandingJudgmentsUlad = t,
                  value);
            }
        }

        public E_TriState aCDecHasOutstandingJudgmentsUlad
        {
            get { return this.m_app.aCDecHasOutstandingJudgmentsUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecHasOutstandingJudgmentsUlad",
                  () => this.m_app.aCDecHasOutstandingJudgmentsUlad,
                  t => this.m_app.aCDecHasOutstandingJudgmentsUlad = t,
                  value);
            }
        }

        public E_TriState aDecHasOutstandingJudgmentsUlad
        {
            get { return this.m_app.aDecHasOutstandingJudgmentsUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecHasOutstandingJudgmentsUlad",
                  () => this.m_app.aDecHasOutstandingJudgmentsUlad,
                  t => this.m_app.aDecHasOutstandingJudgmentsUlad = t,
                  value);
            }
        }

        public string aBDecHasOutstandingJudgmentsExplanationUlad
        {
            get { return this.m_app.aBDecHasOutstandingJudgmentsExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecHasOutstandingJudgmentsExplanationUlad",
                  () => this.m_app.aBDecHasOutstandingJudgmentsExplanationUlad,
                  t => this.m_app.aBDecHasOutstandingJudgmentsExplanationUlad = t,
                  value);
            }
        }

        public string aCDecHasOutstandingJudgmentsExplanationUlad
        {
            get { return this.m_app.aCDecHasOutstandingJudgmentsExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecHasOutstandingJudgmentsExplanationUlad",
                  () => this.m_app.aCDecHasOutstandingJudgmentsExplanationUlad,
                  t => this.m_app.aCDecHasOutstandingJudgmentsExplanationUlad = t,
                  value);
            }
        }

        public string aDecHasOutstandingJudgmentsExplanationUlad
        {
            get { return this.m_app.aDecHasOutstandingJudgmentsExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecHasOutstandingJudgmentsExplanationUlad",
                  () => this.m_app.aDecHasOutstandingJudgmentsExplanationUlad,
                  t => this.m_app.aDecHasOutstandingJudgmentsExplanationUlad = t,
                  value);
            }
        }

        public E_TriState aBDecIsDelinquentOrDefaultFedDebtUlad
        {
            get { return this.m_app.aBDecIsDelinquentOrDefaultFedDebtUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecIsDelinquentOrDefaultFedDebtUlad",
                  () => this.m_app.aBDecIsDelinquentOrDefaultFedDebtUlad,
                  t => this.m_app.aBDecIsDelinquentOrDefaultFedDebtUlad = t,
                  value);
            }
        }

        public E_TriState aCDecIsDelinquentOrDefaultFedDebtUlad
        {
            get { return this.m_app.aCDecIsDelinquentOrDefaultFedDebtUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecIsDelinquentOrDefaultFedDebtUlad",
                  () => this.m_app.aCDecIsDelinquentOrDefaultFedDebtUlad,
                  t => this.m_app.aCDecIsDelinquentOrDefaultFedDebtUlad = t,
                  value);
            }
        }

        public E_TriState aDecIsDelinquentOrDefaultFedDebtUlad
        {
            get { return this.m_app.aDecIsDelinquentOrDefaultFedDebtUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecIsDelinquentOrDefaultFedDebtUlad",
                  () => this.m_app.aDecIsDelinquentOrDefaultFedDebtUlad,
                  t => this.m_app.aDecIsDelinquentOrDefaultFedDebtUlad = t,
                  value);
            }
        }

        public string aBDecIsDelinquentOrDefaultFedDebtExplanationUlad
        {
            get { return this.m_app.aBDecIsDelinquentOrDefaultFedDebtExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecIsDelinquentOrDefaultFedDebtExplanationUlad",
                  () => this.m_app.aBDecIsDelinquentOrDefaultFedDebtExplanationUlad,
                  t => this.m_app.aBDecIsDelinquentOrDefaultFedDebtExplanationUlad = t,
                  value);
            }
        }

        public string aCDecIsDelinquentOrDefaultFedDebtExplanationUlad
        {
            get { return this.m_app.aCDecIsDelinquentOrDefaultFedDebtExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecIsDelinquentOrDefaultFedDebtExplanationUlad",
                  () => this.m_app.aCDecIsDelinquentOrDefaultFedDebtExplanationUlad,
                  t => this.m_app.aCDecIsDelinquentOrDefaultFedDebtExplanationUlad = t,
                  value);
            }
        }

        public string aDecIsDelinquentOrDefaultFedDebtExplanationUlad
        {
            get { return this.m_app.aDecIsDelinquentOrDefaultFedDebtExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecIsDelinquentOrDefaultFedDebtExplanationUlad",
                  () => this.m_app.aDecIsDelinquentOrDefaultFedDebtExplanationUlad,
                  t => this.m_app.aDecIsDelinquentOrDefaultFedDebtExplanationUlad = t,
                  value);
            }
        }

        public E_TriState aBDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityUlad
        {
            get { return this.m_app.aBDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityUlad",
                  () => this.m_app.aBDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityUlad,
                  t => this.m_app.aBDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityUlad = t,
                  value);
            }
        }

        public E_TriState aCDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityUlad
        {
            get { return this.m_app.aCDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityUlad",
                  () => this.m_app.aCDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityUlad,
                  t => this.m_app.aCDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityUlad = t,
                  value);
            }
        }

        public E_TriState aDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityUlad
        {
            get { return this.m_app.aDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityUlad",
                  () => this.m_app.aDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityUlad,
                  t => this.m_app.aDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityUlad = t,
                  value);
            }
        }

        public string aBDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityExplanationUlad
        {
            get { return this.m_app.aBDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityExplanationUlad",
                  () => this.m_app.aBDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityExplanationUlad,
                  t => this.m_app.aBDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityExplanationUlad = t,
                  value);
            }
        }

        public string aCDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityExplanationUlad
        {
            get { return this.m_app.aCDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityExplanationUlad",
                  () => this.m_app.aCDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityExplanationUlad,
                  t => this.m_app.aCDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityExplanationUlad = t,
                  value);
            }
        }

        public string aDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityExplanationUlad
        {
            get { return this.m_app.aDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityExplanationUlad",
                  () => this.m_app.aDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityExplanationUlad,
                  t => this.m_app.aDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityExplanationUlad = t,
                  value);
            }
        }

        public E_TriState aBDecHasConveyedTitleInLieuOfFcLast7YearsUlad
        {
            get { return this.m_app.aBDecHasConveyedTitleInLieuOfFcLast7YearsUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecHasConveyedTitleInLieuOfFcLast7YearsUlad",
                  () => this.m_app.aBDecHasConveyedTitleInLieuOfFcLast7YearsUlad,
                  t => this.m_app.aBDecHasConveyedTitleInLieuOfFcLast7YearsUlad = t,
                  value);
            }
        }

        public E_TriState aCDecHasConveyedTitleInLieuOfFcLast7YearsUlad
        {
            get { return this.m_app.aCDecHasConveyedTitleInLieuOfFcLast7YearsUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecHasConveyedTitleInLieuOfFcLast7YearsUlad",
                  () => this.m_app.aCDecHasConveyedTitleInLieuOfFcLast7YearsUlad,
                  t => this.m_app.aCDecHasConveyedTitleInLieuOfFcLast7YearsUlad = t,
                  value);
            }
        }

        public E_TriState aDecHasConveyedTitleInLieuOfFcLast7YearsUlad
        {
            get { return this.m_app.aDecHasConveyedTitleInLieuOfFcLast7YearsUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecHasConveyedTitleInLieuOfFcLast7YearsUlad",
                  () => this.m_app.aDecHasConveyedTitleInLieuOfFcLast7YearsUlad,
                  t => this.m_app.aDecHasConveyedTitleInLieuOfFcLast7YearsUlad = t,
                  value);
            }
        }

        public string aBDecHasConveyedTitleInLieuOfFcLast7YearsExplanationUlad
        {
            get { return this.m_app.aBDecHasConveyedTitleInLieuOfFcLast7YearsExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecHasConveyedTitleInLieuOfFcLast7YearsExplanationUlad",
                  () => this.m_app.aBDecHasConveyedTitleInLieuOfFcLast7YearsExplanationUlad,
                  t => this.m_app.aBDecHasConveyedTitleInLieuOfFcLast7YearsExplanationUlad = t,
                  value);
            }
        }

        public string aCDecHasConveyedTitleInLieuOfFcLast7YearsExplanationUlad
        {
            get { return this.m_app.aCDecHasConveyedTitleInLieuOfFcLast7YearsExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecHasConveyedTitleInLieuOfFcLast7YearsExplanationUlad",
                  () => this.m_app.aCDecHasConveyedTitleInLieuOfFcLast7YearsExplanationUlad,
                  t => this.m_app.aCDecHasConveyedTitleInLieuOfFcLast7YearsExplanationUlad = t,
                  value);
            }
        }

        public string aDecHasConveyedTitleInLieuOfFcLast7YearsExplanationUlad
        {
            get { return this.m_app.aDecHasConveyedTitleInLieuOfFcLast7YearsExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecHasConveyedTitleInLieuOfFcLast7YearsExplanationUlad",
                  () => this.m_app.aDecHasConveyedTitleInLieuOfFcLast7YearsExplanationUlad,
                  t => this.m_app.aDecHasConveyedTitleInLieuOfFcLast7YearsExplanationUlad = t,
                  value);
            }
        }

        public E_TriState aBDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueUlad
        {
            get { return this.m_app.aBDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueUlad",
                  () => this.m_app.aBDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueUlad,
                  t => this.m_app.aBDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueUlad = t,
                  value);
            }
        }

        public E_TriState aCDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueUlad
        {
            get { return this.m_app.aCDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueUlad",
                  () => this.m_app.aCDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueUlad,
                  t => this.m_app.aCDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueUlad = t,
                  value);
            }
        }

        public E_TriState aDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueUlad
        {
            get { return this.m_app.aDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueUlad",
                  () => this.m_app.aDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueUlad,
                  t => this.m_app.aDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueUlad = t,
                  value);
            }
        }

        public string aBDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueExplanationUlad
        {
            get { return this.m_app.aBDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueExplanationUlad",
                  () => this.m_app.aBDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueExplanationUlad,
                  t => this.m_app.aBDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueExplanationUlad = t,
                  value);
            }
        }

        public string aCDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueExplanationUlad
        {
            get { return this.m_app.aCDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueExplanationUlad",
                  () => this.m_app.aCDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueExplanationUlad,
                  t => this.m_app.aCDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueExplanationUlad = t,
                  value);
            }
        }

        public string aDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueExplanationUlad
        {
            get { return this.m_app.aDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueExplanationUlad",
                  () => this.m_app.aDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueExplanationUlad,
                  t => this.m_app.aDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueExplanationUlad = t,
                  value);
            }
        }

        public E_TriState aBDecHasForeclosedLast7YearsUlad
        {
            get { return this.m_app.aBDecHasForeclosedLast7YearsUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecHasForeclosedLast7YearsUlad",
                  () => this.m_app.aBDecHasForeclosedLast7YearsUlad,
                  t => this.m_app.aBDecHasForeclosedLast7YearsUlad = t,
                  value);
            }
        }

        public E_TriState aCDecHasForeclosedLast7YearsUlad
        {
            get { return this.m_app.aCDecHasForeclosedLast7YearsUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecHasForeclosedLast7YearsUlad",
                  () => this.m_app.aCDecHasForeclosedLast7YearsUlad,
                  t => this.m_app.aCDecHasForeclosedLast7YearsUlad = t,
                  value);
            }
        }

        public E_TriState aDecHasForeclosedLast7YearsUlad
        {
            get { return this.m_app.aDecHasForeclosedLast7YearsUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecHasForeclosedLast7YearsUlad",
                  () => this.m_app.aDecHasForeclosedLast7YearsUlad,
                  t => this.m_app.aDecHasForeclosedLast7YearsUlad = t,
                  value);
            }
        }

        public string aBDecHasForeclosedLast7YearsExplanationUlad
        {
            get { return this.m_app.aBDecHasForeclosedLast7YearsExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecHasForeclosedLast7YearsExplanationUlad",
                  () => this.m_app.aBDecHasForeclosedLast7YearsExplanationUlad,
                  t => this.m_app.aBDecHasForeclosedLast7YearsExplanationUlad = t,
                  value);
            }
        }

        public string aCDecHasForeclosedLast7YearsExplanationUlad
        {
            get { return this.m_app.aCDecHasForeclosedLast7YearsExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecHasForeclosedLast7YearsExplanationUlad",
                  () => this.m_app.aCDecHasForeclosedLast7YearsExplanationUlad,
                  t => this.m_app.aCDecHasForeclosedLast7YearsExplanationUlad = t,
                  value);
            }
        }

        public string aDecHasForeclosedLast7YearsExplanationUlad
        {
            get { return this.m_app.aDecHasForeclosedLast7YearsExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecHasForeclosedLast7YearsExplanationUlad",
                  () => this.m_app.aDecHasForeclosedLast7YearsExplanationUlad,
                  t => this.m_app.aDecHasForeclosedLast7YearsExplanationUlad = t,
                  value);
            }
        }

        public E_TriState aBDecHasBankruptcyLast7YearsUlad
        {
            get { return this.m_app.aBDecHasBankruptcyLast7YearsUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecHasBankruptcyLast7YearsUlad",
                  () => this.m_app.aBDecHasBankruptcyLast7YearsUlad,
                  t => this.m_app.aBDecHasBankruptcyLast7YearsUlad = t,
                  value);
            }
        }

        public E_TriState aCDecHasBankruptcyLast7YearsUlad
        {
            get { return this.m_app.aCDecHasBankruptcyLast7YearsUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecHasBankruptcyLast7YearsUlad",
                  () => this.m_app.aCDecHasBankruptcyLast7YearsUlad,
                  t => this.m_app.aCDecHasBankruptcyLast7YearsUlad = t,
                  value);
            }
        }

        public E_TriState aDecHasBankruptcyLast7YearsUlad
        {
            get { return this.m_app.aDecHasBankruptcyLast7YearsUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecHasBankruptcyLast7YearsUlad",
                  () => this.m_app.aDecHasBankruptcyLast7YearsUlad,
                  t => this.m_app.aDecHasBankruptcyLast7YearsUlad = t,
                  value);
            }
        }

        public string aBDecHasBankruptcyLast7YearsExplanationUlad
        {
            get { return this.m_app.aBDecHasBankruptcyLast7YearsExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecHasBankruptcyLast7YearsExplanationUlad",
                  () => this.m_app.aBDecHasBankruptcyLast7YearsExplanationUlad,
                  t => this.m_app.aBDecHasBankruptcyLast7YearsExplanationUlad = t,
                  value);
            }
        }

        public string aCDecHasBankruptcyLast7YearsExplanationUlad
        {
            get { return this.m_app.aCDecHasBankruptcyLast7YearsExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecHasBankruptcyLast7YearsExplanationUlad",
                  () => this.m_app.aCDecHasBankruptcyLast7YearsExplanationUlad,
                  t => this.m_app.aCDecHasBankruptcyLast7YearsExplanationUlad = t,
                  value);
            }
        }

        public string aDecHasBankruptcyLast7YearsExplanationUlad
        {
            get { return this.m_app.aDecHasBankruptcyLast7YearsExplanationUlad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecHasBankruptcyLast7YearsExplanationUlad",
                  () => this.m_app.aDecHasBankruptcyLast7YearsExplanationUlad,
                  t => this.m_app.aDecHasBankruptcyLast7YearsExplanationUlad = t,
                  value);
            }
        }

        public bool aBDecHasBankruptcyChapter7Ulad
        {
            get { return this.m_app.aBDecHasBankruptcyChapter7Ulad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecHasBankruptcyChapter7Ulad",
                  () => this.m_app.aBDecHasBankruptcyChapter7Ulad,
                  t => this.m_app.aBDecHasBankruptcyChapter7Ulad = t,
                  value);
            }
        }

        public bool aCDecHasBankruptcyChapter7Ulad
        {
            get { return this.m_app.aCDecHasBankruptcyChapter7Ulad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecHasBankruptcyChapter7Ulad",
                  () => this.m_app.aCDecHasBankruptcyChapter7Ulad,
                  t => this.m_app.aCDecHasBankruptcyChapter7Ulad = t,
                  value);
            }
        }

        public bool aDecHasBankruptcyChapter7Ulad
        {
            get { return this.m_app.aDecHasBankruptcyChapter7Ulad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecHasBankruptcyChapter7Ulad",
                  () => this.m_app.aDecHasBankruptcyChapter7Ulad,
                  t => this.m_app.aDecHasBankruptcyChapter7Ulad = t,
                  value);
            }
        }

        public bool aBDecHasBankruptcyChapter11Ulad
        {
            get { return this.m_app.aBDecHasBankruptcyChapter11Ulad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecHasBankruptcyChapter11Ulad",
                  () => this.m_app.aBDecHasBankruptcyChapter11Ulad,
                  t => this.m_app.aBDecHasBankruptcyChapter11Ulad = t,
                  value);
            }
        }

        public bool aCDecHasBankruptcyChapter11Ulad
        {
            get { return this.m_app.aCDecHasBankruptcyChapter11Ulad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecHasBankruptcyChapter11Ulad",
                  () => this.m_app.aCDecHasBankruptcyChapter11Ulad,
                  t => this.m_app.aCDecHasBankruptcyChapter11Ulad = t,
                  value);
            }
        }

        public bool aDecHasBankruptcyChapter11Ulad
        {
            get { return this.m_app.aDecHasBankruptcyChapter11Ulad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecHasBankruptcyChapter11Ulad",
                  () => this.m_app.aDecHasBankruptcyChapter11Ulad,
                  t => this.m_app.aDecHasBankruptcyChapter11Ulad = t,
                  value);
            }
        }

        public bool aBDecHasBankruptcyChapter12Ulad
        {
            get { return this.m_app.aBDecHasBankruptcyChapter12Ulad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecHasBankruptcyChapter12Ulad",
                  () => this.m_app.aBDecHasBankruptcyChapter12Ulad,
                  t => this.m_app.aBDecHasBankruptcyChapter12Ulad = t,
                  value);
            }
        }

        public bool aCDecHasBankruptcyChapter12Ulad
        {
            get { return this.m_app.aCDecHasBankruptcyChapter12Ulad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecHasBankruptcyChapter12Ulad",
                  () => this.m_app.aCDecHasBankruptcyChapter12Ulad,
                  t => this.m_app.aCDecHasBankruptcyChapter12Ulad = t,
                  value);
            }
        }

        public bool aDecHasBankruptcyChapter12Ulad
        {
            get { return this.m_app.aDecHasBankruptcyChapter12Ulad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecHasBankruptcyChapter12Ulad",
                  () => this.m_app.aDecHasBankruptcyChapter12Ulad,
                  t => this.m_app.aDecHasBankruptcyChapter12Ulad = t,
                  value);
            }
        }

        public bool aBDecHasBankruptcyChapter13Ulad
        {
            get { return this.m_app.aBDecHasBankruptcyChapter13Ulad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aBDecHasBankruptcyChapter13Ulad",
                  () => this.m_app.aBDecHasBankruptcyChapter13Ulad,
                  t => this.m_app.aBDecHasBankruptcyChapter13Ulad = t,
                  value);
            }
        }

        public bool aCDecHasBankruptcyChapter13Ulad
        {
            get { return this.m_app.aCDecHasBankruptcyChapter13Ulad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aCDecHasBankruptcyChapter13Ulad",
                  () => this.m_app.aCDecHasBankruptcyChapter13Ulad,
                  t => this.m_app.aCDecHasBankruptcyChapter13Ulad = t,
                  value);
            }
        }

        public bool aDecHasBankruptcyChapter13Ulad
        {
            get { return this.m_app.aDecHasBankruptcyChapter13Ulad; }
            set
            {
                this.m_validator.TryToWriteField(
                  "aDecHasBankruptcyChapter13Ulad",
                  () => this.m_app.aDecHasBankruptcyChapter13Ulad,
                  t => this.m_app.aDecHasBankruptcyChapter13Ulad = t,
                  value);
            }
        }

        public string aBDecJudgment
        {
            get { return m_app.aBDecJudgment; }
            set
            {
                m_validator.TryToWriteField("aBDecJudgment", () => m_app.aBDecJudgment, t => m_app.aBDecJudgment = t, value);
            }
        }


        public string aBDecBankrupt
        {
            get { return m_app.aBDecBankrupt; }
            set
            {
                m_validator.TryToWriteField("aBDecBankrupt", () => m_app.aBDecBankrupt, t => m_app.aBDecBankrupt = t, value);
            }
        }


        public string aBDecForeclosure
        {
            get { return m_app.aBDecForeclosure; }
            set
            {
                m_validator.TryToWriteField("aBDecForeclosure", () => m_app.aBDecForeclosure, t => m_app.aBDecForeclosure = t, value);
            }
        }


        public string aBDecLawsuit
        {
            get { return m_app.aBDecLawsuit; }
            set
            {
                m_validator.TryToWriteField("aBDecLawsuit", () => m_app.aBDecLawsuit, t => m_app.aBDecLawsuit = t, value);
            }
        }


        public string aBDecObligated
        {
            get { return m_app.aBDecObligated; }
            set
            {
                m_validator.TryToWriteField("aBDecObligated", () => m_app.aBDecObligated, t => m_app.aBDecObligated = t, value);
            }
        }


        public string aBDecDelinquent
        {
            get { return m_app.aBDecDelinquent; }
            set
            {
                m_validator.TryToWriteField("aBDecDelinquent", () => m_app.aBDecDelinquent, t => m_app.aBDecDelinquent = t, value);
            }
        }


        public string aBDecAlimony
        {
            get { return m_app.aBDecAlimony; }
            set
            {
                m_validator.TryToWriteField("aBDecAlimony", () => m_app.aBDecAlimony, t => m_app.aBDecAlimony = t, value);
            }
        }


        public string aBDecBorrowing
        {
            get { return m_app.aBDecBorrowing; }
            set
            {
                m_validator.TryToWriteField("aBDecBorrowing", () => m_app.aBDecBorrowing, t => m_app.aBDecBorrowing = t, value);
            }
        }


        public string aBDecEndorser
        {
            get { return m_app.aBDecEndorser; }
            set
            {
                m_validator.TryToWriteField("aBDecEndorser", () => m_app.aBDecEndorser, t => m_app.aBDecEndorser = t, value);
            }
        }


        public string aBDecCitizen
        {
            get { return m_app.aBDecCitizen; }
            set
            {
                m_validator.TryToWriteField("aBDecCitizen", () => m_app.aBDecCitizen, t => m_app.aBDecCitizen = t, value);
            }
        }


        public string aBDecResidency
        {
            get { return m_app.aBDecResidency; }
            set
            {
                m_validator.TryToWriteField("aBDecResidency", () => m_app.aBDecResidency, t => m_app.aBDecResidency = t, value);
            }
        }


        public string aBCitizenDescription
        {
            get { return m_app.aBCitizenDescription; }
        }


        public string aBDecResidencyTotalScorecardWarning
        {
            get { return m_app.aBDecResidencyTotalScorecardWarning; }
        }


        public string aBDecResidencyPmlImportWarning
        {
            get { return m_app.aBDecResidencyPmlImportWarning; }
        }


        public string aBDecForeignNational
        {
            get { return m_app.aBDecForeignNational; }
            set
            {
                m_validator.TryToWriteField("aBDecForeignNational", () => m_app.aBDecForeignNational, t => m_app.aBDecForeignNational = t, value);
            }
        }

        public LanguagePrefType aBLanguagePrefType
        {
            get { return m_app.aBLanguagePrefType; }
            set
            {
                m_validator.TryToWriteField("aBLanguagePrefType", () => m_app.aBLanguagePrefType, t => m_app.aBLanguagePrefType = t, value);
            }
        }

        public LanguagePrefType aCLanguagePrefType
        {
            get { return m_app.aCLanguagePrefType; }
            set
            {
                m_validator.TryToWriteField("aCLanguagePrefType", () => m_app.aCLanguagePrefType, t => m_app.aCLanguagePrefType = t, value);
            }
        }

        public LanguagePrefType aLanguagePrefType
        {
            get { return m_app.aLanguagePrefType; }
            set
            {
                m_validator.TryToWriteField("aLanguagePrefType", () => m_app.aLanguagePrefType, t => m_app.aLanguagePrefType = t, value);
            }
        }

        public string aBLanguagePrefTypeOtherDesc
        {
            get { return m_app.aBLanguagePrefTypeOtherDesc; }
            set
            {
                m_validator.TryToWriteField("aBLanguagePrefTypeOtherDesc", () => m_app.aBLanguagePrefTypeOtherDesc, t => m_app.aBLanguagePrefTypeOtherDesc = t, value);
            }
        }

        public string aCLanguagePrefTypeOtherDesc
        {
            get { return m_app.aCLanguagePrefTypeOtherDesc; }
            set
            {
                m_validator.TryToWriteField("aCLanguagePrefTypeOtherDesc", () => m_app.aCLanguagePrefTypeOtherDesc, t => m_app.aCLanguagePrefTypeOtherDesc = t, value);
            }
        }

        public string aLanguagePrefTypeOtherDesc
        {
            get { return m_app.aLanguagePrefTypeOtherDesc; }
            set
            {
                m_validator.TryToWriteField("aLanguagePrefTypeOtherDesc", () => m_app.aLanguagePrefTypeOtherDesc, t => m_app.aLanguagePrefTypeOtherDesc = t, value);
            }
        }

        public bool aBLanguageRefusal
        {
            get { return m_app.aBLanguageRefusal; }
            set
            {
                m_validator.TryToWriteField("aBLanguageRefusal", () => m_app.aBLanguageRefusal, t => m_app.aBLanguageRefusal = t, value);
            }
        }

        public bool aCLanguageRefusal
        {
            get { return m_app.aCLanguageRefusal; }
            set
            {
                m_validator.TryToWriteField("aCLanguageRefusal", () => m_app.aCLanguageRefusal, t => m_app.aCLanguageRefusal = t, value);
            }
        }

        public bool aLanguageRefusal
        {
            get { return m_app.aLanguageRefusal; }
            set
            {
                m_validator.TryToWriteField("aLanguageRefusal", () => m_app.aLanguageRefusal, t => m_app.aLanguageRefusal = t, value);
            }
        }

        public E_TriState aBDomesticRelationshipTri
        {
            get { return m_app.aBDomesticRelationshipTri; }
            set
            {
                m_validator.TryToWriteField("aBDomesticRelationshipTri", () => m_app.aBDomesticRelationshipTri, t => m_app.aBDomesticRelationshipTri = t, value);
            }
        }

        public E_TriState aCDomesticRelationshipTri
        {
            get { return m_app.aCDomesticRelationshipTri; }
            set
            {
                m_validator.TryToWriteField("aCDomesticRelationshipTri", () => m_app.aCDomesticRelationshipTri, t => m_app.aCDomesticRelationshipTri = t, value);
            }
        }

        public E_TriState aDomesticRelationshipTri
        {
            get { return m_app.aDomesticRelationshipTri; }
            set
            {
                m_validator.TryToWriteField("aDomesticRelationshipTri", () => m_app.aDomesticRelationshipTri, t => m_app.aDomesticRelationshipTri = t, value);
            }
        }

        public DomesticRelationshipType aBDomesticRelationshipType
        {
            get { return m_app.aBDomesticRelationshipType; }
            set
            {
                m_validator.TryToWriteField("aBDomesticRelationshipType", () => m_app.aBDomesticRelationshipType, t => m_app.aBDomesticRelationshipType = t, value);
            }
        }

        public DomesticRelationshipType aCDomesticRelationshipType
        {
            get { return m_app.aCDomesticRelationshipType; }
            set
            {
                m_validator.TryToWriteField("aCDomesticRelationshipType", () => m_app.aCDomesticRelationshipType, t => m_app.aCDomesticRelationshipType = t, value);
            }
        }

        public DomesticRelationshipType aDomesticRelationshipType
        {
            get { return m_app.aDomesticRelationshipType; }
            set
            {
                m_validator.TryToWriteField("aDomesticRelationshipType", () => m_app.aDomesticRelationshipType, t => m_app.aDomesticRelationshipType = t, value);
            }
        }

        public string aBDomesticRelationshipTypeOtherDescription
        {
            get { return m_app.aBDomesticRelationshipTypeOtherDescription; }
            set
            {
                m_validator.TryToWriteField("aBDomesticRelationshipTypeOtherDescription", () => m_app.aBDomesticRelationshipTypeOtherDescription, t => m_app.aBDomesticRelationshipTypeOtherDescription = t, value);
            }
        }

        public string aCDomesticRelationshipTypeOtherDescription
        {
            get { return m_app.aCDomesticRelationshipTypeOtherDescription; }
            set
            {
                m_validator.TryToWriteField("aCDomesticRelationshipTypeOtherDescription", () => m_app.aCDomesticRelationshipTypeOtherDescription, t => m_app.aCDomesticRelationshipTypeOtherDescription = t, value);
            }
        }

        public string aDomesticRelationshipTypeOtherDescription
        {
            get { return m_app.aDomesticRelationshipTypeOtherDescription; }
            set
            {
                m_validator.TryToWriteField("aDomesticRelationshipTypeOtherDescription", () => m_app.aDomesticRelationshipTypeOtherDescription, t => m_app.aDomesticRelationshipTypeOtherDescription = t, value);
            }
        }

        public string aBDomesticRelationshipStateCode
        {
            get { return m_app.aBDomesticRelationshipStateCode; }
            set
            {
                m_validator.TryToWriteField("aBDomesticRelationshipStateCode", () => m_app.aBDomesticRelationshipStateCode, t => m_app.aBDomesticRelationshipStateCode = t, value);
            }
        }

        public string aCDomesticRelationshipStateCode
        {
            get { return m_app.aCDomesticRelationshipStateCode; }
            set
            {
                m_validator.TryToWriteField("aCDomesticRelationshipStateCode", () => m_app.aCDomesticRelationshipStateCode, t => m_app.aCDomesticRelationshipStateCode = t, value);
            }
        }

        public string aDomesticRelationshipStateCode
        {
            get { return m_app.aDomesticRelationshipStateCode; }
            set
            {
                m_validator.TryToWriteField("aDomesticRelationshipStateCode", () => m_app.aDomesticRelationshipStateCode, t => m_app.aDomesticRelationshipStateCode = t, value);
            }
        }

        public bool aBActiveDuty
        {
            get { return m_app.aBActiveDuty; }
            set
            {
                m_validator.TryToWriteField("aBActiveDuty", () => m_app.aBActiveDuty, t => m_app.aBActiveDuty = t, value);
            }
        }

        public bool aCActiveDuty
        {
            get { return m_app.aCActiveDuty; }
            set
            {
                m_validator.TryToWriteField("aCActiveDuty", () => m_app.aCActiveDuty, t => m_app.aCActiveDuty = t, value);
            }
        }

        public bool aActiveDuty
        {
            get { return m_app.aActiveDuty; }
            set
            {
                m_validator.TryToWriteField("aActiveDuty", () => m_app.aActiveDuty, t => m_app.aActiveDuty = t, value);
            }
        }

        public string aBActiveDutyExpirationDate_rep
        {
            get { return m_app.aBActiveDutyExpirationDate_rep; }
            set
            {
                m_validator.TryToWriteField("aBActiveDutyExpirationDate_rep", () => m_app.aBActiveDutyExpirationDate_rep, t => m_app.aBActiveDutyExpirationDate_rep = t, value);
            }
        }

        public string aCActiveDutyExpirationDate_rep
        {
            get { return m_app.aCActiveDutyExpirationDate_rep; }
            set
            {
                m_validator.TryToWriteField("aCActiveDutyExpirationDate_rep", () => m_app.aCActiveDutyExpirationDate_rep, t => m_app.aCActiveDutyExpirationDate_rep = t, value);
            }
        }

        public string aActiveDutyExpirationDate_rep
        {
            get { return m_app.aActiveDutyExpirationDate_rep; }
            set
            {
                m_validator.TryToWriteField("aActiveDutyExpirationDate_rep", () => m_app.aActiveDutyExpirationDate_rep, t => m_app.aActiveDutyExpirationDate_rep = t, value);
            }
        }

        public bool aBRetiredDischargedOrSeparated
        {
            get { return m_app.aBRetiredDischargedOrSeparated; }
            set
            {
                m_validator.TryToWriteField("aBRetiredDischargedOrSeparated", () => m_app.aBRetiredDischargedOrSeparated, t => m_app.aBRetiredDischargedOrSeparated = t, value);
            }
        }

        public bool aCRetiredDischargedOrSeparated
        {
            get { return m_app.aCRetiredDischargedOrSeparated; }
            set
            {
                m_validator.TryToWriteField("aCRetiredDischargedOrSeparated", () => m_app.aCRetiredDischargedOrSeparated, t => m_app.aCRetiredDischargedOrSeparated = t, value);
            }
        }

        public bool aRetiredDischargedOrSeparated
        {
            get { return m_app.aRetiredDischargedOrSeparated; }
            set
            {
                m_validator.TryToWriteField("aRetiredDischargedOrSeparated", () => m_app.aRetiredDischargedOrSeparated, t => m_app.aRetiredDischargedOrSeparated = t, value);
            }
        }

        public bool aBReserveNationalGuardNeverActivated
        {
            get { return m_app.aBReserveNationalGuardNeverActivated; }
            set
            {
                m_validator.TryToWriteField("aBReserveNationalGuardNeverActivated", () => m_app.aBReserveNationalGuardNeverActivated, t => m_app.aBReserveNationalGuardNeverActivated = t, value);
            }
        }

        public bool aCReserveNationalGuardNeverActivated
        {
            get { return m_app.aCReserveNationalGuardNeverActivated; }
            set
            {
                m_validator.TryToWriteField("aCReserveNationalGuardNeverActivated", () => m_app.aCReserveNationalGuardNeverActivated, t => m_app.aCReserveNationalGuardNeverActivated = t, value);
            }
        }

        public bool aReserveNationalGuardNeverActivated
        {
            get { return m_app.aReserveNationalGuardNeverActivated; }
            set
            {
                m_validator.TryToWriteField("aReserveNationalGuardNeverActivated", () => m_app.aReserveNationalGuardNeverActivated, t => m_app.aReserveNationalGuardNeverActivated = t, value);
            }
        }

        public bool aBIntendsToOccupyWithoutOwning
        {
            get { return m_app.aBIntendsToOccupyWithoutOwning; }
        }


        public bool aCIntendsToOccupyWithoutOwning
        {
            get { return m_app.aCIntendsToOccupyWithoutOwning; }
        }


        public string aBDecOcc
        {
            get { return m_app.aBDecOcc; }
            set
            {
                m_validator.TryToWriteField("aBDecOcc", () => m_app.aBDecOcc, t => m_app.aBDecOcc = t, value);
            }
        }


        public E_aH4HNonOccInterestT aBH4HNonOccInterestT
        {
            get { return m_app.aBH4HNonOccInterestT; }
            set
            {
                m_validator.TryToWriteField("aBH4HNonOccInterestT", () => m_app.aBH4HNonOccInterestT, t => m_app.aBH4HNonOccInterestT = t, value);
            }
        }


        public E_aH4HNonOccInterestT aCH4HNonOccInterestT
        {
            get { return m_app.aCH4HNonOccInterestT; }
            set
            {
                m_validator.TryToWriteField("aCH4HNonOccInterestT", () => m_app.aCH4HNonOccInterestT, t => m_app.aCH4HNonOccInterestT = t, value);
            }
        }


        public E_aH4HNonOccInterestT aH4HNonOccInterestT
        {
            get { return m_app.aH4HNonOccInterestT; }
            set
            {
                m_validator.TryToWriteField("aH4HNonOccInterestT", () => m_app.aH4HNonOccInterestT, t => m_app.aH4HNonOccInterestT = t, value);
            }
        }


        public string aBDecOccTotalScorecardWarning
        {
            get { return m_app.aBDecOccTotalScorecardWarning; }
        }


        public string aBDecOccPmlImportWarning
        {
            get { return m_app.aBDecOccPmlImportWarning; }
        }


        public string aBDecPastOwnership
        {
            get { return m_app.aBDecPastOwnership; }
            set
            {
                m_validator.TryToWriteField("aBDecPastOwnership", () => m_app.aBDecPastOwnership, t => m_app.aBDecPastOwnership = t, value);
            }
        }


        public E_aBDecPastOwnedPropT aBDecPastOwnedPropT
        {
            get { return m_app.aBDecPastOwnedPropT; }
            set
            {
                m_validator.TryToWriteField("aBDecPastOwnedPropT", () => m_app.aBDecPastOwnedPropT, t => m_app.aBDecPastOwnedPropT = t, value);
            }
        }


        public E_aCDecPastOwnedPropT aCDecPastOwnedPropT
        {
            get { return m_app.aCDecPastOwnedPropT; }
            set
            {
                m_validator.TryToWriteField("aCDecPastOwnedPropT", () => m_app.aCDecPastOwnedPropT, t => m_app.aCDecPastOwnedPropT = t, value);
            }
        }


        public E_aBDecPastOwnedPropTitleT aBDecPastOwnedPropTitleT
        {
            get { return m_app.aBDecPastOwnedPropTitleT; }
            set
            {
                m_validator.TryToWriteField("aBDecPastOwnedPropTitleT", () => m_app.aBDecPastOwnedPropTitleT, t => m_app.aBDecPastOwnedPropTitleT = t, value);
            }
        }


        public E_aCDecPastOwnedPropTitleT aCDecPastOwnedPropTitleT
        {
            get { return m_app.aCDecPastOwnedPropTitleT; }
            set
            {
                m_validator.TryToWriteField("aCDecPastOwnedPropTitleT", () => m_app.aCDecPastOwnedPropTitleT, t => m_app.aCDecPastOwnedPropTitleT = t, value);
            }
        }


        public string aCDecJudgment
        {
            get { return m_app.aCDecJudgment; }
            set
            {
                m_validator.TryToWriteField("aCDecJudgment", () => m_app.aCDecJudgment, t => m_app.aCDecJudgment = t, value);
            }
        }


        public string aCDecBankrupt
        {
            get { return m_app.aCDecBankrupt; }
            set
            {
                m_validator.TryToWriteField("aCDecBankrupt", () => m_app.aCDecBankrupt, t => m_app.aCDecBankrupt = t, value);
            }
        }


        public string aCDecForeclosure
        {
            get { return m_app.aCDecForeclosure; }
            set
            {
                m_validator.TryToWriteField("aCDecForeclosure", () => m_app.aCDecForeclosure, t => m_app.aCDecForeclosure = t, value);
            }
        }


        public string aCDecLawsuit
        {
            get { return m_app.aCDecLawsuit; }
            set
            {
                m_validator.TryToWriteField("aCDecLawsuit", () => m_app.aCDecLawsuit, t => m_app.aCDecLawsuit = t, value);
            }
        }


        public string aCDecObligated
        {
            get { return m_app.aCDecObligated; }
            set
            {
                m_validator.TryToWriteField("aCDecObligated", () => m_app.aCDecObligated, t => m_app.aCDecObligated = t, value);
            }
        }


        public string aCDecDelinquent
        {
            get { return m_app.aCDecDelinquent; }
            set
            {
                m_validator.TryToWriteField("aCDecDelinquent", () => m_app.aCDecDelinquent, t => m_app.aCDecDelinquent = t, value);
            }
        }


        public string aCDecAlimony
        {
            get { return m_app.aCDecAlimony; }
            set
            {
                m_validator.TryToWriteField("aCDecAlimony", () => m_app.aCDecAlimony, t => m_app.aCDecAlimony = t, value);
            }
        }


        public string aCDecBorrowing
        {
            get { return m_app.aCDecBorrowing; }
            set
            {
                m_validator.TryToWriteField("aCDecBorrowing", () => m_app.aCDecBorrowing, t => m_app.aCDecBorrowing = t, value);
            }
        }


        public string aCDecEndorser
        {
            get { return m_app.aCDecEndorser; }
            set
            {
                m_validator.TryToWriteField("aCDecEndorser", () => m_app.aCDecEndorser, t => m_app.aCDecEndorser = t, value);
            }
        }


        public string aCDecCitizen
        {
            get { return m_app.aCDecCitizen; }
            set
            {
                m_validator.TryToWriteField("aCDecCitizen", () => m_app.aCDecCitizen, t => m_app.aCDecCitizen = t, value);
            }
        }


        public bool aBTotalScoreIsCAIVRSAuthClear
        {
            get { return m_app.aBTotalScoreIsCAIVRSAuthClear; }
            set
            {
                m_validator.TryToWriteField("aBTotalScoreIsCAIVRSAuthClear", () => m_app.aBTotalScoreIsCAIVRSAuthClear, t => m_app.aBTotalScoreIsCAIVRSAuthClear = t, value);
            }
        }


        public bool aCTotalScoreIsCAIVRSAuthClear
        {
            get { return m_app.aCTotalScoreIsCAIVRSAuthClear; }
            set
            {
                m_validator.TryToWriteField("aCTotalScoreIsCAIVRSAuthClear", () => m_app.aCTotalScoreIsCAIVRSAuthClear, t => m_app.aCTotalScoreIsCAIVRSAuthClear = t, value);
            }
        }


        public bool aHasClearCaivrsAuthCode
        {
            get { return m_app.aHasClearCaivrsAuthCode; }
        }


        public bool aBTotalScoreIsFthb
        {
            get { return m_app.aBTotalScoreIsFthb; }
            set
            {
                m_validator.TryToWriteField("aBTotalScoreIsFthb", () => m_app.aBTotalScoreIsFthb, t => m_app.aBTotalScoreIsFthb = t, value);
            }
        }


        public E_aTotalScoreFhtbCounselingT aBTotalScoreFhtbCounselingT
        {
            get { return m_app.aBTotalScoreFhtbCounselingT; }
            set
            {
                m_validator.TryToWriteField("aBTotalScoreFhtbCounselingT", () => m_app.aBTotalScoreFhtbCounselingT, t => m_app.aBTotalScoreFhtbCounselingT = t, value);
            }
        }


        public bool aCTotalScoreIsFthb
        {
            get { return m_app.aCTotalScoreIsFthb; }
            set
            {
                m_validator.TryToWriteField("aCTotalScoreIsFthb", () => m_app.aCTotalScoreIsFthb, t => m_app.aCTotalScoreIsFthb = t, value);
            }
        }


        public E_aTotalScoreFhtbCounselingT aCTotalScoreFhtbCounselingT
        {
            get { return m_app.aCTotalScoreFhtbCounselingT; }
            set
            {
                m_validator.TryToWriteField("aCTotalScoreFhtbCounselingT", () => m_app.aCTotalScoreFhtbCounselingT, t => m_app.aCTotalScoreFhtbCounselingT = t, value);
            }
        }


        public bool aIsHudApprovedFTHBCounselingComplete
        {
            get { return m_app.aIsHudApprovedFTHBCounselingComplete; }
        }


        public E_sTotalScoreLdpOrGsaExclusionT aTotalScoreLdpOrGsaExclusionT
        {
            get { return m_app.aTotalScoreLdpOrGsaExclusionT; }
        }


        public E_aProdCitizenT aCitizenshipType
        {
            get { return m_app.aCitizenshipType; }
        }


        public int aProdRepresentativeScore
        {
            get { return m_app.aProdRepresentativeScore; }
        }

        public int ScoreCBS(string parameters, bool verifyParams = false)
        {
            return m_app.ScoreCBS(parameters, verifyParams);
        }

        public int aScoreCBS
        {
            get { return m_app.aScoreCBS; }
        }

        public string aProdRepresentativeScore_rep
        {
            get { return m_app.aProdRepresentativeScore_rep; }
        }


        public E_aProdCitizenT aProdCCitizenT
        {
            get { return m_app.aProdCCitizenT; }
            set
            {
                m_validator.TryToWriteField("aProdCCitizenT", () => m_app.aProdCCitizenT, t => m_app.aProdCCitizenT = t, value);
            }
        }


        public E_aProdCitizenT aProdBCitizenT
        {
            get { return m_app.aProdBCitizenT; }
            set
            {
                m_validator.TryToWriteField("aProdBCitizenT", () => m_app.aProdBCitizenT, t => m_app.aProdBCitizenT = t, value);
            }
        }

        public E_aVServiceT aBVServiceT
        {
            get { return m_app.aBVServiceT; }
            set
            {
                m_validator.TryToWriteField("aBVServiceT", () => m_app.aBVServiceT, t => m_app.aBVServiceT = t, value);
            }
        }

        public E_aVServiceT aCVServiceT
        {
            get { return m_app.aCVServiceT; }
            set
            {
                m_validator.TryToWriteField("aCVServiceT", () => m_app.aCVServiceT, t => m_app.aCVServiceT = t, value);
            }
        }

        public bool aBIsSurvivingSpouseOfVeteran
        {
            get
            {
                return m_app.aBIsSurvivingSpouseOfVeteran;
            }

            set
            {
                m_validator.TryToWriteField("aBIsSurvivingSpouseOfVeteran", () => m_app.aBIsSurvivingSpouseOfVeteran, t => m_app.aBIsSurvivingSpouseOfVeteran = t, value);
            }
        }

        public bool aCIsSurvivingSpouseOfVeteran
        {
            get
            {
                return m_app.aCIsSurvivingSpouseOfVeteran;
            }

            set
            {
                m_validator.TryToWriteField("aCIsSurvivingSpouseOfVeteran", () => m_app.aCIsSurvivingSpouseOfVeteran, t => m_app.aCIsSurvivingSpouseOfVeteran = t, value);
            }
        }

        public E_aVEnt aBVEnt
        {
            get { return m_app.aBVEnt; }
            set
            {
                m_validator.TryToWriteField("aBVEnt", () => m_app.aBVEnt, t => m_app.aBVEnt = t, value);
            }
        }


        public E_aVEnt aCVEnt
        {
            get { return m_app.aCVEnt; }
            set
            {
                m_validator.TryToWriteField("aCVEnt", () => m_app.aCVEnt, t => m_app.aCVEnt = t, value);
            }
        }


        public string aCDecResidency
        {
            get { return m_app.aCDecResidency; }
            set
            {
                m_validator.TryToWriteField("aCDecResidency", () => m_app.aCDecResidency, t => m_app.aCDecResidency = t, value);
            }
        }


        public string aCCitizenDescription
        {
            get { return m_app.aCCitizenDescription; }
        }


        public string aCDecResidencyTotalScorecardWarning
        {
            get { return m_app.aCDecResidencyTotalScorecardWarning; }
        }


        public string aCDecResidencyPmlImportWarning
        {
            get { return m_app.aCDecResidencyPmlImportWarning; }
        }


        public string aCDecOcc
        {
            get { return m_app.aCDecOcc; }
            set
            {
                m_validator.TryToWriteField("aCDecOcc", () => m_app.aCDecOcc, t => m_app.aCDecOcc = t, value);
            }
        }


        public string aCDecOccTotalScorecardWarning
        {
            get { return m_app.aCDecOccTotalScorecardWarning; }
        }


        public string aCDecOccPmlImportWarning
        {
            get { return m_app.aCDecOccPmlImportWarning; }
        }


        public string aCDecPastOwnership
        {
            get { return m_app.aCDecPastOwnership; }
            set
            {
                m_validator.TryToWriteField("aCDecPastOwnership", () => m_app.aCDecPastOwnership, t => m_app.aCDecPastOwnership = t, value);
            }
        }

        public string aBDecJudgmentExplanation
        {
            get { return m_app.aBDecJudgmentExplanation; }
            set
            {
                m_validator.TryToWriteField("aBDecJudgmentExplanation", () => m_app.aBDecJudgmentExplanation, t => m_app.aBDecJudgmentExplanation = t, value);
            }
        }

        public string aBDecBankruptExplanation
        {
            get { return m_app.aBDecBankruptExplanation; }
            set
            {
                m_validator.TryToWriteField("aBDecBankruptExplanation", () => m_app.aBDecBankruptExplanation, t => m_app.aBDecBankruptExplanation = t, value);
            }
        }

        public string aBDecForeclosureExplanation
        {
            get { return m_app.aBDecForeclosureExplanation; }
            set
            {
                m_validator.TryToWriteField("aBDecForeclosureExplanation", () => m_app.aBDecForeclosureExplanation, t => m_app.aBDecForeclosureExplanation = t, value);
            }
        }

        public string aBDecLawsuitExplanation
        {
            get { return m_app.aBDecLawsuitExplanation; }
            set
            {
                m_validator.TryToWriteField("aBDecLawsuitExplanation", () => m_app.aBDecLawsuitExplanation, t => m_app.aBDecLawsuitExplanation = t, value);
            }
        }

        public string aBDecObligatedExplanation
        {
            get { return m_app.aBDecObligatedExplanation; }
            set
            {
                m_validator.TryToWriteField("aBDecObligatedExplanation", () => m_app.aBDecObligatedExplanation, t => m_app.aBDecObligatedExplanation = t, value);
            }
        }

        public string aBDecDelinquentExplanation
        {
            get { return m_app.aBDecDelinquentExplanation; }
            set
            {
                m_validator.TryToWriteField("aBDecDelinquentExplanation", () => m_app.aBDecDelinquentExplanation, t => m_app.aBDecDelinquentExplanation = t, value);
            }
        }

        public string aBDecAlimonyExplanation
        {
            get { return m_app.aBDecAlimonyExplanation; }
            set
            {
                m_validator.TryToWriteField("aBDecAlimonyExplanation", () => m_app.aBDecAlimonyExplanation, t => m_app.aBDecAlimonyExplanation = t, value);
            }
        }

        public string aBDecBorrowingExplanation
        {
            get { return m_app.aBDecBorrowingExplanation; }
            set
            {
                m_validator.TryToWriteField("aBDecBorrowingExplanation", () => m_app.aBDecBorrowingExplanation, t => m_app.aBDecBorrowingExplanation = t, value);
            }
        }

        public string aBDecEndorserExplanation
        {
            get { return m_app.aBDecEndorserExplanation; }
            set
            {
                m_validator.TryToWriteField("aBDecEndorserExplanation", () => m_app.aBDecEndorserExplanation, t => m_app.aBDecEndorserExplanation = t, value);
            }
        }


        public string aCDecJudgmentExplanation
        {
            get { return m_app.aCDecJudgmentExplanation; }
            set
            {
                m_validator.TryToWriteField("aCDecJudgmentExplanation", () => m_app.aCDecJudgmentExplanation, t => m_app.aCDecJudgmentExplanation = t, value);
            }
        }

        public string aCDecBankruptExplanation
        {
            get { return m_app.aCDecBankruptExplanation; }
            set
            {
                m_validator.TryToWriteField("aCDecBankruptExplanation", () => m_app.aCDecBankruptExplanation, t => m_app.aCDecBankruptExplanation = t, value);
            }
        }

        public string aCDecForeclosureExplanation
        {
            get { return m_app.aCDecForeclosureExplanation; }
            set
            {
                m_validator.TryToWriteField("aCDecForeclosureExplanation", () => m_app.aCDecForeclosureExplanation, t => m_app.aCDecForeclosureExplanation = t, value);
            }
        }

        public string aCDecLawsuitExplanation
        {
            get { return m_app.aCDecLawsuitExplanation; }
            set
            {
                m_validator.TryToWriteField("aCDecLawsuitExplanation", () => m_app.aCDecLawsuitExplanation, t => m_app.aCDecLawsuitExplanation = t, value);
            }
        }

        public string aCDecObligatedExplanation
        {
            get { return m_app.aCDecObligatedExplanation; }
            set
            {
                m_validator.TryToWriteField("aCDecObligatedExplanation", () => m_app.aCDecObligatedExplanation, t => m_app.aCDecObligatedExplanation = t, value);
            }
        }

        public string aCDecDelinquentExplanation
        {
            get { return m_app.aCDecDelinquentExplanation; }
            set
            {
                m_validator.TryToWriteField("aCDecDelinquentExplanation", () => m_app.aCDecDelinquentExplanation, t => m_app.aCDecDelinquentExplanation = t, value);
            }
        }

        public string aCDecAlimonyExplanation
        {
            get { return m_app.aCDecAlimonyExplanation; }
            set
            {
                m_validator.TryToWriteField("aCDecAlimonyExplanation", () => m_app.aCDecAlimonyExplanation, t => m_app.aCDecAlimonyExplanation = t, value);
            }
        }

        public string aCDecBorrowingExplanation
        {
            get { return m_app.aCDecBorrowingExplanation; }
            set
            {
                m_validator.TryToWriteField("aCDecBorrowingExplanation", () => m_app.aCDecBorrowingExplanation, t => m_app.aCDecBorrowingExplanation = t, value);
            }
        }

        public string aCDecEndorserExplanation
        {
            get { return m_app.aCDecEndorserExplanation; }
            set
            {
                m_validator.TryToWriteField("aCDecEndorserExplanation", () => m_app.aCDecEndorserExplanation, t => m_app.aCDecEndorserExplanation = t, value);
            }
        }
        public bool aBNoFurnish
        {
            get { return m_app.aBNoFurnish; }
            set
            {
                m_validator.TryToWriteField("aBNoFurnish", () => m_app.aBNoFurnish, t => m_app.aBNoFurnish = t, value);
            }
        }

        public bool aBNoFurnishLckd
        {
            get
            {
                return this.m_app.aBNoFurnishLckd;
            }

            set
            {
                this.m_validator.TryToWriteField("aBNoFurnishLckd", () => m_app.aBNoFurnishLckd, t => m_app.aBNoFurnishLckd = t, value);
            }
        }

        public bool aCNoFurnish
        {
            get { return m_app.aCNoFurnish; }
            set
            {
                m_validator.TryToWriteField("aCNoFurnish", () => m_app.aCNoFurnish, t => m_app.aCNoFurnish = t, value);
            }
        }

        public bool aCNoFurnishLckd
        {
            get
            {
                return this.m_app.aCNoFurnishLckd;
            }

            set
            {
                this.m_validator.TryToWriteField("aCNoFurnishLckd", () => m_app.aCNoFurnishLckd, t => m_app.aCNoFurnishLckd = t, value);
            }
        }

        public E_GenderT aBGender
        {
            get { return m_app.aBGender; }
            set
            {
                m_validator.TryToWriteField("aBGender", () => m_app.aBGender, t => m_app.aBGender = t, value);
            }
        }

        public E_GenderT aBGenderFallback
        {
            get
            {
                return this.m_app.aBGenderFallback;
            }

            set
            {
                this.m_validator.TryToWriteField("aBGenderFallback", () => this.m_app.aBGenderFallback, t => m_app.aBGenderFallback = t, value);
            }
        }
        public E_GenderT aCGender
        {
            get { return m_app.aCGender; }
            set
            {
                m_validator.TryToWriteField("aCGender", () => m_app.aCGender, t => m_app.aCGender = t, value);
            }
        }

        public E_GenderT aCGenderFallback
        {
            get
            {
                return this.m_app.aCGenderFallback;
            }

            set
            {
                this.m_validator.TryToWriteField("aCGenderFallback", () => this.m_app.aCGenderFallback, t => this.m_app.aCGenderFallback = t, value);
            }
        }

        public E_aBRaceT aBRaceT
        {
            get { return m_app.aBRaceT; }
            set
            {
                m_validator.TryToWriteField("aBRaceT", () => m_app.aBRaceT, t => m_app.aBRaceT = t, value);
            }
        }


        public E_aCRaceT aCRaceT
        {
            get { return m_app.aCRaceT; }
            set
            {
                m_validator.TryToWriteField("aCRaceT", () => m_app.aCRaceT, t => m_app.aCRaceT = t, value);
            }
        }


        public string aBORaceDesc
        {
            get { return m_app.aBORaceDesc; }
            set
            {
                m_validator.TryToWriteField("aBORaceDesc", () => m_app.aBORaceDesc, t => m_app.aBORaceDesc = t, value);
            }
        }


        public string aCORaceDesc
        {
            get { return m_app.aCORaceDesc; }
            set
            {
                m_validator.TryToWriteField("aCORaceDesc", () => m_app.aCORaceDesc, t => m_app.aCORaceDesc = t, value);
            }
        }


        public E_aIntrvwrMethodT aIntrvwrMethodT
        {
            get { return m_app.aIntrvwrMethodT; }
            set
            {
                m_validator.TryToWriteField("aIntrvwrMethodT", () => m_app.aIntrvwrMethodT, t => m_app.aIntrvwrMethodT = t, value);
            }
        }

        public bool aIntrvwrMethodTLckd
        {
            get
            {
                return this.m_app.aIntrvwrMethodTLckd;
            }

            set
            {
                this.m_validator.TryToWriteField("aIntrvwrMethodTLckd", () => this.m_app.aIntrvwrMethodTLckd, t => this.m_app.aIntrvwrMethodTLckd = t, value);
            }
        }

        public E_aIntrvwrMethodT aInterviewMethodT
        {
            get
            {
                return m_app.aInterviewMethodT;
            }

            set
            {
                this.m_validator.TryToWriteField("aInterviewMethodT", () => this.m_app.aInterviewMethodT, t => this.m_app.aInterviewMethodT = t, value);
            }
        }

        public E_aIntrvwrMethodT aBInterviewMethodT
        {
            get
            {
                return m_app.aBInterviewMethodT;
            }

            set
            {
                this.m_validator.TryToWriteField("aBInterviewMethodT", () => this.m_app.aBInterviewMethodT, t => this.m_app.aBInterviewMethodT = t, value);
            }
        }

        public E_aIntrvwrMethodT aCInterviewMethodT
        {
            get
            {
                return this.m_app.aCInterviewMethodT;
            }

            set
            {
                this.m_validator.TryToWriteField("aCInterviewMethodT", () => this.m_app.aCInterviewMethodT, t => this.m_app.aCInterviewMethodT = t, value);
            }
        }

        public E_TriState aEthnicityCollectedByObservationOrSurname
        {
            get
            {
                return this.m_app.aEthnicityCollectedByObservationOrSurname;
            }
            set
            {
                this.m_validator.TryToWriteField("aEthnicityCollectedByObservationOrSurname", () => this.m_app.aEthnicityCollectedByObservationOrSurname, t => this.m_app.aEthnicityCollectedByObservationOrSurname = t, value);
            }
        }

        public E_TriState aBEthnicityCollectedByObservationOrSurname
        {
            get
            {
                return this.m_app.aBEthnicityCollectedByObservationOrSurname;
            }
            set
            {
                this.m_validator.TryToWriteField("aBEthnicityCollectedByObservationOrSurname", () => this.m_app.aBEthnicityCollectedByObservationOrSurname, t => this.m_app.aBEthnicityCollectedByObservationOrSurname = t, value);
            }
        }

        public E_TriState aCEthnicityCollectedByObservationOrSurname
        {
            get
            {
                return this.m_app.aCEthnicityCollectedByObservationOrSurname;
            }
            set
            {
                this.m_validator.TryToWriteField("aCEthnicityCollectedByObservationOrSurname", () => this.m_app.aCEthnicityCollectedByObservationOrSurname, t => this.m_app.aCEthnicityCollectedByObservationOrSurname = t, value);
            }
        }

        public E_TriState aSexCollectedByObservationOrSurname
        {
            get
            {
                return this.m_app.aSexCollectedByObservationOrSurname;
            }
            set
            {
                this.m_validator.TryToWriteField("aSexCollectedByObservationOrSurname", () => this.m_app.aSexCollectedByObservationOrSurname, t => this.m_app.aSexCollectedByObservationOrSurname = t, value);
            }
        }

        public E_TriState aBSexCollectedByObservationOrSurname
        {
            get
            {
                return this.m_app.aBSexCollectedByObservationOrSurname;
            }
            set
            {
                this.m_validator.TryToWriteField("aBSexCollectedByObservationOrSurname", () => this.m_app.aBSexCollectedByObservationOrSurname, t => this.m_app.aBSexCollectedByObservationOrSurname = t, value);
            }
        }

        public E_TriState aCSexCollectedByObservationOrSurname
        {
            get
            {
                return this.m_app.aCSexCollectedByObservationOrSurname;
            }
            set
            {
                this.m_validator.TryToWriteField("aCSexCollectedByObservationOrSurname", () => this.m_app.aCSexCollectedByObservationOrSurname, t => this.m_app.aCSexCollectedByObservationOrSurname = t, value);
            }
        }

        public E_TriState aRaceCollectedByObservationOrSurname
        {
            get
            {
                return this.m_app.aRaceCollectedByObservationOrSurname;
            }
            set
            {
                this.m_validator.TryToWriteField("aRaceCollectedByObservationOrSurname", () => this.m_app.aRaceCollectedByObservationOrSurname, t => this.m_app.aRaceCollectedByObservationOrSurname = t, value);
            }
        }

        public E_TriState aBRaceCollectedByObservationOrSurname
        {
            get
            {
                return this.m_app.aBRaceCollectedByObservationOrSurname;
            }
            set
            {
                this.m_validator.TryToWriteField("aBRaceCollectedByObservationOrSurname", () => this.m_app.aBRaceCollectedByObservationOrSurname, t => this.m_app.aBRaceCollectedByObservationOrSurname = t, value);
            }
        }

        public E_TriState aCRaceCollectedByObservationOrSurname
        {
            get
            {
                return this.m_app.aCRaceCollectedByObservationOrSurname;
            }
            set
            {
                this.m_validator.TryToWriteField("aCRaceCollectedByObservationOrSurname", () => this.m_app.aCRaceCollectedByObservationOrSurname, t => this.m_app.aCRaceCollectedByObservationOrSurname = t, value);
            }
        }

        public DeclarationExplanationDataContainer ConstructDeclarationExplanationData()
        {
            return this.m_app.ConstructDeclarationExplanationData();
        }

        public HmdaRaceEthnicityDataContainer ConstructHmdaRaceEthnicityData()
        {
            return this.m_app.ConstructHmdaRaceEthnicityData();
        }

        public string a1003ContEditSheet
        {
            get { return m_app.a1003ContEditSheet; }
            set
            {
                m_validator.TryToWriteField("a1003ContEditSheet", () => m_app.a1003ContEditSheet, t => m_app.a1003ContEditSheet = t, value);
            }
        }


        public bool aAsstLiaCompletedNotJointly
        {
            get { return m_app.aAsstLiaCompletedNotJointly; }
            set
            {
                m_validator.TryToWriteField("aAsstLiaCompletedNotJointly", () => m_app.aAsstLiaCompletedNotJointly, t => m_app.aAsstLiaCompletedNotJointly = t, value);
            }
        }


        public CDateTime a1003SignD
        {
            get { return m_app.a1003SignD; }
        }


        public string a1003SignD_rep
        {
            get { return m_app.a1003SignD_rep; }
            set
            {
                m_validator.TryToWriteField("a1003SignD_rep", () => m_app.a1003SignD_rep, t => m_app.a1003SignD_rep = t, value);
            }
        }


        public int aBExperianScore
        {
            get { return m_app.aBExperianScore; }
            set
            {
                m_validator.TryToWriteField("aBExperianScore", () => m_app.aBExperianScore, t => m_app.aBExperianScore = t, value);
            }
        }


        public string aBExperianScore_rep
        {
            get { return m_app.aBExperianScore_rep; }
            set
            {
                m_validator.TryToWriteField("aBExperianScore_rep", () => m_app.aBExperianScore_rep, t => m_app.aBExperianScore_rep = t, value);
            }
        }


        public int aCExperianScore
        {
            get { return m_app.aCExperianScore; }
            set
            {
                m_validator.TryToWriteField("aCExperianScore", () => m_app.aCExperianScore, t => m_app.aCExperianScore = t, value);
            }
        }


        public string aCExperianScore_rep
        {
            get { return m_app.aCExperianScore_rep; }
            set
            {
                m_validator.TryToWriteField("aCExperianScore_rep", () => m_app.aCExperianScore_rep, t => m_app.aCExperianScore_rep = t, value);
            }
        }


        public int aBTransUnionScore
        {
            get { return m_app.aBTransUnionScore; }
            set
            {
                m_validator.TryToWriteField("aBTransUnionScore", () => m_app.aBTransUnionScore, t => m_app.aBTransUnionScore = t, value);
            }
        }


        public string aBTransUnionScore_rep
        {
            get { return m_app.aBTransUnionScore_rep; }
            set
            {
                m_validator.TryToWriteField("aBTransUnionScore_rep", () => m_app.aBTransUnionScore_rep, t => m_app.aBTransUnionScore_rep = t, value);
            }
        }


        public int aCTransUnionScore
        {
            get { return m_app.aCTransUnionScore; }
            set
            {
                m_validator.TryToWriteField("aCTransUnionScore", () => m_app.aCTransUnionScore, t => m_app.aCTransUnionScore = t, value);
            }
        }


        public string aCTransUnionScore_rep
        {
            get { return m_app.aCTransUnionScore_rep; }
            set
            {
                m_validator.TryToWriteField("aCTransUnionScore_rep", () => m_app.aCTransUnionScore_rep, t => m_app.aCTransUnionScore_rep = t, value);
            }
        }


        public int aBEquifaxScore
        {
            get { return m_app.aBEquifaxScore; }
            set
            {
                m_validator.TryToWriteField("aBEquifaxScore", () => m_app.aBEquifaxScore, t => m_app.aBEquifaxScore = t, value);
            }
        }


        public string aBEquifaxScore_rep
        {
            get { return m_app.aBEquifaxScore_rep; }
            set
            {
                m_validator.TryToWriteField("aBEquifaxScore_rep", () => m_app.aBEquifaxScore_rep, t => m_app.aBEquifaxScore_rep = t, value);
            }
        }


        public int aCEquifaxScore
        {
            get { return m_app.aCEquifaxScore; }
            set
            {
                m_validator.TryToWriteField("aCEquifaxScore", () => m_app.aCEquifaxScore, t => m_app.aCEquifaxScore = t, value);
            }
        }


        public string aCEquifaxScore_rep
        {
            get { return m_app.aCEquifaxScore_rep; }
            set
            {
                m_validator.TryToWriteField("aCEquifaxScore_rep", () => m_app.aCEquifaxScore_rep, t => m_app.aCEquifaxScore_rep = t, value);
            }
        }


        public int aBMinFicoScore
        {
            get { return m_app.aBMinFicoScore; }
            set
            {
                m_validator.TryToWriteField("aBMinFicoScore", () => m_app.aBMinFicoScore, t => m_app.aBMinFicoScore = t, value);
            }
        }


        public string aBMinFicoScore_rep
        {
            get { return m_app.aBMinFicoScore_rep; }
            set
            {
                m_validator.TryToWriteField("aBMinFicoScore_rep", () => m_app.aBMinFicoScore_rep, t => m_app.aBMinFicoScore_rep = t, value);
            }
        }


        public int aCMinFicoScore
        {
            get { return m_app.aCMinFicoScore; }
            set
            {
                m_validator.TryToWriteField("aCMinFicoScore", () => m_app.aCMinFicoScore, t => m_app.aCMinFicoScore = t, value);
            }
        }


        public string aCMinFicoScore_rep
        {
            get { return m_app.aCMinFicoScore_rep; }
            set
            {
                m_validator.TryToWriteField("aCMinFicoScore_rep", () => m_app.aCMinFicoScore_rep, t => m_app.aCMinFicoScore_rep = t, value);
            }
        }


        public E_BorrowerModeT BorrowerModeT
        {
            get { return m_app.BorrowerModeT; }
            set
            {
                m_validator.TryToWriteField("BorrowerModeT", () => m_app.BorrowerModeT, t => m_app.BorrowerModeT = t, value);
            }
        }


        public string aBMismoId
        {
            get { return m_app.aBMismoId; }
        }


        public string aCMismoId
        {
            get { return m_app.aCMismoId; }
        }


        public string aMismoId
        {
            get { return m_app.aMismoId; }
        }


        public string aSpouseMismoId
        {
            get { return m_app.aSpouseMismoId; }
        }


        public string aSpouseSsn
        {
            get { return m_app.aSpouseSsn; }
        }


        public bool aBIsValidNameSsn
        {
            get { return m_app.aBIsValidNameSsn; }
        }


        public bool aCIsValidNameSsn
        {
            get { return m_app.aCIsValidNameSsn; }
        }


        public bool aIsValidNameSsn
        {
            get { return m_app.aIsValidNameSsn; }
        }


        public bool aHasSpouse
        {
            get { return m_app.aHasSpouse; }
        }


        public CDateTime aExperianCreatedD
        {
            get { return m_app.aExperianCreatedD; }
            set
            {
                m_validator.TryToWriteField("aExperianCreatedD", () => m_app.aExperianCreatedD, t => m_app.aExperianCreatedD = t, value);
            }
        }


        public string aExperianCreatedD_rep
        {
            get { return m_app.aExperianCreatedD_rep; }
            set
            {
                m_validator.TryToWriteField("aExperianCreatedD_rep", () => m_app.aExperianCreatedD_rep, t => m_app.aExperianCreatedD_rep = t, value);
            }
        }


        public CDateTime aTransUnionCreatedD
        {
            get { return m_app.aTransUnionCreatedD; }
            set
            {
                m_validator.TryToWriteField("aTransUnionCreatedD", () => m_app.aTransUnionCreatedD, t => m_app.aTransUnionCreatedD = t, value);
            }
        }


        public string aTransUnionCreatedD_rep
        {
            get { return m_app.aTransUnionCreatedD_rep; }
            set
            {
                m_validator.TryToWriteField("aTransUnionCreatedD_rep", () => m_app.aTransUnionCreatedD_rep, t => m_app.aTransUnionCreatedD_rep = t, value);
            }
        }


        public CDateTime aEquifaxCreatedD
        {
            get { return m_app.aEquifaxCreatedD; }
            set
            {
                m_validator.TryToWriteField("aEquifaxCreatedD", () => m_app.aEquifaxCreatedD, t => m_app.aEquifaxCreatedD = t, value);
            }
        }


        public string aEquifaxCreatedD_rep
        {
            get { return m_app.aEquifaxCreatedD_rep; }
            set
            {
                m_validator.TryToWriteField("aEquifaxCreatedD_rep", () => m_app.aEquifaxCreatedD_rep, t => m_app.aEquifaxCreatedD_rep = t, value);
            }
        }


        public string aExperianFactors
        {
            get { return m_app.aExperianFactors; }
            set
            {
                m_validator.TryToWriteField("aExperianFactors", () => m_app.aExperianFactors, t => m_app.aExperianFactors = t, value);
            }
        }


        public string aTransUnionFactors
        {
            get { return m_app.aTransUnionFactors; }
            set
            {
                m_validator.TryToWriteField("aTransUnionFactors", () => m_app.aTransUnionFactors, t => m_app.aTransUnionFactors = t, value);
            }
        }


        public string aEquifaxFactors
        {
            get { return m_app.aEquifaxFactors; }
            set
            {
                m_validator.TryToWriteField("aEquifaxFactors", () => m_app.aEquifaxFactors, t => m_app.aEquifaxFactors = t, value);
            }
        }


        public int aTransUnionScorePe
        {
            get { return m_app.aTransUnionScorePe; }
            set
            {
                m_validator.TryToWriteField("aTransUnionScorePe", () => m_app.aTransUnionScorePe, t => m_app.aTransUnionScorePe = t, value);
            }
        }


        public string aTransUnionScorePe_rep
        {
            get { return m_app.aTransUnionScorePe_rep; }
            set
            {
                m_validator.TryToWriteField("aTransUnionScorePe_rep", () => m_app.aTransUnionScorePe_rep, t => m_app.aTransUnionScorePe_rep = t, value);
            }
        }


        public int aExperianScorePe
        {
            get { return m_app.aExperianScorePe; }
            set
            {
                m_validator.TryToWriteField("aExperianScorePe", () => m_app.aExperianScorePe, t => m_app.aExperianScorePe = t, value);
            }
        }


        public string aExperianScorePe_rep
        {
            get { return m_app.aExperianScorePe_rep; }
            set
            {
                m_validator.TryToWriteField("aExperianScorePe_rep", () => m_app.aExperianScorePe_rep, t => m_app.aExperianScorePe_rep = t, value);
            }
        }


        public int aEquifaxScorePe
        {
            get { return m_app.aEquifaxScorePe; }
            set
            {
                m_validator.TryToWriteField("aEquifaxScorePe", () => m_app.aEquifaxScorePe, t => m_app.aEquifaxScorePe = t, value);
            }
        }


        public string aEquifaxScorePe_rep
        {
            get { return m_app.aEquifaxScorePe_rep; }
            set
            {
                m_validator.TryToWriteField("aEquifaxScorePe_rep", () => m_app.aEquifaxScorePe_rep, t => m_app.aEquifaxScorePe_rep = t, value);
            }
        }

        public E_CreditScoreModelT aExperianModelT
        {
            get
            {
                return this.m_app.aExperianModelT;
            }
            set
            {
                this.m_validator.TryToWriteField(nameof(aExperianModelT), () => this.aExperianModelT, t => this.aExperianModelT = t, value);
            }
        }

        public string aExperianModelTOtherDescription
        {
            get
            {
                return this.m_app.aExperianModelTOtherDescription;
            }
            set
            {
                this.m_validator.TryToWriteField(nameof(aExperianModelTOtherDescription), () => this.aExperianModelTOtherDescription, t => this.aExperianModelTOtherDescription = t, value);
            }
        }

        public string aExperianModelName
        {
            get
            {
                return this.m_app.aExperianModelName;
            }
            set
            {
                this.m_validator.TryToWriteField(nameof(aExperianModelName), () => this.aExperianModelName, t => this.aExperianModelName = t, value);
            }
        }

        public E_CreditScoreModelT aTransUnionModelT
        {
            get
            {
                return this.m_app.aTransUnionModelT;
            }
            set
            {
                this.m_validator.TryToWriteField(nameof(aTransUnionModelT), () => this.aTransUnionModelT, t => this.aTransUnionModelT = t, value);
            }
        }

        public string aTransUnionModelTOtherDescription
        {
            get
            {
                return this.m_app.aTransUnionModelTOtherDescription;
            }
            set
            {
                this.m_validator.TryToWriteField(nameof(aTransUnionModelTOtherDescription), () => this.aTransUnionModelTOtherDescription, t => this.aTransUnionModelTOtherDescription = t, value);
            }
        }

        public string aTransUnionModelName
        {
            get
            {
                return this.m_app.aTransUnionModelName;
            }
            set
            {
                this.m_validator.TryToWriteField(nameof(aTransUnionModelName), () => this.aTransUnionModelName, t => this.aTransUnionModelName = t, value);
            }
        }

        public E_CreditScoreModelT aEquifaxModelT
        {
            get
            {
                return this.m_app.aEquifaxModelT;
            }
            set
            {
                this.m_validator.TryToWriteField(nameof(aEquifaxModelT), () => this.aEquifaxModelT, t => this.aEquifaxModelT = t, value);
            }
        }

        public string aEquifaxModelTOtherDescription
        {
            get
            {
                return this.m_app.aEquifaxModelTOtherDescription;
            }
            set
            {
                this.m_validator.TryToWriteField(nameof(aEquifaxModelTOtherDescription), () => this.aEquifaxModelTOtherDescription, t => this.aEquifaxModelTOtherDescription = t, value);
            }
        }

        public string aEquifaxModelName
        {
            get
            {
                return this.m_app.aEquifaxModelName;
            }
            set
            {
                this.m_validator.TryToWriteField(nameof(aEquifaxModelName), () => this.aEquifaxModelName, t => this.aEquifaxModelName = t, value);
            }
        }

        public string aOtherCreditModelName
        {
            get
            {
                return this.m_app.aOtherCreditModelName;
            }
        }

        public IEmpCollection aEmpCollection
        {
            get { return m_app.aEmpCollection; }
        }


        public string aEmplrNm
        {
            get { return m_app.aEmplrNm; }
        }


        public string aEmplrAddr
        {
            get { return m_app.aEmplrAddr; }
        }


        public string aEmplrCity
        {
            get { return m_app.aEmplrCity; }
        }


        public string aEmplrState
        {
            get { return m_app.aEmplrState; }
        }


        public string aEmplrZip
        {
            get { return m_app.aEmplrZip; }
        }


        public string aEmplrBusPhone
        {
            get { return m_app.aEmplrBusPhone; }
        }


        public bool aEmplrBusPhoneLckd
        {
            get { return m_app.aEmplrBusPhoneLckd; }
            set
            {
                m_validator.TryToWriteField("aEmplrBusPhoneLckd", () => m_app.aEmplrBusPhoneLckd, t => m_app.aEmplrBusPhoneLckd = t, value);
            }
        }


        public E_CountryMailT aCountryMailT
        {
            get { return m_app.aCountryMailT; }
            set
            {
                m_validator.TryToWriteField("aCountryMailT", () => m_app.aCountryMailT, t => m_app.aCountryMailT = t, value);
            }
        }


        public bool aAddrMailUsePresentAddr
        {
            get { return m_app.aAddrMailUsePresentAddr; }
            set
            {
                m_validator.TryToWriteField("aAddrMailUsePresentAddr", () => m_app.aAddrMailUsePresentAddr, t => m_app.aAddrMailUsePresentAddr = t, value);
            }
        }


        public string aAddrMail
        {
            get { return m_app.aAddrMail; }
            set
            {
                m_validator.TryToWriteField("aAddrMail", () => m_app.aAddrMail, t => m_app.aAddrMail = t, value);
            }
        }


        public string aCityMail
        {
            get { return m_app.aCityMail; }
            set
            {
                m_validator.TryToWriteField("aCityMail", () => m_app.aCityMail, t => m_app.aCityMail = t, value);
            }
        }


        public string aStateMail
        {
            get { return m_app.aStateMail; }
            set
            {
                m_validator.TryToWriteField("aStateMail", () => m_app.aStateMail, t => m_app.aStateMail = t, value);
            }
        }


        public string aZipMail
        {
            get { return m_app.aZipMail; }
            set
            {
                m_validator.TryToWriteField("aZipMail", () => m_app.aZipMail, t => m_app.aZipMail = t, value);
            }
        }


        public string aMidNm
        {
            get { return m_app.aMidNm; }
            set
            {
                m_validator.TryToWriteField("aMidNm", () => m_app.aMidNm, t => m_app.aMidNm = t, value);
            }
        }


        public string aSuffix
        {
            get { return m_app.aSuffix; }
            set
            {
                m_validator.TryToWriteField("aSuffix", () => m_app.aSuffix, t => m_app.aSuffix = t, value);
            }
        }


        public string aCellPhone
        {
            get { return m_app.aCellPhone; }
            set
            {
                m_validator.TryToWriteField("aCellPhone", () => m_app.aCellPhone, t => m_app.aCellPhone = t, value);
            }
        }

        public E_aHispanicT aHispanicT
        {
            get { return m_app.aHispanicT; }
            set
            {
                m_validator.TryToWriteField("aHispanicT", () => m_app.aHispanicT, t => m_app.aHispanicT = t, value);
            }
        }

        public E_aHispanicT aHispanicTFallback
        {
            get
            {
                return this.m_app.aHispanicTFallback;
            }
        }

        public string aIlliquidAssets_rep
        {
            get { return m_app.aIlliquidAssets_rep; }
        }

        public string aGiftFundAssets_rep
        {
            get { return m_app.aGiftFundAssets_rep; }
        }

        public string aLiquidAssetsNotIncludingGift_rep
        {
            get { return m_app.aLiquidAssetsNotIncludingGift_rep; }
        }


        public string aOmittedLiaMonPmt_rep
        {
            get { return m_app.aOmittedLiaMonPmt_rep; }
        }


        public string aOmittedLiaBal_rep
        {
            get { return m_app.aOmittedLiaBal_rep; }
        }


        public string aLiaBal_rep
        {
            get { return m_app.aLiaBal_rep; }
        }


        public string aMonthlyINotIncludingNetRentalI_rep
        {
            get { return m_app.aMonthlyINotIncludingNetRentalI_rep; }
        }


        public CDateTime aDob
        {
            get { return m_app.aDob; }
            set
            {
                m_validator.TryToWriteField("aDob", () => m_app.aDob, t => m_app.aDob = t, value);
            }
        }


        public string aDob_rep
        {
            get { return m_app.aDob_rep; }
            set
            {
                m_validator.TryToWriteField("aDob_rep", () => m_app.aDob_rep, t => m_app.aDob_rep = t, value);
            }
        }


        public int aHighestScore
        {
            get { return m_app.aHighestScore; }
        }


        public int aSecondHighestScore
        {
            get { return m_app.aSecondHighestScore; }
        }


        public string aEmplmtXmlContent
        {
            get { return m_app.aEmplmtXmlContent; }
            set
            {
                m_validator.TryToWriteField("aEmplmtXmlContent", () => m_app.aEmplmtXmlContent, t => m_app.aEmplmtXmlContent = t, value);
            }
        }

        public string aBaseI_rep
        {
            get { return m_app.aBaseI_rep; }
            set
            {
                m_validator.TryToWriteField("aBaseI_rep", () => m_app.aBaseI_rep, t => m_app.aBaseI_rep = t, value);
            }
        }


        public string aOvertimeI_rep
        {
            get { return m_app.aOvertimeI_rep; }
            set
            {
                m_validator.TryToWriteField("aOvertimeI_rep", () => m_app.aOvertimeI_rep, t => m_app.aOvertimeI_rep = t, value);
            }
        }


        public string aBonusesI_rep
        {
            get { return m_app.aBonusesI_rep; }
            set
            {
                m_validator.TryToWriteField("aBonusesI_rep", () => m_app.aBonusesI_rep, t => m_app.aBonusesI_rep = t, value);
            }
        }


        public string aCommisionI_rep
        {
            get { return m_app.aCommisionI_rep; }
            set
            {
                m_validator.TryToWriteField("aCommisionI_rep", () => m_app.aCommisionI_rep, t => m_app.aCommisionI_rep = t, value);
            }
        }


        public string aDividendI_rep
        {
            get { return m_app.aDividendI_rep; }
            set
            {
                m_validator.TryToWriteField("aDividendI_rep", () => m_app.aDividendI_rep, t => m_app.aDividendI_rep = t, value);
            }
        }


        public string aNetRentI_rep
        {
            get { return m_app.aNetRentI_rep; }
        }


        public string aNetRentI1003_rep
        {
            get { return m_app.aNetRentI1003_rep; }
            set
            {
                m_validator.TryToWriteField("aNetRentI1003_rep", () => m_app.aNetRentI1003_rep, t => m_app.aNetRentI1003_rep = t, value);
            }
        }


        public string aNetNegCf_rep
        {
            get { return m_app.aNetNegCf_rep; }
        }

        /// <summary>
        /// Property aSpPosCfReadOnly added to set attribute ReadOnly for fields aBSpPosCf and aCSpPosCf.
        /// </summary>
        public bool aSpPosCfReadOnly
        {
            get { return m_app.aSpPosCfReadOnly; }
        }

        public decimal aSpPosCf
        {
            get { return m_app.aSpPosCf; }
            set
            {
                m_validator.TryToWriteField("aSpPosCf", () => m_app.aSpPosCf, t => m_app.aSpPosCf = t, value);
            }
        }


        public string aSpPosCf_rep
        {
            get { return m_app.aSpPosCf_rep; }
            set
            {
                m_validator.TryToWriteField("aSpPosCf_rep", () => m_app.aSpPosCf_rep, t => m_app.aSpPosCf_rep = t, value);
            }
        }


        public decimal aNonbaseI
        {
            get { return m_app.aNonbaseI; }
        }


        public string aNonbaseI_rep
        {
            get { return m_app.aNonbaseI_rep; }
        }


        public decimal aOIFrom1008
        {
            get { return m_app.aOIFrom1008; }
            set
            {
                m_validator.TryToWriteField("aOIFrom1008", () => m_app.aOIFrom1008, t => m_app.aOIFrom1008 = t, value);
            }
        }


        public string aOIFrom1008_rep
        {
            get { return m_app.aOIFrom1008_rep; }
            set
            {
                m_validator.TryToWriteField("aOIFrom1008_rep", () => m_app.aOIFrom1008_rep, t => m_app.aOIFrom1008_rep = t, value);
            }
        }


        public string aLastNm
        {
            get { return m_app.aLastNm; }
            set
            {
                m_validator.TryToWriteField("aLastNm", () => m_app.aLastNm, t => m_app.aLastNm = t, value);
            }
        }


        public string aFirstNm
        {
            get { return m_app.aFirstNm; }
            set
            {
                m_validator.TryToWriteField("aFirstNm", () => m_app.aFirstNm, t => m_app.aFirstNm = t, value);
            }
        }


        public string aNm
        {
            get { return m_app.aNm; }
        }


        public string aAddr
        {
            get { return m_app.aAddr; }
            set
            {
                m_validator.TryToWriteField("aAddr", () => m_app.aAddr, t => m_app.aAddr = t, value);
            }
        }


        public E_aBAddrT aAddrT
        {
            get { return m_app.aAddrT; }
            set
            {
                m_validator.TryToWriteField("aAddrT", () => m_app.aAddrT, t => m_app.aAddrT = t, value);
            }
        }


        public string aAddrYrs
        {
            get { return m_app.aAddrYrs; }
            set
            {
                m_validator.TryToWriteField("aAddrYrs", () => m_app.aAddrYrs, t => m_app.aAddrYrs = t, value);
            }
        }


        public string aAddrTotalMonths
        {
            get { return m_app.aAddrTotalMonths; }
            set
            {
                m_validator.TryToWriteField("aAddrTotalMonths", () => m_app.aAddrTotalMonths, t => m_app.aAddrTotalMonths = t, value);
            }
        }

        public string aAddrWholeYears_rep
        {
            get
            {
                return m_app.aAddrWholeYears_rep;
            }
        }

        public string aAddrRemainderMonths_rep
        {
            get
            {
                return m_app.aAddrRemainderMonths_rep;
            }
        }

        public int aAge
        {
            get { return m_app.aAge; }
            set
            {
                m_validator.TryToWriteField("aAge", () => m_app.aAge, t => m_app.aAge = t, value);
            }
        }


        public string aAge_rep
        {
            get { return m_app.aAge_rep; }
            set
            {
                m_validator.TryToWriteField("aAge_rep", () => m_app.aAge_rep, t => m_app.aAge_rep = t, value);
            }
        }


        public int aSchoolYrs
        {
            get { return m_app.aSchoolYrs; }
            set
            {
                m_validator.TryToWriteField("aSchoolYrs", () => m_app.aSchoolYrs, t => m_app.aSchoolYrs = t, value);
            }
        }


        public string aSchoolYrs_rep
        {
            get { return m_app.aSchoolYrs_rep; }
            set
            {
                m_validator.TryToWriteField("aSchoolYrs_rep", () => m_app.aSchoolYrs_rep, t => m_app.aSchoolYrs_rep = t, value);
            }
        }


        public string aSchoolTotalMonths_rep
        {
            get { return m_app.aSchoolTotalMonths_rep; }
            set
            {
                m_validator.TryToWriteField("aSchoolTotalMonths_rep", () => m_app.aSchoolTotalMonths_rep, t => m_app.aSchoolTotalMonths_rep = t, value);
            }
        }


        public string aBusPhone
        {
            get { return m_app.aBusPhone; }
            set
            {
                m_validator.TryToWriteField("aBusPhone", () => m_app.aBusPhone, t => m_app.aBusPhone = t, value);
            }
        }


        public string aCity
        {
            get { return m_app.aCity; }
            set
            {
                m_validator.TryToWriteField("aCity", () => m_app.aCity, t => m_app.aCity = t, value);
            }
        }


        public string aDependAges
        {
            get { return m_app.aDependAges; }
            set
            {
                m_validator.TryToWriteField("aDependAges", () => m_app.aDependAges, t => m_app.aDependAges = t, value);
            }
        }


        public int aDependNum
        {
            get { return m_app.aDependNum; }
            set
            {
                m_validator.TryToWriteField("aDependNum", () => m_app.aDependNum, t => m_app.aDependNum = t, value);
            }
        }


        public string aDependNum_rep
        {
            get { return m_app.aDependNum_rep; }
            set
            {
                m_validator.TryToWriteField("aDependNum_rep", () => m_app.aDependNum_rep, t => m_app.aDependNum_rep = t, value);
            }
        }


        public string aEmail
        {
            get { return m_app.aEmail; }
            set
            {
                m_validator.TryToWriteField("aEmail", () => m_app.aEmail, t => m_app.aEmail = t, value);
            }
        }

        public E_aTypeT aTypeT
        {
            get { return m_app.aTypeT; }
            set
            {
                m_validator.TryToWriteField("aTypeT", () => m_app.aTypeT, t => m_app.aTypeT = t, value);
            }
        }

        public E_aTypeT aBorrowerTypeT
        {
            get { return m_app.aBorrowerTypeT; }
        }

        public string aHPhone
        {
            get { return m_app.aHPhone; }
            set
            {
                m_validator.TryToWriteField("aHPhone", () => m_app.aHPhone, t => m_app.aHPhone = t, value);
            }
        }


        public string aFax
        {
            get { return m_app.aFax; }
            set
            {
                m_validator.TryToWriteField("aFax", () => m_app.aFax, t => m_app.aFax = t, value);
            }
        }


        public E_aBMaritalStatT aMaritalStatT
        {
            get { return m_app.aMaritalStatT; }
            set
            {
                m_validator.TryToWriteField("aMaritalStatT", () => m_app.aMaritalStatT, t => m_app.aMaritalStatT = t, value);
            }
        }

        public string aMaritalStatTDescription
        {
            get { return m_app.aMaritalStatTDescription; }
        }


        public string aPrev1Addr
        {
            get { return m_app.aPrev1Addr; }
            set
            {
                m_validator.TryToWriteField("aPrev1Addr", () => m_app.aPrev1Addr, t => m_app.aPrev1Addr = t, value);
            }
        }


        public E_aBPrev1AddrT aPrev1AddrT
        {
            get { return m_app.aPrev1AddrT; }
            set
            {
                m_validator.TryToWriteField("aPrev1AddrT", () => m_app.aPrev1AddrT, t => m_app.aPrev1AddrT = t, value);
            }
        }


        public string aPrev1AddrYrs
        {
            get { return m_app.aPrev1AddrYrs; }
            set
            {
                m_validator.TryToWriteField("aPrev1AddrYrs", () => m_app.aPrev1AddrYrs, t => m_app.aPrev1AddrYrs = t, value);
            }
        }

        public string aPrev1AddrTotalMonths
        {
            get { return m_app.aPrev1AddrTotalMonths; }
            set
            {
                m_validator.TryToWriteField("aPrev1AddrTotalMonths", () => m_app.aPrev1AddrTotalMonths, t => m_app.aPrev1AddrTotalMonths = t, value);
            }
        }

        public string aPrev1AddrWholeYears_rep
        {
            get
            {
                return m_app.aPrev1AddrWholeYears_rep;
            }
        }

        public string aPrev1AddrRemainderMonths_rep
        {
            get
            {
                return m_app.aPrev1AddrRemainderMonths_rep;
            }
        }

        public string aPrev1City
        {
            get { return m_app.aPrev1City; }
            set
            {
                m_validator.TryToWriteField("aPrev1City", () => m_app.aPrev1City, t => m_app.aPrev1City = t, value);
            }
        }


        public string aPrev1State
        {
            get { return m_app.aPrev1State; }
            set
            {
                m_validator.TryToWriteField("aPrev1State", () => m_app.aPrev1State, t => m_app.aPrev1State = t, value);
            }
        }


        public string aPrev1Zip
        {
            get { return m_app.aPrev1Zip; }
            set
            {
                m_validator.TryToWriteField("aPrev1Zip", () => m_app.aPrev1Zip, t => m_app.aPrev1Zip = t, value);
            }
        }


        public string aPrev2Addr
        {
            get { return m_app.aPrev2Addr; }
            set
            {
                m_validator.TryToWriteField("aPrev2Addr", () => m_app.aPrev2Addr, t => m_app.aPrev2Addr = t, value);
            }
        }


        public E_aBPrev2AddrT aPrev2AddrT
        {
            get { return m_app.aPrev2AddrT; }
            set
            {
                m_validator.TryToWriteField("aPrev2AddrT", () => m_app.aPrev2AddrT, t => m_app.aPrev2AddrT = t, value);
            }
        }


        public string aPrev2AddrYrs
        {
            get { return m_app.aPrev2AddrYrs; }
            set
            {
                m_validator.TryToWriteField("aPrev2AddrYrs", () => m_app.aPrev2AddrYrs, t => m_app.aPrev2AddrYrs = t, value);
            }
        }

        public string aPrev2AddrTotalMonths
        {
            get { return m_app.aPrev2AddrTotalMonths; }
            set
            {
                m_validator.TryToWriteField("aPrev2AddrTotalMonths", () => m_app.aPrev2AddrTotalMonths, t => m_app.aPrev2AddrTotalMonths = t, value);
            }
        }

        public string aPrev2AddrWholeYears_rep
        {
            get
            {
                return m_app.aPrev2AddrWholeYears_rep;
            }
        }

        public string aPrev2AddrRemainderMonths_rep
        {
            get
            {
                return m_app.aPrev2AddrRemainderMonths_rep;
            }
        }

        public string aPrev2City
        {
            get { return m_app.aPrev2City; }
            set
            {
                m_validator.TryToWriteField("aPrev2City", () => m_app.aPrev2City, t => m_app.aPrev2City = t, value);
            }
        }


        public string aPrev2State
        {
            get { return m_app.aPrev2State; }
            set
            {
                m_validator.TryToWriteField("aPrev2State", () => m_app.aPrev2State, t => m_app.aPrev2State = t, value);
            }
        }


        public string aPrev2Zip
        {
            get { return m_app.aPrev2Zip; }
            set
            {
                m_validator.TryToWriteField("aPrev2Zip", () => m_app.aPrev2Zip, t => m_app.aPrev2Zip = t, value);
            }
        }


        public string aSsn
        {
            get { return m_app.aSsn; }
            set
            {
                m_validator.TryToWriteField("aSsn", () => m_app.aSsn, t => m_app.aSsn = t, value);
            }
        }

        public string aSsnLastFour
        {
            get
            {
                return m_app.aSsnLastFour;
            }
        }

        public string aState
        {
            get { return m_app.aState; }
            set
            {
                m_validator.TryToWriteField("aState", () => m_app.aState, t => m_app.aState = t, value);
            }
        }


        public string aZip
        {
            get { return m_app.aZip; }
            set
            {
                m_validator.TryToWriteField("aZip", () => m_app.aZip, t => m_app.aZip = t, value);
            }
        }


        public string aDecJudgment
        {
            get { return m_app.aDecJudgment; }
            set
            {
                m_validator.TryToWriteField("aDecJudgment", () => m_app.aDecJudgment, t => m_app.aDecJudgment = t, value);
            }
        }


        public string aDecBankrupt
        {
            get { return m_app.aDecBankrupt; }
            set
            {
                m_validator.TryToWriteField("aDecBankrupt", () => m_app.aDecBankrupt, t => m_app.aDecBankrupt = t, value);
            }
        }


        public string aDecForeclosure
        {
            get { return m_app.aDecForeclosure; }
            set
            {
                m_validator.TryToWriteField("aDecForeclosure", () => m_app.aDecForeclosure, t => m_app.aDecForeclosure = t, value);
            }
        }


        public string aDecLawsuit
        {
            get { return m_app.aDecLawsuit; }
            set
            {
                m_validator.TryToWriteField("aDecLawsuit", () => m_app.aDecLawsuit, t => m_app.aDecLawsuit = t, value);
            }
        }


        public string aDecObligated
        {
            get { return m_app.aDecObligated; }
            set
            {
                m_validator.TryToWriteField("aDecObligated", () => m_app.aDecObligated, t => m_app.aDecObligated = t, value);
            }
        }


        public string aDecDelinquent
        {
            get { return m_app.aDecDelinquent; }
            set
            {
                m_validator.TryToWriteField("aDecDelinquent", () => m_app.aDecDelinquent, t => m_app.aDecDelinquent = t, value);
            }
        }


        public string aDecAlimony
        {
            get { return m_app.aDecAlimony; }
            set
            {
                m_validator.TryToWriteField("aDecAlimony", () => m_app.aDecAlimony, t => m_app.aDecAlimony = t, value);
            }
        }


        public string aDecBorrowing
        {
            get { return m_app.aDecBorrowing; }
            set
            {
                m_validator.TryToWriteField("aDecBorrowing", () => m_app.aDecBorrowing, t => m_app.aDecBorrowing = t, value);
            }
        }


        public string aDecEndorser
        {
            get { return m_app.aDecEndorser; }
            set
            {
                m_validator.TryToWriteField("aDecEndorser", () => m_app.aDecEndorser, t => m_app.aDecEndorser = t, value);
            }
        }


        public string aDecCitizen
        {
            get { return m_app.aDecCitizen; }
            set
            {
                m_validator.TryToWriteField("aDecCitizen", () => m_app.aDecCitizen, t => m_app.aDecCitizen = t, value);
            }
        }


        public string aDecResidency
        {
            get { return m_app.aDecResidency; }
            set
            {
                m_validator.TryToWriteField("aDecResidency", () => m_app.aDecResidency, t => m_app.aDecResidency = t, value);
            }
        }


        public string aDecOcc
        {
            get { return m_app.aDecOcc; }
            set
            {
                m_validator.TryToWriteField("aDecOcc", () => m_app.aDecOcc, t => m_app.aDecOcc = t, value);
            }
        }


        public string aDecPastOwnership
        {
            get { return m_app.aDecPastOwnership; }
            set
            {
                m_validator.TryToWriteField("aDecPastOwnership", () => m_app.aDecPastOwnership, t => m_app.aDecPastOwnership = t, value);
            }
        }


        public E_aBDecPastOwnedPropT aDecPastOwnedPropT
        {
            get { return m_app.aDecPastOwnedPropT; }
            set
            {
                m_validator.TryToWriteField("aDecPastOwnedPropT", () => m_app.aDecPastOwnedPropT, t => m_app.aDecPastOwnedPropT = t, value);
            }
        }


        public E_aBDecPastOwnedPropTitleT aDecPastOwnedPropTitleT
        {
            get { return m_app.aDecPastOwnedPropTitleT; }
            set
            {
                m_validator.TryToWriteField("aDecPastOwnedPropTitleT", () => m_app.aDecPastOwnedPropTitleT, t => m_app.aDecPastOwnedPropTitleT = t, value);
            }
        }


        public bool aNoFurnish
        {
            get { return m_app.aNoFurnish; }
            set
            {
                m_validator.TryToWriteField("aNoFurnish", () => m_app.aNoFurnish, t => m_app.aNoFurnish = t, value);
            }
        }

        public E_GenderT aGenderFallback
        {
            get
            {
                return this.m_app.aGenderFallback;
            }
        }

        public E_GenderT aGender
        {
            get { return m_app.aGender; }
            set
            {
                m_validator.TryToWriteField("aGender", () => m_app.aGender, t => m_app.aGender = t, value);
            }
        }


        public E_aBRaceT aRaceT
        {
            get { return m_app.aRaceT; }
            set
            {
                m_validator.TryToWriteField("aRaceT", () => m_app.aRaceT, t => m_app.aRaceT = t, value);
            }
        }


        public string aORaceDesc
        {
            get { return m_app.aORaceDesc; }
            set
            {
                m_validator.TryToWriteField("aORaceDesc", () => m_app.aORaceDesc, t => m_app.aORaceDesc = t, value);
            }
        }


        public int aExperianScore
        {
            get { return m_app.aExperianScore; }
            set
            {
                m_validator.TryToWriteField("aExperianScore", () => m_app.aExperianScore, t => m_app.aExperianScore = t, value);
            }
        }


        public string aExperianScore_rep
        {
            get { return m_app.aExperianScore_rep; }
            set
            {
                m_validator.TryToWriteField("aExperianScore_rep", () => m_app.aExperianScore_rep, t => m_app.aExperianScore_rep = t, value);
            }
        }


        public int aTransUnionScore
        {
            get { return m_app.aTransUnionScore; }
            set
            {
                m_validator.TryToWriteField("aTransUnionScore", () => m_app.aTransUnionScore, t => m_app.aTransUnionScore = t, value);
            }
        }


        public string aTransUnionScore_rep
        {
            get { return m_app.aTransUnionScore_rep; }
            set
            {
                m_validator.TryToWriteField("aTransUnionScore_rep", () => m_app.aTransUnionScore_rep, t => m_app.aTransUnionScore_rep = t, value);
            }
        }


        public int aEquifaxScore
        {
            get { return m_app.aEquifaxScore; }
            set
            {
                m_validator.TryToWriteField("aEquifaxScore", () => m_app.aEquifaxScore, t => m_app.aEquifaxScore = t, value);
            }
        }


        public string aEquifaxScore_rep
        {
            get { return m_app.aEquifaxScore_rep; }
            set
            {
                m_validator.TryToWriteField("aEquifaxScore_rep", () => m_app.aEquifaxScore_rep, t => m_app.aEquifaxScore_rep = t, value);
            }
        }


        public int aMinFicoScore
        {
            get { return m_app.aMinFicoScore; }
            set
            {
                m_validator.TryToWriteField("aMinFicoScore", () => m_app.aMinFicoScore, t => m_app.aMinFicoScore = t, value);
            }
        }


        public string aMinFicoScore_rep
        {
            get { return m_app.aMinFicoScore_rep; }
            set
            {
                m_validator.TryToWriteField("aMinFicoScore_rep", () => m_app.aMinFicoScore_rep, t => m_app.aMinFicoScore_rep = t, value);
            }
        }


        public bool aIs4506TFiledTaxesSeparately
        {
            get { return m_app.aIs4506TFiledTaxesSeparately; }
            set
            {
                m_validator.TryToWriteField("aIs4506TFiledTaxesSeparately", () => m_app.aIs4506TFiledTaxesSeparately, t => m_app.aIs4506TFiledTaxesSeparately = t, value);
            }
        }


        public string aB4506TPrevStreetAddr
        {
            get { return m_app.aB4506TPrevStreetAddr; }
            set
            {
                m_validator.TryToWriteField("aB4506TPrevStreetAddr", () => m_app.aB4506TPrevStreetAddr, t => m_app.aB4506TPrevStreetAddr = t, value);
            }
        }


        public string aB4506TPrevCity
        {
            get { return m_app.aB4506TPrevCity; }
            set
            {
                m_validator.TryToWriteField("aB4506TPrevCity", () => m_app.aB4506TPrevCity, t => m_app.aB4506TPrevCity = t, value);
            }
        }


        public string aB4506TPrevState
        {
            get { return m_app.aB4506TPrevState; }
            set
            {
                m_validator.TryToWriteField("aB4506TPrevState", () => m_app.aB4506TPrevState, t => m_app.aB4506TPrevState = t, value);
            }
        }


        public string aB4506TPrevZip
        {
            get { return m_app.aB4506TPrevZip; }
            set
            {
                m_validator.TryToWriteField("aB4506TPrevZip", () => m_app.aB4506TPrevZip, t => m_app.aB4506TPrevZip = t, value);
            }
        }


        public string aB4506TThirdPartyName
        {
            get { return m_app.aB4506TThirdPartyName; }
            set
            {
                m_validator.TryToWriteField("aB4506TThirdPartyName", () => m_app.aB4506TThirdPartyName, t => m_app.aB4506TThirdPartyName = t, value);
            }
        }


        public string aB4506TThirdPartyStreetAddr
        {
            get { return m_app.aB4506TThirdPartyStreetAddr; }
            set
            {
                m_validator.TryToWriteField("aB4506TThirdPartyStreetAddr", () => m_app.aB4506TThirdPartyStreetAddr, t => m_app.aB4506TThirdPartyStreetAddr = t, value);
            }
        }


        public string aB4506TThirdPartyCity
        {
            get { return m_app.aB4506TThirdPartyCity; }
            set
            {
                m_validator.TryToWriteField("aB4506TThirdPartyCity", () => m_app.aB4506TThirdPartyCity, t => m_app.aB4506TThirdPartyCity = t, value);
            }
        }


        public string aB4506TThirdPartyState
        {
            get { return m_app.aB4506TThirdPartyState; }
            set
            {
                m_validator.TryToWriteField("aB4506TThirdPartyState", () => m_app.aB4506TThirdPartyState, t => m_app.aB4506TThirdPartyState = t, value);
            }
        }


        public string aB4506TThirdPartyZip
        {
            get { return m_app.aB4506TThirdPartyZip; }
            set
            {
                m_validator.TryToWriteField("aB4506TThirdPartyZip", () => m_app.aB4506TThirdPartyZip, t => m_app.aB4506TThirdPartyZip = t, value);
            }
        }


        public string aB4506TThirdPartyPhone
        {
            get { return m_app.aB4506TThirdPartyPhone; }
            set
            {
                m_validator.TryToWriteField("aB4506TThirdPartyPhone", () => m_app.aB4506TThirdPartyPhone, t => m_app.aB4506TThirdPartyPhone = t, value);
            }
        }


        public string aB4506TTranscript
        {
            get { return m_app.aB4506TTranscript; }
            set
            {
                m_validator.TryToWriteField("aB4506TTranscript", () => m_app.aB4506TTranscript, t => m_app.aB4506TTranscript = t, value);
            }
        }


        public bool aB4056TIsReturnTranscript
        {
            get { return m_app.aB4056TIsReturnTranscript; }
            set
            {
                m_validator.TryToWriteField("aB4056TIsReturnTranscript", () => m_app.aB4056TIsReturnTranscript, t => m_app.aB4056TIsReturnTranscript = t, value);
            }
        }


        public bool aB4506TIsAccountTranscript
        {
            get { return m_app.aB4506TIsAccountTranscript; }
            set
            {
                m_validator.TryToWriteField("aB4506TIsAccountTranscript", () => m_app.aB4506TIsAccountTranscript, t => m_app.aB4506TIsAccountTranscript = t, value);
            }
        }


        public bool aB4056TIsRecordAccountTranscript
        {
            get { return m_app.aB4056TIsRecordAccountTranscript; }
            set
            {
                m_validator.TryToWriteField("aB4056TIsRecordAccountTranscript", () => m_app.aB4056TIsRecordAccountTranscript, t => m_app.aB4056TIsRecordAccountTranscript = t, value);
            }
        }


        public bool aB4506TVerificationNonfiling
        {
            get { return m_app.aB4506TVerificationNonfiling; }
            set
            {
                m_validator.TryToWriteField("aB4506TVerificationNonfiling", () => m_app.aB4506TVerificationNonfiling, t => m_app.aB4506TVerificationNonfiling = t, value);
            }
        }


        public bool aB4506TSeriesTranscript
        {
            get { return m_app.aB4506TSeriesTranscript; }
            set
            {
                m_validator.TryToWriteField("aB4506TSeriesTranscript", () => m_app.aB4506TSeriesTranscript, t => m_app.aB4506TSeriesTranscript = t, value);
            }
        }


        public string aB4506TYear1_rep
        {
            get { return m_app.aB4506TYear1_rep; }
            set
            {
                m_validator.TryToWriteField("aB4506TYear1_rep", () => m_app.aB4506TYear1_rep, t => m_app.aB4506TYear1_rep = t, value);
            }
        }


        public string aB4506TYear2_rep
        {
            get { return m_app.aB4506TYear2_rep; }
            set
            {
                m_validator.TryToWriteField("aB4506TYear2_rep", () => m_app.aB4506TYear2_rep, t => m_app.aB4506TYear2_rep = t, value);
            }
        }


        public string aB4506TYear3_rep
        {
            get { return m_app.aB4506TYear3_rep; }
            set
            {
                m_validator.TryToWriteField("aB4506TYear3_rep", () => m_app.aB4506TYear3_rep, t => m_app.aB4506TYear3_rep = t, value);
            }
        }


        public string aB4506TYear4_rep
        {
            get { return m_app.aB4506TYear4_rep; }
            set
            {
                m_validator.TryToWriteField("aB4506TYear4_rep", () => m_app.aB4506TYear4_rep, t => m_app.aB4506TYear4_rep = t, value);
            }
        }

        public bool aB4506TRequestYrHadIdentityTheft
        {
            get { return m_app.aB4506TRequestYrHadIdentityTheft; }
            set
            {
                m_validator.TryToWriteField("aB4506TRequestYrHadIdentityTheft", () => m_app.aB4506TRequestYrHadIdentityTheft, t => m_app.aB4506TRequestYrHadIdentityTheft = t, value);
            }

        }

        public bool aC4506TRequestYrHadIdentityTheft
        {
            get { return m_app.aC4506TRequestYrHadIdentityTheft; }
            set
            {
                m_validator.TryToWriteField("aC4506TRequestYrHadIdentityTheft", () => m_app.aC4506TRequestYrHadIdentityTheft, t => m_app.aC4506TRequestYrHadIdentityTheft = t, value);
            }

        }

        public bool aB4506TSignatoryAttested
        {
            get { return m_app.aB4506TSignatoryAttested; }
            set
            {
                m_validator.TryToWriteField("aB4506TSignatoryAttested", () => m_app.aB4506TSignatoryAttested, t => m_app.aB4506TSignatoryAttested = t, value);
            }
        }

        public bool aC4506TSignatoryAttested
        {
            get { return m_app.aC4506TSignatoryAttested; }
            set
            {
                m_validator.TryToWriteField("aC4506TSignatoryAttested", () => m_app.aC4506TSignatoryAttested, t => m_app.aC4506TSignatoryAttested = t, value);
            }
        }

        public string aC4506TPrevStreetAddr
        {
            get { return m_app.aC4506TPrevStreetAddr; }
            set
            {
                m_validator.TryToWriteField("aC4506TPrevStreetAddr", () => m_app.aC4506TPrevStreetAddr, t => m_app.aC4506TPrevStreetAddr = t, value);
            }
        }


        public string aC4506TPrevCity
        {
            get { return m_app.aC4506TPrevCity; }
            set
            {
                m_validator.TryToWriteField("aC4506TPrevCity", () => m_app.aC4506TPrevCity, t => m_app.aC4506TPrevCity = t, value);
            }
        }


        public string aC4506TPrevState
        {
            get { return m_app.aC4506TPrevState; }
            set
            {
                m_validator.TryToWriteField("aC4506TPrevState", () => m_app.aC4506TPrevState, t => m_app.aC4506TPrevState = t, value);
            }
        }


        public string aC4506TPrevZip
        {
            get { return m_app.aC4506TPrevZip; }
            set
            {
                m_validator.TryToWriteField("aC4506TPrevZip", () => m_app.aC4506TPrevZip, t => m_app.aC4506TPrevZip = t, value);
            }
        }


        public string aC4506TThirdPartyName
        {
            get { return m_app.aC4506TThirdPartyName; }
            set
            {
                m_validator.TryToWriteField("aC4506TThirdPartyName", () => m_app.aC4506TThirdPartyName, t => m_app.aC4506TThirdPartyName = t, value);
            }
        }


        public string aC4506TThirdPartyStreetAddr
        {
            get { return m_app.aC4506TThirdPartyStreetAddr; }
            set
            {
                m_validator.TryToWriteField("aC4506TThirdPartyStreetAddr", () => m_app.aC4506TThirdPartyStreetAddr, t => m_app.aC4506TThirdPartyStreetAddr = t, value);
            }
        }


        public string aC4506TThirdPartyCity
        {
            get { return m_app.aC4506TThirdPartyCity; }
            set
            {
                m_validator.TryToWriteField("aC4506TThirdPartyCity", () => m_app.aC4506TThirdPartyCity, t => m_app.aC4506TThirdPartyCity = t, value);
            }
        }


        public string aC4506TThirdPartyState
        {
            get { return m_app.aC4506TThirdPartyState; }
            set
            {
                m_validator.TryToWriteField("aC4506TThirdPartyState", () => m_app.aC4506TThirdPartyState, t => m_app.aC4506TThirdPartyState = t, value);
            }
        }


        public string aC4506TThirdPartyZip
        {
            get { return m_app.aC4506TThirdPartyZip; }
            set
            {
                m_validator.TryToWriteField("aC4506TThirdPartyZip", () => m_app.aC4506TThirdPartyZip, t => m_app.aC4506TThirdPartyZip = t, value);
            }
        }


        public string aC4506TThirdPartyPhone
        {
            get { return m_app.aC4506TThirdPartyPhone; }
            set
            {
                m_validator.TryToWriteField("aC4506TThirdPartyPhone", () => m_app.aC4506TThirdPartyPhone, t => m_app.aC4506TThirdPartyPhone = t, value);
            }
        }


        public string aC4506TTranscript
        {
            get { return m_app.aC4506TTranscript; }
            set
            {
                m_validator.TryToWriteField("aC4506TTranscript", () => m_app.aC4506TTranscript, t => m_app.aC4506TTranscript = t, value);
            }
        }


        public bool aC4056TIsReturnTranscript
        {
            get { return m_app.aC4056TIsReturnTranscript; }
            set
            {
                m_validator.TryToWriteField("aC4056TIsReturnTranscript", () => m_app.aC4056TIsReturnTranscript, t => m_app.aC4056TIsReturnTranscript = t, value);
            }
        }


        public bool aC4506TIsAccountTranscript
        {
            get { return m_app.aC4506TIsAccountTranscript; }
            set
            {
                m_validator.TryToWriteField("aC4506TIsAccountTranscript", () => m_app.aC4506TIsAccountTranscript, t => m_app.aC4506TIsAccountTranscript = t, value);
            }
        }


        public bool aC4056TIsRecordAccountTranscript
        {
            get { return m_app.aC4056TIsRecordAccountTranscript; }
            set
            {
                m_validator.TryToWriteField("aC4056TIsRecordAccountTranscript", () => m_app.aC4056TIsRecordAccountTranscript, t => m_app.aC4056TIsRecordAccountTranscript = t, value);
            }
        }


        public bool aC4506TVerificationNonfiling
        {
            get { return m_app.aC4506TVerificationNonfiling; }
            set
            {
                m_validator.TryToWriteField("aC4506TVerificationNonfiling", () => m_app.aC4506TVerificationNonfiling, t => m_app.aC4506TVerificationNonfiling = t, value);
            }
        }


        public bool aC4506TSeriesTranscript
        {
            get { return m_app.aC4506TSeriesTranscript; }
            set
            {
                m_validator.TryToWriteField("aC4506TSeriesTranscript", () => m_app.aC4506TSeriesTranscript, t => m_app.aC4506TSeriesTranscript = t, value);
            }
        }


        public string aC4506TYear1_rep
        {
            get { return m_app.aC4506TYear1_rep; }
            set
            {
                m_validator.TryToWriteField("aC4506TYear1_rep", () => m_app.aC4506TYear1_rep, t => m_app.aC4506TYear1_rep = t, value);
            }
        }


        public string aC4506TYear2_rep
        {
            get { return m_app.aC4506TYear2_rep; }
            set
            {
                m_validator.TryToWriteField("aC4506TYear2_rep", () => m_app.aC4506TYear2_rep, t => m_app.aC4506TYear2_rep = t, value);
            }
        }


        public string aC4506TYear3_rep
        {
            get { return m_app.aC4506TYear3_rep; }
            set
            {
                m_validator.TryToWriteField("aC4506TYear3_rep", () => m_app.aC4506TYear3_rep, t => m_app.aC4506TYear3_rep = t, value);
            }
        }


        public string aC4506TYear4_rep
        {
            get { return m_app.aC4506TYear4_rep; }
            set
            {
                m_validator.TryToWriteField("aC4506TYear4_rep", () => m_app.aC4506TYear4_rep, t => m_app.aC4506TYear4_rep = t, value);
            }
        }

        public string aB4506TNm
        {
            get { return m_app.aB4506TNm; }
            set
            {
                m_validator.TryToWriteField("aB4506TNm", () => m_app.aB4506TNm, t => m_app.aB4506TNm = t, value);
            }

        }
        public bool aB4506TNmLckd
        {
            get { return m_app.aB4506TNmLckd; }
            set
            {
                m_validator.TryToWriteField("aB4506TNmLckd", () => m_app.aB4506TNmLckd, t => m_app.aB4506TNmLckd = t, value);
            }

        }
        public string aC4506TNm
        {
            get { return m_app.aC4506TNm; }
            set
            {
                m_validator.TryToWriteField("aC4506TNm", () => m_app.aC4506TNm, t => m_app.aC4506TNm = t, value);
            }

        }
        public bool aC4506TNmLckd
        {
            get { return m_app.aC4506TNmLckd; }
            set
            {
                m_validator.TryToWriteField("aC4506TNmLckd", () => m_app.aC4506TNmLckd, t => m_app.aC4506TNmLckd = t, value);
            }

        }
        public string aB4506TNmPdfDisplay
        {
            get { return m_app.aB4506TNmPdfDisplay; }
        }


        public string aB4506TSsnTinEin
        {
            get { return m_app.aB4506TSsnTinEin; }
            set
            {
                m_validator.TryToWriteField("aB4506TSsnTinEin", () => m_app.aB4506TSsnTinEin, t => m_app.aB4506TSsnTinEin = t, value);
            }
        }

        public string aB4506TSsnTinEinCalc
        {
            get { return m_app.aB4506TSsnTinEinCalc; }
        }


        public string aC4506TNmPdfDisplay
        {
            get { return m_app.aC4506TNmPdfDisplay; }
        }


        public string aC4506TSsnTinEin
        {
            get { return m_app.aC4506TSsnTinEin; }
            set
            {
                m_validator.TryToWriteField("aC4506TSsnTinEin", () => m_app.aC4506TSsnTinEin, t => m_app.aC4506TSsnTinEin = t, value);
            }
        }

        public string aC4506TSsnTinEinCalc
        {
            get { return m_app.aC4506TSsnTinEinCalc; }
        }

        public string a4506TAddr_Multiline
        {
            get { return m_app.a4506TAddr_Multiline; }
        }

        public string a4506TAddr
        {
            get { return m_app.a4506TAddr; }
        }

        public string a4506TPrevAddr_Multiline
        {
            get { return m_app.a4506TPrevAddr_Multiline; }
        }

        public string a4506TPrevAddr
        {
            get { return m_app.a4506TPrevAddr; }
        }

        public string a4506TThirdPartyInfo
        {
            get { return m_app.a4506TThirdPartyInfo; }
        }


        public string a4506TTranscript
        {
            get { return m_app.a4506TTranscript; }
        }


        public bool a4056TIsReturnTranscript
        {
            get { return m_app.a4056TIsReturnTranscript; }
        }


        public bool a4506TIsAccountTranscript
        {
            get { return m_app.a4506TIsAccountTranscript; }
        }


        public bool a4056TIsRecordAccountTranscript
        {
            get { return m_app.a4056TIsRecordAccountTranscript; }
        }


        public bool a4506TVerificationNonfiling
        {
            get { return m_app.a4506TVerificationNonfiling; }
        }


        public bool a4506TSeriesTranscript
        {
            get { return m_app.a4506TSeriesTranscript; }
        }

        public CDateTime a4506TYear1
        {
            get { return m_app.a4506TYear1; }
        }

        public string a4506TYear1_rep
        {
            get { return m_app.a4506TYear1_rep; }
        }

        public CDateTime a4506TYear2
        {
            get { return m_app.a4506TYear2; }
        }

        public string a4506TYear2_rep
        {
            get { return m_app.a4506TYear2_rep; }
        }

        public CDateTime a4506TYear3
        {
            get { return m_app.a4506TYear3; }
        }

        public string a4506TYear3_rep
        {
            get { return m_app.a4506TYear3_rep; }
        }

        public CDateTime a4506TYear4
        {
            get { return m_app.a4506TYear4; }
        }

        public string a4506TYear4_rep
        {
            get { return m_app.a4506TYear4_rep; }
        }

        public string a4506TStreetAddr
        {
            get { return m_app.a4506TStreetAddr; }
        }
        public string a4506TCity
        {
            get { return m_app.a4506TCity; }
        }
        public string a4506TState
        {
            get { return m_app.a4506TState; }
        }
        public string a4506TZip
        {
            get { return m_app.a4506TZip; }
        }

        public string a4506TPrevStreetAddr
        {
            get { return m_app.a4506TPrevStreetAddr; }
        }
        public string a4506TPrevCity
        {
            get { return m_app.a4506TPrevCity; }
        }
        public string a4506TPrevState
        {
            get { return m_app.a4506TPrevState; }
        }
        public string a4506TPrevZip
        {
            get { return m_app.a4506TPrevZip; }
        }

        public string a4506TThirdPartyName
        {
            get { return m_app.a4506TThirdPartyName; }
        }
        public string a4506TThirdPartyStreetAddr
        {
            get { return m_app.a4506TThirdPartyStreetAddr; }
        }
        public string a4506TThirdPartyCity
        {
            get { return m_app.a4506TThirdPartyCity; }
        }
        public string a4506TThirdPartyState
        {
            get { return m_app.a4506TThirdPartyState; }
        }
        public string a4506TThirdPartyZip
        {
            get { return m_app.a4506TThirdPartyZip; }
        }

        public string a4506TThirdPartyAddress
        {
            get { return m_app.a4506TThirdPartyAddress; }
        }

        public string a4506TThirdPartyPhone
        {
            get { return m_app.a4506TThirdPartyPhone; }
        }

        public bool a4506TRequestYrHadIdentityTheft
        {
            get { return m_app.a4506TRequestYrHadIdentityTheft; }
        }

        public bool a4506TSignatoryAttested
        {
            get { return m_app.a4506TSignatoryAttested; }
        }

        public E_aRelationshipTitleT aBRelationshipTitleT
        {
            get { return m_app.aBRelationshipTitleT; }
            set
            {
                m_validator.TryToWriteField("aBRelationshipTitleT", () => m_app.aBRelationshipTitleT, t => m_app.aBRelationshipTitleT = t, value);
            }
        }


        public string aBRelationshipTitleT_rep
        {
            get { return m_app.aBRelationshipTitleT_rep; }
        }

        public string aBRelationshipTitleOtherDesc
        {
            get { return m_app.aBRelationshipTitleOtherDesc; }
            set
            {
                m_validator.TryToWriteField("aBRelationshipTitleOtherDesc", () => m_app.aBRelationshipTitleOtherDesc, t => m_app.aBRelationshipTitleOtherDesc = t, value);
            }
        }


        public E_aRelationshipTitleT aCRelationshipTitleT
        {
            get { return m_app.aCRelationshipTitleT; }
            set
            {
                m_validator.TryToWriteField("aCRelationshipTitleT", () => m_app.aCRelationshipTitleT, t => m_app.aCRelationshipTitleT = t, value);
            }
        }


        public string aCRelationshipTitleT_rep
        {
            get { return m_app.aCRelationshipTitleT_rep; }
        }

        public string aCRelationshipTitleOtherDesc
        {
            get { return m_app.aCRelationshipTitleOtherDesc; }
            set
            {
                m_validator.TryToWriteField("aCRelationshipTitleOtherDesc", () => m_app.aCRelationshipTitleOtherDesc, t => m_app.aCRelationshipTitleOtherDesc = t, value);
            }
        }


        public E_aRelationshipTitleT aRelationshipTitleT
        {
            get { return m_app.aRelationshipTitleT; }
        }

        public string aRelationshipTitleT_rep
        {
            get { return m_app.aRelationshipTitleT_rep; }
        }

        public string aRelationshipTitleOtherDesc
        {
            get { return m_app.aRelationshipTitleOtherDesc; }
        }


        public E_TriState aWillOccupyRefiResidence
        {
            get { return m_app.aWillOccupyRefiResidence; }
        }


        public decimal aVALAnalysisValue
        {
            get { return m_app.aVALAnalysisValue; }
            set
            {
                m_validator.TryToWriteField("aVALAnalysisValue", () => m_app.aVALAnalysisValue, t => m_app.aVALAnalysisValue = t, value);
            }
        }


        public string aVALAnalysisValue_rep
        {
            get { return m_app.aVALAnalysisValue_rep; }
            set
            {
                m_validator.TryToWriteField("aVALAnalysisValue_rep", () => m_app.aVALAnalysisValue_rep, t => m_app.aVALAnalysisValue_rep = t, value);
            }
        }


        public CDateTime aVALAnalysisExpirationD
        {
            get { return m_app.aVALAnalysisExpirationD; }
            set
            {
                m_validator.TryToWriteField("aVALAnalysisExpirationD", () => m_app.aVALAnalysisExpirationD, t => m_app.aVALAnalysisExpirationD = t, value);
            }
        }


        public string aVALAnalysisExpirationD_rep
        {
            get { return m_app.aVALAnalysisExpirationD_rep; }
            set
            {
                m_validator.TryToWriteField("aVALAnalysisExpirationD_rep", () => m_app.aVALAnalysisExpirationD_rep, t => m_app.aVALAnalysisExpirationD_rep = t, value);
            }
        }


        public int aVALAnalysisEconomicLifeYrs
        {
            get { return m_app.aVALAnalysisEconomicLifeYrs; }
            set
            {
                m_validator.TryToWriteField("aVALAnalysisEconomicLifeYrs", () => m_app.aVALAnalysisEconomicLifeYrs, t => m_app.aVALAnalysisEconomicLifeYrs = t, value);
            }
        }


        public string aVALAnalysisEconomicLifeYrs_rep
        {
            get { return m_app.aVALAnalysisEconomicLifeYrs_rep; }
            set
            {
                m_validator.TryToWriteField("aVALAnalysisEconomicLifeYrs_rep", () => m_app.aVALAnalysisEconomicLifeYrs_rep, t => m_app.aVALAnalysisEconomicLifeYrs_rep = t, value);
            }
        }


        public E_TriState aBUsdaGuaranteeVeteranTri
        {
            get { return m_app.aBUsdaGuaranteeVeteranTri; }
            set
            {
                m_validator.TryToWriteField("aBUsdaGuaranteeVeteranTri", () => m_app.aBUsdaGuaranteeVeteranTri, t => m_app.aBUsdaGuaranteeVeteranTri = t, value);
            }
        }


        public E_TriState aCUsdaGuaranteeVeteranTri
        {
            get { return m_app.aCUsdaGuaranteeVeteranTri; }
            set
            {
                m_validator.TryToWriteField("aCUsdaGuaranteeVeteranTri", () => m_app.aCUsdaGuaranteeVeteranTri, t => m_app.aCUsdaGuaranteeVeteranTri = t, value);
            }
        }


        public E_TriState aBUsdaGuaranteeDisabledTri
        {
            get { return m_app.aBUsdaGuaranteeDisabledTri; }
            set
            {
                m_validator.TryToWriteField("aBUsdaGuaranteeDisabledTri", () => m_app.aBUsdaGuaranteeDisabledTri, t => m_app.aBUsdaGuaranteeDisabledTri = t, value);
            }
        }


        public E_TriState aCUsdaGuaranteeDisabledTri
        {
            get { return m_app.aCUsdaGuaranteeDisabledTri; }
            set
            {
                m_validator.TryToWriteField("aCUsdaGuaranteeDisabledTri", () => m_app.aCUsdaGuaranteeDisabledTri, t => m_app.aCUsdaGuaranteeDisabledTri = t, value);
            }
        }


        public E_TriState aBUsdaGuaranteeFirstTimeHomeBuyerTri
        {
            get { return m_app.aBUsdaGuaranteeFirstTimeHomeBuyerTri; }
            set
            {
                m_validator.TryToWriteField("aBUsdaGuaranteeFirstTimeHomeBuyerTri", () => m_app.aBUsdaGuaranteeFirstTimeHomeBuyerTri, t => m_app.aBUsdaGuaranteeFirstTimeHomeBuyerTri = t, value);
            }
        }


        public E_TriState aCUsdaGuaranteeFirstTimeHomeBuyerTri
        {
            get { return m_app.aCUsdaGuaranteeFirstTimeHomeBuyerTri; }
            set
            {
                m_validator.TryToWriteField("aCUsdaGuaranteeFirstTimeHomeBuyerTri", () => m_app.aCUsdaGuaranteeFirstTimeHomeBuyerTri, t => m_app.aCUsdaGuaranteeFirstTimeHomeBuyerTri = t, value);
            }
        }


        public E_aUsdaGuaranteeEmployeeRelationshipT aBUsdaGuaranteeEmployeeRelationshipT
        {
            get { return m_app.aBUsdaGuaranteeEmployeeRelationshipT; }
            set
            {
                m_validator.TryToWriteField("aBUsdaGuaranteeEmployeeRelationshipT", () => m_app.aBUsdaGuaranteeEmployeeRelationshipT, t => m_app.aBUsdaGuaranteeEmployeeRelationshipT = t, value);
            }
        }


        public E_aUsdaGuaranteeEmployeeRelationshipT aCUsdaGuaranteeEmployeeRelationshipT
        {
            get { return m_app.aCUsdaGuaranteeEmployeeRelationshipT; }
            set
            {
                m_validator.TryToWriteField("aCUsdaGuaranteeEmployeeRelationshipT", () => m_app.aCUsdaGuaranteeEmployeeRelationshipT, t => m_app.aCUsdaGuaranteeEmployeeRelationshipT = t, value);
            }
        }


        public string aBUsdaGuaranteeEmployeeRelationshipDesc
        {
            get { return m_app.aBUsdaGuaranteeEmployeeRelationshipDesc; }
            set
            {
                m_validator.TryToWriteField("aBUsdaGuaranteeEmployeeRelationshipDesc", () => m_app.aBUsdaGuaranteeEmployeeRelationshipDesc, t => m_app.aBUsdaGuaranteeEmployeeRelationshipDesc = t, value);
            }
        }


        public string aCUsdaGuaranteeEmployeeRelationshipDesc
        {
            get { return m_app.aCUsdaGuaranteeEmployeeRelationshipDesc; }
            set
            {
                m_validator.TryToWriteField("aCUsdaGuaranteeEmployeeRelationshipDesc", () => m_app.aCUsdaGuaranteeEmployeeRelationshipDesc, t => m_app.aCUsdaGuaranteeEmployeeRelationshipDesc = t, value);
            }
        }


        public bool aDenialWeObtainedCreditScoreFromCRA
        {
            get { return m_app.aDenialWeObtainedCreditScoreFromCRA; }
            set
            {
                m_validator.TryToWriteField("aDenialWeObtainedCreditScoreFromCRA", () => m_app.aDenialWeObtainedCreditScoreFromCRA, t => m_app.aDenialWeObtainedCreditScoreFromCRA = t, value);
            }
        }

        public bool aDenialCreditScoreLckd
        {
            get { return m_app.aDenialCreditScoreLckd; }
            set
            {
                m_validator.TryToWriteField("aDenialCreditScoreLckd", () => m_app.aDenialCreditScoreLckd, t => m_app.aDenialCreditScoreLckd = t, value);
            }
        }

        public string aDenialCreditScore_rep
        {
            get { return m_app.aDenialCreditScore_rep; }
            set
            {
                m_validator.TryToWriteField("aDenialCreditScore_rep", () => m_app.aDenialCreditScore_rep, t => m_app.aDenialCreditScore_rep = t, value);
            }
        }


        public string aDenialCreditScoreD_rep
        {
            get { return m_app.aDenialCreditScoreD_rep; }
            set
            {
                m_validator.TryToWriteField("aDenialCreditScoreD_rep", () => m_app.aDenialCreditScoreD_rep, t => m_app.aDenialCreditScoreD_rep = t, value);
            }
        }


        public string aDenialCreditScoreRangeFrom_rep
        {
            get { return m_app.aDenialCreditScoreRangeFrom_rep; }
            set
            {
                m_validator.TryToWriteField("aDenialCreditScoreRangeFrom_rep", () => m_app.aDenialCreditScoreRangeFrom_rep, t => m_app.aDenialCreditScoreRangeFrom_rep = t, value);
            }
        }


        public string aDenialCreditScoreRangeTo_rep
        {
            get { return m_app.aDenialCreditScoreRangeTo_rep; }
            set
            {
                m_validator.TryToWriteField("aDenialCreditScoreRangeTo_rep", () => m_app.aDenialCreditScoreRangeTo_rep, t => m_app.aDenialCreditScoreRangeTo_rep = t, value);
            }
        }


        public string aDenialCreditScoreFactor1
        {
            get { return m_app.aDenialCreditScoreFactor1; }
            set
            {
                m_validator.TryToWriteField("aDenialCreditScoreFactor1", () => m_app.aDenialCreditScoreFactor1, t => m_app.aDenialCreditScoreFactor1 = t, value);
            }
        }


        public string aDenialCreditScoreFactor2
        {
            get { return m_app.aDenialCreditScoreFactor2; }
            set
            {
                m_validator.TryToWriteField("aDenialCreditScoreFactor2", () => m_app.aDenialCreditScoreFactor2, t => m_app.aDenialCreditScoreFactor2 = t, value);
            }
        }


        public string aDenialCreditScoreFactor3
        {
            get { return m_app.aDenialCreditScoreFactor3; }
            set
            {
                m_validator.TryToWriteField("aDenialCreditScoreFactor3", () => m_app.aDenialCreditScoreFactor3, t => m_app.aDenialCreditScoreFactor3 = t, value);
            }
        }


        public string aDenialCreditScoreFactor4
        {
            get { return m_app.aDenialCreditScoreFactor4; }
            set
            {
                m_validator.TryToWriteField("aDenialCreditScoreFactor4", () => m_app.aDenialCreditScoreFactor4, t => m_app.aDenialCreditScoreFactor4 = t, value);
            }
        }


        public string aDenialCreditScoreFactorPrint
        {
            get { return m_app.aDenialCreditScoreFactorPrint; }
        }

        public string aCDenialCreditScoreFactorPrint
        {
            get { return m_app.aCDenialCreditScoreFactorPrint; }
        }

        public bool aDenialIsFactorNumberOfRecentInquiries
        {
            get { return m_app.aDenialIsFactorNumberOfRecentInquiries; }
            set
            {
                m_validator.TryToWriteField("aDenialIsFactorNumberOfRecentInquiries", () => m_app.aDenialIsFactorNumberOfRecentInquiries, t => m_app.aDenialIsFactorNumberOfRecentInquiries = t, value);
            }
        }

        public IPreparerFields aDenialCreditScoreContact
        {
            get { return m_app.aDenialCreditScoreContact; }
        }

        public bool aVaServedAltNameIs
        {
            get { return m_app.aVaServedAltNameIs; }
            set
            {
                m_validator.TryToWriteField("aVaServedAltNameIs", () => m_app.aVaServedAltNameIs, t => m_app.aVaServedAltNameIs = t, value);
            }
        }


        public string aVaServedAltName
        {
            get { return m_app.aVaServedAltName; }
            set
            {
                m_validator.TryToWriteField("aVaServedAltName", () => m_app.aVaServedAltName, t => m_app.aVaServedAltName = t, value);
            }
        }


        public string aVaServ1Title
        {
            get { return m_app.aVaServ1Title; }
            set
            {
                m_validator.TryToWriteField("aVaServ1Title", () => m_app.aVaServ1Title, t => m_app.aVaServ1Title = t, value);
            }
        }


        public string aVaServ2Title
        {
            get { return m_app.aVaServ2Title; }
            set
            {
                m_validator.TryToWriteField("aVaServ2Title", () => m_app.aVaServ2Title, t => m_app.aVaServ2Title = t, value);
            }
        }


        public string aVaServ3Title
        {
            get { return m_app.aVaServ3Title; }
            set
            {
                m_validator.TryToWriteField("aVaServ3Title", () => m_app.aVaServ3Title, t => m_app.aVaServ3Title = t, value);
            }
        }


        public string aVaServ4Title
        {
            get { return m_app.aVaServ4Title; }
            set
            {
                m_validator.TryToWriteField("aVaServ4Title", () => m_app.aVaServ4Title, t => m_app.aVaServ4Title = t, value);
            }
        }


        public string aVaServ5Title
        {
            get { return m_app.aVaServ5Title; }
            set
            {
                m_validator.TryToWriteField("aVaServ5Title", () => m_app.aVaServ5Title, t => m_app.aVaServ5Title = t, value);
            }
        }


        public string aVaServ6Title
        {
            get { return m_app.aVaServ6Title; }
            set
            {
                m_validator.TryToWriteField("aVaServ6Title", () => m_app.aVaServ6Title, t => m_app.aVaServ6Title = t, value);
            }
        }


        public string aVaServ7Title
        {
            get { return m_app.aVaServ7Title; }
            set
            {
                m_validator.TryToWriteField("aVaServ7Title", () => m_app.aVaServ7Title, t => m_app.aVaServ7Title = t, value);
            }
        }


        public string aVaServ5BranchNum
        {
            get { return m_app.aVaServ5BranchNum; }
            set
            {
                m_validator.TryToWriteField("aVaServ5BranchNum", () => m_app.aVaServ5BranchNum, t => m_app.aVaServ5BranchNum = t, value);
            }
        }


        public string aVaServ5StartD_rep
        {
            get { return m_app.aVaServ5StartD_rep; }
            set
            {
                m_validator.TryToWriteField("aVaServ5StartD_rep", () => m_app.aVaServ5StartD_rep, t => m_app.aVaServ5StartD_rep = t, value);
            }
        }


        public string aVaServ5EndD_rep
        {
            get { return m_app.aVaServ5EndD_rep; }
            set
            {
                m_validator.TryToWriteField("aVaServ5EndD_rep", () => m_app.aVaServ5EndD_rep, t => m_app.aVaServ5EndD_rep = t, value);
            }
        }


        public string aVaServ5Num
        {
            get { return m_app.aVaServ5Num; }
            set
            {
                m_validator.TryToWriteField("aVaServ5Num", () => m_app.aVaServ5Num, t => m_app.aVaServ5Num = t, value);
            }
        }


        public string aVaServ6BranchNum
        {
            get { return m_app.aVaServ6BranchNum; }
            set
            {
                m_validator.TryToWriteField("aVaServ6BranchNum", () => m_app.aVaServ6BranchNum, t => m_app.aVaServ6BranchNum = t, value);
            }
        }


        public string aVaServ6StartD_rep
        {
            get { return m_app.aVaServ6StartD_rep; }
            set
            {
                m_validator.TryToWriteField("aVaServ6StartD_rep", () => m_app.aVaServ6StartD_rep, t => m_app.aVaServ6StartD_rep = t, value);
            }
        }


        public string aVaServ6EndD_rep
        {
            get { return m_app.aVaServ6EndD_rep; }
            set
            {
                m_validator.TryToWriteField("aVaServ6EndD_rep", () => m_app.aVaServ6EndD_rep, t => m_app.aVaServ6EndD_rep = t, value);
            }
        }


        public string aVaServ6Num
        {
            get { return m_app.aVaServ6Num; }
            set
            {
                m_validator.TryToWriteField("aVaServ6Num", () => m_app.aVaServ6Num, t => m_app.aVaServ6Num = t, value);
            }
        }


        public string aVaServ7BranchNum
        {
            get { return m_app.aVaServ7BranchNum; }
            set
            {
                m_validator.TryToWriteField("aVaServ7BranchNum", () => m_app.aVaServ7BranchNum, t => m_app.aVaServ7BranchNum = t, value);
            }
        }


        public string aVaServ7StartD_rep
        {
            get { return m_app.aVaServ7StartD_rep; }
            set
            {
                m_validator.TryToWriteField("aVaServ7StartD_rep", () => m_app.aVaServ7StartD_rep, t => m_app.aVaServ7StartD_rep = t, value);
            }
        }


        public string aVaServ7EndD_rep
        {
            get { return m_app.aVaServ7EndD_rep; }
            set
            {
                m_validator.TryToWriteField("aVaServ7EndD_rep", () => m_app.aVaServ7EndD_rep, t => m_app.aVaServ7EndD_rep = t, value);
            }
        }


        public string aVaServ7Num
        {
            get { return m_app.aVaServ7Num; }
            set
            {
                m_validator.TryToWriteField("aVaServ7Num", () => m_app.aVaServ7Num, t => m_app.aVaServ7Num = t, value);
            }
        }

        public bool aVaOwnOtherVaLoanTReadOnly
        {
            get { return this.m_app.aVaOwnOtherVaLoanTReadOnly; }
        }

        public E_aVaOwnOtherVaLoanT aVaOwnOtherVaLoanT
        {
            get { return m_app.aVaOwnOtherVaLoanT; }
            set
            {
                m_validator.TryToWriteField("aVaOwnOtherVaLoanT", () => m_app.aVaOwnOtherVaLoanT, t => m_app.aVaOwnOtherVaLoanT = t, value);
            }
        }

        public bool aVaApplyOtherLoanIsCalculated
        {
            get { return this.m_app.aVaApplyOtherLoanIsCalculated; }
        }

        public E_TriState aVaApplyOneTimeRestorationTri
        {
            get { return m_app.aVaApplyOneTimeRestorationTri; }
            set
            {
                m_validator.TryToWriteField("aVaApplyOneTimeRestorationTri", () => m_app.aVaApplyOneTimeRestorationTri, t => m_app.aVaApplyOneTimeRestorationTri = t, value);
            }
        }


        public string aVaApplyOneTimeRestorationDateOfLoan
        {
            get { return m_app.aVaApplyOneTimeRestorationDateOfLoan; }
            set
            {
                m_validator.TryToWriteField("aVaApplyOneTimeRestorationDateOfLoan", () => m_app.aVaApplyOneTimeRestorationDateOfLoan, t => m_app.aVaApplyOneTimeRestorationDateOfLoan = t, value);
            }
        }


        public string aVaApplyOneTimeRestorationStAddr
        {
            get { return m_app.aVaApplyOneTimeRestorationStAddr; }
            set
            {
                m_validator.TryToWriteField("aVaApplyOneTimeRestorationStAddr", () => m_app.aVaApplyOneTimeRestorationStAddr, t => m_app.aVaApplyOneTimeRestorationStAddr = t, value);
            }
        }


        public string aVaApplyOneTimeRestorationCityState
        {
            get { return m_app.aVaApplyOneTimeRestorationCityState; }
            set
            {
                m_validator.TryToWriteField("aVaApplyOneTimeRestorationCityState", () => m_app.aVaApplyOneTimeRestorationCityState, t => m_app.aVaApplyOneTimeRestorationCityState = t, value);
            }
        }


        public E_TriState aVaApplyRegularRefiCashoutTri
        {
            get { return m_app.aVaApplyRegularRefiCashoutTri; }
            set
            {
                m_validator.TryToWriteField("aVaApplyRegularRefiCashoutTri", () => m_app.aVaApplyRegularRefiCashoutTri, t => m_app.aVaApplyRegularRefiCashoutTri = t, value);
            }
        }


        public string aVaApplyRegularRefiCashoutDateOfLoan
        {
            get { return m_app.aVaApplyRegularRefiCashoutDateOfLoan; }
            set
            {
                m_validator.TryToWriteField("aVaApplyRegularRefiCashoutDateOfLoan", () => m_app.aVaApplyRegularRefiCashoutDateOfLoan, t => m_app.aVaApplyRegularRefiCashoutDateOfLoan = t, value);
            }
        }


        public string aVaApplyRegularRefiCashoutStAddr
        {
            get { return m_app.aVaApplyRegularRefiCashoutStAddr; }
            set
            {
                m_validator.TryToWriteField("aVaApplyRegularRefiCashoutStAddr", () => m_app.aVaApplyRegularRefiCashoutStAddr, t => m_app.aVaApplyRegularRefiCashoutStAddr = t, value);
            }
        }


        public string aVaApplyRegularRefiCashoutCityState
        {
            get { return m_app.aVaApplyRegularRefiCashoutCityState; }
            set
            {
                m_validator.TryToWriteField("aVaApplyRegularRefiCashoutCityState", () => m_app.aVaApplyRegularRefiCashoutCityState, t => m_app.aVaApplyRegularRefiCashoutCityState = t, value);
            }
        }


        public E_TriState aVaApplyRefiNoCashoutTri
        {
            get { return m_app.aVaApplyRefiNoCashoutTri; }
            set
            {
                m_validator.TryToWriteField("aVaApplyRefiNoCashoutTri", () => m_app.aVaApplyRefiNoCashoutTri, t => m_app.aVaApplyRefiNoCashoutTri = t, value);
            }
        }


        public string aVaApplyRefiNoCashoutDateOfLoan
        {
            get { return m_app.aVaApplyRefiNoCashoutDateOfLoan; }
            set
            {
                m_validator.TryToWriteField("aVaApplyRefiNoCashoutDateOfLoan", () => m_app.aVaApplyRefiNoCashoutDateOfLoan, t => m_app.aVaApplyRefiNoCashoutDateOfLoan = t, value);
            }
        }


        public string aVaApplyRefiNoCashoutStAddr
        {
            get { return m_app.aVaApplyRefiNoCashoutStAddr; }
            set
            {
                m_validator.TryToWriteField("aVaApplyRefiNoCashoutStAddr", () => m_app.aVaApplyRefiNoCashoutStAddr, t => m_app.aVaApplyRefiNoCashoutStAddr = t, value);
            }
        }


        public string aVaApplyRefiNoCashoutCityState
        {
            get { return m_app.aVaApplyRefiNoCashoutCityState; }
            set
            {
                m_validator.TryToWriteField("aVaApplyRefiNoCashoutCityState", () => m_app.aVaApplyRefiNoCashoutCityState, t => m_app.aVaApplyRefiNoCashoutCityState = t, value);
            }
        }


        public E_TriState aVAFileClaimDisabilityCertifyTri
        {
            get { return m_app.aVAFileClaimDisabilityCertifyTri; }
            set
            {
                m_validator.TryToWriteField("aVAFileClaimDisabilityCertifyTri", () => m_app.aVAFileClaimDisabilityCertifyTri, t => m_app.aVAFileClaimDisabilityCertifyTri = t, value);
            }
        }


        public E_aHomeOwnershipCounselingSourceT aBHomeOwnershipCounselingSourceT
        {
            get { return m_app.aBHomeOwnershipCounselingSourceT; }
            set
            {
                m_validator.TryToWriteField("aBHomeOwnershipCounselingSourceT", () => m_app.aBHomeOwnershipCounselingSourceT, t => m_app.aBHomeOwnershipCounselingSourceT = t, value);
            }
        }


        public E_aHomeOwnershipCounselingFormatT aBHomeOwnershipCounselingFormatT
        {
            get { return m_app.aBHomeOwnershipCounselingFormatT; }
            set
            {
                m_validator.TryToWriteField("aBHomeOwnershipCounselingFormatT", () => m_app.aBHomeOwnershipCounselingFormatT, t => m_app.aBHomeOwnershipCounselingFormatT = t, value);
            }
        }


        public E_aHomeOwnershipCounselingSourceT aCHomeOwnershipCounselingSourceT
        {
            get { return m_app.aCHomeOwnershipCounselingSourceT; }
            set
            {
                m_validator.TryToWriteField("aCHomeOwnershipCounselingSourceT", () => m_app.aCHomeOwnershipCounselingSourceT, t => m_app.aCHomeOwnershipCounselingSourceT = t, value);
            }
        }


        public E_aHomeOwnershipCounselingFormatT aCHomeOwnershipCounselingFormatT
        {
            get { return m_app.aCHomeOwnershipCounselingFormatT; }
            set
            {
                m_validator.TryToWriteField("aCHomeOwnershipCounselingFormatT", () => m_app.aCHomeOwnershipCounselingFormatT, t => m_app.aCHomeOwnershipCounselingFormatT = t, value);
            }
        }


        public bool aCDenialNoCreditFile
        {
            get { return m_app.aCDenialNoCreditFile; }
            set
            {
                m_validator.TryToWriteField("aCDenialNoCreditFile", () => m_app.aCDenialNoCreditFile, t => m_app.aCDenialNoCreditFile = t, value);
            }
        }


        public bool aCDenialInsufficientCreditFile
        {
            get { return m_app.aCDenialInsufficientCreditFile; }
            set
            {
                m_validator.TryToWriteField("aCDenialInsufficientCreditFile", () => m_app.aCDenialInsufficientCreditFile, t => m_app.aCDenialInsufficientCreditFile = t, value);
            }
        }


        public bool aCDenialInsufficientCreditRef
        {
            get { return m_app.aCDenialInsufficientCreditRef; }
            set
            {
                m_validator.TryToWriteField("aCDenialInsufficientCreditRef", () => m_app.aCDenialInsufficientCreditRef, t => m_app.aCDenialInsufficientCreditRef = t, value);
            }
        }


        public bool aCDenialUnableVerifyCreditRef
        {
            get { return m_app.aCDenialUnableVerifyCreditRef; }
            set
            {
                m_validator.TryToWriteField("aCDenialUnableVerifyCreditRef", () => m_app.aCDenialUnableVerifyCreditRef, t => m_app.aCDenialUnableVerifyCreditRef = t, value);
            }
        }


        public bool aCDenialGarnishment
        {
            get { return m_app.aCDenialGarnishment; }
            set
            {
                m_validator.TryToWriteField("aCDenialGarnishment", () => m_app.aCDenialGarnishment, t => m_app.aCDenialGarnishment = t, value);
            }
        }


        public bool aCDenialExcessiveObligations
        {
            get { return m_app.aCDenialExcessiveObligations; }
            set
            {
                m_validator.TryToWriteField("aCDenialExcessiveObligations", () => m_app.aCDenialExcessiveObligations, t => m_app.aCDenialExcessiveObligations = t, value);
            }
        }


        public bool aCDenialInsufficientIncome
        {
            get { return m_app.aCDenialInsufficientIncome; }
            set
            {
                m_validator.TryToWriteField("aCDenialInsufficientIncome", () => m_app.aCDenialInsufficientIncome, t => m_app.aCDenialInsufficientIncome = t, value);
            }
        }


        public bool aCDenialUnacceptablePmtRecord
        {
            get { return m_app.aCDenialUnacceptablePmtRecord; }
            set
            {
                m_validator.TryToWriteField("aCDenialUnacceptablePmtRecord", () => m_app.aCDenialUnacceptablePmtRecord, t => m_app.aCDenialUnacceptablePmtRecord = t, value);
            }
        }


        public bool aCDenialLackOfCashReserves
        {
            get { return m_app.aCDenialLackOfCashReserves; }
            set
            {
                m_validator.TryToWriteField("aCDenialLackOfCashReserves", () => m_app.aCDenialLackOfCashReserves, t => m_app.aCDenialLackOfCashReserves = t, value);
            }
        }


        public bool aCDenialDeliquentCreditObligations
        {
            get { return m_app.aCDenialDeliquentCreditObligations; }
            set
            {
                m_validator.TryToWriteField("aCDenialDeliquentCreditObligations", () => m_app.aCDenialDeliquentCreditObligations, t => m_app.aCDenialDeliquentCreditObligations = t, value);
            }
        }


        public bool aCDenialBankruptcy
        {
            get { return m_app.aCDenialBankruptcy; }
            set
            {
                m_validator.TryToWriteField("aCDenialBankruptcy", () => m_app.aCDenialBankruptcy, t => m_app.aCDenialBankruptcy = t, value);
            }
        }


        public bool aCDenialInfoFromConsumerReportAgency
        {
            get { return m_app.aCDenialInfoFromConsumerReportAgency; }
            set
            {
                m_validator.TryToWriteField("aCDenialInfoFromConsumerReportAgency", () => m_app.aCDenialInfoFromConsumerReportAgency, t => m_app.aCDenialInfoFromConsumerReportAgency = t, value);
            }
        }


        public bool aCDenialUnableVerifyEmployment
        {
            get { return m_app.aCDenialUnableVerifyEmployment; }
            set
            {
                m_validator.TryToWriteField("aCDenialUnableVerifyEmployment", () => m_app.aCDenialUnableVerifyEmployment, t => m_app.aCDenialUnableVerifyEmployment = t, value);
            }
        }


        public bool aCDenialLenOfEmployment
        {
            get { return m_app.aCDenialLenOfEmployment; }
            set
            {
                m_validator.TryToWriteField("aCDenialLenOfEmployment", () => m_app.aCDenialLenOfEmployment, t => m_app.aCDenialLenOfEmployment = t, value);
            }
        }


        public bool aCDenialTemporaryEmployment
        {
            get { return m_app.aCDenialTemporaryEmployment; }
            set
            {
                m_validator.TryToWriteField("aCDenialTemporaryEmployment", () => m_app.aCDenialTemporaryEmployment, t => m_app.aCDenialTemporaryEmployment = t, value);
            }
        }


        public bool aCDenialInsufficientIncomeForMortgagePmt
        {
            get { return m_app.aCDenialInsufficientIncomeForMortgagePmt; }
            set
            {
                m_validator.TryToWriteField("aCDenialInsufficientIncomeForMortgagePmt", () => m_app.aCDenialInsufficientIncomeForMortgagePmt, t => m_app.aCDenialInsufficientIncomeForMortgagePmt = t, value);
            }
        }


        public bool aCDenialUnableVerifyIncome
        {
            get { return m_app.aCDenialUnableVerifyIncome; }
            set
            {
                m_validator.TryToWriteField("aCDenialUnableVerifyIncome", () => m_app.aCDenialUnableVerifyIncome, t => m_app.aCDenialUnableVerifyIncome = t, value);
            }
        }


        public bool aCDenialTempResidence
        {
            get { return m_app.aCDenialTempResidence; }
            set
            {
                m_validator.TryToWriteField("aCDenialTempResidence", () => m_app.aCDenialTempResidence, t => m_app.aCDenialTempResidence = t, value);
            }
        }


        public bool aCDenialShortResidencePeriod
        {
            get { return m_app.aCDenialShortResidencePeriod; }
            set
            {
                m_validator.TryToWriteField("aCDenialShortResidencePeriod", () => m_app.aCDenialShortResidencePeriod, t => m_app.aCDenialShortResidencePeriod = t, value);
            }
        }


        public bool aCDenialUnableVerifyResidence
        {
            get { return m_app.aCDenialUnableVerifyResidence; }
            set
            {
                m_validator.TryToWriteField("aCDenialUnableVerifyResidence", () => m_app.aCDenialUnableVerifyResidence, t => m_app.aCDenialUnableVerifyResidence = t, value);
            }
        }


        public bool aCDenialByHUD
        {
            get { return m_app.aCDenialByHUD; }
            set
            {
                m_validator.TryToWriteField("aCDenialByHUD", () => m_app.aCDenialByHUD, t => m_app.aCDenialByHUD = t, value);
            }
        }


        public bool aCDenialByVA
        {
            get { return m_app.aCDenialByVA; }
            set
            {
                m_validator.TryToWriteField("aCDenialByVA", () => m_app.aCDenialByVA, t => m_app.aCDenialByVA = t, value);
            }
        }


        public bool aCDenialByFedNationalMortAssoc
        {
            get { return m_app.aCDenialByFedNationalMortAssoc; }
            set
            {
                m_validator.TryToWriteField("aCDenialByFedNationalMortAssoc", () => m_app.aCDenialByFedNationalMortAssoc, t => m_app.aCDenialByFedNationalMortAssoc = t, value);
            }
        }


        public bool aCDenialByFedHomeLoanMortCorp
        {
            get { return m_app.aCDenialByFedHomeLoanMortCorp; }
            set
            {
                m_validator.TryToWriteField("aCDenialByFedHomeLoanMortCorp", () => m_app.aCDenialByFedHomeLoanMortCorp, t => m_app.aCDenialByFedHomeLoanMortCorp = t, value);
            }
        }


        public bool aCDenialByOther
        {
            get { return m_app.aCDenialByOther; }
            set
            {
                m_validator.TryToWriteField("aCDenialByOther", () => m_app.aCDenialByOther, t => m_app.aCDenialByOther = t, value);
            }
        }


        public string aCDenialByOtherDesc
        {
            get { return m_app.aCDenialByOtherDesc; }
            set
            {
                m_validator.TryToWriteField("aCDenialByOtherDesc", () => m_app.aCDenialByOtherDesc, t => m_app.aCDenialByOtherDesc = t, value);
            }
        }


        public bool aCDenialInsufficientFundsToClose
        {
            get { return m_app.aCDenialInsufficientFundsToClose; }
            set
            {
                m_validator.TryToWriteField("aCDenialInsufficientFundsToClose", () => m_app.aCDenialInsufficientFundsToClose, t => m_app.aCDenialInsufficientFundsToClose = t, value);
            }
        }


        public bool aCDenialCreditAppIncomplete
        {
            get { return m_app.aCDenialCreditAppIncomplete; }
            set
            {
                m_validator.TryToWriteField("aCDenialCreditAppIncomplete", () => m_app.aCDenialCreditAppIncomplete, t => m_app.aCDenialCreditAppIncomplete = t, value);
            }
        }


        public bool aCDenialInadequateCollateral
        {
            get { return m_app.aCDenialInadequateCollateral; }
            set
            {
                m_validator.TryToWriteField("aCDenialInadequateCollateral", () => m_app.aCDenialInadequateCollateral, t => m_app.aCDenialInadequateCollateral = t, value);
            }
        }


        public bool aCDenialUnacceptableProp
        {
            get { return m_app.aCDenialUnacceptableProp; }
            set
            {
                m_validator.TryToWriteField("aCDenialUnacceptableProp", () => m_app.aCDenialUnacceptableProp, t => m_app.aCDenialUnacceptableProp = t, value);
            }
        }


        public bool aCDenialInsufficientPropData
        {
            get { return m_app.aCDenialInsufficientPropData; }
            set
            {
                m_validator.TryToWriteField("aCDenialInsufficientPropData", () => m_app.aCDenialInsufficientPropData, t => m_app.aCDenialInsufficientPropData = t, value);
            }
        }


        public bool aCDenialUnacceptableAppraisal
        {
            get { return m_app.aCDenialUnacceptableAppraisal; }
            set
            {
                m_validator.TryToWriteField("aCDenialUnacceptableAppraisal", () => m_app.aCDenialUnacceptableAppraisal, t => m_app.aCDenialUnacceptableAppraisal = t, value);
            }
        }


        public bool aCDenialUnacceptableLeasehold
        {
            get { return m_app.aCDenialUnacceptableLeasehold; }
            set
            {
                m_validator.TryToWriteField("aCDenialUnacceptableLeasehold", () => m_app.aCDenialUnacceptableLeasehold, t => m_app.aCDenialUnacceptableLeasehold = t, value);
            }
        }


        public bool aCDenialNotGrantCreditToAnyAppOnSuchTermsAndConds
        {
            get { return m_app.aCDenialNotGrantCreditToAnyAppOnSuchTermsAndConds; }
            set
            {
                m_validator.TryToWriteField("aCDenialNotGrantCreditToAnyAppOnSuchTermsAndConds", () => m_app.aCDenialNotGrantCreditToAnyAppOnSuchTermsAndConds, t => m_app.aCDenialNotGrantCreditToAnyAppOnSuchTermsAndConds = t, value);
            }
        }


        public bool aCDenialWithdrawnByApp
        {
            get { return m_app.aCDenialWithdrawnByApp; }
            set
            {
                m_validator.TryToWriteField("aCDenialWithdrawnByApp", () => m_app.aCDenialWithdrawnByApp, t => m_app.aCDenialWithdrawnByApp = t, value);
            }
        }


        public bool aCDenialOtherReason1
        {
            get { return m_app.aCDenialOtherReason1; }
            set
            {
                m_validator.TryToWriteField("aCDenialOtherReason1", () => m_app.aCDenialOtherReason1, t => m_app.aCDenialOtherReason1 = t, value);
            }
        }


        public string aCDenialOtherReason1Desc
        {
            get { return m_app.aCDenialOtherReason1Desc; }
            set
            {
                m_validator.TryToWriteField("aCDenialOtherReason1Desc", () => m_app.aCDenialOtherReason1Desc, t => m_app.aCDenialOtherReason1Desc = t, value);
            }
        }


        public bool aCDenialOtherReason2
        {
            get { return m_app.aCDenialOtherReason2; }
            set
            {
                m_validator.TryToWriteField("aCDenialOtherReason2", () => m_app.aCDenialOtherReason2, t => m_app.aCDenialOtherReason2 = t, value);
            }
        }


        public string aCDenialOtherReason2Desc
        {
            get { return m_app.aCDenialOtherReason2Desc; }
            set
            {
                m_validator.TryToWriteField("aCDenialOtherReason2Desc", () => m_app.aCDenialOtherReason2Desc, t => m_app.aCDenialOtherReason2Desc = t, value);
            }
        }


        public bool aCDenialDecisionBasedOnReportAgency
        {
            get { return m_app.aCDenialDecisionBasedOnReportAgency; }
            set
            {
                m_validator.TryToWriteField("aCDenialDecisionBasedOnReportAgency", () => m_app.aCDenialDecisionBasedOnReportAgency, t => m_app.aCDenialDecisionBasedOnReportAgency = t, value);
            }
        }


        public bool aCDenialDecisionBasedOnCRA
        {
            get { return m_app.aCDenialDecisionBasedOnCRA; }
            set
            {
                m_validator.TryToWriteField("aCDenialDecisionBasedOnCRA", () => m_app.aCDenialDecisionBasedOnCRA, t => m_app.aCDenialDecisionBasedOnCRA = t, value);
            }
        }


        public bool aCDenialWeObtainedCreditScoreFromCRA
        {
            get { return m_app.aCDenialWeObtainedCreditScoreFromCRA; }
            set
            {
                m_validator.TryToWriteField("aCDenialWeObtainedCreditScoreFromCRA", () => m_app.aCDenialWeObtainedCreditScoreFromCRA, t => m_app.aCDenialWeObtainedCreditScoreFromCRA = t, value);
            }
        }

        public bool aCDenialCreditScoreLckd
        {
            get { return m_app.aCDenialCreditScoreLckd; }
            set
            {
                m_validator.TryToWriteField("aCDenialCreditScoreLckd", () => m_app.aCDenialCreditScoreLckd, t => m_app.aCDenialCreditScoreLckd = t, value);
            }
        }

        public string aCDenialCreditScore_rep
        {
            get { return m_app.aCDenialCreditScore_rep; }
            set
            {
                m_validator.TryToWriteField("aCDenialCreditScore_rep", () => m_app.aCDenialCreditScore_rep, t => m_app.aCDenialCreditScore_rep = t, value);
            }
        }


        public string aCDenialCreditScoreD_rep
        {
            get { return m_app.aCDenialCreditScoreD_rep; }
            set
            {
                m_validator.TryToWriteField("aCDenialCreditScoreD_rep", () => m_app.aCDenialCreditScoreD_rep, t => m_app.aCDenialCreditScoreD_rep = t, value);
            }
        }


        public string aCDenialCreditScoreRangeFrom_rep
        {
            get { return m_app.aCDenialCreditScoreRangeFrom_rep; }
            set
            {
                m_validator.TryToWriteField("aCDenialCreditScoreRangeFrom_rep", () => m_app.aCDenialCreditScoreRangeFrom_rep, t => m_app.aCDenialCreditScoreRangeFrom_rep = t, value);
            }
        }


        public string aCDenialCreditScoreRangeTo_rep
        {
            get { return m_app.aCDenialCreditScoreRangeTo_rep; }
            set
            {
                m_validator.TryToWriteField("aCDenialCreditScoreRangeTo_rep", () => m_app.aCDenialCreditScoreRangeTo_rep, t => m_app.aCDenialCreditScoreRangeTo_rep = t, value);
            }
        }


        public string aCDenialCreditScoreFactor1
        {
            get { return m_app.aCDenialCreditScoreFactor1; }
            set
            {
                m_validator.TryToWriteField("aCDenialCreditScoreFactor1", () => m_app.aCDenialCreditScoreFactor1, t => m_app.aCDenialCreditScoreFactor1 = t, value);
            }
        }


        public string aCDenialCreditScoreFactor2
        {
            get { return m_app.aCDenialCreditScoreFactor2; }
            set
            {
                m_validator.TryToWriteField("aCDenialCreditScoreFactor2", () => m_app.aCDenialCreditScoreFactor2, t => m_app.aCDenialCreditScoreFactor2 = t, value);
            }
        }


        public string aCDenialCreditScoreFactor3
        {
            get { return m_app.aCDenialCreditScoreFactor3; }
            set
            {
                m_validator.TryToWriteField("aCDenialCreditScoreFactor3", () => m_app.aCDenialCreditScoreFactor3, t => m_app.aCDenialCreditScoreFactor3 = t, value);
            }
        }


        public string aCDenialCreditScoreFactor4
        {
            get { return m_app.aCDenialCreditScoreFactor4; }
            set
            {
                m_validator.TryToWriteField("aCDenialCreditScoreFactor4", () => m_app.aCDenialCreditScoreFactor4, t => m_app.aCDenialCreditScoreFactor4 = t, value);
            }
        }


        public bool aCDenialIsFactorNumberOfRecentInquiries
        {
            get { return m_app.aCDenialIsFactorNumberOfRecentInquiries; }
            set
            {
                m_validator.TryToWriteField("aCDenialIsFactorNumberOfRecentInquiries", () => m_app.aCDenialIsFactorNumberOfRecentInquiries, t => m_app.aCDenialIsFactorNumberOfRecentInquiries = t, value);
            }
        }

        public IPreparerFields aCDenialCreditScoreContact
        {
            get { return m_app.aCDenialCreditScoreContact; }
        }

        public bool aBHasHousingHist
        {
            get { return m_app.aBHasHousingHist; }
            set
            {
                m_validator.TryToWriteField("aBHasHousingHist", () => m_app.aBHasHousingHist, t => m_app.aBHasHousingHist = t, value);
            }
        }


        public bool aCHasHousingHist
        {
            get { return m_app.aCHasHousingHist; }
            set
            {
                m_validator.TryToWriteField("aCHasHousingHist", () => m_app.aCHasHousingHist, t => m_app.aCHasHousingHist = t, value);
            }
        }

        public bool aIsBVAElig
        {
            get
            {
                return m_app.aIsBVAElig;
            }

            set
            {
                m_validator.TryToWriteField("aIsBVAElig", () => m_app.aIsBVAElig, t => m_app.aIsBVAElig = t, value);
            }
        }
        
        public bool aIsCVAElig
        {
            get
            {
                return m_app.aIsCVAElig;
            }

            set
            {
                m_validator.TryToWriteField("aIsCVAElig", () => m_app.aIsCVAElig, t => m_app.aIsCVAElig = t, value);
            }
        }

        public bool aIsBVAFFEx
        {
            get { return m_app.aIsBVAFFEx; }
            set
            {
                m_validator.TryToWriteField("aIsBVAFFEx", () => m_app.aIsBVAFFEx, t => m_app.aIsBVAFFEx = t, value);
            }
        }


        public bool aIsCVAFFEx
        {
            get { return m_app.aIsCVAFFEx; }
            set
            {
                m_validator.TryToWriteField("aIsCVAFFEx", () => m_app.aIsCVAFFEx, t => m_app.aIsCVAFFEx = t, value);
            }
        }


        public CDateTime aCCrRd
        {
            get { return m_app.aCCrRd; }
            set
            {
                m_validator.TryToWriteField("aCCrRd", () => m_app.aCCrRd, t => m_app.aCCrRd = t, value);
            }
        }


        public string aCCrRd_rep
        {
            get { return m_app.aCCrRd_rep; }
            set
            {
                m_validator.TryToWriteField("aCCrRd_rep", () => m_app.aCCrRd_rep, t => m_app.aCCrRd_rep = t, value);
            }
        }


        public bool aCDenialHasAdditionalStatement
        {
            get { return m_app.aCDenialHasAdditionalStatement; }
            set
            {
                m_validator.TryToWriteField("aCDenialHasAdditionalStatement", () => m_app.aCDenialHasAdditionalStatement, t => m_app.aCDenialHasAdditionalStatement = t, value);
            }
        }


        public string aCDenialAdditionalStatement
        {
            get { return m_app.aCDenialAdditionalStatement; }
            set
            {
                m_validator.TryToWriteField("aCDenialAdditionalStatement", () => m_app.aCDenialAdditionalStatement, t => m_app.aCDenialAdditionalStatement = t, value);
            }
        }

        public E_IdType aBIdT
        {
            get { return m_app.aBIdT; }
            set
            {
                m_validator.TryToWriteField("aBIdT", () => m_app.aBIdT, t => m_app.aBIdT = t, value);
            }
        }
        public string aBIdState
        {
            get { return m_app.aBIdState; }
            set
            {
                m_validator.TryToWriteField("aBIdState", () => m_app.aBIdState, t => m_app.aBIdState = t, value);
            }
        }
        public string aBIdNumber
        {
            get { return m_app.aBIdNumber; }
            set
            {
                m_validator.TryToWriteField("aBIdNumber", () => m_app.aBIdNumber, t => m_app.aBIdNumber = t, value);
            }
        }
        public string aBIdIssueD_rep
        {
            get { return m_app.aBIdIssueD_rep; }
            set
            {
                m_validator.TryToWriteField("aBIdIssueD_rep", () => m_app.aBIdIssueD_rep, t => m_app.aBIdIssueD_rep = t, value);
            }
        }

        public CDateTime aBIdIssueD
        {
            get { return m_app.aBIdIssueD; }
            set
            {
                m_validator.TryToWriteField("aBIdIssueD", () => m_app.aBIdIssueD, t => m_app.aBIdIssueD = t, value);
            }
        }
        public CDateTime aBIdExpireD
        {
            get { return m_app.aBIdExpireD; }
            set
            {
                m_validator.TryToWriteField("aBIdExpireD", () => m_app.aBIdExpireD, t => m_app.aBIdExpireD = t, value);
            }
        }

        public string aBIdExpireD_rep
        {
            get { return m_app.aBIdExpireD_rep; }
            set
            {
                m_validator.TryToWriteField("aBIdExpireD_rep", () => m_app.aBIdExpireD_rep, t => m_app.aBIdExpireD_rep = t, value);
            }
        }
        public string aBIdCountry
        {
            get { return m_app.aBIdCountry; }
            set
            {
                m_validator.TryToWriteField("aBIdCountry", () => m_app.aBIdCountry, t => m_app.aBIdCountry = t, value);
            }
        }
        public string aBIdGovBranch
        {
            get { return m_app.aBIdGovBranch; }
            set
            {
                m_validator.TryToWriteField("aBIdGovBranch", () => m_app.aBIdGovBranch, t => m_app.aBIdGovBranch = t, value);
            }
        }
        public string aBIdOther
        {
            get { return m_app.aBIdOther; }
            set
            {
                m_validator.TryToWriteField("aBIdOther", () => m_app.aBIdOther, t => m_app.aBIdOther = t, value);
            }
        }

        public E_IdType aB2ndIdT
        {
            get { return m_app.aB2ndIdT; }
            set
            {
                m_validator.TryToWriteField("aB2ndIdT", () => m_app.aB2ndIdT, t => m_app.aB2ndIdT = t, value);
            }
        }
        public string aB2ndIdState
        {
            get { return m_app.aB2ndIdState; }
            set
            {
                m_validator.TryToWriteField("aB2ndIdState", () => m_app.aB2ndIdState, t => m_app.aB2ndIdState = t, value);
            }
        }
        public string aB2ndIdNumber
        {
            get { return m_app.aB2ndIdNumber; }
            set
            {
                m_validator.TryToWriteField("aB2ndIdNumber", () => m_app.aB2ndIdNumber, t => m_app.aB2ndIdNumber = t, value);
            }
        }
        public string aB2ndIdIssueD_rep
        {
            get { return m_app.aB2ndIdIssueD_rep; }
            set
            {
                m_validator.TryToWriteField("aB2ndIdIssueD_rep", () => m_app.aB2ndIdIssueD_rep, t => m_app.aB2ndIdIssueD_rep = t, value);
            }
        }

        public CDateTime aB2ndIdIssueD
        {
            get { return m_app.aB2ndIdIssueD; }
            set
            {
                m_validator.TryToWriteField("aB2ndIdIssueD", () => m_app.aB2ndIdIssueD, t => m_app.aB2ndIdIssueD = t, value);
            }
        }
        public CDateTime aB2ndIdExpireD
        {
            get { return m_app.aB2ndIdExpireD; }
            set
            {
                m_validator.TryToWriteField("aB2ndIdExpireD", () => m_app.aB2ndIdExpireD, t => m_app.aB2ndIdExpireD = t, value);
            }
        }

        public string aB2ndIdExpireD_rep
        {
            get { return m_app.aB2ndIdExpireD_rep; }
            set
            {
                m_validator.TryToWriteField("aB2ndIdExpireD_rep", () => m_app.aB2ndIdExpireD_rep, t => m_app.aB2ndIdExpireD_rep = t, value);
            }
        }
        public string aB2ndIdCountry
        {
            get { return m_app.aB2ndIdCountry; }
            set
            {
                m_validator.TryToWriteField("aB2ndIdCountry", () => m_app.aB2ndIdCountry, t => m_app.aB2ndIdCountry = t, value);
            }
        }
        public string aB2ndIdGovBranch
        {
            get { return m_app.aB2ndIdGovBranch; }
            set
            {
                m_validator.TryToWriteField("aB2ndIdGovBranch", () => m_app.aB2ndIdGovBranch, t => m_app.aB2ndIdGovBranch = t, value);
            }
        }
        public string aB2ndIdOther
        {
            get { return m_app.aB2ndIdOther; }
            set
            {
                m_validator.TryToWriteField("aB2ndIdOther", () => m_app.aB2ndIdOther, t => m_app.aB2ndIdOther = t, value);
            }
        }

        public E_IdType aCIdT
        {
            get { return m_app.aCIdT; }
            set
            {
                m_validator.TryToWriteField("aCIdT", () => m_app.aCIdT, t => m_app.aCIdT = t, value);
            }
        }
        public string aCIdState
        {
            get { return m_app.aCIdState; }
            set
            {
                m_validator.TryToWriteField("aCIdState", () => m_app.aCIdState, t => m_app.aCIdState = t, value);
            }
        }
        public string aCIdNumber
        {
            get { return m_app.aCIdNumber; }
            set
            {
                m_validator.TryToWriteField("aCIdNumber", () => m_app.aCIdNumber, t => m_app.aCIdNumber = t, value);
            }
        }
        public CDateTime aCIdIssueD
        {
            get { return m_app.aCIdIssueD; }
            set
            {
                m_validator.TryToWriteField("aCIdIssueD", () => m_app.aCIdIssueD, t => m_app.aCIdIssueD = t, value);
            }
        }

        public string aCIdIssueD_rep
        {
            get { return m_app.aCIdIssueD_rep; }
            set
            {
                m_validator.TryToWriteField("aCIdIssueD_rep", () => m_app.aCIdIssueD_rep, t => m_app.aCIdIssueD_rep = t, value);
            }
        }

        public CDateTime aCIdExpireD
        {
            get { return m_app.aCIdExpireD; }
            set
            {
                m_validator.TryToWriteField("aCIdExpireD", () => m_app.aCIdExpireD, t => m_app.aCIdExpireD = t, value);
            }
        }

        public string aCIdExpireD_rep
        {
            get { return m_app.aCIdExpireD_rep; }
            set
            {
                m_validator.TryToWriteField("aCIdExpireD_rep", () => m_app.aCIdExpireD_rep, t => m_app.aCIdExpireD_rep = t, value);
            }
        }

        public string aCIdCountry
        {
            get { return m_app.aCIdCountry; }
            set
            {
                m_validator.TryToWriteField("aCIdCountry", () => m_app.aCIdCountry, t => m_app.aCIdCountry = t, value);
            }
        }
        public string aCIdGovBranch
        {
            get { return m_app.aCIdGovBranch; }
            set
            {
                m_validator.TryToWriteField("aCIdGovBranch", () => m_app.aCIdGovBranch, t => m_app.aCIdGovBranch = t, value);
            }
        }
        public string aCIdOther
        {
            get { return m_app.aCIdOther; }
            set
            {
                m_validator.TryToWriteField("aCIdOther", () => m_app.aCIdOther, t => m_app.aCIdOther = t, value);
            }
        }

        public E_IdType aC2ndIdT
        {
            get { return m_app.aC2ndIdT; }
            set
            {
                m_validator.TryToWriteField("aC2ndIdT", () => m_app.aC2ndIdT, t => m_app.aC2ndIdT = t, value);
            }
        }
        public string aC2ndIdState
        {
            get { return m_app.aC2ndIdState; }
            set
            {
                m_validator.TryToWriteField("aC2ndIdState", () => m_app.aC2ndIdState, t => m_app.aC2ndIdState = t, value);
            }
        }
        public string aC2ndIdNumber
        {
            get { return m_app.aC2ndIdNumber; }
            set
            {
                m_validator.TryToWriteField("aC2ndIdNumber", () => m_app.aC2ndIdNumber, t => m_app.aC2ndIdNumber = t, value);
            }
        }
        public CDateTime aC2ndIdIssueD
        {
            get { return m_app.aC2ndIdIssueD; }
            set
            {
                m_validator.TryToWriteField("aC2ndIdIssueD", () => m_app.aC2ndIdIssueD, t => m_app.aC2ndIdIssueD = t, value);
            }
        }

        public string aC2ndIdIssueD_rep
        {
            get { return m_app.aC2ndIdIssueD_rep; }
            set
            {
                m_validator.TryToWriteField("aC2ndIdIssueD_rep", () => m_app.aC2ndIdIssueD_rep, t => m_app.aC2ndIdIssueD_rep = t, value);
            }
        }

        public CDateTime aC2ndIdExpireD
        {
            get { return m_app.aC2ndIdExpireD; }
            set
            {
                m_validator.TryToWriteField("aC2ndIdExpireD", () => m_app.aC2ndIdExpireD, t => m_app.aC2ndIdExpireD = t, value);
            }
        }

        public string aC2ndIdExpireD_rep
        {
            get { return m_app.aC2ndIdExpireD_rep; }
            set
            {
                m_validator.TryToWriteField("aC2ndIdExpireD_rep", () => m_app.aC2ndIdExpireD_rep, t => m_app.aC2ndIdExpireD_rep = t, value);
            }
        }

        public string aC2ndIdCountry
        {
            get { return m_app.aC2ndIdCountry; }
            set
            {
                m_validator.TryToWriteField("aC2ndIdCountry", () => m_app.aC2ndIdCountry, t => m_app.aC2ndIdCountry = t, value);
            }
        }
        public string aC2ndIdGovBranch
        {
            get { return m_app.aC2ndIdGovBranch; }
            set
            {
                m_validator.TryToWriteField("aC2ndIdGovBranch", () => m_app.aC2ndIdGovBranch, t => m_app.aC2ndIdGovBranch = t, value);
            }
        }
        public string aC2ndIdOther
        {
            get { return m_app.aC2ndIdOther; }
            set
            {
                m_validator.TryToWriteField("aC2ndIdOther", () => m_app.aC2ndIdOther, t => m_app.aC2ndIdOther = t, value);
            }
        }

        public string aIdCountry
        {
            get { return m_app.aIdCountry; }
            set
            {
                m_validator.TryToWriteField("aIdCountry", () => m_app.aIdCountry, t => m_app.aIdCountry = t, value);
            }
        }

        public CDateTime aIdExpireD
        {
            get { return m_app.aIdExpireD; }
            set
            {
                m_validator.TryToWriteField("aIdExpireD", () => m_app.aIdExpireD, t => m_app.aIdExpireD = t, value);
            }
        }

        public string aIdExpireD_rep
        {
            get { return m_app.aIdExpireD_rep; }
            set
            {
                m_validator.TryToWriteField("aIdExpireD_rep", () => m_app.aIdExpireD_rep, t => m_app.aIdExpireD_rep = t, value);
            }
        }

        public string aIdGovBranch
        {
            get { return m_app.aIdGovBranch; }
            set
            {
                m_validator.TryToWriteField("aIdGovBranch", () => m_app.aIdGovBranch, t => m_app.aIdGovBranch = t, value);
            }
        }

        public CDateTime aIdIssueD
        {
            get { return m_app.aIdIssueD; }
            set
            {
                m_validator.TryToWriteField("aIdIssueD", () => m_app.aIdIssueD, t => m_app.aIdIssueD = t, value);
            }
        }

        public string aIdIssueD_rep
        {
            get { return m_app.aIdIssueD_rep; }
            set
            {
                m_validator.TryToWriteField("aIdIssueD_rep", () => m_app.aIdIssueD_rep, t => m_app.aIdIssueD_rep = t, value);
            }
        }

        public string aIdNumber
        {
            get { return m_app.aIdNumber; }
            set
            {
                m_validator.TryToWriteField("aIdNumber", () => m_app.aIdNumber, t => m_app.aIdNumber = t, value);
            }
        }

        public string aIdOther
        {
            get { return m_app.aIdOther; }
            set
            {
                m_validator.TryToWriteField("aIdOther", () => m_app.aIdOther, t => m_app.aIdOther = t, value);
            }
        }

        public string aIdState
        {
            get { return m_app.aIdState; }
            set
            {
                m_validator.TryToWriteField("aIdState", () => m_app.aIdState, t => m_app.aIdState = t, value);
            }
        }

        public E_IdType aIdT
        {
            get { return m_app.aIdT; }
            set
            {
                m_validator.TryToWriteField("aIdT", () => m_app.aIdT, t => m_app.aIdT = t, value);
            }
        }

        public string a2ndIdCountry
        {
            get { return m_app.a2ndIdCountry; }
            set
            {
                m_validator.TryToWriteField("a2ndIdCountry", () => m_app.a2ndIdCountry, t => m_app.a2ndIdCountry = t, value);
            }
        }

        public CDateTime a2ndIdExpireD
        {
            get { return m_app.a2ndIdExpireD; }
            set
            {
                m_validator.TryToWriteField("a2ndIdExpireD", () => m_app.a2ndIdExpireD, t => m_app.a2ndIdExpireD = t, value);
            }
        }

        public string a2ndIdExpireD_rep
        {
            get { return m_app.a2ndIdExpireD_rep; }
            set
            {
                m_validator.TryToWriteField("a2ndIdExpireD_rep", () => m_app.a2ndIdExpireD_rep, t => m_app.a2ndIdExpireD_rep = t, value);
            }
        }

        public string a2ndIdGovBranch
        {
            get { return m_app.a2ndIdGovBranch; }
            set
            {
                m_validator.TryToWriteField("a2ndIdGovBranch", () => m_app.a2ndIdGovBranch, t => m_app.a2ndIdGovBranch = t, value);
            }
        }

        public CDateTime a2ndIdIssueD
        {
            get { return m_app.a2ndIdIssueD; }
            set
            {
                m_validator.TryToWriteField("a2ndIdIssueD", () => m_app.a2ndIdIssueD, t => m_app.a2ndIdIssueD = t, value);
            }
        }

        public string a2ndIdIssueD_rep
        {
            get { return m_app.a2ndIdIssueD_rep; }
            set
            {
                m_validator.TryToWriteField("a2ndIdIssueD_rep", () => m_app.a2ndIdIssueD_rep, t => m_app.a2ndIdIssueD_rep = t, value);
            }
        }

        public string a2ndIdNumber
        {
            get { return m_app.a2ndIdNumber; }
            set
            {
                m_validator.TryToWriteField("a2ndIdNumber", () => m_app.a2ndIdNumber, t => m_app.a2ndIdNumber = t, value);
            }
        }

        public string a2ndIdOther
        {
            get { return m_app.a2ndIdOther; }
            set
            {
                m_validator.TryToWriteField("a2ndIdOther", () => m_app.a2ndIdOther, t => m_app.a2ndIdOther = t, value);
            }
        }

        public string a2ndIdState
        {
            get { return m_app.a2ndIdState; }
            set
            {
                m_validator.TryToWriteField("a2ndIdState", () => m_app.a2ndIdState, t => m_app.a2ndIdState = t, value);
            }
        }

        public E_IdType a2ndIdT
        {
            get { return m_app.a2ndIdT; }
            set
            {
                m_validator.TryToWriteField("a2ndIdT", () => m_app.a2ndIdT, t => m_app.a2ndIdT = t, value);
            }
        }

        public string aIdResolution
        {
            get { return m_app.aIdResolution; }
            set
            {
                m_validator.TryToWriteField("aIdResolution", () => m_app.aIdResolution, t => m_app.aIdResolution = t, value);
            }
        }

        /// <summary>
        /// This removes blanks and redundant aliases.  It loads from aBAlias1-6 if it isn't assigned to.
        /// </summary>
        public IEnumerable<string> aBAliases
        {
            get { return m_app.aBAliases; }
            set
            {
                m_validator.TryToWriteField("aBAliases", () => m_app.aBAliases, t => m_app.aBAliases = t, value, new IEnumerableComparer<string>());
            }
        }

        /// <summary>
        /// This removes blanks and redundant aliases.  It loads from aCAlias1-6 if it isn't assigned to.
        /// </summary>
        public IEnumerable<string> aCAliases
        {
            get { return m_app.aCAliases; }
            set
            {
                m_validator.TryToWriteField("aCAliases", () => m_app.aCAliases, t => m_app.aCAliases = t, value, new IEnumerableComparer<string>());
            }
        }

        public string aBAliases_SingleLine
        {
            get { return m_app.aBAliases_SingleLine; }
        }

        public string aCAliases_SingleLine
        {
            get { return m_app.aCAliases_SingleLine; }
        }

        //public string aBAlias1
        //{
        //    get { return m_app.aBAlias1; }
        //    //set
        //    //{
        //    //    m_validator.TryToWriteField("aBAlias1", () => m_app.aBAlias1, t => m_app.aBAlias1 = t, value);
        //    //}
        //}

        //public string aBAlias2
        //{
        //    get { return m_app.aBAlias2; }
        //    //set
        //    //{
        //    //    m_validator.TryToWriteField("aBAlias2", () => m_app.aBAlias2, t => m_app.aBAlias2 = t, value);
        //    //}
        //}

        //public string aBAlias3
        //{
        //    get { return m_app.aBAlias3; }
        //    //set
        //    //{
        //    //    m_validator.TryToWriteField("aBAlias3", () => m_app.aBAlias3, t => m_app.aBAlias3 = t, value);
        //    //}
        //}

        //public string aBAlias4
        //{
        //    get { return m_app.aBAlias4; }
        //    //set
        //    //{
        //    //    m_validator.TryToWriteField("aBAlias4", () => m_app.aBAlias4, t => m_app.aBAlias4 = t, value);
        //    //}
        //}

        //public string aBAlias5
        //{
        //    get { return m_app.aBAlias5; }
        //    //set
        //    //{
        //    //    m_validator.TryToWriteField("aBAlias5", () => m_app.aBAlias5, t => m_app.aBAlias5 = t, value);
        //    //}
        //}

        //public string aBAlias6
        //{
        //    get { return m_app.aBAlias6; }
        //    //set
        //    //{
        //    //    m_validator.TryToWriteField("aBAlias6", () => m_app.aBAlias6, t => m_app.aBAlias6 = t, value);
        //    //}
        //}

        public string aBPowerOfAttorneyNm
        {
            get { return m_app.aBPowerOfAttorneyNm; }
            set
            {
                m_validator.TryToWriteField("aBPowerOfAttorneyNm", () => m_app.aBPowerOfAttorneyNm, t => m_app.aBPowerOfAttorneyNm = t, value);
            }
        }

        //public string aCAlias1
        //{
        //    get { return m_app.aCAlias1; }
        //    //set
        //    //{
        //    //    m_validator.TryToWriteField("aCAlias1", () => m_app.aCAlias1, t => m_app.aCAlias1 = t, value);
        //    //}
        //}

        //public string aCAlias2
        //{
        //    get { return m_app.aCAlias2; }
        //    //set
        //    //{
        //    //    m_validator.TryToWriteField("aCAlias2", () => m_app.aCAlias2, t => m_app.aCAlias2 = t, value);
        //    //}
        //}

        //public string aCAlias3
        //{
        //    get { return m_app.aCAlias3; }
        //    //set
        //    //{
        //    //    m_validator.TryToWriteField("aCAlias3", () => m_app.aCAlias3, t => m_app.aCAlias3 = t, value);
        //    //}
        //}

        //public string aCAlias4
        //{
        //    get { return m_app.aCAlias4; }
        //    //set
        //    //{
        //    //    m_validator.TryToWriteField("aCAlias4", () => m_app.aCAlias4, t => m_app.aCAlias4 = t, value);
        //    //}
        //}

        //public string aCAlias5
        //{
        //    get { return m_app.aCAlias5; }
        //    //set
        //    //{
        //    //    m_validator.TryToWriteField("aCAlias5", () => m_app.aCAlias5, t => m_app.aCAlias5 = t, value);
        //    //}
        //}

        //public string aCAlias6
        //{
        //    get { return m_app.aCAlias6; }
        //    //set
        //    //{
        //    //    m_validator.TryToWriteField("aCAlias6", () => m_app.aCAlias6, t => m_app.aCAlias6 = t, value);
        //    //}
        //}

        public string aCPowerOfAttorneyNm
        {
            get { return m_app.aCPowerOfAttorneyNm; }
            set
            {
                m_validator.TryToWriteField("aCPowerOfAttorneyNm", () => m_app.aCPowerOfAttorneyNm, t => m_app.aCPowerOfAttorneyNm = t, value);
            }
        }

        public LosConvert m_convertLos
        {
            get { return m_app.m_convertLos; }
        }

        public CDateTime aBEConsentReceivedD
        {
            get { return m_app.aBEConsentReceivedD; }
            set
            {
                m_validator.TryToWriteField("aBEConsentReceivedD", () => m_app.aBEConsentReceivedD, t => m_app.aBEConsentReceivedD = t, value);
            }
        }
        public string aBEConsentReceivedD_rep
        {
            get { return m_app.aBEConsentReceivedD_rep; }
        }
        
        public CDateTime aCEConsentReceivedD
        {
            get { return m_app.aCEConsentReceivedD; }
            set
            {
                m_validator.TryToWriteField("aCEConsentReceivedD", () => m_app.aCEConsentReceivedD, t => m_app.aCEConsentReceivedD = t, value);
            }
        }
        public string aCEConsentReceivedD_rep
        {
            get { return m_app.aCEConsentReceivedD_rep; }
        }

        public CDateTime aBEConsentDeclinedD
        {
            get { return m_app.aBEConsentDeclinedD; }
            set
            {
                m_validator.TryToWriteField("aBEConsentDeclinedD", () => m_app.aBEConsentDeclinedD, t => m_app.aBEConsentDeclinedD = t, value);
            }
        }
        public string aBEConsentDeclinedD_rep
        {
            get { return m_app.aBEConsentDeclinedD_rep; }
        }

        public CDateTime aCEConsentDeclinedD
        {
            get { return m_app.aCEConsentDeclinedD; }
            set
            {
                m_validator.TryToWriteField("aCEConsentDeclinedD", () => m_app.aCEConsentDeclinedD, t => m_app.aCEConsentDeclinedD = t, value);
            }
        }
        public string aCEConsentDeclinedD_rep
        {
            get { return m_app.aCEConsentDeclinedD_rep; }
        }
        public bool aIsFannieTestBorrower
        {
            get { return m_app.aIsFannieTestBorrower; }
        }

        public bool aBTotalScoreIsFthbReadOnly
        {
            get { return this.m_app.aBTotalScoreIsFthbReadOnly; }
        }

        public bool aCTotalScoreIsFthbReadOnly
        {
            get { return this.m_app.aCTotalScoreIsFthbReadOnly; }
        }

        public bool aBorrowerAddressSameAsSubjectProperty
        {
            get { return m_app.aBorrowerAddressSameAsSubjectProperty; }
        }
        #endregion

        #region methods
        public decimal aBorrIncomeTotalX(string parameters)
        {
            return m_app.aBorrIncomeTotalX(parameters);
        }
        public void ImportCreditScoresFromCreditReport()
        {
            m_app.ImportCreditScoresFromCreditReport();
        }
        public void ImportLiabilitiesFromCreditReport(bool bSkipZeroBalance, bool bImportBorrowerInfo)
        {
            m_app.ImportLiabilitiesFromCreditReport(bSkipZeroBalance, bImportBorrowerInfo);
        }
        public void ImportLiabilitiesFromCreditReport(bool bSkipZeroBalance, bool bImportBorrowerInfo, bool bImportIdFromCreditTradeline)
        {
            m_app.ImportLiabilitiesFromCreditReport(bSkipZeroBalance, bImportBorrowerInfo, bImportIdFromCreditTradeline);
        }
        public void ImportPublicRecordsFromCreditReport()
        {
            m_app.ImportPublicRecordsFromCreditReport();
        }
        public Guid[] FindMatchedLiabilities(Guid idReo)
        {
            return m_app.FindMatchedLiabilities(idReo);
        }
        public void FhaMcawApplyAppFieldsFrom1003()
        {
            m_app.FhaMcawApplyAppFieldsFrom1003();
        }
        public void InvalidateCache()
        {
            m_app.InvalidateCache();
        }
        public void DelMarriedCobor()
        {
            if (this.m_enforceAccessControl
                && !this.m_validator.HasFullWrite)
            {
                throw new DeleteSpousePermissionDenied();
            }

            m_app.DelMarriedCobor();
        }
        public void SwapMarriedBorAndCobor()
        {
            if (this.m_enforceAccessControl
                && !this.m_validator.HasFullWrite)
            {
                throw new SwapBorrowerAndSpousePermissionDenied();
            }

            m_app.SwapMarriedBorAndCobor();
        }
        public int MaxEmploymentGapWithin(int Months)
        {
            return m_app.MaxEmploymentGapWithin(Months);
        }
        public bool ValidateJobHistory(StringBuilder userMessage)
        {
            return m_app.ValidateJobHistory(userMessage);
        }
        public ArrayList CompareLiaCollectionWithCreditReport()
        {
            return m_app.CompareLiaCollectionWithCreditReport();
        }

        public IEnumerable<E_aProdCitizenT> GetApplicableCitizenshipValues(E_BorrowerModeT borrower)
        {
            return this.m_app.GetApplicableCitizenshipValues(borrower);
        }

        #endregion

        #region
        internal void Flush()
        {
            m_app.Flush();
        }

        public ICreditReport GetOptimisticCreditReport()
        {
            return CreditReportFactory.ConstructOptimisticCreditReport(m_app, null);
        }

        public string aVaPastLXmlContent
        {
            get
            {
                return m_app.aVaPastLXmlContent;
            }
        }

        public string aVorXmlContent
        {
            get
            {
                return m_app.aVorXmlContent;
            }
        }

        public Sensitive<string> aAssetXmlContent
        {
            get
            {
                return m_app.aAssetXmlContent;
            }
        }

        public string aReXmlContent
        {
            get
            {
                return m_app.aReXmlContent;
            }
        }

        internal void AcceptNewVaPastLCollection(string xml)
        {
            m_app.AcceptNewVaPastLCollection(xml);
        }

        public void AcceptNewBEmplmtCollection(string xml)
        {
            m_app.AcceptNewBEmplmtCollection(xml);
        }

        public void AcceptNewCEmplmtCollection(string xml)
        {
            m_app.AcceptNewCEmplmtCollection(xml);
        }

        public void AcceptNewAssetCollection(string xml)
        {
            m_app.AcceptNewAssetCollection(xml);
        }

        public void AcceptNewReCollection(string xml)
        {
            m_app.AcceptNewReCollection(xml);
        }

        public void SetLoanDataOn(CreditReportProxy proxy)
        {
            proxy.SetLoanData(m_app);
        }

        #endregion

        #region collections 


        public IVerificationOfRentCollection aVorCollection
        {
            get
            {
                return m_app.aVorCollection;
            }
        }

        public void RegisterCollection(string collectionName, DataSet ds)
        {
            if (false == m_collections.ContainsKey(collectionName))
            {
                m_collections.Add(collectionName, ds.Copy());
            }
        }

        public Sensitive<string> aLiaXmlContent
        {
            get
            {
                return m_app.aLiaXmlContent;
            }
        }

        /// <summary>
        /// Added for the use of the migration code from xml data to collection table.
        /// </summary>
        public string aPublicRecordXmlContent
        {
            get
            {
                return m_app.aPublicRecordXmlContent;
            }
        }

        public void AcceptNewLiaCollection(string xml)
        {
            m_app.AcceptNewLiaCollection(xml);
        }

        public int GetVorRecordCount()
        {
            return m_app.GetVorRecordCount();
        }
        public IVerificationOfRent GetVorFields(int iRecord)
        {
            return this.aVorCollection.GetRecordByIndex(iRecord);
        }
        public IVerificationOfRent GetVorFields(Guid recordID)
        {
            return this.aVorCollection.GetRecordByRecordId(recordID);
        }

        public IVaPastLoanCollection aVaPastLCollection
        {
            get
            {
                IVaPastLoanCollection col = m_app.aVaPastLCollection;

                if (this.LoanData.sBorrowerApplicationCollectionT == E_sLqbCollectionT.Legacy)
                {
                    SetupChangeTrackingForCollection("CVaPastLCollection", col);
                }

                return col;
            }
        }

        public IAssetCollection aAssetCollection
        {
            get
            {
                IAssetCollection col = m_app.aAssetCollection;

                if (this.LoanData.sBorrowerApplicationCollectionT == E_sLqbCollectionT.Legacy)
                {
                    SetupChangeTrackingForCollection("CAssetCollection", col);
                }

                return col;
            }
        }


        public ILiaCollection aLiaCollection
        {
            get
            {
                ILiaCollection col = m_app.aLiaCollection;

                if (this.LoanData.sBorrowerApplicationCollectionT == E_sLqbCollectionT.Legacy)
                {
                    SetupChangeTrackingForCollection("CLiaCollection", col);
                }

                return col;
            }
        }

        public IReCollection aReCollection
        {
            get 
            { 
                var col = m_app.aReCollection;

                if (this.LoanData.sBorrowerApplicationCollectionT == E_sLqbCollectionT.Legacy)
                {
                    SetupChangeTrackingForCollection("CReCollection", col);
                }

                return col;
            }
        }

        
        public IEmpCollection aCEmpCollection
        {
            get 
            { 
                IEmpCollection col = m_app.aCEmpCollection;

                if (this.LoanData.sBorrowerApplicationCollectionT == E_sLqbCollectionT.Legacy)
                {
                    SetupChangeTrackingForCollection("CEmpCollection", "CCEmpCollection", col);
                }

                return col;
            }
        }

        public IEmpCollection aBEmpCollection
        {
            get
            {
                IEmpCollection col = m_app.aBEmpCollection;

                if (this.LoanData.sBorrowerApplicationCollectionT == E_sLqbCollectionT.Legacy)
                {
                    SetupChangeTrackingForCollection("CEmpCollection", "CBEmpCollection", col);
                }

                return col;
            }
        }

        public List<VerbalVerificationOfEmploymentRecord> aVerbalVerificationsOfEmployment
        {
            get
            {
                return m_app.aVerbalVerificationsOfEmployment;
            }
            set
            {
                 m_validator.TryToWriteField("aVerbalVerificationsOfEmployment", () => m_app.aVerbalVerificationsOfEmployment, t => m_app.aVerbalVerificationsOfEmployment = t, value);
            }
        }

        public bool aBIsVeteran
        {
            get { return m_app.aBIsVeteran; }
            set
            {
                m_validator.TryToWriteField("aBIsVeteran", () => m_app.aBIsVeteran, t => m_app.aBIsVeteran = t, value);
            }
        }

        public bool aCIsVeteran
        {
            get { return m_app.aCIsVeteran; }
            set
            {
                m_validator.TryToWriteField("aCIsVeteran", () => m_app.aCIsVeteran, t => m_app.aCIsVeteran = t, value);
            }
        }

        public bool aIsVeteran
        {
            get { return m_app.aIsVeteran; }
        }

        public CDateTime a1003InterviewD
        {
            get { return m_app.a1003InterviewD; }
            set { m_validator.TryToWriteField(nameof(a1003InterviewD), () => m_app.a1003InterviewD, t => m_app.a1003InterviewD = t, value); }
        }

        public string a1003InterviewD_rep
        {
            get { return m_app.a1003InterviewD_rep; }
            set { m_validator.TryToWriteField(nameof(a1003InterviewD_rep), () => m_app.a1003InterviewD_rep, t => m_app.a1003InterviewD_rep = t, value); }
        }

        public bool a1003InterviewDLckd
        {
            get { return m_app.a1003InterviewDLckd; }
            set { m_validator.TryToWriteField(nameof(a1003InterviewDLckd), () => m_app.a1003InterviewDLckd, t => m_app.a1003InterviewDLckd = t, value); }
        }

        public string aBNmFirstEnteredD_rep
        {
            get { return this.m_app.aBNmFirstEnteredD_rep; }
        }

        public string aCNmFirstEnteredD_rep
        {
            get { return this.m_app.aCNmFirstEnteredD_rep; }
        }

        public string aBSsnFirstEnteredD_rep
        {
            get { return this.m_app.aBSsnFirstEnteredD_rep; }
        }

        public string aCSsnFirstEnteredD_rep
        {
            get { return this.m_app.aCSsnFirstEnteredD_rep; }
        }

        public void Locka1003InterviewDToValueWithVersionBypass(CDateTime value)
        {
            this.m_app.Locka1003InterviewDToValueWithVersionBypass(value);
        }

        public void CopyRespaFirstEnteredDates(CAppData source)
        {
            this.m_app.CopyRespaFirstEnteredDates(source.m_app);
        }

        private void SetupChangeTrackingForCollection(string name, IRecordCollection collection)
        {
            SetupChangeTrackingForCollection(name, name, collection);
        }

        public void UpdateCollectionData( string collectionName, string mappingName, DataSet ds)
        {
            m_validator.UpdateCollection(collectionName, m_collections[mappingName], ds);

            if (m_validator.IsDisabled == false)
            {
                // 1/31/2012 dd - Only execute this block if field validator is enable.
                if (collectionName.Equals("CAssetCollection", StringComparison.OrdinalIgnoreCase))
                {
                    #region Create Asset_<Type>_Added trigger
                    var currentDictionary = ConvertToDictionary(ds);

                    // 1/5/2012 dd - We are only interest in new asset.
                    DataSet originalDs = m_collections[mappingName];
                    foreach (DataRow originalDatarow in originalDs.Tables[0].Rows)
                    {
                        if (originalDatarow["IsEmptyCreated"].ToString() == "True")
                        {
                            var recordid = new Guid(originalDatarow["RecordId"].ToString());

                            DataRow currentRow;

                            if (currentDictionary.TryGetValue(recordid, out currentRow) == true)
                            {
                                if (currentRow["IsEmptyCreated"].ToString() == "False")
                                {
                                    E_AssetT assetType = (E_AssetT)Enum.Parse(typeof(E_AssetT), currentRow["AssetT"].ToString());
                                    TaskTriggerQueueItem trigger = new TaskTriggerQueueItem(0, "Asset_" + assetType.ToString() + "_Added", this.LoanData.sBrokerId, this.LoanData.sLId, this.aAppId, recordid);

                                    this.LoanData.AddHardcodeTaskTrigger(trigger);

                                }
                            }
                        }
                    }
                    #endregion

                }
                else if (collectionName.Equals("CReCollection", StringComparison.OrdinalIgnoreCase))
                {
                    #region Create REO_Incomplete trigger
                    var currentDictionary = ConvertToDictionary(ds);

                    DataSet originalDs = m_collections[mappingName];
                    foreach (DataRow originalDatarow in originalDs.Tables[0].Rows)
                    {
                        if (originalDatarow["IsEmptyCreated"].ToString() == "True")
                        {
                            var recordid = new Guid(originalDatarow["RecordId"].ToString());

                            DataRow currentRow;

                            if (currentDictionary.TryGetValue(recordid, out currentRow) == true)
                            {
                                if (currentRow["IsEmptyCreated"].ToString() == "False")
                                {
                                    string addr = currentRow["Addr"].ToString();
                                    string city = currentRow["City"].ToString();
                                    string state = currentRow["State"].ToString();
                                    string zip = currentRow["Zip"].ToString();
                                    string type = currentRow["Type"].ToString();
                                    string val = currentRow["Val"].ToString();

                                    if (string.IsNullOrEmpty(addr) ||
                                        string.IsNullOrEmpty(city) ||
                                        string.IsNullOrEmpty(state) ||
                                        string.IsNullOrEmpty(zip) ||
                                        string.IsNullOrEmpty(type) ||
                                        string.IsNullOrEmpty(val) ||
                                        val == "$0.00")
                                    {

                                        TaskTriggerQueueItem trigger = new TaskTriggerQueueItem(0, "REO_Incomplete", this.LoanData.sBrokerId, this.LoanData.sLId, this.aAppId, recordid);

                                        this.LoanData.AddHardcodeTaskTrigger(trigger);
                                    }

                                }
                            }
                        }
                    }
                    #endregion
                }
                else if (collectionName.Equals("CLiaCollection", StringComparison.OrdinalIgnoreCase))
                {
                    #region Create Liability_To_Be_Paid trigger
                    var currentDictionary = ConvertToDictionary(ds);

                    DataSet originalDs = m_collections[mappingName];
                    //Tools.LogInfo(ds.GetXml());
                    foreach (DataRow originalDatarow in originalDs.Tables[0].Rows)
                    {
                        string originalWillBePdOff = originalDatarow["WillBePdOff"].ToString();
                        var recordid = new Guid(originalDatarow["RecordId"].ToString());

                        if (originalWillBePdOff == "" || originalWillBePdOff == "False")
                        {
                            DataRow currentRow;

                            if (currentDictionary.TryGetValue(recordid, out currentRow) == true)
                            {
                                if (currentRow["WillBePdOff"].ToString() == "True")
                                {
                                    TaskTriggerQueueItem trigger = new TaskTriggerQueueItem(0, "Liability_To_Be_Paid", this.LoanData.sBrokerId, this.LoanData.sLId, this.aAppId, recordid);

                                    this.LoanData.AddHardcodeTaskTrigger(trigger);
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
        }

        /// <summary>
        /// Attempts to get the data in the location specified by the element.
        /// </summary>
        /// <param name="element">The location of the data being requested.</param>
        /// <returns>The data that was requested.</returns>
        public object GetElement(IDataPathElement element)
        {
            string name = element.Name;

            var classType = PageDataUtilities.GetDataClassType(name);

            if (classType.HasValue && classType.Value == E_PageDataClassType.CAppBase)
            {
                return PageDataUtilities.GetValue(null, this, name);
            }
            else
            {
                throw new ArgumentException("Could not find field '" + name + "' in CAppData");
            }
        }

        private Dictionary<Guid, DataRow> ConvertToDictionary(DataSet ds)
        {
            if (ds == null)
            {
                return null;
            }
            if (ds.Tables.Count > 1)
            {
                throw new CBaseException(ErrorMessages.Generic, "ConvertToDictionary only handle dataset with one datatable.");
            }
            Dictionary<Guid, DataRow> dictionary = new Dictionary<Guid, DataRow>();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                object recordId = row["RecordId"];
                if (recordId != null)
                {
                    dictionary.Add(new Guid(recordId.ToString()), row);
                }
            }
            return dictionary;
        }

        private void SetupChangeTrackingForCollection(string collectionName, string mappingName, IRecordCollection collection)
        {
            // If we're on the new data layer, then we're going to track this at the loan level.
            if (this.LoanData.sBorrowerApplicationCollectionT != E_sLqbCollectionT.Legacy)
            {
                return;
            }

            collection.SetFlushFunction((modSet) => UpdateCollectionData(collectionName, mappingName ,modSet));
            if (false == m_collections.ContainsKey(mappingName))
            {
                DataSet ds = collection.GetDataSet().Copy();
                m_collections.Add(mappingName, ds);
            }
        }

        Dictionary<string, DataSet> m_collections = new Dictionary<string, DataSet>(StringComparer.OrdinalIgnoreCase);

        #endregion
    }
}
