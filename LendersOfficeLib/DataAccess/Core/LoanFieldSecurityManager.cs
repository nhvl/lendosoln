﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web.Caching;
using System.Threading;

using LendersOffice.Security;
using LendersOffice.Constants;

namespace DataAccess
{
    #region class SecureField
    public class SecureField
    {
        public SecureField(string sName, bool bHasPermLockDesk, bool bHasPermCloser, bool bHasPermAccountant, bool bHasPermInvestorInfo)
        {
            Name = sName;
            HasPermLockDesk = bHasPermLockDesk;
            HasPermCloser = bHasPermCloser;
            HasPermAccountant = bHasPermAccountant;
            HasPermInvestorInfo = bHasPermInvestorInfo;
        }

        public string Name
        {
            get;
            set;
        }
        public bool HasPermLockDesk
        {
            get;
            set;
        }
        public bool HasPermCloser
        {
            get;
            set;
        }
        public bool HasPermAccountant
        {
            get;
            set;
        }
        public bool HasPermInvestorInfo
        {
            get;
            set;
        }
    }
    #endregion

    public class LoanFieldSecurityManager
    {       

        #region private members
        private static ReaderWriterLockSlim m_dictionaryLock = new ReaderWriterLockSlim();
        private static Dictionary<string, SecureField> m_dictSecureFields = null;       
       
        private static void InvalidateCache()
        {            
            m_dictionaryLock.EnterWriteLock();
            try
            {
                if (m_dictSecureFields != null)
                    m_dictSecureFields.Clear();
            }
            finally
            {
                m_dictSecureFields = null;
                m_dictionaryLock.ExitWriteLock();                
            }
        }

        private static void LoadSecureFieldsDictionary()
        {
            if (m_dictSecureFields == null)
            {
                m_dictionaryLock.EnterWriteLock();
                try
                {
                    m_dictSecureFields = new Dictionary<string, SecureField>();
                    DataTable dt = GetSecureLoanFieldsDataTable();
                    foreach (DataRow row in dt.Rows)
                    {
                        string sName = row["sFieldId"].ToString();
                        bool bLockDeskPerm = (bool)row["bLockDeskPerm"];
                        bool bCloserPerm = (bool)row["bCloserPerm"];
                        bool bAccountantPerm = (bool)row["bAccountantPerm"];
                        bool bInvestorInfoPerm = (bool)row["bInvestorInfoPerm"];
                        m_dictSecureFields.Add(sName, new SecureField(sName, bLockDeskPerm, bCloserPerm, bAccountantPerm, bInvestorInfoPerm));
                    }
                }
                catch
                {
                    if (m_dictSecureFields != null)
                        m_dictSecureFields.Clear();

                    m_dictSecureFields = null;
                }
                finally
                {
                    m_dictionaryLock.ExitWriteLock();                    
                }
            }
        }
        #endregion

        public static DataTable GetSecureLoanFieldsDataTable()
        {
            return StoredProcedureHelper.ExecuteDataTable(DataSrc.LOShareROnly, "GetPerFieldSecurityEntries");          
        }

        public static void AddSecureLoanField(string sField, string sDesc, bool bLDesc, bool bCloser, bool bAcc, bool bInvInfo)
        {
            SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("FieldId", sField),
                new SqlParameter("Description", sDesc),
                new SqlParameter("LockDeskPerm", bLDesc),
                new SqlParameter("CloserPerm", bCloser),
                new SqlParameter("AccountantPerm", bAcc),
                new SqlParameter("InvestorInfoPerm", bInvInfo)
            };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "InsertPerFieldSecurityEntry", 3, parameters);

            InvalidateCache();
        }

        public static void RemoveSecureLoanField(string field)
        {
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "RemovePerFieldSecurityEntry", 3, new SqlParameter("FieldId", field));

            InvalidateCache();
        }

        public static bool IsSecureField(string sFieldName)
        {
            if(m_dictSecureFields == null)
                LoadSecureFieldsDictionary();

            if (m_dictSecureFields == null)
                return false;
            else
                return m_dictSecureFields.ContainsKey(sFieldName);
        }


        public static bool CanReadField(string sFieldname)
        {
            return CanReadField(sFieldname, BrokerUserPrincipal.CurrentPrincipal);
        }        
        public static bool CanReadField(string sFieldName, AbstractUserPrincipal userPrincipal)
        {
            bool bHasPerm = false;
            try
            {
                if (m_dictSecureFields == null)
                    LoadSecureFieldsDictionary();

                if (m_dictSecureFields == null)
                {
                    return true;
                }
                else
                {
                    if (m_dictSecureFields.ContainsKey(sFieldName))
                    {
                        SecureField field = m_dictSecureFields[sFieldName];

                        //if a secure field doesn't have any access permission, treat it as a normal field
                        if (!(field.HasPermLockDesk || field.HasPermCloser || field.HasPermAccountant || field.HasPermInvestorInfo))
                            return true;

                        //check if the user has access to the restricted field.
                        //If a secure field is accessible to multiple folders, then a user who has access to any of the folder will have access to the field
                        if (
                            (field.HasPermLockDesk ? (HasUserPermission(Permission.AllowLockDeskRead, userPrincipal) || HasUserPermission(Permission.AllowLockDeskWrite, userPrincipal)) : true)
                            && (field.HasPermCloser ? (HasUserPermission(Permission.AllowCloserRead, userPrincipal) || HasUserPermission(Permission.AllowCloserWrite, userPrincipal)) : true)
                            && (field.HasPermAccountant ? (HasUserPermission(Permission.AllowAccountantRead, userPrincipal) || HasUserPermission(Permission.AllowAccountantWrite, userPrincipal)) : true)
                            && (field.HasPermInvestorInfo ? (HasUserPermission(Permission.AllowViewingInvestorInformation, userPrincipal) || HasUserPermission(Permission.AllowEditingInvestorInformation, userPrincipal)): true)
                        )
                        {
                            bHasPerm = true;
                        }
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch
            {
                return true;
            }
            return bHasPerm;            
        }
        private static bool HasUserPermission(Permission perm)
        {
            return BrokerUserPrincipal.CurrentPrincipal.HasPermission(perm);
        }
        private static bool HasUserPermission(Permission perm, AbstractUserPrincipal userPrincipal)
        {
            if (userPrincipal == null)
                return BrokerUserPrincipal.CurrentPrincipal.HasPermission(perm);
            else
                return userPrincipal.HasPermission(perm);
        }
        public static bool CanWriteField(string sFieldname)
        {
            return CanWriteField(sFieldname, BrokerUserPrincipal.CurrentPrincipal);
        }
        public static bool CanWriteField(string sFieldName, AbstractUserPrincipal userPrincipal)
        {
            bool bHasPerm = false;
            try
            {
                if (m_dictSecureFields == null)
                    LoadSecureFieldsDictionary();

                if (m_dictSecureFields == null)
                {
                    return true;
                }
                else
                {
                    if (m_dictSecureFields.ContainsKey(sFieldName))
                    {
                        SecureField field = m_dictSecureFields[sFieldName];

                        //if a secure field doesn't have any access permission, treat it as a normal field
                        if (!(field.HasPermLockDesk || field.HasPermCloser || field.HasPermAccountant || field.HasPermInvestorInfo))
                            return true;

                        //check if the user has access to the restricted field
                        //If a secure field is accessible to multiple folders, then a user who has access to any of the folder will have access to the field
                        if (
                            (field.HasPermLockDesk ? HasUserPermission(Permission.AllowLockDeskWrite, userPrincipal) : true)
                            && (field.HasPermCloser ? HasUserPermission(Permission.AllowCloserWrite, userPrincipal) : true)
                            && (field.HasPermAccountant ? HasUserPermission(Permission.AllowAccountantWrite, userPrincipal) : true)
                            && (field.HasPermInvestorInfo ? HasUserPermission(Permission.AllowEditingInvestorInformation, userPrincipal) : true)
                        )
                        {
                            bHasPerm = true;
                        }                        
                    }
                }
            }
            catch
            {
                return true;
            }
            return bHasPerm;
        }

        public static SecureField SecureFieldPermissions(string sFieldName)
        {
            return SecureFieldPermissions(sFieldName, BrokerUserPrincipal.CurrentPrincipal);
        }

        public static SecureField SecureFieldPermissions(string sFieldName, AbstractUserPrincipal userPrincipal)
        {
            SecureField field = null;
            try
            {
                if (m_dictSecureFields == null)
                    LoadSecureFieldsDictionary();

                if (m_dictSecureFields != null)
                {
                    if (m_dictSecureFields.ContainsKey(sFieldName))
                    {
                        field = m_dictSecureFields[sFieldName];
                    }
                    else
                    {
                        field = new SecureField(sFieldName, false, false, false, false);
                    }
                }
            }
            catch{}

            return field;
        }

        public static void CheckFieldReadPermission(string sFieldName)
        {
            if (IsSecureField(sFieldName) && !CanReadField(sFieldName))
            {
                string sDeveloperMsg = "User does not have permission to read the loan field - '" + sFieldName + "'\n" + FieldPermissionsMsg(sFieldName);                

                throw new PermissionException("You do not have permission to access this loan data", sDeveloperMsg);
            }
        }
        public static void CheckFieldWritePermission(string sFieldName)
        {
            if (IsSecureField(sFieldName) && !CanWriteField(sFieldName))
            {
                string sDeveloperMsg = "User does not have permission to write the loan field - '" + sFieldName + "'\n" + FieldPermissionsMsg(sFieldName);
                
                throw new PermissionException("You do not have permission to access this loan data", sDeveloperMsg);
            }
        }
        private static string FieldPermissionsMsg(string sFieldName)
        {
            string sMsg = string.Empty;
            SecureField field = SecureFieldPermissions(sFieldName);
            if (field != null)
            {                
                string sPermissions = string.Join(",", new string[] { field.HasPermLockDesk ? "Lock Desk" : string.Empty, field.HasPermAccountant ? "Accountant" : string.Empty, field.HasPermCloser ? "Closer" : string.Empty, field.HasPermInvestorInfo ? "Investor Info" : string.Empty });
                sMsg += "This field is only accessible to " + sPermissions;
            }
            return sMsg;
        }
    }
}
