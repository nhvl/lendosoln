﻿namespace DataAccess.Core.Construction
{
    /// <summary>
    /// This is a MISMO field used to indicate if the loan will have one or two closings.
    /// </summary>
    public enum ConstructionToPermanentClosingType
    {
        Blank = 0,
        OneClosing = 1,
        TwoClosing = 2,
        Other = 3
    }
}
