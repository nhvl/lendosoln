﻿namespace DataAccess.Core.Construction
{
    /// <summary>
    /// Enum representing the current owner of the lot for a construction loan.
    /// </summary>
    public enum LotOwnerType
    {
        Blank = 0,
        Borrower = 1,
        Builder = 2,
        Other = 3
    }
}
