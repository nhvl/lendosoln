﻿namespace DataAccess.Core.Construction
{
    /// <summary>
    /// This field indicates if the interim construction financing and the permanent financing will be disclosed on separate disclosures or combined on a single disclosure. 
    /// </summary>
    public enum ConstructionDisclosureType
    {
        Blank = 0,
        Combined = 1,
        Separate = 2
    }
}
