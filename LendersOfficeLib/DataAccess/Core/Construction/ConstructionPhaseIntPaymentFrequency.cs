﻿namespace DataAccess.Core.Construction
{
    /// <summary>
    /// This field indicates when the payments will be due for the interim construction financing.
    /// </summary>
    public enum ConstructionPhaseIntPaymentFrequency
    {
        Blank = 0,
        Biweekly = 1,
        Monthly = 2,
        Other = 3
    }
}
