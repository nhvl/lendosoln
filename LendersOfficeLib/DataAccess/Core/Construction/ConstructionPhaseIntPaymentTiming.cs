﻿namespace DataAccess.Core.Construction
{
    /// <summary>
    /// This field indicates when the interest payments will be made.It is used to determine the payment stream and calculate the APR. 
    /// </summary>
    public enum ConstructionPhaseIntPaymentTiming
    {
        Blank = 0,
        InterestPaidAtEndOfConstruction = 1,
        InterestPaidPeriodically = 2,
        Other = 3
    }
}
