﻿namespace DataAccess.Core.Construction
{
    /// <summary>
    /// Loan Purpose that will be on the TRID disclosures and AUS for Construction Loans. 
    /// </summary>
    public enum ConstructionPurpose
    {
        Blank = 0,
        ConstructionAndLotPurchase = 1,
        ConstructionAndLotRefinance = 2,
        ConstructionOnOwnedLot = 3,
    }
}
