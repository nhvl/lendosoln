﻿namespace DataAccess.Core.Construction
{
    /// <summary>
    /// This indicates how the construction is performed.It is used for product eligibility.
    /// </summary>
    public enum ConstructionMethod
    {
        Blank = 0,
        Manufactured = 1,
        MobileHome = 2,
        Modular = 3,
        OnFrameModular = 4,
        Other = 5,
        SiteBuilt = 6
    }
}
