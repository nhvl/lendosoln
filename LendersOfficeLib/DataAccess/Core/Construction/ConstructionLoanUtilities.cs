﻿namespace DataAccess.Core.Construction
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Static utility class for common Construction Loan related functions.
    /// </summary>
    public static class ConstructionLoanUtilities
    {
        /// <summary>
        /// Calculated the value of sConstructionPurposeT.
        /// </summary>
        /// <param name="dataLoan">Loan data.</param>
        /// <returns>The value sConstructionPurposeT should be migrated to.</returns>
        public static ConstructionPurpose CalculateConstructionPurpose(CPageData dataLoan)
        {
            if (!dataLoan.sIsConstructionLoan)
            {
                return ConstructionPurpose.Blank;
            }

            if (dataLoan.sPurchPrice > 0M || dataLoan.sLandCost > 0M)
            {
                return ConstructionPurpose.ConstructionAndLotPurchase;
            }

            // Look for any liabilites linked to the subject property that are marked as to be paid off at close.
            for (int i = 0; i < dataLoan.nApps; i++)
            {
                var dataApp = dataLoan.GetAppData(i);

                ILiaCollection coll = dataApp.aLiaCollection;
                for (int j = 0; j < coll.CountRegular; ++j)
                {
                    ILiabilityRegular liaField = coll.GetRegularRecordAt(j);
                    if (liaField.DebtT == E_DebtRegularT.Mortgage && liaField.MatchedReRecordId != Guid.Empty)
                    {
                        IRealEstateOwned regRecord = dataApp.aReCollection.GetRecordOf(liaField.MatchedReRecordId) as IRealEstateOwned;
                        if (regRecord != null && regRecord.IsSubjectProp)
                        {
                            return ConstructionPurpose.ConstructionAndLotRefinance;
                        }
                    }
                }
            }

            // Look for an REO marked as the subject property.
            for (int i = 0; i < dataLoan.nApps; i++)
            {
                var dataApp = dataLoan.GetAppData(i);

                foreach (var item in dataApp.aReCollection.GetSubcollection(true, E_ReoGroupT.All))
                {
                    IRealEstateOwned reo = (IRealEstateOwned)item;
                    if (reo.IsSubjectProp)
                    {
                        return ConstructionPurpose.ConstructionOnOwnedLot;
                    }
                }
            }

            // Default to construction and lot purchase.
            return ConstructionPurpose.ConstructionAndLotPurchase;
        }

        /// <summary>
        /// Calculated the value of sLotOwner.
        /// </summary>
        /// <param name="dataLoan">Loan data.</param>
        /// <returns>The value sLotOwner should be migrated to.</returns>
        public static LotOwnerType CalculateLotOwner(CPageData dataLoan)
        {
            if (!dataLoan.sIsConstructionLoan || dataLoan.sConstructionPurposeT != ConstructionPurpose.ConstructionAndLotPurchase)
            {
                return LotOwnerType.Borrower;
            }

            if (dataLoan.sLandCost > 0M)
            {
                return LotOwnerType.Other;
            }

            return LotOwnerType.Builder;
        }
    }
}
