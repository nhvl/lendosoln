﻿namespace DataAccess.Core.Construction
{
    /// <summary>
    /// Indicates how to calculate the interest for the interim construction financing. It is used to calculate the Construction Interest Amount for the interim construction loan. 
    /// </summary>
    public enum ConstructionPhaseIntAccrual
    {
        Blank = 0,
        Monthly_360_360 = 1,
        ActualDays_365_365 = 2,
        ActualDays_365_360 = 3
    }
}
