﻿namespace DataAccess.Core.Construction
{
    /// <summary>
    /// This indicates what for the loan balance in order to calculate the interest for the construction period which is used in the calcs defined in Appendix D. 
    /// </summary>
    public enum ConstructionIntCalcType
    {
        Blank = 0,
        FullCommitment = 1,
        HalfCommitment = 2
    }
}
