﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Common;
using System.Xml;
using LendersOffice.Security;
using System.Xml.Linq;
using System.Xml.XPath;
using LendersOffice.Admin;
using LendersOffice.ObjLib.CustomAttributes;
using LendersOffice.UI;

namespace DataAccess
{
    partial class CPageBase
    {
        #region Broker Rate Lock
        //Custom fields
        [DependsOn]
        public string sU1LockFieldDesc
        {
            get { return GetStringVarCharField("sU1LockFieldDesc"); }
            set { SetStringVarCharField("sU1LockFieldDesc", value); }
        }

        [DependsOn]
        public CDateTime sU1LockFieldD
        {
            get { return GetDateTimeField("sU1LockFieldD"); }
            set { SetDateTimeField("sU1LockFieldD", value); }
        }

        [DependsOn]
        public string sU1LockFieldD_rep
        {
            get { return GetDateTimeField_rep("sU1LockFieldD"); }
            set { SetDateTimeField_rep("sU1LockFieldD", value); }
        }

        [DependsOn]
        public decimal sU1LockFieldAmt
        {
            get { return GetMoneyField("sU1LockFieldAmt"); }
            set { SetMoneyField("sU1LockFieldAmt", value); }
        }

        public string sU1LockFieldAmt_rep
        {
            get { return ToMoneyString(() => sU1LockFieldAmt); }
            set { sU1LockFieldAmt = ToMoney(value); }
        }

        [DependsOn]
        public decimal sU1LockFieldPc
        {
            get { return GetRateField("sU1LockFieldPc"); }
            set { SetRateField("sU1LockFieldPc", value); }
        }

        public string sU1LockFieldPc_rep
        {
            get { return ToRateString(() => sU1LockFieldPc); }
            set { sU1LockFieldPc = ToRate(value); }
        }

        [DependsOn]
        public string sU2LockFieldDesc
        {
            get { return GetStringVarCharField("sU2LockFieldDesc"); }
            set { SetStringVarCharField("sU2LockFieldDesc", value); }
        }

        [DependsOn]
        public CDateTime sU2LockFieldD
        {
            get { return GetDateTimeField("sU2LockFieldD"); }
            set { SetDateTimeField("sU2LockFieldD", value); }
        }

        [DependsOn]
        public string sU2LockFieldD_rep
        {
            get { return GetDateTimeField_rep("sU2LockFieldD"); }
            set { SetDateTimeField_rep("sU2LockFieldD", value); }
        }

        [DependsOn]
        public decimal sU2LockFieldAmt
        {
            get { return GetMoneyField("sU2LockFieldAmt"); }
            set { SetMoneyField("sU2LockFieldAmt", value); }
        }

        public string sU2LockFieldAmt_rep
        {
            get { return ToMoneyString(() => sU2LockFieldAmt); }
            set { sU2LockFieldAmt = ToMoney(value); }
        }

        [DependsOn]
        public decimal sU2LockFieldPc
        {
            get { return GetRateField("sU2LockFieldPc"); }
            set { SetRateField("sU2LockFieldPc", value); }
        }

        public string sU2LockFieldPc_rep
        {
            get { return ToRateString(() => sU2LockFieldPc); }
            set { sU2LockFieldPc = ToRate(value); }
        }

        /// <summary>
        /// Name of the loan program last submitted to the file.
        /// Unlike sLpTemplateNm, this one cannot be changed in UI.
        /// </summary>
        [DependsOn()]
        public string sLpTemplateNmSubmitted
        {
            get { return GetStringVarCharField("sLpTemplateNmSubmitted"); }
            set { SetStringVarCharField("sLpTemplateNmSubmitted", value); }
        }

        /// <summary>
        /// Current pricing row on Broker Rate Lock page that is
        /// locked for editing.
        /// </summary>
        [DependsOn()]
        public E_sBrokerLockPriceRowLockedT sBrokerLockPriceRowLockedT
        {
            get { return (E_sBrokerLockPriceRowLockedT)GetTypeIndexField("sBrokerLockPriceRowLockedT"); }
            set { SetTypeIndexField("sBrokerLockPriceRowLockedT", (int)value); }
        }

        /// <summary>
        /// List of broker adjustments on broker rate lock page.
        /// </summary>
        [DependsOn()]
        public string sBrokerLockAdjustXmlContent
        {
            get { return GetLongTextField("sBrokerLockAdjustXmlContent"); }
            set { SetLongTextField("sBrokerLockAdjustXmlContent", value); }
        }

        /// <summary>
        /// Used by the pricing adjustments property. XmlSerializer is thread safe.
        /// </summary>
        private static System.Xml.Serialization.XmlSerializer x_BrokerAdjustSerializer = new System.Xml.Serialization.XmlSerializer(typeof(List<PricingAdjustment>));

        [DependsOn(nameof(sBrokerLockAdjustXmlContent))]
        public List<PricingAdjustment> sBrokerLockAdjustments
        {
            get
            {
                string data = sBrokerLockAdjustXmlContent;
                if (string.IsNullOrEmpty(data))
                {
                    return new List<PricingAdjustment>();
                }

                //byte[] bytes = Encoding.UTF8.GetBytes(data);
                //using (var ms = new System.IO.MemoryStream(bytes))
                //{
                //    return (List<PricingAdjustment>)x_BrokerAdjustSerializer.Deserialize(ms);
                //}

                using (System.IO.TextReader textReader = new System.IO.StringReader(data))
                {
                    return (List<PricingAdjustment>)x_BrokerAdjustSerializer.Deserialize(textReader);
                }
            }
            set
            {
                this.x_BrokerLockTable = null;

                using (var writer = new System.IO.StringWriter())
                {
                    x_BrokerAdjustSerializer.Serialize(writer, value);
                    sBrokerLockAdjustXmlContent = writer.ToString();
                }
            }
        }

        /// <summary>
        /// Do not access multiple times, cache an instance instead! It recomputes everything on get. 
        /// </summary>
        [DependsOn(nameof(sBrokerLockAdjustments))]
        public VisiblePricingAdjustmentData sVisibleBrokerAdjustmentData
        {
            get
            {
                var cache = ByPassFieldSecurityCheck;   //someone may have turned this on elsewhere so just cache it before we set it.
                ByPassFieldSecurityCheck = true;       
                var adjustments = sBrokerLockAdjustments;
                ByPassFieldSecurityCheck = cache;
                return new VisiblePricingAdjustmentData(adjustments);
            }
        }

        [DependsOn()]
        public CDateTime sBrokerLockRateSheetEffectiveD
        {
            get { return GetDateTimeField("sBrokerLockRateSheetEffectiveD"); }
            set { SetDateTimeField("sBrokerLockRateSheetEffectiveD", value); }
        }
        public string sBrokerLockRateSheetEffectiveD_rep
        {
            get { return GetDateTimeField_rep("sBrokerLockRateSheetEffectiveD"); }
            set { SetDateTimeField_rep("sBrokerLockRateSheetEffectiveD", value); }
        }

        [DependsOn(nameof(sBrokerLockRateSheetEffectiveD))]
        public string sBrokerLockRateSheetEffectiveD_DateTime
        {
            get
            {
                DateTime dt = GetDateTimeField("sBrokerLockRateSheetEffectiveD", DateTime.MinValue);
                if (dt == DateTime.MinValue)
                {
                    return string.Empty;
                }
                else
                {
                    return dt.ToString("MM/dd/yyyy hh:mm tt");
                }
            }
        }

        #region Broker Pricing Table Fields

        BrokerRateLockPriceTable x_BrokerLockTable;

        [DependsOn(nameof(sfLoadBrokerLockData))]
        BrokerRateLockPriceTable BrokerRateLockTable
        {
            get
            {
                // Assume that if someone has not directly loaded up the table by
                // a different price row, we will use the existing broker final row
                if (x_BrokerLockTable == null)
                    LoadBrokerLockData();
                return x_BrokerLockTable;
            }
        }

        /// <summary>
        /// Regenerate the value of BrokerRateLockTable using loan file data.
        /// </summary>
        public void LoadBrokerLockData()
        {
            decimal originatorComp;
            if (sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid
                && sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees)
            {
                originatorComp = sOriginatorCompPoint;
            }
            else
            {
                originatorComp = 0;
            }

            x_BrokerLockTable = new BrokerRateLockPriceTable(sBrokerLockAdjustments
                , E_sBrokerLockPriceRowLockedT.BrokerFinal // We are loading finals.
                , sNoteIR
                , sBrokComp1Pc
                , sRAdjMarginR
                , sIsOptionArm ? sOptionArmTeaserR : 0
                , m_convertLos
                , originatorComp);
        }

        [DependsOn(nameof(sBrokerLockAdjustments), nameof(sNoteIR), nameof(sBrokComp1Pc), nameof(sRAdjMarginR), nameof(sOptionArmTeaserR), nameof(sBrokerLockPriceRowLockedT), 
            nameof(sIsOptionArm), nameof(sIsRateLocked), nameof(sOriginatorCompPoint), nameof(sOriginatorCompensationLenderFeeOptionT), nameof(BrokerRateLockTable), 
            nameof(sOriginatorCompensationPaymentSourceT))]
        public bool sfLoadBrokerLockData
        {
            get { return false; }
        }

        /// <summary>
        /// Generates a new value for BrokerRateLockTable based on the adjustment data in the loan file
        /// combined with the input parameters. Note that which row gets updated will depend on the
        /// value of sBrokerLockPriceRowLockedT in the loan file.
        /// </summary>
        /// <param name="rate">The note rate to set, as a string</param>
        /// <param name="fee">The points to set, as a string.</param>
        /// <param name="margin">The margin to set, as a string.</param>
        /// <param name="teaserRate">The teaser rate to set, as a string.</param>
        public void LoadBrokerLockData(
            string rate
            , string fee
            , string margin
            , string teaserRate
            )
        {
            LoadBrokerLockData(
                ToRate(rate)
                , ToRate(fee)
                , ToRate(margin)
                , ToRate(teaserRate)
                );
        }


        /// <summary>
        /// Generates a new value for BrokerRateLockTable based on the adjustment data in the loan file
        /// combined with the input parameters. Note that which row gets updated will depend on the
        /// value of sBrokerLockPriceRowLockedT in the loan file.
        /// </summary>
        /// <param name="rate">The note rate to set.</param>
        /// <param name="fee">The points to set.</param>
        /// <param name="margin">The margin to set.</param>
        /// <param name="teaserRate">The teaser rate to set.</param>
        public void LoadBrokerLockData(
            decimal rate
            , decimal fee
            , decimal margin
            , decimal teaserRate
            )
        {
            decimal originatorComp;
            if (this.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid
                && this.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees)
            {
                originatorComp = this.sOriginatorCompPoint;
            }
            else
            {
                originatorComp = 0;
            }

            var updatedTable = new BrokerRateLockPriceTable(this.sBrokerLockAdjustments, this.sBrokerLockPriceRowLockedT, rate, fee, margin, teaserRate, this.m_convertLos, originatorComp);

            if (!this.sIsRateLocked)
            {
                // Apply calculated values to the corresponding loan file fields. Note that setting these
                // can clear x_BrokerLockTable, which is an internal cache, so we update the values on the
                // loan file using the computed values first, then update x_BrokerLockTable.
                this.sNoteIR = updatedTable.BrokerFinalPrice.Rate;
                this.sBrokComp1Pc = updatedTable.BrokerFinalPrice.Fee;
                this.sRAdjMarginR = updatedTable.BrokerFinalPrice.Margin;
                if (this.sIsOptionArm)
                {
                    this.sOptionArmTeaserR = updatedTable.BrokerFinalPrice.TeaserRate;
                }
            }

            this.x_BrokerLockTable = updatedTable;
        }
        
        [DependsOn(nameof(BrokerRateLockTable))]
        private decimal sBrokerLockBaseNoteIR
        {
            get { return BrokerRateLockTable.BasePrice.Rate; }
        }
        public string sBrokerLockBaseNoteIR_rep
        {
            get
            {
                return ToRateString(() => BrokerRateLockTable.BasePrice.Rate);
            }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        public string sBrokerLockBaseBrokComp1PcPrice
        {
            get { return BrokerRateLockTable.BasePrice.Price; }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        private decimal sBrokerLockBaseBrokComp1PcFee
        {
            get { return BrokerRateLockTable.BasePrice.Fee; }
        }
        public string sBrokerLockBaseBrokComp1PcFee_rep
        {
            get { return ToRateString(() => sBrokerLockBaseBrokComp1PcFee); }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        private decimal sBrokerLockBaseRAdjMarginR
        {
            get { return BrokerRateLockTable.BasePrice.Margin; }
        }
        public string sBrokerLockBaseRAdjMarginR_rep
        {
            get { return ToRateString(() => sBrokerLockBaseRAdjMarginR); }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        private decimal sBrokerLockBaseOptionArmTeaserR
        {
            get { return BrokerRateLockTable.BasePrice.TeaserRate; }
        }
        public string sBrokerLockBaseOptionArmTeaserR_rep
        {
            get { return ToRateString(() => sBrokerLockBaseOptionArmTeaserR); }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        private decimal sBrokerLockBrokerBaseNoteIR
        {
            get { return BrokerRateLockTable.BrokerBasePrice.Rate; }
        }
        public string sBrokerLockBrokerBaseNoteIR_rep
        {
            get { return ToRateString(() => sBrokerLockBrokerBaseNoteIR); }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        public string sBrokerLockBrokerBaseBrokComp1PcPrice
        {
            get { return BrokerRateLockTable.BrokerBasePrice.Price; }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        private decimal sBrokerLockBrokerBaseBrokComp1PcFee
        {
            get { return BrokerRateLockTable.BrokerBasePrice.Fee; }
        }
        public string sBrokerLockBrokerBaseBrokComp1PcFee_rep
        {
            get { return ToRateString(() => sBrokerLockBrokerBaseBrokComp1PcFee); }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        private decimal sBrokerLockBrokerBaseRAdjMarginR
        {
            get { return BrokerRateLockTable.BrokerBasePrice.Margin; }
        }
        public string sBrokerLockBrokerBaseRAdjMarginR_rep
        {
            get { return ToRateString(() => sBrokerLockBrokerBaseRAdjMarginR); }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        private decimal sBrokerLockBrokerBaseOptionArmTeaserR
        {
            get { return BrokerRateLockTable.BrokerBasePrice.TeaserRate; }
        }
        public string sBrokerLockBrokerBaseOptionArmTeaserR_rep
        {
            get { return ToRateString(() => sBrokerLockBrokerBaseOptionArmTeaserR); }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        private decimal sBrokerLockTotHiddenAdjNoteIR
        {
            get { return BrokerRateLockTable.TotalHiddenAdjustments.Rate; }
        }
        public string sBrokerLockTotHiddenAdjNoteIR_rep
        {
            get { return ToRateString(() => sBrokerLockTotHiddenAdjNoteIR); }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        public string sBrokerLockTotHiddenAdjBrokComp1PcPrice
        {
            get { return BrokerRateLockTable.TotalHiddenAdjustments.AdjPrice; }
        }

        /// <summary>
        /// This field is a workaround for sBrokerLockTotHiddenAdjBrokComp1PcPrice being
        /// added to LOAN_FILE_CACHE_2 with type decimal instead of varchar(8). Now that 
        /// the column has existed for a while, it may not be safe to convert its type.
        /// OPM 65998 - GF
        /// </summary>
        [DependsOn(nameof(sBrokerLockTotHiddenAdjBrokComp1PcPrice))]
        public string sBrokerLockTotHiddenAdjBrokComp1PcPriceCR
        {
            get { return sBrokerLockTotHiddenAdjBrokComp1PcPrice; }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        private decimal sBrokerLockTotHiddenAdjBrokComp1PcFee
        {
            get { return BrokerRateLockTable.TotalHiddenAdjustments.Fee; }
        }
        public string sBrokerLockTotHiddenAdjBrokComp1PcFee_rep
        {
            get { return ToRateString(() => sBrokerLockTotHiddenAdjBrokComp1PcFee); }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        private decimal sBrokerLockTotHiddenAdjRAdjMarginR
        {
            get { return BrokerRateLockTable.TotalHiddenAdjustments.Margin; }
        }
        public string sBrokerLockTotHiddenAdjRAdjMarginR_rep
        {
            get { return ToRateString(() => sBrokerLockTotHiddenAdjRAdjMarginR); }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        private decimal sBrokerLockTotHiddenAdjOptionArmTeaserR
        {
            get { return BrokerRateLockTable.TotalHiddenAdjustments.TeaserRate; }
        }
        public string sBrokerLockTotHiddenAdjOptionArmTeaserR_rep
        {
            get { return ToRateString(() => sBrokerLockTotHiddenAdjOptionArmTeaserR); }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        private decimal sBrokerLockTotVisibleAdjNoteIR
        {
            get { return BrokerRateLockTable.TotalVisibleAdjustments.Rate; }
        }
        public string sBrokerLockTotVisibleAdjNoteIR_rep
        {
            get { return ToRateString(() => sBrokerLockTotVisibleAdjNoteIR); }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        public string sBrokerLockTotVisibleAdjBrokComp1PcPrice
        {
            get { return BrokerRateLockTable.TotalVisibleAdjustments.AdjPrice; }
        }

        /// <summary>
        /// This field is a workaround for sBrokerLockTotVisibleAdjBrokComp1PcPrice being
        /// added to LOAN_FILE_CACHE_2 with type decimal instead of varchar(8). Now that 
        /// the column has existed for a while, it may not be safe to convert its type.
        /// OPM 65998 - GF
        /// </summary>
        [DependsOn(nameof(sBrokerLockTotVisibleAdjBrokComp1PcPrice))]
        public string sBrokerLockTotVisibleAdjBrokComp1PcPriceCR
        {
            get { return sBrokerLockTotVisibleAdjBrokComp1PcPrice; }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        private decimal sBrokerLockTotVisibleAdjBrokComp1PcFee
        {
            get { return BrokerRateLockTable.TotalVisibleAdjustments.Fee; }
        }
        public string sBrokerLockTotVisibleAdjBrokComp1PcFee_rep
        {
            get { return ToRateString(() => sBrokerLockTotVisibleAdjBrokComp1PcFee); }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        private decimal sBrokerLockTotVisibleAdjRAdjMarginR
        {
            get { return BrokerRateLockTable.TotalVisibleAdjustments.Margin; }
        }
        public string sBrokerLockTotVisibleAdjRAdjMarginR_rep
        {
            get { return ToRateString(() => sBrokerLockTotVisibleAdjRAdjMarginR); }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        private decimal sBrokerLockTotVisibleAdjOptionArmTeaserR
        {
            get { return BrokerRateLockTable.TotalVisibleAdjustments.TeaserRate; }
        }
        public string sBrokerLockTotVisibleAdjOptionArmTeaserR_rep
        {
            get { return ToRateString(() => sBrokerLockTotVisibleAdjOptionArmTeaserR); }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        private decimal sBrokerLockFinalNoteIR
        {
            get { return BrokerRateLockTable.BrokerFinalPrice.Rate; }
        }
        public string sBrokerLockFinalNoteIR_rep
        {
            get { return ToRateString(() => sBrokerLockFinalNoteIR); }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        public decimal sBrokerLockFinalBrokComp1PcPrice
        {
            get 
            {
                var oldBypassBrokerAdjXml = this.ByPassFieldsBrokerLockAdjustXmlContent;
                this.ByPassFieldsBrokerLockAdjustXmlContent = true;

                // Basis points form of fee.
                decimal brokerLockFinalBrokComp1PcPrice = (100 - BrokerRateLockTable.BrokerFinalPrice.Fee);

                this.ByPassFieldsBrokerLockAdjustXmlContent = oldBypassBrokerAdjXml;

                return brokerLockFinalBrokComp1PcPrice;
            }
        }
        public string sBrokerLockFinalBrokComp1PcPrice_rep
        {
            get
            {
                return ToRateString(() => sBrokerLockFinalBrokComp1PcPrice);
            }
        }

        [DependsOn(nameof(sBrokerLockFinalBrokComp1PcPrice), nameof(sLDiscntBaseAmt))]
        private decimal sBrokerLockFinalBrokComp1PcAmt
        {
            get
            {
                return ((sBrokerLockFinalBrokComp1PcPrice - 100) / 100) * sLDiscntBaseAmt;
            }
        }

        public string sBrokerLockFinalBrokComp1PcAmt_rep
        {
            get
            {
                return ToMoneyString(() => sBrokerLockFinalBrokComp1PcAmt);
            }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        private decimal sBrokerLockFinalBrokComp1PcFee
        {
            get { return BrokerRateLockTable.BrokerFinalPrice.Fee; }
        }
        public string sBrokerLockFinalBrokComp1PcFee_rep
        {
            get { return ToRateString(() => sBrokerLockFinalBrokComp1PcFee); }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        private decimal sBrokerLockFinalRAdjMarginR
        {
            get { return BrokerRateLockTable.BrokerFinalPrice.Margin; }
        }
        public string sBrokerLockFinalRAdjMarginR_rep
        {
            get { return ToRateString(() => sBrokerLockFinalRAdjMarginR); }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        private decimal sBrokerLockFinalOptionArmTeaserR
        {
            get { return BrokerRateLockTable.BrokerFinalPrice.TeaserRate; }
        }
        public string sBrokerLockFinalOptionArmTeaserR_rep
        {
            get { return ToRateString(() => sBrokerLockFinalOptionArmTeaserR); }
        }


        [DependsOn(nameof(BrokerRateLockTable))]
        private decimal sBrokerLockOriginatorPriceNoteIR
        {
            get { return BrokerRateLockTable.OriginatorPrice.Rate; }
        }
        public string sBrokerLockOriginatorPriceNoteIR_rep
        {
            get { return ToRateString(() => sBrokerLockOriginatorPriceNoteIR); }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        [LqbInputModelAttribute(type: InputFieldType.Percent)]
        public string sBrokerLockOriginatorPriceBrokComp1PcPrice
        {
            get 
            { 
                var oldBypassBrokerAdjXml = this.ByPassFieldsBrokerLockAdjustXmlContent;
                this.ByPassFieldsBrokerLockAdjustXmlContent = true;

                string brokerLockOriginatorPriceBrokComp1PcPrice = BrokerRateLockTable.OriginatorPrice.Price;

                this.ByPassFieldsBrokerLockAdjustXmlContent = oldBypassBrokerAdjXml;

                return brokerLockOriginatorPriceBrokComp1PcPrice;
            }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        [DevNote("The originator comp row fee of broker rate lock table.")]
        public decimal sBrokerLockOriginatorPriceBrokComp1PcFee
        {
            get 
            {
                var oldBypassBrokerAdjXml = this.ByPassFieldsBrokerLockAdjustXmlContent;
                this.ByPassFieldsBrokerLockAdjustXmlContent = true;

                decimal brokerLockOriginatorPriceBrokComp1PcFee = BrokerRateLockTable.OriginatorPrice.Fee; 

                this.ByPassFieldsBrokerLockAdjustXmlContent = oldBypassBrokerAdjXml;

                return brokerLockOriginatorPriceBrokComp1PcFee;
            }
        }

        public string sBrokerLockOriginatorPriceBrokComp1PcFee_rep
        {
            get { return ToRateString(() => sBrokerLockOriginatorPriceBrokComp1PcFee); }
        }

        /// <summary>
        /// The dollar amount of the wholesale price that remains after the broker-paid LO comp (if any) is paid out.
        /// </summary>
        [DependsOn(nameof(sBrokerLockOriginatorPriceBrokComp1PcFee), nameof(sLDiscntBaseAmt))]
        [DevNote("The dollar amount of the wholesale price that remains after the broker-paid LO comp (if any) is paid out.")]
        private decimal sBrokerLockOriginatorPriceBrokComp1PcAmt
        {
            get { return (-1 * sBrokerLockOriginatorPriceBrokComp1PcFee / 100) * sLDiscntBaseAmt; }
        }

        public string sBrokerLockOriginatorPriceBrokComp1PcAmt_rep
        {
            get { return ToMoneyString(() => sBrokerLockOriginatorPriceBrokComp1PcAmt); }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        private decimal sBrokerLockOriginatorPriceRAdjMarginR
        {
            get { return BrokerRateLockTable.OriginatorPrice.Margin; }
        }
        public string sBrokerLockOriginatorPriceRAdjMarginR_rep
        {
            get { return ToRateString(() => sBrokerLockOriginatorPriceRAdjMarginR); }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        private decimal sBrokerLockOriginatorPriceOptionArmTeaserR
        {
            get { return BrokerRateLockTable.OriginatorPrice.TeaserRate; }
        }
        public string sBrokerLockOriginatorPriceOptionArmTeaserR_rep
        {
            get { return ToRateString(() => sBrokerLockOriginatorPriceOptionArmTeaserR); }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        private decimal sBrokerLockOriginatorCompAdjNoteIR
        {
            get { return BrokerRateLockTable.OriginatorCompAdjustments.Rate; }
        }
        public string sBrokerLockOriginatorCompAdjNoteIR_rep
        {
            get { return ToRateString(() => sBrokerLockOriginatorCompAdjNoteIR); }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        public string sBrokerLockOriginatorCompAdjBrokComp1PcPrice
        {
            get { return BrokerRateLockTable.OriginatorCompAdjustments.AdjPrice; }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        private decimal sBrokerLockOriginatorCompAdjBrokComp1PcFee
        {
            get { return BrokerRateLockTable.OriginatorCompAdjustments.Fee; }
        }
        public string sBrokerLockOriginatorCompAdjBrokComp1PcFee_rep
        {
            get { return ToRateString(() => sBrokerLockOriginatorCompAdjBrokComp1PcFee); }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        private decimal sBrokerLockOriginatorCompAdjRAdjMarginR
        {
            get { return BrokerRateLockTable.OriginatorCompAdjustments.Margin; }
        }
        public string sBrokerLockOriginatorCompAdjRAdjMarginR_rep
        {
            get { return ToRateString(() => sBrokerLockOriginatorCompAdjRAdjMarginR); }
        }

        [DependsOn(nameof(BrokerRateLockTable))]
        private decimal sBrokerLockOriginatorCompAdjOptionArmTeaserR
        {
            get { return BrokerRateLockTable.OriginatorCompAdjustments.TeaserRate; }
        }
        public string sBrokerLockOriginatorCompAdjOptionArmTeaserR_rep
        {
            get { return ToRateString(() => sBrokerLockOriginatorCompAdjOptionArmTeaserR); }
        }



        /// <summary>
        /// Total of all lender-based adjustments in the broker adjustment table.
        /// </summary>
        [DependsOn(nameof(sBrokerLockAdjustments))]
        private decimal sBrokerLockTotalLenderAdj
        {
            get
            {
                decimal total = 0;
                foreach (PricingAdjustment adjustment in sBrokerLockAdjustments)
                {
                    if (adjustment.IsLenderAdjustment)
                    {
                        total += ToRate(adjustment.Fee);
                    }
                }
                return total;
            }
        }
        public string sBrokerLockTotalLenderAdj_rep
        {
            get { return ToRateString(() => sBrokerLockTotalLenderAdj); }
        }

        /// <summary>
        /// Broker Rate Lock Investor Price.  The same price one would get in the investor rate lock page if they populated from the broker lock using the default settings - OPM 63312
        /// </summary>
        [DependsOn(nameof(sBrokerLockTotalLenderAdj), nameof(sBrokerLockFinalBrokComp1PcPrice))]
        private decimal sBrokerLockInvestorPrice
        {
            get 
            {
                return sBrokerLockTotalLenderAdj + sBrokerLockFinalBrokComp1PcPrice;
            }
        }

        public string sBrokerLockInvestorPrice_rep
        {
            get { return ToRateString(() => sBrokerLockInvestorPrice); }
        }

        [DependsOn]
        public string sRateLockHistoryXmlContent
        {
            get { return GetLongTextField("sRateLockHistoryXmlContent"); }
            set { SetLongTextField("sRateLockHistoryXmlContent", value); }
        }

        [DependsOn(nameof(sRateLockStatusT))]
        public bool sIsRateLocked
        {
            // 7/27/2009 dd - We are no long use sIsRateLocked in LOAN_FILE_F table. We use sRateLockStatusT
            get
            {
                if (AppliedCCArchive != null  && !this.AppliedCCArchive.IsForCoCProcess)
                {
                    if (AppliedCCArchive.HasValue("sIsRateLocked")) return AppliedCCArchive.GetBitValue("sIsRateLocked");
                }

                switch (sRateLockStatusT)
                {
                    case E_sRateLockStatusT.LockRequested:
                    case E_sRateLockStatusT.NotLocked:
                    case E_sRateLockStatusT.LockSuspended:
                        return false;
                    case E_sRateLockStatusT.Locked:
                        return true;
                    default:
                        throw new UnhandledEnumException(sRateLockStatusT);
                }
            }
        }

        private static Dictionary<E_sRateLockStatusT, string> sRateLockStatusT_map = new Dictionary<E_sRateLockStatusT, string>()
        {
            { E_sRateLockStatusT.NotLocked, "Not Locked"},
            { E_sRateLockStatusT.Locked, "Locked"},
            { E_sRateLockStatusT.LockRequested, "Lock Requested"},
            { E_sRateLockStatusT.LockSuspended, "Lock Suspended"}
        };
        [DependsOn]
        public E_sRateLockStatusT sRateLockStatusT
        {
            // 7/27/2009 dd - OPM 32212 - Add enum value for rate lock status. 
            get { return (E_sRateLockStatusT)GetTypeIndexField("sRateLockStatusT"); }
        }
        private E_sRateLockStatusT sRateLockStatusT_
        {
            set 
            { 
                SetTypeIndexField("sRateLockStatusT", (int)value);
            }
        }

        public static string sRateLockStatusT_map_rep(E_sRateLockStatusT value)
        {
            return sRateLockStatusT_map[value];
        }
        public string sRateLockStatusT_rep
        {
            get { return sRateLockStatusT_map_rep(sRateLockStatusT); }
        }

        [DependsOn(nameof(sIsRateLocked), nameof(sRLckdDays), nameof(sEstCloseDLckd), nameof(sSchedDueD1Lckd), nameof(sfInvalidateCache))]
        public int sRLckdDays
        {
            get { return GetCountField("sRLckdDays"); }
            set
            {
                int current;

                try { current = sRLckdDays; }
                catch { current = 0; }

                if (!m_performingRateLockAction && sIsRateLocked && value != current && (this is CPriceEngineData.CPriceEngineBasePage == false) && sIsInternalPricingMode == false) // TOTAL prices in lock.  Need to set Lenders's total lockdays
                {
                    LogBug("Cannot change the lock period when rate is locked.");
                    CBaseException exc = new CBaseException(ErrorMessages.CannotChangeRateLockPeriodDuringRateLock, "Cannot change the lock period when rate is locked.");
                    exc.IsEmailDeveloper = false;
                    throw exc;
                }

                SetCountField("sRLckdDays", value);

               if (!this.sEstCloseDLckd && !this.sSchedDueD1Lckd)
                {
                    this.InvalidateCache();
                }
            }
        }
        public string sRLckdDays_rep
        {
            get { return ToCountString(() => sRLckdDays); }
            set { sRLckdDays = ToCount(value); }
        }

        #endregion

        #region Rate Lock Methods
        [DependsOn(nameof(sRLckdD), nameof(sRLckdDays), nameof(sIsRateLocked), nameof(sRLckdExpiredD), nameof(sRateLockHistoryXmlDocument), nameof(sBrokComp1Pc), 
            nameof(sRateLockHistoryXmlDocument), nameof(sfLoadBrokerLockData), nameof(sfAddLockEvent), nameof(sfProcessRateLockDisclosureTrigger), 
            nameof(sfSet_sRLckdExpiredD_BasedOn), nameof(sRateLockHistoryXmlContent), nameof(sfCalculateRateLockExtensionExpirationDate), 
            nameof(sfSet_sRLckdExpiredD_Manually), nameof(sfSet_sRLckdDays_BasedOn), nameof(sBrokerLockAdjustments), nameof(sBrokerLockPriceRowLockedT), 
            nameof(sBrokerLockBaseNoteIR), nameof(sBrokerLockBaseBrokComp1PcFee), nameof(sBrokerLockBaseRAdjMarginR), nameof(sBrokerLockBrokerBaseOptionArmTeaserR), 
            nameof(BrokerRateLockTable), nameof(sNoteIR), nameof(sRAdjMarginR), nameof(sIsOptionArm), nameof(sOptionArmTeaserR))]
        public int sfExtendRateLock
        {
            get { return 0; }
        }

        private bool m_performingRateLockAction = false;

        /// <summary>
        /// Call this method before setting sRLckdExpiredD to the new date.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="extensionReason"></param>
        public void ExtendRateLock(string userName, string extensionReason, int addDays, string brokComp1Pc)
        {
            if (!sIsRateLocked)
                throw new CBaseException(ErrorMessages.CannotExtendLockRateIfRateIsNotLocked, ErrorMessages.CannotExtendLockRateIfRateIsNotLocked);

            m_performingRateLockAction = true;

            try
            {
                sRLckdDays = sRLckdDays + addDays;
                Set_sRLckdExpiredD_BasedOn(sRLckdD, sRLckdDays);
                //sRLckdExpiredD = new CDateTime(sRLckdD.DateTimeForComputation.AddDays(sRLckdDays), m_convertLos);

                sBrokComp1Pc_rep = brokComp1Pc;
            }
            finally
            {
                m_performingRateLockAction = false;
            }

            XmlDocument doc = sRateLockHistoryXmlDocument;
            XmlElement root = doc.DocumentElement;

            XmlElement newEvent = doc.CreateElement("Event");
            newEvent.SetAttribute("EvenType", "ExtendLockEvent");
            newEvent.SetAttribute("EventDate", Tools.GetDateTimeNowString());
            newEvent.SetAttribute("DoneBy", userName);
            newEvent.SetAttribute("sRLckdExpiredD", sRLckdExpiredD_rep);
            newEvent.SetAttribute("sRLckBreakReasonDesc", extensionReason);
            newEvent.SetAttribute("sBrokComp1Pc", sBrokComp1Pc_rep);
            root.AppendChild(newEvent);

            sRateLockHistoryXmlContent = doc.OuterXml;

            ProcessRateLockDisclosureTrigger(E_RateLockDisclosureEventT.LockExtension);
        }


        public void ExtendRateLock(string userName, string extensionReason, int addDays, PricingAdjustment adjustment)
        {
            if (!sIsRateLocked)
                throw new CBaseException(ErrorMessages.CannotExtendLockRateIfRateIsNotLocked, ErrorMessages.CannotExtendLockRateIfRateIsNotLocked);

            m_performingRateLockAction = true;

            try
            {
                // We now set the sRLckdExpiredD based on the previous sRLckdExpiredD: OPM 75269 - m.p.
                // We also now set sRLckdDays based on just the lock date and the expiration date
                sRLckdDays = sRLckdExpiredD.DateTimeForComputation.Subtract(sRLckdD.DateTimeForComputation).Days + addDays;
                CDateTime dt = CalculateRateLockExtensionExpirationDate(sRLckdD, sRLckdDays);
                Set_sRLckdExpiredD_Manually(dt); // The date here should reflect the date given in the rate lock extension dialog
                Set_sRLckdDays_BasedOn(sRLckdD, sRLckdExpiredD);

                if (adjustment != null)
                {
                    LoadBrokerLockData();

                    // These can depend on the adjustments. Get their values before we
                    // update the adjustments.
                    string baseRate = sBrokerLockBaseNoteIR_rep;
                    string baseFee = sBrokerLockBaseBrokComp1PcFee_rep;
                    string baseMargin = sBrokerLockBaseRAdjMarginR_rep;
                    string baseTeaserRate = sBrokerLockBrokerBaseOptionArmTeaserR_rep;

                    List<PricingAdjustment> adjustments = sBrokerLockAdjustments;
                    adjustments.Add(adjustment);
                    sBrokerLockAdjustments = adjustments;

                    // Taking into account new adjustment.
                    sBrokerLockPriceRowLockedT = E_sBrokerLockPriceRowLockedT.Base;
                    LoadBrokerLockData(baseRate, baseFee,
                    baseMargin, baseTeaserRate);

                    decimal rate = BrokerRateLockTable.BrokerFinalPrice.Rate;
                    decimal fee = BrokerRateLockTable.BrokerFinalPrice.Fee;
                    decimal margin = BrokerRateLockTable.BrokerFinalPrice.Margin;
                    decimal teaserRate = BrokerRateLockTable.BrokerFinalPrice.TeaserRate;

                    // Update to final rate
                    sNoteIR = rate;
                    sBrokComp1Pc = fee;
                    sRAdjMarginR = margin;
                    if (sIsOptionArm)
                        sOptionArmTeaserR = teaserRate;
                }
            }
            finally
            {
                m_performingRateLockAction = false;
            }

            AddLockEvent("ExtendLockEvent", userName, extensionReason);
            ProcessRateLockDisclosureTrigger(E_RateLockDisclosureEventT.LockExtension);
        }

        [DependsOn(nameof(sRLckdD), nameof(sRLckdDays), nameof(sIsRateLocked), nameof(sRLckdExpiredD), nameof(sRateLockHistoryXmlDocument), nameof(sBrokComp1Pc), 
            nameof(sRateLockHistoryXmlDocument), nameof(sfLoadBrokerLockData), nameof(sfAddLockEvent), nameof(sfProcessRateLockDisclosureTrigger), 
            nameof(sBrokerLockAdjustments), nameof(sBrokerLockPriceRowLockedT), nameof(sBrokerLockBaseNoteIR), nameof(sBrokerLockBaseBrokComp1PcFee), 
            nameof(sBrokerLockBaseRAdjMarginR), nameof(sBrokerLockBrokerBaseOptionArmTeaserR), nameof(sNoteIR), nameof(BrokerRateLockTable), nameof(sRAdjMarginR), 
            nameof(sIsOptionArm), nameof(sOptionArmTeaserR))]
        public int sfFloatDownRate
        {
            get { return 0; }
        }

        public void FloatDownRate(string userName, decimal rate, decimal price, List<PricingAdjustment> adjustments)
        {
            if (!sIsRateLocked)
                throw new CBaseException(ErrorMessages.CannotFloatDownRateIfRateIsNotLocked, ErrorMessages.CannotFloatDownRateIfRateIsNotLocked);

            m_performingRateLockAction = true;

            try
            {
                LoadBrokerLockData();

                // These can depend on the adjustments. Get their values before we
                // update the adjustments.
                string baseRate = sBrokerLockBaseNoteIR_rep;
                string baseFee = sBrokerLockBaseBrokComp1PcFee_rep;
                string baseMargin = sBrokerLockBaseRAdjMarginR_rep;
                string baseTeaserRate = sBrokerLockBrokerBaseOptionArmTeaserR_rep;

                sBrokerLockAdjustments = adjustments;

                // Taking into account new adjustment.
                sBrokerLockPriceRowLockedT = E_sBrokerLockPriceRowLockedT.Base;
                LoadBrokerLockData(baseRate, baseFee,
                baseMargin, baseTeaserRate);

                // Pull the values we are going to set out of the table before setting
                // to avoid having the calculated values shift under our feet as we try
                // to set them.
                decimal margin = BrokerRateLockTable.BrokerFinalPrice.Margin;
                decimal teaserRate = BrokerRateLockTable.BrokerFinalPrice.TeaserRate;

                // Update to final rate
                sNoteIR = rate;
                sBrokComp1Pc = price;
                sRAdjMarginR = margin;
                if (sIsOptionArm)
                    sOptionArmTeaserR = teaserRate;
            }
            finally
            {
                m_performingRateLockAction = false;
            }

            AddLockEvent("FloatDownEvent", userName, "");
            ProcessRateLockDisclosureTrigger(E_RateLockDisclosureEventT.FloatDownLock);
        }

        [DependsOn(nameof(sRLckdD), nameof(sRLckdDays), nameof(sIsRateLocked), nameof(sRLckdExpiredD), nameof(sRateLockHistoryXmlDocument), nameof(sBrokComp1Pc), nameof(sRateLockHistoryXmlDocument), nameof(sfLoadBrokerLockData), nameof(sfAddLockEvent), nameof(sfProcessRateLockDisclosureTrigger))]
        public int sfRelockRate
        {
            get { return 0; }
        }


        public void ReLockRate(string userName, string reason, int addDays, decimal rate, decimal price, List<PricingAdjustment> adjustments)
        {
            if (!sIsRateLocked)
                throw new CBaseException(ErrorMessages.CannotExtendLockRateIfRateIsNotLocked, ErrorMessages.CannotExtendLockRateIfRateIsNotLocked);

            m_performingRateLockAction = true;

            try
            {
                DateTime now = DateTime.Now;
                sRLckdD = CDateTime.Create(now);

                sRLckdDays = addDays;
                CDateTime dt = CalculateRateLockExtensionExpirationDate(sRLckdD, sRLckdDays);
                
                
                Set_sRLckdExpiredD_Manually(dt); // The date here should reflect the date given in the rate lock extension dialog
                Set_sRLckdDays_BasedOn(sRLckdD, sRLckdExpiredD);

                LoadBrokerLockData();

                // These can depend on the adjustments. Get their values before we
                // update the adjustments.
                string baseRate = sBrokerLockBaseNoteIR_rep;
                string baseFee = sBrokerLockBaseBrokComp1PcFee_rep;
                string baseMargin = sBrokerLockBaseRAdjMarginR_rep;
                string baseTeaserRate = sBrokerLockBrokerBaseOptionArmTeaserR_rep;

                sBrokerLockAdjustments = adjustments;

                // Taking into account new adjustment.
                sBrokerLockPriceRowLockedT = E_sBrokerLockPriceRowLockedT.Base;
                LoadBrokerLockData(baseRate, baseFee,
                baseMargin, baseTeaserRate);

                // Update to final rate
                sNoteIR = rate;
                sBrokComp1Pc = price;

                sRAdjMarginR = BrokerRateLockTable.BrokerFinalPrice.Margin;
                if (sIsOptionArm)
                    sOptionArmTeaserR = BrokerRateLockTable.BrokerFinalPrice.TeaserRate;
            }
            finally
            {
                m_performingRateLockAction = false;
            }

            AddLockEvent("ReLockRateEvent", userName, reason);
            ProcessRateLockDisclosureTrigger(E_RateLockDisclosureEventT.Relocked);
        }


        [DependsOn(nameof(sRateLockHistoryXmlContent))]
        private XmlDocument sRateLockHistoryXmlDocument
        {
            get
            {
                XmlDocument doc = null;

                string existingContent = sRateLockHistoryXmlContent;
                XmlElement root;
                if (existingContent == "")
                {
                    doc = new XmlDocument();
                    root = doc.CreateElement("RateLockHistory");
                    doc.AppendChild(root);
                }
                else
                {
                    try
                    {
                        doc = Tools.CreateXmlDoc(existingContent);
                        root = doc.DocumentElement;
                    }
                    catch (Exception exc)
                    {
                        Tools.LogError("Failed to load existing sRateLockHistoryXmlContent into a xml document. Putting the corrupted content into a separate node for future reference.", exc);
                        doc = new XmlDocument();
                        root = doc.CreateElement("RateLockHistory");
                        doc.AppendChild(root);
                        try
                        {
                            XmlElement el = doc.CreateElement("CorruptedHistory");
                            el.InnerText = existingContent;
                            root.AppendChild(el);
                        }
                        catch (Exception e)
                        {
                            Tools.LogError("Failed to save corrupted history of rate lock. The operation is now aborted.", e);
                            throw;
                        }
                    }

                }
                return doc;
            }
        }

        [DependsOn(nameof(sRateLockStatusT), nameof(sPpmtPenaltyMon), nameof(sRLckdExpiredD), nameof(sStatusT), nameof(sStatusLckd), nameof(sApprovD), nameof(sIsRateLocked), nameof(sNoteIR), nameof(sBrokComp1Pc), nameof(sRAdjMarginR), nameof(sTerm), nameof(sDue), nameof(sFinMethT), nameof(sIsRateLocked), nameof(sRLckdD), nameof(sRLckdExpiredD), nameof(sRLckdDays), nameof(sRateLockHistoryXmlDocument), nameof(sfLoadBrokerLockData), nameof(sLpTemplateNm), nameof(sfAddLockEvent), nameof(sfThrowErrorIfDuplicatesFound), nameof(sfProcessRateLockDisclosureTrigger), nameof(sfSet_sRLckdExpiredD_BasedOn), nameof(sHasOriginatorCompensationPlan))]
        public int sfLockRate
        {
            get
            {
                return 0;
            }
        }


        /// <summary>
        /// Call this after you already assign all the fields
        /// </summary>
        public void LockRate(string userName, string lockReason, bool bChangeStatusToApproved)
        {
            if (sIsRateLocked)
                throw new CBaseException(ErrorMessages.CannotChangeNoteRateForLockedRate, "We cannot allow locking rate when the rate is already locked.");
            
            ThrowErrorIfDuplicatesFound();

            //4/11/2014 bs - Add activity type for mobile spec v2.4
            LendersOffice.MobileApp.LatestActivity.Send(sLId, "Rate Lock", "4");

            CDateTime today = CDateTime.Create(DateTime.Today);
            //			
            //            sRLckdD			= today;

            Set_sRLckdExpiredD_BasedOn(sRLckdD, sRLckdDays);
            //sRLckdExpiredD = new CDateTime(sRLckdD.DateTimeForComputation.AddDays(sRLckdDays), m_convertLos);

            if (bChangeStatusToApproved)
            {
                sApprovD = today;
                if (E_sStatusT.Loan_Approved != sStatusT)
                {
                    sStatusLckd = true;
                    sStatusT = E_sStatusT.Loan_Approved;
                }
            }
            sRateLockStatusT_ = E_sRateLockStatusT.Locked;

            AddLockEvent("LockEvent", userName, lockReason);
            ProcessRateLockDisclosureTrigger(E_RateLockDisclosureEventT.LoanLocked);

            //SetPricingAndAdjustmentsToAuditEvent(newEvent, doc);

            // OPM 64269 Apply plan when locking rate if no plan exists.
            if (this.BrokerDB.ShouldApplyCompPlan(E_LoanEvent.RateLocked, this.sHasOriginatorCompensationPlan))
            {
                this.ApplyCompensationFromCurrentOriginator();
            }
        }


        [DependsOn(nameof(sRateLockStatusT), nameof(sPpmtPenaltyMon), nameof(sRLckdExpiredD), nameof(sStatusT), nameof(sStatusLckd), nameof(sApprovD), nameof(sIsRateLocked), nameof(sNoteIR), nameof(sBrokComp1Pc), nameof(sRAdjMarginR), nameof(sTerm), nameof(sDue), nameof(sFinMethT), nameof(sIsRateLocked), nameof(sRLckdD), nameof(sRLckdExpiredD), nameof(sRLckdDays), nameof(sRateLockHistoryXmlDocument), nameof(sfLoadBrokerLockData), nameof(sLpTemplateNm), nameof(sfAddLockEvent), nameof(sfThrowErrorIfDuplicatesFound), nameof(sfProcessRateLockDisclosureTrigger), nameof(sfSet_sRLckdExpiredD_BasedOn), nameof(sHasOriginatorCompensationPlan))]
        [DevNote("Resume a lock after a suspension.")]
        public int sfResumeLock
        {
            get
            {
                return 0;
            }
        }

        /// <summary>
        /// Call this after you already assign all the fields
        /// </summary>
        public void ResumeLock(string userName, string lockReason)
        {
            if (sRateLockStatusT != E_sRateLockStatusT.LockSuspended)
            {
                CBaseException exc = new CBaseException(ErrorMessages.CannotResumeLockWhenRateNotSuspended, "We cannot allow resuming rate lock when the rate is not suspended.");
                exc.IsEmailDeveloper = false;
                throw exc;
            }

            ThrowErrorIfDuplicatesFound();

            //4/11/2014 bs - Add activity type for mobile spec v2.4
            LendersOffice.MobileApp.LatestActivity.Send(sLId, "Resume Lock", "4");

            sRateLockStatusT_ = E_sRateLockStatusT.Locked;

            AddLockEvent("ResumeLockEvent", userName, lockReason);
            ProcessRateLockDisclosureTrigger(E_RateLockDisclosureEventT.LoanLocked);

            //SetPricingAndAdjustmentsToAuditEvent(newEvent, doc);

            // OPM 64269 Apply plan when locking rate if no plan exists.
            if (this.BrokerDB.ShouldApplyCompPlan(E_LoanEvent.RateLocked, this.sHasOriginatorCompensationPlan))
            {
                this.ApplyCompensationFromCurrentOriginator();
            }
        }


        [DependsOn(nameof(sRateLockStatusT), nameof(sPpmtPenaltyMon), nameof(sRLckdExpiredD), nameof(sStatusT), nameof(sStatusLckd), nameof(sApprovD), nameof(sIsRateLocked), 
            nameof(sNoteIR), nameof(sBrokComp1Pc), nameof(sRAdjMarginR), nameof(sTerm), nameof(sDue), nameof(sFinMethT), nameof(sIsRateLocked), nameof(sRLckdD), nameof(sRLckdExpiredD), nameof(sRLckdDays), 
            nameof(sRateLockHistoryXmlDocument), nameof(sfLoadBrokerLockData), nameof(sLpTemplateNm), nameof(sBrokerLockRateSheetEffectiveD_DateTime), 
            nameof(sOptionArmTeaserR), nameof(sIsOptionArm), nameof(sBrokerLockBaseNoteIR), nameof(sBrokerLockBaseBrokComp1PcPrice), nameof(sBrokerLockBaseBrokComp1PcFee), 
            nameof(sBrokerLockBaseRAdjMarginR), nameof(sBrokerLockBaseOptionArmTeaserR), nameof(sBrokerLockBrokerBaseNoteIR), nameof(sBrokerLockBrokerBaseBrokComp1PcPrice), 
            nameof(sBrokerLockBrokerBaseBrokComp1PcFee), nameof(sBrokerLockBrokerBaseRAdjMarginR), nameof(sBrokerLockBrokerBaseOptionArmTeaserR), nameof(sBrokerLockTotHiddenAdjNoteIR), 
            nameof(sBrokerLockTotHiddenAdjBrokComp1PcPrice), nameof(sBrokerLockTotHiddenAdjBrokComp1PcFee), nameof(sBrokerLockTotHiddenAdjRAdjMarginR), nameof(sBrokerLockTotHiddenAdjOptionArmTeaserR), 
            nameof(sBrokerLockTotVisibleAdjNoteIR), nameof(sBrokerLockTotVisibleAdjBrokComp1PcPrice), nameof(sBrokerLockTotVisibleAdjBrokComp1PcFee), nameof(sBrokerLockTotVisibleAdjRAdjMarginR), 
            nameof(sBrokerLockTotVisibleAdjOptionArmTeaserR), nameof(sBrokerLockFinalBrokComp1PcPrice), nameof(sBrokerLockAdjustments), nameof(sRateLockHistoryXmlContent))]
        private bool sfAddLockEvent
        {
            get { return false; }
        }

        private void AddLockEvent(string eventName, string userName, string lockReason)
        {
            XmlDocument doc = sRateLockHistoryXmlDocument;
            XmlElement root = doc.DocumentElement;

            XmlElement newEvent = doc.CreateElement("Event");
            newEvent.SetAttribute("EvenType", eventName);
            newEvent.SetAttribute("EventId", Guid.NewGuid().ToString());
            newEvent.SetAttribute("EventDate", Tools.GetDateTimeNowString());
            newEvent.SetAttribute("DoneBy", userName);
            newEvent.SetAttribute("sNoteIR", sNoteIR_rep);
            newEvent.SetAttribute("sBrokComp1Pc", sBrokComp1Pc_rep);
            newEvent.SetAttribute("sRAdjMarginR", sRAdjMarginR_rep);
            newEvent.SetAttribute("sTerm", sTerm_rep);
            newEvent.SetAttribute("sDue", sDue_rep);
            newEvent.SetAttribute("sRLckdExpiredD", sRLckdExpiredD_rep);
            newEvent.SetAttribute("sPpmtPenaltyMon", sPpmtPenaltyMon_rep);
            newEvent.SetAttribute("sRLckBreakReasonDesc", lockReason);
            newEvent.SetAttribute("sRLckdD", sRLckdD_rep);
            newEvent.SetAttribute("sOptionArmTeaserR", sOptionArmTeaserR_rep);
            newEvent.SetAttribute("sIsOptionArm", sIsOptionArm ? "Yes" : "No");
            newEvent.SetAttribute("sLpTemplateNm", sLpTemplateNm);
            newEvent.SetAttribute("sRLckdDays", sRLckdDays_rep);

            string desc;
            switch (this.sFinMethT)
            {
                case E_sFinMethT.ARM: desc = "ARM"; break;
                case E_sFinMethT.Fixed: desc = "Fixed"; break;
                case E_sFinMethT.Graduated: desc = "Graduated Payment"; break;
                default:
                    Tools.LogBug("Unhanded enum value of sFinMethT in AddLockEvent()");
                    desc = "???";
                    break;
            }
            newEvent.SetAttribute("sAmortMethodT", desc);

            LoadBrokerLockData();

            // Pricing Data
            newEvent.SetAttribute("sBrokerLockBaseNoteIR", sBrokerLockBaseNoteIR_rep);
            newEvent.SetAttribute("sBrokerLockBaseBrokComp1PcPrice", sBrokerLockBaseBrokComp1PcPrice);
            newEvent.SetAttribute("sBrokerLockBaseBrokComp1PcFee", sBrokerLockBaseBrokComp1PcFee_rep);
            newEvent.SetAttribute("sBrokerLockBaseRAdjMarginR", sBrokerLockBaseRAdjMarginR_rep);
            newEvent.SetAttribute("sBrokerLockBaseOptionArmTeaserR", sBrokerLockBaseOptionArmTeaserR_rep);
            newEvent.SetAttribute("sBrokerLockBrokerBaseNoteIR", sBrokerLockBrokerBaseNoteIR_rep);
            newEvent.SetAttribute("sBrokerLockBrokerBaseBrokComp1PcPrice", sBrokerLockBrokerBaseBrokComp1PcPrice);
            newEvent.SetAttribute("sBrokerLockBrokerBaseBrokComp1PcFee", sBrokerLockBrokerBaseBrokComp1PcFee_rep);
            newEvent.SetAttribute("sBrokerLockBrokerBaseRAdjMarginR", sBrokerLockBrokerBaseRAdjMarginR_rep);
            newEvent.SetAttribute("sBrokerLockBrokerBaseOptionArmTeaserR", sBrokerLockBrokerBaseOptionArmTeaserR_rep);
            newEvent.SetAttribute("sBrokerLockTotHiddenAdjNoteIR", sBrokerLockTotHiddenAdjNoteIR_rep);
            newEvent.SetAttribute("sBrokerLockTotHiddenAdjBrokComp1PcPrice", sBrokerLockTotHiddenAdjBrokComp1PcPrice);
            newEvent.SetAttribute("sBrokerLockTotHiddenAdjBrokComp1PcFee", sBrokerLockTotHiddenAdjBrokComp1PcFee_rep);
            newEvent.SetAttribute("sBrokerLockTotHiddenAdjRAdjMarginR", sBrokerLockTotHiddenAdjRAdjMarginR_rep);
            newEvent.SetAttribute("sBrokerLockTotHiddenAdjOptionArmTeaserR", sBrokerLockTotHiddenAdjOptionArmTeaserR_rep);
            newEvent.SetAttribute("sBrokerLockTotVisibleAdjNoteIR", sBrokerLockTotVisibleAdjNoteIR_rep);
            newEvent.SetAttribute("sBrokerLockTotVisibleAdjBrokComp1PcPrice", sBrokerLockTotVisibleAdjBrokComp1PcPrice);
            newEvent.SetAttribute("sBrokerLockTotVisibleAdjBrokComp1PcFee", sBrokerLockTotVisibleAdjBrokComp1PcFee_rep);
            newEvent.SetAttribute("sBrokerLockTotVisibleAdjRAdjMarginR", sBrokerLockTotVisibleAdjRAdjMarginR_rep);
            newEvent.SetAttribute("sBrokerLockTotVisibleAdjOptionArmTeaserR", sBrokerLockTotVisibleAdjOptionArmTeaserR_rep);
            newEvent.SetAttribute("sBrokerLockFinalBrokComp1PcPrice", sBrokerLockFinalBrokComp1PcPrice_rep);
            newEvent.SetAttribute("sBrokerLockRateSheetEffectiveD", sBrokerLockRateSheetEffectiveD_DateTime);

            // List of Adjustments
            XmlElement adjustList = doc.CreateElement("Adjustments");

            foreach (PricingAdjustment adj in sBrokerLockAdjustments)
            {
                XmlElement adjustment = doc.CreateElement("Adjustment");
                adjustment.SetAttribute("Description", adj.Description);
                adjustment.SetAttribute("Rate", adj.Rate);
                adjustment.SetAttribute("Price", adj.Fee);
                adjustment.SetAttribute("Fee", adj.Fee);
                adjustment.SetAttribute("Margin", adj.Margin);
                adjustment.SetAttribute("TRate", adj.TeaserRate);
                adjustment.SetAttribute("IsHidden", adj.IsHidden ? "Yes" : "No");
                adjustList.AppendChild(adjustment);
            }
            newEvent.AppendChild(adjustList);

            root.AppendChild(newEvent);

            sRateLockHistoryXmlContent = doc.OuterXml;
        }

        [DependsOn(nameof(sRateLockHistoryXmlDocument), nameof(sRateLockStatusT), nameof(sNoteIRSubmitted), nameof(sRLckdD), nameof(sfSet_sRLckdExpiredD_Manually), nameof(sRateLockHistoryXmlContent))]
        public int sfBreakRateLock
        {
            get { return 0; }
        }

        public void BreakRateLock(string userName, string lockBreakReason)
        {
            if (sRateLockStatusT != E_sRateLockStatusT.Locked)
            {
                CBaseException exc = new CBaseException(ErrorMessages.CannotBreakRateLockWithoutRateLock, "We cannot allow breaking rate lock when the rate is not locked.");
                exc.IsEmailDeveloper = false;
                throw exc;
            }
            if (sNoteIRSubmitted > 0)
            {
                // 8/26/2009 dd - If there is a requested rate then change sRateLockStatusT to RateLockRequest. Relate to case 33800
                sRateLockStatusT_ = E_sRateLockStatusT.LockRequested;
            }
            else
            {
                sRateLockStatusT_ = E_sRateLockStatusT.NotLocked;
            }
            //4/11/2014 bs - Add activity type for mobile spec v2.4
            LendersOffice.MobileApp.LatestActivity.Send(sLId, "Rate Lock Broken: " + lockBreakReason, "4");
            sRLckdD_rep = "";
            //sRLckdExpiredD_rep = "";
            Set_sRLckdExpiredD_Manually("");

            XmlDocument doc = sRateLockHistoryXmlDocument;
            XmlElement root = doc.DocumentElement;

            XmlElement newEvent = doc.CreateElement("Event");
            newEvent.SetAttribute("EvenType", "BreakLockEvent");
            newEvent.SetAttribute("EventDate", Tools.GetDateTimeNowString());
            newEvent.SetAttribute("DoneBy", userName);
            newEvent.SetAttribute("sRLckBreakReasonDesc", lockBreakReason);

            root.AppendChild(newEvent);

            sRateLockHistoryXmlContent = doc.OuterXml;
        }


        [DependsOn(nameof(sRateLockHistoryXmlDocument), nameof(sRateLockStatusT), nameof(sNoteIRSubmitted), nameof(sRateLockHistoryXmlContent))]
        [DevNote("Suspend a lock.")]
        public int sfSuspendLock
        {
            get { return 0; }
        }

        public void SuspendLock(string userName, string suspendReason)
        {
            if (sRateLockStatusT != E_sRateLockStatusT.Locked)
            {
                CBaseException exc = new CBaseException(ErrorMessages.CannotSuspendLockWithoutRateLock, "We cannot allow suspending rate lock when the rate is not locked.");
                exc.IsEmailDeveloper = false;
                throw exc;
            }
                
            sRateLockStatusT_ = E_sRateLockStatusT.LockSuspended;
            
            //4/11/2014 bs - Add activity type for mobile spec v2.4
            LendersOffice.MobileApp.LatestActivity.Send(sLId, "Rate Lock Suspended: " + suspendReason, "4");
           

            XmlDocument doc = sRateLockHistoryXmlDocument;
            XmlElement root = doc.DocumentElement;

            XmlElement newEvent = doc.CreateElement("Event");
            newEvent.SetAttribute("EvenType", "SuspendLockEvent");
            newEvent.SetAttribute("EventDate", Tools.GetDateTimeNowString());
            newEvent.SetAttribute("DoneBy", userName);
            newEvent.SetAttribute("sRLckBreakReasonDesc", suspendReason);

            root.AppendChild(newEvent);

            sRateLockHistoryXmlContent = doc.OuterXml;
        }




        //opm 19109 fs 06/18/08
        [DependsOn(nameof(sNoteIRSubmitted), nameof(sLOrigFPcSubmitted), nameof(sRAdjMarginRSubmitted), nameof(sIsOptionArmSubmitted), nameof(sOptionArmTeaserRSubmitted), 
            nameof(sQualIRSubmitted), nameof(sOptionArmTeaserR), nameof(sIsOptionArmSubmitted), nameof(sTermSubmitted), nameof(sDueSubmitted), nameof(sFinMethTSubmitted), 
            nameof(sRateLockStatusT), nameof(sLpTemplateNmSubmitted), nameof(sRateLockHistoryXmlContent), nameof(sRateLockHistoryXmlDocument), nameof(sMostRecentRegistrationD))]
        public int sfRemoveRequestedRate
        {
            get { return 0; }
        }

        public void RemoveRequestedRate()
        {
            sRateLockStatusT_ = E_sRateLockStatusT.NotLocked;
            sNoteIRSubmitted_rep = "";
            sLOrigFPcSubmitted_rep = "";
            sRAdjMarginRSubmitted_rep = "";
            sIsOptionArmSubmitted = false;
            sOptionArmTeaserRSubmitted_rep = "";
            sQualIRSubmitted_rep = "";
            sLpTemplateNmSubmitted = "";
            sMostRecentRegistrationD = CDateTime.Create((DateTime?)null);


            XmlDocument doc = sRateLockHistoryXmlDocument;
            XmlElement root = doc.DocumentElement;

            string userName = PrincipalFactory.CurrentPrincipal.DisplayName;

            XmlElement newEvent = doc.CreateElement("Event");
            newEvent.SetAttribute("EvenType", "RemoveRequestedRateEvent");
            newEvent.SetAttribute("EventDate", Tools.GetDateTimeNowString());
            newEvent.SetAttribute("DoneBy", userName);

            root.AppendChild(newEvent);

            sRateLockHistoryXmlContent = doc.OuterXml;
        }

        #endregion

        #region AutoRateLock
        // OPM 105723 related

        [DependsOn(nameof(sIsRateLocked), nameof(sStatusT), nameof(sRLckdExpiredD), nameof(sLockPolicy))]
        public bool sIsRateLockExtentionAllowed
        {
            get 
            {
                //  4.1	Extend Lock Link
                //	Hide if “Automatically Approve Lock Extensions” is bool_no for the lock policy associated with the loan’s price group.
                //	Hide if the loan file’s rate lock status is NOT “Locked”.
                //	Hide if today’s date is not LESS than or equal to the loan file’s lock expiration date.
                //	Hide if loan file status is set to “Loan Canceled” or “Loan Denied”.

                if (BrokerDB.HasFeatures(E_BrokerFeatureT.PriceMyLoan) == false)
                    return false;

                if (sIsRateLocked == false)
                    return false;

                E_sStatusT status = sStatusT;
                if (status == E_sStatusT.Loan_Canceled
                    || status == E_sStatusT.Loan_Rejected)
                    return false;

                if (false == DateTime.Now.Date <= sRLckdExpiredD.DateTimeForComputation)
                    return false;

                // Only load policy if no other reason to reject
                if (sLockPolicy.IsCodeBased)
                {
                    return sLockPolicy.GetCustomLockPolicy().IsAllowLockExtension(sLId);
                }
                else
                {
                    return sLockPolicy.EnableAutoLockExtensions;
                }
            }
        }

        [DependsOn(nameof(sIsRateLocked), nameof(sStatusT), nameof(sRLckdExpiredD), nameof(sLockPolicy))]
        public bool sIsRateLockFloatDownAllowed
        {
            get
            {
                //  4.2	Float Down Lock Link
                //	Hide if “Automatically Approve Float Downs” is bool_no for the lock policy associated with the loan’s price group.
                //	Hide if the loan file’s rate lock status is NOT “Locked”.
                //	Hide if today’s date is not LESS than or equal to the loan file’s lock expiration date.
                //	Hide if loan file status is set to “Loan Canceled” or “Loan Denied”.

                if (BrokerDB.HasFeatures(E_BrokerFeatureT.PriceMyLoan) == false)
                    return false;

                if (sIsRateLocked == false)
                    return false;

                E_sStatusT status = sStatusT;
                if (status == E_sStatusT.Loan_Canceled
                    || status == E_sStatusT.Loan_Rejected)
                    return false;

                if (false == DateTime.Now.Date <= sRLckdExpiredD.DateTimeForComputation)
                    return false;

                if (sLockPolicy.IsCodeBased)
                {
                    return sLockPolicy.GetCustomLockPolicy().IsAllowFloatDown(sLId);
                }
                else
                {
                    // Only load policy if no other reason to reject
                    return sLockPolicy.EnableAutoFloatDowns;
                }
            }
        }

        [DependsOn(nameof(sIsRateLocked), nameof(sRLckdExpiredD), nameof(sStatusT), nameof(sLockPolicy))]
        public bool sIsRateReLockAllowed
        {
            get
            {
                //  4.3	Rate Re-Lock Link
                // We want the rate re-lock link to only display when rate re-locks are set to automatically be approved, 
                // the loan has a “Locked” rate lock status, and when a rate re-lock would be needed due to a canceled or expired rate lock. 
                // Therefore the following logic for the link should be implemented:
                //	Hide if “Automatically Approve Rate Re-Locks” is bool_no for the lock policy associated with the loan’s price group.
                //	Hide if the loan file’s rate lock status is NOT “Locked”.
                //	Hide if today’s date is less than, or equal to, the loan file’s lock expiration date AND the loan status is not set to “Canceled”.

                if (BrokerDB.HasFeatures(E_BrokerFeatureT.PriceMyLoan) == false)
                    return false;

                if (sIsRateLocked == false)
                    return false;

                if (sLockPolicy.IsCodeBased)
                {
                    return sLockPolicy.GetCustomLockPolicy().IsAllowRelock(sLId);
                }
                else
                {
                    if (DateTime.Now.Date <= sRLckdExpiredD.DateTimeForComputation
                        && sStatusT != E_sStatusT.Loan_Canceled)
                        return false;

                    // Only load policy if no other reason to reject
                    return sLockPolicy.EnableAutoReLocks;
                }
            }
        }

        /// <summary>
        ///	The original lock expiration date for the loan file
        /// </summary>
        [DependsOn(nameof(RateLockHistoryList))]
        public CDateTime sOriginalLockExpirationD
        {
            get 
            {
                var firstLock = RateLockHistoryList.Where(le => le.sRateLockAuditActionT == E_RateLockAuditActionT.LockRate).Reverse().FirstOrDefault();

                if (firstLock != null)
                {
                    DateTime firstLockExpireDate;
                    if (DateTime.TryParse(firstLock.sRLckdExpiredD, out firstLockExpireDate))
                    {
                        return CDateTime.Create(firstLockExpireDate);
                    }
                }

                return CDateTime.InvalidWrapValue; 
            }
        }
        
        public string sOriginalLockExpirationD_rep
        {
            get { return sOriginalLockExpirationD.ToString(m_convertLos); }
        }

        /// <summary>
        /// Get the lock period at the time of lock. Don't include number of lock extension days here.
        /// Example:
        ///     Original Lock Request @ 30 days
        ///     Lock Extension for 10 days
        ///     sRLckdDays = 40
        ///     However sRLckdDaysAtLock should be 30 days.
        /// </summary>
        [DependsOn(nameof(RateLockHistoryList))]
        public int sRLckdDaysAtLock
        {
            get
            {
                foreach (var o in RateLockHistoryList)
                {
                    if (o.sRateLockAuditActionT == E_RateLockAuditActionT.LockRate)
                    {
                        return int.Parse(o.sRLckdDays);
                    }
                }
                return 0;
            }
        }
        public string sRLckdDaysAtLock_rep
        {
            get { return ToCountString(() => sRLckdDaysAtLock); }
        }
        /// <summary>
        /// The total number of times the file has already had its lock extended.
        /// </summary>
        [DependsOn(nameof(RateLockHistoryList), nameof(sBrokerLockAdjustments), nameof(sLockPolicy))]
        public int sTotalLockExtensions
        {
            get
            {
                int count = 0;

                if (sLockPolicy.IsCodeBased)
                {
                    // 1/14/2014 dd - For code based policy look in adjustment
                    foreach (var o in sBrokerLockAdjustments)
                    {
                        if (o.PricingAdjustmentT == E_PricingAdjustmentT.LockExtension)
                        {
                            count++;
                        }
                    }
                }
                else
                {

                    // Fall back to look in rate lock history.
                    for (int i = 0; i < RateLockHistoryList.Count; i++)
                    {
                        RateLockHistoryItem historyEvent = RateLockHistoryList[i];
                        if (historyEvent.sRateLockAuditActionT == E_RateLockAuditActionT.BreakRateLock)
                            break;

                        if (historyEvent.sRateLockAuditActionT == E_RateLockAuditActionT.ExtendRateLock)
                        {
                            count++;
                        }

                    }
                }
                return count;
            }

        }
        public string sTotalLockExtensions_rep
        {
            get { return ToCountString(() => sTotalLockExtensions); }
        }

        /// <summary>
        /// The total number of days in lock extension previously applied to the loan file.
        /// Note that it cannot be reliably calculated by comparing the current lock extension date 
        /// against the original lock extension date because lock extensions of a certain number 
        /// of days that fall on a weekend/holiday are adjusted. 
        /// </summary>
        [DependsOn(nameof(RateLockHistoryList), nameof(sBrokerLockAdjustments), nameof(sLockPolicy))]
        public int sTotalDaysLockExtended
        {
            get
            {
                int days = 0;
                if (sLockPolicy.IsCodeBased)
                {
                    // 1/13/2014 dd - Tried to look in adjustment list first.
                    foreach (var o in sBrokerLockAdjustments)
                    {
                        if (o.PricingAdjustmentT == E_PricingAdjustmentT.LockExtension)
                        {
                            days += o.ExtensionDays;
                        }
                    }
                }
                else 
                {

                // 1/13/2014 dd - Fall back to old behavior, look in rate lock history
                    for (int i = 0; i < RateLockHistoryList.Count; i++)
                    {
                        RateLockHistoryItem historyEvent = RateLockHistoryList[i];
                        if (historyEvent.sRateLockAuditActionT == E_RateLockAuditActionT.LockRate)
                            break;

                        if (historyEvent.sRateLockAuditActionT == E_RateLockAuditActionT.ExtendRateLock)
                        {
                            if (i + 1 < RateLockHistoryList.Count)
                            {
                                RateLockHistoryItem previousEvent = RateLockHistoryList[i + 1];

                                DateTime extentionExpiration;
                                DateTime previousExpiration;
                                if (DateTime.TryParse(historyEvent.sRLckdExpiredD, out extentionExpiration)
                                    && DateTime.TryParse(previousEvent.sRLckdExpiredD, out previousExpiration))
                                {
                                    days += (extentionExpiration - previousExpiration).Days;
                                }
                            }
                        }
                    }
                }
                return days;
            }

        }
        public string sTotalDaysLockExtended_rep
        {
            get { return ToCountString(() => sTotalDaysLockExtended); }
        }


        /// <summary>
        /// Number of times the file has had a Float Down performed on it.
        /// </summary>
        [DependsOn(nameof(RateLockHistoryList), nameof(sBrokerLockAdjustments), nameof(sLockPolicy))]
        public int sTotalRateLockFloatDowns
        {
            get
            {
                int count = 0;

                if (sLockPolicy.IsCodeBased)
                {
                    // 1/13/2014 dd - Tried to look in adjustment list first.
                    foreach (var o in sBrokerLockAdjustments)
                    {
                        if (o.PricingAdjustmentT == E_PricingAdjustmentT.RolldownFee)
                        {
                            count++;
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < RateLockHistoryList.Count; i++)
                    {
                        RateLockHistoryItem historyEvent = RateLockHistoryList[i];
                        if (historyEvent.sRateLockAuditActionT == E_RateLockAuditActionT.LockRate)
                            break;

                        if (historyEvent.sRateLockAuditActionT == E_RateLockAuditActionT.FloatDownRate)
                        {
                            count++;
                        }

                    }
                }
                return count;

            }
        }
        public string sTotalRateLockFloatDowns_rep
        {
            get { return ToCountString(() => sTotalRateLockFloatDowns); }
        }

        /// <summary>
        /// The number of times the file has done a Rate Re-Lock.
        /// </summary>
        [DependsOn(nameof(RateLockHistoryList))]
        public int sTotalRateReLocks
        {
            get
            {
                int count = 0;

                for (int i = 0; i < RateLockHistoryList.Count; i++)
                {
                    RateLockHistoryItem historyEvent = RateLockHistoryList[i];
                    if (historyEvent.sRateLockAuditActionT == E_RateLockAuditActionT.LockRate)
                        break;

                    if (historyEvent.sRateLockAuditActionT == E_RateLockAuditActionT.ReLockRate)
                    {
                        count++;
                    }

                }
                return count;

            }
        }
        public string sTotalRateReLocks_rep
        {
            get { return ToCountString(() => sTotalRateReLocks); }
        }

        [DependsOn(nameof(RateLockHistoryList), nameof(sIsRateLocked))]
        [DevNote("Returns if the current lock has been modified.")]
        public bool sIsCurrentLockUpdated
        {
            get
            {
                // OPM 125577. 

                if (sIsRateLocked == false)
                    return false;

                // Going from most recent action, if there is a 
                // lock modification between now and the most recent lock,
                // this lock is considered modified.
                for (int i = 0; i < RateLockHistoryList.Count; i++)
                {
                    RateLockHistoryItem historyEvent = RateLockHistoryList[i];
                    if (historyEvent.sRateLockAuditActionT == E_RateLockAuditActionT.LockRate)
                    {
                        return false;
                    }

                    if (historyEvent.sRateLockAuditActionT == E_RateLockAuditActionT.ResumeLock)
                    {
                        return true;
                    }

                }
                
                return false;

            }
        }


        private IList<RateLockHistoryItem> x_rateLockHistory = null;

        [DependsOn(nameof(sRateLockHistoryXmlContent))]
        private IList<RateLockHistoryItem> RateLockHistoryList
        {
            get
            {
                if (x_rateLockHistory == null)
                {
                    x_rateLockHistory = RateLockHistoryItem.GetSortedRateHistory(sRateLockHistoryXmlContent);
                }
                return x_rateLockHistory;
            }
        }

        [DevNote("Returns the most recent Auto Lock Event.")]
        [DependsOn(nameof(RateLockHistoryList))]
        public RateLockHistoryItem sMostRecentAutoRateLockEvent
        {
            get
            {
                return RateLockHistoryList.FirstOrDefault(p => string.Equals("Rate Locked", p.Action) && string.Equals("AUTO LOCK", p.DoneBy));
            } 
        }

        // Point minus the extention-related fees (used in calcuating the market movement).

        [DependsOn(nameof(sBrokComp1Pc), nameof(sTotalLockFees))]
        public decimal sPointLessLockFees
        {
            get
            {
                return sBrokComp1Pc - sTotalLockFees ;
            }
        }

        [DependsOn(nameof(sBrokerLockAdjustments))]
        public decimal sTotalLockFees
        {
            get
            {
                decimal sum = 0;
                // Not ideal to search by name, but name is not editable when locked.
                foreach (var adjustment in sBrokerLockAdjustments.Where(
                    p => p.Description.StartsWith("LOCK EXTENSION")
                        || p.Description.StartsWith("FLOAT DOWN")
                        || p.Description.StartsWith("RE-LOCK FEE")
                        || p.Description.StartsWith("WORST-CASE MARKET ADJUSTMENT")
                        ))
                {
                    decimal fee;
                    if (decimal.TryParse(adjustment.Fee, out fee))
                    {
                        sum += fee;
                    }
                }
                return sum;
            }
        }

        [DependsOn(nameof(sBrokerLockAdjustments))]
        public decimal sTotalNonFloatLockFees
        {
            get
            {
                decimal sum = 0;
                // Not ideal to search by name, but name is not editable when locked.
                foreach (var adjustment in sBrokerLockAdjustments.Where(
                    p => p.Description.StartsWith("LOCK EXTENSION")
                        || p.Description.StartsWith("FLOAT DOWN FEE")
                        || p.Description.StartsWith("RE-LOCK FEE")
                        || p.Description.StartsWith("WORST-CASE MARKET ADJUSTMENT")))
                {
                    decimal fee;
                    if (decimal.TryParse(adjustment.Fee, out fee))
                    {
                        sum += fee;
                    }
                }
                return sum;
            }
        }

        [DependsOn(nameof(sBrokerLockAdjustments))]
        public decimal sTotalNonFloatRelockFees
        {
            get
            {
                decimal sum = 0;
                // Not ideal to search by name, but name is not editable when locked.
                foreach (var adjustment in sBrokerLockAdjustments.Where(
                    p => p.Description.StartsWith("LOCK EXTENSION")
                        || p.Description.StartsWith("FLOAT DOWN FEE")
                        || p.Description.StartsWith("RE-LOCK FEE")))
                {
                    decimal fee;
                    if (decimal.TryParse(adjustment.Fee, out fee))
                    {
                        sum += fee;
                    }
                }
                return sum;
            }
        }

        
        private XDocument x_sPmlCertXmlDoc = null;
        [DependsOn(nameof(sPmlCertXmlContent))]
        private XDocument sPmlCertXmlDoc
        {
            get 
            {
                if (x_sPmlCertXmlDoc == null)
                {
                    x_sPmlCertXmlDoc = XDocument.Load(new XmlNodeReader(Tools.CreateXmlDoc(sPmlCertXmlContent.Value)));
                }
                return x_sPmlCertXmlDoc;
            }
        }

        [DependsOn(nameof(sPmlCertXmlDoc), nameof(sIsQualifiedForOriginatorCompensation), nameof(sOriginatorCompensationPaymentSourceT), nameof(sOriginatorCompensationLenderFeeOptionT))]
        public IDictionary<decimal, decimal> sSubmittedRatePrices
        {
            get
            {
                Dictionary<decimal,decimal> rateOptions = new Dictionary<decimal,decimal>();
                bool isQualifiedForOriginatorComp = sIsQualifiedForOriginatorCompensation
                        && sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid
                        && sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees;


                var options = from option in sPmlCertXmlDoc.XPathSelectElements("//pricing/rate_options/rate_option")
                               select new KeyValuePair<decimal, decimal>
                               (Decimal.Parse(option.Element("rate").Value),
                               Decimal.Parse(option.Element(isQualifiedForOriginatorComp ? "comp_point" : "point").Value));

                foreach (var option in options)
                {
                    if (!rateOptions.ContainsKey(option.Key))
                    {
                        rateOptions.Add(option.Key, option.Value);
                    }
                }

                return rateOptions;
                
            }
        }

        [DependsOn(nameof(sPmlCertXmlDoc))]
        public bool sSubmittedRatePriceIsInPriceFormat
        {
            get
            {
                var optionsElement = sPmlCertXmlDoc.XPathSelectElement("//pricing/rate_options");
                if (optionsElement != null && optionsElement.Attribute("point_basis") != null)
                {

                    return optionsElement.Attribute("point_basis").Value == "100";
                }

                return false;
            }
        }

        #endregion

        #endregion

        #region Investor Rate Lock

        /// <summary>
        /// Investor Name for investor rate lock.
        /// </summary>
        [DependsOn(nameof(sIsInvestorRateLocked), nameof(sInvestorLockLpInvestorNm))]
        public string sInvestorLockLpInvestorNm
        {
            // 6/12/2014 dd - We are no longer allow user to set sInvestorLockLpInvestorNm manually.
            // User must select sInvestorRolodexId. See OPM 177694.
            get { return GetStringVarCharField("sInvestorLockLpInvestorNm"); }
            private set 
            {
                string current = GetStringVarCharField("sInvestorLockLpInvestorNm");

                if (sIsInvestorRateLocked && value != current)
                {
                    LogBug(ErrorMessages.CannotChangeNameDuringInvestorRateLock);
                    CBaseException exc = new CBaseException(ErrorMessages.CannotChangeNameDuringInvestorRateLock, ErrorMessages.CannotChangeNameDuringInvestorRateLock);
                    exc.IsEmailDeveloper = false;
                    throw exc;
                }

                SetStringVarCharField("sInvestorLockLpInvestorNm", value); 
            }
        }

        /// <summary>
        /// Investor Program for investor rate lock
        /// </summary>
        [DependsOn(nameof(sInvestorLockLpTemplateNm), nameof(sIsInvestorRateLocked))]
        public string sInvestorLockLpTemplateNm
        {
            get { return GetStringVarCharField("sInvestorLockLpTemplateNm"); }
            set 
            {
                string current = GetStringVarCharField("sInvestorLockLpTemplateNm");
                
                if (sIsInvestorRateLocked && value != current)
                {
                    LogBug(ErrorMessages.CannotChangeProgramDuringInvestorRateLock);
                    CBaseException exc = new CBaseException(ErrorMessages.CannotChangeProgramDuringInvestorRateLock, ErrorMessages.CannotChangeProgramDuringInvestorRateLock);
                    exc.IsEmailDeveloper = false;
                    throw exc;
                }

                SetStringVarCharField("sInvestorLockLpTemplateNm", value); 
            }
        }

        /// <summary>
        /// Status of investor rate lock.
        /// </summary>
        /// <seealso cref="CPageBase.sIsInvestorRateLocked"/>
        [DependsOn]
        public E_sInvestorLockRateLockStatusT sInvestorLockRateLockStatusT
        {
            get { return (E_sInvestorLockRateLockStatusT)GetTypeIndexField("sInvestorLockRateLockStatusT"); }
        }
        public string sInvestorLockRateLockStatusT_rep
        {
            get
            {
                switch (sInvestorLockRateLockStatusT)
                {
                    case E_sInvestorLockRateLockStatusT.Locked:
                        return "Locked";
                    case E_sInvestorLockRateLockStatusT.NotLocked:
                        return "Not Locked";
                    case E_sInvestorLockRateLockStatusT.LockSuspended:
                        return "Lock Suspended";
                    default:
                        throw new UnhandledEnumException(sInvestorLockRateLockStatusT);
                }
            }
        }
        private E_sInvestorLockRateLockStatusT sInvestorLockRateLockStatusT_
        {
            set { SetTypeIndexField("sInvestorLockRateLockStatusT", (int)value); }
        }

        /// <summary>
        /// True if investor rate is locked, otherwise false.
        /// </summary>
        [DependsOn(nameof(sInvestorLockRateLockStatusT))]
        public bool sIsInvestorRateLocked
        {
            get 
            {
                switch (sInvestorLockRateLockStatusT)
                {
                    case E_sInvestorLockRateLockStatusT.Locked:
                        return true;
                    case E_sInvestorLockRateLockStatusT.NotLocked:
                    case E_sInvestorLockRateLockStatusT.LockSuspended:
                        return false;
                    default:
                        throw new UnhandledEnumException(sInvestorLockRateLockStatusT);
                }
            }
        }
        /// <summary>
        /// Investor Lock period in days.
        /// </summary>
        [DependsOn(nameof(sIsInvestorRateLocked), nameof(sInvestorLockRLckdDays))]
        public int sInvestorLockRLckdDays
        {
            get { return GetCountField("sInvestorLockRLckdDays"); }
            set
            {
                int current;

                try { current = sInvestorLockRLckdDays; }
                catch { current = 0; }

                if (!m_extendingInvestorRateLock && sIsInvestorRateLocked && value != current)
                {
                    LogBug(ErrorMessages.CannotChangeLockPeriodDuringInvestorRateLock);
                    CBaseException exc = new CBaseException(ErrorMessages.CannotChangeLockPeriodDuringInvestorRateLock, ErrorMessages.CannotChangeLockPeriodDuringInvestorRateLock);
                    exc.IsEmailDeveloper = false;
                    throw exc;
                }

                SetCountField("sInvestorLockRLckdDays", value);
            }
        }
        public string sInvestorLockRLckdDays_rep
        {
            get { return ToCountString(() => sInvestorLockRLckdDays); }
            set { sInvestorLockRLckdDays = ToCount(value); }
        }

        /// <summary>
        /// Investor rate lock date.
        /// </summary>
        [DependsOn(nameof(sInvestorLockRLckdD), nameof(sIsInvestorRateLocked), nameof(sInvestorLockRateLockStatusT))]
        public CDateTime sInvestorLockRLckdD
        {
            get { return GetDateTimeField("sInvestorLockRLckdD"); }
            set
            {
                CDateTime current;

                try { current = sInvestorLockRLckdD; }
                catch { current = CDateTime.InvalidWrapValue; }

                if ( (sIsInvestorRateLocked || sInvestorLockRateLockStatusT == E_sInvestorLockRateLockStatusT.LockSuspended) && current.CompareTo(value, true) != 0)
                {
                    LogBug(ErrorMessages.CannotChangeLockDateDuringInvestorRateLock);
                    CBaseException exc = new CBaseException(ErrorMessages.CannotChangeLockDateDuringInvestorRateLock, ErrorMessages.CannotChangeLockDateDuringInvestorRateLock);
                    exc.IsEmailDeveloper = false;
                    throw exc;
                }

                SetDateTimeField("sInvestorLockRLckdD", value);
            }
        }
        public string sInvestorLockRLckdD_rep
        {
            get { return GetDateTimeField_rep("sInvestorLockRLckdD"); }
            set
            {
                if (sIsInvestorRateLocked && sInvestorLockRLckdD_rep != value)
                {
                    LogBug(ErrorMessages.CannotChangeLockDateDuringInvestorRateLock);
                    CBaseException exc = new CBaseException(ErrorMessages.CannotChangeLockDateDuringInvestorRateLock, ErrorMessages.CannotChangeLockDateDuringInvestorRateLock);
                    exc.IsEmailDeveloper = false;
                    throw exc;
                }
                SetDateTimeField_rep("sInvestorLockRLckdD", value);
            }
        }
        /// <summary>
        /// Investor rate lock expiration date.
        /// </summary>
        [DependsOn(nameof(sInvestorLockRLckExpiredD), nameof(sInvestorLockRLckExpiredDLckd), nameof(sIsInvestorRateLocked), nameof(sInvestorLockRLckdD), nameof(sInvestorLockRLckdDays), nameof(sInvestorLockRateLockStatusT))]
        public CDateTime sInvestorLockRLckExpiredD
        {
            get 
            {
                if (sInvestorLockRLckExpiredDLckd)
                {
                    return GetDateTimeField("sInvestorLockRLckExpiredD");
                }
                else if (sInvestorLockRLckdD.IsValid)
                {
                    return CDateTime.Create(sInvestorLockRLckdD.DateTimeForComputation.AddDays(sInvestorLockRLckdDays));
                }
                else
                {
                    return CDateTime.InvalidWrapValue;
                }
            }

            set
            {
                CDateTime current;

                try { current = sInvestorLockRLckExpiredD; }
                catch { current = CDateTime.InvalidWrapValue; }

                if (!m_extendingInvestorRateLock && ( sIsInvestorRateLocked || sInvestorLockRateLockStatusT == E_sInvestorLockRateLockStatusT.LockSuspended ) && current.CompareTo(value, true) != 0)
                {
                    LogBug(ErrorMessages.CannotChangeLockExpirationDuringInvestorRateLock);
                    CBaseException exc = new CBaseException(ErrorMessages.CannotChangeLockExpirationDuringInvestorRateLock, ErrorMessages.CannotChangeLockExpirationDuringInvestorRateLock + " New Value=" + value + ", Old Value=" + sInvestorLockRLckExpiredD_rep);
                    exc.IsEmailDeveloper = false;
                    throw exc;
                }

                SetDateTimeField("sInvestorLockRLckExpiredD", value);
            }
        }
        public string sInvestorLockRLckExpiredD_rep
        {
            get { return sInvestorLockRLckExpiredD.ToString(this.m_convertLos); }
            set
            {
                if (!m_extendingInvestorRateLock && sIsInvestorRateLocked && sInvestorLockRLckExpiredD_rep != value)
                {
                    LogBug(ErrorMessages.CannotChangeLockExpirationDuringInvestorRateLock);
                    CBaseException exc = new CBaseException(ErrorMessages.RateLockExpirationCannotBeChangedDuringRateLock, ErrorMessages.CannotChangeLockExpirationDuringInvestorRateLock + " - New Value=" + value + ", Old Value=" + sInvestorLockRLckExpiredD_rep);
                    exc.IsEmailDeveloper = false;
                    throw exc;
                }
                SetDateTimeField_rep("sInvestorLockRLckExpiredD", value);
            }
        }

        /// <summary>
        /// True if there is a locked value for rate lock expiration date.
        /// </summary>
        [DependsOn]
        public bool sInvestorLockRLckExpiredDLckd
        {
            get { return GetBoolField("sInvestorLockRLckExpiredDLckd"); }
            set { SetBoolField("sInvestorLockRLckExpiredDLckd", value); }
        }

        /// <summary>
        /// Investor Rate Lock delivery expiration date
        /// </summary>
        [DependsOn(nameof(sInvestorLockDeliveryExpiredD), nameof(sInvestorLockDeliveryExpiredDLckd), nameof(sIsInvestorRateLocked), nameof(sInvestorLockRLckdD), nameof(sInvestorLockRLckdDays), nameof(sInvestorLockRateLockStatusT))]
        public CDateTime sInvestorLockDeliveryExpiredD
        {
            get
            {
                if (sInvestorLockDeliveryExpiredDLckd)
                {
                    return GetDateTimeField("sInvestorLockDeliveryExpiredD");
                }
                else if (sInvestorLockRLckdD.IsValid)
                {
                    return CDateTime.Create(sInvestorLockRLckdD.DateTimeForComputation.AddDays(sInvestorLockRLckdDays));
                }
                else
                {
                    return CDateTime.InvalidWrapValue;
                }
            }

            set
            {
                CDateTime current;

                try { current = sInvestorLockDeliveryExpiredD; }
                catch { current = CDateTime.InvalidWrapValue; }

                if (!m_extendingInvestorRateLock && (sIsInvestorRateLocked || sInvestorLockRateLockStatusT == E_sInvestorLockRateLockStatusT.LockSuspended ) && current.CompareTo(value, true) != 0)
                {
                    LogBug(ErrorMessages.CannotChangeLockDeliveryExpirationDuringInvestorRateLock);
                    CBaseException exc = new CBaseException(ErrorMessages.CannotChangeLockDeliveryExpirationDuringInvestorRateLock, ErrorMessages.CannotChangeLockDeliveryExpirationDuringInvestorRateLock + " - New Value=" + value + ", Old Value=" + sInvestorLockDeliveryExpiredD_rep);
                    exc.IsEmailDeveloper = false;
                    throw exc;
                }

                SetDateTimeField("sInvestorLockDeliveryExpiredD", value);
            }
        }
        public string sInvestorLockDeliveryExpiredD_rep
        {
            get { return sInvestorLockDeliveryExpiredD.ToString(this.m_convertLos); }
            set
            {
                if (!m_extendingInvestorRateLock && sIsInvestorRateLocked && sInvestorLockDeliveryExpiredD_rep != value)
                {
                    LogBug(ErrorMessages.CannotChangeLockDeliveryExpirationDuringInvestorRateLock);
                    CBaseException exc = new CBaseException(ErrorMessages.RateLockExpirationCannotBeChangedDuringRateLock, ErrorMessages.CannotChangeLockDeliveryExpirationDuringInvestorRateLock + " - New Value=" + value + ", Old Value=" + sInvestorLockDeliveryExpiredD_rep);
                    exc.IsEmailDeveloper = false;
                    throw exc;
                }
                SetDateTimeField_rep("sInvestorLockDeliveryExpiredD", value);
            }
        }

        /// <summary>
        /// True if investor rate lock delivery expiration date is locked.
        /// </summary>
        [DependsOn]
        public bool sInvestorLockDeliveryExpiredDLckd
        {
            get { return GetBoolField("sInvestorLockDeliveryExpiredDLckd"); }
            set { SetBoolField("sInvestorLockDeliveryExpiredDLckd", value); }
        }
        /// <summary>
        /// Investor Rate Lock's 'Investor Loan #'
        /// </summary>
        [DependsOn(nameof(sInvestorLockLoanNum), nameof(sIsInvestorRateLocked))]
        public string sInvestorLockLoanNum
        {
            get { return GetStringVarCharField("sInvestorLockLoanNum"); }
            set 
            {
                if (sIsInvestorRateLocked && sInvestorLockLoanNum != value)
                {
                    LogBug(ErrorMessages.CannotChangeLoanNumberDuringInvestorRateLock);
                    CBaseException exc = new CBaseException(ErrorMessages.CannotChangeLoanNumberDuringInvestorRateLock, ErrorMessages.CannotChangeLoanNumberDuringInvestorRateLock);
                    exc.IsEmailDeveloper = false;
                    throw exc;
                }

                SetStringVarCharField("sInvestorLockLoanNum", value); 
            }
        }

        [DevNote("The investor loan program identifier.")]
        [DependsOn(nameof(sInvestorLockProgramId), nameof(sIsInvestorRateLocked))]
        public string sInvestorLockProgramId
        {
            get { return GetStringVarCharField("sInvestorLockProgramId"); }
            set
            {
                if (sIsInvestorRateLocked && sInvestorLockProgramId != value)
                {
                    LogBug(ErrorMessages.CannotChangeProgramIdDuringInvestorRateLock);
                    CBaseException exc = new CBaseException(ErrorMessages.CannotChangeProgramIdDuringInvestorRateLock, ErrorMessages.CannotChangeProgramIdDuringInvestorRateLock);
                    exc.IsEmailDeveloper = false;
                    throw exc;
                }

                SetStringVarCharField("sInvestorLockProgramId", value);
            }
        }

        /// <summary>
        /// Investor Rate Lock's 'Investor Lock Confirmation #'
        /// </summary>
        [DependsOn(nameof(sInvestorLockConfNum), nameof(sIsInvestorRateLocked))]
        public string sInvestorLockConfNum
        {
            get { return GetStringVarCharField("sInvestorLockConfNum"); }
            set
            {
                if (sIsInvestorRateLocked && sInvestorLockConfNum != value)
                {
                    LogBug(ErrorMessages.CannotChangeLockConfirmationNumberDuringInvestorRateLock);
                    CBaseException exc = new CBaseException(ErrorMessages.CannotChangeLockConfirmationNumberDuringInvestorRateLock, ErrorMessages.CannotChangeLockConfirmationNumberDuringInvestorRateLock);
                    exc.IsEmailDeveloper = false;
                    throw exc;
                }

                SetStringVarCharField("sInvestorLockConfNum", value);
            }
        }

        /// <summary>
        /// Investor Rate Lock's 'Investor Rate Sheet ID'
        /// </summary>
        [DependsOn(nameof(sInvestorLockRateSheetID), nameof(sIsInvestorRateLocked))]
        public string sInvestorLockRateSheetID
        {
            get { return GetStringVarCharField("sInvestorLockRateSheetID"); }
            set
            {
                if (sIsInvestorRateLocked && sInvestorLockRateSheetID != value)
                {
                    LogBug(ErrorMessages.CannotChangeRateSheetIdDuringInvestorRateLock);
                    CBaseException exc = new CBaseException(ErrorMessages.CannotChangeRateSheetIdDuringInvestorRateLock, ErrorMessages.CannotChangeRateSheetIdDuringInvestorRateLock);
                    exc.IsEmailDeveloper = false;
                    throw exc;
                }

                SetStringVarCharField("sInvestorLockRateSheetID", value);
            }
        }

        /// <summary>
        /// Investor Rate Lock's 'Inv. Rate Sheet Effective Time'
        /// </summary>
        /// <remarks>
        /// It was completely intentional that this be a string rather than datetime
        /// This is a value that lenders use to reference the time with the investor,
        /// so it is free text, so for example the lender can use a specific timezone.
        /// </remarks>
        [DependsOn(nameof(sInvestorLockRateSheetEffectiveTime), nameof(sIsInvestorRateLocked))]
        public string sInvestorLockRateSheetEffectiveTime
        {
            get { return GetStringVarCharField("sInvestorLockRateSheetEffectiveTime"); }
            set {

                if (sIsInvestorRateLocked && sInvestorLockRateSheetEffectiveTime != value)
                {
                    LogBug(ErrorMessages.CannotChangeRateSheetEffectiveTimeDuringInvestorRateLock);
                    CBaseException exc = new CBaseException(ErrorMessages.CannotChangeRateSheetEffectiveTimeDuringInvestorRateLock, ErrorMessages.CannotChangeRateSheetEffectiveTimeDuringInvestorRateLock);
                    exc.IsEmailDeveloper = false;
                    throw exc;
                }

                SetStringVarCharField("sInvestorLockRateSheetEffectiveTime", value); 
            }
        }

        /// <summary>
        /// Investor lock fee for current lock. 
        /// Use <see cref="CPageBase.sInvestorLockTotalLockFees"/> instead if the total cumulative fees are required.
        /// </summary>
        [DependsOn(nameof(sInvestorLockLockFee), nameof(sIsInvestorRateLocked))]
        public decimal sInvestorLockLockFee
        {
            get { return GetMoneyField("sInvestorLockLockFee"); }
            set 
            {
                decimal current;

                try { current = sInvestorLockLockFee; }
                catch { current = 0; }

                if (!m_extendingInvestorRateLock && sIsInvestorRateLocked && value != current)
                {
                    LogBug(ErrorMessages.CannotChangeLockFeeDuringInvestorRateLock);
                    CBaseException exc = new CBaseException(ErrorMessages.CannotChangeLockFeeDuringInvestorRateLock, ErrorMessages.CannotChangeLockFeeDuringInvestorRateLock);
                    exc.IsEmailDeveloper = false;
                    throw exc;
                }

                SetMoneyField("sInvestorLockLockFee", value); 
            }
        }
        public string sInvestorLockLockFee_rep
        {
            get { return ToMoneyString(() => sInvestorLockLockFee); }
            set { sInvestorLockLockFee = ToMoney(value); }
        }

        /// <summary>
        /// Investor Rate Lock 'Investor Commitment Type'
        /// </summary>
        [DependsOn(nameof(sIsInvestorRateLocked), nameof(sInvestorLockCommitmentT))]
        public E_sInvestorLockCommitmentT sInvestorLockCommitmentT
        {
            get { return (E_sInvestorLockCommitmentT)GetTypeIndexField("sInvestorLockCommitmentT"); }
            set 
            {
                E_sInvestorLockCommitmentT current = (E_sInvestorLockCommitmentT)GetTypeIndexField("sInvestorLockCommitmentT");

                if (sIsInvestorRateLocked && value != current)
                {
                    LogBug(ErrorMessages.CannotChangeCommitmentTypeDuringInvestorRateLock);
                    CBaseException exc = new CBaseException(ErrorMessages.CannotChangeCommitmentTypeDuringInvestorRateLock, ErrorMessages.CannotChangeCommitmentTypeDuringInvestorRateLock);
                    exc.IsEmailDeveloper = false;
                    throw exc;
                }

                SetTypeIndexField("sInvestorLockCommitmentT", (int)value); 
            
            }
        }
        public string sInvestorLockCommitmentT_rep
        {
            get
            {
                switch (sInvestorLockCommitmentT)
                {
                    case E_sInvestorLockCommitmentT.BestEfforts:
                        return "Best Efforts";
                    case E_sInvestorLockCommitmentT.Mandatory:
                        return "Mandatory";
                    case E_sInvestorLockCommitmentT.Hedged:
                        return "Hedged";
                    case E_sInvestorLockCommitmentT.Securitized:
                        return "Securitized";
                    default:
                        throw new UnhandledEnumException(sInvestorLockCommitmentT);
                }
            }
        }

        /// <summary>
        /// Total Fixed Monthly Payment
        /// </summary>
        [DependsOn(nameof(sProOHExp), nameof(sTransmOMonPmt))]
        private decimal sTotalFixedPmt
        {
            get
            {
                return SumMoney(sProOHExp, sTransmOMonPmt);
            }
        }

        /// <summary>
        /// The product code of the assigned program.
        /// </summary>
        [DependsOn()]
        public string sLpProdCode
        {
            get { return GetStringVarCharField("sLpProdCode"); }
            set { SetStringVarCharField("sLpProdCode", value); }
        }

        [DependsOn()]
        [DevNote("Loan Product Identifier.")]
        public string sLoanProductIdentifier
        {
            get { return GetStringVarCharField("sLoanProductIdentifier"); }
            set { SetStringVarCharField("sLoanProductIdentifier", value); }
        }

        InvestorRateLockPriceTable x_InvestorLockTable;

        [DependsOn(nameof(sfLoadInvestorLockData))]
        InvestorRateLockPriceTable InvestorRateLockTable
        {
            get
            {
                // Assume that if someone has not directly loaded up the table by
                // a different price row, we will use the existing adjusted row
                if (x_InvestorLockTable == null)
                    LoadInvestorLockData();
                return x_InvestorLockTable;
            }
        }

        /// <summary>
        /// Loads the investor rate lock table directly without a row locked for edit. 
        /// </summary>
        public void LoadInvestorLockData()
        {
            x_InvestorLockTable = new InvestorRateLockPriceTable(sInvestorLockAdjustments
                , E_sInvestorLockPriceRowLockedT.Adjusted // We are loading finals.
                , sInvestorLockNoteIR
                , sInvestorLockBrokComp1Pc
                , sInvestorLockRAdjMarginR
                , sIsOptionArm ? sInvestorLockOptionArmTeaserR : 0
                , m_convertLos);
        }

        [DependsOn(nameof(sInvestorLockAdjustments), nameof(sInvestorLockNoteIR), nameof(sInvestorLockBrokComp1Pc), nameof(sInvestorLockRAdjMarginR), 
            nameof(sInvestorLockOptionArmTeaserR), nameof(sInvestorLockPriceRowLockedT), nameof(sIsOptionArm), nameof(sIsInvestorRateLocked), 
            nameof(InvestorRateLockTable))]
        public bool sfLoadInvestorLockData
        {
            get { return false; }
        }

        /// <summary>
        /// Loads the investor rate lock table using provided values, and locking
        /// the pricing row set by <see cref="CPageBase.sInvestorLockPriceRowLockedT" />.
        /// </summary>
        public void LoadInvestorLockData(
            string rate
            , string fee
            , string margin
            , string teaserRate
            )
        {
            LoadInvestorLockData(
                ToRate(rate)
                , ToRate(fee)
                , ToRate(margin)
                , ToRate(teaserRate)
                );
        }


        /// <summary>
        /// Loads the investor rate lock table using provided pricing values, and locking
        /// the pricing row set by <see cref="CPageBase.sInvestorLockPriceRowLockedT" />.
        /// </summary>
        public void LoadInvestorLockData(
            decimal rate
            , decimal fee
            , decimal margin
            , decimal teaserRate
            )
        {
            x_InvestorLockTable = new InvestorRateLockPriceTable(sInvestorLockAdjustments, sInvestorLockPriceRowLockedT, rate, fee, margin, teaserRate, m_convertLos);

            // Update calculated to final rate
            if (!sIsInvestorRateLocked)
            {
                sInvestorLockNoteIR = InvestorRateLockTable.AdjustedPrice.Rate;
                sInvestorLockBrokComp1Pc = InvestorRateLockTable.AdjustedPrice.Fee;
                sInvestorLockRAdjMarginR = InvestorRateLockTable.AdjustedPrice.Margin;
                if (sIsOptionArm)
                    sInvestorLockOptionArmTeaserR = InvestorRateLockTable.AdjustedPrice.TeaserRate;
            }
        }

        /// <summary>
        /// Investor Rate Lock Note Rate
        /// </summary>
        [DependsOn(nameof(sInvestorLockNoteIR), nameof(sIsInvestorRateLocked))]
        public decimal sInvestorLockNoteIR
        {
            get { return GetRateField("sInvestorLockNoteIR"); }
            set 
            {
                decimal current;
                try { current = sInvestorLockNoteIR; }
                catch { current = 0; }

                if (!m_extendingInvestorRateLock && sIsInvestorRateLocked && value != current)
                {
                    LogBug(ErrorMessages.CannotChangeRateDuringInvestorRateLock);
                    CBaseException exc = new CBaseException(ErrorMessages.CannotChangeRateDuringInvestorRateLock, ErrorMessages.CannotChangeRateDuringInvestorRateLock);
                    exc.IsEmailDeveloper = false;
                    throw exc;
                }
                
                SetRateField("sInvestorLockNoteIR", value); 
            
            }
        }
        public string sInvestorLockNoteIR_rep
        {
            get { return ToRateString(() => sInvestorLockNoteIR); }
            set { sInvestorLockNoteIR = ToRate(value); }
        }

        
        /// <summary>
        /// Investor Rate Lock Fee in bp format
        /// </summary>
        [DependsOn(nameof(sInvestorLockBrokComp1Pc))]
        public decimal sInvestorLockBrokComp1PcPrice
        {
            get { return (100 - sInvestorLockBrokComp1Pc); }
        }

        public string sInvestorLockBrokComp1PcPrice_rep
        {
            // Centralize this calculation!
            get { return ToRateString(() => sInvestorLockBrokComp1PcPrice); }
        }

        /// <summary>
        /// Investor Rate Lock Fee
        /// </summary>
        [DependsOn(nameof(sInvestorLockBrokComp1Pc), nameof(sIsInvestorRateLocked))]
        public decimal sInvestorLockBrokComp1Pc
        {
            get { return GetRateField("sInvestorLockBrokComp1Pc"); }
            set 
            {
                decimal current;
                try { current = sInvestorLockBrokComp1Pc; }
                catch { current = 0; }

                if (!m_extendingInvestorRateLock && sIsInvestorRateLocked && value != current)
                {
                    LogBug(ErrorMessages.CannotChangeFeeDuringInvestorRateLock);
                    CBaseException exc = new CBaseException(ErrorMessages.CannotChangeFeeDuringInvestorRateLock, ErrorMessages.CannotChangeFeeDuringInvestorRateLock);
                    exc.IsEmailDeveloper = false;
                    throw exc;
                }

                SetRateField("sInvestorLockBrokComp1Pc", value); }
        }
        public string sInvestorLockBrokComp1Pc_rep
        {
            get { return ToRateString(() => sInvestorLockBrokComp1Pc); }
            set { sInvestorLockBrokComp1Pc = ToRate(value); }
        }

        /// <summary>
        /// Investor Rate Lock Margin
        /// </summary>
        [DependsOn(nameof(sInvestorLockRAdjMarginR), nameof(sIsInvestorRateLocked))]
        public decimal sInvestorLockRAdjMarginR
        {
            get { return GetRateField("sInvestorLockRAdjMarginR"); }
            set {

                decimal current;
                try { current = sInvestorLockRAdjMarginR; }
                catch { current = 0; }

                if (!m_extendingInvestorRateLock && sIsInvestorRateLocked && value != current)
                {
                    LogBug(ErrorMessages.CannotChangeMarginDuringInvestorRateLock);
                    CBaseException exc = new CBaseException(ErrorMessages.CannotChangeMarginDuringInvestorRateLock, ErrorMessages.CannotChangeMarginDuringInvestorRateLock);
                    exc.IsEmailDeveloper = false;
                    throw exc;
                }

                SetRateField("sInvestorLockRAdjMarginR", value); 
            }
        }
        public string sInvestorLockRAdjMarginR_rep
        {
            get { return ToRateString(() => sInvestorLockRAdjMarginR); }
            set { sInvestorLockRAdjMarginR = ToRate(value); }
        }

        /// <summary>
        /// Investor Rate Lock Teaser rate
        /// </summary>
        [DependsOn(nameof(sInvestorLockOptionArmTeaserR), nameof(sIsInvestorRateLocked))]
        public decimal sInvestorLockOptionArmTeaserR
        {
            get { return GetRateField("sInvestorLockOptionArmTeaserR"); }
            set {

                decimal current;
                try { current = sInvestorLockOptionArmTeaserR; }
                catch { current = 0; }

                if (!m_extendingInvestorRateLock && sIsInvestorRateLocked && value != current)
                {
                    LogBug(ErrorMessages.CannotChangeTeaserRateDuringInvestorRateLock);
                    CBaseException exc = new CBaseException(ErrorMessages.CannotChangeTeaserRateDuringInvestorRateLock, ErrorMessages.CannotChangeTeaserRateDuringInvestorRateLock);
                    exc.IsEmailDeveloper = false;
                    throw exc;
                }

                SetRateField("sInvestorLockOptionArmTeaserR", value); }
        }
        public string sInvestorLockOptionArmTeaserR_rep
        {
            get { return ToRateString(() => sInvestorLockOptionArmTeaserR); }
            set { sInvestorLockOptionArmTeaserR = ToRate(value); }
        }

        /// <summary>
        /// Investor Rate Lock Base (unadjusted) Note rate
        /// </summary>
        [DependsOn(nameof(InvestorRateLockTable))]
        private decimal sInvestorLockBaseNoteIR
        {
            get { return InvestorRateLockTable.BasePrice.Rate; }
        }
        public string sInvestorLockBaseNoteIR_rep
        {
            get { return ToRateString(() => sInvestorLockBaseNoteIR); }
        }

        /// <summary>
        /// Investor Rate Lock Base (unadjusted) Fee in bp format
        /// </summary>
        [DependsOn(nameof(InvestorRateLockTable))]
        public string sInvestorLockBaseBrokComp1PcPrice
        {
            get { return InvestorRateLockTable.BasePrice.Price; }
        }

        /// <summary>
        /// Investor Rate Lock Base (unadjusted) Fee
        /// </summary>
        [DependsOn(nameof(InvestorRateLockTable))]
        private decimal sInvestorLockBaseBrokComp1PcFee
        {
            get { return InvestorRateLockTable.BasePrice.Fee; }
        }
        public string sInvestorLockBaseBrokComp1PcFee_rep
        {
            get { return ToRateString(() => sInvestorLockBaseBrokComp1PcFee); }
        }

        /// <summary>
        /// Investor Rate Lock Base (unadjusted) Margin
        /// </summary>
        [DependsOn(nameof(InvestorRateLockTable))]
        private decimal sInvestorLockBaseRAdjMarginR
        {
            get { return InvestorRateLockTable.BasePrice.Margin; }
        }
        public string sInvestorLockBaseRAdjMarginR_rep
        {
            get { return ToRateString(() => sInvestorLockBaseRAdjMarginR); }
        }

        /// <summary>
        /// Investor Rate Lock Base (unadjusted) Teaser Rate
        /// </summary>
        [DependsOn(nameof(InvestorRateLockTable))]
        private decimal sInvestorLockBaseOptionArmTeaserR
        {
            get { return InvestorRateLockTable.BasePrice.TeaserRate; }
        }
        public string sInvestorLockBaseOptionArmTeaserR_rep
        {
            get { return ToRateString(() => sInvestorLockBaseOptionArmTeaserR); }
        }

        /// <summary>
        /// Investor Rate Lock total adjustments to Note rate
        /// </summary>
        [DependsOn(nameof(InvestorRateLockTable))]
        private decimal sInvestorLockTotAdjNoteIR
        {
            get { return InvestorRateLockTable.TotalAdjustments.Rate; }
        }
        public string sInvestorLockTotAdjNoteIR_rep
        {
            get { return ToRateString(() => sInvestorLockTotAdjNoteIR); }
        }

        /// <summary>
        /// Investor Rate Lock total adjustments to Price
        /// </summary>
        [DependsOn(nameof(InvestorRateLockTable))]
        public string sInvestorLockTotAdjBrokComp1PcPrice
        {
            get { return InvestorRateLockTable.TotalAdjustments.AdjPrice; }
        }

        [DependsOn(nameof(InvestorRateLockTable))]
        public string sInvestorLockTotNonSRPAdjBrokComp1PcPrice
        {
            get { return InvestorRateLockTable.TotalNonSRPAdjustments.AdjPrice; }
        }

        /// <summary>
        /// Investor Rate Lock total adjustments to Price
        /// </summary>
        [DependsOn(nameof(InvestorRateLockTable))]
        private decimal sInvestorLockTotAdjBrokComp1PcFee
        {
            get { return InvestorRateLockTable.TotalAdjustments.Fee; }
        }
        public string sInvestorLockTotAdjBrokComp1PcFee_rep
        {
            get { return ToRateString(() => sInvestorLockTotAdjBrokComp1PcFee); }
        }

        /// <summary>
        /// Investor Rate Lock total adjustments to Margin
        /// </summary>
        [DependsOn(nameof(InvestorRateLockTable))]
        private decimal sInvestorLockTotAdjRAdjMarginR
        {
            get { return InvestorRateLockTable.TotalAdjustments.Margin; }
        }
        public string sInvestorLockTotAdjRAdjMarginR_rep
        {
            get { return ToRateString(() => sInvestorLockTotAdjRAdjMarginR); }
        }

        /// <summary>
        /// Investor Rate Lock total adjustments to Teaser rate
        /// </summary>
        [DependsOn(nameof(InvestorRateLockTable))]
        private decimal sInvestorLockTotAdjOptionArmTeaserR
        {
            get { return InvestorRateLockTable.TotalAdjustments.TeaserRate; }
        }
        public string sInvestorLockTotAdjOptionArmTeaserR_rep
        {
            get { return ToRateString(() => sInvestorLockTotAdjOptionArmTeaserR); }
        }

        /// <summary>
        /// Investor rate lock pricing row locked for editing.
        /// </summary>
        [DependsOn]
        public E_sInvestorLockPriceRowLockedT sInvestorLockPriceRowLockedT
        {
            get { return (E_sInvestorLockPriceRowLockedT)GetTypeIndexField("sInvestorLockPriceRowLockedT"); }
            set { SetTypeIndexField("sInvestorLockPriceRowLockedT", (int)value); }
        }

        [DependsOn]
        public string sInvestorLockAdjustXmlContent
        {
            get { return GetLongTextField("sInvestorLockAdjustXmlContent"); }
            set { SetLongTextField("sInvestorLockAdjustXmlContent", value); }
        }

        /// <summary>
        /// Used by the pricing adjustments property. XmlSerializer is thread safe.
        /// </summary>
        private static System.Xml.Serialization.XmlSerializer x_InvestorAdjustSerializer = new System.Xml.Serialization.XmlSerializer(typeof(List<PricingAdjustment>));

        [DependsOn(nameof(sInvestorLockAdjustXmlContent))]
        public List<PricingAdjustment> sInvestorLockAdjustments
        {
            get
            {
                string data = sInvestorLockAdjustXmlContent;
                if (string.IsNullOrEmpty(data))
                {
                    return new List<PricingAdjustment>();
                }

                //byte[] bytes = Encoding.UTF8.GetBytes(data);
                //using (var ms = new System.IO.MemoryStream(bytes))
                //{
                //    return (List<PricingAdjustment>)x_InvestorAdjustSerializer.Deserialize(ms);
                //}

                using (System.IO.TextReader textReader = new System.IO.StringReader(data))
                {
                    return (List<PricingAdjustment>)x_InvestorAdjustSerializer.Deserialize(textReader);
                }
            }
            set
            {
                using (var writer = new System.IO.StringWriter())
                {
                    x_InvestorAdjustSerializer.Serialize(writer, value);
                    sInvestorLockAdjustXmlContent = writer.ToString();
                }
            }
        }

        [DependsOn(nameof(sfLoadInvestorLockData), nameof(sInvestorLockAdjustments), nameof(sInvestorLockPriceRowLockedT), 
            nameof(sInvestorLockBaseNoteIR), nameof(sInvestorLockBaseBrokComp1PcFee), nameof(sInvestorLockBaseRAdjMarginR), 
            nameof(sInvestorLockBaseOptionArmTeaserR), nameof(sInvestorLockNoteIR), nameof(sInvestorLockBrokComp1Pc), 
            nameof(sInvestorLockRAdjMarginR), nameof(sIsOptionArm), nameof(sInvestorLockOptionArmTeaserR), nameof(InvestorRateLockTable))]
        private bool sfUpdateInvestorLockAdjustmentFee { get { return false; } }

        public void UpdateInvestorLockAdjustmentFee(string adjustmentDescription, decimal fee)
        {
            this.LoadInvestorLockData();

            var adjustments = this.sInvestorLockAdjustments;

            var targetAdjustment = adjustments.Where(a => a.Description.Equals(adjustmentDescription))
                .FirstOrDefault();

            if (targetAdjustment == null)
            {
                // We need to create/add a new adjustment.
                var adjustment = new PricingAdjustment();
                adjustment.Description = adjustmentDescription;
                adjustment.Fee = this.ToRateString(fee);
                adjustments.Add(adjustment);
            }
            else
            {
                targetAdjustment.Fee = this.ToRateString(fee);
            }

            this.sInvestorLockAdjustments = adjustments;

            // Taking into account new adjustment.
            this.sInvestorLockPriceRowLockedT = E_sInvestorLockPriceRowLockedT.Base;
            this.LoadInvestorLockData(this.sInvestorLockBaseNoteIR_rep, this.sInvestorLockBaseBrokComp1PcFee_rep,
                this.sInvestorLockBaseRAdjMarginR_rep, this.sInvestorLockBaseOptionArmTeaserR_rep);

            // Update to final rate
            this.sInvestorLockNoteIR = this.InvestorRateLockTable.AdjustedPrice.Rate;
            this.sInvestorLockBrokComp1Pc = this.InvestorRateLockTable.AdjustedPrice.Fee;
            this.sInvestorLockRAdjMarginR = this.InvestorRateLockTable.AdjustedPrice.Margin;
            if (this.sIsOptionArm)
                this.sInvestorLockOptionArmTeaserR = this.InvestorRateLockTable.AdjustedPrice.TeaserRate;
        }

        /// <summary>
        /// Xml content for storing the investor rate lock history
        /// </summary>
        [DependsOn]
        public string sInvestorLockHistoryXmlContent
        {
            get { return GetLongTextField("sInvestorLockHistoryXmlContent"); }
            set { SetLongTextField("sInvestorLockHistoryXmlContent", value); }
        }

        /// <summary>
        /// Document representing investor rate lock history if it exists, otherwise, a new document with only root node.
        /// </summary>
        [DependsOn(nameof(sInvestorLockHistoryXmlContent))]
        private XmlDocument sInvestorLockHistoryXmlDocument
        {
            get
            {
                XmlDocument doc = null;

                string existingContent = sInvestorLockHistoryXmlContent;
                XmlElement root;
                if (existingContent == "")
                {
                    doc = new XmlDocument();
                    root = doc.CreateElement("RateLockHistory");
                    doc.AppendChild(root);
                }
                else
                {
                    try
                    {
                        doc = Tools.CreateXmlDoc(existingContent);
                        root = doc.DocumentElement;
                    }
                    catch (Exception exc)
                    {
                        Tools.LogError("Failed to load existing sInvestorLockHistoryXmlContent into a xml document. Putting the corrupted content into a separate node for future reference.", exc);
                        doc = new XmlDocument();
                        root = doc.CreateElement("RateLockHistory");
                        doc.AppendChild(root);
                        try
                        {
                            XmlElement el = doc.CreateElement("CorruptedHistory");
                            el.InnerText = existingContent;
                            root.AppendChild(el);
                        }
                        catch (Exception e)
                        {
                            Tools.LogError("Failed to save corrupted history of rate lock. The operation is now aborted.", e);
                            throw;
                        }
                    }

                }
                return doc;
            }
        }

        /// <summary>
        /// Investor rate lock 'Gross Projected Profit/Loss'
        /// </summary>
        [DependsOn(nameof(sInvestorLockBrokComp1Pc), nameof(sBrokComp1Pc))]
        public decimal sInvestorLockProjectedProfit
        {
            get
            {
                return -1 * ( sInvestorLockBrokComp1Pc - sBrokComp1Pc ); 
            }
        }

        public string sInvestorLockProjectedProfit_rep
        {
            get
            {
                return ToRateString(() => sInvestorLockProjectedProfit);
            }
        }

        [DependsOn(nameof(sInvestorLockProjectedProfit), nameof(sFinalLAmt))]
        public decimal sInvestorLockProjectedProfitAmt
        {
            get { return sInvestorLockProjectedProfit * sFinalLAmt / 100; }
        }

        public string sInvestorLockProjectedProfitAmt_rep
        {
            get { return ToMoneyString(() => sInvestorLockProjectedProfitAmt); }
        }

        /// <summary>
        /// Investor Rate Lock cumulative lock fees.
        /// </summary>
        [DependsOn(nameof(sInvestorLockTotalLockFeesLckd), nameof(sInvestorLockTotalLockFees), nameof(sInvestorLockHistoryXmlContent))]
        public decimal sInvestorLockTotalLockFees
        {
            get
            {
                if (sInvestorLockTotalLockFeesLckd)
                    return GetMoneyField("sInvestorLockTotalLockFees");
                else
                {
                    // The total of all past lock event fees.
                    IList<InvestorRateLockHistoryItem> investorLockEvents = InvestorRateLockHistoryItem.GetSortedRateHistory(sInvestorLockHistoryXmlContent);

                    decimal totalFees = 0;

                    foreach (InvestorRateLockHistoryItem lockEvent in investorLockEvents)
                    {
                        decimal eventFee = m_convertLos.ToMoney(lockEvent.sInvestorLockLockFee);
                        totalFees += eventFee;
                    }

                    return totalFees;
                }
                    
            }
            set { SetMoneyField("sInvestorLockTotalLockFees", value); }
        }
        public string sInvestorLockTotalLockFees_rep
        {
            get { return ToMoneyString(() => sInvestorLockTotalLockFees); }
            set { sInvestorLockTotalLockFees = ToMoney(value); }
        }
        [DependsOn(nameof(sInvestorLockLpePriceGroupId), nameof(sProdLpePriceGroupId))]
        public Guid sInvestorLockLpePriceGroupId
        {
            get 
            {
                var field = GetGuidField("sInvestorLockLpePriceGroupId");
                if (Guid.Empty == field) // don't allow returning an empty guid.
                {
                    return sProdLpePriceGroupId; // always returns non-empty guid.
                }
                else
                {
                    return field;
                }
            }
            set { SetGuidField("sInvestorLockLpePriceGroupId", value); }
        }
        /// <summary>
        /// Investor Rate Lock cumulative lock fees locked.
        /// </summary>
        [DependsOn]
        public bool sInvestorLockTotalLockFeesLckd
        {
            get { return GetBoolField("sInvestorLockTotalLockFeesLckd"); }
            set { SetBoolField("sInvestorLockTotalLockFeesLckd", value); }
        }

        /// <summary>
        /// Lock buffer used by pricing engine to determine current pricing.
        /// Basically the offset from the requested lock days to the lock days
        /// we used to price the loan.
        /// </summary>
        [DependsOn]
        public int sInvestorLockLockBuffer
        {
            get { return GetCountField("sInvestorLockLockBuffer"); }
            set { SetCountField("sInvestorLockLockBuffer", value); }
        }
        public string sInvestorLockLockBuffer_rep
        {
            get { return ToCountString(() => sInvestorLockLockBuffer); }
            set { sInvestorLockLockBuffer = ToCount(value); }
        }

        /// <summary>
        /// True if Extend Investor Rate lock's 'extend broker rate lock...' checkbox should be checked by default.
        /// </summary>
        /// <remarks>
        /// In best-efforts, extending the investor and broker rate locks typically go together.
        /// This will normally be checked by default, but will not under any of these circumstances:
        ///   If the loan is at or past the Closed/Funded stage, this should not be checked by default. 
        ///   If the broker lock was extended that day, do not check this by default either.
        /// </remarks>
        [DependsOn(nameof(sStatusT), nameof(sRateLockHistoryXmlContent), nameof(sClosedD))]
        public bool sInvestorLockCheckBrokerExtendByDefault
        {
            get
            {
                // Funded or closed date has been set
                if (sFundD_rep != string.Empty
                    || sClosedD_rep != string.Empty)
                    return false;

                IList<RateLockHistoryItem> brokerLockEvents = RateLockHistoryItem.GetSortedRateHistory(sRateLockHistoryXmlContent);
                foreach (RateLockHistoryItem lockEvent in brokerLockEvents)
                {
                    if (lockEvent.sRateLockAuditActionT == E_RateLockAuditActionT.ExtendRateLock)
                    {
                        DateTime eventDate;

                        if (DateTime.TryParse(lockEvent.EventDate.Replace("PST", "").Replace("PDT", ""), out eventDate))
                        {
                            if (eventDate.Date == DateTime.Now.Date)
                                return false;
                        }
                    }
                }

                return true;
            }
        }


        #region Locking Methods

        [DependsOn(nameof(sIsInvestorRateLocked), nameof(sInvestorLockRLckExpiredD), nameof(sInvestorLockRLckExpiredDLckd), nameof(sInvestorLockRLckdD), nameof(sInvestorLockRLckdDays), nameof(sInvestorLockDeliveryExpiredD), nameof(sInvestorLockDeliveryExpiredDLckd), nameof(sInvestorLockRateLockStatusT), nameof(sfAddInvestorLockEvent), nameof(sLNm))]
        public int sfLockInvestorRate
        {
            get
            {
                return 0;
            }
        }

        private bool m_isNotifyProLender = false;
        /// <summary>
        /// Call this after you already assign all the fields
        /// </summary>
        public void LockInvestorRate(string userName, string lockReason)
        {
            if (sIsInvestorRateLocked)
                throw new CBaseException(ErrorMessages.CannotChangeNoteRateForLockedRate, "We cannot allow locking investor rate when the investor rate is already locked.");

            if ( sInvestorLockRLckExpiredDLckd == false)
                sInvestorLockRLckExpiredD = CDateTime.Create(sInvestorLockRLckdD.DateTimeForComputation.AddDays(sInvestorLockRLckdDays));
            if ( sInvestorLockDeliveryExpiredDLckd == false)
                sInvestorLockDeliveryExpiredD = CDateTime.Create(sInvestorLockRLckdD.DateTimeForComputation.AddDays(sInvestorLockRLckdDays));

            sInvestorLockRateLockStatusT_ = E_sInvestorLockRateLockStatusT.Locked;

            AddInvestorLockEvent("InvestorLockEvent", userName, lockReason);

            if (!string.IsNullOrEmpty(BrokerDB.ProLenderServerURL))
            {
                m_isNotifyProLender = true;
            }
        }


        [DependsOn(nameof(sInvestorLockHistoryXmlDocument), nameof(sIsInvestorRateLocked), nameof(sInvestorLockRateLockStatusT), nameof(sInvestorLockRLckdD), nameof(sInvestorLockRLckExpiredD), nameof(sInvestorLockDeliveryExpiredD),  nameof(sInvestorLockHistoryXmlContent))]
        public int sfBreakInvestorRateLock
        {
            get { return 0; }
        }

        public void BreakInvestorRateLock(string userName, string lockBreakReason)
        {
            if ( sIsInvestorRateLocked == false )
            {
                CBaseException exc = new CBaseException(ErrorMessages.CannotBreakRateLockWithoutRateLock, "We cannot allow breaking investor rate lock when the rate is not locked.");
                exc.IsEmailDeveloper = false;
                throw exc;
            }

            sInvestorLockRateLockStatusT_ = E_sInvestorLockRateLockStatusT.NotLocked;

            sInvestorLockRLckdD_rep = "";
            sInvestorLockRLckExpiredD_rep = "";
            sInvestorLockDeliveryExpiredD_rep = "";

            XmlDocument doc = sInvestorLockHistoryXmlDocument;
            XmlElement root = doc.DocumentElement;

            XmlElement newEvent = doc.CreateElement("Event");
            newEvent.SetAttribute("EvenType", "BreakInvestorLockEvent");
            newEvent.SetAttribute("EventDate", Tools.GetDateTimeNowString());
            newEvent.SetAttribute("DoneBy", userName);
            newEvent.SetAttribute("sRLckBreakReasonDesc", lockBreakReason);

            root.AppendChild(newEvent);

            sInvestorLockHistoryXmlContent = doc.OuterXml;
        }



        [DependsOn(nameof(sIsInvestorRateLocked), nameof(sInvestorLockRLckExpiredD), nameof(sInvestorLockRLckExpiredDLckd), nameof(sInvestorLockRLckdD), nameof(sInvestorLockRLckdDays), nameof(sInvestorLockDeliveryExpiredD), nameof(sInvestorLockDeliveryExpiredDLckd), nameof(sInvestorLockRateLockStatusT), nameof(sfAddInvestorLockEvent), nameof(sLNm))]
        [DevNote("Resume a suspended investor lock.")]
        public int sfResumeInvestorRateLock
        {
            get
            {
                return 0;
            }
        }

        public void ResumeInvestorRateLock(string userName, string lockReason)
        {
            if (sInvestorLockRateLockStatusT != E_sInvestorLockRateLockStatusT.LockSuspended)
            {
                CBaseException exc = new CBaseException(ErrorMessages.CannotResumeLockWhenRateNotSuspended, "We cannot allow resuming rate lock when the rate is not suspended.");
                exc.IsEmailDeveloper = false;
                throw exc;
            }

            sInvestorLockRateLockStatusT_ = E_sInvestorLockRateLockStatusT.Locked;

            AddInvestorLockEvent("InvestorResumeLockEvent", userName, lockReason);

            if (!string.IsNullOrEmpty(BrokerDB.ProLenderServerURL))
            {
                m_isNotifyProLender = true;
            }
        }


        [DependsOn(nameof(sInvestorLockHistoryXmlDocument), nameof(sIsInvestorRateLocked), nameof(sInvestorLockRateLockStatusT), nameof(sInvestorLockRLckdD), nameof(sInvestorLockRLckExpiredD), nameof(sInvestorLockDeliveryExpiredD),  nameof(sInvestorLockHistoryXmlContent))]
        [DevNote("Suspend an investor rate lock.")]
        public int sfSuspendInvestorRateLock
        {
            get { return 0; }
        }

        public void SuspendInvestorRateLock(string userName, string lockSuspendReason)
        {
            if (sInvestorLockRateLockStatusT != E_sInvestorLockRateLockStatusT.Locked )
            {
                CBaseException exc = new CBaseException(ErrorMessages.CannotSuspendLockWithoutRateLock, "We cannot allow suspending rate lock when the rate is not locked.");
                exc.IsEmailDeveloper = false;
                throw exc;
            }

            sInvestorLockRateLockStatusT_ = E_sInvestorLockRateLockStatusT.LockSuspended;

            XmlDocument doc = sInvestorLockHistoryXmlDocument;
            XmlElement root = doc.DocumentElement;

            XmlElement newEvent = doc.CreateElement("Event");
            newEvent.SetAttribute("EvenType", "SuspendInvestorLockEvent");
            newEvent.SetAttribute("EventDate", Tools.GetDateTimeNowString());
            newEvent.SetAttribute("DoneBy", userName);
            newEvent.SetAttribute("sRLckBreakReasonDesc", lockSuspendReason);

            root.AppendChild(newEvent);

            sInvestorLockHistoryXmlContent = doc.OuterXml;
        }


        [DependsOn(nameof(sIsInvestorRateLocked), nameof(sInvestorLockRLckdDays), nameof(sInvestorLockRLckExpiredD), nameof(sInvestorLockRLckdD), nameof(sfLoadInvestorLockData), nameof(sInvestorLockAdjustments),  nameof(sInvestorLockPriceRowLockedT), nameof(sInvestorLockBaseNoteIR), nameof(sInvestorLockBaseBrokComp1PcFee), nameof(sInvestorLockBaseRAdjMarginR),  nameof(sInvestorLockBaseOptionArmTeaserR), nameof(sIsOptionArm), nameof(sfAddInvestorLockEvent), nameof(sInvestorLockRLckExpiredDLckd), nameof(sInvestorLockDeliveryExpiredDLckd),  nameof(sInvestorLockDeliveryExpiredD), nameof(sInvestorLockLockFee), nameof(InvestorRateLockTable), nameof(sInvestorLockNoteIR), nameof(sInvestorLockBrokComp1Pc),  nameof(sInvestorLockRAdjMarginR), nameof(sInvestorLockOptionArmTeaserR))]
        public int sfExtendInvestorRateLock
        {
            get { return 0; }
        }

        private bool m_extendingInvestorRateLock = false;

        /// <summary>
        /// Call this method before setting sRLckdExpiredD to the new date.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="extensionReason"></param>

        public void ExtendInvestorRateLock(string userName, string extensionReason, int addDays, string newExpirationDate, string newDeliveryExpirationDate,  PricingAdjustment adjustment, string lockFee)
        {
            if (!sIsInvestorRateLocked)
                throw new CBaseException(ErrorMessages.CannotExtendLockRateIfRateIsNotLocked, ErrorMessages.CannotExtendLockRateIfRateIsNotLocked);

            m_extendingInvestorRateLock = true;

            try
            {
                sInvestorLockRLckdDays = sInvestorLockRLckdDays + addDays;
                sInvestorLockLockFee_rep = lockFee;

                if (! String.IsNullOrEmpty(newExpirationDate))
                {
                    sInvestorLockRLckExpiredDLckd = true;
                    sInvestorLockRLckExpiredD_rep = newExpirationDate;

                    if (sInvestorLockRLckExpiredD_rep == string.Empty)
                    {
                        // User wanted to lock to a date we could not validate.  Let them re-enter it.
                        throw new CBaseException("Invalid Rate Lock Expiration Date", "Invalid Rate Lock Expiration Date");

                    }
                }
                else
                {
                    sInvestorLockRLckExpiredDLckd = false;
                    sInvestorLockRLckExpiredD = CDateTime.Create(sInvestorLockRLckdD.DateTimeForComputation.AddDays(sInvestorLockRLckdDays));
                }

                if (! String.IsNullOrEmpty(newDeliveryExpirationDate))
                {
                    sInvestorLockDeliveryExpiredDLckd = true;
                    sInvestorLockDeliveryExpiredD_rep = newDeliveryExpirationDate;
                    
                    if (sInvestorLockDeliveryExpiredD_rep == string.Empty)
                    {
                        // User wanted to lock to a date we could not validate.  Let them re-enter it.
                        throw new CBaseException("Invalid Rate Lock Delivery Expiration Date", "Invalid Rate Lock Delivery Expiration Date");
                    }
                }
                else
                {
                    sInvestorLockDeliveryExpiredDLckd = false;
                    sInvestorLockDeliveryExpiredD = CDateTime.Create(sInvestorLockRLckdD.DateTimeForComputation.AddDays(sInvestorLockRLckdDays));
                }

                if (adjustment != null)
                {
                    LoadInvestorLockData();

                    List<PricingAdjustment> adjustments = sInvestorLockAdjustments;
                    adjustments.Add(adjustment);
                    sInvestorLockAdjustments = adjustments;

                    // Taking into account new adjustment.
                    sInvestorLockPriceRowLockedT = E_sInvestorLockPriceRowLockedT.Base;
                    LoadInvestorLockData(sInvestorLockBaseNoteIR_rep, sInvestorLockBaseBrokComp1PcFee_rep,
                    sInvestorLockBaseRAdjMarginR_rep, sInvestorLockBaseOptionArmTeaserR_rep);

                    // Update to final rate
                    sInvestorLockNoteIR = InvestorRateLockTable.AdjustedPrice.Rate;
                    sInvestorLockBrokComp1Pc = InvestorRateLockTable.AdjustedPrice.Fee;
                    sInvestorLockRAdjMarginR = InvestorRateLockTable.AdjustedPrice.Margin;
                    if (sIsOptionArm)
                        sInvestorLockOptionArmTeaserR = InvestorRateLockTable.AdjustedPrice.TeaserRate;
                }
            }
            finally
            {
                m_extendingInvestorRateLock = false;
            }

            AddInvestorLockEvent("ExtendInvestorLockEvent", userName, extensionReason);
        }


        [DependsOn(nameof(sIsInvestorRateLocked), nameof(sInvestorLockPriceRowLockedT), nameof(sInvestorLockBaseNoteIR), nameof(sInvestorLockBaseBrokComp1PcFee), nameof(sInvestorLockBaseRAdjMarginR),  nameof(sInvestorLockBaseOptionArmTeaserR), nameof(sIsOptionArm), nameof(sfLoadInvestorLockData), nameof(sInvestorLockAdjustments), nameof(InvestorRateLockTable), nameof(sInvestorLockNoteIR),  nameof(sInvestorLockBrokComp1Pc), nameof(sInvestorLockRAdjMarginR), nameof(sInvestorLockOptionArmTeaserR))]
        public int sfExtendInvestorRateLockForPricing
        {
            get { return 0; }
        }
        /// <summary>
        /// Provides a mechanism to "preview" the investor pricing that would result from adding an adjustment upon investor lock extention.
        /// </summary>
        /// <remarks>
        /// Please keep this in sync with the adjustment calculation in <see cref="CPageBase.ExtendInvestorRateLock" />.
        /// </remarks>
        /// <param name="adjustment">Adjustment to add to current investor price</param>
        public void ExtendInvestorRateLockForPricing(PricingAdjustment adjustment)
        {
            m_extendingInvestorRateLock = true;

            try
            {
                if (adjustment != null)
                {
                    LoadInvestorLockData();

                    List<PricingAdjustment> adjustments = sInvestorLockAdjustments;
                    adjustments.Add(adjustment);
                    sInvestorLockAdjustments = adjustments;

                    // Taking into account new adjustment.
                    sInvestorLockPriceRowLockedT = E_sInvestorLockPriceRowLockedT.Base;
                    LoadInvestorLockData(sInvestorLockBaseNoteIR_rep, sInvestorLockBaseBrokComp1PcFee_rep,
                    sInvestorLockBaseRAdjMarginR_rep, sInvestorLockBaseOptionArmTeaserR_rep);

                    // Update to final rate
                    sInvestorLockNoteIR = InvestorRateLockTable.AdjustedPrice.Rate;
                    sInvestorLockBrokComp1Pc = InvestorRateLockTable.AdjustedPrice.Fee;
                    sInvestorLockRAdjMarginR = InvestorRateLockTable.AdjustedPrice.Margin;
                    if (sIsOptionArm)
                        sInvestorLockOptionArmTeaserR = InvestorRateLockTable.AdjustedPrice.TeaserRate;
                }
            }
            finally
            {
                m_extendingInvestorRateLock = false;
            }

        }

        [DependsOn(nameof(sInvestorLockHistoryXmlDocument), nameof(sInvestorLockLpInvestorNm), nameof(sInvestorLockLpTemplateNm), nameof(sInvestorLockRLckdDays), nameof(sInvestorLockRLckdD), nameof(sInvestorLockRLckExpiredD),  nameof(sInvestorLockDeliveryExpiredD), nameof(sInvestorLockLoanNum), nameof(sInvestorLockProgramId), nameof(sInvestorLockConfNum), nameof(sInvestorLockRateSheetEffectiveTime), nameof(sInvestorLockLockFee), nameof(sInvestorLockCommitmentT),  nameof(sInvestorLockNoteIR), nameof(sInvestorLockBrokComp1Pc), nameof(sInvestorLockBrokComp1PcPrice), nameof(sInvestorLockRAdjMarginR), nameof(sInvestorLockOptionArmTeaserR),  nameof(sInvestorLockBaseNoteIR), nameof(sInvestorLockBaseBrokComp1PcFee), nameof(sInvestorLockBaseBrokComp1PcPrice), nameof(sInvestorLockBaseRAdjMarginR), nameof(sInvestorLockBaseOptionArmTeaserR),  nameof(sInvestorLockTotAdjNoteIR), nameof(sInvestorLockTotAdjBrokComp1PcFee), nameof(sInvestorLockTotAdjBrokComp1PcPrice), nameof(sInvestorLockTotAdjRAdjMarginR), nameof(sInvestorLockTotAdjOptionArmTeaserR),  nameof(sInvestorLockAdjustments), nameof(sInvestorLockHistoryXmlContent))]
        private bool sfAddInvestorLockEvent
        {
            get { return false; }
        }

        private void AddInvestorLockEvent(string eventName, string userName, string lockReason)
        {
            XmlDocument doc = sInvestorLockHistoryXmlDocument;
            XmlElement root = doc.DocumentElement;

            XmlElement newEvent = doc.CreateElement("Event");
            
            // Bookkeeping Data
            newEvent.SetAttribute("EvenType", eventName);
            newEvent.SetAttribute("EventId", Guid.NewGuid().ToString());
            newEvent.SetAttribute("EventDate", Tools.GetDateTimeNowString());
            newEvent.SetAttribute("DoneBy", userName);
            newEvent.SetAttribute("sRLckBreakReasonDesc", lockReason);

            // Lock Data
            newEvent.SetAttribute("sInvestorLockLpInvestorNm", sInvestorLockLpInvestorNm);
            newEvent.SetAttribute("sInvestorLockLpTemplateNm", sInvestorLockLpTemplateNm);
            newEvent.SetAttribute("sInvestorLockRLckdDays", sInvestorLockRLckdDays_rep);
            newEvent.SetAttribute("sInvestorLockRLckdD", sInvestorLockRLckdD_rep);
            newEvent.SetAttribute("sInvestorLockRLckExpiredD", sInvestorLockRLckExpiredD_rep);
            newEvent.SetAttribute("sInvestorLockDeliveryExpiredD", sInvestorLockDeliveryExpiredD_rep);
            newEvent.SetAttribute("sInvestorLockLoanNum", sInvestorLockLoanNum);
            newEvent.SetAttribute("sInvestorLockProgramId", this.sInvestorLockProgramId);
            newEvent.SetAttribute("sInvestorLockConfNum", sInvestorLockConfNum);
            newEvent.SetAttribute("sInvestorLockRateSheetEffectiveTime", sInvestorLockRateSheetEffectiveTime);
            newEvent.SetAttribute("sInvestorLockLockFee", sInvestorLockLockFee_rep);
            newEvent.SetAttribute("sInvestorLockCommitmentT", sInvestorLockCommitmentT_rep);

            // Pricing Data
            newEvent.SetAttribute("sInvestorLockNoteIR", sInvestorLockNoteIR_rep);
            newEvent.SetAttribute("sInvestorLockBrokComp1Pc", sInvestorLockBrokComp1Pc_rep);
            newEvent.SetAttribute("sInvestorLockBrokComp1PcPrice", sInvestorLockBrokComp1PcPrice_rep);
            newEvent.SetAttribute("sInvestorLockRAdjMarginR", sInvestorLockRAdjMarginR_rep);
            newEvent.SetAttribute("sInvestorLockOptionArmTeaserR", sInvestorLockOptionArmTeaserR_rep);
            newEvent.SetAttribute("sInvestorLockBaseNoteIR", sInvestorLockBaseNoteIR_rep);
            newEvent.SetAttribute("sInvestorLockBaseBrokComp1PcFee", sInvestorLockBaseBrokComp1PcFee_rep);
            newEvent.SetAttribute("sInvestorLockBaseBrokComp1PcPrice", sInvestorLockBaseBrokComp1PcPrice);
            newEvent.SetAttribute("sInvestorLockBaseRAdjMarginR", sInvestorLockBaseRAdjMarginR_rep);
            newEvent.SetAttribute("sInvestorLockBaseOptionArmTeaserR", sInvestorLockBaseOptionArmTeaserR_rep);
            newEvent.SetAttribute("sInvestorLockTotAdjNoteIR", sInvestorLockTotAdjNoteIR_rep);
            newEvent.SetAttribute("sInvestorLockTotAdjBrokComp1PcFee", sInvestorLockTotAdjBrokComp1PcFee_rep);
            newEvent.SetAttribute("sInvestorLockTotAdjBrokComp1PcPrice", sInvestorLockTotAdjBrokComp1PcPrice);
            newEvent.SetAttribute("sInvestorLockTotAdjRAdjMarginR", sInvestorLockTotAdjRAdjMarginR_rep);
            newEvent.SetAttribute("sInvestorLockTotAdjOptionArmTeaserR", sInvestorLockTotAdjOptionArmTeaserR_rep);

            // List of Adjustments
            XmlElement adjustList = doc.CreateElement("Adjustments");

            foreach (PricingAdjustment adj in sInvestorLockAdjustments)
            {
                XmlElement adjustment = doc.CreateElement("Adjustment");
                adjustment.SetAttribute("Description", adj.Description);
                adjustment.SetAttribute("Rate", adj.Rate);
                adjustment.SetAttribute("Price", adj.Fee);
                adjustment.SetAttribute("Fee", adj.Fee);
                adjustment.SetAttribute("Margin", adj.Margin);
                adjustment.SetAttribute("TRate", adj.TeaserRate);
                adjustList.AppendChild(adjustment);
            }
            newEvent.AppendChild(adjustList);

            root.AppendChild(newEvent);

            sInvestorLockHistoryXmlContent = doc.OuterXml;
        }

        #endregion

        [DependsOn(nameof(sInvestorLockBrokComp1PcPrice), nameof(sFinalLAmt))]
        public decimal sProjectedTotalPrice
        {
            get { return ((sInvestorLockBrokComp1PcPrice - 100)/100)*sFinalLAmt; }
        }

        public string sProjectedTotalPrice_rep
        {            
            get { return ToMoneyString(() => sProjectedTotalPrice); }
        }
        #endregion
    }

}