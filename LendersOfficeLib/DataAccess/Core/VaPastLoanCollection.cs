using System;
using System.Collections;
using LendersOffice.Common;

namespace DataAccess
{
    public class CVaPastLCollection : CXmlRecordCollection, IVaPastLoanCollection
    {
		public CVaPastLCollection( CAppBase appData, string xmlSchema, string xmlContent )
			: base( appData.LoanData , appData, xmlSchema, xmlContent )
		{
		}
        		
		
		public ISubcollection GetSubcollection( bool bInclusiveGroup, E_VaPastLGroupT group )
		{
			return base.GetSubcollectionCore( bInclusiveGroup, group );
		}

		protected override bool IsTypeOfSpecialRecord( Enum recordT )
		{
			return false;
		}
		
		
		protected override ICollectionItemBase2 CreateRegularRawRecord()
		{
			return CVaPastL.Create( m_appData, m_ds, this );
		}
		
	
	
		override protected ArrayList SpecialRecords
		{
			get
			{				
				ArrayList list = new ArrayList( 0 );
				return list;
			}
		}
		
		override protected ICollectionItemBase2 Reconstruct( int iRow )
		{
			if( iRow < 0 )
                throw new CBaseException(ErrorMessages.Generic, "Invalid iRow value for xml record.");

			return CVaPastL.Recontruct( m_appData, m_ds, this, iRow );			
		}


		override protected E_ExistingRecordFaith DetermineRawRecordDestiny(ICollectionItemBase2 record )
		{
			return E_ExistingRecordFaith.E_RegularOK;
		}
		
		/// <summary>
		/// Update CAppData with the new values
		/// </summary>
		override public void Flush()
		{
			ExecuteDeathRowRecords();

			var subcoll = this.GetSubcollection( true, E_VaPastLGroupT.ALL );
			foreach( IVaPastLoan rec in subcoll )
			{			
				rec.PrepareToFlush();
			}

			m_appData.aVaPastLXmlContent = m_ds.GetXml();
            base.Flush();
		}

		override protected void DetachSpecialRecords()
		{
		}

		new public IVaPastLoan GetRegularRecordAt( int pos )
		{
			return base.GetRegularRecordAt( pos ) as IVaPastLoan;

		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="recordId"></param>
		/// <returns></returns>
		new public IVaPastLoan GetRegRecordOf( Guid recordId )
		{
			return base.GetRegRecordOf( recordId ) as IVaPastLoan;
		}


		new public IVaPastLoan AddRegularRecord()
		{
			return base.AddRegularRecord() as IVaPastLoan;
		}

		/// <summary>
		/// This method can throw if cannot add
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		protected override ICollectionItemBase2 AddSpecialRecord( Enum type )
		{
            throw new CBaseException(ErrorMessages.Generic, "Cannot add any more special CVaPastL record of the requested type = " + type.ToString());
		}
		
		/// <summary>
		/// Avoid using this method if possible, it's intended for data object use only.
		/// </summary>
		/// <returns></returns>
		public IVaPastLoan AddRecord( E_VaPastLT VaPastLT )
		{		
			return (IVaPastLoan) base.AddRecord( VaPastLT );
		}
		

		public IVaPastLoan EnsureRegularRecordAt( int pos )
		{
            for ( int i = CountRegular - 1; i < pos; ++i )
			{
				AddRegularRecord();
			}

			return GetRegularRecordAt( pos );
		}

		new public IVaPastLoan AddRegularRecordAt( int pos )
		{			
			return (IVaPastLoan) base.AddRegularRecordAt( pos );
		}
	}
}