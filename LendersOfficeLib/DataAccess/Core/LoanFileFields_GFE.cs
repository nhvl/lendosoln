﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOfficeApp.los.RatePrice;
using LendersOffice.ObjLib.CustomAttributes;
using LendersOffice.Admin;

namespace DataAccess
{
    public partial class CPageBase : CBase, IGetSymbolTable, IAmortizationDataProvider
    {
        [DependsOn(nameof(s1006RsrvProps))]
        public bool s1006RsrvProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(s1006RsrvProps); }
            set { s1006RsrvProps = LosConvert.GfeItemProps_UpdateApr(s1006RsrvProps, value); }
        }
        [DependsOn(nameof(s1006RsrvProps))]
        public bool s1006RsrvProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(s1006RsrvProps); }
            set { s1006RsrvProps = LosConvert.GfeItemProps_UpdateFhaAllow(s1006RsrvProps, value); }
        }
        [DependsOn(nameof(s1006RsrvProps))]
        public int s1006RsrvProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(s1006RsrvProps); }
            set { s1006RsrvProps = LosConvert.GfeItemProps_UpdatePayer(s1006RsrvProps, value); }
        }
        [DependsOn(nameof(s1006RsrvProps))]
        public bool s1006RsrvProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(s1006RsrvProps); }
            set { s1006RsrvProps = LosConvert.GfeItemProps_UpdatePoc(s1006RsrvProps, value); }
        }
        [DependsOn(nameof(s1007RsrvProps))]
        public bool s1007RsrvProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(s1007RsrvProps); }
            set { s1007RsrvProps = LosConvert.GfeItemProps_UpdateApr(s1007RsrvProps, value); }
        }
        [DependsOn(nameof(s1007RsrvProps))]
        public bool s1007RsrvProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(s1007RsrvProps); }
            set { s1007RsrvProps = LosConvert.GfeItemProps_UpdateFhaAllow(s1007RsrvProps, value); }
        }
        [DependsOn(nameof(s1007RsrvProps))]
        public int s1007RsrvProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(s1007RsrvProps); }
            set { s1007RsrvProps = LosConvert.GfeItemProps_UpdatePayer(s1007RsrvProps, value); }
        }
        [DependsOn(nameof(s1007RsrvProps))]
        public bool s1007RsrvProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(s1007RsrvProps); }
            set { s1007RsrvProps = LosConvert.GfeItemProps_UpdatePoc(s1007RsrvProps, value); }
        }
        [DependsOn(nameof(sU3RsrvProps))]
        public bool sU3RsrvProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sU3RsrvProps); }
            set { sU3RsrvProps = LosConvert.GfeItemProps_UpdateApr(sU3RsrvProps, value); }
        }
        [DependsOn(nameof(sU3RsrvProps))]
        public bool sU3RsrvProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sU3RsrvProps); }
            set { sU3RsrvProps = LosConvert.GfeItemProps_UpdateFhaAllow(sU3RsrvProps, value); }
        }
        [DependsOn(nameof(sU3RsrvProps))]
        public int sU3RsrvProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sU3RsrvProps); }
            set { sU3RsrvProps = LosConvert.GfeItemProps_UpdatePayer(sU3RsrvProps, value); }
        }
        [DependsOn(nameof(sU3RsrvProps))]
        public bool sU3RsrvProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sU3RsrvProps); }
            set { sU3RsrvProps = LosConvert.GfeItemProps_UpdatePoc(sU3RsrvProps, value); }
        }
        [DependsOn(nameof(sU4RsrvProps))]
        public bool sU4RsrvProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sU4RsrvProps); }
            set { sU4RsrvProps = LosConvert.GfeItemProps_UpdateApr(sU4RsrvProps, value); }
        }
        [DependsOn(nameof(sU4RsrvProps))]
        public bool sU4RsrvProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sU4RsrvProps); }
            set { sU4RsrvProps = LosConvert.GfeItemProps_UpdateFhaAllow(sU4RsrvProps, value); }
        }
        [DependsOn(nameof(sU4RsrvProps))]
        public int sU4RsrvProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sU4RsrvProps); }
            set { sU4RsrvProps = LosConvert.GfeItemProps_UpdatePayer(sU4RsrvProps, value); }
        }
        [DependsOn(nameof(sU4RsrvProps))]
        public bool sU4RsrvProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sU4RsrvProps); }
            set { sU4RsrvProps = LosConvert.GfeItemProps_UpdatePoc(sU4RsrvProps, value); }
        }
        [DependsOn(nameof(s800U1FProps))]
        public bool s800U1FProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(s800U1FProps); }
            set { s800U1FProps = LosConvert.GfeItemProps_UpdateApr(s800U1FProps, value); }
        }
        [DependsOn(nameof(s800U1FProps))]
        public bool s800U1FProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(s800U1FProps); }
            set { s800U1FProps = LosConvert.GfeItemProps_UpdateFhaAllow(s800U1FProps, value); }
        }
        [DependsOn(nameof(s800U1FProps))]
        public int s800U1FProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(s800U1FProps); }
            set { s800U1FProps = LosConvert.GfeItemProps_UpdatePayer(s800U1FProps, value); }
        }
        [DependsOn(nameof(s800U1FProps))]
        public bool s800U1FProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(s800U1FProps); }
            set { s800U1FProps = LosConvert.GfeItemProps_UpdatePoc(s800U1FProps, value); }
        }
        [DependsOn(nameof(s800U1FProps))]
        public bool s800U1FProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(s800U1FProps); }
            set { s800U1FProps = LosConvert.GfeItemProps_UpdateToBr(s800U1FProps, value); }
        }
        [DependsOn(nameof(s800U2FProps))]
        public bool s800U2FProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(s800U2FProps); }
            set { s800U2FProps = LosConvert.GfeItemProps_UpdateApr(s800U2FProps, value); }
        }
        [DependsOn(nameof(s800U2FProps))]
        public bool s800U2FProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(s800U2FProps); }
            set { s800U2FProps = LosConvert.GfeItemProps_UpdateFhaAllow(s800U2FProps, value); }
        }
        [DependsOn(nameof(s800U2FProps))]
        public int s800U2FProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(s800U2FProps); }
            set { s800U2FProps = LosConvert.GfeItemProps_UpdatePayer(s800U2FProps, value); }
        }
        [DependsOn(nameof(s800U2FProps))]
        public bool s800U2FProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(s800U2FProps); }
            set { s800U2FProps = LosConvert.GfeItemProps_UpdatePoc(s800U2FProps, value); }
        }
        [DependsOn(nameof(s800U2FProps))]
        public bool s800U2FProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(s800U2FProps); }
            set { s800U2FProps = LosConvert.GfeItemProps_UpdateToBr(s800U2FProps, value); }
        }
        [DependsOn(nameof(s800U3FProps))]
        public bool s800U3FProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(s800U3FProps); }
            set { s800U3FProps = LosConvert.GfeItemProps_UpdateApr(s800U3FProps, value); }
        }
        [DependsOn(nameof(s800U3FProps))]
        public bool s800U3FProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(s800U3FProps); }
            set { s800U3FProps = LosConvert.GfeItemProps_UpdateFhaAllow(s800U3FProps, value); }
        }
        [DependsOn(nameof(s800U3FProps))]
        public int s800U3FProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(s800U3FProps); }
            set { s800U3FProps = LosConvert.GfeItemProps_UpdatePayer(s800U3FProps, value); }
        }
        [DependsOn(nameof(s800U3FProps))]
        public bool s800U3FProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(s800U3FProps); }
            set { s800U3FProps = LosConvert.GfeItemProps_UpdatePoc(s800U3FProps, value); }
        }
        [DependsOn(nameof(s800U3FProps))]
        public bool s800U3FProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(s800U3FProps); }
            set { s800U3FProps = LosConvert.GfeItemProps_UpdateToBr(s800U3FProps, value); }
        }
        [DependsOn(nameof(s800U4FProps))]
        public bool s800U4FProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(s800U4FProps); }
            set { s800U4FProps = LosConvert.GfeItemProps_UpdateApr(s800U4FProps, value); }
        }
        [DependsOn(nameof(s800U4FProps))]
        public bool s800U4FProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(s800U4FProps); }
            set { s800U4FProps = LosConvert.GfeItemProps_UpdateFhaAllow(s800U4FProps, value); }
        }
        [DependsOn(nameof(s800U4FProps))]
        public int s800U4FProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(s800U4FProps); }
            set { s800U4FProps = LosConvert.GfeItemProps_UpdatePayer(s800U4FProps, value); }
        }
        [DependsOn(nameof(s800U4FProps))]
        public bool s800U4FProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(s800U4FProps); }
            set { s800U4FProps = LosConvert.GfeItemProps_UpdatePoc(s800U4FProps, value); }
        }
        [DependsOn(nameof(s800U4FProps))]
        public bool s800U4FProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(s800U4FProps); }
            set { s800U4FProps = LosConvert.GfeItemProps_UpdateToBr(s800U4FProps, value); }
        }
        [DependsOn(nameof(s800U5FProps))]
        public bool s800U5FProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(s800U5FProps); }
            set { s800U5FProps = LosConvert.GfeItemProps_UpdateApr(s800U5FProps, value); }
        }
        [DependsOn(nameof(s800U5FProps))]
        public bool s800U5FProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(s800U5FProps); }
            set { s800U5FProps = LosConvert.GfeItemProps_UpdateFhaAllow(s800U5FProps, value); }
        }
        [DependsOn(nameof(s800U5FProps))]
        public int s800U5FProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(s800U5FProps); }
            set { s800U5FProps = LosConvert.GfeItemProps_UpdatePayer(s800U5FProps, value); }
        }
        [DependsOn(nameof(s800U5FProps))]
        public bool s800U5FProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(s800U5FProps); }
            set { s800U5FProps = LosConvert.GfeItemProps_UpdatePoc(s800U5FProps, value); }
        }
        [DependsOn(nameof(s800U5FProps))]
        public bool s800U5FProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(s800U5FProps); }
            set { s800U5FProps = LosConvert.GfeItemProps_UpdateToBr(s800U5FProps, value); }
        }
        [DependsOn(nameof(s900U1PiaProps))]
        public bool s900U1PiaProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(s900U1PiaProps); }
            set { s900U1PiaProps = LosConvert.GfeItemProps_UpdateApr(s900U1PiaProps, value); }
        }
        [DependsOn(nameof(s900U1PiaProps))]
        public bool s900U1PiaProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(s900U1PiaProps); }
            set { s900U1PiaProps = LosConvert.GfeItemProps_UpdateFhaAllow(s900U1PiaProps, value); }
        }
        [DependsOn(nameof(s900U1PiaProps))]
        public int s900U1PiaProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(s900U1PiaProps); }
            set { s900U1PiaProps = LosConvert.GfeItemProps_UpdatePayer(s900U1PiaProps, value); }
        }
        [DependsOn(nameof(s900U1PiaProps))]
        public bool s900U1PiaProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(s900U1PiaProps); }
            set { s900U1PiaProps = LosConvert.GfeItemProps_UpdatePoc(s900U1PiaProps, value); }
        }
        [DependsOn(nameof(s904PiaProps))]
        public bool s904PiaProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(s904PiaProps); }
            set { s904PiaProps = LosConvert.GfeItemProps_UpdateApr(s904PiaProps, value); }
        }
        [DependsOn(nameof(s904PiaProps))]
        public bool s904PiaProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(s904PiaProps); }
            set { s904PiaProps = LosConvert.GfeItemProps_UpdateFhaAllow(s904PiaProps, value); }
        }
        [DependsOn(nameof(s904PiaProps))]
        public int s904PiaProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(s904PiaProps); }
            set { s904PiaProps = LosConvert.GfeItemProps_UpdatePayer(s904PiaProps, value); }
        }
        [DependsOn(nameof(s904PiaProps))]
        public bool s904PiaProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(s904PiaProps); }
            set { s904PiaProps = LosConvert.GfeItemProps_UpdatePoc(s904PiaProps, value); }
        }
        [DependsOn(nameof(sAggregateAdjRsrvProps))]
        public bool sAggregateAdjRsrvProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sAggregateAdjRsrvProps); }
            set { sAggregateAdjRsrvProps = LosConvert.GfeItemProps_UpdateApr(sAggregateAdjRsrvProps, value); }
        }
        [DependsOn(nameof(sAggregateAdjRsrvProps))]
        public bool sAggregateAdjRsrvProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sAggregateAdjRsrvProps); }
            set { sAggregateAdjRsrvProps = LosConvert.GfeItemProps_UpdateFhaAllow(sAggregateAdjRsrvProps, value); }
        }
        [DependsOn(nameof(sAggregateAdjRsrvProps))]
        public int sAggregateAdjRsrvProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sAggregateAdjRsrvProps); }
            set { sAggregateAdjRsrvProps = LosConvert.GfeItemProps_UpdatePayer(sAggregateAdjRsrvProps, value); }
        }
        [DependsOn(nameof(sAggregateAdjRsrvProps))]
        public bool sAggregateAdjRsrvProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sAggregateAdjRsrvProps); }
            set { sAggregateAdjRsrvProps = LosConvert.GfeItemProps_UpdatePoc(sAggregateAdjRsrvProps, value); }
        }
        [DependsOn(nameof(sApprFProps))]
        public bool sApprFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sApprFProps); }
            set { sApprFProps = LosConvert.GfeItemProps_UpdateApr(sApprFProps, value); }
        }
        [DependsOn(nameof(sApprFProps))]
        public bool sApprFProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sApprFProps); }
            set { sApprFProps = LosConvert.GfeItemProps_UpdateFhaAllow(sApprFProps, value); }
        }
        [DependsOn(nameof(sApprFProps))]
        public int sApprFProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sApprFProps); }
            set { sApprFProps = LosConvert.GfeItemProps_UpdatePayer(sApprFProps, value); }
        }
        [DependsOn(nameof(sApprFProps))]
        public bool sApprFProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sApprFProps); }
            set { sApprFProps = LosConvert.GfeItemProps_UpdatePoc(sApprFProps, value); }
        }
        [DependsOn(nameof(sApprFProps))]
        public bool sApprFProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(sApprFProps); }
            set { sApprFProps = LosConvert.GfeItemProps_UpdateToBr(sApprFProps, value); }
        }
        [DependsOn(nameof(sAttorneyFProps))]
        public bool sAttorneyFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sAttorneyFProps); }
            set { sAttorneyFProps = LosConvert.GfeItemProps_UpdateApr(sAttorneyFProps, value); }
        }
        [DependsOn(nameof(sAttorneyFProps))]
        public bool sAttorneyFProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sAttorneyFProps); }
            set { sAttorneyFProps = LosConvert.GfeItemProps_UpdateFhaAllow(sAttorneyFProps, value); }
        }
        [DependsOn(nameof(sAttorneyFProps))]
        public int sAttorneyFProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sAttorneyFProps); }
            set { sAttorneyFProps = LosConvert.GfeItemProps_UpdatePayer(sAttorneyFProps, value); }
        }
        [DependsOn(nameof(sAttorneyFProps))]
        public bool sAttorneyFProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sAttorneyFProps); }
            set { sAttorneyFProps = LosConvert.GfeItemProps_UpdatePoc(sAttorneyFProps, value); }
        }
        [DependsOn(nameof(sAttorneyFProps))]
        public bool sAttorneyFProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(sAttorneyFProps); }
            set { sAttorneyFProps = LosConvert.GfeItemProps_UpdateToBr(sAttorneyFProps, value); }
        }
        [DependsOn(nameof(sCountyRtcProps))]
        public bool sCountyRtcProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sCountyRtcProps); }
            set { sCountyRtcProps = LosConvert.GfeItemProps_UpdateApr(sCountyRtcProps, value); }
        }
        [DependsOn(nameof(sCountyRtcProps))]
        public bool sCountyRtcProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sCountyRtcProps); }
            set { sCountyRtcProps = LosConvert.GfeItemProps_UpdateFhaAllow(sCountyRtcProps, value); }
        }
        [DependsOn(nameof(sCountyRtcProps))]
        public int sCountyRtcProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sCountyRtcProps); }
            set { sCountyRtcProps = LosConvert.GfeItemProps_UpdatePayer(sCountyRtcProps, value); }
        }
        [DependsOn(nameof(sCountyRtcProps))]
        public bool sCountyRtcProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sCountyRtcProps); }
            set { sCountyRtcProps = LosConvert.GfeItemProps_UpdatePoc(sCountyRtcProps, value); }
        }
        [DependsOn(nameof(sCrFProps))]
        public bool sCrFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sCrFProps); }
            set { sCrFProps = LosConvert.GfeItemProps_UpdateApr(sCrFProps, value); }
        }
        [DependsOn(nameof(sCrFProps))]
        public bool sCrFProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sCrFProps); }
            set { sCrFProps = LosConvert.GfeItemProps_UpdateFhaAllow(sCrFProps, value); }
        }
        [DependsOn(nameof(sCrFProps))]
        public int sCrFProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sCrFProps); }
            set { sCrFProps = LosConvert.GfeItemProps_UpdatePayer(sCrFProps, value); }
        }
        [DependsOn(nameof(sCrFProps))]
        public bool sCrFProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sCrFProps); }
            set { sCrFProps = LosConvert.GfeItemProps_UpdatePoc(sCrFProps, value); }
        }
        [DependsOn(nameof(sCrFProps))]
        public bool sCrFProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(sCrFProps); }
            set { sCrFProps = LosConvert.GfeItemProps_UpdateToBr(sCrFProps, value); }
        }
        [DependsOn(nameof(sDocPrepFProps))]
        public bool sDocPrepFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sDocPrepFProps); }
            set { sDocPrepFProps = LosConvert.GfeItemProps_UpdateApr(sDocPrepFProps, value); }
        }
        [DependsOn(nameof(sDocPrepFProps))]
        public bool sDocPrepFProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sDocPrepFProps); }
            set { sDocPrepFProps = LosConvert.GfeItemProps_UpdateFhaAllow(sDocPrepFProps, value); }
        }
        [DependsOn(nameof(sDocPrepFProps))]
        public int sDocPrepFProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sDocPrepFProps); }
            set { sDocPrepFProps = LosConvert.GfeItemProps_UpdatePayer(sDocPrepFProps, value); }
        }
        [DependsOn(nameof(sDocPrepFProps))]
        public bool sDocPrepFProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sDocPrepFProps); }
            set { sDocPrepFProps = LosConvert.GfeItemProps_UpdatePoc(sDocPrepFProps, value); }
        }
        [DependsOn(nameof(sDocPrepFProps))]
        public bool sDocPrepFProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(sDocPrepFProps); }
            set { sDocPrepFProps = LosConvert.GfeItemProps_UpdateToBr(sDocPrepFProps, value); }
        }
        [DependsOn(nameof(sEscrowFProps))]
        public bool sEscrowFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sEscrowFProps); }
            set { sEscrowFProps = LosConvert.GfeItemProps_UpdateApr(sEscrowFProps, value); }
        }
        [DependsOn(nameof(sEscrowFProps))]
        public bool sEscrowFProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sEscrowFProps); }
            set { sEscrowFProps = LosConvert.GfeItemProps_UpdateFhaAllow(sEscrowFProps, value); }
        }
        [DependsOn(nameof(sEscrowFProps))]
        public int sEscrowFProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sEscrowFProps); }
            set { sEscrowFProps = LosConvert.GfeItemProps_UpdatePayer(sEscrowFProps, value); }
        }
        [DependsOn(nameof(sEscrowFProps))]
        public bool sEscrowFProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sEscrowFProps); }
            set { sEscrowFProps = LosConvert.GfeItemProps_UpdatePoc(sEscrowFProps, value); }
        }
        [DependsOn(nameof(sEscrowFProps))]
        public bool sEscrowFProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(sEscrowFProps); }
            set { sEscrowFProps = LosConvert.GfeItemProps_UpdateToBr(sEscrowFProps, value); }
        }
        [DependsOn(nameof(sFloodCertificationFProps))]
        public bool sFloodCertificationFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sFloodCertificationFProps); }
            set { sFloodCertificationFProps = LosConvert.GfeItemProps_UpdateApr(sFloodCertificationFProps, value); }
        }
        [DependsOn(nameof(sFloodCertificationFProps))]
        public bool sFloodCertificationFProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sFloodCertificationFProps); }
            set { sFloodCertificationFProps = LosConvert.GfeItemProps_UpdateFhaAllow(sFloodCertificationFProps, value); }
        }
        [DependsOn(nameof(sFloodCertificationFProps))]
        public int sFloodCertificationFProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sFloodCertificationFProps); }
            set { sFloodCertificationFProps = LosConvert.GfeItemProps_UpdatePayer(sFloodCertificationFProps, value); }
        }
        [DependsOn(nameof(sFloodCertificationFProps))]
        public bool sFloodCertificationFProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sFloodCertificationFProps); }
            set { sFloodCertificationFProps = LosConvert.GfeItemProps_UpdatePoc(sFloodCertificationFProps, value); }
        }
        [DependsOn(nameof(sFloodCertificationFProps))]
        public bool sFloodCertificationFProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(sFloodCertificationFProps); }
            set { sFloodCertificationFProps = LosConvert.GfeItemProps_UpdateToBr(sFloodCertificationFProps, value); }
        }
        [DependsOn(nameof(sFloodInsRsrvProps))]
        public bool sFloodInsRsrvProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sFloodInsRsrvProps); }
            set { sFloodInsRsrvProps = LosConvert.GfeItemProps_UpdateApr(sFloodInsRsrvProps, value); }
        }
        [DependsOn(nameof(sFloodInsRsrvProps))]
        public bool sFloodInsRsrvProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sFloodInsRsrvProps); }
            set { sFloodInsRsrvProps = LosConvert.GfeItemProps_UpdateFhaAllow(sFloodInsRsrvProps, value); }
        }
        [DependsOn(nameof(sFloodInsRsrvProps))]
        public int sFloodInsRsrvProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sFloodInsRsrvProps); }
            set { sFloodInsRsrvProps = LosConvert.GfeItemProps_UpdatePayer(sFloodInsRsrvProps, value); }
        }
        [DependsOn(nameof(sFloodInsRsrvProps))]
        public bool sFloodInsRsrvProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sFloodInsRsrvProps); }
            set { sFloodInsRsrvProps = LosConvert.GfeItemProps_UpdatePoc(sFloodInsRsrvProps, value); }
        }
        [DependsOn(nameof(sGfeDiscountPointFProps))]
        public bool sGfeDiscountPointFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sGfeDiscountPointFProps); }
            set { sGfeDiscountPointFProps = LosConvert.GfeItemProps_UpdateApr(sGfeDiscountPointFProps, value); }
        }
        [DependsOn(nameof(sGfeDiscountPointFProps))]
        public bool sGfeDiscountPointFProps_BF
        {
            get { return LosConvert.GfeItemProps_BF(sGfeDiscountPointFProps); }
            set { sGfeDiscountPointFProps = LosConvert.GfeItemProps_UpdateBF(sGfeDiscountPointFProps, value); }
        }
        [DependsOn(nameof(sGfeDiscountPointFProps))]
        public bool sGfeDiscountPointFProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sGfeDiscountPointFProps); }
            set { sGfeDiscountPointFProps = LosConvert.GfeItemProps_UpdateFhaAllow(sGfeDiscountPointFProps, value); }
        }
        [DependsOn(nameof(sGfeDiscountPointFProps))]
        public int sGfeDiscountPointFProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sGfeDiscountPointFProps); }
            set { sGfeDiscountPointFProps = LosConvert.GfeItemProps_UpdatePayer(sGfeDiscountPointFProps, value); }
        }
        [DependsOn(nameof(sGfeLenderCreditFProps))]
        public bool sGfeLenderCreditFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sGfeLenderCreditFProps); }
            set { sGfeLenderCreditFProps = LosConvert.GfeItemProps_UpdateApr(sGfeLenderCreditFProps, value); }
        }
        [DependsOn(nameof(sGfeLenderCreditFProps))]
        public bool sGfeLenderCreditFProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sGfeLenderCreditFProps); }
            set { sGfeLenderCreditFProps = LosConvert.GfeItemProps_UpdateFhaAllow(sGfeLenderCreditFProps, value); }
        }
        [DependsOn(nameof(sGfeOriginatorCompFProps))]
        public bool sGfeOriginatorCompFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sGfeOriginatorCompFProps); }
            set { sGfeOriginatorCompFProps = LosConvert.GfeItemProps_UpdateApr(sGfeOriginatorCompFProps, value); }
        }
        [DependsOn(nameof(sGfeOriginatorCompFProps))]
        public bool sGfeOriginatorCompFProps_BF
        {
            get { return LosConvert.GfeItemProps_BF(sGfeOriginatorCompFProps); }
            set { sGfeOriginatorCompFProps = LosConvert.GfeItemProps_UpdateBF(sGfeOriginatorCompFProps, value); }
        }
        [DependsOn(nameof(sGfeOriginatorCompFProps))]
        public bool sGfeOriginatorCompFProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sGfeOriginatorCompFProps); }
            set { sGfeOriginatorCompFProps = LosConvert.GfeItemProps_UpdateFhaAllow(sGfeOriginatorCompFProps, value); }
        }
        [DependsOn(nameof(sGfeOriginatorCompFProps))]
        public int sGfeOriginatorCompFProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sGfeOriginatorCompFProps); }
            set { sGfeOriginatorCompFProps = LosConvert.GfeItemProps_UpdatePayer(sGfeOriginatorCompFProps, value); }
        }

        [DependsOn(nameof(sGfeOriginatorCompFProps))]
        public bool sGfeOriginatorCompFProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(sGfeOriginatorCompFProps); }
            set { sGfeOriginatorCompFProps = LosConvert.GfeItemProps_UpdateToBr(sGfeOriginatorCompFProps, value); }
        }

        [DependsOn(nameof(sHazInsPiaProps))]
        public bool sHazInsPiaProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sHazInsPiaProps); }
            set { sHazInsPiaProps = LosConvert.GfeItemProps_UpdateApr(sHazInsPiaProps, value); }
        }

        [DependsOn(nameof(sHazInsPiaProps))]
        public bool sHazInsPiaProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sHazInsPiaProps); }
            set { sHazInsPiaProps = LosConvert.GfeItemProps_UpdateFhaAllow(sHazInsPiaProps, value); }
        }
        [DependsOn(nameof(sHazInsPiaProps))]
        public int sHazInsPiaProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sHazInsPiaProps); }
            set { sHazInsPiaProps = LosConvert.GfeItemProps_UpdatePayer(sHazInsPiaProps, value); }
        }
        [DependsOn(nameof(sHazInsPiaProps))]
        public bool sHazInsPiaProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sHazInsPiaProps); }
            set { sHazInsPiaProps = LosConvert.GfeItemProps_UpdatePoc(sHazInsPiaProps, value); }
        }
        [DependsOn(nameof(sHazInsRsrvProps))]
        public bool sHazInsRsrvProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sHazInsRsrvProps); }
            set { sHazInsRsrvProps = LosConvert.GfeItemProps_UpdateApr(sHazInsRsrvProps, value); }
        }
        [DependsOn(nameof(sHazInsRsrvProps))]
        public bool sHazInsRsrvProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sHazInsRsrvProps); }
            set { sHazInsRsrvProps = LosConvert.GfeItemProps_UpdateFhaAllow(sHazInsRsrvProps, value); }
        }
        [DependsOn(nameof(sHazInsRsrvProps))]
        public int sHazInsRsrvProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sHazInsRsrvProps); }
            set { sHazInsRsrvProps = LosConvert.GfeItemProps_UpdatePayer(sHazInsRsrvProps, value); }
        }
        [DependsOn(nameof(sHazInsRsrvProps))]
        public bool sHazInsRsrvProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sHazInsRsrvProps); }
            set { sHazInsRsrvProps = LosConvert.GfeItemProps_UpdatePoc(sHazInsRsrvProps, value); }
        }
        [DependsOn(nameof(sInspectFProps))]
        public bool sInspectFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sInspectFProps); }
            set { sInspectFProps = LosConvert.GfeItemProps_UpdateApr(sInspectFProps, value); }
        }
        [DependsOn(nameof(sInspectFProps))]
        public bool sInspectFProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sInspectFProps); }
            set { sInspectFProps = LosConvert.GfeItemProps_UpdateFhaAllow(sInspectFProps, value); }
        }
        [DependsOn(nameof(sInspectFProps))]
        public int sInspectFProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sInspectFProps); }
            set { sInspectFProps = LosConvert.GfeItemProps_UpdatePayer(sInspectFProps, value); }
        }
        [DependsOn(nameof(sInspectFProps))]
        public bool sInspectFProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sInspectFProps); }
            set { sInspectFProps = LosConvert.GfeItemProps_UpdatePoc(sInspectFProps, value); }
        }
        [DependsOn(nameof(sInspectFProps))]
        public bool sInspectFProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(sInspectFProps); }
            set { sInspectFProps = LosConvert.GfeItemProps_UpdateToBr(sInspectFProps, value); }
        }
        [DependsOn(nameof(sIPiaProps))]
        public bool sIPiaProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sIPiaProps); }
            set { sIPiaProps = LosConvert.GfeItemProps_UpdateApr(sIPiaProps, value); }
        }
        [DependsOn(nameof(sIPiaProps))]
        public bool sIPiaProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sIPiaProps); }
            set { sIPiaProps = LosConvert.GfeItemProps_UpdateFhaAllow(sIPiaProps, value); }
        }
        [DependsOn(nameof(sIPiaProps))]
        public int sIPiaProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sIPiaProps); }
            set { sIPiaProps = LosConvert.GfeItemProps_UpdatePayer(sIPiaProps, value); }
        }
        [DependsOn(nameof(sIPiaProps))]
        public bool sIPiaProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sIPiaProps); }
            set { sIPiaProps = LosConvert.GfeItemProps_UpdatePoc(sIPiaProps, value); }
        }
        [DependsOn(nameof(sLOrigFProps))]
        public bool sLOrigFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sLOrigFProps); }
            set { sLOrigFProps = LosConvert.GfeItemProps_UpdateApr(sLOrigFProps, value); }
        }
        [DependsOn(nameof(sLOrigFProps))]
        public bool sLOrigFProps_BF
        {
            get { return LosConvert.GfeItemProps_BF(sLOrigFProps); }
            set { sLOrigFProps = LosConvert.GfeItemProps_UpdateBF(sLOrigFProps, value); }
        }
        [DependsOn(nameof(sLOrigFProps))]
        public bool sLOrigFProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sLOrigFProps); }
            set { sLOrigFProps = LosConvert.GfeItemProps_UpdateFhaAllow(sLOrigFProps, value); }
        }
        [DependsOn(nameof(sLOrigFProps))]
        public int sLOrigFProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sLOrigFProps); }
            set { sLOrigFProps = LosConvert.GfeItemProps_UpdatePayer(sLOrigFProps, value); }
        }
        [DependsOn(nameof(sLOrigFProps))]
        public bool sLOrigFProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(sLOrigFProps); }
            set { sLOrigFProps = LosConvert.GfeItemProps_UpdateToBr(sLOrigFProps, value); }
        }
        [DependsOn(nameof(sMBrokFProps))]
        public bool sMBrokFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sMBrokFProps); }
            set { sMBrokFProps = LosConvert.GfeItemProps_UpdateApr(sMBrokFProps, value); }
        }
        [DependsOn(nameof(sMBrokFProps))]
        public bool sMBrokFProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sMBrokFProps); }
            set { sMBrokFProps = LosConvert.GfeItemProps_UpdateFhaAllow(sMBrokFProps, value); }
        }
        [DependsOn(nameof(sMBrokFProps))]
        public int sMBrokFProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sMBrokFProps); }
            set { sMBrokFProps = LosConvert.GfeItemProps_UpdatePayer(sMBrokFProps, value); }
        }
        [DependsOn(nameof(sMBrokFProps))]
        public bool sMBrokFProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sMBrokFProps); }
            set { sMBrokFProps = LosConvert.GfeItemProps_UpdatePoc(sMBrokFProps, value); }
        }
        [DependsOn(nameof(sMBrokFProps))]
        public bool sMBrokFProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(sMBrokFProps); }
            set { sMBrokFProps = LosConvert.GfeItemProps_UpdateToBr(sMBrokFProps, value); }
        }
        [DependsOn(nameof(sMInsRsrvProps))]
        public bool sMInsRsrvProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sMInsRsrvProps); }
            set { sMInsRsrvProps = LosConvert.GfeItemProps_UpdateApr(sMInsRsrvProps, value); }
        }
        [DependsOn(nameof(sMInsRsrvProps))]
        public bool sMInsRsrvProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sMInsRsrvProps); }
            set { sMInsRsrvProps = LosConvert.GfeItemProps_UpdateFhaAllow(sMInsRsrvProps, value); }
        }
        [DependsOn(nameof(sMInsRsrvProps))]
        public int sMInsRsrvProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sMInsRsrvProps); }
            set { sMInsRsrvProps = LosConvert.GfeItemProps_UpdatePayer(sMInsRsrvProps, value); }
        }
        [DependsOn(nameof(sMInsRsrvProps))]
        public bool sMInsRsrvProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sMInsRsrvProps); }
            set { sMInsRsrvProps = LosConvert.GfeItemProps_UpdatePoc(sMInsRsrvProps, value); }
        }
        [DependsOn(nameof(sMipPiaProps))]
        public bool sMipPiaProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sMipPiaProps); }
            set { sMipPiaProps = LosConvert.GfeItemProps_UpdateApr(sMipPiaProps, value); }
        }
        [DependsOn(nameof(sMipPiaProps))]
        public bool sMipPiaProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sMipPiaProps); }
            set { sMipPiaProps = LosConvert.GfeItemProps_UpdateFhaAllow(sMipPiaProps, value); }
        }
        [DependsOn(nameof(sMipPiaProps))]
        public int sMipPiaProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sMipPiaProps); }
            set { sMipPiaProps = LosConvert.GfeItemProps_UpdatePayer(sMipPiaProps, value); }
        }
        [DependsOn(nameof(sMipPiaProps))]
        public bool sMipPiaProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sMipPiaProps); }
            set { sMipPiaProps = LosConvert.GfeItemProps_UpdatePoc(sMipPiaProps, value); }
        }
        [DependsOn(nameof(sNotaryFProps))]
        public bool sNotaryFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sNotaryFProps); }
            set { sNotaryFProps = LosConvert.GfeItemProps_UpdateApr(sNotaryFProps, value); }
        }
        [DependsOn(nameof(sNotaryFProps))]
        public bool sNotaryFProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sNotaryFProps); }
            set { sNotaryFProps = LosConvert.GfeItemProps_UpdateFhaAllow(sNotaryFProps, value); }
        }
        [DependsOn(nameof(sNotaryFProps))]
        public int sNotaryFProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sNotaryFProps); }
            set { sNotaryFProps = LosConvert.GfeItemProps_UpdatePayer(sNotaryFProps, value); }
        }
        [DependsOn(nameof(sNotaryFProps))]
        public bool sNotaryFProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sNotaryFProps); }
            set { sNotaryFProps = LosConvert.GfeItemProps_UpdatePoc(sNotaryFProps, value); }
        }
        [DependsOn(nameof(sNotaryFProps))]
        public bool sNotaryFProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(sNotaryFProps); }
            set { sNotaryFProps = LosConvert.GfeItemProps_UpdateToBr(sNotaryFProps, value); }
        }
        [DependsOn(nameof(sOwnerTitleInsProps))]
        public bool sOwnerTitleInsProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sOwnerTitleInsProps); }
            set { sOwnerTitleInsProps = LosConvert.GfeItemProps_UpdateApr(sOwnerTitleInsProps, value); }
        }
        [DependsOn(nameof(sOwnerTitleInsProps))]
        public bool sOwnerTitleInsProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sOwnerTitleInsProps); }
            set { sOwnerTitleInsProps = LosConvert.GfeItemProps_UpdateFhaAllow(sOwnerTitleInsProps, value); }
        }
        [DependsOn(nameof(sOwnerTitleInsProps))]
        public int sOwnerTitleInsProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sOwnerTitleInsProps); }
            set { sOwnerTitleInsProps = LosConvert.GfeItemProps_UpdatePayer(sOwnerTitleInsProps, value); }
        }
        [DependsOn(nameof(sOwnerTitleInsProps))]
        public bool sOwnerTitleInsProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sOwnerTitleInsProps); }
            set { sOwnerTitleInsProps = LosConvert.GfeItemProps_UpdatePoc(sOwnerTitleInsProps, value); }
        }
        [DependsOn(nameof(sOwnerTitleInsProps))]
        public bool sOwnerTitleInsProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(sOwnerTitleInsProps); }
            set { sOwnerTitleInsProps = LosConvert.GfeItemProps_UpdateToBr(sOwnerTitleInsProps, value); }
        }
        [DependsOn(nameof(sPestInspectFProps))]
        public bool sPestInspectFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sPestInspectFProps); }
            set { sPestInspectFProps = LosConvert.GfeItemProps_UpdateApr(sPestInspectFProps, value); }
        }
        [DependsOn(nameof(sPestInspectFProps))]
        public bool sPestInspectFProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sPestInspectFProps); }
            set { sPestInspectFProps = LosConvert.GfeItemProps_UpdateFhaAllow(sPestInspectFProps, value); }
        }
        [DependsOn(nameof(sPestInspectFProps))]
        public int sPestInspectFProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sPestInspectFProps); }
            set { sPestInspectFProps = LosConvert.GfeItemProps_UpdatePayer(sPestInspectFProps, value); }
        }
        [DependsOn(nameof(sPestInspectFProps))]
        public bool sPestInspectFProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sPestInspectFProps); }
            set { sPestInspectFProps = LosConvert.GfeItemProps_UpdatePoc(sPestInspectFProps, value); }
        }
        [DependsOn(nameof(sPestInspectFProps))]
        public bool sPestInspectFProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(sPestInspectFProps); }
            set { sPestInspectFProps = LosConvert.GfeItemProps_UpdateToBr(sPestInspectFProps, value); }
        }
        [DependsOn(nameof(sProcFProps))]
        public bool sProcFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sProcFProps); }
            set { sProcFProps = LosConvert.GfeItemProps_UpdateApr(sProcFProps, value); }
        }
        [DependsOn(nameof(sProcFProps))]
        public bool sProcFProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sProcFProps); }
            set { sProcFProps = LosConvert.GfeItemProps_UpdateFhaAllow(sProcFProps, value); }
        }
        [DependsOn(nameof(sProcFProps))]
        public int sProcFProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sProcFProps); }
            set { sProcFProps = LosConvert.GfeItemProps_UpdatePayer(sProcFProps, value); }
        }
        [DependsOn(nameof(sProcFProps))]
        public bool sProcFProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sProcFProps); }
            set { sProcFProps = LosConvert.GfeItemProps_UpdatePoc(sProcFProps, value); }
        }
        [DependsOn(nameof(sProcFProps))]
        public bool sProcFProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(sProcFProps); }
            set { sProcFProps = LosConvert.GfeItemProps_UpdateToBr(sProcFProps, value); }
        }
        [DependsOn(nameof(sRealETxRsrvProps))]
        public bool sRealETxRsrvProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sRealETxRsrvProps); }
            set { sRealETxRsrvProps = LosConvert.GfeItemProps_UpdateApr(sRealETxRsrvProps, value); }
        }
        [DependsOn(nameof(sRealETxRsrvProps))]
        public bool sRealETxRsrvProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sRealETxRsrvProps); }
            set { sRealETxRsrvProps = LosConvert.GfeItemProps_UpdateFhaAllow(sRealETxRsrvProps, value); }
        }
        [DependsOn(nameof(sRealETxRsrvProps))]
        public int sRealETxRsrvProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sRealETxRsrvProps); }
            set { sRealETxRsrvProps = LosConvert.GfeItemProps_UpdatePayer(sRealETxRsrvProps, value); }
        }
        [DependsOn(nameof(sRealETxRsrvProps))]
        public bool sRealETxRsrvProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sRealETxRsrvProps); }
            set { sRealETxRsrvProps = LosConvert.GfeItemProps_UpdatePoc(sRealETxRsrvProps, value); }
        }
        [DependsOn(nameof(sRecFProps))]
        public bool sRecFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sRecFProps); }
            set { sRecFProps = LosConvert.GfeItemProps_UpdateApr(sRecFProps, value); }
        }
        [DependsOn(nameof(sRecFProps))]
        public bool sRecFProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sRecFProps); }
            set { sRecFProps = LosConvert.GfeItemProps_UpdateFhaAllow(sRecFProps, value); }
        }
        [DependsOn(nameof(sRecFProps))]
        public int sRecFProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sRecFProps); }
            set { sRecFProps = LosConvert.GfeItemProps_UpdatePayer(sRecFProps, value); }
        }
        [DependsOn(nameof(sRecFProps))]
        public bool sRecFProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sRecFProps); }
            set { sRecFProps = LosConvert.GfeItemProps_UpdatePoc(sRecFProps, value); }
        }
        [DependsOn(nameof(sSchoolTxRsrvProps))]
        public bool sSchoolTxRsrvProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sSchoolTxRsrvProps); }
            set { sSchoolTxRsrvProps = LosConvert.GfeItemProps_UpdateApr(sSchoolTxRsrvProps, value); }
        }
        [DependsOn(nameof(sSchoolTxRsrvProps))]
        public bool sSchoolTxRsrvProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sSchoolTxRsrvProps); }
            set { sSchoolTxRsrvProps = LosConvert.GfeItemProps_UpdateFhaAllow(sSchoolTxRsrvProps, value); }
        }
        [DependsOn(nameof(sSchoolTxRsrvProps))]
        public int sSchoolTxRsrvProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sSchoolTxRsrvProps); }
            set { sSchoolTxRsrvProps = LosConvert.GfeItemProps_UpdatePayer(sSchoolTxRsrvProps, value); }
        }
        [DependsOn(nameof(sSchoolTxRsrvProps))]
        public bool sSchoolTxRsrvProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sSchoolTxRsrvProps); }
            set { sSchoolTxRsrvProps = LosConvert.GfeItemProps_UpdatePoc(sSchoolTxRsrvProps, value); }
        }
        [DependsOn(nameof(sStateRtcProps))]
        public bool sStateRtcProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sStateRtcProps); }
            set { sStateRtcProps = LosConvert.GfeItemProps_UpdateApr(sStateRtcProps, value); }
        }
        [DependsOn(nameof(sStateRtcProps))]
        public bool sStateRtcProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sStateRtcProps); }
            set { sStateRtcProps = LosConvert.GfeItemProps_UpdateFhaAllow(sStateRtcProps, value); }
        }
        [DependsOn(nameof(sStateRtcProps))]
        public int sStateRtcProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sStateRtcProps); }
            set { sStateRtcProps = LosConvert.GfeItemProps_UpdatePayer(sStateRtcProps, value); }
        }
        [DependsOn(nameof(sStateRtcProps))]
        public bool sStateRtcProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sStateRtcProps); }
            set { sStateRtcProps = LosConvert.GfeItemProps_UpdatePoc(sStateRtcProps, value); }
        }
        [DependsOn(nameof(sTitleInsFProps))]
        public bool sTitleInsFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sTitleInsFProps); }
            set { sTitleInsFProps = LosConvert.GfeItemProps_UpdateApr(sTitleInsFProps, value); }
        }
        [DependsOn(nameof(sTitleInsFProps))]
        public bool sTitleInsFProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sTitleInsFProps); }
            set { sTitleInsFProps = LosConvert.GfeItemProps_UpdateFhaAllow(sTitleInsFProps, value); }
        }
        [DependsOn(nameof(sTitleInsFProps))]
        public int sTitleInsFProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sTitleInsFProps); }
            set { sTitleInsFProps = LosConvert.GfeItemProps_UpdatePayer(sTitleInsFProps, value); }
        }
        [DependsOn(nameof(sTitleInsFProps))]
        public bool sTitleInsFProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sTitleInsFProps); }
            set { sTitleInsFProps = LosConvert.GfeItemProps_UpdatePoc(sTitleInsFProps, value); }
        }
        [DependsOn(nameof(sTitleInsFProps))]
        public bool sTitleInsFProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(sTitleInsFProps); }
            set { sTitleInsFProps = LosConvert.GfeItemProps_UpdateToBr(sTitleInsFProps, value); }
        }
        [DependsOn(nameof(sTxServFProps))]
        public bool sTxServFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sTxServFProps); }
            set { sTxServFProps = LosConvert.GfeItemProps_UpdateApr(sTxServFProps, value); }
        }
        [DependsOn(nameof(sTxServFProps))]
        public bool sTxServFProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sTxServFProps); }
            set { sTxServFProps = LosConvert.GfeItemProps_UpdateFhaAllow(sTxServFProps, value); }
        }
        [DependsOn(nameof(sTxServFProps))]
        public int sTxServFProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sTxServFProps); }
            set { sTxServFProps = LosConvert.GfeItemProps_UpdatePayer(sTxServFProps, value); }
        }
        [DependsOn(nameof(sTxServFProps))]
        public bool sTxServFProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sTxServFProps); }
            set { sTxServFProps = LosConvert.GfeItemProps_UpdatePoc(sTxServFProps, value); }
        }
        [DependsOn(nameof(sTxServFProps))]
        public bool sTxServFProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(sTxServFProps); }
            set { sTxServFProps = LosConvert.GfeItemProps_UpdateToBr(sTxServFProps, value); }
        }
        [DependsOn(nameof(sU1GovRtcProps))]
        public bool sU1GovRtcProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sU1GovRtcProps); }
            set { sU1GovRtcProps = LosConvert.GfeItemProps_UpdateApr(sU1GovRtcProps, value); }
        }
        [DependsOn(nameof(sU1GovRtcProps))]
        public bool sU1GovRtcProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sU1GovRtcProps); }
            set { sU1GovRtcProps = LosConvert.GfeItemProps_UpdateFhaAllow(sU1GovRtcProps, value); }
        }
        [DependsOn(nameof(sU1GovRtcProps))]
        public int sU1GovRtcProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sU1GovRtcProps); }
            set { sU1GovRtcProps = LosConvert.GfeItemProps_UpdatePayer(sU1GovRtcProps, value); }
        }
        [DependsOn(nameof(sU1GovRtcProps))]
        public bool sU1GovRtcProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sU1GovRtcProps); }
            set { sU1GovRtcProps = LosConvert.GfeItemProps_UpdatePoc(sU1GovRtcProps, value); }
        }
        [DependsOn(nameof(sU1ScProps))]
        public bool sU1ScProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sU1ScProps); }
            set { sU1ScProps = LosConvert.GfeItemProps_UpdateApr(sU1ScProps, value); }
        }
        [DependsOn(nameof(sU1ScProps))]
        public bool sU1ScProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sU1ScProps); }
            set { sU1ScProps = LosConvert.GfeItemProps_UpdateFhaAllow(sU1ScProps, value); }
        }
        [DependsOn(nameof(sU1ScProps))]
        public int sU1ScProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sU1ScProps); }
            set { sU1ScProps = LosConvert.GfeItemProps_UpdatePayer(sU1ScProps, value); }
        }
        [DependsOn(nameof(sU1ScProps))]
        public bool sU1ScProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sU1ScProps); }
            set { sU1ScProps = LosConvert.GfeItemProps_UpdatePoc(sU1ScProps, value); }
        }
        [DependsOn(nameof(sU1ScProps))]
        public bool sU1ScProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(sU1ScProps); }
            set { sU1ScProps = LosConvert.GfeItemProps_UpdateToBr(sU1ScProps, value); }
        }
        [DependsOn(nameof(sU1TcProps))]
        public bool sU1TcProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sU1TcProps); }
            set { sU1TcProps = LosConvert.GfeItemProps_UpdateApr(sU1TcProps, value); }
        }
        [DependsOn(nameof(sU1TcProps))]
        public bool sU1TcProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sU1TcProps); }
            set { sU1TcProps = LosConvert.GfeItemProps_UpdateFhaAllow(sU1TcProps, value); }
        }
        [DependsOn(nameof(sU1TcProps))]
        public int sU1TcProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sU1TcProps); }
            set { sU1TcProps = LosConvert.GfeItemProps_UpdatePayer(sU1TcProps, value); }
        }
        [DependsOn(nameof(sU1TcProps))]
        public bool sU1TcProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sU1TcProps); }
            set { sU1TcProps = LosConvert.GfeItemProps_UpdatePoc(sU1TcProps, value); }
        }
        [DependsOn(nameof(sU1TcProps))]
        public bool sU1TcProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(sU1TcProps); }
            set { sU1TcProps = LosConvert.GfeItemProps_UpdateToBr(sU1TcProps, value); }
        }
        [DependsOn(nameof(sU2GovRtcProps))]
        public bool sU2GovRtcProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sU2GovRtcProps); }
            set { sU2GovRtcProps = LosConvert.GfeItemProps_UpdateApr(sU2GovRtcProps, value); }
        }
        [DependsOn(nameof(sU2GovRtcProps))]
        public bool sU2GovRtcProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sU2GovRtcProps); }
            set { sU2GovRtcProps = LosConvert.GfeItemProps_UpdateFhaAllow(sU2GovRtcProps, value); }
        }
        [DependsOn(nameof(sU2GovRtcProps))]
        public int sU2GovRtcProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sU2GovRtcProps); }
            set { sU2GovRtcProps = LosConvert.GfeItemProps_UpdatePayer(sU2GovRtcProps, value); }
        }
        [DependsOn(nameof(sU2GovRtcProps))]
        public bool sU2GovRtcProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sU2GovRtcProps); }
            set { sU2GovRtcProps = LosConvert.GfeItemProps_UpdatePoc(sU2GovRtcProps, value); }
        }
        [DependsOn(nameof(sU2ScProps))]
        public bool sU2ScProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sU2ScProps); }
            set { sU2ScProps = LosConvert.GfeItemProps_UpdateApr(sU2ScProps, value); }
        }
        [DependsOn(nameof(sU2ScProps))]
        public bool sU2ScProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sU2ScProps); }
            set { sU2ScProps = LosConvert.GfeItemProps_UpdateFhaAllow(sU2ScProps, value); }
        }
        [DependsOn(nameof(sU2ScProps))]
        public int sU2ScProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sU2ScProps); }
            set { sU2ScProps = LosConvert.GfeItemProps_UpdatePayer(sU2ScProps, value); }
        }
        [DependsOn(nameof(sU2ScProps))]
        public bool sU2ScProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sU2ScProps); }
            set { sU2ScProps = LosConvert.GfeItemProps_UpdatePoc(sU2ScProps, value); }
        }
        [DependsOn(nameof(sU2ScProps))]
        public bool sU2ScProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(sU2ScProps); }
            set { sU2ScProps = LosConvert.GfeItemProps_UpdateToBr(sU2ScProps, value); }
        }
        [DependsOn(nameof(sU2TcProps))]
        public bool sU2TcProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sU2TcProps); }
            set { sU2TcProps = LosConvert.GfeItemProps_UpdateApr(sU2TcProps, value); }
        }
        [DependsOn(nameof(sU2TcProps))]
        public bool sU2TcProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sU2TcProps); }
            set { sU2TcProps = LosConvert.GfeItemProps_UpdateFhaAllow(sU2TcProps, value); }
        }
        [DependsOn(nameof(sU2TcProps))]
        public int sU2TcProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sU2TcProps); }
            set { sU2TcProps = LosConvert.GfeItemProps_UpdatePayer(sU2TcProps, value); }
        }
        [DependsOn(nameof(sU2TcProps))]
        public bool sU2TcProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sU2TcProps); }
            set { sU2TcProps = LosConvert.GfeItemProps_UpdatePoc(sU2TcProps, value); }
        }
        [DependsOn(nameof(sU2TcProps))]
        public bool sU2TcProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(sU2TcProps); }
            set { sU2TcProps = LosConvert.GfeItemProps_UpdateToBr(sU2TcProps, value); }
        }
        [DependsOn(nameof(sU3GovRtcProps))]
        public bool sU3GovRtcProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sU3GovRtcProps); }
            set { sU3GovRtcProps = LosConvert.GfeItemProps_UpdateApr(sU3GovRtcProps, value); }
        }
        [DependsOn(nameof(sU3GovRtcProps))]
        public bool sU3GovRtcProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sU3GovRtcProps); }
            set { sU3GovRtcProps = LosConvert.GfeItemProps_UpdateFhaAllow(sU3GovRtcProps, value); }
        }
        [DependsOn(nameof(sU3GovRtcProps))]
        public int sU3GovRtcProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sU3GovRtcProps); }
            set { sU3GovRtcProps = LosConvert.GfeItemProps_UpdatePayer(sU3GovRtcProps, value); }
        }
        [DependsOn(nameof(sU3GovRtcProps))]
        public bool sU3GovRtcProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sU3GovRtcProps); }
            set { sU3GovRtcProps = LosConvert.GfeItemProps_UpdatePoc(sU3GovRtcProps, value); }
        }
        [DependsOn(nameof(sU3ScProps))]
        public bool sU3ScProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sU3ScProps); }
            set { sU3ScProps = LosConvert.GfeItemProps_UpdateApr(sU3ScProps, value); }
        }
        [DependsOn(nameof(sU3ScProps))]
        public bool sU3ScProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sU3ScProps); }
            set { sU3ScProps = LosConvert.GfeItemProps_UpdateFhaAllow(sU3ScProps, value); }
        }
        [DependsOn(nameof(sU3ScProps))]
        public int sU3ScProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sU3ScProps); }
            set { sU3ScProps = LosConvert.GfeItemProps_UpdatePayer(sU3ScProps, value); }
        }
        [DependsOn(nameof(sU3ScProps))]
        public bool sU3ScProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sU3ScProps); }
            set { sU3ScProps = LosConvert.GfeItemProps_UpdatePoc(sU3ScProps, value); }
        }
        [DependsOn(nameof(sU3ScProps))]
        public bool sU3ScProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(sU3ScProps); }
            set { sU3ScProps = LosConvert.GfeItemProps_UpdateToBr(sU3ScProps, value); }
        }
        [DependsOn(nameof(sU3TcProps))]
        public bool sU3TcProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sU3TcProps); }
            set { sU3TcProps = LosConvert.GfeItemProps_UpdateApr(sU3TcProps, value); }
        }
        [DependsOn(nameof(sU3TcProps))]
        public bool sU3TcProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sU3TcProps); }
            set { sU3TcProps = LosConvert.GfeItemProps_UpdateFhaAllow(sU3TcProps, value); }
        }
        [DependsOn(nameof(sU3TcProps))]
        public int sU3TcProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sU3TcProps); }
            set { sU3TcProps = LosConvert.GfeItemProps_UpdatePayer(sU3TcProps, value); }
        }
        [DependsOn(nameof(sU3TcProps))]
        public bool sU3TcProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sU3TcProps); }
            set { sU3TcProps = LosConvert.GfeItemProps_UpdatePoc(sU3TcProps, value); }
        }
        [DependsOn(nameof(sU3TcProps))]
        public bool sU3TcProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(sU3TcProps); }
            set { sU3TcProps = LosConvert.GfeItemProps_UpdateToBr(sU3TcProps, value); }
        }
        [DependsOn(nameof(sU4ScProps))]
        public bool sU4ScProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sU4ScProps); }
            set { sU4ScProps = LosConvert.GfeItemProps_UpdateApr(sU4ScProps, value); }
        }
        [DependsOn(nameof(sU4ScProps))]
        public bool sU4ScProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sU4ScProps); }
            set { sU4ScProps = LosConvert.GfeItemProps_UpdateFhaAllow(sU4ScProps, value); }
        }
        [DependsOn(nameof(sU4ScProps))]
        public int sU4ScProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sU4ScProps); }
            set { sU4ScProps = LosConvert.GfeItemProps_UpdatePayer(sU4ScProps, value); }
        }
        [DependsOn(nameof(sU4ScProps))]
        public bool sU4ScProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sU4ScProps); }
            set { sU4ScProps = LosConvert.GfeItemProps_UpdatePoc(sU4ScProps, value); }
        }
        [DependsOn(nameof(sU4ScProps))]
        public bool sU4ScProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(sU4ScProps); }
            set { sU4ScProps = LosConvert.GfeItemProps_UpdateToBr(sU4ScProps, value); }
        }
        [DependsOn(nameof(sU4TcProps))]
        public bool sU4TcProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sU4TcProps); }
            set { sU4TcProps = LosConvert.GfeItemProps_UpdateApr(sU4TcProps, value); }
        }
        [DependsOn(nameof(sU4TcProps))]
        public bool sU4TcProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sU4TcProps); }
            set { sU4TcProps = LosConvert.GfeItemProps_UpdateFhaAllow(sU4TcProps, value); }
        }
        [DependsOn(nameof(sU4TcProps))]
        public int sU4TcProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sU4TcProps); }
            set { sU4TcProps = LosConvert.GfeItemProps_UpdatePayer(sU4TcProps, value); }
        }
        [DependsOn(nameof(sU4TcProps))]
        public bool sU4TcProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sU4TcProps); }
            set { sU4TcProps = LosConvert.GfeItemProps_UpdatePoc(sU4TcProps, value); }
        }
        [DependsOn(nameof(sU4TcProps))]
        public bool sU4TcProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(sU4TcProps); }
            set { sU4TcProps = LosConvert.GfeItemProps_UpdateToBr(sU4TcProps, value); }
        }
        [DependsOn(nameof(sU5ScProps))]
        public bool sU5ScProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sU5ScProps); }
            set { sU5ScProps = LosConvert.GfeItemProps_UpdateApr(sU5ScProps, value); }
        }
        [DependsOn(nameof(sU5ScProps))]
        public bool sU5ScProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sU5ScProps); }
            set { sU5ScProps = LosConvert.GfeItemProps_UpdateFhaAllow(sU5ScProps, value); }
        }
        [DependsOn(nameof(sU5ScProps))]
        public int sU5ScProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sU5ScProps); }
            set { sU5ScProps = LosConvert.GfeItemProps_UpdatePayer(sU5ScProps, value); }
        }
        [DependsOn(nameof(sU5ScProps))]
        public bool sU5ScProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sU5ScProps); }
            set { sU5ScProps = LosConvert.GfeItemProps_UpdatePoc(sU5ScProps, value); }
        }
        [DependsOn(nameof(sU5ScProps))]
        public bool sU5ScProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(sU5ScProps); }
            set { sU5ScProps = LosConvert.GfeItemProps_UpdateToBr(sU5ScProps, value); }
        }
        [DependsOn(nameof(sUwFProps))]
        public bool sUwFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sUwFProps); }
            set { sUwFProps = LosConvert.GfeItemProps_UpdateApr(sUwFProps, value); }
        }
        [DependsOn(nameof(sUwFProps))]
        public bool sUwFProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sUwFProps); }
            set { sUwFProps = LosConvert.GfeItemProps_UpdateFhaAllow(sUwFProps, value); }
        }
        [DependsOn(nameof(sUwFProps))]
        public int sUwFProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sUwFProps); }
            set { sUwFProps = LosConvert.GfeItemProps_UpdatePayer(sUwFProps, value); }
        }
        [DependsOn(nameof(sUwFProps))]
        public bool sUwFProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sUwFProps); }
            set { sUwFProps = LosConvert.GfeItemProps_UpdatePoc(sUwFProps, value); }
        }
        [DependsOn(nameof(sUwFProps))]
        public bool sUwFProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(sUwFProps); }
            set { sUwFProps = LosConvert.GfeItemProps_UpdateToBr(sUwFProps, value); }
        }
        [DependsOn(nameof(sVaFfProps))]
        public bool sVaFfProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sVaFfProps); }
            set { sVaFfProps = LosConvert.GfeItemProps_UpdateApr(sVaFfProps, value); }
        }
        [DependsOn(nameof(sVaFfProps))]
        public bool sVaFfProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sVaFfProps); }
            set { sVaFfProps = LosConvert.GfeItemProps_UpdateFhaAllow(sVaFfProps, value); }
        }
        [DependsOn(nameof(sVaFfProps))]
        public int sVaFfProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sVaFfProps); }
            set { sVaFfProps = LosConvert.GfeItemProps_UpdatePayer(sVaFfProps, value); }
        }
        [DependsOn(nameof(sVaFfProps))]
        public bool sVaFfProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sVaFfProps); }
            set { sVaFfProps = LosConvert.GfeItemProps_UpdatePoc(sVaFfProps, value); }
        }
        [DependsOn(nameof(sWireFProps))]
        public bool sWireFProps_Apr
        {
            get { return LosConvert.GfeItemProps_Apr(sWireFProps); }
            set { sWireFProps = LosConvert.GfeItemProps_UpdateApr(sWireFProps, value); }
        }
        [DependsOn(nameof(sWireFProps))]
        public bool sWireFProps_FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(sWireFProps); }
            set { sWireFProps = LosConvert.GfeItemProps_UpdateFhaAllow(sWireFProps, value); }
        }
        [DependsOn(nameof(sWireFProps))]
        public int sWireFProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sWireFProps); }
            set { sWireFProps = LosConvert.GfeItemProps_UpdatePayer(sWireFProps, value); }
        }
        [DependsOn(nameof(sWireFProps))]
        public bool sWireFProps_Poc
        {
            get { return LosConvert.GfeItemProps_Poc(sWireFProps); }
            set { sWireFProps = LosConvert.GfeItemProps_UpdatePoc(sWireFProps, value); }
        }
        [DependsOn(nameof(sWireFProps))]
        public bool sWireFProps_ToBroker
        {
            get { return LosConvert.GfeItemProps_ToBr(sWireFProps); }
            set { sWireFProps = LosConvert.GfeItemProps_UpdateToBr(sWireFProps, value); }
        }

        [DependsOn(nameof(sGfeOriginatorCompFProps))]
        public bool sGfeOriginatorCompFProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sGfeOriginatorCompFProps); }
            set { sGfeOriginatorCompFProps = LosConvert.GfeItemProps_UpdateDflp(sGfeOriginatorCompFProps, value); }
        }
        [DependsOn(nameof(sMipPiaProps))]
        public bool sMipPiaProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sMipPiaProps); }
            set { sMipPiaProps = LosConvert.GfeItemProps_UpdateDflp(sMipPiaProps, value); }
        }
        [DependsOn(nameof(sSettlement1008RsrvProps))]
        public bool sSettlement1008RsrvProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlement1008RsrvProps); }
            set { sSettlement1008RsrvProps = LosConvert.GfeItemProps_UpdateDflp(sSettlement1008RsrvProps, value); }
        }
        [DependsOn(nameof(sSettlement1009RsrvProps))]
        public bool sSettlement1009RsrvProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlement1009RsrvProps); }
            set { sSettlement1009RsrvProps = LosConvert.GfeItemProps_UpdateDflp(sSettlement1009RsrvProps, value); }
        }
        [DependsOn(nameof(sSettlementU3RsrvProps))]
        public bool sSettlementU3RsrvProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementU3RsrvProps); }
            set { sSettlementU3RsrvProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementU3RsrvProps, value); }
        }
        [DependsOn(nameof(sSettlementU4RsrvProps))]
        public bool sSettlementU4RsrvProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementU4RsrvProps); }
            set { sSettlementU4RsrvProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementU4RsrvProps, value); }
        }
        [DependsOn(nameof(sSettlement800U1FProps))]
        public bool sSettlement800U1FProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlement800U1FProps); }
            set { sSettlement800U1FProps = LosConvert.GfeItemProps_UpdateDflp(sSettlement800U1FProps, value); }
        }
        [DependsOn(nameof(sSettlement800U2FProps))]
        public bool sSettlement800U2FProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlement800U2FProps); }
            set { sSettlement800U2FProps = LosConvert.GfeItemProps_UpdateDflp(sSettlement800U2FProps, value); }
        }
        [DependsOn(nameof(sSettlement800U3FProps))]
        public bool sSettlement800U3FProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlement800U3FProps); }
            set { sSettlement800U3FProps = LosConvert.GfeItemProps_UpdateDflp(sSettlement800U3FProps, value); }
        }
        [DependsOn(nameof(sSettlement800U4FProps))]
        public bool sSettlement800U4FProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlement800U4FProps); }
            set { sSettlement800U4FProps = LosConvert.GfeItemProps_UpdateDflp(sSettlement800U4FProps, value); }
        }
        [DependsOn(nameof(sSettlement800U5FProps))]
        public bool sSettlement800U5FProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlement800U5FProps); }
            set { sSettlement800U5FProps = LosConvert.GfeItemProps_UpdateDflp(sSettlement800U5FProps, value); }
        }
        [DependsOn(nameof(sSettlement900U1PiaProps))]
        public bool sSettlement900U1PiaProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlement900U1PiaProps); }
            set { sSettlement900U1PiaProps = LosConvert.GfeItemProps_UpdateDflp(sSettlement900U1PiaProps, value); }
        }
        [DependsOn(nameof(sSettlement904PiaProps))]
        public bool sSettlement904PiaProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlement904PiaProps); }
            set { sSettlement904PiaProps = LosConvert.GfeItemProps_UpdateDflp(sSettlement904PiaProps, value); }
        }
        [DependsOn(nameof(sSettlementAggregateAdjRsrvProps))]
        public bool sSettlementAggregateAdjRsrvProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementAggregateAdjRsrvProps); }
            set { sSettlementAggregateAdjRsrvProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementAggregateAdjRsrvProps, value); }
        }
        [DevNote("Payer property for Settlement Aggregate Adjustment")]
        [DependsOn(nameof(sSettlementAggregateAdjRsrvProps))]
        public int sSettlementAggregateAdjRsrvProps_PdByT
        {
            get { return LosConvert.GfeItemProps_Payer(sSettlementAggregateAdjRsrvProps); }
            set { sSettlementAggregateAdjRsrvProps = LosConvert.GfeItemProps_UpdatePayer(sSettlementAggregateAdjRsrvProps, value); }
        }
        [DependsOn(nameof(sSettlementApprFProps))]
        public bool sSettlementApprFProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementApprFProps); }
            set { sSettlementApprFProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementApprFProps, value); }
        }
        [DependsOn(nameof(sSettlementAttorneyFProps))]
        public bool sSettlementAttorneyFProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementAttorneyFProps); }
            set { sSettlementAttorneyFProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementAttorneyFProps, value); }
        }
        [DependsOn(nameof(sSettlementCountyRtcProps))]
        public bool sSettlementCountyRtcProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementCountyRtcProps); }
            set { sSettlementCountyRtcProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementCountyRtcProps, value); }
        }
        [DependsOn(nameof(sSettlementCrFProps))]
        public bool sSettlementCrFProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementCrFProps); }
            set { sSettlementCrFProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementCrFProps, value); }
        }
        [DependsOn(nameof(sSettlementDiscountPointFProps))]
        public bool sSettlementDiscountPointFProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementDiscountPointFProps); }
            set { sSettlementDiscountPointFProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementDiscountPointFProps, value); }
        }
        [DependsOn(nameof(sSettlementDocPrepFProps))]
        public bool sSettlementDocPrepFProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementDocPrepFProps); }
            set { sSettlementDocPrepFProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementDocPrepFProps, value); }
        }
        [DependsOn(nameof(sSettlementEscrowFProps))]
        public bool sSettlementEscrowFProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementEscrowFProps); }
            set { sSettlementEscrowFProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementEscrowFProps, value); }
        }
        [DependsOn(nameof(sSettlementFloodCertificationFProps))]
        public bool sSettlementFloodCertificationFProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementFloodCertificationFProps); }
            set { sSettlementFloodCertificationFProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementFloodCertificationFProps, value); }
        }
        [DependsOn(nameof(sSettlementFloodInsRsrvProps))]
        public bool sSettlementFloodInsRsrvProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementFloodInsRsrvProps); }
            set { sSettlementFloodInsRsrvProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementFloodInsRsrvProps, value); }
        }
        [DependsOn(nameof(sSettlementHazInsPiaProps))]
        public bool sSettlementHazInsPiaProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementHazInsPiaProps); }
            set { sSettlementHazInsPiaProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementHazInsPiaProps, value); }
        }
        [DependsOn(nameof(sSettlementHazInsRsrvProps))]
        public bool sSettlementHazInsRsrvProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementHazInsRsrvProps); }
            set { sSettlementHazInsRsrvProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementHazInsRsrvProps, value); }
        }
        [DependsOn(nameof(sSettlementInspectFProps))]
        public bool sSettlementInspectFProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementInspectFProps); }
            set { sSettlementInspectFProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementInspectFProps, value); }
        }
        [DependsOn(nameof(sSettlementIPiaProps))]
        public bool sSettlementIPiaProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementIPiaProps); }
            set { sSettlementIPiaProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementIPiaProps, value); }
        }
        [DependsOn(nameof(sSettlementLOrigFProps))]
        public bool sSettlementLOrigFProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementLOrigFProps); }
            set { sSettlementLOrigFProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementLOrigFProps, value); }
        }
        [DependsOn(nameof(sSettlementMBrokFProps))]
        public bool sSettlementMBrokFProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementMBrokFProps); }
            set { sSettlementMBrokFProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementMBrokFProps, value); }
        }
        [DependsOn(nameof(sSettlementMInsRsrvProps))]
        public bool sSettlementMInsRsrvProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementMInsRsrvProps); }
            set { sSettlementMInsRsrvProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementMInsRsrvProps, value); }
        }
        [DependsOn(nameof(sSettlementNotaryFProps))]
        public bool sSettlementNotaryFProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementNotaryFProps); }
            set { sSettlementNotaryFProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementNotaryFProps, value); }
        }
        [DependsOn(nameof(sSettlementOwnerTitleInsFProps))]
        public bool sSettlementOwnerTitleInsFProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementOwnerTitleInsFProps); }
            set { sSettlementOwnerTitleInsFProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementOwnerTitleInsFProps, value); }
        }
        [DependsOn(nameof(sSettlementPestInspectFProps))]
        public bool sSettlementPestInspectFProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementPestInspectFProps); }
            set { sSettlementPestInspectFProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementPestInspectFProps, value); }
        }
        [DependsOn(nameof(sSettlementProcFProps))]
        public bool sSettlementProcFProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementProcFProps); }
            set { sSettlementProcFProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementProcFProps, value); }
        }
        [DependsOn(nameof(sSettlementRealETxRsrvProps))]
        public bool sSettlementRealETxRsrvProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementRealETxRsrvProps); }
            set { sSettlementRealETxRsrvProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementRealETxRsrvProps, value); }
        }
        [DependsOn(nameof(sSettlementRecFProps))]
        public bool sSettlementRecFProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementRecFProps); }
            set { sSettlementRecFProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementRecFProps, value); }
        }
        [DependsOn(nameof(sSettlementSchoolTxRsrvProps))]
        public bool sSettlementSchoolTxRsrvProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementSchoolTxRsrvProps); }
            set { sSettlementSchoolTxRsrvProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementSchoolTxRsrvProps, value); }
        }
        [DependsOn(nameof(sSettlementStateRtcProps))]
        public bool sSettlementStateRtcProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementStateRtcProps); }
            set { sSettlementStateRtcProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementStateRtcProps, value); }
        }
        [DependsOn(nameof(sSettlementTitleInsFProps))]
        public bool sSettlementTitleInsFProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementTitleInsFProps); }
            set { sSettlementTitleInsFProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementTitleInsFProps, value); }
        }
        [DependsOn(nameof(sSettlementTxServFProps))]
        public bool sSettlementTxServFProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementTxServFProps); }
            set { sSettlementTxServFProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementTxServFProps, value); }
        }
        [DependsOn(nameof(sSettlementU1GovRtcProps))]
        public bool sSettlementU1GovRtcProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementU1GovRtcProps); }
            set { sSettlementU1GovRtcProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementU1GovRtcProps, value); }
        }
        [DependsOn(nameof(sSettlementU1ScProps))]
        public bool sSettlementU1ScProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementU1ScProps); }
            set { sSettlementU1ScProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementU1ScProps, value); }
        }
        [DependsOn(nameof(sSettlementU1TcProps))]
        public bool sSettlementU1TcProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementU1TcProps); }
            set { sSettlementU1TcProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementU1TcProps, value); }
        }
        [DependsOn(nameof(sSettlementU2GovRtcProps))]
        public bool sSettlementU2GovRtcProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementU2GovRtcProps); }
            set { sSettlementU2GovRtcProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementU2GovRtcProps, value); }
        }
        [DependsOn(nameof(sSettlementU2ScProps))]
        public bool sSettlementU2ScProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementU2ScProps); }
            set { sSettlementU2ScProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementU2ScProps, value); }
        }
        [DependsOn(nameof(sSettlementU2TcProps))]
        public bool sSettlementU2TcProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementU2TcProps); }
            set { sSettlementU2TcProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementU2TcProps, value); }
        }
        [DependsOn(nameof(sSettlementU3GovRtcProps))]
        public bool sSettlementU3GovRtcProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementU3GovRtcProps); }
            set { sSettlementU3GovRtcProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementU3GovRtcProps, value); }
        }
        [DependsOn(nameof(sSettlementU3ScProps))]
        public bool sSettlementU3ScProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementU3ScProps); }
            set { sSettlementU3ScProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementU3ScProps, value); }
        }
        [DependsOn(nameof(sSettlementU3TcProps))]
        public bool sSettlementU3TcProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementU3TcProps); }
            set { sSettlementU3TcProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementU3TcProps, value); }
        }
        [DependsOn(nameof(sSettlementU4ScProps))]
        public bool sSettlementU4ScProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementU4ScProps); }
            set { sSettlementU4ScProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementU4ScProps, value); }
        }
        [DependsOn(nameof(sSettlementU4TcProps))]
        public bool sSettlementU4TcProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementU4TcProps); }
            set { sSettlementU4TcProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementU4TcProps, value); }
        }
        [DependsOn(nameof(sSettlementU5ScProps))]
        public bool sSettlementU5ScProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementU5ScProps); }
            set { sSettlementU5ScProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementU5ScProps, value); }
        }
        [DependsOn(nameof(sSettlementUwFProps))]
        public bool sSettlementUwFProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementUwFProps); }
            set { sSettlementUwFProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementUwFProps, value); }
        }
        [DependsOn(nameof(sSettlementWireFProps))]
        public bool sSettlementWireFProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sSettlementWireFProps); }
            set { sSettlementWireFProps = LosConvert.GfeItemProps_UpdateDflp(sSettlementWireFProps, value); }
        }
        [DependsOn(nameof(sVaFfProps))]
        public bool sVaFfProps_Dflp
        {
            get { return LosConvert.GfeItemProps_Dflp(sVaFfProps); }
            set { sVaFfProps = LosConvert.GfeItemProps_UpdateDflp(sVaFfProps, value); }
        }
        public string sLOrigFProps_PdByT_rep
        {
            get { return ToCountString(() => sLOrigFProps_PdByT); }
            set { sLOrigFProps_PdByT = ToCount(value); }
        }
        public string sGfeDiscountPointFProps_PdByT_rep
        {
            get { return ToCountString(() => sGfeDiscountPointFProps_PdByT); }
            set { sGfeDiscountPointFProps_PdByT = ToCount(value); }
        }
        public string sApprFProps_PdByT_rep
        {
            get { return ToCountString(() => sApprFProps_PdByT); }
            set { sApprFProps_PdByT = ToCount(value); }
        }
        public string sCrFProps_PdByT_rep
        {
            get { return ToCountString(() => sCrFProps_PdByT); }
            set { sCrFProps_PdByT = ToCount(value); }
        }
        public string sTxServFProps_PdByT_rep
        {
            get { return ToCountString(() => sTxServFProps_PdByT); }
            set { sTxServFProps_PdByT = ToCount(value); }
        }
        public string sFloodCertificationFProps_PdByT_rep
        {
            get { return ToCountString(() => sFloodCertificationFProps_PdByT); }
            set { sFloodCertificationFProps_PdByT = ToCount(value); }
        }
        public string sMBrokFProps_PdByT_rep
        {
            get { return ToCountString(() => sMBrokFProps_PdByT); }
            set { sMBrokFProps_PdByT = ToCount(value); }
        }
        public string sInspectFProps_PdByT_rep
        {
            get { return ToCountString(() => sInspectFProps_PdByT); }
            set { sInspectFProps_PdByT = ToCount(value); }
        }
        public string sProcFProps_PdByT_rep
        {
            get { return ToCountString(() => sProcFProps_PdByT); }
            set { sProcFProps_PdByT = ToCount(value); }
        }
        public string sUwFProps_PdByT_rep
        {
            get { return ToCountString(() => sUwFProps_PdByT); }
            set { sUwFProps_PdByT = ToCount(value); }
        }
        public string sWireFProps_PdByT_rep
        {
            get { return ToCountString(() => sWireFProps_PdByT); }
            set { sWireFProps_PdByT = ToCount(value); }
        }
        public string s800U1FProps_PdByT_rep
        {
            get { return ToCountString(() => s800U1FProps_PdByT); }
            set { s800U1FProps_PdByT = ToCount(value); }
        }
        public string s800U2FProps_PdByT_rep
        {
            get { return ToCountString(() => s800U2FProps_PdByT); }
            set { s800U2FProps_PdByT = ToCount(value); }
        }
        public string s800U3FProps_PdByT_rep
        {
            get { return ToCountString(() => s800U3FProps_PdByT); }
            set { s800U3FProps_PdByT = ToCount(value); }
        }
        public string s800U4FProps_PdByT_rep
        {
            get { return ToCountString(() => s800U4FProps_PdByT); }
            set { s800U4FProps_PdByT = ToCount(value); }
        }
        public string s800U5FProps_PdByT_rep
        {
            get { return ToCountString(() => s800U5FProps_PdByT); }
            set { s800U5FProps_PdByT = ToCount(value); }
        }
        public string sIPiaProps_PdByT_rep
        {
            get { return ToCountString(() => sIPiaProps_PdByT); }
            set { sIPiaProps_PdByT = ToCount(value); }
        }
        public string sMipPiaProps_PdByT_rep
        {
            get { return ToCountString(() => sMipPiaProps_PdByT); }
            set { sMipPiaProps_PdByT = ToCount(value); }
        }
        public string sHazInsPiaProps_PdByT_rep
        {
            get { return ToCountString(() => sHazInsPiaProps_PdByT); }
            set { sHazInsPiaProps_PdByT = ToCount(value); }
        }
        public string s904PiaProps_PdByT_rep
        {
            get { return ToCountString(() => s904PiaProps_PdByT); }
            set { s904PiaProps_PdByT = ToCount(value); }
        }
        public string sVaFfProps_PdByT_rep
        {
            get { return ToCountString(() => sVaFfProps_PdByT); }
            set { sVaFfProps_PdByT = ToCount(value); }
        }
        public string s900U1PiaProps_PdByT_rep
        {
            get { return ToCountString(() => s900U1PiaProps_PdByT); }
            set { s900U1PiaProps_PdByT = ToCount(value); }
        }
        public string sHazInsRsrvProps_PdByT_rep
        {
            get { return ToCountString(() => sHazInsRsrvProps_PdByT); }
            set { sHazInsRsrvProps_PdByT = ToCount(value); }
        }
        public string sMInsRsrvProps_PdByT_rep
        {
            get { return ToCountString(() => sMInsRsrvProps_PdByT); }
            set { sMInsRsrvProps_PdByT = ToCount(value); }
        }
        public string sRealETxRsrvProps_PdByT_rep
        {
            get { return ToCountString(() => sRealETxRsrvProps_PdByT); }
            set { sRealETxRsrvProps_PdByT = ToCount(value); }
        }
        public string sSchoolTxRsrvProps_PdByT_rep
        {
            get { return ToCountString(() => sSchoolTxRsrvProps_PdByT); }
            set { sSchoolTxRsrvProps_PdByT = ToCount(value); }
        }
        public string sFloodInsRsrvProps_PdByT_rep
        {
            get { return ToCountString(() => sFloodInsRsrvProps_PdByT); }
            set { sFloodInsRsrvProps_PdByT = ToCount(value); }
        }
        public string sAggregateAdjRsrvProps_PdByT_rep
        {
            get { return ToCountString(() => sAggregateAdjRsrvProps_PdByT); }
            set { sAggregateAdjRsrvProps_PdByT = ToCount(value); }
        }
        public string s1006RsrvProps_PdByT_rep
        {
            get { return ToCountString(() => s1006RsrvProps_PdByT); }
            set { s1006RsrvProps_PdByT = ToCount(value); }
        }
        public string s1007RsrvProps_PdByT_rep
        {
            get { return ToCountString(() => s1007RsrvProps_PdByT); }
            set { s1007RsrvProps_PdByT = ToCount(value); }
        }
        public string sU3RsrvProps_PdByT_rep
        {
            get { return ToCountString(() => sU3RsrvProps_PdByT); }
            set { sU3RsrvProps_PdByT = ToCount(value); }
        }
        public string sU4RsrvProps_PdByT_rep
        {
            get { return ToCountString(() => sU4RsrvProps_PdByT); }
            set { sU4RsrvProps_PdByT = ToCount(value); }
        }
        public string sEscrowFProps_PdByT_rep
        {
            get { return ToCountString(() => sEscrowFProps_PdByT); }
            set { sEscrowFProps_PdByT = ToCount(value); }
        }
        public string sOwnerTitleInsProps_PdByT_rep
        {
            get { return ToCountString(() => sOwnerTitleInsProps_PdByT); }
            set { sOwnerTitleInsProps_PdByT = ToCount(value); }
        }
        public string sTitleInsFProps_PdByT_rep
        {
            get { return ToCountString(() => sTitleInsFProps_PdByT); }
            set { sTitleInsFProps_PdByT = ToCount(value); }
        }
        public string sDocPrepFProps_PdByT_rep
        {
            get { return ToCountString(() => sDocPrepFProps_PdByT); }
            set { sDocPrepFProps_PdByT = ToCount(value); }
        }
        public string sNotaryFProps_PdByT_rep
        {
            get { return ToCountString(() => sNotaryFProps_PdByT); }
            set { sNotaryFProps_PdByT = ToCount(value); }
        }
        public string sAttorneyFProps_PdByT_rep
        {
            get { return ToCountString(() => sAttorneyFProps_PdByT); }
            set { sAttorneyFProps_PdByT = ToCount(value); }
        }
        public string sU1TcProps_PdByT_rep
        {
            get { return ToCountString(() => sU1TcProps_PdByT); }
            set { sU1TcProps_PdByT = ToCount(value); }
        }
        public string sU2TcProps_PdByT_rep
        {
            get { return ToCountString(() => sU2TcProps_PdByT); }
            set { sU2TcProps_PdByT = ToCount(value); }
        }
        public string sU3TcProps_PdByT_rep
        {
            get { return ToCountString(() => sU3TcProps_PdByT); }
            set { sU3TcProps_PdByT = ToCount(value); }
        }
        public string sU4TcProps_PdByT_rep
        {
            get { return ToCountString(() => sU4TcProps_PdByT); }
            set { sU4TcProps_PdByT = ToCount(value); }
        }
        public string sRecFProps_PdByT_rep
        {
            get { return ToCountString(() => sRecFProps_PdByT); }
            set { sRecFProps_PdByT = ToCount(value); }
        }
        public string sCountyRtcProps_PdByT_rep
        {
            get { return ToCountString(() => sCountyRtcProps_PdByT); }
            set { sCountyRtcProps_PdByT = ToCount(value); }
        }
        public string sStateRtcProps_PdByT_rep
        {
            get { return ToCountString(() => sStateRtcProps_PdByT); }
            set { sStateRtcProps_PdByT = ToCount(value); }
        }
        public string sU1GovRtcProps_PdByT_rep
        {
            get { return ToCountString(() => sU1GovRtcProps_PdByT); }
            set { sU1GovRtcProps_PdByT = ToCount(value); }
        }
        public string sU2GovRtcProps_PdByT_rep
        {
            get { return ToCountString(() => sU2GovRtcProps_PdByT); }
            set { sU2GovRtcProps_PdByT = ToCount(value); }
        }
        public string sU3GovRtcProps_PdByT_rep
        {
            get { return ToCountString(() => sU3GovRtcProps_PdByT); }
            set { sU3GovRtcProps_PdByT = ToCount(value); }
        }
        public string sPestInspectFProps_PdByT_rep
        {
            get { return ToCountString(() => sPestInspectFProps_PdByT); }
            set { sPestInspectFProps_PdByT = ToCount(value); }
        }
        public string sU1ScProps_PdByT_rep
        {
            get { return ToCountString(() => sU1ScProps_PdByT); }
            set { sU1ScProps_PdByT = ToCount(value); }
        }
        public string sU2ScProps_PdByT_rep
        {
            get { return ToCountString(() => sU2ScProps_PdByT); }
            set { sU2ScProps_PdByT = ToCount(value); }
        }
        public string sU3ScProps_PdByT_rep
        {
            get { return ToCountString(() => sU3ScProps_PdByT); }
            set { sU3ScProps_PdByT = ToCount(value); }
        }
        public string sU4ScProps_PdByT_rep
        {
            get { return ToCountString(() => sU4ScProps_PdByT); }
            set { sU4ScProps_PdByT = ToCount(value); }
        }
        public string sU5ScProps_PdByT_rep
        {
            get { return ToCountString(() => sU5ScProps_PdByT); }
            set { sU5ScProps_PdByT = ToCount(value); }
        }
        public string sSettlementAggregateAdjRsrvProps_PdByT_rep
        {
            get { return ToCountString(() => sSettlementAggregateAdjRsrvProps_PdByT); }
            set { sSettlementAggregateAdjRsrvProps_PdByT = ToCount(value); } 
        }
        [DependsOn(nameof(sGfeUsePaidToFromOfficialContact), nameof(sfGetAgentOfRole))]
        private bool sfGetLinkedPaidToThirdParty
        {
            set { }
        }

        private bool GetLinkedPaidToThirdParty(int props, E_AgentRoleT agentRoleT)
        {
            if (sGfeUsePaidToFromOfficialContact)
            {
                CAgentFields f = GetAgentOfRole(agentRoleT, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                if (f == CAgentFields.Empty)
                {
                    return false;
                }

                return f.BrokerLevelAgentID != Guid.Empty;

            }
            else
            {
                return LosConvert.GfeItemProps_PaidToThirdParty(props);
            }
        }

        [DependsOn(nameof(sfGetLinkedPaidToThirdParty), nameof(sfIsAgentOriginatingCompanyAffiliate), nameof(sGfeUsePaidToFromOfficialContact), nameof(sfGetAgentOfRole))]
        private bool sfGetLinkedThisPartyIsAffiliate
        {
            set { }
        }

        private bool GetLinkedThisPartyIsAffiliate(int props, E_AgentRoleT agentRoleT)
        {
            if (sGfeUsePaidToFromOfficialContact)
            {
                CAgentFields f = GetAgentOfRole(agentRoleT, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                if (f == CAgentFields.Empty)
                {
                    return false;
                }

                var isThirdParty = GetLinkedPaidToThirdParty(props, agentRoleT);
                return isThirdParty && (f.IsLenderAffiliate || IsAgentOriginatingCompanyAffiliate(f));

            }
            else
            {
                return LosConvert.GfeItemProps_ThisPartyIsAffiliate(props);
            }
        }

        [DependsOn(nameof(sBrokerId), nameof(m_pmlBroker), nameof(PmlBrokerAffiliates))]
        private bool sfIsAgentOriginatingCompanyAffiliate
        {
            set { }
        }
        private bool IsAgentOriginatingCompanyAffiliate(CAgentFields fields)
        {
            if (m_pmlBroker != null)
            {
                return PmlBrokerAffiliates.Any(r => r.ID == fields.BrokerLevelAgentID);
            }

            return false;
        }

        [DependsOn(nameof(sfGetLinkedPaidToThirdParty), nameof(sfGetLinkedThisPartyIsAffiliate), nameof(sfGetLinkedShowQmWarning))]
        private bool sfGetPropsUpdatedWithLinkedProps
        {
            set { }
        }

        private int GetPropsUpdatedWithLinkedProps(int props, E_AgentRoleT agentRoleT)
        {
            props = LosConvert.GfeItemProps_UpdateShowQmWarning(props, GetLinkedShowQmWarning(props, agentRoleT));
            props = LosConvert.GfeItemProps_UpdatePaidToThirdParty(props, GetLinkedPaidToThirdParty(props, agentRoleT));
            props = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(props, GetLinkedThisPartyIsAffiliate(props, agentRoleT));
            return props;
        }

        [DependsOn(nameof(sfGetLinkedPaidToThirdParty), nameof(sGfeUsePaidToFromOfficialContact), nameof(sfGetAgentOfRole))]
        private bool sfGetLinkedShowQmWarning
        {
            set { }
        }

        private bool GetLinkedShowQmWarning(int props, E_AgentRoleT agentRoleT)
        {
            if (sGfeUsePaidToFromOfficialContact)
            {
                CAgentFields f = GetAgentOfRole(agentRoleT, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                if (f == CAgentFields.Empty)
                {
                    return false;
                }

                var isThirdParty = GetLinkedPaidToThirdParty(props, agentRoleT);
                return !isThirdParty && f.EmployeeId == Guid.Empty;

            }
            else
            {
                return LosConvert.GfeItemProps_ShowQmWarning(props);
            }
        }

        [DependsOn(nameof(s1006RsrvProps))]
        public bool s1006RsrvProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(s1006RsrvProps); }
            set { s1006RsrvProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(s1006RsrvProps, value); }
        }
        [DependsOn(nameof(s1006RsrvProps))]
        public bool s1006RsrvProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(s1006RsrvProps); }
            set { s1006RsrvProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(s1006RsrvProps, value); }
        }
        [DependsOn(nameof(s1007RsrvProps))]
        public bool s1007RsrvProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(s1007RsrvProps); }
            set { s1007RsrvProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(s1007RsrvProps, value); }
        }
        [DependsOn(nameof(s1007RsrvProps))]
        public bool s1007RsrvProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(s1007RsrvProps); }
            set { s1007RsrvProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(s1007RsrvProps, value); }
        }
        [DependsOn(nameof(sU3RsrvProps))]
        public bool sU3RsrvProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sU3RsrvProps); }
            set { sU3RsrvProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sU3RsrvProps, value); }
        }
        [DependsOn(nameof(sU3RsrvProps))]
        public bool sU3RsrvProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sU3RsrvProps); }
            set { sU3RsrvProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sU3RsrvProps, value); }
        }
        [DependsOn(nameof(sU4RsrvProps))]
        public bool sU4RsrvProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sU4RsrvProps); }
            set { sU4RsrvProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sU4RsrvProps, value); }
        }
        [DependsOn(nameof(sU4RsrvProps))]
        public bool sU4RsrvProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sU4RsrvProps); }
            set { sU4RsrvProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sU4RsrvProps, value); }
        }
        [DependsOn(nameof(s800U1FProps))]
        public bool s800U1FProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(s800U1FProps); }
            set { s800U1FProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(s800U1FProps, value); }
        }
        [DependsOn(nameof(s800U1FProps))]
        public bool s800U1FProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(s800U1FProps); }
            set { s800U1FProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(s800U1FProps, value); }
        }
        [DependsOn(nameof(s800U2FProps))]
        public bool s800U2FProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(s800U2FProps); }
            set { s800U2FProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(s800U2FProps, value); }
        }
        [DependsOn(nameof(s800U2FProps))]
        public bool s800U2FProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(s800U2FProps); }
            set { s800U2FProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(s800U2FProps, value); }
        }
        [DependsOn(nameof(s800U3FProps))]
        public bool s800U3FProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(s800U3FProps); }
            set { s800U3FProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(s800U3FProps, value); }
        }
        [DependsOn(nameof(s800U3FProps))]
        public bool s800U3FProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(s800U3FProps); }
            set { s800U3FProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(s800U3FProps, value); }
        }
        [DependsOn(nameof(s800U4FProps))]
        public bool s800U4FProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(s800U4FProps); }
            set { s800U4FProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(s800U4FProps, value); }
        }
        [DependsOn(nameof(s800U4FProps))]
        public bool s800U4FProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(s800U4FProps); }
            set { s800U4FProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(s800U4FProps, value); }
        }
        [DependsOn(nameof(s800U5FProps))]
        public bool s800U5FProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(s800U5FProps); }
            set { s800U5FProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(s800U5FProps, value); }
        }
        [DependsOn(nameof(s800U5FProps))]
        public bool s800U5FProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(s800U5FProps); }
            set { s800U5FProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(s800U5FProps, value); }
        }
        [DependsOn(nameof(s900U1PiaProps))]
        public bool s900U1PiaProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(s900U1PiaProps); }
            set { s900U1PiaProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(s900U1PiaProps, value); }
        }
        [DependsOn(nameof(s900U1PiaProps))]
        public bool s900U1PiaProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(s900U1PiaProps); }
            set { s900U1PiaProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(s900U1PiaProps, value); }
        }
        [DependsOn(nameof(s904PiaProps))]
        public bool s904PiaProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(s904PiaProps); }
            set { s904PiaProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(s904PiaProps, value); }
        }
        [DependsOn(nameof(s904PiaProps))]
        public bool s904PiaProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(s904PiaProps); }
            set { s904PiaProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(s904PiaProps, value); }
        }
        [DependsOn(nameof(sAggregateAdjRsrvProps))]
        public bool sAggregateAdjRsrvProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sAggregateAdjRsrvProps); }
            set { sAggregateAdjRsrvProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sAggregateAdjRsrvProps, value); }
        }
        [DependsOn(nameof(sAggregateAdjRsrvProps))]
        public bool sAggregateAdjRsrvProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sAggregateAdjRsrvProps); }
            set { sAggregateAdjRsrvProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sAggregateAdjRsrvProps, value); }
        }
        [DependsOn(nameof(sApprFProps))]
        public bool sApprFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sApprFProps); }
            set { sApprFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sApprFProps, value); }
        }
        [DependsOn(nameof(sApprFProps))]
        public bool sApprFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sApprFProps); }
            set { sApprFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sApprFProps, value); }
        }
        [DependsOn(nameof(sAttorneyFProps))]
        public bool sAttorneyFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sAttorneyFProps); }
            set { sAttorneyFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sAttorneyFProps, value); }
        }
        [DependsOn(nameof(sAttorneyFProps))]
        public bool sAttorneyFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sAttorneyFProps); }
            set { sAttorneyFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sAttorneyFProps, value); }
        }
        [DependsOn(nameof(sCountyRtcProps))]
        public bool sCountyRtcProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sCountyRtcProps); }
            set { sCountyRtcProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sCountyRtcProps, value); }
        }
        [DependsOn(nameof(sCountyRtcProps))]
        public bool sCountyRtcProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sCountyRtcProps); }
            set { sCountyRtcProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sCountyRtcProps, value); }
        }
        [DependsOn(nameof(sCrFProps))]
        public bool sCrFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sCrFProps); }
            set { sCrFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sCrFProps, value); }
        }
        [DependsOn(nameof(sCrFProps))]
        public bool sCrFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sCrFProps); }
            set { sCrFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sCrFProps, value); }
        }
        [DependsOn(nameof(sDocPrepFProps))]
        public bool sDocPrepFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sDocPrepFProps); }
            set { sDocPrepFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sDocPrepFProps, value); }
        }
        [DependsOn(nameof(sDocPrepFProps))]
        public bool sDocPrepFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sDocPrepFProps); }
            set { sDocPrepFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sDocPrepFProps, value); }
        }
        [DependsOn(nameof(sEscrowFProps))]
        public bool sEscrowFProps_PaidToThirdParty
        {
            get
            {
                return LosConvert.GfeItemProps_PaidToThirdParty(sEscrowFProps);
            }
            set { sEscrowFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sEscrowFProps, value); }
        }
        [DependsOn(nameof(sEscrowFProps))]
        public bool sEscrowFProps_ThisPartyIsAffiliate
        {
            get
            {
                return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sEscrowFProps);
            }
            set { sEscrowFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sEscrowFProps, value); }
        }
        [DependsOn(nameof(sFloodCertificationFProps))]
        public bool sFloodCertificationFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sFloodCertificationFProps); }
            set { sFloodCertificationFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sFloodCertificationFProps, value); }
        }
        [DependsOn(nameof(sFloodCertificationFProps))]
        public bool sFloodCertificationFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sFloodCertificationFProps); }
            set { sFloodCertificationFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sFloodCertificationFProps, value); }
        }
        [DependsOn(nameof(sFloodInsRsrvProps))]
        public bool sFloodInsRsrvProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sFloodInsRsrvProps); }
            set { sFloodInsRsrvProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sFloodInsRsrvProps, value); }
        }
        [DependsOn(nameof(sFloodInsRsrvProps))]
        public bool sFloodInsRsrvProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sFloodInsRsrvProps); }
            set { sFloodInsRsrvProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sFloodInsRsrvProps, value); }
        }
        [DependsOn(nameof(sGfeDiscountPointFProps))]
        public bool sGfeDiscountPointFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sGfeDiscountPointFProps); }
            set { sGfeDiscountPointFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sGfeDiscountPointFProps, value); }
        }
        [DependsOn(nameof(sGfeDiscountPointFProps))]
        public bool sGfeDiscountPointFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sGfeDiscountPointFProps); }
            set { sGfeDiscountPointFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sGfeDiscountPointFProps, value); }
        }
        [DependsOn(nameof(sGfeLenderCreditFProps))]
        public bool sGfeLenderCreditFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sGfeLenderCreditFProps); }
            set { sGfeLenderCreditFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sGfeLenderCreditFProps, value); }
        }
        [DependsOn(nameof(sGfeLenderCreditFProps))]
        public bool sGfeLenderCreditFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sGfeLenderCreditFProps); }
            set { sGfeLenderCreditFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sGfeLenderCreditFProps, value); }
        }
        [DependsOn(nameof(sGfeOriginatorCompFProps))]
        public bool sGfeOriginatorCompFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sGfeOriginatorCompFProps); }
            set { sGfeOriginatorCompFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sGfeOriginatorCompFProps, value); }
        }
        [DependsOn(nameof(sGfeOriginatorCompFProps))]
        public bool sGfeOriginatorCompFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sGfeOriginatorCompFProps); }
            set { sGfeOriginatorCompFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sGfeOriginatorCompFProps, value); }
        }
        [DependsOn(nameof(sHazInsPiaProps))]
        public bool sHazInsPiaProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sHazInsPiaProps); }
            set { sHazInsPiaProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sHazInsPiaProps, value); }
        }
        [DependsOn(nameof(sHazInsPiaProps))]
        public bool sHazInsPiaProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sHazInsPiaProps); }
            set { sHazInsPiaProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sHazInsPiaProps, value); }
        }
        [DependsOn(nameof(sHazInsRsrvProps))]
        public bool sHazInsRsrvProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sHazInsRsrvProps); }
            set { sHazInsRsrvProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sHazInsRsrvProps, value); }
        }
        [DependsOn(nameof(sHazInsRsrvProps))]
        public bool sHazInsRsrvProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sHazInsRsrvProps); }
            set { sHazInsRsrvProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sHazInsRsrvProps, value); }
        }
        [DependsOn(nameof(sInspectFProps))]
        public bool sInspectFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sInspectFProps); }
            set { sInspectFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sInspectFProps, value); }
        }
        [DependsOn(nameof(sInspectFProps))]
        public bool sInspectFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sInspectFProps); }
            set { sInspectFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sInspectFProps, value); }
        }
        [DependsOn(nameof(sIPiaProps))]
        public bool sIPiaProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sIPiaProps); }
            set { sIPiaProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sIPiaProps, value); }
        }
        [DependsOn(nameof(sIPiaProps))]
        public bool sIPiaProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sIPiaProps); }
            set { sIPiaProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sIPiaProps, value); }
        }
        [DependsOn(nameof(sLOrigFProps))]
        public bool sLOrigFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sLOrigFProps); }
            set { sLOrigFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sLOrigFProps, value); }
        }
        [DependsOn(nameof(sLOrigFProps))]
        public bool sLOrigFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sLOrigFProps); }
            set { sLOrigFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sLOrigFProps, value); }
        }
        [DependsOn(nameof(sMBrokFProps))]
        public bool sMBrokFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sMBrokFProps); }
            set { sMBrokFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sMBrokFProps, value); }
        }
        [DependsOn(nameof(sMBrokFProps))]
        public bool sMBrokFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sMBrokFProps); }
            set { sMBrokFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sMBrokFProps, value); }
        }
        [DependsOn(nameof(sMInsRsrvProps))]
        public bool sMInsRsrvProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sMInsRsrvProps); }
            set { sMInsRsrvProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sMInsRsrvProps, value); }
        }
        [DependsOn(nameof(sMInsRsrvProps))]
        public bool sMInsRsrvProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sMInsRsrvProps); }
            set { sMInsRsrvProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sMInsRsrvProps, value); }
        }
        [DependsOn(nameof(sMipPiaProps))]
        public bool sMipPiaProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sMipPiaProps); }
            set { sMipPiaProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sMipPiaProps, value); }
        }
        [DependsOn(nameof(sMipPiaProps))]
        public bool sMipPiaProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sMipPiaProps); }
            set { sMipPiaProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sMipPiaProps, value); }
        }
        [DependsOn(nameof(sNotaryFProps))]
        public bool sNotaryFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sNotaryFProps); }
            set { sNotaryFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sNotaryFProps, value); }
        }
        [DependsOn(nameof(sNotaryFProps))]
        public bool sNotaryFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sNotaryFProps); }
            set { sNotaryFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sNotaryFProps, value); }
        }
        [DependsOn(nameof(sOwnerTitleInsProps))]
        public bool sOwnerTitleInsProps_PaidToThirdParty
        {
            get
            {
                return LosConvert.GfeItemProps_PaidToThirdParty(sOwnerTitleInsProps);
            }
            set { sOwnerTitleInsProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sOwnerTitleInsProps, value); }
        }
        [DependsOn(nameof(sOwnerTitleInsProps))]
        public bool sOwnerTitleInsProps_ThisPartyIsAffiliate
        {
            get
            {
                return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sOwnerTitleInsProps);
            }
            set { sOwnerTitleInsProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sOwnerTitleInsProps, value); }
        }
        [DependsOn(nameof(sPestInspectFProps))]
        public bool sPestInspectFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sPestInspectFProps); }
            set { sPestInspectFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sPestInspectFProps, value); }
        }
        [DependsOn(nameof(sPestInspectFProps))]
        public bool sPestInspectFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sPestInspectFProps); }
            set { sPestInspectFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sPestInspectFProps, value); }
        }
        [DependsOn(nameof(sProcFProps))]
        public bool sProcFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sProcFProps); }
            set { sProcFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sProcFProps, value); }
        }
        [DependsOn(nameof(sProcFProps))]
        public bool sProcFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sProcFProps); }
            set { sProcFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sProcFProps, value); }
        }
        [DependsOn(nameof(sRealETxRsrvProps))]
        public bool sRealETxRsrvProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sRealETxRsrvProps); }
            set { sRealETxRsrvProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sRealETxRsrvProps, value); }
        }
        [DependsOn(nameof(sRealETxRsrvProps))]
        public bool sRealETxRsrvProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sRealETxRsrvProps); }
            set { sRealETxRsrvProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sRealETxRsrvProps, value); }
        }
        [DependsOn(nameof(sRecFProps))]
        public bool sRecFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sRecFProps); }
            set { sRecFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sRecFProps, value); }
        }
        [DependsOn(nameof(sRecFProps))]
        public bool sRecFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sRecFProps); }
            set { sRecFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sRecFProps, value); }
        }
        [DependsOn(nameof(sSchoolTxRsrvProps))]
        public bool sSchoolTxRsrvProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sSchoolTxRsrvProps); }
            set { sSchoolTxRsrvProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sSchoolTxRsrvProps, value); }
        }
        [DependsOn(nameof(sSchoolTxRsrvProps))]
        public bool sSchoolTxRsrvProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sSchoolTxRsrvProps); }
            set { sSchoolTxRsrvProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sSchoolTxRsrvProps, value); }
        }
        [DependsOn(nameof(sStateRtcProps))]
        public bool sStateRtcProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sStateRtcProps); }
            set { sStateRtcProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sStateRtcProps, value); }
        }
        [DependsOn(nameof(sStateRtcProps))]
        public bool sStateRtcProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sStateRtcProps); }
            set { sStateRtcProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sStateRtcProps, value); }
        }
        [DependsOn(nameof(sTitleInsFProps))]
        public bool sTitleInsFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sTitleInsFProps); }
            set { sTitleInsFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sTitleInsFProps, value); }
        }
        [DependsOn(nameof(sTitleInsFProps))]
        public bool sTitleInsFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sTitleInsFProps); }
            set { sTitleInsFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sTitleInsFProps, value); }
        }
        [DependsOn(nameof(sTxServFProps))]
        public bool sTxServFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sTxServFProps); }
            set { sTxServFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sTxServFProps, value); }
        }
        [DependsOn(nameof(sTxServFProps))]
        public bool sTxServFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sTxServFProps); }
            set { sTxServFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sTxServFProps, value); }
        }
        [DependsOn(nameof(sU1GovRtcProps))]
        public bool sU1GovRtcProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sU1GovRtcProps); }
            set { sU1GovRtcProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sU1GovRtcProps, value); }
        }
        [DependsOn(nameof(sU1GovRtcProps))]
        public bool sU1GovRtcProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sU1GovRtcProps); }
            set { sU1GovRtcProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sU1GovRtcProps, value); }
        }
        [DependsOn(nameof(sU1ScProps))]
        public bool sU1ScProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sU1ScProps); }
            set { sU1ScProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sU1ScProps, value); }
        }
        [DependsOn(nameof(sU1ScProps))]
        public bool sU1ScProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sU1ScProps); }
            set { sU1ScProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sU1ScProps, value); }
        }
        [DependsOn(nameof(sU1TcProps))]
        public bool sU1TcProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sU1TcProps); }
            set { sU1TcProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sU1TcProps, value); }
        }
        [DependsOn(nameof(sU1TcProps))]
        public bool sU1TcProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sU1TcProps); }
            set { sU1TcProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sU1TcProps, value); }
        }
        [DependsOn(nameof(sU2GovRtcProps))]
        public bool sU2GovRtcProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sU2GovRtcProps); }
            set { sU2GovRtcProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sU2GovRtcProps, value); }
        }
        [DependsOn(nameof(sU2GovRtcProps))]
        public bool sU2GovRtcProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sU2GovRtcProps); }
            set { sU2GovRtcProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sU2GovRtcProps, value); }
        }
        [DependsOn(nameof(sU2ScProps))]
        public bool sU2ScProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sU2ScProps); }
            set { sU2ScProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sU2ScProps, value); }
        }
        [DependsOn(nameof(sU2ScProps))]
        public bool sU2ScProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sU2ScProps); }
            set { sU2ScProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sU2ScProps, value); }
        }
        [DependsOn(nameof(sU2TcProps))]
        public bool sU2TcProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sU2TcProps); }
            set { sU2TcProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sU2TcProps, value); }
        }
        [DependsOn(nameof(sU2TcProps))]
        public bool sU2TcProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sU2TcProps); }
            set { sU2TcProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sU2TcProps, value); }
        }
        [DependsOn(nameof(sU3GovRtcProps))]
        public bool sU3GovRtcProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sU3GovRtcProps); }
            set { sU3GovRtcProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sU3GovRtcProps, value); }
        }
        [DependsOn(nameof(sU3GovRtcProps))]
        public bool sU3GovRtcProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sU3GovRtcProps); }
            set { sU3GovRtcProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sU3GovRtcProps, value); }
        }
        [DependsOn(nameof(sU3ScProps))]
        public bool sU3ScProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sU3ScProps); }
            set { sU3ScProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sU3ScProps, value); }
        }
        [DependsOn(nameof(sU3ScProps))]
        public bool sU3ScProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sU3ScProps); }
            set { sU3ScProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sU3ScProps, value); }
        }
        [DependsOn(nameof(sU3TcProps))]
        public bool sU3TcProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sU3TcProps); }
            set { sU3TcProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sU3TcProps, value); }
        }
        [DependsOn(nameof(sU3TcProps))]
        public bool sU3TcProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sU3TcProps); }
            set { sU3TcProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sU3TcProps, value); }
        }
        [DependsOn(nameof(sU4ScProps))]
        public bool sU4ScProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sU4ScProps); }
            set { sU4ScProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sU4ScProps, value); }
        }
        [DependsOn(nameof(sU4ScProps))]
        public bool sU4ScProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sU4ScProps); }
            set { sU4ScProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sU4ScProps, value); }
        }
        [DependsOn(nameof(sU4TcProps))]
        public bool sU4TcProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sU4TcProps); }
            set { sU4TcProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sU4TcProps, value); }
        }
        [DependsOn(nameof(sU4TcProps))]
        public bool sU4TcProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sU4TcProps); }
            set { sU4TcProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sU4TcProps, value); }
        }
        [DependsOn(nameof(sU5ScProps))]
        public bool sU5ScProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sU5ScProps); }
            set { sU5ScProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sU5ScProps, value); }
        }
        [DependsOn(nameof(sU5ScProps))]
        public bool sU5ScProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sU5ScProps); }
            set { sU5ScProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sU5ScProps, value); }
        }
        [DependsOn(nameof(sUwFProps))]
        public bool sUwFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sUwFProps); }
            set { sUwFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sUwFProps, value); }
        }
        [DependsOn(nameof(sUwFProps))]
        public bool sUwFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sUwFProps); }
            set { sUwFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sUwFProps, value); }
        }
        [DependsOn(nameof(sVaFfProps))]
        public bool sVaFfProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sVaFfProps); }
            set { sVaFfProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sVaFfProps, value); }
        }
        [DependsOn(nameof(sVaFfProps))]
        public bool sVaFfProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sVaFfProps); }
            set { sVaFfProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sVaFfProps, value); }
        }
        [DependsOn(nameof(sWireFProps))]
        public bool sWireFProps_PaidToThirdParty
        {
            get { return LosConvert.GfeItemProps_PaidToThirdParty(sWireFProps); }
            set { sWireFProps = LosConvert.GfeItemProps_UpdatePaidToThirdParty(sWireFProps, value); }
        }
        [DependsOn(nameof(sWireFProps))]
        public bool sWireFProps_ThisPartyIsAffiliate
        {
            get { return LosConvert.GfeItemProps_ThisPartyIsAffiliate(sWireFProps); }
            set { sWireFProps = LosConvert.GfeItemProps_UpdateThisPartyIsAffiliate(sWireFProps, value); }
        }

        [DependsOn(nameof(s1006RsrvProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool s1006RsrvProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(s1006RsrvProps); }
            set { s1006RsrvProps = LosConvert.GfeItemProps_UpdateShowQmWarning(s1006RsrvProps, value); }
        }

        [DependsOn(nameof(s1007RsrvProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool s1007RsrvProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(s1007RsrvProps); }
            set { s1007RsrvProps = LosConvert.GfeItemProps_UpdateShowQmWarning(s1007RsrvProps, value); }
        }
 
        [DependsOn(nameof(sU3RsrvProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sU3RsrvProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sU3RsrvProps); }
            set { sU3RsrvProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sU3RsrvProps, value); }
        }

        [DependsOn(nameof(sU4RsrvProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sU4RsrvProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sU4RsrvProps); }
            set { sU4RsrvProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sU4RsrvProps, value); }
        }

        [DependsOn(nameof(s800U1FProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool s800U1FProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(s800U1FProps); }
            set { s800U1FProps = LosConvert.GfeItemProps_UpdateShowQmWarning(s800U1FProps, value); }
        }

        [DependsOn(nameof(s800U2FProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool s800U2FProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(s800U2FProps); }
            set { s800U2FProps = LosConvert.GfeItemProps_UpdateShowQmWarning(s800U2FProps, value); }
        }

        [DependsOn(nameof(s800U3FProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool s800U3FProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(s800U3FProps); }
            set { s800U3FProps = LosConvert.GfeItemProps_UpdateShowQmWarning(s800U3FProps, value); }
        }

        [DependsOn(nameof(s800U4FProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool s800U4FProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(s800U4FProps); }
            set { s800U4FProps = LosConvert.GfeItemProps_UpdateShowQmWarning(s800U4FProps, value); }
        }

        [DependsOn(nameof(s800U5FProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool s800U5FProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(s800U5FProps); }
            set { s800U5FProps = LosConvert.GfeItemProps_UpdateShowQmWarning(s800U5FProps, value); }
        }

        [DependsOn(nameof(s900U1PiaProps))]
        [DevNote("s900U1PiaProps Show QM Warning bit.")]
        public bool s900U1PiaProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(s900U1PiaProps); }
            set { s900U1PiaProps = LosConvert.GfeItemProps_UpdateShowQmWarning(s900U1PiaProps, value); }
        }

        [DependsOn(nameof(s904PiaProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool s904PiaProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(s904PiaProps); }
            set { s904PiaProps = LosConvert.GfeItemProps_UpdateShowQmWarning(s904PiaProps, value); }
        }

        [DependsOn(nameof(sAggregateAdjRsrvProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sAggregateAdjRsrvProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sAggregateAdjRsrvProps); }
            set { sAggregateAdjRsrvProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sAggregateAdjRsrvProps, value); }
        }

        [DependsOn(nameof(sApprFProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sApprFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sApprFProps); }
            set { sApprFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sApprFProps, value); }
        }

        [DependsOn(nameof(sAttorneyFProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sAttorneyFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sAttorneyFProps); }
            set { sAttorneyFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sAttorneyFProps, value); }
        }

        [DependsOn(nameof(sCountyRtcProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sCountyRtcProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sCountyRtcProps); }
            set { sCountyRtcProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sCountyRtcProps, value); }
        }

        [DependsOn(nameof(sCrFProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sCrFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sCrFProps); }
            set { sCrFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sCrFProps, value); }
        }
 
        [DependsOn(nameof(sDocPrepFProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sDocPrepFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sDocPrepFProps); }
            set { sDocPrepFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sDocPrepFProps, value); }
        }

        [DependsOn(nameof(sEscrowFProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sEscrowFProps_ShowQmWarning
        {
            get
            {
                return LosConvert.GfeItemProps_ShowQmWarning(sEscrowFProps);
            }
            set { sEscrowFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sEscrowFProps, value); }
        }

        [DependsOn(nameof(sFloodCertificationFProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sFloodCertificationFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sFloodCertificationFProps); }
            set { sFloodCertificationFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sFloodCertificationFProps, value); }
        }

        [DependsOn(nameof(sFloodInsRsrvProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sFloodInsRsrvProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sFloodInsRsrvProps); }
            set { sFloodInsRsrvProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sFloodInsRsrvProps, value); }
        }

        [DependsOn(nameof(sGfeDiscountPointFProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sGfeDiscountPointFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sGfeDiscountPointFProps); }
            set { sGfeDiscountPointFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sGfeDiscountPointFProps, value); }
        }

        [DependsOn(nameof(sGfeLenderCreditFProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sGfeLenderCreditFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sGfeLenderCreditFProps); }
            set { sGfeLenderCreditFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sGfeLenderCreditFProps, value); }
        }

        [DependsOn(nameof(sGfeOriginatorCompFProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sGfeOriginatorCompFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sGfeOriginatorCompFProps); }
            set { sGfeOriginatorCompFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sGfeOriginatorCompFProps, value); }
        }

        [DependsOn(nameof(sHazInsPiaProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sHazInsPiaProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sHazInsPiaProps); }
            set { sHazInsPiaProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sHazInsPiaProps, value); }
        }

        [DependsOn(nameof(sHazInsRsrvProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sHazInsRsrvProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sHazInsRsrvProps); }
            set { sHazInsRsrvProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sHazInsRsrvProps, value); }
        }

        [DependsOn(nameof(sInspectFProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sInspectFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sInspectFProps); }
            set { sInspectFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sInspectFProps, value); }
        }

        [DependsOn(nameof(sIPiaProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sIPiaProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sIPiaProps); }
            set { sIPiaProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sIPiaProps, value); }
        }

        [DependsOn(nameof(sLOrigFProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sLOrigFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sLOrigFProps); }
            set { sLOrigFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sLOrigFProps, value); }
        }

        [DependsOn(nameof(sMBrokFProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sMBrokFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sMBrokFProps); }
            set { sMBrokFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sMBrokFProps, value); }
        }

        [DependsOn(nameof(sMInsRsrvProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sMInsRsrvProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sMInsRsrvProps); }
            set { sMInsRsrvProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sMInsRsrvProps, value); }
        }

        [DependsOn(nameof(sMipPiaProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sMipPiaProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sMipPiaProps); }
            set { sMipPiaProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sMipPiaProps, value); }
        }

        [DependsOn(nameof(sNotaryFProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sNotaryFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sNotaryFProps); }
            set { sNotaryFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sNotaryFProps, value); }
        }

        [DependsOn(nameof(sOwnerTitleInsProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sOwnerTitleInsProps_ShowQmWarning
        {
            get
            {
                return LosConvert.GfeItemProps_ShowQmWarning(sOwnerTitleInsProps);
            }
            set { sOwnerTitleInsProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sOwnerTitleInsProps, value); }
        }

        [DependsOn(nameof(sPestInspectFProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sPestInspectFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sPestInspectFProps); }
            set { sPestInspectFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sPestInspectFProps, value); }
        }

        [DependsOn(nameof(sProcFProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sProcFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sProcFProps); }
            set { sProcFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sProcFProps, value); }
        }

        [DependsOn(nameof(sRealETxRsrvProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sRealETxRsrvProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sRealETxRsrvProps); }
            set { sRealETxRsrvProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sRealETxRsrvProps, value); }
        }

        [DependsOn(nameof(sRecFProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sRecFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sRecFProps); }
            set { sRecFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sRecFProps, value); }
        }

        [DependsOn(nameof(sSchoolTxRsrvProps))]
        [DevNote("sSchoolTxRsrvProps Show QM Warning bit.")]
        public bool sSchoolTxRsrvProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sSchoolTxRsrvProps); }
            set { sSchoolTxRsrvProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sSchoolTxRsrvProps, value); }
        }

        [DependsOn(nameof(sStateRtcProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sStateRtcProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sStateRtcProps); }
            set { sStateRtcProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sStateRtcProps, value); }
        }

        [DependsOn(nameof(sTitleInsFProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sTitleInsFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sTitleInsFProps); }
            set { sTitleInsFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sTitleInsFProps, value); }
        }

        [DependsOn(nameof(sTxServFProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sTxServFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sTxServFProps); }
            set { sTxServFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sTxServFProps, value); }
        }

        [DependsOn(nameof(sU1GovRtcProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sU1GovRtcProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sU1GovRtcProps); }
            set { sU1GovRtcProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sU1GovRtcProps, value); }
        }

        [DependsOn(nameof(sU1ScProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sU1ScProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sU1ScProps); }
            set { sU1ScProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sU1ScProps, value); }
        }

        [DependsOn(nameof(sU1TcProps))]
        [DevNote("sU1TcProps Show QM Warning bit.")]
        public bool sU1TcProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sU1TcProps); }
            set { sU1TcProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sU1TcProps, value); }
        }

        [DependsOn(nameof(sU2GovRtcProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sU2GovRtcProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sU2GovRtcProps); }
            set { sU2GovRtcProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sU2GovRtcProps, value); }
        }

        [DependsOn(nameof(sU2ScProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sU2ScProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sU2ScProps); }
            set { sU2ScProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sU2ScProps, value); }
        }

        [DependsOn(nameof(sU2TcProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sU2TcProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sU2TcProps); }
            set { sU2TcProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sU2TcProps, value); }
        }

        [DependsOn(nameof(sU3GovRtcProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sU3GovRtcProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sU3GovRtcProps); }
            set { sU3GovRtcProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sU3GovRtcProps, value); }
        }

        [DependsOn(nameof(sU3ScProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sU3ScProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sU3ScProps); }
            set { sU3ScProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sU3ScProps, value); }
        }

        [DependsOn(nameof(sU3TcProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sU3TcProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sU3TcProps); }
            set { sU3TcProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sU3TcProps, value); }
        }

        [DependsOn(nameof(sU4ScProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sU4ScProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sU4ScProps); }
            set { sU4ScProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sU4ScProps, value); }
        }

        [DependsOn(nameof(sU4TcProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sU4TcProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sU4TcProps); }
            set { sU4TcProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sU4TcProps, value); }
        }

        [DependsOn(nameof(sU5ScProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sU5ScProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sU5ScProps); }
            set { sU5ScProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sU5ScProps, value); }
        }

        [DependsOn(nameof(sUwFProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sUwFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sUwFProps); }
            set { sUwFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sUwFProps, value); }
        }

        [DependsOn(nameof(sVaFfProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sVaFfProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sVaFfProps); }
            set { sVaFfProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sVaFfProps, value); }
        }

        [DependsOn(nameof(sWireFProps))]
        [DevNote("sWireFProps Show QM Warning bit.")]
        public bool sWireFProps_ShowQmWarning
        {
            get { return LosConvert.GfeItemProps_ShowQmWarning(sWireFProps); }
            set { sWireFProps = LosConvert.GfeItemProps_UpdateShowQmWarning(sWireFProps, value); }
        }
    }
}
