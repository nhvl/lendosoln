using System;
using System.Data;
using LendersOffice.Common;

namespace DataAccess
{
	abstract public class CXmlRecordBase2 : CXmlRecordBase, ICollectionItemBase2
    {
		private DataRow m_row;
		protected DataSet m_ds; 
		private CBase m_parent;
		private string m_tableName;
		private IRecordCollection m_recordList;
		private bool m_isOnDeathRow = false;
		
		public virtual string SortValueFor( string fieldName )
		{
			return m_row[fieldName].ToString();
		}

		public bool IsOnDeathRow
		{
			get{ return m_isOnDeathRow; }
			set{ m_isOnDeathRow = value; }
		}

		private bool m_bIsValid;
		
		protected CXmlRecordBase2()
		{
			m_bIsValid = false;	
		}
		
		public CXmlRecordBase2( CBase parent, DataSet ds, IRecordCollection recordList, int iRow, string tableName )
		{
			m_ds = ds;
			m_recordList = recordList;
			m_parent = parent;
			m_tableName = tableName;

			DataTable table = ds.Tables[m_tableName];
			DataRowCollection rows = table.Rows;

			if( 0 > iRow || rows.Count <= iRow )
				throw new CBaseException( ErrorMessages.Generic, "Fails to reconstruct record.  Invalid row position: " + iRow.ToString() );
			m_row = table.Rows[iRow];
			m_bIsValid = true;
		}

        // DO NOT USE!  CREATED JUST TO ENABLE SOME QUICK UNIT TESTING OF THE SHIM
        public CXmlRecordBase2(DataSet data, LosConvert converter, Guid recordId)
            : base(converter)
        {
            m_ds = data;
            var table = m_ds.Tables[0];
            m_tableName = table.TableName;

            for (int i = 0; i < table.Rows.Count; ++i)
            {
                if (recordId.Equals(new Guid(table.Rows[i]["RecordId"].ToString())))
                {
                    m_row = table.Rows[i];
                    break;
                }
            }

            m_bIsValid = true;
        }

		abstract public void PrepareToFlush();
		/// <summary>
		/// Create new row
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="ds"></param>
		/// <param name="recordList"></param>
		/// <param name="tableName"></param>
		public CXmlRecordBase2( CBase parent, DataSet ds, IRecordCollection recordList, string tableName )
		{
			m_ds = ds;
			m_recordList = recordList;
			m_parent = parent;
			m_tableName = tableName;

			DataTable table = ds.Tables[m_tableName];
			m_row = table.NewRow();
			m_row["RecordId"] = Guid.NewGuid().ToString();
			table.Rows.Add( m_row );
			
			m_bIsValid = true;
		}
        protected override string TableName 
        {
            get { return m_tableName; }
        }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="ds"></param>
		///	<param name="recordList"></param>
		/// <param name="recordId"></param>
		/// <param name="tableName"></param>
		public CXmlRecordBase2( CBase parent, DataSet ds, IRecordCollection recordList, Guid recordId, string tableName )
		{
			m_ds = ds;
			m_recordList = recordList;
			m_parent = parent;
			m_tableName = tableName;

			DataTable table = ds.Tables[m_tableName];

			bool bFoundRecord = false;
			for( int i = 0; i < table.Rows.Count; ++i )
			{
				if( recordId.Equals( new Guid( table.Rows[i]["RecordId"].ToString() ) ) )
				{
					m_row = table.Rows[i];
					bFoundRecord = true;
					break;
				}
			}
			if( !bFoundRecord )
				throw new CBaseException( ErrorMessages.Generic, "Fails to reconstruct record.  Cannot find existing row with record id " + recordId.ToString() );
			m_bIsValid = true;
		}

		public DataRow DataRow
		{
			get { return m_row; }
		}

		public override bool IsValid
		{
			get { return m_bIsValid; }
		}


        protected override DataRow CurrentRow 
        {
            get { return m_row; }
        }

        protected override CBase Parent 
        {
            get { return m_parent; }
        }
		public int RowPos
		{
			get 
			{
				DataRowCollection rows = this.m_ds.Tables[0].Rows;
				for( int i = 0; i < rows.Count; ++i )
				{
					if( rows[i] == m_row )
						return i;
				}
				throw new CBaseException( ErrorMessages.Generic, "Invalid RowPos." );
			}
			
		}

		public double OrderRankValue
		{
			get 
			{ 
				if( IsValid )
				{
					string rawVal = m_row["OrderRankValue"].ToString();
					if( "" == rawVal )
					{
						m_row["OrderRankValue"] = "100";
						return 100D;
					}

					return double.Parse( rawVal ); 
				}
				throw new CBaseException( ErrorMessages.Generic, "Invalid OrderRankValue." );
			}
			set
			{
				m_row["OrderRankValue"] = value.ToString();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="record"></param>
		/// <returns>new position, if it's already at 0, 0 will be return</returns>
		public void MoveUp()
		{
			m_recordList.MoveRegularRecordUp( this );
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="record"></param>
		/// <returns>new position, if it's already at the end, count-1 will be returned.</returns>
		public void MoveDown()
		{
			m_recordList.MoveRegularRecordDown( this );
		}

        //TODO: Remove the need of calling Update for each indvidual record.  Do it
        // at the record list level instead. That would save time for importing feature.
        //[Obsolete("Use IRecordCollection.Flush() instead.", true)]
        override public void Update()
		{
			if( IsValid )
			{
				m_recordList.Flush();
			}
			else
				Tools.LogError( "Programming Error, CXmlRecordBase.Update() is called on an invalid CXmlRecordBase object." ); 
		}
		public Guid RecordId
		{
			get
			{
				if( !IsValid )
					return Guid.Empty;

				string val = m_row["RecordId"].ToString();
				if( 0 == val.Length )
					throw new CBaseException( ErrorMessages.Generic, "Encountered a record in " + m_tableName + " without id." );
				return new Guid( val );
			}
		}

		/// <summary>
		/// Check with Thinh before using this method
		/// </summary>
		public void ChangeRecordId( Guid newId )
		{
			if( Guid.Empty == newId )
				throw new CBaseException( ErrorMessages.Generic, "Cannot change RecordId to Guid.Empty.  CXmlRecordBase.ChangeRecordId()" );
			SetGuid( "RecordId", newId );
		}

	}

}
