﻿// <copyright file="LenderCustomCredit.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>This is LenderCustomCredit class.</summary>
// <author>david</author>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;

    /// <summary>
    /// JSON Representation of Lender Custom Credit.
    /// </summary>
    [DataContract]
    public class LenderCustomCredit
    {
        /// <summary>
        /// Gets or sets the disclose location.
        /// </summary>
        /// <value>The disclose location.</value>
        [DataMember(Name = "location")]
        public E_LenderCreditDiscloseLocationT DiscloseLocationT { get; set; }

        /// <summary>
        /// Gets or sets the description of lender credit.
        /// </summary>
        /// <value>The description of lender credit.</value>
        [DataMember(Name = "desc")]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the amount of lender credit.
        /// </summary>
        /// <value>The amount of lender credit.</value>
        [DataMember(Name = "amt")]
        public decimal Amount { get; set; }
    }
}
