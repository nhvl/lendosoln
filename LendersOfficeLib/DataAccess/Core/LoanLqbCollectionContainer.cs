﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using LendersOffice.Common;
    using LendingQB.Core;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Container class for holding the collections related to a loan file.
    /// </summary>
    /// <remarks>
    /// If a method has arguments consisting of Guids then that method is called by a shim and should
    /// be enforced as such (see below).  If a method has arguments consisting of DataObjectIdentifiers then 
    /// the method is called by the new collections code and should be marked as public.
    /// </remarks>
    internal sealed partial class LoanLqbCollectionContainer : ILoanLqbCollectionContainer, IShimContainer
    {
        /// <summary>
        /// The loan data.
        /// </summary>
        private ILoanLqbCollectionContainerLoanDataProvider loanData;

        /// <summary>
        /// Interface used to manage consumers and legacy applications.
        /// </summary>
        private ILoanLqbCollectionBorrowerManagement borrMgmt;

        /// <summary>
        /// The observer for the container.
        /// </summary>
        private ILoanLqbCollectionContainerObserver observer;

        /// <summary>
        /// Consumer ordering for the loan is derived from the ordering for ULAD applications
        /// and consumer ordering for each ULAD application.  In order to keep a consistent 
        /// order throughout various operations, the order needs to be held in memory.  Since 
        /// the loan level order isn't actually stored in the database, we keep a reference to 
        /// the order here.
        /// </summary>
        private IOrderedIdentifierCollection<DataObjectKind.Consumer, Guid> consumerOrder;

        /// <summary>
        /// Suppress the repair code when Save is called.
        /// </summary>
        /// <remarks>
        /// Sometimes we call Save internally prior to making matching changes
        /// to the legacy applications and borrowers.  If the repair code is 
        /// invoked it will re-add the collection items.  The reason it is done
        /// this way has to do with the complexity of the legacy loan management code.
        /// We will just bypass the repair.  Since the Save code is called by
        /// calling down to the loan management which then in turn calls up to the 
        /// LoanLqbCollectionContainer.Save method, there is no good way to handle this
        /// scenario.  This flag is a quick and dirty technique; it is important to
        /// make sure it gets reset.
        /// </remarks>
        private bool suppressRepairOnSave;

        /// <summary>
        /// Remember that the BeginTrackingChanges method was called.
        /// </summary>
        private bool isTrackingChanges;

        /// <summary>
        /// The collections used for this loan action.
        /// </summary>
        private List<string> registeredCollectionNames = new List<string>();

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanLqbCollectionContainer"/> class.
        /// </summary>
        /// <param name="collectionProvider">The data provider that loads/saves the data.</param>
        /// <param name="loanData">The loan data.</param>
        /// <param name="borrMgmt">Interface used to manage consumers and legacy applications.</param>
        /// <param name="observer">The observer for the container.</param>
        public LoanLqbCollectionContainer(
            ILoanLqbCollectionProvider collectionProvider,
            ILoanLqbCollectionContainerLoanDataProvider loanData,
            ILoanLqbCollectionBorrowerManagement borrMgmt,
            ILoanLqbCollectionContainerObserver observer)
        {
            this.collectionProvider = (ILoanLqbCollectionProviderMethods)collectionProvider;
            this.loanData = loanData;
            this.borrMgmt = borrMgmt;
            this.observer = observer;
        }

        /// <summary>
        /// Gets a value indicating whether load has been called.
        /// </summary>
        public bool IsLoaded { get; private set; }

        /// <summary>
        /// Gets the legacy applications.
        /// </summary>
        public IReadOnlyLqbCollection<DataObjectKind.LegacyApplication, Guid, ILegacyApplication> LegacyApplications => this.loanData.LegacyApplications;

        /// <summary>
        /// Gets the legacy application to consumer associations.
        /// </summary>
        public IReadOnlyLqbAssociationSet<DataObjectKind.LegacyApplicationConsumerAssociation, Guid, ILegacyApplicationConsumerAssociation> LegacyApplicationConsumers => this.loanData.LegacyApplicationConsumers;

        /// <summary>
        /// Gets a value indicating whether any collections have been registered.
        /// </summary>
        bool ILoanLqbCollectionContainer.HasRegisteredCollections => this.registeredCollectionNames.Any();

        /// <summary>
        /// Returns true if the the field is the name of a loan-lqb collection.
        /// </summary>
        /// <param name="fieldName">The loan field.</param>
        /// <returns>True if the the field is the name of a loan-lqb collection.</returns>
        public static bool IsLoanLqbCollection(string fieldName)
        {
            return LoanCollections.Contains(fieldName);
        }

        /// <summary>
        /// Registers loan collections that will need to be loaded or saved.
        /// This must be done prior to accessing the collections.
        /// </summary>
        /// <param name="collectionNames">The names of the collections.</param>
        public void RegisterCollections(IEnumerable<string> collectionNames)
        {
            foreach (var collectionName in collectionNames)
            {
                this.RegisterCollection(collectionName);
            }
        }

        /// <summary>
        /// Register a loan collection that will need to be loaded or saved.
        /// This must be done prior to accessing the collection.
        /// </summary>
        /// <param name="collectionName">The name of the collection.</param>
        public void RegisterCollection(string collectionName)
        {
            if (!LoanCollections.Contains(collectionName))
            {
                throw new CBaseException(ErrorMessages.Generic, "Registering Unknown Collection: " + collectionName);
            }

            if (!this.registeredCollectionNames.Contains(collectionName))
            {
                this.registeredCollectionNames.Add(collectionName);
            }
        }

        /// <summary>
        /// Method to initialize ULAD apps when migrating a file onto the ULAD data layer.
        /// </summary>
        public void InitializeUladAppsToMatchLegacyApps()
        {
            if (this.UladApplications.Any() || this.UladApplicationConsumers.Any())
            {
                throw new CBaseException(
                    ErrorMessages.Generic,
                    "This method is only intended to be called when the ULAD applications and associations are empty.");
            }
            
            foreach (var legacyApplication in this.LegacyApplications)
            {
                var uladApp = new UladApplication();
                uladApp.IsPrimary = legacyApplication.Value.IsPrimary;

                var uladAppId = this.UladApplications.Add(uladApp);

                var legacyAppConsumers = this.LegacyApplicationConsumers
                    .Values
                    .Where(a => a.LegacyApplicationId == legacyApplication.Key);

                foreach (var legacyAppConsumer in legacyAppConsumers)
                {
                    this.CreateUladConsumerAssociation(uladAppId, legacyAppConsumer.ConsumerId, legacyApplication.Key, legacyAppConsumer.IsPrimary);
                }
            }
        }

        /// <summary>
        /// Duplicates the ULAD applications from the given source container.
        /// Clears all existing ULAD applications and associations.
        /// </summary>
        /// <param name="source">The source container.</param>
        /// <param name="consumerIdMap">A map from source consumer id to destination consumer id.</param>
        /// <param name="legacyAppIdMap">A map from source legacy app id to destination legacy app id.</param>
        public void DuplicateUladApplications(
            ILoanLqbCollectionContainer source,
            Dictionary<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, DataObjectIdentifier<DataObjectKind.Consumer, Guid>> consumerIdMap,
            Dictionary<DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>, DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>> legacyAppIdMap)
        {
            this.ClearUladApplications();

            var uladAppIdMap = new Dictionary<DataObjectIdentifier<DataObjectKind.UladApplication, Guid>, DataObjectIdentifier<DataObjectKind.UladApplication, Guid>>();

            foreach (var sourceUladApplication in source.UladApplications)
            {
                var destinationUladApplication = new UladApplication(sourceUladApplication.Value);
                var destinationUladApplicationId = this.UladApplications.Add(destinationUladApplication);

                uladAppIdMap.Add(sourceUladApplication.Key, destinationUladApplicationId);
            }

            foreach (var sourceUladApplicationConsumer in source.UladApplicationConsumers)
            {
                var sourceUladApplicationId = sourceUladApplicationConsumer.Value.UladApplicationId;
                var sourceConsumerId = sourceUladApplicationConsumer.Value.ConsumerId;
                var sourceLegacyApplicationId = sourceUladApplicationConsumer.Value.LegacyAppId;

                var destinationApplicationId = uladAppIdMap[sourceUladApplicationId];
                var destinationConsumerId = consumerIdMap[sourceConsumerId];

                this.CreateUladConsumerAssociation(destinationApplicationId, destinationConsumerId, legacyAppIdMap[sourceLegacyApplicationId], sourceUladApplicationConsumer.Value.IsPrimary);
            }

            foreach (var sourceUladAppId in source.UladApplications.Keys)
            {
                var sourceOrder = source.GetConsumerOrderingForUladApplication(sourceUladAppId);
                var destinationOrder = this.GetConsumerOrderingForUladApplicationWithoutRepair(uladAppIdMap[sourceUladAppId]);
                var newDestinationOrder = sourceOrder.Select(sourceConsumerId => consumerIdMap[sourceConsumerId]);
                destinationOrder.ReOrder(destinationOrder.ToArray(), newDestinationOrder.ToArray());
            }
        }

        /// <summary>
        /// Gets a value indicating whether the liability is for the subject property mortgage.
        /// </summary>
        /// <param name="id">The id of the liability.</param>
        /// <param name="liability">The liability.</param>
        /// <returns>True if it's for the subject property mortgage. False if it is not. May return null if needed data is null.</returns>
        public bool? GetIsSpMtg(DataObjectIdentifier<DataObjectKind.Liability, Guid> id, Liability liability)
        {
            if (liability.DebtType == null)
            {
                return null;
            }
            else if (!liability.DebtType.EqualsOneOf(E_DebtT.Mortgage, E_DebtT.Heloc))
            {
                return false;
            }

            var thisAsInterface = this as ILoanLqbCollectionContainer;
            var associatedRealPropertyId = thisAsInterface.GetRealPropertyIdLinkedToLiability(id);

            if (associatedRealPropertyId == null)
            {
                return true;
            }
            else
            {
                return thisAsInterface.IsRealPropertySubjectProperty(associatedRealPropertyId.Value);
            }
        }

        /// <summary>
        /// Sets a value indicating whether the liability is for the first mortgage of the subject property.
        /// </summary>
        /// <param name="id">The liability id.</param>
        /// <param name="liability">The liability.</param>
        /// <param name="value">The value to set.</param>
        public void SetIsSpMtg(DataObjectIdentifier<DataObjectKind.Liability, Guid> id, Liability liability, bool value)
        {
            var thisAsInterface = this as ILoanLqbCollectionContainer;
            if (value)
            {
                thisAsInterface.AssociateLiabilityWithSubjectPropertyReo(id);
            }
            else
            {
                var associatedRealPropertyId = thisAsInterface.GetRealPropertyIdLinkedToLiability(id);

                if (associatedRealPropertyId != null && thisAsInterface.IsRealPropertySubjectProperty(associatedRealPropertyId.Value))
                {
                    var shim = (IShimContainer)this;
                    shim.RemoveRealPropertyAssociationForLiability(id.Value);
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether the liability is the subject property 1st mortgage.
        /// </summary>
        /// <param name="id">The liability id.</param>
        /// <param name="liability">The liability.</param>
        /// <returns>True if the liability is the subject property 1st mortgage. False if not. Null if depended upon data is null.</returns>
        public bool? GetIsSp1stMtg(DataObjectIdentifier<DataObjectKind.Liability, Guid> id, Liability liability)
        {
            var thisAsInterface = this as ILoanLqbCollectionContainer;
            bool? isSubjectPropertyMortgage = thisAsInterface.GetIsSpMtg(id, liability);

            if (isSubjectPropertyMortgage == null)
            {
                return null;
            }
            else if (isSubjectPropertyMortgage.Value)
            {
                return liability.IsSp1stMtgData;
            }

            return false;
        }

        /// <summary>
        /// Sets a value indicating that the liability is the subject property 1st mortgage.
        /// </summary>
        /// <param name="id">The liability id.</param>
        /// <param name="liability">The liability.</param>
        /// <param name="value">The value to set.</param>
        public void SetIsSp1stMtg(DataObjectIdentifier<DataObjectKind.Liability, Guid> id, Liability liability, bool value)
        {
            var thisAsInterface = this as ILoanLqbCollectionContainer;
            bool? isSubjectPropertyMortgage = thisAsInterface.GetIsSpMtg(id, liability);

            if (isSubjectPropertyMortgage.HasValue && isSubjectPropertyMortgage.Value)
            {
                this.ClearIsSp1stMtgDataForAllLiabilities();
            }

            liability.IsSp1stMtgData = value;
        }

        /// <summary>
        /// Clears <see cref="LendingQB.Core.Data.ILiability.IsSp1stMtgData"/> for all liabilities.
        /// </summary>
        public void ClearIsSp1stMtgDataForAllLiabilities()
        {
            foreach (var liability in this.Liabilities.Values)
            {
                liability.IsSp1stMtgData = false;
            }
        }

        /// <summary>
        /// Gets the collection of liabilities linked with a real property.
        /// </summary>
        /// <param name="realPropertyId">The identifier for the real property.</param>
        /// <returns>A list of the linked liabilities.</returns>
        List<CReFields.LiaDisplay> IShimContainer.GetLiabilitesLinkedToRealProperty(Guid realPropertyId)
        {
            var subliabilities = new List<CReFields.LiaDisplay>();
            foreach (var identifierAndLiability in this.Liabilities)
            {
                if (subliabilities.Count >= 3)
                {
                    break;
                }

                var liability = identifierAndLiability.Value;
                if (liability.DebtType.EqualsOneOf(E_DebtT.Mortgage, E_DebtT.Heloc))
                {
                    foreach (var assoc in this.RealPropertyLiabilities.Values)
                    {
                        if (assoc.RealPropertyId.Value == realPropertyId && assoc.LiabilityId == identifierAndLiability.Key)
                        {
                            string id = assoc.LiabilityId.Value.ToString();

                            string name = string.Empty;
                            if (liability.CompanyName != null)
                            {
                                name = liability.CompanyName.Value.ToString();
                            }

                            var bal = 0.00m;
                            if (liability.Bal != null)
                            {
                                bal = Convert.ToDecimal(liability.Bal.Value.Value);
                            }

                            var pmt = 0.00m;
                            if (liability.Pmt != null)
                            {
                                pmt = Convert.ToDecimal(liability.Pmt.Value.Value);
                            }

                            var lia = CReFields.LiaDisplay.CreateLiaDisplay(id, name, bal, pmt);
                            subliabilities.Add(lia);
                            if (subliabilities.Count >= 3)
                            {
                                break;
                            }
                        }
                    }
                }
            }

            return subliabilities;
        }

        /// <summary>
        /// Updates the id of an existing liability.
        /// </summary>
        /// <param name="existingId">The current id.</param>
        /// <param name="newId">The new id.</param>
        public void UpdateLiabilityId(Guid existingId, Guid newId)
        {
            var existingKey = DataObjectIdentifier<DataObjectKind.Liability, Guid>.Create(existingId);
            var newKey = DataObjectIdentifier<DataObjectKind.Liability, Guid>.Create(newId);
            var existingLiability = this.Liabilities[existingKey];

            var existingRealPropertyAssociation = this.RealPropertyLiabilities.Where(a => a.Value.LiabilityId == existingKey);
            var existingConsumerAssociations = this.ConsumerLiabilities.Where(a => a.Value.LiabilityId == existingKey);

            // We will add at the new key first and we will only remove the old item
            // if the addition succeeds.
            this.Liabilities.Add(newKey, existingLiability);

            // Then we want to move the real property associations to the new liability id.
            if (existingRealPropertyAssociation.Any())
            {
                var association = existingRealPropertyAssociation.Single();
                var updatedAssociation = new RealPropertyLiabilityAssociation(
                    association.Value.RealPropertyId,
                    newKey);

                this.RealPropertyLiabilities.Add(updatedAssociation);
                this.RealPropertyLiabilities.Remove(association.Key);
            }

            // Then we want to move the ownership associations over to the new liability id.
            if (existingConsumerAssociations.Any())
            {
                // Call ToList() to avoid issues with modifying the underlying collection
                // while enumerating.
                var existingAssociationKeys = existingConsumerAssociations.Select(a => a.Key).ToList();
                var updatedAssociations = existingConsumerAssociations.Select(existing =>
                {
                    var updated = new ConsumerLiabilityAssociation(
                        existing.Value.ConsumerId,
                        newKey,
                        existing.Value.AppId);
                    updated.IsPrimary = existing.Value.IsPrimary;
                    return updated;
                }).ToList();

                foreach (var updatedAssociation in updatedAssociations)
                {
                    this.ConsumerLiabilities.Add(updatedAssociation);
                }

                foreach (var existingAssociationKey in existingAssociationKeys)
                {
                    this.ConsumerLiabilities.Remove(existingAssociationKey);
                }
            }

            this.Liabilities.Remove(existingKey);
        }

        /// <summary>
        /// Gets the identifier for the real property record associated with the given
        /// liability id.
        /// </summary>
        /// <param name="liabilityId">The id of the liability record.</param>
        /// <returns>The id of the associated real property or Guid.Empty if there isn't one.</returns>
        public DataObjectIdentifier<DataObjectKind.RealProperty, Guid>? GetRealPropertyIdLinkedToLiability(
            DataObjectIdentifier<DataObjectKind.Liability, Guid> liabilityId)
        {
            var association = this.RealPropertyLiabilities
                .Values
                .SingleOrDefault(a => a.LiabilityId == liabilityId);

            if (association == null)
            {
                return null;
            }

            return association.RealPropertyId;
        }

        /// <summary>
        /// Associates a liability with a real property.
        /// </summary>
        /// <param name="liabilityId">The liability id.</param>
        /// <param name="realPropertyId">The real property id.</param>
        public void AssociateLiabilityWithRealProperty(Guid liabilityId, Guid realPropertyId)
        {
            var liaIdentifier = DataObjectIdentifier<DataObjectKind.Liability, Guid>.Create(liabilityId);
            var propIdentifier = DataObjectIdentifier<DataObjectKind.RealProperty, Guid>.Create(realPropertyId);

            this.AssociateLiabilityWithRealProperty(liaIdentifier, propIdentifier);
        }
        
        /// <summary>
        /// Associates a liability with a real property.
        /// </summary>
        /// <param name="liabilityId">The liability id.</param>
        /// <param name="realPropertyId">The real property id.</param>
        public void AssociateLiabilityWithRealProperty(
            DataObjectIdentifier<DataObjectKind.Liability, Guid> liabilityId,
            DataObjectIdentifier<DataObjectKind.RealProperty, Guid> realPropertyId)
        {
            // We will assume that the ID they're passing in is legitimate.
            // The old code would accept any id in the setter, but when retrieving
            // the value it would reset it to Guid.Empty if it didn't exist.
            // The new code will enforce foreign key constraints, so if they try to
            // associate the liability with a non-existent real property it will be
            // rejected at the database level.
            // It is possible (though I'd imagine unlikely) that the association is
            // being created before the real property has been added to the collection,
            // so we aren't going to enforce that the record must exist yet -- we'll
            // let the database handle that at save time.
            var existingAssociation = this.RealPropertyLiabilities
                .Where(a => a.Value.LiabilityId == liabilityId);

            var newAssociation = new RealPropertyLiabilityAssociation(realPropertyId, liabilityId);

            if (!existingAssociation.Any())
            {
                this.RealPropertyLiabilities.Add(newAssociation);
            }
            else if (existingAssociation.Single().Value.RealPropertyId != newAssociation.RealPropertyId)
            {
                this.RealPropertyLiabilities.Remove(existingAssociation.Single().Key);
                this.RealPropertyLiabilities.Add(newAssociation);
            }

            // If the association is the same, we don't need to do anything.
        }

        /// <summary>
        /// Gets a value indicating whether the real property is marked as the subject property.
        /// </summary>
        /// <param name="realPropertyId">The id of the real property.</param>
        /// <returns>A value indicating whether the property is marked as the subject property.</returns>
        /// <exception cref="CBaseException">Thrown when a real property with the specified id does not exist.</exception>
        public bool IsRealPropertySubjectProperty(Guid realPropertyId)
        {
            var id = DataObjectIdentifier<DataObjectKind.RealProperty, Guid>.Create(realPropertyId);

            var thisAsInterface = this as ILoanLqbCollectionContainer;
            return thisAsInterface.IsRealPropertySubjectProperty(id);
        }

        /// <summary>
        /// Gets a value indicating whether the real property is marked as the subject property.
        /// </summary>
        /// <param name="realPropertyId">The id of the real property.</param>
        /// <returns>A value indicating whether the property is marked as the subject property.</returns>
        /// <exception cref="CBaseException">Thrown when a real property with the specified id does not exist.</exception>
        public bool IsRealPropertySubjectProperty(DataObjectIdentifier<DataObjectKind.RealProperty, Guid> realPropertyId)
        {
            if (this.RealProperties.ContainsKey(realPropertyId))
            {
                return this.RealProperties[realPropertyId].IsSubjectProp ?? false;
            }

            throw new CBaseException(ErrorMessages.Generic, "Cannot find REO record - RealProperties collection doesn't contain realPropertyId = " + realPropertyId.ToString());
        }

        /// <summary>
        /// Associates the given liability with a subject property REO. If the
        /// subject property REO does not exist, it is created.
        /// </summary>
        /// <param name="liabilityId">The liability id.</param>
        public void AssociateLiabilityWithSubjectPropertyReo(DataObjectIdentifier<DataObjectKind.Liability, Guid> liabilityId)
        {
            var existingSubjectPropertyReos = this.RealProperties
                .Where(kvp => kvp.Value.IsSubjectProp.HasValue && kvp.Value.IsSubjectProp.Value);

            if (existingSubjectPropertyReos.Any())
            {
                // The old code would just pick the first one.
                var firstSubjectPropertyReo = existingSubjectPropertyReos.First();
                this.AssociateLiabilityWithRealProperty(liabilityId, firstSubjectPropertyReo.Key);
            }
            else
            {
                var newRealProperty = new RealProperty();
                newRealProperty.IsSubjectProp = true;
                var newRealPropertyId = this.RealProperties.Add(newRealProperty);
                this.AssociateLiabilityWithRealProperty(liabilityId, newRealPropertyId);
            }
        }

        /// <summary>
        /// Removes the real property association for the given liability if it exists.
        /// </summary>
        /// <param name="liabilityId">The liability id.</param>
        public void RemoveRealPropertyAssociationForLiability(Guid liabilityId)
        {
            var existingAssociations = this.RealPropertyLiabilities
                .Where(a => a.Value.LiabilityId.Value == liabilityId);

            if (existingAssociations.Any())
            {
                var association = existingAssociations.Single();
                this.RealPropertyLiabilities.Remove(association.Key);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the real property is sold.
        /// </summary>
        /// <param name="realPropertyId">The real property id.</param>
        /// <returns>True if the real property is being sold. False otherwise.</returns>
        public bool IsRealPropertySold(Guid realPropertyId)
        {
            var key = DataObjectIdentifier<DataObjectKind.RealProperty, Guid>.Create(realPropertyId);
            return this.RealProperties[key].Status == E_ReoStatusT.Sale;
        }

        /// <summary>
        /// Sets the ownership type for the given record.
        /// </summary>
        /// <typeparam name="TOwnedKind">The data object kind of the owned entity.</typeparam>
        /// <typeparam name="TAssociationKind">The data object kind of the ownership association.</typeparam>
        /// <typeparam name="TOwnedValue">The type of the owned entity.</typeparam>
        /// <typeparam name="TAssociationValue">The type of the association.</typeparam>
        /// <param name="ownedId">The identifier for the owned entity.</param>
        /// <param name="appId">The application identifier.</param>
        /// <param name="borrowerId">The borrower identifier.</param>
        /// <param name="coborrowerId">The coborrower identifier.</param>
        /// <param name="ownership">The ownership type to set.</param>
        /// <param name="associationFactory">Factory method for creation of instances of the ownership association.</param>
        public void SetOwnership<TOwnedKind, TAssociationKind, TOwnedValue, TAssociationValue>(
            Guid ownedId,
            Guid appId,
            Guid borrowerId,
            Guid coborrowerId,
            Ownership ownership,
            Func<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, DataObjectIdentifier<TOwnedKind, Guid>, DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>, TAssociationValue> associationFactory)
                where TOwnedKind : DataObjectKind
                where TAssociationKind : DataObjectKind
                where TAssociationValue : IPrimaryOwnershipAssociation<TOwnedKind, Guid>
        {
            var recordId = DataObjectIdentifier<TOwnedKind, Guid>.Create(ownedId);
            var applicationId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(appId);
            var borrId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(borrowerId);
            var coborrId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(coborrowerId);

            var collection = this.GetCollection<TOwnedKind, TOwnedValue>();
            var associationSet = this.GetAssociationSet<TAssociationKind, TAssociationValue>();

            bool crossAppPrimaryOwnership = associationSet.Values
                .Any(a => a.OwnedEntityId == recordId && a.IsPrimary && a.ConsumerId != borrId && a.ConsumerId != coborrId);

            if (crossAppPrimaryOwnership)
            {
                // If we are trying to retrieve app-level ownership but the record does not
                // have a primary owner on this application, blow up!
                throw new CBaseException(
                    LendersOffice.Common.ErrorMessages.Generic,
                    "No app-level primary ownership found.");
            }

            var recordAssociations = associationSet.Where(a => a.Value.OwnedEntityId == recordId);
            var existingBorrowerOwnership = recordAssociations.Where(a => a.Value.ConsumerId == borrId);
            var existingCoborrowerOwnership = recordAssociations.Where(a => a.Value.ConsumerId == coborrId);

            if (ownership == Ownership.Borrower)
            {
                bool coborrowerWasPrimary = false;
                if (existingCoborrowerOwnership.Any())
                {
                    coborrowerWasPrimary = existingCoborrowerOwnership.Single().Value.IsPrimary;
                    associationSet.Remove(existingCoborrowerOwnership.Single().Key);
                }

                if (existingBorrowerOwnership.Any() && coborrowerWasPrimary)
                {
                    existingBorrowerOwnership.Single().Value.IsPrimary = true;
                }
                else if (!existingBorrowerOwnership.Any())
                {
                    var newBorrowerAssociation = this.CreateAssociation(applicationId, borrId, recordId, isPrimary: true, associationFactory: associationFactory);
                    associationSet.Add(newBorrowerAssociation);
                }
            }
            else if (ownership == Ownership.Coborrower)
            {
                bool borrowerWasPrimary = false;
                if (existingBorrowerOwnership.Any())
                {
                    borrowerWasPrimary = existingBorrowerOwnership.Single().Value.IsPrimary;
                    associationSet.Remove(existingBorrowerOwnership.Single().Key);
                }

                if (existingCoborrowerOwnership.Any() && borrowerWasPrimary)
                {
                    existingCoborrowerOwnership.Single().Value.IsPrimary = true;
                }
                else if (!existingCoborrowerOwnership.Any())
                {
                    var newCoborrowerAssociation = this.CreateAssociation(applicationId, coborrId, recordId, isPrimary: true, associationFactory: associationFactory);
                    associationSet.Add(newCoborrowerAssociation);
                }
            }
            else if (ownership == Ownership.Joint)
            {
                if (existingBorrowerOwnership.Any() && existingCoborrowerOwnership.Any())
                {
                    return;
                }
                else if (existingBorrowerOwnership.Any())
                {
                    var newCoborrowerAssociation = this.CreateAssociation(applicationId, coborrId, recordId, isPrimary: false, associationFactory: associationFactory);
                    associationSet.Add(newCoborrowerAssociation);
                }
                else if (existingCoborrowerOwnership.Any())
                {
                    var newBorrowerAssociation = this.CreateAssociation(applicationId, borrId, recordId, isPrimary: false, associationFactory: associationFactory);
                    associationSet.Add(newBorrowerAssociation);
                }
                else
                {
                    var newBorrowerAssociation = this.CreateAssociation(applicationId, borrId, recordId, isPrimary: true, associationFactory: associationFactory);
                    associationSet.Add(newBorrowerAssociation);

                    var newCoborrowerAssociation = this.CreateAssociation(applicationId, coborrId, recordId, isPrimary: false, associationFactory: associationFactory);
                    associationSet.Add(newCoborrowerAssociation);
                }
            }
            else
            {
                throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, $"Unhandled Ownership: ({nameof(ownership)}={ownership}) ");
            }
        }

        /// <summary>
        /// Gets the ownership type for the given record.
        /// </summary>
        /// <typeparam name="TOwnedKind">The data object kind of the owned entity.</typeparam>
        /// <typeparam name="TAssociationKind">The data object kind of the ownership association.</typeparam>
        /// <typeparam name="TOwnedValue">The type of the owned entity.</typeparam>
        /// <typeparam name="TAssociationValue">The type of the association.</typeparam>
        /// <param name="ownedId">The record id.</param>
        /// <param name="borrowerId">The borrower identifier.</param>
        /// <param name="coborrowerId">The coborrower identifier.</param>
        /// <returns>The ownership type for the given record id.</returns>
        public Ownership GetOwnership<TOwnedKind, TAssociationKind, TOwnedValue, TAssociationValue>(Guid ownedId, Guid borrowerId, Guid coborrowerId)
            where TOwnedKind : DataObjectKind
            where TAssociationKind : DataObjectKind
            where TAssociationValue : IPrimaryOwnershipAssociation<TOwnedKind, Guid>
        {
            var recordId = DataObjectIdentifier<TOwnedKind, Guid>.Create(ownedId);

            var associationSet = this.GetAssociationSet<TAssociationKind, TAssociationValue>();

            bool crossAppPrimaryOwnership = associationSet.Values
                .Any(a => a.OwnedEntityId == recordId && a.IsPrimary && a.ConsumerId.Value != borrowerId && a.ConsumerId.Value != coborrowerId);

            if (crossAppPrimaryOwnership)
            {
                // If we are trying to retrieve app-level ownership but the record does not
                // have a primary owner on this application, blow up!
                throw new CBaseException(
                    LendersOffice.Common.ErrorMessages.Generic,
                    "No app-level primary ownership found.");
            }

            var recordAssociations = associationSet.Values.Where(a => a.OwnedEntityId == recordId);
            var borrowerAssociated = recordAssociations.Any(a => a.ConsumerId.Value == borrowerId);
            var coborrowerAssociated = recordAssociations.Any(a => a.ConsumerId.Value == coborrowerId);

            if (borrowerAssociated && coborrowerAssociated)
            {
                return Ownership.Joint;
            }
            else if (borrowerAssociated)
            {
                return Ownership.Borrower;
            }
            else if (coborrowerAssociated)
            {
                return Ownership.Coborrower;
            }
            else
            {
                // If we're trying to retrieve ownership but this application has no ownership
                // associations, blow up!
                throw new CBaseException(
                    LendersOffice.Common.ErrorMessages.Generic,
                    "No ownership associations found.");
            }
        }

        /// <summary>
        /// Gets the identifier for the real property record associated with the given
        /// liability id.
        /// </summary>
        /// <param name="liabilityId">The id of the liability record.</param>
        /// <returns>The id of the associated real property or Guid.Empty if there isn't one.</returns>
        public Guid GetRealPropertyIdLinkedToLiability(Guid liabilityId)
        {
            var id = DataObjectIdentifier<DataObjectKind.Liability, Guid>.Create(liabilityId);

            var thisAsInterface = this as ILoanLqbCollectionContainer;
            var realPropertyId = thisAsInterface.GetRealPropertyIdLinkedToLiability(id);

            return realPropertyId == null ? Guid.Empty : realPropertyId.Value.Value;
        }

        /// <summary>
        /// Associates the given liability with a subject property REO. If the
        /// subject property REO does not exist, it is created.
        /// </summary>
        /// <param name="liabilityId">The liability id.</param>
        public void AssociateLiabilityWithSubjectPropertyReo(Guid liabilityId)
        {
            var id = DataObjectIdentifier<DataObjectKind.Liability, Guid>.Create(liabilityId);

            var thisAsInterface = this as ILoanLqbCollectionContainer;
            thisAsInterface.AssociateLiabilityWithSubjectPropertyReo(id);
        }

        /// <summary>
        /// Gets the consumer ordering at the loan level.
        /// </summary>
        /// <returns>The consumer ordering at the loan level.</returns>
        public IOrderedIdentifierCollection<DataObjectKind.Consumer, Guid> GetConsumerOrderingForLoan()
        {
            if (this.consumerOrder == null)
            {
                this.RepairUladConsumerAssociations();

                var baseOrders = new List<IOrderedIdentifierCollection<DataObjectKind.Consumer, Guid>>();
                var order = OrderedIdentifierCollection<DataObjectKind.Consumer, Guid>.Create() as IMutableOrderedIdentifierCollection<DataObjectKind.Consumer, Guid>;
                foreach (var uladAppId in this.UladApplications.Keys)
                {
                    baseOrders.Add(this.GetConsumerOrderingForUladApplicationWithoutRepair(uladAppId));
                }

                this.consumerOrder = new DerivedOrderedIdentifierCollection<DataObjectKind.Consumer, Guid>(baseOrders);
            }

            return this.consumerOrder;
        }

        /// <summary>
        /// Gets the consumer ordering at the level of the ULAD application.
        /// </summary>
        /// <param name="uladAppId">The ULAD application identifier.</param>
        /// <returns>The consumer ordering at the level of the ULAD application.</returns>
        public IOrderedIdentifierCollection<DataObjectKind.Consumer, Guid> GetConsumerOrderingForUladApplication(DataObjectIdentifier<DataObjectKind.UladApplication, Guid> uladAppId)
        {
            this.RepairUladConsumerAssociations();
            return this.GetConsumerOrderingForUladApplicationWithoutRepair(uladAppId);
        }

        /// <summary>
        /// Gets the entity order for the given data object kind and legacy application.
        /// </summary>
        /// <typeparam name="TValueKind">The data object kind.</typeparam>
        /// <param name="appId">The legacy application id.</param>
        /// <returns>The ordering for the given data object kind and legacy application.</returns>
        public IOrderedIdentifierCollection<TValueKind, Guid> GetOrderForApplication<TValueKind>(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId)
            where TValueKind : DataObjectKind
        {
            return this.collectionProvider.RetrieveCollectionOrder<TValueKind>(appId);
        }

        /// <summary>
        /// Gets the item ordering for a given consumer.
        /// </summary>
        /// <typeparam name="TValueKind">The type of the data object for which an order needs to be retrieved.</typeparam>
        /// <param name="consumerId">The consumer id.</param>
        /// <returns>The item ordering.</returns>
        public IOrderedIdentifierCollection<TValueKind, Guid> GetOrderForConsumer<TValueKind>(DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId)
            where TValueKind : DataObjectKind
        {
            return this.collectionProvider.RetrieveCollectionOrder<TValueKind>(consumerId: consumerId);
        }

        /// <summary>
        /// Add a new ULAD application to the entity collection.
        /// </summary>
        /// <remarks>
        /// A ULAD application must have a consumer, so an empty consumer needs to be created
        /// and association generated.  Since the consumer is stored with the legacy application,
        /// this implies the creation of a new legacy application.
        /// </remarks>
        /// <returns>The newly added record and its identifier.</returns>
        public KeyValuePair<DataObjectIdentifier<DataObjectKind.UladApplication, Guid>, UladApplication> AddBlankUladApplication()
        {
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> legacyAppId;
            KeyValuePair<DataObjectIdentifier<DataObjectKind.UladApplication, Guid>, UladApplication> uladData;
            this.AddBlankLegacyAndUladApplication(out legacyAppId, out uladData);
            return uladData;
        }

        /// <summary>
        /// Remove the ULAD application and any borrowers associated with the application.
        /// </summary>
        /// <param name="uladApplicationId">The identifier for the ULAD application.</param>
        public void RemoveUladApplication(DataObjectIdentifier<DataObjectKind.UladApplication, Guid> uladApplicationId)
        {
            if (this.UladApplications.Keys.Where(k => k == uladApplicationId).Count() == 0)
            {
                throw new CBaseException(ErrorMessages.Generic, "Attempting to delete a ULAD application that isn't in the collection.");
            }

            if (this.UladApplications.Count == 1)
            {
                throw new CBaseException(ErrorMessages.Generic, "Attempting to delete the only ULAD application for a loan.");
            }

            const int Empty = 0;
            const int Borrower = 1;
            const int Coborrower = 2;
            const int Joint = 3;

            var borrowerToApp = new Dictionary<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>>();
            var coborrowerToApp = new Dictionary<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>>();
            var currentState = new Dictionary<DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>, int>();
            var desiredState = new Dictionary<DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>, int>();

            // Setup the data used to figure out what operations need be done.
            var legacyAppConsumers = this.LegacyApplicationConsumers;
            foreach (var association in legacyAppConsumers.Values)
            {
                if (!currentState.ContainsKey(association.LegacyApplicationId))
                {
                    DataObjectIdentifier<DataObjectKind.Consumer, Guid> borrowerId;
                    DataObjectIdentifier<DataObjectKind.Consumer, Guid> coborrowerId;
                    bool isCoborrowerDefined;
                    this.loanData.GetConsumersForLegacyApplication(association.LegacyApplicationId, out borrowerId, out coborrowerId, out isCoborrowerDefined);

                    int state = isCoborrowerDefined ? Joint : Borrower;
                    currentState[association.LegacyApplicationId] = state;
                    desiredState[association.LegacyApplicationId] = state;

                    borrowerToApp[borrowerId] = association.LegacyApplicationId;
                    if (isCoborrowerDefined)
                    {
                        coborrowerToApp[coborrowerId] = association.LegacyApplicationId;
                    }
                }
            }

            // Calculate what operations need be done based on which users are getting deleted.
            var uladAssocsToRemove = new HashSet<DataObjectIdentifier<DataObjectKind.UladApplicationConsumerAssociation, Guid>>();
            foreach (var keyPair in this.UladApplicationConsumers.Where(a => a.Value.UladApplicationId == uladApplicationId))
            {
                uladAssocsToRemove.Add(keyPair.Key);

                var association = keyPair.Value;
                if (borrowerToApp.ContainsKey(association.ConsumerId))
                {
                    desiredState[association.LegacyAppId] -= Borrower;
                    if (desiredState[association.LegacyAppId] < 0)
                    {
                        throw new CBaseException(ErrorMessages.Generic, "Attempting to remove invalid borrower, there is a database inconsistency for aAppId = " + association.LegacyAppId.ToString());
                    }
                }
                else if (coborrowerToApp.ContainsKey(association.ConsumerId))
                {
                    desiredState[association.LegacyAppId] -= Coborrower;
                    if (desiredState[association.LegacyAppId] < 0)
                    {
                        throw new CBaseException(ErrorMessages.Generic, "Attempting to remove invalid coborrower, there is a database inconsistency for aAppId = " + association.LegacyAppId.ToString());
                    }
                }
            }

            // Handle the swap borrower operations
            var listForSwap = new List<DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>>();
            listForSwap.AddRange(desiredState.Keys.Where(k => desiredState[k] == Coborrower));
            foreach (var appId in listForSwap)
            {
                // Just swap the borrowers for now and change the desired state.
                // The removal of the unwanted (now) coborrower will be done below.
                this.borrMgmt.BorrowerManagementOnly_SwapBorrowers(appId);
                desiredState[appId] = Borrower;
            }

            // Remove entities and ownership associations
            foreach (var association in this.UladApplicationConsumers.Values.Where(a => a.UladApplicationId == uladApplicationId))
            {
                this.RemoveAllOwnedEntities(association.ConsumerId);
            }

            // Remove the consumer - ulad application associations
            foreach (var id in uladAssocsToRemove)
            {
                this.UladApplicationConsumers.Remove(id);
            }

            // Remove the coborrowers
            foreach (var appId in desiredState.Keys.Where(k => desiredState[k] == Borrower && currentState[k] == Joint))
            {
                this.borrMgmt.BorrowerManagementOnly_RemoveCoborrowerFromLegacyApplication(appId);
            }

            // Remove the ULAD application
            var uladApp = this.UladApplications[uladApplicationId];
            this.UladApplications.Remove(uladApplicationId);
            if (uladApp.IsPrimary)
            {
                var newPrimary = this.UladApplications.Values.First();
                newPrimary.IsPrimary = true;
            }

            this.ClearOrdersForUladApplication(uladApplicationId);

            // Remove any collection order records for applications that will be deleted.
            foreach (var appId in desiredState.Keys.Where(k => desiredState[k] == Empty))
            {
                this.ClearOrdersForLegacyApplication(appId);
            }

            try
            {
                this.suppressRepairOnSave = true;
                this.borrMgmt.SaveAllChanges(); // the state of the loan is going to be Normal after this call.
            }
            finally
            {
                this.suppressRepairOnSave = false;
            }

            // Remove the legacy applications
            foreach (var appId in desiredState.Keys.Where(k => desiredState[k] == Empty))
            {
                this.borrMgmt.BorrowerManagementOnly_RemoveLegacyApplication(appId);
            }
        }

        /// <summary>
        /// Set the primary ULAD application for the loan.
        /// </summary>
        /// <param name="id">The identifier for the new primary ULAD application.</param>
        public void SetPrimaryUladApplication(DataObjectIdentifier<DataObjectKind.UladApplication, Guid> id)
        {
            var newPrimary = this.UladApplications[id];
            if (newPrimary == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "Cannot find ULAD application " + id.ToString());
            }

            if (newPrimary.IsPrimary)
            {
                return; // already primary
            }

            foreach (var ulad in this.UladApplications.Values.Where(u => u.IsPrimary))
            {
                ulad.IsPrimary = false;
            }

            newPrimary.IsPrimary = true;

            if (this.loanData.sSyncUladAndLegacyApps)
            {
                var assoc = this.UladApplicationConsumers.Where(a => a.Value.UladApplicationId == id).First().Value;
                this.borrMgmt.BorrowerManagementOnly_SetLegacyApplicationAsPrimary(assoc.LegacyAppId);
            }
        }

        /// <summary>
        /// Set the primary legacy application for the loan.
        /// </summary>
        /// <param name="appId">The identifier for the new primary legacy application.</param>
        public void SetPrimaryLegacyApplication(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId)
        {
            if (this.loanData.sSyncUladAndLegacyApps)
            {
                DataObjectIdentifier<DataObjectKind.Consumer, Guid> borrowerId;
                DataObjectIdentifier<DataObjectKind.Consumer, Guid> coborrowerId;
                bool isCoborrowerDefined;

                this.loanData.GetConsumersForLegacyApplication(appId, out borrowerId, out coborrowerId, out isCoborrowerDefined);
                var assoc = this.UladApplicationConsumers.Single(a => a.Value.ConsumerId == borrowerId).Value;
                this.SetPrimaryUladApplication(assoc.UladApplicationId);
            }
            else
            {
                this.borrMgmt.BorrowerManagementOnly_SetLegacyApplicationAsPrimary(appId);
            }
        }

        /// <summary>
        /// Add a new consumer as a coborrower to the specified ULAD application. Method should be called with
        /// loan file in InitLoad data state and returned with file in InitLoad data state. Upon method completion,
        /// InitLoad() will need to be called again to ensure the changes are available in the data layer.
        /// </summary>
        /// <param name="uladAppId">The ULAD application under which the consumer is added.</param>
        /// <returns>The identifier for the newly added consumer.</returns>
        public DataObjectIdentifier<DataObjectKind.Consumer, Guid> AddConsumerToUladApplication(
            DataObjectIdentifier<DataObjectKind.UladApplication, Guid> uladAppId)
        {
            if (!this.UladApplications.ContainsKey(uladAppId))
            {
                throw new CBaseException(
                    ErrorMessages.Generic,
                    "Unable to find ULAD application with id " + uladAppId.Value.ToString());
            }

            var uladApplication = this.UladApplications[uladAppId];
            var uladApplicationConsumers = this.UladApplicationConsumers.Values.Where(a => a.UladApplicationId == uladAppId);
            var numConsumersOnUladApp = uladApplicationConsumers.Count();

            if (numConsumersOnUladApp == 0)
            {
                throw new CBaseException(
                    ErrorMessages.Generic,
                    "Unable to find consumer associations for ULAD application with id " + uladAppId.Value.ToString());
            }
            else if (this.loanData.sSyncUladAndLegacyApps && numConsumersOnUladApp > 1)
            {
                var errorMessage = "Cannot add coborrower to ULAD application with id "
                    + uladAppId.Value.ToString()
                    + " because it already has 2 coborrowers and the file is syncing ULAD and Legacy apps.";
                throw new CBaseException(ErrorMessages.Generic, errorMessage);
            }

            if (numConsumersOnUladApp == 1)
            {
                var existingBorrowerConsumerId = uladApplicationConsumers
                    .Single()
                    .ConsumerId;
                var existingBorrowerLegacyAppId = this.loanData.LegacyApplicationConsumers.Values
                    .Single(a => a.ConsumerId == existingBorrowerConsumerId)
                    .LegacyApplicationId;
                var numConsumersOnLegacyApp = this.LegacyApplicationConsumers.Values
                    .Count(a => a.LegacyApplicationId == existingBorrowerLegacyAppId);

                if (numConsumersOnLegacyApp > 1 && this.loanData.sSyncUladAndLegacyApps)
                {
                    var errorMessage = "Cannot add coborrower to ULAD application with id "
                        + uladAppId.Value.ToString()
                        + " because the associated legacy app already has 2 coborrowers.";
                    throw new CBaseException(ErrorMessages.Generic, errorMessage);
                }
                else if (numConsumersOnLegacyApp > 1)
                {
                    return this.AddBlankLegacyAppAndUladAppConsumerAssociation(uladAppId);
                }

                DataObjectIdentifier<DataObjectKind.Consumer, Guid> coborrowerId;
                DataObjectIdentifier<DataObjectKind.UladApplicationConsumerAssociation, Guid> uladAppConsumerAssocId;
                this.collectionProvider.AddCoborrowerToLegacyAppAndUladApp(existingBorrowerLegacyAppId, uladAppId, out coborrowerId, out uladAppConsumerAssocId);
                return coborrowerId;
            }

            return this.AddBlankLegacyAppAndUladAppConsumerAssociation(uladAppId);
        }

        /// <summary>
        /// Remove a borrower record and relevant associations.
        /// </summary>
        /// <remarks>
        /// If an exception is thrown then changes to the collections won't be saved so we don't need to worry about undoing them.
        /// However, changes to the legacy application state are saved immediately so should only be done after exception throwing
        /// logic has been passed.
        /// </remarks>
        /// <param name="consumerId">The consumer id.</param>
        public void RemoveBorrower(DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId)
        {
            if (this.UladApplicationConsumers.Count == 1)
            {
                throw new CBaseException(ErrorMessages.Generic, "Cannot remove the only borrower from a loan.");
            }

            var consumerUladAppAssoc = this.UladApplicationConsumers.Single(a => a.Value.ConsumerId == consumerId);
            var legacyAppId = consumerUladAppAssoc.Value.LegacyAppId;

            DataObjectIdentifier<DataObjectKind.Consumer, Guid> borrowerId;
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> coborrowerId;
            bool isCoborrowerDefined;
            this.loanData.GetConsumersForLegacyApplication(legacyAppId, out borrowerId, out coborrowerId, out isCoborrowerDefined);

            bool needToRemoveLegacyApplication = false;
            if (!isCoborrowerDefined)
            {
                if (consumerId == coborrowerId)
                {
                    throw new CBaseException(ErrorMessages.Generic, "Attempting to remove a consumer that is an undefined coborrower for legacy application : " + legacyAppId.ToString());
                }

                if (consumerId != borrowerId)
                {
                    throw new CBaseException(ErrorMessages.Generic, "Inconsistent state - a (legacyAppId, consumerId) key in the UladApplicationConsumers collection doesn't match the legacy application state, association : " + consumerUladAppAssoc.Key.ToString());
                }

                needToRemoveLegacyApplication = true;
            }
            else if (consumerId == coborrowerId)
            {
                this.borrMgmt.BorrowerManagementOnly_RemoveCoborrowerFromLegacyApplication(legacyAppId);
            }
            else if (consumerId == borrowerId)
            {
                this.borrMgmt.BorrowerManagementOnly_SwapBorrowers(legacyAppId);
                this.borrMgmt.BorrowerManagementOnly_RemoveCoborrowerFromLegacyApplication(legacyAppId);
            }

            if (needToRemoveLegacyApplication)
            {
                this.ClearOrdersForLegacyApplication(legacyAppId);
            }

            this.RemoveConsumerFromUladDataLayer(consumerId);

            if (needToRemoveLegacyApplication)
            {
                try
                {
                    this.suppressRepairOnSave = true;
                    this.borrMgmt.SaveAllChanges(); // the state of the loan is going to be Normal after this call.
                }
                finally
                {
                    this.suppressRepairOnSave = false;
                }

                this.borrMgmt.BorrowerManagementOnly_RemoveLegacyApplication(legacyAppId);
            }
            else
            {
                this.borrMgmt.SaveAllChanges();
            }
        }

        /// <summary>
        /// Set the primary borrower for the ULAD application.
        /// </summary>
        /// <param name="consumerId">The identifier for the new primary borrower.</param>
        public void SetPrimaryBorrowerOnUladApplication(DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId)
        {
            var appAssociation = this.UladApplicationConsumers.Values.SingleOrDefault(a => a.ConsumerId == consumerId);
            if (appAssociation == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "Invalid consumer id provided (" + consumerId + ")");
            }

            if (appAssociation.IsPrimary)
            {
                return; // no-op; consumer is already the primary on their application
            }

            this.SetPrimaryBorrowerOnUladApplication(appAssociation.UladApplicationId, appAssociation.ConsumerId);
        }

        /// <summary>
        /// Set the primary borrower for the ULAD application.
        /// </summary>
        /// <param name="uladId">The identifier for the ULAD application.</param>
        /// <param name="consumerId">The identifier for the new primary borrower.</param>
        public void SetPrimaryBorrowerOnUladApplication(DataObjectIdentifier<DataObjectKind.UladApplication, Guid> uladId, DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId)
        {
            var appAssociations = this.UladApplicationConsumers.Values.Where(a => a.UladApplicationId == uladId);
            var currentPrimary = appAssociations.SingleOrDefault(a => a.IsPrimary);
            if (currentPrimary != null && currentPrimary.ConsumerId == consumerId)
            {
                return;
            }

            var newPrimary = appAssociations.SingleOrDefault(a => a.ConsumerId == consumerId);
            if (newPrimary == null)
            {
                throw new CBaseException(ErrorMessages.Generic, $"Consumer {consumerId.ToString()} is not associated with ULAD application {uladId.ToString()}");
            }

            DataObjectIdentifier<DataObjectKind.Consumer, Guid> borrowerId;
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> coborrowerId;
            bool isCoborrowerDefined;

            this.loanData.GetConsumersForLegacyApplication(newPrimary.LegacyAppId, out borrowerId, out coborrowerId, out isCoborrowerDefined);
            if (coborrowerId == consumerId)
            {
                if (!isCoborrowerDefined)
                {
                    this.borrMgmt.SetCoborrowerActiveFlag(newPrimary.LegacyAppId, true);
                }

                this.borrMgmt.BorrowerManagementOnly_SwapBorrowers(newPrimary.LegacyAppId);
            }
            else if (borrowerId != consumerId)
            {
                throw new CBaseException(ErrorMessages.Generic, $"Consumer {consumerId.ToString()} is not associated with ULAD application {uladId.ToString()}");
            }

            if (currentPrimary != null)
            {
                currentPrimary.IsPrimary = false;
            }

            newPrimary.IsPrimary = true;
        }

        /// <summary>
        /// Swap the borrower and coborrower for an application.
        /// </summary>
        /// <remarks>
        /// There are some hidden constraints:
        /// 1. Both borrower and coborrower should always be on the same ULAD application.
        /// 2. The primary borrower on the ULAD application must be the primary on it's legacy application, so it must be a borrower.
        /// 3. A ULAD application can contain multiple legacy borrowers, only one of which can be the primary borrower.
        /// </remarks>
        /// <param name="appId">The identifier for the legacy application.</param>
        public void SwapBorrowers(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId)
        {
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> borrowerId;
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> coborrowerId;
            bool coborrowerDefinedBeforeSwap;
            this.loanData.GetConsumersForLegacyApplication(appId, out borrowerId, out coborrowerId, out coborrowerDefinedBeforeSwap);

            if (!coborrowerDefinedBeforeSwap)
            {
                var uladAppId = this.UladApplicationConsumers
                    .Values
                    .Single(a => a.ConsumerId == borrowerId)
                    .UladApplicationId;

                this.CreateUladConsumerAssociation(uladAppId, coborrowerId, appId, false);
            }

            this.borrMgmt.BorrowerManagementOnly_SwapBorrowers(appId); // Note that this forces the new coborrower to be defined.

            var borrowerAssoc = this.UladApplicationConsumers.Single(a => a.Value.ConsumerId == borrowerId).Value;
            if (borrowerAssoc.IsPrimary)
            {
                var coborrowerAssoc = this.UladApplicationConsumers.Single(a => a.Value.ConsumerId == coborrowerId).Value;

                borrowerAssoc.IsPrimary = false;
                coborrowerAssoc.IsPrimary = true;
            }
        }

        /// <summary>
        /// Remove a coborrower from the perspective of a legacy application.
        /// </summary>
        /// <param name="appId">The legacy application id.</param>
        public void RemoveCoborrowerFromLegacyApplication(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId)
        {
            var coborrowerLegacyAppAssoc = this.loanData.LegacyApplicationConsumers.Values
                .SingleOrDefault(a => a.LegacyApplicationId == appId && !a.IsPrimary);
            if (coborrowerLegacyAppAssoc == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "Coborrower not defined for legacy application : " + appId.ToString());
            }

            var consumerId = coborrowerLegacyAppAssoc.ConsumerId;
            var ownedEntities = this.GetOwnedEntityTypes(consumerId);
            if (ownedEntities.Count() > 0)
            {
                string list = string.Join(", ", ownedEntities);
                throw new CBaseException(ErrorMessages.Generic, $"Coborrower is the exclusive owner of {list} items for legacy application : {appId.ToString()}");
            }

            this.borrMgmt.BorrowerManagementOnly_RemoveCoborrowerFromLegacyApplication(appId);
            this.RemoveConsumerFromUladDataLayer(consumerId);
        }

        /// <summary>
        /// Remove the legacy application, which automatically removes the borrowers.
        /// </summary>
        /// <param name="appId">The application id.</param>
        public void RemoveLegacyApplication(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId)
        {
            if (this.loanData.LegacyApplicationConsumers.Where(a => a.Value.LegacyApplicationId != appId).Count() == 0)
            {
                throw new CBaseException(ErrorMessages.Generic, "Attempting to delete the only legacy application for a loan.");
            }

            DataObjectIdentifier<DataObjectKind.Consumer, Guid> borrowerId;
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> coborrowerId;
            bool isCoborrowerDefined;
            this.loanData.GetConsumersForLegacyApplication(appId, out borrowerId, out coborrowerId, out isCoborrowerDefined);

            // Remove entities and ownership associations
            this.RemoveAllOwnedEntities(borrowerId);
            if (isCoborrowerDefined)
            {
                this.RemoveAllOwnedEntities(coborrowerId);
            }

            if (this.loanData.sUsingUladApplications)
            {
                // Remove the consumer - ulad application associations
                var assocBorrId = this.UladApplicationConsumers.Single(a => a.Value.ConsumerId == borrowerId).Key;
                this.UladApplicationConsumers.Remove(assocBorrId);
                this.collectionProvider.RemoveFromCollectionOrders(borrowerId);

                if (isCoborrowerDefined)
                {
                    var assocCoborrId = this.UladApplicationConsumers.Single(a => a.Value.ConsumerId == coborrowerId).Key;
                    this.UladApplicationConsumers.Remove(assocCoborrId);
                    this.collectionProvider.RemoveFromCollectionOrders(coborrowerId);
                }

                // Remove empty ULAD applications
                var emptyUladKeys = new HashSet<DataObjectIdentifier<DataObjectKind.UladApplication, Guid>>();
                foreach (var uladAppId in this.UladApplications.Keys)
                {
                    if (!this.UladApplicationConsumers.Any(a => a.Value.UladApplicationId == uladAppId))
                    {
                        emptyUladKeys.Add(uladAppId);
                    }
                }

                foreach (var uladAppId in emptyUladKeys)
                {
                    this.UladApplications.Remove(uladAppId);
                }

                // If primary consumer removed, reassign primary
                foreach (var uladAppId in this.UladApplications.Keys)
                {
                    if (!this.UladApplicationConsumers.Any(a => a.Value.UladApplicationId == uladAppId && a.Value.IsPrimary))
                    {
                        var newPrimary = this.UladApplicationConsumers.Where(a => a.Value.UladApplicationId == uladAppId).First().Value;
                        newPrimary.IsPrimary = true;
                    }
                }

                if (!this.UladApplications.Any(a => a.Value.IsPrimary))
                {
                    this.UladApplications.First().Value.IsPrimary = true; // This is very rough, but it at least ensures that we're picking one of the apps to be primary
                }

                foreach (var uladAppId in emptyUladKeys)
                {
                    this.ClearOrdersForUladApplication(uladAppId);
                }
            }

            // Remove any collection order records for application that will be deleted.
            this.ClearOrdersForConsumer(borrowerId);
            if (isCoborrowerDefined)
            {
                this.ClearOrdersForConsumer(coborrowerId);
            }

            this.ClearOrdersForLegacyApplication(appId);

            try
            {
                this.suppressRepairOnSave = true;
                this.borrMgmt.SaveAllChanges(); // the state of the loan is going to be Normal after this call.
            }
            finally
            {
                this.suppressRepairOnSave = false;
            }

            // Remove the legacy application
            this.borrMgmt.BorrowerManagementOnly_RemoveLegacyApplication(appId);
        }

        /// <summary>
        /// Adds a co-borrower to an existing legacy application.
        /// </summary>
        /// <param name="appId">The application id.</param>
        /// <returns>The consumer id of the created co-borrower.</returns>
        public DataObjectIdentifier<DataObjectKind.Consumer, Guid> AddCoborrowerToLegacyApplication(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId)
        {
            // The coborrower fields already exist in the APPLICATION_A table.
            // The GUI will call this method, then bring the user to a screen to 
            // edit the coborrower data.  All this method has to do is deal with
            // making the correct association with the ULAD application.
            // From the spec:
            // If the user specifies that the new Borrower should go on an existing Legacy Application,
            // the new Borrower should default to be assigned to the ULAD Application
            // that the Legacy Application Borrower is assigned to, regardless of the number of Borrowers
            // assigned to that ULAD Application.
            var legacyAppConsumers = this.loanData.LegacyApplicationConsumers.Values.Where(a => a.LegacyApplicationId == appId);
            if (!legacyAppConsumers.Any())
            {
                throw new CBaseException(ErrorMessages.Generic, "Failed to find an existing borrower association for legacy application " + appId.ToString());
            }
            else if (legacyAppConsumers.Count() > 1)
            {
                throw new CBaseException(ErrorMessages.Generic, "Cannot add coborrower to legacy application " + appId.ToString());
            }

            var legacyAppConsumer = legacyAppConsumers.First();

            DataObjectIdentifier<DataObjectKind.Consumer, Guid> loanBorrowerId;
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> loanCoborrowerId;
            bool isCoborrowerDefined;
            this.loanData.GetConsumersForLegacyApplication(appId, out loanBorrowerId, out loanCoborrowerId, out isCoborrowerDefined);
            if (legacyAppConsumer.ConsumerId != loanBorrowerId)
            {
                throw new CBaseException(ErrorMessages.Generic, "Mismatch state for borrower in legacy application " + appId.ToString());
            }
            else if (isCoborrowerDefined)
            {
                throw new CBaseException(ErrorMessages.Generic, "Cannot add coborrower because coborrower defined flag already set for legacy application " + appId.ToString());
            }

            var existingUladAssociations = this.UladApplicationConsumers.Where(a => a.Value.ConsumerId == legacyAppConsumer.ConsumerId);
            if (!existingUladAssociations.Any())
            {
                throw new CBaseException(ErrorMessages.Generic, "Failed to find an existing ulad association for the borrower " + legacyAppConsumer.ConsumerId.ToString());
            }

            var existingUladAssociation = existingUladAssociations.First().Value;
            this.CreateUladConsumerAssociation(existingUladAssociation.UladApplicationId, loanCoborrowerId, legacyAppConsumer.LegacyApplicationId, false);

            this.borrMgmt.SetCoborrowerActiveFlag(appId, true);
            return loanCoborrowerId;
        }

        /// <summary>
        /// Add a legacy application to a loan.
        /// </summary>
        /// <returns>The identifier for the new legacy application.</returns>
        public DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> AddBlankLegacyApplication()
        {
            // This is functionally equivalent to adding a blank ULAD application.
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> legacyAppId;
            KeyValuePair<DataObjectIdentifier<DataObjectKind.UladApplication, Guid>, UladApplication> uladData;
            this.AddBlankLegacyAndUladApplication(out legacyAppId, out uladData);

            return legacyAppId;
        }

        /// <summary>
        /// Clear all the primary income sources associated with the given consumer.
        /// </summary>
        /// <param name="consumerId">The consumer id whose income sources should be deleted.</param>
        public void ClearPrimaryIncomeSourcesForConsumer(DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId)
        {
            var primaryIncomeSources = new HashSet<DataObjectIdentifier<DataObjectKind.IncomeSource, Guid>>();
            foreach (var association in this.ConsumerIncomeSources)
            {
                if (association.Value.IsPrimary && consumerId == association.Value.ConsumerId)
                {
                    primaryIncomeSources.Add(association.Value.OwnedEntityId);
                }
            }

            foreach (DataObjectIdentifier<DataObjectKind.IncomeSource, Guid> incomeSource in primaryIncomeSources)
            {
                this.Remove(incomeSource);
            }
        }

        /// <summary>
        /// Gets the identifier for the legacy application the specified consumer belongs to.
        /// </summary>
        /// <param name="consumerId">The identifier of the consumer to look up.</param>
        /// <returns>The identifier of the legacy application to which the consumer is a member.</returns>
        private DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> GetLegacyApplicationIdentifier(DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId)
        {
            return this.LegacyApplicationConsumers.Values.SingleOrDefault(c => c.ConsumerId == consumerId)?.LegacyApplicationId
                ?? this.loanData.LegacyApplicationInvalidConsumers.Single(c => c.ConsumerId == consumerId).LegacyApplicationId;
        }

        /// <summary>
        /// Gets the consumer ordering at the level of the ULAD application.
        /// </summary>
        /// <param name="uladAppId">The ULAD application identifier.</param>
        /// <returns>The consumer ordering at the level of the ULAD application.</returns>
        private IMutableOrderedIdentifierCollection<DataObjectKind.Consumer, Guid> GetConsumerOrderingForUladApplicationWithoutRepair(DataObjectIdentifier<DataObjectKind.UladApplication, Guid> uladAppId)
        {
            return this.collectionProvider.RetrieveCollectionOrder<DataObjectKind.Consumer>(legacyApplicationId: null, consumerId: null, uladApplicationId: uladAppId);
        }

        /// <summary>
        /// Create an association between a ULAD application and a consumer.
        /// </summary>
        /// <param name="uladAppId">The identifier for the ULAD application.</param>
        /// <param name="consumerId">The identifier for the consumer.</param>
        /// <param name="legacyAppId">The identifier for the legacy application that contains the consumer.</param>
        /// <param name="isPrimary">Ture if the association is primary, false otherwise.</param>
        /// <returns>The identifier for the created association.</returns>
        private DataObjectIdentifier<DataObjectKind.UladApplicationConsumerAssociation, Guid> CreateUladConsumerAssociation(
            DataObjectIdentifier<DataObjectKind.UladApplication, Guid> uladAppId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId,
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> legacyAppId,
            bool isPrimary)
        {
            var uladAppConsumer = new UladApplicationConsumerAssociation(uladAppId, consumerId, legacyAppId);
            uladAppConsumer.IsPrimary = isPrimary;

            var assocId = this.UladApplicationConsumers.Add(uladAppConsumer);

            var orders = this.GetConsumerOrderingForUladApplicationWithoutRepair(uladAppId);
            if (!orders.Contains(consumerId))
            {
                orders.Add(consumerId);
            }

            return assocId;
        }

        /// <summary>
        /// Called by the Save method, this code enforces logical constraints that
        /// we cannot rely on the database to maintain.
        /// </summary>
        /// <remarks>
        /// Normally our code should preserve all the constraints, however if there
        /// are bugs somewhere this method can provide a final backstop.  What we want
        /// to do is make repairs that can be done automatically without user input.
        /// For constraints that require user input we will throw exceptions in the
        /// ValidateForSave method.
        /// </remarks>
        private void EnsureLogicalConstraintsSatisfied()
        {
            if (this.loanData.sUsingUladApplications)
            {
                this.RepairUladConsumerAssociations();
                this.RepairLoanPrimaryUladApp();
                this.RepairOtherPrimaryConsumers();
            }
        }

        /// <summary>
        /// Utility method to log whenever a repair operation is done, so that
        /// we can investigate later.
        /// </summary>
        /// <remarks>
        /// This will only work in the context of a unit testing application, and can be
        /// disabled permanently by modifying the value of AllowLogging.
        /// </remarks>
        private void LogRepairCall()
        {
            const bool AllowLogging = false; // turn on only when we wish to discover which tests are triggering repair work
            const string IgnoreTestClassFile = "EnsureLogicalConstraintsTest.cs";
            const string Path = @"c:\temp\LqbCollectionsRepairLog.txt";

            if (AllowLogging && LqbGrammar.LqbApplication.Current.Type == LqbGrammar.ApplicationType.UnitTest)
            {
                var stack = new System.Diagnostics.StackTrace(1, true);
                string logEntry = stack.ToString() + "\r\n--------\r\n";
                if (!logEntry.Contains(IgnoreTestClassFile))
                {
                    System.IO.File.AppendAllText(Path, logEntry);
                }
            }
        }

        /// <summary>
        /// If there are active consumers for which there are no associations to ULAD applications,
        /// add those associations.  Also, remove associations for consumers that are
        /// no longer attached to legacy applications.  Finally, set IsPrimary when we can.
        /// </summary>
        /// <remarks>
        /// The primary consumer for a ULAD application must be the borrower on the legacy application.  The
        /// association for a coborrower may have its IsPrimary value set to true when a swap borrowers event
        /// occurred, possibly followed by inactivation of the coborrower.  We may set IsPrimary to true for
        /// a borrower if there are no primary borrowers.  Which borrower to select depends on whether any of the 
        /// coborrowers are set as primary.  Active coborrowers, if any, take precedence followed by inactive coborrowers.
        /// We'll take the first borrower we find according to these precedence rules and set IsPrimary to true.
        /// When this method returns, all coborrowers will have IsActive set to false and at least one borrower will 
        /// have IsActive set to true.  There is the possibility that on method entry more than one borrower had IsPrimary 
        /// set to true, in which case they will be unaffected.
        /// </remarks>
        private void RepairUladConsumerAssociations()
        {
            // Remove ULAD applications that aren't associated with any of the active borrowers and associations with inactive borrowers
            var emptyUladAppIds = new HashSet<DataObjectIdentifier<DataObjectKind.UladApplication, Guid>>();
            var inactiveConsumerMap = new Dictionary<DataObjectIdentifier<DataObjectKind.UladApplication, Guid>, HashSet<DataObjectIdentifier<DataObjectKind.Consumer, Guid>>>();

            var listActiveConsumers = new List<DataObjectIdentifier<DataObjectKind.Consumer, Guid>>();
            listActiveConsumers.AddRange(this.loanData.ActiveConsumerIdentifiers);

            foreach (var uladAppId in this.UladApplications.Keys)
            {
                bool isEmpty = true;
                foreach (var consumerId in listActiveConsumers)
                {
                    if (this.UladApplicationConsumers.Values.Any(a => a.UladApplicationId == uladAppId && a.ConsumerId == consumerId))
                    {
                        isEmpty = false;
                        break;
                    }
                }

                if (isEmpty)
                {
                    emptyUladAppIds.Add(uladAppId);
                }

                foreach (var associatedConsumerId in this.UladApplicationConsumers.Values.Where(a => a.UladApplicationId == uladAppId).Select(a => a.ConsumerId))
                {
                    if (!listActiveConsumers.Contains(associatedConsumerId))
                    {
                        if (!inactiveConsumerMap.ContainsKey(uladAppId))
                        {
                            inactiveConsumerMap[uladAppId] = new HashSet<DataObjectIdentifier<DataObjectKind.Consumer, Guid>>();
                        }

                        inactiveConsumerMap[uladAppId].Add(associatedConsumerId);
                    }
                }
            }

            foreach (var uladAppId in inactiveConsumerMap.Keys)
            {
                var order = this.GetConsumerOrderingForUladApplicationWithoutRepair(uladAppId);
                foreach (var inactiveConsumerId in inactiveConsumerMap[uladAppId])
                {
                    var assocKey = this.UladApplicationConsumers.Single(a => a.Value.UladApplicationId == uladAppId && a.Value.ConsumerId == inactiveConsumerId).Key;
                    this.UladApplicationConsumers.Remove(assocKey);
                    if (order.Contains(inactiveConsumerId))
                    {
                        order.Remove(inactiveConsumerId);
                    }
                }
            }

            foreach (var emptyUladAppId in emptyUladAppIds)
            {
                var order = this.GetConsumerOrderingForUladApplicationWithoutRepair(emptyUladAppId);
                order.Clear();

                this.UladApplications.Remove(emptyUladAppId);
            }

            // Initialize counts of primary consumers for each existing ULAD application.
            var primaryCountsMap = new Dictionary<DataObjectIdentifier<DataObjectKind.UladApplication, Guid>, PrimaryCounts>();
            foreach (var uladAppId in this.UladApplications.Keys)
            {
                primaryCountsMap[uladAppId] = new PrimaryCounts();
            }

            // Calculate the counts of primary consumers for each existing ULAD application.
            foreach (var legacyAppId in this.LegacyApplications.Keys)
            {
                DataObjectIdentifier<DataObjectKind.Consumer, Guid> borrowerId;
                DataObjectIdentifier<DataObjectKind.Consumer, Guid> coborrowerId;
                bool isCoborrowerDefined;
                this.loanData.GetConsumersForLegacyApplication(legacyAppId, out borrowerId, out coborrowerId, out isCoborrowerDefined);
                var borrowerAssociation = this.UladApplicationConsumers.Values.SingleOrDefault(a => a.ConsumerId == borrowerId);
                var coborrowerAssociation = this.UladApplicationConsumers.Values.SingleOrDefault(a => a.ConsumerId == coborrowerId);

                if (borrowerAssociation != null && borrowerAssociation.LegacyAppId != legacyAppId)
                {
                    throw new CBaseException(ErrorMessages.Generic, "Borrower association has incorrect legacy identifier.");
                }

                if (coborrowerAssociation != null && coborrowerAssociation.LegacyAppId != legacyAppId)
                {
                    throw new CBaseException(ErrorMessages.Generic, "Coborrower association has incorrect legacy identifier.");
                }

                if (isCoborrowerDefined && borrowerAssociation != null && coborrowerAssociation != null && borrowerAssociation.UladApplicationId != coborrowerAssociation.UladApplicationId)
                {
                    throw new CBaseException(ErrorMessages.Generic, "Borrower and coborrower associated with different ULAD applications.");
                }

                if (borrowerAssociation != null && borrowerAssociation.IsPrimary)
                {
                    primaryCountsMap[borrowerAssociation.UladApplicationId].Borrowers++;
                }

                if (coborrowerAssociation != null && coborrowerAssociation.IsPrimary)
                {
                    if (isCoborrowerDefined)
                    {
                        primaryCountsMap[borrowerAssociation.UladApplicationId].ActiveCoborrowers++;
                    }
                    else
                    {
                        primaryCountsMap[borrowerAssociation.UladApplicationId].InactiveCoborrowers++;
                    }
                }
            }

            // Bring the ULAD application structure in conformance with the legacy applications and borrowers
            foreach (var legacyAppId in this.LegacyApplications.Keys)
            {
                DataObjectIdentifier<DataObjectKind.Consumer, Guid> borrowerId;
                DataObjectIdentifier<DataObjectKind.Consumer, Guid> coborrowerId;
                bool isCoborrowerDefined;
                this.loanData.GetConsumersForLegacyApplication(legacyAppId, out borrowerId, out coborrowerId, out isCoborrowerDefined);
                var borrowerAssociation = this.UladApplicationConsumers.Values.SingleOrDefault(a => a.ConsumerId == borrowerId);
                var coborrowerAssociation = this.UladApplicationConsumers.Values.SingleOrDefault(a => a.ConsumerId == coborrowerId);

                // Although the code can be compactified, I'm leaving each scenario as a stand-alone block
                // of code so that it is easier to understand and verify the correctness.
                if (borrowerAssociation == null && coborrowerAssociation == null && !isCoborrowerDefined)
                {
                    // Create a new ULAD application for the borrower
                    this.LogRepairCall();
                    var newUladApplication = new UladApplication();
                    var newUladAppId = this.UladApplications.Add(newUladApplication);

                    this.CreateUladConsumerAssociation(newUladAppId, borrowerId, legacyAppId, true);
                }
                else if (borrowerAssociation == null && coborrowerAssociation == null && isCoborrowerDefined)
                {
                    // Create a new ULAD application for both borrower and coborrower
                    // NOTE: Should never actually get here because the spurious coborrower association gets deleted above because the coborrower is not active
                    this.LogRepairCall();
                    var newUladApplication = new UladApplication();
                    var newUladAppId = this.UladApplications.Add(newUladApplication);

                    this.CreateUladConsumerAssociation(newUladAppId, borrowerId, legacyAppId, true);
                    this.CreateUladConsumerAssociation(newUladAppId, coborrowerId, legacyAppId, false);
                }
                else if (borrowerAssociation == null && coborrowerAssociation != null && !isCoborrowerDefined)
                {
                    // Assign the borrower to the ULAD application for the coborrower and then remove the coborrower
                    this.LogRepairCall();
                    var newAssociation = new UladApplicationConsumerAssociation(coborrowerAssociation.UladApplicationId, borrowerId, legacyAppId);
                    newAssociation.IsPrimary = false;

                    var primaryCounts = primaryCountsMap[coborrowerAssociation.UladApplicationId];
                    bool setPrimary = primaryCounts.AllZero || (primaryCounts.Borrowers == 0 && primaryCounts.ActiveCoborrowers == 0 && coborrowerAssociation.IsPrimary);
                    if (setPrimary)
                    {
                        this.LogRepairCall();
                        newAssociation.IsPrimary = true;
                        primaryCounts.Borrowers = 1;
                    }

                    if (coborrowerAssociation.IsPrimary)
                    {
                        this.LogRepairCall();
                        primaryCountsMap[coborrowerAssociation.UladApplicationId].InactiveCoborrowers--;
                    }

                    this.UladApplicationConsumers.Add(newAssociation);

                    var coborrowerAssocId = this.UladApplicationConsumers.Single(a => a.Value.ConsumerId == coborrowerId).Key;
                    this.UladApplicationConsumers.Remove(coborrowerAssocId);

                    this.RepairConsumerOrder(coborrowerAssociation.UladApplicationId, borrowerId, coborrowerId, isCoborrowerDefined);
                }
                else if (borrowerAssociation == null && coborrowerAssociation != null && isCoborrowerDefined)
                {
                    // Add borrower to same ULAD application as coborrower
                    this.LogRepairCall();
                    var newAssociation = new UladApplicationConsumerAssociation(coborrowerAssociation.UladApplicationId, borrowerId, legacyAppId);
                    newAssociation.IsPrimary = false;

                    var primaryCounts = primaryCountsMap[coborrowerAssociation.UladApplicationId];
                    bool setPrimary = primaryCounts.AllZero || (primaryCounts.Borrowers == 0 && primaryCounts.ActiveCoborrowers > 0 && coborrowerAssociation.IsPrimary);
                    if (setPrimary)
                    {
                        this.LogRepairCall();
                        newAssociation.IsPrimary = true;
                        primaryCountsMap[coborrowerAssociation.UladApplicationId].Borrowers = 1;
                    }

                    if (coborrowerAssociation.IsPrimary)
                    {
                        this.LogRepairCall();
                        coborrowerAssociation.IsPrimary = false;
                        primaryCountsMap[coborrowerAssociation.UladApplicationId].ActiveCoborrowers--;
                    }

                    this.UladApplicationConsumers.Add(newAssociation);

                    this.RepairConsumerOrder(coborrowerAssociation.UladApplicationId, borrowerId, coborrowerId, isCoborrowerDefined);
                }
                else if (borrowerAssociation != null && coborrowerAssociation == null && !isCoborrowerDefined)
                {
                    // Valid state for borrowers, just make sure order and primary association are set correctly
                    var primaryCounts = primaryCountsMap[borrowerAssociation.UladApplicationId];
                    bool setPrimary = primaryCounts.AllZero;
                    if (setPrimary)
                    {
                        this.LogRepairCall();
                        borrowerAssociation.IsPrimary = true;
                        primaryCountsMap[borrowerAssociation.UladApplicationId].Borrowers = 1;
                    }

                    this.RepairConsumerOrder(borrowerAssociation.UladApplicationId, borrowerId, coborrowerId, isCoborrowerDefined);
                }
                else if (borrowerAssociation != null && coborrowerAssociation == null && isCoborrowerDefined)
                {
                    // Add coborrower to same ULAD application as borrower
                    this.LogRepairCall();
                    var newAssociation = new UladApplicationConsumerAssociation(borrowerAssociation.UladApplicationId, coborrowerId, legacyAppId);
                    newAssociation.IsPrimary = false;
                    this.UladApplicationConsumers.Add(newAssociation);

                    var primaryCounts = primaryCountsMap[borrowerAssociation.UladApplicationId];
                    bool setPrimary = primaryCounts.AllZero;
                    if (setPrimary)
                    {
                        this.LogRepairCall();
                        borrowerAssociation.IsPrimary = true;
                        primaryCountsMap[borrowerAssociation.UladApplicationId].Borrowers = 1;
                    }

                    this.RepairConsumerOrder(borrowerAssociation.UladApplicationId, borrowerId, coborrowerId, isCoborrowerDefined);
                }
                else if (borrowerAssociation != null && coborrowerAssociation != null && !isCoborrowerDefined)
                {
                    // Remove the coborrower association
                    // NOTE: Should never actually get here because the spurious coborrower association gets deleted above because the coborrower is not active
                    this.LogRepairCall();
                    var coborrowerAssocId = this.UladApplicationConsumers.Single(a => a.Value.ConsumerId == coborrowerId).Key;
                    this.UladApplicationConsumers.Remove(coborrowerAssocId);

                    var primaryCounts = primaryCountsMap[borrowerAssociation.UladApplicationId];
                    bool setPrimary = primaryCounts.AllZero || (primaryCounts.Borrowers == 0 && primaryCounts.ActiveCoborrowers == 0 && coborrowerAssociation.IsPrimary);
                    if (setPrimary)
                    {
                        this.LogRepairCall();
                        borrowerAssociation.IsPrimary = true;
                        primaryCountsMap[borrowerAssociation.UladApplicationId].Borrowers = 1;
                    }

                    if (coborrowerAssociation.IsPrimary)
                    {
                        this.LogRepairCall();
                        primaryCountsMap[borrowerAssociation.UladApplicationId].InactiveCoborrowers--;
                    }

                    this.RepairConsumerOrder(borrowerAssociation.UladApplicationId, borrowerId, coborrowerId, isCoborrowerDefined);
                }
                else if (borrowerAssociation != null && coborrowerAssociation != null && isCoborrowerDefined)
                {
                    // Valid state for borrowers, just make sure order and primary association are set correctly
                    var primaryCounts = primaryCountsMap[borrowerAssociation.UladApplicationId];
                    bool setPrimary = primaryCounts.AllZero || (primaryCounts.Borrowers == 0 && primaryCounts.ActiveCoborrowers > 0 && coborrowerAssociation.IsPrimary);
                    if (setPrimary)
                    {
                        this.LogRepairCall();
                        borrowerAssociation.IsPrimary = true;
                        primaryCountsMap[borrowerAssociation.UladApplicationId].Borrowers = 1;
                    }

                    if (coborrowerAssociation.IsPrimary)
                    {
                        this.LogRepairCall();
                        coborrowerAssociation.IsPrimary = false;
                        primaryCountsMap[coborrowerAssociation.UladApplicationId].ActiveCoborrowers--;
                    }

                    this.RepairConsumerOrder(borrowerAssociation.UladApplicationId, borrowerId, coborrowerId, isCoborrowerDefined);
                }
            }
        }

        /// <summary>
        /// Make sure the consumer order for the ULAD application is correct for the borrower and coborrower.
        /// </summary>
        /// <param name="uladAppId">The identifier for the ULAD application.</param>
        /// <param name="borrowerId">The identifier for the borrower.</param>
        /// <param name="coborrowerId">The identifier for the coborrower.</param>
        /// <param name="isCoborrowerDefined">If true the coborrower should be in the order, if false then the coborrower should not be in the order.</param>
        private void RepairConsumerOrder(
            DataObjectIdentifier<DataObjectKind.UladApplication, Guid> uladAppId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> borrowerId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> coborrowerId,
            bool isCoborrowerDefined)
        {
            var order = this.GetConsumerOrderingForUladApplicationWithoutRepair(uladAppId);
            if (!order.Contains(borrowerId))
            {
                this.LogRepairCall();
                order.Add(borrowerId);
            }

            if (isCoborrowerDefined)
            {
                if (!order.Contains(coborrowerId))
                {
                    this.LogRepairCall();
                    order.Add(coborrowerId);
                }
            }
            else
            {
                if (order.Contains(coborrowerId))
                {
                    this.LogRepairCall();
                    order.Remove(coborrowerId);
                }
            }
        }

        /// <summary>
        /// The most important thing to get correct is the primary ULAD application
        /// for the loan.  We will locate this first and make sure it is correct.
        /// </summary>
        private void RepairLoanPrimaryUladApp()
        {
            var primaryBorrowerId = this.FindLoanPrimaryBorrower();
            if (primaryBorrowerId == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "Unable to locate the primary borrower for the loan.");
            }

            var primaryAssoc = this.UladApplicationConsumers.Values.Single(a => a.ConsumerId == primaryBorrowerId.Value);
            if (this.UladApplicationConsumers.Values.Count(a => a.UladApplicationId == primaryAssoc.UladApplicationId && a.IsPrimary) != 1)
            {
                this.LogRepairCall();
            }

            foreach (var uladAppKeyValuePair in this.UladApplications)
            {
                if (uladAppKeyValuePair.Key == primaryAssoc.UladApplicationId)
                {
                    uladAppKeyValuePair.Value.IsPrimary = true;
                    foreach (var consumerAssoc in this.UladApplicationConsumers.Values.Where(a => a.UladApplicationId == uladAppKeyValuePair.Key))
                    {
                        if (consumerAssoc.ConsumerId == primaryAssoc.ConsumerId)
                        {
                            consumerAssoc.IsPrimary = true;
                        }
                        else
                        {
                            consumerAssoc.IsPrimary = false;
                        }
                    }
                }
                else
                {
                    uladAppKeyValuePair.Value.IsPrimary = false;
                }
            }
        }

        /// <summary>
        /// Every ULAD application should have exactly one primary consumer.  When this method is called
        /// the number of primary borrowers will be one or more.  This method must pare that down to one.
        /// </summary>
        private void RepairOtherPrimaryConsumers()
        {
            foreach (var uladAppId in this.UladApplications.Keys)
            {
                if (this.UladApplicationConsumers.Values.Count(a => a.UladApplicationId == uladAppId && a.IsPrimary) > 1)
                {
                    this.LogRepairCall();
                    var firstPrimaryAssocId = this.UladApplicationConsumers.First(a => a.Value.UladApplicationId == uladAppId && a.Value.IsPrimary).Key;
                    foreach (var assocKeyValuePair in this.UladApplicationConsumers.Where(a => a.Value.UladApplicationId == uladAppId))
                    {
                        if (assocKeyValuePair.Key == firstPrimaryAssocId)
                        {
                            assocKeyValuePair.Value.IsPrimary = true;
                        }
                        else
                        {
                            assocKeyValuePair.Value.IsPrimary = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Locate the primary borrower for the loan, first via its definition, then via a series of
        /// increasingly broad searches for a good candidate.
        /// </summary>
        /// <returns>The identifier that is determined to be the primary borrower for a loan.</returns>
        private DataObjectIdentifier<DataObjectKind.Consumer, Guid>? FindLoanPrimaryBorrower()
        {
            // If there is exactly one primary ulad application that has exactly one primary borrower, return that borrower.
            if (this.UladApplications.Count(a => a.Value.IsPrimary) == 1)
            {
                var primaryUladAppId = this.UladApplications.Single(u => u.Value.IsPrimary).Key;
                if (this.UladApplicationConsumers.Values.Count(a => a.UladApplicationId == primaryUladAppId && a.IsPrimary) == 1)
                {
                    var primaryAssoc = this.UladApplicationConsumers.Values.Single(a => a.UladApplicationId == primaryUladAppId && a.IsPrimary);
                    return primaryAssoc.ConsumerId;
                }
            }

            // If there is exactly one primary legacy application that has exactly one primary borrower, return that borrower.
            if (this.LegacyApplications.Count(a => a.Value.IsPrimary) == 1)
            {
                var primaryLegacyAppId = this.LegacyApplications.Single(u => u.Value.IsPrimary).Key;
                if (this.LegacyApplicationConsumers.Values.Count(a => a.LegacyApplicationId == primaryLegacyAppId && a.IsPrimary) == 1)
                {
                    this.LogRepairCall();
                    var primaryAssoc = this.LegacyApplicationConsumers.Values.Single(a => a.LegacyApplicationId == primaryLegacyAppId && a.IsPrimary);
                    return primaryAssoc.ConsumerId;
                }
            }

            // If there is exactly one primary borrower for any legacy application, return that borrower.  The first legacy application should have precedence.
            foreach (var legacyAppId in this.LegacyApplications.Keys)
            {
                if (this.LegacyApplicationConsumers.Values.Count(a => a.LegacyApplicationId == legacyAppId && a.IsPrimary) == 1)
                {
                    this.LogRepairCall();
                    var primaryAssoc = this.LegacyApplicationConsumers.Values.Single(a => a.LegacyApplicationId == legacyAppId && a.IsPrimary);
                    return primaryAssoc.ConsumerId;
                }
            }

            // If the first legacy application has a borrower, return that borrower
            var firstLegacyAppId = this.LegacyApplications.First().Key;
            var firstLegacyAppAssoc = this.LegacyApplicationConsumers.Values.FirstOrDefault(a => a.LegacyApplicationId == firstLegacyAppId);
            if (firstLegacyAppAssoc != null)
            {
                this.LogRepairCall();
                return firstLegacyAppAssoc.ConsumerId;
            }

            // Return the primary borrower for the first legacy application that has one
            var legacyAppAssoc = this.LegacyApplicationConsumers.Values.FirstOrDefault(a => a.IsPrimary);
            if (legacyAppAssoc != null)
            {
                this.LogRepairCall();
                return legacyAppAssoc.ConsumerId;
            }

            // Return the first borrower of the first legacy application
            legacyAppAssoc = this.LegacyApplicationConsumers.Values.FirstOrDefault();
            if (legacyAppAssoc != null)
            {
                this.LogRepairCall();
                return legacyAppAssoc.ConsumerId;
            }

            return null;
        }

        /// <summary>
        /// Removes the consumer from the ULAD data layer. Does not affect legacy applications.
        /// </summary>
        /// <remarks>
        /// Removes the consumer from a ULAD app, removes the ULAD app if it no longer has borrowers,
        /// removes any ownership associations or entities that were only owned by the given consumer,
        /// and removes collection orders associated with the consumer.
        /// </remarks>
        /// <param name="consumerId">The consumer id.</param>
        private void RemoveConsumerFromUladDataLayer(DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId)
        {
            var assocKeyValuePair = this.UladApplicationConsumers.Single(a => a.Value.ConsumerId == consumerId);
            this.UladApplicationConsumers.Remove(assocKeyValuePair.Key);

            this.RemoveAllOwnedEntities(consumerId);
            this.ClearOrdersForConsumer(consumerId);

            if (!this.UladApplicationConsumers.Any(a => a.Value.UladApplicationId == assocKeyValuePair.Value.UladApplicationId))
            {
                this.UladApplications.Remove(assocKeyValuePair.Value.UladApplicationId);
            }
            else if (assocKeyValuePair.Value.IsPrimary)
            {
                this.UladApplicationConsumers.First(a => a.Value.UladApplicationId == assocKeyValuePair.Value.UladApplicationId).Value.IsPrimary = true;
            }

            var order = this.GetConsumerOrderingForUladApplicationWithoutRepair(assocKeyValuePair.Value.UladApplicationId);
            order.Remove(consumerId);

            if (!this.UladApplications.Values.Any(app => app.IsPrimary))
            {
                // if we're in this case, we've deleted our primary ULAD application, which should mean we're also going to delete the
                // primary legacy application.  One of the challenges here is that due to the design of the legacy app stuff, it won't be
                // removed until later in the process.
                var primaryLegacyAppId = this.LegacyApplications.Single(app => app.Value.IsPrimary).Key;
                var remainingPrimaryLegacyAppConsumers = this.LegacyApplicationConsumers.Values.Where(c => c.LegacyApplicationId == primaryLegacyAppId && c.ConsumerId != consumerId);
                if (remainingPrimaryLegacyAppConsumers.Any())
                {
                    var chosenLegacyAppConsumerId = remainingPrimaryLegacyAppConsumers.OrderByDescending(a => a.IsPrimary).First().ConsumerId;
                    var chosenUladAppId = this.UladApplicationConsumers.Values.Single(c => c.ConsumerId == chosenLegacyAppConsumerId).UladApplicationId;
                    this.UladApplications[chosenUladAppId].IsPrimary = true;
                }
                else
                {
                    // our primary legacy app only has the consumer being removed from the ULAD data layer, so we expect them to be
                    // removed from the legacy application as well.  Thus, we just pick an arbitrary ULAD app to make primary
                    this.UladApplications.First().Value.IsPrimary = true;
                }
            }
        }

        /// <summary>
        /// Add both a legacy application and matching ULAD application.
        /// </summary>
        /// <param name="legacyAppId">The identifier for the new legacy application.</param>
        /// <param name="uladData">The new UladApplication instance and its identifier.</param>
        private void AddBlankLegacyAndUladApplication(
            out DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> legacyAppId,
            out KeyValuePair<DataObjectIdentifier<DataObjectKind.UladApplication, Guid>, UladApplication> uladData)
        {
            this.collectionProvider.AddBlankLegacyAndUladApplication(this.borrMgmt, out legacyAppId, out uladData);
        }

        /// <summary>
        /// Creates an ownership association, optionally marked as primary.
        /// </summary>
        /// <typeparam name="TOwnedKind">The data object kind of the owned entity.</typeparam>
        /// <typeparam name="TAssociationValue">The type of the association.</typeparam>
        /// <param name="appId">The application id.</param>
        /// <param name="consumerId">The consumer id.</param>
        /// <param name="recordId">The record id.</param>
        /// <param name="isPrimary">Indicates whether the created association should be marked as primary.</param>
        /// <param name="associationFactory">Factory method for creating the appropriate association.</param>
        /// <returns>The new association.</returns>
        private TAssociationValue CreateAssociation<TOwnedKind, TAssociationValue>(
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId,
            DataObjectIdentifier<TOwnedKind, Guid> recordId,
            bool isPrimary,
            Func<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, DataObjectIdentifier<TOwnedKind, Guid>, DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>, TAssociationValue> associationFactory)
                where TOwnedKind : DataObjectKind
                where TAssociationValue : IPrimaryOwnershipAssociation<TOwnedKind, Guid>
        {
            var association = associationFactory(consumerId, recordId, appId);

            association.IsPrimary = isPrimary;

            return association;
        }

        /// <summary>
        /// Adds the record to the app-level order.
        /// </summary>
        /// <typeparam name="TValueKind">The data object kind.</typeparam>
        /// <param name="appId">The application id.</param>
        /// <param name="recordId">The record id to add.</param>
        private void AddToOrderForApplication<TValueKind>(
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId,
            DataObjectIdentifier<TValueKind, Guid> recordId)
            where TValueKind : DataObjectKind
        {
            var order = this.GetMutableOrderForApplication<TValueKind>(appId);
            order.Add(recordId);
        }

        /// <summary>
        /// Removes the record from the app-level order.
        /// </summary>
        /// <typeparam name="TValueKind">The data object kind.</typeparam>
        /// <param name="appId">The application id.</param>
        /// <param name="recordId">The record id to remove.</param>
        private void RemoveFromOrderForApplication<TValueKind>(
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId,
            DataObjectIdentifier<TValueKind, Guid> recordId)
            where TValueKind : DataObjectKind
        {
            var order = this.GetMutableOrderForApplication<TValueKind>(appId);
            order.Remove(recordId);
        }

        /// <summary>
        /// Adds the record to the consumer-level order.
        /// </summary>
        /// <typeparam name="TValueKind">The data object kind.</typeparam>
        /// <param name="consumerId">The consumer id.</param>
        /// <param name="recordId">The record id to add.</param>
        private void AddToOrderForConsumer<TValueKind>(
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId,
            DataObjectIdentifier<TValueKind, Guid> recordId)
            where TValueKind : DataObjectKind
        {
            var order = this.GetMutableOrderForConsumer<TValueKind>(consumerId);
            order.Add(recordId);
        }

        /// <summary>
        /// Inserts the record to the consumer-level order.
        /// </summary>
        /// <typeparam name="TValueKind">The data object kind.</typeparam>
        /// <param name="index">The index where the id should be inserted.</param>
        /// <param name="consumerId">The consumer id.</param>
        /// <param name="recordId">The record id to add.</param>
        private void InsertToOrderForConsumer<TValueKind>(
            int index,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId,
            DataObjectIdentifier<TValueKind, Guid> recordId)
            where TValueKind : DataObjectKind
        {
            var order = this.GetMutableOrderForConsumer<TValueKind>(consumerId);
            order.InsertAt(index, recordId);
        }

        /// <summary>
        /// Removes the record from the consumer-level order.
        /// </summary>
        /// <typeparam name="TValueKind">The data object kind.</typeparam>
        /// <param name="consumerId">The consumer id.</param>
        /// <param name="recordId">The record id to remove.</param>
        private void RemoveFromOrderForConsumer<TValueKind>(
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId,
            DataObjectIdentifier<TValueKind, Guid> recordId)
            where TValueKind : DataObjectKind
        {
            var order = this.GetMutableOrderForConsumer<TValueKind>(consumerId);
            order.Remove(recordId);
        }

        /// <summary>
        /// Gets the entity order for the given data object kind and legacy application.
        /// </summary>
        /// <typeparam name="TValueKind">The data object kind.</typeparam>
        /// <param name="appId">The legacy application id.</param>
        /// <returns>The ordering for the given data object kind and legacy application.</returns>
        private IMutableOrderedIdentifierCollection<TValueKind, Guid> GetMutableOrderForApplication<TValueKind>(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId)
            where TValueKind : DataObjectKind
        {
            return this.collectionProvider.RetrieveCollectionOrder<TValueKind>(appId);
        }

        /// <summary>
        /// Gets the item ordering for a given consumer.
        /// </summary>
        /// <typeparam name="TValueKind">The type of the data object for which an order needs to be retrieved.</typeparam>
        /// <param name="consumerId">The consumer id.</param>
        /// <returns>The item ordering.</returns>
        private IMutableOrderedIdentifierCollection<TValueKind, Guid> GetMutableOrderForConsumer<TValueKind>(DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId) where TValueKind : DataObjectKind
        {
            return this.collectionProvider.RetrieveCollectionOrder<TValueKind>(consumerId: consumerId);
        }

        /// <summary>
        /// Adds a new legacy application and associates the primary borrower for that application with
        /// the specified ulad application.
        /// </summary>
        /// <param name="uladAppId">The ULAD app identifier.</param>
        /// <returns>The identifier of the consumer that was added.</returns>
        private DataObjectIdentifier<DataObjectKind.Consumer, Guid> AddBlankLegacyAppAndUladAppConsumerAssociation(
            DataObjectIdentifier<DataObjectKind.UladApplication, Guid> uladAppId)
        {
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> legacyAppId;
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId;
            DataObjectIdentifier<DataObjectKind.UladApplicationConsumerAssociation, Guid> uladAppConsumerAssocId;
            this.collectionProvider.AddBlankLegacyAppAndUladAppConsumerAssociation(
                this.borrMgmt,
                uladAppId,
                out legacyAppId,
                out consumerId,
                out uladAppConsumerAssocId);

            return consumerId;
        }

        /// <summary>
        /// Clears the ULAD applications and associations.
        /// </summary>
        private void ClearUladApplications()
        {
            var assocIds = this.UladApplicationConsumers.Keys.ToList();
            foreach (var assocId in assocIds)
            {
                this.UladApplicationConsumers.Remove(assocId);
            }

            var appIds = this.UladApplications.Keys.ToList();
            foreach (var appId in appIds)
            {
                this.UladApplications.Remove(appId);
                this.ClearOrdersForUladApplication(appId);
            }
        }
        
        /// <summary>
        /// Hold counts of various legacy borrower types, used for counting IsPrimary flags.
        /// </summary>
        private class PrimaryCounts
        {
            /// <summary>
            /// Gets or sets the number of borrowers.
            /// </summary>
            public int Borrowers { get; set; }

            /// <summary>
            /// Gets or sets the number of active coborrowers.
            /// </summary>
            public int ActiveCoborrowers { get; set; }

            /// <summary>
            /// Gets or sets the number of inactive coborrowers.
            /// </summary>
            public int InactiveCoborrowers { get; set; }

            /// <summary>
            /// Gets a value indicating whether all the counts are zero.
            /// </summary>
            public bool AllZero
            {
                get { return this.Borrowers == 0 && this.ActiveCoborrowers == 0 && this.InactiveCoborrowers == 0; }
            }
        }
    }
}
