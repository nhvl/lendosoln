/// Author: David Dao

using System;
using System.Reflection;
using System.IO;

namespace DataAccess
{
	public class SchemaFolder
	{
        public static string RetrieveSchema(string fileName) 
        {
            // 5/17/2009 dd - We are embedded the schema file *.xml.config as a embedded resource.
            // To embedded the resource in assembly, right click on the file and choose Properties. In the Build Action choose "Embedded Resource"
            // To retrieve resouce, the name must match exactly. "{namespace}.{resourcename}". You can use Reflector.exe to find out the resource name.
            Assembly assembly = typeof(SchemaFolder).Assembly;

            using (StreamReader stream = new StreamReader(assembly.GetManifestResourceStream("LendersOffice.DataAccess." + fileName)))
            {
                return stream.ReadToEnd();
            }
        }


	}
}
