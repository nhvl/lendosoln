﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{
    public class NotEnforceAccessControlPageData : CPageData
    {
        public NotEnforceAccessControlPageData(Guid sLId, IEnumerable<string> dependencyFields)
            : base(sLId, "NotEnforceAccessControlPageData",  dependencyFields)
        {
            
        }

        protected override bool m_enforceAccessControl
        {
            get
            {
                return false;
            }
        }
    }
}
