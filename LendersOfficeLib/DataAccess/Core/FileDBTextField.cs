﻿// <copyright file="FileDBTextField.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   11/11/2015
// </summary>

namespace DataAccess
{
    using System;
    using System.IO;

    /// <summary>
    /// Represent a single text content store in file db.
    /// </summary>
    public class FileDBTextField
    {
        /// <summary>
        /// The text content from <code>filedb</code>. Value is null when it is not load from <code>filedb</code>.
        /// </summary>
        private string content = null;

        /// <summary>
        /// Whether the content is modified since last load.
        /// </summary>
        private bool isContentModified = false;

        /// <summary>
        /// Whether the key existed in <code>filedb</code>.
        /// </summary>
        private bool? isExistedInFileDB = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileDBTextField" /> class.
        /// </summary>
        /// <param name="fileDBType">The location of <code>filedb</code> database.</param>
        /// <param name="fieldName">The describe name of the field.</param>
        /// <param name="fileDbKey">The key of <code>filedb</code>.</param>
        public FileDBTextField(E_FileDB fileDBType, string fieldName, string fileDbKey)
        {
            this.FileDBType = fileDBType;
            this.FieldName = fieldName;
            this.FileDbKey = fileDbKey;
        }

        /// <summary>
        /// Gets the database type.
        /// </summary>
        /// <value>The database type.</value>
        public E_FileDB FileDBType { get; private set; }

        /// <summary>
        /// Gets the name of the field.
        /// </summary>
        /// <value>The name of the field.</value>
        public string FieldName { get; private set; }

        /// <summary>
        /// Gets the key of <code>filedb</code>.
        /// </summary>
        /// <value>The key of <code>filedb</code>.</value>
        public string FileDbKey { get; private set; }

        /// <summary>
        /// Gets or sets the content of <code>filedb</code>.
        /// </summary>
        /// <value>The content of <code>filedb</code>.</value>
        public string Content
        {
            get
            {
                if (this.content == null)
                {
                    this.LoadFromFileDB();
                }

                return this.content;
            }

            set
            {
                if (this.Content.Equals(value))
                {
                    return; // New data is the same.
                }

                this.isContentModified = true;
                this.content = value ?? string.Empty; // If value is null then set as empty string.
            }
        }

        /// <summary>
        /// Gets a value indicating whether file existed in <code>filedb</code>.
        /// </summary>
        /// <value>Whether file existed in <code>filedb</code>.</value>
        public bool IsExistedInFileDB
        {
            get
            {
                if (this.isExistedInFileDB.HasValue == false)
                {
                    this.LoadFromFileDB();
                }

                return this.isExistedInFileDB.Value;
            }
        }

        /// <summary>
        /// Save the content to <code>filedb</code>.
        /// </summary>
        public void Save()
        {
            if (this.isContentModified == false)
            {
                return; // No-op. No need to save.
            }

            FileDBTools.WriteData(this.FileDBType, this.FileDbKey, this.Content);
        }

        /// <summary>
        /// Load content from <code>filedb</code>. If file does not exists then content is set to empty string.
        /// </summary>
        private void LoadFromFileDB()
        {
            try
            {
                this.content = FileDBTools.ReadDataText(this.FileDBType, this.FileDbKey);
                this.isExistedInFileDB = true;
            }
            catch (FileNotFoundException)
            {
                this.content = string.Empty;
                this.isExistedInFileDB = false;
            }
        }
    }
}