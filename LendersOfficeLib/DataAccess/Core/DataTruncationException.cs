﻿namespace DataAccess
{
    /// <summary>
    /// Exception that represents when data would be truncated.
    /// </summary>
    public class DataTruncationException : CBaseException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataTruncationException"/> class.
        /// </summary>
        /// <param name="developerMessage">The developer message.</param>
        public DataTruncationException(string developerMessage) 
            : base(LendersOffice.Common.ErrorMessages.Generic, developerMessage)
        {
        }
    }
}
