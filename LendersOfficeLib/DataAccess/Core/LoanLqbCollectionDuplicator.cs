﻿namespace DataAccess
{
    using System.Collections.Generic;
    using System.Linq;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;

    using ConsumerId = LqbGrammar.DataTypes.DataObjectIdentifier<LqbGrammar.DataTypes.DataObjectKind.Consumer, System.Guid>;
    using EmpRecId = LqbGrammar.DataTypes.DataObjectIdentifier<LqbGrammar.DataTypes.DataObjectKind.EmploymentRecord, System.Guid>;
    using LegacyApplicationId = LqbGrammar.DataTypes.DataObjectIdentifier<LqbGrammar.DataTypes.DataObjectKind.LegacyApplication, System.Guid>;

    /// <summary>
    /// Provides functionality to copy the LQB collections between files.
    /// </summary>
    internal partial class LoanLqbCollectionDuplicator
    {
        /// <summary>
        /// The data source.
        /// </summary>
        private ILoanLqbCollectionContainer sourceLoan;

        /// <summary>
        /// The destination for the copied data.
        /// </summary>
        private ILoanLqbCollectionContainer destinationLoan;

        /// <summary>
        /// A map from source file consumer id to destination field consumer id.
        /// </summary>
        private Dictionary<ConsumerId, ConsumerId> consumerIdMap;

        /// <summary>
        /// A map from source file legacy app id to destination field legacy app id.
        /// </summary>
        private Dictionary<LegacyApplicationId, LegacyApplicationId> legacyAppIdMap;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanLqbCollectionDuplicator"/> class.
        /// </summary>
        /// <param name="sourceLoan">The source.</param>
        /// <param name="destinationLoan">The destination.</param>
        /// <param name="consumerIdMap">The map from source consumer id to destination consumer id.</param>
        /// <param name="legacyAppIdMap">The map from source legacy app id to destination legacy app id.</param>
        public LoanLqbCollectionDuplicator(
            ILoanLqbCollectionContainer sourceLoan,
            ILoanLqbCollectionContainer destinationLoan,
            Dictionary<ConsumerId, ConsumerId> consumerIdMap,
            Dictionary<LegacyApplicationId, LegacyApplicationId> legacyAppIdMap)
        {
            this.sourceLoan = sourceLoan;
            this.destinationLoan = destinationLoan;
            this.consumerIdMap = consumerIdMap;
            this.legacyAppIdMap = legacyAppIdMap;
        }

        /// <summary>
        /// Duplicates the ULAD applications and associations. Clears all existing ULAD applications and associations.
        /// </summary>
        public void DuplicateUladApplications()
        {
            this.destinationLoan.DuplicateUladApplications(this.sourceLoan, this.consumerIdMap, this.legacyAppIdMap);
        }
    }
}
