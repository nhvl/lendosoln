﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Defines a portion of the inputs required to be entered into APRWIN
    /// in order to get an APR calculation.
    /// </summary>
    /// <remarks>
    /// The properties in these classes correspond to input fields in APRWIN.
    /// No amount of code documentation is going to be a substitute for having
    /// APRWIN open when working with this class. As such, no such documentation
    /// is provided.
    /// </remarks>
    public abstract class AprWinInputs
    {
        /// <summary>
        /// Generates a string-based representation of the class's slate of APRWIN
        /// input elements.
        /// </summary>
        /// <param name="indentLevel">The level of indentation. If passing to a subordinate class, add one.</param>
        /// <returns>The string-based representation of the class's slate of APRWIN input elements.</returns>
        public abstract string GenerateInputs(int indentLevel);

        /// <summary>
        /// Top-level APRWIN data set, currently supporting APRWIN 6.2.
        /// </summary>
        public class AprWin_6_2 : AprWinInputs
        {
            /// <summary>
            /// Lazy backing field for RegularInputs.
            /// </summary>
            private Lazy<Regular> regularInputs = new Lazy<Regular>(() => new Regular());

            /// <summary>
            /// Lazy backing field for ConstructionInputs.
            /// </summary>
            private Lazy<Construction> constructionInputs = new Lazy<Construction>(() => new Construction());

            /// <summary>
            /// Options for the Loan Type selection.
            /// </summary>
            public enum LoanTypeOptions
            {
                /// <summary>
                /// Blank default to destinguish failure to enter data from data.
                /// </summary>
                [Description("Blank")]
                Blank,

                /// <summary>
                /// The option as it exists in APRWIN.
                /// </summary>
                [Description("Regular APR")]
                Regular,

                /// <summary>
                /// The option as it exists in APRWIN.
                /// </summary>
                [Description("Construction Loan")]
                Construction
            }

            /// <summary>
            /// Options for the Loan Type selection for payments.
            /// </summary>
            public enum LoanPaymentTypeOptions
            {
                /// <summary>
                /// Blank default to destinguish failure to enter data from data.
                /// </summary>
                [Description("Blank")]
                Blank,

                /// <summary>
                /// The option as it exists in APRWIN.
                /// </summary>
                [Description("Installment Loan")]
                Installment,

                /// <summary>
                /// The option as it exists in APRWIN.
                /// </summary>
                [Description("Single Advance/Single Payment")]
                SingleAdvanceSinglePayment
            }

            /// <summary>
            /// Options for the Payment Frequency selection.
            /// </summary>
            public enum PaymentFrequencyOptions
            {
                /// <summary>
                /// Blank default to destinguish failure to enter data from data.
                /// </summary>
                [Description("Blank")]
                Blank,

                /// <summary>
                /// The option as it exists in APRWIN.
                /// </summary>
                [Description("Monthly")]
                Monthly,

                /// <summary>
                /// The option as it exists in APRWIN.
                /// </summary>
                [Description("Multiples of a Month")]
                MultiplesOfAMonth,

                /// <summary>
                /// The option as it exists in APRWIN.
                /// </summary>
                [Description("Semi-Monthly")]
                SemiMonthly,

                /// <summary>
                /// The option as it exists in APRWIN.
                /// </summary>
                [Description("Actual Days")]
                ActualDays
            }

            /// <summary>
            /// Options for the Interest Payable selection.
            /// </summary>
            public enum InterestPayableOptions
            {
                /// <summary>
                /// Blank default to destinguish failure to enter data from data.
                /// </summary>
                [Description("Blank")]
                Blank,

                /// <summary>
                /// The option as it exists in APRWIN.
                /// </summary>
                [Description("On Advances When Made")]
                OnAdvancesWhenMade,

                /// <summary>
                /// The option as it exists in APRWIN.
                /// </summary>
                [Description("On Entire Commitment")]
                OnEntireCommitment
            }

            /// <summary>
            /// Options for the Accrual Options selection for the construction phase.
            /// </summary>
            public enum ConstructionPhaseInterestAccrualOptions
            {
                /// <summary>
                /// Blank default to destinguish failure to enter data from data.
                /// </summary>
                [Description("Blank")]
                Blank,

                /// <summary>
                /// The option as it exists in APRWIN.
                /// </summary>
                [Description("Whole Months (360/360)")]
                WholeMonths360_360,

                /// <summary>
                /// The option as it exists in APRWIN.
                /// </summary>
                [Description("Actual Days (365/365)")]
                ActualDays365_365,

                /// <summary>
                /// The option as it exists in APRWIN.
                /// </summary>
                [Description("Actual Days (365/360)")]
                ActualDays365_360,

                /// <summary>
                /// The option as it exists in APRWIN.
                /// </summary>
                [Description("Other")]
                Other
            }

            /// <summary>
            /// Options for the Basis for Measuring Loan Term selection for the Other accrual selection.
            /// </summary>
            public enum BasisForMeasuringLoanTermOptions
            {
                /// <summary>
                /// Blank default to destinguish failure to enter data from data.
                /// </summary>
                [Description("Blank")]
                Blank,

                /// <summary>
                /// The option as it exists in APRWIN.
                /// </summary>
                [Description("Whole Months")]
                WholeMonths,

                /// <summary>
                /// The option as it exists in APRWIN.
                /// </summary>
                [Description("Actual Days")]
                ActualDays
            }

            /// <summary>
            /// Gets value of the container for the Regular APR screens.
            /// </summary>
            public Regular RegularInputs
            {
                get
                {
                    if (this.constructionInputs.IsValueCreated)
                    {
                        return null;
                    }

                    return this.regularInputs.Value;
                }
            }

            /// <summary>
            /// Gets the value of the container for the Construction Loan screens.
            /// </summary>
            public Construction ConstructionInputs
            {
                get
                {
                    if (this.regularInputs.IsValueCreated)
                    {
                        return null;
                    }

                    return this.constructionInputs.Value;
                }
            }

            /// <summary>
            /// Converts a ConstructionPhaseIntAccrual value to the equivalent ConstructionPhaseInterestAccrualOptions value.
            /// </summary>
            /// <param name="e">The ConstructionPhaseIntAccrual value to convert.</param>
            /// <returns>The corresponding ConstructionPhaseInterestAccrualOptions value.</returns>
            public static ConstructionPhaseInterestAccrualOptions ToAccrualOptions(DataAccess.Core.Construction.ConstructionPhaseIntAccrual e)
            {
                switch (e)
                {
                    case Core.Construction.ConstructionPhaseIntAccrual.Monthly_360_360:
                        return ConstructionPhaseInterestAccrualOptions.WholeMonths360_360;
                    case Core.Construction.ConstructionPhaseIntAccrual.ActualDays_365_365:
                        return ConstructionPhaseInterestAccrualOptions.ActualDays365_365;
                    case Core.Construction.ConstructionPhaseIntAccrual.ActualDays_365_360:
                        return ConstructionPhaseInterestAccrualOptions.ActualDays365_360;
                    case Core.Construction.ConstructionPhaseIntAccrual.Blank:
                        return ConstructionPhaseInterestAccrualOptions.Blank;
                    default:
                        throw new UnhandledEnumException(e);
                }
            }

            /// <summary>
            /// Generates a string-based representation of the class's slate of APRWIN
            /// input elements.
            /// </summary>
            /// <param name="indentLevel">The level of indentation. If passing to a subordinate class, add one.</param>
            /// <returns>The string-based representation of the class's slate of APRWIN input elements.</returns>
            public override string GenerateInputs(int indentLevel)
            {
                string indent = new string(' ', indentLevel * 2);

                if (this.regularInputs.IsValueCreated)
                {
                    return indent + "Regular APR" + Environment.NewLine + indent + this.RegularInputs.GenerateInputs(indentLevel + 1);
                }
                else if (this.constructionInputs.IsValueCreated)
                {
                    return indent + "Construction Loan" + Environment.NewLine + indent + this.ConstructionInputs.GenerateInputs(indentLevel + 1);
                }
                else
                {
                    return indent + "Error: Neither Regular nor Construction inputs set" + Environment.NewLine;
                }
            }

            /// <summary>
            /// Top level data for the Regular APR path.
            /// </summary>
            public class Regular : AprWinInputs
            {
                /// <summary>
                /// Lazy backing field for the InstallmentLoan property.
                /// </summary>
                private Lazy<InstallmentLoan> installmentLoan = new Lazy<InstallmentLoan>(() => new InstallmentLoan());

                /// <summary>
                /// Lazy backing field for the PaymenSchedule property.
                /// </summary>
                private Lazy<RegularPaymentSchedule> paymentSchedule = new Lazy<RegularPaymentSchedule>(() => new RegularPaymentSchedule());

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public decimal AmountFinanced { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public decimal DisclosedOrEstimatedApr { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public decimal DisclosedFinanceCharge { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public LoanTypeOptions LoanType { get; set; }

                /// <summary>
                /// Gets the value for the Installment Loan screen in APRWIN.
                /// </summary>
                public InstallmentLoan InstallmentLoan
                {
                    get
                    {
                        return this.installmentLoan.Value;
                    }
                }

                /// <summary>
                /// Gets a value indicating whether the corresponding field in APRWIN is checked, because bools are just that special.
                /// </summary>
                public bool LoanSecuredByRealEstateOrDwelling
                {
                    get { return true; }
                }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public UnzonedDate PaymentAdvanceDate { get; set; }

                /// <summary>
                /// Gets the value of the container for the Payment Schedule screens in APRWIN.
                /// </summary>
                public RegularPaymentSchedule PaymentSchedule
                {
                    get
                    {
                        return this.paymentSchedule.Value;
                    }
                }

                /// <summary>
                /// Generates a string-based representation of the class's slate of APRWIN
                /// input elements.
                /// </summary>
                /// <param name="indentLevel">The level of indentation. If passing to a subordinate class, add one.</param>
                /// <returns>The string-based representation of the class's slate of APRWIN input elements.</returns>
                public override string GenerateInputs(int indentLevel)
                {
                    StringBuilder sb = new StringBuilder();
                    string indent = new string(' ', indentLevel * 2);

                    sb.AppendLine(indent + $"Amount Financed: {this.AmountFinanced}");
                    sb.AppendLine(indent + $"Disclosed (or estimated) APR: {this.DisclosedOrEstimatedApr}");
                    sb.AppendLine(indent + $"Disclosed Finance Charge: {this.DisclosedFinanceCharge}");

                    if (this.installmentLoan.IsValueCreated)
                    {
                        sb.AppendLine(indent + $"Loan Type: Installment Loan");
                        sb.Append(this.InstallmentLoan.GenerateInputs(indentLevel + 1));
                    }

                    sb.AppendLine(indent + $"Loan Secured by Real Estate or Dwelling: {this.LoanSecuredByRealEstateOrDwelling}");

                    sb.AppendLine(indent + "Payment Schedule");
                    sb.Append(this.PaymentSchedule.GenerateInputs(indentLevel + 1));

                    return sb.ToString();
                }
            }

            /// <summary>
            /// Data for the Installment Loan data entry path.
            /// </summary>
            public class InstallmentLoan : AprWinInputs
            {
                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public PaymentFrequencyOptions PaymentFrequency { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public int MonthsInAUnitPeriod { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public int DaysInAUnitPeriod { get; set; }

                /// <summary>
                /// Generates a string-based representation of the class's slate of APRWIN
                /// input elements.
                /// </summary>
                /// <param name="indentLevel">The level of indentation. If passing to a subordinate class, add one.</param>
                /// <returns>The string-based representation of the class's slate of APRWIN input elements.</returns>
                public override string GenerateInputs(int indentLevel)
                {
                    StringBuilder sb = new StringBuilder();
                    string indent = new string(' ', indentLevel * 2);

                    sb.Append(indent + "Payment Frequency: ");

                    switch (this.PaymentFrequency)
                    {
                        case PaymentFrequencyOptions.Monthly:
                            sb.AppendLine("Monthly");
                            break;
                        case PaymentFrequencyOptions.MultiplesOfAMonth:
                            sb.AppendLine($"Multiples of a Month ({this.MonthsInAUnitPeriod} months per unit period)");
                            break;
                        case PaymentFrequencyOptions.SemiMonthly:
                            sb.AppendLine("Semi-Monthly");
                            break;
                        case PaymentFrequencyOptions.ActualDays:
                            sb.AppendLine($"Actual Days ({this.DaysInAUnitPeriod} days per unit period)");
                            break;
                        case PaymentFrequencyOptions.Blank:
                            sb.AppendLine("Blank");
                            break;
                        default:
                            throw new UnhandledEnumException(this.PaymentFrequency);
                    }

                    return sb.ToString();
                }
            }

            /// <summary>
            /// Common data for the Payment Schedule paths.
            /// </summary>
            public abstract class PaymentSchedule : AprWinInputs
            {
                /// <summary>
                /// Gets or sets the list of payments that goes on the Payment Schedule screen in APRWIN.
                /// </summary>
                public IEnumerable<AprAmountData> PaymentList { get; set; }

                /// <summary>
                /// Generates a string-based representation of the dates for the
                /// calendar portion of the Payment Schedule screen.
                /// </summary>
                /// <param name="indentLevel">The level of indentation.</param>
                /// <returns>A string-based representation of the dates for the calendar portion of the Payment Schedule screen.</returns>
                public abstract string GenerateDatePart(int indentLevel);

                /// <summary>
                /// Generates a string-based representation of the payment stream for
                /// the Payment Schedule screen.
                /// </summary>
                /// <param name="indentLevel">The level of indentation.</param>
                /// <returns>A string-based representation of the payment stream of the Payment Schedule screen.</returns>
                public string GeneratePaymentListPart(int indentLevel)
                {
                    StringBuilder sb = new StringBuilder();
                    string indent = new string(' ', indentLevel * 2);

                    AprAmountData lastPmt = this.PaymentList.First();
                    int runningCount = 1;
                    int lastUnitPeriod = lastPmt.UnitPeriod;
                    var groupedPaymentList = new List<Tuple<AprAmountData, int>>();

                    foreach (var pmt in this.PaymentList.Skip(1))
                    {
                        if (lastPmt.Amount == pmt.Amount && lastPmt.OddDays == pmt.OddDays && lastUnitPeriod + 1 == pmt.UnitPeriod)
                        {
                            runningCount++;
                        }
                        else
                        {
                            groupedPaymentList.Add(new Tuple<AprAmountData, int>(lastPmt, runningCount));
                            runningCount = 1;
                            lastPmt = pmt;
                        }

                        lastUnitPeriod = pmt.UnitPeriod;
                    }

                    groupedPaymentList.Add(new Tuple<AprAmountData, int>(lastPmt, runningCount));

                    sb.AppendLine(indent + "Payment List");
                    string paymentIndent = new string(' ', (indentLevel + 1) * 2);

                    int streamNumber = 0;

                    foreach (var groupedPmt in groupedPaymentList)
                    {
                        var pmt = groupedPmt.Item1;
                        int count = groupedPmt.Item2;

                        sb.AppendLine(paymentIndent + $"#: {++streamNumber}, Amount: {pmt.Amount}, Number of Payments: {count}, UnitPeriods: {pmt.UnitPeriod}, Odd Days: {pmt.OddDays}");
                    }

                    return sb.ToString();
                }

                /// <summary>
                /// Generates a string-based representation of the class's slate of APRWIN
                /// input elements.
                /// </summary>
                /// <param name="indentLevel">The level of indentation. If passing to a subordinate class, add one.</param>
                /// <returns>The string-based representation of the class's slate of APRWIN input elements.</returns>
                public override string GenerateInputs(int indentLevel)
                {
                    return this.GenerateDatePart(indentLevel) + this.GeneratePaymentListPart(indentLevel);
                }
            }

            /// <summary>
            /// Payment Schedule data specific to the Regular APR data entry path.
            /// </summary>
            public class RegularPaymentSchedule : PaymentSchedule
            {
                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public UnzonedDate LoanDate { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public UnzonedDate PaymentAdvanceDate { get; set; }

                /// <summary>
                /// Generates a string-based representation of the dates for the
                /// calendar portion of the Payment Schedule screen.
                /// </summary>
                /// <param name="indentLevel">The level of indentation.</param>
                /// <returns>A string-based representation of the dates for the calendar portion of the Payment Schedule screen.</returns>
                public override string GenerateDatePart(int indentLevel)
                {
                    string indent = new string(' ', indentLevel * 2);

                    StringBuilder sb = new StringBuilder();

                    sb.AppendLine(indent + $"Loan Date: {this.LoanDate}");
                    sb.AppendLine(indent + $"Payment/Advance Date: {this.PaymentAdvanceDate}");

                    return sb.ToString();
                }
            }
            
            /// <summary>
            /// Top level data for the Construction Loan path.
            /// </summary>
            public class Construction : AprWinInputs
            {
                /// <summary>
                /// Lazy backing field for the ConstructionSeparately property.
                /// </summary>
                private Lazy<ConstructionSeparately> constructionSeparately = new Lazy<ConstructionSeparately>(() => new ConstructionSeparately());

                /// <summary>
                /// Lazy backing field for the ConstructionAndPermanent property.
                /// </summary>
                private Lazy<ConstructionAndPermanent> constructionAndPermanent = new Lazy<ConstructionAndPermanent>(() => new ConstructionAndPermanent());

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public decimal LoanCommitmentAmount { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public decimal AnnualSimpleInterestRate { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public decimal PrepaidFinanceCharge { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public decimal SubsequentlyPaidFinanceCharge { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public InterestPayableOptions InterestPayable { get; set; }

                /// <summary>
                /// Gets a value indicating whether the corresponding field in APRWIN is checked, because bools are just that special.
                /// </summary>
                public bool LoanSecuredByRealEstateOrDwelling
                {
                    get { return true; }
                }

                /// <summary>
                /// Gets a value indicating whether the corresponding field in APRWIN is checked, because bools are just that special.
                /// </summary>
                public bool LoanDateEarlierThan_9_30_1995
                {
                    get { return false; }
                }

                /// <summary>
                /// Gets the value of the container for the Construction Separately screens in APRWIN.
                /// </summary>
                public ConstructionSeparately ConstructionSeparately
                {
                    get
                    {
                        if (this.constructionAndPermanent.IsValueCreated)
                        {
                            return null;
                        }

                        return this.constructionSeparately.Value;
                    }
                }

                /// <summary>
                /// Gets the value for the container for the Construction and Permanent screens in APRWIN.
                /// </summary>
                public ConstructionAndPermanent ConstructionAndPermanent
                {
                    get
                    {
                        if (this.constructionSeparately.IsValueCreated)
                        {
                            return null;
                        }

                        return this.constructionAndPermanent.Value;
                    }
                }

                /// <summary>
                /// Generates a string-based representation of the class's slate of APRWIN
                /// input elements.
                /// </summary>
                /// <param name="indentLevel">The level of indentation. If passing to a subordinate class, add one.</param>
                /// <returns>The string-based representation of the class's slate of APRWIN input elements.</returns>
                public override string GenerateInputs(int indentLevel)
                {
                    StringBuilder sb = new StringBuilder();
                    string indent = new string(' ', indentLevel * 2);

                    sb.AppendLine(indent + $"Loan Commitment Amount: {this.LoanCommitmentAmount}");
                    sb.AppendLine(indent + $"Annual Simple Interest Rate: {this.AnnualSimpleInterestRate}");
                    sb.AppendLine(indent + $"Prepaid Finance Charge: {this.PrepaidFinanceCharge}");
                    sb.AppendLine(indent + $"Subsequently Paid Finance Charge: {this.SubsequentlyPaidFinanceCharge}");
                    sb.AppendLine(indent + $"Interest Payable: {this.InterestPayable.GetDescription()}");
                    sb.AppendLine(indent + $"Loan Secured by Real Estate or Dwelling: {this.LoanSecuredByRealEstateOrDwelling}");
                    sb.AppendLine(indent + $"Loan Date Earlier than 9/30/1995: {this.LoanDateEarlierThan_9_30_1995}");

                    if (this.constructionSeparately.IsValueCreated)
                    {
                        sb.AppendLine(indent + "Construction Separately");
                        sb.Append(this.ConstructionSeparately.GenerateInputs(indentLevel + 1));
                    }
                    else if (this.constructionAndPermanent.IsValueCreated)
                    {
                        sb.AppendLine(indent + "Construction and Permanent");
                        sb.Append(this.ConstructionAndPermanent.GenerateInputs(indentLevel + 1));
                    }
                    else
                    {
                        sb.AppendLine(indent + "Error: Neither Construction Separately nor Construction And Permanent set");
                    }

                    return sb.ToString();
                }
            }

            /// <summary>
            /// Data for the Construction Separately data path.
            /// </summary>
            public class ConstructionSeparately : AprWinInputs
            {
                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public ConstructionPhaseInterestAccrualOptions ConstructionPhaseInterestAccrual { get; set; }

                /// <summary>
                /// Gets or sets a value indicating whether the corresponding field in APRWIN is checked, because bools are just that special.
                /// </summary>
                public bool RequiredInterestReserve { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public UnzonedDate LoanDate { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public UnzonedDate ConstructionPeriodEndDate { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public int NumberOfMonths { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public int NumberOfDays { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public int NumberOfUnitPeriods { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public int NumberOfOddDays { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public BasisForMeasuringLoanTermOptions OtherBasisForMeasuringLoanTerm { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public decimal EstimatedConstructionInterest { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public decimal DisclosedOrEstimatedApr { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public decimal DisclosedFinanceCharge { get; set; }

                /// <summary>
                /// Generates a string-based representation of the class's slate of APRWIN
                /// input elements.
                /// </summary>
                /// <param name="indentLevel">The level of indentation. If passing to a subordinate class, add one.</param>
                /// <returns>The string-based representation of the class's slate of APRWIN input elements.</returns>
                public override string GenerateInputs(int indentLevel)
                {
                    StringBuilder sb = new StringBuilder();
                    string indent = new string(' ', indentLevel * 2);

                    sb.AppendLine(indent + $"Construction Phase Interest Accrual: {this.ConstructionPhaseInterestAccrual.GetDescription()}");
                    sb.AppendLine(indent + $"Required Interest Reserve: {this.RequiredInterestReserve}");
                    sb.AppendLine(indent + "Construction Phase Time Period");

                    string timePeriodIndent = new string(' ', (indentLevel + 1) * 2);

                    switch (this.ConstructionPhaseInterestAccrual)
                    {
                        case ConstructionPhaseInterestAccrualOptions.WholeMonths360_360:
                            sb.AppendLine(timePeriodIndent + $"Number of Months: {this.NumberOfMonths}");
                            sb.AppendLine(timePeriodIndent + $"Loan Date: {this.LoanDate}");
                            sb.AppendLine(timePeriodIndent + $"Construction Period End Date: {this.ConstructionPeriodEndDate}");
                            break;
                        case ConstructionPhaseInterestAccrualOptions.ActualDays365_365:
                        case ConstructionPhaseInterestAccrualOptions.ActualDays365_360:
                            sb.AppendLine(timePeriodIndent + $"Number of Days: {this.NumberOfDays}");
                            sb.AppendLine(timePeriodIndent + $"Number of Unit Periods: {this.NumberOfUnitPeriods}");
                            sb.AppendLine(timePeriodIndent + $"Number of Odd Days: {this.NumberOfOddDays}");
                            sb.AppendLine(timePeriodIndent + $"Loan Date: {this.LoanDate}");
                            sb.AppendLine(timePeriodIndent + $"Construction Period End Date: {this.ConstructionPeriodEndDate}");
                            break;
                        case ConstructionPhaseInterestAccrualOptions.Other:
                            sb.AppendLine(timePeriodIndent + $"Basis for Measuring Loan Term: {this.OtherBasisForMeasuringLoanTerm.GetDescription()}");
                            
                            if (this.OtherBasisForMeasuringLoanTerm == BasisForMeasuringLoanTermOptions.ActualDays)
                            {
                                sb.AppendLine(timePeriodIndent + $"Number of Months: {this.NumberOfMonths}");
                                sb.AppendLine(timePeriodIndent + $"Loan Date: {this.LoanDate}");
                                sb.AppendLine(timePeriodIndent + $"Construction Period End Date: {this.ConstructionPeriodEndDate}");
                            }
                            else
                            {
                                sb.AppendLine(timePeriodIndent + $"Number of Days: {this.NumberOfDays}");
                                sb.AppendLine(timePeriodIndent + $"Number of Unit Periods: {this.NumberOfUnitPeriods}");
                                sb.AppendLine(timePeriodIndent + $"Number of Odd Days: {this.NumberOfOddDays}");
                                sb.AppendLine(timePeriodIndent + $"Loan Date: {this.LoanDate}");
                                sb.AppendLine(timePeriodIndent + $"Construction Period End Date: {this.ConstructionPeriodEndDate}");
                            }

                            break;
                        default:
                            throw new UnhandledEnumException(this.ConstructionPhaseInterestAccrual);
                    }

                    sb.AppendLine(indent + $"Estimated Construction Interest: {this.EstimatedConstructionInterest}");
                    sb.AppendLine(indent + $"Disclosed (or estimated) APR: {this.DisclosedOrEstimatedApr}");
                    sb.AppendLine(indent + $"Disclosed Finance Charge: {this.DisclosedFinanceCharge}");

                    return sb.ToString();
                }
            }

            /// <summary>
            /// Data for the Construction and Permanent data entry path.
            /// </summary>
            public class ConstructionAndPermanent : AprWinInputs
            {
                /// <summary>
                /// Lazy backing field for the InstallmentLoan property.
                /// </summary>
                private Lazy<InstallmentLoan> installmentLoan = new Lazy<InstallmentLoan>(() => new InstallmentLoan());

                /// <summary>
                /// Lazy backing field for the PaymentSchedule property.
                /// </summary>
                private Lazy<ConstructionPaymentSchedule> paymentSchedule = new Lazy<ConstructionPaymentSchedule>(() => new ConstructionPaymentSchedule());

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public ConstructionPhaseInterestAccrualOptions ConstructionPhaseInterestAccrual { get; set; }

                /// <summary>
                /// Gets or sets a value indicating whether the corresponding field in APRWIN is checked, because bools are just that special.
                /// </summary>
                public bool RequiredInterestReserve { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public UnzonedDate LoanDate { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public UnzonedDate ConstructionPeriodEndDate { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public int NumberOfMonths { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public int NumberOfDays { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public decimal EstimatedConstructionInterest { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public decimal DisclosedOrEstimatedApr { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public decimal DisclosedFinanceCharge { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public LoanPaymentTypeOptions LoanType { get; set; }

                /// <summary>
                /// Gets the value for the container for the Installment Loan screen in APRWIN.
                /// </summary>
                public InstallmentLoan InstallmentLoan
                {
                    get { return this.installmentLoan.Value; }
                }

                /// <summary>
                /// Gets the value for the container for the Payment Schedule screen in APRWIN.
                /// </summary>
                public ConstructionPaymentSchedule PaymentSchedule
                {
                    get { return this.paymentSchedule.Value; }
                }

                /// <summary>
                /// Generates a string-based representation of the class's slate of APRWIN
                /// input elements.
                /// </summary>
                /// <param name="indentLevel">The level of indentation. If passing to a subordinate class, add one.</param>
                /// <returns>The string-based representation of the class's slate of APRWIN input elements.</returns>
                public override string GenerateInputs(int indentLevel)
                {
                    StringBuilder sb = new StringBuilder();
                    string indent = new string(' ', indentLevel * 2);

                    sb.AppendLine(indent + $"Construction Phase Interest Accrual: {this.ConstructionPhaseInterestAccrual.GetDescription()}");
                    sb.AppendLine(indent + $"Required Interest Reserve: {this.RequiredInterestReserve}");
                    sb.AppendLine(indent + "Construction Phase Time Period");

                    string timePeriodIndent = new string(' ', (indentLevel + 1) * 2);

                    switch (this.ConstructionPhaseInterestAccrual)
                    {
                        case ConstructionPhaseInterestAccrualOptions.WholeMonths360_360:
                            sb.AppendLine(timePeriodIndent + $"Number of Months: {this.NumberOfMonths}");
                            sb.AppendLine(timePeriodIndent + $"Loan Date: {this.LoanDate}");
                            sb.AppendLine(timePeriodIndent + $"Construction Period End Date: {this.ConstructionPeriodEndDate}");
                            break;
                        case ConstructionPhaseInterestAccrualOptions.ActualDays365_365:
                        case ConstructionPhaseInterestAccrualOptions.ActualDays365_360:
                            sb.AppendLine(timePeriodIndent + $"Number of Days: {this.NumberOfDays}");
                            sb.AppendLine(timePeriodIndent + $"Loan Date: {this.LoanDate}");
                            sb.AppendLine(timePeriodIndent + $"Construction Period End Date: {this.ConstructionPeriodEndDate}");
                            break;
                        case ConstructionPhaseInterestAccrualOptions.Other:
                            break;
                        default:
                            throw new UnhandledEnumException(this.ConstructionPhaseInterestAccrual);
                    }

                    sb.AppendLine(indent + $"Estimated Construction Interest: {this.EstimatedConstructionInterest}");
                    sb.AppendLine(indent + $"Disclosed (or estimated) APR: {this.DisclosedOrEstimatedApr}");
                    sb.AppendLine(indent + $"Disclosed Finance Charge: {this.DisclosedFinanceCharge}");

                    sb.AppendLine(indent + $"Loan Type: {this.LoanType.GetDescription()}");

                    if (this.LoanType == LoanPaymentTypeOptions.Installment)
                    {
                        sb.Append(this.InstallmentLoan.GenerateInputs(indentLevel + 1));
                    }
                    else
                    {
                        sb.AppendLine(indent + "Unsupported payment type. Please fix the APRWIN inputs generator.");
                    }

                    sb.AppendLine(indent + "Payment Schedule");
                    sb.Append(this.PaymentSchedule.GenerateInputs(indentLevel + 1));

                    return sb.ToString();
                }
            }

            /// <summary>
            /// Payment Schedule data specific to the Construction Loan data entry path.
            /// </summary>
            public class ConstructionPaymentSchedule : PaymentSchedule
            {
                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public UnzonedDate LoanDate { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public UnzonedDate ConstructionPeriodEndDate { get; set; }

                /// <summary>
                /// Gets or sets the value for the corresponding field in APRWIN.
                /// </summary>
                public UnzonedDate AmortizationPaymentDate { get; set; }

                /// <summary>
                /// Generates a string-based representation of the dates for the
                /// calendar portion of the Payment Schedule screen.
                /// </summary>
                /// <param name="indentLevel">The level of indentation.</param>
                /// <returns>A string-based representation of the dates for the calendar portion of the Payment Schedule screen.</returns>
                public override string GenerateDatePart(int indentLevel)
                {
                    string indent = new string(' ', indentLevel * 2);

                    StringBuilder sb = new StringBuilder();

                    sb.AppendLine(indent + $"Loan Date: {this.LoanDate}");
                    sb.AppendLine(indent + $"Construction Period End Date: {this.ConstructionPeriodEndDate}");
                    sb.AppendLine(indent + $"Amortization Payment Date: {this.AmortizationPaymentDate}");

                    return sb.ToString();
                }
            }
        }
    }
}
