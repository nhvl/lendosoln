﻿namespace DataAccess
{
    /// <summary>
    /// Holds borrower and coborrower name and ssn entered dates.
    /// </summary>
    public class NameSsnEnteredDates
    {
        /// <summary>
        /// The timestamp of when the borrower's name was first entered.
        /// </summary>
        public readonly CDateTime NameFirstEntered;

        /// <summary>
        /// The timestamp of when the borrower's SSN was first entered.
        /// </summary>
        public readonly CDateTime SsnFirstEntered;

        /// <summary>
        /// Initializes a new instance of the <see cref="NameSsnEnteredDates"/> class.
        /// </summary>
        /// <param name="nameFirstEntered">The date the borrower's name was first entered.</param>
        /// <param name="ssnFirstEntered">The date the borrower's ssn was first entered.</param>
        public NameSsnEnteredDates(CDateTime nameFirstEntered, CDateTime ssnFirstEntered)
        {
            this.NameFirstEntered = nameFirstEntered;
            this.SsnFirstEntered = ssnFirstEntered;
        }
    }
}
