﻿// <copyright file="EnumTools.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Scott Kibler
//    Date:   4/13/2015
// </summary>
namespace DataAccess
{
    /// <summary>
    /// A collection of miscellaneous methods pertaining to enumerations.
    /// </summary>
    public static class EnumTools
    {
        /// <summary>
        /// Tells whether or not the provided property type is considered mobile.
        /// </summary>
        /// <param name="gseSpT">The property type.</param>
        /// <returns>True if the provided type is considered mobile.</returns>
        public static bool E_sGseSpTIsAMobileType(DataAccess.E_sGseSpT gseSpT)
        {
            switch (gseSpT)
            {
                case E_sGseSpT.LeaveBlank:
                case E_sGseSpT.Attached:
                case E_sGseSpT.Condominium:
                case E_sGseSpT.Cooperative:
                case E_sGseSpT.Detached:
                case E_sGseSpT.DetachedCondominium:
                case E_sGseSpT.HighRiseCondominium:
                    return false;
                case E_sGseSpT.ManufacturedHomeCondominium:
                case E_sGseSpT.ManufacturedHomeMultiwide:
                case E_sGseSpT.ManufacturedHousing:
                case E_sGseSpT.ManufacturedHousingSingleWide:
                    return true;
                case E_sGseSpT.Modular:
                case E_sGseSpT.PUD:
                    return false;
                default:
                    throw new UnhandledEnumException(gseSpT);
            }
        }
    }
}
