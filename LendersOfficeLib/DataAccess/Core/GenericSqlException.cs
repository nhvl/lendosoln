using System;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Common;

namespace DataAccess
{
	public class GenericSqlException : CBaseException
	{
        private SqlException m_innerException;
		public GenericSqlException(string detailDevMessage, SqlException exc) : base (ErrorMessages.Generic,
            string.Format("{0}{1}{1}{2}", detailDevMessage, Environment.NewLine, exc.ToString()))
		{
            m_innerException = exc;
		}

        public override string EmailSubjectCode 
        {
            get { return "SQL_EXCEPTION::" + m_innerException.Number + " - " + m_innerException.Procedure; }
        }
	}
}
