﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using Adapter;
    using LendersOffice.Drivers.SqlServerDB;
    using LendingQB.Core.Data;
    using LendingQB.Core.Mapping;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Utils;

    /// <summary>
    /// Handles methods requiring database interaction for the collection container.
    /// </summary>
    internal partial class LoanLqbCollectionProvider
    {
        /// <summary>
        /// Save the entire set of changes to the collections.
        /// </summary>
        /// <param name="connection">The database connection.</param>
        /// <param name="transaction">The database transaction.</param>
        /// <param name="entityAndAssociationCommands">The commands to save changes to entity collections and association sets.</param>
        public void SaveCollections(
            IDbConnection connection,
            IDbTransaction transaction,
            IEnumerable<StoredProcedureCommandData> entityAndAssociationCommands)
        {
            var saveCallData = new StoredProcedureCallSet();

            var orderCommands = this.RetrieveSaveEmptyOrdersCallData();
            foreach (var command in orderCommands)
            {
                saveCallData.Add(command.Name, command.Parameters);
            }

            foreach (var command in entityAndAssociationCommands)
            {
                saveCallData.Add(command.Name, command.Parameters);
            }

            orderCommands = this.RetrieveSaveOrdersCallData();
            foreach (var command in orderCommands)
            {
                saveCallData.Add(command.Name, command.Parameters);
            }

            if (saveCallData.CallCount > 0)
            {
                SQLQueryString query;
                IEnumerable<DbParameter> allPrams;
                saveCallData.AggregateCalls(out query, out allPrams);

                var dbconnection = (DbConnection)connection;
                var dbtransaction = (DbTransaction)transaction;

                var factory = GenericLocator<ISqlDriverFactory>.Factory;
                var driver = factory.Create(TimeoutInSeconds.Sixty);
                driver.Update(dbconnection, dbtransaction, query, allPrams);
            }
        }

        /// <summary>
        /// Provides a way to set the broker id that will be used by the collection provider.
        /// This is meant to provide a minor performance boost by not retrieving the broker id
        /// from the database when we don't have to.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        public void SetBrokerId(DataObjectIdentifier<DataObjectKind.ClientCompany, Guid> brokerId)
        {
            this.brokerId = new Lazy<DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>>(() => brokerId);
        }

        /// <summary>
        /// Adds a blank legacy application and a blank ULAD application directly to the database.
        /// </summary>
        /// <remarks>The additions are performed within the same transaction.</remarks>
        /// <param name="borrMgmt">The borrower management loan methods.</param>
        /// <param name="legacyAppId">The legacy application id that was added.</param>
        /// <param name="uladData">The ULAD application that was added.</param>
        public void AddBlankLegacyAndUladApplication(
            ILoanLqbCollectionBorrowerManagement borrMgmt,
            out DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> legacyAppId,
            out KeyValuePair<DataObjectIdentifier<DataObjectKind.UladApplication, Guid>, UladApplication> uladData)
        {
            var appId = Guid.Empty;

            using (var connection = this.connectionFactory())
            {
                connection.OpenWithRetry();
                var transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                try
                {
                    legacyAppId = borrMgmt.AddLegacyApplication(connection, transaction);
                    appId = legacyAppId.Value;

                    KeyValuePair<DataObjectIdentifier<DataObjectKind.UladApplicationConsumerAssociation, Guid>, UladApplicationConsumerAssociation> assocData;
                    this.CreateNewUladApplication(connection, transaction, legacyAppId, out uladData, out assocData);

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, ex);
                }
            }

            borrMgmt.OnSuccessfulApplicationCreation(appId);
        }

        /// <summary>
        /// Adds a blank legacy application and associates the primary borrower of that application
        /// with the specified ULAD application. Performed directly via the database.
        /// </summary>
        /// <param name="borrMgmt">The borrower management loan methods.</param>
        /// <param name="uladAppId">The ULAD application id.</param>
        /// <param name="legacyAppId">The legacy application id that was added.</param>
        /// <param name="consumerId">The consumer id of the new borrower.</param>
        /// <param name="uladAppConsumerAssocId">The ULAD app consumer association ID that was added.</param>
        public void AddBlankLegacyAppAndUladAppConsumerAssociation(
            ILoanLqbCollectionBorrowerManagement borrMgmt,
            DataObjectIdentifier<DataObjectKind.UladApplication, Guid> uladAppId,
            out DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> legacyAppId,
            out DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId,
            out DataObjectIdentifier<DataObjectKind.UladApplicationConsumerAssociation, Guid> uladAppConsumerAssocId)
        {
            using (var connection = this.connectionFactory())
            {
                connection.OpenWithRetry();
                var transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                try
                {
                    legacyAppId = borrMgmt.AddLegacyApplication(connection, transaction);

                    KeyValuePair<DataObjectIdentifier<DataObjectKind.UladApplicationConsumerAssociation, Guid>, UladApplicationConsumerAssociation> assocData;
                    this.CreateNewUladAppConsumerAssociationForLegacyAppBorrower(connection, transaction, uladAppId, legacyAppId, out assocData);
                    consumerId = assocData.Value.ConsumerId;
                    uladAppConsumerAssocId = assocData.Key;

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, ex);
                }
            }

            borrMgmt.OnSuccessfulApplicationCreation(legacyAppId.Value);
        }

        /// <summary>
        /// Adds a coborrower to the specified legacy application and ULAD application directly via the database.
        /// </summary>
        /// <param name="legacyAppId">The legacy app id.</param>
        /// <param name="uladAppId">The ULAD app id.</param>
        /// <param name="consumerId">The id of the borrower that was added.</param>
        /// <param name="uladAppConsumerAssocId">The id of the ULAD app consumer association that was added.</param>
        public void AddCoborrowerToLegacyAppAndUladApp(
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> legacyAppId,
            DataObjectIdentifier<DataObjectKind.UladApplication, Guid> uladAppId,
            out DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId,
            out DataObjectIdentifier<DataObjectKind.UladApplicationConsumerAssociation, Guid> uladAppConsumerAssocId)
        {
            using (var connection = this.connectionFactory())
            {
                connection.OpenWithRetry();
                var transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                try
                {
                    var procedureName = StoredProcedureName.Create("AddCoborrowerToLegacyAppAndUladApp").Value;

                    var loanIdParam = new SqlParameter("@LoanId", this.loanId.Value);
                    var legacyAppIdParam = new SqlParameter("@LegacyAppId", legacyAppId.Value);
                    var uladAppIdParam = new SqlParameter("@UladAppId", uladAppId.Value);
                    var consumerIdParam = new SqlParameter("@ConsumerId", SqlDbType.UniqueIdentifier) { Direction = ParameterDirection.Output };
                    var uladAppConsumerIdParam = new SqlParameter("@UladAppConsumerId", SqlDbType.UniqueIdentifier) { Direction = ParameterDirection.Output };

                    var parameters = new SqlParameter[]
                    {
                        loanIdParam,
                        legacyAppIdParam,
                        uladAppIdParam,
                        uladAppConsumerIdParam,
                        consumerIdParam,
                    };

                    StoredProcedureDriverHelper.ExecuteNonQuery(connection, transaction, procedureName, parameters, TimeoutInSeconds.Default);

                    consumerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create((Guid)consumerIdParam.Value);
                    uladAppConsumerAssocId = DataObjectIdentifier<DataObjectKind.UladApplicationConsumerAssociation, Guid>.Create((Guid)uladAppConsumerIdParam.Value);

                    this.AddConsumerToOrderCollection(connection, transaction, uladAppId, consumerId);

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, ex);
                }
            }
        }

        /// <summary>
        /// Retrieve the stored procedure calls for handling the empty orderings.
        /// </summary>
        /// <returns>List of stored procedure call data.</returns>
        private List<StoredProcedureCommandData> RetrieveSaveEmptyOrdersCallData()
        {
            return this.OrderMapper.RetrieveSaveEmptyOrdersCallData();
        }

        /// <summary>
        /// Retrieve the stored procedure calls for handling the non-empty orderings.
        /// </summary>
        /// <returns>List of stored procedure call data.</returns>
        private List<StoredProcedureCommandData> RetrieveSaveOrdersCallData()
        {
            return this.OrderMapper.RetrieveSaveOrdersCallData();
        }

        /// <summary>
        /// Creates a new blank ULAD application.
        /// </summary>
        /// <param name="connection">The database connection.</param>
        /// <param name="transaction">The database transaction.</param>
        /// <param name="legacyAppId">The legacy app id.</param>
        /// <param name="uladAppData">The ULAD app that was added.</param>
        /// <param name="associationData">The association that was added.</param>
        private void CreateNewUladApplication(
            DbConnection connection,
            DbTransaction transaction,
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> legacyAppId,
            out KeyValuePair<DataObjectIdentifier<DataObjectKind.UladApplication, Guid>, UladApplication> uladAppData,
            out KeyValuePair<DataObjectIdentifier<DataObjectKind.UladApplicationConsumerAssociation, Guid>, UladApplicationConsumerAssociation> associationData)
        {
            var procedureName = StoredProcedureName.Create("CreateNewUladApplication").Value;

            var loanIdParam = new SqlParameter("@LoanId", this.loanId.Value);
            var brokerIdParam = new SqlParameter("@BrokerId", this.brokerId.Value.Value);
            var legacyAppIdParam = new SqlParameter("@LegacyAppId", legacyAppId.Value);
            var uladAppIdParam = new SqlParameter("@UladAppId", SqlDbType.UniqueIdentifier) { Direction = ParameterDirection.Output };
            var uladAppConsumerIdParam = new SqlParameter("@UladAppConsumerId", SqlDbType.UniqueIdentifier) { Direction = ParameterDirection.Output };
            var consumerIdParam = new SqlParameter("@ConsumerId", SqlDbType.UniqueIdentifier) { Direction = ParameterDirection.Output };
            var isPrimaryParam = new SqlParameter("@IsPrimary", SqlDbType.Bit) { Direction = ParameterDirection.Output };

            var parameters = new SqlParameter[]
            {
                loanIdParam,
                brokerIdParam,
                legacyAppIdParam,
                uladAppIdParam,
                uladAppConsumerIdParam,
                consumerIdParam,
                isPrimaryParam
            };

            StoredProcedureDriverHelper.ExecuteNonQuery(connection, transaction, procedureName, parameters, TimeoutInSeconds.Default);
            var uladAppId = DataObjectIdentifier<DataObjectKind.UladApplication, Guid>.Create((Guid)uladAppIdParam.Value);
            var associationId = DataObjectIdentifier<DataObjectKind.UladApplicationConsumerAssociation, Guid>.Create((Guid)uladAppConsumerIdParam.Value);
            var consumerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create((Guid)consumerIdParam.Value);
            bool isPrimary = Convert.ToBoolean(isPrimaryParam.Value);

            var uladApp = new UladApplication();
            uladApp.IsPrimary = isPrimary;
            uladAppData = new KeyValuePair<DataObjectIdentifier<DataObjectKind.UladApplication, Guid>, UladApplication>(uladAppId, uladApp);

            var association = new UladApplicationConsumerAssociation(uladAppId, consumerId, legacyAppId);
            association.IsPrimary = true;
            associationData = new KeyValuePair<DataObjectIdentifier<DataObjectKind.UladApplicationConsumerAssociation, Guid>, UladApplicationConsumerAssociation>(associationId, association);

            // The order entry is placed in the database within the stored procedure CreateNewUladApplication, so all that is needed is to make sure the in-memory order has the consumer.
            this.OrderMapper.RetrieveCollectionOrder<DataObjectKind.Consumer>(legacyApplicationId: null, consumerId: null, uladApplicationId: uladAppId).Add(consumerId);
        }

        /// <summary>
        /// Adds a ULAD app consumer association for the primary borrower on a legacy application.
        /// </summary>
        /// <param name="connection">The database connection.</param>
        /// <param name="transaction">The database transaction.</param>
        /// <param name="uladAppId">The ULAD app id.</param>
        /// <param name="legacyAppId">The legacy app id.</param>
        /// <param name="associationData">The association that was added.</param>
        private void CreateNewUladAppConsumerAssociationForLegacyAppBorrower(
            DbConnection connection,
            DbTransaction transaction,
            DataObjectIdentifier<DataObjectKind.UladApplication, Guid> uladAppId,
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> legacyAppId,
            out KeyValuePair<DataObjectIdentifier<DataObjectKind.UladApplicationConsumerAssociation, Guid>, UladApplicationConsumerAssociation> associationData)
        {
            var procedureName = StoredProcedureName.Create("ULAD_APPLICATION_X_CONSUMER_AddForPrimaryBorrowerOnLegacyApp").Value;

            var loanIdParam = new SqlParameter("@LoanId", this.loanId.Value);
            var legacyAppIdParam = new SqlParameter("@LegacyAppId", legacyAppId.Value);
            var uladAppIdParam = new SqlParameter("@UladAppId", uladAppId.Value);
            var uladAppConsumerIdParam = new SqlParameter("@UladAppConsumerId", SqlDbType.UniqueIdentifier) { Direction = ParameterDirection.Output };
            var consumerIdParam = new SqlParameter("@ConsumerId", SqlDbType.UniqueIdentifier) { Direction = ParameterDirection.Output };

            var parameters = new SqlParameter[]
            {
                loanIdParam,
                legacyAppIdParam,
                uladAppIdParam,
                uladAppConsumerIdParam,
                consumerIdParam,
            };

            StoredProcedureDriverHelper.ExecuteNonQuery(connection, transaction, procedureName, parameters, TimeoutInSeconds.Default);

            var associationId = DataObjectIdentifier<DataObjectKind.UladApplicationConsumerAssociation, Guid>.Create((Guid)uladAppConsumerIdParam.Value);
            var consumerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create((Guid)consumerIdParam.Value);

            var association = new UladApplicationConsumerAssociation(uladAppId, consumerId, legacyAppId);
            association.IsPrimary = false;
            associationData = new KeyValuePair<DataObjectIdentifier<DataObjectKind.UladApplicationConsumerAssociation, Guid>, UladApplicationConsumerAssociation>(associationId, association);

            this.AddConsumerToOrderCollection(connection, transaction, uladAppId, consumerId);
        }

        /// <summary>
        /// When a consumer is added to a ULAD application, also add it to the order for that ULAD application.
        /// </summary>
        /// <param name="connection">The database connection.</param>
        /// <param name="transaction">The database transaction.</param>
        /// <param name="uladAppId">The ULAD app id.</param>
        /// <param name="consumerId">The consumer identifier.</param>
        private void AddConsumerToOrderCollection(
            DbConnection connection,
            DbTransaction transaction,
            DataObjectIdentifier<DataObjectKind.UladApplication, Guid> uladAppId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId)
        {
            if (this.OrderMapper == null)
            {
                // I wish we didn't need to have this code, but we have methods that initialize the LqbCollectionContainer
                // without calling Load on it.  We also have methods here that change the collections without passing
                // through the LqbCollectionContainer.Save method.
                StoredProcedureName procedureName;
                IEnumerable<SqlParameter> prams;
                var loadCallData = new StoredProcedureCallSet();

                this.RetrieveOrderLoadCallData(out procedureName, out prams);
                loadCallData.Add(procedureName, prams);

                SQLQueryString query;
                IEnumerable<DbParameter> allPrams;
                loadCallData.AggregateCalls(out query, out allPrams);

                using (var reader = this.ExecuteQuery(connection, transaction, query, allPrams, TimeoutInSeconds.Sixty))
                {
                    this.FillOrders(reader);
                }
            }

            var order = this.OrderMapper.RetrieveCollectionOrder<DataObjectKind.Consumer>(legacyApplicationId: null, consumerId: null, uladApplicationId: uladAppId);
            order.Add(consumerId);
            this.OrderMapper.Save(connection, transaction);
        }
    }
}
