﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web.UI.WebControls;
using CommonProjectLib.Common.Lib;
using ConfigSystem.DataAccess;
using ConfigSystem.Engine;
using DataAccess.PathDispatch;
using LendersOffice.Admin;
using LendersOffice.Audit;
using LendersOffice.Common;
using LendersOffice.ConfigSystem;
using LendersOffice.ConfigSystem.Operations;
using LendersOffice.Constants;
using LendersOffice.Drivers.SqlServerDB;
using LendersOffice.Events;
using LendersOffice.HttpModule;
using LendersOffice.Integration.UcdDelivery;
using LendersOffice.ObjLib.BackgroundJobs.TaskAutomation;
using LendersOffice.ObjLib.HistoricalPricing;
using LendersOffice.ObjLib.LoanFileCache;
using LendersOffice.ObjLib.StatusEvents;
using LendersOffice.ObjLib.Task;
using LendersOffice.Security;
using LendersOfficeApp.los.RatePrice;
using LqbGrammar.DataTypes;
using LqbGrammar.DataTypes.PathDispatch;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace DataAccess
{
    public sealed class FieldOriginalCurrentItem
    {
        public string Original;
        public string Current;
    }
    public enum E_DataState
    {
        Normal = 0,
        InitLoad = 1,
        InitSave = 2
    };

    public partial class CPageBase : CBase,
        IGetSymbolTable,
        IAmortizationDataProvider,
        IShimLoanDataProvider,
        ILoanLqbCollectionContainerLoanDataProvider,
        ILoanLqbCollectionBorrowerManagement,
        IUladDataLayerMigrationLoanDataProvider,
        IHaveReadableDependencies
    {
        private bool isWarehouseChecked = false;

        /// <summary>
        /// Initializes member variables based on the given data and sets the data state to <see cref="E_DataState.InitLoad"/>.
        /// </summary>
        /// <param name="loan">The loan data.</param>
        /// <param name="apps">The application data.</param>
        /// <param name="assignedEmployees">The employee data.</param>
        /// <param name="assignedTeams">The team data.</param>
        /// <param name="submissionSnapshots">The submission snapshot data.</param>
        private void PickupLoadedDataReadOnly(Dictionary<string, object> loan, List<Dictionary<string, object>> apps, List<Dictionary<string, object>> assignedEmployees, List<Dictionary<string, object>> assignedTeams, List<Dictionary<string, object>> submissionSnapshots)
        {
            this.m_rowLoan = new CacheContainer(new DictionaryContainer(loan));

            if (null != apps)
            {
                int appCount = apps.Count;
                this.m_dataApps = new CAppBase[appCount];
                for (int iApp = 0; iApp < appCount; ++iApp)
                {
                    this.m_dataApps[iApp] = new CAppBase(this, this.m_fileId, new CacheContainer(new DictionaryContainer(apps[iApp])), this.m_namePage);
                }
            }

            if (null != assignedEmployees)
            {
                int employeeCount = assignedEmployees.Count;
                for (int iEmployee = 0; iEmployee < employeeCount; ++iEmployee)
                {
                    Dictionary<string,object> row = assignedEmployees[iEmployee];
                    CEmployeeFields employee = new CEmployeeFields(new DictionaryContainer(row));
                    switch (employee.Role)
                    {
                        case CEmployeeFields.E_Role.CallCenterAgent:
                            this.m_employeeCallCenterAgent = employee; break;
                        case CEmployeeFields.E_Role.LoanRep:
                            this.m_employeeLoanRep = employee; break;
                        case CEmployeeFields.E_Role.Manager:
                            this.m_employeeManager = employee; break;
                        case CEmployeeFields.E_Role.Processor:
                            this.m_employeeProcessor = employee; break;
                        case CEmployeeFields.E_Role.RealEstateAgent:
                            this.m_employeeRealEstateAgent = employee; break;
                        case CEmployeeFields.E_Role.LenderAccountExec:
                            this.m_employeeLenderAccExec = employee; break;
                        case CEmployeeFields.E_Role.LockDesk:
                            this.m_employeeLockDesk = employee; break;
                        case CEmployeeFields.E_Role.Underwriter:
                            this.m_employeeUnderwriter = employee; break;
                        case CEmployeeFields.E_Role.LoanOpener:
                            this.m_employeeLoanOpener = employee; break;
                        case CEmployeeFields.E_Role.Closer:
                            this.m_employeeCloser = employee; break;
                        case CEmployeeFields.E_Role.BrokerProcessor:
                            this.m_employeeBrokerProcessor = employee; break;
                        case CEmployeeFields.E_Role.Shipper:
                            this.m_employeeShipper = employee; break;
                        case CEmployeeFields.E_Role.Funder:
                            this.m_employeeFunder = employee; break;
                        case CEmployeeFields.E_Role.PostCloser:
                            this.m_employeePostCloser = employee; break;
                        case CEmployeeFields.E_Role.Insuring:
                            this.m_employeeInsuring = employee; break;
                        case CEmployeeFields.E_Role.CollateralAgent:
                            this.m_employeeCollateralAgent = employee; break;
                        case CEmployeeFields.E_Role.DocDrawer:
                            this.m_employeeDocDrawer = employee; break;
                        case CEmployeeFields.E_Role.CreditAuditor:
                            this.m_employeeCreditAuditor = employee; break;
                        case CEmployeeFields.E_Role.DisclosureDesk:
                            this.m_employeeDisclosureDesk = employee; break;
                        case CEmployeeFields.E_Role.JuniorProcessor:
                            this.m_employeeJuniorProcessor = employee; break;
                        case CEmployeeFields.E_Role.JuniorUnderwriter:
                            this.m_employeeJuniorUnderwriter = employee; break;
                        case CEmployeeFields.E_Role.LegalAuditor:
                            this.m_employeeLegalAuditor = employee; break;
                        case CEmployeeFields.E_Role.LoanOfficerAssistant:
                            this.m_employeeLoanOfficerAssistant = employee; break;
                        case CEmployeeFields.E_Role.Purchaser:
                            this.m_employeePurchaser = employee; break;
                        case CEmployeeFields.E_Role.QCCompliance:
                            this.m_employeeQCCompliance = employee; break;
                        case CEmployeeFields.E_Role.Secondary:
                            this.m_employeeSecondary = employee; break;
                        case CEmployeeFields.E_Role.Servicing:
                            this.m_employeeServicing = employee; break;
                        case CEmployeeFields.E_Role.ExternalSecondary:
                            this.m_employeeExternalSecondary = employee; break;
                        case CEmployeeFields.E_Role.ExternalPostCloser:
                            this.m_employeeExternalPostCloser = employee; break;
                    }
                }
            }

            if (null != assignedTeams)
            {
                int teamCount = assignedTeams.Count;
                for (int iTeam = 0; iTeam < teamCount; ++iTeam)
                {
                    Dictionary<string,object> row = assignedTeams[iTeam];
                    CTeamFields team = new CTeamFields(new DictionaryContainer(row));
                    switch (team.Role)
                    {
                        case CEmployeeFields.E_Role.CallCenterAgent:
                            this.m_teamCallCenterAgent = team; break;
                        case CEmployeeFields.E_Role.LoanRep:
                            this.m_teamLoanRep = team; break;
                        case CEmployeeFields.E_Role.Manager:
                            this.m_teamManager = team; break;
                        case CEmployeeFields.E_Role.Processor:
                            this.m_teamProcessor = team; break;
                        case CEmployeeFields.E_Role.RealEstateAgent:
                            this.m_teamRealEstateAgent = team; break;
                        case CEmployeeFields.E_Role.LenderAccountExec:
                            this.m_teamLenderAccExec = team; break;
                        case CEmployeeFields.E_Role.LockDesk:
                            this.m_teamLockDesk = team; break;
                        case CEmployeeFields.E_Role.Underwriter:
                            this.m_teamUnderwriter = team; break;
                        case CEmployeeFields.E_Role.LoanOpener:
                            this.m_teamLoanOpener = team; break;
                        case CEmployeeFields.E_Role.Closer:
                            this.m_teamCloser = team; break;
                        case CEmployeeFields.E_Role.Shipper:
                            this.m_teamShipper = team; break;
                        case CEmployeeFields.E_Role.Funder:
                            this.m_teamFunder = team; break;
                        case CEmployeeFields.E_Role.PostCloser:
                            this.m_teamPostCloser = team; break;
                        case CEmployeeFields.E_Role.Insuring:
                            this.m_teamInsuring = team; break;
                        case CEmployeeFields.E_Role.CollateralAgent:
                            this.m_teamCollateralAgent = team; break;
                        case CEmployeeFields.E_Role.DocDrawer:
                            this.m_teamDocDrawer = team; break;
                        case CEmployeeFields.E_Role.CreditAuditor:
                            this.m_teamCreditAuditor = team; break;
                        case CEmployeeFields.E_Role.DisclosureDesk:
                            this.m_teamDisclosureDesk = team; break;
                        case CEmployeeFields.E_Role.JuniorProcessor:
                            this.m_teamJuniorProcessor = team; break;
                        case CEmployeeFields.E_Role.JuniorUnderwriter:
                            this.m_teamJuniorUnderwriter = team; break;
                        case CEmployeeFields.E_Role.LegalAuditor:
                            this.m_teamLegalAuditor = team; break;
                        case CEmployeeFields.E_Role.LoanOfficerAssistant:
                            this.m_teamLoanOfficerAssistant = team; break;
                        case CEmployeeFields.E_Role.Purchaser:
                            this.m_teamPurchaser = team; break;
                        case CEmployeeFields.E_Role.QCCompliance:
                            this.m_teamQCCompliance = team; break;
                        case CEmployeeFields.E_Role.Secondary:
                            this.m_teamSecondary = team; break;
                        case CEmployeeFields.E_Role.Servicing:
                            this.m_teamServicing = team; break;
                    }
                }
            }

            if (submissionSnapshots != null)
            {
                this.m_submissionSnapshots = submissionSnapshots
                    .Select(objectDictionary => new SubmissionSnapshot(new DictionaryContainer(objectDictionary)));
            }

            this.m_dataState = E_DataState.InitLoad;
        }

        /// <summary>
        /// Adds the fields involved in the RESPA 6 to the set of potentially tracked fields.
        /// </summary>
        private void AddRespaFieldsForTracking()
        {
            var fieldIds = new[]
            {
                nameof(CAppBase.aBNm),
                nameof(CAppBase.aBSsn),
                nameof(CAppBase.aCNm),
                nameof(CAppBase.aCSsn),
                nameof(sLTotI),
                nameof(sSpAddr),
                nameof(sSpAddrTBD),
                nameof(sApprVal),
                nameof(sPurchPrice),
                nameof(sLAmtCalc),
            };

            this.AddFieldsToTrack(fieldIds);
        }

        /// <summary>
        /// Builds the set of fields to include in the field snapshot, determines the additional
        /// fields that will need to be loaded, and loads the data.
        /// </summary>
        /// <exception cref="GenericUserErrorMessageException">
        /// Thrown when the file is read-only or when a legacy GFE or CoC archive has been applied.
        /// </exception>
        /// <exception cref="CBaseException">Thrown when called with a <see cref="E_DataState.InitSave"/> data state.</exception>
        private void InitSavePrivate()
        {
            if (this.HasGFEorCoCArchiveBeenApplied)
            {
                throw new GenericUserErrorMessageException("Programming Error:  Can't call Save on a loan once an archived GFE has been applied");
            }

            if (this.IsReadOnly)
            {
                throw new GenericUserErrorMessageException("Programming Error: InitSave() cannot get call when IsReadOnly= true");
            }

            if (E_DataState.Normal != this.m_dataState && E_DataState.InitLoad != this.m_dataState)
            {
                throw new CBaseException(ErrorMessages.Generic, "Programming Error:  InitSave() is called when datastate = " + this.m_dataState.ToString("D"));
            }

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            this.savedStatusEvent = false;

            if (ConstStage.LoadTriggerFieldDependenciesAllTheTime == false && this.IsCachingUtilityObj == false)
            {
                // This will append the disclosure fields. 
                this.m_selectProvider = CSelectStatementProvider.GetProviderFor(
                    E_ReadDBScenarioType.InputDirtyFields_NeedTargetAndTriggerFields,
                    this.m_selectProvider.FieldSet.ToArray());
            }

            this.AddFieldsToTrack(TrackedField.Retrieve().Keys);
            this.AddFieldsToTrack(this.ListTriggerFieldList());
            this.AddFieldsToTrack(this.DisclosureFieldChangeTriggerFields);
            this.AddFieldsToTrack(this.DisclosureWorkflowTriggerFields);

            var nmlsCallReportfields = new List<string>() {"sNMLSApplicationAmount", "sNMLSApplicationAmountLckd"};
            this.AddFieldsToTrack(nmlsCallReportfields);

            this.AddFieldsToTrack(ConstApp.TridAutoMigrationTriggerFieldIds);
            this.AddFieldsToTrack(new List<string>() { "sClosingCostFeeVersionT", nameof(sUniversalLoanIdentifier) });

            if (this.m_loanfileFieldValidator != null)
            {
                this.AddFieldsToTrack(this.m_loanfileFieldValidator.GetProtectedFields());
            }

            this.AddRespaFieldsForTracking();
            this.AddFieldsToTrack(new[] { nameof(sAdjustmentListJson) });

            CFieldInfoTable fieldInfoTable = CFieldInfoTable.GetInstance();

            HashSet<string> additionalInputFields = null;

            if (this.IsCachingUtilityObj == false)
            {
                var dependenciesByTrackedFieldId = new Dictionary<string, HashSet<string>>(StringComparer.OrdinalIgnoreCase);

                using (PerformanceMonitorItem.Time("GetMasterListOfPossibleFieldsToSaveSnapshot"))
                {
                    foreach (string trackedFieldId in this.GetMasterListOfPossibleFieldsToSaveSnapshot())
                    {
                        if (dependenciesByTrackedFieldId.ContainsKey(trackedFieldId) == false)
                        {
                            var fieldDependencies = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
                            foreach (FieldSpec dependency in fieldInfoTable.GatherRequiredIndepFields(new string[] { trackedFieldId }).DbFields.GetFieldSpecList())
                            {
                                fieldDependencies.Add(dependency.Name);
                            }

                            dependenciesByTrackedFieldId.Add(trackedFieldId, fieldDependencies);
                        }
                    }
                }

                additionalInputFields = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

                foreach (string trackedFieldId in dependenciesByTrackedFieldId.Keys)
                {
                    if (dependenciesByTrackedFieldId[trackedFieldId].Overlaps(this.m_selectProvider.FieldSet))
                    {
                        additionalInputFields.UnionWith(dependenciesByTrackedFieldId[trackedFieldId]);
                        this.AddFieldsToKeepTrackOfSnapshot(trackedFieldId);
                    }
                }

                using (PerformanceMonitorItem.Time("TriggerDependencyFieldDictionary for LFF_Core InitSavePrivate"))
                {
                    // Opm 103760 The trigger field dependency dictionary contains a list of fields used in a trigger 
                    // it does not include the dependent fields. Additional Input fields has the same issue. 
                    // m_selectprovider.fieldlist also only contains the used fields and not the roots.  
                    var rootFields = new HashSet<string>(
                        fieldInfoTable.GatherRequiredIndepFields(this.m_selectProvider.FieldSet).DbFields.StringCollection);

                    // Loop Through Trigger list to see which trigger could affect by this InitSave.
                    foreach (string triggerName in this.TriggerDependencyFieldDictionary.Keys)
                    {
                        // Compare the DB roots the trigger/field set fields use, otherwise if a trigger uses abnm
                        // and the page uses abfirstnm the trigger wont be included.
                        IEnumerable<string> triggerRootFields = fieldInfoTable.GatherRequiredIndepFields(
                            this.TriggerDependencyFieldDictionary[triggerName]).DbFields.StringCollection;

                        if (rootFields.Overlaps(triggerRootFields))
                        {
                            additionalInputFields.UnionWith(this.TriggerDependencyFieldDictionary[triggerName]);
                            foreach (var field in this.TriggerDependencyFieldDictionary[triggerName])
                            {
                                this.AddFieldsToKeepTrackOfSnapshot(field);
                            }
                        }
                    }
                }

                // 10/30/2013 gf - opm 142462 This field always needs to be included,
                // it is referenced in AddToComplianceEaseQueue in LFF. This was done
                // do avoid unnecessarily adding the field for InitLoad().
                // 10/11/2014 BB - Ditto for ComplianceEagle, and for additional fields to confine both compliance queues to valid loan files.
                additionalInputFields.UnionWith(new List<string>() { "sIsQuickPricerLoan", "sLoanFileT", "IsTemplate", "IsValid", "sStatusT" } );

                // 3/18/2014 dd - OPM 144112 - Gather dependency fields necessary for perform PreSave Validation check.
                ExecutingEngine executingEngine = this.GetBrokerExecutingEngine();

                if (executingEngine != null)
                {
                    if (executingEngine.HasOperation(WorkflowOperations.PreSaveValidation))
                    {
                        using (PerformanceMonitorItem.Time("CPageBase.InitSavePrivate - OPERATION_PRESAVE_VALIDATION"))
                        {
                            var preSaveValidationFields = executingEngine.GetDependencyVariableList(new[] { WorkflowOperations.PreSaveValidation });
                            foreach (string fieldId in preSaveValidationFields)
                            {
                                string fieldIdToUse = fieldId;
                                if (fieldId.Equals(ParameterReporting.LEAD_SOURCE_ENUM_FIELD_ID, StringComparison.OrdinalIgnoreCase))
                                {
                                    fieldIdToUse = ParameterReporting.LEAD_SOURCE_ENUM_REMAP_ID;
                                }

                                if (fieldId.Equals(ParameterReporting.STATUS_PROGRESS_ENUM_FIELD_ID, StringComparison.OrdinalIgnoreCase))
                                {
                                    fieldIdToUse = ParameterReporting.STATUS_PROGRESS_ENUM_REMAP_ID;
                                }

                                if (fieldId.StartsWith(DataPath.PathHeader, StringComparison.OrdinalIgnoreCase))
                                {
                                    IEnumerable<string> dependencies = PathResolver.GetPathDependencyFields(fieldId);

                                    this.AddFieldsToKeepTrackOfSnapshot(dependencies);
                                    additionalInputFields.UnionWith(dependencies);
                                }
                                else
                                {
                                    this.AddFieldsToKeepTrackOfSnapshot(fieldIdToUse);
                                    additionalInputFields.Add(fieldIdToUse);
                                }
                            }
                        }
                    }
                }
            }

            var factory = DbAccessUtils.GetDbProvider();
            this.m_cmdUpdateLoan = factory.CreateCommand();
            this.m_cmdUpdateApp = factory.CreateCommand(); 
            this.m_cmdUpdateCache = factory.CreateCommand();

            this.m_cmdUpdateLoan.Parameters.Add(CPageBase.sLId_OrigParam());
            this.m_cmdUpdateApp.Parameters.Add(CAppBase.aAppId_OrigParam());
            this.m_cmdUpdateCache.Parameters.Add(CPageBase.sLId_OrigParam());

            this.m_dirtyFieldsLoanA = new StringCollection();
            this.m_dirtyFieldsLoanB = new StringCollection();
            this.m_dirtyFieldsLoanC = new StringCollection();
            this.m_dirtyFieldsLoanD = new StringCollection();
            this.m_dirtyFieldsLoanE = new StringCollection();
            this.m_dirtyFieldsLoanF = new StringCollection();
            this.m_dirtyFieldsTrushAcc = new StringCollection();
            this.m_dirtyFieldsAppA = new StringCollection();
            this.m_dirtyFieldsAppB = new StringCollection();
            this.m_dirtyFieldsLoanCache = new StringCollection();
            this.m_dirtyFieldsLoanCache2 = new StringCollection();
            this.m_dirtyFieldsLoanCache3 = new StringCollection();
            this.m_dirtyFieldsLoanCache4 = new StringCollection();
            this.m_dirtyFieldsEmployeeAssignment = new NameValueCollection();
            this.m_dirtyFieldsTeamAssignment = new NameValueCollection();

            this.ClearAllDbRelatedData();
            
            Exception e = null;
            for (int i = 0; i < 6; ++i)
            {
                try
                {
                    using (PerformanceMonitorItem.Time("CPageBase.__InitSaveCore"))
                    {
                        this.__InitSaveCore(additionalInputFields);
                    }

                    return;
                }
                catch (VersionMismatchException)
                {
                    throw;
                }
                catch (LoanNotFoundException)
                {
                    throw; // 8/26/2009 dd - No need to retry if loan does not exist.
                }
                catch (AccessDenied)
                {
                    throw; // 7/31/2014 sk - No need to retry if they simply can't save.
                }
                catch (Exception exc)
                {
                    if (null == e)
                    {
                        e = exc;
                    }

                    this.LogWarning(string.Format("Failed to executing InitSave(), retry count ={0}", i.ToString())
                        , exc);

                    Tools.SleepAndWakeupRandomlyWithin(1000, 4500);
                }
                finally
                {
                    stopwatch.Stop();
                    PerformanceMonitorItem monitorItem = PerformanceMonitorItem.Current;
                    
                    if (null != monitorItem)
                    {
                        string name = this.m_namePage;
                        if (string.IsNullOrEmpty(name))
                        {
                            name = this.ToString();
                        }

                        string log = name + ".InitSave()";
                        monitorItem.AddTimingDetail(log, stopwatch.ElapsedMilliseconds);
                    }
                }
            }

            this.LogErrorAndSendMail("Failed to executing InitSave(), after 5 retries", e);
            throw e;
        }

        /// <summary>
        /// Loads the needed data from the database, initializes members, takes the initial field
        /// tracking snapshot, and puts the file into the <see cref="E_DataState.InitSave"/> state.
        /// </summary>
        /// <param name="additionalInputFields">
        /// Extra fields that need to be loaded in addition to the base required fields. Some examples
        /// of how fields get included here: field tracking, task triggers, workflow. There are also
        /// some fields that are always loaded when performing an <see cref="InitSave"/>.
        /// </param>
        /// <exception cref="LoanNotFoundException">Thrown when the loan was not found.</exception>
        /// <exception cref="AccessDenied">
        /// Thrown when called on a QuickPricer 2.0 file without the <see cref="AllowSaveWhileQP2Sandboxed"/>
        /// flag set.
        /// </exception>
        /// <exception cref="VersionMismatchException">
        /// Thrown when the <see cref="sExpectedFileVersion"/> does not match the value of
        /// <see cref="sFileVersion"/> from the database.
        /// </exception>
        private void __InitSaveCore(HashSet<string> additionalInputFields)
        {
            using (PerformanceStopwatch.Start("CPageBase._InitSaveCore"))
            {
                DbProviderFactory factory = DbAccessUtils.GetDbProvider();
                DbTransaction transaction = null;
                DbConnection connection = this.GetConnectionForReadOnly();

                try
                {
                    var sqlSelectItem = this.m_selectProvider.GenerateSelectStatement(additionalInputFields);

                    string loanQuery = sqlSelectItem.LoanSelectFormat;
                    string appQuery = sqlSelectItem.AppSelectFormat;
                    string assignedEmployeeQuery = sqlSelectItem.AssignedEmployeeSelectFormat;
                    string assignedTeamQuery = sqlSelectItem.AssignedTeamSelectFormat;
                    string submissionSnapshotQuery = sqlSelectItem.SubmissionSnapshotSelectFormat;

                    bool hasLoanQuery = loanQuery != "";
                    bool hasAppQuery = appQuery != "";
                    bool hasEmployeeQuery = assignedEmployeeQuery != "";
                    bool hasTeamQuery = assignedTeamQuery != "";
                    bool hasSubmissionSnapshotQuery = submissionSnapshotQuery != "";

                    this.includeUcdDeliveryCollection = sqlSelectItem.IncludeUcdDeliveryCollection;

                    this.InitializeLoanLqbCollectionContainer();

                    this.loanLqbCollectionContainer.RegisterCollections(sqlSelectItem.RegisteredLqbCollections);

                    if (hasLoanQuery || hasAppQuery || hasEmployeeQuery || hasTeamQuery || hasSubmissionSnapshotQuery || this.includeUcdDeliveryCollection || this.loanLqbCollectionContainer.HasRegisteredCollections)
                    {
                        if (hasLoanQuery)
                        {
                            this.m_dsLoan = new DataSet("LOAN_FILE");
                        }

                        if (hasAppQuery)
                        {
                            this.m_dsApp = new DataSet("APPLICATION");
                        }

                        if (hasEmployeeQuery)
                        {
                            this.m_dsEmployee = new DataSet("EMPLOYEE");
                        }

                        if (hasTeamQuery)
                        {
                            this.m_dsTeam = new DataSet("TEAM");
                        }

                        if (hasSubmissionSnapshotQuery)
                        {
                            this.m_dsSubmissionSnapshots = new DataSet("SUBMISSION_SNAPSHOT");
                        }

                        connection.Open();
                        transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);

                        if (this.m_dsLoan != null)
                        {
                            this.m_daLoan = factory.CreateDataAdapter();
                            this.m_daLoan.SelectCommand = connection.CreateCommand();
                            this.m_daLoan.SelectCommand.CommandType = CommandType.Text;
                            this.m_daLoan.SelectCommand.CommandText = loanQuery;
                            this.m_daLoan.SelectCommand.Parameters.Add(new SqlParameter("@sLId", this.sLId));
                            this.m_daLoan.SelectCommand.Transaction = transaction;

                            Stopwatch sw = Stopwatch.StartNew();
                            this.m_daLoan.Fill(this.m_dsLoan, "LOAN_FILE");
                            sw.Stop();
                            PerformanceMonitorItem.AddTimingDetailsToCurrent("sql:CPageBase._InitSaveCore::LOAN_FILE", sw.ElapsedMilliseconds);
                        }

                        if (this.m_dsApp != null)
                        {
                            this.m_daApp = factory.CreateDataAdapter();
                            this.m_daApp.SelectCommand = connection.CreateCommand();
                            this.m_daApp.SelectCommand.CommandText = appQuery;
                            this.m_daApp.SelectCommand.CommandType = CommandType.Text;
                            this.m_daApp.SelectCommand.Parameters.Add(new SqlParameter("@sLId", this.sLId));
                            this.m_daApp.SelectCommand.Transaction = transaction;

                            Stopwatch sw = Stopwatch.StartNew();
                            this.m_daApp.Fill(this.m_dsApp, "APPLICATION");
                            sw.Stop();
                            PerformanceMonitorItem.AddTimingDetailsToCurrent("sql:CPageBase._InitSaveCore::APPLICATION", sw.ElapsedMilliseconds);
                        }

                        if (this.m_dsEmployee != null)
                        {
                            this.m_daEmployee = factory.CreateDataAdapter();
                            this.m_daEmployee.SelectCommand = connection.CreateCommand();
                            this.m_daEmployee.SelectCommand.CommandText = assignedEmployeeQuery;
                            this.m_daEmployee.SelectCommand.CommandType = CommandType.Text;
                            this.m_daEmployee.SelectCommand.Parameters.Add(new SqlParameter("@sLId", this.sLId));
                            this.m_daEmployee.SelectCommand.Transaction = transaction;

                            Stopwatch sw = Stopwatch.StartNew();
                            this.m_daEmployee.Fill(this.m_dsEmployee, "EMPLOYEE");
                            sw.Stop();
                            PerformanceMonitorItem.AddTimingDetailsToCurrent("sql:CPageBase._InitSaveCore::EMPLOYEE", sw.ElapsedMilliseconds);
                        }

                        if (this.m_dsTeam != null)
                        {
                            this.m_daTeam = factory.CreateDataAdapter();
                            this.m_daTeam.SelectCommand = connection.CreateCommand();
                            this.m_daTeam.SelectCommand.CommandText = assignedTeamQuery;
                            this.m_daTeam.SelectCommand.CommandType = CommandType.Text;
                            this.m_daTeam.SelectCommand.Parameters.Add(new SqlParameter("@sLId", this.sLId));
                            this.m_daTeam.SelectCommand.Transaction = transaction;

                            Stopwatch sw = Stopwatch.StartNew();
                            this.m_daTeam.Fill(this.m_dsTeam, "TEAM");
                            sw.Stop();
                            PerformanceMonitorItem.AddTimingDetailsToCurrent("sql:CPageBase._InitSaveCore::TEAM", sw.ElapsedMilliseconds);
                        }

                        if (this.m_dsSubmissionSnapshots != null)
                        {
                            this.m_daSubmissionSnapshot = factory.CreateDataAdapter();
                            this.m_daSubmissionSnapshot.SelectCommand = connection.CreateCommand();
                            this.m_daSubmissionSnapshot.SelectCommand.CommandText = submissionSnapshotQuery;
                            this.m_daSubmissionSnapshot.SelectCommand.CommandType = CommandType.Text;
                            this.m_daSubmissionSnapshot.SelectCommand.Parameters.Add(new SqlParameter("@sLId", this.sLId));
                            this.m_daSubmissionSnapshot.SelectCommand.Transaction = transaction;

                            Stopwatch sw = Stopwatch.StartNew();
                            this.m_daSubmissionSnapshot.Fill(this.m_dsSubmissionSnapshots, "SUBMISSION_SNAPSHOT");
                            sw.Stop();
                            PerformanceMonitorItem.AddTimingDetailsToCurrent("sql:CPageBase._InitSaveCore::SUBMISSION_SNAPSHOT", sw.ElapsedMilliseconds);
                        }

                        // OPM 457741 - If included in dependency graph, load UCD Delivery Collection into loan file at loan initialization.
                        if (this.includeUcdDeliveryCollection
                            && (this.m_dsLoan?.Tables["LOAN_FILE"]?.Rows?.Count ?? -1) > 0)
                        {
                            Guid brokerId = (Guid)this.m_dsLoan.Tables["LOAN_FILE"].Rows[0]["sBrokerId"];
                            this.xUcdDeliveryCollection = UcdDeliveryCollection.Load(this.sLId, brokerId, connection, transaction);
                            this.m_dirtyFields[nameof(CPageBase.sUcdDeliveryCollection)] = null;    // Mark as dirty to trigger cache update.
                        }

                        if (ConstStage.EnableUladCollectionLoading
                            && this.loanLqbCollectionContainer.HasRegisteredCollections
                            && ShouldUseLqbCollectionContainer(
                                this.m_dsLoan.Tables["LOAN_FILE"].Rows[0]["sBorrowerApplicationCollectionT"].AsDbNullable<E_sLqbCollectionT>()))
                        {
                            Guid brokerId = (Guid)this.m_dsLoan.Tables["LOAN_FILE"].Rows[0]["sBrokerId"];

                            this.loanLqbCollectionContainer.Load(
                                DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(brokerId),
                                connection,
                                transaction);

                            this.loanLqbCollectionContainer.BeginTrackingChanges();
                        }

                        transaction.Commit();
                        transaction = null;

                        connection.Close();
                        connection = null;

                        if (this.m_dsLoan != null)
                        {
                            if (this.m_dsLoan.Tables["LOAN_FILE"].Rows.Count == 0)
                            {
                                throw new LoanNotFoundException(this.m_fileId);
                            }

                            DataRow row = this.m_dsLoan.Tables["LOAN_FILE"].Rows[0];
                            var isForQP2LoanCreationProcess =CurrentRequestOrThreadCache.GetRequestOrThreadObjectByName(GeneratedKeyForRequestOrThreadCache.IsForQP2LoanCreationProcess());
                            if (false == this.AllowSaveWhileQP2Sandboxed
                                && (null == isForQP2LoanCreationProcess || false == (bool)isForQP2LoanCreationProcess)
                                && false == this.IsCachingUtilityObj
                                && row.Table.Columns.Contains("sLoanFileT")
                                && row["sLoanFileT"] != DBNull.Value
                                && E_sLoanFileT.QuickPricer2Sandboxed == (E_sLoanFileT)row["sLoanFileT"])
                            {
                                var msg = "You may not update the quickpricer 2.0 loan in this way.";
                                throw new AccessDenied(msg, msg);
                            }

                            this.m_rowLoan = new DataRowContainer(row);
                        }

                        // 7/30/2010 dd - Perform version mismatch here. If version from db does not match 
                        // with expected version then bail early.
                        if (this.IsEnforceFileVersionCheck)
                        {
                            if (this.sExpectedFileVersion != this.sFileVersion)
                            {
                                throw new VersionMismatchException();
                            }
                        }

                        if (this.m_dsApp != null)
                        {
                            DataRowCollection rowsApp = this.m_dsApp.Tables["APPLICATION"].Rows;
                            int appCount = rowsApp.Count;
                            this.m_dataApps = new CAppBase[appCount];
                            for (int i = 0; i < appCount; ++i)
                            {
                                this.m_dataApps[i] = new CAppBase(this, this.m_fileId, new DataRowContainer(rowsApp[i]), this.m_namePage);
                            }
                        }

                        if (this.m_dsEmployee != null)
                        {
                            DataRowCollection rowsEmployee = this.m_dsEmployee.Tables["EMPLOYEE"].Rows;
                            int employeeCount = rowsEmployee.Count;

                            for (int iEmployee = 0; iEmployee < employeeCount; ++iEmployee)
                            {
                                DataRow row = rowsEmployee[iEmployee];
                                CEmployeeFields employee = new CEmployeeFields(new DataRowContainer(row));
                                switch (employee.Role)
                                {
                                    // TODO: This code also shows up in the PickupLoadedData
                                    case CEmployeeFields.E_Role.CallCenterAgent:
                                        this.m_employeeCallCenterAgent = employee; break;
                                    case CEmployeeFields.E_Role.LoanRep:
                                        this.m_employeeLoanRep = employee; break;
                                    case CEmployeeFields.E_Role.Manager:
                                        this.m_employeeManager = employee; break;
                                    case CEmployeeFields.E_Role.Processor:
                                        this.m_employeeProcessor = employee; break;
                                    case CEmployeeFields.E_Role.RealEstateAgent:
                                        this.m_employeeRealEstateAgent = employee; break;
                                    case CEmployeeFields.E_Role.LenderAccountExec:
                                        this.m_employeeLenderAccExec = employee; break;
                                    case CEmployeeFields.E_Role.LockDesk:
                                        this.m_employeeLockDesk = employee; break;
                                    case CEmployeeFields.E_Role.Underwriter:
                                        this.m_employeeUnderwriter = employee; break;
                                    case CEmployeeFields.E_Role.LoanOpener:
                                        this.m_employeeLoanOpener = employee; break;
                                    case CEmployeeFields.E_Role.Closer:
                                        this.m_employeeCloser = employee; break;
                                    case CEmployeeFields.E_Role.BrokerProcessor:
                                        this.m_employeeBrokerProcessor = employee; break;
                                    case CEmployeeFields.E_Role.Shipper:
                                        this.m_employeeShipper = employee; break;
                                    case CEmployeeFields.E_Role.Funder:
                                        this.m_employeeFunder = employee; break;
                                    case CEmployeeFields.E_Role.PostCloser:
                                        this.m_employeePostCloser = employee; break;
                                    case CEmployeeFields.E_Role.Insuring:
                                        this.m_employeeInsuring = employee; break;
                                    case CEmployeeFields.E_Role.CollateralAgent:
                                        this.m_employeeCollateralAgent = employee; break;
                                    case CEmployeeFields.E_Role.DocDrawer:
                                        this.m_employeeDocDrawer = employee; break;
                                    case CEmployeeFields.E_Role.CreditAuditor:
                                        this.m_employeeCreditAuditor = employee; break;
                                    case CEmployeeFields.E_Role.DisclosureDesk:
                                        this.m_employeeDisclosureDesk = employee; break;
                                    case CEmployeeFields.E_Role.JuniorProcessor:
                                        this.m_employeeJuniorProcessor = employee; break;
                                    case CEmployeeFields.E_Role.JuniorUnderwriter:
                                        this.m_employeeJuniorUnderwriter = employee; break;
                                    case CEmployeeFields.E_Role.LegalAuditor:
                                        this.m_employeeLegalAuditor = employee; break;
                                    case CEmployeeFields.E_Role.LoanOfficerAssistant:
                                        this.m_employeeLoanOfficerAssistant = employee; break;
                                    case CEmployeeFields.E_Role.Purchaser:
                                        this.m_employeePurchaser = employee; break;
                                    case CEmployeeFields.E_Role.QCCompliance:
                                        this.m_employeeQCCompliance = employee; break;
                                    case CEmployeeFields.E_Role.Secondary:
                                        this.m_employeeSecondary = employee; break;
                                    case CEmployeeFields.E_Role.Servicing:
                                        this.m_employeeServicing = employee; break;
                                    case CEmployeeFields.E_Role.ExternalSecondary:
                                        this.m_employeeExternalSecondary = employee; break;
                                    case CEmployeeFields.E_Role.ExternalPostCloser:
                                        this.m_employeeExternalPostCloser = employee; break;
                                }
                            }
                        }

                        if (this.m_dsTeam != null)
                        {
                            DataRowCollection rowsTeam = this.m_dsTeam.Tables["TEAM"].Rows;
                            int teamCount = rowsTeam.Count;

                            for (int iTeam = 0; iTeam < teamCount; ++iTeam)
                            {
                                DataRow row = rowsTeam[iTeam];
                                CTeamFields team = new CTeamFields(new DataRowContainer(row));
                                switch (team.Role)
                                {
                                    case CEmployeeFields.E_Role.CallCenterAgent:
                                        this.m_teamCallCenterAgent = team; break;
                                    case CEmployeeFields.E_Role.LoanRep:
                                        this.m_teamLoanRep = team; break;
                                    case CEmployeeFields.E_Role.Manager:
                                        this.m_teamManager = team; break;
                                    case CEmployeeFields.E_Role.Processor:
                                        this.m_teamProcessor = team; break;
                                    case CEmployeeFields.E_Role.RealEstateAgent:
                                        this.m_teamRealEstateAgent = team; break;
                                    case CEmployeeFields.E_Role.LenderAccountExec:
                                        this.m_teamLenderAccExec = team; break;
                                    case CEmployeeFields.E_Role.LockDesk:
                                        this.m_teamLockDesk = team; break;
                                    case CEmployeeFields.E_Role.Underwriter:
                                        this.m_teamUnderwriter = team; break;
                                    case CEmployeeFields.E_Role.LoanOpener:
                                        this.m_teamLoanOpener = team; break;
                                    case CEmployeeFields.E_Role.Closer:
                                        this.m_teamCloser = team; break;
                                    case CEmployeeFields.E_Role.Shipper:
                                        this.m_teamShipper = team; break;
                                    case CEmployeeFields.E_Role.Funder:
                                        this.m_teamFunder = team; break;
                                    case CEmployeeFields.E_Role.PostCloser:
                                        this.m_teamPostCloser = team; break;
                                    case CEmployeeFields.E_Role.Insuring:
                                        this.m_teamInsuring = team; break;
                                    case CEmployeeFields.E_Role.CollateralAgent:
                                        this.m_teamCollateralAgent = team; break;
                                    case CEmployeeFields.E_Role.DocDrawer:
                                        this.m_teamDocDrawer = team; break;
                                    case CEmployeeFields.E_Role.CreditAuditor:
                                        this.m_teamCreditAuditor = team; break;
                                    case CEmployeeFields.E_Role.DisclosureDesk:
                                        this.m_teamDisclosureDesk = team; break;
                                    case CEmployeeFields.E_Role.JuniorProcessor:
                                        this.m_teamJuniorProcessor = team; break;
                                    case CEmployeeFields.E_Role.JuniorUnderwriter:
                                        this.m_teamJuniorUnderwriter = team; break;
                                    case CEmployeeFields.E_Role.LegalAuditor:
                                        this.m_teamLegalAuditor = team; break;
                                    case CEmployeeFields.E_Role.LoanOfficerAssistant:
                                        this.m_teamLoanOfficerAssistant = team; break;
                                    case CEmployeeFields.E_Role.Purchaser:
                                        this.m_teamPurchaser = team; break;
                                    case CEmployeeFields.E_Role.QCCompliance:
                                        this.m_teamQCCompliance = team; break;
                                    case CEmployeeFields.E_Role.Secondary:
                                        this.m_teamSecondary = team; break;
                                    case CEmployeeFields.E_Role.Servicing:
                                        this.m_teamServicing = team; break;
                                }
                            }
                        }

                        if (this.m_dsSubmissionSnapshots != null)
                        {
                            DataRowCollection submissionSnapshotRows = this.m_dsSubmissionSnapshots.Tables["SUBMISSION_SNAPSHOT"].Rows;
                            this.m_submissionSnapshots = submissionSnapshotRows
                                .Cast<DataRow>()
                                .Select(row => new SubmissionSnapshot(new DataRowContainer(row)));
                        }
                    }

                    this.SetEncryptionData();
                    this.TakeSnapshotOfTrackingFields(TrackFieldStatusT.Original);
                    this.m_dataState = E_DataState.InitSave;
                }
                catch (Exception)
                {
                    if (null != transaction)
                    {
                        try
                        {
                            transaction.Rollback();
                            transaction = null;
                        }
                        catch { }
                    }

                    throw;
                }
                finally
                {
                    if (null != connection)
                    {
                        connection.Close();
                        connection = null;
                    }
                }
            }
        }

        private void AttachRowUpdatingEvent(SqlDataAdapter sqlAdapter)
        {
            sqlAdapter.RowUpdating += new SqlRowUpdatingEventHandler(SqlDataAdapter_RowUpdating);
        }

        /// <summary>
        /// Round fields and replace fields with sentinel values as needed.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void SqlDataAdapter_RowUpdating(object sender, SqlRowUpdatingEventArgs e)
        {
            // 1/27/2011 dd - Before we save the data to SQL Server, we need to loop through all the Decimal value type and
            // perform banker rounding.
            // 6/7/2011 dd - Set DateTime column to null if value is exceed valid range.
            // 7/23/2012 mp - Perform LTV rounding for LTV, CLTV, and HLTV
            DataRow row = e.Row;
            var parameterCollection = e.Command.Parameters;
            //StringBuilder sb = new StringBuilder();
            foreach (SqlParameter p in parameterCollection)
            {
                switch (p.DbType)
                {
                    case DbType.Currency:
                    case DbType.Decimal:
                    case DbType.Double:
                    case DbType.Single:
                        try
                        {
                            if (p.Scale > 0)
                            {
                                if (p.Value != DBNull.Value)
                                {
                                    decimal v;
                                    if (p.ParameterName.Contains("sLtvR") || // Perform LTV rounding
                                        p.ParameterName.Contains("sCltvR") ||
                                        p.ParameterName.Contains("sHcltvR"))
                                    {
                                        v = Tools.LtvRounding((decimal)p.Value);
                                    }
                                    else // Perform banker rounding
                                    {
                                        v = Math.Round((decimal)p.Value, p.Scale); // Perform banker rounding.
                                        //sb.AppendLine("Round " + p.SourceColumn + " old:" + p.Value + ", new:" + v);
                                    }

                                    // In order to avoid decimal overflows, we will check if the given value
                                    // will fit in the database column and replace it with a either a max or
                                    // min value. We do this here in case the rounding pushes the value out
                                    // of the allowable range.
                                    // opm 449969 gf
                                    if (this.IsCachingUtilityObj && p.Value is decimal)
                                    {
                                        var minMax = DecimalInfo.GetMinMaxTuple(p.Precision, p.Scale);
                                        var min = minMax.Item1;
                                        var max = minMax.Item2;

                                        if (v > max)
                                        {
                                            v = max;
                                            this.LogCacheSentinel(p, v);
                                        }
                                        else if (v < min)
                                        {
                                            v = min;
                                            this.LogCacheSentinel(p, v);
                                        }
                                    }

                                    p.Value = v;
                                    row[p.SourceColumn] = v;
                                }
                            }
                        }
                        catch (Exception exc)
                        {
                            p.Scale = 0;
                            p.Precision = 0;
                            Tools.LogError("Error in rounding " + p.SourceColumn, exc);
                        }
                        break;
                    case DbType.Date:
                    case DbType.DateTime:
                    case DbType.DateTime2:
                        if (p.Value != DBNull.Value)
                        {
                            if (p.Value.GetType() == typeof(DateTime))
                            {
                                DateTime dt = (DateTime)p.Value;
                                if (dt < SqlDateTime.MinValue.Value || dt > SqlDateTime.MaxValue.Value)
                                {
                                    p.Value = DBNull.Value; // 6/7/2011 dd - Set DateTime column to null if value is exceed valid range.
                                }
                            }
                        }
                        break;

                    case DbType.AnsiString:
                    case DbType.AnsiStringFixedLength:
                    case DbType.Binary:
                    case DbType.Boolean:
                    case DbType.Byte:
                    case DbType.DateTimeOffset:
                    case DbType.Guid:
                    case DbType.Int16:
                    case DbType.Int32:
                    case DbType.Int64:
                    case DbType.Object:
                    case DbType.SByte:
                    case DbType.String:
                    case DbType.StringFixedLength:
                    case DbType.Time:
                    case DbType.UInt16:
                    case DbType.UInt32:
                    case DbType.UInt64:
                    case DbType.VarNumeric:
                    case DbType.Xml:
                        continue; // NO-OP
                    default:
                        throw new UnhandledEnumException(p.DbType);
                }
            }
            //Tools.LogError(sb.ToString());
        }

        private void LogCacheSentinel(SqlParameter sqlParam, decimal sentinelValue)
        {
            var errorType = sentinelValue > 0 ? "overflow" : "underflow";
            var msg = "Field " + sqlParam.SourceColumn + " would have " + errorType + "ed in the cache for loan id " + this.sLId.ToString() + ". "
                + "Original value: " + sqlParam.Value.ToString() + " New value: " + sentinelValue.ToString() + ", "
                + "Precision: " + sqlParam.Precision.ToString() + ", Scale: " + sqlParam.Scale.ToString();
            Tools.LogInfo("LoanCacheSentinel", msg);
        }

        /// <summary>
        /// Save the sql update fields so they can be displayed to the person who is trying to debug a cache
        /// update request failure.
        /// </summary>
        /// <remarks>
        /// Updates the affected cache fields of the associated loan cache update request. Also 
        /// performs a database read to determine the current affected cached fields value. If this
        /// has already been performed for a given cache update request, it will not run again. The
        /// affected fields that were added from the first call should not change for subsequent
        /// executions.
        /// </remarks>
        private void SaveAffectedCachedFields()
        {
            var listParams = new SqlParameter[] { new SqlParameter("@p", this.m_cacheUpdateRequestId) };
            string sql = "SELECT AffectedCachedFields AS fields from LOAN_CACHE_UPDATE_REQUEST WHERE RequestId = @p";

            string affectedFields = "";
            Action<IDataReader> readHandler = delegate(IDataReader sdr)
            {
                if (sdr.Read())
                    affectedFields = sdr["fields"].ToString();
            };

            DBSelectUtility.ProcessDBData(this.sBrokerId, sql, null, listParams, readHandler);

            int index = affectedFields.IndexOf(":");

            //only perform the actions inside this block once; once they've been performed, they don't need
            //to repeated for subsequent success attempts
            if (index < 0)
            {
                char[] endTrim = { ',', ' ' };
                StringBuilder invalidFields = new StringBuilder();

                ICollection SaveInvalidatedCachedFields = m_dirtyFieldsLoanCache;
                IEnumerator numer = SaveInvalidatedCachedFields.GetEnumerator();
                while (numer.MoveNext())
                {
                    if (invalidFields.Length != 0)
                        invalidFields.Append(", ");
                    invalidFields.Append(numer.Current);
                }

                SaveInvalidatedCachedFields = m_dirtyFieldsLoanCache2;
                numer = SaveInvalidatedCachedFields.GetEnumerator();
                while (numer.MoveNext())
                {
                    if (invalidFields.Length != 0)
                        invalidFields.Append(", ");
                    invalidFields.Append(numer.Current);
                }

                SaveInvalidatedCachedFields = m_dirtyFieldsLoanCache3;
                numer = SaveInvalidatedCachedFields.GetEnumerator();
                while (numer.MoveNext())
                {
                    if (invalidFields.Length != 0)
                        invalidFields.Append(", ");
                    invalidFields.Append(numer.Current);
                }

                SaveInvalidatedCachedFields = m_dirtyFieldsLoanCache4;
                numer = SaveInvalidatedCachedFields.GetEnumerator();
                while (numer.MoveNext())
                {
                    if (invalidFields.Length != 0)
                        invalidFields.Append(", ");
                    invalidFields.Append(numer.Current);
                }
                string iFields = invalidFields.ToString().TrimEnd(endTrim);
                string totalFields = affectedFields + ":" + iFields;

                LoanCacheUpdateRequest.UpdateAffectedCachedFields(m_cacheUpdateRequestId, this.sBrokerId, totalFields);
            }
        }

        /// <summary>
        /// A value indicating whether a status event was successfully saved for an <see cref="InitSave"/>
        /// -> <see cref="Save"/> cycle.
        /// </summary>
        private bool savedStatusEvent = false;

        /// <summary>
        /// Saves updated data to storage, updates the loan file cache, and performs a lot of related automation.
        /// </summary>
        /// <remarks>
        /// There are a lot of things going on here. Brief summary:
        /// * Increments file version
        /// * Take snapshot of tracked fields
        /// * Ensures field values are up-to-date and ready to be saved
        ///   * Serializes in-memory collections to backing fields, updating related calculated fields
        ///   * Automation that updates field values
        /// * Add methods that need to be run after the data is saved (ie add to background compliance queue)
        /// * Calls into the core saving method -- with lots of custom error handling
        /// * Adds file to be processed by integrations (ie SQL Transmission Service)
        /// * Updates the cache tables
        /// * Adds audit events
        /// * Saves imported conditions
        /// * Updates mobile app data
        /// * Enqueues tasks for updates
        /// </remarks>
        virtual public void Save()
        {
            if (this.IsReadOnly)
            {
                throw new GenericUserErrorMessageException("Programming Error: Save() cannot be called when IsReadOnly is true");
            }
            else if (E_DataState.InitSave != this.m_dataState)
            {
                throw new CBaseException(ErrorMessages.Generic, "Programming Error: Save() was called when datastate was " + this.m_dataState.ToString());
            }
            else if (this.IsCachingUtilityObj && E_CalcModeT.LendersOfficePageEditing != this.CalcModeT)
            {
                string error = "Potential Bug: Should not call Save on loan data object with "
                    + "CalcModeT != LendersOfficePageEditing, it can save the wrong values to "
                    + "the loan file cache tables.";
                Tools.LogError(error);
                throw new CBaseException(ErrorMessages.Generic, error);
            }

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            if (this.CalcModeT == E_CalcModeT.PriceMyLoan)
            {
                if (this.IsPmlLoansModified)
                {
                    // 11/12/2008 dd - OPM 25881 - Always synchronize the peval value before saving.
                    this.CalculatePmlLoans();
                }
            }

            // This code is responsible for assigning IDs to adjustments and prorations. Newly
            // added records will not have an ID until after we re-serialize the records here.
            if (!this.IsCachingUtilityObj)
            {
                if (this.GetDataRow().Contains(nameof(sAdjustmentListJson)))
                {
                    this.sAdjustmentListWithoutAssets.GenerateIdsOnSerialization = true;

                    // the below reserializes it so not a no-op.
                    this.sAdjustmentList = this.sAdjustmentListWithoutAssets;
                }

                if (this.GetDataRow().Contains(nameof(sProrationListJson)))
                {
                    this.sProrationList.GenerateIdsOnSerialization = true;

                    // the below reserializes it so not a no-op.
                    this.sProrationList = this.sProrationList;
                }
            }

            this.Flush();

            // 7/30/2010 dd - OPM 47751 - Increase version number on each save.
            this.SetCountField("sFileVersion", this.sFileVersion + 1);

            if (!this.IsCachingUtilityObj)
            {
                if (!this.DisableComplianceAutomation
                    && (E_BrokerBillingVersion.PerTransaction == this.BrokerDB.BillingVersion) && this.BrokerDB.IsEnablePTMComplianceEaseIndicator
                    && !this.sIsQuickPricerLoan && (this.sLoanFileT == E_sLoanFileT.Loan)
                    && !this.IsTemplate && this.IsValid)
                {
                    bool addToQueue = false;
                    IEnumerable<string> fields = LendersOffice.Conversions.ComplianceEase.ComplianceEaseExporter.SelectStatementProvider.FieldSet;
                    foreach (string dirtyField in this.m_dirtyFields.Keys)
                    {
                        if (dirtyField.Equals("sFileVersion", StringComparison.OrdinalIgnoreCase))
                        {
                            continue;
                        }

                        if (fields.Contains(dirtyField))
                        {
                            addToQueue = true;
                            break;
                        }
                    }

                    if (addToQueue)
                    {
                        // opm 147695, only add to compliance ease queue after save, or could get stuck updating forever.
                        this.RegisterAfterSaveAction(this.AddToComplianceEaseQueue);
                        this.sComplianceEaseStatus = "Updating";
                    }
                }

                // We need to take a "Current" snapshot here because a lot of the following
                // automation depends on detecting dirty fields.
                this.TakeSnapshotOfTrackingFields(TrackFieldStatusT.Current);

                using (PerformanceMonitorItem.Time("CPageBase.EvaluateWorkflowTriggersForDisclosureAutomation"))
                {
                    this.EvaluateWorkflowTriggersForDisclosureAutomation();
                }

                using (PerformanceMonitorItem.Time("CPageBase.ProcessFieldChangeDisclosureTriggers"))
                {
                    this.ProcessFieldChangeDisclosureTriggers();
                }

                this.RegisterAfterSaveAction(this.TryBillLoanIfStatusChangedToBillableStatus); // opm 172868
                
                if (this.BrokerDB.IsFundedDatePopulatedToHmdaActionTaken)
                {
                    this.SetHmdaDataWhenLoanFunded();
                }

                if (this.BrokerDB.IsCanceledDatePopulatedToHmdaActionTaken)
                {
                    this.SetHmdaDataWhenLoanCanceled();
                }

                if (this.BrokerDB.IsDeniedDatePopulatedToHmdaActionTaken) 
                { 
                    this.SetHmdaDataWhenLoanDenied(); 
                }

                if (this.BrokerDB.IsWithdrawnDatePopulatedToHmdaActionTaken)
                {
                    this.SetHmdaDataWhenLoanWithdrawn();
                }

                if (this.ShouldAddToComplianceEagleQueue())
                {
                    this.RegisterAfterSaveAction(this.AddToComplianceEagleQueue);
                    
                    // Not a typo. The ComplianceEase status has been re-purposed as a Compliance status that can be used with any such vendor.
                    this.sComplianceEaseStatus = "Updating";
                }

                if (this.updatePmlBrokerId)
                {
                    var newPmlBrokerId = this.UpdatePmlBrokerId();

                    this.updatePmlBrokerId = false;

                    if (newPmlBrokerId != null)
                    {
                        // 7/28/2015 - To avoid potential of StackOverflow I have to set the value manually instead of using the setter.
                        this.UpdatePmlBrokerIdWithSetterBypass(newPmlBrokerId.Value);
                    }
                }

                if (this.IsFieldModified("sFinalLAmt") || this.IsFieldModified("sStatusT"))
                {
                    this.SetNMLSApplicationAmount();
                }

                this.SetRespaItemEnteredDates();

                if (ConstApp.TridAutoMigrationTriggerFieldIds.Any(f => this.IsFieldModified(f)) ||
                    this.applicationDateWasUpdatedInRespaTrigger)
                {
                    this.RegisterAfterSaveAction(this.MigrateOnTRID2015);
                    this.applicationDateWasUpdatedInRespaTrigger = false;
                }

                if (this.IsFieldModified(nameof(sUniversalLoanIdentifier)))
                {
                    Tools.CheckForUniqueUniversalLoanIdentifier(this.sBrokerId, this.sLId, this.sUniversalLoanIdentifier);
                }

                if (this.IsFieldModified(nameof(sAdjustmentListJson)))
                {
                    this.UpdateLinkedServicingPayments();
                }
            }

            this.PrepareUpdateCommandParameters();

            // e can be the exception thrown by SaveCore or an exception that we created in response
            // to an error that occurred while executing SaveCore. It starts out as null and then is
            // assigned to the Exception that is caught. There are some exceptions that we know will
            // not succeed on retry where we assign e to some new exception and then break
            // out of the retry. For some exceptions, we throw immediately without assigning it to
            // e. If we have not successfully executed SaveCore once we're out of the retry loop we
            // will throw whatever is stored in e.
            Exception e = null;

            bool successfullyExecutedSaveCore = false;
            for (int i = 0; i < 6; ++i)
            {
                try
                {
                    using (PerformanceMonitorItem.Time("CPageBase.SaveCore()"))
                    {
                        this.SaveCore();
                    }

                    successfullyExecutedSaveCore = true;
                    break;
                }
                catch (DataTruncationException)
                {
                    // No use retrying if the data is too long.
                    throw;
                }
                catch (PageDataValidationException)
                {
                    // 3/21/2014 dd - Do not retry. This is workflow rule user exception.
                    throw;
                }
                catch (VersionMismatchException)
                {
                    // 7/30/2010 dd - Do not retry if this exception is throw.
                    throw;
                }
                catch (OverflowException)
                {
                    // 11/1/2010 dd - Retry will not help if overflow exception occur.
                    throw;
                }
                catch (Exception exc)
                {
                    if (null == e)
                    {
                        e = exc;
                        if (e is SqlException)
                        {
                            SqlException ex = (SqlException)e;
                            if (ex.Message.IndexOf("Constraint_UniqueLoanNumberPerBroker") >= 0)
                            {
                                Tools.LogError("Duplicate loan number error " + this.sLNm);

                                // 3/20/2006 dd - Loan name collides, quit right away.
                                e = new CLoanNumberDuplicateException(exc);
                                break;
                            }

                            if (ex.Message.IndexOf("Constraint_UniqueLoanReferenceNumberPerBroker") >= 0)
                            {
                                e = new LoanReferenceNumberDuplicateException(exc);
                                break;
                            }

                            if (ex.Message.IndexOf("Constraint_DocRequestUniqueBorrowerEmail") >= 0)
                            {
                                e = new CBaseException(ErrorMessages.DuplicateEmailInLoanFileWithOpenESign, ex.Message);
                                break;
                            }

                            if (ex.Message.IndexOf("chk_ProdFilterDueMustHaveOneChecked") >= 0)
                            {
                                e = new CBaseException("Must have one of sProdFilterTermXX field check.", "Must have one of sProdFilterTermXX field check.");
                                ((CBaseException)e).IsEmailDeveloper = false;
                                break;
                            }

                            if (ex.Message.IndexOf("Changing the branch will change the cp") >= 0)
                            {
                                var y = new CBaseException("Changing the branch will result in new Consumer Portal association and there are open document request.", ex);
                                y.IsEmailDeveloper = false;
                                e = y;
                                break;
                            }

                            if (ex.Message.Contains("String or binary data would be truncated"))
                            {
                                // If we're trying to put a string that is too long in the loan file
                                // cache we will truncate the value to fit and retry.
                                if (this.IsCachingUtilityObj)
                                {
                                    CFieldInfoTable infoTable = CFieldInfoTable.GetInstance();
                                    List<StringCollection> dirtyFieldsList = new List<StringCollection>()
                                    {
                                        this.m_dirtyFieldsLoanCache,
                                        this.m_dirtyFieldsLoanCache2,
                                        this.m_dirtyFieldsLoanCache3,
                                        this.m_dirtyFieldsLoanCache4
                                    };

                                    bool stringTruncationOccurred = false;

                                    foreach (StringCollection dirtyFields in dirtyFieldsList)
                                    {
                                        foreach (string field in dirtyFields)
                                        {
                                            SqlDbType? type = infoTable[field].DbInfo.m_Cache_dbType;
                                            bool fieldIsStringType = type.HasValue
                                                && (type.Value == SqlDbType.VarChar
                                                    || type.Value == SqlDbType.Text
                                                    || type.Value == SqlDbType.NVarChar
                                                    || type.Value == SqlDbType.NText);
                                            bool fieldValueIsDbNull = Convert.IsDBNull(this.m_dsCache.Tables[0].Rows[0][field]);

                                            if (fieldIsStringType
                                                && !fieldValueIsDbNull)
                                            {
                                                int dbLength = infoTable[field].DbInfo.m_Cache_charMaxLen;
                                                string currentValue = (string)this.m_dsCache.Tables[0].Rows[0][field];

                                                // OPM 236456 - Large fields (varchar(max)) return dbLength == -1, so skip any field with dbLength < 0.
                                                if (dbLength > 0 && dbLength < currentValue.Length)
                                                {
                                                    if (dbLength >= 3)
                                                    {
                                                        this.m_dsCache.Tables[0].Rows[0][field] = currentValue.Substring(0, dbLength - 3) + "...";   
                                                    }
                                                    else
                                                    {
                                                        string msg = "Truncating field with length less than 3. Field = " + field + ". Length = " + dbLength;
                                                        Tools.LogWarning(msg, exc);
                                                        this.m_dsCache.Tables[0].Rows[0][field] = currentValue.Substring(0, dbLength);
                                                    }

                                                    stringTruncationOccurred = true;
                                                    e = null;
                                                }
                                            }
                                        }
                                    }

                                    if (!stringTruncationOccurred)
                                    {
                                        Tools.LogError(exc);
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }
                                    
                                e = new CBaseException(ErrorMessages.Generic, "Data Is Being Truncated");  
                                break;
                            }
                        }
                    }

                    this.LogWarning(string.Format("Db error. Failed to execute Save(), retry count = {0}", i.ToString())
                        , exc);

                    Tools.SleepAndWakeupRandomlyWithin(1000, 4500);
                }
            }

            if (successfullyExecutedSaveCore)
            {
                if (!ConstStage.DisableStatusEventFeature && this.IsFieldModified("sStatusT") && !this.savedStatusEvent && !(this.sStatusEventMigrationVersion == StatusEventMigrationVersion.NotMigrated))
                {
                    var statusDate = this.GetStatusDateRepFromStatus(this.sStatusT, true);
                    StatusEvent.AddStatusEvent(this.sLId, this.sBrokerId, this.sStatusT, statusDate, Tools.RetrieveUserNameForAudit(PrincipalFactory.CurrentPrincipal));
                    this.savedStatusEvent = true;
                }

                if (!this.IsCachingUtilityObj) // w/o this check, we'll be in an infinite loop
                {
                    this.ProcessIntegrations();

                    // Creates a new loan instance specifically to perform the cache update.
                    Tools.UpdateCacheTable(this.GetConnectionInfo(), this.m_fileId, this.m_dirtyFields.Keys, this.m_cacheUpdateRequestId, false);
                }
            }
            else
            {
                if (this.IsCachingUtilityObj)
                {
                    this.SaveAffectedCachedFields();
                }

                this.LogErrorAndSendMail("Failed to execute Save() - Last SQL command on record: " + this.RetrieveLastExecutedQuery(), e);
                throw e;
            }

            if (!this.IsCachingUtilityObj)
            {
                if (this.m_conditionImporter != null)
                {
                    this.SaveImportedConditions();
                }

                if (this.IsTemplate == false)
                {
                    this.UpdateMobileAppLatestActivity();
                }

                if (this.BrokerDB.IsUseNewTaskSystem
                    && false == this.IsTemplate
                    && false == this.IsSkipTaskUpdateUponSave
                    && false == this.sLNm.StartsWith("QUICKPRICER_")) // Using this instead of sIsQuickPricerLoan due to case 110206 
                {
                    if (this.TasksNeedDueDateUpdate())
                    {
                        TaskUtilities.EnqueueTasksDueDateUpdate(this.sBrokerId, this.sLId);
                    }

                    if (this.m_dirtyFieldsEmployeeAssignment != null && this.m_dirtyFieldsEmployeeAssignment.Count > 0)
                    {
                        TaskUtilities.UpdateTaskAssignmentsByLoan(this.sBrokerId, this.sLId, this.m_dirtyFieldsEmployeeAssignment, assignmentLosses);
                    }
                }

                // log qp2 template updates.
                if (this.IsTemplate
                    && this.BrokerDB.Pml2AsQuickPricerMode != E_Pml2AsQuickPricerMode.Disabled
                    && this.sLId == this.BrokerDB.QuickPricerTemplateId)
                {
                    Tools.LogInfo($"qp2template_was_updated for template '{this.sLId}' for broker: " + this.BrokerDB.AsSimpleStringForLog());
                }
            }

            // No active lenders are using this in production.
            if (this.m_isNotifyProLender)
            {
                this.CallProLenderWebService();
            }

            stopwatch.Stop();
            PerformanceMonitorItem monitorItem = PerformanceMonitorItem.Current;
            if (null != monitorItem)
            {
                string name = this.m_namePage;
                if (string.IsNullOrEmpty(name))
                {
                    name = this.ToString();
                }

                string log = name + ".Save()";
                monitorItem.AddTimingDetail(log, stopwatch.ElapsedMilliseconds);
            }
        }

        /// <summary>
        /// Creates new or updates existing conditions based on imported data.
        /// </summary>
        /// <remarks>
        /// Some examples of where condition importer is used: TOTAL, Loan Prospector
        /// importer, some Fannie responses.
        /// This performs some heavy operations such as loading all existing tasks and
        /// condition choices, loading employee details and employee relationships, and
        /// creating or updating existing tasks.
        /// </remarks>
        private void SaveImportedConditions()
        {
            using (PerformanceStopwatch.Start("ConditionImporter.Save"))
            {
                AbstractUserPrincipal principal = Thread.CurrentPrincipal as AbstractUserPrincipal;
                Guid loUserId = this.sEmployeeLoanRepId;              //get the current stuff
                Guid uwUserId = this.sEmployeeUnderwriterId;
                Guid processorId = this.sEmployeeProcessorId;

                if (this.m_dirtyFieldsEmployeeAssignment[ConstApp.ROLE_UNDERWRITER] != null)   //check if it has changed
                {
                    uwUserId = new Guid(this.m_dirtyFieldsEmployeeAssignment[ConstApp.ROLE_UNDERWRITER]);
                }

                if (this.m_dirtyFieldsEmployeeAssignment[ConstApp.ROLE_LOAN_OFFICER] != null) //check if it has changed.
                {
                    loUserId = new Guid(this.m_dirtyFieldsEmployeeAssignment[ConstApp.ROLE_LOAN_OFFICER]);
                }

                if (this.m_dirtyFieldsEmployeeAssignment[ConstApp.ROLE_PROCESSOR] != null)
                {
                    processorId = new Guid(this.m_dirtyFieldsEmployeeAssignment[ConstApp.ROLE_PROCESSOR]);
                }

                this.m_conditionImporter.Save(principal, processorId, loUserId, uwUserId);
            }
        }

        /// <summary>
        /// Triggers a latest activity status update for the mobile application.
        /// </summary>
        private void UpdateMobileAppLatestActivity()
        {
            // 11/30/2011 dd - If any of the status date is modify then update Latest Activity for mobile app.
            string[] statusFields =
            {
                "sRLckdD",
                "sRLckdExpiredD",
                "sLeadD",
                "sOpenedD",
                "sPreQualD",
                "sSubmitD",
                "sProcessingD",
                "sPreApprovD",
                "sEstCloseD",
                "sLoanSubmittedD",
                "sUnderwritingD",
                "sApprovD",
                "sFinalUnderwritingD",
                "sClearToCloseD",
                "sDocsD",
                "sDocsBackD",
                "sFundingConditionsD",
                "sFundD",
                "sRecordedD",
                "sFinalDocsD",
                "sClosedD",
                "sOnHoldD",
                "sCanceledD",
                "sRejectD",
                "sSuspendedD",
                "sShippedToInvestorD",
                "sLPurchaseD",
                "sGoodByLetterD",
                "sServicingStartD",
                "sU1LStatD",
                "sU2LStatD",
                "sU3LStatD",
                "sU4LStatD",
                "sPrelimRprtOd",
                "sApprRprtOd",
                "sAppSubmittedD",
                "sTilGfeOd",
                "sU1DocStatOd",
                "sU2DocStatOd",
                "sU3DocStatOd",
                "sU4DocStatOd",
                "sU5DocStatOd",
                "sDocumentNoteD",
                "sGfeInitialDisclosureD",
                "sGfeRedisclosureD",
                "sTilInitialDisclosureD",
                "sTilRedisclosureD",
                "sApprRprtExpD",
                "sIncomeDocExpD",
                "sCrExpD",
                "sAssetExpD",
                "sBondDocExpD"
            };

            foreach (string o in this.m_dirtyFields.Keys)
            {
                if (statusFields.Contains(o, StringComparer.OrdinalIgnoreCase))
                {
                    LendersOffice.MobileApp.LatestActivity.UpdateStatus(this.sLId);
                    break;
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether the tasks for this loan may need a due date update.
        /// </summary>
        /// <remarks>
        /// The task date update will also update the loan number and primary borrower, so those
        /// fields are checked here too.
        /// </remarks>
        /// <returns>True if the tasks need a due date update. Otherwise, false.</returns>
        private bool TasksNeedDueDateUpdate()
        {
            // 6/15/2011 dd - Update the task calculated due date fields and statistics.
            List<string> validCalcDateFields = Task.ListValidDateFields();

            // 6/22/2011 dd - These two fields are required so that an updated loan number or borrower name will update all existing tasks.
            validCalcDateFields.Add("sLNm");
            validCalcDateFields.Add("sPrimBorrowerFullNm");
            bool isNeedRecalcTask = false;
            foreach (string o in this.m_dirtyFields.Keys)
            {
                if (validCalcDateFields.Contains(o))
                {
                    isNeedRecalcTask = true;
                    break;
                }
            }

            if (isNeedRecalcTask == false)
            {
                // 6/15/2011 dd - Look for date field & task loan cached field in LOAN_FILE_CACHE that affect by this save.
                foreach (var o in CFieldInfoTable.GetInstance().GatherAffectedCacheFields_Obsolete(this.m_dirtyFields.Keys).UpdateFields.GetFieldSpecList())
                {
                    if (validCalcDateFields.Contains(o.Name))
                    {
                        isNeedRecalcTask = true;
                        break;
                    }
                }
            }

            return isNeedRecalcTask;
        }

        /// <summary>
        /// Calls a ProLender web service.
        /// </summary>
        private void CallProLenderWebService()
        {
            LendersOffice.ProLender.Pricing pricing = new LendersOffice.ProLender.Pricing();
            pricing.Url = this.BrokerDB.ProLenderServerURL;
            System.Data.DataSet ds = pricing.GetPricing("PML", 0, this.sLNm);

            string status = ds.Tables[0].Rows[0]["Status"].ToString();
            string message = ds.Tables[0].Rows[0]["message"].ToString();

            Tools.LogInfo("Prolender Pricing.asmx::GetPricing sLNm=" + this.sLNm + ", url=" + this.BrokerDB.ProLenderServerURL + Environment.NewLine + "Status = " + status + ", message = " + message);

            if (status.ToLower() == "failure")
            {
                Tools.LogErrorWithCriticalTracking($"Attempted to update loan: {this.sLId} at url: {pricing.Url} but received error: {message}");
            }
        }

        /// <summary>
        /// Updates the loan-level and application-level RESPA item entered dates for modified
        /// RESPA data points.
        /// </summary>
        /// <remarks>
        /// Note: The entered dates are included as dependencies of the RESPA items as opposed to
        /// the special dependency collection.
        /// </remarks>
        private void SetRespaItemEnteredDates()
        {
            var loanRespaItemToEnteredDateSetter = new Dictionary<string, Action<CDateTime>>()
            {
                [nameof(sLTotI)] = date => sRespaIncomeFirstEnteredD = date,
                [nameof(sSpAddr)] = date => sRespaPropAddressFirstEnteredD = date,
                [nameof(sApprVal)] = date => sRespaPropValueFirstEnteredD = date,
                [nameof(sPurchPrice)] = date => sRespaPropValueFirstEnteredD = date,
                [nameof(sLAmtCalc)] = date => sRespaLoanAmountFirstEnteredD = date
            };

            var now = CDateTime.Create(DateTime.Now);
            var modifiedRespaItems = loanRespaItemToEnteredDateSetter.Where(kvp => IsFieldModified(kvp.Key));
            foreach (var kvp in modifiedRespaItems)
            {
                kvp.Value(now);
            }

            var appRespaItemToEnteredDateSetter = new Dictionary<string, Action<Guid, CDateTime>>()
            {
                [nameof(CAppBase.aBNm)] = (appId, date) => this.GetAppData(appId).aBNmFirstEnteredD = date,
                [nameof(CAppBase.aBSsn)] = (appId, date) => this.GetAppData(appId).aBSsnFirstEnteredD = date,
                [nameof(CAppBase.aCNm)] = (appId, date) => this.GetAppData(appId).aCNmFirstEnteredD = date,
                [nameof(CAppBase.aCSsn)] = (appId, date) => this.GetAppData(appId).aCSsnFirstEnteredD = date,
            };

            // This code is duplicated in multiple places, we could use a better way of dealing with
            // app-level field modifications.
            foreach (var dateSetterByFieldId in appRespaItemToEnteredDateSetter)
            {
                foreach (var appDictionaryByAppId in x_appDataFieldDictionary)
                {
                    Guid appId = appDictionaryByAppId.Key;
                    string fieldId = dateSetterByFieldId.Key;
                    FieldOriginalCurrentItem item;
                    if (appDictionaryByAppId.Value.TryGetValue(fieldId, out item))
                    {
                        string original = StaticMethodsAndExtensions.NormalizeNewLines(item.Original);
                        string current = StaticMethodsAndExtensions.NormalizeNewLines(item.Current);

                        if (!string.Equals(original, current))
                        {
                            dateSetterByFieldId.Value(appId, now);
                        }
                    }
                }
            }
        }

        private bool x_isLoadBrokerExecutingEngine = false;
        private ExecutingEngine x_brokerExecutingEngine = null;

        /// <summary>
        /// Gets the lender's workflow configuration. Will be null if there is no principal available 
        /// or the lender does not have a custom workflow configuration.
        /// </summary>
        /// <returns>
        /// The lender's workflow configuration if the lender has one. Returns null if there is no 
        /// principal available or if the lender doesn't have a custom workflow configuration. 
        /// </returns>
        private ExecutingEngine GetBrokerExecutingEngine()
        {
            if (x_isLoadBrokerExecutingEngine == false)
            {

                // x_brokerExecutingEngine can be null if broker does not have custom workflow.

                // 3/19/2014 dd - Using BrokerId from principal instead of loan object is the sBrokerId is not available before InitSave method.
                if (PrincipalFactory.CurrentPrincipal == null)
                {
                    x_brokerExecutingEngine = null;
                }
                else
                {
                    Guid brokerId = PrincipalFactory.CurrentPrincipal.BrokerId;

                    if (brokerId == Guid.Empty)
                    {
                        brokerId = Tools.GetBrokerIdByLoanID(sLId);
                    }
                    
                    IConfigRepository configRepository = ConfigHandler.GetRepository(brokerId);
                    if (configRepository == null)
                    {
                        throw new GenericUserErrorMessageException("ConfigRepository is null for BrokerId=" + PrincipalFactory.CurrentPrincipal.BrokerId);
                    }
                    x_brokerExecutingEngine = ExecutingEngine.GetEngineByBrokerId(configRepository, PrincipalFactory.CurrentPrincipal.BrokerId);
                }
                x_isLoadBrokerExecutingEngine = true;
            }
            return x_brokerExecutingEngine;
        }

        /// <summary>
        /// Saves updates to the database and external storage.
        /// </summary>
        /// <remarks>
        /// As with Save() there are a lot of things going on here. Summary:
        /// * Performs pre-save validation workflow check
        /// * Builds the update statements for the objects
        /// * Saves updated fields to the database and external storage
        /// * Inserts loan cache update request
        /// * Perform actions registered to occur within the save transaction
        /// * Commits the database transaction
        /// * Creates employee/team audit items
        /// * Perform actions that were registered for after save
        /// * Generate and record audit items
        /// * Evaluate/enqueue task triggers
        /// * Resets snapshot fields
        /// </remarks>
        private void SaveCore()
        {
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;

            using (PerformanceStopwatch.Start("CPageBase.SaveCore"))
            {
                if (this.IsCachingUtilityObj == false)
                {
                    // 3/18/2014 dd - OPM 144112 - Perform Workflow Validation before save.
                    if (this.InternalCPageData != null && this.InternalCPageData.EnforceAccessControl)
                    {
                        this.EnforcePreSaveValidationWorkflow(principal);
                    }
                }

                DbTransaction transaction = null;
                DbConnection connection = this.GetConnection();

                bool committedSaveTransaction = false;
                try
                {
                    DbCommand cmdLoanUpdate = null;
                    if (this.m_dsLoan != null && this.m_dsLoan.HasChanges())
                    {
                        cmdLoanUpdate = this.GetLoanUpdateCmd();
                    }

                    DbCommand cmdAppUpdate = null;
                    if (this.m_dsApp != null && this.m_dsApp.HasChanges())
                    {
                        cmdAppUpdate = this.GetAppUpdateCmd();
                    }

                    DbCommand cmdEmployeeUpdate = this.GetEmployeeAssignedUpdateCmd();

                    DbCommand cmdTeamUpdate = this.GetTeamAssignedUpdateCmd();

                    DbCommand cmdSubmissionSnapshot = this.GetSubmissionSnapshotUpdateCmd();

                    DbCommand cmdLoanCacheUpdate = this.GetLoanCacheUpdateCmd();

                    var fileDBTextFieldList = this.GetModifiedFileDBTextField();

                    if (cmdLoanUpdate != null || cmdAppUpdate != null || 0 < this.LargeFieldStorer.Count || null != cmdEmployeeUpdate || null != cmdLoanCacheUpdate || null != cmdTeamUpdate
                        || fileDBTextFieldList.Any() || cmdSubmissionSnapshot != null)
                    {
                        // We don't want our loan and application data sets to accept the updated data
                        // until the transaction is actually committed.
                        bool acceptLoanDataSetChangesAfterTransactionCommit = false;
                        bool acceptAppDataSetChangesAfterTransactionCommit = false;

                        connection.Open();

                        // We are not possibly adding new row, so this isolation level should be safe
                        // and it should give us some boost for file creation
                        transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);

                        if (this.IsEnforceFileVersionCheck)
                        {
                            this.EnforceFileVersionCheck(connection, transaction);
                        }

                        if (cmdLoanUpdate != null)
                        {
                            this.LastExecutedCommand = cmdLoanUpdate;
                            cmdLoanUpdate.Connection = connection;
                            cmdLoanUpdate.Transaction = transaction;
                            this.m_daLoan.AcceptChangesDuringUpdate = false;
                            Stopwatch sw = Stopwatch.StartNew();
                            this.m_daLoan.UpdateWithoutProfiling(cmdLoanUpdate, this.m_dsLoan, "LOAN_FILE", this.AttachRowUpdatingEvent);
                            acceptLoanDataSetChangesAfterTransactionCommit = true;
                            sw.Stop();
                            PerformanceMonitorItem.AddTimingDetailsToCurrent("sql:CPageBase.SaveCore::LOAN_FILE", sw.ElapsedMilliseconds);
                        }

                        if (cmdAppUpdate != null)
                        {
                            this.LastExecutedCommand = cmdAppUpdate;
                            cmdAppUpdate.Connection = connection;
                            cmdAppUpdate.Transaction = transaction;
                            this.m_daApp.AcceptChangesDuringUpdate = false;
                            Stopwatch sw = Stopwatch.StartNew();
                            this.m_daApp.UpdateWithoutProfiling(cmdAppUpdate, this.m_dsApp, "APPLICATION", this.AttachRowUpdatingEvent);
                            acceptAppDataSetChangesAfterTransactionCommit = true;
                            sw.Stop();
                            PerformanceMonitorItem.AddTimingDetailsToCurrent("sql:CPageBase.SaveCore::APPLICATION", sw.ElapsedMilliseconds);
                        }

                        if (cmdEmployeeUpdate != null)
                        {
                            this.LastExecutedCommand = cmdEmployeeUpdate;
                            Stopwatch sw = Stopwatch.StartNew();
                            cmdEmployeeUpdate.Connection = connection;
                            cmdEmployeeUpdate.Transaction = transaction;
                            cmdEmployeeUpdate.ExecuteNonQuery();
                            sw.Stop();
                            PerformanceMonitorItem.AddTimingDetailsToCurrent("sql:CPageBase.SaveCore::EMPLOYEE", sw.ElapsedMilliseconds);
                        }

                        if (cmdTeamUpdate != null)
                        {
                            this.LastExecutedCommand = cmdTeamUpdate;
                            Stopwatch sw = Stopwatch.StartNew();
                            cmdTeamUpdate.Connection = connection;
                            cmdTeamUpdate.Transaction = transaction;
                            cmdTeamUpdate.ExecuteNonQuery();
                            sw.Stop();
                            PerformanceMonitorItem.AddTimingDetailsToCurrent("sql:CPageBase.SaveCore::TEAM", sw.ElapsedMilliseconds);
                        }

                        if (cmdSubmissionSnapshot != null)
                        {
                            this.LastExecutedCommand = cmdSubmissionSnapshot;
                            Stopwatch sw = Stopwatch.StartNew();
                            cmdSubmissionSnapshot.Connection = connection;
                            cmdSubmissionSnapshot.Transaction = transaction;
                            cmdSubmissionSnapshot.ExecuteNonQuery();
                            sw.Stop();
                            PerformanceMonitorItem.AddTimingDetailsToCurrent("sql:CPageBase.SaveCore::SUBMISSION_SNAPSHOT", sw.ElapsedMilliseconds);
                        }

                        // OPM 457741 - Save UCD Collection if necessary.
                        if (this.includeUcdDeliveryCollection)
                        {
                            this.xUcdDeliveryCollection.Save(connection, transaction);
                        }

                        // NOTE: In the caching context, none of the collections should have been modified, so skip the save to ensure suppressing the repair operations.
                        if (!this.IsCachingUtilityObj && ConstStage.EnableUladCollectionLoading && this.loanLqbCollectionContainer.HasRegisteredCollections
                            && ShouldUseLqbCollectionContainer(this.sBorrowerApplicationCollectionT))
                        {
                            this.loanLqbCollectionContainer.Save(connection, transaction);
                        }

                        if (null != cmdLoanCacheUpdate)
                        {
                            this.LastExecutedCommand = cmdLoanCacheUpdate;
                            cmdLoanCacheUpdate.Connection = connection;
                            cmdLoanCacheUpdate.Transaction = transaction;

                            DbDataAdapter da = DbAccessUtils.GetDbProvider().CreateDataAdapter();
                            Stopwatch sw = Stopwatch.StartNew();
                            try
                            {
                                da.UpdateWithoutProfiling(cmdLoanCacheUpdate, this.m_dsCache, "CACHE", this.AttachRowUpdatingEvent);
                            }
                            catch (OverflowException)
                            {
                                StringBuilder errorMessage = new StringBuilder();
                                errorMessage.AppendLine("UPDATE LOAN_FILE_CACHE");
                                errorMessage.AppendLine();
                                foreach (SqlParameter p in cmdLoanCacheUpdate.Parameters)
                                {
                                    errorMessage.AppendLine(p.ParameterName + " =[" + p.Value + "]");
                                }

                                throw new OverflowException(errorMessage.ToString());
                            }

                            sw.Stop();
                            PerformanceMonitorItem.AddTimingDetailsToCurrent("sql:CPageBase.SaveCore::CACHE", sw.ElapsedMilliseconds);
                        }

                        if (null != principal)
                        {
                            this.TrackLoanModification(principal);
                        }

                        if (0 < this.LargeFieldStorer.Count)
                        {
                            this.LargeFieldStorer.Flush();
                        }

                        if (fileDBTextFieldList.Any())
                        {
                            foreach (var o in fileDBTextFieldList)
                            {
                                o.Save();
                            }
                        }

                        //save cache file info changes during loan file change transaction so that if 
                        //there is an interruption before cache file changes occur, the necessary operations
                        //will be saved in the db and can be recreated
                        if (!this.IsCachingUtilityObj)
                        {
                            // It is not clear why this flag is set here. This value is only used
                            // in PrepareCmdParams when IsCachingUtilityObj is true and
                            // InvalidatedCachedFields is not null. We use a different loan
                            // instance to update the cache too, and this value is not passed to
                            // the instance that performs the cache update.
                            this.m_rerunningCacheUpdateRequest = true;
                            this.InsertCacheUpdateRequest(principal);
                        }

                        // Perform the save transaction actions that have been registered after all SQL updates
                        // that happen as part of save. If any FileDB actions have been registered we only want
                        // to execute them if all the SQL updates in SaveCore() have been successful. If we do
                        // not do this, then it is possible for FileDB actions to be executed even though the db
                        // changes have been rolled back.
                        foreach (var action in this.saveTransactionActions)
                        {
                            action();
                        }

                        if (null != transaction)
                        {
                            // Any failure past this point will result in strange behavior because
                            // the SQL transaction will have already been committed but other
                            // actions that we wanted to occur as part of SaveCore may not have
                            // finished.
                            transaction.Commit();
                            transaction = null;
                            committedSaveTransaction = true;

                            if (acceptLoanDataSetChangesAfterTransactionCommit)
                            {
                                this.m_dsLoan.AcceptChanges();
                            }

                            if (acceptAppDataSetChangesAfterTransactionCommit)
                            {
                                this.m_dsApp.AcceptChanges();
                            }
                        }

                        // 11/8/2006 dd - OPM 6018 Create audit for loan assignment
                        if (this.m_employeeAssignmentChangeList.Count > 0)
                        {
                            AssignmentChangeAuditHelper.CreateAuditEvents(principal, this.m_fileId, this.m_employeeAssignmentChangeList);
                        }

                        if (this.m_teamAssignmentChangeList.Count > 0)
                        {
                            AssignmentChangeAuditHelper.CreateTeamAuditEvents(principal, this.m_fileId, this.m_teamAssignmentChangeList);
                        }
                    }

                    //av 12-01-12 save the pricing states if there were changes for loan comparison
                    using (PerformanceMonitorItem.Time("CPageBase.SaveCore - ActionAfterSave"))
                    {
                        this.SavePricingStatesOnEdit();
                        this.SaveGFEArchivesToFileDBIfArchived();
                        this.SaveCoCArchivesToFileDBIfArchived();
                        foreach (Action t in this.m_AfterSaveAction)
                        {
                            t();
                        }
                    }

                    this.m_dataState = E_DataState.Normal;
                }
                catch (SqlException exc)
                {
                    if (!(this.IsCachingUtilityObj && exc.Message.Contains("String or binary data would be truncated")))
                    {
                        this.LogErrorException(exc);
                    }

                    throw;
                }
                catch (OverflowException)
                {
                    if (transaction != null)
                    {
                        try { transaction.Rollback(); }
                        catch { }
                    }

                    throw;
                }
                catch (DataTruncationException)
                {
                    if (transaction != null)
                    {
                        try { transaction.Rollback(); }
                        catch { }
                    }

                    throw;
                }
                catch (VersionMismatchException)
                {
                    if (transaction != null)
                    {
                        try { transaction.Rollback(); }
                        catch { }
                    }

                    throw;
                }
                catch (System.Exception ex)
                {
                    this.LogErrorException(ex);

                    if (transaction != null)
                    {
                        try { transaction.Rollback(); }
                        catch { }
                    }

                    throw;
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }

                //dont do this stuff for quickpricer loans. opm 83340 do not send events for quick pricer loans
                if (committedSaveTransaction && this.IsCachingUtilityObj == false && this.sLNm.StartsWith("QUICKPRICER_") == false && this.sLoanFileT != E_sLoanFileT.QuickPricer2Sandboxed)
                {
                    // We need to redo the "Current" snapshot of fields because it was possible the
                    // system updated them since the last snapshot.
                    this.TakeSnapshotOfTrackingFields(TrackFieldStatusT.Current);
                    this.CheckedTrackedFieldModifications();
                    this.EvaluateTriggersForTaskAutomation(); // 12/15/2011 dd - OPM 75428
                    if (this.RecordAuditEvent())
                    {
                        AuditManager.RecordAudit(this.sLId, this.m_auditItem.ToArray());
                    }

                    this.m_auditItem.Clear(); //in case they call save again.

                    if (this.IsFieldModified("sStatusT"))
                    {
                        LoanStatusChanged.Send(this.sLId);
                    }

                    // Add any additional audits that aren't conditionally audited.
                    try
                    {
                        AuditManager.RecordAudit(this.sLId, this.m_alwaysAuditItems.ToArray());
                    }
                    catch (OutOfMemoryException e) // this is not ideal.  at some point the above method should be redesigned not to throw.
                    {
                        Tools.LogErrorWithCriticalTracking("audits are too big.  retrying individually.", e);
                        foreach (var auditItem in this.m_alwaysAuditItems)
                        {
                            AuditManager.RecordAudit(this.sLId, auditItem);
                        }
                    }

                    this.m_alwaysAuditItems.Clear();

                    // Re-enable the disclosure workflow & field change triggers.
                    this.m_markingLoanAsDisclosed = false;

                    this.ResetMasterListOfPossibleFieldsToSaveSnapshot();
                }
            }
        }

        /// <summary>
        /// Adds a cache update request for the dirty fields.
        /// </summary>
        /// <param name="principal">The user performing the save.</param>
        private void InsertCacheUpdateRequest(AbstractUserPrincipal principal)
        {
            char[] endTrim = { ',', ' ' };

            StringBuilder invalidFields = new StringBuilder();

            foreach (string field in this.m_dirtyFields.Keys)
            {
                invalidFields.Append(field);
                invalidFields.Append(", ");
            }

            string iFields = invalidFields.ToString().TrimEnd(endTrim);

            DbConnectionInfo connectionInfo = null;

            Guid userId = Guid.Empty;
            string loginName = "Undefined";

            if (principal != null)
            {
                userId = principal.UserId;
                loginName = principal.LoginNm;
                connectionInfo = principal.ConnectionInfo;
            }

            if (this.m_cacheUpdateRequestId == Guid.Empty)
            {
                this.m_cacheUpdateRequestId = Guid.NewGuid();
            }

            if (connectionInfo == null)
            {
                // 4/7/2015 dd - If connection info is unable to determine from principal then resolve from loan id.
                Guid tmpBrokerId = Guid.Empty;
                connectionInfo = DbConnectionInfo.GetConnectionInfoByLoanId(this.m_fileId, out tmpBrokerId);
            }

            LoanCacheUpdateRequest.InsertForLoanSave(
                connection: connectionInfo,
                requestId: this.m_cacheUpdateRequestId,
                loanId: this.m_fileId,
                userId: userId,
                loginName: loginName,
                pageName: this.m_namePage,
                dirtyFields: iFields);
        }

        /// <summary>
        /// Inserts a record into the LOAN_FILE_RECENT_MODIFICATION table.
        /// </summary>
        /// <param name="principal">The user saving the file.</param>
        private void TrackLoanModification(AbstractUserPrincipal principal)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@sLId", this.sLId),
                new SqlParameter("@UserId", principal.UserId),
                new SqlParameter("@UserName", principal.DisplayNameForAuditRecentModification),
                new SqlParameter("@UserType", principal.Type),
                new SqlParameter("@Action", 1) // 1 - For Save Action.
            };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "LoanModification_Insert", 3, parameters);
        }

        /// <summary>
        /// Verifies that the file version in the database is what we expect from when the loan was
        /// initially loaded. Throws an exception if there is a mismatch.
        /// </summary>
        /// <param name="connection">The database connection.</param>
        /// <param name="transaction">The database transaction.</param>
        /// <exception cref="VersionMismatchException">
        /// Thrown when the expected version does not match the actual version.
        /// </exception>
        private void EnforceFileVersionCheck(DbConnection connection, DbTransaction transaction)
        {
            // 7/30/2010 dd - We perform additional check for file version before update in db. This check
            // must occur in the same transaction as UPDATE statements.
            var ensureVersionCommand = connection.CreateCommand();
            ensureVersionCommand.CommandText = "SELECT sFileVersion FROM LOAN_FILE_A WHERE sLId=@sLId";
            ensureVersionCommand.Transaction = transaction;
            ensureVersionCommand.Parameters.Add(new SqlParameter("@sLId", this.sLId));
            using (DbDataReader reader = ensureVersionCommand.ExecuteReader(CommandBehavior.SingleRow))
            {
                if (reader.Read())
                {
                    int sFileVersionFromDb = (int)reader["sFileVersion"];
                    if (sFileVersionFromDb != this.sExpectedFileVersion)
                    {
                        throw new VersionMismatchException();
                    }
                }
            }
        }

        /// <summary>
        /// Checks the lender's pre-save validation rules and throws an exception if a violation
        /// is detected.
        /// </summary>
        /// <param name="principal">The user performing the save.</param>
        /// <exception cref="PageDataValidationException">
        /// Thrown if a workflow violation is detected.
        /// </exception>
        private void EnforcePreSaveValidationWorkflow(AbstractUserPrincipal principal)
        {
            ExecutingEngine executingEngine = this.GetBrokerExecutingEngine();
            if (executingEngine != null)
            {
                if (executingEngine.HasOperation(WorkflowOperations.PreSaveValidation))
                {
                    using (PerformanceMonitorItem.Time("CPageBase.SaveCore - OPERATION_PRESAVE_VALIDATION"))
                    {
                        var oldBypassFieldSecurityCheck = this.ByPassFieldSecurityCheck;
                        var oldFormatTarget = this.GetFormatTarget();

                        this.ByPassFieldSecurityCheck = true;
                        this.SetFormatTarget(FormatTarget.Webform);

                        LoanValueEvaluator valueEvaluator = new LoanValueEvaluator(this.InternalCPageData, this.InternalCPageData.GetAppData(0));
                        valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(principal));

                        // 3/23/2014 dd - Pre-Save Validation is behavior differently than rest of operation.
                        // If it is true the validation error occur. Need to display message to user.
                        IEnumerable<Guid> conditionList = executingEngine.GetAllPassingPrivilegeConditionIds(WorkflowOperations.PreSaveValidation, valueEvaluator);
                        bool isValid = conditionList.Count() == 0;

                        if (isValid == false)
                        {
                            IConfigRepository repository = ConfigHandler.GetRepository(this.sBrokerId);
                            var activeRelease = repository.LoadActiveRelease(this.sBrokerId);
                            if (activeRelease == null)
                            {
                                var systemRepository = ConfigHandler.GetRepository(ConstAppDavid.SystemBrokerGuid);
                                activeRelease = systemRepository.LoadActiveRelease(ConstAppDavid.SystemBrokerGuid);
                            }

                            var config = activeRelease.Configuration;

                            StringBuilder failedReason = new StringBuilder();
                            failedReason.AppendLine("Cannot save loan:");
                            foreach (var condId in conditionList)
                            {
                                failedReason.AppendLine(config.Conditions.Get(condId).FailureMessage);
                            }

                            PageDataValidationException validationException = new PageDataValidationException(failedReason.ToString());
                            validationException.IsEmailDeveloper = false; // ejm 235413 - Stop emailing these errors.
                            throw validationException;
                        }

                        this.ByPassFieldSecurityCheck = oldBypassFieldSecurityCheck;
                        this.SetFormatTarget(oldFormatTarget);
                    }
                }
            }
        }

        [DependsOn((nameof(CAppBase.aHasCoborrower)))]
        public bool GetCoborrowerActiveFlag(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId)
        {
            var appData = this.GetAppData(appId.Value);
            return appData.aHasCoborrower;
        }

        [DependsOn((nameof(CAppBase.aHasCoborrowerData)))]
        public void SetCoborrowerActiveFlag(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId, bool value)
        {
            var appData = this.GetAppData(appId.Value);
            appData.aHasCoborrowerData = value;
        }

        /// <summary>
        /// This method creates a new empty legacy application, and should only be called by the LoanLqbCollectionContainer class.
        /// </summary>
        /// <param name="connection">A database connection.</param>
        /// <param name="transaction">A database transaction.</param>
        /// <returns></returns>
        public DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> AddLegacyApplication(DbConnection connection, DbTransaction transaction)
        {
            Guid appId;
            this.CreateNewApplication(connection, transaction, out appId);
            return DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(appId);
        }

        /// <summary>
        /// This method is intended to be called by the LoanLqbCollectionContainer class only.
        /// </summary>
        /// <param name="legacyAppId">The legacy application that was recently added, or null.</param>
        public void OnSuccessfulApplicationCreation(Guid? legacyAppId)
        {
            Tools.UpdateCacheTable(this.m_fileId, null);
            if (legacyAppId != null)
            {
                LogInfo("App with id=" + legacyAppId.ToString() + " has been created.");
            }

            this.ResetMasterListOfPossibleFieldsToSaveSnapshot();
        }

        /// <summary>
        /// Set the indicated legacy application to be the primary for the loan.
        /// </summary>
        /// <param name="appId">The identifier for the legacy application.</param>
        [DependsOn(nameof(CAppBase.SetAsPrimaryApplication))]
        public void BorrowerManagementOnly_SetLegacyApplicationAsPrimary(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId)
        {
            var appData = this.GetAppData(appId.Value);
            if (appData == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "Cannot find legacy application " + appId.ToString());
            }

            appData.SetAsPrimaryApplication();
            }

        // Return the row number (0-based) of the app.
        virtual public int AddNewApp()
        {
            switch (m_dataState)
            {
                case E_DataState.Normal:
                    break; // perfect
                case E_DataState.InitLoad:
                    throw new CBaseException(ErrorMessages.Generic, "Programming error:  AddNewApp() is executed while dataobject is in InitLoad state.");
                case E_DataState.InitSave:
                    throw new CBaseException(ErrorMessages.Generic, "Programming error:  AddNewApp() is executed while dataobject is in InitSave state.");
                default:
                    throw new UnhandledEnumException(m_dataState);
            }

            // Since the file is in the "Normal" data state, we are not guaranteed to have the
            // data loaded. Go out and retrieve it from the DB. This should not present a large
            // performance issue because we don't expect AddNewApp to be called often.
            var borrowerApplicationCollectionType = this.GetBorrowerApplicationCollectionTFromDb();

            if (borrowerApplicationCollectionType != E_sLqbCollectionT.Legacy)
            {
                if (this.loanLqbCollectionContainer == null)
                {
                    this.InitializeLoanLqbCollectionContainer();
                }

                Guid brokerId;
                if (this.m_rowLoan == null)
                {
                    // In this case, we haven't necessarily performed an InitLoad/InitSave,
                    // so retrieve the broker id directly from the database.
                    brokerId = Tools.GetBrokerIdByLoanID(this.sLId);
                }
                else
                {
                    // We don't expect the broker id to change between InitLoad/InitSave cycles,
                    // so pull the loaded broker id.
                    brokerId = this.sBrokerId;
                }

                this.loanLqbCollectionContainer.AddBlankLegacyApplication();
            }
            else
            {
                Guid newAppId;
                int nRowAffected;

                using (var connection = this.GetConnection())
                {
                    nRowAffected = this.CreateNewApplication(connection, transaction: null, newAppId: out newAppId);
                }

                if (nRowAffected > 0)
                {
                    Tools.UpdateCacheTable(m_fileId, null);
                    LogInfo("App with id=" + newAppId.ToString() + " has been created.");
                }

                ResetMasterListOfPossibleFieldsToSaveSnapshot();
            }

            InitLoad(); // TODO: What a waste especially if this is a big dataobject.
            int iNewApp = m_dataApps.Length - 1;
            DoneLoad();
            return iNewApp; // Since we always sort based on rank, the new one should be last.
        }

        private E_sLqbCollectionT GetBorrowerApplicationCollectionTFromDb()
        {
            var procedure = StoredProcedureName.Create("GetBorrowerApplicationCollectionT").Value;
            var parameters = new[]
            {
                new SqlParameter("sLId", this.sLId)
            };

            E_sLqbCollectionT borrowerApplicationCollectionType = E_sLqbCollectionT.Legacy;
            using (var connection = this.GetConnectionForReadOnly())
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(connection, null, procedure, parameters, TimeoutInSeconds.Default))
            {
                reader.Read();

                if(!Convert.IsDBNull(reader["sBorrowerApplicationCollectionT"]))
                {
                    borrowerApplicationCollectionType = (E_sLqbCollectionT)reader["sBorrowerApplicationCollectionT"];
                }
            }

            return borrowerApplicationCollectionType;
        }

        /// <summary>
        /// Calls the CreateNewApplication stored procedure and returns the number of rows affected.
        /// </summary>
        /// <param name="connection">The database connection.</param>
        /// <param name="transaction">The database transaction.</param>
        /// <param name="newAppId">The new application id. Set as an out parameter.</param>
        /// <returns>The number of rows affected.</returns>
        private int CreateNewApplication(DbConnection connection, DbTransaction transaction, out Guid newAppId)
        {
            var procedureName = StoredProcedureName.Create("CreateNewApplication").Value;
            var parameters = new SqlParameter[2];
            parameters[0] = new SqlParameter("@LoanId", m_fileId);
            parameters[1] = new SqlParameter("@AppId", SqlDbType.UniqueIdentifier);
            parameters[1].Direction = ParameterDirection.Output;

            ModifiedRowCount numRowsAffected = StoredProcedureDriverHelper.ExecuteNonQuery(connection, transaction, procedureName, parameters, TimeoutInSeconds.Default);
            newAppId = (Guid)parameters[1].Value;
            return numRowsAffected.Value;
        }

        [DependsOn(nameof(RemoveLegacyApplication), nameof(sBorrowerApplicationCollectionT))]
        private bool sfDelApp { set { } }

        virtual public bool DelApp(int iApp)
        {
            if (E_DataState.Normal == m_dataState)
                throw new CBaseException(ErrorMessages.Generic, "Programming error:  DelApp() is called when datastate is in normal state (un-initialized).");

            if (iApp >= 0 && iApp < nApps && nApps > 1)
            {
                Guid appId = m_dataApps[iApp].aAppId;

                return DelApp(appId);
            }

            return false;
        }

        /// <summary>
        /// Delete application by id.
        /// </summary>
        /// <param name="aAppId"></param>
        /// <returns></returns>
        public bool DelApp(Guid aAppId)
        {
            if (this.m_dataState != E_DataState.InitSave)
            {
                throw new CBaseException(ErrorMessages.Generic, "Programming error: DelApp() is called when datastate is not InitSave.");
            }

            if (nApps > 1)
            {
                if (this.sBorrowerApplicationCollectionT != E_sLqbCollectionT.Legacy)
                {
                    var appIdentifier = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(aAppId);
                    this.RemoveLegacyApplication(appIdentifier);
                    return true;
                }
                else
                {
                    return DelAppImpl(aAppId);
                }
            }
            else
            {
                return false;
            }
        }

        private bool DelAppImpl(Guid aAppId)
        {
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;

            if (x_appDataFieldDictionary.ContainsKey(aAppId))
            {
                x_appDataFieldDictionary.Remove(aAppId);
            }

            // TODO - (after we have an edocs cleanup mechanism) Get fileDB keys of all docs associated with this app
            /*List<Guid> fileDBKeys = new List<Guid>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader("EDOCS_GetDocsByApp", new SqlParameter("@AppId", aAppId)))
            {
                while (reader.Read())
                {
                    fileDBKeys.Add(new Guid(reader["FileDBKey_CurrentRevision"].ToString()));
                }
            }*/

            List<Guid> idsToDelete = new List<Guid>();
            int nRowAffected = 0;

            using (CStoredProcedureExec spExec = new CStoredProcedureExec(this.sBrokerId))
            {
                try
                {
                    spExec.BeginTransactionForWrite();

                    idsToDelete = ConsumerActionItem.GetFileDBKeysForDelete(aAppId, spExec);

                    SqlParameter[] parameters = {
                                    new SqlParameter("@AppId", aAppId),
                                    new SqlParameter("@UserId", principal.UserId)
                                };
                    nRowAffected = spExec.ExecuteNonQuery("DeleteApplication", 0, parameters);

                    spExec.CommitTransaction();
                }
                catch (Exception e)
                {
                    Tools.LogError("Failed to delete loan app", e);
                    spExec.RollbackTransaction();
                    throw;
                }
            }

            if (nRowAffected > 0)
            {
                LogInfo("App with id=" + aAppId.ToString() + " has been deleted.");
                Tools.UpdateCacheTable(m_fileId, null);
                Tools.TrackIntegrationModifiedFile(this.sBrokerId, this.sLId, this.sLNm);

                // We successfully deleted the app.  Cleanup filedb
                // Unfortunately, there is no batch delete transaction for filedb,
                foreach (Guid idToDelete in idsToDelete)
                {
                    try
                    {
                        FileDBTools.Delete(E_FileDB.EDMS, idToDelete.ToString());
                    }
                    catch
                    {
                        Tools.LogError("Failed to delete from filedb after app deletion id: " + idToDelete.ToString());
                    }
                }

            }
            else
            {
                LogError("Failed to delete App with id=" + aAppId.ToString());
            }

            m_dataState = E_DataState.Normal;
            ResetMasterListOfPossibleFieldsToSaveSnapshot();
            return true;
        }

        private string BuildUpdateTableStatement(string dbTableName, ICollection dirtyFields, string WherePhrase)
        {
            if (dirtyFields.Count == 0)
                return "";

            StringBuilder strBuilder = new StringBuilder(100);
            foreach (string fieldName in dirtyFields)
            {
                if (strBuilder.Length > 0)
                    strBuilder.Append(", ");
                strBuilder.Append(string.Format("{0} = @p{0}", fieldName));
            }

            return string.Format(" Update {0} set {1} WHERE {2}; ", dbTableName, strBuilder.ToString(), WherePhrase);
        }

        /// <summary>
        /// Generates the update statements for loan data and updates the loan update SqlCommand
        /// member to use it.
        /// </summary>
        /// <remarks>
        /// This populates the update statement that will be used by the command, the parameters
        /// are populated by PrepareCmdParams().
        /// </remarks>
        /// <returns>
        /// The SqlCommand to use for loan update if an update needs to be performed. If no update
        /// needs to be performed, returns null.
        /// </returns>
        virtual protected DbCommand GetLoanUpdateCmd()
        {
            StringBuilder strBuilder = new StringBuilder(200);
            strBuilder.Append(BuildUpdateTableStatement("LOAN_FILE_A", m_dirtyFieldsLoanA, "sLId = @psLId"));
            strBuilder.Append(BuildUpdateTableStatement("LOAN_FILE_B", m_dirtyFieldsLoanB, "sLId = @psLId"));
            strBuilder.Append(BuildUpdateTableStatement("LOAN_FILE_C", m_dirtyFieldsLoanC, "sLId = @psLId"));
            strBuilder.Append(BuildUpdateTableStatement("LOAN_FILE_D", m_dirtyFieldsLoanD, "sLId = @psLId"));
            strBuilder.Append(BuildUpdateTableStatement("LOAN_FILE_E", m_dirtyFieldsLoanE, "sLId = @psLId"));
            strBuilder.Append(BuildUpdateTableStatement("LOAN_FILE_F", m_dirtyFieldsLoanF, "sLId = @psLId"));
            strBuilder.Append(BuildUpdateTableStatement("TRUST_ACCOUNT", m_dirtyFieldsTrushAcc, "sLId = @psLId"));

            m_cmdUpdateLoan.CommandText = strBuilder.ToString();
#if( DEBUG )
            Tools.LogInfo("Loan Update Cmd: " + m_cmdUpdateLoan.CommandText);
#endif
            return (m_cmdUpdateLoan.CommandText.Length > 0) ? m_cmdUpdateLoan : null;
        }

        /// <summary>
        /// Generates the update statements for loan file cache data and updates the loan file cache
        /// update SqlCommand member to use it.
        /// </summary>
        /// <remarks>
        /// This populates the update statement that will be used by the command, the parameters
        /// are populated by PrepareCmdParams.
        /// </remarks>
        /// <returns>
        /// The SqlCommand to use for loan file cache update if an update needs to be performed. If
        /// no update needs to be performed, returns null.
        /// </returns>
        virtual protected DbCommand GetLoanCacheUpdateCmd()
        {
            StringBuilder strBuilder = new StringBuilder(200);
            strBuilder.Append(BuildUpdateTableStatement("LOAN_FILE_CACHE", m_dirtyFieldsLoanCache, "sLId = @psLId"));
            strBuilder.Append(BuildUpdateTableStatement("LOAN_FILE_CACHE_2", m_dirtyFieldsLoanCache2, "sLId = @psLId"));
            strBuilder.Append(BuildUpdateTableStatement("LOAN_FILE_CACHE_3", m_dirtyFieldsLoanCache3, "sLId=@psLId"));
            strBuilder.Append(BuildUpdateTableStatement("LOAN_FILE_CACHE_4", m_dirtyFieldsLoanCache4, "sLId = @psLId"));
            m_cmdUpdateCache.CommandText = strBuilder.ToString();

#if(DEBUG)
            Tools.LogInfo("Cache update command text" + m_cmdUpdateCache.CommandText);
#endif
            return (m_cmdUpdateCache.CommandText.Length > 0) ? m_cmdUpdateCache : null;
        }

        /// <summary>
        /// Gets a SqlCommand to update employee assignments. Will be null if there are no
        /// assignment updates.
        /// </summary>
        /// <returns>
        /// A SqlCommand to update employee assignments. Null if there are no assignment updates.
        /// </returns>
        virtual protected DbCommand GetEmployeeAssignedUpdateCmd()
        {
            int employeeCount = m_dirtyFieldsEmployeeAssignment.Count;
            if (employeeCount > 0)
            {
                DbCommand cmd = DbAccessUtils.GetDbProvider().CreateCommand();

                // TODO: Watch out for this command type, injected sql statement is possible
                // for this command type.  This code looks secure as we don't use anything 
                // that user can edit but never know what we would do in the future. Can just 
                // use commandtype.StoreProc instead.  
                cmd.CommandType = CommandType.Text;  // tn: Reviewed on 11/16/07. This is safe, we only deals with the data that cannot be altered by users.

                string strLoanId = "'" + sLId.ToString() + "'";
                StringBuilder strBuilder = new StringBuilder(300);

                for (int i = 0; i < employeeCount; ++i)
                {
                    /*
                        Underwriter
                        Administrator
                        LoanOpener
                        Telemarketer
                        Processor
                        Manager
                        Agent
                        RealEstateAgent
                        Funder
                     */

                    strBuilder.Append("exec AssignRoleToLoan ");
                    strBuilder.Append(" @LoanId= " + strLoanId + ", ");
                    strBuilder.Append(" @RoleId='" + Role.Get(m_dirtyFieldsEmployeeAssignment.AllKeys[i]).Id + "', ");
                    // Guid.Empty would make the entry deleted in the store proc
                    strBuilder.Append(" @EmployeeId='" + m_dirtyFieldsEmployeeAssignment[i].ToString() + "';");
                }
                cmd.CommandText = strBuilder.ToString();
#if( DEBUG )
                Tools.LogRegTest(string.Format("EmployeeAssignment Update Command text: {0}", cmd.CommandText));
#endif
                return cmd;
            }
#if( DEBUG )
            Tools.LogRegTest("EmployeeAssignment Update Command text:");
#endif
            return null;
        }

        /// <summary>
        /// Gets a SqlCommand to update team assignments. Will be null if there are no
        /// assignment updates.
        /// </summary>
        /// <returns>
        /// A SqlCommand to update team assignments. Null if there are no assignment updates.
        /// </returns>
        virtual protected DbCommand GetTeamAssignedUpdateCmd()
        {
            int teamCount = m_dirtyFieldsTeamAssignment.Count;
            if (teamCount > 0)
            {
                DbCommand cmd = DbAccessUtils.GetDbProvider().CreateCommand();

                // TODO: Watch out for this command type, injected sql statement is possible
                // for this command type.  This code looks secure as we don't use anything 
                // that user can edit but never know what we would do in the future. Can just 
                // use commandtype.StoreProc instead.  
                cmd.CommandType = CommandType.Text;  // tn: Reviewed on 11/16/07. This is safe, we only deals with the data that cannot be altered by users.

                string strLoanId = "'" + sLId.ToString() + "'";
                StringBuilder strBuilder = new StringBuilder(300);

                for (int i = 0; i < teamCount; ++i)
                {
                    /*
                        Underwriter
                        Administrator
                        LoanOpener
                        Telemarketer
                        Processor
                        Manager
                        Agent
                        RealEstateAgent
                        Funder
                     */

                    strBuilder.Append("exec TEAM_SetLoanAssignment ");
                    strBuilder.Append(" @LoanId= " + strLoanId + ", ");
                    strBuilder.Append(" @RoleId='" + Role.Get(m_dirtyFieldsTeamAssignment.AllKeys[i]).Id + "', ");
                    // Guid.Empty would make the entry deleted in the store proc
                    strBuilder.Append(" @TeamId='" + m_dirtyFieldsTeamAssignment[i].ToString() + "';");
                }
                cmd.CommandText = strBuilder.ToString();
#if( DEBUG )
                Tools.LogRegTest(string.Format("TeamAssignment Update Command text: {0}", cmd.CommandText));
#endif
                return cmd;
            }
#if( DEBUG )
            Tools.LogRegTest("TeamAssignment Update Command text:");
#endif
            return null;
        }

        /// <summary>
        /// Gets a SqlCommand to update the submission snapshots. Will be null if there is no update.
        /// </summary>
        /// <returns>A SqlCommand to update the submission snapshots. Null if there is no update.</returns>
        virtual protected DbCommand GetSubmissionSnapshotUpdateCmd()
        {
            if (this.m_newSubmissionSnapshot != null)
            {

                var command = DbAccessUtils.GetDbProvider().CreateCommand();
                command.CommandText = "SUBMISSION_SNAPSHOT_Insert";
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddRange(
                    new[]
                    {
                        new SqlParameter("LoanId", this.sLId),
                        new SqlParameter("BrokerId", this.sBrokerId),
                        new SqlParameter("SnapshotDataLastModifiedD", this.m_newSubmissionSnapshot.SnapshotDataLastModifiedD),
                        new SqlParameter("SnapshotFileKey", this.m_newSubmissionSnapshot.SnapshotFileKey.Value),
                        new SqlParameter("PriceGroupId", this.m_newSubmissionSnapshot.PriceGroupId),
                        new SqlParameter("PriceGroupFileKey", this.m_newSubmissionSnapshot.PriceGroupFileKey.Value),
                        new SqlParameter("FeeServiceRevisionId", this.m_newSubmissionSnapshot.FeeServiceRevisionId ?? Convert.DBNull),
                        new SqlParameter("SubmissionType", this.m_newSubmissionSnapshot.SubmissionType),
                        new SqlParameter("SubmissionDate", this.m_newSubmissionSnapshot.SubmissionDate),
                        new SqlParameter("Id", SqlDbType.Int) { Direction = ParameterDirection.Output },
                    });

                return command;
            }

            return null;
        }

        /// <summary>
        /// Generates the update statements for application data and updates the application update 
        /// SqlCommand member to use it.
        /// </summary>
        /// <remarks>
        /// This populates the update statement that will be used by the command, the parameters
        /// are populated by PrepareCmdParams.
        /// </remarks>
        /// <returns>
        /// The SqlCommand to use for loan update if an update needs to be performed. If no update
        /// needs to be performed, returns null.
        /// </returns>
        virtual protected DbCommand GetAppUpdateCmd()
        {
            StringBuilder strBuilder = new StringBuilder(200);
            strBuilder.Append(BuildUpdateTableStatement("APPLICATION_A", m_dirtyFieldsAppA, "aAppId = @paAppId"));
            strBuilder.Append(BuildUpdateTableStatement("APPLICATION_B", m_dirtyFieldsAppB, "aAppId = @paAppId"));

            m_cmdUpdateApp.CommandText = strBuilder.ToString();
#if( DEBUG )
            Tools.LogRegTest("App update command text" + m_cmdUpdateApp.CommandText);
#endif // DEBUG
            return (m_cmdUpdateApp.CommandText.Length > 0) ? m_cmdUpdateApp : null;
        }



        /// <summary>
        /// It's not required unless there are multiple actions needs to be done
        /// with one dataobject.
        /// </summary>
        public virtual void DoneLoad()
        {
            if (E_DataState.InitLoad != m_dataState)
                throw new CBaseException(ErrorMessages.Generic, "Programming Error:  InitLoad() is called when datastate = " + m_dataState.ToString("D"));

            m_dataState = E_DataState.Normal;
        }


        /// <summary>
        /// Loads the needed data and leaves the loan in a state where the data can be accessed. If 
        /// you need to modify and save loan data then use <see cref="InitSave" />.
        /// </summary>
        virtual public void InitLoad()
        {
            using (PerformanceStopwatch.Start("CPageBase.InitLoad"))
            {
                Exception e = null;
                for (int i = 0; i < 5; ++i)
                {
                    try
                    {
                        Stopwatch stopwatch = new Stopwatch();
                        stopwatch.Start();

                        try
                        {
                            this.InitLoadImpl();
                            return;
                        }
                        finally
                        {
                            stopwatch.Stop();
                            PerformanceMonitorItem monitorItem = PerformanceMonitorItem.Current;
                            if (null != monitorItem)
                            {
                                string name = this.m_namePage;
                                if (string.IsNullOrEmpty(name))
                                {
                                    name = this.ToString();
                                }

                                string log = name + ".InitLoad()";
                                monitorItem.AddTimingDetail(log, stopwatch.ElapsedMilliseconds);
                            }
                        }
                    }
                    catch (UnhandledEnumException exc)
                    {
                        this.LogErrorAndSendMail("UnhandleEnumException", exc);
                        throw; // 3/19/2015 dd - Stop retries.
                    }
                    catch (LoanNotFoundException)
                    {
                        throw; // 8/26/2009 dd - Stop retries when LoanNotFoundException is throw.
                    }
                    catch (AccessDenied)
                    {
                        throw; // 7/31/2014 sk - Stop retries when AcessDenied is thrown.
                    }
                    catch (Exception exc)
                    {
                        if (null == e)
                        {
                            e = exc;
                        }

                        this.LogWarning(string.Format("Retry counter = {0}. DB failure in InitLoad().", i.ToString()), exc);
                        Tools.SleepAndWakeupRandomlyWithin(1000, 4500);
                    }
                }

                this.LogErrorAndSendMail("Failure in InitLoad() after 5 retries.", e);
                throw e;
            }
        }

        protected virtual bool GetCacheDataset(ref DataSet dsLoan, ref DataSet dsApp, ref DataSet dsEmployee)
        {
            return false;
        }

        protected virtual void AddDatasetIntoCache(DataSet dsLoan, DataSet dsApp, DataSet dsEmployee)
        {
        }

        protected virtual void LockCache()
        {
        }

        protected virtual void UnlockCache()
        {
        }

        /// <summary>
        /// Executes the given query using the given primary key as the only parameter and populates the results
        /// to a list of dictionaries, where each dictionary represents the data for a single record.
        /// </summary>
        /// <param name="connection">The SQL connection.</param>
        /// <param name="query">The query to execute.</param>
        /// <param name="primaryKey">
        /// The primary key, which is added as the only parameter for the query. Expected format:
        /// (ParameterName, Id).
        /// </param>
        /// <param name="throwOnNoRows">
        /// Indicates whether an exception should be thrown when no results are returned by the query.
        /// </param>
        /// <exception cref="LoanNotFoundException">
        /// Thrown when <paramref name="throwOnNoRows"/> is true and the query returns no results.
        /// </exception>
        /// <exception cref="ArgumentException">
        /// Thrown when the query returns a record with duplicate field names that have different values.
        /// </exception>
        /// <returns>
        /// A list of dictionaries, where each dictionary represents the data for a single record.
        /// </returns>
        private List<Dictionary<string, object>> GetDictionariesFrom(DbConnection connection, string query, Tuple<string, Guid> primaryKey, bool throwOnNoRows)
        {
            if (string.IsNullOrEmpty(query))
            {
                return null;
            }

            var command = connection.CreateCommand();
            command.CommandText = query; 
            command.Parameters.Add(new SqlParameter(primaryKey.Item1, primaryKey.Item2));
            List<Dictionary<string, object>> results = new List<Dictionary<string, object>>();

            using (DbDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    Dictionary<string, object> dictionary = new FriendlyDictionary<string, object>(StringComparer.OrdinalIgnoreCase);

                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        string fieldName = reader.GetName(i);
                        try
                        {
                            dictionary.Add(fieldName, reader[i]);
                        }
                        catch (ArgumentException)
                        {
                            Tools.LogInfo(fieldName + " already exist in the dictionary");
                            object val = dictionary[fieldName];
                            if (!val.Equals(reader[i]))
                            {
                                throw;
                            }
                        }
                    }

                    results.Add(dictionary);
                }
            }

            if (throwOnNoRows && results.Count == 0)
            {
                throw new LoanNotFoundException(sLId);
            }

            return results;
        }

        /// <summary>
        /// Executes the given query using the given primary key as the only parameter and populates the results
        /// to the given dictionary.
        /// </summary>
        /// <param name="connection">The SQL connection.</param>
        /// <param name="query">The query to execute.</param>
        /// <param name="primaryKey">
        /// The primary key, which is added as the only parameter for the query. Expected format:
        /// (ParameterName, Id).
        /// </param>
        /// <param name="values">The dictionary to populate the returned data to.</param>
        /// <param name="throwOnNoRows">
        /// Indicates whether an exception should be thrown when no results are returned by the query.
        /// </param>
        /// <exception cref="LoanNotFoundException">
        /// Thrown when <paramref name="throwOnNoRows"/> is true and the query returns no results.
        /// </exception>
        private void FillDictionary(DbConnection connection, string query, Tuple<string, Guid> primaryKey, Dictionary<string, object> values, bool throwOnNoRows)
        {
            if (string.IsNullOrEmpty(query))
            {
                return;
            }

            var command = connection.CreateCommand();
            command.CommandText = query;
            command.Parameters.Add(new SqlParameter(primaryKey.Item1, primaryKey.Item2));

            using (DbDataReader reader = command.ExecuteReader())
            {
                if (reader.Read())
                {
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        values.Add(reader.GetName(i), reader[i]);
                    }
                }
                else if (throwOnNoRows)
                {
                    throw new LoanNotFoundException(sLId);
                }
            }
        }


        /// <summary>
        /// Loads all of the needed data from the database.
        /// </summary>
        /// <exception cref="CBaseException">Thrown when the data state is not <see cref="E_DataState.Normal"/>.</exception>
        /// <exception cref="AccessDenied">
        /// Thrown when attempting to load a QuickPricer 2 file without the <see cref="AllowLoadWhileQP2Sandboxed"/> flag set.
        /// </exception>
        public void InitLoadImpl()
        {
            if (E_DataState.Normal != this.m_dataState)
            {
                throw new CBaseException(ErrorMessages.Generic, "Programming Error: InitLoad() is called when datastate = " + this.m_dataState.ToString());
            }

            this.ClearAllDbRelatedData();

            var provider = this.m_selectProvider.GenerateSelectStatement(null);

            Dictionary<string, object> loanData = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            List<Dictionary<string, object>> appData;
            List<Dictionary<string, object>> employeData;
            List<Dictionary<string, object>> teamData;
            List<Dictionary<string, object>> submissionSnapshotData;

            Tuple<string, Guid> key = Tuple.Create("@sLId", this.sLId);
            using (DbConnection connection = this.GetConnectionForReadOnly())
            {
                connection.Open();
                this.FillDictionary(connection, provider.Query_LoanFileA, key, loanData, true);
                this.FillDictionary(connection, provider.Query_LoanFileB, key, loanData, true);
                this.FillDictionary(connection, provider.Query_LoanFileC, key, loanData, true);
                this.FillDictionary(connection, provider.Query_LoanFileD, key, loanData, true);
                this.FillDictionary(connection, provider.Query_LoanFileE, key, loanData, true);
                this.FillDictionary(connection, provider.Query_LoanFileF, key, loanData, true);
                this.FillDictionary(connection, provider.Query_TrustedAccount, key, loanData, true);

                if (!string.IsNullOrEmpty(provider.Query_Branch))
                {
                    this.FillDictionary(connection, provider.Query_Branch, Tuple.Create("@BranchId", (Guid)loanData["sBranchId"]), loanData, true);
                }

                appData = this.GetDictionariesFrom(connection, provider.AppSelectFormat, key, true);
                employeData = this.GetDictionariesFrom(connection, provider.AssignedEmployeeSelectFormat, key, false);
                teamData = this.GetDictionariesFrom(connection, provider.AssignedTeamSelectFormat, key, false);
                submissionSnapshotData = this.GetDictionariesFrom(connection, provider.SubmissionSnapshotSelectFormat, key, false);

                // OPM 457741 - If included in dependency graph, load UCD Delivery Collection into loan file at loan initialization.
                this.includeUcdDeliveryCollection = provider.IncludeUcdDeliveryCollection;
                if (this.includeUcdDeliveryCollection)
                {
                    this.xUcdDeliveryCollection = UcdDeliveryCollection.Load(this.sLId, (Guid)loanData["sBrokerId"], connection, null);
                }

                this.InitializeLoanLqbCollectionContainer();

                this.loanLqbCollectionContainer.RegisterCollections(provider.RegisteredLqbCollections);

                if (ConstStage.EnableUladCollectionLoading && this.loanLqbCollectionContainer.HasRegisteredCollections
                    && ShouldUseLqbCollectionContainer(loanData["sBorrowerApplicationCollectionT"].AsDbNullable<E_sLqbCollectionT>()))
                {
                    Guid brokerId = (Guid)loanData["sBrokerId"];

                    this.loanLqbCollectionContainer.Load(
                        DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(brokerId),
                        connection,
                        transaction: null);
                }
            }

            var isForQP2LoanCreationProcess = CurrentRequestOrThreadCache.GetRequestOrThreadObjectByName(GeneratedKeyForRequestOrThreadCache.IsForQP2LoanCreationProcess());
            if (false == this.AllowLoadWhileQP2Sandboxed
                && (null == isForQP2LoanCreationProcess || false == (bool)isForQP2LoanCreationProcess)
                && false == this.IsCachingUtilityObj
                && loanData != null
                && loanData.ContainsKey("sLoanFileT")
                && loanData["sLoanFileT"] != DBNull.Value
                && E_sLoanFileT.QuickPricer2Sandboxed == (E_sLoanFileT)loanData["sLoanFileT"])
            {
                var msg = "You may not view the quickpricer 2.0 loan in this way";
                throw new AccessDenied(msg, msg);
            }

            // Attaches data retrieved from SQL to this CPageBase Object.
            this.PickupLoadedDataReadOnly(loanData, appData, employeData, teamData, submissionSnapshotData);
            this.SetEncryptionData();
        }

        /// <summary>
        /// Returns a value indicating whether, based on the loan data provided, the call should load or save the <see cref="loanLqbCollectionContainer"/> data.
        /// </summary>
        /// <param name="borrowerApplicationCollectionT">The value of <see cref="sBorrowerApplicationCollectionT"/>.</param>
        /// <returns><see langword="true""/> if the collection container should be used; <see langword="false"/> otherwise.</returns>
        private static bool ShouldUseLqbCollectionContainer(E_sLqbCollectionT? borrowerApplicationCollectionT)
        {
            return (borrowerApplicationCollectionT ?? E_sLqbCollectionT.Legacy) != E_sLqbCollectionT.Legacy;
        }

        /// <summary>
        /// Initializes the collection container for this file.
        /// </summary>
        protected void InitializeLoanLqbCollectionContainer()
        {
            var providerFactory = new LoanLqbCollectionProviderFactory();
            var loanId = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.sLId);
            var collectionProvider = providerFactory.CreateStoredProcedureProvider(loanId, null, this, this.GetConnection, PrincipalFactory.CurrentPrincipal?.DisplayName);

            var containerFactory = new LoanLqbCollectionContainerFactory();
            var container = containerFactory.Create(collectionProvider, this, this, this.InternalCPageData);
            this.loanLqbCollectionContainer = this.InjectTestLoanLqbCollectionContainer?.Invoke(container) ?? container;
        }

        /// <summary>
        /// Provides an injection site for tests to replace the default instance of <see cref="ILoanLqbCollectionContainer"/>.
        /// </summary>
        internal Func<ILoanLqbCollectionContainer, ILoanLqbCollectionContainer> InjectTestLoanLqbCollectionContainer = null;

        /// <summary>
        /// Loads the needed data and leaves the loan in a state where data can be modified and saved.
        /// If you just need to read values then use <see cref="InitLoad"/>.
        /// </summary>
        /// <param name="expectedVersion">
        /// The expected file version. This setting is only used if the lender has the version check
        /// enabled. Pass <see cref="ConstAppDavid.SkipVersionCheck"/> if you want to ignore the
        /// check regardless of the lender setting. 
        /// </param>
        public virtual void InitSave(int expectedVersion)
        {
            this.ResetMasterListOfPossibleFieldsToSaveSnapshot();
            this.sExpectedFileVersion = expectedVersion;
            this.InitSavePrivate();
        }
        /// <summary>
        /// *NOTE*, these aren't always recorded, see RecordAuditEvent in SaveCore.
        /// If you want to always record it, use m_alwaysAuditItems.
        /// </summary>
        private List<AbstractAuditItem> m_auditItem = new List<AbstractAuditItem>();

        /// <summary>
        /// Creates audit items for tracked fields that have been modified. The audit items are not
        /// always recorded -- they are only recorded when <see cref="RecordAuditEvent" /> returns
        /// true.
        /// </summary>
        private void CheckedTrackedFieldModifications()
        {
            IDictionary<string, TrackedField> fields = TrackedField.Retrieve();

            foreach (string modifiedField in GetListOfModifiedTrackFields())
            {
                TrackedField trackedField;
                if (false == fields.TryGetValue(modifiedField, out trackedField))
                {
                    continue;
                }

                FieldOriginalCurrentItem item = null;
                if (x_loanFileFieldDictionary.TryGetValue(modifiedField, out item))
                {
                    InsertFieldModificationToAudit(trackedField, item);
                }
                else
                {
                    foreach (Guid appId in x_appDataFieldDictionary.Keys)
                    {
                        Dictionary<string, FieldOriginalCurrentItem> appField = x_appDataFieldDictionary[appId];

                        if (appField.TryGetValue(modifiedField, out item) && false == string.Equals(item.Current, item.Original, StringComparison.OrdinalIgnoreCase))
                        {
                            InsertFieldModificationToAudit(trackedField, item);
                        }
                    }
                }
            }

            isWarehouseChecked = false;
        }

        private void InsertFieldModificationToAudit(TrackedField field, FieldOriginalCurrentItem item)
        {
            string originalDescription = field.Description;
            string original = item.Original;
            string current = item.Current;

            Type fieldType;

            if (field.FieldId == "sStatusT")
            {
                current = sStatusT_map_rep((E_sStatusT)Enum.Parse(typeof(E_sStatusT), current));
                original = sStatusT_map_rep((E_sStatusT)Enum.Parse(typeof(E_sStatusT), original));
            }
            else if (PageDataUtilities.GetFieldEnumType(field.FieldId, out fieldType))
            {
                original = Enum.Parse(fieldType, original).ToString();
                current = Enum.Parse(fieldType, current).ToString();
            }

            var principal = PrincipalFactory.CurrentPrincipal;

            string changedAt = "Page Name " + (string.IsNullOrEmpty(m_namePage) ? "No Name" : m_namePage);
            if (System.Web.HttpContext.Current != null) changedAt += " WebPath: "  + System.Web.HttpContext.Current.Request.Path;

            if (field.FieldId == "sAsstLiqTotal")
            {
                field.setDescription(field.Description + " on file");
            }
            else if (field.FieldId == "sWarehouseLenderRolodexId" || field.FieldId == "sWarehouseFunderDesc")
            {
                if (isWarehouseChecked == true)
                {
                    return;
                }

                isWarehouseChecked = true;

                
                FieldOriginalCurrentItem itemId;
                x_loanFileFieldDictionary.TryGetValue("sWarehouseLenderRolodexId", out itemId);

                FieldOriginalCurrentItem itemDesc;
                x_loanFileFieldDictionary.TryGetValue("sWarehouseFunderDesc", out itemDesc);

                if (itemId.Original == "-2")
                    original = "Other - " + itemDesc.Original;
                else
                    original = itemDesc.Original;

                if (itemId.Current == "-2")
                    current = "Other - " + itemDesc.Current;
                else
                    current = itemDesc.Current;

            }
            else if (field.Description.Contains("[sUZDocStatDesc]"))
            {
                CPageData dataLoan = this.InternalCPageData;

                String desc = "";
                switch (field.FieldId)
                {
                    case "sU1DocStatOd":
                        desc = dataLoan.sU1DocStatDesc;
                        break;
                    case "sU2DocStatOd":
                        desc = dataLoan.sU2DocStatDesc;
                        break;
                    case "sU3DocStatOd":
                        desc = dataLoan.sU3DocStatDesc;
                        break;
                    case "sU4DocStatOd":
                        desc = dataLoan.sU4DocStatDesc;
                        break;
                    case "sU5DocStatOd":
                        desc = dataLoan.sU5DocStatDesc;
                        break;
                }

                field.setDescription("Custom Ordered Date for " + desc + " has");
            }

            // 5/4/2016 BS - Case 242186. Custom Event description in the Audit history
            if (field.Description.Contains("Custom Event Date"))
            {
                CPageData dataLoan = this.InternalCPageData;

                String customEventDateDesc = "";
                switch (field.FieldId)
                {
                    case "sU1LStatD":
                        customEventDateDesc = dataLoan.sU1LStatDesc + " (Custom Event #1) Date";
                        break;
                    case "sU2LStatD":
                        customEventDateDesc = dataLoan.sU2LStatDesc + " (Custom Event #2) Date";
                        break;
                    case "sU3LStatD":
                        customEventDateDesc = dataLoan.sU3LStatDesc + " (Custom Event #3) Date";
                        break;
                    case "sU4LStatD":
                        customEventDateDesc = dataLoan.sU4LStatDesc + " (Custom Event #4) Date";
                        break;
                }
                field.setDescription(customEventDateDesc);
            }

            if (field.Description.Contains("Custom Event Comments"))
            {
                CPageData dataLoan = this.InternalCPageData;

                String customEventCommentDesc = "";
                switch (field.FieldId)
                {
                    case "sU1LStatN":
                        customEventCommentDesc = dataLoan.sU1LStatDesc + " (Custom Event #1) Comments";
                        break;
                    case "sU2LStatN":
                        customEventCommentDesc = dataLoan.sU2LStatDesc + " (Custom Event #2) Comments";
                        break;
                    case "sU3LStatN":
                        customEventCommentDesc = dataLoan.sU3LStatDesc + " (Custom Event #3) Comments";
                        break;
                    case "sU4LStatN":
                        customEventCommentDesc = dataLoan.sU4LStatDesc + " (Custom Event #4) Comments";
                        break;
                }
                field.setDescription(customEventCommentDesc);
            }

            if (field.Description.Contains("Trailing"))
            {
                String oldDescription = field.Description;
                //remove Trailing from the description (Trailing is at the end)
                String description = field.Description.Substring(0, field.Description.Length - 8);
                
                if (field.FieldId.Contains("sCustomTrailingDoc"))
                {
                    String customName = "";
                    CPageData dataLoan = this.InternalCPageData;
                    if (field.FieldId.Contains("sCustomTrailingDoc1"))
                        customName = dataLoan.sCustomTrailingDoc1Desc;
                    if (field.FieldId.Contains("sCustomTrailingDoc2"))
                        customName = dataLoan.sCustomTrailingDoc2Desc;
                    if (field.FieldId.Contains("sCustomTrailingDoc3"))
                        customName = dataLoan.sCustomTrailingDoc3Desc;
                    if (field.FieldId.Contains("sCustomTrailingDoc4"))
                        customName = dataLoan.sCustomTrailingDoc4Desc;

                    //there's 21 characters(including spaces) in "Custom Trailing Doc #"
                    customName += description.Substring(21);
                    field.setDescription(customName);
                }

                TrailingDocAuditItem auditItem = new TrailingDocAuditItem(
                    field,
                    principal.UserId,
                    principal.DisplayName,
                    original, current,
                    changedAt, false);


                m_auditItem.Add(auditItem);
                field.setDescription(oldDescription);
            }
            else
            {
                TrackedFieldModifiedAuditItem auditItem = new TrackedFieldModifiedAuditItem(
                    field,
                    (principal == null) ? Guid.Empty : principal.UserId,
                    (principal == null) ? "System" : principal.DisplayName,
                    original, current,
                    changedAt, false);

                if (ConstApp.AlwaysAuditFieldIds.Contains(field.FieldId))
                {
                    m_alwaysAuditItems.Add(auditItem);
                }
                else
                {
                    m_auditItem.Add(auditItem);
                }

                field.setDescription(originalDescription);
            }
        }

        private string m_sCurrentTrigger = "";
        protected override string GetTriggerLog()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Current Trigger  : {0}{1}", m_sCurrentTrigger, System.Environment.NewLine);
            foreach (var triggerInfo in TriggerDependencyFieldDictionary)
            {
                sb.AppendFormat("{0} : [", triggerInfo.Key);
                var items = new List<string>(triggerInfo.Value);
                for (int i = 0; i < items.Count; i++)
                {
                    if (i != 0)
                    {
                        sb.Append(",");
                    }

                    sb.Append(items[i]);
                }
                sb.AppendLine("]");
            }
            return sb.ToString();
        }

        /// <summary>
        /// Evaluates task triggers and enqueues tasks for creation when a trigger evaluates to true.
        /// </summary>
        private void EvaluateTriggersForTaskAutomation()
        {
            try
            {
                LogTriggerInfo = true;
                EvaluateTriggersForTaskAutomationImpl();
            }
            finally
            {
                LogTriggerInfo = false;
            }
        }

        private void EvaluateTriggersForTaskAutomationImpl()
        {
            // Add hard coded triggers.
            // NOTE: This needs to be done regardless of what fields have been modified.
            EvaluateHardCodedTaskTriggers();

            // 12/22/2011 dd - Get a list of fields from trigger that was modify in this save.
            HashSet<string> modifyFieldsList = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            foreach (var field in ListTriggerFieldList())
            {
                if (IsFieldModified(field) == true)
                {
                    modifyFieldsList.Add(field);
                }
            }

            if (modifyFieldsList.Count == 0)
            {
                return; // Nothing to do.
            }

            var oldBypassFieldSecurityCheck = this.ByPassFieldSecurityCheck;
            var oldFormatTarget = this.GetFormatTarget();

            this.ByPassFieldSecurityCheck = true;
            this.SetFormatTarget(FormatTarget.Webform);

            LoanValueEvaluator valueEvaluator = new LoanValueEvaluator(this.InternalCPageData, this.InternalCPageData.GetAppData(0));
            valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(this.sBrokerId, (SystemUserPrincipal)SystemUserPrincipal.TaskSystemUser));
            
            // Run triggers.
            EvaluateTaskCreationTriggers(modifyFieldsList, valueEvaluator);
            EvaluateTaskAutomationTriggers(modifyFieldsList, valueEvaluator);

            this.ByPassFieldSecurityCheck = oldBypassFieldSecurityCheck;
            this.SetFormatTarget(oldFormatTarget);
        }

        private void EvaluateTaskAutomationTriggers(HashSet<string> modifyFieldsList, LoanValueEvaluator valueEvaluator)
        {
            Dictionary<string, bool> cachedEvaluationResults = new Dictionary<string, bool>(StringComparer.OrdinalIgnoreCase);

            var resolutionBlockTriggers = Task.GetAllTaskResolutionBlockTriggers(this.sBrokerId, this.sLId);
            var taskTriggerAssociations = TaskTriggerAssociation.GetTaskTriggerAssociationsForLoan(this.sBrokerId, this.sLId);
            foreach (var kvp in taskTriggerAssociations)
            {
                string taskId = kvp.Key;
                IReadOnlyDictionary<TaskTriggerType, TaskTriggerAssociation> associationsByType = kvp.Value;

                // Check if task resolution is blocked.
                bool isResolutionBlocked = false;
                string resolutionBlockTriggerName = null;
                if (resolutionBlockTriggers.TryGetValue(taskId, out resolutionBlockTriggerName) &&
                    !cachedEvaluationResults.TryGetValue(resolutionBlockTriggerName, out isResolutionBlocked))
                {
                    isResolutionBlocked = LendingQBExecutingEngine.EvaluateTrigger(resolutionBlockTriggerName, valueEvaluator);
                    cachedEvaluationResults.Add(resolutionBlockTriggerName, isResolutionBlocked);
                }

                if (isResolutionBlocked)
                {
                    continue;
                }

                // Check Auto-Close trigger before Auto-Resolve trigger. We can skip Auto-Resolve check if we know we're going to close the task anyway.
                bool triggeredAutoClose = false;
                TaskTriggerAssociation autoCloseAssociation = null;
                if (associationsByType.TryGetValue(TaskTriggerType.AutoClose, out autoCloseAssociation))
                {
                    Action AutoCloseAction = () => TaskAutomationJob.CreateTaskAutomationJob(autoCloseAssociation.BrokerId, autoCloseAssociation.LoanId, autoCloseAssociation.TaskId, TaskAutomationJobType.AutoClose);
                    triggeredAutoClose = EvaluateSingleAutomationTrigger(autoCloseAssociation.TriggerName, resolutionBlockTriggerName, modifyFieldsList, valueEvaluator, cachedEvaluationResults, AutoCloseAction);
                }

                TaskTriggerAssociation autoResolveAssociation = null;
                if (!triggeredAutoClose && associationsByType.TryGetValue(TaskTriggerType.AutoResolve, out autoResolveAssociation))
                {
                    Action AutoResolveAction = () => TaskAutomationJob.CreateTaskAutomationJob(autoResolveAssociation.BrokerId, autoResolveAssociation.LoanId, autoResolveAssociation.TaskId, TaskAutomationJobType.AutoResolve);
                    triggeredAutoClose = EvaluateSingleAutomationTrigger(autoResolveAssociation.TriggerName, resolutionBlockTriggerName, modifyFieldsList, valueEvaluator, cachedEvaluationResults, AutoResolveAction);
                }
            }
        }

        private bool EvaluateSingleAutomationTrigger (
            string triggerName,
            string blockTriggerName,
            HashSet<string> modifyFieldsList,
            LoanValueEvaluator valueEvaluator,
            Dictionary<string, bool> cachedEvaluationResults,
            Action triggerAction)
        {
            bool evaluationResult;
            if (cachedEvaluationResults.TryGetValue(triggerName, out evaluationResult))
            {
                if (evaluationResult)
                {
                    triggerAction();
                }

                return evaluationResult;
            }

            // We haven't run into this trigger before. Only run it if it's parameters have been modified.
            HashSet<string> dependencies = null;
            HashSet<string> blockDependencies = null;

            if(!TriggerDependencyFieldDictionary.TryGetValue(triggerName, out dependencies))
            {
                cachedEvaluationResults.Add(triggerName, false);
                return false;
            }

            if (!string.IsNullOrEmpty(blockTriggerName))
            {
                TriggerDependencyFieldDictionary.TryGetValue(blockTriggerName, out blockDependencies);
            }

            if (!modifyFieldsList.Intersect(dependencies, StringComparer.OrdinalIgnoreCase).Any()
                && (blockDependencies == null || !modifyFieldsList.Intersect(blockDependencies, StringComparer.OrdinalIgnoreCase).Any()))
            {
                cachedEvaluationResults.Add(triggerName, false);
                return false;
            }

            evaluationResult = LendingQBExecutingEngine.EvaluateTrigger(triggerName, valueEvaluator);

            if (evaluationResult)
            {
                triggerAction();
            }

            cachedEvaluationResults.Add(triggerName, evaluationResult);

            return evaluationResult;
        }

        private void EvaluateTaskCreationTriggers(HashSet<string> modifyFieldsList, LoanValueEvaluator valueEvaluator)
        {
            // 12/15/2011 dd - List only triggers that has field overlap with dirty fields.
            HashSet<string> triggerToBeEvaluateSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            foreach (string triggerName in this.BrokerDB.GetTaskTriggerTemplateIdToCreationTriggerNameMap().Values)
            {
                HashSet<string> dependencies;
                if (!TriggerDependencyFieldDictionary.TryGetValue(triggerName, out dependencies))
                {
                    continue;
                }

                if (modifyFieldsList.Intersect(dependencies, StringComparer.OrdinalIgnoreCase).Any())
                {
                    triggerToBeEvaluateSet.Add(triggerName);
                }
            }

            HashSet<string> hitTriggerSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            foreach (var trigger in triggerToBeEvaluateSet)
            {
                m_sCurrentTrigger = trigger;
                bool bEval = LendingQBExecutingEngine.EvaluateTrigger(trigger, valueEvaluator);

                if (bEval)
                {
                    hitTriggerSet.Add(trigger);
                }
            }

            if (hitTriggerSet.Count > 0)
            {
                Guid aAppId = GetAppData(0).aAppId; // 12/27/2011 dd - Use primary app for now.
                foreach (var o in this.BrokerDB.GetTaskTriggerTemplateIdToCreationTriggerNameMap())
                {
                    if (hitTriggerSet.Contains(o.Value))
                    {
                        TaskTriggerQueueItem item = new TaskTriggerQueueItem(o.Key, o.Value, this.sBrokerId, sLId, aAppId, Guid.Empty);
                        TaskTriggerQueue.Send(item);
                    }
                }
            }
        }

        private void EvaluateHardCodedTaskTriggers()
        {
            if (m_hardCodeTaskTriggerList.Count > 0)
            {
                HashSet<string> set = new HashSet<string>();
                foreach (var trigger in m_hardCodeTaskTriggerList)
                {
                    string key = trigger.TriggerName + "_" + trigger.RecordId;

                    if (set.Contains(key) == false)
                    {
                        foreach (var taskTrigger in this.BrokerDB.GetTaskTriggerTemplateIdToCreationTriggerNameMap())
                        {
                            if (taskTrigger.Value.Equals(trigger.TriggerName, StringComparison.OrdinalIgnoreCase))
                            {
                                TaskTriggerQueueItem item = new TaskTriggerQueueItem(taskTrigger.Key, taskTrigger.Value, trigger.BrokerId, trigger.sLId, trigger.aAppId, trigger.RecordId);
                                TaskTriggerQueue.Send(item);
                            }
                        }
                        set.Add(key);
                    }
                }
            }
        }

        public IEnumerable<Tuple<string, FieldOriginalCurrentItem>> GetChangedFields()
        {
            List<Tuple<string, FieldOriginalCurrentItem>> changedFields = new List<Tuple<string, FieldOriginalCurrentItem>>();
            TakeSnapshotOfTrackingFields(TrackFieldStatusT.Current);
            foreach (string modifiedField in GetListOfModifiedTrackFields())
            {
                FieldOriginalCurrentItem item = null;
                if (x_loanFileFieldDictionary.TryGetValue(modifiedField, out item))
                {
                    changedFields.Add(Tuple.Create(modifiedField, item));
                }
                else
                {
                    foreach (Dictionary<string, FieldOriginalCurrentItem> appField in x_appDataFieldDictionary.Values)
                    {
                        if (appField.TryGetValue(modifiedField, out item) && false == string.Equals(item.Current, item.Original, StringComparison.OrdinalIgnoreCase))
                        {
                            changedFields.Add(Tuple.Create(modifiedField, item));
                        }
                    }
                }
            }
            return changedFields;
        }



        // 12/21/2011 dd - DO NOT USE x_masterListOfFieldsForsnapshot directly. Must call GetMasterListOfPossibleFieldsToSaveSnapshot()
        // Only AddFieldsToTrack can insert to x_masterListOfFieldsForsnapshot
        private HashSet<string> x_masterListOfFieldsForsnapshot = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
        private bool x_masterListOfFieldsLock = false;

        /// <summary>
        /// Gets all of the fields that have been added for tracking. Prevents any further calls
        /// to <see cref="AddFieldsToTrack"/> until <see cref="ResetMasterListOfPossibleFieldsToSaveSnapshot"/>
        /// has been called.
        /// </summary>
        /// <returns>All of the fields that have been added for tracking.</returns>
        private IEnumerable<string> GetMasterListOfPossibleFieldsToSaveSnapshot()
        {
            x_masterListOfFieldsLock = true; // 12/21/2011 dd - Prevent any more modification to x_masterListOfFieldsForsnapshot.
            return x_masterListOfFieldsForsnapshot;
        }

        /// <summary>
        /// Clears the set of fields used in the snapshot and re-enables modifying it.
        /// </summary>
        private void ResetMasterListOfPossibleFieldsToSaveSnapshot()
        {
            x_masterListOfFieldsForsnapshot = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            x_masterListOfFieldsLock = false;
        }
        private Dictionary<string, HashSet<string>> x_triggerDependencyFieldDictionary = null;

        /// <summary>
        /// Populates <see cref="x_triggerDependencyFieldDictionary"/> with the field dependencies
        /// for each task trigger name.
        /// </summary>
        private void LoadTaskTriggerInformation()
        {
            if (x_triggerDependencyFieldDictionary != null)
            {
                return;
            }

            x_triggerDependencyFieldDictionary = new Dictionary<string, HashSet<string>>(StringComparer.OrdinalIgnoreCase);

            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            if (principal == null)
            {
                return;
            }

            if (principal.Type == "I")
            {
                principal = ((InternalUserPrincipal)principal).BecomeUserPrincipal;
                if (null == principal)
                {
                    return;
                }
            }

            if (principal.BrokerId == Guid.Empty)
            {
                return;
            }

            // Get creation triggers from templates.
            // 12/27/2011 dd - A trigger list template can contains duplicate trigger name.
            // Therefore only evaluate a list of distinct trigger name.
            HashSet<string> triggerList = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            triggerList.UnionWith(principal.BrokerDB.GetTaskTriggerTemplateIdToCreationTriggerNameMap().Values);

            // Get associated triggers from task.
            var taskTriggerAssociations = TaskTriggerAssociation.GetTaskTriggerAssociationsForLoan(principal.BrokerId, this.sLId);
            triggerList.UnionWith(taskTriggerAssociations.SelectMany(kvp => kvp.Value.Values.Select(association => association.TriggerName)));

            // Get Resolution block triggers
            triggerList.UnionWith(Task.GetAllTaskResolutionBlockTriggers(principal.BrokerId, this.sLId).Values);

            // Get dependencies for all triggers.
            foreach (var trigger in triggerList)
            {
                var set = LendingQBExecutingEngine.GetDependencyFieldsByTrigger(principal.BrokerDB.BrokerID, trigger);

                if (set.Any())
                {
                    x_triggerDependencyFieldDictionary.Add(trigger, new HashSet<string>(set, StringComparer.OrdinalIgnoreCase));
                }
            }
        }

        /// <summary>
        /// A map from task trigger name to the set of fields used by that trigger.
        /// </summary>
        private Dictionary<string, HashSet<string>> TriggerDependencyFieldDictionary
        {
            get
            {
                if (x_triggerDependencyFieldDictionary == null)
                {
                    LoadTaskTriggerInformation();
                }
                return x_triggerDependencyFieldDictionary;
            }
        }

        /// <summary>
        /// Gets all the field dependencies of the lender's task triggers.
        /// </summary>
        /// <returns>The field dependencies of the lender's task triggers.</returns>
        private IEnumerable<string> ListTriggerFieldList()
        {
            var dictionary = TriggerDependencyFieldDictionary;

            HashSet<string> list = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            foreach (var set in dictionary.Values)
            {
                list.UnionWith(set);
            }
            return list.ToList();
        }

        private List<TaskTriggerQueueItem> m_hardCodeTaskTriggerList = new List<TaskTriggerQueueItem>();
        internal void AddHardcodeTaskTrigger(TaskTriggerQueueItem item)
        {
            m_hardCodeTaskTriggerList.Add(item);
        }

        /// <summary>
        /// Adds fields to the set of fields that may be included in the field snapshot. Adding
        /// a field here does not guarantee that it will be included in the snapshot. The data
        /// layer may determine a field does not need to be tracked given the data that is being
        /// loaded or the lender configuration.
        /// </summary>
        /// <param name="fields">The fields to add.</param>
        /// <exception cref="CBaseException">
        /// Thrown when <see cref="GetMasterListOfPossibleFieldsToSaveSnapshot"/> has already been
        /// called for an <see cref="InitSave"/> -> <see cref="Save"/> cycle.
        /// </exception>
        internal void AddFieldsToTrack(IEnumerable<string> fields)
        {
            if (null == fields)
            {
                return;
            }
            if (x_masterListOfFieldsLock)
            {
                // 12/21/2011 dd - Once we invoke the method GetMastListOfPossibleFieldsToSaveSnapshot we will restrict
                // AddFieldsToTrack in order to minimize bug.
                throw new CBaseException(ErrorMessages.Generic, "Cannot invoke AddFieldsToTrack method after GetMasterListOfPossibleFieldsToSaveSnapshot()");
            }
            x_masterListOfFieldsForsnapshot.UnionWith(fields);
        }

        private HashSet<string> x_fieldSetInSnapshot = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

        /// <summary>
        /// Adds the given field to be included in the field snapshot.
        /// </summary>
        /// <param name="fieldName">The field to track in the snapshot.</param>
        private void AddFieldsToKeepTrackOfSnapshot(string fieldName)
        {
            x_fieldSetInSnapshot.Add(fieldName);
        }

        protected void AddFieldsToKeepTrackOfSnapshot(IEnumerable<string> fieldNames)
        {
            x_fieldSetInSnapshot.UnionWith(fieldNames);
        }
        private IEnumerable<string> GetFieldListInSnapshot()
        {
            return x_fieldSetInSnapshot;
        }
        protected enum TrackFieldStatusT { Original, Current };
        /// <summary>
        /// Always false.
        /// </summary>
        /// <remarks>
        /// Looks like this was intended to indicate if we performed a snapshot of the "current"
        /// versions of fields, but it is never set to true. It also seems potentially unsafe to
        /// set this to true. It also seems potentially unsafe to set this to true -- we take a
        /// "current" snapshot in save and may modify fields before we take another "current"
        /// snapshot later in the save process.
        /// </remarks>
        private bool fetchedCurrent = false;
        /// <summary>
        /// Gets the values of the tracked fields to maintain as either original or to-be-saved
        /// values.
        /// </summary>
        /// <param name="status">
        /// A value indicating whether we are capturing the original or to-be-saved values.
        /// </param>
        protected void TakeSnapshotOfTrackingFields(TrackFieldStatusT status)
        {
            using (PerformanceMonitorItem.Time("CPageBase.TakeSnapshotOfTrackingFields - " + status))
            {
                E_CalcModeT oldCalcMode = CalcModeT;
                CalcModeT = E_CalcModeT.LendersOfficePageEditing;
                TakeSnapshotOfTrackingFieldsImpl(status);
                CalcModeT = oldCalcMode;
            }
        }

        internal bool TryGetOriginalValue(string fieldName, out string fieldValue)
        {
            fieldValue = null;

            if (x_fieldSetInSnapshot.Contains(fieldName) == false)
            {
                return false;
            }
            E_PageDataClassType pageDataClassType;
            bool isFound = PageDataUtilities.GetPageDataClassType(fieldName, out pageDataClassType);
            if (isFound == false)
            {
                return false;
            }
            bool hasResult = false;
            FieldOriginalCurrentItem item = null;
            switch (pageDataClassType)
            {
                case E_PageDataClassType.CBasePage:

                    if (x_loanFileFieldDictionary.TryGetValue(fieldName, out item))
                    {
                        hasResult = true;
                        fieldValue = item.Original;
                    }
                    break;
                case E_PageDataClassType.CAppBase:
                    throw new NotSupportedException("Current not support for type=E_PageDataClassType.CAppBase. FieldName=[" + fieldName +"]");
                default:
                    throw new UnhandledEnumException(pageDataClassType);
            }


            return hasResult;
        }
        private void TakeSnapshotOfTrackingFieldsImpl(TrackFieldStatusT status)
        {
            using (PerformanceStopwatch.Start("CPageBase.TakeSnapshotOfTrackingFields"))
            {
                if (status == TrackFieldStatusT.Current && fetchedCurrent)
                {
                    return;
                }
                var oldByPass = ByPassFieldSecurityCheck;
                ByPassFieldSecurityCheck = true;
                // 12/15/2011 dd - Go through the list of field in GetFieldListInSnapshot() and record the before value.
                // DO NOT USE GetMasterListOfPossibleFieldsToSaveSnapshot() here because not all fields are require to be record in snapshot.

                foreach (var fieldName in GetFieldListInSnapshot())
                {
                    string correctedFieldName = fieldName;
                    if (fieldName.Equals("branchgroup", StringComparison.OrdinalIgnoreCase) || fieldName.Equals("branch", StringComparison.OrdinalIgnoreCase))
                    {
                        correctedFieldName = "sBranchId";
                    }
                    else if (fieldName.Equals("sleadsrcidenum", StringComparison.OrdinalIgnoreCase))
                    {
                        correctedFieldName = "sLeadSrcId";
                    }
                    else if (fieldName.Equals("sstatusprogresst", StringComparison.OrdinalIgnoreCase))
                    {
                        correctedFieldName = "sStatusT";
                    }

                    E_PageDataClassType pageDataClassType;
                    FieldOriginalCurrentItem item = null;

                    bool isFound = PageDataUtilities.GetPageDataClassType(correctedFieldName, out pageDataClassType);
                    if (isFound)
                    {
                        CPageData dataLoan = this.InternalCPageData;
                        if (dataLoan != null)
                        {
                            var originalFormatTarget = dataLoan.GetFormatTarget();

                            // 2/16/2012 dd - When taking snapshot, we must always use a consistent format target.
                            dataLoan.SetFormatTarget(FormatTarget.Webform);
                            string oldStr = string.Empty;
                            string newStr = string.Empty;

                            switch (pageDataClassType)
                            {
                                case E_PageDataClassType.CBasePage:
                                    if (x_loanFileFieldDictionary.TryGetValue(correctedFieldName, out item) == false)
                                    {
                                        item = new FieldOriginalCurrentItem();
                                        x_loanFileFieldDictionary.Add(correctedFieldName, item);
                                    }
                                    if (status == TrackFieldStatusT.Original)
                                    {
                                        item.Original = PageDataUtilities.GetValue(dataLoan, null, correctedFieldName);
                                    }
                                    else if (status == TrackFieldStatusT.Current)
                                    {
                                        item.Current = PageDataUtilities.GetValue(dataLoan, null, correctedFieldName);
                                    }
                                    break;
                                case E_PageDataClassType.CAppBase:
                                    for (int iApp = 0; iApp < nApps; iApp++)
                                    {
                                        CAppData dataApp = dataLoan.GetAppData(iApp);

                                        Dictionary<string, FieldOriginalCurrentItem> dataAppDictionary = null;

                                        if (x_appDataFieldDictionary.TryGetValue(dataApp.aAppId, out dataAppDictionary) == false)
                                        {
                                            dataAppDictionary = new Dictionary<string, FieldOriginalCurrentItem>(StringComparer.OrdinalIgnoreCase);
                                            x_appDataFieldDictionary.Add(dataApp.aAppId, dataAppDictionary);
                                        }
                                        if (dataAppDictionary.TryGetValue(correctedFieldName, out item) == false)
                                        {
                                            item = new FieldOriginalCurrentItem();
                                            dataAppDictionary.Add(correctedFieldName, item);
                                        }
                                        if (status == TrackFieldStatusT.Original)
                                        {
                                            item.Original = PageDataUtilities.GetValue(dataLoan, dataApp, correctedFieldName);
                                        }
                                        else if (status == TrackFieldStatusT.Current)
                                        {
                                            item.Current = PageDataUtilities.GetValue(dataLoan, dataApp, correctedFieldName);
                                        }

                                    }
                                    break;
                                default:
                                    throw new UnhandledEnumException(pageDataClassType);
                            }

                            dataLoan.SetFormatTarget(originalFormatTarget);
                        }
                        else
                        {
                            string fullType = this.GetType().FullName;
                            if (fullType == "DataAccess.UpdateTPOData")
                            {
                                // Skip.
                            }
                            else
                            {
                                //  Tools.LogBug("InternalCPageData is null. " + this.GetType().FullName);
                            }
                        }
                    }
                    else
                    {
                        string[] sCustomPMLFieldList = new string[] { "sCustomPMLField1", "sCustomPMLField2", "sCustomPMLField3", "sCustomPMLField4", "sCustomPMLField5",
                            "sCustomPMLField6", "sCustomPMLField7", "sCustomPMLField8", "sCustomPMLField9", "sCustomPMLField10", "sCustomPMLField11", "sCustomPMLField12",
                            "sCustomPMLField13", "sCustomPMLField14", "sCustomPMLField15", "sCustomPMLField16", "sCustomPMLField17", "sCustomPMLField18", "sCustomPMLField19",
                            "sCustomPMLField20" };
                        if (correctedFieldName.StartsWith("today+", StringComparison.OrdinalIgnoreCase) || correctedFieldName.StartsWith("today-", StringComparison.OrdinalIgnoreCase))
                        {
                            // NO-OP
                        }
                        else if (correctedFieldName.Equals("sAgentXmlContent", StringComparison.OrdinalIgnoreCase))
                        {
                            item = AddFieldToTracking(status, correctedFieldName, () => this.sAgentXmlContent.Value, () => this.sAgentXmlContent.Value);
                        }
                        else if (correctedFieldName.Equals("sClosingCostSet", StringComparison.OrdinalIgnoreCase))
                        {
                            item = AddFieldToTracking(status, correctedFieldName, () => this.sClosingCostSetJsonContent, () => this.sClosingCostSetJsonContent);
                        }
                        else if (correctedFieldName.Equals("sSellerResponsibleClosingCostSet", StringComparison.OrdinalIgnoreCase))
                        {
                            item = AddFieldToTracking(status, correctedFieldName, () => this.sSellerResponsibleClosingCostSetJsonContent, () => this.sSellerResponsibleClosingCostSetJsonContent);
                        }
                        else if (correctedFieldName.Equals(nameof(sClosingDisclosureDatesInfo), StringComparison.OrdinalIgnoreCase))
                        {
                            item = AddFieldToTracking(status, correctedFieldName, () => this.sClosingDisclosureDatesJSONContent, () => this.sClosingDisclosureDatesJSONContent);
                        }
                        else if (correctedFieldName.Equals(nameof(sLoanEstimateDatesInfo), StringComparison.OrdinalIgnoreCase))
                        {
                            item = AddFieldToTracking(status, correctedFieldName, () => this.sLoanEstimateDatesJSONContent, () => this.sLoanEstimateDatesJSONContent);
                        }
                        else if (correctedFieldName.Equals(nameof(sAdjustmentListJson), StringComparison.OrdinalIgnoreCase))
                        {
                            item = AddFieldToTracking(status, correctedFieldName, () => this.sAdjustmentListJson, () => this.sAdjustmentListJson);
                        }
                        else if (sCustomPMLFieldList.Contains(correctedFieldName, StringComparer.OrdinalIgnoreCase))
                        {
                            // 10/8/2014 dd - Support sCustomPMLFieldXXX. This is discover on CMG.
                            CPageData dataLoan = this.InternalCPageData;

                            if (x_loanFileFieldDictionary.TryGetValue(correctedFieldName, out item) == false)
                            {
                                item = new FieldOriginalCurrentItem();
                                x_loanFileFieldDictionary.Add(correctedFieldName, item);
                            }

                            // Find the index. The sCustomPMLFieldList must contains name in order.
                            string value = string.Empty;
                            if (correctedFieldName.Equals("sCustomPMLField1", StringComparison.OrdinalIgnoreCase)) 
                            {
                                value = this.sCustomPMLField1;
                            }
                            else if (correctedFieldName.Equals("sCustomPMLField2", StringComparison.OrdinalIgnoreCase))
                            {
                                value = this.sCustomPMLField2;
                            }
                            else if (correctedFieldName.Equals("sCustomPMLField3", StringComparison.OrdinalIgnoreCase))
                            {
                                value = this.sCustomPMLField3;
                            }
                            else if (correctedFieldName.Equals("sCustomPMLField4", StringComparison.OrdinalIgnoreCase))
                            {
                                value = this.sCustomPMLField4;
                            }
                            else if (correctedFieldName.Equals("sCustomPMLField5", StringComparison.OrdinalIgnoreCase))
                            {
                                value = this.sCustomPMLField5;
                            }
                            else if (correctedFieldName.Equals("sCustomPMLField6", StringComparison.OrdinalIgnoreCase))
                            {
                                value = this.sCustomPMLField6;
                            }
                            else if (correctedFieldName.Equals("sCustomPMLField7", StringComparison.OrdinalIgnoreCase))
                            {
                                value = this.sCustomPMLField7;
                            }
                            else if (correctedFieldName.Equals("sCustomPMLField8", StringComparison.OrdinalIgnoreCase))
                            {
                                value = this.sCustomPMLField8;
                            }
                            else if (correctedFieldName.Equals("sCustomPMLField9", StringComparison.OrdinalIgnoreCase))
                            {
                                value = this.sCustomPMLField9;
                            }
                            else if (correctedFieldName.Equals("sCustomPMLField10", StringComparison.OrdinalIgnoreCase))
                            {
                                value = this.sCustomPMLField10;
                            }
                            else if (correctedFieldName.Equals("sCustomPMLField11", StringComparison.OrdinalIgnoreCase))
                            {
                                value = this.sCustomPMLField11;
                            }
                            else if (correctedFieldName.Equals("sCustomPMLField12", StringComparison.OrdinalIgnoreCase))
                            {
                                value = this.sCustomPMLField12;
                            }
                            else if (correctedFieldName.Equals("sCustomPMLField13", StringComparison.OrdinalIgnoreCase))
                            {
                                value = this.sCustomPMLField13;
                            }
                            else if (correctedFieldName.Equals("sCustomPMLField14", StringComparison.OrdinalIgnoreCase))
                            {
                                value = this.sCustomPMLField14;
                            }
                            else if (correctedFieldName.Equals("sCustomPMLField15", StringComparison.OrdinalIgnoreCase))
                            {
                                value = this.sCustomPMLField15;
                            }
                            else if (correctedFieldName.Equals("sCustomPMLField16", StringComparison.OrdinalIgnoreCase))
                            {
                                value = this.sCustomPMLField16;
                            }
                            else if (correctedFieldName.Equals("sCustomPMLField17", StringComparison.OrdinalIgnoreCase))
                            {
                                value = this.sCustomPMLField17;
                            }
                            else if (correctedFieldName.Equals("sCustomPMLField18", StringComparison.OrdinalIgnoreCase))
                            {
                                value = this.sCustomPMLField18;
                            }
                            else if (correctedFieldName.Equals("sCustomPMLField19", StringComparison.OrdinalIgnoreCase))
                            {
                                value = this.sCustomPMLField19;
                            }
                            else if (correctedFieldName.Equals("sCustomPMLField20", StringComparison.OrdinalIgnoreCase))
                            {
                                value = this.sCustomPMLField20;
                            }

                            if (status == TrackFieldStatusT.Original)
                            {
                                item.Original = value;
                            }
                            else if (status == TrackFieldStatusT.Current)
                            {
                                item.Current = value;
                            }
                        }
                        else
                        {
                            //find some way to centralize this.
                            var ignoreList = new HashSet<string>(StringComparer.OrdinalIgnoreCase) {
                                "scope",
                                "alwaystrue",
                                "usergroup",
                                "sleadsrcidenum",
                                "role",
                                "ocgroup",
                                "isassigned",
                                "lien",
                                "canwritenonassignedloan",
                            };

                            if (!ignoreList.Contains(correctedFieldName))
                            {
                                Tools.LogInfo("The field " + correctedFieldName + " is not in the datalayer but needs to be tracked.");
                            }
                        }
                    }
                }
                ByPassFieldSecurityCheck = oldByPass;
            }
        }

        private FieldOriginalCurrentItem AddFieldToTracking(TrackFieldStatusT status, string correctedFieldName, Func<string> originalGetter, Func<string> currentGetter)
        {
            FieldOriginalCurrentItem item;
            if (!x_loanFileFieldDictionary.TryGetValue(correctedFieldName, out item))
            {
                item = new FieldOriginalCurrentItem();
                x_loanFileFieldDictionary.Add(correctedFieldName, item);
            }

            if (status == TrackFieldStatusT.Original)
            {
                item.Original = originalGetter();
            }
            else if (status == TrackFieldStatusT.Current)
            {
                item.Current = currentGetter();
            }

            return item;
        }

        private HashSet<string> trackedNonDataLayerFields = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
        {
            "sAgentXmlContent",
            "sClosingCostSet",
            "sSellerResponsibleClosingCostSet",
            nameof(sAdjustmentListJson),
            nameof(sClosingDisclosureDatesInfo),
            nameof(sLoanEstimateDatesInfo)
        };

        private Dictionary<string, FieldOriginalCurrentItem> x_loanFileFieldDictionary = new Dictionary<string, FieldOriginalCurrentItem>(StringComparer.OrdinalIgnoreCase);
        private Dictionary<Guid, Dictionary<string, FieldOriginalCurrentItem>> x_appDataFieldDictionary = new Dictionary<Guid, Dictionary<string, FieldOriginalCurrentItem>>();
        /// <summary>
        /// 12/15/2011 dd - This method will return whether the field name is modify or not. In order for it to work correctly, the field must be include
        /// in GetListOfFieldsToSaveSnapshot()
        /// The field name can be
        ///      Regular loan file: i.e: sLNm
        ///      Calculate field: i.e: sLAmtCalc
        ///      App field: i.e: aBNm - In multiple apps file, the field will consider to be modify if value for one of the app change.
        /// </summary>
        /// <param name="fieldName"></param>
        private bool IsFieldModified(string fieldName)
        {
            bool isModified = false;

            E_PageDataClassType pageDataClassType;
            bool isFound = PageDataUtilities.GetPageDataClassType(fieldName, out pageDataClassType);
            FieldOriginalCurrentItem item = null;

            if (isFound)
            {
                switch (pageDataClassType)
                {
                    case E_PageDataClassType.CBasePage:
                        if (x_loanFileFieldDictionary.TryGetValue(fieldName, out item) == true)
                        {
                            //dont want to deal with null handling.
                            string original = StaticMethodsAndExtensions.NormalizeNewLines(item.Original);
                            string current = StaticMethodsAndExtensions.NormalizeNewLines(item.Current);
                            isModified = String.Equals(original, current) == false;  
                        }
                        break;

                    case E_PageDataClassType.CAppBase:
                        foreach (var dataAppDictionary in x_appDataFieldDictionary.Values)
                        {
                            if (dataAppDictionary.TryGetValue(fieldName, out item) == true)
                            {
                                string original = StaticMethodsAndExtensions.NormalizeNewLines(item.Original);
                                string current = StaticMethodsAndExtensions.NormalizeNewLines(item.Current);

                                if (String.Equals(original, current) == false)
                                {
                                    isModified = true;
                                    break; // As long as one app is change we consider field is modified.
                                }
                            }
                        }
                        break;

                    default:
                        throw new UnhandledEnumException(pageDataClassType);
                }
            }
            else if (trackedNonDataLayerFields.Contains(fieldName) && x_loanFileFieldDictionary.TryGetValue(fieldName, out item) == true)
            {
                isModified = !item.Original.Equals(item.Current, StringComparison.OrdinalIgnoreCase);
            }

            return isModified;
        }

        private IEnumerable<string> GetListOfModifiedTrackFields()
        {
            List<string> list = new List<string>();

            foreach (var fieldName in GetFieldListInSnapshot())
            {
                if (IsFieldModified(fieldName))
                {
                    list.Add(fieldName);
                }
            }
            return list;
        }

        private ILoanFileFieldValidator m_loanfileFieldValidator = null;
        internal void SetLoanFileFieldValidator(ILoanFileFieldValidator validator)
        {
            m_loanfileFieldValidator = validator;
        }

        /// <summary>
        /// Clears out member variables that should be reloaded from the database.
        /// </summary>
        /// <remarks>
        /// As of 12/12/17, this does not correctly clear *all* of the fields that should be reloaded. gf
        /// </remarks>
        private void ClearAllDbRelatedData()
        {
            this.DataState = E_DataState.Normal;
            this.InvalidateCache();
            this.m_dsLoan = null;
            this.m_dsApp = null;
            this.m_dsEmployee = null;
            this.m_dataApps = null;
            this.m_rowLoan = null;
            this.m_employeeCallCenterAgent = CEmployeeFields.Empty;
            this.m_employeeLoanRep = CEmployeeFields.Empty;
            this.m_employeeManager = CEmployeeFields.Empty;
            this.m_employeeProcessor = CEmployeeFields.Empty;
            this.m_employeeRealEstateAgent = CEmployeeFields.Empty;
            this.m_employeeLenderAccExec = CEmployeeFields.Empty;
            this.m_employeeLockDesk = CEmployeeFields.Empty;
            this.m_employeeUnderwriter = CEmployeeFields.Empty;
            this.m_employeeLoanOpener = CEmployeeFields.Empty;

            // TODO: Ensure all CEmployeeFields members are properly cleared here.

            this.m_dirtyFields = null;
            this.m_largeFieldStorage = new LargeFieldStorage(this.m_fileId);
        }

        protected override void OnFieldChange(string fieldName)
        {
            if (this.m_dataState == E_DataState.InitSave)
            {
                m_dirtyFields[fieldName] = null;
            }
        }

        internal void MarkFieldDirty(string fieldName)
        {
            OnFieldChange(fieldName);
        }

        protected override void OnFieldChange(string strFieldNm, SqlDbType dbType, object oldVal, object newVal)
        {
            if (E_DataState.InitSave == m_dataState)
                m_dirtyFields[strFieldNm] = dbType;
        }


        /// <summary>
        /// Gets a read-only SQL connection. Does not open the connection.
        /// </summary>
        /// <returns>A SQL connection that is not opened.</returns>
        private DbConnection GetConnectionForReadOnly()
        {
            DbConnectionInfo connInfo = GetConnectionInfo();
            DbConnection conn = connInfo.GetReadOnlyConnection();
            return conn;
        }

        /// <summary>
        /// Gets a SQL connection. Does not open the connection.
        /// </summary>
        /// <returns>A SQL connection that is not opened.</returns>
        private DbConnection GetConnection()
        {
            DbConnectionInfo connInfo = GetConnectionInfo();
            DbConnection conn = connInfo.GetConnection();
            return conn;
        }

        private DbConnectionInfo GetConnectionInfo()
        {
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;

            DbConnectionInfo connInfo = null;

            if (principal != null)
            {
                connInfo = principal.ConnectionInfo;
            }

            if (connInfo == null)
            {
                Guid brokerId = Guid.Empty;
                connInfo = DbConnectionInfo.GetConnectionInfoByLoanId(this.m_fileId, out brokerId);
            }

            return connInfo;
        }

        /// <summary>
        /// Groups dirty field ids together by tables, creates SQL parameters for them, and
        /// associates the SQL parameters with one of the SQL command members.
        /// </summary>
        /// <remarks>
        /// When performing a standard save (not a cache update):
        /// * Group dirty field ids together by table
        /// * Create SQL parameters for the dirty fields and associate SQL parameters with an update
        ///   command (ie m_cmdUpdateLoan, m_cmdUpdateApp)
        /// When performing a cache update:
        /// * Select the potentially invalided cached fields from the loan file cache
        /// * Determine which fields actually need to be updated in the cache
        /// * Group the dirty field ids together by table
        /// * Create SQL parameters for the dirty fields and associate them with an update command
        /// * Update the cache data sets row to have the updated value.
        /// </remarks>
        private void PrepareUpdateCommandParameters()
        {
            CFieldInfoTable fieldInfoTable = CFieldInfoTable.GetInstance();

            if (!this.IsCachingUtilityObj)
            {
                foreach (DictionaryEntry dirtyField in this.m_dirtyFields)
                {
                    if (null == dirtyField.Value)
                    {
                        continue; // not meant to be saved to db
                    }

                    string strFieldNm = dirtyField.Key.ToString();
                    SqlDbType dbType = (SqlDbType)dirtyField.Value;

                    StringCollection fields;
                    DbParameterCollection parameters;

                    FieldProperties fieldProps = fieldInfoTable[strFieldNm];
                    CFieldDbInfo dbInfo = fieldProps.DbInfo;

                    switch (dbInfo.m_dbTable)
                    {
                        case E_DbSrcTable.Loan_A:
                            fields = this.m_dirtyFieldsLoanA;
                            parameters = this.m_cmdUpdateLoan.Parameters;
                            break;
                        case E_DbSrcTable.Loan_B:
                            fields = this.m_dirtyFieldsLoanB;
                            parameters = this.m_cmdUpdateLoan.Parameters;
                            break;
                        case E_DbSrcTable.Loan_C:
                            fields = this.m_dirtyFieldsLoanC;
                            parameters = this.m_cmdUpdateLoan.Parameters;
                            break;
                        case E_DbSrcTable.Loan_D:
                            fields = this.m_dirtyFieldsLoanD;
                            parameters = this.m_cmdUpdateLoan.Parameters;
                            break;
                        case E_DbSrcTable.Loan_E:
                            fields = this.m_dirtyFieldsLoanE;
                            parameters = this.m_cmdUpdateLoan.Parameters;
                            break;
                        case E_DbSrcTable.Loan_F:
                            fields = this.m_dirtyFieldsLoanF;
                            parameters = this.m_cmdUpdateLoan.Parameters;
                            break;
                        case E_DbSrcTable.App_A:
                            fields = this.m_dirtyFieldsAppA;
                            parameters = this.m_cmdUpdateApp.Parameters;
                            break;
                        case E_DbSrcTable.App_B:
                            fields = this.m_dirtyFieldsAppB;
                            parameters = this.m_cmdUpdateApp.Parameters;
                            break;
                        case E_DbSrcTable.Employee:
                            this.LogWarning(strFieldNm + " should not go through CPageBase:PrepareUpdateCommandParameters()");
                            continue;
                        case E_DbSrcTable.Team:
                            this.LogWarning(strFieldNm + " should not go through CPageBase:PrepareUpdateCommandParameters()");
                            continue;
                        case E_DbSrcTable.Trust_Account:
                            fields = this.m_dirtyFieldsTrushAcc;
                            parameters = this.m_cmdUpdateLoan.Parameters;
                            break;
                        default:
                            throw new CBaseException(ErrorMessages.Generic, "Unhandled table source in CPageBase:PrepareUpdateCommandParameters()");
                    }

                    SqlParameter param = new SqlParameter("@p" + strFieldNm, dbType);
                    param.SourceVersion = DataRowVersion.Current;
                    param.SourceColumn = strFieldNm;

                    if (dbInfo.m_mumericPrecision != DBNull.Value)
                    {
                        param.Precision = Convert.ToByte(dbInfo.m_mumericPrecision);
                    }

                    if (dbInfo.m_numericScale != DBNull.Value)
                    {
                        param.Scale = Convert.ToByte(dbInfo.m_numericScale);
                    }

                    parameters.Add(param);
                    fields.Add(strFieldNm);
                }
            }
            else if (null != this.InvalidatedCachedFields)
            {
                StringBuilder selectFields = new StringBuilder(50);

                // Pulling only the set of fields that potentially get changed
                foreach (string strFieldNm in this.InvalidatedCachedFields)
                {
                    selectFields.Append(",");
                    selectFields.Append(strFieldNm);
                }

                var listParams = new SqlParameter[] { new SqlParameter("@slid", this.sLId) };
                string hint = ConstStage.IsLoanForceOrderHintEnabled ? "OPTION (FORCE ORDER)" : string.Empty;
                string sCacheQuery = string.Format("select top 1 c1.sLId {0} from loan_file_cache c1 join loan_file_cache_2 c2 on c1.slid = c2.slid join loan_file_cache_3 c3 on c1.slid=c3.slid join loan_file_cache_4 c4 on c1.slid=c4.slid where c1.sLId = @slid {1}", selectFields.ToString(), hint);
                this.m_dsCache = new DataSet("CACHE");
                using (DbConnection conn = this.GetConnectionForReadOnly())
                {
                    DBSelectUtility.FillDataSet(conn, null, this.m_dsCache, sCacheQuery, null, listParams);
                }

                DataRow rowCache = this.m_dsCache.Tables[0].Rows[0];

                DbParameterCollection pars = this.m_cmdUpdateCache.Parameters;

                StringBuilder cacheFieldsError = new StringBuilder();
                foreach (string strFieldNm in this.InvalidatedCachedFields)
                {
                    FieldProperties field = fieldInfoTable[strFieldNm];

                    StringCollection dirtyFields;

                    switch (field.DbInfo.m_cacheTable)
                    {
                        case E_DbCacheTable.Loan_Cache_1:
                            dirtyFields = this.m_dirtyFieldsLoanCache;
                            break;
                        case E_DbCacheTable.Loan_Cache_2:
                            dirtyFields = this.m_dirtyFieldsLoanCache2;
                            break;
                        case E_DbCacheTable.Loan_Cache_3:
                            dirtyFields = this.m_dirtyFieldsLoanCache3;
                            break;
                        case E_DbCacheTable.Loan_Cache_4:
                            dirtyFields = this.m_dirtyFieldsLoanCache4;
                            break;
                        case E_DbCacheTable.None:
                        default:
                            throw new CBaseException(ErrorMessages.Generic, string.Format("Table source {0} is not expected for preparing cmd for loan caching. Field name is {1}", field.DbInfo.m_dbTable.ToString(), strFieldNm));
                    }

                    if (!dirtyFields.Contains(strFieldNm))
                    {
                        if (null == field || null == field.DbInfo)
                        {
                            this.LogError(string.Format("{0} doesn't have db info structure and it should be because it's a cached field.", strFieldNm));
                            continue;
                        }

                        CFieldDbInfo dbInfo = field.DbInfo;
                        object newVal = dbInfo.GetValueOf(this);

                        if (newVal == null)
                        {
                            newVal = DBNull.Value;
                        }
                        else
                        {
                            if (newVal is Enum)
                            {
                                newVal = (int)newVal;
                            }
                            else if (newVal is CDateTime)
                            {
                                CDateTime dt = newVal as CDateTime;
                                if (dbInfo.m_dbType == SqlDbType.SmallDateTime && dt.IsValid && dt.IsValidSmallDateTime == false)
                                {
                                    newVal = DBNull.Value;
                                    cacheFieldsError.AppendFormat(", field[{0}]={1} is overflow default to Null", dbInfo.m_fieldName, dt);
                                }
                                else
                                {
                                    newVal = dt.DateTimeForDBWriting;
                                }
                            }
                            else if (newVal is decimal)
                            {
                                // 2/11/2014 dd - To Avoid Overflow exception in LOAN_FILE_CACHE table, save NULL when value is decimal.MinValue
                                // 2/16/2017 gf - We now have more advanced checks for decimal overflows in the data adapter event method.
                                if (((decimal)newVal) == decimal.MinValue)
                                {
                                    newVal = DBNull.Value;
                                }
                            }
                        }

                        object oldVal = rowCache[strFieldNm];

                        //if this is the first attempt at running the cache update requests, ignore 
                        //fields whose oldVal and newVal are the same.  If this is not the first time
                        //the oldVal will not have been preserved, so all update requests must be attempted
                        if (!this.m_rerunningCacheUpdateRequest)
                        {
                            // TODO: Review and eliminate some of these comparisons. 
                            if (oldVal.Equals(newVal)) // this won't detect 2 decimal being the same!!!
                                continue;

                            if (object.Equals(oldVal, newVal))
                                continue;

                            if (oldVal == newVal) // this won't detect 2 strings being the same for some strings (bug in .net? )
                                continue;

                            // makes 2 strings equal if they are made of just (differing amounts of) space
                            // av Turns out DBNull.Value.toString() == "" which means dbnulls dont get updated when running cache request
                            // Array types will return their type name for ToString and should ignore this check.
                            if (oldVal != DBNull.Value && 
                                !oldVal.GetType().IsArray &&
                                oldVal.ToString().TrimWhitespaceAndBOM().Equals(newVal.ToString().TrimWhitespaceAndBOM()))
                            {
                                continue;
                            }
                        }

                        SqlParameter par = new SqlParameter("@p" + strFieldNm, dbInfo.m_dbType);
                        par.SourceVersion = DataRowVersion.Current;
                        par.SourceColumn = strFieldNm;

                        if (dbInfo.m_mumericPrecision != DBNull.Value)
                        {
                            par.Precision = Convert.ToByte(dbInfo.m_mumericPrecision);
                        }

                        if (dbInfo.m_numericScale != DBNull.Value)
                        {
                            par.Scale = Convert.ToByte(dbInfo.m_numericScale);
                        }

                        pars.Add(par);
                        dirtyFields.Add(strFieldNm);

                        try
                        {
                            rowCache[strFieldNm] = (null == newVal) ? DBNull.Value : newVal;
                        }
                        catch
                        {
                            rowCache[strFieldNm] = DBNull.Value;
                        }
                    }
                }

                if (cacheFieldsError.Length > 0)
                {
                    Tools.LogError(cacheFieldsError.ToString());
                }
            }
        }
    }
}
