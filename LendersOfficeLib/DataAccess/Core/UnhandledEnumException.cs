﻿namespace DataAccess
{
    using System;
    using LendersOffice.Common;

    public class UnhandledEnumException : CBaseException
    {
        public UnhandledEnumException(Enum v)
            : base(ErrorMessages.Generic, "Unhandled enum value=" + v.GetType() + "." + v.ToString())
        {
        }

        public UnhandledEnumException(Enum v, string message)
            : base(ErrorMessages.Generic, "Unhandled enum value=" + v.GetType() + "." + v.ToString() + ". " + message)
        {
        }
    }
}
