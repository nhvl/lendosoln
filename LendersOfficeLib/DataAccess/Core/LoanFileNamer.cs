using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

using LendersOffice.Admin;
using LendersOffice.Common;
using System.Collections.Generic;
using CommonProjectLib.Common.Lib;
using System.Linq;
using System.Text.RegularExpressions;
namespace DataAccess
{
    /// <summary>
    /// We split out loan naming so that we have a single point
    /// of loan naming for loan file creation and for loan file
    /// editing (we now allow a loan to be renamed with an auto
    /// name).
    /// 
    /// 1/26/2005 kb - We'll use this in the auto naming function
    /// for loan file creator.  Refactoring after this update.
    /// </summary>

    internal static class CLoanFileNamerExtensions
    {
        public static string Truncate(this string str, int count)
        {
            if (string.IsNullOrEmpty(str) || str.Length <= count) return str ?? "";
            return str.Substring(0, count);
        }

        public static string Truncate(this string str, int start, int count)
        {
            if (string.IsNullOrEmpty(str) || str.Length <= start) return "";
            var maxCount = Math.Min(str.Length, start + count) - start;

            return str.Substring(start, maxCount);
        }
    }

    public class CLoanFileNamer
    {
        public class LoanNamingData
        {
            public readonly string namingPattern;
            public readonly long namingCounter;
            public readonly long refCounter;
            public readonly string refPattern;
            public readonly string branchLNmPrefix;
            public readonly long mersCounter;
            public readonly string mersOrganizationId;
            public readonly string mersPattern;
            public readonly DateTime dt = DateTime.Now;

            public bool HasMersId
            {
                get
                {
                    return !string.IsNullOrEmpty(mersOrganizationId);
                }
            }
            public bool NameIsDefaultMERSPattern
            {
                get
                {
                    return namingPattern.IndexOf("{mers:") >= 0;
                }
            }

            public bool HasBranchPrefix
            {
                get{
                    return !string.IsNullOrEmpty(branchLNmPrefix);
                }
            }

            SqlParameter namingPatternParameter;
            SqlParameter namingCounterParameter;
            SqlParameter refCounterParameter;
            SqlParameter refPatternParameter;
            SqlParameter branchLNmPrefixParameter;
            SqlParameter mersOrganizationIdParameter;
            SqlParameter mersCounterParameter;

            protected List<SqlParameter> GetParameters(Guid brokerId, Guid branchId, bool useLeadNumbering, bool useTestNumbering, bool enableMersGeneration)
            {
                List<SqlParameter> parameters = new List<SqlParameter>(8);
                parameters.Add(new SqlParameter("@BrokerId", brokerId));
                parameters.Add(new SqlParameter("@BranchId", branchId));

                namingPatternParameter = new SqlParameter("@NamingPattern", SqlDbType.VarChar, 50);
                namingPatternParameter.Direction = ParameterDirection.Output;

                namingCounterParameter = new SqlParameter("@NamingCounter", SqlDbType.BigInt);
                namingCounterParameter.Direction = ParameterDirection.Output;

                branchLNmPrefixParameter = new SqlParameter("@BranchLNmPrefix", SqlDbType.VarChar, 10);
                branchLNmPrefixParameter.Direction = ParameterDirection.Output;

                mersOrganizationIdParameter = new SqlParameter("@MersOrganizationId", SqlDbType.VarChar, 7);
                mersOrganizationIdParameter.Direction = ParameterDirection.Output;

                mersCounterParameter = new SqlParameter("@MersCounter", SqlDbType.BigInt);
                mersCounterParameter.Direction = ParameterDirection.Output;

                refCounterParameter = new SqlParameter("@ReferenceCounter", SqlDbType.BigInt);
                refCounterParameter.Direction = ParameterDirection.Output;

                refPatternParameter = new SqlParameter("@ReferenceNamingPattern", SqlDbType.VarChar, 50);
                refPatternParameter.Direction = ParameterDirection.Output;

                parameters.Add(namingCounterParameter);
                parameters.Add(branchLNmPrefixParameter);

                if (useTestNumbering == false)
                {
                    parameters.Add(namingPatternParameter);
                    parameters.Add(refCounterParameter);
                    parameters.Add(refPatternParameter);
                }

                if (useLeadNumbering == false && useTestNumbering == false)
                {
                    parameters.Add(new SqlParameter("@EnableMersGeneration", enableMersGeneration));
                    parameters.Add(mersOrganizationIdParameter);
                    parameters.Add(mersCounterParameter);
                }

                return parameters;
            }

            public LoanNamingData(Guid brokerId, Guid branchId, bool useLeadNumbering, bool useTestNumbering, bool enableMersGeneration, bool checkNameOnly)
            {
                // OPM 243181 - Add broker option to use Loan counter for leads.
                BrokerDB broker = BrokerDB.RetrieveById(brokerId);
                useLeadNumbering &= !broker.IsForceLeadToUseLoanCounter;

                var parameters = GetParameters(brokerId, branchId, useLeadNumbering, useTestNumbering, enableMersGeneration);

                if (checkNameOnly)
                {
                    parameters.Add(new SqlParameter("@UpdateCounters", false));
                }

                string spName;
                if (useTestNumbering)
                {
                    spName = "GetTestNamingPatternAndCounter";
                }
                else if (useLeadNumbering)
                {
                    spName = "GetLeadNamingPatternAndCounter";
                }
                else
                {
                    spName = "GetNamingPatternAndCounter";
                }

                StoredProcedureHelper.ExecuteNonQuery(brokerId, spName, 5, parameters);

                namingCounter = (long)namingCounterParameter.Value; //If this one is null there's serious problems, so it's ok to throw
                branchLNmPrefix = branchLNmPrefixParameter.Value == DBNull.Value ? "" : (string) branchLNmPrefixParameter.Value;

                if (useTestNumbering)
                {
                    namingPattern = "{yyyy}{mm}{c:4}";
                }
                else
                {
                    refCounter = (long)refCounterParameter.Value;
                    refPattern = (refPatternParameter.Value == DBNull.Value || string.IsNullOrEmpty((string)refPatternParameter.Value)) ? "{yy}{mm}{dd}{c:5}" : (string)refPatternParameter.Value;
                    namingPattern = (namingPatternParameter.Value == DBNull.Value || string.IsNullOrEmpty((string)namingPatternParameter.Value)) ? "{yy}{mm}{dd}{c:5}" : (string)namingPatternParameter.Value;
                }

                mersCounter = 0;
                mersOrganizationId = string.Empty;

                if (useLeadNumbering == false && useTestNumbering == false)
                {
                    if (mersCounterParameter.Value != DBNull.Value)
                    {
                        mersCounter = (long)mersCounterParameter.Value;
                    }
                    if (mersOrganizationIdParameter.Value != DBNull.Value)
                    {
                        mersOrganizationId = (string)mersOrganizationIdParameter.Value;
                    }
                }

                mersPattern = broker.MersFormat;
            }
        }

        /// <summary>
        /// Generate new auto name for this broker.  If the broker
        /// uses sequential auto naming, then his persisted counter
        /// will get incremented.
        /// </summary>
        public string GetLoanAutoName(Guid brokerId, Guid branchId, bool useLeadNumbering, bool useTestNumbering, bool enableMersGeneration, out string mersMin, out string refNm)
        {
            // 12/29/2004 kb - Get the auto naming strategy for this
            // broker.
            bool checkNameOnly = false;
            LoanNamingData nameInfo = new LoanNamingData(brokerId, branchId, useLeadNumbering, useTestNumbering, enableMersGeneration, checkNameOnly);

            string sAuto = CreateLoanName(nameInfo);

            if (sAuto.Length > 30)
            {
                throw new CBaseException(ErrorMessages.Generic, "Loan auto name too long (must be <= 30 characters).");
            }

            refNm = useTestNumbering ? string.Empty : CreateRefNm(nameInfo);

            mersMin = CreateMersMin(nameInfo);

            return sAuto;
        }

        public string GetLoanAutoNameAddPrefix(Guid brokerId, bool useLeadNumbering, bool useTestNumbering, Guid branchId, string sPrefix, bool enableMersGeneration, out string mersMin, out string RefNm)
        {
            string sAuto = GetLoanAutoName(brokerId, branchId, useLeadNumbering, useTestNumbering, enableMersGeneration, out mersMin, out RefNm);
            string sName = CombinePrefix(sPrefix, sAuto);

            return sName;
        }

        private static string CombinePrefix(string sPrefix, string sAuto)
        {
            string sName = sPrefix + sAuto;

            if (sName.Length > 36)
            {
                if (sAuto.Length > 30)
                {
                    throw new CBaseException(ErrorMessages.Generic, "Loan auto name too long (must be <= 30 characters).");
                }

                sName = sPrefix.Substring(0, sPrefix.Length - (sName.Length - 35)) + "+" + sAuto;
            }
            return sName;
        }

        /// <summary>
        /// Generate loan name from pattern and counter. I (dd) decide to move the algorithm for transform naming pattern to actual name from
        /// stored procedure to C# code. The reason is naming pattern need to support MERS number generation, which is too complicate
        /// to implement in stored procedure.
        /// 
        /// Keyword for pattern.
        ///   {yyyy}  - Year in 4 digits
        ///   {yy}    - Year in 2 digits
        ///   {mm}    - Month in 2 digits, append leading 0 for month less than 10
        ///   {m}     - Month in 1 digits
        ///   {dd}    - Days in 2 digits, append leading 0 for day less than 10
        ///   {d}     - Days either in single or double digit. No leading 0 is append.
        ///   {c:xx}  - xx digit counter. Note that this counter occasionally rolls over depending on the rest of the pattern 
        ///                            (see sproc GetNamingPatternAndCounter)
        ///   {mers:xxxxxxx} - xx is 7 digit MERS organization number -> results in the full 18 MERS loan id using the 10 digit MERS counter and MERS checksum
        ///                    (equivalent to {mersID}{mersCounter}, except the {mersID} is hardcoded on a lead)
        ///   
        ///   {mersCounter} - 10 digit MERS counter only (is separate from the normal c:xx counter). This counter does not roll over.
        ///                    
        ///   {mersID} - 7 digit MERS organization number. Using this automatically implies the name is an 18 digit MERS number, with the
        ///              id being the first 7 digits, the next 10 digits being a counter of some sort, and the 18th digit a check digit.
        ///               If the counter is longer than 10 digits, it will be truncated from the right; if it is shorter, it will be zero-padded
        ///              on the left.
        ///               Any non-numeric elements will be stripped out. This pattern is applied last, as it affects the rest of the name.
        /// 
        ///   
        /// </summary>
        /// <param name="namingPattern"></param>
        /// <param name="counter"></param>
        /// <returns></returns>
        /// 
        public static string CreateLoanName(LoanNamingData info)
        {
            string name;

            if (info.NameIsDefaultMERSPattern)
            {
                name = GenerateMERSMinName(info);
            }
            else
            {
                name = GenerateSequentialName(info);
            }

            return name;
        }

        public static string CreateRefNm(LoanNamingData info)
        {
            string name;

            if (info.NameIsDefaultMERSPattern)
            {
                name = GenerateMERSMinName(info);
            }
            else
            {
                name = ApplyPatterns(info.refPattern, info, false, true);
            }

            return name;
        }

        /// <summary>
        /// Creates a standard MersMin loan name using the normal {mersID} {sequential mers counter} format.
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        private static string GenerateMERSMinName(LoanNamingData info)
        {
            // Generate MERS number naming scheme.
            int _startIndex = info.namingPattern.IndexOf("{mers:");
            int _endIndex = info.namingPattern.IndexOf("}", _startIndex);

            string oldValue = info.namingPattern.Substring(_startIndex, _endIndex - _startIndex + 1);

            string mers = string.Empty;
            if (string.IsNullOrEmpty(info.mersOrganizationId))
            {
                // 4/16/2012 dd - When create lead or template, we use the mers organization and counter from regular naming scheme.
                string organizationId = info.namingPattern.Substring(_startIndex + 6, _endIndex - _startIndex - 6);
                mers = MersUtilities.GenerateMersNumber(organizationId, info.namingCounter);
            }
            else
            {
                mers = MersUtilities.GenerateMersNumber(info); // 4/16/2012 dd - Use MersCounter so loan number and Mers # are match.
            }

            string name = info.namingPattern.Replace(oldValue, mers);
            return name;
        }

        /// <summary>
        /// Creates a MersMin based on the broker's MersFormat pattern.
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        private static string CreateMersMin(LoanNamingData info)
        {
            //Sometimes we're generating a default mers min (equivalent to {mers:12345} or {mersId}{mersCounter})
            if (string.IsNullOrEmpty(info.mersPattern)) return MersUtilities.GenerateMersNumber(info);

            //Otherwise, the mersMin has a pattern similar to the loan name
            return ApplyPatterns(info.mersPattern, info, true, false);
        }

        private static string GenerateSequentialName(LoanNamingData info)
        {
            return ApplyPatterns(info.namingPattern, info, false, false);
        }

        /// <summary>
        /// Takes a string and turns it into a valid Mers MIN. This means the function does the following: 
        /// Strips out everything that isn't a digit, 
        /// grabs the first 7 digits as a Mers org ID, 
        /// grabs the next 10 digits (zero padded on the left if less than 10 digits) as a Mers counter, 
        /// calculates the check digits for those 17 digits, appends it and returns an 18 digit Mers MIN
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private static string ConvertToMersMIN(string name)
        {
            name = onlyDigits.Replace(name, "");

            var mersId = name.Truncate(7);
            var mersCounter = long.Parse(name.Truncate(7, 10));

            return MersUtilities.GenerateMersNumber(mersId, mersCounter);
        }

        /// <summary>
        /// Applies the naming patterns described in the comment over CreateLoanName
        /// </summary>
        /// <param name="pattern"></param>
        /// <param name="info"></param>
        /// <returns></returns>
        private static string ApplyPatterns(string pattern, LoanNamingData info, bool isForMersMin, bool isForRefNm)
        {
            //Since we want the mersID to be first even if there's some sort of prefix,
            //we'll temporarily remove it.
            bool startsWithMers = pattern.StartsWith("{mersID}");
            pattern = pattern.Replace("{mersID}", "");

            if (isForRefNm)
            {
                // OPM 244087 - Remove prefix for ref number.
                pattern = pattern.Substring(Math.Max(0, CorporatePrefixEndIdx(pattern)));
            }
            else if (info.HasBranchPrefix && (false == isForMersMin || (isForMersMin && Tools.UsesPrefix(pattern))))
            {
                // 3/21/2006 dd - OPM 1905 Use branch prefix.
                pattern = info.branchLNmPrefix + pattern.Substring(Math.Max(0, CorporatePrefixEndIdx(pattern)));
            }

            if (!isForRefNm && startsWithMers) pattern = "{mersID}" + pattern;

            // OPM 244087 - Only include date and counter related parts of pattern for Ref Number,
            // NOTE: mersID and prefix already removed, so just need to remove mersCounter, if it exists.
            if (isForRefNm)
            {
                pattern = pattern.Replace("{mersCounter}", "").Replace("}", "}-").TrimEnd('-');
            }

            foreach (var rep in Replacements)
            {
                pattern = rep(pattern, info, isForRefNm);
            }

            return pattern;
        }

        private static bool HasCorporatePrefix(string pattern)
        {
            return CorporatePrefixEndIdx(pattern) > 0;
        }

        private static int CorporatePrefixEndIdx(string pattern)
        {
            return pattern.IndexOf("{");
        }

        private static readonly Regex onlyDigits = new Regex(@"[^0-9]", RegexOptions.Compiled);
        private static List<Func<string, LoanNamingData, bool, string>> Replacements = new List<Func<string, LoanNamingData, bool, string>>()
        {
            (name, info, isForRefNm) => name.Replace("{yyyy}", info.dt.Year.ToString()),
            (name, info, isForRefNm) => name.Replace("{yy}", info.dt.Year.ToString().Substring(2, 2)),
            (name, info, isForRefNm) => name.Replace("{mm}", info.dt.Month.ToString("D2")),
            (name, info, isForRefNm) => {
                string m = "";
                if (info.dt.Month < 10)
                    m = info.dt.Month.ToString();
                else
                    m = ((char)(info.dt.Month + 55)).ToString(); // Convert to Hex value: 10 - A, 11 - B, 12 - C
                return name.Replace("{m}", m);
            },
            (name, info, isForRefNm) => name.Replace("{dd}", info.dt.Day.ToString("D2")),
            (name, info, isForRefNm) => name.Replace("{d}", info.dt.Day.ToString()),
            (name, info, isForRefNm) => {
                int _startIndex = name.IndexOf("{c:");
                if(_startIndex == -1) return name;

                int _endIndex = name.IndexOf("}", _startIndex);
                int numOfDigits = 0;
                if(!int.TryParse(name.Substring(_startIndex + 3, _endIndex - _startIndex - 3), out numOfDigits))
                {
                    numOfDigits = 0;
                }
                string oldValue = name.Substring(_startIndex, _endIndex - _startIndex + 1);

                long counter = isForRefNm ? info.refCounter : info.namingCounter;
                return name.Replace(oldValue, counter.ToString("D" + numOfDigits));
            },
            (name, info, isForRefNm) => name.Replace("{mersCounter}", isForRefNm ? string.Empty : info.mersCounter.ToString()),
            (name, info, isForRefNm) => {
                if(!name.Contains("{mersID}")) return name;
                // 3/20/14 gf - opm 173853, If we do not have a Mers Org Id then the generated min will be wrong.
                // Handle similarly to MersUtilities.GenerateMersNumber() and just return empty string.
                else if (!info.HasMersId) return "";

                name = name.Replace("{mersID}", isForRefNm ? string.Empty : info.mersOrganizationId);
                return ConvertToMersMIN(name);
            }

        };

        public static string CheckNextMERSMin(Guid brokerId, Guid branchId)
        {
            bool useLead = false;
            bool useTest = false;
            bool enableMers = true;
            bool checkNameOnly = true;
            LoanNamingData nameInfo = new LoanNamingData(brokerId, branchId, useLead, useTest, enableMers, checkNameOnly);

            return CheckNextMERSMin(nameInfo);
        }

        public static string CheckNextMERSMin(LoanNamingData nameInfo)
        {
            var mersMin = CreateMersMin(nameInfo);
            return mersMin;
        }

        /// <summary>
        /// Check the broker's loans to see if an active loan with
        /// the given name exists.
        /// </summary>
        public static bool CheckIfLoanNameExists(Guid brokerId, Guid loanId, String sLoanName)
        {
            // Look for an active loan with that name.

            int iRes;

            SqlParameter[] parameters = {
                                            new SqlParameter("@LoanNumber", sLoanName)
                                            , new SqlParameter("@LoanID", loanId)
                                            , new SqlParameter("@BrokerID", brokerId)
                                        };
            iRes = (int)StoredProcedureHelper.ExecuteScalar(brokerId, "CheckLoanNumber", parameters);

            if (iRes == 0)
            {
                return false;
            }

            return true;
        }
    }
}
