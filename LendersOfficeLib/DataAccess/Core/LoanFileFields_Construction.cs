﻿namespace DataAccess
{
    using System;
    using DataAccess.Core.Construction;
    using LendersOffice.Common;
    using LendersOffice.Migration;
    using LendersOffice.ObjLib.CustomAttributes;

    /// <summary>
    /// <see cref="CPageBase"/> properties for Construction Loans.
    /// </summary>
    public partial class CPageBase
    {
        /// <summary>
        /// Computes the daily construction interest rate based on the current rate
        /// and the interest accrual type.
        /// </summary>
        /// <param name="rate">The current annual interest rate percentage.</param>
        /// <param name="accrualType">The interest accrual type.</param>
        /// <returns>The daily interest monetary rate.</returns>
        public static decimal CalcConstructionDailyRate(decimal rate, ConstructionPhaseIntAccrual accrualType)
        {
            switch (accrualType)
            {
                case ConstructionPhaseIntAccrual.Monthly_360_360:
                case ConstructionPhaseIntAccrual.ActualDays_365_360:
                    return rate / (360M * 100M);
                case ConstructionPhaseIntAccrual.ActualDays_365_365:
                    return rate / (365M * 100M);
                default:
                    throw new UnhandledEnumException(accrualType);
            }
        }

        /// <summary>
        /// Computes the estimated number of days in a month for each interest
        /// accrual type.
        /// </summary>
        /// <remarks>
        /// Note that Actual/360 does not return 30 because if it did, the resulting
        /// estimated monthly interest would be the same as Monthly/360, but the
        /// actual interest costs would be more. The regulations expressly forbid
        /// the use of estimated values that have this effect, so we compensate by
        /// increasing the number of days in the month.
        /// </remarks>
        /// <param name="accrualType">The interest accrual type.</param>
        /// <returns>The estimated number of days in a month.</returns>
        public static decimal CalcConstructionEstimatedDaysPerMonth(ConstructionPhaseIntAccrual accrualType)
        {
            switch (accrualType)
            {
                case ConstructionPhaseIntAccrual.Monthly_360_360:
                case ConstructionPhaseIntAccrual.ActualDays_365_365:
                    return 30M;
                case ConstructionPhaseIntAccrual.ActualDays_365_360:
                    return 365M / 12M;
                default:
                    throw new UnhandledEnumException(accrualType);
            }
        }

        [DependsOn(nameof(sIsConstructionLoan), nameof(sConstructionPurposeT), nameof(sLandCost), nameof(sLotVal))]
        [DevNote("This is a calculated field that is used on the 1003. For construction loans, it is equal to sLandCost if the construction purpose is 'Construction and Lot Purchase', sLotVal otherwise.")]
        public decimal sPresentValOfLot
        {
            get
            {
                if (this.sIsConstructionLoan)
                {
                    if (this.sConstructionPurposeT == ConstructionPurpose.ConstructionAndLotPurchase)
                    {
                        return this.sLandCost;
                    }

                    return this.sLotVal;
                }

                return 0M;
            }

            set
            {
                if (this.sIsConstructionLoan)
                {
                    if (this.sConstructionPurposeT == ConstructionPurpose.ConstructionAndLotPurchase)
                    {
                        this.sLandCost = value;
                    }

                    this.sLotVal = value;
                }
            }
        }

        public string sPresentValOfLot_rep
        {
            get { return this.ToMoneyString(() => this.sPresentValOfLot); }
            set { this.sPresentValOfLot = ToMoney(value); }
        }

        [DependsOn(nameof(sIsConstructionLoan), nameof(sConstructionPurposeT), nameof(sLotOwnerT), nameof(sLotImprovC),
            nameof(sLandCost), nameof(sPurchPrice), nameof(sLoanVersionT))]
        [DevNote("This field replaces sPurchPrice on the Details of Transaction page.")]
        public decimal sPurchasePrice1003
        {
            get
            {
                if (AppliedCCArchive != null && !this.AppliedCCArchive.IsForCoCProcess)
                {
                    if (AppliedCCArchive.HasValue("sPurchasePrice1003")) return AppliedCCArchive.GetMoneyValue("sPurchasePrice1003");
                }

                if (!LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V31_ConstructionLoans_ExposeNewDataPoints))
                {
                    return sPurchPrice;
                }

                if (this.sIsConstructionLoan)
                {
                    if (this.sConstructionPurposeT == ConstructionPurpose.ConstructionAndLotPurchase)
                    {
                        if (this.sLotOwnerT == LotOwnerType.Other)
                        {
                            return sLotImprovC;
                        }

                        return this.SumMoney(this.sLotImprovC, this.sLandCost);
                    }

                    return 0M;
                }

                return sPurchPrice;
            }
        }

        public string sPurchasePrice1003_rep
        {
            get { return this.ToMoneyString(() => this.sPurchasePrice1003); }
        }

        [DependsOn(nameof(sIsConstructionLoan), nameof(sConstructionPurposeT), nameof(sLotOwnerT), nameof(sLandCost), nameof(sLoanVersionT))]
        [DevNote("This field replaces sLandCost on the Details of Transaction page.")]
        public decimal sLandIfAcquiredSeparately1003
        {
            get
            {
                if (AppliedCCArchive != null && AppliedCCArchive.HasValue("sLandIfAcquiredSeparately1003"))
                {
                    return AppliedCCArchive.GetMoneyValue("sLandIfAcquiredSeparately1003");
                }

                if (!this.sIsConstructionLoan
                    || !LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V31_ConstructionLoans_ExposeNewDataPoints))
                {
                    return this.sLandCost;
                }

                if (this.sConstructionPurposeT == ConstructionPurpose.ConstructionAndLotPurchase && this.sLotOwnerT == LotOwnerType.Other)
                {
                    return this.sLandCost;
                }

                return 0M;
            }
        }

        public string sLandIfAcquiredSeparately1003_rep
        {
            get { return this.ToMoneyString(() => this.sLandIfAcquiredSeparately1003); }
        }

        [DependsOn(nameof(sLotOwnerT), nameof(sIsConstructionLoan), nameof(sConstructionPurposeT))]
        [DevNote("This field indicates who is the current owner of the lot.")]
        public LotOwnerType sLotOwnerT
        {
            get
            {
                if (!this.sIsConstructionLoan || this.sConstructionPurposeT != ConstructionPurpose.ConstructionAndLotPurchase)
                {
                    return LotOwnerType.Borrower;
                }

                return GetTypeIndexField("sLotOwnerT", LotOwnerType.Blank);
            }

            set
            {
                if (!value.EqualsOneOf(LotOwnerType.Blank, LotOwnerType.Borrower))
                {
                    SetTypeIndexField("sLotOwnerT", (int)value);
                }
            }
        }

        [DependsOn(nameof(sConstructionPurposeT), nameof(sIsConstructionLoan), nameof(sfUpdateNLMSCallReportData))]
        [DevNote("This indicates how the funds are to be used. It determines what the Loan Purpose will be on the TRID disclosures and what Loan Purpose is used for AUS.")]
        public ConstructionPurpose sConstructionPurposeT
        {
            get
            {
                if (!this.sIsConstructionLoan)
                {
                    return ConstructionPurpose.Blank;
                }

                ConstructionPurpose value = GetTypeIndexField("sConstructionPurposeT", ConstructionPurpose.Blank);

                if (value == ConstructionPurpose.Blank
                    && LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.sLoanVersionT, LoanVersionT.V31_ConstructionLoans_ExposeNewDataPoints))
                {
                    return ConstructionPurpose.ConstructionAndLotPurchase;
                }

                return value;
            }

            set
            {
                if (value != ConstructionPurpose.Blank)
                {
					var previous = GetTypeIndexField("sConstructionPurposeT", ConstructionPurpose.Blank);

                    SetTypeIndexField("sConstructionPurposeT", (int)value);

                	if (previous != value)
                	{
                    	UpdateNLMSCallReportData(isModifyPurpose: true);
                	}
				}
            }
        }

        [DependsOn(nameof(sConstructionIntCalcT), nameof(sIsConstructionLoan))]
        [DevNote("This indicates what to use for the loan balance in order to calculate the interest for the construction period which is used in the calcs defined in Appendix D.")]
        public ConstructionIntCalcType sConstructionIntCalcT
        {
            get
            {
                if (AppliedCCArchive != null && AppliedCCArchive.HasValue("sConstructionIntCalcT"))
                {
                    return (ConstructionIntCalcType)AppliedCCArchive.GetCountValue("sConstructionIntCalcT");
                }
				
				if (!this.sIsConstructionLoan)
				{
					return ConstructionIntCalcType.Blank;
				}

                // OPM 467676 - TODO: PHASE 2 - UPDATE CALCULATION WHEN CALC FOR sIsAdvanceScheduleKnown IS DONE.
                return GetTypeIndexField("sConstructionIntCalcT", ConstructionIntCalcType.Blank);
            }

            set
            {
                SetTypeIndexField("sConstructionIntCalcT", (int)value);
            }
        }

        [DependsOn(nameof(sConstructionMethodT), nameof(sIsConstructionLoan))]
        [DevNote("This indicates how the construction is performed.It is used for product eligibility.")]
        public ConstructionMethod sConstructionMethodT
        {
            get
            {
				if (!this.sIsConstructionLoan)
				{
					return ConstructionMethod.Blank;
				}
			
                return GetTypeIndexField("sConstructionMethodT", ConstructionMethod.Blank);
            }

            set
            {
                SetTypeIndexField("sConstructionMethodT", (int)value);
            }
        }

        [DependsOn(nameof(sConstructionPhaseIntPaymentTimingT), nameof(sIsConstructionLoan))]
        [DevNote("This field indicates when the interest payments will be made. It is used to determine the payment stream and calculate the APR.")]
        public ConstructionPhaseIntPaymentTiming sConstructionPhaseIntPaymentTimingT
        {
            get
            {
				if (!this.sIsConstructionLoan)
				{
					return ConstructionPhaseIntPaymentTiming.Blank;
				}
			
                return GetTypeIndexField("sConstructionPhaseIntPaymentTimingT", ConstructionPhaseIntPaymentTiming.InterestPaidPeriodically);
            }

            set
            {
                SetTypeIndexField("sConstructionPhaseIntPaymentTimingT", (int)value);
            }
        }

        [DependsOn(nameof(sConstructionPhaseIntPaymentFrequencyT), nameof(sIsConstructionLoan))]
        [DevNote("This field indicates when the payments will be due for the interim construction financing.")]
        public ConstructionPhaseIntPaymentFrequency sConstructionPhaseIntPaymentFrequencyT
        {
            get
            {
				if (!this.sIsConstructionLoan)
				{
					return ConstructionPhaseIntPaymentFrequency.Blank;
				}
			
                return GetTypeIndexField("sConstructionPhaseIntPaymentFrequencyT", ConstructionPhaseIntPaymentFrequency.Monthly);
            }

            set
            {
                SetTypeIndexField("sConstructionPhaseIntPaymentFrequencyT", (int)value);
            }
        }

        [DependsOn(nameof(sConstructionPhaseIntAccrualT), nameof(sIsConstructionLoan))]
        [DevNote("This field indicates how to calculate the interest for the interim construction financing. It is used to calculate the Construction Interest Amount for the interim construction loan.")]
        public ConstructionPhaseIntAccrual sConstructionPhaseIntAccrualT
        {
            get
            {
                if (AppliedCCArchive != null && AppliedCCArchive.HasValue("sConstructionPhaseIntAccrualT"))
                {
                    return (ConstructionPhaseIntAccrual)AppliedCCArchive.GetCountValue("sConstructionPhaseIntAccrualT");
                }
				
				if (!this.sIsConstructionLoan)
				{
					return ConstructionPhaseIntAccrual.Blank;
				}

                return GetTypeIndexField("sConstructionPhaseIntAccrualT", ConstructionPhaseIntAccrual.Blank);
            }

            set
            {
                SetTypeIndexField("sConstructionPhaseIntAccrualT", (int)value);
            }
        }

        [DependsOn(nameof(sSubsequentlyPaidFinanceChargeAmt), nameof(sIsConstructionLoan))]
        [DevNote("This field captures any finance charges that are paid after closing which has an effect on the APR.It is used to calculate the APR.")]
        public decimal sSubsequentlyPaidFinanceChargeAmt
        {
            get
            {
				if (!this.sIsConstructionLoan)
				{
					return 0M;
				}
			
                return GetMoneyField("sSubsequentlyPaidFinanceChargeAmt", 0M);
            }

            set
            {
                SetNullableMoneyField("sSubsequentlyPaidFinanceChargeAmt", value);
            }
        }

        public string sSubsequentlyPaidFinanceChargeAmt_rep
        {
            get
            {
                return this.ToMoneyString(() => this.sSubsequentlyPaidFinanceChargeAmt);
            }
            set
            {
                this.sSubsequentlyPaidFinanceChargeAmt = this.ToMoney(value);
            }
        }

        [DependsOn(nameof(sConstructionInitialAdvanceAmt), nameof(sIsConstructionLoan))]
        [DevNote("This field captures the amount of the initial advance disbursed at closing.")]
        public decimal sConstructionInitialAdvanceAmt
        {
            get
            {
				if (!this.sIsConstructionLoan)
				{
					return 0M;
				}
			
                return GetMoneyField("sConstructionInitialAdvanceAmt", 0M);
            }

            set
            {
                SetNullableMoneyField("sConstructionInitialAdvanceAmt", value);
            }
        }

        public string sConstructionInitialAdvanceAmt_rep
        {
            get
            {
                return this.ToMoneyString(() => this.sConstructionInitialAdvanceAmt);
            }
            set
            {
                this.sConstructionInitialAdvanceAmt = this.ToMoney(value);
            }
        }

        [DependsOn(nameof(sIsIntReserveRequired), nameof(sIsConstructionLoan))]
        [DevNote("This field indicates if the lender requires an interest reserve so that the interest payments will be paid from the loan. It is used to calculate the APR.")]
        public bool sIsIntReserveRequired
        {
            get
            {
                if (AppliedCCArchive != null && AppliedCCArchive.HasValue("sIsIntReserveRequired"))
                {
                    return AppliedCCArchive.GetBitValue("sIsIntReserveRequired");
                }
				
				if (!this.sIsConstructionLoan)
				{
					return false;
				}

                return GetBoolField("sIsIntReserveRequired", false);
            }

            set
            {
                SetNullableBoolField("sIsIntReserveRequired", value);
            }
        }

        [DependsOn(nameof(sIntReserveAmt), nameof(sIsConstructionLoan))]
        [DevNote("Minimal support for interest reserves.")]
        public decimal sIntReserveAmt
        {
            get
            {
                if (!this.sIsConstructionLoan)
                {
                    return 0M;
                }

                return GetMoneyField("sIntReserveAmt", 0M);
            }
            set { SetNullableMoneyField("sIntReserveAmt", value); }
        }

        public string sIntReserveAmt_rep
        {
            get { return ToMoneyString(() => sIntReserveAmt); }
            set { sIntReserveAmt = ToMoney(value); }
        }

        [DependsOn(nameof(sIsAdvanceScheduleKnown), nameof(sIsConstructionLoan))]
        [DevNote("This field indicates if the advance schedule is known. There could be either a single advance or multiple advances. This is used to determine if Appendix D can be used to calculate the APR.")]
        public bool sIsAdvanceScheduleKnown
        {
            get
            {
				
				if (!this.sIsConstructionLoan)
				{
					return false;
				}
				
                // OPM 467676 - TODO: PHASE 2 - WILL NEED TO UPDATE CALCULATION ONCE PHASE 2 SPEC IS DONE.
                // THIS MAY END UP BEING A PURELY CALCULATED FIELD. BUT IT SOUNDS LIKE WE MAY WANT A WAY
                // FOR USER TO ENTER VALUE DIRECTLY.
                return GetBoolField("sIsAdvanceScheduleKnown", false);
            }

            set
            {
                SetNullableBoolField("sIsAdvanceScheduleKnown", value);
            }
        }

        [DependsOn(nameof(sConstructionLoanD), nameof(sConstructionPeriodEndD), nameof(sIsConstructionLoan))]
        [DevNote("The number of days in the construction period, including Feb. 29 whenever it occurs.")]
        public int sConstructionPeriodDays
        {
            get 
			{ 
				if (!this.sIsConstructionLoan)
				{
					return 0;
				}
			
				return this.sConstructionPeriodEndD.DateTimeForComputation.Subtract(this.sConstructionLoanD.DateTimeForComputation).Days; 
			}
        }
        
        [DependsOn(nameof(sConstructionPhaseIntAccrualT), nameof(sConstructionPeriodIR), nameof(sConstructionPeriodDays), nameof(sConstructionLoanD),
            nameof(sConstructionPeriodEndD), nameof(sLAmtCalc), nameof(sLPurposeT), nameof(sConstructionDiscT), 
            nameof(sConstructionPeriodMon), nameof(sConstructionIntCalcT), nameof(sIsIntReserveRequired), nameof(sNoteIR), nameof(sConstructionIntAccrualD),
			nameof(sIsConstructionLoan))]
        [DevNote("This is a calculated field representing how much interest will be charged during the construction period based on the interim construction loan terms.")]
        public decimal sConstructionIntAmount
        {
            get
            {
				if (!this.sIsConstructionLoan)
				{
					return 0M;
				}
			
                // Start populating debug info if applicable.
                if (this.x_AprWinDebugInputs != null)
                {
                    this.x_AprWinDebugInputs.ConstructionInputs.AnnualSimpleInterestRate = this.sConstructionPeriodIR;
                    this.x_AprWinDebugInputs.ConstructionInputs.InterestPayable = this.sConstructionIntCalcT == ConstructionIntCalcType.HalfCommitment ? AprWinInputs.AprWin_6_2.InterestPayableOptions.OnAdvancesWhenMade : AprWinInputs.AprWin_6_2.InterestPayableOptions.OnEntireCommitment;
                }

                decimal periodicInterestRate = CalcConstructionDailyRate(this.sConstructionPeriodIR, this.sConstructionPhaseIntAccrualT);
                int totalPeriods;

                FedCalendarPeriods periods = FedCalendarPeriods.ConstructionOnlyPeriodBetween(this.sConstructionLoanD, this.sConstructionPeriodEndD, this.sConstructionPhaseIntAccrualT);

                // If there are odd days not accounted for, we cannot calculate construction interest.
                if (periods.HasAdditionalOddDays)
                {
                    throw new CBaseException(ErrorMessages.Generic, "Illegal odd days in APR calculation.");
                }

                // If Interest Accrual Date is before the Construction Loan Date, we cannot calculate construction interest.
                if (this.sConstructionIntAccrualD.DateTimeForComputation < this.sConstructionLoanD.DateTimeForComputation)
                {
                    throw new CBaseException(ErrorMessages.Generic, "Construction Interest Accrual Date must be equal to the Construction Loan Date or else the APR will not calculate.");
                }

                // We start by assuming that the basics of this calculation are sane:
                //  * We get the periodic rate for each accrual type by the definition
                //  * We get the simple number of periods that corresponds to the accrual type
                switch (this.sConstructionPhaseIntAccrualT)
                {
                    case ConstructionPhaseIntAccrual.ActualDays_365_360:
                        totalPeriods = sConstructionPeriodDays;
                        break;
                    case ConstructionPhaseIntAccrual.ActualDays_365_365:
                        totalPeriods = sConstructionPeriodDays;
                        break;
                    case ConstructionPhaseIntAccrual.Monthly_360_360:
                        periodicInterestRate *= 30M; // Convert from a daily rate to a monthly rate.
                        totalPeriods = periods.UnitPeriods * periods.FractionsPerUnitPeriod + periods.FractionalPeriods;
                        break;
                    case ConstructionPhaseIntAccrual.Blank:
                        return 0;
                    default:
                        throw new UnhandledEnumException(this.sConstructionPhaseIntAccrualT);
                }

                decimal constructionPhaseIntFactor;

                bool isConstructionSeperately = this.sLPurposeT == E_sLPurposeT.Construct || (this.sLPurposeT == E_sLPurposeT.ConstructPerm && this.sConstructionDiscT == ConstructionDisclosureType.Separate);
                bool isConstructionCombined = this.sLPurposeT == E_sLPurposeT.ConstructPerm && this.sConstructionDiscT == ConstructionDisclosureType.Combined;

                // And then things get confusing. If this is a construction-only loan, or we are disclosing
                // only the construction phase of a construction-to-perm loan, for some reason we have to use
                // the Fed Calendar to compute interest, which has no other effect than to make daily interest
                // be off by a day if a leap year falls within a "whole year" by the Fed Calculator's reckoning.
                // Why? Because that is what APRWIN does. We are still looking for a reason in the regulations.
                // For a combined construction-to-perm disclosure, we do the sane thing and just count all the days.
                // Why? Because it makes sense and APRWIN does it, but mostly because APRWIN does it.
                if (isConstructionSeperately)
                {
                    int totalAdjustedPeriods = periods.UnitPeriods * periods.FractionsPerUnitPeriod + periods.FractionalPeriods;
                    constructionPhaseIntFactor = periodicInterestRate * totalAdjustedPeriods;

                    if (this.x_AprWinDebugInputs != null)
                    {
                        this.x_AprWinDebugInputs.ConstructionInputs.ConstructionSeparately.ConstructionPhaseInterestAccrual = AprWinInputs.AprWin_6_2.ToAccrualOptions(this.sConstructionPhaseIntAccrualT);

                        if (this.sConstructionPhaseIntAccrualT == ConstructionPhaseIntAccrual.Monthly_360_360)
                        {
                            this.x_AprWinDebugInputs.ConstructionInputs.ConstructionSeparately.NumberOfMonths = totalAdjustedPeriods;
                        }
                        else
                        {
                            this.x_AprWinDebugInputs.ConstructionInputs.ConstructionSeparately.NumberOfDays = totalPeriods;
                            this.x_AprWinDebugInputs.ConstructionInputs.ConstructionSeparately.NumberOfUnitPeriods = periods.UnitPeriods < 1 ? 1 : periods.UnitPeriods;
                            this.x_AprWinDebugInputs.ConstructionInputs.ConstructionSeparately.NumberOfOddDays = periods.UnitPeriods < 1 ? 0 : periods.FractionalPeriods;
                        }
                    }
                }
                else if (isConstructionCombined)
                {
                    constructionPhaseIntFactor = periodicInterestRate * totalPeriods;

                    if (this.x_AprWinDebugInputs != null)
                    {
                        this.x_AprWinDebugInputs.ConstructionInputs.ConstructionAndPermanent.ConstructionPhaseInterestAccrual = AprWinInputs.AprWin_6_2.ToAccrualOptions(this.sConstructionPhaseIntAccrualT);

                        if (this.sConstructionPhaseIntAccrualT == ConstructionPhaseIntAccrual.Monthly_360_360)
                        {
                            this.x_AprWinDebugInputs.ConstructionInputs.ConstructionAndPermanent.NumberOfMonths = totalPeriods;
                        }
                        else
                        {
                            this.x_AprWinDebugInputs.ConstructionInputs.ConstructionAndPermanent.NumberOfDays = totalPeriods;
                        }
                    }
                }
                else
                {
                    return 0;
                }

                // Per the regulations, we cut the commitment amount in half when calculating the
                // interest that accrues during the construction period if the interest is charged
                // based on the actual draws. If the interest accrues based on the whole commitment
                // amount, the whole amount is used.
                decimal constructionCommitmentBalance = this.sLAmtCalc * (this.sConstructionIntCalcT == ConstructionIntCalcType.HalfCommitment ? 0.5M : 1.0M);

                // Finally, compute the interest.
                decimal estimatedConstructionPhaseInt = constructionCommitmentBalance  * constructionPhaseIntFactor;

                // But wait! If there is an interest reserve required, we need to calculate interest
                // on the interest.
                if (this.sIsIntReserveRequired)
                {
                    // Using half the interest because that's what APRWIN seems to do.
                    // The best post-hoc rationalization we have for that is that the interest
                    // is charged as it accrues rather than up front, and thus we assume that
                    // half accrues at closing, which is consistent with how the commitment amount
                    // is handled. Note that the halving is not dependent on whether or not we
                    // halve the commitment amount since the interest is charged as it accrues
                    // regardless.
                    estimatedConstructionPhaseInt = (constructionCommitmentBalance + estimatedConstructionPhaseInt / 2) * constructionPhaseIntFactor;
                }

                // Okay, we really are done now.
                var finalInterest = System.Math.Round(estimatedConstructionPhaseInt, 2, System.MidpointRounding.AwayFromZero);

                if (this.x_AprWinDebugInputs != null)
                {
                    if (isConstructionSeperately)
                    {
                        this.x_AprWinDebugInputs.ConstructionInputs.ConstructionSeparately.RequiredInterestReserve = this.sIsIntReserveRequired;
                        this.x_AprWinDebugInputs.ConstructionInputs.ConstructionSeparately.EstimatedConstructionInterest = finalInterest;
                        this.x_AprWinDebugInputs.ConstructionInputs.ConstructionSeparately.DisclosedOrEstimatedApr = this.sConstructionPeriodIR;
                    }
                    else if (isConstructionCombined)
                    {
                        this.x_AprWinDebugInputs.ConstructionInputs.ConstructionAndPermanent.RequiredInterestReserve = this.sIsIntReserveRequired;
                        this.x_AprWinDebugInputs.ConstructionInputs.ConstructionAndPermanent.EstimatedConstructionInterest = finalInterest;
                        this.x_AprWinDebugInputs.ConstructionInputs.ConstructionAndPermanent.DisclosedOrEstimatedApr = this.sNoteIR;
                    }
                }

                return finalInterest;
            }
        }

        public string sConstructionIntAmount_rep
        {
            get { return ToMoneyString(() => this.sConstructionIntAmount);  }
        }

        [DependsOn(nameof(sConstructionIntAccrualD), nameof(sConstructionLoanD), nameof(sIsConstructionLoan))]
        [DevNote("The number of days between construction loan closing and the construction accrual date.")]
        public int sConstructionInitialOddDays
        {
            get
            {
                if (AppliedCCArchive != null && AppliedCCArchive.HasValue("sConstructionInitialOddDays"))
                {
                    return AppliedCCArchive.GetCountValue("sConstructionInitialOddDays");
                }
				
				if (!this.sIsConstructionLoan)
				{
					return 0;
				}

                CDateTime constructionIntAccrualD = this.sConstructionIntAccrualD;
                if (!constructionIntAccrualD.IsValid)
                {
                    return 0;
                }

                CDateTime constructionLoanD = this.sConstructionLoanD;
                if (!constructionLoanD.IsValid)
                {
                    return 0;
                }

                return constructionIntAccrualD.DateTimeForComputation.Subtract(constructionLoanD.DateTimeForComputation).Days;
            }
        }

        [DependsOn]
        [DevNote("Lock for sConstructionLoanDLckd.")]
        public bool sConstructionLoanDLckd
        {
            get
            {
                return GetBoolField("sConstructionLoanDLckd", false);
            }

            set
            {
                SetNullableBoolField("sConstructionLoanDLckd", value);
            }
        }

        [DependsOn(nameof(sConstructionLoanD), nameof(sConstructionLoanDLckd), nameof(sIsConstructionLoan), nameof(sEstCloseD))]
        [DevNote("The date the loan funds and if there are odd days, the beginning of the odd period.")]
        public CDateTime sConstructionLoanD
        {
            get
            {
                if (this.sConstructionLoanDLckd)
                {
                    return GetDateTimeField("sConstructionLoanD");
                }

                if (this.sIsConstructionLoan)
                {
                    return this.sEstCloseD;
                }

                return CDateTime.InvalidWrapValue;
            }

            set
            {
                SetDateTimeField("sConstructionLoanD", value);
            }
        }

        public string sConstructionLoanD_rep
        {
            get
            {
                return ToDateString(sConstructionLoanD.GetSafeDateTimeForComputation(CDateTime.InvalidDateTime));
            }
            set
            {
                sConstructionLoanD = CDateTime.Create(value, m_convertLos);
            }
        }

        [DependsOn]
        [DevNote("Lock for sConstructionIntAccrualD.")]
        public bool sConstructionIntAccrualDLckd
        {
            get
            {
                return GetBoolField("sConstructionIntAccrualDLckd", false);
            }

            set
            {
                SetNullableBoolField("sConstructionIntAccrualDLckd", value);
            }
        }

        [DependsOn(nameof(sConstructionIntAccrualD), nameof(sConstructionIntAccrualDLckd), nameof(sLPurposeT), nameof(sConstructionLoanD), nameof(sConstructionPhaseIntAccrualT), nameof(sIsConstructionLoan))]
        [DevNote("The date the interest begins to accrue (typically one month before the first payment date) and if there are odd days, the end of the odd period.")]
        public CDateTime sConstructionIntAccrualD
        {
            get
            {
                if (this.sConstructionIntAccrualDLckd)
                {
					return GetDateTimeField("sConstructionIntAccrualD");
				}

                if (!this.sIsConstructionLoan)
                {
                    return CDateTime.InvalidWrapValue;
                }

                if (!this.sConstructionLoanD.IsValid || this.sConstructionLoanD.DateTimeForComputation.Day == 1
                    || this.sConstructionPhaseIntAccrualT == ConstructionPhaseIntAccrual.Monthly_360_360)
                {
                    // NOTE: sConstructionLoanD is invalid for non-Construction loans.
                    return this.sConstructionLoanD;
                }

                DateTime nextMonth = this.sConstructionLoanD.DateTimeForComputation.AddMonths(1);
                return CDateTime.Create(new DateTime(nextMonth.Year, nextMonth.Month, 1));
            }

            set
            {
                SetDateTimeField("sConstructionIntAccrualD", value);
            }
        }

        public string sConstructionIntAccrualD_rep
        {
            get
            {
                return ToDateString(sConstructionIntAccrualD.GetSafeDateTimeForComputation(CDateTime.InvalidDateTime));
            }
            set
            {
                sConstructionIntAccrualD = CDateTime.Create(value, m_convertLos);
            }
        }

        [DependsOn]
        [DevNote("Lock for sConstructionFirstPaymentD")]
        public bool sConstructionFirstPaymentDLckd
        {
            get
            {
                return GetBoolField("sConstructionFirstPaymentDLckd", false);
			}

            set
            {
                SetNullableBoolField("sConstructionFirstPaymentDLckd", value);
            }
        }

        [DependsOn(nameof(sConstructionFirstPaymentD), nameof(sConstructionFirstPaymentDLckd), nameof(sConstructionIntAccrualD), nameof(sIsConstructionLoan))]
        [DevNote("The date the first payment is due for the construction phase.")]
        public CDateTime sConstructionFirstPaymentD
        {
            get
            {
                if (this.sConstructionFirstPaymentDLckd)
                {
                    return GetDateTimeField("sConstructionFirstPaymentD");
                }

                if (!this.sIsConstructionLoan)
                {
                    return CDateTime.InvalidWrapValue;
                }

                if (!sConstructionIntAccrualD.IsValid)
                {
                    return CDateTime.InvalidWrapValue;
                }

                return CDateTime.Create(sConstructionIntAccrualD.DateTimeForComputation.AddMonths(1));
            }

            set
            {
                SetDateTimeField("sConstructionFirstPaymentD", value);
            }
        }

        public string sConstructionFirstPaymentD_rep
        {
            get
            {
                return ToDateString(sConstructionFirstPaymentD.GetSafeDateTimeForComputation(CDateTime.InvalidDateTime));
            }
            set
            {
                sConstructionFirstPaymentD = CDateTime.Create(value, m_convertLos);
            }
        }

        [DependsOn(nameof(sConstructionIntAccrualD), nameof(sConstructionPeriodMon), nameof(sIsConstructionLoan))]
        [DevNote("This is a calculated field based on the construction loan term and the Construction Interest Accrual Date.")]
        public CDateTime sConstructionPeriodEndD
        {
            get
            {
                if (!this.sIsConstructionLoan || !this.sConstructionIntAccrualD.IsValid)
                {
                    return CDateTime.InvalidWrapValue;
                }

                return CDateTime.Create(this.sConstructionIntAccrualD.DateTimeForComputationWithTime.AddMonths(this.sConstructionPeriodMon));
            }
        }

        public string sConstructionPeriodEndD_rep
        {
            get
            {
                return this.sConstructionPeriodEndD.ToString(m_convertLos);
            }
        }

        [DependsOn(nameof(sConstructionToPermanentClosingT), nameof(sIsConstructionLoan))]
        [DevNote("This is a MISMO field used to indicate if the loan will have one or two closings.")]
        public ConstructionToPermanentClosingType sConstructionToPermanentClosingT
        {
            get
            {
				if (!this.sIsConstructionLoan)
                {
                    return ConstructionToPermanentClosingType.Blank;
                }
			
                return GetTypeIndexField("sConstructionToPermanentClosingT", ConstructionToPermanentClosingType.OneClosing);
            }

            set
            {
                SetTypeIndexField("sConstructionToPermanentClosingT", (int)value);
            }
        }

        [DependsOn(nameof(sConstructionDiscT), nameof(sIsConstructionLoan))]
        [DevNote("This field indicates if the interim construction financing and the permanent financing will be disclosed on separate disclosures or combined on a single disclosure.")]
        public ConstructionDisclosureType sConstructionDiscT
        {
            get
            {
				if (!this.sIsConstructionLoan)
                {
                    return ConstructionDisclosureType.Blank;
                }
			
                return GetTypeIndexField("sConstructionDiscT", ConstructionDisclosureType.Combined);
            }

            set
            {
                SetTypeIndexField("sConstructionDiscT", (int)value);
            }
        }

        [DependsOn(nameof(sConstructionAmortT), nameof(sFinMethT))]
        [DevNote("This indicates the amortization used for the interim construction financing. It is used to enable the appropriated fields for Fixed or Adjustable loan terms.")]
        public E_sFinMethT sConstructionAmortT
        {
            get
            {
				if (this.sLPurposeT == E_sLPurposeT.Construct)
                {
                    return this.sFinMethT;
                }

                if (this.sLPurposeT == E_sLPurposeT.ConstructPerm)
                {
                    return GetTypeIndexField("sConstructionAmortT", E_sFinMethT.Fixed);
                }

                return E_sFinMethT.Fixed;
            }

            set
            {
                if (this.sLPurposeT != E_sLPurposeT.ConstructPerm)
                {
                    this.sFinMethT = value;
                }

                SetTypeIndexField("sConstructionAmortT", (int)value);
            }
        }

        [DependsOn(nameof(sConstructionRAdj1stCapR), nameof(sRAdj1stCapR), nameof(sLPurposeT))]
        [DevNote("Rate adjustment first cap rate for construction loans.")]
        public decimal sConstructionRAdj1stCapR
        {
            get
            {
                if (this.sLPurposeT == E_sLPurposeT.Construct)
                {
                    return this.sRAdj1stCapR;
                }

                if (this.sLPurposeT == E_sLPurposeT.ConstructPerm)
                {
                    return GetRateField("sConstructionRAdj1stCapR", 0M);
                }

                return 0M;
            }

            set
            {
                if (this.sLPurposeT != E_sLPurposeT.ConstructPerm)
                {
                    this.sRAdj1stCapR = value;
                }

                SetNullableRateField("sConstructionRAdj1stCapR", value);
            }
        }

        public string sConstructionRAdj1stCapR_rep
        {
            get { return this.ToRateString(() => this.sConstructionRAdj1stCapR); }
            set { this.sConstructionRAdj1stCapR = this.ToRate(value); }
        }

        [DependsOn(nameof(sConstructionRAdj1stCapMon), nameof(sRAdj1stCapMon), nameof(sLPurposeT))]
        [DevNote("Rate adjustment first cap months for construction loans.")]
        public int sConstructionRAdj1stCapMon
        {
            get
            {
                if (this.sLPurposeT == E_sLPurposeT.Construct)
                {
                    return this.sRAdj1stCapMon;
                }
				
				if (this.sLPurposeT == E_sLPurposeT.ConstructPerm)
				{
					return GetCountField("sConstructionRAdj1stCapMon", 0);
				}

                return 0;
            }

            set
            {
                if (this.sLPurposeT != E_sLPurposeT.ConstructPerm)
                {
                    this.sRAdj1stCapMon = value;
                }

                SetNullableCountField("sConstructionRAdj1stCapMon", value);
            }
        }

        public string sConstructionRAdj1stCapMon_rep
        {
            get { return this.ToCountString(() => this.sConstructionRAdj1stCapMon); }
            set { this.sConstructionRAdj1stCapMon = this.ToCount(value); }
        }

        [DependsOn(nameof(sConstructionRAdjCapR), nameof(sRAdjCapR), nameof(sLPurposeT))]
        [DevNote("Rate adjustment cap rate for construction loans.")]
        public decimal sConstructionRAdjCapR
        {
            get
            {
                if (this.sLPurposeT == E_sLPurposeT.Construct)
                {
                    return this.sRAdjCapR;
                }
				
				if (this.sLPurposeT == E_sLPurposeT.ConstructPerm)
				{
					return GetRateField("sConstructionRAdjCapR", 0M);
				}

                return 0M;
            }

            set
            {
                if (this.sLPurposeT != E_sLPurposeT.ConstructPerm)
                {
                    this.sRAdjCapR = value;
                }

                SetNullableRateField("sConstructionRAdjCapR", value);
            }
        }

        public string sConstructionRAdjCapR_rep
        {
            get { return this.ToRateString(() => this.sConstructionRAdjCapR); }
            set { this.sConstructionRAdjCapR = this.ToRate(value); }
        }

        [DependsOn(nameof(sConstructionRAdjCapMon), nameof(sRAdjCapMon), nameof(sLPurposeT))]
        [DevNote("Rate adjustment cap months for construction loans.")]
        public int sConstructionRAdjCapMon
        {
            get
            {
                if (this.sLPurposeT == E_sLPurposeT.Construct)
                {
                    return this.sRAdjCapMon;
                }
				
				if (this.sLPurposeT == E_sLPurposeT.ConstructPerm)
				{
					return GetCountField("sConstructionRAdjCapMon", 0);
				}

                return 0;
            }

            set
            {
                if (this.sLPurposeT != E_sLPurposeT.ConstructPerm)
                {
                    this.sRAdjCapMon = value;
                }

                SetNullableCountField("sConstructionRAdjCapMon", value);
            }
        }

        public string sConstructionRAdjCapMon_rep
        {
            get { return this.ToCountString(() => this.sConstructionRAdjCapMon); }
            set { this.sConstructionRAdjCapMon = this.ToCount(value); }
        }

        [DependsOn(nameof(sConstructionRAdjLifeCapR), nameof(sRAdjLifeCapR), nameof(sLPurposeT))]
        [DevNote("Rate adjustment life cap rate for construction loans.")]
        public decimal sConstructionRAdjLifeCapR
        {
            get
            {
                if (this.sLPurposeT == E_sLPurposeT.Construct)
                {
                    return this.sRAdjLifeCapR;
                }
				
				if (this.sLPurposeT == E_sLPurposeT.ConstructPerm)
				{
					return GetRateField("sConstructionRAdjLifeCapR", 0M);
				}

                return 0M;
            }

            set
            {
                if (this.sLPurposeT != E_sLPurposeT.ConstructPerm)
                {
                    this.sRAdjLifeCapR = value;
                }

                SetNullableRateField("sConstructionRAdjLifeCapR", value);
            }
        }

        public string sConstructionRAdjLifeCapR_rep
        {
            get { return this.ToRateString(() => this.sConstructionRAdjLifeCapR); }
            set { this.sConstructionRAdjLifeCapR = this.ToRate(value); }
        }

        [DependsOn(nameof(sConstructionRAdjMarginR), nameof(sRAdjMarginR), nameof(sLPurposeT))]
        [DevNote("Rate adjustment margin rate for construction loans.")]
        public decimal sConstructionRAdjMarginR
        {
            get
            {
                if (this.sLPurposeT == E_sLPurposeT.Construct)
                {
                    return this.sRAdjMarginR;
                }

				if (this.sLPurposeT == E_sLPurposeT.ConstructPerm)
				{
					return GetRateField("sConstructionRAdjMarginR", 0M);
				}
                
				return 0M;
            }

            set
            {
                if (this.sLPurposeT != E_sLPurposeT.ConstructPerm)
                {
                    this.sRAdjMarginR = value;
                }

                SetNullableRateField("sConstructionRAdjMarginR", value);
            }
        }

        public string sConstructionRAdjMarginR_rep
        {
            get { return this.ToRateString(() => this.sConstructionRAdjMarginR); }
            set { this.sConstructionRAdjMarginR = this.ToRate(value); }
        }

        [DependsOn(nameof(sConstructionArmIndexNameVstr), nameof(sArmIndexNameVstr), nameof(sLPurposeT))]
        [DevNote("ARM index name for construction loans.")]
        public string sConstructionArmIndexNameVstr
        {
            get
            {
                if (this.sLPurposeT == E_sLPurposeT.Construct)
                {
                    return this.sArmIndexNameVstr;
                }
				
				if (this.sLPurposeT == E_sLPurposeT.ConstructPerm)
				{
					return GetStringVarCharField("sConstructionArmIndexNameVstr");
				}

                return string.Empty;
            }

            set
            {
                if (this.sLPurposeT != E_sLPurposeT.ConstructPerm)
                {
                    this.sArmIndexNameVstr = value;
                }

                SetStringVarCharField("sConstructionArmIndexNameVstr", value);
            }
        }

        [DependsOn(nameof(sConstructionRAdjIndexR), nameof(sRAdjIndexR), nameof(sLPurposeT))]
        [DevNote("Rate adjustment index rate for construction loans.")]
        public decimal sConstructionRAdjIndexR
        {
            get
            {
                if (this.sLPurposeT == E_sLPurposeT.Construct)
                {
                    return this.sRAdjIndexR;
                }
				
				if (this.sLPurposeT == E_sLPurposeT.ConstructPerm)
				{
					return GetRateField("sConstructionRAdjIndexR", 0M);
				}
                
				return 0M;
            }

            set
            {
                if (this.sLPurposeT != E_sLPurposeT.ConstructPerm)
                {
                    this.sRAdjIndexR = value;
                }

                SetNullableRateField("sConstructionRAdjIndexR", value);
            }
        }

        public string sConstructionRAdjIndexR_rep
        {
            get { return this.ToRateString(() => this.sConstructionRAdjIndexR); }
            set { this.sConstructionRAdjIndexR = this.ToRate(value); }
        }

        [DevNote("Loan Program Rate Floor Calculation for Construction Loans.")]
        [DependsOn(nameof(sConstructionRAdjFloorCalcT), nameof(sRAdjFloorCalcT), nameof(sLPurposeT))]
        public RateAdjFloorCalcT sConstructionRAdjFloorCalcT
        {
            get
            {
                if (this.sLPurposeT != E_sLPurposeT.ConstructPerm)
                {
                    return this.sRAdjFloorCalcT;
                }

                return GetTypeIndexField<RateAdjFloorCalcT>("sConstructionRAdjFloorCalcT", RateAdjFloorCalcT.SetManually);
            }

            set
            {
                if (this.sLPurposeT != E_sLPurposeT.ConstructPerm)
                {
                    this.sRAdjFloorCalcT = value;
                }

                this.SetTypeIndexField("sConstructionRAdjFloorCalcT", (int)value);
            }
        }

        [DevNote("The read only conditions for sConstructionRAdjFloorR.")]
        [DependsOn(nameof(sConstructionRAdjFloorCalcT))]
        public bool sIsConstructionRAdjFloorRReadOnly
        {
            get
            {
                return this.sConstructionRAdjFloorCalcT != RateAdjFloorCalcT.SetManually;
            }
        }

        [DevNote("The sum of the base and additional rate adjustment rates for construction loans.")]
        [DependsOn(nameof(sConstructionRAdjFloorBaseR), nameof(sConstructionRAdjFloorAddR))]
        public decimal sConstructionRAdjFloorTotalR
        {
            get
            {
                return this.sConstructionRAdjFloorBaseR + this.sConstructionRAdjFloorAddR;
            }
        }

        public string sConstructionRAdjFloorTotalR_rep
        {
            get
            {
                return this.ToRateString(() => this.sConstructionRAdjFloorTotalR);
            }
        }

        [DevNote("Rate Adjustment Floor Base Rate for Construction Loans.")]
        [DependsOn(nameof(sConstructionRAdjFloorCalcT), nameof(sConstructionRAdjMarginR), nameof(sConstructionPeriodIR))]
        public decimal sConstructionRAdjFloorBaseR
        {
            get
            {
                switch (this.sConstructionRAdjFloorCalcT)
                {
                    case RateAdjFloorCalcT.MarginFixedPercent:
                        return this.sConstructionRAdjMarginR;
                    case RateAdjFloorCalcT.StartRateFixedPercent:
                        return this.sConstructionPeriodIR;
                    case RateAdjFloorCalcT.ZeroFixedPercent:
                        return 0M;
                    case RateAdjFloorCalcT.SetManually:
                        return 0M;
                    default:
                        throw new UnhandledEnumException(this.sConstructionRAdjFloorCalcT);
                }
            }
        }
        public string sConstructionRAdjFloorBaseR_rep
        {
            get
            {
                return ToRateString(() => this.sConstructionRAdjFloorBaseR);
            }
        }

        [DevNote("Rate Floor Adjustment Additional Rate for Construction Loans.")]
        [DependsOn(nameof(sConstructionRAdjFloorAddR), nameof(sRAdjFloorAddR), nameof(sLPurposeT))]
        public decimal sConstructionRAdjFloorAddR
        {
            get
            {
                if (this.sLPurposeT != E_sLPurposeT.ConstructPerm)
                {
                    return this.sRAdjFloorAddR;
                }

                return this.GetRateField("sConstructionRAdjFloorAddR");
            }

            set
            {
                if (this.sLPurposeT != E_sLPurposeT.ConstructPerm)
                {
                    this.sRAdjFloorAddR = value;
                }

                this.SetRateField("sConstructionRAdjFloorAddR", value);
            }
        }
        public string sConstructionRAdjFloorAddR_rep
        {
            get
            {
                return this.ToRateString(() => this.sRAdjFloorAddR);
            }

            set
            {
                this.sRAdjFloorAddR = this.ToRate(value);
            }
        }

        [DevNote("The difference between sNoteIr and sRadjLifeCapR for construction loans.")]
        [DependsOn(nameof(sConstructionPeriodIR), nameof(sConstructionRAdjLifeCapR))]
        public decimal sConstructionRAdjFloorLifeCapR
        {
            get
            {
                return this.sConstructionPeriodIR - this.sConstructionRAdjLifeCapR;
            }
        }
        public string sConstructionRAdjFloorLifeCapR_rep
        {
            get
            {
                return this.ToRateString(() => this.sConstructionRAdjFloorLifeCapR);
            }
        }

        [DependsOn(nameof(sConstructionRAdjFloorR), nameof(sRAdjFloorR), nameof(sLPurposeT),
            nameof(sIsConstructionRAdjFloorRReadOnly), nameof(sConstructionRAdjFloorTotalR), nameof(sConstructionRAdjFloorLifeCapR))]
        [DevNote("Rate adjustment floor rate for construction loans.")]
        public decimal sConstructionRAdjFloorR
        {
            get
            {
                if (this.sLPurposeT == E_sLPurposeT.Construct)
                {
                    return this.sRAdjFloorR;
                }
				
				if (this.sLPurposeT == E_sLPurposeT.ConstructPerm)
				{
					if (this.sIsConstructionRAdjFloorRReadOnly)
					{
						return Math.Max(this.sConstructionRAdjFloorTotalR, sConstructionRAdjFloorLifeCapR);
					}

					return GetRateField("sConstructionRAdjFloorR", 0M);
				}

                return 0M;
            }

            set
            {
                if (this.sLPurposeT != E_sLPurposeT.ConstructPerm)
                {
                    this.sRAdjFloorR = value;
                }

                SetNullableRateField("sConstructionRAdjFloorR", value);
            }
        }

        public string sConstructionRAdjFloorR_rep
        {
            get { return this.ToRateString(() => this.sConstructionRAdjFloorR); }
            set { this.sConstructionRAdjFloorR = this.ToRate(value); }
        }

        [DependsOn(nameof(sConstructionRAdjRoundT), nameof(sRAdjRoundT), nameof(sLPurposeT))]
        [DevNote("This indicates how the funds are to be used. It determines what the Loan Purpose will be on the TRID disclosures and what Loan Purpose is used for AUS.")]
        public E_sRAdjRoundT sConstructionRAdjRoundT
        {
            get
            {
                if (this.sLPurposeT == E_sLPurposeT.Construct)
                {
                    return this.sRAdjRoundT;
                }
				
				if (this.sLPurposeT == E_sLPurposeT.ConstructPerm)
				{
					return GetTypeIndexField("sConstructionRAdjRoundT", E_sRAdjRoundT.Normal);
				}

                return E_sRAdjRoundT.Normal;
            }

            set
            {
                if (this.sLPurposeT != E_sLPurposeT.ConstructPerm)
                {
                    this.sRAdjRoundT = value;
                }

                SetTypeIndexField("sConstructionRAdjRoundT", (int)value);
            }
        }

        [DependsOn(nameof(sConstructionRAdjRoundToR), nameof(sRAdjRoundToR), nameof(sLPurposeT))]
        [DevNote("Rate adjustment round to rate for construction loans.")]
        public decimal sConstructionRAdjRoundToR
        {
            get
            {
                if (this.sLPurposeT == E_sLPurposeT.Construct)
                {
                    return this.sRAdjRoundToR;
                }

				if (this.sLPurposeT == E_sLPurposeT.ConstructPerm)
				{
					return GetRateField("sConstructionRAdjRoundToR", 0M);
				}
                
				return 0M;
            }

            set
            {
                if (this.sLPurposeT != E_sLPurposeT.ConstructPerm)
                {
                    this.sRAdjRoundToR = value;
                }

                SetNullableRateField("sConstructionRAdjRoundToR", value);
            }
        }

        public string sConstructionRAdjRoundToR_rep
        {
            get { return this.ToRateString(() => this.sConstructionRAdjRoundToR); }
            set { this.sConstructionRAdjRoundToR = this.ToRate(value); }
        }
    }
}