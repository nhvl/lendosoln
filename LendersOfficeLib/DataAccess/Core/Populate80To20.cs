using System;
using System.Linq;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Migration;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using LendersOffice.ObjLib.AdjustmentsAndOtherCredits;
using LendersOffice.ObjLib.TRID2;
using LendersOffice.ObjLib;

namespace DataAccess
{

	/*
	 aBFirstNm
	 */
	[ Flags ]
	public enum E_Populate80To20Sections
	{
		BorrowerInfo = 0x1,
		SubjPropInfo = 0x2,
		LAmtAndPmt = 0x4,
		LpeSearchParam = 0x8,
        PreparerInfo = 0x10, // Decimal value is 16
        AgentInfo = 0x20, // Decimal value is 32
		All = 0xFFFF


		/*
		agent info (preparers info)
		internal assignment info   
		loan pmt and amount (reversing lien position, auto correct the lien pos) [checked by default]
		cc/gfe [static]
		rate [static]
		truth in lending/loan program info [static]
		accounting [ appending, replace entirely ]
		tasks [ appending, replace entirely ]
		Other forms
		*/
	}

	public class CPopulate80To20
	{
        private const string FirstMortgageClosingCostsDesc = "1st mortgage closing costs";
        private const string SecondMortgageClosingCostsDesc = "2nd mortgage closing costs";

        private readonly Guid brokerId;
        private readonly Guid invokingLoanId;
        private readonly Guid linkedLoanId;

        private bool errorUpdatingAdditionalHud1FirstLien = false;
        private bool errorUpdatingAdditionalHud1SecondLien = false;

		private System.Text.StringBuilder msgToUser = new System.Text.StringBuilder();

        public CPopulate80To20(LendersOffice.Admin.BrokerDB brokerDB, Guid invokingLoanId, Guid linkedLoanId)
		{
            this.brokerId = brokerDB.BrokerID;
            this.invokingLoanId = invokingLoanId;
            this.linkedLoanId = linkedLoanId;
            this.additionalFieldsToUpdate = brokerDB.LinkedLoanUpdateFields;

            string[] fields = new string[DependencyFields.Count + this.additionalFieldsToUpdate.Count];
            DependencyFields.CopyTo(fields, 0);
            this.additionalFieldsToUpdate.CopyTo(fields, DependencyFields.Count);
            this.selectStatementProvider = CSelectStatementProvider.GetProviderForTargets(fields, true);
		}

        public string MsgToUser
		{
			get { return this.msgToUser.ToString(); }
		}

        public void AddMsgToUser(string msg, bool bLogBug)
        {
            this.msgToUser.AppendLine(" * " + msg);

            if (bLogBug)
            {
                this.LogBug(msg);
            }
        }

        private void LogBug(string msg)
		{
            Tools.LogBug(msg + string.Format(". SrcLoanid={0}. LinkedLoanId={1}", this.invokingLoanId.ToString(), this.linkedLoanId.ToString()));
		}

        private static readonly ICollection<string> DependencyFields = new ReadOnlyCollection<string>(CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(CPopulate80To20)).ToList());

        private readonly ICollection<string> additionalFieldsToUpdate;

        private readonly CSelectStatementProvider selectStatementProvider;

        public UpdatedFirstLienInfo UpdateLinkedLoans()
        {
            var firstAndSecondLiens = this.InitializeLinkedLoans();

            CPageData firstLien = firstAndSecondLiens[0],
                secondLien = firstAndSecondLiens[1];

            this.UpdateSecondLienWithFirstLienData(firstLien, secondLien, E_Populate80To20Sections.All);

            firstLien.sONewFinCc = secondLien.GetOtherFinancingCostsForLinkedLoan();
            secondLien.sONewFinCc = firstLien.GetOtherFinancingCostsForLinkedLoan();

            this.UpdateAdditionalHud1Data(firstLien, secondLien);
            this.UpdateAdditionalHud1Data(secondLien, firstLien);

            firstLien.sSubFinPmtLckd = true;
            firstLien.sSubFinPmt = secondLien.sProThisMPmt + secondLien.sProMIns;

            // Added for case 243023
            firstLien.sIsOFinCreditLineInDrawPeriod = secondLien.sIsLineOfCredit;
            firstLien.sIsIOnlyForSubFin = secondLien.sIsIOnly;
            firstLien.sOtherLFinMethT = secondLien.sFinMethT;
            firstLien.sSubFin = secondLien.sCreditLineAmt;
            firstLien.sConcurSubFin = secondLien.sLAmtCalc;
            firstLien.sSubFinIR = secondLien.sNoteIR;
            firstLien.sSubFinTerm = secondLien.sTerm;
            firstLien.sSubFinMb = 0;

            // case 467063 & 470257 - adjustments and prorations update for 2nd liens.
            secondLien.sDisclosureRegulationTLckd = firstLien.sDisclosureRegulationTLckd;
            secondLien.sDisclosureRegulationT = firstLien.sDisclosureRegulationT;

            secondLien.sGseTargetApplicationTLckd = firstLien.sGseTargetApplicationTLckd;
            secondLien.sGseTargetApplicationT = firstLien.sGseTargetApplicationT;

            secondLien.sTridTargetRegulationVersionTLckd = firstLien.sTridTargetRegulationVersionTLckd;
            secondLien.sTridTargetRegulationVersionT = firstLien.sTridTargetRegulationVersionT;

            firstLien.CreateOrUpdateSubordinateFinancingLiens(secondLien);

            firstLien.Save();
            secondLien.Save();

            if (this.errorUpdatingAdditionalHud1FirstLien || this.errorUpdatingAdditionalHud1SecondLien)
            {
                this.AddAdditionalHud1ErrorMessage();
            }

            return new UpdatedFirstLienInfo
            {
                sIsOFinCreditLineInDrawPeriod = firstLien.sIsOFinCreditLineInDrawPeriod,
                sIsIOnlyForSubFin = firstLien.sIsIOnlyForSubFin,
                sSubFin = firstLien.sSubFin_rep,
                sConcurSubFin = firstLien.sConcurSubFin_rep,
                sSubFinIR = firstLien.sSubFinIR_rep,
                sSubFinTerm = firstLien.sSubFinTerm_rep,
                sSubFinMb = firstLien.sSubFinMb_rep,
                sSubFinPmt = firstLien.sSubFinPmt_rep,
                sSubFinPmtLckd = firstLien.sSubFinPmtLckd,
                sOtherLFinMethT = firstLien.sOtherLFinMethT
            };
        }

        /// <summary>
        /// Initializes the two linked loans and validates that they are indeed linked.
        /// </summary>
        /// <returns>Initialized loans for the first and second liens, respectively, wrapped in a list.</returns>
        private List<CPageData> InitializeLinkedLoans()
        {
            var invokingLoan = new CPageData(this.invokingLoanId, "CPopulate80To20", this.selectStatementProvider);
            invokingLoan.BypassClosingCostSetValidation = true;
            invokingLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            var linkedLoan = new CPageData(this.linkedLoanId, "CPopulate80To20", this.selectStatementProvider);
            linkedLoan.BypassClosingCostSetValidation = true;
            linkedLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            if (!invokingLoan.sLinkedLoanInfo.IsLoanLinked)
            {
                throw CBaseException.GenericException("The invoking loan is not linked.");
            }
            else if (invokingLoan.sLinkedLoanInfo.LinkedLId != linkedLoan.sLId)
            {
                throw CBaseException.GenericException("The invoking loan is not linked to the other loan.");
            }
            else if (invokingLoan.sBrokerId != this.brokerId)
            {
                throw CBaseException.GenericException("The invoking loan's broker ID does not match the broker ID used to load settings");
            }

            CPageData firstLien, secondLien;
            if (invokingLoan.sLienPosT == E_sLienPosT.First)
            {
                firstLien = invokingLoan;
                secondLien = linkedLoan;
            }
            else
            {
                string errMsg = ErrorMessages.FeatureNotAvailableForSecondLienPosition;
                AddMsgToUser(errMsg, false);

                throw new CBaseException(errMsg, errMsg);
            }

            var list = new List<CPageData>(2);
            list.Add(firstLien);
            list.Add(secondLien);
            return list;
        }

        /// <summary>
        /// If there was an error updating the Additional HUD-1 Data page for
        /// either file this will add a message to the user explaining the reason.
        /// </summary>
        private void AddAdditionalHud1ErrorMessage()
        {
            if (this.errorUpdatingAdditionalHud1FirstLien || this.errorUpdatingAdditionalHud1SecondLien)
            {
                string lienDesc;
                if (this.errorUpdatingAdditionalHud1FirstLien && this.errorUpdatingAdditionalHud1SecondLien)
                {
                    lienDesc = "either loan file";
                }
                else if (this.errorUpdatingAdditionalHud1FirstLien)
                {
                    lienDesc = "the 1st lien loan file";
                }
                else
                {
                    lienDesc = "the 2nd lien loan file";
                }

                var userMessage = string.Format(
                    "The system did not update line 105 of the Additional " +
                    "HUD-1 Data page of {0} because the description is different " +
                    "than what was expected. Expected values are {1}, {2}, or blank.",
                    lienDesc,
                    FirstMortgageClosingCostsDesc,
                    SecondMortgageClosingCostsDesc);

                this.AddMsgToUser(userMessage, false);
            }
        }

        /// <summary>
        /// Handles updating the Additional HUD-1 Data page to include the closing
        /// costs of the linked file.
        /// </summary>
        /// <remarks>
        /// Will not update the field if the description is not blank or the 
        /// expected description for line 105.
        /// </remarks>
        /// <param name="sourceLoan">The source file.</param>
        /// <param name="destinationLoan">The destination file.</param>
        private void UpdateAdditionalHud1Data(CPageData sourceLoan, CPageData destinationLoan)
        {
            if (sourceLoan == null)
            {
                throw new ArgumentNullException("sourceLoan");
            }
            else if (destinationLoan == null)
            {
                throw new ArgumentNullException("destinationLoan");
            }

            if (destinationLoan.sLienPosT == E_sLienPosT.First)
            {
                if (string.IsNullOrEmpty(destinationLoan.sGrossDueFromBorrU2FDesc))
                {
                    destinationLoan.sGrossDueFromBorrU2FDesc = SecondMortgageClosingCostsDesc;
                }
                else if (!destinationLoan.sGrossDueFromBorrU2FDesc.Equals(SecondMortgageClosingCostsDesc, StringComparison.OrdinalIgnoreCase))
                {
                    this.errorUpdatingAdditionalHud1FirstLien = true;
                    return;
                }
            }
            else if (destinationLoan.sLienPosT == E_sLienPosT.Second)
            {
                if (string.IsNullOrEmpty(destinationLoan.sGrossDueFromBorrU2FDesc))
                {
                    destinationLoan.sGrossDueFromBorrU2FDesc = FirstMortgageClosingCostsDesc;
                }
                else if (!destinationLoan.sGrossDueFromBorrU2FDesc.Equals(FirstMortgageClosingCostsDesc, StringComparison.OrdinalIgnoreCase))
                {
                    this.errorUpdatingAdditionalHud1SecondLien = true;
                    return;
                }
            }
            
            destinationLoan.sGrossDueFromBorrU2F_rep = sourceLoan.sSettlementTotalEstimateSettlementCharge_rep;
        }

        private void UpdateSecondLienWithFirstLienRaceEthnicityData(CAppData da, CAppData sa)
        {
            da.aBIsMexican = sa.aBIsMexican;
            da.aCIsMexican = sa.aCIsMexican;
            da.aBIsPuertoRican = sa.aBIsPuertoRican;
            da.aCIsPuertoRican = sa.aCIsPuertoRican;
            da.aBIsCuban = sa.aBIsCuban;
            da.aCIsCuban = sa.aCIsCuban;
            da.aBIsOtherHispanicOrLatino = sa.aBIsOtherHispanicOrLatino;
            da.aCIsOtherHispanicOrLatino = sa.aCIsOtherHispanicOrLatino;
            da.aBDoesNotWishToProvideEthnicity = sa.aBDoesNotWishToProvideEthnicity;
            da.aCDoesNotWishToProvideEthnicity = sa.aCDoesNotWishToProvideEthnicity;
            da.aBIsAmericanIndian = sa.aBIsAmericanIndian;
            da.aCIsAmericanIndian = sa.aCIsAmericanIndian;
            da.aBIsAsian = sa.aBIsAsian;
            da.aCIsAsian = sa.aCIsAsian;
            da.aBIsAsianIndian = sa.aBIsAsianIndian;
            da.aCIsAsianIndian = sa.aCIsAsianIndian;
            da.aBIsChinese = sa.aBIsChinese;
            da.aCIsChinese = sa.aCIsChinese;
            da.aBIsFilipino = sa.aBIsFilipino;
            da.aCIsFilipino = sa.aCIsFilipino;
            da.aBIsJapanese = sa.aBIsJapanese;
            da.aCIsJapanese = sa.aCIsJapanese;
            da.aBIsKorean = sa.aBIsKorean;
            da.aCIsKorean = sa.aCIsKorean;
            da.aBIsVietnamese = sa.aBIsVietnamese;
            da.aCIsVietnamese = sa.aCIsVietnamese;
            da.aBIsOtherAsian = sa.aBIsOtherAsian;
            da.aCIsOtherAsian = sa.aCIsOtherAsian;
            da.aBIsWhite = sa.aBIsWhite;
            da.aCIsWhite = sa.aCIsWhite;
            da.aBDoesNotWishToProvideRace = sa.aBDoesNotWishToProvideRace;
            da.aCDoesNotWishToProvideRace = sa.aCDoesNotWishToProvideRace;
            da.aBIsBlack = sa.aBIsBlack;
            da.aCIsBlack = sa.aCIsBlack;
            da.aBIsPacificIslander = sa.aBIsPacificIslander;
            da.aCIsPacificIslander = sa.aCIsPacificIslander;
            da.aBIsNativeHawaiian = sa.aBIsNativeHawaiian;
            da.aCIsNativeHawaiian = sa.aCIsNativeHawaiian;
            da.aBIsGuamanianOrChamorro = sa.aBIsGuamanianOrChamorro;
            da.aCIsGuamanianOrChamorro = sa.aCIsGuamanianOrChamorro;
            da.aBIsSamoan = sa.aBIsSamoan;
            da.aCIsSamoan = sa.aCIsSamoan;
            da.aBIsOtherPacificIslander = sa.aBIsOtherPacificIslander;
            da.aCIsOtherPacificIslander = sa.aCIsOtherPacificIslander;
            da.aBOtherHispanicOrLatinoDescription = sa.aBOtherHispanicOrLatinoDescription;
            da.aCOtherHispanicOrLatinoDescription = sa.aCOtherHispanicOrLatinoDescription;
            da.aBOtherAmericanIndianDescription = sa.aBOtherAmericanIndianDescription;
            da.aCOtherAmericanIndianDescription = sa.aCOtherAmericanIndianDescription;
            da.aBOtherAsianDescription = sa.aBOtherAsianDescription;
            da.aCOtherAsianDescription = sa.aCOtherAsianDescription;
            da.aBOtherPacificIslanderDescription = sa.aBOtherPacificIslanderDescription;
            da.aCOtherPacificIslanderDescription = sa.aCOtherPacificIslanderDescription;
            da.aBEthnicityCollectedByObservationOrSurname = sa.aBEthnicityCollectedByObservationOrSurname;
            da.aCEthnicityCollectedByObservationOrSurname = sa.aCEthnicityCollectedByObservationOrSurname;
            da.aBSexCollectedByObservationOrSurname = sa.aBSexCollectedByObservationOrSurname;
            da.aCSexCollectedByObservationOrSurname = sa.aCSexCollectedByObservationOrSurname;
            da.aBRaceCollectedByObservationOrSurname = sa.aBRaceCollectedByObservationOrSurname;
            da.aCRaceCollectedByObservationOrSurname = sa.aCRaceCollectedByObservationOrSurname;
            da.aBInterviewMethodT = sa.aBInterviewMethodT;
            da.aCInterviewMethodT = sa.aCInterviewMethodT;
            da.aBHispanicT = sa.aBHispanicT;
            da.aCHispanicT = sa.aCHispanicT;
            da.aBGender = sa.aBGender;
            da.aCGender = sa.aCGender;
            da.aBHispanicTFallback = sa.aBHispanicTFallback;
            da.aCHispanicTFallback = sa.aCHispanicTFallback;
            da.aBGenderFallback = sa.aBGenderFallback;
            da.aCGenderFallback = sa.aCGenderFallback;
        }

        public void UpdateSecondLienWithFirstLienData(CPageData sl, CPageData dl, E_Populate80To20Sections sections)
		{
            // OPM 459984 - Don't update 
            if (sl.sLoanVersionT != dl.sLoanVersionT)
            {
                string errMsg = $"Loan file version number does not match on source loan file (v{sl.sLoanVersionT.ToString("d")}) and destination loan file (v{dl.sLoanVersionT.ToString("d")}).";
                AddMsgToUser(errMsg, false);
                throw new CBaseException(errMsg, errMsg);
            }

            int sl_nApps = sl.nApps;
//			if( nApps > 1 )
//			{
//				// TODO: How do we ensure the ranking of the app, we must ensure that too.
//				string errMsg = "This feature is not avaiable for multiple applications. Source loan contains more than one application.";
//				AddMsgToUser( errMsg, false );
            //	throw new CBaseException(ErrorMessages.Generic, errMsg);
//			}

            for (int nAppIndex = 0; nAppIndex < sl_nApps; nAppIndex++) 
            {
                CAppData sa = sl.GetAppData( nAppIndex );


                if( dl.sLienPosT != E_sLienPosT.Second )
                {
                    string errMsg = ErrorMessages.FeatureNotAvailableForFirstLienPosition;
                    AddMsgToUser( errMsg, false );
                    throw new CBaseException(errMsg, errMsg);
                }

                int dl_nApps = dl.nApps;

                if (sl_nApps != dl_nApps) 
                {
                    string errMsg = ErrorMessages.NumberOfCoborrowersDoesntMatchOnLoanFiles;
                    AddMsgToUser(errMsg, false);
                    throw new CBaseException(errMsg, errMsg);
                }
//                if( nApps > 1 )
//                    // TODO: How do we ensure the ranking of the app, we must ensure that too.
//                    throw new CBaseException(ErrorMessages.Generic, "This feature is not avaiable for multiple applications. Destination loan contains more than application." );
                CAppData da = dl.GetAppData( nAppIndex );

                if (sa.aBSsn != da.aBSsn) 
                {
                    // 10/3/2005 dd - OPM 2873 - Support multiple application.
                    string errMsg = ErrorMessages.BorrowerSSNDoesntMatchOnLoanFiles;
                    AddMsgToUser(errMsg, false);
                    throw new CBaseException(errMsg, errMsg);
                }

			#region INTENTIONAL SKIPPING
                // sInternalFundingSrcDesc varchar(100) not null default( '' ),
                // sExternalFundingSrcDesc varchar(100) not null default( '' ),
                // sShippedToInvestorD smalldatetime,
                // sLPurchaseD smalldatetime,
                // sGoodByLetterD smalldatetime,
                // sWarehouseFunderDesc varchar(100) not null default( '' )
			#endregion // INTENTIONAL SKIPPING
			#region BASIC INFO (AUTOMATIC)
			
                dl.sLPurposeT = sl.sLPurposeT;
                dl.sIsOFinNew = true;

                dl.sLpIsNegAmortOtherLien = sl.sIsOptionArm;
                dl.sIsIOnlyForSubFin = sl.sIsIOnly;
                dl.sOtherLFinMethT = sl.sFinMethT;

                if (sl.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)
                {
                    dl.CopyCompensationPlanFromLoan(sl);
                    // The plan type is does not seem to be set in any centralized
                    // fashion, instead it seems we just set it manually all over
                    // the place. It is not cleared in the method called above, so
                    // I am just going to reset it manually. We will eventually want
                    // to centralize how that field is set. gf opm 225494
                    dl.sOriginatorCompensationPlanT = sl.sOriginatorCompensationPlanT;
                }

                #endregion

                #region BORROWER INFO PAGE
                if ( 0 != ( sections & E_Populate80To20Sections.BorrowerInfo ) )
                {
                    // BORROWER INFO PAGE

                    da.aBCoreSystemId = sa.aBCoreSystemId;
                    da.aCCoreSystemId = sa.aCCoreSystemId;
                    da.aBFirstNm	= sa.aBFirstNm;
                    da.aBMidNm		= sa.aBMidNm;
                    da.aBLastNm		= sa.aBLastNm;
                    da.aBSuffix		= sa.aBSuffix;
                    da.aBSsn		= sa.aBSsn;
                    da.aBDob_rep	= sa.aBDob_rep;
                    da.aBAge_rep	= sa.aBAge_rep;
                    da.aBSchoolYrs_rep = sa.aBSchoolYrs_rep;
                    da.aBMaritalStatT = sa.aBMaritalStatT;
                    da.aBDependNum_rep = sa.aBDependNum_rep;
                    da.aBDependAges = sa.aBDependAges;
                    da.aBHPhone		= sa.aBHPhone;
                    da.aBFax        = sa.aBFax;
                    da.aBCellPhone	= sa.aBCellPhone;
                    da.aBBusPhone	= sa.aBBusPhone;
                    da.aBEmail		= sa.aBEmail;

                    da.aBExperianScore_rep = sa.aBExperianScore_rep;
                    da.aBTransUnionScore_rep = sa.aBTransUnionScore_rep;
                    da.aBEquifaxScore_rep = sa.aBEquifaxScore_rep;
                    da.aBEmplrBusPhoneLckd = sa.aBEmplrBusPhoneLckd;

                    da.aHasCoborrowerData = sa.aHasCoborrowerData;
                    da.aCFirstNm	= sa.aCFirstNm;
                    da.aCMidNm		= sa.aCMidNm;
                    da.aCLastNm		= sa.aCLastNm;
                    da.aCSuffix		= sa.aCSuffix;
                    da.aCSsn		= sa.aCSsn;
                    da.aCDob_rep	= sa.aCDob_rep;
                    da.aCAge_rep	= sa.aCAge_rep;
                    da.aCSchoolYrs_rep = sa.aCSchoolYrs_rep;
                    da.aCMaritalStatT = sa.aCMaritalStatT;
                    da.aCDependNum_rep = sa.aCDependNum_rep;
                    da.aCDependAges = sa.aCDependAges;
                    da.aCFax        = sa.aCFax;
                    da.aCHPhone		= sa.aCHPhone;
                    da.aCCellPhone	= sa.aCCellPhone;
                    da.aCBusPhone	= sa.aCBusPhone;
                    da.aCEmail		= sa.aCEmail;

                    ////da.aBAddrMailUsePresentAddr = sa.aBAddrMailUsePresentAddr;
                    da.aBAddrMailSourceT = sa.aBAddrMailSourceT;
                    da.aBMailingAddressData = sa.aBMailingAddressData;
                    da.aBAddrPostSourceTLckd = sa.aBAddrPostSourceTLckd;
                    da.aBAddrPostSourceT = sa.aBAddrPostSourceT;
                    da.aBPostClosingAddressData = sa.aBPostClosingAddressData;

                    ////da.aCAddrMailUsePresentAddr = sa.aCAddrMailUsePresentAddr;
                    da.aCAddrMailSourceT = sa.aCAddrMailSourceT;
                    da.aCMailingAddressData = sa.aCMailingAddressData;
                    da.aCAddrPostSourceTLckd = sa.aCAddrPostSourceTLckd;
                    da.aCAddrPostSourceT = sa.aCAddrPostSourceT;
                    da.aCPostClosingAddressData = sa.aCPostClosingAddressData;

                    if (!sl.sIsHousingHistoryEntriesCollectionEnabled)
                    {
                        da.aBAddr = sa.aBAddr;
                        da.aBCity = sa.aBCity;
                        da.aBState = sa.aBState;
                        da.aBZip = sa.aBZip;
                        da.aBAddrT = sa.aBAddrT;
                        da.aBAddrYrs = sa.aBAddrYrs;
                        da.aBPrev1Addr = sa.aBPrev1Addr;
                        da.aBPrev1City = sa.aBPrev1City;
                        da.aBPrev1State = sa.aBPrev1State;
                        da.aBPrev1Zip = sa.aBPrev1Zip;
                        da.aBPrev1AddrT = sa.aBPrev1AddrT;
                        da.aBPrev1AddrYrs = sa.aBPrev1AddrYrs;
                        da.aBPrev2Addr = sa.aBPrev2Addr;
                        da.aBPrev2City = sa.aBPrev2City;
                        da.aBPrev2State = sa.aBPrev2State;
                        da.aBPrev2Zip = sa.aBPrev2Zip;
                        da.aBPrev2AddrT = sa.aBPrev2AddrT;
                        da.aBPrev2AddrYrs = sa.aBPrev2AddrYrs;
                        da.aBAddrMail = sa.aBAddrMail;
                        da.aBCityMail = sa.aBCityMail;
                        da.aBStateMail = sa.aBStateMail;
                        da.aBZipMail = sa.aBZipMail;
                        da.aBAddrPost = sa.aBAddrPost;
                        da.aBCityPost = sa.aBCityPost;
                        da.aBStatePost = sa.aBStatePost;
                        da.aBZipPost = sa.aBZipPost;

                        da.aCAddr = sa.aCAddr;
                        da.aCCity = sa.aCCity;
                        da.aCState = sa.aCState;
                        da.aCZip = sa.aCZip;
                        da.aCAddrT = sa.aCAddrT;
                        da.aCAddrYrs = sa.aCAddrYrs;
                        da.aCPrev1Addr = sa.aCPrev1Addr;
                        da.aCPrev1City = sa.aCPrev1City;
                        da.aCPrev1State = sa.aCPrev1State;
                        da.aCPrev1Zip = sa.aCPrev1Zip;
                        da.aCPrev1AddrT = sa.aCPrev1AddrT;
                        da.aCPrev1AddrYrs = sa.aCPrev1AddrYrs;
                        da.aCPrev2Addr = sa.aCPrev2Addr;
                        da.aCPrev2City = sa.aCPrev2City;
                        da.aCPrev2State = sa.aCPrev2State;
                        da.aCPrev2Zip = sa.aCPrev2Zip;
                        da.aCPrev2AddrT = sa.aCPrev2AddrT;
                        da.aCPrev2AddrYrs = sa.aCPrev2AddrYrs;
                        da.aCAddrMail = sa.aCAddrMail;
                        da.aCCityMail = sa.aCCityMail;
                        da.aCStateMail = sa.aCStateMail;
                        da.aCZipMail = sa.aCZipMail;
                        da.aCAddrPost = sa.aCAddrPost;
                        da.aCCityPost = sa.aCCityPost;
                        da.aCStatePost = sa.aCStatePost;
                        da.aCZipPost = sa.aCZipPost;
                    }

                    da.aCExperianScore_rep = sa.aCExperianScore_rep;
                    da.aCTransUnionScore_rep = sa.aCTransUnionScore_rep;
                    da.aCEquifaxScore_rep = sa.aCEquifaxScore_rep;
                    da.aCEmplrBusPhoneLckd = sa.aCEmplrBusPhoneLckd;

                    if (sl.sBorrowerApplicationCollectionT == E_sLqbCollectionT.Legacy)
                    {
                        // BORROWER EMPLOYMENT PAGE
                        da.AcceptNewBEmplmtCollection(sa.aBEmplmtXmlContent);

                        // COBORROWER EMPLOYMENT PAGE
                        da.AcceptNewCEmplmtCollection(sa.aCEmplmtXmlContent);
                    }

                    // MONTHLY INCOME PAGE

                    if (!sl.sIsIncomeCollectionEnabled)
                    {
                        da.aBBaseI_rep = sa.aBBaseI_rep;
                        da.aBOvertimeI_rep = sa.aBOvertimeI_rep;
                        da.aBBonusesI_rep = sa.aBBonusesI_rep;
                        da.aBCommisionI_rep = sa.aBCommisionI_rep;
                        da.aBDividendI_rep = sa.aBDividendI_rep;
                        da.aCBaseI_rep = sa.aCBaseI_rep;
                        da.aCOvertimeI_rep = sa.aCOvertimeI_rep;
                        da.aCBonusesI_rep = sa.aCBonusesI_rep;
                        da.aCCommisionI_rep = sa.aCCommisionI_rep;
                        da.aCDividendI_rep = sa.aCDividendI_rep;
                        da.aOtherIncomeList = sa.aOtherIncomeList;
                    }

                    da.aBNetRentI1003_rep = sa.aBNetRentI1003_rep;
                    da.aCNetRentI1003_rep = sa.aCNetRentI1003_rep;
                    da.aNetRentI1003Lckd= sa.aNetRentI1003Lckd;

                    if (sl.sBorrowerApplicationCollectionT == E_sLqbCollectionT.Legacy)
                    {
                        // LIABILITIES PAGE
                        da.AcceptNewLiaCollection(sa.aLiaXmlContent.Value);

                        // ASSETS PAGE
                        da.AcceptNewAssetCollection(sa.aAssetXmlContent.Value);

                        // REO PAGE
                        da.AcceptNewReCollection(sa.aReXmlContent);
                    }

                    // HOUSING EXPENSE PAGE

                    da.aPresRent_rep = sa.aPresRent_rep;
                    da.aPres1stM_rep = sa.aPres1stM_rep;
                    da.aPresOFin_rep = sa.aPresOFin_rep;
                    da.aPresHazIns_rep = sa.aPresHazIns_rep;
                    da.aPresRealETx_rep = sa.aPresRealETx_rep;
                    da.aPresMIns_rep = sa.aPresMIns_rep;
                    da.aPresHoAssocDues_rep = sa.aPresHoAssocDues_rep;
                    da.aPresOHExpDesc = sa.aPresOHExpDesc;
                    da.aPresOHExp_rep = sa.aPresOHExp_rep;
                    da.aOccT = sa.aOccT;
				
                }
			#endregion // BORROWER INFO PAGE

			#region SUBJECT PROPERTY INFO
                if( 0 != ( sections & E_Populate80To20Sections.SubjPropInfo ) )
                {
                    // SUBJECT PROPERTY DESCRIPTION PAGE

                    dl.sSpAddr = sl.sSpAddr;
                    dl.sSpCity = sl.sSpCity;
                    dl.sSpState = sl.sSpState;
                    dl.sSpZip	= sl.sSpZip;
                    dl.sSpCounty = sl.sSpCounty;
                    dl.sUnitsNum_rep = sl.sUnitsNum_rep;
                    dl.sYrBuilt = sl.sYrBuilt;
                    dl.sSpLegalDesc = sl.sSpLegalDesc;
                    dl.sNfipFloodZoneId = sl.sNfipFloodZoneId;
                    dl.sAssessorsParcelId = sl.sAssessorsParcelId;
                    dl.sHasMultipleParcels = sl.sHasMultipleParcels;
                    dl.sSubjPropertyMineralAbbrLegalDesc = sl.sSubjPropertyMineralAbbrLegalDesc;
                    dl.sFloodCertificationIsInSpecialArea = sl.sFloodCertificationIsInSpecialArea;

                    // SUBJECT PROPERTY RENTAL INCOME PAGE
                    dl.sSpGrossRentLckd = sl.sSpGrossRentLckd;
                    dl.sSpGrossRent_rep = sl.sSpGrossRent_rep;
                    dl.sOccRLckd = sl.sOccRLckd;
                    dl.sOccR_rep = sl.sOccR_rep;
                    dl.sSpCountRentalIForPrimaryResidToo = sl.sSpCountRentalIForPrimaryResidToo;

                    // THIS LOAN INFORMATION PAGE (partial)
                    if( dl.sProRealETx_rep != sl.sProRealETx_rep )
                    {
                        dl.sProRealETxR_rep = "0";
                        dl.sProRealETxMb_rep = sl.sProRealETx_rep;
                    }
                    if( dl.sProHazIns_rep != sl.sProHazIns_rep )
                    {
                        dl.sProHazInsR_rep = "0";
                        dl.sProHazInsMb_rep = sl.sProHazIns_rep;
                    }
                    dl.sProHoAssocDues_rep = sl.sProHoAssocDues_rep;
                    dl.sProOHExp_rep = sl.sProOHExp_rep;

                    dl.sPurchPrice_rep = sl.sPurchPrice_rep;
                    dl.sApprVal_rep = sl.sApprVal_rep;
                }
			#endregion // SUBJECT PROPERTY INFO

			#region LOAN AMOUNT AND PAYMENT
                if( 0 != ( sections & E_Populate80To20Sections.LAmtAndPmt ) )
                {
                    dl.s1stMtgOrigLAmt_rep = sl.sLAmtCalc_rep;
                    dl.sRemain1stMPmt = sl.sProThisMPmt + sl.sProMIns;
                }
			#endregion // LOAN AMOUNT AND PAYMENT

			#region LPE SEARCH PARAMETERS
                if( 0 != ( sections & E_Populate80To20Sections.LpeSearchParam ) )
                {
                    dl.sProdDocT				= sl.sProdDocT;
                    dl.sProdSpT					= sl.sProdSpT;
                    dl.sProdCondoStories_rep	= sl.sProdCondoStories_rep;
                    dl.sProdCashoutAmt_rep		= sl.sProdCashoutAmt_rep;
                    dl.sProdRLckdDays_rep		= sl.sProdRLckdDays_rep;
                    dl.sProdImpound				= sl.sProdImpound;
                }
			#endregion // LPE SEARCH PARAMETERS

            #region 1003
                da.a1003ContEditSheet          = sa.a1003ContEditSheet;        
                da.aAltNm1                     = sa.aAltNm1;
                da.aAltNm1AccNum               = sa.aAltNm1AccNum;
                da.aAltNm1CreditorNm           = sa.aAltNm1CreditorNm;
                da.aAltNm2                     = sa.aAltNm2;
                da.aAltNm2AccNum               = sa.aAltNm2AccNum;
                da.aAltNm2CreditorNm           = sa.aAltNm2CreditorNm;
                da.aAsstLiaCompletedNotJointly = sa.aAsstLiaCompletedNotJointly;
                da.aBDecAlimony                = sa.aBDecAlimony;
                da.aBDecBankrupt               = sa.aBDecBankrupt;
                da.aBDecBorrowing              = sa.aBDecBorrowing;
                da.aBDecCitizen                = sa.aBDecCitizen;
                da.aBDecDelinquent             = sa.aBDecDelinquent;
                da.aBDecEndorser               = sa.aBDecEndorser;
                da.aBDecForeclosure            = sa.aBDecForeclosure;
                da.aBDecJudgment               = sa.aBDecJudgment;
                da.aBDecLawsuit                = sa.aBDecLawsuit;
                da.aBDecObligated              = sa.aBDecObligated;
                da.aBDecOcc                    = sa.aBDecOcc;
                da.aBDecPastOwnedPropT         = sa.aBDecPastOwnedPropT;
                da.aBDecPastOwnedPropTitleT    = sa.aBDecPastOwnedPropTitleT;
                da.aBDecPastOwnership          = sa.aBDecPastOwnership;
                da.aBDecResidency              = sa.aBDecResidency;
                da.aBNoFurnish                 = sa.aBNoFurnish;
                da.aBNoFurnishLckd             = sa.aBNoFurnishLckd;
                da.aCDecAlimony                = sa.aCDecAlimony;
                da.aCDecBankrupt               = sa.aCDecBankrupt;
                da.aCDecBorrowing              = sa.aCDecBorrowing;
                da.aCDecCitizen                = sa.aCDecCitizen;
                da.aCDecDelinquent             = sa.aCDecDelinquent;
                da.aCDecEndorser               = sa.aCDecEndorser;
                da.aCDecForeclosure            = sa.aCDecForeclosure;
                da.aCDecJudgment               = sa.aCDecJudgment;
                da.aCDecLawsuit                = sa.aCDecLawsuit;
                da.aCDecObligated              = sa.aCDecObligated;
                da.aCDecOcc                    = sa.aCDecOcc;
                da.aCDecPastOwnedPropT         = sa.aCDecPastOwnedPropT;
                da.aCDecPastOwnedPropTitleT    = sa.aCDecPastOwnedPropTitleT;
                da.aCDecPastOwnership          = sa.aCDecPastOwnership;
                da.aCDecResidency              = sa.aCDecResidency;
                da.aCNoFurnish                 = sa.aCNoFurnish;
                da.aCNoFurnishLckd             = sa.aCNoFurnishLckd;
                da.aIntrvwrMethodT             = sa.aIntrvwrMethodT;
                da.aIntrvwrMethodTLckd = sa.aIntrvwrMethodTLckd;
                da.aManner                     = sa.aManner;
                da.aSpouseIExcl                = sa.aSpouseIExcl;
                da.aTitleNm1                   = sa.aTitleNm1;
                da.aTitleNm2                   = sa.aTitleNm2;

                this.UpdateSecondLienWithFirstLienRaceEthnicityData(da, sa);

                dl.sAgencyCaseNum              = sl.sAgencyCaseNum;
                dl.sAltCost_rep                = sl.sAltCost_rep;
                dl.sDwnPmtSrc                  = sl.sDwnPmtSrc;
                dl.sDwnPmtSrcExplain           = sl.sDwnPmtSrcExplain;
                dl.sEstateHeldT                = sl.sEstateHeldT;
                //dl.sFfUfmip1003_rep            = sl.sFfUfmip1003_rep;
                //dl.sFfUfmip1003Lckd            = sl.sFfUfmip1003Lckd;
                //dl.sLDiscnt1003_rep            = sl.sLDiscnt1003_rep;
                //dl.sLDiscnt1003Lckd            = sl.sLDiscnt1003Lckd;
                dl.sLT                         = sl.sLT;
                dl.sLTODesc                    = sl.sLTODesc;
                dl.sLandCost_rep               = sl.sLandCost_rep;
                dl.sLeaseHoldExpireD_rep       = sl.sLeaseHoldExpireD_rep;
//                dl.sLenderCaseNum              = sl.sLenderCaseNum; // OPM 7854
                dl.sLotAcqYr                   = sl.sLotAcqYr; 
                dl.sLotImprovC_rep             = sl.sLotImprovC_rep; 
                dl.sLotLien_rep                = sl.sLotLien_rep; 
                dl.sLotOrigC_rep               = sl.sLotOrigC_rep; 
                dl.sLotVal_rep                 = sl.sLotVal_rep; 
                dl.sMultiApps                  = sl.sMultiApps;
                sl.CopyCreditsToSecondLien(dl);
                dl.sOLPurposeDesc              = sl.sOLPurposeDesc;
                //dl.sProHazInsT                 = sl.sProHazInsT; 
                //dl.sProMInsT                   = sl.sProMInsT; 
                //dl.sProRealETxT                = sl.sProRealETxT; 
                dl.sRefPdOffAmt1003_rep        = sl.sRefPdOffAmt1003_rep;
                dl.sRefPdOffAmt1003Lckd        = sl.sRefPdOffAmt1003Lckd;
                dl.sRefPurpose                 = sl.sRefPurpose; 
                dl.sSpAcqYr                    = sl.sSpAcqYr; 
                dl.sSpImprovC                  = sl.sSpImprovC; 
                dl.sSpImprovDesc               = sl.sSpImprovDesc; 
                dl.sSpImprovTimeFrameT         = sl.sSpImprovTimeFrameT; 
                dl.sSpLien_rep                 = sl.sSpLien_rep; 
                dl.sSpOrigC_rep                = sl.sSpOrigC_rep; 
                //dl.sTotCcPbs_rep               = sl.sTotCcPbs_rep;
                //dl.sTotCcPbsLocked             = sl.sTotCcPbsLocked;
                dl.sTotEstCc1003Lckd           = sl.sTotEstCc1003Lckd;
                dl.sTotEstCcNoDiscnt1003_rep   = sl.sTotEstCcNoDiscnt1003_rep;
                //dl.sTotEstPp1003_rep           = sl.sTotEstPp1003_rep;
                //dl.sTotEstPp1003Lckd           = sl.sTotEstPp1003Lckd;
                dl.sTransNetCash_rep           = sl.sTransNetCash_rep;
                dl.sTransNetCashLckd           = sl.sTransNetCashLckd;

            #endregion

            #region URLA
            da.aBDecAmtBorrowedFromOthersForTransUlad = sa.aBDecAmtBorrowedFromOthersForTransUlad;
            da.aCDecAmtBorrowedFromOthersForTransUlad = sa.aCDecAmtBorrowedFromOthersForTransUlad;
            da.aBDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueUlad = sa.aBDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueUlad;
            da.aCDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueUlad = sa.aCDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueUlad;
            da.aBDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueExplanationUlad = sa.aBDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueExplanationUlad;
            da.aCDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueExplanationUlad = sa.aCDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueExplanationUlad;
            da.aBDecHasBankruptcyChapter11Ulad = sa.aBDecHasBankruptcyChapter11Ulad;
            da.aCDecHasBankruptcyChapter11Ulad = sa.aCDecHasBankruptcyChapter11Ulad;
            da.aBDecHasBankruptcyChapter12Ulad = sa.aBDecHasBankruptcyChapter12Ulad;
            da.aCDecHasBankruptcyChapter12Ulad = sa.aCDecHasBankruptcyChapter12Ulad;
            da.aBDecHasBankruptcyChapter13Ulad = sa.aBDecHasBankruptcyChapter13Ulad;
            da.aCDecHasBankruptcyChapter13Ulad = sa.aCDecHasBankruptcyChapter13Ulad;
            da.aBDecHasBankruptcyChapter7Ulad = sa.aBDecHasBankruptcyChapter7Ulad;
            da.aCDecHasBankruptcyChapter7Ulad = sa.aCDecHasBankruptcyChapter7Ulad;
            da.aBDecHasBankruptcyLast7YearsUlad = sa.aBDecHasBankruptcyLast7YearsUlad;
            da.aCDecHasBankruptcyLast7YearsUlad = sa.aCDecHasBankruptcyLast7YearsUlad;
            da.aBDecHasBankruptcyLast7YearsExplanationUlad = sa.aBDecHasBankruptcyLast7YearsExplanationUlad;
            da.aCDecHasBankruptcyLast7YearsExplanationUlad = sa.aCDecHasBankruptcyLast7YearsExplanationUlad;
            da.aBDecHasConveyedTitleInLieuOfFcLast7YearsUlad = sa.aBDecHasConveyedTitleInLieuOfFcLast7YearsUlad;
            da.aCDecHasConveyedTitleInLieuOfFcLast7YearsUlad = sa.aCDecHasConveyedTitleInLieuOfFcLast7YearsUlad;
            da.aBDecHasConveyedTitleInLieuOfFcLast7YearsExplanationUlad = sa.aBDecHasConveyedTitleInLieuOfFcLast7YearsExplanationUlad;
            da.aCDecHasConveyedTitleInLieuOfFcLast7YearsExplanationUlad = sa.aCDecHasConveyedTitleInLieuOfFcLast7YearsExplanationUlad;
            da.aBDecHasForeclosedLast7YearsUlad = sa.aBDecHasForeclosedLast7YearsUlad;
            da.aCDecHasForeclosedLast7YearsUlad = sa.aCDecHasForeclosedLast7YearsUlad;
            da.aBDecHasForeclosedLast7YearsExplanationUlad = sa.aBDecHasForeclosedLast7YearsExplanationUlad;
            da.aCDecHasForeclosedLast7YearsExplanationUlad = sa.aCDecHasForeclosedLast7YearsExplanationUlad;
            da.aBDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscUlad = sa.aBDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscUlad;
            da.aCDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscUlad = sa.aCDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscUlad;
            da.aBDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscExplanationUlad = sa.aBDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscExplanationUlad;
            da.aCDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscExplanationUlad = sa.aCDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscExplanationUlad;
            da.aBDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscUlad = sa.aBDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscUlad;
            da.aCDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscUlad = sa.aCDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscUlad;
            da.aBDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscExplanationUlad = sa.aBDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscExplanationUlad;
            da.aCDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscExplanationUlad = sa.aCDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscExplanationUlad;
            da.aBDecHasOutstandingJudgmentsUlad = sa.aBDecHasOutstandingJudgmentsUlad;
            da.aCDecHasOutstandingJudgmentsUlad = sa.aCDecHasOutstandingJudgmentsUlad;
            da.aBDecHasOutstandingJudgmentsExplanationUlad = sa.aBDecHasOutstandingJudgmentsExplanationUlad;
            da.aCDecHasOutstandingJudgmentsExplanationUlad = sa.aCDecHasOutstandingJudgmentsExplanationUlad;
            da.aBDecHasOwnershipIntOtherPropLastThreeYearsUlad = sa.aBDecHasOwnershipIntOtherPropLastThreeYearsUlad;
            da.aCDecHasOwnershipIntOtherPropLastThreeYearsUlad = sa.aCDecHasOwnershipIntOtherPropLastThreeYearsUlad;
            da.aBDecHasOwnershipIntOtherPropLastThreeYearsExplanationUlad = sa.aBDecHasOwnershipIntOtherPropLastThreeYearsExplanationUlad;
            da.aCDecHasOwnershipIntOtherPropLastThreeYearsExplanationUlad = sa.aCDecHasOwnershipIntOtherPropLastThreeYearsExplanationUlad;
            da.aBDecIsBorrowingFromOthersForTransUlad = sa.aBDecIsBorrowingFromOthersForTransUlad;
            da.aCDecIsBorrowingFromOthersForTransUlad = sa.aCDecIsBorrowingFromOthersForTransUlad;
            da.aBDecIsBorrowingFromOthersForTransExplanationUlad = sa.aBDecIsBorrowingFromOthersForTransExplanationUlad;
            da.aCDecIsBorrowingFromOthersForTransExplanationUlad = sa.aCDecIsBorrowingFromOthersForTransExplanationUlad;
            da.aBDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedUlad = sa.aBDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedUlad;
            da.aCDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedUlad = sa.aCDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedUlad;
            da.aBDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedExplanationUlad = sa.aBDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedExplanationUlad;
            da.aCDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedExplanationUlad = sa.aCDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedExplanationUlad;
            da.aBDecIsDelinquentOrDefaultFedDebtUlad = sa.aBDecIsDelinquentOrDefaultFedDebtUlad;
            da.aCDecIsDelinquentOrDefaultFedDebtUlad = sa.aCDecIsDelinquentOrDefaultFedDebtUlad;
            da.aBDecIsDelinquentOrDefaultFedDebtExplanationUlad = sa.aBDecIsDelinquentOrDefaultFedDebtExplanationUlad;
            da.aCDecIsDelinquentOrDefaultFedDebtExplanationUlad = sa.aCDecIsDelinquentOrDefaultFedDebtExplanationUlad;
            da.aBDecIsFamilyOrBusAffiliateOfSellerUlad = sa.aBDecIsFamilyOrBusAffiliateOfSellerUlad;
            da.aCDecIsFamilyOrBusAffiliateOfSellerUlad = sa.aCDecIsFamilyOrBusAffiliateOfSellerUlad;
            da.aBDecIsFamilyOrBusAffiliateOfSellerExplanationUlad = sa.aBDecIsFamilyOrBusAffiliateOfSellerExplanationUlad;
            da.aCDecIsFamilyOrBusAffiliateOfSellerExplanationUlad = sa.aCDecIsFamilyOrBusAffiliateOfSellerExplanationUlad;
            da.aBDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityUlad = sa.aBDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityUlad;
            da.aCDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityUlad = sa.aCDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityUlad;
            da.aBDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityExplanationUlad = sa.aBDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityExplanationUlad;
            da.aCDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityExplanationUlad = sa.aCDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityExplanationUlad;
            da.aBDecIsPrimaryResidenceUlad = sa.aBDecIsPrimaryResidenceUlad;
            da.aCDecIsPrimaryResidenceUlad = sa.aCDecIsPrimaryResidenceUlad;
            da.aBDecIsPrimaryResidenceExplanationUlad = sa.aBDecIsPrimaryResidenceExplanationUlad;
            da.aCDecIsPrimaryResidenceExplanationUlad = sa.aCDecIsPrimaryResidenceExplanationUlad;
            da.aBDecOwnershipIntOtherPropLastThreeYearsPropTypeUlad = sa.aBDecOwnershipIntOtherPropLastThreeYearsPropTypeUlad;
            da.aCDecOwnershipIntOtherPropLastThreeYearsPropTypeUlad = sa.aCDecOwnershipIntOtherPropLastThreeYearsPropTypeUlad;
            da.aBDecOwnershipIntOtherPropLastThreeYearsTitleHoldTypeUlad = sa.aBDecOwnershipIntOtherPropLastThreeYearsTitleHoldTypeUlad;
            da.aCDecOwnershipIntOtherPropLastThreeYearsTitleHoldTypeUlad = sa.aCDecOwnershipIntOtherPropLastThreeYearsTitleHoldTypeUlad;
            da.aBDecPropSubjectToHigherPriorityLienThanFirstMtgUlad = sa.aBDecPropSubjectToHigherPriorityLienThanFirstMtgUlad;
            da.aCDecPropSubjectToHigherPriorityLienThanFirstMtgUlad = sa.aCDecPropSubjectToHigherPriorityLienThanFirstMtgUlad;
            da.aBDecPropSubjectToHigherPriorityLienThanFirstMtgExplanationUlad = sa.aBDecPropSubjectToHigherPriorityLienThanFirstMtgExplanationUlad;
            da.aCDecPropSubjectToHigherPriorityLienThanFirstMtgExplanationUlad = sa.aCDecPropSubjectToHigherPriorityLienThanFirstMtgExplanationUlad;
            #endregion

            #region PREPARER INFORMATION
                if( 0 != ( sections & E_Populate80To20Sections.PreparerInfo ) )
                {
                    dl.sPreparerDataSet = sl.sPreparerDataSet;
                }
            #endregion

            #region AGENT INFORMATION
                if(0 != (sections & E_Populate80To20Sections.AgentInfo))
                {
                    ////dl.sAgentDataSet = sl.sAgentDataSet;
                    MergeAgents(sl, dl);
                }
            #endregion

            #region GFE
            
                /*
                            dl.s1006ProHExp_rep       = sl.s1006ProHExp_rep;
                            dl.s1006ProHExpDesc       = sl.s1006ProHExpDesc;
                            dl.s1006RsrvMon_rep       = sl.s1006RsrvMon_rep;
                            dl.s1006RsrvProps         = sl.s1006RsrvProps;
                            dl.s1007ProHExp_rep       = sl.s1007ProHExp_rep;
                            dl.s1007ProHExpDesc       = sl.s1007ProHExpDesc;
                            dl.s1007RsrvMon_rep       = sl.s1007RsrvMon_rep;
                            dl.s1007RsrvProps         = sl.s1007RsrvProps;
                            dl.s800U1F_rep            = sl.s800U1F_rep;
                            dl.s800U1FCode            = sl.s800U1FCode;
                            dl.s800U1FDesc            = sl.s800U1FDesc;
                            dl.s800U1FProps           = sl.s800U1FProps;
                            dl.s800U2F_rep            = sl.s800U2F_rep;
                            dl.s800U2FCode            = sl.s800U2FCode;
                            dl.s800U2FDesc            = sl.s800U2FDesc;
                            dl.s800U2FProps           = sl.s800U2FProps;
                            dl.s800U3F_rep            = sl.s800U3F_rep;
                            dl.s800U3FCode            = sl.s800U3FCode;
                            dl.s800U3FDesc            = sl.s800U3FDesc;
                            dl.s800U3FProps           = sl.s800U3FProps;
                            dl.s800U4F_rep            = sl.s800U4F_rep;
                            dl.s800U4FCode            = sl.s800U4FCode;
                            dl.s800U4FDesc            = sl.s800U4FDesc;
                            dl.s800U4FProps           = sl.s800U4FProps;
                            dl.s800U5F_rep            = sl.s800U5F_rep;
                            dl.s800U5FCode            = sl.s800U5FCode;
                            dl.s800U5FDesc            = sl.s800U5FDesc;
                            dl.s800U5FProps           = sl.s800U5FProps;
                            dl.s900U1Pia_rep          = sl.s900U1Pia_rep;
                            dl.s900U1PiaCode          = sl.s900U1PiaCode;
                            dl.s900U1PiaDesc          = sl.s900U1PiaDesc;
                            dl.s900U1PiaProps         = sl.s900U1PiaProps;
                            dl.s904Pia_rep            = sl.s904Pia_rep;
                            dl.s904PiaDesc            = sl.s904PiaDesc;
                            dl.s904PiaProps           = sl.s904PiaProps;
                            dl.sAggregateAdjRsrv_rep  = sl.sAggregateAdjRsrv_rep;
                            dl.sAggregateAdjRsrvProps = sl.sAggregateAdjRsrvProps;
                            dl.sApprF_rep             = sl.sApprF_rep;
                            dl.sApprFPaid             = sl.sApprFPaid;
                            dl.sApprFProps            = sl.sApprFProps;
                            dl.sAttorneyF_rep         = sl.sAttorneyF_rep;
                            dl.sAttorneyFProps        = sl.sAttorneyFProps;
                            dl.sCountyRtcBaseT        = sl.sCountyRtcBaseT;
                            dl.sCountyRtcDesc         = sl.sCountyRtcDesc;
                            dl.sCountyRtcMb_rep       = sl.sCountyRtcMb_rep;
                            dl.sCountyRtcPc_rep       = sl.sCountyRtcPc_rep;
                            dl.sCountyRtcProps        = sl.sCountyRtcProps;
                            dl.sCrF_rep               = sl.sCrF_rep;
                            dl.sCrFPaid               = sl.sCrFPaid;
                            dl.sCrFProps              = sl.sCrFProps;
                            dl.sDaysInYr_rep          = sl.sDaysInYr_rep;
                            dl.sDocPrepF_rep          = sl.sDocPrepF_rep;
                            dl.sDocPrepFProps         = sl.sDocPrepFProps;
                            dl.sEscrowF_rep           = sl.sEscrowF_rep;
                            dl.sEscrowFProps          = sl.sEscrowFProps;
                            dl.sEscrowFTable          = sl.sEscrowFTable;
                            dl.sEstCloseD_rep         = sl.sEstCloseD_rep;
                            dl.sFloodInsRsrvMon_rep   = sl.sFloodInsRsrvMon_rep;
                            dl.sFloodInsRsrvProps     = sl.sFloodInsRsrvProps;
                            dl.sHazInsPiaMon_rep      = sl.sHazInsPiaMon_rep;
                            dl.sHazInsPiaProps        = sl.sHazInsPiaProps;
                            dl.sHazInsRsrvMon_rep     = sl.sHazInsRsrvMon_rep;
                            dl.sHazInsRsrvProps       = sl.sHazInsRsrvProps;
                            dl.sIPiaDy_rep            = sl.sIPiaDy_rep;
                            dl.sIPiaProps             = sl.sIPiaProps;
                            dl.sInspectF_rep          = sl.sInspectF_rep;
                            dl.sInspectFProps         = sl.sInspectFProps;
                            dl.sLDiscntFMb_rep        = sl.sLDiscntFMb_rep; 
                            dl.sLDiscntPc_rep         = sl.sLDiscntPc_rep; 
                            dl.sLDiscntProps          = sl.sLDiscntProps; 
                            dl.sLOrigFMb_rep          = sl.sLOrigFMb_rep; 
                            dl.sLOrigFPc_rep          = sl.sLOrigFPc_rep; 
                            dl.sLOrigFProps           = sl.sLOrigFProps; 
                            dl.sMBrokFMb_rep          = sl.sMBrokFMb_rep;
                            dl.sMBrokFPc_rep          = sl.sMBrokFPc_rep;
                            dl.sMBrokFProps           = sl.sMBrokFProps;
                            dl.sMInsRsrvMon_rep       = sl.sMInsRsrvMon_rep;
                            dl.sMInsRsrvProps         = sl.sMInsRsrvProps;
                            dl.sMipPiaProps           = sl.sMipPiaProps;
                            dl.sNotaryF_rep           = sl.sNotaryF_rep;
                            dl.sNotaryFProps          = sl.sNotaryFProps;
                            dl.sPestInspectF_rep      = sl.sPestInspectF_rep;
                            dl.sPestInspectFProps     = sl.sPestInspectFProps;
                            dl.sProFloodIns_rep       = sl.sProFloodIns_rep;
                            dl.sProSchoolTx_rep       = sl.sProSchoolTx_rep;
                            dl.sProcF_rep             = sl.sProcF_rep;
                            dl.sProcFPaid             = sl.sProcFPaid;
                            dl.sProcFProps            = sl.sProcFProps;
                            dl.sRealETxRsrvMon_rep    = sl.sRealETxRsrvMon_rep;
                            dl.sRealETxRsrvProps      = sl.sRealETxRsrvProps;
                            dl.sRecBaseT              = sl.sRecBaseT;
                            dl.sRecFDesc              = sl.sRecFDesc;
                            dl.sRecFMb_rep            = sl.sRecFMb_rep;
                            dl.sRecFPc_rep            = sl.sRecFPc_rep;
                            dl.sRecFProps             = sl.sRecFProps;
                            dl.sSchedDueD1_rep        = sl.sSchedDueD1_rep;
                            dl.sSchoolTxRsrvMon_rep   = sl.sSchoolTxRsrvMon_rep;
                            dl.sSchoolTxRsrvProps     = sl.sSchoolTxRsrvProps;
                            dl.sStateRtcBaseT         = sl.sStateRtcBaseT;
                            dl.sStateRtcDesc          = sl.sStateRtcDesc;
                            dl.sStateRtcMb_rep        = sl.sStateRtcMb_rep;
                            dl.sStateRtcPc_rep        = sl.sStateRtcPc_rep;
                            dl.sStateRtcProps         = sl.sStateRtcProps;
                            dl.sTitleInsF_rep         = sl.sTitleInsF_rep;
                            dl.sTitleInsFProps        = sl.sTitleInsFProps;
                            dl.sTitleInsFTable        = sl.sTitleInsFTable;
                            dl.sTxServF_rep           = sl.sTxServF_rep;
                            dl.sTxServFProps          = sl.sTxServFProps;
                            dl.sU1GovRtcBaseT         = sl.sU1GovRtcBaseT;
                            dl.sU1GovRtcCode          = sl.sU1GovRtcCode;
                            dl.sU1GovRtcDesc          = sl.sU1GovRtcDesc;
                            dl.sU1GovRtcMb_rep        = sl.sU1GovRtcMb_rep;
                            dl.sU1GovRtcPc_rep        = sl.sU1GovRtcPc_rep;
                            dl.sU1GovRtcProps         = sl.sU1GovRtcProps;
                            dl.sU1Sc_rep              = sl.sU1Sc_rep;
                            dl.sU1ScCode              = sl.sU1ScCode;
                            dl.sU1ScDesc              = sl.sU1ScDesc;
                            dl.sU1ScProps             = sl.sU1ScProps;
                            dl.sU1Tc_rep              = sl.sU1Tc_rep;
                            dl.sU1TcCode              = sl.sU1TcCode;
                            dl.sU1TcDesc              = sl.sU1TcDesc;
                            dl.sU1TcProps             = sl.sU1TcProps;
                            dl.sU2GovRtcBaseT         = sl.sU2GovRtcBaseT;
                            dl.sU2GovRtcCode          = sl.sU2GovRtcCode;
                            dl.sU2GovRtcDesc          = sl.sU2GovRtcDesc;
                            dl.sU2GovRtcMb_rep        = sl.sU2GovRtcMb_rep;
                            dl.sU2GovRtcPc_rep        = sl.sU2GovRtcPc_rep;
                            dl.sU2GovRtcProps         = sl.sU2GovRtcProps;
                            dl.sU2Sc_rep              = sl.sU2Sc_rep;
                            dl.sU2ScCode              = sl.sU2ScCode;
                            dl.sU2ScDesc              = sl.sU2ScDesc;
                            dl.sU2ScProps             = sl.sU2ScProps;
                            dl.sU2Tc_rep              = sl.sU2Tc_rep;
                            dl.sU2TcCode              = sl.sU2TcCode;
                            dl.sU2TcDesc              = sl.sU2TcDesc;
                            dl.sU2TcProps             = sl.sU2TcProps;
                            dl.sU3GovRtcBaseT         = sl.sU3GovRtcBaseT;
                            dl.sU3GovRtcCode          = sl.sU3GovRtcCode;
                            dl.sU3GovRtcDesc          = sl.sU3GovRtcDesc;
                            dl.sU3GovRtcMb_rep        = sl.sU3GovRtcMb_rep;
                            dl.sU3GovRtcPc_rep        = sl.sU3GovRtcPc_rep;
                            dl.sU3GovRtcProps         = sl.sU3GovRtcProps;
                            dl.sU3Sc_rep              = sl.sU3Sc_rep;
                            dl.sU3ScCode              = sl.sU3ScCode;
                            dl.sU3ScDesc              = sl.sU3ScDesc;
                            dl.sU3ScProps             = sl.sU3ScProps;
                            dl.sU3Tc_rep              = sl.sU3Tc_rep;
                            dl.sU3TcCode              = sl.sU3TcCode;
                            dl.sU3TcDesc              = sl.sU3TcDesc;
                            dl.sU3TcProps             = sl.sU3TcProps;
                            dl.sU4Sc_rep              = sl.sU4Sc_rep;
                            dl.sU4ScCode              = sl.sU4ScCode;
                            dl.sU4ScDesc              = sl.sU4ScDesc;
                            dl.sU4ScProps             = sl.sU4ScProps;
                            dl.sU4Tc_rep              = sl.sU4Tc_rep;
                            dl.sU4TcCode              = sl.sU4TcCode;
                            dl.sU4TcDesc              = sl.sU4TcDesc;
                            dl.sU4TcProps             = sl.sU4TcProps;
                            dl.sU5Sc_rep              = sl.sU5Sc_rep;
                            dl.sU5ScCode              = sl.sU5ScCode;
                            dl.sU5ScDesc              = sl.sU5ScDesc;
                            dl.sU5ScProps             = sl.sU5ScProps;
                            dl.sUwF_rep               = sl.sUwF_rep;
                            dl.sUwFProps              = sl.sUwFProps;
                            dl.sVaFfProps             = sl.sVaFfProps;
                            dl.sWireF_rep             = sl.sWireF_rep;
                            dl.sWireFProps            = sl.sWireFProps;
                */
            #endregion

            #region TRUTH-IN-LENDING (Skip, this depends on the loan)
            #endregion

            #region CA MLDS
                /*
                dl.sBrokControlledFundT = sl.sBrokControlledFundT;
                dl.sDisabilityIns_rep = sl.sDisabilityIns_rep;
                dl.sLien1AmtAfter_rep = sl.sLien1AmtAfter_rep;
                dl.sLien1AmtBefore_rep = sl.sLien1AmtBefore_rep;
                dl.sLien1PriorityAfter = sl.sLien1PriorityAfter;
                dl.sLien1PriorityBefore = sl.sLien1PriorityBefore;    
                dl.sLien2AmtAfter_rep = sl.sLien2AmtAfter_rep;
                dl.sLien2AmtBefore_rep = sl.sLien2AmtBefore_rep;
                dl.sLien2PriorityAfter = sl.sLien2PriorityAfter;
                dl.sLien2PriorityBefore = sl.sLien2PriorityBefore;
                dl.sLien3AmtAfter_rep = sl.sLien3AmtAfter_rep;
                dl.sLien3AmtBefore_rep = sl.sLien3AmtBefore_rep;
                dl.sLien3PriorityAfter = sl.sLien3PriorityAfter;
                dl.sLien3PriorityBefore = sl.sLien3PriorityBefore;
                dl.sLienholder1NmAfter = sl.sLienholder1NmAfter;
                dl.sLienholder1NmBefore = sl.sLienholder1NmBefore;
                dl.sLienholder2NmAfter = sl.sLienholder2NmAfter;
                dl.sLienholder2NmBefore = sl.sLienholder2NmBefore;
                dl.sLienholder3NmAfter = sl.sLienholder3NmAfter;
                dl.sLienholder3NmBefore = sl.sLienholder3NmBefore;
                dl.sMldsPpmtBaseT = sl.sMldsPpmtBaseT; 
                dl.sMldsPpmtMonMax_rep = sl.sMldsPpmtMonMax_rep; 
                dl.sMldsPpmtT = sl.sMldsPpmtT; 
                dl.sU1Fntc_rep = sl.sU1Fntc_rep;
                dl.sU1FntcDesc = sl.sU1FntcDesc;
                */
            #endregion

            #region STATUS GENERAL
                da.aBusCrDueD_rep = sa.aBusCrDueD_rep;
                da.aBusCrN = sa.aBusCrN;
                da.aBusCrOd_rep = sa.aBusCrOd_rep;
                da.aBusCrRd_rep = sa.aBusCrRd_rep;
                da.aCrDueD_rep = sa.aCrDueD_rep;
                da.aCrN = sa.aCrN;
                da.aCrOd_rep = sa.aCrOd_rep;
                da.aCrRd_rep = sa.aCrRd_rep;
                da.aU1DocStatDesc = sa.aU1DocStatDesc;
                da.aU1DocStatDueD_rep = sa.aU1DocStatDueD_rep;
                da.aU1DocStatN = sa.aU1DocStatN;
                da.aU1DocStatOd_rep = sa.aU1DocStatOd_rep;
                da.aU1DocStatRd_rep = sa.aU1DocStatRd_rep;
                da.aU2DocStatDesc = sa.aU2DocStatDesc;
                da.aU2DocStatDueD_rep = sa.aU2DocStatDueD_rep;
                da.aU2DocStatN = sa.aU2DocStatN;
                da.aU2DocStatOd_rep = sa.aU2DocStatOd_rep;
                da.aU2DocStatRd_rep = sa.aU2DocStatRd_rep;
                dl.sApprRprtDueD_rep = sl.sApprRprtDueD_rep;
                dl.sApprRprtN = sl.sApprRprtN;
                dl.sApprRprtOd_rep = sl.sApprRprtOd_rep;
                dl.sApprRprtRd_rep = sl.sApprRprtRd_rep;
                dl.sAppSubmittedD_rep = sl.sAppSubmittedD_rep;
                dl.sApprovD_rep = sl.sApprovD_rep;
                dl.sApprovN = sl.sApprovN;
                dl.sCanceledD_rep = sl.sCanceledD_rep;
                dl.sCanceledN = sl.sCanceledN;
                dl.sClosedD_rep = sl.sClosedD_rep;
                dl.sClosedN = sl.sClosedN;
                dl.sDocsD_rep = sl.sDocsD_rep;
                dl.sDocsN = sl.sDocsN;
                dl.sEstCloseN = sl.sEstCloseN;
                dl.sFundD_rep = sl.sFundD_rep;
                dl.sFundN = sl.sFundN;
                dl.sLeadD_rep = sl.sLeadD_rep;
                dl.sLeadN = sl.sLeadN;
                dl.sOnHoldD_rep = sl.sOnHoldD_rep;
                dl.sOnHoldN = sl.sOnHoldN;
                dl.sOpenedD_rep = sl.sOpenedD_rep;
                dl.sOpenedN = sl.sOpenedN;
                dl.sPreApprovD_rep = sl.sPreApprovD_rep;
                dl.sPreApprovN = sl.sPreApprovN;
                dl.sPreQualD_rep = sl.sPreQualD_rep;
                dl.sPreQualN = sl.sPreQualN;
                dl.sPrelimRprtDueD_rep = sl.sPrelimRprtDueD_rep;
                dl.sPrelimRprtN = sl.sPrelimRprtN;
                dl.sPrelimRprtOd_rep = sl.sPrelimRprtOd_rep;
                dl.sPrelimRprtRd_rep = sl.sPrelimRprtRd_rep;
                //dl.sRLckdD_rep = sl.sRLckdD_rep;
                //dl.sRLckdExpiredD_rep = sl.sRLckdExpiredD_rep;
                //dl.sRLckdExpiredN = sl.sRLckdExpiredN;
                dl.sRLckdN = sl.sRLckdN;
                dl.sRecordedD_rep = sl.sRecordedD_rep;
                dl.sRecordedN = sl.sRecordedN;
                dl.sRejectD_rep = sl.sRejectD_rep;
                dl.sRejectN = sl.sRejectN;
                dl.sSubmitD_rep = sl.sSubmitD_rep;
                dl.sSubmitN = sl.sSubmitN;
                dl.sSuspendedD_rep = sl.sSuspendedD_rep;
                dl.sSuspendedN = sl.sSuspendedN;
                //dl.sTilGfeDueD_rep = sl.sTilGfeDueD_rep;
                //dl.sTilGfeN = sl.sTilGfeN;
                //dl.sTilGfeOd_rep = sl.sTilGfeOd_rep;
                //dl.sTilGfeRd_rep = sl.sTilGfeRd_rep;

                //            dl.sTrNotes = sl.sTrNotes;

                dl.sU1DocStatDesc = sl.sU1DocStatDesc;
                dl.sU1DocStatDueD_rep = sl.sU1DocStatDueD_rep;
                dl.sU1DocStatN = sl.sU1DocStatN;
                dl.sU1DocStatOd_rep = sl.sU1DocStatOd_rep;
                dl.sU1DocStatRd_rep = sl.sU1DocStatRd_rep;
                dl.sU1LStatD_rep = sl.sU1LStatD_rep;
                dl.sU1LStatDesc = sl.sU1LStatDesc;
                dl.sU1LStatN = sl.sU1LStatN;
                dl.sU2DocStatDesc = sl.sU2DocStatDesc;
                dl.sU2DocStatDueD_rep = sl.sU2DocStatDueD_rep;
                dl.sU2DocStatN = sl.sU2DocStatN;
                dl.sU2DocStatOd_rep = sl.sU2DocStatOd_rep;
                dl.sU2DocStatRd_rep = sl.sU2DocStatRd_rep;
                dl.sU2LStatD_rep = sl.sU2LStatD_rep;
                dl.sU2LStatDesc = sl.sU2LStatDesc;
                dl.sU2LStatN = sl.sU2LStatN;
                dl.sU3DocStatDesc = sl.sU3DocStatDesc;
                dl.sU3DocStatDueD_rep = sl.sU3DocStatDueD_rep;
                dl.sU3DocStatN = sl.sU3DocStatN;
                dl.sU3DocStatOd_rep = sl.sU3DocStatOd_rep;
                dl.sU3DocStatRd_rep = sl.sU3DocStatRd_rep;
                dl.sU3LStatD_rep = sl.sU3LStatD_rep;
                dl.sU3LStatDesc = sl.sU3LStatDesc;
                dl.sU3LStatN = sl.sU3LStatN;
                dl.sU4DocStatDesc = sl.sU4DocStatDesc;
                dl.sU4DocStatDueD_rep = sl.sU4DocStatDueD_rep;
                dl.sU4DocStatN = sl.sU4DocStatN;
                dl.sU4DocStatOd_rep = sl.sU4DocStatOd_rep;
                dl.sU4DocStatRd_rep = sl.sU4DocStatRd_rep;
                dl.sU4LStatD_rep = sl.sU4LStatD_rep;
                dl.sU4LStatDesc = sl.sU4LStatDesc;
                dl.sU4LStatN = sl.sU4LStatN;
                dl.sU5DocStatDesc = sl.sU5DocStatDesc;
                dl.sU5DocStatDueD_rep = sl.sU5DocStatDueD_rep;
                dl.sU5DocStatN = sl.sU5DocStatN;
                dl.sU5DocStatOd_rep = sl.sU5DocStatOd_rep;
                dl.sU5DocStatRd_rep = sl.sU5DocStatRd_rep;
                dl.sUnderwritingD_rep = sl.sUnderwritingD_rep;
                dl.sUnderwritingN = sl.sUnderwritingN;

            #endregion

            #region HMDA
                dl.sHmdaActionD_rep         = sl.sHmdaActionD_rep;
                dl.sHmdaAprRateSpread       = sl.sHmdaAprRateSpread;
                dl.sHmdaCounterOfferDetails = sl.sHmdaCounterOfferDetails;
                dl.sHmdaCounterOfferMade    = sl.sHmdaCounterOfferMade;
                dl.sHmdaCounterOfferMadeBy  = sl.sHmdaCounterOfferMadeBy;
                dl.sHmdaCounterOfferMadeD   = sl.sHmdaCounterOfferMadeD;
                dl.sHmdaCountyCode          = sl.sHmdaCountyCode;

                if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(sl.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement))
                {
                    dl.sHmdaActionTakenT = sl.sHmdaActionTakenT;
                    dl.sHmdaDenialReasonsList = sl.sHmdaDenialReasonsList;
                }
                else
                {
                    dl.sHmdaActionTaken = sl.sHmdaActionTaken;
                    dl.sHmdaDenialReason1 = sl.sHmdaDenialReason1;
                    dl.sHmdaDenialReason2 = sl.sHmdaDenialReason2;
                    dl.sHmdaDenialReason3 = sl.sHmdaDenialReason3;
                    dl.sHmdaDenialReason4 = sl.sHmdaDenialReason4;
                }

                dl.sHmdaDeniedFormDoneBy    = sl.sHmdaDeniedFormDoneBy;
                dl.sHmdaExcludedFromReport  = sl.sHmdaExcludedFromReport;
                //dl.sHmdaLienT               = sl.sHmdaLienT;
                dl.sHmdaLoanDenied          = sl.sHmdaLoanDenied;
                dl.sHmdaLoanDeniedBy        = sl.sHmdaLoanDeniedBy;
                dl.sHmdaMsaNum              = sl.sHmdaMsaNum;
                dl.sHmdaPreapprovalT        = sl.sHmdaPreapprovalT;
                dl.sHmdaPropT               = sl.sHmdaPropT;
                dl.sHmdaPurchaser           = sl.sHmdaPurchaser;
                dl.sHmdaPurchaser2004T      = sl.sHmdaPurchaser2004T;
                dl.sHmdaReportAsHoepaLoan   = sl.sHmdaReportAsHoepaLoan;
                dl.sHmdaReportAsHomeImprov  = sl.sHmdaReportAsHomeImprov;
                dl.sHmdaStateCode           = sl.sHmdaStateCode;


            #endregion

            #region COMMISSIONS (SKIP, This depends on the loan) 
            #endregion

            #region Transmittal Summary (1008)
                da.aBOIFrom1008                  = sa.aBOIFrom1008;
                da.aCOIFrom1008                  = sa.aCOIFrom1008;
                da.aSpNegCfLckd                  = sa.aSpNegCfLckd;
                da.aOpNegCfLckd                  = sa.aOpNegCfLckd;
                da.aOIFrom1008Desc               = sa.aOIFrom1008Desc;
                da.aOpNegCf                      = sa.aOpNegCf;
                da.aSpNegCf                      = sa.aSpNegCf;
                dl.sApprDriveBy                  = sl.sApprDriveBy;
                dl.sApprFull                     = sl.sApprFull;
                dl.sAusRecommendation            = sl.sAusRecommendation;
                //dl.sBalloonPmt                   = sl.sBalloonPmt;
                //dl.sBiweeklyPmt                  = sl.sBiweeklyPmt;
                //dl.sBuydown                      = sl.sBuydown; 
                dl.sCombinedBorFirstNm           = sl.sCombinedBorFirstNm;
                dl.sCombinedBorInfoLckd          = sl.sCombinedBorInfoLckd;
                dl.sCombinedBorLastNm            = sl.sCombinedBorLastNm;
                dl.sCombinedBorMidNm             = sl.sCombinedBorMidNm;
                dl.sCombinedBorSsn               = sl.sCombinedBorSsn;
                dl.sCombinedBorSuffix            = sl.sCombinedBorSuffix;
                dl.sCombinedCoborFirstNm         = sl.sCombinedCoborFirstNm;
                dl.sCombinedCoborLastNm          = sl.sCombinedCoborLastNm;
                dl.sCombinedCoborMidNm           = sl.sCombinedCoborMidNm;
                dl.sCombinedCoborSsn             = sl.sCombinedCoborSsn;
                dl.sCombinedCoborSuffix          = sl.sCombinedCoborSuffix;
                dl.sCombinedCoborUsing2ndAppBorr = sl.sCombinedCoborUsing2ndAppBorr;
                dl.sCommitNum                    = sl.sCommitNum;
                dl.sContractNum                  = sl.sContractNum;
                dl.sDebtToHousingGapR_rep        = sl.sDebtToHousingGapR_rep;
                dl.sDebtToHousingGapRLckd        = sl.sDebtToHousingGapRLckd;
                //dl.sDuCaseId                     = sl.sDuCaseId;
                //dl.sFinMethDesc                  = sl.sFinMethDesc;
                //dl.sFinMethT                     = sl.sFinMethT;
                //dl.sFntcSrc                      = sl.sFntcSrc;
                dl.sGseRefPurposeT               = sl.sGseRefPurposeT;
                dl.sGseSpT                       = sl.sGseSpT;
                dl.sHcltvR_rep                   = sl.sHcltvR_rep;
                dl.sHcltvRLckd                   = sl.sHcltvRLckd;
                dl.sInterestedPartyContribR_rep  = sl.sInterestedPartyContribR_rep;
                //dl.sInvestLNum                   = sl.sInvestLNum;
                dl.sIsCommLen                    = sl.sIsCommLen;
                //dl.sIsDuUw                       = sl.sIsDuUw;
                dl.sIsHOwnershipEdCertInFile     = sl.sIsHOwnershipEdCertInFile;
                //dl.sIsLpUw                       = sl.sIsLpUw;
                dl.sIsMOrigBroker                = sl.sIsMOrigBroker;
                dl.sIsMOrigCorrespondent         = sl.sIsMOrigCorrespondent;
                dl.sIsManualUw                   = sl.sIsManualUw;
                dl.sIsOtherUw                    = sl.sIsOtherUw;
                dl.sIsSpReviewNoAppr             = sl.sIsSpReviewNoAppr;
                dl.sLenAddr                      = sl.sLenAddr;
                dl.sLenCity                      = sl.sLenCity;
                dl.sLenContactNm                 = sl.sLenContactNm;
                dl.sLenContactPhone              = sl.sLenContactPhone;
                dl.sLenContactTitle              = sl.sLenContactTitle;
                dl.sLenContractD_rep             = sl.sLenContractD_rep;
//                dl.sLenLNum                      = sl.sLenLNum; // OPM 7854
                dl.sLenNm                        = sl.sLenNm;
                dl.sLenNum                       = sl.sLenNum;
                dl.sLenState                     = sl.sLenState;
                dl.sLenZip                       = sl.sLenZip;
                //dl.sLpAusKey                     = sl.sLpAusKey;
                dl.sLpDocClass                   = sl.sLpDocClass;
                dl.sMOrigT                       = sl.sMOrigT;
                dl.sOtherUwDesc                  = sl.sOtherUwDesc;
                dl.sProjNm                       = sl.sProjNm;
                dl.sQualIRDeriveT                = sl.sQualIRDeriveT;
                dl.sRepCrScore                   = sl.sRepCrScore;
                dl.sRsrvMonNumDesc               = sl.sRsrvMonNumDesc;
                dl.sSpReviewFormNum              = sl.sSpReviewFormNum;
                //dl.sTransmBuydwnTermDesc         = sl.sTransmBuydwnTermDesc;
                //dl.sTransmFntc_rep               = sl.sTransmFntc_rep;
                //dl.sTransmFntcLckd               = sl.sTransmFntcLckd;
                dl.sTransmUwerComments           = sl.sTransmUwerComments;
                dl.sVerifAssetAmt_rep            = sl.sVerifAssetAmt_rep;
                dl.sWillEscrowBeWaived           = sl.sWillEscrowBeWaived;
            #endregion

            #region LOAN SUBMISSIONS (Skip Loan Demand Section.)
                dl.sHmdaCensusTract        = sl.sHmdaCensusTract;
                //dl.sSubmitProgramCode      = sl.sSubmitProgramCode;
                dl.sSubmitPropTDesc        = sl.sSubmitPropTDesc;
                dl.sSubmitDocFull          = sl.sSubmitDocFull;
                dl.sSubmitDocOther         = sl.sSubmitDocOther;
                //dl.sSubmitImpoundTaxes     = sl.sSubmitImpoundTaxes;
                //dl.sSubmitImpoundHazard    = sl.sSubmitImpoundHazard;
                //dl.sSubmitImpoundMI        = sl.sSubmitImpoundMI;
                //dl.sSubmitImpoundFlood     = sl.sSubmitImpoundFlood;
                //dl.sSubmitImpoundOther     = sl.sSubmitImpoundOther;
                //dl.sSubmitImpoundOtherDesc = sl.sSubmitImpoundOtherDesc;
                //dl.sSubmitBuydownDesc      = sl.sSubmitBuydownDesc;
            #endregion

            #region MORTGAGE LOAN COMMITMENT
                //dl.sCommitExpD_rep           = sl.sCommitExpD_rep;
                //dl.sCommitRepayTermsDesc     = sl.sCommitRepayTermsDesc;
                dl.sCommitTitleEvidence      = sl.sCommitTitleEvidence;
                dl.sCommitReturnToAboveAddr  = sl.sCommitReturnToAboveAddr;
                dl.sCommitReturnToFollowAddr = sl.sCommitReturnToFollowAddr;
                dl.sCommitReturnWithinDays   = sl.sCommitReturnWithinDays;
            #endregion

            #region CREDIT DENIAL
                da.aDenialAdditionalStatement                       = sa.aDenialAdditionalStatement;
                da.aDenialBankruptcy                                = sa.aDenialBankruptcy;
                da.aDenialByFedHomeLoanMortCorp                     = sa.aDenialByFedHomeLoanMortCorp;
                da.aDenialByFedNationalMortAssoc                    = sa.aDenialByFedNationalMortAssoc;
                da.aDenialByHUD                                     = sa.aDenialByHUD;
                da.aDenialByOther                                   = sa.aDenialByOther;
                da.aDenialByOtherDesc                               = sa.aDenialByOtherDesc;
                da.aDenialByVA                                      = sa.aDenialByVA;
                da.aDenialCreditAppIncomplete                       = sa.aDenialCreditAppIncomplete;
                da.aDenialDecisionBasedOnCRA                        = sa.aDenialDecisionBasedOnCRA;
                da.aDenialDecisionBasedOnReportAgency               = sa.aDenialDecisionBasedOnReportAgency;
                da.aDenialDeliquentCreditObligations                = sa.aDenialDeliquentCreditObligations;
                da.aDenialExcessiveObligations                      = sa.aDenialExcessiveObligations;
                da.aDenialGarnishment                               = sa.aDenialGarnishment;
                da.aDenialHasAdditionalStatement                    = sa.aDenialHasAdditionalStatement;
                da.aDenialInadequateCollateral                      = sa.aDenialInadequateCollateral;
                da.aDenialInfoFromConsumerReportAgency              = sa.aDenialInfoFromConsumerReportAgency;
                da.aDenialInsufficientCreditFile                    = sa.aDenialInsufficientCreditFile;
                da.aDenialInsufficientCreditRef                     = sa.aDenialInsufficientCreditRef;
                da.aDenialInsufficientFundsToClose                  = sa.aDenialInsufficientFundsToClose;
                da.aDenialInsufficientIncome                        = sa.aDenialInsufficientIncome;
                da.aDenialInsufficientIncomeForMortgagePmt          = sa.aDenialInsufficientIncomeForMortgagePmt;
                da.aDenialInsufficientPropData                      = sa.aDenialInsufficientPropData;
                da.aDenialLackOfCashReserves                        = sa.aDenialLackOfCashReserves;
                da.aDenialLenOfEmployment                           = sa.aDenialLenOfEmployment;
                da.aDenialNoCreditFile                              = sa.aDenialNoCreditFile;
                da.aDenialNotGrantCreditToAnyAppOnSuchTermsAndConds = sa.aDenialNotGrantCreditToAnyAppOnSuchTermsAndConds;
                da.aDenialOtherReason1                              = sa.aDenialOtherReason1;
                da.aDenialOtherReason1Desc                          = sa.aDenialOtherReason1Desc;
                da.aDenialOtherReason2                              = sa.aDenialOtherReason2;
                da.aDenialOtherReason2Desc                          = sa.aDenialOtherReason2Desc;
                da.aDenialShortResidencePeriod                      = sa.aDenialShortResidencePeriod;
                da.aDenialTempResidence                             = sa.aDenialTempResidence;
                da.aDenialTemporaryEmployment                       = sa.aDenialTemporaryEmployment;
                da.aDenialUnableVerifyCreditRef                     = sa.aDenialUnableVerifyCreditRef;
                da.aDenialUnableVerifyEmployment                    = sa.aDenialUnableVerifyEmployment;
                da.aDenialUnableVerifyIncome                        = sa.aDenialUnableVerifyIncome;
                da.aDenialUnableVerifyResidence                     = sa.aDenialUnableVerifyResidence;
                da.aDenialUnacceptableAppraisal                     = sa.aDenialUnacceptableAppraisal;
                da.aDenialUnacceptableLeasehold                     = sa.aDenialUnacceptableLeasehold;
                da.aDenialUnacceptablePmtRecord                     = sa.aDenialUnacceptablePmtRecord;
                da.aDenialUnacceptableProp                          = sa.aDenialUnacceptableProp;
                da.aDenialWithdrawnByApp                            = sa.aDenialWithdrawnByApp;
                dl.sHmdaDeniedFormDoneD_rep                         = sl.sHmdaDeniedFormDoneD_rep;

            #endregion

            #region SERVICING DISCLOSURE
                dl.sAbleToServiceAndDecisionT                 = sl.sAbleToServiceAndDecisionT;
                dl.sAppliedProgramTransfered                  = sl.sAppliedProgramTransfered;
                dl.sAppliedProgramTransferedPercent_rep       = sl.sAppliedProgramTransferedPercent_rep;
                dl.sAppliedProgramTransferedT                 = sl.sAppliedProgramTransferedT;
                dl.sDontService                               = sl.sDontService;
                dl.sDontServiceInPast3Yrs                     = sl.sDontServiceInPast3Yrs;
                dl.sServiceDontIntendToTransfer               = sl.sServiceDontIntendToTransfer;
                dl.sFirstLienLoanDeclareServiceRecord         = sl.sFirstLienLoanDeclareServiceRecord;
                dl.sFirstLienLoanDeclareServiceRecordIncludeT = sl.sFirstLienLoanDeclareServiceRecordIncludeT;
                dl.sFirstLienLoanPreviouslyTransfered         = sl.sFirstLienLoanPreviouslyTransfered;
                dl.sFirstLienLoanServiceEstimateT             = sl.sFirstLienLoanServiceEstimateT;
                dl.sFirstLienLoanTransferRecordYear1          = sl.sFirstLienLoanTransferRecordYear1;
                dl.sFirstLienLoanTransferRecordYear1Percent   = sl.sFirstLienLoanTransferRecordYear1Percent;
                dl.sFirstLienLoanTransferRecordYear2          = sl.sFirstLienLoanTransferRecordYear2;
                dl.sFirstLienLoanTransferRecordYear2Percent   = sl.sFirstLienLoanTransferRecordYear2Percent;
                dl.sFirstLienLoanTransferRecordYear3          = sl.sFirstLienLoanTransferRecordYear3;
                dl.sFirstLienLoanTransferRecordYear3Percent   = sl.sFirstLienLoanTransferRecordYear3Percent;
                dl.sMayAssignSellTransfer                     = sl.sMayAssignSellTransfer;
                dl.sTransferPercentFirstLienLoanT             = sl.sTransferPercentFirstLienLoanT;
            #endregion

            #region TX MORTGAGE BROKER DISCLOSURE
                dl.Set_sTexasDiscAppFInc_sTexasDiscAppFAmt(sl.sTexasDiscAppFInc, sl.sTexasDiscAppFAmt_rep);
                //dl.sTexasDiscAppFAmt_rep              = sl.sTexasDiscAppFAmt_rep;
                dl.Set_sTexasDiscApprFInc_sTexasDiscApprF(sl.sTexasDiscApprFInc, sl.sTexasDiscApprF_rep);
                //dl.sTexasDiscAppFInc                  = sl.sTexasDiscAppFInc;
                //dl.sTexasDiscApprFInc                 = sl.sTexasDiscApprFInc;
                dl.sTexasDiscAsIndependentContractor  = sl.sTexasDiscAsIndependentContractor;
                //dl.sTexasDiscAutoUnderwritingFAmt_rep = sl.sTexasDiscAutoUnderwritingFAmt_rep;
                //dl.sTexasDiscAutoUnderwritingFInc     = sl.sTexasDiscAutoUnderwritingFInc;
                dl.Set_sTexasDiscAutoUnderwritingFInc_sTexasDiscAutoUnderwritingFAmt(sl.sTexasDiscAutoUnderwritingFInc, sl.sTexasDiscAutoUnderwritingFAmt_rep);
                dl.sTexasDiscChargeVaried             = sl.sTexasDiscChargeVaried;
                dl.sTexasDiscCompensationIncluded     = sl.sTexasDiscCompensationIncluded;
                //dl.sTexasDiscCrFInc                   = sl.sTexasDiscCrFInc;
                dl.Set_sTexasDiscCrFInc_sTexasDiscCrF(sl.sTexasDiscCrFInc, sl.sTexasDiscCrF_rep);
                dl.sTexasDiscNonRefundAmt_rep         = sl.sTexasDiscNonRefundAmt_rep;
                dl.sTexasDiscOF1IncAmt_rep            = sl.sTexasDiscOF1IncAmt_rep;
                dl.sTexasDiscOF1IncDesc               = sl.sTexasDiscOF1IncDesc;
                dl.sTexasDiscOF2IncAmt_rep            = sl.sTexasDiscOF2IncAmt_rep;
                dl.sTexasDiscOF2IncDesc               = sl.sTexasDiscOF2IncDesc;
                //dl.sTexasDiscProcFInc                 = sl.sTexasDiscProcFInc;
                dl.Set_sTexasDiscProcFInc_sTexasDiscProcF(sl.sTexasDiscProcFInc, sl.sTexasDiscProcF_rep);
                dl.sTexasDiscReceivedF_rep            = sl.sTexasDiscReceivedF_rep;
                dl.sTexasDiscWillActAsFollows         = sl.sTexasDiscWillActAsFollows;
                dl.sTexasDiscWillActAsFollowsDesc     = sl.sTexasDiscWillActAsFollowsDesc;
                dl.sTexasDiscWillSubmitToLender       = sl.sTexasDiscWillSubmitToLender;
            #endregion

            #region FHA/VA ADDENDUM
                da.aFHABorrCertInformedPropValAwareAtContractSigning    = sa.aFHABorrCertInformedPropValAwareAtContractSigning;
                da.aFHABorrCertInformedPropValDeterminedByHUD           = sa.aFHABorrCertInformedPropValDeterminedByHUD;
                da.aFHABorrCertInformedPropValDeterminedByVA            = sa.aFHABorrCertInformedPropValDeterminedByVA;
                da.aFHABorrCertInformedPropValNotAwareAtContractSigning = sa.aFHABorrCertInformedPropValNotAwareAtContractSigning;
                da.aFHABorrCertInformedPropVal_rep                      = sa.aFHABorrCertInformedPropVal_rep;
                da.aFHABorrCertOccIsAsHome                              = sa.aFHABorrCertOccIsAsHome;
                da.aFHABorrCertOccIsAsHomeForActiveSpouse               = sa.aFHABorrCertOccIsAsHomeForActiveSpouse;
                da.aFHABorrCertOccIsAsHomePrev                          = sa.aFHABorrCertOccIsAsHomePrev;
                da.aFHABorrCertOccIsAsHomePrevForActiveSpouse           = sa.aFHABorrCertOccIsAsHomePrevForActiveSpouse;
                da.aFHABorrCertOtherPropCity                            = sa.aFHABorrCertOtherPropCity;
                da.aFHABorrCertOtherPropCoveredByThisLoanTri            = sa.aFHABorrCertOtherPropCoveredByThisLoanTri;
                da.aFHABorrCertOtherPropOrigMAmt_rep                    = sa.aFHABorrCertOtherPropOrigMAmt_rep;
                da.aFHABorrCertOtherPropSalesPrice_rep                  = sa.aFHABorrCertOtherPropSalesPrice_rep;
                da.aFHABorrCertOtherPropStAddr                          = sa.aFHABorrCertOtherPropStAddr;
                da.aFHABorrCertOtherPropState                           = sa.aFHABorrCertOtherPropState;
                da.aFHABorrCertOtherPropToBeSoldTri                     = sa.aFHABorrCertOtherPropToBeSoldTri;
                da.aFHABorrCertOtherPropZip                             = sa.aFHABorrCertOtherPropZip;
                da.aFHABorrCertOwnMoreThan4DwellingsTri                 = sa.aFHABorrCertOwnMoreThan4DwellingsTri;
                da.aFHABorrCertOwnOrSoldOtherFHAPropTri                 = sa.aFHABorrCertOwnOrSoldOtherFHAPropTri;
                da.aFHABorrCertReceivedLeadPaintPoisonInfoTri           = sa.aFHABorrCertReceivedLeadPaintPoisonInfoTri;
                da.aHas1stTimeBuyerTri                                  = sa.aHas1stTimeBuyerTri;
                da.aVaBorrCertHadVaLoanTri                              = sa.aVaBorrCertHadVaLoanTri;
                da.aVaVestTitleODesc                                    = sa.aVaVestTitleODesc;
                da.aVaVestTitleT                                        = sa.aVaVestTitleT;
                dl.sFHAHousingActSection                                = sl.sFHAHousingActSection;
                //dl.sFHALenderIdCode                                     = sl.sFHALenderIdCode;
                dl.sFHAPurposeIsConstructHome                           = sl.sFHAPurposeIsConstructHome;
                dl.sFHAPurposeIsFinanceCoopPurchase                     = sl.sFHAPurposeIsFinanceCoopPurchase;
                dl.sFHAPurposeIsFinanceImprovement                      = sl.sFHAPurposeIsFinanceImprovement;
                dl.sFHAPurposeIsManufacturedHomeAndLot                  = sl.sFHAPurposeIsManufacturedHomeAndLot;
                dl.sFHAPurposeIsPurchaseExistCondo                      = sl.sFHAPurposeIsPurchaseExistCondo;
                dl.sFHAPurposeIsPurchaseExistHome                       = sl.sFHAPurposeIsPurchaseExistHome;
                dl.sFHAPurposeIsPurchaseManufacturedHome                = sl.sFHAPurposeIsPurchaseManufacturedHome;
                dl.sFHAPurposeIsPurchaseNewCondo                        = sl.sFHAPurposeIsPurchaseNewCondo;
                dl.sFHAPurposeIsPurchaseNewHome                         = sl.sFHAPurposeIsPurchaseNewHome;
                dl.sFHAPurposeIsRefiManufacturedHomeOrLotLoan           = sl.sFHAPurposeIsRefiManufacturedHomeOrLotLoan;
                dl.sFHAPurposeIsRefiManufacturedHomeToBuyLot            = sl.sFHAPurposeIsRefiManufacturedHomeToBuyLot;
                dl.sFHAPurposeIsRefinance                               = sl.sFHAPurposeIsRefinance;
                dl.sFHASponsorAgentIdCode                               = sl.sFHASponsorAgentIdCode;

            #endregion

            #region FHA MCAW - PURCHASE
                da.aFHAAssetAvail_rep         = sa.aFHAAssetAvail_rep;
                da.aFHABBaseI_rep             = sa.aFHABBaseI_rep;
                da.aFHABCaivrsNum             = sa.aFHABCaivrsNum;
                da.aFHABLpdGsa                = sa.aFHABLpdGsa;
                da.aFHABOI_rep                = sa.aFHABOI_rep;
                da.aFHACBaseI_rep             = sa.aFHACBaseI_rep;
                da.aFHACLpdGsa                = sa.aFHACLpdGsa;
                da.aFHACOI_rep                = sa.aFHACOI_rep;

                if (!LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(sl.sLoanVersionT, LoanVersionT.V25_ConsolidateMonthlyChildSupportPayments))
                {
                    // Migrated loans will be updated when copying the liability XML content.
                    da.aFHAChildSupportPmt_rep = sa.aFHAChildSupportPmt_rep;
                }

                da.aFHACreditRating           = sa.aFHACreditRating;
                da.aFHADebtInstallBal_rep     = sa.aFHADebtInstallBal_rep;
                da.aFHADebtInstallPmt_rep     = sa.aFHADebtInstallPmt_rep;
                da.aFHAGiftFundAmt_rep        = sa.aFHAGiftFundAmt_rep;
                da.aFHAGiftFundSrc            = sa.aFHAGiftFundSrc;
                da.aFHANetRentalI_rep         = sa.aFHANetRentalI_rep;
                da.aFHAOtherDebtBal_rep       = sa.aFHAOtherDebtBal_rep;
                da.aFHAOtherDebtPmt_rep       = sa.aFHAOtherDebtPmt_rep;
                da.aFHARatingAssetAdequacy    = sa.aFHARatingAssetAdequacy;
                da.aFHARatingIAdequacy        = sa.aFHARatingIAdequacy;
                da.aFHARatingIStability       = sa.aFHARatingIStability;
                //dl.sFHA2ndMAmt_rep            = sl.sFHA2ndMAmt_rep;
                //dl.sFHA2ndMSrc                = sl.sFHA2ndMSrc;
                dl.sFHAAmtPaid_rep            = sl.sFHAAmtPaid_rep;
                dl.sFHAAmtPaidByCash          = sl.sFHAAmtPaidByCash;
                dl.sFHAAmtPaidByOtherT        = sl.sFHAAmtPaidByOtherT;
                dl.sFHACcPbs_rep              = sl.sFHACcPbs_rep;
                dl.sFHACcTot_rep              = sl.sFHACcTot_rep;
                dl.sFHAConstructionT          = sl.sFHAConstructionT;
                dl.sFHACreditAnalysisRemarks  = sl.sFHACreditAnalysisRemarks;
                dl.sFHADiscountPoints_rep     = sl.sFHADiscountPoints_rep;
                dl.sFHAEnergyEffImprov_rep    = sl.sFHAEnergyEffImprov_rep;
                dl.sFHAExcessContribution_rep = sl.sFHAExcessContribution_rep;
                dl.sFHAImprovements_rep       = sl.sFHAImprovements_rep;
                dl.sFHALtv_rep                = sl.sFHALtv_rep;
                dl.sFHALtvLckd                = sl.sFHALtvLckd;
                dl.sFHANonrealty_rep          = sl.sFHANonrealty_rep;
                dl.sFHANonrealtyLckd          = sl.sFHANonrealtyLckd;
                dl.sFHAPrepaidExp_rep         = sl.sFHAPrepaidExp_rep;
                dl.sFHAPro1stMPmt_rep         = sl.sFHAPro1stMPmt_rep;
                dl.sFHAPro2ndFinPmt_rep       = sl.sFHAPro2ndFinPmt_rep; 
                dl.sFHAProGroundRent_rep      = sl.sFHAProGroundRent_rep;
                dl.sFHAProHazIns_rep          = sl.sFHAProHazIns_rep;
                dl.sFHAProHoAssocDues_rep     = sl.sFHAProHoAssocDues_rep;
                dl.sFHAProMIns_rep            = sl.sFHAProMIns_rep;
                dl.sFHAProRealETx_rep         = sl.sFHAProRealETx_rep;
                dl.sFHAPurchPriceSrcT         = sl.sFHAPurchPriceSrcT;
                dl.sFHAReqAdj_rep             = sl.sFHAReqAdj_rep;
                dl.sFHASellerContribution_rep = sl.sFHASellerContribution_rep;
                //dl.sFfUfmipR_rep              = sl.sFfUfmipR_rep; 
                //dl.sMipPiaMon_rep             = sl.sMipPiaMon_rep;
                //dl.sUfCashPd_rep              = sl.sUfCashPd_rep;

            #endregion

            #region FHA MCAW - REFI
                dl.sFHAApprValMultiply_rep     = sl.sFHAApprValMultiply_rep;
                dl.sFHAApprValMultiplyLckd     = sl.sFHAApprValMultiplyLckd;
                dl.sFHAExistingMLien_rep       = sl.sFHAExistingMLien_rep;
                dl.sFHAGiftAmtTot_rep          = sl.sFHAGiftAmtTot_rep;
                dl.sFHAImprovementsDesc        = sl.sFHAImprovementsDesc;
                dl.sFHAIsAmtPdInCash           = sl.sFHAIsAmtPdInCash;
                dl.sFHAIsAmtPdInOther          = sl.sFHAIsAmtPdInOther;
                dl.sFHAIsAmtToBePdInCash       = sl.sFHAIsAmtToBePdInCash;
                dl.sFHAIsAmtToBePdInOther      = sl.sFHAIsAmtToBePdInOther;
                dl.sFHAMBasisRefinMultiply_rep = sl.sFHAMBasisRefinMultiply_rep;
                dl.sFHAMBasisRefinMultiplyLckd = sl.sFHAMBasisRefinMultiplyLckd;
                dl.sFHARefinanceTypeDesc       = sl.sFHARefinanceTypeDesc;
                dl.sFHASalesConcessions_rep    = sl.sFHASalesConcessions_rep;

            #endregion

            #region FHA 203(k) WORKSHEET
                dl.sFHA203kActualCashInvRequired_rep   = sl.sFHA203kActualCashInvRequired_rep;
                dl.sFHA203kActualCashInvRequiredLckd   = sl.sFHA203kActualCashInvRequiredLckd;
                dl.sFHA203kAllowEnergyImprovement_rep  = sl.sFHA203kAllowEnergyImprovement_rep;
                dl.sFHA203kBorPdCc_rep                 = sl.sFHA203kBorPdCc_rep;
                dl.sFHA203kBorrAcknowledgeDesc         = sl.sFHA203kBorrAcknowledgeDesc;
                dl.sFHA203kBorrAcknowledgeT            = sl.sFHA203kBorrAcknowledgeT;
                dl.sFHA203kConsultantFee_rep           = sl.sFHA203kConsultantFee_rep;
                dl.sFHA203kCostPerInspect_rep          = sl.sFHA203kCostPerInspect_rep;
                dl.sFHA203kCostPerTitleDraw_rep        = sl.sFHA203kCostPerTitleDraw_rep;
                dl.sFHA203kD1_rep                      = sl.sFHA203kD1_rep;
                dl.sFHA203kD1Lckd                      = sl.sFHA203kD1Lckd;
                dl.sFHA203kE1_rep                      = sl.sFHA203kE1_rep;
                dl.sFHA203kE1Lckd                      = sl.sFHA203kE1Lckd;
                dl.sFHA203kEngineeringFee_rep          = sl.sFHA203kEngineeringFee_rep;
                dl.sFHA203kEscrowedFundsTot_rep        = sl.sFHA203kEscrowedFundsTot_rep;
                dl.sFHA203kFHAMipRefund_rep            = sl.sFHA203kFHAMipRefund_rep;
                dl.sFHA203kInspectCount_rep            = sl.sFHA203kInspectCount_rep;
                dl.sFHA203kMaxMAmtC5_rep               = sl.sFHA203kMaxMAmtC5_rep;
                dl.sFHA203kMaxMAmtC5Lckd               = sl.sFHA203kMaxMAmtC5Lckd;
                dl.sFHA203kMaxMAmtD5_rep               = sl.sFHA203kMaxMAmtD5_rep;
                dl.sFHA203kMaxMAmtD5Lckd               = sl.sFHA203kMaxMAmtD5Lckd;
                dl.sFHA203kMonhtlyPmtEscrowed_rep      = sl.sFHA203kMonhtlyPmtEscrowed_rep;
                dl.sFHA203kOtherFee_rep                = sl.sFHA203kOtherFee_rep;
                dl.sFHA203kPlanReviewerCostPerMile_rep = sl.sFHA203kPlanReviewerCostPerMile_rep;
                dl.sFHA203kPlanReviewerMiles_rep       = sl.sFHA203kPlanReviewerMiles_rep;
                dl.sFHA203kPmtEscrowedMonths_rep       = sl.sFHA203kPmtEscrowedMonths_rep;
                dl.sFHA203kRemarks                     = sl.sFHA203kRemarks;
                dl.sFHA203kRepairCost_rep              = sl.sFHA203kRepairCost_rep;
                dl.sFHA203kRepairCostReserveR_rep      = sl.sFHA203kRepairCostReserveR_rep;
                dl.sFHA203kRepairDiscountFeePt_rep     = sl.sFHA203kRepairDiscountFeePt_rep;
                dl.sFHA203kSpHudOwnedTri               = sl.sFHA203kSpHudOwnedTri;
                dl.sFHA203kTitleDrawCount_rep          = sl.sFHA203kTitleDrawCount_rep;
                dl.sFHACommitStageT                    = sl.sFHACommitStageT;
                dl.sFHAIsEscrowCommitment              = sl.sFHAIsEscrowCommitment;
                dl.sFHAIsExistingDebt                  = sl.sFHAIsExistingDebt;
                dl.sFHAIsGovAgency                     = sl.sFHAIsGovAgency;
                dl.sFHAIsNonProfit                     = sl.sFHAIsNonProfit;
                dl.sFHASpAfterImprovedVal_rep          = sl.sFHASpAfterImprovedVal_rep;
                dl.sFHASpAsIsVal_rep                   = sl.sFHASpAsIsVal_rep;
            #endregion

            #region FHA DE Analysis of Appraisal 
                dl.sFHAApprAdjAcceptableTri = sl.sFHAApprAdjAcceptableTri;
                dl.sFHAApprAdjNotAcceptableExplanation = sl.sFHAApprAdjNotAcceptableExplanation;
                dl.sFHAApprAreComparablesAcceptableTri = sl.sFHAApprAreComparablesAcceptableTri;
                dl.sFHAApprComparablesComment = sl.sFHAApprComparablesComment;
                dl.sFHAApprCorrectedVal_rep = sl.sFHAApprCorrectedVal_rep;
                dl.sFHAApprIsFairTri = sl.sFHAApprIsFairTri;
                dl.sFHAApprIsValAcceptableTri = sl.sFHAApprIsValAcceptableTri;
                dl.sFHAApprNotFairExplanation = sl.sFHAApprNotFairExplanation;
                dl.sFHAApprOtherComments = sl.sFHAApprOtherComments;
                dl.sFHAApprQualityComment = sl.sFHAApprQualityComment;
                dl.sFHAApprRepairConditions = sl.sFHAApprRepairConditions;
                dl.sFHAApprValCorrectionJustification = sl.sFHAApprValCorrectionJustification;
                dl.sFHAApprValNeedCorrectionTri = sl.sFHAApprValNeedCorrectionTri;

            #endregion

            #region FHA STATEMENT OF APPRAISED VALUE
                dl.sFHACondCommAssuranceOfCompletionAmt_rep = sl.sFHACondCommAssuranceOfCompletionAmt_rep;
                dl.sFHACondCommCondoComExp                  = sl.sFHACondCommCondoComExp;
                dl.sFHACondCommCondsOnBack1                 = sl.sFHACondCommCondsOnBack1;
                dl.sFHACondCommCondsOnBack2                 = sl.sFHACondCommCondsOnBack2;
                dl.sFHACondCommCondsOnBack3                 = sl.sFHACondCommCondsOnBack3;
                dl.sFHACondCommCondsOnBack4                 = sl.sFHACondCommCondsOnBack4;
                dl.sFHACondCommCondsOnBack5                 = sl.sFHACondCommCondsOnBack5;
                dl.sFHACondCommCondsOnBack6                 = sl.sFHACondCommCondsOnBack6;
                dl.sFHACondCommCondsOnBack7                 = sl.sFHACondCommCondsOnBack7;
                dl.sFHACondCommExpiredD_rep                 = sl.sFHACondCommExpiredD_rep;
                dl.sFHACondCommImprovedLivingArea           = sl.sFHACondCommImprovedLivingArea;
                dl.sFHACondCommInstCaseRef                  = sl.sFHACondCommInstCaseRef;
                dl.sFHACondCommIsAssuranceOfCompletion      = sl.sFHACondCommIsAssuranceOfCompletion;
                dl.sFHACondCommIsManufacturedHousing        = sl.sFHACondCommIsManufacturedHousing;
                dl.sFHACondCommIsSection221d                = sl.sFHACondCommIsSection221d;
                dl.sFHACondCommIssuedD_rep                  = sl.sFHACondCommIssuedD_rep;
                dl.sFHACondCommNationalHousingActSection    = sl.sFHACondCommNationalHousingActSection;
                dl.sFHACondCommRemainEconLife               = sl.sFHACondCommRemainEconLife;
                dl.sFHACondCommSection221dAmt_rep           = sl.sFHACondCommSection221dAmt_rep;
                dl.sFHACondCommSeeAttached                  = sl.sFHACondCommSeeAttached;
                dl.sFHACondCommSeeAttachedDesc              = sl.sFHACondCommSeeAttachedDesc;
                dl.sFHACondCommSeeBelow                     = sl.sFHACondCommSeeBelow;
                dl.sFHACondCommSeeFollowingCondsOnBack      = sl.sFHACondCommSeeFollowingCondsOnBack;
                dl.sFHACondCommUnderNationalHousingAct      = sl.sFHACondCommUnderNationalHousingAct;
                //dl.sFHALenderIdCode                         = sl.sFHALenderIdCode;
                //dl.sFHASponsorAgentIdCode                   = sl.sFHASponsorAgentIdCode;

            #endregion

            #region FHA PROPERTY IMPROVEMENT
                dl.sFHAPropImprovBorrBankInfo                      = sl.sFHAPropImprovBorrBankInfo;
                dl.sFHAPropImprovBorrHasChecking                   = sl.sFHAPropImprovBorrHasChecking;
                dl.sFHAPropImprovBorrHasNoBankAcount               = sl.sFHAPropImprovBorrHasNoBankAcount;
                dl.sFHAPropImprovBorrHasSaving                     = sl.sFHAPropImprovBorrHasSaving;
                dl.sFHAPropImprovBorrRelativeAddr                  = sl.sFHAPropImprovBorrRelativeAddr;
                dl.sFHAPropImprovBorrRelativeNm                    = sl.sFHAPropImprovBorrRelativeNm;
                dl.sFHAPropImprovBorrRelativePhone                 = sl.sFHAPropImprovBorrRelativePhone;
                dl.sFHAPropImprovBorrRelativeRelationship          = sl.sFHAPropImprovBorrRelativeRelationship;
                dl.sFHAPropImprovCoborBankInfo                     = sl.sFHAPropImprovCoborBankInfo;
                dl.sFHAPropImprovCoborHasChecking                  = sl.sFHAPropImprovCoborHasChecking;
                dl.sFHAPropImprovCoborHasNoBankAcount              = sl.sFHAPropImprovCoborHasNoBankAcount;
                dl.sFHAPropImprovCoborHasSaving                    = sl.sFHAPropImprovCoborHasSaving;
                dl.sFHAPropImprovCoborRelativeAddr                 = sl.sFHAPropImprovCoborRelativeAddr;
                dl.sFHAPropImprovCoborRelativeNm                   = sl.sFHAPropImprovCoborRelativeNm;
                dl.sFHAPropImprovCoborRelativePhone                = sl.sFHAPropImprovCoborRelativePhone;
                dl.sFHAPropImprovCoborRelativeRelationship         = sl.sFHAPropImprovCoborRelativeRelationship;
                dl.sFHAPropImprovDealerContractorContactInfo       = sl.sFHAPropImprovDealerContractorContactInfo;
                dl.sFHAPropImprovHasFHAPendingAppTri               = sl.sFHAPropImprovHasFHAPendingAppTri;
                dl.sFHAPropImprovHasFHAPendingAppWithWhom          = sl.sFHAPropImprovHasFHAPendingAppWithWhom;
                dl.sFHAPropImprovHasFedPastDueTri                  = sl.sFHAPropImprovHasFedPastDueTri;
                dl.sFHAPropImprovIsPropBeingPurchasedOnContract    = sl.sFHAPropImprovIsPropBeingPurchasedOnContract;
                dl.sFHAPropImprovIsPropHealthCareFacility          = sl.sFHAPropImprovIsPropHealthCareFacility;
                dl.sFHAPropImprovIsPropHistoricResidential         = sl.sFHAPropImprovIsPropHistoricResidential;
                dl.sFHAPropImprovIsPropHistoricResidentialUnitsNum = sl.sFHAPropImprovIsPropHistoricResidentialUnitsNum;
                dl.sFHAPropImprovIsPropLeasedFromSomeone           = sl.sFHAPropImprovIsPropLeasedFromSomeone;
                dl.sFHAPropImprovIsPropManufacturedHome            = sl.sFHAPropImprovIsPropManufacturedHome;
                dl.sFHAPropImprovIsPropMultifamily                 = sl.sFHAPropImprovIsPropMultifamily;
                dl.sFHAPropImprovIsPropNewOccMoreThan90Days        = sl.sFHAPropImprovIsPropNewOccMoreThan90Days;
                dl.sFHAPropImprovIsPropNonresidential              = sl.sFHAPropImprovIsPropNonresidential;
                dl.sFHAPropImprovIsPropOwnedByBorr                 = sl.sFHAPropImprovIsPropOwnedByBorr;
                dl.sFHAPropImprovIsPropSingleFamily                = sl.sFHAPropImprovIsPropSingleFamily;
                dl.sFHAPropImprovIsThereMortOnProp                 = sl.sFHAPropImprovIsThereMortOnProp;
                dl.sFHAPropImprovLeaseExpireD_rep                  = sl.sFHAPropImprovLeaseExpireD_rep;
                dl.sFHAPropImprovLeaseMonPmt_rep                   = sl.sFHAPropImprovLeaseMonPmt_rep;
                dl.sFHAPropImprovLeaseOwnerInfo                    = sl.sFHAPropImprovLeaseOwnerInfo;
                dl.sFHAPropImprovNonresidentialUsageDesc           = sl.sFHAPropImprovNonresidentialUsageDesc;
                dl.sFHAPropImprovRefinTitle1LoanBal                = sl.sFHAPropImprovRefinTitle1LoanBal;
                dl.sFHAPropImprovRefinTitle1LoanNum                = sl.sFHAPropImprovRefinTitle1LoanNum;
                dl.sFHAPropImprovRefinTitle1LoanTri                = sl.sFHAPropImprovRefinTitle1LoanTri;

            #endregion 

            #region VA REQUEST FOR CERTIFICATE OF VETERAN STATUS
                da.aActiveMilitaryStatTri             = sa.aActiveMilitaryStatTri;
                da.aVaClaimNum                        = sa.aVaClaimNum;
                da.aVaServ1BranchNum                  = sa.aVaServ1BranchNum;
                da.aVaServ1EndD_rep                   = sa.aVaServ1EndD_rep;
                da.aVaServ1FullNm                     = sa.aVaServ1FullNm;
                da.aVaServ1Num                        = sa.aVaServ1Num;
                da.aVaServ1Ssn                        = sa.aVaServ1Ssn;
                da.aVaServ1StartD_rep                 = sa.aVaServ1StartD_rep;
                da.aVaServ2BranchNum                  = sa.aVaServ2BranchNum;
                da.aVaServ2EndD_rep                   = sa.aVaServ2EndD_rep;
                da.aVaServ2FullNm                     = sa.aVaServ2FullNm;
                da.aVaServ2Num                        = sa.aVaServ2Num;
                da.aVaServ2Ssn                        = sa.aVaServ2Ssn;
                da.aVaServ2StartD_rep                 = sa.aVaServ2StartD_rep;
                da.aWereActiveMilitaryDutyDayAfterTri = sa.aWereActiveMilitaryDutyDayAfterTri;

            #endregion


            #region VA CERTIFICATE OF LOAN DISBURSEMENT
                dl.sIsAlterationCompleted                        = sl.sIsAlterationCompleted;
                dl.sLotAcquiredD_rep                             = sl.sLotAcquiredD_rep;
                dl.sProFloodInsFaceAmt_rep                       = sl.sProFloodInsFaceAmt_rep;
                dl.sProFloodInsPerYr_rep                         = sl.sProFloodInsPerYr_rep;
                dl.sProFloodInsPerYrLckd                         = sl.sProFloodInsPerYrLckd;
                dl.sProHazInsFaceAmt_rep                         = sl.sProHazInsFaceAmt_rep;
                dl.sProHazInsPerYr_rep                           = sl.sProHazInsPerYr_rep;
                dl.sProHazInsPerYrLckd                           = sl.sProHazInsPerYrLckd;
                dl.sProRealETxPerYr_rep                          = sl.sProRealETxPerYr_rep;
                dl.sProRealETxPerYrLckd                          = sl.sProRealETxPerYrLckd;
                dl.sVaAdditionalSecurityTakenDesc                = sl.sVaAdditionalSecurityTakenDesc;
                dl.sVaAdditionalSecurityTakenDescPrintSeparately = sl.sVaAdditionalSecurityTakenDescPrintSeparately;
                dl.sVaAgent1RoleDesc                             = sl.sVaAgent1RoleDesc;
                dl.sVaAgent2RoleDesc                             = sl.sVaAgent2RoleDesc;
                dl.sVaAgent3RoleDesc                             = sl.sVaAgent3RoleDesc;
                dl.sVaAgent4RoleDesc                             = sl.sVaAgent4RoleDesc;
                dl.sVaAgent5RoleDesc                             = sl.sVaAgent5RoleDesc;
                dl.sVaEstateHeldOtherDesc                        = sl.sVaEstateHeldOtherDesc;
                dl.sVaEstateHeldT                                = sl.sVaEstateHeldT;
                dl.sVaEstateHeldTLckd                            = sl.sVaEstateHeldTLckd;
                dl.sVaIsAutoProc                                 = sl.sVaIsAutoProc;
                dl.sVaIsGuarantyEvidenceRequested                = sl.sVaIsGuarantyEvidenceRequested;
                dl.sVaIsInsuranceEvidenceRequested               = sl.sVaIsInsuranceEvidenceRequested;
                dl.sVaIsPriorApprovalProc                        = sl.sVaIsPriorApprovalProc;
                dl.sVaLProceedDepositDesc                        = sl.sVaLProceedDepositDesc;
                dl.sVaLProceedDepositT                           = sl.sVaLProceedDepositT;
                dl.sVaLenderCaseNum                              = sl.sVaLenderCaseNum;
                dl.sVaLenderCaseNumLckd                          = sl.sVaLenderCaseNumLckd;
                dl.sVaMaintainAssessPmtPerYear_rep               = sl.sVaMaintainAssessPmtPerYear_rep;
                dl.sVaMaintainAssessPmtPerYearLckd               = sl.sVaMaintainAssessPmtPerYearLckd;
                dl.sVaNonrealtyAcquiredWithLDesc                 = sl.sVaNonrealtyAcquiredWithLDesc;
                dl.sVaNonrealtyAcquiredWithLDescPrintSeparately  = sl.sVaNonrealtyAcquiredWithLDescPrintSeparately;
                dl.sVaSpecialAssessUnpaid_rep                    = sl.sVaSpecialAssessUnpaid_rep;

            #endregion

            #region VA LOAN ANALYSIS
                da.aFHACCaivrsNum                  = sa.aFHACCaivrsNum;
                da.aVaBEmplmtI_rep                 = sa.aVaBEmplmtI_rep;
                da.aVaBEmplmtILckd                 = sa.aVaBEmplmtILckd;
                da.aVaBFedITax_rep                 = sa.aVaBFedITax_rep;
                da.aVaBOITax_rep                   = sa.aVaBOITax_rep;
                da.aVaBONetI_rep                   = sa.aVaBONetI_rep;
                da.aVaBONetILckd                   = sa.aVaBONetILckd;
                da.aVaBSsnTax_rep                  = sa.aVaBSsnTax_rep;
                da.aVaBStateITax_rep               = sa.aVaBStateITax_rep;
                da.aVaCEmplmtI_rep                 = sa.aVaCEmplmtI_rep;
                da.aVaCEmplmtILckd                 = sa.aVaCEmplmtILckd;
                da.aVaCFedITax_rep                 = sa.aVaCFedITax_rep;
                da.aVaCOITax_rep                   = sa.aVaCOITax_rep;
                da.aVaCONetI_rep                   = sa.aVaCONetI_rep;
                da.aVaCONetILckd                   = sa.aVaCONetILckd;
                da.aVaCSsnTax_rep                  = sa.aVaCSsnTax_rep;
                da.aVaCStateITax_rep               = sa.aVaCStateITax_rep;
                da.aVaCrRecordSatisfyTri           = sa.aVaCrRecordSatisfyTri;
                da.aVaFamilySuportGuidelineAmt_rep = sa.aVaFamilySuportGuidelineAmt_rep;
                da.aVaLAnalysisN                   = sa.aVaLAnalysisN;
                da.aVaLMeetCrStandardTri           = sa.aVaLMeetCrStandardTri;
                da.aVaONetIDesc                    = sa.aVaONetIDesc;
                da.aVaUtilityIncludedTri           = sa.aVaUtilityIncludedTri;
                dl.sVaCashdwnPmt_rep               = sl.sVaCashdwnPmt_rep;
                dl.sVaCashdwnPmtLckd               = sl.sVaCashdwnPmtLckd;
                dl.sVaMaintainAssessPmtLckd        = sl.sVaMaintainAssessPmtLckd;
                dl.sVaMaintainAssessPmt_rep        = sl.sVaMaintainAssessPmt_rep;
                dl.sVaProHazIns_rep                = sl.sVaProHazIns_rep;
                dl.sVaProHazInsLckd                = sl.sVaProHazInsLckd;
                dl.sVaProMaintenancePmt_rep        = sl.sVaProMaintenancePmt_rep;
                dl.sVaProRealETx_rep               = sl.sVaProRealETx_rep;
                dl.sVaProRealETxLckd               = sl.sVaProRealETxLckd;
                dl.sVaProUtilityPmt_rep            = sl.sVaProUtilityPmt_rep;
                dl.sVaSpecialAssessPmt_rep         = sl.sVaSpecialAssessPmt_rep;
            #endregion

            #region VA LOAN SUMMARY

                da.aVaEntitleAmt                        = sa.aVaEntitleAmt;
                da.aVaEntitleCode                       = sa.aVaEntitleCode;
                da.aVaIsVeteranFirstTimeBuyerTri        = sa.aVaIsVeteranFirstTimeBuyerTri;
                da.aVaMilitaryStatT                     = sa.aVaMilitaryStatT;
                da.aVaServiceBranchT                    = sa.aVaServiceBranchT;
                dl.sIsProcessedUnderAutoUnderwritingTri = sl.sIsProcessedUnderAutoUnderwritingTri;
                dl.sSpAge                               = sl.sSpAge;
                dl.sSpBathCount                         = sl.sSpBathCount;
                dl.sSpBedroomCount                      = sl.sSpBedroomCount;
                dl.sSpLivingSqf                         = sl.sSpLivingSqf;
                dl.sSpRoomCount                         = sl.sSpRoomCount;
                dl.sSpT                                 = sl.sSpT;
                dl.sVaApprT                             = sl.sVaApprT;
                dl.sVaAppraisalOrSarAdjustmentTri       = sl.sVaAppraisalOrSarAdjustmentTri;
                dl.sVaAutoUnderwritingT                 = sl.sVaAutoUnderwritingT;
                dl.sVaEnergyImprovAmt                   = sl.sVaEnergyImprovAmt;
                dl.sVaEnergyImprovIsInsulation          = sl.sVaEnergyImprovIsInsulation;
                dl.sVaEnergyImprovIsMajorSystem         = sl.sVaEnergyImprovIsMajorSystem;
                dl.sVaEnergyImprovIsNewFeature          = sl.sVaEnergyImprovIsNewFeature;
                dl.sVaEnergyImprovIsOther               = sl.sVaEnergyImprovIsOther;
                dl.sVaEnergyImprovIsSolar               = sl.sVaEnergyImprovIsSolar;
                dl.sVaFfExemptTri = sl.sVaFfExemptTri;
                dl.sVaFinMethT                          = sl.sVaFinMethT;
                dl.sVaFinMethTLckd                      = sl.sVaFinMethTLckd;
                dl.sVaIrrrlsUsedOnlyOrigIR              = sl.sVaIrrrlsUsedOnlyOrigIR;
                dl.sVaIrrrlsUsedOnlyOrigLAmt            = sl.sVaIrrrlsUsedOnlyOrigLAmt;
                dl.sVaIrrrlsUsedOnlyPdInFullLNum        = sl.sVaIrrrlsUsedOnlyPdInFullLNum;
                dl.sVaIrrrlsUsedOnlyRemarks             = sl.sVaIrrrlsUsedOnlyRemarks;
                dl.sVaIsAutoIrrrlProc                   = sl.sVaIsAutoIrrrlProc;
                dl.sVaLCodeT                            = sl.sVaLCodeT;
                dl.sVaLCodeTLckd                        = sl.sVaLCodeTLckd;
                dl.sVaLPurposeT                         = sl.sVaLPurposeT;
                dl.sVaLPurposeTLckd                     = sl.sVaLPurposeTLckd;
                dl.sVaLenSarId                          = sl.sVaLenSarId;
                dl.sVaManufacturedHomeT                 = sl.sVaManufacturedHomeT;
                dl.sVaMcrvNum                           = sl.sVaMcrvNum;
                dl.sVaOwnershipT                        = sl.sVaOwnershipT;
                dl.sVaPropDesignationT                  = sl.sVaPropDesignationT;
                dl.sVaRiskT                             = sl.sVaRiskT;
                dl.sVaSarNotifIssuedD                   = sl.sVaSarNotifIssuedD;
                dl.sVaStructureT                        = sl.sVaStructureT;
                dl.sVaVetMedianCrScore                  = sl.sVaVetMedianCrScore;
                dl.sVaVetMedianCrScoreLckd              = sl.sVaVetMedianCrScoreLckd;

            #endregion

            #region VA REFI WORKSHEET
                dl.sVaRefiWsAllowableCcAndPp_rep = sl.sVaRefiWsAllowableCcAndPp_rep;
                dl.sVaRefiWsCashPmtFromVet_rep   = sl.sVaRefiWsCashPmtFromVet_rep;
                dl.sVaRefiWsDiscntPc_rep         = sl.sVaRefiWsDiscntPc_rep;
                dl.sVaRefiWsExistingVaLBal_rep   = sl.sVaRefiWsExistingVaLBal_rep;
                dl.sVaRefiWsFinalDiscntPc_rep    = sl.sVaRefiWsFinalDiscntPc_rep;
                dl.sVaRefiWsOrigFeePc_rep        = sl.sVaRefiWsOrigFeePc_rep;

            #endregion

            #region VA REQUEST FOR ELIGIBILITY
                da.aVaDischargedDisabilityIs = sa.aVaDischargedDisabilityIs;
                da.aVaServ3BranchNum         = sa.aVaServ3BranchNum;
                da.aVaServ3EndD_rep          = sa.aVaServ3EndD_rep;
                da.aVaServ3FullNm            = sa.aVaServ3FullNm;
                da.aVaServ3Num               = sa.aVaServ3Num;
                da.aVaServ3Ssn               = sa.aVaServ3Ssn;
                da.aVaServ3StartD_rep        = sa.aVaServ3StartD_rep;
                da.aVaServ4BranchNum         = sa.aVaServ4BranchNum;
                da.aVaServ4EndD_rep          = sa.aVaServ4EndD_rep;
                da.aVaServ4FullNm            = sa.aVaServ4FullNm;
                da.aVaServ4Num               = sa.aVaServ4Num;
                da.aVaServ4Ssn               = sa.aVaServ4Ssn;
                da.aVaServ4StartD_rep        = sa.aVaServ4StartD_rep;

                if (sl.sBorrowerApplicationCollectionT == E_sLqbCollectionT.Legacy)
                {
                    da.AcceptNewVaPastLCollection(sa.aVaPastLXmlContent);
                }
            #endregion

            #region VA REQUEST DETERMINATION OF REASONABLE VALUE

                dl.sLotPurchaseSeparatelyTri = sl.sLotPurchaseSeparatelyTri;
                dl.sSpHasClothesWasher = sl.sSpHasClothesWasher;
                dl.sSpHasDishWasher = sl.sSpHasDishWasher;
                dl.sSpHasDryer = sl.sSpHasDryer;
                dl.sSpHasGarbageDisposal = sl.sSpHasGarbageDisposal;
                dl.sSpHasOtherEquip = sl.sSpHasOtherEquip;
                dl.sSpHasOven = sl.sSpHasOven;
                dl.sSpHasRefrig = sl.sSpHasRefrig;
                dl.sSpHasVentFan = sl.sSpHasVentFan;
                dl.sSpLeaseAnnualGroundRent = sl.sSpLeaseAnnualGroundRent;
                dl.sSpLeaseIs99Yrs = sl.sSpLeaseIs99Yrs;
                dl.sSpLeaseIsRenewable = sl.sSpLeaseIsRenewable;
                dl.sSpLotAcres = sl.sSpLotAcres;
                dl.sSpLotDimension = sl.sSpLotDimension;
                dl.sSpLotIrregularSqf = sl.sSpLotIrregularSqf;
                dl.sSpLotIsAcres = sl.sSpLotIsAcres;
                dl.sSpLotIsIrregular = sl.sSpLotIsIrregular;
                dl.sSpMineralRightsReservedExplain = sl.sSpMineralRightsReservedExplain;
                dl.sSpMineralRightsReservedTri = sl.sSpMineralRightsReservedTri;
                dl.sSpOtherEquipDesc = sl.sSpOtherEquipDesc;
                dl.sSpUtilElecT = sl.sSpUtilElecT;
                dl.sSpUtilGasT = sl.sSpUtilGasT;
                dl.sSpUtilSanSewerT = sl.sSpUtilSanSewerT;
                dl.sSpUtilWaterT = sl.sSpUtilWaterT;
                dl.sVaBuildingStatusT = sl.sVaBuildingStatusT;
                dl.sVaBuildingStatusTLckd = sl.sVaBuildingStatusTLckd;
                dl.sVaBuildingT = sl.sVaBuildingT;
                dl.sVaConstructCompleteD_rep = sl.sVaConstructCompleteD_rep;
                dl.sVaConstructComplianceInspectionMadeByT = sl.sVaConstructComplianceInspectionMadeByT;
                dl.sVaConstructExpiredD_rep = sl.sVaConstructExpiredD_rep;
                dl.sVaConstructPlansFirstSubmitTri = sl.sVaConstructPlansFirstSubmitTri;
                dl.sVaConstructPrevPlansCaseNum = sl.sVaConstructPrevPlansCaseNum;
                dl.sVaConstructWarrantyProgramNm = sl.sVaConstructWarrantyProgramNm;
                dl.sVaConstructWarrantyTri = sl.sVaConstructWarrantyTri;
                dl.sVaIsFactoryFabricatedTri = sl.sVaIsFactoryFabricatedTri;
                dl.sVaNumOfBuildings = sl.sVaNumOfBuildings;
                dl.sVaNumOfLivingUnits = sl.sVaNumOfLivingUnits;
                dl.sVaOwnerNm = sl.sVaOwnerNm;
                dl.sVaPrevApprovedContractNum = sl.sVaPrevApprovedContractNum;
                dl.sVaRefiAmt_rep = sl.sVaRefiAmt_rep;
                dl.sVaSaleContractAttachedTri = sl.sVaSaleContractAttachedTri;
                dl.sVaSpAvailableForInspectDesc = sl.sVaSpAvailableForInspectDesc;
                dl.sVaSpAvailableForInspectTimeT = sl.sVaSpAvailableForInspectTimeT;
                dl.sVaSpOccT = sl.sVaSpOccT;
                dl.sVaSpOccupantNm = sl.sVaSpOccupantNm;
                dl.sVaSpOccupantPhone = sl.sVaSpOccupantPhone;
                dl.sVaSpRentalMonthly = sl.sVaSpRentalMonthly;
                dl.sVaSpecialAssessmentsComments = sl.sVaSpecialAssessmentsComments;
                dl.sVaStreetAccessPrivateTri = sl.sVaStreetAccessPrivateTri;
                dl.sVaStreetMaintenancePrivateTri = sl.sVaStreetMaintenancePrivateTri;
                dl.sVaTitleLimitDesc = sl.sVaTitleLimitDesc;
                dl.sVaTitleLimitIsCondo = sl.sVaTitleLimitIsCondo;
                dl.sVaTitleLimitIsPud = sl.sVaTitleLimitIsPud;

            #endregion

            #region VA VERIFICATION OF BENEFIT
                da.aVaClaimFolderNum = sa.aVaClaimFolderNum;
                da.aVaIndebtCertifyTri = sa.aVaIndebtCertifyTri;

            #endregion


            #region REQUEST FOR APPRAISAL
                dl.sApprContactForEntry    = sl.sApprContactForEntry;
                dl.sApprInfo               = sl.sApprInfo;
                dl.sApprMarketRentAnalysis = sl.sApprMarketRentAnalysis;
                dl.sGseRequestAppraisalT   = sl.sGseRequestAppraisalT;
                dl.sGseRequestAppraisalOther = sl.sGseRequestAppraisalOther;
                dl.sFannie2055             = sl.sFannie2055;
                dl.sFannie2065             = sl.sFannie2065;
                dl.sFreddie2055            = sl.sFreddie2055;
                dl.sFreddie2070            = sl.sFreddie2070;

            #endregion

            #region REQUEST FOR INSURANCE
                dl.sInsReqFlood    = sl.sInsReqFlood;
                dl.sInsReqWind     = sl.sInsReqWind;
                dl.sInsReqHazard   = sl.sInsReqHazard;
                dl.sInsReqEscrow   = sl.sInsReqEscrow;
                dl.sInsReqComments = sl.sInsReqComments;
            #endregion

            #region REQUEST FOR TITLE
                dl.sTitleReqOwnerNm         = sl.sTitleReqOwnerNm;
                dl.sTitleReqOwnerPhone      = sl.sTitleReqOwnerPhone;
                dl.sTitleReqPriorPolicy     = sl.sTitleReqPriorPolicy;
                dl.sTitleReqWarrantyDeed    = sl.sTitleReqWarrantyDeed;
                dl.sTitleReqInsRequirements = sl.sTitleReqInsRequirements;
                dl.sTitleReqSurvey          = sl.sTitleReqSurvey;
                dl.sTitleReqContract        = sl.sTitleReqContract;
                dl.sTitleReqPolicyTypeDesc  = sl.sTitleReqPolicyTypeDesc;
                dl.sTitleReqMailAway        = sl.sTitleReqMailAway;
                dl.sTitleReqInstruction     = sl.sTitleReqInstruction;
            #endregion

            #region TRUST ACCOUNTS
                /*
                dl.sTrust1Desc               = sl.sTrust1Desc;
                dl.sTrust1D_rep              = sl.sTrust1D_rep;
                dl.sTrust1PayableChkNum      = sl.sTrust1PayableChkNum;
                dl.sTrust1PayableAmt_rep     = sl.sTrust1PayableAmt_rep;
                dl.sTrust1ReceivableChkNum   = sl.sTrust1ReceivableChkNum;
                dl.sTrust1ReceivableAmt_rep  = sl.sTrust1ReceivableAmt_rep;
                dl.sTrust1ReceivableN        = sl.sTrust1ReceivableN;
                dl.sTrust2Desc               = sl.sTrust2Desc;
                dl.sTrust2D_rep              = sl.sTrust2D_rep;
                dl.sTrust2PayableChkNum      = sl.sTrust2PayableChkNum;
                dl.sTrust2PayableAmt_rep     = sl.sTrust2PayableAmt_rep;
                dl.sTrust2ReceivableChkNum   = sl.sTrust2ReceivableChkNum;
                dl.sTrust2ReceivableAmt_rep  = sl.sTrust2ReceivableAmt_rep;
                dl.sTrust2ReceivableN        = sl.sTrust2ReceivableN;
                dl.sTrust3Desc               = sl.sTrust3Desc;
                dl.sTrust3D_rep              = sl.sTrust3D_rep;
                dl.sTrust3PayableChkNum      = sl.sTrust3PayableChkNum;
                dl.sTrust3PayableAmt_rep     = sl.sTrust3PayableAmt_rep;
                dl.sTrust3ReceivableChkNum   = sl.sTrust3ReceivableChkNum;
                dl.sTrust3ReceivableAmt_rep  = sl.sTrust3ReceivableAmt_rep;
                dl.sTrust3ReceivableN        = sl.sTrust3ReceivableN;
                dl.sTrust4Desc               = sl.sTrust4Desc;
                dl.sTrust4D_rep              = sl.sTrust4D_rep;
                dl.sTrust4PayableChkNum      = sl.sTrust4PayableChkNum;
                dl.sTrust4PayableAmt_rep     = sl.sTrust4PayableAmt_rep;
                dl.sTrust4ReceivableChkNum   = sl.sTrust4ReceivableChkNum;
                dl.sTrust4ReceivableAmt_rep  = sl.sTrust4ReceivableAmt_rep;
                dl.sTrust4ReceivableN        = sl.sTrust4ReceivableN;
                dl.sTrust5Desc               = sl.sTrust5Desc;
                dl.sTrust5D_rep              = sl.sTrust5D_rep;
                dl.sTrust5PayableChkNum      = sl.sTrust5PayableChkNum;
                dl.sTrust5PayableAmt_rep     = sl.sTrust5PayableAmt_rep;
                dl.sTrust5ReceivableChkNum   = sl.sTrust5ReceivableChkNum;
                dl.sTrust5ReceivableAmt_rep  = sl.sTrust5ReceivableAmt_rep;
                dl.sTrust5ReceivableN        = sl.sTrust5ReceivableN;
                dl.sTrust6Desc               = sl.sTrust6Desc;
                dl.sTrust6D_rep              = sl.sTrust6D_rep;
                dl.sTrust6PayableChkNum      = sl.sTrust6PayableChkNum;
                dl.sTrust6PayableAmt_rep     = sl.sTrust6PayableAmt_rep;
                dl.sTrust6ReceivableChkNum   = sl.sTrust6ReceivableChkNum;
                dl.sTrust6ReceivableAmt_rep  = sl.sTrust6ReceivableAmt_rep;
                dl.sTrust6ReceivableN        = sl.sTrust6ReceivableN;
                dl.sTrust7Desc               = sl.sTrust7Desc;
                dl.sTrust7D_rep              = sl.sTrust7D_rep;
                dl.sTrust7PayableChkNum      = sl.sTrust7PayableChkNum;
                dl.sTrust7PayableAmt_rep     = sl.sTrust7PayableAmt_rep;
                dl.sTrust7ReceivableChkNum   = sl.sTrust7ReceivableChkNum;
                dl.sTrust7ReceivableAmt_rep  = sl.sTrust7ReceivableAmt_rep;
                dl.sTrust7ReceivableN        = sl.sTrust7ReceivableN;
                dl.sTrust8Desc               = sl.sTrust8Desc;
                dl.sTrust8D_rep              = sl.sTrust8D_rep;
                dl.sTrust8PayableChkNum      = sl.sTrust8PayableChkNum;
                dl.sTrust8PayableAmt_rep     = sl.sTrust8PayableAmt_rep;
                dl.sTrust8ReceivableChkNum   = sl.sTrust8ReceivableChkNum;
                dl.sTrust8ReceivableAmt_rep  = sl.sTrust8ReceivableAmt_rep;
                dl.sTrust8ReceivableN        = sl.sTrust8ReceivableN;
                dl.sTrust9Desc               = sl.sTrust9Desc;
                dl.sTrust9D_rep              = sl.sTrust9D_rep;
                dl.sTrust9PayableChkNum      = sl.sTrust9PayableChkNum;
                dl.sTrust9PayableAmt_rep     = sl.sTrust9PayableAmt_rep;
                dl.sTrust9ReceivableChkNum   = sl.sTrust9ReceivableChkNum;
                dl.sTrust9ReceivableAmt_rep  = sl.sTrust9ReceivableAmt_rep;
                dl.sTrust9ReceivableN        = sl.sTrust9ReceivableN;
                dl.sTrust10Desc              = sl.sTrust10Desc;
                dl.sTrust10D_rep             = sl.sTrust10D_rep;
                dl.sTrust10PayableChkNum     = sl.sTrust10PayableChkNum;
                dl.sTrust10PayableAmt_rep    = sl.sTrust10PayableAmt_rep;
                dl.sTrust10ReceivableChkNum  = sl.sTrust10ReceivableChkNum;
                dl.sTrust10ReceivableAmt_rep = sl.sTrust10ReceivableAmt_rep;
                dl.sTrust10ReceivableN       = sl.sTrust10ReceivableN;
                */
            #endregion


			#region VERIFICATION
			
                if( dl.sQualBottomR_rep != sl.sQualBottomR_rep )
                {
                    string err = "Two loans don't have the same bottom ratios after update."; 
                    AddMsgToUser( err, true );
                }
                if( dl.sQualTopR_rep != sl.sQualTopR_rep )
                {
                    string err = "Two loans don't have the same top ratios after update."; 
                    AddMsgToUser( err, true );
				
                }
                if( dl.sCltvR_rep != sl.sCltvR_rep )
                {
                    string err = "Two loans don't have the same combined LTV after update."; 
                    AddMsgToUser( err, true );
                }

			#endregion

            #region CREDIT SCORE DISCLOSURE
                dl.sEquifaxScoreTo_rep      = sl.sEquifaxScoreTo_rep;
                dl.sEquifaxScoreFrom_rep    = sl.sEquifaxScoreFrom_rep;
                dl.sTransUnionScoreTo_rep   = sl.sTransUnionScoreTo_rep;
                dl.sTransUnionScoreFrom_rep = sl.sTransUnionScoreFrom_rep;
                dl.sExperianScoreTo_rep     = sl.sExperianScoreTo_rep;
                dl.sExperianScoreFrom_rep   = sl.sExperianScoreFrom_rep;
                da.aBExperianCreatedD_rep   = sa.aBExperianCreatedD_rep;
                da.aBTransUnionCreatedD_rep = sa.aBTransUnionCreatedD_rep;
                da.aBEquifaxCreatedD_rep    = sa.aBEquifaxCreatedD_rep;
                da.aCExperianCreatedD_rep   = sa.aCExperianCreatedD_rep;
                da.aCTransUnionCreatedD_rep = sa.aCTransUnionCreatedD_rep;
                da.aCEquifaxCreatedD_rep    = sa.aCEquifaxCreatedD_rep;
                da.aBExperianFactors        = sa.aBExperianFactors;
                da.aBTransUnionFactors      = sa.aBTransUnionFactors;
                da.aBEquifaxFactors         = sa.aBEquifaxFactors;
                da.aCExperianFactors        = sa.aCExperianFactors;
                da.aCTransUnionFactors      = sa.aCTransUnionFactors;
                da.aCEquifaxFactors         = sa.aCEquifaxFactors;
                da.aBExperianScore_rep      = sa.aBExperianScore_rep;
                da.aCExperianScore_rep      = sa.aCExperianScore_rep;
                da.aBTransUnionScore_rep    = sa.aBTransUnionScore_rep;
                da.aCTransUnionScore_rep    = sa.aCTransUnionScore_rep;
                da.aBEquifaxScore_rep       = sa.aBEquifaxScore_rep;
                da.aCEquifaxScore_rep       = sa.aCEquifaxScore_rep;
                da.aBExperianModelName      = sa.aBExperianModelName;
                da.aCExperianModelName      = sa.aCExperianModelName;
                da.aBTransUnionModelName    = sa.aBTransUnionModelName;
                da.aCTransUnionModelName    = sa.aCTransUnionModelName;
                da.aBEquifaxModelName       = sa.aBEquifaxModelName;
                da.aCEquifaxModelName       = sa.aCEquifaxModelName;
                da.aBOtherCreditModelName   = sa.aBOtherCreditModelName;
                da.aCOtherCreditModelName   = sa.aCOtherCreditModelName;
                da.aBDecisionCreditScore_rep = sa.aBDecisionCreditScore_rep;
                da.aCDecisionCreditScore_rep = sa.aCDecisionCreditScore_rep;
                #endregion

                #region SURVEY REQUEST
                dl.sSurveyRequestN      = sl.sSurveyRequestN;
                dl.sAttachedSpecsBit    = sl.sAttachedSpecsBit;
                dl.sAttachedCostsBit    = sl.sAttachedCostsBit;
                dl.sAttachedPlansBit    = sl.sAttachedPlansBit;
                dl.sAttachedSurveyBit   = sl.sAttachedSurveyBit;
                dl.sAttachedContractBit = sl.sAttachedContractBit;
            #endregion

            #region ARM Disclosure (SKIP, Depend on loan)
                // sArmIndexT
                // sFreddieArmIndexT
                // sArmIndexEffectiveD
                // sArmIndexBasedOnVstr
                // sArmIndexCanBeFoundVstr
                // sArmIndexAffectInitIRBit
                // sArmIndexNotifyAtLeastDaysVstr
                // sArmIndexNotifyNotBeforeDaysVstr

            #endregion
                dl.sMBrokerNmOfLaw = sl.sMBrokerNmOfLaw;
                da.aIsBorrSpousePrimaryWageEarner = sa.aIsBorrSpousePrimaryWageEarner;
                //dl.sTotEstCcNoDiscnt1003_rep = sl.sONewFinCc_rep;
                //dl.sTotEstCc1003Lckd = true;

            #region Hazard Insurance Policy Page
                dl.sHazInsCompanyNm = sl.sHazInsCompanyNm;
                dl.sHazInsPolicyNum = sl.sHazInsPolicyNum;
                dl.sHazInsPolicyActivationD = sl.sHazInsPolicyActivationD;
                dl.sHazInsPolicyExpirationD = sl.sHazInsPolicyExpirationD;
                dl.sHazInsPaidByT = sl.sHazInsPaidByT;
                dl.sHazInsCoverageAmt = sl.sHazInsCoverageAmt;
                dl.sHazInsDeductibleAmt = sl.sHazInsDeductibleAmt;
                dl.sHazInsPolicyPaymentTypeT = sl.sHazInsPolicyPaymentTypeT;
                dl.sHazInsPaymentDueD = sl.sHazInsPaymentDueD;
                dl.sHazInsPaymentMadeD = sl.sHazInsPaymentMadeD;
                dl.sHazInsPaidThroughD = sl.sHazInsPaidThroughD;
                dl.sHazInsPolicyPayeeCode = sl.sHazInsPolicyPayeeCode;
                dl.sHazInsPolicyLossPayeeTransD = sl.sHazInsPolicyLossPayeeTransD;
                dl.sHazInsCompanyId = sl.sHazInsCompanyId;
                dl.sHazInsPaymentDueAmt = sl.sHazInsPaymentDueAmt;
                dl.sHazInsPaymentDueAmtLckd = sl.sHazInsPaymentDueAmtLckd;
            #endregion

            #region Title and Vesting Page
                dl.sRequiredEndorsements = sl.sRequiredEndorsements;
                dl.sTitleReportItemsDescription = sl.sTitleReportItemsDescription;
                da.aBRelationshipTitleT = sa.aBRelationshipTitleT;
                da.aCRelationshipTitleT = sa.aCRelationshipTitleT;
                dl.sPropertyTaxMessageDescription = sl.sPropertyTaxMessageDescription;
                //da.aManner = sa.aManner;

                // are these already copied? According to excel, they are but doesn't seem like it
                dl.sTrustName = sl.sTrustName;
                dl.sTrustForBenefitOf = sl.sTrustForBenefitOf;
                dl.sTrustAgreementD = sl.sTrustAgreementD;
                dl.sTrustState = sl.sTrustState;
                dl.sTrustId = sl.sTrustId;

                // For both Trustors/Settlors and Trustees
                dl.sTrustCollection = sl.sTrustCollection;
                dl.sVestingToReadLckd = sl.sVestingToReadLckd;
                dl.sVestingToRead = sl.sVestingToRead;
            #endregion
            }

            // For legacy loans, the collections were copied via the xml above.
            // For loans that use the new collections data layer, they are copied here.
            if (sl.sBorrowerApplicationCollectionT != E_sLqbCollectionT.Legacy)
            {
                dl.DuplicateLqbCollectionEntityDataFrom(sl);
            }

            UpdateLenderSpecifiedFields(sl, dl);
        }

        public static void UpdateLinkedLoanWithLenderSpecifiedFieldsFromInvokingLoan(Guid brokerId, Guid invokingLoanId, Guid linkedLoanId)
        {
            var brokerDB = LendersOffice.Admin.BrokerDB.RetrieveById(brokerId);
            if (brokerDB.LinkedLoanUpdateFields.Count > 0)
            {
                CPopulate80To20 populator = new CPopulate80To20(brokerDB, invokingLoanId, linkedLoanId);
                var firstAndSecondLiens = populator.InitializeLinkedLoans();
                UpdateLenderSpecifiedFields(firstAndSecondLiens[0], firstAndSecondLiens[1]);
                firstAndSecondLiens[1].Save();
            }
        }

        private static void UpdateLenderSpecifiedFields(CPageData sourceLoan, CPageData destinationLoan)
        {
            if (sourceLoan.sBrokerId != destinationLoan.sBrokerId)
            {
                throw new GenericUserErrorMessageException(
                    "Source and Destination loans do not have matching broker IDs" + Environment.NewLine +
                    "Source      Broker ID = " + sourceLoan.sBrokerId + Environment.NewLine +
                    "Destination Broker ID = " + destinationLoan.sBrokerId);
            }

            List<string> additionalAppFields = new List<string>(sourceLoan.BrokerDB.LinkedLoanUpdateFields.Count);
            foreach (string field in sourceLoan.BrokerDB.LinkedLoanUpdateFields)
            {
                E_PageDataClassType dataClassType;
                if (PageDataUtilities.GetPageDataClassType(field, out dataClassType))
                {
                    switch (dataClassType)
                    {
                        case E_PageDataClassType.CBasePage:
                            PageDataUtilities.SetValue(destinationLoan, null, field, PageDataUtilities.GetValue(sourceLoan, null, field));
                            break;
                        case E_PageDataClassType.CAppBase:
                            additionalAppFields.Add(field);
                            break;
                        default:
                            throw new UnhandledEnumException(dataClassType);
                    }
                }
            }

            if (additionalAppFields.Count > 0)
            {
                for (int i = 0; i < sourceLoan.nApps; ++i)
                {
                    CAppData sourceApp = sourceLoan.GetAppData(i);
                    CAppData destinationApp = destinationLoan.GetAppData(i);
                    foreach (string field in additionalAppFields)
                    {
                        PageDataUtilities.SetValue(destinationLoan, destinationApp, field, PageDataUtilities.GetValue(sourceLoan, sourceApp, field));
                    }
                }
            }
        }

        public static void MergeAgents(CPageData sourceLoan, CPageData destinationLoan)
        {
            // Dodge the expensive operation if possible.
            if (destinationLoan.GetAgentRecordCount() == 0)
            {
                destinationLoan.sAgentDataSet = sourceLoan.sAgentDataSet;
                return;
            }

#if (DEBUG)
            LogAgents("before merge", sourceLoan, destinationLoan);
#endif
            var table = destinationLoan.sAgentDataSet.Tables[CAgentFields.TableName];
            table.PrimaryKey = new System.Data.DataColumn[] { table.Columns[CAgentFields.RecordIdColumnName] };
            destinationLoan.sAgentDataSet.Merge(sourceLoan.sAgentDataSet, false, System.Data.MissingSchemaAction.Error);
            destinationLoan.sAgentDataSet = destinationLoan.sAgentDataSet; // Need to call the setter to update the serialization fields
#if (DEBUG)
            LogAgents("after merge", sourceLoan, destinationLoan);
#endif
        }

        private static void LogAgents(string message, CPageData sourceLoan, CPageData destinationLoan)
        {
            Func<CAgentFields, string> agentString = agent => agent.RecordId.ToString().PadRight(40) + agent.AgentRoleT.ToString().PadRight(25) + agent.AgentName;
            Func<IEnumerable<CAgentFields>, string> agentsToString = agents => "Record ID".PadRight(40) + "Agent Role T".PadRight(25) + "Agent Name" + Environment.NewLine
                + string.Join(Environment.NewLine, agents.Select(agent => agentString(agent)).ToArray());

            Tools.LogInfo("Agents [" + message + "]:" + Environment.NewLine
                + "Source:" + Environment.NewLine
                + agentsToString(sourceLoan.sAgents) + Environment.NewLine
                + "Destination:" + Environment.NewLine
                + agentsToString(destinationLoan.sAgents));
        }
	}
}