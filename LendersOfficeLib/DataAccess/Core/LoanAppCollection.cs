﻿namespace DataAccess
{
    using System;
    using DataAccess.PathDispatch;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// Wrapper class to provide an object that CPageDate can delegate selecting
    /// a particular application when locating data using paths.
    /// </summary>
    public class LoanAppCollection : IPathResolvable
    {
        /// <summary>
        /// The <see cref="CPageData"/> instance being wrapped.
        /// </summary>
        private CPageData dataLoan;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanAppCollection"/> class.
        /// </summary>
        /// <param name="dataLoan">The CPageData instance being wrapped.</param>
        public LoanAppCollection(CPageData dataLoan)
        {
            this.dataLoan = dataLoan;
        }

        /// <summary>
        /// Returns the value specified by the element.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns>The value specified by the element.</returns>
        public object GetElement(IDataPathElement element)
        {
            if (!(element is DataPathSelectionElement))
            {
                throw new ArgumentException("LoanApp can only handle DataPathSelectionElements, but passed element of type " + element.GetType());
            }

            var selectorElement = (DataPathSelectionElement)element;

            int appNum;

            if (int.TryParse(selectorElement.Name, out appNum))
            {
                return this.dataLoan.GetAppData(appNum);
            }
            else
            {
                throw new ArgumentException("Could not parse application argument to a valid integer");
            }
        }
    }
}
