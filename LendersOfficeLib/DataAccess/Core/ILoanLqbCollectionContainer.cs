﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using LendingQB.Core;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Interface for the LoanLqbCollectionContainer class.
    /// </summary>
    public partial interface ILoanLqbCollectionContainer
    {
        /// <summary>
        /// Gets a value indicating whether load has been called.
        /// </summary>
        bool IsLoaded { get; }

        /// <summary>
        /// Gets a value indicating whether any collections have been registered.
        /// </summary>
        bool HasRegisteredCollections { get; }

        /// <summary>
        /// Gets the legacy application to consumer associations.
        /// </summary>
        IReadOnlyLqbAssociationSet<DataObjectKind.LegacyApplicationConsumerAssociation, Guid, ILegacyApplicationConsumerAssociation> LegacyApplicationConsumers { get; }

        /// <summary>
        /// Registers loan collections that will need to be loaded or saved.
        /// This must be done prior to accessing the collections.
        /// </summary>
        /// <param name="collectionNames">The names of the collections.</param>
        void RegisterCollections(IEnumerable<string> collectionNames);

        /// <summary>
        /// Register a loan collection that will need to be loaded or saved.
        /// This must be done prior to accessing the collection.
        /// </summary>
        /// <param name="collectionName">The name of the collection.</param>
        void RegisterCollection(string collectionName);

        /// <summary>
        /// Gets a value indicating if the specified collection has changes that are being tracked for saving.
        /// </summary>
        /// <param name="collectionName">The name of the collection.</param>
        /// <returns><see langword="true"/> if the collection has changes that are being tracked; <see langword="false"/> otherwise.</returns>
        bool IsCollectionModified(string collectionName);

        /// <summary>
        /// Load the applicable collections.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="connection">The database connection.</param>
        /// <param name="transaction">The database transaction.</param>
        void Load(
            DataObjectIdentifier<DataObjectKind.ClientCompany, Guid> brokerId,
            IDbConnection connection,
            IDbTransaction transaction);

        /// <summary>
        /// Begin tracking changes on all collections.  Call this if
        /// planning to save a change to a collection.
        /// </summary>
        void BeginTrackingChanges();

        /// <summary>
        /// Commit the changes to the database.
        /// </summary>
        /// <param name="connection">The database connection.</param>
        /// <param name="transaction">The database transaction.</param>
        void Save(IDbConnection connection, IDbTransaction transaction);

        /// <summary>
        /// Method to initialize ULAD apps when migrating a file onto the ULAD data layer.
        /// </summary>
        void InitializeUladAppsToMatchLegacyApps();

        /// <summary>
        /// Duplicates the ULAD applications from the given source container.
        /// </summary>
        /// <param name="source">The source container.</param>
        /// <param name="consumerIdMap">A map from source consumer id to destination consumer id.</param>
        /// <param name="legacyAppIdMap">A map from source legacy app id to destination legacy app id.</param>
        void DuplicateUladApplications(
            ILoanLqbCollectionContainer source,
            Dictionary<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, DataObjectIdentifier<DataObjectKind.Consumer, Guid>> consumerIdMap,
            Dictionary<DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>, DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>> legacyAppIdMap);

        /// <summary>
        /// Add a new consumer as a coborrower to the specified ULAD application.
        /// </summary>
        /// <param name="uladAppId">The ULAD application under which the consumer is added.</param>
        /// <returns>The identifier for the newly added consumer.</returns>
        DataObjectIdentifier<DataObjectKind.Consumer, Guid> AddConsumerToUladApplication(
            DataObjectIdentifier<DataObjectKind.UladApplication, Guid> uladAppId);

        /// <summary>
        /// Add a new legacy application to the loan.
        /// </summary>
        /// <returns>The identifier for the newly added legacy application.</returns>
        DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> AddBlankLegacyApplication();

        /// <summary>
        /// Add a new ULAD application to the entity collection.
        /// </summary>
        /// <returns>The newly added record and its identifier.</returns>
        KeyValuePair<DataObjectIdentifier<DataObjectKind.UladApplication, Guid>, UladApplication> AddBlankUladApplication();

        /// <summary>
        /// Adds a co-borrower to an existing legacy application.
        /// </summary>
        /// <param name="appId">The application id.</param>
        /// <returns>The consumer id of the created co-borrower.</returns>
        DataObjectIdentifier<DataObjectKind.Consumer, Guid> AddCoborrowerToLegacyApplication(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId);

        /// <summary>
        /// Associates the given liability with a subject property REO. If the
        /// subject property REO does not exist, it is created.
        /// </summary>
        /// <param name="liabilityId">The liability id.</param>
        void AssociateLiabilityWithSubjectPropertyReo(DataObjectIdentifier<DataObjectKind.Liability, Guid> liabilityId);

        /// <summary>
        /// Clears <see cref="LendingQB.Core.Data.Liability.IsSp1stMtgData"/> for all liabilities.
        /// </summary>
        void ClearIsSp1stMtgDataForAllLiabilities();

        /// <summary>
        /// Gets a value indicating whether the liability is the subject property 1st mortgage.
        /// </summary>
        /// <param name="id">The liability id.</param>
        /// <param name="liability">The liability.</param>
        /// <returns>True if the liability is the subject property 1st mortgage. False if not. Null if depended upon data is null.</returns>
        bool? GetIsSp1stMtg(DataObjectIdentifier<DataObjectKind.Liability, Guid> id, Liability liability);

        /// <summary>
        /// Gets a value indicating whether the liability is for the subject property mortgage.
        /// </summary>
        /// <param name="id">The id of the liability.</param>
        /// <param name="liability">The liability.</param>
        /// <returns>True if it's for the subject property mortgage. False if it is not. May return null if needed data is null.</returns>
        bool? GetIsSpMtg(DataObjectIdentifier<DataObjectKind.Liability, Guid> id, Liability liability);

        /// <summary>
        /// Gets the consumer ordering at the loan level.
        /// </summary>
        /// <returns>The consumer ordering at the loan level.</returns>
        IOrderedIdentifierCollection<DataObjectKind.Consumer, Guid> GetConsumerOrderingForLoan();

        /// <summary>
        /// Gets the consumer ordering at the level of the ULAD application.
        /// </summary>
        /// <param name="uladAppId">The ULAD application identifier.</param>
        /// <returns>The consumer ordering at the level of the ULAD application.</returns>
        IOrderedIdentifierCollection<DataObjectKind.Consumer, Guid> GetConsumerOrderingForUladApplication(DataObjectIdentifier<DataObjectKind.UladApplication, Guid> uladAppId);

        /// <summary>
        /// Gets the entity order for the given data object kind and legacy application.
        /// </summary>
        /// <typeparam name="TValueKind">The data object kind.</typeparam>
        /// <param name="appId">The legacy application id.</param>
        /// <returns>The ordering for the given data object kind and legacy application.</returns>
        IOrderedIdentifierCollection<TValueKind, Guid> GetOrderForApplication<TValueKind>(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId) where TValueKind : DataObjectKind;

        /// <summary>
        /// Gets the item ordering for a given consumer.
        /// </summary>
        /// <typeparam name="TValueKind">The type of the data object for which an order needs to be retrieved.</typeparam>
        /// <param name="consumerId">The consumer id.</param>
        /// <returns>The item ordering.</returns>
        IOrderedIdentifierCollection<TValueKind, Guid> GetOrderForConsumer<TValueKind>(DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId) where TValueKind : DataObjectKind;

        /// <summary>
        /// Associates a liability with a real property.
        /// </summary>
        /// <remarks>
        /// THIS APPEARS TO BE SUPERFLUOUS AS THERE IS ALREADY A GENERATED METHOD AddAssociation THAT DOES THIS, ALTHOUGH THE IMPLEMENTATION OF THIS DOES SOME CHECKING.
        /// </remarks>
        /// <param name="liabilityId">The liability id.</param>
        /// <param name="realPropertyId">The real property id.</param>
        void AssociateLiabilityWithRealProperty(
            DataObjectIdentifier<DataObjectKind.Liability, Guid> liabilityId,
            DataObjectIdentifier<DataObjectKind.RealProperty, Guid> realPropertyId);
        
        /// <summary>
        /// Gets the identifier for the real property record associated with the given
        /// liability id.
        /// </summary>
        /// <param name="liabilityId">The id of the liability record.</param>
        /// <returns>The id of the associated real property or Guid.Empty if there isn't one.</returns>
        DataObjectIdentifier<DataObjectKind.RealProperty, Guid>? GetRealPropertyIdLinkedToLiability(DataObjectIdentifier<DataObjectKind.Liability, Guid> liabilityId);

        /// <summary>
        /// Gets a value indicating whether the real property is marked as the subject property.
        /// </summary>
        /// <param name="realPropertyId">The id of the real property.</param>
        /// <returns>A value indicating whether the property is marked as the subject property.</returns>
        /// <exception cref="DeveloperException">Thrown when a real property with the specified id does not exist.</exception>
        bool IsRealPropertySubjectProperty(DataObjectIdentifier<DataObjectKind.RealProperty, Guid> realPropertyId);

        /// <summary>
        /// Remove a borrower from the application.
        /// </summary>
        /// <param name="appId">The application id.</param>
        void RemoveCoborrowerFromLegacyApplication(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId);

        /// <summary>
        /// Remove a borrower record and relevant associations.
        /// </summary>
        /// <param name="consumerId">The consumer id.</param>
        void RemoveBorrower(DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId);

        /// <summary>
        /// Remove the legacy application, which automatically removes the borrowers.
        /// </summary>
        /// <param name="appId">The application id.</param>
        void RemoveLegacyApplication(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId);

        /// <summary>
        /// Remove the ULAD application and any borrowers associated with the application.
        /// </summary>
        /// <param name="uladApplicationId">The identifier for the ULAD application.</param>
        void RemoveUladApplication(DataObjectIdentifier<DataObjectKind.UladApplication, Guid> uladApplicationId);

        /// <summary>
        /// Sets a value indicating that the liability is the subject property 1st mortgage.
        /// </summary>
        /// <param name="id">The liability id.</param>
        /// <param name="liability">The liability.</param>
        /// <param name="value">The value to set.</param>
        void SetIsSp1stMtg(DataObjectIdentifier<DataObjectKind.Liability, Guid> id, Liability liability, bool value);

        /// <summary>
        /// Sets a value indicating whether the liability is for the first mortgage of the subject property.
        /// </summary>
        /// <param name="id">The liability id.</param>
        /// <param name="liability">The liability.</param>
        /// <param name="value">The value to set.</param>
        void SetIsSpMtg(DataObjectIdentifier<DataObjectKind.Liability, Guid> id, Liability liability, bool value);

        /// <summary>
        /// Set the primary borrower for the ULAD application.
        /// </summary>
        /// <param name="consumerId">The identifier for the new primary borrower.</param>
        void SetPrimaryBorrowerOnUladApplication(DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId);

        /// <summary>
        /// Set the primary borrower for the ULAD application.
        /// </summary>
        /// <param name="uladId">The identifier for the ULAD application.</param>
        /// <param name="consumerId">The identifier for the new primary borrower.</param>
        void SetPrimaryBorrowerOnUladApplication(DataObjectIdentifier<DataObjectKind.UladApplication, Guid> uladId, DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId);

        /// <summary>
        /// Set the primary legacy application for the loan.
        /// </summary>
        /// <param name="appId">The identifier for the new primary legacy application.</param>
        void SetPrimaryLegacyApplication(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId);

        /// <summary>
        /// Set the primary ULAD application for the loan.
        /// </summary>
        /// <param name="id">The identifier for the new primary ULAD application.</param>
        void SetPrimaryUladApplication(DataObjectIdentifier<DataObjectKind.UladApplication, Guid> id);

        /// <summary>
        /// Swap the borrower and coborrower for an application.
        /// </summary>
        /// <param name="appId">The identifier for the legacy application.</param>
        void SwapBorrowers(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId);

        /// <summary>
        /// Clear all the primary income sources associated with the given consumer.
        /// </summary>
        /// <param name="consumerId">The consumer id whose income sources should be deleted.</param>
        void ClearPrimaryIncomeSourcesForConsumer(DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId);
    }
}
