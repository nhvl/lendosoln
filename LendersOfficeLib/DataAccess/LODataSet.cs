using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using LendersOffice.Drivers.SqlServerDB;
using LqbGrammar.DataTypes;
using LqbGrammar.Drivers;

namespace DataAccess
{
    public class CDataSet : DataSet
    {
        const int WriteThresholdInMs = 1000;
        const int ReadThresholdInMs = 1000;

        protected DbDataAdapter m_da;
        private DbCommandBuilder m_cb;
        const string myTableName = "Product_Price_Association";
        protected int m_timeOut = -1;

        public CDataSet() : base()
        {
        }

        public CDataSet(int timeOut) : base()
        {
            m_timeOut = timeOut;
        }

        public void LoadWithSqlQueryDangerously(Guid brokerId, string sqlQuery, IEnumerable<SqlParameter> parameters)
        {
            LoadWithSqlQueryDangerouslyImpl(brokerId, DataSrc.LOShare, sqlQuery, parameters);
        }

        public void LoadWithSqlQueryDangerously(DataSrc dataSrc, string sqlQuery)
        {
            LoadWithSqlQueryDangerouslyImpl(Guid.Empty, dataSrc, sqlQuery, (IEnumerable<SqlParameter>)null);
        }

        public void LoadWithSqlQueryDangerously(DataSrc dataSrc, string sqlQuery, ArrayList sqlParameters)
        {
            List<SqlParameter> list = null;
            if (sqlParameters != null)
            {
                list = new List<SqlParameter>();
                foreach (SqlParameter p in sqlParameters)
                {
                    list.Add(p);
                }

            }
            LoadWithSqlQueryDangerously(dataSrc, sqlQuery, list);
        }

        private DbConnection GetConnection(Guid brokerId, DataSrc dataSrc)
        {
            if (brokerId != Guid.Empty)
            {
                return DbAccessUtils.GetConnection(brokerId, shouldProfile: false);
            }
            else
            {
                return DbAccessUtils.GetConnection(dataSrc, shouldProfile: false);
            }
        }

        public void LoadWithSqlQueryDangerously(DataSrc dataSrc, string sqlQuery, IEnumerable<SqlParameter> sqlParameters)
        {
            LoadWithSqlQueryDangerouslyImpl(Guid.Empty, dataSrc, sqlQuery, sqlParameters);
        }

        private void LoadWithSqlQueryDangerouslyImpl(Guid brokerId, DataSrc dataSrc, string sqlQuery, IEnumerable<SqlParameter> sqlParameters)
        {
            DbConnection conn = null;
            try
            {
                var factory = DbAccessUtils.GetDbProvider(shouldProfile: false);
                conn = GetConnection(brokerId, dataSrc);
                var command = conn.CreateCommand();
                command.CommandText = sqlQuery;
                m_da = factory.CreateDataAdapter();
                m_da.SelectCommand = command;

                if (sqlParameters != null && sqlParameters.Count() > 0)
                {
                    foreach (SqlParameter p in sqlParameters)
                    {
                        m_da.SelectCommand.Parameters.Add(p);
                    }
                }

                if (m_timeOut >= 0)
                {
                    m_da.SelectCommand.CommandTimeout = m_timeOut;
                }

                m_cb = factory.CreateCommandBuilder();

                if (m_da is LendersOffice.ObjLib.Profiling.PerformanceMonitor.PerformanceMonitorDbDataAdapter)
                {
                    m_cb.DataAdapter = ((LendersOffice.ObjLib.Profiling.PerformanceMonitor.PerformanceMonitorDbDataAdapter)m_da).InternalAdapter;
                }
                else
                {
                    m_cb.DataAdapter = m_da;
                }

                var sqlDriver = CreateSqlDriver(this.m_timeOut);
                var tableName = DBTableName.Create(myTableName);
                sqlDriver.FillDataSet(this.m_da, this, tableName);
            }
            finally
            {
                conn?.Close();
            }
        }

        //ThienChange : will review later.
        virtual protected void PrepareUpdateStatement(DbDataAdapter da, DataTable table)
        {

        }

        public int Save()
        {
            if (Tables[myTableName] == null)
                return 0;

            PrepareUpdateStatement(m_da, Tables[myTableName]);
            return UpdateData(this);
        }

        public static int UpdateData(CDataSet ds)
        {
            if (ds.m_da.UpdateCommand == null)
            {
                ds.m_da.UpdateCommand = ds.m_cb.GetUpdateCommand();
            }
            
            if (ds.m_da.UpdateCommand.Connection == null)
            {
                ds.m_da.UpdateCommand.Connection = ds.m_da.SelectCommand.Connection;
            }

            DBTableName? tableName = DBTableName.Create(myTableName);

            var sqlDriver = CreateSqlDriver(ds.m_timeOut);

            try
            {
                var rowCount = sqlDriver.UpdateDataSet(ds.m_da, ds, tableName);
                return rowCount.Value;
            }
            finally
            {
                // OPM 453484.  Current implementation of UpdateDataSet will open the connection,
                // leaving it to the caller to close or dispose the connection or adapter.
                // To avoid connection leak, it must be manually closed it here.
                if ((ds.m_da.UpdateCommand != null) && (ds.m_da.UpdateCommand.Connection != null) && (ds.m_da.UpdateCommand.Connection.State == ConnectionState.Open))
                {
                    ds.m_da.UpdateCommand.Connection.Close();
                }
            }
        }

		private static ISqlDriver CreateSqlDriver(int desiredTimeout)
		{
			var timeout = TimeoutInSeconds.Default;
			if (desiredTimeout == 30)
			{
				timeout = TimeoutInSeconds.Thirty;
			}
			else if (desiredTimeout == 60)
			{
				timeout = TimeoutInSeconds.Sixty;
			}
			else if (desiredTimeout > 0)
			{
                var check = TimeoutInSeconds.Create(desiredTimeout);
                if (check == null)
                {
                    timeout = TimeoutInSeconds.Default;
                }
                else
                {
                    timeout = check.Value;
                }
			}

			return SqlServerHelper.CreateSpecificDriver(timeout, SqlDecoratorType.StopWatch);
		}
    }
}