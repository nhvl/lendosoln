﻿// <copyright file="DownPaymentGiftSet.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Antonio Valencia
//  Date:   3/12/2015 4:31:23 PM
// </summary>
namespace DataAccess
{
    using System.Collections;
    using System.Collections.Generic;
    using LendersOffice.Common;
    using Newtonsoft.Json;

    /// <summary>
    /// Represents a set of Down Payments Gifts. 
    /// </summary>
    public class DownPaymentGiftSet : IEnumerable<DownPaymentGift>
    {
        /// <summary>
        /// The down payments used for calculations. 
        /// </summary>
        private List<DownPaymentGift> gifts;

        /// <summary>
        /// The convertor used to turn a string into money or vice versa.
        /// </summary>
        private LosConvert convert;

        /// <summary>
        /// Initializes a new instance of the <see cref="DownPaymentGiftSet" /> class.
        /// </summary>
        /// <param name="convert">The convertor for string to money.</param>
        /// <param name="json">The json representation.</param>
        internal DownPaymentGiftSet(LosConvert convert, string json)
        {
            this.convert = convert;
            this.ImportImpl(json, true);
        }

        /// <summary>
        /// Imports the given JSON into the object. It will sanitize the amount value.
        /// </summary>
        /// <param name="json">The JSON containing the down payment information.</param>
        public void Import(string json)
        {
            this.ImportImpl(json, false);
        }

        /// <summary>
        /// Adds a collection of DownPaymentGift into the object.
        /// </summary>
        /// <param name="downPaymentGifts">The DownPaymentGift collection.</param>
        public void Import(IEnumerable<DownPaymentGift> downPaymentGifts)
        {
            if (downPaymentGifts != null)
            {
                this.gifts.AddRange(downPaymentGifts);
            }
        }

        /// <summary>
        /// Clears all the gift funds entries.
        /// </summary>
        public void Clear()
        {
            this.gifts.Clear();
        }

        /// <summary>
        /// Gets the JSON representation for storage.
        /// </summary>
        /// <returns>The JSON representation of the set.</returns>
        public string Serialize()
        {
            JsonSerializerSettings settings = SerializationHelper.GetJsonNetSerializerSettings();
            settings.ContractResolver = new DownPaymentGiftContractResolver(false);
            return SerializationHelper.JsonNetSerialize(this.gifts, settings);
        }

        /// <summary>
        /// Gets an <code>enumerator</code> for the down payments.
        /// </summary>
        /// <returns><code>An enumerator</code> for all the down payment gifts.</returns>
        IEnumerator<DownPaymentGift> IEnumerable<DownPaymentGift>.GetEnumerator()
        {
            return this.gifts.GetEnumerator();
        }

        /// <summary>
        /// Gets an enumerator for the down payments.
        /// </summary>
        /// <returns>An enumerator for all the down payment gifts.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.gifts.GetEnumerator();
        }

        /// <summary>
        /// Serializes the Down Payment Gift Fund set for storage.
        /// </summary>
        /// <returns>A JSON string representing the gift fund set.</returns>
        internal string SerializeForStorage()
        {
            JsonSerializerSettings settings = SerializationHelper.GetJsonNetSerializerSettings();
            settings.ContractResolver = new DownPaymentGiftContractResolver(true);
            return SerializationHelper.JsonNetSerialize(this.gifts, settings);
        }

        /// <summary>
        /// Imports the given JSON into the object. If isFromStorage is true it will convert the amount
        /// to money back to the rep. Otherwise it will sanitize the rep.
        /// </summary>
        /// <param name="json">The json containing the down payment information.</param>
        /// <param name="isFromStorage">Indicating whether the json came from the DB.</param>
        private void ImportImpl(string json, bool isFromStorage)
        {
            if (string.IsNullOrEmpty(json))
            {
                this.gifts = new List<DownPaymentGift>();
                return;
            }

            this.gifts = SerializationHelper.JsonNetDeserialize<List<DownPaymentGift>>(json);

            foreach (var gift in this.gifts)
            {
                if (isFromStorage)
                {
                    gift.Amount_rep = this.convert.ToMoneyString(gift.Amount, FormatDirection.ToRep);
                }
                else
                {
                    gift.Amount = this.convert.ToMoney(gift.Amount_rep);
                    gift.Amount_rep = this.convert.ToMoneyString(gift.Amount, FormatDirection.ToRep);
                }
            }
        }
    }
}
