﻿// <copyright file="DownPaymentGift.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Antonio Valencia
//  Date:   3/12/2015 10:13:12 AM
// </summary>
namespace DataAccess
{
    using Newtonsoft.Json;

    /// <summary>
    /// DownPaymentGift represents an amount given as a gift to be used as a down payment.
    /// It tracks the source, reasoning and amount.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class DownPaymentGift
    {
        /// <summary>
        /// Gets or sets the down payment source. 
        /// </summary>
        /// <value>The source of the down payment.</value>
        [JsonProperty("s")]
        public string Source { get; set; }

        /// <summary>
        /// Gets or sets the explanation for the down payment.
        /// </summary>
        /// <value>An explanation for the amount.</value>
        [JsonProperty("e")]
        public string Explanation { get; set; }

        /// <summary>
        /// Gets or sets the internal down payment amount. This field is mainly for serialization purposes.
        /// </summary>
        /// <value>The down payment amount.</value>
        [JsonProperty("b")]
        public string Amount_rep { get; set; }

        /// <summary>
        /// Gets or sets the amount of the down payment.
        /// </summary>
        /// <value>The down payment amount.</value>
        [JsonProperty("a")]
        public decimal Amount { get; set; }
    }
}