﻿namespace DataAccess.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Roles;
    using LendersOffice.Security;

    /// <summary>
    /// Services for Employee Role page.
    /// </summary>
    public class EmployeeRole
    {
        /// <summary>
        /// The Search Result enum.
        /// </summary>
        private const string SearchResult = "SearchResult";

        /// <summary>
        /// The OriginationCompanyLimitingRoleDesc enum.
        /// </summary>
        private const string OriginationCompanyLimitingRoleDesc = "OriginationCompanyLimitingRoleDesc";

        /// <summary>
        /// The OrigRoleDesc enum.
        /// </summary>
        private const string OrigRoleDesc = "OrigRoleDesc";

        /// <summary>
        /// Copy To Official Contact checkbox is Enabled or not.
        /// </summary>
        private const string CopyToOfficialContactEnabledCheckbox = "CopyToOfficialContactEnabledCheckbox";

        /// <summary>
        /// Copy To Official Contact is Checked or not.
        /// </summary>
        private const string CopyToOfficialContactChecked = "CopyToOfficialContactChecked";

        /// <summary>
        /// Pml Broker Id Filter value.
        /// </summary>
        private const string PmlBrokerIdFilter = "PmlBrokerIdFilter";

        /// <summary>
        /// Clear Assignment.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="copyToOfficialAgent">Is copy to Official Agent.</param>
        /// <param name="role">The Agent Role.</param>
        /// <returns>The Error List if exists.</returns>
        public static List<string> ClearAssignment(Guid loanId, bool copyToOfficialAgent, string role)
        {
            try
            {
                return AssignEmployee(Guid.Empty, string.Empty, string.Empty, false, loanId, copyToOfficialAgent, role);
            }
            catch (LoanFieldWritePermissionDenied ex)
            {
                return new List<string>() { ex.UserMessage };
            }
            catch (PageDataAccessDenied)
            {
                return new List<string>() { "Access denied. You do not have permission to clear role." };
            }
            catch (Exception)
            {
                return new List<string>() { "Failed to clear role." };
            }            
        }

        /// <summary>
        /// Assign To Employee.
        /// </summary>
        /// <param name="employeeID">Employee ID.</param>
        /// <param name="email">Email Address.</param>
        /// <param name="employeeName">Employee Name.</param>
        /// <param name="pmlUser">PML User Value.</param>
        /// <param name="loanId">Loan ID Value.</param>
        /// <param name="copyToOfficialAgent">Copy To Official Agent.</param>
        /// <param name="role">The Role Value.</param>
        /// <returns>The list of error if exists.</returns>
        [SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "Retrying on error.")]
        public static List<string> AssignEmployee(Guid employeeID, string email, string employeeName, bool pmlUser, Guid loanId, bool copyToOfficialAgent, string role)
        {
            // Calculate loan assignment delta and send out email
            // notifications to every affected user who currently
            // wants to recieve emails.
            //
            // 8/16/2004 kb - We now process role change notification
            // in the background.  All we do here is track the delta
            // based on who is being changed.  Note: We use the set
            // id property after it's set to retrieve the previous.
            // This works because the previous remains after setting.
            CPageData dataLoan = new CEmployeeDataWithAccessControl(loanId);

            List<string> errorList = new List<string>();
            Guid roleId = Guid.Empty;
            Guid prevId = Guid.Empty;
            bool hasErrorToShow = false;
            var brokerUser = PrincipalFactory.CurrentPrincipal;
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAgentFields agent = null;
            try
            {
                switch (role)
                {
                    case ConstApp.ROLE_LOAN_OFFICER:
                        // Change branch to that of loan officer - OPM 51224
                        if (employeeID != Guid.Empty)
                        {
                            EmployeeDB emp = new EmployeeDB(employeeID, brokerUser.BrokerId);
                            emp.Retrieve();

                            if (emp.UserType == 'B' && dataLoan.sEmployeeExternalSecondaryId != Guid.Empty)
                            {
                                // throw new Exception("Unable to assign internal loan officer when external secondary is assigned.");
                                return new List<string>() { "Unable to assign internal loan officer when external secondary is assigned." };
                            }

                            if (dataLoan.sBranchId != emp.BranchID &&
                                dataLoan.sBranchChannelT != E_BranchChannelT.Correspondent)
                            {
                                return WarningLO(emp, null, null, copyToOfficialAgent);
                            }
                            else
                            {
                                dataLoan.sEmployeeLoanRepId = employeeID;
                                roleId = CEmployeeFields.s_LoanRepRoleId;
                                prevId = dataLoan.sEmployeeLoanRepId;
                                if (copyToOfficialAgent)
                                {
                                    agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
                                    agent.ClearExistingData();
                                }

                                // 10/13/2010 dd - OPM 41485 - When user assign loan officer role recalculate Tpo bit.                            
                                dataLoan.RecalculateTpoValue();
                            }
                        }
                        else
                        {
                            dataLoan.sEmployeeLoanRepId = employeeID;
                            roleId = CEmployeeFields.s_LoanRepRoleId;
                            prevId = dataLoan.sEmployeeLoanRepId;
                            if (copyToOfficialAgent)
                            {
                                agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
                                agent.ClearExistingData();
                            }

                            dataLoan.RecalculateTpoValue();
                        }

                        break;
                    case ConstApp.ROLE_PROCESSOR:
                        dataLoan.sEmployeeProcessorId = employeeID;
                        roleId = CEmployeeFields.s_ProcessorRoleId;
                        prevId = dataLoan.sEmployeeProcessorId;
                        if (copyToOfficialAgent)
                        {
                            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Processor, E_ReturnOptionIfNotExist.CreateNew);
                        }

                        break;
                    case ConstApp.ROLE_MANAGER:
                        dataLoan.sEmployeeManagerId = employeeID;
                        roleId = CEmployeeFields.s_ManagerRoleId;
                        prevId = dataLoan.sEmployeeManagerId;
                        if (copyToOfficialAgent)
                        {
                            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Manager, E_ReturnOptionIfNotExist.CreateNew);
                        }

                        break;
                    case ConstApp.ROLE_REAL_ESTATE_AGENT:
                        dataLoan.sEmployeeRealEstateAgentId = employeeID;
                        roleId = CEmployeeFields.s_RealEstateAgentRoleId;
                        prevId = dataLoan.sEmployeeRealEstateAgentId;
                        if (copyToOfficialAgent)
                        {
                            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Realtor, E_ReturnOptionIfNotExist.CreateNew);
                        }

                        break;
                    case ConstApp.ROLE_CALL_CENTER_AGENT:
                        dataLoan.sEmployeeCallCenterAgentId = employeeID;
                        roleId = CEmployeeFields.s_CallCenterAgentRoleId;
                        prevId = dataLoan.sEmployeeCallCenterAgentId;
                        if (copyToOfficialAgent)
                        {
                            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.CallCenterAgent, E_ReturnOptionIfNotExist.CreateNew);
                        }

                        break;
                    case ConstApp.ROLE_LENDER_ACCOUNT_EXEC:
                        dataLoan.sEmployeeLenderAccExecId = employeeID;
                        roleId = CEmployeeFields.s_LenderAccountExecRoleId;
                        prevId = dataLoan.sEmployeeLenderAccExecId;

                        break;
                    case ConstApp.ROLE_LOCK_DESK:
                        dataLoan.sEmployeeLockDeskId = employeeID;
                        roleId = CEmployeeFields.s_LockDeskRoleId;
                        prevId = dataLoan.sEmployeeLockDeskId;

                        break;
                    case ConstApp.ROLE_UNDERWRITER:
                        dataLoan.sEmployeeUnderwriterId = employeeID;
                        roleId = CEmployeeFields.s_UnderwriterRoleId;
                        prevId = dataLoan.sEmployeeUnderwriterId;
                        if (copyToOfficialAgent)
                        {
                            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Underwriter, E_ReturnOptionIfNotExist.CreateNew);
                        }

                        break;
                    case ConstApp.ROLE_LOAN_OPENER:
                        dataLoan.sEmployeeLoanOpenerId = employeeID;
                        roleId = CEmployeeFields.s_LoanOpenerId;
                        prevId = dataLoan.sEmployeeLoanOpenerId;
                        if (copyToOfficialAgent)
                        {
                            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOpener, E_ReturnOptionIfNotExist.CreateNew);
                        }

                        break;
                    case ConstApp.ROLE_CLOSER:
                        dataLoan.sEmployeeCloserId = employeeID;
                        roleId = CEmployeeFields.s_CloserId;
                        prevId = dataLoan.sEmployeeCloserId;
                        break;

                    case ConstApp.ROLE_BROKERPROCESSOR:
                        dataLoan.sEmployeeBrokerProcessorId = employeeID;
                        roleId = CEmployeeFields.s_BrokerProcessorId;
                        prevId = dataLoan.sEmployeeBrokerProcessorId;
                        if (copyToOfficialAgent)
                        {
                            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.BrokerProcessor, E_ReturnOptionIfNotExist.CreateNew);
                            agent.ClearExistingData();
                        }

                        break;
                    case ConstApp.ROLE_SHIPPER:
                        dataLoan.sEmployeeShipperId = employeeID;
                        roleId = CEmployeeFields.s_ShipperId;
                        prevId = dataLoan.sEmployeeShipperId;
                        if (copyToOfficialAgent)
                        {
                            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Shipper, E_ReturnOptionIfNotExist.CreateNew);
                            agent.ClearExistingData();
                        }

                        break;
                    case ConstApp.ROLE_FUNDER:
                        dataLoan.sEmployeeFunderId = employeeID;
                        roleId = CEmployeeFields.s_FunderRoleId;
                        prevId = dataLoan.sEmployeeFunderId;
                        if (copyToOfficialAgent)
                        {
                            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Funder, E_ReturnOptionIfNotExist.CreateNew);
                            agent.ClearExistingData();
                        }

                        break;
                    case ConstApp.ROLE_POSTCLOSER:
                        dataLoan.sEmployeePostCloserId = employeeID;
                        roleId = CEmployeeFields.s_PostCloserId;
                        prevId = dataLoan.sEmployeePostCloserId;
                        if (copyToOfficialAgent)
                        {
                            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.PostCloser, E_ReturnOptionIfNotExist.CreateNew);
                            agent.ClearExistingData();
                        }

                        break;
                    case ConstApp.ROLE_INSURING:
                        dataLoan.sEmployeeInsuringId = employeeID;
                        roleId = CEmployeeFields.s_InsuringId;
                        prevId = dataLoan.sEmployeeInsuringId;
                        if (copyToOfficialAgent)
                        {
                            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Insuring, E_ReturnOptionIfNotExist.CreateNew);
                            agent.ClearExistingData();
                        }

                        break;
                    case ConstApp.ROLE_COLLATERALAGENT:
                        dataLoan.sEmployeeCollateralAgentId = employeeID;
                        roleId = CEmployeeFields.s_CollateralAgentId;
                        prevId = dataLoan.sEmployeeCollateralAgentId;
                        if (copyToOfficialAgent)
                        {
                            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.CollateralAgent, E_ReturnOptionIfNotExist.CreateNew);
                            agent.ClearExistingData();
                        }

                        break;
                    case ConstApp.ROLE_DOCDRAWER:
                        dataLoan.sEmployeeDocDrawerId = employeeID;
                        roleId = CEmployeeFields.s_DocDrawerId;
                        prevId = dataLoan.sEmployeeDocDrawerId;
                        if (copyToOfficialAgent)
                        {
                            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.DocDrawer, E_ReturnOptionIfNotExist.CreateNew);
                            agent.ClearExistingData();
                        }

                        break;
                    case ConstApp.ROLE_CREDITAUDITOR:
                        dataLoan.sEmployeeCreditAuditorId = employeeID;
                        roleId = CEmployeeFields.s_CreditAuditorId;
                        prevId = dataLoan.sEmployeeCreditAuditorId;
                        if (copyToOfficialAgent)
                        {
                            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.CreditAuditor, E_ReturnOptionIfNotExist.CreateNew);
                            agent.ClearExistingData();
                        }

                        break;
                    case ConstApp.ROLE_DISCLOSUREDESK:
                        dataLoan.sEmployeeDisclosureDeskId = employeeID;
                        roleId = CEmployeeFields.s_DisclosureDeskId;
                        prevId = dataLoan.sEmployeeDisclosureDeskId;
                        if (copyToOfficialAgent)
                        {
                            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.DisclosureDesk, E_ReturnOptionIfNotExist.CreateNew);
                            agent.ClearExistingData();
                        }

                        break;
                    case ConstApp.ROLE_JUNIORPROCESSOR:
                        dataLoan.sEmployeeJuniorProcessorId = employeeID;
                        roleId = CEmployeeFields.s_JuniorProcessorId;
                        prevId = dataLoan.sEmployeeJuniorProcessorId;
                        if (copyToOfficialAgent)
                        {
                            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.JuniorProcessor, E_ReturnOptionIfNotExist.CreateNew);
                            agent.ClearExistingData();
                        }

                        break;
                    case ConstApp.ROLE_JUNIORUNDERWRITER:
                        dataLoan.sEmployeeJuniorUnderwriterId = employeeID;
                        roleId = CEmployeeFields.s_JuniorUnderwriterId;
                        prevId = dataLoan.sEmployeeJuniorUnderwriterId;
                        if (copyToOfficialAgent)
                        {
                            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.JuniorUnderwriter, E_ReturnOptionIfNotExist.CreateNew);
                            agent.ClearExistingData();
                        }

                        break;
                    case ConstApp.ROLE_LEGALAUDITOR:
                        dataLoan.sEmployeeLegalAuditorId = employeeID;
                        roleId = CEmployeeFields.s_LegalAuditorId;
                        prevId = dataLoan.sEmployeeLegalAuditorId;
                        if (copyToOfficialAgent)
                        {
                            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LegalAuditor, E_ReturnOptionIfNotExist.CreateNew);
                            agent.ClearExistingData();
                        }

                        break;
                    case ConstApp.ROLE_LOANOFFICERASSISTANT:
                        dataLoan.sEmployeeLoanOfficerAssistantId = employeeID;
                        roleId = CEmployeeFields.s_LoanOfficerAssistantId;
                        prevId = dataLoan.sEmployeeLoanOfficerAssistantId;
                        if (copyToOfficialAgent)
                        {
                            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficerAssistant, E_ReturnOptionIfNotExist.CreateNew);
                            agent.ClearExistingData();
                        }

                        break;
                    case ConstApp.ROLE_PURCHASER:
                        dataLoan.sEmployeePurchaserId = employeeID;
                        roleId = CEmployeeFields.s_PurchaserId;
                        prevId = dataLoan.sEmployeePurchaserId;
                        if (copyToOfficialAgent)
                        {
                            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Purchaser, E_ReturnOptionIfNotExist.CreateNew);
                            agent.ClearExistingData();
                        }

                        break;
                    case ConstApp.ROLE_QCCOMPLIANCE:
                        dataLoan.sEmployeeQCComplianceId = employeeID;
                        roleId = CEmployeeFields.s_QCComplianceId;
                        prevId = dataLoan.sEmployeeQCComplianceId;
                        if (copyToOfficialAgent)
                        {
                            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.QCCompliance, E_ReturnOptionIfNotExist.CreateNew);
                            agent.ClearExistingData();
                        }

                        break;
                    case ConstApp.ROLE_SECONDARY:
                        dataLoan.sEmployeeSecondaryId = employeeID;
                        roleId = CEmployeeFields.s_SecondaryId;
                        prevId = dataLoan.sEmployeeSecondaryId;
                        if (copyToOfficialAgent)
                        {
                            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Secondary, E_ReturnOptionIfNotExist.CreateNew);
                            agent.ClearExistingData();
                        }

                        break;
                    case ConstApp.ROLE_SERVICING:
                        dataLoan.sEmployeeServicingId = employeeID;
                        roleId = CEmployeeFields.s_ServicingId;
                        prevId = dataLoan.sEmployeeServicingId;
                        if (copyToOfficialAgent)
                        {
                            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Servicing, E_ReturnOptionIfNotExist.CreateNew);
                            agent.ClearExistingData();
                        }

                        break;
                    case ConstApp.ROLE_EXTERNAL_SECONDARY:
                        if (employeeID != Guid.Empty)
                        {
                            if (dataLoan.sEmployeeLoanRepId != Guid.Empty)
                            {
                                var loanAssignmentContactTable = new LoanAssignmentContactTable(brokerUser.BrokerId, dataLoan.sLId);
                                var loanOfficer = loanAssignmentContactTable.FindByRoleT(E_RoleT.LoanOfficer);
                                if (loanOfficer != null && !loanOfficer.IsPmlUser)
                                {
                                    // throw new Exception("Unable to assign external secondary when internal loan officer is assigned.");
                                    return new List<string>() { "Unable to assign external secondary when internal loan officer is assigned." };
                                }
                            }

                            EmployeeDB emp = new EmployeeDB(employeeID, brokerUser.BrokerId);
                            emp.Retrieve();

                            // We want to reassign the branch when there is no LO assigned.
                            if (dataLoan.sBranchChannelT == E_BranchChannelT.Correspondent)
                            {
                                Guid branchToAssign = Guid.Empty;

                                if (dataLoan.sCorrespondentProcessT == E_sCorrespondentProcessT.MiniCorrespondent)
                                {
                                    branchToAssign = emp.MiniCorrespondentBranchID;
                                }
                                else
                                {
                                    branchToAssign = emp.CorrespondentBranchID;
                                }

                                if (branchToAssign != Guid.Empty && branchToAssign != dataLoan.sBranchId)
                                {
                                    return WarningLO(emp, E_RoleT.Pml_Secondary, branchToAssign, copyToOfficialAgent);
                                }
                            }
                        }

                        dataLoan.sEmployeeExternalSecondaryId = employeeID;
                        roleId = CEmployeeFields.s_ExternalSecondaryId;
                        prevId = dataLoan.sEmployeeExternalSecondaryId;

                        if (copyToOfficialAgent)
                        {
                            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.ExternalSecondary, E_ReturnOptionIfNotExist.CreateNew);
                            agent.ClearExistingData();
                        }

                        dataLoan.RecalculateTpoValue();
                        break;
                    case ConstApp.ROLE_EXTERNAL_POST_CLOSER:
                        dataLoan.sEmployeeExternalPostCloserId = employeeID;
                        roleId = CEmployeeFields.s_ExternalPostCloserId;
                        prevId = dataLoan.sEmployeeExternalPostCloserId;

                        if (copyToOfficialAgent)
                        {
                            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.ExternalPostCloser, E_ReturnOptionIfNotExist.CreateNew);
                            agent.ClearExistingData();
                        }

                        break;
                    default:
                        throw new CBaseException(ErrorMessages.Generic, "Invalid role:  '" + role + "'.");
                }
            }
            catch (CBaseException ex)
            {
                return new List<string>() { ex.UserMessage };
            }

            if (hasErrorToShow == false)
            {
                // Save the changes to the loan.  We update the cache table
                // under the hood.
                try
                {
                    if (agent != null && copyToOfficialAgent)
                    {
                        agent.ClearExistingData();

                        // 9/21/2006 dd - Retrieve more information on agent.
                        if (employeeID != Guid.Empty)
                        {
                            bool copyCommissionToAgent = CommonFunctions.GetEmployeeRoleWithCommissionInLoanFile(employeeID, loanId) == E_AgentRoleT.Other;
                            CommonFunctions.CopyEmployeeInfoToAgent(dataLoan.sBrokerId, agent, employeeID, copyCommissionToAgent);
                        }

                        agent.Update();
                    }

                    try
                    {
                        dataLoan.Save();

                        // CommonFunctions.FixLoanTasksOnRoleAssignement(dataLoan.sBrokerId, RequestHelper.LoanID, employeeID, roleId);
                        RoleChange.Mark(loanId, brokerUser.BrokerId, dataLoan, prevId, employeeID, roleId, E_RoleChangeT.Update);
                    }
                    catch (LoanFieldWritePermissionDenied)
                    {
                        hasErrorToShow = true;
                        errorList.Add("Cannot update the loan assignment due to workflow.");
                    }
                }
                catch (AccessDenied e)
                {
                    errorList.Add(string.Format("{0}: Access denied.", dataLoan.sLNm));

                    hasErrorToShow = true;

                    Tools.LogError("Employee role change: Failed to save and mark role change.  Access denied.", e);
                }
                catch (CBaseException e)
                {
                    errorList.Add(e.UserMessage);
                    hasErrorToShow = true;
                }
                catch (Exception e)
                {
                    Tools.LogError("Employee role change: Failed to save and mark role change.", e);
                }
            }

            if (hasErrorToShow == true)
            {
                errorList.Add(string.Format("Changes to file {0} are disregarded.", dataLoan.sLNm));
            }

            return errorList;
        }

        /// <summary>
        /// Assign LO And Branch.
        /// </summary>
        /// <param name="loanId">Loan ID Value.</param>
        /// <param name="loanofficerId">Loan Officer ID.</param>
        /// <param name="branchId">Branch ID Value.</param>
        /// <param name="copyToOfficialAgent">Copy To Official Agent.</param>
        /// <param name="loanOfficerName">Loan Officer Name.</param>
        /// <param name="loanOfficerEmail">Loan Officer Email.</param>
        /// <param name="roleTStr">The roleT String.</param>
        /// <returns>The error or status.</returns>
        public static string AssignLOAndBranch(Guid loanId, Guid loanofficerId, Guid branchId, bool copyToOfficialAgent, string loanOfficerName, string loanOfficerEmail, string roleTStr)
        {
            CPageData dataLoan = new CEmployeeDataWithAccessControl(loanId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            // Setting these roles may affect the assigned originating company.
            // Setting the originating company may perform different behavior
            // based on the branch information. So we need to assign the branch first :(.
            dataLoan.AssignBranch(branchId);

            var agentT = E_AgentRoleT.LoanOfficer;
            var roleT = (E_RoleT)Enum.Parse(typeof(E_RoleT), roleTStr);
            string roleDescription = LendersOffice.Security.Role.Get(roleT).ModifiableDesc;

            if (roleT == E_RoleT.LoanOfficer)
            {
                dataLoan.sEmployeeLoanRepId = loanofficerId;
            }
            else if (roleT == E_RoleT.Pml_Secondary)
            {
                dataLoan.sEmployeeExternalSecondaryId = loanofficerId;
                agentT = E_AgentRoleT.ExternalSecondary;
            }
            else
            {
                return "ERROR: Cannot assign " + roleDescription + " in this manner.";
            }

            if (copyToOfficialAgent)
            {
                CAgentFields agent = dataLoan.GetAgentOfRole(agentT, E_ReturnOptionIfNotExist.CreateNew);
                agent.ClearExistingData();

                bool copyCommissionToAgent = CommonFunctions.GetEmployeeRoleWithCommissionInLoanFile(loanofficerId, loanId) == E_AgentRoleT.Other;
                CommonFunctions.CopyEmployeeInfoToAgent(dataLoan.sBrokerId, agent, loanofficerId, copyCommissionToAgent);
                agent.Update();
            }

            dataLoan.RecalculateTpoValue();

            try
            {
                dataLoan.Save();
                return loanOfficerName + ',' + loanOfficerEmail;
            }
            catch (LoanFieldWritePermissionDenied)
            {
                return "ERROR: " + roleDescription + " cannot be reassigned due to workflow.";
            }
        }

        /// <summary>
        /// Search Broker Loan Assignment.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="origid">The origid value.</param>
        /// <param name="searchFilter">The search filter.</param>
        /// <param name="status">The status value.</param>
        /// <param name="pmlBrokerId">The pml broker ID.</param>
        /// <param name="role">The role value.</param>
        /// <param name="showMode">The show Mode.</param>
        /// <param name="isOnlyDisplayLqbUsers">Is Display LqbUsers.</param>
        /// <param name="isOnlyDisplayPmlUsers">Is Dislplay Pml Users.</param>
        /// <returns>The list of Broker Loan Assignment.</returns>
        [SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "Retrying on error.")]
        [SuppressMessage("LendingQBStyleCop.LendingQBCustomRules", "LB1001:DefaultSwitchCaseMustThrowException", Justification = "This is not an enumerated switch. Invalid keys should simply be skipped")]
        public static Dictionary<string, object> Search(Guid loanId, Guid origid, string searchFilter, int status, Guid pmlBrokerId, string role, string showMode, bool isOnlyDisplayLqbUsers, bool isOnlyDisplayPmlUsers)
        {
            var result = new Dictionary<string, object>();

            Guid m_pmlBrokerIdFilter = Guid.Empty;
            SetSearchValues(ref result, PmlBrokerIdFilter, m_pmlBrokerIdFilter);
            var brokerUser = PrincipalFactory.CurrentPrincipal;
            BrokerDB brokerDB = BrokerDB.RetrieveById(brokerUser.BrokerId);

            // We now check if the user has restricted assignment turned on.
            // With assignment restriction, the user can only assign an
            // employee that belongs to their branch.  The initial requirement
            // is to simplify processing of loans originated outside a central
            // processing center (hub) and assigned to a processor within
            // the central pool.  To maintain consistency in the pairing up
            // of shared processor with remote loan officers, we create
            // branches for each loan officer and add the processors that
            // they will use as internal employees of the officer's branch.
            // This way, remote officers won't get confused about who to
            // add to their loan.
            //
            // If you want to expose broker level employees as well, then
            // add that logic to the sproc (that's the central point for
            // retrieval and still generic enough -- passing the branch
            // as optional allows us to limit the result w/o implying too
            // much app-level logic).
            //
            // 8/2/2005 kb - Per case 2533, we will allow for searching
            // for employees by name matching.  To prevent large payloads,
            // we show the too much message when the number of employees
            // is great.  For this patch, we will limit this check to
            // only officer viewing.
            bool isSupervisor = role.ToLower() == "supervisor";
            bool isPmlRole = role == ConstApp.ROLE_BROKERPROCESSOR
                || role == ConstApp.ROLE_LOAN_OFFICER
                || isSupervisor
                || role == ConstApp.ROLE_EXTERNAL_SECONDARY
                || role == ConstApp.ROLE_EXTERNAL_POST_CLOSER;

            if (pmlBrokerId != Guid.Empty && isPmlRole)
            {
                 m_pmlBrokerIdFilter = origid;
                SetSearchValues(ref result, PmlBrokerIdFilter, m_pmlBrokerIdFilter);
            }

            BrokerLoanAssignmentTable roleTable = new BrokerLoanAssignmentTable();

            if (role.ToLower() == "agent")
            {
                // 8/4/2005 kb - We previously counted the number of employees
                // with a given role to decide whether or not to hide them
                // at start (see case 2541).  However, it's easier to just
                // check a broker bit.
                if (string.IsNullOrWhiteSpace(searchFilter) && brokerDB.HasManyUsers == true)
                {
                    // Too many employees to show.  Go to the help box.
                    SetSearchValues(ref result, CopyToOfficialContactEnabledCheckbox, false);
                    return result;
                }
            }

            CEmployeeData dataLoan = null;
            try
            {
                if (brokerUser.HasPermission(Permission.AllowLoanAssignmentsToAnyBranch) == false)
                {
                    E_BranchChannelT? branchChannelFilter = null;
                    E_sCorrespondentProcessT? correspondentProcessFilter = null;

                    // PML users have a broker branch, mini-correspondent branch,
                    // and correspondent branch. We need to filter based on the 
                    // correct branch, which requires looking at the channel and
                    // correspondent process type of the file.
                    if (loanId != Guid.Empty && isPmlRole && !isSupervisor)
                    {
                        // Bypass security to avoid any conflicts with workflow.
                        // We need to filter based on the correct branch and corr.
                        // process type regardless of user access.
                        dataLoan = new CEmployeeData(loanId);
                        dataLoan.InitLoad();

                        branchChannelFilter = dataLoan.sBranchChannelT;
                        correspondentProcessFilter = dataLoan.sCorrespondentProcessT;
                    }

                    roleTable.Retrieve(
                        brokerUser.BrokerId,
                        brokerUser.BranchId,
                        searchFilter,
                        role,
                        (E_EmployeeStatusFilterT)status,
                        branchChannelFilter,
                        correspondentProcessFilter);
                }
                else
                {
                    roleTable.Retrieve(
                        brokerUser.BrokerId,
                        Guid.Empty,
                        searchFilter,
                        role,
                        (E_EmployeeStatusFilterT)status,
                        null,
                        null);
                }

                if (loanId != Guid.Empty &&
                    (role == ConstApp.ROLE_BROKERPROCESSOR || role == ConstApp.ROLE_LOAN_OFFICER ||
                     role == ConstApp.ROLE_EXTERNAL_SECONDARY || role == ConstApp.ROLE_EXTERNAL_POST_CLOSER))
                {
                    LoanAccessInfo info = new LoanAccessInfo(loanId);

                    bool hasLoanOfficerAssigned = info.Assignments.HasAssignment(E_RoleT.LoanOfficer);
                    bool hasBrokerProcessorAssigned = info.Assignments.HasAssignment(E_RoleT.Pml_BrokerProcessor);
                    bool hasExternalSecondaryAssigned = info.Assignments.HasAssignment(E_RoleT.Pml_Secondary);
                    bool hasExternalPostCloserAssigned = info.Assignments.HasAssignment(E_RoleT.Pml_PostCloser);

                    bool isOriginatingCompanyConstrained =
                        (role != ConstApp.ROLE_LOAN_OFFICER && hasLoanOfficerAssigned) ||
                        (role != ConstApp.ROLE_BROKERPROCESSOR && hasBrokerProcessorAssigned) ||
                        (role != ConstApp.ROLE_EXTERNAL_SECONDARY && hasExternalSecondaryAssigned) ||
                        (role != ConstApp.ROLE_EXTERNAL_POST_CLOSER && hasExternalPostCloserAssigned);

                    var limitingRoles = new List<string>();

                    if (role != ConstApp.ROLE_LOAN_OFFICER && hasLoanOfficerAssigned)
                    {
                        limitingRoles.Add("Loan Officer");
                    }

                    if (role != ConstApp.ROLE_BROKERPROCESSOR && hasBrokerProcessorAssigned)
                    {
                        limitingRoles.Add("Processor (External)");
                    }

                    if (role != ConstApp.ROLE_EXTERNAL_SECONDARY && hasExternalSecondaryAssigned)
                    {
                        limitingRoles.Add("Secondary (External)");
                    }

                    if (role != ConstApp.ROLE_EXTERNAL_POST_CLOSER && hasExternalPostCloserAssigned)
                    {
                        limitingRoles.Add("Post-Closer (External)");
                    }

                    var limitingRolesDesc = string.Empty;

                    if (limitingRoles.Count == 1)
                    {
                        limitingRolesDesc = limitingRoles.First();
                    }
                    else if (limitingRoles.Count == 2)
                    {
                        limitingRolesDesc = string.Format(
                            "{0} and {1}",
                            limitingRoles[0],
                            limitingRoles[1]);
                    }
                    else if (limitingRoles.Count > 2)
                    {
                        limitingRolesDesc = string.Format(
                            "{0}, and {1}",
                            string.Join(", ", limitingRoles.Take(limitingRoles.Count - 1).ToArray()),
                            limitingRoles.Last());
                    }
                    
                    string roleToChooseDesc = null;

                    switch (role)
                    {
                        case ConstApp.ROLE_LOAN_OFFICER:
                            roleToChooseDesc = "loan officers";
                            break;
                        case ConstApp.ROLE_BROKERPROCESSOR:
                            roleToChooseDesc = "external processors";
                            break;
                        case ConstApp.ROLE_EXTERNAL_SECONDARY:
                            roleToChooseDesc = "external secondaries";
                            break;
                        case ConstApp.ROLE_EXTERNAL_POST_CLOSER:
                            roleToChooseDesc = "external post-closers";
                            break;
                        default:
                            break;
                    }

                    if (isOriginatingCompanyConstrained &&
                        !isOnlyDisplayLqbUsers)
                    {
                        SetSearchValues(ref result, OriginationCompanyLimitingRoleDesc, limitingRolesDesc);
                        SetSearchValues(ref result, OrigRoleDesc, roleToChooseDesc);
                        m_pmlBrokerIdFilter = info.sPmlBrokerId;
                        SetSearchValues(ref result, PmlBrokerIdFilter, m_pmlBrokerIdFilter);
                    }
                }
            }
            catch (Exception e)
            {
                // Oops!
                Tools.LogError("Failed to load employees in role.", e);
            }

            if (roleTable[role] != null)
            {
                // Get the corresponding AgentRoleT of the corresponding E_Role.  If the user does not have a contact associated with that agent role, check the
                // copy checkbox.
                E_AgentRoleT agentRole = LendersOffice.Security.Role.GetAgentRoleT(LendersOffice.Security.Role.Get(role).RoleT);
                if (dataLoan == null && !loanId.Equals(Guid.Empty))
                {
                    dataLoan = new CEmployeeData(loanId);
                    dataLoan.InitLoad();
                }

                if (dataLoan != null)
                {
                    CAgentFields agentFields = dataLoan.GetAgentOfRole(agentRole, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                    if (!agentFields.IsValid)
                    {
                        SetSearchValues(ref result, CopyToOfficialContactChecked, true);
                    }
                }

                List<BrokerLoanAssignmentTable.Spec> ls = new List<BrokerLoanAssignmentTable.Spec>();

                if (m_pmlBrokerIdFilter != Guid.Empty)
                {
                    foreach (BrokerLoanAssignmentTable.Spec spec in roleTable[role])
                    {
                        if (spec.PmlBrokerId == m_pmlBrokerIdFilter)
                        {
                            ls.Add(spec);
                        }
                    }

                    SetSearchValues(ref result, SearchResult, ls);
                    return result;
                }
                else
                {
                    if (isOnlyDisplayPmlUsers)
                    {
                        var pmlUsers = roleTable[role].Where(s => s.PmlBrokerId != Guid.Empty);

                        SetSearchValues(ref result, SearchResult, pmlUsers);
                        return result;
                    }
                    else if (isOnlyDisplayLqbUsers)
                    {
                        var lqbUsers = roleTable[role].Where(s => s.PmlBrokerId == Guid.Empty);

                        SetSearchValues(ref result, SearchResult, lqbUsers);
                        return result;
                    }
                    else
                    {
                        SetSearchValues(ref result, SearchResult, roleTable[role]);
                        return result;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Set Search values.
        /// </summary>
        /// <param name="list">The list of serch values.</param>
        /// <param name="key">The key to add.</param>
        /// <param name="value">The value to add.</param>
        private static void SetSearchValues(ref Dictionary<string, object> list, string key, object value)
        {
            if (list == null)
            {
                list = new Dictionary<string, object>();
            }

            if (list.ContainsKey(key))
            {
                list[key] = value;
            }
            else
            {
                list.Add(key, value);
            }
        }

        /// <summary>
        /// Set Warning Message.
        /// </summary>
        /// <param name="emp">Employee DB.</param>
        /// <param name="roleT">The roleT value.</param>
        /// <param name="branchId">The branch ID.</param>
        /// <param name="copyToOfficialAgent">Copy To Officical Agent.</param>
        /// <returns>The list of error if exists.</returns>
        private static List<string> WarningLO(EmployeeDB emp, E_RoleT? roleT, Guid? branchId, bool copyToOfficialAgent)
        {
            return new List<string>()
            {
                "WARNING",
                emp.ID.ToString(),
                branchId.HasValue ? branchId.ToString() : emp.BranchID.ToString(),
                copyToOfficialAgent.ToString(),
                emp.FullName,
                emp.Email,
                (roleT.HasValue ? roleT : E_RoleT.LoanOfficer).ToString()
            };
        }

        /// <summary>
        /// Records information to the audit history about the agent being deleted.
        /// </summary>
        /// <param name="dataLoan">The associated loan.</param>
        /// <param name="agent">A datarow representing an agent.</param>
        /// <param name="subjectPropertyState">The state in which the subject property is located.</param>
        private static void RecordAuditOnAgentDelete(CPageData dataLoan, DataRow agent, string subjectPropertyState)
        {
            dataLoan.RecordAuditOnSave(AgentRecordChangeAuditHelper.CreateAuditEvent(agent, subjectPropertyState, AgentRecordChangeType.DeleteRecord));
        }
    }
}
