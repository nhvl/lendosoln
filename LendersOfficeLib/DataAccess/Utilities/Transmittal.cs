﻿namespace DataAccess.Utilities
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    
    /// <summary>
    /// Services for Transmittal.
    /// </summary>
    public static class Transmittal
    {        
        /// <summary>
        /// Calculate Asset Total.
        /// </summary>
        /// <param name="loanId">The loan ID Value.</param>
        /// <returns>The Asset Total.</returns>
        public static decimal CalculateAssetTotal(Guid loanId)
        {
            CPageData dataLoan = ConstructPageDataClass(loanId);
            dataLoan.InitLoad();

            // 5/10/2012 vm - OPM 25561 - Copy the value from aAsstLiqTot
            int numberApps = dataLoan.nApps;
            var arrAsstLiqTots = new List<decimal>();
            for (int i = 0; i < numberApps; i++)
            {
                CAppData dataApp = dataLoan.GetAppData(i);
                arrAsstLiqTots.Add(dataApp.aAsstLiqTot);
            }

            return Tools.SumMoney(arrAsstLiqTots);
        }

        /// <summary>
        /// Contruct Page Data Class.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        /// <returns>The Page Data Class.</returns>
        private static CPageData ConstructPageDataClass(Guid loanId)
        {
            return CPageData.CreateUsingSmartDependency(loanId, typeof(Transmittal));
        }
    }
}
