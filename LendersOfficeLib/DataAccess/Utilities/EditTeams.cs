﻿namespace DataAccess.Utilities
{
    using System;
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.Security;

    /// <summary>
    /// Services for Edit Team page.
    /// </summary>
    public class EditTeams
    {
        /// <summary>
        /// Clear Assignment.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="roleString">The role string.</param>
        public static void ClearLoanAssignment(Guid loanId, string roleString)
        {
            Role role = Role.Get(roleString);
            foreach (Team team in Team.ListTeamsOnLoan(loanId))
            {
                if (role.Id == team.RoleId)
                {
                    Team.RemoveLoanAssignment(PrincipalFactory.CurrentPrincipal.BrokerId, team.Id, loanId);
                }
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(EditTeams));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            switch (role.RoleT)
            {
                case E_RoleT.CallCenterAgent:
                    dataLoan.sTeamCallCenterAgentId = Guid.Empty;
                    break;
                case E_RoleT.Closer:
                    dataLoan.sTeamCloserId = Guid.Empty;
                    break;
                case E_RoleT.CollateralAgent:
                    dataLoan.sTeamCollateralAgentId = Guid.Empty;
                    break;
                case E_RoleT.CreditAuditor:
                    dataLoan.sTeamCreditAuditorId = Guid.Empty;
                    break;
                case E_RoleT.DisclosureDesk:
                    dataLoan.sTeamDisclosureDeskId = Guid.Empty;
                    break;
                case E_RoleT.DocDrawer:
                    dataLoan.sTeamDocDrawerId = Guid.Empty;
                    break;
                case E_RoleT.Funder:
                    dataLoan.sTeamFunderId = Guid.Empty;
                    break;
                case E_RoleT.Insuring:
                    dataLoan.sTeamInsuringId = Guid.Empty;
                    break;
                case E_RoleT.JuniorProcessor:
                    dataLoan.sTeamJuniorProcessorId = Guid.Empty;
                    break;
                case E_RoleT.LegalAuditor:
                    dataLoan.sTeamLegalAuditorId = Guid.Empty;
                    break;
                case E_RoleT.LenderAccountExecutive:
                    dataLoan.sTeamLenderAccExecId = Guid.Empty;
                    break;
                case E_RoleT.LoanOfficer:
                    dataLoan.sTeamLoanRepId = Guid.Empty;
                    break;
                case E_RoleT.LoanOfficerAssistant:
                    dataLoan.sTeamLoanOfficerAssistantId = Guid.Empty;
                    break;
                case E_RoleT.LoanOpener:
                    dataLoan.sTeamLoanOpenerId = Guid.Empty;
                    break;
                case E_RoleT.LockDesk:
                    dataLoan.sTeamLockDeskId = Guid.Empty;
                    break;
                case E_RoleT.Manager:
                    dataLoan.sTeamManagerId = Guid.Empty;
                    break;
                case E_RoleT.PostCloser:
                    dataLoan.sTeamPostCloserId = Guid.Empty;
                    break;
                case E_RoleT.Processor:
                    dataLoan.sTeamProcessorId = Guid.Empty;
                    break;
                case E_RoleT.Purchaser:
                    dataLoan.sTeamPurchaserId = Guid.Empty;
                    break;
                case E_RoleT.QCCompliance:
                    dataLoan.sTeamQCComplianceId = Guid.Empty;
                    break;
                case E_RoleT.RealEstateAgent:
                    dataLoan.sTeamRealEstateAgentId = Guid.Empty;
                    break;
                case E_RoleT.Secondary:
                    dataLoan.sTeamSecondaryId = Guid.Empty;
                    break;
                case E_RoleT.Servicing:
                    dataLoan.sTeamServicingId = Guid.Empty;
                    break;
                case E_RoleT.Shipper:
                    dataLoan.sTeamShipperId = Guid.Empty;
                    break;
                case E_RoleT.Underwriter:
                    dataLoan.sTeamUnderwriterId = Guid.Empty;
                    break;
                default:
                    throw new CBaseException("Invalid role", "Invalid role:  '" + role + "'.");
            }

            dataLoan.Save();
        }

        /// <summary>
        /// Assign Loan.
        /// </summary>
        /// <param name="loanId">The Loan ID.</param>
        /// <param name="teamId">The team ID.</param>
        public static void AssignLoan(Guid loanId, Guid teamId)
        {
            Team team = Team.LoadTeam(teamId);
            Role role = Role.Get(team.RoleId);
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(EditTeams));
            dataLoan.InitLoad();

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            switch (role.RoleT)
            {
                case E_RoleT.CallCenterAgent:
                    dataLoan.sTeamCallCenterAgentId = team.Id;
                    break;
                case E_RoleT.Closer:
                    dataLoan.sTeamCloserId = team.Id;
                    break;
                case E_RoleT.CollateralAgent:
                    dataLoan.sTeamCollateralAgentId = team.Id;
                    break;
                case E_RoleT.CreditAuditor:
                    dataLoan.sTeamCreditAuditorId = team.Id;
                    break;
                case E_RoleT.DisclosureDesk:
                    dataLoan.sTeamDisclosureDeskId = team.Id;
                    break;
                case E_RoleT.DocDrawer:
                    dataLoan.sTeamDocDrawerId = team.Id;
                    break;
                case E_RoleT.Funder:
                    dataLoan.sTeamFunderId = team.Id;
                    break;
                case E_RoleT.Insuring:
                    dataLoan.sTeamInsuringId = team.Id;
                    break;
                case E_RoleT.JuniorProcessor:
                    dataLoan.sTeamJuniorProcessorId = team.Id;
                    break;
                case E_RoleT.LegalAuditor:
                    dataLoan.sTeamLegalAuditorId = team.Id;
                    break;
                case E_RoleT.LenderAccountExecutive:
                    dataLoan.sTeamLenderAccExecId = team.Id;
                    break;
                case E_RoleT.LoanOfficer:
                    dataLoan.sTeamLoanRepId = team.Id;
                    break;
                case E_RoleT.LoanOfficerAssistant:
                    dataLoan.sTeamLoanOfficerAssistantId = team.Id;
                    break;
                case E_RoleT.LoanOpener:
                    dataLoan.sTeamLoanOpenerId = team.Id;
                    break;
                case E_RoleT.LockDesk:
                    dataLoan.sTeamLockDeskId = team.Id;
                    break;
                case E_RoleT.Manager:
                    dataLoan.sTeamManagerId = team.Id;
                    break;
                case E_RoleT.PostCloser:
                    dataLoan.sTeamPostCloserId = team.Id;
                    break;
                case E_RoleT.Processor:
                    dataLoan.sTeamProcessorId = team.Id;
                    break;
                case E_RoleT.Purchaser:
                    dataLoan.sTeamPurchaserId = team.Id;
                    break;
                case E_RoleT.QCCompliance:
                    dataLoan.sTeamQCComplianceId = team.Id;
                    break;
                case E_RoleT.RealEstateAgent:
                    dataLoan.sTeamRealEstateAgentId = team.Id;
                    break;
                case E_RoleT.Secondary:
                    dataLoan.sTeamSecondaryId = team.Id;
                    break;
                case E_RoleT.Servicing:
                    dataLoan.sTeamServicingId = team.Id;
                    break;
                case E_RoleT.Shipper:
                    dataLoan.sTeamShipperId = team.Id;
                    break;
                case E_RoleT.Underwriter:
                    dataLoan.sTeamUnderwriterId = team.Id;
                    break;
                default:
                    throw new CBaseException("Invalid role", "Invalid role:  '" + role + "'.");
            }

            dataLoan.Save();
        }
    }
}
