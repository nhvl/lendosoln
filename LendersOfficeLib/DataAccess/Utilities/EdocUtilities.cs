﻿namespace DataAccess.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using EDocs;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.ObjLib.DocMagicLib;
    using LendersOffice.ObjLib.DocumentConditionAssociation;
    using LendersOffice.PdfLayout;
    using LendersOffice.Security;
    using LendersOffice.Services;
    using LendersOffice.UI;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Utilities for edoc services.
    /// </summary>
    public class EdocUtilities
    {
        /// <summary>
        /// Get ConditionAssociation of EDocument.
        /// </summary>
        /// <param name="loanId">Id of the loan.</param>
        /// <param name="docId">Id of the Edocument.</param>
        /// <returns>List of ConditionAssociation.</returns>
        public static ConditionAssociationDTO[] GetConditionAssociation(Guid loanId, Guid? docId)
        {
            if (BrokerUserPrincipal.CurrentPrincipal == null)
            {
                throw new Exception("CurrentPrincipal is null");
            }

            var docIsProtected = (docId.HasValue && docId.Value != Guid.Empty)
                ? EDocumentRepository.GetUserRepository().GetDocumentById(docId.Value).DocStatus == E_EDocStatus.Approved
                : false;

            return
                DocumentConditionAssociation.GetConditionsByLoanDocument(PrincipalFactory.CurrentPrincipal, loanId, docId.Value, excludeThoseUserDoesntHaveAccessTo: false)
                .Where(a => a.Condition.TaskStatus != LendersOffice.ObjLib.Task.E_TaskStatus.Closed)
                .Select((c, i) => new ConditionAssociationDTO(c, docIsProtected, i, BrokerUserPrincipal.CurrentPrincipal.BrokerDB.IsUseNewTaskSystemStaticConditionIds))
                .OrderBy(a => a.RowId)
                .ToArray();
        }

        /// <summary>
        /// Gets a dictionary mapping edoc ids to app ids for the docs in a loan.
        /// </summary>
        /// <param name="loanId">The loan to look at.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <returns>The dictionary.</returns>
        public static Dictionary<Guid, Guid?> GetEdocToAppIdMap(Guid loanId, Guid brokerId)
        {
            Dictionary<Guid, Guid?> edocToAppId = new Dictionary<Guid, Guid?>();

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@LoanId", loanId),
                new SqlParameter("@BrokerId", brokerId)
            };

            StoredProcedureName edocsFetchAllDocsForLoan = StoredProcedureName.Create("EDOCS_FetchAllDocsForLoan").Value;
            using (var connection = DbAccessUtils.GetConnection(brokerId))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(connection, null, edocsFetchAllDocsForLoan, parameters, TimeoutInSeconds.Thirty))
            {
                while (reader.Read())
                {
                    Guid edocId = (Guid)reader["DocumentId"];
                    Guid? appId = reader.AsNullableGuid("aAppId");

                    edocToAppId.Add(edocId, appId);
                }
            }

            return edocToAppId;
        }

        /// <summary>
        /// Get all documents from a loan.
        /// </summary>
        /// <param name="loanId">Id of a loan to get all document from.</param>
        /// <returns>All document from a specific loan.</returns>
        public static Dictionary<string, object> GetDocuments(Guid loanId)
        {
            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            var docs = repo.GetDocumentsByLoanId(loanId);

            var dict = new Dictionary<string, object>();

            dict.Add("documents", docs.Select(x => EdocUtilities.GetDocument(x)));

            return dict;
        }

        /// <summary>
        /// Update documents of a specific loan.
        /// </summary>
        /// <param name="documentDict">Document models.</param>
        /// <param name="loanId">Id of the loan.</param>
        public static void SaveDoc(Dictionary<string, object> documentDict, Guid loanId)
        {
            Guid docId = new Guid((string)documentDict[nameof(EDocument.DocumentId)]);

            int version = System.Convert.ToInt32(documentDict[nameof(EDocument.Version)]);

            string publicDescription = (string)documentDict[nameof(EDocument.PublicDescription)];
            string description = (string)documentDict[nameof(EDocument.InternalDescription)];

            E_EDocStatus status = (E_EDocStatus)System.Convert.ToInt32(documentDict[nameof(EDocument.DocStatus)]);
            string statusDescription = (string)documentDict[nameof(EDocument.StatusDescription)];

            int docTypeId = System.Convert.ToInt32(documentDict[nameof(EDocument.DocumentTypeId)]);

            string signatures = (string)documentDict["signatures"];
            List<PdfField> signatureFields = SerializationHelper.JsonNetDeserialize<List<PdfField>>(signatures);

            bool hideFromPML = System.Convert.ToBoolean(documentDict[nameof(EDocument.HideFromPMLUsers)]);

            var repo = EDocumentRepository.GetUserRepository();
            var pages = new List<PdfPageItem>();

            var pageObjects = SerializationHelper.JsonNetDeserialize<List<PdfPageItem>>(documentDict["pages"].ToString());
            var annotations = new List<EDocumentAnnotationItem>();
            var esignTags = new List<EDocumentAnnotationItem>();
            int i = 0;
            foreach (var page in pageObjects)
            {
                pages.Add(page);

                var pageJson = SerializationHelper.JsonNetSerialize(page);
                var pageDict = SerializationHelper.JsonNetDeserialize<Dictionary<string, object>>(pageJson);

                var pageAnnotationsJson = pageDict["annotations"].ToString();
                var pageAnnotations = SerializationHelper.JsonNetDeserialize<List<EDocumentAnnotationItem>>(pageAnnotationsJson);
                foreach (var annotation in pageAnnotations)
                {
                    annotation.PageNumber = i + 1;
                }

                annotations.AddRange(pageAnnotations);

                var esignTagsJson = pageDict["eSignTags"].ToString();
                esignTags = SerializationHelper.JsonNetDeserialize<List<EDocumentAnnotationItem>>(esignTagsJson);
                esignTags.ForEach((tag) => tag.PageNumber = i + 1);

                i++;
            }

            if (pages.Count == 0)
            {
                repo.DeleteDocument(docId, "User removed all pages.");
                return;
            }

            var auditDicts = SerializationHelper.JsonNetDeserialize<List<Dictionary<string, object>>>(documentDict["audit"].ToString());

            var tracker = new EDocumentIntermediateAuditTracker(docId, PrincipalFactory.CurrentPrincipal.UserId);

            foreach (var auditDict in auditDicts)
            {
                var userAction = (E_PdfUserAction)auditDict["event"];
                var time = (DateTime)auditDict["time"];
                tracker.AddEvent(userAction);
            }

            bool wasPdfUpdated;

            EDocumentViewer.SavePages(PrincipalFactory.CurrentPrincipal, docId, version, docTypeId, description, publicDescription, status, statusDescription, hideFromPML, annotations, pages, out wasPdfUpdated, tracker, signatureFields, esignTags);

            var nonPDFData = SerializationHelper.JsonNetDeserialize<Dictionary<string, object>>(documentDict["nonPdfData"].ToString());
            var conditionAssociations = SerializationHelper.JsonNetDeserialize<List<EdocUtilities.ConditionAssociationDTO>>(nonPDFData["conditionAssociations"].ToString());
            EdocUtilities.SaveConditionAsscociations(conditionAssociations, docId, loanId);
        }

        /// <summary>
        /// Serialize document object to a dictionary.
        /// </summary>
        /// <param name="doc">EDocument object.</param>
        /// <returns>Dictionary serialized from the document.</returns>
        private static Dictionary<string, object> GetDocument(EDocument doc)
        {
            var docDict = new Dictionary<string, object>();
            docDict[nameof(doc.DocumentId)] = doc.DocumentId;

            docDict[nameof(doc.DocTypeName)] = doc.DocTypeName;
            docDict[nameof(doc.FolderAndDocTypeName)] = doc.FolderAndDocTypeName;
            docDict[nameof(doc.DocumentTypeId)] = doc.DocumentTypeId;

            docDict[nameof(doc.AppName)] = doc.AppName;
            docDict[nameof(doc.FolderId)] = doc.FolderId;
            docDict[nameof(doc.Folder)] = doc.Folder;

            docDict[nameof(doc.InternalDescription)] = doc.InternalDescription;
            docDict[nameof(doc.PublicDescription)] = doc.PublicDescription;
            docDict[nameof(doc.Comments)] = doc.Comments;
            docDict[nameof(doc.Version)] = doc.Version;
            docDict[nameof(doc.HideFromPMLUsers)] = doc.HideFromPMLUsers;
            docDict[nameof(doc.DocStatus)] = doc.DocStatus;
            docDict[nameof(doc.StatusDescription)] = doc.StatusDescription;

            var annotations = doc.InternalAnnotations.GenerateEditorList(BrokerUserPrincipal.CurrentPrincipal);
            foreach (var annotation in annotations)
            {
                if (annotation.PageNumber <= 0)
                {
                    annotation.PageNumber = 1;
                }
            }

            var pages = new List<object>();

            int i = 0;
            foreach (var page in doc.GetPdfPageInfoList())
            {
                var pageJson = SerializationHelper.JsonNetSerialize(page);
                var pageDict = SerializationHelper.JsonNetDeserialize<Dictionary<string, object>>(pageJson);

                pageDict.Add("annotations", SerializationHelper.JsonNetSerialize(annotations.Where(x => x.PageNumber == i + 1).ToList()));
                pages.Add(pageDict);
                i++;
            }

            docDict["pages"] = pages;
            docDict["nonPdfData"] = new Dictionary<string, object>() { { "conditionAssociations", EdocService.GetConditionAssociation(doc.LoanId.Value, doc.DocumentId) } };

            docDict["signatures"] = string.Empty;
            docDict["audit"] = new string[0];

            return docDict;
        }

        /// <summary>
        /// Save condiation assocaitions of a document.
        /// </summary>
        /// <param name="conditionAssociations">List of condiation assocaitions to be updated. </param>
        /// <param name="docId">Id of document.</param>
        /// <param name="loanId">Id of loan.</param>
        private static void SaveConditionAsscociations(IEnumerable<EdocUtilities.ConditionAssociationDTO> conditionAssociations, Guid docId, Guid loanId)
        {
            var allConditions = DocumentConditionAssociation.GetConditionsByLoanDocument(PrincipalFactory.CurrentPrincipal, loanId, docId, excludeThoseUserDoesntHaveAccessTo: false);

            IEnumerable<AssociatedTask> editableConditions = allConditions;

            if (!BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowEditingApprovedEDocs))
            {
                editableConditions = from assoc in allConditions
                                     where assoc.Association == null || assoc.Association.Status != E_DocumentConditionAssociationStatus.Satisfied
                                     select assoc;
            }

            foreach (var cond in editableConditions)
            {
                var conAssociation = conditionAssociations.Where(x => x.TaskId == cond.Condition.TaskId).FirstOrDefault();

                if (conAssociation != null)
                {
                    var temp = new DocumentConditionAssociation(PrincipalFactory.CurrentPrincipal.BrokerId, loanId, cond.Condition.TaskId, docId);
                    temp.Save();
                }
            }
        }

        /// <summary>
        /// Data object of condition asscocation.
        /// </summary>
        public class ConditionAssociationDTO
        {
            /// <summary>
            ///  Initializes a new instance of the ConditionAssociationDTO class.
            /// </summary>
            public ConditionAssociationDTO()
            {
            }

            /// <summary>
            ///  Initializes a new instance of the ConditionAssociationDTO class.
            /// </summary>
            /// <param name="task">Associated Task.</param>
            /// <param name="docIsProtected">Document is protected.</param>
            /// <param name="idx">Index of item in the list.</param>
            /// <param name="useStaticConditionIds">Detect if using static condition Ids.</param>
            public ConditionAssociationDTO(AssociatedTask task, bool docIsProtected, int idx, bool useStaticConditionIds)
            {
                this.IsAssociated = task.IsAssociated;
                this.TaskId = task.Condition.TaskId;
                this.RowId = useStaticConditionIds ? task.Condition.CondRowId : idx + 1;
                this.Order = idx;
                this.IsProtected = (docIsProtected && !BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowEditingApprovedEDocs)) &&
                                (task.Association != null) &&
                                (task.Association.Status == E_DocumentConditionAssociationStatus.Satisfied);
                this.ConditionText = this.RowId + ". " + (this.IsProtected ? "(PROTECTED) " : string.Empty) + task.Condition.TaskSubject;
                this.Category = task.Condition.CondCategoryId_rep;
            }

            /// <summary>
            /// Gets a value of associated task id.
            /// </summary>
            /// <value>Value of associated task id.</value>
            public string TaskId { get; private set; }

            /// <summary>
            /// Gets a value indicating whether doc is protected.
            /// </summary>
            /// <value>Value indicating whether doc is protected.</value>
            public bool IsProtected { get; private set; }

            /// <summary>
            /// Gets a value indicating whether a doc is associated.
            /// </summary>
            /// <value>Value indicating whether a doc is associated.</value>
            public bool IsAssociated { get; private set; }

            /// <summary>
            /// Gets a value of condition text.
            /// </summary>
            /// <value>Value of condition text.</value>
            public string ConditionText { get; private set; }

            /// <summary>
            /// Gets a value of category.
            /// </summary>
            /// <value>Value of category.</value>
            public string Category { get; private set; }

            /// <summary>
            /// Gets a value of row id of the condition.
            /// </summary>
            /// <value>Value of row id of the condition.</value>
            public int RowId { get; private set; }

            /// <summary>
            /// Gets a value of item order.
            /// </summary>
            /// <value>Value of item order.</value>
            public int Order { get; private set; }
        }
    }
}
