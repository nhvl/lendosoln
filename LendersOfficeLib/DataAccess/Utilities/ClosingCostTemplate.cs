﻿namespace DataAccess.Utilities
{
    using System;
    using System.Linq;
    using DataAccess;
    using DataAccess.ClosingCostAutomation;
    using LendersOffice.Security;

    /// <summary>
    /// Services for Closing Cost.
    /// </summary>
    public static class ClosingCostTemplate
    {
        /// <summary>
        /// Current Principal.
        /// </summary>
        private static AbstractUserPrincipal Principal => PrincipalFactory.CurrentPrincipal;

        /// <summary>
        /// Get Closing Cost List.
        /// </summary>
        /// <param name="loanId">Loan Id (sLId).</param>
        /// <param name="gfeVersion">GFE Version.</param>
        /// <returns>A list of Closing Cost records.</returns>
        public static CCTemplateDetails[] GetClosingCost(Guid loanId, int gfeVersion)
        {
            return (Principal.BrokerDB.AreAutomatedClosingCostPagesVisible
                ? new ClosingCostAutomationStorage(Principal).GetAllTemplateDetails()
                : CCcTemplateData.RetrieveManualClosingCostTemplateInfo(Principal, gfeVersion)
                    .Select(t => new CCTemplateDetails
                    {
                        Id = t.Id,
                        Name = t.Name,
                        Version = (E_GfeVersion)t.Version,
                    })).ToArray();
        }

        /// <summary>
        /// Apply Closing Cost template.
        /// </summary>
        /// <param name="loanId">Loan Id (sLId).</param>
        /// <param name="templateId">Closing Cost Template ID.</param>
        public static void ApplyClosingCostTemplate(Guid loanId, Guid templateId)
        {
            //// allow even if PrincipalFactory.CurrentPrincipal.HasPermission(Permission.CanAccessCCTemplates) == fasle.

            Guid brokerId = Principal.BrokerId;

            var data = new CCcTemplateData(brokerId, templateId);
            data.InitLoad();
            data.ApplyTo(loanId, true);
        }
    }
}
