﻿namespace DataAccess.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using System.Text.RegularExpressions;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Security;
    using LendersOffice.UI;
    using Newtonsoft.Json;

    /// <summary>
    /// Utilities for Broker Info services.
    /// </summary>
    public class LoanNaming
    {
        /// <summary>
        /// Get loan naming scheme of current broker.
        /// </summary>
        /// <param name="broker">Broker Info input.</param>
        /// <param name="branchId">Id of current branch.</param>
        /// <returns>Return the naming options of current broker naming scheme.</returns>
        public static Dictionary<string, object> GetNamingOptions(BrokerDB broker, Guid branchId)
        {
            var namingOptions = new LoanNamingOption();
            var pattern = broker.NamingPattern;

            namingOptions.NamingScheme = pattern.IndexOf("{mers") != -1 ? LoanNamingOption.NameSchemeEnum.Mers : LoanNamingOption.NameSchemeEnum.Sequential;

            namingOptions.Prefix = Tools.GetCorporatePrefix(broker.NamingPattern);
            namingOptions.UsingLoanMers = !string.IsNullOrEmpty(broker.MersFormat);
            namingOptions.MersIncludePrefix = Tools.UsesPrefix(broker.MersFormat);
            namingOptions.MersOrganizationId = broker.MersOrganizationId;
            namingOptions.MersStartingCounter = broker.MersCounter + 1;

            namingOptions.IsDuplicateLoanCreatedWAutonameBit = broker.IsDuplicateLoanCreatedWAutonameBit;
            namingOptions.Is2ndLoanCreatedWAutonameBit = broker.Is2ndLoanCreatedWAutonameBit;
            namingOptions.IsAutoGenerateMersMinCB = broker.IsAutoGenerateMersMin;

            var loanNamingData = new CLoanFileNamer.LoanNamingData(broker.BrokerID, branchId, false, false, broker.IsAutoGenerateMersMin, true);

            namingOptions.NextMersMin = MersUtilities.GenerateMersNumber(loanNamingData);

            if (pattern.IndexOf("{mm}") != -1)
            {
                namingOptions.MonthLength = 2;
                namingOptions.UsingMonth = true;
            }
            else if (pattern.IndexOf("{m}") != -1)
            {
                namingOptions.MonthLength = 1;
                namingOptions.UsingMonth = true;
            }
            else
            {
                namingOptions.UsingMonth = false;
            }

            if (pattern.IndexOf("{yyyy}") != -1)
            {
                namingOptions.YearLength = 4;
            }
            else if (pattern.IndexOf("{yy}") != -1)
            {
                namingOptions.YearLength = 2;
                namingOptions.UsingYear = true;
            }
            else
            {
                namingOptions.UsingYear = false;
            }

            namingOptions.UsingDay = pattern.IndexOf("{d}") != -1 || pattern.IndexOf("{dd}") != -1;

            namingOptions.CounterDigits = LoanNamingOption.CounterDigitEnum.Digit3;
            var counterRegex = Regex.Match(pattern, @"{c:(\d+)}");
            if (counterRegex.Success)
            {
                int counter;
                if (int.TryParse(counterRegex.Groups[1].Value, out counter))
                {
                    var enumIndex = counter - 3;
                    if (enumIndex >= 0)
                    {
                        namingOptions.CounterDigits = (LoanNamingOption.CounterDigitEnum)counter;
                    }
                }
            }

            return LoanNaming.CalculateNamingOptions(namingOptions);
        }

        /// <summary>
        /// Caluclate naming option model.
        /// </summary>
        /// <param name="namingOption">Naming option input to calculated.</param>
        /// <returns>New naming option model.</returns>
        public static Dictionary<string, object> CalculateNamingOptions(LoanNamingOption namingOption)
        {
            if (!namingOption.UsingYear)
            {
                namingOption.YearLength = 0;
                namingOption.UsingMonth = false;
            }
            else if (namingOption.YearLength == 0)
            {
                namingOption.YearLength = 4;
            }

            if (!namingOption.UsingMonth)
            {
                namingOption.MonthLength = 0;
                namingOption.UsingDay = false;
            }
            else if (namingOption.MonthLength == 0)
            {
                namingOption.MonthLength = 2;
            }

            if (!namingOption.IsAutoGenerateMersMinCB)
            {
                namingOption.MersOrganizationId = string.Empty;
                namingOption.MersStartingCounter = 0;
            }

            namingOption.ErrorString = string.Empty;

            if (namingOption.NamingScheme == LoanNamingOption.NameSchemeEnum.Sequential)
            {
                int maxLength = 10;

                var counterLength = namingOption.YearLength + namingOption.MonthLength + (int)namingOption.CounterDigits + namingOption.Prefix.Count(x => char.IsDigit(x));
                if (namingOption.UsingDay)
                {
                    counterLength += 2;
                }

                if (counterLength > maxLength)
                {
                    namingOption.ErrorString += "To generate a MERS MIN from the loan number, the loan number must be 10 digits or less\n";
                }

                namingOption.AvailableDigits = maxLength - counterLength;
            }
            else
            {
                namingOption.IsAutoGenerateMersMinCB = true;
                namingOption.UsingLoanMers = false;
            }

            if (!namingOption.IsAutoGenerateMersMinCB)
            {
                namingOption.UsingLoanMers = false;
            }

            if (namingOption.UsingLoanMers)
            {
                namingOption.Is2ndLoanCreatedWAutonameBit = true;
                namingOption.ErrorString += "To generate a MERS MIN from the loan number including the prefix, the corporate prefix and all branch prefixes must contain only digits.";
            }

            if (!namingOption.PrefixesHaveDigitOnly())
            {
                namingOption.MersIncludePrefix = false;
            }

            return InputModelProvider.GetInputModel(namingOption);
        }

        /// <summary>
        /// Bind naming option to broker loan naming scheme.
        /// </summary>
        /// <param name="namingOptionModel">Naming options.</param>
        /// <param name="broker">Broker need to be updated loan naming scheme.</param>
        public static void BindNamingOptions(Dictionary<string, object> namingOptionModel, BrokerDB broker)
        {
            var namingOption = new LoanNamingOption();
            InputModelProvider.BindObjectFromInputModel(namingOption, namingOptionModel);

            string namingPattern = string.Empty;

            namingPattern += namingOption.Prefix.TrimWhitespaceAndBOM();

            if (namingOption.UsingYear)
            {
                namingPattern += namingOption.YearLength == 2 ? "{ yy}" : "{yyyy}";
            }

            if (namingOption.UsingMonth)
            {
                namingPattern += namingOption.MonthLength == 1 ? "{m}" : "{mm}";
            }

            if (namingOption.UsingDay)
            {
                namingPattern += "{dd}";
            }

            if (namingOption.CounterDigits > 0)
            {
                namingPattern += namingOption.CounterDigits;
            }

            broker.IsDuplicateLoanCreatedWAutonameBit = namingOption.IsDuplicateLoanCreatedWAutonameBit;
            broker.Is2ndLoanCreatedWAutonameBit = namingOption.Is2ndLoanCreatedWAutonameBit;

            bool isMersUsedForNewLoanName = namingOption.NamingScheme == LoanNamingOption.NameSchemeEnum.Mers;
            var sequentialMers = !namingOption.UsingLoanMers;

            if (namingOption.IsAutoGenerateMersMinCB)
            {
                broker.MersOrganizationId = namingOption.MersOrganizationId;

                if (sequentialMers)
                {
                    broker.MersFormat = string.Empty;
                    broker.MersCounter = namingOption.MersStartingCounter - 1;
                }
                else
                {
                    var mersPattern = namingPattern;
                    if (namingOption.MersIncludePrefix)
                    {
                        var prefixIdx = namingPattern.IndexOf('{');
                        mersPattern = namingPattern.Substring(prefixIdx);
                    }

                    broker.MersFormat = "{mersID}" + mersPattern;
                }
            }

            if (isMersUsedForNewLoanName)
            {
                if (sequentialMers)
                {
                    broker.NamingPattern = "{mers:" + namingOption.MersOrganizationId + "}";
                    broker.NamingCounter = namingOption.MersStartingCounter - 1;
                }
                else
                {
                    if (namingOption.PreviousNamingScheme == LoanNamingOption.NameSchemeEnum.Mers)
                    {
                        broker.NamingCounter = 0;
                    }

                    broker.NamingPattern = broker.MersFormat;
                }
            }
            else
            {
                if (namingOption.PreviousNamingScheme == LoanNamingOption.NameSchemeEnum.Mers)
                {
                    broker.NamingCounter = 0;
                }

                broker.NamingPattern = namingPattern;
            }

            // OPM 451012 - Copy the broker's loan naming pattern to their lead pattern
            // to keep the two in sync.
            broker.LeadNamingPattern = broker.NamingPattern;

            bool isValid = broker.Save();
            return;
        }

        /// <summary>
        /// Get branch prefix models.
        /// </summary>
        /// <param name="brokerId">ID of current broker.</param>
        /// <returns>Branch prefix models.</returns>
        public static IEnumerable<SimpleObjectInputFieldModel> GetBranchNamePrefix(Guid brokerId)
        {
            var prefixModels = new List<SimpleObjectInputFieldModel>();
            var brancheDBs = BranchDB.GetBranchObjects(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
            foreach (var branch in brancheDBs)
            {
                var loanNamingData = new CLoanFileNamer.LoanNamingData(brokerId, branch.BranchID, false, false, false, true);

                var model = new SimpleObjectInputFieldModel();
                model.Add(nameof(branch.BranchID), new InputFieldModel(branch.BranchID));

                var hasPrefix = !string.IsNullOrEmpty(loanNamingData.branchLNmPrefix);
                model.Add(nameof(loanNamingData.branchLNmPrefix), new InputFieldModel(loanNamingData.branchLNmPrefix, readOnly: !hasPrefix));
                model.Add(nameof(hasPrefix), new InputFieldModel(hasPrefix));
                model.Add(nameof(branch.Name), new InputFieldModel(branch.Name));

                prefixModels.Add(model);
            }

            return prefixModels;
        }

        /// <summary>
        /// Calculate branch prefix models.
        /// </summary>
        /// <param name="prefixModels">Branch prefix models to be calculated.</param>
        /// <returns>Calculated branch prefix models.</returns>
        public static IEnumerable<SimpleObjectInputFieldModel> CalculateBranchNamePrefix(IEnumerable<SimpleObjectInputFieldModel> prefixModels)
        {
            var newModels = prefixModels;
            var hasPrefixKey = "hasPrefix";
            var prefixKey = nameof(CLoanFileNamer.LoanNamingData.branchLNmPrefix);

            foreach (var model in newModels)
            {
                if (model.ContainsKey(hasPrefixKey) && model.ContainsKey(prefixKey))
                {
                    bool hasPrefix = (bool)model[hasPrefixKey].ValueFromType(typeof(bool));
                    model[prefixKey].IsReadOnly = !hasPrefix;

                    if (!hasPrefix)
                    {
                        model[prefixKey].Value = string.Empty;
                    }
                }
            }

            return newModels;
        }

        /// <summary>
        /// Save branch prefix models.
        /// </summary>
        /// <param name="brokerId">ID of current broker.</param>
        /// <param name="prefixModels">Branch prefix models to be saved.</param>
        public static void SetBranchNamePrefix(Guid brokerId, IEnumerable<SimpleObjectInputFieldModel> prefixModels)
        {
            var newModels = prefixModels;
            var prefixKey = nameof(CLoanFileNamer.LoanNamingData.branchLNmPrefix);
            var branchIdKey = nameof(BranchDB.BranchID);

            foreach (var model in newModels)
            {
                SqlParameter[] parameters = new SqlParameter[]
                {
                    new SqlParameter("@BrokerID", brokerId),
                    new SqlParameter("@BranchID", model[branchIdKey].Value),
                    new SqlParameter("@NamingPrefix", model[prefixKey].Value)
                };

                try
                {
                    StoredProcedureHelper.ExecuteNonQuery(brokerId, "UpdateBranchNamingPrefix", 0, parameters);
                }
                catch (Exception exc)
                {
                    throw new CBaseException("Failed to update branch prefix settings.", exc);
                }
            }
        }

        /// <summary>
        /// Contain options for naming a loan.
        /// </summary>
        public class LoanNamingOption
        {
            /// <summary>
            /// Enum store name scheme type.
            /// </summary>
            public enum NameSchemeEnum
            {
                /// <summary>
                /// MERS naming scheme.
                /// </summary>
                Mers = 0,

                /// <summary>
                /// Sequential naming scheme.
                /// </summary>
                Sequential = 1
            }

            /// <summary>
            /// Enum for counter digit.
            /// </summary>
            public enum CounterDigitEnum
            {
                /// <summary>
                /// Counter with 3 digits.
                /// </summary>
                [OrderedDescription("3-digits")]
                Digit3 = 3,

                /// <summary>
                /// Counter with 4 digits.
                /// </summary>
                [OrderedDescription("4-digits")]
                Digit4 = 4,

                /// <summary>
                /// Counter with 5 digits.
                /// </summary>
                [OrderedDescription("5-digits")]
                Digit5 = 5,

                /// <summary>
                /// Counter with 6 digits.
                /// </summary>
                [OrderedDescription("6-digits")]
                Digit6 = 6,

                /// <summary>
                /// Counter with 7 digits.
                /// </summary>
                [OrderedDescription("7-digits")]
                Digit7 = 7,

                /// <summary>
                /// Counter with 8 digits.
                /// </summary>
                [OrderedDescription("8-digits")]
                Digit8 = 8,

                /// <summary>
                /// Counter with 3 digits.
                /// </summary>
                [OrderedDescription("9-digits")]
                Digit9 = 9,
            }

            /// <summary>
            /// Gets or sets value of naming scheme option.
            /// </summary>
            /// <value>Value indicating naming scheme option.</value>
            [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate, NullValueHandling = NullValueHandling.Ignore)]
            public NameSchemeEnum NamingScheme { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether using this scheme for duplicated loan.
            /// </summary>
            /// <value>Value indicating whether whether using this scheme for duplicated loan.</value>
            [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate, NullValueHandling = NullValueHandling.Ignore)]
            public bool IsDuplicateLoanCreatedWAutonameBit { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether using this scheme for second loan.
            /// </summary>
            /// <value>Value indicating whether using this scheme for second loan.</value>
            [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate, NullValueHandling = NullValueHandling.Ignore)]
            public bool Is2ndLoanCreatedWAutonameBit { get; set; }

            /// <summary>
            /// Gets a value indicating whether property Is2ndLoanCreatedWAutonameBit should be read only.
            /// </summary>
            /// <value>Value  indicating whether property Is2ndLoanCreatedWAutonameBit should be read only.</value>
            public bool Is2ndLoanCreatedWAutonameBitReadOnly
            {
                get
                {
                    return this.UsingLoanMers && this.MersIncludePrefix;
                }
            }

            /// <summary>
            /// Gets or sets a value indicating whether auto generate Mers Min.
            /// </summary>
            /// <value>Value indicating whether auto generate Mers Min.</value>
            [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate, NullValueHandling = NullValueHandling.Ignore)]
            public bool IsAutoGenerateMersMinCB { get; set; }

            /// <summary>
            /// Gets a value indicating whether property IsAutoGenerateMersMinCB should be read only.
            /// </summary>
            /// <value>Value  indicating whether property IsAutoGenerateMersMinCB should be read only.</value>
            public bool IsAutoGenerateMersMinCBReadOnly
            {
                get { return this.NamingScheme == LoanNamingOption.NameSchemeEnum.Mers; }
            }

            /// <summary>
            /// Gets or sets a value of previous naming scheme.
            /// </summary>
            /// <value>Value of previous naming scheme.</value>
            [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate, NullValueHandling = NullValueHandling.Ignore)]
            public NameSchemeEnum PreviousNamingScheme { get; set; }

            /// <summary>
            /// Gets a value indicating whether property PreviousNamingScheme should be read only.
            /// </summary>
            /// <value>Value  indicating whether property PreviousNamingScheme should be read only.</value>
            public bool PreviousNamingSchemeReadOnly
            {
                get { return true; }
            }

            /// <summary>
            /// Gets or sets a value of MERS counter.
            /// </summary>
            /// <value>Value of MERS counter.</value>
            [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate, NullValueHandling = NullValueHandling.Ignore)]
            public long MersStartingCounter { get; set; }

            /// <summary>
            /// Gets a value indicating whether property MersStartingCounter should be read only.
            /// </summary>
            /// <value>Value  indicating whether property MersStartingCounter should be read only.</value>
            public bool MersStartingCounterReadOnly
            {
                get { return this.UsingLoanMers; }
            }

            /// <summary>
            /// Gets or sets a value of MERS organization Id.
            /// </summary>
            /// <value>Value of MERS organization Id.</value>
            [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate, NullValueHandling = NullValueHandling.Ignore)]
            public string MersOrganizationId { get; set; }

            /// <summary>
            /// Gets a value indicating whether property MersOrganizationId should be read only.
            /// </summary>
            /// <value>Value  indicating whether property MersOrganizationId should be read only.</value>
            public bool MersOrganizationIdReadOnly
            {
                get { return !this.IsAutoGenerateMersMinCB; }
            }

            /// <summary>
            /// Gets or sets a value of next MERS MIN.
            /// </summary>
            /// <value>Value of next MERS MIN.</value>
            [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate, NullValueHandling = NullValueHandling.Ignore)]
            public string NextMersMin { get; set; }

            /// <summary>
            /// Gets a value indicating whether property NextMersMin should be read only.
            /// </summary>
            /// <value>Value  indicating whether property NextMersMin should be read only.</value>
            public bool NextMersMinReadOnly
            {
                get { return true; }
            }

            /// <summary>
            /// Gets or sets a value of prefix text.
            /// </summary>
            /// <value>Value of prefix text.</value>
            [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate, NullValueHandling = NullValueHandling.Ignore)]
            public string Prefix { get; set; }

            /// <summary>
            /// Gets a value indicating whether property Prefix should be read only.
            /// </summary>
            /// <value>Value  indicating whether property Prefix should be read only.</value>
            public bool PrefixReadOnly
            {
                get { return this.NamingScheme == NameSchemeEnum.Mers; }
            }

            /// <summary>
            /// Gets a value indicating whether property NextLoanName should be read only.
            /// </summary>
            /// <value>Value  indicating whether property NextLoanName should be read only.</value>
            public bool NextLoanNameReadOnly
            {
                get { return true; }
            }

            /// <summary>
            /// Gets or sets a value indicating length of month format.
            /// </summary>
            /// <value>Value indicating length of month format.</value>
            [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate, NullValueHandling = NullValueHandling.Ignore)]
            public int MonthLength { get; set; }

            /// <summary>
            /// Gets a value indicating whether property MonthLength should be read only.
            /// </summary>
            /// <value>Value  indicating whether property MonthLength should be read only.</value>
            public bool MonthLengthReadOnly
            {
                get { return !this.UsingMonth; }
            }

            /// <summary>
            /// Gets or sets a value indicating length of year format.
            /// </summary>
            /// <value>Value indicating length of year format.</value>
            [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate, NullValueHandling = NullValueHandling.Ignore)]
            public int YearLength { get; set; }

            /// <summary>
            /// Gets a value indicating whether property YearLength should be read only.
            /// </summary>
            /// <value>Value  indicating whether property YearLength should be read only.</value>
            public bool YearLengthReadOnly
            {
                get { return !this.UsingYear; }
            }

            /// <summary>
            /// Gets or sets a value indicating whether year format is used.
            /// </summary>
            /// <value>Value indicating whether  year format is used.</value>
            [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate, NullValueHandling = NullValueHandling.Ignore)]
            public bool UsingYear { get; set; }

            /// <summary>
            /// Gets a value indicating whether year format is used.
            /// </summary>
            /// <value>Value indicating whether  year format is used.</value>
            [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate, NullValueHandling = NullValueHandling.Ignore)]
            public bool UsingYearReadOnly
            {
                get { return this.NamingScheme == NameSchemeEnum.Mers; }
            }

            /// <summary>
            /// Gets or sets a value indicating whether month format is used.
            /// </summary>
            /// <value>Value indicating whether month format is used.</value>
            [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate, NullValueHandling = NullValueHandling.Ignore)]
            public bool UsingMonth { get; set; }

            /// <summary>
            /// Gets a value indicating whether property UsingMonth should be read only.
            /// </summary>
            /// <value>Value  indicating whether property UsingMonth should be read only.</value>
            public bool UsingMonthReadOnly
            {
                get { return !this.UsingYear; }
            }

            /// <summary>
            /// Gets or sets a value indicating whether day format is used.
            /// </summary>
            /// <value>Value indicating whether day format is used.</value>
            [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate, NullValueHandling = NullValueHandling.Ignore)]
            public bool UsingDay { get; set; }

            /// <summary>
            /// Gets a value indicating whether property UsingDay should be read only.
            /// </summary>
            /// <value>Value  indicating whether property UsingDay should be read only.</value>
            public bool UsingDayReadOnly
            {
                get { return !this.UsingMonth; }
            }

            /// <summary>
            /// Gets or sets a value of counter digits something.
            /// </summary>
            /// <value>Value  counter digits.</value>
            [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate, NullValueHandling = NullValueHandling.Ignore)]
            public CounterDigitEnum CounterDigits { get; set; }

            /// <summary>
            /// Gets a value indicating whether property CounterDigits should be read only.
            /// </summary>
            /// <value>Value  indicating whether property CounterDigits should be read only.</value>
            public bool CounterDigitsReadOnly
            {
                get { return this.NamingScheme == NameSchemeEnum.Mers; }
            }

            /// <summary>
            /// Gets or sets a value indicating whether loan number is used in MERS.
            /// </summary>
            /// <value>Value indicating whether loan number is used in MERS.</value>
            [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate, NullValueHandling = NullValueHandling.Ignore)]
            public bool UsingLoanMers { get; set; }

            /// <summary>
            /// Gets a value indicating whether property UsingLoanMers should be read only.
            /// </summary>
            /// <value>Value  indicating whether property UsingLoanMers should be read only.</value>
            public bool UsingLoanMersReadOnly
            {
                get { return !this.IsAutoGenerateMersMinCB || this.NamingScheme == NameSchemeEnum.Mers; }
            }

            /// <summary>
            /// Gets or sets a value indicating whether prefix is included in MERS.
            /// </summary>
            /// <value>Value indicating whether prefix is included in MERS.</value>
            [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate, NullValueHandling = NullValueHandling.Ignore)]
            public bool MersIncludePrefix { get; set; }

            /// <summary>
            /// Gets a value indicating whether property MersIncludePrefix should be read only.
            /// </summary>
            /// <value>Value  indicating whether property MersIncludePrefix should be read only.</value>
            public bool MersIncludePrefixReadOnly
            {
                get { return !this.UsingLoanMers || !this.PrefixesHaveDigitOnly(); }
            }

            /// <summary>
            /// Gets or sets a value indicating the current naming error.
            /// </summary>
            /// <value>A value indicating the current naming error.</value>
            public string ErrorString { get; set; }

            /// <summary>
            /// Gets a value indicating the current naming error.
            /// </summary>
            /// <value>A value indicating the current naming error.</value>
            public string CurrentScheme
            {
                get
                {
                    return this.NamingScheme == NameSchemeEnum.Mers ? "MERS" : "Sequential";
                }
            }

            /// <summary>
            /// Gets or sets a value of available digits.
            /// </summary>
            /// <value>A value of available digits.</value>
            public int AvailableDigits { get; set; }

            /// <summary>
            /// Gets a value indicating whether use this scheme for template loan.
            /// </summary>
            /// <value>A value indicating whether use this scheme for template loan.</value>
            public bool IsUsedForTemplateLoan
            {
                get
                {
                    return true;
                }
            }

            /// <summary>
            /// Check if loan naming prefixes only have digit.
            /// </summary>
            /// <returns>A value detecting whether loan naming prefixes only have digit.</returns>
            public bool PrefixesHaveDigitOnly()
            {
                return this.IsDigitsOnly(this.Prefix) && LoanNaming.GetBranchNamePrefix(PrincipalFactory.CurrentPrincipal.BrokerId).All(x => this.IsDigitsOnly(x.ValueFromModel(nameof(CLoanFileNamer.LoanNamingData.branchLNmPrefix))));
            }

            /// <summary>
            /// Check if a string contain only digits.
            /// </summary>
            /// <param name="str">Input string to check.</param>
            /// <returns>A boolean value detecting whether string contain only digits.</returns>
            private bool IsDigitsOnly(string str)
            {
                foreach (char c in str)
                {
                    if (c < '0' || c > '9')
                    {
                        return false;
                    }
                }

                return true;
            }
        }
    }
}
