﻿namespace DataAccess.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Admin;

    /// <summary>
    /// Services for Loan Status.
    /// </summary>
    public static class LoanStatus
    {
        /// <summary>
        /// A list of statuses for loan.
        /// </summary>
        private static readonly E_sStatusT[] LoanStatuses =
        {
            E_sStatusT.Loan_Open,
            E_sStatusT.Loan_Prequal,
            E_sStatusT.Loan_Registered,
            E_sStatusT.Loan_Processing,
            E_sStatusT.Loan_Preapproval,
            E_sStatusT.Loan_LoanSubmitted,
            E_sStatusT.Loan_Underwriting,
            E_sStatusT.Loan_Approved,
            E_sStatusT.Loan_FinalUnderwriting,
            E_sStatusT.Loan_ClearToClose,
            E_sStatusT.Loan_Docs,
            E_sStatusT.Loan_DocsBack,
            E_sStatusT.Loan_FundingConditions,
            E_sStatusT.Loan_Funded,
            E_sStatusT.Loan_Recorded,
            E_sStatusT.Loan_FinalDocs,
            E_sStatusT.Loan_Closed,
            E_sStatusT.Loan_Shipped,
            E_sStatusT.Loan_LoanPurchased,
            E_sStatusT.Loan_OnHold,
            E_sStatusT.Loan_Canceled,
            E_sStatusT.Loan_Rejected,
            E_sStatusT.Loan_Suspended,
            E_sStatusT.Loan_Other,
            E_sStatusT.Loan_PreProcessing,    // start sk 1/6/2014 opm 145251
            E_sStatusT.Loan_DocumentCheck,
            E_sStatusT.Loan_DocumentCheckFailed,
            E_sStatusT.Loan_PreUnderwriting,
            E_sStatusT.Loan_ConditionReview,
            E_sStatusT.Loan_PreDocQC,
            E_sStatusT.Loan_DocsOrdered,
            E_sStatusT.Loan_DocsDrawn,
            E_sStatusT.Loan_InvestorConditions,       // this is "promoting the date to a status". tied to sSuspendedByInvestorD
            E_sStatusT.Loan_InvestorConditionsSent,   // this is "promoting the date to a status". tied to sCondSentToInvestorD
            E_sStatusT.Loan_ReadyForSale,
            E_sStatusT.Loan_SubmittedForPurchaseReview,
            E_sStatusT.Loan_InPurchaseReview,
            E_sStatusT.Loan_PrePurchaseConditions,
            E_sStatusT.Loan_SubmittedForFinalPurchaseReview,
            E_sStatusT.Loan_InFinalPurchaseReview,
            E_sStatusT.Loan_ClearToPurchase,
            E_sStatusT.Loan_Purchased,        // don't confuse with the old E_sStatusT.Loan_LoanPurchased, which is now Loan Sold in the UI.
            E_sStatusT.Loan_CounterOffer,
            E_sStatusT.Loan_Withdrawn,
            E_sStatusT.Loan_Archived
        };

        /// <summary>
        /// Status to Description Dictionary.
        /// </summary>
        private static readonly Dictionary<E_sStatusT, string> Status2DescriptionDict = new Dictionary<E_sStatusT, string>
        {
            { E_sStatusT.Loan_WebConsumer, "Loan Web Consumer" },
            { E_sStatusT.Loan_Open, "Loan Open" },
            { E_sStatusT.Loan_Prequal, "Pre-qual" },
            { E_sStatusT.Loan_Docs, "Docs Out" },
            { E_sStatusT.Loan_Registered, "Registered" },
            { E_sStatusT.Loan_Preapproval, "Pre-approved" },
            { E_sStatusT.Loan_Funded, "Funded" },
            { E_sStatusT.Loan_Approved, "Approved" },
            { E_sStatusT.Loan_ClearToClose, "Clear to Close" },
            { E_sStatusT.Loan_Closed, "Loan Closed" },
            { E_sStatusT.Loan_OnHold, "Loan On-hold" },
            { E_sStatusT.Loan_Suspended, "Loan Suspended" },
            { E_sStatusT.Loan_Canceled, "Loan Canceled" },
            { E_sStatusT.Loan_Rejected, "Loan Denied" },
            { E_sStatusT.Loan_Underwriting, "In Underwriting" },
            { E_sStatusT.Loan_Other, "Loan Other" },
            { E_sStatusT.Lead_New, "Lead New" },
            { E_sStatusT.Lead_Canceled, "Lead Canceled" },
            { E_sStatusT.Lead_Declined, "Lead Declined" },
            { E_sStatusT.Lead_Other, "Lead Other" },
            { E_sStatusT.Loan_Recorded, "Recorded" },
            { E_sStatusT.Loan_Shipped, "Loan Shipped" },
            { E_sStatusT.Loan_Processing, "Processing" },
            { E_sStatusT.Loan_FinalUnderwriting, "Final Underwriting" },
            { E_sStatusT.Loan_DocsBack, "Docs Back" },
            { E_sStatusT.Loan_FundingConditions, "Funding Conditions" },
            { E_sStatusT.Loan_FinalDocs, "Final Docs" },
            { E_sStatusT.Loan_LoanPurchased, "Loan Sold" },
            { E_sStatusT.Loan_LoanSubmitted, "Loan Submitted" },
            { E_sStatusT.Loan_PreProcessing, "Pre-Processing" }, // start sk 1/6/2014 opm 145251
            { E_sStatusT.Loan_DocumentCheck, "Document Check" },
            { E_sStatusT.Loan_DocumentCheckFailed, "Document Check Failed" },
            { E_sStatusT.Loan_PreUnderwriting, "Pre-Underwriting" },
            { E_sStatusT.Loan_ConditionReview, "Condition Review" },
            { E_sStatusT.Loan_PreDocQC, "Pre-Doc QC" },
            { E_sStatusT.Loan_DocsOrdered, "Docs Ordered" },
            { E_sStatusT.Loan_DocsDrawn, "Docs Drawn" },
            { E_sStatusT.Loan_InvestorConditions, "Investor Conditions" }, // tied to sSuspendedByInvestorD
            { E_sStatusT.Loan_InvestorConditionsSent, "Investor Conditions Sent" }, // tied to sCondSentToInvestorD
            { E_sStatusT.Loan_ReadyForSale, "Ready For Sale" },
            { E_sStatusT.Loan_SubmittedForPurchaseReview, "Submitted For Purchase Review" },
            { E_sStatusT.Loan_InPurchaseReview, "In Purchase Review" },
            { E_sStatusT.Loan_PrePurchaseConditions, "Pre-Purchase Conditions" },
            { E_sStatusT.Loan_SubmittedForFinalPurchaseReview, "Submitted For Final Purchase Review" },
            { E_sStatusT.Loan_InFinalPurchaseReview, "In Final Purchase Review" },
            { E_sStatusT.Loan_ClearToPurchase, "Clear To Purchase" },
            { E_sStatusT.Loan_Purchased, "Loan Purchased" }, // don't confuse with the old E_sStatusT.Loan_LoanPurchased, which is now Loan Sold in the UI.
            { E_sStatusT.Loan_CounterOffer, "Counter Offer Approved" },
            { E_sStatusT.Loan_Withdrawn, "Loan Withdrawn" },
            { E_sStatusT.Loan_Archived, "Loan Archived" },
        };

        /// <summary>
        /// Description to Status Dictionary.
        /// </summary>
        private static readonly Dictionary<string, E_sStatusT> Description2StatusDict =
            Status2DescriptionDict.ToDictionary(p => p.Value, p => p.Key, StringComparer.OrdinalIgnoreCase);

        /// <summary>
        /// Get Status Description.
        /// </summary>
        /// <param name="status">E_sStatusT status.</param>
        /// <returns>Status Description.</returns>
        public static string GetStatusDescription(E_sStatusT status)
        {
            return Status2DescriptionDict.ContainsKey(status) ? Status2DescriptionDict[status] : string.Empty;
        }

        /// <summary>
        /// Get Status Description.
        /// </summary>
        /// <param name="status">E_sStatusT status.</param>
        /// <returns>Status Description.</returns>
        public static E_sStatusT? GetStatusFromDescription(string status)
        {
            return Description2StatusDict.ContainsKey(status) ? Description2StatusDict[status] : (E_sStatusT?)null;
        }

        /// <summary>
        /// Get list of Loan Status.
        /// </summary>
        /// <param name="channel">Branch Channel.</param>
        /// <param name="process">Correspondent Process.</param>
        /// <param name="brokerId">Broker Id.</param>
        /// <returns>A list of Loan Statuses.</returns>
        public static E_sStatusT[] GetLoanStatus(E_BranchChannelT channel, E_sCorrespondentProcessT process, Guid brokerId)
        {
            var broker = BrokerDB.RetrieveById(brokerId);
            var enabledStatusesForThisChannel = broker.GetEnabledStatusesByChannelAndProcessType(channel, process);
            return enabledStatusesForThisChannel.Intersect(LoanStatuses)
                .OrderBy(s => CPageBase.s_OrderByStatusT[s])
                .ToArray();
        }

        /// <summary>
        /// Get list of Loan Status.
        /// </summary>
        /// <param name="channel">Branch Channel.</param>
        /// <param name="process">Correspondent Process.</param>
        /// <param name="brokerId">Broker Id.</param>
        /// <returns>A list of Loan Statuses.</returns>
        public static KeyValuePair<E_sStatusT, string>[] GetLoanStatusWithDesc(E_BranchChannelT channel, E_sCorrespondentProcessT process, Guid brokerId)
        {
            return GetLoanStatus(channel, process, brokerId)
                .Select(status => new KeyValuePair<E_sStatusT, string>(status, GetStatusDescription(status)))
                .ToArray();
        }

        /// <summary>
        /// Get list of Lead Status.
        /// </summary>
        /// <returns>A list of Loan Statuses.</returns>
        public static KeyValuePair<E_sStatusT, string>[] GetLeadStatusWithDesc()
        {
            return new[] 
                {
                    E_sStatusT.Lead_New,
                    E_sStatusT.Lead_Canceled,
                    E_sStatusT.Lead_Declined,
                    E_sStatusT.Lead_Other
                }
                .Select(status => new KeyValuePair<E_sStatusT, string>(status, GetStatusDescription(status)))
                .ToArray();
        }
    }
}
