﻿namespace DataAccess.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.ObjLib.Rolodex;
    using LendersOffice.Security;
    
    /// <summary>
    /// Services for Goodbye Letter.
    /// </summary>
    public static class GoodbyeLetter
    {
        /// <summary>
        /// Get the next payment date after the acceptingD parameter. Use logic similar to what is used on the servicing page.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="acceptingD">Accepting Date.</param>
        /// <returns>The next payment date.</returns>
        public static string GetSchedDateOfFirstPmt(Guid loanId, string acceptingD)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(GoodbyeLetter));
            dataLoan.InitLoad();

            var pmts = dataLoan.sServicingPayments;
            CDateTime accCD = CDateTime.Create(acceptingD, null);
            if (!accCD.IsValid)
            {
                return string.Empty;
            }

            DateTime accD = accCD.DateTimeForComputation;
            DateTime lastDueD = DateTime.MinValue;
            CDateTime nextDueD = CDateTime.InvalidWrapValue;

            // Find the first payment with a date >= the accepting date
            foreach (var pmt in pmts)
            {
                if (pmt.ServicingTransactionT.ToLower() == CPageBase.sServicingTransT_map[1].ToLower())
                {
                    lastDueD = pmt.DueD;

                    if (accD <= pmt.DueD)
                    {
                        nextDueD = CDateTime.Create(pmt.DueD);
                        break;
                    }
                }
            }

            // If no valid date was found, increment the last due date
            if (!nextDueD.IsValid)
            {
                // If there were no payments, set the last due date to the first
                // scheduled due date.
                if (pmts.Count == 0 && dataLoan.sSchedDueD1.IsValid)
                {
                    lastDueD = dataLoan.sSchedDueD1.DateTimeForComputation;
                }

                if (lastDueD != DateTime.MinValue)
                {
                    while (accD > lastDueD)
                    {
                        lastDueD = lastDueD.AddMonths(1);
                    }

                    nextDueD = CDateTime.Create(lastDueD);
                }
            }

            return nextDueD.ToString(dataLoan.m_convertLos);
        }

        /// <summary>
        /// Get the list of Investors.
        /// </summary>
        /// <returns>The list of Investors.</returns>
        public static Dictionary<string, string> GetInvestorNames()
        {
            List<InvestorRolodexEntry> entries = InvestorRolodexEntry.GetAll(PrincipalFactory.CurrentPrincipal.BrokerId, null).ToList();
            var nameToIdMap = new Dictionary<string, string>();
            foreach (var entry in entries.Where(p => p.Status == E_InvestorStatus.Active))
            {
                if (!nameToIdMap.ContainsKey(entry.InvestorName))
                {
                    nameToIdMap.Add(entry.InvestorName, entry.Id.ToString());
                }
            }

            return nameToIdMap;
        }
    }
}
