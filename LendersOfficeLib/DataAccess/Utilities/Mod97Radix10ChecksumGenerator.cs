﻿// <copyright file="Mod97Radix10ChecksumGenerator.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Michael Leinweaver
//    Date:   12/27/2016
// </summary>
namespace DataAccess.Utilities
{
    using System;

    /// <summary>
    /// Utility class for generating checksums using the ISO/IEC 7064, MOD 97-10 approach.
    /// </summary>
    /// <remarks>
    /// This class was adapted from the Apache IBAN ISO/IEC 7064 algorithm available at
    /// https://commons.apache.org/proper/commons-validator/apidocs/src-html/org/apache/commons/validator/routines/checkdigit/IBANCheckDigit.html.
    /// </remarks>
    public class Mod97Radix10ChecksumGenerator
    {
        /// <summary>
        /// Represents the modulus for the generator.
        /// </summary>
        private const int Modulus = 97;

        /// <summary>
        /// Represents the radix for the generator.
        /// </summary>
        private const int Radix = 10;

        /// <summary>
        /// Represents the number of check digits in the result.
        /// </summary>
        private const int NumCheckDigits = 2;

        /// <summary>
        /// Represents the maximum size of an intermediate value before
        /// a modulus operation is performed.
        /// </summary>
        private const long MaxIntermediateValueBeforeMod = 999999999L;

        /// <summary>
        /// Represents the character set for the generator.
        /// </summary>
        private const string CharacterSet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        /// <summary>
        /// Computes the check digits for the given <paramref name="value"/>.
        /// </summary>
        /// <param name="value">
        /// The value from which the check digits will be computed.
        /// </param>
        /// <returns>
        /// The computed check digits.
        /// </returns>
        /// <exception cref="ArgumentException">
        /// <paramref name="value"/> is null, empty, whitespace, or has a length smaller than the 
        /// number of check digit characters.
        /// </exception>
        public string ComputeCheckDigits(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(value));
            }

            if (value.Length <= NumCheckDigits)
            {
                throw new ArgumentException($"Value length should be greater than {NumCheckDigits}", nameof(value));
            }

            value = value.ToUpper() + "00";

            var total = 0L;
            foreach (var character in value)
            {
                var numericValue = CharacterSet.IndexOf(character);
                if (numericValue < 0)
                {
                    throw new ArgumentException($"Illegal character {character} in input.", nameof(value));
                }

                total = (numericValue > Radix - 1 ? (total * Radix * Radix) : total * Radix) + numericValue;

                if (total > MaxIntermediateValueBeforeMod)
                {
                    total %= Modulus;
                }
            }

            total = (Modulus + 1) - (total % Modulus);

            // Add a leading zero for single digit checksums.
            return total > 9 ? total.ToString() : "0" + total;
        }
    }
}
