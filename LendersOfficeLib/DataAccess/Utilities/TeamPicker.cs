﻿namespace DataAccess.Utilities
{
    using System.Collections.Generic;
    using LendersOffice.Security;

    /// <summary>
    /// Services for Team Picker page.
    /// </summary>
    public class TeamPicker
    {
        /// <summary>
        /// Get list of teams.
        /// </summary>
        /// <param name="isAssignedTeam">Is Assigned Team or Is All Teams.</param>
        /// <param name="roleString">The role string.</param>
        /// <returns>The list of teams.</returns>
        public static List<Team> Search(bool isAssignedTeam, string roleString)
        {
            Role role = Role.Get(roleString);
            List<Team> teams = new List<Team>();
            if (isAssignedTeam)
            {
                teams = (List<Team>)Team.ListTeamsByUserInRole(PrincipalFactory.CurrentPrincipal.EmployeeId, role.Id);
            }
            else
            {
                List<Team> allTeams = (List<Team>)Team.ListTeamsAtLender(string.Empty);
                foreach (Team team in allTeams)
                {
                    if (role.Id == team.RoleId)
                    {
                        teams.Add(team);
                    }
                }
            }

            return teams;
        }
    }
}
