﻿// <copyright file="AgentCommission.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Huy Nguyen
//  Date:   1/20/2017
// </summary>

namespace DataAccess.Utilities
{
    using System;

    /// <summary>
    /// Data for an agent commission.
    /// </summary>
    public class AgentCommission
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AgentCommission"/> class.
        /// </summary>
        public AgentCommission()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AgentCommission"/> class from an agent data.
        /// </summary>
        /// <param name="agent">Agent data to be converted.</param>
        public AgentCommission(CAgentFields agent)
        {
            this.AgentType = agent.AgentRoleDescription;
            this.AgentName = agent.AgentName;
            this.CompanyName = agent.CompanyName;
            this.CommissionAmount = agent.Commission_rep;
            this.IsPaid = agent.IsCommissionPaid;
            this.Notes = agent.PaymentNotes;
            this.RecordId = agent.RecordId;
        }

        /// <summary>
        /// Gets the agent type.
        /// </summary>
        /// <value>The agent type.</value>
        public string AgentType { get; private set; }

        /// <summary>
        /// Gets the agent name.
        /// </summary>
        /// <value>The agent name.</value>
        public string AgentName { get; private set; }

        /// <summary>
        /// Gets the company name.
        /// </summary>
        /// <value>The company name.</value>
        public string CompanyName { get; private set; }

        /// <summary>
        /// Gets the commission amount.
        /// </summary>
        /// <value>The commission amount.</value>
        public string CommissionAmount { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether the commission is paid or not.
        /// </summary>
        /// <value>A value indicating whether the commission is paid or not.</value>
        public bool IsPaid { get; set; }

        /// <summary>
        /// Gets or sets the note.
        /// </summary>
        /// <value>The note data.</value>
        public string Notes { get; set; }

        /// <summary>
        /// Gets the record Id.
        /// </summary>
        /// <value>The record Id.</value>
        public Guid RecordId { get; private set; }
    }
}
