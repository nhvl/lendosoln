using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
	public class CCustomFormData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CCustomFormData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("BranchNm");
            list.Add("sBranchId");
            list.Add("sfTransformLoPropertyTypeToPml");
            list.Add("sLNm");
            list.Add("aBFirstNm");
            list.Add("aBMidNm");
            list.Add("aBLastNm");
            list.Add("aBSuffix");
            list.Add("aBNm");
            list.Add("aBSsn");
            list.Add("aBDob");
            list.Add("aBHPhone");
            list.Add("aBBusPhone");
            list.Add("aBCellPhone");
            list.Add("aBEmail");

            list.Add("aCFirstNm");
            list.Add("aCMidNm");
            list.Add("aCLastNm");
            list.Add("aCSuffix");
            list.Add("aCNm");
            list.Add("aCSsn");
            list.Add("aCDob");
            list.Add("aCHPhone");
            list.Add("aCBusPhone");
            list.Add("aCCellPhone");
            list.Add("aCEmail");

            list.Add("aBAddr");
            list.Add("aBCity");
            list.Add("aBState");
            list.Add("aBZip");
            list.Add("sSpAddr");
            list.Add("sSpCity");
            list.Add("sSpState");
            list.Add("sSpZip");
            list.Add("sSpCounty");

            list.Add("sLpTemplateNm");
            list.Add("sLPurposeT");
            list.Add("sLienPosT");
            list.Add("sLT");
            list.Add("sPurchPrice");
            list.Add("sApprVal");
            list.Add("sEquity");
            list.Add("sLAmt");
            list.Add("sNoteIR");
            list.Add("sQualIR");
            list.Add("sTerm");
            list.Add("sDue");
            list.Add("sGseSpT");
            list.Add("sLtvR");
            list.Add("sCltvR");
            list.Add("sHcltvR");
            list.Add("sQualTopR");
            list.Add("sQualBottomR");
            list.Add("sMonthlyPmt");
            list.Add("sLOrigFPc");
            list.Add("sLOrigF");

            list.Add("sEmployeeLoanRep");
            list.Add("sEmployeeProcessor");
            list.Add("sEmployeeLenderAccExec");
            list.Add("sOpenedD");
            list.Add("sEstCloseD");
            list.Add("sClosedD");
            list.Add("sRLckdD");
            list.Add("sRLckdExpiredD");
            list.Add("sfGetAgentOfRole");

            list.Add("sUnitsNum");
            list.Add("sYrBuilt");
            list.Add("sProfitRebate");
            list.Add("sGrossProfit");
            list.Add("sCommissionTotal");
            list.Add("sNetProfit");

            list.Add("sPrelimRprtOd");   
            list.Add("sApprRprtOd");
            list.Add("sPreQualD");
            list.Add("sPreApprovD");
            list.Add("sSubmitD");
            list.Add("sApprovD");
            list.Add("sDocsD");
            list.Add("sFundD");
            list.Add("sRecordedD");
            list.Add("sOnHoldD");
            list.Add("sCanceledD");
            list.Add("sRejectD");
            list.Add("sSuspendedD");
            list.Add("sDemandYieldSpreadPremiumLenderAmt");
            list.Add("sDemandYieldSpreadPremiumBrokerAmt");
            list.Add("sApprF");
            list.Add("sCrF");
            list.Add("sMBrokF");
            list.Add("sProcF");
            list.Add("sUwF");
            list.Add("s800U1F");
            list.Add("s800U2F");
            list.Add("s800U3F");
            list.Add("s800U4F");
            list.Add("s800U5F");
            list.Add("sTransNetCash");
            list.Add("sRAdjMarginR");
            list.Add("sBrokComp1");
            list.Add("sLDiscntPc");
            list.Add("sLDiscnt");
            list.Add("aOccT");
            list.Add("sU1Sc");
            list.Add("sU2Sc");            
            list.Add("sU3Sc");
            list.Add("sU4Sc");
            list.Add("sU5Sc");
            list.Add("sProThisMPmt");
            list.Add("sProRealETx");
            list.Add("sProMIns");
            list.Add("sProHazIns");
            list.Add("sRAdj1stCapR");
            list.Add("sRAdj1stCapMon");
            list.Add("sRAdjCapR");
            list.Add("sRAdjCapMon");
            list.Add("sRAdjLifeCapR");
            list.Add("sRAdjIndexR");
            list.Add("aBExperianScore");
            list.Add("aBTransUnionScore");
            list.Add("aBEquifaxScore");
            list.Add("aCExperianScore");
            list.Add("aCTransUnionScore");
            list.Add("aCEquifaxScore");
            list.Add("sCreditScoreType1");
            list.Add("sCreditScoreType2");
            list.Add("sProdDocT");
            list.Add("sProdCashoutAmt");
            list.Add("sPpmtPenaltyMon");
            list.Add("sProdImpound");
            list.Add("sIOnlyMon");
            list.Add("sProdAvailReserveMonths");
            list.Add("sIsIOnly");
            list.Add("sArmIndexT");
            list.Add("sFreddieArmIndexT");
            list.Add("sBrokComp1Pc");
			list.Add("sSubFin");
			list.Add("sSubFinTerm");
			list.Add("sSubFinIR");
			list.Add("sSubFinPmt");
			list.Add("aProdBCitizenT");
            list.Add("sSchedDueD1");
			list.Add("sProHoAssocDues");
			list.Add("sProOHExp");

			list.Add("aBAddrMail");
			list.Add("aBCityMail");
			list.Add("aBStateMail");
			list.Add("aBZipMail");
			list.Add("sInvestLNum");
			list.Add("sGoodByLetterD");
			list.Add("sLPurchaseD");
			list.Add("sLenContractD");

			list.Add("sPreparerXmlContent");
			list.Add("sServicingStartD");

			list.Add("sFinMethT");
			list.Add("sFinMethDesc");

			list.Add("aBEmplmtXmlContent");
			list.Add("aCEmplmtXmlContent");
			list.Add("aBEmplrBusPhoneLckd");
			list.Add("aCEmplrBusPhoneLckd");
			list.Add("aBBusPhone");
			list.Add("aCBusPhone");

			list.Add("sStatusT");

            list.Add("sApprInfo"); // OPM 16556 - Add Appraisal Comments field
            list.Add("sMBrokFPc"); // OPM 24786 - Add Broker Fee Percent field

            #region OPM 20827
            list.Add("sLTotI");
            list.Add("sPresLTotPersistentHExp");
            list.Add("sUnderwritingD");
            list.Add("sFannieDocT");
            list.Add("sAusRecommendation");
            list.Add("sWillEscrowBeWaived");
            list.Add("sProdImpound");
            list.Add("aTransmOMonPmt");
            #endregion

            // 11/1/07 db - OPM 18245 - Add custom fields
			#region Custom Form 1 Set
			list.Add("sCustomField1Desc");
			list.Add("sCustomField1D");
			list.Add("sCustomField1Money");
			list.Add("sCustomField1Pc");
			list.Add("sCustomField1Bit");
			list.Add("sCustomField1Notes");
			#endregion

			#region Custom Form 2 Set
			list.Add("sCustomField2Desc");
			list.Add("sCustomField2D");
			list.Add("sCustomField2Money");
			list.Add("sCustomField2Pc");
			list.Add("sCustomField2Bit");
			list.Add("sCustomField2Notes");
			#endregion

			#region Custom Form 3 Set
			list.Add("sCustomField3Desc");
			list.Add("sCustomField3D");
			list.Add("sCustomField3Money");
			list.Add("sCustomField3Pc");
			list.Add("sCustomField3Bit");
			list.Add("sCustomField3Notes");
			#endregion

			#region Custom Form 4 Set
			list.Add("sCustomField4Desc");
			list.Add("sCustomField4D");
			list.Add("sCustomField4Money");
			list.Add("sCustomField4Pc");
			list.Add("sCustomField4Bit");
			list.Add("sCustomField4Notes");
			#endregion

			#region Custom Form 5 Set
			list.Add("sCustomField5Desc");
			list.Add("sCustomField5D");
			list.Add("sCustomField5Money");
			list.Add("sCustomField5Pc");
			list.Add("sCustomField5Bit");
			list.Add("sCustomField5Notes");
			#endregion

			#region Custom Form 6 Set
			list.Add("sCustomField6Desc");
			list.Add("sCustomField6D");
			list.Add("sCustomField6Money");
			list.Add("sCustomField6Pc");
			list.Add("sCustomField6Bit");
			list.Add("sCustomField6Notes");
			#endregion

			#region Custom Form 7 Set
			list.Add("sCustomField7Desc");
			list.Add("sCustomField7D");
			list.Add("sCustomField7Money");
			list.Add("sCustomField7Pc");
			list.Add("sCustomField7Bit");
			list.Add("sCustomField7Notes");
			#endregion

			#region Custom Form 8 Set
			list.Add("sCustomField8Desc");
			list.Add("sCustomField8D");
			list.Add("sCustomField8Money");
			list.Add("sCustomField8Pc");
			list.Add("sCustomField8Bit");
			list.Add("sCustomField8Notes");
			#endregion

			#region Custom Form 9 Set
			list.Add("sCustomField9Desc");
			list.Add("sCustomField9D");
			list.Add("sCustomField9Money");
			list.Add("sCustomField9Pc");
			list.Add("sCustomField9Bit");
			list.Add("sCustomField9Notes");
			#endregion

			#region Custom Form 10 Set
			list.Add("sCustomField10Desc");
			list.Add("sCustomField10D");
			list.Add("sCustomField10Money");
			list.Add("sCustomField10Pc");
			list.Add("sCustomField10Bit");
			list.Add("sCustomField10Notes");
			#endregion

			#region Custom Form 11 Set
			list.Add("sCustomField11Desc");
			list.Add("sCustomField11D");
			list.Add("sCustomField11Money");
			list.Add("sCustomField11Pc");
			list.Add("sCustomField11Bit");
			list.Add("sCustomField11Notes");
			#endregion

			#region Custom Form 12 Set
			list.Add("sCustomField12Desc");
			list.Add("sCustomField12D");
			list.Add("sCustomField12Money");
			list.Add("sCustomField12Pc");
			list.Add("sCustomField12Bit");
			list.Add("sCustomField12Notes");
			#endregion

			#region Custom Form 13 Set
			list.Add("sCustomField13Desc");
			list.Add("sCustomField13D");
			list.Add("sCustomField13Money");
			list.Add("sCustomField13Pc");
			list.Add("sCustomField13Bit");
			list.Add("sCustomField13Notes");
			#endregion

			#region Custom Form 14 Set
			list.Add("sCustomField14Desc");
			list.Add("sCustomField14D");
			list.Add("sCustomField14Money");
			list.Add("sCustomField14Pc");
			list.Add("sCustomField14Bit");
			list.Add("sCustomField14Notes");
			#endregion

			#region Custom Form 15 Set
			list.Add("sCustomField15Desc");
			list.Add("sCustomField15D");
			list.Add("sCustomField15Money");
			list.Add("sCustomField15Pc");
			list.Add("sCustomField15Bit");
			list.Add("sCustomField15Notes");
			#endregion

			#region Custom Form 16 Set
			list.Add("sCustomField16Desc");
			list.Add("sCustomField16D");
			list.Add("sCustomField16Money");
			list.Add("sCustomField16Pc");
			list.Add("sCustomField16Bit");
			list.Add("sCustomField16Notes");
			#endregion

			#region Custom Form 17 Set
			list.Add("sCustomField17Desc");
			list.Add("sCustomField17D");
			list.Add("sCustomField17Money");
			list.Add("sCustomField17Pc");
			list.Add("sCustomField17Bit");
			list.Add("sCustomField17Notes");
			#endregion

			#region Custom Form 18 Set
			list.Add("sCustomField18Desc");
			list.Add("sCustomField18D");
			list.Add("sCustomField18Money");
			list.Add("sCustomField18Pc");
			list.Add("sCustomField18Bit");
			list.Add("sCustomField18Notes");
			#endregion

			#region Custom Form 19 Set
			list.Add("sCustomField19Desc");
			list.Add("sCustomField19D");
			list.Add("sCustomField19Money");
			list.Add("sCustomField19Pc");
			list.Add("sCustomField19Bit");
			list.Add("sCustomField19Notes");
			#endregion

			#region Custom Form 20 Set
			list.Add("sCustomField20Desc");
			list.Add("sCustomField20D");
			list.Add("sCustomField20Money");
			list.Add("sCustomField20Pc");
			list.Add("sCustomField20Bit");
			list.Add("sCustomField20Notes");
			#endregion

            #region Custom Form 21 Set
            list.Add("sCustomField21Desc");
            list.Add("sCustomField21D");
            list.Add("sCustomField21Money");
            list.Add("sCustomField21Pc");
            list.Add("sCustomField21Bit");
            list.Add("sCustomField21Notes");
            #endregion

            #region Custom Form 22 Set
            list.Add("sCustomField22Desc");
            list.Add("sCustomField22D");
            list.Add("sCustomField22Money");
            list.Add("sCustomField22Pc");
            list.Add("sCustomField22Bit");
            list.Add("sCustomField22Notes");
            #endregion

            #region Custom Form 23 Set
            list.Add("sCustomField23Desc");
            list.Add("sCustomField23D");
            list.Add("sCustomField23Money");
            list.Add("sCustomField23Pc");
            list.Add("sCustomField23Bit");
            list.Add("sCustomField23Notes");
            #endregion

            #region Custom Form 24 Set
            list.Add("sCustomField24Desc");
            list.Add("sCustomField24D");
            list.Add("sCustomField24Money");
            list.Add("sCustomField24Pc");
            list.Add("sCustomField24Bit");
            list.Add("sCustomField24Notes");
            #endregion

            #region Custom Form 25 Set
            list.Add("sCustomField25Desc");
            list.Add("sCustomField25D");
            list.Add("sCustomField25Money");
            list.Add("sCustomField25Pc");
            list.Add("sCustomField25Bit");
            list.Add("sCustomField25Notes");
            #endregion

            #region Custom Form 26 Set
            list.Add("sCustomField26Desc");
            list.Add("sCustomField26D");
            list.Add("sCustomField26Money");
            list.Add("sCustomField26Pc");
            list.Add("sCustomField26Bit");
            list.Add("sCustomField26Notes");
            #endregion

            #region Custom Form 27 Set
            list.Add("sCustomField27Desc");
            list.Add("sCustomField27D");
            list.Add("sCustomField27Money");
            list.Add("sCustomField27Pc");
            list.Add("sCustomField27Bit");
            list.Add("sCustomField27Notes");
            #endregion

            #region Custom Form 28 Set
            list.Add("sCustomField28Desc");
            list.Add("sCustomField28D");
            list.Add("sCustomField28Money");
            list.Add("sCustomField28Pc");
            list.Add("sCustomField28Bit");
            list.Add("sCustomField28Notes");
            #endregion

            #region Custom Form 29 Set
            list.Add("sCustomField29Desc");
            list.Add("sCustomField29D");
            list.Add("sCustomField29Money");
            list.Add("sCustomField29Pc");
            list.Add("sCustomField29Bit");
            list.Add("sCustomField29Notes");
            #endregion

            #region Custom Form 30 Set
            list.Add("sCustomField30Desc");
            list.Add("sCustomField30D");
            list.Add("sCustomField30Money");
            list.Add("sCustomField30Pc");
            list.Add("sCustomField30Bit");
            list.Add("sCustomField30Notes");
            #endregion

            #region Custom Form 31 Set
            list.Add("sCustomField31Desc");
            list.Add("sCustomField31D");
            list.Add("sCustomField31Money");
            list.Add("sCustomField31Pc");
            list.Add("sCustomField31Bit");
            list.Add("sCustomField31Notes");
            #endregion

            #region Custom Form 32 Set
            list.Add("sCustomField32Desc");
            list.Add("sCustomField32D");
            list.Add("sCustomField32Money");
            list.Add("sCustomField32Pc");
            list.Add("sCustomField32Bit");
            list.Add("sCustomField32Notes");
            #endregion

            #region Custom Form 33 Set
            list.Add("sCustomField33Desc");
            list.Add("sCustomField33D");
            list.Add("sCustomField33Money");
            list.Add("sCustomField33Pc");
            list.Add("sCustomField33Bit");
            list.Add("sCustomField33Notes");
            #endregion

            #region Custom Form 34 Set
            list.Add("sCustomField34Desc");
            list.Add("sCustomField34D");
            list.Add("sCustomField34Money");
            list.Add("sCustomField34Pc");
            list.Add("sCustomField34Bit");
            list.Add("sCustomField34Notes");
            #endregion

            #region Custom Form 35 Set
            list.Add("sCustomField35Desc");
            list.Add("sCustomField35D");
            list.Add("sCustomField35Money");
            list.Add("sCustomField35Pc");
            list.Add("sCustomField35Bit");
            list.Add("sCustomField35Notes");
            #endregion

            #region Custom Form 36 Set
            list.Add("sCustomField36Desc");
            list.Add("sCustomField36D");
            list.Add("sCustomField36Money");
            list.Add("sCustomField36Pc");
            list.Add("sCustomField36Bit");
            list.Add("sCustomField36Notes");
            #endregion

            #region Custom Form 37 Set
            list.Add("sCustomField37Desc");
            list.Add("sCustomField37D");
            list.Add("sCustomField37Money");
            list.Add("sCustomField37Pc");
            list.Add("sCustomField37Bit");
            list.Add("sCustomField37Notes");
            #endregion

            #region Custom Form 38 Set
            list.Add("sCustomField38Desc");
            list.Add("sCustomField38D");
            list.Add("sCustomField38Money");
            list.Add("sCustomField38Pc");
            list.Add("sCustomField38Bit");
            list.Add("sCustomField38Notes");
            #endregion

            #region Custom Form 39 Set
            list.Add("sCustomField39Desc");
            list.Add("sCustomField39D");
            list.Add("sCustomField39Money");
            list.Add("sCustomField39Pc");
            list.Add("sCustomField39Bit");
            list.Add("sCustomField39Notes");
            #endregion

            #region Custom Form 40 Set
            list.Add("sCustomField40Desc");
            list.Add("sCustomField40D");
            list.Add("sCustomField40Money");
            list.Add("sCustomField40Pc");
            list.Add("sCustomField40Bit");
            list.Add("sCustomField40Notes");
            #endregion

            #region Custom Form 41 Set
            list.Add("sCustomField41Desc");
            list.Add("sCustomField41D");
            list.Add("sCustomField41Money");
            list.Add("sCustomField41Pc");
            list.Add("sCustomField41Bit");
            list.Add("sCustomField41Notes");
            #endregion

            #region Custom Form 42 Set
            list.Add("sCustomField42Desc");
            list.Add("sCustomField42D");
            list.Add("sCustomField42Money");
            list.Add("sCustomField42Pc");
            list.Add("sCustomField42Bit");
            list.Add("sCustomField42Notes");
            #endregion

            #region Custom Form 43 Set
            list.Add("sCustomField43Desc");
            list.Add("sCustomField43D");
            list.Add("sCustomField43Money");
            list.Add("sCustomField43Pc");
            list.Add("sCustomField43Bit");
            list.Add("sCustomField43Notes");
            #endregion

            #region Custom Form 44 Set
            list.Add("sCustomField44Desc");
            list.Add("sCustomField44D");
            list.Add("sCustomField44Money");
            list.Add("sCustomField44Pc");
            list.Add("sCustomField44Bit");
            list.Add("sCustomField44Notes");
            #endregion

            #region Custom Form 45 Set
            list.Add("sCustomField45Desc");
            list.Add("sCustomField45D");
            list.Add("sCustomField45Money");
            list.Add("sCustomField45Pc");
            list.Add("sCustomField45Bit");
            list.Add("sCustomField45Notes");
            #endregion

            #region Custom Form 46 Set
            list.Add("sCustomField46Desc");
            list.Add("sCustomField46D");
            list.Add("sCustomField46Money");
            list.Add("sCustomField46Pc");
            list.Add("sCustomField46Bit");
            list.Add("sCustomField46Notes");
            #endregion

            #region Custom Form 47 Set
            list.Add("sCustomField47Desc");
            list.Add("sCustomField47D");
            list.Add("sCustomField47Money");
            list.Add("sCustomField47Pc");
            list.Add("sCustomField47Bit");
            list.Add("sCustomField47Notes");
            #endregion

            #region Custom Form 48 Set
            list.Add("sCustomField48Desc");
            list.Add("sCustomField48D");
            list.Add("sCustomField48Money");
            list.Add("sCustomField48Pc");
            list.Add("sCustomField48Bit");
            list.Add("sCustomField48Notes");
            #endregion

            #region Custom Form 49 Set
            list.Add("sCustomField49Desc");
            list.Add("sCustomField49D");
            list.Add("sCustomField49Money");
            list.Add("sCustomField49Pc");
            list.Add("sCustomField49Bit");
            list.Add("sCustomField49Notes");
            #endregion

            #region Custom Form 50 Set
            list.Add("sCustomField50Desc");
            list.Add("sCustomField50D");
            list.Add("sCustomField50Money");
            list.Add("sCustomField50Pc");
            list.Add("sCustomField50Bit");
            list.Add("sCustomField50Notes");
            #endregion

            #region Custom Form 51 Set
            list.Add("sCustomField51Desc");
            list.Add("sCustomField51D");
            list.Add("sCustomField51Money");
            list.Add("sCustomField51Pc");
            list.Add("sCustomField51Bit");
            list.Add("sCustomField51Notes");
            #endregion

            #region Custom Form 52 Set
            list.Add("sCustomField52Desc");
            list.Add("sCustomField52D");
            list.Add("sCustomField52Money");
            list.Add("sCustomField52Pc");
            list.Add("sCustomField52Bit");
            list.Add("sCustomField52Notes");
            #endregion

            #region Custom Form 53 Set
            list.Add("sCustomField53Desc");
            list.Add("sCustomField53D");
            list.Add("sCustomField53Money");
            list.Add("sCustomField53Pc");
            list.Add("sCustomField53Bit");
            list.Add("sCustomField53Notes");
            #endregion

            #region Custom Form 54 Set
            list.Add("sCustomField54Desc");
            list.Add("sCustomField54D");
            list.Add("sCustomField54Money");
            list.Add("sCustomField54Pc");
            list.Add("sCustomField54Bit");
            list.Add("sCustomField54Notes");
            #endregion

            #region Custom Form 55 Set
            list.Add("sCustomField55Desc");
            list.Add("sCustomField55D");
            list.Add("sCustomField55Money");
            list.Add("sCustomField55Pc");
            list.Add("sCustomField55Bit");
            list.Add("sCustomField55Notes");
            #endregion

            #region Custom Form 56 Set
            list.Add("sCustomField56Desc");
            list.Add("sCustomField56D");
            list.Add("sCustomField56Money");
            list.Add("sCustomField56Pc");
            list.Add("sCustomField56Bit");
            list.Add("sCustomField56Notes");
            #endregion

            #region Custom Form 57 Set
            list.Add("sCustomField57Desc");
            list.Add("sCustomField57D");
            list.Add("sCustomField57Money");
            list.Add("sCustomField57Pc");
            list.Add("sCustomField57Bit");
            list.Add("sCustomField57Notes");
            #endregion

            #region Custom Form 58 Set
            list.Add("sCustomField58Desc");
            list.Add("sCustomField58D");
            list.Add("sCustomField58Money");
            list.Add("sCustomField58Pc");
            list.Add("sCustomField58Bit");
            list.Add("sCustomField58Notes");
            #endregion

            #region Custom Form 59 Set
            list.Add("sCustomField59Desc");
            list.Add("sCustomField59D");
            list.Add("sCustomField59Money");
            list.Add("sCustomField59Pc");
            list.Add("sCustomField59Bit");
            list.Add("sCustomField59Notes");
            #endregion

            #region Custom Form 60 Set
            list.Add("sCustomField60Desc");
            list.Add("sCustomField60D");
            list.Add("sCustomField60Money");
            list.Add("sCustomField60Pc");
            list.Add("sCustomField60Bit");
            list.Add("sCustomField60Notes");
            #endregion

	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CCustomFormData(Guid fileId) : base(fileId, "CCustomFormData", s_selectProvider)
		{
		}
	}
}
