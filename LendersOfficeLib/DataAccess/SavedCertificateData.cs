using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
	public class CSavedCertificateData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CSavedCertificateData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("sPmlCertXmlContent");
            list.Add("sPmlSubmitStatusT");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CSavedCertificateData(Guid fileId) : base(fileId, "CSavedCertificateData", s_selectProvider)
		{
		}
	}
}
