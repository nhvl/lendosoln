using System;
using System.Collections;
using System.Data;

namespace DataAccess
{

	public class CPdfEncryptionPasswordData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CPdfEncryptionPasswordData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
	        list.Add("sSpZip");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CPdfEncryptionPasswordData(Guid fileId) : base(fileId, "CPdfEncryptionPasswordData", s_selectProvider)
		{
		}
	}
}
