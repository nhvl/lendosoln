/// Author: David Dao

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using LendersOffice.Common;
using LendersOffice.CreditReport;
using LendersOffice.Security;

namespace DataAccess
{

    public class CPublicRecordAuditItem
    {
        private string m_userName;
        private string m_eventDate;
        private string m_description = null;
        public CPublicRecordAuditItem(string user, string date, string description)
        {
            m_userName = user;
            m_eventDate = date;
            m_description = description;
        }
        public string UserName
        {
            get { return m_userName; }
        }
        public string EventDate
        {
            get { return m_eventDate; }
        }
        public string Description
        {
            get { return m_description; }
        }
    }
	public class CPublicRecordFields : CXmlRecordBase2, IPublicRecord
    {
        private class CFieldAudit
        {
            public string OldValue = "";
            public string NewValue = "";
        }
        private bool m_requireAuditLog = false;
        private Hashtable m_hash = new Hashtable();

        private bool m_isNewRecord = false;

        internal static IPublicRecord Create(CAppBase parent, DataSet ds, CPublicRecordCollection coll) 
        {
            return new CPublicRecordFields(parent, ds, coll);
        }

        internal static IPublicRecord Reconstruct(CAppBase parent, DataSet ds, IRecordCollection recordList, int iRow) 
        {
            return new CPublicRecordFields(parent, ds, recordList, iRow);
        }
        internal static IPublicRecord Reconstruct(CAppBase parent, DataSet ds, IRecordCollection recordList, Guid id) 
        {
            return new CPublicRecordFields(parent, ds, recordList, id);
        }

        // DO NOT USE!  CREATED JUST TO ENABLE SOME QUICK UNIT TESTING OF THE SHIM
        public CPublicRecordFields(DataSet data, LosConvert converter, Guid recordId)
            : base(data, converter, recordId)
        {

        }

        private CPublicRecordFields(CAppBase parent, DataSet ds, CPublicRecordCollection coll) :
            base(parent, ds, coll, "PublicRecordXmlContent")
		{
            m_isNewRecord = true;
		}

        private CPublicRecordFields(CAppBase parent, DataSet ds, IRecordCollection recordList, int iRow) :
            base(parent, ds, recordList, iRow, "PublicRecordXmlContent")
        {
        }

        private CPublicRecordFields(CAppBase parent, DataSet ds, IRecordCollection recordList, Guid id) :
            base(parent, ds, recordList, id, "PublicRecordXmlContent") 
        {
        }

        public override Enum KeyType 
        {
            get { return Type; }
            set { Type = (E_CreditPublicRecordType) value; }
        }
        private void KeepTrackOfFieldChange(string fieldId, string oldValue, string newValue) 
        {
            if (!m_isNewRecord) 
            {
                CFieldAudit field = (CFieldAudit) m_hash[fieldId];
                if (null == field) 
                {
                    field = new CFieldAudit();
                    field.OldValue = oldValue;
                    m_hash[fieldId] = field;
                }
                field.NewValue = newValue;
                if (field.OldValue != field.NewValue)
                    m_requireAuditLog = true;

            }
        }
        public string IdFromCreditReport 
        {
            get { return GetDescString("IdFromCreditReport"); }
            set 
            {
                KeepTrackOfFieldChange("IdFromCreditReport", IdFromCreditReport, value);
                SetDescString("IdFromCreditReport", value);
            }
        }

        public string CourtName 
        {
            get { return GetDescString("CourtName"); }
            set 
            {
                KeepTrackOfFieldChange("Court Name", CourtName, value);
                SetDescString("CourtName", value);
            }
        }

        public CDateTime ReportedD 
        {
            get { return GetDateTime("ReportedD"); }
            // For auditing change, user must set datetime in _rep
        }

        public string ReportedD_rep 
        {
            get { return ReportedD.ToString(m_convertLos); }
            set 
            {
                KeepTrackOfFieldChange("Reported Date", ReportedD_rep, value);
                SetDateTime("ReportedD", CDateTime.Create(value, m_convertLos));
            }
        }

        public CDateTime DispositionD 
        {
            get { return GetDateTime("DispositionD"); }
            // For auditing change, user must set datetime in _rep

        }

        public string DispositionD_rep 
        {
            get { return DispositionD.ToString(m_convertLos); }
            set 
            {
                KeepTrackOfFieldChange("Disposition Date", DispositionD_rep, value);
                SetDateTime("DispositionD", CDateTime.Create(value, m_convertLos));
            }
        }

        public E_CreditPublicRecordDispositionType DispositionT 
        {
            get { return (E_CreditPublicRecordDispositionType) GetCount("DispositionT"); }
            set 
            {
                KeepTrackOfFieldChange("Disposition", Tools.CreditPublicRecordDispositionType_rep( DispositionT ) , Tools.CreditPublicRecordDispositionType_rep( value ));
                SetDescString("DispositionT", value.ToString("D"));
            }
        }

        public decimal BankruptcyLiabilitiesAmount 
        {
            get { return GetMoney("BankruptcyLiabilitiesAmount"); }
        }

        public string BankruptcyLiabilitiesAmount_rep 
        {
            get { return m_convertLos.ToMoneyString(BankruptcyLiabilitiesAmount, FormatDirection.ToRep); }
            set 
            {
                KeepTrackOfFieldChange("Liabilities Amount", BankruptcyLiabilitiesAmount_rep, value);
                SetMoney("BankruptcyLiabilitiesAmount", m_convertLos.ToMoney(value)); 
            }
        }

        public  E_CreditPublicRecordType Type 
        {
            get { return (E_CreditPublicRecordType) GetEnum("Type", E_CreditPublicRecordType.Unknown); }
            set 
            {
                KeepTrackOfFieldChange("Type", Tools.CreditPublicRecordType_rep( Type ), Tools.CreditPublicRecordType_rep( value ) );
                SetEnum("Type", value);
            }
        }

        public CDateTime LastEffectiveD 
        {
            get { return GetDateTime("LastEffectiveD"); }
        }

        public string LastEffectiveD_rep 
        {
            get { return LastEffectiveD.ToString(m_convertLos); }
            set 
            {
                KeepTrackOfFieldChange("Last Effective Date", LastEffectiveD_rep, value);
                SetDateTime("LastEffectiveD", CDateTime.Create(value, m_convertLos));
            }
        }
        public CDateTime BkFileD 
        {
            get { return GetDateTime("BkFileD"); }
        }
        public string BkFileD_rep 
        {
            get { return BkFileD.ToString(m_convertLos); }
            set 
            {
                KeepTrackOfFieldChange("File Date", BkFileD_rep, value);
                SetDateTime("BkFileD", CDateTime.Create(value, m_convertLos));
            }
        }
        public E_PublicRecordOwnerT OwnerT
        {
            get { return (E_PublicRecordOwnerT)GetEnum("OwnerT", E_PublicRecordOwnerT.Borrower); }
            set 
            {
                KeepTrackOfFieldChange("Owner", OwnerT.ToString(), value.ToString());
                SetEnum("OwnerT", value);
            }
        }

        public bool IncludeInPricing 
        {
            get { return GetBool("IncludeInPricing"); }
            set 
            {
                KeepTrackOfFieldChange("IncludeInPricing", IncludeInPricing ? "Yes" : "No", value ? "Yes" : "No");
                SetBool("IncludeInPricing", value);
            }
        }
        private string AuditTrailXmlContent 
        {
            get { return GetDescString("AuditTrailXmlContent"); }
            set { SetDescString("AuditTrailXmlContent", value); }
        }
        public List<CPublicRecordAuditItem> AuditTrailItems
        {
            get
            {
                List<CPublicRecordAuditItem> list = new List<CPublicRecordAuditItem>();

                if (!string.IsNullOrEmpty(AuditTrailXmlContent))
                {
                    XmlDocument doc = Tools.CreateXmlDoc(AuditTrailXmlContent);
                    XmlNodeList nodeList = doc.SelectNodes("//History/Event");
                    foreach (XmlElement auditEvent in nodeList)
                    {
                        string userName = auditEvent.GetAttribute("UserName");
                        string eventDate = auditEvent.GetAttribute("EventDate");
                        string description = auditEvent.GetAttribute("Description");
                        list.Add(new CPublicRecordAuditItem(userName, eventDate, description));
                    }
                }
                return list;
            }
        }
        public bool IsModified 
        {
            get { return !string.IsNullOrEmpty(AuditTrailXmlContent); }
        }
        private void Audit() 
        {
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            string userName = "Unknown";
            if (null != principal) 
            {
                userName = principal.DisplayName;
            }

            XmlDocument doc = new XmlDocument();
            string xml = this.AuditTrailXmlContent;
            XmlElement rootElement = null;
            if (null == xml || "" == xml) 
            {
                rootElement = doc.CreateElement("History");
                doc.AppendChild(rootElement);
            } 
            else 
            {
                try 
                {
                    doc.LoadXml(xml);
                    rootElement = (XmlElement) doc.ChildNodes[0];
                }
                catch (Exception exc) 
                {
                    Tools.LogError("AuditTrailXmlContent is corrupted. xml=" + xml, exc);
                    rootElement = doc.CreateElement("History");
                    doc.AppendChild(rootElement);
                }
            }

            XmlElement eventElement = doc.CreateElement("Event");
            rootElement.AppendChild(eventElement);
            eventElement.SetAttribute("UserName", userName);
            eventElement.SetAttribute("EventDate", Tools.GetDateTimeNowString());
            string str = "";

            foreach (string key in m_hash.Keys) 
            {
                CFieldAudit field = (CFieldAudit) m_hash[key];

                if (field.OldValue != field.NewValue) 
                {
                    str += string.Format("{0}: {1} -> {2}{3}"
						, key
						, field.OldValue == string.Empty ? "[Blank]" : field.OldValue
						, field.NewValue == string.Empty ? "[Blank]" : field.NewValue
						, Environment.NewLine);
                }
            }
            if (str != "") 
            {
                eventElement.SetAttribute("Description", str);
                this.AuditTrailXmlContent = doc.OuterXml;
            }

            m_requireAuditLog = false;
            m_hash = new Hashtable();

        }

        public override void PrepareToFlush() 
        {
            if (m_requireAuditLog) 
            {
                Audit();
            }
        }

	}
}
