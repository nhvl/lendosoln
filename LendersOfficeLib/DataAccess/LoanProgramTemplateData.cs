using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using Adapter;
using LendersOffice;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.LoanPrograms;
using LendersOffice.RatePrice;
using LendersOfficeApp.los.RatePrice;
using LqbGrammar.DataTypes;
using LendersOffice.ObjLib.DynaTree;

namespace DataAccess
{
    public class CRateOptionNotFoundException : CBaseException 
    {
        public CRateOptionNotFoundException(Guid lLpTemplateId, string lLpTemplateNm, int sProdRLckdDays, int sProdPpmtPenaltyMon, int sIOnlyMon) : 
            base ("No valid rate option", "Rate option is not found lLpTemplateId=" + lLpTemplateId + ", lLpTemplateNm=" + lLpTemplateNm + ", sProdRLckdDays=" + sProdRLckdDays + ", sProdPpmtPenaltyMon=" + sProdPpmtPenaltyMon + ", sIOnlyMon=" + sIOnlyMon) 
        {
        }
    }
    public class CLoanProductData: CLoanProductBase
    {
        private bool m_bSuppressEventHandlerCall = false;

        public bool SuppressEventHandlerCall
        {
            set { m_bSuppressEventHandlerCall = value; }
            get { return m_bSuppressEventHandlerCall; }
        }

        public DataSrc DataSourceForLoad
        {
            // Set only when we need to read from a snapshot instead of 
            // the default of lpedb.
            set { m_dataSrcForLoad = value; }
        }

        /// <summary>
        /// Commit this instance to the database.  The instance already
        /// exist -- you can't create with a save.  Expect cached field
        /// fixup within the base.
        /// </summary>

        public override void Save()
        {
            // 2/11/2005 kb - Override the save call to make sure we
            // propagate changes throughout our distributed cache.
            // When a product that is a base changes, all the derived
            // products need their cached "Inherit" variables to be
            // updated.  If this gets to be too slow, we'll need to
            // only propagate a change when the "inheritable" fields
            // change.

            base.Save();

            if( SuppressEventHandlerCall == true )
            {
                return;
            }

            try
            {
                if( IsMaster == false )
                {
                    Toolbox.LoanProgram.EventHandlers.AfterModifForThisNonmasterProd( lLpTemplateId , BrokerId );
                }
                else
                {
                    Toolbox.LoanProgram.EventHandlers.AfterModifOfThisMaster( lLpTemplateId , BrokerId );
                }
            }
            catch( Exception e )
            {
                Tools.LogError( "Failed to save loan product " + lLpTemplateNm + "." , e );

                throw;
            }
        }


        /// <summary>
        /// This method should be use to duplicate loan program. This fix the OPM 2376
        /// </summary>
        /// <param name="lienPosition"></param>
        /// <param name="isLpe"></param>
        /// <param name="isMaster"></param>
        /// <param name="baseId"></param>
        /// <param name="folderId"></param>
        /// <param name="brokerId"></param>
        /// <param name="srcProductId"></param>
        /// <returns></returns>
		//     OPM 17145 Cord
		//     Must include ProductCode here too.
		private static CLoanProductData 
			CreateForDuplication( E_sLienPosT lienPosition , Boolean isLpe , Boolean isMaster , Guid baseId , 
			Guid folderId , Guid brokerId, Guid srcProductId, Guid srcRateOptionsProgIdInherit, string investor, string productCode)
		{
			// Commit a new row in the database.  We will fix it up
			// in a second.

			SqlParameter lLpTemplateId = new SqlParameter( "@LoanProductID" , Guid.Empty );
			List<SqlParameter>               pA = new List<SqlParameter>();

			lLpTemplateId.Direction = ParameterDirection.Output;

			pA.Add( new SqlParameter( "@BrokerID" , brokerId ) );

			if( folderId != Guid.Empty )
			{
				pA.Add( new SqlParameter( "@FolderID" , folderId ) );
			}

			if( baseId != Guid.Empty )
			{
				pA.Add( new SqlParameter( "@lBaseLpId" , baseId ) );
				pA.Add( new SqlParameter( "@SrcRateOptionsProgIdInherit" , srcRateOptionsProgIdInherit ) );
			}

			pA.Add( new SqlParameter( "@sLienPosT" , lienPosition ) );
			pA.Add( new SqlParameter( "@IsMaster" , isMaster ) );
			pA.Add( new SqlParameter( "@IsLpe" , isLpe ) );

			// OPM 7752
			pA.Add( new SqlParameter( "@lLpInvestorNm", investor ) );

			// OPM 17145 Cord
			pA.Add( new SqlParameter( "@ProductCode", productCode ) );

			pA.Add( lLpTemplateId );

			if( StoredProcedureHelper.ExecuteNonQuery( DataSrc.LpeSrc, "CreateBlankLoanProductWithProductCode"	, 3	, pA ) < 1 )
			{
				throw new CBaseException( ErrorMessages.Generic, "Failed to create blank loan product." );
			}

			if( lLpTemplateId.Value is DBNull == true )
			{
				throw new CBaseException( ErrorMessages.Generic, "Failed to return blank loan product." );
			}

            #region Perform a straight simple product price association copy.
			SqlParameter[] parameters = {
											new SqlParameter("@SrcProductId", srcProductId),
											new SqlParameter("@DestProductId", (Guid) lLpTemplateId.Value)
										};
			StoredProcedureHelper.ExecuteNonQuery(DataSrc.LpeSrc, "CopyProductPriceAssociation", 3, parameters);

            #endregion

			// Now fixup what we created.  We suppress event handling so
			// that we're not spreading twice.  The only spreading that's
			// needed is the after-creation event.

			CLoanProductData data = new CLoanProductData( ( Guid ) lLpTemplateId.Value );
			data.SuppressEventHandlerCall = true;
			data.InitSave();
			data.InitOverrides();
			data.Save();

			// 01/09/06 mf. OPM 9401 We should only spread changes for lpe programs
			if ( isLpe )
			{
				try
				{
					if( isMaster == false )
					{
						Toolbox.LoanProgram.EventHandlers.AfterCreationForThisNonmasterProd( data.lLpTemplateId , brokerId );
					}
					else
					{
						Toolbox.LoanProgram.EventHandlers.AfterCreationOfThisMaster( data.lLpTemplateId , brokerId );
					}
				}
				catch( Exception e )
				{
					Tools.LogError( "Create new loan product spread failed for " + data.lLpTemplateId + "." , e );

					throw;
				}
			}
			return data;
		}


        /// <summary>
        /// Prepare this loan program by creating a new instance within the
        /// database.  Failure to save will leave it in its initialized
        /// state.  Leaving it this way tends to be useless.
        /// </summary>
		//    OPM 17145 Cord
		//    Creates a blank loan product with ProductCode specified
		//    because ProductCode/Investor pairs are checked via trigger.
		public static CLoanProductData Create( E_sLienPosT lienPosition , Boolean isLpe , Boolean isMaster , Guid baseId , 
            Guid folderId , Guid brokerId, Guid srcRateOptionsProgIdInherit, string investor, string productCode )
        {
            // Commit a new row in the database.  We will fix it up
            // in a second.

            SqlParameter lLpTemplateId = new SqlParameter( "@LoanProductID" , Guid.Empty );
            List<SqlParameter>               pA = new List<SqlParameter>();

            lLpTemplateId.Direction = ParameterDirection.Output;

            pA.Add( new SqlParameter( "@BrokerID" , brokerId ) );

            if( folderId != Guid.Empty )
            {
                pA.Add( new SqlParameter( "@FolderID" , folderId ) );
            }

            if( baseId != Guid.Empty )
            {
                pA.Add( new SqlParameter( "@lBaseLpId" , baseId ) );
                pA.Add( new SqlParameter( "@SrcRateOptionsProgIdInherit" , srcRateOptionsProgIdInherit ) );
            }

            pA.Add( new SqlParameter( "@sLienPosT" , lienPosition ) );

            pA.Add( new SqlParameter( "@IsMaster" , isMaster ) );

            pA.Add( new SqlParameter( "@IsLpe" , isLpe ) );

            // OPM 7752
            pA.Add( new SqlParameter( "@lLpInvestorNm", investor ) );
			
			// OPM 17145 Cord
			pA.Add( new SqlParameter( "@ProductCode", productCode ) );

            pA.Add( lLpTemplateId );

            if( StoredProcedureHelper.ExecuteNonQuery( DataSrc.LpeSrc, "CreateBlankLoanProductWithProductCode"	, 3	, pA ) < 1 )
            {
                throw new CBaseException( ErrorMessages.Generic, "Failed to create blank loan product." );
            }

            if( lLpTemplateId.Value is DBNull == true )
            {
                throw new CBaseException( ErrorMessages.Generic, "Failed to return blank loan product." );
            }

            // Now fixup what we created.  We suppress event handling so
            // that we're not spreading twice.  The only spreading that's
            // needed is the after-creation event.

            CLoanProductData data = new CLoanProductData( ( Guid ) lLpTemplateId.Value );

            data.SuppressEventHandlerCall = true;

            data.InitSave();

            data.InitOverrides();

            data.Save();

			// 01/09/06 mf. OPM 9401 We only want to spread changes if lpe program
			if ( isLpe )
			{
				try
				{
					if( isMaster == false )
					{
						Toolbox.LoanProgram.EventHandlers.AfterCreationForThisNonmasterProd( data.lLpTemplateId , brokerId );
					}
					else
					{
						Toolbox.LoanProgram.EventHandlers.AfterCreationOfThisMaster( data.lLpTemplateId , brokerId );
					}
				}
				catch( Exception e )
				{
					Tools.LogError( "Create new loan product spread failed for " + data.lLpTemplateId + "." , e );

					throw;
				}
			}
            return data;
        }

        /// <summary>
        /// Prepare this loan program by creating a new instance within the
        /// database.  Failure to save will leave it in its initialized
        /// state.  Leaving it this way tends to be useless.
        /// </summary>
        public static CLoanProductData CreatePriceGroupMaster(Guid priceGroupId, Guid brokerId)
        {
            // 7/02/09 mf.  OPM 21028 Creating a PriceGroup master.
            SqlParameter lLpTemplateId = new SqlParameter("@LoanProductID", Guid.Empty);
            lLpTemplateId.Direction = ParameterDirection.Output;

            List<SqlParameter> pA = new List<SqlParameter>();
            pA.Add(new SqlParameter("@BrokerID", brokerId));
            pA.Add(new SqlParameter("@sLienPosT", E_sLienPosT.First));
            pA.Add(new SqlParameter("@IsMaster", true));
            pA.Add(new SqlParameter("@IsLpe", true));
            pA.Add(new SqlParameter("@IsMasterPriceGroup", true));
            pA.Add(new SqlParameter("@UseThisId", priceGroupId));
            pA.Add(lLpTemplateId);

            if (StoredProcedureHelper.ExecuteNonQuery(DataSrc.LpeSrc, "CreateBlankLoanProductWithProductCode", 3, pA) < 1)
            {
                throw new CBaseException(ErrorMessages.Generic, "Failed to create blank loan product.");
            }

            if (lLpTemplateId.Value is DBNull == true)
            {
                throw new CBaseException(ErrorMessages.Generic, "Failed to return blank loan product.");
            }

            CLoanProductData data = new CLoanProductData((Guid)lLpTemplateId.Value);

            data.SuppressEventHandlerCall = true;

            data.InitSave();

            data.InitOverrides();

            data.Save();

            return data;
        }
		
        /// <summary>
        /// Prepare this loan program by creating a new instance within the
        /// database.  Failure to save will leave it in its initialized
        /// state.  Leaving it this way tends to be useless.
        /// </summary>
		//    OPM 17145 Cord
		//    Now with ProductCode specified also.
		public static CLoanProductData 
			Create( E_sLienPosT lienPosition , Boolean isLpe , Boolean isMaster , Guid baseId , Guid folderId , 
			Guid brokerId, Guid srcRateOptionsProgIdInherit, string investor, string productCode, CStoredProcedureExec spExec )
		{
			// Create the starting row for this new loan product.  We pull
			// the details off the db once we finish successful extracting
			// the new product's identifier.

			SqlParameter lLpTemplateId = new SqlParameter( "@LoanProductID" , Guid.Empty );
			ArrayList aP = new ArrayList();

			lLpTemplateId.Direction = ParameterDirection.Output;

			aP.Add( new SqlParameter( "@BrokerID" , brokerId ) );

			aP.Add( new SqlParameter( "@sLienPosT" , lienPosition ) );
			aP.Add( new SqlParameter( "@IsMaster"  , isMaster     ) );
			aP.Add( new SqlParameter( "@IsLpe"     , isLpe        ) );

			// OPM 7752
			aP.Add( new SqlParameter( "@lLpInvestorNm", investor ) );

			// OPM 17145 Cord
			aP.Add( new SqlParameter( "@ProductCode", productCode) );

			if( folderId != Guid.Empty )
			{
				aP.Add( new SqlParameter( "@FolderID" , folderId ) );
			}

			if( baseId != Guid.Empty )
			{
				aP.Add( new SqlParameter( "@lBaseLpId" , baseId ) );
				aP.Add( new SqlParameter( "@SrcRateOptionsProgIdInherit" , srcRateOptionsProgIdInherit ) );
			}

			aP.Add( lLpTemplateId );
										//OPM 17145 Cord
			if( spExec.ExecuteNonQuery( "CreateBlankLoanProductWithProductCode" , aP ) < 1 )
			{
				throw new CBaseException( ErrorMessages.Generic, "Failed to create blank loan product." );
			}

			try
			{
				// Now fixup what we created.  We suppress event handling so
				// that we're not spreading twice.  The only spreading that's
				// needed is the after-creation event.

				CLoanProductData data = new CLoanProductData( ( Guid ) lLpTemplateId.Value );

				data.SuppressEventHandlerCall = true;

				data.InitSave( spExec );

				data.InitOverrides();

				data.Save( spExec );

				return data;
			}
			catch( Exception e )
			{
				// Oops!

				throw new CBaseException( ErrorMessages.Generic, "Failed to return blank loan product:  " + e.ToString() );
			}
		}

        /// <summary>
        /// Create a new instance that is an exact replica of the source.
        /// Beware: the instance returned is shallow, i.e. all field content
        /// was copied by reference.
        /// </summary>

        public static CLoanProductData Duplicate( Guid sourceId )
        {
            // Load the source so we can pull a quick switcheroo.  Make
            // sure the only references to the source in this instance
            // are its identifier.  Everything buried within the loan
            // product better be copy-able.

            CLoanProductData copy = null;
			
            try
            {
                // We create a copy, and then transfer everything one
                // cell at a time.  Note that we end up spreading this
                // product twice -- after create and after save.

                CLoanProductData data = new CLoanProductData( sourceId );

                data.InitLoad();

				// OPM 17145 Cord - Had to supply product code here.
				copy = CreateForDuplication( data.lLienPosT , data.IsLpe , data.IsMaster , data.lBaseLpId , 
                    data.FolderId , data.BrokerId, sourceId, data.SrcRateOptionsProgId, data.lLpInvestorNm, data.ProductCode ); 

				// 01/09/07 mf OPM 9401. We don't want to spread if we are duplicating
				// a non-lpe program.

				copy.SuppressEventHandlerCall = ! data.IsLpe;

                copy.CopyAllValues( data );

                copy.ChangeAllFields();

                copy.Save();
            }
            catch( Exception e )
            {
                Tools.LogError( "Duplicate loan product failed for " + sourceId + "." , e );

                throw;
            }

            return copy;
        }

        /// <summary>
        /// Perform batch update to DataTrac loan program name.
        /// </summary>
        /// <param name="updateDataTracNameList">List of loan program template id and data trac name.</param>
        public static void BatchUpdateDataTracLoanProgramName(IEnumerable<Tuple<Guid, string>> updateDataTracNameList)
        {
            // 11/23/2016 - dd - Why do I use generic for holding DataTrac program name and id?
            //                   Because integration with datatrac is only available for 1 or 2 active lenders.
            //                   There is no plan to support this integration for future client since DataTrac is no longer in business.

            if (updateDataTracNameList == null)
            {
                return;
            }

            using (CStoredProcedureExec transaction = new CStoredProcedureExec(DataSrc.LpeSrc))
            {
                try
                {
                    transaction.BeginTransactionForWrite();
                    foreach (var item in updateDataTracNameList)
                    {
                        SqlParameter[] parameters = {
                                                                 new SqlParameter("@lLpTemplateId", item.Item1),
                                                                 new SqlParameter("@lDataTracLpId", item.Item2),
                                                        };

                        transaction.ExecuteNonQuery("UpdateDataTracLpId", 0, parameters);
                    }
                    transaction.CommitTransaction();
                }
                catch
                {
                    transaction.RollbackTransaction();
                    throw;
                }

            }
        }
        #region ( Loading overrides )

        /// <summary>
        /// Load the instance from the database using dynamic sql.
        /// </summary>
        protected override Tuple<string, List<SqlParameter>> GetTemplateSelectString()
        {
            var sql = "SELECT LOAN_PROGRAM_TEMPLATE.*, r.lRateSheetXmlContent, r.LpeAcceptableRsFileId, r.LpeAcceptableRsFileVersionNumber , download.lRateSheetDownloadStartD, download.lRateSheetDownloadEndD, COALESCE(inherit.lRateSheetXmlContent, '') as lRateSheetXmlContentInherit " +
                " , r.ContentKey AS RateOptionsContentKey, inherit.ContentKey AS RateOptionsContentKeyInherit, r.RatesProtobufContent" +
                " FROM LOAN_PROGRAM_TEMPLATE " +
                "     join Rate_Options as r on LOAN_PROGRAM_TEMPLATE.SrcRateOptionsProgId = r.SrcProgId " +
                "     join RATE_OPTIONS_DOWNLOAD_TIME as download on LOAN_PROGRAM_TEMPLATE.SrcRateOptionsProgId = download.SrcProgId " +
                "     left join Rate_Options as inherit on LOAN_PROGRAM_TEMPLATE.SrcRateOptionsProgIdInherit = inherit.SrcProgId " +
                " WHERE lLpTemplateId = @fileid";
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@fileid", this.m_fileId));
            return new Tuple<string, List<SqlParameter>>(sql, parameters);
        }

		#endregion

		#region ( Constructors )

        /// <summary>
        /// If this constructor is used, InitLoad(), InitSave(), and Save()  must be overridden.
        /// </summary>

        protected CLoanProductData( DataRow rowLoanProduct , CRowHashtable rowsFolders , DataRow rowCcTemplate ) : base( rowLoanProduct , rowsFolders , rowCcTemplate , "Loan Program Template" )
        {
        }

        /// <summary>
        /// Pass template id to 'fileId' parameter
        /// </summary>

        public CLoanProductData( Guid productId ) : base( productId , "Loan Program Template" )
        {
        }

		#endregion

    }
    public class CLoanProductDataForRunningPricing : CLoanProductData
    {
        internal CLoanProductDataForRunningPricing(DataRow rowLoanProduct, CRowHashtable rowsFolders, DataRow rowCcTemplate)
            : base(rowLoanProduct, rowsFolders, rowCcTemplate)
        {
        }
        private DataSet LoadXmlWithRateSheetXmlSchema(string xml)
        {
            return GetDataSet(s_RateSheetXmlSchema, xml);
        }

        AbstractRateSheetProxy.LoadXmlDelegate m_xml2DataSetFunct;

        override protected DataSet RateSheetXmlContent2SimpleDataSet()
        {
            IDataContainer row = GetDataRow();
            AbstractRateSheetProxy proxy = row["RateSheetProxy"] as AbstractRateSheetProxy;

            if (proxy == null)
                return base.RateSheetXmlContent2SimpleDataSet();

            if (m_xml2DataSetFunct == null)
                m_xml2DataSetFunct = new AbstractRateSheetProxy.LoadXmlDelegate(LoadXmlWithRateSheetXmlSchema);

            return proxy.GetDataSet(m_xml2DataSetFunct);
        }

        private AbstractRateSheetProxy GetRatesheetProxy()
        {
            return (AbstractRateSheetProxy)(GetDataRow()["RateSheetProxy"]);
        }

        override public string LpeAcceptableRsFileId
        {
            get { return GetRatesheetProxy().LpeAcceptableRsFileId; }
            set
            {
                Tools.LogBug("The CApplicantPrice.LpeAcceptableRsFileId is read only");
                throw new CBaseException(ErrorMessages.Generic, "The CApplicantPrice.LpeAcceptableRsFileId is read only");
            }
        }

        override public long LpeAcceptableRsFileVersionNumber
        {
            get { return GetRatesheetProxy().LpeAcceptableRsFileVersionNumber; }
            set
            {
                Tools.LogBug("The CApplicantPrice.LpeAcceptableRsFileVersionNumber is read only");
                throw new CBaseException(ErrorMessages.Generic, "The CApplicantPrice.LpeAcceptableRsFileVersionNumber is read only");
            }
        }
    }
    public abstract class CLoanProductBase: CTemplateData, ILoanProgramTemplate
    {
		#region SCHEMA

        // lLpTemplateId guid
        // BrokerId guid
        // lBaseLpId guid
        // lLpTemplateNm varchar
        // lLendNm companyname
        // lLT typename
        // lLienPosT typename
        // lQualR rate
        // lTerm count
        // lDue count
        // lLockedDays count
        // lReqTopR rate
        // lReqBotR rate
        // lRadj1stCapR rate
        // lRadj1stCapMon count
        // lRAdjCapR rate
        // lRAdjCapMon count
        // lRAdjLifeCapR rate
        // lRAdjMarginR rate
        // lRAdjIndexR rate
        // lRAdjFloorR rate
        // lRAdjRoundT typename
        // lPmtAdjCapR rate
        // lPmtAdjCapMon count
        // lPmtAdjRecastPeriodMon count
        // lPmtAdjRecastStop count
        // lBuydwnR1 rate
        // lBuydwnR2 rate
        // lBuydwnR3 rate
        // lBuydwnR4 rate
        // lBuydwnR5 rate
        // lBuydwnMon1 count
        // lBuydwnMon2 count
        // lBuydwnMon3 count
        // lBuydwnMon4 count
        // lBuydwnMon5 count
        // lGradPmtYrs count
        // lGradPmtR rate
        // lIOnlyMon count
        // lHasVarRFeature boolean
        // lVarRNotes mediumnotes
        // lAprIncludesReqDeposit boolean
        // lHasDemandFeature boolean
        // lLateDays count
        // lLateChargePc countString
        // lPrepmtPenaltyT typename
        // lAssumeLT typename
        // lCcTemplateId uniqueidentifier
        // lFilingF char
        // lLateChargeBaseDesc varchar
        // lPmtAdjMaxBalPc ratenodefault
        // lFinMethT int
        // lFinMethDesc varchar
        // lPrepmtRefundT int
        // lRateSheetXmlContent text
        // FolderId uniqueidentifier
        // lRateSheetEffectiveD smalldatetime
        // lRateSheetExpirationD smalldatetime
        // IsMaster bit
        // lLenderNmInheritT int
        // lLpeFeeMin decimal
        // lLpeFeeMax decimal
        // PairingProductIds text
        // IsEnabled bit
        // lArmIndexGuid uniqueidentifier
        // lArmIndexBasedOnVstr varchar
        // lArmIndexCanBeFoundVstr varchar
        // lArmIndexAffectInitIRBit bit
        // lArmIndexNotifyAtLeastDaysVStr varchar
        // lArmIndexNotifyNotBeforeDaysVStr varchar
        // lRAdjRoundToR decimal
        // lArmIndexT int
        // lArmIndexNameVstr varchar
        // lArmIndexEffectiveD smalldatetime
        // lFreddieArmIndexT int
        // lLpTemplateNmInherit varchar
        // lCcTemplateIdInherit uniqueidentifier
        // lLockedDaysInherit int
        // lLpeFeeMinInherit decimal
        // lLpeFeeMaxInherit decimal
        // IsEnabledInherit bit
        // lPpmtPenaltyMon int
        // lPpmtPenaltyMonInherit int
        // lRateDelta decimal
        // lFeeDelta decimal
        // lRateSheetXmlContentInherit text
        // lLpTemplateNmOverrideBit bit
        // lCcTemplateIdOverrideBit bit
        // lLockedDaysOverrideBit bit
        // lLpeFeeMinOverrideBit bit
        // lLpeFeeMaxOverrideBit bit
        // IsEnabledOverrideBit bit
        // lPpmtPenaltyMonOverrideBit bit
        // lRateSheetxmlContentOverrideBit bit
        // lPpmtPenaltyMonLowerSearch int
        // lLockedDaysLowerSearch int
        // lLockedDaysLowerSearchInherit int
        // lPpmtPenaltyMonLowerSearchInherit int
        // lHardPrepmtPeriodMonths int
        // lSoftPrepmtPeriodMonths int

		#endregion

		#region STATIC
        protected static string s_RateSheetXmlSchema;
        private static Regex s_RateSheetRegEx;

		static CLoanProductBase()
		{
			s_RateSheetXmlSchema = SchemaFolder.RetrieveSchema("RateSheetXmlSchema.xml.config");

			// Parse either comma or space deliminate rate,point string.
			  s_RateSheetRegEx = new Regex(@"(?<rate>[-A-Z0-9.]+%?)(([ \f\t\v])+|([ \f\t\v])*,([ \f\t\v])*)(?<point>[-0-9.]+%?)(([ \f\t\v])+|([ \f\t\v])*,([ \f\t\v])*)(?<margin>[-0-9.]+%?)(([ \f\t\v])+|([ \f\t\v])*,([ \f\t\v])*)(?<qratebase>[-0-9.]+%?)(([ \f\t\v])+|([ \f\t\v])*,([ \f\t\v])*)(?<teaserrate>[-0-9.]+%?)", RegexOptions.Compiled);
		}


        public static void DelLoanProduct( Guid loanProductId )
        {
            // 2/14/2005 kb - We now check if the loan product is currently
            // in use as a base product.  If so, we can't delete because we
            // would violate the base - derived foreign key relationship.

            bool isaBase = false;

            using( DbDataReader sR = StoredProcedureHelper.ExecuteReader( DataSrc.LpeSrc, "ListLoanProductDerivations" , new SqlParameter( "@ProductId" , loanProductId ) ) )
            {
                if( sR.Read() == true )
                {
                    isaBase = true;
                }
            }

            if( isaBase == false )
            {
                // 2/14/2005 kb - Go ahead: Delete and update the cache.  We
                // don't really need to spread changes, because the product
                // is not derived.

                int iRes = DeleteLoanProduct(loanProductId);

                if( iRes < 0 )
                {
                    // Oops!

                    throw new CBaseException
                        ( "Unable to delete loan product.  When deleting a master, make sure"
                        + " the containing folder is empty (contains no subfolder nor loan product)."
                        , "Delete denied for " + loanProductId + " because it is a master."
                        );
                }
            }
            else
            {
                // Oops!

                throw new CBaseException
                    ( "Unable to delete loan product because it is currently a base product to others."
                    , "Delete denied for " + loanProductId + " because it is a base."
                    );
            }
        }

		public static int DelLoanProductAndDerived( Guid loanProductId )
		{
			// 03/24/08 mf. Per OPM 17141 we need to allow for automatic deletion
			// of all derived programs when deleting a base program.

			// Get the complete derived program set in semi-breadth first order.
			// The resulting array will be grouped by generation in the tree, from root.
			ArrayList idsToLookup = new ArrayList();
			ArrayList idsToDelete = new ArrayList();

			idsToLookup.Add( loanProductId );

			while ( idsToLookup.Count > 0 )
			{
				ArrayList nextSet = new ArrayList();
				foreach (Guid id in idsToLookup )
				{
					using( DbDataReader sR 
							   = StoredProcedureHelper.ExecuteReader(
							   DataSrc.LpeSrc
							   , "ListLoanProductDerivations" 
							   , new SqlParameter( "@ProductId" , id ) ) )
					{
						while( sR.Read() == true )
						{
							nextSet.Add( ( Guid ) sR[ "lLpTemplateId" ] );
						}
					}
				}
				idsToDelete.AddRange( idsToLookup );
				idsToLookup = nextSet;
			}
			
			// Perform the deletions in the reverse order that we found
			// the programs above, so last generation gets deleted first.
			idsToDelete.Reverse();

			int programsDeleted = 0;
			foreach ( Guid id in idsToDelete )
			{
				try
				{
					int iRes = DeleteLoanProduct(id);

					if( iRes < 0 )
					{
						continue;
					}
				}
				catch (Exception e )
				{
					Tools.LogError("Failed to delete program: " + id, e);
					continue;
				}
				if (id != loanProductId) 
					programsDeleted++;
			}
			return programsDeleted;
		}

        static protected SqlParameter	lLpTemplateId_OrigParam
        {
            get
            {
                SqlParameter param = new SqlParameter( "@plLpTemplateId", SqlDbType.UniqueIdentifier );
                param.SourceVersion = DataRowVersion.Original;
                param.SourceColumn = "lLpTemplateId";
                return param;
            }
        }

        /// <summary>
        /// Invoke DeleteLoanProduct stored procedure. If stored procedure is execute successfully then also delete 
        /// record from LPE_PRICE_GROUP_PRODUCT in main db.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static int DeleteLoanProduct(Guid id)
        {
            int ret = StoredProcedureHelper.ExecuteNonQuery(DataSrc.LpeSrc, "DeleteLoanProduct", 1, new SqlParameter("@LoanProdId", id));
            if (ret >= 0)
            {
                // 7/11/2014 dd - Since we don't know the actual database the id belong. Therefore we need to execute on ALL databases.
                foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
                {
                    SqlParameter[] paramters = {
                                                   new SqlParameter("@lLpTemplateId", id)
                                               };
                    StoredProcedureHelper.ExecuteNonQuery(connInfo, "LPE_PRICE_GROUP_PRODUCT_DeleteByLpTemplateId", 1, paramters);
                }
            }
            return ret;
        }

		#endregion //STATIC

		#region DATA MEMBERS

        private DataSet          m_dsLoanProd;
        private CRowHashtable    m_rowsFolders;
        private DbDataAdapter   m_daLoanProd;
        private DataRowContainer          m_rowLoanProd;
        private DataRow          m_rowCcTemplate;
        private StringCollection m_updatedLoanProdFields;
        private DbCommand       m_cmdUpdateLoanProd;
        private CCcTemplateData  m_ccTemplate;
        private DataSet          m_dsRateSheet;
        private DataSet          m_dsRateSheetInherit;
        private DataSet          m_dsRateSheetWithDeltas;
        private string           m_fullname = "";
        
        // 02/05/09 mf TEMP: Adding ability to load from snapshot 
        // because of the short notice requested rate option viewer.
        // It will be faster loading to use a lighter class
        // Only CLoanProductData should be changing this value.
        protected DataSrc m_dataSrcForLoad = DataSrc.LpeSrc;

        private enum InitMode
        {
            InitNone = 0,
            InitLoad = 1,
            InitSave = 2
        };

        private InitMode m_initMode;

		#endregion // DATA MEMBERS

        /// <summary>
        /// If this constructor is used, GetDateRow(), InitLoad(), InitSave(), and Save()  must be overridden.
        /// </summary>
        protected CLoanProductBase( DataRow rowLoanProduct, CRowHashtable rowsFolders, DataRow rowCcTemplate, string pageName )
            : base( rowLoanProduct == null ? Guid.Empty : new Guid( rowLoanProduct["lLpTemplateId"].ToString() ), pageName, E_DataObjectType.LoanProgramTemplate )
        {
            m_rowLoanProd = new DataRowContainer(rowLoanProduct);
            m_rowCcTemplate = rowCcTemplate;
            m_rowsFolders = rowsFolders;

            m_dsLoanProd = null;
            m_daLoanProd = null;
            m_updatedLoanProdFields = null;
            m_cmdUpdateLoanProd = null;
            m_ccTemplate = null;
            m_dsRateSheet = null;
            m_dsRateSheetInherit = null;
            m_dsRateSheetWithDeltas = null;

            m_initMode = InitMode.InitNone;
        }

        /// <summary>
        /// Pass template id to 'fileId' parameter
        /// </summary>

        public CLoanProductBase( Guid fileId , string pageName ) : base( fileId , pageName , E_DataObjectType.LoanProgramTemplate )
        {
            m_updatedLoanProdFields = new StringCollection();
            m_cmdUpdateLoanProd = DbAccessUtils.GetDbProvider().CreateCommand();

            m_initMode = InitMode.InitNone;

            m_ccTemplate = null;
        }

        public bool HasCcTemplate
        {
            get { return !lCcTemplateId.Equals( Guid.Empty ); }
        }

        public bool lCcTemplateIdOverrideBit
        {
            get { return GetBoolField( "lCcTemplateIdOverrideBit" ); }
            set { SetBoolField( "lCcTemplateIdOverrideBit", value ); }
        }

        public Guid lCcTemplateIdInherit
        {
            get { return GetGuidField( "lCcTemplateIdInherit" ); }
            set { SetGuidFieldPreserveEmptyGuid( "lCcTemplateIdInherit", value ); }
        }

        public Guid lCcTemplateId
        {
            get { return GetGuidField( "lCcTemplateId" ); }
            set { SetGuidFieldPreserveEmptyGuid( "lCcTemplateId", value ); }
        }
        public string lCcTemplateId_rep
        {
            set { SetGuidFieldPreserveEmptyGuid( "lCcTemplateId", new Guid( value ) ); }
        }

        public CCcTemplateData CcTemplate
        {
            get 
            {
                if( !HasCcTemplate )
                    throw new CBaseException( ErrorMessages.Generic, "No cc template associated with this loan program" );

                if( null == m_ccTemplate )
                {
                    if( null == m_rowCcTemplate )
                        m_ccTemplate = new CCcTemplateData( this.BrokerId, lCcTemplateId );
                    else
                        m_ccTemplate = new CCcTemplateData( this.BrokerId, m_rowCcTemplate );
                    switch( m_initMode )
                    {
                        case InitMode.InitLoad: m_ccTemplate.InitLoad(); break;
                        case InitMode.InitSave: m_ccTemplate.InitSave(); break;											 
                    }
                }
				
                return m_ccTemplate;
            }
            set
            {
                m_ccTemplate = value;
                if( null == value )
                    lCcTemplateId = Guid.Empty;
                else
                    lCcTemplateId = value.cCcTemplateId;			
            }
        }

        protected override IDataContainer	GetDataRow() 
        {
            return m_rowLoanProd;
        }


        protected DbCommand PrepareLoanProdUpdateCmd()
        {

            if( m_updatedLoanProdFields.Count > 0 )
            {

                SqlParameter keyId = lLpTemplateId_OrigParam;

                if( m_cmdUpdateLoanProd.Parameters.Contains( keyId.ParameterName ) == false )
                {
                    m_cmdUpdateLoanProd.Parameters.Add( lLpTemplateId_OrigParam );
                }

                StringBuilder sFields = new StringBuilder();

                bool has_lRateSheetXmlContent = false;
				bool has_RateSheetDownloadEndDContent = false;
				bool has_RateSheetDownloadStartDContent = false;
				bool has_LpeAcceptableRsFileId = false;
				bool has_LpeAcceptableRsFileVersionNumber = false;

                bool has_RatesProtobufContent = false;
                StringEnumerator eStr = m_updatedLoanProdFields.GetEnumerator();
                while( eStr.MoveNext() )
                {
                    if( eStr.Current == "lRateSheetXmlContent")
                    {
                        has_lRateSheetXmlContent = true;
                        continue;
                    }
                    if (eStr.Current == "RateOptionsContentKey" || string.Equals(eStr.Current,"RateOptionsContentKeyInherit") || string.Equals(eStr.Current, "RateOptionsContentKeyInherit"))
                    {
                        // dd 7/23/2018 - RateOptionsContentKey are no longer in use.
                        continue;
                    }
					if( eStr.Current == "LpeAcceptableRsFileId" )
					{
						has_LpeAcceptableRsFileId = true;
						continue;
					}
					if( eStr.Current == "LpeAcceptableRsFileVersionNumber" )
					{
						has_LpeAcceptableRsFileVersionNumber = true;
						continue;
					}
					if( eStr.Current == "lRateSheetDownloadStartD" )
					{
						has_RateSheetDownloadStartDContent = true;
						continue;
					}
					if( eStr.Current == "lRateSheetDownloadEndD" )
					{
						has_RateSheetDownloadEndDContent = true;
						continue;
					}

                    if (eStr.Current == "RatesProtobufContent")
                    {
                        has_RatesProtobufContent = true;
                        continue;
                    }

                    if ( eStr.Current == "SrcRateOptionsProgId" || eStr.Current == "lRateSheetXmlContentInherit"
						|| eStr.Current == "VersionTimestamp")
                        continue;
 
                    if( sFields.Length > 0 )
                        sFields.Append(", ");

                    sFields.AppendFormat("{0} = @p{0}", eStr.Current);

                }

				if( lRateSheetXmlContentOverrideBit == false )
				{
					has_lRateSheetXmlContent = false;
					has_RateSheetDownloadEndDContent = false;
					has_RateSheetDownloadStartDContent = false;
				}

                if( sFields.Length > 0 
					|| has_lRateSheetXmlContent 
					|| has_RateSheetDownloadStartDContent 
					|| has_RateSheetDownloadEndDContent
					|| has_LpeAcceptableRsFileId
					|| has_LpeAcceptableRsFileVersionNumber
                    || has_RatesProtobufContent
                    )
                {
                    if( sFields.Length > 0 )
                        sFields.Insert(0, "Update LOAN_PROGRAM_TEMPLATE set ")
                            .AppendFormat(" WHERE lLpTemplateId = @plLpTemplateId; ");

                    if ( has_lRateSheetXmlContent || has_LpeAcceptableRsFileId || has_LpeAcceptableRsFileVersionNumber || has_RatesProtobufContent)
                    {
                        if( m_rowLoanProd.DataRow["lLpTemplateId", DataRowVersion.Original].Equals(
                            m_rowLoanProd.DataRow["lLpTemplateId", DataRowVersion.Current] ) == false )
                        {
                            throw new CBaseException( ErrorMessages.Generic, "Internal Error : don't support to modified the field 'lLpTemplateId'." );
                        }

						string setList = string.Empty;

						if ( has_lRateSheetXmlContent )
						{
							setList = " lRateSheetXmlContent = @plRateSheetXmlContent ";

                            var rateOptionList = CLoanProductBase.ConvertRateOption(this.my_lRateSheetXmlContent);
                            var bytes = SerializationHelper.ProtobufSerialize(rateOptionList);

                            // 2/20/2018 - dd - Also save RatesProtobufContent. However this is a temporary solution. Ideally, 
                            //             we should stop using lRateSheetXmlContent all together and only use RatesProtobufContent for better memory footprint.
                            this.my_RatesProtobufContent = bytes; // this command potential insert "RatesProtobufContent" to m_updatedLoanProdFields
                            has_RatesProtobufContent = m_updatedLoanProdFields.Contains("RatesProtobufContent");
                        }
                        //if (has_RateOptionsContentKey)
                        //{
                        //    setList += (setList == string.Empty) ? "" : ",";
                        //    setList += "ContentKey = @pRateOptionsContentKey ";
                        //}
						if ( has_LpeAcceptableRsFileId )
						{
							setList += (setList == string.Empty) ? "" : ",";
							setList += " LpeAcceptableRsFileId = @pLpeAcceptableRsFileId ";
						}
						if ( has_LpeAcceptableRsFileVersionNumber )
						{
							setList += (setList == string.Empty) ? "" : ",";
							setList += " LpeAcceptableRsFileVersionNumber = @pLpeAcceptableRsFileVersionNumber ";
						}

                        if (has_RatesProtobufContent)
                        {
                            setList += ", RatesProtobufContent = @pRatesProtobufContent";
                        }

                        sFields.AppendFormat( " Update Rate_options set {0} WHERE SrcProgId = @plLpTemplateId; ", setList);
                    }


                    if( has_RateSheetDownloadStartDContent || has_RateSheetDownloadEndDContent )
                    {
						string setFields = "";

						if ( has_RateSheetDownloadStartDContent )
						{
							setFields = " lRateSheetDownloadStartD = @plRateSheetDownloadStartD ";
						}
						if ( has_RateSheetDownloadEndDContent )
						{
							setFields += setFields == string.Empty? "" : " , ";
							setFields += " lRateSheetDownloadEndD = @plRateSheetDownloadEndD ";
						}

                        sFields.Append( " Update RATE_OPTIONS_DOWNLOAD_TIME set ").Append(setFields).Append( "  WHERE SrcProgId = @plLpTemplateId; ");
                    }

                    m_cmdUpdateLoanProd.CommandText = sFields.ToString();
                }
            }

            return (m_cmdUpdateLoanProd.CommandText.Length > 0 ) ? m_cmdUpdateLoanProd : null;
        }

        public static List<LendersOffice.RatePrice.Model.RateOptionV2> ConvertRateOption(string xml)
        {
            if (string.IsNullOrEmpty(xml))
            {
                return new List<LendersOffice.RatePrice.Model.RateOptionV2>();
            }

            var rateOptionList = new List<LendersOffice.RatePrice.Model.RateOptionV2>();

            XDocument xdoc = XDocument.Parse(xml);

            var rateSheetContenList = xdoc.Root.Nodes();

            LosConvert convert = new LosConvert();
            foreach (XElement rateSheetContent in rateSheetContenList)
            {
                var rateOption = new LendersOffice.RatePrice.Model.RateOptionV2();
                foreach (XElement childElement in rateSheetContent.Nodes())
                {
                    switch (childElement.Name.LocalName)
                    {
                        case "RecordId":
                            break; // NO-OP. No need to record RecordId
                        case "Rate":
                            if (childElement.Value == "LOCK")
                            {
                                rateOption.ItemType = E_RateSheetKeywordT.Lock;
                                rateOption.Rate = 0;
                            }
                            else if (childElement.Value == "IO")
                            {
                                rateOption.ItemType = E_RateSheetKeywordT.IO;
                                rateOption.Rate = 0;
                            }
                            else if (childElement.Value == "PP")
                            {
                                rateOption.ItemType = E_RateSheetKeywordT.Prepay;
                                rateOption.Rate = 0;
                            }
                            else
                            {
                                rateOption.Rate = convert.ToRate(childElement.Value);
                            }
                            break;
                        case "Point":
                            rateOption.Point = convert.ToRate(childElement.Value);
                            break;
                        case "Margin":
                            rateOption.Margin = convert.ToRate(childElement.Value);
                            break;
                        case "QRateBase":
                            rateOption.QRateBase = convert.ToRate(childElement.Value);
                            break;
                        case "TeaserRate":
                            rateOption.TeaserRate = convert.ToRate(childElement.Value);
                            break;
                        default:
                            throw new Exception("Unsupport element name [" + childElement.Name.LocalName);
                    }
                }
                rateOptionList.Add(rateOption);
            }

            return rateOptionList;
        }

        protected override int GetColumnMaxLength( string strFieldNm )
        { 
            DataTable table = m_dsLoanProd.Tables["TEMPLATE"];
            return table.Columns[strFieldNm].MaxLength;
        }

        private static DateTime x_loanProductFolderGoodUntilTime = DateTime.MinValue;
        private static CRowHashtable x_loanProductFolders = null;
        private static object x_loanProductFoldersLock = new object();
        private static CRowHashtable GetLoanProductFolders()
        {
            // 3/14/2012 dd - I created this method to temporary cache the whole LOAN_PRODUCT_FOLDER
            // in memory instead of load each time in InitLoad method.
            // However we can improve it better by only load folder on demand. But to save time
            // and minimize the amount of code change I will just cache the object every hour.
            lock (x_loanProductFoldersLock)
            {
                if (x_loanProductFolderGoodUntilTime < DateTime.Now)
                {
                    DataSet dsFolders = new DataSet("FOLDER");
                    LoadAllProductFolders(dsFolders);

                    x_loanProductFolders = new CRowHashtable(dsFolders.Tables[0].Rows, "FolderId");

                    x_loanProductFolderGoodUntilTime = DateTime.Now.AddHours(1);
                }
                return x_loanProductFolders;
            }
        }

        public static void LoadAllProductFolders(DataSet dsFolders)
        {
            string sQuery = "select * from LOAN_PRODUCT_FOLDER"; // 3/14/2012 dd - THIS IS BAD. REFACTOR IF THERE ARE TIME.
            DBSelectUtility.FillDataSet(DataSrc.LpeSrc, dsFolders, sQuery, null, null);
        }

        /// <summary>
        /// Load the product's details from disk and set the base mode to load.
        /// </summary>

        //static int debugSrcRateOptionsProgIdCounter = 0;
        override public void InitLoad()
        {
            try
            {
                var data = GetTemplateSelectString();
                if (data.Item1.Length == 0)
                {
                    return;
                }

                this.m_dsLoanProd = new DataSet("TEMPLATE");
                PopulateLoanProducts(this.m_dataSrcForLoad, data.Item1, data.Item2, this.m_dsLoanProd);

                m_rowLoanProd = new DataRowContainer(m_dsLoanProd.Tables["TEMPLATE"].Rows[0]);
                m_initMode = InitMode.InitLoad;
                m_rowsFolders = GetLoanProductFolders();
            }
            catch (System.Exception e)
            {
                LogError(e.Message + ". Stack trace:\n" + e.StackTrace);
                throw e;
            }
        }

        public static void PopulateLoanProducts(DataSrc source, string sql, List<SqlParameter> listParams, DataSet toFill)
        {
            using (DbConnection conn = DbAccessUtils.GetConnection(source))
            {
                conn.OpenWithRetry();

                using (DbTransaction tx = conn.BeginTransaction(IsolationLevel.RepeatableRead))
                {
                    DBSelectUtility.FillDataSet(conn, tx, toFill, sql, null, listParams);
                    tx.Commit();
                }
            }
        }

        /// <summary>
        /// Load the product's details from disk and set the base mode to load.
        /// </summary>
        public void InitLoad( CStoredProcedureExec spExec )
        {
            try
            {
                m_dsLoanProd = new DataSet("TEMPLATE");
                spExec.Fill(m_dsLoanProd, "LoadLoanProductData", "TEMPLATE", new SqlParameter("@fileid", this.m_fileId));
                m_rowLoanProd = new DataRowContainer(m_dsLoanProd.Tables["TEMPLATE"].Rows[0]);

                DataSet dsFolders = new DataSet("FOLDER");
                spExec.Fill(dsFolders, "LoadLoanProductFolderData", "FOLDER");
                m_rowsFolders = new CRowHashtable( dsFolders.Tables["FOLDER"].Rows, "FolderId" );

                m_initMode = InitMode.InitLoad;
            }
            catch( System.Exception e )
            {
                LogError( e.Message + ". Stack trace:\n" + e.StackTrace  );
                throw;
            }
        }

        /// <summary>
        /// Prepare the loan program for saving.  All existing fields are loaded.
        /// We don't invoke saving when creating a new one.
        /// </summary>

        public override void InitSave()
        {
            using (DbConnection conn = DbAccessUtils.GetConnection(DataSrc.LpeSrc))
            {
                try
                {
                    var data = GetTemplateSelectString();
                    if (data.Item1.Length == 0)
                    {
                        return;
                    }

                    this.m_dsLoanProd = new DataSet("TEMPLATE");

                    InitForSaveLoanProducts(conn, this.m_dsLoanProd, data);

                    this.m_rowLoanProd = new DataRowContainer(m_dsLoanProd.Tables["TEMPLATE"].Rows[0]);
                    m_initMode = InitMode.InitSave;
                }
                catch (Exception e)
                {
                    LogError(e.Message + ".  Stack:\n" + e.StackTrace);

                    throw;
                }
            }
        }

        static public void InitForSaveLoanProducts(DbConnection conn, DataSet ds, Tuple<string, List<SqlParameter>> data)
        {
            if (conn.State != ConnectionState.Open)
            {
                conn.OpenWithRetry();
            }

            using (DbTransaction tx = conn.BeginTransaction(IsolationLevel.RepeatableRead))
            {
                DBSelectUtility.FillDataSet(conn, tx, ds, data.Item1, null, data.Item2);
                tx.Commit();
            }
        }

        /// <summary>
        /// Prepare the loan program for saving.  All existing fields are loaded.
        /// We don't invoke saving when creating a new one.
        /// </summary>

        public void InitSave( CStoredProcedureExec spExec )
        {
            try
            {	
                this.m_dsLoanProd = new DataSet("TEMPLATE");
                spExec.Fill(this.m_dsLoanProd, "LoadLoanProductData", "TEMPLATE", new SqlParameter(@"fileid", this.m_fileId));
                this.m_rowLoanProd = new DataRowContainer(m_dsLoanProd.Tables[ "TEMPLATE" ].Rows[ 0 ]);
                this.m_initMode = InitMode.InitSave;
            }
            catch ( Exception e )
            {
                LogError( e.Message + ".  Stack:\n" + e.StackTrace  );

                throw;
            }
        }

        /// <summary>
        /// Handle copying the source product item by item using a shallow
        /// reference copy of each column stored in the data row.
        /// </summary>

        public virtual void CopyAllValues( CLoanProductData lpSource )
        {
            // 5/11/2005 kb - Copy each source product column object.

            foreach( DataColumn dCol in m_rowLoanProd.DataRow.Table.Columns )
            {
                Boolean copyCol = true;

                switch( dCol.ColumnName )
                {
                    case "lLpTemplateId":
                    case "BrokerId":
                    case "FolderId":
                    case "lBaseLpId":
                    case "SrcRateOptionsProgId":
                    case "SrcRateOptionsProgIdInherit":
					case "CreatedD": // 04/24/07 mf OPM 11856.
					case "VersionTimestamp":

                    {
                        copyCol = false;
                    }
                        break;
                }

                if( copyCol == true )
                {
                    m_rowLoanProd[ dCol.ColumnName ] = lpSource.m_rowLoanProd[ dCol.ColumnName ];
                }
            }
        }

        /// <summary>
        /// Resolve all fields that have an override bit and a corresponding
        /// inherit variable.  Each of these "cached" fields is stored in
        /// the database as a final value that contributes directly to loan
        /// program engine calculations.  These final values are what the
        /// editor has when overriding, and what the inherit fields are
        /// when not.  As new cached fields show up, add them here.
        /// </summary>

        public virtual void FixupCachedFields()
        {
            if (m_islArmIndexGuidModified)
            {
                // 7/23/2014 dd - Populate the loan program with current value from ARM_INDEX.
                if (this.lArmIndexGuid == Guid.Empty)
                {
                    this.lArmIndexNameVstr = string.Empty;
                    this.lRAdjIndexR = 0;
                    this.lArmIndexEffectiveD = CDateTime.InvalidWrapValue;
                    this.lFreddieArmIndexT = E_sFreddieArmIndexT.LeaveBlank;
                    this.lArmIndexBasedOnVstr = string.Empty;
                    this.lArmIndexCanBeFoundVstr = string.Empty;
                }
                else
                {
                    bool foundArmIndex = false;
                    foreach (var arm in SystemArmIndex.ListSystemArmIndexes())
                    {
                        if (arm.IndexIdGuid == this.lArmIndexGuid)
                        {
                            this.lArmIndexNameVstr = arm.IndexNameVstr;
                            this.lRAdjIndexR = arm.IndexCurrentValueDecimal;
                            this.lArmIndexEffectiveD =  CDateTime.Create(arm.EffectiveD);
                            this.lFreddieArmIndexT = E_sFreddieArmIndexT.LeaveBlank;
                            this.lArmIndexBasedOnVstr = arm.IndexBasedOnVstr;
                            this.lArmIndexCanBeFoundVstr = arm.IndexCanBeFoundVstr;

                            foundArmIndex = true;
                            break;
                        }
                    }
                    if (foundArmIndex == false)
                    {
                        ARMIndexDescriptorSet armSet = new ARMIndexDescriptorSet();
                        armSet.Retrieve(this.BrokerId);

                        foreach (ARMIndexDescriptor arm in armSet)
                        {
                            if (arm.IndexIdGuid == this.lArmIndexGuid)
                            {
                                this.lArmIndexNameVstr = arm.IndexNameVstr;
                                this.lRAdjIndexR = arm.IndexCurrentValueDecimal;
                                this.lArmIndexEffectiveD = CDateTime.Create(arm.EffectiveD);
                                this.lFreddieArmIndexT = E_sFreddieArmIndexT.LeaveBlank;
                                this.lArmIndexBasedOnVstr = arm.IndexBasedOnVstr;
                                this.lArmIndexCanBeFoundVstr = arm.IndexCanBeFoundVstr;

                                foundArmIndex = true;
                                break;
                            }
                        }
                    }
                }
            }
            // 5/5/2005 kb - Transfer the inherited values into position for
            // each field that is *not* overriding.  If overriding, we assume
            // that the editor already set a valid value.

            if( lCcTemplateIdOverrideBit == false )
            {
                lCcTemplateId = lCcTemplateIdInherit;
            }

            if( lLpTemplateNmOverrideBit == false )
            {
                lLpTemplateNm = lLpTemplateNmInherit;
            }

            if( lLendNmOverrideBit == false )
            {
                lLendNm = lLendNmInherit;
            }

            if( lLockedDaysOverrideBit == false )
            {
                lLockedDays            = lLockedDaysInherit;
                lLockedDaysLowerSearch = lLockedDaysLowerSearchInherit;
            }

			if( lLpeFeeMinOverrideBit == false )
            {
                lLpeFeeMin = lLpeFeeMinInherit + lLpeFeeMinParam;
            }
            else
            {
                lLpeFeeMin = lLpeFeeMinParam;
            }

            if( lLpeFeeMaxOverrideBit == false )
            {
                lLpeFeeMax = lLpeFeeMaxInherit;
            }

            if( PairingProductIdsOverrideBit == false )
            {
                PairingProductIds        = PairingProductIdsInherit;
                PairIdFor1stLienProdGuid = PairIdFor1stLienProdGuidInherit;
            }
            else
            {
                PairIdFor1stLienProdGuid = lLpTemplateId;
            }

            if( lPpmtPenaltyMonOverrideBit == false )
            {
                lPpmtPenaltyMon            = lPpmtPenaltyMonInherit;
                lPpmtPenaltyMonLowerSearch = lPpmtPenaltyMonLowerSearchInherit;
            }

            if( IsEnabledOverrideBit == false )
            {
                IsEnabled = IsEnabledInherit;
            }

            if (!lLpCustomCode1OverrideBit)
            {
                lLpCustomCode1 = lLpCustomCode1Inherit;
            }

            if (!lLpCustomCode2OverrideBit)
            {
                lLpCustomCode2 = lLpCustomCode2Inherit;
            }

            if (!lLpCustomCode3OverrideBit)
            {
                lLpCustomCode3 = lLpCustomCode3Inherit;
            }

            if (!lLpCustomCode4OverrideBit)
            {
                lLpCustomCode4 = lLpCustomCode4Inherit;
            }

            if (!lLpCustomCode5OverrideBit)
            {
                lLpCustomCode5 = lLpCustomCode5Inherit;
            }

            if (!lLpInvestorCode1OverrideBit)
            {
                lLpInvestorCode1 = lLpInvestorCode1Inherit;
            }

            if (!lLpInvestorCode2OverrideBit)
            {
                lLpInvestorCode2 = lLpInvestorCode2Inherit;
            }

            if (!lLpInvestorCode3OverrideBit)
            {
                lLpInvestorCode3 = lLpInvestorCode3Inherit;
            }

            if (!lLpInvestorCode4OverrideBit)
            {
                lLpInvestorCode4 = lLpInvestorCode4Inherit;
            }

            if (!lLpInvestorCode5OverrideBit)
            {
                lLpInvestorCode5 = lLpInvestorCode5Inherit;
            }

            if (lLpmiSupportedOutsidePmiOverrideBit == false)
            {
                lLpmiSupportedOutsidePmi = lLpmiSupportedOutsidePmiInherit;
            }

            if (lLT != E_sLT.Conventional)
            {
                // Cannot have this set for non-conventional
                lLpmiSupportedOutsidePmi = false;
                lLpmiSupportedOutsidePmiOverrideBit = false;
            }

            if (ProductCodeOverrideBit == false && IsLpe == true)
            {
                ProductCode = ProductCodeInherit;
            }

            if (!ProductIdentifierOverride)
            {
                ProductIdentifier = ProductIdentifierInherited;
            }
        }

        /// <summary>
        /// We have created a new loan product, so setup the override
        /// bits to work well with fixup and subsequent saving.
        /// </summary>

        public virtual void InitOverrides()
        {
            // Setup override bits as if this product was brand new.

            lCcTemplateIdOverrideBit        = true;
            lLpTemplateNmOverrideBit        = true;
            lLendNmOverrideBit              = true;
            lLockedDaysOverrideBit          = true;
            lRateSheetXmlContentOverrideBit = true;
            lLpeFeeMinOverrideBit           = true;
            lLpeFeeMaxOverrideBit           = true;
            PairingProductIdsOverrideBit    = true;
            lPpmtPenaltyMonOverrideBit      = true;
            IsEnabledOverrideBit            = true;
            lLpCustomCode1OverrideBit       = true;
            lLpCustomCode2OverrideBit = true;
            lLpCustomCode3OverrideBit = true;
            lLpCustomCode4OverrideBit = true;
            lLpCustomCode5OverrideBit = true;
            lLpInvestorCode1OverrideBit = true;
            lLpInvestorCode2OverrideBit = true;
            lLpInvestorCode3OverrideBit = true;
            lLpInvestorCode4OverrideBit = true;
            lLpInvestorCode5OverrideBit = true;
            lLpmiSupportedOutsidePmiOverrideBit = true;
            ProductCodeOverrideBit = true;
            ProductIdentifierOverride = true;
        }

        /// <summary>
        /// We have created a derived loan product, so setup the override
        /// bits to work well with fixup and subsequent saving.
        /// </summary>

        public virtual void InitInherits()
        {
            // Setup override bits as if this product was brand new.

            lCcTemplateIdOverrideBit        = false;
            lLpTemplateNmOverrideBit        = false;
            lLendNmOverrideBit              = false;
            lLockedDaysOverrideBit          = false;
            lRateSheetXmlContentOverrideBit = false;
            lLpeFeeMinOverrideBit           = false;
            lLpeFeeMaxOverrideBit           = false;
            PairingProductIdsOverrideBit    = false;
            lPpmtPenaltyMonOverrideBit      = false;
            IsEnabledOverrideBit            = false;
            lLpCustomCode1OverrideBit = false;
            lLpCustomCode2OverrideBit = false;
            lLpCustomCode3OverrideBit = false;
            lLpCustomCode4OverrideBit = false;
            lLpCustomCode5OverrideBit = false;
            lLpInvestorCode1OverrideBit = false;
            lLpInvestorCode2OverrideBit = false;
            lLpInvestorCode3OverrideBit = false;
            lLpInvestorCode4OverrideBit = false;
            lLpInvestorCode5OverrideBit = false;
            lLpmiSupportedOutsidePmiOverrideBit = false;
            ProductCodeOverrideBit = false;
            ProductIdentifierOverride = false;
        }

        /// <summary>
        /// For all derived products, we initialize these fields to a
        /// particular state that is consistent with being derived.
        /// Put starting values here for all derived products.
        /// </summary>

        public virtual void InitDerived()
        {
            // Initialize the following fields with derivation seeds.


            #region OPM #2415 - When deriving programs, rate delta & fee delta should not be inherited.

            this.lFeeDelta = 0;
            this.lRateDelta = 0;
            #endregion
            lLpeFeeMinParam = 0;
        }

        /// <summary>
        /// Commit the program to the database (whether new or existing).
        /// </summary>

		public virtual void Save()
		{
            FixupCachedFields();

			// 06/15/07 mf. OPM 16337. Saving can create deadlocks.
			// We want to retry save here upon failure.  Also move
			// the statement preperation to before the connection
			// is requested.

			DbCommand cmdLoanProdUpdate = null;
                    
			if( m_dsLoanProd != null && m_dsLoanProd.HasChanges() )
			{
				cmdLoanProdUpdate = PrepareLoanProdUpdateCmd();
			}

			if( cmdLoanProdUpdate != null )
			{
				int retry = 1;
				bool isSaved = false;

				while ( isSaved == false )
				{
					try
					{
                        using (PerformanceStopwatch.Start("CLoanProductBase.Save - db"))
                        {
                            using (var conn = DbAccessUtils.GetConnection(DataSrc.LpeSrc))
                            {
                                conn.OpenWithRetry();

                                DbTransaction tx = conn.BeginTransaction(IsolationLevel.Serializable);

                                if (cmdLoanProdUpdate != null)
                                {
                                    cmdLoanProdUpdate.Connection = conn;
                                    cmdLoanProdUpdate.Transaction = tx;

                                    m_daLoanProd = DbAccessUtils.GetDbProvider().CreateDataAdapter();
                                    m_daLoanProd.UpdateCommand = cmdLoanProdUpdate;

                                    var factory = LqbGrammar.GenericLocator<LqbGrammar.Drivers.ISqlDriverFactory>.Factory;
                                    var driver = factory.Create(LqbGrammar.DataTypes.TimeoutInSeconds.Default);

                                    LqbGrammar.DataTypes.DBTableName? tableName = LqbGrammar.DataTypes.DBTableName.Create("TEMPLATE");

                                    driver.UpdateDataSet(m_daLoanProd, m_dsLoanProd, tableName);
                                }

                                tx.Commit();

                                isSaved = true;
                            }
                        }
					}
					catch( Exception e )
					{
						LogError("Loan program template save try " + retry++ + " failed.", e );
						if (retry > 3) throw e;
						Tools.SleepAndWakeupRandomlyWithin(1000, 4000);
					}
				}
			}
		}

        /// <summary>
        /// Commit the program to the database (whether new or existing).
        /// </summary>
        public void Save( CStoredProcedureExec spExec )
        {
            FixupCachedFields();

            DbCommand cmdLoanProdUpdate = null;

            if (m_dsLoanProd != null && m_dsLoanProd.HasChanges())
            {
                cmdLoanProdUpdate = PrepareLoanProdUpdateCmd();
            }

            if (cmdLoanProdUpdate != null)
            {
                spExec.Update(m_dsLoanProd, cmdLoanProdUpdate, "TEMPLATE");
            }
        }

        protected override void OnFieldChange(string fieldName)
        {
            // NO OP
        }
        protected override void OnFieldChange( string strFieldNm, SqlDbType dbType, object oldValue, object newValue )
        {
            if( null != m_cmdUpdateLoanProd )
            {
                DbParameterCollection parameters = m_cmdUpdateLoanProd.Parameters;
                StringCollection           fields = m_updatedLoanProdFields;

                if( !parameters.Contains( "@p" + strFieldNm ) )
                {
                    SqlParameter param = new SqlParameter( "@p" + strFieldNm, dbType );
                    param.SourceVersion = DataRowVersion.Current;
                    param.SourceColumn = strFieldNm;

                    parameters.Add( param );
                }

                if( !fields.Contains( strFieldNm ) )
                {
                    fields.Add( strFieldNm );
                }
            }
        }

        public virtual void ChangeAllFields()
        {
            if( m_cmdUpdateLoanProd != null )
            {
                DbParameterCollection parameters = m_cmdUpdateLoanProd.Parameters;
                StringCollection           fields = m_updatedLoanProdFields;

                foreach( DataColumn dCol in m_rowLoanProd.DataRow.Table.Columns )
                {
                    if( dCol.ColumnName != "lLpTemplateId" && dCol.ColumnName !=  "SrcRateOptionsProgId" &&
                        dCol.ColumnName != "SrcRateOptionsProgIdInherit"  ) 
                    {

                        if( !parameters.Contains( "@p" + dCol.ColumnName ) )
                        {
                            SqlParameter sP = new SqlParameter( "@p" + dCol.ColumnName , null );
                            if (dCol.ColumnName == "RatesProtobufContent")
                            {
                                // SetBinaryField(string fieldName, byte[] newVal) uses SqlDbType.Image instead of SqlDbType.VarBinary, 
                                // as VarBinary is capped at 8000 bytes.
                                sP.SqlDbType = SqlDbType.Image;
                            }

                            sP.SourceVersion = DataRowVersion.Current;
                            sP.SourceColumn  = dCol.ColumnName;

                            parameters.Add( sP );
                        }

                        if( !fields.Contains(  dCol.ColumnName ) )
                        {
                            fields.Add(  dCol.ColumnName );
                        }
                    }
                }
            }
        }

        override public void ApplyTo( Guid loanFileId, bool bApplyCcTemplate )
        {
            CAllData loanData = new CAllData( loanFileId );
            loanData.InitSave(ConstAppDavid.SkipVersionCheck);
            loanData.ApplyLoanProductTemplate( this, bApplyCcTemplate );
            loanData.Save();
        }
		
		#region RATESHEET

        #region OPM 18661 - Ratesheet expiration feature

		public virtual string LpeAcceptableRsFileId
		{
			get { return GetStringVarCharField( "LpeAcceptableRsFileId" ); }
			set { SetStringVarCharField( "LpeAcceptableRsFileId", value ); }
		}
		public virtual long LpeAcceptableRsFileVersionNumber
		{
			get { return GetBigCountField( "LpeAcceptableRsFileVersionNumber" ); }
			set { SetBigCountField( "LpeAcceptableRsFileVersionNumber", value ); }
		}
		public string LpeAcceptableRsFileVersionNumber_rep
		{
			get { return GetBigCountField_rep( "LpeAcceptableRsFileVersionNumber" ); }
			set { LpeAcceptableRsFileVersionNumber = m_convertLos.ToBigCount( value ); }
		}

        #endregion

        public string lRateOptionBaseId
        {
            get { return GetStringVarCharField( "lRateOptionBaseId" ); }
            set { SetStringVarCharField( "lRateOptionBaseId", value ); }
        }

        public bool lRateSheetXmlContentOverrideBit
        {
            get { return GetBoolField( "lRateSheetxmlContentOverrideBit" ); }
            set 
            { 
                SetBoolField( "lRateSheetxmlContentOverrideBit", value ); 
                SetGuidField("SrcRateOptionsProgId", lRateSheetXmlContentOverrideBit ? m_fileId : SrcRateOptionsProgIdInherit);
            }
        }


        public DateTime lRateSheetDownloadStartD 
        {
            get { return GetDateTimeField( "lRateSheetDownloadStartD", new DateTime(2000,1,1) ); }
            set { SetDateTimeField("lRateSheetDownloadStartD", value); }
        }

        public string lRateSheetDownloadStartD_rep 
        {
            get 
            {
                try 
                {
                    return GetDateTimeField_rep("lRateSheetDownloadStartD");
                } 
                catch 
                {
                    return "";
                }
            }
            set { SetDateTimeField_rep("lRateSheetDownloadStartD", value); }
        }
        public DateTime lRateSheetDownloadEndD 
        {
            get { return GetDateTimeField("lRateSheetDownloadEndD", new DateTime(2000, 1, 1) ); }
            set { SetDateTimeField("lRateSheetDownloadEndD", value); }

        }
        public string lRateSheetDownloadEndD_rep
        {
            get 
            { 
                try 
                {
                    return GetDateTimeField_rep("lRateSheetDownloadEndD");
                } 
                catch 
                {
                    return "";
                }
            }
            set { SetDateTimeField_rep("lRateSheetDownloadEndD", value); }
        }

        /// <summary>
        /// Base rates
        /// </summary>

        public CRateSheetFieldsReadOnly[] RateSheetInherit
        {
            get
            {

                // DataSet dS = lRateSheetDataSetInherit;

                if( null == m_dsRateSheetInherit )
                {
                    DataSet ds           = GetDataSet( s_RateSheetXmlSchema, GetLongTextField( "lRateSheetXmlContentInherit" ) );
                    m_dsRateSheetInherit = AdjustRateSheetDataSetWithDeltas(ds, lRateDeltaInherit, lFeeDeltaInherit);
                }

                DataSet dS = m_dsRateSheetInherit;

                int i , c;

                if( dS != null )
                {
                    CRateSheetFieldsReadOnly[] rateOptions = new CRateSheetFieldsReadOnly[ c = dS.Tables[ 0 ].Rows.Count ];

                    for( i = 0 ; i < c ; ++i )
                    {
                        rateOptions[ i ] = new CRateSheetFieldsReadOnly( this , dS , i );
                    }

                    return rateOptions;
                }

                return null;
            }
        }

		private string my_lRateSheetXmlContent
		{            
			get { return GetLongTextField("lRateSheetXmlContent" ); }
			set 
            { 
                if( lRateSheetXmlContentOverrideBit == false)
                    throw new CBaseException( ErrorMessages.Generic, "lRateSheetXmlContent is readonly if lRateSheetXmlContentOverrideBit = false" );

                SetLongTextField( "lRateSheetXmlContent", value ); 
            }
		}

        private byte[] my_RatesProtobufContent
        {
            get { return GetBinaryField("RatesProtobufContent"); }

            set { SetBinaryField("RatesProtobufContent", value); }
        }
        /// <summary>
        /// Base (investor) rates
        /// </summary>
        /// 

        public void Set_lRateSheetDataSetWithoutDeltas( DataSet ds )
        {
            my_lRateSheetXmlContent = ds.GetXml();
            m_dsRateSheet           = null;
        }


        virtual protected DataSet RateSheetXmlContent2SimpleDataSet()
        {
            return GetDataSet( s_RateSheetXmlSchema, my_lRateSheetXmlContent );
        }

		public DataSet lRateSheetDataSetWithoutDeltas
		{
			get	
			{ 
                if( null == m_dsRateSheet )
                {
                    DataSet ds    = RateSheetXmlContent2SimpleDataSet();

                    m_dsRateSheet = AdjustRateSheetDataSetWithDeltas( ds, (lRateSheetXmlContentOverrideBit ? 0 : lRateDeltaInherit), 
                                                                          (lRateSheetXmlContentOverrideBit ? 0 : lFeeDeltaInherit) );
                }
                return m_dsRateSheet;
			}
		}

        private DataSet AdjustRateSheetDataSetWithDeltas(DataSet ds, decimal rateDelta, decimal feeDelta)
        {
            if( ds == null )
                return null;

            int count = ds.Tables[0].Rows.Count;

            for (int i = 0; i < count; i++)
            {
                CRateSheetFields f = new CRateSheetFields(this, ds, i);
                if (f.IsSpecialKeyword)
                    continue; // Skip
                        
                DataRow row = ds.Tables[0].Rows[i];
                try 
                {
                    row[ "Rate"  ]  = string.Format( "{0:N3}%" , Convert.ToDecimal( row[ "Rate"  ].ToString().TrimEnd( '%' ) ) + rateDelta);
                } 
                catch (Exception exc) 
                {
                    Tools.LogWarning("Unable to set rate in lRateSheetDataSetWithDeltas. Rate=" + row["Rate"] + ".", exc);
                }
                try 
                {
                    row[ "Point" ] = string.Format( "{0:N3}%" , Convert.ToDecimal( row[ "Point" ].ToString().TrimEnd( '%' ) ) + feeDelta);
                } 
                catch (Exception exc) 
                {
                    Tools.LogWarning("Unable to set point in lRateSheetDataSetWithDeltas. Point=" + row["Point"] + ".", exc);
                }

            }
            return ds;
        }

		public DataSet lRateSheetDataSetWithDeltas
		{
			get	
			{ 
				if( null == m_dsRateSheetWithDeltas )
				{
                    DataSet ds    = RateSheetXmlContent2SimpleDataSet();

                    m_dsRateSheetWithDeltas = AdjustRateSheetDataSetWithDeltas(ds, effective_lRateDelta, effective_lFeeDelta);
				}
				return m_dsRateSheetWithDeltas;
			}
		}

        internal void SetRateSheetXmlContent(string xml)
        {
            // 11/16/2016 - dd
            // This method is use in RateOptionImport. Ideally, I want to get rid of this method and use the UploadCSVRateSheet() instead.
            // However, as of right now I don't have time to verify the logic in RateOptionImport and UploadCSVRateSheet are equivalent.
            my_lRateSheetXmlContent = xml;

            m_dsRateSheet = null;
            m_dsRateSheetWithDeltas = null;
        }

        /// <summary>
        /// Upload new comma separated list of ratesheet to loan product.
        /// Note that the existing set is maintained -- new entries are
        /// appended onto the tail of the set.
        /// 
        /// 5/5/2005 kb - We now clear the existing before uploading.
        /// </summary>
        /// <param name="content">Comma or space separated list of rate,point</param>
		public void UploadCSVRateSheet(string content) 
		{

			RateSheetRemoveAll();

			// OPM 12259 mf. Here we need to determine if we are looking at old or 
			// new 2d rate format

			RateSheetLineItems lines = new RateSheetLineItems();

			if ( Is2dFormat(content) )
			{
				RateSheet2D rates = new RateSheet2D();
				rates.Process( content );
				rates.ApplyToLineItems( lines );
			}
			else
			{
				// Original Processing of the rates.
			
				ParseRateOptionCSV( content, lines );
			}
		
			if (lines.IsValidRateOption()) 
			{
				my_lRateSheetXmlContent = lines.ToString();

				m_dsRateSheet = null;
				m_dsRateSheetWithDeltas = null;

				// 9/19/2005 dd - If there is special keyword LOCK, PP then update the range.
				if (lines.HasSpecialKeyword) 
				{
					lLockedDays = lines.MaxProdRLckDays == int.MinValue ? -1 : lines.MaxProdRLckDays;
					lLockedDaysLowerSearch = lines.MinProdRLckdDays == int.MaxValue ? -1 : lines.MinProdRLckdDays;
					lPpmtPenaltyMon = lines.MaxProdPpmtPenaltyMon == int.MinValue ? -1 : lines.MaxProdPpmtPenaltyMon;
					lPpmtPenaltyMonLowerSearch = lines.MinProdPpmtPenaltyMon == int.MaxValue ? -1 : lines.MinProdPpmtPenaltyMon;
					lIOnlyMonLowerSearch = lines.MinIOnlyMon == int.MaxValue ? -1 : lines.MinIOnlyMon;
					lIOnlyMonUpperSearch = lines.MaxIOnlyMon == int.MinValue ? -1 : lines.MaxIOnlyMon;

					if (IsDerived) 
					{
						lLockedDaysOverrideBit = true;
						lPpmtPenaltyMonOverrideBit = true;
						lIOnlyMonLowerSearchOverrideBit = true;
						lIOnlyMonUpperSearchOverrideBit = true;
					}
				}

			} 
			else 
			{
				throw new CBaseException("Invalid rate option", "Invalid rate option");
			}
			
		}

		public static bool Is2dFormat(string content)
		{
			// Inexpensive way to tell if we are looking at the old format or new one.
			// (Once everything is migrated, we will remove this check)

			// We assume SAE would not mix up keyword styles in the same ratesheet.
			if ( content.TrimWhitespaceAndBOM() != string.Empty)
			{
				string peek = content.Substring(0, Math.Min(content.Length, 21)).Trim('\t',',').TrimWhitespaceAndBOM().ToUpper();
			
				if ( peek.StartsWith("LOCK")
					|| peek.StartsWith("IO")
					|| peek.StartsWith("PP") )
				{
					return (peek.IndexOf(":") > 0);
				}
			}
			// We know that it did not start with a keyword, so
			// we can process as old. For it to be 2d with more than one group in x direction,
			// it would have to start with a keyword for the grouping.
			return false;
		}

		private void ParseRateOptionCSV( string content, RateSheetLineItems lines )
		{
			// Rate options getting a little too complex for maintainable regular expression.
			// Parse it out here.  We accept 3-5 columns, 4 and 5 are optional.
			Regex rateex = new Regex("[-A-Z0-9.]+%?");
			foreach(string line in content.Split('\n') )
			{
				string[] ratelist = new string[5];
				bool acceptOption = true;
				for (int i = 0; i < ratelist.Length; i++ )
				  ratelist[i] = "0";

				string[] rates = line.Split( ',', '\t' );
				if ( rates.Length < 3 )
					continue;

				for (int i = 0; i < rates.Length; i++ )
				{
					if ( rates[i].TrimWhitespaceAndBOM() == string.Empty && i >= 3 )
						continue;

					if ( rateex.IsMatch( rates[i].TrimWhitespaceAndBOM() ) )
						ratelist[i] = rates[i].TrimWhitespaceAndBOM();
					else
					{
						acceptOption = false;
						break;
					}
				}
				
				if (acceptOption) lines.Add( ratelist[0], ratelist[1], ratelist[2], ratelist[3], ratelist[4] );
			}
		}
        /// <summary>
        /// Parse CSV ratesheet. First value will be rate and second value is fee.
        /// </summary>
        public void UploadCSVRateSheet(Stream stream) 
        {
            using (StreamReader reader = new StreamReader(stream)) 
            {
                UploadCSVRateSheet(reader.ReadToEnd());
            }
        }

		// OPM 12259. 09/13/07 mf.  We need to accept 2d input of rate options
		public class RateSheet2D
		{
		#region Member variables
			RateNode m_root = new RateNode();
			private readonly string[] allowedKeywords = new string[3] {"PP", "LOCK", "IO" };
			private const int RATE_ENTRY_LENGTH = 5;
			private Hashtable seenKeywords = new Hashtable();
		#endregion

			// Process 2d input to a tree of RateNodes.
			public void Process(string input)
			{
				m_root.Keyword = "ROOT";
				int yOffset = 0;

				StringReader reader = new StringReader(input);
				int lineNo = 0;

				string currentLine = reader.ReadLine();

				// Process Headers. (There could be no headers or many).
				while ( currentLine != null && IsKeyWordLine( currentLine ) )
				{
					currentLine = currentLine.TrimEnd(','); // Excel inserts trailing commas.
					ArrayList headerNodes = new ArrayList();
					if( ( headerNodes = GetHeaderNodes(currentLine) ) != null )
					{
						if ( m_root.Children.Count > 0 )
						{
							Queue attachList = new Queue(headerNodes.Count * 2);
							
							foreach (RateNode headerNode in headerNodes)
							{

								RateNode nodeToAttach = m_root.GetSubNodesByPosition( lineNo, headerNode.Position );

								attachList.Enqueue(headerNode);
								attachList.Enqueue(nodeToAttach);
							}

							// Need to do this attaching later, so we hit the right nodes.
							while( attachList.Count > 0 )
							{
								RateNode toBeAdded = (RateNode) attachList.Dequeue();
								RateNode toAcceptNewChild = (RateNode) attachList.Dequeue();
								toAcceptNewChild.AddNode( toBeAdded );

								//Tools.LogInfo( String.Format( "Process: Attach {0}: {1} - {2}(Position:{3}) to {4}: {5} - {6}(Position:{7})",
									//toBeAdded.Keyword, toBeAdded.Min, toBeAdded.Max, toBeAdded.Position,
									//toAcceptNewChild.Keyword, toAcceptNewChild.Min, toAcceptNewChild.Max, toAcceptNewChild.Position) );

							}
						}
						else
						{
							// Root has nobody. We add first generation.
							foreach (RateNode headerNode in headerNodes)
								m_root.AddNode( headerNode );
						}
					}
					else
						break; // Header Over

					currentLine = reader.ReadLine();
					lineNo++;
				}

				while ( currentLine != null )
				{
					// At this point we should have the nodes set up.  We would just need to add
					// the line items to the appropriate nodes.
					currentLine = currentLine.TrimEnd(',');

					if ( !IsKeyWordLine( currentLine ) )
					{
						
						// Add each entry to the appropriate rate node.
						ArrayList rateLines = GetRateGroups(currentLine, yOffset);
						for (int i = 0; i < rateLines.Count; i++)
						{ 
							((RateNode) m_root.GetSubNodesByPosition(lineNo, (i * RATE_ENTRY_LENGTH) + yOffset)).Rates.Add(rateLines[i]);
						}

					}
					else
					{
						// This is a new Y-axis group.  Any line items we see will need to be added to the
						// new node.  All of the nodes one level above this will get a copy with their
						// appropriate rate lines.
						
						yOffset = 1; // For now, allow only one level of nesting in Y direction.

						string[] lineItems = currentLine.Split('\t', ',');
											
						ArrayList nodes = GetHeaderNodes( lineItems[0] );

						ArrayList lines = GetRateGroups(lineItems, yOffset);

						ArrayList attachableLeaves = new ArrayList();

						RateNode baseNode = nodes[0] as RateNode;

						m_root.GetLeafNodes( attachableLeaves );

						if (attachableLeaves.Count != lines.Count)
						{
							throw new CBaseException("Parse Error: Number of groups (" + attachableLeaves.Count + ")  does not match with rates found (" + lines.Count + ")."
								, "Parse Error: Number of groups does not match with rates found.");
						}

						for (int i = 0; i < attachableLeaves.Count; i++)
						{
							RateNode newNode = new RateNode();

							newNode.Keyword = baseNode.Keyword;
							newNode.Min = baseNode.Min;
							newNode.Max = baseNode.Max;
							newNode.Position = lineNo;
							newNode.isYGroup = true;
                        
							string[] rateItem = (string[]) lines[i];
							newNode.Rates.Add(rateItem);

							RateNode newParent = attachableLeaves[i] as RateNode;
							newParent.AddNode(newNode);
						}
					}
					currentLine = reader.ReadLine();
					lineNo++;
				}
			}

			public void ApplyToLineItems( LendersOffice.LoanPrograms.RateSheetLineItems lines )
			{
				// We use this method is to get our tree structure into a flat ratesheet
				// The order of the Keywords should match the order of the input.
				m_root.ApplyToLineItems(lines);
			}

			// Check if this line appears to begin with a keyword.
			private bool IsKeyWordLine( string line )
			{
				string processLine = line.Trim(',','\t').TrimWhitespaceAndBOM().ToUpper();
				foreach( string keyWord in allowedKeywords)
				{
					// Cheap way to tell if this is a keyword line
					if ( processLine.StartsWith( keyWord ) )
						return true;
				}

				return false;
			}


			private ArrayList GetRateGroups( string line, int position )
			{
				return GetRateGroups( line.Split('\t',','), position);
			}

			// Get all rate groups in this line.
			// Return list of all rates.
			private ArrayList GetRateGroups( string[] lineItems, int position )
			{
				ArrayList ret = new ArrayList();
			
				int count = 0;

				string[] rates = new string[RATE_ENTRY_LENGTH];
				
				for (int i = position; i < lineItems.Length; i++)
				{
					rates[count++ % RATE_ENTRY_LENGTH] = lineItems[i];
				
					if ( count % RATE_ENTRY_LENGTH == 0 )
					{
						ret.Add(rates);
						rates = new string[RATE_ENTRY_LENGTH];
					}
				}

				return ret;
			}

			// Get all header nodes from this header line.
			private ArrayList GetHeaderNodes( string line )
			{
				Regex expression = new Regex(@"(?<keyword>(IO|LOCK|PP))\s*:\s*(?<min>[0-9]+)\s*-\s*(?<max>[0-9]+)", System.Text.RegularExpressions.RegexOptions.Compiled|System.Text.RegularExpressions.RegexOptions.IgnoreCase);

				ArrayList ret = new ArrayList();
				int positionCount = 0;
				foreach (string str in line.Split('\t', ',') )
				{
					if (str.TrimWhitespaceAndBOM() != string.Empty)
					{
						System.Text.RegularExpressions.Match match = expression.Match(str);
						if ( match.Success )
						{							
							RateNode node = new RateNode();
							node.Keyword = match.Groups["keyword"].Value;
							node.Min = int.Parse(match.Groups["min"].Value);
							node.Max = int.Parse(match.Groups["max"].Value);
							node.Position = positionCount;
							node.isYGroup = false;

							ret.Add(node);
						}
						else
						{
							return null;
						}
					}
					positionCount++;
				}

				return ret;
				
			}

			// Node in our rate tree.
			private class RateNode
			{
				private string m_keyword = string.Empty; // Keyword of this node.
				private int m_min = -1; // Start of range of this keyword.
				private int m_max = -1; // End of range of this keyword.
				private int m_position; // Tells us offset into the document, or line number.
				private bool m_isYGroup; // Tells us the axis of the group
				private RateNode m_parent = null; // Upward pointer to parent of this node.
				private ArrayList m_children = new ArrayList(); // Child rate nodes.
				private ArrayList m_rateItems = new ArrayList(); // RateList of rate options.

				public string Keyword
				{
					get { return m_keyword; }
					set { m_keyword = value.ToUpper(); }
				}
				public int Min
				{
					get { return m_min; }
					set { m_min = value; }
				}
				public int Max
				{
					get { return m_max; }
					set { m_max = value; }
				}
				public int Position
				{
					get { return m_position; }
					set { m_position = value; }
				}
				public bool isYGroup
				{
					get { return m_isYGroup; }
					set { m_isYGroup = value; }
				}
				public RateNode Parent
				{
					get { return m_parent; }
					set { m_parent = value; }
				}
				public ArrayList Children
				{
					get { return m_children; }
					set { m_children = value; }
				}

				public ArrayList Rates
				{
					get { return m_rateItems; }
					set { m_rateItems = value; }
				}

				// Add a child node
				public void AddNode( RateNode node )
				{
					node.Parent = this;
					Children.Add(node);
				}

				// Go up tree to find all direct ancestors 
				// (keyword ranges applied to this node)
				public ArrayList GetAllAppliedKeywords()
				{
					ArrayList ret = new ArrayList();
					RateNode node = this;
					while ( node != null && node.Parent != null )
					{
						ret.Add(node);
						node = node.Parent;
					}
					return ret;
				}

				// Given a line and column position, find out what node governs that
				// area of the document for parsing.
				public RateNode GetSubNodesByPosition(int line, int position)
				{
					RateNode previousNode = null;
					foreach (RateNode node in m_children)
					{
						int compareVal = (node.isYGroup) ? line : position;
						if ( node.Position == compareVal )
						{
							return node.GetSubNodesByPosition( line, position );
						}
						else
						{
							if ( node.Position > compareVal )
							{
								if ( previousNode != null )
								{
									return previousNode.GetSubNodesByPosition(line, position);
								}
							}

						}
						previousNode = node;
					}

					// If we made it out here, we overshot our node.
					if (previousNode != null)
						return previousNode.GetSubNodesByPosition(line, position);
					else
					{
						// Here, we might be able to check for error,
						// that there is a missing starting node.
						return this;
					}
				}

				// For storage in the DB we need to format our tree into
				// the flat list.
				public void ApplyToLineItems(RateSheetLineItems lines)
				{
					if (m_children.Count == 0 )
					{
						// Add all applied keywords
						ArrayList appliedKeywords = GetAllAppliedKeywords();
						appliedKeywords.Reverse();
						foreach ( RateNode node in appliedKeywords )
						{
							lines.Add(
								node.Keyword
								, node.Min.ToString()
								, node.Max.ToString()
								, "0"
								, "0"
								);
							//Tools.LogInfo( node.Keyword + ", " + node.Min + ", " + node.Max + ", 0, 0");

						}

						// Add all rates
						foreach (string[] rate in m_rateItems)
						{
							lines.Add(rate[0], rate[1], rate[2], rate[3], rate[4] );
							//Tools.LogInfo(String.Format("{0}, {1}, {2}, {3}, {4}", rate[0], rate[1], rate[2], rate[3], rate[4]) );
						}
					}
					else
					{
						foreach (RateNode node in m_children)
						{
							node.ApplyToLineItems(lines);
						}
					}
				}

				// Get the leaves.  Parameter getLowest specifies that we would
				// want the leaves to attach rates to.  Otherwise, we would get 
				// the parents of the leaves (for adding more leaves to).
				
				// getLowest is always false.  We never need to get the attachable
				// leaves because we use the position to determine that.\

				// It would appear to be more complicated than that.  We need to 
				// get the leaves when the first y group has not been added yet,
				// then we get the parentals.
				public void GetLeafNodes( ArrayList nodeContainer )
				{
					if ( m_children.Count > 0 )
					{
						foreach(RateNode node in m_children)
						{
							node.GetLeafNodes( nodeContainer );
						}
					}
					else
					{
						// We are a leaf w/rates, add our parent.
						if ( Rates.Count != 0 )
						{
							if ( Parent != null )
							{
								if ( !nodeContainer.Contains( Parent ) )
								{
									nodeContainer.Add( Parent );
								}
							}
						}
						else
						{
							// We are a leaf with no rates, add ourself.
							if ( !nodeContainer.Contains( this ) )
							{
								nodeContainer.Add( this );
							}

						}
					}

				}

			}

		}


		/// <summary>
		/// Base rates
		/// </summary>
		/// <param name="recordId"></param>
		/// <returns></returns>

		public CRateSheetFields GetRateSheetFieldsWithoutDeltas( Guid recordId )
		{
			return new CRateSheetFields( this, lRateSheetDataSetWithoutDeltas, recordId );
		}

		
		//RateSheetWithoutDeltas
		/// <summary>
		/// Base rates
		/// </summary>

		public CRateSheetFields[] RateSheetWithoutDeltas
		{
			get
			{
				int count = lRateSheetDataSetWithoutDeltas.Tables[0].Rows.Count;
				CRateSheetFields[] rateOptions = new CRateSheetFields[ count ];
				for( int i = 0; i < count; ++i )
				{
					rateOptions[i] = new CRateSheetFields( this, lRateSheetDataSetWithoutDeltas, i );
				}
				
				return rateOptions;
			}
		}

		/// <summary>
		/// Base rates
		/// </summary>

        private List<CRateSheetGroup> m_rateSheetGroupList = null;
        private void ConstructRateSheetGroupList() 
        {
            using (PerformanceStopwatch.Start("CLoanProductBase.ConstructRateSheetGroupList"))
            {
                m_rateSheetGroupList = new List<CRateSheetGroup>();

                int count = lRateSheetDataSetWithDeltas.Tables[0].Rows.Count;

                CRateSheetGroup rateSheetGroup = null;
                int minProdRLckdDays = int.MinValue;
                int maxProdRLckdDays = int.MaxValue;
                int minProdPpmtPenaltyMon = int.MinValue;
                int maxProdPpmtPenaltyMon = int.MaxValue;
                int minIOnlyMon = int.MinValue;
                int maxIOnlyMon = int.MaxValue;
                bool bCreateNewGroup = true;
                for (int i = 0; i < count; i++)
                {
                    CRateSheetFields field = new CRateSheetFieldsReadOnly(this, lRateSheetDataSetWithDeltas, i);
                    if (field.IsSpecialKeyword)
                    {
                        bCreateNewGroup = true;
                        if (field.RateSheetKeyword == E_RateSheetKeywordT.Lock)
                        {
                            minProdRLckdDays = field.LowerLimit;
                            maxProdRLckdDays = field.UpperLimit;
                        }
                        else if (field.RateSheetKeyword == E_RateSheetKeywordT.Prepay)
                        {
                            minProdPpmtPenaltyMon = field.LowerLimit;
                            maxProdPpmtPenaltyMon = field.UpperLimit;
                        }
                        else if (field.RateSheetKeyword == E_RateSheetKeywordT.IO)
                        {
                            minIOnlyMon = field.LowerLimit;
                            maxIOnlyMon = field.UpperLimit;
                        }

                        continue;
                    }

                    if (bCreateNewGroup)
                    {
                        rateSheetGroup = new CRateSheetGroup(minProdRLckdDays, maxProdRLckdDays, minProdPpmtPenaltyMon, maxProdPpmtPenaltyMon, minIOnlyMon, maxIOnlyMon);
                        m_rateSheetGroupList.Add(rateSheetGroup);
                        bCreateNewGroup = false;
                    }
                    rateSheetGroup.AddRate(field);

                }
            }

            
        }

        public IRateItem[] GetRawRateSheetWithDeltas() 
        {
            int count = lRateSheetDataSetWithDeltas.Tables[0].Rows.Count;
            IRateItem[] rateOptions = new IRateItem[count];
            for( int i = 0; i < count; ++i )
            {
                rateOptions[i] = new CRateSheetFields( this, lRateSheetDataSetWithDeltas, i );
            }
				
            return rateOptions;

        }

		public decimal lRateDelta
		{
			get { return GetRateField( "lRateDelta" ); }
			set 
			{ 
				SetRateField( "lRateDelta", value ); 
				m_dsRateSheetWithDeltas = null;
			}
		}
		public string lRateDelta_rep
		{
			get { return   GetRateField_rep( "lRateDelta" ); }
			set { lRateDelta = m_convertLos.ToRate( value ); }
		}

        public decimal effective_lRateDelta
        {
            get 
            { 
                return lRateDelta   + (lRateSheetXmlContentOverrideBit ? 0 : lRateDeltaInherit);
            }
        }


		public decimal lFeeDelta
		{
			get { return GetRateField( "lFeeDelta" ); }
			set 
			{ 
				SetRateField( "lFeeDelta", value ); 
				m_dsRateSheetWithDeltas = null;
			}
		}
		public string lFeeDelta_rep
		{
			get { return   GetRateField_rep( "lFeeDelta" ); }
			set { lFeeDelta = m_convertLos.ToRate( value ); }
		}

        public decimal effective_lFeeDelta
        {
            get
            {
                return lFeeDelta  + (lRateSheetXmlContentOverrideBit ? 0 :  lFeeDeltaInherit);
            }
        }

		public void RateSheetRemoveAll()
		{
			lRateSheetDataSetWithoutDeltas.Tables[0].Clear();
            m_dsRateSheet = null;
		}

        public Guid SrcRateOptionsProgIdInherit 
        {
            get {   return GetGuidField("SrcRateOptionsProgIdInherit"); }
            set {   SetGuidField("SrcRateOptionsProgIdInherit", value); }
        }

        public Guid SrcRateOptionsProgId
        {
            get {   return GetGuidField("SrcRateOptionsProgId"); }
        }

        public Guid EffectiveSrcRateOptionProgId
        {
            get
            {
                return ( SrcRateOptionsProgId == Guid.Empty ) ? lLpTemplateId : SrcRateOptionsProgId; 
                    
            }
        }

        public decimal lRateDeltaInherit
        {
            get { return GetRateField( "lRateDeltaInherit" ); }
            set 
            { 
                SetRateField( "lRateDeltaInherit", value ); 
                m_dsRateSheetWithDeltas = null;
                m_dsRateSheetInherit    = null;
            }
        }

        public decimal lFeeDeltaInherit
        {
            get { return GetRateField( "lFeeDeltaInherit" ); }
            set 
            { 
                SetRateField( "lFeeDeltaInherit", value ); 
                m_dsRateSheetWithDeltas = null;
                m_dsRateSheetInherit    = null;
            }
        }

		#endregion // RATESHEET

		#region FIELDS

		[DependsOn()]
		public string lLpInvestorNm
		{
			get { return GetStringVarCharField( "lLpInvestorNm" ); }
			set { SetStringVarCharField( "lLpInvestorNm", value ); }
		}

        public string ProductCode
        {
            get { return GetStringVarCharField( "ProductCode" ); }
            set { SetStringVarCharField( "ProductCode", value ); }
        }

        public bool ProductCodeOverrideBit
        {
            get { return GetBoolField("ProductCodeOverrideBit"); }
            set { SetBoolField("ProductCodeOverrideBit", value); }
        }

        public string ProductCodeInherit
        {
            get { return GetStringVarCharField("ProductCodeInherit"); }
            set { SetStringVarCharField("ProductCodeInherit", value); }
        }
        
        public string ProductIdentifier
        {
            get
            {
                return GetStringVarCharField("ProductIdentifier");
            }

            set
            {
                SetStringVarCharField("ProductIdentifier", value);
            }
        }

        public string ProductIdentifierInherited
        {
            get
            {
                return GetStringVarCharField("ProductIdentifierInherited");
            }
            set
            {
                SetStringVarCharField("ProductIdentifierInherited", value);
            }
        }

        public bool ProductIdentifierOverride
        {
            get
            {
                return GetNullableBoolField("ProductIdentifierOverride") ?? false;
            }

            set
            {
                SetNullableBoolField("ProductIdentifierOverride", value);
            }
        }


        public bool IsOptionArm
		{
			get { return GetBoolField( "IsOptionArm" ); }
			set { SetBoolField( "IsOptionArm", value ); }
		}

		public bool lHasQRateInRateOptions
		{
			get { return GetBoolField ( "lHasQRateInRateOptions" ); }
			set { SetBoolField( "lHasQRateInRateOptions" , value ); }
		}
		
		public bool IsLpeDummyProgram
		{
			get { return GetBoolField ( "IsLpeDummyProgram" ); }
			set { SetBoolField ( "IsLpeDummyProgram", value ); }
		}
		public bool IsPairedOnlyWithSameInvestor
		{
			get { return GetBoolField ( "IsPairedOnlyWithSameInvestor" ); }
			set { SetBoolField ( "IsPairedOnlyWithSameInvestor", value ); }
		}
		public bool CanBeStandAlone2nd
		{
			get { return GetBoolField ( "CanBeStandAlone2nd" ); }
			set { SetBoolField ( "CanBeStandAlone2nd", value ); }
		}
		public bool lDtiUsingMaxBalPc
		{
			get { return GetBoolField ( "lDtiUsingMaxBalPc" ); }
			set { SetBoolField ( "lDtiUsingMaxBalPc", value ); }
		}
        
        public QualRateCalculationT lQualRateCalculationT
        {
            get { return this.GetTypeIndexField<QualRateCalculationT>(nameof(lQualRateCalculationT)); }
            set { this.SetTypeIndexField(nameof(lQualRateCalculationT), (int)value); }
        }
        
        public QualRateCalculationFieldT lQualRateCalculationFieldT1
        {
            get { return this.GetTypeIndexField<QualRateCalculationFieldT>(nameof(lQualRateCalculationFieldT1)); }
            set { this.SetTypeIndexField(nameof(lQualRateCalculationFieldT1), (int)value); }
        }
        
        public decimal lQualRateCalculationAdjustment1
        {
            get { return this.GetRateField(nameof(lQualRateCalculationAdjustment1)); }
            set { this.SetRateField(nameof(lQualRateCalculationAdjustment1), value); }
        }

        public string lQualRateCalculationAdjustment1_rep
        {
            get { return this.GetRateField_rep(nameof(lQualRateCalculationAdjustment1)); }
            set { this.lQualRateCalculationAdjustment1 = this.m_convertLos.ToRate(value); }
        }

        public QualRateCalculationFieldT lQualRateCalculationFieldT2
        {
            get { return this.GetTypeIndexField<QualRateCalculationFieldT>(nameof(lQualRateCalculationFieldT2)); }
            set { this.SetTypeIndexField(nameof(lQualRateCalculationFieldT2), (int)value); }
        }

        public decimal lQualRateCalculationAdjustment2
        {
            get { return this.GetRateField(nameof(lQualRateCalculationAdjustment2)); }
            set { this.SetRateField(nameof(lQualRateCalculationAdjustment2), value); }
        }

        public string lQualRateCalculationAdjustment2_rep
        {
            get { return this.GetRateField_rep(nameof(lQualRateCalculationAdjustment2)); }
            set { this.lQualRateCalculationAdjustment2 = this.m_convertLos.ToRate(value); }
        }

        public int lQualTerm
        {
            get
            {
                switch (this.lQualTermCalculationType)
                {
                    case QualTermCalculationType.Standard:
                        return this.lTerm;
                    case QualTermCalculationType.Amortizing:
                        return this.lTerm - this.lIOnlyMon;
                    case QualTermCalculationType.InterestOnly:
                        return this.lIOnlyMon;
                    case QualTermCalculationType.Manual:
                        return this.GetCountField(nameof(lQualTerm), 0);
                    default:
                        throw new UnhandledEnumException(this.lQualTermCalculationType);
                }
            }

            set
            {
                this.SetNullableCountField(nameof(lQualTerm), value);
            }
        }

        public string lQualTerm_rep
        {
            get { return ToCountString(lQualTerm); }
            set { this.lQualTerm = this.m_convertLos.ToCount(value); }
        }

        public QualTermCalculationType lQualTermCalculationType
        {
            get { return this.GetTypeIndexField<QualTermCalculationType>(nameof(lQualTermCalculationType), QualTermCalculationType.Standard); }
            set { this.SetTypeIndexField(nameof(lQualTermCalculationType), (int)value); }
        }

        public Guid lLpTemplateId
		{
			get
			{
				return GetGuidField( "lLpTemplateId" );
			}
		}

		public Guid lBaseLpId
		{
			get { return GetGuidField( "lBaseLpId" ); }
		}

		public Guid BrokerId
		{
			get { return GetGuidField( "BrokerId" ); }
		}
		
		public bool lLpTemplateNmOverrideBit
		{
			get	{ return GetBoolField( "lLpTemplateNmOverrideBit" ); }
			set { SetBoolField( "lLpTemplateNmOverrideBit", value ); }
		}

		public string lLpTemplateNmInherit
		{
			get	{ return GetStringVarCharField( "lLpTemplateNmInherit" ); }
			set { SetStringVarCharField( "lLpTemplateNmInherit", value ); }
		}

		public string lLpTemplateNm
		{
			get	{ return GetStringVarCharField( "lLpTemplateNm" ); }
			set { SetStringVarCharField( "lLpTemplateNm", value ); }
		}

        public string lLpProductType 
        {
            get { return GetStringVarCharField("lLpProductType"); }
            set { SetStringVarCharField("lLpProductType", value); }
        }

        public bool IsHeloc
        {
            get
            {
                // 10/24/2013 dd - Use lLpProductType = HELOC to determine if the program a HELOC.
                return lLpProductType.Equals("HELOC", StringComparison.OrdinalIgnoreCase);
            }
        }

		private string GetFullName( Guid startFolderId )
		{
			if( Guid.Empty == startFolderId )
				return "";

			StringBuilder result = new StringBuilder( 100 );
			//string strCurrentId = startFolderId.ToString();
			Guid guidCurrent = startFolderId;
			while( true )
			{
				DataRow row;
				try
				{
					row = m_rowsFolders.GetRowByKey( guidCurrent );
				}
				catch
				{
					return result.ToString();
				}

				if( result.Length > 0 )
					result.Insert( 0, " / " );
				result.Insert( 0, row["FolderName"].ToString() );
				if( DBNull.Value == row["ParentFolderId"] )
					return result.ToString();
				guidCurrent = (Guid) row["ParentFolderId"];
			}
		}


        public bool IsDerived 
        {
            get { return lBaseLpId != Guid.Empty; }
        }
		public bool IsLpe
		{
			get { return GetBoolField( "IsLpe" ); }
			set { SetBoolField( "IsLpe", value ); }
		}

		public bool IsMaster
		{
			get { return GetBoolField( "IsMaster" ); }
			set { SetBoolField( "IsMaster", value ); }
		}

        public bool IsMasterPriceGroup
        {
            get { return GetBoolField("IsMasterPriceGroup"); }
            set { SetBoolField("IsMasterPriceGroup", value); }
        }

		public bool IsEnabledOverrideBit
		{
			get { return GetBoolField( "IsEnabledOverrideBit" ); }
			set { SetBoolField( "IsEnabledOverrideBit", value ); }
		}

		public bool IsEnabledInherit
		{
			get { return GetBoolField( "IsEnabledInherit" ); }
			set { SetBoolField( "IsEnabledInherit", value ); }
		}

		public bool IsEnabled
		{
			get { return GetBoolField( "IsEnabled" ); }
			set { SetBoolField( "IsEnabled", value ); }
		}

		/// <summary>
		/// return Guid.Empty if it's null
		/// </summary>
		public Guid FolderId
		{
			get
			{
				try
				{
					return GetGuidField( "FolderId" );
				}
				catch
				{
					return Guid.Empty;
				}
			}
		}
		
		public string FullName
		{
			get
			{
				if( "" == m_fullname )
				{
					try
					{
						m_fullname = GetFullName( FolderId );
						if( m_fullname.Length > 0 )
							m_fullname += " / " + lLpTemplateNm;
						else
							m_fullname = lLpTemplateNm;
					}
					catch( Exception ex )
					{
						Tools.LogError( ex );
						m_fullname = "???";
					}
				}
				return m_fullname;
			}
		}


        public bool lIsArmMarginDisplayed
        {
            get 
            {
                if (lFinMethT == E_sFinMethT.ARM)
                    return true; // per opm 13035
                    //return GetBoolField( "lIsArmMarginDisplayed" ); 
                return false;
            }
            set { SetBoolField( "lIsArmMarginDisplayed", value ); }
        }

		public string lLendNm
		{
			get	{ return GetStringVarCharField( "lLendNm" ); }
			set { SetStringVarCharField( "lLendNm", value ); }
		}

		public string lLendNmInherit
		{
			get	{ return GetStringVarCharField( "lLendNmInherit" ); }
			set { SetStringVarCharField( "lLendNmInherit", value ); }
		}

		public bool lLendNmOverrideBit
		{
			get { return GetBoolField( "lLendNmOverrideBit" ); }
			set { SetBoolField( "lLendNmOverrideBit", value ); }
		}


		public decimal lMaxCltvR
		{
			get{ return GetRateField( "lMaxCltvR" ); }
			set{ SetRateField( "lMaxCltvR", value ); }
		}
		public string lMaxCltvR_rep
		{
			get{ return GetRateField_rep( "lMaxCltvR" ); }
			set{ lMaxCltvR = m_convertLos.ToRate( value ); }
		}
		public E_sLT lLT
		{
			get { return (E_sLT) GetTypeIndexField( "lLT" ); }
			set { SetTypeIndexField( "lLT", (int) value ); }
		}

		public E_sLienPosT lLienPosT
		{
			get { return (E_sLienPosT) GetTypeIndexField( "lLienPosT" ); }
			set { SetTypeIndexField( "lLienPosT" , (int) value );        }
		}

        
        /// <summary>
        ///This rateoption is used to approximately calculate DTI, monthly pmt
        ///The right thing for LPE in the long term is to circulate through all
        ///rate option to narrow down the rate option set, not all rateoptions are 
        ///qualified even if a few are qualified.
        /// </summary>
        /// <param name="sProdRLckdDays"></param>
        /// <param name="sProdPpmtPenaltyMon"></param>
        /// <returns>returning null if it's for non-pml product.</returns>
		
        public IRateItem GetRateOptionKey( int sProdRLckdDays, int sProdPpmtPenaltyMon, int sIOnlyMon ) 
        {

            if (null == m_rateSheetGroupList)
                ConstructRateSheetGroupList();

            foreach (CRateSheetGroup rateSheetGroup in m_rateSheetGroupList) 
            {
                if (rateSheetGroup.IsInRange(sProdRLckdDays, sProdPpmtPenaltyMon, sIOnlyMon)) 
                {
                    IRateItem[] fields = rateSheetGroup.RateSheetFields;

                    IRateItem keyOption = null;

                    for (int i = 0; i < fields.Length; i++) 
                    {
                        if (null == keyOption)
                            keyOption = fields[i];

                        if (fields[i].Point > -1)
                            break;

                        // Assuming that rateoptions are always from most negative fee to least negative fee.
                        keyOption = fields[i];
                    }
                    return keyOption;
                }
            }

            // If Non-LPE broker get here return null if no valid rate option found
                return null;
        }

        public IRateItem GetParRateOption(int sProdRLckdDays, int sProdPpmtPenaltyMon, int sIOnlyMon, decimal adjustPoint) 
        {

            if (null == m_rateSheetGroupList)
                ConstructRateSheetGroupList();

            foreach (CRateSheetGroup rateSheetGroup in m_rateSheetGroupList) 
            {
                if (rateSheetGroup.IsInRange(sProdRLckdDays, sProdPpmtPenaltyMon, sIOnlyMon)) 
                {
                    IRateItem[] fields = rateSheetGroup.RateSheetFields;

                    IRateItem keyOption = null;
                    decimal          selectedFee = decimal.MaxValue;

                    for (int i = 0; i < fields.Length; i++) 
                    {
                        decimal fee = Math.Abs( fields[i].Point + adjustPoint );

                        if( /*null == keyOption ||*/ fee < selectedFee )
                        {
                            keyOption = fields[i];
                            selectedFee = fee;
                        }
                    }
                    return keyOption;
                }
            }

            // If Non-LPE broker get here return null if no valid rate option found. Else throw exception
            if (!this.IsLpe)
                return null;
            else
                throw new CRateOptionNotFoundException(lLpTemplateId, lLpTemplateNm, sProdRLckdDays, sProdPpmtPenaltyMon, sIOnlyMon);
        }


        public decimal lNoteR
		{
			get
			{ 
				DataRowCollection rows = lRateSheetDataSetWithDeltas.Tables[0].Rows;
                // Get the first valid rate.
                for (int i = 0; i < rows.Count; i++) 
                {
                    CRateSheetFieldsReadOnly firstOption = new CRateSheetFieldsReadOnly( this, lRateSheetDataSetWithDeltas, i );
                    if (!firstOption.IsSpecialKeyword)
                        return firstOption.Rate;

                }
				return 0;
			}
		}

		public string lNoteR_rep
		{
			get
			{
				try
				{
					return m_convertLos.ToRateString( lNoteR );
				}
				catch
				{
					return "";
				}
			}
		}

		public decimal lLOrigFPc
		{
			get 
			{ 
				DataRowCollection rows = lRateSheetDataSetWithDeltas.Tables[0].Rows;
				if( rows.Count > 0 )
				{
					CRateSheetFieldsReadOnly firstOption = new CRateSheetFieldsReadOnly( this, lRateSheetDataSetWithDeltas, 0 );
					return firstOption.Point;
				}
				return 0;
			}
		}

		public string lLOrigFPc_rep
		{
			get 
			{ 
				try
				{
					return m_convertLos.ToRateString( lNoteR );
				}
				catch
				{
					return "";
				}
			}
		}

		public decimal lQualR
		{
			get{ return GetRateField( "lQualR" ); }
			set{ SetRateField( "lQualR", value ); }
		}
		public string lQualR_rep
		{
			get{ return GetRateField_rep( "lQualR" ); }
			set{ lQualR = m_convertLos.ToRate( value ); }
		}

		public int lDue
		{
			get 
			{ 
				int due = GetCountField( "lDue" );
				if( due <= 0 )
					return lTerm;
				return due;
			}
			set { SetCountField( "lDue", value ); }
		}
		public string lDue_rep
		{
			get { 
				try
				{
					return m_convertLos.ToCountString( lDue );
				}
				catch{}
				return ""; 
			}
			set { lDue = m_convertLos.ToCount( value ); }
		}

		public decimal lReqTopR
		{
			get { return GetRateField( "lReqTopR" ); }
			set { SetRateField( "lReqTopR", value ); }
		}
		public string lReqTopR_rep
		{
			get { return   GetRateField_rep( "lReqTopR" ); }
			set { lReqTopR = m_convertLos.ToRate( value ); }
		}

		public decimal lReqBotR
		{
			get { return GetRateField( "lReqBotR" ); }
			set { SetRateField( "lReqBotR", value ); }
		}
		public string lReqBotR_rep
		{
			get { return   GetRateField_rep( "lReqBotR" ); }
			set { lReqBotR = m_convertLos.ToRate( value ); }
		}

		public bool lLockedDaysOverrideBit
		{
			get { return GetBoolField( "lLockedDaysOverrideBit" ); }
			set { SetBoolField( "lLockedDaysOverrideBit", value ); }
		}

		public int lLockedDaysInherit
		{
			get { return GetCountField( "lLockedDaysInherit" ); }
			set { SetCountField( "lLockedDaysInherit", value ); }
		}
		public string lLockedDaysInherit_rep
		{
			get { return   GetCountField_rep( "lLockedDaysInherit" ); }
			set { lLockedDaysInherit = m_convertLos.ToCount( value ); }
		}

		public int lLockedDays
		{
			get { return GetCountField( "lLockedDays" ); }
			set { SetCountField( "lLockedDays", value ); }
		}
		public string lLockedDays_rep
		{
            get 
            { 
                try
                { 
                    return m_convertLos.ToCountString( lLockedDays );
                }
                catch{}
                return "";
            }
			set { lLockedDays = m_convertLos.ToCount( value ); }
		}


      	public int lLockedDaysLowerSearchInherit
		{
			get { return GetCountField( "lLockedDaysLowerSearchInherit" ); }
			set { SetCountField( "lLockedDaysLowerSearchInherit", value ); }
		}
		public string lLockedDaysLowerSearchInherit_rep
		{
			get { return   GetCountField_rep( "lLockedDaysLowerSearchInherit" ); }
			set { lLockedDaysLowerSearchInherit = m_convertLos.ToCount( value ); }
		}

		public int lLockedDaysLowerSearch
		{
			get { return GetCountField( "lLockedDaysLowerSearch" ); }
			set { SetCountField( "lLockedDaysLowerSearch", value ); }
		}
		public string lLockedDaysLowerSearch_rep
		{
			get { return   GetCountField_rep( "lLockedDaysLowerSearch" ); }
			set { lLockedDaysLowerSearch = m_convertLos.ToCount( value ); }
		}

        public bool lLpeFeeMinAddRuleAdjInheritBit
        {
            get{ return GetBoolField( "lLpeFeeMinAddRuleAdjInheritBit" ); }
            set{ SetBoolField( "lLpeFeeMinAddRuleAdjInheritBit", value ); }
        }

        /// <summary>
        /// If true, add static setting of max ysp to max ysp adjustment from rules
        /// If false, just use static setting of max ysp
        /// </summary>
        public bool lLpeFeeMinAddRuleAdjBit
        {
            get 
            { 
                if( Guid.Empty == lBaseLpId )
                    return true;
                if( lLpeFeeMinOverrideBit )
                    return false;
                return lLpeFeeMinAddRuleAdjInheritBit;
            }
        }

		public bool lLpeFeeMinOverrideBit
		{
			get{ return GetBoolField( "lLpeFeeMinOverrideBit" ); }
			set{ SetBoolField( "lLpeFeeMinOverrideBit", value ); }
		}

		public decimal lLpeFeeMinInherit
		{
			get{ return GetRateField( "lLpeFeeMinInherit" ); }
			set{ SetRateField( "lLpeFeeMinInherit", value ); }
		}
		public string lLpeFeeMinInherit_rep
		{
			get{ return   GetRateField_rep( "lLpeFeeMinInherit" ); }
			set{ lLpeFeeMinInherit = m_convertLos.ToRate( value ); }
		}

		public decimal lLpeFeeMin
		{
			get{ return GetRateField( "lLpeFeeMin" ); }
			set{ SetRateField( "lLpeFeeMin", value ); }
		}
		public string lLpeFeeMin_rep
		{
			get{ return   GetRateField_rep( "lLpeFeeMin" ); }
			set{ lLpeFeeMin = m_convertLos.ToRate( value ); }
		}

		public decimal lLpeFeeMinParam
		{
			get{ return GetRateField( "lLpeFeeMinParam" ); }
			set{ SetRateField( "lLpeFeeMinParam", value ); }
		}
		public string lLpeFeeMinParam_rep
		{
			get{ return   GetRateField_rep( "lLpeFeeMinParam" ); }
			set{ lLpeFeeMinParam = m_convertLos.ToRate( value ); }
		}

		public bool lLpeFeeMaxOverrideBit
		{
			get{ return GetBoolField( "lLpeFeeMaxOverrideBit" ); }
			set{ SetBoolField( "lLpeFeeMaxOverrideBit", value ); }
		}

		public decimal lLpeFeeMaxInherit
		{
			get{ return GetRateField( "lLpeFeeMaxInherit" ); }
			set{ SetRateField( "lLpeFeeMaxInherit", value ); }
		}
		public string lLpeFeeMaxInherit_rep
		{
			get{ return   GetRateField_rep( "lLpeFeeMaxInherit" ); }
			set{ lLpeFeeMaxInherit = m_convertLos.ToRate( value ); }
		}

		public decimal lLpeFeeMax
		{
			get{ return GetRateField( "lLpeFeeMax" ); }
			set{ SetRateField( "lLpeFeeMax", value ); }
		}
		public string lLpeFeeMax_rep
		{
			get{ return   GetRateField_rep( "lLpeFeeMax" ); }
			set{ lLpeFeeMax = m_convertLos.ToRate( value ); }
		}

		public bool PairingProductIdsOverrideBit
		{
			get{ return GetBoolField( "PairingProductIdsOverrideBit" ); }
			set{ SetBoolField( "PairingProductIdsOverrideBit", value ); }
		}

		public string PairingProductIdsInherit
		{
			get{ return GetLongTextField( "PairingProductIdsInherit" ); }
			set{ SetLongTextField( "PairingProductIdsInherit", value ); }
		}
		public string PairingProductIdsInherit_rep
		{
			get{ return PairingProductIdsInherit; }
			set{ PairingProductIdsInherit= value; }
		}

		public string PairingProductIds
		{
			get{ return GetLongTextField( "PairingProductIds" ); }
			set{ SetLongTextField( "PairingProductIds", value ); }
		}
		public string PairingProductIds_rep
		{
			get{ return PairingProductIds; }
			set{ PairingProductIds= value; }
		}

        public Guid PairIdFor1stLienProdGuid
        {
            get { return GetGuidField( "PairIdFor1stLienProdGuid" ); }
          
            set 
            { 
                // Guid empty when the product is 2nd lien
                SetGuidFieldPreserveEmptyGuid( "PairIdFor1stLienProdGuid", value ); 
            }
        }
		public string PairIdFor1stLienProdGuid_rep
		{
			get { return GetGuidField( "PairIdFor1stLienProdGuid" ).ToString(); }
			set
            { 
                 // Guid empty when the product is 2nd lien
                SetGuidFieldPreserveEmptyGuid( "PairIdFor1stLienProdGuid", new Guid( value )); 
            }
		}

        public Guid PairIdFor1stLienProdGuidInherit
        {
            get { return GetGuidField( "PairIdFor1stLienProdGuidInherit" ); }
            set { SetGuidFieldPreserveEmptyGuid( "PairIdFor1stLienProdGuidInherit", value ); }
        }
		public string PairIdFor1stLienProdGuidInherit_rep
		{
			get { return GetGuidField( "PairIdFor1stLienProdGuidInherit" ).ToString(); }
			set { SetGuidFieldPreserveEmptyGuid( "PairIdFor1stLienProdGuidInherit", new Guid( value )); }
		}

		public decimal lRadj1stCapR
		{
			get{ return GetRateField( "lRadj1stCapR" ); }
			set{ SetRateField( "lRadj1stCapR", value ); }
		}
		public string lRadj1stCapR_rep
		{
			get{ return GetRateField_rep( "lRadj1stCapR" ); }
			set{ lRadj1stCapR = m_convertLos.ToRate( value ); }
		}
		public int lRadj1stCapMon
		{
			get {return this.GetCountField( "lRadj1stCapMon" ); }
			set {this.SetCountField( "lRadj1stCapMon", value ); }
		}
		public string lRadj1stCapMon_rep
		{
			get { return GetCountField_rep( "lRadj1stCapMon" ); }
			set { lRadj1stCapMon = m_convertLos.ToCount( value ); }
		}
		public decimal lRAdjCapR
		{
			get{ return GetRateField( "lRAdjCapR" ); }
			set{ SetRateField( "lRAdjCapR", value ); }
		}
		public string lRAdjCapR_rep
		{
			get{ return GetRateField_rep( "lRAdjCapR" ); }
			set{ lRAdjCapR = m_convertLos.ToRate( value ); }
		}

		public int lRAdjCapMon
		{
			get{ return GetCountField( "lRAdjCapMon" ); }
			set{ SetCountField( "lRAdjCapMon", value ); }
		}
		public string lRAdjCapMon_rep
		{
			get{ return GetCountField_rep( "lRAdjCapMon" ); }
			set{ lRAdjCapMon = m_convertLos.ToCount( value ); }
		}

		public decimal lRAdjLifeCapR
		{
			get{ return GetRateField( "lRAdjLifeCapR" ); }
			set{ SetRateField( "lRAdjLifeCapR", value ); }
		}
		public string lRAdjLifeCapR_rep
		{
			get{ return GetRateField_rep( "lRAdjLifeCapR" ); }
			set{ lRAdjLifeCapR = m_convertLos.ToRate( value ); }
		}
		/// <summary>
		/// Base margin
		/// </summary>
		public decimal lRAdjMarginR
		{
			get{ return GetRateField( "lRAdjMarginR" ); }
			set{ SetRateField( "lRAdjMarginR", value ); }
		}
		public string lRAdjMarginR_rep
		{
			get{ return GetRateField_rep( "lRAdjMarginR" ); }
			set{ lRAdjMarginR = m_convertLos.ToRate( value ); }
		}
		public decimal lRAdjIndexR
		{
			get{ return GetRateField( "lRAdjIndexR" ); }
			set{ SetRateField( "lRAdjIndexR", value ); }
		}
		public string lRAdjIndexR_rep
		{
			get{ return GetRateField_rep( "lRAdjIndexR" ); }
			set{ lRAdjIndexR = m_convertLos.ToRate( value ); }
		}

		public decimal lRAdjFloorR
		{
			get{ return GetRateField( "lRAdjFloorR" ); }
			set{ SetRateField( "lRAdjFloorR", value ); }
		}
		public string lRAdjFloorR_rep
		{
			get{ return GetRateField_rep( "lRAdjFloorR" ); }
			set{ lRAdjFloorR = m_convertLos.ToRate( value ); }
		}

        public E_sRAdjFloorBaseT lRAdjFloorBaseT
        {
            get { return (E_sRAdjFloorBaseT)GetTypeIndexField("lRAdjFloorBaseT"); }
            set { SetTypeIndexField("lRAdjFloorBaseT", (int)value); }
        }

		public E_sRAdjRoundT lRAdjRoundT
		{
			get { return (E_sRAdjRoundT) GetTypeIndexField( "lRAdjRoundT" ); }
			set { SetTypeIndexField( "lRAdjRoundT", (int) value ); }
		}

        public E_sLpDPmtT lLpDPmtT 
        {
            get { return (E_sLpDPmtT)GetTypeIndexField("lLpDPmtT"); }
            set { SetTypeIndexField("lLpDPmtT", (int) value); }
        }

        public E_sLpQPmtT lLpQPmtT 
        {
            get { return (E_sLpQPmtT)GetTypeIndexField("lLpQPmtT"); }
            set { SetTypeIndexField("lLpQPmtT", (int) value); }
        }
        public E_sHelocPmtBaseT lHelocQualPmtBaseT
        {
            get 
            {
                if (lHasQRateInRateOptions)
                {
                    return (E_sHelocPmtBaseT)GetTypeIndexField("lHelocQualPmtBaseT");
                }
                else
                {
                    return lHelocPmtBaseT;
                }
            }
            set { SetTypeIndexField("lHelocQualPmtBaseT", (int)value); }
        }
        public E_sHelocPmtFormulaT lHelocQualPmtFormulaT
        {
            get
            {
                if (lHasQRateInRateOptions)
                {
                    return (E_sHelocPmtFormulaT)GetTypeIndexField("lHelocQualPmtFormulaT");
                }
                else
                {
                    return lHelocPmtFormulaT;
                }
            }
            set { SetTypeIndexField("lHelocQualPmtFormulaT", (int)value); }
        }
        public E_sHelocPmtFormulaRateT lHelocQualPmtFormulaRateT
        {
            get
            {
                if (lHasQRateInRateOptions)
                {
                    return (E_sHelocPmtFormulaRateT)GetTypeIndexField("lHelocQualPmtFormulaRateT");
                }
                else
                {
                    return lHelocPmtFormulaRateT;
                }
            }
            set { SetTypeIndexField("lHelocQualPmtFormulaRateT", (int)value); }
        }
        public E_sHelocPmtAmortTermT lHelocQualPmtAmortTermT
        {
            get
            {
                if (lHasQRateInRateOptions)
                {
                    return (E_sHelocPmtAmortTermT)GetTypeIndexField("lHelocQualPmtAmortTermT");
                }
                else
                {
                    return lHelocPmtAmortTermT;
                }
            }
            set { SetTypeIndexField("lHelocQualPmtAmortTermT", (int)value); }
        }

        public E_sHelocPmtPcBaseT lHelocQualPmtPcBaseT
        {
            get 
            {
                if (lHasQRateInRateOptions)
                {
                    return (E_sHelocPmtPcBaseT)GetTypeIndexField("lHelocQualPmtPcBaseT");
                }
                else
                {
                    return lHelocPmtPcBaseT;
                }
            }
            set { SetTypeIndexField("lHelocQualPmtPcBaseT", (int)value); }
        }
        public decimal lHelocQualPmtMb
        {
            get 
            {
                if (lHasQRateInRateOptions)
                {
                    return GetMoneyField("lHelocQualPmtMb");
                }
                else
                {
                    return lHelocPmtMb;
                }
            }
            set { SetMoneyField("lHelocQualPmtMb", value); }
        }
        public string lHelocQualPmtMb_rep
        {
            get 
            {
                if (lHasQRateInRateOptions)
                {
                    return GetMoneyField_rep("lHelocQualPmtMb");
                }
                else
                {
                    return lHelocPmtMb_rep;
                }
            }
            set { lHelocQualPmtMb = m_convertLos.ToMoney(value); }
        }

        public E_sHelocPmtBaseT lHelocPmtBaseT
        {
            get { return (E_sHelocPmtBaseT)GetTypeIndexField("lHelocPmtBaseT"); }
            set { SetTypeIndexField("lHelocPmtBaseT", (int)value); }
        }
        public E_sHelocPmtFormulaT lHelocPmtFormulaT
        {
            get { return (E_sHelocPmtFormulaT)GetTypeIndexField("lHelocPmtFormulaT"); }
            set { SetTypeIndexField("lHelocPmtFormulaT", (int)value); }
        }
        public E_sHelocPmtFormulaRateT lHelocPmtFormulaRateT
        {
            get { return (E_sHelocPmtFormulaRateT)GetTypeIndexField("lHelocPmtFormulaRateT"); }
            set { SetTypeIndexField("lHelocPmtFormulaRateT", (int)value); }
        }
        public E_sHelocPmtAmortTermT lHelocPmtAmortTermT
        {
            get { return (E_sHelocPmtAmortTermT)GetTypeIndexField("lHelocPmtAmortTermT"); }
            set { SetTypeIndexField("lHelocPmtAmortTermT", (int)value); }
        }
        public E_sHelocPmtPcBaseT lHelocPmtPcBaseT
        {
            get { return (E_sHelocPmtPcBaseT)GetTypeIndexField("lHelocPmtPcBaseT"); }
            set { SetTypeIndexField("lHelocPmtPcBaseT", (int)value); }
        }
        public decimal lHelocPmtMb
        {
            get { return GetMoneyField("lHelocPmtMb"); }
            set { SetMoneyField("lHelocPmtMb", value); }
        }
        public string lHelocPmtMb_rep
        {
            get { return GetMoneyField_rep("lHelocPmtMb"); }
            set { lHelocPmtMb = m_convertLos.ToMoney(value); }
        }
        public int lHelocDraw
        {
            get { return GetCountField("lHelocDraw"); }
            set { SetCountField("lHelocDraw", value); }
        }
        public string lHelocDraw_rep
        {
            get { return GetCountField_rep("lHelocDraw"); }
            set { lHelocDraw = ToCount(value); }
        }

        public bool lHelocCalculatePrepaidInterest
        {
            get { return GetBoolField("lHelocCalculatePrepaidInterest", false);  }
            set { SetNullableBoolField("lHelocCalculatePrepaidInterest", value); }
        }

        public int lHelocRepay
        {
            get { return lDue - lHelocDraw; }
        }
        public string lHelocRepay_rep
        {
            get { return ToCountString(() => lHelocRepay); }
        }

		public decimal lRAdjRoundToR
		{
			get{ return GetRateField( "lRAdjRoundToR" ); }
			set{ SetRateField( "lRAdjRoundToR", value ); }
		}
		public string lRAdjRoundToR_rep
		{
			get{ return GetRateField_rep( "lRAdjRoundToR" ); }
			set{ lRAdjRoundToR = m_convertLos.ToRate( value ); }
		}

		public decimal lPmtAdjCapR
		{
			get{ return GetRateField( "lPmtAdjCapR" ); }
			set{ SetRateField( "lPmtAdjCapR", value ); }
		}
		public string lPmtAdjCapR_rep
		{
			get{ return GetRateField_rep( "lPmtAdjCapR" ); }
			set{ lPmtAdjCapR = m_convertLos.ToRate( value ); }
		}
		public int lPmtAdjCapMon
		{
			get { return this.GetCountField( "lPmtAdjCapMon" ); }
			set { SetCountField( "lPmtAdjCapMon", value ); }
		}
		public string lPmtAdjCapMon_rep
		{
			get { return this.GetCountField_rep( "lPmtAdjCapMon" ); }
			set { lPmtAdjCapMon = m_convertLos.ToCount( value ); }
		}
		public int lPmtAdjRecastPeriodMon
		{
			get { return this.GetCountField( "lPmtAdjRecastPeriodMon" ); }
			set { SetCountField( "lPmtAdjRecastPeriodMon", value ); }
		}
		public string lPmtAdjRecastPeriodMon_rep
		{
			get { return this.GetCountField_rep( "lPmtAdjRecastPeriodMon" ); }
			set { lPmtAdjRecastPeriodMon = m_convertLos.ToCount( value ); }
		}

		public int lPmtAdjRecastStop
		{
			get { return this.GetCountField( "lPmtAdjRecastStop" ); }
			set { SetCountField( "lPmtAdjRecastStop", value ); }
		}
		public string lPmtAdjRecastStop_rep
		{
			get { return this.GetCountField_rep( "lPmtAdjRecastStop" ); }
			set { lPmtAdjRecastStop = m_convertLos.ToCount( value ); }
		}

		public decimal lBuydwnR1
		{
			get{ return GetRateField( "lBuydwnR1" ); }
			set{ SetRateField( "lBuydwnR1", value ); }
		}
		public string lBuydwnR1_rep
		{
			get{ return GetRateField_rep( "lBuydwnR1" ); }
			set{ lBuydwnR1 = m_convertLos.ToRate( value ); }
		}
		public decimal lBuydwnR2
		{
			get{ return GetRateField( "lBuydwnR2" ); }
			set{ SetRateField( "lBuydwnR2", value ); }
		}
		public string lBuydwnR2_rep
		{
			get{ return GetRateField_rep( "lBuydwnR2" ); }
			set{ lBuydwnR2 = m_convertLos.ToRate( value ); }
		}
		public decimal lBuydwnR3
		{
			get{ return GetRateField( "lBuydwnR3" ); }
			set{ SetRateField( "lBuydwnR3", value ); }
		}
		public string lBuydwnR3_rep
		{
			get{ return GetRateField_rep( "lBuydwnR3" ); }
			set{ lBuydwnR3 = m_convertLos.ToRate( value ); }
		}
		public decimal lBuydwnR4
		{
			get{ return GetRateField( "lBuydwnR4" ); }
			set{ SetRateField( "lBuydwnR4", value ); }
		}
		public string lBuydwnR4_rep
		{
			get{ return GetRateField_rep( "lBuydwnR4" ); }
			set{ lBuydwnR4 = m_convertLos.ToRate( value ); }
		}
		public decimal lBuydwnR5
		{
			get{ return GetRateField( "lBuydwnR5" ); }
			set{ SetRateField( "lBuydwnR5", value ); }
		}
		public string lBuydwnR5_rep
		{
			get{ return GetRateField_rep( "lBuydwnR5" ); }
			set{ lBuydwnR5 = m_convertLos.ToRate( value ); }
		}

		public int lBuydwnMon1
		{
			get { return this.GetCountField( "lBuydwnMon1" ); }
			set { SetCountField( "lBuydwnMon1", value ); }
		}
		public string lBuydwnMon1_rep
		{
			get { return this.GetCountField_rep( "lBuydwnMon1" ); }
			set { lBuydwnMon1 = m_convertLos.ToCount( value ); }
		}

		public int lBuydwnMon2
		{
			get { return this.GetCountField( "lBuydwnMon2" ); }
			set { SetCountField( "lBuydwnMon2", value ); }
		}
		public string lBuydwnMon2_rep
		{
			get { return this.GetCountField_rep( "lBuydwnMon2" ); }
			set { lBuydwnMon2 = m_convertLos.ToCount( value ); }
		}

		public int lBuydwnMon3
		{
			get { return this.GetCountField( "lBuydwnMon3" ); }
			set { SetCountField( "lBuydwnMon3", value ); }
		}
		public string lBuydwnMon3_rep
		{
			get { return this.GetCountField_rep( "lBuydwnMon3" ); }
			set { lBuydwnMon3 = m_convertLos.ToCount( value ); }
		}

		public int lBuydwnMon4
		{
			get { return this.GetCountField( "lBuydwnMon4" ); }
			set { SetCountField( "lBuydwnMon4", value ); }
		}
		public string lBuydwnMon4_rep
		{
			get { return this.GetCountField_rep( "lBuydwnMon4" ); }
			set { lBuydwnMon4 = m_convertLos.ToCount( value ); }
		}

		public int lBuydwnMon5
		{
			get { return this.GetCountField( "lBuydwnMon5" ); }
			set { SetCountField( "lBuydwnMon5", value ); }
		}
		public string lBuydwnMon5_rep
		{
			get { return this.GetCountField_rep( "lBuydwnMon5" ); }
			set { lBuydwnMon5 = m_convertLos.ToCount( value ); }
		}

		public int lGradPmtYrs
		{
			get { return this.GetCountField( "lGradPmtYrs" ); }
			set { SetCountField( "lGradPmtYrs", value ); }
		}
		public string lGradPmtYrs_rep
		{
			get { return this.GetCountField_rep( "lGradPmtYrs" ); }
			set { lGradPmtYrs = m_convertLos.ToCount( value ); }
		}

		public decimal lGradPmtR
		{
			get{ return GetRateField( "lGradPmtR" ); }
			set{ SetRateField( "lGradPmtR", value ); }
		}
		public string lGradPmtR_rep
		{
			get{ return GetRateField_rep( "lGradPmtR" ); }
			set{ lGradPmtR = m_convertLos.ToRate( value ); }
		}

		public int lIOnlyMon
		{
			get {return GetCountField( "lIOnlyMon" ); }
			set {this.SetCountField( "lIOnlyMon", value ); }
		}
		public string lIOnlyMon_rep
		{
			get { return GetCountField_rep( "lIOnlyMon" ); }
			set { lIOnlyMon = m_convertLos.ToCount( value ); }
		}

		public int lBMinFico
		{
			get { return this.GetCountField( "lBMinFico" ); }
			set { SetCountField( "lBMinFico", value ); }
		}
		public string lBMinFico_rep
		{
			get { return this.GetCountField_rep( "lBMinFico" ); }
			set { lBMinFico = m_convertLos.ToCount( value ); }
		}
		public int lCMinFico
		{
			get { return this.GetCountField( "lCMinFico" ); }
			set { SetCountField( "lCMinFico", value ); }
		}
		public string lCMinFico_rep
		{
			get { return this.GetCountField_rep( "lCMinFico" ); }
			set { lCMinFico = m_convertLos.ToCount( value ); }
		}
		public bool lHasVarRFeature
		{
			get { return this.GetBoolField( "lHasVarRFeature"); }
			set {this.SetBoolField( "lHasVarRFeature", value ); }
        }
        public bool lLpmiSupportedOutsidePmi
        {
            get { return this.GetBoolField("lLpmiSupportedOutsidePmi"); }
            set { this.SetBoolField("lLpmiSupportedOutsidePmi", value); }
        }
        public bool lLpmiSupportedOutsidePmiOverrideBit
        {
            get { return this.GetBoolField("lLpmiSupportedOutsidePmiOverrideBit"); }
            set { this.SetBoolField("lLpmiSupportedOutsidePmiOverrideBit", value); }
        }
        public bool lLpmiSupportedOutsidePmiInherit
        {
            get { return this.GetBoolField("lLpmiSupportedOutsidePmiInherit"); }
            set { this.SetBoolField("lLpmiSupportedOutsidePmiInherit", value); }
        }
        public bool lWholesaleChannelProgram
        {
            get { return this.GetBoolField("lWholesaleChannelProgram"); }
            set { this.SetBoolField("lWholesaleChannelProgram", value); }
        }
        public bool IsNonQmProgram
        {
            get { return this.GetBoolField("IsNonQmProgram", false); }
            set { this.SetNullableBoolField("IsNonQmProgram", value); }
        }
        public bool IsDisplayInNonQmQuickPricer
        {
            get
            {
                if (!IsNonQmProgram)
                {
                    return false;
                }

                return this.GetBoolField("IsDisplayInNonQmQuickPricer", true);
            }
            set { this.SetNullableBoolField("IsDisplayInNonQmQuickPricer", value); }
        }
        public bool IsDisplayInNonQmQuickPricerOverride
        {
            get
            {
                if (!IsNonQmProgram)
                {
                    return false;
                }

                return this.GetBoolField("IsDisplayInNonQmQuickPricerOverride", false);
            }
            set { this.SetNullableBoolField("IsDisplayInNonQmQuickPricerOverride", value); }
        }

        public bool IsDisplayInNonQmQuickPricerInherit
        {
            get
            {
                if (!IsNonQmProgram)
                {
                    return false;
                }

                return this.GetBoolField("IsDisplayInNonQmQuickPricerInherit", true);
            }
            set { this.SetNullableBoolField("IsDisplayInNonQmQuickPricerInherit", value); }
        }

        public string lVarRNotes
		{
			get	{ return GetStringVarCharField( "lVarRNotes" ); }
			set { SetStringVarCharField( "lVarRNotes", value ); }
		}

		public bool lAprIncludesReqDeposit
		{
			get{ return GetBoolField( "lAprIncludesReqDeposit" ); }
			set{ SetBoolField( "lAprIncludesReqDeposit", value ); }
		}
	
		public bool lHasDemandFeature
		{
			get { return this.GetBoolField( "lHasDemandFeature"); }
			set {this.SetBoolField( "lHasDemandFeature", value ); }
		}
		public string lFilingF
		{
			get { return GetStringCharField( "lFilingF" ); }
			set { SetStringCharField( "lFilingF", value ); }
		}


		public string lLateDays
		{
			get { return GetStringCharField( "lLateDays" ); }
			set { SetStringCharField( "lLateDays", value ); }
		}

		public string lLateChargePc
		{
			get { return GetStringCharField( "lLateChargePc" ); }
			set { SetStringCharField( "lLateChargePc", value ); }
		}

		public E_sPrepmtRefundT lPrepmtRefundT
		{
			get { return (E_sPrepmtRefundT) GetTypeIndexField( "lPrepmtRefundT" ); }
			set { SetTypeIndexField( "lPrepmtRefundT", (int) value ); }
		}
		

		public E_sPrepmtPenaltyT lPrepmtPenaltyT
		{
			get { return (E_sPrepmtPenaltyT) GetTypeIndexField( "lPrepmtPenaltyT" ); }
			set { SetTypeIndexField( "lPrepmtPenaltyT", (int) value ); }
		}

		public bool lPpmtPenaltyMonOverrideBit
		{
			get { return GetBoolField( "lPpmtPenaltyMonOverrideBit" ); }
			set { SetBoolField( "lPpmtPenaltyMonOverrideBit", value ); }
		}

        public bool IsSpreadingNeededForProductData
        {
            get { return GetBoolField("IsSpreadingNeededForProductData"); }
            set { SetBoolField("IsSpreadingNeededForProductData", value); }
        }

        public int lPpmtPenaltyMonInherit
		{
			get { return GetCountField( "lPpmtPenaltyMonInherit" ); }
			set { SetCountField( "lPpmtPenaltyMonInherit", value ); }
		}
		public string lPpmtPenaltyMonInherit_rep
		{
			get { return   GetCountField_rep( "lPpmtPenaltyMonInherit" ); }
			set { lPpmtPenaltyMonInherit = m_convertLos.ToCount( value ); }
		}

		public int lPpmtPenaltyMon
		{
			get { return GetCountField( "lPpmtPenaltyMon" ); }
			set { SetCountField( "lPpmtPenaltyMon", value ); }
		}
		public string lPpmtPenaltyMon_rep
		{
			get { return   GetCountField_rep( "lPpmtPenaltyMon" ); }
			set { lPpmtPenaltyMon = m_convertLos.ToCount( value ); }
		}

		public int lPpmtPenaltyMonLowerSearchInherit
		{
			get { return GetCountField( "lPpmtPenaltyMonLowerSearchInherit" ); }
			set { SetCountField( "lPpmtPenaltyMonLowerSearchInherit", value ); }
		}
		public string lPpmtPenaltyMonLowerSearchInherit_rep
		{
			get { return   GetCountField_rep( "lPpmtPenaltyMonLowerSearchInherit" ); }
			set { lPpmtPenaltyMonLowerSearchInherit = m_convertLos.ToCount( value ); }
		}

        public int lHardPrepmtPeriodMonths
        {
            get { return GetCountField("lHardPrepmtPeriodMonths"); }
            set { SetCountField("lHardPrepmtPeriodMonths", value); }
        }
        public string lHardPrepmtPeriodMonths_rep
        {
            get { return GetCountField_rep("lHardPrepmtPeriodMonths"); }
            set { lHardPrepmtPeriodMonths = m_convertLos.ToCount(value); }
        }

        public int lSoftPrepmtPeriodMonths
        {
            get { return GetCountField("lSoftPrepmtPeriodMonths"); }
            set { SetCountField("lSoftPrepmtPeriodMonths", value); }
        }
        public string lSoftPrepmtPeriodMonths_rep
        {
            get { return GetCountField_rep("lSoftPrepmtPeriodMonths"); }
            set { lSoftPrepmtPeriodMonths = m_convertLos.ToCount(value); }
        }

        public string lPrepmtPeriod_rep
        {
            get { return m_convertLos.ToCountString( lSoftPrepmtPeriodMonths + lHardPrepmtPeriodMonths); } 
        }

		public int lPpmtPenaltyMonLowerSearch
		{
			get { return GetCountField( "lPpmtPenaltyMonLowerSearch" ); }
			set { SetCountField( "lPpmtPenaltyMonLowerSearch", value ); }
		}
		public string lPpmtPenaltyMonLowerSearch_rep
		{
			get { return   GetCountField_rep( "lPpmtPenaltyMonLowerSearch" ); }
			set { lPpmtPenaltyMonLowerSearch = m_convertLos.ToCount( value ); }
		}


		public bool lPpmtPenaltyMonIsAny
		{
			get{ return lPpmtPenaltyMon < 0; }
		}

        public int lIOnlyMonLowerSearch 
        {
            get { return GetCountField("lIOnlyMonLowerSearch"); }
            set { SetCountField("lIOnlyMonLowerSearch", value); }
        }

        public string lIOnlyMonLowerSearch_rep 
        {
            get { return GetCountField_rep("lIOnlyMonLowerSearch"); }
            set { lIOnlyMonLowerSearch = m_convertLos.ToCount(value); }
        }
        public int lIOnlyMonLowerSearchInherit 
        {
            get { return GetCountField("lIOnlyMonLowerSearchInherit"); }
            set { SetCountField("lIOnlyMonLowerSearchInherit", value); }
        }
        public string lIOnlyMonLowerSearchInherit_rep 
        {
            get { return GetCountField_rep("lIOnlyMonLowerSearchInherit"); }
            set { lIOnlyMonLowerSearchInherit = m_convertLos.ToCount(value); }
        }

        public bool lIOnlyMonLowerSearchOverrideBit 
        {
            get { return GetBoolField("lIOnlyMonLowerSearchOverrideBit"); }
            set { SetBoolField("lIOnlyMonLowerSearchOverrideBit", value); }
        }

        public int lIOnlyMonUpperSearch 
        {
            get { return GetCountField("lIOnlyMonUpperSearch"); }
            set { SetCountField("lIOnlyMonUpperSearch", value); }
        }

        public string lIOnlyMonUpperSearch_rep 
        {
            get { return GetCountField_rep("lIOnlyMonUpperSearch"); }
            set { lIOnlyMonUpperSearch = m_convertLos.ToCount(value); }
        }
        public int lIOnlyMonUpperSearchInherit 
        {
            get { return GetCountField("lIOnlyMonUpperSearchInherit"); }
            set { SetCountField("lIOnlyMonUpperSearchInherit", value); }
        }
        public string lIOnlyMonUpperSearchInherit_rep 
        {
            get { return GetCountField_rep("lIOnlyMonUpperSearchInherit"); }
            set { lIOnlyMonUpperSearchInherit = m_convertLos.ToCount(value); }
        }

        public bool lIOnlyMonUpperSearchOverrideBit 
        {
            get { return GetBoolField("lIOnlyMonUpperSearchOverrideBit"); }
            set { SetBoolField("lIOnlyMonUpperSearchOverrideBit", value); }
        }
        public bool lIOnlyMonIsAny 
        {
            get { return lIOnlyMonLowerSearch < 0; }
        }

		public E_sAssumeLT lAssumeLT
		{
			get { return (E_sAssumeLT) GetTypeIndexField( "lAssumeLT" ); }
			set { SetTypeIndexField( "lAssumeLT", (int) value ); }
		}

		public string lLateChargeBaseDesc
		{
			get { return GetStringVarCharField( "lLateChargeBaseDesc" ); }
			set { SetStringVarCharField( "lLateChargeBaseDesc", value ); }
		}

		public decimal lPmtAdjMaxBalPc
		{
			get { return GetRateField( "lPmtAdjMaxBalPc" ); }
			set { SetRateField( "lPmtAdjMaxBalPc", value ); }
		}

		public string lPmtAdjMaxBalPc_rep
		{
			get
			{
				try
				{
					return m_convertLos.ToRateString( lPmtAdjMaxBalPc );
				}
				catch
				{
					return "";
				}
			}
			set { lPmtAdjMaxBalPc = m_convertLos.ToRate( value ); }
		}
		public int lTerm
		{
			get 
			{ 
				int rawVal = GetCountField( "lTerm" );
				return ( rawVal <= 0 ) ? 360 : rawVal;
			}
			set { SetCountField( "lTerm", value ); }
		}
		public string lTerm_rep
		{
			get { 
				try
				{
					return m_convertLos.ToCountString( lTerm );
				}
				catch{}
				return "";
			}
			set { lTerm = m_convertLos.ToCount( value ); }
		}

		public E_sFinMethT lFinMethT
		{
			get { return (E_sFinMethT ) this.GetTypeIndexField( "lFinMethT" ); }
			set { SetTypeIndexField( "lFinMethT", (int) value ); }
		}

		public string lFinMethDesc
		{
			get { return GetStringVarCharField( "lFinMethDesc" ); }
			set { SetStringVarCharField( "lFinMethDesc", value ); }
		}

		// [ Obsolete ]
		public CDateTime lRateSheetEffectiveD
		{
			get { return GetDateTimeField( "lRateSheetEffectiveD" ); }
			set { SetDateTimeField( "lRateSheetEffectiveD", value ); }
		}
		// [ Obsolete ]
		public string lRateSheetEffectiveD_rep
		{
			get
			{
				try
				{
					return GetDateTimeField_rep( "lRateSheetEffectiveD" );
				}
				catch
				{
					return "";
				}
			}
			set { SetDateTimeField_rep( "lRateSheetEffectiveD", value ); }
		}

		// [ Obsolete ]
		public CDateTime lRateSheetExpirationD
		{
			get { return GetDateTimeField( "lRateSheetExpirationD" ); }
			set { SetDateTimeField( "lRateSheetExpirationD", value ); }
		}
		// [ Obsolete ]
		public string lRateSheetExpirationD_rep
		{
			get
			{
				try
				{
					return GetDateTimeField_rep( "lRateSheetExpirationD" );
				}
				catch
				{
					return "";
				}
			}
			set { SetDateTimeField_rep( "lRateSheetExpirationD", value ); }
		}


		#endregion // FIELDS

		#region ARM INDEX
        bool m_islArmIndexGuidModified = false;
		public Guid lArmIndexGuid
		{
			get
			{
				return GetGuidField( "lArmIndexGuid" );
			}
            set
            {
                Guid old = GetGuidField("lArmIndexGuid");

                if (value != old)
                {
                    m_islArmIndexGuidModified = true;
                }
                SetGuidField("lArmIndexGuid", value);
            }
		}

		public string lArmIndexBasedOnVstr
		{
			get
			{
				return GetStringVarCharField( "lArmIndexBasedOnVstr" );
			}
			set
			{
				SetStringVarCharField( "lArmIndexBasedOnVstr", value );
			}
		}

		public string lArmIndexCanBeFoundVstr
		{
			get
			{
				return GetStringVarCharField( "lArmIndexCanBeFoundVstr" );
			}
			set
			{
				SetStringVarCharField( "lArmIndexCanBeFoundVstr", value );
			}
		}

		public bool lArmIndexAffectInitIRBit
		{
			get
			{
				return GetBoolField( "lArmIndexAffectInitIRBit" );
			}
			set
			{
				SetBoolField( "lArmIndexAffectInitIRBit", value );
			}
		}

        public bool IsConvertibleMortgage
        {
            get
            {
                return GetBoolField("IsConvertibleMortgage");
            }
            set
            {
                SetBoolField("IsConvertibleMortgage", value);
            }
        }
        

		public string lArmIndexNotifyAtLeastDaysVstr
		{
			get
			{
				return GetStringVarCharField( "lArmIndexNotifyAtLeastDaysVstr" );
			}
			set
			{
				SetStringVarCharField( "lArmIndexNotifyAtLeastDaysVstr", value );
			}
		}

		public string lArmIndexNotifyNotBeforeDaysVstr
		{
			get
			{
				return GetStringVarCharField( "lArmIndexNotifyNotBeforeDaysVstr" );
			}
			set
			{
				SetStringVarCharField( "lArmIndexNotifyNotBeforeDaysVstr", value );
			}
		}

		public E_sArmIndexT lArmIndexT
		{
			get{ return (E_sArmIndexT) GetTypeIndexField( "lArmIndexT" );}
			set{ SetTypeIndexField( "lArmIndexT", (int) value ); }
		}

		public string lArmIndexNameVstr
		{
			get { return GetStringVarCharField( "lArmIndexNameVstr" ); }
			set { SetStringVarCharField( "lArmIndexNameVstr", value ); }
		}

		public E_sFreddieArmIndexT lFreddieArmIndexT
		{
			get{ return (E_sFreddieArmIndexT) GetTypeIndexField( "lFreddieArmIndexT" );}
			set{ SetTypeIndexField( "lFreddieArmIndexT", (int) value ); }
		}

		[DependsOn()]
		public CDateTime lArmIndexEffectiveD
		{
			get { return GetDateTimeField( "lArmIndexEffectiveD" );  }
			set { SetDateTimeField( "lArmIndexEffectiveD", value ); }
		}
		public string lArmIndexEffectiveD_rep
		{
			get
			{
				try { return GetDateTimeField_rep( "lArmIndexEffectiveD" ); }
				catch{ return ""; }
			}
			set { SetDateTimeField_rep( "lArmIndexEffectiveD", value ); }
		}	
        

		#endregion // ARM Disclosure

        #region Custom Properties OPM 150541
        public string lLpCustomCode1
        {
            get { return GetStringVarCharField("lLpCustomCode1"); }
            set { SetStringVarCharField("lLpCustomCode1", value); }
        }
        public string lLpCustomCode1Inherit
        {
            get { return GetStringVarCharField("lLpCustomCode1Inherit"); }
            set { SetStringVarCharField("lLpCustomCode1Inherit", value); }
        }
        public bool lLpCustomCode1OverrideBit
        {
            get { return GetBoolField("lLpCustomCode1OverrideBit"); }
            set { SetBoolField("lLpCustomCode1OverrideBit", value); }
        }

        public string lLpCustomCode2
        {
            get { return GetStringVarCharField("lLpCustomCode2"); }
            set { SetStringVarCharField("lLpCustomCode2", value); }
        }
        public string lLpCustomCode2Inherit
        {
            get { return GetStringVarCharField("lLpCustomCode2Inherit"); }
            set { SetStringVarCharField("lLpCustomCode2Inherit", value); }
        }
        public bool lLpCustomCode2OverrideBit
        {
            get { return GetBoolField("lLpCustomCode2OverrideBit"); }
            set { SetBoolField("lLpCustomCode2OverrideBit", value); }
        }

        public string lLpCustomCode3
        {
            get { return GetStringVarCharField("lLpCustomCode3"); }
            set { SetStringVarCharField("lLpCustomCode3", value); }
        }
        public string lLpCustomCode3Inherit
        {
            get { return GetStringVarCharField("lLpCustomCode3Inherit"); }
            set { SetStringVarCharField("lLpCustomCode3Inherit", value); }
        }
        public bool lLpCustomCode3OverrideBit
        {
            get { return GetBoolField("lLpCustomCode3OverrideBit"); }
            set { SetBoolField("lLpCustomCode3OverrideBit", value); }
        }

        public string lLpCustomCode4
        {
            get { return GetStringVarCharField("lLpCustomCode4"); }
            set { SetStringVarCharField("lLpCustomCode4", value); }
        }
        public string lLpCustomCode4Inherit
        {
            get { return GetStringVarCharField("lLpCustomCode4Inherit"); }
            set { SetStringVarCharField("lLpCustomCode4Inherit", value); }
        }
        public bool lLpCustomCode4OverrideBit
        {
            get { return GetBoolField("lLpCustomCode4OverrideBit"); }
            set { SetBoolField("lLpCustomCode4OverrideBit", value); }
        }

        public string lLpCustomCode5
        {
            get { return GetStringVarCharField("lLpCustomCode5"); }
            set { SetStringVarCharField("lLpCustomCode5", value); }
        }
        public string lLpCustomCode5Inherit
        {
            get { return GetStringVarCharField("lLpCustomCode5Inherit"); }
            set { SetStringVarCharField("lLpCustomCode5Inherit", value); }
        }
        public bool lLpCustomCode5OverrideBit
        {
            get { return GetBoolField("lLpCustomCode5OverrideBit"); }
            set { SetBoolField("lLpCustomCode5OverrideBit", value); }
        }
        #endregion

        #region Investor Code OPM 246915
        public string lLpInvestorCode1
        {
            get { return GetStringVarCharField("lLpInvestorCode1"); }
            set { SetStringVarCharField("lLpInvestorCode1", value); }
        }
        public string lLpInvestorCode1Inherit
        {
            get { return GetStringVarCharField("lLpInvestorCode1Inherit"); }
            set { SetStringVarCharField("lLpInvestorCode1Inherit", value); }
        }
        public bool lLpInvestorCode1OverrideBit
        {
            get { return GetBoolField("lLpInvestorCode1OverrideBit"); }
            set { SetBoolField("lLpInvestorCode1OverrideBit", value); }
        }

        public string lLpInvestorCode2
        {
            get { return GetStringVarCharField("lLpInvestorCode2"); }
            set { SetStringVarCharField("lLpInvestorCode2", value); }
        }
        public string lLpInvestorCode2Inherit
        {
            get { return GetStringVarCharField("lLpInvestorCode2Inherit"); }
            set { SetStringVarCharField("lLpInvestorCode2Inherit", value); }
        }
        public bool lLpInvestorCode2OverrideBit
        {
            get { return GetBoolField("lLpInvestorCode2OverrideBit"); }
            set { SetBoolField("lLpInvestorCode2OverrideBit", value); }
        }

        public string lLpInvestorCode3
        {
            get { return GetStringVarCharField("lLpInvestorCode3"); }
            set { SetStringVarCharField("lLpInvestorCode3", value); }
        }
        public string lLpInvestorCode3Inherit
        {
            get { return GetStringVarCharField("lLpInvestorCode3Inherit"); }
            set { SetStringVarCharField("lLpInvestorCode3Inherit", value); }
        }
        public bool lLpInvestorCode3OverrideBit
        {
            get { return GetBoolField("lLpInvestorCode3OverrideBit"); }
            set { SetBoolField("lLpInvestorCode3OverrideBit", value); }
        }

        public string lLpInvestorCode4
        {
            get { return GetStringVarCharField("lLpInvestorCode4"); }
            set { SetStringVarCharField("lLpInvestorCode4", value); }
        }
        public string lLpInvestorCode4Inherit
        {
            get { return GetStringVarCharField("lLpInvestorCode4Inherit"); }
            set { SetStringVarCharField("lLpInvestorCode4Inherit", value); }
        }
        public bool lLpInvestorCode4OverrideBit
        {
            get { return GetBoolField("lLpInvestorCode4OverrideBit"); }
            set { SetBoolField("lLpInvestorCode4OverrideBit", value); }
        }

        public string lLpInvestorCode5
        {
            get { return GetStringVarCharField("lLpInvestorCode5"); }
            set { SetStringVarCharField("lLpInvestorCode5", value); }
        }
        public string lLpInvestorCode5Inherit
        {
            get { return GetStringVarCharField("lLpInvestorCode5Inherit"); }
            set { SetStringVarCharField("lLpInvestorCode5Inherit", value); }
        }
        public bool lLpInvestorCode5OverrideBit
        {
            get { return GetBoolField("lLpInvestorCode5OverrideBit"); }
            set { SetBoolField("lLpInvestorCode5OverrideBit", value); }
        }
        #endregion Investor Code OPM 246915
    }

	/// <summary>
	/// Base class for all loan product tree items to facilitate
	/// easy traversing through the graph.
	/// </summary>

	public enum LoanProductType
	{
		Invalid = 0 ,
		Product = 1 ,
		Folder  = 2 ,

	}

	/// <summary>
	/// Whether product or folder, each node has an identifier and
	/// a type.
	/// </summary>

	public class LoanProductNode
	{
		/// <summary>
		/// Whether product or folder, each node has an identifier
		/// and a type.
		/// </summary>

		#region ( Loan product properties )

		public virtual LoanProductType Type
		{
			get { return LoanProductType.Invalid; }
		}

		public virtual String Name
		{
			get { return String.Empty; }
		}

		public virtual Guid Id
		{
			get { return Guid.Empty; }
		}

		public virtual Guid Parent
		{
			get { return Guid.Empty; }
		}

		#endregion

	}

	/// <summary>
	/// Keep track of the embedded loan product.  We keep an in memory
	/// copy in case we want to duplicate or save with changes.
	/// </summary>

	public class LoanProductTemplate : LoanProductNode
	{
		/// <summary>
		/// Each product has a name, id, and a parent.  If the parent
		/// is empty, then we assume that it lies at the root.
		/// </summary>

		protected String       m_Name = String.Empty;
		protected Guid           m_Id = Guid.Empty;
		protected Guid       m_Parent = Guid.Empty;
		protected Guid         m_Base = Guid.Empty;
		protected Boolean  m_IsMaster = false;
		protected Boolean m_IsEnabled = true;
		protected String m_Investor = String.Empty;

		#region ( Loan product properties )

		public IDataReader Contents
		{
			// Access member.

			set
			{
				Object o;

				o = value[ "lLpTemplateId" ];

				if( o != null && o is DBNull == false )
				{
					m_Id = ( Guid ) o;
				}
				else
				{
					m_Id = Guid.Empty;
				}

				o = value[ "lLpTemplateNm" ];

				if( o != null && o is DBNull == false )
				{
					m_Name = ( String ) o;
				}
				else
				{
					m_Name = String.Empty;
				}

				o = value[ "FolderId" ];

				if( o != null && o is DBNull == false )
				{
					m_Parent = ( Guid ) o;
				}
				else
				{
					m_Parent = Guid.Empty;
				}

				o = value[ "lBaseLpId" ];

				if( o != null && o is DBNull == false )
				{
					m_Base = ( Guid ) o;
				}
				else
				{
					m_Base = Guid.Empty;
				}

				o = value[ "IsMaster" ];

				if( o != null && o is DBNull == false )
				{
					m_IsMaster = ( Boolean ) o;
				}
				else
				{
					m_IsMaster = true;
				}

				o = value[ "IsEnabled" ];

				if( o != null && o is DBNull == false )
				{
					m_IsEnabled = ( Boolean ) o;
				}
				else
				{
					m_IsEnabled = true;
				}

				o = value[ "lLpInvestorNm" ];

				if( o != null && o is DBNull == false )
				{
					m_Investor = ( String ) o;
				}
				else
				{
					m_Investor = String.Empty;
				}

			}
		}

		public override LoanProductType Type
		{
			get { return LoanProductType.Product; }
		}

		public override String Name
		{
			get { return m_Name; }
		}

		public override Guid Id
		{
			get { return m_Id; }
		}

		public override Guid Parent
		{
			get { return m_Parent; }
		}

		public Guid Base
		{
			set { m_Base = value; }
			get { return m_Base; }
		}

        public Guid RelatedBase { get; set; } = Guid.Empty;

        public bool IsMaster
		{
			get { return m_IsMaster; }
		}

		public bool IsEnabled
		{
			get { return m_IsEnabled; }
		}

		public string Investor
		{
			get { return m_Investor; }
		}

		#endregion

		public Guid Update
		{
			set { m_Id = value; }
		}
	}

	/// <summary>
	/// Store other folders and products in this composite tree node.
	/// </summary>

	public class LoanProductContainer : LoanProductNode
	{
		/// <summary>
		/// Keep track of all added children.  Each child can be a
		/// folder or product.  By storing other folders within, we
		/// implement the composite pattern.
		/// </summary>

		protected ArrayList m_Set = new ArrayList();

		/// <summary>
		/// Keep track of basic folder details.
		/// </summary>

		protected String m_Name = String.Empty;
		protected Guid     m_Id = Guid.Empty;
		protected Guid m_Parent = Guid.Empty;

		#region ( Loan product properties )

		public IDataReader Contents
		{
			// Access member.

			set
			{
				Object o;

				o = value[ "FolderId" ];

				if( o != null && o is DBNull == false )
				{
					m_Id = ( Guid ) o;
				}
				else
				{
					m_Id = Guid.Empty;
				}

				o = value[ "FolderName" ];

				if( o != null && o is DBNull == false )
				{
					m_Name = ( String ) o;
				}
				else
				{
					m_Name = String.Empty;
				}

				o = value[ "ParentFolderId" ];

				if( o != null && o is DBNull == false )
				{
					m_Parent = ( Guid ) o;
				}
				else
				{
					m_Parent = Guid.Empty;
				}
			}
		}

		public override LoanProductType Type
		{
			// Access member.

			get
			{
				return LoanProductType.Folder;
			}
		}

		public override String Name
		{
			// Access member.

			get
			{
				return m_Name;
			}
		}

		public override Guid Id
		{
			// Access member.

			get
			{
				return m_Id;
			}
		}

		public override Guid Parent
		{
			// Access member.

			get
			{
				return m_Parent;
			}
		}

		#endregion

		public Guid Update
		{
			set { m_Id = value; }
		}

		/// <summary>
		/// Add new template to this folder's set of loan product items.
		/// </summary>

		public void Add( LoanProductTemplate tChild )
		{
			foreach( LoanProductNode lNode in m_Set )
			{
				if( lNode.Type == LoanProductType.Product && lNode.Id == tChild.Id )
				{
					return;
				}
			}

			m_Set.Add( tChild );
		}

		/// <summary>
		/// Add new folder to this folder's set of loan product items.
		/// </summary>

		public void Add( LoanProductContainer fChild )
		{
			foreach( LoanProductNode lNode in m_Set )
			{
				if( lNode.Type == LoanProductType.Folder && lNode.Id == fChild.Id )
				{
					return;
				}
			}

			m_Set.Add( fChild );
		}

		/// <summary>
		/// Return our embedded set's looping interface for walking.
		/// </summary>

		public IEnumerator GetEnumerator()
		{
			// Return the looping interface for for-walking.

			return m_Set.GetEnumerator();
		}

	}

	/// <summary>
	/// Track all loan product entries for a single broker.  This class
	/// is good for loading and binding to the ui for tree rendering.
	/// </summary>

	public class LoanProductSet : LoanProductContainer
	{
		/// <summary>
		/// Provide lookup table for collecting all contained folders
		/// and products retrieved for this set.
		/// </summary>

		private Hashtable m_All = new Hashtable();
        private Hashtable hashNodes = new Hashtable();

        #region ( Loan product set properties )

        public DynaTreeNode rootNode = new DynaTreeNode();

		public LoanProductNode this[ Guid nodeId ]
		{
			// Access member.

			get
			{
				return m_All[ nodeId ] as LoanProductNode;
			}
		}

		#endregion

		private Int32 Shadow( LoanProductContainer lpC , Boolean isLpe , Guid targetFolderId , Guid brokerId , ArrayList programs , CStoredProcedureExec spExec, bool becomeIndependent)
		{
			// Create a duplicate folder structure in the target by recursively
			// descending into the give tree.

			Int32 nXfer = 0;

			try
			{
				SqlParameter folderIdParam = new SqlParameter( "@FolderId"       , Guid.Empty );
				SqlParameter parentIdParam = new SqlParameter( "@ParentFolderId" , Guid.Empty );

				folderIdParam.Direction = ParameterDirection.Output;

				if( targetFolderId != Guid.Empty )
				{
					parentIdParam.Value = targetFolderId;
				}
				else
				{
					parentIdParam.Value = DBNull.Value;
				}

				spExec.ExecuteNonQuery
					( "CreateLoanProductFolder"
					, new SqlParameter( "@FolderName" , lpC.Name       )
					, new SqlParameter( "@IsLpe"      , isLpe          )
					, new SqlParameter( "@BrokerId"   , brokerId       )
					, parentIdParam
					, folderIdParam
					);

				lpC.Update = ( Guid ) folderIdParam.Value;

				foreach( LoanProductNode lpN in lpC )
				{
					if( lpN.Type == LoanProductType.Folder )
					{
						nXfer += Shadow
							( lpN as LoanProductContainer
							, isLpe
							, lpC.Id
							, brokerId
							, programs
							, spExec
                            , becomeIndependent
                            );
					}

					if( lpN.Type == LoanProductType.Product )
					{
						nXfer += Shadow
							( lpN as LoanProductTemplate
							, isLpe
							, lpC.Id
							, brokerId
							, programs
							, spExec
                            , becomeIndependent
                            );
					}
				}
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( e );

				throw;
			}

			return nXfer;
		}

		private Int32 Shadow( LoanProductTemplate lpT , Boolean isLpe , Guid targetFolderId , Guid brokerId , ArrayList programs , CStoredProcedureExec spExec, bool becomeIndependent)
		{
			// Create a duplicate that inherits everything from the source
			// as its base.  This is how we derive a product.

			Int32 nXfer = 0;

			try
			{
				if( lpT.IsMaster == false )
				{
					CLoanProductData data = new CLoanProductData( lpT.Id );
                    Guid baseId = becomeIndependent ? Guid.Empty : lpT.Id;
                    Guid relatedBase = lpT.Id;


                    data.InitLoad( spExec );

					if( data.lLpTemplateId == lpT.Id )
					{
						//OPM 17145 Cord
						CLoanProductData copy = CLoanProductData.Create( data.lLienPosT , isLpe , false , baseId , targetFolderId , brokerId , data.SrcRateOptionsProgId, lpT.Investor, data.ProductCode, spExec );
                        //CLoanProductData copy = CLoanProductData.Create( data.lLienPosT , isLpe , false , baseId , targetFolderId , brokerId , data.SrcRateOptionsProgId, lpT.Investor, spExec );

                        lpT.Update = copy.lLpTemplateId;
						lpT.Base   = copy.lBaseLpId;


                        copy.CopyAllValues( data );

						copy.ChangeAllFields();
						copy.InitInherits();
						copy.InitDerived();

                        #region OPM 1979
                        if (data.lRateOptionBaseId.TrimWhitespaceAndBOM() != "") 
                        {
                            if (data.lBaseLpId != Guid.Empty) 
                            {
                                // Source product is a derived product.
                                copy.lRateOptionBaseId = "";
                            } 
                            else 
                            {
                                // Source product is not a derived product.
                                copy.lRateSheetXmlContentOverrideBit = true;
                            }
                        }
                        #endregion

                        if (becomeIndependent)
                        {
                            lpT.RelatedBase = relatedBase;
                            copy.lRateSheetXmlContentOverrideBit = true;
                            copy.SetRateSheetXmlContent("");
                        }

                        copy.Save( spExec );

                        // OPM 17199 mf - We need the ID of the new program
                        programs.Add( copy.lLpTemplateId );

						++nXfer;
					}
				}
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( e );

				throw;
			}

			return nXfer;
		}

		/// <summary>
		/// Copy the source items into the specified broker.  We don't spread
		/// within the shared transaction -- it's not required to have a
		/// successful derive, though we will look empty in a lot of key fields
		/// without it.
		/// </summary>

        // expect : the connection of spExec is  Lpe connnection.

        // Will rename back to Derive in next version
		public void Derive_CalledFromInsertExternalProduct( LoanProductNode lpN , Boolean isLpe , Guid targetFolderId , Guid brokerId , ArrayList programs , CStoredProcedureExec spExec,
                                                            bool becomeIndependent=false)
		{
			// Copy the source items into the specified broker.  We don't
			// spread within the shared transaction -- it's not required
			// to have a successful derive, though we will look empty in
			// a lot of key fields without it.

			Int32 nXfer = 0;

			if( lpN.Type == LoanProductType.Folder )
			{
				nXfer += Shadow
					( lpN as LoanProductContainer
					, isLpe
					, targetFolderId
					, brokerId
					, programs
					, spExec
                    , becomeIndependent
                    );
			}

			if( lpN.Type == LoanProductType.Product )
			{
				nXfer += Shadow
					( lpN as LoanProductTemplate
					, isLpe
					, targetFolderId
					, brokerId
					, programs
					, spExec
                    , becomeIndependent
                    );
			}

			if( nXfer == 0 )
			{
				throw new CBaseException( "Nothing derived.  Remember: master products are ignored.", "Nothing derived.  Remember: master products are ignored." );
			}
		}

		/// <summary>
		/// Now, spread the changes.  We first spread creation events, then
		/// the corresponding modify events.  We can check for masters, but
		/// masters aren't xfered.
		/// </summary>

		public void Spread( LoanProductNode lpN , Guid brokerId, bool becomeIndependent)
		{
			// Now, spread the changes.  We first spread creation events,
			// then the corresponding modify events.  We can check for
			// masters, but masters aren't xfered.
			//
			// We spread from the top down.

			if( lpN.Type == LoanProductType.Folder )
			{
				LoanProductContainer lpC = lpN as LoanProductContainer;

				foreach( LoanProductNode lpK in lpC )
				{
					Spread( lpK , brokerId, becomeIndependent);
				}
			}

			if( lpN.Type == LoanProductType.Product )
			{
				LoanProductTemplate lpT = lpN as LoanProductTemplate;

				try
				{
					if( lpT.IsMaster == false )
					{
						Toolbox.LoanProgram.EventHandlers.AfterCreationForThisNonmasterProd( lpT.Id , brokerId );

						if( lpT.Base != Guid.Empty)
						{
							Toolbox.LoanProgram.EventHandlers.AfterDerivationForThisNonmasterProd( lpT.Base , brokerId );
						}
						else if (lpT.RelatedBase != Guid.Empty)
                        {
                            Toolbox.LoanProgram.EventHandlers.IndependentAfterDerivationForThisNonmasterProd(lpT.Id, lpT.RelatedBase, brokerId);
                        }
                        else
                        {
							Toolbox.LoanProgram.EventHandlers.AfterModifForThisNonmasterProd( lpT.Id , brokerId );
						}
					}
					else
					{
                        if( becomeIndependent)
                        {
                            throw new CBaseException(ErrorMessages.Generic, "Don't support master product with option 'becomeIndependent=true'");
                        }

						Toolbox.LoanProgram.EventHandlers.AfterCreationOfThisMaster( lpT.Id , brokerId );

						Toolbox.LoanProgram.EventHandlers.AfterModifOfThisMaster( lpT.Id , brokerId );
					}
				}
				catch( Exception e )
				{
					// Oops!

					Tools.LogErrorWithCriticalTracking( String.Format
						( "Failed to spread for loan product {0} :: {1}."
						, lpN.Name
						, lpN.Id
						)
						, e
						);
				}
			}
		}

		/// <summary>
		/// Load all loan products and folders for a given broker.
		/// </summary>

		public void Retrieve( Guid brokerId , Boolean isLpe, bool folderOnly = false, Func<LoanProductTemplate,bool> acceptFilter = null )
		{
			// Get the broker's loan product tree in one shot.  We order
			// it within the tree as we receive it.

			Boolean allWell = true;

			using( IDataReader sR = StoredProcedureHelper.ExecuteReader( DataSrc.LpeSrc, "ListLoanProductFolderByBrokerID" , new SqlParameter( "@BrokerId" , brokerId ) , new SqlParameter( "@IsLpe" , isLpe ) ) )
			{
				while( sR.Read() == true )
				{
					try
					{
						LoanProductContainer parentFolder = new LoanProductContainer();

						parentFolder.Contents = sR;

						m_All.Add( parentFolder.Id , parentFolder );                        
					}
					catch( Exception e )
					{
						// Oops!
                        Tools.LogError("Exception parsing results of 'ListLoanProductFolderByBrokerID' stored procedure.", e);

						allWell = false;
					}
				}
			}

			foreach( LoanProductContainer loanProductContainer in m_All.Values )
			{
				try
				{
					if( loanProductContainer.Parent != Guid.Empty )
					{
                        // node with a parent => child node.
                        LoanProductContainer parentProductContainer = m_All[ loanProductContainer.Parent ] as LoanProductContainer;
                        if (parentProductContainer == null && loanProductContainer != null)
                            throw new CBaseException(ErrorMessages.FailedToLoad, string.Format("Folder {0} ({1}) has a parentId ({2}) not found for this broker.", loanProductContainer.Name, loanProductContainer.Id, loanProductContainer.Parent));
						parentProductContainer.Add( loanProductContainer );

                        DynaTreeNode parentNode = GetOrCreateDynaTreeNode(parentProductContainer);                        
                        DynaTreeNode childNode = GetOrCreateDynaTreeNode(loanProductContainer);

                        parentNode.children.Add(childNode);

                    }
					else
					{
                        // parent node does not exist => child of the root node
                        Add( loanProductContainer );
                        DynaTreeNode node = GetOrCreateDynaTreeNode(loanProductContainer);
                        this.rootNode.children.Add(node);
					}
				}
				catch( Exception e )
				{
					// Oops!
                    Tools.LogError("Exception trying to find a folder's parent.", e);

					allWell = false;
				}
			}

            if (!folderOnly)
            {
                using (IDataReader sR = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "ListLoanProductByBrokerID", new SqlParameter("@BrokerId", brokerId), new SqlParameter("@IsLpe", isLpe)))
                {
                    while (sR.Read() == true)
                    {
                        try
                        {
                            LoanProductTemplate loanProductTemplate = new LoanProductTemplate();

                            loanProductTemplate.Contents = sR;

                            if (loanProductTemplate.IsMaster == true || (acceptFilter != null && !acceptFilter(loanProductTemplate) ) )
                            {
                                continue;
                            }

                            if (loanProductTemplate.Parent != Guid.Empty)
                            {
                                LoanProductContainer parentProductContainer = m_All[loanProductTemplate.Parent] as LoanProductContainer;
                                if (parentProductContainer == null && loanProductTemplate != null)
                                    throw new CBaseException(ErrorMessages.FailedToLoad, string.Format("Loan Product {0} ({1}) belongs to the folderId {2} not found for this broker.", loanProductTemplate.Name, loanProductTemplate.Id, loanProductTemplate.Parent));
                                parentProductContainer.Add(loanProductTemplate);

                                DynaTreeNode parentNode = GetOrCreateDynaTreeNode(parentProductContainer);
                                DynaTreeNode childNode = GetOrCreateDynaTreeNode(loanProductTemplate);

                                parentNode.children.Add(childNode);
                            }
                            else
                            {
                                Add(loanProductTemplate);

                                DynaTreeNode node = GetOrCreateDynaTreeNode(loanProductTemplate);
                                this.rootNode.children.Add(node);
                            }

                            m_All.Add(loanProductTemplate.Id, loanProductTemplate);
                        }
                        catch (Exception e)
                        {
                            // Oops!
                            Tools.LogError("Exception parsing results of 'ListLoanProductByBrokerID' stored procedure.", e);

                            allWell = false;
                        }
                    }
                }
            }

			if( allWell == false )
			{
				throw new CBaseException(ErrorMessages.FailedToLoad, "Unable to completely load all folders and products for broker " + brokerId + ".");
			}
		}

        private DynaTreeNode GetOrCreateDynaTreeNode(LoanProductContainer container)
        {
            DynaTreeNode node = (DynaTreeNode)hashNodes[container.Id];
            if (node != null)
            {
                return node;
            }
            else
            {
                node = new DynaTreeNode();
                node.title = container.Name;
                node.key = container.Id.ToString();
                node.isFolder = container.Type == LoanProductType.Folder;
                hashNodes[container.Id] = node;

                return node;
            }
        }

        private DynaTreeNode GetOrCreateDynaTreeNode(LoanProductTemplate template)
        {
            DynaTreeNode node = (DynaTreeNode)hashNodes[template.Id];
            if (node != null)
            {
                return node;
            }
            else
            {
                node = new DynaTreeNode();
                node.title = template.Name;
                node.key = template.Id.ToString();
                node.isFolder = template.Type == LoanProductType.Folder;
                hashNodes[template.Id] = node;

                return node;
            }
        }        
	}
}
