using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
	public class CSubmitFirstLoanData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CSubmitFirstLoanData()
	    {
	        StringList list = new StringList();

	        #region Target Fields
            DataAccessUtils.AddCertificateFields(list);

            list.Add("sEmployeeLockDesk");
            list.Add("sEmployeeUnderwriter");
            list.Add("sLpTemplateId");
            list.Add("sNoteIRSubmitted");
            list.Add("sPmlCertXmlContent");
            list.Add("sProThisMPmt");
            list.Add("sfApplyLoanProductTemplate");
            list.Add("sfConfirm1stLienLoanTentativelyAccepted");
            list.Add("sfSpinOffLoanForSubsequent2ndLienLoan");
            list.Add("sNoteIR");
            list.Add("sQualBottomR");
            list.Add("sLpeRateOptionIdOf1stLienIn8020");
            list.Add( "sfRunLpe" );
            list.Add("sPml1stSubmitD");
            list.Add("sLinkedLoanInfo");
            list.Add("sEmployeeProcessor");
            list.Add("lpeRunModeT");
            list.Add("sBranchId");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CSubmitFirstLoanData(Guid fileId) : base(fileId, "CSubmitFirstLoanData", s_selectProvider)
		{
		}
        protected override bool m_enforceAccessControl
        {
            get { return false; }
        }

	}
}
