﻿// <summary>
// <copyright file="DeterminationRuleSet.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Eduardo Michel
//  Date:   9/17/2015
// </summary>

namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Xml.Serialization;
    using LendersOffice.Common;

    /// <summary>
    /// This class is a container for the Determination Rules that are used in the PML Certificate.  Use this to determination if the XML is valid, or to retrieve the rules that match
    /// the given conditions.
    /// </summary>
    public class DeterminationRuleSet
    {
        /// <summary>
        /// The list of Determination Rules.
        /// </summary>
        private List<DeterminationRule> rules = new List<DeterminationRule>();

        /// <summary>
        /// Initializes a new instance of the DeterminationRuleSet class.
        /// </summary>
        /// <param name="xmlContent">The xml to create the rules from.  Aside from checking if it's empty, it will not be validated to determine if the rules account for all
        /// possible conditions.</param>
        public DeterminationRuleSet(string xmlContent)
        {
            if (!string.IsNullOrEmpty(xmlContent))
            {
                this.rules = (List<DataAccess.DeterminationRule>)SerializationHelper.XmlDeserialize(xmlContent, typeof(List<DeterminationRule>));
            }
        }

        /// <summary>
        /// Gets a value indicating whether the Determination Rules set contains any rules.
        /// </summary>
        /// <value>A bool determining whether this set contains any rules at all.</value>
        public bool HasRules
        {
            get
            {
                return this.rules.Count() > 0;
            }
        }

        /// <summary>
        /// Returns a bool determinating whether the given xml is valid.  To be valid, a rule must exist for every possible combination of conditions, 
        /// and no more than 1 rule may affect that combination.
        /// </summary>
        /// <param name="xmlContent">The xml to be validated.</param>
        /// <param name="errorMessage">A list of errors detailing why the XML isn't valid.</param>
        /// <returns>A bool determining whether the xml is valid.</returns>
        public static bool ValidateDeterminationRulesXMLContent(string xmlContent, out string errorMessage)
        {
            StringBuilder errorList = new StringBuilder();
            try
            {
                List<DataAccess.DeterminationRule> rules = (List<DataAccess.DeterminationRule>)SerializationHelper.XmlDeserialize(xmlContent, typeof(List<DataAccess.DeterminationRule>));

                foreach (DeterminationRule rule in rules)
                {
                    if (
                        rule.Conditions.SubmissionType.Exists(p => p.End.HasValue && p.Start > p.End)
                        || rule.Conditions.Channel.Exists(p => p.End.HasValue && p.Start > p.End)
                        || rule.Conditions.LoanStatus.Exists(p => p.End.HasValue && p.Start > p.End))
                    {
                        errorList.AppendLine("The end value is greater than the beginning value for one of the conditions for rule: '" + rule.Id + "'");
                    }
                }

                if (errorList.Length <= 0)
                {
                    for (int submissionType = 0; submissionType < 2; submissionType++)
                    {
                        foreach (E_BranchChannelT branchChannelT in Enum.GetValues(typeof(E_BranchChannelT)))
                        {
                            foreach (E_sStatusT statusT in Enum.GetValues(typeof(E_sStatusT)))
                            {
                                var matches = rules.FindAll(p => p.ShouldApply(submissionType, branchChannelT, statusT));

                                if (matches.Count <= 0)
                                {
                                    errorList.AppendLine("This is missing:  submissionT - " + submissionType + ", statusT - " + (int)statusT + ", branchChannelT - " + (int)branchChannelT + "<br><br>");
                                }

                                if (matches.Count > 1)
                                {
                                    errorList.Append("The following scenario has overlapping conditions: submissionT - " + submissionType + ", statusT - " + (int)statusT + ", branchChannelT - " + (int)branchChannelT + "<br>");
                                    errorList.Append("The ids for these overlapping rules are:");

                                    foreach (DeterminationRule rule in matches)
                                    {
                                        errorList.AppendLine("<br> '" + rule.Id + "'");
                                    }

                                    errorList.AppendLine("<br><br>");
                                }
                            }
                        }
                    }
                }
            }
            catch (GenericUserErrorMessageException exc)
            {
                errorList.AppendLine("The XML for \"PML certificate determination configuration\" could not be parsed correctly.  Please fix the XML.  for more info, check the PB logs.");
                Tools.LogError(exc);
            }

            if (errorList.Length > 0)
            {
                errorMessage = errorList.ToString();
                return false;
            }
            else
            {
                errorMessage = string.Empty;
                return true;
            }
        }

        /// <summary>
        /// The list of rules that meet conditions given.
        /// </summary>
        /// <param name="isRateLockRequested">A bool determining whether the lock is requested.</param>
        /// <param name="branchChannelT">The loan's branch channel.</param>
        /// <param name="statusT">The loan's loan status.</param>
        /// <returns>Returns a list of rules that meet the conditions given.</returns>
        public IEnumerable<DeterminationRule> QualifyingRules(bool isRateLockRequested, E_BranchChannelT branchChannelT, E_sStatusT statusT)
        {
            return this.rules.Where(p => p.ShouldApply(isRateLockRequested, branchChannelT, statusT));
        }
    }
}