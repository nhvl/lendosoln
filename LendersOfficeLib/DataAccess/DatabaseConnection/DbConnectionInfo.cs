﻿// <copyright file="DbConnectionInfo.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   7/7/2014 10:53:24 PM 
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Runtime.Caching;
    using LendersOffice.Common;
    using LendersOffice.Drivers.ConnectionStringDecryption;
    using LendersOffice.ObjLib.Profiling;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Contains necessary information to build the connection string.
    /// </summary>
    public class DbConnectionInfo
    {
        /// <summary>
        /// Cache for DbConnectionInfo by BrokerId.
        /// Or cache for BrokerId by LoanId.
        /// The reason a single MemoryCache object is used instead of 2 separate one is each MemoryCache
        /// has some overhead.
        /// </summary>
        private static MemoryCache internalCache = new MemoryCache("DbConnectionInfoCache");

        /// <summary>
        /// Dictionary of all the available connection info.
        /// </summary>
        private static TimeBasedCacheObject<Dictionary<Guid, DbConnectionInfo>> connectionInfoDictionary;

        /// <summary>
        /// Dictionary of broker and connection info.
        /// </summary>
        private static Dictionary<Guid, Guid> brokerConnectionInfoDictionary;

        /// <summary>
        /// Initializes static members of the <see cref="DbConnectionInfo" /> class.
        /// </summary>
        static DbConnectionInfo()
        {
            connectionInfoDictionary = new TimeBasedCacheObject<Dictionary<Guid, DbConnectionInfo>>(new TimeSpan(1, 0, 0), DbConnectionStringManager.GetAllSplitDatabaseConnectionStrings);
            brokerConnectionInfoDictionary = new Dictionary<Guid, Guid>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DbConnectionInfo" /> class.
        /// </summary>
        /// <param name="id">Unique identifier of the connection.</param>
        /// <param name="code">Unique code of the connection.</param>
        /// <param name="description">Description of the connection.</param>
        /// <param name="dataSource">Name or network address of the instance of SQL Server to connect to.</param>
        /// <param name="initialCatalog">Name of the database associated with the connection.</param>
        /// <param name="failoverPartner">The name or address of the partner server to connect to if the primary server is down.</param>
        /// <param name="readOnlyEncryptedUserName">User name for the readonly account.</param>
        /// <param name="readOnlyEncryptedPassword">Password for the readonly account.</param>
        /// <param name="fullAccessEncryptedUserName">User name for the full access account.</param>
        /// <param name="fullAccessEncryptedPassword">Password for the full access account.</param>
        /// <param name="connectionType">The type of connection.</param>
        /// <param name="useDefaultEncryptionKey">Whether or not to use default encryption.</param>
        internal DbConnectionInfo(
            Guid id,
            string code,
            string description,
            string dataSource,
            string initialCatalog,
            string failoverPartner,
            string readOnlyEncryptedUserName,
            string readOnlyEncryptedPassword,
            string fullAccessEncryptedUserName,
            string fullAccessEncryptedPassword,
            string connectionType,
            bool useDefaultEncryptionKey = false)
        {
            this.Id = id;
            this.Code = code;
            this.Description = description;
            this.DataSource = dataSource;
            this.InitialCatalog = initialCatalog;
            this.FailoverPartner = failoverPartner;
            this.ConnectionType = connectionType;

            this.ReadOnlyConnectionString = this.BuildConnectionString(readOnlyEncryptedUserName, readOnlyEncryptedPassword, useDefaultEncryptionKey);
            this.FullAccessConnectionString = this.BuildConnectionString(fullAccessEncryptedUserName, fullAccessEncryptedPassword, useDefaultEncryptionKey);
        }

        /// <summary>
        /// Gets the default loan database.
        /// </summary>
        /// <value>The default loan database.</value>
        public static DbConnectionInfo DefaultConnectionInfo
        {
            get
            {
                foreach (var o in ListAll())
                {
                    if (o.Code == string.Empty)
                    {
                        return o;
                    }
                }

                throw new NotFoundException("Could not locate default DbConnectionInfo");
            }
        }

        /// <summary>
        /// Gets the unique identifier of the connection.
        /// </summary>
        /// <value>The unique identifier of the connection.</value>
        public Guid Id { get; private set; }

        /// <summary>
        /// Gets the unique code of the connection.
        /// </summary>
        /// <value>The unique code of the connection.</value>
        public string Code { get; private set; }

        /// <summary>
        /// Gets the friendly description of the connection.
        /// </summary>
        /// <value>The friendly description of the connection.</value>
        public string Description { get; private set; }

        /// <summary>
        /// Gets the name or network address of the instance of SQL Server to connect to.
        /// </summary>
        /// <value>The name or network address of the instance of SQL Server to connect to.</value>
        public string DataSource { get; private set; }

        /// <summary>
        /// Gets the name of the database associated with the connection.
        /// </summary>
        /// <value>The name of the database associated with the connection.</value>
        public string InitialCatalog { get; private set; }

        /// <summary>
        /// Gets the name or address of the partner server to connect to if the primary server is down.
        /// </summary>
        /// <value>The name or address of the partner server to connect to if the primary server is down.</value>
        public string FailoverPartner { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this is a split database connection.
        /// </summary>
        public bool IsSplitDatabaseConnection => this.ConnectionType == "Split";
        
        /// <summary>
        /// Gets the friendly description of the connection.
        /// </summary>
        /// <value>Friendly description of the connection.</value>
        public string FriendlyDisplay
        {
            get { return "[" + this.Code + "] " + this.Description + " (" + this.DataSource + "; " + this.InitialCatalog + ")"; }
        }

        /// <summary>
        /// Gets the readonly connection string.
        /// </summary>
        protected string ReadOnlyConnectionString { get; private set; }

        /// <summary>
        /// Gets the full access connection string.
        /// </summary>
        protected string FullAccessConnectionString { get; private set; }

        /// <summary>
        /// Gets or sets the type of the connection.
        /// </summary>
        /// <value>The type of connection.</value>
        private string ConnectionType { get; set; }

        /// <summary>
        /// Gets the read-only connection that store the broker data.
        /// </summary>
        /// <param name="brokerId">Id of the broker.</param>
        /// <param name="shouldProfile">True if the connection should profile, false otherwise.</param>
        /// <returns>Read-only sql connection.</returns>
        public static DbConnection GetReadOnlyConnection(Guid brokerId, bool shouldProfile = true)
        {
            DbConnectionInfo connectionInfo = GetConnectionInfo(brokerId);

            return connectionInfo.GetReadOnlyConnection(shouldProfile);
        }

        /// <summary>
        /// Gets the regular connection that store the broker data.
        /// </summary>
        /// <param name="brokerId">Id of the broker.</param>
        /// <param name="shouldProfile">True if the connection should profile, false otherwise.</param>
        /// <returns>Regular sql connection.</returns>
        public static DbConnection GetConnection(Guid brokerId, bool shouldProfile = true)
        {
            DbConnectionInfo connectionInfo = GetConnectionInfo(brokerId);

            return connectionInfo.GetConnection(shouldProfile);
        }

        /// <summary>
        /// Get connection info by broker id.
        /// </summary>
        /// <param name="brokerId">Id of the broker.</param>
        /// <returns>The connection info.</returns>
        public static DbConnectionInfo GetConnectionInfo(Guid brokerId)
        {
            if (brokerId == Guid.Empty)
            {
                Tools.LogWarning("Should not pass Empty guid for broker id. " + Environment.NewLine + Environment.StackTrace);
            }

            string key = brokerId.ToString();

            DbConnectionInfo connectionInfo = internalCache.Get(key) as DbConnectionInfo;

            if (connectionInfo == null)
            {
                Guid connectionId = Guid.Empty;

                SqlParameter[] parameters =
                {
                        new SqlParameter("@BrokerId", brokerId)
                };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "DB_CONNECTION_RetrieveByBrokerId", parameters))
                {
                    if (reader.Read())
                    {
                        connectionId = (Guid)reader["DbConnectionId"];
                    }
                    else
                    {
                        throw new NotFoundException("Could not retrieve database connection.", "Could not retrieve database connection for broker id:" + brokerId);
                    }
                }

                connectionInfo = connectionInfoDictionary.Value[connectionId];
                internalCache.Add(key, connectionInfo, DateTimeOffset.Now.AddHours(1));
            }

            return connectionInfo;
        }

        /// <summary>
        /// Get connection info by user login name.
        /// Will throw NotFoundException if user login is not found.
        /// </summary>
        /// <param name="loginName">Login name of the "B" user account.</param>
        /// <param name="brokerId">Broker Id of the login name.</param>
        /// <returns>The connection info.</returns>
        public static DbConnectionInfo GetConnectionInfoByLoginName(string loginName, out Guid brokerId)
        {
            brokerId = Guid.Empty;

            SqlParameter[] parameters = 
            {
                new SqlParameter("@LoginNm", loginName)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "ALL_USER_SHARED_DB_RetrieveByLoginNm", parameters))
            {
                if (reader.Read())
                {
                    brokerId = (Guid)reader["BrokerId"];
                }
                else
                {
                    throw new NotFoundException("Login name is not found.");
                }
            }

            return GetConnectionInfo(brokerId);
        }

        /// <summary>
        /// Get connection info by customer code.
        /// Will throw NotFoundException if customer code is not found.
        /// </summary>
        /// <param name="customerCode">Client customer code.</param>
        /// <param name="brokerId">Broker Id of the customer code.</param>
        /// <returns>The connection info.</returns>
        public static DbConnectionInfo GetConnectionInfoByCustomerCode(string customerCode, out Guid brokerId)
        {
            brokerId = Guid.Empty;

            SqlParameter[] parameters = 
            {
                new SqlParameter("@CustomerCode", customerCode)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "RetrieveBrokerIdByCustomerCode", parameters))
            {
                if (reader.Read())
                {
                    brokerId = (Guid)reader["BrokerId"];
                }
                else
                {
                    throw new NotFoundException("Customer Code is not found.");
                }
            }

            return GetConnectionInfo(brokerId);
        }

        /// <summary>
        /// Get connection info by loan id. Try to avoid this method as much as possible. This method
        /// loop through all possible connections and look for the loan id.
        /// Will throw LoanNotFoundException if loan id is invalid.
        /// </summary>
        /// <param name="loanId">Loan Id to locate.</param>
        /// <param name="brokerId">Broker Id that the loan belong.</param>
        /// <returns>The connection info.</returns>
        public static DbConnectionInfo GetConnectionInfoByLoanId(Guid loanId, out Guid brokerId)
        {
            brokerId = Guid.Empty;

            string key = loanId.ToString();
            object item = internalCache.Get(key);

            DbConnectionInfo returnDbConnectionInfo = null;

            if (item != null && item is Guid)
            {
                brokerId = (Guid)item;
                returnDbConnectionInfo = GetConnectionInfo(brokerId);
            }
            else
            {
                returnDbConnectionInfo = GetConnectionInfoByLoanIdImpl(loanId, out brokerId);
                internalCache.Add(key, brokerId, DateTimeOffset.Now.AddHours(1));
            }

            return returnDbConnectionInfo;
        }

        /// <summary>
        /// Get connection info by user id. Try to avoid this method as much as possible. This method
        /// loop through all possible connections and look for the user id.
        /// Will throw NotFoundException if user id is invalid.
        /// </summary>
        /// <param name="userId">User Id to locate.</param>
        /// <param name="brokerId">Broker Id that the user belong to.</param>
        /// <returns>The connection Info.</returns>
        public static DbConnectionInfo GetConnectionInfoByUserId(Guid userId, out Guid brokerId)
        {
            brokerId = Guid.Empty;
            foreach (DbConnectionInfo connInfo in ListAll())
            {
                SqlParameter[] parameters = 
                {
                    new SqlParameter("@UserId", userId)
                };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "GetBrokerIdByUserId", parameters))
                {
                    if (reader.Read())
                    {
                        brokerId = (Guid)reader["BrokerId"];
                        return connInfo;
                    }
                }
            }

            throw new NotFoundException("Could not locate userId:" + userId);
        }

        /// <summary>
        /// Add connection mapping for the broker.
        /// </summary>
        /// <param name="connectionId">Connection id.</param>
        /// <param name="brokerId">Id of the broker.</param>
        /// <param name="customerCode">Customer code of the broker.</param>
        public static void AddConnectionInfoMapping(Guid connectionId, Guid brokerId, string customerCode)
        {
            SqlParameter[] parameter =
            {
                new SqlParameter("@DbConnectionId", connectionId),
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@CustomerCode", customerCode)
            };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "DB_CONNECTION_x_BROKER_Add", 3, parameter);
        }

        /// <summary>
        /// Gets a list of all database connection information.
        /// </summary>
        /// <returns>List of all database connection information.</returns>
        public static IEnumerable<DbConnectionInfo> ListAll()
        {
            List<DbConnectionInfo> list = new List<DbConnectionInfo>();

            foreach (var o in connectionInfoDictionary.Value.Values)
            {
                list.Add(o);
            }

            // dd 7-20-2016 - Enforce the database connection order.
            return list.OrderBy(o => o.Code);
        }

        /// <summary>
        /// Gets the full access sql connection.
        /// </summary>
        /// <param name="shouldProfile">True if the connection should profile, false otherwise.</param>
        /// <returns>Full access sql connection.</returns>
        public DbConnection GetConnection(bool shouldProfile = true)
        {
            var connection = new SqlConnection(this.FullAccessConnectionString);

            if (shouldProfile)
            {
                var profiler = ProfilerFactory.Create();
                return profiler.Wrap(connection);
            }

            return connection;
        }

        /// <summary>
        /// Gets the read-only sql connection.
        /// </summary>
        /// <param name="shouldProfile">True if the connection should profile, false otherwise.</param>
        /// <returns>Read-only sql connection.</returns>
        public DbConnection GetReadOnlyConnection(bool shouldProfile = true)
        {
            var connection = new SqlConnection(this.ReadOnlyConnectionString);

            if (shouldProfile)
            {
                var profiler = ProfilerFactory.Create();
                return profiler.Wrap(connection);
            }

            return connection;
        }

        /// <summary>
        /// Get connection info by loan id. Try to avoid this method as much as possible. This method
        /// loop through all possible connections and look for the loan id.
        /// Will throw LoanNotFoundException if loan id is invalid.
        /// </summary>
        /// <param name="loanId">Loan Id to locate.</param>
        /// <param name="brokerId">Broker Id that the loan belong.</param>
        /// <returns>The connection info.</returns>
        private static DbConnectionInfo GetConnectionInfoByLoanIdImpl(Guid loanId, out Guid brokerId)
        {
            brokerId = Guid.Empty;

            foreach (DbConnectionInfo connInfo in ListAll())
            {
                SqlParameter[] parameters = 
                {
                    new SqlParameter("@LoanId", loanId)
                };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "GetBrokerIdByLoanId", parameters))
                {
                    if (reader.Read())
                    {
                        brokerId = (Guid)reader["sBrokerId"];
                        return connInfo;
                    }
                }
            }

            throw new LoanNotFoundException(loanId);
        }

        /// <summary>
        /// Build a connection string.
        /// </summary>
        /// <param name="encryptedUserName">User name for the connection string.</param>
        /// <param name="encryptedPassword">Password for the connection string.</param>
        /// <param name="useDefaultEncryptionKey">Whether to use default encryption.</param>
        /// <returns>A sql connection string.</returns>
        private string BuildConnectionString(string encryptedUserName, string encryptedPassword, bool useDefaultEncryptionKey)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = this.DataSource;
            builder.InitialCatalog = this.InitialCatalog;

            // Hard code for opm 472386
            string serverName = LendersOffice.Constants.ConstAppDavid.ServerName;
            if (serverName.Equals("LOPROC3A", StringComparison.OrdinalIgnoreCase) || serverName.Equals("LOPROC3B", StringComparison.OrdinalIgnoreCase))
            {
                builder.MaxPoolSize = 200;
            }

            if (string.IsNullOrEmpty(this.FailoverPartner) == false)
            {
                builder.FailoverPartner = this.FailoverPartner;
            }

            if (useDefaultEncryptionKey)
            {
                builder.UserID = EncryptionHelper.DecryptConnectionString(encryptedUserName);
                builder.Password = EncryptionHelper.DecryptConnectionString(encryptedPassword);

                return builder.ConnectionString;
            }
            else
            {
                // This is file-based encryption.  We defer to the decryption helper.
                builder.UserID = encryptedUserName;
                builder.Password = encryptedPassword;

                var encryptedString = builder.ConnectionString;
                var connectionString = DBConnectionString.Create(encryptedString).Value;

                // OPM 454459.  Attempt to decrypt the connection strings using
                // the keys located in an xml file.
                // To allow for a smoother deploment of this feature, it is legal
                // to have the file missing.  In that case it should operate as if
                // the strings are still in plain text.  Additionally, a deployed
                // file contains a blank key entry, it is interpreted as plain text.
                try
                {
                    DBConnectionString decryptedConnectionString;
                    if (ConnectionStringDecryptionDriverHelper.TryDecryptConnectionString(connectionString, out decryptedConnectionString))
                    {
                        return decryptedConnectionString.Value;
                    }
                    else
                    {
                        return encryptedString;
                    }
                }
                catch (LqbGrammar.Exceptions.DeveloperException exc)
                    when (exc.InnerException != null
                        && (exc.InnerException is System.IO.FileNotFoundException
                        || exc.InnerException is System.IO.DirectoryNotFoundException))
                {
                    // Key file missing is allowed while we are still deploying
                    // our key files to all machines.
                    return encryptedString;
                }
            }
        }
    }
}