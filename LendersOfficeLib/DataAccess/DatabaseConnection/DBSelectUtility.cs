﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using LendersOffice.Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Utility for executing adhoc sql SELECT statements via the FOOL architecture.
    /// </summary>
    public static class DBSelectUtility
    {
        /// <summary>
        /// Select method that takes in a DataSrc parameter for selecting the appropriate DB.
        /// </summary>
        /// <param name="dataSrc">The DataSrc parameter for selecting the appropriate DB.</param>
        /// <param name="sql">The adhoc sql SELECT statement.</param>
        /// <param name="timeout">Optional timeout value.</param>
        /// <param name="parameters">The data that the update is passing to the DB.</param>
        /// <param name="readerCode">Delegate to the data processing code.</param>
        public static void ProcessDBData(DataSrc dataSrc, string sql, TimeoutInSeconds? timeout, IEnumerable<SqlParameter> parameters, Action<IDataReader> readerCode)
        {
            using (var sqlConnection = DbAccessUtils.GetConnection(dataSrc))
            {
                ProcessDBData(sqlConnection, null, sql, timeout, parameters, readerCode);
            }
        }

        /// <summary>
        /// Select method that takes in a Guid broker identifier parameter for selecting the appropriate DB.
        /// </summary>
        /// <param name="brokerId">The Guid broker identifier parameter for selecting the appropriate DB.</param>
        /// <param name="sql">The adhoc sql SELECT statement.</param>
        /// <param name="timeout">Optional timeout value.</param>
        /// <param name="parameters">The data that the update is passing to the DB.</param>
        /// <param name="readerCode">Delegate to the data processing code.</param>
        public static void ProcessDBData(Guid brokerId, string sql, TimeoutInSeconds? timeout, IEnumerable<SqlParameter> parameters, Action<IDataReader> readerCode)
        {
            using (var sqlConnection = DbAccessUtils.GetReadonlyConnection(brokerId))
            {
                ProcessDBData(sqlConnection, null, sql, timeout, parameters, readerCode);
            }
        }

        /// <summary>
        /// Select method that takes in a DbConnectionInfo parameter for selecting the appropriate DB.
        /// </summary>
        /// <param name="connInfo">The DbConnectionInfo parameter for selecting the appropriate DB.</param>
        /// <param name="sql">The adhoc sql SELECT statement.</param>
        /// <param name="timeout">Optional timeout value.</param>
        /// <param name="parameters">The data that the update is passing to the DB.</param>
        /// <param name="readerCode">Delegate to the data processing code.</param>
        public static void ProcessDBData(DbConnectionInfo connInfo, string sql, TimeoutInSeconds? timeout, IEnumerable<SqlParameter> parameters, Action<IDataReader> readerCode)
        {
            using (var sqlConnection = connInfo.GetReadOnlyConnection())
            {
                ProcessDBData(sqlConnection, null, sql, timeout, parameters, readerCode);
            }
        }

        /// <summary>
        /// Select method that takes in a DbConnection parameter for selecting the appropriate DB.
        /// This can be used in order to run multiple queries with the same connection.  However, the
        /// calling code is responsible for properly closing the connection.
        /// </summary>
        /// <param name="sqlConnection">The DbConnection parameter for selecting the appropriate DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="sql">The adhoc sql SELECT statement.</param>
        /// <param name="timeout">Optional timeout value.</param>
        /// <param name="parameters">The data that the update is passing to the DB.</param>
        /// <param name="readerCode">Delegate to the data processing code.</param>
        public static void ProcessDBData(DbConnection sqlConnection, DbTransaction transaction, string sql, TimeoutInSeconds? timeout, IEnumerable<SqlParameter> parameters, Action<IDataReader> readerCode)
        {
            TimeoutInSeconds to = (timeout == null) ? TimeoutInSeconds.Thirty : timeout.Value;

            var sqlQuery = SQLQueryString.Create(sql);
            if (sqlQuery == null)
            {
                throw new ApplicationException("Bad query construction");
            }

            using (var reader = SqlServerHelper.Select(sqlConnection, transaction, sqlQuery.Value, parameters, to))
            {
                readerCode(reader);
            }
        }

        /// <summary>
        /// Populate the input DataSet with the results of the SELECT query.
        /// </summary>
        /// <param name="dataSrc">The DataSrc parameter for selecting the appropriate DB.</param>
        /// <param name="dataSet">The DataSet to populate.</param>
        /// <param name="sql">The adhoc sql SELECT statement.</param>
        /// <param name="timeout">Optional timeout value.</param>
        /// <param name="parameters">The data that the update is passing to the DB.</param>
        public static void FillDataSet(DataSrc dataSrc, DataSet dataSet, string sql, TimeoutInSeconds? timeout, IEnumerable<SqlParameter> parameters)
        {
            using (var sqlConnection = DbAccessUtils.GetConnection(dataSrc))
            {
                FillDataSet(sqlConnection, null, dataSet, sql, timeout, parameters);
            }
        }

        /// <summary>
        /// Populate the input DataSet with the results of the SELECT query.
        /// </summary>
        /// <param name="brokerId">The Guid broker identifier parameter for selecting the appropriate DB.</param>
        /// <param name="dataSet">The DataSet to populate.</param>
        /// <param name="sql">The adhoc sql SELECT statement.</param>
        /// <param name="timeout">Optional timeout value.</param>
        /// <param name="parameters">The data that the update is passing to the DB.</param>
        public static void FillDataSet(Guid brokerId, DataSet dataSet, string sql, TimeoutInSeconds? timeout, IEnumerable<SqlParameter> parameters)
        {
            using (var sqlConnection = DbAccessUtils.GetReadonlyConnection(brokerId))
            {
                FillDataSet(sqlConnection, null, dataSet, sql, timeout, parameters);
            }
        }

        /// <summary>
        /// Populate the input DataSet with the results of the SELECT query.
        /// </summary>
        /// <param name="connInfo">The DbConnectionInfo parameter for selecting the appropriate DB.</param>
        /// <param name="dataSet">The DataSet to populate.</param>
        /// <param name="sql">The adhoc sql SELECT statement.</param>
        /// <param name="timeout">Optional timeout value.</param>
        /// <param name="parameters">The data that the update is passing to the DB.</param>
        public static void FillDataSet(DbConnectionInfo connInfo, DataSet dataSet, string sql, TimeoutInSeconds? timeout, IEnumerable<SqlParameter> parameters)
        {
            using (var sqlConnection = connInfo.GetReadOnlyConnection())
            {
                FillDataSet(sqlConnection, null, dataSet, sql, timeout, parameters);
            }
        }

        /// <summary>
        /// Populate the input DataSet with the results of the SELECT query.
        /// This can be used in order to run multiple queries with the same connection.  However, the
        /// calling code is responsible for properly closing the connection.
        /// </summary>
        /// <param name="sqlConnection">The DbConnection parameter for selecting the appropriate DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="dataSet">The DataSet to populate.</param>
        /// <param name="sql">The adhoc sql SELECT statement.</param>
        /// <param name="timeout">Optional timeout value.</param>
        /// <param name="parameters">The data that the update is passing to the DB.</param>
        public static void FillDataSet(DbConnection sqlConnection, DbTransaction transaction, DataSet dataSet, string sql, TimeoutInSeconds? timeout, IEnumerable<SqlParameter> parameters)
        {
            TimeoutInSeconds to = (timeout == null) ? TimeoutInSeconds.Thirty : timeout.Value;

            var sqlQuery = SQLQueryString.Create(sql);
            if (sqlQuery == null)
            {
                throw new ApplicationException("Bad query construction");
            }

            SqlServerHelper.FillDataSet(sqlConnection, transaction, dataSet, sqlQuery.Value, parameters, to);
        }
    }
}
