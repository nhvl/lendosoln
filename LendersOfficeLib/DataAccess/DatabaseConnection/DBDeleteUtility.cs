﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using LendersOffice.Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Utility for executing adhoc sql DELETE statements via the FOOL architecture.
    /// </summary>
    public static class DBDeleteUtility
    {
        /// <summary>
        /// Delete method that takes in a DataSrc parameter for selecting the appropriate DB.
        /// </summary>
        /// <param name="dataSrc">The DataSrc parameter for selecting the appropriate DB.</param>
        /// <param name="sql">The adhoc sql DELETE statement.</param>
        /// <param name="timeout">Optional timeout value.</param>
        /// <param name="parameters">The data that the delete is passing to the DB.</param>
        /// <returns>The number of rows affected by the DELETE statement.</returns>
        public static ModifiedRowCount Delete(DataSrc dataSrc, string sql, TimeoutInSeconds? timeout, IEnumerable<SqlParameter> parameters)
        {
            using (var sqlConnection = DbAccessUtils.GetConnection(dataSrc))
            {
                return Delete(sqlConnection, null, sql, timeout, parameters);
            }
        }

        /// <summary>
        /// Delete method that takes in a Guid broker identifier parameter for selecting the appropriate DB.
        /// </summary>
        /// <param name="brokerId">The Guid broker identifier parameter for selecting the appropriate DB.</param>
        /// <param name="sql">The adhoc sql DELETE statement.</param>
        /// <param name="timeout">Optional timeout value.</param>
        /// <param name="parameters">The data that the delete is passing to the DB.</param>
        /// <returns>The number of rows affected by the DELETE statement.</returns>
        public static ModifiedRowCount Delete(Guid brokerId, string sql, TimeoutInSeconds? timeout, IEnumerable<SqlParameter> parameters)
        {
            using (var sqlConnection = DbAccessUtils.GetConnection(brokerId))
            {
                return Delete(sqlConnection, null, sql, timeout, parameters);
            }
        }

        /// <summary>
        /// Delete method that takes in a DbConnectionInfo parameter for selecting the appropriate DB.
        /// </summary>
        /// <param name="connInfo">The DbConnectionInfo parameter for selecting the appropriate DB.</param>
        /// <param name="sql">The adhoc sql DELETE statement.</param>
        /// <param name="timeout">Optional timeout value.</param>
        /// <param name="parameters">The data that the delete is passing to the DB.</param>
        /// <returns>The number of rows affected by the DELETE statement.</returns>
        public static ModifiedRowCount Delete(DbConnectionInfo connInfo, string sql, TimeoutInSeconds? timeout, IEnumerable<SqlParameter> parameters)
        {
            using (var sqlConnection = connInfo.GetConnection())
            {
                return Delete(sqlConnection, null, sql, timeout, parameters);
            }
        }

        /// <summary>
        /// Delete method that takes in a DbConnection parameter for selecting the appropriate DB.
        /// This can be used in order to run multiple queries with the same connection.  However, the
        /// calling code is responsible for properly closing the connection.
        /// </summary>
        /// <param name="sqlConnection">The DbConnection parameter for selecting the appropriate DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="sql">The adhoc sql DELETE statement.</param>
        /// <param name="timeout">Optional timeout value.</param>
        /// <param name="parameters">The data that the delete is passing to the DB.</param>
        /// <returns>The number of rows affected by the DELETE statement.</returns>
        public static ModifiedRowCount Delete(DbConnection sqlConnection, DbTransaction transaction, string sql, TimeoutInSeconds? timeout, IEnumerable<SqlParameter> parameters)
        {
            TimeoutInSeconds to = (timeout == null) ? TimeoutInSeconds.Default : timeout.Value;

            var sqlQuery = SQLQueryString.Create(sql);
            if (sqlQuery == null)
            {
                throw new ApplicationException("Bad query construction");
            }

            return SqlServerHelper.Delete(sqlConnection, transaction, sqlQuery.Value, parameters, to);
        }
    }
}
