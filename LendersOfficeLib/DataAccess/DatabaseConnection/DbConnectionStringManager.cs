﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using LendersOffice.Common;
    using LendersOffice.Drivers.ConnectionStringDecryption;
    using LendersOffice.ObjLib.Extensions;
    using LqbGrammar.DataTypes;
    using Toolbox;

    /// <summary>
    /// This class manages communication with the secure database to retrieve connection strings.
    /// </summary>
    public static class DbConnectionStringManager
    {
        /// <summary>
        /// Placeholder to put into snapshot DB's catalog name to avoid runaway
        /// static constructor issues when ConstStage is loading up.
        /// </summary>
        private const string SnapshotCatalogueIndicator = "SNAPSHOT_CATALOG";

        /// <summary>
        /// The ID of the secure database connection string.
        /// </summary>
        private static readonly Guid SecureConnectionStringId = Guid.Empty;

        /// <summary>
        /// A mapping between data sources and their corresponding IDs in the secure database.
        /// </summary>
        private static readonly Dictionary<DataSrc, Guid> ConnectionStringMappings
            = new Dictionary<DataSrc, Guid>()
            {
                [DataSrc.LQBSecure] = SecureConnectionStringId,
                [DataSrc.LOShare] = new Guid("cd3ee90d-ae4a-40a6-8b72-5aa22fbaec68"),
                [DataSrc.LOShareROnly] = new Guid("4ac184e8-1e3f-4bc8-be5d-5506a36ca108"),
                [DataSrc.LOTransient] = new Guid("f7087a23-2ef0-4ba4-9736-8c1a24b0ba8e"),
                [DataSrc.LpeRelease1] = new Guid("6f41e284-8a78-4f95-970f-7106d6971a4b"),
                [DataSrc.LpeRelease2] = new Guid("eca34252-38e6-4aec-927e-0a1e74173f1c"),
                [DataSrc.LpeServerSnapshot] = new Guid("c70a15d1-4def-406d-bbed-dad954e9c014"),
                [DataSrc.LpeSrc] = new Guid("4a8c0f29-7fdf-4dea-a3d9-27fc1a6f3452"),
                [DataSrc.RateSheet] = new Guid("415f331a-51a1-485b-8b55-5affd41ba9b8"),
                [DataSrc.Hangfire] = new Guid("f3820290-9b57-49e9-9e15-50836e6ddc52"),
                [DataSrc.ConversationLog] = new Guid("35B85051-9FC2-46D1-86DD-C05F9D11CD4D")
            };

        /// <summary>
        /// Gets or sets a cache of retrieved connection strings so we don't have to hit the database each time
        /// they're requested.
        /// </summary>
        /// <value>A dictionary of cached connection strings from the secure database.</value>
        private static readonly LqbSingleThreadInitializeLazy<Dictionary<Guid, ManagedDbConnectionInfo>> ConnectionStrings = 
            new LqbSingleThreadInitializeLazy<Dictionary<Guid, ManagedDbConnectionInfo>>(PopulateConnectionStrings);

        /// <summary>
        /// Retrieves the connection string corresponding to the requested data source.
        /// </summary>
        /// <param name="source">Indicates which connection string is requested.</param>
        /// <returns>The connection string to access the requested data source.</returns>
        public static string GetConnectionString(DataSrc source)
        {
            var stringId = ConnectionStringMappings[source];
            var connStr = ConnectionStrings.Value[stringId].FullAccessConnectionString;

            if (source == DataSrc.LpeServerSnapshot)
            {
                connStr = connStr.Replace(SnapshotCatalogueIndicator, SnapshotControler.GetSelfSnapshotName());
            }

            return connStr;
        }

        /// <summary>
        /// Gets the dictionary of split database connection strings.
        /// </summary>
        /// <returns>The dictionary of split database connection strings.</returns>
        public static Dictionary<Guid, DbConnectionInfo> GetAllSplitDatabaseConnectionStrings()
        {
            return ConnectionStrings.Value.Values.Where(p => p.IsSplitDatabaseConnection).Cast<DbConnectionInfo>().ToDictionary(q => q.Id);
        }

        /// <summary>
        /// Populates the list of connection strings by ID.
        /// </summary>
        /// <returns>
        /// The populated connection string mapping.
        /// </returns>
        private static Dictionary<Guid, ManagedDbConnectionInfo> PopulateConnectionStrings()
        {
            var connectionStrings = new Dictionary<Guid, ManagedDbConnectionInfo>();

            try
            {
                const string SecureConnnectionKey = "SecureDBConnStrReadOnly";

                // Avoid SiteConfig class as that involves database calls that cannot be made until this method finishes.
                var encryptedConnectionString = System.Configuration.ConfigurationManager.AppSettings[SecureConnnectionKey];
                LoadFromSecureDB(connectionStrings, encryptedConnectionString);
            }
            catch (SystemException exc) when
                (exc is KeyNotFoundException ||
                exc is FormatException ||
                exc is ArgumentException)
            {
                // This indicates that there was a problem obtaining the secure DB connection.
                // For now we can continue and leave the connection dictionary empty as any connection
                // requests will default to the legacy implementation. However, more comprehensive
                // error handling may need to be added when we deprecate the legacy connection retrieval.
            }
            catch (CBaseException)
            {
                // See above
            }

            return connectionStrings;
        }

        /// <summary>
        /// Read in the connection strings from the secure database.
        /// </summary>
        /// <param name="connectionStrings">The list of connection strings.</param>
        /// <param name="encryptedConnectionString">Encrypted connection string for the secure database.</param>
        private static void LoadFromSecureDB(Dictionary<Guid, ManagedDbConnectionInfo> connectionStrings, string encryptedConnectionString)
        {
            const string Sproc = "CONNECTION_STRING_ListAll";

            var secureDbConnection = GetSecureConnectionInfo(encryptedConnectionString);
            connectionStrings[SecureConnectionStringId] = secureDbConnection;

            var sprocName = StoredProcedureName.Create(Sproc).Value;
            using (var conn = new SqlConnection(secureDbConnection.FullAccessConnectionString))
            {
                using (var reader = LendersOffice.Drivers.SqlServerDB.StoredProcedureDriverHelper.ExecuteReader(conn, null, sprocName, null, TimeoutInSeconds.Default))
                {
                    int indexId = reader.GetOrdinal("Id");
                    int indexDataSource = reader.GetOrdinal("DataSource");
                    int indexInitialCatalog = reader.GetOrdinal("InitialCatalog");
                    int indexUserId = reader.GetOrdinal("UserId");
                    int indexPassword = reader.GetOrdinal("Password");
                    int indexFailoverPartner = reader.GetOrdinal("FailoverPartner");
                    int indexEncrypt = reader.GetOrdinal("Encrypt");
                    int indexReadOnlyUserId = reader.GetOrdinal("ReadOnlyUserId");
                    int indexReadOnlyPassword = reader.GetOrdinal("ReadOnlyPassword");
                    int indexConnectionCode = reader.GetOrdinal("ConnectionCode");
                    int indexDescription = reader.GetOrdinal("Description");
                    int indexConnectionType = reader.GetOrdinal("ConnectionType");

                    while (reader.Read())
                    {
                        Guid stringId = reader.GetGuid(indexId);

                        var dataSource = reader.GetString(indexDataSource);
                        var initialCatalog =
                            stringId == ConnectionStringMappings[DataSrc.LpeServerSnapshot]
                            ? SnapshotCatalogueIndicator
                            : reader.GetString(indexInitialCatalog);
                        var userID = reader.GetString(indexUserId);
                        var password = reader.GetString(indexPassword);
                        var failoverPartner = reader.GetString(indexFailoverPartner);
                        var encrypt = reader.GetBoolean(indexEncrypt); // NOTE: this has nothing to do with whether or not the UserId/Password are stored encrypted!
                        var readonlyUserId = reader.GetString(indexReadOnlyUserId);
                        var readOnlyPassword = reader.GetString(indexReadOnlyPassword);
                        var connectionCode = reader.GetString(indexConnectionCode);
                        var description = reader.GetString(indexDescription);
                        var connectionType = reader.GetString(indexConnectionType);

                        var info = new ManagedDbConnectionInfo(
                            stringId,
                            connectionCode,
                            description,
                            dataSource,
                            initialCatalog,
                            failoverPartner,
                            readonlyUserId, 
                            readOnlyPassword,
                            userID,
                            password, 
                            connectionType);

                        connectionStrings[stringId] = info;
                    }
                }
            }
        }

        /// <summary>
        /// Process database connection string encryption and decrypt keys if
        /// needed.
        /// </summary>
        /// <param name="connectionStrings">The list of connection strings.</param>
        private static void ProcessConnectionStringEncryption(Dictionary<Guid, string> connectionStrings)
        {
            List<Guid> connectionStringIds = new List<Guid>(connectionStrings.Keys);
            foreach (var stringId in connectionStringIds)
            {
                if (stringId == SecureConnectionStringId)
                {
                    // The secure database connection string has already been
                    // decrypted so it can be used to retrieve the other 
                    // connection strings.
                    continue;
                }

                var connectionString = DBConnectionString.Create(connectionStrings[stringId]).Value;
                DBConnectionString newConnectionString;
                if (ConnectionStringDecryptionDriverHelper.TryDecryptConnectionString(connectionString, out newConnectionString))
                {
                    connectionStrings[stringId] = newConnectionString.Value;
                }
            }
        }

        /// <summary>
        /// Process database connection string encryption and decrypt keys if
        /// needed.
        /// </summary>
        /// <param name="encrypytedConnectionString">The encrypted connection string.</param>
        /// <param name="processedConnectionString">The decrypted connection string.</param>
        /// <returns>True if successfully processed.</returns>
        private static bool ProcessConnectionStringEncryption(string encrypytedConnectionString, out string processedConnectionString)
        {
            DBConnectionString decryptedConnectionString;
            var connectionString = DBConnectionString.Create(encrypytedConnectionString).Value;
            if (ConnectionStringDecryptionDriverHelper.TryDecryptConnectionString(connectionString, out decryptedConnectionString))
            {
                processedConnectionString = decryptedConnectionString.Value;
                return true;
            }
            else
            {
                processedConnectionString = encrypytedConnectionString;
                return false;
            }
        }

        /// <summary>
        /// Decrypt the connection string.
        /// </summary>
        /// <param name="encryptConnectionString">The encrypted connection string.</param>
        /// <returns>The decrypted connection string.</returns>
        private static string DecryptSqlConnection(string encryptConnectionString)
        {
            var builder = new SqlConnectionStringBuilder(encryptConnectionString);
            builder.UserID = EncryptionHelper.DecryptConnectionString(builder.UserID);
            builder.Password = EncryptionHelper.DecryptConnectionString(builder.Password);

            return builder.ConnectionString;
        }

        /// <summary>
        /// Get the secure connection string from the encrypted string.
        /// </summary>
        /// <param name="secureEncryptedConnectionString">The encrypted connection string.</param>
        /// <returns>A DbConnectionInfo object from the encrypted secure string.</returns>
        private static ManagedDbConnectionInfo GetSecureConnectionInfo(string secureEncryptedConnectionString)
        {
            var builder = new SqlConnectionStringBuilder(secureEncryptedConnectionString);
            var secureConnectionInfo = new ManagedDbConnectionInfo(
                Guid.Empty,
                string.Empty,
                string.Empty,
                builder.DataSource,
                builder.InitialCatalog,
                builder.FailoverPartner,
                builder.UserID,
                builder.Password,
                builder.UserID,
                builder.Password,
                string.Empty,
                useDefaultEncryptionKey: true);

            return secureConnectionInfo;
        }
    }
}