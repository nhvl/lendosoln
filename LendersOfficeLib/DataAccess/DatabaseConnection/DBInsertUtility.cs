﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using LendersOffice.Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Utility for executing adhoc sql INSERT statements via the FOOL architecture.
    /// Note that different tables have different types of primary keys.  Some
    /// tables have a Guid as the primary key.  Some tables have an integer as the
    /// primary key.  Some tables have multiple rows for the primary key.  There
    /// are multiple sets of methods here that correspond to both the parameter used
    /// for selection of the appropriate DB and the table's primary key type.
    /// </summary>
    public static class DBInsertUtility
    {
        /// <summary>
        /// Insert method that takes in a DataSrc parameter for selecting the appropriate DB.
        /// </summary>
        /// <param name="dataSrc">The DataSrc parameter for selecting the appropriate DB.</param>
        /// <param name="sql">The adhoc sql INSERT statement.</param>
        /// <param name="timeout">Optional timeout value.</param>
        /// <param name="parameters">The data that the insert is passing to the DB.</param>
        /// <returns>The primary key for the new row.</returns>
        public static PrimaryKeyGuid InsertGetGuid(DataSrc dataSrc, string sql, TimeoutInSeconds? timeout, IEnumerable<SqlParameter> parameters)
        {
            using (var sqlConnection = DbAccessUtils.GetConnection(dataSrc))
            {
                return InsertGetGuid(sqlConnection, null, sql, timeout, parameters);
            }
        }

        /// <summary>
        /// Insert method that takes in a Guid broker identifier parameter for selecting the appropriate DB.
        /// </summary>
        /// <param name="brokerId">The Guid broker identifier parameter for selecting the appropriate DB.</param>
        /// <param name="sql">The adhoc sql INSERT statement.</param>
        /// <param name="timeout">Optional timeout value.</param>
        /// <param name="parameters">The data that the insert is passing to the DB.</param>
        /// <returns>The primary key for the new row.</returns>
        public static PrimaryKeyGuid InsertGetGuid(Guid brokerId, string sql, TimeoutInSeconds? timeout, IEnumerable<SqlParameter> parameters)
        {
            using (var sqlConnection = DbAccessUtils.GetConnection(brokerId))
            {
                return InsertGetGuid(sqlConnection, null, sql, timeout, parameters);
            }
        }

        /// <summary>
        /// Insert method that takes in a DbConnectionInfo parameter for selecting the appropriate DB.
        /// </summary>
        /// <param name="connInfo">The DbConnectionInfo parameter for selecting the appropriate DB.</param>
        /// <param name="sql">The adhoc sql INSERT statement.</param>
        /// <param name="timeout">Optional timeout value.</param>
        /// <param name="parameters">The data that the insert is passing to the DB.</param>
        /// <returns>The primary key for the new row.</returns>
        public static PrimaryKeyGuid InsertGetGuid(DbConnectionInfo connInfo, string sql, TimeoutInSeconds? timeout, IEnumerable<SqlParameter> parameters)
        {
            using (var sqlConnection = connInfo.GetConnection())
            {
                return InsertGetGuid(sqlConnection, null, sql, timeout, parameters);
            }
        }

        /// <summary>
        /// Insert method that takes in a DataSrc parameter for selecting the appropriate DB.
        /// </summary>
        /// <param name="dataSrc">The DataSrc parameter for selecting the appropriate DB.</param>
        /// <param name="sql">The adhoc sql INSERT statement.</param>
        /// <param name="timeout">Optional timeout value.</param>
        /// <param name="parameters">The data that the insert is passing to the DB.</param>
        /// <returns>The primary key for the new row.</returns>
        public static PrimaryKeyInt32 InsertGetInt32(DataSrc dataSrc, string sql, TimeoutInSeconds? timeout, IEnumerable<SqlParameter> parameters)
        {
            using (var sqlConnection = DbAccessUtils.GetConnection(dataSrc))
            {
                return InsertGetInt32(sqlConnection, null, sql, timeout, parameters);
            }
        }

        /// <summary>
        /// Insert method that takes in a Guid broker identifier parameter for selecting the appropriate DB.
        /// </summary>
        /// <param name="brokerId">The Guid broker identifier parameter for selecting the appropriate DB.</param>
        /// <param name="sql">The adhoc sql INSERT statement.</param>
        /// <param name="timeout">Optional timeout value.</param>
        /// <param name="parameters">The data that the insert is passing to the DB.</param>
        /// <returns>The primary key for the new row.</returns>
        public static PrimaryKeyInt32 InsertGetInt32(Guid brokerId, string sql, TimeoutInSeconds? timeout, IEnumerable<SqlParameter> parameters)
        {
            using (var sqlConnection = DbAccessUtils.GetConnection(brokerId))
            {
                return InsertGetInt32(sqlConnection, null, sql, timeout, parameters);
            }
        }

        /// <summary>
        /// Insert method that takes in a DbConnectionInfo parameter for selecting the appropriate DB.
        /// </summary>
        /// <param name="connInfo">The DbConnectionInfo parameter for selecting the appropriate DB.</param>
        /// <param name="sql">The adhoc sql INSERT statement.</param>
        /// <param name="timeout">Optional timeout value.</param>
        /// <param name="parameters">The data that the insert is passing to the DB.</param>
        /// <returns>The primary key for the new row.</returns>
        public static PrimaryKeyInt32 InsertGetInt32(DbConnectionInfo connInfo, string sql, TimeoutInSeconds? timeout, IEnumerable<SqlParameter> parameters)
        {
            using (var sqlConnection = connInfo.GetConnection())
            {
                return InsertGetInt32(sqlConnection, null, sql, timeout, parameters);
            }
        }

        /// <summary>
        /// Insert method that takes in a DataSrc parameter for selecting the appropriate DB.
        /// </summary>
        /// <param name="dataSrc">The DataSrc parameter for selecting the appropriate DB.</param>
        /// <param name="sql">The adhoc sql INSERT statement.</param>
        /// <param name="timeout">Optional timeout value.</param>
        /// <param name="parameters">The data that the insert is passing to the DB.</param>
        public static void InsertNoKey(DataSrc dataSrc, string sql, TimeoutInSeconds? timeout, IEnumerable<SqlParameter> parameters)
        {
            using (var sqlConnection = DbAccessUtils.GetConnection(dataSrc))
            {
                InsertNoKey(sqlConnection, null, sql, timeout, parameters);
            }
        }

        /// <summary>
        /// Insert method that takes in a Guid broker identifier parameter for selecting the appropriate DB.
        /// </summary>
        /// <param name="brokerId">The Guid broker identifier parameter for selecting the appropriate DB.</param>
        /// <param name="sql">The adhoc sql INSERT statement.</param>
        /// <param name="timeout">Optional timeout value.</param>
        /// <param name="parameters">The data that the insert is passing to the DB.</param>
        public static void InsertNoKey(Guid brokerId, string sql, TimeoutInSeconds? timeout, IEnumerable<SqlParameter> parameters)
        {
            using (var sqlConnection = DbAccessUtils.GetConnection(brokerId))
            {
                InsertNoKey(sqlConnection, null, sql, timeout, parameters);
            }
        }

        /// <summary>
        /// Insert method that takes in a DbConnectionInfo parameter for selecting the appropriate DB.
        /// </summary>
        /// <param name="connInfo">The DbConnectionInfo parameter for selecting the appropriate DB.</param>
        /// <param name="sql">The adhoc sql INSERT statement.</param>
        /// <param name="timeout">Optional timeout value.</param>
        /// <param name="parameters">The data that the insert is passing to the DB.</param>
        public static void InsertNoKey(DbConnectionInfo connInfo, string sql, TimeoutInSeconds? timeout, IEnumerable<SqlParameter> parameters)
        {
            using (var sqlConnection = connInfo.GetConnection())
            {
                InsertNoKey(sqlConnection, null, sql, timeout, parameters);
            }
        }

        /// <summary>
        /// Insert method that takes in a DbConnection parameter for selecting the appropriate DB.
        /// This can be used in order to run multiple queries with the same connection.  However, the
        /// calling code is responsible for properly closing the connection.
        /// </summary>
        /// <param name="sqlConnection">The DbConnection parameter for selecting the appropriate DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="sql">The adhoc sql INSERT statement.</param>
        /// <param name="timeout">Optional timeout value.</param>
        /// <param name="parameters">The data that the insert is passing to the DB.</param>
        /// <returns>The primary key for the new row.</returns>
        public static PrimaryKeyGuid InsertGetGuid(DbConnection sqlConnection, DbTransaction transaction, string sql, TimeoutInSeconds? timeout, IEnumerable<SqlParameter> parameters)
        {
            TimeoutInSeconds to = (timeout == null) ? TimeoutInSeconds.Default : timeout.Value;

            var sqlQuery = SQLQueryString.Create(sql);
            if (sqlQuery == null)
            {
                throw new ApplicationException("Bad query construction");
            }

            return SqlServerHelper.InsertGetGuid(sqlConnection, transaction, sqlQuery.Value, parameters, to);
        }

        /// <summary>
        /// Insert method that takes in a DbConnection parameter for selecting the appropriate DB.
        /// This can be used in order to run multiple queries with the same connection.  However, the
        /// calling code is responsible for properly closing the connection.
        /// </summary>
        /// <param name="sqlConnection">The DbConnection parameter for selecting the appropriate DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="sql">The adhoc sql INSERT statement.</param>
        /// <param name="timeout">Optional timeout value.</param>
        /// <param name="parameters">The data that the insert is passing to the DB.</param>
        /// <returns>The primary key for the new row.</returns>
        public static PrimaryKeyInt32 InsertGetInt32(DbConnection sqlConnection, DbTransaction transaction, string sql, TimeoutInSeconds? timeout, IEnumerable<SqlParameter> parameters)
        {
            TimeoutInSeconds to = (timeout == null) ? TimeoutInSeconds.Default : timeout.Value;

            var sqlQuery = SQLQueryString.Create(sql);
            if (sqlQuery == null)
            {
                throw new ApplicationException("Bad query construction");
            }

            return SqlServerHelper.InsertGetInt32(sqlConnection, transaction, sqlQuery.Value, parameters, to);
        }

        /// <summary>
        /// Insert method that takes in a DbConnection parameter for selecting the appropriate DB.
        /// This can be used in order to run multiple queries with the same connection.  However, the
        /// calling code is responsible for properly closing the connection.
        /// </summary>
        /// <param name="sqlConnection">The DbConnection parameter for selecting the appropriate DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="sql">The adhoc sql INSERT statement.</param>
        /// <param name="timeout">Optional timeout value.</param>
        /// <param name="parameters">The data that the insert is passing to the DB.</param>
        public static void InsertNoKey(DbConnection sqlConnection, DbTransaction transaction, string sql, TimeoutInSeconds? timeout, IEnumerable<SqlParameter> parameters)
        {
            TimeoutInSeconds to = (timeout == null) ? TimeoutInSeconds.Default : timeout.Value;

            var sqlQuery = SQLQueryString.Create(sql);
            if (sqlQuery == null)
            {
                throw new ApplicationException("Bad query construction");
            }

            SqlServerHelper.InsertNoKey(sqlConnection, transaction, sqlQuery.Value, parameters, to);
        }
    }
}
