/// 3/6/2012 - dd
/// 
/// THIS FILE IS EXTREMELY CRITICAL AND SENSITIVE. ALL CHECKIN ON THIS FILE SHOULD BE CODE REVIEW.
/// 
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Profiling;

    public enum DataSrc
    {
        LpeSrc      = 3, // LoDevLPE, LendersOfficeDemoLPE, LendersOfficeLPE
        LpeRelease1 = 4, 
        LpeRelease2 = 5,
        LOTransient = 6,
        RateSheet   = 8,
        LpeServerSnapshot = 11,
        LOShare = 12,
        LOShareROnly = 13,
        LQBSecure = 14,
        Hangfire = 15,
        LpeRelease3 = 16,
        LpeRelease4 = 17,
        ConversationLog = 18,
    }

    public static class DbAccessUtils
    {
        public static DbProviderFactory GetDbProvider(bool shouldProfile = true)
        {
            var sqlFactory = DbProviderFactories.GetFactory("System.Data.SqlClient");

            if (shouldProfile)
            {
                var profilerFactory = ProfilerFactory.Create();
                return profilerFactory.Wrap(sqlFactory);
            }

            return sqlFactory;
        }

        #region Encrypt/Decrypt SQL Connection String.
        public static string EncryptSqlConnection(string connectionString)
        {
            // 3/6/2012 dd - 
            // This method will take the connection string and return the connection string with encrypt password.
            // This method is public because we will provide a utilities in LOAdmin to encrypt SQL connection string.

            // To encrypt the SQL connection go to http://localhost/LendersOfficeApp/LOAdmin/Manage/SiteConfigEdit.aspx

            var builder = new SqlConnectionStringBuilder(connectionString);
            builder.UserID = EncryptionHelper.EncryptConnectionString(builder.UserID);
            builder.Password = EncryptionHelper.EncryptConnectionString(builder.Password);

            return builder.ConnectionString;
        }

		internal static string Decrypt(string data)
        {
            return EncryptionHelper.DecryptConnectionString(data);
        }
        #endregion

        public static SqlParameter CopySqlParameter(SqlParameter toCopy)
        {
            ICloneable iClone = toCopy as ICloneable;
            return iClone.Clone() as SqlParameter;
        }

        /// <summary>
        /// Sets up a connection to the desired data source.
        /// </summary>
        /// <param name="source">The data source to connect to.</param>
        /// <param name="shouldProfile">True if the connection should profile, false otherwise.</param>
        /// <returns>An established SQL connection.</returns>
        public static DbConnection GetConnection(DataSrc source, bool shouldProfile = true)
        {
            ILqbProfiler profiler;
            SqlConnection connection;
            if(source == DataSrc.LpeRelease3 || source == DataSrc.LpeRelease4)
            {
                string dbName = GetLpeDbSnapshotName(source);
                if( string.IsNullOrEmpty(dbName))
                {
                    throw new ArgumentException($"GetConnection({source}) error: its snapshot name is empty.");
                }

                var builder = new SqlConnectionStringBuilder(DbConnectionStringManager.GetConnectionString(DataSrc.LpeRelease1));
                builder.InitialCatalog = dbName;

                connection = new SqlConnection(builder.ConnectionString);

                if (shouldProfile)
                {
                    profiler = ProfilerFactory.Create();
                    return profiler.Wrap(connection);
                }

                return connection;
            }

            var requestedSourceConnectionString = DbConnectionStringManager.GetConnectionString(source);
            connection = new SqlConnection(requestedSourceConnectionString);

            if (source == DataSrc.LQBSecure || !shouldProfile)
            {
                return connection;
            }

            profiler = ProfilerFactory.Create();
            return profiler.Wrap(connection);
        }

        public static DbConnection GetReadonlyConnection(Guid brokerId, bool shouldProfile = true)
        {
            return DbConnectionInfo.GetReadOnlyConnection(brokerId, shouldProfile);
        }

        public static DbConnection GetConnection(Guid brokerId, bool shouldProfile = true)
        {
            return DbConnectionInfo.GetConnection(brokerId, shouldProfile);
        }

        public static SqlParameter SqlParameterForVarchar(string name, string value)
        {
            // NOTE: new SqlParameter(name, value) will assume nvarchar, which will
            //       make the usage less efficient.  Use this method for varchar columns.
            if (value == null)
            {
                var param = new SqlParameter(name, SqlDbType.VarChar);
                param.Value = DBNull.Value;
                return param;
            }
            else
            {
                var param = new SqlParameter(name, System.Data.SqlDbType.VarChar, value.Length);
                param.Value = value;
                return param;
            }
        }

		public static SqlParameter SqlParameterForSingleChar(string name, char value)
		{
			var param = new SqlParameter(name, SqlDbType.Char, 1);
			param.Value = value;
			return param;
		}

		public static string SQLString(string s)
        {
            return "'" + s.TrimWhitespaceAndBOM().Replace("'", "''") + "'" ;
        }

        public static DataTable InitializeTableValueDataTable(string name, Dictionary<string, Type> columns)
        {
            var table = new DataTable(name);
            foreach (var column in columns)
            {
                table.Columns.Add(column.Key, column.Value);
            }

            return table;
        }

        #region ConvertFromDB
        public static void ConvertFromDB<T>(object o, T ifDBNull, T ifConversionFails, out T x)
        {

            try
            {
                if (null == o || System.Convert.IsDBNull(o))
                    x = ifDBNull;
                else
                    x = (T)o;
            }
            catch
            {
                x = ifConversionFails;
            }
        }
        /// <summary>
        /// defaults to defaultObj if isDBNull or conversion fails.
        /// </summary>
        public static void ConvertFromDB<T>(object o, T defaultObj, out T x)
        {
            ConvertFromDB(o, defaultObj, defaultObj, out x);
        }

        /// <summary>
        /// defaults to the default for the type if is dbnull or if conversion fails.
        /// </summary>
        public static void ConvertFromDB<T>(object o, out T x)
        {
            ConvertFromDB(o, default(T), out x);
        }

        public static void SetLpeDbSnapshotName( DataSrc dataSrc, string lpeDbSnapshotName)
        {
            int idx = 0;

            switch(dataSrc)
            {
            case DataSrc.LpeRelease3:
                idx = 0;
                break;

            case DataSrc.LpeRelease4:
                idx = 1;
                break;

            default:
                throw new ArgumentException($"SetLpeDbSnapshotName error: unexpected {dataSrc}");
            }

            var snapshotNames = s_snapshotNames;
            string orgName = snapshotNames[idx];
            if (orgName != lpeDbSnapshotName)
            {
                lock(snapshotNamesLock)
                {
                    if(s_snapshotNames[idx] != lpeDbSnapshotName)
                    {
                        var tmp = new string[] { s_snapshotNames[0], s_snapshotNames[1] };
                        tmp[idx] = lpeDbSnapshotName;
                        s_snapshotNames = tmp;
                        Tools.LogInfo($"SetLpeDbSnapshotName( {dataSrc}, '{lpeDbSnapshotName}' ) : previous value '{orgName}'"); // is it ok for using Tools.LogInfo(...)
                    }
                }

            }
        }

        internal static string GetLpeDbSnapshotName(DataSrc dataSrc)
        {
            var snapshotNames = s_snapshotNames;
            switch (dataSrc)
            {
            case DataSrc.LpeRelease3:
                return snapshotNames[0];

            case DataSrc.LpeRelease4:
                return snapshotNames[1];

            default:
                throw new ArgumentException($"GetLpeDbSnapshotName error: unexpected {dataSrc}");
            }
        }
        #endregion

        static private string[] s_snapshotNames = {string.Empty, string.Empty};
        static private readonly object snapshotNamesLock = new object();
    }
}