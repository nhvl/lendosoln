﻿namespace DataAccess
{
    using System;

    /// <summary>
    /// The purpose of this class is to allow the manager class to
    /// access the raw connection strings without exposing them to
    /// the callers. This is not perfect as callers could cast to
    /// get access, but since all use is internal, this design should
    /// serve as an indication that caller should not need the string.
    /// </summary>
    internal class ManagedDbConnectionInfo : DbConnectionInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ManagedDbConnectionInfo" /> class.
        /// </summary>
        /// <param name="id">Unique identifier of the connection.</param>
        /// <param name="code">Unique code of the connection.</param>
        /// <param name="description">Description of the connection.</param>
        /// <param name="dataSource">Name or network address of the instance of SQL Server to connect to.</param>
        /// <param name="initialCatalog">Name of the database associated with the connection.</param>
        /// <param name="failoverPartner">The name or address of the partner server to connect to if the primary server is down.</param>
        /// <param name="readOnlyEncryptedUserName">User name for the readonly account.</param>
        /// <param name="readOnlyEncryptedPassword">Password for the readonly account.</param>
        /// <param name="fullAccessEncryptedUserName">User name for the full access account.</param>
        /// <param name="fullAccessEncryptedPassword">Password for the full access account.</param>
        /// <param name="connectionType">The type of connection.</param>
        /// <param name="useDefaultEncryptionKey">Whether or not to use default encryption.</param>
        internal ManagedDbConnectionInfo(Guid id, string code, string description, string dataSource, string initialCatalog, string failoverPartner, string readOnlyEncryptedUserName, string readOnlyEncryptedPassword, string fullAccessEncryptedUserName, string fullAccessEncryptedPassword, string connectionType, bool useDefaultEncryptionKey = false) 
            : base(id, code, description, dataSource, initialCatalog, failoverPartner, readOnlyEncryptedUserName, readOnlyEncryptedPassword, fullAccessEncryptedUserName, fullAccessEncryptedPassword, connectionType, useDefaultEncryptionKey)
        {
        }
       
        /// <summary>
        /// Gets or sets the read only connection string.
        /// </summary>
        public new string ReadOnlyConnectionString => base.ReadOnlyConnectionString;

        /// <summary>
        /// Gets or sets the full access connection string.
        /// </summary>
        public new string FullAccessConnectionString => base.FullAccessConnectionString;
    }
}