using System;
using System.Collections;
using System.Collections.Generic;

namespace DataAccess
{
    public class CLeftSummaryData : CPageData
    {
        private static CSelectStatementProvider s_selectProvider;

        static CLeftSummaryData()
        {
            StringList list = new StringList();

            list.Add("aBLastNm");
            list.Add("aBFirstNm");
            list.Add("sLNm");
            list.Add("sStatusT");
            list.Add("sfGetAgentOfRole");
            list.Add("sEmployeeLoanRep");
            list.Add("sQualTopR");
            list.Add("sQualBottomR");
            list.Add("sLtvR");
            list.Add("sCltvR");
            list.Add("sHcltvR");
            list.Add("IsTemplate");
            list.Add("sRateLockStatusT");
            list.Add("sDocMagicPlanCodeId");
            list.Add("sApr");
            list.Add("sFinMethT");
            list.Add("sLOrigFProps");
            list.Add("sLastDiscAPR");
            list.Add("sIsLineOfCredit");
            list.Add("sClosingCostFeeVersionT");
            list.Add("sIsAprOutOfTolerence");

            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection);

        }

        public CLeftSummaryData(Guid fileId) : base(fileId , "LeftSummaryData" , s_selectProvider )
        {
        }
    }
}
