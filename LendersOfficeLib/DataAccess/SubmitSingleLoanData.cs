using System;
using System.Collections;
using System.Data;

namespace DataAccess
{

	public class CSubmitSingleLoanData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CSubmitSingleLoanData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
            DataAccessUtils.AddCertificateFields(list);

            list.Add("sPmlCertXmlContent");
            list.Add("sQualBottomR");
            list.Add("sfApplyLoanProductTemplate");
            list.Add("sPml1stSubmitD");
            list.Add("lpeRunModeT");
            list.Add("sEmployeeLockDesk");
            list.Add("sEmployeeUnderwriter");
            list.Add("sLpTemplateId");
            list.Add("sNoteIRSubmitted");
            list.Add("sProThisMPmt");
            list.Add("sfConfirm1stLienLoanTentativelyAccepted");
            list.Add("sfSpinOffLoanForSubsequent2ndLienLoan");
            list.Add("sNoteIR");
            list.Add("sQualBottomR");
            list.Add("sLpeRateOptionIdOf1stLienIn8020");
            list.Add("sfRunLpe");
            list.Add("sLinkedLoanInfo");
            list.Add("sEmployeeProcessor");
            list.Add("sBranchId");
            list.Add("sfSetInternalRateLockPricing");
            list.Add("sOriginatorCompensationPaymentSourceT");
            list.Add("sOriginatorCompensationLenderFeeOptionT");
            list.Add("sIsOFinCreditLineInDrawPeriod");
            list.Add("sSubFin");
            list.Add("sConcurSubFin");
            list.Add("sSubFinT");
            list.Add("sProOFinBalPe");
            list.Add("SetPricingState");
            list.Add("sQMParR");
            list.Add("sProMIns");
            list.Add("sBranchChannelT");
            list.Add("sCorrespondentProcessT");
            list.Add("sSubFinIR");
            list.Add("sSubFinTerm");
            list.Add("sQMParR");
            list.Add("sfGetLoanProgramsToRunLpe");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CSubmitSingleLoanData(Guid fileId) : base(fileId, "CSubmitSingleLoanData", s_selectProvider)
		{
		}
        protected override bool m_enforceAccessControl
        {
            get { return false; }
        }

	}
}
