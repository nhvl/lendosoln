﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;

namespace DataAccess
{
    public class SqlParamSerializer
    {
        private static string SqlParamsToString(SqlParameter sqlParam)
        {
            return sqlParam.ParameterName + ": " + (sqlParam.Value == null ? "[DBNULL]" : sqlParam.Value);
        }
        public static string ToString(IEnumerable<SqlParameter> sqlParams)
        {
            return string.Join(", ", sqlParams.Select(p => SqlParamsToString(p)).ToArray());
        }
    }
}
