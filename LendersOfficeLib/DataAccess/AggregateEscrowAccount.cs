using System;
using System.Xml;

using LendersOffice.Common;
namespace DataAccess
{
    /// <summary>
    /// Note:
    ///     Aggregate Escrow Account requires two items:
    ///         1) First Payment Date
    ///         2) Initial Escrow Account Screen.
    ///             Structure of Initial Escrow Account Screen. Use the following as default.
    ///             
    ///             #_months   Tax    Haz_Ins    Mtg_Ins    Flood_Ins   School_Tax    User_Define_1    User_Define_2    User_Define_3    User_Define_4
    ///             ----------------------------------------------------------------------------------------------------------------------------------
    ///             Cushion     2       2           1          2            2             2                  2                2                2
    ///             Jan                             1
    ///             Feb                             1
    ///             Mar                             1
    ///             Apr         6                   1
    ///             May                             1
    ///             Jun                12           1                                     12                12               12               12
    ///             Jul                             1
    ///             Aug                             1          12
    ///             Sep                             1
    ///             Oct                             1                       12
    ///             Nov         6                   1
    ///             Dec                             1
    ///             
    ///     Xml schema for sInitialEscrowAccXmlContent
    ///     <InitialEscrowAccXmlContent>
    ///         <item type="HazIns|MIns|SchoolTx|RealETx|FloodIns|1006Rsrv|1007Rsrv|U3Rsrv|U4Rsrv"
    ///               cushion="" jan="" feb="" mar="" apr="" may="" jun="" jul="" aug="" sep="" oct="" nov="" dec=""/>
    ///               
    ///     </InitialEscrowAccXmlContent>
    /// </summary>
	public class AggregateEscrowAccount
	{
        private const int ROW_CUSHION = 0;
        private const int ROW_JAN = 1;
        private const int ROW_FEB = 2;
        private const int ROW_MAR = 3;
        private const int ROW_APR = 4;
        private const int ROW_MAY = 5;
        private const int ROW_JUN = 6;
        private const int ROW_JUL = 7;
        private const int ROW_AUG = 8;
        private const int ROW_SEP = 9;
        private const int ROW_OCT = 10;
        private const int ROW_NOV = 11;
        private const int ROW_DEC = 12;
        protected const int COL_TAX = 0;
        protected const int COL_HAZ_INS = 1;
        protected const int COL_MTG_INS = 2;
        protected const int COL_FLOOD_INS = 3;
        protected const int COL_SCHOOL_TAX = 4;
        protected const int COL_USER_1 = 5;
        protected const int COL_USER_2 = 6;
        protected const int COL_USER_3 = 7;
        protected const int COL_USER_4 = 8;
        protected const int COL_AGGR_DISBURSEMENT = 9;
        protected const int COL_AGGR_BALANCE = 10;


        protected decimal[,] m_aggregateTable = new decimal[12, 11];
        protected AggregateEscrowItem[] m_aggregateItems = new AggregateEscrowItem[12];

        protected decimal m_initialDeposit = 0.0M;
        protected decimal m_aggregateEscrowAdjustment = 0.0M;

        protected CPageBase m_dataLoan;

        public decimal InitialDeposit 
        {
            get { return m_initialDeposit; }
        }
        public string InitialDeposit_rep 
        {
            get 
            {
                try { return m_dataLoan.m_convertLos.ToMoneyString(InitialDeposit, FormatDirection.ToRep); }
                catch { return ""; }
            }
        }
        public decimal AggregateEscrowAdjustment 
        {
            get { return m_aggregateEscrowAdjustment; }
        }
        public string AggregateEscrowAdjustment_rep 
        {
            get 
            { 
                try { return m_dataLoan.m_convertLos.ToMoneyString(AggregateEscrowAdjustment, FormatDirection.ToRep); }
                catch { return ""; }
            }
        }
        public AggregateEscrowItem[] AggregateItems 
        {
            get { return m_aggregateItems; }
        }

        protected AggregateEscrowAccount() { }

		public AggregateEscrowAccount(CPageBase dataLoan)
		{
            m_dataLoan = dataLoan;

            if (m_dataLoan.sSchedDueD1_rep == "") 
            {
                // return empty table.
                for (int i = 0; i < 12; i++)
                    m_aggregateItems[i] = new AggregateEscrowItem(dataLoan, -1);

                return;
            }
            FillAggregateTable();

		}
        private void Debug(int[,] table) 
        {
            for (int i = 0; i < 13; i++) 
            {
                string str = string.Format("i[{0,2}]=", i);
                for (int j = 0; j < 9; j++) 
                {
                    str += table[i,j].ToString();
                }
                Tools.LogError(str);
            }
        }

        /// <summary>
        /// Note if you update this, consider updating LFF sAggregateEscrowAccount and sSettlementAggregateEscrowAccount
        /// </summary>
        protected void FillAggregateTable() 
        {
            if (m_dataLoan.sSchedDueD1_rep == "")
                throw new CBaseException(ErrorMessages.AggrEscrow_FirstPaymentDateRequired, ErrorMessages.AggrEscrow_FirstPaymentDateRequired);

            // OPM 217071. Expensive checks outside loop, to avoid the
            // repeated gets for same value.  
            // 08/19/15. Update to cheap lazy load because apparently there is a condition
            // where pre-loading a field that is not needed will cause issues.
            System.Collections.Generic.Dictionary<string, Decimal> decimalCache = new System.Collections.Generic.Dictionary<string, Decimal>();
            Func<string, decimal> GetCacheDecimal = (field) =>
                {
                    decimal result;
                    if (decimalCache.TryGetValue(field, out result) == false)
                    {
                        switch (field)
                        {
                            case "sEscrowPmt": result = m_dataLoan.sEscrowPmt; break;
                            case "sProRealETx": result = m_dataLoan.sProRealETx; break;
                            case "sProHazIns": result = m_dataLoan.sProHazIns; break;
                            case "sProMIns": result = m_dataLoan.sProMIns; break;
                            case "sProFloodIns": result = m_dataLoan.sProFloodIns; break;
                            case "sProSchoolTx": result = m_dataLoan.sProSchoolTx; break;
                            case "s1006ProHExp": result = m_dataLoan.s1006ProHExp; break;
                            case "s1007ProHExp": result = m_dataLoan.s1007ProHExp; break;
                            case "sProU3Rsrv": result = m_dataLoan.sProU3Rsrv; break;
                            case "sProU4Rsrv": result = m_dataLoan.sProU4Rsrv; break;
                            default: throw new GenericUserErrorMessageException("Unhandled value: " + field);
                        }

                        decimalCache[field] = result;
                    }
                    return result;
                };

            System.Collections.Generic.Dictionary<string, E_TriState> triCache = new System.Collections.Generic.Dictionary<string, E_TriState>();
            Func<string, E_TriState> GetCacheTri = (field) =>
                {
                    E_TriState result;
                    if (triCache.TryGetValue(field, out result) == false)
                    {
                        switch (field)
                        {
                            case "sRealETxRsrvEscrowedTri": result = m_dataLoan.sRealETxRsrvEscrowedTri; break;
                            case "sMInsRsrvEscrowedTri": result = m_dataLoan.sMInsRsrvEscrowedTri; break;
                            case "sHazInsRsrvEscrowedTri": result = m_dataLoan.sHazInsRsrvEscrowedTri; break;
                            case "sFloodInsRsrvEscrowedTri": result = m_dataLoan.sFloodInsRsrvEscrowedTri; break;
                            case "sSchoolTxRsrvEscrowedTri": result = m_dataLoan.sSchoolTxRsrvEscrowedTri; break;
                            case "s1006RsrvEscrowedTri": result = m_dataLoan.s1006RsrvEscrowedTri; break;
                            case "s1007RsrvEscrowedTri": result = m_dataLoan.s1007RsrvEscrowedTri; break;
                            case "sU3RsrvEscrowedTri": result = m_dataLoan.sU3RsrvEscrowedTri; break;
                            case "sU4RsrvEscrowedTri": result = m_dataLoan.sU4RsrvEscrowedTri; break;
                            default: throw new GenericUserErrorMessageException("Unhandled value: " + field);
                        }

                        triCache[field] = result;
                    }
                    return result;
                };

            System.Collections.Generic.Dictionary<string, string> strCache = new System.Collections.Generic.Dictionary<string, string>();
            Func<string, string> GetCacheStr = (field) =>
                {
                    string result;
                    if (strCache.TryGetValue(field, out result) == false)
                    {
                        switch (field)
                        {
                            case "s1006ProHExpDesc": result = m_dataLoan.s1006ProHExpDesc; break;
                            case "s1007ProHExpDesc": result = m_dataLoan.s1007ProHExpDesc; break;
                            case "sU3RsrvDesc": result = m_dataLoan.sU3RsrvDesc; break;
                            case "sU4RsrvDesc": result = m_dataLoan.sU4RsrvDesc; break;
                            default: throw new GenericUserErrorMessageException("Unhandled value: " + field);
                        }

                        strCache[field] = result;
                    }
                    return result;
                };

            int[,] initialEscrowAcc = m_dataLoan.sInitialEscrowAcc;
            
            decimal sAggrEscrowCushionRequired = m_dataLoan.sAggrEscrowCushionRequired;


            int initialMonth = m_dataLoan.sSchedDueD1.DateTimeForComputation.Month;

            decimal previousBalance = 0.0M;
            decimal minimumBalance = 0.0M;
            for (int offset = 0; offset < 12; offset++) 
            {
                int currentMonth = (initialMonth + offset) % 13;
                if (initialMonth + offset > 12)
                    currentMonth = currentMonth + 1;
                m_aggregateItems[offset] = new AggregateEscrowItem(m_dataLoan, currentMonth);
                m_aggregateItems[offset].PmtToEscrow = GetCacheDecimal("sEscrowPmt"); 
                // updated for opm 180983
                m_aggregateTable[offset, COL_TAX] = ((E_TriState.Yes == GetCacheTri("sRealETxRsrvEscrowedTri")) ? GetCacheDecimal("sProRealETx") * initialEscrowAcc[currentMonth, COL_TAX] : 0);
                m_aggregateTable[offset, COL_HAZ_INS] = ((E_TriState.Yes == GetCacheTri("sHazInsRsrvEscrowedTri")) ? GetCacheDecimal("sProHazIns") * initialEscrowAcc[currentMonth, COL_HAZ_INS] : 0);
                m_aggregateTable[offset, COL_MTG_INS] = ((E_TriState.Yes == GetCacheTri("sMInsRsrvEscrowedTri")) ? GetCacheDecimal("sProMIns") * initialEscrowAcc[currentMonth, COL_MTG_INS] : 0);
                m_aggregateTable[offset, COL_FLOOD_INS] = ((E_TriState.Yes == GetCacheTri("sFloodInsRsrvEscrowedTri")) ? GetCacheDecimal("sProFloodIns") * initialEscrowAcc[currentMonth, COL_FLOOD_INS] : 0);
                m_aggregateTable[offset, COL_SCHOOL_TAX] = ((E_TriState.Yes == GetCacheTri("sSchoolTxRsrvEscrowedTri")) ? GetCacheDecimal("sProSchoolTx") * initialEscrowAcc[currentMonth, COL_SCHOOL_TAX] : 0);
                m_aggregateTable[offset, COL_USER_1] = ((E_TriState.Yes == GetCacheTri("s1006RsrvEscrowedTri")) ? GetCacheDecimal("s1006ProHExp") * initialEscrowAcc[currentMonth, COL_USER_1] : 0);
                m_aggregateTable[offset, COL_USER_2] = ((E_TriState.Yes == GetCacheTri("s1007RsrvEscrowedTri")) ? GetCacheDecimal("s1007ProHExp") * initialEscrowAcc[currentMonth, COL_USER_2] : 0);
                m_aggregateTable[offset, COL_USER_3] = ((E_TriState.Yes == GetCacheTri("sU3RsrvEscrowedTri")) ? GetCacheDecimal("sProU3Rsrv") * initialEscrowAcc[currentMonth, COL_USER_3] : 0);
                m_aggregateTable[offset, COL_USER_4] = ((E_TriState.Yes == GetCacheTri("sU4RsrvEscrowedTri")) ? GetCacheDecimal("sProU4Rsrv") * initialEscrowAcc[currentMonth, COL_USER_4] : 0);
                m_aggregateTable[offset, COL_AGGR_DISBURSEMENT] = m_aggregateTable[offset, COL_TAX] + m_aggregateTable[offset, COL_HAZ_INS]
                    + m_aggregateTable[offset, COL_MTG_INS] + m_aggregateTable[offset, COL_FLOOD_INS] + m_aggregateTable[offset, COL_SCHOOL_TAX]
                    + m_aggregateTable[offset, COL_USER_1] + m_aggregateTable[offset, COL_USER_2]
                    + m_aggregateTable[offset, COL_USER_3] + m_aggregateTable[offset, COL_USER_4];

                m_aggregateItems[offset].PmtFromEscrow = m_aggregateTable[offset, COL_AGGR_DISBURSEMENT];

                if (m_aggregateTable[offset, COL_TAX] > 0.0M)
                    m_aggregateItems[offset].Desc += "Property Taxes" + Environment.NewLine;
                if (m_aggregateTable[offset, COL_HAZ_INS] > 0.0M)
                    m_aggregateItems[offset].Desc += "Hazard Insurance" + Environment.NewLine;
                if (m_aggregateTable[offset, COL_MTG_INS] > 0.0M)
                    m_aggregateItems[offset].Desc += "Mortgage Insurance" + Environment.NewLine;
                if (m_aggregateTable[offset, COL_SCHOOL_TAX] > 0.0M)
                    m_aggregateItems[offset].Desc += "School Taxes" + Environment.NewLine;
                if (m_aggregateTable[offset, COL_FLOOD_INS] > 0.0M)
                    m_aggregateItems[offset].Desc += "Flood Insurance" + Environment.NewLine;
                if (m_aggregateTable[offset, COL_USER_1] > 0.0M) 
                {
                    if (GetCacheStr("s1006ProHExpDesc").TrimWhitespaceAndBOM() != "")
                        m_aggregateItems[offset].Desc += GetCacheStr("s1006ProHExpDesc") + Environment.NewLine;
                    else
                        m_aggregateItems[offset].Desc += "User Defined 1" + Environment.NewLine;
                }
                if (m_aggregateTable[offset, COL_USER_2] > 0.0M) 
                {
                    if (GetCacheStr("s1007ProHExpDesc").TrimWhitespaceAndBOM() != "")
                        m_aggregateItems[offset].Desc += GetCacheStr("s1007ProHExpDesc") + Environment.NewLine;
                    else
                        m_aggregateItems[offset].Desc += "User Defined 2" + Environment.NewLine;
                }
                if (m_aggregateTable[offset, COL_USER_3] > 0.0M) 
                {
                    if (GetCacheStr("sU3RsrvDesc").TrimWhitespaceAndBOM() != "")
                        m_aggregateItems[offset].Desc += GetCacheStr("sU3RsrvDesc") + Environment.NewLine;
                    else
                        m_aggregateItems[offset].Desc += "User Defined 3" + Environment.NewLine;
                }
                if (m_aggregateTable[offset, COL_USER_4] > 0.0M) 
                {
                    if (GetCacheStr("sU4RsrvDesc").TrimWhitespaceAndBOM() != "")
                        m_aggregateItems[offset].Desc += GetCacheStr("sU4RsrvDesc") + Environment.NewLine;
                    else
                        m_aggregateItems[offset].Desc += "User Defined 4" + Environment.NewLine;
                }

                previousBalance = previousBalance + GetCacheDecimal("sEscrowPmt") - m_aggregateTable[offset, COL_AGGR_DISBURSEMENT];
                m_aggregateTable[offset, COL_AGGR_BALANCE] = previousBalance;
                if (previousBalance < minimumBalance)
                    minimumBalance = previousBalance;

            }
            m_initialDeposit = sAggrEscrowCushionRequired - minimumBalance;
            SetAdjustment();


            // Shift up the balance by minimum balance.
            for (int i = 0; i < 12; i++) 
            {
                m_aggregateTable[i, COL_AGGR_BALANCE] += m_initialDeposit; 
                m_aggregateItems[i].Bal = m_aggregateTable[i, COL_AGGR_BALANCE];
            }
        }

        /// <summary>
        /// Note if you update this, consider updating LFF sAggregateEscrowAccount and AggregateEscrowAccount_Settlement.cs
        /// </summary>
        protected virtual void SetAdjustment()
        {
            decimal sTotLenderRsrv = m_dataLoan.sTotLenderRsrv;
            m_aggregateEscrowAdjustment = m_initialDeposit - sTotLenderRsrv;
        }
	}


}
