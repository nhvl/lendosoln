﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using EDocs;
using System.IO;
using LendersOffice.Common;
using PdfRasterizerLib;
using LendersOffice.Constants;
using iTextSharp.text.pdf;

namespace DataAccess
{
    public sealed class FileDBPdfFile
    {
        private string m_filedbkey;
        private byte[] m_pdfData;
        private iTextSharp.text.pdf.PdfReader m_pdfReader;

        public int PageCount
        {
            get { return m_pdfReader.NumberOfPages; }
        }

        public byte[] PdfBinaryData { get { return m_pdfData; } }

        public FileDBPdfFile(Guid key) 
        {
            Init(key.ToString());
        }
        /// <summary>
        /// Loads the given key from file db - expects it to be a pdf 
        /// </summary>
        /// <param name="key"></param>
        /// <exception cref="FileNotFoundException">Thrown when the key is not found</exception>
        public FileDBPdfFile(string key)
        {
            Init(key);
        }


        private void Init(string key)
        {
            m_filedbkey = key;
            LoadPdfDataFromFileDB();
        }

        /// <summary>
        /// Reads the file from file db
        /// </summary>
        /// <exception cref="FileNotFoundException"></exception>
        /// <exception cref="IOException"></exception>
        /// <exception cref="InsufficientPdfPermissionException"></exception>
        /// <exception cref="InvalidPDFFileException"></exception>
        private void LoadPdfDataFromFileDB()
        {
            m_pdfData = FileDBTools.ReadData(E_FileDB.EDMS,m_filedbkey);
            CreatePdfReader(m_pdfData);
        }

        public List<PdfPageItem> GetPdfPageInfoList()
        {
            // 5/3/2011 dd - I cannot use the FileDBTools.ReadData because this code need to be hotfix and FileDBTools class are new.
            // Using FileDB local file directly instead of load whole content to memory.
            iTextSharp.text.pdf.PdfReader pdfReader = new iTextSharp.text.pdf.PdfReader(m_pdfData);

            int pageCount = pdfReader.NumberOfPages;
            List<PdfPageItem> list = new List<PdfPageItem>(pageCount);
            for (int i = 1; i <= pageCount; i++)
            {
                var size = pdfReader.GetPageSizeWithRotation(i);
                list.Add(new PdfPageItem() { DocId = Guid.Empty, Page = i, Version = 0, Rotation = 0, PageHeight = (int)size.Height, PageWidth = (int)size.Width });

            }
            return list;


        }
        /// <summary>
        /// Todo migrate exceptions to be more common instead of edoc specific
        /// </summary>
        /// <param name="data"></param>
        private void CreatePdfReader(byte[] data)
        {
            try
            {
                m_pdfReader = new iTextSharp.text.pdf.PdfReader(data);
            }
            catch (IOException e)
            {
                if (e.Message.Contains("Bad user Password"))
                {
                    throw new InsufficientPdfPermissionException();
                }
                else if (e.Message.Contains("PDF header signature not found"))
                {
                    throw new InvalidPDFFileException();
                }
                throw new CBaseException(ErrorMessages.EDocs.InvalidPDF, e);
            }

            if (!m_pdfReader.IsOpenedWithFullPermissions)
            {
                throw new InsufficientPdfPermissionException();
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        private byte[] LoadPage(int page)
        {

            if (page <= 0 || page > m_pdfReader.NumberOfPages)
            {
                throw new PageOutOfRangeException();
            }

            // Write Single Page to buffer.
            var document = new iTextSharp.text.Document();
            using (var outputMemoryStream = new MemoryStream())
            {
                var pdfWriter = new iTextSharp.text.pdf.PdfCopy(document, outputMemoryStream);
                document.Open();

                pdfWriter.AddPage(pdfWriter.GetImportedPage(m_pdfReader, page));
                document.Close();

                return outputMemoryStream.ToArray();
            }
        }


        public void RotatePages(int degrees)
        {
            using (MemoryStream outputMemoryStream = new MemoryStream())
            {
                var document = new iTextSharp.text.Document();
                PdfCopy pdfWriter = new PdfCopy(document, outputMemoryStream);
                document.Open();
                
                for (var i = 1; i <= m_pdfReader.NumberOfPages; i++)
                {
                    int newRotation = m_pdfReader.GetPageRotation(i) + degrees;
                    m_pdfReader.GetPageN(i).Put(iTextSharp.text.pdf.PdfName.ROTATE, new iTextSharp.text.pdf.PdfNumber(newRotation));
                    pdfWriter.AddPage(pdfWriter.GetImportedPage(m_pdfReader, i));
                }
                document.Close();
                m_pdfData = outputMemoryStream.ToArray();
            }
        }

        /// <summary>
        /// Call this to delete the cache.
        /// </summary>
        public void DeleteCachedImages()
        {
            for (var i = 1; i <= m_pdfReader.NumberOfPages; i++)
            {
                var name = GetPngFileName(i);
                if (File.Exists(name))
                {
                    try
                    {
                        File.Delete(name);
                    }
                    catch (Exception e)
                    {
                        Tools.LogWarning("Could not delete temporary files after checking if they existed first.", e);
                    }
                }
            }
        }

        /// <summary>
        /// Call this to cache png files
        /// </summary>
        public void CachePNGFiles()
        {
            IPdfRasterizer rasterizer = PdfRasterizerFactory.Create(ConstSite.PdfRasterizerHost);
            List<byte[]> pngContentList = rasterizer.ConvertToPng(m_pdfData, ConstAppDavid.PdfPngScaling);

            if (null == pngContentList)
            {
                throw new InvalidPDFFileException();
            }

            for (int i = 1; i <= pngContentList.Count; i++)
            {
                string outputFileName = GetPngFileName(i);
                File.WriteAllBytes(outputFileName, pngContentList[i - 1]);
            }
        }
        
        /// <summary>
        /// Turns the PDF into png unless it already exist in temp files. If not it writes it to temp files.
        /// This caches! If you are making changes to the PDF somewhere else you will need add support for that.
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public byte[] GetPNG(int page)
        {
            if (false == File.Exists(GetPngFileName(page)))
            {
                CachePNGFiles();
            }

            return File.ReadAllBytes(GetPngFileName(page));

        }

        private string GetPngFileName(int page)
        {
            return string.Format("{0}\\{1}_{2}.png", ConstApp.TempFolder, m_filedbkey, page);
        }

    }
}
