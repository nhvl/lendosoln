﻿// <copyright file="OtherIncome.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Updater: Huy Nguyen 
//  Date:   5/12/16
// </summary>

namespace DataAccess
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using LendersOffice.UI;

    /// <summary>
    /// Represen an other income entry.
    /// </summary>
    public class OtherIncome
    {
        /// <summary>
        /// The predefined other income pairs. 
        /// </summary>
        private static readonly IReadOnlyDictionary<E_aOIDescT, string> PredefinedOtherIncomePairs = new Dictionary<E_aOIDescT, string>()
        {
            { E_aOIDescT.Other, "Other Income" },
            { E_aOIDescT.MilitaryBasePay, "Military Base Pay" },
            { E_aOIDescT.MilitaryRationsAllowance, "Military Rations Allowance" },
            { E_aOIDescT.MilitaryFlightPay, "Military Flight Pay" },
            { E_aOIDescT.MilitaryHazardPay, "Military Hazard Pay" },
            { E_aOIDescT.MilitaryClothesAllowance, "Military Clothes Allowance" },
            { E_aOIDescT.MilitaryQuartersAllowance, "Military Quarters Allowance" },
            { E_aOIDescT.MilitaryPropPay, "Military Prop Pay" },
            { E_aOIDescT.MilitaryOverseasPay, "Military Overseas Pay" },
            { E_aOIDescT.MilitaryCombatPay, "Military Combat Pay" },
            { E_aOIDescT.MilitaryVariableHousingAllowance, "Military Variable Housing Allowance" },
            { E_aOIDescT.AlimonyChildSupport, "Alimony/Child Support Income" },
            { E_aOIDescT.NotesReceivableInstallment, "Notes Receivable/Installment" },
            { E_aOIDescT.PensionRetirement, "Pension/Retirement Income" },
            { E_aOIDescT.SocialSecurityDisability, "Social Security/Disability Income" },
            { E_aOIDescT.RealEstateMortgageDifferential, "Real Estate, Mortgage Differential Income" },
            { E_aOIDescT.Trust, "Trust Income" },
            { E_aOIDescT.UnemploymentWelfare, "Unemployment/Welfare Income" },
            { E_aOIDescT.AutomobileExpenseAccount, "Automobile/Expense Account Income" },
            { E_aOIDescT.FosterCare, "Foster Care" },
            { E_aOIDescT.VABenefitsNonEducation, "VA Benefits (Non-education)" },
            { E_aOIDescT.SubjPropNetCashFlow, "Subject Property Net Cash Flow" },  // OPM 21208
            { E_aOIDescT.CapitalGains, "Capital Gains" },
            { E_aOIDescT.EmploymentRelatedAssets, "Employment Related Assets" },
            { E_aOIDescT.ForeignIncome, "Foreign Income" },
            { E_aOIDescT.RoyaltyPayment, "Royalty Payment" },
            { E_aOIDescT.SeasonalIncome, "Seasonal Income" },
            { E_aOIDescT.TemporaryLeave, "Temporary Leave" },
            { E_aOIDescT.TipIncome, "Tip Income" },
            { E_aOIDescT.BoarderIncome, "Boarder Income" },
            { E_aOIDescT.MortgageCreditCertificate, "Mortgage Credit Certificate (MCC)" },
            { E_aOIDescT.TrailingCoBorrowerIncome, "Trailing Co-borrower Income" },
            { E_aOIDescT.AccessoryUnitIncome, "Accessory Unit Income" },
            { E_aOIDescT.NonBorrowerHouseholdIncome, "Non-Borrower Household Income" },
            { E_aOIDescT.HousingChoiceVoucher, "Housing Choice Voucher (Sec 8)" },
            { E_aOIDescT.SocialSecurity, "Social Security" },
            { E_aOIDescT.Disability, "Disability" },
            { E_aOIDescT.Alimony, "Alimony" },
            { E_aOIDescT.ChildSupport, "Child Support" },
            { E_aOIDescT.ContractBasis, "Contract Basis" },
            { E_aOIDescT.DefinedContributionPlan, "Defined Contribution Plan" },
            { E_aOIDescT.HousingAllowance, "Housing Allowance" },
            { E_aOIDescT.MiscellaneousIncome, "Miscellaneous Income" },
            { E_aOIDescT.PublicAssistance, "Public Assistance" },
            { E_aOIDescT.WorkersCompensation, "Workers Compensation" }
        };

        /// <summary>
        /// List of options of IsForCoBorrowerString property.
        /// </summary>
        private static List<KeyValuePair<string, string>> isForCoBorrowerStringOptions = new[] 
        {
            new KeyValuePair<string, string>("B", null),
            new KeyValuePair<string, string>("C", null),
        }.ToList();

        /// <summary>
        /// Initializes a new instance of the <see cref="OtherIncome" /> class.
        /// </summary>
        public OtherIncome()
        {
        }

        /// <summary>
        /// Gets or sets the description of other income entry.
        /// </summary>
        /// <value>The  of other income entry.</value>
        [LqbInputModelAttribute(name: "desc")]
        public string Desc { get; set; }

        /// <summary>
        /// Gets or sets the name of other income entry.
        /// </summary>
        /// <value>The  of other income entry.</value>
        [LqbInputModelAttribute(name: "val")]
        public decimal Amount { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the other income is for borrower or coborrower.
        /// </summary>
        /// <value>The value indicating whether the other income is for borrower or coborrower.</value>
        [LqbInputModelAttribute(invalid: true)]
        public bool IsForCoBorrower { get; set; }

        /// <summary>
        /// Gets or sets the string value indicating whether the other income is for borrower or coborrower.
        /// </summary>
        /// <value>The string value indicating whether the other income is for borrower or coborrower.</value>
        [LqbInputModelAttribute(name: "isCo", type: InputFieldType.DropDownList)]
        [System.Xml.Serialization.XmlIgnore]
        public string IsForCoBorrowerString
        { 
            get
            {
                return this.IsForCoBorrower ? "C" : "B";
            }

            set
            {
                this.IsForCoBorrower = value == "C";
            }
        }

        /// <summary>
        /// Convert description string to E_aOIDescT enum.
        /// </summary>
        /// <param name="desc">Other income description.</param>
        /// <returns>Enum value of E_aOIDescT.</returns>
        public static E_aOIDescT Get_aOIDescT(string desc)
        {
            if (string.IsNullOrEmpty(desc))
            {
                return E_aOIDescT.Other;
            }

            KeyValuePair<E_aOIDescT, string> foundPair = PredefinedOtherIncomePairs.FirstOrDefault(pair => desc.Equals(pair.Value, System.StringComparison.OrdinalIgnoreCase));
            if (foundPair.Equals(default(KeyValuePair<E_aOIDescT, string>)))
            {
                return E_aOIDescT.Other;
            }

            return foundPair.Key;
        }

        /// <summary>
        /// Gets the default description from the enum type.
        /// </summary>
        /// <param name="descriptionType">The description type.</param>
        /// <returns>The default description based on type. Empty string if not found.</returns>
        public static string GetDescription(E_aOIDescT descriptionType)
        {
            string description;
            if (!PredefinedOtherIncomePairs.TryGetValue(descriptionType, out description))
            {
                return string.Empty;
            }

            return description;
        }

        /// <summary>
        /// The type-description mappings for predefined other incomes.
        /// </summary>
        /// <returns>The mappings.</returns>
        public static IReadOnlyDictionary<E_aOIDescT, string> GetTypeDescriptionMappings()
        {
            return PredefinedOtherIncomePairs;
        }     
    }
}
