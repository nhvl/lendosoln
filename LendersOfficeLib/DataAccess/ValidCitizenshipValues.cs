﻿namespace DataAccess
{
    using System.Collections.Generic;

    /// <summary>
    /// Represents valid citizenship values and the default for invalid values.
    /// </summary>
    public class ValidCitizenshipValues
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ValidCitizenshipValues"/> class.
        /// </summary>
        /// <param name="validValues">The valid values.</param>
        /// <param name="defaultValue">The default for invalid values.</param>
        public ValidCitizenshipValues(
            IEnumerable<E_aProdCitizenT> validValues,
            E_aProdCitizenT? defaultValue)
        {
            this.ValidValues = validValues;
            this.DefaultValue = defaultValue;
        }

        /// <summary>
        /// Gets the valid values.
        /// </summary>
        /// <value>The valid values.</value>
        public IEnumerable<E_aProdCitizenT> ValidValues { get; private set; }

        /// <summary>
        /// Gets the default value, if applicable.
        /// </summary>
        /// <value>The default value. Null if not applicable.</value>
        public E_aProdCitizenT? DefaultValue { get; private set; }
    }
}
