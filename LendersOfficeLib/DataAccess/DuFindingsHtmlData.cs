using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
	public class CDuFindingsHtmlData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CDuFindingsHtmlData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
	        list.Add("sDuFindingsHtml");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CDuFindingsHtmlData(Guid fileId) : base(fileId, "CDuFindingsHtmlData", s_selectProvider)
		{
		}
	}
}
