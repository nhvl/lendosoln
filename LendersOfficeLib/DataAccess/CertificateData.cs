using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
	public class CCertificateData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CCertificateData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
            DataAccessUtils.AddCertificateFields(list);
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CCertificateData(Guid fileId) : base(fileId, "CCertificateData", s_selectProvider)
		{
		}
	}

    public class CCertificateNoPermissionCheckData : CPageData
    {
        private static CSelectStatementProvider s_selectProvider;
	    
        static CCertificateNoPermissionCheckData()
        {
            StringList list = new StringList();
	        
	        #region Target Fields
            DataAccessUtils.AddCertificateFields(list);
            list.Add("sPmlCertXmlContent");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
        public CCertificateNoPermissionCheckData(Guid fileId) : base(fileId, "CCertificateNoPermissionCheckData", s_selectProvider)
        {
        }
        protected override Boolean m_enforceAccessControl
        {
            get { return false; }
        }
    }
}
