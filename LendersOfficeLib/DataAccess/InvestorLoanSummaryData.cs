﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.IO;
using System.Xml;

namespace DataAccess
{
    public class InvestorLoanSummaryData
    {
        public static string GenerateXml(Guid LoanId)
        {
            string bytesString = "";

            CPageData dataLoan = new CInvestorLoanSummaryData(LoanId);
            dataLoan.InitLoad();

            using (MemoryStream stream = new MemoryStream(5000))
            {
                XmlTextWriter xmlWriter = new XmlTextWriter(stream, Encoding.ASCII);
                xmlWriter.WriteProcessingInstruction("xml", "version=\"1.0\" standalone=\"yes\"");
                Generate(xmlWriter, dataLoan);
                xmlWriter.Flush();
                bytesString = System.Text.Encoding.ASCII.GetString(stream.GetBuffer(), 0, (int)stream.Position);
            }

            return bytesString;
        }

        public static void Generate(XmlWriter writer, CPageData dataLoan)
        {
            writer.WriteStartElement("investor_summary");
            writer.WriteElementString("Date", Tools.GetDateTimeNowString());
            CAgentFields loanOfficerAgent = dataLoan.sAgentCollection.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            writer.WriteElementString("E_AgentRoleT_LoanOfficer_Name", loanOfficerAgent.AgentName);
            writer.WriteElementString("sLNm", dataLoan.sLNm);

            writer.WriteStartElement("Borrowers");
            string primaryBorrower = string.Empty;
            for (int i = 0; i < dataLoan.nApps; i++)
            {
                CAppData dataApp = dataLoan.GetAppData(i);

                if (i == 0)
                {
                    primaryBorrower = dataApp.aBNm;
                }
               
                writer.WriteStartElement("application");
                if (dataApp.aBIsValidNameSsn)
                {
                    if (dataApp.aBIsPrimaryWageEarner)
                        primaryBorrower = dataApp.aBNm;

                    writer.WriteAttributeString("aBNm", dataApp.aBNm);
                    writer.WriteAttributeString("aBSsn", dataApp.aBSsn);
                    writer.WriteAttributeString("aBExperianScore", dataApp.aBExperianScore_rep);
                    writer.WriteAttributeString("aBTransUnionScore", dataApp.aBTransUnionScore_rep);
                    writer.WriteAttributeString("aBEquifaxScore", dataApp.aBEquifaxScore_rep);
                    writer.WriteAttributeString("aBCitizenDescription", dataApp.aBCitizenDescription);
                    writer.WriteAttributeString("aBIsSelfEmplmt", (dataApp.aBIsSelfEmplmt) ? "Yes" : "No");
                    writer.WriteAttributeString("aBTotI", dataApp.aBTotI_rep);

                    writer.WriteAttributeString("aBSecondHighestScoreAgency", dataApp.aBSecondHighestScoreAgency);
                }

                if (dataApp.aCIsValidNameSsn)
                {
                    if ( dataApp.aCIsPrimaryWageEarner)
                        primaryBorrower = dataApp.aCNm;
                    
                    writer.WriteAttributeString("aCNm", dataApp.aCNm);
                    writer.WriteAttributeString("aCSsn", dataApp.aCSsn);
                    writer.WriteAttributeString("aCExperianScore", dataApp.aCExperianScore_rep);
                    writer.WriteAttributeString("aCTransUnionScore", dataApp.aCTransUnionScore_rep);
                    writer.WriteAttributeString("aCEquifaxScore", dataApp.aCEquifaxScore_rep);
                    writer.WriteAttributeString("aCCitizenDescription", dataApp.aCCitizenDescription);
                    writer.WriteAttributeString("aCIsSelfEmplmt", (dataApp.aCIsSelfEmplmt) ? "Yes" : "No");
                    writer.WriteAttributeString("aCTotI", dataApp.aCTotI_rep);

                    writer.WriteAttributeString("aCSecondHighestScoreAgency", dataApp.aCSecondHighestScoreAgency);

                }

                writer.WriteEndElement(); // </application>
            }
            writer.WriteEndElement();// </Borrowers>

            // Untitled Section
            writer.WriteStartElement("auprocessing");
            writer.WriteElementString("PrimaryBorrower", primaryBorrower);
            writer.WriteElementString("RepresentativeScore", dataLoan.sCreditScoreType2Soft_rep);
            writer.WriteElementString("sHas1stTimeBuyer", dataLoan.sHas1stTimeBuyer? "Yes":"No");
            writer.WriteElementString("sProdHasHousingHistory", dataLoan.sProdHasHousingHistory ? "Yes" : "No");
            writer.WriteElementString("sProd3rdPartyUwResultT", Tools.GetProd3rdPartyUwResultTDescription(dataLoan.sProd3rdPartyUwResultT));
            writer.WriteElementString("sProdIncludeNormalProc", dataLoan.sProdIncludeNormalProc ? "Yes" : "No");
            writer.WriteElementString("sProdIncludeMyCommunityProc", dataLoan.sProdIncludeMyCommunityProc ? "Yes" : "No");
            writer.WriteElementString("sProdIncludeHomePossibleProc", dataLoan.sProdIncludeHomePossibleProc ? "Yes" : "No");
            writer.WriteElementString("sProdIncludeFHATotalProc", dataLoan.sProdIncludeFHATotalProc ? "Yes" : "No");
            writer.WriteElementString("sProdIncludeVAProc", dataLoan.sProdIncludeVAProc ? "Yes" : "No");
            writer.WriteElementString("sProdIsDuRefiPlus", dataLoan.sProdIsDuRefiPlus ? "Yes" : "No");
            writer.WriteElementString("sFreddieLoanId", dataLoan.sFreddieLoanId);
            writer.WriteElementString("sDuCaseId", dataLoan.sDuCaseId);
            writer.WriteEndElement(); // </auprocessing>


            //Income, Assets, and Expensese
            writer.WriteStartElement("Assets");
            writer.WriteElementString("sProdDocT", dataLoan.sProdDocT_rep);
            writer.WriteElementString("sLTotI", dataLoan.sLTotI_rep);
            writer.WriteElementString("sProThisMPmt", dataLoan.sProThisMPmt_rep);
            writer.WriteElementString("sMonthlyPmt", dataLoan.sMonthlyPmt_rep);
            writer.WriteElementString("sPresLTotHExp", dataLoan.sPresLTotHExp_rep);

            writer.WriteElementString("sTransmOMonPmt", dataLoan.sTransmOMonPmt_rep);
            writer.WriteElementString("sTotalFixedPmt", dataLoan.sTotalScoreTotalFixedPayment_rep);
            writer.WriteElementString("sQualTopR", dataLoan.sQualTopR_rep);
            writer.WriteElementString("sQualBottomR", dataLoan.sQualBottomR_rep);
            writer.WriteElementString("sProdAvailReserveMonths", dataLoan.sProdAvailReserveMonths_rep);
            writer.WriteEndElement(); // </Assets>


            // PropertyInfo
            writer.WriteStartElement("PropertyInfo");
            writer.WriteElementString("purpose", GetPropertyPurposeDescription(dataLoan.sOccT));
            writer.WriteElementString("sPurchPrice", dataLoan.sPurchPrice_rep);
            writer.WriteElementString("sApprVal", dataLoan.sApprVal_rep);
            writer.WriteElementString("sProRealETx", dataLoan.sProRealETx_rep);
            writer.WriteElementString("sProOHExp", dataLoan.sProOHExp_rep);
            writer.WriteElementString("sOccR", dataLoan.sOccR_rep);
            writer.WriteElementString("sSpGrossRent", dataLoan.sSpGrossRent_rep);

            writer.WriteElementString("sProdSpT", dataLoan.sProdSpT_rep);
            writer.WriteElementString("sProdSpStructureT", Tools.GetStructureTypeDescription(dataLoan.sProdSpStructureT));
            writer.WriteElementString("sProdIsSpInRuralArea", dataLoan.sProdIsSpInRuralArea ? "Yes" : "No");
            writer.WriteElementString("sProdIsCondotel", dataLoan.sProdIsCondotel ? "Yes" : "No");
            writer.WriteElementString("sProdIsNonwarrantableProj", dataLoan.sProdIsNonwarrantableProj ? "Yes" : "No");
            writer.WriteElementString("address", dataLoan.sSpAddr);
            writer.WriteElementString("citystatezip", Tools.CombineCityStateZip(dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
            writer.WriteElementString("county", dataLoan.sSpCounty);
            writer.WriteEndElement(); // </PropertyInfo>


            // LoanInfo
            writer.WriteStartElement("LoanInfo");

            writer.WriteElementString("sLAmtCalc", dataLoan.sLAmtCalc_rep);
            writer.WriteElementString("sFfUfmipFinanced", dataLoan.sFfUfmipFinanced_rep);
            writer.WriteElementString("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            writer.WriteElementString("sLtvR", dataLoan.sLtvR_rep);
            writer.WriteElementString("sCltvR", dataLoan.sCltvR_rep);
            writer.WriteElementString("sNoteIR", dataLoan.sNoteIR_rep);
            writer.WriteElementString("sLPurposeT", dataLoan.sLPurposeT_rep);
            writer.WriteElementString("sProdCashoutAmt", dataLoan.sProdCashoutAmt_rep);
            writer.WriteElementString("sProdImpound", dataLoan.sProdImpound ? "Yes" : "No");
            writer.WriteElementString("sProdMIOptionT", GetMortgageInsuranceDescription(dataLoan.sProdMIOptionT));
            
            string sPpmtPenaltyMon = dataLoan.sProdPpmtPenaltyMon_rep;
            string prepayment_penalty = "";
            switch (sPpmtPenaltyMon) 
            {
                case "0": prepayment_penalty = "No Prepay"; break;
                case "12": prepayment_penalty = "1 year"; break;
                case "24": prepayment_penalty = "2 years"; break;
                case "36": prepayment_penalty = "3 years"; break;
                case "48": prepayment_penalty = "4 years"; break;
                case "60": prepayment_penalty = "5 years"; break;
            }
            writer.WriteElementString("sPpmtPenaltyMon", prepayment_penalty);


            writer.WriteElementString("sTerm", dataLoan.sTerm_rep);
            writer.WriteElementString("sDue", dataLoan.sDue_rep);
            writer.WriteElementString("sLienPosT", GetLienPositionDescription(dataLoan.sLienPosT));
            writer.WriteElementString("sFinMethT", GetFinanceMethodDescription(dataLoan.sFinMethT));
            writer.WriteElementString("pmt_type", dataLoan.sIOnlyMon > 0 ? "Interest Only" : "Principal & Interest");
            //writer.WriteElementString("slpArmIndex", "");
            writer.WriteElementString("sRAdj1stCapMon", dataLoan.sRAdj1stCapMon_rep);
            writer.WriteElementString("arm_caps", string.Format( "{0} / {1} / {2}",  dataLoan.sRAdj1stCapR_rep, dataLoan.sRAdjCapR_rep, dataLoan.sRAdjLifeCapR_rep ));
            writer.WriteElementString("sRAdjMarginR", dataLoan.sRAdjMarginR_rep);
            writer.WriteEndElement(); // </LoanInfo>

            writer.WriteStartElement("BrokerLockInfo");
            writer.WriteElementString("sInvestorLockLpTemplateNm", dataLoan.sInvestorLockLpTemplateNm);
            writer.WriteElementString("sInvestorLockLpInvestorNm", dataLoan.sInvestorLockLpInvestorNm);
            writer.WriteElementString("sLpProdCode", dataLoan.sLpProdCode);
            writer.WriteElementString("sStatusT", dataLoan.sStatusT_rep);
            writer.WriteElementString("sRateLockStatusT", dataLoan.sRateLockStatusT_rep);
            writer.WriteElementString("sInvestorLockRateLockStatusT", dataLoan.sInvestorLockRateLockStatusT_rep);
            writer.WriteElementString("sRLckdD", dataLoan.sRLckdD_rep) ;
            writer.WriteElementString("sRLckdDays", dataLoan.sRLckdDays_rep);
            writer.WriteElementString("sInvestorLockLockBuffer", dataLoan.sInvestorLockLockBuffer_rep);
            writer.WriteElementString("sRLckdExpiredD", dataLoan.sRLckdExpiredD_rep);
            writer.WriteEndElement(); // </BrokerLockInfo>

            writer.WriteEndElement(); // </investor_summary>


        }


        private static string GetLienPositionDescription(E_sLienPosT sLienPosT)
        {
            switch (sLienPosT)
            {
                case E_sLienPosT.First: return "First Lien";
                case E_sLienPosT.Second: return "Second Lien";
            }
            return "";
        }


        private static string GetMortgageInsuranceDescription(E_sProdMIOptionT sProdMIOptionT)
        {
            switch (sProdMIOptionT)
            {
                case E_sProdMIOptionT.BorrowerPdPmi:
                    return "Borrower Paid";
                case E_sProdMIOptionT.NoPmi:
                    return "No MI / LPMI / Other";
                case E_sProdMIOptionT.LeaveBlank:
                    return "N/A";
            }
            return "";
        }

        private static string GetPropertyPurposeDescription(E_sOccT sOccT)
        {
            switch (sOccT)
            {
                case E_sOccT.Investment: return "Investment";
                case E_sOccT.PrimaryResidence: return "Primary Residence";
                case E_sOccT.SecondaryResidence: return "Secondary Residence";
            }
            return "";
        }

        private static string GetFinanceMethodDescription(E_sFinMethT sFinMethT)
        {
            switch (sFinMethT)
            {
                case E_sFinMethT.Fixed:
                    return "Fixed";
                case E_sFinMethT.ARM:
                    return "ARM";
                case E_sFinMethT.Graduated:
                    return "Graduated";
            }
            return "";
        }

    }
}