﻿namespace DataAccess.FannieMae
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LendersOffice.Common;

    /// <summary>
    /// Represents a list of Fannie Mae Third Party Data Providers for the Desktop Underwriter Day 1 Certainty initiative, 
    ///     each matched with a reference number tying the provider to the application or loan file.
    /// </summary>
    public class FannieMaeThirdPartyProviderList : IEquatable<FannieMaeThirdPartyProviderList>
    {
        /// <summary>
        /// The internal representation of the provider list.
        /// </summary>
        private List<FannieMaeThirdPartyProvider> providers = new List<FannieMaeThirdPartyProvider>();

        /// <summary>
        /// Initializes a new instance of the <see cref="FannieMaeThirdPartyProviderList"/> class with an empty list of providers.
        /// </summary>
        public FannieMaeThirdPartyProviderList()
        {
            this.IsDirty = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FannieMaeThirdPartyProviderList"/> class, using the given list of providers.
        /// </summary>
        /// <param name="list">The list of providers to use.</param>
        public FannieMaeThirdPartyProviderList(IEnumerable<FannieMaeThirdPartyProvider> list)
        {
            this.providers = list.ToList();
            this.IsDirty = true;
        }

        /// <summary>
        /// Gets a value indicating whether the contents of the third party provider list are different from the data store. 
        /// For determining whether the list should be written to long-term storage on loan file save.
        /// </summary>
        /// <value>False when first initialized from long-term storage, true after any changes are made to the underlying list or if initialized from another source.</value>
        public bool IsDirty
        {
            get; private set;
        }

        /// <summary>
        /// Parses a JSON-formatted list of providers into this <see cref="FannieMaeThirdPartyProviderList"/>. Replaces the internal provider list.
        /// </summary>
        /// <param name="json">A JSON-formatted list of providers.</param>
        /// <param name="fromStorage">Whether the json came from long-term storage like FileDB, for determining whether the list should be written on save or can be skipped.</param>
        /// <returns>A new instance of the <see cref="FannieMaeThirdPartyProviderList"/> class.</returns>
        public static FannieMaeThirdPartyProviderList ParseFromJson(string json, bool fromStorage = false)
        {
            var newList = new FannieMaeThirdPartyProviderList();
            newList.AddProvidersWithCustomDuplicateLogic(
                newProviders: SerializationHelper.JsonNetDeserialize<List<FannieMaeThirdPartyProvider>>(json),
                removeDuplicates: p => Enumerable.Empty<FannieMaeThirdPartyProvider>()); // the UI and storage won't remove duplicates
            newList.IsDirty = !fromStorage;
            return newList;
        }

        /// <summary>
        /// Adds a list of Fannie Mae Third Party Providers to the internal list of providers.
        /// </summary>
        /// <param name="newProviders">An enumerable of <see cref="FannieMaeThirdPartyProvider"/> to add to the internal list of providers.</param>
        /// <returns>The modified <see cref="FannieMaeThirdPartyProviderList"/> object.</returns>
        public FannieMaeThirdPartyProviderList AddProviders(IEnumerable<FannieMaeThirdPartyProvider> newProviders)
        {
            return this.AddProvidersWithCustomDuplicateLogic(newProviders, this.RemoveDuplicateReferenceNumbers);
        }

        /// <summary>
        /// Removes the specified provider from the list.
        /// </summary>
        /// <param name="providerToRemove">The provider to remove.</param>
        public void RemoveProvider(FannieMaeThirdPartyProvider providerToRemove)
        {
            this.IsDirty |= this.providers.Remove(providerToRemove);
        }

        /// <summary>
        /// Returns a copy of the internal providers list.
        /// </summary>
        /// <returns>A copy of the internal providers list.</returns>
        public List<FannieMaeThirdPartyProvider> GetProviders()
        {
            return new List<FannieMaeThirdPartyProvider>(this.providers);
        }

        /// <summary>
        /// Serializes the internal provider list into JSON format for use in the UI or storage in a text file.
        /// </summary>
        /// <returns>A JSON-formatted list of providers.</returns>
        public string SerializeJson()
        {
            return SerializationHelper.JsonNetSerialize(this.providers);
        }

        /// <summary>
        /// Sets a list of Fannie Mae Third Party Providers to the internal list of providers.
        /// </summary>
        /// <param name="newProviders">An enumerable of <see cref="FannieMaeThirdPartyProvider"/> to set as the internal list of providers.</param>
        public void SetProviders(IEnumerable<FannieMaeThirdPartyProvider> newProviders)
        {
            this.providers = newProviders.Where(provider => !string.IsNullOrEmpty(provider.Name) && !string.IsNullOrEmpty(provider.ReferenceNumber)).ToList();
            this.IsDirty = true;
        }

        /// <summary>
        /// Determines whether the provider list of this object and another provider list are equal.
        /// </summary>
        /// <param name="other">The other Fannie Mae Third-Party Provider List.</param>
        /// <returns>True if for each index in the list, both lists have equal providers in that index. False otherwise.</returns>
        public bool Equals(FannieMaeThirdPartyProviderList other)
        {
            if (this == other)
            {
                return true;
            }

            if (this.providers.Count != other.providers.Count)
            {
                return false;
            }

            for (int i = 0; i < this.providers.Count; i++)
            {
                if (!this.providers[i].Equals(other.providers[i]))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Compares equality of this object and another.
        /// </summary>
        /// <param name="obj">The other object to compare.</param>
        /// <returns>Whether this object is equal to the other object.</returns>
        public override bool Equals(object obj)
        {
            var other = obj as FannieMaeThirdPartyProviderList;
            return other != null && this.Equals(other);
        }

        /// <summary>
        /// Objects of this class should not need to be put into a hashing structure.
        /// Overriding to prevent the "overrides Equals but does not override GetHashCode" error.
        /// </summary>
        /// <returns>The default object implementation for this object's hash code.</returns>
        public override int GetHashCode()
        {
            return 0; // This object is mutable
        }

        /// <summary>
        /// Gets a list of the duplicated reference numbers from a list of existing providers and a new provider.
        /// </summary>
        /// <param name="existingProviders">The existing providers, which may contain duplicates of <paramref name="newProvider"/>.</param>
        /// <param name="newProvider">The new provider to check against for duplicates.</param>
        /// <returns>A list of reference numbers which are duplicated between <paramref name="existingProviders"/> and <paramref name="newProvider"/>.</returns>
        private static List<FannieMaeThirdPartyProvider> GetDuplicateReferenceNumbers(IEnumerable<FannieMaeThirdPartyProvider> existingProviders, FannieMaeThirdPartyProvider newProvider)
        {
            var duplicates = new List<FannieMaeThirdPartyProvider>();
            foreach (FannieMaeThirdPartyProvider existingProvider in existingProviders)
            {
                if (StringComparer.OrdinalIgnoreCase.Equals(existingProvider.Name, newProvider.Name)
                    && (GetFirstNineDigits(existingProvider.ReferenceNumber) == GetFirstNineDigits(newProvider.ReferenceNumber)
                    || StringComparer.OrdinalIgnoreCase.Equals(newProvider.Name, "DataVerify")))
                {
                    duplicates.Add(existingProvider);
                }
            }

            return duplicates;
        }

        /// <summary>
        /// Gets the first nine digits of the specified string.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns>The first nine digits found in the string, or all the digits in the string if fewer than 9 are found.</returns>
        private static string GetFirstNineDigits(string str)
        {
            const int DigitCount = 9;
            var result = new char[DigitCount];
            int index = 0;
            foreach (char c in str ?? string.Empty)
            {
                if (char.IsDigit(c))
                {
                    result[index++] = c;
                    if (index == DigitCount)
                    {
                        break;
                    }
                }
            }

            return new string(result, 0, index);
        }

        /// <summary>
        /// Adds a list of Fannie Mae Third Party Providers to the internal list of providers.
        /// </summary>
        /// <param name="newProviders">An enumerable of <see cref="FannieMaeThirdPartyProvider"/> to add to the internal list of providers.</param>
        /// <param name="removeDuplicates">The custom duplicate removal logic, which is normally <see cref="RemoveDuplicateReferenceNumbers(FannieMaeThirdPartyProvider)"/>.</param>
        /// <returns>The modified <see cref="FannieMaeThirdPartyProviderList"/> object.</returns>
        private FannieMaeThirdPartyProviderList AddProvidersWithCustomDuplicateLogic(IEnumerable<FannieMaeThirdPartyProvider> newProviders, Func<FannieMaeThirdPartyProvider, IEnumerable<FannieMaeThirdPartyProvider>> removeDuplicates)
        {
            if (newProviders == null)
            {
                return this;
            }

            IEnumerable<FannieMaeThirdPartyProvider> providersToAdd = newProviders.Where(provider => !string.IsNullOrEmpty(provider.Name) && !string.IsNullOrEmpty(provider.ReferenceNumber)).Except(this.providers);
            var providersRemoved = new List<FannieMaeThirdPartyProvider>();
            foreach (FannieMaeThirdPartyProvider provider in providersToAdd)
            {
                providersRemoved.AddRange(removeDuplicates(provider));
                this.providers.Add(provider);
                this.IsDirty = true;
            }

            return this;
        }

        /// <summary>
        /// Removes the duplicate reference numbers from the list.
        /// </summary>
        /// <param name="newProvider">The provider that will be added, which <see cref="providers"/> will be checked against.</param>
        /// <returns>The list of duplicates that were removed.</returns>
        private List<FannieMaeThirdPartyProvider> RemoveDuplicateReferenceNumbers(FannieMaeThirdPartyProvider newProvider)
        {
            var duplicates = GetDuplicateReferenceNumbers(this.providers, newProvider);
            foreach (FannieMaeThirdPartyProvider duplicate in duplicates)
            {
                this.RemoveProvider(duplicate);
            }

            return duplicates;
        }
    }
}
