﻿namespace DataAccess.FannieMae
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;

    /// <summary>
    /// Represents the list of approved Third Party Data/Service Providers for the DU system.
    /// This is a system-wide singleton and should correspond to the official list of accepted Service Providers published by Fannie Mae.
    /// </summary>
    public class DuServiceProviders
    {
        /// <summary>
        /// The lazy instance of the DU Service Providers list, for implementing the Singleton pattern.
        /// </summary>
        private static Lazy<DuServiceProviders> instanceLazy = new Lazy<DuServiceProviders>(() => new DuServiceProviders());

        /// <summary>
        /// Prevents a default instance of the <see cref="DuServiceProviders" /> class from being created.
        /// Instead, use <see cref="DuServiceProviders.Instance"/> to retrieve the Singleton.
        /// </summary>
        private DuServiceProviders()
        {
            this.Reload();
        }

        /// <summary>
        /// Gets the single instance of this singleton list class.
        /// </summary>
        /// <value>
        /// The singleton instance.
        /// </value>
        public static DuServiceProviders Instance
        {
            get
            {
                return instanceLazy.Value;
            }
        }

        /// <summary>
        /// Gets the list of approved DU Service Providers, as stored in the database.
        /// This is read-only for now. If changes need to be made to the list, make a DB query.
        /// </summary>
        /// <value>
        /// A read-only list of DU Service Providers.
        /// </value>
        public IReadOnlyList<DuServiceProvider> Providers { get; private set; }

        /// <summary>
        /// Retrieves the instance corresponding to the specified <see cref="DuServiceProvider.ProviderName"/>.
        /// </summary>
        /// <param name="name">The <see cref="DuServiceProvider.ProviderName"/> to look for.</param>
        /// <returns>The matching instance, if it exists; null otherwise.</returns>
        public static DuServiceProvider? RetrieveByProviderName(string name)
        {
            foreach (var serviceProvider in Instance.Providers)
            {
                if (StringComparer.OrdinalIgnoreCase.Equals(name, serviceProvider.ProviderName)
                    || StringComparer.OrdinalIgnoreCase.Equals(name, serviceProvider.DisplayName))
                {
                    return serviceProvider;
                }
            }

            return null;
        }

        /// <summary>
        /// Re-loads the list of service providers from the database.
        /// </summary>
        public void Reload()
        {
            this.Providers = StoredProcedureHelper.ExecuteReaderAndGetResults(
                record => new DuServiceProvider((string)record["ProviderName"], (string)record["DisplayName"], (int)record["ProviderId"]),
                DbConnectionInfo.DefaultConnectionInfo,
                "DU_THIRD_PARTY_PROVIDERS_ListAll",
                new SqlParameter[] { }).ToList().AsReadOnly();
        }

        /// <summary>
        /// Represents a DU Third Party Service Provider.
        /// </summary>
        public struct DuServiceProvider
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="DuServiceProvider" /> struct.
            /// </summary>
            /// <param name="providerName">Name of the provider.</param>
            /// <param name="displayName">Name of the provider for display purposes.</param>
            /// <param name="id">The provider id.</param>
            /// <remarks>
            /// This should not be called except by the <see cref="DuServiceProviders.Reload"/> method.
            /// </remarks>
            internal DuServiceProvider(string providerName, string displayName, int id)
            {
                this.ProviderName = providerName;
                this.DisplayName = displayName;
                this.ProviderId = id;
            }

            /// <summary>
            /// Gets the name of the service provider for displaying to users.
            /// </summary>
            /// <value>
            /// Display name of the service provider.
            /// </value>
            public string DisplayName { get; }

            /// <summary>
            /// Gets the internal name of the provider for sending in FNMA 3.2 format.
            /// </summary>
            /// <value>
            /// FNMA-recognized name of the service provider.
            /// </value>
            public string ProviderName { get; }

            /// <summary>
            /// Gets the id for this DuServiceProvider.
            /// </summary>
            /// <value>Id for DuServiceProvider.</value>
            public int ProviderId { get; }

            /// <summary>
            /// Creates a new instance of the <see cref="DuServiceProvider" /> struct for testing.  Do not use this for anything else.
            /// </summary>
            /// <param name="providerName">Name of the provider.</param>
            /// <param name="displayName">Name of the provider for display purposes.</param>
            /// <param name="id">The provider id.</param>
            /// <returns>A new instance of the <see cref="DuServiceProvider" /> struct.</returns>
            public static DuServiceProvider CreateTestInstance(string providerName, string displayName, int id)
            {
                return new DuServiceProvider(providerName, displayName, id);
            }
        }
    }
}
