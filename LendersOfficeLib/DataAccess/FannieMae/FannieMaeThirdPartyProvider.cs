﻿namespace DataAccess.FannieMae
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents a Fannie Mae-approved 3rd-party data provider for the Desktop Underwriter 1-day certainty initiative.
    /// This object represents the provider as it relates to the loan. The <see cref="ReferenceNumber"/> is meant to correspond to the individual application/loan.
    /// </summary>
    [DataContract]
    public class FannieMaeThirdPartyProvider : IEquatable<FannieMaeThirdPartyProvider>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FannieMaeThirdPartyProvider"/> class.
        /// </summary>
        /// <param name="serviceProvider">The service provider.</param>
        /// <param name="referenceNumber">The reference number linking the service provider to the loan application.</param>
        public FannieMaeThirdPartyProvider(DuServiceProviders.DuServiceProvider serviceProvider, string referenceNumber)
        {
            this.Name = serviceProvider.ProviderName;
            this.ReferenceNumber = referenceNumber;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FannieMaeThirdPartyProvider"/> class.
        /// </summary>
        /// <param name="name">The Fannie Mae-recognized name of the service provider.</param>
        /// <param name="referenceNumber">The reference number linking the service provider to the loan application.</param>
        /// <remarks>
        /// This constructor is used only for serialization.  While I'd like to remove the dependency on Json.Net, there
        /// isn't an obvious alternative.  This constructor is also validating its inputs against <see cref="DuServiceProviders"/>,
        /// as this is how we currently set data from the UI, and our application behavior isn't well defined for removed service
        /// providers.  Thus, it will be very difficult (though not technically impossible) to create an invalid instance of this type.
        /// </remarks>
        [Newtonsoft.Json.JsonConstructor]
        private FannieMaeThirdPartyProvider(string name, string referenceNumber)
            : this(DuServiceProviders.RetrieveByProviderName(name).Value, referenceNumber)
        {
        }

        /// <summary>
        /// Gets the name of the provider, as recognized by Fannie Mae. Corresponds to <see cref="DuServiceProviders.DuServiceProvider.ProviderName"/>.
        /// </summary>
        /// <value>The name of the provider.</value>
        [DataMember]
        public string Name { get; }

        /// <summary>
        /// Gets the Reference number for exporting to DU.
        /// </summary>
        /// <value>The reference number.</value>
        [DataMember]
        public string ReferenceNumber { get; }

        /// <summary>
        /// Determines whether this instance is equal to another instance of the <see cref="FannieMaeThirdPartyProvider"/> class.
        /// </summary>
        /// <param name="other">The other instance to check equality against.</param>
        /// <returns>Whether the two objects are equal.</returns>
        public bool Equals(FannieMaeThirdPartyProvider other)
        {
            if (other == null)
            {
                return false;
            }

            return this.Name == other.Name && this.ReferenceNumber == other.ReferenceNumber;
        }

        /// <summary>
        /// Overrides the Equality operator.
        /// </summary>
        /// <param name="obj">The other object to compare against.</param>
        /// <returns>Whether this is equal to the other object.</returns>
        public override bool Equals(object obj)
        {
            var other = obj as FannieMaeThirdPartyProvider;

            return this.Equals(other);
        }

        /// <summary>
        /// Gets a hash code of the object.
        /// </summary>
        /// <returns>A hash code for the object.</returns>
        public override int GetHashCode()
        {
            return (this.Name + this.ReferenceNumber).GetHashCode();
        }
    }
}
