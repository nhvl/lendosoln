using System;
using System.Collections;

namespace DataAccess
{
	/// <summary>
	/// Use this class to retrieve loan data for boiler plate
	/// loan event statement generation.
	/// </summary>

	public class LoanEventData : CPageData
	{
		private static CSelectStatementProvider s_selectProvider;

		protected override bool m_enforceAccessControl
		{
			// 2/15/2005 kb - Access control isn't needed for populating
			// loan event notifications with details.

			get
			{
				return false;
			}
		}

		static LoanEventData()
		{
			StringList list = new StringList();

            list.Add("sBranchId");
			list.Add( "sEmployeeLenderAccExec");
			list.Add( "sEmployeeLoanRep");
			list.Add( "sStatusT");
			list.Add( "sOpenedD");
			list.Add( "sLNm");
			list.Add( "sPrimBorrowerFullNm");
			list.Add( "sLinkedLoanInfo");
            list.Add("sAgentCollection");
            list.Add("sStatusT");
            list.Add("sfGetAgentRecordCount");

			s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

		}

		public LoanEventData( Guid fileId ) : base( fileId , "Loan Event Data" , s_selectProvider )
		{
		}

	}
	
}