/// Author: David Dao

using System;
using System.Data;
namespace DataAccess
{
    public class CPreparerFieldsForImport : CPreparerFields 
    {
        public CPreparerFieldsForImport( CPageBase parent, DataSet ds, int iRow ) : base( parent, ds, iRow, null )
        {
        }

        protected override void SetString(string fieldName, string value) 
        {
            if (IsValid) 
            {
                if (null == value)
                    value = "";

                string oldValue = CurrentRow[fieldName].ToString();
                string uniqueFieldName = GetUniqueFieldName(fieldName);

                if (this.Parent.IsRequireFieldSetForXmlRecord(uniqueFieldName, oldValue, value) )
                {
                    CurrentRow[fieldName] = value;
                }

            }
        }
    }
}
