using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
	public class CLoanSubmissionData : CPageData
	{
		private static CSelectStatementProvider s_selectProvider;
		static CLoanSubmissionData()
		{
			StringList list = new StringList();

            list.Add("sIsRateLocked");
            list.Add( "aBFirstNm" );
            list.Add( "aBLastNm" );
            list.Add( "aBMidNm" );
            list.Add( "aBSsn" );
            list.Add( "aBSuffix" );
            list.Add( "aCFirstNm" );
            list.Add( "aCLastNm" );
            list.Add( "aCMidNm" );
            list.Add( "aCSsn" );
            list.Add( "aCSuffix" );
            list.Add( "aOccT" );
            list.Add("sfGetAgentOfRole");
            list.Add( "sApprF" );
            list.Add( "sApprVal" );
            list.Add( "sCltvR" );
            list.Add( "sCrF" );
            list.Add( "sDemandApprFeeBrokerDueAmt" );
            list.Add( "sDemandApprFeeBrokerPaidAmt" );
            list.Add( "sDemandApprFeeLenderAmt" );
            list.Add( "sDemandCreditReportFeeBrokerDueAmt" );
            list.Add( "sDemandCreditReportFeeBrokerPaidAmt" );
            list.Add( "sDemandCreditReportFeeLenderAmt" );
            list.Add( "sDemandLDiscountBrokerAmt" );
            list.Add( "sDemandLDiscountLenderAmt" );
            list.Add( "sDemandLDiscountLenderMb" );
            list.Add( "sDemandLDiscountLenderPc" );
            list.Add( "sDemandLoanDocFeeBorrowerAmt" );
            list.Add( "sDemandLoanDocFeeBrokerDueAmt" );
            list.Add( "sDemandLoanDocFeeBrokerPaidAmt" );
            list.Add( "sDemandLoanDocFeeLenderAmt" );
            list.Add( "sDemandLOrigFeeBrokerAmt" );
            list.Add( "sDemandLOrigFeeLenderAmt" );
            list.Add( "sDemandLOrigFeeLenderMb" );
            list.Add( "sDemandLOrigFeeLenderPc" );
            list.Add( "sDemandNotes" );
            list.Add( "sDemandProcessingFeeBrokerDueAmt" );
            list.Add( "sDemandProcessingFeeBrokerPaidAmt" );
            list.Add( "sDemandProcessingFeeLenderAmt" );
            list.Add( "sDemandTotalBorrowerAmt" );
            list.Add( "sDemandTotalBrokerDueAmt" );
            list.Add( "sDemandTotalBrokerPaidAmt" );
            list.Add( "sDemandTotalLenderAmt" );
            list.Add( "sDemandU1BorrowerAmt" );
            list.Add( "sDemandU1BrokerDueAmt" );
            list.Add( "sDemandU1BrokerPaidAmt" );
            list.Add( "sDemandU1LenderAmt" );
            list.Add( "sDemandU1LenderDesc" );
            list.Add( "sDemandU2BorrowerAmt" );
            list.Add( "sDemandU2BrokerDueAmt" );
            list.Add( "sDemandU2BrokerPaidAmt" );
            list.Add( "sDemandU2LenderAmt" );
            list.Add( "sDemandU2LenderDesc" );
            list.Add( "sDemandU3BorrowerAmt" );
            list.Add( "sDemandU3BrokerDueAmt" );
            list.Add( "sDemandU3BrokerPaidAmt" );
            list.Add( "sDemandU3LenderAmt" );
            list.Add( "sDemandU3LenderDesc" );
            list.Add( "sDemandU4BorrowerAmt" );
            list.Add( "sDemandU4BrokerDueAmt" );
            list.Add( "sDemandU4BrokerPaidAmt" );
            list.Add( "sDemandU4LenderAmt" );
            list.Add( "sDemandU4LenderDesc" );
            list.Add( "sDemandU5BorrowerAmt" );
            list.Add( "sDemandU5BrokerDueAmt" );
            list.Add( "sDemandU5BrokerPaidAmt" );
            list.Add( "sDemandU5LenderAmt" );
            list.Add( "sDemandU5LenderDesc" );
            list.Add( "sDemandU6BorrowerAmt" );
            list.Add( "sDemandU6BrokerDueAmt" );
            list.Add( "sDemandU6BrokerPaidAmt" );
            list.Add( "sDemandU6LenderAmt" );
            list.Add( "sDemandU6LenderDesc" );
            list.Add( "sDemandU7BorrowerAmt" );
            list.Add( "sDemandU7BrokerDueAmt" );
            list.Add( "sDemandU7BrokerPaidAmt" );
            list.Add( "sDemandU7LenderAmt" );
            list.Add( "sDemandU7LenderDesc" );
            list.Add( "sDemandYieldSpreadPremiumBorrowerAmt" );
            list.Add( "sDemandYieldSpreadPremiumBorrowerMb" );
            list.Add( "sDemandYieldSpreadPremiumBorrowerPc" );
            list.Add( "sDemandYieldSpreadPremiumBrokerAmt" );
            list.Add( "sDemandYieldSpreadPremiumLenderAmt" );
            list.Add( "sDemandYieldSpreadPremiumLenderMb" );
            list.Add( "sDemandYieldSpreadPremiumLenderPc" );
            list.Add( "sEquityCalc" );
            list.Add( "sEstCloseD" );
            list.Add( "sFinalLAmt" );
            list.Add( "sHmdaCensusTract" );
            list.Add( "sLDiscnt" );
            list.Add( "sLienPosT" );
            list.Add( "sLOrigF" );
            list.Add( "sLpTemplateNm" );
            list.Add( "sLPurposeT" );
            list.Add( "sLtvR" );
            list.Add( "sNoteIR" );
            list.Add( "sPreparerXmlContent" );
            list.Add( "sProcF" );
            list.Add( "sPurchPrice" );
            list.Add( "sQualIR" );
            list.Add( "sRAdjCapMon" );
            list.Add( "sRAdjCapR" );
            list.Add( "sRAdjIndexR" );
            list.Add( "sRAdjLifeCapR" );
            list.Add( "sRAdjMarginR" );
            list.Add( "sRLckdD" );
            list.Add( "sRLckdExpiredD" );
            list.Add( "sRLckdIsLocked" );
            list.Add( "sRLckdNumOfDays" );
			list.Add("sRLckdDays");	//fs opm 9135 06/05/08
            list.Add( "sSpAddr" );
            list.Add( "sSpCity" );
            list.Add( "sSpCounty" );
            list.Add( "sSpState" );
            list.Add( "sSpZip" );
            list.Add( "sSubmitAmortDesc" );
            list.Add( "sSubmitBrokerFax" );
            list.Add( "sSubmitBuydownDesc" );
            list.Add( "sSubmitDocFull" );
            list.Add( "sSubmitDocOther" );
            list.Add( "sSubmitImpoundFlood" );
            list.Add( "sSubmitImpoundHazard" );
            list.Add( "sSubmitImpoundMI" );
            list.Add( "sSubmitImpoundOther" );
            list.Add( "sSubmitImpoundOtherDesc" );
            list.Add( "sSubmitImpoundTaxes" );
            list.Add( "sSubmitInitPmtCapR" );
            list.Add( "sSubmitMITypeDesc" );
            list.Add( "sSubmitMIYes" );
            list.Add( "sSubmitProgramCode" );
            list.Add( "sSubmitPropTDesc" );
            list.Add( "sSubmitRAdjustOtherDesc" );
            list.Add( "sTerm" );
            list.Add( "sUnitsNum" );
			list.Add( "sDue" );
			list.Add( "sRAdjWorstIndex" );
            list.Add("sLpTemplateId");
            list.Add("sfTransformLoPropertyTypeToPml");
            list.Add(nameof(CPageData.sQualIRLckd));
            list.Add(nameof(CPageData.sUseQualRate));
            list.Add(nameof(CPageData.sQualRateCalculationT));
            list.Add(nameof(CPageData.sQualRateCalculationFieldT1));
            list.Add(nameof(CPageData.sQualRateCalculationFieldT2));
            list.Add(nameof(CPageData.sQualRateCalculationAdjustment1));
            list.Add(nameof(CPageData.sQualRateCalculationAdjustment2));
            list.Add(nameof(CPageData.sQualTermCalculationType));
            list.Add(nameof(CPageData.sQualTerm));
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

		}

        public CLoanSubmissionData( Guid fileId ) : base( fileId , "LoanSubmission" , s_selectProvider )
        {
        }

	}

}