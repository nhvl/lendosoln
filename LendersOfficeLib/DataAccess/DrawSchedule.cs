﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using LendersOffice.UI;

namespace DataAccess
{
    [XmlType]
    public sealed class DrawSchedule
    {
        string m_condition = "";
        string m_timing = "0";
        string m_percent = "0.000%";
        string m_basis = "0";
        string m_fixedAmount = "$0.00";
        string m_total = "$0.00";      

        [LqbInputModelAttribute(type: InputFieldType.TextArea)]
        [XmlElement]
        public string Condition
        {
            get { return m_condition; }
            set { m_condition = value; }
            
        }

        [XmlElement]
        [LqbInputModelAttribute(type: InputFieldType.DropDownList, enumType: typeof(E_Timing))]
        public string Timing
        {
            get { return m_timing; }
            set
            {
                if (value == null || value == "")
                    m_timing = "0";
                else
                    m_timing = value;
            }
        }

        [XmlElement]
        [LqbInputModelAttribute(type: InputFieldType.Percent)]
        public string Percent
        {
            get { return m_percent; }
            set
            {
                if (value == null || value == "")
                    m_percent = "0.000%";
                else
                    m_percent = value;
            }
        }

        [XmlElement]
        [LqbInputModelAttribute(type: InputFieldType.DropDownList, enumType: typeof(E_PercentBaseTForConstruction))]
        public string Basis
        {
            get { return m_basis; }
            set
            {
                if (value == null || value == "")
                    m_basis = "0";
                else
                    m_basis = value;
            }
        }

        [XmlElement]
        [LqbInputModelAttribute(type: InputFieldType.Money)]
        public string FixedAmount
        {
            get { return m_fixedAmount; }
            set
            {
                if (value == null || value == "")
                    m_fixedAmount = "$0.00";
                else
                    m_fixedAmount = value;
            }
        }

        public bool TotalReadOnly
        {
            get { return true; }
        }

        [XmlElement]
        [LqbInputModelAttribute(type: InputFieldType.Money)]
        public string Total
        {
            get { return m_total; }
            set
            {
                if (value == null || value == "")
                    m_total = "$0.00";
                else
                    m_total = value;
            }
        }
    }
}
