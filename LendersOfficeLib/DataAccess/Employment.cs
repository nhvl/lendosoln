using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Data.Common;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.IO;

namespace DataAccess
{
    public class CEmploymentInfoData : CPageData
    {
        private static CSelectStatementProvider s_selectProvider;
        static CEmploymentInfoData() 
        {
            StringList list = new StringList();

            list.Add("aBEmpCollection");
            list.Add("aCEmpCollection");
            list.Add("aBEmplrBusPhoneLckd");
            list.Add("aCEmplrBusPhoneLckd");

            s_selectProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection);
        }

        public CEmploymentInfoData(Guid fileId, Guid appId, bool isOwner) : base(fileId, "Employment", s_selectProvider) 
        {
        }

        public CEmploymentInfoData(Guid fileId) : base(fileId, "Employment", s_selectProvider) 
        {
        }

    }

}
