﻿using System;
using LendersOfficeApp.los.RatePrice;

namespace DataAccess
{
    public interface ILoanProgramTemplate
    {
        Guid lLpTemplateId { get; }
        Guid BrokerId { get; }
        string lLpTemplateNm { get; }
        string lLendNm { get; }
        E_sLT lLT { get; }
        E_sLienPosT lLienPosT { get; }
        decimal lQualR { get; }
        int lTerm { get; }
        string lTerm_rep { get; }
        int lDue { get; }
        string lDue_rep { get; }
        int lLockedDays { get; }
        string lLockedDays_rep { get; }
        decimal lReqTopR { get; }
        string lReqTopR_rep { get; }
        decimal lReqBotR { get; }
        string lReqBotR_rep { get; }
        decimal lRadj1stCapR { get; }
        string lRadj1stCapR_rep { get; }
        int lRadj1stCapMon { get; }
        string lRadj1stCapMon_rep { get; }
        decimal lRAdjCapR { get; }
        string lRAdjCapR_rep { get; }
        int lRAdjCapMon { get; }
        string lRAdjCapMon_rep { get; }
        decimal lRAdjLifeCapR { get; }
        string lRAdjLifeCapR_rep { get; }
        decimal lRAdjMarginR { get; set; }
        string lRAdjMarginR_rep { get; }
        decimal lRAdjIndexR { get; }
        string lRAdjIndexR_rep { get; }
        decimal lRAdjFloorR { get; }
        string lRAdjFloorR_rep { get; }
        E_sRAdjRoundT lRAdjRoundT { get; }
        decimal lPmtAdjCapR { get; }
        string lPmtAdjCapR_rep { get; }
        int lPmtAdjCapMon { get; }
        string lPmtAdjCapMon_rep { get; }
        int lPmtAdjRecastPeriodMon { get; }
        string lPmtAdjRecastPeriodMon_rep { get; }
        int lPmtAdjRecastStop { get; }
        string lPmtAdjRecastStop_rep { get; }
        decimal lBuydwnR1 { get; }
        string lBuydwnR1_rep { get; }
        decimal lBuydwnR2 { get; }
        string lBuydwnR2_rep { get; }
        decimal lBuydwnR3 { get; }
        string lBuydwnR3_rep { get; }
        decimal lBuydwnR4 { get; }
        string lBuydwnR4_rep { get; }
        decimal lBuydwnR5 { get; }
        string lBuydwnR5_rep { get; }
        int lBuydwnMon1 { get; }
        string lBuydwnMon1_rep { get; }
        int lBuydwnMon2 { get; }
        string lBuydwnMon2_rep { get; }
        int lBuydwnMon3 { get; }
        string lBuydwnMon3_rep { get; }
        int lBuydwnMon4 { get; }
        string lBuydwnMon4_rep { get; }
        int lBuydwnMon5 { get; }
        string lBuydwnMon5_rep { get; }
        int lGradPmtYrs { get; }
        string lGradPmtYrs_rep { get; }
        decimal lGradPmtR { get; }
        string lGradPmtR_rep { get; }
        int lIOnlyMon { get; }
        string lIOnlyMon_rep { get; }
        bool lHasVarRFeature { get; }
        string lVarRNotes { get; }
        bool lAprIncludesReqDeposit { get; }
        bool lHasDemandFeature { get; }
        string lLateDays { get; }
        string lLateChargePc { get; }
        E_sPrepmtPenaltyT lPrepmtPenaltyT { get; }
        E_sAssumeLT lAssumeLT { get; }
        Guid lCcTemplateId { get; }
        string lFilingF { get; }
        string lLateChargeBaseDesc { get; }
        decimal lPmtAdjMaxBalPc { get; }
        string lPmtAdjMaxBalPc_rep { get; }
        E_sFinMethT lFinMethT { get; }
        string lFinMethDesc { get; }
        E_sPrepmtRefundT lPrepmtRefundT { get; }
        Guid FolderId { get; }
        CDateTime lRateSheetEffectiveD { get; }
        CDateTime lRateSheetExpirationD { get; }
        bool IsMaster { get; }
        //int lLenderNmInheritT { get; }
        decimal lLpeFeeMin { get; }
        string lLpeFeeMin_rep { get; }
        decimal lLpeFeeMax { get; }
        string lLpeFeeMax_rep { get; }
        string PairingProductIds { get; }
        Guid lBaseLpId { get; }
        bool IsEnabled { get; }
        Guid lArmIndexGuid { get; }
        string lArmIndexBasedOnVstr { get; }
        string lArmIndexCanBeFoundVstr { get; }
        bool lArmIndexAffectInitIRBit { get; }
        decimal lRAdjRoundToR { get; }
        CDateTime lArmIndexEffectiveD { get; }
        string lArmIndexEffectiveD_rep { get; }
        E_sFreddieArmIndexT lFreddieArmIndexT { get; }
        string lArmIndexNameVstr { get; }
        E_sArmIndexT lArmIndexT { get; }
        string lArmIndexNotifyAtLeastDaysVstr { get; }
        string lArmIndexNotifyNotBeforeDaysVstr { get; }
        string lLpTemplateNmInherit { get; }
        bool lLpTemplateNmOverrideBit { get; }
        Guid lCcTemplateIdInherit { get; }
        bool lCcTemplateIdOverrideBit { get; }
        int lLockedDaysInherit { get; }
        bool lLockedDaysOverrideBit { get; }
        decimal lLpeFeeMinInherit { get; }
        bool lLpeFeeMinOverrideBit { get; }
        decimal lLpeFeeMaxInherit { get; }
        bool lLpeFeeMaxOverrideBit { get; }
        bool IsEnabledInherit { get; }
        bool IsEnabledOverrideBit { get; }
        int lPpmtPenaltyMon { get; }
        int lPpmtPenaltyMonInherit { get; }
        bool lPpmtPenaltyMonOverrideBit { get; }
        decimal lRateDelta { get; }
        string lRateDelta_rep { get; }
        decimal lFeeDelta { get; }
        string lFeeDelta_rep { get; }
        //bool lRateSheetxmlContentOverrideBit { get; }
        int lPpmtPenaltyMonLowerSearch { get; }
        int lLockedDaysLowerSearch { get; }
        int lLockedDaysLowerSearchInherit { get; }
        int lPpmtPenaltyMonLowerSearchInherit { get; }
        string PairingProductIdsInherit { get; }
        bool PairingProductIdsOverrideBit { get; }
        string lLpInvestorNm { get; }
        string lLendNmInherit { get; }
        bool lLendNmOverrideBit { get; }
        //bool lPairingInheritAcceptBit { get; }
        string lRateOptionBaseId { get; }
        //string lRateOptionBaseIdInherit { get; }
        Guid PairIdFor1stLienProdGuid { get; }
        Guid PairIdFor1stLienProdGuidInherit { get; }
        decimal lLpeFeeMinParam { get; }
        bool lLpeFeeMinAddRuleAdjInheritBit { get; }
        bool IsLpe { get; }
        bool lIsArmMarginDisplayed { get; }
        string ProductCode { get; }
        string ProductIdentifier { get; }
        bool IsOptionArm { get; }
        decimal lRateDeltaInherit { get; }
        decimal lFeeDeltaInherit { get; }
        Guid SrcRateOptionsProgIdInherit { get; }
        Guid SrcRateOptionsProgId { get; }
        //bool IsSpreadingNeededForProductData { get; }
        E_sLpDPmtT lLpDPmtT { get; }
        E_sLpQPmtT lLpQPmtT { get; }
        //bool lIsOptionArm { get; }
        bool lHasQRateInRateOptions { get; set; }
        bool IsLpeDummyProgram { get; }
        bool IsPairedOnlyWithSameInvestor { get; }
        //DateTime CreatedD { get; }
        //byte[] VersionTimestamp { get; }
        int lIOnlyMonLowerSearch { get; }
        int lIOnlyMonLowerSearchInherit { get; }
        bool lIOnlyMonLowerSearchOverrideBit { get; }
        int lIOnlyMonUpperSearch { get; }
        int lIOnlyMonUpperSearchInherit { get; }
        bool lIOnlyMonUpperSearchOverrideBit { get; }
        bool CanBeStandAlone2nd { get; }
        bool lDtiUsingMaxBalPc { get; }
        bool IsMasterPriceGroup { get; }
        string lLpProductType { get; }
        //string lDataTracLpId { get; }
        //string lDataTracLpIdInherit { get; }
        //bool lDataTracLpIdOverrideBit { get; }
        E_sRAdjFloorBaseT lRAdjFloorBaseT { get; }
        bool IsConvertibleMortgage { get; }
        bool lLpmiSupportedOutsidePmi { get; }
        bool lWholesaleChannelProgram { get; }
        int lHardPrepmtPeriodMonths { get; }
        string lHardPrepmtPeriodMonths_rep { get; }
        int lSoftPrepmtPeriodMonths { get; }
        string lSoftPrepmtPeriodMonths_rep { get; }
        E_sHelocPmtBaseT lHelocQualPmtBaseT { get; }
        E_sHelocPmtFormulaT lHelocQualPmtFormulaT { get; }
        E_sHelocPmtFormulaRateT lHelocQualPmtFormulaRateT { get; }
        E_sHelocPmtAmortTermT lHelocQualPmtAmortTermT { get; }
        E_sHelocPmtPcBaseT lHelocQualPmtPcBaseT { get; }
        decimal lHelocQualPmtMb { get; }
        string lHelocQualPmtMb_rep { get; }
        E_sHelocPmtBaseT lHelocPmtBaseT { get; }
        E_sHelocPmtFormulaT lHelocPmtFormulaT { get; }
        E_sHelocPmtFormulaRateT lHelocPmtFormulaRateT { get; }
        E_sHelocPmtAmortTermT lHelocPmtAmortTermT { get; }
        E_sHelocPmtPcBaseT lHelocPmtPcBaseT { get; }
        decimal lHelocPmtMb { get; }
        string lHelocPmtMb_rep { get; }
        int lHelocDraw { get; }
        string lHelocDraw_rep { get; }
        bool lHelocCalculatePrepaidInterest { get; }
        bool IsHeloc { get; }

        string lLpCustomCode1 { get; }
        string lLpCustomCode2 { get; }
        string lLpCustomCode3 { get; }
        string lLpCustomCode4 { get; }
        string lLpCustomCode5 { get; }

        string lLpInvestorCode1 { get; }
        string lLpInvestorCode2 { get; }
        string lLpInvestorCode3 { get; }
        string lLpInvestorCode4 { get; }
        string lLpInvestorCode5 { get; }

        string lPrepmtPeriod_rep { get; }
        decimal lNoteR { get; }
        decimal lLOrigFPc { get; }
        string lHelocRepay_rep { get; }
        DateTime lRateSheetDownloadEndD { get; }
        DateTime lRateSheetDownloadStartD { get; }

        CCcTemplateData CcTemplate { get; }
        bool HasCcTemplate { get; }
        string FullName { get; }
        IRateItem[] GetRawRateSheetWithDeltas();

        IRateItem GetRateOptionKey( int sProdRLckdDays, int sProdPpmtPenaltyMon, int sIOnlyMon );

        bool lLpeFeeMinAddRuleAdjBit { get; }

        long LpeAcceptableRsFileVersionNumber { get; }

        string LpeAcceptableRsFileId { get; }

        QualRateCalculationT lQualRateCalculationT { get; }

        QualRateCalculationFieldT lQualRateCalculationFieldT1 { get; }

        QualRateCalculationFieldT lQualRateCalculationFieldT2 { get; }

        decimal lQualRateCalculationAdjustment1 { get; }

        decimal lQualRateCalculationAdjustment2 { get; }

        int lQualTerm { get; }

        QualTermCalculationType lQualTermCalculationType { get; }
    }
}
