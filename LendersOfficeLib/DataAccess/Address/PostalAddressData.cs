﻿namespace DataAccess
{
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Flatten out all the settable properties of a postal address for easy serialization.
    /// </summary>
    public sealed class PostalAddressData
    {
        /// <summary>
        /// Indicator for a GeneralPostalAddress.
        /// </summary>
        private const string GeneralIndicator = "General";

        /// <summary>
        /// Indicator for a UnitedStatesPostalAddress.
        /// </summary>
        private const string UsNormalIndicator = "UsNormal";

        /// <summary>
        /// Initializes a new instance of the <see cref="PostalAddressData"/> class.
        /// </summary>
        /// <remarks>
        /// This is necessary for the json serializer.
        /// </remarks>
        public PostalAddressData()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PostalAddressData"/> class.
        /// </summary>
        /// <param name="address">An instance of the GeneralPostalAddress class.</param>
        public PostalAddressData(GeneralPostalAddress address)
        {
            this.SetBaseFields(address);
            this.StreetAddress = address.StreetAddress?.ToString();
            this.AddressType = GeneralIndicator;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PostalAddressData"/> class.
        /// </summary>
        /// <param name="address">An instance of the UnitedStatesPostalAddress class.</param>
        public PostalAddressData(UnitedStatesPostalAddress address)
        {
            this.SetBaseFields(address);
            this.SetUsSpecificFields(address);
            this.AddressType = UsNormalIndicator;
        }

        /// <summary>
        /// Gets or sets the address type.
        /// </summary>
        public string AddressType { get; set; }

        /// <summary>
        /// Gets or sets the full street address.
        /// </summary>
        /// <summary>
        /// Gets or sets the unparsed street address.
        /// </summary>
        public string StreetAddress { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the state or province.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets the postal code.
        /// </summary>
        public string PostalCode { get; set; }

        /// <summary>
        /// Gets or sets the country code.
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// Gets or sets the address number.
        /// </summary>
        public string AddressNumber { get; set; }

        /// <summary>
        /// Gets or sets the pre direction.
        /// </summary>
        public string StreetPreDirection { get; set; }

        /// <summary>
        /// Gets or sets the post direction.
        /// </summary>
        public string StreetPostDirection { get; set; }

        /// <summary>
        /// Gets or sets the street suffix.
        /// </summary>
        public string StreetSuffix { get; set; }

        /// <summary>
        /// Gets or sets the street name.
        /// </summary>
        public string StreetName { get; set; }

        /// <summary>
        /// Gets or sets the unit type.
        /// </summary>
        public string UnitType { get; set; }

        /// <summary>
        /// Gets or sets the unit identifier.
        /// </summary>
        public string UnitIdentifier { get; set; }

        /// <summary>
        /// Construct an instance of a sub-class of PostalAddress.
        /// </summary>
        /// <returns>An instance of a sub-class of PostalAddress.</returns>
        public PostalAddress BuildPostalAddress()
        {
            switch (this.AddressType)
            {
                case GeneralIndicator:
                    return this.BuildGeneralPostalAddress();

                case UsNormalIndicator:
                    return this.BuildUsNormalPostalAddress();

                default:
                    throw new CBaseException(ErrorMessages.Generic, "Unrecognized postal address type indicator : " + this.AddressType);
            }
        }

        /// <summary>
        /// Set the fields that are declared in the base class PostalAddress.
        /// </summary>
        /// <param name="address">The address.</param>
        private void SetBaseFields(PostalAddress address)
        {
            this.City = address.City?.ToString();
            this.State = address.State?.ToString();
            this.PostalCode = address.PostalCode?.ToString();
            this.CountryCode = address.CountryCode?.ToString();
        }

        /// <summary>
        /// Set the fields that are in the UnitedStatesPostalAddress
        /// that aren't also in the base class PostalAddress.
        /// </summary>
        /// <param name="address">The US address.</param>
        private void SetUsSpecificFields(UnitedStatesPostalAddress address)
        {
            this.StreetAddress = address.UnparsedStreetAddress?.ToString();
            this.AddressNumber = address.AddressNumber?.ToString();
            this.StreetPreDirection = address.StreetPreDirection?.ToString();
            this.StreetPostDirection = address.StreetPostDirection?.ToString();
            this.StreetSuffix = address.StreetSuffix?.ToString();
            this.StreetName = address.StreetName?.ToString();
            this.UnitType = address.UnitType?.ToString();
            this.UnitIdentifier = address.UnitIdentifier?.ToString();
        }

        /// <summary>
        /// Construct an instance of GeneralPostalAddress with the contained data.
        /// </summary>
        /// <returns>An instance of GeneralPostalAddress.</returns>
        private GeneralPostalAddress BuildGeneralPostalAddress()
        {
            var builder = new GeneralPostalAddress.Builder();

            if (!string.IsNullOrEmpty(this.StreetAddress))
            {
                builder.StreetAddress = LqbGrammar.DataTypes.StreetAddress.Create(this.StreetAddress).Value;
            }

            if (!string.IsNullOrEmpty(this.City))
            {
                builder.City = LqbGrammar.DataTypes.City.Create(this.City).Value;
            }

            if (!string.IsNullOrEmpty(this.State))
            {
                builder.State = LqbGrammar.DataTypes.AdministrativeArea.Create(this.State).Value;
            }

            if (!string.IsNullOrEmpty(this.PostalCode))
            {
                builder.PostalCode = LqbGrammar.DataTypes.PostalCode.Create(this.PostalCode).Value;
            }

            if (!string.IsNullOrEmpty(this.CountryCode))
            {
                builder.CountryCode = LqbGrammar.DataTypes.CountryCodeIso3.Create(this.CountryCode).Value;
            }

            return builder.GetAddress();
        }

        /// <summary>
        /// Construct an instance of UnitedStatesPostalAddress with the contained data.
        /// </summary>
        /// <returns>An instance of UnitedStatesPostalAddress.</returns>
        private UnitedStatesPostalAddress BuildUsNormalPostalAddress()
        {
            var builder = new UnitedStatesPostalAddress.Builder();

            if (!string.IsNullOrEmpty(this.StreetAddress))
            {
                builder.UnparsedStreetAddress = LqbGrammar.DataTypes.StreetAddress.Create(this.StreetAddress).Value;
            }

            if (!string.IsNullOrEmpty(this.City))
            {
                builder.City = LqbGrammar.DataTypes.City.Create(this.City).Value;
            }

            if (!string.IsNullOrEmpty(this.State))
            {
                builder.UsState = LqbGrammar.DataTypes.UnitedStatesPostalState.Create(this.State).Value;
            }

            if (!string.IsNullOrEmpty(this.PostalCode))
            {
                builder.Zipcode = LqbGrammar.DataTypes.Zipcode.Create(this.PostalCode).Value;
            }

            if (!string.IsNullOrEmpty(this.AddressNumber))
            {
                builder.AddressNumber = LqbGrammar.DataTypes.UspsAddressNumber.Create(this.AddressNumber).Value;
            }

            if (!string.IsNullOrEmpty(this.StreetPreDirection))
            {
                builder.StreetPreDirection = LqbGrammar.DataTypes.UspsDirectional.Create(this.StreetPreDirection).Value;
            }

            if (!string.IsNullOrEmpty(this.StreetPostDirection))
            {
                builder.StreetPostDirection = LqbGrammar.DataTypes.UspsDirectional.Create(this.StreetPostDirection).Value;
            }

            if (!string.IsNullOrEmpty(this.StreetSuffix))
            {
                builder.StreetSuffix = LqbGrammar.DataTypes.StreetSuffix.Create(this.StreetSuffix).Value;
            }

            if (!string.IsNullOrEmpty(this.StreetName))
            {
                builder.StreetName = LqbGrammar.DataTypes.StreetName.Create(this.StreetName).Value;
            }

            if (!string.IsNullOrEmpty(this.UnitType))
            {
                builder.UnitType = LqbGrammar.DataTypes.AddressUnitType.Create(this.UnitType).Value;
            }

            if (!string.IsNullOrEmpty(this.UnitIdentifier))
            {
                builder.UnitIdentifier = LqbGrammar.DataTypes.AddressUnitIdentifier.Create(this.UnitIdentifier).Value;
            }

            return builder.GetAddress();
        }
    }
}
