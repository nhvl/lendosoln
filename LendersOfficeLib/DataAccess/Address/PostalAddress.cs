﻿namespace DataAccess
{
    using LqbGrammar.DataTypes;
    using LqbGrammar.Utils.Country;

    /// <summary>
    /// Base class for postal addresses, with the expectation of handling international addresses.
    /// </summary>
    public abstract class PostalAddress
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PostalAddress"/> class.
        /// </summary>
        /// <param name="city">The city value.</param>
        public PostalAddress(City? city)
        {
            this.City = city;
        }

        /// <summary>
        /// Gets the street address.
        /// </summary>
        public abstract StreetAddress? StreetAddress { get; }

        /// <summary>
        /// Gets the city.
        /// </summary>
        public City? City { get; }

        /// <summary>
        /// Gets the state (province, whatever).
        /// </summary>
        public abstract AdministrativeArea? State { get; }

        /// <summary>
        /// Gets the postal code.
        /// </summary>
        public abstract PostalCode? PostalCode { get; }

        /// <summary>
        /// Gets the country code.
        /// </summary>
        public abstract CountryCodeIso3? CountryCode { get; }

        /// <summary>
        /// Gets the name of the country.
        /// </summary>
        public virtual Country? Country
        {
            get
            {
                return this.CountryCode?.ToCountryName();
            }
        }
    }
}
