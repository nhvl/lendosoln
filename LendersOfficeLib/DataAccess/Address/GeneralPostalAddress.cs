﻿namespace DataAccess
{
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Class to handle any international address.
    /// </summary>
    /// <remarks>
    /// As we beef up support for different countries we should be able
    /// to create individual classes specific to each country.  However, we
    /// will always need to be able to store address data for those countries
    /// for which we haven't yet developed a specific class.
    /// </remarks>
    public sealed class GeneralPostalAddress : PostalAddress
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GeneralPostalAddress"/> class.
        /// </summary>
        /// <param name="builder">Builder that contains the initialized data.</param>
        private GeneralPostalAddress(Builder builder)
            : base(builder.City)
        {
            this.StreetAddress = builder.StreetAddress;
            this.State = builder.State;
            this.PostalCode = builder.PostalCode;
            this.CountryCode = builder.CountryCode;
        }

        /// <summary>
        /// Gets the street address.
        /// </summary>
        public override StreetAddress? StreetAddress { get; }

        /// <summary>
        /// Gets the state (province, whatever).
        /// </summary>
        /// <remarks>
        /// Some countries have multiple levels of adminstrative areas
        /// so this field will capture the higher levels in the hierarchy.
        /// The city property is intended to carry the lower levels of the hieriarchy.
        /// It is assumed that the breaking point will be understood by
        /// whomever is entering the data.
        /// </remarks>
        public override AdministrativeArea? State { get; }

        /// <summary>
        /// Gets the postal code.
        /// </summary>
        public override PostalCode? PostalCode { get; }

        /// <summary>
        /// Gets the country code.
        /// </summary>
        public override CountryCodeIso3? CountryCode { get; }

        /// <summary>
        /// Gets a builder representing the current instance, used for mutation.
        /// </summary>
        /// <returns>A builder for the current instance.</returns>
        public Builder GetBuilder()
        {
            return new Builder
            {
                StreetAddress = this.StreetAddress,
                City = this.City,
                State = this.State,
                PostalCode = this.PostalCode,
                CountryCode = this.CountryCode,
            };
        }

        /// <summary>
        /// Builder for a GeneralPostalAddress.
        /// </summary>
        public sealed class Builder
        {
            /// <summary>
            /// Gets or sets the street address.
            /// </summary>
            public StreetAddress? StreetAddress { get; set; }

            /// <summary>
            /// Gets or sets the city.
            /// </summary>
            public City? City { get; set; }

            /// <summary>
            /// Gets or sets the state (province, whatever).
            /// </summary>
            public AdministrativeArea? State { get; set; }

            /// <summary>
            /// Gets or sets the postal code.
            /// </summary>
            public PostalCode? PostalCode { get; set; }

            /// <summary>
            /// Gets or sets the country code.
            /// </summary>
            public CountryCodeIso3? CountryCode { get; set; }

            /// <summary>
            /// Generate the GeneralPostalAddress from the data added to this builder.
            /// </summary>
            /// <returns>The built GeneralPostalAddress instance.</returns>
            public GeneralPostalAddress GetAddress()
            {
                return new GeneralPostalAddress(this);
            }
        }
    }
}
