﻿namespace DataAccess
{
    using System.Text;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Class to handle addresses within the United States.
    /// </summary>
    public sealed class UnitedStatesPostalAddress : PostalAddress
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UnitedStatesPostalAddress"/> class.
        /// </summary>
        /// <param name="builder">Builder that contains the initialized data.</param>
        private UnitedStatesPostalAddress(Builder builder)
            : base(builder.City)
        {
            this.UnparsedStreetAddress = builder.UnparsedStreetAddress;
            this.AddressNumber = builder.AddressNumber;
            this.StreetPreDirection = builder.StreetPreDirection;
            this.StreetPostDirection = builder.StreetPostDirection;
            this.StreetSuffix = builder.StreetSuffix;
            this.StreetName = builder.StreetName;
            this.UnitType = builder.UnitType;
            this.UnitIdentifier = builder.UnitIdentifier;
            this.UsState = builder.UsState;
            this.Zipcode = builder.Zipcode;
        }

        /// <summary>
        /// Gets the street address.
        /// </summary>
        public override StreetAddress? StreetAddress
        {
            get
            {
                return string.IsNullOrEmpty(this.UnparsedStreetAddress.ToString()) ? this.CalculateStreetAddress() : this.UnparsedStreetAddress;
            }
        }

        /// <summary>
        /// Gets the unparsed street address.
        /// </summary>
        public StreetAddress? UnparsedStreetAddress { get; }

        /// <summary>
        /// Gets the street number component of the street address.
        /// </summary>
        public UspsAddressNumber? AddressNumber { get; }

        /// <summary>
        /// Gets the pre-direction component of the street address.
        /// </summary>
        public UspsDirectional? StreetPreDirection { get; }

        /// <summary>
        /// Gets the post-direction component of the street address.
        /// </summary>
        public UspsDirectional? StreetPostDirection { get; }

        /// <summary>
        /// Gets the street type component of the street address.
        /// </summary>
        public StreetSuffix? StreetSuffix { get; }

        /// <summary>
        /// Gets the street name component of the street address.
        /// </summary>
        public StreetName? StreetName { get; }

        /// <summary>
        /// Gets the unit type component of the street address.
        /// </summary>
        public AddressUnitType? UnitType { get; }

        /// <summary>
        /// Gets the unit identifier component of the street address.
        /// </summary>
        public AddressUnitIdentifier? UnitIdentifier { get; }

        /// <summary>
        /// Gets the state.
        /// </summary>
        public UnitedStatesPostalState? UsState { get; }

        /// <summary>
        /// Gets the state.
        /// </summary>
        public override AdministrativeArea? State
        {
            get
            {
                return AdministrativeArea.Create(this.UsState?.ToString());
            }
        }

        /// <summary>
        /// Gets the zipcode.
        /// </summary>
        public Zipcode? Zipcode { get; }

        /// <summary>
        /// Gets the postal code.
        /// </summary>
        public override PostalCode? PostalCode
        {
            get
            {
                return LqbGrammar.DataTypes.PostalCode.Create(this.Zipcode?.ToString());
            }
        }

        /// <summary>
        /// Gets the country code.
        /// </summary>
        public override CountryCodeIso3? CountryCode
        {
            get
            {
                return CountryCodeIso3.UnitedStates;
            }
        }

        /// <summary>
        /// Gets a builder representing the current instance, used for mutation.
        /// </summary>
        /// <returns>A builder for the current instance.</returns>
        public Builder GetBuilder()
        {
            return new Builder
            {
                UnparsedStreetAddress = this.UnparsedStreetAddress,
                AddressNumber = this.AddressNumber,
                StreetPreDirection = this.StreetPreDirection,
                StreetPostDirection = this.StreetPostDirection,
                StreetSuffix = this.StreetSuffix,
                StreetName = this.StreetName,
                UnitType = this.UnitType,
                UnitIdentifier = this.UnitIdentifier,
                City = this.City,
                UsState = this.UsState,
                Zipcode = this.Zipcode,
            };
        }

        /// <summary>
        /// Assemble the various components of the street address into a single value.
        /// </summary>
        /// <returns>The street address.</returns>
        private StreetAddress? CalculateStreetAddress()
        {
            var sb = new StringBuilder();
            if (this.AddressNumber != null)
            {
                this.AddValue(this.AddressNumber.Value.ToString(), sb);
            }

            if (this.StreetPreDirection != null)
            {
                this.AddValue(this.StreetPreDirection.Value.ToString(), sb);
            }

            if (this.StreetName != null)
            {
                this.AddValue(this.StreetName.Value.ToString(), sb);
            }

            if (this.StreetSuffix != null)
            {
                this.AddValue(this.StreetSuffix.Value.ToString(), sb);
            }

            if (this.StreetPostDirection != null)
            {
                this.AddValue(this.StreetPostDirection.Value.ToString(), sb);
            }

            if (this.UnitType != null)
            {
                this.AddValue(this.UnitType.Value.ToString(), sb);
            }

            if (this.UnitIdentifier != null)
            {
                this.AddValue(this.UnitIdentifier.Value.ToString(), sb);
            }

            return sb.Length == 0 ? null : LqbGrammar.DataTypes.StreetAddress.Create(sb.ToString());
        }

        /// <summary>
        /// Append a value to the string builder if the value contains printable characters.
        /// </summary>
        /// <param name="value">The value to append.</param>
        /// <param name="sb">The string builder.</param>
        private void AddValue(string value, StringBuilder sb)
        {
            value = value?.Trim();
            if (!string.IsNullOrWhiteSpace(value))
            {
                value = sb.Length == 0 ? value : " " + value;
                sb.Append(value);
            }
        }

        /// <summary>
        /// Builder for a UnitedStatesPostalAddress.
        /// </summary>
        public sealed class Builder
        {
            /// <summary>
            /// Gets or sets the unparsed street address.
            /// </summary>
            public StreetAddress? UnparsedStreetAddress { get; set; }

            /// <summary>
            /// Gets or sets the street number component of the street address.
            /// </summary>
            public UspsAddressNumber? AddressNumber { get; set; }

            /// <summary>
            /// Gets or sets the pre-direction component of the street address.
            /// </summary>
            public UspsDirectional? StreetPreDirection { get; set; }

            /// <summary>
            /// Gets or sets the post-direction component of the street address.
            /// </summary>
            public UspsDirectional? StreetPostDirection { get; set; }

            /// <summary>
            /// Gets or sets the street type component of the street address.
            /// </summary>
            public StreetSuffix? StreetSuffix { get; set; }

            /// <summary>
            /// Gets or sets the street name component of the street address.
            /// </summary>
            public StreetName? StreetName { get; set; }

            /// <summary>
            /// Gets or sets the unit type component of the street address.
            /// </summary>
            public AddressUnitType? UnitType { get; set; }

            /// <summary>
            /// Gets or sets the unit identifier component of the street address.
            /// </summary>
            public AddressUnitIdentifier? UnitIdentifier { get; set; }

            /// <summary>
            /// Gets or sets the city.
            /// </summary>
            public City? City { get; set; }

            /// <summary>
            /// Gets or sets the state.
            /// </summary>
            public UnitedStatesPostalState? UsState { get; set; }

            /// <summary>
            /// Gets or sets the zipcode.
            /// </summary>
            public Zipcode? Zipcode { get; set; }

            /// <summary>
            /// Generate the UnitedStatesPostalAddress from the data added to this builder.
            /// </summary>
            /// <returns>The built UnitedStatesPostalAddress instance.</returns>
            public UnitedStatesPostalAddress GetAddress()
            {
                return new UnitedStatesPostalAddress(this);
            }
        }
    }
}
