using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
	public class CLpeSnapshotData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CLpeSnapshotData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
	        list.Add("sLNm");
            list.Add("sProdLpePriceGroupId");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CLpeSnapshotData(Guid fileId) : base(fileId, "CLpeSnapshotData", s_selectProvider)
		{
		}
	}
}
