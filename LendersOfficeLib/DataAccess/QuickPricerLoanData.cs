﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    public class CQuickPricerLoanData : CPageData
    {
		private static CSelectStatementProvider s_selectProvider;
		static CQuickPricerLoanData()
		{
			StringList list = new StringList();
            #region Target Fields
            list.Add("sQuickPricerLoanItem");
            list.Add("sLPurposeTPe");
            list.Add("sLAmtCalcPe");
            list.Add("sHouseValPe");
            list.Add("sProdCashoutAmt");
            list.Add("sSpCounty");
            list.Add("sSpState");
            list.Add("sSpZip");
            list.Add("aBExperianScorePe");
            list.Add("aBEquifaxScorePe");
            list.Add("aBTransUnionScorePe");
            list.Add("aBFirstNm");
            list.Add("aBLastNm");
            list.Add("aBSsn");
            list.Add("sfGetLoanProgramsToRunLpe");
            list.Add("sProdCalcEntryT");
            list.Add("sfTransformDataToPml");
            list.Add("sCreditScoreType1");
            list.Add("sEmployeeManagerId");
            list.Add("sEmployeeLenderAccExecId");
            list.Add("sEmployeeCallCenterAgentId");
            list.Add("sEmployeeLoanOpenerId");
            list.Add("sEmployeeManagerId");
            list.Add("sEmployeeUnderwriterId");
            list.Add("sEmployeeLockDeskId");
            list.Add("sEmployeeProcessorId");
            list.Add("sEmployeeLoanRepId");
            list.Add("sEmployeeShipperId");
            list.Add("sEmployeeFunderId");
            list.Add("sEmployeePostCloserId");
            list.Add("sEmployeeInsuringId");
            list.Add("sEmployeeCollateralAgentId");
            list.Add("sEmployeeDocDrawerId");
            list.Add("sLienPosT");
            list.Add("sProdLpePriceGroupId");
            list.Add("sProdLpePriceGroupNm");
            list.Add("sSpStatePe");
            list.Add("sOccTPe");
            list.Add("sProdSpT");
            list.Add("sLtvRPe");    //opm 28139 fs 03/12/09
            list.Add("sProdIncludeMyCommunityProc");
            list.Add("sProdIncludeHomePossibleProc");
            list.Add("sProdIncludeNormalProc");
            list.Add("sProdIncludeFHATotalProc");
            list.Add("sProdIncludeVAProc");
            list.Add("sProdIncludeUSDARuralProc");
            list.Add("sProdFilterDue10Yrs");
            list.Add("sProdFilterDue15Yrs");
            list.Add("sProdFilterDue20Yrs");
            list.Add("sProdFilterDue25Yrs");
            list.Add("sProdFilterDue30Yrs");            
            list.Add("sProdFilterDueOther");
            list.Add("sProdFilterFinMethFixed");            
            list.Add("sProdFilterFinMeth3YrsArm");
            list.Add("sProdFilterFinMeth5YrsArm");
            list.Add("sProdFilterFinMeth7YrsArm");
            list.Add("sProdFilterFinMeth10YrsArm");
            list.Add("sProdFilterFinMethOther");
            list.Add("sProdFilterPmtTPI");
            list.Add("sProdFilterPmtTIOnly");
            //opm 3247 fs 07/20/09
            list.Add("sProdRLckdDays");
            list.Add("sProdImpound");
            list.Add("sLtvRPe");
            list.Add("sDownPmtPcPe");
            list.Add("sEquityPe");
            list.Add("sLtvROtherFinPe");
            list.Add("sProOFinBalPe");
            list.Add("sCltvRPe");
            list.Add("sProdImpound");
            list.Add("sProdIsDuRefiPlus");
            list.Add("sHas2ndFinPe");
            list.Add("sIsIOnlyPe");
            list.Add("sProdMIOptionT");

            //opm 47824 fs 04/02/10
            list.Add("sSpCity");

            list.Add("sLNm");
            
            // OPM 32225 12/06/10
            list.Add("aBFirstNm");
            list.Add("aBLastNm");
            list.Add("aBSsn");
            list.Add("sOriginatorCompensationPaymentSourceT");
            list.Add("sApprValPe");
            list.Add("sOriginatorCompensationLenderFeeOptionT");

            list.Add("sProdFilterDisplayrateMerge");
            list.Add("sProdFilterDisplayUsingCurrentNoteRate");
            list.Add("sProdFilterRestrictResultToCurrentRegistered");
            list.Add("sProdFilterMatchCurrentTerm");
            list.Add("sInclude10yrAnd25yrInOtherDueFilter");
            list.Add("sPriceGroup");
            list.Add("sCreditScoreEstimatePe");
            list.Add("sConvSplitMIRT");
            #endregion
			s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

		}
        protected override bool m_enforceAccessControl
        {
            get
            {
                // This will allow "P" to load value from template.
                return false;
            }
        }
        public CQuickPricerLoanData(Guid fileId)
            : base(fileId, "QuickPricer", s_selectProvider)
        {
        }
    }
}
