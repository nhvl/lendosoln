﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace DataAccess
{
    public class SettlementServiceProvider
    {
        [JsonConstructor]
        public SettlementServiceProvider()
        {
        }

        public SettlementServiceProvider(bool shouldMakeNewId)
            : this()
        {
            if (shouldMakeNewId)
            {
                this.Id = Guid.NewGuid();
            }
        }

        public SettlementServiceProvider(LendersOffice.Rolodex.RolodexDB sourceContact)
            : this(shouldMakeNewId: true)
        {
            this.AgentId = sourceContact.ID;
            this.ServiceT = sourceContact.Type;

            this.ContactName = sourceContact.Name;
            this.CompanyName = sourceContact.CompanyName;

            this.Phone = sourceContact.PhoneOfCompany;
            this.Fax = sourceContact.FaxOfCompany;
            this.Email = sourceContact.Email;

            this.StreetAddress = sourceContact.Address.StreetAddress;
            this.City = sourceContact.Address.City;
            this.State = sourceContact.Address.State;
            this.Zip = sourceContact.Address.Zipcode;
        }

        public SettlementServiceProvider(CAgentFields agent)
            : this(shouldMakeNewId: true)
        {
            this.AgentId = agent.RecordId;
            this.ServiceT = agent.AgentRoleT;

            this.ContactName = agent.AgentName;
            this.CompanyName = agent.CompanyName;

            this.Phone = agent.Phone;
            this.Fax = agent.FaxNum;
            this.Email = agent.EmailAddr;

            this.StreetAddress = agent.StreetAddr;
            this.City = agent.City;
            this.State = agent.State;
            this.Zip = agent.Zip;
        }

        public virtual E_AgentRoleT ServiceT { get; set; }
        public string CompanyName { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string ContactName { get; set; }
        public string Phone { get; set; }
        public Guid AgentId { get; set; }
        public bool EstimatedCostAmountPopulated { get; set; }
        public decimal EstimatedCostAmount { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public bool IsSystemGenerated { get; set; }

        public Guid Id
        {
            get;
            set;
        }

        public string ServiceTDesc
        {
            get { return LendersOffice.Rolodex.RolodexDB.GetTypeDescription(ServiceT); }
        }

        public string GetEstimatedCostAmount_rep(LosConvert converter)
        {
            if (converter != null)
            {
                return converter.ToMoneyString(this.EstimatedCostAmount, FormatDirection.ToRep);
            }
            else
            {
                return string.Empty;
            }
        }

        public void SetEstimatedCostAmount_rep(LosConvert converter, string value)
        {
            if (converter != null)
            {
                this.EstimatedCostAmount = converter.ToMoney(value);
            }
        }

        public override string ToString()
        {
            return string.Format("{0}: {1} - {2}{3}{4} {5}", ServiceTDesc, CompanyName, 
                Tools.FormatSingleLineAddress(StreetAddress, City, State, Zip), 
                Environment.NewLine, ContactName, Phone);
        }

        public bool IsEqual(SettlementServiceProvider other)
        {
            if (other == null)
            {
                return false;
            }

            return this.ServiceT == other.ServiceT &&
                this.CompanyName == other.CompanyName &&
                this.StreetAddress == other.StreetAddress &&
                this.City == other.City &&
                this.State == other.State &&
                this.Zip == other.Zip &&
                this.ContactName == other.ContactName &&
                this.Phone == other.Phone &&
                this.AgentId == other.AgentId &&
                this.EstimatedCostAmount == other.EstimatedCostAmount &&
                this.EstimatedCostAmountPopulated == other.EstimatedCostAmountPopulated &&
                this.Email == other.Email &&
                this.Fax == other.Fax &&
                this.IsSystemGenerated == other.IsSystemGenerated &&
                this.Id == other.Id;
        }

        public bool IsDataComplete()
        {
            return !string.IsNullOrWhiteSpace(this.CompanyName)
                && !string.IsNullOrWhiteSpace(this.StreetAddress)
                && !string.IsNullOrWhiteSpace(this.City)
                && !string.IsNullOrWhiteSpace(this.State)
                && !string.IsNullOrWhiteSpace(this.Zip)
                && !string.IsNullOrWhiteSpace(this.Phone);
        }

        public void GenerateIdIfEmpty()
        {
            if (this.Id == Guid.Empty)
            {
                this.Id = Guid.NewGuid();
            }
        }

        [OnSerializing]
        private void PrepareForSerialization(StreamingContext context)
        {
            this.GenerateIdIfEmpty();
        }
    }

    public class TitleInsuranceProvider : SettlementServiceProvider
    {
        public override E_AgentRoleT ServiceT
        {
            get { return E_AgentRoleT.Title; }
        }
    }

    public class EscrowProvider : SettlementServiceProvider
    {
        public override E_AgentRoleT ServiceT
        {
            get { return E_AgentRoleT.Escrow; }
        }
    }

    public class SurveyProvider : SettlementServiceProvider
    {
        public override E_AgentRoleT ServiceT
        {
            get { return E_AgentRoleT.Surveyor; }
        }
    }

    public class PestInspectionProvider : SettlementServiceProvider
    {
        public override E_AgentRoleT ServiceT
        {
            get { return E_AgentRoleT.PestInspection; }
        }
    }

    public class SettlementServiceProviderComparer : IEqualityComparer<SettlementServiceProvider>
    {
        // Service Providers are equal if all properties are equal.
        public bool Equals(SettlementServiceProvider x, SettlementServiceProvider y)
        {
            // Check whether the compared objects reference the same data. 
            if (Object.ReferenceEquals(x, y))
            {
                return true;
            }

            // Check whether any of the compared objects is null. 
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
            {
                return false;
            }

            return x.ServiceT == y.ServiceT
                && x.CompanyName == y.CompanyName
                && x.StreetAddress == y.StreetAddress
                && x.City == y.City
                && x.State == y.State
                && x.Zip == y.Zip
                && x.ContactName == y.ContactName
                && x.Phone == y.Phone
                && x.AgentId == y.AgentId
                && x.Email == y.Email
                && x.Fax == y.Fax
                && x.Id == y.Id;
        }

        // If Equals() returns true for a pair of objects, 
        // GetHashCode must return the same value for these objects. 

        public int GetHashCode(SettlementServiceProvider settlementServiceProvider)
        {
            // Check whether the object is null. 
            if (Object.ReferenceEquals(settlementServiceProvider, null)) return 0;

            // Get the hash code for the Service Type field if it is not null. 
            int hashServiceT = settlementServiceProvider.ServiceT.GetHashCode();

            // Get the hash code for the Company Name field. 
            int hashCompanyName = settlementServiceProvider.CompanyName == null ? 0 : settlementServiceProvider.CompanyName.GetHashCode();

            // Get the hash code for the Street Address field. 
            int hashStreetAddress = settlementServiceProvider.StreetAddress == null ? 0 : settlementServiceProvider.StreetAddress.GetHashCode();

            // Get the hash code for the City field. 
            int hashCity = settlementServiceProvider.City == null ? 0 : settlementServiceProvider.City.GetHashCode();

            // Get the hash code for the State field. 
            int hashState = settlementServiceProvider.State == null ? 0 : settlementServiceProvider.State.GetHashCode();

            // Get the hash code for the Zip Code field. 
            int hashZip = settlementServiceProvider.Zip == null ? 0 : settlementServiceProvider.Zip.GetHashCode();

            // Get the hash code for the Contact Name field. 
            int hashContactName = settlementServiceProvider.ContactName == null ? 0 : settlementServiceProvider.ContactName.GetHashCode();

            // Get the hash code for the Phone Number field. 
            int hashPhoneNumber = settlementServiceProvider.Phone == null ? 0 : settlementServiceProvider.Phone.GetHashCode();

            // Get the hash code for the Agent Id field. 
            int hashAgentId = settlementServiceProvider.AgentId == Guid.Empty ? 0 : settlementServiceProvider.AgentId.GetHashCode();

            // Get the hash code for the Agent Email field.
            int hashEmail = settlementServiceProvider.Email == null ? 0 : settlementServiceProvider.Email.GetHashCode();

            // Get the hash code for the Agent Fax field.
            int hashFax = settlementServiceProvider.Fax == null ? 0 : settlementServiceProvider.Fax.GetHashCode();

            int hashId = settlementServiceProvider.Id == Guid.Empty ? 0 : settlementServiceProvider.Id.GetHashCode();

            // Calculate the hash code for the product. 
            return hashServiceT
                ^ hashCompanyName
                ^ hashStreetAddress
                ^ hashCity
                ^ hashState
                ^ hashZip
                ^ hashContactName
                ^ hashPhoneNumber
                ^ hashAgentId
                ^ hashEmail
                ^ hashFax
                ^ hashId;
        }
    }
}
