﻿using System;
using System.Xml;
using LendersOffice.Common;

namespace DataAccess
{
    public class AggregateEscrowAccount_Settlement : AggregateEscrowAccount
    {
        public AggregateEscrowAccount_Settlement(CPageBase dataLoan) : base(dataLoan) 
        {
        }
        /// <summary>
        /// Note if you update this, consider updating LFF sSettlementAggregateEscrowAccount
        /// </summary>
        protected override void SetAdjustment()
        {
            decimal sTotLenderRsrv = m_dataLoan.sSettlementTotLenderRsrv;
            m_aggregateEscrowAdjustment = m_initialDeposit - sTotLenderRsrv;
        }
    }
}