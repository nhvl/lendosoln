using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
	public class CClosingCostData : CPageData
	{
		private static CSelectStatementProvider s_selectProvider;
		static CClosingCostData()
		{
			StringList list = new StringList();

            list.Add("sfApplyCCTemplate");

			s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

		}

        public CClosingCostData( Guid fileId ) : base( fileId , "ClosingCostData" , s_selectProvider )
        {
        }

	}

}