using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
	public class CLiaData : CPageData
	{
        private bool enforceAccessControl = false;
		private static CSelectStatementProvider s_selectProvider;
		static CLiaData()
		{            

			StringList list = new StringList();

            list.Add("aAsstLiaCompletedNotJointly");
            list.Add( "aAppNm" );
            list.Add( "aLiaBalTot" );
            list.Add( "aLiaCollection" );
            list.Add( "aLiaMonTot" );
            list.Add( "aLiaPdOffTot" );
            list.Add( "sLiaBalLTot" );
            list.Add( "sLiaMonLTot" );
            list.Add( "sRefPdOffAmt" );
            list.Add("sTransmOMonPmtPe");
            list.Add("aTransmOMonPmtPe");
            list.Add("sLienToPayoffTotDebt");
            list.Add("sTridTargetRegulationVersionT");
            list.Add("sTridTargetRegulationVersionTLckd");
            list.Add("sLienPosT");
            list.Add("sLienPosTPelckd");
            list.Add("sLienPosTPeval");
            list.Add("sIsOFinNew");
            list.Add("sLPurposeT");
            list.Add("sLinkedLoanInfo");
            list.Add("sLNm");
            list.Add("sBrokerId");
            list.Add("sConcurSubFin");

			s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

		}

        public CLiaData( Guid fileId , bool enforceAccessControlForSave) : base( fileId , "Liability" , s_selectProvider )
        {
            this.enforceAccessControl = enforceAccessControlForSave;
        }

        protected override bool m_enforceAccessControl
        {
            get
            {
                return enforceAccessControl;
            }
        }

	}

	public class CLiaRecordData : CPageData
	{
		private static CSelectStatementProvider s_selectProvider;
		static CLiaRecordData()
		{
			StringList list = new StringList();;

            list.Add("aAsstLiaCompletedNotJointly");
            list.Add("aBEquifaxScore" );
            list.Add("aBExperianScore" );
            list.Add("aBFirstNm" );
            list.Add("aBLastNm" );
            list.Add("aBNm" );
            list.Add("aBSsn");
            list.Add("aBTransUnionScore" );
            list.Add("aCEquifaxScore" );
            list.Add("aCExperianScore" );
            list.Add("aCFirstNm" );
            list.Add("aCLastNm" );
            list.Add("aCNm" );
            list.Add("aCrOd");
            list.Add("aCrRd");
            list.Add("aCTransUnionScore" );
            list.Add("aLiaBalTot" );
            list.Add("aLiaCollection" );
            list.Add("aLiaMonTot" );
            list.Add("aReCollection" );
            list.Add("sfGetAgentOfRole");
            list.Add("sLT" );
            list.Add("sLenderNumVerif");
            list.Add("sLiaBalLTot" );
            list.Add("sLiaMonLTot" );
            list.Add("sPreparerXmlContent");
            list.Add("sRefPdOffAmt" );
            list.Add("sfUpdateCreditReportDirtyBit");
            list.Add("sHas1stTimeBuyerPe");
            list.Add("sProdHasHousingHistory");
            list.Add("aBAliases");
            list.Add("aCAliases");
            list.Add("sLienToPayoffTotDebt");
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

		}

        public CLiaRecordData( Guid fileId ) : base( fileId , "LiabilityRecord" , s_selectProvider )
        {
        }

	}

}
