using System;
using System.Collections;
using LendersOffice.Constants;

namespace DataAccess
{

	public class DataAccessUtils
	{
        /// <summary>
        /// Dependency fields when render PML Certificate.
        /// </summary>
        /// <param name="list"></param>
        public static void AddCertificateFields(StringList list) 
        {
            list.Add("sfTransformLoPropertyTypeToPml");
            list.Add("aBEquifaxScore");  
            list.Add("aBExperianScore");  
            list.Add("aBNm");
            list.Add("aBSsn");
            list.Add("aBTransUnionScore");  
            list.Add("aCEquifaxScore"); 
            list.Add("aCExperianScore");  
            list.Add("aCNm");
            list.Add("aCSsn");
            list.Add("aCTransUnionScore");  
            list.Add("aProdBCitizenT");
            list.Add("sAgentCollection");
            list.Add("sCltvR");  
            list.Add("sCreditScoreType1");
            list.Add("sCreditScoreType2");
            list.Add("sCreditScoreType3");
            list.Add("sEmployeeLenderAccExec");
            list.Add("sEmployeeUnderwriter");
            list.Add("sFinMethT");
            list.Add("sHouseVal");
            list.Add("sIsIOnly");
            list.Add("sIsSelfEmployed");
            list.Add("sLAmtCalc");
            list.Add("sLNm");
            list.Add("sLPurposeT");  
            list.Add("sLT");
            list.Add("sLiaMonLTot");  
            list.Add("sLienPosT");
            list.Add("sLpeNotesFromBrokerToUnderwriterHistory");
            list.Add("sLtvR");
            list.Add("sNoteIR");
            list.Add("sLTotI");
            list.Add("sOccT");
            list.Add("sPpmtPenaltyMon");
            list.Add("sPreparerXmlContent");
            list.Add("sProOHExp");
            list.Add("sProRealETx");
            list.Add("sProdCashoutAmt");
            list.Add("sProdCondoStories");
            list.Add("sProdDocT");
            list.Add("sProdImpound");
            list.Add("sProdIsCreditScoresAltered");
            list.Add("sProdPpmtPenaltyMon");
            list.Add("sProdRLckdDays");
            list.Add("sProdSpT");
            list.Add("sQualIR");
            list.Add("sRLckdDays");
            list.Add("sSpState");
            list.Add("sStatusT");
            list.Add("sTerm");
            list.Add("sTransmOMonPmt");
            list.Add("sUnitsNum");
            list.Add("sfSubmitFromPML");
            list.Add(nameof(CPageBase.SwapFor2ndLienQualifying));
            list.Add(nameof(CPageBase.MakeThisInto2ndLienForQualifying));
            list.Add(nameof(CPageBase.sClosingCostSetJsonContent));
            list.Add(nameof(CPageBase.sFeeServiceApplicationHistoryXmlContent));
            list.Add("sDue");
            list.Add("Is1stTimeHomeBuyer");
            list.Add("aIsCreditReportOnFile");
            list.Add("sEstCloseD");
            list.Add("sPrimAppTotNonspI");
            list.Add("sSpAddr");
            list.Add("sSpCity");
            list.Add("sSpZip");
            list.Add("sSpCounty");
            list.Add("sAgentCollection");
            list.Add("sProdAvailReserveMonths");
            list.Add("sProdSpStructureT");
            list.Add("sProd3rdPartyUwResultT");
            list.Add("sProdIsSpInRuralArea");
            list.Add("sProdIsCondotel");
            list.Add("sProdIsNonwarrantableProj");
            list.Add("aIsBorrSpousePrimaryWageEarner");
            list.Add("aBHasSpouse");
			list.Add("sLpDPmtT");
			list.Add("sBranchId");
			list.Add("sIsOptionArm");
			list.Add("sProdMIOptionT");
			list.Add("sProdHasHousingHistory");
			list.Add("sProdEstimatedResidualI");
			list.Add("sHas1stTimeBuyer");
			list.Add("sProd3rdPartyUwProcessingT"); // 04/26/07 mf - OPM 12103 AU Processing
            list.Add("sPresLTotPersistentHExp");
            list.Add("sOccR");
            list.Add("sSpGrossRent");
            list.Add("sOccR");
            list.Add("sSpGrossRent");
			list.Add("sIsStandAlone2ndLien"); // OPM 4442
			list.Add("sLpIsNegAmortOtherLien");
			list.Add("sOtherLFinMethT");
			list.Add("sProOFinPmt");
            list.Add("sProdIncludeMyCommunityProc");
            list.Add("sProdIncludeHomePossibleProc");
            list.Add("sProdIncludeNormalProc");
            list.Add("sProdIncludeFHATotalProc");
            list.Add("sProdIncludeVAProc");
            list.Add("sProdIsDuRefiPlus");
            list.Add( "sLpProductType" );
            list.Add("sIsCreditQualifying");
            list.Add("sProdVaFundingFee");
            list.Add("sProdIsVaFundingFinanced");
            list.Add("sOriginalAppraisedValue");
            list.Add("sHasAppraisal");
            list.Add("sOriginatorCompPoint");
            list.Add("sOriginatorCompensationPaymentSourceT");
            list.Add("sOriginatorCompForCert");
            list.Add("sOriginatorCompensationEffectiveD");
            list.Add("sIsQualifiedForOriginatorCompensation");
            list.Add("sOriginatorCompensationLenderFeeOptionT");
            list.Add("sAppraisedValueCltvR_rep");
            list.Add("sShowAppraisedBasedLtvsOnCert");
            list.Add("sAppraisedValueLtvR_rep");
            list.Add("sPriorSalesD");
            list.Add("sPriorSalesPrice");
            list.Add("sPriorSalesPropertySellerT");
            list.Add("sProdIncludeUSDARuralProc");
            list.Add("sHCLTVRPe");
            list.Add("sSubFinT");
            list.Add("sHas2ndFinPe");
            list.Add("sLpProductT");
            list.Add("sConcurSubFin");
            // start 8/20/2012 sk opm 64482 
            list.Add("sIsRenovationLoan");
            list.Add("sAsCompletedValuePeval");
            list.Add("sTotalRenovationCosts");
            list.Add("sApprValPe");
            // end 8/20/2012 sk opm 64482 
            list.Add("sfSetPricingState");
            list.Add("sPriceGroup");
            list.Add("sIsEmployeeLoan");
            list.Add("sCreditLineAmt");
            list.Add("sfSetLoanOfficerPricingPolicyFromLORecord");
            list.Add("sIsCustomPricingPolicySet");
            list.Add("sLenderFeeBuyoutRequestedT");
            list.Add("sOriginatorCompensationFixedAmount");
            list.Add("aBFicoScore");
            list.Add("aCFicoScore");
            list.Add("sMaxR");
            list.Add("sApprVal");
            list.Add("sQualBottomR");
            // 12/31/12 gf opm 90750
            for (int i = 1; i <= ConstApp.MAX_NUMBER_CUSTOM_PML_FIELDS; i++)
            {
                list.Add(String.Format("sCustomPMLField{0}", i));
            }

            list.Add("sIsRequireFeesFromDropDown"); // OPM 169207
            list.Add("sIsCurrentLockUpdated"); // OPM 125577

            list.Add("sEmployeeExternalSecondary");
            list.Add("sEmployeeExternalPostCloser");
            list.Add("sBranchChannelT");
            list.Add("sPmlBrokerId");
            list.Add("GetLastDisclosedLoanEstimate");
            list.Add("aBTotI");
            list.Add("aCTotI");
            list.Add("sIsRateLocked");
            list.Add("sIsInvestorRateLocked");
            list.Add("sClearToCloseD");
            list.Add("sEmployeeDocDrawer");
            list.Add("sEmployeeLockDesk");
            list.Add("sEmployeeCreditAuditor");
            list.Add("sEmployeeFunder");
            list.Add("sPurchPrice");
            list.Add("sQualTopR");
            list.Add("sStatusD");
            list.Add("sDocsOrderedD");
            list.Add("sDocsD");
            list.Add("sDocsBackD");
            list.Add("sSubmitD");
            list.Add("sApprovD");
            list.Add("sAppExpD");
            list.Add("sConditionReviewD");
            list.Add("sSuspendedD");
            list.Add("sApprRprtOd");
            list.Add("sApprRprtDueD");
            list.Add("sApprRprtRd");
            list.Add("sLoanEstimateDatesInfo");
            list.Add("sClosingDisclosureDatesInfo");
            list.Add("sLoanSubmittedD");
            list.Add("sHasOriginatorCompensationPlan");
            list.Add("sHighPricedMortgageT");
            list.Add("sApprRprtExpD");
            list.Add("sProThisMPmt");
            list.Add("sRAdjMarginR");
        }
	}
}
