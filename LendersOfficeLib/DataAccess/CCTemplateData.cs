namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using Adapter;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.TitleProvider;
    using LendersOffice.Security;
    using System.Text;

    public abstract class CTemplateData : CBase
    {
        private LosConvert _m_convertLos;

        public override LosConvert m_convertLos => _m_convertLos ?? (_m_convertLos = new LosConvert());

        protected CTemplateData(Guid fileId, string pageName, E_DataObjectType objType)
            : base(fileId, pageName, objType)
        {
        }

        abstract public void InitLoad();

        // Call this first then start update dataobject with the values from
        // enabled controls.
        abstract public void InitSave();


        protected abstract Tuple<string, List<SqlParameter>> GetTemplateSelectString();

        protected string GetRateField_rep(string strFieldNm)
        {
            try
            {
                return m_convertLos.ToRateString(GetRateField(strFieldNm));
            }
            catch { }
            return "";
        }

        protected string GetRateField_rep(decimal value)
        {
            try
            {
                return m_convertLos.ToRateString(value);
            }
            catch { }
            return "";
        }

        protected string GetMoneyField_rep(string strFieldNm)
        {
            try
            {
                return m_convertLos.ToMoneyString(GetMoneyField(strFieldNm), FormatDirection.ToRep);
            }
            catch { }
            return "";
        }

        protected string GetMoneyField_rep(Func<Decimal> value)
        {
            try
            {
                return m_convertLos.ToMoneyString(value(), FormatDirection.ToRep);
            }
            catch { }
            return "";
        }


        protected string GetCountField_rep(string strFieldNm)
        {
            try
            {
                return m_convertLos.ToCountString(GetCountField(strFieldNm));
            }
            catch { }
            return "";
        }
        protected string GetBigCountField_rep(string strFieldNm)
        {
            try
            {
                return m_convertLos.ToBigCountString(GetBigCountField(strFieldNm));
            }
            catch { }
            return "";
        }


        abstract public void ApplyTo(Guid loanFileId, bool bApplyCcTemplate);
    }

    public class CCcTemplateInfo
    {
        public Guid Id { get; private set; }
        public String Name { get; private set; }
        public int Version { get; private set; }
        public string GfeVersion { get; private set; }
        internal CCcTemplateInfo(DbDataReader reader)
        {
            Id = (Guid) reader["cCcTemplateId"];
            Name = (string)reader["cCcTemplateNm"];
            Version = (int) reader["GfeVersion"];
            GfeVersion = Version == 0 ? "2008" : "2010";
        }
    }

    public class CCcTemplateData : CCcTemplateBase
    {
        public static List<string> ListClosingCostTemplateName(AbstractUserPrincipal principal)
        {
            return ListClosingCostTemplate(principal, reader => (string)reader["cCcTemplateNm"], -1);
        }

        public static IEnumerable<CCcTemplateInfo> RetrieveManualClosingCostTemplateInfo(AbstractUserPrincipal principal, int gfeVersion = -1)
        {
            return ListClosingCostTemplate(principal, reader => new CCcTemplateInfo(reader), gfeVersion);
        }

        public static List<T> ListClosingCostTemplate<T>(AbstractUserPrincipal principal, Func<DbDataReader, T> selector, int gfeVersion = -1)
        {
            if (!principal.HasPermission(Permission.CanAccessCCTemplates))
            {
                throw new AccessDenied();
            }
            return ListClosingCostTemplate(principal.BrokerId, selector, gfeVersion);
        }
        public static List<T> ListClosingCostTemplate<T>(Guid brokerId, Func<DbDataReader, T> selector, int gfeVersion = -1)
        {
            var list = new List<T>();

            var parameters = new[]
            {
                new SqlParameter("@BrokerID", brokerId),
                gfeVersion >= 0 ? new SqlParameter("@GfeVersion", gfeVersion) : null,
            }.Where(p => p != null);

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "ListClosingCostByBrokerID", parameters))
            {
                while (reader.Read())
                {
                    list.Add(selector(reader));
                }
            }

            return list;
        }

        /// <summary>
        /// List Closing Cost By Broker ID.
        /// </summary>
        /// <param name="brokerId">Broker ID.</param>
        /// <param name="gfeVersion">GFR Version.</param>
        /// <returns>DataSet that fill with Closing Costs records.</returns>
        public static DataSet GetClosingCostDataSet(Guid brokerId, int gfeVersion = -1)
        {
            var parameters = new[]
            {
                new SqlParameter("@BrokerId", brokerId),
                gfeVersion >= 0 ? new SqlParameter("@GfeVersion", gfeVersion) : null,
            }.Where(x => x != null);

            var ds = new DataSet();
            DataSetHelper.Fill(ds, brokerId, "ListClosingCostByBrokerID", parameters);
            return ds;
        }

        public static CCcTemplateData RetrieveClosingCostTemplateById(AbstractUserPrincipal principal, Guid cCcTemplateId)
        {
            if (principal.HasPermission(Permission.CanAccessCCTemplates) == false)
            {
                //throw new AccessDenied();
            }
            CCcTemplateData cc = new CCcTemplateData(principal.BrokerId, cCcTemplateId);
            cc.InitLoad();
            if (cc.BrokerId != principal.BrokerId)
            {
                throw new AccessDenied();
            }
            return cc;
        }
        public static CCcTemplateData RetrieveClosingCostTemplateByName(AbstractUserPrincipal principal, string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new CBaseException("Template name cannot be empty", "Template name cannot be empty");
            }
            if (principal.HasPermission(Permission.CanAccessCCTemplates) == false)
            {
                throw new AccessDenied();
            }
            Guid cCcTemplateId = Guid.Empty;

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerID", principal.BrokerId),
                                            new SqlParameter("@cCcTemplateNm", name)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(principal.BrokerId, "CC_TEMPLATE_RetrieveIdByName", parameters))
            {
                if (reader.Read())
                {
                    cCcTemplateId = (Guid)reader["cCcTemplateId"];
                }
            }
            if (cCcTemplateId == Guid.Empty)
            {
                throw new NotFoundException($"Closing Cost Template [{name}] does not existed.");
            }
            return RetrieveClosingCostTemplateById(principal, cCcTemplateId);

        }
        public CCcTemplateData(Guid brokerId, DataRow rowCcTemplate)
            : base(brokerId, rowCcTemplate)
        { }

        public CCcTemplateData(Guid brokerId, Guid fileId)
            : base(brokerId, fileId)
        { }

        protected override Tuple<string, List<SqlParameter>> GetTemplateSelectString()
        {
            var sql = "SELECT * FROM CC_TEMPLATE WHERE cCcTemplateId = @fileid";
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@fileid", this.m_fileId));
            return new Tuple<string, List<SqlParameter>>(sql, parameters);
        }
    }

    abstract public class CCcTemplateBase : CTemplateData
    {
        public DbProviderFactory dbFactory = DbAccessUtils.GetDbProvider();

        public QuoteResultData PricingQuote { get; private set; }

        public void SetPricingQuotes(QuoteResultData quote)
        {
            PricingQuote = quote;
        }

        public decimal GetPercentFromQuoteOrDb(string field)
        {
            if (PricingQuote == null)
            {
                return GetRateField(field);
            }
            Func<bool> hasValue = () => false;

            switch (field)
            {
                case "cCountyRtcPc":
                    hasValue = () => PricingQuote.IsCountyRtcSet;
                    break;
                case "cOwnerTitleInsFPc":
                    hasValue = () => PricingQuote.IsOwnerTitleInsFSet;
                    break;
                case "cStateRtcPc":
                    hasValue = () => PricingQuote.IsStateRtcSet;
                    break;
                case "cTitleInsFPc":
                    hasValue = () => PricingQuote.IsTitleInsFSet;
                    break;
                case "cRecFPc":
                    hasValue = () => PricingQuote.IsRecFSet;
                    break;
                case "cEscrowFPc":
                    hasValue = () => PricingQuote.IsEscrowFSet;
                    break;
                default:
                    Tools.LogError("calling GetMoneyFromQuoteOrDb with unexpected field. " + field);
                    break;
            }

            return hasValue() ? 0 : GetRateField(field);
        }

        public decimal GetMoneyFromQuoteOrDb(string field)
        {
            if (PricingQuote == null)
            {
                return GetMoneyField(field);
            }
            Func<bool> hasValue = () => false;
            Func<decimal> getValue = () => 0;
            switch (field)
            {
                case "cCountyRtcMb":
                    hasValue = () =>  PricingQuote.IsCountyRtcSet;
                    getValue = () => PricingQuote.sCountyRtc;
                    break;
                case "cNotaryF":
                    hasValue = () => PricingQuote.IsNotaryFSet;
                    getValue = () => PricingQuote.sNotaryF;
                    break;
                case "cOwnerTitleInsFMb":
                    hasValue = () => PricingQuote.IsOwnerTitleInsFSet;
                    getValue = () => PricingQuote.sOwnerTitleInsF;
                    break;
                case "cStateRtcMb":
                    hasValue = () => PricingQuote.IsStateRtcSet;
                    getValue = () => PricingQuote.sStateRtc;
                    break;
                case "cTitleInsFMb":
                    hasValue = () => PricingQuote.IsTitleInsFSet;
                    getValue = () => PricingQuote.sTitleInsF;
                    break;
                case "cRecFMb":
                    hasValue = () => PricingQuote.IsRecFSet;
                    getValue = () => PricingQuote.sRecF;
                    break;
                case "cEscrowFMb":
                    hasValue = () => PricingQuote.IsEscrowFSet;
                    getValue = () => PricingQuote.sEscrowF;
                    break;
                default:
                    Tools.LogError("calling GetMoneyFromQuoteOrDb with unexpected field. " + field);
                    break;
            }

            return hasValue() ? getValue() : GetMoneyField(field);
        }

        public static Guid CreateCCTemplate(Guid brokerId, int GfeVersion)
        {
            var parameters = new List<SqlParameter> {new SqlParameter("@BrokerId", brokerId)};
            var outTemplateId = new SqlParameter("@cCCTemplateId", Guid.Empty)
            {
                Direction = ParameterDirection.Output
            };
            parameters.Add(outTemplateId);
            parameters.Add(new SqlParameter("@GfeVersion", GfeVersion));

            if (0 < StoredProcedureHelper.ExecuteNonQuery(brokerId, "CreateBlankCCTemplate", 0, parameters))
            {
                return new Guid(outTemplateId.Value.ToString());
            }
            throw new CBaseException(ErrorMessages.Generic, "Failed to create new closing cost template.");
        }

        public static Guid CreateAutomatedCCTemplate(Guid brokerId, int automationId )
        {
            SqlParameter outTemplateId = new SqlParameter("@cCCTemplateId", Guid.Empty)
            {
                Direction = ParameterDirection.Output
            };

            var parameters = new List<SqlParameter>
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@CCTemplateSystemId", automationId),
                outTemplateId,
                new SqlParameter("@GfeVersion", 1)
            };

            if (0 < StoredProcedureHelper.ExecuteNonQuery(brokerId, "CreateBlankCCTemplate", 0, parameters))
            {
                return new Guid(outTemplateId.Value.ToString());
            }

            throw new CBaseException(ErrorMessages.Generic, "Failed to create new closing cost template.");
        }

        public static Guid DuplicateCCTemplate(Guid brokerId, int GfeVersion, Guid cCcTemplateId)
        {
            SqlParameter outTemplateId = new SqlParameter("@cCCTemplateId", Guid.Empty)
            {
                Direction = ParameterDirection.Output
            };

            var parameters = new List<SqlParameter>
            {
                new SqlParameter("@BrokerId", brokerId),
                outTemplateId,
                new SqlParameter("@GfeVersion", GfeVersion),
                new SqlParameter("@origTemplateId", cCcTemplateId)
            };

            if (0 < StoredProcedureHelper.ExecuteNonQuery(brokerId, "CC_TEMPLATE_CreateDuplicate", 0, parameters))
            {
                return new Guid(outTemplateId.Value.ToString());
            }

            throw new CBaseException(ErrorMessages.Generic, "Failed to create new closing cost template.");
        }

        public static void DelCCTemplate(Guid brokerId, Guid cCcTemplateId)
        {

            SqlParameter[] parameters = {
                                            new SqlParameter("@cCcTemplateId", cCcTemplateId)
                                        };
            try
            {
                if (0 == StoredProcedureHelper.ExecuteNonQuery(brokerId, "DeleteCCTemplate", 0, parameters))
                {
                    throw new CBaseException(ErrorMessages.Generic, "Failed to delete closing cost template.");
                }
            }
            catch (SqlException e)
            {
                if (e.Message.Contains("REFERENCE constraint"))
                {
                    throw new CBaseException("The template is in use by another feature and cannot be deleted.", e);
                }

                throw CBaseException.GenericException(e.Message);
            }
        }

        private DataSet m_ds;
        private DataRowContainer m_row;
        private StringCollection m_updatedFields;
        protected DbCommand m_cmdUpdate;
        private Guid m_brokerIdForSqlConnection = Guid.Empty;

        public CCcTemplateBase(Guid brokerId, DataRow rowCcTemplate)
            : base(new Guid(rowCcTemplate["cCcTemplateId"].ToString()), "CC Template", E_DataObjectType.CCTemplate)
        {
            m_brokerIdForSqlConnection = brokerId;
            m_row = new DataRowContainer(rowCcTemplate);
        }

        public CCcTemplateBase(Guid brokerId, Guid fileId)
            : base(fileId, "CC Template", E_DataObjectType.CCTemplate)
        {
            m_brokerIdForSqlConnection = brokerId;
            m_updatedFields = new StringCollection();
            m_cmdUpdate = this.dbFactory.CreateCommand();
        }

        protected override IDataContainer GetDataRow()
        {
            return m_row;
        }

        protected override void OnFieldChange(string fieldName)
        {
            // NO-OP
        }
        protected override void OnFieldChange(string strFieldNm, SqlDbType dbType, object oldValue, object newValue)
        {
            if (!m_updatedFields.Contains(strFieldNm))
            {
                var param = new SqlParameter("@p" + strFieldNm, dbType)
                {
                    SourceVersion = DataRowVersion.Current,
                    SourceColumn = strFieldNm
                };

                m_cmdUpdate.Parameters.Add(param);
                m_updatedFields.Add(strFieldNm);
            }
        }

        protected override int GetColumnMaxLength(string strFieldNm)
        {
            DataTable table = m_ds.Tables["TEMPLATE"];
            return table.Columns[strFieldNm].MaxLength;
        }

        override public void InitLoad()
        {
            using (var conn = DbAccessUtils.GetConnection(m_brokerIdForSqlConnection))
            {
                InitLoad(conn);
            }
        }

        public void InitLoad(DbConnection conn)
        {
            try
            {
                var data = GetTemplateSelectString();

                if (data.Item1.Length == 0)
                {
                    return;
                }

                this.m_ds = new DataSet("TEMPLATE");
                PopulateDataSet(conn, data.Item1, data.Item2, this.m_ds);

                this.m_row = new DataRowContainer(m_ds.Tables["TEMPLATE"].Rows[0]);
            }
            catch (System.Exception ex)
            {
                LogError(ex.Message + ". Stack trace:\n" + ex.StackTrace);
                throw ex;
            }
        }

        public static void PopulateDataSet(DbConnection conn, string sql, List<SqlParameter> listParams, DataSet ds)
        {
            if (conn.State != ConnectionState.Open)
            {
                conn.OpenWithRetry();
            }

            using (DbTransaction tx = conn.BeginTransaction(IsolationLevel.RepeatableRead))
            {
                DBSelectUtility.FillDataSet(conn, tx, ds, sql, null, listParams);
                tx.Commit();
            }
        }

        override public void InitSave()
        {
            try
            {
                var data = GetTemplateSelectString();
                if (data.Item1.Length == 0)
                {
                    return;
                }

                this.m_ds = new DataSet("TEMPLATE");
                using (var conn = DbAccessUtils.GetConnection(m_brokerIdForSqlConnection))
                {
                    PopulateDataSet(conn, data.Item1, data.Item2, this.m_ds);
                }

                this.m_row = new DataRowContainer(m_ds.Tables["TEMPLATE"].Rows[0]);
            }
            catch (System.Exception ex)
            {
                LogError(ex.Message + ".  Stack:\n" + ex.StackTrace);
            }
        }

        public void Save()
        {
            // NOTE: This is going to take more thought before bringing into the new FOOL architecture.
            //       It isn't clear how m_cmdUpdate is used throughout the lifecycle of this class
            //       and I don't want to cause any unintended side-effects - AD
            DbConnection conn = null;
            DbProviderFactory factory = DbAccessUtils.GetDbProvider();

            try
            {
                conn = DbAccessUtils.GetConnection(m_brokerIdForSqlConnection);
                DbCommand cmdUpdate = null;
                if (m_ds != null && m_ds.HasChanges())
                {
                    cmdUpdate = PrepareUpdateCmd();
                }


                if (cmdUpdate != null)
                {
                    conn.OpenWithRetry();

                    DbTransaction tx = conn.BeginTransaction(IsolationLevel.Serializable);

                    if (cmdUpdate != null)
                    {
                        cmdUpdate.Connection = conn;
                        cmdUpdate.Transaction = tx;
                        DbDataAdapter da = factory.CreateDataAdapter();
                        da.UpdateCommand = cmdUpdate;
                        da.Update(m_ds, "TEMPLATE");
                    }

                    tx.Commit();
                }
            }
            finally
            {
                conn?.Close();
            }
        }

        protected DbCommand PrepareUpdateCmd()
        {
            if (m_updatedFields.Count > 0)
            {
                var param = new SqlParameter("@pcCcTemplateId", SqlDbType.UniqueIdentifier)
                {
                    SourceVersion = DataRowVersion.Original,
                    SourceColumn = "cCcTemplateId"
                };
                m_cmdUpdate.Parameters.Add(param);

                StringBuilder sFields = new StringBuilder(string.Empty);
                StringEnumerator eStr = m_updatedFields.GetEnumerator();
                while (eStr.MoveNext())
                {
                    if (sFields.Length > 0)
                    {
                        sFields.Append(", ");
                    }
                    sFields.Append($"{eStr.Current} = @p{eStr.Current}");
                }

                if (sFields.Length > 0)
                    m_cmdUpdate.CommandText = "Update CC_TEMPLATE set " + sFields + " WHERE cCcTemplateId = @pcCcTemplateId";
            }

            return (m_cmdUpdate.CommandText.Length > 0) ? m_cmdUpdate : null;
        }

        override public void ApplyTo(Guid loanFileId, bool bApplyCcTemplate)
        {
            Tools.Assert(bApplyCcTemplate, "bApplyCcTemplate must be true in ApplyTo() method of CCcTemplateBase.");
            CPageData loanData = new CClosingCostData(loanFileId);
            loanData.InitSave(ConstAppDavid.SkipVersionCheck);
            loanData.ApplyCCTemplate(this);
            bool oldBypassClosingCostSetValidation = loanData.BypassClosingCostSetValidation;
            loanData.BypassClosingCostSetValidation = true;
            loanData.Save();
            loanData.BypassClosingCostSetValidation = oldBypassClosingCostSetValidation;
        }


        public IEnumerable<HudFeeItem> HudFeeItemList
        {
            get
            {
                List<HudFeeItem> list = new List<HudFeeItem>();

                #region 800 Items Payable in connection with loan
                list.Add(new HudFeeItem("801", "Loan Origination Fee", cLOrigFPc, E_PercentBaseT.LoanAmount, cLOrigFMb, E_GfeSectionT.B1, cLOrigFProps));
                list.Add(new HudFeeItem("802", "Loan Discount", cLDiscntPc, cLDiscntBaseT, cLDiscntFMb, E_GfeSectionT.B2, cLDiscntProps));
                list.Add(new HudFeeItem(cApprFCode, "Appraisal Fee", 0, E_PercentBaseT.LoanAmount, cApprF, E_GfeSectionT.B3, cApprFProps));
                list.Add(new HudFeeItem(cCrFCode, "Credit Report", 0, E_PercentBaseT.LoanAmount, cCrF, E_GfeSectionT.B3, cCrFProps));
                list.Add(new HudFeeItem(cTxServFCode, "Tax Service Fee", 0, E_PercentBaseT.LoanAmount, cTxServF, E_GfeSectionT.B3, cTxServFProps));
                if (GfeVersion == E_GfeVersion.Gfe2010)
                {
                    list.Add(new HudFeeItem("807", "Flood Certification", 0, E_PercentBaseT.LoanAmount, cFloodCertificationF, E_GfeSectionT.B3, cFloodCertificationFProps));
                }
                list.Add(new HudFeeItem("808", "Mortgage Broker Fee", cMBrokFPc, cMBrokFBaseT, cMBrokFMb, E_GfeSectionT.B1, cMBrokFProps));
                list.Add(new HudFeeItem(cInspectFCode, "Lender's Inspection Fee", 0, E_PercentBaseT.LoanAmount, cInspectF, E_GfeSectionT.B1, cInspectFProps));
                list.Add(new HudFeeItem("810", "Processing Fee", 0, E_PercentBaseT.LoanAmount, cProcF, E_GfeSectionT.B1, cProcFProps));
                list.Add(new HudFeeItem("811", "Underwriting Fee", 0, E_PercentBaseT.LoanAmount, cUwF, E_GfeSectionT.B1, cUwFProps));
                list.Add(new HudFeeItem("812", "Wire Transfer", 0, E_PercentBaseT.LoanAmount, cWireF, E_GfeSectionT.B1, cWireFProps));
                list.Add(new HudFeeItem(c800U1FCode, c800U1FDesc, 0, E_PercentBaseT.LoanAmount, c800U1F, c800U1FGfeSection, c800U1FProps));
                list.Add(new HudFeeItem(c800U2FCode, c800U2FDesc, 0, E_PercentBaseT.LoanAmount, c800U2F, c800U2FGfeSection, c800U2FProps));
                list.Add(new HudFeeItem(c800U3FCode, c800U3FDesc, 0, E_PercentBaseT.LoanAmount, c800U3F, c800U3FGfeSection, c800U3FProps));
                list.Add(new HudFeeItem(c800U4FCode, c800U4FDesc, 0, E_PercentBaseT.LoanAmount, c800U4F, c800U4FGfeSection, c800U4FProps));
                list.Add(new HudFeeItem(c800U5FCode, c800U5FDesc, 0, E_PercentBaseT.LoanAmount, c800U5F, c800U5FGfeSection, c800U5FProps));

                #endregion

                #region 900 Items Required by lender to be paid in advance
                // OPM 120179 - Now only collecting the number of months paid
                // for haz. ins. in cc template.
                //list.Add(new HudFeeItem("903", "Hazard Insurance", cProHazInsR, cProHazInsT, cProHazInsMb, E_GfeSectionT.B11, cHazInsPiaProps));
                list.Add(new HudFeeItem("904", c904PiaDesc, 0, E_PercentBaseT.LoanAmount, c904Pia, c904PiaGfeSection, c904PiaProps));
                list.Add(new HudFeeItem(c900U1PiaCode, c900U1PiaDesc, 0, E_PercentBaseT.LoanAmount, c900U1Pia, c900U1PiaGfeSection, c900U1PiaProps));
                #endregion

                #region 1000 Reserves Deposited with lender
                // OPM 120179 - Now only collecting the number of months paid
                // for these lines in cc template.
                //list.Add(new HudFeeItem("1004", "Tax Reserve", cProRealETxR, cProRealETxT, cProRealETxMb, E_GfeSectionT.B9, cRealETxRsrvProps));
                //list.Add(new HudFeeItem(cProSchoolTxCode, "School Taxes", 0, E_PercentBaseT.LoanAmount, cProSchoolTx, E_GfeSectionT.B9, cSchoolTxRsrvProps));
                //list.Add(new HudFeeItem(cProFloodInsCode, "Flood Insurance Reserve", 0, E_PercentBaseT.LoanAmount, cProFloodIns, E_GfeSectionT.B9, cFloodInsRsrvProps));
                //list.Add(new HudFeeItem(c1006ProHExpCode, c1006ProHExpDesc, 0, E_PercentBaseT.LoanAmount, c1006ProHExp, E_GfeSectionT.B9, c1006RsrvProps));
                //list.Add(new HudFeeItem(c1007ProHExpCode, c1007ProHExpDesc, 0, E_PercentBaseT.LoanAmount, c1007ProHExp, E_GfeSectionT.B9, c1007RsrvProps));
                #endregion

                #region 1100 Title Charges
                list.Add(new HudFeeItem(cEscrowFCode, "Closing/Escrow Fee", cEscrowFPc, cEscrowFBaseT, cEscrowFMb, cEscrowFGfeSection, cEscrowFProps));
                if (GfeVersion == E_GfeVersion.Gfe2010)
                {
                    list.Add(new HudFeeItem("1103", "Owner's Title Insurance", cOwnerTitleInsFPc, cOwnerTitleInsFBaseT, cOwnerTitleInsFMb, E_GfeSectionT.B5, cOwnerTitleInsProps));
                }
                list.Add(new HudFeeItem(cTitleInsFCode, "Lender's Title Insurance", cTitleInsFPc, cTitleInsFBaseT, cTitleInsFMb, cTitleInsFGfeSection, cTitleInsFProps));
                list.Add(new HudFeeItem(cDocPrepFCode, "Doc Preparation Fee", 0, E_PercentBaseT.LoanAmount, cDocPrepF, cDocPrepFGfeSection, cDocPrepFProps));
                list.Add(new HudFeeItem(cNotaryFCode, "Notary Fees", 0, E_PercentBaseT.LoanAmount, cNotaryF, cNotaryFGfeSection, cNotaryFProps));
                list.Add(new HudFeeItem(cAttorneyFCode, "Attorney Fees", 0, E_PercentBaseT.LoanAmount, cAttorneyF, cAttorneyFGfeSection, cAttorneyFProps));
                list.Add(new HudFeeItem(cU1TcCode, cU1TcDesc, 0, E_PercentBaseT.LoanAmount, cU1Tc, cU1TcGfeSection, cU1TcProps));
                list.Add(new HudFeeItem(cU2TcCode, cU2TcDesc, 0, E_PercentBaseT.LoanAmount, cU2Tc, cU2TcGfeSection, cU2TcProps));
                list.Add(new HudFeeItem(cU3TcCode, cU3TcDesc, 0, E_PercentBaseT.LoanAmount, cU3Tc, cU3TcGfeSection, cU3TcProps));
                list.Add(new HudFeeItem(cU4TcCode, cU4TcDesc, 0, E_PercentBaseT.LoanAmount, cU4Tc, cU4TcGfeSection, cU4TcProps));

                #endregion

                #region 1200 Government Recording & Transfer Charges
                list.Add(new HudFeeItem("1201", "Recording Fees", cRecFPc, cRecBaseT, cRecFMb, E_GfeSectionT.B7, cRecFProps));
                list.Add(new HudFeeItem(cCountyRtcCode, "City tax/stamps", cCountyRtcPc, cCountyRtcBaseT, cCountyRtcMb, E_GfeSectionT.B8, cCountyRtcProps));
                list.Add(new HudFeeItem(cStateRtcCode, "State tax/stamps", cStateRtcPc, cStateRtcBaseT, cStateRtcMb, E_GfeSectionT.B8, cStateRtcProps));
                list.Add(new HudFeeItem(cU1GovRtcCode, cU1GovRtcDesc, cU1GovRtcPc, cU1GovRtcBaseT, cU1GovRtcMb, E_GfeSectionT.B8, cU1GovRtcProps));
                list.Add(new HudFeeItem(cU2GovRtcCode, cU2GovRtcDesc, cU2GovRtcPc, cU2GovRtcBaseT, cU2GovRtcMb, E_GfeSectionT.B8, cU2GovRtcProps));
                list.Add(new HudFeeItem(cU3GovRtcCode, cU3GovRtcDesc, cU3GovRtcPc, cU3GovRtcBaseT, cU3GovRtcMb, E_GfeSectionT.B8, cU3GovRtcProps));

                #endregion

                #region 1300 Additional Settlement Charges
                list.Add(new HudFeeItem("1302", "Pest Inspection", 0, E_PercentBaseT.LoanAmount, cPestInspectF, E_GfeSectionT.B6, cPestInspectFProps));
                list.Add(new HudFeeItem(cU1ScCode, cU1ScDesc, 0, E_PercentBaseT.LoanAmount, cU1Sc, cU1ScGfeSection, cU1ScProps));
                list.Add(new HudFeeItem(cU2ScCode, cU2ScDesc, 0, E_PercentBaseT.LoanAmount, cU2Sc, cU2ScGfeSection, cU2ScProps));
                list.Add(new HudFeeItem(cU3ScCode, cU3ScDesc, 0, E_PercentBaseT.LoanAmount, cU3Sc, cU3ScGfeSection, cU3ScProps));
                list.Add(new HudFeeItem(cU4ScCode, cU4ScDesc, 0, E_PercentBaseT.LoanAmount, cU4Sc, cU4ScGfeSection, cU4ScProps));
                list.Add(new HudFeeItem(cU5ScCode, cU5ScDesc, 0, E_PercentBaseT.LoanAmount, cU5Sc, cU5ScGfeSection, cU5ScProps));

                #endregion

                return list;
            }
        }
        public Guid BrokerId
        {
            get { return GetGuidField("BrokerId"); }
        }
        public Guid cCcTemplateId
        {
            get { return GetGuidField("cCcTemplateId"); }
            set { SetGuidField("cCcTemplateId", value); }
        }
        public string cCcTemplateNm
        {
            get { return GetStringVarCharField("cCcTemplateNm"); }
            set { SetStringVarCharField("cCcTemplateNm", value); }
        }
        public decimal cProHazInsR
        {
            get { return GetRateField("cProHazInsR"); }
            set { SetRateField("cProHazInsR", value); }
        }
        public string cProHazInsR_rep
        {
            get { return GetRateField_rep("cProHazInsR"); }
            set { cProHazInsR = m_convertLos.ToRate(value); }
        }
        public decimal cProRealETxR
        {
            get { return GetRateField("cProRealETxR"); }
            set { SetRateField("cProRealETxR", value); }
        }
        public string cProRealETxR_rep
        {
            get { return GetRateField_rep("cProRealETxR"); }
            set { cProRealETxR = m_convertLos.ToRate(value); }
        }
        public decimal cProMInsR
        {
            get { return GetRateField("cProMInsR"); }
            set { SetRateField("cProMInsR", value); }
        }
        public string cProMInsR_rep
        {
            get { return GetRateField_rep("cProMInsR"); }
            set { cProMInsR = m_convertLos.ToRate(value); }
        }

        public decimal cLOrigFPc
        {
            get { return GetRateField("cLOrigFPc"); }
            set { SetRateField("cLOrigFPc", value); }
        }
        public string cLOrigFPc_rep
        {
            get { return GetRateField_rep("cLOrigFPc"); }
            set { cLOrigFPc = m_convertLos.ToRate(value); }
        }

        public decimal cLOrigFMb
        {
            get { return GetMoneyField("cLOrigFMb"); }
            set { SetMoneyField("cLOrigFMb", value); }
        }
        public string cLOrigFMb_rep
        {
            get { return GetMoneyField_rep("cLOrigFMb"); }
            set { cLOrigFMb = m_convertLos.ToMoney(value); }
        }
        public int cFloodCertificationFProps
        {
            get { return Get32BitPropsField("cFloodCertificationFProps"); }
            set { Set32BitPropsField("cFloodCertificationFProps", value); }
        }
        public int cOwnerTitleInsProps
        {
            get { return Get32BitPropsField("cOwnerTitleInsProps"); }
            set { Set32BitPropsField("cOwnerTitleInsProps", value); }
        }
        public int cLOrigFProps
        {
            get { return Get32BitPropsField("cLOrigFProps"); }
            set { Set32BitPropsField("cLOrigFProps", value); }
        }
        public int cCrFProps
        {
            get { return Get32BitPropsField("cCrFProps"); }
            set { Set32BitPropsField("cCrFProps", value); }
        }

        public int cInspectFProps
        {
            get { return Get32BitPropsField("cInspectFProps"); }
            set { Set32BitPropsField("cInspectFProps", value); }
        }
        public int cMBrokFProps
        {
            get { return Get32BitPropsField("cMBrokFProps"); }
            set { Set32BitPropsField("cMBrokFProps", value); }
        }
        public int cTxServFProps
        {
            get { return Get32BitPropsField("cTxServFProps"); }
            set { Set32BitPropsField("cTxServFProps", value); }
        }
        public int cProcFProps
        {
            get { return Get32BitPropsField("cProcFProps"); }
            set { Set32BitPropsField("cProcFProps", value); }
        }
        public int cUwFProps
        {
            get { return Get32BitPropsField("cUwFProps"); }
            set { Set32BitPropsField("cUwFProps", value); }
        }
        public int cWireFProps
        {
            get { return Get32BitPropsField("cWireFProps"); }
            set { Set32BitPropsField("cWireFProps", value); }
        }
        public int c800U1FProps
        {
            get { return Get32BitPropsField("c800U1FProps"); }
            set { Set32BitPropsField("c800U1FProps", value); }
        }
        public int c800U2FProps
        {
            get { return Get32BitPropsField("c800U2FProps"); }
            set { Set32BitPropsField("c800U2FProps", value); }
        }
        public int c800U3FProps
        {
            get { return Get32BitPropsField("c800U3FProps"); }
            set { Set32BitPropsField("c800U3FProps", value); }
        }
        public int c800U4FProps
        {
            get { return Get32BitPropsField("c800U4FProps"); }
            set { Set32BitPropsField("c800U4FProps", value); }
        }
        public int c800U5FProps
        {
            get { return Get32BitPropsField("c800U5FProps"); }
            set { Set32BitPropsField("c800U5FProps", value); }
        }
        public int cIPiaProps
        {
            get { return Get32BitPropsField("cIPiaProps"); }
            set { Set32BitPropsField("cIPiaProps", value); }
        }
        public int cMipPiaProps
        {
            get { return Get32BitPropsField("cMipPiaProps"); }
            set { Set32BitPropsField("cMipPiaProps", value); }
        }
        public int cHazInsPiaProps
        {
            get { return Get32BitPropsField("cHazInsPiaProps"); }
            set { Set32BitPropsField("cHazInsPiaProps", value); }
        }
        public int c904PiaProps
        {
            get { return Get32BitPropsField("c904PiaProps"); }
            set { Set32BitPropsField("c904PiaProps", value); }
        }
        public int cVaFfProps
        {
            get { return Get32BitPropsField("cVaFfProps"); }
            set { Set32BitPropsField("cVaFfProps", value); }
        }
        public int c900U1PiaProps
        {
            get { return Get32BitPropsField("c900U1PiaProps"); }
            set { Set32BitPropsField("c900U1PiaProps", value); }
        }
        public int cHazInsRsrvProps
        {
            get { return Get32BitPropsField("cHazInsRsrvProps"); }
            set { Set32BitPropsField("cHazInsRsrvProps", value); }
        }
        public int cMInsRsrvProps
        {
            get { return Get32BitPropsField("cMInsRsrvProps"); }
            set { Set32BitPropsField("cMInsRsrvProps", value); }
        }
        public int cSchoolTxRsrvProps
        {
            get { return Get32BitPropsField("cSchoolTxRsrvProps"); }
            set { Set32BitPropsField("cSchoolTxRsrvProps", value); }
        }
        public int cRealETxRsrvProps
        {
            get { return Get32BitPropsField("cRealETxRsrvProps"); }
            set { Set32BitPropsField("cRealETxRsrvProps", value); }
        }
        public int cFloodInsRsrvProps
        {
            get { return Get32BitPropsField("cFloodInsRsrvProps"); }
            set { Set32BitPropsField("cFloodInsRsrvProps", value); }
        }
        public int c1006RsrvProps
        {
            get { return Get32BitPropsField("c1006RsrvProps"); }
            set { Set32BitPropsField("c1006RsrvProps", value); }
        }
        public int c1007RsrvProps
        {
            get { return Get32BitPropsField("c1007RsrvProps"); }
            set { Set32BitPropsField("c1007RsrvProps", value); }
        }
        public int cU3RsrvProps
        {
            get { return Get32BitPropsField("cU3RsrvProps"); }
            set { Set32BitPropsField("cU3RsrvProps", value); }
        }
        public int cU4RsrvProps
        {
            get { return Get32BitPropsField("cU4RsrvProps"); }
            set { Set32BitPropsField("cU4RsrvProps", value); }
        }
        public int cAggregateAdjRsrvProps
        {
            get { return Get32BitPropsField("cAggregateAdjRsrvProps"); }
            set { Set32BitPropsField("cAggregateAdjRsrvProps", value); }
        }
        public int cEscrowFProps
        {
            get { return Get32BitPropsField("cEscrowFProps"); }
            set { Set32BitPropsField("cEscrowFProps", value); }
        }
        public int cDocPrepFProps
        {
            get { return Get32BitPropsField("cDocPrepFProps"); }
            set { Set32BitPropsField("cDocPrepFProps", value); }
        }
        public int cNotaryFProps
        {
            get { return Get32BitPropsField("cNotaryFProps"); }
            set { Set32BitPropsField("cNotaryFProps", value); }
        }
        public int cAttorneyFProps
        {
            get { return Get32BitPropsField("cAttorneyFProps"); }
            set { Set32BitPropsField("cAttorneyFProps", value); }
        }
        public int cTitleInsFProps
        {
            get { return Get32BitPropsField("cTitleInsFProps"); }
            set { Set32BitPropsField("cTitleInsFProps", value); }
        }
        public int cU1TcProps
        {
            get { return Get32BitPropsField("cU1TcProps"); }
            set { Set32BitPropsField("cU1TcProps", value); }
        }
        public int cU2TcProps
        {
            get { return Get32BitPropsField("cU2TcProps"); }
            set { Set32BitPropsField("cU2TcProps", value); }
        }
        public int cU3TcProps
        {
            get { return Get32BitPropsField("cU3TcProps"); }

            set { Set32BitPropsField("cU3TcProps", value); }
        }
        public int cU4TcProps
        {
            get { return Get32BitPropsField("cU4TcProps"); }
            set { Set32BitPropsField("cU4TcProps", value); }
        }
        public int cRecFProps
        {
            get { return Get32BitPropsField("cRecFProps"); }
            set { Set32BitPropsField("cRecFProps", value); }
        }
        public int cCountyRtcProps
        {
            get { return Get32BitPropsField("cCountyRtcProps"); }
            set { Set32BitPropsField("cCountyRtcProps", value); }
        }
        public int cStateRtcProps
        {
            get { return Get32BitPropsField("cStateRtcProps"); }
            set { Set32BitPropsField("cStateRtcProps", value); }
        }
        public int cU1GovRtcProps
        {
            get { return Get32BitPropsField("cU1GovRtcProps"); }
            set { Set32BitPropsField("cU1GovRtcProps", value); }
        }
        public int cU2GovRtcProps
        {
            get { return Get32BitPropsField("cU2GovRtcProps"); }
            set { Set32BitPropsField("cU2GovRtcProps", value); }
        }
        public int cU3GovRtcProps
        {
            get { return Get32BitPropsField("cU3GovRtcProps"); }
            set { Set32BitPropsField("cU3GovRtcProps", value); }
        }
        public int cPestInspectFProps
        {
            get { return Get32BitPropsField("cPestInspectFProps"); }
            set { Set32BitPropsField("cPestInspectFProps", value); }
        }
        public int cU1ScProps
        {
            get { return Get32BitPropsField("cU1ScProps"); }
            set { Set32BitPropsField("cU1ScProps", value); }
        }
        public int cU2ScProps
        {
            get { return Get32BitPropsField("cU2ScProps"); }
            set { Set32BitPropsField("cU2ScProps", value); }
        }
        public int cU3ScProps
        {
            get { return Get32BitPropsField("cU3ScProps"); }
            set { Set32BitPropsField("cU3ScProps", value); }
        }
        public int cU4ScProps
        {
            get { return Get32BitPropsField("cU4ScProps"); }
            set { Set32BitPropsField("cU4ScProps", value); }
        }
        public int cU5ScProps
        {
            get { return Get32BitPropsField("cU5ScProps"); }
            set { Set32BitPropsField("cU5ScProps", value); }
        }
        public decimal cLDiscntPc
        {
            get { return GetRateField("cLDiscntPc"); }
            set { SetRateField("cLDiscntPc", value); }
        }
        public string cLDiscntPc_rep
        {
            get { return GetRateField_rep("cLDiscntPc"); }
            set { cLDiscntPc = m_convertLos.ToRate(value); }
        }
        public E_PercentBaseT cLDiscntBaseT
        {
            get { return (E_PercentBaseT)GetTypeIndexField("cLDiscntBaseT"); }
            set { SetTypeIndexField("cLDiscntBaseT", (int)value); }
        }
        public decimal cLDiscntFMb
        {
            get { return GetMoneyField("cLDiscntFMb"); }
            set { SetMoneyField("cLDiscntFMb", value); }
        }
        public string cLDiscntFMb_rep
        {
            get { return GetMoneyField_rep("cLDiscntFMb"); }
            set { cLDiscntFMb = m_convertLos.ToMoney(value); }
        }
        public int cLDiscntProps
        {
            get { return Get32BitPropsField("cLDiscntProps"); }
            set { Set32BitPropsField("cLDiscntProps", value); }
        }
        public int cLCreditProps
        {
            get { return Get32BitPropsField("cLCreditProps"); }
            set { Set32BitPropsField("cLCreditProps", value); }
        }
        public string cApprFCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return "803";
                    case E_GfeVersion.Gfe2010:
                        return "804";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }
            }
        }
        public decimal cApprF
        {
            get { return GetMoneyField("cApprF"); }
            set { SetMoneyField("cApprF", value); }
        }
        public string cApprF_rep
        {
            get { return GetMoneyField_rep("cApprF"); }
            set { cApprF = m_convertLos.ToMoney(value); }
        }

        public int cApprFProps
        {
            get { return Get32BitPropsField("cApprFProps"); }
            set { Set32BitPropsField("cApprFProps", value); }
        }
        public string cCrFCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return "804";
                    case E_GfeVersion.Gfe2010:
                        return "805";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }
            }
        }
        public decimal cCrF
        {
            get { return GetMoneyField("cCrF"); }
            set { SetMoneyField("cCrF", value); }
        }
        public string cCrF_rep
        {
            get { return GetMoneyField_rep("cCrF"); }
            set { cCrF = m_convertLos.ToMoney(value); }
        }
        public string cInspectFCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return "805";
                    case E_GfeVersion.Gfe2010:
                        return "809";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }
            }
        }
        public decimal cInspectF
        {
            get { return GetMoneyField("cInspectF"); }
            set { SetMoneyField("cInspectF", value); }
        }
        public string cInspectF_rep
        {
            get { return GetMoneyField_rep("cInspectF"); }
            set { cInspectF = m_convertLos.ToMoney(value); }
        }
        public decimal cMBrokFPc
        {
            get { return GetRateField("cMBrokFPc"); }
            set { SetRateField("cMBrokFPc", value); }
        }
        public string cMBrokFPc_rep
        {
            get { return GetRateField_rep("cMBrokFPc"); }
            set { cMBrokFPc = m_convertLos.ToRate(value); }
        }
        public decimal cMBrokFMb
        {
            get { return GetMoneyField("cMBrokFMb"); }
            set { SetMoneyField("cMBrokFMb", value); }
        }
        public string cMBrokFMb_rep
        {
            get { return GetMoneyField_rep("cMBrokFMb"); }
            set { cMBrokFMb = m_convertLos.ToMoney(value); }
        }
        public E_PercentBaseT cMBrokFBaseT
        {
            get { return (E_PercentBaseT) GetTypeIndexField("cMBrokFBaseT"); }
            set { SetTypeIndexField("cMBrokFBaseT", (int) value); }
        }
        public string cTxServFCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return "809";
                    case E_GfeVersion.Gfe2010:
                        return "806";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }
            }
        }
        public decimal cTxServF
        {
            get { return GetMoneyField("cTxServF"); }
            set { SetMoneyField("cTxServF", value); }
        }
        public string cTxServF_rep
        {
            get { return GetMoneyField_rep("cTxServF"); }
            set { cTxServF = m_convertLos.ToMoney(value); }
        }
        public decimal cProcF
        {
            get { return GetMoneyField("cProcF"); }
            set { SetMoneyField("cProcF", value); }
        }
        public string cProcF_rep
        {
            get { return GetMoneyField_rep("cProcF"); }
            set { cProcF = m_convertLos.ToMoney(value); }
        }
        public decimal cUwF
        {
            get { return GetMoneyField("cUwF"); }
            set { SetMoneyField("cUwF", value); }
        }
        public string cUwF_rep
        {
            get { return GetMoneyField_rep("cUwF"); }
            set { cUwF = m_convertLos.ToMoney(value); }
        }
        public decimal cWireF
        {
            get { return GetMoneyField("cWireF"); }
            set { SetMoneyField("cWireF", value); }
        }
        public string cWireF_rep
        {
            get { return GetMoneyField_rep("cWireF"); }
            set { cWireF = m_convertLos.ToMoney(value); }
        }

        public string c800U1FCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return GetStringVarCharField("c800U1FCode");
                    case E_GfeVersion.Gfe2010:
                        return "813";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }
            }
            set { SetStringVarCharField("c800U1FCode", value); }
        }

        public string c800U1FDesc
        {
            get { return GetStringVarCharField("c800U1FDesc"); }
            set { SetStringVarCharField("c800U1FDesc", value); }
        }
        public decimal c800U1F
        {
            get { return GetMoneyField("c800U1F"); }
            set { SetMoneyField("c800U1F", value); }
        }
        public string c800U1F_rep
        {
            get { return GetMoneyField_rep("c800U1F"); }
            set { c800U1F = m_convertLos.ToMoney(value); }
        }
        public string c800U2FCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return GetStringVarCharField("c800U2FCode");
                    case E_GfeVersion.Gfe2010:
                        return "814";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }
            }
            set { SetStringVarCharField("c800U2FCode", value); }
        }
        public string c800U2FDesc
        {
            get { return GetStringVarCharField("c800U2FDesc"); }
            set { SetStringVarCharField("c800U2FDesc", value); }
        }
        public decimal c800U2F
        {
            get { return GetMoneyField("c800U2F"); }
            set { SetMoneyField("c800U2F", value); }
        }
        public string c800U2F_rep
        {
            get { return GetMoneyField_rep("c800U2F"); }
            set { c800U2F = m_convertLos.ToMoney(value); }
        }

        public string c800U3FCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return GetStringVarCharField("c800U3FCode");
                    case E_GfeVersion.Gfe2010:
                        return "815";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }
            }
            set { SetStringVarCharField("c800U3FCode", value); }
        }
        public string c800U3FDesc
        {
            get { return GetStringVarCharField("c800U3FDesc"); }
            set { SetStringVarCharField("c800U3FDesc", value); }
        }
        public decimal c800U3F
        {
            get { return GetMoneyField("c800U3F"); }
            set { SetMoneyField("c800U3F", value); }
        }
        public string c800U3F_rep
        {
            get { return GetMoneyField_rep("c800U3F"); }
            set { c800U3F = m_convertLos.ToMoney(value); }
        }

        public string c800U4FCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return GetStringVarCharField("c800U4FCode");
                    case E_GfeVersion.Gfe2010:
                        return "816";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }
            }
            set { SetStringVarCharField("c800U4FCode", value); }
        }
        public string c800U4FDesc
        {
            get { return GetStringVarCharField("c800U4FDesc"); }
            set { SetStringVarCharField("c800U4FDesc", value); }
        }
        public decimal c800U4F
        {
            get { return GetMoneyField("c800U4F"); }
            set { SetMoneyField("c800U4F", value); }
        }
        public string c800U4F_rep
        {
            get { return GetMoneyField_rep("c800U4F"); }
            set { c800U4F = m_convertLos.ToMoney(value); }
        }
        public string c800U5FCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return GetStringVarCharField("c800U5FCode");
                    case E_GfeVersion.Gfe2010:
                        return "817";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }
            }
            set { SetStringVarCharField("c800U5FCode", value); }
        }
        public string c800U5FDesc
        {
            get { return GetStringVarCharField("c800U5FDesc"); }
            set { SetStringVarCharField("c800U5FDesc", value); }
        }
        public decimal c800U5F
        {
            get { return GetMoneyField("c800U5F"); }
            set { SetMoneyField("c800U5F", value); }
        }
        public string c800U5F_rep
        {
            get { return GetMoneyField_rep("c800U5F"); }
            set { c800U5F = m_convertLos.ToMoney(value); }
        }

        public int cIPiaDy
        {
            get { return GetCountField("cIPiaDy"); }
            set { SetCountField("cIPiaDy", value); }
        }
        public string cIPiaDy_rep
        {
            get { return GetCountField_rep("cIPiaDy"); }
            set { cIPiaDy = m_convertLos.ToCount(value); }
        }
        public int cDaysInYr
        {
            get { return GetCountField("cDaysInYr"); }
            set { SetCountField("cDaysInYr", value); }
        }
        public string cDaysInYr_rep
        {
            get { return GetCountField_rep("cDaysInYr"); }
            set { cDaysInYr = m_convertLos.ToCount(value); }
        }

        public int cMipPiaMon
        {
            get { return GetCountField("cMipPiaMon"); }
            set { SetCountField("cMipPiaMon", value); }
        }
        public string cMipPiaMon_rep
        {
            get { return GetCountField_rep("cMipPiaMon"); }
            set { cMipPiaMon = m_convertLos.ToCount(value); }
        }
        public int cHazInsPiaMon
        {
            get { return GetCountField("cHazInsPiaMon"); }
            set { SetCountField("cHazInsPiaMon", value); }
        }
        public string cHazInsPiaMon_rep
        {
            get { return GetCountField_rep("cHazInsPiaMon"); }
            set { cHazInsPiaMon = m_convertLos.ToCount(value); }
        }
        public string c904PiaDesc
        {
            get { return GetStringVarCharField("c904PiaDesc"); }
            set { SetStringVarCharField("c904PiaDesc", value); }
        }
        public decimal c904Pia
        {
            get { return GetMoneyField("c904Pia"); }
            set { SetMoneyField("c904Pia", value); }
        }
        public string c904Pia_rep
        {
            get { return GetMoneyField_rep("c904Pia"); }
            set { c904Pia = m_convertLos.ToMoney(value); }
        }
        public string c900U1PiaCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return GetStringVarCharField("c900U1PiaCode");
                    case E_GfeVersion.Gfe2010:
                        return "906";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }

            }
            set { SetStringVarCharField("c900U1PiaCode", value); }
        }
        public string c900U1PiaDesc
        {
            get { return GetStringVarCharField("c900U1PiaDesc"); }
            set { SetStringVarCharField("c900U1PiaDesc", value); }
        }
        public decimal c900U1Pia
        {
            get { return GetMoneyField("c900U1Pia"); }
            set { SetMoneyField("c900U1Pia", value); }
        }
        public string c900U1Pia_rep
        {
            get { return GetMoneyField_rep("c900U1Pia"); }
            set { c900U1Pia = m_convertLos.ToMoney(value); }
        }
        public int cHazInsRsrvMon
        {
            get { return GetCountField("cHazInsRsrvMon"); }
            set { SetCountField("cHazInsRsrvMon", value); }
        }
        public string cHazInsRsrvMon_rep
        {
            get { return GetCountField_rep("cHazInsRsrvMon"); }
            set { cHazInsRsrvMon = m_convertLos.ToCount(value); }
        }
        public int cMInsRsrvMon
        {
            get { return GetCountField("cMInsRsrvMon"); }
            set { SetCountField("cMInsRsrvMon", value); }
        }
        public string cMInsRsrvMon_rep
        {
            get { return GetCountField_rep("cMInsRsrvMon"); }
            set { cMInsRsrvMon = m_convertLos.ToCount(value); }
        }
        public string cProSchoolTxCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return "1003";
                    case E_GfeVersion.Gfe2010:
                        return "1005";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }
            }
        }
        public decimal cProSchoolTx
        {
            get { return GetMoneyField("cProSchoolTx"); }
            set { SetMoneyField("cProSchoolTx", value); }
        }
        public string cProSchoolTx_rep
        {
            get { return GetMoneyField_rep("cProSchoolTx"); }
            set { cProSchoolTx = m_convertLos.ToMoney(value); }
        }
        public int cRealETxRsrvMon
        {
            get { return GetCountField("cRealETxRsrvMon"); }
            set { SetCountField("cRealETxRsrvMon", value); }
        }
        public string cRealETxRsrvMon_rep
        {
            get { return GetCountField_rep("cRealETxRsrvMon"); }
            set { cRealETxRsrvMon = m_convertLos.ToCount(value); }
        }
        public int cFloodInsRsrvMon
        {
            get { return GetCountField("cFloodInsRsrvMon"); }
            set { SetCountField("cFloodInsRsrvMon", value); }
        }
        public string cFloodInsRsrvMon_rep
        {
            get { return GetCountField_rep("cFloodInsRsrvMon"); }
            set { cFloodInsRsrvMon = m_convertLos.ToCount(value); }
        }
        public string cProFloodInsCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return "1005";
                    case E_GfeVersion.Gfe2010:
                        return "1006";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }
            }
        }
        public decimal cProFloodIns
        {
            get { return GetMoneyField("cProFloodIns"); }
            set { SetMoneyField("cProFloodIns", value); }
        }
        public string cProFloodIns_rep
        {
            get { return GetMoneyField_rep("cProFloodIns"); }
            set { cProFloodIns = m_convertLos.ToMoney(value); }
        }
        public string c1006ProHExpCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return "1006";
                    case E_GfeVersion.Gfe2010:
                        return "1008";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }
            }
        }
        public string c1006ProHExpDesc
        {
            get { return GetStringVarCharField("c1006ProHExpDesc"); }
            set { SetStringVarCharField("c1006ProHExpDesc", value); }
        }
        public int c1006RsrvMon
        {
            get { return GetCountField("c1006RsrvMon"); }
            set { SetCountField("c1006RsrvMon", value); }
        }
        public string c1006RsrvMon_rep
        {
            get { return GetCountField_rep("c1006RsrvMon"); }
            set { c1006RsrvMon = m_convertLos.ToCount(value); }
        }
        public decimal c1006ProHExp
        {
            get { return GetMoneyField("c1006ProHExp"); }
            set { SetMoneyField("c1006ProHExp", value); }
        }
        public string c1006ProHExp_rep
        {
            get { return GetMoneyField_rep("c1006ProHExp"); }
            set { c1006ProHExp = m_convertLos.ToMoney(value); }
        }
        public string c1007ProHExpCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return "1007";
                    case E_GfeVersion.Gfe2010:
                        return "1009";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }
            }
        }
        public string c1007ProHExpDesc
        {
            get { return GetStringVarCharField("c1007ProHExpDesc"); }
            set { SetStringVarCharField("c1007ProHExpDesc", value); }
        }
        public int c1007RsrvMon
        {
            get { return GetCountField("c1007RsrvMon"); }
            set { SetCountField("c1007RsrvMon", value); }
        }
        public string c1007RsrvMon_rep
        {
            get { return GetCountField_rep("c1007RsrvMon"); }
            set { c1007RsrvMon = m_convertLos.ToCount(value); }
        }
        public decimal c1007ProHExp
        {
            get { return GetMoneyField("c1007ProHExp"); }
            set { SetMoneyField("c1007ProHExp", value); }
        }
        public string c1007ProHExp_rep
        {
            get { return GetMoneyField_rep("c1007ProHExp"); }
            set { c1007ProHExp = m_convertLos.ToMoney(value); }
        }
        public string cU3RsrvDesc
        {
            get { return GetStringVarCharField("cU3RsrvDesc"); }
            set { SetStringVarCharField("cU3RsrvDesc", value); }
        }
        public int cU3RsrvMon
        {
            get { return GetCountField("cU3RsrvMon"); }
            set { SetCountField("cU3RsrvMon", value); }
        }
        public string cU3RsrvMon_rep
        {
            get { return GetCountField_rep("cU3RsrvMon"); }
            set { cU3RsrvMon = ToCount(value); }
        }
        public string cU4RsrvDesc
        {
            get { return GetStringVarCharField("cU4RsrvDesc"); }
            set { SetStringVarCharField("cU4RsrvDesc", value); }
        }
        public int cU4RsrvMon
        {
            get { return GetCountField("cU4RsrvMon"); }
            set { SetCountField("cU4RsrvMon", value); }
        }
        public string cU4RsrvMon_rep
        {
            get { return GetCountField_rep("cU4RsrvMon"); }
            set { cU4RsrvMon = ToCount(value); }
        }
        public string cEscrowFTable
        {
            get { return GetStringVarCharField("cEscrowFTable"); }
            set { SetStringVarCharField("cEscrowFTable", value); }
        }
        public string cEscrowFCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return "1101";
                    case E_GfeVersion.Gfe2010:
                        return "1102";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }
            }
        }

        public decimal cEscrowFPc
        {
            get { return GetPercentFromQuoteOrDb("cEscrowFPc"); }
            set { SetRateField("cEscrowFPc", value); }
        }
        public string cEscrowFPc_rep
        {
            get { return GetRateField_rep(cEscrowFPc); }
            set { cEscrowFPc = m_convertLos.ToRate(value); }
        }
        public E_PercentBaseT cEscrowFBaseT
        {
            get { return  (E_PercentBaseT)GetTypeIndexField("cEscrowFBaseT"); }
            set { SetTypeIndexField("cEscrowFBaseT", (int)value); }
        }

        public decimal cEscrowFMb
        {
            get { return  GetMoneyFromQuoteOrDb("cEscrowFMb"); }
            set { SetMoneyField("cEscrowFMb", value); }
        }
        public string cEscrowFMb_rep
        {
            get { return GetMoneyField_rep(() => cEscrowFMb); }
            set { cEscrowFMb = m_convertLos.ToRate(value); }
        }

        //public decimal cEscrowF
        //{
        //    get { return GetMoneyField("cEscrowF"); }
        //}
        //public string cEscrowF_rep
        //{
        //    get { return GetMoneyField_rep("cEscrowF"); }
        //}
        public string cNotaryFCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return "1106";
                    case E_GfeVersion.Gfe2010:
                        return "1110";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }
            }
        }
        public decimal cNotaryF
        {
            get { return GetMoneyFromQuoteOrDb("cNotaryF"); }
            set { SetMoneyField("cNotaryF", value); }
        }
        public string cNotaryF_rep
        {
            get { return GetMoneyField_rep(() => cNotaryF); }
            set { cNotaryF = m_convertLos.ToMoney(value); }
        }
        public string cTitleInsFCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return "1108";
                    case E_GfeVersion.Gfe2010:
                        return "1104";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }
            }
        }
        public string cTitleInsFTable
        {
            get { return GetStringVarCharField("cTitleInsFTable"); }
            set { SetStringVarCharField("cTitleInsFTable", value); }
        }


        public decimal cTitleInsFPc
        {
            get { return GetPercentFromQuoteOrDb("cTitleInsFPc"); }
            set { SetRateField("cTitleInsFPc", value); }
        }
        public string cTitleInsFPc_rep
        {
            get { return GetRateField_rep(cTitleInsFPc); }
            set { cTitleInsFPc = m_convertLos.ToRate(value); }
        }
        public E_PercentBaseT cTitleInsFBaseT
        {
            get { return (E_PercentBaseT)GetTypeIndexField("cTitleInsFBaseT"); }
            set { SetTypeIndexField("cTitleInsFBaseT", (int)value); }
        }

        public decimal cTitleInsFMb
        {
            get { return GetMoneyFromQuoteOrDb("cTitleInsFMb"); }
            set { SetMoneyField("cTitleInsFMb", value); }
        }
        public string cTitleInsFMb_rep
        {
            get { return GetMoneyField_rep(() => cTitleInsFMb ); }
            set { cTitleInsFMb = m_convertLos.ToRate(value); }
        }


        //public decimal cTitleInsF
        //{
        //    get
        //    {
        //        return GetMoneyField("cTitleInsF");
        //    }
        //}
        //public string cTitleInsF_rep
        //{
        //    get
        //    {
        //        return GetMoneyField_rep("cTitleInsF");
        //    }
        //}
        public string cU1TcCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return GetStringVarCharField("cU1TcCode");
                    case E_GfeVersion.Gfe2010:
                        return "1112";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }

            }
            set { SetStringVarCharField("cU1TcCode", value); }
        }
        public string cU1TcDesc
        {
            get { return GetStringVarCharField("cU1TcDesc"); }
            set { SetStringVarCharField("cU1TcDesc", value); }
        }
        public decimal cU1Tc
        {
            get { return GetMoneyField("cU1Tc"); }
            set { SetMoneyField("cU1Tc", value); }
        }
        public string cU1Tc_rep
        {
            get { return GetMoneyField_rep("cU1Tc"); }
            set { cU1Tc = m_convertLos.ToMoney(value); }
        }
        public string cU2TcCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return GetStringVarCharField("cU2TcCode");
                    case E_GfeVersion.Gfe2010:
                        return "1113";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }

            }
            set { SetStringVarCharField("cU2TcCode", value); }
        }
        public string cU2TcDesc
        {
            get { return GetStringVarCharField("cU2TcDesc"); }
            set { SetStringVarCharField("cU2TcDesc", value); }
        }
        public decimal cU2Tc
        {
            get { return GetMoneyField("cU2Tc"); }
            set { SetMoneyField("cU2Tc", value); }
        }
        public string cU2Tc_rep
        {
            get { return GetMoneyField_rep("cU2Tc"); }
            set { cU2Tc = m_convertLos.ToMoney(value); }
        }
        public string cU3TcCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return GetStringVarCharField("cU3TcCode");
                    case E_GfeVersion.Gfe2010:
                        return "1114";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }

            }
            set { SetStringVarCharField("cU3TcCode", value); }
        }
        public string cU3TcDesc
        {
            get { return GetStringVarCharField("cU3TcDesc"); }
            set { SetStringVarCharField("cU3TcDesc", value); }
        }
        public decimal cU3Tc
        {
            get { return GetMoneyField("cU3Tc"); }
            set { SetMoneyField("cU3Tc", value); }
        }
        public string cU3Tc_rep
        {
            get { return GetMoneyField_rep("cU3Tc"); }
            set { cU3Tc = m_convertLos.ToMoney(value); }
        }
        public string cU4TcCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return GetStringVarCharField("cU4TcCode");
                    case E_GfeVersion.Gfe2010:
                        return "1115";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }


            }
            set { SetStringVarCharField("cU4TcCode", value); }
        }
        public string cU4TcDesc
        {
            get { return GetStringVarCharField("cU4TcDesc"); }
            set { SetStringVarCharField("cU4TcDesc", value); }
        }
        public decimal cU4Tc
        {
            get { return GetMoneyField("cU4Tc"); }
            set { SetMoneyField("cU4Tc", value); }
        }
        public string cU4Tc_rep
        {
            get { return GetMoneyField_rep("cU4Tc"); }
            set { cU4Tc = m_convertLos.ToMoney(value); }
        }
        public decimal cRecFPc
        {
            get { return GetPercentFromQuoteOrDb("cRecFPc"); }
            set { SetRateField("cRecFPc", value); }
        }
        public string cRecFPc_rep
        {
            get { return GetRateField_rep(cRecFPc); }
            set { cRecFPc = m_convertLos.ToRate(value); }
        }
        public E_PercentBaseT cRecBaseT
        {
            get { return (E_PercentBaseT)GetTypeIndexField("cRecBaseT"); }
            set { SetTypeIndexField("cRecBaseT", (int)value); }
        }
        public E_PercentBaseT cProRealETxT
        {
            get { return (E_PercentBaseT)GetTypeIndexField("cProRealETxT"); }
            set { SetTypeIndexField("cProRealETxT", (int)value); }
        }
        public decimal cRecFMb
        {
            get { return GetMoneyFromQuoteOrDb("cRecFMb"); }
            set { SetMoneyField("cRecFMb", value); }
        }
        public string cRecFMb_rep
        {
            get { return GetMoneyField_rep(() =>cRecFMb); }
            set { cRecFMb = m_convertLos.ToMoney(value); }
        }

        public decimal cRecDeed
        {
            get { return GetMoneyFromQuoteOrDb("cRecDeed"); }
            set { SetMoneyField("cRecDeed", value); }
        }
        public string cRecDeed_rep
        {
            get { return GetMoneyField_rep(() => cRecDeed); }
            set { cRecDeed = m_convertLos.ToMoney(value); }
        }

        public decimal cRecMortgage
        {
            get { return GetMoneyFromQuoteOrDb("cRecMortgage"); }
            set { SetMoneyField("cRecMortgage", value); }
        }
        public string cRecMortgage_rep
        {
            get { return GetMoneyField_rep(() => cRecMortgage); }
            set { cRecMortgage = m_convertLos.ToMoney(value); }
        }

        public decimal cRecRelease
        {
            get { return GetMoneyFromQuoteOrDb("cRecRelease"); }
            set { SetMoneyField("cRecRelease", value); }
        }
        public string cRecRelease_rep
        {
            get { return GetMoneyField_rep(() => cRecRelease); }
            set { cRecRelease = m_convertLos.ToMoney(value); }
        }

        public bool cRecFLckd
        {
            get { return GetBoolField("cRecFLckd"); }
            set { SetBoolField("cRecFLckd", value); }
        }

        public string cCountyRtcCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return "1202";
                    case E_GfeVersion.Gfe2010:
                        return "1204";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }
            }
        }
        public decimal cCountyRtcPc
        {
            get { return GetPercentFromQuoteOrDb("cCountyRtcPc"); }
            set { SetRateField("cCountyRtcPc", value); }
        }
        public string cCountyRtcPc_rep
        {
            get { return GetRateField_rep(cCountyRtcPc); }
            set { cCountyRtcPc = m_convertLos.ToRate(value); }
        }
        public E_PercentBaseT cCountyRtcBaseT
        {
            get { return (E_PercentBaseT)GetTypeIndexField("cCountyRtcBaseT"); }
            set { SetTypeIndexField("cCountyRtcBaseT", (int)value); }
        }
        public decimal cCountyRtcMb
        {
            get { return GetMoneyFromQuoteOrDb("cCountyRtcMb"); }
            set { SetMoneyField("cCountyRtcMb", value); }
        }
        public string cCountyRtcMb_rep
        {
            get { return GetMoneyField_rep(() => cCountyRtcMb); }
            set { cCountyRtcMb = m_convertLos.ToMoney(value); }
        }
        public string cStateRtcCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return "1203";
                    case E_GfeVersion.Gfe2010:
                        return "1205";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }
            }
        }
        public decimal cStateRtcPc
        {
            get { return GetPercentFromQuoteOrDb("cStateRtcPc"); }
            set { SetRateField("cStateRtcPc", value); }
        }
        public string cStateRtcPc_rep
        {
            get { return GetRateField_rep(cStateRtcPc); }
            set { cStateRtcPc = m_convertLos.ToRate(value); }
        }
        public E_PercentBaseT cStateRtcBaseT
        {
            get { return (E_PercentBaseT)GetTypeIndexField("cStateRtcBaseT"); }
            set { SetTypeIndexField("cStateRtcBaseT", (int)value); }
        }
        public decimal cStateRtcMb
        {
            get { return GetMoneyFromQuoteOrDb("cStateRtcMb"); }
            set { SetMoneyField("cStateRtcMb", value); }
        }
        public string cStateRtcMb_rep
        {
            get { return GetMoneyField_rep(() => cStateRtcMb); }
            set { cStateRtcMb = m_convertLos.ToMoney(value); }
        }
        public string cU1GovRtcCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return GetStringVarCharField("cU1GovRtcCode");
                    case E_GfeVersion.Gfe2010:
                        return "1206";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }

            }
            set { SetStringVarCharField("cU1GovRtcCode", value); }
        }
        public string cU1GovRtcDesc
        {
            get { return GetStringVarCharField("cU1GovRtcDesc"); }
            set { SetStringVarCharField("cU1GovRtcDesc", value); }
        }
        public decimal cU1GovRtcPc
        {
            get { return GetRateField("cU1GovRtcPc"); }
            set { SetRateField("cU1GovRtcPc", value); }
        }
        public string cU1GovRtcPc_rep
        {
            get { return GetRateField_rep("cU1GovRtcPc"); }
            set { cU1GovRtcPc = m_convertLos.ToRate(value); }
        }
        public E_PercentBaseT cU1GovRtcBaseT
        {
            get { return (E_PercentBaseT)GetTypeIndexField("cU1GovRtcBaseT"); }
            set { SetTypeIndexField("cU1GovRtcBaseT", (int)value); }
        }
        public decimal cU1GovRtcMb
        {
            get { return GetMoneyField("cU1GovRtcMb"); }
            set { SetMoneyField("cU1GovRtcMb", value); }
        }
        public string cU1GovRtcMb_rep
        {
            get { return GetMoneyField_rep("cU1GovRtcMb"); }
            set { cU1GovRtcMb = m_convertLos.ToMoney(value); }
        }

        public string cU2GovRtcCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return GetStringVarCharField("cU2GovRtcCode");
                    case E_GfeVersion.Gfe2010:
                        return "1207";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }


            }
            set { SetStringVarCharField("cU2GovRtcCode", value); }
        }
        public string cU2GovRtcDesc
        {
            get { return GetStringVarCharField("cU2GovRtcDesc"); }
            set { SetStringVarCharField("cU2GovRtcDesc", value); }
        }
        public decimal cU2GovRtcPc
        {
            get { return GetRateField("cU2GovRtcPc"); }
            set { SetRateField("cU2GovRtcPc", value); }
        }
        public string cU2GovRtcPc_rep
        {
            get { return GetRateField_rep("cU2GovRtcPc"); }
            set { cU2GovRtcPc = m_convertLos.ToRate(value); }
        }
        public E_PercentBaseT cU2GovRtcBaseT
        {
            get { return (E_PercentBaseT)GetTypeIndexField("cU2GovRtcBaseT"); }
            set { SetTypeIndexField("cU2GovRtcBaseT", (int)value); }
        }
        public decimal cU2GovRtcMb
        {
            get { return GetMoneyField("cU2GovRtcMb"); }
            set { SetMoneyField("cU2GovRtcMb", value); }
        }
        public string cU2GovRtcMb_rep
        {
            get { return GetMoneyField_rep("cU2GovRtcMb"); }
            set { cU2GovRtcMb = m_convertLos.ToMoney(value); }
        }

        public string cU3GovRtcCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return GetStringVarCharField("cU3GovRtcCode");
                    case E_GfeVersion.Gfe2010:
                        return "1208";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }


            }
            set { SetStringVarCharField("cU3GovRtcCode", value); }
        }
        public string cU3GovRtcDesc
        {
            get { return GetStringVarCharField("cU3GovRtcDesc"); }
            set { SetStringVarCharField("cU3GovRtcDesc", value); }
        }
        public decimal cU3GovRtcPc
        {
            get { return GetRateField("cU3GovRtcPc"); }
            set { SetRateField("cU3GovRtcPc", value); }
        }
        public string cU3GovRtcPc_rep
        {
            get { return GetRateField_rep("cU3GovRtcPc"); }
            set { cU3GovRtcPc = m_convertLos.ToRate(value); }
        }
        public E_PercentBaseT cU3GovRtcBaseT
        {
            get { return (E_PercentBaseT)GetTypeIndexField("cU3GovRtcBaseT"); }
            set { SetTypeIndexField("cU3GovRtcBaseT", (int)value); }
        }
        public decimal cU3GovRtcMb
        {
            get { return GetMoneyField("cU3GovRtcMb"); }
            set { SetMoneyField("cU3GovRtcMb", value); }
        }
        public string cU3GovRtcMb_rep
        {
            get { return GetMoneyField_rep("cU3GovRtcMb"); }
            set { cU3GovRtcMb = m_convertLos.ToMoney(value); }
        }

        public decimal cPestInspectF
        {
            get { return GetMoneyField("cPestInspectF"); }
            set { SetMoneyField("cPestInspectF", value); }
        }
        public string cPestInspectF_rep
        {
            get { return GetMoneyField_rep("cPestInspectF"); }
            set { cPestInspectF = m_convertLos.ToMoney(value); }
        }
        public string cU1ScCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return GetStringVarCharField("cU1ScCode");
                    case E_GfeVersion.Gfe2010:
                        return "1303";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }

            }
            set { SetStringVarCharField("cU1ScCode", value); }
        }

        public string cU1ScDesc
        {
            get { return GetStringVarCharField("cU1ScDesc"); }
            set { SetStringVarCharField("cU1ScDesc", value); }
        }
        public decimal cU1Sc
        {
            get { return GetMoneyField("cU1Sc"); }
            set { SetMoneyField("cU1Sc", value); }
        }
        public string cU1Sc_rep
        {
            get { return GetMoneyField_rep("cU1Sc"); }
            set { cU1Sc = m_convertLos.ToMoney(value); }
        }

        public string cU2ScCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return GetStringVarCharField("cU2ScCode");
                    case E_GfeVersion.Gfe2010:
                        return "1304";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }


            }
            set { SetStringVarCharField("cU2ScCode", value); }
        }
        public string cU2ScDesc
        {
            get { return GetStringVarCharField("cU2ScDesc"); }
            set { SetStringVarCharField("cU2ScDesc", value); }
        }
        public decimal cU2Sc
        {
            get { return GetMoneyField("cU2Sc"); }
            set { SetMoneyField("cU2Sc", value); }
        }
        public string cU2Sc_rep
        {
            get { return GetMoneyField_rep("cU2Sc"); }
            set { cU2Sc = m_convertLos.ToMoney(value); }
        }

        public string cU3ScCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return GetStringVarCharField("cU3ScCode");
                    case E_GfeVersion.Gfe2010:
                        return "1305";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }


            }
            set { SetStringVarCharField("cU3ScCode", value); }
        }
        public string cU3ScDesc
        {
            get { return GetStringVarCharField("cU3ScDesc"); }
            set { SetStringVarCharField("cU3ScDesc", value); }
        }
        public decimal cU3Sc
        {
            get { return GetMoneyField("cU3Sc"); }
            set { SetMoneyField("cU3Sc", value); }
        }
        public string cU3Sc_rep
        {
            get { return GetMoneyField_rep("cU3Sc"); }
            set { cU3Sc = m_convertLos.ToMoney(value); }
        }
        public string cU4ScCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return GetStringVarCharField("cU4ScCode");
                    case E_GfeVersion.Gfe2010:
                        return "1306";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }
            }
            set { SetStringVarCharField("cU4ScCode", value); }
        }
        public string cU4ScDesc
        {
            get { return GetStringVarCharField("cU4ScDesc"); }
            set { SetStringVarCharField("cU4ScDesc", value); }
        }
        public decimal cU4Sc
        {
            get { return GetMoneyField("cU4Sc"); }
            set { SetMoneyField("cU4Sc", value); }
        }
        public string cU4Sc_rep
        {
            get { return GetMoneyField_rep("cU4Sc"); }
            set { cU4Sc = m_convertLos.ToMoney(value); }
        }
        public string cU5ScCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return GetStringVarCharField("cU5ScCode");
                    case E_GfeVersion.Gfe2010:
                        return "1307";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }


            }
            set { SetStringVarCharField("cU5ScCode", value); }
        }
        public string cU5ScDesc
        {
            get { return GetStringVarCharField("cU5ScDesc"); }
            set { SetStringVarCharField("cU5ScDesc", value); }
        }
        public decimal cU5Sc
        {
            get { return GetMoneyField("cU5Sc"); }
            set { SetMoneyField("cU5Sc", value); }
        }
        public string cU5Sc_rep
        {
            get { return GetMoneyField_rep("cU5Sc"); }
            set { cU5Sc = m_convertLos.ToMoney(value); }
        }
        public string cBrokComp1Desc
        {
            get { return GetStringVarCharField("cBrokComp1Desc"); }
            set { SetStringVarCharField("cBrokComp1Desc", value); }
        }

        public decimal cBrokComp1
        {
            get { return GetMoneyField("cBrokComp1"); }
            set { SetMoneyField("cBrokComp1", value); }
        }
        public string cBrokComp1_rep
        {
            get { return GetMoneyField_rep("cBrokComp1"); }
            set { cBrokComp1 = m_convertLos.ToMoney(value); }
        }
        public string cBrokComp2Desc
        {
            get { return GetStringVarCharField("cBrokComp2Desc"); }
            set { SetStringVarCharField("cBrokComp2Desc", value); }
        }
        public decimal cBrokComp2
        {
            get { return GetMoneyField("cBrokComp2"); }
            set { SetMoneyField("cBrokComp2", value); }
        }
        public string cBrokComp2_rep
        {
            get { return GetMoneyField_rep("cBrokComp2"); }
            set { cBrokComp2 = m_convertLos.ToMoney(value); }
        }

        public decimal cTotCcPbs
        {
            get { return GetMoneyField("cTotCcPbs"); }
            set { SetMoneyField("cTotCcPbs", value); }
        }
        public string cTotCcPbs_rep
        {
            get { return GetMoneyField_rep("cTotCcPbs"); }
            set { cTotCcPbs = m_convertLos.ToMoney(value); }
        }
        public bool cTotCcPbsLocked
        {
            get { return GetBoolField("cTotCcPbsLocked"); }
            set { SetBoolField("cTotCcPbsLocked", value); }
        }
        public string cU1FntcDesc
        {
            get { return GetStringVarCharField("cU1FntcDesc"); }
            set { SetStringVarCharField("cU1FntcDesc", value); }
        }
        public decimal cU1Fntc
        {
            get { return GetMoneyField("cU1Fntc"); }
            set { SetMoneyField("cU1Fntc", value); }
        }
        public string cU1Fntc_rep
        {
            get { return GetMoneyField_rep("cU1Fntc"); }
            set { cU1Fntc = m_convertLos.ToMoney(value); }
        }
        public decimal cFloodCertificationF
        {
            get { return GetMoneyField("cFloodCertificationF"); }
            set { SetMoneyField("cFloodCertificationF", value); }
        }
        public string cFloodCertificationF_rep
        {
            get { return GetMoneyField_rep("cFloodCertificationF"); }
            set { cFloodCertificationF = m_convertLos.ToMoney(value); }
        }
        public decimal cProHazInsMb
        {
            get { return GetMoneyField("cProHazInsMb"); }
            set { SetMoneyField("cProHazInsMb", value); }
        }
        public string cProHazInsMb_rep
        {
            get { return GetMoneyField_rep("cProHazInsMb"); }
            set { cProHazInsMb = m_convertLos.ToMoney(value); }
        }
        public decimal cProMInsMb
        {
            get { return GetMoneyField("cProMInsMb"); }
            set { SetMoneyField("cProMInsMb", value); }
        }
        public string cProMInsMb_rep
        {
            get { return GetMoneyField_rep("cProMInsMb"); }
            set { cProMInsMb = m_convertLos.ToMoney(value); }
        }
        public decimal cProRealETxMb
        {
            get { return GetMoneyField("cProRealETxMb"); }
            set { SetMoneyField("cProRealETxMb", value); }
        }
        public string cProRealETxMb_rep
        {
            get { return GetMoneyField_rep("cProRealETxMb"); }
            set { cProRealETxMb = m_convertLos.ToMoney(value); }
        }

        public decimal cOwnerTitleInsFPc
        {
            get { return GetPercentFromQuoteOrDb("cOwnerTitleInsFPc"); }
            set { SetRateField("cOwnerTitleInsFPc", value); }
        }
        public string cOwnerTitleInsFPc_rep
        {
            get { return GetRateField_rep(cOwnerTitleInsFPc); }
            set { cOwnerTitleInsFPc = m_convertLos.ToRate(value); }
        }
        public E_PercentBaseT cOwnerTitleInsFBaseT
        {
            get { return (E_PercentBaseT)GetTypeIndexField("cOwnerTitleInsFBaseT"); }
            set { SetTypeIndexField("cOwnerTitleInsFBaseT", (int)value); }
        }

        public decimal cOwnerTitleInsFMb
        {
            get { return GetMoneyFromQuoteOrDb("cOwnerTitleInsFMb"); }
            set { SetMoneyField("cOwnerTitleInsFMb", value); }
        }
        public string cOwnerTitleInsFMb_rep
        {
            get { return GetMoneyField_rep(() =>cOwnerTitleInsFMb); }
            set { cOwnerTitleInsFMb = m_convertLos.ToRate(value); }
        }


        //public decimal cOwnerTitleInsF
        //{
        //    get { return GetMoneyField("cOwnerTitleInsF"); }
        //}
        //public string cOwnerTitleInsF_rep
        //{
        //    get { return GetMoneyField_rep("cOwnerTitleInsF"); }
        //}

        public bool cGfeProvByBrok
        {
            get { return GetBoolField("cGfeProvByBrok"); }
            set { SetBoolField("cGfeProvByBrok", value); }
        }
        public bool cProcFPaid
        {
            get { return GetBoolField("cProcFPaid"); }
            set { SetBoolField("cProcFPaid", value); }
        }
        public bool cCrFPaid
        {
            get { return GetBoolField("cCrFPaid"); }
            set { SetBoolField("cCrFPaid", value); }
        }
        public bool cApprFPaid
        {
            get { return GetBoolField("cApprFPaid"); }
            set { SetBoolField("cApprFPaid", value); }
        }
        public string cAttorneyFCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return "1107";
                    case E_GfeVersion.Gfe2010:
                        return "1111";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }
            }
        }
        public decimal cAttorneyF
        {
            get { return GetMoneyField("cAttorneyF"); }
            set { SetMoneyField("cAttorneyF", value); }
        }
        public string cAttorneyF_rep
        {
            get { return GetMoneyField_rep("cAttorneyF"); }
            set { cAttorneyF = m_convertLos.ToMoney(value); }
        }
        public string cDocPrepFCode
        {
            get
            {
                switch (GfeVersion)
                {
                    case E_GfeVersion.Gfe2009:
                        return "1105";
                    case E_GfeVersion.Gfe2010:
                        return "1109";
                    default:
                        throw new UnhandledEnumException(GfeVersion);
                }
            }
        }
        public decimal cDocPrepF
        {
            get { return GetMoneyField("cDocPrepF"); }
            set { SetMoneyField("cDocPrepF", value); }
        }
        public string cDocPrepF_rep
        {
            get { return GetMoneyField_rep("cDocPrepF"); }
            set { cDocPrepF = m_convertLos.ToMoney(value); }
        }
        public decimal cAggregateAdjRsrv
        {
            get { return GetMoneyField("cAggregateAdjRsrv"); }
            set { SetMoneyField("cAggregateAdjRsrv", value); }
        }
        public string cAggregateAdjRsrv_rep
        {
            get { return GetMoneyField_rep("cAggregateAdjRsrv"); }
            set { cAggregateAdjRsrv = m_convertLos.ToMoney(value); }
        }
        public decimal cDisabilityIns
        {
            get { return GetMoneyField("cDisabilityIns"); }
            set { SetMoneyField("cDisabilityIns", value); }
        }
        public string cDisabilityIns_rep
        {
            get { return GetMoneyField_rep("cDisabilityIns"); }
            set { cDisabilityIns = m_convertLos.ToMoney(value); }
        }

        public E_PercentBaseT cProHazInsT
        {
            get { return (E_PercentBaseT)GetTypeIndexField("cProHazInsT"); }
            set { SetTypeIndexField("cProHazInsT", (int)value); }
        }
        public E_PercentBaseT cProMInsT
        {
            get { return (E_PercentBaseT)GetTypeIndexField("cProMInsT"); }
            set { SetTypeIndexField("cProMInsT", (int)value); }
        }
        public int cSchoolTxRsrvMon
        {
            get { return GetCountField("cSchoolTxRsrvMon"); }
            set { SetCountField("cSchoolTxRsrvMon", value); }
        }
        public string cSchoolTxRsrvMon_rep
        {
            get { return GetCountField_rep("cSchoolTxRsrvMon"); }
            set { cSchoolTxRsrvMon = m_convertLos.ToCount(value); }
        }
        public string cRecFDesc
        {
            get { return GetStringVarCharField("cRecFDesc"); }
            set { SetStringVarCharField("cRecFDesc", value); }
        }
        public string cCountyRtcDesc
        {
            get { return GetStringVarCharField("cCountyRtcDesc"); }
            set { SetStringVarCharField("cCountyRtcDesc", value); }
        }
        public string cStateRtcDesc
        {
            get { return GetStringVarCharField("cStateRtcDesc"); }
            set { SetStringVarCharField("cStateRtcDesc", value); }
        }
        public E_GfeVersion GfeVersion
        {
            get { return (E_GfeVersion)GetTypeIndexField("GfeVersion"); }
            set { SetTypeIndexField("GfeVersion", (int)value); }
        }
        private bool IsAPR(int props)
        {
            return LosConvert.GfeItemProps_Apr(props);
        }
        public string cApprFPaidTo
        {
            get { return GetStringVarCharField("cApprFPaidTo"); }
            set { SetStringVarCharField("cApprFPaidTo", value); }
        }
        public string cCrFPaidTo
        {
            get { return GetStringVarCharField("cCrFPaidTo"); }
            set { SetStringVarCharField("cCrFPaidTo", value); }
        }
        public string cTxServFPaidTo
        {
            get { return GetStringVarCharField("cTxServFPaidTo"); }
            set { SetStringVarCharField("cTxServFPaidTo", value); }
        }
        public string cFloodCertificationFPaidTo
        {
            get { return GetStringVarCharField("cFloodCertificationFPaidTo"); }
            set { SetStringVarCharField("cFloodCertificationFPaidTo", value); }
        }
        public string cInspectFPaidTo
        {
            get { return GetStringVarCharField("cInspectFPaidTo"); }
            set { SetStringVarCharField("cInspectFPaidTo", value); }
        }
        public string cProcFPaidTo
        {
            get { return GetStringVarCharField("cProcFPaidTo"); }
            set { SetStringVarCharField("cProcFPaidTo", value); }
        }
        public string cUwFPaidTo
        {
            get { return GetStringVarCharField("cUwFPaidTo"); }
            set { SetStringVarCharField("cUwFPaidTo", value); }
        }
        public string cWireFPaidTo
        {
            get { return GetStringVarCharField("cWireFPaidTo"); }
            set { SetStringVarCharField("cWireFPaidTo", value); }
        }
        public string c800U1FPaidTo
        {
            get { return GetStringVarCharField("c800U1FPaidTo"); }
            set { SetStringVarCharField("c800U1FPaidTo", value); }
        }
        public string c800U2FPaidTo
        {
            get { return GetStringVarCharField("c800U2FPaidTo"); }
            set { SetStringVarCharField("c800U2FPaidTo", value); }
        }
        public string c800U3FPaidTo
        {
            get { return GetStringVarCharField("c800U3FPaidTo"); }
            set { SetStringVarCharField("c800U3FPaidTo", value); }
        }
        public string c800U4FPaidTo
        {
            get { return GetStringVarCharField("c800U4FPaidTo"); }
            set { SetStringVarCharField("c800U4FPaidTo", value); }
        }
        public string c800U5FPaidTo
        {
            get { return GetStringVarCharField("c800U5FPaidTo"); }
            set { SetStringVarCharField("c800U5FPaidTo", value); }
        }
        public string cOwnerTitleInsPaidTo
        {
            get { return GetStringVarCharField("cOwnerTitleInsPaidTo"); }
            set { SetStringVarCharField("cOwnerTitleInsPaidTo", value); }
        }
        public string cDocPrepFPaidTo
        {
            get { return GetStringVarCharField("cDocPrepFPaidTo"); }
            set { SetStringVarCharField("cDocPrepFPaidTo", value); }
        }
        public string cNotaryFPaidTo
        {
            get { return GetStringVarCharField("cNotaryFPaidTo"); }
            set { SetStringVarCharField("cNotaryFPaidTo", value); }
        }
        public string cU1TcPaidTo
        {
            get { return GetStringVarCharField("cU1TcPaidTo"); }
            set { SetStringVarCharField("cU1TcPaidTo", value); }
        }
        public string cU2TcPaidTo
        {
            get { return GetStringVarCharField("cU2TcPaidTo"); }
            set { SetStringVarCharField("cU2TcPaidTo", value); }
        }
        public string cU3TcPaidTo
        {
            get { return GetStringVarCharField("cU3TcPaidTo"); }
            set { SetStringVarCharField("cU3TcPaidTo", value); }
        }
        public string cU4TcPaidTo
        {
            get { return GetStringVarCharField("cU4TcPaidTo"); }
            set { SetStringVarCharField("cU4TcPaidTo", value); }
        }

        public string cU1GovRtcPaidTo
        {
            get { return GetStringVarCharField("cU1GovRtcPaidTo"); }
            set { SetStringVarCharField("cU1GovRtcPaidTo", value); }
        }
        public string cU2GovRtcPaidTo
        {
            get { return GetStringVarCharField("cU2GovRtcPaidTo"); }
            set { SetStringVarCharField("cU2GovRtcPaidTo", value); }
        }
        public string cU3GovRtcPaidTo
        {
            get { return GetStringVarCharField("cU3GovRtcPaidTo"); }
            set { SetStringVarCharField("cU3GovRtcPaidTo", value); }
        }
        public string cPestInspectPaidTo
        {
            get { return GetStringVarCharField("cPestInspectPaidTo"); }
            set { SetStringVarCharField("cPestInspectPaidTo", value); }
        }
        public string cU1ScPaidTo
        {
            get { return GetStringVarCharField("cU1ScPaidTo"); }
            set { SetStringVarCharField("cU1ScPaidTo", value); }
        }
        public string cU2ScPaidTo
        {
            get { return GetStringVarCharField("cU2ScPaidTo"); }
            set { SetStringVarCharField("cU2ScPaidTo", value); }
        }
        public string cU3ScPaidTo
        {
            get { return GetStringVarCharField("cU3ScPaidTo"); }
            set { SetStringVarCharField("cU3ScPaidTo", value); }
        }
        public string cU4ScPaidTo
        {
            get { return GetStringVarCharField("cU4ScPaidTo"); }
            set { SetStringVarCharField("cU4ScPaidTo", value); }
        }
        public string cU5ScPaidTo
        {
            get { return GetStringVarCharField("cU5ScPaidTo"); }
            set { SetStringVarCharField("cU5ScPaidTo", value); }
        }

        public string cAttorneyFPaidTo
        {
            get { return GetStringVarCharField("cAttorneyFPaidTo"); }
            set { SetStringVarCharField("cAttorneyFPaidTo", value); }
        }

        public string cMipPiaPaidTo
        {
            get { return GetStringVarCharField("cMipPiaPaidTo"); }
            set { SetStringVarCharField("cMipPiaPaidTo", value); }
        }

        public string cHazInsPiaPaidTo
        {
            get { return GetStringVarCharField("cHazInsPiaPaidTo"); }
            set { SetStringVarCharField("cHazInsPiaPaidTo", value); }
        }

        public string cVaFfPaidTo
        {
            get { return GetStringVarCharField("cVaFfPaidTo"); }
            set { SetStringVarCharField("cVaFfPaidTo", value); }
        }

        public E_GfeSectionT c800U1FGfeSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetTypeIndexField("c800U1FGfeSection");
                if (v == E_GfeSectionT.B1 || v == E_GfeSectionT.B3 || v == E_GfeSectionT.NotApplicable)
                {
                    return v;
                }
                else
                {
                    return E_GfeSectionT.B1;
                }
            }
            set { SetTypeIndexField("c800U1FGfeSection", (int)value); }
        }
        public E_GfeSectionT c800U2FGfeSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetTypeIndexField("c800U2FGfeSection");
                if (v == E_GfeSectionT.B1 || v == E_GfeSectionT.B3 || v == E_GfeSectionT.NotApplicable)
                {
                    return v;
                }
                else
                {
                    return E_GfeSectionT.B1;
                }
            }
            set { SetTypeIndexField("c800U2FGfeSection", (int)value); }
        }

        public E_GfeSectionT c800U3FGfeSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetTypeIndexField("c800U3FGfeSection");
                if (v == E_GfeSectionT.B1 || v == E_GfeSectionT.B3 || v == E_GfeSectionT.NotApplicable)
                {
                    return v;
                }
                else
                {
                    return E_GfeSectionT.B1;
                }
            }
            set { SetTypeIndexField("c800U3FGfeSection", (int)value); }
        }

        public E_GfeSectionT c800U4FGfeSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetTypeIndexField("c800U4FGfeSection");
                if (v == E_GfeSectionT.B1 || v == E_GfeSectionT.B3 || v == E_GfeSectionT.NotApplicable)
                {
                    return v;
                }
                else
                {
                    return E_GfeSectionT.B1;
                }
            }
            set { SetTypeIndexField("c800U4FGfeSection", (int)value); }
        }

        public E_GfeSectionT c800U5FGfeSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetTypeIndexField("c800U5FGfeSection");
                if (v == E_GfeSectionT.B1 || v == E_GfeSectionT.B3 || v == E_GfeSectionT.NotApplicable)
                {
                    return v;
                }
                else
                {
                    return E_GfeSectionT.B1;
                }
            }
            set { SetTypeIndexField("c800U5FGfeSection", (int)value); }
        }

        public E_GfeSectionT cEscrowFGfeSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetTypeIndexField("cEscrowFGfeSection");
                if (v == E_GfeSectionT.B4 || v == E_GfeSectionT.B6)
                {
                    //return v;
                }
                else
                {
                    v = E_GfeSectionT.B4;
                }


                if (v == E_GfeSectionT.B6 && m_brokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    v = E_GfeSectionT.B4;
                }
                return v;
            }
            set { SetTypeIndexField("cEscrowFGfeSection", (int)value); }
        }

        public E_GfeSectionT cTitleInsFGfeSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetTypeIndexField("cDocPrepFGfeSection");
                if (v == E_GfeSectionT.B4 || v == E_GfeSectionT.B6)
                {
                    //return v;
                }
                else
                {
                    v = E_GfeSectionT.B4;
                }

                if (v == E_GfeSectionT.B6 && m_brokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    v = E_GfeSectionT.B4;
                }
                return v;
            }
            set { SetTypeIndexField("cTitleInsFGfeSection", (int)value); }
        }

        public E_GfeSectionT cDocPrepFGfeSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetTypeIndexField("cDocPrepFGfeSection");
                if (v == E_GfeSectionT.B4 || v == E_GfeSectionT.B6)
                {
                    //return v;
                }
                else
                {
                    v = E_GfeSectionT.B4;
                }

                if (v == E_GfeSectionT.B6 && m_brokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    v = E_GfeSectionT.B4;
                }
                return v;
            }
            set { SetTypeIndexField("cDocPrepFGfeSection", (int)value); }
        }

        public E_GfeSectionT cNotaryFGfeSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetTypeIndexField("cNotaryFGfeSection");
                if (v == E_GfeSectionT.B4 || v == E_GfeSectionT.B6)
                {
                    //return v;
                }
                else
                {
                    v = E_GfeSectionT.B4;
                }


                if (v == E_GfeSectionT.B6 && m_brokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    v = E_GfeSectionT.B4;
                }
                return v;
            }
            set { SetTypeIndexField("cNotaryFGfeSection", (int)value); }
        }

        public E_GfeSectionT cAttorneyFGfeSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetTypeIndexField("cAttorneyFGfeSection");
                if (v == E_GfeSectionT.B4 || v == E_GfeSectionT.B6)
                {
                    //return v;
                }
                else
                {
                    v = E_GfeSectionT.B4;
                }

                if (v == E_GfeSectionT.B6 && m_brokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    v = E_GfeSectionT.B4;
                }
                return v;
            }
            set { SetTypeIndexField("cAttorneyFGfeSection", (int)value); }
        }

        public E_GfeSectionT cU1TcGfeSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetTypeIndexField("cU1TcGfeSection");
                if (v == E_GfeSectionT.B4 || v == E_GfeSectionT.B6 || v == E_GfeSectionT.NotApplicable)
                {
                    //return v;
                }
                else
                {
                    v = E_GfeSectionT.B4;
                }


                if (v == E_GfeSectionT.B6 && m_brokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    v = E_GfeSectionT.B4;
                }
                return v;
            }
            set { SetTypeIndexField("cU1TcGfeSection", (int)value); }
        }

        public E_GfeSectionT cU2TcGfeSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetTypeIndexField("cU2TcGfeSection");
                if (v == E_GfeSectionT.B4 || v == E_GfeSectionT.B6 || v == E_GfeSectionT.NotApplicable)
                {
                    //return v;
                }
                else
                {
                    v = E_GfeSectionT.B4;
                }


                if (v == E_GfeSectionT.B6 && m_brokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    v = E_GfeSectionT.B4;
                }
                return v;
            }
            set { SetTypeIndexField("cU2TcGfeSection", (int)value); }
        }

        public E_GfeSectionT cU3TcGfeSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetTypeIndexField("cU3TcGfeSection");
                if (v == E_GfeSectionT.B4 || v == E_GfeSectionT.B6 || v == E_GfeSectionT.NotApplicable)
                {
                    //return v;
                }
                else
                {
                    v = E_GfeSectionT.B4;
                }


                if (v == E_GfeSectionT.B6 && m_brokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    v = E_GfeSectionT.B4;
                }
                return v;
            }
            set { SetTypeIndexField("cU3TcGfeSection", (int)value); }
        }

        public E_GfeSectionT cU4TcGfeSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetTypeIndexField("cU4TcGfeSection");
                if (v == E_GfeSectionT.B4 || v == E_GfeSectionT.B6 || v == E_GfeSectionT.NotApplicable)
                {
                    //return v;
                }
                else
                {
                    v = E_GfeSectionT.B4;
                }


                if (v == E_GfeSectionT.B6 && m_brokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    v = E_GfeSectionT.B4;
                }
                return v;
            }
            set { SetTypeIndexField("cU4TcGfeSection", (int)value); }
        }

        public E_GfeSectionT cU1GovRtcGfeSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetTypeIndexField("cU1GovRtcGfeSection");
                if (v == E_GfeSectionT.B7 || v == E_GfeSectionT.B8)
                {
                    return v;
                }
                else
                {
                    return E_GfeSectionT.B8;
                }
            }
            set { SetTypeIndexField("cU1GovRtcGfeSection", (int)value); }
        }

        public E_GfeSectionT cU2GovRtcGfeSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetTypeIndexField("cU2GovRtcGfeSection");
                if (v == E_GfeSectionT.B7 || v == E_GfeSectionT.B8)
                {
                    return v;
                }
                else
                {
                    return E_GfeSectionT.B8;
                }
            }
            set { SetTypeIndexField("cU2GovRtcGfeSection", (int)value); }
        }

        public E_GfeSectionT cU3GovRtcGfeSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetTypeIndexField("cU3GovRtcGfeSection");
                if (v == E_GfeSectionT.B7 || v == E_GfeSectionT.B8)
                {
                    return v;
                }
                else
                {
                    return E_GfeSectionT.B8;
                }
            }
            set { SetTypeIndexField("cU3GovRtcGfeSection", (int)value); }
        }

        public E_GfeSectionT cU1ScGfeSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetTypeIndexField("cU1ScGfeSection");
                if (v == E_GfeSectionT.B4 || v == E_GfeSectionT.B6 || v == E_GfeSectionT.NotApplicable)
                {
                    //return v;
                }
                else
                {
                    v = E_GfeSectionT.B4;
                }


                if (v == E_GfeSectionT.B4 && m_brokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    v = E_GfeSectionT.B6;
                }
                return v;
            }
            set { SetTypeIndexField("cU1ScGfeSection", (int)value); }
        }

        public E_GfeSectionT cU2ScGfeSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetTypeIndexField("cU2ScGfeSection");
                if (v == E_GfeSectionT.B4 || v == E_GfeSectionT.B6 || v == E_GfeSectionT.NotApplicable)
                {
                    //return v;
                }
                else
                {
                    v = E_GfeSectionT.B4;
                }


                if (v == E_GfeSectionT.B4 && m_brokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    v = E_GfeSectionT.B6;
                }
                return v;
            }
            set { SetTypeIndexField("cU2ScGfeSection", (int)value); }
        }

        public E_GfeSectionT cU3ScGfeSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetTypeIndexField("cU3ScGfeSection");
                if (v == E_GfeSectionT.B4 || v == E_GfeSectionT.B6 || v == E_GfeSectionT.NotApplicable)
                {
                    //return v;
                }
                else
                {
                    v = E_GfeSectionT.B4;
                }


                if (v == E_GfeSectionT.B4 && m_brokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    v = E_GfeSectionT.B6;
                }
                return v;
            }
            set { SetTypeIndexField("cU3ScGfeSection", (int)value); }
        }

        public E_GfeSectionT cU4ScGfeSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetTypeIndexField("cU4ScGfeSection");
                if (v == E_GfeSectionT.B4 || v == E_GfeSectionT.B6 || v == E_GfeSectionT.NotApplicable)
                {
                    //return v;
                }
                else
                {
                    v = E_GfeSectionT.B4;
                }


                if (v == E_GfeSectionT.B4 && m_brokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    v = E_GfeSectionT.B6;
                }
                return v;
            }
            set { SetTypeIndexField("cU4ScGfeSection", (int)value); }
        }

        public E_GfeSectionT cU5ScGfeSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetTypeIndexField("cU5ScGfeSection");
                if (v == E_GfeSectionT.B4 || v == E_GfeSectionT.B6 || v == E_GfeSectionT.NotApplicable)
                {
                    //return v;
                }
                else
                {
                    v = E_GfeSectionT.B4;
                }


                if (v == E_GfeSectionT.B4 && m_brokerDB.IsB4AndB6DisabledIn1100And1300OfGfe)
                {
                    v = E_GfeSectionT.B6;
                }
                return v;

            }
            set { SetTypeIndexField("cU5ScGfeSection", (int)value); }
        }
        public E_GfeSectionT c904PiaGfeSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetTypeIndexField("c904PiaGfeSection");
                if (v == E_GfeSectionT.B3 || v == E_GfeSectionT.B11)
                {
                    return v;
                }
                else
                {
                    return E_GfeSectionT.B3;
                }
            }
            set { SetTypeIndexField("c904PiaGfeSection", (int)value); }
        }
        public E_GfeSectionT c900U1PiaGfeSection
        {
            get
            {
                E_GfeSectionT v = (E_GfeSectionT)GetTypeIndexField("c900U1PiaGfeSection");
                if (v == E_GfeSectionT.B3 || v == E_GfeSectionT.B11)
                {
                    return v;
                }
                else
                {
                    return E_GfeSectionT.B3;
                }
            }
            set { SetTypeIndexField("c900U1PiaGfeSection", (int)value); }
        }

        public E_cPricingEngineCreditT cPricingEngineCreditT
        {
            get { return (E_cPricingEngineCreditT)GetTypeIndexField("cPricingEngineCreditT"); }
            set { SetTypeIndexField("cPricingEngineCreditT", (int)value); }
        }

        public E_cPricingEngineLimitCreditToT cPricingEngineLimitCreditToT
        {
            get { return (E_cPricingEngineLimitCreditToT)GetTypeIndexField("cPricingEngineLimitCreditToT"); }
            set { SetTypeIndexField("cPricingEngineLimitCreditToT", (int)value); }
        }

        public E_cPricingEngineCostT cPricingEngineCostT
        {
            get { return (E_cPricingEngineCostT)GetTypeIndexField("cPricingEngineCostT"); }
            set { SetTypeIndexField("cPricingEngineCostT", (int)value); }
        }


        public E_cTitleInsurancePolicyT cTitleInsurancePolicyT
        {
            get { return (E_cTitleInsurancePolicyT)GetTypeIndexField("cTitleInsurancePolicy"); }
            set { SetTypeIndexField("cTitleInsurancePolicy", (int)value); }
        }

        public bool cGfeIsTPOTransaction
        {
            get { return GetBoolField("cGfeIsTPOTransaction"); }
            set { SetBoolField("cGfeIsTPOTransaction", value); }
        }
        public bool cGfeUsePaidToFromOfficialContact
        {
            get { return GetBoolField("cGfeUsePaidToFromOfficialContact"); }
            set { SetBoolField("cGfeUsePaidToFromOfficialContact", value); }
        }

        private BrokerDB x_brokerDB = null;
        private BrokerDB m_brokerDB => x_brokerDB ?? (x_brokerDB = PrincipalFactory.CurrentPrincipal.BrokerDB);
        private decimal m_TotalPercentFeesForAPR = -1;
        private decimal m_TotalFixedFeesForAPR = -1;
        private Dictionary<E_FeeT, decimal> m_fixedAprFees = new Dictionary<E_FeeT, decimal>();
        private Dictionary<E_FeeT, decimal> m_percentAprFees = new Dictionary<E_FeeT, decimal>();

        // Hardcoding these field names for speed.  Using reflection or delegates would be more elegant,
        // but would slow this down, and it is being implemented for OPM 40324 where speed is critical
        public decimal TotalPercentFeesForAPR => m_TotalPercentFeesForAPR >= 0
            ? m_TotalPercentFeesForAPR
            : (m_TotalPercentFeesForAPR = 0
                   + (IsAPR(cLOrigFProps   ) ? cLOrigFPc   : 0)
                   + (IsAPR(cLDiscntProps  ) ? cLDiscntPc  : 0)
                   + (IsAPR(cMBrokFProps   ) ? cMBrokFPc   : 0)
                // See the note above TotalFixedFeesForAPR for explanation on why these aren't included.
                // + (IsAPR(cMipPiaProps   ) ? cProMInsR   : 0)
                // + (IsAPR(cHazInsPiaProps) ? cProHazInsR : 0)
                // + (IsAPR(cRecFProps     ) ? cProHazInsR : 0)
                // + (IsAPR(cCountyRtcProps) ? cProHazInsR : 0)
                // + (IsAPR(cStateRtcProps ) ? cProHazInsR : 0)
                // + (IsAPR(cU1GovRtcProps ) ? cProHazInsR : 0)
                // + (IsAPR(cU2GovRtcProps ) ? cProHazInsR : 0)
                // + (IsAPR(cU3GovRtcProps ) ? cU3GovRtcPc : 0)
            );

        // db - adding for Google integration
        // Hardcoding these field names for speed.  Using reflection or delegates would be more elegant,
        // but would slow this down, and it is being implemented for OPM 40324 where speed is critical
        // See the note above TotalFixedFeesForAPR for exaplanation on why not all percent fees are included
        public Dictionary<E_FeeT, decimal> ListPercentAPRFees
        {
            get
            {
                return m_percentAprFees.Count > 0
                    ? m_percentAprFees
                    : (m_percentAprFees = new[]
                    {
                        IsAPR(cLOrigFProps) ? Tuple.Create(E_FeeT.Origination, cLOrigFPc) : null,
                        IsAPR(cLDiscntProps) ? Tuple.Create(E_FeeT.CreditOrCharge, cLDiscntPc) : null,
                        IsAPR(cMBrokFProps) ? Tuple.Create(E_FeeT.MortgageBroker, cMBrokFPc) : null,
                    }.Where(p => p != null).ToDictionary(t => t.Item1, t => t.Item2));
            }
        }

        // db - adding for Google integration
        // Hardcoding these field names for speed.  Using reflection or delegates would be more elegant,
        // but would slow this down, and it is being implemented for OPM 40324 where speed is critical
        public Dictionary<E_FeeT, decimal> ListFixedAPRFees
        {
            get
            {
                return m_fixedAprFees.Count > 0
                    ? m_fixedAprFees
                    : (m_fixedAprFees = new[]
                    {
                        IsAPR(cLOrigFProps          ) ? Tuple.Create(E_FeeT.Origination        , cLOrigFMb        ) : null,
                        IsAPR(cLDiscntProps         ) ? Tuple.Create(E_FeeT.CreditOrCharge     , cLDiscntFMb      ) : null,
                        IsAPR(cApprFProps           ) ? Tuple.Create(E_FeeT.Appraisal          , cApprF           ) : null,
                        IsAPR(cCrFProps             ) ? Tuple.Create(E_FeeT.CreditReport       , cCrF             ) : null,
                        IsAPR(cInspectFProps        ) ? Tuple.Create(E_FeeT.LendersInspection  , cInspectF        ) : null,
                        IsAPR(cMBrokFProps          ) ? Tuple.Create(E_FeeT.MortgageBroker     , cMBrokFMb        ) : null,
                        IsAPR(cTxServFProps         ) ? Tuple.Create(E_FeeT.TaxService         , cTxServF         ) : null,
                        IsAPR(cProcFProps           ) ? Tuple.Create(E_FeeT.Processing         , cProcF           ) : null,
                        IsAPR(cUwFProps             ) ? Tuple.Create(E_FeeT.Underwriting       , cUwF             ) : null,
                        IsAPR(cWireFProps           ) ? Tuple.Create(E_FeeT.WireTransfer       , cWireF           ) : null,
                        IsAPR(c800U1FProps          ) ? Tuple.Create(E_FeeT.Line813            , c800U1F          ) : null,
                        IsAPR(c800U2FProps          ) ? Tuple.Create(E_FeeT.Line814            , c800U2F          ) : null,
                        IsAPR(c800U3FProps          ) ? Tuple.Create(E_FeeT.Line815            , c800U3F          ) : null,
                        IsAPR(c800U4FProps          ) ? Tuple.Create(E_FeeT.Line816            , c800U4F          ) : null,
                        IsAPR(c800U5FProps          ) ? Tuple.Create(E_FeeT.Line817            , c800U5F          ) : null,
                        IsAPR(c904PiaProps          ) ? Tuple.Create(E_FeeT.Line904            , c904Pia          ) : null,
                        IsAPR(c900U1PiaProps        ) ? Tuple.Create(E_FeeT.Line906            , c900U1Pia        ) : null,
                        IsAPR(cAggregateAdjRsrvProps) ? Tuple.Create(E_FeeT.AggregateAdjustment, cAggregateAdjRsrv) : null,
                     // IsAPR(cEscrowFProps         ) ? Tuple.Create(E_FeeT.Escrow             , cEscrowF         ) : null,
                        IsAPR(cDocPrepFProps        ) ? Tuple.Create(E_FeeT.DocPrep            , cDocPrepF        ) : null,
                        IsAPR(cNotaryFProps         ) ? Tuple.Create(E_FeeT.Notary             , cNotaryF         ) : null,
                        IsAPR(cAttorneyFProps       ) ? Tuple.Create(E_FeeT.Attorney           , cAttorneyF       ) : null,
                     // IsAPR(cTitleInsFProps       ) ? Tuple.Create(E_FeeT.LenderTitleIns     , cTitleInsF       ) : null,
                        IsAPR(cU1TcProps            ) ? Tuple.Create(E_FeeT.Line1112           , cU1Tc            ) : null,
                        IsAPR(cU2TcProps            ) ? Tuple.Create(E_FeeT.Line1113           , cU2Tc            ) : null,
                        IsAPR(cU3TcProps            ) ? Tuple.Create(E_FeeT.Line1114           , cU3Tc            ) : null,
                        IsAPR(cU4TcProps            ) ? Tuple.Create(E_FeeT.Line1115           , cU4Tc            ) : null,
                        IsAPR(cPestInspectFProps    ) ? Tuple.Create(E_FeeT.PestInspection     , cPestInspectF    ) : null,
                        IsAPR(cU1ScProps            ) ? Tuple.Create(E_FeeT.Line1303           , cU1Sc            ) : null,
                        IsAPR(cU2ScProps            ) ? Tuple.Create(E_FeeT.Line1304           , cU2Sc            ) : null,
                        IsAPR(cU3ScProps            ) ? Tuple.Create(E_FeeT.Line1305           , cU3Sc            ) : null,
                        IsAPR(cU4ScProps            ) ? Tuple.Create(E_FeeT.Line1306           , cU4Sc            ) : null,
                        IsAPR(cU5ScProps            ) ? Tuple.Create(E_FeeT.Line1307           , cU5Sc            ) : null,
                    }.Where(t => t != null).ToDictionary(t => t.Item1, t => t.Item2));
            }
        }

        // db - NOTE: this does not include MIP or Hazard insurance because this is designed for the Google integration,
        // and Binh said the following about these 2 fields:
        // "Ignore it for now. Even if we send them the insurance fees from the closing cost template,
        // it would not be accurate. The insurance fee is dependent upon the loan program and the insurance
        // company. None of that is in the closing cost template. "
        // It also does not include section 1000 "Reserves Deposited With Lender"
        // (except for the Aggregate Adjustment field, #1008)
        // because they are calculations.
        // Also, section 1200 (Government Recording & Teansfer Charges) is not included for the same
        // reasons Binh gave about Hazard Insurance and MIP.
        // If these fields need to be included for another use, this Property can be made into a method that
        // takes a "mode" parameter for Google mode and non-Google mode.
        //
        // Hardcoding these field names for speed.  Using reflection or delegates would be more elegant,
        // but would slow this down, and it is being implemented for OPM 40324 where speed is critical
        public decimal TotalFixedFeesForAPR => m_TotalFixedFeesForAPR >= 0
            ? m_TotalFixedFeesForAPR
            : (m_TotalFixedFeesForAPR =
                         (IsAPR(cLOrigFProps          ) ? cLOrigFMb         : 0)
                       + (IsAPR(cLDiscntProps         ) ? cLDiscntFMb       : 0)
                       + (IsAPR(cApprFProps           ) ? cApprF            : 0)
                       + (IsAPR(cCrFProps             ) ? cCrF              : 0)
                       + (IsAPR(cInspectFProps        ) ? cInspectF         : 0)
                       + (IsAPR(cMBrokFProps          ) ? cMBrokFMb         : 0)
                       + (IsAPR(cTxServFProps         ) ? cTxServF          : 0)
                       + (IsAPR(cProcFProps           ) ? cProcF            : 0)
                       + (IsAPR(cUwFProps             ) ? cUwF              : 0)
                       + (IsAPR(cWireFProps           ) ? cWireF            : 0)
                       + (IsAPR(c800U1FProps          ) ? c800U1F           : 0)
                       + (IsAPR(c800U2FProps          ) ? c800U2F           : 0)
                       + (IsAPR(c800U3FProps          ) ? c800U3F           : 0)
                       + (IsAPR(c800U4FProps          ) ? c800U4F           : 0)
                       + (IsAPR(c800U5FProps          ) ? c800U5F           : 0)
                       + (IsAPR(c904PiaProps          ) ? c904Pia           : 0)
                       + (IsAPR(c900U1PiaProps        ) ? c900U1Pia         : 0)
                       + (IsAPR(cAggregateAdjRsrvProps) ? cAggregateAdjRsrv : 0)
                    // + (IsAPR(cEscrowFProps         ) ? cEscrowF          : 0)
                       + (IsAPR(cDocPrepFProps        ) ? cDocPrepF         : 0)
                       + (IsAPR(cNotaryFProps         ) ? cNotaryF          : 0)
                       + (IsAPR(cAttorneyFProps       ) ? cAttorneyF        : 0)
                    // + (IsAPR(cTitleInsFProps       ) ? cTitleInsF        : 0)
                       + (IsAPR(cU1TcProps            ) ? cU1Tc             : 0)
                       + (IsAPR(cU2TcProps            ) ? cU2Tc             : 0)
                       + (IsAPR(cU3TcProps            ) ? cU3Tc             : 0)
                       + (IsAPR(cU4TcProps            ) ? cU4Tc             : 0)
                       + (IsAPR(cPestInspectFProps    ) ? cPestInspectF     : 0)
                       + (IsAPR(cU1ScProps            ) ? cU1Sc             : 0)
                       + (IsAPR(cU2ScProps            ) ? cU2Sc             : 0)
                       + (IsAPR(cU3ScProps            ) ? cU3Sc             : 0)
                       + (IsAPR(cU4ScProps            ) ? cU4Sc             : 0)
                       + (IsAPR(cU5ScProps            ) ? cU5Sc             : 0)
            );
    }
}
