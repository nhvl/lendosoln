using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
	public class CPMLDefaultValuesData : CPmlPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CPMLDefaultValuesData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("aBExperianScorePe");
            list.Add("aBTransUnionScorePe");
            list.Add("aBEquifaxScorePe");
            list.Add("aCExperianScorePe");
            list.Add("aCTransUnionScorePe");
            list.Add("aCEquifaxScorePe");
            list.Add("sTransmOMonPmtPe");
            list.Add("sProdCrManualNonRolling30MortLateCount");
            list.Add("sProdCrManual60MortLateCount");
            list.Add("sProdCrManual90MortLateCount");
            list.Add("sProdCrManual120MortLateCount");
            list.Add("sProdCrManual150MortLateCount");
            list.Add("sProdCrManual30MortLateCount");
            list.Add("sProdCrManualForeclosureHas");
            list.Add("sProdCrManualForeclosureRecentFileMon");
            list.Add("sProdCrManualForeclosureRecentFileYr");
            list.Add("sProdCrManualForeclosureRecentStatusT");
            list.Add("sProdCrManualForeclosureRecentSatisfiedMon");
            list.Add("sProdCrManualForeclosureRecentSatisfiedYr");
            list.Add("sProdCrManualBk7Has");
            list.Add("sProdCrManualBk7RecentFileMon");
            list.Add("sProdCrManualBk7RecentFileYr");
            list.Add("sProdCrManualBk7RecentStatusT");
            list.Add("sProdCrManualBk7RecentSatisfiedMon");
            list.Add("sProdCrManualBk7RecentSatisfiedYr");
            list.Add("sProdCrManualBk13Has");
            list.Add("sProdCrManualBk13RecentFileMon");
            list.Add("sProdCrManualBk13RecentFileYr");
            list.Add("sProdCrManualBk13RecentStatusT");
            list.Add("sProdCrManualBk13RecentSatisfiedMon");
            list.Add("sProdCrManualBk13RecentSatisfiedYr");

            list.Add("aBFirstNm");
            list.Add("aBMidNm");
            list.Add("aBLastNm");
            list.Add("aBSuffix");
            list.Add("aBSsn");
            list.Add("aProdBCitizenT");
            list.Add("aBHasSpouse");
            list.Add("aCFirstNm");
            list.Add("aCMidNm");
            list.Add("aCLastNm");
            list.Add("aCSuffix");
            list.Add("aCSsn");
            list.Add("aIsBorrSpousePrimaryWageEarner");
            list.Add("sIsSelfEmployed");
            list.Add("sOpNegCfPe");

            list.Add("sSpStatePe");
            list.Add("sOccTPe");
            list.Add("sProRealETxPe");
            list.Add("sProOHExpPe");
            list.Add("sOccRPe");
            list.Add("sSpGrossRentPe");
            list.Add("sProdSpT");
            list.Add("sProdSpStructureT");
            list.Add("sProdCondoStories");
            list.Add("sProdIsSpInRuralArea");
            list.Add("sProdIsCondotel");
            list.Add("sProdIsNonwarrantableProj");
            list.Add("aPresOHExpPe");
            list.Add("sLPurposeTPe");
            list.Add("sProdCashoutAmt");
            list.Add("sHas1stTimeBuyerPe");
            list.Add("sIsIOnlyPe");
            list.Add("sProdImpound");
            list.Add("sProdDocT");
            list.Add("sPrimAppTotNonspIPe");
            list.Add("sProdPpmtPenaltyMon");
            list.Add("sProdRLckdDays");
            list.Add("sProdAvailReserveMonths");
            list.Add("sProd3rdPartyUwResultT");
            list.Add("sProdPpmtPenaltyMon2ndLien");
			list.Add("sProdEstimatedResidualI");
			list.Add("sProdCrManualRolling60MortLateCount");
			list.Add("sProdCrManualRolling90MortLateCount");
			list.Add("sProdMIOptionT");
			list.Add("sProdFilterDue15Yrs");
            list.Add("sProdFilterDue20Yrs");
			list.Add("sProdFilterDue30Yrs");			
			list.Add("sProdFilterDueOther");
			list.Add("sProdFilterFinMethFixed");
			list.Add("sProdFilterFinMethOptionArm");			
			list.Add("sProdFilterFinMeth3YrsArm");
			list.Add("sProdFilterFinMeth5YrsArm");
            list.Add("sProdFilterFinMeth7YrsArm");
            list.Add("sProdFilterFinMeth10YrsArm");
			list.Add("sProdFilterFinMethOther");
            list.Add("sProdIncludeMyCommunityProc");
            list.Add("sProdIncludeHomePossibleProc");
            list.Add("sProdIncludeNormalProc");
            list.Add("sProdIncludeFHATotalProc");
            list.Add("sProdIncludeVAProc");

	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CPMLDefaultValuesData(Guid fileId) : base(fileId, "CPMLDefaultValuesData", s_selectProvider)
		{
		}
	}
}
