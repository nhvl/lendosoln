﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using System.Text.RegularExpressions;
    using LendersOffice.UI;

    public sealed class TitleBorrower
    {
        private LosConvert converter = new LosConvert();

        private string nameOnCurrentTitle = null;
        private string titleWillBeHeldInWhatName = null;

        List<string> mAliases = new List<string>();

        public string POA {get; set; }

        [LqbInputModel(invalid: true)]
        public List<string> Aliases
        {
            get { return mAliases; }
            set { mAliases = value; }
        }

        public string FirstNm { get; set; }

        public string MidNm { get; set; }

        public string LastNm { get; set; }

        public string Email { get; set; }

        public string SSN { get; set; }

        public string SSNForExport
        {
            get
            {
                if (!string.IsNullOrEmpty(SSN))
                {
                    return SSN.Replace("-", "");
                }
                return string.Empty;
            }
        }

        public Guid Id { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }

        public E_aRelationshipTitleT RelationshipTitleT { get; set; }

        public string RelationshipTitleTOtherDesc { get; set; }

        public Guid AssociatedApplicationId { get; set; }

        public string FullName
        {
            get
            {
                string name = String.Concat(FirstNm, " ", MidNm, " ", LastNm);
                return Regex.Replace(name, @"[ ]{2,}", " ");
            }
        }

        public DateTime Dob
        {
            get;
            set;
        }

        public string Dob_rep
        {
            get
            {
                return this.converter.ToDateTimeString(this.Dob);
            }

            set
            {
                CDateTime date = CDateTime.Create(value, this.converter);
                if (date.IsValid)
                {
                    this.Dob = date.DateTimeForComputation;
                }
                else
                {
                    this.Dob = CDateTime.InvalidDateTime;
                }
            }
        }

        public string HPhone { get; set; }

        public bool CurrentlyHoldsTitle { get; set; }

        public bool WillHoldTitle { get; set; }

        public string NameOnCurrentTitle
        {
            get
            {
                if (this.NameOnCurrentTitleLckd)
                {
                    return this.nameOnCurrentTitle;
                }

                return this.FullName;
            }
            set { this.nameOnCurrentTitle = value; }
        }

        public bool NameOnCurrentTitleLckd { get; set; }

        public string TitleWillBeHeldInWhatName
        {
            get
            {
                if (this.TitleWillBeHeldInWhatNameLckd)
                {
                    return this.titleWillBeHeldInWhatName;
                }

                return this.FullName;
            }
            set { this.titleWillBeHeldInWhatName = value; }
        }

        public bool TitleWillBeHeldInWhatNameLckd { get; set; }

        /// <summary>
        /// Updates the backing fields for calculated properties to have the most up-to-date value.
        /// </summary>
        /// <param name="context">The streaming context.</param>
        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            this.nameOnCurrentTitle = this.NameOnCurrentTitle;
            this.titleWillBeHeldInWhatName = this.TitleWillBeHeldInWhatName;
        }
    }
}
