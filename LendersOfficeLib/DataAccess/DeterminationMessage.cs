﻿// <summary>
// <copyright file="DeterminationMessage.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Eduardo Michel
//  Date:   9/17/2015
// </summary>

namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Xml.Serialization;

    /// <summary>
    /// This class contain's details about the message for the Pml Certificate.
    /// </summary>
    [Serializable]
    public class DeterminationMessage
    {
        /// <summary>
        /// Gets or sets The font style that will applied to the message.
        /// </summary>
        /// <value>The font style that will applied to the message.</value>
        [XmlAttribute("font")]
        public string Font { get; set; }

        /// <summary>
        /// Gets or sets The text for the message.
        /// </summary>
        /// <value>The text for the message.</value>
        [XmlText]
        public string Text { get; set; }
    }
}
