﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Threading;
using System.Web;
using EDocs;
using iTextSharp.text;
using iTextSharp.text.pdf;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.ObjLib.ConsumerPortal;
using LendersOffice.ObjLib.Security;
using LendersOffice.PdfLayout;
using LendersOffice.Security;

namespace DataAccess
{
    /// <summary>
    /// Lightweight POD class for read-only access to a consumer request.
    /// Main purpose is for UI display.
    /// </summary>
    public class ReadOnlyConsumerActionItem
    {
        public long Id { get; private set; }

        /// <summary>
        /// The application id of this request.
        /// </summary>
        public Guid AppId { get; private set; }

        /// <summary>
        /// The loan id of this request.
        /// </summary>
        public Guid LoanId { get; private set; }

        /// <summary>
        /// The document type name
        /// </summary>
        public string DocumentTypeDescription { get; private set; }

        /// <summary>
        /// The document folder name
        /// </summary>
        public string FolderDescription { get; private set; }

        /// <summary>
        /// Description of the request.
        /// </summary>
        public string RequestDescription { get; private set; }

        /// <summary>
        /// The full borrower name
        /// </summary>
        public string BorrowerName { get; private set; }

        /// <summary>
        /// The full coborrower name
        /// </summary>
        public string CoborrowerName { get; private set; }

        /// <summary>
        /// True if this is a signature request.  False if it is a "Send in Doc" request.
        /// </summary>
        public bool IsSignatureRequired { get; private set; }

        /// <summary>
        /// True if this signature request allows signing online.
        /// </summary>
        public bool IsEsignAllowed { get; private set; }

        /// <summary>
        /// True if this request applies to the borrower
        /// </summary>
        public bool IsApplicableToBorrower { get; private set; }

        /// <summary>
        /// True if the borrower has electronically signed
        /// </summary>
        public bool HasBorrowerEsigned { get; private set; }

        /// <summary>
        /// True if this request applies to the coborrower
        /// </summary>
        public bool IsApplicableToCoBorrower { get; private set; }

        /// <summary>
        /// True if the borrower has electronically signed
        /// </summary>
        public bool HasCoborrowerEsigned { get; private set; }

        /// <summary>
        /// If null, consumer has not finished the request.
        /// If date exists, it means either 
        /// Something came in via fax on this date, or we collected
        /// all required eSignatures on the date.
        /// </summary>
        public DateTime? ConsumerCompletedDate { get; private set; }

        public Guid? CoborrowerTitleId { get; private set; }

        /// <summary>
        /// Lender response status.
        /// </summary>
        public E_ConsumerResponseStatusT ResponseStatus { get; private set; }

        public DateTime? CreatedOn
        {
            get;
            private set;
        }

        private ReadOnlyConsumerActionItem()
        {
        }

        public override string ToString()
        {
            StringBuilder ret = new StringBuilder();
            ret.AppendLine("LoanId:                   " + LoanId.ToString());
            ret.AppendLine("AppId:                    " + AppId.ToString());
            ret.AppendLine("BorrowerName:             " + BorrowerName);
            ret.AppendLine("CoborrowerName:           " + CoborrowerName);
            ret.AppendLine("FolderDescription:        " + FolderDescription);
            ret.AppendLine("DocumentTypeDescription:  " + DocumentTypeDescription);
            ret.AppendLine("IsSignatureRequired:      " + IsSignatureRequired);
            ret.AppendLine("IsEsignAllowed:           " + IsEsignAllowed);
            ret.AppendLine("IsApplicableToBorrower:   " + IsApplicableToBorrower);
            ret.AppendLine("IsApplicableToCoBorrower: " + IsApplicableToCoBorrower);
            ret.AppendLine("HasBorrowerEsigned:       " + HasBorrowerEsigned);
            ret.AppendLine("HasCoborrowerEsigned:     " + HasCoborrowerEsigned);
            ret.AppendLine("ResponseStatus:           " + ResponseStatus);
            ret.AppendLine("RequestDescription        " + RequestDescription);
            ret.AppendLine("ConsumerCompletedDate:    " + (ConsumerCompletedDate.HasValue ? ConsumerCompletedDate.Value.ToShortDateString() : ""));

            return ret.ToString();
        }

        public static List<ReadOnlyConsumerActionItem> GetConsumerActionItems(Guid brokerId, Guid loanId)
        {
            List<ReadOnlyConsumerActionItem> consumerRequests = new List<ReadOnlyConsumerActionItem>();

            SqlParameter[] parameters = {
                                            new SqlParameter("@sLId", loanId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "CP_ListActionRequestResponseByLoanId", parameters))
            {

                while (reader.Read())
                {
                    ReadOnlyConsumerActionItem ActionItem = new ReadOnlyConsumerActionItem();
                    ActionItem.LoanId= (Guid)reader["sLId"];
                    ActionItem.AppId = (Guid)reader["aAppId"];
                    ActionItem.Id = (long)reader["ConsumerActionRequestId"];
                    ActionItem.BorrowerName = reader["aBFullName"].ToString();
                    ActionItem.CoborrowerName = reader["aCFullName"].ToString();
                    ActionItem.FolderDescription = reader["FolderName"].ToString();
                    ActionItem.DocumentTypeDescription = reader["DocTypeName"].ToString();
                    ActionItem.IsSignatureRequired = (bool)reader["IsSignatureRequested"];
                    ActionItem.IsEsignAllowed = (bool)reader["IsESignAllowed"];
                    ActionItem.IsApplicableToBorrower = (bool)reader["IsApplicableToBorrower"];
                    ActionItem.IsApplicableToCoBorrower = (bool)reader["IsApplicableToCoborrower"];
                    ActionItem.ResponseStatus = (E_ConsumerResponseStatusT)reader["ResponseStatus"];
                    ActionItem.RequestDescription = reader["Description"].ToString();
                    ActionItem.HasBorrowerEsigned = reader["BorrowerSignedEventId"] != DBNull.Value;
                    ActionItem.HasCoborrowerEsigned = reader["CoborrowerSignedEventId"] != DBNull.Value;
                    ActionItem.ConsumerCompletedDate = (reader["ConsumerCompletedD"] != DBNull.Value) ? (DateTime)reader["ConsumerCompletedD"] : (DateTime?)null;
                    ActionItem.CreatedOn = reader["CreateOnD"] != DBNull.Value ? (DateTime)reader["CreateOnD"] : new DateTime?();
                    ActionItem.CoborrowerTitleId = reader.AsNullableGuid("sTitleBorrowerId");
                    consumerRequests.Add(ActionItem);
                }
            }

            return consumerRequests;
        }
    }

    public sealed class ConsumerActionItem
    {
        // About this class:
        // Typical callers should instantiate, call event, then save.
        // We should hope to avoid having callers set properties directly
        // to keep logic centralized here.  If something new needs to be done,
        // hopefully we can add new methods instead of exposing property setters.

        #region General Members

        public long ConsumerActionRequestId { get; private set; }
        
        private bool m_isNew = false;
        private bool m_isDelete = false;
        private bool m_acceptingResponse = false;
        private E_CompleteType m_completeType = E_CompleteType.None;
        private bool m_allowSave = true;
        private bool m_allowEvent = true; 
        private byte[] m_newPdf = null;

        private enum E_CompleteType
        {
            None,
            Fax,
            Esigned,
            UserUpload
        }

        public string ApplicantNames
        {
            get
            {
                return aBFullName + ( (aCFullName == string.Empty) ? string.Empty : " & " + aCFullName );
            }
        }

        #endregion

        #region Response Members

        /// <summary>
        /// Date when this response had a final consumer action. (We got a fax, or all signing parties have signed)
        /// </summary>
        public DateTime? ConsumerCompletedD { get; private set; }

        /// <summary>
        /// Number that this came in on if it was faxed.  EDocs system needs this.
        /// </summary>
        public string ReceivingFaxNumber { get; private set; }

        /// <summary>
        /// Gets the title id of the borrower when applicable.
        /// </summary>
        /// <value>This is only when the coborrower is title borrower. Normally will be null. If non null then the coborrower is a title borrower.</value>
        public Guid? CoborrowerTitleBorrowerId { get; private set; }

        /// <summary>
        /// The associated eventId for when this was signed by the borrower
        /// </summary>
        public Guid? BorrowerSignedEventId { get; private set; }

        /// <summary>
        /// The associated eventId for when this was signed by the coborrower
        /// </summary>
        public Guid? CoborrowerSignedEventId { get; private set; }

        /// <summary>
        /// True if the borrower has electronically signed
        /// </summary>
        public bool HasBorrowerEsigned
        {
            get {
                return BorrowerSignedEventId.HasValue ? true : false;
            }
        }

        /// <summary>
        /// True if the coborrower has electronically signed
        /// </summary>
        public bool HasCoborrowerEsigned
        {
            get {
                return CoborrowerSignedEventId.HasValue ? true : false;
            }
        }

        public DateTime? CreatedOn
        {
            get;
            private set;
        }

        #endregion // Response Members

        #region Request Members

        /// <summary>
        /// Associcated loan Id
        /// </summary>
        public Guid sLId { get; private set; }

        /// <summary>
        /// Associated application Id
        /// </summary>
        public Guid aAppId { get; private set; }

        /// <summary>
        /// Name of the borrower as it existed when this request was created.
        /// This is the name to be signed if eSigning is allowed.
        /// </summary>
        public string aBFullName { get; private set; }

        /// <summary>
        /// Borrower initials as it existed when this request was created.
        /// This is the name to be signed if eSigning is allowed.
        /// </summary>
        public string aBInitials { get; private set; }

        /// <summary>
        /// Name of the coborrower as it existed when this request was created.
        /// This is the name to be signed if eSigning is allowed.
        /// </summary>
        public string aCFullName { get; private set; }

        /// <summary>
        /// CoBorrower initials as it existed when this request was created.
        /// This is the name to be signed if eSigning is allowed.
        /// </summary>
        public string aCInitials { get; private set; }

        /// <summary>
        /// Status of the response
        /// </summary>
        public E_ConsumerResponseStatusT ResponseStatus { get; private set; }

        /// <summary>
        /// DocTypeid requested
        /// </summary>
        public int DocTypeId { get; private set; }

        /// <summary>
        /// Name of the doctype requested
        /// </summary>
        public string DocTypeName { get; private set; }

        /// <summary>
        /// Description of this request.
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// FileDB key of the requested document.
        /// </summary>
        public Guid PdfFileDbKey { get; private set; }

        /// <summary>
        /// Is this request applicable to the borrower
        /// </summary>
        public bool IsApplicableToBorrower { get; private set; }

        /// <summary>
        /// Is this request applicable to the coborrower
        /// </summary>
        public bool IsApplicableToCoborrower { get; private set; }

        /// <summary>
        /// Is this a signature request? The alternative is that this is a request for the consumer to 
        /// </summary>
        public bool IsSignatureRequested { get; private set; }

        /// <summary>
        /// Is eSign allowed for this request? The alternative is that the doc must be signed and faxed in (uploaded in the future)
        /// </summary>
        public bool IsESignAllowed { get; private set; }

        /// <summary>
        /// Original PDF metadata.
        /// </summary>
        public string PdfMetaDataSnapshot { get; private set; }

        public Guid CreatorEmployeeId { get; private set; }

        public void SetTitleCoborrower(TitleBorrower borrower)
        {
            if (!UserDataValidation.IsValidEmailAddress(borrower.Email) ||
                !UserDataValidation.IsValidSSN(borrower.SSN)
                )
            {
                throw new CBaseException("Title Borrower must have a valid email and ssn to sign.", "Title borrower doesn't have valid email or ssn.");
            }
            CoborrowerTitleBorrowerId = borrower.Id;
        }

        #endregion // Request Members

        #region SigningMembers

        ConsumerSignedEvent m_newBorrowerSignedEvent = null;
        ConsumerSignedEvent m_newCoborrowerSignedEvent = null;
        #endregion

        #region Constructors

        /// <summary>
        /// Loading existing entry from DB
        /// </summary>
        /// <param name="ConsumerRequestActionId"></param>
        /// <param name="brokerIdForDatabase">The broker id.</param>
        public ConsumerActionItem(long ConsumerRequestActionId, Guid? brokerIdForDatabase = null)
        {
            AbstractUserPrincipal principal = null;
            ConsumerUserPrincipal consumer = null;
            ConsumerPortalUserPrincipal consumer2 = null;

            principal = Thread.CurrentPrincipal as AbstractUserPrincipal;
            consumer2 = principal as ConsumerPortalUserPrincipal;

            // Doc receiver does not have user principal context
            ////if (principal == null)
            ////    throw new Exception("AbstractUserPrincipal is null on loading 'ConsumerActionItem'.");

            Guid brokerId = Guid.Empty;

            if (principal != null)
            {
                brokerId = principal.BrokerId;
            }

            // We always load right away.
            LoadData(brokerIdForDatabase ?? brokerId, ConsumerRequestActionId);

            consumer = principal as ConsumerUserPrincipal;
            if (consumer != null && !consumer.CanAccessLoanFile(this.sLId))
            {
                throw new Exception("Access denied.");
            }
            if (consumer2 != null && !ConsumerPortalUser.IsLinkedToLoan(consumer2.BrokerId, consumer2.Id, sLId))
            {
                throw new Exception("Access denied.");
            }
        }

        /// <summary>
        /// New document/signature request.  Call either RequestSignature or RequestDocument after this.
        /// </summary>
        public ConsumerActionItem(
            Guid loanId
            , Guid appId
            , string borrowerName
            , string coborrowerName
            , string borrowerInitials
            , string coborrowerInitials
            )
        {
            m_isNew = true;
            sLId = loanId;
            aAppId = appId;
            aBFullName = borrowerName;
            aCFullName = coborrowerName;
            aBInitials = borrowerInitials;
            aCInitials = coborrowerInitials;

            AbstractUserPrincipal creatingPrincipal =   BrokerUserPrincipal.CurrentPrincipal;
            if (creatingPrincipal != null)
            {
                CreatorEmployeeId = creatingPrincipal.EmployeeId;
            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, "Cannot create request without LO user");
            }
        }

        public ConsumerActionItem(
            Guid loanId,
            Guid appId,
            string borrowerName,
            string coborrowerName,
            string borrowerInitials,
            string coborrowerInitials,
            Guid creatorEmployeeId)
        {
            this.m_isNew = true;
            this.sLId = loanId;
            this.aAppId = appId;
            this.aBFullName = borrowerName;
            this.aCFullName = coborrowerName;
            this.aBInitials = borrowerInitials;
            this.aCInitials = coborrowerInitials;
            this.CreatorEmployeeId = creatorEmployeeId;
        }
        #endregion

        #region Data Access

        /// <summary>
        /// Load object from DB.
        /// </summary>
        private void LoadData(Guid brokerId, long consumerActionRequestId)
        {
            if (brokerId == Guid.Empty)
            {
                throw CBaseException.GenericException("Unable to determine appropriate database to load consumer action request from."); // consumerActionRequestId could match several databases, so it isn't safe to continue
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@ConsumerActionRequestId", consumerActionRequestId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "CP_RetrieveActionRequestResponseByRequestId", parameters))
            {
                if (reader.Read())
                {
                    // Request
                    sLId = (Guid)reader["sLId"];
                    aAppId = (Guid)reader["aAppId"];
                    aBFullName = reader["aBFullName"].ToString();
                    aCFullName = reader["aCFullName"].ToString();
                    ResponseStatus = (E_ConsumerResponseStatusT)reader["ResponseStatus"];
                    DocTypeId = (int)reader["DocTypeId"];
                    DocTypeName = reader["DocTypeName"].ToString();
                    Description = reader["Description"].ToString();
                    PdfFileDbKey = (Guid)reader["PdfFileDbKey"];
                    PdfMetaDataSnapshot = reader["PdfMetaDataSnapshot"].ToString();
                    IsApplicableToBorrower = (bool)reader["IsApplicableToBorrower"];
                    IsApplicableToCoborrower = (bool)reader["IsApplicableToCoborrower"];
                    IsSignatureRequested = (bool)reader["IsSignatureRequested"];
                    IsESignAllowed = (bool)reader["IsESignAllowed"];
                    aBInitials = reader["aBInitials"].ToString();
                    aCInitials = reader["aCInitials"].ToString();
                    CreatorEmployeeId = (reader["CreatorEmployeeId"] != DBNull.Value) ? (Guid)reader["CreatorEmployeeId"] : Guid.Empty;
                    CreatedOn = reader["CreateOnD"] != DBNull.Value ? (DateTime)reader["CreateOnD"] : new DateTime?();
                    CoborrowerTitleBorrowerId = reader.AsNullableGuid("sTitleBorrowerId");

                    // Response
                    ConsumerCompletedD = (reader["ConsumerCompletedD"] != DBNull.Value) ? (DateTime)reader["ConsumerCompletedD"] : (DateTime?)null;
                    ReceivingFaxNumber = (reader["ReceivingFaxNumber"] != DBNull.Value) ? reader["ReceivingFaxNumber"].ToString() : null;
                    BorrowerSignedEventId = (reader["BorrowerSignedEventId"] != DBNull.Value) ? (Guid)reader["BorrowerSignedEventId"] : (Guid?)null;
                    CoborrowerSignedEventId = (reader["CoborrowerSignedEventId"] != DBNull.Value) ? (Guid)reader["CoborrowerSignedEventId"] : (Guid?)null;

                    ConsumerActionRequestId = consumerActionRequestId;
                }
                else
                {
                    throw new CBaseException(ErrorMessages.Generic, "Could not load ActionItem: " + consumerActionRequestId);
                }
            }

        }

        /// <summary>
        /// Commit the changes of this object to db. Sets up account and sends 
        /// emails for new requests.
        /// </summary>
        public void Save()
        {
            this.Save(true);
        }

        /// <summary>
        /// Commits the changes of the object to the database. Sets up accounts
        /// and sends emails if applicable.
        /// </summary>
        /// <param name="setupAccountsAndSendEmailsIfNeeded">
        /// If true, will send out emails to borrower if this is a new item. 
        /// Otherwise, the save will not trigger any emails.
        /// </param>
        public void Save(bool setupAccountsAndSendEmailsIfNeeded)
        {
            Guid brokerId = Guid.Empty;

            DbConnectionInfo connInfo = DbConnectionInfo.GetConnectionInfoByLoanId(this.sLId, out brokerId);

            bool setupAccountsAndSendEmail = this.m_isNew && setupAccountsAndSendEmailsIfNeeded;
            using (CStoredProcedureExec spExec = new CStoredProcedureExec(connInfo))
            {
                try
                {
                    spExec.BeginTransactionForWrite();
                    Save(spExec);
                    spExec.CommitTransaction();
                    if (setupAccountsAndSendEmail)
                    {
                        EDocumentNotifier.SetupAccountsAndSendEmails(this);
                    }
                }
                catch (Exception e)
                {
                    Tools.LogError("Failed to save ActionItem " + this.ToString(), e);
                    RollbackTransaction(spExec);
                    throw;
                }
            }
        }

        /// <summary>
        /// Commit the changes of this object into the DB as part of atomic transaction.
        /// </summary>
        public void Save(CStoredProcedureExec spExec)
        {
            VerifyDataForSave();

            if (m_isNew)
            {
                CreateActionRequest(spExec);
                
                //Moving the notification for new requests to CreateRequestService as the notification needed to include the consumer account password can be created only after the request has been saved.
                //EDocumentNotifier.SendNewConsumerRequest(this);
            }
            else if (m_isDelete) 
            {
                DeleteActionRequest(spExec);
            }
            else
            {
                // If we are accepting the response, we delete this request/response,
                // its associated pdf and add the accepted doc to edocs system.
                if (m_acceptingResponse)
                {
                    CreateEdocAndDeleteRequest(spExec);
                }
                else
                {
                    UpdateActionRequest(spExec);

                    try
                    {
                        if (m_newBorrowerSignedEvent != null)
                            EDocumentNotifier.SendESignConfirmation(this, true);

                        if (m_newCoborrowerSignedEvent != null)
                            EDocumentNotifier.SendESignConfirmation(this, false);

                        if (m_completeType != E_CompleteType.None)
                            EDocumentNotifier.SendConsumerRequestCompletedNotification(this, m_completeType == E_CompleteType.Fax, m_completeType == E_CompleteType.Fax);
                    }
                    catch (Exception exc)
                    {
                        // We consider notification independent of saving.
                        // Let save finish even if these fail.
                        Tools.LogError("Failed to send notifications.", exc);
                    }
                }
            }

            m_newPdf = null;
            m_isNew = false;
            m_acceptingResponse = false;
        }

        private void UpdateActionRequest(CStoredProcedureExec spExec)
        {
            if (m_newPdf != null)
            {
                if (PdfFileDbKey == Guid.Empty)
                {
                    // This a new pdf, give it an id
                    PdfFileDbKey = GenerateNewFileDbKey();
                }
                else
                {
                    // We update an existing pdf, using the same id.
                }
            }

            List<SqlParameter> parameters = new List<SqlParameter>(
                new SqlParameter[] 
                {
                    new SqlParameter("@ConsumerActionRequestId", ConsumerActionRequestId )
                   , new SqlParameter("@ResponseStatus", (byte) ResponseStatus)
                   , new SqlParameter("@PdfFileDbKey", PdfFileDbKey )
                });

            if (ConsumerCompletedD.HasValue)
                parameters.Add(new SqlParameter("@ConsumerCompletedD", ConsumerCompletedD));
            if (ReceivingFaxNumber != null)
                parameters.Add(new SqlParameter("@ReceivingFaxNumber", ReceivingFaxNumber));
            if (BorrowerSignedEventId.HasValue)
                parameters.Add(new SqlParameter("@BorrowerSignedEventId", BorrowerSignedEventId));
            if (CoborrowerSignedEventId.HasValue)
                parameters.Add(new SqlParameter("@CoborrowerSignedEventId", CoborrowerSignedEventId));

            if (m_newBorrowerSignedEvent != null)
            {
                parameters.Add(new SqlParameter("@bSignedTransactionId", m_newBorrowerSignedEvent.SignedTransactionId));
                parameters.Add(new SqlParameter("@bSignature", m_newBorrowerSignedEvent.Signature));
                parameters.Add(new SqlParameter("@bSignedD", m_newBorrowerSignedEvent.SignedD));
                parameters.Add(new SqlParameter("@bIpAddress", m_newBorrowerSignedEvent.IpAddress));
                parameters.Add(new SqlParameter("@bEmailUsedToSign", m_newBorrowerSignedEvent.EmailUsedToSign));
                parameters.Add(new SqlParameter("@bPdfFileDbKey", m_newBorrowerSignedEvent.PdfFileDbKey));
            }

            if (m_newCoborrowerSignedEvent != null)
            {
                parameters.Add(new SqlParameter("@cSignedTransactionId", m_newCoborrowerSignedEvent.SignedTransactionId));
                parameters.Add(new SqlParameter("@cSignature", m_newCoborrowerSignedEvent.Signature));
                parameters.Add(new SqlParameter("@cSignedD", m_newCoborrowerSignedEvent.SignedD));
                parameters.Add(new SqlParameter("@cIpAddress", m_newCoborrowerSignedEvent.IpAddress));
                parameters.Add(new SqlParameter("@cEmailUsedToSign", m_newCoborrowerSignedEvent.EmailUsedToSign));
                parameters.Add(new SqlParameter("@cPdfFileDbKey", m_newCoborrowerSignedEvent.PdfFileDbKey));
            }

            spExec.ExecuteNonQuery("CP_UpdateActionRequestResponse", 3, parameters.ToArray());

            if (m_newPdf != null)
            {
                try
                {
                    FileDBTools.WriteData(E_FileDB.EDMS,PdfFileDbKey.ToString(), m_newPdf);
                }
                catch (Exception exc)
                {
                    // We set the id, but were unable to actually add it to filedb.
                    Tools.LogError("Unable to update filedb for action item", exc);
                    RollbackTransaction(spExec);
                    throw;
                }

            }

        }

        private void CreateActionRequest(CStoredProcedureExec spExec)
        {

            if (m_newPdf != null)
            {
                // We have a new pdf for filedb that goes along with this save.
                Guid newId = GenerateNewFileDbKey();
                FileDBTools.WriteData(E_FileDB.EDMS, newId.ToString().ToString(), m_newPdf);
                PdfFileDbKey = newId;
            }

            try
            {
                SqlParameter requestId = new SqlParameter("@ConsumerActionRequestId", SqlDbType.BigInt);
                requestId.Direction = ParameterDirection.Output;

                SqlParameter[] parameters = {
                                                requestId
                                                , new SqlParameter("@sLId",sLId)
                                                , new SqlParameter("@aAppId", aAppId )
                                                , new SqlParameter("@aBFullName", aBFullName)
                                                , new SqlParameter("@aCFullName", aCFullName)
                                                , new SqlParameter("@ResponseStatus", ResponseStatus)
                                                , new SqlParameter("@DocTypeId", DocTypeId )
                                                , new SqlParameter("@Description", Description)
                                                , new SqlParameter("@PdfFileDbKey", PdfFileDbKey)
                                                , new SqlParameter("@PdfMetaDataSnapshot", PdfMetaDataSnapshot) 
                                                , new SqlParameter("@IsApplicableToBorrower", IsApplicableToBorrower)
                                                , new SqlParameter("@IsApplicableToCoborrower",IsApplicableToCoborrower ) 
                                                , new SqlParameter("@IsSignatureRequested",IsSignatureRequested )
                                                , new SqlParameter("@IsESignAllowed",IsESignAllowed )
                                                , new SqlParameter("@aBInitials", aBInitials )
                                                , new SqlParameter("@aCInitials", aCInitials)
                                                , new SqlParameter("@CreatorEmployeeId", CreatorEmployeeId)
                                                , new SqlParameter("@sTitleBorrowerId", CoborrowerTitleBorrowerId)
                                            };
                spExec.ExecuteNonQuery("CP_CreateActionRequest", 3, parameters);

                ConsumerActionRequestId = (long)requestId.Value;

            }
            catch (Exception exc)
            {
                Tools.LogError("Unable to save ActionItem", exc);

                if (m_newPdf != null)
                {
                    // Disaster.  We created a new pdf in filedb, but were unable to save
                    // its id to the DB.  Delete this orphan from filedb.
                    FileDBTools.Delete(E_FileDB.EDMS,PdfFileDbKey.ToString());
                    RollbackTransaction(spExec);
                }
                throw;
            }
        }

        private void DeleteActionRequest( CStoredProcedureExec spExec)
        {
            bool shouldDeleteFromFileDB = false;
            try
            {
                shouldDeleteFromFileDB = DeleteRequestFromDb(spExec);
            }
            catch (Exception exc)
            {
                Tools.LogError("Could not delete action request from DB", exc);
                RollbackTransaction(spExec);
                throw;
            }

            if (PdfFileDbKey!= Guid.Empty && shouldDeleteFromFileDB)
            {
                // This filedb entry is not in the eVault.  Safe to destory
                // If this delete fails, we are rolling back DB to avoid orphan pdf
                try
                {
                    FileDBTools.Delete(E_FileDB.EDMS, PdfFileDbKey.ToString());
                }
                catch
                {
                    Tools.LogError("Failed to delete pdf: " + PdfFileDbKey.ToString());
                    RollbackTransaction(spExec);
                    throw;
                }
            }
        }

        private Guid ConvertPdfToEdoc()
        {
            string tempPath = FileDBTools.CreateCopy(E_FileDB.EDMS, PdfFileDbKey.ToString());

            EDocumentRepository repository = EDocumentRepository.GetUserRepository();
            EDocument eDoc = repository.CreateDocument(E_EDocumentSource.AcceptedFromConsumer);


            eDoc.IsUploadedByPmlUser = false;
            eDoc.FaxNumber = ReceivingFaxNumber;
            eDoc.LoanId = sLId;
            eDoc.AppId = aAppId;
            eDoc.DocumentTypeId = DocTypeId;
            eDoc.PublicDescription = Description;
            eDoc.UpdatePDFContentOnSave(tempPath);
            eDoc.EDocOrigin = E_EDocOrigin.LO;
            

            if (CreatorEmployeeId != Guid.Empty)
            {
                EmployeeDB db = new EmployeeDB(CreatorEmployeeId, BrokerUserPrincipal.CurrentPrincipal.BrokerId);
                db.Retrieve();
                if (CreatedOn.HasValue)
                {
                    eDoc.AddAuditEntry(E_PdfUserAction.SentToConsumer, CreatedOn.Value, db.UserID);
                }

                if (BorrowerSignedEventId.HasValue)
                {
                    DateTime? signedDate = GetSignedEventDate(db.BrokerID, BorrowerSignedEventId.Value);
                    if (signedDate.HasValue)
                    {
                        eDoc.AddAuditEntry(E_PdfUserAction.ConsumerSignature, signedDate.Value, null);
                        eDoc.MarkDocumentAsAccepted();
                    }
                }

                if (CoborrowerSignedEventId.HasValue)
                {
                    DateTime? signedDate = GetSignedEventDate(db.BrokerID, CoborrowerSignedEventId.Value);
                    if (signedDate.HasValue)
                    {
                        eDoc.AddAuditEntry(E_PdfUserAction.ConsumerSignature, signedDate.Value, null);
                        eDoc.MarkDocumentAsAccepted();
                    }
                }
            }

            repository.Save(eDoc);
            File.Delete(tempPath);
            eDoc = repository.GetDocumentById(eDoc.DocumentId);

            return eDoc.DocumentId;
        }

        private void CreateEdocAndDeleteRequest( CStoredProcedureExec spExec)
        {
            if (PdfFileDbKey == Guid.Empty)
                throw new CBaseException(ErrorMessages.Generic, "Cannot accept no response");

            // 1. Delete the request from the DB
            bool shouldDeleteFromFileDB = false;
            try
            {
                shouldDeleteFromFileDB = DeleteRequestFromDb(spExec);
            }
            catch (Exception exc)
            {
                Tools.LogError("Could not delete action request from DB", exc);
                RollbackTransaction(spExec);
                throw;
            }

            // 2. Create the eDoc
            Guid eDocId = Guid.Empty;
            try
            {
                eDocId = ConvertPdfToEdoc();
            }
            catch (Exception exc)
            {
                Tools.LogError("Could not copy accepted request document to eDocs", exc);
                RollbackTransaction(spExec);
                throw;
            }

            // 3. If this eDoc is not needed by eVault, we can delete it.
            if (shouldDeleteFromFileDB && PdfFileDbKey != Guid.Empty)
            {
                // This filedb entry is not in the eVault.  Safe to destory
                // If this delete fails, we are rolling back DB to avoid orphan pdf
                try
                {
                    FileDBTools.Delete(E_FileDB.EDMS,PdfFileDbKey.ToString());
                }
                catch
                {
                    Tools.LogError("Failed to delete pdf: " + PdfFileDbKey.ToString());
                    RollbackTransaction(spExec);

                    if (eDocId != Guid.Empty)
                    {
                        // We have to delete this eDoc, since we were unable to mark acceptance.
                        EDocumentRepository repository = EDocumentRepository.GetSystemRepository(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
                        repository.DeleteDocument(eDocId, "Error saving eDoc.");
                    }

                    throw;
                }
            }
        }

        private bool DeleteRequestFromDb(CStoredProcedureExec spExec)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            SqlParameter ShouldDeleteFromFileDB = new SqlParameter("@ShouldDeleteFromFileDB", SqlDbType.Bit);
            ShouldDeleteFromFileDB.Direction = ParameterDirection.Output;
            parameters.Add(ShouldDeleteFromFileDB);
            parameters.Add(new SqlParameter("@ConsumerActionRequestId", ConsumerActionRequestId));

            spExec.ExecuteNonQuery("CP_DeleteActionRequest", 3, parameters.ToArray());

            return (bool)ShouldDeleteFromFileDB.Value;
        }

        private void VerifyDataForSave()
        {
            // One event per save means one save per instance.

            if ( m_allowSave == false )
                throw new CBaseException(ErrorMessages.Generic, "Cannot save consumer action item.");

            m_allowSave = false;
        }

        private void RollbackTransaction( CStoredProcedureExec spExec )
        {
            try
            {
                spExec.RollbackTransaction();
            }
            catch (InvalidOperationException)
            {
                // Sometimes these are already rolled back at DB, which is also fine.
            }
        }

        #endregion

        #region Event Methods



        private void VerifyEventStatus()
        {
            // Saving gets very complicated when we allow multiple events.
            // For now, we will only allow one event call.

            if (m_allowEvent == false)
            {
                throw new CBaseException(ErrorMessages.Generic, "Cannot call multiple events on a ConsumerActionItem.");
            }
            m_allowEvent = false;
        }

        /// <summary>
        /// Request for consumer to send in a document.  Only call this
        /// when the request is new.
        /// </summary>
        public void RequestDocument(int docTypeId, string description)
        {
            VerifyEventStatus();

            if (!m_isNew)
            {
                throw new CBaseException(ErrorMessages.Generic, "Cannot re-request a document.");
            }

            IsSignatureRequested = false;
            IsESignAllowed = false;

            // Send in Docs requests are applicable to the entire app
            PdfFileDbKey = Guid.Empty;
            PdfMetaDataSnapshot = String.Empty;

            IsApplicableToBorrower = aBFullName != string.Empty;
            IsApplicableToCoborrower = aCFullName != string.Empty;

            DocTypeId = docTypeId;
            Description = description;

            ResponseStatus = E_ConsumerResponseStatusT.WaitingForConsumer;
        }

        /// <summary>
        /// Request for consumer to sign a document that is already stored in 
        /// EDMS FileDB. Should only be called for new requests
        /// </summary>
        /// <param name="edmsFileDbKey">The key to access the file in EDMS FileDB.</param>
        /// <param name="pdfMetaDataSnapshot">The document layout information.</param>
        /// <param name="docTypeId">The id of the DocType.</param>
        /// <param name="description">The description of the request.</param>
        /// <param name="isEsignAllowed">Indicates whether e-signing is allowed for the document.</param>
        /// <param name="isFormApplicableToBorrower">Indicates whether the form requires the borrower's signature.</param>
        /// <param name="isFormApplicabletoCoborrower">Indicates whether the form requires the co-borrower's signature.</param>
        public void RequestSignature(
            Guid edmsFileDbKey,
            string pdfMetaDataSnapshot,
            int docTypeId,
            string description,
            bool isEsignAllowed,
            bool isFormApplicableToBorrower,
            bool isFormApplicabletoCoborrower)
        {
            VerifyEventStatus();

            this.PdfFileDbKey = edmsFileDbKey;

            this.RequestSignature(
                pdfMetaDataSnapshot,
                docTypeId,
                description,
                isEsignAllowed,
                isFormApplicableToBorrower,
                isFormApplicabletoCoborrower);
        }

        /// <summary>
        /// Finish requesting the signature. The pdf file data (not the layout info) 
        /// should be set up when this is called. Callers are expected to verify that
        /// this event can occur.
        /// </summary>
        /// <param name="pdfMetaDataSnapshot"></param>
        /// <param name="docTypeId"></param>
        /// <param name="description"></param>
        /// <param name="isEsignAllowed"></param>
        /// <param name="isFormApplicableToBorrower"></param>
        /// <param name="isFormApplicabletoCoborrower"></param>
        private void RequestSignature(
            string pdfMetaDataSnapshot,
            int docTypeId,
            string description,
            bool isEsignAllowed,
            bool isFormApplicableToBorrower,
            bool isFormApplicabletoCoborrower)
        {
            if (!m_isNew)
            {
                throw new CBaseException(ErrorMessages.Generic, "Cannot re-request a signature.");
            }

            IsSignatureRequested = true;

            bool IsConsumerAbleToEsignDocs = true;
            // 6/12/2014 gf - Use security bypass because this is a system action
            // on behalf of the user. The Document Request Queue processor needs
            // to be able to send these without a principal.
            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId,typeof(ConsumerActionItem));
            dataLoan.InitLoad();
            if (dataLoan.BrokerDB.IsEnableNewConsumerPortal)
            {
                BranchDB branchDb = new BranchDB(dataLoan.sBranchId, dataLoan.sBrokerId);
                branchDb.Retrieve();
                if (branchDb.ConsumerPortalId.HasValue)
                    IsConsumerAbleToEsignDocs = false;
            }

            IsESignAllowed = isEsignAllowed && IsConsumerAbleToEsignDocs;
            PdfMetaDataSnapshot = pdfMetaDataSnapshot;

            if (isEsignAllowed == true)
            {
                if (isFormApplicableToBorrower == false && isFormApplicabletoCoborrower == false )
                    throw new CBaseException(ErrorMessages.Generic, "Cannot esign a form with no signature locations.");

                if ( isFormApplicableToBorrower == true && (string.IsNullOrEmpty(aBFullName) || string.IsNullOrEmpty(aBInitials ) ) )
                    throw new CBaseException(ErrorMessages.Generic, "Blank borrower name cannot esign.");

                if (isFormApplicabletoCoborrower == true && (string.IsNullOrEmpty(aCFullName) || string.IsNullOrEmpty(aCInitials)))
                    throw new CBaseException(ErrorMessages.Generic, "Blank coborrower name cannot esign.");
            }

            IsApplicableToBorrower = isFormApplicableToBorrower;
            IsApplicableToCoborrower = isFormApplicabletoCoborrower;

            DocTypeId = docTypeId;
            Description = description;

            ResponseStatus = E_ConsumerResponseStatusT.WaitingForConsumer;
        }

        /// <summary>
        /// Request has been cancelled by LO user.
        /// </summary>
        public void CancelRequest()
        {
            VerifyEventStatus();

            m_isDelete = true;
        }

        /// <summary>
        /// Request has been rejected by LO user.
        /// </summary>
        public void RejectRequest()
        {
            VerifyEventStatus();

            m_isDelete = true;
        }

        /// <summary>
        /// Accepts a user's uploaded file. Assumes that all requested signing was done by the user, since there's no way of knowing who signed the printed document.
        /// </summary>
        /// <param name="pdfFile"></param>
        public void AcceptConsumerUpload(byte[] pdfFile)
        {
            VerifyEventStatus();
            if (pdfFile == null) throw new CBaseException(ErrorMessages.Generic, "Need a pdf file from the user");

            int pageCount;
            bool hasPassword;

            EDocumentViewer.GetPdfInfo(pdfFile, out hasPassword, out pageCount);

            if (ResponseStatus != E_ConsumerResponseStatusT.WaitingForConsumer
                || ConsumerCompletedD.HasValue
                || BorrowerSignedEventId.HasValue
                || CoborrowerSignedEventId.HasValue)
                throw new CBaseException(ErrorMessages.Generic, "Can't accept a user upload if there's already been a response.");

            if (IsSignatureRequested && (IsApplicableToCoborrower || IsApplicableToBorrower))
            {
                E_eSignBorrowerMode mode;
                if (IsApplicableToBorrower && !IsApplicableToCoborrower) mode = E_eSignBorrowerMode.Borrower;
                else if (!IsApplicableToBorrower && IsApplicableToCoborrower) mode = E_eSignBorrowerMode.Coborrower;
                else if (IsApplicableToBorrower && IsApplicableToCoborrower) mode = E_eSignBorrowerMode.Both;
                else throw new CBaseException(ErrorMessages.Generic, 
                    "IsApplicableToBorrower and IsApplicableToCoborrower were in an inconsistent state; " + 
                    "at least one of them was true earlier but now both are false. Request id is: " + ConsumerActionRequestId);

                CreateSigningEvents(mode);
            }

            ConsumerCompletedD = DateTime.Now;
            m_completeType = E_CompleteType.UserUpload;
            m_newPdf = pdfFile;
            ResponseStatus = E_ConsumerResponseStatusT.PendingResponse;
        }

        /// <summary>
        /// A fax with barcode for this item came in. Attach it.
        /// </summary>
        public void AttachArrivedFax(string faxNumber, byte[] pdfFile)
        {
            VerifyEventStatus();

            if (pdfFile == null)
                throw new CBaseException(ErrorMessages.Generic, "Attaching a fax requests requires a pdf file.");

            if ( string.IsNullOrEmpty(faxNumber ) )
                throw new CBaseException(ErrorMessages.Generic, "Attaching a fax requires a fax number.");

            if (ResponseStatus != E_ConsumerResponseStatusT.WaitingForConsumer
                || ConsumerCompletedD.HasValue 
                || BorrowerSignedEventId.HasValue
                || CoborrowerSignedEventId.HasValue )
                throw new CBaseException(ErrorMessages.Generic, "Cannot attach fax when there is already a consumer response.");

            ConsumerCompletedD = DateTime.Now;
            m_completeType = E_CompleteType.Fax;
            m_newPdf = pdfFile;
            ReceivingFaxNumber = faxNumber;
            ResponseStatus = E_ConsumerResponseStatusT.PendingResponse;
        }

        /// <summary>
        /// For attaching a signature.  Either borrower, coborrower, or both.
        /// When signing occurs, we have to save a new pdf.
        /// </summary>
        public void SubmitSignature(E_eSignBorrowerMode signers)
        {
            VerifyEventStatus();

            if (IsSignatureRequested == false)
                throw new CBaseException(ErrorMessages.Generic, "Cannot sign a non-signature request.");

            if (IsESignAllowed == false)
                throw new CBaseException(ErrorMessages.Generic, "eSign not allowed on this request.");

            if (IsApplicableToBorrower == false && (signers == E_eSignBorrowerMode.Both || signers == E_eSignBorrowerMode.Borrower))
                throw new CBaseException(ErrorMessages.Generic, "Borrower cannot sign when request is not applicable to borrower");

            if (IsApplicableToCoborrower == false && (signers == E_eSignBorrowerMode.Both || signers == E_eSignBorrowerMode.Coborrower))
                throw new CBaseException(ErrorMessages.Generic, "Coborrower cannot sign when request is not applicable to coborrower");

            if (ResponseStatus != E_ConsumerResponseStatusT.WaitingForConsumer
                || ConsumerCompletedD.HasValue
                || (signers == E_eSignBorrowerMode.Borrower && BorrowerSignedEventId.HasValue)
                || (signers == E_eSignBorrowerMode.Coborrower && CoborrowerSignedEventId.HasValue)
                || (signers == E_eSignBorrowerMode.Both && (CoborrowerSignedEventId.HasValue || BorrowerSignedEventId.HasValue))
                )
                throw new CBaseException(ErrorMessages.Generic, "Cannot sign when there is already a consumer response.");

            DateTime now = DateTime.Now; // So we have identical signing time if both signatures occur

            m_newPdf = ESignDocument(signers, now);

            CreateSigningEvents(signers);

            if ((IsApplicableToBorrower == false || BorrowerSignedEventId.HasValue)
                && (IsApplicableToCoborrower == false || CoborrowerSignedEventId.HasValue))
            {
                // This signing event made this item user-complete.
                ResponseStatus = E_ConsumerResponseStatusT.PendingResponse;
                ConsumerCompletedD = now;
                m_completeType = E_CompleteType.Esigned;
            }
        }

        private void CreateSigningEvents(E_eSignBorrowerMode signers)
        {
            DateTime now = DateTime.Now;
            string ipAddress = GetCurrentIpAddress();

            string signingEmail = GetCurrentEmailAddress();

            if (signers == E_eSignBorrowerMode.Borrower || signers == E_eSignBorrowerMode.Both)
            {
                if (m_newBorrowerSignedEvent != null)
                    throw new CBaseException(ErrorMessages.Generic, "Borrower signing event cannot occur twice");

                m_newBorrowerSignedEvent = new ConsumerSignedEvent()
                {
                    EmailUsedToSign = signingEmail,
                    PdfFileDbKey = PdfFileDbKey,
                    IpAddress = ipAddress,
                    Signature = aBFullName,
                    SignedD = now,
                    SignedTransactionId = BorrowerSignedEventId ?? Guid.NewGuid()
                };
                BorrowerSignedEventId = m_newBorrowerSignedEvent.SignedTransactionId;
            }

            if (signers == E_eSignBorrowerMode.Coborrower || signers == E_eSignBorrowerMode.Both)
            {
                if (m_newCoborrowerSignedEvent != null)
                    throw new CBaseException(ErrorMessages.Generic, "Coborrower signing event cannot occur twice");

                m_newCoborrowerSignedEvent = new ConsumerSignedEvent()
                {
                    EmailUsedToSign = signingEmail,
                    PdfFileDbKey = PdfFileDbKey,
                    IpAddress = ipAddress,
                    Signature = aCFullName,
                    SignedD = now,
                    SignedTransactionId = CoborrowerSignedEventId ?? Guid.NewGuid()
                };
                CoborrowerSignedEventId = m_newCoborrowerSignedEvent.SignedTransactionId;
            }            
        }

        /// <summary>
        /// Approve the response.
        /// </summary>
        public void AcceptConsumerResponse()
        {
            VerifyEventStatus();

            if ( PdfFileDbKey == Guid.Empty )
                throw new CBaseException(ErrorMessages.Generic, "Cannot accept no response.");

            if (ResponseStatus != E_ConsumerResponseStatusT.PendingResponse
                || ConsumerCompletedD.HasValue == false
                )
                throw new CBaseException(ErrorMessages.Generic, "Cannot accept document not incomplete doc.");

            m_acceptingResponse = true;
            ResponseStatus = E_ConsumerResponseStatusT.Accepted;
        }

        #endregion

        #region Helper Methods

        private string GetCurrentEmailAddress()
        {
            ConsumerUserPrincipal consumer = Thread.CurrentPrincipal as ConsumerUserPrincipal;
            ConsumerPortalUserPrincipal consumer2 = Thread.CurrentPrincipal as ConsumerPortalUserPrincipal;
            if (consumer != null)
            {
                return consumer.Email;
            }

            if (consumer2 != null)
            {
                return consumer2.Email;
            }


            throw new CBaseException(ErrorMessages.Generic, "Cannot detect email of signing user");

        }

        private string GetCurrentIpAddress()
        {

            if (HttpContext.Current != null)
            {
                if (HttpContext.Current.Request != null)
                {
                    return HttpContext.Current.Request.UserHostAddress;
                }
                else
                {
                    throw new CBaseException(ErrorMessages.Generic, "Cannot determine ip address--no current http request");
                }
            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, "Cannot determine ip address--no current http context.");
            }
        }

        private byte[] ESignDocument(E_eSignBorrowerMode signingMode, DateTime signingTime)
        {
            using (MemoryStream outputStream = new MemoryStream())
            {

                Document document = new Document();

                PdfCopy writer = new PdfCopy(document, outputStream);
                document.Open();

                PdfFormLayout layout = new PdfFormLayout();

                using (MemoryStream reader = new MemoryStream(System.Text.ASCIIEncoding.ASCII.GetBytes(PdfMetaDataSnapshot)))
                {
                    layout.Load(reader);
                }

                PdfFormContainer container = new PdfFormContainer(FileDBTools.ReadData(E_FileDB.EDMS,PdfFileDbKey.ToString()), layout);

                if (signingMode == E_eSignBorrowerMode.Borrower || signingMode == E_eSignBorrowerMode.Both)
                {
                    BorrowerSignedEventId = Guid.NewGuid();
                    container.SetFieldValue("aBSignature", new PdfSignatureValue() { Name = aBFullName, SignedDate = signingTime, TransactionNumber = BorrowerSignedEventId.ToString() });
                    container.SetFieldValue("aBInitials",  aBInitials);

                }

                if (signingMode == E_eSignBorrowerMode.Coborrower || signingMode == E_eSignBorrowerMode.Both)
                {
                    CoborrowerSignedEventId = Guid.NewGuid();
                    container.SetFieldValue("aCSignature", new PdfSignatureValue() { Name = aCFullName, SignedDate = signingTime, TransactionNumber = CoborrowerSignedEventId.ToString() });
                    container.SetFieldValue("aCInitials", aCInitials);
                }

                container.AppendTo(writer);

                document.Close();

                return outputStream.ToArray();
            }
        }
        #endregion

        #region Static Methods

        /// <summary>
        /// Gets the fax cover sheet for this request.
        /// </summary>
        /// <param name="consumerActionRequestId"></param>
        /// <returns></returns>
        public static byte[] GetFaxCover(long consumerActionRequestId, Guid brokerId)
        {
            return GetFaxCover(consumerActionRequestId, true, brokerId);
        }


        public static byte[] GetFaxCover(long consumerActionRequestId, bool includeFaxCover, Guid? brokerIdForDatabase = null)
        {
            ConsumerActionItem item = new ConsumerActionItem(consumerActionRequestId, brokerIdForDatabase);

            byte[] currentPdf = null;
            if (item.PdfFileDbKey != Guid.Empty)
                currentPdf = FileDBTools.ReadData(E_FileDB.EDMS, item.PdfFileDbKey.ToString());

            if (!includeFaxCover) return currentPdf;



            if (item.HasBorrowerEsigned || item.HasCoborrowerEsigned)
                throw new CBaseException(ErrorMessages.Generic, "Fax is inapplicable when someone has signed.");

            if (item.ResponseStatus != E_ConsumerResponseStatusT.WaitingForConsumer)
                throw new CBaseException(ErrorMessages.Generic, "Fax is inapplicable when consumer action has already taken place.");

            // Use the official loan officer contact information for the company name.
            // We have to bypass security because consumer portal users do not have
            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(item.sLId, typeof(ConsumerActionItem));

            dataLoan.InitLoad();
            CAgentFields fields = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
            string lenderName = fields.CompanyName;

            Guid brokerId;
            DbConnectionInfo.GetConnectionInfoByLoanId(item.sLId, out brokerId);


            EDocsFaxNumber number = EDocsFaxNumber.Retrieve(brokerId);

            EdocFaxCover.FaxCoverData coverData = new EdocFaxCover.FaxCoverData()
            {
                Type = E_FaxCoverType.ConsumerPortalRequest,
                BrokerId = brokerId,
                ConsumerRequestId = consumerActionRequestId,
                LenderName = lenderName,
                DocTypeDescription = item.DocTypeName,
                ApplicationDescription = item.ApplicantNames,
                LoanNumber = dataLoan.sLNm,
                FaxNumber = number.FaxNumber,
                DocumentToAttach = currentPdf
            };

            return EdocFaxCover.GenerateBarcodePackagePdf( new EDocs.EdocFaxCover.FaxCoverData[] { coverData } );

        }

        public static DateTime? GetSignedEventDate(Guid brokerId, Guid id)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@SignedTransactionId", id)
                                        };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "CP_GetTransactionDateBySignedTransactionId", parameters))
            {
                if (reader.Read())
                {
                    DateTime signedD = (DateTime)reader["SignedD"];
                    return signedD;
                }
                else
                {
                    return null;
                }
            }
        }


        /// <summary>
        /// Get the list of filedb keys associated with the app that can be deleted when the app is deleted.
        /// </summary>
        public static List<Guid> GetFileDBKeysForDelete(Guid aAppId, CStoredProcedureExec spExec)
        {
            List<Guid> idList = new List<Guid>();
            using (DbDataReader reader = spExec.ExecuteReader("CP_ListConsumerFileIdsToDeleteByAppId", new SqlParameter("@aAppId", aAppId)))
            {
                while (reader.Read())
                {
                    idList.Add((Guid)reader["PdfFileDbKey"]);
                }
            }
            return idList;
        }

        public static Guid GenerateNewFileDbKey()
        {
            return Guid.NewGuid();
        }

        public static void MoveRequestAndResponse(Guid brokerId, Guid src_sLId, Guid src_aAppId, Guid dst_sLId, Guid dst_aAppId)
        {
            StoredProcedureHelper.ExecuteNonQuery(brokerId, "CP_MoveRequestResponse", 3, new SqlParameter[] {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@src_sLId", src_sLId),
                new SqlParameter("@src_aAppId", src_aAppId),
                new SqlParameter("@dst_sLId", dst_sLId),
                new SqlParameter("@dst_aAppId", dst_aAppId)
            });
        }

        #endregion

        /// <summary>
        /// POD Class for a signing event
        /// </summary>
        private class ConsumerSignedEvent
        {
            public Guid SignedTransactionId;
            public string Signature;
            public DateTime? SignedD;
            public string IpAddress;
            public string EmailUsedToSign;
            public Guid PdfFileDbKey;
        }
    }
}

