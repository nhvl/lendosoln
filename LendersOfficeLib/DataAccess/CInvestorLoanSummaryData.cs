﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{
    class CInvestorLoanSummaryData : CPageData 
    {
        private static CSelectStatementProvider s_selectProvider;
        static CInvestorLoanSummaryData()
        {
            StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("sfGetAgentOfRole");
            list.Add("sLNm");
            list.Add("aBIsValidNameSsn");
            list.Add("aBIsPrimaryWageEarner");
            list.Add("aBNm");
            list.Add("aBSsn");
            list.Add("aBExperianScore");
            list.Add("aBTransUnionScore");
            list.Add("aBEquifaxScore");
            list.Add("aBCitizenDescription");
            list.Add("aBIsSelfEmplmt");
            list.Add("aBTotI");
            list.Add("aBSecondHighestScoreAgency");
            list.Add("aCIsValidNameSsn");
            list.Add("aCIsPrimaryWageEarner");
            list.Add("aCNm");
            list.Add("aCSsn");
            list.Add("aCExperianScore");
            list.Add("aCTransUnionScore");
            list.Add("aCEquifaxScore");
            list.Add("aCCitizenDescription");
            list.Add("aCIsSelfEmplmt");
            list.Add("aCTotI");
            list.Add("aCSecondHighestScoreAgency");
            list.Add("sCreditScoreType2Soft");
            list.Add("sHas1stTimeBuyer");
            list.Add("sProdHasHousingHistory");
            list.Add("sProd3rdPartyUwResultT");
            list.Add("sProdIncludeNormalProc");
            list.Add("sProdIncludeMyCommunityProc");
            list.Add("sProdIncludeHomePossibleProc");
            list.Add("sProdIncludeFHATotalProc");
            list.Add("sProdIncludeVAProc");
            list.Add("sProdIsDuRefiPlus");
            list.Add("sProdDocT");
            list.Add("sLTotI");
            list.Add("sProThisMPmt");
            list.Add("sMonthlyPmt");
            list.Add("sPresLTotHExp");
            list.Add("sTransmOMonPmt");
            list.Add("sTotalFixedPmt");
            list.Add("sQualTopR");
            list.Add("sQualBottomR");
            list.Add("sProdAvailReserveMonths");
            list.Add("sOccT");
            list.Add("sPurchPrice");
            list.Add("sApprVal");
            list.Add("sProRealETx");
            list.Add("sProOHExp");
            list.Add("sOccR");
            list.Add("sSpGrossRent");
            list.Add("sProdSpT");
            list.Add("sProdSpStructureT");
            list.Add("sProdIsSpInRuralArea");
            list.Add("sProdIsCondotel");
            list.Add("sProdIsNonwarrantableProj");
            list.Add("sSpAddr");
            list.Add("sSpCity");
            list.Add("sSpZip");
            list.Add("sSpCounty");
            list.Add("sLAmtCalc");
            list.Add("sFfUfmipFinanced");
            list.Add("sFinalLAmt");
            list.Add("sLtvR");
            list.Add("sCltvR");
            list.Add("sNoteIR");
            list.Add("sLPurposeT");
            list.Add("sProdCashoutAmt");
            list.Add("sProdImpound");
            list.Add("sProdMIOptionT");
            list.Add("sProdPpmtPenaltyMon");
            list.Add("sPpmtPenaltyMon");
            list.Add("sTerm");
            list.Add("sDue");
            list.Add("sLienPosT");
            list.Add("sFinMethT");
            list.Add("sIOnlyMon");
            list.Add("sRAdj1stCapMon");
            list.Add("sRAdjLifeCapR");
            list.Add("sRAdjMarginR");
            list.Add("sInvestorLockLpTemplateNm");
            list.Add("sInvestorLockLpInvestorNm");
            list.Add("sStatusT");
            list.Add("sRateLockStatusT");
            list.Add("sInvestorLockRateLockStatusT");
            list.Add("sRLckdD");
            list.Add("sRLckdDays");
            list.Add("sInvestorLockLockBuffer");
            list.Add("sRLckdExpiredD");
            list.Add("sLpProdCode");
            list.Add("sFreddieLoanId");
            list.Add("sDuCaseId");
            #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }
        public CInvestorLoanSummaryData(Guid fileId)
            : base(fileId, "CInvestorLoanSummaryData", s_selectProvider)
		{
		} 

    }
}
