﻿namespace DataAccess
{
    using System.Collections.Generic;
    using LendersOffice.CreditReport;

    /// <summary>
    /// Interface for the fields associated with a public record, which is a court case reported to the credit repositories.
    /// </summary>
    public interface IPublicRecord : ICollectionItemBase2
    {
        /// <summary>
        /// Gets the audit records.
        /// </summary>
        /// <value>The audit records.</value>
        List<CPublicRecordAuditItem> AuditTrailItems { get; }

        /// <summary>
        /// Gets the bankruptcy liabilities amount.
        /// </summary>
        /// <value>The bankruptcy liabilities amount.</value>
        decimal BankruptcyLiabilitiesAmount { get; }

        /// <summary>
        /// Gets or sets the bankruptcy liabilities amount as a string.
        /// </summary>
        /// <value>The bankruptcy liabilities amount as a string.</value>
        string BankruptcyLiabilitiesAmount_rep { get; set; }

        /// <summary>
        /// Gets the date when the bankruptcy was filed.
        /// </summary>
        /// <value>The date when the bankruptcy was filed.</value>
        CDateTime BkFileD { get; }

        /// <summary>
        /// Gets or sets the date when the bankruptcy was filed as a string.
        /// </summary>
        /// <value>The date when the bankruptcy was filed as a string.</value>
        string BkFileD_rep { get; set; }

        /// <summary>
        /// Gets or sets the name of the court where the public record is filed.
        /// </summary>
        /// <value>The name of the court where the public record is filed.</value>
        string CourtName { get; set; }

        /// <summary>
        /// Gets the date when the most recent disposition change was made on the court case.
        /// </summary>
        /// <value>The date when the decision was made on the court case.</value>
        CDateTime DispositionD { get; }

        /// <summary>
        /// Gets or sets the date when the most recent disposition change was made on the court case as a string.
        /// </summary>
        /// <value>The date when the decision was made on the court case as a string.</value>
        string DispositionD_rep { get; set; }

        /// <summary>
        /// Gets or sets the state of the current disposition for the court case.
        /// </summary>
        /// <value>The state of the current disposition for the court case.</value>
        E_CreditPublicRecordDispositionType DispositionT { get; set; }

        /// <summary>
        /// Gets or sets the identifier for the public record from the credit report data.
        /// </summary>
        /// <value>The identifier for the public record from the credit report data.</value>
        string IdFromCreditReport { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the public record should be included in pricing calculations.
        /// </summary>
        /// <value>A value indicating whether the public record should be included in pricing calculations.</value>
        bool IncludeInPricing { get; set; }

        /// <summary>
        /// Gets a value indicating whether this public record has been modified.
        /// </summary>
        /// <value>A value indicating whether this public record has been modified.</value>
        bool IsModified { get; }

        /// <summary>
        /// Gets the last effective date.
        /// </summary>
        /// <value>The last effective date.</value>
        CDateTime LastEffectiveD { get; }

        /// <summary>
        /// Gets or sets the last effective date as a string.
        /// </summary>
        /// <value>The last effective date as a string.</value>
        string LastEffectiveD_rep { get; set; }

        /// <summary>
        /// Gets or sets the owner of the public record.
        /// </summary>
        /// <value>The owner of the public record.</value>
        E_PublicRecordOwnerT OwnerT { get; set; }

        /// <summary>
        /// Gets the date when the public record was reported to the credit repositories.
        /// </summary>
        /// <value>The date when the public record was reported to the credit repositories.</value>
        CDateTime ReportedD { get; }

        /// <summary>
        /// Gets or sets the date when the public record was reported to the credit repositories as a string.
        /// </summary>
        /// <value>The date when the public record was reported to the credit repositories as a string.</value>
        string ReportedD_rep { get; set; }

        /// <summary>
        /// Gets or sets the type of the public record.
        /// </summary>
        /// <value>The type of the public record.</value>
        E_CreditPublicRecordType Type { get; set; }
    }
}