using System;
using System.Collections;
using System.Data;

namespace DataAccess
{

	public class CCreateSubfinancingLoanData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CCreateSubfinancingLoanData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
	        list.Add("sLinkedLoanInfo");
            list.Add("sLienPosT");
            list.Add("sfSpinOffLoanForSubsequent2ndLienLoan");
            list.Add("IsTemplate");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CCreateSubfinancingLoanData(Guid fileId) : base(fileId, "CCreateSubfinancingLoanData", s_selectProvider)
		{
		}
	}
}
