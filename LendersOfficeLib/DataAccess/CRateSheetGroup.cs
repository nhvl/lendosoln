﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using LendersOffice.Common;
using LendersOfficeApp.los.RatePrice;

namespace DataAccess
{
    public class CRateSheetGroup
    {
        public const string IO_DISQUAL_MSG = "THIS IS AN INTEREST ONLY PROGRAM";
        public const string NO_IO_DISQUAL_MSG = "INTEREST ONLY NOT ALLOWED FOR THIS PROGRAM";
        public const string NO_RATES_FOR_PP = "RATES NOT AVAILABLE FOR SELECTED PREPAYMENT PENALTY";
        public const string NO_RATES_FOR_LOCK = "RATES NOT AVAILABLE FOR SELECTED LOCK PERIOD";
        private int m_min_sProdRLckdDays = int.MinValue;
        private int m_max_sProdRLckdDays = int.MaxValue;
        private int m_min_sProdPpmtPenaltyMon = int.MinValue;
        private int m_max_sProdPpmtPenaltyMon = int.MaxValue;
        private int m_min_sIOnlyMon = int.MinValue;
        private int m_max_sIOnlyMon = int.MaxValue;

        private List<IRateItem> m_list;

        public CRateSheetGroup(int minsProdRLckdDays, int maxsProdRLckdDays, int minsProdPpmtPenaltyMon, int maxsProdPpmtPenaltyMon, int minsIOnlyMon, int maxsIOnlyMon)
        {
            Tools.Assert(minsProdPpmtPenaltyMon <= maxsProdPpmtPenaltyMon, "Invalid range for sProdPpmtPenaltyMon. Min need to be smaller.");
            Tools.Assert(minsProdRLckdDays <= maxsProdRLckdDays, "Invalid range for sProdRLckdDays. Min need to be smaller.");
            Tools.Assert(minsIOnlyMon <= maxsIOnlyMon, "Invalid range for sIOnlyMon. Min need to be smaller.");

            m_min_sProdPpmtPenaltyMon = minsProdPpmtPenaltyMon;
            m_max_sProdPpmtPenaltyMon = maxsProdPpmtPenaltyMon;
            m_min_sProdRLckdDays = minsProdRLckdDays;
            m_max_sProdRLckdDays = maxsProdRLckdDays;
            m_min_sIOnlyMon = minsIOnlyMon;
            m_max_sIOnlyMon = maxsIOnlyMon;

            m_list = new List<IRateItem>();
        }

        public void AddRate(IRateItem field)
        {
            m_list.Add(field);
        }

        public bool IsInRange(int sProdRLckdDays, int sProdPpmtPenaltyMon, int sIOnlyMon)
        {
            return m_min_sProdRLckdDays <= sProdRLckdDays && sProdRLckdDays <= m_max_sProdRLckdDays &&
                m_min_sProdPpmtPenaltyMon <= sProdPpmtPenaltyMon && sProdPpmtPenaltyMon <= m_max_sProdPpmtPenaltyMon &&
                m_min_sIOnlyMon <= sIOnlyMon && sIOnlyMon <= m_max_sIOnlyMon;
        }

        public bool IsInRange(E_RateSheetKeywordT keyword, int period)
        {
            switch (keyword)
            {
                case E_RateSheetKeywordT.Lock: return m_min_sProdRLckdDays <= period && period <= m_max_sProdRLckdDays;
                case E_RateSheetKeywordT.Prepay: return m_min_sProdPpmtPenaltyMon <= period && period <= m_max_sProdPpmtPenaltyMon;
                case E_RateSheetKeywordT.IO: return m_min_sIOnlyMon <= period && period <= m_max_sIOnlyMon;
                default:
                    throw new UnhandledEnumException(keyword);
            }
        }
        public IRateItem[] RateSheetFields
        {
            get
            {
                return m_list.ToArray();
            }

        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("CRateSheetGroup:[sProdRLckdDays={0},{1} | sProdPpmtPenaltyMon={2}, {3} | sIOnlyMon={4}, {5}]", m_min_sProdRLckdDays, m_max_sProdRLckdDays, m_min_sProdPpmtPenaltyMon, m_max_sProdPpmtPenaltyMon, m_min_sIOnlyMon, m_max_sIOnlyMon);

            foreach (CRateSheetFields o in m_list)
            {
                sb.AppendFormat("{0}   Rate={1}, Point={2}, Margin={3}", Environment.NewLine, o.Rate_rep, o.Point_rep, o.Margin_rep);
            }
            return sb.ToString();
        }

        // 06/23/09 OPM 2950
        // Verify that there exists the appropriate pricing segment on the rate sheet.
        public static bool VerifyRateOptionPricingSegment(List<CRateSheetGroup> RateSheetGroupList, int sProdRLckdDays, int sProdPpmtPenaltyMon, int sIOnlyMon, ref string DisqualMessage)
        {

            if (RateSheetGroupList == null || RateSheetGroupList.Count == 0)
            {
                // Blank rate sheet can create this situation.
                DisqualMessage = "RATES NOT AVAILABLE.";
                return false;
            }

            System.Collections.Generic.List<CRateSheetGroup> segments = new System.Collections.Generic.List<CRateSheetGroup>(RateSheetGroupList.Count);
            foreach (CRateSheetGroup rateSheetGroup in RateSheetGroupList)
            {
                if (rateSheetGroup.IsInRange(sProdRLckdDays, sProdPpmtPenaltyMon, sIOnlyMon))
                {
                    // There exists a segment for this
                    return true;
                }

                if (rateSheetGroup.IsInRange(E_RateSheetKeywordT.IO, sIOnlyMon))
                {
                    segments.Add(rateSheetGroup);
                }
            }

            // We need to determine which error message to show using
            // this priority from OPM 2950.
            // 1. I/O message if we cannot group into IO
            // 2. PP message if we cannot group into PP
            // 3. LOCK message.

            if (segments.Count == 0)
            {
                // The request does not fit in any IO group
                DisqualMessage = sIOnlyMon == 0 ? IO_DISQUAL_MSG : NO_IO_DISQUAL_MSG;
                return false;
            }

            segments.RemoveAll((CRateSheetGroup group) => !group.IsInRange(E_RateSheetKeywordT.Prepay, sProdPpmtPenaltyMon));
            if (segments.Count == 0)
            {
                // There is at least one IO group where this request fits, but none that also allow the PP value
                DisqualMessage = NO_RATES_FOR_PP;
                return false;
            }

            segments.RemoveAll((CRateSheetGroup group) => !group.IsInRange(E_RateSheetKeywordT.Lock, sProdRLckdDays));
            if (segments.Count == 0)
            {
                // There is a least one group where both IO and PP fit, but none that also allow the LOCK value
                DisqualMessage = NO_RATES_FOR_LOCK;
                return false;
            }

            // Execution getting here means that we detected no valid segment,
            // but found one while calculating the disqual message.  Logic error.
            throw new CBaseException(ErrorMessages.Generic, "No valid rate group found, but no grouping violations found.");
        }

    }

}
