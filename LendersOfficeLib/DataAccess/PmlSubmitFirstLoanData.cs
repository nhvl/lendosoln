using System;
using System.Collections;
using System.Data;

namespace DataAccess
{

	public class CPmlSubmitFirstLoanData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CPmlSubmitFirstLoanData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("sfTransformLoPropertyTypeToPml");
            list.Add("sfApplyLoanProductTemplate");
            list.Add("sfSpinOffLoanForSubsequent2ndLienLoan");
            list.Add("sfConfirm1stLienLoanTentativelyAccepted");
            list.Add("sLpTemplateId");
            list.Add("sNoteIRSubmitted");
            list.Add("sLNm");
            list.Add("sProThisMPmt");
            list.Add("aBEquifaxScore");  
            list.Add("aBExperianScore");  
            list.Add("aBNm");
            list.Add("aBTransUnionScore");  
            list.Add("aCEquifaxScore"); 
            list.Add("aCExperianScore");  
            list.Add("aCNm");
            list.Add("aCSsn");
            list.Add("aCTransUnionScore"); 
            list.Add("sCltvR");  
            list.Add("sCreditScoreType1");
            list.Add("sCreditScoreType2");
            list.Add("sCreditScoreType3");
            list.Add("sFinMethT");
            list.Add("sIsIOnly");
            list.Add("sIsSelfEmployed");
            list.Add("sLAmtCalc");
            list.Add("sLPurposeT");  
            list.Add("sLiaMonLTot");  
            list.Add("sLienPosT");
            list.Add("sLtvR");
            list.Add("sOccT");
            list.Add("sProdCashoutAmt");
            list.Add("sProdCondoStories");
            list.Add("sProdDocT");
            list.Add("sProdImpound");
            list.Add("sProdPpmtPenaltyMon");
            list.Add("sProdRLckdDays");
            list.Add("sProdSpT");
            list.Add("sSpState");
            list.Add("sStatusT");
            list.Add("sUnitsNum");
            list.Add("sQualIR");
            list.Add("sNoteIR");
            list.Add("sTerm");
            list.Add("sLT");
            list.Add("sPreparerXmlContent");
            list.Add("sfGetAgentOfRole");
            list.Add("sEmployeeLenderAccExec");
            list.Add(nameof(CPageBase.SwapFor2ndLienQualifying));
            list.Add(nameof(CPageBase.MakeThisInto2ndLienForQualifying));
            list.Add(nameof(CPageBase.sClosingCostSetJsonContent));
            list.Add(nameof(CPageBase.sFeeServiceApplicationHistoryXmlContent));
            list.Add("sProdIsCreditScoresAltered");
            list.Add("sLNm");
            list.Add("sProOHExp");
            list.Add("sProRealETx");
            list.Add("sTransmOMonPmt");
            list.Add("sPmlCertXmlContent");
            list.Add("sPpmtPenaltyMon");
            list.Add("sRLckdDays");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CPmlSubmitFirstLoanData(Guid fileId) : base(fileId, "CPmlSubmitFirstLoanData", s_selectProvider)
		{
		}
	}
}
