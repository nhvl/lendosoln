﻿namespace DataAccess
{
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Class used by the EmploymentRecordShim to override the default formatting for semantic types.
    /// </summary>
    public class FormatEmploymentRecordShimAsString : FormatEntityShimAsString
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FormatEmploymentRecordShimAsString"/> class.
        /// </summary>
        /// <param name="losConvert">The LosConvert instance to use for formatting.</param>
        public FormatEmploymentRecordShimAsString(LosConvert losConvert)
            : base(losConvert)
        {
        }

        /// <summary>
        /// Format an UnzonedDate value as a string.
        /// </summary>
        /// <param name="value">The UnzonedDate value.</param>
        /// <returns>The string representation of the UnzonedDate value.</returns>
        public override string Format(UnzonedDate? value)
        {
            string date = value == null ? string.Empty : value.Value.ToString();
            return this.Converter.ToDateTimeVarCharString(date);
        }
    }
}
