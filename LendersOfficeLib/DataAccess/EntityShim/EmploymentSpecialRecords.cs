﻿namespace DataAccess
{
    using System;

    /// <summary>
    /// Class that determines which employment records are considered special.
    /// </summary>
    public class EmploymentSpecialRecords : IDetermineSpecialEntity
    {
        /// <summary>
        /// Determine whether or not a particular record type is special.
        /// </summary>
        /// <param name="type">The record type.</param>
        /// <returns>True if the record type is determined to be a special record, false otherwise.</returns>
        public bool IsSpecial(Enum type)
        {
            return (E_EmplmtStat)type == E_EmplmtStat.Current;
        }

        /// <summary>
        /// Determine whether the input employment record is special.
        /// </summary>
        /// <param name="shim">The shim containing the employment record of interest.</param>
        /// <returns>True if the employment record is special, false otherwise.</returns>
        public bool IsSpecial(ICollectionItemBase2 shim)
        {
            var employment = (IEmploymentRecord)shim;
            return employment.EmplmtStat == E_EmplmtStat.Current;
        }
    }
}
