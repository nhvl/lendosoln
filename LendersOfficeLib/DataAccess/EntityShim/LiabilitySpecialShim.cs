﻿namespace DataAccess
{
    using System;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Abstract class for the special liabilities.
    /// </summary>
    public abstract class LiabilitySpecialShim : LiabilityShim, ILiabilitySpecial
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LiabilitySpecialShim"/> class.
        /// </summary>
        /// <param name="shimContainer">The container that holds all of the classes wrapped by the shims and allows accessing / updating data in the other collections.</param>
        /// <param name="collectionShim">The record collection.</param>
        /// <param name="contained">The contained entity.</param>
        /// <param name="ownershipManager">The ownership manager for the collection records.</param>
        /// <param name="recordId">The record id.</param>
        /// <param name="stringFormatter">The string formatter.</param>
        /// <param name="numericFormatter">The numeric formatter.</param>
        /// <param name="stringParser">The string parser.</param>
        protected LiabilitySpecialShim(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            LendingQB.Core.Data.Liability contained,
            IShimOwnershipManager<Guid> ownershipManager,
            DataObjectIdentifier<DataObjectKind.Liability, Guid> recordId,
            IFormatAsString stringFormatter,
            IFormatAsNumeric numericFormatter,
            IParseFromString stringParser)
            : base(shimContainer, collectionShim, contained, ownershipManager, recordId, stringFormatter, numericFormatter, stringParser)
        {
        }

        /// <summary>
        /// Gets or sets the debt type.
        /// </summary>
        /// <value>The debt type.</value>
        public new E_DebtSpecialT DebtT
        {
            get
            {
                return (E_DebtSpecialT)base.DebtT;
            }

            protected set
            {
                base.DebtT = (E_DebtT)value;
            }
        }
    }
}
