﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using LendersOffice.CreditReport;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Shim that presents an IVerificationOfRent interface around a VorRecord entity.
    /// </summary>
    public sealed class VorRecordShim : CollectionItemBaseShim, IVerificationOfRent
    {
        /// <summary>
        /// Name to use for the DataTable constructed to pass out data for binding to user controls.
        /// </summary>
        private const string TableName = "VorXmlContent";

        /// <summary>
        /// Initializes a new instance of the <see cref="VorRecordShim"/> class.
        /// </summary>
        /// <param name="collectionShim">The shim for the collection that contains this entity shim.</param>
        /// <param name="contained">The VorRecord entity that holds the data.</param>
        /// <param name="recordId">The identifier for this VOR record.</param>
        /// <param name="stringFormatter">Object that controls how semantic types are serialized as strings.</param>
        /// <param name="numericFormatter">Object that controls how semantic types are converted to numeric values.</param>
        /// <param name="stringParser">Object that controls how strings are parsed into semantic types.</param>
        private VorRecordShim(
            IVerificationOfRentCollection collectionShim,
            VorRecord contained,
            DataObjectIdentifier<DataObjectKind.VorRecord, Guid> recordId,
            IFormatAsString stringFormatter,
            IFormatAsNumeric numericFormatter,
            IParseFromString stringParser)
            : base(recordId.Value, stringFormatter, numericFormatter, stringParser)
        {
            this.Collection = collectionShim;
            this.Contained = contained;

            if (contained.LegacyRecordSpecialType != null)
            {
                this.VerifT = E_VorT.Rental;

                // If we are imitating a special record, use the special record's
                // hard-coded id as our record id.
                this.RecordId = VorRecord.LegacyRecordIdToSpecialType
                    .Single(kvp => kvp.Value == contained.LegacyRecordSpecialType)
                    .Key;
            }
        }

        /// <summary>
        /// Gets or sets the AccountName of the information to be verified.
        /// </summary>
        public string AccountName
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.AccountName);
            }

            set
            {
                this.Contained.AccountName = this.StringParser.TryParseDescriptionField(value);
            }
        }

        /// <summary>
        /// Gets or sets the Address of the information to be verified.
        /// </summary>
        public string AddressFor
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.StreetAddressFor);
            }

            set
            {
                this.Contained.StreetAddressFor = this.StringParser.TryParseStreetAddress(value);
            }
        }

        /// <summary>
        /// Gets the single line address of the information to be verified.
        /// </summary>
        public string AddressFor_SingleLine
        {
            get
            {
                return Tools.FormatSingleLineAddress(this.AddressFor, this.CityFor, this.StateFor, this.ZipFor);
            }
        }

        /// <summary>
        /// Gets or sets the Address of the mortgage holder, credit union, or landlord.
        /// </summary>
        public string AddressTo
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.StreetAddressTo);
            }

            set
            {
                this.Contained.StreetAddressTo = this.StringParser.TryParseStreetAddress(value);
            }
        }

        /// <summary>
        /// Gets or sets the Attention of the "To" address of the verification.
        /// </summary>
        public string Attention
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.Attention);
            }

            set
            {
                this.Contained.Attention = this.StringParser.TryParseEntityName(value);
            }
        }

        /// <summary>
        /// Gets or sets the City of the information to be verified.
        /// </summary>
        public string CityFor
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.CityFor);
            }

            set
            {
                this.Contained.CityFor = this.StringParser.TryParseCity(value);
            }
        }

        /// <summary>
        /// Gets or sets the City of the mortgage holder, credit union, or landlord.
        /// </summary>
        public string CityTo
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.CityTo);
            }

            set
            {
                this.Contained.CityTo = this.StringParser.TryParseCity(value);
            }
        }

        /// <summary>
        /// Gets or sets the Description of the verification.
        /// </summary>
        public string Description
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.Description);
            }

            set
            {
                this.Contained.Description = this.StringParser.TryParseDescriptionField(value);
            }
        }

        /// <summary>
        /// Gets or sets the Fax of the mortgage holder, credit union, or landlord.
        /// </summary>
        public string FaxTo
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.FaxTo);
            }

            set
            {
                this.Contained.FaxTo = this.StringParser.TryParsePhoneNumber(value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to indicate to see 
        /// attachement for borrower signature on the documents.
        /// </summary>
        public bool IsSeeAttachment
        {
            get
            {
                return this.BooleanFromNullable(this.Contained.IsSeeAttachment);
            }

            set
            {
                this.Contained.IsSeeAttachment = value;
            }
        }

        /// <summary>
        /// Gets or sets a type for an enumeration that is used as a key.
        /// </summary>
        public Enum KeyType
        {
            get
            {
                return this.VerifT;
            }

            set
            {
                this.VerifT = (E_VorT)value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the mortgage holder, credit union, or landlord.
        /// </summary>
        public string LandlordCreditorName
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.LandlordCreditorName);
            }

            set
            {
                this.Contained.LandlordCreditorName = this.StringParser.TryParseEntityName(value);
            }
        }

        /// <summary>
        /// Gets or sets the Phone of the mortgage holder, credit union, or landlord.
        /// </summary>
        public string PhoneTo
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.PhoneTo);
            }

            set
            {
                this.Contained.PhoneTo = this.StringParser.TryParsePhoneNumber(value);
            }
        }

        /// <summary>
        /// Gets or sets the Prepared date of the verification.
        /// </summary>
        public CDateTime PrepD
        {
            get
            {
                return this.UnzonedDateToCDateTime(this.Contained.PrepDate);
            }

            set
            {
                this.Contained.PrepDate = this.CDateTimeToUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the State of the information to be verified.
        /// </summary>
        public string StateFor
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.StateFor);
            }

            set
            {
                this.Contained.StateFor = this.StringParser.TryParseUnitedStatesPostalState(value);
            }
        }

        /// <summary>
        /// Gets or sets the State the mortgage holder, credit union, or landlord.
        /// </summary>
        public string StateTo
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.StateTo);
            }

            set
            {
                this.Contained.StateTo = this.StringParser.TryParseUnitedStatesPostalState(value);
            }
        }

        /// <summary>
        /// Gets or sets the expected date of the verification.
        /// </summary>
        public CDateTime VerifExpD
        {
            get
            {
                return this.UnzonedDateToCDateTime(this.Contained.VerifExpDate);
            }

            set
            {
                this.Contained.VerifExpDate = this.CDateTimeToUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the expected date of the verification.
        /// </summary>
        public string VerifExpD_rep
        {
            get
            {
                return this.FormatUnzonedDate(this.VerifExpD, this.Contained.VerifExpDate);
            }

            set
            {
                this.Contained.VerifExpDate = this.StringParser.TryParseUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the verification has a signature.
        /// </summary>
        public bool VerifHasSignature
        {
            get
            {
                return !string.IsNullOrEmpty(this.VerifSignatureImgId);
            }
        }

        /// <summary>
        /// Gets or sets the received date of the verification.
        /// </summary>
        public CDateTime VerifRecvD
        {
            get
            {
                return this.UnzonedDateToCDateTime(this.Contained.VerifRecvDate);
            }

            set
            {
                this.Contained.VerifRecvDate = this.CDateTimeToUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the received date of the verification.
        /// </summary>
        public string VerifRecvD_rep
        {
            get
            {
                return this.FormatUnzonedDate(this.VerifRecvD, this.Contained.VerifRecvDate);
            }

            set
            {
                this.Contained.VerifRecvDate = this.StringParser.TryParseUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets thr re-ordered of the verification.
        /// </summary>
        public CDateTime VerifReorderedD
        {
            get
            {
                return this.UnzonedDateToCDateTime(this.Contained.VerifReorderedDate);
            }

            set
            {
                this.Contained.VerifReorderedDate = this.CDateTimeToUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the re-ordered date of the verification.
        /// </summary>
        public string VerifReorderedD_rep
        {
            get
            {
                return this.FormatUnzonedDate(this.VerifReorderedD, this.Contained.VerifReorderedDate);
            }

            set
            {
                this.Contained.VerifReorderedDate = this.StringParser.TryParseUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the ordered date of the verification.
        /// </summary>
        public CDateTime VerifSentD
        {
            get
            {
                return this.UnzonedDateToCDateTime(this.Contained.VerifSentDate);
            }

            set
            {
                this.Contained.VerifSentDate = this.CDateTimeToUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the ordered date of the verification.
        /// </summary>
        public string VerifSentD_rep
        {
            get
            {
                return this.FormatUnzonedDate(this.VerifSentD, this.Contained.VerifSentDate);
            }

            set
            {
                this.Contained.VerifSentDate = this.StringParser.TryParseUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets the ID of the signature image of the verification.
        /// </summary>
        public string VerifSignatureImgId
        {
            get
            {
                if (this.Contained.VerifSignatureImgId == null)
                {
                    return string.Empty;
                }
                else
                {
                    return this.Contained.VerifSignatureImgId.Value.ToString();
                }
            }
        }

        /// <summary>
        /// Gets the Signing EmployeeId of the verification.
        /// </summary>
        public Guid VerifSigningEmployeeId
        {
            get
            {
                if (this.Contained.VerifSigningEmployeeId == null)
                {
                    return Guid.Empty;
                }
                else
                {
                    return this.Contained.VerifSigningEmployeeId.Value.Value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the type of the verification.
        /// </summary>
        /// <remarks>
        /// Note that while VorT has mortgage as a type, it is not used for 
        /// VORs.  Mortgage verifications are associated with the appropriate 
        /// mortgage liability instead.
        /// </remarks>
        public E_VorT VerifT
        {
            get
            {
                return this.GetEnumWithDefault<E_VorT>(this.Contained.VerifType, E_VorT.Other);
            }

            set
            {
                this.Contained.VerifType = value;
            }
        }

        /// <summary>
        /// Gets or sets the Zip code of the information to be verified.
        /// </summary>
        public string ZipFor
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.ZipFor);
            }

            set
            {
                this.Contained.ZipFor = this.StringParser.TryParseZipcode(value);
            }
        }

        /// <summary>
        /// Gets or sets the Zip the mortgage holder, credit union, or landlord.
        /// </summary>
        public string ZipTo
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.ZipTo);
            }

            set
            {
                this.Contained.ZipTo = this.StringParser.TryParseZipcode(value);
            }
        }

        /// <summary>
        /// Gets or sets the contained vor record data.
        /// </summary>
        private LendingQB.Core.Data.VorRecord Contained { get; set; }

        /// <summary>
        /// Gets or sets the collection that contains this shim.
        /// </summary>
        private IVerificationOfRentCollection Collection { get; set; }

        /// <summary>
        /// Gets or sets a callback method to be called at the end of the Flush method.
        /// </summary>
        private Action<DataSet> OnFlush { get; set; }

        /// <summary>
        /// Applies the signature.
        /// </summary>
        /// <param name="employeeId">The employee Id.</param>
        /// <param name="imgId">The image id of the signature.</param>
        public void ApplySignature(Guid employeeId, string imgId)
        {
            Guid imgGuid;
            bool ok = Guid.TryParse(imgId, out imgGuid);
            if (ok)
            {
                var empIdentifier = DataObjectIdentifier<DataObjectKind.Employee, Guid>.Create(employeeId);
                var imgIdentifier = DataObjectIdentifier<DataObjectKind.Signature, Guid>.Create(imgGuid);
                this.Contained.SetVerifSignature(empIdentifier, imgIdentifier);
            }
        }

        /// <summary>
        /// Clears the signature associated with the verification.
        /// </summary>
        public void ClearSignature()
        {
            this.Contained.ClearVerifSignature();
        }

        /// <summary>
        /// Set the flush function.
        /// </summary>
        /// <param name="onFlush">The action to perform on flush.</param>
        public void SetFlushFunction(Action<DataSet> onFlush)
        {
            this.OnFlush = onFlush;
        }

        /// <summary>
        /// Update the item.
        /// </summary>
        public override void Update()
        {
            base.Update();

            this.Collection.Update();

            if (this.OnFlush != null)
            {
                var vorcoll = (VorRecordCollectionShim)this.Collection;
                var ds = vorcoll.GetDataSet(false);
                this.OnFlush(ds);
            }
        }

        /// <summary>
        /// Create a shim implementation of the IVerificationOfRent interface.
        /// </summary>
        /// <param name="collectionShim">The shim for the collection that contains this entity shim.</param>
        /// <param name="contained">The contained VOR record data.</param>
        /// <param name="recordId">The identifier for this VOR record.</param>
        /// <param name="converter">Instance of LosConvert used to format types as strings.</param>
        /// <returns>A shim for the IVerificationOfRent interface.</returns>
        internal static IVerificationOfRent Create(
            IVerificationOfRentCollection collectionShim,
            LendingQB.Core.Data.VorRecord contained,
            DataObjectIdentifier<DataObjectKind.VorRecord, Guid> recordId,
            LosConvert converter)
        {
            return Create(collectionShim, contained, recordId, new FormatVorRecordShimAsString(converter), null, new ParseVorRecordShimFromString(converter));
        }

        /// <summary>
        /// Create a DataTable under the DataSet, and create the columns.
        /// </summary>
        /// <remarks>
        /// NOTE: This is only used by NUnit testing methods to instantiate a VorFields class so side-by-side comparisons can be done.
        /// </remarks>
        /// <param name="ds">The DataSet to which the table will be attached.</param>
        /// <returns>The DataTable with columns initialized.</returns>
        internal static DataTable CreateAndAddDataTable(DataSet ds)
        {
            var table = ds.Tables.Add(TableName);
            table.Columns.Add("RecordId");
            table.Columns.Add("LandlordCreditorName");
            table.Columns.Add("Description");
            table.Columns.Add("AddressFor");
            table.Columns.Add("CityFor");
            table.Columns.Add("StateFor");
            table.Columns.Add("ZipFor");
            table.Columns.Add("AddressTo");
            table.Columns.Add("CityTo");
            table.Columns.Add("StateTo");
            table.Columns.Add("ZipTo");
            table.Columns.Add("PhoneTo");
            table.Columns.Add("FaxTo");
            table.Columns.Add("AccountName");
            table.Columns.Add("VerifSentD");
            table.Columns.Add("VerifRecvD");
            table.Columns.Add("VerifExpD");
            table.Columns.Add("VerifReorderedD");
            table.Columns.Add("IsForBorrower");
            table.Columns.Add("VerifT");
            table.Columns.Add("Attention");
            table.Columns.Add("PrepD");
            table.Columns.Add("IsSeeAttachment");
            table.Columns.Add("VerifSigningEmployeeId");
            table.Columns.Add("VerifSignatureImgId");

            return table;
        }

        /// <summary>
        /// Create a DataRow, populate it, and add it to the input table.
        /// </summary>
        /// <remarks>
        /// NOTE: This is only used by NUnit testing methods to instantiate a VorFields class so side-by-side comparisons can be done.
        /// </remarks>
        /// <param name="table">The table to which the DataRow will be added.</param>
        /// <param name="data">The shim data.</param>
        /// <returns>The DataRow that was created.</returns>
        internal static DataRow CreateAndAddDataRow(DataTable table, IVerificationOfRent data)
        {
            var row = table.NewRow();
            row["RecordId"] = data.RecordId.ToString("D");
            row["LandlordCreditorName"] = data.LandlordCreditorName;
            row["Description"] = data.Description;
            row["AddressFor"] = data.AddressFor;
            row["CityFor"] = data.CityFor;
            row["StateFor"] = data.StateFor;
            row["ZipFor"] = data.ZipFor;
            row["AddressTo"] = data.AddressTo;
            row["CityTo"] = data.CityTo;
            row["StateTo"] = data.StateTo;
            row["ZipTo"] = data.ZipTo;
            row["PhoneTo"] = data.PhoneTo;
            row["FaxTo"] = data.FaxTo;
            row["AccountName"] = data.AccountName;
            row["VerifSentD"] = CollectionItemBaseShim.FormatCDateTime(data.VerifSentD);
            row["VerifRecvD"] = CollectionItemBaseShim.FormatCDateTime(data.VerifRecvD);
            row["VerifExpD"] = CollectionItemBaseShim.FormatCDateTime(data.VerifExpD);
            row["VerifReorderedD"] = CollectionItemBaseShim.FormatCDateTime(data.VerifReorderedD);
            row["IsForBorrower"] = string.Empty; // This is never used by the legacy code, it only appears in the XML schema.
            row["VerifT"] = ((int)data.VerifT).ToString();
            row["Attention"] = data.Attention;
            row["PrepD"] = CollectionItemBaseShim.FormatCDateTime(data.PrepD);
            row["IsSeeAttachment"] = data.IsSeeAttachment;
            row["VerifSigningEmployeeId"] = data.VerifSigningEmployeeId.ToString("D");
            row["VerifSignatureImgId"] = data.VerifSignatureImgId;

            table.Rows.Add(row);
            return row;
        }

        /// <summary>
        /// Create a shim implementation of the IVerificationOfRent interface.
        /// </summary>
        /// <param name="collectionShim">The shim for the collection that contains this entity shim.</param>
        /// <param name="contained">The contained VOR record data.</param>
        /// <param name="recordId">The identifier for this VOR record.</param>
        /// <param name="stringFormatter">Object that implements formatting semantic types into string values.</param>
        /// <param name="numericFormatter">Object that implements conversion of semantic types into numeric values.</param>
        /// <param name="stringParser">Object that implements parsing strings into semantic types.</param>
        /// <returns>A shim for the IVerificationOfRent interface.</returns>
        private static IVerificationOfRent Create(
            IVerificationOfRentCollection collectionShim,
            LendingQB.Core.Data.VorRecord contained,
            DataObjectIdentifier<DataObjectKind.VorRecord, Guid> recordId,
            IFormatAsString stringFormatter,
            IFormatAsNumeric numericFormatter,
            IParseFromString stringParser)
        {
            return new VorRecordShim(collectionShim, contained, recordId, stringFormatter ?? new FormatAsStringBase(), numericFormatter ?? new FormatAsNumericBase(), stringParser ?? new ParseFromStringBase());
        }
    }
}
