﻿namespace DataAccess
{
    using System;

    /// <summary>
    /// Class that doesn't permit special shim entity records.
    /// </summary>
    public class DisallowSpecialRecords : IDetermineSpecialEntity
    {
        /// <summary>
        /// Determine whether or not a particular record type is special.
        /// </summary>
        /// <param name="type">The record type.</param>
        /// <returns>True if the record type is determined to be a special record, false otherwise.</returns>
        public bool IsSpecial(Enum type)
        {
            return false;
        }

        /// <summary>
        /// Always return false.
        /// </summary>
        /// <param name="shim">The parameter is ignored.</param>
        /// <returns>False is always returned.</returns>
        public bool IsSpecial(ICollectionItemBase2 shim)
        {
            return false;
        }
    }
}
