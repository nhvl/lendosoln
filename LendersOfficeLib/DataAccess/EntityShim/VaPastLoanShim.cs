﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using LendersOffice.CreditReport;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Shim that presents an IVaPastLoan interface around a VaPreviousLoan entity.
    /// </summary>
    internal sealed class VaPastLoanShim : CollectionItemBase2Shim, IVaPastLoan
    {
        /// <summary>
        /// Name to use for the DataTable constructed to pass out data for binding to user controls.
        /// </summary>
        private const string TableName = "VaPastLXmlContent";

        /// <summary>
        /// Initializes a new instance of the <see cref="VaPastLoanShim"/> class.
        /// </summary>
        /// <param name="shimContainer">Reference to the container that holds all shims.</param>
        /// <param name="collectionShim">The shim for the collection that contains this entity shim.</param>
        /// <param name="contained">The VaPreviousLoan entity that holds the data.</param>
        /// <param name="recordId">The identifier for this VA previous loan.</param>
        /// <param name="stringFormatter">Object that controls how semantic types are serialized as strings.</param>
        /// <param name="numericFormatter">Object that controls how semantic types are converted to numeric values.</param>
        /// <param name="stringParser">Object that controls how strings are parsed into semantic types.</param>
        private VaPastLoanShim(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            LendingQB.Core.Data.VaPreviousLoan contained,
            DataObjectIdentifier<DataObjectKind.VaPreviousLoan, Guid> recordId,
            IFormatAsString stringFormatter,
            IFormatAsNumeric numericFormatter,
            IParseFromString stringParser)
            : base(shimContainer, collectionShim, recordId.Value, stringFormatter, numericFormatter, stringParser)
        {
            this.Contained = contained;
        }

        /// <summary>
        /// Gets or sets the City of the previous VA loan.
        /// </summary>
        public string City
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.City);
            }

            set
            {
                this.Contained.City = this.StringParser.TryParseCity(value);
            }
        }

        /// <summary>
        /// Gets or sets the City and State of the previous VA loan.
        /// </summary>
        public string CityState
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.CityState);
            }

            set
            {
                this.Contained.CityState = this.StringParser.TryParseCityState(value);
            }
        }

        /// <summary>
        /// Gets or sets the Date, in Month and Year format, of the previous VA loan.
        /// </summary>
        public string DateOfLoan
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.DateOfLoan);
            }

            set
            {
                this.Contained.DateOfLoan = this.StringParser.TryParseDescriptionField(value);
            }
        }

        /// <summary>
        /// Gets or sets whether the previous VA loan is still owned.
        /// </summary>
        public E_TriState IsStillOwned
        {
            get
            {
                return this.TriStateFromBooleanKleene(this.Contained.IsStillOwned);
            }

            set
            {
                this.Contained.IsStillOwned = this.BooleanKleeneFromTriState(value);
            }
        }

        /// <summary>
        /// Gets or sets a type for an enumeration that is used as a key.
        /// </summary>
        public Enum KeyType
        {
            get
            {
                return this.VaPastLT;
            }

            set
            {
                // no op
            }
        }

        /// <summary>
        /// Gets or sets the Loan date of the previous VA loan.
        /// </summary>
        public CDateTime LoanD
        {
            get
            {
                return this.UnzonedDateToCDateTime(this.Contained.LoanDate);
            }

            set
            {
                this.Contained.LoanDate = this.CDateTimeToUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the Loan date of the previous VA loan.
        /// </summary>
        public string LoanD_rep
        {
            get
            {
                return this.FormatUnzonedDate(this.Contained.LoanDate);
            }

            set
            {
                this.Contained.LoanDate = this.ParseUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the LoanType of the previous VA loan.
        /// </summary>
        public string LoanTypeDesc
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.LoanTypeDesc);
            }

            set
            {
                this.Contained.LoanTypeDesc = this.StringParser.TryParseDescriptionField(value);
            }
        }

        /// <summary>
        /// Gets or sets the Sold date of the previous VA loan.
        /// </summary>
        public CDateTime PropSoldD
        {
            get
            {
                return this.UnzonedDateToCDateTime(this.Contained.PropertySoldDate);
            }

            set
            {
                this.Contained.PropertySoldDate = this.CDateTimeToUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the Sold date of the previous VA loan.
        /// </summary>
        public string PropSoldD_rep
        {
            get
            {
                return this.FormatUnzonedDate(this.Contained.PropertySoldDate);
            }

            set
            {
                this.Contained.PropertySoldDate = this.ParseUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the Street Address of the previous VA loan.
        /// </summary>
        public string StAddr
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.StreetAddress);
            }

            set
            {
                this.Contained.StreetAddress = this.StringParser.TryParseStreetAddress(value);
            }
        }

        /// <summary>
        /// Gets or sets the State of the previous VA loan.
        /// </summary>
        public string State
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.State);
            }

            set
            {
                this.Contained.State = this.StringParser.TryParseUnitedStatesPostalState(value);
            }
        }

        /// <summary>
        /// Gets or sets the VA loan number of the previous VA loan.
        /// </summary>
        public string VaLoanNum
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.VaLoanNumber);
            }

            set
            {
                this.Contained.VaLoanNumber = this.StringParser.TryParseVaLoanNumber(value);
            }
        }

        /// <summary>
        /// Gets the VaPastLT of the previous VA loan.
        /// </summary>
        public E_VaPastLT VaPastLT
        {
            get
            {
                return E_VaPastLT.Regular;
            }
        }

        /// <summary>
        /// Gets or sets the Zip code of the previous VA loan.
        /// </summary>
        public string Zip
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.Zip);
            }

            set
            {
                this.Contained.Zip = this.StringParser.TryParseZipcode(value);
            }
        }

        /// <summary>
        /// Gets or sets the VA previous loan entity.
        /// </summary>
        private LendingQB.Core.Data.VaPreviousLoan Contained { get; set; }

        /// <summary>
        /// Create a shim implementation of the IVaPastLoan interface.
        /// </summary>
        /// <param name="shimContainer">Reference to the container that holds all shims.</param>
        /// <param name="collectionShim">The shim for the collection that contains this entity shim.</param>
        /// <param name="contained">The contained VA previous loan data.</param>
        /// <param name="recordId">The identifier for this VA previous loan.</param>
        /// <param name="converter">Instance of LosConvert used to format types as strings.</param>
        /// <returns>A shim for the IVaPastLoan interface.</returns>
        internal static IVaPastLoan Create(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            LendingQB.Core.Data.VaPreviousLoan contained,
            DataObjectIdentifier<DataObjectKind.VaPreviousLoan, Guid> recordId,
            LosConvert converter)
        {
            return Create(shimContainer, collectionShim, contained, recordId, new FormatEntityShimAsString(converter), null, new ParseEntityShimFromString(converter));
        }

        /// <summary>
        /// Create a DataTable under the DataSet, and create the columns.
        /// </summary>
        /// <param name="ds">The DataSet to which the table will be attached.</param>
        /// <returns>The DataTable with columns initialized.</returns>
        internal static DataTable CreateAndAddDataTable(DataSet ds)
        {
            var table = ds.Tables.Add(TableName);
            table.Columns.Add("RecordId");
            table.Columns.Add("LoanTypeDesc");
            table.Columns.Add("StAddr");
            table.Columns.Add("City");
            table.Columns.Add("State");
            table.Columns.Add("Zip");
            table.Columns.Add("LoanD");
            table.Columns.Add("IsStillOwned");
            table.Columns.Add("PropSoldD");
            table.Columns.Add("VaLoanNum");
            table.Columns.Add("OrderRankValue");
            table.Columns.Add("DateOfLoan");
            table.Columns.Add("CityState");

            return table;
        }

        /// <summary>
        /// Create a DataRow, populate it, and add it to the input table.
        /// </summary>
        /// <param name="table">The table to which the DataRow will be added.</param>
        /// <returns>The DataRow that was created.</returns>
        internal DataRow CreateAndAddDataRow(DataTable table)
        {
            var row = table.NewRow();
            row["LoanTypeDesc"] = this.LoanTypeDesc;
            row["StAddr"] = this.StAddr;
            row["City"] = this.City;
            row["State"] = this.State;
            row["Zip"] = this.Zip;
            row["LoanD"] = this.DataRowFormattedUnzonedDate(this.LoanD_rep);
            row["IsStillOwned"] = ((int)this.IsStillOwned).ToString();
            row["PropSoldD"] = this.DataRowFormattedUnzonedDate(this.PropSoldD_rep);
            row["VaLoanNum"] = this.VaLoanNum;
            row["DateOfLoan"] = this.DateOfLoan;
            row["CityState"] = this.CityState;

            this.DataRow = row;
            table.Rows.Add(row);
            return row;
        }

        /// <summary>
        /// Create a shim implementation of the IVaPastLoan interface.
        /// </summary>
        /// <param name="shimContainer">Reference to the container that holds all shims.</param>
        /// <param name="collectionShim">The shim for the collection that contains this entity shim.</param>
        /// <param name="contained">The contained VA previous loan data.</param>
        /// <param name="recordId">The identifier for this VA previous loan.</param>
        /// <param name="stringFormatter">Object that implements formatting semantic types into string values.</param>
        /// <param name="numericFormatter">Object that implements conversion of semantic types into numeric values.</param>
        /// <param name="stringParser">Object that implements parsing strings into semantic types.</param>
        /// <returns>A shim for the IVaPastLoan interface.</returns>
        private static IVaPastLoan Create(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            LendingQB.Core.Data.VaPreviousLoan contained,
            DataObjectIdentifier<DataObjectKind.VaPreviousLoan, Guid> recordId,
            IFormatAsString stringFormatter,
            IFormatAsNumeric numericFormatter,
            IParseFromString stringParser)
        {
            return new VaPastLoanShim(shimContainer, collectionShim, contained, recordId, stringFormatter ?? new FormatAsStringBase(), numericFormatter ?? new FormatAsNumericBase(), stringParser ?? new ParseFromStringBase());
        }

        /// <summary>
        /// Format an UnzonedDate to a string representation.
        /// </summary>
        /// <param name="uzd">The UnzonedDate to format.</param>
        /// <returns>A string representation of the UnzonedDate.</returns>
        private string FormatUnzonedDate(UnzonedDate? uzd)
        {
            if (this.StringFormatter is IContainLosConvert)
            {
                var provider = (IContainLosConvert)this.StringFormatter;
                var converter = provider.Converter;

                var cdate = this.UnzonedDateToCDateTime(uzd);
                return cdate.ToString(converter);
            }
            else if (uzd == null)
            {
                return CDateTime.InvalidDateTime.ToString();
            }
            else
            {
                return uzd.Value.ToString();
            }
        }

        /// <summary>
        /// Parse a string representation of a date to an UnzonedDate instance.
        /// </summary>
        /// <param name="value">A string representation of a date.</param>
        /// <returns>An instance of an UnzonedDate, or null.</returns>
        private UnzonedDate? ParseUnzonedDate(string value)
        {
            if (this.StringFormatter is IContainLosConvert)
            {
                var provider = (IContainLosConvert)this.StringFormatter;
                var converter = provider.Converter;

                var cdate = CDateTime.Create(value, converter);
                return cdate.IsValid ? UnzonedDate.Create(cdate.DateTimeForComputation) : null;
            }
            else
            {
                return this.StringParser.TryParseUnzonedDate(value);
            }
        }
    }
}
