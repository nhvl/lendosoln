﻿namespace DataAccess
{
    using System;

    /// <summary>
    /// Class that checks if an asset type is special.
    /// </summary>
    public class AssetDetermineSpecialEntity : IDetermineSpecialEntity
    {
        /// <summary>
        /// Determine whether or not a particular record type is special.
        /// </summary>
        /// <param name="type">The record type.</param>
        /// <returns>True if the record type is determined to be a special record, false otherwise.</returns>
        public bool IsSpecial(Enum type)
        {
            var assetType = (E_AssetT)type;
            return CAssetFields.IsTypeOfSpecialRecord(assetType);
        }

        /// <summary>
        /// Determine if the item is a special type.
        /// </summary>
        /// <param name="shim">The shimmed asset.</param>
        /// <returns>True if the asset is special, otherwise false.</returns>
        public bool IsSpecial(ICollectionItemBase2 shim)
        {
            return !((IAsset)shim).IsRegularType;
        }
    }
}
