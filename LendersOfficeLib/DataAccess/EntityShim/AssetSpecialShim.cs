﻿namespace DataAccess
{
    using System;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Shim for an asset that is a special asset.
    /// </summary>
    public abstract class AssetSpecialShim : AssetShim, IAssetSpecial
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AssetSpecialShim"/> class.
        /// </summary>
        /// <param name="shimContainer">Reference to the container that holds all shims.</param>
        /// <param name="collectionShim">The shim for the collection that contains this entity shim.</param>
        /// <param name="contained">The Asset entity that holds the data.</param>
        /// <param name="ownershipManager">The ownership manager for the collection records.</param>
        /// <param name="recordId">The identifier for this asset.</param>
        /// <param name="stringFormatter">Object that controls how semantic types are serialized as strings.</param>
        /// <param name="numericFormatter">Object that controls how semantic types are converted to numeric values.</param>
        /// <param name="stringParser">Object that controls how strings are parsed into semantic types.</param>
        protected AssetSpecialShim(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            LendingQB.Core.Data.Asset contained,
            IShimOwnershipManager<Guid> ownershipManager,
            DataObjectIdentifier<DataObjectKind.Asset, Guid> recordId,
            IFormatAsString stringFormatter,
            IFormatAsNumeric numericFormatter,
            IParseFromString stringParser)
            : base(shimContainer, collectionShim, contained, ownershipManager, recordId, stringFormatter, numericFormatter, stringParser)
        {
        }

        /// <summary>
        /// Gets or sets the type of the special asset.
        /// </summary>
        /// <value>The type of the special asset.</value>
        E_AssetSpecialT IAssetSpecial.AssetT
        {
            get
            {
                return this.Contained.AssetType.HasValue ?
                    (E_AssetSpecialT)this.Contained.AssetType
                    : E_AssetSpecialT.OtherIlliquidAsset;
            }

            set
            {
                this.Contained.AssetType = (E_AssetT)value;
            }
        }

        /// <summary>
        /// Determines whether the asset is credited at closing.  If credit at closing,
        /// assets will not be included in the asset totals.  Instead, they'll be added
        /// to the adjustments.
        /// </summary>
        public override bool IsCreditAtClosing
        {
            get
            {
                return false;
            }
        }
    }
}
