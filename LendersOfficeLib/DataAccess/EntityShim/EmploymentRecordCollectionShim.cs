﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using LendersOffice.Common;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;

    using InterfaceDefs = LendingQB.Core;
    using ShimTarget = LendingQB.Core.Data;

    /// <summary>
    /// Shim for an employment record collection.
    /// </summary>
    public class EmploymentRecordCollectionShim : RecordCollectionShim<DataObjectKind.EmploymentRecord, ShimTarget.EmploymentRecord>, IEmpCollection
    {
        /// <summary>
        /// The defaults provider for employment records.
        /// </summary>
        private IEmploymentRecordDefaultsProvider defaultsProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmploymentRecordCollectionShim"/> class.
        /// </summary>
        /// <param name="applicationData">The application data needed to by some of the shim properties.</param>
        /// <param name="borrowerManagement">Reference to borrower management.</param>
        /// <param name="isBorrower">The employment records are for the borrower if true, else they are for the coborrower.</param>
        /// <param name="shimContainer">Reference to the container that holds all shims.</param>
        /// <param name="defaultsProvider">The employment record defaults provider.</param>
        /// <param name="converter">An instance of LosConvert used for formatting the data.</param>
        public EmploymentRecordCollectionShim(
            IShimAppDataProvider applicationData,
            ILoanLqbCollectionBorrowerManagement borrowerManagement,
            bool isBorrower,
            IShimContainer shimContainer,
            IEmploymentRecordDefaultsProvider defaultsProvider,
            LosConvert converter)
            : base(shimContainer, DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(applicationData.aAppId), new EmploymentSpecialRecords(), converter)
        {
            this.ApplicationData = applicationData;
            this.BorrowerManagement = borrowerManagement;
            this.IsBorrower = isBorrower;
            this.ConsumerId = (isBorrower ? applicationData.aBConsumerId : applicationData.aCConsumerId).ToIdentifier<DataObjectKind.Consumer>();
            this.defaultsProvider = defaultsProvider;

            var ds = this.GetDataSet();
            var table = EmploymentRecordShim.CreateAndAddDataTable(ds);
            this.RebuildShimData();
        }

        /// <summary>
        /// Gets the extra previous employments.
        /// </summary>
        public ISubcollection ExtraPreviousEmployments
        {
            get
            {
                var mainList = this.GetSubcollection(true, E_EmpGroupT.Regular);
                if (mainList.Count > 2)
                {
                    var list = new ICollectionItemBase[mainList.Count - 2];
                    for (int i = 0; i < mainList.Count - 2; i++)
                    {
                        list[i] = mainList.GetRecordAt(i + 2);
                    }

                    return CXmlRecordSubcollection.Create(list);
                }
                else
                {
                    return CXmlRecordSubcollection.Create(new ICollectionItemBase[0]);
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether there are more than two regular employment records.
        /// </summary>
        public bool HasMoreThan2PreviousEmployments
        {
            get
            {
                return this.CountRegular > 2;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the employment history is valid.
        /// </summary>
        public bool IsEmploymentHistoryValid
        {
            get
            {
                var history = this.GetSubcollection(true, E_EmpGroupT.All);
                return LendersOffice.CalculatedFields.EmploymentRecord.IsEmploymentHistoryValid(history);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the employer business phone is "locked".
        /// </summary>
        public bool IsEmplrBusPhoneLckd
        {
            get
            {
                return this.IsBorrower ? this.ApplicationData.aBEmplrBusPhoneLckd : this.ApplicationData.aCEmplrBusPhoneLckd;
            }
        }

        /// <summary>
        /// Gets the business phone of the associated borrower.
        /// </summary>
        public string PersonBusPhone
        {
            get
            {
                return this.IsBorrower ? this.ApplicationData.aBBusPhone : this.ApplicationData.aCBusPhone;
            }
        }

        /// <summary>
        /// Gets the where clause for the sorted view.
        /// </summary>
        protected override string SortedViewRowFilter
        {
            get
            {
                return "EmplmtStat NOT IN ('0')"; // Only previous employment records
            }
        }

        /// <summary>
        /// Gets the ordering for the contained collection.
        /// </summary>
        protected override InterfaceDefs.IOrderedIdentifierCollection<DataObjectKind.EmploymentRecord, Guid> Order
        {
            get
            {
                return this.ShimContainer.GetOrderForConsumer<DataObjectKind.EmploymentRecord>(this.ConsumerId);
            }
        }

        /// <summary>
        /// Gets or sets the application data associated with this employment record shim.
        /// </summary>
        private IShimAppDataProvider ApplicationData { get; set; }

        /// <summary>
        /// Gets the borrower management implementation.
        /// </summary>
        private ILoanLqbCollectionBorrowerManagement BorrowerManagement { get; }

        /// <summary>
        /// Gets a value indicating whether this collection is for the borrower rather than the coborrower.
        /// </summary>
        private bool IsBorrower { get; }

        /// <summary>
        /// Gets the consumer id for the employment records.
        /// </summary>
        private DataObjectIdentifier<DataObjectKind.Consumer, Guid> ConsumerId { get; }

        /// <summary>
        /// Adds a regular employment record.
        /// </summary>
        /// <returns>The added record.</returns>
        public IRegularEmploymentRecord AddRegularRecord()
        {
            var item = new ShimTarget.EmploymentRecord(this.defaultsProvider);
            item.EmploymentStatusType = E_EmplmtStat.Previous;
            var id = this.AddEntity(item);
            this.RebuildShimData();

            return this.GetRegRecordOf(id.Value);
        }

        /// <summary>
        /// Gets the length of the longest continuous employment gap (in months, rounding down)
        /// within the period specified in the parameter.
        /// </summary>
        /// <param name="months">The number of months to go back from the current date.</param>
        /// <returns>
        /// The length of the longest continuous employment gap in months, rounding down.
        /// </returns>
        public int GetMaxEmploymentGapWithin(int months)
        {
            var jobEntries = this.GetSubcollection(true, E_EmpGroupT.All);
            return LendersOffice.CalculatedFields.EmploymentRecord.GetMaxEmploymentGapWithin(months, jobEntries);
        }

        /// <summary>
        /// Gets the primary employment record.
        /// </summary>
        /// <param name="forceCreate">A value indicating whether the record should be created if it does not exist.</param>
        /// <returns>The primary employment if it exists or if it is being force-created. Otherwise, null.</returns>
        public IPrimaryEmploymentRecord GetPrimaryEmp(bool forceCreate)
        {
            if (this.CountSpecial == 0)
            {
                if (forceCreate)
                {
                    var appIdentifier = this.AppId.ToIdentifier<DataObjectKind.LegacyApplication>();
                    if (!this.IsBorrower && !this.BorrowerManagement.GetCoborrowerActiveFlag(appIdentifier))
                    {
                        this.BorrowerManagement.AddCoborrowerToLegacyApplication(appIdentifier);
                    }

                    var item = new ShimTarget.EmploymentRecord(this.defaultsProvider);
                    item.EmploymentStatusType = E_EmplmtStat.Current;
                    item.IsPrimary = true;

                    var id = this.ShimContainer.Add(this.ConsumerId, item);
                    this.RebuildShimData();
                }
                else
                {
                    return null;
                }
            }

            return (IPrimaryEmploymentRecord)this.SpecialRecords[0];
        }

        /// <summary>
        /// Gets the regular record associated with the given id.
        /// </summary>
        /// <param name="recordId">The record id.</param>
        /// <returns>The regular record associated with the given id.</returns>
        public IRegularEmploymentRecord GetRegRecordOf(Guid recordId)
        {
            return (IRegularEmploymentRecord)this.GetRegularRecordOf(recordId);
        }

        /// <summary>
        /// Gets the regular employment record at the specified index.
        /// </summary>
        /// <param name="pos">The index.</param>
        /// <returns>The regular employment record.</returns>
        public IRegularEmploymentRecord GetRegularRecordAt(int pos)
        {
            return (IRegularEmploymentRecord)this.GetRegularItemAt(pos);
        }

        /// <summary>
        /// Validates the employment history dates.
        /// </summary>
        /// <param name="userMessage">The error message to append errors to.</param>
        /// <param name="borrowerName">The borrower's name.</param>
        /// <returns>True if the employment history dates are valid. Otherwise, false.</returns>
        public bool ValidateEmploymentHistoryDates(StringBuilder userMessage, string borrowerName)
        {
            var history = this.GetSubcollection(true, E_EmpGroupT.All);
            return LendersOffice.CalculatedFields.EmploymentRecord.ValidateEmploymentHistoryDates(userMessage, borrowerName, history);
        }

        /// <summary>
        /// Gets the specified sub-collection.
        /// </summary>
        /// <param name="inclusiveGroup">A value indicating whether the specified groups should be included or excluded.</param>
        /// <param name="group">An enum value flagged with the desired groups.</param>
        /// <returns>The specified sub-collection.</returns>
        public ISubcollection GetSubcollection(bool inclusiveGroup, E_EmpGroupT group)
        {
            return this.GetSubcollectionCore(inclusiveGroup, group);
        }

        /// <summary>
        /// Create and add a new record of the specified type.
        /// </summary>
        /// <param name="recordT">The type of record to create.</param>
        /// <returns>The new record.</returns>
        public ICollectionItemBase2 AddRecord(Enum recordT)
        {
            var type = (E_EmplmtStat)recordT;

            if (type == E_EmplmtStat.Current)
            {
                if (this.CountSpecial == 0)
                {
                    return this.GetPrimaryEmp(true);
                }
                else
                {
                    throw new CBaseException(ErrorMessages.Generic, "Cannot add any more special liability record of the requested type = " + type.ToString());
                }
            }
            else
            {
                return this.AddRegularRecord();
            }
        }

        /// <summary>
        /// Create and add a new record.
        /// </summary>
        /// <returns>The new record.</returns>
        ICollectionItemBase2 IRecordCollection.AddRegularRecord()
        {
            return this.AddRegularRecord();
        }

        /// <summary>
        /// Create and add a new record at the specified position.
        /// </summary>
        /// <param name="pos">The position into which the record should be inserted.</param>
        /// <returns>The new record.</returns>
        public ICollectionItemBase2 AddRegularRecordAt(int pos)
        {
            var item = new ShimTarget.EmploymentRecord(this.defaultsProvider);
            item.EmploymentStatusType = E_EmplmtStat.Previous;
            var id = this.InsertRegularRecordAt(item, pos);
            return this.GetRegRecordOf(id.Value);
        }

        /// <summary>
        /// If there is no record with the indicated identifier, create one.  Return the record with the indicated identifier.
        /// </summary>
        /// <param name="recordId">The identifier that the returned record should have.</param>
        /// <returns>The record with the specified identifier.</returns>
        public ICollectionItemBase2 EnsureRegularRecordOf(Guid recordId)
        {
            var shim = this.TryGetRegularRecordOf(recordId);
            if (shim == null)
            {
                var identifier = DataObjectIdentifier<DataObjectKind.EmploymentRecord, Guid>.Create(recordId);
                var item = new ShimTarget.EmploymentRecord(this.defaultsProvider);
                item.EmploymentStatusType = E_EmplmtStat.Previous;
                this.AddEntity(identifier, item);
                this.RebuildShimData();

                shim = this.GetRegRecordOf(recordId);
            }

            return shim;
        }

        /// <summary>
        /// Retrieve the regular record at the specified position.
        /// </summary>
        /// <param name="pos">The position.</param>
        /// <returns>The record at the specified position, or throw an out of bounds exception.</returns>
        ICollectionItemBase2 IRecordCollection.GetRegularRecordAt(int pos)
        {
            return this.GetRegularItemAt(pos);
        }

        /// <summary>
        /// Retrieve the special record at the specified position.
        /// </summary>
        /// <param name="i">The position.</param>
        /// <returns>The record at the specified position, or throw an out of bounds exception.</returns>
        public ICollectionItemBase2 GetSpecialRecordAt(int i)
        {
            return this.GetSpecialItemAt(i);
        }

        /// <summary>
        /// Gets the regular record associated with the given id.
        /// </summary>
        /// <param name="recordId">The record id.</param>
        /// <returns>The regular record associated with the given id.</returns>
        ICollectionItemBase2 IRecordCollection.GetRegRecordOf(Guid recordId)
        {
            return this.GetRegularRecordOf(recordId);
        }

        /// <summary>
        /// Build up the shim data.
        /// </summary>
        protected override void BuildShimData()
        {
            var ds = this.GetDataSet();
            var table = ds.Tables[0];
            foreach (var keyValue in this.GetAllCollectionEntities())
            {
                var type = keyValue.Value.EmploymentStatusType;
                if (type == E_EmplmtStat.Current && this.CountSpecial > 0)
                {
                    throw new CBaseException(ErrorMessages.Generic, "Cannot add more than one primary employment record");
                }

                // Note that the key value pairs should be returned in the correct order because
                // the collection should be pre-ordered for us.
                var shim = EmploymentRecordShim.Create(this.ShimContainer, this, keyValue.Value, keyValue.Key, this.Converter);

                if (this.SpecialHandler.IsSpecial(shim))
                {
                    this.SpecialRecords.Add(shim);
                }
                else
                {
                    this.RegularRecords.Add(shim);
                }

                var er = (EmploymentRecordShim)shim;
                er.CreateAndAddDataRow(table);
            }
        }

        /// <summary>
        /// Adds the given record to the underlying collection via the aggregate.
        /// </summary>
        /// <param name="value">The record to add.</param>
        /// <returns>The identifier of the new record.</returns>
        protected override DataObjectIdentifier<DataObjectKind.EmploymentRecord, Guid> AddEntity(ShimTarget.EmploymentRecord value)
        {
            return this.ShimContainer.Add(this.ConsumerId, value);
        }

        /// <summary>
        /// Adds the given id and record to the underlying collection via the aggregate.
        /// </summary>
        /// <param name="recordId">The id to use.</param>
        /// <param name="value">The record to add.</param>
        protected override void AddEntity(DataObjectIdentifier<DataObjectKind.EmploymentRecord, Guid> recordId, ShimTarget.EmploymentRecord value)
        {
            this.ShimContainer.Add(this.ConsumerId, recordId, value);
        }

        /// <summary>
        /// Removes a record from the underlying collection via the aggregate.
        /// </summary>
        /// <param name="recordId">The record id.</param>
        protected override void RemoveEntity(DataObjectIdentifier<DataObjectKind.EmploymentRecord, Guid> recordId)
        {
            this.ShimContainer.Remove(recordId);
        }

        /// <summary>
        /// Gets the collection records relevant to the shimmed application.
        /// </summary>
        /// <returns>The collection records relevant to the shimmed application.</returns>
        protected override IEnumerable<KeyValuePair<DataObjectIdentifier<DataObjectKind.EmploymentRecord, Guid>, ShimTarget.EmploymentRecord>> GetAllCollectionEntities()
        {
            return this.ShimContainer.GetEmploymentRecordsForConsumer(this.ConsumerId);
        }
    }
}
