﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using LendersOffice.CreditReport;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Shim that presents an IPublicRecord interface around a PublicRecord entity.
    /// </summary>
    public sealed class RealPropertyShim : CollectionItemBase2Shim, IRealEstateOwned, IRememberBorrowers
    {
        /// <summary>
        /// Name to use for the DataTable constructed to pass out data for binding to user controls.
        /// </summary>
        private const string TableName = "ReXmlContent";

        /// <summary>
        /// The ownership manager.
        /// </summary>
        private IShimOwnershipManager<Guid> ownershipManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="RealPropertyShim"/> class.
        /// </summary>
        /// <param name="shimContainer">Reference to the container that holds all shims.</param>
        /// <param name="collectionShim">The shim for the collection that contains this entity shim.</param>
        /// <param name="contained">The RealProperty entity that holds the data.</param>
        /// <param name="ownershipManager">The ownership manager for the collection records.</param>
        /// <param name="recordId">The identifier for this public record.</param>
        /// <param name="stringFormatter">Object that controls how semantic types are serialized as strings.</param>
        /// <param name="numericFormatter">Object that controls how semantic types are converted to numeric values.</param>
        /// <param name="stringParser">Object that controls how strings are parsed into semantic types.</param>
        private RealPropertyShim(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            LendingQB.Core.Data.RealProperty contained,
            IShimOwnershipManager<Guid> ownershipManager,
            DataObjectIdentifier<DataObjectKind.RealProperty, Guid> recordId,
            IFormatAsString stringFormatter,
            IFormatAsNumeric numericFormatter,
            IParseFromString stringParser)
            : base(shimContainer, collectionShim, recordId.Value, stringFormatter, numericFormatter, stringParser)
        {
            this.ownershipManager = ownershipManager;

            this.Contained = contained;
            this.OriginalStatus = this.Stat;
        }

        /// <summary>
        /// Gets or sets the street address of the property.
        /// </summary>
        /// <value>The street address of the property.</value>
        public string Addr
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.StreetAddress);
            }

            set
            {
                this.Contained.StreetAddress = this.StringParser.TryParseStreetAddress(value);
            }
        }

        /// <summary>
        /// Gets or sets the city in which the property is located.
        /// </summary>
        /// <value>The city in which the property is located.</value>
        public string City
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.City);
            }

            set
            {
                this.Contained.City = this.StringParser.TryParseCity(value);
            }
        }

        /// <summary>
        /// Gets or sets the gross rent earned from the property.
        /// </summary>
        /// <value>The gross rent earned from the property.</value>
        public decimal GrossRentI
        {
            get
            {
                if (this.StatT != E_ReoStatusT.Rental)
                {
                    return 0;
                }

                return this.NumericFormatter.Format(this.Contained.GrossRentInc);
            }

            set
            {
                this.Contained.GrossRentInc = Money.Create(value);
            }
        }

        /// <summary>
        /// Gets or sets the gross rent earned from the property in string format.
        /// </summary>
        /// <value>The gross rent earned from the property in string format.</value>
        public string GrossRentI_rep
        {
            get
            {
                // Use the shim's GrossRentI because it contains a calculation.
                return this.StringFormatter.Format(Money.Create(this.GrossRentI));
            }

            set
            {
                this.Contained.GrossRentInc = this.StringParser.TryParseMoney(value);
            }
        }

        /// <summary>
        /// Gets or sets the expense incurred from this property.
        /// </summary>
        /// <value>The expense incurred from this property.</value>
        public decimal HExp
        {
            get
            {
                return this.NumericFormatter.Format(this.Contained.HExp);
            }

            set
            {
                this.Contained.HExp = Money.Create(value);
            }
        }

        /// <summary>
        /// Gets or sets the expense incurred from this property in string format.
        /// </summary>
        /// <value>The expense incurred from this property in string format.</value>
        public string HExp_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.HExp);
            }

            set
            {
                this.Contained.HExp = this.StringParser.TryParseMoney(value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether some of the 'required' fields are not yet populated.
        /// </summary>
        /// <value>A value indicating whether some of the 'required' fields are not yet populated.</value>
        public bool IsEmptyCreated
        {
            get
            {
                return this.BooleanFromNullable(this.Contained.IsEmptyCreated);
            }

            set
            {
                this.Contained.IsEmptyCreated = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this property should be included in the income calculation.
        /// </summary>
        /// <value>A value indicating whether this property should be included in the income calculation.</value>
        public bool IsForceCalcNetRentalI
        {
            get
            {
                if (this.IsSubjectProp)
                {
                    return false;
                }

                return this.BooleanFromNullable(this.Contained.IsForceCalcNetRentalInc);
            }

            set
            {
                this.Contained.IsForceCalcNetRentalInc = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the IsForceCalNetRentalI value is visible to the GUI.
        /// </summary>
        /// <value>A value indicating whether the IsForceCalNetRentalI value is visible to the GUI.</value>
        public bool IsForceCalcNetRentalIVisible
        {
            get
            {
                return LendersOffice.CalculatedFields.RealProperty.IsForceCalcNetRentalIVisible(this.Stat);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this property is the primary residence of one or more borrowers.
        /// </summary>
        /// <value>A value indicating whether this property is the primary residence of one or more borrowers.</value>
        public bool IsPrimaryResidence
        {
            get
            {
                return this.BooleanFromNullable(this.Contained.IsPrimaryResidence);
            }

            set
            {
                this.Contained.IsPrimaryResidence = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the status of the property has been updated.
        /// </summary>
        /// <remarks>
        /// The behavior here differs from the class ReFields in that this is correct and the other method was incorrect.
        /// </remarks>
        /// <value>A value indicating whether the status of the property has been updated.</value>
        public bool IsStatusUpdated
        {
            get
            {
                return this.Stat != this.OriginalStatus;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the property is the subject property of the loan application.
        /// </summary>
        /// <value>A value indicating whether the property is the subject property of the loan application.</value>
        public bool IsSubjectProp
        {
            get
            {
                return this.BooleanFromNullable(this.Contained.IsSubjectProp);
            }

            set
            {
                this.Contained.IsSubjectProp = value;
            }
        }

        /// <summary>
        /// Gets or sets a type for an enumeration that is used as a key.
        /// </summary>
        public Enum KeyType
        {
            get
            {
                return this.StatT;
            }

            set
            {
                this.StatT = (E_ReoStatusT)value;
            }
        }

        /// <summary>
        /// Gets the collection of liabilities that correspond to this property.
        /// </summary>
        /// <value>The collection of liabilities that correspond to this property.</value>
        public List<CReFields.LiaDisplay> LinkedLia
        {
            get
            {
                return this.ShimContainer.GetLiabilitesLinkedToRealProperty(this.RecordId);
            }
        }

        /// <summary>
        /// Gets or sets the mortgage amount for this property.
        /// </summary>
        /// <value>The mortgage amount for this property.</value>
        public decimal MAmt
        {
            get
            {
                return this.NumericFormatter.Format(this.Contained.MtgAmt);
            }

            set
            {
                this.Contained.MtgAmt = Money.Create(value);
            }
        }

        /// <summary>
        /// Gets or sets the mortgage amount for this property in string format.
        /// </summary>
        /// <value>The mortgage amount for this property in string format.</value>
        public string MAmt_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.MtgAmt);
            }

            set
            {
                this.Contained.MtgAmt = this.StringParser.TryParseMoney(value);
            }
        }

        /// <summary>
        /// Gets or sets the mortgage payment for this property.
        /// </summary>
        /// <value>The mortgage payment for this property.</value>
        public decimal MPmt
        {
            get
            {
                return this.NumericFormatter.Format(this.Contained.MtgPmt);
            }

            set
            {
                this.Contained.MtgPmt = Money.Create(value);
            }
        }

        /// <summary>
        /// Gets or sets the mortgage payment for this property in string format.
        /// </summary>
        /// <value>The mortgage payment for this property in string format.</value>
        public string MPmt_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.MtgPmt);
            }

            set
            {
                this.Contained.MtgPmt = this.StringParser.TryParseMoney(value);
            }
        }

        /// <summary>
        /// Gets or sets the net rental income earned from this property.
        /// </summary>
        /// <value>The net rental income earned from this property.</value>
        public decimal NetRentI
        {
            get
            {
                return LendersOffice.CalculatedFields.RealProperty.NetRentInc(
                    this.NetRentILckd,
                    this.NumericFormatter.Format(this.Contained.NetRentIncData),
                    this.StatT,
                    this.IsForceCalcNetRentalI,
                    this.OccR,
                    this.GrossRentI,
                    this.HExp,
                    this.MPmt);
            }

            set
            {
                this.Contained.NetRentIncData = Money.Create(value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the net rental income is locked and cannot be edited.
        /// </summary>
        /// <value>A value indicating whether the net rental income is locked and cannot be edited.</value>
        public bool NetRentILckd
        {
            get
            {
                return this.BooleanFromNullable(this.Contained.NetRentIncLocked);
            }

            set
            {
                this.Contained.NetRentIncLocked = value;
            }
        }

        /// <summary>
        /// Gets or sets the net rental income earned from this property in string format.
        /// </summary>
        /// <value>The net rental income earned from this property in string format.</value>
        public string NetRentI_rep
        {
            get
            {
                // Use shim NetRentI because it has a calculation.
                return this.StringFormatter.Format(Money.Create(this.NetRentI));
            }

            set
            {
                this.Contained.NetRentIncData = this.StringParser.TryParseMoney(value);
            }
        }

        /// <summary>
        /// Gets the net value of this property.
        /// </summary>
        /// <value>The net value of this property.</value>
        public decimal NetVal
        {
            get
            {
                var netvalue = LendersOffice.CalculatedFields.RealProperty.NetValue(this.Contained.MarketValue, this.Contained.MtgAmt);
                return this.NumericFormatter.Format(netvalue);
            }
        }

        /// <summary>
        /// Gets the net value of this property in string format.
        /// </summary>
        /// <value>The net value of this property in string format.</value>
        public string NetValue_rep
        {
            get
            {
                var netvalue = LendersOffice.CalculatedFields.RealProperty.NetValue(this.Contained.MarketValue, this.Contained.MtgAmt);
                return this.StringFormatter.Format(netvalue);
            }
        }

        /// <summary>
        /// Gets or sets the rough percentage of units that are currently occupied.
        /// </summary>
        /// <value>The rough percentage of units that are currently occupied.</value>
        public int OccR
        {
            get
            {
                if (this.StatT != E_ReoStatusT.Rental)
                {
                    return 0;
                }

                var asdecimal = this.NumericFormatter.Format(this.Contained.OccR);
                return Convert.ToInt32(asdecimal);
            }

            set
            {
                this.Contained.OccR = Percentage.Create(value);
            }
        }

        /// <summary>
        /// Gets or sets the rough percentage of units that are currently occupied in string format.
        /// </summary>
        /// <value>The rough percentage of units that are currently occupied in string format.</value>
        public string OccR_rep
        {
            get
            {
                // Use the shim's OccR because it contains a calculation.
                return this.StringFormatter.Format(Percentage.Create(this.OccR));
            }

            set
            {
                this.Contained.OccR = this.StringParser.TryParsePercentage(value);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the occupancy rate is read only or whether it can be edited.
        /// </summary>
        /// <value>A value indicating whether the occupancy rate is read only or whether it can be edited.</value>
        public bool OccR_repReadOnly
        {
            get
            {
                return LendersOffice.CalculatedFields.RealProperty.OccR_repReadOnly(this.StatT);
            }
        }

        /// <summary>
        /// Gets or sets the ownership of the property.
        /// </summary>
        /// <value>The ownership of the property.</value>
        public E_ReOwnerT ReOwnerT
        {
            get
            {
                return (E_ReOwnerT)this.ownershipManager.GetOwnership(this.RecordId);
            }

            set
            {
                this.ownershipManager.SetOwnership(this.RecordId, (Ownership)value);
            }
        }

        /// <summary>
        /// Gets or sets the status code for the property.
        /// </summary>
        /// <value>The status code for the property.</value>
        public string Stat
        {
            get
            {
                return LendersOffice.CalculatedFields.RealProperty.StatusCode(this.StatT);
            }

            set
            {
                this.StatT = LendersOffice.CalculatedFields.RealProperty.StatusFromCode(value);
            }
        }

        /// <summary>
        /// Gets or sets the state in which the property is located.
        /// </summary>
        /// <value>The state in which the property is located.</value>
        public string State
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.State);
            }

            set
            {
                this.Contained.State = this.StringParser.TryParseUnitedStatesPostalState(value);
            }
        }

        /// <summary>
        /// Gets or sets the status for the property, closely assocated with the status code Stat.
        /// </summary>
        /// <value>The status for the property, closely assocated with the status code Stat.</value>
        public E_ReoStatusT StatT
        {
            get
            {
                if (this.Contained.Status == null)
                {
                    return E_ReoStatusT.Residence;
                }
                else
                {
                    return this.Contained.Status.Value;
                }
            }

            set
            {
                this.Contained.Status = value;
            }
        }

        /// <summary>
        /// Gets or sets the type code for the property.
        /// </summary>
        /// <value>The type code for the property.</value>
        public string Type
        {
            get
            {
                return LendersOffice.CalculatedFields.RealProperty.TypeCode(this.TypeT);
            }

            set
            {
                this.TypeT = LendersOffice.CalculatedFields.RealProperty.TypeFromCode(value);
            }
        }

        /// <summary>
        /// Gets or sets the type for the property, closely associated with the type code Type.
        /// </summary>
        /// <value>The type for the property, closely associated with the type code Type.</value>
        public E_ReoTypeT TypeT
        {
            get
            {
                if (this.Contained.Type == null)
                {
                    return E_ReoTypeT.LeaveBlank;
                }
                else
                {
                    return this.Contained.Type.Value;
                }
            }

            set
            {
                this.Contained.Type = value;
            }
        }

        /// <summary>
        /// Gets or sets the gross value of the property.
        /// </summary>
        /// <value>The gross value of the property.</value>
        public decimal Val
        {
            get
            {
                return this.NumericFormatter.Format(this.Contained.MarketValue);
            }

            set
            {
                this.Contained.MarketValue = Money.Create(value);
            }
        }

        /// <summary>
        /// Gets or sets the gross value of the property as a string.
        /// </summary>
        /// <value>The gross value of the property as a string.</value>
        public string Val_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.MarketValue);
            }

            set
            {
                this.Contained.MarketValue = this.StringParser.TryParseMoney(value);
            }
        }

        /// <summary>
        /// Gets or sets the zipcode in which the property is located.
        /// </summary>
        /// <value>The zipcode in which the property is located.</value>
        public string Zip
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.Zip);
            }

            set
            {
                this.Contained.Zip = this.StringParser.TryParseZipcode(value);
            }
        }

        /// <summary>
        /// Gets or sets the original value of the status.
        /// </summary>
        private string OriginalStatus { get; set; }

        /// <summary>
        /// Gets or sets the RealProperty entity.
        /// </summary>
        private LendingQB.Core.Data.RealProperty Contained { get; set; }

        /// <summary>
        /// Method called to notify the implementing class that the roles of the 
        /// two borrowers have been exchanged.
        /// </summary>
        public void SwapBorrowers()
        {
            this.ownershipManager.SwapBorrowers();
        }

        /// <summary>
        /// Create a shim implementation of the IRealEstateOwned interface.
        /// </summary>
        /// <param name="shimContainer">Reference to the container that holds all shims.</param>
        /// <param name="collectionShim">The shim for the collection that contains this entity shim.</param>
        /// <param name="contained">The contained real property data.</param>
        /// <param name="ownershipManager">The ownership manager for the collection records.</param>
        /// <param name="recordId">The identifier for this real property.</param>
        /// <param name="converter">Instance of LosConvert used to format types as strings.</param>
        /// <returns>A shim for the IRealEstateOwned interface.</returns>
        internal static IRealEstateOwned Create(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            LendingQB.Core.Data.RealProperty contained,
            IShimOwnershipManager<Guid> ownershipManager,
            DataObjectIdentifier<DataObjectKind.RealProperty, Guid> recordId,
            LosConvert converter)
        {
            return Create(shimContainer, collectionShim, contained, ownershipManager, recordId, new FormatRealPropertyShimAsString(converter), null, new ParseRealPropertyShimFromString(converter));
        }

        /// <summary>
        /// Create a DataTable under the DataSet, and create the columns.
        /// </summary>
        /// <param name="ds">The DataSet to which the table will be attached.</param>
        /// <returns>The DataTable with columns initialized.</returns>
        internal static DataTable CreateAndAddDataTable(DataSet ds)
        {
            var table = ds.Tables.Add(TableName);
            table.Columns.Add("RecordId");
            table.Columns.Add("Addr");
            table.Columns.Add("City");
            table.Columns.Add("State");
            table.Columns.Add("Zip");
            table.Columns.Add("Stat");
            table.Columns.Add("Type");
            table.Columns.Add("OccR");
            table.Columns.Add("Val");
            table.Columns.Add("MAmt");
            table.Columns.Add("GrossRentI");
            table.Columns.Add("MPmt");
            table.Columns.Add("HExp");
            table.Columns.Add("NetRentI");
            table.Columns.Add("NetRentILckd");
            table.Columns.Add("ReOwnerT");
            table.Columns.Add("IsSubjectProp");
            table.Columns.Add("IsPrimaryResidence");
            table.Columns.Add("OrderRankValue");
            table.Columns.Add("IsForceCalcNetRentalI");
            table.Columns.Add("VerifSigningEmployeeId");
            table.Columns.Add("VerifSignatureImgId");
            table.Columns.Add("H4HNumOfJointOwners");
            table.Columns.Add("IsEmptyCreated");

            return table;
        }

        /// <summary>
        /// Create a DataRow, populate it, and add it to the input table.
        /// </summary>
        /// <param name="table">The table to which the DataRow will be added.</param>
        /// <returns>The DataRow that was created.</returns>
        internal DataRow CreateAndAddDataRow(DataTable table)
        {
            var row = table.NewRow();
            row["RecordId"] = this.RecordId.ToString("D");
            row["Addr"] = this.Addr;
            row["City"] = this.City;
            row["State"] = this.State;
            row["Zip"] = this.Zip;
            row["Stat"] = this.Stat;
            row["Type"] = this.Type;
            row["OccR"] = this.OccR_rep;
            row["Val"] = this.Val_rep;
            row["MAmt"] = this.MAmt_rep;
            row["GrossRentI"] = this.GrossRentI_rep;
            row["MPmt"] = this.MPmt_rep;
            row["HExp"] = this.HExp_rep;
            row["NetRentI"] = this.NetRentI_rep;
            row["NetRentILckd"] = this.NetRentILckd.ToString();
            row["ReOwnerT"] = ((int)this.ReOwnerT).ToString();
            row["IsSubjectProp"] = this.IsSubjectProp.ToString();
            row["IsPrimaryResidence"] = this.IsPrimaryResidence.ToString();
            row["IsForceCalcNetRentalI"] = this.IsForceCalcNetRentalI.ToString();
            row["VerifSigningEmployeeId"] = DBNull.Value; // Hard-coded for now..
            row["H4HNumOfJointOwners"] = string.Empty; // This item was added for a HUD project that was never released so all production data should be empty, and can be ignored if not.
            row["IsEmptyCreated"] = this.IsEmptyCreated.ToString();

            this.DataRow = row;
            table.Rows.Add(row);
            return row;
        }

        /// <summary>
        /// Create a shim implementation of the IRealEstateOwned interface.
        /// </summary>
        /// <param name="shimContainer">Reference to the container that holds all shims.</param>
        /// <param name="collectionShim">The shim for the collection that contains this entity shim.</param>
        /// <param name="contained">The contained real property data.</param>
        /// <param name="ownershipManager">The ownership manager for the collection records.</param>
        /// <param name="recordId">The identifier for this real property.</param>
        /// <param name="stringFormatter">Object that implements formatting semantic types into string values.</param>
        /// <param name="numericFormatter">Object that implements conversion of semantic types into numeric values.</param>
        /// <param name="stringParser">Object that implements parsing strings into semantic types.</param>
        /// <returns>A shim for the IRealEstateOwned interface.</returns>
        private static IRealEstateOwned Create(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            LendingQB.Core.Data.RealProperty contained,
            IShimOwnershipManager<Guid> ownershipManager,
            DataObjectIdentifier<DataObjectKind.RealProperty, Guid> recordId,
            IFormatAsString stringFormatter,
            IFormatAsNumeric numericFormatter,
            IParseFromString stringParser)
        {
            return new RealPropertyShim(shimContainer, collectionShim, contained, ownershipManager, recordId, stringFormatter ?? new FormatAsStringBase(), numericFormatter ?? new FormatAsNumericBase(), stringParser ?? new ParseFromStringBase());
        }
    }
}
