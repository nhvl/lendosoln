﻿namespace DataAccess
{
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Class used by entity shims to to override the default parsing for semantic types.
    /// </summary>
    public sealed class ParseRealPropertyShimFromString : ParseEntityShimFromString
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ParseRealPropertyShimFromString"/> class.
        /// </summary>
        /// <param name="losConvert">The LosConvert instance to use for formatting.</param>
        public ParseRealPropertyShimFromString(LosConvert losConvert)
            : base(losConvert)
        {
        }

        /// <summary>
        /// Parse the string representation into a Percentage instance.
        /// </summary>
        /// <param name="value">The string representation of a percentage value.</param>
        /// <returns>The Percentage instance, or null.</returns>
        public override Percentage? TryParsePercentage(string value)
        {
            int numeric = this.Converter.ToCount(value);
            return Percentage.Create(numeric);
        }
    }
}
