﻿namespace DataAccess
{
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Class used by the VorRecordShim class to override the default formatting for semantic types.
    /// </summary>
    public class FormatVorRecordShimAsString : FormatEntityShimAsString
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FormatVorRecordShimAsString"/> class.
        /// </summary>
        /// <param name="losConvert">The LosConvert instance to use for formatting.</param>
        public FormatVorRecordShimAsString(LosConvert losConvert)
            : base(losConvert)
        {
        }

        /// <summary>
        /// Format a PhoneNumber value as a string.
        /// </summary>
        /// <param name="value">The PhoneNumber value.</param>
        /// <returns>The string representation of the PhoneNumber value.</returns>
        public override string Format(PhoneNumber? value)
        {
            if (value == null)
            {
                return string.Empty;
            }
            else
            {
                return value.Value.ToString();
            }
        }
    }
}
