﻿namespace DataAccess
{
    using System;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides functionality for retrieving and setting the ownership type of a shim record.
    /// </summary>
    /// <typeparam name="TIdValue">The type of the record identifier.</typeparam>
    public interface IShimOwnershipManager<TIdValue>
        where TIdValue : struct, IEquatable<TIdValue>
    {
        /// <summary>
        /// Gets the default owner for new records.
        /// </summary>
        DataObjectIdentifier<DataObjectKind.Consumer, Guid> DefaultOwner { get; }

        /// <summary>
        /// Gets the id of the legacy application.
        /// </summary>
        DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> AppId { get; }

        /// <summary>
        /// Gets the consumer id of the borrower.
        /// </summary>
        DataObjectIdentifier<DataObjectKind.Consumer, Guid> BorrowerId { get; }

        /// <summary>
        /// Gets the consumer id of the coborrower.
        /// </summary>
        DataObjectIdentifier<DataObjectKind.Consumer, Guid> CoborrowerId { get; }

        /// <summary>
        /// Gets the ownership type for the given record.
        /// </summary>
        /// <param name="recordId">The record id.</param>
        /// <returns>The ownership type for the given record.</returns>
        Ownership GetOwnership(TIdValue recordId);

        /// <summary>
        /// Sets the ownership type for the given record.
        /// </summary>
        /// <param name="recordId">The record id.</param>
        /// <param name="ownership">The ownership type to set.</param>
        void SetOwnership(TIdValue recordId, Ownership ownership);

        /// <summary>
        /// When a borrower swap is performed, the interpretation of the identifiers
        /// changes.  The ownership managers need to be advised of this operation so
        /// they continue to function correctly.
        /// </summary>
        void SwapBorrowers();
    }
}