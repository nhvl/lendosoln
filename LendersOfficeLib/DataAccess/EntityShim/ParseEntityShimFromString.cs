﻿namespace DataAccess
{
    using System;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Class used by entity shims to to override the default parsing for semantic types.
    /// </summary>
    public class ParseEntityShimFromString : ParseFromStringBase, IContainLosConvert
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ParseEntityShimFromString"/> class.
        /// </summary>
        /// <param name="losConvert">The LosConvert instance to use for formatting.</param>
        public ParseEntityShimFromString(LosConvert losConvert)
        {
            this.Converter = losConvert;
        }

        /// <summary>
        /// Gets the LosConvert instance to use for parsing.
        /// </summary>
        public LosConvert Converter { get; private set; }

        /// <summary>
        /// Parse the string representation into a Count&lt;T&gt; instance.
        /// </summary>
        /// <typeparam name="T">The generic type.</typeparam>
        /// <param name="value">The string representation of a Count&lt;T&gt; value.</param>
        /// <returns>The Count&lt;T&gt; instance, or null.</returns>
        public override Count<T>? TryParseCount<T>(string value)
        {
            int count = this.Converter.ToCount(value);
            return Count<T>.Create(count);
        }

        /// <summary>
        /// Parse the string representation into a Money instance.
        /// </summary>
        /// <param name="value">The string representation of a monetary value.</param>
        /// <returns>The Money instance, or null.</returns>
        public override Money? TryParseMoney(string value)
        {
            decimal numeric = this.Converter.ToMoney(value);
            return Money.Create(numeric);
        }

        /// <summary>
        /// Parse the string representation into a Percentage instance.
        /// </summary>
        /// <param name="value">The string representation of a percentage value.</param>
        /// <returns>The Percentage instance, or null.</returns>
        public override Percentage? TryParsePercentage(string value)
        {
            decimal numeric = this.Converter.ToRate(value);
            return Percentage.Create(numeric);
        }

        /// <summary>
        /// Parse the string representation into a PhoneNumber instance.
        /// </summary>
        /// <param name="value">The string representation of a phone number value.</param>
        /// <returns>The PhoneNumber instance, or null.</returns>
        public override PhoneNumber? TryParsePhoneNumber(string value)
        {
            string parsed = this.Converter.ToPhoneNumFormat(value, FormatDirection.ToDb);
            return PhoneNumber.Create(parsed);
        }

        /// <summary>
        /// Parse the string representation into an UnzonedDate intstance.
        /// </summary>
        /// <param name="value">The string representation of a date value.</param>
        /// <returns>The UnzonedDate instance, or null.</returns>
        public override UnzonedDate? TryParseUnzonedDate(string value)
        {
            var cdate = CDateTime.Create(value, this.Converter);
            if (!cdate.IsValid)
            {
                return null;
            }

            var dt = cdate.DateTimeForComputation;
            return UnzonedDate.Create(dt);
        }

        /// <summary>
        /// Parses a string value into an UncheckedZipcode.
        /// </summary>
        /// <param name="value">The string value.</param>
        /// <returns>The unchecked zip code or null.</returns>
        public override Zipcode? TryParseZipcode(string value)
        {
            if (value == null)
            {
                value = string.Empty;
            }
            else
            {
                value = value.TrimWhitespaceAndBOM();
                value = value.Substring(0, Math.Min(5, value.Length));
            }

            return Zipcode.Create(value);
        }
    }
}
