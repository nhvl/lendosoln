﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using LendersOffice.Common;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;

    using ShimTarget = LendingQB.Core.Data;

    /// <summary>
    /// Shim for a verification of rent record collection.
    /// </summary>
    internal sealed class VorRecordCollectionShim : IVerificationOfRentCollection
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VorRecordCollectionShim"/> class.
        /// </summary>
        /// <param name="shimContainer">The shim container.</param>
        /// <param name="appId">The application id.</param>
        /// <param name="borrowerId">The consumer id of the borrower.</param>
        /// <param name="coborrowerId">The consumer id of the coborrower.</param>
        /// <param name="converter">An instance of LosConvert used for formatting the data.</param>
        public VorRecordCollectionShim(
            IShimContainer shimContainer,
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> borrowerId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> coborrowerId,
            LosConvert converter)
        {
            this.ShimContainer = shimContainer;
            this.AppId = appId;
            this.BorrowerId = borrowerId;
            this.CoborrowerId = coborrowerId;
            this.Converter = converter;
        }

        /// <summary>
        /// Gets the shim container.
        /// </summary>
        private IShimContainer ShimContainer { get; }

        /// <summary>
        /// Gets the id of the application.
        /// </summary>
        private DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> AppId { get; }

        /// <summary>
        /// Gets the consumer id of the borrower.
        /// </summary>
        private DataObjectIdentifier<DataObjectKind.Consumer, Guid> BorrowerId { get; }

        /// <summary>
        /// Gets the consumer id of the coborrower.
        /// </summary>
        private DataObjectIdentifier<DataObjectKind.Consumer, Guid> CoborrowerId { get; }

        /// <summary>
        /// Gets the LosConvert instance used for string formatting.
        /// </summary>
        private LosConvert Converter { get; }

        /// <summary>
        /// Gets or sets the DataSet that is used temporarily during an Update call.
        /// </summary>
        private DataSet TheDataSet { get; set; }

        /// <summary>
        /// Clear all entries from the record collection.
        /// </summary>
        public void Clear()
        {
            foreach (var item in this.BuildShimData())
            {
                var identifier = DataObjectIdentifier<DataObjectKind.VorRecord, Guid>.Create(item.RecordId);
                this.ShimContainer.RemoveVorRecordFromLegacyApplication(identifier, this.AppId, this.BorrowerId, this.CoborrowerId);
            }
        }

        /// <summary>
        /// Count the number of entries in the collection.
        /// </summary>
        /// <returns>The number of entries in the collection.</returns>
        public int Count()
        {
            return this.ShimContainer.GetVorRecordsForLegacyApplication(this.AppId, this.BorrowerId, this.CoborrowerId).Count();
        }

        /// <summary>
        /// Delete the record by id.
        /// </summary>
        /// <param name="recordId">The record id of the record to delete.</param>
        public void DeleteByRecordId(Guid recordId)
        {
            var uponEmpty = default(KeyValuePair<DataObjectIdentifier<DataObjectKind.VorRecord, Guid>, VorRecord>);
            var idEntityPair = this.GetIdEntityPair(recordId);
            if (!idEntityPair.Equals(uponEmpty))
            {
                this.ShimContainer.RemoveVorRecordFromLegacyApplication(idEntityPair.Key, this.AppId, this.BorrowerId, this.CoborrowerId);
            }
        }

        /// <summary>
        /// Get the record by the index.
        /// </summary>
        /// <param name="index">The index of the record.</param>
        /// <returns>The record assoicated with the record id.</returns>
        public IVerificationOfRent GetRecordByIndex(int index)
        {
            var keyPair = this.ShimContainer.GetVorRecordsForLegacyApplication(this.AppId, this.BorrowerId, this.CoborrowerId).ElementAt(index);
            return VorRecordShim.Create(this, keyPair.Value, keyPair.Key, this.Converter);
        }

        /// <summary>
        /// Get the record by the record id.
        /// </summary>
        /// <param name="recordId">The id of the record.</param>
        /// <returns>The record associated with the record id.</returns>
        public IVerificationOfRent GetRecordByRecordId(Guid recordId)
        {
            var uponEmpty = default(KeyValuePair<DataObjectIdentifier<DataObjectKind.VorRecord, Guid>, VorRecord>);
            var idEntityPair = this.GetIdEntityPair(recordId);
            if (!idEntityPair.Equals(uponEmpty))
            {
                return VorRecordShim.Create(this, idEntityPair.Value, idEntityPair.Key, this.Converter);
            }
            else
            {
                bool isSpecialLegacyId = VorRecord.LegacyRecordIdToSpecialType.ContainsKey(recordId);

                // We don't want to try to use one of the hard-coded legacy ids as a primary key,
                // as they will be duplicated across files. Instead, we will create a new identifier
                // and update the special record type for the new entity.
                var identifier = DataObjectIdentifier<DataObjectKind.VorRecord, Guid>.Create(isSpecialLegacyId ? Guid.NewGuid() : recordId);
                var vor = new VorRecord();
                if (isSpecialLegacyId)
                {
                    vor.SetLegacyRecordSpecialType(VorRecord.LegacyRecordIdToSpecialType[recordId]);
                }

                this.ShimContainer.Add(this.BorrowerId, identifier, vor);

                return VorRecordShim.Create(this, vor, identifier, this.Converter);
            }
        }

        /// <summary>
        /// Update the changes in the collection back to the source.
        /// </summary>
        public void Update()
        {
            var ds = this.GetDataSet(createNew: true);
        }

        /// <summary>
        /// Gets a DataSet representing the current state of the collection.
        /// </summary>
        /// <returns>A DataSet represetning teh current state of the collection.</returns>
        public DataSet GetDataSet()
        {
            return this.GetDataSet(createNew: true);
        }

        /// <summary>
        /// Ensures that the publicly retrievable data set accurately reflects the state of
        /// the collection shim.
        /// </summary>
        public void EnsureDataSetUpToDate()
        {
            // This is a no-op for this class, as the publicly retrievable data set
            // is rebuilt each time it is accessed.
        }

        /// <summary>
        /// Build and return a DataSet representing the collection.
        /// </summary>
        /// <param name="createNew">True if the DataSet should be recreated from scratch.</param>
        /// <returns>A DataSet with all the data in the collection.</returns>
        internal DataSet GetDataSet(bool createNew)
        {
            if (createNew || this.TheDataSet == null)
            {
                var ds = new DataSet();
                var table = VorRecordShim.CreateAndAddDataTable(ds);
                foreach (var shim in this.BuildShimData())
                {
                    VorRecordShim.CreateAndAddDataRow(table, shim);
                }

                this.TheDataSet = ds;
            }

            return this.TheDataSet;
        }

        /// <summary>
        /// Create the shims.
        /// </summary>
        /// <remarks>
        /// Cannot use the yield return pattern here due to implementation of the Clear() method above.
        /// </remarks>
        /// <returns>An enumerator for the shims.</returns>
        private IEnumerable<IVerificationOfRent> BuildShimData()
        {
            var shims = new List<IVerificationOfRent>();
            foreach (var keyPair in this.ShimContainer.GetVorRecordsForLegacyApplication(this.AppId, this.BorrowerId, this.CoborrowerId))
            {
                var vor = VorRecordShim.Create(this, keyPair.Value, keyPair.Key, this.Converter);
                shims.Add(vor);
            }

            return shims;
        }

        /// <summary>
        /// Gets the key-value pair for the given legacy record id. Understands the special legacy record ids.
        /// </summary>
        /// <param name="recordId">The legacy record id.</param>
        /// <returns>The key-value pair for the given record id, or the default key-value pair if it wasn't found.</returns>
        private KeyValuePair<DataObjectIdentifier<DataObjectKind.VorRecord, Guid>, VorRecord> GetIdEntityPair(Guid recordId)
        {
            var vorRecordsForApp = this.ShimContainer.GetVorRecordsForLegacyApplication(this.AppId, this.BorrowerId, this.CoborrowerId);

            if (VorRecord.LegacyRecordIdToSpecialType.ContainsKey(recordId))
            {
                return vorRecordsForApp.SingleOrDefault(a => a.Value.LegacyRecordSpecialType == VorRecord.LegacyRecordIdToSpecialType[recordId]);
            }
            else
            {
                return vorRecordsForApp.SingleOrDefault(a => a.Key.Value == recordId && a.Value.LegacyRecordSpecialType == null);
            }
        }
    }
}
