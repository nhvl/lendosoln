﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using LqbGrammar.DataTypes;
    using InterfaceDefs = LendingQB.Core;
    using ShimTarget = LendingQB.Core.Data;

    /// <summary>
    /// Shim for an asset collection.
    /// </summary>
    public sealed class AssetCollectionShim : RecordCollectionShim<DataObjectKind.Asset, ShimTarget.Asset>, IAssetCollection
    {
        /// <summary>
        /// The loan file data.
        /// </summary>
        private IShimLoanDataProvider loanData;

        /// <summary>
        /// The application data.
        /// </summary>
        private IShimAppDataProvider appData;

        /// <summary>
        /// The ownership manager for the real properties.
        /// </summary>
        private IShimOwnershipManager<Guid> ownershipManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="AssetCollectionShim"/> class.
        /// </summary>
        /// <param name="loan">Reference to the loan.</param>
        /// <param name="application">Reference to the application.</param>
        /// <param name="shimContainer">Reference to the container that holds all shims.</param>
        /// <param name="ownershipManager">The ownership manager for the collection records.</param>
        /// <param name="converter">An instance of LosConvert used for formatting the data.</param>
        public AssetCollectionShim(
            IShimLoanDataProvider loan,
            IShimAppDataProvider application,
            IShimContainer shimContainer,
            IShimOwnershipManager<Guid> ownershipManager,
            LosConvert converter)
            : base(shimContainer, ownershipManager.AppId, new AssetDetermineSpecialEntity(), converter)
        {
            this.ownershipManager = ownershipManager;

            var ds = this.GetDataSet();
            var table = AssetShim.CreateAndAddDataTable(ds);
            this.loanData = loan;
            this.appData = application;
            this.RebuildShimData();
        }

        /// <summary>
        /// Gets the collection of automobiles in this collection.
        /// </summary>
        public ISubcollection AutomobileCollection
        {
            get
            {
                return this.GetSubcollection(true, E_AssetGroupT.Auto);
            }
        }

        /// <summary>
        /// Gets the total worth of all the automobiles in the collection.
        /// </summary>
        public string AutomobileWorthTotal_rep
        {
            get
            {
                decimal total = Sum(this.AutomobileCollection);
                return this.Converter.ToMoneyString(total, FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// Gets the collection of normal assets beyond the fourth.
        /// </summary>
        public ISubcollection ExtraNormalAssets
        {
            get
            {
                var normalAssets = this.NormalAssetCollection;
                if (normalAssets.Count > 4)
                {
                    var list = new ICollectionItemBase[normalAssets.Count - 4];

                    for (int i = 0; i < normalAssets.Count - 4; i++)
                    {
                        list[i] = normalAssets.GetRecordAt(i + 4);
                    }

                    return CXmlRecordSubcollection.Create(list);
                }
                else
                {
                    return CXmlRecordSubcollection.Create(new ICollectionItemBase[0]);
                }
            }
        }

        /// <summary>
        /// Gets the first four normal assets.
        /// </summary>
        public ISubcollection First4NormalAssets
        {
            get
            {
                var normalAssets = this.NormalAssetCollection;
                if (normalAssets.Count > 4)
                {
                    // 5/24/2004 dd - Create an array of record with maximum 4 entries.
                    var list = new ICollectionItemBase[4];
                    for (int i = 0; i < 4; i++)
                    {
                        list[i] = normalAssets.GetRecordAt(i);
                    }

                    return CXmlRecordSubcollection.Create(list);
                }
                else
                {
                    // 5/24/2004 dd - Less than 4 check/saving account, just return the list.
                    return normalAssets;
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether there are more than three automobiles in this collection.
        /// </summary>
        public bool HasExtraAutomobiles
        {
            get
            {
                return this.AutomobileCollection.Count > 3;
            }
        }

        /// <summary>
        /// Gets a value indicating whether are more than three extra assets.
        /// </summary>
        public bool HasExtraOtherAssets
        {
            get
            {
                return this.OtherAssetCollection.Count > 3;
            }
        }

        /// <summary>
        /// Gets a value indicating whether there are more than three stocks in the collection.
        /// </summary>
        public bool HasExtraStocks
        {
            get
            {
                return this.StockCollection.Count > 3;
            }
        }

        /// <summary>
        /// Gets a value indicating whether there are more than four normal assets in the collection.
        /// </summary>
        public bool HasMoreThan4NormalAssets
        {
            get
            {
                return this.NormalAssetCollection.Count > 4;
            }
        }

        /// <summary>
        /// Gets the collection of normal assets in this collection.
        /// </summary>
        public ISubcollection NormalAssetCollection
        {
            get
            {
                var filter =
                    E_AssetGroupT.Checking |
                    E_AssetGroupT.Savings |
                    E_AssetGroupT.GiftFunds |
                    E_AssetGroupT.OtherLiquidAsset |
                    E_AssetGroupT.GiftEquity |
                    E_AssetGroupT.BridgeLoanNotDposited |
                    E_AssetGroupT.CertificateOfDeposit |
                    E_AssetGroupT.TrustFunds |
                    E_AssetGroupT.SecuredBorrowedFundsNotDeposit |
                    E_AssetGroupT.MoneyMarketFund |
                    E_AssetGroupT.PendingNetSaleProceedsFromRealEstateAssets |
                    E_AssetGroupT.MutualFunds |
                    E_AssetGroupT.StockOptions |
                    E_AssetGroupT.EmployerAssistance |
                    E_AssetGroupT.IndividualDevelopmentAccount |
                    E_AssetGroupT.ProceedsFromSaleOfNonRealEstateAsset |
                    E_AssetGroupT.ProceedsFromUnsecuredLoan |
                    E_AssetGroupT.Grant |
                    E_AssetGroupT.LeasePurchaseCredit |
                    E_AssetGroupT.SweatEquity |
                    E_AssetGroupT.TradeEquityFromPropertySwap;

                return this.GetSubcollection(
                    true,
                    filter);
            }
        }

        /// <summary>
        /// Gets the collection of other assets.
        /// </summary>
        public ISubcollection OtherAssetCollection
        {
            get
            {
                return this.GetSubcollection(true, E_AssetGroupT.OtherIlliquidAsset);
            }
        }

        /// <summary>
        /// Gets a collection of all the "regular" assets within the current instance. 
        /// </summary>
        public IEnumerable<IAssetRegular> RegularAssets
        {
            get
            {
                int regularCount = this.CountRegular;
                for (int i = 0; i < regularCount; ++i)
                {
                    yield return (IAssetRegular)this.GetRegularRecordAt(i);
                }
            }
        }

        /// <summary>
        /// Gets a collection of assets to consider for VOA/VOD orders.
        /// </summary>
        public IEnumerable<IAssetRegular> AssetsForVerification => this.RegularAssets.Where(a => a.IsLiquidAsset);

        /// <summary>
        /// Gets the total of the collection of other assets.
        /// </summary>
        public string OtherAssetTotal_rep
        {
            get
            {
                decimal total = Sum(this.OtherAssetCollection);
                return this.Converter.ToMoneyString(total, FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// Gets the collection of stocks and bonds.
        /// </summary>
        public ISubcollection StockCollection
        {
            get
            {
                return this.GetSubcollection(true, E_AssetGroupT.Stocks | E_AssetGroupT.Bonds);
            }
        }

        /// <summary>
        /// Gets the total of the collection of stocks and bonds.
        /// </summary>
        public string StockTotal_rep
        {
            get
            {
                decimal total = Sum(this.StockCollection);
                return this.Converter.ToMoneyString(total, FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// Filters out special assets.
        /// </summary>
        protected override string SortedViewRowFilter => CAssetCollection.SortedViewRowFilter;

        /// <summary>
        /// Adds a new asset record.
        /// </summary>
        /// <returns>The newly added record.</returns>
        public IAsset AddRegularRecord()
        {
            var item = new ShimTarget.Asset();
            var id = this.AddEntity(item);
            this.RebuildShimData();

            return this.GetRegRecordOf(id.Value);
        }

        /// <summary>
        /// Adds a new asset at the given index.
        /// </summary>
        /// <remarks>
        /// MoveRegularRecordUp calls RebuildShimData so there is no need to do so here after calling that method.
        /// </remarks>
        /// <param name="pos">The index.</param>
        /// <returns>The newly added record.</returns>
        public IAsset AddRegularRecordAt(int pos)
        {
            var item = new ShimTarget.Asset();
            var id = this.InsertRegularRecordAt(item, pos);
            return this.GetRegRecordOf(id.Value);
        }

        /// <summary>
        /// Gets the record with the given id.
        /// </summary>
        /// <param name="recordId">The record id.</param>
        /// <returns>The record with the given id.</returns>
        public IAsset GetRegRecordOf(Guid recordId)
        {
            return (IAsset)this.GetRegularRecordOf(recordId);
        }

        /// <summary>
        /// Gets the record at the given index.
        /// </summary>
        /// <param name="pos">The index.</param>
        /// <returns>The record at the given index.</returns>
        public IAsset GetRegularRecordAt(int pos)
        {
            return (IAsset)this.GetRegularItemAt(pos);
        }

        /// <summary>
        /// If there is no record with the indicated identifier, create one.  Return the record with the indicated identifier.
        /// </summary>
        /// <param name="recordId">The identifier that the returned record should have.</param>
        /// <returns>The record with the specified identifier.</returns>
        public ICollectionItemBase2 EnsureRegularRecordOf(Guid recordId)
        {
            var shim = this.TryGetRegularRecordOf(recordId);
            if (shim == null)
            {
                var identifier = DataObjectIdentifier<DataObjectKind.Asset, Guid>.Create(recordId);
                var item = new ShimTarget.Asset();
                this.AddEntity(identifier, item);
                this.RebuildShimData();

                shim = this.GetRegRecordOf(recordId);
            }

            return shim;
        }

        /// <summary>
        /// Create and add a new record of the specified type.
        /// </summary>
        /// <remarks>
        /// The method AddRegularRecord calls RebuildShimData so there is no need to do so here after calling that method.
        /// </remarks>
        /// <param name="recordT">The type of the asset.</param>
        /// <returns>The new record.</returns>
        ICollectionItemBase2 IRecordCollection.AddRecord(Enum recordT)
        {
            if (this.IsSpecial(recordT))
            {
                return this.AddSpecialRecord((E_AssetT)recordT);
            }
            else
            {
                var regularShim = this.AddRegularRecord();
                regularShim.KeyType = recordT;
                return regularShim;
            }
        }

        /// <summary>
        /// Retrieve the record with the specified identifier.
        /// </summary>
        /// <param name="recordId">The identifier for the desired record.</param>
        /// <returns>The desired record, or throw an exception.</returns>
        ICollectionItemBase2 IRecordCollection.GetRegRecordOf(Guid recordId)
        {
            return this.GetRegRecordOf(recordId);
        }

        /// <summary>
        /// Retrieve the regular record at the specified position.
        /// </summary>
        /// <param name="pos">The position.</param>
        /// <returns>The record at the specified position, or throw an out of bounds exception.</returns>
        ICollectionItemBase2 IRecordCollection.GetRegularRecordAt(int pos)
        {
            return this.GetRegularRecordAt(pos);
        }

        /// <summary>
        /// Retrieve the special record at the specified position.
        /// </summary>
        /// <param name="i">The position.</param>
        /// <returns>The record at the specified position, or throw an out of bounds exception.</returns>
        ICollectionItemBase2 IRecordCollection.GetSpecialRecordAt(int i)
        {
            return this.GetSpecialRecordAt(i);
        }

        /// <summary>
        /// Create and add a new record.
        /// </summary>
        /// <returns>The new record.</returns>
        ICollectionItemBase2 IRecordCollection.AddRegularRecord()
        {
            return this.AddRegularRecord();
        }

        /// <summary>
        /// Create and add a new record at the specified position.
        /// </summary>
        /// <remarks>
        /// The method AddRegularRecordAt calls RebuildShimData so there is no need to do so here after calling that method.
        /// </remarks>
        /// <param name="pos">The position into which the record should be inserted.</param>
        /// <returns>The new record.</returns>
        ICollectionItemBase2 IRecordCollection.AddRegularRecordAt(int pos)
        {
            return this.AddRegularRecordAt(pos);
        }

        /// <summary>
        /// Add a special record of the specified type.
        /// </summary>
        /// <param name="assetT">The asset type to create.</param>
        /// <returns>The new record.</returns>
        public IAssetSpecial AddSpecialRecord(E_AssetT assetT)
        {
            var item = new ShimTarget.Asset();
            IAssetSpecial special = null;
            item.AssetType = assetT;
            switch (assetT)
            {
                case E_AssetT.Business:
                        special = this.GetBusinessWorth(false);
                        if (special == null)
                        {
                            return this.GetBusinessWorth(true);
                        }

                    break;
                case E_AssetT.CashDeposit:
                    special = this.GetCashDeposit1(false);
                    if (special == null)
                    {
                        return this.GetCashDeposit1(true);
                    }

                    special = this.GetCashDeposit2(false);
                    if (special == null)
                    {
                        return this.GetCashDeposit2(true);
                    }

                    break;
                case E_AssetT.LifeInsurance:
                        special = this.GetLifeInsurance(false);
                        if (special == null)
                        {
                            return this.GetLifeInsurance(true);
                        }

                    break;
                case E_AssetT.Retirement:
                    special = this.GetRetirement(false);
                    if (special == null)
                    {
                        return this.GetRetirement(true);
                    }

                    break;
                default:
                    throw new UnhandledEnumException(assetT);
            }

            throw new CBaseException(
                LendersOffice.Common.ErrorMessages.Generic,
                "Cannot add any more special liability record of the requested type = " + assetT.ToString());
        }

        /// <summary>
        /// Add a record.
        /// </summary>
        /// <param name="assetT">The asset type.</param>
        /// <returns>The new record.</returns>
        public IAsset AddRecord(E_AssetT assetT)
        {
            if (this.IsSpecial(assetT))
            {
                return this.AddSpecialRecord(assetT);
            }
            else
            {
                var regularRecord = this.AddRegularRecord();
                regularRecord.KeyType = assetT;
                return regularRecord;
            }
        }

        /// <summary>
        /// Add a new regular record.
        /// </summary>
        /// <returns>The new record.</returns>
        IAssetRegular IAssetCollection.AddRegularRecord()
        {
            return (IAssetRegular)this.AddRegularRecord();
        }

        /// <summary>
        /// Add a regular record at a specified position.
        /// </summary>
        /// <param name="pos">The position to add the record.</param>
        /// <returns>The record added.</returns>
        IAssetRegular IAssetCollection.AddRegularRecordAt(int pos)
        {
            return (IAssetRegular)this.AddRegularRecordAt(pos);
        }

        /// <summary>
        /// Bind from object model.
        /// </summary>
        /// <param name="inputModelDict">Input model dictionary.</param>
        public void BindFromObjectModel(Dictionary<string, object> inputModelDict)
        {
            // Related to new UI not supported by Shim.
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get the first business asset record.
        /// </summary>
        /// <param name="forceCreate">Create a record if it does not exist.</param>
        /// <returns>The new record.</returns>
        public IAssetBusiness GetBusinessWorth(bool forceCreate)
        {
            var businessAsset = this.SpecialRecords.Where(r => ((IAsset)r).AssetT == E_AssetT.Business).FirstOrDefault();
            if (businessAsset != null)
            {
                return (IAssetBusiness)businessAsset;
            }

            if (forceCreate)
            {
                var item = new ShimTarget.Asset();
                item.AssetType = E_AssetT.Business;
                var id = this.AddEntity(item);
                this.RebuildShimData();

                return (IAssetBusiness)this.GetSpecialItemAt(this.FindSpecialPosition(id.Value));
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Get the first cash deposit record.
        /// </summary>
        /// <param name="forceCreate">Create a record if it does not exist.</param>
        /// <returns>The new record.</returns>
        public IAssetCashDeposit GetCashDeposit1(bool forceCreate)
        {
            var cashDepositAsset = this.SpecialRecords.Where(r => r is IAssetCashDeposit && ((IAssetCashDeposit)r).AssetCashDepositT == E_AssetCashDepositT.CashDeposit1).FirstOrDefault();
            if (cashDepositAsset != null)
            {
                return (IAssetCashDeposit)cashDepositAsset;
            }

            if (forceCreate)
            {
                var item = new ShimTarget.Asset();
                item.AssetType = E_AssetT.CashDeposit;
                var id = this.AddEntity(item);
                item.AssetCashDepositType = E_AssetCashDepositT.CashDeposit1;
                this.RebuildShimData();

                return (IAssetCashDeposit)this.GetSpecialItemAt(this.FindSpecialPosition(id.Value));
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Get the second cash deposit record.
        /// </summary>
        /// <param name="forceCreate">Create a record if it does not exist.</param>
        /// <returns>The new record.</returns>
        public IAssetCashDeposit GetCashDeposit2(bool forceCreate)
        {
            var cashDepositAsset = this.SpecialRecords.Where(r => r is IAssetCashDeposit && ((IAssetCashDeposit)r).AssetCashDepositT == E_AssetCashDepositT.CashDeposit2).FirstOrDefault();
            if (cashDepositAsset != null)
            {
                return (IAssetCashDeposit)cashDepositAsset;
            }

            if (forceCreate)
            {
                var item = new ShimTarget.Asset();
                item.AssetType = E_AssetT.CashDeposit;
                item.AssetCashDepositType = E_AssetCashDepositT.CashDeposit2;
                var id = this.AddEntity(item);
                this.RebuildShimData();

                return (IAssetCashDeposit)this.GetSpecialItemAt(this.FindSpecialPosition(id.Value));
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Get the first life insurance record.
        /// </summary>
        /// <param name="forceCreate">Create a record if it does not exist.</param>
        /// <returns>The new record.</returns>
        public IAssetLifeInsurance GetLifeInsurance(bool forceCreate)
        {
            var lifeInsuranceAsset = this.SpecialRecords.Where(r => ((IAsset)r).AssetT == E_AssetT.LifeInsurance).FirstOrDefault();
            if (lifeInsuranceAsset != null)
            {
                return (IAssetLifeInsurance)lifeInsuranceAsset;
            }

            if (forceCreate)
            {
                var item = new ShimTarget.Asset();
                item.AssetType = E_AssetT.LifeInsurance;
                var id = this.AddEntity(item);
                this.RebuildShimData();

                return (IAssetLifeInsurance)this.GetSpecialItemAt(this.FindSpecialPosition(id.Value));
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Retrieve the record with the specified identifier.
        /// </summary>
        /// <param name="recordId">The identifier for the desired record.</param>
        /// <returns>The desired record, or throw an exception.</returns>
        IAssetRegular IAssetCollection.GetRegRecordOf(Guid recordId)
        {
            return (IAssetRegular)this.GetRegRecordOf(recordId);
        }

        /// <summary>
        /// Retrieve the regular record at the specified position.
        /// </summary>
        /// <param name="pos">The position.</param>
        /// <returns>The record at the specified position, or throw an out of bounds exception.</returns>
        IAssetRegular IAssetCollection.GetRegularRecordAt(int pos)
        {
            return (IAssetRegular)GetRegularItemAt(pos);
        }

        /// <summary>
        /// Get the first retirement record.
        /// </summary>
        /// <param name="forceCreate">Create a record if it does not exist.</param>
        /// <returns>The new record.</returns>
        public IAssetRetirement GetRetirement(bool forceCreate)
        {
            var retirementAsset = this.SpecialRecords.Where(r => ((IAsset)r).AssetT == E_AssetT.Retirement).FirstOrDefault();
            if (retirementAsset != null)
            {
                return (IAssetRetirement)retirementAsset;
            }

            if (forceCreate)
            {
                var item = new ShimTarget.Asset();
                item.AssetType = E_AssetT.Retirement;
                var id = this.AddEntity(item);
                this.RebuildShimData();

                return (IAssetRetirement)this.GetSpecialItemAt(this.FindSpecialPosition(id.Value));
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Retrieve the special record at the specified position.
        /// </summary>
        /// <param name="i">The position.</param>
        /// <returns>The record at the specified position, or throw an out of bounds exception.</returns>
        public IAssetSpecial GetSpecialRecordAt(int i)
        {
            return (IAssetSpecial)GetSpecialItemAt(i);
        }

        /// <summary>
        /// Get the subcollection of the group.
        /// </summary>
        /// <param name="inclusiveGroup">Whether inclusive of the group.</param>
        /// <param name="group">The group.</param>
        /// <returns>The matching subcollection.</returns>
        public ISubcollection GetSubcollection(bool inclusiveGroup, E_AssetGroupT group)
        {
            return this.GetSubcollectionCore(inclusiveGroup, group);
        }

        /// <summary>
        /// Get the input model.
        /// </summary>
        /// <returns>The input model.</returns>
        public Dictionary<string, object> ToInputModel()
        {
            // Related to new UI not supported by Shim.
            throw new NotImplementedException();
        }

        /// <summary>
        /// Aggregate data needs to be calculated and passed up to the containing legacy application.
        /// </summary>
        protected override void HandleFlushData()
        {
            var subcoll = this.GetSubcollection(true,  E_AssetGroupT.All);

            var shouldIncludeCashDepositInTotalLiquidAssets =
                !LendersOffice.Migration.LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(
                this.loanData.sLoanVersionT,
                    LendersOffice.Migration.LoanVersionT.V7_ExcludeCashDeposit_Opm236245) &&
                        !this.loanData.BrokerDB.ExcludeCashDepositFromAssetTotals;

            var aggData = LendersOffice.CalculatedFields.Asset.CalculateAggregateData(subcoll.Cast<IAsset>(), shouldIncludeCashDepositInTotalLiquidAssets);

            this.appData.aAsstLiqTot_ = aggData.AsstLiqTot.Value;
            this.appData.aAsstNonReSolidTot_ = aggData.AsstNonReSolidTot.Value;
        }

        /// <summary>
        /// Build up the shim data.
        /// </summary>
        protected override void BuildShimData()
        {
            var ds = this.GetDataSet();
            var table = ds.Tables[0];
            foreach (var keyValue in this.GetAllCollectionEntities())
            {
                var shim = AssetShim.Create(this.ShimContainer, this, this.appData, keyValue.Value, this.ownershipManager, keyValue.Key, this.Converter);
                if (shim is AssetRegularShim)
                {
                    this.RegularRecords.Add(shim);
                }
                else
                {
                    this.SpecialRecords.Add(shim);
                }

                var asset = (AssetShim)shim;
                asset.CreateAndAddDataRow(table);
            }
        }

        /// <summary>
        /// Adds the given record to the underlying collection via the aggregate.
        /// </summary>
        /// <param name="value">The record to add.</param>
        /// <returns>The identifier of the new record.</returns>
        protected override DataObjectIdentifier<DataObjectKind.Asset, Guid> AddEntity(ShimTarget.Asset value)
        {
            return this.ShimContainer.Add(this.ownershipManager.DefaultOwner, value);
        }

        /// <summary>
        /// Adds the given id and record to the underlying collection via the aggregate.
        /// </summary>
        /// <param name="recordId">The id to use.</param>
        /// <param name="value">The record to add.</param>
        protected override void AddEntity(DataObjectIdentifier<DataObjectKind.Asset, Guid> recordId, ShimTarget.Asset value)
        {
            this.ShimContainer.Add(this.ownershipManager.DefaultOwner, recordId, value);
        }

        /// <summary>
        /// Removes a record from the underlying collection via the aggregate.
        /// </summary>
        /// <param name="recordId">The record id.</param>
        protected override void RemoveEntity(DataObjectIdentifier<DataObjectKind.Asset, Guid> recordId)
        {
            this.ShimContainer.RemoveAssetFromLegacyApplication(recordId, this.ownershipManager.AppId, this.ownershipManager.BorrowerId, this.ownershipManager.CoborrowerId);
        }

        /// <summary>
        /// Gets the collection records relevant to the shimmed application.
        /// </summary>
        /// <returns>The collection records relevant to the shimmed application.</returns>
        protected override IEnumerable<KeyValuePair<DataObjectIdentifier<DataObjectKind.Asset, Guid>, ShimTarget.Asset>> GetAllCollectionEntities()
        {
            return this.ShimContainer.GetAssetsForLegacyApplication(this.ownershipManager.AppId, this.ownershipManager.BorrowerId, this.ownershipManager.CoborrowerId);
        }

        /// <summary>
        /// Sum the values of a collection.
        /// </summary>
        /// <param name="collection">The applicable collection.</param>
        /// <returns>The sum of the values.</returns>
        private static decimal Sum(ISubcollection collection)
        {
            decimal total = 0.0M;
            int count = collection.Count;
            for (int i = 0; i < count; i++)
            {
                var field = (IAsset)collection.GetRecordAt(i);
                total += field.Val;
            }

            return total;
        }
    }
}
