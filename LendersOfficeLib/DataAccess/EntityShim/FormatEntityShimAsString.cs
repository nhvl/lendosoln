﻿namespace DataAccess
{
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Class used by entity shims to override the default formatting for semantic types.
    /// </summary>
    public class FormatEntityShimAsString : FormatAsStringBase, IContainLosConvert
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FormatEntityShimAsString"/> class.
        /// </summary>
        /// <param name="losConvert">The LosConvert instance to use for formatting.</param>
        public FormatEntityShimAsString(LosConvert losConvert)
        {
            this.Converter = losConvert;
            this.NumericConverter = new FormatAsNumericBase();
        }

        /// <summary>
        /// Gets the LosConvert instance to use for formatting.
        /// </summary>
        public LosConvert Converter { get; private set; }

        /// <summary>
        /// Gets a converter to extract numeric values from semantic types.
        /// </summary>
        protected IFormatAsNumeric NumericConverter { get; private set; }

        /// <summary>
        /// Format a Count&lt;T&gt; as a string.
        /// </summary>
        /// <typeparam name="T">The generic type specifier.</typeparam>
        /// <param name="value">The Count&lt;T&gt; value.</param>
        /// <returns>The string representation of the Count&lt;T&gt; value.</returns>
        public override string Format<T>(Count<T>? value)
        {
            if (value == null)
            {
                return string.Empty;
            }
            else
            {
                int count = this.NumericConverter.Format(value);
                return this.Converter.ToCountString(count);
            }
        }

        /// <summary>
        /// Format a Money value as a string.
        /// </summary>
        /// <param name="value">The Money value.</param>
        /// <returns>The string representation of the Money value.</returns>
        public override string Format(Money? value)
        {
            if (value == null)
            {
                return string.Empty;
            }
            else
            {
                decimal numeric = this.NumericConverter.Format(value);
                return this.Converter.ToMoneyString(numeric, FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// Format a Percentage value as a string.
        /// </summary>
        /// <param name="value">The Percentage value.</param>
        /// <returns>The string representation of the Percentage value.</returns>
        public override string Format(Percentage? value)
        {
            if (value == null)
            {
                return string.Empty;
            }
            else
            {
                decimal numeric = this.NumericConverter.Format(value);
                return this.Converter.ToRateString(numeric);
            }
        }

        /// <summary>
        /// Format an UnzonedDate value as a string.
        /// </summary>
        /// <param name="value">The UnzonedDate value.</param>
        /// <returns>The string representation of the UnzonedDate value.</returns>
        public override string Format(UnzonedDate? value)
        {
            if (value == null)
            {
                return string.Empty;
            }
            else
            {
                var dt = value.Value.Date;
                var cdt = CDateTime.Create(dt);
                return this.Converter.ToDateTimeString(cdt);
            }
        }

        /// <summary>
        /// Format a PhoneNumber value as a string.
        /// </summary>
        /// <param name="value">The PhoneNumber value.</param>
        /// <returns>The string representation of the PhoneNumber value.</returns>
        public override string Format(PhoneNumber? value)
        {
            if (value == null)
            {
                return string.Empty;
            }

            return this.Converter.ToPhoneNumFormat(value.ToString(), FormatDirection.ToRep);
        }

        /// <summary>
        /// Formats a string-based count as a string.
        /// </summary>
        /// <param name="value">The count to format.</param>
        /// <returns>The string representation of the string-based count.</returns>
        public override string Format(CountString? value)
        {
            return this.Converter.ToCountVarCharStringRep(value?.ToString() ?? string.Empty);
        }

        /// <summary>
        /// Formats the remaining months for a liability as a string.
        /// </summary>
        /// <param name="value">The remaining months to format.</param>
        /// <returns>The string representation of the remaining months.</returns>
        public override string Format(LiabilityRemainingMonths? value)
        {
            return this.Converter.ToCountVarCharStringRep(value?.ToString() ?? string.Empty);
        }
    }
}
