﻿namespace DataAccess
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using LendersOffice.Common;
    using LendingQB.Core;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Shim for the IRecordCollection interface.
    /// </summary>
    /// <typeparam name="TValueKind">The kind of data held in the collection.</typeparam>
    /// <typeparam name="TValue">The class that contains the data.</typeparam>
    /// <remarks>
    /// Here is how the underlying collection works:
    /// m_ds holds all the data, both regular and special records
    /// m_sortedList hold regular records, sorted; the special records are held by type-specific collections are managed separately
    /// When a record is retrieved by position, the position is an index into m_sortedList for regular records, or a constructed array for special records
    /// Here is how we are implementing the shim:
    /// DataSet still holds all the data
    /// listRegular holds the regular records
    /// listSpecial holds the special records - type-specific collections can store the special records here.
    /// </remarks>
    public abstract class RecordCollectionShim<TValueKind, TValue> : IRemoveEntityShim
        where TValueKind : DataObjectKind
    {
        /// <summary>
        /// Maintain an ordered list of regular records.
        /// </summary>
        private List<ICollectionItemBase2> listRegular;

        /// <summary>
        /// Maintain an ordered list of special records.
        /// </summary>
        private List<ICollectionItemBase2> listSpecial;

        /// <summary>
        /// Initializes a new instance of the <see cref="RecordCollectionShim{TValueKind, TValue}"/> class.
        /// </summary>
        /// <param name="shimContainer">Reference to the container that holds all shims.</param>
        /// <param name="appId">The legacy application id.</param>
        /// <param name="specialHandler">An object used to determine whether shim objects are special.</param>
        /// <param name="converter">An instance of LosConvert used for formatting the data.</param>
        protected RecordCollectionShim(
            IShimContainer shimContainer,
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId,
            IDetermineSpecialEntity specialHandler,
            LosConvert converter)
        {
            this.ShimContainer = shimContainer;
            this.AppId = appId.Value;
            this.SpecialHandler = specialHandler;
            this.Converter = converter;

            this.DataSet = new DataSet("XmlTable");
        }

        /// <summary>
        /// Gets the number of regular records.
        /// </summary>
        public int CountRegular
        {
            get
            {
                return this.listRegular == null ? 0 : this.listRegular.Count;
            }
        }

        /// <summary>
        /// Gets the number of special records that are to be exposed as such.
        /// </summary>
        public int CountSpecial
        {
            get
            {
                return this.listSpecial == null ? 0 : this.listSpecial.Count;
            }
        }

        /// <summary>
        /// Gets the data sorted into a DataView instance for binding to GUI controls.
        /// </summary>
        public DataView SortedView
        {
            get
            {
                DataTable table = this.DataSet.Tables[0];

                DataView view = table.DefaultView;

                if (!string.IsNullOrEmpty(this.SortedViewRowFilter))
                {
                    view.RowFilter = this.SortedViewRowFilter;
                }

                view.Sort = "OrderRankValue";
                return view;
            }
        }

        /// <summary>
        /// Gets the where clause for the sorted view.
        /// </summary>
        /// <remarks>Some of the legacy collections exclude records from the sorted view.</remarks>
        protected virtual string SortedViewRowFilter => null;

        /// <summary>
        /// Gets the application id for the collection shim.
        /// </summary>
        protected Guid AppId { get; }

        /// <summary>
        /// Gets the regular items.
        /// </summary>
        protected List<ICollectionItemBase2> RegularRecords
        {
            get
            {
                if (this.listRegular == null)
                {
                    this.listRegular = new List<ICollectionItemBase2>();
                }

                return this.listRegular;
            }
        }

        /// <summary>
        /// Gets the special items.
        /// </summary>
        protected List<ICollectionItemBase2> SpecialRecords
        {
            get
            {
                if (this.listSpecial == null)
                {
                    this.listSpecial = new List<ICollectionItemBase2>();
                }

                return this.listSpecial;
            }
        }

        /// <summary>
        /// Gets the container that provides access to all the shims for the associated loan.
        /// </summary>
        protected IShimContainer ShimContainer { get; private set; }

        /// <summary>
        /// Gets the ordering for the contained collection.
        /// </summary>
        protected virtual IOrderedIdentifierCollection<TValueKind, Guid> Order
        {
            get
            {
                return this.ShimContainer.GetOrderForApplication<TValueKind>(this.AppId.ToIdentifier<DataObjectKind.LegacyApplication>());
            }
        }

        /// <summary>
        /// Gets the LosConvert instance.
        /// </summary>
        protected LosConvert Converter { get; private set; }

        /// <summary>
        /// Gets or sets a handler that can determine whether or not shims are special records.
        /// </summary>
        protected IDetermineSpecialEntity SpecialHandler { get; set; }

        /// <summary>
        /// Gets or sets a DataSet representation of all the data within this collection.
        /// </summary>
        private DataSet DataSet { get; set; }

        /// <summary>
        /// Gets or sets a callback method for the Flush method.
        /// </summary>
        private Action<DataSet> OnFlush { get; set; }

        /// <summary>
        /// Retrieve a property from the collection by id.
        /// </summary>
        /// <param name="recordId">The identifier for the property.</param>
        /// <returns>The property with the input identifier, or returns null if no such property is contained within the collection.</returns>
        public ICollectionItemBase2 GetRecordOf(Guid recordId)
        {
            if (recordId == Guid.Empty)
            {
                return null;
            }

            if (this.CountRegular > 0)
            {
                foreach (var item in this.RegularRecords)
                {
                    if (item.RecordId == recordId)
                    {
                        return item;
                    }
                }
            }

            if (this.CountSpecial > 0)
            {
                foreach (var item in this.SpecialRecords)
                {
                    if (item.RecordId == recordId)
                    {
                        return item;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Gets a value indicating whether the collection contains a record with the specified identifier.
        /// </summary>
        /// <remarks>
        /// This only considers records that are exposed as regular or special.  If there are special
        /// records that aren't exposed then they won't get picked up here.
        /// </remarks>
        /// <param name="recordId">The identifier.</param>
        /// <returns>True if the record exists, false otherwise.</returns>
        public bool HasRecordOf(Guid recordId)
        {
            return this.GetRecordOf(recordId) != null;
        }

        /// <summary>
        /// Get the data in the format of a DataSet.
        /// </summary>
        /// <returns>The data in the format of a DataSet.</returns>
        public DataSet GetDataSet()
        {
            return this.DataSet;
        }

        /// <summary>
        /// Ensures that the publicly retrievable data set accurately reflects the state of
        /// the collection shim.
        /// </summary>
        public void EnsureDataSetUpToDate()
        {
            this.RebuildShimData();
        }

        /// <summary>
        /// Create a data table with the data filtered by the given expression.
        /// </summary>
        /// <param name="strFilterExpression">The filter expression.</param>
        /// <returns>A data table with the requested records.</returns>
        public DataTable CloneFilteredTable(string strFilterExpression)
        {
            var ds = this.GetDataSet();
            var table = ds.Tables[0];
            var rows = table.Select(strFilterExpression);

            var fiteredTable = table.Clone();
            foreach (DataRow row in rows)
            {
                fiteredTable.ImportRow(row);
            }

            return fiteredTable;
        }

        /// <summary>
        /// Empty the collection.
        /// </summary>
        public void ClearAll()
        {
            var list = new List<DataObjectIdentifier<TValueKind, Guid>>();
            foreach (var id in this.GetAllCollectionEntities().Select(kvp => kvp.Key))
            {
                list.Add(id);
            }

            foreach (var id in list)
            {
                this.RemoveEntity(id);
            }

            this.ClearShimData();
        }

        /// <summary>
        /// Set a function that will be called during the flush operation.
        /// </summary>
        /// <param name="onFlush">The function that will be called during the flush operation.</param>
        public void SetFlushFunction(Action<DataSet> onFlush)
        {
            this.OnFlush = onFlush;
        }

        /// <summary>
        /// Flush the changes and prepare for saving.
        /// </summary>
        public void Flush()
        {
            if (this.listRegular != null)
            {
                foreach (var item in this.RegularRecords)
                {
                    item.PrepareToFlush();
                }
            }

            if (this.listSpecial != null)
            {
                foreach (var item in this.SpecialRecords)
                {
                    item.PrepareToFlush();
                }
            }

            this.EnsureDataSetUpToDate();
            this.HandleFlushData();

            this.OnFlush?.Invoke(this.DataSet);
        }

        /// <summary>
        /// Move a regular record up one spot in the order.
        /// </summary>
        /// <param name="record">The record that is to be moved.</param>
        public void MoveRegularRecordUp(ICollectionItemBase2 record)
        {
            int position = this.FindRegularPosition(record.RecordId);
            if (position > 0)
            {
                var identifier = DataObjectIdentifier<TValueKind, Guid>.Create(record.RecordId);
                this.Order.MoveForward(identifier);
                this.RebuildShimData();
            }
        }

        /// <summary>
        /// Move a regular record down one spot in the order.
        /// </summary>
        /// <param name="record">The record that is to be moved.</param>
        public void MoveRegularRecordDown(ICollectionItemBase2 record)
        {
            int position = this.FindRegularPosition(record.RecordId);
            if (position < this.CountRegular - 1)
            {
                var identifier = DataObjectIdentifier<TValueKind, Guid>.Create(record.RecordId);
                this.Order.MoveBack(identifier);
                this.RebuildShimData();
            }
        }

        /// <summary>
        /// Remove the shim from the shim collection.
        /// </summary>
        /// <param name="shim">The shim to remove.</param>
        public void Remove(ICollectionItemBase2 shim)
        {
            var identifier = DataObjectIdentifier<TValueKind, Guid>.Create(shim.RecordId);
            this.RemoveEntity(identifier);
            this.RebuildShimData();
        }

        /// <summary>
        /// Sort the records according to the specifed field name and direction.
        /// </summary>
        /// <param name="fieldName">The field to use for sorting.</param>
        /// <param name="directionAsc">True if using an ascending sort, false if using a descending sort.</param>
        /// <remarks>
        /// Only the regular records are sorted.  The special records get tacked on at the end.
        /// </remarks>
        public void SortRegularRecordsByKey(string fieldName, bool directionAsc)
        {
            // Don't do anything if there are no regular records
            int count = this.CountRegular;
            if (count == 0)
            {
                return;
            }

            var previousOrder = new DataObjectIdentifier<TValueKind, Guid>[this.Order.Count];
            var currentOrder = new DataObjectIdentifier<TValueKind, Guid>[this.Order.Count];

            for (int i = 0; i < this.Order.Count; ++i)
            {
                previousOrder[i] = this.Order.GetIdentifierAt(i);
            }

            // Order the regular records
            var keys = new string[count];
            var records = new ICollectionItemBase2[count];
            for (int i = 0; i < count; ++i)
            {
                records[i] = this.GetRegularItemAt(i);
                keys[i] = records[i].SortValueFor(fieldName);
            }

            Array.Sort(keys, records, new CXmlRecordCollection.CMoneyStringComparer(directionAsc));
            int index = 0;

            // Add the regular records to the order
            foreach (var item in records)
            {
                var identifier = DataObjectIdentifier<TValueKind, Guid>.Create(item.RecordId);
                currentOrder[index++] = identifier;
            }

            // Add the special records to the order
            if (this.listSpecial != null)
            {
                foreach (var item in this.SpecialRecords)
                {
                    var identifier = DataObjectIdentifier<TValueKind, Guid>.Create(item.RecordId);
                    currentOrder[index++] = identifier;
                }
            }

            this.Order.ReOrder(previousOrder, currentOrder);
            this.RebuildShimData();
        }

        /// <summary>
        /// Virtual method that specific collection shims may need to call to 
        /// process their data.
        /// </summary>
        protected virtual void HandleFlushData()
        {
            // do nothing at this level, some collections may need to override.
        }

        /// <summary>
        /// Retrieve the regular record based on the identifier.
        /// </summary>
        /// <param name="recordId">The identifier for the desired regular record.</param>
        /// <returns>The regular record, or throws an exception if it isn't found.</returns>
        protected ICollectionItemBase2 GetRegularRecordOf(Guid recordId)
        {
            int position = this.FindRegularPosition(recordId);
            return this.GetRegularItemAt(position);
        }

        /// <summary>
        /// Retrieve the regular record based on the identifier.
        /// </summary>
        /// <param name="recordId">The identifier for the desired regular record.</param>
        /// <returns>The regular record.</returns>
        protected ICollectionItemBase2 TryGetRegularRecordOf(Guid recordId)
        {
            if (this.listRegular != null)
            {
                foreach (var shim in this.RegularRecords)
                {
                    if (shim.RecordId == recordId)
                    {
                        return shim;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Adds a new record at the given index.
        /// </summary>
        /// <param name="item">The item that will be inserted.</param>
        /// <param name="pos">The index.</param>
        /// <returns>The newly added record.</returns>
        protected DataObjectIdentifier<TValueKind, Guid> InsertRegularRecordAt(TValue item, int pos)
        {
            if (pos > this.CountRegular || pos < 0)
            {
                throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, $"Invalid insertion position. ({nameof(pos)}={pos}) ");
            }

            int insertPosition = this.GetAllCollectionEntities().Count();
            var id = this.AddEntity(item);
            if (insertPosition > pos)
            {
                while (insertPosition-- > pos)
                {
                    this.Order.MoveForward(id);
                }
            }

            this.RebuildShimData();
            return id;
        }

        /// <summary>
        /// Rebuild the shim data after some change to the underlying collection.
        /// </summary>
        protected void RebuildShimData()
        {
            this.ClearShimData();
            this.BuildShimData();

            int count = 0;
            if (this.CountRegular > 0)
            {
                foreach (var shim in this.RegularRecords)
                {
                    shim.DataRow[CollectionItemBase2Shim.RecordIdKey] = shim.RecordId.ToString(CollectionItemBase2Shim.GuidFormatForDatabase);
                    shim.DataRow[CollectionItemBase2Shim.OrderRankValueKey] = (count++ * 100m).ToString();
                }
            }

            if (this.CountSpecial > 0)
            {
                foreach (var shim in this.SpecialRecords)
                {
                    shim.DataRow[CollectionItemBase2Shim.RecordIdKey] = shim.RecordId.ToString(CollectionItemBase2Shim.GuidFormatForDatabase);
                    shim.DataRow[CollectionItemBase2Shim.OrderRankValueKey] = (count++ * 100m).ToString();
                }
            }
        }

        /// <summary>
        /// Locate the position of the desired regular item within the collection.
        /// </summary>
        /// <param name="id">The identifier for the item of interest.</param>
        /// <returns>The position, or throws an exception if not found.</returns>
        protected int FindRegularPosition(Guid id)
        {
            int index = 0;
            if (this.listRegular != null)
            {
                foreach (var item in this.RegularRecords)
                {
                    if (item.RecordId == id)
                    {
                        return index;
                    }

                    index++;
                }
            }

            throw new CBaseException(
                LendersOffice.Common.ErrorMessages.Generic,
                $"Item not found in regular collection (id={id.ToString()}).");
        }

        /// <summary>
        /// Locate the position of the desired special item within the collection.
        /// </summary>
        /// <param name="id">The identifier for the item of interest.</param>
        /// <returns>The position, or throws an exception if not found.</returns>
        protected int FindSpecialPosition(Guid id)
        {
            int index = 0;
            if (this.listSpecial != null)
            {
                foreach (var item in this.SpecialRecords)
                {
                    if (item.RecordId == id)
                    {
                        return index;
                    }

                    index++;
                }
            }

            throw new CBaseException(
                LendersOffice.Common.ErrorMessages.Generic,
                $"Item not found in special collection (id={id.ToString()}).");
        }

        /// <summary>
        /// Retrieve the regular shim item at the input position.
        /// </summary>
        /// <param name="pos">The position of interest.</param>
        /// <returns>The shim item, or throws an exception if the position is out of bounds.</returns>
        protected ICollectionItemBase2 GetRegularItemAt(int pos)
        {
            if (pos < 0 || pos >= this.CountRegular)
            {
                // The exception type is what the legacy class would give.
                throw new IndexOutOfRangeException();
            }

            if (this.listRegular != null)
            {
                return this.RegularRecords[pos];
            }

            throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Null Regular list.");
        }

        /// <summary>
        /// Retrieve the special shim item at the input position.
        /// </summary>
        /// <param name="pos">The position of interest.</param>
        /// <returns>The shim item, or throws an exception if the position is out of bounds.</returns>
        protected ICollectionItemBase2 GetSpecialItemAt(int pos)
        {
            if (pos < 0 || pos >= this.CountSpecial)
            {
                // The exception type is what the legacy class would give.
                throw new IndexOutOfRangeException();
            }

            if (this.listSpecial != null)
            {
                return this.SpecialRecords[pos];
            }

            throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Null special list.");
        }

        /// <summary>
        /// Determine whether the input item is a special record.
        /// </summary>
        /// <param name="item">The item of interest.</param>
        /// <returns>True if the item is special, false if it is regular.</returns>
        protected bool IsSpecial(ICollectionItemBase2 item)
        {
            return this.SpecialHandler.IsSpecial(item);
        }

        /// <summary>
        /// Determine whether or not a particular record type is special.
        /// </summary>
        /// <param name="type">The record type.</param>
        /// <returns>True if the record type is determined to be a special record, false otherwise.</returns>
        protected bool IsSpecial(Enum type)
        {
            return this.SpecialHandler.IsSpecial(type);
        }

        /// <summary>
        /// Create a subcollection of items that have their type within or not within the indicated group.
        /// </summary>
        /// <param name="inclusiveGroup">Flag indicating whether the subcollection should contain items that have their type within the indicated group, or type not within the group.</param>
        /// <param name="group">The group of intereset.</param>
        /// <returns>The subcollection containing the desired items.</returns>
        protected ISubcollection GetSubcollectionCore(bool inclusiveGroup, Enum group)
        {
            int size = this.CountRegular + this.CountSpecial;
            ArrayList subcoll = new ArrayList(size);

            if (this.listRegular != null)
            {
                foreach (var item in this.RegularRecords)
                {
                    bool belong = this.BelongToTypeGroup(item.KeyType, group);
                    if (inclusiveGroup == belong)
                    {
                        subcoll.Add(item);
                    }
                }
            }

            if (this.listSpecial != null)
            {
                foreach (var item in this.SpecialRecords)
                {
                    bool belong = this.BelongToTypeGroup(item.KeyType, group);
                    if (inclusiveGroup == belong)
                    {
                        subcoll.Add(item);
                    }
                }
            }

            return CXmlRecordSubcollection.Create((ICollectionItemBase[])subcoll.ToArray(typeof(ICollectionItemBase)));
        }

        /// <summary>
        /// Build the shim data.
        /// </summary>
        protected abstract void BuildShimData();

        /// <summary>
        /// Adds the given record to the underlying collection via the aggregate.
        /// </summary>
        /// <param name="value">The record to add.</param>
        /// <returns>The identifier of the new record.</returns>
        protected abstract DataObjectIdentifier<TValueKind, Guid> AddEntity(TValue value);

        /// <summary>
        /// Adds the given id and record to the underlying collection via the aggregate.
        /// </summary>
        /// <param name="recordId">The id to use.</param>
        /// <param name="value">The record to add.</param>
        protected abstract void AddEntity(DataObjectIdentifier<TValueKind, Guid> recordId, TValue value);

        /// <summary>
        /// Removes a record from the underlying collection via the aggregate.
        /// </summary>
        /// <param name="recordId">The record id.</param>
        protected abstract void RemoveEntity(DataObjectIdentifier<TValueKind, Guid> recordId);

        /// <summary>
        /// Gets the collection records relevant to the shimmed application.
        /// </summary>
        /// <returns>The collection records relevant to the shimmed application.</returns>
        protected abstract IEnumerable<KeyValuePair<DataObjectIdentifier<TValueKind, Guid>, TValue>> GetAllCollectionEntities();

        /// <summary>
        /// Empty the shim data so that it can be rebuilt.
        /// </summary>
        private void ClearShimData()
        {
            this.listRegular = null;
            this.listSpecial = null;

            var ds = this.GetDataSet();
            var table = ds.Tables[0];
            table.Rows.Clear();
        }

        /// <summary>
        /// Determine whether the type belongs within the group.
        /// </summary>
        /// <param name="type">The type of interest.</param>
        /// <param name="group">The group of interest.</param>
        /// <returns>True if the type belongs within the group, false otherwise.</returns>
        private bool BelongToTypeGroup(Enum type, Enum group)
        {
            int expVal = (int)Math.Pow(2, int.Parse(type.ToString("D")));
            return ((int)(int.Parse(group.ToString("D")) & expVal)) != 0;
        }
    }
}
