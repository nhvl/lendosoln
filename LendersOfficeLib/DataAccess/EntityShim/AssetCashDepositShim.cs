﻿namespace DataAccess
{
    using System;
    using System.Data;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// A shim for an asset that is a cash deposit.
    /// </summary>
    public sealed class AssetCashDepositShim : AssetSpecialShim, IAssetCashDeposit
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AssetCashDepositShim"/> class.
        /// </summary>
        /// <param name="shimContainer">Reference to the container that holds all shims.</param>
        /// <param name="collectionShim">The shim for the collection that contains this entity shim.</param>
        /// <param name="contained">The Asset entity that holds the data.</param>
        /// <param name="ownershipManager">The ownership manager for the collection records.</param>
        /// <param name="recordId">The identifier for this asset.</param>
        /// <param name="stringFormatter">Object that controls how semantic types are serialized as strings.</param>
        /// <param name="numericFormatter">Object that controls how semantic types are converted to numeric values.</param>
        /// <param name="stringParser">Object that controls how strings are parsed into semantic types.</param>
        private AssetCashDepositShim(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            LendingQB.Core.Data.Asset contained,
            IShimOwnershipManager<Guid> ownershipManager,
            DataObjectIdentifier<DataObjectKind.Asset, Guid> recordId,
            IFormatAsString stringFormatter,
            IFormatAsNumeric numericFormatter,
            IParseFromString stringParser)
            : base(shimContainer, collectionShim, contained, ownershipManager, recordId, stringFormatter, numericFormatter, stringParser)
        {
            this.Contained.AssetType = E_AssetT.CashDeposit;
        }

        /// <summary>
        /// Gets or sets the type of the cash deposit.
        /// </summary>
        /// <value>The type of the cash deposit.</value>
        public E_AssetCashDepositT AssetCashDepositT
        {
            get
            {
                return this.Contained.AssetCashDepositType ?? E_AssetCashDepositT.CashDeposit1;
            }

            set
            {
                this.Contained.AssetCashDepositType = value;
            }
        }

        /// <summary>
        /// Create a shim implementation of the IAssetCashDeposit interface.
        /// </summary>
        /// <param name="shimContainer">Reference to the container that holds all shims.</param>
        /// <param name="collectionShim">The shim for the collection that contains this entity shim.</param>
        /// <param name="contained">The contained asset data.</param>
        /// <param name="ownershipManager">The ownership manager for the collection records.</param>
        /// <param name="recordId">The identifier for this asset.</param>
        /// <param name="converter">Converter to use.</param>
        /// <returns>A shim for the IAssetCashDeposit interface.</returns>
        internal static IAssetCashDeposit Create(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            LendingQB.Core.Data.Asset contained,
            IShimOwnershipManager<Guid> ownershipManager,
            DataObjectIdentifier<DataObjectKind.Asset, Guid> recordId,
            LosConvert converter)
        {
            return new AssetCashDepositShim(
                shimContainer,
                collectionShim,
                contained,
                ownershipManager,
                recordId,
                new FormatEntityShimAsString(converter),
                new FormatAsNumericBase(),
                new ParseEntityShimFromString(converter));
        }

        /// <summary>
        /// Create a DataRow, populate it, and add it to the input table.
        /// </summary>
        /// <param name="table">The table to which the DataRow will be added.</param>
        /// <returns>The DataRow that was created.</returns>
        internal override DataRow CreateAndAddDataRow(DataTable table)
        {
            var row = base.CreateAndAddDataRow(table);
            row["AssetCashDepositT"] = ((int)this.AssetCashDepositT).ToString();
            return row;
        }
    }
}