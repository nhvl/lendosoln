﻿namespace DataAccess
{
    using System;
    using System.Data;
    using LendersOffice;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// A shim for an asset that is a regular asset.
    /// </summary>
    public sealed class AssetRegularShim : AssetShim, IAssetRegular
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AssetRegularShim"/> class.
        /// </summary>
        /// <param name="shimContainer">Reference to the container that holds all shims.</param>
        /// <param name="collectionShim">The shim for the collection that contains this entity shim.</param>
        /// <param name="appData">The app data.</param>
        /// <param name="contained">The Asset entity that holds the data.</param>
        /// <param name="ownershipManager">The ownership manager for the collection records.</param>
        /// <param name="recordId">The identifier for this asset.</param>
        /// <param name="stringFormatter">Object that controls how semantic types are serialized as strings.</param>
        /// <param name="numericFormatter">Object that controls how semantic types are converted to numeric values.</param>
        /// <param name="stringParser">Object that controls how strings are parsed into semantic types.</param>
        private AssetRegularShim(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            IShimAppDataProvider appData,
            LendingQB.Core.Data.Asset contained,
            IShimOwnershipManager<Guid> ownershipManager,
            DataObjectIdentifier<DataObjectKind.Asset, Guid> recordId,
            IFormatAsString stringFormatter,
            IFormatAsNumeric numericFormatter,
            IParseFromString stringParser)
            : base(shimContainer, collectionShim, contained, ownershipManager, recordId, stringFormatter, numericFormatter, stringParser)
        {
            // Defaults from CAssetRegular:
            if (!this.Contained.AssetType.HasValue)
            {
                this.Contained.AssetType = E_AssetT.Checking;
            }

            if (!this.Contained.AccountName.HasValue)
            {
                this.Contained.AccountName = DescriptionField.Create(appData.aBNm);
            }
        }

        /// <summary>
        /// Gets or sets the account name.
        /// </summary>
        public string AccNm
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.AccountName);
            }

            set
            {
                this.Contained.AccountName = this.StringParser.TryParseDescriptionField(value);
            }
        }

        /// <summary>
        /// Gets or sets the account number.
        /// </summary>
        public Sensitive<string> AccNum
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.AccountNum);
            }

            set
            {
                this.Contained.AccountNum = this.StringParser.TryParseBankAccountNumber(value.Value);
            }
        }

        /// <summary>
        /// Gets the asset type.
        /// </summary>
        public string AssetT_rep
        {
            get
            {
                return CalculatedFields.Asset.DisplayStringForAssetT(
                    (E_AssetRegularT)this.AssetT,
                    this.OtherTypeDesc);
            }
        }

        /// <summary>
        /// Gets or sets the Attention line.
        /// </summary>
        public string Attention
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.Attention);
            }

            set
            {
                this.Contained.Attention = this.StringParser.TryParseEntityName(value);
            }
        }

        /// <summary>
        /// Gets or sets the City.
        /// </summary>
        public string City
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.City);
            }

            set
            {
                this.Contained.City = this.StringParser.TryParseCity(value);
            }
        }

        /// <summary>
        /// Gets or sets the Company name.
        /// </summary>
        public string ComNm
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.CompanyName);
            }

            set
            {
                this.Contained.CompanyName = this.StringParser.TryParseEntityName(value);
            }
        }

        /// <summary>
        /// Gets or sets the Department Name.
        /// </summary>
        public string DepartmentName
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.DepartmentName);
            }

            set
            {
                this.Contained.DepartmentName = this.StringParser.TryParseDescriptionField(value);
            }
        }

        /// <summary>
        /// Gets or sets the Gift Source.
        /// </summary>
        public E_GiftFundSourceT GiftSource
        {
            get
            {
                return this.Contained.GiftSource.HasValue ?
                    this.Contained.GiftSource.Value
                    : E_GiftFundSourceT.Blank;
            }

            set
            {
                this.Contained.GiftSourceData = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the is see attachment is set.
        /// </summary>
        public bool IsSeeAttachment
        {
            get
            {
                return this.BooleanFromNullable(this.Contained.IsSeeAttachment);
            }

            set
            {
                this.Contained.IsSeeAttachment = value;
            }
        }

        /// <summary>
        /// Gets or sets the other type descrption.
        /// </summary>
        public string OtherTypeDesc
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.OtherTypeDesc);
            }

            set
            {
                this.Contained.OtherTypeDesc = this.StringParser.TryParseDescriptionField(value);
            }
    }

        /// <summary>
        /// Gets or sets the prepared date.
        /// </summary>
        public CDateTime PrepD
        {
            get
            {
                return this.UnzonedDateToCDateTime(this.Contained.PrepDate);
            }

            set
            {
                this.Contained.PrepDate = this.CDateTimeToUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the prepared date.
        /// </summary>
        public string PrepD_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.PrepDate);
            }

            set
            {
                this.Contained.PrepDate = this.StringParser.TryParseUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the street address.
        /// </summary>
        public string StAddr
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.StreetAddress);
            }

            set
            {
                this.Contained.StreetAddress = this.StringParser.TryParseStreetAddress(value);
            }
        }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        public string State
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.State);
            }

            set
            {
                this.Contained.State = this.StringParser.TryParseUnitedStatesPostalState(value);
            }
        }

        /// <summary>
        /// Gets or sets the verification expiration date.
        /// </summary>
        public CDateTime VerifExpD
        {
            get
            {
                return this.UnzonedDateToCDateTime(this.Contained.VerifExpiresDate);
            }

            set
            {
                this.Contained.VerifExpiresDate = this.CDateTimeToUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the  verification expiration date.
        /// </summary>
        public string VerifExpD_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.VerifExpiresDate);
            }

            set
            {
                this.Contained.VerifExpiresDate = this.StringParser.TryParseUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the verification has a signature.
        /// </summary>
        public bool VerifHasSignature
        {
            get
            {
                return this.BooleanFromNullable(this.Contained.VerifHasSignature);
            }
        }

        /// <summary>
        /// Gets or sets the verification received date.
        /// </summary>
        public CDateTime VerifRecvD
        {
            get
            {
                return this.UnzonedDateToCDateTime(this.Contained.VerifRecvDate);
            }

            set
            {
                this.Contained.VerifRecvDate = this.CDateTimeToUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the verification received date.
        /// </summary>
        public string VerifRecvD_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.VerifRecvDate);
            }

            set
            {
                this.Contained.VerifRecvDate = this.StringParser.TryParseUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the verification re-ordered date.
        /// </summary>
        public CDateTime VerifReorderedD
        {
            get
            {
                return this.UnzonedDateToCDateTime(this.Contained.VerifReorderedDate);
            }

            set
            {
                this.Contained.VerifReorderedDate = this.CDateTimeToUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the verification re-ordered date.
        /// </summary>
        public string VerifReorderedD_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.VerifReorderedDate);
            }

            set
            {
                this.Contained.VerifReorderedDate = this.StringParser.TryParseUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the verification sent date.
        /// </summary>
        public CDateTime VerifSentD
        {
            get
            {
                return this.UnzonedDateToCDateTime(this.Contained.VerifSentDate);
            }

            set
            {
                this.Contained.VerifSentDate = this.CDateTimeToUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the verification sent date.
        /// </summary>
        public string VerifSentD_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.VerifSentDate);
            }

            set
            {
                this.Contained.VerifSentDate = this.StringParser.TryParseUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets the verification signature Id.
        /// </summary>
        public string VerifSignatureImgId
        {
            get
            {
                return this.Contained.VerifSignatureImgId.HasValue ?
                    this.Contained.VerifSignatureImgId.Value.Value.ToString()
                    : string.Empty;
            }
        }

        /// <summary>
        /// Gets the verification signing employee id.
        /// </summary>
        public Guid VerifSigningEmployeeId
        {
            get
            {
                return this.Contained.VerifSigningEmployeeId.HasValue ?
                    this.Contained.VerifSigningEmployeeId.Value.Value
                    : Guid.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the zip code.
        /// </summary>
        public string Zip
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.Zip);
            }

            set
            {
                this.Contained.Zip = this.StringParser.TryParseZipcode(value);
            }
        }

        /// <summary>
        /// Gets or sets the asset type.
        /// </summary>
        E_AssetRegularT IAssetRegular.AssetT
        {
            get
            {
                return this.Contained.AssetType.HasValue ?
                    (E_AssetRegularT)this.Contained.AssetType.Value
                    : E_AssetRegularT.Checking;
            }

            set
            {
                this.Contained.AssetType = (E_AssetT)value;
            }
        }

        /// <summary>
        /// Create a shim implementation of the IAssetRegular interface.
        /// </summary>
        /// <param name="shimContainer">Reference to the container that holds all shims.</param>
        /// <param name="collectionShim">The shim for the collection that contains this entity shim.</param>
        /// <param name="appBase">The application data.</param>
        /// <param name="contained">The contained asset data.</param>
        /// <param name="ownershipManager">The ownership manager for the collection records.</param>
        /// <param name="recordId">The identifier for this asset.</param>
        /// <param name="converter">Converter to use.</param>
        /// <returns>A shim for the IAssetCashDeposit interface.</returns>
        public static new IAssetRegular Create(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            IShimAppDataProvider appBase,
            LendingQB.Core.Data.Asset contained,
            IShimOwnershipManager<Guid> ownershipManager,
            DataObjectIdentifier<DataObjectKind.Asset, Guid> recordId,
            LosConvert converter)
        {
            return new AssetRegularShim(
                shimContainer,
                collectionShim,
                appBase,
                contained,
                ownershipManager,
                recordId,
                new FormatEntityShimAsString(converter),
                new FormatAsNumericBase(),
                new ParseEntityShimFromString(converter));
        }

        /// <summary>
        /// Apply the Signature.
        /// </summary>
        /// <param name="employeeId">The employeeid.</param>
        /// <param name="imgId">The image key.</param>
        public void ApplySignature(Guid employeeId, string imgId)
        {
            var signer = DataObjectIdentifier<DataObjectKind.Employee, Guid>.Create(employeeId);
            var imageId = DataObjectIdentifier<DataObjectKind.Signature, Guid>.Create(Guid.Parse(imgId));

            this.Contained.SetVerifSignature(signer, imageId);
        }

        /// <summary>
        /// Clear the signature.
        /// </summary>
        public void ClearSignature()
        {
            this.Contained.ClearSignature();
        }

        /// <summary>
        /// Create a DataRow, populate it, and add it to the input table.
        /// </summary>
        /// <param name="table">The table to which the DataRow will be added.</param>
        /// <returns>The DataRow that was created.</returns>
        internal override DataRow CreateAndAddDataRow(DataTable table)
        {
            var row = base.CreateAndAddDataRow(table);
            row["AccNm"] = this.AccNm;
            row["AccNum"] = this.AccNum;
            row["Attention"] = this.Attention;
            row["City"] = this.City;
            row["ComNm"] = this.ComNm;
            row["DepartmentName"] = this.DepartmentName;
            row["GiftSource"] = ((int)this.GiftSource).ToString();
            row["IsSeeAttachment"] = this.IsSeeAttachment.ToString();
            row["OtherTypeDesc"] = this.OtherTypeDesc;
            row["PrepD"] = this.PrepD_rep;
            row["StAddr"] = this.StAddr;
            row["State"] = this.State;
            row["VerifExpD"] = this.VerifExpD_rep;
            row["VerifRecvD"] = this.VerifRecvD_rep;
            row["VerifReorderedD"] = this.VerifReorderedD_rep;
            row["VerifSentD"] = this.VerifSentD_rep;
            row["Zip"] = this.Zip;
            return row;
        }
    }
}