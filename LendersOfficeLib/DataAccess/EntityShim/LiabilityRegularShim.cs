﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics.CodeAnalysis;
    using System.Xml;
    using LendersOffice;
    using LendersOffice.Common;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// A shim for the regular liability.
    /// </summary>
    public sealed class LiabilityRegularShim : LiabilityShim, ILiabilityRegular
    {
        /// <summary>
        /// The defaults provider for the liability.
        /// </summary>
        private ILiabilityDefaultsProvider defaultsProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="LiabilityRegularShim"/> class.
        /// </summary>
        /// <param name="shimContainer">The container that holds all of the classes wrapped by the shims and allows accessing / updating data in the other collections.</param>
        /// <param name="collectionShim">The record collection.</param>
        /// <param name="contained">The contained entity.</param>
        /// <param name="defaultsProvider">The defaults for the liability.</param>
        /// <param name="ownershipManager">The ownership manager for the collection records.</param>
        /// <param name="recordId">The record id.</param>
        /// <param name="stringFormatter">The string formatter.</param>
        /// <param name="numericFormatter">The decimal formatter.</param>
        /// <param name="stringParser">The string parser.</param>
        private LiabilityRegularShim(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            LendingQB.Core.Data.Liability contained,
            ILiabilityDefaultsProvider defaultsProvider,
            IShimOwnershipManager<Guid> ownershipManager,
            DataObjectIdentifier<DataObjectKind.Liability, Guid> recordId,
            IFormatAsString stringFormatter,
            IFormatAsNumeric numericFormatter,
            IParseFromString stringParser)
            : base(shimContainer, collectionShim, contained, ownershipManager, recordId, stringFormatter, numericFormatter, stringParser)
        {
            this.defaultsProvider = defaultsProvider;

            // When regular liabilities are added in the legacy shim, they are added as
            // revolving by default. We will restore that value here, as it is critical
            // to prevent a regular record from defaulting to a debt type of Alimony.
            if (this.Contained.DebtType == null)
            {
                this.DebtT = E_DebtRegularT.Revolving;
            }
        }

        /// <summary>
        /// Gets or sets the account name -- the name of the debtor.
        /// </summary>
        /// <value>The name of the debtor.</value>
        public string AccNm
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.AccountName);
            }

            set
            {
                this.Contained.AccountName = this.StringParser.TryParseDescriptionField(value);
            }
        }

        /// <summary>
        /// Gets or sets the account number as a sensitive value.
        /// </summary>
        /// <value>The account number as a sensitive value.</value>
        public Sensitive<string> AccNum
        {
            get
            {
                return new Sensitive<string>(this.StringFormatter.Format(this.Contained.AccountNum));
            }

            set
            {
                this.Contained.AccountNum = this.StringParser.TryParseThirdPartyIdentifier(value.Value);
            }
        }

        /// <summary>
        /// Gets or sets the name of a person to whom attention is called.
        /// </summary>
        /// <value>The name of a person to whom attention is called.</value>
        public string Attention
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.Attention);
            }

            set
            {
                this.Contained.Attention = this.StringParser.TryParseEntityName(value);
            }
        }

        /// <summary>
        /// Gets or sets the year and make of an automobile when this liability is an auto loan.
        /// </summary>
        /// <value>The year and make of an automobile when this liability is an auto loan.</value>
        public string AutoYearMake
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.AutoYearMakeAsString);
            }

            set
            {
                this.Contained.AutoYearMakeAsString = this.StringParser.TryParseYearAsString(value);
            }
        }

        /// <summary>
        /// Gets or sets the balance for the liability.
        /// </summary>
        /// <value>The balance for the liability.</value>
        public decimal Bal
        {
            get
            {
                return this.NumericFormatter.Format(this.Contained.Bal);
            }

            set
            {
                this.Contained.Bal = Money.Create(value);
            }
        }

        /// <summary>
        /// Gets or sets a string representation of the balance for the liability.
        /// </summary>
        /// <value>A string representation of the balance for the liability.</value>
        public string Bal_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.Bal);
            }

            set
            {
                this.Contained.Bal = this.StringParser.TryParseMoney(value);
            }
        }

        /// <summary>
        /// Gets or sets the address of the company that is the listed creditor for this liability.
        /// </summary>
        /// <value>The address of the company that is the listed creditor for this liability.</value>
        public string ComAddr
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.CompanyAddress);
            }

            set
            {
                this.Contained.CompanyAddress = this.StringParser.TryParseStreetAddress(value);
            }
        }

        /// <summary>
        /// Gets or sets the city of the company that is the listed creditor for this liability.
        /// </summary>
        /// <value>The city of the company that is the listed creditor for this liability.</value>
        public string ComCity
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.CompanyCity);
            }

            set
            {
                this.Contained.CompanyCity = this.StringParser.TryParseCity(value);
            }
        }

        /// <summary>
        /// Gets or sets the fax number of the company that is the listed creditor for this liability.
        /// </summary>
        /// <value>The fax number of the company that is the listed creditor for this liability.</value>
        public string ComFax
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.CompanyFax);
            }

            set
            {
                this.Contained.CompanyFax = this.StringParser.TryParsePhoneNumber(value);
            }
        }

        /// <summary>
        /// Gets or sets the name of the company that is the listed creditor for this liability.
        /// </summary>
        /// <value>The name of the company that is the listed creditor for this liability.</value>
        public string ComNm
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.CompanyName);
            }

            set
            {
                this.Contained.CompanyName = this.StringParser.TryParseEntityName(value);
            }
        }

        /// <summary>
        /// Gets or sets the phone number of the company that is the listed creditor for this liability.
        /// </summary>
        /// <value>The phone number of the company that is the listed creditor for this liability.</value>
        public string ComPhone
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.CompanyPhone);
            }

            set
            {
                this.Contained.CompanyPhone = this.StringParser.TryParsePhoneNumber(value);
            }
        }

        /// <summary>
        /// Gets or sets the state of the company that is the listed creditor for this liability.
        /// </summary>
        /// <value>The state of the company that is the listed creditor for this liability.</value>
        public string ComState
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.CompanyState);
            }

            set
            {
                this.Contained.CompanyState = this.StringParser.TryParseUnitedStatesPostalState(value);
            }
        }

        /// <summary>
        /// Gets or sets the zipcode of the company that is the listed creditor for this liability.
        /// </summary>
        /// <value>The zipcode of the company that is the listed creditor for this liability.</value>
        public string ComZip
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.CompanyZip);
            }

            set
            {
                this.Contained.CompanyZip = this.StringParser.TryParseZipcode(value);
            }
        }

        /// <summary>
        /// Gets or sets the debt type.
        /// </summary>
        /// <value>The debt type.</value>
        public new E_DebtRegularT DebtT
        {
            get
            {
                return (E_DebtRegularT)base.DebtT;
            }

            set
            {
                if (this.DebtT.EqualsOneOf(E_DebtRegularT.Mortgage, E_DebtRegularT.Heloc) && 
                    !value.EqualsOneOf(E_DebtRegularT.Mortgage, E_DebtRegularT.Heloc))
                {
                    this.ShimContainer.RemoveRealPropertyAssociationForLiability(this.RecordId);
                }

                base.DebtT = (E_DebtT)value;
            }
        }

        /// <summary>
        /// Gets the string representation of the debt type.
        /// </summary>
        /// <value>The string representation of the debt type.</value>
        public string DebtT_rep
        {
            get
            {
                return CLiaRegular.DisplayStringOfDebtT(this.DebtT);
            }
        }

        /// <summary>
        /// Gets or sets the description of the liability.
        /// </summary>
        /// <value>The description of the liability.</value>
        public string Desc
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.Desc);
            }

            set
            {
                this.Contained.Desc = this.StringParser.TryParseDescriptionField(value);
            }
        }

        /// <summary>
        /// Gets or sets the string representation for the amount due.
        /// </summary>
        /// <value>The string representation for the amount due.</value>
        public string Due_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.DueInMonAsString);
            }

            set
            {
                this.Contained.DueInMonAsString = this.StringParser.TryParseCountString(value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this liability should be excluded from the underwriting.
        /// </summary>
        /// <value>A value indicating whether this liability should be excluded from the underwriting.</value>
        public bool ExcFromUnderwriting
        {
            get
            {
                return this.BooleanFromNullable(this.Contained.ExcludeFromUw);
            }

            set
            {
                this.Contained.ExcludeFromUw = value;
            }
        }

        /// <summary>
        /// Gets or sets nothing. Always throws a NotImplementedException.
        /// </summary>
        /// <exception cref="NotImplementedException">Always thrown.</exception>
        public decimal FullyIndexedPITIPayment
        {
            get
            {
                // This was for H4H, not bringing behavior over to the new Liability class.
                return 0;
            }

            set
            {
                // This was for H4H, not bringing behavior over to the new Liability class.
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this liability was included in a bankruptcy.
        /// </summary>
        /// <value>A value indicating whether this liability was included in a bankruptcy.</value>
        public bool IncInBankruptcy
        {
            get
            {
                return this.BooleanFromNullable(this.Contained.IncludeInBk);
            }

            set
            {
                this.Contained.IncludeInBk = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this liability was included in a foreclosure.
        /// </summary>
        /// <value>A value indicating whether this liability was included in a foreclosure.</value>
        public bool IncInForeclosure
        {
            get
            {
                return this.BooleanFromNullable(this.Contained.IncludeInFc);
            }

            set
            {
                this.Contained.IncludeInFc = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this liability was included in a repossession.
        /// </summary>
        /// <value>A value indicating whether this liability was included in a repossession.</value>
        public bool IncInReposession
        {
            get
            {
                return this.BooleanFromNullable(this.Contained.IncludeInReposession);
            }

            set
            {
                this.Contained.IncludeInReposession = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this liability represents an auto loan.
        /// </summary>
        /// <value>A value indicating whether this liability represents an auto loan.</value>
        public bool IsForAuto
        {
            get
            {
                return this.BooleanFromNullable(this.Contained.IsForAuto);
            }

            set
            {
                this.Contained.IsForAuto = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this liability is an FHA insured mortgage.
        /// </summary>
        /// <value>A value indicating whether this liability is an FHA insured mortgage.</value>
        public bool IsMortFHAInsured
        {
            get
            {
                return this.BooleanFromNullable(this.Contained.IsMtgFhaInsured);
            }

            set
            {
                this.Contained.IsMtgFhaInsured = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this liability is a piggy-back loan.
        /// </summary>
        /// <value>A value indicating whether this liability is a piggy-back loan.</value>
        public bool IsPiggyBack
        {
            get
            {
                return this.BooleanFromNullable(this.Contained.IsPiggyBack);
            }

            set
            {
                this.Contained.IsPiggyBack = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether there is an attachment referenced with further information about this liability.
        /// </summary>
        /// <value>A value indicating whether there is an attachment referenced with further information about this liability.</value>
        public bool IsSeeAttachment
        {
            get
            {
                return this.BooleanFromNullable(this.Contained.IsSeeAttachment, true);
            }

            set
            {
                this.Contained.IsSeeAttachment = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this liability is the first mortgage for the subject property.
        /// </summary>
        /// <value>A value indicating whether this liability is the first mortgage for the subject property.</value>
        public bool IsSubjectProperty1stMortgage
        {
            get
            {
                if (this.IsSubjectPropertyMortgage)
                {
                    return this.BooleanFromNullable(this.Contained.IsSp1stMtgData);
                }
                else
                {
                    return false;
                }
            }

            set
            {
                if (value)
                {
                    // 11/17/2009 dd - If value is true then we loop through all liability and mark other field false.
                    this.ShimContainer.ClearIsSp1stMtgDataForAllLiabilities();
                }

                this.Contained.IsSp1stMtgData = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this liability is a mortgage for the subject property.
        /// </summary>
        /// <value>A value indicating whether this liability is a mortgage for the subject property.</value>
        public bool IsSubjectPropertyMortgage
        {
            get
            {
                if (this.DebtT.EqualsOneOf(E_DebtRegularT.Mortgage, E_DebtRegularT.Heloc))
                {
                    Guid reoId = this.MatchedReRecordId;

                    if (reoId == Guid.Empty)
                    {
                        // 11/16/2009 dd - There is no REO associate with this mortgage tradeline. Default to true.
                        return true;
                    }
                    else
                    {
                        return this.ShimContainer.IsRealPropertySubjectProperty(reoId);
                    }
                }
                else
                {
                    return false; // This is not a mortgage tradeline.
                }
            }

            set
            {
                // If we are saying this is associated with the subject property liability
                // We need to either associate it with an existing subject property liability
                // or we need to create one.
                if (value)
                {
                    this.ShimContainer.AssociateLiabilityWithSubjectPropertyReo(this.RecordId);
                }
                else if (this.MatchedReRecordId != Guid.Empty && this.ShimContainer.IsRealPropertySubjectProperty(this.MatchedReRecordId))
                {
                    // If this is currently associated with the subject property REO
                    // then we need to delete the association.
                    this.ShimContainer.RemoveRealPropertyAssociationForLiability(this.RecordId);
                }

                // If we are setting this field to false and we are either not associated with
                // an REO or not associated with an REO marked as subject property, we don't
                // need to do anything.
            }
        }

        /// <summary>
        /// Gets or sets the string representation of the 30 day late indicator.
        /// </summary>
        /// <value>The string representation of the 30 day late indicator.</value>
        public string Late30_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.Late30AsString);
            }

            set
            {
                this.Contained.Late30AsString = this.StringParser.TryParseCountString(value);
            }
        }

        /// <summary>
        /// Gets or sets the string representation of the 60 day late indicator.
        /// </summary>
        /// <value>The string representation of the 60 day late indicator.</value>
        public string Late60_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.Late60AsString);
            }

            set
            {
                this.Contained.Late60AsString = this.StringParser.TryParseCountString(value);
            }
        }

        /// <summary>
        /// Gets or sets the string representation of the 90+ day late indicator.
        /// </summary>
        /// <value>The string representation of the 90+ day late indicator.</value>
        public string Late90Plus_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.Late90PlusAsString);
            }

            set
            {
                this.Contained.Late90PlusAsString = this.StringParser.TryParseCountString(value);
            }
        }

        /// <summary>
        /// Gets or sets the matched real estate record identifier.
        /// </summary>
        /// <value>The matched real estate record identifier.</value>
        public Guid MatchedReRecordId
        {
            get
            {
                if (!this.DebtT.EqualsOneOf(E_DebtRegularT.Mortgage, E_DebtRegularT.Heloc))
                {
                    return Guid.Empty;
                }

                return this.ShimContainer.GetRealPropertyIdLinkedToLiability(this.RecordId);
            }

            set
            {
                if (value == Guid.Empty)
                {
                    this.ShimContainer.RemoveRealPropertyAssociationForLiability(this.RecordId);
                }
                else
                {
                    this.ShimContainer.AssociateLiabilityWithRealProperty(this.RecordId, value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the original debt amount.
        /// </summary>
        /// <value>The original debt amount.</value>
        public decimal OrigDebtAmt
        {
            get
            {
                return this.NumericFormatter.Format(this.Contained.OriginalDebtAmt);
            }

            set
            {
                this.Contained.OriginalDebtAmt = Money.Create(value);
            }
        }

        /// <summary>
        /// Gets or sets the string representation of the original debt amount.
        /// </summary>
        /// <value>The string representation of the original debt amount.</value>
        public string OrigDebtAmt_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.OriginalDebtAmt);
            }

            set
            {
                this.Contained.OriginalDebtAmt = this.StringParser.TryParseMoney(value);
            }
        }

        /// <summary>
        /// Gets or sets the string representation of the original term.
        /// </summary>
        /// <value>The string representation of the original term.</value>
        public string OrigTerm_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.OriginalTermAsString);
            }

            set
            {
                this.Contained.OriginalTermAsString = this.StringParser.TryParseCountString(value);
            }
        }

        /// <summary>
        /// Gets or sets the amount required to payoff the debt.
        /// </summary>
        /// <value>The amount required to payoff the debt.</value>
        public decimal PayoffAmt
        {
            get
            {
                if (this.Contained.PayoffAmt == null)
                {
                    return this.PayoffAmtLckd ? this.PayoffAmt : this.Bal;
                }

                return this.NumericFormatter.Format(this.Contained.PayoffAmt);
            }

            set
            {
                this.Contained.PayoffAmtData = Money.Create(value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether there is an edit lock on the amount required to payoff the debt.
        /// </summary>
        /// <value>A value indicating whether there is an edit lock on the amount required to payoff the debt.</value>
        public bool PayoffAmtLckd
        {
            get
            {
                return this.BooleanFromNullable(this.Contained.PayoffAmtLocked);
            }

            set
            {
                this.Contained.PayoffAmtLocked = value;
            }
        }

        /// <summary>
        /// Gets or sets the string representation of the amount required to payoff the debt.
        /// </summary>
        /// <value>The string representation of the amount required to payoff the debt.</value>
        public string PayoffAmt_rep
        {
            get
            {
                // Use the payoff amount we calculate in the shim as opposed to the payoff amount from the entity.
                return this.StringFormatter.Format(Money.Create(this.PayoffAmt));
            }

            set
            {
                this.Contained.PayoffAmtData = this.StringParser.TryParseMoney(value);
            }
        }

        /// <summary>
        /// Gets or sets an indicator of when the debt is expected to be paid off relative to the loan's closing date.
        /// </summary>
        /// <value>An indicator of when the debt is expected to be paid off relative to the loan's closing date.</value>
        public E_Timing PayoffTiming
        {
            get
            {
                PayoffTiming? timing = CalculatedFields.Liability.PayoffTiming(
                    this.WillBePdOff,
                    this.PayoffTimingLckd,
                    this.GetEnum(this.Contained.PayoffTimingData, LendingQB.Core.Data.PayoffTiming.Blank),
                    this.defaultsProvider);

                if (timing == null)
                {
                    return E_Timing.Blank;
                }

                return (E_Timing)timing;
            }

            set
            {
                if (value == E_Timing.After_Closing)
                {
                    throw new CBaseException(
                        ErrorMessages.Generic,
                        "After Closing is not a valid value for the payoff timing of a liability. Accepted values: Blank, At_Closing, Before_Closing");
                }

                this.Contained.PayoffTimingData = (PayoffTiming)value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether there is an edit lock on the payoff timing indicator.
        /// </summary>
        /// <value>A value indicating whether there is an edit lock on the payoff timing indicator.</value>
        public bool PayoffTimingLckd
        {
            get
            {
                return this.BooleanFromNullable(this.Contained.PayoffTimingLocked);
            }

            set
            {
                this.Contained.PayoffTimingLockedData = value;
            }
        }

        /// <summary>
        /// Gets or sets the date when the liability was prepared.
        /// </summary>
        /// <value>The date when the liability was prepared.</value>
        public CDateTime PrepD
        {
            get
            {
                return this.UnzonedDateToCDateTime(this.Contained.PrepDate);
            }

            set
            {
                this.Contained.PrepDate = this.CDateTimeToUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the status for the debt's reconciliation.
        /// </summary>
        /// <value>The status for the debt's reconciliation.</value>
        public E_LiabilityReconcileStatusT ReconcileStatusT
        {
            get
            {
                return this.GetEnum(this.Contained.ReconcileStatusType, E_LiabilityReconcileStatusT.LoanFileOnly);
            }

            set
            {
                this.Contained.ReconcileStatusType = value;
            }
        }

        /// <summary>
        /// Gets or sets the rate.
        /// </summary>
        public decimal R
        {
            get
            {
                return (decimal)this.Contained.Rate;
            }

            set
            {
                this.Contained.Rate = Percentage.Create(value);
            }
        }

        /// <summary>
        /// Gets or sets the string representation for the R value.
        /// </summary>
        /// <value>The string representation for the R value.</value>
        public string R_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.Rate);
            }

            set
            {
                this.Contained.Rate = this.StringParser.TryParsePercentage(value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this liability should be used in the DTI calculation.
        /// </summary>
        /// <value>A value indicating whether this liability should be used in the DTI calculation.</value>
        public bool UsedInRatio
        {
            get
            {
                return !this.NotUsedInRatio;
            }

            set
            {
                this.NotUsedInRatio = !value;
            }
        }

        /// <summary>
        /// Gets or sets the verification date for this liability.
        /// </summary>
        /// <value>The verification date for this liability.</value>
        public CDateTime VerifExpD
        {
            get
            {
                return this.UnzonedDateToCDateTime(this.Contained.VerifExpiresDate);
            }

            set
            {
                this.Contained.VerifExpiresDate = this.CDateTimeToUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the string representation of the verification date for this liability.
        /// </summary>
        /// <value>The string representation of the verification date for this liability.</value>
        public string VerifExpD_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.VerifExpiresDate);
            }

            set
            {
                this.Contained.VerifExpiresDate = this.StringParser.TryParseUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets a value indicating whether there is a signature associated with the debt verification.
        /// </summary>
        /// <value>A value indicating whether there is a signature associated with the debt verification.</value>
        public bool VerifHasSignature
        {
            get
            {
                return this.BooleanFromNullable(this.Contained.VerifHasSignature);
            }
        }

        /// <summary>
        /// Gets or sets the date when the debt verification was received.
        /// </summary>
        /// <value>The date when the debt verification was received.</value>
        public CDateTime VerifRecvD
        {
            get
            {
                return this.UnzonedDateToCDateTime(this.Contained.VerifRecvDate);
            }

            set
            {
                this.Contained.VerifRecvDate = this.CDateTimeToUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the string representation for the date when the debt verification was received.
        /// </summary>
        /// <value>The string representation for the date when the debt verification was received.</value>
        public string VerifRecvD_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.VerifRecvDate);
            }

            set
            {
                this.Contained.VerifRecvDate = this.StringParser.TryParseUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the date when the debt verification was re-ordered.
        /// </summary>
        /// <value>The date when the debt verification was re-ordered.</value>
        public CDateTime VerifReorderedD
        {
            get
            {
                return this.UnzonedDateToCDateTime(this.Contained.VerifReorderedDate);
            }

            set
            {
                this.Contained.VerifReorderedDate = this.CDateTimeToUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the string representation for the date when the debt verification was re-ordered.
        /// </summary>
        /// <value>The string representation for the date when the debt verification was re-ordered.</value>
        public string VerifReorderedD_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.VerifReorderedDate);
            }

            set
            {
                this.Contained.VerifReorderedDate = this.StringParser.TryParseUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the date when the debt verification request was sent.
        /// </summary>
        /// <value>The date when the debt verification request was sent.</value>
        public CDateTime VerifSentD
        {
            get
            {
                return this.UnzonedDateToCDateTime(this.Contained.VerifSentDate);
            }

            set
            {
                this.Contained.VerifSentDate = this.CDateTimeToUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the string representation for the date when the debt verification request was sent.
        /// </summary>
        /// <value>The string representation for the date when the debt verification request was sent.</value>
        public string VerifSentD_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.VerifSentDate);
            }

            set
            {
                this.Contained.VerifSentDate = this.StringParser.TryParseUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets the identifier for the image that holds the signature associated with the debt verification.
        /// </summary>
        /// <value>The identifier for the image that holds the signature associated with the debt verification.</value>
        public string VerifSignatureImgId
        {
            get
            {
                return this.Contained.VerifSignatureImgId?.Value.ToString() ?? string.Empty;
            }
        }

        /// <summary>
        /// Gets the identifier for the employee who issued the debt verification.
        /// </summary>
        /// <value>The identifier for the employee who issued the debt verification.</value>
        public Guid VerifSigningEmployeeId
        {
            get
            {
                return this.Contained.VerifSigningEmployeeId?.Value ?? Guid.Empty;
            }
        }

        /// <summary>
        /// Attach a signature image to this liability.
        /// </summary>
        /// <param name="employeeId">The identifier for the employee who's signature is contained in the image.</param>
        /// <param name="imgId">The identifier for the image.</param>
        public void ApplySignature(Guid employeeId, string imgId)
        {
            this.Contained.SetVerifSignature(
                DataObjectIdentifier<DataObjectKind.Employee, Guid>.Create(employeeId),
                DataObjectIdentifier<DataObjectKind.Signature, Guid>.Create(Guid.Parse(imgId)));
        }

        /// <summary>
        /// Remove the signature image from this liability.
        /// </summary>
        public void ClearSignature()
        {
            this.Contained.ClearVerifSignature();
        }

        /// <summary>
        /// Create an audit item for a field change.
        /// </summary>
        /// <param name="userName">The name of the user making the field change.</param>
        /// <param name="loginName">The login of the user account that made the field change.</param>
        /// <param name="field">The name of the field that has changed.</param>
        /// <param name="value">The new value for the field that has changed.</param>
        public void FieldUpdateAudit(string userName, string loginName, string field, string value)
        {
            this.Audit(userName, loginName, "Field Update", field, value);
        }

        /// <summary>
        /// Perform a matching operation on the input account number and creditor name.
        /// </summary>
        /// <param name="accNumber">The account number for the liability that is checked for a match.</param>
        /// <param name="creditorName">The creditor name for the liability that is checked for a match.</param>
        /// <returns>True if the tradelines match, false otherwise.</returns>
        public bool IsSameTradeline(string accNumber, string creditorName)
        {
            return CalculatedFields.Liability.IsSameTradeline(
                this.AccNum.Value,
                this.ComNm,
                accNumber,
                creditorName);
        }

        /// <summary>
        /// Create an audit record that indicates that a liability record is created via user input.
        /// </summary>
        /// <param name="userName">The name of the user making creating the liability record.</param>
        /// <param name="loginName">The login of the user account that created the liability record.</param>
        public void LiabilityCreationAudit(string userName, string loginName)
        {
            this.Audit(userName, loginName, "Liability Creation", string.Empty, string.Empty);
        }

        /// <summary>
        /// Assign the record identifier for this liability.
        /// </summary>
        /// <param name="s">The record identifier for this liability.</param>
        public void SetRecordId(string s)
        {
            var newId = CLiaRegular.ConvertStringToGuid(s);
            this.ShimContainer.UpdateLiabilityId(this.RecordId, newId);
            this.UpdateRecordId(newId);
        }

        /// <summary>
        /// Retrieve a value based on the field name.
        /// </summary>
        /// <param name="fieldName">The field name used to locate the value.</param>
        /// <returns>The value associated with the field name.</returns>
        [SuppressMessage("LendingQBStyleCop.LendingQBCustomRules", "LB1001:DefaultSwitchCaseMustThrowException", Justification = "Copied legacy code out of whitelisted class. And I generally object to this custom rule -- especially when applied to types other than enums.")]
        public override string SortValueFor(string fieldName)
        {
            switch (fieldName)
            {
                case "DebtT":
                    return CLiaRegular.DisplayStringOfDebtT(this.DebtT);
                case "OwnerT":
                    return CLiaFields.DisplayStringOfOwnerT(this.OwnerT);
                case "WillBePdOff":
                    return CLiaRegular.DisplayWillBePdOff(this.WillBePdOff);
                case "NotUsedInRatio":                    
                    return CLiaRegular.DisplayUsedInRatio(!this.NotUsedInRatio); // This one seems strange, but it is the behavior from the legacy one.
                default:
                    return base.SortValueFor(fieldName);
            }
        }

        /// <summary>
        /// Gets a calculated count of the remaining months to pay off a liability, automatically calculating a value for revolving "(R)" and liabilities excluded for underwriting.
        /// This value is intended to be appropriate for the Loan Product Advisor export, but may be appropriate for other uses.
        /// </summary>
        /// <returns>The liability remaining months for LPA.</returns>
        public string GetRemainingMonthsForLpa()
        {
            if (this.RemainMons_raw.Equals("(R)", StringComparison.OrdinalIgnoreCase))
            {
                // Should return empty as of LPA v4.3.
                return string.Empty;
            }
            else if (string.IsNullOrEmpty(this.RemainMons_raw) && this.ExcFromUnderwriting)
            {
                if (this.Pmt == 0)
                {
                    return "0";
                }
                else
                {
                    return this.StringFormatter.Format(Count<UnitType.Month>.Create((int)Math.Ceiling(this.Bal / this.Pmt)));
                }
            }
            else if (string.IsNullOrEmpty(this.RemainMons_raw))
            {
                // If blank, don't presume 0.
                return string.Empty;
            }
            else
            {
                return this.StringFormatter.Format(Count<UnitType.Month>.Create(this.RemainMons_raw));
            }
        }

        /// <summary>
        /// Create a shim implementation of the ILiabilityRegular interface.
        /// </summary>
        /// <param name="shimContainer">The container that holds all shims.</param>
        /// <param name="collectionShim">The shim for the collection that contains this entity shim.</param>
        /// <param name="contained">The contained entity data.</param>
        /// <param name="defaultsProvider">The defaults for the liability.</param>
        /// <param name="ownershipManager">The ownership manager for the collection records.</param>
        /// <param name="recordId">The identifier for this record.</param>
        /// <param name="converter">Instance of LosConvert used to format types as strings.</param>
        /// <returns>A shim for the ILiabilityRegular interface.</returns>
        internal static ILiabilityRegular Create(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            LendingQB.Core.Data.Liability contained,
            ILiabilityDefaultsProvider defaultsProvider,
            IShimOwnershipManager<Guid> ownershipManager,
            DataObjectIdentifier<DataObjectKind.Liability, Guid> recordId,
            LosConvert converter)
        {
            return new LiabilityRegularShim(
                shimContainer,
                collectionShim,
                contained,
                defaultsProvider,
                ownershipManager,
                recordId,
                new FormatEntityShimAsString(converter),
                new FormatAsNumericBase(),
                new ParseEntityShimFromString(converter));
        }

        /// <summary>
        /// Sets the fields specific to this liability type to the data row.
        /// </summary>
        /// <param name="row">The data row.</param>
        protected override void SetFieldsToDataRow(DataRow row)
        {
            base.SetFieldsToDataRow(row);

            row["MatchedReRecordId"] = this.MatchedReRecordId.ToString();
            row["ComNm"] = this.ComNm;
            row["ComAddr"] = this.ComAddr;
            row["ComCity"] = this.ComCity;
            row["ComState"] = this.ComState;
            row["ComZip"] = this.ComZip;
            row["ComPhone"] = this.ComPhone;
            row["ComFax"] = this.ComFax;
            row["AccNum"] = this.AccNum.Value;
            row["AccNm"] = this.AccNm;
            row["Bal"] = this.Bal_rep;
            row["R"] = this.R_rep;
            row["OrigTerm"] = this.OrigTerm_rep;
            row["Due"] = this.Due_rep;
            row["IsPiggyBack"] = this.IsPiggyBack.ToString();
            row["Late30"] = this.Late30_rep;
            row["Late60"] = this.Late60_rep;
            row["Late90Plus"] = this.Late90Plus_rep;
            row["IncInReposession"] = this.IncInReposession.ToString();
            row["IncInBankruptcy"] = this.IncInBankruptcy.ToString();
            row["IncInForeclosure"] = this.IncInForeclosure.ToString();
            row["ExcFromUnderwriting"] = this.ExcFromUnderwriting.ToString();
            row["VerifSentD"] = this.VerifSentD?.ToString("MM/dd/yyyy") ?? string.Empty;
            row["VerifRecvD"] = this.VerifRecvD?.ToString("MM/dd/yyyy") ?? string.Empty;
            row["VerifExpD"] = this.VerifExpD?.ToString("MM/dd/yyyy") ?? string.Empty;
            row["VerifReorderedD"] = this.VerifReorderedD?.ToString("MM/dd/yyyy") ?? string.Empty;
            row["Desc"] = this.Desc;
            row["Attention"] = this.Attention;
            row["PrepD"] = this.PrepD?.ToString("MM/dd/yyyy") ?? string.Empty;
            row["IsSeeAttachment"] = this.IsSeeAttachment.ToString();
            row["OrigDebtAmt"] = this.OrigDebtAmt_rep;
            row["AutoYearMake"] = this.AutoYearMake;
            row["IsMortFHAInsured"] = this.IsMortFHAInsured.ToString();
            row["IsForAuto"] = this.IsForAuto.ToString();
            row["ReconcileStatusT"] = ((int)this.ReconcileStatusT).ToString();
            row["IsSubjectProperty1stMortgage"] = this.IsSubjectProperty1stMortgage.ToString();
            row["FullyIndexedPITIPayment"] = string.Empty; // Hard-coded since this data isn't being moved over.
            row["VerifSigningEmployeeId"] = this.VerifSigningEmployeeId.ToString();
            row["VerifSignatureImgId"] = this.VerifSignatureImgId;
            row["PayoffAmtLckd"] = this.PayoffAmtLckd.ToString();
            row["PayoffAmt"] = this.PayoffAmt_rep;
            row["PayoffTimingLckd"] = this.PayoffTimingLckd.ToString();
            row["PayoffTiming"] = ((int)this.PayoffTiming).ToString();
        }

        /// <summary>
        /// Records an audit.
        /// </summary>
        /// <param name="userName">The user name.</param>
        /// <param name="loginName">The user's login.</param>
        /// <param name="action">The action to audit.</param>
        /// <param name="field">The field that was changed.</param>
        /// <param name="value">The value the field was changed to.</param>
        [SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "This is just to mimic what happens for the regular liability.")]
        private void Audit(string userName, string loginName, string action, string field, string value)
        {
            this.PmlAuditTrailXmlContent = CalculatedFields.Liability.Audit(
                this.PmlAuditTrailXmlContent,
                userName,
                loginName,
                action,
                field,
                value);
        }
    }
}
