﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using LendersOffice;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Shim that allows files using loan-level liabilities collections to work with the
    /// legacy interface.
    /// </summary>
    public class LiabilityCollectionShim : RecordCollectionShim<DataObjectKind.Liability, Liability>, ILiaCollection
    {
        /// <summary>
        /// The application associated with this collection.
        /// </summary>
        private IShimAppDataProvider app;

        /// <summary>
        /// The defaults provider for liabilities.
        /// </summary>
        private ILiabilityDefaultsProvider defaultsProvider;

        /// <summary>
        /// The ownership manager for the liabilities.
        /// </summary>
        private IShimOwnershipManager<Guid> ownershipManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="LiabilityCollectionShim"/> class.
        /// </summary>
        /// <param name="shimContainer">The aggregate to use when accessing other collections.</param>
        /// <param name="defaultsProvider">The liability defaults.</param>
        /// <param name="app">The application this shim is associated with.</param>
        /// <param name="ownershipManager">The ownership manager for the collection records.</param>
        /// <param name="converter">The converter to use.</param>
        public LiabilityCollectionShim(
            IShimContainer shimContainer,
            ILiabilityDefaultsProvider defaultsProvider,
            IShimAppDataProvider app,
            IShimOwnershipManager<Guid> ownershipManager,
            LosConvert converter)
            : base(shimContainer, ownershipManager.AppId, new SpecialLiabilityDetector(), converter)
        {
            this.ownershipManager = ownershipManager;

            this.defaultsProvider = defaultsProvider;
            this.app = app;
            LiabilityShim.CreateAndAddDataTable(this.GetDataSet());
            this.RebuildShimData();
        }

        /// <summary>
        /// Gets the extra normal liabilities to print on 1003 pg 4 (additional page).
        /// </summary>
        public ISubcollection ExtraNormalLiabilities
        {
            get
            {
                var extra = this.RegularRecords
                    .Skip(7)
                    .Cast<ICollectionItemBase>();
                return CXmlRecordSubcollection.Create(extra.ToArray());
            }
        }

        /// <summary>
        /// Gets the extra normal liabilities to print on 1003 pg 5 (additional page) for the new 1003
        /// The new 1003 only printout 6 liabilities in normal page.
        /// </summary>
        public ISubcollection ExtraNormalLiabilitiesInNew1003
        {
            get
            {
                var extra = this.RegularRecords
                    .Skip(6)
                    .Cast<ICollectionItemBase>();
                return CXmlRecordSubcollection.Create(extra.ToArray());
            }
        }

        /// <summary>
        /// Gets the first 7 regular liabilities to print on 1003 pg 2.
        /// </summary>
        public ISubcollection First7NormalLiabilities
        {
            get
            {
                var firstSeven = this.RegularRecords
                    .Take(7)
                    .Cast<ICollectionItemBase>();

                return CXmlRecordSubcollection.Create(firstSeven.ToArray());
            }
        }

        /// <summary>
        /// Gets a value indicating whether there are more than 7 regular liabilities.
        /// </summary>
        public bool HasMoreThan7NormalLiablities
        {
            get
            {
                return this.CountRegular > 7;
            }
        }

        /// <summary>
        /// Filters out special liabilities.
        /// </summary>
        protected override string SortedViewRowFilter => CLiaCollection.SortedViewRowFilter;

        /// <summary>
        /// Adds a record of the specified debt type.
        /// </summary>
        /// <param name="recordT">The type of debt.</param>
        /// <returns>The new liability.</returns>
        public ICollectionItemBase2 AddRecord(Enum recordT)
        {
            if (this.IsSpecial(recordT))
            {
                return this.AddSpecialRecord(recordT);
            }

            var regularShim = this.AddRegularRecord();
            regularShim.KeyType = recordT;
            return regularShim;
        }

        /// <summary>
        /// Adds a regular liability to the collection.
        /// </summary>
        /// <returns>The regular liability that was added.</returns>
        public ILiabilityRegular AddRegularRecord()
        {
            var entity = this.GetEntityForNewRegularLiability();
            var id = this.AddEntity(entity);
            this.RebuildShimData();

            return this.GetRegRecordOf(id.Value);
        }

        /// <summary>
        /// Adds a regular liability at the specified index.
        /// </summary>
        /// <param name="pos">The index.</param>
        /// <returns>The liability that was added.</returns>
        public ILiabilityRegular AddRegularRecordAt(int pos)
        {
            var entity = this.GetEntityForNewRegularLiability();
            var id = this.InsertRegularRecordAt(entity, pos);

            return this.GetRegRecordOf(id.Value);
        }

        /// <summary>
        /// Always throws a NotImplementedException.
        /// </summary>
        /// <param name="inputModelDict">Input field model to update liabilities.</param> 
        /// <exception cref="NotImplementedException">Always thrown.</exception>
        public void BindFromObjectModel(Dictionary<string, object> inputModelDict)
        {
            // This is from the new UI that is being abandoned. Leaving not implemented.
            throw new NotImplementedException();
        }

        /// <summary>
        /// Attempts to create a job expense at the first available slot.
        /// </summary>
        /// <returns>The created job expense. Null if no slots were available.</returns>
        public ILiabilityJobExpense CreateNewJobExpenseAtFirstAvailSlot()
        {
            ILiabilityJobExpense jobExp = this.GetJobRelated1(false);
            if (jobExp == null)
            {
                return this.GetJobRelated1(true);
            }

            jobExp = this.GetJobRelated2(false);
            if (jobExp == null)
            {
                return this.GetJobRelated2(true);
            }

            return null;
        }

        /// <summary>
        /// If there is no record with the indicated identifier, create one.  Return the record with the indicated identifier.
        /// </summary>
        /// <param name="recordId">The identifier that the returned record should have.</param>
        /// <returns>The record with the specified identifier.</returns>
        public ICollectionItemBase2 EnsureRegularRecordOf(Guid recordId)
        {
            var shim = this.TryGetRegularRecordOf(recordId);
            if (shim == null)
            {
                var identifier = DataObjectIdentifier<DataObjectKind.Liability, Guid>.Create(recordId);
                var entity = this.GetEntityForNewRegularLiability();
                this.AddEntity(identifier, entity);
                this.RebuildShimData();

                shim = this.GetRegRecordOf(recordId);
            }

            return shim;
        }

        /// <summary>
        /// Gets the alimony liability.
        /// </summary>
        /// <param name="forceCreate">
        /// A value indicating whether the liability should be created if it doesn't already exist.
        /// </param>
        /// <returns>The alimony liability if it exists or was force-created. Null otherwise.</returns>
        public ILiabilityAlimony GetAlimony(bool forceCreate)
        {
            Func<ILiabilityAlimony> getAlimonyOrNull = () => 
                (ILiabilityAlimony)this.SpecialRecords
                    .Cast<ILiabilitySpecial>()
                    .SingleOrDefault(lia => lia.DebtT == E_DebtSpecialT.Alimony);

            var alimony = getAlimonyOrNull();

            if (alimony == null && forceCreate)
            {
                var entity = new Liability(this.defaultsProvider);
                entity.DebtType = E_DebtT.Alimony;

                var id = this.AddEntity(entity);
                this.RebuildShimData();

                alimony = getAlimonyOrNull();
            }

            return alimony;
        }

        /// <summary>
        /// Gets the child support liability.
        /// </summary>
        /// <param name="forceCreate">
        /// A value indicating whether the liability should be created if it doesn't already exist.
        /// </param>
        /// <returns>The child support liability if it exists or was force-created. Null otherwise.</returns>
        public ILiabilityChildSupport GetChildSupport(bool forceCreate)
        {
            Func<ILiabilityChildSupport> getChildSupportOrNull = () => 
                (ILiabilityChildSupport)this.SpecialRecords
                    .Cast<ILiabilitySpecial>()
                    .SingleOrDefault(lia => lia.DebtT == E_DebtSpecialT.ChildSupport);
            var childSupport = getChildSupportOrNull();

            if (childSupport == null && forceCreate)
            {
                var entity = new Liability(this.defaultsProvider);
                entity.DebtType = E_DebtT.ChildSupport;

                var id = this.AddEntity(entity);
                this.RebuildShimData();

                childSupport = getChildSupportOrNull();
            }

            return childSupport;
        }

        /// <summary>
        /// Gets the job expense #1 liability.
        /// </summary>
        /// <param name="forceCreate">
        /// A value indicating whether the liability should be created if it doesn't already exist.
        /// </param>
        /// <returns>The job expense #1 if it exists or was force-created. Null otherwise.</returns>
        public ILiabilityJobExpense GetJobRelated1(bool forceCreate)
        {
            Func<ILiabilityJobExpense> getJobExpense1OrNull = () => this.SpecialRecords
                .Cast<ILiabilitySpecial>()
                .Where(lia => lia.DebtT == E_DebtSpecialT.JobRelatedExpense)
                .Cast<ILiabilityJobExpense>()
                .SingleOrDefault(lia => lia.DebtJobExpenseT == E_DebtJobExpenseT.JobExpense1);

            var jobExpense1 = getJobExpense1OrNull();

            if (jobExpense1 == null && forceCreate)
            {
                var entity = new Liability(this.defaultsProvider);
                entity.DebtType = E_DebtT.JobRelatedExpense;

                var id = this.AddEntity(entity);
                this.RebuildShimData();

                jobExpense1 = getJobExpense1OrNull();
            }

            return jobExpense1;
        }

        /// <summary>
        /// Gets the job expense #2 liability.
        /// </summary>
        /// <param name="forceCreate">
        /// A value indicating whether the liability should be created if it doesn't already exist.
        /// </param>
        /// <returns>The job expense #2 if it exists or was force-created. Null otherwise.</returns>
        public ILiabilityJobExpense GetJobRelated2(bool forceCreate)
        {
            Func<ILiabilityJobExpense> getJobExpense2OrNull = () => this.SpecialRecords
                .Cast<ILiabilitySpecial>()
                .Where(lia => lia.DebtT == E_DebtSpecialT.JobRelatedExpense)
                .Cast<ILiabilityJobExpense>()
                .SingleOrDefault(lia => lia.DebtJobExpenseT == E_DebtJobExpenseT.JobExpense2);

            var jobExpense2 = getJobExpense2OrNull();

            if (jobExpense2 == null && forceCreate)
            {
                // If we're force creating job expense two, we want to ensure that we also
                // have a job expense 1.
                this.GetJobRelated1(true);

                var entity = new Liability(this.defaultsProvider);
                entity.DebtType = E_DebtT.JobRelatedExpense;

                var id = this.AddEntity(entity);
                this.RebuildShimData();

                jobExpense2 = getJobExpense2OrNull();
            }

            return jobExpense2;
        }

        /// <summary>
        /// Gets a regular liability by id.
        /// </summary>
        /// <param name="recordId">The record id.</param>
        /// <returns>The liability with the specified id.</returns>
        public ILiabilityRegular GetRegRecordOf(Guid recordId)
        {
            return (ILiabilityRegular)this.GetRegularRecordOf(recordId);
        }

        /// <summary>
        /// Gets the regular liability at the specified index.
        /// </summary>
        /// <param name="pos">The index.</param>
        /// <returns>The regular liability at the index.</returns>
        public ILiabilityRegular GetRegularRecordAt(int pos)
        {
            return (ILiabilityRegular)this.GetRegularItemAt(pos);
        }

        /// <summary>
        /// Gets the special liability at the specified index.
        /// </summary>
        /// <param name="i">The index.</param>
        /// <returns>The special liability at the index.</returns>
        public ILiabilitySpecial GetSpecialRecordAt(int i)
        {
            return (ILiabilitySpecial)this.GetSpecialItemAt(i);
        }

        /// <summary>
        /// Gets a subset of the liabilities.
        /// </summary>
        /// <param name="inclusiveGroup">
        /// A value indicating whether the specified groups should be included or excluded.
        /// </param>
        /// <param name="group">The liability groups to either include or exclude.</param>
        /// <returns>The specified subset of the liabilities.</returns>
        public ISubcollection GetSubcollection(bool inclusiveGroup, E_DebtGroupT group)
        {
            return this.GetSubcollectionCore(inclusiveGroup, group);
        }

        /// <summary>
        /// Always throws a NotImplementedException.
        /// </summary>
        /// <returns>Nothing, always throws.</returns>
        /// <exception cref="NotImplementedException">Always thrown.</exception>
        public Dictionary<string, object> ToInputModel()
        {
            // This is from the new UI that is being abandoned. Leaving not implemented.
            throw new NotImplementedException();
        }

        /// <summary>
        /// Create and add a new record.
        /// </summary>
        /// <returns>The new record.</returns>
        ICollectionItemBase2 IRecordCollection.AddRegularRecord()
        {
            return this.AddRegularRecord();
        }

        /// <summary>
        /// Create and add a new record at the specified position.
        /// </summary>
        /// <param name="pos">The position into which the record should be inserted.</param>
        /// <returns>The new record.</returns>
        ICollectionItemBase2 IRecordCollection.AddRegularRecordAt(int pos)
        {
            return this.AddRegularRecordAt(pos);
        }

        /// <summary>
        /// Retrieve the record with the specified identifier.
        /// </summary>
        /// <param name="recordId">The identifier for the desired record.</param>
        /// <returns>The desired record, or throw an exception.</returns>
        ICollectionItemBase2 IRecordCollection.GetRegRecordOf(Guid recordId)
        {
            return this.GetRegRecordOf(recordId);
        }

        /// <summary>
        /// Retrieve the regular record at the specified position.
        /// </summary>
        /// <param name="pos">The position.</param>
        /// <returns>The record at the specified position, or throw an out of bounds exception.</returns>
        ICollectionItemBase2 IRecordCollection.GetRegularRecordAt(int pos)
        {
            return this.GetRegularRecordAt(pos);
        }

        /// <summary>
        /// Retrieve the special record at the specified position.
        /// </summary>
        /// <param name="i">The position.</param>
        /// <returns>The record at the specified position, or throw an out of bounds exception.</returns>
        ICollectionItemBase2 IRecordCollection.GetSpecialRecordAt(int i)
        {
            return this.GetSpecialRecordAt(i);
        }

        /// <summary>
        /// Flush liability totals to the associated application.
        /// </summary>
        protected override void HandleFlushData()
        {
            decimal totBal = 0; // aLiaBalTot
            decimal totMon = 0; // aLiaMonTot
            decimal totPdOff = 0; // aLiaPdOffTot

            int count = this.CountRegular;
            for (int i = 0; i < count; ++i)
            {
                ILiabilityRegular rec = this.GetRegularRecordAt(i);

                // OPM 143988: DT - Don't include liabilities marked as excluded or that are
                //                  associated with sold REO records in the balance totals. At 
                //                  some point, force liabilities associated with sold REOs to
                //                  be excluded, and only check the exclusion field here.
                bool isAssociatedWithSoldReo = false;

                if (rec.MatchedReRecordId != Guid.Empty)
                {
                    isAssociatedWithSoldReo = this.ShimContainer.IsRealPropertySold(rec.MatchedReRecordId);
                }

                if (!isAssociatedWithSoldReo && !rec.ExcFromUnderwriting)
                {
                    totBal += rec.Bal;

                    if (rec.WillBePdOff && rec.PayoffTiming == E_Timing.At_Closing)
                    {
                        totPdOff += rec.PayoffAmt;
                    }
                }

                if (!rec.NotUsedInRatio)
                {
                    totMon += rec.Pmt;
                }
            }

            count = this.CountSpecial;
            for (int i = 0; i < count; ++i)
            {
                var rec = this.GetSpecialRecordAt(i);

                if (!rec.NotUsedInRatio)
                {
                    totMon += rec.Pmt;
                }
            }

            // OPM 174375.  Need to sneak in a special liability that is the
            // non-primary non-rental retained property to conform to DU.
            decimal totRetainedNegCf = this.app.aBRetainedNegCf + this.app.aCRetainedNegCf;

            // The following lines will prepare the app data object ready to be saved
            // and all cached calculated values are updated to be used in ratio calculations etc...
            // Need to set these values to the app.
            var aggregateData = new CalculatedFields.Liability.AggregateData(
                totBal,
                totMon + totRetainedNegCf,
                totPdOff);

            this.app.AcceptLiabilityAggregateTotals(aggregateData);
        }

        /// <summary>
        /// Build the shim data.
        /// </summary>
        protected override void BuildShimData()
        {
            var ds = this.GetDataSet();
            var table = ds.Tables[0];

            // If we create a job expense, this variable is incremented to JobExpense2
            var jobExpenseType = E_DebtJobExpenseT.JobExpense1;

            foreach (var idEntityPair in this.GetAllCollectionEntities())
            {
                // Note that the key value pairs should be returned in the correct order because
                // the collection should be pre-ordered for us.
                var id = idEntityPair.Key;
                var entity = idEntityPair.Value;
                ILiability shim = null;

                switch (entity.DebtType)
                {
                    case E_DebtT.Alimony:
                        shim = LiabilityAlimonyShim.Create(this.ShimContainer, this, entity, this.ownershipManager, id, this.Converter);
                        break;
                    case E_DebtT.ChildSupport:
                        shim = LiabilityChildSupportShim.Create(this.ShimContainer, this, entity, this.ownershipManager, id, this.Converter);
                        break;
                    case E_DebtT.JobRelatedExpense:
                        shim = LiabilityJobExpenseShim.Create(this.ShimContainer, this, entity, this.ownershipManager, id, jobExpenseType, this.Converter);

                        if (jobExpenseType == E_DebtJobExpenseT.JobExpense1)
                        {
                            jobExpenseType = E_DebtJobExpenseT.JobExpense2;
                        }

                        break;
                    case E_DebtT.Installment:
                    case E_DebtT.Mortgage:
                    case E_DebtT.Open:
                    case E_DebtT.Other:
                    case E_DebtT.Revolving:
                    case E_DebtT.Lease:
                    case E_DebtT.OtherExpenseLiability:
                    case E_DebtT.SeparateMaintenance:
                    case E_DebtT.Heloc:
                    case null:
                        shim = LiabilityRegularShim.Create(this.ShimContainer, this, entity, this.defaultsProvider, this.ownershipManager, id, this.Converter);
                        break;
                    default:
                        throw new UnhandledEnumException(entity.DebtType);
                }

                if (this.IsSpecial(shim.DebtT))
                {
                    this.SpecialRecords.Add(shim);
                }
                else
                {
                    this.RegularRecords.Add(shim);
                }

                var lia = (LiabilityShim)shim;
                var row = lia.CreateAndAddDataRow(table);
            }

            this.ValidateSpecialRecords();
        }

        /// <summary>
        /// Adds the given record to the underlying collection via the aggregate.
        /// </summary>
        /// <param name="value">The record to add.</param>
        /// <returns>The identifier of the new record.</returns>
        protected override DataObjectIdentifier<DataObjectKind.Liability, Guid> AddEntity(Liability value)
        {
            return this.ShimContainer.Add(this.ownershipManager.DefaultOwner, value);
        }

        /// <summary>
        /// Adds the given id and record to the underlying collection via the aggregate.
        /// </summary>
        /// <param name="recordId">The id to use.</param>
        /// <param name="value">The record to add.</param>
        protected override void AddEntity(DataObjectIdentifier<DataObjectKind.Liability, Guid> recordId, Liability value)
        {
            this.ShimContainer.Add(this.ownershipManager.DefaultOwner, recordId, value);
        }

        /// <summary>
        /// Removes a record from the underlying collection via the aggregate.
        /// </summary>
        /// <param name="recordId">The record id.</param>
        protected override void RemoveEntity(DataObjectIdentifier<DataObjectKind.Liability, Guid> recordId)
        {
            this.ShimContainer.RemoveLiabilityFromLegacyApplication(recordId, this.ownershipManager.AppId, this.ownershipManager.BorrowerId, this.ownershipManager.CoborrowerId);
        }

        /// <summary>
        /// Gets the collection records relevant to the shimmed application.
        /// </summary>
        /// <returns>The collection records relevant to the shimmed application.</returns>
        protected override IEnumerable<KeyValuePair<DataObjectIdentifier<DataObjectKind.Liability, Guid>, Liability>> GetAllCollectionEntities()
        {
            return this.ShimContainer.GetLiabilitiesForLegacyApplication(this.ownershipManager.AppId, this.ownershipManager.BorrowerId, this.ownershipManager.CoborrowerId);
        }

        /// <summary>
        /// Adds a special record of the given type.
        /// </summary>
        /// <param name="type">The debt type.</param>
        /// <returns>The created special record.</returns>
        /// <exception cref="CBaseException">Thrown when the special record already exists.</exception>
        private ILiabilitySpecial AddSpecialRecord(Enum type)
        {
            E_DebtT debtType = (E_DebtT)type;
            ILiabilitySpecial special = null;

            switch (debtType)
            {
                case E_DebtT.Alimony:
                    special = this.GetAlimony(false);
                    if (special == null)
                    {
                        return this.GetAlimony(true);
                    }

                    break;
                case E_DebtT.JobRelatedExpense:
                    special = this.GetJobRelated1(false);
                    if (special == null)
                    {
                        return this.GetJobRelated1(true);
                    }

                    special = this.GetJobRelated2(false);
                    if (special == null)
                    {
                        return this.GetJobRelated2(true);
                    }

                    break;
                case E_DebtT.ChildSupport:
                    special = this.GetChildSupport(false);
                    if (special == null)
                    {
                        return this.GetChildSupport(true);
                    }

                    break;
                default:
                    throw new UnhandledEnumException(debtType);
            }

            throw new CBaseException(
                LendersOffice.Common.ErrorMessages.Generic,
                "Cannot add any more special liability record of the requested type = " + debtType.ToString());
        }

        /// <summary>
        /// Provides a new liability entity with the account name defaulted to the primary borrower's name.
        /// </summary>
        /// <returns>The new liability.</returns>
        private Liability GetEntityForNewRegularLiability()
        {
            var liability = new Liability(this.defaultsProvider);
            liability.AccountName = DescriptionField.Create(this.app.aBNm);
            liability.DebtType = E_DebtT.Revolving;
            return liability;
        }

        /// <summary>
        /// Ensures that we do not allow more than the expected number of special records
        /// for each special record type.
        /// </summary>
        /// <exeption cref="UnhandledEnumException">
        /// Thrown when a special record is of an unrecognized type.
        /// </exeption>
        /// <exception cref="CBaseException">
        /// Thrown when there are more special liabilities than were allowed with the legacy collection.
        /// </exception>
        private void ValidateSpecialRecords()
        {
            int numAlimony = 0;
            int numChildSupport = 0;
            int numJobExpense = 0;

            foreach (ILiabilitySpecial special in this.SpecialRecords)
            {
                switch (special.DebtT)
                {
                    case E_DebtSpecialT.Alimony:
                        numAlimony++;
                        break;
                    case E_DebtSpecialT.ChildSupport:
                        numChildSupport++;
                        break;
                    case E_DebtSpecialT.JobRelatedExpense:
                        numJobExpense++;
                        break;
                    default:
                        throw new UnhandledEnumException(special.DebtT);
                }
            }

            if (numAlimony > 1
                || numChildSupport > 1
                || numJobExpense > 2)
            {
                throw new CBaseException(
                    LendersOffice.Common.ErrorMessages.Generic,
                    "More liability special records than are allowed.");
            }
        }
    }
}
