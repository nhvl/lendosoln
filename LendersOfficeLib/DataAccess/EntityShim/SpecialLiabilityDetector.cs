﻿namespace DataAccess
{
    using System;

    /// <summary>
    /// Determines whether liability records are considered special.
    /// </summary>
    public class SpecialLiabilityDetector : IDetermineSpecialEntity
    {
        /// <summary>
        /// Determine whether or not a debt type is special.
        /// </summary>
        /// <param name="type">The debt type.</param>
        /// <returns>True if the debt type is determined to be a special record, false otherwise.</returns>
        public bool IsSpecial(Enum type)
        {
            var debtType = (E_DebtT)type;
            return CLiaFields.IsTypeOfSpecialRecord(debtType);
        }

        /// <summary>
        /// Determine whether or not a liability shim is a special record.
        /// </summary>
        /// <param name="shim">The liability shim of interest.</param>
        /// <returns>True if the shim is determined to be a special record, false otherwise.</returns>
        public bool IsSpecial(ICollectionItemBase2 shim)
        {
            var liability = shim as ILiability;

            if (liability == null)
            {
                throw new CBaseException(
                    LendersOffice.Common.ErrorMessages.Generic,
                    "No app-level primary ownership found.");
            }

            return this.IsSpecial(liability.DebtT);
        }
    }
}
