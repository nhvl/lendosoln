﻿namespace DataAccess
{
    using System;
    using System.Data;
    using System.Linq;
    using LendersOffice;
    using LendersOffice.UI;
    using LendingQB.Core.Mapping;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// A shim for the base liability class.
    /// </summary>
    public abstract class LiabilityShim : CollectionItemBase2Shim, ILiability, IRememberBorrowers
    {
        /// <summary>
        /// The table name to use for the DataTable. Mimics what is used by the legacy liability collection.
        /// </summary>
        private const string TableName = "LiaXmlContent";

        /// <summary>
        /// The ownership manager for the liability.
        /// </summary>
        private IShimOwnershipManager<Guid> ownershipManager;

        /// <summary>
        /// The mapper to use when converting the PML audit trail to/from XML.
        /// </summary>
        private LiabilityPmlAuditTrailMapper auditMapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="LiabilityShim"/> class.
        /// </summary>
        /// <param name="shimContainer">The container that holds all of the classes wrapped by the shims and allows accessing / updating data in the other collections.</param>
        /// <param name="collectionShim">The record collection.</param>
        /// <param name="contained">The contained entity.</param>
        /// <param name="ownershipManager">The ownership manager for the collection records.</param>
        /// <param name="recordId">The record id.</param>
        /// <param name="stringFormatter">The string formatter.</param>
        /// <param name="numericFormatter">The numeric formatter.</param>
        /// <param name="stringParser">The string parser.</param>
        protected LiabilityShim(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            LendingQB.Core.Data.Liability contained,
            IShimOwnershipManager<Guid> ownershipManager,
            DataObjectIdentifier<DataObjectKind.Liability, Guid> recordId,
            IFormatAsString stringFormatter,
            IFormatAsNumeric numericFormatter,
            IParseFromString stringParser)
            : base(shimContainer, collectionShim, recordId.Value, stringFormatter, numericFormatter, stringParser)
        {
            this.ownershipManager = ownershipManager;

            this.Contained = contained;
            this.auditMapper = new LiabilityPmlAuditTrailMapper();
        }

        /// <summary>
        /// Gets or sets the type of debt for the liability.
        /// </summary>
        /// <value>The type of debt for the liability.</value>
        public E_DebtT DebtT
        {
            get
            {
                return this.GetEnum(this.Contained.DebtType, E_DebtT.Other);
            }

            set
            {
                this.Contained.DebtType = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether... Always throws a NotImplementedException.
        /// </summary>
        /// <exception cref="NotImplementedException">Always thrown.</exception>
        public bool H4HIsRetirement
        {
            get
            {
                // This was for H4H, not bringing behavior over to the new Liability class.
                return false;
            }

            set
            {
                // This was for H4H, not bringing behavior over to the new Liability class.
            }
        }

        /// <summary>
        /// Gets or sets they type of key that will be used for this record.
        /// </summary>
        public Enum KeyType
        {
            get
            {
                return this.Contained.DebtType;
            }

            set
            {
                this.Contained.DebtType = (E_DebtT)value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this liability should not be used in calculating DTI.
        /// </summary>
        /// <value>A value indicating whether this liability should not be used in calculating DTI.</value>
        public bool NotUsedInRatio
        {
            get
            {
                return !this.BooleanFromNullable(this.Contained.UsedInRatio);
            }

            set
            {
                this.Contained.UsedInRatioData = !value;
            }
        }

        /// <summary>
        /// Gets or sets the owner type for this liability.
        /// </summary>
        /// <value>The owner type for this liability.</value>
        public E_LiaOwnerT OwnerT
        {
            get
            {
                return (E_LiaOwnerT)this.ownershipManager.GetOwnership(this.RecordId);
            }

            set
            {
                this.ownershipManager.SetOwnership(this.RecordId, (Ownership)value);
            }
        }

        /// <summary>
        /// Gets the string representation of the owner type for this liability.
        /// </summary>
        /// <value>The string representation of the owner type for this liability.</value>
        public string OwnerT_rep
        {
            get
            {
                return CLiaFields.DisplayStringOfOwnerT(this.OwnerT);
            }
        }

        /// <summary>
        /// Gets or sets the audit trail XML.
        /// </summary>
        /// <value>The audit trail XML.</value>
        public string PmlAuditTrailXmlContent
        {
            get
            {
                var stringValue = this.auditMapper.DatabaseRepresentation(this.Contained.PmlAuditTrail);
                return stringValue ?? string.Empty;
            }

            set
            {
                this.Contained.PmlAuditTrail = this.auditMapper.Create(value);
            }
        }

        /// <summary>
        /// Gets or sets the payment value.
        /// </summary>
        /// <value>The payment value.</value>
        public decimal Pmt
        {
            get
            {
                return this.NumericFormatter.Format(this.Contained.Pmt);
            }

            set
            {
                this.Contained.Pmt = Money.Create(value);
            }
        }

        /// <summary>
        /// Gets the string representation for remaining payment for form 1003.
        /// </summary>
        /// <value>The string representation for remaining payment for form 1003.</value>
        public string PmtRemainMons_rep1003
        {
            get
            {
                return CalculatedFields.Liability.PmtRemainMons_rep1003(this.Pmt_repPoint, this.RemainMons_rep, this.WillBePdOff);
            }
        }

        /// <summary>
        /// Gets or sets the string representation of the payment value.
        /// </summary>
        /// <value>The string representation of the payment value.</value>
        public string Pmt_rep
        {
            get
            {
                // Use the Pmt property of the shim to avoid issues where the underlying
                // entity's payment is null. We want to default to 0 to match the existing
                // behavior.
                // Note: There is an implicit cast from decimal to Money.
                return this.StringFormatter.Format(this.Pmt);
            }

            set
            {
                this.Contained.Pmt = this.StringParser.TryParseMoney(value);
            }
        }

        /// <summary>
        /// Gets or sets the string representation for the payment amount for Point.
        /// </summary>
        /// <value>The string representation for the payment amount for Point.</value>
        public string Pmt_repPoint
        {
            get
            {
                return CalculatedFields.Liability.Pmt_repPoint(this.NotUsedInRatio, this.Pmt, this.Pmt_rep);
            }

            set
            {
                string newPmtRep = CLiaFields.StripParens(value);
                this.Pmt_rep = newPmtRep;
                this.NotUsedInRatio = newPmtRep.Length < value.Length; // non included in ratio if there were "(" and ")", 
            }
        }

        /// <summary>
        /// Gets the string representation for the number of payments that remain.
        /// </summary>
        /// <value>The string representation for the number of payments that remain.</value>
        public string RemainMons_raw
        {
            get
            {
                return this.Contained.RemainMon.ToString();
            }
        }

        /// <summary>
        /// Gets or sets another string representation for the number of payments that remain.
        /// </summary>
        /// <value>Another string representation for the number of payments that remain.</value>
        public string RemainMons_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.RemainMon);
            }

            set
            {
                this.Contained.RemainMon = this.StringParser.TryParseLiabilityRemainingMonths(value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the liability is expected to be paid off.
        /// </summary>
        /// <value>A value indicating whether the liability is expected to be paid off.</value>
        public bool WillBePdOff
        {
            get
            {
                return this.BooleanFromNullable(this.Contained.WillBePdOff);
            }

            set
            {
                this.Contained.WillBePdOff = value;
            }
        }

        /// <summary>
        /// Gets the contained liability entity.
        /// </summary>
        protected LendingQB.Core.Data.Liability Contained { get; private set; }

        /// <summary>
        /// Method called to notify the implementing class that the roles of the 
        /// two borrowers have been exchanged.
        /// </summary>
        public void SwapBorrowers()
        {
            this.ownershipManager.SwapBorrowers();
        }

        /// <summary>
        /// Always throws a NotImplementedException.
        /// </summary>
        /// <param name="objInputFieldModel">This is never used.</param>
        /// <exception cref="NotImplementedException">Always thrown.</exception>
        public void BindFromObjectModel(SimpleObjectInputFieldModel objInputFieldModel)
        {
            // This is just for the new LoanController, which we aren't going to use.
            // I am leaving this unimplemented.
            throw new NotImplementedException();
        }

        /// <summary>
        /// Always throws a NotImplementedException.
        /// </summary>
        /// <returns>Nothing, always throws.</returns>
        /// <exception cref="NotImplementedException">Always thrown.</exception>
        public SimpleObjectInputFieldModel ToInputFieldModel()
        {
            // This is just for the new LoanController, which we aren't going to use.
            // I am leaving this unimplemented.
            throw new NotImplementedException();
        }

        /// <summary>
        /// Creates a DataTable for a liability and adds the table to the provided DataSet.
        /// </summary>
        /// <param name="dataSet">The data set.</param>
        /// <returns>The data table that was added to the data set.</returns>
        internal static DataTable CreateAndAddDataTable(DataSet dataSet)
        {
            var table = dataSet.Tables.Add(TableName);
            table.Columns.Add("RecordId");
            table.Columns.Add("MatchedReRecordId");
            table.Columns.Add("OwnerT");
            table.Columns.Add("DebtT");
            table.Columns.Add("ComNm");
            table.Columns.Add("ComAddr");
            table.Columns.Add("ComCity");
            table.Columns.Add("ComState");
            table.Columns.Add("ComZip");
            table.Columns.Add("ComPhone");
            table.Columns.Add("ComFax");
            table.Columns.Add("AccNum");
            table.Columns.Add("AccNm");
            table.Columns.Add("Bal");
            table.Columns.Add("Pmt");
            table.Columns.Add("R");
            table.Columns.Add("OrigTerm");
            table.Columns.Add("Due");
            table.Columns.Add("RemainMons");
            table.Columns.Add("WillBePdOff");
            table.Columns.Add("NotUsedInRatio");
            table.Columns.Add("IsPiggyBack");
            table.Columns.Add("Late30");
            table.Columns.Add("Late60");
            table.Columns.Add("Late90Plus");
            table.Columns.Add("IncInReposession");
            table.Columns.Add("IncInBankruptcy");
            table.Columns.Add("IncInForeclosure");
            table.Columns.Add("ExcFromUnderwriting");
            table.Columns.Add("VerifSentD");
            table.Columns.Add("VerifRecvD");
            table.Columns.Add("VerifExpD");
            table.Columns.Add("VerifReorderedD");
            table.Columns.Add("Desc");
            table.Columns.Add("Attention");
            table.Columns.Add("PrepD");
            table.Columns.Add("IsSeeAttachment");
            table.Columns.Add("OrderRankValue");
            table.Columns.Add("DebtJobExpenseT");
            table.Columns.Add("OrigDebtAmt");
            table.Columns.Add("AutoYearMake");
            table.Columns.Add("IsMortFHAInsured");
            table.Columns.Add("IsForAuto");
            table.Columns.Add("ReconcileStatusT");
            table.Columns.Add("PmlAuditTrailXmlContent");
            table.Columns.Add("IsSubjectProperty1stMortgage");
            table.Columns.Add("FullyIndexedPITIPayment");
            table.Columns.Add("H4HIsRetirement");
            table.Columns.Add("H4HNumOfJointOwners");
            table.Columns.Add("VerifSigningEmployeeId");
            table.Columns.Add("VerifSignatureImgId");
            table.Columns.Add("PayoffAmtLckd");
            table.Columns.Add("PayoffAmt");
            table.Columns.Add("PayoffTimingLckd");
            table.Columns.Add("PayoffTiming");

            return table;
        }

        /// <summary>
        /// Create a DataRow, populate it, and add it to the input table.
        /// </summary>
        /// <param name="table">The table to which the DataRow will be added.</param>
        /// <returns>The DataRow that was created.</returns>
        internal DataRow CreateAndAddDataRow(DataTable table)
        {
            var row = table.NewRow();

            this.SetFieldsToDataRow(row);

            this.DataRow = row;
            table.Rows.Add(row);
            return row;
        }

        /// <summary>
        /// Sets the fields specific to this liability type to the data row.
        /// </summary>
        /// <param name="row">The data row.</param>
        protected virtual void SetFieldsToDataRow(DataRow row)
        {
            row["DebtT"] = ((int)this.DebtT).ToString();
            row["H4HIsRetirement"] = string.Empty; // Hard-coded since this data isn't being moved over.
            row["H4HNumOfJointOwners"] = string.Empty; // Hard-coded since this data isn't being moved over.
            row["NotUsedInRatio"] = this.NotUsedInRatio.ToString();
            row["OwnerT"] = ((int)this.OwnerT).ToString();
            row["PmlAuditTrailXmlContent"] = this.PmlAuditTrailXmlContent;
            row["Pmt"] = this.Pmt_rep;
            row["RemainMons"] = this.RemainMons_raw;
            row["WillBePdOff"] = this.WillBePdOff.ToString();
        }
    }
}
