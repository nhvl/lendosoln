﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    using InterfaceDefs = LendingQB.Core;
    using ShimTarget = LendingQB.Core.Data;

    /// <summary>
    /// Shim for a real property collection.
    /// </summary>
    public class RealPropertyCollectionShim : RecordCollectionShim<DataObjectKind.RealProperty, ShimTarget.RealProperty>, IReCollection
    {
        /// <summary>
        /// The ownership manager for the real properties.
        /// </summary>
        private IShimOwnershipManager<Guid> ownershipManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="RealPropertyCollectionShim"/> class.
        /// </summary>
        /// <param name="applicationData">The application data needed to by some of the shim properties.</param>
        /// <param name="shimContainer">Reference to the container that holds all shims.</param>
        /// <param name="ownershipManager">The ownership manager for the collection records.</param>
        /// <param name="converter">An instance of LosConvert used for formatting the data.</param>
        public RealPropertyCollectionShim(
            IShimAppDataProvider applicationData,
            IShimContainer shimContainer,
            IShimOwnershipManager<Guid> ownershipManager,
            LosConvert converter)
            : base(shimContainer, ownershipManager.AppId, new DisallowSpecialRecords(), converter)
        {
            this.ApplicationData = applicationData;
            this.ownershipManager = ownershipManager;

            var ds = this.GetDataSet();
            var table = RealPropertyShim.CreateAndAddDataTable(ds);
            this.RebuildShimData();
        }

        /// <summary>
        /// Gets a value indicating whether this application requires extra 1003 page 4 for REO.
        /// Return true if has more than 3 REO.
        /// </summary>
        public bool HasExtraREO
        {
            get
            {
                return this.CountRegular > 3;
            }
        }

        /// <summary>
        /// Gets or sets the application data associated with this employment record shim.
        /// </summary>
        private IShimAppDataProvider ApplicationData { get; set; }

        /// <summary>
        /// Add an unitialized property to the collection.
        /// </summary>
        /// <param name="recordT">The type of the property.</param>
        /// <returns>The added property.</returns>
        public ICollectionItemBase2 AddRecord(Enum recordT)
        {
            var statusType = (E_ReoStatusT)recordT;
            return this.AddRecord(statusType);
        }

        /// <summary>
        /// Add an unitialized property to the collection.
        /// </summary>
        /// <param name="reT">The type of the property.</param>
        /// <returns>The added property.</returns>
        public IRealEstateOwned AddRecord(E_ReoStatusT reT)
        {
            var added = this.AddRegularRecord();
            added.KeyType = reT;
            return added;
        }

        /// <summary>
        /// Add an uninitialized regular property to the collection.
        /// </summary>
        /// <returns>The added property.</returns>
        public IRealEstateOwned AddRegularRecord()
        {
            var item = new ShimTarget.RealProperty();
            var id = this.AddEntity(item);
            this.RebuildShimData();

            return this.GetRegRecordOf(id.Value);
        }

        /// <summary>
        /// Add an unitialized regular property into the collection at a specific position.
        /// </summary>
        /// <param name="pos">The position within the collection where the new property is to be located.</param>
        /// <returns>The added property.</returns>
        public IRealEstateOwned AddRegularRecordAt(int pos)
        {
            var item = new ShimTarget.RealProperty();
            var id = this.InsertRegularRecordAt(item, pos);
            return this.GetRegRecordOf(id.Value);
        }

        /// <summary>
        /// Retrieve a property from the collection, adding it if it doesn't already exist.
        /// </summary>
        /// <param name="recordId">The identifier for the property.</param>
        /// <returns>The property with the input identifier.</returns>
        public IRealEstateOwned EnsureRegularRecordOf(Guid recordId)
        {
            var shim = this.TryGetRegularRecordOf(recordId);
            if (shim == null)
            {
                var identifier = DataObjectIdentifier<DataObjectKind.RealProperty, Guid>.Create(recordId);
                var item = new ShimTarget.RealProperty();
                this.AddEntity(identifier, item);
                this.RebuildShimData();

                shim = this.GetRegRecordOf(recordId);
            }

            return (IRealEstateOwned)shim;
        }

        /// <summary>
        /// Retrieve a property from the collection.
        /// </summary>
        /// <param name="recordId">The identifier for the property.</param>
        /// <returns>The property with the input identifier, or throws an exception if no such property is contained within the collection.</returns>
        public IRealEstateOwned GetRegRecordOf(Guid recordId)
        {
            return (IRealEstateOwned)this.GetRegularRecordOf(recordId);
        }

        /// <summary>
        /// Retrieve a regular property from the collection.
        /// </summary>
        /// <param name="pos">The position within the collection where the returned property is located.</param>
        /// <returns>The regular property, or throws an out of bounds exception.</returns>
        public IRealEstateOwned GetRegularRecordAt(int pos)
        {
            return (IRealEstateOwned)this.GetRegularItemAt(pos);
        }

        /// <summary>
        /// Retrieve a special property from the collection.
        /// </summary>
        /// <param name="i">The position within the collection where the returned property is located.</param>
        /// <returns>The special property, or throws an out of bounds exception.</returns>
        public IRealEstateOwned GetSpecialRecordAt(int i)
        {
            // The exception type is what the legacy class would give, but the error message here is more explicit than the canned .net framework message.
            throw new IndexOutOfRangeException("Real property collection doesn't support special records.");
        }

        /// <summary>
        /// Retrieve a subcollection of properties.
        /// </summary>
        /// <param name="inclusiveGroup">If true, items that match the group specifier are included, else other items are.</param>
        /// <param name="group">A group specifier that is used to filter the collection into the returned sub-collection.</param>
        /// <returns>The sub-collection of items.</returns>
        public ISubcollection GetSubcollection(bool inclusiveGroup, E_ReoGroupT group)
        {
            return this.GetSubcollectionCore(inclusiveGroup, group);
        }

        /// <summary>
        /// Add an uninitialized regular property to the collection.
        /// </summary>
        /// <returns>The added property.</returns>
        ICollectionItemBase2 IRecordCollection.AddRegularRecord()
        {
            return this.AddRegularRecord();
        }

        /// <summary>
        /// Add an unitialized regular property into the collection at a specific position.
        /// </summary>
        /// <param name="pos">The position within the collection where the new property is to be located.</param>
        /// <returns>The added property.</returns>
        ICollectionItemBase2 IRecordCollection.AddRegularRecordAt(int pos)
        {
            return this.AddRegularRecordAt(pos);
        }

        /// <summary>
        /// Retrieve a property from the collection, adding it if it doesn't already exist.
        /// </summary>
        /// <param name="recordId">The identifier for the property.</param>
        /// <returns>The property with the input identifier.</returns>
        ICollectionItemBase2 IRecordCollection.EnsureRegularRecordOf(Guid recordId)
        {
            return this.EnsureRegularRecordOf(recordId);
        }

        /// <summary>
        /// Retrieve a property from the collection.
        /// </summary>
        /// <param name="recordId">The identifier for the property.</param>
        /// <returns>The property with the input identifier, or throws an exception if no such property is contained within the collection.</returns>
        ICollectionItemBase2 IRecordCollection.GetRegRecordOf(Guid recordId)
        {
            return this.GetRegularRecordOf(recordId);
        }

        /// <summary>
        /// Retrieve a regular property from the collection.
        /// </summary>
        /// <param name="pos">The position within the collection where the returned property is located.</param>
        /// <returns>The regular property, or throws an out of bounds exception.</returns>
        ICollectionItemBase2 IRecordCollection.GetRegularRecordAt(int pos)
        {
            return this.GetRegularRecordAt(pos);
        }

        /// <summary>
        /// Retrieve a special property from the collection.
        /// </summary>
        /// <param name="i">The position within the collection where the returned property is located.</param>
        /// <returns>The special property, or throws an out of bounds exception.</returns>
        ICollectionItemBase2 IRecordCollection.GetSpecialRecordAt(int i)
        {
            return this.GetSpecialRecordAt(i);
        }

        /// <summary>
        /// Build up the shim data.
        /// </summary>
        protected override void BuildShimData()
        {
            var ds = this.GetDataSet();
            var table = ds.Tables[0];
            foreach (var keyValue in this.GetAllCollectionEntities())
            {
                // Note that the key value pairs should be returned in the correct order because
                // the collection should be pre-ordered for us.
                var shim = RealPropertyShim.Create(this.ShimContainer, this, keyValue.Value, this.ownershipManager, keyValue.Key, this.Converter);
                this.RegularRecords.Add(shim);

                var rp = (RealPropertyShim)shim;
                var row = rp.CreateAndAddDataRow(table);
            }
        }

        /// <summary>
        /// Aggregate data needs to be calculated and passed up to the containing legacy application.
        /// </summary>
        protected override void HandleFlushData()
        {
            var subcoll = this.GetSubcollection(true, E_ReoGroupT.All);
            var aggData = LendersOffice.CalculatedFields.RealProperty.CalculateAggregateData(subcoll.Cast<IRealEstateOwned>(), this.ApplicationData.aOccT == E_aOccT.PrimaryResidence);

            this.ApplicationData.aReTotVal_ = (decimal)aggData.ReTotVal;
            this.ApplicationData.aReTotMAmt_ = (decimal)aggData.ReTotMAmt;
            this.ApplicationData.aReTotGrossRentI_ = (decimal)aggData.ReTotGrossRentI;
            this.ApplicationData.aReTotMPmt_ = (decimal)aggData.ReTotMPmt;
            this.ApplicationData.aReTotHExp_ = (decimal)aggData.ReTotHExp;
            this.ApplicationData.aReTotNetRentI_ = (decimal)aggData.ReNetRentI;
            this.ApplicationData.aBRetainedNegCf_ = (decimal)aggData.ReBRetainedRentI < 0 ? -(decimal)aggData.ReBRetainedRentI : 0;
            this.ApplicationData.aCRetainedNegCf_ = (decimal)aggData.ReCRetainedRentI < 0 ? -(decimal)aggData.ReCRetainedRentI : 0;

            if ((decimal)aggData.ReBNetRentI + (decimal)aggData.ReCNetRentI > 0)
            {
                this.ApplicationData.aBNetRentI_ = (decimal)aggData.ReBNetRentI;
                this.ApplicationData.aCNetRentI_ = (decimal)aggData.ReCNetRentI;
                this.ApplicationData.aBNetNegCf_ = 0;
                this.ApplicationData.aCNetNegCf_ = 0;
            }
            else
            {
                this.ApplicationData.aBNetRentI_ = 0;
                this.ApplicationData.aCNetRentI_ = 0;
                this.ApplicationData.aBNetNegCf_ = -(decimal)aggData.ReBNetRentI;
                this.ApplicationData.aCNetNegCf_ = -(decimal)aggData.ReCNetRentI;
            }
        }

        /// <summary>
        /// Adds the given record to the underlying collection via the aggregate.
        /// </summary>
        /// <param name="value">The record to add.</param>
        /// <returns>The identifier of the new record.</returns>
        protected override DataObjectIdentifier<DataObjectKind.RealProperty, Guid> AddEntity(ShimTarget.RealProperty value)
        {
            return this.ShimContainer.Add(this.ownershipManager.DefaultOwner, value);
        }

        /// <summary>
        /// Adds the given id and record to the underlying collection via the aggregate.
        /// </summary>
        /// <param name="recordId">The id to use.</param>
        /// <param name="value">The record to add.</param>
        protected override void AddEntity(DataObjectIdentifier<DataObjectKind.RealProperty, Guid> recordId, ShimTarget.RealProperty value)
        {
            this.ShimContainer.Add(this.ownershipManager.DefaultOwner, recordId, value);
        }

        /// <summary>
        /// Removes a record from the underlying collection via the aggregate.
        /// </summary>
        /// <param name="recordId">The record id.</param>
        protected override void RemoveEntity(DataObjectIdentifier<DataObjectKind.RealProperty, Guid> recordId)
        {
            this.ShimContainer.RemoveRealPropertyFromLegacyApplication(recordId, this.ownershipManager.AppId, this.ownershipManager.BorrowerId, this.ownershipManager.CoborrowerId);
        }

        /// <summary>
        /// Gets the collection records relevant to the shimmed application.
        /// </summary>
        /// <returns>The collection records relevant to the shimmed application.</returns>
        protected override IEnumerable<KeyValuePair<DataObjectIdentifier<DataObjectKind.RealProperty, Guid>, ShimTarget.RealProperty>> GetAllCollectionEntities()
        {
            return this.ShimContainer.GetRealPropertiesForLegacyApplication(this.ownershipManager.AppId, this.ownershipManager.BorrowerId, this.ownershipManager.CoborrowerId);
        }

        /// <summary>
        /// Assign the values from the current item to the previous shim.
        /// </summary>
        /// <remarks>
        /// The current item may or may not be a shim, but the previous item is definitely a shim around the value
        /// that needs to get modified.
        /// </remarks>
        /// <param name="current">The item with the current values.</param>
        /// <param name="previous">The shim with the previous values.</param>
        private void AssignValuesFromCurrentToPrevious(IRealEstateOwned current, IRealEstateOwned previous)
        {
            previous.Addr = current.Addr;
            previous.City = current.City;
            previous.GrossRentI = current.GrossRentI;
            previous.HExp = current.HExp;
            previous.IsForceCalcNetRentalI = current.IsForceCalcNetRentalI;
            previous.IsPrimaryResidence = current.IsPrimaryResidence;
            previous.IsSubjectProp = current.IsSubjectProp;
            previous.MAmt = current.MAmt;
            previous.MPmt = current.MPmt;
            previous.NetRentI = current.NetRentI;
            previous.NetRentILckd = current.NetRentILckd;
            previous.OccR = current.OccR;
            ////previous.ReOwnerT = current.ReOwnerT;
            previous.State = current.State;
            previous.StatT = current.StatT;
            previous.TypeT = current.TypeT;
            previous.Val = current.Val;
            previous.Zip = current.Zip;
        }

        /// <summary>
        /// Search for a matching item in the two lists.
        /// </summary>
        /// <param name="item1">The item being searched for in the list.</param>
        /// <param name="list">The list that is searched for the item.</param>
        /// <returns>The matching item in the list, or null.</returns>
        private ICollectionItemBase2 SearchListMatch(ICollectionItemBase2 item1, IEnumerable<ICollectionItemBase2> list)
        {
            foreach (var item2 in list)
            {
                if (item1.RecordId == item2.RecordId)
                {
                    return item2;
                }
            }

            return null;
        }
    }
}
