﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Utils;

    /// <summary>
    /// Shim representing employment records.
    /// </summary>
    public sealed class EmploymentRecordShim : CollectionItemBase2Shim, IRegularEmploymentRecord, IPrimaryEmploymentRecord
    {
        /// <summary>
        /// Name to use for the DataTable constructed to pass out data for binding to user controls.
        /// </summary>
        private const string TableName = "EmplmtXmlContent";

        /// <summary>
        /// Initializes a new instance of the <see cref="EmploymentRecordShim"/> class.
        /// </summary>
        /// <param name="shimContainer">Reference to the container that holds all shims.</param>
        /// <param name="collectionShim">The shim for the collection that contains this entity shim.</param>
        /// <param name="contained">The EmploymentRecord entity that holds the data.</param>
        /// <param name="recordId">The identifier for this employment record.</param>
        /// <param name="stringFormatter">Object that controls how semantic types are serialized as strings.</param>
        /// <param name="numericFormatter">Object that controls how semantic types are converted to decimals.</param>
        /// <param name="stringParser">Object that controls how strings are parsed into semantic types.</param>
        private EmploymentRecordShim(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            LendingQB.Core.Data.EmploymentRecord contained,
            DataObjectIdentifier<DataObjectKind.EmploymentRecord, Guid> recordId,
            IFormatAsString stringFormatter,
            IFormatAsNumeric numericFormatter,
            IParseFromString stringParser)
            : base(shimContainer, collectionShim, recordId.Value, stringFormatter, numericFormatter, stringParser)
        {
            this.Contained = contained;
            if (this.Contained.EmployerPhone != null)
            {
                this.AssignedEmployerPhoneNumber = this.Contained.EmployerPhone.Value.ToString();
            }
            else
            {
                this.AssignedEmployerPhoneNumber = string.Empty;
            }

            // override default null logic
            if (this.Contained.IsSeeAttachment == null)
            {
                this.Contained.IsSeeAttachment = true;
            }
        }

        /// <summary>
        /// Gets or sets the Attention entity in the VoE.
        /// </summary>
        public string Attention
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.Attention);
            }

            set
            {
                this.Contained.Attention = this.StringParser.TryParseEntityName(value);
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets the employment end date.
        /// </summary>
        /// <remarks>
        /// It should be noted that regular employment records contain 
        /// EmplmtStartD/EndD,  while primary employment records use 
        /// EmpltStartD (no second 'm').
        /// </remarks>
        public string EmplmtEndD_rep
        {
            get
            {
                if (this.IsCurrent)
                {
                    if (this.StringFormatter is IContainLosConvert)
                    {
                        var provider = (IContainLosConvert)this.StringFormatter;
                        var converter = provider.Converter;
                        if (converter.FormatTargetCurrent == FormatTarget.FannieMaeMornetPlus ||
                            converter.FormatTargetCurrent == FormatTarget.MismoClosing ||
                            converter.FormatTargetCurrent == FormatTarget.XsltExport)
                        {
                            // 4/6/2010 dd - Do not return "PRESENT" value when export.
                            return string.Empty;
                        }
                    }

                    return "PRESENT";
                }
                else
                {
                    return this.StringFormatter.Format(this.Contained.EmploymentEndDate);
                }
            }

            set
            {
                this.Contained.EmploymentEndDate = this.StringParser.TryParseUnzonedDate(value);
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets the number of months beyond the most recent completed full 
        /// year of employment.
        /// </summary>
        public int EmplmtLenInMonths
        {
            get
            {
                if (this.Contained.EmploymentStartDate == null)
                {
                    return 0;
                }

                if (this.EmplmtStat == E_EmplmtStat.Current)
                {
                    YearMonthParser ym = new YearMonthParser();
                    ym.Parse(this.EmplmtLen_rep);
                    return ym.NumberOfMonths;
                }
                else
                {
                    YearMonthParser ym = new YearMonthParser();
                    ym.Parse(this.Contained.EmploymentStartDate.Value.Date, this.Contained.EmploymentEndDate.Value.Date);
                    return ym.NumberOfMonths;
                }
            }
        }

        /// <summary>
        /// Gets the number of months beyond the most recent completed full 
        /// year of employment. 
        /// </summary>
        public string EmplmtLenInMonths_rep
        {
            get
            {
                if (this.StringFormatter is IContainLosConvert)
                {
                    var provider = this.StringFormatter as IContainLosConvert;
                    var converter = provider.Converter;
                    return converter.ToCountString(this.EmplmtLenInMonths);
                }
                else
                {
                    return this.EmplmtLenInMonths.ToString();
                }
            }
        }

        /// <summary>
        /// Gets the employment length in years.
        /// </summary>
        public int EmplmtLenInYrs
        {
            get
            {
                if (this.Contained.EmploymentStartDate == null)
                {
                    return 0;
                }

                if (this.EmplmtStat == E_EmplmtStat.Current)
                {
                    YearMonthParser ym = new YearMonthParser();
                    ym.Parse(this.EmplmtLen_rep);
                    return ym.NumberOfYears;
                }
                else
                {
                    YearMonthParser ym = new YearMonthParser();
                    ym.Parse(this.Contained.EmploymentStartDate.Value.Date, this.Contained.EmploymentEndDate.Value.Date);
                    return ym.NumberOfYears;
                }
            }
        }

        /// <summary>
        /// Gets the employment length in years.
        /// </summary>
        public string EmplmtLenInYrs_rep
        {
            get
            {
                if (this.StringFormatter is IContainLosConvert)
                {
                    var provider = this.StringFormatter as IContainLosConvert;
                    var converter = provider.Converter;
                    return converter.ToCountString(this.EmplmtLenInYrs);
                }
                else
                {
                    return this.EmplmtLenInYrs.ToString();
                }
            }
        }

        /// <summary>
        /// Gets or sets the employment length in total months.
        /// </summary>
        /// <remarks>
        /// The client code that calls this only sets this property and not any of the related properties/methods
        /// so there is no potential conflict.
        /// </remarks>
        public string EmplmtLenTotalMonths
        {
            get
            {
                var count = TimeUtils.CalcWholeMonths(this.Contained.EmploymentStartDate, this.Contained.EmploymentEndDate);
                if (this.StringFormatter is IContainLosConvert)
                {
                    int countInt = count == null ? 0 : count.Value.Value;
                    var provider = this.StringFormatter as IContainLosConvert;
                    var converter = provider.Converter;
                    return converter.ToCountString(countInt);
                }
                else
                {
                    return count.ToString();
                }
            }

            set
            {
                if (this.StringFormatter is IContainLosConvert)
                {
                    var provider = this.StringFormatter as IContainLosConvert;
                    var converter = provider.Converter;
                    string years = converter.ConvertMonthToYearToAccuracy(value, 3);
                    this.EmplmtLen_rep = years;
                }
                else
                {
                    int months;
                    if (int.TryParse(value, out months))
                    {
                        months = 0;
                    }

                    var start = DateTime.Today.AddMonths(-months);
                    this.Contained.EmploymentStartDate = UnzonedDate.Create(start);
                    this.Contained.EmploymentEndDate = null;
                }

                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets the years on the job.
        /// </summary>
        /// <remarks>
        /// In the legacy class this property and the start/end dates are independently managed.
        /// Most client code calls one or the other.  The new code just keeps the start/end dates
        /// so we will use a last-call wins strategy and assume that calling this method is the 
        /// same as saying the employment is current.
        /// </remarks>
        public string EmplmtLen_rep
        {
            get
            {
                var duration = TimeUtils.CalculateYearDuration(this.Contained.EmploymentStartDate, this.Contained.EmploymentEndDate);
                return duration.ToString();
            }

            set
            {
                this.Contained.EmploymentEndDate = null;
                this.Contained.EmploymentStartDate = this.StartDateFromYearsDurationString(value);
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets the employment start date.
        /// </summary>
        /// <remarks>
        /// It should be noted that regular employment records contain 
        /// EmplmtStartD/EndD,  while primary employment records use 
        /// EmpltStartD (no second 'm').
        /// </remarks>
        public string EmplmtStartD_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.EmploymentStartDate);
            }

            set
            {
                this.Contained.EmploymentStartDate = this.StringParser.TryParseUnzonedDate(value);
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets the employment status.
        /// </summary>
        /// <remarks>
        /// While we can indicate Current or Previous, what is really being 
        /// indicated is the distinction between primary and non-primary
        /// employment.  One can have more than one job at a time, but only 
        /// one is considered primary employment.
        /// </remarks>
        public E_EmplmtStat EmplmtStat
        {
            get
            {
                var stat = this.Contained.EmploymentStatusType;
                if (stat == null)
                {
                    return E_EmplmtStat.Current;
                }
                else
                {
                    return stat.Value;
                }
            }

            set
            {
                this.Contained.EmploymentStatusType = value;
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets the VOE employee Id.
        /// </summary>
        public string EmployeeIdVoe
        {
            get
            {
                var voes = this.Contained.EmploymentRecordVoeData;
                if (voes != null && voes.Count > 0)
                {
                    return voes[0].EmployeeIdVoe;
                }
                else
                {
                    return string.Empty;
                }
            }

            set
            {
                var voe = this.GetFirstVoeData();
                voe.EmployeeIdVoe = value;
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets the VOE employer code.
        /// </summary>
        public string EmployerCodeVoe
        {
            get
            {
                var voes = this.Contained.EmploymentRecordVoeData;
                if (voes != null && voes.Count > 0)
                {
                    return voes[0].EmployerCodeVoe;
                }
                else
                {
                    return string.Empty;
                }
            }

            set
            {
                var voe = this.GetFirstVoeData();
                voe.EmployerCodeVoe = value;
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets the employer street address.
        /// </summary>
        public string EmplrAddr
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.EmployerAddress);
            }

            set
            {
                this.Contained.EmployerAddress = this.StringParser.TryParseStreetAddress(value);
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets the employer phone.
        /// </summary>
        public string EmplrBusPhone
        {
            get
            {
                if (this.IsCurrent)
                {
                    var collection = (IEmpCollection)this.CollectionShim;
                    if (collection.IsEmplrBusPhoneLckd)
                    {
                        return this.StringFormatter.Format(this.Contained.EmployerPhone);
                    }
                    else
                    {
                        return collection.PersonBusPhone;
                    }
                }
                else
                {
                    return this.StringFormatter.Format(this.Contained.EmployerPhone);
                }
            }

            set
            {
                this.Contained.EmployerPhone = this.StringParser.TryParsePhoneNumber(value);
                this.AssignedEmployerPhoneNumber = this.Contained.EmployerPhone == null ? string.Empty : this.Contained.EmployerPhone.Value.ToString();
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets the employer city.
        /// </summary>
        public string EmplrCity
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.EmployerCity);
            }

            set
            {
                this.Contained.EmployerCity = this.StringParser.TryParseCity(value);
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets the employer fax.
        /// </summary>
        public string EmplrFax
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.EmployerFax);
            }

            set
            {
                this.Contained.EmployerFax = this.StringParser.TryParsePhoneNumber(value);
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets the EmployerName.
        /// </summary>
        public string EmplrNm
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.EmployerName);
            }

            set
            {
                this.Contained.EmployerName = this.StringParser.TryParseEntityName(value);
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets the employer state.
        /// </summary>
        public string EmplrState
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.EmployerState);
            }

            set
            {
                this.Contained.EmployerState = this.StringParser.TryParseUnitedStatesPostalState(value);
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets the employer zip code.
        /// </summary>
        public string EmplrZip
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.EmployerZip);
            }

            set
            {
                this.Contained.EmployerZip = this.StringParser.TryParseZipcode(value);
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets the employment start date.
        /// </summary>
        /// <remarks>
        /// It should be noted that regular employment records contain 
        /// EmplmtStartD/EndD,  while primary employment records use 
        /// EmpltStartD (no second 'm').
        /// </remarks>
        public string EmpltStartD_rep
        {
            get
            {
                return this.EmplmtStartD_rep;
            }

            set
            {
                this.EmplmtStartD_rep = value;
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the job is current.
        /// </summary>
        public bool IsCurrent
        {
            get
            {
                return this.BooleanFromNullable(this.Contained.IsCurrent);
            }

            set
            {
                this.Contained.IsCurrent = value;
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets a value indicating whether this is the primary 
        /// employment.
        /// </summary>
        public bool IsPrimaryEmp
        {
            get
            {
                if (this.CollectionShim.CountSpecial > 0)
                {
                    var primary = this.CollectionShim.GetSpecialRecordAt(0);
                    return primary.RecordId == this.RecordId;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the see attachment text
        /// should be indicated in the verification of employment form.
        /// </summary>
        public bool IsSeeAttachment
        {
            get
            {
                return this.BooleanFromNullable(this.Contained.IsSeeAttachment);
            }

            set
            {
                this.Contained.IsSeeAttachment = value;
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the record is self
        /// employment.
        /// </summary>
        public bool IsSelfEmplmt
        {
            get
            {
                return this.BooleanFromNullable(this.Contained.IsSelfEmployed);
            }

            set
            {
                this.Contained.IsSelfEmployed = value;
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets the job title.
        /// </summary>
        public string JobTitle
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.JobTitle);
            }

            set
            {
                this.Contained.JobTitle = this.StringParser.TryParseDescriptionField(value);
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets a type for an enumeration that is used as a key.
        /// </summary>
        public Enum KeyType
        {
            get
            {
                return this.EmplmtStat;
            }

            set
            {
                this.EmplmtStat = (E_EmplmtStat)value;
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets the monthly income description.
        /// </summary>
        public string MonI_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.MonthlyIncome);
            }

            set
            {
                this.Contained.MonthlyIncome = this.StringParser.TryParseMoney(value);
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets the prepared date.
        /// </summary>
        public CDateTime PrepD
        {
            get
            {
                return this.UnzonedDateToCDateTime(this.Contained.PrepDate);
            }

            set
            {
                this.Contained.PrepDate = this.CDateTimeToUnzonedDate(value);
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets the years in profession.
        /// </summary>
        public decimal ProfLen
        {
            get
            {
                var duration = TimeUtils.CalculateYearDuration(this.Contained.ProfessionStartDate, null);
                if (duration == null)
                {
                    return 0m;
                }
                else
                {
                    var semantic = (ISemanticType)duration.Value;
                    return Convert.ToDecimal(semantic.Value);
                }
            }
        }

        /// <summary>
        /// Gets profession length in months, after removing the year component.
        /// <example>If profession length is 3.78 (years), this value will be 9 
        /// (months; == 0.78 years).</example>
        /// </summary>
        public string ProfLenRemainderMonths
        {
            get
            {
                decimal remainderYears = this.ProfLen % 1;

                if (this.StringFormatter is IContainLosConvert)
                {
                    var provider = this.StringFormatter as IContainLosConvert;
                    var converter = provider.Converter;
                    return converter.ConvertYearToMonth(remainderYears.ToString());
                }
                else
                {
                    var months = Convert.ToInt32(12 * remainderYears);
                    return months.ToString();
                }
            }
        }

        /// <summary>
        /// Gets or sets the profession length in total months.
        /// </summary>
        public string ProfLenTotalMonths
        {
            get
            {
                if (this.StringFormatter is IContainLosConvert)
                {
                    var provider = this.StringFormatter as IContainLosConvert;
                    var converter = provider.Converter;
                    return converter.ConvertYearToMonth(this.ProfLen.ToString());
                }
                else
                {
                    var months = Convert.ToInt32(12 * this.ProfLen);
                    return months.ToString();
                }
            }

            set
            {
                if (this.StringFormatter is IContainLosConvert)
                {
                    var provider = this.StringFormatter as IContainLosConvert;
                    var converter = provider.Converter;
                    string years = converter.ConvertMonthToYear(value);
                    this.Contained.ProfessionStartDate = this.StartDateFromYearsDurationString(years);
                }
                else
                {
                    int months;
                    if (int.TryParse(value, out months))
                    {
                        months = 0;
                    }

                    var start = DateTime.Today.AddMonths(-months);
                    this.Contained.ProfessionStartDate = UnzonedDate.Create(start);
                }

                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets profession length in years, rounded down.
        /// <example>If profession length is 3.78 (years), this value will be 
        /// 3 (years).</example>
        /// </summary>
        public string ProfLenYears
        {
            get
            {
                int years = (int)Math.Truncate(this.ProfLen);
                if (this.StringFormatter is IContainLosConvert)
                {
                    var provider = this.StringFormatter as IContainLosConvert;
                    var converter = provider.Converter;
                    return converter.ToCountString(years);
                }
                else
                {
                    return years.ToString();
                }
            }
        }

        /// <summary>
        /// Gets or sets the years in profession.
        /// </summary>
        public string ProfLen_rep
        {
            get
            {
                if (this.Contained.ProfessionStartDate == null)
                {
                    return string.Empty;
                }
                else
                {
                    return this.ProfLen.ToString();
                }
            }

            set
            {
                this.Contained.ProfessionStartDate = this.StartDateFromYearsDurationString(value);
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets the profession start date.
        /// </summary>
        public string ProfStartD_rep
        {
            get
            {
                if (this.Contained.ProfessionStartDate == null)
                {
                    return string.Empty;
                }

                string defaultFormat = this.Contained.ProfessionStartDate.Value.Date.ToShortDateString();
                if (this.StringFormatter is IContainLosConvert)
                {
                    var provider = this.StringFormatter as IContainLosConvert;
                    var converter = provider.Converter;
                    return converter.ToDateTimeVarCharString(defaultFormat);
                }
                else
                {
                    return defaultFormat;
                }
            }

            set
            {
                this.Contained.ProfessionStartDate = this.StringParser.TryParseUnzonedDate(value);
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets the verification expiration date.
        /// </summary>
        public CDateTime VerifExpD
        {
            get
            {
                return this.UnzonedDateToCDateTime(this.Contained.VerifExpiresDate);
            }

            set
            {
                this.Contained.VerifExpiresDate = this.CDateTimeToUnzonedDate(value);
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets the verification expiration date.
        /// </summary>
        public string VerifExpD_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.VerifExpiresDate);
            }

            set
            {
                this.Contained.VerifExpiresDate = this.StringParser.TryParseUnzonedDate(value);
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets a value indicating whether the verification has an 
        /// associated signature.
        /// </summary>
        public bool VerifHasSignature
        {
            get
            {
                return this.Contained.VerifSignatureImgId != null;
            }
        }

        /// <summary>
        /// Gets or sets the verification received date.
        /// </summary>
        public CDateTime VerifRecvD
        {
            get
            {
                return this.UnzonedDateToCDateTime(this.Contained.VerifRecvDate);
            }

            set
            {
                this.Contained.VerifRecvDate = this.CDateTimeToUnzonedDate(value);
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets the verification received date.
        /// </summary>
        public string VerifRecvD_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.VerifRecvDate);
            }

            set
            {
                this.Contained.VerifRecvDate = this.StringParser.TryParseUnzonedDate(value);
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets the verification re-order date.
        /// </summary>
        public CDateTime VerifReorderedD
        {
            get
            {
                return this.UnzonedDateToCDateTime(this.Contained.VerifReorderedDate);
            }

            set
            {
                this.Contained.VerifReorderedDate = this.CDateTimeToUnzonedDate(value);
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets the verification re-order date.
        /// </summary>
        public string VerifReorderedD_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.VerifReorderedDate);
            }

            set
            {
                this.Contained.VerifReorderedDate = this.StringParser.TryParseUnzonedDate(value);
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets the verification sent date.
        /// </summary>
        public CDateTime VerifSentD
        {
            get
            {
                return this.UnzonedDateToCDateTime(this.Contained.VerifSentDate);
            }

            set
            {
                this.Contained.VerifSentDate = this.CDateTimeToUnzonedDate(value);
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets the verification sent date.
        /// </summary>
        public string VerifSentD_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.VerifSentDate);
            }

            set
            {
                this.Contained.VerifSentDate = this.StringParser.TryParseUnzonedDate(value);
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets image id of the verification signature.
        /// </summary>
        public string VerifSignatureImgId
        {
            get
            {
                if (this.Contained.VerifSigningEmployeeId == null)
                {
                    return string.Empty;
                }
                else
                {
                    return this.Contained.VerifSignatureImgId.Value.ToString();
                }
            }
        }

        /// <summary>
        /// Gets the employee id of verification signing employee.
        /// </summary>
        public Guid VerifSigningEmployeeId
        {
            get
            {
                if (this.Contained.VerifSigningEmployeeId == null)
                {
                    return Guid.Empty;
                }
                else
                {
                    return this.Contained.VerifSigningEmployeeId.Value.Value;
                }
            }
        }

        /// <summary>
        /// Gets a list of the associated VOE data.
        /// </summary>
        public List<EmploymentRecordVOEData> EmploymentRecordVOEData => this.Contained.EmploymentRecordVoeData;

        /// <summary>
        /// Gets or sets a value indicating whether the borrower has a special relationship with the Employer.
        /// </summary>
        public bool IsSpecialBorrowerEmployerRelationship
        {
            get
            {
                return this.BooleanFromNullable(this.Contained.IsSpecialBorrowerEmployerRelationship);
            }

            set
            {
                this.Contained.IsSpecialBorrowerEmployerRelationshipData = value;
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets a value determining whether the Ownership share is Blank, 0 - 25%, 25% or more.
        /// </summary>
        public SelfOwnershipShare SelfOwnershipShareT
        {
            get
            {
                if (this.Contained.SelfOwnershipShareT == null)
                {
                    return SelfOwnershipShare.Blank;
                }

                return this.Contained.SelfOwnershipShareT.Value;
            }

            set
            {
                this.Contained.SelfOwnershipShareTData = value;
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Gets or sets the employment record entity.
        /// </summary>
        private LendingQB.Core.Data.EmploymentRecord Contained { get; set; }

        /// <summary>
        /// Gets or sets the value that is assigned as the phone number.
        /// </summary>
        /// <remarks>
        /// The Getter bypasses the value that is set so we need to take care to keep
        /// the value given to the setter and retrieve that value from here.
        /// </remarks>
        private string AssignedEmployerPhoneNumber { get; set; }

        /// <summary>
        /// Applies the signature.
        /// </summary>
        /// <param name="employeeId">The employee id.</param>
        /// <param name="imgId">The image id.</param>
        public void ApplySignature(Guid employeeId, string imgId)
        {
            Guid imgGuid;
            bool ok = Guid.TryParse(imgId, out imgGuid);
            if (ok)
            {
                var empIdentifier = DataObjectIdentifier<DataObjectKind.Employee, Guid>.Create(employeeId);
                var imgIdentifier = DataObjectIdentifier<DataObjectKind.Signature, Guid>.Create(imgGuid);
                this.Contained.SetVerifSignature(empIdentifier, imgIdentifier);
                this.UpdateDataRow();
            }
        }

        /// <summary>
        /// Clears the signature.
        /// </summary>
        public void ClearSignature()
        {
            this.Contained.ClearVerifSignature();
            this.UpdateDataRow();
        }

        /// <summary>
        /// Sets the employment start date.
        /// </summary>
        /// <remarks>
        /// Similar to properties that do the same thing, we will adopt a last-call wins strategy.
        /// </remarks>
        /// <param name="date">The employement start date.</param>
        public void SetEmploymentStartDate(string date)
        {
            this.EmpltStartD_rep = date;
        }

        /// <summary>
        /// Sets the profession start date.
        /// </summary>
        /// <param name="date">The profession start date.</param>
        public void SetProfessionStartDate(string date)
        {
            this.ProfStartD_rep = date;
        }

        /// <summary>
        /// Create a shim implementation of the IEmploymentRecord interface.
        /// </summary>
        /// <param name="shimContainer">Reference to the container that holds all shims.</param>
        /// <param name="collectionShim">The shim for the collection that contains this entity shim.</param>
        /// <param name="contained">The contained employment record data.</param>
        /// <param name="recordId">The identifier for this employment record.</param>
        /// <param name="converter">Instance of LosConvert used to format types as strings.</param>
        /// <returns>A shim for the IEmploymentRecord interface.</returns>
        internal static IEmploymentRecord Create(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            LendingQB.Core.Data.EmploymentRecord contained,
            DataObjectIdentifier<DataObjectKind.EmploymentRecord, Guid> recordId,
            LosConvert converter)
        {
            return Create(shimContainer, collectionShim, contained, recordId, new FormatEmploymentRecordShimAsString(converter), null, new ParseEntityShimFromString(converter));
        }

        /// <summary>
        /// Create a DataTable under the DataSet, and create the columns.
        /// </summary>
        /// <param name="ds">The DataSet to which the table will be attached.</param>
        /// <returns>The DataTable with columns initialized.</returns>
        internal static DataTable CreateAndAddDataTable(DataSet ds)
        {
            var table = ds.Tables.Add(TableName);
            table.Columns.Add("RecordId");
            table.Columns.Add("EmplmtStartD");
            table.Columns.Add("EmplmtEndD");
            table.Columns.Add("EmplrNm");
            table.Columns.Add("EmplrBusPhone");
            table.Columns.Add("EmplrFax");
            table.Columns.Add("EmplrAddr");
            table.Columns.Add("EmplrCity");
            table.Columns.Add("EmplrState");
            table.Columns.Add("JobTitle");
            table.Columns.Add("EmplrZip");
            table.Columns.Add("VerifSentD");
            table.Columns.Add("VerifReorderedD");
            table.Columns.Add("VerifRecvD");
            table.Columns.Add("VerifExpD");
            table.Columns.Add("IsSelfEmplmt");
            table.Columns.Add("MonI");
            table.Columns.Add("EmplmtStat");
            table.Columns.Add("EmpltStartD");
            table.Columns.Add("EmplmtLen");
            table.Columns.Add("ProfStartD");
            table.Columns.Add("ProfLen");
            table.Columns.Add("Attention");
            table.Columns.Add("PrepD");
            table.Columns.Add("IsSeeAttachment");
            table.Columns.Add("OrderRankValue");
            table.Columns.Add("VerifSigningEmployeeId");
            table.Columns.Add("VerifSignatureImgId");
            table.Columns.Add("IsCurrent");
            table.Columns.Add("EmploymentRecordVOEDataJsonContent");
            table.Columns.Add("IsSpecialBorrowerEmployerRelationship");
            table.Columns.Add("SelfOwnershipShareT");

            return table;
        }

        /// <summary>
        /// Create a DataRow, populate it, and add it to the input table.
        /// </summary>
        /// <param name="table">The table to which the DataRow will be added.</param>
        /// <returns>The DataRow that was created.</returns>
        internal DataRow CreateAndAddDataRow(DataTable table)
        {
            var row = table.NewRow();
            this.DataRow = row;
            this.UpdateDataRow();

            table.Rows.Add(row);
            return row;
        }

        /// <summary>
        /// Create a shim implementation of the IEmploymentRecord interface.
        /// </summary>
        /// <param name="shimContainer">Reference to the container that holds all shims.</param>
        /// <param name="collectionShim">The shim for the collection that contains this entity shim.</param>
        /// <param name="contained">The contained employment record data.</param>
        /// <param name="recordId">The identifier for this employment record.</param>
        /// <param name="stringFormatter">Object that implements formatting semantic types into string values.</param>
        /// <param name="numericFormatter">Object that implements conversion of semantic types into decimal values.</param>
        /// <param name="stringParser">Object that implements parsing strings into semantic types.</param>
        /// <returns>A shim for the IEmploymentRecord interface.</returns>
        private static IEmploymentRecord Create(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            LendingQB.Core.Data.EmploymentRecord contained,
            DataObjectIdentifier<DataObjectKind.EmploymentRecord, Guid> recordId,
            IFormatAsString stringFormatter,
            IFormatAsNumeric numericFormatter,
            IParseFromString stringParser)
        {
            return new EmploymentRecordShim(shimContainer, collectionShim, contained, recordId, stringFormatter ?? new FormatAsStringBase(), numericFormatter ?? new FormatAsNumericBase(), stringParser ?? new ParseFromStringBase());
        }

        /// <summary>
        /// Set the values for the data row whenever a state change has occurred.
        /// </summary>
        private void UpdateDataRow()
        {
            var row = this.DataRow;
            row["EmplmtStartD"] = this.DataRowFormattedUnzonedDate(this.Contained.EmploymentStartDate);
            row["EmplmtEndD"] = this.DataRowFormattedUnzonedDate(this.Contained.EmploymentEndDate);
            row["VerifSentD"] = this.DataRowFormattedUnzonedDate(this.Contained.VerifSentDate);
            row["VerifReorderedD"] = this.DataRowFormattedUnzonedDate(this.Contained.VerifReorderedDate);
            row["VerifRecvD"] = this.DataRowFormattedUnzonedDate(this.Contained.VerifRecvDate);
            row["VerifExpD"] = this.DataRowFormattedUnzonedDate(this.Contained.VerifExpiresDate);
            row["ProfStartD"] = this.DataRowFormattedUnzonedDate(this.Contained.ProfessionStartDate);
            row["PrepD"] = this.DataRowFormattedUnzonedDate(this.Contained.PrepDate);
            row["EmpltStartD"] = row["EmplmtStartD"];

            row["EmplrNm"] = this.EmplrNm;
            row["EmplrBusPhone"] = this.AssignedEmployerPhoneNumber;
            row["EmplrFax"] = this.EmplrFax;
            row["EmplrAddr"] = this.EmplrAddr;
            row["EmplrCity"] = this.EmplrCity;
            row["EmplrState"] = this.EmplrState;
            row["JobTitle"] = this.JobTitle;
            row["EmplrZip"] = this.EmplrZip;
            row["IsSelfEmplmt"] = this.IsSelfEmplmt.ToString();
            row["MonI"] = this.MonI_rep;
            row["EmplmtStat"] = ((int)this.EmplmtStat).ToString();
            row["EmplmtLen"] = this.EmplmtLen_rep;
            row["ProfLen"] = this.ProfLen_rep;
            row["Attention"] = this.Attention;
            row["IsSeeAttachment"] = this.IsSeeAttachment.ToString();
            row["VerifSigningEmployeeId"] = this.VerifSigningEmployeeId.ToString();
            row["VerifSignatureImgId"] = this.VerifSignatureImgId;
            row["IsCurrent"] = this.IsCurrent.ToString();
            row["IsSpecialBorrowerEmployerRelationship"] = this.IsSpecialBorrowerEmployerRelationship.ToString();
            row["SelfOwnershipShareT"] = this.SelfOwnershipShareT;

            if (this.Contained.EmploymentRecordVoeData == null || this.Contained.EmploymentRecordVoeData.Count == 0)
            {
                row["EmploymentRecordVOEDataJsonContent"] = string.Empty;
            }
            else
            {
                row["EmploymentRecordVOEDataJsonContent"] = SerializationHelper.JsonNetSerialize(this.Contained.EmploymentRecordVoeData);
            }
        }

        /// <summary>
        /// Retrieve the first item in the VOE collection.  If the collection is empty, add an empty item.
        /// </summary>
        /// <returns>The first item in the VOE collection, possibly empty.</returns>
        private EmploymentRecordVOEData GetFirstVoeData()
        {
            var voes = this.Contained.EmploymentRecordVoeData;
            if (voes == null)
            {
                this.Contained.EmploymentRecordVoeData = new List<EmploymentRecordVOEData>();
            }

            if (voes.Count == 0)
            {
                this.Contained.EmploymentRecordVoeData.Add(new EmploymentRecordVOEData());
            }

            return voes[0];
        }

        /// <summary>
        /// Utility for taking a string representation of a number of years (possibly a fraction)
        /// and converting it to a start date assuming that today is the end date.
        /// </summary>
        /// <param name="value">A string representation of a number of years.</param>
        /// <returns>A corresponding start date, or null.</returns>
        private UnzonedDate? StartDateFromYearsDurationString(string value)
        {
            decimal duration;
            if (decimal.TryParse(value, out duration))
            {
                int years = Convert.ToInt32(Math.Truncate(duration));
                int days = Convert.ToInt32(Math.Round((duration - years) * 365));

                var start = DateTime.Today.AddYears(-years).AddDays(-days);
                return UnzonedDate.Create(start);
            }
            else
            {
                return null;
            }
        }
    }
}
