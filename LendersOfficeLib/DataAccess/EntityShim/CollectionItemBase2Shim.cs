﻿namespace DataAccess
{
    using System;
    using System.Data;
    using LendingQB.Core;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Shim implementation for the ICollectionItemBase2 interface.
    /// </summary>
    public abstract class CollectionItemBase2Shim : CollectionItemBaseShim
    {
        /// <summary>
        /// Format flag for the database representation of the Guid.
        /// </summary>
        public const string GuidFormatForDatabase = "D";

        /// <summary>
        /// Column name to use for the RecordId property.
        /// </summary>
        public const string RecordIdKey = "RecordId";

        /// <summary>
        /// Column name to use for the OrderRankValue property.
        /// </summary>
        public const string OrderRankValueKey = "OrderRankValue";

        /// <summary>
        /// Initializes a new instance of the <see cref="CollectionItemBase2Shim"/> class.
        /// </summary>
        /// <param name="shimContainer">Reference to the container that holds all shims.</param>
        /// <param name="collectionShim">The shim for the collection that contains this entity shim.</param>
        /// <param name="recordId">The identifier for this public record.</param>
        /// <param name="stringFormatter">Object that controls how semantic types are serialized as strings.</param>
        /// <param name="numericFormatter">Object that controls how semantic types are converted to numeric values.</param>
        /// <param name="stringParser">Object that controls how strings are parsed into semantic types.</param>
        protected CollectionItemBase2Shim(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            Guid recordId,
            IFormatAsString stringFormatter,
            IFormatAsNumeric numericFormatter,
            IParseFromString stringParser)
            : base(recordId, stringFormatter, numericFormatter, stringParser)
        {
            this.ShimContainer = shimContainer;
            this.CollectionShim = collectionShim;
        }

        /// <summary>
        /// Gets or sets the data row for corresponding to this shim.
        /// </summary>
        public DataRow DataRow { get; protected set; }

        /// <summary>
        /// Gets the position from within a containing collection that this item has.
        /// </summary>
        public int RowPos
        {
            get
            {
                var rows = this.CollectionShim.GetDataSet().Tables[0].Rows;
                for (int i = 0; i < rows.Count; ++i)
                {
                    if (rows[i] == this.DataRow)
                    {
                        return i;
                    }
                }

                // Legacy code throws in this, so we continue it here in the shims.
                throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Invalid RowPos.");
            }
        }

        /// <summary>
        /// Gets or sets a value that can be used to order the items in a collection.
        /// </summary>
        /// <remarks>
        /// This is used in two contexts, XmlRecordList.cs and SharedImportExport.cs.
        /// For xmlRecordList.cs, the value is calculated based on the desired position.
        /// In this context we just need to locate the targets and insert.
        /// For SharedImportExport.cs, the collection is built up from pre-existing xml
        /// and the positions are assigned for every item.  There may or may not already be 
        /// items in the collection to match so order as best we can.
        /// Conclusion: as long as we use the pre-existing values as a guide and adjust the 
        /// ordering accordingly, then the final order should mimic the assigned values.
        /// Important to remember - the collection isn't held for long...a single item is inserted
        /// or the whole collection is built from a resource.  Multiple calls on the same record
        /// will not happen.
        /// </remarks>
        public double OrderRankValue
        {
            get
            {
                return double.Parse((string)this.DataRow[OrderRankValueKey]);
            }

            set
            {
                int maxPos = int.MinValue;
                double maxRank = double.MinValue;

                for (int i = 0; i < this.CollectionShim.CountRegular; ++i)
                {
                    var shim = this.CollectionShim.GetRegularRecordAt(i);
                    double rank = shim.OrderRankValue;
                    if (rank < value && rank > maxRank)
                    {
                        maxRank = rank;
                        maxPos = shim.RowPos;
                    }
                }

                if (maxPos > this.RowPos)
                {
                    int steps = maxPos - this.RowPos;
                    while (steps-- > 0)
                    {
                        this.MoveDown();
                    }
                }
                else if (maxPos < this.RowPos)
                {
                    int steps = this.RowPos - maxPos - 1;
                    while (steps-- > 0)
                    {
                        this.MoveUp();
                    }
                }

                this.DataRow["OrderRankValue"] = value.ToString();
            }
        }

        /// <summary>
        /// Gets the container that provides access to all the shims for the associated loan.
        /// </summary>
        protected IShimContainer ShimContainer { get; private set; }

        /// <summary>
        /// Gets the collection shim used to carry out add/remove operations.
        /// </summary>
        protected IRecordCollection CollectionShim { get; private set; }

        /// <summary>
        /// Assign a new identifier to this item.
        /// </summary>
        /// <param name="newId">The new identifier.</param>
        public virtual void ChangeRecordId(Guid newId)
        {
            // This should never need to be called when shimming
            // because it is used when the existing record id has no value.
            throw new NotImplementedException();
        }

        /// <summary>
        /// Move the item down one position in the containing collection.
        /// </summary>
        public void MoveDown()
        {
            this.CollectionShim.MoveRegularRecordDown((ICollectionItemBase2)this);
        }

        /// <summary>
        /// Move the item up one position in the containing position.
        /// </summary>
        public void MoveUp()
        {
            this.CollectionShim.MoveRegularRecordUp((ICollectionItemBase2)this);
        }

        /// <summary>
        /// Let the item know that it is about to be saved, so any necessary cleanup work can be done.
        /// </summary>
        public virtual void PrepareToFlush()
        {
            if (this.IsOnDeathRow)
            {
                var remover = this.CollectionShim as IRemoveEntityShim;
                if (remover != null)
                {
                    remover.Remove((ICollectionItemBase2)this);
                }
            }
        }

        /// <summary>
        /// Retrieve a value based on the field name.
        /// </summary>
        /// <param name="fieldName">The field name used to locate the value.</param>
        /// <returns>The value associated with the field name.</returns>
        public virtual string SortValueFor(string fieldName)
        {
            return this.DataRow[fieldName].ToString();
        }

        /// <summary>
        /// Updates the record id. Please don't use unless you must.
        /// </summary>
        /// <param name="id">The new id.</param>
        protected void UpdateRecordId(Guid id)
        {
            this.RecordId = id;
            this.DataRow[RecordIdKey] = id.ToString(GuidFormatForDatabase);
        }

        /// <summary>
        /// Gets an enum field in a way that preserves the behavior from the legacy class.
        /// </summary>
        /// <typeparam name="T">The type of the enum.</typeparam>
        /// <param name="rawValue">The raw value.</param>
        /// <param name="defaultValue">The default value if the raw value is out of the defined range.</param>
        /// <returns>
        /// The default value if the raw value is not null and not defined for the enum type. 
        /// If the value is defined for the enum, returns that value.
        /// If the raw value is null, this will return the enum option that corresponds to 0 if it exists.
        /// If the raw value is null and there is no enum value for 0 then this will return the provided default.
        /// </returns>
        protected T GetEnum<T>(T? rawValue, T defaultValue) where T : struct
        {
            var enumType = typeof(T);

            // The old code would call GetCount on the field name, which would.
            // return 0 by default if the value was not defined.
            object valueToParse = rawValue ?? (object)0;

            if (Enum.IsDefined(enumType, valueToParse))
            {
                return (T)Enum.Parse(enumType, valueToParse.ToString());
            }
            else
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// There are some formats that LosFormat returns that cannot be parsed by some of our legacy code.
        /// This means that the values in the DataRow should be specially handled.
        /// </summary>
        /// <param name="unzoned">The date to format for the DataRow.</param>
        /// <returns>The formatted data so that it is safe for the DataRow.</returns>
        protected string DataRowFormattedUnzonedDate(UnzonedDate? unzoned)
        {
            if (unzoned != null && this.StringFormatter is IContainLosConvert)
            {
                var format = this.StringFormatter as IContainLosConvert;
                var converter = format.Converter;
                if (converter.FormatTargetCurrent == FormatTarget.FannieMaeMornetPlus || converter.FormatTargetCurrent == FormatTarget.FreddieMacAUS || converter.FormatTargetCurrent == FormatTarget.GinnieNet)
                {
                    return unzoned.Value.Date.ToString("MM/dd/yyyy");
                }
            }

            return this.StringFormatter.Format(unzoned);
        }

        /// <summary>
        /// There are some formats that LosFormat returns that cannot be parsed by some of our legacy code.
        /// This means that the values in the DataRow should be specially handled.
        /// </summary>
        /// <param name="unzoned">The date to format for the DataRow.</param>
        /// <returns>The formatted data so that it is safe for the DataRow.</returns>
        protected string DataRowFormattedUnzonedDate(string unzoned)
        {
            if (unzoned != null && this.StringFormatter is IContainLosConvert)
            {
                var format = this.StringFormatter as IContainLosConvert;
                var converter = format.Converter;
                if (converter.FormatTargetCurrent == FormatTarget.FannieMaeMornetPlus || converter.FormatTargetCurrent == FormatTarget.FreddieMacAUS || converter.FormatTargetCurrent == FormatTarget.GinnieNet)
                {
                    var date = DateTime.ParseExact(unzoned, "yyyyMMdd", null);
                    return date.ToString("MM/dd/yyyy");
                }
            }

            return unzoned;
        }
    }
}
