﻿namespace DataAccess
{
    using System;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Shim for an asset that is a retirement asset.
    /// </summary>
    public sealed class AssetRetirementShim : AssetSpecialShim, IAssetRetirement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AssetRetirementShim"/> class.
        /// </summary>
        /// <param name="shimContainer">Reference to the container that holds all shims.</param>
        /// <param name="collectionShim">The shim for the collection that contains this entity shim.</param>
        /// <param name="contained">The Asset entity that holds the data.</param>
        /// <param name="ownershipManager">The ownership manager for the collection records.</param>
        /// <param name="recordId">The identifier for this asset.</param>
        /// <param name="stringFormatter">Object that controls how semantic types are serialized as strings.</param>
        /// <param name="numericFormatter">Object that controls how semantic types are converted to numeric values.</param>
        /// <param name="stringParser">Object that controls how strings are parsed into semantic types.</param>
        private AssetRetirementShim(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            LendingQB.Core.Data.Asset contained,
            IShimOwnershipManager<Guid> ownershipManager,
            DataObjectIdentifier<DataObjectKind.Asset, Guid> recordId,
            IFormatAsString stringFormatter,
            IFormatAsNumeric numericFormatter,
            IParseFromString stringParser)
            : base(shimContainer, collectionShim, contained, ownershipManager, recordId, stringFormatter, numericFormatter, stringParser)
        {
            this.Contained.AssetType = E_AssetT.Retirement;
        }

        /// <summary>
        /// Create a shim implementation of the IAssetRetirement interface.
        /// </summary>
        /// <param name="shimContainer">Reference to the container that holds all shims.</param>
        /// <param name="collectionShim">The shim for the collection that contains this entity shim.</param>
        /// <param name="contained">The contained asset data.</param>
        /// <param name="ownershipManager">The ownership manager for the collection records.</param>
        /// <param name="recordId">The identifier for this asset.</param>
        /// <param name="converter">Converter to use.</param>
        /// <returns>A shim for the IAssetRetirement interface.</returns>
        internal static IAssetRetirement Create(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            LendingQB.Core.Data.Asset contained,
            IShimOwnershipManager<Guid> ownershipManager,
            DataObjectIdentifier<DataObjectKind.Asset, Guid> recordId,
            LosConvert converter)
        {
            return new AssetRetirementShim(
                shimContainer,
                collectionShim,
                contained,
                ownershipManager,
                recordId,
                new FormatEntityShimAsString(converter),
                new FormatAsNumericBase(),
                new ParseEntityShimFromString(converter));
        }
    }
}
