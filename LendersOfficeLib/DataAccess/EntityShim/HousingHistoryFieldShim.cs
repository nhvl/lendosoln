﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LendersOffice.Common;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;

    using HousingHistoryEntryId = LqbGrammar.DataTypes.DataObjectIdentifier<LqbGrammar.DataTypes.DataObjectKind.HousingHistoryEntry, System.Guid>;
    using HousingHistoryIdAndEntry = System.Collections.Generic.KeyValuePair<LqbGrammar.DataTypes.DataObjectIdentifier<LqbGrammar.DataTypes.DataObjectKind.HousingHistoryEntry, System.Guid>, LendingQB.Core.Data.HousingHistoryEntry>;

    /// <summary>
    /// This class represents the shim from the static borrower address fields to the <see cref="CPageData.HousingHistoryEntries"/> collection.
    /// </summary>
    /// <remarks>
    /// The legacy fields involved here are all consistently named. They each begin with a value matching the pattern <code>a(B|C)(Prev(1|2))</code>.
    /// This indicates whether they refer to the borrower or co-borrower's present, 1st previous, or 2nd previous housing history entry. The fields
    /// names end with something matching the pattern <code>Addr|City|State|Zip|AddrT|AddrYrs</code>, indicating the type of data stored in the field.
    /// Each data type has public static get/set methods in this class for the legacy data types from some <see cref="HousingHistoryEntry"/>.
    /// </remarks>
    public class HousingHistoryFieldShim
    {
        /// <summary>
        /// Adds the specified <see cref="HousingHistoryEntry"/> to the borrower's history, returning the identifier where it is located.
        /// </summary>
        private readonly Func<HousingHistoryEntry, HousingHistoryEntryId> addBorrHousingHistoryEntry;

        /// <summary>
        /// Adds the specified <see cref="HousingHistoryEntry"/> to the co-borrower's history, returning the identifier where it is located.
        /// </summary>
        private readonly Func<HousingHistoryEntry, HousingHistoryEntryId> addCoboHousingHistoryEntry;

        /// <summary>
        /// This maps the beginning of the field name (e.g. <code>"aBPrev1"</code>) to the identifier for the entry it describes.
        /// This is set up once and can only be added to when entries are set that don't yet exist.
        /// </summary>
        private readonly Dictionary<string, HousingHistoryEntryId> shimmedEntries;

        /// <summary>
        /// Initializes a new instance of the <see cref="HousingHistoryFieldShim"/> class.
        /// </summary>
        /// <param name="borrHousingHistory">The borrower's housing history entries.</param>
        /// <param name="coboHousingHistory">The co-borrower's housing history entries.</param>
        /// <param name="addBorrHousingHistoryEntry">The function to add an entry to the borrower's housing history.</param>
        /// <param name="addCoboHousingHistoryEntry">The function to add an entry to the co-borrower's housing history.</param>
        public HousingHistoryFieldShim(
            IEnumerable<HousingHistoryIdAndEntry> borrHousingHistory,
            IEnumerable<HousingHistoryIdAndEntry> coboHousingHistory,
            Func<HousingHistoryEntry, HousingHistoryEntryId> addBorrHousingHistoryEntry,
            Func<HousingHistoryEntry, HousingHistoryEntryId> addCoboHousingHistoryEntry)
        {
            this.addBorrHousingHistoryEntry = addBorrHousingHistoryEntry;
            this.addCoboHousingHistoryEntry = addCoboHousingHistoryEntry;

            // 2019-01 tj - Per conversation with Doug, the shim will only support US addresses, since we need to be sure that
            // the data roundtrips.  This means aBAddr will be blank if the borrower has a foreign present address.
            var borrEntries = borrHousingHistory.Where(e => e.Value.Address == null || e.Value.Address is UnitedStatesPostalAddress);
            var coboEntries = coboHousingHistory.Where(e => e.Value.Address == null || e.Value.Address is UnitedStatesPostalAddress);
            var shimMapping = new List<Tuple<string, HousingHistoryEntryId?>>(6)
            {
                Tuple.Create("aB", borrEntries.FirstOrNull(e => e.Value.IsPresentAddress ?? false)?.Key),
                Tuple.Create("aC", coboEntries.FirstOrNull(e => e.Value.IsPresentAddress ?? false)?.Key),
                Tuple.Create("aBPrev1", borrEntries.FirstOrNull(e => !(e.Value.IsPresentAddress ?? false))?.Key),
                Tuple.Create("aCPrev1", coboEntries.FirstOrNull(e => !(e.Value.IsPresentAddress ?? false))?.Key),
            };
            shimMapping.Add(Tuple.Create("aBPrev2", borrEntries.FirstOrNull(e => !(e.Value.IsPresentAddress ?? false) && e.Key != shimMapping[2].Item2)?.Key));
            shimMapping.Add(Tuple.Create("aCPrev2", coboEntries.FirstOrNull(e => !(e.Value.IsPresentAddress ?? false) && e.Key != shimMapping[3].Item2)?.Key));
            this.shimmedEntries = shimMapping.Where(t => t.Item2 != null).ToDictionary(t => t.Item1, t => t.Item2.Value);
        }

        /// <summary>
        /// Gets the street address for an entry in the housing history.
        /// </summary>
        /// <param name="entry">The housing history entry.</param>
        /// <returns>The street address.</returns>
        public static string GetStreetAddress(HousingHistoryEntry entry) => GetStreetAddress(entry?.Address);

        /// <summary>
        /// Gets the street address for an address.
        /// </summary>
        /// <param name="address">The address object.</param>
        /// <returns>The street address.</returns>
        public static string GetStreetAddress(PostalAddress address) => address?.StreetAddress.ToString() ?? string.Empty;

        /// <summary>
        /// Sets the street address for an entry in the housing history.
        /// </summary>
        /// <param name="entry">The housing history entry.</param>
        /// <param name="value">The street address.</param>
        public static void SetStreetAddress(HousingHistoryEntry entry, string value) => entry.Address = UpdateStreetAddress(entry.Address, value);

        /// <summary>
        /// Retruns an address instance with the street address updated to the specified value.
        /// </summary>
        /// <param name="address">The address object.</param>
        /// <param name="value">The street address.</param>
        /// <returns>The address object with the specified street address.</returns>
        public static PostalAddress UpdateStreetAddress(PostalAddress address, string value) => UpdateAddress(address, StreetAddress.Create(value));

        /// <summary>
        /// Gets the city for an entry in the housing history.
        /// </summary>
        /// <param name="entry">The housing history entry.</param>
        /// <returns>The city of the address.</returns>
        public static string GetCity(HousingHistoryEntry entry) => GetCity(entry?.Address);

        /// <summary>
        /// Gets the city for an address.
        /// </summary>
        /// <param name="address">The address object.</param>
        /// <returns>The city of the address.</returns>
        public static string GetCity(PostalAddress address) => address?.City.ToString() ?? string.Empty;

        /// <summary>
        /// Sets the city for an entry in the housing history.
        /// </summary>
        /// <param name="entry">The housing history entry.</param>
        /// <param name="value">The city of the address.</param>
        public static void SetCity(HousingHistoryEntry entry, string value) => entry.Address = UpdateCity(entry.Address, value);

        /// <summary>
        /// Retruns an address instance with the city updated to the specified value.
        /// </summary>
        /// <param name="address">The address object.</param>
        /// <param name="value">The city of the address.</param>
        /// <returns>The address object with the specified city.</returns>
        public static PostalAddress UpdateCity(PostalAddress address, string value) => UpdateAddress(address, City.Create(value));

        /// <summary>
        /// Gets the state for an entry in the housing history.
        /// </summary>
        /// <param name="entry">The housing history entry.</param>
        /// <returns>The state.</returns>
        public static string GetState(HousingHistoryEntry entry) => entry?.Address?.State.ToString() ?? string.Empty;

        /// <summary>
        /// Gets the state for an address.
        /// </summary>
        /// <param name="address">The address object.</param>
        /// <returns>The zipcode.</returns>
        public static string GetState(PostalAddress address) => address?.State.ToString() ?? string.Empty;

        /// <summary>
        /// Sets the state for an entry in the housing history.
        /// </summary>
        /// <param name="entry">The housing history entry.</param>
        /// <param name="value">The state.</param>
        public static void SetState(HousingHistoryEntry entry, string value) => entry.Address = UpdateState(entry.Address, value);

        /// <summary>
        /// Retruns an address instance with the state updated to the specified value.
        /// </summary>
        /// <param name="address">The address object.</param>
        /// <param name="value">The state.</param>
        /// <returns>The address object with the specified state.</returns>
        public static PostalAddress UpdateState(PostalAddress address, string value) => UpdateAddress(address, UnitedStatesPostalState.CreateWithValidation(value));

        /// <summary>
        /// Gets the zipcode for an entry in the housing history.
        /// </summary>
        /// <param name="entry">The housing history entry.</param>
        /// <returns>The zipcode.</returns>
        public static string GetZipcode(HousingHistoryEntry entry) => entry?.Address?.PostalCode.ToString() ?? string.Empty;

        /// <summary>
        /// Gets the zipcode for an address.
        /// </summary>
        /// <param name="address">The address object.</param>
        /// <returns>The zipcode.</returns>
        public static string GetZipcode(PostalAddress address) => address?.PostalCode.ToString() ?? string.Empty;

        /// <summary>
        /// Sets the zipcode for an entry in the housing history.
        /// </summary>
        /// <param name="entry">The housing history entry.</param>
        /// <param name="value">The zipcode.</param>
        public static void SetZipcode(HousingHistoryEntry entry, string value) => entry.Address = UpdateZipcode(entry.Address, value);

        /// <summary>
        /// Retruns an address instance with the zipcode updated to the specified value.
        /// </summary>
        /// <param name="address">The address object.</param>
        /// <param name="value">The zipcode.</param>
        /// <returns>The address object with the specified zipcode.</returns>
        public static PostalAddress UpdateZipcode(PostalAddress address, string value) => UpdateAddress(address, Zipcode.CreateWithValidation(value));

        /// <summary>
        /// Gets the address type for an entry in the housing history.
        /// </summary>
        /// <param name="entry">The housing history entry.</param>
        /// <returns>The address type.</returns>
        public static E_aBAddrT GetAddressType(HousingHistoryEntry entry) => GetAddressType(entry?.ResidencyType);

        /// <summary>
        /// Sets the address type for an entry in the housing history.
        /// </summary>
        /// <param name="entry">The housing history entry.</param>
        /// <param name="value">The address type.</param>
        public static void SetAddressType(HousingHistoryEntry entry, E_aBAddrT value) => entry.ResidencyType = GetResidencyType(value);

        /// <summary>
        /// Gets the years at the address for an entry in the housing history.
        /// </summary>
        /// <param name="entry">The housing history entry.</param>
        /// <returns>The years at the address.</returns>
        public static string GetYearsAtAddress(HousingHistoryEntry entry)
        {
            string yearsString = (entry?.TimeAtAddress?.Value / 12M)?.ToString() ?? string.Empty;
            if (yearsString.Length > 4)
            {
                yearsString = yearsString.Remove(4);
            }

            if (yearsString.Contains("."))
            {
                yearsString = yearsString.TrimEnd('0');
            }

            return yearsString.TrimEnd('.');
        }

        /// <summary>
        /// Sets the years at the address for an entry in the housing history.
        /// </summary>
        /// <param name="entry">The housing history entry.</param>
        /// <param name="value">The years at the address.</param>
        public static void SetYearsAtAddress(HousingHistoryEntry entry, string value)
        {
            decimal? years = value.ToNullable<decimal>(decimal.TryParse);
            entry.TimeAtAddress = years.HasValue ? Count<UnitType.Month>.Create((int)Math.Round(years.Value * 12, MidpointRounding.AwayFromZero)) : null;
        }

        /// <summary>
        /// Gets the identifier for the <see cref="HousingHistoryEntry"/> specified by <paramref name="fieldName"/>.
        /// </summary>
        /// <param name="fieldName">The name of the field.</param>
        /// <returns>The <see cref="HousingHistoryEntryId"/> representing the field, or null if no such field exists.</returns>
        public HousingHistoryEntryId? GetEntryIdentifier(string fieldName)
        {
            return this.shimmedEntries.GetNullableValue(GetEntryName(fieldName));
        }

        /// <summary>
        /// Creates a new instance of <see cref="HousingHistoryEntry"/> representing the specified field name.
        /// </summary>
        /// <param name="fieldName">The name of the field.</param>
        /// <returns>The new housing history entry.</returns>
        public HousingHistoryEntry CreateNewHousingHistoryEntry(string fieldName)
        {
            HousingHistoryEntry entry = new HousingHistoryEntry { IsPresentAddress = IsPresentAddress(fieldName), };
            Func<HousingHistoryEntry, HousingHistoryEntryId> addEntry = IsCoborrowerShimField(fieldName) ? this.addCoboHousingHistoryEntry : this.addBorrHousingHistoryEntry;
            this.shimmedEntries.Add(GetEntryName(fieldName), addEntry(entry));
            return entry;
        }

        /// <summary>
        /// Gets the name of the <see cref="HousingHistoryEntry"/> as stored in <see cref="shimmedEntries"/>.
        /// </summary>
        /// <param name="fieldName">The name of the field.</param>
        /// <returns>The name of the entry.</returns>
        private static string GetEntryName(string fieldName)
        {
            bool isPresentAddress = IsPresentAddress(fieldName);
            return isPresentAddress ? fieldName.Remove(2) : fieldName.Remove(7); // This will be a(B|C)(Prev(1|2))?
        }

        /// <summary>
        /// Indicates if the field is the present address.
        /// </summary>
        /// <param name="fieldName">The name of the field.</param>
        /// <returns><see langword="false"/> if <paramref name="fieldName"/> refers to a previous address field; <see langword="true"/> otherwise.</returns>
        private static bool IsPresentAddress(string fieldName)
        {
            return fieldName.IndexOf("Prev", 2) != 2; // The field names are of the form a(B|C)(Prev(1|2))?(Addr|City|State|Zip|AddrT|AddrYrs)
        }

        /// <summary>
        /// Indicates if the field is the co-borrower's field (rather than the borrower's).
        /// </summary>
        /// <param name="fieldName">The name of the field.</param>
        /// <returns><see langword="false"/> if <paramref name="fieldName"/> refers to a borrower field; <see langword="true"/> if it refers to a co-borrower field.</returns>
        private static bool IsCoborrowerShimField(string fieldName)
        {
            if (fieldName.StartsWith("aB"))
            {
                return false;
            }
            else if (fieldName.StartsWith("aC"))
            {
                return true;
            }

            throw CBaseException.GenericException("Invalid field for shim \"" + fieldName + "\".");
        }

        /// <summary>
        /// Returns a version of <paramref name="address"/> with the street address updated.
        /// </summary>
        /// <param name="address">The address to update.</param>
        /// <param name="streetAddress">The new street address.</param>
        /// <returns>An updated address, or <paramref name="address"/> if no update is necessary.</returns>
        private static PostalAddress UpdateAddress(PostalAddress address, StreetAddress? streetAddress)
        {
            if (streetAddress == address?.StreetAddress)
            {
                return address;
            }

            UnitedStatesPostalAddress.Builder builder = GetBuilder(address);
            builder.UnparsedStreetAddress = streetAddress;
            builder.AddressNumber = null;
            builder.StreetPreDirection = null;
            builder.StreetName = null;
            builder.StreetSuffix = null;
            builder.StreetPostDirection = null;
            builder.UnitType = null;
            builder.UnitIdentifier = null;
            return builder.GetAddress();
        }

        /// <summary>
        /// Returns a version of <paramref name="address"/> with the city updated.
        /// </summary>
        /// <param name="address">The address to update.</param>
        /// <param name="city">The new city.</param>
        /// <returns>An updated address, or <paramref name="address"/> if no update is necessary.</returns>
        private static PostalAddress UpdateAddress(PostalAddress address, City? city)
        {
            if (city == address?.City)
            {
                return address;
            }

            UnitedStatesPostalAddress.Builder builder = GetBuilder(address);
            builder.City = city;
            return builder.GetAddress();
        }

        /// <summary>
        /// Returns a version of <paramref name="address"/> with the state updated.
        /// </summary>
        /// <param name="address">The address to update.</param>
        /// <param name="state">The new state.</param>
        /// <returns>An updated address, or <paramref name="address"/> if no update is necessary.</returns>
        private static PostalAddress UpdateAddress(PostalAddress address, UnitedStatesPostalState? state)
        {
            UnitedStatesPostalAddress.Builder builder = GetBuilder(address);
            if (state == builder?.UsState)
            {
                return address;
            }

            builder.UsState = state;
            return builder.GetAddress();
        }

        /// <summary>
        /// Returns a version of <paramref name="address"/> with the zipcode updated.
        /// </summary>
        /// <param name="address">The address to update.</param>
        /// <param name="zipcode">The new zipcode.</param>
        /// <returns>An updated address, or <paramref name="address"/> if no update is necessary.</returns>
        private static PostalAddress UpdateAddress(PostalAddress address, Zipcode? zipcode)
        {
            UnitedStatesPostalAddress.Builder builder = GetBuilder(address);
            if (zipcode == builder?.Zipcode)
            {
                return address;
            }

            builder.Zipcode = zipcode;
            return builder.GetAddress();
        }

        /// <summary>
        /// Gets a builder representing the <paramref name="address"/> instance.
        /// </summary>
        /// <param name="address">The address to create a builder from.</param>
        /// <returns>A builder for a United States postal address.</returns>
        private static UnitedStatesPostalAddress.Builder GetBuilder(PostalAddress address)
        {
            if (address != null && !(address is UnitedStatesPostalAddress))
            {
                throw CBaseException.GenericException("The shim only handles United States addresses.");
            }

            return ((UnitedStatesPostalAddress)address)?.GetBuilder() ?? new UnitedStatesPostalAddress.Builder();
        }

        /// <summary>
        /// Gets the value of <see cref="E_aBAddrT"/> for the specified <see cref="ResidencyType?"/>.
        /// </summary>
        /// <param name="residencyType">The residency type.</param>
        /// <returns>The address type.</returns>
        private static E_aBAddrT GetAddressType(ResidencyType? residencyType)
        {
            switch (residencyType)
            {
                case null: return E_aBAddrT.LeaveBlank;
                case ResidencyType.Own: return E_aBAddrT.Own;
                case ResidencyType.Rent: return E_aBAddrT.Rent;
                case ResidencyType.LivingRentFree: return E_aBAddrT.LivingRentFree;
                default: throw new UnhandledEnumException(residencyType);
            }
        }

        /// <summary>
        /// Gets the value of <see cref="ResidencyType?"/> for the specified <see cref="E_aBAddrT"/>.
        /// </summary>
        /// <param name="addressType">The address type.</param>
        /// <returns>The residency type.</returns>
        private static ResidencyType? GetResidencyType(E_aBAddrT addressType)
        {
            switch (addressType)
            {
                case E_aBAddrT.LeaveBlank: return null;
                case E_aBAddrT.Own: return ResidencyType.Own;
                case E_aBAddrT.Rent: return ResidencyType.Rent;
                case E_aBAddrT.LivingRentFree: return ResidencyType.LivingRentFree;
                default: throw new UnhandledEnumException(addressType);
            }
        }
    }
}
