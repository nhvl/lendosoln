﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using LendersOffice.CreditReport;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Shim that presents an IPublicRecord interface around a PublicRecord entity.
    /// </summary>
    public sealed class PublicRecordShim : CollectionItemBase2Shim, IPublicRecord, IRememberBorrowers
    {
        /// <summary>
        /// Name to use for the DataTable constructed to pass out data for binding to user controls.
        /// </summary>
        private const string TableName = "PublicRecordXmlContent";

        /// <summary>
        /// The application this record is associated with.
        /// </summary>
        private IShimOwnershipManager<Guid> ownershipManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="PublicRecordShim"/> class.
        /// </summary>
        /// <param name="shimContainer">Reference to the container that holds all shims.</param>
        /// <param name="collectionShim">The shim for the collection that contains this entity shim.</param>
        /// <param name="contained">The PublicRecord entity that holds the data.</param>
        /// <param name="ownershipManager">The ownership manager.</param>
        /// <param name="recordId">The identifier for this public record.</param>
        /// <param name="stringFormatter">Object that controls how semantic types are serialized as strings.</param>
        /// <param name="numericFormatter">Object that controls how semantic types are converted to numeric values.</param>
        /// <param name="stringParser">Object that controls how strings are parsed into semantic types.</param>
        private PublicRecordShim(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            LendingQB.Core.Data.PublicRecord contained,
            IShimOwnershipManager<Guid> ownershipManager,
            DataObjectIdentifier<DataObjectKind.PublicRecord, Guid> recordId,
            IFormatAsString stringFormatter,
            IFormatAsNumeric numericFormatter,
            IParseFromString stringParser)
            : base(shimContainer, collectionShim, recordId.Value, stringFormatter, numericFormatter, stringParser)
        {
            this.Contained = contained;
            this.ownershipManager = ownershipManager;
        }

        /// <summary>
        /// Gets the audit records.
        /// </summary>
        /// <value>The audit records.</value>
        public List<CPublicRecordAuditItem> AuditTrailItems
        {
            get
            {
                return this.Contained.AuditTrail;
            }
        }

        /// <summary>
        /// Gets the bankruptcy liabilities amount.
        /// </summary>
        /// <value>The bankruptcy liabilities amount.</value>
        public decimal BankruptcyLiabilitiesAmount
        {
            get
            {
                return this.NumericFormatter.Format(this.Contained.BankruptcyLiabilitiesAmount);
            }
        }

        /// <summary>
        /// Gets or sets the bankruptcy liabilities amount as a string.
        /// </summary>
        /// <value>The bankruptcy liabilities amount as a string.</value>
        public string BankruptcyLiabilitiesAmount_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.BankruptcyLiabilitiesAmount);
            }

            set
            {
                this.Contained.BankruptcyLiabilitiesAmount = this.StringParser.TryParseMoney(value);
            }
        }

        /// <summary>
        /// Gets the date when the bankruptcy was filed.
        /// </summary>
        /// <value>The date when the bankruptcy was filed.</value>
        public CDateTime BkFileD
        {
            get
            {
                return this.UnzonedDateToCDateTime(this.Contained.BkFileDate);
            }
        }

        /// <summary>
        /// Gets or sets the date when the bankruptcy was filed as a string.
        /// </summary>
        /// <value>The date when the bankruptcy was filed as a string.</value>
        public string BkFileD_rep
        {
            get
            {
                return this.FormatUnzonedDate(this.BkFileD, this.Contained.BkFileDate);
            }

            set
            {
                this.Contained.BkFileDate = this.StringParser.TryParseUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the name of the court where the public record is filed.
        /// </summary>
        /// <value>The name of the court where the public record is filed.</value>
        public string CourtName
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.CourtName);
            }

            set
            {
                this.Contained.CourtName = this.StringParser.TryParseEntityName(value);
            }
        }

        /// <summary>
        /// Gets the date when the most recent disposition change was made on the court case.
        /// </summary>
        /// <value>The date when the decision was made on the court case.</value>
        public CDateTime DispositionD
        {
            get
            {
                return this.UnzonedDateToCDateTime(this.Contained.DispositionDate);
            }
        }

        /// <summary>
        /// Gets or sets the date when the most recent disposition change was made on the court case as a string.
        /// </summary>
        /// <value>The date when the decision was made on the court case as a string.</value>
        public string DispositionD_rep
        {
            get
            {
                return this.FormatUnzonedDate(this.DispositionD, this.Contained.DispositionDate);
            }

            set
            {
                this.Contained.DispositionDate = this.StringParser.TryParseUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the state of the current disposition for the court case.
        /// </summary>
        /// <value>The state of the current disposition for the court case.</value>
        public E_CreditPublicRecordDispositionType DispositionT
        {
            get
            {
                if (this.Contained.DispositionType == null)
                {
                    return E_CreditPublicRecordDispositionType.Unknown;
                }
                else
                {
                    return this.Contained.DispositionType.Value;
                }
            }

            set
            {
                this.Contained.DispositionType = value;
            }
        }

        /// <summary>
        /// Gets or sets the identifier for the public record from the credit report data.
        /// </summary>
        /// <value>The identifier for the public record from the credit report data.</value>
        public string IdFromCreditReport
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.IdFromCreditReport);
            }

            set
            {
                this.Contained.IdFromCreditReport = this.StringParser.TryParseThirdPartyIdentifier(value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the public record should be included in pricing calculations.
        /// </summary>
        /// <value>A value indicating whether the public record should be included in pricing calculations.</value>
        public bool IncludeInPricing
        {
            get
            {
                return this.BooleanFromNullable(this.Contained.IncludeInPricing);
            }

            set
            {
                this.Contained.IncludeInPricing = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this public record has been modified.
        /// </summary>
        /// <value>A value indicating whether this public record has been modified.</value>
        public bool IsModified
        {
            get
            {
                var audit = this.Contained.AuditTrail;
                return audit != null && audit.Count > 0;
            }
        }

        /// <summary>
        /// Gets or sets a type for an enumeration that is used as a key.
        /// </summary>
        public Enum KeyType
        {
            get
            {
                return this.Contained.Type;
            }

            set
            {
                this.Contained.Type = (E_CreditPublicRecordType)value;
            }
        }

        /// <summary>
        /// Gets the last effective date.
        /// </summary>
        /// <value>The last effective date.</value>
        public CDateTime LastEffectiveD
        {
            get
            {
                return this.UnzonedDateToCDateTime(this.Contained.LastEffectiveDate);
            }
        }

        /// <summary>
        /// Gets or sets the last effective date as a string.
        /// </summary>
        /// <value>The last effective date as a string.</value>
        public string LastEffectiveD_rep
        {
            get
            {
                return this.FormatUnzonedDate(this.LastEffectiveD, this.Contained.LastEffectiveDate);
            }

            set
            {
                this.Contained.LastEffectiveDate = this.StringParser.TryParseUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the owner of the public record.
        /// </summary>
        /// <value>The owner of the public record.</value>
        public E_PublicRecordOwnerT OwnerT
        {
            get
            {
                return (E_PublicRecordOwnerT)this.ownershipManager.GetOwnership(this.RecordId);
            }

            set
            {
                this.ownershipManager.SetOwnership(this.RecordId, (Ownership)value);
            }
        }

        /// <summary>
        /// Gets the date when the public record was reported to the credit repositories.
        /// </summary>
        /// <value>The date when the public record was reported to the credit repositories.</value>
        public CDateTime ReportedD
        {
            get
            {
                return this.UnzonedDateToCDateTime(this.Contained.ReportedDate);
            }
        }

        /// <summary>
        /// Gets or sets the date when the public record was reported to the credit repositories as a string.
        /// </summary>
        /// <value>The date when the public record was reported to the credit repositories as a string.</value>
        public string ReportedD_rep
        {
            get
            {
                return this.FormatUnzonedDate(this.ReportedD, this.Contained.ReportedDate);
            }

            set
            {
                this.Contained.ReportedDate = this.StringParser.TryParseUnzonedDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the type of the public record.
        /// </summary>
        /// <value>The type of the public record.</value>
        public E_CreditPublicRecordType Type
        {
            get
            {
                return this.Contained.Type == null ? E_CreditPublicRecordType.Unknown : this.Contained.Type.Value;
            }

            set
            {
                this.Contained.Type = value;
            }
        }

        /// <summary>
        /// Gets or sets the PublicRecord entity.
        /// </summary>
        private LendingQB.Core.Data.PublicRecord Contained { get; set; }

        /// <summary>
        /// Method called to notify the implementing class that the roles of the 
        /// two borrowers have been exchanged.
        /// </summary>
        public void SwapBorrowers()
        {
            this.ownershipManager.SwapBorrowers();
        }

        /// <summary>
        /// Create a shim implementation of the IPublicRecord interface.
        /// </summary>
        /// <param name="shimContainer">Reference to the container that holds all shims.</param>
        /// <param name="collectionShim">The shim for the collection that contains this entity shim.</param>
        /// <param name="contained">The contained public record data.</param>
        /// <param name="ownershipManager">The ownership manager for the collection records.</param>
        /// <param name="recordId">The identifier for this public record.</param>
        /// <param name="converter">Instance of LosConvert used to format types as strings.</param>
        /// <returns>A shim for the IPublicRecord interface.</returns>
        internal static IPublicRecord Create(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            LendingQB.Core.Data.PublicRecord contained,
            IShimOwnershipManager<Guid> ownershipManager,
            DataObjectIdentifier<DataObjectKind.PublicRecord, Guid> recordId,
            LosConvert converter)
        {
            return Create(shimContainer, collectionShim, contained, ownershipManager, recordId, new FormatEntityShimAsString(converter), null, new ParseEntityShimFromString(converter));
        }

        /// <summary>
        /// Create a DataTable under the DataSet, and create the columns.
        /// </summary>
        /// <param name="ds">The DataSet to which the table will be attached.</param>
        /// <returns>The DataTable with columns initialized.</returns>
        internal static DataTable CreateAndAddDataTable(DataSet ds)
        {
            var table = ds.Tables.Add(TableName);
            table.Columns.Add("RecordId");
            table.Columns.Add("IdFromCreditReport");
            table.Columns.Add("CourtName");
            table.Columns.Add("ReportedD");
            table.Columns.Add("DispositionD");
            table.Columns.Add("DispositionT");
            table.Columns.Add("BankruptcyLiabilitiesAmount");
            table.Columns.Add("Type");
            table.Columns.Add("LastEffectiveD");
            table.Columns.Add("BkFileD");
            table.Columns.Add("OrderRankValue");
            table.Columns.Add("IncludeInPricing");
            table.Columns.Add("AuditTrailXmlContent");
            table.Columns.Add("OwnerT");

            return table;
        }

        /// <summary>
        /// Create a DataRow, populate it, and add it to the input table.
        /// </summary>
        /// <param name="table">The table to which the DataRow will be added.</param>
        /// <returns>The DataRow that was created.</returns>
        internal DataRow CreateAndAddDataRow(DataTable table)
        {
            var row = table.NewRow();
            row["RecordId"] = this.RecordId.ToString("D");
            row["IdFromCreditReport"] = this.IdFromCreditReport;
            row["CourtName"] = this.CourtName;
            row["ReportedD"] = this.ReportedD_rep;
            row["DispositionD"] = this.DispositionD_rep;
            row["DispositionT"] = ((int)this.DispositionT).ToString();
            row["BankruptcyLiabilitiesAmount"] = this.BankruptcyLiabilitiesAmount_rep;
            row["Type"] = ((int)this.Type).ToString();
            row["LastEffectiveD"] = this.LastEffectiveD_rep;
            row["BkFileD"] = this.BkFileD_rep;
            row["IncludeInPricing"] = this.IncludeInPricing.ToString();
            row["OwnerT"] = ((int)this.OwnerT).ToString();

            var auditMapper = new LendingQB.Core.Mapping.PublicRecordAuditTrailMapper();
            string audit = auditMapper.DatabaseRepresentation(this.AuditTrailItems);
            if (audit == null)
            {
                row["AuditTrailXmlContent"] = DBNull.Value;
            }
            else
            {
                row["AuditTrailXmlContent"] = audit;
            }

            this.DataRow = row;
            table.Rows.Add(row);
            return row;
        }

        /// <summary>
        /// Create a shim implementation of the IPublicRecord interface.
        /// </summary>
        /// <param name="shimContainer">Reference to the container that holds all shims.</param>
        /// <param name="collectionShim">The shim for the collection that contains this entity shim.</param>
        /// <param name="contained">The contained public record data.</param>
        /// <param name="ownershipManager">The ownership manager for the collection records.</param>
        /// <param name="recordId">The identifier for this public record.</param>
        /// <param name="stringFormatter">Object that implements formatting semantic types into string values.</param>
        /// <param name="numericFormatter">Object that implements conversion of semantic types into numeric values.</param>
        /// <param name="stringParser">Object that implements parsing strings into semantic types.</param>
        /// <returns>A shim for the IPublicRecord interface.</returns>
        private static IPublicRecord Create(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            LendingQB.Core.Data.PublicRecord contained,
            IShimOwnershipManager<Guid> ownershipManager,
            DataObjectIdentifier<DataObjectKind.PublicRecord, Guid> recordId,
            IFormatAsString stringFormatter,
            IFormatAsNumeric numericFormatter,
            IParseFromString stringParser)
        {
            return new PublicRecordShim(shimContainer, collectionShim, contained, ownershipManager, recordId, stringFormatter ?? new FormatAsStringBase(), numericFormatter ?? new FormatAsNumericBase(), stringParser ?? new ParseFromStringBase());
        }
    }
}
