﻿namespace DataAccess
{
    using System;
    using LendersOffice;

    /// <summary>
    /// Provides the application data needed by collection / record shims.
    /// </summary>
    public interface IShimAppDataProvider
    {
        //// ---- Common ----

        /// <summary>
        /// Gets the application id.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        Guid aAppId { get; }

        /// <summary>
        /// Gets the borrower's consumer id.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        Guid aBConsumerId { get; }

        /// <summary>
        /// Gets the coborrower's consumer id.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        Guid aCConsumerId { get; }

        /// <summary>
        /// Gets the borrower's name.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        string aBNm { get; } // Used by asset and liability.

        //// ---- Assets ----

        /// <summary>
        /// Sets the liquid asset total.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        decimal aAsstLiqTot_ { set; }

        /// <summary>
        /// Sets the non-real estate illiquid assets total.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        decimal aAsstNonReSolidTot_ { set; }

        //// ---- Employment ----

        /// <summary>
        /// Gets a value indicating whether the borrower's employer business phone is locked.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        bool aBEmplrBusPhoneLckd { get; }

        /// <summary>
        /// Gets a value indicating whether the coborrower's employer business phone is locked.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        bool aCEmplrBusPhoneLckd { get; }

        /// <summary>
        /// Gets the borrower's business phone.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        string aBBusPhone { get; }

        /// <summary>
        /// Gets the coborrower's business phone.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        string aCBusPhone { get; }

        //// ---- Real Property ----

        /// <summary>
        /// Gets the occupancy type.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        E_aOccT aOccT { get; }

        /// <summary>
        /// Sets the real property total value.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        decimal aReTotVal_ { set; }

        /// <summary>
        /// Sets the real property total monthly amount.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        decimal aReTotMAmt_ { set; }

        /// <summary>
        /// Sets the real property total gross rent income.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        decimal aReTotGrossRentI_ { set; }

        /// <summary>
        /// Sets the real property total monthly payment.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        decimal aReTotMPmt_ { set; }

        /// <summary>
        /// Sets the real property total housing expenses.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        decimal aReTotHExp_ { set; }

        /// <summary>
        /// Sets the real property total net rent income.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        decimal aReTotNetRentI_ { set; }

        /// <summary>
        /// Sets the borrower's retained negative cashflow.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        decimal aBRetainedNegCf_ { set; }

        /// <summary>
        /// Sets the coborrower's retained negative cashflow.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        decimal aCRetainedNegCf_ { set; }

        /// <summary>
        /// Sets the borrower's net rent income.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        decimal aBNetRentI_ { set; }

        /// <summary>
        /// Sets the coborrower's net rent income.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        decimal aCNetRentI_ { set; }

        /// <summary>
        /// Sets the borrower's net negative cashflow.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        decimal aBNetNegCf_ { set; }

        /// <summary>
        /// Sets the coborrower's net negative cashflow.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        decimal aCNetNegCf_ { set; }

        //// ---- Liability ----

        /// <summary>
        /// Gets the borrower's retained negative cashflow.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        decimal aBRetainedNegCf { get; }

        /// <summary>
        /// Gets the coborrower's retained negative cashflow.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        decimal aCRetainedNegCf { get; }

        /// <summary>
        /// Sets the liability aggregate totals.
        /// </summary>
        /// <param name="aggregateData">The aggregate data.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        void AcceptLiabilityAggregateTotals(CalculatedFields.Liability.AggregateData aggregateData);
    }
}