﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    using InterfaceDefs = LendingQB.Core;
    using ShimTarget = LendingQB.Core.Data;

    /// <summary>
    /// Shim for a VA previous loan collection.
    /// </summary>
    internal sealed class VaPastLoanCollectionShim : RecordCollectionShim<DataObjectKind.VaPreviousLoan, ShimTarget.VaPreviousLoan>, IVaPastLoanCollection
    {
        /// <summary>
        /// The ownership manager for the VA previous loans.
        /// </summary>
        private IShimOwnershipManager<Guid> ownershipManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="VaPastLoanCollectionShim"/> class.
        /// </summary>
        /// <param name="shimContainer">Reference to the container that holds all shims.</param>
        /// <param name="ownershipManager">The ownership manager for the collection records.</param>
        /// <param name="converter">An instance of LosConvert used for formatting the data.</param>
        public VaPastLoanCollectionShim(
            IShimContainer shimContainer,
            IShimOwnershipManager<Guid> ownershipManager,
            LosConvert converter)
            : base(shimContainer, ownershipManager.AppId, new DisallowSpecialRecords(), converter)
        {
            this.ownershipManager = ownershipManager;

            var ds = this.GetDataSet();
            var table = VaPastLoanShim.CreateAndAddDataTable(ds);
            this.RebuildShimData();
        }

        /// <summary>
        /// Create and add a new record of the specified type.
        /// </summary>
        /// <param name="recordT">The type of record to create.</param>
        /// <returns>The new record.</returns>
        public ICollectionItemBase2 AddRecord(Enum recordT)
        {
            return this.AddRecord((E_VaPastLT)recordT);
        }

        /// <summary>
        /// Adds a record of the specified type.
        /// </summary>
        /// <param name="pastLT">The type of record to add.</param>
        /// <returns>The new record.</returns>
        public IVaPastLoan AddRecord(E_VaPastLT pastLT)
        {
            // E_VaPastLT only has one value, Regular
            return this.AddRegularRecord();
        }

        /// <summary>
        /// Adds a new VA past loan record.
        /// </summary>
        /// <returns>The new VA past loan record.</returns>
        public IVaPastLoan AddRegularRecord()
        {
            var item = new ShimTarget.VaPreviousLoan();
            var id = this.AddEntity(item);
            this.RebuildShimData();

            return this.GetRegRecordOf(id.Value);
        }

        /// <summary>
        /// Adds a regular record at the specified index.
        /// </summary>
        /// <param name="pos">The index.</param>
        /// <returns>The new record.</returns>
        public IVaPastLoan AddRegularRecordAt(int pos)
        {
            var item = new ShimTarget.VaPreviousLoan();
            var id = this.InsertRegularRecordAt(item, pos);
            return this.GetRegRecordOf(id.Value);
        }

        /// <summary>
        /// Ensures that a record exists at the given index, creating new records as needed.
        /// </summary>
        /// <param name="pos">The index.</param>
        /// <returns>The record associated with that index.</returns>
        public IVaPastLoan EnsureRegularRecordAt(int pos)
        {
            for (int i = this.CountRegular - 1; i < pos; ++i)
            {
                this.AddRegularRecord();
            }

            return this.GetRegularRecordAt(pos);
        }

        /// <summary>
        /// If there is no record with the indicated identifier, create one.  Return the record with the indicated identifier.
        /// </summary>
        /// <param name="recordId">The identifier that the returned record should have.</param>
        /// <returns>The record with the specified identifier.</returns>
        public ICollectionItemBase2 EnsureRegularRecordOf(Guid recordId)
        {
            var shim = this.TryGetRegularRecordOf(recordId);
            if (shim == null)
            {
                var identifier = DataObjectIdentifier<DataObjectKind.VaPreviousLoan, Guid>.Create(recordId);
                var item = new ShimTarget.VaPreviousLoan();
                this.AddEntity(identifier, item);
                this.RebuildShimData();

                shim = this.GetRegRecordOf(recordId);
            }

            return shim;
        }

        /// <summary>
        /// Gets the record associated with the given id.
        /// </summary>
        /// <param name="recordId">The record id.</param>
        /// <returns>The record associated with the id.</returns>
        public IVaPastLoan GetRegRecordOf(Guid recordId)
        {
            return (IVaPastLoan)this.GetRegularRecordOf(recordId);
        }

        /// <summary>
        /// Gets the record at the specified index.
        /// </summary>
        /// <param name="pos">The index.</param>
        /// <returns>The record for that index.</returns>
        public IVaPastLoan GetRegularRecordAt(int pos)
        {
            return (IVaPastLoan)this.GetRegularItemAt(pos);
        }

        /// <summary>
        /// Retrieve the special record at the specified position.
        /// </summary>
        /// <param name="i">The position.</param>
        /// <returns>The record at the specified position, or throw an out of bounds exception.</returns>
        public ICollectionItemBase2 GetSpecialRecordAt(int i)
        {
            // The exception type is what the legacy class VaPastLoanCollection would give, but the error message here is more explicit than the canned .net framework message.
            throw new IndexOutOfRangeException("VaPastLoanCollection doesn't support special records.");
        }

        /// <summary>
        /// Gets a subset of the records.
        /// </summary>
        /// <param name="inclusiveGroup">
        /// A value indicating whether the given group(s) should be included or excluded.
        /// </param>
        /// <param name="group">The record group types to either include or exclude.</param>
        /// <returns>The specified subset of records.</returns>
        public ISubcollection GetSubcollection(bool inclusiveGroup, E_VaPastLGroupT group)
        {
            return this.GetSubcollectionCore(inclusiveGroup, group);
        }

        /// <summary>
        /// Create and add a new record.
        /// </summary>
        /// <returns>The new record.</returns>
        ICollectionItemBase2 IRecordCollection.AddRegularRecord()
        {
            return this.AddRegularRecord();
        }

        /// <summary>
        /// Create and add a new record at the specified position.
        /// </summary>
        /// <param name="pos">The position into which the record should be inserted.</param>
        /// <returns>The new record.</returns>
        ICollectionItemBase2 IRecordCollection.AddRegularRecordAt(int pos)
        {
            return this.AddRegularRecordAt(pos);
        }

        /// <summary>
        /// Retrieve the record with the specified identifier.
        /// </summary>
        /// <param name="recordId">The identifier for the desired record.</param>
        /// <returns>The desired record, or throw an exception.</returns>
        ICollectionItemBase2 IRecordCollection.GetRegRecordOf(Guid recordId)
        {
            return this.GetRegRecordOf(recordId);
        }

        /// <summary>
        /// Retrieve the regular record at the specified position.
        /// </summary>
        /// <param name="pos">The position.</param>
        /// <returns>The record at the specified position, or throw an out of bounds exception.</returns>
        ICollectionItemBase2 IRecordCollection.GetRegularRecordAt(int pos)
        {
            return this.GetRegularRecordAt(pos);
        }

        /// <summary>
        /// Build up the shim data.
        /// </summary>
        protected override void BuildShimData()
        {
            var ds = this.GetDataSet();
            var table = ds.Tables[0];
            foreach (var keyValue in this.GetAllCollectionEntities())
            {
                // Note that the key value pairs should be returned in the correct order because
                // the collection should be pre-ordered for us.
                var shim = VaPastLoanShim.Create(this.ShimContainer, this, keyValue.Value, keyValue.Key, this.Converter);
                this.RegularRecords.Add(shim);

                var vapl = (VaPastLoanShim)shim;
                var row = vapl.CreateAndAddDataRow(table);
            }
        }

        /// <summary>
        /// Adds the given record to the underlying collection via the aggregate.
        /// </summary>
        /// <param name="value">The record to add.</param>
        /// <returns>The identifier of the new record.</returns>
        protected override DataObjectIdentifier<DataObjectKind.VaPreviousLoan, Guid> AddEntity(ShimTarget.VaPreviousLoan value)
        {
            return this.ShimContainer.Add(this.ownershipManager.DefaultOwner, value);
        }

        /// <summary>
        /// Adds the given id and record to the underlying collection via the aggregate.
        /// </summary>
        /// <param name="recordId">The id to use.</param>
        /// <param name="value">The record to add.</param>
        protected override void AddEntity(DataObjectIdentifier<DataObjectKind.VaPreviousLoan, Guid> recordId, ShimTarget.VaPreviousLoan value)
        {
            this.ShimContainer.Add(this.ownershipManager.DefaultOwner, recordId, value);
        }

        /// <summary>
        /// Removes a record from the underlying collection via the aggregate.
        /// </summary>
        /// <param name="recordId">The record id.</param>
        protected override void RemoveEntity(DataObjectIdentifier<DataObjectKind.VaPreviousLoan, Guid> recordId)
        {
            this.ShimContainer.RemoveVaPreviousLoanFromLegacyApplication(recordId, this.ownershipManager.AppId, this.ownershipManager.BorrowerId, this.ownershipManager.CoborrowerId);
        }

        /// <summary>
        /// Gets the collection records relevant to the shimmed application.
        /// </summary>
        /// <returns>The collection records relevant to the shimmed application.</returns>
        protected override IEnumerable<KeyValuePair<DataObjectIdentifier<DataObjectKind.VaPreviousLoan, Guid>, ShimTarget.VaPreviousLoan>> GetAllCollectionEntities()
        {
            return this.ShimContainer.GetVaPreviousLoansForLegacyApplication(this.ownershipManager.AppId, this.ownershipManager.BorrowerId, this.ownershipManager.CoborrowerId);
        }
    }
}
