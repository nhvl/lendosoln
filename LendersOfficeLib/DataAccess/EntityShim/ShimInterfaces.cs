﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using LendersOffice;
    using LendersOffice.Admin;
    using LendersOffice.Migration;
    using LendingQB.Core;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Interface that adds the ability to remove an entity shim from a shim collection.
    /// </summary>
    /// <remarks>
    /// In the legacy implementation this was done via the dataset, but now the dataset is
    /// relegated to a subservient role.
    /// </remarks>
    public interface IRemoveEntityShim
    {
        /// <summary>
        /// Remove the shim from the shim collection.
        /// </summary>
        /// <param name="shim">The shim to remove.</param>
        void Remove(ICollectionItemBase2 shim);
    }

    /// <summary>
    /// Interface used when a swap borrower operation must be propagated to 
    /// the shims so that ownership determinations can continue to be correct.
    /// </summary>
    public interface IRememberBorrowers
    {
        /// <summary>
        /// Method called to notify the implementing class that the roles of the 
        /// two borrowers have been exchanged.
        /// </summary>
        void SwapBorrowers();
    }

    /// <summary>
    /// Interface for a class that can determine whether or not a particular entity shim is a special record.
    /// </summary>
    public interface IDetermineSpecialEntity
    {
        /// <summary>
        /// Determine whether or not a particular record type is special.
        /// </summary>
        /// <param name="type">The record type.</param>
        /// <returns>True if the record type is determined to be a special record, false otherwise.</returns>
        bool IsSpecial(Enum type);

        /// <summary>
        /// Determine whether or not a particular entity shim is a special record.
        /// </summary>
        /// <param name="shim">The entity shim of interest.</param>
        /// <returns>True if the shim is determined to be a special record, false otherwise.</returns>
        bool IsSpecial(ICollectionItemBase2 shim);
    }

    /// <summary>
    /// Interface for working with entity and collection shims.
    /// </summary>
    /// <remarks>
    /// This may grow to be large.  It will contain methods for retrieving shims as well as methods
    /// used by shims when they need access to other shims.  When shims are removed from the system 
    /// this interface will be dropped.
    /// </remarks>
    public partial interface IShimContainer
    {
        /// <summary>
        /// Gets the collection of liabilities linked with a real property.
        /// </summary>
        /// <param name="realPropertyId">The identifier for the real property.</param>
        /// <returns>A list of the linked liabilities.</returns>
        List<CReFields.LiaDisplay> GetLiabilitesLinkedToRealProperty(Guid realPropertyId);

        /// <summary>
        /// Updates the id of an existing liability.
        /// </summary>
        /// <param name="existingId">The current id.</param>
        /// <param name="newId">The new id.</param>
        void UpdateLiabilityId(Guid existingId, Guid newId);

        /// <summary>
        /// Gets the identifier for the real property record associated with the given
        /// liability id.
        /// </summary>
        /// <param name="liabilityId">The id of the liability record.</param>
        /// <returns>The id of the associated real property or Guid.Empty if there isn't one.</returns>
        Guid GetRealPropertyIdLinkedToLiability(Guid liabilityId);

        /// <summary>
        /// Associates a liability with a real property.
        /// </summary>
        /// <param name="liabilityId">The liability id.</param>
        /// <param name="realPropertyId">The real property id.</param>
        void AssociateLiabilityWithRealProperty(Guid liabilityId, Guid realPropertyId);

        /// <summary>
        /// Gets a value indicating whether the real property is marked as the subject property.
        /// </summary>
        /// <param name="realPropertyId">The id of the real property.</param>
        /// <returns>A value indicating whether the property is marked as the subject property.</returns>
        bool IsRealPropertySubjectProperty(Guid realPropertyId);

        /// <summary>
        /// Associates the given liability with a subject property REO. If the
        /// subject property REO does not exist, it is created.
        /// </summary>
        /// <param name="liabilityId">The liability id.</param>
        void AssociateLiabilityWithSubjectPropertyReo(Guid liabilityId);

        /// <summary>
        /// Removes the real property association for the given liability if it exists.
        /// </summary>
        /// <param name="liabilityId">The liability id.</param>
        void RemoveRealPropertyAssociationForLiability(Guid liabilityId);

        /// <summary>
        /// Gets a value indicating whether the real property is sold.
        /// </summary>
        /// <param name="realPropertyId">The real property id.</param>
        /// <returns>True if the real property is being sold. False otherwise.</returns>
        bool IsRealPropertySold(Guid realPropertyId);

        /// <summary>
        /// Sets the ownership type for the given record.
        /// </summary>
        /// <typeparam name="TOwnedKind">The data object kind of the owned entity.</typeparam>
        /// <typeparam name="TAssociationKind">The data object kind of the ownership association.</typeparam>
        /// <typeparam name="TOwnedValue">The type of the owned entity.</typeparam>
        /// <typeparam name="TAssociationValue">The type of the association.</typeparam>
        /// <param name="ownedId">The identifier for the owned entity.</param>
        /// <param name="appId">The application identifier.</param>
        /// <param name="borrowerId">The borrower identifier.</param>
        /// <param name="coborrowerId">The coborrower identifier.</param>
        /// <param name="ownership">The ownership type to set.</param>
        /// <param name="associationFactory">Factory method for creation of instances of the ownership association.</param>
        void SetOwnership<TOwnedKind, TAssociationKind, TOwnedValue, TAssociationValue>(
            Guid ownedId,
            Guid appId,
            Guid borrowerId,
            Guid coborrowerId,
            Ownership ownership,
            Func<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, DataObjectIdentifier<TOwnedKind, Guid>, DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>, TAssociationValue> associationFactory)
                where TOwnedKind : DataObjectKind
                where TAssociationKind : DataObjectKind
                where TAssociationValue : IPrimaryOwnershipAssociation<TOwnedKind, Guid>;

        /// <summary>
        /// Gets the ownership type for the given record.
        /// </summary>
        /// <typeparam name="TOwnedKind">The data object kind of the owned entity.</typeparam>
        /// <typeparam name="TAssociationKind">The data object kind of the ownership association.</typeparam>
        /// <typeparam name="TOwnedValue">The type of the owned entity.</typeparam>
        /// <typeparam name="TAssociationValue">The type of the association.</typeparam>
        /// <param name="ownedId">The record id.</param>
        /// <param name="borrowerId">The borrower identifier.</param>
        /// <param name="coborrowerId">The coborrower identifier.</param>
        /// <returns>The ownership type for the given record id.</returns>
        Ownership GetOwnership<TOwnedKind, TAssociationKind, TOwnedValue, TAssociationValue>(Guid ownedId, Guid borrowerId, Guid coborrowerId)
            where TOwnedKind : DataObjectKind
            where TAssociationKind : DataObjectKind
            where TAssociationValue : IPrimaryOwnershipAssociation<TOwnedKind, Guid>;

        /// <summary>
        /// Clears <see cref="LendingQB.Core.Data.Liability.IsSp1stMtgData"/> for all liabilities.
        /// </summary>
        void ClearIsSp1stMtgDataForAllLiabilities();

        /// <summary>
        /// Gets the item ordering for a given application.
        /// </summary>
        /// <typeparam name="TValueKind">The type of the data object for which an order needs to be retrieved.</typeparam>
        /// <param name="appId">The application id.</param>
        /// <returns>The item ordering.</returns>
        IOrderedIdentifierCollection<TValueKind, Guid> GetOrderForApplication<TValueKind>(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId) where TValueKind : DataObjectKind;

        /// <summary>
        /// Gets the item ordering for a given consumer.
        /// </summary>
        /// <typeparam name="TValueKind">The type of the data object for which an order needs to be retrieved.</typeparam>
        /// <param name="consumerId">The consumer id.</param>
        /// <returns>The item ordering.</returns>
        IOrderedIdentifierCollection<TValueKind, Guid> GetOrderForConsumer<TValueKind>(DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId) where TValueKind : DataObjectKind;
    }

    /// <summary>
    /// Interface supported by a class that uses an LosConvert instance for formatting.
    /// </summary>
    public interface IContainLosConvert
    {
        /// <summary>
        /// Gets the LosConvert instance to the calling code.
        /// </summary>
        LosConvert Converter { get; }
    }

    /// <summary>
    /// Interface to provide loan data access to shims.
    /// </summary>
    public interface IShimLoanDataProvider
    {
        /// <summary>
        /// Gets a value indicating whether the loan is a refinance scenario.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        bool sIsRefinancing { get; }

        /// <summary>
        /// Gets the loan file version.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan field.")]
        LoanVersionT sLoanVersionT { get; }

        /// <summary>
        /// Gets the BrokerDB for this loan.
        /// </summary>
        BrokerDB BrokerDB { get; }
    }
}
