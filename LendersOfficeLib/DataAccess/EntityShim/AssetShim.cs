﻿namespace DataAccess
{
    using System;
    using System.Data;
    using LendersOffice.UI;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Shim that presents an IAsset interface around a Asset entity.
    /// </summary>
    public abstract class AssetShim : CollectionItemBase2Shim, IAsset, IRememberBorrowers
    {
        /// <summary>
        /// Name to use for the DataTable constructed to pass out data for binding to user controls.
        /// </summary>
        private const string TableName = "AssetXmlContent";

        /// <summary>
        /// The ownership manager for the asset.
        /// </summary>
        private IShimOwnershipManager<Guid> ownershipManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="AssetShim"/> class.
        /// </summary>
        /// <param name="shimContainer">Reference to the container that holds all shims.</param>
        /// <param name="collectionShim">The shim for the collection that contains this entity shim.</param>
        /// <param name="contained">The Asset entity that holds the data.</param>
        /// <param name="ownershipManager">The ownership manager for the collection records.</param>
        /// <param name="recordId">The identifier for this asset.</param>
        /// <param name="stringFormatter">Object that controls how semantic types are serialized as strings.</param>
        /// <param name="numericFormatter">Object that controls how semantic types are converted to numeric values.</param>
        /// <param name="stringParser">Object that controls how strings are parsed into semantic types.</param>
        protected AssetShim(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            LendingQB.Core.Data.Asset contained,
            IShimOwnershipManager<Guid> ownershipManager,
            DataObjectIdentifier<DataObjectKind.Asset, Guid> recordId,
            IFormatAsString stringFormatter,
            IFormatAsNumeric numericFormatter,
            IParseFromString stringParser)
            : base(shimContainer, collectionShim, recordId.Value, stringFormatter, numericFormatter, stringParser)
        {
            this.ownershipManager = ownershipManager;

            this.Contained = contained;
        }

        /// <summary>
        /// Gets the type of asset.
        /// </summary>
        /// <value>The type of asset.</value>
        public E_AssetT AssetT
        {
            get
            {
                return this.GetEnum(this.Contained.AssetType, E_AssetT.Checking);
            }
        }

        /// <summary>
        /// Gets or sets the text description of the asset.
        /// </summary>
        /// <value>The text description of the asset.</value>
        public string Desc
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.Desc);
            }

            set
            {
                this.Contained.Desc = this.StringParser.TryParseDescriptionField(value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the asset has just been created and is still empty of data.
        /// </summary>
        /// <value>A value indicating whether the asset has just been created and is still empty of data.</value>
        public bool IsEmptyCreated
        {
            get
            {
                return this.BooleanFromNullable(this.Contained.IsEmptyCreated);
            }

            set
            {
                this.Contained.IsEmptyCreated = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the asset can be quickly monetized.
        /// </summary>
        /// <value>A value indicating whether the asset can be quickly monetized.</value>
        public bool IsLiquidAsset
        {
            get
            {
                return this.BooleanFromNullable(this.Contained.IsLiquidAsset);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the asset is a generic, regular, type.
        /// </summary>
        /// <value>A value indicating whether the asset is a generic, regular, type.</value>
        public bool IsRegularType
        {
            get
            {
                return CAssetFields.IsTypeOfSpecialRecord(this.AssetT);
            }
        }

        /// <summary>
        /// Gets or sets the owner type of the asset.
        /// </summary>
        /// <value>The owner type of the asset.</value>
        public E_AssetOwnerT OwnerT
        {
            get
            {
                return (E_AssetOwnerT)this.ownershipManager.GetOwnership(this.RecordId);
            }

            set
            {
                this.ownershipManager.SetOwnership(this.RecordId, (Ownership)value);
            }
        }

        /// <summary>
        /// Gets the owner type of the asset.
        /// </summary>
        /// <value>The owner type of the asset.</value>
        public string OwnerT_rep
        {
            get
            {
                return CAssetFields.DisplayStringForOwnerT(this.OwnerT);
            }
        }

        /// <summary>
        /// Gets or sets the phone number that can be used to verify the asset.
        /// </summary>
        /// <value>The phone number that can be used to verify the asset.</value>
        public string PhoneNumber
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.Phone);
            }

            set
            {
                this.Contained.Phone = this.StringParser.TryParsePhoneNumber(value);
            }
        }

        /// <summary>
        /// Gets or sets the value of the asset.
        /// </summary>
        /// <value>The value of the asset.</value>
        public decimal Val
        {
            get
            {
                return this.NumericFormatter.Format(this.Contained.Value);
            }

            set
            {
                this.Contained.Value = this.NumericFormatter.Format(value);
            }
        }

        /// <summary>
        /// Gets or sets the value of the asset.
        /// </summary>
        /// <value>The value of the asset.</value>
        public string Val_rep
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.Value);
            }

            set
            {
                this.Contained.Value = this.StringParser.TryParseMoney(value);
            }
        }

        /// <summary>
        /// Gets or sets the enum keytype of the asset.
        /// </summary>
        public Enum KeyType
        {
            get
            {
                return this.Contained.AssetType;
            }

            set
            {
                this.Contained.AssetType = (E_AssetT)value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the asset is credited at closing.  If credit at closing,
        /// assets will not be included in the asset totals.  Instead, they'll be added
        /// to the adjustments.
        /// </summary>
        public virtual bool IsCreditAtClosing
        {
            get
            {
                return this.BooleanFromNullable(this.Contained.IsCreditAtClosing);
            }

            set
            {
                this.Contained.IsCreditAtClosingData = value;
            }
        }

        /// <summary>
        /// Gets or sets the Asset entity.
        /// </summary>
        protected LendingQB.Core.Data.Asset Contained { get; set; }

        /// <summary>
        /// Method called to notify the implementing class that the roles of the 
        /// two borrowers have been exchanged.
        /// </summary>
        public void SwapBorrowers()
        {
            this.ownershipManager.SwapBorrowers();
        }

        /// <summary>
        /// Bind from object model.
        /// </summary>
        /// <param name="objInputFieldModel">The input model.</param>
        public void BindFromObjectModel(SimpleObjectInputFieldModel objInputFieldModel)
        {
            // Related to new UI not supported by Shim.
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get input field model.
        /// </summary>
        /// <returns>The input field model.</returns>
        public SimpleObjectInputFieldModel ToInputFieldModel()
        {
            // Related to new UI not supported by Shim.
            throw new NotImplementedException();
        }

        /// <summary>
        /// Create a shim implementation of the IAsset interface.
        /// </summary>
        /// <param name="shimContainer">Reference to the container that holds all shims.</param>
        /// <param name="collectionShim">The shim for the collection that contains this entity shim.</param>
        /// <param name="appData">The associated app data.</param>
        /// <param name="contained">The contained asset data.</param>
        /// <param name="ownershipManager">The ownership manager for the collection records.</param>
        /// <param name="recordId">The identifier for this asset.</param>
        /// <param name="converter">The los convert to use.</param>
        /// <returns>A shim for the IAsset interface.</returns>
        internal static IAsset Create(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            IShimAppDataProvider appData,
            LendingQB.Core.Data.Asset contained,
            IShimOwnershipManager<Guid> ownershipManager,
            DataObjectIdentifier<DataObjectKind.Asset, Guid> recordId,
            LosConvert converter)
        {
            if (contained.AssetType.HasValue)
            {
                var assetType = contained.AssetType.Value;

                if (assetType == E_AssetT.Retirement)
                {
                    return AssetRetirementShim.Create(
                        shimContainer,
                        collectionShim,
                        contained,
                        ownershipManager,
                        recordId,
                        converter);
                }
                else if (assetType == E_AssetT.LifeInsurance)
                {
                    return AssetLifeInsuranceShim.Create(
                        shimContainer,
                        collectionShim,
                        contained,
                        ownershipManager,
                        recordId,
                        converter);
                }
                else if (assetType == E_AssetT.CashDeposit)
                {
                    return AssetCashDepositShim.Create(
                        shimContainer,
                        collectionShim,
                        contained,
                        ownershipManager,
                        recordId,
                        converter);
                }
                else if (assetType == E_AssetT.Business)
                {
                    return AssetBusinessShim.Create(
                        shimContainer,
                        collectionShim,
                        contained,
                        ownershipManager,
                        recordId,
                        converter);
                }
            }

            return AssetRegularShim.Create(
                shimContainer,
                collectionShim,
                appData,
                contained,
                ownershipManager,
                recordId,
                converter);
        }

        /// <summary>
        /// Create a DataTable under the DataSet, and create the columns.
        /// </summary>
        /// <param name="ds">The DataSet to which the table will be attached.</param>
        /// <returns>The DataTable with columns initialized.</returns>
        internal static DataTable CreateAndAddDataTable(DataSet ds)
        {
            var table = ds.Tables.Add(TableName);
            table.Columns.Add("RecordId");
            table.Columns.Add("AssetT");
            table.Columns.Add("Val");
            table.Columns.Add("Desc");
            table.Columns.Add("FaceVal");
            table.Columns.Add("AccNum");
            table.Columns.Add("AccNm");
            table.Columns.Add("ComNm");
            table.Columns.Add("StAddr");
            table.Columns.Add("City");
            table.Columns.Add("State");
            table.Columns.Add("Zip");
            table.Columns.Add("VerifReorderedD");
            table.Columns.Add("VerifSentD");
            table.Columns.Add("VerifRecvD");
            table.Columns.Add("VerifExpD");
            table.Columns.Add("OtherTypeDesc");
            table.Columns.Add("DepartmentName");
            table.Columns.Add("Attention");
            table.Columns.Add("PrepD");
            table.Columns.Add("IsSeeAttachment");
            table.Columns.Add("AssetCashDepositT");
            table.Columns.Add("GiftSource");
            table.Columns.Add("VerifSignatureImgId");
            table.Columns.Add("VerifSigningEmployeeId");
            table.Columns.Add("IsEmptyCreated");
            table.Columns.Add("PhoneNumber");
            table.Columns.Add("OwnerT");
            table.Columns.Add("OrderRankValue");
            table.Columns.Add("IsCreditAtClosingData");

            return table;
        }

        /// <summary>
        /// Create a DataRow, populate it, and add it to the input table.
        /// </summary>
        /// <param name="table">The table to which the DataRow will be added.</param>
        /// <returns>The DataRow that was created.</returns>
        internal virtual DataRow CreateAndAddDataRow(DataTable table)
        {
            var row = table.NewRow();
            row["RecordId"] = this.RecordId.ToString("D");
            row["AssetT"] = ((int)this.AssetT).ToString();
            row["Desc"] = this.Desc;
            row["IsEmptyCreated"] = this.IsEmptyCreated.ToString();
            row["PhoneNumber"] = this.PhoneNumber;
            row["Val"] = this.Val_rep;
            row["OwnerT"] = ((int)this.OwnerT).ToString();

            this.DataRow = row;
            table.Rows.Add(row);
            return row;
        }
    }
}
