﻿namespace DataAccess
{
    using System;
    using System.Linq;
    using LendingQB.Core;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Provides the functionality needed to manage application-level ownership for shim records.
    /// </summary>
    /// <typeparam name="TOwnedKind">The data object kind of the owned entity.</typeparam>
    /// <typeparam name="TAssociationKind">The data object kind of the ownership association.</typeparam>
    /// <typeparam name="TOwnedValue">The type of the owned entity.</typeparam>
    /// <typeparam name="TAssociationValue">The type of the association.</typeparam>
    public class ShimOwnershipManager<TOwnedKind, TAssociationKind, TOwnedValue, TAssociationValue> : IShimOwnershipManager<Guid>
        where TOwnedKind : DataObjectKind
        where TAssociationKind : DataObjectKind
        where TAssociationValue : IPrimaryOwnershipAssociation<TOwnedKind, Guid>
    {
        /// <summary>
        /// The shim container used to manipulate the collections.
        /// </summary>
        private IShimContainer shimContainer;

        /// <summary>
        /// A factory function to create an ownership association given a consumer id, entity id and a legacy app id.
        /// </summary>
        private Func<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, DataObjectIdentifier<TOwnedKind, Guid>, DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>, TAssociationValue> associationFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="ShimOwnershipManager{TOwnedKind,TAssociationKind,TOwnedValue,TAssociationValue}" /> class.
        /// </summary>
        /// <param name="shimContainer">The shim container that carries the implementation code.</param>
        /// <param name="borrowerId">The borrower's consumer id.</param>
        /// <param name="coborrowerId">The coborrower's consumer id.</param>
        /// <param name="applicationId">The legacy application's id.</param>
        /// <param name="associationFactory">A factory function to create an ownership association given a consumer id and an entity id.</param>
        public ShimOwnershipManager(
            IShimContainer shimContainer,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> borrowerId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> coborrowerId,
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> applicationId,
            Func<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, DataObjectIdentifier<TOwnedKind, Guid>, DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>, TAssociationValue> associationFactory)
        {
            this.shimContainer = shimContainer;
            this.BorrowerId = borrowerId;
            this.CoborrowerId = coborrowerId;
            this.AppId = applicationId;
            this.associationFactory = associationFactory;
        }

        /// <summary>
        /// The default owner id for new entities.
        /// </summary>
        public DataObjectIdentifier<DataObjectKind.Consumer, Guid> DefaultOwner => this.BorrowerId;

        /// <summary>
        /// Gets the borrower's consumer id.
        /// </summary>
        public DataObjectIdentifier<DataObjectKind.Consumer, Guid> BorrowerId { get; private set; }

        /// <summary>
        /// Gets the coborrower's consumer id.
        /// </summary>
        public DataObjectIdentifier<DataObjectKind.Consumer, Guid> CoborrowerId { get; private set; }

        /// <summary>
        /// Gets the legacy application's id.
        /// </summary>
        public DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> AppId { get; private set; }

        /// <summary>
        /// Sets the ownership type for the given record.
        /// </summary>
        /// <param name="primitiveId">The record id.</param>
        /// <param name="ownership">The ownership type to set.</param>
        public void SetOwnership(Guid primitiveId, Ownership ownership)
        {
            this.shimContainer.SetOwnership<TOwnedKind, TAssociationKind, TOwnedValue, TAssociationValue>(primitiveId, this.AppId.Value, this.BorrowerId.Value, this.CoborrowerId.Value, ownership, this.associationFactory);
        }

        /// <summary>
        /// Gets the ownership type for the given record.
        /// </summary>
        /// <param name="primitiveId">The record id.</param>
        /// <returns>The ownership type for the given record id.</returns>
        public Ownership GetOwnership(Guid primitiveId)
        {
            return this.shimContainer.GetOwnership<TOwnedKind, TAssociationKind, TOwnedValue, TAssociationValue>(primitiveId, this.BorrowerId.Value, this.CoborrowerId.Value);
        }

        /// <summary>
        /// When a borrower swap is performed, the interpretation of the identifiers
        /// changes.  The ownership managers need to be advised of this operation so
        /// they continue to function correctly.
        /// </summary>
        public void SwapBorrowers()
        {
            var temp = this.BorrowerId;
            this.BorrowerId = this.CoborrowerId;
            this.CoborrowerId = temp;
        }
    }
}
