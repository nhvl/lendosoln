﻿namespace DataAccess
{
    using System;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Class used by the RealPropertyShim to override the default formatting for semantic types.
    /// </summary>
    public class FormatRealPropertyShimAsString : FormatEntityShimAsString
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FormatRealPropertyShimAsString"/> class.
        /// </summary>
        /// <param name="losConvert">The LosConvert instance to use for formatting.</param>
        public FormatRealPropertyShimAsString(LosConvert losConvert)
            : base(losConvert)
        {
        }

        /// <summary>
        /// Format a Percentage value as a string.
        /// </summary>
        /// <param name="value">The Percentage value.</param>
        /// <returns>The string representation of the Percentage value.</returns>
        public override string Format(Percentage? value)
        {
            if (value == null)
            {
                return string.Empty;
            }
            else
            {
                decimal numeric = this.NumericConverter.Format(value);
                int asinteger = Convert.ToInt32(numeric);
                return this.Converter.ToCountString(asinteger);
            }
        }
    }
}
