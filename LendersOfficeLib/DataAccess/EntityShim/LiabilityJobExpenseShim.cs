﻿namespace DataAccess
{
    using System;
    using System.Data;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// A shim for the job expense liability.
    /// </summary>
    public sealed class LiabilityJobExpenseShim : LiabilitySpecialShim, ILiabilityJobExpense
    {
        /// <summary>
        /// Indicates whether this is the first or second special record for the Job Expense liability.
        /// </summary>
        private E_DebtJobExpenseT expenseType;

        /// <summary>
        /// Initializes a new instance of the <see cref="LiabilityJobExpenseShim"/> class.
        /// </summary>
        /// <param name="shimContainer">The container that holds all of the classes wrapped by the shims and allows accessing / updating data in the other collections.</param>
        /// <param name="collectionShim">The record collection.</param>
        /// <param name="contained">The contained entity.</param>
        /// <param name="ownershipManager">The ownership manager for the collection records.</param>
        /// <param name="recordId">The record id.</param>
        /// <param name="expenseType">A value indicating whether this is the first or second job expense.</param>
        /// <param name="stringFormatter">The string formatter.</param>
        /// <param name="numericFormatter">The numeric formatter.</param>
        /// <param name="stringParser">The string parser.</param>
        private LiabilityJobExpenseShim(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            LendingQB.Core.Data.Liability contained,
            IShimOwnershipManager<Guid> ownershipManager,
            DataObjectIdentifier<DataObjectKind.Liability, Guid> recordId,
            E_DebtJobExpenseT expenseType,
            IFormatAsString stringFormatter,
            IFormatAsNumeric numericFormatter,
            IParseFromString stringParser)
            : base(shimContainer, collectionShim, contained, ownershipManager, recordId, stringFormatter, numericFormatter, stringParser)
        {
            this.expenseType = expenseType;

            this.DebtT = E_DebtSpecialT.JobRelatedExpense;
        }

        /// <summary>
        /// Gets or sets the type of job expense. Do not set this directly.
        /// </summary>
        /// <value>The type of job expense.</value>
        public E_DebtJobExpenseT DebtJobExpenseT
        {
            get
            {
                return this.expenseType;
            }

            set
            {
                // This is publicly set-able, but client code should not be messing around
                // with this value directly.
                this.expenseType = value;
            }
        }

        /// <summary>
        /// Gets or sets a string description of the job expense.
        /// </summary>
        /// <value>A string description of the job expense.</value>
        public string ExpenseDesc
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.JobExpenseDesc);
            }

            set
            {
                this.Contained.JobExpenseDesc = this.StringParser.TryParseDescriptionField(value);
            }
        }

        /// <summary>
        /// Create a shim implementation of the ILiabilityJobExpense interface.
        /// </summary>
        /// <param name="shimContainer">The container that holds all shims.</param>
        /// <param name="collectionShim">The shim for the collection that contains this entity shim.</param>
        /// <param name="contained">The contained entity data.</param>
        /// <param name="ownershipManager">The ownership manager for the collection records.</param>
        /// <param name="recordId">The identifier for this record.</param>
        /// <param name="expenseType">A value indicating whether this is the first or second job expense.</param>
        /// <param name="converter">Instance of LosConvert used to format types as strings.</param>
        /// <returns>A shim for the ILiabilityJobExpense interface.</returns>
        internal static ILiabilityJobExpense Create(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            LendingQB.Core.Data.Liability contained,
            IShimOwnershipManager<Guid> ownershipManager,
            DataObjectIdentifier<DataObjectKind.Liability, Guid> recordId,
            E_DebtJobExpenseT expenseType,
            LosConvert converter)
        {
            return new LiabilityJobExpenseShim(
                shimContainer,
                collectionShim,
                contained,
                ownershipManager,
                recordId,
                expenseType,
                new FormatEntityShimAsString(converter),
                new FormatAsNumericBase(),
                new ParseEntityShimFromString(converter));
        }

        /// <summary>
        /// Sets the fields specific to this liability type to the data row.
        /// </summary>
        /// <param name="row">The data row.</param>
        protected override void SetFieldsToDataRow(DataRow row)
        {
            base.SetFieldsToDataRow(row);

            row["DebtJobExpenseT"] = ((int)this.DebtJobExpenseT).ToString();
            row["ComNm"] = this.ExpenseDesc; // Yes, this intentionally goes into ComNm... :(
        }
    }
}
