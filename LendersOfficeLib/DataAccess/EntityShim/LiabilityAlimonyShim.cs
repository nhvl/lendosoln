﻿namespace DataAccess
{
    using System;
    using System.Data;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Shim for the Alimony liability.
    /// </summary>
    public sealed class LiabilityAlimonyShim : LiabilitySpecialShim, ILiabilityAlimony
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LiabilityAlimonyShim"/> class.
        /// </summary>
        /// <param name="shimContainer">The container that holds all of the classes wrapped by the shims and allows accessing / updating data in the other collections.</param>
        /// <param name="collectionShim">The record collection.</param>
        /// <param name="contained">The contained entity.</param>
        /// <param name="ownershipManager">The ownership manager for the collection records.</param>
        /// <param name="recordId">The record id.</param>
        /// <param name="stringFormatter">The string formatter.</param>
        /// <param name="numericFormatter">The numeric formatter.</param>
        /// <param name="stringParser">The string parser.</param>
        private LiabilityAlimonyShim(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            LendingQB.Core.Data.Liability contained,
            IShimOwnershipManager<Guid> ownershipManager,
            DataObjectIdentifier<DataObjectKind.Liability, Guid> recordId,
            IFormatAsString stringFormatter,
            IFormatAsNumeric numericFormatter,
            IParseFromString stringParser)
            : base(shimContainer, collectionShim, contained, ownershipManager, recordId, stringFormatter, numericFormatter, stringParser)
        {
            this.DebtT = E_DebtSpecialT.Alimony;
        }

        /// <summary>
        /// Gets or sets the name of the person to whom is paid the alimony.
        /// </summary>
        /// <value>The name of the person to whom is paid the alimony.</value>
        public string OwedTo
        {
            get
            {
                return this.StringFormatter.Format(this.Contained.OwedTo);
            }

            set
            {
                this.Contained.OwedTo = this.StringParser.TryParsePersonName(value);
            }
        }

        /// <summary>
        /// Create a shim implementation of the ILiabilityAlimony interface.
        /// </summary>
        /// <param name="shimContainer">The container that holds all shims.</param>
        /// <param name="collectionShim">The shim for the collection that contains this entity shim.</param>
        /// <param name="contained">The contained entity data.</param>
        /// <param name="ownershipManager">The ownership manager for the collection records.</param>
        /// <param name="recordId">The identifier for this record.</param>
        /// <param name="converter">Instance of LosConvert used to format types as strings.</param>
        /// <returns>A shim for the ILiabilityAlimony interface.</returns>
        internal static ILiabilityAlimony Create(
            IShimContainer shimContainer,
            IRecordCollection collectionShim,
            LendingQB.Core.Data.Liability contained,
            IShimOwnershipManager<Guid> ownershipManager,
            DataObjectIdentifier<DataObjectKind.Liability, Guid> recordId,
            LosConvert converter)
        {
            return new LiabilityAlimonyShim(
                shimContainer,
                collectionShim,
                contained,
                ownershipManager,
                recordId,
                new FormatEntityShimAsString(converter),
                new FormatAsNumericBase(),
                new ParseEntityShimFromString(converter));
        }

        /// <summary>
        /// Sets the fields specific to this liability type to the data row.
        /// </summary>
        /// <param name="row">The data row.</param>
        protected override void SetFieldsToDataRow(DataRow row)
        {
            base.SetFieldsToDataRow(row);

            row["ComNm"] = this.OwedTo;
        }
    }
}
