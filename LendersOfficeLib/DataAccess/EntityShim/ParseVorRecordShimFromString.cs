﻿namespace DataAccess
{
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Class used by entity shims to to override the default parsing for semantic types.
    /// </summary>
    public class ParseVorRecordShimFromString : ParseEntityShimFromString
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ParseVorRecordShimFromString"/> class.
        /// </summary>
        /// <param name="losConvert">The LosConvert instance to use for formatting.</param>
        public ParseVorRecordShimFromString(LosConvert losConvert)
            : base(losConvert)
        {
        }

        /// <summary>
        /// Parse the string representation into a PhoneNumber instance.
        /// </summary>
        /// <param name="value">The string representation of a phone number value.</param>
        /// <returns>The PhoneNumber instance, or null.</returns>
        public override PhoneNumber? TryParsePhoneNumber(string value)
        {
            return PhoneNumber.Create(value);
        }
    }
}
