﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    using InterfaceDefs = LendingQB.Core;
    using ShimTarget = LendingQB.Core.Data;

    /// <summary>
    /// Shim for a public record collection.
    /// </summary>
    public class PublicRecordCollectionShim : RecordCollectionShim<DataObjectKind.PublicRecord, ShimTarget.PublicRecord>, IPublicRecordCollection
    {
        /// <summary>
        /// The ownership manager for the public records.
        /// </summary>
        private IShimOwnershipManager<Guid> ownershipManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="PublicRecordCollectionShim"/> class.
        /// </summary>
        /// <param name="shimContainer">Reference to the container that holds all shims.</param>
        /// <param name="ownershipManager">The ownership manager for the collection records.</param>
        /// <param name="converter">An instance of LosConvert used for formatting the data.</param>
        public PublicRecordCollectionShim(
            IShimContainer shimContainer,
            IShimOwnershipManager<Guid> ownershipManager,
            LosConvert converter)
            : base(shimContainer, ownershipManager.AppId, new DisallowSpecialRecords(), converter)
        {
            this.ownershipManager = ownershipManager;

            var ds = this.GetDataSet();
            var table = PublicRecordShim.CreateAndAddDataTable(ds);
            this.RebuildShimData();
        }

        /// <summary>
        /// Adds a new public record.
        /// </summary>
        /// <returns>The newly added record.</returns>
        public IPublicRecord AddRegularRecord()
        {
            var item = new ShimTarget.PublicRecord();
            var id = this.AddEntity(item);
            this.RebuildShimData();

            return this.GetRegRecordOf(id.Value);
        }

        /// <summary>
        /// Adds a new public record at the given index.
        /// </summary>
        /// <remarks>
        /// MoveRegularRecordUp calls RebuildShimData so there is no need to do so here after calling that method.
        /// </remarks>
        /// <param name="pos">The index.</param>
        /// <returns>The newly added record.</returns>
        public IPublicRecord AddRegularRecordAt(int pos)
        {
            var item = new ShimTarget.PublicRecord();
            var id = this.InsertRegularRecordAt(item, pos);
            return this.GetRegRecordOf(id.Value);
        }

        /// <summary>
        /// Gets the record with the given id.
        /// </summary>
        /// <param name="recordId">The record id.</param>
        /// <returns>The record with the given id.</returns>
        public IPublicRecord GetRegRecordOf(Guid recordId)
        {
            return (IPublicRecord)this.GetRegularRecordOf(recordId);
        }

        /// <summary>
        /// Gets the record at the given index.
        /// </summary>
        /// <param name="pos">The index.</param>
        /// <returns>The record at the given index.</returns>
        public IPublicRecord GetRegularRecordAt(int pos)
        {
            return (IPublicRecord)this.GetRegularItemAt(pos);
        }

        /// <summary>
        /// If there is no record with the indicated identifier, create one.  Return the record with the indicated identifier.
        /// </summary>
        /// <param name="recordId">The identifier that the returned record should have.</param>
        /// <returns>The record with the specified identifier.</returns>
        public ICollectionItemBase2 EnsureRegularRecordOf(Guid recordId)
        {
            var shim = this.TryGetRegularRecordOf(recordId);
            if (shim == null)
            {
                var identifier = DataObjectIdentifier<DataObjectKind.PublicRecord, Guid>.Create(recordId);
                var item = new ShimTarget.PublicRecord();
                this.AddEntity(identifier, item);
                this.RebuildShimData();

                shim = this.GetRegRecordOf(recordId);
            }

            return shim;
        }

        /// <summary>
        /// Create and add a new record of the specified type.
        /// </summary>
        /// <remarks>
        /// The method AddRegularRecord calls RebuildShimData so there is no need to do so here after calling that method.
        /// </remarks>
        /// <param name="recordT">This parameter is ignored.</param>
        /// <returns>The new record.</returns>
        ICollectionItemBase2 IRecordCollection.AddRecord(Enum recordT)
        {
            return this.AddRegularRecord();
        }

        /// <summary>
        /// Retrieve the record with the specified identifier.
        /// </summary>
        /// <param name="recordId">The identifier for the desired record.</param>
        /// <returns>The desired record, or throw an exception.</returns>
        ICollectionItemBase2 IRecordCollection.GetRegRecordOf(Guid recordId)
        {
            return this.GetRegularRecordOf(recordId);
        }

        /// <summary>
        /// Retrieve the regular record at the specified position.
        /// </summary>
        /// <param name="pos">The position.</param>
        /// <returns>The record at the specified position, or throw an out of bounds exception.</returns>
        ICollectionItemBase2 IRecordCollection.GetRegularRecordAt(int pos)
        {
            return this.GetRegularRecordAt(pos);
        }

        /// <summary>
        /// Retrieve the special record at the specified position.
        /// </summary>
        /// <param name="i">The position.</param>
        /// <returns>The record at the specified position, or throw an out of bounds exception.</returns>
        ICollectionItemBase2 IRecordCollection.GetSpecialRecordAt(int i)
        {
            // The exception type is what the legacy class would give, but the error message here is more explicit than the canned .net framework message.
            throw new IndexOutOfRangeException("Public record collection doesn't support special records.");
        }

        /// <summary>
        /// Create and add a new record.
        /// </summary>
        /// <returns>The new record.</returns>
        ICollectionItemBase2 IRecordCollection.AddRegularRecord()
        {
            return this.AddRegularRecord();
        }

        /// <summary>
        /// Create and add a new record at the specified position.
        /// </summary>
        /// <remarks>
        /// The method AddRegularRecordAt calls RebuildShimData so there is no need to do so here after calling that method.
        /// </remarks>
        /// <param name="pos">The position into which the record should be inserted.</param>
        /// <returns>The new record.</returns>
        ICollectionItemBase2 IRecordCollection.AddRegularRecordAt(int pos)
        {
            return this.AddRegularRecordAt(pos);
        }

        /// <summary>
        /// Retrieve all the items similar to the GetSubcollection method on other collections.
        /// </summary>
        /// <returns>A collection of all the items that can be iterated over.</returns>
        public ISubcollection GetAllItems()
        {
            var list = new ICollectionItemBase[this.CountRegular];
            for (int i = 0; i < this.CountRegular; ++i)
            {
                list[i] = this.GetRegularItemAt(i);
            }

            return CXmlRecordSubcollection.Create(list);
        }

        /// <summary>
        /// Build up the shim data.
        /// </summary>
        protected override void BuildShimData()
        {
            var ds = this.GetDataSet();
            var table = ds.Tables[0];
            foreach (var keyValue in this.GetAllCollectionEntities())
            {
                // Note that the key value pairs should be returned in the correct order because
                // the collection should be pre-ordered for us.
                var shim = PublicRecordShim.Create(this.ShimContainer, this, keyValue.Value, this.ownershipManager, keyValue.Key, this.Converter);
                this.RegularRecords.Add(shim);

                var pr = (PublicRecordShim)shim;
                var row = pr.CreateAndAddDataRow(table);
            }
        }

        /// <summary>
        /// Adds the given record to the underlying collection via the aggregate.
        /// </summary>
        /// <param name="value">The record to add.</param>
        /// <returns>The identifier of the new record.</returns>
        protected override DataObjectIdentifier<DataObjectKind.PublicRecord, Guid> AddEntity(ShimTarget.PublicRecord value)
        {
            return this.ShimContainer.Add(this.ownershipManager.DefaultOwner, value);
        }

        /// <summary>
        /// Adds the given id and record to the underlying collection via the aggregate.
        /// </summary>
        /// <param name="recordId">The id to use.</param>
        /// <param name="value">The record to add.</param>
        protected override void AddEntity(DataObjectIdentifier<DataObjectKind.PublicRecord, Guid> recordId, ShimTarget.PublicRecord value)
        {
            this.ShimContainer.Add(this.ownershipManager.DefaultOwner, recordId, value);
        }

        /// <summary>
        /// Removes a record from the underlying collection via the aggregate.
        /// </summary>
        /// <param name="recordId">The record id.</param>
        protected override void RemoveEntity(DataObjectIdentifier<DataObjectKind.PublicRecord, Guid> recordId)
        {
            this.ShimContainer.RemovePublicRecordFromLegacyApplication(recordId, this.ownershipManager.AppId, this.ownershipManager.BorrowerId, this.ownershipManager.CoborrowerId);
        }

        /// <summary>
        /// Gets the collection records relevant to the shimmed application.
        /// </summary>
        /// <returns>The collection records relevant to the shimmed application.</returns>
        protected override IEnumerable<KeyValuePair<DataObjectIdentifier<DataObjectKind.PublicRecord, Guid>, ShimTarget.PublicRecord>> GetAllCollectionEntities()
        {
            return this.ShimContainer.GetPublicRecordsForLegacyApplication(this.ownershipManager.AppId, this.ownershipManager.BorrowerId, this.ownershipManager.CoborrowerId);
        }
    }
}
