﻿namespace DataAccess
{
    using System;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Shim implementation for the ICollectionItemBase interface.
    /// </summary>
    public abstract class CollectionItemBaseShim
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CollectionItemBaseShim"/> class.
        /// </summary>
        /// <param name="recordId">The identifier for collection item.</param>
        /// <param name="stringFormatter">Object that controls how semantic types are serialized as strings.</param>
        /// <param name="numericFormatter">Object that controls how semantic types are converted to numeric values.</param>
        /// <param name="stringParser">Object that controls how strings are parsed into semantic types.</param>
        protected CollectionItemBaseShim(
            Guid recordId,
            IFormatAsString stringFormatter,
            IFormatAsNumeric numericFormatter,
            IParseFromString stringParser)
        {
            this.StringFormatter = stringFormatter;
            this.NumericFormatter = numericFormatter;
            this.StringParser = stringParser;
            this.RecordId = recordId;
        }

        /// <summary>
        /// Gets a value indicating whether the item is valid.
        /// </summary>
        public bool IsValid
        {
            get
            {
                return true; // no longer relevant since state maintenance is done through the collection.
            }
        }

        /// <summary>
        /// Gets or sets the guid identifier for this item.
        /// </summary>
        public Guid RecordId { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether the item has been marked as deleted.
        /// </summary>
        public bool IsOnDeathRow { get; set; }

        /// <summary>
        /// Gets the formatter for properties that return string values.
        /// </summary>
        protected IFormatAsString StringFormatter { get; private set; }

        /// <summary>
        /// Gets the formatter for properties that return numeric values.
        /// </summary>
        protected IFormatAsNumeric NumericFormatter { get; private set; }

        /// <summary>
        /// Gets the parser for properties with string setters.
        /// </summary>
        protected IParseFromString StringParser { get; private set; }

        /// <summary>
        /// Update the item.
        /// </summary>
        public virtual void Update()
        {
            // do nothing, the updates are now handled through the collection.
        }

        /// <summary>
        /// Format a CDateTime instance.
        /// </summary>
        /// <param name="cdatetime">The CDateTime instance.</param>
        /// <returns>The formatted date.</returns>
        internal static string FormatCDateTime(CDateTime cdatetime)
        {
            if (cdatetime.IsValid)
            {
                return cdatetime.DateTimeForComputation.ToShortDateString();
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Format an unzoned date, either directly or via an equivalent CDateTime instance.
        /// </summary>
        /// <param name="cdatetime">A CDateTime instance equivalent to the unzoned date.</param>
        /// <param name="unzoned">The unzoned date.</param>
        /// <returns>The formatted date, using the CDateTime formatted with LosConvert as the preference.</returns>
        protected string FormatUnzonedDate(CDateTime cdatetime, UnzonedDate? unzoned)
        {
            if (this.StringFormatter is IContainLosConvert)
            {
                var format = this.StringFormatter as IContainLosConvert;
                return cdatetime.ToString(format.Converter);
            }
            else
            {
                return this.StringFormatter.Format(unzoned);
            }
        }

        /// <summary>
        /// Convert an unzoned date to a CDateTime instance.
        /// </summary>
        /// <param name="unzoned">The unzoned date.</param>
        /// <returns>A CDateTime instance with the same value as the unzoned date.</returns>
        protected CDateTime UnzonedDateToCDateTime(UnzonedDate? unzoned)
        {
            if (unzoned == null)
            {
                return CDateTime.Create(null);
            }
            else
            {
                return CDateTime.Create(unzoned.Value.Date);
            }
        }

        /// <summary>
        /// Convert a CDateTime to an unzoned date.
        /// </summary>
        /// <param name="cdate">The CDateTime to convert.</param>
        /// <returns>The unzoned date, which may be null.</returns>
        protected UnzonedDate? CDateTimeToUnzonedDate(CDateTime cdate)
        {
            if (cdate == null || !cdate.IsValid)
            {
                return null;
            }

            return UnzonedDate.Create(cdate.DateTimeForComputation);
        }

        /// <summary>
        /// Centralize the logic of how to handle nullable booleans when a true boolean is expected.
        /// </summary>
        /// <param name="nullable">The nullable boolean.</param>
        /// <param name="defaultIfNull">The default value to use if the nullable is null. Defaults to false.</param>
        /// <returns>True if the nullable boolean is not null and true, false otherwise.</returns>
        protected bool BooleanFromNullable(bool? nullable, bool defaultIfNull = false)
        {
            return nullable == null ? defaultIfNull : nullable.Value;
        }

        /// <summary>
        /// Convert a BooleanKleene value to an equivalent E_TriState value.
        /// </summary>
        /// <param name="kleene">The BooleanKleene value.</param>
        /// <returns>The E_TriState value equivalent to the input BooleanKleene value.</returns>
        protected E_TriState TriStateFromBooleanKleene(BooleanKleene? kleene)
        {
            if (kleene == null)
            {
                return E_TriState.Blank;
            }
            else if (kleene == BooleanKleene.True)
            {
                return E_TriState.Yes;
            }
            else if (kleene == BooleanKleene.False)
            {
                return E_TriState.No;
            }
            else
            {
                return E_TriState.Blank;
            }
        }

        /// <summary>
        /// Convert an E_TriState value to an equivalent BooleanKleene value.
        /// </summary>
        /// <param name="tristate">The E_TriState value.</param>
        /// <returns>The BooleanKleene value equivalent to the input E_TriState value.</returns>
        protected BooleanKleene BooleanKleeneFromTriState(E_TriState tristate)
        {
            if (tristate == E_TriState.Yes)
            {
                return BooleanKleene.True;
            }
            else if (tristate == E_TriState.No)
            {
                return BooleanKleene.False;
            }
            else
            {
                return BooleanKleene.Indeterminant;
            }
        }

        /// <summary>
        /// Gets an enum field in a way that preserves the behavior from the legacy class.
        /// </summary>
        /// <typeparam name="T">The type of the enum.</typeparam>
        /// <param name="rawValue">The raw value.</param>
        /// <param name="defaultValue">The default value if the raw value is out of the defined range.</param>
        /// <returns>
        /// The default value if the raw value is not null and not defined for the enum type. 
        /// If the value is defined for the enum, returns that value.
        /// If the raw value is null, this will return the enum option that corresponds to 0 if it exists.
        /// If the raw value is null and there is no enum value for 0 then this will return the provided default.
        /// </returns>
        protected T GetEnumWithDefault<T>(T? rawValue, T defaultValue) where T : struct
        {
            var enumType = typeof(T);

            // The old code would call GetCount on the field name, which would
            // return 0 by default if the value was not defined.
            object valueToParse = rawValue ?? (object)0;

            if (Enum.IsDefined(enumType, valueToParse))
            {
                return (T)Enum.Parse(enumType, valueToParse.ToString());
            }
            else
            {
                return defaultValue;
            }
        }
    }
}
