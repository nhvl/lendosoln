using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
	public class CFannieMae32ImporterData : CImportPageData
	{
	    private static CSelectStatementProvider s_selectProvider;

		/// <summary>
		/// Override access control checking.
		/// </summary>

		protected override bool m_enforceAccessControl
		{
			// 12/14/2004 kb - Skip security checking when we
			// import files.  We need this from now on.

			get
			{
				return false;
			}
		}

	    static CFannieMae32ImporterData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("sfTransformLoPropertyTypeToPml");
            list.Add("sDue");
            list.Add("a1003SignD");
            list.Add("aAssetCollection");
            list.Add("aBAddr");
            list.Add("aBAddrMail");
            list.Add("aBAddrMailUsePresentAddr");
            list.Add("aBAddrMailSourceT");
            list.Add("aBAddrT");
            list.Add("aBAddrYrs");
            list.Add("aBAge");      
            list.Add("aBBaseI");
            list.Add("aBBonusesI");
            list.Add("aBCity");
            list.Add("aBCityMail");
            list.Add("aBCommisionI");
            list.Add("aBDecAlimony");
            list.Add("aBDecBankrupt");
            list.Add("aBDecBorrowing");
            list.Add("aBDecCitizen");
            list.Add("aBDecDelinquent");
            list.Add("aBDecEndorser");
            list.Add("aBDecForeclosure");
            list.Add("aBDecJudgment");
            list.Add("aBDecLawsuit");
            list.Add("aBDecObligated");
            list.Add("aBDecOcc");
            list.Add("aBDecPastOwnedPropT");
            list.Add("aBDecPastOwnedPropTitleT");
            list.Add("aBDecPastOwnership");
            list.Add("aBDecResidency");
            list.Add("aBDependAges");
            list.Add("aBDependNum");
            list.Add("aBDividendI");
            list.Add("aBDob");      
            list.Add("aBEmail");        
            list.Add("aBEmpCollection");
            list.Add("aBFirstNm");      
            list.Add("aBHPhone");       
            list.Add("aBLastNm");       
            list.Add("aBMaritalStatT"); 
            list.Add("aBMidNm");        
            list.Add("aBNetRentI1003");
            list.Add("aBNoFurnish");
            list.Add("aBOvertimeI");
            list.Add("aBPrev1Addr");
            list.Add("aBPrev1AddrT");
            list.Add("aBPrev1AddrYrs");
            list.Add("aBPrev1City");
            list.Add("aBPrev1State");
            list.Add("aBPrev1Zip");
            list.Add("aBPrev2Addr");
            list.Add("aBPrev2AddrT");
            list.Add("aBPrev2AddrYrs");
            list.Add("aBPrev2City");
            list.Add("aBPrev2State");
            list.Add("aBPrev2Zip");
            list.Add("aBSchoolYrs");
            list.Add("aBSsn");          
            list.Add("aBState");
            list.Add("aBStateMail");
            list.Add("aBSuffix");       
            list.Add("aBZip");
            list.Add("aBZipMail");
            list.Add("aCAddr");
            list.Add("aCAddrMail");
            list.Add("aCAddrMailUsePresentAddr");
            list.Add("aCAddrMailSourceT");
            list.Add("aCAddrT");
            list.Add("aCAddrYrs");
            list.Add("aCAge");      
            list.Add("aCBaseI");
            list.Add("aCBonusesI");
            list.Add("aCCity");
            list.Add("aCCityMail");
            list.Add("aCCommisionI");
            list.Add("aCDecAlimony");
            list.Add("aCDecBankrupt");
            list.Add("aCDecBorrowing");
            list.Add("aCDecCitizen");
            list.Add("aCDecDelinquent");
            list.Add("aCDecEndorser");
            list.Add("aCDecForeclosure");
            list.Add("aCDecJudgment");
            list.Add("aCDecLawsuit");
            list.Add("aCDecObligated");
            list.Add("aCDecOcc");
            list.Add("aCDecPastOwnedPropT");
            list.Add("aCDecPastOwnedPropTitleT");
            list.Add("aCDecPastOwnership");
            list.Add("aCDecResidency");
            list.Add("aCDependAges");
            list.Add("aCDependNum");
            list.Add("aCDividendI");
            list.Add("aCDob");
            list.Add("aCEmail");
            list.Add("aCEmpCollection");
            list.Add("aCFirstNm");      
            list.Add("aCHPhone");       
            list.Add("aCLastNm");       
            list.Add("aCMaritalStatT"); 
            list.Add("aCMidNm");        
            list.Add("aCNetRentI1003");
            list.Add("aCNoFurnish");
            list.Add("aCOvertimeI");
            list.Add("aCPrev1Addr");
            list.Add("aCPrev1AddrT");
            list.Add("aCPrev1AddrYrs");
            list.Add("aCPrev1City");
            list.Add("aCPrev1State");
            list.Add("aCPrev1Zip");
            list.Add("aCPrev2Addr");
            list.Add("aCPrev2AddrT");
            list.Add("aCPrev2AddrYrs");
            list.Add("aCPrev2City");
            list.Add("aCPrev2State");
            list.Add("aCPrev2Zip");
            list.Add("aCSchoolYrs");
            list.Add("aCSsn");          
            list.Add("aCState");
            list.Add("aCStateMail");
            list.Add("aCSuffix");       
            list.Add("aCZip");
            list.Add("aCZipMail");
            list.Add("aLiaCollection");
            list.Add("aManner");              
            list.Add("aNetRentI1003Lckd");
            list.Add("aOtherIncomeList");
            list.Add("aOccT");                
            list.Add("aPres1stM");
            list.Add("aPresHazIns");
            list.Add("aPresHoAssocDues");
            list.Add("aPresMIns");
            list.Add("aPresOFin");
            list.Add("aPresOHExp");
            list.Add("aPresRealETx");
            list.Add("aPresRent");
            list.Add("aReCollection");
            list.Add("aSpouseIExcl");
            list.Add("aTitleNm1");
            list.Add("aTitleNm2");
            list.Add("s1stMtgOrigLAmt");
            list.Add("sAgencyCaseNum"); 
            list.Add("sAltCost");
            list.Add("sApprVal");
            list.Add("sBalloonPmt");
            list.Add("sConcurSubFin");
            list.Add("sDwnPmtSrc");       
            list.Add("sDwnPmtSrcExplain");
            list.Add("sEquityCalc");
            list.Add("sEstCloseD");
            list.Add("sEstCloseDLckd");
            list.Add("sEstateHeldT");         
            list.Add("sFannieDocT");
            list.Add("sFannieSpT");
            list.Add("sFfUfmip1003");
            list.Add("sFfUfmip1003Lckd");
            list.Add("sFinMethDesc");   
            list.Add("sFinMethT");      
            list.Add("sHmdaAprRateSpread");
            list.Add("sHmdaReportAsHoepaLoan");
            list.Add("sIsSellerProvidedBelowMktFin");
            list.Add("sLAmtCalc");  
            list.Add("sLAmtLckd");      
            list.Add("sLDiscnt1003");
            list.Add("sLDiscnt1003Lckd");
            list.Add("sLPurposeT");           
            list.Add("sLT");
            list.Add("sLTODesc");
            list.Add("sLandCost");
            list.Add("sLeaseHoldExpireD");
            list.Add("sLenderCaseNum"); 
            list.Add("sLienPosT");
            list.Add("sLotAcqYr");      
            list.Add("sLotImprovC");
            list.Add("sLotLien");
            list.Add("sLotOrigC");
            list.Add("sLotVal");
            list.Add("sMInsCoverPc");
            list.Add("sMultiApps");
            list.Add("sNoteIR");    
            list.Add("sOCredit2Amt");
            list.Add("sOCredit2Desc");
            list.Add("sOCredit3Amt");
            list.Add("sOCredit3Desc");
            list.Add("sOCredit4Amt");
            list.Add("sOCredit4Desc");
            list.Add("sOLPurposeDesc");       
            list.Add("sPmtAdjMaxBalPc");
            list.Add("sPreparerDataSet");
            list.Add("sProHazInsMb");
            list.Add("sProHazInsR");
            list.Add("sProHoAssocDues");
            list.Add("sProMInsMb");
            list.Add("sProMInsR");
            list.Add("sProOHExp");
            list.Add("sProRealETxMb");
            list.Add("sProRealETxR");
            list.Add("sPurchPrice");
            list.Add("sRAdjLifeCapR");
            list.Add("sRefPdOffAmt1003");
            list.Add("sRefPdOffAmt1003Lckd");
            list.Add("sRefPurpose");         
            list.Add("sGseRefPurposeT");
            list.Add("sSchedDueD1");
            list.Add("sSchedDueD1Lckd");
            list.Add("sSpAcqYr");            
            list.Add("sSpAddr");
            list.Add("sSpAppraisalId");
            list.Add("sSpCity");      
            list.Add("sSpImprovC");
            list.Add("sSpImprovDesc");       
            list.Add("sSpImprovTimeFrameT"); 
            list.Add("sSpLegalDesc"); 
            list.Add("sSpLien");
            list.Add("sSpOrigC");
            list.Add("sSpProjectClassFannieT");
            list.Add("sSpState");     
            list.Add("sSpZip");       
            list.Add("sTerm");      
            list.Add("sTotCcPbs");
            list.Add("sTotCcPbsLocked");
            list.Add("sTotEstCc1003Lckd");
            list.Add("sTotEstCcNoDiscnt1003");
            list.Add("sTotEstPp1003");
            list.Add("sTotEstPp1003Lckd");
            list.Add("sUnitsNum");
            list.Add("sWillEscrowBeWaived");
            list.Add("sProdImpound");
            list.Add("sYrBuilt"); 
            list.Add("aAsstLiaCompletedNotJointly");
            list.Add("aIntrvwrMethodT");
            list.Add("sFannieARMPlanNum");
            list.Add("sFannieARMPlanNumLckd");
            list.Add("sAssumeLT");
            list.Add("sPrepmtPenaltyT");
            list.Add("sAgentDataSet");
            list.Add("sLNm");
            list.Add("sRAdjIndexR");
            list.Add("sArmIndexT");
            list.Add("sArmIndexTLckd");
            list.Add("sRAdjMarginR");
            list.Add("sQualIR");

            list.Add("sFinMethPrintAsOtherDesc");
            list.Add("sFinMethodPrintAsOther");
            list.Add("sStatusT");
            list.Add("sStatusLckd");
            list.Add("sEmployeeLoanRepId");
            list.Add("sEmployeeProcessorId");
            list.Add("sEmployeeLoanOpenerId");
            list.Add("sEmployeeManagerId");
            list.Add("sEmployeeCallCenterAgentId");
            list.Add("sEmployeeRealEstateAgentId");
            list.Add("sEmployeeLenderAccExecId");
            list.Add("sEmployeeLockDeskId");
            list.Add("sOCredit2Lckd");
            // 9/2/2004 dd - For PML
            list.Add("sfTransformDataToPml");
            list.Add("aBBusPhone");
            list.Add("aCBusPhone");

			//10/08/07 db - For OPM 18181
			list.Add("sIsCommunityLending");
			list.Add("sFannieMsa");
			list.Add("sFannieCommunityLendingT");
            list.Add("sFannieCommunitySecondsRepaymentStructureT");
			list.Add("sIsFannieNeighbors");
			list.Add("sIsCommunitySecond");
			list.Add("sFannieIncomeLimitAdjPc");
			list.Add("sFHASellerContribution");
			list.Add("sFHACcPbb");
			list.Add("sFHACcPbs");
			list.Add("sFHARefinanceTypeDesc");
			list.Add("sSpCounty");
			list.Add("sFHAHousingActSection");
			list.Add("sFHA203kRepairCost");
			list.Add("sFfUfmipR");
			list.Add("sFHASalesConcessions");
			list.Add("sVaProMaintenancePmt");
			list.Add("sVaProUtilityPmt");
			list.Add("sVaFf");
			list.Add("aVaEntitleAmt");
			list.Add("aBSsn");
			list.Add("aCSsn");
			list.Add("aVaBTotITax");
			list.Add("aVaCTotITax");
			list.Add("aVaBStateITax");
			list.Add("aVaCStateITax");
			list.Add("aVaBSsnTax");
			list.Add("aVaCSsnTax");
			list.Add("aVaBONetI");
			list.Add("aVaCONetI");
			list.Add("aVaBEmplmtI");
			list.Add("aVaCEmplmtI");
			list.Add("aFHABCaivrsNum");
			list.Add("aFHACCaivrsNum");
            list.Add("sDuCaseId");
			
			//10/17/07 db - For OPM 18570
			list.Add("sFHALenderIdCode");
			list.Add("sFHASponsorAgentIdCode");

			//10/22/07 db - For OPM 18538
			list.Add("sHmdaPreapprovalT");
            list.Add("sIsRateLocked");

            list.Add("sProMInsLckd");
            list.Add("sProMIns");
            list.Add("sFannieProdDesc");

            list.Add("sfCalculateMipFields");

            // OPM 48337
            list.Add("sFHAPurposeIsStreamlineRefi");
            list.Add("sFHAPurposeIsStreamlineRefiWithAppr");
            list.Add("sFHAPurposeIsStreamlineRefiWithoutAppr");

            list.Add("sFloodCertificationFProps");
            list.Add("sOwnerTitleInsProps");
            list.Add("sVaLCodeT");
            list.Add("sTotalScoreRefiT");
            list.Add("aBTotalScoreFhtbCounselingT");
            list.Add("aCTotalScoreFhtbCounselingT");
            list.Add("sIOnlyMon");

            list.Add("aPresTotHExpDesc");
            list.Add("aPresTotHExpCalc");
            list.Add("aPresTotHExpDLckd");
            list.Add("aReTotMPmtPrimaryResidence");
            list.Add("sIsRenovationLoan");
            list.Add("sTotalScoreFhaProductT");
            list.Add("sTotalScoreAppraisedFairMarketRent"); //av opm 105485
            list.Add("sTotalScoreVacancyFactor"); //av opm 105485

            list.Add("sLeadD");  // sk opm 120005
            list.Add("sOpenedD");
            list.Add("sPreQualD");
            list.Add("sSubmitD");
            list.Add("sProcessingD");
            list.Add("sPreApprovD");
            list.Add("sLoanSubmittedD");
            list.Add("sUnderwritingD");
            list.Add("sApprovD");
            list.Add("sFinalUnderwritingD");
            list.Add("sClearToCloseD");
            list.Add("sDocsD");
            list.Add("sDocsBackD");
            list.Add("sFundingConditionsD");
            list.Add("sFundD");
            list.Add("sRecordedD");
            list.Add("sFinalDocsD");
            list.Add("sClosedD");
            list.Add("sOnHoldD");
            list.Add("sCanceledD");
            list.Add("sRejectD");
            list.Add("sSuspendedD");
            list.Add("sLPurchaseD");
            list.Add("sShippedToInvestorD");
            list.Add("aBDecJudgmentExplanation");
            list.Add("aBDecBankruptExplanation");
            list.Add("aBDecForeclosureExplanation");
            list.Add("aBDecLawsuitExplanation");
            list.Add("aBDecObligatedExplanation");
            list.Add("aBDecDelinquentExplanation");
            list.Add("aBDecAlimonyExplanation");
            list.Add("aBDecBorrowingExplanation");
            list.Add("aBDecEndorserExplanation");

            list.Add("aCDecJudgmentExplanation");
            list.Add("aCDecBankruptExplanation");
            list.Add("aCDecForeclosureExplanation");
            list.Add("aCDecLawsuitExplanation");
            list.Add("aCDecObligatedExplanation");
            list.Add("aCDecDelinquentExplanation");
            list.Add("aCDecAlimonyExplanation");
            list.Add("aCDecBorrowingExplanation");
            list.Add("aCDecEndorserExplanation");
            list.Add("sNumFinancedProperties");

            list.Add("sFannieFipsLckd");
            list.Add("sFannieFips");

            list.Add(nameof(CAppBase.a1003InterviewD));
            list.Add(nameof(CAppBase.a1003InterviewDLckd));

            list.Add("sFannieHomebuyerEducationT");
            list.Add("sFannieProductCode");

            list.Add(nameof(CPageBase.sSponsoredOriginatorEIN));
            list.Add(nameof(CPageBase.sFhaSponsoredOriginatorEinLckd));

            list.Add(nameof(CAppBase.aBInterviewMethodT));
            list.Add(nameof(CAppBase.aCInterviewMethodT));

            list.Add(nameof(CPageBase.sLoanVersionT));

            list.Add(nameof(CAppBase.aBGender));
            list.Add(nameof(CAppBase.aCGender));
            list.Add(nameof(CAppBase.aBInterviewMethodT));
            list.Add(nameof(CAppBase.aCInterviewMethodT));
            list.Add(nameof(CAppBase.aBSexCollectedByObservationOrSurname));
            list.Add(nameof(CAppBase.aCSexCollectedByObservationOrSurname));
            list.Add(nameof(CAppBase.aBDoesNotWishToProvideEthnicity));
            list.Add(nameof(CAppBase.aCDoesNotWishToProvideEthnicity));
            list.Add(nameof(CAppBase.aBEthnicityCollectedByObservationOrSurname));
            list.Add(nameof(CAppBase.aCEthnicityCollectedByObservationOrSurname));
            list.Add(nameof(CAppBase.aBHispanicT));
            list.Add(nameof(CAppBase.aCHispanicT));
            list.Add(nameof(CAppBase.aBIsCuban));
            list.Add(nameof(CAppBase.aCIsCuban));
            list.Add(nameof(CAppBase.aBIsMexican));
            list.Add(nameof(CAppBase.aCIsMexican));
            list.Add(nameof(CAppBase.aBIsPuertoRican));
            list.Add(nameof(CAppBase.aCIsPuertoRican));
            list.Add(nameof(CAppBase.aBIsOtherHispanicOrLatino));
            list.Add(nameof(CAppBase.aCIsOtherHispanicOrLatino));
            list.Add(nameof(CAppBase.aBOtherHispanicOrLatinoDescription));
            list.Add(nameof(CAppBase.aCOtherHispanicOrLatinoDescription));
            list.Add(nameof(CAppBase.aBDoesNotWishToProvideEthnicity));
            list.Add(nameof(CAppBase.aCDoesNotWishToProvideEthnicity));
            list.Add(nameof(CAppBase.aBEthnicityCollectedByObservationOrSurname));
            list.Add(nameof(CAppBase.aCEthnicityCollectedByObservationOrSurname));
            list.Add(nameof(CAppBase.aBIsAmericanIndian));
            list.Add(nameof(CAppBase.aCIsAmericanIndian));
            list.Add(nameof(CAppBase.aBOtherAmericanIndianDescription));
            list.Add(nameof(CAppBase.aCOtherAmericanIndianDescription));
            list.Add(nameof(CAppBase.aBIsBlack));
            list.Add(nameof(CAppBase.aCIsBlack));
            list.Add(nameof(CAppBase.aBIsWhite));
            list.Add(nameof(CAppBase.aCIsWhite));
            list.Add(nameof(CAppBase.aBIsAsian));
            list.Add(nameof(CAppBase.aCIsAsian));
            list.Add(nameof(CAppBase.aBIsAsianIndian));
            list.Add(nameof(CAppBase.aCIsAsianIndian));
            list.Add(nameof(CAppBase.aBIsChinese));
            list.Add(nameof(CAppBase.aCIsChinese));
            list.Add(nameof(CAppBase.aBIsFilipino));
            list.Add(nameof(CAppBase.aCIsFilipino));
            list.Add(nameof(CAppBase.aBIsJapanese));
            list.Add(nameof(CAppBase.aCIsJapanese));
            list.Add(nameof(CAppBase.aBIsKorean));
            list.Add(nameof(CAppBase.aCIsKorean));
            list.Add(nameof(CAppBase.aBIsVietnamese));
            list.Add(nameof(CAppBase.aCIsVietnamese));
            list.Add(nameof(CAppBase.aBIsOtherAsian));
            list.Add(nameof(CAppBase.aCIsOtherAsian));
            list.Add(nameof(CAppBase.aBOtherAsianDescription));
            list.Add(nameof(CAppBase.aCOtherAsianDescription));
            list.Add(nameof(CAppBase.aBIsPacificIslander));
            list.Add(nameof(CAppBase.aCIsPacificIslander));
            list.Add(nameof(CAppBase.aBIsNativeHawaiian));
            list.Add(nameof(CAppBase.aCIsNativeHawaiian));
            list.Add(nameof(CAppBase.aBIsGuamanianOrChamorro));
            list.Add(nameof(CAppBase.aCIsGuamanianOrChamorro));
            list.Add(nameof(CAppBase.aBIsSamoan));
            list.Add(nameof(CAppBase.aCIsSamoan));
            list.Add(nameof(CAppBase.aBIsOtherPacificIslander));
            list.Add(nameof(CAppBase.aCIsOtherPacificIslander));
            list.Add(nameof(CAppBase.aBOtherPacificIslanderDescription));
            list.Add(nameof(CAppBase.aCOtherPacificIslanderDescription));
            list.Add(nameof(CAppBase.aBDoesNotWishToProvideRace));
            list.Add(nameof(CAppBase.aCDoesNotWishToProvideRace));
            list.Add(nameof(CAppBase.aBRaceCollectedByObservationOrSurname));
            list.Add(nameof(CAppBase.aCRaceCollectedByObservationOrSurname));

            list.Add(nameof(CPageBase.sSpInvestorCurrentLoanT));
            list.Add(nameof(CPageBase.sConstructionPurposeT));
            list.Add(nameof(CPageBase.sLotOwnerT));

            list.Add(nameof(CPageBase.sIsIncomeCollectionEnabled));
            list.Add(nameof(CPageBase.ClearIncomeSources));
            list.Add(nameof(CAppBase.aBConsumerId));
            list.Add(nameof(CAppBase.aCConsumerId));
            list.Add(nameof(CPageBase.AddIncomeSource));
            list.Add(nameof(CPageBase.ImportGrossRent));

            list.Add(nameof(CPageBase.sHomeIsMhAdvantageTri));
            #endregion

            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );
        }        
	        
		public CFannieMae32ImporterData(Guid fileId) : base("CFannieMae32ImporterData",fileId,  s_selectProvider)
		{
		}
	}

    public class CFannieMae32ExporterData : CPageData
    {
        private static CSelectStatementProvider s_selectProvider;
	    
        static CFannieMae32ExporterData()
        {
            StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("sfTransformLoPropertyTypeToPml");
            list.Add("sDue");
			list.Add("sFinMethDesc");
            list.Add("a1003SignD");
            list.Add("aAssetCollection");
            list.Add("aAsstLiaCompletedNotJointly");
            list.Add("aBAddr");
            list.Add("aBAddrMail");
            list.Add("aBAddrMailUsePresentAddr");
            list.Add("aBAddrMailSourceT");
            list.Add("aBAddrT");
            list.Add("aBAddrYrs");
            list.Add("aBAge");
            list.Add("aBBaseI");
            list.Add("aBBonusesI");
            list.Add("aBCity");
            list.Add("aBCityMail");
            list.Add("aBCommisionI");
            list.Add("aBDecAlimony");
            list.Add("aBDecBankrupt");
            list.Add("aBDecBorrowing");
            list.Add("aBDecCitizen");
            list.Add("sIsRenovationLoan");
            list.Add("aBDecDelinquent");
            list.Add("aBDecEndorser");
            list.Add("aBDecForeclosure");
            list.Add("aBDecJudgment");
            list.Add("aBDecLawsuit");
            list.Add("aBDecObligated");
            list.Add("aBDecOcc");
            list.Add("aBDecPastOwnedPropT");
            list.Add("aBDecPastOwnedPropTitleT");
            list.Add("aBDecPastOwnership");
            list.Add("aBDecResidency");
            list.Add("aBDependAges");
            list.Add("aBDependNum");
            list.Add("aBDividendI");
            list.Add("aBDob");
            list.Add("aBEmail");
            list.Add("aBEmpCollection");
            list.Add("aBFirstNm");
            list.Add("aBHPhone");
            list.Add("aBLastNm");
            list.Add("aBMaritalStatT");
            list.Add("aBMidNm");
            list.Add("aBNetRentI1003");
            list.Add("aBNoFurnish");
            list.Add("aBOvertimeI");
            list.Add("aBPrev1Addr");
            list.Add("aBPrev1AddrT");
            list.Add("aBPrev1AddrYrs");
            list.Add("aBPrev1City");
            list.Add("aBPrev1State");
            list.Add("aBPrev1Zip");
            list.Add("aBPrev2Addr");
            list.Add("aBPrev2AddrT");
            list.Add("aBPrev2AddrYrs");
            list.Add("aBPrev2City");
            list.Add("aBPrev2State");
            list.Add("aBPrev2Zip");
            list.Add("aBSchoolYrs");
            list.Add("aBSsn");
            list.Add("aBState");
            list.Add("aBStateMail");
            list.Add("aBSuffix");
            list.Add("aBZip");
            list.Add("aBZipMail");
            list.Add("aCAddr");
            list.Add("aCAddrMail");
            list.Add("aCAddrMailUsePresentAddr");
            list.Add("aCAddrMailSourceT");
            list.Add("aCAddrT");
            list.Add("aCAddrYrs");
            list.Add("aCAge");
            list.Add("aCBaseI");
            list.Add("aCBonusesI");
            list.Add("aCCity");
            list.Add("aCCityMail");
            list.Add("aCCommisionI");
            list.Add("aCDecAlimony");
            list.Add("aCDecBankrupt");
            list.Add("aCDecBorrowing");
            list.Add("aCDecCitizen");
            list.Add("aCDecDelinquent");
            list.Add("aCDecEndorser");
            list.Add("aCDecForeclosure");
            list.Add("aCDecJudgment");
            list.Add("aCDecLawsuit");
            list.Add("aCDecObligated");
            list.Add("aCDecOcc");
            list.Add("aCDecPastOwnedPropT");
            list.Add("aCDecPastOwnedPropTitleT");
            list.Add("aCDecPastOwnership");
            list.Add("aCDecResidency");
            list.Add("aCDependAges");
            list.Add("aCDependNum");
            list.Add("aCDividendI");
            list.Add("aCDob");
            list.Add("aCEmail");
            list.Add("aCEmpCollection");
            list.Add("aCFirstNm");
            list.Add("aCHPhone");
            list.Add("aCLastNm");
            list.Add("aCMaritalStatT");
            list.Add("aCMidNm");
            list.Add("aCNetRentI1003");
            list.Add("aCNoFurnish");
            list.Add("aCOvertimeI");
            list.Add("aCPrev1Addr");
            list.Add("aCPrev1AddrT");
            list.Add("aCPrev1AddrYrs");
            list.Add("aCPrev1City");
            list.Add("aCPrev1State");
            list.Add("aCPrev1Zip");
            list.Add("aCPrev2Addr");
            list.Add("aCPrev2AddrT");
            list.Add("aCPrev2AddrYrs");
            list.Add("aCPrev2City");
            list.Add("aCPrev2State");
            list.Add("aCPrev2Zip");
            list.Add("aCSchoolYrs");
            list.Add("aCSsn");
            list.Add("aCState");
            list.Add("aCStateMail");
            list.Add("aCSuffix");
            list.Add("aCZip");
            list.Add("aCZipMail");
            list.Add("aIntrvwrMethodT");
            list.Add("aIsPrimary");
            list.Add("aLiaCollection");
            list.Add("aManner");
            list.Add("aOtherIncomeList");
            list.Add("aOccT");
            list.Add("aPres1stM");
            list.Add("aPresHazIns");
            list.Add("aPresHoAssocDues");
            list.Add("aPresMIns");
            list.Add("aPresOFin");
            list.Add("aPresOHExp");
            list.Add("aPresRealETx");
            list.Add("aPresRent");
            list.Add("aReCollection");
            list.Add("aSpouseIExcl");
            list.Add("aTitleNm1");
            list.Add("aTitleNm2");
            list.Add("sAgencyCaseNum");
            list.Add("sAgentDataSet");
            list.Add("sAltCost");
            list.Add("sAmortTable");
            list.Add("sApprVal");
            list.Add("sArmIndexT");
            list.Add("sAssumeLT");
            list.Add("sBalloonPmt");
            list.Add("sBuydown");
            list.Add("sBuydwnMon1");
            list.Add("sDwnPmtSrc");
            list.Add("sDwnPmtSrcExplain");
            list.Add("sEquityCalc");
            list.Add("sEstCloseD");
            list.Add("sEstateHeldT");
            list.Add("sFannieARMPlanNum");
            list.Add("sFannieDocT");
            list.Add("sFannieSpT");
            list.Add("sFfUfmip1003");
            list.Add("sFfUfmipFinanced");
            list.Add("sFinMethT");
            list.Add("sHmdaAprRateSpread");
            list.Add("sHmdaReportAsHoepaLoan");
            list.Add("sIsSellerProvidedBelowMktFin");
            list.Add("sLAmtCalc");
            list.Add("sLDiscnt1003");
            list.Add("sLPurposeT");
            list.Add("sLT");
            list.Add("sLTODesc");
            list.Add("sUfCashPd");
            list.Add("sUfCashPdLckd");
            list.Add("sLandCost");
            list.Add("sLeaseHoldExpireD");
            list.Add("sFfUfMipIsBeingFinanced");
            list.Add("sLenderCaseNum");
            list.Add("sLienPosT");
            list.Add("sLotAcqYr");
            list.Add("sLotImprovC");
            list.Add("sLotLien");
            list.Add("sLotOrigC");
            list.Add("sLotVal");
            list.Add("sMInsCode");
            list.Add("sMInsCoverPc");
            list.Add("sMultiApps");
            list.Add("sNoteIR");
            list.Add("sOCredit1Amt");
            list.Add("sOCredit1Desc");
            list.Add("sOCredit2Amt");
            list.Add("sOCredit2Desc");
            list.Add("sOCredit3Amt");
            list.Add("sOCredit3Desc");
            list.Add("sOCredit4Amt");
            list.Add("sOCredit4Desc");
            list.Add("sOLPurposeDesc");
            list.Add("sONewFinBal");
            list.Add("sPmtAdjMaxBalPc");
            list.Add("sPreparerDataSet");
            list.Add("sPrepmtPenaltyT");
            list.Add("sProFirstMPmt");
            list.Add("sProHazIns");
            list.Add("sProHoAssocDues");
            list.Add("sProMIns");
            list.Add("sProOHExp");
            list.Add("sProRealETx");
            list.Add("sProSecondMPmt");
            list.Add("sPurchPrice");
            list.Add("sQualIR");
            list.Add("sRAdjIndexR");
            list.Add("sRAdjLifeCapR");
            list.Add("sRAdjMarginR");
            list.Add("sRefPdOffAmt1003");
            list.Add("sGseRefPurposeT");
            list.Add("sRefPurpose");
            list.Add("sSchedDueD1");
            list.Add("sSchedIR1");
            list.Add("sSpAcqYr");
            list.Add("sSpAddr");
            list.Add("sSpAppraisalId");
            list.Add("sSpCity");
            list.Add("sSpImprovC");
            list.Add("sSpImprovDesc");
            list.Add("sSpImprovTimeFrameT");
            list.Add("sSpLegalDesc");
            list.Add("sSpLien");
            list.Add("sSpOrigC");
            list.Add("sSpProjectClassFannieT");
            list.Add("sSpRentalIRaw");
            list.Add("sSpState");
            list.Add("sSpValuationMethodT");
            list.Add("sSpZip");
            list.Add("sTerm");
            list.Add("sTotCcPbs");
            list.Add("sTotEstCcNoDiscnt1003");
            list.Add("sTotEstPp1003");
            list.Add("sUnitsNum");
            list.Add("sWillEscrowBeWaived");
            list.Add("sProdImpound");
            list.Add("sYrBuilt");
            list.Add("sFinMethPrintAsOtherDesc");
            list.Add("sFinMethodPrintAsOther");
            list.Add("sDuCaseId");
            list.Add("aBBusPhone");
            list.Add("aCBusPhone");
         
			//10/08/07 db - For OPM 18181
			list.Add("sIsCommunityLending");
			list.Add("sFannieMsa");
			list.Add("sFannieCommunityLendingT");
			list.Add("sIsFannieNeighbors");
			list.Add("sIsCommunitySecond");
			list.Add("sFannieIncomeLimitAdjPc");
			list.Add("sFHASellerContribution");
			list.Add("sFHACcPbb");
			list.Add("sFHACcPbs");
			list.Add("sFHARefinanceTypeDesc");
			list.Add("sSpCounty");
			list.Add("sFHAHousingActSection");
			list.Add("sFHA203kRepairCost");
			list.Add("sFfUfmipR");
			list.Add("sFHASalesConcessions");
			list.Add("sVaProMaintenancePmt");
			list.Add("sVaProUtilityPmt");
			list.Add("sVaFf");
			list.Add("aVaEntitleAmt");
			list.Add("aBSsn");
			list.Add("aCSsn");
			list.Add("aVaBTotITax");
			list.Add("aVaCTotITax");
			list.Add("aVaBStateITax");
			list.Add("aVaCStateITax");
			list.Add("aVaBSsnTax");
			list.Add("aVaCSsnTax");
			list.Add("aVaBONetI");
			list.Add("aVaCONetI");
			list.Add("aVaBEmplmtI");
			list.Add("aVaCEmplmtI");
			list.Add("aFHABCaivrsNum");
			list.Add("aFHACCaivrsNum");

			//10/17/07 db - For OPM 18570
			list.Add("sFHALenderIdCode");
			list.Add("sFHASponsorAgentIdCode");

			//10/22/07 db - For OPM 18538
			list.Add("sHmdaPreapprovalT");
            list.Add("sFannieProdDesc");

            // OPM 48337
            list.Add("sFHAPurposeIsStreamlineRefi");
            list.Add("sFHAPurposeIsStreamlineRefiWithAppr");
            list.Add("sFHAPurposeIsStreamlineRefiWithoutAppr");

            list.Add("sFloodCertificationFProps");
            list.Add("sOwnerTitleInsProps");
            list.Add("sVaLCodeT");
            list.Add("sTotalScoreRefiT");

            // Fields needed for sSpGrossRent
            list.Add("aSpNegCfLckd");
            list.Add("sSpGrossRent");
            list.Add("sMonthlyPmtQual");
            list.Add("sOccR");
            list.Add("aBSpPosCf");
            list.Add("aCSpPosCf");
            list.Add("aTotSpPosCf");
            list.Add("aBTotalScoreFhtbCounselingT");
            list.Add("aCTotalScoreFhtbCounselingT");
            list.Add("sIOnlyMon");

            list.Add("aPresTotHExpCalc");
            list.Add("aPresTotHExpDesc");
            list.Add("sExportAdditionalLiabitiesFannieMae");
            list.Add("sExportAdditionalLiabitiesDODU");
            list.Add("aReTotMPmtPrimaryResidence");

            //For OPM 91497
            list.Add("aSpNegCf");
            list.Add("sTotalScoreAppraisedFairMarketRent"); //av opm 105485
            list.Add("sTotalScoreVacancyFactor"); //av opm 105485

            // OPM 185961
            list.Add("sCorrespondentProcessT");

            list.Add("aBDecJudgmentExplanation");
            list.Add("aBDecBankruptExplanation");
            list.Add("aBDecForeclosureExplanation");
            list.Add("aBDecLawsuitExplanation");
            list.Add("aBDecObligatedExplanation");
            list.Add("aBDecDelinquentExplanation");
            list.Add("aBDecAlimonyExplanation");
            list.Add("aBDecBorrowingExplanation");
            list.Add("aBDecEndorserExplanation");

            list.Add("aCDecJudgmentExplanation");
            list.Add("aCDecBankruptExplanation");
            list.Add("aCDecForeclosureExplanation");
            list.Add("aCDecLawsuitExplanation");
            list.Add("aCDecObligatedExplanation");
            list.Add("aCDecDelinquentExplanation");
            list.Add("aCDecAlimonyExplanation");
            list.Add("aCDecBorrowingExplanation");
            list.Add("aCDecEndorserExplanation");

            list.Add(nameof(CAppBase.aBGender));
            list.Add(nameof(CAppBase.aCGender));
            list.Add(nameof(CAppBase.aBInterviewMethodT));
            list.Add(nameof(CAppBase.aCInterviewMethodT));
            list.Add(nameof(CAppBase.aBSexCollectedByObservationOrSurname));
            list.Add(nameof(CAppBase.aCSexCollectedByObservationOrSurname));
            list.Add(nameof(CAppBase.aBDoesNotWishToProvideEthnicity));
            list.Add(nameof(CAppBase.aCDoesNotWishToProvideEthnicity));
            list.Add(nameof(CAppBase.aBEthnicityCollectedByObservationOrSurname));
            list.Add(nameof(CAppBase.aCEthnicityCollectedByObservationOrSurname));
            list.Add(nameof(CAppBase.aBHispanicT));
            list.Add(nameof(CAppBase.aCHispanicT));
            list.Add(nameof(CAppBase.aBIsCuban));
            list.Add(nameof(CAppBase.aCIsCuban));
            list.Add(nameof(CAppBase.aBIsMexican));
            list.Add(nameof(CAppBase.aCIsMexican));
            list.Add(nameof(CAppBase.aBIsPuertoRican));
            list.Add(nameof(CAppBase.aCIsPuertoRican));
            list.Add(nameof(CAppBase.aBIsOtherHispanicOrLatino));
            list.Add(nameof(CAppBase.aCIsOtherHispanicOrLatino));
            list.Add(nameof(CAppBase.aBOtherHispanicOrLatinoDescription));
            list.Add(nameof(CAppBase.aCOtherHispanicOrLatinoDescription));
            list.Add(nameof(CAppBase.aBDoesNotWishToProvideEthnicity));
            list.Add(nameof(CAppBase.aCDoesNotWishToProvideEthnicity));
            list.Add(nameof(CAppBase.aBEthnicityCollectedByObservationOrSurname));
            list.Add(nameof(CAppBase.aCEthnicityCollectedByObservationOrSurname));
            list.Add(nameof(CAppBase.aBIsAmericanIndian));
            list.Add(nameof(CAppBase.aCIsAmericanIndian));
            list.Add(nameof(CAppBase.aBOtherAmericanIndianDescription));
            list.Add(nameof(CAppBase.aCOtherAmericanIndianDescription));
            list.Add(nameof(CAppBase.aBIsBlack));
            list.Add(nameof(CAppBase.aCIsBlack));
            list.Add(nameof(CAppBase.aBIsWhite));
            list.Add(nameof(CAppBase.aCIsWhite));
            list.Add(nameof(CAppBase.aBIsAsian));
            list.Add(nameof(CAppBase.aCIsAsian));
            list.Add(nameof(CAppBase.aBIsAsianIndian));
            list.Add(nameof(CAppBase.aCIsAsianIndian));
            list.Add(nameof(CAppBase.aBIsChinese));
            list.Add(nameof(CAppBase.aCIsChinese));
            list.Add(nameof(CAppBase.aBIsFilipino));
            list.Add(nameof(CAppBase.aCIsFilipino));
            list.Add(nameof(CAppBase.aBIsJapanese));
            list.Add(nameof(CAppBase.aCIsJapanese));
            list.Add(nameof(CAppBase.aBIsKorean));
            list.Add(nameof(CAppBase.aCIsKorean));
            list.Add(nameof(CAppBase.aBIsVietnamese));
            list.Add(nameof(CAppBase.aCIsVietnamese));
            list.Add(nameof(CAppBase.aBIsOtherAsian));
            list.Add(nameof(CAppBase.aCIsOtherAsian));
            list.Add(nameof(CAppBase.aBOtherAsianDescription));
            list.Add(nameof(CAppBase.aCOtherAsianDescription));
            list.Add(nameof(CAppBase.aBIsPacificIslander));
            list.Add(nameof(CAppBase.aCIsPacificIslander));
            list.Add(nameof(CAppBase.aBIsNativeHawaiian));
            list.Add(nameof(CAppBase.aCIsNativeHawaiian));
            list.Add(nameof(CAppBase.aBIsGuamanianOrChamorro));
            list.Add(nameof(CAppBase.aCIsGuamanianOrChamorro));
            list.Add(nameof(CAppBase.aBIsSamoan));
            list.Add(nameof(CAppBase.aCIsSamoan));
            list.Add(nameof(CAppBase.aBIsOtherPacificIslander));
            list.Add(nameof(CAppBase.aCIsOtherPacificIslander));
            list.Add(nameof(CAppBase.aBOtherPacificIslanderDescription));
            list.Add(nameof(CAppBase.aCOtherPacificIslanderDescription));
            list.Add(nameof(CAppBase.aBDoesNotWishToProvideRace));
            list.Add(nameof(CAppBase.aCDoesNotWishToProvideRace));
            list.Add(nameof(CAppBase.aBRaceCollectedByObservationOrSurname));
            list.Add(nameof(CAppBase.aCRaceCollectedByObservationOrSurname));

            list.Add(nameof(CPageBase.sSpInvestorCurrentLoanT));
            #endregion

            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );
        }        
	        
        public CFannieMae32ExporterData(Guid fileId) : base(fileId, "CFannieMae32ExporterData", s_selectProvider)
        {
        }
    }
}
