﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Text;

    /// <summary>
    /// An interface extracted from the CEmpCollection class. Intended to be implemented by a collection shim.
    /// </summary>
    public interface IEmpCollection : IRecordCollection
    {
        /// <summary>
        /// Gets the extra previous employments.
        /// </summary>
        ISubcollection ExtraPreviousEmployments { get; }

        /// <summary>
        /// Gets a value indicating whether there are more than two regular employment records.
        /// </summary>
        bool HasMoreThan2PreviousEmployments { get; }

        /// <summary>
        /// Gets a value indicating whether the employment history is valid.
        /// </summary>
        bool IsEmploymentHistoryValid { get; }

        /// <summary>
        /// Gets a value indicating whether the employer business phone is "locked".
        /// </summary>
        bool IsEmplrBusPhoneLckd { get; }

        /// <summary>
        /// Gets the business phone of the associated borrower.
        /// </summary>
        string PersonBusPhone { get; }

        /// <summary>
        /// Gets a DataView of previous employment records sorted by OrderRankValue.
        /// </summary>
        DataView SortedView { get; }

        /// <summary>
        /// Adds a regular employment record.
        /// </summary>
        /// <returns>The added record.</returns>
        new IRegularEmploymentRecord AddRegularRecord();

        /// <summary>
        /// Gets the length of the longest continuous employment gap (in months, rounding down)
        /// within the period specified in the parameter.
        /// </summary>
        /// <param name="months">The number of months to go back from the current date.</param>
        /// <returns>
        /// The length of the longest continuous employment gap in months, rounding down.
        /// </returns>
        int GetMaxEmploymentGapWithin(int months);

        /// <summary>
        /// Gets the primary employment record.
        /// </summary>
        /// <param name="forceCreate">A value indicating whether the record should be created if it does not exist.</param>
        /// <returns>The primary employment if it exists or if it is being force-created. Otherwise, null.</returns>
        IPrimaryEmploymentRecord GetPrimaryEmp(bool forceCreate);

        /// <summary>
        /// Gets the regular employment record at the specified index.
        /// </summary>
        /// <param name="pos">The index.</param>
        /// <returns>The regular employment record.</returns>
        new IRegularEmploymentRecord GetRegularRecordAt(int pos);

        /// <summary>
        /// Gets the regular record associated with the given id.
        /// </summary>
        /// <param name="recordId">The record id.</param>
        /// <returns>The regular record associated with the given id.</returns>
        new IRegularEmploymentRecord GetRegRecordOf(Guid recordId);

        /// <summary>
        /// Gets the specified sub-collection.
        /// </summary>
        /// <param name="inclusiveGroup">A value indicating whether the specified groups should be included or excluded.</param>
        /// <param name="group">An enum value flagged with the desired groups.</param>
        /// <returns>The specified sub-collection.</returns>
        ISubcollection GetSubcollection(bool inclusiveGroup, E_EmpGroupT group);

        /// <summary>
        /// Validates the employment history dates.
        /// </summary>
        /// <param name="userMessage">The error message to append errors to.</param>
        /// <param name="borrowerName">The borrower's name.</param>
        /// <returns>True if the employment history dates are valid. Otherwise, false.</returns>
        bool ValidateEmploymentHistoryDates(StringBuilder userMessage, string borrowerName);
    }
}