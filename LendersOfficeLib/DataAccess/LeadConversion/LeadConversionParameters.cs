﻿namespace DataAccess
{
    using System;
    using LendersOffice.Constants;

    /// <summary>
    /// Encapsulates parameters for lead to loan conversion.
    /// </summary>
    public class LeadConversionParameters
    {
        /// <summary>
        /// Gets or sets the ID of the lead to be converted.
        /// </summary>
        /// <value>The ID of the lead to be converted.</value>
        public Guid LeadId { get; set; }

        /// <summary>
        /// Gets or sets the file version to be passed into Loan.Save.
        /// </summary>
        /// <value>The file version to be passed into Loan.Save.</value>
        public int FileVersion { get; set; } = ConstAppDavid.SkipVersionCheck;

        /// <summary>
        /// Gets or sets a value indicating whether the loan file should be created from a template.
        /// </summary>
        /// <value>A boolean indicating whether the loan file should be created from a template.</value>
        public bool CreateFromTemplate { get; set; }

        /// <summary>
        /// Gets or sets the ID of the loan file template to create from, if applicable.
        /// </summary>
        /// <value>The ID of the loan file template to create from.</value>
        public Guid TemplateId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the lead prefix should be removed.
        /// </summary>
        /// <value>A boolean indicating whether the lead prefix should be removed.</value>
        public bool RemoveLeadPrefix { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the lead is a QP2 file.
        /// </summary>
        /// <value>A boolean indicating whether the lead is a QP2 file.</value>
        /// <remarks>This is only used by PML lead-to-loan conversion.</remarks>
        public bool ConvertFromQp2File { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the file is being converted to a lead.
        /// This is only applicable for a QP2 lead being converted to a lead.
        /// </summary>
        /// <value>A boolean indicating whether the file is being converted to a lead.</value>
        /// <remarks>This is only used by PML lead-to-loan conversion.</remarks>
        public bool ConvertToLead { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user is using embedded PML.
        /// </summary>
        /// <value>A boolean indicating whether the user is using embedded PML.</value>
        /// <remarks>This is only used by PML lead-to-loan conversion.</remarks>
        public bool IsEmbeddedPML { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user is using non-QM.
        /// </summary>
        /// <value>A boolean indicating whether the user is using non-QM.</value>
        public bool IsNonQm { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a consumer portal user made a verbal submission.
        /// </summary>
        /// <value>A boolean indicating whether a consumer portal user made a verbal submission.</value>
        /// <remarks>This is only used by LQB lead-to-loan conversion.</remarks>
        public bool ConsumerPortalUserMadeVerbalSubmission { get; set; }

        /// <summary>
        /// Gets or sets the date of the verbal submission, if applicable.
        /// </summary>
        /// <value>The date of the verbal submission.</value>
        /// <remarks>This is only used by LQB lead-to-loan conversion.</remarks>
        public string ConsumerPortalVerbalSubmissionDate { get; set; }

        /// <summary>
        /// Gets or sets the comments associated with the verbal submission, if applicable.
        /// </summary>
        /// <value>The comments associated with the verbal submission.</value>
        /// <remarks>This is only used by LQB lead-to-loan conversion.</remarks>
        public string ConsumerPortalVerbalSubmissionComments { get; set; }
    }
}
