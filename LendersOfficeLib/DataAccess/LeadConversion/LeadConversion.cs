﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using LendersOffice.Admin;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.Migration;
    using LendersOffice.ObjLib.AdjustmentsAndOtherCredits;
    using LendersOffice.ObjLib.ConsumerPortal;
    using LendersOffice.ObjLib.QuickPricer;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Handles the lead-to-loan conversion process for both LQB and PML.
    /// </summary>
    public static class LeadConversion
    {
        /// <summary>
        /// The LQB implementation for converting a lead to a loan.
        /// </summary>
        /// <param name="principal">The user performing the conversion.</param>
        /// <param name="parameters">The parameters for the lead conversion.</param>
        /// <returns>
        /// The conversion results, which may include the ID of the converted loan and any messages or
        /// errors related to the conversion.
        /// </returns>
        public static LeadConversionResults ConvertLeadToLoan_Lqb(AbstractUserPrincipal principal, LeadConversionParameters parameters)
        {
            try
            {
                string denialMessage;
                if (!VerifyLeadConversionPermission(principal, parameters, out denialMessage))
                {
                    var exception = new CBaseException(denialMessage, denialMessage);
                    exception.IsEmailDeveloper = false;
                    throw exception;
                }

                var brokerDb = BrokerDB.RetrieveById(principal.BrokerId);
                var results = new LeadConversionResults();

                string oldLeadNum = string.Empty;
                string newLoanNum = string.Empty;
                Guid loanToRecordAudit = Guid.Empty;

                if (!parameters.CreateFromTemplate)
                {
                    // 1/23/2006 dd - Only Change status of lead to loan.                    
                    CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(parameters.LeadId, typeof(LeadConversion));
                    dataLoan.ByPassFieldSecurityCheck = true;
                    dataLoan.InitSave(parameters.FileVersion);

                    oldLeadNum = dataLoan.sLNm;
                    loanToRecordAudit = parameters.LeadId;

                    if (!Tools.IsStatusLead(dataLoan.sStatusT))
                    {
                        throw new CBaseException("This lead has already been converted into a loan, with name: " + dataLoan.sLNm, "User tried to convert lead to loan, when the file was already a loan. Loan Id: " + dataLoan.sLId);
                    }

                    dataLoan.sStatusLckd = false;
                    dataLoan.sStatusT = E_sStatusT.Loan_Open;

                    string mersMin = string.Empty;
                    if (parameters.RemoveLeadPrefix)
                    {
                        if (dataLoan.sLNm.ToLower().StartsWith("lead"))
                        {
                            dataLoan.SetsLNmWithPermissionBypass(dataLoan.sLNm.Substring(4));
                        }

                        if (brokerDb.IsAutoGenerateMersMin)
                        {
                            string mersErrors;
                            if (!Tools.TryGenerateMersMin(principal, dataLoan.sLNm, out mersMin, out mersErrors))
                            {
                                mersMin = string.Empty;
                            }

                            if (!string.IsNullOrWhiteSpace(mersErrors))
                            {
                                results.MersErrors = mersErrors;
                            }
                        }
                    }
                    else
                    {
                        CLoanFileNamer loanNamer = new CLoanFileNamer();
                        string referenceNumber = string.Empty;
                        string generatedLoanNumber = loanNamer.GetLoanAutoName(principal.BrokerId, dataLoan.sBranchId, false, false, true/* enableMersGeneration*/, out mersMin, out referenceNumber);
                        dataLoan.SetsLNmWithPermissionBypass(generatedLoanNumber);
                    }

                    dataLoan.DisableMersMinCheck = true;
                    dataLoan.sMersMin = mersMin; // OPM 246033 - sets sMersMin to blank if generation fails.
                    dataLoan.DisableMersMinCheck = false;

                    dataLoan.sOpenedD_rep = DateTime.Now.ToShortDateString();
                    dataLoan.sFHALenderIdCode = Tools.GetFhaLenderIDFromLoanBranchOrBroker(dataLoan.sBrokerId, dataLoan.sLId, dataLoan.sBranchId);

                    dataLoan.sConsumerPortalVerbalSubmissionD_rep = string.Empty;
                    dataLoan.sConsumerPortalVerbalSubmissionComments = string.Empty;

                    if (dataLoan.sIsConsumerPortalCreatedButNotSubmitted)
                    {
                        if (parameters.ConsumerPortalUserMadeVerbalSubmission)
                        {
                            dataLoan.sConsumerPortalVerbalSubmissionD_rep = parameters.ConsumerPortalVerbalSubmissionDate;
                            dataLoan.sConsumerPortalVerbalSubmissionComments = parameters.ConsumerPortalVerbalSubmissionComments;
                        }
                    }

                    if (dataLoan.sIsConsumerPortalCreatedButNotSubmitted
                        && !dataLoan.sConsumerPortalVerbalSubmissionD.IsValid)
                    {
                        var msg = "You may not convert a lead created in consumer portal without a submission date or a verbal submission date.";
                        throw new CBaseException(msg, msg);
                    }

                    dataLoan.sLegalEntityIdentifier = dataLoan.Branch.LegalEntityIdentifier;

                    dataLoan.Save();
                    CLoanFileCreator.WriteInitialOfficialAgentList(dataLoan.sLId, BrokerDB.RetrieveById(principal.BrokerId), BranchAssignmentOption.Allow);
                    results.Action = "Close";

                    if (CanReadConvertedLoan(principal, parameters.LeadId))
                    {
                        results.ConvertedLoanId = parameters.LeadId;
                    }

                    newLoanNum = dataLoan.sLNm;
                }
                else
                {
                    // 1) Create new loan from Template.
                    // 2) Copy visible data to new loan.
                    // 3) Delete the lead file.
                    CPageData srcDataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(parameters.LeadId, typeof(LeadConversion));
                    srcDataLoan.InitLoad();

                    List<Tuple<Guid, Guid>> appMap = new List<Tuple<Guid, Guid>>();

                    if (!Tools.IsStatusLead(srcDataLoan.sStatusT))
                    {
                        throw new CBaseException("This lead has already been converted into a loan, with name: " + srcDataLoan.sLNm, "User tried to convert lead to loan, when the file was already a loan. Loan Id: " + srcDataLoan.sLId);
                    }

                    var defaults = new Dictionary<string, object>()
                    {
                        ["sUse2016ComboAndProrationUpdatesTo1003Details"] = srcDataLoan.sUse2016ComboAndProrationUpdatesTo1003Details,
                        [nameof(CPageBase.sLegalEntityIdentifier)] = brokerDb.LegalEntityIdentifier
                    };

                    CLoanFileCreator creator = CLoanFileCreator.GetCreator(principal, srcDataLoan.sBranchId, LendersOffice.Audit.E_LoanCreationSource.LeadConvertToLoanUsingTemplate);
                    Guid loanId = creator.BeginCreateNewLoanFromLead(sourceLeadId: parameters.TemplateId, defaultValues: defaults, isLead: false);

                    CPageData destDataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(LeadConversion));

                    // This is a system action--we can ignore user-permission
                    srcDataLoan.ByPassFieldSecurityCheck = true;
                    destDataLoan.ByPassFieldSecurityCheck = true;

                    // OPM 109480 gf 1/21/13
                    // The newly created loan will already have 1 application.
                    // Add an app for each additional app in the source file.
                    for (int appNum = 1; appNum < srcDataLoan.nApps; appNum++)
                    {
                        destDataLoan.AddNewApp();
                    }

                    // Init save after adding the applications to the file.
                    destDataLoan.InitSave(parameters.FileVersion);

                    // Upconvert underlying lead file if created loan will be a higher version.
                    srcDataLoan.EnsureMigratedToAtLeastUladCollectionVersion(destDataLoan.sBorrowerApplicationCollectionT);

                    // OPM 243181 - In order to keep the same name after conversion, we cannot
                    // set destination loan name until source loan is deleted/invalidated.
                    bool setNameAfterSrcDeleted = false;
                    string loanName = srcDataLoan.sLNm;
                    if (parameters.RemoveLeadPrefix)
                    {
                        if (loanName.ToLower().StartsWith("lead"))
                        {
                            destDataLoan.SetsLNmWithPermissionBypass(loanName.Substring(4));
                        }
                        else if (brokerDb.IsForceLeadToUseLoanCounter)
                        {
                            setNameAfterSrcDeleted = true;
                        }
                    }

                    // 08/14/2015 - je - OPM 222507 - Set disclosure type to match that of the source.
                    destDataLoan.sDisclosureRegulationTLckd = srcDataLoan.sDisclosureRegulationTLckd;
                    destDataLoan.sDisclosureRegulationT = srcDataLoan.sDisclosureRegulationT;

                    destDataLoan.sGseTargetApplicationTLckd = srcDataLoan.sGseTargetApplicationTLckd;
                    destDataLoan.sGseTargetApplicationT = srcDataLoan.sGseTargetApplicationT;

                    destDataLoan.sAlwaysRequireAllBorrowersToReceiveClosingDisclosure = srcDataLoan.sAlwaysRequireAllBorrowersToReceiveClosingDisclosure;

                    // If destination disclosure type is not locked and evaluates to TRID 2015 (sOpenedD would be the
                    // open date of the new loan) and the source closing cost fee version type is Legacy,
                    // then migrate the source lead before continuing.
                    if (destDataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID && srcDataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
                    {
                        // Migrate loan
                        ClosingCostFeeMigration.Migrate(principal, parameters.LeadId, destDataLoan.sClosingCostFeeVersionT);

                        // Reload source
                        srcDataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(parameters.LeadId, typeof(LeadConversion));
                        srcDataLoan.InitLoad();
                        srcDataLoan.ByPassFieldSecurityCheck = true;
                    }

                    // je - 10/03/15 - If destination disclosure type evaluates to TRID 2015 but the source disclosure type does not,
                    // then lock source type to TRID 2015.
                    // NOTE: Source data loan would have already been migrated by previous if block, if it had not been migrated previously.
                    if (destDataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID && srcDataLoan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID)
                    {
                        srcDataLoan.sDisclosureRegulationTLckd = true;
                        srcDataLoan.sDisclosureRegulationT = E_sDisclosureRegulationT.TRID;
                    }

                    // OPM 406379 ejm - Want to grab the product filter from the lead. 
                    if (srcDataLoan.sRawSelectedProductCodeFilter.Any())
                    {
                        destDataLoan.sRawSelectedProductCodeFilter = srcDataLoan.sRawSelectedProductCodeFilter;
                    }

                    // OPM 236635 ejm - We want to copy the migration audit and loan version from the lead, rather than from the template.
                    destDataLoan.sLoanVersionT = srcDataLoan.sLoanVersionT;
                    destDataLoan.ManuallySetLoanDataMigrationAuditHistory(srcDataLoan.sLoanDataMigrationAuditHistory);

                    // 01/22/2015 ejm - opm 198068
                    // Set the new housing expense bit to match that of the source.
                    destDataLoan.sIsHousingExpenseMigrated = srcDataLoan.sIsHousingExpenseMigrated;
                    destDataLoan.sClosingCostFeeVersionT = srcDataLoan.sClosingCostFeeVersionT;

                    // OPM 238031 ejm - New HMDA fields
                    destDataLoan.sHmdaLenderHasPreapprovalProgram = srcDataLoan.sHmdaLenderHasPreapprovalProgram;
                    destDataLoan.sHmdaBorrowerRequestedPreapproval = srcDataLoan.sHmdaBorrowerRequestedPreapproval;
                    destDataLoan.sHmdaPreapprovalTLckd = srcDataLoan.sHmdaPreapprovalTLckd;

                    destDataLoan.sHmdaLoanAmount = srcDataLoan.sHmdaLoanAmount;
                    destDataLoan.sHmdaLoanAmountLckd = srcDataLoan.sHmdaLoanAmountLckd;

                    // 6/28/2016 ejm - opm 74472 
                    // Added new rate floor fields.
                    destDataLoan.sRAdjFloorAddR = srcDataLoan.sRAdjFloorAddR;
                    destDataLoan.sRAdjFloorCalcT = srcDataLoan.sRAdjFloorCalcT;

                    destDataLoan.sLeadSrcDesc = srcDataLoan.sLeadSrcDesc;
                    destDataLoan.sConsumerPortalCreationD = srcDataLoan.sConsumerPortalCreationD;
                    destDataLoan.sConsumerPortalCreationT = srcDataLoan.sConsumerPortalCreationT;
                    destDataLoan.sConsumerPortalSubmittedD = srcDataLoan.sConsumerPortalSubmittedD;
                    destDataLoan.sConsumerPortalVerbalSubmissionD_rep = string.Empty;
                    destDataLoan.sConsumerPortalVerbalSubmissionComments = string.Empty;

                    // 4/28/2015 ejm - opm 179810
                    // Merge the official contacts list rather than using only the template's.
                    Tools.MergeAgentDataSets(srcDataLoan.sAgentDataSet, destDataLoan.sAgentDataSet);

                    // Need to do this to update serialization fields. 
                    destDataLoan.sAgentDataSet = destDataLoan.sAgentDataSet;

                    if (destDataLoan.sIsConsumerPortalCreatedButNotSubmitted)
                    {
                        if (parameters.ConsumerPortalUserMadeVerbalSubmission)
                        {
                            destDataLoan.sConsumerPortalVerbalSubmissionD_rep = parameters.ConsumerPortalVerbalSubmissionDate;
                            destDataLoan.sConsumerPortalVerbalSubmissionComments = parameters.ConsumerPortalVerbalSubmissionComments;
                        }
                    }

                    if (destDataLoan.sIsConsumerPortalCreatedButNotSubmitted
                        && !destDataLoan.sConsumerPortalVerbalSubmissionD.IsValid)
                    {
                        var msg = "You may not convert a lead created in consumer portal without a submission date or a verbal submission date.";
                        throw new CBaseException(msg, msg);
                    }

                    // Copy applications
                    // OPM 109480 gf 1/21/13
                    for (int appNum = 0; appNum < srcDataLoan.nApps; ++appNum)
                    {
                        CAppData srcDataApp = srcDataLoan.GetAppData(appNum);
                        CAppData destDataApp = destDataLoan.GetAppData(appNum);

                        appMap.Add(Tuple.Create(srcDataApp.aAppId, destDataApp.aAppId));

                        // OPM 203382 ejm 3/17/2015 - Add fields for big lead/loan info page
                        destDataApp.aVaEntitleCode = srcDataApp.aVaEntitleCode;
                        destDataApp.aVaEntitleAmt = srcDataApp.aVaEntitleAmt;
                        destDataApp.aVaServiceBranchT = srcDataApp.aVaServiceBranchT;
                        destDataApp.aVaMilitaryStatT = srcDataApp.aVaMilitaryStatT;

                        destDataApp.aBIsSurvivingSpouseOfVeteran = srcDataApp.aBIsSurvivingSpouseOfVeteran;
                        destDataApp.aCIsSurvivingSpouseOfVeteran = srcDataApp.aCIsSurvivingSpouseOfVeteran;

                        destDataApp.aBFirstNm = srcDataApp.aBFirstNm;
                        destDataApp.aBPreferredNm = srcDataApp.aBPreferredNm;
                        destDataApp.aBPreferredNmLckd = srcDataApp.aBPreferredNmLckd;
                        destDataApp.aBMidNm = srcDataApp.aBMidNm;
                        destDataApp.aBLastNm = srcDataApp.aBLastNm;
                        destDataApp.aBSuffix = srcDataApp.aBSuffix;
                        destDataApp.aBDob_rep = srcDataApp.aBDob_rep;
                        destDataApp.aBAge = srcDataApp.aBAge;
                        destDataApp.aBSsn = srcDataApp.aBSsn;
                        destDataApp.aBMaritalStatT = srcDataApp.aBMaritalStatT;

                        destDataApp.aBEmail = srcDataApp.aBEmail;
                        destDataApp.aBHPhone = srcDataApp.aBHPhone;
                        destDataApp.aBBusPhone = srcDataApp.aBBusPhone;
                        destDataApp.aBCellPhone = srcDataApp.aBCellPhone;
                        destDataApp.aBFax = srcDataApp.aBFax;

                        destDataApp.aHasCoborrowerData = srcDataApp.aHasCoborrowerData;
                        destDataApp.aCFirstNm = srcDataApp.aCFirstNm;
                        destDataApp.aCPreferredNm = srcDataApp.aCPreferredNm;
                        destDataApp.aCPreferredNmLckd = srcDataApp.aCPreferredNmLckd;
                        destDataApp.aCMidNm = srcDataApp.aCMidNm;
                        destDataApp.aCLastNm = srcDataApp.aCLastNm;
                        destDataApp.aCSuffix = srcDataApp.aCSuffix;
                        destDataApp.aCDob_rep = srcDataApp.aCDob_rep;
                        destDataApp.aCAge = srcDataApp.aCAge;
                        destDataApp.aCSsn = srcDataApp.aCSsn;
                        destDataApp.aCMaritalStatT = srcDataApp.aCMaritalStatT;

                        CopyBorrowerAddressFields(srcDataLoan, srcDataApp, destDataApp);

                        destDataApp.aBDependNum = srcDataApp.aBDependNum;
                        destDataApp.aBSchoolYrs = srcDataApp.aBSchoolYrs;
                        destDataApp.aCDependNum = srcDataApp.aCDependNum;
                        destDataApp.aCSchoolYrs = srcDataApp.aCSchoolYrs;

                        destDataApp.aVaServ1Ssn = srcDataApp.aVaServ1Ssn;
                        destDataApp.aVaServ2Ssn = srcDataApp.aVaServ2Ssn;
                        destDataApp.aVaServ3Ssn = srcDataApp.aVaServ3Ssn;
                        destDataApp.aVaServ4Ssn = srcDataApp.aVaServ4Ssn;

                        if (!string.Equals(srcDataApp.aBSsn, srcDataApp.aB4506TSsnTinEin, StringComparison.Ordinal))
                        {
                            destDataApp.aB4506TSsnTinEin = srcDataApp.aB4506TSsnTinEin;
                        }

                        if (!string.Equals(srcDataApp.aCSsn, srcDataApp.aC4506TSsnTinEin, StringComparison.Ordinal))
                        {
                            destDataApp.aC4506TSsnTinEin = srcDataApp.aC4506TSsnTinEin;
                        }

                        // Credit
                        // SK opm 94181 10/3/2012
                        destDataApp.aCrRd = srcDataApp.aCrRd;
                        destDataApp.aCrOd = srcDataApp.aCrOd;
                        destDataApp.aCrDueD = srcDataApp.aCrDueD;
                        destDataApp.aCrN = srcDataApp.aCrN;

                        destDataApp.aLqiCrRd = srcDataApp.aLqiCrRd;
                        destDataApp.aLqiCrOd = srcDataApp.aLqiCrOd;
                        destDataApp.aLqiCrDueD = srcDataApp.aLqiCrDueD;
                        destDataApp.aLqiCrN = srcDataApp.aLqiCrN;

                        destDataApp.aBCreditAuthorizationD_rep = srcDataApp.aBCreditAuthorizationD_rep;
                        destDataApp.aCCreditAuthorizationD_rep = srcDataApp.aCCreditAuthorizationD_rep;

                        destDataApp.aPresRent_rep = srcDataApp.aPresRent_rep;
                        destDataApp.aPres1stM_rep = srcDataApp.aPres1stM_rep;
                        destDataApp.aPresOFin_rep = srcDataApp.aPresOFin_rep;
                        destDataApp.aPresHazIns_rep = srcDataApp.aPresHazIns_rep;
                        destDataApp.aPresHoAssocDues_rep = srcDataApp.aPresHoAssocDues_rep;
                        destDataApp.aPresRealETx_rep = srcDataApp.aPresRealETx_rep;
                        destDataApp.aPresMIns_rep = srcDataApp.aPresMIns_rep;
                        destDataApp.aPresOHExp_rep = srcDataApp.aPresOHExp_rep;

                        if (!srcDataLoan.sIsIncomeCollectionEnabled)
                        {
                            destDataApp.aBBaseI_rep = srcDataApp.aBBaseI_rep;
                            destDataApp.aCBaseI_rep = srcDataApp.aCBaseI_rep;
                            destDataApp.aBOvertimeI_rep = srcDataApp.aBOvertimeI_rep;
                            destDataApp.aCOvertimeI_rep = srcDataApp.aCOvertimeI_rep;
                            destDataApp.aBBonusesI_rep = srcDataApp.aBBonusesI_rep;
                            destDataApp.aCBonusesI_rep = srcDataApp.aCBonusesI_rep;
                            destDataApp.aBCommisionI_rep = srcDataApp.aBCommisionI_rep;
                            destDataApp.aCCommisionI_rep = srcDataApp.aCCommisionI_rep;
                            destDataApp.aBDividendI_rep = srcDataApp.aBDividendI_rep;
                            destDataApp.aCDividendI_rep = srcDataApp.aCDividendI_rep;
                            destDataApp.aOtherIncomeList = srcDataApp.aOtherIncomeList;
                        }

                        destDataApp.aNetRentI1003Lckd = srcDataApp.aNetRentI1003Lckd;
                        destDataApp.aBNetRentI1003_rep = srcDataApp.aBNetRentI1003_rep;
                        destDataApp.aCNetRentI1003_rep = srcDataApp.aCNetRentI1003_rep;

                        destDataApp.aBDecJudgment = srcDataApp.aBDecJudgment;
                        destDataApp.aCDecJudgment = srcDataApp.aCDecJudgment;
                        destDataApp.aBDecBankrupt = srcDataApp.aBDecBankrupt;
                        destDataApp.aCDecBankrupt = srcDataApp.aCDecBankrupt;
                        destDataApp.aBDecForeclosure = srcDataApp.aBDecForeclosure;
                        destDataApp.aCDecForeclosure = srcDataApp.aCDecForeclosure;
                        destDataApp.aBDecLawsuit = srcDataApp.aBDecLawsuit;
                        destDataApp.aCDecLawsuit = srcDataApp.aCDecLawsuit;
                        destDataApp.aBDecObligated = srcDataApp.aBDecObligated;
                        destDataApp.aCDecObligated = srcDataApp.aCDecObligated;
                        destDataApp.aBDecDelinquent = srcDataApp.aBDecDelinquent;
                        destDataApp.aCDecDelinquent = srcDataApp.aCDecDelinquent;
                        destDataApp.aBDecAlimony = srcDataApp.aBDecAlimony;
                        destDataApp.aCDecAlimony = srcDataApp.aCDecAlimony;
                        destDataApp.aBDecBorrowing = srcDataApp.aBDecBorrowing;
                        destDataApp.aCDecBorrowing = srcDataApp.aCDecBorrowing;
                        destDataApp.aBDecEndorser = srcDataApp.aBDecEndorser;
                        destDataApp.aCDecEndorser = srcDataApp.aCDecEndorser;
                        destDataApp.aBDecCitizen = srcDataApp.aBDecCitizen;
                        destDataApp.aCDecCitizen = srcDataApp.aCDecCitizen;
                        destDataApp.aBDecResidency = srcDataApp.aBDecResidency;
                        destDataApp.aCDecResidency = srcDataApp.aCDecResidency;
                        destDataApp.aBDecOcc = srcDataApp.aBDecOcc;
                        destDataApp.aCDecOcc = srcDataApp.aCDecOcc;
                        destDataApp.aBDecPastOwnership = srcDataApp.aBDecPastOwnership;
                        destDataApp.aCDecPastOwnership = srcDataApp.aCDecPastOwnership;
                        destDataApp.aBDecPastOwnedPropT = srcDataApp.aBDecPastOwnedPropT;
                        destDataApp.aCDecPastOwnedPropT = srcDataApp.aCDecPastOwnedPropT;
                        destDataApp.aBDecPastOwnedPropTitleT = srcDataApp.aBDecPastOwnedPropTitleT;
                        destDataApp.aCDecPastOwnedPropTitleT = srcDataApp.aCDecPastOwnedPropTitleT;
                        destDataApp.aBNoFurnish = srcDataApp.aBNoFurnish;
                        destDataApp.aBNoFurnishLckd = srcDataApp.aBNoFurnishLckd;
                        destDataApp.aCNoFurnish = srcDataApp.aCNoFurnish;
                        destDataApp.aCNoFurnishLckd = srcDataApp.aCNoFurnishLckd;
                        AssignRaceEthnicityData(destDataApp, srcDataApp);

                        TransferUladDeclarationData(destDataApp, srcDataApp);

                        destDataApp.aCEmail = srcDataApp.aCEmail;
                        destDataApp.aCHPhone = srcDataApp.aCHPhone;
                        destDataApp.aCBusPhone = srcDataApp.aCBusPhone;
                        destDataApp.aCCellPhone = srcDataApp.aCCellPhone;
                        destDataApp.aCFax = srcDataApp.aCFax;

                        if (srcDataLoan.sBorrowerApplicationCollectionT == E_sLqbCollectionT.Legacy)
                        {
                            destDataApp.AcceptNewLiaCollection(srcDataApp.aLiaXmlContent.Value);
                            destDataApp.AcceptNewAssetCollection(srcDataApp.aAssetXmlContent.Value);
                            destDataApp.AcceptNewBEmplmtCollection(srcDataApp.aBEmplmtXmlContent);
                            destDataApp.AcceptNewCEmplmtCollection(srcDataApp.aCEmplmtXmlContent);
                            destDataApp.AcceptNewReCollection(srcDataApp.aReXmlContent);
                        }

                        destDataApp.aBExperianScore_rep = srcDataApp.aBExperianScore_rep;
                        destDataApp.aBExperianCreatedD_rep = srcDataApp.aBExperianCreatedD_rep;
                        destDataApp.aBExperianFactors = srcDataApp.aBExperianFactors;
                        destDataApp.aCExperianScore_rep = srcDataApp.aCExperianScore_rep;
                        destDataApp.aCExperianCreatedD_rep = srcDataApp.aCExperianCreatedD_rep;
                        destDataApp.aBTransUnionScore_rep = srcDataApp.aBTransUnionScore_rep;
                        destDataApp.aBTransUnionCreatedD_rep = srcDataApp.aBTransUnionCreatedD_rep;
                        destDataApp.aBTransUnionFactors = srcDataApp.aBTransUnionFactors;
                        destDataApp.aCTransUnionScore_rep = srcDataApp.aCTransUnionScore_rep;
                        destDataApp.aCTransUnionCreatedD_rep = srcDataApp.aCTransUnionCreatedD_rep;
                        destDataApp.aCTransUnionFactors = srcDataApp.aCTransUnionFactors;
                        destDataApp.aBEquifaxScore_rep = srcDataApp.aBEquifaxScore_rep;
                        destDataApp.aBEquifaxCreatedD_rep = srcDataApp.aBEquifaxCreatedD_rep;
                        destDataApp.aBEquifaxFactors = srcDataApp.aBEquifaxFactors;
                        destDataApp.aCEquifaxScore_rep = srcDataApp.aCEquifaxScore_rep;
                        destDataApp.aCEquifaxCreatedD_rep = srcDataApp.aCEquifaxCreatedD_rep;
                        destDataApp.aCEquifaxFactors = srcDataApp.aCEquifaxFactors;
                        destDataApp.aBExperianModelT = srcDataApp.aBExperianModelT;
                        destDataApp.aBTransUnionModelT = srcDataApp.aBTransUnionModelT;
                        destDataApp.aBEquifaxModelT = srcDataApp.aBEquifaxModelT;
                        destDataApp.aCExperianModelT = srcDataApp.aCExperianModelT;
                        destDataApp.aCTransUnionModelT = srcDataApp.aCTransUnionModelT;
                        destDataApp.aCEquifaxModelT = srcDataApp.aCEquifaxModelT;

                        // 9/27/2013 gf - from PML Summary
                        destDataApp.aProdBCitizenT = srcDataApp.aProdBCitizenT;
                        destDataApp.aIsBorrSpousePrimaryWageEarner = srcDataApp.aIsBorrSpousePrimaryWageEarner;

                        destDataApp.aBTypeT = srcDataApp.aBTypeT;
                        destDataApp.aCTypeT = srcDataApp.aCTypeT;
                        destDataApp.aBIsVeteran = srcDataApp.aBIsVeteran;
                        destDataApp.aCIsVeteran = srcDataApp.aCIsVeteran;
                        destDataApp.aEmplrBusPhoneLckd = srcDataApp.aEmplrBusPhoneLckd;
                        destDataApp.aPresOHExpDesc = srcDataApp.aPresOHExpDesc;

                        destDataApp.aBAliases = srcDataApp.aBAliases;
                        destDataApp.aCAliases = srcDataApp.aCAliases;

                        // 9/12/2013 gf - The regions below do not necessarily
                        // contain all of the fields assigned to from that page.
                        // If the field was already assigned elsewhere it was
                        // left out.
                        // LoanInfo (Expanded)
                        destDataApp.aOccT = srcDataApp.aOccT;

                        // 1003 Pg 1 (Expanded)
                        destDataApp.aBDependAges = srcDataApp.aBDependAges;
                        destDataApp.aBCoreSystemId = srcDataApp.aBCoreSystemId;
                        destDataApp.aCDependAges = srcDataApp.aCDependAges;
                        destDataApp.aCCoreSystemId = srcDataApp.aCCoreSystemId;
                        destDataApp.aManner = srcDataApp.aManner;
                        destDataApp.aSpouseIExcl = srcDataApp.aSpouseIExcl;
                        destDataApp.aTitleNm1 = srcDataApp.aTitleNm1;
                        destDataApp.aTitleNm2 = srcDataApp.aTitleNm2;

                        // 1003 Pg 2 (Expanded)
                        destDataApp.aAsstLiaCompletedNotJointly = srcDataApp.aAsstLiaCompletedNotJointly;

                        // 1003 Pg 3 (Expanded)
                        destDataApp.aAltNm1 = srcDataApp.aAltNm1;
                        destDataApp.aAltNm1AccNum = srcDataApp.aAltNm1AccNum;
                        destDataApp.aAltNm1CreditorNm = srcDataApp.aAltNm1CreditorNm;
                        destDataApp.aAltNm2 = srcDataApp.aAltNm2;
                        destDataApp.aAltNm2AccNum = srcDataApp.aAltNm2AccNum;
                        destDataApp.aAltNm2CreditorNm = srcDataApp.aAltNm2CreditorNm;
                        destDataApp.aBDecForeignNational = srcDataApp.aBDecForeignNational;

                        // 1003 Pg 4 (Expanded)
                        destDataApp.a1003ContEditSheet = srcDataApp.a1003ContEditSheet;

                        // Credit Denial (Expanded)
                        destDataApp.aDenialNoCreditFile = srcDataApp.aDenialNoCreditFile;
                        destDataApp.aDenialInsufficientCreditRef = srcDataApp.aDenialInsufficientCreditRef;
                        destDataApp.aDenialInsufficientCreditFile = srcDataApp.aDenialInsufficientCreditFile;
                        destDataApp.aDenialUnableVerifyCreditRef = srcDataApp.aDenialUnableVerifyCreditRef;
                        destDataApp.aDenialGarnishment = srcDataApp.aDenialGarnishment;
                        destDataApp.aDenialExcessiveObligations = srcDataApp.aDenialExcessiveObligations;
                        destDataApp.aDenialInsufficientIncome = srcDataApp.aDenialInsufficientIncome;
                        destDataApp.aDenialUnacceptablePmtRecord = srcDataApp.aDenialUnacceptablePmtRecord;
                        destDataApp.aDenialLackOfCashReserves = srcDataApp.aDenialLackOfCashReserves;
                        destDataApp.aDenialDeliquentCreditObligations = srcDataApp.aDenialDeliquentCreditObligations;
                        destDataApp.aDenialBankruptcy = srcDataApp.aDenialBankruptcy;
                        destDataApp.aDenialInfoFromConsumerReportAgency = srcDataApp.aDenialInfoFromConsumerReportAgency;
                        destDataApp.aDenialUnableVerifyEmployment = srcDataApp.aDenialUnableVerifyEmployment;
                        destDataApp.aDenialLenOfEmployment = srcDataApp.aDenialLenOfEmployment;
                        destDataApp.aDenialTemporaryEmployment = srcDataApp.aDenialTemporaryEmployment;
                        destDataApp.aDenialInsufficientIncomeForMortgagePmt = srcDataApp.aDenialInsufficientIncomeForMortgagePmt;
                        destDataApp.aDenialUnableVerifyIncome = srcDataApp.aDenialUnableVerifyIncome;
                        destDataApp.aDenialTempResidence = srcDataApp.aDenialTempResidence;
                        destDataApp.aDenialShortResidencePeriod = srcDataApp.aDenialShortResidencePeriod;
                        destDataApp.aDenialUnableVerifyResidence = srcDataApp.aDenialUnableVerifyResidence;
                        destDataApp.aDenialByHUD = srcDataApp.aDenialByHUD;
                        destDataApp.aDenialByVA = srcDataApp.aDenialByVA;
                        destDataApp.aDenialByFedNationalMortAssoc = srcDataApp.aDenialByFedNationalMortAssoc;
                        destDataApp.aDenialByFedHomeLoanMortCorp = srcDataApp.aDenialByFedHomeLoanMortCorp;
                        destDataApp.aDenialByOther = srcDataApp.aDenialByOther;
                        destDataApp.aDenialByOtherDesc = srcDataApp.aDenialByOtherDesc;
                        destDataApp.aDenialInsufficientFundsToClose = srcDataApp.aDenialInsufficientFundsToClose;
                        destDataApp.aDenialCreditAppIncomplete = srcDataApp.aDenialCreditAppIncomplete;
                        destDataApp.aDenialInadequateCollateral = srcDataApp.aDenialInadequateCollateral;
                        destDataApp.aDenialUnacceptableProp = srcDataApp.aDenialUnacceptableProp;
                        destDataApp.aDenialInsufficientPropData = srcDataApp.aDenialInsufficientPropData;
                        destDataApp.aDenialUnacceptableAppraisal = srcDataApp.aDenialUnacceptableAppraisal;
                        destDataApp.aDenialUnacceptableLeasehold = srcDataApp.aDenialUnacceptableLeasehold;
                        destDataApp.aDenialNotGrantCreditToAnyAppOnSuchTermsAndConds = srcDataApp.aDenialNotGrantCreditToAnyAppOnSuchTermsAndConds;
                        destDataApp.aDenialWithdrawnByApp = srcDataApp.aDenialWithdrawnByApp;
                        destDataApp.aDenialOtherReason1 = srcDataApp.aDenialOtherReason1;
                        destDataApp.aDenialOtherReason1Desc = srcDataApp.aDenialOtherReason1Desc;
                        destDataApp.aDenialOtherReason2 = srcDataApp.aDenialOtherReason2;
                        destDataApp.aDenialOtherReason2Desc = srcDataApp.aDenialOtherReason2Desc;
                        destDataApp.aDenialDecisionBasedOnReportAgency = srcDataApp.aDenialDecisionBasedOnReportAgency;
                        destDataApp.aDenialDecisionBasedOnCRA = srcDataApp.aDenialDecisionBasedOnCRA;
                        destDataApp.aDenialAdditionalStatement = srcDataApp.aDenialAdditionalStatement;
                        destDataApp.aDenialWeObtainedCreditScoreFromCRA = srcDataApp.aDenialWeObtainedCreditScoreFromCRA;
                        destDataApp.aDenialCreditScore_rep = srcDataApp.aDenialCreditScore_rep;
                        destDataApp.aDenialCreditScoreD_rep = srcDataApp.aDenialCreditScoreD_rep;
                        destDataApp.aDenialCreditScoreRangeFrom_rep = srcDataApp.aDenialCreditScoreRangeFrom_rep;
                        destDataApp.aDenialCreditScoreRangeTo_rep = srcDataApp.aDenialCreditScoreRangeTo_rep;
                        destDataApp.aDenialCreditScoreFactor1 = srcDataApp.aDenialCreditScoreFactor1;
                        destDataApp.aDenialCreditScoreFactor2 = srcDataApp.aDenialCreditScoreFactor2;
                        destDataApp.aDenialCreditScoreFactor3 = srcDataApp.aDenialCreditScoreFactor3;
                        destDataApp.aDenialCreditScoreFactor4 = srcDataApp.aDenialCreditScoreFactor4;
                        destDataApp.aDenialIsFactorNumberOfRecentInquiries = srcDataApp.aDenialIsFactorNumberOfRecentInquiries;
                        destDataApp.aCrRd_rep = srcDataApp.aCrRd_rep;
                        destDataApp.aCDenialNoCreditFile = srcDataApp.aCDenialNoCreditFile;
                        destDataApp.aCDenialInsufficientCreditRef = srcDataApp.aCDenialInsufficientCreditRef;
                        destDataApp.aCDenialInsufficientCreditFile = srcDataApp.aCDenialInsufficientCreditFile;
                        destDataApp.aCDenialUnableVerifyCreditRef = srcDataApp.aCDenialUnableVerifyCreditRef;
                        destDataApp.aCDenialGarnishment = srcDataApp.aCDenialGarnishment;
                        destDataApp.aCDenialExcessiveObligations = srcDataApp.aCDenialExcessiveObligations;
                        destDataApp.aCDenialInsufficientIncome = srcDataApp.aCDenialInsufficientIncome;
                        destDataApp.aCDenialUnacceptablePmtRecord = srcDataApp.aCDenialUnacceptablePmtRecord;
                        destDataApp.aCDenialLackOfCashReserves = srcDataApp.aCDenialLackOfCashReserves;
                        destDataApp.aCDenialDeliquentCreditObligations = srcDataApp.aCDenialDeliquentCreditObligations;
                        destDataApp.aCDenialBankruptcy = srcDataApp.aCDenialBankruptcy;
                        destDataApp.aCDenialInfoFromConsumerReportAgency = srcDataApp.aCDenialInfoFromConsumerReportAgency;
                        destDataApp.aCDenialUnableVerifyEmployment = srcDataApp.aCDenialUnableVerifyEmployment;
                        destDataApp.aCDenialLenOfEmployment = srcDataApp.aCDenialLenOfEmployment;
                        destDataApp.aCDenialTemporaryEmployment = srcDataApp.aCDenialTemporaryEmployment;
                        destDataApp.aCDenialInsufficientIncomeForMortgagePmt = srcDataApp.aCDenialInsufficientIncomeForMortgagePmt;
                        destDataApp.aCDenialUnableVerifyIncome = srcDataApp.aCDenialUnableVerifyIncome;
                        destDataApp.aCDenialTempResidence = srcDataApp.aCDenialTempResidence;
                        destDataApp.aCDenialShortResidencePeriod = srcDataApp.aCDenialShortResidencePeriod;
                        destDataApp.aCDenialUnableVerifyResidence = srcDataApp.aCDenialUnableVerifyResidence;
                        destDataApp.aCDenialByHUD = srcDataApp.aCDenialByHUD;
                        destDataApp.aCDenialByVA = srcDataApp.aCDenialByVA;
                        destDataApp.aCDenialByFedNationalMortAssoc = srcDataApp.aCDenialByFedNationalMortAssoc;
                        destDataApp.aCDenialByFedHomeLoanMortCorp = srcDataApp.aCDenialByFedHomeLoanMortCorp;
                        destDataApp.aCDenialByOther = srcDataApp.aCDenialByOther;
                        destDataApp.aCDenialByOtherDesc = srcDataApp.aCDenialByOtherDesc;
                        destDataApp.aCDenialInsufficientFundsToClose = srcDataApp.aCDenialInsufficientFundsToClose;
                        destDataApp.aCDenialCreditAppIncomplete = srcDataApp.aCDenialCreditAppIncomplete;
                        destDataApp.aCDenialInadequateCollateral = srcDataApp.aCDenialInadequateCollateral;
                        destDataApp.aCDenialUnacceptableProp = srcDataApp.aCDenialUnacceptableProp;
                        destDataApp.aCDenialInsufficientPropData = srcDataApp.aCDenialInsufficientPropData;
                        destDataApp.aCDenialUnacceptableAppraisal = srcDataApp.aCDenialUnacceptableAppraisal;
                        destDataApp.aCDenialUnacceptableLeasehold = srcDataApp.aCDenialUnacceptableLeasehold;
                        destDataApp.aCDenialNotGrantCreditToAnyAppOnSuchTermsAndConds = srcDataApp.aCDenialNotGrantCreditToAnyAppOnSuchTermsAndConds;
                        destDataApp.aCDenialWithdrawnByApp = srcDataApp.aCDenialWithdrawnByApp;
                        destDataApp.aCDenialOtherReason1 = srcDataApp.aCDenialOtherReason1;
                        destDataApp.aCDenialOtherReason1Desc = srcDataApp.aCDenialOtherReason1Desc;
                        destDataApp.aCDenialOtherReason2 = srcDataApp.aCDenialOtherReason2;
                        destDataApp.aCDenialOtherReason2Desc = srcDataApp.aCDenialOtherReason2Desc;
                        destDataApp.aCDenialDecisionBasedOnReportAgency = srcDataApp.aCDenialDecisionBasedOnReportAgency;
                        destDataApp.aCDenialDecisionBasedOnCRA = srcDataApp.aCDenialDecisionBasedOnCRA;
                        destDataApp.aCDenialAdditionalStatement = srcDataApp.aCDenialAdditionalStatement;
                        destDataApp.aCDenialWeObtainedCreditScoreFromCRA = srcDataApp.aCDenialWeObtainedCreditScoreFromCRA;
                        destDataApp.aCDenialCreditScore_rep = srcDataApp.aCDenialCreditScore_rep;
                        destDataApp.aCDenialCreditScoreD_rep = srcDataApp.aCDenialCreditScoreD_rep;
                        destDataApp.aCDenialCreditScoreRangeFrom_rep = srcDataApp.aCDenialCreditScoreRangeFrom_rep;
                        destDataApp.aCDenialCreditScoreRangeTo_rep = srcDataApp.aCDenialCreditScoreRangeTo_rep;
                        destDataApp.aCDenialCreditScoreFactor1 = srcDataApp.aCDenialCreditScoreFactor1;
                        destDataApp.aCDenialCreditScoreFactor2 = srcDataApp.aCDenialCreditScoreFactor2;
                        destDataApp.aCDenialCreditScoreFactor3 = srcDataApp.aCDenialCreditScoreFactor3;
                        destDataApp.aCDenialCreditScoreFactor4 = srcDataApp.aCDenialCreditScoreFactor4;
                        destDataApp.aCDenialIsFactorNumberOfRecentInquiries = srcDataApp.aCDenialIsFactorNumberOfRecentInquiries;
                        destDataApp.aCCrRd_rep = srcDataApp.aCCrRd_rep;

                        // VA Loan Analysis (Expanded)
                        destDataApp.aFHABCaivrsNum = srcDataApp.aFHABCaivrsNum;
                        destDataApp.aFHACCaivrsNum = srcDataApp.aFHACCaivrsNum;
                        destDataApp.aVaBEmplmtI_rep = srcDataApp.aVaBEmplmtI_rep;
                        destDataApp.aVaBEmplmtILckd = srcDataApp.aVaBEmplmtILckd;
                        destDataApp.aVaBFedITax_rep = srcDataApp.aVaBFedITax_rep;
                        destDataApp.aVaBOITax_rep = srcDataApp.aVaBOITax_rep;
                        destDataApp.aVaBONetI_rep = srcDataApp.aVaBONetI_rep;
                        destDataApp.aVaBONetILckd = srcDataApp.aVaBONetILckd;
                        destDataApp.aVaBSsnTax_rep = srcDataApp.aVaBSsnTax_rep;
                        destDataApp.aVaBStateITax_rep = srcDataApp.aVaBStateITax_rep;
                        destDataApp.aVaCEmplmtI_rep = srcDataApp.aVaCEmplmtI_rep;
                        destDataApp.aVaCEmplmtILckd = srcDataApp.aVaCEmplmtILckd;
                        destDataApp.aVaCFedITax_rep = srcDataApp.aVaCFedITax_rep;
                        destDataApp.aVaCOITax_rep = srcDataApp.aVaCOITax_rep;
                        destDataApp.aVaCONetI_rep = srcDataApp.aVaCONetI_rep;
                        destDataApp.aVaCONetILckd = srcDataApp.aVaCONetILckd;
                        destDataApp.aVaCSsnTax_rep = srcDataApp.aVaCSsnTax_rep;
                        destDataApp.aVaCStateITax_rep = srcDataApp.aVaCStateITax_rep;
                        destDataApp.aVaCrRecordSatisfyTri = srcDataApp.aVaCrRecordSatisfyTri;
                        destDataApp.aVaFamilySuportGuidelineAmt_rep = srcDataApp.aVaFamilySuportGuidelineAmt_rep;
                        destDataApp.aVaLAnalysisN = srcDataApp.aVaLAnalysisN;
                        destDataApp.aVaLMeetCrStandardTri = srcDataApp.aVaLMeetCrStandardTri;
                        destDataApp.aVaONetIDesc = srcDataApp.aVaONetIDesc;
                        destDataApp.aVaUtilityIncludedTri = srcDataApp.aVaUtilityIncludedTri;
                        destDataApp.aVALAnalysisValue_rep = srcDataApp.aVALAnalysisValue_rep;
                        destDataApp.aVALAnalysisExpirationD_rep = srcDataApp.aVALAnalysisExpirationD_rep;
                        destDataApp.aVALAnalysisEconomicLifeYrs_rep = srcDataApp.aVALAnalysisEconomicLifeYrs_rep;

                        // Submit to DO/DU (Expanded)
                        destDataApp.a1003SignD_rep = srcDataApp.a1003SignD_rep;
                        destDataApp.aPresTotHExpDesc = srcDataApp.aPresTotHExpDesc;
                        destDataApp.aPresTotHExpCalc_rep = srcDataApp.aPresTotHExpCalc_rep;
                        destDataApp.aPresTotHExpLckd = srcDataApp.aPresTotHExpLckd;

                        // copy disclosure fields opm 196899
                        destDataApp.aBEConsentDeclinedD = srcDataApp.aBEConsentDeclinedD;
                        destDataApp.aBEConsentReceivedD = srcDataApp.aBEConsentReceivedD;
                        destDataApp.aCEConsentDeclinedD = srcDataApp.aCEConsentDeclinedD;
                        destDataApp.aCEConsentReceivedD = srcDataApp.aCEConsentReceivedD;

                        // OPM 223001 - Preserve VA Fields.
                        destDataApp.aIsBVAElig = srcDataApp.aIsBVAElig;
                        destDataApp.aIsCVAElig = srcDataApp.aIsCVAElig;
                        destDataApp.aIsBVAFFEx = srcDataApp.aIsBVAFFEx;
                        destDataApp.aBVServiceT = srcDataApp.aBVServiceT;
                        destDataApp.aBVEnt = srcDataApp.aBVEnt;
                        destDataApp.aIsCVAFFEx = srcDataApp.aIsCVAFFEx;
                        destDataApp.aCVServiceT = srcDataApp.aCVServiceT;
                        destDataApp.aCVEnt = srcDataApp.aCVEnt;

                        destDataApp.CopyRespaFirstEnteredDates(srcDataApp);
                        destDataApp.a1003InterviewD = srcDataApp.a1003InterviewD;
                        destDataApp.a1003InterviewDLckd = srcDataApp.a1003InterviewDLckd;
                    }

                    if (srcDataLoan.sBorrowerApplicationCollectionT != E_sLqbCollectionT.Legacy)
                    {
                        // This needs to happen after we copy over the coborrower indicator.
                        // This will copy the ULAD apps and the entities from the source file.
                        destDataLoan.DuplicateLqbCollectionDataFrom(srcDataLoan);
                    }

                    // Copy All Fields Assignable in Lead Editor
                    if (principal.HasPermission(Permission.CanAccessLendingStaffNotes))
                    {
                        destDataLoan.sLendingStaffNotes = srcDataLoan.sLendingStaffNotes;
                    }

                    destDataLoan.sHmdaCoApplicantSourceSsnLckd = srcDataLoan.sHmdaCoApplicantSourceSsnLckd;
                    if (srcDataLoan.sHmdaCoApplicantSourceSsnLckd)
                    {
                        destDataLoan.sHmdaCoApplicantSourceSsn = srcDataLoan.sHmdaCoApplicantSourceSsn;
                    }

                    destDataLoan.sFhaSponsoredOriginatorEinLckd = srcDataLoan.sFhaSponsoredOriginatorEinLckd;
                    if (srcDataLoan.sFhaSponsoredOriginatorEinLckd)
                    {
                        destDataLoan.sFHASponsoredOriginatorEIN = srcDataLoan.sFHASponsoredOriginatorEIN;
                    }

                    destDataLoan.sCombinedBorInfoLckd = srcDataLoan.sCombinedBorInfoLckd;
                    if (srcDataLoan.sCombinedBorInfoLckd)
                    {
                        destDataLoan.sCombinedBorSsn = srcDataLoan.sCombinedBorSsn;
                        destDataLoan.sCombinedCoborSsn = srcDataLoan.sCombinedCoborSsn;
                    }

                    destDataLoan.sTax1098PayerSsnLckd = srcDataLoan.sTax1098PayerSsnLckd;
                    if (srcDataLoan.sTax1098PayerSsnLckd)
                    {
                        destDataLoan.sTax1098PayerSsn = srcDataLoan.sTax1098PayerSsn;
                    }

                    // OPM 203382 ejm 3/17/2015 - Add fields for big lead/loan info page
                    destDataLoan.sCaseAssignmentD = srcDataLoan.sCaseAssignmentD;
                    destDataLoan.sFHAHousingActSection = srcDataLoan.sFHAHousingActSection;

                    destDataLoan.sLT = srcDataLoan.sLT;
                    destDataLoan.sLPurposeT = srcDataLoan.sLPurposeT;
                    destDataLoan.sLienPosT = srcDataLoan.sLienPosT;
                    destDataLoan.sLpTemplateNm = srcDataLoan.sLpTemplateNm;
                    destDataLoan.sLoanProductIdentifier = srcDataLoan.sLoanProductIdentifier;
                    destDataLoan.sLpTemplateId = srcDataLoan.sLpTemplateId;
                    destDataLoan.sPurchPrice = srcDataLoan.sPurchPrice;
                    destDataLoan.sApprVal = srcDataLoan.sApprVal;
                    destDataLoan.sProdSpT = srcDataLoan.sProdSpT;
                    destDataLoan.sEquityCalc = srcDataLoan.sEquityCalc;
                    destDataLoan.sLAmtLckd = srcDataLoan.sLAmtLckd;
                    destDataLoan.sLAmtCalc = srcDataLoan.sLAmtCalc;
                    destDataLoan.sNoteIR = srcDataLoan.sNoteIR;
                    destDataLoan.sTerm = srcDataLoan.sTerm;
                    destDataLoan.sDue = srcDataLoan.sDue;
                    destDataLoan.sSpAddr = srcDataLoan.sSpAddr;
                    destDataLoan.sSpCity = srcDataLoan.sSpCity;
                    destDataLoan.sSpState = srcDataLoan.sSpState;
                    destDataLoan.sSpZip = srcDataLoan.sSpZip;
                    destDataLoan.sTrNotes = srcDataLoan.sTrNotes;
                    destDataLoan.sLeadD_rep = srcDataLoan.sLeadD_rep;

                    destDataLoan.sEmployeeLoanRepId = srcDataLoan.sEmployeeLoanRepId;
                    destDataLoan.sEmployeeProcessorId = srcDataLoan.sEmployeeProcessorId;
                    destDataLoan.sEmployeeLoanOpenerId = srcDataLoan.sEmployeeLoanOpenerId;
                    destDataLoan.sEmployeeCallCenterAgentId = srcDataLoan.sEmployeeCallCenterAgentId;
                    destDataLoan.sEmployeeRealEstateAgentId = srcDataLoan.sEmployeeRealEstateAgentId;
                    destDataLoan.sEmployeeLenderAccExecId = srcDataLoan.sEmployeeLenderAccExecId;
                    destDataLoan.sEmployeeLockDeskId = srcDataLoan.sEmployeeLockDeskId;
                    destDataLoan.sEmployeeUnderwriterId = srcDataLoan.sEmployeeUnderwriterId;
                    destDataLoan.sEmployeeManagerId = srcDataLoan.sEmployeeManagerId;
                    destDataLoan.sEmployeeCloserId = srcDataLoan.sEmployeeCloserId;
                    destDataLoan.sEmployeeFunderId = srcDataLoan.sEmployeeFunderId;
                    destDataLoan.sEmployeeShipperId = srcDataLoan.sEmployeeShipperId;
                    destDataLoan.sEmployeePostCloserId = srcDataLoan.sEmployeePostCloserId;
                    destDataLoan.sEmployeeInsuringId = srcDataLoan.sEmployeeInsuringId;
                    destDataLoan.sEmployeeCollateralAgentId = srcDataLoan.sEmployeeCollateralAgentId;
                    destDataLoan.sEmployeeDocDrawerId = srcDataLoan.sEmployeeDocDrawerId;

                    // 03/30/2014 ir - opm 169478 Transfer CRMNow Lead Id
                    destDataLoan.sCrmNowLeadId = srcDataLoan.sCrmNowLeadId;

                    // 11/22/2013 gf - opm 145015
                    destDataLoan.sEmployeeCreditAuditorId = srcDataLoan.sEmployeeCreditAuditorId;
                    destDataLoan.sEmployeeDisclosureDeskId = srcDataLoan.sEmployeeDisclosureDeskId;
                    destDataLoan.sEmployeeJuniorProcessorId = srcDataLoan.sEmployeeJuniorProcessorId;
                    destDataLoan.sEmployeeJuniorUnderwriterId = srcDataLoan.sEmployeeJuniorUnderwriterId;
                    destDataLoan.sEmployeeLegalAuditorId = srcDataLoan.sEmployeeLegalAuditorId;
                    destDataLoan.sEmployeeLoanOfficerAssistantId = srcDataLoan.sEmployeeLoanOfficerAssistantId;
                    destDataLoan.sEmployeePurchaserId = srcDataLoan.sEmployeePurchaserId;
                    destDataLoan.sEmployeeQCComplianceId = srcDataLoan.sEmployeeQCComplianceId;
                    destDataLoan.sEmployeeSecondaryId = srcDataLoan.sEmployeeSecondaryId;
                    destDataLoan.sEmployeeServicingId = srcDataLoan.sEmployeeServicingId;

                    // 9/27/2013 gf - opm 126044 General PML Fields from PML Summary page.
                    destDataLoan.sIsSelfEmployed = srcDataLoan.sIsSelfEmployed;
                    destDataLoan.sOccRLckd = srcDataLoan.sOccRLckd;
                    destDataLoan.sOccR_rep = srcDataLoan.sOccR_rep;
                    destDataLoan.sSpGrossRent_rep = srcDataLoan.sSpGrossRent_rep;
                    destDataLoan.sSpCountRentalIForPrimaryResidToo = srcDataLoan.sSpCountRentalIForPrimaryResidToo;
                    destDataLoan.sPriorSalesPrice_rep = srcDataLoan.sPriorSalesPrice_rep;
                    destDataLoan.sPriorSalesPropertySellerT = srcDataLoan.sPriorSalesPropertySellerT;
                    destDataLoan.sPriorSalesD_rep = srcDataLoan.sPriorSalesD_rep;

                    E_CalcModeT oldCalcModeT = destDataLoan.CalcModeT;
                    destDataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                    destDataLoan.sProdSpT = destDataLoan.sProdSpT;
                    destDataLoan.sProdSpStructureT = destDataLoan.sProdSpStructureT;
                    destDataLoan.sProdCondoStories_rep = destDataLoan.sProdCondoStories_rep;
                    destDataLoan.CalcModeT = oldCalcModeT;

                    destDataLoan.sProdIsSpInRuralArea = srcDataLoan.sProdIsSpInRuralArea;
                    destDataLoan.sProdIsCondotel = srcDataLoan.sProdIsCondotel;
                    destDataLoan.sProdIsNonwarrantableProj = srcDataLoan.sProdIsNonwarrantableProj;
                    destDataLoan.sProdMIOptionT = srcDataLoan.sProdMIOptionT;
                    destDataLoan.sProdIsTexas50a6Loan = srcDataLoan.sProdIsTexas50a6Loan;
                    destDataLoan.sPreviousLoanIsTexas50a6Loan = srcDataLoan.sPreviousLoanIsTexas50a6Loan;
                    destDataLoan.sIsStudentLoanCashoutRefi = srcDataLoan.sIsStudentLoanCashoutRefi;
                    destDataLoan.sProdCashoutAmt_rep = srcDataLoan.sProdCashoutAmt_rep;
                    destDataLoan.sIsRenovationLoan = srcDataLoan.sIsRenovationLoan;
                    destDataLoan.sTotalRenovationCosts_rep = srcDataLoan.sTotalRenovationCosts_rep;
                    destDataLoan.sNumFinancedProperties_rep = srcDataLoan.sNumFinancedProperties_rep;
                    destDataLoan.sProdImpound = srcDataLoan.sProdImpound;
                    destDataLoan.sProdRLckdDays_rep = srcDataLoan.sProdRLckdDays_rep;
                    destDataLoan.sProdDocT = srcDataLoan.sProdDocT;
                    destDataLoan.sFinMethT = srcDataLoan.sFinMethT;
                    destDataLoan.sProdAvailReserveMonths_rep = srcDataLoan.sProdAvailReserveMonths_rep;
                    destDataLoan.sQualIRLckd = srcDataLoan.sQualIRLckd;
                    destDataLoan.sQualIR_rep = srcDataLoan.sQualIR_rep;
                    destDataLoan.sUseQualRate = srcDataLoan.sUseQualRate;
                    destDataLoan.sQualRateCalculationT = srcDataLoan.sQualRateCalculationT;
                    destDataLoan.sQualRateCalculationFieldT1 = srcDataLoan.sQualRateCalculationFieldT1;
                    destDataLoan.sQualRateCalculationFieldT2 = srcDataLoan.sQualRateCalculationFieldT2;
                    destDataLoan.sQualRateCalculationAdjustment1 = srcDataLoan.sQualRateCalculationAdjustment1;
                    destDataLoan.sQualRateCalculationAdjustment2 = srcDataLoan.sQualRateCalculationAdjustment2;
                    destDataLoan.sIsQRateIOnly = srcDataLoan.sIsQRateIOnly;
                    destDataLoan.sQualTermCalculationType = srcDataLoan.sQualTermCalculationType;
                    destDataLoan.sQualTerm = srcDataLoan.sQualTerm;
                    destDataLoan.sIsOptionArm = srcDataLoan.sIsOptionArm;
                    destDataLoan.sIOnlyMon_rep = srcDataLoan.sIOnlyMon_rep;
                    destDataLoan.sOptionArmTeaserR_rep = srcDataLoan.sOptionArmTeaserR_rep;
                    destDataLoan.sLpIsNegAmortOtherLien = srcDataLoan.sLpIsNegAmortOtherLien;
                    destDataLoan.sOtherLFinMethT = srcDataLoan.sOtherLFinMethT;
                    destDataLoan.sProd3rdPartyUwResultT = srcDataLoan.sProd3rdPartyUwResultT;
                    destDataLoan.sProdEstimatedResidualI_rep = srcDataLoan.sProdEstimatedResidualI_rep;

                    // Results Filter
                    destDataLoan.sProdFilterRestrictResultToRegisteredProgram = srcDataLoan.sProdFilterRestrictResultToRegisteredProgram;
                    destDataLoan.sProdFilterRestrictResultToCurrentInvestor = srcDataLoan.sProdFilterRestrictResultToCurrentInvestor;
                    destDataLoan.sProdFilterDue10Yrs = srcDataLoan.sProdFilterDue10Yrs;
                    destDataLoan.sProdFilterDue15Yrs = srcDataLoan.sProdFilterDue15Yrs;
                    destDataLoan.sProdFilterDue20Yrs = srcDataLoan.sProdFilterDue20Yrs;
                    destDataLoan.sProdFilterDue25Yrs = srcDataLoan.sProdFilterDue25Yrs;
                    destDataLoan.sProdFilterDue30Yrs = srcDataLoan.sProdFilterDue30Yrs;
                    destDataLoan.sProdFilterDueOther = srcDataLoan.sProdFilterDueOther;
                    destDataLoan.sProdFilterFinMethFixed = srcDataLoan.sProdFilterFinMethFixed;
                    destDataLoan.sProdFilterFinMeth3YrsArm = srcDataLoan.sProdFilterFinMeth3YrsArm;
                    destDataLoan.sProdFilterFinMeth5YrsArm = srcDataLoan.sProdFilterFinMeth5YrsArm;
                    destDataLoan.sProdFilterFinMeth7YrsArm = srcDataLoan.sProdFilterFinMeth7YrsArm;
                    destDataLoan.sProdFilterFinMeth10YrsArm = srcDataLoan.sProdFilterFinMeth10YrsArm;
                    destDataLoan.sProdFilterFinMethOther = srcDataLoan.sProdFilterFinMethOther;
                    destDataLoan.sProdIncludeNormalProc = srcDataLoan.sProdIncludeNormalProc;
                    destDataLoan.sProdIncludeMyCommunityProc = srcDataLoan.sProdIncludeMyCommunityProc;
                    destDataLoan.sProdIncludeHomePossibleProc = srcDataLoan.sProdIncludeHomePossibleProc;
                    destDataLoan.sProdIncludeFHATotalProc = srcDataLoan.sProdIncludeFHATotalProc;
                    destDataLoan.sProdIncludeVAProc = srcDataLoan.sProdIncludeVAProc;
                    destDataLoan.sProdIncludeUSDARuralProc = srcDataLoan.sProdIncludeUSDARuralProc;
                    destDataLoan.sProdFilterPmtTPI = srcDataLoan.sProdFilterPmtTPI;
                    destDataLoan.sProdFilterPmtTIOnly = srcDataLoan.sProdFilterPmtTIOnly;

                    // PML Manual Credit
                    destDataLoan.sProdCrManual120MortLateCount_rep = srcDataLoan.sProdCrManual120MortLateCount_rep;
                    destDataLoan.sProdCrManual150MortLateCount_rep = srcDataLoan.sProdCrManual150MortLateCount_rep;
                    destDataLoan.sProdCrManual30MortLateCount_rep = srcDataLoan.sProdCrManual30MortLateCount_rep;
                    destDataLoan.sProdCrManual60MortLateCount_rep = srcDataLoan.sProdCrManual60MortLateCount_rep;
                    destDataLoan.sProdCrManual90MortLateCount_rep = srcDataLoan.sProdCrManual90MortLateCount_rep;
                    destDataLoan.sProdCrManualNonRolling30MortLateCount_rep = srcDataLoan.sProdCrManualNonRolling30MortLateCount_rep;
                    destDataLoan.sProdCrManualRolling60MortLateCount_rep = srcDataLoan.sProdCrManualRolling60MortLateCount_rep;
                    destDataLoan.sProdCrManualRolling90MortLateCount_rep = srcDataLoan.sProdCrManualRolling90MortLateCount_rep;
                    destDataLoan.sProdCrManualBk13Has = srcDataLoan.sProdCrManualBk13Has;
                    destDataLoan.sProdCrManualBk13RecentFileMon_rep = srcDataLoan.sProdCrManualBk13RecentFileMon_rep;
                    destDataLoan.sProdCrManualBk13RecentFileYr_rep = srcDataLoan.sProdCrManualBk13RecentFileYr_rep;
                    destDataLoan.sProdCrManualBk13RecentSatisfiedMon_rep = srcDataLoan.sProdCrManualBk13RecentSatisfiedMon_rep;
                    destDataLoan.sProdCrManualBk13RecentSatisfiedYr_rep = srcDataLoan.sProdCrManualBk13RecentSatisfiedYr_rep;
                    destDataLoan.sProdCrManualBk13RecentStatusT = srcDataLoan.sProdCrManualBk13RecentStatusT;
                    destDataLoan.sProdCrManualBk7Has = srcDataLoan.sProdCrManualBk7Has;
                    destDataLoan.sProdCrManualBk7RecentFileMon_rep = srcDataLoan.sProdCrManualBk7RecentFileMon_rep;
                    destDataLoan.sProdCrManualBk7RecentFileYr_rep = srcDataLoan.sProdCrManualBk7RecentFileYr_rep;
                    destDataLoan.sProdCrManualBk7RecentSatisfiedMon_rep = srcDataLoan.sProdCrManualBk7RecentSatisfiedMon_rep;
                    destDataLoan.sProdCrManualBk7RecentSatisfiedYr_rep = srcDataLoan.sProdCrManualBk7RecentSatisfiedYr_rep;
                    destDataLoan.sProdCrManualBk7RecentStatusT = srcDataLoan.sProdCrManualBk7RecentStatusT;
                    destDataLoan.sProdCrManualForeclosureHas = srcDataLoan.sProdCrManualForeclosureHas;
                    destDataLoan.sProdCrManualForeclosureRecentFileMon_rep = srcDataLoan.sProdCrManualForeclosureRecentFileMon_rep;
                    destDataLoan.sProdCrManualForeclosureRecentFileYr_rep = srcDataLoan.sProdCrManualForeclosureRecentFileYr_rep;
                    destDataLoan.sProdCrManualForeclosureRecentSatisfiedMon_rep = srcDataLoan.sProdCrManualForeclosureRecentSatisfiedMon_rep;
                    destDataLoan.sProdCrManualForeclosureRecentSatisfiedYr_rep = srcDataLoan.sProdCrManualForeclosureRecentSatisfiedYr_rep;
                    destDataLoan.sProdCrManualForeclosureRecentStatusT = srcDataLoan.sProdCrManualForeclosureRecentStatusT;

                    // Custom PML Fields - gf OPM 109263 1/16/13
                    destDataLoan.sCustomPMLField1_rep = srcDataLoan.sCustomPMLField1_rep;
                    destDataLoan.sCustomPMLField2_rep = srcDataLoan.sCustomPMLField2_rep;
                    destDataLoan.sCustomPMLField3_rep = srcDataLoan.sCustomPMLField3_rep;
                    destDataLoan.sCustomPMLField4_rep = srcDataLoan.sCustomPMLField4_rep;
                    destDataLoan.sCustomPMLField5_rep = srcDataLoan.sCustomPMLField5_rep;
                    destDataLoan.sCustomPMLField6_rep = srcDataLoan.sCustomPMLField6_rep;
                    destDataLoan.sCustomPMLField7_rep = srcDataLoan.sCustomPMLField7_rep;
                    destDataLoan.sCustomPMLField8_rep = srcDataLoan.sCustomPMLField8_rep;
                    destDataLoan.sCustomPMLField9_rep = srcDataLoan.sCustomPMLField9_rep;
                    destDataLoan.sCustomPMLField10_rep = srcDataLoan.sCustomPMLField10_rep;

                    // moar custom PML fields - JK OPM 471186 8/17/18
                    destDataLoan.sCustomPMLField11_rep = srcDataLoan.sCustomPMLField11_rep;
                    destDataLoan.sCustomPMLField12_rep = srcDataLoan.sCustomPMLField12_rep;
                    destDataLoan.sCustomPMLField13_rep = srcDataLoan.sCustomPMLField13_rep;
                    destDataLoan.sCustomPMLField14_rep = srcDataLoan.sCustomPMLField14_rep;
                    destDataLoan.sCustomPMLField15_rep = srcDataLoan.sCustomPMLField15_rep;
                    destDataLoan.sCustomPMLField16_rep = srcDataLoan.sCustomPMLField16_rep;
                    destDataLoan.sCustomPMLField17_rep = srcDataLoan.sCustomPMLField17_rep;
                    destDataLoan.sCustomPMLField18_rep = srcDataLoan.sCustomPMLField18_rep;
                    destDataLoan.sCustomPMLField19_rep = srcDataLoan.sCustomPMLField19_rep;
                    destDataLoan.sCustomPMLField20_rep = srcDataLoan.sCustomPMLField20_rep;

                    // opm 129319
                    destDataLoan.sAppTotLiqAssetExists = srcDataLoan.sAppTotLiqAssetExists;
                    destDataLoan.sAppTotLiqAsset = srcDataLoan.sAppTotLiqAsset;

                    // Submission Related
                    // OPM 172615. Because user can submit in leads again,
                    // any data that can get set in the submission process
                    // needs to be copied to the newly-created loan.
                    destDataLoan.sHistoricalPricingVersionD = srcDataLoan.sHistoricalPricingVersionD;
                    destDataLoan.sFirstLienProductId = srcDataLoan.sFirstLienProductId;
                    destDataLoan.sOptionArmMinPayOptionT = srcDataLoan.sOptionArmMinPayOptionT;
                    destDataLoan.sBrokComp1Lckd = srcDataLoan.sBrokComp1Lckd;
                    destDataLoan.sBrokComp1MldsLckd = srcDataLoan.sBrokComp1MldsLckd;
                    destDataLoan.sBrokComp1Pc = srcDataLoan.sBrokComp1Pc;
                    destDataLoan.sBrokComp1Desc = srcDataLoan.sBrokComp1Desc;
                    destDataLoan.sRAdjMarginR = srcDataLoan.sRAdjMarginR;
                    destDataLoan.sNoteIRSubmitted = srcDataLoan.sNoteIRSubmitted;
                    destDataLoan.sLOrigFPcSubmitted = srcDataLoan.sLOrigFPcSubmitted;
                    destDataLoan.sRAdjMarginRSubmitted = srcDataLoan.sRAdjMarginRSubmitted;
                    destDataLoan.sQualIRSubmitted = srcDataLoan.sQualIRSubmitted;
                    destDataLoan.sOptionArmTeaserRSubmitted = srcDataLoan.sOptionArmTeaserRSubmitted;
                    destDataLoan.sTermSubmitted = srcDataLoan.sTermSubmitted;
                    destDataLoan.sDueSubmitted = srcDataLoan.sDueSubmitted;
                    destDataLoan.sFinMethTSubmitted = srcDataLoan.sFinMethTSubmitted;
                    destDataLoan.sIsOptionArmSubmitted = srcDataLoan.sIsOptionArmSubmitted;
                    destDataLoan.sMaxDti_rep = srcDataLoan.sMaxDti_rep;
                    destDataLoan.sCreditScoreLpeQual_rep = srcDataLoan.sCreditScoreLpeQual_rep;
                    destDataLoan.sLpTemplateNmSubmitted = srcDataLoan.sLpTemplateNmSubmitted;
                    destDataLoan.sRAdjFloorR = srcDataLoan.sRAdjFloorR;
                    destDataLoan.sBrokerLockAdjustments = srcDataLoan.sBrokerLockAdjustments;
                    destDataLoan.sBrokerLockRateSheetEffectiveD_rep = srcDataLoan.sBrokerLockRateSheetEffectiveD_rep;
                    destDataLoan.sInvestorLockLockBuffer = srcDataLoan.sInvestorLockLockBuffer;
                    destDataLoan.sLpProdCode = srcDataLoan.sLpProdCode;
                    destDataLoan.sOriginatorCompensationLenderFeeOptionT = srcDataLoan.sOriginatorCompensationLenderFeeOptionT;
                    destDataLoan.sOriginatorCompensationBorrPaidBaseT = srcDataLoan.sOriginatorCompensationBorrPaidBaseT;
                    destDataLoan.sPrepmtPenaltyT = srcDataLoan.sPrepmtPenaltyT;
                    destDataLoan.sPrimAppTotNonspIPe = srcDataLoan.sPrimAppTotNonspIPe;
                    destDataLoan.sProOFinPmtPe_rep = srcDataLoan.sProOFinPmtPe_rep;
                    destDataLoan.sPmlSubmitStatusT = srcDataLoan.sPmlSubmitStatusT;
                    destDataLoan.sSubmitD = srcDataLoan.sSubmitD;
                    destDataLoan.sDocMagicPlanCodeId = srcDataLoan.sDocMagicPlanCodeId;
                    destDataLoan.sLpIsArmMarginDisplayed = srcDataLoan.sLpIsArmMarginDisplayed;
                    destDataLoan.sLenNm = srcDataLoan.sLenNm;
                    destDataLoan.sVaFfExemptTri = srcDataLoan.sVaFfExemptTri;
                    destDataLoan.sLDiscntProps = srcDataLoan.sLDiscntProps;
                    destDataLoan.s800U1FCode = srcDataLoan.s800U1FCode;
                    destDataLoan.s800U2FCode = srcDataLoan.s800U2FCode;
                    destDataLoan.s800U3FCode = srcDataLoan.s800U3FCode;
                    destDataLoan.s800U4FCode = srcDataLoan.s800U4FCode;
                    destDataLoan.s800U5FCode = srcDataLoan.s800U5FCode;
                    destDataLoan.s900U1PiaCode = srcDataLoan.s900U1PiaCode;
                    destDataLoan.sU1TcCode = srcDataLoan.sU1TcCode;
                    destDataLoan.sU2TcCode = srcDataLoan.sU2TcCode;
                    destDataLoan.sU3TcCode = srcDataLoan.sU3TcCode;
                    destDataLoan.sU4TcCode = srcDataLoan.sU4TcCode;
                    destDataLoan.sU1GovRtcCode = srcDataLoan.sU1GovRtcCode;
                    destDataLoan.sU2GovRtcCode = srcDataLoan.sU2GovRtcCode;
                    destDataLoan.sU3GovRtcCode = srcDataLoan.sU3GovRtcCode;
                    destDataLoan.sU1ScCode = srcDataLoan.sU1ScCode;
                    destDataLoan.sU2ScCode = srcDataLoan.sU2ScCode;
                    destDataLoan.sU3ScCode = srcDataLoan.sU3ScCode;
                    destDataLoan.sU4ScCode = srcDataLoan.sU4ScCode;
                    destDataLoan.sU5ScCode = srcDataLoan.sU5ScCode;
                    destDataLoan.sBrokComp1 = srcDataLoan.sBrokComp1;
                    destDataLoan.sBrokComp1_rep = srcDataLoan.sBrokComp1_rep;
                    destDataLoan.sBrokComp2Desc = srcDataLoan.sBrokComp2Desc;
                    destDataLoan.sBrokComp2 = srcDataLoan.sBrokComp2;
                    destDataLoan.sBrokComp2_rep = srcDataLoan.sBrokComp2_rep;
                    destDataLoan.sU1FntcDesc = srcDataLoan.sU1FntcDesc;
                    destDataLoan.sU1Fntc = srcDataLoan.sU1Fntc;
                    destDataLoan.sU1Fntc_rep = srcDataLoan.sU1Fntc_rep;
                    destDataLoan.sGfeProvByBrok = srcDataLoan.sGfeProvByBrok;
                    destDataLoan.sDisabilityIns = srcDataLoan.sDisabilityIns;
                    destDataLoan.sDisabilityIns_rep = srcDataLoan.sDisabilityIns_rep;
                    destDataLoan.sTitleInsFGfeSection = srcDataLoan.sTitleInsFGfeSection;
                    destDataLoan.sMortgageLoanT = srcDataLoan.sMortgageLoanT;
                    destDataLoan.sJumboT = srcDataLoan.sJumboT;
                    destDataLoan.sHelocDraw_rep = srcDataLoan.sHelocDraw_rep;
                    destDataLoan.sHelocQualPmtBaseT = srcDataLoan.sHelocQualPmtBaseT;
                    destDataLoan.sHelocQualPmtPcBaseT = srcDataLoan.sHelocQualPmtPcBaseT;
                    destDataLoan.sHelocQualPmtMb_rep = srcDataLoan.sHelocQualPmtMb_rep;
                    destDataLoan.sHelocQualPmtFormulaT = srcDataLoan.sHelocQualPmtFormulaT;
                    destDataLoan.sHelocQualPmtFormulaRateT = srcDataLoan.sHelocQualPmtFormulaRateT;
                    destDataLoan.sHelocQualPmtAmortTermT = srcDataLoan.sHelocQualPmtAmortTermT;
                    destDataLoan.sHelocPmtBaseT = srcDataLoan.sHelocPmtBaseT;
                    destDataLoan.sHelocPmtPcBaseT = srcDataLoan.sHelocPmtPcBaseT;
                    destDataLoan.sHelocPmtMb_rep = srcDataLoan.sHelocPmtMb_rep;
                    destDataLoan.sHelocPmtFormulaT = srcDataLoan.sHelocPmtFormulaT;
                    destDataLoan.sHelocPmtFormulaRateT = srcDataLoan.sHelocPmtFormulaRateT;
                    destDataLoan.sHelocPmtAmortTermT = srcDataLoan.sHelocPmtAmortTermT;
                    destDataLoan.sTotalScoreFhaProductT = srcDataLoan.sTotalScoreFhaProductT;
                    destDataLoan.sIsConvertibleMortgage = srcDataLoan.sIsConvertibleMortgage;
                    destDataLoan.sRAdjFloorBaseT = srcDataLoan.sRAdjFloorBaseT;
                    destDataLoan.sRLifeCapR = srcDataLoan.sRLifeCapR;
                    destDataLoan.sRAdjWorstIndex = srcDataLoan.sRAdjWorstIndex;
                    destDataLoan.sRAdjIndexR = srcDataLoan.sRAdjIndexR;
                    destDataLoan.sRAdjRoundT = srcDataLoan.sRAdjRoundT;
                    destDataLoan.sRAdjRoundToR = srcDataLoan.sRAdjRoundToR;
                    destDataLoan.sBuydwnR1 = srcDataLoan.sBuydwnR1;
                    destDataLoan.sBuydwnR2 = srcDataLoan.sBuydwnR2;
                    destDataLoan.sBuydwnR3 = srcDataLoan.sBuydwnR3;
                    destDataLoan.sBuydwnR4 = srcDataLoan.sBuydwnR4;
                    destDataLoan.sBuydwnR5 = srcDataLoan.sBuydwnR5;
                    destDataLoan.sBuydwnMon1 = srcDataLoan.sBuydwnMon1;
                    destDataLoan.sBuydwnMon2 = srcDataLoan.sBuydwnMon2;
                    destDataLoan.sBuydwnMon3 = srcDataLoan.sBuydwnMon3;
                    destDataLoan.sBuydwnMon4 = srcDataLoan.sBuydwnMon4;
                    destDataLoan.sBuydwnMon5 = srcDataLoan.sBuydwnMon5;
                    destDataLoan.sGradPmtYrs = srcDataLoan.sGradPmtYrs;
                    destDataLoan.sGradPmtR = srcDataLoan.sGradPmtR;
                    destDataLoan.sDtiUsingMaxBalPc = srcDataLoan.sDtiUsingMaxBalPc;
                    destDataLoan.sVarRNotes = srcDataLoan.sVarRNotes;
                    destDataLoan.sAprIncludesReqDeposit = srcDataLoan.sAprIncludesReqDeposit;
                    destDataLoan.sHasDemandFeature = srcDataLoan.sHasDemandFeature;
                    destDataLoan.sFilingF = srcDataLoan.sFilingF;
                    destDataLoan.sArmIndexAffectInitIRBit = srcDataLoan.sArmIndexAffectInitIRBit;
                    destDataLoan.sArmIndexBasedOnVstr = srcDataLoan.sArmIndexBasedOnVstr;
                    destDataLoan.sArmIndexCanBeFoundVstr = srcDataLoan.sArmIndexCanBeFoundVstr;
                    destDataLoan.sArmIndexNotifyAtLeastDaysVstr = srcDataLoan.sArmIndexNotifyAtLeastDaysVstr;
                    destDataLoan.sArmIndexNotifyNotBeforeDaysVstr = srcDataLoan.sArmIndexNotifyNotBeforeDaysVstr;
                    destDataLoan.sHasVarRFeature = srcDataLoan.sHasVarRFeature;
                    destDataLoan.sPrepmtRefundT = srcDataLoan.sPrepmtRefundT;
                    destDataLoan.sAssumeLT = srcDataLoan.sAssumeLT;
                    destDataLoan.sArmIndexNameVstr = srcDataLoan.sArmIndexNameVstr;
                    destDataLoan.sArmIndexEffectiveD_rep = srcDataLoan.sArmIndexEffectiveD_rep;

                    // OPM 184964. When this setting is on, the lead could have values
                    // from loan program application, so we need to copy to template-created loan.
                    // If setting is off, leave the template-created loan values alone: template wins.
                    if (brokerDb.PopulateLateFeesFromPml_139391)
                    {
                        destDataLoan.sLateDays = srcDataLoan.sLateDays;
                        destDataLoan.sLateChargePc = srcDataLoan.sLateChargePc;
                        destDataLoan.sLateChargeBaseDesc = srcDataLoan.sLateChargeBaseDesc;
                    }

                    destDataLoan.sLpInvestorNm = srcDataLoan.sLpInvestorNm;
                    destDataLoan.sLpProductType = srcDataLoan.sLpProductType;
                    destDataLoan.sLpProductCode = srcDataLoan.sLpProductCode;
                    destDataLoan.sPpmtPenaltyMon = srcDataLoan.sPpmtPenaltyMon;
                    destDataLoan.sSoftPrepmtPeriodMonths_rep = srcDataLoan.sSoftPrepmtPeriodMonths_rep;
                    destDataLoan.sPrepmtPeriodMonths_rep = srcDataLoan.sPrepmtPeriodMonths_rep;
                    destDataLoan.sRLckdDays = srcDataLoan.sRLckdDays;
                    destDataLoan.sRLckdDaysFromInvestor = srcDataLoan.sRLckdDaysFromInvestor;

                    // OPM 173379 Only set CCTemplate Id if there is one.
                    if (srcDataLoan.sCcTemplateId != Guid.Empty)
                    {
                        destDataLoan.sCcTemplateId = srcDataLoan.sCcTemplateId;
                    }

                    destDataLoan.sFeeServiceApplicationHistoryXmlContent = srcDataLoan.sFeeServiceApplicationHistoryXmlContent;
                    destDataLoan.sPmlCertXmlContent = srcDataLoan.sPmlCertXmlContent;

                    // 8/7/2014 gf - opm 189088, originator compensation can also be assigned at submission.
                    destDataLoan.CopyCompensationPlanFromLoan(srcDataLoan);

                    // 9/12/2013 gf - The regions below do not necessarily
                    // contain all of the fields assigned to from that page.
                    // If the field was already assigned elsewhere it was
                    // left out.
                    // Expanded Lead Editor
                    // LoanInfo (Expanded)
                    destDataLoan.sCcTemplateNm = srcDataLoan.sCcTemplateNm;
                    destDataLoan.sFinMethDesc = srcDataLoan.sFinMethDesc;
                    destDataLoan.sProRealETxMb_rep = srcDataLoan.sProRealETxMb_rep;
                    destDataLoan.sProRealETxT = srcDataLoan.sProRealETxT;
                    destDataLoan.sProRealETxR_rep = srcDataLoan.sProRealETxR_rep;
                    destDataLoan.sProHoAssocDues_rep = srcDataLoan.sProHoAssocDues_rep;
                    destDataLoan.sProOHExp_rep = srcDataLoan.sProOHExp_rep;
                    destDataLoan.sProOHExpLckd = srcDataLoan.sProOHExpLckd;
                    destDataLoan.sProHazInsT = srcDataLoan.sProHazInsT;
                    destDataLoan.sProHazInsR_rep = srcDataLoan.sProHazInsR_rep;
                    destDataLoan.sProHazInsMb_rep = srcDataLoan.sProHazInsMb_rep;
                    destDataLoan.sProMIns_rep = srcDataLoan.sProMIns_rep;
                    destDataLoan.sProMInsLckd = srcDataLoan.sProMInsLckd;
                    destDataLoan.sGseSpT = srcDataLoan.sGseSpT;
                    destDataLoan.sHomeIsMhAdvantageTri = srcDataLoan.sHomeIsMhAdvantageTri;
                    destDataLoan.sOriginalAppraisedValue_rep = srcDataLoan.sOriginalAppraisedValue_rep;
                    destDataLoan.sHighPricedMortgageT = srcDataLoan.sHighPricedMortgageT;
                    destDataLoan.sHighPricedMortgageTLckd = srcDataLoan.sHighPricedMortgageTLckd;
                    destDataLoan.sIsEmployeeLoan = srcDataLoan.sIsEmployeeLoan;
                    destDataLoan.sConstructionPeriodMon_rep = srcDataLoan.sConstructionPeriodMon_rep;
                    destDataLoan.sConstructionPeriodIR_rep = srcDataLoan.sConstructionPeriodIR_rep;
                    destDataLoan.sLandCost_rep = srcDataLoan.sLandCost_rep;

                    if (!LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(srcDataLoan.sLoanVersionT, LoanVersionT.V31_ConstructionLoans_ExposeNewDataPoints))
                    {
                        destDataLoan.sConstructionImprovementAmt_rep = srcDataLoan.sConstructionImprovementAmt_rep;
                    }

                    // dd 3/10/2014 - OPM 172407 - Lead Expand Editor does not have UI to indicate
                    // if the lead is line of credit. Therefore must honor from template.
                    // destDataLoan.sIsLineOfCredit = srcDataLoan.sIsLineOfCredit;
                    destDataLoan.sCreditLineAmt_rep = srcDataLoan.sCreditLineAmt_rep;

                    // UpfrontMIP (Expanded)
                    destDataLoan.sFfUfmipR_rep = srcDataLoan.sFfUfmipR_rep;
                    destDataLoan.sFfUfmip1003_rep = srcDataLoan.sFfUfmip1003_rep;
                    destDataLoan.sFfUfmip1003Lckd = srcDataLoan.sFfUfmip1003Lckd;
                    destDataLoan.sMipFrequency = srcDataLoan.sMipFrequency;
                    destDataLoan.sFfUfMipIsBeingFinanced = srcDataLoan.sFfUfMipIsBeingFinanced;
                    destDataLoan.sMipPiaMon_rep = srcDataLoan.sMipPiaMon_rep;
                    destDataLoan.sUfCashPdLckd = srcDataLoan.sUfCashPdLckd;
                    destDataLoan.sUfCashPd_rep = srcDataLoan.sUfCashPd_rep;
                    destDataLoan.sProMInsR_rep = srcDataLoan.sProMInsR_rep;
                    destDataLoan.sProMInsMb_rep = srcDataLoan.sProMInsMb_rep;
                    destDataLoan.sProMInsT = srcDataLoan.sProMInsT;
                    destDataLoan.sIncludeUfmipInLtvCalc = srcDataLoan.sIncludeUfmipInLtvCalc;
                    destDataLoan.sMiLenderPaidCoverage_rep = srcDataLoan.sMiLenderPaidCoverage_rep;
                    destDataLoan.sMiCommitmentRequestedD_rep = srcDataLoan.sMiCommitmentRequestedD_rep;
                    destDataLoan.sMiCommitmentReceivedD_rep = srcDataLoan.sMiCommitmentReceivedD_rep;
                    destDataLoan.sMiCommitmentExpirationD_rep = srcDataLoan.sMiCommitmentExpirationD_rep;
                    destDataLoan.sMiCertId = srcDataLoan.sMiCertId;
                    destDataLoan.sProMInsMon_rep = srcDataLoan.sProMInsMon_rep;
                    destDataLoan.sProMIns2Mon_rep = srcDataLoan.sProMIns2Mon_rep;
                    destDataLoan.sProMInsR2_rep = srcDataLoan.sProMInsR2_rep;
                    destDataLoan.sProMInsCancelLtv_rep = srcDataLoan.sProMInsCancelLtv_rep;
                    destDataLoan.sProMInsCancelMinPmts_rep = srcDataLoan.sProMInsCancelMinPmts_rep;
                    destDataLoan.sProMInsMidptCancel = srcDataLoan.sProMInsMidptCancel;
                    destDataLoan.sMiInsuranceT = srcDataLoan.sMiInsuranceT;
                    destDataLoan.sMiCompanyNmT = srcDataLoan.sMiCompanyNmT;
                    destDataLoan.sLenderUfmipR = srcDataLoan.sLenderUfmipR;
                    destDataLoan.sLenderUfmip = srcDataLoan.sLenderUfmip;
                    destDataLoan.sLenderUfmipLckd = srcDataLoan.sLenderUfmipLckd;

                    // Other Financing Info (Expanded)
                    destDataLoan.sSubFin_rep = srcDataLoan.sSubFin_rep;
                    destDataLoan.sConcurSubFin_rep = srcDataLoan.sConcurSubFin_rep;
                    destDataLoan.sSubFinIR_rep = srcDataLoan.sSubFinIR_rep;
                    destDataLoan.sSubFinTerm_rep = srcDataLoan.sSubFinTerm_rep;
                    destDataLoan.sSubFinMb_rep = srcDataLoan.sSubFinMb_rep;
                    destDataLoan.sSubFinPmt_rep = srcDataLoan.sSubFinPmt_rep;
                    destDataLoan.sSubFinPmtLckd = srcDataLoan.sSubFinPmtLckd;
                    destDataLoan.sIsIOnlyForSubFin = srcDataLoan.sIsIOnlyForSubFin;
                    destDataLoan.sIsOFinNew = srcDataLoan.sIsOFinNew;
                    destDataLoan.sIsOFinCreditLineInDrawPeriod = srcDataLoan.sIsOFinCreditLineInDrawPeriod;
                    destDataLoan.sLayeredTotalForgivableBalance_rep = srcDataLoan.sLayeredTotalForgivableBalance_rep;
                    destDataLoan.s1stMtgOrigLAmt_rep = srcDataLoan.s1stMtgOrigLAmt_rep;
                    destDataLoan.sRemain1stMBal_rep = srcDataLoan.sRemain1stMBal_rep;
                    destDataLoan.sRemain1stMPmt_rep = srcDataLoan.sRemain1stMPmt_rep;

                    // 1003 Pg 1(Expanded)
                    destDataLoan.sAgencyCaseNum = srcDataLoan.sAgencyCaseNum;
                    destDataLoan.sDwnPmtSrc = srcDataLoan.sDwnPmtSrc;
                    destDataLoan.sDwnPmtSrcExplain = srcDataLoan.sDwnPmtSrcExplain;
                    destDataLoan.sEstateHeldT = srcDataLoan.sEstateHeldT;
                    destDataLoan.sRefPurpose = srcDataLoan.sRefPurpose;
                    destDataLoan.sLTODesc = srcDataLoan.sLTODesc;
                    destDataLoan.sLeaseHoldExpireD_rep = srcDataLoan.sLeaseHoldExpireD_rep;
                    destDataLoan.sLenderCaseNum = srcDataLoan.sLenderCaseNum;
                    destDataLoan.sLenderCaseNumLckd = srcDataLoan.sLenderCaseNumLckd;
                    destDataLoan.sLotAcqYr = srcDataLoan.sLotAcqYr;
                    destDataLoan.sLotImprovC_rep = srcDataLoan.sLotImprovC_rep;
                    destDataLoan.sLotLien_rep = srcDataLoan.sLotLien_rep;
                    destDataLoan.sLotOrigC_rep = srcDataLoan.sLotOrigC_rep;
                    destDataLoan.sLotVal_rep = srcDataLoan.sLotVal_rep;
                    destDataLoan.sMultiApps = srcDataLoan.sMultiApps;
                    destDataLoan.sOLPurposeDesc = srcDataLoan.sOLPurposeDesc;
                    destDataLoan.sSpAcqYr = srcDataLoan.sSpAcqYr;
                    destDataLoan.sSpCounty = srcDataLoan.sSpCounty;
                    destDataLoan.sSpImprovC_rep = srcDataLoan.sSpImprovC_rep;
                    destDataLoan.sSpImprovDesc = srcDataLoan.sSpImprovDesc;
                    destDataLoan.sSpImprovTimeFrameT = srcDataLoan.sSpImprovTimeFrameT;
                    destDataLoan.sSpLegalDesc = srcDataLoan.sSpLegalDesc;
                    destDataLoan.sSpLien_rep = srcDataLoan.sSpLien_rep;
                    destDataLoan.sSpOrigC_rep = srcDataLoan.sSpOrigC_rep;
                    destDataLoan.sUnitsNum_rep = srcDataLoan.sUnitsNum_rep;
                    destDataLoan.sYrBuilt = srcDataLoan.sYrBuilt;
                    destDataLoan.sFinMethodPrintAsOther = srcDataLoan.sFinMethodPrintAsOther;
                    destDataLoan.sFinMethPrintAsOtherDesc = srcDataLoan.sFinMethPrintAsOtherDesc;

                    // 1003 Pg 2 (Expanded)
                    destDataLoan.sProOHExpDesc = srcDataLoan.sProOHExpDesc;

                    // 1003 Pg 3 (Expanded)
                    destDataLoan.sAltCost_rep = srcDataLoan.sAltCost_rep;
                    destDataLoan.sLDiscnt1003Lckd = srcDataLoan.sLDiscnt1003Lckd;
                    destDataLoan.sLDiscnt1003_rep = srcDataLoan.sLDiscnt1003_rep;
                    destDataLoan.sOCredit1Amt_rep = srcDataLoan.sOCredit1Amt_rep;
                    destDataLoan.sOCredit1Desc = srcDataLoan.sOCredit1Desc;
                    destDataLoan.sOCredit1Lckd = srcDataLoan.sOCredit1Lckd;

                    // logic for sOCredit 2-4 moved to after sDisclosureRegulationT determination.
                    destDataLoan.sRefPdOffAmt1003Lckd = srcDataLoan.sRefPdOffAmt1003Lckd;
                    destDataLoan.sRefPdOffAmt1003_rep = srcDataLoan.sRefPdOffAmt1003_rep;
                    destDataLoan.sTotCcPbsLocked = srcDataLoan.sTotCcPbsLocked;
                    destDataLoan.sTotCcPbs_rep = srcDataLoan.sTotCcPbs_rep;
                    destDataLoan.sTotEstCc1003Lckd = srcDataLoan.sTotEstCc1003Lckd;
                    destDataLoan.sTotEstCcNoDiscnt1003_rep = srcDataLoan.sTotEstCcNoDiscnt1003_rep;
                    destDataLoan.sTotEstPp1003Lckd = srcDataLoan.sTotEstPp1003Lckd;
                    destDataLoan.sTotEstPp1003_rep = srcDataLoan.sTotEstPp1003_rep;
                    destDataLoan.sTransNetCashLckd = srcDataLoan.sTransNetCashLckd;
                    destDataLoan.sTransNetCash_rep = srcDataLoan.sTransNetCash_rep;
                    destDataLoan.sONewFinCc_rep = srcDataLoan.sONewFinCc_rep;

                    // GFE (Expanded)
                    destDataLoan.sHazInsRsrvMonLckd = srcDataLoan.sHazInsRsrvMonLckd; // start opm 180983
                    destDataLoan.sMInsRsrvMonLckd = srcDataLoan.sMInsRsrvMonLckd;
                    destDataLoan.sRealETxRsrvMonLckd = srcDataLoan.sRealETxRsrvMonLckd;
                    destDataLoan.sSchoolTxRsrvMonLckd = srcDataLoan.sSchoolTxRsrvMonLckd;
                    destDataLoan.sFloodInsRsrvMonLckd = srcDataLoan.sFloodInsRsrvMonLckd;
                    destDataLoan.s1006RsrvMonLckd = srcDataLoan.s1006RsrvMonLckd;
                    destDataLoan.s1007RsrvMonLckd = srcDataLoan.s1007RsrvMonLckd;
                    destDataLoan.sU3RsrvMonLckd = srcDataLoan.sU3RsrvMonLckd;
                    destDataLoan.sU4RsrvMonLckd = srcDataLoan.sU4RsrvMonLckd;  // end opm 180983

                    destDataLoan.sEstCloseD_rep = srcDataLoan.sEstCloseD_rep;
                    destDataLoan.sEstCloseDLckd = srcDataLoan.sEstCloseDLckd;
                    destDataLoan.sSchedDueD1_rep = srcDataLoan.sSchedDueD1_rep;
                    destDataLoan.sSchedDueD1Lckd = srcDataLoan.sSchedDueD1Lckd;
                    destDataLoan.sDaysInYr_rep = srcDataLoan.sDaysInYr_rep;
                    destDataLoan.sRAdj1stCapR_rep = srcDataLoan.sRAdj1stCapR_rep;
                    destDataLoan.sRAdjCapR_rep = srcDataLoan.sRAdjCapR_rep;
                    destDataLoan.sRAdjLifeCapR_rep = srcDataLoan.sRAdjLifeCapR_rep;
                    destDataLoan.sRAdjCapMon_rep = srcDataLoan.sRAdjCapMon_rep;
                    destDataLoan.sRAdj1stCapMon_rep = srcDataLoan.sRAdj1stCapMon_rep;
                    destDataLoan.sPmtAdjCapR_rep = srcDataLoan.sPmtAdjCapR_rep;
                    destDataLoan.sPmtAdjMaxBalPc_rep = srcDataLoan.sPmtAdjMaxBalPc_rep;
                    destDataLoan.sPmtAdjRecastStop_rep = srcDataLoan.sPmtAdjRecastStop_rep;
                    destDataLoan.sPmtAdjRecastPeriodMon_rep = srcDataLoan.sPmtAdjRecastPeriodMon_rep;
                    destDataLoan.sPmtAdjCapMon_rep = srcDataLoan.sPmtAdjCapMon_rep;
                    destDataLoan.sIPiaDyLckd = srcDataLoan.sIPiaDyLckd;
                    destDataLoan.sConsummationD_rep = srcDataLoan.sConsummationD_rep;
                    destDataLoan.sConsummationDLckd = srcDataLoan.sConsummationDLckd;
                    destDataLoan.sGfeNoteIRAvailTillD = srcDataLoan.sGfeNoteIRAvailTillD;
                    destDataLoan.sGfeEstScAvailTillD = srcDataLoan.sGfeEstScAvailTillD;
                    destDataLoan.sGfeRateLockPeriod_rep = srcDataLoan.sGfeRateLockPeriod_rep;
                    destDataLoan.sGfeLockPeriodBeforeSettlement_rep = srcDataLoan.sGfeLockPeriodBeforeSettlement_rep;
                    destDataLoan.sGfeHavePpmtPenalty = srcDataLoan.sGfeHavePpmtPenalty;
                    destDataLoan.sGfeMaxPpmtPenaltyAmt_rep = srcDataLoan.sGfeMaxPpmtPenaltyAmt_rep;
                    destDataLoan.sMldsHasImpound = srcDataLoan.sMldsHasImpound;
                    destDataLoan.sGfeUsePaidToFromOfficialContact = srcDataLoan.sGfeUsePaidToFromOfficialContact;
                    destDataLoan.sGfeNoteIRAvailTillDTimeZoneT = srcDataLoan.sGfeNoteIRAvailTillDTimeZoneT;
                    destDataLoan.sGfeEstScAvailTillDTimeZoneT = srcDataLoan.sGfeEstScAvailTillDTimeZoneT;
                    destDataLoan.sIsPrintTimeForGfeNoteIRAvailTillD = srcDataLoan.sIsPrintTimeForGfeNoteIRAvailTillD;
                    destDataLoan.sIsPrintTimeForsGfeEstScAvailTillD = srcDataLoan.sIsPrintTimeForsGfeEstScAvailTillD;
                    destDataLoan.s800U5F_rep = srcDataLoan.s800U5F_rep;
                    destDataLoan.s800U5FDesc = srcDataLoan.s800U5FDesc;
                    destDataLoan.s800U4F_rep = srcDataLoan.s800U4F_rep;
                    destDataLoan.s800U4FDesc = srcDataLoan.s800U4FDesc;
                    destDataLoan.s800U3F_rep = srcDataLoan.s800U3F_rep;
                    destDataLoan.s800U3FDesc = srcDataLoan.s800U3FDesc;
                    destDataLoan.s800U2F_rep = srcDataLoan.s800U2F_rep;
                    destDataLoan.s800U2FDesc = srcDataLoan.s800U2FDesc;
                    destDataLoan.s800U1F_rep = srcDataLoan.s800U1F_rep;
                    destDataLoan.s800U1FDesc = srcDataLoan.s800U1FDesc;
                    destDataLoan.sWireF_rep = srcDataLoan.sWireF_rep;
                    destDataLoan.sUwF_rep = srcDataLoan.sUwF_rep;
                    destDataLoan.sProcF_rep = srcDataLoan.sProcF_rep;
                    destDataLoan.sProcFPaid = srcDataLoan.sProcFPaid;
                    destDataLoan.sTxServF_rep = srcDataLoan.sTxServF_rep;
                    destDataLoan.sFloodCertificationDeterminationT = srcDataLoan.sFloodCertificationDeterminationT;
                    destDataLoan.sMBrokFMb_rep = srcDataLoan.sMBrokFMb_rep;
                    destDataLoan.sMBrokFPc_rep = srcDataLoan.sMBrokFPc_rep;
                    destDataLoan.sMBrokFBaseT = srcDataLoan.sMBrokFBaseT;
                    destDataLoan.sInspectF_rep = srcDataLoan.sInspectF_rep;
                    destDataLoan.sCrF_rep = srcDataLoan.sCrF_rep;
                    destDataLoan.sCrFPaid = srcDataLoan.sCrFPaid;
                    destDataLoan.sApprF_rep = srcDataLoan.sApprF_rep;
                    destDataLoan.sApprFPaid = srcDataLoan.sApprFPaid;
                    destDataLoan.sFloodCertificationF_rep = srcDataLoan.sFloodCertificationF_rep;
                    destDataLoan.sLDiscntFMb_rep = srcDataLoan.sLDiscntFMb_rep;
                    destDataLoan.sLDiscntPc_rep = srcDataLoan.sLDiscntPc_rep;
                    destDataLoan.sLDiscntBaseT = srcDataLoan.sLDiscntBaseT;

                    // opm 464662
                    destDataLoan.sLoanTransactionInvolvesSeller = srcDataLoan.sLoanTransactionInvolvesSeller;
                    destDataLoan.sLoanTransactionInvolvesSellerLckd = srcDataLoan.sLoanTransactionInvolvesSellerLckd;
                    destDataLoan.sIsRequireAlternateCashToCloseForNonSellerTransactions = srcDataLoan.sIsRequireAlternateCashToCloseForNonSellerTransactions;

                    if (brokerDb.IsGFEandCoCVersioningEnabled)
                    {
                        if (srcDataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
                        {
                            destDataLoan.sLastDisclosedGFEArchiveD_rep = srcDataLoan.sLastDisclosedGFEArchiveD_rep;
                        }
                    }

                    destDataLoan.sLOrigFMb_rep = srcDataLoan.sLOrigFMb_rep;
                    destDataLoan.sLOrigFPc_rep = srcDataLoan.sLOrigFPc_rep;
                    destDataLoan.s900U1Pia_rep = srcDataLoan.s900U1Pia_rep;
                    destDataLoan.s900U1PiaDesc = srcDataLoan.s900U1PiaDesc;
                    destDataLoan.s904PiaDesc = srcDataLoan.s904PiaDesc;
                    destDataLoan.s904Pia_rep = srcDataLoan.s904Pia_rep;
                    destDataLoan.sHazInsPiaMon_rep = srcDataLoan.sHazInsPiaMon_rep;
                    destDataLoan.sIPiaDy_rep = srcDataLoan.sIPiaDy_rep;
                    destDataLoan.sIPerDayLckd = srcDataLoan.sIPerDayLckd;
                    destDataLoan.sIPerDay_rep = srcDataLoan.sIPerDay_rep;
                    destDataLoan.sAggregateAdjRsrv_rep = srcDataLoan.sAggregateAdjRsrv_rep;
                    destDataLoan.sAggregateAdjRsrvLckd = srcDataLoan.sAggregateAdjRsrvLckd;
                    destDataLoan.s1007ProHExp_rep = srcDataLoan.s1007ProHExp_rep;
                    destDataLoan.s1007RsrvMon_rep = srcDataLoan.s1007RsrvMon_rep;
                    destDataLoan.s1007ProHExpDesc = srcDataLoan.s1007ProHExpDesc;
                    destDataLoan.s1006ProHExp_rep = srcDataLoan.s1006ProHExp_rep;
                    destDataLoan.s1006RsrvMon_rep = srcDataLoan.s1006RsrvMon_rep;
                    destDataLoan.s1006ProHExpDesc = srcDataLoan.s1006ProHExpDesc;
                    destDataLoan.sProU3Rsrv_rep = srcDataLoan.sProU3Rsrv_rep;
                    destDataLoan.sU3RsrvMon_rep = srcDataLoan.sU3RsrvMon_rep;
                    destDataLoan.sU3RsrvDesc = srcDataLoan.sU3RsrvDesc;
                    destDataLoan.sProU4Rsrv_rep = srcDataLoan.sProU4Rsrv_rep;
                    destDataLoan.sU4RsrvMon_rep = srcDataLoan.sU4RsrvMon_rep;
                    destDataLoan.sU4RsrvDesc = srcDataLoan.sU4RsrvDesc;
                    destDataLoan.sProFloodIns_rep = srcDataLoan.sProFloodIns_rep;
                    destDataLoan.sFloodInsRsrvMon_rep = srcDataLoan.sFloodInsRsrvMon_rep;
                    destDataLoan.sRealETxRsrvMon_rep = srcDataLoan.sRealETxRsrvMon_rep;
                    destDataLoan.sProSchoolTx_rep = srcDataLoan.sProSchoolTx_rep;
                    destDataLoan.sSchoolTxRsrvMon_rep = srcDataLoan.sSchoolTxRsrvMon_rep;
                    destDataLoan.sMInsRsrvMon_rep = srcDataLoan.sMInsRsrvMon_rep;
                    destDataLoan.sHazInsRsrvMon_rep = srcDataLoan.sHazInsRsrvMon_rep;
                    destDataLoan.sU4Tc_rep = srcDataLoan.sU4Tc_rep;
                    destDataLoan.sU4TcDesc = srcDataLoan.sU4TcDesc;
                    destDataLoan.sU3Tc_rep = srcDataLoan.sU3Tc_rep;
                    destDataLoan.sU3TcDesc = srcDataLoan.sU3TcDesc;
                    destDataLoan.sU2Tc_rep = srcDataLoan.sU2Tc_rep;
                    destDataLoan.sU2TcDesc = srcDataLoan.sU2TcDesc;
                    destDataLoan.sU1Tc_rep = srcDataLoan.sU1Tc_rep;
                    destDataLoan.sU1TcDesc = srcDataLoan.sU1TcDesc;
                    destDataLoan.sOwnerTitleInsF_rep = srcDataLoan.sOwnerTitleInsF_rep;
                    destDataLoan.sTitleInsF_rep = srcDataLoan.sTitleInsF_rep;
                    destDataLoan.sTitleInsFTable = srcDataLoan.sTitleInsFTable;
                    destDataLoan.sAttorneyF_rep = srcDataLoan.sAttorneyF_rep;
                    destDataLoan.sNotaryF_rep = srcDataLoan.sNotaryF_rep;
                    destDataLoan.sDocPrepF_rep = srcDataLoan.sDocPrepF_rep;
                    destDataLoan.sEscrowF_rep = srcDataLoan.sEscrowF_rep;
                    destDataLoan.sEscrowFTable = srcDataLoan.sEscrowFTable;
                    destDataLoan.sU3GovRtcMb_rep = srcDataLoan.sU3GovRtcMb_rep;
                    destDataLoan.sU3GovRtcBaseT = srcDataLoan.sU3GovRtcBaseT;
                    destDataLoan.sU3GovRtcPc_rep = srcDataLoan.sU3GovRtcPc_rep;
                    destDataLoan.sU3GovRtcDesc = srcDataLoan.sU3GovRtcDesc;
                    destDataLoan.sU2GovRtcMb_rep = srcDataLoan.sU2GovRtcMb_rep;
                    destDataLoan.sU2GovRtcBaseT = srcDataLoan.sU2GovRtcBaseT;
                    destDataLoan.sU2GovRtcPc_rep = srcDataLoan.sU2GovRtcPc_rep;
                    destDataLoan.sU2GovRtcDesc = srcDataLoan.sU2GovRtcDesc;
                    destDataLoan.sU1GovRtcMb_rep = srcDataLoan.sU1GovRtcMb_rep;
                    destDataLoan.sU1GovRtcBaseT = srcDataLoan.sU1GovRtcBaseT;
                    destDataLoan.sU1GovRtcPc_rep = srcDataLoan.sU1GovRtcPc_rep;
                    destDataLoan.sU1GovRtcDesc = srcDataLoan.sU1GovRtcDesc;
                    destDataLoan.sStateRtcDesc = srcDataLoan.sStateRtcDesc;
                    destDataLoan.sStateRtcMb_rep = srcDataLoan.sStateRtcMb_rep;
                    destDataLoan.sStateRtcBaseT = srcDataLoan.sStateRtcBaseT;
                    destDataLoan.sStateRtcPc_rep = srcDataLoan.sStateRtcPc_rep;
                    destDataLoan.sCountyRtcDesc = srcDataLoan.sCountyRtcDesc;
                    destDataLoan.sCountyRtcMb_rep = srcDataLoan.sCountyRtcMb_rep;
                    destDataLoan.sCountyRtcBaseT = srcDataLoan.sCountyRtcBaseT;
                    destDataLoan.sCountyRtcPc_rep = srcDataLoan.sCountyRtcPc_rep;
                    destDataLoan.sRecFDesc = srcDataLoan.sRecFDesc;
                    destDataLoan.sRecFMb_rep = srcDataLoan.sRecFMb_rep;
                    destDataLoan.sRecBaseT = srcDataLoan.sRecBaseT;
                    destDataLoan.sRecFPc_rep = srcDataLoan.sRecFPc_rep;
                    destDataLoan.sRecFLckd = srcDataLoan.sRecFLckd;
                    destDataLoan.sRecDeed_rep = srcDataLoan.sRecDeed_rep;
                    destDataLoan.sRecMortgage_rep = srcDataLoan.sRecMortgage_rep;
                    destDataLoan.sRecRelease_rep = srcDataLoan.sRecRelease_rep;
                    destDataLoan.sU5Sc_rep = srcDataLoan.sU5Sc_rep;
                    destDataLoan.sU5ScDesc = srcDataLoan.sU5ScDesc;
                    destDataLoan.sU4Sc_rep = srcDataLoan.sU4Sc_rep;
                    destDataLoan.sU4ScDesc = srcDataLoan.sU4ScDesc;
                    destDataLoan.sU3Sc_rep = srcDataLoan.sU3Sc_rep;
                    destDataLoan.sU3ScDesc = srcDataLoan.sU3ScDesc;
                    destDataLoan.sU2Sc_rep = srcDataLoan.sU2Sc_rep;
                    destDataLoan.sU2ScDesc = srcDataLoan.sU2ScDesc;
                    destDataLoan.sU1Sc_rep = srcDataLoan.sU1Sc_rep;
                    destDataLoan.sU1ScDesc = srcDataLoan.sU1ScDesc;
                    destDataLoan.sPestInspectF_rep = srcDataLoan.sPestInspectF_rep;
                    destDataLoan.sGfeCreditLenderPaidItemT = srcDataLoan.sGfeCreditLenderPaidItemT;
                    destDataLoan.sLOrigFProps = srcDataLoan.sLOrigFProps;
                    destDataLoan.sGfeOriginatorCompFProps = srcDataLoan.sGfeOriginatorCompFProps;
                    destDataLoan.sGfeLenderCreditFProps = srcDataLoan.sGfeLenderCreditFProps;
                    destDataLoan.sGfeDiscountPointFProps = srcDataLoan.sGfeDiscountPointFProps;
                    destDataLoan.sApprFProps = srcDataLoan.sApprFProps;
                    destDataLoan.sCrFProps = srcDataLoan.sCrFProps;
                    destDataLoan.sInspectFProps = srcDataLoan.sInspectFProps;
                    destDataLoan.sMBrokFProps = srcDataLoan.sMBrokFProps;
                    destDataLoan.sTxServFProps = srcDataLoan.sTxServFProps;
                    destDataLoan.sFloodCertificationFProps = srcDataLoan.sFloodCertificationFProps;
                    destDataLoan.sProcFProps = srcDataLoan.sProcFProps;
                    destDataLoan.sUwFProps = srcDataLoan.sUwFProps;
                    destDataLoan.sWireFProps = srcDataLoan.sWireFProps;
                    destDataLoan.s800U1FProps = srcDataLoan.s800U1FProps;
                    destDataLoan.s800U2FProps = srcDataLoan.s800U2FProps;
                    destDataLoan.s800U3FProps = srcDataLoan.s800U3FProps;
                    destDataLoan.s800U4FProps = srcDataLoan.s800U4FProps;
                    destDataLoan.s800U5FProps = srcDataLoan.s800U5FProps;
                    destDataLoan.sIPiaProps = srcDataLoan.sIPiaProps;
                    destDataLoan.sMipPiaProps = srcDataLoan.sMipPiaProps;
                    destDataLoan.sHazInsPiaProps = srcDataLoan.sHazInsPiaProps;
                    destDataLoan.s904PiaProps = srcDataLoan.s904PiaProps;
                    destDataLoan.sVaFfProps = srcDataLoan.sVaFfProps;
                    destDataLoan.s900U1PiaProps = srcDataLoan.s900U1PiaProps;
                    destDataLoan.sHazInsRsrvProps = srcDataLoan.sHazInsRsrvProps;
                    destDataLoan.sMInsRsrvProps = srcDataLoan.sMInsRsrvProps;
                    destDataLoan.sSchoolTxRsrvProps = srcDataLoan.sSchoolTxRsrvProps;
                    destDataLoan.sRealETxRsrvProps = srcDataLoan.sRealETxRsrvProps;
                    destDataLoan.sFloodInsRsrvProps = srcDataLoan.sFloodInsRsrvProps;
                    destDataLoan.s1006RsrvProps = srcDataLoan.s1006RsrvProps;
                    destDataLoan.s1007RsrvProps = srcDataLoan.s1007RsrvProps;
                    destDataLoan.sAggregateAdjRsrvProps = srcDataLoan.sAggregateAdjRsrvProps;
                    destDataLoan.sEscrowFProps = srcDataLoan.sEscrowFProps;
                    destDataLoan.sOwnerTitleInsProps = srcDataLoan.sOwnerTitleInsProps;
                    destDataLoan.sDocPrepFProps = srcDataLoan.sDocPrepFProps;
                    destDataLoan.sNotaryFProps = srcDataLoan.sNotaryFProps;
                    destDataLoan.sAttorneyFProps = srcDataLoan.sAttorneyFProps;
                    destDataLoan.sTitleInsFProps = srcDataLoan.sTitleInsFProps;
                    destDataLoan.sU1TcProps = srcDataLoan.sU1TcProps;
                    destDataLoan.sU2TcProps = srcDataLoan.sU2TcProps;
                    destDataLoan.sU3TcProps = srcDataLoan.sU3TcProps;
                    destDataLoan.sU4TcProps = srcDataLoan.sU4TcProps;
                    destDataLoan.sRecFProps = srcDataLoan.sRecFProps;
                    destDataLoan.sCountyRtcProps = srcDataLoan.sCountyRtcProps;
                    destDataLoan.sStateRtcProps = srcDataLoan.sStateRtcProps;
                    destDataLoan.sU1GovRtcProps = srcDataLoan.sU1GovRtcProps;
                    destDataLoan.sU2GovRtcProps = srcDataLoan.sU2GovRtcProps;
                    destDataLoan.sU3GovRtcProps = srcDataLoan.sU3GovRtcProps;
                    destDataLoan.sPestInspectFProps = srcDataLoan.sPestInspectFProps;
                    destDataLoan.sU1ScProps = srcDataLoan.sU1ScProps;
                    destDataLoan.sU2ScProps = srcDataLoan.sU2ScProps;
                    destDataLoan.sU3ScProps = srcDataLoan.sU3ScProps;
                    destDataLoan.sU4ScProps = srcDataLoan.sU4ScProps;
                    destDataLoan.sU5ScProps = srcDataLoan.sU5ScProps;
                    destDataLoan.sApprFPaidTo = srcDataLoan.sApprFPaidTo;
                    destDataLoan.sCrFPaidTo = srcDataLoan.sCrFPaidTo;
                    destDataLoan.sTxServFPaidTo = srcDataLoan.sTxServFPaidTo;
                    destDataLoan.sFloodCertificationFPaidTo = srcDataLoan.sFloodCertificationFPaidTo;
                    destDataLoan.sInspectFPaidTo = srcDataLoan.sInspectFPaidTo;
                    destDataLoan.sProcFPaidTo = srcDataLoan.sProcFPaidTo;
                    destDataLoan.sUwFPaidTo = srcDataLoan.sUwFPaidTo;
                    destDataLoan.sWireFPaidTo = srcDataLoan.sWireFPaidTo;
                    destDataLoan.s800U1FPaidTo = srcDataLoan.s800U1FPaidTo;
                    destDataLoan.s800U2FPaidTo = srcDataLoan.s800U2FPaidTo;
                    destDataLoan.s800U3FPaidTo = srcDataLoan.s800U3FPaidTo;
                    destDataLoan.s800U4FPaidTo = srcDataLoan.s800U4FPaidTo;
                    destDataLoan.s800U5FPaidTo = srcDataLoan.s800U5FPaidTo;
                    destDataLoan.sOwnerTitleInsPaidTo = srcDataLoan.sOwnerTitleInsPaidTo;
                    destDataLoan.sDocPrepFPaidTo = srcDataLoan.sDocPrepFPaidTo;
                    destDataLoan.sNotaryFPaidTo = srcDataLoan.sNotaryFPaidTo;
                    destDataLoan.sAttorneyFPaidTo = srcDataLoan.sAttorneyFPaidTo;
                    destDataLoan.sU1TcPaidTo = srcDataLoan.sU1TcPaidTo;
                    destDataLoan.sU2TcPaidTo = srcDataLoan.sU2TcPaidTo;
                    destDataLoan.sU3TcPaidTo = srcDataLoan.sU3TcPaidTo;
                    destDataLoan.sU4TcPaidTo = srcDataLoan.sU4TcPaidTo;
                    destDataLoan.sU1GovRtcPaidTo = srcDataLoan.sU1GovRtcPaidTo;
                    destDataLoan.sU2GovRtcPaidTo = srcDataLoan.sU2GovRtcPaidTo;
                    destDataLoan.sU3GovRtcPaidTo = srcDataLoan.sU3GovRtcPaidTo;
                    destDataLoan.sPestInspectPaidTo = srcDataLoan.sPestInspectPaidTo;
                    destDataLoan.sU1ScPaidTo = srcDataLoan.sU1ScPaidTo;
                    destDataLoan.sU2ScPaidTo = srcDataLoan.sU2ScPaidTo;
                    destDataLoan.sU3ScPaidTo = srcDataLoan.sU3ScPaidTo;
                    destDataLoan.sU4ScPaidTo = srcDataLoan.sU4ScPaidTo;
                    destDataLoan.sU5ScPaidTo = srcDataLoan.sU5ScPaidTo;
                    destDataLoan.sHazInsPiaPaidTo = srcDataLoan.sHazInsPiaPaidTo;
                    destDataLoan.sMipPiaPaidTo = srcDataLoan.sMipPiaPaidTo;
                    destDataLoan.sVaFfPaidTo = srcDataLoan.sVaFfPaidTo;
                    destDataLoan.s800U1FGfeSection = srcDataLoan.s800U1FGfeSection;
                    destDataLoan.s800U2FGfeSection = srcDataLoan.s800U2FGfeSection;
                    destDataLoan.s800U3FGfeSection = srcDataLoan.s800U3FGfeSection;
                    destDataLoan.s800U4FGfeSection = srcDataLoan.s800U4FGfeSection;
                    destDataLoan.s800U5FGfeSection = srcDataLoan.s800U5FGfeSection;
                    destDataLoan.sEscrowFGfeSection = srcDataLoan.sEscrowFGfeSection;
                    destDataLoan.sDocPrepFGfeSection = srcDataLoan.sDocPrepFGfeSection;
                    destDataLoan.sNotaryFGfeSection = srcDataLoan.sNotaryFGfeSection;
                    destDataLoan.sAttorneyFGfeSection = srcDataLoan.sAttorneyFGfeSection;
                    destDataLoan.sU1TcGfeSection = srcDataLoan.sU1TcGfeSection;
                    destDataLoan.sU2TcGfeSection = srcDataLoan.sU2TcGfeSection;
                    destDataLoan.sU3TcGfeSection = srcDataLoan.sU3TcGfeSection;
                    destDataLoan.sU4TcGfeSection = srcDataLoan.sU4TcGfeSection;
                    destDataLoan.sU1GovRtcGfeSection = srcDataLoan.sU1GovRtcGfeSection;
                    destDataLoan.sU2GovRtcGfeSection = srcDataLoan.sU2GovRtcGfeSection;
                    destDataLoan.sU3GovRtcGfeSection = srcDataLoan.sU3GovRtcGfeSection;
                    destDataLoan.sU1ScGfeSection = srcDataLoan.sU1ScGfeSection;
                    destDataLoan.sU2ScGfeSection = srcDataLoan.sU2ScGfeSection;
                    destDataLoan.sU3ScGfeSection = srcDataLoan.sU3ScGfeSection;
                    destDataLoan.sU4ScGfeSection = srcDataLoan.sU4ScGfeSection;
                    destDataLoan.sU5ScGfeSection = srcDataLoan.sU5ScGfeSection;
                    destDataLoan.s904PiaGfeSection = srcDataLoan.s904PiaGfeSection;
                    destDataLoan.s900U1PiaGfeSection = srcDataLoan.s900U1PiaGfeSection;
                    destDataLoan.sGfeTradeOffLowerCCLoanAmt_rep = srcDataLoan.sGfeTradeOffLowerCCLoanAmt_rep;
                    destDataLoan.sGfeTradeOffLowerCCNoteIR_rep = srcDataLoan.sGfeTradeOffLowerCCNoteIR_rep;
                    destDataLoan.sGfeTradeOffLowerCCClosingCost_rep = srcDataLoan.sGfeTradeOffLowerCCClosingCost_rep;
                    destDataLoan.sGfeTradeOffLowerRateLoanAmt_rep = srcDataLoan.sGfeTradeOffLowerRateLoanAmt_rep;
                    destDataLoan.sGfeTradeOffLowerRateNoteIR_rep = srcDataLoan.sGfeTradeOffLowerRateNoteIR_rep;
                    destDataLoan.sGfeTradeOffLowerRateClosingCost_rep = srcDataLoan.sGfeTradeOffLowerRateClosingCost_rep;
                    destDataLoan.sGfeShoppingCartLoan1OriginatorName = srcDataLoan.sGfeShoppingCartLoan1OriginatorName;
                    destDataLoan.sGfeShoppingCartLoan1LoanAmt_rep = srcDataLoan.sGfeShoppingCartLoan1LoanAmt_rep;
                    destDataLoan.sGfeShoppingCartLoan1LoanTerm_rep = srcDataLoan.sGfeShoppingCartLoan1LoanTerm_rep;
                    destDataLoan.sGfeShoppingCartLoan1NoteIR_rep = srcDataLoan.sGfeShoppingCartLoan1NoteIR_rep;
                    destDataLoan.sGfeShoppingCartLoan1InitialPmt_rep = srcDataLoan.sGfeShoppingCartLoan1InitialPmt_rep;
                    destDataLoan.sGfeShoppingCartLoan1RateLockPeriod_rep = srcDataLoan.sGfeShoppingCartLoan1RateLockPeriod_rep;
                    destDataLoan.sGfeShoppingCartLoan1CanRateIncreaseTri = srcDataLoan.sGfeShoppingCartLoan1CanRateIncreaseTri;
                    destDataLoan.sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri = srcDataLoan.sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri;
                    destDataLoan.sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri = srcDataLoan.sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri;
                    destDataLoan.sGfeShoppingCartLoan1HavePpmtPenaltyTri = srcDataLoan.sGfeShoppingCartLoan1HavePpmtPenaltyTri;
                    destDataLoan.sGfeShoppingCartLoan1IsBalloonTri = srcDataLoan.sGfeShoppingCartLoan1IsBalloonTri;
                    destDataLoan.sGfeShoppingCartLoan1TotalClosingCost_rep = srcDataLoan.sGfeShoppingCartLoan1TotalClosingCost_rep;
                    destDataLoan.sGfeShoppingCartLoan2OriginatorName = srcDataLoan.sGfeShoppingCartLoan2OriginatorName;
                    destDataLoan.sGfeShoppingCartLoan2LoanAmt_rep = srcDataLoan.sGfeShoppingCartLoan2LoanAmt_rep;
                    destDataLoan.sGfeShoppingCartLoan2LoanTerm_rep = srcDataLoan.sGfeShoppingCartLoan2LoanTerm_rep;
                    destDataLoan.sGfeShoppingCartLoan2NoteIR_rep = srcDataLoan.sGfeShoppingCartLoan2NoteIR_rep;
                    destDataLoan.sGfeShoppingCartLoan2InitialPmt_rep = srcDataLoan.sGfeShoppingCartLoan2InitialPmt_rep;
                    destDataLoan.sGfeShoppingCartLoan2RateLockPeriod_rep = srcDataLoan.sGfeShoppingCartLoan2RateLockPeriod_rep;
                    destDataLoan.sGfeShoppingCartLoan2CanRateIncreaseTri = srcDataLoan.sGfeShoppingCartLoan2CanRateIncreaseTri;
                    destDataLoan.sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri = srcDataLoan.sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri;
                    destDataLoan.sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri = srcDataLoan.sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri;
                    destDataLoan.sGfeShoppingCartLoan2HavePpmtPenaltyTri = srcDataLoan.sGfeShoppingCartLoan2HavePpmtPenaltyTri;
                    destDataLoan.sGfeShoppingCartLoan2IsBalloonTri = srcDataLoan.sGfeShoppingCartLoan2IsBalloonTri;
                    destDataLoan.sGfeShoppingCartLoan2TotalClosingCost_rep = srcDataLoan.sGfeShoppingCartLoan2TotalClosingCost_rep;
                    destDataLoan.sGfeShoppingCartLoan3OriginatorName = srcDataLoan.sGfeShoppingCartLoan3OriginatorName;
                    destDataLoan.sGfeShoppingCartLoan3LoanAmt_rep = srcDataLoan.sGfeShoppingCartLoan3LoanAmt_rep;
                    destDataLoan.sGfeShoppingCartLoan3LoanTerm_rep = srcDataLoan.sGfeShoppingCartLoan3LoanTerm_rep;
                    destDataLoan.sGfeShoppingCartLoan3NoteIR_rep = srcDataLoan.sGfeShoppingCartLoan3NoteIR_rep;
                    destDataLoan.sGfeShoppingCartLoan3InitialPmt_rep = srcDataLoan.sGfeShoppingCartLoan3InitialPmt_rep;
                    destDataLoan.sGfeShoppingCartLoan3RateLockPeriod_rep = srcDataLoan.sGfeShoppingCartLoan3RateLockPeriod_rep;
                    destDataLoan.sGfeShoppingCartLoan3CanRateIncreaseTri = srcDataLoan.sGfeShoppingCartLoan3CanRateIncreaseTri;
                    destDataLoan.sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri = srcDataLoan.sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri;
                    destDataLoan.sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri = srcDataLoan.sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri;
                    destDataLoan.sGfeShoppingCartLoan3HavePpmtPenaltyTri = srcDataLoan.sGfeShoppingCartLoan3HavePpmtPenaltyTri;
                    destDataLoan.sGfeShoppingCartLoan3IsBalloonTri = srcDataLoan.sGfeShoppingCartLoan3IsBalloonTri;
                    destDataLoan.sGfeShoppingCartLoan3TotalClosingCost_rep = srcDataLoan.sGfeShoppingCartLoan3TotalClosingCost_rep;
                    destDataLoan.sGfeIsTPOTransaction = srcDataLoan.sGfeIsTPOTransaction;
                    destDataLoan.sIsItemizeBrokerCommissionOnIFW = srcDataLoan.sIsItemizeBrokerCommissionOnIFW;
                    destDataLoan.SetOriginatorCompensation(
                        srcDataLoan.sOriginatorCompensationPaymentSourceT,
                        srcDataLoan.sGfeOriginatorCompFPc_rep,
                        srcDataLoan.sGfeOriginatorCompFBaseT,
                        srcDataLoan.sGfeOriginatorCompFMb_rep);
                    destDataLoan.sGfeRedisclosureD_rep = srcDataLoan.sGfeRedisclosureD_rep;
                    destDataLoan.Set_sGfeInitialDisclosureD_rep(srcDataLoan.sGfeInitialDisclosureD_rep, E_GFEArchivedReasonT.InitialDislosureDateSetViaLeadConversionLO);

                    // Credit Denial (Expanded)
                    destDataLoan.sRejectD_rep = srcDataLoan.sRejectD_rep;
                    destDataLoan.sHmdaDeniedFormDoneD_rep = srcDataLoan.sHmdaDeniedFormDoneD_rep;
                    destDataLoan.sHmdaDeniedFormDone = srcDataLoan.sHmdaDeniedFormDone;
                    destDataLoan.sHmdaDeniedFormDoneBy = srcDataLoan.sHmdaDeniedFormDoneBy;

                    // VA Loan Analysis (Expanded)
                    destDataLoan.sVaCashdwnPmt_rep = srcDataLoan.sVaCashdwnPmt_rep;
                    destDataLoan.sVaCashdwnPmtLckd = srcDataLoan.sVaCashdwnPmtLckd;
                    destDataLoan.sVaMaintainAssessPmtLckd = srcDataLoan.sVaMaintainAssessPmtLckd;
                    destDataLoan.sVaMaintainAssessPmt_rep = srcDataLoan.sVaMaintainAssessPmt_rep;
                    destDataLoan.sVaProHazIns_rep = srcDataLoan.sVaProHazIns_rep;
                    destDataLoan.sVaProHazInsLckd = srcDataLoan.sVaProHazInsLckd;
                    destDataLoan.sVaProMaintenancePmt_rep = srcDataLoan.sVaProMaintenancePmt_rep;
                    destDataLoan.sVaProRealETx_rep = srcDataLoan.sVaProRealETx_rep;
                    destDataLoan.sVaProRealETxLckd = srcDataLoan.sVaProRealETxLckd;
                    destDataLoan.sVaProThisMPmt_rep = srcDataLoan.sVaProThisMPmt_rep;
                    destDataLoan.sVaProThisMPmtLckd = srcDataLoan.sVaProThisMPmtLckd;
                    destDataLoan.sVaProUtilityPmt_rep = srcDataLoan.sVaProUtilityPmt_rep;
                    destDataLoan.sVaSpecialAssessPmt_rep = srcDataLoan.sVaSpecialAssessPmt_rep;

                    // Submit to DO/DU (Expanded)
                    if (brokerDb.IsExpandedLeadEditorDoEnabled || brokerDb.IsExpandedLeadEditorDuEnabled)
                    {
                        destDataLoan.sFannieARMPlanNum_rep = srcDataLoan.sFannieARMPlanNum_rep;
                        destDataLoan.sFannieARMPlanNumLckd = srcDataLoan.sFannieARMPlanNumLckd;
                        destDataLoan.sIsSellerProvidedBelowMktFin = srcDataLoan.sIsSellerProvidedBelowMktFin;
                        destDataLoan.sArmIndexT = srcDataLoan.sArmIndexT;
                        destDataLoan.sArmIndexTLckd = srcDataLoan.sArmIndexTLckd;
                        destDataLoan.sFannieSpT = srcDataLoan.sFannieSpT;
                        destDataLoan.sFannieDocT = srcDataLoan.sFannieDocT;
                        destDataLoan.sDuCaseId = srcDataLoan.sDuCaseId;
                        destDataLoan.sIsCommunityLending = srcDataLoan.sIsCommunityLending;
                        destDataLoan.sFannieMsa = srcDataLoan.sFannieMsa;
                        destDataLoan.sFannieCommunityLendingT = srcDataLoan.sFannieCommunityLendingT;
                        destDataLoan.sIsFannieNeighbors = srcDataLoan.sIsFannieNeighbors;
                        destDataLoan.sIsCommunitySecond = srcDataLoan.sIsCommunitySecond;
                        destDataLoan.sFannieIncomeLimitAdjPc_rep = srcDataLoan.sFannieIncomeLimitAdjPc_rep;
                        destDataLoan.sFannieProdDesc = srcDataLoan.sFannieProdDesc;
                        destDataLoan.sExportAdditionalLiabitiesFannieMae = srcDataLoan.sExportAdditionalLiabitiesFannieMae;
                        destDataLoan.sExportAdditionalLiabitiesDODU = srcDataLoan.sExportAdditionalLiabitiesDODU;

                        destDataLoan.sAusRecommendation = srcDataLoan.sAusRecommendation;
                        destDataLoan.sDuFindingsHtml = srcDataLoan.sDuFindingsHtml;
                    }

                    // Submit to LP (Expanded)
                    if (brokerDb.IsExpandedLeadEditorLpEnabled)
                    {
                        destDataLoan.sBuildingStatusT = srcDataLoan.sBuildingStatusT;
                        destDataLoan.sBuydown = srcDataLoan.sBuydown;
                        destDataLoan.sBuydownContributorT = srcDataLoan.sBuydownContributorT;
                        destDataLoan.sFHASalesConcessions_rep = srcDataLoan.sFHASalesConcessions_rep;
                        destDataLoan.sFredAffordProgId = srcDataLoan.sFredAffordProgId;
                        destDataLoan.sFredProcPointT = srcDataLoan.sFredProcPointT;
                        destDataLoan.sFreddieArmIndexT = srcDataLoan.sFreddieArmIndexT;
                        destDataLoan.sFreddieArmIndexTLckd = srcDataLoan.sFreddieArmIndexTLckd;
                        destDataLoan.sFredieReservesAmt_rep = srcDataLoan.sFredieReservesAmt_rep;
                        destDataLoan.sGseRefPurposeT = srcDataLoan.sGseRefPurposeT;
                        destDataLoan.sHelocBal_rep = srcDataLoan.sHelocBal_rep;
                        destDataLoan.sHelocCreditLimit_rep = srcDataLoan.sHelocCreditLimit_rep;
                        destDataLoan.sNegAmortT = srcDataLoan.sNegAmortT;
                        destDataLoan.sPayingOffSubordinate = srcDataLoan.sPayingOffSubordinate;
                        destDataLoan.sSpIsInPud = srcDataLoan.sSpIsInPud;
                        destDataLoan.sSpMarketVal_rep = srcDataLoan.sSpMarketVal_rep;
                        destDataLoan.sFHAFinancedDiscPtAmt_rep = srcDataLoan.sFHAFinancedDiscPtAmt_rep;
                        destDataLoan.sLpAusKey = srcDataLoan.sLpAusKey;
                        destDataLoan.sFreddieLoanId = srcDataLoan.sFreddieLoanId;
                        destDataLoan.sFreddieTransactionId = srcDataLoan.sFreddieTransactionId;
                        destDataLoan.sFreddieSellerNum = srcDataLoan.sFreddieSellerNum;
                        destDataLoan.sFreddieTpoNum = srcDataLoan.sFreddieTpoNum;
                        destDataLoan.sFreddieNotpNum = srcDataLoan.sFreddieNotpNum;
                        destDataLoan.sFreddieLenderBranchId = srcDataLoan.sFreddieLenderBranchId;
                        destDataLoan.sFreddieConstructionT = srcDataLoan.sFreddieConstructionT;
                        destDataLoan.sFredieReservesAmtLckd = srcDataLoan.sFredieReservesAmtLckd;
                        destDataLoan.sFreddieLpPassword = srcDataLoan.sFreddieLpPassword;

                        destDataLoan.sFreddieFeedbackResponseXml = srcDataLoan.sFreddieFeedbackResponseXml;
                    }

                    // TOTAL (Expanded)
                    if (brokerDb.IsExpandedLeadEditorTotalEnabled)
                    {
                        destDataLoan.sTotalScoreCertificateXmlContent = srcDataLoan.sTotalScoreCertificateXmlContent;
                    }

                    // Expanded Preparer Info
                    var expandedPreparerList = new List<E_PreparerFormT>()
                        {
                            E_PreparerFormT.App1003Interviewer,
                            E_PreparerFormT.BorrSignatureAuthorization,
                            E_PreparerFormT.CreditDenialScoreContact,
                            E_PreparerFormT.CreditDenialStatement,
                            E_PreparerFormT.ECOA,
                            E_PreparerFormT.Gfe,
                            E_PreparerFormT.PrivacyPolicyDisclosure
                        };

                    if (brokerDb.IsEditLeadsInFullLoanEditor)
                    {
                        expandedPreparerList.Add(E_PreparerFormT.MortgageServicingRightsHolder);
                    }

                    foreach (var preparerFormT in expandedPreparerList)
                    {
                        var srcPreparer = srcDataLoan.GetPreparerOfForm(preparerFormT, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                        if (srcPreparer.IsEmpty)
                        {
                            continue;
                        }

                        destDataLoan.PreparerCollection.AddCopyOfPreparer(srcPreparer);
                    }

                    // Custom Fields
                    if (brokerDb.IsExpandedLeadEditorCustomFieldsEnabled)
                    {
                        // Custom Fields 1-20
                        destDataLoan.sCustomField1Desc = srcDataLoan.sCustomField1Desc;
                        destDataLoan.sCustomField2Desc = srcDataLoan.sCustomField2Desc;
                        destDataLoan.sCustomField3Desc = srcDataLoan.sCustomField3Desc;
                        destDataLoan.sCustomField4Desc = srcDataLoan.sCustomField4Desc;
                        destDataLoan.sCustomField5Desc = srcDataLoan.sCustomField5Desc;
                        destDataLoan.sCustomField6Desc = srcDataLoan.sCustomField6Desc;
                        destDataLoan.sCustomField7Desc = srcDataLoan.sCustomField7Desc;
                        destDataLoan.sCustomField8Desc = srcDataLoan.sCustomField8Desc;
                        destDataLoan.sCustomField9Desc = srcDataLoan.sCustomField9Desc;
                        destDataLoan.sCustomField10Desc = srcDataLoan.sCustomField10Desc;
                        destDataLoan.sCustomField11Desc = srcDataLoan.sCustomField11Desc;
                        destDataLoan.sCustomField12Desc = srcDataLoan.sCustomField12Desc;
                        destDataLoan.sCustomField13Desc = srcDataLoan.sCustomField13Desc;
                        destDataLoan.sCustomField14Desc = srcDataLoan.sCustomField14Desc;
                        destDataLoan.sCustomField15Desc = srcDataLoan.sCustomField15Desc;
                        destDataLoan.sCustomField16Desc = srcDataLoan.sCustomField16Desc;
                        destDataLoan.sCustomField17Desc = srcDataLoan.sCustomField17Desc;
                        destDataLoan.sCustomField18Desc = srcDataLoan.sCustomField18Desc;
                        destDataLoan.sCustomField19Desc = srcDataLoan.sCustomField19Desc;
                        destDataLoan.sCustomField20Desc = srcDataLoan.sCustomField20Desc;

                        destDataLoan.sCustomField1D = srcDataLoan.sCustomField1D;
                        destDataLoan.sCustomField2D = srcDataLoan.sCustomField2D;
                        destDataLoan.sCustomField3D = srcDataLoan.sCustomField3D;
                        destDataLoan.sCustomField4D = srcDataLoan.sCustomField4D;
                        destDataLoan.sCustomField5D = srcDataLoan.sCustomField5D;
                        destDataLoan.sCustomField6D = srcDataLoan.sCustomField6D;
                        destDataLoan.sCustomField7D = srcDataLoan.sCustomField7D;
                        destDataLoan.sCustomField8D = srcDataLoan.sCustomField8D;
                        destDataLoan.sCustomField9D = srcDataLoan.sCustomField9D;
                        destDataLoan.sCustomField10D = srcDataLoan.sCustomField10D;
                        destDataLoan.sCustomField11D = srcDataLoan.sCustomField11D;
                        destDataLoan.sCustomField12D = srcDataLoan.sCustomField12D;
                        destDataLoan.sCustomField13D = srcDataLoan.sCustomField13D;
                        destDataLoan.sCustomField14D = srcDataLoan.sCustomField14D;
                        destDataLoan.sCustomField15D = srcDataLoan.sCustomField15D;
                        destDataLoan.sCustomField16D = srcDataLoan.sCustomField16D;
                        destDataLoan.sCustomField17D = srcDataLoan.sCustomField17D;
                        destDataLoan.sCustomField18D = srcDataLoan.sCustomField18D;
                        destDataLoan.sCustomField19D = srcDataLoan.sCustomField19D;
                        destDataLoan.sCustomField20D = srcDataLoan.sCustomField20D;

                        destDataLoan.sCustomField1Money = srcDataLoan.sCustomField1Money;
                        destDataLoan.sCustomField2Money = srcDataLoan.sCustomField2Money;
                        destDataLoan.sCustomField3Money = srcDataLoan.sCustomField3Money;
                        destDataLoan.sCustomField4Money = srcDataLoan.sCustomField4Money;
                        destDataLoan.sCustomField5Money = srcDataLoan.sCustomField5Money;
                        destDataLoan.sCustomField6Money = srcDataLoan.sCustomField6Money;
                        destDataLoan.sCustomField7Money = srcDataLoan.sCustomField7Money;
                        destDataLoan.sCustomField8Money = srcDataLoan.sCustomField8Money;
                        destDataLoan.sCustomField9Money = srcDataLoan.sCustomField9Money;
                        destDataLoan.sCustomField10Money = srcDataLoan.sCustomField10Money;
                        destDataLoan.sCustomField11Money = srcDataLoan.sCustomField11Money;
                        destDataLoan.sCustomField12Money = srcDataLoan.sCustomField12Money;
                        destDataLoan.sCustomField13Money = srcDataLoan.sCustomField13Money;
                        destDataLoan.sCustomField14Money = srcDataLoan.sCustomField14Money;
                        destDataLoan.sCustomField15Money = srcDataLoan.sCustomField15Money;
                        destDataLoan.sCustomField16Money = srcDataLoan.sCustomField16Money;
                        destDataLoan.sCustomField17Money = srcDataLoan.sCustomField17Money;
                        destDataLoan.sCustomField18Money = srcDataLoan.sCustomField18Money;
                        destDataLoan.sCustomField19Money = srcDataLoan.sCustomField19Money;
                        destDataLoan.sCustomField20Money = srcDataLoan.sCustomField20Money;

                        destDataLoan.sCustomField1Pc = srcDataLoan.sCustomField1Pc;
                        destDataLoan.sCustomField2Pc = srcDataLoan.sCustomField2Pc;
                        destDataLoan.sCustomField3Pc = srcDataLoan.sCustomField3Pc;
                        destDataLoan.sCustomField4Pc = srcDataLoan.sCustomField4Pc;
                        destDataLoan.sCustomField5Pc = srcDataLoan.sCustomField5Pc;
                        destDataLoan.sCustomField6Pc = srcDataLoan.sCustomField6Pc;
                        destDataLoan.sCustomField7Pc = srcDataLoan.sCustomField7Pc;
                        destDataLoan.sCustomField8Pc = srcDataLoan.sCustomField8Pc;
                        destDataLoan.sCustomField9Pc = srcDataLoan.sCustomField9Pc;
                        destDataLoan.sCustomField10Pc = srcDataLoan.sCustomField10Pc;
                        destDataLoan.sCustomField11Pc = srcDataLoan.sCustomField11Pc;
                        destDataLoan.sCustomField12Pc = srcDataLoan.sCustomField12Pc;
                        destDataLoan.sCustomField13Pc = srcDataLoan.sCustomField13Pc;
                        destDataLoan.sCustomField14Pc = srcDataLoan.sCustomField14Pc;
                        destDataLoan.sCustomField15Pc = srcDataLoan.sCustomField15Pc;
                        destDataLoan.sCustomField16Pc = srcDataLoan.sCustomField16Pc;
                        destDataLoan.sCustomField17Pc = srcDataLoan.sCustomField17Pc;
                        destDataLoan.sCustomField18Pc = srcDataLoan.sCustomField18Pc;
                        destDataLoan.sCustomField19Pc = srcDataLoan.sCustomField19Pc;
                        destDataLoan.sCustomField20Pc = srcDataLoan.sCustomField20Pc;

                        destDataLoan.sCustomField1Bit = srcDataLoan.sCustomField1Bit;
                        destDataLoan.sCustomField2Bit = srcDataLoan.sCustomField2Bit;
                        destDataLoan.sCustomField3Bit = srcDataLoan.sCustomField3Bit;
                        destDataLoan.sCustomField4Bit = srcDataLoan.sCustomField4Bit;
                        destDataLoan.sCustomField5Bit = srcDataLoan.sCustomField5Bit;
                        destDataLoan.sCustomField6Bit = srcDataLoan.sCustomField6Bit;
                        destDataLoan.sCustomField7Bit = srcDataLoan.sCustomField7Bit;
                        destDataLoan.sCustomField8Bit = srcDataLoan.sCustomField8Bit;
                        destDataLoan.sCustomField9Bit = srcDataLoan.sCustomField9Bit;
                        destDataLoan.sCustomField10Bit = srcDataLoan.sCustomField10Bit;
                        destDataLoan.sCustomField11Bit = srcDataLoan.sCustomField11Bit;
                        destDataLoan.sCustomField12Bit = srcDataLoan.sCustomField12Bit;
                        destDataLoan.sCustomField13Bit = srcDataLoan.sCustomField13Bit;
                        destDataLoan.sCustomField14Bit = srcDataLoan.sCustomField14Bit;
                        destDataLoan.sCustomField15Bit = srcDataLoan.sCustomField15Bit;
                        destDataLoan.sCustomField16Bit = srcDataLoan.sCustomField16Bit;
                        destDataLoan.sCustomField17Bit = srcDataLoan.sCustomField17Bit;
                        destDataLoan.sCustomField18Bit = srcDataLoan.sCustomField18Bit;
                        destDataLoan.sCustomField19Bit = srcDataLoan.sCustomField19Bit;
                        destDataLoan.sCustomField20Bit = srcDataLoan.sCustomField20Bit;

                        destDataLoan.sCustomField1Notes = srcDataLoan.sCustomField1Notes;
                        destDataLoan.sCustomField2Notes = srcDataLoan.sCustomField2Notes;
                        destDataLoan.sCustomField3Notes = srcDataLoan.sCustomField3Notes;
                        destDataLoan.sCustomField4Notes = srcDataLoan.sCustomField4Notes;
                        destDataLoan.sCustomField5Notes = srcDataLoan.sCustomField5Notes;
                        destDataLoan.sCustomField6Notes = srcDataLoan.sCustomField6Notes;
                        destDataLoan.sCustomField7Notes = srcDataLoan.sCustomField7Notes;
                        destDataLoan.sCustomField8Notes = srcDataLoan.sCustomField8Notes;
                        destDataLoan.sCustomField9Notes = srcDataLoan.sCustomField9Notes;
                        destDataLoan.sCustomField10Notes = srcDataLoan.sCustomField10Notes;
                        destDataLoan.sCustomField11Notes = srcDataLoan.sCustomField11Notes;
                        destDataLoan.sCustomField12Notes = srcDataLoan.sCustomField12Notes;
                        destDataLoan.sCustomField13Notes = srcDataLoan.sCustomField13Notes;
                        destDataLoan.sCustomField14Notes = srcDataLoan.sCustomField14Notes;
                        destDataLoan.sCustomField15Notes = srcDataLoan.sCustomField15Notes;
                        destDataLoan.sCustomField16Notes = srcDataLoan.sCustomField16Notes;
                        destDataLoan.sCustomField17Notes = srcDataLoan.sCustomField17Notes;
                        destDataLoan.sCustomField18Notes = srcDataLoan.sCustomField18Notes;
                        destDataLoan.sCustomField19Notes = srcDataLoan.sCustomField19Notes;
                        destDataLoan.sCustomField20Notes = srcDataLoan.sCustomField20Notes;

                        // Custom Fields 21-40
                        destDataLoan.sCustomField21Desc = srcDataLoan.sCustomField21Desc;
                        destDataLoan.sCustomField22Desc = srcDataLoan.sCustomField22Desc;
                        destDataLoan.sCustomField23Desc = srcDataLoan.sCustomField23Desc;
                        destDataLoan.sCustomField24Desc = srcDataLoan.sCustomField24Desc;
                        destDataLoan.sCustomField25Desc = srcDataLoan.sCustomField25Desc;
                        destDataLoan.sCustomField26Desc = srcDataLoan.sCustomField26Desc;
                        destDataLoan.sCustomField27Desc = srcDataLoan.sCustomField27Desc;
                        destDataLoan.sCustomField28Desc = srcDataLoan.sCustomField28Desc;
                        destDataLoan.sCustomField29Desc = srcDataLoan.sCustomField29Desc;
                        destDataLoan.sCustomField30Desc = srcDataLoan.sCustomField30Desc;
                        destDataLoan.sCustomField31Desc = srcDataLoan.sCustomField31Desc;
                        destDataLoan.sCustomField32Desc = srcDataLoan.sCustomField32Desc;
                        destDataLoan.sCustomField33Desc = srcDataLoan.sCustomField33Desc;
                        destDataLoan.sCustomField34Desc = srcDataLoan.sCustomField34Desc;
                        destDataLoan.sCustomField35Desc = srcDataLoan.sCustomField35Desc;
                        destDataLoan.sCustomField36Desc = srcDataLoan.sCustomField36Desc;
                        destDataLoan.sCustomField37Desc = srcDataLoan.sCustomField37Desc;
                        destDataLoan.sCustomField38Desc = srcDataLoan.sCustomField38Desc;
                        destDataLoan.sCustomField39Desc = srcDataLoan.sCustomField39Desc;
                        destDataLoan.sCustomField40Desc = srcDataLoan.sCustomField40Desc;

                        destDataLoan.sCustomField21D = srcDataLoan.sCustomField21D;
                        destDataLoan.sCustomField22D = srcDataLoan.sCustomField22D;
                        destDataLoan.sCustomField23D = srcDataLoan.sCustomField23D;
                        destDataLoan.sCustomField24D = srcDataLoan.sCustomField24D;
                        destDataLoan.sCustomField25D = srcDataLoan.sCustomField25D;
                        destDataLoan.sCustomField26D = srcDataLoan.sCustomField26D;
                        destDataLoan.sCustomField27D = srcDataLoan.sCustomField27D;
                        destDataLoan.sCustomField28D = srcDataLoan.sCustomField28D;
                        destDataLoan.sCustomField29D = srcDataLoan.sCustomField29D;
                        destDataLoan.sCustomField30D = srcDataLoan.sCustomField30D;
                        destDataLoan.sCustomField31D = srcDataLoan.sCustomField31D;
                        destDataLoan.sCustomField32D = srcDataLoan.sCustomField32D;
                        destDataLoan.sCustomField33D = srcDataLoan.sCustomField33D;
                        destDataLoan.sCustomField34D = srcDataLoan.sCustomField34D;
                        destDataLoan.sCustomField35D = srcDataLoan.sCustomField35D;
                        destDataLoan.sCustomField36D = srcDataLoan.sCustomField36D;
                        destDataLoan.sCustomField37D = srcDataLoan.sCustomField37D;
                        destDataLoan.sCustomField38D = srcDataLoan.sCustomField38D;
                        destDataLoan.sCustomField39D = srcDataLoan.sCustomField39D;
                        destDataLoan.sCustomField40D = srcDataLoan.sCustomField40D;

                        destDataLoan.sCustomField21Money = srcDataLoan.sCustomField21Money;
                        destDataLoan.sCustomField22Money = srcDataLoan.sCustomField22Money;
                        destDataLoan.sCustomField23Money = srcDataLoan.sCustomField23Money;
                        destDataLoan.sCustomField24Money = srcDataLoan.sCustomField24Money;
                        destDataLoan.sCustomField25Money = srcDataLoan.sCustomField25Money;
                        destDataLoan.sCustomField26Money = srcDataLoan.sCustomField26Money;
                        destDataLoan.sCustomField27Money = srcDataLoan.sCustomField27Money;
                        destDataLoan.sCustomField28Money = srcDataLoan.sCustomField28Money;
                        destDataLoan.sCustomField29Money = srcDataLoan.sCustomField29Money;
                        destDataLoan.sCustomField30Money = srcDataLoan.sCustomField30Money;
                        destDataLoan.sCustomField31Money = srcDataLoan.sCustomField31Money;
                        destDataLoan.sCustomField32Money = srcDataLoan.sCustomField32Money;
                        destDataLoan.sCustomField33Money = srcDataLoan.sCustomField33Money;
                        destDataLoan.sCustomField34Money = srcDataLoan.sCustomField34Money;
                        destDataLoan.sCustomField35Money = srcDataLoan.sCustomField35Money;
                        destDataLoan.sCustomField36Money = srcDataLoan.sCustomField36Money;
                        destDataLoan.sCustomField37Money = srcDataLoan.sCustomField37Money;
                        destDataLoan.sCustomField38Money = srcDataLoan.sCustomField38Money;
                        destDataLoan.sCustomField39Money = srcDataLoan.sCustomField39Money;
                        destDataLoan.sCustomField40Money = srcDataLoan.sCustomField40Money;

                        destDataLoan.sCustomField21Pc = srcDataLoan.sCustomField21Pc;
                        destDataLoan.sCustomField22Pc = srcDataLoan.sCustomField22Pc;
                        destDataLoan.sCustomField23Pc = srcDataLoan.sCustomField23Pc;
                        destDataLoan.sCustomField24Pc = srcDataLoan.sCustomField24Pc;
                        destDataLoan.sCustomField25Pc = srcDataLoan.sCustomField25Pc;
                        destDataLoan.sCustomField26Pc = srcDataLoan.sCustomField26Pc;
                        destDataLoan.sCustomField27Pc = srcDataLoan.sCustomField27Pc;
                        destDataLoan.sCustomField28Pc = srcDataLoan.sCustomField28Pc;
                        destDataLoan.sCustomField29Pc = srcDataLoan.sCustomField29Pc;
                        destDataLoan.sCustomField30Pc = srcDataLoan.sCustomField30Pc;
                        destDataLoan.sCustomField31Pc = srcDataLoan.sCustomField31Pc;
                        destDataLoan.sCustomField32Pc = srcDataLoan.sCustomField32Pc;
                        destDataLoan.sCustomField33Pc = srcDataLoan.sCustomField33Pc;
                        destDataLoan.sCustomField34Pc = srcDataLoan.sCustomField34Pc;
                        destDataLoan.sCustomField35Pc = srcDataLoan.sCustomField35Pc;
                        destDataLoan.sCustomField36Pc = srcDataLoan.sCustomField36Pc;
                        destDataLoan.sCustomField37Pc = srcDataLoan.sCustomField37Pc;
                        destDataLoan.sCustomField38Pc = srcDataLoan.sCustomField38Pc;
                        destDataLoan.sCustomField39Pc = srcDataLoan.sCustomField39Pc;
                        destDataLoan.sCustomField40Pc = srcDataLoan.sCustomField40Pc;

                        destDataLoan.sCustomField21Bit = srcDataLoan.sCustomField21Bit;
                        destDataLoan.sCustomField22Bit = srcDataLoan.sCustomField22Bit;
                        destDataLoan.sCustomField23Bit = srcDataLoan.sCustomField23Bit;
                        destDataLoan.sCustomField24Bit = srcDataLoan.sCustomField24Bit;
                        destDataLoan.sCustomField25Bit = srcDataLoan.sCustomField25Bit;
                        destDataLoan.sCustomField26Bit = srcDataLoan.sCustomField26Bit;
                        destDataLoan.sCustomField27Bit = srcDataLoan.sCustomField27Bit;
                        destDataLoan.sCustomField28Bit = srcDataLoan.sCustomField28Bit;
                        destDataLoan.sCustomField29Bit = srcDataLoan.sCustomField29Bit;
                        destDataLoan.sCustomField30Bit = srcDataLoan.sCustomField30Bit;
                        destDataLoan.sCustomField31Bit = srcDataLoan.sCustomField31Bit;
                        destDataLoan.sCustomField32Bit = srcDataLoan.sCustomField32Bit;
                        destDataLoan.sCustomField33Bit = srcDataLoan.sCustomField33Bit;
                        destDataLoan.sCustomField34Bit = srcDataLoan.sCustomField34Bit;
                        destDataLoan.sCustomField35Bit = srcDataLoan.sCustomField35Bit;
                        destDataLoan.sCustomField36Bit = srcDataLoan.sCustomField36Bit;
                        destDataLoan.sCustomField37Bit = srcDataLoan.sCustomField37Bit;
                        destDataLoan.sCustomField38Bit = srcDataLoan.sCustomField38Bit;
                        destDataLoan.sCustomField39Bit = srcDataLoan.sCustomField39Bit;
                        destDataLoan.sCustomField40Bit = srcDataLoan.sCustomField40Bit;

                        destDataLoan.sCustomField21Notes = srcDataLoan.sCustomField21Notes;
                        destDataLoan.sCustomField22Notes = srcDataLoan.sCustomField22Notes;
                        destDataLoan.sCustomField23Notes = srcDataLoan.sCustomField23Notes;
                        destDataLoan.sCustomField24Notes = srcDataLoan.sCustomField24Notes;
                        destDataLoan.sCustomField25Notes = srcDataLoan.sCustomField25Notes;
                        destDataLoan.sCustomField26Notes = srcDataLoan.sCustomField26Notes;
                        destDataLoan.sCustomField27Notes = srcDataLoan.sCustomField27Notes;
                        destDataLoan.sCustomField28Notes = srcDataLoan.sCustomField28Notes;
                        destDataLoan.sCustomField29Notes = srcDataLoan.sCustomField29Notes;
                        destDataLoan.sCustomField30Notes = srcDataLoan.sCustomField30Notes;
                        destDataLoan.sCustomField31Notes = srcDataLoan.sCustomField31Notes;
                        destDataLoan.sCustomField32Notes = srcDataLoan.sCustomField32Notes;
                        destDataLoan.sCustomField33Notes = srcDataLoan.sCustomField33Notes;
                        destDataLoan.sCustomField34Notes = srcDataLoan.sCustomField34Notes;
                        destDataLoan.sCustomField35Notes = srcDataLoan.sCustomField35Notes;
                        destDataLoan.sCustomField36Notes = srcDataLoan.sCustomField36Notes;
                        destDataLoan.sCustomField37Notes = srcDataLoan.sCustomField37Notes;
                        destDataLoan.sCustomField38Notes = srcDataLoan.sCustomField38Notes;
                        destDataLoan.sCustomField39Notes = srcDataLoan.sCustomField39Notes;
                        destDataLoan.sCustomField40Notes = srcDataLoan.sCustomField40Notes;

                        // Custom Fields 41-60
                        destDataLoan.sCustomField41Desc = srcDataLoan.sCustomField41Desc;
                        destDataLoan.sCustomField42Desc = srcDataLoan.sCustomField42Desc;
                        destDataLoan.sCustomField43Desc = srcDataLoan.sCustomField43Desc;
                        destDataLoan.sCustomField44Desc = srcDataLoan.sCustomField44Desc;
                        destDataLoan.sCustomField45Desc = srcDataLoan.sCustomField45Desc;
                        destDataLoan.sCustomField46Desc = srcDataLoan.sCustomField46Desc;
                        destDataLoan.sCustomField47Desc = srcDataLoan.sCustomField47Desc;
                        destDataLoan.sCustomField48Desc = srcDataLoan.sCustomField48Desc;
                        destDataLoan.sCustomField49Desc = srcDataLoan.sCustomField49Desc;
                        destDataLoan.sCustomField50Desc = srcDataLoan.sCustomField50Desc;
                        destDataLoan.sCustomField51Desc = srcDataLoan.sCustomField51Desc;
                        destDataLoan.sCustomField52Desc = srcDataLoan.sCustomField52Desc;
                        destDataLoan.sCustomField53Desc = srcDataLoan.sCustomField53Desc;
                        destDataLoan.sCustomField54Desc = srcDataLoan.sCustomField54Desc;
                        destDataLoan.sCustomField55Desc = srcDataLoan.sCustomField55Desc;
                        destDataLoan.sCustomField56Desc = srcDataLoan.sCustomField56Desc;
                        destDataLoan.sCustomField57Desc = srcDataLoan.sCustomField57Desc;
                        destDataLoan.sCustomField58Desc = srcDataLoan.sCustomField58Desc;
                        destDataLoan.sCustomField59Desc = srcDataLoan.sCustomField59Desc;
                        destDataLoan.sCustomField60Desc = srcDataLoan.sCustomField60Desc;

                        destDataLoan.sCustomField41D = srcDataLoan.sCustomField41D;
                        destDataLoan.sCustomField42D = srcDataLoan.sCustomField42D;
                        destDataLoan.sCustomField43D = srcDataLoan.sCustomField43D;
                        destDataLoan.sCustomField44D = srcDataLoan.sCustomField44D;
                        destDataLoan.sCustomField45D = srcDataLoan.sCustomField45D;
                        destDataLoan.sCustomField46D = srcDataLoan.sCustomField46D;
                        destDataLoan.sCustomField47D = srcDataLoan.sCustomField47D;
                        destDataLoan.sCustomField48D = srcDataLoan.sCustomField48D;
                        destDataLoan.sCustomField49D = srcDataLoan.sCustomField49D;
                        destDataLoan.sCustomField50D = srcDataLoan.sCustomField50D;
                        destDataLoan.sCustomField51D = srcDataLoan.sCustomField51D;
                        destDataLoan.sCustomField52D = srcDataLoan.sCustomField52D;
                        destDataLoan.sCustomField53D = srcDataLoan.sCustomField53D;
                        destDataLoan.sCustomField54D = srcDataLoan.sCustomField54D;
                        destDataLoan.sCustomField55D = srcDataLoan.sCustomField55D;
                        destDataLoan.sCustomField56D = srcDataLoan.sCustomField56D;
                        destDataLoan.sCustomField57D = srcDataLoan.sCustomField57D;
                        destDataLoan.sCustomField58D = srcDataLoan.sCustomField58D;
                        destDataLoan.sCustomField59D = srcDataLoan.sCustomField59D;
                        destDataLoan.sCustomField60D = srcDataLoan.sCustomField60D;

                        destDataLoan.sCustomField41Money = srcDataLoan.sCustomField41Money;
                        destDataLoan.sCustomField42Money = srcDataLoan.sCustomField42Money;
                        destDataLoan.sCustomField43Money = srcDataLoan.sCustomField43Money;
                        destDataLoan.sCustomField44Money = srcDataLoan.sCustomField44Money;
                        destDataLoan.sCustomField45Money = srcDataLoan.sCustomField45Money;
                        destDataLoan.sCustomField46Money = srcDataLoan.sCustomField46Money;
                        destDataLoan.sCustomField47Money = srcDataLoan.sCustomField47Money;
                        destDataLoan.sCustomField48Money = srcDataLoan.sCustomField48Money;
                        destDataLoan.sCustomField49Money = srcDataLoan.sCustomField49Money;
                        destDataLoan.sCustomField50Money = srcDataLoan.sCustomField50Money;
                        destDataLoan.sCustomField51Money = srcDataLoan.sCustomField51Money;
                        destDataLoan.sCustomField52Money = srcDataLoan.sCustomField52Money;
                        destDataLoan.sCustomField53Money = srcDataLoan.sCustomField53Money;
                        destDataLoan.sCustomField54Money = srcDataLoan.sCustomField54Money;
                        destDataLoan.sCustomField55Money = srcDataLoan.sCustomField55Money;
                        destDataLoan.sCustomField56Money = srcDataLoan.sCustomField56Money;
                        destDataLoan.sCustomField57Money = srcDataLoan.sCustomField57Money;
                        destDataLoan.sCustomField58Money = srcDataLoan.sCustomField58Money;
                        destDataLoan.sCustomField59Money = srcDataLoan.sCustomField59Money;
                        destDataLoan.sCustomField60Money = srcDataLoan.sCustomField60Money;

                        destDataLoan.sCustomField41Pc = srcDataLoan.sCustomField41Pc;
                        destDataLoan.sCustomField42Pc = srcDataLoan.sCustomField42Pc;
                        destDataLoan.sCustomField43Pc = srcDataLoan.sCustomField43Pc;
                        destDataLoan.sCustomField44Pc = srcDataLoan.sCustomField44Pc;
                        destDataLoan.sCustomField45Pc = srcDataLoan.sCustomField45Pc;
                        destDataLoan.sCustomField46Pc = srcDataLoan.sCustomField46Pc;
                        destDataLoan.sCustomField47Pc = srcDataLoan.sCustomField47Pc;
                        destDataLoan.sCustomField48Pc = srcDataLoan.sCustomField48Pc;
                        destDataLoan.sCustomField49Pc = srcDataLoan.sCustomField49Pc;
                        destDataLoan.sCustomField50Pc = srcDataLoan.sCustomField50Pc;
                        destDataLoan.sCustomField51Pc = srcDataLoan.sCustomField51Pc;
                        destDataLoan.sCustomField52Pc = srcDataLoan.sCustomField52Pc;
                        destDataLoan.sCustomField53Pc = srcDataLoan.sCustomField53Pc;
                        destDataLoan.sCustomField54Pc = srcDataLoan.sCustomField54Pc;
                        destDataLoan.sCustomField55Pc = srcDataLoan.sCustomField55Pc;
                        destDataLoan.sCustomField56Pc = srcDataLoan.sCustomField56Pc;
                        destDataLoan.sCustomField57Pc = srcDataLoan.sCustomField57Pc;
                        destDataLoan.sCustomField58Pc = srcDataLoan.sCustomField58Pc;
                        destDataLoan.sCustomField59Pc = srcDataLoan.sCustomField59Pc;
                        destDataLoan.sCustomField60Pc = srcDataLoan.sCustomField60Pc;

                        destDataLoan.sCustomField41Bit = srcDataLoan.sCustomField41Bit;
                        destDataLoan.sCustomField42Bit = srcDataLoan.sCustomField42Bit;
                        destDataLoan.sCustomField43Bit = srcDataLoan.sCustomField43Bit;
                        destDataLoan.sCustomField44Bit = srcDataLoan.sCustomField44Bit;
                        destDataLoan.sCustomField45Bit = srcDataLoan.sCustomField45Bit;
                        destDataLoan.sCustomField46Bit = srcDataLoan.sCustomField46Bit;
                        destDataLoan.sCustomField47Bit = srcDataLoan.sCustomField47Bit;
                        destDataLoan.sCustomField48Bit = srcDataLoan.sCustomField48Bit;
                        destDataLoan.sCustomField49Bit = srcDataLoan.sCustomField49Bit;
                        destDataLoan.sCustomField50Bit = srcDataLoan.sCustomField50Bit;
                        destDataLoan.sCustomField51Bit = srcDataLoan.sCustomField51Bit;
                        destDataLoan.sCustomField52Bit = srcDataLoan.sCustomField52Bit;
                        destDataLoan.sCustomField53Bit = srcDataLoan.sCustomField53Bit;
                        destDataLoan.sCustomField54Bit = srcDataLoan.sCustomField54Bit;
                        destDataLoan.sCustomField55Bit = srcDataLoan.sCustomField55Bit;
                        destDataLoan.sCustomField56Bit = srcDataLoan.sCustomField56Bit;
                        destDataLoan.sCustomField57Bit = srcDataLoan.sCustomField57Bit;
                        destDataLoan.sCustomField58Bit = srcDataLoan.sCustomField58Bit;
                        destDataLoan.sCustomField59Bit = srcDataLoan.sCustomField59Bit;
                        destDataLoan.sCustomField60Bit = srcDataLoan.sCustomField60Bit;

                        destDataLoan.sCustomField41Notes = srcDataLoan.sCustomField41Notes;
                        destDataLoan.sCustomField42Notes = srcDataLoan.sCustomField42Notes;
                        destDataLoan.sCustomField43Notes = srcDataLoan.sCustomField43Notes;
                        destDataLoan.sCustomField44Notes = srcDataLoan.sCustomField44Notes;
                        destDataLoan.sCustomField45Notes = srcDataLoan.sCustomField45Notes;
                        destDataLoan.sCustomField46Notes = srcDataLoan.sCustomField46Notes;
                        destDataLoan.sCustomField47Notes = srcDataLoan.sCustomField47Notes;
                        destDataLoan.sCustomField48Notes = srcDataLoan.sCustomField48Notes;
                        destDataLoan.sCustomField49Notes = srcDataLoan.sCustomField59Notes;
                        destDataLoan.sCustomField50Notes = srcDataLoan.sCustomField50Notes;
                        destDataLoan.sCustomField51Notes = srcDataLoan.sCustomField51Notes;
                        destDataLoan.sCustomField52Notes = srcDataLoan.sCustomField52Notes;
                        destDataLoan.sCustomField53Notes = srcDataLoan.sCustomField53Notes;
                        destDataLoan.sCustomField54Notes = srcDataLoan.sCustomField54Notes;
                        destDataLoan.sCustomField55Notes = srcDataLoan.sCustomField55Notes;
                        destDataLoan.sCustomField56Notes = srcDataLoan.sCustomField56Notes;
                        destDataLoan.sCustomField57Notes = srcDataLoan.sCustomField57Notes;
                        destDataLoan.sCustomField58Notes = srcDataLoan.sCustomField58Notes;
                        destDataLoan.sCustomField59Notes = srcDataLoan.sCustomField59Notes;
                        destDataLoan.sCustomField60Notes = srcDataLoan.sCustomField60Notes;
                    }

                    destDataLoan.sAusFindingsPull = srcDataLoan.sAusFindingsPull;

                    // copy disclosure fields opm 196899
                    destDataLoan.sDisclosuresDueD_rep = srcDataLoan.sDisclosuresDueD_rep;
                    destDataLoan.sLastDisclosedD = srcDataLoan.sLastDisclosedD;
                    destDataLoan.sDisclosureNeededT = srcDataLoan.sDisclosureNeededT;
                    destDataLoan.sNeedInitialDisc = srcDataLoan.sNeedInitialDisc;
                    destDataLoan.sNeedRedisc = srcDataLoan.sNeedRedisc;

                    // ejm opm 212376
                    // Copy Fields from CFPB pages
                    destDataLoan.sIsAllowExcludeToleranceCure = srcDataLoan.sIsAllowExcludeToleranceCure;

                    // sk opm 217108
                    if (destDataLoan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID)
                    {
                        destDataLoan.sLoads1003LineLFromAdjustments = false;
                        destDataLoan.sOCredit2Amt_rep = srcDataLoan.sOCredit2Amt_rep;
                        destDataLoan.sOCredit2Desc = srcDataLoan.sOCredit2Desc;
                        destDataLoan.sOCredit3Amt_rep = srcDataLoan.sOCredit3Amt_rep;
                        destDataLoan.sOCredit3Desc = srcDataLoan.sOCredit3Desc;
                        destDataLoan.sOCredit4Amt_rep = srcDataLoan.sOCredit4Amt_rep;
                        destDataLoan.sOCredit4Desc = srcDataLoan.sOCredit4Desc;
                    }
                    else 
                    {
                        if (destDataLoan.sLoads1003LineLFromAdjustments == false && srcDataLoan.sLoads1003LineLFromAdjustments == false)
                        {
                            destDataLoan.sOCredit2Amt_rep = srcDataLoan.sOCredit2Amt_rep;
                            destDataLoan.sOCredit2Desc = srcDataLoan.sOCredit2Desc;
                            destDataLoan.sOCredit3Amt_rep = srcDataLoan.sOCredit3Amt_rep;
                            destDataLoan.sOCredit3Desc = srcDataLoan.sOCredit3Desc;
                            destDataLoan.sOCredit4Amt_rep = srcDataLoan.sOCredit4Amt_rep;
                            destDataLoan.sOCredit4Desc = srcDataLoan.sOCredit4Desc;
                        }
                        else
                        {
                            // one or the other has sLoads1003LineLFromAdjustments set to true
                            if (destDataLoan.sLoads1003LineLFromAdjustments == false)
                            {
                                destDataLoan.sLoads1003LineLFromAdjustments = true; // migrates
                            }
                            else if (srcDataLoan.sLoads1003LineLFromAdjustments == false)
                            {
                                srcDataLoan.sLoads1003LineLFromAdjustments = true; // migrates
                            }
                        }
                    }

                    // OPM 228267, 10/8/2015, ML
                    // These fields, which are not copied elsewhere during the loan conversion process,
                    // are present on the Adjustments and Other Credits Page. This page is only available
                    // in the lead editor if the loan is using TRID disclosures.
                    if (srcDataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
                    {
                        destDataLoan.sProrationList = srcDataLoan.sProrationList.GetUnlinkedCopy(IdAssignment.Copy);
                        destDataLoan.sGrossDueFromBorrPersonalProperty_rep = srcDataLoan.sGrossDueFromBorrPersonalProperty_rep;
                        destDataLoan.sReductionsDueToSellerExcessDeposit_rep = srcDataLoan.sReductionsDueToSellerExcessDeposit_rep;
                        destDataLoan.sReductionsDueToSellerPayoffOf1stMtgLoan_rep = srcDataLoan.sReductionsDueToSellerPayoffOf1stMtgLoan_rep;
                        destDataLoan.sReductionsDueToSellerPayoffOf2ndMtgLoan_rep = srcDataLoan.sReductionsDueToSellerPayoffOf2ndMtgLoan_rep;
                        destDataLoan.sONewFinNetProceeds = srcDataLoan.sONewFinNetProceeds;
                        destDataLoan.sONewFinNetProceedsLckd = srcDataLoan.sONewFinNetProceedsLckd;

                        // Per PDE, this should always be copied over if the lead is TRID 2015, regardless of
                        // the value of sLoads1003LineLFromAdjustments.
                        destDataLoan.sAdjustmentList = srcDataLoan.sAdjustmentList.GetUnlinkedCopy(IdAssignment.Copy);

                        destDataLoan.sIsRestrictAdjustmentsAndOtherCreditsDescriptions = srcDataLoan.sIsRestrictAdjustmentsAndOtherCreditsDescriptions;

                        // OPM 231307, 12/28/2015, ML
                        if (srcDataLoan.sPropertyTransferDLckd)
                        {
                            destDataLoan.sPropertyTransferDLckd = true;
                            destDataLoan.sPropertyTransferD = srcDataLoan.sPropertyTransferD;
                        }
                    }

                    destDataLoan.sLOrigFProps_BF = srcDataLoan.sLOrigFProps_BF;
                    destDataLoan.sGfeDiscountPointFProps_BF = srcDataLoan.sGfeDiscountPointFProps_BF;
                    destDataLoan.sGfeLenderCreditFProps_Apr = srcDataLoan.sGfeLenderCreditFProps_Apr;
                    destDataLoan.sFloodCertificationDeterminationT = srcDataLoan.sFloodCertificationDeterminationT;
                    destDataLoan.sIsOptionArm = srcDataLoan.sIsOptionArm;
                    destDataLoan.sOptionArmMinPayPeriod = srcDataLoan.sOptionArmMinPayPeriod;
                    destDataLoan.sOptionArmIntroductoryPeriod = srcDataLoan.sOptionArmIntroductoryPeriod;
                    destDataLoan.sOptionArmInitialFixMinPmtPeriod = srcDataLoan.sOptionArmInitialFixMinPmtPeriod;
                    destDataLoan.sOptionArmMinPayIsIOOnly = srcDataLoan.sOptionArmMinPayIsIOOnly;
                    destDataLoan.sIsFullAmortAfterRecast = srcDataLoan.sIsFullAmortAfterRecast;
                    destDataLoan.sOptionArmMinPayOptionT = srcDataLoan.sOptionArmMinPayOptionT;
                    destDataLoan.sOptionArmPmtDiscount = srcDataLoan.sOptionArmPmtDiscount;
                    destDataLoan.sPpmtAmt = srcDataLoan.sPpmtAmt;
                    destDataLoan.sPpmtMon = srcDataLoan.sPpmtMon;
                    destDataLoan.sPpmtStartMon = srcDataLoan.sPpmtStartMon;
                    destDataLoan.sPpmtOneAmt = srcDataLoan.sPpmtOneAmt;
                    destDataLoan.sPpmtOneMon = srcDataLoan.sPpmtOneMon;
                    destDataLoan.sAprIncludesReqDeposit = srcDataLoan.sAprIncludesReqDeposit;
                    destDataLoan.sHasDemandFeature = srcDataLoan.sHasDemandFeature;
                    destDataLoan.sIsPropertyBeingSoldByCreditor = srcDataLoan.sIsPropertyBeingSoldByCreditor;
                    destDataLoan.sHasVarRFeature = srcDataLoan.sHasVarRFeature;
                    destDataLoan.sInsReqDesc = srcDataLoan.sInsReqDesc;
                    destDataLoan.sReqCreditLifeIns = srcDataLoan.sReqCreditLifeIns;
                    destDataLoan.sReqCreditDisabilityIns = srcDataLoan.sReqCreditDisabilityIns;
                    destDataLoan.sReqPropIns = srcDataLoan.sReqPropIns;
                    destDataLoan.sReqFloodIns = srcDataLoan.sReqFloodIns;
                    destDataLoan.sIfPurchInsFrCreditor = srcDataLoan.sIfPurchInsFrCreditor;
                    destDataLoan.sIfPurchPropInsFrCreditor = srcDataLoan.sIfPurchPropInsFrCreditor;
                    destDataLoan.sIfPurchFloodInsFrCreditor = srcDataLoan.sIfPurchFloodInsFrCreditor;
                    destDataLoan.sInsFrCreditorAmt = srcDataLoan.sInsFrCreditorAmt;
                    destDataLoan.sSecurityPurch = srcDataLoan.sSecurityPurch;
                    destDataLoan.sSecurityCurrentOwn = srcDataLoan.sSecurityCurrentOwn;
                    destDataLoan.sAsteriskEstimate = srcDataLoan.sAsteriskEstimate;
                    destDataLoan.sOnlyLatePmtEstimate = srcDataLoan.sOnlyLatePmtEstimate;
                    destDataLoan.sLenderCreditCalculationMethodT = srcDataLoan.sLenderCreditCalculationMethodT;
                    destDataLoan.sGfeCreditLenderPaidItemT = srcDataLoan.sGfeCreditLenderPaidItemT;
                    destDataLoan.sLenderCustomCredit1Description = srcDataLoan.sLenderCustomCredit1Description;
                    destDataLoan.sLenderCustomCredit2Description = srcDataLoan.sLenderCustomCredit2Description;
                    destDataLoan.sLenderCustomCredit1Amount_rep = srcDataLoan.sLenderCustomCredit1Amount_rep;
                    destDataLoan.sLenderCustomCredit2Amount_rep = srcDataLoan.sLenderCustomCredit2Amount_rep;
                    destDataLoan.sToleranceCureCalculationT = srcDataLoan.sToleranceCureCalculationT;
                    destDataLoan.sLDiscntBaseT = srcDataLoan.sLDiscntBaseT;
                    destDataLoan.sLenderCreditMaxT = srcDataLoan.sLenderCreditMaxT;
                    destDataLoan.sLenderCreditMaxAmt_Neg_rep = srcDataLoan.sLenderCreditMaxAmt_Neg_rep;
                    destDataLoan.sLenderPaidFeeDiscloseLocationT = srcDataLoan.sLenderPaidFeeDiscloseLocationT;
                    destDataLoan.sLenderGeneralCreditDiscloseLocationT = srcDataLoan.sLenderGeneralCreditDiscloseLocationT;
                    destDataLoan.sLenderCustomCredit1DiscloseLocationT = srcDataLoan.sLenderCustomCredit1DiscloseLocationT;
                    destDataLoan.sLenderCustomCredit2DiscloseLocationT = srcDataLoan.sLenderCustomCredit2DiscloseLocationT;
                    destDataLoan.sAggEscrowCalcModeT = srcDataLoan.sAggEscrowCalcModeT;
                    destDataLoan.sCustomaryEscrowImpoundsCalcMinT = srcDataLoan.sCustomaryEscrowImpoundsCalcMinT;
                    destDataLoan.sIsRequireFeesFromDropDown = srcDataLoan.sIsRequireFeesFromDropDown;
                    destDataLoan.sGfeIsTPOTransaction = srcDataLoan.sGfeIsTPOTransaction;
                    destDataLoan.sTRIDLoanEstimateLenderIntendsToServiceLoan = srcDataLoan.sTRIDLoanEstimateLenderIntendsToServiceLoan;
                    destDataLoan.sTRIDLoanEstimateNoteIRAvailTillDTimeZoneT = srcDataLoan.sTRIDLoanEstimateNoteIRAvailTillDTimeZoneT;
                    destDataLoan.sTRIDLoanEstimateSetLockStatusMethodT = srcDataLoan.sTRIDLoanEstimateSetLockStatusMethodT;
                    destDataLoan.sTRIDLoanEstimateNoteIRAvailTillD = srcDataLoan.sTRIDLoanEstimateNoteIRAvailTillD;
                    destDataLoan.sTRIDLoanEstimateNoteIRAvailTillDTime = srcDataLoan.sTRIDLoanEstimateNoteIRAvailTillDTime;
                    destDataLoan.sGfeEstScAvailTillDTimeZoneT = srcDataLoan.sGfeEstScAvailTillDTimeZoneT;
                    destDataLoan.sGfeEstScAvailTillD = srcDataLoan.sGfeEstScAvailTillD;
                    destDataLoan.sIsPrintTimeForsGfeEstScAvailTillD = srcDataLoan.sIsPrintTimeForsGfeEstScAvailTillD;
                    destDataLoan.sGfeEstScAvailTillDLckd = srcDataLoan.sGfeEstScAvailTillDLckd;
                    destDataLoan.sLoanEstScAvailTillDLckd = srcDataLoan.sLoanEstScAvailTillDLckd;
                    destDataLoan.sLoanEstScAvailTillD = srcDataLoan.sLoanEstScAvailTillD;
                    destDataLoan.sIsPrintTimeForsLoanEstScAvailTillD = srcDataLoan.sIsPrintTimeForsLoanEstScAvailTillD;
                    destDataLoan.sLoanEstScAvailTillDTimeZoneT = srcDataLoan.sLoanEstScAvailTillDTimeZoneT;

                    destDataLoan.sLoanBeingRefinancedAmortizationT = srcDataLoan.sLoanBeingRefinancedAmortizationT;
                    destDataLoan.sLoanBeingRefinancedLienPosT = srcDataLoan.sLoanBeingRefinancedLienPosT;
                    destDataLoan.sLoanBeingRefinancedInterestRate = srcDataLoan.sLoanBeingRefinancedInterestRate;
                    destDataLoan.sLoanBeingRefinancedRemainingTerm = srcDataLoan.sLoanBeingRefinancedRemainingTerm;
                    destDataLoan.sLoanBeingRefinancedTotalOfPayments = srcDataLoan.sLoanBeingRefinancedTotalOfPayments;
                    destDataLoan.sLoanBeingRefinancedLtvR = srcDataLoan.sLoanBeingRefinancedLtvR;
                    destDataLoan.sLoanBeingRefinancedUsedToConstructAlterRepair = srcDataLoan.sLoanBeingRefinancedUsedToConstructAlterRepair;

                    destDataLoan.CopyRespaFirstEnteredDates(srcDataLoan);
                    if (brokerDb.IsEditLeadsInFullLoanEditor)
                    {
                        destDataLoan.sRespa6D = srcDataLoan.sRespa6D;
                        destDataLoan.sRespa6DLckd = srcDataLoan.sRespa6DLckd;
                        destDataLoan.sRespaBorrowerNameCollectedD = srcDataLoan.sRespaBorrowerNameCollectedD;
                        destDataLoan.sRespaBorrowerNameCollectedDLckd = srcDataLoan.sRespaBorrowerNameCollectedDLckd;
                        destDataLoan.sRespaBorrowerSsnCollectedD = srcDataLoan.sRespaBorrowerSsnCollectedD;
                        destDataLoan.sRespaBorrowerSsnCollectedDLckd = srcDataLoan.sRespaBorrowerSsnCollectedDLckd;
                        destDataLoan.sRespaIncomeCollectedD = srcDataLoan.sRespaIncomeCollectedD;
                        destDataLoan.sRespaIncomeCollectedDLckd = srcDataLoan.sRespaIncomeCollectedDLckd;
                        destDataLoan.sRespaPropValueCollectedD = srcDataLoan.sRespaPropValueCollectedD;
                        destDataLoan.sRespaPropValueCollectedDLckd = srcDataLoan.sRespaPropValueCollectedDLckd;
                        destDataLoan.sRespaPropAddressCollectedD = srcDataLoan.sRespaPropAddressCollectedD;
                        destDataLoan.sRespaPropAddressCollectedDLckd = srcDataLoan.sRespaPropAddressCollectedDLckd;
                        destDataLoan.sRespaLoanAmountCollectedD = srcDataLoan.sRespaLoanAmountCollectedD;
                        destDataLoan.sRespaLoanAmountCollectedDLckd = srcDataLoan.sRespaLoanAmountCollectedDLckd;

                        destDataLoan.sMortgageServicingRightsSalesDate = srcDataLoan.sMortgageServicingRightsSalesDate;
                        destDataLoan.sMortgageServicingRightsTransferDate = srcDataLoan.sMortgageServicingRightsTransferDate;
                        destDataLoan.sMortgageServicingRightsHolderFirstPaymentDueDate = srcDataLoan.sMortgageServicingRightsHolderFirstPaymentDueDate;
                    }

                    // OPM 467676 - Copy construction loan fields.
                    if (srcDataLoan.sIsConstructionLoan)
                    {
                        CopyConstructionFields(destDataLoan, srcDataLoan);
                    }

                    // Copy isManuallySetThirdPartyAffiliateProps with a bypass due to permission stuff.
                    bool oldBypass = destDataLoan.LeadConversionBypassForsIsManuallySetThirdPartyAffiliateProps;
                    destDataLoan.LeadConversionBypassForsIsManuallySetThirdPartyAffiliateProps = true;
                    destDataLoan.sIsManuallySetThirdPartyAffiliateProps = srcDataLoan.sIsManuallySetThirdPartyAffiliateProps;
                    destDataLoan.LeadConversionBypassForsIsManuallySetThirdPartyAffiliateProps = oldBypass;

                    IPreparerFields srcLender = srcDataLoan.PreparerCollection.GetPreparerOfForm(E_PreparerFormT.TRIDClosingCosts_Lender, E_AgentRoleT.Lender, true);

                    destDataLoan.PreparerCollection.AddCopyOfPreparer(srcLender);

                    IPreparerFields srcLoanOfficer = srcDataLoan.PreparerCollection.GetPreparerOfForm(E_PreparerFormT.TRIDClosingCosts_LoanOfficer, E_AgentRoleT.LoanOfficer, true);

                    destDataLoan.PreparerCollection.AddCopyOfPreparer(srcLoanOfficer);

                    IPreparerFields srcMb = srcDataLoan.PreparerCollection.GetPreparerOfForm(E_PreparerFormT.TRIDClosingCosts_MortgageBroker, E_AgentRoleT.Broker, true);

                    destDataLoan.PreparerCollection.AddCopyOfPreparer(srcMb);

                    destDataLoan.CopysHousingExpenseJson(srcDataLoan.GetsHousingExpenseJson());
                    destDataLoan.CopysClosingCostSetJson(srcDataLoan.GetsClosingCostSetJson());

                    E_PreparerFormT[] preparerList = { E_PreparerFormT.CreditEquifax, E_PreparerFormT.CreditExperian, E_PreparerFormT.CreditTransUnion };
                    foreach (E_PreparerFormT type in preparerList)
                    {
                        IPreparerFields destPreparer = destDataLoan.GetPreparerOfForm(type, E_ReturnOptionIfNotExist.CreateNew);
                        IPreparerFields srcPreparer = srcDataLoan.GetPreparerOfForm(type, E_ReturnOptionIfNotExist.CreateNew);

                        destPreparer.CompanyName = srcPreparer.CompanyName;
                        destPreparer.StreetAddr = srcPreparer.StreetAddr;
                        destPreparer.City = srcPreparer.City;
                        destPreparer.State = srcPreparer.State;
                        destPreparer.Zip = srcPreparer.Zip;
                        destPreparer.PhoneOfCompany = srcPreparer.PhoneOfCompany;
                        destPreparer.Update();
                    }

                    // Duplicate Credit Report.
                    CLoanFileCreator.DuplicateCreditReport(principal.BrokerId, principal.UserId, srcDataLoan.sLId, destDataLoan.sLId);

                    // Duplicate tasks.
                    CLoanFileCreator.DuplicateLoanTasks(
                        "Starting new task.", 
                        principal.BrokerId,
                        srcDataLoan.sLId, 
                        destDataLoan.sLId, 
                        creator.LoanName, 
                        principal.UserId, 
                        principal.LoginNm, 
                        creator.CreatedOn);

                    // 8/30/2013 gf - OPM 136559 Duplicate new task system tasks.
                    // Don't need to wait until role assignments are saved because 
                    // these are not task templates.
                    Task.DuplicateTasksToLoan(
                        principal.BrokerId,
                        srcDataLoan.sLId,
                        destDataLoan.sLId,
                        principal.EmployeeId,
                        false,
                        false /* Don't check permission of src file */,
                        false /* Don't copy condition sort order from lead file */);

                    // 8/19/15 gf - Per opm 222507, we need to copy the branch from the source lead.
                    destDataLoan.AssignBranch(srcDataLoan.sBranchId);

                    destDataLoan.Save();
                    creator.CommitFileCreation(false);

                    // 8/19/15 gf - We need to suppress branch assignment to make sure we maintain
                    // the branch id from the source file.
                    CLoanFileCreator.WriteInitialOfficialAgentList(destDataLoan.sLId, BrokerDB.RetrieveById(principal.BrokerId), BranchAssignmentOption.Suppress);

                    // 7/22/2015 - je - OPM 217312 - Copy linked loan info (Needs to be done before loan is declared invalid).
                    if (srcDataLoan.sLinkedLoanInfo.IsLoanLinked)
                    {
                        Tools.LinkLoans(principal.BrokerId, destDataLoan.sLId, srcDataLoan.sLinkedLoanInfo.LinkedLId);
                    }

                    string refNum = srcDataLoan.sLRefNm; // Store reference number before deleting.

                    Tools.SystemDeclareLoanFileInvalid(principal, parameters.LeadId, false, false, isLeadToLoan: true); // Delete the lead file.

                    // Update values that cannot be updated until after source loan is invalidated (OPM 243181, OPM 244087, OPM 246033).
                    destDataLoan = new CFullAccessPageData(loanId, new string[] { "sLNm", "sLRefNm", "sfSetReferenceNumber", "sMersMin" });
                    destDataLoan.ByPassFieldSecurityCheck = true;
                    destDataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                    if (setNameAfterSrcDeleted)
                    {
                        destDataLoan.SetsLNmWithPermissionBypass(loanName);
                    }

                    destDataLoan.SetReferenceNumber(refNum);

                    // OPM 273220 - Blank out MERS MIN if MERS generation uses loan name and loan name is produced by removing prefix.
                    if (parameters.RemoveLeadPrefix && brokerDb.IsAutoGenerateMersMin && !string.IsNullOrEmpty(brokerDb.MersFormat))
                    {
                        string mersMin;
                        string mersErrors;
                        if (!Tools.TryGenerateMersMin(principal, destDataLoan.sLNm, out mersMin, out mersErrors))
                        {
                            mersMin = string.Empty;
                            results.MersErrors = mersErrors;
                        }

                        destDataLoan.DisableMersMinCheck = true;
                        destDataLoan.sMersMin = mersMin;
                        destDataLoan.DisableMersMinCheck = false;
                    }

                    destDataLoan.Save();

                    // Copy Edocs 
                    foreach (Tuple<Guid, Guid> appMapping in appMap)
                    {
                        SqlParameter[] sqlParameters =
                            {
                                new SqlParameter("@BrokerId", brokerDb.BrokerID),
                                new SqlParameter("@src_sLId", parameters.LeadId),
                                new SqlParameter("@src_aAppId", appMapping.Item1),
                                new SqlParameter("@dst_sLId", loanId),
                                new SqlParameter("@dst_aAppId", appMapping.Item2)
                            };
                        StoredProcedureHelper.ExecuteNonQuery(brokerDb.BrokerID, "EDOCS_MoveDocs", 1, sqlParameters);
                    }

                    // Give Consumer portal usesr access 
                    foreach (var item in ConsumerPortalUser.GetConsumerPortalUsersWithAccessToLoan(principal.BrokerId, parameters.LeadId))
                    {
                        ConsumerPortalUser.LinkLoan(principal.BrokerId, item.Item1.Id, loanId);
                    }

                    foreach (var entry in appMap)
                    {
                        ConsumerActionItem.MoveRequestAndResponse(brokerDb.BrokerID, parameters.LeadId, entry.Item1, loanId, entry.Item2);
                        PendingDocumentRequest.MoveExistingToNewLoan(brokerDb.BrokerID, parameters.LeadId, loanId, entry.Item1, entry.Item2);
                    }

                    results.Action = "Close";

                    if (CanReadConvertedLoan(principal, loanId))
                    {
                        results.ConvertedLoanId = loanId;
                    }

                    oldLeadNum = srcDataLoan.sLNm;
                    newLoanNum = destDataLoan.sLNm;
                    loanToRecordAudit = destDataLoan.sLId;
                }

                LoanCreationFromLeadAuditItem audit = new LoanCreationFromLeadAuditItem(principal, oldLeadNum, newLoanNum);
                AuditManager.RecordAudit(loanToRecordAudit, audit);
                return results;
            }
            catch (CBaseException exc)
            {
                string userMsg = string.Empty;
                if (exc.DeveloperMessage.StartsWith("You don't have permission to delete this loan."))
                {
                    userMsg = "Conversion successful.  The new loan has been created, but the original" + Environment.NewLine + "lead could not be deleted because you do not have permission to delete files.";
                    CBaseException exception = new CBaseException(userMsg, exc);
                    exception.IsEmailDeveloper = false;
                    throw exception;
                }
                else
                {
                    userMsg = ErrorMessages.UnableToConvertLead + Environment.NewLine + "Reason: " + exc.UserMessage;
                    if (exc.UserMessage.Equals(JsMessages.LoanInfo_DuplicateLoanNumber))
                    {
                        CBaseException exception = new CBaseException(userMsg, exc);
                        exception.IsEmailDeveloper = false;
                        throw exception;
                    }
                    else
                    {
                        var exception = new CBaseException(userMsg, exc);
                        exception.IsEmailDeveloper = exc.IsEmailDeveloper;
                        throw exception;
                    }
                }
            }
            catch (Exception exc)
            {
                throw new CBaseException(ErrorMessages.UnableToConvertLead, exc);
            }
        }

        /// <summary>
        /// The PML implementation for converting a lead to a loan. Note that this may convert the lead
        /// to a lead if in QuickPricer 2.0 mode.
        /// </summary>
        /// <param name="principal">The user performing the conversion.</param>
        /// <param name="parameters">The parameters for the lead conversion.</param>
        /// <returns>
        /// The conversion results, which may include the ID of the converted loan and any messages or
        /// errors related to the conversion.
        /// </returns>
        public static LeadConversionResults ConvertLeadToLoan_Pml(AbstractUserPrincipal principal, LeadConversionParameters parameters)
        {
            string denialMessage;
            if (!VerifyLeadConversionPermission(principal, parameters, out denialMessage))
            {
                var exception = new CBaseException(denialMessage, denialMessage);
                exception.IsEmailDeveloper = false;
                throw exception;
            }

            var broker = principal.BrokerDB;
            var results = new LeadConversionResults();

            FixupParameters(parameters, broker);

            var reasonCantRunConversion = ReasonCantRunConvertLeadToLoanImpl(principal, broker, parameters);
            if (reasonCantRunConversion != null)
            {
                Tools.LogError(reasonCantRunConversion);
                return results;
            }

            string oldLeadNum = string.Empty;
            Guid loanToRecordAuditIn;

            if (!parameters.CreateFromTemplate)
            {
                try
                {
                    // 1/23/2006 dd - Only Change status of lead to loan.
                    CPageData dataLoan = null;
                    var maxNamingTries = 5;
                    for (int i = 1; i <= maxNamingTries; i++)
                    {
                        try
                        {
                            dataLoan = new CLeadAssignmentData(parameters.LeadId);
                            dataLoan.AllowSaveWhileQP2Sandboxed = true;
                            dataLoan.ByPassFieldSecurityCheck = true;
                            dataLoan.InitSave(parameters.FileVersion);

                            if (dataLoan.sIsConsumerPortalCreatedButNotSubmitted
                                && !dataLoan.sConsumerPortalVerbalSubmissionD.IsValid
                                && !parameters.ConvertToLead)
                            {
                                results.Message = "Files created in consumer portal must have a submission date or a verbal submission date to be converted.";
                                Tools.LogError(results.Message);
                                return results;
                            }

                            if (!parameters.ConvertFromQp2File && E_sLoanFileT.QuickPricer2Sandboxed == dataLoan.sLoanFileT)
                            {
                                Tools.LogBug("Somehow PML had convertFromQP2File set to false but the loan had sLoanFileT QuickPricer2Sandboxed.  Setting convertFromQP2File to true.");
                                parameters.ConvertFromQp2File = true;

                                reasonCantRunConversion = ReasonCantRunConvertLeadToLoanImpl(principal, broker, parameters);
                                if (reasonCantRunConversion != null)
                                {
                                    Tools.LogError(reasonCantRunConversion);
                                    return results;
                                }
                            }

                            oldLeadNum = dataLoan.sLNm;

                            if (parameters.ConvertFromQp2File)
                            {
                                CPageData srcDataLoan = dataLoan;

                                // Make sure this a QP loan.
                                if (E_sLoanFileT.QuickPricer2Sandboxed != srcDataLoan.sLoanFileT)
                                {
                                    results.Message = "An error occurred.  Please try again or contact us.";
                                    var devMsg = results.Message + string.Format(
                                        " user {0} attempted to convertFromQP2File but the file {1} was not a QP2 file.",
                                        principal.UserId,
                                        srcDataLoan.sLId);
                                    Tools.LogError(devMsg);
                                    return results;
                                }

                                // Delete old audits - we only need to do this once.
                                if (i == 1)
                                {
                                    // don't want any remnant audits on the file.
                                    AuditManager.MoveToPermanentStorage(parameters.LeadId);
                                    var auditEntryInfo = Tools.GetFileDBEntryInfoForLoan(E_FileDBKeyType.Normal_Loan_audit, parameters.LeadId);
                                    FileDBTools.Delete(auditEntryInfo.FileDBType, auditEntryInfo.Key);
                                    var backupAuditEntryInfo = Tools.GetFileDBEntryInfoForLoan(E_FileDBKeyType.Temp_Loan_backup_audit, parameters.LeadId);
                                    FileDBTools.Delete(backupAuditEntryInfo.FileDBType, backupAuditEntryInfo.Key);
                                }

                                // Duplicate Loan
                                CLoanFileCreator creator = CLoanFileCreator.GetCreator(principal, srcDataLoan.sBranchId, E_LoanCreationSource.LeadConvertToLoanUsingTemplate);
                                Guid id = creator.DuplicateQP2SandboxFileToLoan(parameters.LeadId, parameters.ConvertToLead);

                                dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(id, typeof(LeadConversion));
                                dataLoan.ByPassFieldSecurityCheck = true;
                                dataLoan.InitSave(parameters.FileVersion);

                                // Need to set loan status if not converting to lead, as DuplicateLoanFile SP copies over Lead status from source.
                                if (!parameters.ConvertToLead)
                                {
                                    dataLoan.sStatusLckd = false;
                                    dataLoan.sStatusT = E_sStatusT.Loan_Open;
                                    dataLoan.sOpenedD_rep = DateTime.Now.ToShortDateString();
                                }

                                dataLoan.sFHALenderIdCode = Tools.GetFhaLenderIDFromLoanBranchOrBroker(dataLoan.sBrokerId, dataLoan.sLId, dataLoan.sBranchId);

                                // OPM 245460 - Reset VA values when converting QP loan.
                                for (int appNum = 0; appNum < srcDataLoan.nApps; ++appNum)
                                {
                                    CAppData dataApp = dataLoan.GetAppData(appNum);
                                    dataApp.aIsBVAElig = false;
                                    dataApp.aBVServiceT = E_aVServiceT.Blank;
                                    dataApp.aBVEnt = E_aVEnt.Blank;
                                }

                                dataLoan.Save();

                                creator.CommitFileCreation(false);

                                Tools.CreateLoanCreationAudit(principal, dataLoan.sSrcsLId, dataLoan.sLId, E_LoanCreationSource.FromTemplate, Tools.IsStatusLead(dataLoan.sStatusT));

                                // putting this after loan file conversion.  The order is important for the cleanup process.
                                QuickPricer2LoanPoolManager.RemoveFromPool(principal, parameters.LeadId);

                                // Clean up old QP loan.
                                Tools.SystemDeclareLoanFileInvalid(principal, parameters.LeadId, false, false, isLeadToLoan: true); // Delete the lead file.
                            }
                            else
                            {
                                // If here, we are not converting from QP.
                                // NOTE: convertToLead should be false here as it only applies to QP loans (No point converting Lead to Lead), so no need to check it.
                                dataLoan.sStatusLckd = false;
                                dataLoan.sStatusT = E_sStatusT.Loan_Open;
                                dataLoan.sOpenedD_rep = DateTime.Now.ToShortDateString();

                                string mersMin = string.Empty;
                                if (parameters.RemoveLeadPrefix)
                                {
                                    if (dataLoan.sLNm.ToLower().StartsWith("lead"))
                                    {
                                        dataLoan.SetsLNmWithPermissionBypass(dataLoan.sLNm.Substring(4));
                                    }

                                    if (broker.IsAutoGenerateMersMin)
                                    {
                                        string mersErrors;
                                        if (!Tools.TryGenerateMersMin(LendersOffice.Security.PrincipalFactory.CurrentPrincipal, dataLoan.sLNm, out mersMin, out mersErrors))
                                        {
                                            mersMin = string.Empty;
                                        }

                                        if (!string.IsNullOrWhiteSpace(mersErrors))
                                        {
                                            results.MersErrors = mersErrors;
                                        }
                                    }
                                }
                                else
                                {
                                    CLoanFileNamer loanNamer = new CLoanFileNamer();
                                    string referenceNumber = string.Empty;
                                    string generatedLoanNumber = loanNamer.GetLoanAutoName(
                                                                    principal.BrokerId, 
                                                                    dataLoan.sBranchId, 
                                                                    useLeadNumbering: false, 
                                                                    useTestNumbering: false, 
                                                                    enableMersGeneration: true, 
                                                                    mersMin: out mersMin, 
                                                                    refNm: out referenceNumber);
                                    dataLoan.SetsLNmWithPermissionBypass(generatedLoanNumber);
                                }

                                dataLoan.DisableMersMinCheck = true;
                                dataLoan.sMersMin = mersMin; // OPM 246033 - sets sMersMin to blank if generation fails.
                                dataLoan.DisableMersMinCheck = false;

                                dataLoan.sFHALenderIdCode = Tools.GetFhaLenderIDFromLoanBranchOrBroker(dataLoan.sBrokerId, dataLoan.sLId, dataLoan.sBranchId);
                                dataLoan.sLegalEntityIdentifier = dataLoan.Branch.LegalEntityIdentifier;
                                dataLoan.Save();

                                CLoanFileCreator.WriteInitialOfficialAgentList(dataLoan.sLId, broker, BranchAssignmentOption.Allow);
                            }

                            break;
                        }
                        catch (CLoanNumberDuplicateException e)
                        {
                            if (i == maxNamingTries)
                            {
                                Tools.LogErrorWithCriticalTracking(e);
                                throw;
                            }
                        }
                    }

                    // The code that follows needs to run for both QP and non-QP conversion.
                    // Note: This check can be done of source data. Consider doing first.
                    results.ConvertedLoanNumber = dataLoan.sLNm;
                    loanToRecordAuditIn = dataLoan.sLId;
                    LoanCreationFromLeadAuditItem audit = new LoanCreationFromLeadAuditItem(LendersOffice.Security.PrincipalFactory.CurrentPrincipal, oldLeadNum, results.ConvertedLoanNumber);
                    AuditManager.RecordAudit(loanToRecordAuditIn, audit);

                    if (CanReadConvertedLoan(principal, dataLoan.sLId))
                    {
                        results.ConvertedLoanId = dataLoan.sLId;
                        return results;
                    }

                    results.Message = "You do not have permission to access the converted loan.";
                    return results;
                }
                catch (CBaseException e)
                {
                    results.Message = e.UserMessage;
                    return results;
                }
            }

            // If we are here, then we are converting lead to loan with template.
            Guid? convertedLoanId = null;
            try
            {
                // Otherwise we're not creating the loan from a template
                // 1) Create new loan from Template.
                // 2) Copy visible data to new loan.
                // 3) Delete the lead file.
                List<Tuple<Guid, Guid>> appMap = new List<Tuple<Guid, Guid>>();

                CPageData srcDataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(parameters.LeadId, typeof(LeadConversion));
                srcDataLoan.AllowLoadWhileQP2Sandboxed = true;
                srcDataLoan.InitLoad();

                if (parameters.ConvertFromQp2File)
                {
                    if (E_sLoanFileT.QuickPricer2Sandboxed != srcDataLoan.sLoanFileT)
                    {
                        results.Message = "An error occurred.  Please try again or contact us.";
                        var devMsg = results.Message + string.Format(
                            " user {0} attempted to convertFromQP2File but the file {1} was not a QP2 file.",
                            principal.UserId,
                            srcDataLoan.sLId);
                        Tools.LogError(devMsg);
                        return results;
                    }
                }

                if (!parameters.ConvertFromQp2File && E_sLoanFileT.QuickPricer2Sandboxed == srcDataLoan.sLoanFileT)
                {
                    Tools.LogBug("Somehow PML had convertFromQP2File set to false but the loan had sLoanFileT QuickPricer2Sandboxed.  Setting convertFromQP2File to true.");
                    parameters.ConvertFromQp2File = true;

                    reasonCantRunConversion = ReasonCantRunConvertLeadToLoanImpl(principal, broker, parameters);
                    if (reasonCantRunConversion != null)
                    {
                        Tools.LogError(reasonCantRunConversion);
                        return results;
                    }
                }

                if (srcDataLoan.sIsConsumerPortalCreatedButNotSubmitted
                        && !srcDataLoan.sConsumerPortalVerbalSubmissionD.IsValid
                        && !parameters.ConvertToLead)
                {
                    results.Message = "Files created in consumer portal must have a submission date or a verbal submission date to be converted.";
                    Tools.LogError(results.Message);
                    return results;
                }

                var defaults = new Dictionary<string, object>()
                {
                    ["sUse2016ComboAndProrationUpdatesTo1003Details"] = srcDataLoan.sUse2016ComboAndProrationUpdatesTo1003Details,
                    [nameof(CPageBase.sLegalEntityIdentifier)] = broker.LegalEntityIdentifier
                };

                CLoanFileCreator creator = CLoanFileCreator.GetCreator(principal, srcDataLoan.sBranchId, LendersOffice.Audit.E_LoanCreationSource.LeadConvertToLoanUsingTemplate);
                convertedLoanId = creator.BeginCreateNewLoanFromLead(sourceLeadId: parameters.TemplateId, defaultValues: defaults, isLead: parameters.ConvertToLead);

                CPageData destDataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(convertedLoanId.Value, typeof(LeadConversion));

                // This is a system action--we can ignore user-permission
                srcDataLoan.ByPassFieldSecurityCheck = true;
                destDataLoan.ByPassFieldSecurityCheck = true;

                // OPM 109480 gf 1/21/13
                // The newly created loan will already have 1 application.
                // Add an app for each additional app in the source file.
                for (int appNum = 1; appNum < srcDataLoan.nApps; appNum++)
                {
                    destDataLoan.AddNewApp();
                }

                // Init save after adding the applications to the file.
                destDataLoan.InitSave(parameters.FileVersion);

                // Upconvert underlying lead file if created loan will be a higher version.
                srcDataLoan.EnsureMigratedToAtLeastUladCollectionVersion(destDataLoan.sBorrowerApplicationCollectionT);

                if (parameters.ConvertFromQp2File)
                {
                    // TODO: Is this check necessary? There should be no way that a QP loan can be created from a call to BeginCreateFileFromTemplate.
                    if (E_sLoanFileT.QuickPricer2Sandboxed == destDataLoan.sLoanFileT)
                    {
                        destDataLoan.sLoanFileT = E_sLoanFileT.Loan;
                    }

                    // don't want any remnant audits on the file.
                    AuditManager.MoveToPermanentStorage(parameters.LeadId);
                    var auditEntryInfo = Tools.GetFileDBEntryInfoForLoan(E_FileDBKeyType.Normal_Loan_audit, parameters.LeadId);
                    FileDBTools.Delete(auditEntryInfo.FileDBType, auditEntryInfo.Key);
                    var backupAuditEntryInfo = Tools.GetFileDBEntryInfoForLoan(E_FileDBKeyType.Temp_Loan_backup_audit, parameters.LeadId);
                    FileDBTools.Delete(backupAuditEntryInfo.FileDBType, backupAuditEntryInfo.Key);
                }

                // OPM 243181 - In order to keep the same name after conversion, we cannot
                // set destination loan name until source loan is deleted/invalidated.
                bool setNameAfterSrcDeleted = false;
                string loanName = srcDataLoan.sLNm;
                if (parameters.RemoveLeadPrefix)
                {
                    if (loanName.ToLower().StartsWith("lead"))
                    {
                        destDataLoan.SetsLNmWithPermissionBypass(srcDataLoan.sLNm.Substring(4));
                    }
                    else if (broker.IsForceLeadToUseLoanCounter)
                    {
                        setNameAfterSrcDeleted = true;
                    }
                }

                // 08/14/2015 - je - OPM 222507 - Set disclosure type to match that of the source.
                destDataLoan.sDisclosureRegulationTLckd = srcDataLoan.sDisclosureRegulationTLckd;
                destDataLoan.sDisclosureRegulationT = srcDataLoan.sDisclosureRegulationT;

                destDataLoan.sGseTargetApplicationTLckd = srcDataLoan.sGseTargetApplicationTLckd;
                destDataLoan.sGseTargetApplicationT = srcDataLoan.sGseTargetApplicationT;

                destDataLoan.sAlwaysRequireAllBorrowersToReceiveClosingDisclosure = srcDataLoan.sAlwaysRequireAllBorrowersToReceiveClosingDisclosure;

                // If destination disclosure type is not locked and evaluates to TRID 2015 (sOpenedD would be the
                // open date of the new loan) and the source closing cost fee version type is Legacy,
                // then migrate the source lead before continuing.
                if (destDataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID && srcDataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
                {
                    // Migrate loan
                    ClosingCostFeeMigration.Migrate(principal, parameters.LeadId, destDataLoan.sClosingCostFeeVersionT);

                    // Reload source
                    srcDataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(parameters.LeadId, typeof(LeadConversion));
                    srcDataLoan.AllowLoadWhileQP2Sandboxed = true;
                    srcDataLoan.InitLoad();

                    srcDataLoan.ByPassFieldSecurityCheck = true;
                }

                // je - 10/03/15 - If destination disclosure type evaluates to TRID 2015 but the source disclosure type does not,
                // then lock source type to TRID 2015.
                // NOTE: Source data loan would have already been migrated by previous if block, if it had not been migrated previously.
                if (destDataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID && srcDataLoan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID)
                {
                    srcDataLoan.sDisclosureRegulationTLckd = true;
                    srcDataLoan.sDisclosureRegulationT = E_sDisclosureRegulationT.TRID;
                }

                // OPM 406379 ejm - Want to grab the product filter from the lead. 
                if (srcDataLoan.sRawSelectedProductCodeFilter.Any())
                {
                    destDataLoan.sRawSelectedProductCodeFilter = srcDataLoan.sRawSelectedProductCodeFilter;
                }

                // OPM 236635 ejm - We want to copy the migration audit and loan version from the lead, rather than from the template.
                destDataLoan.sLoanVersionT = srcDataLoan.sLoanVersionT;
                destDataLoan.ManuallySetLoanDataMigrationAuditHistory(srcDataLoan.sLoanDataMigrationAuditHistory);

                // 01/22/2015 ejm - opm 198068
                // Set the new housing expense bit to match that of the source. 
                destDataLoan.sIsHousingExpenseMigrated = srcDataLoan.sIsHousingExpenseMigrated;
                destDataLoan.sClosingCostFeeVersionT = srcDataLoan.sClosingCostFeeVersionT;

                // OPM 238031 ejm - New HMDA fields
                destDataLoan.sHmdaLenderHasPreapprovalProgram = srcDataLoan.sHmdaLenderHasPreapprovalProgram;
                destDataLoan.sHmdaBorrowerRequestedPreapproval = srcDataLoan.sHmdaBorrowerRequestedPreapproval;
                destDataLoan.sHmdaPreapprovalTLckd = srcDataLoan.sHmdaPreapprovalTLckd;

                destDataLoan.sHmdaLoanAmount = srcDataLoan.sHmdaLoanAmount;
                destDataLoan.sHmdaLoanAmountLckd = srcDataLoan.sHmdaLoanAmountLckd;

                // 6/28/2016 ejm - opm 74472 
                // Added new rate floor fields.
                destDataLoan.sRAdjFloorAddR = srcDataLoan.sRAdjFloorAddR;
                destDataLoan.sRAdjFloorCalcT = srcDataLoan.sRAdjFloorCalcT;

                destDataLoan.sLeadSrcDesc = srcDataLoan.sLeadSrcDesc;

                // Massive copying from template along these lines
                // Copy applications
                // OPM 109480 gf 1/21/13
                for (int appNum = 0; appNum < srcDataLoan.nApps; ++appNum)
                {
                    CAppData srcDataApp = srcDataLoan.GetAppData(appNum);
                    CAppData destDataApp = destDataLoan.GetAppData(appNum);

                    appMap.Add(Tuple.Create(srcDataApp.aAppId, destDataApp.aAppId));

                    destDataApp.aBIsSurvivingSpouseOfVeteran = srcDataApp.aBIsSurvivingSpouseOfVeteran;
                    destDataApp.aCIsSurvivingSpouseOfVeteran = srcDataApp.aCIsSurvivingSpouseOfVeteran;

                    destDataApp.aBHasHousingHist = srcDataApp.aBHasHousingHist;
                    destDataApp.aBTotalScoreIsFthb = srcDataApp.aBTotalScoreIsFthb;

                    // OPM 203382 ejm 3/17/2015 - Add fields for big lead/loan info page
                    destDataApp.aVaEntitleCode = srcDataApp.aVaEntitleCode;
                    destDataApp.aVaEntitleAmt = srcDataApp.aVaEntitleAmt;
                    destDataApp.aVaServiceBranchT = srcDataApp.aVaServiceBranchT;
                    destDataApp.aVaMilitaryStatT = srcDataApp.aVaMilitaryStatT;

                    destDataApp.aBFirstNm = srcDataApp.aBFirstNm;
                    destDataApp.aBPreferredNm = srcDataApp.aBPreferredNm;
                    destDataApp.aBPreferredNmLckd = srcDataApp.aBPreferredNmLckd;
                    destDataApp.aBMidNm = srcDataApp.aBMidNm;
                    destDataApp.aBLastNm = srcDataApp.aBLastNm;
                    destDataApp.aBSuffix = srcDataApp.aBSuffix;
                    destDataApp.aBDob_rep = srcDataApp.aBDob_rep;
                    destDataApp.aBAge = srcDataApp.aBAge;
                    destDataApp.aBSsn = srcDataApp.aBSsn;
                    destDataApp.aBMaritalStatT = srcDataApp.aBMaritalStatT;

                    destDataApp.aBEmail = srcDataApp.aBEmail;
                    destDataApp.aBHPhone = srcDataApp.aBHPhone;
                    destDataApp.aBBusPhone = srcDataApp.aBBusPhone;
                    destDataApp.aBCellPhone = srcDataApp.aBCellPhone;
                    destDataApp.aBFax = srcDataApp.aBFax;

                    destDataApp.aHasCoborrowerData = srcDataApp.aHasCoborrowerData;
                    destDataApp.aCFirstNm = srcDataApp.aCFirstNm;
                    destDataApp.aCPreferredNm = srcDataApp.aCPreferredNm;
                    destDataApp.aCPreferredNmLckd = srcDataApp.aCPreferredNmLckd;
                    destDataApp.aCMidNm = srcDataApp.aCMidNm;
                    destDataApp.aCLastNm = srcDataApp.aCLastNm;
                    destDataApp.aCSuffix = srcDataApp.aCSuffix;
                    destDataApp.aCDob_rep = srcDataApp.aCDob_rep;
                    destDataApp.aCAge = srcDataApp.aCAge;
                    destDataApp.aCSsn = srcDataApp.aCSsn;
                    destDataApp.aCMaritalStatT = srcDataApp.aCMaritalStatT;

                    CopyBorrowerAddressFields(srcDataLoan, srcDataApp, destDataApp);

                    destDataApp.aBDependNum = srcDataApp.aBDependNum;
                    destDataApp.aBSchoolYrs = srcDataApp.aBSchoolYrs;
                    destDataApp.aCDependNum = srcDataApp.aCDependNum;
                    destDataApp.aCSchoolYrs = srcDataApp.aCSchoolYrs;

                    destDataApp.aVaServ1Ssn = srcDataApp.aVaServ1Ssn;
                    destDataApp.aVaServ2Ssn = srcDataApp.aVaServ2Ssn;
                    destDataApp.aVaServ3Ssn = srcDataApp.aVaServ3Ssn;
                    destDataApp.aVaServ4Ssn = srcDataApp.aVaServ4Ssn;

                    if (!string.Equals(srcDataApp.aBSsn, srcDataApp.aB4506TSsnTinEin, StringComparison.Ordinal))
                    {
                        destDataApp.aB4506TSsnTinEin = srcDataApp.aB4506TSsnTinEin;
                    }

                    if (!string.Equals(srcDataApp.aCSsn, srcDataApp.aC4506TSsnTinEin, StringComparison.Ordinal))
                    {
                        destDataApp.aC4506TSsnTinEin = srcDataApp.aC4506TSsnTinEin;
                    }

                    // credit stuff.
                    // SK opm 94181 10/3/2012
                    destDataApp.aCrRd = srcDataApp.aCrRd;
                    destDataApp.aCrOd = srcDataApp.aCrOd;
                    destDataApp.aCrDueD = srcDataApp.aCrDueD;
                    destDataApp.aCrN = srcDataApp.aCrN;

                    destDataApp.aLqiCrRd = srcDataApp.aLqiCrRd;
                    destDataApp.aLqiCrOd = srcDataApp.aLqiCrOd;
                    destDataApp.aLqiCrDueD = srcDataApp.aLqiCrDueD;
                    destDataApp.aLqiCrN = srcDataApp.aLqiCrN;

                    destDataApp.aBCreditAuthorizationD_rep = srcDataApp.aBCreditAuthorizationD_rep;
                    destDataApp.aCCreditAuthorizationD_rep = srcDataApp.aCCreditAuthorizationD_rep;

                    destDataApp.aPresRent_rep = srcDataApp.aPresRent_rep;
                    destDataApp.aPres1stM_rep = srcDataApp.aPres1stM_rep;
                    destDataApp.aPresOFin_rep = srcDataApp.aPresOFin_rep;
                    destDataApp.aPresHazIns_rep = srcDataApp.aPresHazIns_rep;
                    destDataApp.aPresHoAssocDues_rep = srcDataApp.aPresHoAssocDues_rep;
                    destDataApp.aPresRealETx_rep = srcDataApp.aPresRealETx_rep;
                    destDataApp.aPresMIns_rep = srcDataApp.aPresMIns_rep;
                    destDataApp.aPresOHExp_rep = srcDataApp.aPresOHExp_rep;

                    if (!srcDataLoan.sIsIncomeCollectionEnabled)
                    {
                        destDataApp.aBBaseI_rep = srcDataApp.aBBaseI_rep;
                        destDataApp.aCBaseI_rep = srcDataApp.aCBaseI_rep;
                        destDataApp.aBOvertimeI_rep = srcDataApp.aBOvertimeI_rep;
                        destDataApp.aCOvertimeI_rep = srcDataApp.aCOvertimeI_rep;
                        destDataApp.aBBonusesI_rep = srcDataApp.aBBonusesI_rep;
                        destDataApp.aCBonusesI_rep = srcDataApp.aCBonusesI_rep;
                        destDataApp.aBCommisionI_rep = srcDataApp.aBCommisionI_rep;
                        destDataApp.aCCommisionI_rep = srcDataApp.aCCommisionI_rep;
                        destDataApp.aBDividendI_rep = srcDataApp.aBDividendI_rep;
                        destDataApp.aCDividendI_rep = srcDataApp.aCDividendI_rep;
                        destDataApp.aOtherIncomeList = srcDataApp.aOtherIncomeList;
                    }

                    destDataApp.aNetRentI1003Lckd = srcDataApp.aNetRentI1003Lckd;
                    destDataApp.aBNetRentI1003_rep = srcDataApp.aBNetRentI1003_rep;
                    destDataApp.aCNetRentI1003_rep = srcDataApp.aCNetRentI1003_rep;

                    destDataApp.aBDecJudgment = srcDataApp.aBDecJudgment;
                    destDataApp.aCDecJudgment = srcDataApp.aCDecJudgment;
                    destDataApp.aBDecBankrupt = srcDataApp.aBDecBankrupt;
                    destDataApp.aCDecBankrupt = srcDataApp.aCDecBankrupt;
                    destDataApp.aBDecForeclosure = srcDataApp.aBDecForeclosure;
                    destDataApp.aCDecForeclosure = srcDataApp.aCDecForeclosure;
                    destDataApp.aBDecLawsuit = srcDataApp.aBDecLawsuit;
                    destDataApp.aCDecLawsuit = srcDataApp.aCDecLawsuit;
                    destDataApp.aBDecObligated = srcDataApp.aBDecObligated;
                    destDataApp.aCDecObligated = srcDataApp.aCDecObligated;
                    destDataApp.aBDecDelinquent = srcDataApp.aBDecDelinquent;
                    destDataApp.aCDecDelinquent = srcDataApp.aCDecDelinquent;
                    destDataApp.aBDecAlimony = srcDataApp.aBDecAlimony;
                    destDataApp.aCDecAlimony = srcDataApp.aCDecAlimony;
                    destDataApp.aBDecBorrowing = srcDataApp.aBDecBorrowing;
                    destDataApp.aCDecBorrowing = srcDataApp.aCDecBorrowing;
                    destDataApp.aBDecEndorser = srcDataApp.aBDecEndorser;
                    destDataApp.aCDecEndorser = srcDataApp.aCDecEndorser;
                    destDataApp.aBDecCitizen = srcDataApp.aBDecCitizen;
                    destDataApp.aCDecCitizen = srcDataApp.aCDecCitizen;
                    destDataApp.aBDecResidency = srcDataApp.aBDecResidency;
                    destDataApp.aCDecResidency = srcDataApp.aCDecResidency;
                    destDataApp.aBDecOcc = srcDataApp.aBDecOcc;
                    destDataApp.aCDecOcc = srcDataApp.aCDecOcc;
                    destDataApp.aBDecPastOwnership = srcDataApp.aBDecPastOwnership;
                    destDataApp.aCDecPastOwnership = srcDataApp.aCDecPastOwnership;
                    destDataApp.aBDecPastOwnedPropT = srcDataApp.aBDecPastOwnedPropT;
                    destDataApp.aCDecPastOwnedPropT = srcDataApp.aCDecPastOwnedPropT;
                    destDataApp.aBDecPastOwnedPropTitleT = srcDataApp.aBDecPastOwnedPropTitleT;
                    destDataApp.aCDecPastOwnedPropTitleT = srcDataApp.aCDecPastOwnedPropTitleT;
                    destDataApp.aBNoFurnish = srcDataApp.aBNoFurnish;
                    destDataApp.aBNoFurnishLckd = srcDataApp.aBNoFurnishLckd;
                    destDataApp.aCNoFurnish = srcDataApp.aCNoFurnish;
                    destDataApp.aCNoFurnishLckd = srcDataApp.aCNoFurnishLckd;
                    AssignRaceEthnicityData(destDataApp, srcDataApp);

                    TransferUladDeclarationData(destDataApp, srcDataApp);

                    destDataApp.aCEmail = srcDataApp.aCEmail;
                    destDataApp.aCHPhone = srcDataApp.aCHPhone;
                    destDataApp.aCBusPhone = srcDataApp.aCBusPhone;
                    destDataApp.aCCellPhone = srcDataApp.aCCellPhone;
                    destDataApp.aCFax = srcDataApp.aCFax;

                    if (srcDataLoan.sBorrowerApplicationCollectionT == E_sLqbCollectionT.Legacy)
                    {
                        destDataApp.AcceptNewLiaCollection(srcDataApp.aLiaXmlContent.Value);
                        destDataApp.AcceptNewAssetCollection(srcDataApp.aAssetXmlContent.Value);
                        destDataApp.AcceptNewBEmplmtCollection(srcDataApp.aBEmplmtXmlContent);
                        destDataApp.AcceptNewCEmplmtCollection(srcDataApp.aCEmplmtXmlContent);
                        destDataApp.AcceptNewReCollection(srcDataApp.aReXmlContent);
                    }

                    destDataApp.aBExperianScore_rep = srcDataApp.aBExperianScore_rep;
                    destDataApp.aBExperianCreatedD_rep = srcDataApp.aBExperianCreatedD_rep;
                    destDataApp.aBExperianFactors = srcDataApp.aBExperianFactors;
                    destDataApp.aCExperianScore_rep = srcDataApp.aCExperianScore_rep;
                    destDataApp.aCExperianCreatedD_rep = srcDataApp.aCExperianCreatedD_rep;
                    destDataApp.aBTransUnionScore_rep = srcDataApp.aBTransUnionScore_rep;
                    destDataApp.aBTransUnionCreatedD_rep = srcDataApp.aBTransUnionCreatedD_rep;
                    destDataApp.aBTransUnionFactors = srcDataApp.aBTransUnionFactors;
                    destDataApp.aCTransUnionScore_rep = srcDataApp.aCTransUnionScore_rep;
                    destDataApp.aCTransUnionCreatedD_rep = srcDataApp.aCTransUnionCreatedD_rep;
                    destDataApp.aCTransUnionFactors = srcDataApp.aCTransUnionFactors;
                    destDataApp.aBEquifaxScore_rep = srcDataApp.aBEquifaxScore_rep;
                    destDataApp.aBEquifaxCreatedD_rep = srcDataApp.aBEquifaxCreatedD_rep;
                    destDataApp.aBEquifaxFactors = srcDataApp.aBEquifaxFactors;
                    destDataApp.aCEquifaxScore_rep = srcDataApp.aCEquifaxScore_rep;
                    destDataApp.aCEquifaxCreatedD_rep = srcDataApp.aCEquifaxCreatedD_rep;
                    destDataApp.aCEquifaxFactors = srcDataApp.aCEquifaxFactors;
                    destDataApp.aBExperianModelT = srcDataApp.aBExperianModelT;
                    destDataApp.aBTransUnionModelT = srcDataApp.aBTransUnionModelT;
                    destDataApp.aBEquifaxModelT = srcDataApp.aBEquifaxModelT;
                    destDataApp.aCExperianModelT = srcDataApp.aCExperianModelT;
                    destDataApp.aCTransUnionModelT = srcDataApp.aCTransUnionModelT;
                    destDataApp.aCEquifaxModelT = srcDataApp.aCEquifaxModelT;

                    // 9/27/2013 gf - from PML Summary
                    destDataApp.aProdBCitizenT = srcDataApp.aProdBCitizenT;
                    destDataApp.aIsBorrSpousePrimaryWageEarner = srcDataApp.aIsBorrSpousePrimaryWageEarner;

                    destDataApp.aBTypeT = srcDataApp.aBTypeT;
                    destDataApp.aCTypeT = srcDataApp.aCTypeT;
                    destDataApp.aBIsVeteran = srcDataApp.aBIsVeteran;
                    destDataApp.aCIsVeteran = srcDataApp.aCIsVeteran;
                    destDataApp.aEmplrBusPhoneLckd = srcDataApp.aEmplrBusPhoneLckd;
                    destDataApp.aPresOHExpDesc = srcDataApp.aPresOHExpDesc;

                    destDataApp.aBAliases = srcDataApp.aBAliases;
                    destDataApp.aCAliases = srcDataApp.aCAliases;

                    // 9/12/2013 gf - The regions below do not necessarily
                    // contain all of the fields assigned to from that page.
                    // If the field was already assigned elsewhere it was
                    // left out.
                    // LoanInfo (Expanded)
                    destDataApp.aOccT = srcDataApp.aOccT;

                    // 1003 Pg 1 (Expanded)
                    destDataApp.aBDependAges = srcDataApp.aBDependAges;
                    destDataApp.aBCoreSystemId = srcDataApp.aBCoreSystemId;
                    destDataApp.aCDependAges = srcDataApp.aCDependAges;
                    destDataApp.aCCoreSystemId = srcDataApp.aCCoreSystemId;
                    destDataApp.aManner = srcDataApp.aManner;
                    destDataApp.aSpouseIExcl = srcDataApp.aSpouseIExcl;
                    destDataApp.aTitleNm1 = srcDataApp.aTitleNm1;
                    destDataApp.aTitleNm2 = srcDataApp.aTitleNm2;

                    // 1003 Pg 2 (Expanded)
                    destDataApp.aAsstLiaCompletedNotJointly = srcDataApp.aAsstLiaCompletedNotJointly;

                    // 1003 Pg 3 (Expanded)
                    destDataApp.aAltNm1 = srcDataApp.aAltNm1;
                    destDataApp.aAltNm1AccNum = srcDataApp.aAltNm1AccNum;
                    destDataApp.aAltNm1CreditorNm = srcDataApp.aAltNm1CreditorNm;
                    destDataApp.aAltNm2 = srcDataApp.aAltNm2;
                    destDataApp.aAltNm2AccNum = srcDataApp.aAltNm2AccNum;
                    destDataApp.aAltNm2CreditorNm = srcDataApp.aAltNm2CreditorNm;
                    destDataApp.aBDecForeignNational = srcDataApp.aBDecForeignNational;

                    // 1003 Pg 4 (Expanded)
                    destDataApp.a1003ContEditSheet = srcDataApp.a1003ContEditSheet;

                    // Credit Denial (Expanded)
                    destDataApp.aDenialNoCreditFile = srcDataApp.aDenialNoCreditFile;
                    destDataApp.aDenialInsufficientCreditRef = srcDataApp.aDenialInsufficientCreditRef;
                    destDataApp.aDenialInsufficientCreditFile = srcDataApp.aDenialInsufficientCreditFile;
                    destDataApp.aDenialUnableVerifyCreditRef = srcDataApp.aDenialUnableVerifyCreditRef;
                    destDataApp.aDenialGarnishment = srcDataApp.aDenialGarnishment;
                    destDataApp.aDenialExcessiveObligations = srcDataApp.aDenialExcessiveObligations;
                    destDataApp.aDenialInsufficientIncome = srcDataApp.aDenialInsufficientIncome;
                    destDataApp.aDenialUnacceptablePmtRecord = srcDataApp.aDenialUnacceptablePmtRecord;
                    destDataApp.aDenialLackOfCashReserves = srcDataApp.aDenialLackOfCashReserves;
                    destDataApp.aDenialDeliquentCreditObligations = srcDataApp.aDenialDeliquentCreditObligations;
                    destDataApp.aDenialBankruptcy = srcDataApp.aDenialBankruptcy;
                    destDataApp.aDenialInfoFromConsumerReportAgency = srcDataApp.aDenialInfoFromConsumerReportAgency;
                    destDataApp.aDenialUnableVerifyEmployment = srcDataApp.aDenialUnableVerifyEmployment;
                    destDataApp.aDenialLenOfEmployment = srcDataApp.aDenialLenOfEmployment;
                    destDataApp.aDenialTemporaryEmployment = srcDataApp.aDenialTemporaryEmployment;
                    destDataApp.aDenialInsufficientIncomeForMortgagePmt = srcDataApp.aDenialInsufficientIncomeForMortgagePmt;
                    destDataApp.aDenialUnableVerifyIncome = srcDataApp.aDenialUnableVerifyIncome;
                    destDataApp.aDenialTempResidence = srcDataApp.aDenialTempResidence;
                    destDataApp.aDenialShortResidencePeriod = srcDataApp.aDenialShortResidencePeriod;
                    destDataApp.aDenialUnableVerifyResidence = srcDataApp.aDenialUnableVerifyResidence;
                    destDataApp.aDenialByHUD = srcDataApp.aDenialByHUD;
                    destDataApp.aDenialByVA = srcDataApp.aDenialByVA;
                    destDataApp.aDenialByFedNationalMortAssoc = srcDataApp.aDenialByFedNationalMortAssoc;
                    destDataApp.aDenialByFedHomeLoanMortCorp = srcDataApp.aDenialByFedHomeLoanMortCorp;
                    destDataApp.aDenialByOther = srcDataApp.aDenialByOther;
                    destDataApp.aDenialByOtherDesc = srcDataApp.aDenialByOtherDesc;
                    destDataApp.aDenialInsufficientFundsToClose = srcDataApp.aDenialInsufficientFundsToClose;
                    destDataApp.aDenialCreditAppIncomplete = srcDataApp.aDenialCreditAppIncomplete;
                    destDataApp.aDenialInadequateCollateral = srcDataApp.aDenialInadequateCollateral;
                    destDataApp.aDenialUnacceptableProp = srcDataApp.aDenialUnacceptableProp;
                    destDataApp.aDenialInsufficientPropData = srcDataApp.aDenialInsufficientPropData;
                    destDataApp.aDenialUnacceptableAppraisal = srcDataApp.aDenialUnacceptableAppraisal;
                    destDataApp.aDenialUnacceptableLeasehold = srcDataApp.aDenialUnacceptableLeasehold;
                    destDataApp.aDenialNotGrantCreditToAnyAppOnSuchTermsAndConds = srcDataApp.aDenialNotGrantCreditToAnyAppOnSuchTermsAndConds;
                    destDataApp.aDenialWithdrawnByApp = srcDataApp.aDenialWithdrawnByApp;
                    destDataApp.aDenialOtherReason1 = srcDataApp.aDenialOtherReason1;
                    destDataApp.aDenialOtherReason1Desc = srcDataApp.aDenialOtherReason1Desc;
                    destDataApp.aDenialOtherReason2 = srcDataApp.aDenialOtherReason2;
                    destDataApp.aDenialOtherReason2Desc = srcDataApp.aDenialOtherReason2Desc;
                    destDataApp.aDenialDecisionBasedOnReportAgency = srcDataApp.aDenialDecisionBasedOnReportAgency;
                    destDataApp.aDenialDecisionBasedOnCRA = srcDataApp.aDenialDecisionBasedOnCRA;
                    destDataApp.aDenialAdditionalStatement = srcDataApp.aDenialAdditionalStatement;
                    destDataApp.aDenialWeObtainedCreditScoreFromCRA = srcDataApp.aDenialWeObtainedCreditScoreFromCRA;
                    destDataApp.aDenialCreditScore_rep = srcDataApp.aDenialCreditScore_rep;
                    destDataApp.aDenialCreditScoreD_rep = srcDataApp.aDenialCreditScoreD_rep;
                    destDataApp.aDenialCreditScoreRangeFrom_rep = srcDataApp.aDenialCreditScoreRangeFrom_rep;
                    destDataApp.aDenialCreditScoreRangeTo_rep = srcDataApp.aDenialCreditScoreRangeTo_rep;
                    destDataApp.aDenialCreditScoreFactor1 = srcDataApp.aDenialCreditScoreFactor1;
                    destDataApp.aDenialCreditScoreFactor2 = srcDataApp.aDenialCreditScoreFactor2;
                    destDataApp.aDenialCreditScoreFactor3 = srcDataApp.aDenialCreditScoreFactor3;
                    destDataApp.aDenialCreditScoreFactor4 = srcDataApp.aDenialCreditScoreFactor4;
                    destDataApp.aDenialIsFactorNumberOfRecentInquiries = srcDataApp.aDenialIsFactorNumberOfRecentInquiries;
                    destDataApp.aCrRd_rep = srcDataApp.aCrRd_rep;
                    destDataApp.aCDenialNoCreditFile = srcDataApp.aCDenialNoCreditFile;
                    destDataApp.aCDenialInsufficientCreditRef = srcDataApp.aCDenialInsufficientCreditRef;
                    destDataApp.aCDenialInsufficientCreditFile = srcDataApp.aCDenialInsufficientCreditFile;
                    destDataApp.aCDenialUnableVerifyCreditRef = srcDataApp.aCDenialUnableVerifyCreditRef;
                    destDataApp.aCDenialGarnishment = srcDataApp.aCDenialGarnishment;
                    destDataApp.aCDenialExcessiveObligations = srcDataApp.aCDenialExcessiveObligations;
                    destDataApp.aCDenialInsufficientIncome = srcDataApp.aCDenialInsufficientIncome;
                    destDataApp.aCDenialUnacceptablePmtRecord = srcDataApp.aCDenialUnacceptablePmtRecord;
                    destDataApp.aCDenialLackOfCashReserves = srcDataApp.aCDenialLackOfCashReserves;
                    destDataApp.aCDenialDeliquentCreditObligations = srcDataApp.aCDenialDeliquentCreditObligations;
                    destDataApp.aCDenialBankruptcy = srcDataApp.aCDenialBankruptcy;
                    destDataApp.aCDenialInfoFromConsumerReportAgency = srcDataApp.aCDenialInfoFromConsumerReportAgency;
                    destDataApp.aCDenialUnableVerifyEmployment = srcDataApp.aCDenialUnableVerifyEmployment;
                    destDataApp.aCDenialLenOfEmployment = srcDataApp.aCDenialLenOfEmployment;
                    destDataApp.aCDenialTemporaryEmployment = srcDataApp.aCDenialTemporaryEmployment;
                    destDataApp.aCDenialInsufficientIncomeForMortgagePmt = srcDataApp.aCDenialInsufficientIncomeForMortgagePmt;
                    destDataApp.aCDenialUnableVerifyIncome = srcDataApp.aCDenialUnableVerifyIncome;
                    destDataApp.aCDenialTempResidence = srcDataApp.aCDenialTempResidence;
                    destDataApp.aCDenialShortResidencePeriod = srcDataApp.aCDenialShortResidencePeriod;
                    destDataApp.aCDenialUnableVerifyResidence = srcDataApp.aCDenialUnableVerifyResidence;
                    destDataApp.aCDenialByHUD = srcDataApp.aCDenialByHUD;
                    destDataApp.aCDenialByVA = srcDataApp.aCDenialByVA;
                    destDataApp.aCDenialByFedNationalMortAssoc = srcDataApp.aCDenialByFedNationalMortAssoc;
                    destDataApp.aCDenialByFedHomeLoanMortCorp = srcDataApp.aCDenialByFedHomeLoanMortCorp;
                    destDataApp.aCDenialByOther = srcDataApp.aCDenialByOther;
                    destDataApp.aCDenialByOtherDesc = srcDataApp.aCDenialByOtherDesc;
                    destDataApp.aCDenialInsufficientFundsToClose = srcDataApp.aCDenialInsufficientFundsToClose;
                    destDataApp.aCDenialCreditAppIncomplete = srcDataApp.aCDenialCreditAppIncomplete;
                    destDataApp.aCDenialInadequateCollateral = srcDataApp.aCDenialInadequateCollateral;
                    destDataApp.aCDenialUnacceptableProp = srcDataApp.aCDenialUnacceptableProp;
                    destDataApp.aCDenialInsufficientPropData = srcDataApp.aCDenialInsufficientPropData;
                    destDataApp.aCDenialUnacceptableAppraisal = srcDataApp.aCDenialUnacceptableAppraisal;
                    destDataApp.aCDenialUnacceptableLeasehold = srcDataApp.aCDenialUnacceptableLeasehold;
                    destDataApp.aCDenialNotGrantCreditToAnyAppOnSuchTermsAndConds = srcDataApp.aCDenialNotGrantCreditToAnyAppOnSuchTermsAndConds;
                    destDataApp.aCDenialWithdrawnByApp = srcDataApp.aCDenialWithdrawnByApp;
                    destDataApp.aCDenialOtherReason1 = srcDataApp.aCDenialOtherReason1;
                    destDataApp.aCDenialOtherReason1Desc = srcDataApp.aCDenialOtherReason1Desc;
                    destDataApp.aCDenialOtherReason2 = srcDataApp.aCDenialOtherReason2;
                    destDataApp.aCDenialOtherReason2Desc = srcDataApp.aCDenialOtherReason2Desc;
                    destDataApp.aCDenialDecisionBasedOnReportAgency = srcDataApp.aCDenialDecisionBasedOnReportAgency;
                    destDataApp.aCDenialDecisionBasedOnCRA = srcDataApp.aCDenialDecisionBasedOnCRA;
                    destDataApp.aCDenialAdditionalStatement = srcDataApp.aCDenialAdditionalStatement;
                    destDataApp.aCDenialWeObtainedCreditScoreFromCRA = srcDataApp.aCDenialWeObtainedCreditScoreFromCRA;
                    destDataApp.aCDenialCreditScore_rep = srcDataApp.aCDenialCreditScore_rep;
                    destDataApp.aCDenialCreditScoreD_rep = srcDataApp.aCDenialCreditScoreD_rep;
                    destDataApp.aCDenialCreditScoreRangeFrom_rep = srcDataApp.aCDenialCreditScoreRangeFrom_rep;
                    destDataApp.aCDenialCreditScoreRangeTo_rep = srcDataApp.aCDenialCreditScoreRangeTo_rep;
                    destDataApp.aCDenialCreditScoreFactor1 = srcDataApp.aCDenialCreditScoreFactor1;
                    destDataApp.aCDenialCreditScoreFactor2 = srcDataApp.aCDenialCreditScoreFactor2;
                    destDataApp.aCDenialCreditScoreFactor3 = srcDataApp.aCDenialCreditScoreFactor3;
                    destDataApp.aCDenialCreditScoreFactor4 = srcDataApp.aCDenialCreditScoreFactor4;
                    destDataApp.aCDenialIsFactorNumberOfRecentInquiries = srcDataApp.aCDenialIsFactorNumberOfRecentInquiries;
                    destDataApp.aCCrRd_rep = srcDataApp.aCCrRd_rep;

                    // VA Loan Analysis (Expanded)
                    destDataApp.aFHABCaivrsNum = srcDataApp.aFHABCaivrsNum;
                    destDataApp.aFHACCaivrsNum = srcDataApp.aFHACCaivrsNum;
                    destDataApp.aVaBEmplmtI_rep = srcDataApp.aVaBEmplmtI_rep;
                    destDataApp.aVaBEmplmtILckd = srcDataApp.aVaBEmplmtILckd;
                    destDataApp.aVaBFedITax_rep = srcDataApp.aVaBFedITax_rep;
                    destDataApp.aVaBOITax_rep = srcDataApp.aVaBOITax_rep;
                    destDataApp.aVaBONetI_rep = srcDataApp.aVaBONetI_rep;
                    destDataApp.aVaBONetILckd = srcDataApp.aVaBONetILckd;
                    destDataApp.aVaBSsnTax_rep = srcDataApp.aVaBSsnTax_rep;
                    destDataApp.aVaBStateITax_rep = srcDataApp.aVaBStateITax_rep;
                    destDataApp.aVaCEmplmtI_rep = srcDataApp.aVaCEmplmtI_rep;
                    destDataApp.aVaCEmplmtILckd = srcDataApp.aVaCEmplmtILckd;
                    destDataApp.aVaCFedITax_rep = srcDataApp.aVaCFedITax_rep;
                    destDataApp.aVaCOITax_rep = srcDataApp.aVaCOITax_rep;
                    destDataApp.aVaCONetI_rep = srcDataApp.aVaCONetI_rep;
                    destDataApp.aVaCONetILckd = srcDataApp.aVaCONetILckd;
                    destDataApp.aVaCSsnTax_rep = srcDataApp.aVaCSsnTax_rep;
                    destDataApp.aVaCStateITax_rep = srcDataApp.aVaCStateITax_rep;
                    destDataApp.aVaCrRecordSatisfyTri = srcDataApp.aVaCrRecordSatisfyTri;
                    destDataApp.aVaFamilySuportGuidelineAmt_rep = srcDataApp.aVaFamilySuportGuidelineAmt_rep;
                    destDataApp.aVaLAnalysisN = srcDataApp.aVaLAnalysisN;
                    destDataApp.aVaLMeetCrStandardTri = srcDataApp.aVaLMeetCrStandardTri;
                    destDataApp.aVaONetIDesc = srcDataApp.aVaONetIDesc;
                    destDataApp.aVaUtilityIncludedTri = srcDataApp.aVaUtilityIncludedTri;
                    destDataApp.aVALAnalysisValue_rep = srcDataApp.aVALAnalysisValue_rep;
                    destDataApp.aVALAnalysisExpirationD_rep = srcDataApp.aVALAnalysisExpirationD_rep;
                    destDataApp.aVALAnalysisEconomicLifeYrs_rep = srcDataApp.aVALAnalysisEconomicLifeYrs_rep;

                    // OPM 223001 - Preserve VA Fields.
                    if (!parameters.ConvertFromQp2File)
                    {
                        destDataApp.aIsBVAFFEx = srcDataApp.aIsBVAFFEx;
                        destDataApp.aBVServiceT = srcDataApp.aBVServiceT;
                        destDataApp.aBVEnt = srcDataApp.aBVEnt;
                        destDataApp.aIsBVAElig = srcDataApp.aIsBVAElig;

                        destDataApp.aIsCVAFFEx = srcDataApp.aIsCVAFFEx;
                        destDataApp.aCVServiceT = srcDataApp.aCVServiceT;
                        destDataApp.aCVEnt = srcDataApp.aCVEnt;
                        destDataApp.aIsCVAElig = srcDataApp.aIsCVAElig;
                    }

                    // Submit to DO/DU (Expanded)
                    destDataApp.a1003SignD_rep = srcDataApp.a1003SignD_rep;
                    destDataApp.aPresTotHExpDesc = srcDataApp.aPresTotHExpDesc;
                    destDataApp.aPresTotHExpCalc_rep = srcDataApp.aPresTotHExpCalc_rep;
                    destDataApp.aPresTotHExpLckd = srcDataApp.aPresTotHExpLckd;

                    // copy disclosure fields opm 196899
                    destDataApp.aBEConsentDeclinedD = srcDataApp.aBEConsentDeclinedD;
                    destDataApp.aBEConsentReceivedD = srcDataApp.aBEConsentReceivedD;
                    destDataApp.aCEConsentDeclinedD = srcDataApp.aCEConsentDeclinedD;
                    destDataApp.aCEConsentReceivedD = srcDataApp.aCEConsentReceivedD;

                    destDataApp.CopyRespaFirstEnteredDates(srcDataApp);
                    destDataApp.a1003InterviewD = srcDataApp.a1003InterviewD;
                    destDataApp.a1003InterviewDLckd = srcDataApp.a1003InterviewDLckd;
                }

                if (srcDataLoan.sBorrowerApplicationCollectionT != E_sLqbCollectionT.Legacy)
                {
                    // This needs to happen after we copy over the coborrower indicator.
                    // This will copy the ULAD apps and the entities from the source file.
                    destDataLoan.DuplicateLqbCollectionDataFrom(srcDataLoan);
                }

                // 03/31/2014 ir - opm 169478 Transfer CRMNow Lead Id
                destDataLoan.sCrmNowLeadId = srcDataLoan.sCrmNowLeadId;

                if (principal.HasPermission(Permission.CanAccessLendingStaffNotes))
                {
                    destDataLoan.sLendingStaffNotes = srcDataLoan.sLendingStaffNotes;
                }

                destDataLoan.sHmdaCoApplicantSourceSsnLckd = srcDataLoan.sHmdaCoApplicantSourceSsnLckd;
                if (srcDataLoan.sHmdaCoApplicantSourceSsnLckd)
                {
                    destDataLoan.sHmdaCoApplicantSourceSsn = srcDataLoan.sHmdaCoApplicantSourceSsn;
                }

                destDataLoan.sFhaSponsoredOriginatorEinLckd = srcDataLoan.sFhaSponsoredOriginatorEinLckd;
                if (srcDataLoan.sFhaSponsoredOriginatorEinLckd)
                {
                    destDataLoan.sFHASponsoredOriginatorEIN = srcDataLoan.sFHASponsoredOriginatorEIN;
                }

                destDataLoan.sCombinedBorInfoLckd = srcDataLoan.sCombinedBorInfoLckd;
                if (srcDataLoan.sCombinedBorInfoLckd)
                {
                    destDataLoan.sCombinedBorSsn = srcDataLoan.sCombinedBorSsn;
                    destDataLoan.sCombinedCoborSsn = srcDataLoan.sCombinedCoborSsn;
                }

                destDataLoan.sTax1098PayerSsnLckd = srcDataLoan.sTax1098PayerSsnLckd;
                if (srcDataLoan.sTax1098PayerSsnLckd)
                {
                    destDataLoan.sTax1098PayerSsn = srcDataLoan.sTax1098PayerSsn;
                }

                // OPM 203382 ejm 3/17/2015 - Add fields for big lead/loan info page
                destDataLoan.sCaseAssignmentD = srcDataLoan.sCaseAssignmentD;
                destDataLoan.sFHAHousingActSection = srcDataLoan.sFHAHousingActSection;

                // 4/28/2015 ejm - opm 179810
                // Merge the official contacts list rather than using only the template's.
                Tools.MergeAgentDataSets(srcDataLoan.sAgentDataSet, destDataLoan.sAgentDataSet);

                // Need to do this to update serialization fields. 
                destDataLoan.sAgentDataSet = destDataLoan.sAgentDataSet;

                destDataLoan.sLT = srcDataLoan.sLT;
                destDataLoan.sLPurposeT = srcDataLoan.sLPurposeT;
                destDataLoan.sLienPosT = srcDataLoan.sLienPosT;
                destDataLoan.sLpTemplateNm = srcDataLoan.sLpTemplateNm;
                destDataLoan.sLoanProductIdentifier = srcDataLoan.sLoanProductIdentifier;
                destDataLoan.sLpTemplateId = srcDataLoan.sLpTemplateId;
                destDataLoan.sPurchPrice = srcDataLoan.sPurchPrice;
                destDataLoan.sApprVal = srcDataLoan.sApprVal;
                destDataLoan.sProdSpT = srcDataLoan.sProdSpT;
                destDataLoan.sEquityCalc = srcDataLoan.sEquityCalc;
                destDataLoan.sLAmtLckd = srcDataLoan.sLAmtLckd;
                destDataLoan.sLAmtCalc = srcDataLoan.sLAmtCalc;
                destDataLoan.sNoteIR = srcDataLoan.sNoteIR;
                destDataLoan.sTerm = srcDataLoan.sTerm;
                destDataLoan.sDue = srcDataLoan.sDue;
                destDataLoan.sSpAddr = srcDataLoan.sSpAddr;
                destDataLoan.sSpCity = srcDataLoan.sSpCity;
                destDataLoan.sSpState = srcDataLoan.sSpState;
                destDataLoan.sSpZip = srcDataLoan.sSpZip;
                destDataLoan.sTrNotes = srcDataLoan.sTrNotes;
                destDataLoan.sLeadD_rep = srcDataLoan.sLeadD_rep;

                destDataLoan.sEmployeeLoanRepId = srcDataLoan.sEmployeeLoanRepId;
                destDataLoan.sEmployeeProcessorId = srcDataLoan.sEmployeeProcessorId;
                destDataLoan.sEmployeeLoanOpenerId = srcDataLoan.sEmployeeLoanOpenerId;
                destDataLoan.sEmployeeCallCenterAgentId = srcDataLoan.sEmployeeCallCenterAgentId;
                destDataLoan.sEmployeeRealEstateAgentId = srcDataLoan.sEmployeeRealEstateAgentId;
                destDataLoan.sEmployeeLenderAccExecId = srcDataLoan.sEmployeeLenderAccExecId;
                destDataLoan.sEmployeeLockDeskId = srcDataLoan.sEmployeeLockDeskId;
                destDataLoan.sEmployeeUnderwriterId = srcDataLoan.sEmployeeUnderwriterId;
                destDataLoan.sEmployeeCloserId = srcDataLoan.sEmployeeCloserId;
                destDataLoan.sEmployeeManagerId = srcDataLoan.sEmployeeManagerId;
                destDataLoan.sEmployeeFunderId = srcDataLoan.sEmployeeFunderId;
                destDataLoan.sEmployeeShipperId = srcDataLoan.sEmployeeShipperId;
                destDataLoan.sEmployeePostCloserId = srcDataLoan.sEmployeePostCloserId;
                destDataLoan.sEmployeeInsuringId = srcDataLoan.sEmployeeInsuringId;
                destDataLoan.sEmployeeCollateralAgentId = srcDataLoan.sEmployeeCollateralAgentId;
                destDataLoan.sEmployeeDocDrawerId = srcDataLoan.sEmployeeDocDrawerId;

                // 11/22/2013 gf - opm 145015
                destDataLoan.sEmployeeCreditAuditorId = srcDataLoan.sEmployeeCreditAuditorId;
                destDataLoan.sEmployeeDisclosureDeskId = srcDataLoan.sEmployeeDisclosureDeskId;
                destDataLoan.sEmployeeJuniorProcessorId = srcDataLoan.sEmployeeJuniorProcessorId;
                destDataLoan.sEmployeeJuniorUnderwriterId = srcDataLoan.sEmployeeJuniorUnderwriterId;
                destDataLoan.sEmployeeLegalAuditorId = srcDataLoan.sEmployeeLegalAuditorId;
                destDataLoan.sEmployeeLoanOfficerAssistantId = srcDataLoan.sEmployeeLoanOfficerAssistantId;
                destDataLoan.sEmployeePurchaserId = srcDataLoan.sEmployeePurchaserId;
                destDataLoan.sEmployeeQCComplianceId = srcDataLoan.sEmployeeQCComplianceId;
                destDataLoan.sEmployeeSecondaryId = srcDataLoan.sEmployeeSecondaryId;
                destDataLoan.sEmployeeServicingId = srcDataLoan.sEmployeeServicingId;

                // 9/27/2013 gf - opm 126044 General PML Fields from PML Summary page.
                destDataLoan.sIsSelfEmployed = srcDataLoan.sIsSelfEmployed;
                destDataLoan.sOccRLckd = srcDataLoan.sOccRLckd;
                destDataLoan.sOccR_rep = srcDataLoan.sOccR_rep;
                destDataLoan.sSpGrossRent_rep = srcDataLoan.sSpGrossRent_rep;
                destDataLoan.sPriorSalesPrice_rep = srcDataLoan.sPriorSalesPrice_rep;
                destDataLoan.sPriorSalesPropertySellerT = srcDataLoan.sPriorSalesPropertySellerT;
                destDataLoan.sPriorSalesD_rep = srcDataLoan.sPriorSalesD_rep;

                E_CalcModeT oldCalcModeT = destDataLoan.CalcModeT;
                destDataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                destDataLoan.sProdSpT = destDataLoan.sProdSpT;
                destDataLoan.sProdSpStructureT = destDataLoan.sProdSpStructureT;
                destDataLoan.sProdCondoStories_rep = destDataLoan.sProdCondoStories_rep;
                destDataLoan.CalcModeT = oldCalcModeT;

                destDataLoan.sProdIsSpInRuralArea = srcDataLoan.sProdIsSpInRuralArea;
                destDataLoan.sProdIsCondotel = srcDataLoan.sProdIsCondotel;
                destDataLoan.sProdIsNonwarrantableProj = srcDataLoan.sProdIsNonwarrantableProj;
                destDataLoan.sProdMIOptionT = srcDataLoan.sProdMIOptionT;
                destDataLoan.sProdIsTexas50a6Loan = srcDataLoan.sProdIsTexas50a6Loan;
                destDataLoan.sPreviousLoanIsTexas50a6Loan = srcDataLoan.sPreviousLoanIsTexas50a6Loan;
                destDataLoan.sIsStudentLoanCashoutRefi = srcDataLoan.sIsStudentLoanCashoutRefi;
                destDataLoan.sProdCashoutAmt_rep = srcDataLoan.sProdCashoutAmt_rep;
                destDataLoan.sIsRenovationLoan = srcDataLoan.sIsRenovationLoan;
                destDataLoan.sTotalRenovationCosts_rep = srcDataLoan.sTotalRenovationCosts_rep;
                destDataLoan.sNumFinancedProperties_rep = srcDataLoan.sNumFinancedProperties_rep;
                destDataLoan.sProdImpound = srcDataLoan.sProdImpound;
                destDataLoan.sProdRLckdDays_rep = srcDataLoan.sProdRLckdDays_rep;
                destDataLoan.sProdDocT = srcDataLoan.sProdDocT;
                destDataLoan.sFinMethT = srcDataLoan.sFinMethT;
                destDataLoan.sProdAvailReserveMonths_rep = srcDataLoan.sProdAvailReserveMonths_rep;
                destDataLoan.sQualIRLckd = srcDataLoan.sQualIRLckd;
                destDataLoan.sQualIR_rep = srcDataLoan.sQualIR_rep;
                destDataLoan.sUseQualRate = srcDataLoan.sUseQualRate;
                destDataLoan.sQualRateCalculationT = srcDataLoan.sQualRateCalculationT;
                destDataLoan.sQualRateCalculationFieldT1 = srcDataLoan.sQualRateCalculationFieldT1;
                destDataLoan.sQualRateCalculationFieldT2 = srcDataLoan.sQualRateCalculationFieldT2;
                destDataLoan.sQualRateCalculationAdjustment1 = srcDataLoan.sQualRateCalculationAdjustment1;
                destDataLoan.sQualRateCalculationAdjustment2 = srcDataLoan.sQualRateCalculationAdjustment2;
                destDataLoan.sIsQRateIOnly = srcDataLoan.sIsQRateIOnly;
                destDataLoan.sQualTermCalculationType = srcDataLoan.sQualTermCalculationType;
                destDataLoan.sQualTerm = srcDataLoan.sQualTerm;
                destDataLoan.sIsOptionArm = srcDataLoan.sIsOptionArm;
                destDataLoan.sIOnlyMon_rep = srcDataLoan.sIOnlyMon_rep;
                destDataLoan.sOptionArmTeaserR_rep = srcDataLoan.sOptionArmTeaserR_rep;
                destDataLoan.sLpIsNegAmortOtherLien = srcDataLoan.sLpIsNegAmortOtherLien;
                destDataLoan.sOtherLFinMethT = srcDataLoan.sOtherLFinMethT;
                destDataLoan.sProd3rdPartyUwResultT = srcDataLoan.sProd3rdPartyUwResultT;
                destDataLoan.sProdEstimatedResidualI_rep = srcDataLoan.sProdEstimatedResidualI_rep;

                // Results Filter
                destDataLoan.sProdFilterRestrictResultToRegisteredProgram = srcDataLoan.sProdFilterRestrictResultToRegisteredProgram;
                destDataLoan.sProdFilterRestrictResultToCurrentInvestor = srcDataLoan.sProdFilterRestrictResultToCurrentInvestor;
                destDataLoan.sProdFilterDue10Yrs = srcDataLoan.sProdFilterDue10Yrs;
                destDataLoan.sProdFilterDue15Yrs = srcDataLoan.sProdFilterDue15Yrs;
                destDataLoan.sProdFilterDue20Yrs = srcDataLoan.sProdFilterDue20Yrs;
                destDataLoan.sProdFilterDue25Yrs = srcDataLoan.sProdFilterDue25Yrs;
                destDataLoan.sProdFilterDue30Yrs = srcDataLoan.sProdFilterDue30Yrs;
                destDataLoan.sProdFilterDueOther = srcDataLoan.sProdFilterDueOther;
                destDataLoan.sProdFilterFinMethFixed = srcDataLoan.sProdFilterFinMethFixed;
                destDataLoan.sProdFilterFinMeth3YrsArm = srcDataLoan.sProdFilterFinMeth3YrsArm;
                destDataLoan.sProdFilterFinMeth5YrsArm = srcDataLoan.sProdFilterFinMeth5YrsArm;
                destDataLoan.sProdFilterFinMeth7YrsArm = srcDataLoan.sProdFilterFinMeth7YrsArm;
                destDataLoan.sProdFilterFinMeth10YrsArm = srcDataLoan.sProdFilterFinMeth10YrsArm;
                destDataLoan.sProdFilterFinMethOther = srcDataLoan.sProdFilterFinMethOther;
                destDataLoan.sProdIncludeNormalProc = srcDataLoan.sProdIncludeNormalProc;
                destDataLoan.sProdIncludeMyCommunityProc = srcDataLoan.sProdIncludeMyCommunityProc;
                destDataLoan.sProdIncludeHomePossibleProc = srcDataLoan.sProdIncludeHomePossibleProc;
                destDataLoan.sProdIncludeFHATotalProc = srcDataLoan.sProdIncludeFHATotalProc;
                destDataLoan.sProdIncludeVAProc = srcDataLoan.sProdIncludeVAProc;
                destDataLoan.sProdIncludeUSDARuralProc = srcDataLoan.sProdIncludeUSDARuralProc;
                destDataLoan.sProdFilterPmtTPI = srcDataLoan.sProdFilterPmtTPI;
                destDataLoan.sProdFilterPmtTIOnly = srcDataLoan.sProdFilterPmtTIOnly;

                // PML Manual Credit
                destDataLoan.sProdCrManual120MortLateCount_rep = srcDataLoan.sProdCrManual120MortLateCount_rep;
                destDataLoan.sProdCrManual150MortLateCount_rep = srcDataLoan.sProdCrManual150MortLateCount_rep;
                destDataLoan.sProdCrManual30MortLateCount_rep = srcDataLoan.sProdCrManual30MortLateCount_rep;
                destDataLoan.sProdCrManual60MortLateCount_rep = srcDataLoan.sProdCrManual60MortLateCount_rep;
                destDataLoan.sProdCrManual90MortLateCount_rep = srcDataLoan.sProdCrManual90MortLateCount_rep;
                destDataLoan.sProdCrManualNonRolling30MortLateCount_rep = srcDataLoan.sProdCrManualNonRolling30MortLateCount_rep;
                destDataLoan.sProdCrManualRolling60MortLateCount_rep = srcDataLoan.sProdCrManualRolling60MortLateCount_rep;
                destDataLoan.sProdCrManualRolling90MortLateCount_rep = srcDataLoan.sProdCrManualRolling90MortLateCount_rep;
                destDataLoan.sProdCrManualBk13Has = srcDataLoan.sProdCrManualBk13Has;
                destDataLoan.sProdCrManualBk13RecentFileMon_rep = srcDataLoan.sProdCrManualBk13RecentFileMon_rep;
                destDataLoan.sProdCrManualBk13RecentFileYr_rep = srcDataLoan.sProdCrManualBk13RecentFileYr_rep;
                destDataLoan.sProdCrManualBk13RecentSatisfiedMon_rep = srcDataLoan.sProdCrManualBk13RecentSatisfiedMon_rep;
                destDataLoan.sProdCrManualBk13RecentSatisfiedYr_rep = srcDataLoan.sProdCrManualBk13RecentSatisfiedYr_rep;
                destDataLoan.sProdCrManualBk13RecentStatusT = srcDataLoan.sProdCrManualBk13RecentStatusT;
                destDataLoan.sProdCrManualBk7Has = srcDataLoan.sProdCrManualBk7Has;
                destDataLoan.sProdCrManualBk7RecentFileMon_rep = srcDataLoan.sProdCrManualBk7RecentFileMon_rep;
                destDataLoan.sProdCrManualBk7RecentFileYr_rep = srcDataLoan.sProdCrManualBk7RecentFileYr_rep;
                destDataLoan.sProdCrManualBk7RecentSatisfiedMon_rep = srcDataLoan.sProdCrManualBk7RecentSatisfiedMon_rep;
                destDataLoan.sProdCrManualBk7RecentSatisfiedYr_rep = srcDataLoan.sProdCrManualBk7RecentSatisfiedYr_rep;
                destDataLoan.sProdCrManualBk7RecentStatusT = srcDataLoan.sProdCrManualBk7RecentStatusT;
                destDataLoan.sProdCrManualForeclosureHas = srcDataLoan.sProdCrManualForeclosureHas;
                destDataLoan.sProdCrManualForeclosureRecentFileMon_rep = srcDataLoan.sProdCrManualForeclosureRecentFileMon_rep;
                destDataLoan.sProdCrManualForeclosureRecentFileYr_rep = srcDataLoan.sProdCrManualForeclosureRecentFileYr_rep;
                destDataLoan.sProdCrManualForeclosureRecentSatisfiedMon_rep = srcDataLoan.sProdCrManualForeclosureRecentSatisfiedMon_rep;
                destDataLoan.sProdCrManualForeclosureRecentSatisfiedYr_rep = srcDataLoan.sProdCrManualForeclosureRecentSatisfiedYr_rep;
                destDataLoan.sProdCrManualForeclosureRecentStatusT = srcDataLoan.sProdCrManualForeclosureRecentStatusT;

                // Custom PML Fields - gf OPM 109263 1/16/13
                destDataLoan.sCustomPMLField1_rep = srcDataLoan.sCustomPMLField1_rep;
                destDataLoan.sCustomPMLField2_rep = srcDataLoan.sCustomPMLField2_rep;
                destDataLoan.sCustomPMLField3_rep = srcDataLoan.sCustomPMLField3_rep;
                destDataLoan.sCustomPMLField4_rep = srcDataLoan.sCustomPMLField4_rep;
                destDataLoan.sCustomPMLField5_rep = srcDataLoan.sCustomPMLField5_rep;
                destDataLoan.sCustomPMLField6_rep = srcDataLoan.sCustomPMLField6_rep;
                destDataLoan.sCustomPMLField7_rep = srcDataLoan.sCustomPMLField7_rep;
                destDataLoan.sCustomPMLField8_rep = srcDataLoan.sCustomPMLField8_rep;
                destDataLoan.sCustomPMLField9_rep = srcDataLoan.sCustomPMLField9_rep;
                destDataLoan.sCustomPMLField10_rep = srcDataLoan.sCustomPMLField10_rep;

                // moar custom PML fields - JK OPM 471186 8/17/18
                destDataLoan.sCustomPMLField11_rep = srcDataLoan.sCustomPMLField11_rep;
                destDataLoan.sCustomPMLField12_rep = srcDataLoan.sCustomPMLField12_rep;
                destDataLoan.sCustomPMLField13_rep = srcDataLoan.sCustomPMLField13_rep;
                destDataLoan.sCustomPMLField14_rep = srcDataLoan.sCustomPMLField14_rep;
                destDataLoan.sCustomPMLField15_rep = srcDataLoan.sCustomPMLField15_rep;
                destDataLoan.sCustomPMLField16_rep = srcDataLoan.sCustomPMLField16_rep;
                destDataLoan.sCustomPMLField17_rep = srcDataLoan.sCustomPMLField17_rep;
                destDataLoan.sCustomPMLField18_rep = srcDataLoan.sCustomPMLField18_rep;
                destDataLoan.sCustomPMLField19_rep = srcDataLoan.sCustomPMLField19_rep;
                destDataLoan.sCustomPMLField20_rep = srcDataLoan.sCustomPMLField20_rep;

                // opm 129319
                destDataLoan.sAppTotLiqAssetExists = srcDataLoan.sAppTotLiqAssetExists;
                destDataLoan.sAppTotLiqAsset = srcDataLoan.sAppTotLiqAsset;

                destDataLoan.sConsumerPortalCreationD = srcDataLoan.sConsumerPortalCreationD;
                destDataLoan.sConsumerPortalCreationT = srcDataLoan.sConsumerPortalCreationT;
                destDataLoan.sConsumerPortalSubmittedD = srcDataLoan.sConsumerPortalSubmittedD;
                destDataLoan.sConsumerPortalVerbalSubmissionD = srcDataLoan.sConsumerPortalVerbalSubmissionD;
                destDataLoan.sConsumerPortalVerbalSubmissionComments = srcDataLoan.sConsumerPortalVerbalSubmissionComments;

                // Submission Related
                // OPM 172615. Because user can submit in leads again,
                // any data that can get set in the submission process
                // needs to be copied to the newly-created loan.
                destDataLoan.sHistoricalPricingVersionD = srcDataLoan.sHistoricalPricingVersionD;
                destDataLoan.sFirstLienProductId = srcDataLoan.sFirstLienProductId;
                destDataLoan.sOptionArmMinPayOptionT = srcDataLoan.sOptionArmMinPayOptionT;
                destDataLoan.sBrokComp1Lckd = srcDataLoan.sBrokComp1Lckd;
                destDataLoan.sBrokComp1MldsLckd = srcDataLoan.sBrokComp1MldsLckd;
                destDataLoan.sBrokComp1Pc = srcDataLoan.sBrokComp1Pc;
                destDataLoan.sBrokComp1Desc = srcDataLoan.sBrokComp1Desc;
                destDataLoan.sRAdjMarginR = srcDataLoan.sRAdjMarginR;
                destDataLoan.sNoteIRSubmitted = srcDataLoan.sNoteIRSubmitted;
                destDataLoan.sLOrigFPcSubmitted = srcDataLoan.sLOrigFPcSubmitted;
                destDataLoan.sRAdjMarginRSubmitted = srcDataLoan.sRAdjMarginRSubmitted;
                destDataLoan.sQualIRSubmitted = srcDataLoan.sQualIRSubmitted;
                destDataLoan.sOptionArmTeaserRSubmitted = srcDataLoan.sOptionArmTeaserRSubmitted;
                destDataLoan.sTermSubmitted = srcDataLoan.sTermSubmitted;
                destDataLoan.sDueSubmitted = srcDataLoan.sDueSubmitted;
                destDataLoan.sFinMethTSubmitted = srcDataLoan.sFinMethTSubmitted;
                destDataLoan.sIsOptionArmSubmitted = srcDataLoan.sIsOptionArmSubmitted;
                destDataLoan.sMaxDti_rep = srcDataLoan.sMaxDti_rep;
                destDataLoan.sCreditScoreLpeQual_rep = srcDataLoan.sCreditScoreLpeQual_rep;
                destDataLoan.sLpTemplateNmSubmitted = srcDataLoan.sLpTemplateNmSubmitted;
                destDataLoan.sRAdjFloorR = srcDataLoan.sRAdjFloorR;
                destDataLoan.sBrokerLockAdjustments = srcDataLoan.sBrokerLockAdjustments;
                destDataLoan.sBrokerLockRateSheetEffectiveD_rep = srcDataLoan.sBrokerLockRateSheetEffectiveD_rep;
                destDataLoan.sInvestorLockLockBuffer = srcDataLoan.sInvestorLockLockBuffer;
                destDataLoan.sLpProdCode = srcDataLoan.sLpProdCode;
                destDataLoan.sOriginatorCompensationLenderFeeOptionT = srcDataLoan.sOriginatorCompensationLenderFeeOptionT;
                destDataLoan.sOriginatorCompensationBorrPaidBaseT = srcDataLoan.sOriginatorCompensationBorrPaidBaseT;
                destDataLoan.sPrepmtPenaltyT = srcDataLoan.sPrepmtPenaltyT;
                destDataLoan.sPrimAppTotNonspIPe = srcDataLoan.sPrimAppTotNonspIPe;
                destDataLoan.sProOFinPmtPe_rep = srcDataLoan.sProOFinPmtPe_rep;
                destDataLoan.sPmlSubmitStatusT = srcDataLoan.sPmlSubmitStatusT;
                destDataLoan.sSubmitD = srcDataLoan.sSubmitD;
                destDataLoan.sDocMagicPlanCodeId = srcDataLoan.sDocMagicPlanCodeId;
                destDataLoan.sLpIsArmMarginDisplayed = srcDataLoan.sLpIsArmMarginDisplayed;
                destDataLoan.sLenNm = srcDataLoan.sLenNm;
                destDataLoan.sVaFfExemptTri = srcDataLoan.sVaFfExemptTri;
                destDataLoan.sLDiscntProps = srcDataLoan.sLDiscntProps;
                destDataLoan.s800U1FCode = srcDataLoan.s800U1FCode;
                destDataLoan.s800U2FCode = srcDataLoan.s800U2FCode;
                destDataLoan.s800U3FCode = srcDataLoan.s800U3FCode;
                destDataLoan.s800U4FCode = srcDataLoan.s800U4FCode;
                destDataLoan.s800U5FCode = srcDataLoan.s800U5FCode;
                destDataLoan.s900U1PiaCode = srcDataLoan.s900U1PiaCode;
                destDataLoan.sU1TcCode = srcDataLoan.sU1TcCode;
                destDataLoan.sU2TcCode = srcDataLoan.sU2TcCode;
                destDataLoan.sU3TcCode = srcDataLoan.sU3TcCode;
                destDataLoan.sU4TcCode = srcDataLoan.sU4TcCode;
                destDataLoan.sU1GovRtcCode = srcDataLoan.sU1GovRtcCode;
                destDataLoan.sU2GovRtcCode = srcDataLoan.sU2GovRtcCode;
                destDataLoan.sU3GovRtcCode = srcDataLoan.sU3GovRtcCode;
                destDataLoan.sU1ScCode = srcDataLoan.sU1ScCode;
                destDataLoan.sU2ScCode = srcDataLoan.sU2ScCode;
                destDataLoan.sU3ScCode = srcDataLoan.sU3ScCode;
                destDataLoan.sU4ScCode = srcDataLoan.sU4ScCode;
                destDataLoan.sU5ScCode = srcDataLoan.sU5ScCode;
                destDataLoan.sBrokComp1 = srcDataLoan.sBrokComp1;
                destDataLoan.sBrokComp1_rep = srcDataLoan.sBrokComp1_rep;
                destDataLoan.sBrokComp2Desc = srcDataLoan.sBrokComp2Desc;
                destDataLoan.sBrokComp2 = srcDataLoan.sBrokComp2;
                destDataLoan.sBrokComp2_rep = srcDataLoan.sBrokComp2_rep;
                destDataLoan.sU1FntcDesc = srcDataLoan.sU1FntcDesc;
                destDataLoan.sU1Fntc = srcDataLoan.sU1Fntc;
                destDataLoan.sU1Fntc_rep = srcDataLoan.sU1Fntc_rep;
                destDataLoan.sGfeProvByBrok = srcDataLoan.sGfeProvByBrok;
                destDataLoan.sDisabilityIns = srcDataLoan.sDisabilityIns;
                destDataLoan.sDisabilityIns_rep = srcDataLoan.sDisabilityIns_rep;
                destDataLoan.sTitleInsFGfeSection = srcDataLoan.sTitleInsFGfeSection;
                destDataLoan.sMortgageLoanT = srcDataLoan.sMortgageLoanT;
                destDataLoan.sJumboT = srcDataLoan.sJumboT;
                destDataLoan.sHelocDraw_rep = srcDataLoan.sHelocDraw_rep;
                destDataLoan.sHelocQualPmtBaseT = srcDataLoan.sHelocQualPmtBaseT;
                destDataLoan.sHelocQualPmtPcBaseT = srcDataLoan.sHelocQualPmtPcBaseT;
                destDataLoan.sHelocQualPmtMb_rep = srcDataLoan.sHelocQualPmtMb_rep;
                destDataLoan.sHelocPmtBaseT = srcDataLoan.sHelocPmtBaseT;
                destDataLoan.sHelocPmtPcBaseT = srcDataLoan.sHelocPmtPcBaseT;
                destDataLoan.sHelocPmtMb_rep = srcDataLoan.sHelocPmtMb_rep;
                destDataLoan.sTotalScoreFhaProductT = srcDataLoan.sTotalScoreFhaProductT;
                destDataLoan.sIsConvertibleMortgage = srcDataLoan.sIsConvertibleMortgage;
                destDataLoan.sRAdjFloorBaseT = srcDataLoan.sRAdjFloorBaseT;
                destDataLoan.sRLifeCapR = srcDataLoan.sRLifeCapR;
                destDataLoan.sRAdjWorstIndex = srcDataLoan.sRAdjWorstIndex;
                destDataLoan.sRAdjIndexR = srcDataLoan.sRAdjIndexR;
                destDataLoan.sRAdjRoundT = srcDataLoan.sRAdjRoundT;
                destDataLoan.sRAdjRoundToR = srcDataLoan.sRAdjRoundToR;
                destDataLoan.sBuydwnR1 = srcDataLoan.sBuydwnR1;
                destDataLoan.sBuydwnR2 = srcDataLoan.sBuydwnR2;
                destDataLoan.sBuydwnR3 = srcDataLoan.sBuydwnR3;
                destDataLoan.sBuydwnR4 = srcDataLoan.sBuydwnR4;
                destDataLoan.sBuydwnR5 = srcDataLoan.sBuydwnR5;
                destDataLoan.sBuydwnMon1 = srcDataLoan.sBuydwnMon1;
                destDataLoan.sBuydwnMon2 = srcDataLoan.sBuydwnMon2;
                destDataLoan.sBuydwnMon3 = srcDataLoan.sBuydwnMon3;
                destDataLoan.sBuydwnMon4 = srcDataLoan.sBuydwnMon4;
                destDataLoan.sBuydwnMon5 = srcDataLoan.sBuydwnMon5;
                destDataLoan.sGradPmtYrs = srcDataLoan.sGradPmtYrs;
                destDataLoan.sGradPmtR = srcDataLoan.sGradPmtR;
                destDataLoan.sDtiUsingMaxBalPc = srcDataLoan.sDtiUsingMaxBalPc;
                destDataLoan.sVarRNotes = srcDataLoan.sVarRNotes;
                destDataLoan.sAprIncludesReqDeposit = srcDataLoan.sAprIncludesReqDeposit;
                destDataLoan.sHasDemandFeature = srcDataLoan.sHasDemandFeature;
                destDataLoan.sFilingF = srcDataLoan.sFilingF;
                destDataLoan.sArmIndexAffectInitIRBit = srcDataLoan.sArmIndexAffectInitIRBit;
                destDataLoan.sArmIndexBasedOnVstr = srcDataLoan.sArmIndexBasedOnVstr;
                destDataLoan.sArmIndexCanBeFoundVstr = srcDataLoan.sArmIndexCanBeFoundVstr;
                destDataLoan.sArmIndexNotifyAtLeastDaysVstr = srcDataLoan.sArmIndexNotifyAtLeastDaysVstr;
                destDataLoan.sArmIndexNotifyNotBeforeDaysVstr = srcDataLoan.sArmIndexNotifyNotBeforeDaysVstr;
                destDataLoan.sHasVarRFeature = srcDataLoan.sHasVarRFeature;
                destDataLoan.sPrepmtRefundT = srcDataLoan.sPrepmtRefundT;
                destDataLoan.sAssumeLT = srcDataLoan.sAssumeLT;
                destDataLoan.sArmIndexNameVstr = srcDataLoan.sArmIndexNameVstr;
                destDataLoan.sArmIndexEffectiveD_rep = srcDataLoan.sArmIndexEffectiveD_rep;

                // OPM 184964. When this setting is on, the lead could have values
                // from loan program application, so we need to copy to template-created loan.
                // If setting is off, leave the template-created loan values alone: template wins.
                if (broker.PopulateLateFeesFromPml_139391)
                {
                    destDataLoan.sLateDays = srcDataLoan.sLateDays;
                    destDataLoan.sLateChargePc = srcDataLoan.sLateChargePc;
                    destDataLoan.sLateChargeBaseDesc = srcDataLoan.sLateChargeBaseDesc;
                }

                destDataLoan.sLpInvestorNm = srcDataLoan.sLpInvestorNm;
                destDataLoan.sLpProductType = srcDataLoan.sLpProductType;
                destDataLoan.sLpProductCode = srcDataLoan.sLpProductCode;
                destDataLoan.sPpmtPenaltyMon = srcDataLoan.sPpmtPenaltyMon;
                destDataLoan.sSoftPrepmtPeriodMonths_rep = srcDataLoan.sSoftPrepmtPeriodMonths_rep;
                destDataLoan.sPrepmtPeriodMonths_rep = srcDataLoan.sPrepmtPeriodMonths_rep;
                destDataLoan.sRLckdDays = srcDataLoan.sRLckdDays;
                destDataLoan.sRLckdDaysFromInvestor = srcDataLoan.sRLckdDaysFromInvestor;

                // OPM 173379 Only set CCTemplate Id if there is one.
                if (srcDataLoan.sCcTemplateId != Guid.Empty)
                {
                    destDataLoan.sCcTemplateId = srcDataLoan.sCcTemplateId;
                }

                destDataLoan.sFeeServiceApplicationHistoryXmlContent = srcDataLoan.sFeeServiceApplicationHistoryXmlContent;
                destDataLoan.sPmlCertXmlContent = srcDataLoan.sPmlCertXmlContent;

                // 8/7/2014 gf - opm 189088, originator compensation can also be assigned at submission.
                destDataLoan.CopyCompensationPlanFromLoan(srcDataLoan);

                // 9/12/2013 gf - The regions below do not necessarily
                // contain all of the fields assigned to from that page.
                // If the field was already assigned elsewhere it was
                // left out.
                // Expanded Lead Editor
                // LoanInfo (Expanded)
                destDataLoan.sCcTemplateNm = srcDataLoan.sCcTemplateNm;
                destDataLoan.sFinMethDesc = srcDataLoan.sFinMethDesc;
                destDataLoan.sProRealETxMb_rep = srcDataLoan.sProRealETxMb_rep;
                destDataLoan.sProRealETxT = srcDataLoan.sProRealETxT;
                destDataLoan.sProRealETxR_rep = srcDataLoan.sProRealETxR_rep;
                destDataLoan.sProHoAssocDues_rep = srcDataLoan.sProHoAssocDues_rep;
                destDataLoan.sProOHExp_rep = srcDataLoan.sProOHExp_rep;
                destDataLoan.sProOHExpLckd = srcDataLoan.sProOHExpLckd;
                destDataLoan.sProHazInsT = srcDataLoan.sProHazInsT;
                destDataLoan.sProHazInsR_rep = srcDataLoan.sProHazInsR_rep;
                destDataLoan.sProHazInsMb_rep = srcDataLoan.sProHazInsMb_rep;
                destDataLoan.sProMIns_rep = srcDataLoan.sProMIns_rep;
                destDataLoan.sProMInsLckd = srcDataLoan.sProMInsLckd;
                destDataLoan.sGseSpT = srcDataLoan.sGseSpT;
                destDataLoan.sHomeIsMhAdvantageTri = srcDataLoan.sHomeIsMhAdvantageTri;
                destDataLoan.sOriginalAppraisedValue_rep = srcDataLoan.sOriginalAppraisedValue_rep;
                destDataLoan.sHighPricedMortgageT = srcDataLoan.sHighPricedMortgageT;
                destDataLoan.sHighPricedMortgageTLckd = srcDataLoan.sHighPricedMortgageTLckd;
                destDataLoan.sIsEmployeeLoan = srcDataLoan.sIsEmployeeLoan;
                destDataLoan.sConstructionPeriodMon_rep = srcDataLoan.sConstructionPeriodMon_rep;
                destDataLoan.sConstructionPeriodIR_rep = srcDataLoan.sConstructionPeriodIR_rep;
                destDataLoan.sLandCost_rep = srcDataLoan.sLandCost_rep;

                if (!LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(srcDataLoan.sLoanVersionT, LoanVersionT.V31_ConstructionLoans_ExposeNewDataPoints))
                {
                    destDataLoan.sConstructionImprovementAmt_rep = srcDataLoan.sConstructionImprovementAmt_rep;
                }

                // 3/11/14 gf - opm 172407, sIsLineOfCredit is no longer available in expanded 
                // lead editor. Honor template value.
                // destDataLoan.sIsLineOfCredit = srcDataLoan.sIsLineOfCredit;
                destDataLoan.sCreditLineAmt_rep = srcDataLoan.sCreditLineAmt_rep;

                // UpfrontMIP (Expanded)
                destDataLoan.sFfUfmipR_rep = srcDataLoan.sFfUfmipR_rep;
                destDataLoan.sFfUfmip1003_rep = srcDataLoan.sFfUfmip1003_rep;
                destDataLoan.sFfUfmip1003Lckd = srcDataLoan.sFfUfmip1003Lckd;
                destDataLoan.sMipFrequency = srcDataLoan.sMipFrequency;
                destDataLoan.sFfUfMipIsBeingFinanced = srcDataLoan.sFfUfMipIsBeingFinanced;
                destDataLoan.sMipPiaMon_rep = srcDataLoan.sMipPiaMon_rep;
                destDataLoan.sUfCashPdLckd = srcDataLoan.sUfCashPdLckd;
                destDataLoan.sUfCashPd_rep = srcDataLoan.sUfCashPd_rep;
                destDataLoan.sProMInsR_rep = srcDataLoan.sProMInsR_rep;
                destDataLoan.sProMInsMb_rep = srcDataLoan.sProMInsMb_rep;
                destDataLoan.sProMInsT = srcDataLoan.sProMInsT;
                destDataLoan.sIncludeUfmipInLtvCalc = srcDataLoan.sIncludeUfmipInLtvCalc;
                destDataLoan.sMiLenderPaidCoverage_rep = srcDataLoan.sMiLenderPaidCoverage_rep;
                destDataLoan.sMiCommitmentRequestedD_rep = srcDataLoan.sMiCommitmentRequestedD_rep;
                destDataLoan.sMiCommitmentReceivedD_rep = srcDataLoan.sMiCommitmentReceivedD_rep;
                destDataLoan.sMiCommitmentExpirationD_rep = srcDataLoan.sMiCommitmentExpirationD_rep;
                destDataLoan.sMiCertId = srcDataLoan.sMiCertId;
                destDataLoan.sProMInsMon_rep = srcDataLoan.sProMInsMon_rep;
                destDataLoan.sProMIns2Mon_rep = srcDataLoan.sProMIns2Mon_rep;
                destDataLoan.sProMInsR2_rep = srcDataLoan.sProMInsR2_rep;
                destDataLoan.sProMInsCancelLtv_rep = srcDataLoan.sProMInsCancelLtv_rep;
                destDataLoan.sProMInsCancelMinPmts_rep = srcDataLoan.sProMInsCancelMinPmts_rep;
                destDataLoan.sProMInsMidptCancel = srcDataLoan.sProMInsMidptCancel;
                destDataLoan.sMiInsuranceT = srcDataLoan.sMiInsuranceT;
                destDataLoan.sMiCompanyNmT = srcDataLoan.sMiCompanyNmT;
                destDataLoan.sLenderUfmipR = srcDataLoan.sLenderUfmipR;
                destDataLoan.sLenderUfmip = srcDataLoan.sLenderUfmip;
                destDataLoan.sLenderUfmipLckd = srcDataLoan.sLenderUfmipLckd;

                // Other Financing Info (Expanded)
                destDataLoan.sSubFin_rep = srcDataLoan.sSubFin_rep;
                destDataLoan.sConcurSubFin_rep = srcDataLoan.sConcurSubFin_rep;
                destDataLoan.sSubFinIR_rep = srcDataLoan.sSubFinIR_rep;
                destDataLoan.sSubFinTerm_rep = srcDataLoan.sSubFinTerm_rep;
                destDataLoan.sSubFinMb_rep = srcDataLoan.sSubFinMb_rep;
                destDataLoan.sSubFinPmt_rep = srcDataLoan.sSubFinPmt_rep;
                destDataLoan.sSubFinPmtLckd = srcDataLoan.sSubFinPmtLckd;
                destDataLoan.sIsIOnlyForSubFin = srcDataLoan.sIsIOnlyForSubFin;
                destDataLoan.sIsOFinNew = srcDataLoan.sIsOFinNew;
                destDataLoan.sIsOFinCreditLineInDrawPeriod = srcDataLoan.sIsOFinCreditLineInDrawPeriod;
                destDataLoan.sLayeredTotalForgivableBalance_rep = srcDataLoan.sLayeredTotalForgivableBalance_rep;
                destDataLoan.s1stMtgOrigLAmt_rep = srcDataLoan.s1stMtgOrigLAmt_rep;
                destDataLoan.sRemain1stMBal_rep = srcDataLoan.sRemain1stMBal_rep;
                destDataLoan.sRemain1stMPmt_rep = srcDataLoan.sRemain1stMPmt_rep;

                // 1003 Pg 1(Expanded)
                destDataLoan.sAgencyCaseNum = srcDataLoan.sAgencyCaseNum;
                destDataLoan.sDwnPmtSrc = srcDataLoan.sDwnPmtSrc;
                destDataLoan.sDwnPmtSrcExplain = srcDataLoan.sDwnPmtSrcExplain;
                destDataLoan.sEstateHeldT = srcDataLoan.sEstateHeldT;
                destDataLoan.sRefPurpose = srcDataLoan.sRefPurpose;
                destDataLoan.sLTODesc = srcDataLoan.sLTODesc;
                destDataLoan.sLeaseHoldExpireD_rep = srcDataLoan.sLeaseHoldExpireD_rep;
                destDataLoan.sLenderCaseNum = srcDataLoan.sLenderCaseNum;
                destDataLoan.sLenderCaseNumLckd = srcDataLoan.sLenderCaseNumLckd;
                destDataLoan.sLotAcqYr = srcDataLoan.sLotAcqYr;
                destDataLoan.sLotImprovC_rep = srcDataLoan.sLotImprovC_rep;
                destDataLoan.sLotLien_rep = srcDataLoan.sLotLien_rep;
                destDataLoan.sLotOrigC_rep = srcDataLoan.sLotOrigC_rep;
                destDataLoan.sLotVal_rep = srcDataLoan.sLotVal_rep;
                destDataLoan.sMultiApps = srcDataLoan.sMultiApps;
                destDataLoan.sOLPurposeDesc = srcDataLoan.sOLPurposeDesc;
                destDataLoan.sSpAcqYr = srcDataLoan.sSpAcqYr;
                destDataLoan.sSpCounty = srcDataLoan.sSpCounty;
                destDataLoan.sSpImprovC_rep = srcDataLoan.sSpImprovC_rep;
                destDataLoan.sSpImprovDesc = srcDataLoan.sSpImprovDesc;
                destDataLoan.sSpImprovTimeFrameT = srcDataLoan.sSpImprovTimeFrameT;
                destDataLoan.sSpLegalDesc = srcDataLoan.sSpLegalDesc;
                destDataLoan.sSpLien_rep = srcDataLoan.sSpLien_rep;
                destDataLoan.sSpOrigC_rep = srcDataLoan.sSpOrigC_rep;
                destDataLoan.sUnitsNum_rep = srcDataLoan.sUnitsNum_rep;
                destDataLoan.sYrBuilt = srcDataLoan.sYrBuilt;
                destDataLoan.sFinMethodPrintAsOther = srcDataLoan.sFinMethodPrintAsOther;
                destDataLoan.sFinMethPrintAsOtherDesc = srcDataLoan.sFinMethPrintAsOtherDesc;

                // 1003 Pg 2 (Expanded)
                destDataLoan.sProOHExpDesc = srcDataLoan.sProOHExpDesc;

                // 1003 Pg 3 (Expanded)
                destDataLoan.sAltCost_rep = srcDataLoan.sAltCost_rep;
                destDataLoan.sLDiscnt1003Lckd = srcDataLoan.sLDiscnt1003Lckd;
                destDataLoan.sLDiscnt1003_rep = srcDataLoan.sLDiscnt1003_rep;
                destDataLoan.sOCredit1Amt_rep = srcDataLoan.sOCredit1Amt_rep;
                destDataLoan.sOCredit1Desc = srcDataLoan.sOCredit1Desc;
                destDataLoan.sOCredit1Lckd = srcDataLoan.sOCredit1Lckd;

                // logic for sOCredit 2-4 moved to after sDisclosureRegulationT determination.
                destDataLoan.sRefPdOffAmt1003Lckd = srcDataLoan.sRefPdOffAmt1003Lckd;
                destDataLoan.sRefPdOffAmt1003_rep = srcDataLoan.sRefPdOffAmt1003_rep;
                destDataLoan.sTotCcPbsLocked = srcDataLoan.sTotCcPbsLocked;
                destDataLoan.sTotCcPbs_rep = srcDataLoan.sTotCcPbs_rep;
                destDataLoan.sTotEstCc1003Lckd = srcDataLoan.sTotEstCc1003Lckd;
                destDataLoan.sTotEstCcNoDiscnt1003_rep = srcDataLoan.sTotEstCcNoDiscnt1003_rep;
                destDataLoan.sTotEstPp1003Lckd = srcDataLoan.sTotEstPp1003Lckd;
                destDataLoan.sTotEstPp1003_rep = srcDataLoan.sTotEstPp1003_rep;
                destDataLoan.sTransNetCashLckd = srcDataLoan.sTransNetCashLckd;
                destDataLoan.sTransNetCash_rep = srcDataLoan.sTransNetCash_rep;
                destDataLoan.sONewFinCc_rep = srcDataLoan.sONewFinCc_rep;

                // GFE (Expanded)
                destDataLoan.sHazInsRsrvMonLckd = srcDataLoan.sHazInsRsrvMonLckd; // start opm 180983
                destDataLoan.sMInsRsrvMonLckd = srcDataLoan.sMInsRsrvMonLckd;
                destDataLoan.sRealETxRsrvMonLckd = srcDataLoan.sRealETxRsrvMonLckd;
                destDataLoan.sSchoolTxRsrvMonLckd = srcDataLoan.sSchoolTxRsrvMonLckd;
                destDataLoan.sFloodInsRsrvMonLckd = srcDataLoan.sFloodInsRsrvMonLckd;
                destDataLoan.s1006RsrvMonLckd = srcDataLoan.s1006RsrvMonLckd;
                destDataLoan.s1007RsrvMonLckd = srcDataLoan.s1007RsrvMonLckd;
                destDataLoan.sU3RsrvMonLckd = srcDataLoan.sU3RsrvMonLckd;
                destDataLoan.sU4RsrvMonLckd = srcDataLoan.sU4RsrvMonLckd;  // end opm 180983

                destDataLoan.sEstCloseD_rep = srcDataLoan.sEstCloseD_rep;
                destDataLoan.sEstCloseDLckd = srcDataLoan.sEstCloseDLckd;
                destDataLoan.sSchedDueD1_rep = srcDataLoan.sSchedDueD1_rep;
                destDataLoan.sSchedDueD1Lckd = srcDataLoan.sSchedDueD1Lckd;
                destDataLoan.sDaysInYr_rep = srcDataLoan.sDaysInYr_rep;
                destDataLoan.sRAdj1stCapR_rep = srcDataLoan.sRAdj1stCapR_rep;
                destDataLoan.sRAdjCapR_rep = srcDataLoan.sRAdjCapR_rep;
                destDataLoan.sRAdjLifeCapR_rep = srcDataLoan.sRAdjLifeCapR_rep;
                destDataLoan.sRAdjCapMon_rep = srcDataLoan.sRAdjCapMon_rep;
                destDataLoan.sRAdj1stCapMon_rep = srcDataLoan.sRAdj1stCapMon_rep;
                destDataLoan.sPmtAdjCapR_rep = srcDataLoan.sPmtAdjCapR_rep;
                destDataLoan.sPmtAdjMaxBalPc_rep = srcDataLoan.sPmtAdjMaxBalPc_rep;
                destDataLoan.sPmtAdjRecastStop_rep = srcDataLoan.sPmtAdjRecastStop_rep;
                destDataLoan.sPmtAdjRecastPeriodMon_rep = srcDataLoan.sPmtAdjRecastPeriodMon_rep;
                destDataLoan.sPmtAdjCapMon_rep = srcDataLoan.sPmtAdjCapMon_rep;
                destDataLoan.sIPiaDyLckd = srcDataLoan.sIPiaDyLckd;
                destDataLoan.sConsummationD_rep = srcDataLoan.sConsummationD_rep;
                destDataLoan.sConsummationDLckd = srcDataLoan.sConsummationDLckd;
                destDataLoan.sGfeNoteIRAvailTillD = srcDataLoan.sGfeNoteIRAvailTillD;
                destDataLoan.sGfeEstScAvailTillD = srcDataLoan.sGfeEstScAvailTillD;
                destDataLoan.sGfeRateLockPeriod_rep = srcDataLoan.sGfeRateLockPeriod_rep;
                destDataLoan.sGfeLockPeriodBeforeSettlement_rep = srcDataLoan.sGfeLockPeriodBeforeSettlement_rep;
                destDataLoan.sGfeHavePpmtPenalty = srcDataLoan.sGfeHavePpmtPenalty;
                destDataLoan.sGfeMaxPpmtPenaltyAmt_rep = srcDataLoan.sGfeMaxPpmtPenaltyAmt_rep;
                destDataLoan.sMldsHasImpound = srcDataLoan.sMldsHasImpound;
                destDataLoan.sGfeUsePaidToFromOfficialContact = srcDataLoan.sGfeUsePaidToFromOfficialContact;
                destDataLoan.sGfeNoteIRAvailTillDTimeZoneT = srcDataLoan.sGfeNoteIRAvailTillDTimeZoneT;
                destDataLoan.sGfeEstScAvailTillDTimeZoneT = srcDataLoan.sGfeEstScAvailTillDTimeZoneT;
                destDataLoan.sIsPrintTimeForGfeNoteIRAvailTillD = srcDataLoan.sIsPrintTimeForGfeNoteIRAvailTillD;
                destDataLoan.sIsPrintTimeForsGfeEstScAvailTillD = srcDataLoan.sIsPrintTimeForsGfeEstScAvailTillD;
                destDataLoan.s800U5F_rep = srcDataLoan.s800U5F_rep;
                destDataLoan.s800U5FDesc = srcDataLoan.s800U5FDesc;
                destDataLoan.s800U4F_rep = srcDataLoan.s800U4F_rep;
                destDataLoan.s800U4FDesc = srcDataLoan.s800U4FDesc;
                destDataLoan.s800U3F_rep = srcDataLoan.s800U3F_rep;
                destDataLoan.s800U3FDesc = srcDataLoan.s800U3FDesc;
                destDataLoan.s800U2F_rep = srcDataLoan.s800U2F_rep;
                destDataLoan.s800U2FDesc = srcDataLoan.s800U2FDesc;
                destDataLoan.s800U1F_rep = srcDataLoan.s800U1F_rep;
                destDataLoan.s800U1FDesc = srcDataLoan.s800U1FDesc;
                destDataLoan.sWireF_rep = srcDataLoan.sWireF_rep;
                destDataLoan.sUwF_rep = srcDataLoan.sUwF_rep;
                destDataLoan.sProcF_rep = srcDataLoan.sProcF_rep;
                destDataLoan.sProcFPaid = srcDataLoan.sProcFPaid;
                destDataLoan.sTxServF_rep = srcDataLoan.sTxServF_rep;
                destDataLoan.sFloodCertificationDeterminationT = srcDataLoan.sFloodCertificationDeterminationT;
                destDataLoan.sMBrokFMb_rep = srcDataLoan.sMBrokFMb_rep;
                destDataLoan.sMBrokFPc_rep = srcDataLoan.sMBrokFPc_rep;
                destDataLoan.sMBrokFBaseT = srcDataLoan.sMBrokFBaseT;
                destDataLoan.sInspectF_rep = srcDataLoan.sInspectF_rep;
                destDataLoan.sCrF_rep = srcDataLoan.sCrF_rep;
                destDataLoan.sCrFPaid = srcDataLoan.sCrFPaid;
                destDataLoan.sApprF_rep = srcDataLoan.sApprF_rep;
                destDataLoan.sApprFPaid = srcDataLoan.sApprFPaid;
                destDataLoan.sFloodCertificationF_rep = srcDataLoan.sFloodCertificationF_rep;
                destDataLoan.sLDiscntFMb_rep = srcDataLoan.sLDiscntFMb_rep;
                destDataLoan.sLDiscntPc_rep = srcDataLoan.sLDiscntPc_rep;
                destDataLoan.sLDiscntBaseT = srcDataLoan.sLDiscntBaseT;

                // opm 464662
                destDataLoan.sLoanTransactionInvolvesSeller = srcDataLoan.sLoanTransactionInvolvesSeller;
                destDataLoan.sLoanTransactionInvolvesSellerLckd = srcDataLoan.sLoanTransactionInvolvesSellerLckd;
                destDataLoan.sIsRequireAlternateCashToCloseForNonSellerTransactions = srcDataLoan.sIsRequireAlternateCashToCloseForNonSellerTransactions;

                if (broker.IsGFEandCoCVersioningEnabled)
                {
                    if (srcDataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
                    {
                        destDataLoan.sLastDisclosedGFEArchiveD_rep = srcDataLoan.sLastDisclosedGFEArchiveD_rep;
                    }
                }

                destDataLoan.sLOrigFMb_rep = srcDataLoan.sLOrigFMb_rep;
                destDataLoan.sLOrigFPc_rep = srcDataLoan.sLOrigFPc_rep;
                destDataLoan.s900U1Pia_rep = srcDataLoan.s900U1Pia_rep;
                destDataLoan.s900U1PiaDesc = srcDataLoan.s900U1PiaDesc;
                destDataLoan.s904PiaDesc = srcDataLoan.s904PiaDesc;
                destDataLoan.s904Pia_rep = srcDataLoan.s904Pia_rep;
                destDataLoan.sHazInsPiaMon_rep = srcDataLoan.sHazInsPiaMon_rep;
                destDataLoan.sIPiaDy_rep = srcDataLoan.sIPiaDy_rep;
                destDataLoan.sIPerDayLckd = srcDataLoan.sIPerDayLckd;
                destDataLoan.sIPerDay_rep = srcDataLoan.sIPerDay_rep;
                destDataLoan.sAggregateAdjRsrv_rep = srcDataLoan.sAggregateAdjRsrv_rep;
                destDataLoan.sAggregateAdjRsrvLckd = srcDataLoan.sAggregateAdjRsrvLckd;
                destDataLoan.s1007ProHExp_rep = srcDataLoan.s1007ProHExp_rep;
                destDataLoan.s1007RsrvMon_rep = srcDataLoan.s1007RsrvMon_rep;
                destDataLoan.s1007ProHExpDesc = srcDataLoan.s1007ProHExpDesc;
                destDataLoan.s1006ProHExp_rep = srcDataLoan.s1006ProHExp_rep;
                destDataLoan.s1006RsrvMon_rep = srcDataLoan.s1006RsrvMon_rep;
                destDataLoan.s1006ProHExpDesc = srcDataLoan.s1006ProHExpDesc;
                destDataLoan.sProU3Rsrv_rep = srcDataLoan.sProU3Rsrv_rep;
                destDataLoan.sU3RsrvMon_rep = srcDataLoan.sU3RsrvMon_rep;
                destDataLoan.sU3RsrvDesc = srcDataLoan.sU3RsrvDesc;
                destDataLoan.sProU4Rsrv_rep = srcDataLoan.sProU4Rsrv_rep;
                destDataLoan.sU4RsrvMon_rep = srcDataLoan.sU4RsrvMon_rep;
                destDataLoan.sU4RsrvDesc = srcDataLoan.sU4RsrvDesc;
                destDataLoan.sProFloodIns_rep = srcDataLoan.sProFloodIns_rep;
                destDataLoan.sFloodInsRsrvMon_rep = srcDataLoan.sFloodInsRsrvMon_rep;
                destDataLoan.sRealETxRsrvMon_rep = srcDataLoan.sRealETxRsrvMon_rep;
                destDataLoan.sProSchoolTx_rep = srcDataLoan.sProSchoolTx_rep;
                destDataLoan.sSchoolTxRsrvMon_rep = srcDataLoan.sSchoolTxRsrvMon_rep;
                destDataLoan.sMInsRsrvMon_rep = srcDataLoan.sMInsRsrvMon_rep;
                destDataLoan.sHazInsRsrvMon_rep = srcDataLoan.sHazInsRsrvMon_rep;
                destDataLoan.sU4Tc_rep = srcDataLoan.sU4Tc_rep;
                destDataLoan.sU4TcDesc = srcDataLoan.sU4TcDesc;
                destDataLoan.sU3Tc_rep = srcDataLoan.sU3Tc_rep;
                destDataLoan.sU3TcDesc = srcDataLoan.sU3TcDesc;
                destDataLoan.sU2Tc_rep = srcDataLoan.sU2Tc_rep;
                destDataLoan.sU2TcDesc = srcDataLoan.sU2TcDesc;
                destDataLoan.sU1Tc_rep = srcDataLoan.sU1Tc_rep;
                destDataLoan.sU1TcDesc = srcDataLoan.sU1TcDesc;
                destDataLoan.sOwnerTitleInsF_rep = srcDataLoan.sOwnerTitleInsF_rep;
                destDataLoan.sTitleInsF_rep = srcDataLoan.sTitleInsF_rep;
                destDataLoan.sTitleInsFTable = srcDataLoan.sTitleInsFTable;
                destDataLoan.sAttorneyF_rep = srcDataLoan.sAttorneyF_rep;
                destDataLoan.sNotaryF_rep = srcDataLoan.sNotaryF_rep;
                destDataLoan.sDocPrepF_rep = srcDataLoan.sDocPrepF_rep;
                destDataLoan.sEscrowF_rep = srcDataLoan.sEscrowF_rep;
                destDataLoan.sEscrowFTable = srcDataLoan.sEscrowFTable;
                destDataLoan.sU3GovRtcMb_rep = srcDataLoan.sU3GovRtcMb_rep;
                destDataLoan.sU3GovRtcBaseT = srcDataLoan.sU3GovRtcBaseT;
                destDataLoan.sU3GovRtcPc_rep = srcDataLoan.sU3GovRtcPc_rep;
                destDataLoan.sU3GovRtcDesc = srcDataLoan.sU3GovRtcDesc;
                destDataLoan.sU2GovRtcMb_rep = srcDataLoan.sU2GovRtcMb_rep;
                destDataLoan.sU2GovRtcBaseT = srcDataLoan.sU2GovRtcBaseT;
                destDataLoan.sU2GovRtcPc_rep = srcDataLoan.sU2GovRtcPc_rep;
                destDataLoan.sU2GovRtcDesc = srcDataLoan.sU2GovRtcDesc;
                destDataLoan.sU1GovRtcMb_rep = srcDataLoan.sU1GovRtcMb_rep;
                destDataLoan.sU1GovRtcBaseT = srcDataLoan.sU1GovRtcBaseT;
                destDataLoan.sU1GovRtcPc_rep = srcDataLoan.sU1GovRtcPc_rep;
                destDataLoan.sU1GovRtcDesc = srcDataLoan.sU1GovRtcDesc;
                destDataLoan.sStateRtcDesc = srcDataLoan.sStateRtcDesc;
                destDataLoan.sStateRtcMb_rep = srcDataLoan.sStateRtcMb_rep;
                destDataLoan.sStateRtcBaseT = srcDataLoan.sStateRtcBaseT;
                destDataLoan.sStateRtcPc_rep = srcDataLoan.sStateRtcPc_rep;
                destDataLoan.sCountyRtcDesc = srcDataLoan.sCountyRtcDesc;
                destDataLoan.sCountyRtcMb_rep = srcDataLoan.sCountyRtcMb_rep;
                destDataLoan.sCountyRtcBaseT = srcDataLoan.sCountyRtcBaseT;
                destDataLoan.sCountyRtcPc_rep = srcDataLoan.sCountyRtcPc_rep;
                destDataLoan.sRecFDesc = srcDataLoan.sRecFDesc;
                destDataLoan.sRecFMb_rep = srcDataLoan.sRecFMb_rep;
                destDataLoan.sRecBaseT = srcDataLoan.sRecBaseT;
                destDataLoan.sRecFPc_rep = srcDataLoan.sRecFPc_rep;
                destDataLoan.sRecFLckd = srcDataLoan.sRecFLckd;
                destDataLoan.sRecDeed_rep = srcDataLoan.sRecDeed_rep;
                destDataLoan.sRecMortgage_rep = srcDataLoan.sRecMortgage_rep;
                destDataLoan.sRecRelease_rep = srcDataLoan.sRecRelease_rep;
                destDataLoan.sU5Sc_rep = srcDataLoan.sU5Sc_rep;
                destDataLoan.sU5ScDesc = srcDataLoan.sU5ScDesc;
                destDataLoan.sU4Sc_rep = srcDataLoan.sU4Sc_rep;
                destDataLoan.sU4ScDesc = srcDataLoan.sU4ScDesc;
                destDataLoan.sU3Sc_rep = srcDataLoan.sU3Sc_rep;
                destDataLoan.sU3ScDesc = srcDataLoan.sU3ScDesc;
                destDataLoan.sU2Sc_rep = srcDataLoan.sU2Sc_rep;
                destDataLoan.sU2ScDesc = srcDataLoan.sU2ScDesc;
                destDataLoan.sU1Sc_rep = srcDataLoan.sU1Sc_rep;
                destDataLoan.sU1ScDesc = srcDataLoan.sU1ScDesc;
                destDataLoan.sPestInspectF_rep = srcDataLoan.sPestInspectF_rep;
                destDataLoan.sGfeCreditLenderPaidItemT = srcDataLoan.sGfeCreditLenderPaidItemT;
                destDataLoan.sLOrigFProps = srcDataLoan.sLOrigFProps;
                destDataLoan.sGfeOriginatorCompFProps = srcDataLoan.sGfeOriginatorCompFProps;
                destDataLoan.sGfeLenderCreditFProps = srcDataLoan.sGfeLenderCreditFProps;
                destDataLoan.sGfeDiscountPointFProps = srcDataLoan.sGfeDiscountPointFProps;
                destDataLoan.sApprFProps = srcDataLoan.sApprFProps;
                destDataLoan.sCrFProps = srcDataLoan.sCrFProps;
                destDataLoan.sInspectFProps = srcDataLoan.sInspectFProps;
                destDataLoan.sMBrokFProps = srcDataLoan.sMBrokFProps;
                destDataLoan.sTxServFProps = srcDataLoan.sTxServFProps;
                destDataLoan.sFloodCertificationFProps = srcDataLoan.sFloodCertificationFProps;
                destDataLoan.sProcFProps = srcDataLoan.sProcFProps;
                destDataLoan.sUwFProps = srcDataLoan.sUwFProps;
                destDataLoan.sWireFProps = srcDataLoan.sWireFProps;
                destDataLoan.s800U1FProps = srcDataLoan.s800U1FProps;
                destDataLoan.s800U2FProps = srcDataLoan.s800U2FProps;
                destDataLoan.s800U3FProps = srcDataLoan.s800U3FProps;
                destDataLoan.s800U4FProps = srcDataLoan.s800U4FProps;
                destDataLoan.s800U5FProps = srcDataLoan.s800U5FProps;
                destDataLoan.sIPiaProps = srcDataLoan.sIPiaProps;
                destDataLoan.sMipPiaProps = srcDataLoan.sMipPiaProps;
                destDataLoan.sHazInsPiaProps = srcDataLoan.sHazInsPiaProps;
                destDataLoan.s904PiaProps = srcDataLoan.s904PiaProps;
                destDataLoan.sVaFfProps = srcDataLoan.sVaFfProps;
                destDataLoan.s900U1PiaProps = srcDataLoan.s900U1PiaProps;
                destDataLoan.sHazInsRsrvProps = srcDataLoan.sHazInsRsrvProps;
                destDataLoan.sMInsRsrvProps = srcDataLoan.sMInsRsrvProps;
                destDataLoan.sSchoolTxRsrvProps = srcDataLoan.sSchoolTxRsrvProps;
                destDataLoan.sRealETxRsrvProps = srcDataLoan.sRealETxRsrvProps;
                destDataLoan.sFloodInsRsrvProps = srcDataLoan.sFloodInsRsrvProps;
                destDataLoan.s1006RsrvProps = srcDataLoan.s1006RsrvProps;
                destDataLoan.s1007RsrvProps = srcDataLoan.s1007RsrvProps;
                destDataLoan.sAggregateAdjRsrvProps = srcDataLoan.sAggregateAdjRsrvProps;
                destDataLoan.sEscrowFProps = srcDataLoan.sEscrowFProps;
                destDataLoan.sOwnerTitleInsProps = srcDataLoan.sOwnerTitleInsProps;
                destDataLoan.sDocPrepFProps = srcDataLoan.sDocPrepFProps;
                destDataLoan.sNotaryFProps = srcDataLoan.sNotaryFProps;
                destDataLoan.sAttorneyFProps = srcDataLoan.sAttorneyFProps;
                destDataLoan.sTitleInsFProps = srcDataLoan.sTitleInsFProps;
                destDataLoan.sU1TcProps = srcDataLoan.sU1TcProps;
                destDataLoan.sU2TcProps = srcDataLoan.sU2TcProps;
                destDataLoan.sU3TcProps = srcDataLoan.sU3TcProps;
                destDataLoan.sU4TcProps = srcDataLoan.sU4TcProps;
                destDataLoan.sRecFProps = srcDataLoan.sRecFProps;
                destDataLoan.sCountyRtcProps = srcDataLoan.sCountyRtcProps;
                destDataLoan.sStateRtcProps = srcDataLoan.sStateRtcProps;
                destDataLoan.sU1GovRtcProps = srcDataLoan.sU1GovRtcProps;
                destDataLoan.sU2GovRtcProps = srcDataLoan.sU2GovRtcProps;
                destDataLoan.sU3GovRtcProps = srcDataLoan.sU3GovRtcProps;
                destDataLoan.sPestInspectFProps = srcDataLoan.sPestInspectFProps;
                destDataLoan.sU1ScProps = srcDataLoan.sU1ScProps;
                destDataLoan.sU2ScProps = srcDataLoan.sU2ScProps;
                destDataLoan.sU3ScProps = srcDataLoan.sU3ScProps;
                destDataLoan.sU4ScProps = srcDataLoan.sU4ScProps;
                destDataLoan.sU5ScProps = srcDataLoan.sU5ScProps;
                destDataLoan.sApprFPaidTo = srcDataLoan.sApprFPaidTo;
                destDataLoan.sCrFPaidTo = srcDataLoan.sCrFPaidTo;
                destDataLoan.sTxServFPaidTo = srcDataLoan.sTxServFPaidTo;
                destDataLoan.sFloodCertificationFPaidTo = srcDataLoan.sFloodCertificationFPaidTo;
                destDataLoan.sInspectFPaidTo = srcDataLoan.sInspectFPaidTo;
                destDataLoan.sProcFPaidTo = srcDataLoan.sProcFPaidTo;
                destDataLoan.sUwFPaidTo = srcDataLoan.sUwFPaidTo;
                destDataLoan.sWireFPaidTo = srcDataLoan.sWireFPaidTo;
                destDataLoan.s800U1FPaidTo = srcDataLoan.s800U1FPaidTo;
                destDataLoan.s800U2FPaidTo = srcDataLoan.s800U2FPaidTo;
                destDataLoan.s800U3FPaidTo = srcDataLoan.s800U3FPaidTo;
                destDataLoan.s800U4FPaidTo = srcDataLoan.s800U4FPaidTo;
                destDataLoan.s800U5FPaidTo = srcDataLoan.s800U5FPaidTo;
                destDataLoan.sOwnerTitleInsPaidTo = srcDataLoan.sOwnerTitleInsPaidTo;
                destDataLoan.sDocPrepFPaidTo = srcDataLoan.sDocPrepFPaidTo;
                destDataLoan.sNotaryFPaidTo = srcDataLoan.sNotaryFPaidTo;
                destDataLoan.sAttorneyFPaidTo = srcDataLoan.sAttorneyFPaidTo;
                destDataLoan.sU1TcPaidTo = srcDataLoan.sU1TcPaidTo;
                destDataLoan.sU2TcPaidTo = srcDataLoan.sU2TcPaidTo;
                destDataLoan.sU3TcPaidTo = srcDataLoan.sU3TcPaidTo;
                destDataLoan.sU4TcPaidTo = srcDataLoan.sU4TcPaidTo;
                destDataLoan.sU1GovRtcPaidTo = srcDataLoan.sU1GovRtcPaidTo;
                destDataLoan.sU2GovRtcPaidTo = srcDataLoan.sU2GovRtcPaidTo;
                destDataLoan.sU3GovRtcPaidTo = srcDataLoan.sU3GovRtcPaidTo;
                destDataLoan.sPestInspectPaidTo = srcDataLoan.sPestInspectPaidTo;
                destDataLoan.sU1ScPaidTo = srcDataLoan.sU1ScPaidTo;
                destDataLoan.sU2ScPaidTo = srcDataLoan.sU2ScPaidTo;
                destDataLoan.sU3ScPaidTo = srcDataLoan.sU3ScPaidTo;
                destDataLoan.sU4ScPaidTo = srcDataLoan.sU4ScPaidTo;
                destDataLoan.sU5ScPaidTo = srcDataLoan.sU5ScPaidTo;
                destDataLoan.sHazInsPiaPaidTo = srcDataLoan.sHazInsPiaPaidTo;
                destDataLoan.sMipPiaPaidTo = srcDataLoan.sMipPiaPaidTo;
                destDataLoan.sVaFfPaidTo = srcDataLoan.sVaFfPaidTo;
                destDataLoan.s800U1FGfeSection = srcDataLoan.s800U1FGfeSection;
                destDataLoan.s800U2FGfeSection = srcDataLoan.s800U2FGfeSection;
                destDataLoan.s800U3FGfeSection = srcDataLoan.s800U3FGfeSection;
                destDataLoan.s800U4FGfeSection = srcDataLoan.s800U4FGfeSection;
                destDataLoan.s800U5FGfeSection = srcDataLoan.s800U5FGfeSection;
                destDataLoan.sEscrowFGfeSection = srcDataLoan.sEscrowFGfeSection;
                destDataLoan.sDocPrepFGfeSection = srcDataLoan.sDocPrepFGfeSection;
                destDataLoan.sNotaryFGfeSection = srcDataLoan.sNotaryFGfeSection;
                destDataLoan.sAttorneyFGfeSection = srcDataLoan.sAttorneyFGfeSection;
                destDataLoan.sU1TcGfeSection = srcDataLoan.sU1TcGfeSection;
                destDataLoan.sU2TcGfeSection = srcDataLoan.sU2TcGfeSection;
                destDataLoan.sU3TcGfeSection = srcDataLoan.sU3TcGfeSection;
                destDataLoan.sU4TcGfeSection = srcDataLoan.sU4TcGfeSection;
                destDataLoan.sU1GovRtcGfeSection = srcDataLoan.sU1GovRtcGfeSection;
                destDataLoan.sU2GovRtcGfeSection = srcDataLoan.sU2GovRtcGfeSection;
                destDataLoan.sU3GovRtcGfeSection = srcDataLoan.sU3GovRtcGfeSection;
                destDataLoan.sU1ScGfeSection = srcDataLoan.sU1ScGfeSection;
                destDataLoan.sU2ScGfeSection = srcDataLoan.sU2ScGfeSection;
                destDataLoan.sU3ScGfeSection = srcDataLoan.sU3ScGfeSection;
                destDataLoan.sU4ScGfeSection = srcDataLoan.sU4ScGfeSection;
                destDataLoan.sU5ScGfeSection = srcDataLoan.sU5ScGfeSection;
                destDataLoan.s904PiaGfeSection = srcDataLoan.s904PiaGfeSection;
                destDataLoan.s900U1PiaGfeSection = srcDataLoan.s900U1PiaGfeSection;
                destDataLoan.sGfeTradeOffLowerCCLoanAmt_rep = srcDataLoan.sGfeTradeOffLowerCCLoanAmt_rep;
                destDataLoan.sGfeTradeOffLowerCCNoteIR_rep = srcDataLoan.sGfeTradeOffLowerCCNoteIR_rep;
                destDataLoan.sGfeTradeOffLowerCCClosingCost_rep = srcDataLoan.sGfeTradeOffLowerCCClosingCost_rep;
                destDataLoan.sGfeTradeOffLowerRateLoanAmt_rep = srcDataLoan.sGfeTradeOffLowerRateLoanAmt_rep;
                destDataLoan.sGfeTradeOffLowerRateNoteIR_rep = srcDataLoan.sGfeTradeOffLowerRateNoteIR_rep;
                destDataLoan.sGfeTradeOffLowerRateClosingCost_rep = srcDataLoan.sGfeTradeOffLowerRateClosingCost_rep;
                destDataLoan.sGfeShoppingCartLoan1OriginatorName = srcDataLoan.sGfeShoppingCartLoan1OriginatorName;
                destDataLoan.sGfeShoppingCartLoan1LoanAmt_rep = srcDataLoan.sGfeShoppingCartLoan1LoanAmt_rep;
                destDataLoan.sGfeShoppingCartLoan1LoanTerm_rep = srcDataLoan.sGfeShoppingCartLoan1LoanTerm_rep;
                destDataLoan.sGfeShoppingCartLoan1NoteIR_rep = srcDataLoan.sGfeShoppingCartLoan1NoteIR_rep;
                destDataLoan.sGfeShoppingCartLoan1InitialPmt_rep = srcDataLoan.sGfeShoppingCartLoan1InitialPmt_rep;
                destDataLoan.sGfeShoppingCartLoan1RateLockPeriod_rep = srcDataLoan.sGfeShoppingCartLoan1RateLockPeriod_rep;
                destDataLoan.sGfeShoppingCartLoan1CanRateIncreaseTri = srcDataLoan.sGfeShoppingCartLoan1CanRateIncreaseTri;
                destDataLoan.sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri = srcDataLoan.sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri;
                destDataLoan.sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri = srcDataLoan.sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri;
                destDataLoan.sGfeShoppingCartLoan1HavePpmtPenaltyTri = srcDataLoan.sGfeShoppingCartLoan1HavePpmtPenaltyTri;
                destDataLoan.sGfeShoppingCartLoan1IsBalloonTri = srcDataLoan.sGfeShoppingCartLoan1IsBalloonTri;
                destDataLoan.sGfeShoppingCartLoan1TotalClosingCost_rep = srcDataLoan.sGfeShoppingCartLoan1TotalClosingCost_rep;
                destDataLoan.sGfeShoppingCartLoan2OriginatorName = srcDataLoan.sGfeShoppingCartLoan2OriginatorName;
                destDataLoan.sGfeShoppingCartLoan2LoanAmt_rep = srcDataLoan.sGfeShoppingCartLoan2LoanAmt_rep;
                destDataLoan.sGfeShoppingCartLoan2LoanTerm_rep = srcDataLoan.sGfeShoppingCartLoan2LoanTerm_rep;
                destDataLoan.sGfeShoppingCartLoan2NoteIR_rep = srcDataLoan.sGfeShoppingCartLoan2NoteIR_rep;
                destDataLoan.sGfeShoppingCartLoan2InitialPmt_rep = srcDataLoan.sGfeShoppingCartLoan2InitialPmt_rep;
                destDataLoan.sGfeShoppingCartLoan2RateLockPeriod_rep = srcDataLoan.sGfeShoppingCartLoan2RateLockPeriod_rep;
                destDataLoan.sGfeShoppingCartLoan2CanRateIncreaseTri = srcDataLoan.sGfeShoppingCartLoan2CanRateIncreaseTri;
                destDataLoan.sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri = srcDataLoan.sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri;
                destDataLoan.sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri = srcDataLoan.sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri;
                destDataLoan.sGfeShoppingCartLoan2HavePpmtPenaltyTri = srcDataLoan.sGfeShoppingCartLoan2HavePpmtPenaltyTri;
                destDataLoan.sGfeShoppingCartLoan2IsBalloonTri = srcDataLoan.sGfeShoppingCartLoan2IsBalloonTri;
                destDataLoan.sGfeShoppingCartLoan2TotalClosingCost_rep = srcDataLoan.sGfeShoppingCartLoan2TotalClosingCost_rep;
                destDataLoan.sGfeShoppingCartLoan3OriginatorName = srcDataLoan.sGfeShoppingCartLoan3OriginatorName;
                destDataLoan.sGfeShoppingCartLoan3LoanAmt_rep = srcDataLoan.sGfeShoppingCartLoan3LoanAmt_rep;
                destDataLoan.sGfeShoppingCartLoan3LoanTerm_rep = srcDataLoan.sGfeShoppingCartLoan3LoanTerm_rep;
                destDataLoan.sGfeShoppingCartLoan3NoteIR_rep = srcDataLoan.sGfeShoppingCartLoan3NoteIR_rep;
                destDataLoan.sGfeShoppingCartLoan3InitialPmt_rep = srcDataLoan.sGfeShoppingCartLoan3InitialPmt_rep;
                destDataLoan.sGfeShoppingCartLoan3RateLockPeriod_rep = srcDataLoan.sGfeShoppingCartLoan3RateLockPeriod_rep;
                destDataLoan.sGfeShoppingCartLoan3CanRateIncreaseTri = srcDataLoan.sGfeShoppingCartLoan3CanRateIncreaseTri;
                destDataLoan.sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri = srcDataLoan.sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri;
                destDataLoan.sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri = srcDataLoan.sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri;
                destDataLoan.sGfeShoppingCartLoan3HavePpmtPenaltyTri = srcDataLoan.sGfeShoppingCartLoan3HavePpmtPenaltyTri;
                destDataLoan.sGfeShoppingCartLoan3IsBalloonTri = srcDataLoan.sGfeShoppingCartLoan3IsBalloonTri;
                destDataLoan.sGfeShoppingCartLoan3TotalClosingCost_rep = srcDataLoan.sGfeShoppingCartLoan3TotalClosingCost_rep;
                destDataLoan.sGfeIsTPOTransaction = srcDataLoan.sGfeIsTPOTransaction;
                destDataLoan.sIsItemizeBrokerCommissionOnIFW = srcDataLoan.sIsItemizeBrokerCommissionOnIFW;
                destDataLoan.SetOriginatorCompensation(
                    srcDataLoan.sOriginatorCompensationPaymentSourceT,
                    srcDataLoan.sGfeOriginatorCompFPc_rep,
                    srcDataLoan.sGfeOriginatorCompFBaseT,
                    srcDataLoan.sGfeOriginatorCompFMb_rep);
                destDataLoan.sGfeRedisclosureD_rep = srcDataLoan.sGfeRedisclosureD_rep;
                destDataLoan.Set_sGfeInitialDisclosureD_rep(srcDataLoan.sGfeInitialDisclosureD_rep, E_GFEArchivedReasonT.InitialDislosureDateSetViaLeadConversionPML);

                // Credit Denial (Expanded)
                destDataLoan.sRejectD_rep = srcDataLoan.sRejectD_rep;
                destDataLoan.sHmdaDeniedFormDoneD_rep = srcDataLoan.sHmdaDeniedFormDoneD_rep;
                destDataLoan.sHmdaDeniedFormDone = srcDataLoan.sHmdaDeniedFormDone;
                destDataLoan.sHmdaDeniedFormDoneBy = srcDataLoan.sHmdaDeniedFormDoneBy;

                // VA Loan Analysis (Expanded)
                destDataLoan.sVaCashdwnPmt_rep = srcDataLoan.sVaCashdwnPmt_rep;
                destDataLoan.sVaCashdwnPmtLckd = srcDataLoan.sVaCashdwnPmtLckd;
                destDataLoan.sVaMaintainAssessPmtLckd = srcDataLoan.sVaMaintainAssessPmtLckd;
                destDataLoan.sVaMaintainAssessPmt_rep = srcDataLoan.sVaMaintainAssessPmt_rep;
                destDataLoan.sVaProHazIns_rep = srcDataLoan.sVaProHazIns_rep;
                destDataLoan.sVaProHazInsLckd = srcDataLoan.sVaProHazInsLckd;
                destDataLoan.sVaProMaintenancePmt_rep = srcDataLoan.sVaProMaintenancePmt_rep;
                destDataLoan.sVaProRealETx_rep = srcDataLoan.sVaProRealETx_rep;
                destDataLoan.sVaProRealETxLckd = srcDataLoan.sVaProRealETxLckd;
                destDataLoan.sVaProThisMPmt_rep = srcDataLoan.sVaProThisMPmt_rep;
                destDataLoan.sVaProThisMPmtLckd = srcDataLoan.sVaProThisMPmtLckd;
                destDataLoan.sVaProUtilityPmt_rep = srcDataLoan.sVaProUtilityPmt_rep;
                destDataLoan.sVaSpecialAssessPmt_rep = srcDataLoan.sVaSpecialAssessPmt_rep;

                // Submit to DO/DU (Expanded)
                if (broker.IsExpandedLeadEditorDoEnabled || broker.IsExpandedLeadEditorDuEnabled)
                {
                    destDataLoan.sFannieARMPlanNum_rep = srcDataLoan.sFannieARMPlanNum_rep;
                    destDataLoan.sFannieARMPlanNumLckd = srcDataLoan.sFannieARMPlanNumLckd;
                    destDataLoan.sIsSellerProvidedBelowMktFin = srcDataLoan.sIsSellerProvidedBelowMktFin;
                    destDataLoan.sArmIndexT = srcDataLoan.sArmIndexT;
                    destDataLoan.sArmIndexTLckd = srcDataLoan.sArmIndexTLckd;
                    destDataLoan.sFannieSpT = srcDataLoan.sFannieSpT;
                    destDataLoan.sFannieDocT = srcDataLoan.sFannieDocT;
                    destDataLoan.sDuCaseId = srcDataLoan.sDuCaseId;
                    destDataLoan.sIsCommunityLending = srcDataLoan.sIsCommunityLending;
                    destDataLoan.sFannieMsa = srcDataLoan.sFannieMsa;
                    destDataLoan.sFannieCommunityLendingT = srcDataLoan.sFannieCommunityLendingT;
                    destDataLoan.sIsFannieNeighbors = srcDataLoan.sIsFannieNeighbors;
                    destDataLoan.sIsCommunitySecond = srcDataLoan.sIsCommunitySecond;
                    destDataLoan.sFannieIncomeLimitAdjPc_rep = srcDataLoan.sFannieIncomeLimitAdjPc_rep;
                    destDataLoan.sFannieProdDesc = srcDataLoan.sFannieProdDesc;
                    destDataLoan.sExportAdditionalLiabitiesFannieMae = srcDataLoan.sExportAdditionalLiabitiesFannieMae;
                    destDataLoan.sExportAdditionalLiabitiesDODU = srcDataLoan.sExportAdditionalLiabitiesDODU;

                    destDataLoan.sAusRecommendation = srcDataLoan.sAusRecommendation;
                    destDataLoan.sDuFindingsHtml = srcDataLoan.sDuFindingsHtml;
                }

                // Submit to LP (Expanded)
                if (broker.IsExpandedLeadEditorLpEnabled)
                {
                    destDataLoan.sBuildingStatusT = srcDataLoan.sBuildingStatusT;
                    destDataLoan.sBuydown = srcDataLoan.sBuydown;
                    destDataLoan.sBuydownContributorT = srcDataLoan.sBuydownContributorT;
                    destDataLoan.sFHASalesConcessions_rep = srcDataLoan.sFHASalesConcessions_rep;
                    destDataLoan.sFredAffordProgId = srcDataLoan.sFredAffordProgId;
                    destDataLoan.sFredProcPointT = srcDataLoan.sFredProcPointT;
                    destDataLoan.sFreddieArmIndexT = srcDataLoan.sFreddieArmIndexT;
                    destDataLoan.sFreddieArmIndexTLckd = srcDataLoan.sFreddieArmIndexTLckd;
                    destDataLoan.sFredieReservesAmt_rep = srcDataLoan.sFredieReservesAmt_rep;
                    destDataLoan.sGseRefPurposeT = srcDataLoan.sGseRefPurposeT;
                    destDataLoan.sHelocBal_rep = srcDataLoan.sHelocBal_rep;
                    destDataLoan.sHelocCreditLimit_rep = srcDataLoan.sHelocCreditLimit_rep;
                    destDataLoan.sNegAmortT = srcDataLoan.sNegAmortT;
                    destDataLoan.sPayingOffSubordinate = srcDataLoan.sPayingOffSubordinate;
                    destDataLoan.sSpIsInPud = srcDataLoan.sSpIsInPud;
                    destDataLoan.sSpMarketVal_rep = srcDataLoan.sSpMarketVal_rep;
                    destDataLoan.sFHAFinancedDiscPtAmt_rep = srcDataLoan.sFHAFinancedDiscPtAmt_rep;
                    destDataLoan.sLpAusKey = srcDataLoan.sLpAusKey;
                    destDataLoan.sFreddieLoanId = srcDataLoan.sFreddieLoanId;
                    destDataLoan.sFreddieTransactionId = srcDataLoan.sFreddieTransactionId;
                    destDataLoan.sFreddieSellerNum = srcDataLoan.sFreddieSellerNum;
                    destDataLoan.sFreddieTpoNum = srcDataLoan.sFreddieTpoNum;
                    destDataLoan.sFreddieNotpNum = srcDataLoan.sFreddieNotpNum;
                    destDataLoan.sFreddieLenderBranchId = srcDataLoan.sFreddieLenderBranchId;
                    destDataLoan.sFreddieConstructionT = srcDataLoan.sFreddieConstructionT;
                    destDataLoan.sFredieReservesAmtLckd = srcDataLoan.sFredieReservesAmtLckd;
                    destDataLoan.sFreddieLpPassword = srcDataLoan.sFreddieLpPassword;

                    destDataLoan.sFreddieFeedbackResponseXml = srcDataLoan.sFreddieFeedbackResponseXml;
                }

                // TOTAL (Expanded)
                if (broker.IsExpandedLeadEditorTotalEnabled)
                {
                    destDataLoan.sTotalScoreCertificateXmlContent = srcDataLoan.sTotalScoreCertificateXmlContent;
                }

                // Expanded Preparer Info
                var expandedPreparerList = new List<E_PreparerFormT>()
                    {
                            E_PreparerFormT.App1003Interviewer,
                            E_PreparerFormT.BorrSignatureAuthorization,
                            E_PreparerFormT.CreditDenialScoreContact,
                            E_PreparerFormT.CreditDenialStatement,
                            E_PreparerFormT.ECOA,
                            E_PreparerFormT.Gfe,
                            E_PreparerFormT.PrivacyPolicyDisclosure
                    };

                if (broker.IsEditLeadsInFullLoanEditor)
                {
                    expandedPreparerList.Add(E_PreparerFormT.MortgageServicingRightsHolder);
                }

                foreach (var preparerFormT in expandedPreparerList)
                {
                    var srcPreparer = srcDataLoan.GetPreparerOfForm(preparerFormT, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                    if (srcPreparer.IsEmpty)
                    {
                        continue;
                    }

                    destDataLoan.PreparerCollection.AddCopyOfPreparer(srcPreparer);
                }

                // Custom Fields
                if (broker.IsExpandedLeadEditorCustomFieldsEnabled)
                {
                    // Custom Fields 1-20
                    destDataLoan.sCustomField1Desc = srcDataLoan.sCustomField1Desc;
                    destDataLoan.sCustomField2Desc = srcDataLoan.sCustomField2Desc;
                    destDataLoan.sCustomField3Desc = srcDataLoan.sCustomField3Desc;
                    destDataLoan.sCustomField4Desc = srcDataLoan.sCustomField4Desc;
                    destDataLoan.sCustomField5Desc = srcDataLoan.sCustomField5Desc;
                    destDataLoan.sCustomField6Desc = srcDataLoan.sCustomField6Desc;
                    destDataLoan.sCustomField7Desc = srcDataLoan.sCustomField7Desc;
                    destDataLoan.sCustomField8Desc = srcDataLoan.sCustomField8Desc;
                    destDataLoan.sCustomField9Desc = srcDataLoan.sCustomField9Desc;
                    destDataLoan.sCustomField10Desc = srcDataLoan.sCustomField10Desc;
                    destDataLoan.sCustomField11Desc = srcDataLoan.sCustomField11Desc;
                    destDataLoan.sCustomField12Desc = srcDataLoan.sCustomField12Desc;
                    destDataLoan.sCustomField13Desc = srcDataLoan.sCustomField13Desc;
                    destDataLoan.sCustomField14Desc = srcDataLoan.sCustomField14Desc;
                    destDataLoan.sCustomField15Desc = srcDataLoan.sCustomField15Desc;
                    destDataLoan.sCustomField16Desc = srcDataLoan.sCustomField16Desc;
                    destDataLoan.sCustomField17Desc = srcDataLoan.sCustomField17Desc;
                    destDataLoan.sCustomField18Desc = srcDataLoan.sCustomField18Desc;
                    destDataLoan.sCustomField19Desc = srcDataLoan.sCustomField19Desc;
                    destDataLoan.sCustomField20Desc = srcDataLoan.sCustomField20Desc;

                    destDataLoan.sCustomField1D = srcDataLoan.sCustomField1D;
                    destDataLoan.sCustomField2D = srcDataLoan.sCustomField2D;
                    destDataLoan.sCustomField3D = srcDataLoan.sCustomField3D;
                    destDataLoan.sCustomField4D = srcDataLoan.sCustomField4D;
                    destDataLoan.sCustomField5D = srcDataLoan.sCustomField5D;
                    destDataLoan.sCustomField6D = srcDataLoan.sCustomField6D;
                    destDataLoan.sCustomField7D = srcDataLoan.sCustomField7D;
                    destDataLoan.sCustomField8D = srcDataLoan.sCustomField8D;
                    destDataLoan.sCustomField9D = srcDataLoan.sCustomField9D;
                    destDataLoan.sCustomField10D = srcDataLoan.sCustomField10D;
                    destDataLoan.sCustomField11D = srcDataLoan.sCustomField11D;
                    destDataLoan.sCustomField12D = srcDataLoan.sCustomField12D;
                    destDataLoan.sCustomField13D = srcDataLoan.sCustomField13D;
                    destDataLoan.sCustomField14D = srcDataLoan.sCustomField14D;
                    destDataLoan.sCustomField15D = srcDataLoan.sCustomField15D;
                    destDataLoan.sCustomField16D = srcDataLoan.sCustomField16D;
                    destDataLoan.sCustomField17D = srcDataLoan.sCustomField17D;
                    destDataLoan.sCustomField18D = srcDataLoan.sCustomField18D;
                    destDataLoan.sCustomField19D = srcDataLoan.sCustomField19D;
                    destDataLoan.sCustomField20D = srcDataLoan.sCustomField20D;

                    destDataLoan.sCustomField1Money = srcDataLoan.sCustomField1Money;
                    destDataLoan.sCustomField2Money = srcDataLoan.sCustomField2Money;
                    destDataLoan.sCustomField3Money = srcDataLoan.sCustomField3Money;
                    destDataLoan.sCustomField4Money = srcDataLoan.sCustomField4Money;
                    destDataLoan.sCustomField5Money = srcDataLoan.sCustomField5Money;
                    destDataLoan.sCustomField6Money = srcDataLoan.sCustomField6Money;
                    destDataLoan.sCustomField7Money = srcDataLoan.sCustomField7Money;
                    destDataLoan.sCustomField8Money = srcDataLoan.sCustomField8Money;
                    destDataLoan.sCustomField9Money = srcDataLoan.sCustomField9Money;
                    destDataLoan.sCustomField10Money = srcDataLoan.sCustomField10Money;
                    destDataLoan.sCustomField11Money = srcDataLoan.sCustomField11Money;
                    destDataLoan.sCustomField12Money = srcDataLoan.sCustomField12Money;
                    destDataLoan.sCustomField13Money = srcDataLoan.sCustomField13Money;
                    destDataLoan.sCustomField14Money = srcDataLoan.sCustomField14Money;
                    destDataLoan.sCustomField15Money = srcDataLoan.sCustomField15Money;
                    destDataLoan.sCustomField16Money = srcDataLoan.sCustomField16Money;
                    destDataLoan.sCustomField17Money = srcDataLoan.sCustomField17Money;
                    destDataLoan.sCustomField18Money = srcDataLoan.sCustomField18Money;
                    destDataLoan.sCustomField19Money = srcDataLoan.sCustomField19Money;
                    destDataLoan.sCustomField20Money = srcDataLoan.sCustomField20Money;

                    destDataLoan.sCustomField1Pc = srcDataLoan.sCustomField1Pc;
                    destDataLoan.sCustomField2Pc = srcDataLoan.sCustomField2Pc;
                    destDataLoan.sCustomField3Pc = srcDataLoan.sCustomField3Pc;
                    destDataLoan.sCustomField4Pc = srcDataLoan.sCustomField4Pc;
                    destDataLoan.sCustomField5Pc = srcDataLoan.sCustomField5Pc;
                    destDataLoan.sCustomField6Pc = srcDataLoan.sCustomField6Pc;
                    destDataLoan.sCustomField7Pc = srcDataLoan.sCustomField7Pc;
                    destDataLoan.sCustomField8Pc = srcDataLoan.sCustomField8Pc;
                    destDataLoan.sCustomField9Pc = srcDataLoan.sCustomField9Pc;
                    destDataLoan.sCustomField10Pc = srcDataLoan.sCustomField10Pc;
                    destDataLoan.sCustomField11Pc = srcDataLoan.sCustomField11Pc;
                    destDataLoan.sCustomField12Pc = srcDataLoan.sCustomField12Pc;
                    destDataLoan.sCustomField13Pc = srcDataLoan.sCustomField13Pc;
                    destDataLoan.sCustomField14Pc = srcDataLoan.sCustomField14Pc;
                    destDataLoan.sCustomField15Pc = srcDataLoan.sCustomField15Pc;
                    destDataLoan.sCustomField16Pc = srcDataLoan.sCustomField16Pc;
                    destDataLoan.sCustomField17Pc = srcDataLoan.sCustomField17Pc;
                    destDataLoan.sCustomField18Pc = srcDataLoan.sCustomField18Pc;
                    destDataLoan.sCustomField19Pc = srcDataLoan.sCustomField19Pc;
                    destDataLoan.sCustomField20Pc = srcDataLoan.sCustomField20Pc;

                    destDataLoan.sCustomField1Bit = srcDataLoan.sCustomField1Bit;
                    destDataLoan.sCustomField2Bit = srcDataLoan.sCustomField2Bit;
                    destDataLoan.sCustomField3Bit = srcDataLoan.sCustomField3Bit;
                    destDataLoan.sCustomField4Bit = srcDataLoan.sCustomField4Bit;
                    destDataLoan.sCustomField5Bit = srcDataLoan.sCustomField5Bit;
                    destDataLoan.sCustomField6Bit = srcDataLoan.sCustomField6Bit;
                    destDataLoan.sCustomField7Bit = srcDataLoan.sCustomField7Bit;
                    destDataLoan.sCustomField8Bit = srcDataLoan.sCustomField8Bit;
                    destDataLoan.sCustomField9Bit = srcDataLoan.sCustomField9Bit;
                    destDataLoan.sCustomField10Bit = srcDataLoan.sCustomField10Bit;
                    destDataLoan.sCustomField11Bit = srcDataLoan.sCustomField11Bit;
                    destDataLoan.sCustomField12Bit = srcDataLoan.sCustomField12Bit;
                    destDataLoan.sCustomField13Bit = srcDataLoan.sCustomField13Bit;
                    destDataLoan.sCustomField14Bit = srcDataLoan.sCustomField14Bit;
                    destDataLoan.sCustomField15Bit = srcDataLoan.sCustomField15Bit;
                    destDataLoan.sCustomField16Bit = srcDataLoan.sCustomField16Bit;
                    destDataLoan.sCustomField17Bit = srcDataLoan.sCustomField17Bit;
                    destDataLoan.sCustomField18Bit = srcDataLoan.sCustomField18Bit;
                    destDataLoan.sCustomField19Bit = srcDataLoan.sCustomField19Bit;
                    destDataLoan.sCustomField20Bit = srcDataLoan.sCustomField20Bit;

                    destDataLoan.sCustomField1Notes = srcDataLoan.sCustomField1Notes;
                    destDataLoan.sCustomField2Notes = srcDataLoan.sCustomField2Notes;
                    destDataLoan.sCustomField3Notes = srcDataLoan.sCustomField3Notes;
                    destDataLoan.sCustomField4Notes = srcDataLoan.sCustomField4Notes;
                    destDataLoan.sCustomField5Notes = srcDataLoan.sCustomField5Notes;
                    destDataLoan.sCustomField6Notes = srcDataLoan.sCustomField6Notes;
                    destDataLoan.sCustomField7Notes = srcDataLoan.sCustomField7Notes;
                    destDataLoan.sCustomField8Notes = srcDataLoan.sCustomField8Notes;
                    destDataLoan.sCustomField9Notes = srcDataLoan.sCustomField9Notes;
                    destDataLoan.sCustomField10Notes = srcDataLoan.sCustomField10Notes;
                    destDataLoan.sCustomField11Notes = srcDataLoan.sCustomField11Notes;
                    destDataLoan.sCustomField12Notes = srcDataLoan.sCustomField12Notes;
                    destDataLoan.sCustomField13Notes = srcDataLoan.sCustomField13Notes;
                    destDataLoan.sCustomField14Notes = srcDataLoan.sCustomField14Notes;
                    destDataLoan.sCustomField15Notes = srcDataLoan.sCustomField15Notes;
                    destDataLoan.sCustomField16Notes = srcDataLoan.sCustomField16Notes;
                    destDataLoan.sCustomField17Notes = srcDataLoan.sCustomField17Notes;
                    destDataLoan.sCustomField18Notes = srcDataLoan.sCustomField18Notes;
                    destDataLoan.sCustomField19Notes = srcDataLoan.sCustomField19Notes;
                    destDataLoan.sCustomField20Notes = srcDataLoan.sCustomField20Notes;

                    // Custom Fields 21-40
                    destDataLoan.sCustomField21Desc = srcDataLoan.sCustomField21Desc;
                    destDataLoan.sCustomField22Desc = srcDataLoan.sCustomField22Desc;
                    destDataLoan.sCustomField23Desc = srcDataLoan.sCustomField23Desc;
                    destDataLoan.sCustomField24Desc = srcDataLoan.sCustomField24Desc;
                    destDataLoan.sCustomField25Desc = srcDataLoan.sCustomField25Desc;
                    destDataLoan.sCustomField26Desc = srcDataLoan.sCustomField26Desc;
                    destDataLoan.sCustomField27Desc = srcDataLoan.sCustomField27Desc;
                    destDataLoan.sCustomField28Desc = srcDataLoan.sCustomField28Desc;
                    destDataLoan.sCustomField29Desc = srcDataLoan.sCustomField29Desc;
                    destDataLoan.sCustomField30Desc = srcDataLoan.sCustomField30Desc;
                    destDataLoan.sCustomField31Desc = srcDataLoan.sCustomField31Desc;
                    destDataLoan.sCustomField32Desc = srcDataLoan.sCustomField32Desc;
                    destDataLoan.sCustomField33Desc = srcDataLoan.sCustomField33Desc;
                    destDataLoan.sCustomField34Desc = srcDataLoan.sCustomField34Desc;
                    destDataLoan.sCustomField35Desc = srcDataLoan.sCustomField35Desc;
                    destDataLoan.sCustomField36Desc = srcDataLoan.sCustomField36Desc;
                    destDataLoan.sCustomField37Desc = srcDataLoan.sCustomField37Desc;
                    destDataLoan.sCustomField38Desc = srcDataLoan.sCustomField38Desc;
                    destDataLoan.sCustomField39Desc = srcDataLoan.sCustomField39Desc;
                    destDataLoan.sCustomField40Desc = srcDataLoan.sCustomField40Desc;

                    destDataLoan.sCustomField21D = srcDataLoan.sCustomField21D;
                    destDataLoan.sCustomField22D = srcDataLoan.sCustomField22D;
                    destDataLoan.sCustomField23D = srcDataLoan.sCustomField23D;
                    destDataLoan.sCustomField24D = srcDataLoan.sCustomField24D;
                    destDataLoan.sCustomField25D = srcDataLoan.sCustomField25D;
                    destDataLoan.sCustomField26D = srcDataLoan.sCustomField26D;
                    destDataLoan.sCustomField27D = srcDataLoan.sCustomField27D;
                    destDataLoan.sCustomField28D = srcDataLoan.sCustomField28D;
                    destDataLoan.sCustomField29D = srcDataLoan.sCustomField29D;
                    destDataLoan.sCustomField30D = srcDataLoan.sCustomField30D;
                    destDataLoan.sCustomField31D = srcDataLoan.sCustomField31D;
                    destDataLoan.sCustomField32D = srcDataLoan.sCustomField32D;
                    destDataLoan.sCustomField33D = srcDataLoan.sCustomField33D;
                    destDataLoan.sCustomField34D = srcDataLoan.sCustomField34D;
                    destDataLoan.sCustomField35D = srcDataLoan.sCustomField35D;
                    destDataLoan.sCustomField36D = srcDataLoan.sCustomField36D;
                    destDataLoan.sCustomField37D = srcDataLoan.sCustomField37D;
                    destDataLoan.sCustomField38D = srcDataLoan.sCustomField38D;
                    destDataLoan.sCustomField39D = srcDataLoan.sCustomField39D;
                    destDataLoan.sCustomField40D = srcDataLoan.sCustomField40D;

                    destDataLoan.sCustomField21Money = srcDataLoan.sCustomField21Money;
                    destDataLoan.sCustomField22Money = srcDataLoan.sCustomField22Money;
                    destDataLoan.sCustomField23Money = srcDataLoan.sCustomField23Money;
                    destDataLoan.sCustomField24Money = srcDataLoan.sCustomField24Money;
                    destDataLoan.sCustomField25Money = srcDataLoan.sCustomField25Money;
                    destDataLoan.sCustomField26Money = srcDataLoan.sCustomField26Money;
                    destDataLoan.sCustomField27Money = srcDataLoan.sCustomField27Money;
                    destDataLoan.sCustomField28Money = srcDataLoan.sCustomField28Money;
                    destDataLoan.sCustomField29Money = srcDataLoan.sCustomField29Money;
                    destDataLoan.sCustomField30Money = srcDataLoan.sCustomField30Money;
                    destDataLoan.sCustomField31Money = srcDataLoan.sCustomField31Money;
                    destDataLoan.sCustomField32Money = srcDataLoan.sCustomField32Money;
                    destDataLoan.sCustomField33Money = srcDataLoan.sCustomField33Money;
                    destDataLoan.sCustomField34Money = srcDataLoan.sCustomField34Money;
                    destDataLoan.sCustomField35Money = srcDataLoan.sCustomField35Money;
                    destDataLoan.sCustomField36Money = srcDataLoan.sCustomField36Money;
                    destDataLoan.sCustomField37Money = srcDataLoan.sCustomField37Money;
                    destDataLoan.sCustomField38Money = srcDataLoan.sCustomField38Money;
                    destDataLoan.sCustomField39Money = srcDataLoan.sCustomField39Money;
                    destDataLoan.sCustomField40Money = srcDataLoan.sCustomField40Money;

                    destDataLoan.sCustomField21Pc = srcDataLoan.sCustomField21Pc;
                    destDataLoan.sCustomField22Pc = srcDataLoan.sCustomField22Pc;
                    destDataLoan.sCustomField23Pc = srcDataLoan.sCustomField23Pc;
                    destDataLoan.sCustomField24Pc = srcDataLoan.sCustomField24Pc;
                    destDataLoan.sCustomField25Pc = srcDataLoan.sCustomField25Pc;
                    destDataLoan.sCustomField26Pc = srcDataLoan.sCustomField26Pc;
                    destDataLoan.sCustomField27Pc = srcDataLoan.sCustomField27Pc;
                    destDataLoan.sCustomField28Pc = srcDataLoan.sCustomField28Pc;
                    destDataLoan.sCustomField29Pc = srcDataLoan.sCustomField29Pc;
                    destDataLoan.sCustomField30Pc = srcDataLoan.sCustomField30Pc;
                    destDataLoan.sCustomField31Pc = srcDataLoan.sCustomField31Pc;
                    destDataLoan.sCustomField32Pc = srcDataLoan.sCustomField32Pc;
                    destDataLoan.sCustomField33Pc = srcDataLoan.sCustomField33Pc;
                    destDataLoan.sCustomField34Pc = srcDataLoan.sCustomField34Pc;
                    destDataLoan.sCustomField35Pc = srcDataLoan.sCustomField35Pc;
                    destDataLoan.sCustomField36Pc = srcDataLoan.sCustomField36Pc;
                    destDataLoan.sCustomField37Pc = srcDataLoan.sCustomField37Pc;
                    destDataLoan.sCustomField38Pc = srcDataLoan.sCustomField38Pc;
                    destDataLoan.sCustomField39Pc = srcDataLoan.sCustomField39Pc;
                    destDataLoan.sCustomField40Pc = srcDataLoan.sCustomField40Pc;

                    destDataLoan.sCustomField21Bit = srcDataLoan.sCustomField21Bit;
                    destDataLoan.sCustomField22Bit = srcDataLoan.sCustomField22Bit;
                    destDataLoan.sCustomField23Bit = srcDataLoan.sCustomField23Bit;
                    destDataLoan.sCustomField24Bit = srcDataLoan.sCustomField24Bit;
                    destDataLoan.sCustomField25Bit = srcDataLoan.sCustomField25Bit;
                    destDataLoan.sCustomField26Bit = srcDataLoan.sCustomField26Bit;
                    destDataLoan.sCustomField27Bit = srcDataLoan.sCustomField27Bit;
                    destDataLoan.sCustomField28Bit = srcDataLoan.sCustomField28Bit;
                    destDataLoan.sCustomField29Bit = srcDataLoan.sCustomField29Bit;
                    destDataLoan.sCustomField30Bit = srcDataLoan.sCustomField30Bit;
                    destDataLoan.sCustomField31Bit = srcDataLoan.sCustomField31Bit;
                    destDataLoan.sCustomField32Bit = srcDataLoan.sCustomField32Bit;
                    destDataLoan.sCustomField33Bit = srcDataLoan.sCustomField33Bit;
                    destDataLoan.sCustomField34Bit = srcDataLoan.sCustomField34Bit;
                    destDataLoan.sCustomField35Bit = srcDataLoan.sCustomField35Bit;
                    destDataLoan.sCustomField36Bit = srcDataLoan.sCustomField36Bit;
                    destDataLoan.sCustomField37Bit = srcDataLoan.sCustomField37Bit;
                    destDataLoan.sCustomField38Bit = srcDataLoan.sCustomField38Bit;
                    destDataLoan.sCustomField39Bit = srcDataLoan.sCustomField39Bit;
                    destDataLoan.sCustomField40Bit = srcDataLoan.sCustomField40Bit;

                    destDataLoan.sCustomField21Notes = srcDataLoan.sCustomField21Notes;
                    destDataLoan.sCustomField22Notes = srcDataLoan.sCustomField22Notes;
                    destDataLoan.sCustomField23Notes = srcDataLoan.sCustomField23Notes;
                    destDataLoan.sCustomField24Notes = srcDataLoan.sCustomField24Notes;
                    destDataLoan.sCustomField25Notes = srcDataLoan.sCustomField25Notes;
                    destDataLoan.sCustomField26Notes = srcDataLoan.sCustomField26Notes;
                    destDataLoan.sCustomField27Notes = srcDataLoan.sCustomField27Notes;
                    destDataLoan.sCustomField28Notes = srcDataLoan.sCustomField28Notes;
                    destDataLoan.sCustomField29Notes = srcDataLoan.sCustomField29Notes;
                    destDataLoan.sCustomField30Notes = srcDataLoan.sCustomField30Notes;
                    destDataLoan.sCustomField31Notes = srcDataLoan.sCustomField31Notes;
                    destDataLoan.sCustomField32Notes = srcDataLoan.sCustomField32Notes;
                    destDataLoan.sCustomField33Notes = srcDataLoan.sCustomField33Notes;
                    destDataLoan.sCustomField34Notes = srcDataLoan.sCustomField34Notes;
                    destDataLoan.sCustomField35Notes = srcDataLoan.sCustomField35Notes;
                    destDataLoan.sCustomField36Notes = srcDataLoan.sCustomField36Notes;
                    destDataLoan.sCustomField37Notes = srcDataLoan.sCustomField37Notes;
                    destDataLoan.sCustomField38Notes = srcDataLoan.sCustomField38Notes;
                    destDataLoan.sCustomField39Notes = srcDataLoan.sCustomField39Notes;
                    destDataLoan.sCustomField40Notes = srcDataLoan.sCustomField40Notes;

                    // Custom Fields 41-60
                    destDataLoan.sCustomField41Desc = srcDataLoan.sCustomField41Desc;
                    destDataLoan.sCustomField42Desc = srcDataLoan.sCustomField42Desc;
                    destDataLoan.sCustomField43Desc = srcDataLoan.sCustomField43Desc;
                    destDataLoan.sCustomField44Desc = srcDataLoan.sCustomField44Desc;
                    destDataLoan.sCustomField45Desc = srcDataLoan.sCustomField45Desc;
                    destDataLoan.sCustomField46Desc = srcDataLoan.sCustomField46Desc;
                    destDataLoan.sCustomField47Desc = srcDataLoan.sCustomField47Desc;
                    destDataLoan.sCustomField48Desc = srcDataLoan.sCustomField48Desc;
                    destDataLoan.sCustomField49Desc = srcDataLoan.sCustomField49Desc;
                    destDataLoan.sCustomField50Desc = srcDataLoan.sCustomField50Desc;
                    destDataLoan.sCustomField51Desc = srcDataLoan.sCustomField51Desc;
                    destDataLoan.sCustomField52Desc = srcDataLoan.sCustomField52Desc;
                    destDataLoan.sCustomField53Desc = srcDataLoan.sCustomField53Desc;
                    destDataLoan.sCustomField54Desc = srcDataLoan.sCustomField54Desc;
                    destDataLoan.sCustomField55Desc = srcDataLoan.sCustomField55Desc;
                    destDataLoan.sCustomField56Desc = srcDataLoan.sCustomField56Desc;
                    destDataLoan.sCustomField57Desc = srcDataLoan.sCustomField57Desc;
                    destDataLoan.sCustomField58Desc = srcDataLoan.sCustomField58Desc;
                    destDataLoan.sCustomField59Desc = srcDataLoan.sCustomField59Desc;
                    destDataLoan.sCustomField60Desc = srcDataLoan.sCustomField60Desc;

                    destDataLoan.sCustomField41D = srcDataLoan.sCustomField41D;
                    destDataLoan.sCustomField42D = srcDataLoan.sCustomField42D;
                    destDataLoan.sCustomField43D = srcDataLoan.sCustomField43D;
                    destDataLoan.sCustomField44D = srcDataLoan.sCustomField44D;
                    destDataLoan.sCustomField45D = srcDataLoan.sCustomField45D;
                    destDataLoan.sCustomField46D = srcDataLoan.sCustomField46D;
                    destDataLoan.sCustomField47D = srcDataLoan.sCustomField47D;
                    destDataLoan.sCustomField48D = srcDataLoan.sCustomField48D;
                    destDataLoan.sCustomField49D = srcDataLoan.sCustomField49D;
                    destDataLoan.sCustomField50D = srcDataLoan.sCustomField50D;
                    destDataLoan.sCustomField51D = srcDataLoan.sCustomField51D;
                    destDataLoan.sCustomField52D = srcDataLoan.sCustomField52D;
                    destDataLoan.sCustomField53D = srcDataLoan.sCustomField53D;
                    destDataLoan.sCustomField54D = srcDataLoan.sCustomField54D;
                    destDataLoan.sCustomField55D = srcDataLoan.sCustomField55D;
                    destDataLoan.sCustomField56D = srcDataLoan.sCustomField56D;
                    destDataLoan.sCustomField57D = srcDataLoan.sCustomField57D;
                    destDataLoan.sCustomField58D = srcDataLoan.sCustomField58D;
                    destDataLoan.sCustomField59D = srcDataLoan.sCustomField59D;
                    destDataLoan.sCustomField60D = srcDataLoan.sCustomField60D;

                    destDataLoan.sCustomField41Money = srcDataLoan.sCustomField41Money;
                    destDataLoan.sCustomField42Money = srcDataLoan.sCustomField42Money;
                    destDataLoan.sCustomField43Money = srcDataLoan.sCustomField43Money;
                    destDataLoan.sCustomField44Money = srcDataLoan.sCustomField44Money;
                    destDataLoan.sCustomField45Money = srcDataLoan.sCustomField45Money;
                    destDataLoan.sCustomField46Money = srcDataLoan.sCustomField46Money;
                    destDataLoan.sCustomField47Money = srcDataLoan.sCustomField47Money;
                    destDataLoan.sCustomField48Money = srcDataLoan.sCustomField48Money;
                    destDataLoan.sCustomField49Money = srcDataLoan.sCustomField49Money;
                    destDataLoan.sCustomField50Money = srcDataLoan.sCustomField50Money;
                    destDataLoan.sCustomField51Money = srcDataLoan.sCustomField51Money;
                    destDataLoan.sCustomField52Money = srcDataLoan.sCustomField52Money;
                    destDataLoan.sCustomField53Money = srcDataLoan.sCustomField53Money;
                    destDataLoan.sCustomField54Money = srcDataLoan.sCustomField54Money;
                    destDataLoan.sCustomField55Money = srcDataLoan.sCustomField55Money;
                    destDataLoan.sCustomField56Money = srcDataLoan.sCustomField56Money;
                    destDataLoan.sCustomField57Money = srcDataLoan.sCustomField57Money;
                    destDataLoan.sCustomField58Money = srcDataLoan.sCustomField58Money;
                    destDataLoan.sCustomField59Money = srcDataLoan.sCustomField59Money;
                    destDataLoan.sCustomField60Money = srcDataLoan.sCustomField60Money;

                    destDataLoan.sCustomField41Pc = srcDataLoan.sCustomField41Pc;
                    destDataLoan.sCustomField42Pc = srcDataLoan.sCustomField42Pc;
                    destDataLoan.sCustomField43Pc = srcDataLoan.sCustomField43Pc;
                    destDataLoan.sCustomField44Pc = srcDataLoan.sCustomField44Pc;
                    destDataLoan.sCustomField45Pc = srcDataLoan.sCustomField45Pc;
                    destDataLoan.sCustomField46Pc = srcDataLoan.sCustomField46Pc;
                    destDataLoan.sCustomField47Pc = srcDataLoan.sCustomField47Pc;
                    destDataLoan.sCustomField48Pc = srcDataLoan.sCustomField48Pc;
                    destDataLoan.sCustomField49Pc = srcDataLoan.sCustomField49Pc;
                    destDataLoan.sCustomField50Pc = srcDataLoan.sCustomField50Pc;
                    destDataLoan.sCustomField51Pc = srcDataLoan.sCustomField51Pc;
                    destDataLoan.sCustomField52Pc = srcDataLoan.sCustomField52Pc;
                    destDataLoan.sCustomField53Pc = srcDataLoan.sCustomField53Pc;
                    destDataLoan.sCustomField54Pc = srcDataLoan.sCustomField54Pc;
                    destDataLoan.sCustomField55Pc = srcDataLoan.sCustomField55Pc;
                    destDataLoan.sCustomField56Pc = srcDataLoan.sCustomField56Pc;
                    destDataLoan.sCustomField57Pc = srcDataLoan.sCustomField57Pc;
                    destDataLoan.sCustomField58Pc = srcDataLoan.sCustomField58Pc;
                    destDataLoan.sCustomField59Pc = srcDataLoan.sCustomField59Pc;
                    destDataLoan.sCustomField60Pc = srcDataLoan.sCustomField60Pc;

                    destDataLoan.sCustomField41Bit = srcDataLoan.sCustomField41Bit;
                    destDataLoan.sCustomField42Bit = srcDataLoan.sCustomField42Bit;
                    destDataLoan.sCustomField43Bit = srcDataLoan.sCustomField43Bit;
                    destDataLoan.sCustomField44Bit = srcDataLoan.sCustomField44Bit;
                    destDataLoan.sCustomField45Bit = srcDataLoan.sCustomField45Bit;
                    destDataLoan.sCustomField46Bit = srcDataLoan.sCustomField46Bit;
                    destDataLoan.sCustomField47Bit = srcDataLoan.sCustomField47Bit;
                    destDataLoan.sCustomField48Bit = srcDataLoan.sCustomField48Bit;
                    destDataLoan.sCustomField49Bit = srcDataLoan.sCustomField49Bit;
                    destDataLoan.sCustomField50Bit = srcDataLoan.sCustomField50Bit;
                    destDataLoan.sCustomField51Bit = srcDataLoan.sCustomField51Bit;
                    destDataLoan.sCustomField52Bit = srcDataLoan.sCustomField52Bit;
                    destDataLoan.sCustomField53Bit = srcDataLoan.sCustomField53Bit;
                    destDataLoan.sCustomField54Bit = srcDataLoan.sCustomField54Bit;
                    destDataLoan.sCustomField55Bit = srcDataLoan.sCustomField55Bit;
                    destDataLoan.sCustomField56Bit = srcDataLoan.sCustomField56Bit;
                    destDataLoan.sCustomField57Bit = srcDataLoan.sCustomField57Bit;
                    destDataLoan.sCustomField58Bit = srcDataLoan.sCustomField58Bit;
                    destDataLoan.sCustomField59Bit = srcDataLoan.sCustomField59Bit;
                    destDataLoan.sCustomField60Bit = srcDataLoan.sCustomField60Bit;

                    destDataLoan.sCustomField41Notes = srcDataLoan.sCustomField41Notes;
                    destDataLoan.sCustomField42Notes = srcDataLoan.sCustomField42Notes;
                    destDataLoan.sCustomField43Notes = srcDataLoan.sCustomField43Notes;
                    destDataLoan.sCustomField44Notes = srcDataLoan.sCustomField44Notes;
                    destDataLoan.sCustomField45Notes = srcDataLoan.sCustomField45Notes;
                    destDataLoan.sCustomField46Notes = srcDataLoan.sCustomField46Notes;
                    destDataLoan.sCustomField47Notes = srcDataLoan.sCustomField47Notes;
                    destDataLoan.sCustomField48Notes = srcDataLoan.sCustomField48Notes;
                    destDataLoan.sCustomField49Notes = srcDataLoan.sCustomField59Notes;
                    destDataLoan.sCustomField50Notes = srcDataLoan.sCustomField50Notes;
                    destDataLoan.sCustomField51Notes = srcDataLoan.sCustomField51Notes;
                    destDataLoan.sCustomField52Notes = srcDataLoan.sCustomField52Notes;
                    destDataLoan.sCustomField53Notes = srcDataLoan.sCustomField53Notes;
                    destDataLoan.sCustomField54Notes = srcDataLoan.sCustomField54Notes;
                    destDataLoan.sCustomField55Notes = srcDataLoan.sCustomField55Notes;
                    destDataLoan.sCustomField56Notes = srcDataLoan.sCustomField56Notes;
                    destDataLoan.sCustomField57Notes = srcDataLoan.sCustomField57Notes;
                    destDataLoan.sCustomField58Notes = srcDataLoan.sCustomField58Notes;
                    destDataLoan.sCustomField59Notes = srcDataLoan.sCustomField59Notes;
                    destDataLoan.sCustomField60Notes = srcDataLoan.sCustomField60Notes;
                }

                destDataLoan.sAusFindingsPull = srcDataLoan.sAusFindingsPull;

                // copy disclosure fields opm 196899
                destDataLoan.sDisclosuresDueD_rep = srcDataLoan.sDisclosuresDueD_rep;
                destDataLoan.sLastDisclosedD = srcDataLoan.sLastDisclosedD;
                destDataLoan.sDisclosureNeededT = srcDataLoan.sDisclosureNeededT;
                destDataLoan.sNeedInitialDisc = srcDataLoan.sNeedInitialDisc;
                destDataLoan.sNeedRedisc = srcDataLoan.sNeedRedisc;

                // ejm opm 212376
                // Copy Fields from CFPB pages
                destDataLoan.sIsAllowExcludeToleranceCure = srcDataLoan.sIsAllowExcludeToleranceCure;

                // sk opm 217108
                if (destDataLoan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID)
                {
                    destDataLoan.sLoads1003LineLFromAdjustments = false;
                    destDataLoan.sOCredit2Amt_rep = srcDataLoan.sOCredit2Amt_rep;
                    destDataLoan.sOCredit2Desc = srcDataLoan.sOCredit2Desc;
                    destDataLoan.sOCredit3Amt_rep = srcDataLoan.sOCredit3Amt_rep;
                    destDataLoan.sOCredit3Desc = srcDataLoan.sOCredit3Desc;
                    destDataLoan.sOCredit4Amt_rep = srcDataLoan.sOCredit4Amt_rep;
                    destDataLoan.sOCredit4Desc = srcDataLoan.sOCredit4Desc;
                }
                else
                {
                    if (destDataLoan.sLoads1003LineLFromAdjustments == false && srcDataLoan.sLoads1003LineLFromAdjustments == false)
                    {
                        destDataLoan.sOCredit2Amt_rep = srcDataLoan.sOCredit2Amt_rep;
                        destDataLoan.sOCredit2Desc = srcDataLoan.sOCredit2Desc;
                        destDataLoan.sOCredit3Amt_rep = srcDataLoan.sOCredit3Amt_rep;
                        destDataLoan.sOCredit3Desc = srcDataLoan.sOCredit3Desc;
                        destDataLoan.sOCredit4Amt_rep = srcDataLoan.sOCredit4Amt_rep;
                        destDataLoan.sOCredit4Desc = srcDataLoan.sOCredit4Desc;
                    }
                    else
                    {
                        // one or the other has sLoads1003LineLFromAdjustments set to true
                        if (destDataLoan.sLoads1003LineLFromAdjustments == false)
                        {
                            destDataLoan.sLoads1003LineLFromAdjustments = true; // migrates
                        }
                        else if (srcDataLoan.sLoads1003LineLFromAdjustments == false)
                        {
                            srcDataLoan.sLoads1003LineLFromAdjustments = true; // migrates
                        }
                    }
                }

                // OPM 228267, 10/8/2015, ML
                if (srcDataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
                {
                    destDataLoan.sProrationList = srcDataLoan.sProrationList.GetUnlinkedCopy(IdAssignment.Copy);
                    destDataLoan.sGrossDueFromBorrPersonalProperty_rep = srcDataLoan.sGrossDueFromBorrPersonalProperty_rep;
                    destDataLoan.sReductionsDueToSellerExcessDeposit_rep = srcDataLoan.sReductionsDueToSellerExcessDeposit_rep;
                    destDataLoan.sReductionsDueToSellerPayoffOf1stMtgLoan_rep = srcDataLoan.sReductionsDueToSellerPayoffOf1stMtgLoan_rep;
                    destDataLoan.sReductionsDueToSellerPayoffOf2ndMtgLoan_rep = srcDataLoan.sReductionsDueToSellerPayoffOf2ndMtgLoan_rep;
                    destDataLoan.sONewFinNetProceeds = srcDataLoan.sONewFinNetProceeds;
                    destDataLoan.sONewFinNetProceedsLckd = srcDataLoan.sONewFinNetProceedsLckd;

                    // Per PDE, this should always be copied over if the lead is TRID 2015, regardless of
                    // the value of sLoads1003LineLFromAdjustments.
                    destDataLoan.sAdjustmentList = srcDataLoan.sAdjustmentList.GetUnlinkedCopy(IdAssignment.Copy);

                    // OPM 231307, 12/28/2015, ML
                    if (srcDataLoan.sPropertyTransferDLckd)
                    {
                        destDataLoan.sPropertyTransferDLckd = true;
                        destDataLoan.sPropertyTransferD = srcDataLoan.sPropertyTransferD;
                    }
                }

                destDataLoan.sLOrigFProps_BF = srcDataLoan.sLOrigFProps_BF;
                destDataLoan.sGfeDiscountPointFProps_BF = srcDataLoan.sGfeDiscountPointFProps_BF;
                destDataLoan.sGfeLenderCreditFProps_Apr = srcDataLoan.sGfeLenderCreditFProps_Apr;
                destDataLoan.sFloodCertificationDeterminationT = srcDataLoan.sFloodCertificationDeterminationT;
                destDataLoan.sIsOptionArm = srcDataLoan.sIsOptionArm;
                destDataLoan.sOptionArmMinPayPeriod = srcDataLoan.sOptionArmMinPayPeriod;
                destDataLoan.sOptionArmIntroductoryPeriod = srcDataLoan.sOptionArmIntroductoryPeriod;
                destDataLoan.sOptionArmInitialFixMinPmtPeriod = srcDataLoan.sOptionArmInitialFixMinPmtPeriod;
                destDataLoan.sOptionArmMinPayIsIOOnly = srcDataLoan.sOptionArmMinPayIsIOOnly;
                destDataLoan.sIsFullAmortAfterRecast = srcDataLoan.sIsFullAmortAfterRecast;
                destDataLoan.sOptionArmMinPayOptionT = srcDataLoan.sOptionArmMinPayOptionT;
                destDataLoan.sOptionArmPmtDiscount = srcDataLoan.sOptionArmPmtDiscount;
                destDataLoan.sPpmtAmt = srcDataLoan.sPpmtAmt;
                destDataLoan.sPpmtMon = srcDataLoan.sPpmtMon;
                destDataLoan.sPpmtStartMon = srcDataLoan.sPpmtStartMon;
                destDataLoan.sPpmtOneAmt = srcDataLoan.sPpmtOneAmt;
                destDataLoan.sPpmtOneMon = srcDataLoan.sPpmtOneMon;
                destDataLoan.sAprIncludesReqDeposit = srcDataLoan.sAprIncludesReqDeposit;
                destDataLoan.sHasDemandFeature = srcDataLoan.sHasDemandFeature;
                destDataLoan.sIsPropertyBeingSoldByCreditor = srcDataLoan.sIsPropertyBeingSoldByCreditor;
                destDataLoan.sHasVarRFeature = srcDataLoan.sHasVarRFeature;
                destDataLoan.sInsReqDesc = srcDataLoan.sInsReqDesc;
                destDataLoan.sReqCreditLifeIns = srcDataLoan.sReqCreditLifeIns;
                destDataLoan.sReqCreditDisabilityIns = srcDataLoan.sReqCreditDisabilityIns;
                destDataLoan.sReqPropIns = srcDataLoan.sReqPropIns;
                destDataLoan.sReqFloodIns = srcDataLoan.sReqFloodIns;
                destDataLoan.sIfPurchInsFrCreditor = srcDataLoan.sIfPurchInsFrCreditor;
                destDataLoan.sIfPurchPropInsFrCreditor = srcDataLoan.sIfPurchPropInsFrCreditor;
                destDataLoan.sIfPurchFloodInsFrCreditor = srcDataLoan.sIfPurchFloodInsFrCreditor;
                destDataLoan.sInsFrCreditorAmt = srcDataLoan.sInsFrCreditorAmt;
                destDataLoan.sSecurityPurch = srcDataLoan.sSecurityPurch;
                destDataLoan.sSecurityCurrentOwn = srcDataLoan.sSecurityCurrentOwn;
                destDataLoan.sAsteriskEstimate = srcDataLoan.sAsteriskEstimate;
                destDataLoan.sOnlyLatePmtEstimate = srcDataLoan.sOnlyLatePmtEstimate;
                destDataLoan.sLenderCreditCalculationMethodT = srcDataLoan.sLenderCreditCalculationMethodT;
                destDataLoan.sGfeCreditLenderPaidItemT = srcDataLoan.sGfeCreditLenderPaidItemT;
                destDataLoan.sLenderCustomCredit1Description = srcDataLoan.sLenderCustomCredit1Description;
                destDataLoan.sLenderCustomCredit2Description = srcDataLoan.sLenderCustomCredit2Description;
                destDataLoan.sLenderCustomCredit1Amount_rep = srcDataLoan.sLenderCustomCredit1Amount_rep;
                destDataLoan.sLenderCustomCredit2Amount_rep = srcDataLoan.sLenderCustomCredit2Amount_rep;
                destDataLoan.sToleranceCureCalculationT = srcDataLoan.sToleranceCureCalculationT;
                destDataLoan.sLDiscntBaseT = srcDataLoan.sLDiscntBaseT;
                destDataLoan.sLenderCreditMaxT = srcDataLoan.sLenderCreditMaxT;
                destDataLoan.sLenderCreditMaxAmt_Neg_rep = srcDataLoan.sLenderCreditMaxAmt_Neg_rep;
                destDataLoan.sLenderPaidFeeDiscloseLocationT = srcDataLoan.sLenderPaidFeeDiscloseLocationT;
                destDataLoan.sLenderGeneralCreditDiscloseLocationT = srcDataLoan.sLenderGeneralCreditDiscloseLocationT;
                destDataLoan.sLenderCustomCredit1DiscloseLocationT = srcDataLoan.sLenderCustomCredit1DiscloseLocationT;
                destDataLoan.sLenderCustomCredit2DiscloseLocationT = srcDataLoan.sLenderCustomCredit2DiscloseLocationT;
                destDataLoan.sAggEscrowCalcModeT = srcDataLoan.sAggEscrowCalcModeT;
                destDataLoan.sCustomaryEscrowImpoundsCalcMinT = srcDataLoan.sCustomaryEscrowImpoundsCalcMinT;
                destDataLoan.sIsRequireFeesFromDropDown = srcDataLoan.sIsRequireFeesFromDropDown;
                destDataLoan.sGfeIsTPOTransaction = srcDataLoan.sGfeIsTPOTransaction;
                destDataLoan.sTRIDLoanEstimateLenderIntendsToServiceLoan = srcDataLoan.sTRIDLoanEstimateLenderIntendsToServiceLoan;
                destDataLoan.sTRIDLoanEstimateNoteIRAvailTillDTimeZoneT = srcDataLoan.sTRIDLoanEstimateNoteIRAvailTillDTimeZoneT;
                destDataLoan.sTRIDLoanEstimateSetLockStatusMethodT = srcDataLoan.sTRIDLoanEstimateSetLockStatusMethodT;
                destDataLoan.sTRIDLoanEstimateNoteIRAvailTillD_rep = srcDataLoan.sTRIDLoanEstimateNoteIRAvailTillD_rep;
                destDataLoan.sTRIDLoanEstimateNoteIRAvailTillDTime = srcDataLoan.sTRIDLoanEstimateNoteIRAvailTillDTime;
                destDataLoan.sGfeEstScAvailTillDTimeZoneT = srcDataLoan.sGfeEstScAvailTillDTimeZoneT;
                destDataLoan.sGfeEstScAvailTillD = srcDataLoan.sGfeEstScAvailTillD;
                destDataLoan.sIsPrintTimeForsGfeEstScAvailTillD = srcDataLoan.sIsPrintTimeForsGfeEstScAvailTillD;
                destDataLoan.sGfeEstScAvailTillDLckd = srcDataLoan.sGfeEstScAvailTillDLckd;
                destDataLoan.sLoanEstScAvailTillDLckd = srcDataLoan.sLoanEstScAvailTillDLckd;
                destDataLoan.sLoanEstScAvailTillD = srcDataLoan.sLoanEstScAvailTillD;
                destDataLoan.sIsPrintTimeForsLoanEstScAvailTillD = srcDataLoan.sIsPrintTimeForsLoanEstScAvailTillD;
                destDataLoan.sLoanEstScAvailTillDTimeZoneT = srcDataLoan.sLoanEstScAvailTillDTimeZoneT;

                destDataLoan.sLoanBeingRefinancedAmortizationT = srcDataLoan.sLoanBeingRefinancedAmortizationT;
                destDataLoan.sLoanBeingRefinancedLienPosT = srcDataLoan.sLoanBeingRefinancedLienPosT;
                destDataLoan.sLoanBeingRefinancedInterestRate = srcDataLoan.sLoanBeingRefinancedInterestRate;
                destDataLoan.sLoanBeingRefinancedRemainingTerm = srcDataLoan.sLoanBeingRefinancedRemainingTerm;
                destDataLoan.sLoanBeingRefinancedTotalOfPayments = srcDataLoan.sLoanBeingRefinancedTotalOfPayments;
                destDataLoan.sLoanBeingRefinancedLtvR = srcDataLoan.sLoanBeingRefinancedLtvR;
                destDataLoan.sLoanBeingRefinancedUsedToConstructAlterRepair = srcDataLoan.sLoanBeingRefinancedUsedToConstructAlterRepair;

                destDataLoan.CopyRespaFirstEnteredDates(srcDataLoan);
                if (broker.IsEditLeadsInFullLoanEditor)
                {
                    destDataLoan.sRespa6D = srcDataLoan.sRespa6D;
                    destDataLoan.sRespa6DLckd = srcDataLoan.sRespa6DLckd;
                    destDataLoan.sRespaBorrowerNameCollectedD = srcDataLoan.sRespaBorrowerNameCollectedD;
                    destDataLoan.sRespaBorrowerNameCollectedDLckd = srcDataLoan.sRespaBorrowerNameCollectedDLckd;
                    destDataLoan.sRespaBorrowerSsnCollectedD = srcDataLoan.sRespaBorrowerSsnCollectedD;
                    destDataLoan.sRespaBorrowerSsnCollectedDLckd = srcDataLoan.sRespaBorrowerSsnCollectedDLckd;
                    destDataLoan.sRespaIncomeCollectedD = srcDataLoan.sRespaIncomeCollectedD;
                    destDataLoan.sRespaIncomeCollectedDLckd = srcDataLoan.sRespaIncomeCollectedDLckd;
                    destDataLoan.sRespaPropValueCollectedD = srcDataLoan.sRespaPropValueCollectedD;
                    destDataLoan.sRespaPropValueCollectedDLckd = srcDataLoan.sRespaPropValueCollectedDLckd;
                    destDataLoan.sRespaPropAddressCollectedD = srcDataLoan.sRespaPropAddressCollectedD;
                    destDataLoan.sRespaPropAddressCollectedDLckd = srcDataLoan.sRespaPropAddressCollectedDLckd;
                    destDataLoan.sRespaLoanAmountCollectedD = srcDataLoan.sRespaLoanAmountCollectedD;
                    destDataLoan.sRespaLoanAmountCollectedDLckd = srcDataLoan.sRespaLoanAmountCollectedDLckd;

                    destDataLoan.sMortgageServicingRightsSalesDate = srcDataLoan.sMortgageServicingRightsSalesDate;
                    destDataLoan.sMortgageServicingRightsTransferDate = srcDataLoan.sMortgageServicingRightsTransferDate;
                    destDataLoan.sMortgageServicingRightsHolderFirstPaymentDueDate = srcDataLoan.sMortgageServicingRightsHolderFirstPaymentDueDate;
                }

                // OPM 467676 - Copy construction loan fields.
                if (srcDataLoan.sIsConstructionLoan)
                {
                    CopyConstructionFields(destDataLoan, srcDataLoan);
                }

                // Copy isManuallySetThirdPartyAffiliateProps with a bypass due to permission requirement.
                bool oldBypass = destDataLoan.LeadConversionBypassForsIsManuallySetThirdPartyAffiliateProps;
                destDataLoan.LeadConversionBypassForsIsManuallySetThirdPartyAffiliateProps = true;
                destDataLoan.sIsManuallySetThirdPartyAffiliateProps = srcDataLoan.sIsManuallySetThirdPartyAffiliateProps;
                destDataLoan.LeadConversionBypassForsIsManuallySetThirdPartyAffiliateProps = oldBypass;

                IPreparerFields destlender = destDataLoan.GetPreparerOfForm(E_PreparerFormT.TRIDClosingCosts_Lender, E_ReturnOptionIfNotExist.CreateNew);
                IPreparerFields srclender = srcDataLoan.GetPreparerOfForm(E_PreparerFormT.TRIDClosingCosts_Lender, E_ReturnOptionIfNotExist.CreateNew);
                destlender.CompanyName = srclender.CompanyName;
                destlender.CompanyLoanOriginatorIdentifier = srclender.CompanyLoanOriginatorIdentifier;
                destlender.LicenseNumOfCompany = srclender.LicenseNumOfCompany;
                destlender.AgentRoleT = srclender.AgentRoleT;
                destlender.IsLocked = srclender.IsLocked;
                destlender.Update();

                IPreparerFields destLoanOfficer = destDataLoan.GetPreparerOfForm(E_PreparerFormT.TRIDClosingCosts_LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
                IPreparerFields srcLoanOfficer = srcDataLoan.GetPreparerOfForm(E_PreparerFormT.TRIDClosingCosts_LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
                destLoanOfficer.PreparerName = srcLoanOfficer.PreparerName;
                destLoanOfficer.LoanOriginatorIdentifier = srcLoanOfficer.LoanOriginatorIdentifier;
                destLoanOfficer.LicenseNumOfAgent = srcLoanOfficer.LicenseNumOfAgent;
                destLoanOfficer.EmailAddr = srcLoanOfficer.EmailAddr;
                destLoanOfficer.Phone = srcLoanOfficer.Phone;
                destLoanOfficer.AgentRoleT = srcLoanOfficer.AgentRoleT;
                destLoanOfficer.IsLocked = srcLoanOfficer.IsLocked;
                destLoanOfficer.Update();

                IPreparerFields destMb = destDataLoan.GetPreparerOfForm(E_PreparerFormT.TRIDClosingCosts_MortgageBroker, E_ReturnOptionIfNotExist.CreateNew);
                IPreparerFields srcMb = srcDataLoan.GetPreparerOfForm(E_PreparerFormT.TRIDClosingCosts_MortgageBroker, E_ReturnOptionIfNotExist.CreateNew);
                destMb.CompanyName = srcMb.CompanyName;
                destMb.CompanyLoanOriginatorIdentifier = srcMb.CompanyLoanOriginatorIdentifier;
                destMb.LicenseNumOfCompany = srcMb.LicenseNumOfCompany;
                destMb.AgentRoleT = srcMb.AgentRoleT;
                destMb.IsLocked = srcMb.IsLocked;
                destMb.Update();

                destDataLoan.CopysHousingExpenseJson(srcDataLoan.GetsHousingExpenseJson());
                destDataLoan.CopysClosingCostSetJson(srcDataLoan.GetsClosingCostSetJson());

                E_PreparerFormT[] preparerList = { E_PreparerFormT.CreditEquifax, E_PreparerFormT.CreditExperian, E_PreparerFormT.CreditTransUnion };
                foreach (E_PreparerFormT type in preparerList)
                {
                    IPreparerFields destPreparer = destDataLoan.GetPreparerOfForm(type, E_ReturnOptionIfNotExist.CreateNew);
                    IPreparerFields srcPreparer = srcDataLoan.GetPreparerOfForm(type, E_ReturnOptionIfNotExist.CreateNew);

                    destPreparer.CompanyName = srcPreparer.CompanyName;
                    destPreparer.StreetAddr = srcPreparer.StreetAddr;
                    destPreparer.City = srcPreparer.City;
                    destPreparer.State = srcPreparer.State;
                    destPreparer.Zip = srcPreparer.Zip;
                    destPreparer.PhoneOfCompany = srcPreparer.PhoneOfCompany;
                    destPreparer.Update();
                }

                // Duplicate Credit Report.
                CLoanFileCreator.DuplicateCreditReport(principal.BrokerId, principal.UserId, srcDataLoan.sLId, destDataLoan.sLId);

                // Duplicate tasks.
                CLoanFileCreator.DuplicateLoanTasks(
                    "Starting new task.", 
                    principal.BrokerId,
                    srcDataLoan.sLId, 
                    destDataLoan.sLId, 
                    creator.LoanName, 
                    principal.UserId, 
                    principal.LoginNm, 
                    creator.CreatedOn);

                // 8/30/2013 gf - OPM 136559 Duplicate new task system tasks.
                // Don't need to wait until role assignments are saved because 
                // these are not task templates.
                Task.DuplicateTasksToLoan(
                    principal.BrokerId, 
                    srcDataLoan.sLId, 
                    destDataLoan.sLId, 
                    principal.EmployeeId,
                    false,
                    false /* Don't check permission of src file */,
                    false /* Don't copy condition sort order from lead file */);

                // 8/19/15 gf - Per opm 222507, we need to copy the branch from the source lead.
                destDataLoan.AssignBranch(srcDataLoan.sBranchId);

                destDataLoan.Save();
                creator.CommitFileCreation(false);

                oldLeadNum = srcDataLoan.sLNm;
                results.ConvertedLoanNumber = destDataLoan.sLNm;
                loanToRecordAuditIn = destDataLoan.sLId;

                // 8/19/15 gf - We need to suppress branch assignment to make sure we maintain
                // the branch id from the source file.
                CLoanFileCreator.WriteInitialOfficialAgentList(destDataLoan.sLId, broker, BranchAssignmentOption.Suppress);

                // 7/22/2015 - je - OPM 217312 - Copy linked loan info (Needs to be done before loan is declared invalid).
                if (srcDataLoan.sLinkedLoanInfo.IsLoanLinked)
                {
                    Tools.LinkLoans(principal.BrokerId, destDataLoan.sLId, srcDataLoan.sLinkedLoanInfo.LinkedLId);
                }

                string refNum = srcDataLoan.sLRefNm; // Store reference number before deleting.
                
                Tools.SystemDeclareLoanFileInvalid(principal, parameters.LeadId, false, false, isLeadToLoan: true); // Delete the lead file.

                bool regenMersMin = parameters.RemoveLeadPrefix && broker.IsAutoGenerateMersMin && !string.IsNullOrEmpty(broker.MersFormat);
                if (setNameAfterSrcDeleted || regenMersMin || !parameters.ConvertFromQp2File)
                {
                    // Update values that cannot be updated until after source loan is invalidated (OPM 243181, OPM 244087, OPM 246033).
                    destDataLoan = new CFullAccessPageData(destDataLoan.sLId, new string[] { "sLNm", "sLRefNm", "sfSetReferenceNumber", "sMersMin" });
                    destDataLoan.ByPassFieldSecurityCheck = true;
                    destDataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                    if (setNameAfterSrcDeleted)
                    {
                        destDataLoan.SetsLNmWithPermissionBypass(loanName);
                        results.ConvertedLoanNumber = loanName;
                    }

                    // OPM 244087 - Do not copy reference number over when converting from QP loan.
                    if (!parameters.ConvertFromQp2File)
                    {
                        destDataLoan.SetReferenceNumber(refNum);
                    }

                    // OPM 273220 - Blank out MERS MIN if MERS generation uses loan name and loan name is produced by removing prefix.
                    if (regenMersMin)
                    {
                        string mersMin;
                        string mersErrors;
                        if (!Tools.TryGenerateMersMin(principal, destDataLoan.sLNm, out mersMin, out mersErrors))
                        {
                            mersMin = string.Empty;
                            results.MersErrors = mersErrors;
                        }

                        destDataLoan.DisableMersMinCheck = true;
                        destDataLoan.sMersMin = mersMin;
                        destDataLoan.DisableMersMinCheck = false;
                    }

                    destDataLoan.Save();
                }

                // Copy Edocs 
                foreach (Tuple<Guid, Guid> appMapping in appMap)
                {
                    SqlParameter[] sqlParameters =
                        {
                            new SqlParameter("@BrokerId", broker.BrokerID),
                            new SqlParameter("@src_sLId", srcDataLoan.sLId),
                            new SqlParameter("@src_aAppId", appMapping.Item1),
                            new SqlParameter("@dst_sLId", destDataLoan.sLId),
                            new SqlParameter("@dst_aAppId", appMapping.Item2)
                        };
                    StoredProcedureHelper.ExecuteNonQuery(broker.BrokerID, "EDOCS_MoveDocs", 1, sqlParameters);
                }

                // Give Consumer portal usesr access 
                foreach (var item in ConsumerPortalUser.GetConsumerPortalUsersWithAccessToLoan(broker.BrokerID, srcDataLoan.sLId))
                {
                    ConsumerPortalUser.LinkLoan(broker.BrokerID, item.Item1.Id, destDataLoan.sLId);
                }

                foreach (var entry in appMap)
                {
                    ConsumerActionItem.MoveRequestAndResponse(broker.BrokerID, srcDataLoan.sLId, entry.Item1, destDataLoan.sLId, entry.Item2);
                    PendingDocumentRequest.MoveExistingToNewLoan(broker.BrokerID, srcDataLoan.sLId, destDataLoan.sLId, entry.Item1, entry.Item2);
                }

                if (parameters.ConvertFromQp2File)
                {
                    // putting this after loan file conversion.  The order is important for the cleanup process.
                    QuickPricer2LoanPoolManager.RemoveFromPool(principal, parameters.LeadId);
                }

                LoanCreationFromLeadAuditItem audit = new LoanCreationFromLeadAuditItem(LendersOffice.Security.PrincipalFactory.CurrentPrincipal, oldLeadNum, results.ConvertedLoanNumber);
                AuditManager.RecordAudit(loanToRecordAuditIn, audit);

                if (CanReadConvertedLoan(principal, convertedLoanId.Value))
                {
                    results.ConvertedLoanId = convertedLoanId ?? Guid.Empty;
                    return results;
                }

                results.Message = "You do not have permission to access the converted loan.";
                return results;
            }
            catch (CBaseException exc)
            {
                if (exc.DeveloperMessage.StartsWith("You don't have permission to delete this loan."))
                {
                    results.Message = "Conversion successful.  The new loan has been created, but the original" + Environment.NewLine + "lead could not be deleted because you do not have permission to delete files.";
                    results.ConvertedLoanId = convertedLoanId ?? Guid.Empty;
                    return results;
                }

                results.Message = ErrorMessages.UnableToConvertLead + Environment.NewLine + "Reason: " + exc.UserMessage;
                if (exc.UserMessage.Equals(JsMessages.LoanInfo_DuplicateLoanNumber))
                {
                    return results;
                }
                else
                {
                    throw new CBaseException(results.Message, exc);
                }
            }
        }

        /// <summary>
        /// Saves the race and ethnicity data to the destination app.
        /// </summary>
        /// <param name="destDataApp">The destination app.</param>
        /// <param name="srcDataApp">The source app.</param>
        private static void AssignRaceEthnicityData(CAppData destDataApp, CAppData srcDataApp)
        {
            destDataApp.aBIsMexican = srcDataApp.aBIsMexican;
            destDataApp.aCIsMexican = srcDataApp.aCIsMexican;
            destDataApp.aBIsPuertoRican = srcDataApp.aBIsPuertoRican;
            destDataApp.aCIsPuertoRican = srcDataApp.aCIsPuertoRican;
            destDataApp.aBIsCuban = srcDataApp.aBIsCuban;
            destDataApp.aCIsCuban = srcDataApp.aCIsCuban;
            destDataApp.aBIsOtherHispanicOrLatino = srcDataApp.aBIsOtherHispanicOrLatino;
            destDataApp.aCIsOtherHispanicOrLatino = srcDataApp.aCIsOtherHispanicOrLatino;
            destDataApp.aBDoesNotWishToProvideEthnicity = srcDataApp.aBDoesNotWishToProvideEthnicity;
            destDataApp.aCDoesNotWishToProvideEthnicity = srcDataApp.aCDoesNotWishToProvideEthnicity;
            destDataApp.aBIsAmericanIndian = srcDataApp.aBIsAmericanIndian;
            destDataApp.aCIsAmericanIndian = srcDataApp.aCIsAmericanIndian;
            destDataApp.aBIsAsian = srcDataApp.aBIsAsian;
            destDataApp.aCIsAsian = srcDataApp.aCIsAsian;
            destDataApp.aBIsAsianIndian = srcDataApp.aBIsAsianIndian;
            destDataApp.aCIsAsianIndian = srcDataApp.aCIsAsianIndian;
            destDataApp.aBIsChinese = srcDataApp.aBIsChinese;
            destDataApp.aCIsChinese = srcDataApp.aCIsChinese;
            destDataApp.aBIsFilipino = srcDataApp.aBIsFilipino;
            destDataApp.aCIsFilipino = srcDataApp.aCIsFilipino;
            destDataApp.aBIsJapanese = srcDataApp.aBIsJapanese;
            destDataApp.aCIsJapanese = srcDataApp.aCIsJapanese;
            destDataApp.aBIsKorean = srcDataApp.aBIsKorean;
            destDataApp.aCIsKorean = srcDataApp.aCIsKorean;
            destDataApp.aBIsVietnamese = srcDataApp.aBIsVietnamese;
            destDataApp.aCIsVietnamese = srcDataApp.aCIsVietnamese;
            destDataApp.aBIsOtherAsian = srcDataApp.aBIsOtherAsian;
            destDataApp.aCIsOtherAsian = srcDataApp.aCIsOtherAsian;
            destDataApp.aBIsWhite = srcDataApp.aBIsWhite;
            destDataApp.aCIsWhite = srcDataApp.aCIsWhite;
            destDataApp.aBDoesNotWishToProvideRace = srcDataApp.aBDoesNotWishToProvideRace;
            destDataApp.aCDoesNotWishToProvideRace = srcDataApp.aCDoesNotWishToProvideRace;
            destDataApp.aBIsBlack = srcDataApp.aBIsBlack;
            destDataApp.aCIsBlack = srcDataApp.aCIsBlack;
            destDataApp.aBIsPacificIslander = srcDataApp.aBIsPacificIslander;
            destDataApp.aCIsPacificIslander = srcDataApp.aCIsPacificIslander;
            destDataApp.aBIsNativeHawaiian = srcDataApp.aBIsNativeHawaiian;
            destDataApp.aCIsNativeHawaiian = srcDataApp.aCIsNativeHawaiian;
            destDataApp.aBIsGuamanianOrChamorro = srcDataApp.aBIsGuamanianOrChamorro;
            destDataApp.aCIsGuamanianOrChamorro = srcDataApp.aCIsGuamanianOrChamorro;
            destDataApp.aBIsSamoan = srcDataApp.aBIsSamoan;
            destDataApp.aCIsSamoan = srcDataApp.aCIsSamoan;
            destDataApp.aBIsOtherPacificIslander = srcDataApp.aBIsOtherPacificIslander;
            destDataApp.aCIsOtherPacificIslander = srcDataApp.aCIsOtherPacificIslander;
            destDataApp.aBOtherHispanicOrLatinoDescription = srcDataApp.aBOtherHispanicOrLatinoDescription;
            destDataApp.aCOtherHispanicOrLatinoDescription = srcDataApp.aCOtherHispanicOrLatinoDescription;
            destDataApp.aBOtherAmericanIndianDescription = srcDataApp.aBOtherAmericanIndianDescription;
            destDataApp.aCOtherAmericanIndianDescription = srcDataApp.aCOtherAmericanIndianDescription;
            destDataApp.aBOtherAsianDescription = srcDataApp.aBOtherAsianDescription;
            destDataApp.aCOtherAsianDescription = srcDataApp.aCOtherAsianDescription;
            destDataApp.aBOtherPacificIslanderDescription = srcDataApp.aBOtherPacificIslanderDescription;
            destDataApp.aCOtherPacificIslanderDescription = srcDataApp.aCOtherPacificIslanderDescription;
            destDataApp.aBEthnicityCollectedByObservationOrSurname = srcDataApp.aBEthnicityCollectedByObservationOrSurname;
            destDataApp.aCEthnicityCollectedByObservationOrSurname = srcDataApp.aCEthnicityCollectedByObservationOrSurname;
            destDataApp.aBSexCollectedByObservationOrSurname = srcDataApp.aBSexCollectedByObservationOrSurname;
            destDataApp.aCSexCollectedByObservationOrSurname = srcDataApp.aCSexCollectedByObservationOrSurname;
            destDataApp.aBRaceCollectedByObservationOrSurname = srcDataApp.aBRaceCollectedByObservationOrSurname;
            destDataApp.aCRaceCollectedByObservationOrSurname = srcDataApp.aCRaceCollectedByObservationOrSurname;
            destDataApp.aBInterviewMethodT = srcDataApp.aBInterviewMethodT;
            destDataApp.aCInterviewMethodT = srcDataApp.aCInterviewMethodT;
            destDataApp.aBHispanicT = srcDataApp.aBHispanicT;
            destDataApp.aCHispanicT = srcDataApp.aCHispanicT;
            destDataApp.aBGender = srcDataApp.aBGender;
            destDataApp.aCGender = srcDataApp.aCGender;
            destDataApp.aBHispanicTFallback = srcDataApp.aBHispanicTFallback;
            destDataApp.aCHispanicTFallback = srcDataApp.aCHispanicTFallback;
            destDataApp.aBGenderFallback = srcDataApp.aBGenderFallback;
            destDataApp.aCGenderFallback = srcDataApp.aCGenderFallback;
            destDataApp.aIntrvwrMethodT = srcDataApp.aIntrvwrMethodT;
            destDataApp.aIntrvwrMethodTLckd = srcDataApp.aIntrvwrMethodTLckd;
        }

        /// <summary>
        /// Copies the housing history legacy fields as well as the mailing and post-closing addresses.
        /// </summary>
        /// <param name="srcDataLoan">The loan being copied from.</param>
        /// <param name="srcDataApp">The application being copied from.</param>
        /// <param name="destDataApp">The application being updated.</param>
        private static void CopyBorrowerAddressFields(CPageData srcDataLoan, CAppData srcDataApp, CAppData destDataApp)
        {
            destDataApp.aBAddrMailSourceT = srcDataApp.aBAddrMailSourceT;
            destDataApp.aBMailingAddressData = srcDataApp.aBMailingAddressData;
            destDataApp.aBAddrPostSourceTLckd = srcDataApp.aBAddrPostSourceTLckd;
            destDataApp.aBAddrPostSourceT = srcDataApp.aBAddrPostSourceT;
            destDataApp.aBPostClosingAddressData = srcDataApp.aBPostClosingAddressData;

            destDataApp.aCAddrMailSourceT = srcDataApp.aCAddrMailSourceT;
            destDataApp.aCMailingAddressData = srcDataApp.aCMailingAddressData;
            destDataApp.aCAddrPostSourceTLckd = srcDataApp.aCAddrPostSourceTLckd;
            destDataApp.aCAddrPostSourceT = srcDataApp.aCAddrPostSourceT;
            destDataApp.aCPostClosingAddressData = srcDataApp.aCPostClosingAddressData;

            if (!srcDataLoan.sIsHousingHistoryEntriesCollectionEnabled)
            {
                destDataApp.aBAddr = srcDataApp.aBAddr;
                destDataApp.aBCity = srcDataApp.aBCity;
                destDataApp.aBState = srcDataApp.aBState;
                destDataApp.aBZip = srcDataApp.aBZip;
                destDataApp.aBAddrT = srcDataApp.aBAddrT;
                destDataApp.aBAddrYrs = srcDataApp.aBAddrYrs;
                destDataApp.aBPrev1Addr = srcDataApp.aBPrev1Addr;
                destDataApp.aBPrev1City = srcDataApp.aBPrev1City;
                destDataApp.aBPrev1State = srcDataApp.aBPrev1State;
                destDataApp.aBPrev1Zip = srcDataApp.aBPrev1Zip;
                destDataApp.aBPrev1AddrT = srcDataApp.aBPrev1AddrT;
                destDataApp.aBPrev1AddrYrs = srcDataApp.aBPrev1AddrYrs;
                destDataApp.aBPrev2Addr = srcDataApp.aBPrev2Addr;
                destDataApp.aBPrev2City = srcDataApp.aBPrev2City;
                destDataApp.aBPrev2State = srcDataApp.aBPrev2State;
                destDataApp.aBPrev2Zip = srcDataApp.aBPrev2Zip;
                destDataApp.aBPrev2AddrT = srcDataApp.aBPrev2AddrT;
                destDataApp.aBPrev2AddrYrs = srcDataApp.aBPrev2AddrYrs;
                destDataApp.aBAddrMail = srcDataApp.aBAddrMail;
                destDataApp.aBCityMail = srcDataApp.aBCityMail;
                destDataApp.aBStateMail = srcDataApp.aBStateMail;
                destDataApp.aBZipMail = srcDataApp.aBZipMail;
                destDataApp.aBAddrPost = srcDataApp.aBAddrPost;
                destDataApp.aBCityPost = srcDataApp.aBCityPost;
                destDataApp.aBStatePost = srcDataApp.aBStatePost;
                destDataApp.aBZipPost = srcDataApp.aBZipPost;

                destDataApp.aCAddr = srcDataApp.aCAddr;
                destDataApp.aCCity = srcDataApp.aCCity;
                destDataApp.aCState = srcDataApp.aCState;
                destDataApp.aCZip = srcDataApp.aCZip;
                destDataApp.aCAddrT = srcDataApp.aCAddrT;
                destDataApp.aCAddrYrs = srcDataApp.aCAddrYrs;
                destDataApp.aCPrev1Addr = srcDataApp.aCPrev1Addr;
                destDataApp.aCPrev1City = srcDataApp.aCPrev1City;
                destDataApp.aCPrev1State = srcDataApp.aCPrev1State;
                destDataApp.aCPrev1Zip = srcDataApp.aCPrev1Zip;
                destDataApp.aCPrev1AddrT = srcDataApp.aCPrev1AddrT;
                destDataApp.aCPrev1AddrYrs = srcDataApp.aCPrev1AddrYrs;
                destDataApp.aCPrev2Addr = srcDataApp.aCPrev2Addr;
                destDataApp.aCPrev2City = srcDataApp.aCPrev2City;
                destDataApp.aCPrev2State = srcDataApp.aCPrev2State;
                destDataApp.aCPrev2Zip = srcDataApp.aCPrev2Zip;
                destDataApp.aCPrev2AddrT = srcDataApp.aCPrev2AddrT;
                destDataApp.aCPrev2AddrYrs = srcDataApp.aCPrev2AddrYrs;
                destDataApp.aCAddrMail = srcDataApp.aCAddrMail;
                destDataApp.aCCityMail = srcDataApp.aCCityMail;
                destDataApp.aCStateMail = srcDataApp.aCStateMail;
                destDataApp.aCZipMail = srcDataApp.aCZipMail;
                destDataApp.aCAddrPost = srcDataApp.aCAddrPost;
                destDataApp.aCCityPost = srcDataApp.aCCityPost;
                destDataApp.aCStatePost = srcDataApp.aCStatePost;
                destDataApp.aCZipPost = srcDataApp.aCZipPost;
            }
        }

        /// <summary>
        /// Verifies that the user has workflow permission to run lead-to-loan conversion.
        /// </summary>
        /// <param name="user">
        /// The user running lead-to-loan conversion.
        /// </param>
        /// <param name="parameters">
        /// The parameters for the lead being converted.
        /// </param>
        /// <param name="denialMessage">
        /// The denial message selected by workflow, if the user does not have permission
        /// to run lead-to-loan conversion.
        /// </param>
        /// <returns>
        /// True, if the operation was verified, false otherwise.
        /// </returns>
        private static bool VerifyLeadConversionPermission(AbstractUserPrincipal user, LeadConversionParameters parameters, out string denialMessage)
        {
            denialMessage = null;

            if (parameters.ConvertFromQp2File)
            {
                // Per PDE, QP2 files are mainly for providing pricing 
                // quotes and conversion to a lead or loan file should
                // not be restricted by workflow.
                return true;
            }

            var valueEvaluator = new LoanValueEvaluator(user.ConnectionInfo, user.BrokerId, parameters.LeadId, WorkflowOperations.LeadToLoanConversion);
            valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(user));

            if (LendingQBExecutingEngine.CanPerform(WorkflowOperations.LeadToLoanConversion, valueEvaluator))
            {
                return true;
            }

            denialMessage = LendingQBExecutingEngine.GetUserFriendlyMessage(WorkflowOperations.LeadToLoanConversion, valueEvaluator);
            return false;
        }

        /// <summary>
        /// Copys construction fields from the source to the destination.
        /// </summary>
        /// <param name="destDataLoan">The destination loan.</param>
        /// <param name="srcDataLoan">The source loan.</param>
        private static void CopyConstructionFields(CPageData destDataLoan, CPageData srcDataLoan)
        {
            destDataLoan.sConstructionIntAccrualD = srcDataLoan.sConstructionIntAccrualD;
            destDataLoan.sConstructionFirstPaymentD = srcDataLoan.sConstructionFirstPaymentD;
            destDataLoan.sConstructionToPermanentClosingT = srcDataLoan.sConstructionToPermanentClosingT;
            destDataLoan.sConstructionDiscT = srcDataLoan.sConstructionDiscT;
            destDataLoan.sConstructionAmortT = srcDataLoan.sConstructionAmortT;
            destDataLoan.sConstructionRAdj1stCapR = srcDataLoan.sConstructionRAdj1stCapR;
            destDataLoan.sConstructionRAdj1stCapMon = srcDataLoan.sConstructionRAdj1stCapMon;
            destDataLoan.sConstructionRAdjCapR = srcDataLoan.sConstructionRAdjCapR;
            destDataLoan.sConstructionRAdjCapMon = srcDataLoan.sConstructionRAdjCapMon;
            destDataLoan.sConstructionRAdjLifeCapR = srcDataLoan.sConstructionRAdjLifeCapR;
            destDataLoan.sConstructionRAdjMarginR = srcDataLoan.sConstructionRAdjMarginR;
            destDataLoan.sConstructionArmIndexNameVstr = srcDataLoan.sConstructionArmIndexNameVstr;
            destDataLoan.sConstructionRAdjIndexR = srcDataLoan.sConstructionRAdjIndexR;
            destDataLoan.sConstructionRAdjFloorR = srcDataLoan.sConstructionRAdjFloorR;
            destDataLoan.sConstructionRAdjRoundT = srcDataLoan.sConstructionRAdjRoundT;
            destDataLoan.sConstructionRAdjRoundToR = srcDataLoan.sConstructionRAdjRoundToR;
            destDataLoan.sLotOwnerT = srcDataLoan.sLotOwnerT;
            destDataLoan.sConstructionPurposeT = srcDataLoan.sConstructionPurposeT;
            destDataLoan.sConstructionIntCalcT = srcDataLoan.sConstructionIntCalcT;
            destDataLoan.sConstructionMethodT = srcDataLoan.sConstructionMethodT;
            destDataLoan.sConstructionPhaseIntPaymentTimingT = srcDataLoan.sConstructionPhaseIntPaymentTimingT;
            destDataLoan.sConstructionPhaseIntPaymentFrequencyT = srcDataLoan.sConstructionPhaseIntPaymentFrequencyT;
            destDataLoan.sConstructionPhaseIntAccrualT = srcDataLoan.sConstructionPhaseIntAccrualT;
            destDataLoan.sSubsequentlyPaidFinanceChargeAmt = srcDataLoan.sSubsequentlyPaidFinanceChargeAmt;
            destDataLoan.sConstructionInitialAdvanceAmt = srcDataLoan.sConstructionInitialAdvanceAmt;
            destDataLoan.sIsIntReserveRequired = srcDataLoan.sIsIntReserveRequired;
            destDataLoan.sIsAdvanceScheduleKnown = srcDataLoan.sIsAdvanceScheduleKnown;
            destDataLoan.sConstructionLoanD = srcDataLoan.sConstructionLoanD;
        }

        /// <summary>
        /// Transfer ULAD declarations data from the source to the destination application.
        /// </summary>
        /// <param name="destDataApp">
        /// The destination application.
        /// </param>
        /// <param name="srcDataApp">
        /// The source application.
        /// </param>
        private static void TransferUladDeclarationData(CAppData destDataApp, CAppData srcDataApp)
        {
            destDataApp.aBDecAmtBorrowedFromOthersForTransUlad = srcDataApp.aBDecAmtBorrowedFromOthersForTransUlad;
            destDataApp.aCDecAmtBorrowedFromOthersForTransUlad = srcDataApp.aCDecAmtBorrowedFromOthersForTransUlad;
            destDataApp.aBDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueUlad = srcDataApp.aBDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueUlad;
            destDataApp.aCDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueUlad = srcDataApp.aCDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueUlad;
            destDataApp.aBDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueExplanationUlad = srcDataApp.aBDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueExplanationUlad;
            destDataApp.aCDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueExplanationUlad = srcDataApp.aCDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueExplanationUlad;
            destDataApp.aBDecHasBankruptcyChapter11Ulad = srcDataApp.aBDecHasBankruptcyChapter11Ulad;
            destDataApp.aCDecHasBankruptcyChapter11Ulad = srcDataApp.aCDecHasBankruptcyChapter11Ulad;
            destDataApp.aBDecHasBankruptcyChapter12Ulad = srcDataApp.aBDecHasBankruptcyChapter12Ulad;
            destDataApp.aCDecHasBankruptcyChapter12Ulad = srcDataApp.aCDecHasBankruptcyChapter12Ulad;
            destDataApp.aBDecHasBankruptcyChapter13Ulad = srcDataApp.aBDecHasBankruptcyChapter13Ulad;
            destDataApp.aCDecHasBankruptcyChapter13Ulad = srcDataApp.aCDecHasBankruptcyChapter13Ulad;
            destDataApp.aBDecHasBankruptcyChapter7Ulad = srcDataApp.aBDecHasBankruptcyChapter7Ulad;
            destDataApp.aCDecHasBankruptcyChapter7Ulad = srcDataApp.aCDecHasBankruptcyChapter7Ulad;
            destDataApp.aBDecHasBankruptcyLast7YearsUlad = srcDataApp.aBDecHasBankruptcyLast7YearsUlad;
            destDataApp.aCDecHasBankruptcyLast7YearsUlad = srcDataApp.aCDecHasBankruptcyLast7YearsUlad;
            destDataApp.aBDecHasBankruptcyLast7YearsExplanationUlad = srcDataApp.aBDecHasBankruptcyLast7YearsExplanationUlad;
            destDataApp.aCDecHasBankruptcyLast7YearsExplanationUlad = srcDataApp.aCDecHasBankruptcyLast7YearsExplanationUlad;
            destDataApp.aBDecHasConveyedTitleInLieuOfFcLast7YearsUlad = srcDataApp.aBDecHasConveyedTitleInLieuOfFcLast7YearsUlad;
            destDataApp.aCDecHasConveyedTitleInLieuOfFcLast7YearsUlad = srcDataApp.aCDecHasConveyedTitleInLieuOfFcLast7YearsUlad;
            destDataApp.aBDecHasConveyedTitleInLieuOfFcLast7YearsExplanationUlad = srcDataApp.aBDecHasConveyedTitleInLieuOfFcLast7YearsExplanationUlad;
            destDataApp.aCDecHasConveyedTitleInLieuOfFcLast7YearsExplanationUlad = srcDataApp.aCDecHasConveyedTitleInLieuOfFcLast7YearsExplanationUlad;
            destDataApp.aBDecHasForeclosedLast7YearsUlad = srcDataApp.aBDecHasForeclosedLast7YearsUlad;
            destDataApp.aCDecHasForeclosedLast7YearsUlad = srcDataApp.aCDecHasForeclosedLast7YearsUlad;
            destDataApp.aBDecHasForeclosedLast7YearsExplanationUlad = srcDataApp.aBDecHasForeclosedLast7YearsExplanationUlad;
            destDataApp.aCDecHasForeclosedLast7YearsExplanationUlad = srcDataApp.aCDecHasForeclosedLast7YearsExplanationUlad;
            destDataApp.aBDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscUlad = srcDataApp.aBDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscUlad;
            destDataApp.aCDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscUlad = srcDataApp.aCDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscUlad;
            destDataApp.aBDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscExplanationUlad = srcDataApp.aBDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscExplanationUlad;
            destDataApp.aCDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscExplanationUlad = srcDataApp.aCDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscExplanationUlad;
            destDataApp.aBDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscUlad = srcDataApp.aBDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscUlad;
            destDataApp.aCDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscUlad = srcDataApp.aCDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscUlad;
            destDataApp.aBDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscExplanationUlad = srcDataApp.aBDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscExplanationUlad;
            destDataApp.aCDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscExplanationUlad = srcDataApp.aCDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscExplanationUlad;
            destDataApp.aBDecHasOutstandingJudgmentsUlad = srcDataApp.aBDecHasOutstandingJudgmentsUlad;
            destDataApp.aCDecHasOutstandingJudgmentsUlad = srcDataApp.aCDecHasOutstandingJudgmentsUlad;
            destDataApp.aBDecHasOutstandingJudgmentsExplanationUlad = srcDataApp.aBDecHasOutstandingJudgmentsExplanationUlad;
            destDataApp.aCDecHasOutstandingJudgmentsExplanationUlad = srcDataApp.aCDecHasOutstandingJudgmentsExplanationUlad;
            destDataApp.aBDecHasOwnershipIntOtherPropLastThreeYearsUlad = srcDataApp.aBDecHasOwnershipIntOtherPropLastThreeYearsUlad;
            destDataApp.aCDecHasOwnershipIntOtherPropLastThreeYearsUlad = srcDataApp.aCDecHasOwnershipIntOtherPropLastThreeYearsUlad;
            destDataApp.aBDecHasOwnershipIntOtherPropLastThreeYearsExplanationUlad = srcDataApp.aBDecHasOwnershipIntOtherPropLastThreeYearsExplanationUlad;
            destDataApp.aCDecHasOwnershipIntOtherPropLastThreeYearsExplanationUlad = srcDataApp.aCDecHasOwnershipIntOtherPropLastThreeYearsExplanationUlad;
            destDataApp.aBDecIsBorrowingFromOthersForTransUlad = srcDataApp.aBDecIsBorrowingFromOthersForTransUlad;
            destDataApp.aCDecIsBorrowingFromOthersForTransUlad = srcDataApp.aCDecIsBorrowingFromOthersForTransUlad;
            destDataApp.aBDecIsBorrowingFromOthersForTransExplanationUlad = srcDataApp.aBDecIsBorrowingFromOthersForTransExplanationUlad;
            destDataApp.aCDecIsBorrowingFromOthersForTransExplanationUlad = srcDataApp.aCDecIsBorrowingFromOthersForTransExplanationUlad;
            destDataApp.aBDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedUlad = srcDataApp.aBDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedUlad;
            destDataApp.aCDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedUlad = srcDataApp.aCDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedUlad;
            destDataApp.aBDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedExplanationUlad = srcDataApp.aBDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedExplanationUlad;
            destDataApp.aCDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedExplanationUlad = srcDataApp.aCDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedExplanationUlad;
            destDataApp.aBDecIsDelinquentOrDefaultFedDebtUlad = srcDataApp.aBDecIsDelinquentOrDefaultFedDebtUlad;
            destDataApp.aCDecIsDelinquentOrDefaultFedDebtUlad = srcDataApp.aCDecIsDelinquentOrDefaultFedDebtUlad;
            destDataApp.aBDecIsDelinquentOrDefaultFedDebtExplanationUlad = srcDataApp.aBDecIsDelinquentOrDefaultFedDebtExplanationUlad;
            destDataApp.aCDecIsDelinquentOrDefaultFedDebtExplanationUlad = srcDataApp.aCDecIsDelinquentOrDefaultFedDebtExplanationUlad;
            destDataApp.aBDecIsFamilyOrBusAffiliateOfSellerUlad = srcDataApp.aBDecIsFamilyOrBusAffiliateOfSellerUlad;
            destDataApp.aCDecIsFamilyOrBusAffiliateOfSellerUlad = srcDataApp.aCDecIsFamilyOrBusAffiliateOfSellerUlad;
            destDataApp.aBDecIsFamilyOrBusAffiliateOfSellerExplanationUlad = srcDataApp.aBDecIsFamilyOrBusAffiliateOfSellerExplanationUlad;
            destDataApp.aCDecIsFamilyOrBusAffiliateOfSellerExplanationUlad = srcDataApp.aCDecIsFamilyOrBusAffiliateOfSellerExplanationUlad;
            destDataApp.aBDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityUlad = srcDataApp.aBDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityUlad;
            destDataApp.aCDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityUlad = srcDataApp.aCDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityUlad;
            destDataApp.aBDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityExplanationUlad = srcDataApp.aBDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityExplanationUlad;
            destDataApp.aCDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityExplanationUlad = srcDataApp.aCDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityExplanationUlad;
            destDataApp.aBDecIsPrimaryResidenceUlad = srcDataApp.aBDecIsPrimaryResidenceUlad;
            destDataApp.aCDecIsPrimaryResidenceUlad = srcDataApp.aCDecIsPrimaryResidenceUlad;
            destDataApp.aBDecIsPrimaryResidenceExplanationUlad = srcDataApp.aBDecIsPrimaryResidenceExplanationUlad;
            destDataApp.aCDecIsPrimaryResidenceExplanationUlad = srcDataApp.aCDecIsPrimaryResidenceExplanationUlad;
            destDataApp.aBDecOwnershipIntOtherPropLastThreeYearsPropTypeUlad = srcDataApp.aBDecOwnershipIntOtherPropLastThreeYearsPropTypeUlad;
            destDataApp.aCDecOwnershipIntOtherPropLastThreeYearsPropTypeUlad = srcDataApp.aCDecOwnershipIntOtherPropLastThreeYearsPropTypeUlad;
            destDataApp.aBDecOwnershipIntOtherPropLastThreeYearsTitleHoldTypeUlad = srcDataApp.aBDecOwnershipIntOtherPropLastThreeYearsTitleHoldTypeUlad;
            destDataApp.aCDecOwnershipIntOtherPropLastThreeYearsTitleHoldTypeUlad = srcDataApp.aCDecOwnershipIntOtherPropLastThreeYearsTitleHoldTypeUlad;
            destDataApp.aBDecPropSubjectToHigherPriorityLienThanFirstMtgUlad = srcDataApp.aBDecPropSubjectToHigherPriorityLienThanFirstMtgUlad;
            destDataApp.aCDecPropSubjectToHigherPriorityLienThanFirstMtgUlad = srcDataApp.aCDecPropSubjectToHigherPriorityLienThanFirstMtgUlad;
            destDataApp.aBDecPropSubjectToHigherPriorityLienThanFirstMtgExplanationUlad = srcDataApp.aBDecPropSubjectToHigherPriorityLienThanFirstMtgExplanationUlad;
            destDataApp.aCDecPropSubjectToHigherPriorityLienThanFirstMtgExplanationUlad = srcDataApp.aCDecPropSubjectToHigherPriorityLienThanFirstMtgExplanationUlad;
        }
        
        /// <summary>
        /// Apply various mutations to the parameters to align with valid inputs.
        /// </summary>
        /// <param name="parameters">The parameters to fix.</param>
        /// <param name="broker">The lender's settings.</param>
        private static void FixupParameters(LeadConversionParameters parameters, BrokerDB broker)
        {
            // For QuickPricer files, "if the user chooses to convert to lead or loan without template, then apply the quick pricer template."
            // This is in part to ensure tasks get copied over, as mentioned in 247233.
            if (!parameters.CreateFromTemplate && parameters.ConvertFromQp2File)
            {
                parameters.CreateFromTemplate = true;
                parameters.TemplateId = broker.QuickPricerTemplateId;
            }
        }

        /// <summary>
        /// Checks whether there are any error conditions that would prevent the PML conversion from running.
        /// </summary>
        /// <param name="principal">The user attempting to run a conversion.</param>
        /// <param name="broker">The broker associated with the user running a conversion.</param>
        /// <param name="options">The parameters for the lead conversion.</param>
        /// <returns>An error message if the conversion should be stopped, or null otherwise.</returns>
        private static string ReasonCantRunConvertLeadToLoanImpl(AbstractUserPrincipal principal, BrokerDB broker, LeadConversionParameters options)
        {
            if (options.ConvertToLead && options.CreateFromTemplate && !options.ConvertFromQp2File)
            {
                return "Create lead from template is not yet supported.";
            }

            if (options.ConvertFromQp2File && options.RemoveLeadPrefix)
            {
                return "May not remove lead prefix when converting from QuickPricer 2.0 files.";
            }

            if (options.ConvertFromQp2File && options.CreateFromTemplate && !options.IsEmbeddedPML && options.TemplateId != broker.QuickPricerTemplateId)
            {
                return "May not create loan from template in PML.";
            }

            if (options.ConvertFromQp2File && options.ConvertToLead && !options.IsEmbeddedPML && principal.PortalMode != E_PortalMode.Retail && !options.IsNonQm)
            {
                return "May not create lead in PML.";
            }

            if (options.ConvertFromQp2File && !broker.IsAllowExternalUserToCreateNewLoan && !options.IsEmbeddedPML && principal.PortalMode != E_PortalMode.Retail && !options.IsNonQm)
            {
                return "May not create to lead/loan in PML.";
            }

            if (options.ConvertFromQp2File && !options.ConvertToLead && !options.IsEmbeddedPML && principal.PortalMode == E_PortalMode.Retail && !principal.HasPermission(Permission.AllowCreatingNewLoanFiles))
            {
                return "May not create to loan in PML.";
            }

            if (options.ConvertFromQp2File && options.ConvertToLead && !options.IsEmbeddedPML && principal.PortalMode == E_PortalMode.Retail && !principal.HasPermission(Permission.AllowCreatingNewLeadFiles))
            {
                return "May not create to lead in PML.";
            }

            return null;
        }

        /// <summary>
        /// Determines whether the user who converted the lead also has permission to read the resultant loan file.
        /// </summary>
        /// <param name="user">The user running the conversion.</param>
        /// <param name="loanId">The ID of the converted loan.</param>
        /// <returns>A boolean indicating whether the user can read the converted file.</returns>
        private static bool CanReadConvertedLoan(AbstractUserPrincipal user, Guid loanId)
        {
            LoanValueEvaluator valueEvaluator = new LoanValueEvaluator(user.ConnectionInfo, user.BrokerId, loanId, WorkflowOperations.ReadLoanOrTemplate);
            valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(user));
            return LendingQBExecutingEngine.CanPerform(WorkflowOperations.ReadLoanOrTemplate, valueEvaluator);
        }
    }
}
