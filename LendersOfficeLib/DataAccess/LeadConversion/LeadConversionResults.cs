﻿namespace DataAccess
{
    using System;

    /// <summary>
    /// Encapsulates results from a lead conversion, including the converted loan ID and any
    /// related messages or errors.
    /// </summary>
    public class LeadConversionResults
    {
        /// <summary>
        /// Gets or sets the ID of the loan converted from a lead.
        /// </summary>
        /// <value>The ID of the loan converted from a lead.</value>
        public Guid ConvertedLoanId { get; set; }

        /// <summary>
        /// Gets or sets the loan number for the file converted from a lead.
        /// </summary>
        /// <value>The loan number for the converted file.</value>
        /// <remarks>This is only used by the QuickPricer API.</remarks>
        public string ConvertedLoanNumber { get; set; }

        /// <summary>
        /// Gets or sets a message to pass back to the caller.
        /// </summary>
        /// <value>A message to pass back to the caller.</value>
        /// <remarks>This is only used by PML lead-to-loan conversion.</remarks>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets any errors from the MERS MIN generation.
        /// </summary>
        /// <value>Any errors from the MERS MIN generation.</value>
        public string MersErrors { get; set; }

        /// <summary>
        /// Gets or sets an action that should be taken after conversion.
        /// </summary>
        /// <value>An action that should be taken after conversion.</value>
        /// <remarks>This is only used by LQB lead-to-loan conversion.</remarks>
        public string Action { get; set; }
    }
}
