using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using LendersOffice.Constants;
using LendersOffice.Common;
using LendersOffice.Security;
using System.Threading;
using CommonProjectLib.Common.Lib;

namespace DataAccess
{
    public class CPriceEngineData : CPageData
    {
        private AbstractUserPrincipal Principal
        {
            get
            {
                return BrokerUserPrincipal.CurrentPrincipal ?? (Thread.CurrentPrincipal as AbstractUserPrincipal);
            }
        }

        internal class CPriceEngineBasePage : CPageBaseWrapped
        {
            public override bool IsReadOnly
            {
                get
                {
                    // 11/30/2009 dd - For performance reason CPriceEngineData does not allow to invoke Save.
                    return true;
                }
            }
            private bool m_enableCaching = false;

            // 8/12/2015 - dd - Set this property to true to force comparision between cache and actual value. 
            // It will log error to PaulBunyan when values are different. This is only for debugging purpose only.
            // DO NOT ENABLE ON PRODUCTION.
            private bool m_isVerifyCaching = false;

            private Dictionary<string, decimal> m_cacheDecimalDictionary = new FriendlyDictionary<string, decimal>();
            private List<Tuple<BorrowerClosingCostFee, decimal, IEnumerable<LoanClosingCostFeePayment>>> m_closingCostFeeSumCache = null;
            private Tuple<decimal, decimal> m_QmAmtTotalsCache = null;

            /// <summary>
            /// Enable the region to start cache value of loan file field.
            /// </summary>
            public override void StartCaching()
            {
                if (ConstStage.EnablePricingDataFieldCache == false)
                {
                    return; // NO-OP This feature is turn off.
                }

                this.InvalidateCache();
                this.m_enableCaching = true;
            }

            /// <summary>
            /// Do not cache value of loan file field.
            /// </summary>
            public override void StopCaching()
            {
                this.m_enableCaching = false;
            }

            private decimal GetDecimalCache(string key, Func<decimal> getter)
            {
                if (m_enableCaching == false)
                {
                    return getter();
                }
                else
                {
                    decimal v = 0;

                    bool hasKey = m_cacheDecimalDictionary.TryGetValue(key, out v);
                    if (hasKey == false)
                    {
                        v = getter();
                        m_cacheDecimalDictionary[key] = v;
                    }

                    if (hasKey == true && m_isVerifyCaching == true)
                    {
                        decimal expected = getter();
                        if (v != expected)
                        {
                            Tools.LogError("Caching Mismatch. FieldId=[" + key + "]. CacheValue=[" + v + "]. ActualValue=[" + expected + "]");
                        }
                    }

                    return v;
                }


            }

            public override void InvalidateCache()
            {
                base.InvalidateCache();

                // A null set means recreate the cache.
                m_closingCostFeeSumCache = null;
                m_QmAmtTotalsCache = null;
                m_cacheDecimalDictionary = new Dictionary<string, decimal>();
            }


            public override decimal sTransNetCash
            {
                get
                {
                    return this.GetDecimalCache("sTransNetCash", () => base.sTransNetCash);
                }

                set
                {

                    base.sTransNetCash = value;
                    this.InvalidateCache();
                }
            }

            public override decimal sAprIncludedCc
            {
                get
                {
                    return this.GetDecimalCache("sAprIncludedCc", () => base.sAprIncludedCc);
                }
            }

            public override decimal sFinCharge
            {
                get
                {
                    return this.GetDecimalCache("sFinCharge", ()=> base.sFinCharge);
                }
            }

            public override decimal sMonthlyPmt
            {
                get
                {
                    return this.GetDecimalCache("sMonthlyPmt", () => base.sMonthlyPmt);
                }
            }

            protected override decimal sLenderPaidFeesAmt
            {
                get
                {
                    return this.GetDecimalCache("sLenderPaidFeesAmt", () => base.sLenderPaidFeesAmt);
                }
            }

            [Gfe("sLDiscntProps", E_GfeFlags.RealCost)]
            public override decimal sLDiscnt
            {
                get
                {
                    return this.GetDecimalCache("sLDiscnt", () => base.sLDiscnt);
                }
            }

            public override decimal sTotEstCCPoc
            {
                get
                {
                    return this.GetDecimalCache("sTotEstCCPoc", () => base.sTotEstCCPoc);
                }
            }

            protected override decimal sLenderInitialCreditAmt
            {
                get
                {
                    return this.GetDecimalCache("sLenderInitialCreditAmt", () => base.sLenderInitialCreditAmt);
                }
            }

            public override decimal sFinalLAmt
            {
                get
                {
                    return this.GetDecimalCache("sFinalLAmt", () => base.sFinalLAmt);
                }
            }

            public override decimal sFfUfmipFinanced
            {
                get
                {
                    return this.GetDecimalCache("sFfUfmipFinanced", () => base.sFfUfmipFinanced);
                }
            }

            public override decimal sLAmtCalc
            {
                get
                {
                    return this.GetDecimalCache("sLAmtCalc", ()=> base.sLAmtCalc);
                }
                set
                {
                    base.sLAmtCalc = value;
                    this.InvalidateCache();
                }
            }

            protected override decimal sGfeCreditLenderPaidItemF
            {
                get
                {
                    return this.GetDecimalCache("sGfeCreditLenderPaidItemF", () => base.sGfeCreditLenderPaidItemF);
                }
            }

            public override decimal sGfeTotalFundByLender
            {
                get
                {
                    return this.GetDecimalCache("sGfeTotalFundByLender", () => base.sGfeTotalFundByLender);
                }
            }

            [Gfe("sGfeOriginatorCompFProps", E_GfeFlags.RealCost)]
            public override decimal sGfeOriginatorCompF
            {
                get
                {
                    return this.GetDecimalCache("sGfeOriginatorCompF", () => base.sGfeOriginatorCompF);
                }
            }

            protected override decimal sLenderTargetInitialCreditAmt
            {
                get
                {
                    return this.GetDecimalCache("sLenderTargetInitialCreditAmt", () => base.sLenderTargetInitialCreditAmt);
                }
            }

            public override decimal sRecurringMipPia
            {
                get
                {
                    return this.GetDecimalCache("sRecurringMipPia", () => base.sRecurringMipPia);
                }
            }

            public override decimal sUpfrontMipPia
            {
                get
                {
                    return this.GetDecimalCache("sUpfrontMipPia", () => base.sUpfrontMipPia);
                }
            }

            public override decimal sVaFf
            {
                get
                {
                    return this.GetDecimalCache("sVaFf", () => base.sVaFf);
                }
            }

            public override decimal sProHazInsMb
            {
                get
                {
                    return this.GetDecimalCache("sProHazInsMb", () => base.sProHazInsMb);
                }
            }

            public override decimal sProHazInsR
            {
                get
                {
                    return this.GetDecimalCache("sProHazInsR", () => base.sProHazInsR);
                }
            }

            public override decimal sIPerDay
            {
                get
                {
                    return this.GetDecimalCache("sIPerDay", () => base.sIPerDay);
                }
            }

            public override decimal sProHazIns
            {
                get
                {
                    return this.GetDecimalCache("sProHazIns", () => base.sProHazIns);
                }
            }

            public override decimal sProMIns
            {
                get
                {
                    return this.GetDecimalCache("sProMIns", () => base.sProMIns);
                }
            }

            public override decimal sProRealETxMb
            {
                get
                {
                    return this.GetDecimalCache("sProRealETxMb", () => base.sProRealETxMb);
                }
            }

            public override decimal sProSchoolTx
            {
                get
                {
                    return this.GetDecimalCache("sProSchoolTx", () => base.sProSchoolTx);
                }
            }

            public override decimal sProFloodIns
            {
                get
                {
                    return this.GetDecimalCache("sProFloodIns", () => base.sProFloodIns);
                }
            }


            [Gfe("sAggregateAdjRsrvProps", E_GfeFlags.McawPurch12b)]
            public override decimal sAggregateAdjRsrv
            {
                get
                {
                    return this.GetDecimalCache("sAggregateAdjRsrv", () => base.sAggregateAdjRsrv);
                }
            }


            public override decimal s1006ProHExp
            {
                get
                {
                    return this.GetDecimalCache("s1006ProHExp", () => base.s1006ProHExp);
                }
            }

            public override decimal s1007ProHExp
            {
                get
                {
                    return this.GetDecimalCache("s1007ProHExp", () => base.s1007ProHExp);
                }
            }

            public override decimal sProRealETxR
            {
                get
                {
                    return this.GetDecimalCache("sProRealETxR", () => base.sProRealETxR);
                }
            }

            public override decimal sGfeDiscountPointF
            {
                get
                {
                    return this.GetDecimalCache("sGfeDiscountPointF", () => base.sGfeDiscountPointF);
                }
            }


            [Gfe("sMInsRsrvProps", E_GfeFlags.McawPurch12b)]
            public override decimal sMInsRsrv
            {
                get
                {
                    return this.GetDecimalCache("sMInsRsrv", () => base.sMInsRsrv);
                }
            }


            public override decimal sOriginatorCompensationTotalAmount
            {
                get
                {
                    return this.GetDecimalCache("sOriginatorCompensationTotalAmount", () => base.sOriginatorCompensationTotalAmount);
                }
            }

            protected override decimal SumClosingCostFeeTotalsMatching(Func<LoanClosingCostFee, bool> feePredicate, Func<LoanClosingCostFeePayment, bool> paymentPredicate)
            {
                if (!m_enableCaching || buildingCache)
                {
                    return base.SumClosingCostFeeTotalsMatching(feePredicate, paymentPredicate);
                }

                if (m_closingCostFeeSumCache == null)
                {
                    PopulateClosingCostFeeCache();
                }


                List<decimal> amounts = new List<decimal>();
                foreach (var cachedFeeData in this.m_closingCostFeeSumCache)
                {
                    if (feePredicate(cachedFeeData.Item1))
                    {
                        foreach (var paymentCache in cachedFeeData.Item3)
                        {
                            if (paymentPredicate(paymentCache))
                            {
                                amounts.Add(paymentCache.Amount);
                            }
                        }
                    }
                }

                return this.SumMoney(amounts);
            }

            protected override decimal SumClosingCostFeeTotalsMatching(Func<LoanClosingCostFee, bool> predicate)
            {
                if (!m_enableCaching || buildingCache)
                {
                    return base.SumClosingCostFeeTotalsMatching(predicate);
                }

                if (m_closingCostFeeSumCache == null)
                {
                    PopulateClosingCostFeeCache();
                }

                List<decimal> amounts = new List<decimal>();
                foreach (var cachedFeeData in this.m_closingCostFeeSumCache)
                {
                    if (predicate(cachedFeeData.Item1))
                    {
                        amounts.Add(cachedFeeData.Item2);
                    }
                }

                return this.SumMoney(amounts);
            }

            private bool buildingCache = false;
            private void PopulateClosingCostFeeCache()
            {
                buildingCache = true;
                var data  = new List<Tuple<BorrowerClosingCostFee, decimal, IEnumerable<LoanClosingCostFeePayment>>>();

                Func<BaseClosingCostFee, bool> allValidFeesFilter = bFee => ClosingCostSetUtils.ValidDiscSecDescriptionFilter((BorrowerClosingCostFee)bFee);

                foreach (BorrowerClosingCostFee fee in this.sClosingCostSet.GetFees(allValidFeesFilter))
                {
                    decimal feeAmount = fee.TotalAmount;

                    if (feeAmount == 0)
                    {
                        continue;
                    }

                    var cachePaymentList = new List<LoanClosingCostFeePayment>();

                    var paymentList = fee.GetPaymentsWithCachedTotal(feeAmount);
                    foreach (var payment in paymentList)
                    {
                        if (payment.Amount == 0)
                        {
                            continue;
                        }

                        cachePaymentList.Add(payment);
                    }

                    data.Add(Tuple.Create(fee, feeAmount, paymentList));
                }

                this.m_closingCostFeeSumCache = data;
                buildingCache = false;
            }

 
            protected override decimal SumClosingCostQMAmtTotals(bool financed)
            {
                if (!m_enableCaching)
                {
                    return base.SumClosingCostQMAmtTotals(financed);
                }

                if (m_QmAmtTotalsCache == null)
                {
                    m_QmAmtTotalsCache = Tuple.Create(base.SumClosingCostQMAmtTotals(true), base.SumClosingCostQMAmtTotals(false));
                }

                return financed ? m_QmAmtTotalsCache.Item1 : m_QmAmtTotalsCache.Item2;
            }

            #region Special Caching for OPM 108042
            // Profiler indicates these fields are very hot when accessing directly in datalayer.
            // Cache them here.
            private int? _sTerm = null;
            public override int sTerm
            {
                get
                {
                    if (_sTerm.HasValue == false)
                    {
                        _sTerm = base.sTerm;
                    }

                    return _sTerm.Value;
                }
                set
                {
                    base.sTerm = value;

                    if (_sTerm.HasValue && base.sTerm != _sTerm)
                        _sTerm = null; 
                }
            }

            private int? _sDue = null;
            public override int sDue
            {
                get
                {
                    if (_sDue.HasValue == false)
                    {
                        _sDue = base.sDue;
                    }

                    return _sDue.Value;
                }
                set
                {
                    base.sDue = value;

                    if (_sDue.HasValue && base.sDue != _sDue)
                        _sDue = null;
                }
            }

            private E_sLPurposeT? _sLPurposeT;
            public override E_sLPurposeT sLPurposeT
            {
                get
                {
                    if (_sLPurposeT.HasValue == false)
                    {
                        _sLPurposeT = base.sLPurposeT;
                    }
                    return base.sLPurposeT;
                }
                set
                {
                    base.sLPurposeT = value;

                    if (_sLPurposeT.HasValue && base.sLPurposeT != _sLPurposeT)
                        _sLPurposeT = null;
                }
            }

            private bool? _sIsOptionArm = null;
            public override bool sIsOptionArm
            {
                get
                {
                    if (_sIsOptionArm.HasValue == false)
                    {
                        _sIsOptionArm = base.sIsOptionArm;
                    }

                    return _sIsOptionArm.Value;
                }
                set
                {
                    base.sIsOptionArm = value;

                    if (_sIsOptionArm.HasValue && base.sIsOptionArm != _sIsOptionArm)
                        _sIsOptionArm = null;
                }
            }

            private E_sStatusT? _sStatusT = null;
            public override E_sStatusT sStatusT
            {
                get
                {
                    if (_sStatusT.HasValue == false)
                    {
                        _sStatusT = base.sStatusT;
                    }

                    return _sStatusT.Value;
                }
                set
                {
                    base.sStatusT = value;

                    if (_sStatusT.HasValue && base.sStatusT != _sStatusT)
                        _sStatusT = null;
                }
            }

            // dd 2/24/2017 - The value sDisclosureRegulation does not change during the pricing run. This value is some what expensive to compute. 
            //                This is why I implement cache here.
            private E_sDisclosureRegulationT? _sDisclosureRegulationT = null;
            public override E_sDisclosureRegulationT sDisclosureRegulationT
            {
                get
                {
                    if (_sDisclosureRegulationT.HasValue == false)
                    {
                        _sDisclosureRegulationT = base.sDisclosureRegulationT;
                    }
                    return _sDisclosureRegulationT.Value;
                }

                set
                {
                    base.sDisclosureRegulationT = value;

                    if (_sDisclosureRegulationT.HasValue && base.sDisclosureRegulationT != _sDisclosureRegulationT)
                    {
                        _sDisclosureRegulationT = null;
                    }
                }
            }
            #endregion

            private static CSelectStatementProvider s_selectProvider;

            static CPriceEngineBasePage()
            {
                StringList list = new StringList();

                list.Add("sfRunLpe");
                list.Add("sfPrepareQuickPricerLoan");
                list.Add("GetDataForSecondLien");

                s_selectProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection);

            }

            public CPriceEngineBasePage(Guid fileId)
                : base(fileId, "CPriceEngineData", s_selectProvider)
            {

            }
        }

        public static CPriceEngineData CreatePricingEngineHeloc(Guid sLId)
        {
            return new CPriceEngineData(sLId, new CPriceEngineHelocBasePage(sLId));
        }
        public static CPriceEngineData CreatePricingEngineNonHeloc(Guid sLId)
        {
            return new CPriceEngineData(sLId, new CPriceEngineBasePage(sLId));
        }
        private CPriceEngineData(Guid sLId, CPageBase internalPage)
            : base(sLId)
        {
            m_pageBase = internalPage;
        }
        public CPriceEngineData(Guid fileId)
            : base(fileId)
        {
            if (Tools.IsLoanAHELOC(Principal, fileId))
            {
                m_pageBase = new CPriceEngineHelocBasePage(fileId);
            }
            else
            {
                m_pageBase = new CPriceEngineBasePage(fileId);
            }
        }
        protected override bool m_enforceAccessControl
        {
            get
            {
                // 3/30/2011 dd - We need any P user able to retrieve this.
                return false;
            }
        }


        // Mix the pricing engine performance enhacements with the HELOC special calculations.
        internal class CPriceEngineHelocBasePage : CPageHelocBase
        {
            public override bool IsReadOnly
            {
                get
                {
                    // 11/30/2009 dd - For performance reason CPriceEngineData does not allow to invoke Save.
                    return true;
                }
            }

            #region Special Caching for OPM 108042
            // Profiler indicates these fields are very hot when accessing directly in datalayer.
            // Cache them here.
            private int? _sTerm = null;
            public override int sTerm
            {
                get
                {
                    if (_sTerm.HasValue == false)
                    {
                        _sTerm = base.sTerm;
                    }

                    return _sTerm.Value;
                }
                set
                {
                    base.sTerm = value;

                    if (_sTerm.HasValue && base.sTerm != _sTerm)
                        _sTerm = null;
                }
            }

            private int? _sDue = null;
            public override int sDue
            {
                get
                {
                    if (_sDue.HasValue == false)
                    {
                        _sDue = base.sDue;
                    }

                    return _sDue.Value;
                }
                set
                {
                    base.sDue = value;

                    if (_sDue.HasValue && base.sDue != _sDue)
                        _sDue = null;
                }
            }

            private E_sLPurposeT? _sLPurposeT;
            public override E_sLPurposeT sLPurposeT
            {
                get
                {
                    if (_sLPurposeT.HasValue == false)
                    {
                        _sLPurposeT = base.sLPurposeT;
                    }
                    return base.sLPurposeT;
                }
                set
                {
                    base.sLPurposeT = value;

                    if (_sLPurposeT.HasValue && base.sLPurposeT != _sLPurposeT)
                        _sLPurposeT = null;
                }
            }

            private bool? _sIsOptionArm = null;
            public override bool sIsOptionArm
            {
                get
                {
                    if (_sIsOptionArm.HasValue == false)
                    {
                        _sIsOptionArm = base.sIsOptionArm;
                    }

                    return _sIsOptionArm.Value;
                }
                set
                {
                    base.sIsOptionArm = value;

                    if (_sIsOptionArm.HasValue && base.sIsOptionArm != _sIsOptionArm)
                        _sIsOptionArm = null;
                }
            }

            private E_sStatusT? _sStatusT = null;
            public override E_sStatusT sStatusT
            {
                get
                {
                    if (_sStatusT.HasValue == false)
                    {
                        _sStatusT = base.sStatusT;
                    }

                    return _sStatusT.Value;
                }
                set
                {
                    base.sStatusT = value;

                    if (_sStatusT.HasValue && base.sStatusT != _sStatusT)
                        _sStatusT = null;
                }
            }

            // dd 2/24/2017 - The value sDisclosureRegulation does not change during the pricing run. This value is some what expensive to compute. 
            //                This is why I implement cache here.
            private E_sDisclosureRegulationT? _sDisclosureRegulationT = null;
            public override E_sDisclosureRegulationT sDisclosureRegulationT
            {
                get
                {
                    if (_sDisclosureRegulationT.HasValue == false)
                    {
                        _sDisclosureRegulationT = base.sDisclosureRegulationT;
                    }
                    return _sDisclosureRegulationT.Value;
                }

                set
                {
                    base.sDisclosureRegulationT = value;

                    if (_sDisclosureRegulationT.HasValue && base.sDisclosureRegulationT != _sDisclosureRegulationT)
                    {
                        _sDisclosureRegulationT = null;
                    }
                }
            }
            #endregion

            private static CSelectStatementProvider s_selectProvider;

            static CPriceEngineHelocBasePage()
            {
                StringList list = new StringList();

                list.Add("sfRunLpe");
                list.Add("sfPrepareQuickPricerLoan");
                list.Add("GetDataForSecondLien");

                s_selectProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection);

            }

            public CPriceEngineHelocBasePage(Guid fileId)
                : base(fileId, "CPriceEngineHelocData", s_selectProvider)
            {

            }
        }

    }




}
