﻿// <copyright file="TrackIntegrationModifiedFileProcessor.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   5/6/2014 2:22:38 PM
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Messaging;
    using System.Xml.Linq;
    using CommonProjectLib.Common;
    using LendersOffice.Constants;

    /// <summary>
    /// Process the track integration modified file message queue and insert record into TrackIntegrationModified table.
    /// </summary>
    public class TrackIntegrationModifiedFileProcessor : CommonProjectLib.Runnable.IRunnable
    {
        /// <summary>
        /// Gets the description of the continuous task.
        /// </summary>
        /// <value>The description of the continuous task.</value>
        public string Description
        {
            get { return "TrackIntegrationModifiedFileProcessor - " + ConstStage.MSMQ_TrackIntegrationModifiedFile; }
        }

        /// <summary>
        /// Read the message queue and insert record into TrackIntegrationModified table. Only update record every minutes.
        /// </summary>
        public void Run()
        {
            if (string.IsNullOrEmpty(ConstStage.MSMQ_TrackIntegrationModifiedFile))
            {
                return;
            }

            Tools.ResetLogCorrelationId();

            using (var queue = LendersOffice.Drivers.Gateways.MessageQueueHelper.PrepareForXML(ConstStage.MSMQ_TrackIntegrationModifiedFile, false))
            {
                Dictionary<Guid, DateTime> loanIdIgnoreUntilCache = new Dictionary<Guid, DateTime>();

                while (true)
                {
                    try
                    {
                        DateTime messageReceived;
                        XDocument xdoc = LendersOffice.Drivers.Gateways.MessageQueueHelper.ReceiveXML(queue, null, out messageReceived);
                        if (xdoc == null)
                        {
                            break; // same behavior as MessageQueueException.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout
                        }

                        XElement rootElement = xdoc.Root;

                        Guid loanId = new Guid(rootElement.Attribute("sLId").Value);
                        Guid brokerId = new Guid(rootElement.Attribute("sBrokerId").Value);
                        string loanName = rootElement.Attribute("loanName").Value;

                        DateTime ignoreUntil;
                        if (!loanIdIgnoreUntilCache.TryGetValue(loanId, out ignoreUntil) || messageReceived > ignoreUntil)
                        {
                            SqlParameter[] parameters =
                            {
                                new SqlParameter("@LoanID", loanId),
                                new SqlParameter("@LoanName", loanName),
                                new SqlParameter("@BrokerId", brokerId)
                            };

                            StoredProcedureHelper.ExecuteNonQuery(brokerId, "TrackIntegrationModifiedFile", 5, parameters);
                            loanIdIgnoreUntilCache[loanId] = messageReceived.AddMinutes(1);
                        }
                    }
                    catch (MessageQueueException exc)
                    {
                        if (exc.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout)
                        {
                            break;
                        }

                        throw;
                    }
                    catch (Exception exc)
                    {
                        Tools.LogWarning("TrackIntegrationModifiedFile encountered an exception", exc);
                        throw;
                    }
                }
            }
        }
    }
}