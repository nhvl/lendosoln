using System;
using System.Collections;
using System.Data;

namespace DataAccess
{

	public class CLeadAssignmentData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CLeadAssignmentData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("sStatusT");
            list.Add("sStatusLckd");
            list.Add("TableEmployee");
            list.Add("sOpenedD");
            list.Add("sPreQualD");
            list.Add("sDocsD");
            list.Add("sPreApprovD");
            list.Add("sSubmitD");
            list.Add("sApprovD");
            list.Add("sFundD");
            list.Add("sRecordedD");
            list.Add("sClosedD");
            list.Add("sUnderwritingD");
            list.Add("sOnHoldD");
            list.Add("sSuspendedD");
            list.Add("sCanceledD");
            list.Add("sRejectD");
            list.Add("sLeadD");
            list.Add("sCanceledD");
            list.Add("sLeadDeclinedD");
            list.Add("sLNm");
            list.Add("sShippedToInvestorD");
            list.Add("sMersMin");
            list.Add("sFHALenderIdCode");
            list.Add("sSrcsLId"); // opm 185732.
            list.Add("sConsumerPortalVerbalSubmissionD");
            list.Add("sConsumerPortalVerbalSubmissionComments");
            list.Add("sConsumerPortalCreationD");
            list.Add("sConsumerPortalSubmissionD");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CLeadAssignmentData(Guid fileId) : base(fileId, "CLeadAssignmentData", s_selectProvider)
		{
		}

        protected override bool m_enforceAccessControl
        {
            get { return false; }
        }
    }
}
