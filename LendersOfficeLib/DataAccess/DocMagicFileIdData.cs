using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
	public class CDocMagicFileIdData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CDocMagicFileIdData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
	        list.Add("sDocMagicFileId");
            list.Add("sDisclosureRegulationT");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
        protected override bool m_enforceAccessControl
        {
            get
            {
                return false;
            }
        }

		public CDocMagicFileIdData(Guid fileId) : base(fileId, "CDocMagicFileIdData", s_selectProvider)
		{
		}
	}
}
