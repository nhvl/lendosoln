﻿namespace DataAccess.CounselingOrganization
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Holds agency data retrieved from CFPB.
    /// </summary>
    [DataContract]
    public class CfpbAgencyData
    {
        /// <summary>
        /// The first address line.
        /// </summary>
        [DataMember(Name = "adr1")]
        private string address1 = string.Empty;

        /// <summary>
        /// The second address line.
        /// </summary>
        [DataMember(Name = "adr2")]
        private string address2 = string.Empty;

        /// <summary>
        /// The address's city.
        /// </summary>
        [DataMember(Name = "city")]
        private string city = string.Empty;

        /// <summary>
        /// The state code.
        /// </summary>
        [DataMember(Name = "statecd")]
        private string stateCode = string.Empty;

        /// <summary>
        /// The zip code.
        /// </summary>
        [DataMember(Name = "zipcd")]
        private string zipcode = string.Empty;

        /// <summary>
        /// The first mailing address line.
        /// </summary>
        [DataMember(Name = "mailingadr1")]
        private string mailingAddress1 = string.Empty;

        /// <summary>
        /// The second mailing address line.
        /// </summary>
        [DataMember(Name = "mailingadr2")]
        private string mailingAddress2 = string.Empty;

        /// <summary>
        /// The mailing address city.
        /// </summary>
        [DataMember(Name = "mailingcity")]
        private string mailingCity = string.Empty;

        /// <summary>
        /// The mailing address state code.
        /// </summary>
        [DataMember(Name = "mailingstatecd")]
        private string mailingStateCode = string.Empty;

        /// <summary>
        /// The mailing address zipcode.
        /// </summary>
        [DataMember(Name = "mailingzipcd")]
        private string mailingZipcode = string.Empty;

        /// <summary>
        /// The first phone number.
        /// </summary>
        [DataMember(Name = "phone1")]
        private string phoneNumber1 = string.Empty;

        /// <summary>
        /// The second phone number.
        /// </summary>
        [DataMember(Name = "phone2")]
        private string phoneNumber2 = string.Empty;

        /// <summary>
        /// The fax number.
        /// </summary>
        [DataMember(Name = "fax")]
        private string fax = string.Empty;

        /// <summary>
        /// Gets or sets the name of the agency.
        /// </summary>
        /// <value>The name of the agency.</value>
        [DataMember(Name = "nme")]
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        [DataMember(Name = "email")]
        public string Email
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the website URL.
        /// </summary>
        /// <value>The website URL.</value>
        [DataMember(Name = "weburl")]
        public string Website
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the services provided.
        /// </summary>
        /// <value>The services provided.</value>
        [DataMember(Name = "services")]
        public List<string> Services
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the langauge spoken.
        /// </summary>
        /// <value>The languages spoken.</value>
        [DataMember(Name = "languages")]
        public List<string> Languages
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the distance to the agency.
        /// </summary>
        /// <value>The distance to the agency.</value>
        [DataMember(Name = "distance")]
        public string Distance
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the street address.
        /// </summary>
        /// <value>The street address.</value>
        public string StreetAddress
        {
            get
            {
                if (!string.IsNullOrEmpty(this.address1))
                {
                    return this.address1;
                }
                else
                {
                    return this.mailingAddress1;
                }
            }
        }

        /// <summary>
        /// Gets the second street address line.
        /// </summary>
        /// <value>The second street address line.</value>
        public string StreetAddress2
        {
            get
            {
                if (!string.IsNullOrEmpty(this.address2))
                {
                    return this.address2;
                }
                else
                {
                    return this.mailingAddress2;
                }
            }
        }

        /// <summary>
        /// Gets the address's city.
        /// </summary>
        /// <value>The address's city.</value>
        public string City
        {
            get
            {
                if (!string.IsNullOrEmpty(this.city))
                {
                    return this.city;
                }
                else
                {
                    return this.mailingCity;
                }
            }
        }

        /// <summary>
        /// Gets the address's state code.
        /// </summary>
        /// <value>The address's state code.</value>
        public string State
        {
            get
            {
                if (!string.IsNullOrEmpty(this.stateCode))
                {
                    return this.stateCode;
                }
                else
                {
                    return this.mailingStateCode;
                }
            }
        }

        /// <summary>
        /// Gets the address's zipcode.
        /// </summary>
        /// <value>The address's zipcode.</value>
        public string Zipcode
        {
            get
            {
                if (!string.IsNullOrEmpty(this.zipcode))
                {
                    return this.zipcode.Split('-')[0];
                }
                else
                {
                    return this.mailingZipcode.Split('-')[0];
                }
            }
        }

        /// <summary>
        /// Gets the phone number to use.
        /// </summary>
        /// <value>The phone number to use.</value>
        public string Phone
        {
            get
            {
                string phoneToUse;
                if (!string.IsNullOrEmpty(this.phoneNumber1))
                {
                    phoneToUse = this.phoneNumber1;
                }
                else if (!string.IsNullOrEmpty(this.phoneNumber2))
                {
                    phoneToUse = this.phoneNumber2;
                }
                else
                {
                    phoneToUse = this.fax;
                }

                int firstDashIndex = phoneToUse.IndexOf('-');
                if (firstDashIndex > 0)
                {
                    phoneToUse = $"({phoneToUse.Substring(0, firstDashIndex)}) {phoneToUse.Substring(firstDashIndex + 1)}";
                }

                return phoneToUse;
            }
        }
    }
}
