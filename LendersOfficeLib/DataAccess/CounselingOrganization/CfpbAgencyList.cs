﻿namespace DataAccess.CounselingOrganization
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// A list of counseling agencies from the CFPB.
    /// </summary>
    [DataContract]
    public class CfpbAgencyList
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CfpbAgencyList"/> class.
        /// </summary>
        public CfpbAgencyList()
        {
            this.CounselingAgencies = new List<CfpbAgencyData>();
        }

        /// <summary>
        /// Gets or sets the counseling agencies.
        /// </summary>
        /// <value>The counseling agencies.</value>
        [DataMember(Name = "counseling_agencies")]
        public List<CfpbAgencyData> CounselingAgencies
        {
            get;
            set;
        }
    }
}
