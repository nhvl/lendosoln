﻿namespace DataAccess.CounselingOrganization
{
    using System;
    using System.IO;
    using System.Net;
    using System.Security.Cryptography;
    using System.Text;
    using global::DataAccess;
    using HomeownerCounselingOrganizations;
    using LendersOffice.Common;
    using LendersOffice.Constants;

    /// <summary>
    /// Utilities for getting Counseling Organizations.
    /// </summary>
    public class CounselingOrganizationUtilities
    {
        /// <summary>
        /// Fetches data from CFPB.
        /// </summary>
        /// <param name="zipcode">The zipcode to search for.</param>
        /// <param name="error">Any errors we encounter.</param>
        /// <param name="cfpbData">Any CFPB data we get back.</param>
        /// <returns>True if successful getting data. False otherwise.</returns>
        public static bool FetchDataFromCFPB(string zipcode, out string error, out string cfpbData)
        {
            cfpbData = string.Empty;
            error = string.Empty;

            // Must be valid zipcode.
            if (string.IsNullOrEmpty(zipcode) || !System.Text.RegularExpressions.Regex.IsMatch(zipcode, "^[0-9]{5}$")) 
            {
                string zipcodeDebug = string.IsNullOrEmpty(zipcode) ? string.Empty : "(" + zipcode + ") ";
                error = $"Borrower's present address zip code {zipcodeDebug} is not valid.";
                return false;
            }

            string urlFormatString = ConstStage.CFPBCounselingOrganizationURLFormatString;
            string url = string.Format(urlFormatString, zipcode);
            bool needsDecode = url.ToLower().IndexOf("lpqproxy") > 0;

            StringBuilder log = new StringBuilder("CFPB: ");
            try
            {
                int webExceptionsAllowedBeforeFail = 5;
                while (true)
                {
                    try
                    {
                        if (string.IsNullOrEmpty(urlFormatString))
                        {
                            throw new CBaseException(ErrorMessages.Generic, "Empty string CFPBCounselingOrganizationURLFormatString.");
                        }

                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                        log.AppendLine(url);
                        request.MaximumAutomaticRedirections = 4;
                        request.MaximumResponseHeadersLength = 4;
                        request.Credentials = CredentialCache.DefaultCredentials;
                        string data;
                        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                        using (Stream receiveStream = response.GetResponseStream())
                        {
                            if (needsDecode)
                            {
                                using (Stream base64stream = new CryptoStream(receiveStream, new FromBase64Transform(), CryptoStreamMode.Read))
                                using (StreamReader readStream = new StreamReader(base64stream, Encoding.UTF8))
                                {
                                    // JSON literal.
                                    data = readStream.ReadToEnd();
                                }
                            }
                            else
                            {
                                using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
                                {
                                    // JSON literal.
                                    data = readStream.ReadToEnd();
                                }
                            }
                        }

                        log.AppendLine("Response stream received.");
                        log.AppendLine();
                        log.AppendLine(data);
                        cfpbData = data;
                        return true;
                    }
                    catch (WebException exc)
                    {
                        log.Append("Unable to connect to CFPB. Exception was: ");
                        log.AppendLine(exc.Message);
                        if (--webExceptionsAllowedBeforeFail < 0)
                        {
                            // Rethrow will cause loss of stacktrace.
                            Tools.LogError(exc); 
                            throw;
                        }

                        Tools.SleepAndWakeupRandomlyWithin(0, 500);
                        log.AppendLine("Retrying...");
                    }
                }
            }
            catch (SystemException exc)
            {
                Tools.LogError("Unable to connect to CFPB. Log to this point was: \r\n" + log.ToString(), exc);
                error = "Unable to connect to CFPB";
                return false;
            }
        }

        /// <summary>
        /// Turns the JSON into a CfpbAgencyList object.
        /// </summary>
        /// <param name="json">The json to convert.</param>
        /// <returns>The CfpbAgencyList object.</returns>
        public static CfpbAgencyList CreateAgencyListFromJson(string json)
        {
            return SerializationHelper.JsonNetDeserialize<CfpbAgencyList>(json);
        }

        /// <summary>
        /// Converts the CFPB data into the object supported by our system.
        /// </summary>
        /// <param name="agency">The CFPB agency data.</param>
        /// <returns>The HomeownerCounselingOrganization object.</returns>
        public static HomeownerCounselingOrganization CreateHomeownerCounselingOrgFromCfpbData(CfpbAgencyData agency)
        {
            HomeownerCounselingOrganization organization = new HomeownerCounselingOrganization(shouldSetNewId: true);
            organization.Name = agency.Name;
            organization.Address.StreetAddress = agency.StreetAddress;
            organization.Address.StreetAddress2 = agency.StreetAddress2;
            organization.Address.City = agency.City;
            organization.Address.State = agency.State;
            organization.Address.Zipcode = agency.Zipcode;
            organization.Phone = agency.Phone;
            organization.Email = agency.Email;
            organization.Website = agency.Website;
            organization.Services = agency.Services == null ? string.Empty : string.Join(",", agency.Services);
            organization.Languages = agency.Languages == null ? string.Empty : string.Join(",", agency.Languages);
            organization.Distance = agency.Distance;

            return organization;
        }
    }
}
