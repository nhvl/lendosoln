﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;

    /// <summary>
    /// Interface extracted from CAssetCollection.
    /// </summary>
    public interface IAssetCollection : IRecordCollection
    {
        /// <summary>
        /// Gets all of the Auto assets.
        /// </summary>
        ISubcollection AutomobileCollection { get; }

        /// <summary>
        /// Gets the total of all Auto assets in string format.
        /// </summary>
        string AutomobileWorthTotal_rep { get; }

        /// <summary>
        /// Gets the extra checking/saving/gift funds/other liquid asset record to print on 1003 pg 4 (additional page). 
        /// </summary>
        ISubcollection ExtraNormalAssets { get; }

        /// <summary>
        /// Gets the first 4 checking/saving/gift funds/other liquid asset record to print on 1003 pg 2.
        /// </summary>
        ISubcollection First4NormalAssets { get; }

        /// <summary>
        /// Gets a value indicating whether there are more than 3 Auto assets. This is useful for 1003 printing. 5/24/2004 dd.
        /// </summary>
        bool HasExtraAutomobiles { get; }

        /// <summary>
        /// Gets a value indicating whether there are more than 3 OtherIlliquidAsset type. Used in 1003 printing.
        /// </summary>
        bool HasExtraOtherAssets { get; }

        /// <summary>
        /// Gets a value indicating whether there are more than 3 stocks/bonds in asset list.
        /// </summary>
        bool HasExtraStocks { get; }

        /// <summary>
        /// Gets a value indicating whether there are more than 4 normal/regular assets.
        /// </summary>
        bool HasMoreThan4NormalAssets { get; }

        /// <summary>
        /// Gets all of the regular assets.
        /// </summary>
        ISubcollection NormalAssetCollection { get; }

        /// <summary>
        /// Gets a collection of OtherIlliquidAsset type. Utility for 1003 printing.
        /// </summary>
        ISubcollection OtherAssetCollection { get; }

        /// <summary>
        /// Gets a collection of all the "regular" assets within the current instance. 
        /// </summary>
        IEnumerable<IAssetRegular> RegularAssets { get; }

        /// <summary>
        /// Gets a collection of assets to consider for VOA/VOD orders.
        /// </summary>
        IEnumerable<IAssetRegular> AssetsForVerification { get; }

        /// <summary>
        /// Gets the total of the <see cref="OtherAssetCollection"/> in string format.
        /// </summary>
        string OtherAssetTotal_rep { get; }

        /// <summary>
        /// Gets a view of all assets except for business, cash deposit, life insurance, and
        /// retirement sorted by order rank value.
        /// </summary>
        DataView SortedView { get; }

        /// <summary>
        /// Gets a collection of stocks and bonds assets.
        /// </summary>
        ISubcollection StockCollection { get; }

        /// <summary>
        /// Gets the total of the <see cref="StockCollection"/> in string format.
        /// </summary>
        string StockTotal_rep { get; }

        /// <summary>
        /// Adds an asset of the specified type.
        /// </summary>
        /// <param name="assetT">The type of asset.</param>
        /// <returns>The newly added asset record.</returns>
        IAsset AddRecord(E_AssetT assetT);

        /// <summary>
        /// Adds a regular asset.
        /// </summary>
        /// <returns>The newly added asset record.</returns>
        new IAssetRegular AddRegularRecord();

        /// <summary>
        /// Adds a regular asset at the specified index.
        /// </summary>
        /// <param name="pos">The index.</param>
        /// <returns>The newly added asset record.</returns>
        new IAssetRegular AddRegularRecordAt(int pos);

        /// <summary>
        /// Update asset collection from an input field model.
        /// </summary>
        /// <param name="inputModelDict">Input field model to update assets.</param> 
        void BindFromObjectModel(Dictionary<string, object> inputModelDict);

        /// <summary>
        /// Gets the business asset, potentially creating it if it doesn't already exist.
        /// </summary>
        /// <param name="forceCreate">
        /// A value indicating whether the asset should be created if it doesn't already exist.
        /// </param>
        /// <returns>The asset if it exists or was force-created. Otherwise, null.</returns>
        IAssetBusiness GetBusinessWorth(bool forceCreate);

        /// <summary>
        /// Gets the cash deposit #1 asset, potentially creating it if it doesn't already exist.
        /// </summary>
        /// <param name="forceCreate">
        /// A value indicating whether the asset should be created if it doesn't already exist.
        /// </param>
        /// <returns>The asset if it exists or was force-created. Otherwise, null.</returns>
        IAssetCashDeposit GetCashDeposit1(bool forceCreate);

        /// <summary>
        /// Gets the cash deposit #2 asset, potentially creating it if it doesn't already exist.
        /// </summary>
        /// <param name="forceCreate">
        /// A value indicating whether the asset should be created if it doesn't already exist.
        /// </param>
        /// <returns>The asset if it exists or was force-created. Otherwise, null.</returns>
        IAssetCashDeposit GetCashDeposit2(bool forceCreate);

        /// <summary>
        /// Gets life insurance asset, potentially creating it if it doesn't already exist.
        /// </summary>
        /// <param name="forceCreate">
        /// A value indicating whether the asset should be created if it doesn't already exist.
        /// </param>
        /// <returns>The asset if it exists or was force-created. Otherwise, null.</returns>
        IAssetLifeInsurance GetLifeInsurance(bool forceCreate);

        /// <summary>
        /// Gets the record with the given id.
        /// </summary>
        /// <param name="recordId">The record id.</param>
        /// <returns>The record with the given id.</returns>
        new IAssetRegular GetRegRecordOf(Guid recordId);

        /// <summary>
        /// Gets the record at the given index.
        /// </summary>
        /// <param name="pos">The index.</param>
        /// <returns>The record at the given index.</returns>
        new IAssetRegular GetRegularRecordAt(int pos);

        /// <summary>
        /// Gets the retirement asset, potentially creating it if it doesn't already exist.
        /// </summary>
        /// <param name="forceCreate">
        /// A value indicating whether the asset should be created if it doesn't already exist.
        /// </param>
        /// <returns>The asset if it exists or was force-created. Otherwise, null.</returns>
        IAssetRetirement GetRetirement(bool forceCreate);

        /// <summary>
        /// Gets the special record at the given index.
        /// </summary>
        /// <param name="i">The index.</param>
        /// <returns>The special record at the given index.</returns>
        new IAssetSpecial GetSpecialRecordAt(int i);

        /// <summary>
        /// Gets the specified subset of assets.
        /// </summary>
        /// <param name="inclusiveGroup">A value indicating whether the specified groups should be included or excluded.</param>
        /// <param name="group">The groups to include or exclude.</param>
        /// <returns>The specified subset of assets.</returns>
        ISubcollection GetSubcollection(bool inclusiveGroup, E_AssetGroupT group);

        /// <summary>
        /// Gets a dictionary that represents the asset collection for the UI.
        /// </summary>
        /// <returns>A dictionary of assets and input models.</returns>
        Dictionary<string, object> ToInputModel();
    }
}