using System;
using System.Collections;
using System.Data;

namespace DataAccess
{

	public class CSubmitMainLoanFromLOData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CSubmitMainLoanFromLOData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("sfSubmitMainLoanFromPML");
            list.Add("sLpeRateOptionIdOf1stLienIn8020");
            list.Add("sfRunLpe");
            DataAccessUtils.AddCertificateFields(list);

            list.Add("sLNm");
            list.Add("sLOrigFPcSubmitted");
            list.Add("sLpTemplateId");
            list.Add("sLpTemplateNm");
            list.Add("sLoanProductIdentifier");
            list.Add("sNoteIRSubmitted");
            list.Add("sProOFinPmtPe");
            list.Add("sProThisMPmt");
            list.Add("sQualBottomR");
            list.Add("sQualTopR");
            list.Add("sSubFinIR");
            list.Add("sSubFinMb");
            list.Add("sRAdjMarginR");
            list.Add("sLpIsArmMarginDisplayed");
            list.Add("sOriginatorCompensationPaymentSourceT");
            list.Add("sOriginatorCompensationLenderFeeOptionT");
            list.Add("sIsRenovationLoan");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CSubmitMainLoanFromLOData(Guid fileId) : base(fileId, "CSubmitMainLoanFromLOData", s_selectProvider)
		{
		}
	}

    public class CSubmitMainLoanFromLOData2 : CPageData
    {
        private static CSelectStatementProvider s_selectProvider;
	    
        static CSubmitMainLoanFromLOData2()
        {
            StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("sfSubmitMainLoanFromPML");
            list.Add("sLpeRateOptionIdOf1stLienIn8020");
            list.Add("sfRunLpe");
            DataAccessUtils.AddCertificateFields(list);

            list.Add("sLNm");
            list.Add("sLOrigFPcSubmitted");
            list.Add("sLpTemplateId");
            list.Add("sLpTemplateNm");
            list.Add("sLoanProductIdentifier");
            list.Add("sNoteIRSubmitted");
            list.Add("sProOFinPmtPe");
            list.Add("sProThisMPmt");
            list.Add("sQualBottomR");
            list.Add("sQualTopR");
            list.Add("sSubFinIR");
            list.Add("sSubFinMb");
            list.Add("sRAdjMarginR");
            list.Add("sLpIsArmMarginDisplayed");

            list.Add("sEmployeeLockDesk");
            list.Add("sEmployeeUnderwriter");
            list.Add("sNoteIRSubmitted");
            list.Add("sPmlCertXmlContent");
            list.Add("sProThisMPmt");
            list.Add("sfApplyLoanProductTemplate");
            list.Add("sfConfirm1stLienLoanTentativelyAccepted");
            list.Add("sfSpinOffLoanForSubsequent2ndLienLoan");
            list.Add("sNoteIR");
            list.Add("sLTotI");
            list.Add("sQualBottomR");
            list.Add("sLpeRateOptionIdOf1stLienIn8020");
            list.Add("sPml1stSubmitD");
            list.Add("sLinkedLoanInfo");
            list.Add("sOriginatorCompensationPaymentSourceT");
            list.Add("sOriginatorCompensationLenderFeeOptionT");
            list.Add("sPriceGroup");
            list.Add("sIsRenovationLoan");
            #endregion

            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
        protected override bool m_enforceAccessControl
        {
            get { return false; }
        }

	        
        public CSubmitMainLoanFromLOData2(Guid fileId) : base(fileId, "CSubmitMainLoanFromLOData2", s_selectProvider)
        {
        }
    }




}
