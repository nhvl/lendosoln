﻿namespace DataAccess
{
    /// <summary>
    /// This is an interface that describes the properties used by CVaPastL.
    /// It will also be implemented by a shim so that we can support both 
    /// collection types in legacy mode.
    /// </summary>
    public interface IVaPastLoan : ICollectionItemBase2
    {
        /// <summary>
        /// Gets or sets the LoanType of the previous VA loan.
        /// </summary>
        string LoanTypeDesc { get; set; }

        /// <summary>
        /// Gets or sets the Street Address of the previous VA loan.
        /// </summary>
        string StAddr { get; set; }

        /// <summary>
        /// Gets or sets the City of the previous VA loan.
        /// </summary>
        string City { get; set; }

        /// <summary>
        /// Gets or sets the State of the previous VA loan.
        /// </summary>
        string State { get; set; }

        /// <summary>
        /// Gets or sets the City and State of the previous VA loan.
        /// </summary>
        string CityState { get; set; }

        /// <summary>
        /// Gets or sets the Date, in Month and Year format, of the previous VA loan.
        /// </summary>
        string DateOfLoan { get; set; }

        /// <summary>
        /// Gets or sets the Zip code of the previous VA loan.
        /// </summary>
        string Zip { get; set; }

        /// <summary>
        /// Gets or sets the Loan date of the previous VA loan.
        /// </summary>
        CDateTime LoanD { get; set; }

        /// <summary>
        /// Gets or sets the Loan date of the previous VA loan.
        /// </summary>
        string LoanD_rep { get; set; }

        /// <summary>
        /// Gets or sets whether the previous VA loan is still owned.
        /// </summary>
        E_TriState IsStillOwned { get; set; }

        /// <summary>
        /// Gets or sets the Sold date of the previous VA loan.
        /// </summary>
        CDateTime PropSoldD { get; set; }

        /// <summary>
        /// Gets or sets the Sold date of the previous VA loan.
        /// </summary>
        string PropSoldD_rep { get; set; }

        /// <summary>
        /// Gets or sets the VA loan number of the previous VA loan.
        /// </summary>
        string VaLoanNum { get; set; }

        /// <summary>
        /// Gets the VaPastLT of the previous VA loan.
        /// </summary>
        E_VaPastLT VaPastLT { get; }
    }
}
