﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Interface to represent the data for real estate owned by a borrower applying for a loan.
    /// </summary>
    public interface IRealEstateOwned : ICollectionItemBase2
    {
        /// <summary>
        /// Gets or sets the street address of the property.
        /// </summary>
        /// <value>The street address of the property.</value>
        string Addr { get; set; }

        /// <summary>
        /// Gets or sets the city in which the property is located.
        /// </summary>
        /// <value>The city in which the property is located.</value>
        string City { get; set; }

        /// <summary>
        /// Gets or sets the gross rent earned from the property.
        /// </summary>
        /// <value>The gross rent earned from the property.</value>
        decimal GrossRentI { get; set; }

        /// <summary>
        /// Gets or sets the gross rent earned from the property in string format.
        /// </summary>
        /// <value>The gross rent earned from the property in string format.</value>
        string GrossRentI_rep { get; set; }

        /// <summary>
        /// Gets or sets the expense incurred from this property.
        /// </summary>
        /// <value>The expense incurred from this property.</value>
        decimal HExp { get; set; }

        /// <summary>
        /// Gets or sets the expense incurred from this property in string format.
        /// </summary>
        /// <value>The expense incurred from this property in string format.</value>
        string HExp_rep { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether some of the 'required' fields are not yet populated.
        /// </summary>
        /// <value>A value indicating whether some of the 'required' fields are not yet populated.</value>
        bool IsEmptyCreated { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this property should be included in the income calculation.
        /// </summary>
        /// <value>A value indicating whether this property should be included in the income calculation.</value>
        bool IsForceCalcNetRentalI { get; set; }

        /// <summary>
        /// Gets a value indicating whether the IsForceCalNetRentalI value is visible to the GUI.
        /// </summary>
        /// <value>A value indicating whether the IsForceCalNetRentalI value is visible to the GUI.</value>
        bool IsForceCalcNetRentalIVisible { get; }

        /// <summary>
        /// Gets or sets a value indicating whether this property is the primary residence of one or more borrowers.
        /// </summary>
        /// <value>A value indicating whether this property is the primary residence of one or more borrowers.</value>
        bool IsPrimaryResidence { get; set; }

        /// <summary>
        /// Gets a value indicating whether the status of the property has been updated.
        /// </summary>
        /// <value>A value indicating whether the status of the property has been updated.</value>
        bool IsStatusUpdated { get; }

        /// <summary>
        /// Gets or sets a value indicating whether the property is the subject property of the loan application.
        /// </summary>
        /// <value>A value indicating whether the property is the subject property of the loan application.</value>
        bool IsSubjectProp { get; set; }

        /// <summary>
        /// Gets the collection of liabilities that correspond to this property.
        /// </summary>
        /// <value>The collection of liabilities that correspond to this property.</value>
        List<CReFields.LiaDisplay> LinkedLia { get; }

        /// <summary>
        /// Gets or sets the mortgage amount for this property.
        /// </summary>
        /// <value>The mortgage amount for this property.</value>
        decimal MAmt { get; set; }

        /// <summary>
        /// Gets or sets the mortgage amount for this property in string format.
        /// </summary>
        /// <value>The mortgage amount for this property in string format.</value>
        string MAmt_rep { get; set; }

        /// <summary>
        /// Gets or sets the mortgage payment for this property.
        /// </summary>
        /// <value>The mortgage payment for this property.</value>
        decimal MPmt { get; set; }

        /// <summary>
        /// Gets or sets the mortgage payment for this property in string format.
        /// </summary>
        /// <value>The mortgage payment for this property in string format.</value>
        string MPmt_rep { get; set; }

        /// <summary>
        /// Gets or sets the net rental income earned from this property.
        /// </summary>
        /// <value>The net rental income earned from this property.</value>
        decimal NetRentI { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the net rental income is locked and cannot be edited.
        /// </summary>
        /// <value>A value indicating whether the net rental income is locked and cannot be edited.</value>
        bool NetRentILckd { get; set; }

        /// <summary>
        /// Gets or sets the net rental income earned from this property in string format.
        /// </summary>
        /// <value>The net rental income earned from this property in string format.</value>
        string NetRentI_rep { get; set; }

        /// <summary>
        /// Gets the net value of this property.
        /// </summary>
        /// <value>The net value of this property.</value>
        decimal NetVal { get; }

        /// <summary>
        /// Gets the net value of this property in string format.
        /// </summary>
        /// <value>The net value of this property in string format.</value>
        string NetValue_rep { get; }

        /// <summary>
        /// Gets or sets the rough percentage of units that are currently occupied.
        /// </summary>
        /// <value>The rough percentage of units that are currently occupied.</value>
        int OccR { get; set; }

        /// <summary>
        /// Gets or sets the rough percentage of units that are currently occupied in string format.
        /// </summary>
        /// <value>The rough percentage of units that are currently occupied in string format.</value>
        string OccR_rep { get; set; }

        /// <summary>
        /// Gets a value indicating whether the occupancy rate is read only or whether it can be edited.
        /// </summary>
        /// <value>A value indicating whether the occupancy rate is read only or whether it can be edited.</value>
        bool OccR_repReadOnly { get; }

        /// <summary>
        /// Gets or sets the ownership of the property.
        /// </summary>
        /// <value>The ownership of the property.</value>
        E_ReOwnerT ReOwnerT { get; set; }

        /// <summary>
        /// Gets or sets the status code for the property.
        /// </summary>
        /// <value>The status code for the property.</value>
        string Stat { get; set; }

        /// <summary>
        /// Gets or sets the state in which the property is located.
        /// </summary>
        /// <value>The state in which the property is located.</value>
        string State { get; set; }

        /// <summary>
        /// Gets or sets the status for the property, closely assocated with the status code Stat.
        /// </summary>
        /// <value>The status for the property, closely assocated with the status code Stat.</value>
        E_ReoStatusT StatT { get; set; }

        /// <summary>
        /// Gets or sets the type code for the property.
        /// </summary>
        /// <value>The type code for the property.</value>
        string Type { get; set; }

        /// <summary>
        /// Gets or sets the type for the property, closely associated with the type code Type.
        /// </summary>
        /// <value>The type for the property, closely associated with the type code Type.</value>
        E_ReoTypeT TypeT { get; set; }

        /// <summary>
        /// Gets or sets the gross value of the property.
        /// </summary>
        /// <value>The gross value of the property.</value>
        decimal Val { get; set; }

        /// <summary>
        /// Gets or sets the gross value of the property as a string.
        /// </summary>
        /// <value>The gross value of the property as a string.</value>
        string Val_rep { get; set; }

        /// <summary>
        /// Gets or sets the zipcode in which the property is located.
        /// </summary>
        /// <value>The zipcode in which the property is located.</value>
        string Zip { get; set; }
    }
}