﻿namespace DataAccess
{
    using System;

    /// <summary>
    /// Interface extracted from CPublicRecordCollection.
    /// </summary>
    public interface IPublicRecordCollection : IRecordCollection
    {
        /// <summary>
        /// Adds a new public record.
        /// </summary>
        /// <returns>The newly added record.</returns>
        new IPublicRecord AddRegularRecord();

        /// <summary>
        /// Adds a new public record at the given index.
        /// </summary>
        /// <param name="pos">The index.</param>
        /// <returns>The newly added record.</returns>
        new IPublicRecord AddRegularRecordAt(int pos);

        /// <summary>
        /// Gets the record with the given id.
        /// </summary>
        /// <param name="recordId">The record id.</param>
        /// <returns>The record with the given id.</returns>
        new IPublicRecord GetRegRecordOf(Guid recordId);

        /// <summary>
        /// Gets the record at the given index.
        /// </summary>
        /// <param name="pos">The index.</param>
        /// <returns>The record at the given index.</returns>
        new IPublicRecord GetRegularRecordAt(int pos);

        /// <summary>
        /// Retrieve all the items similar to the GetSubcollection method on other collections.
        /// </summary>
        /// <returns>A collection of all the items that can be iterated over.</returns>
        ISubcollection GetAllItems();
    }
}