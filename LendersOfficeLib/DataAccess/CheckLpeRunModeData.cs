using System;
using System.Collections;
using System.Data;

namespace DataAccess
{

	public class CCheckLpeRunModeData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CCheckLpeRunModeData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
	        list.Add("lpeRunModeT");
            list.Add("sReasonForNotAllowingResubmission");
            list.Add("sLPurposeT");
            list.Add("sIsLineOfCredit");
            list.Add("sLienPosT");
            list.Add("sLoanFileT"); // 185732 - it's loaded every time anyway but that could change, might as well keep it.
            #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CCheckLpeRunModeData(Guid fileId) : base(fileId, "CCheckLpeRunModeData", s_selectProvider)
		{
		}
	}
}
