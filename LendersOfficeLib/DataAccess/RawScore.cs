﻿// <copyright file="RawScore.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Long Nguyen
//  Date:   5/17/2016
// </summary>

namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using LendersOffice.UI;

    /// <summary>
    /// Data for an raw score in Credit Score page.
    /// </summary>
    public class RawScore
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RawScore"/> class.
        /// </summary>
        public RawScore()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RawScore"/> class from an app data.
        /// </summary>
        /// <param name="appData">App data to get raw score.</param>
        public RawScore(CAppData appData)
        {
            this.AAppId = appData.aAppId;
            this.ABNm = appData.aBNm;
            this.ABExperianScore = appData.aBExperianScore;
            this.ABTransUnionScore = appData.aBTransUnionScore;
            this.ABEquifaxScore = appData.aBEquifaxScore;
            this.ABHasSpouse = appData.aBHasSpouse;
            this.ACNm = appData.aCNm;
            this.ACExperianScore = appData.aCExperianScore;
            this.ACTransUnionScore = appData.aCTransUnionScore;
            this.ACEquifaxScore = appData.aCEquifaxScore;
        }

        /// <summary>
        /// Gets aBNm.
        /// </summary>
        /// <value>A aBNm value.</value>
        [LqbInputModelAttribute(name: "aBNm")]
        public string ABNm { get; private set; }

        /// <summary>
        /// Gets or sets aAppId.
        /// </summary>
        /// <value>A aAppId value.</value>
        [LqbInputModelAttribute(name: "aAppId")]
        public Guid AAppId { get; set; }

        /// <summary>
        /// Gets or sets aBExperianScore.
        /// </summary>
        /// <value>A aBExperianScore value.</value>
        [LqbInputModelAttribute(name: "aBExperianScore")]
        public int ABExperianScore { get; set; }

        /// <summary>
        /// Gets or sets aBTransUnionScore.
        /// </summary>
        /// <value>A aBTransUnionScore value.</value>
        [LqbInputModelAttribute(name: "aBTransUnionScore")]
        public int ABTransUnionScore { get; set; }
        
        /// <summary>
        /// Gets or sets aBEquifaxScore.
        /// </summary>
        /// <value>A aBEquifaxScore value.</value>
        [LqbInputModelAttribute(name: "aBEquifaxScore")]
        public int ABEquifaxScore { get; set; }

        /// <summary>
        /// Gets a value indicating whether aBHasSpouse.
        /// </summary>
        /// <value>A aBHasSpouse value.</value>
        [LqbInputModelAttribute(name: "aBHasSpouse")]
        public bool ABHasSpouse { get; private set; }

        /// <summary>
        /// Gets aCNm.
        /// </summary>
        /// <value>A aCNm value.</value>
        [LqbInputModelAttribute(name: "aCNm")]
        public string ACNm { get; private set; }
        
        /// <summary>
        /// Gets or sets aCExperianScore.
        /// </summary>
        /// <value>A aCExperianScore value.</value>
        [LqbInputModelAttribute(name: "aCExperianScore")]
        public int ACExperianScore { get; set; }

        /// <summary>
        /// Gets or sets aCTransUnionScore.
        /// </summary>
        /// <value>A aCTransUnionScore value.</value>
        [LqbInputModelAttribute(name: "aCTransUnionScore")]
        public int ACTransUnionScore { get; set; }

        /// <summary>
        /// Gets or sets aCEquifaxScore.
        /// </summary>
        /// <value>A aCEquifaxScore value.</value>
        [LqbInputModelAttribute(name: "aCEquifaxScore")]
        public int ACEquifaxScore { get; set; }

        /// <summary>
        /// Update the value to dataLoan when calculating.
        /// </summary>
        /// <param name="dataLoan">A dataLoan value.</param>
        public void Update(CPageData dataLoan)
        {
            var appData = dataLoan.GetAppData(this.AAppId);
            appData.aBExperianScore = this.ABExperianScore;
            appData.aBTransUnionScore = this.ABTransUnionScore;
            appData.aBEquifaxScore = this.ABEquifaxScore;
            appData.aCExperianScore = this.ACExperianScore;
            appData.aCTransUnionScore = this.ACTransUnionScore;
            appData.aCEquifaxScore = this.ACEquifaxScore;
        }
    }
}
