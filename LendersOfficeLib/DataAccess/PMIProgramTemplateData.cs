﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using LendersOffice.Common;
using System.Data.Common;
using System.Data.SqlClient;

namespace DataAccess
{
    public class PMIProgramTemplateData
    {
        #region Fields
        public Guid ProductId { get; set; }
        public Guid FolderId { get; set; }
        public bool UfmipIsRefundableOnProRataBasis { get; set; }
        public E_PmiCompanyT MICompany { get; set; }
        public string MICompany_rep
        {
            get
            {
                return Tools.PmiCompanyT_map[MICompany];
            }
        }
        public E_PmiTypeT MIType { get; set; }
        public string MIType_rep
        {
            get
            {
                return Tools.PmiTypeT_map[MIType];
            }
        }
        public bool IsEnabled { get; set; }
        public DateTime CreatedD { get; set; }
        #endregion

        public PMIProgramTemplateData() { }

        public PMIProgramTemplateData(DbDataReader dr)
        {
            ProductId = new Guid(dr["ProductId"].ToString());
            FolderId = new Guid(dr["FolderId"].ToString());
            MICompany = (E_PmiCompanyT)Convert.ToInt32(dr["MICompany"].ToString());
            MIType = (E_PmiTypeT)Convert.ToInt32(dr["MIType"].ToString());
            IsEnabled = (bool)dr["IsEnabled"];
            CreatedD = Convert.ToDateTime(dr["CreatedD"].ToString());
            UfmipIsRefundableOnProRataBasis = (bool)dr["UfmipIsRefundableOnProRataBasis"];
        }

        public static PMIProgramTemplateData Retrieve(Guid id)
        {
            if (id == Guid.Empty)
                throw new CBaseException(ErrorMessages.FailedToLoad, "Unable to load template. No id provided.");

            PMIProgramTemplateData template;
            using (DbDataReader dr = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "LoadPMIProgramTemplate", new SqlParameter("@ProductId", id)))
            {
                if (dr.Read())
                {
                    template = new PMIProgramTemplateData(dr);
                }
                else
                {
                    throw new CBaseException(ErrorMessages.FailedToLoad, "Unable to load template " + id);
                }
            }
            return template;
        }

        public void Save()
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@ProductId", ProductId));
            parameters.Add(new SqlParameter("@FolderId", FolderId));
            parameters.Add(new SqlParameter("@MICompany", MICompany));
            parameters.Add(new SqlParameter("@MIType", MIType));
            parameters.Add(new SqlParameter("@IsEnabled", IsEnabled));
            parameters.Add(new SqlParameter("@UfmipIsRefundableOnProRataBasis", UfmipIsRefundableOnProRataBasis));

            using (DbDataReader dr = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "SavePMIProgramTemplate", parameters))
            {
                if (dr.Read())
                {
                    CreatedD = Convert.ToDateTime(dr["CreatedD"].ToString());
                }
                else
                {
                    throw new CBaseException(ErrorMessages.FailedToSave, "Failed to save template: " + ProductId);
                }
            }
        }

        public static List<PMIProgramTemplateData> GetTemplatesByFolder(Guid FolderId)
        {
            List<PMIProgramTemplateData> templates = new List<PMIProgramTemplateData>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "RetrievePMIProgramTemplatesByFolder", new SqlParameter("@FolderId", FolderId)))
            {
                while (reader.Read())
                {
                    PMIProgramTemplateData template = new PMIProgramTemplateData(reader);
                    templates.Add(template);
                }
            }

            return templates;
        }

        public static void DeleteById(Guid ProductId)
        {
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LpeSrc, "DeletePMIProgramTemplatesById", 1, new SqlParameter("@ProductId", ProductId));
        }

        public static void EnableById(Guid ProductId)
        {
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LpeSrc, "EnablePMIProgramTemplatesById", 1, new SqlParameter("@ProductId", ProductId));
        }
    }
}