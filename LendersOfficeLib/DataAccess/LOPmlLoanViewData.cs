using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
	public class CLOPmlLoanViewData : CPmlPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CLOPmlLoanViewData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("sfTransformDataToPml");
            list.Add("aBNm");
            list.Add("aBSsn");
            list.Add("aProdBCitizenT");
            list.Add("aCNm");
            list.Add("aCSsn");
            list.Add("sSpState");
            list.Add("sOccT");
            list.Add("sProdSpT");
            list.Add("sUnitsNum");
            list.Add("sProdCondoStories");
            list.Add("sLPurposeT");
            list.Add("sProdCashoutAmt");
            list.Add("sFinMethT");
            list.Add("sProdDocT");
            list.Add("sProdImpound");
            list.Add("sLienPosT");
            list.Add("sProdRLckdDays");
            list.Add("sProdPpmtPenaltyMon");
            list.Add("sIsIOnly");
            list.Add("sHouseVal");
            list.Add("sDownPmtPc");
            list.Add("sEquityCalc");
            list.Add("sLtvR");
            list.Add("sLAmtCalc");
            list.Add("sCltvR");
            list.Add("sLtvROtherFin");
            list.Add("sProOFinBal");
            list.Add("sProOFinPmt");
            list.Add("sPreparerXmlContent");
            list.Add("sfGetAgentOfRole");
            list.Add("sEmployeeLenderAccExec");
            list.Add("sLpTemplateNm");
            list.Add("sLoanProductIdentifier");
            list.Add("sStatusT");

            list.Add("sNoteIR");
            list.Add("sLOrigFPc");
            list.Add("sApr");
            list.Add("sRAdjCapR");
            list.Add("sRAdjMarginR");
            list.Add("sQualTopR");
            list.Add("sQualBottomR");
            list.Add("sProThisMPmt");
            list.Add("sRLckdD");
            list.Add("sRLckdExpiredD");
            list.Add("sSubmitD");
            list.Add("sApprovD");
            list.Add("sBrokComp1Pc");
            list.Add("sLNm");
            list.Add("sProOHExp");
            list.Add("sProRealETx");
            list.Add("sCreditScoreType1");
            list.Add("sCreditScoreType2");
            list.Add("sTransmOMonPmt");
            list.Add("sProdIsCreditScoresAltered");
            list.Add("sSpAddr");
            list.Add("sSpCity");
            list.Add("sSpState");
            list.Add("sSpCounty");
            list.Add("sSpZip");
            list.Add("sPpmtPenaltyMon");
            list.Add("sRLckdDays");
            list.Add("sHouseVal");
            list.Add("sTerm");
            list.Add("sDue");
            list.Add("aIsCreditReportOnFile");
            list.Add("Is1stTimeHomeBuyer");
            list.Add("sPurchaseContractDate");
            list.Add("sEstCloseD");
            list.Add("sLpIsArmMarginDisplayed");
            list.Add("sPrimAppTotNonspI");
            list.Add("sIsSelfEmployed");
            list.Add("sLpeNotesFromBrokerToUnderwriterHistory");
            list.Add("sProdAvailReserveMonths");
            list.Add("sBrokerId");
            list.Add("sProdSpStructureT");
            list.Add("sProd3rdPartyUwResultT");
            list.Add("sProdIsSpInRuralArea");
            list.Add("sProdIsCondotel");
            list.Add("sProdIsNonwarrantableProj");
            list.Add("aIsBorrSpousePrimaryWageEarner");
            list.Add("aBHasSpouse");
			list.Add("sLpDPmtT");
			list.Add("sBranchId");
			list.Add("sProdHasHousingHistory");
			list.Add("sIsOptionArm");
			list.Add("sProdMIOptionT");
            list.Add("sProdConvMIOptionT");
			list.Add("sProdEstimatedResidualI");
			list.Add("sHas1stTimeBuyer");
			list.Add("sProd3rdPartyUwProcessingT"); // 04/26/07 mf - OPM 12103 AU Processing
            list.Add("aPresOHExp");
            list.Add("sOccR");
            list.Add("sSpGrossRent");
			list.Add("sIsStandAlone2ndLien"); // OPM 4442
			list.Add("sLpIsNegAmortOtherLien");
			list.Add("sOtherLFinMethT");
			list.Add("sProOFinPmt");
            list.Add("sProdIncludeMyCommunityProc");
            list.Add("sProdIncludeHomePossibleProc");
            list.Add("sProdIncludeNormalProc");
            list.Add("sProdIncludeFHATotalProc");
            list.Add("sProdIncludeVAProc");
            list.Add("sProdIsDuRefiPlus"); // 4/10/2009 dd - OPM 29134
            list.Add("sProdIncludeUSDARuralProc");
            list.Add("sMaxDti"); // OPM 32212
            list.Add("sDuCaseId");
            list.Add("sHcltvR");
            list.Add("sFreddieLoanId");
            list.Add("sAgencyCaseNum");
            list.Add("sApprRprtExpD");
            list.Add("sIncomeDocExpD");
            list.Add("sCrExpD");
            list.Add("sCreditScoreLpeQual");
            list.Add("sMaxR");
            list.Add("sPurchPrice");
            list.Add("sBrokerLockAdjustments");
            list.Add("sProdVaFundingFee");
            list.Add("sFfUfmip1003");
            list.Add("sFfUfMipIsBeingFinanced");
            list.Add("sOriginalAppraisedValue");
            list.Add("sHasAppraisal");
            list.Add("sIsProd3rdPartyUwResultTModifiedByUser");
            list.Add("sOriginatorCompensationPaymentSourceT");
            list.Add("sOriginatorCompForCert");
            list.Add("sOriginatorCompNetPoints");
            list.Add("sOriginatorCompensationEffectiveD");
            list.Add("sBrokerLockOriginatorPriceBrokComp1PcFee");
            list.Add("sShowAppraisedBasedLtvsOnCert");
            list.Add("sAppraisedValueCltvR_rep");
            list.Add("sAppraisedValueLtvR_rep");
            list.Add("sPriorSalesPrice");
            list.Add("sPriorSalesPropertySellerT");
            list.Add("sPriorSalesD");
            list.Add("sAssetExpD");
            list.Add("sBondDocExpD");
            list.Add("sPurchPrice");
            list.Add("sApprVal");

            list.Add("sHCLTVRPe");
            list.Add("sSubFinT");
            list.Add("sHas2ndFinPe");
            list.Add("sLpProductT");
            // start 8/20/2012 sk opm 64482 
            list.Add("sIsRenovationLoan");
            list.Add("sAsCompletedValuePeval");
            list.Add("sTotalRenovationCosts");
            list.Add("sApprValPe");
            // end 8/20/2012 sk opm 64482 
            list.Add("sIsOFinCreditLineInDrawPeriod");
            list.Add("sLinkedLoanInfo");
            list.Add("sPriceGroup");

            list.Add("sAppExpD");
            list.Add("sStatusD");
            list.Add("sFundD");
            list.Add("sApprRprtOd");
            list.Add("sApprRprtDueD");
            list.Add("sSpValuationEffectiveD");
            list.Add("sApprRprtRd");
            list.Add("sEmployeeDocDrawer");
            list.Add("sEmployeeLockDesk");
            list.Add("sEmployeeCreditAuditor");
            list.Add("sEmployeePurchaser");
            list.Add("sEmployeeFunder");
            list.Add("sTilInitialDisclosureD");
            list.Add("sInitAPR");
            list.Add("sLastDiscAPR");
            list.Add("sTilRedisclosureD");
            list.Add("sRedisclosureMethodT");
            list.Add("sTilRedisclosuresReceivedD");
            list.Add("aBFicoScore");
            list.Add("aCFicoScore");
            list.Add("sIsCurrentLockUpdated");
            list.Add("sEmployeeExternalSecondary");
            list.Add("sEmployeeExternalPostCloser");
            list.Add("sBranchChannelT");
            list.Add("sPmlBrokerId");
            list.Add("sConditionReviewD");
            list.Add("sFundingConditionsD");
            list.Add("sQMStatusT");
            list.Add("sLoanEstimateDatesInfo");
            list.Add("sClosingDisclosureDatesInfo");
            list.Add("sCustomField6D");
            list.Add("sCustomField7D");
            list.Add("sDocsBackD");
            list.Add("sDocsD");

            list.Add("sPrepmtPeriodMonths");
            list.Add("sSoftPrepmtPeriodMonths");
            list.Add("sDocsOrderedD");
            list.Add("sLoanSubmittedD");

            list.Add("GetLastDisclosedLoanEstimate");
            list.Add("aBTotI");
            list.Add("aCTotI");
            list.Add("sIsRateLocked");
            list.Add("sClearToCloseD");

            // OPM 367039
            list.Add("sPrelimRprtDocumentD");
            list.Add("sHasOriginatorCompensationPlan");

            // OPM 465171
            list.Add("sNumFinancedProperties");
            list.Add("sConcurSubFin");
            list.Add("sLenderFeeBuyoutRequestedT");

            // OPM 466973
            list.Add("sBrokerLockBrokerBaseNoteIR");
            list.Add("sBrokerLockBrokerBaseBrokComp1PcFee");
            list.Add("sBrokerLockBrokerBaseRAdjMarginR");

            list.Add("sBrokerLockOriginatorCompAdjNoteIR");
            list.Add("sBrokerLockOriginatorCompAdjBrokComp1PcFee");
            list.Add("sBrokerLockOriginatorCompAdjRAdjMarginR");

            list.Add("sBrokerLockOriginatorPriceNoteIR");
            list.Add("sBrokerLockOriginatorPriceRAdjMarginR");

            list.Add("sOriginatorCompensationLenderFeeOptionT");
            list.Add("sOriginatorCompensationAmount");
            #endregion

            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CLOPmlLoanViewData(Guid fileId) : base(fileId, "CLOPmlLoanViewData", s_selectProvider)
		{
		}
	}
}
