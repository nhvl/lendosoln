using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using LendersOffice.Common;
using LendersOffice.Migration;
using LendersOffice.UI;

namespace DataAccess
{
    public class CAssetCollection : CXmlRecordCollection, IAssetCollection
    {
        public const string SortedViewRowFilter = "AssetT NOT IN ('2', '5', '10', '6')";
        private IAssetCashDeposit m_cashDep1 = null;
		private IAssetCashDeposit m_cashDep2 = null;
		private IAssetLifeInsurance m_lifeIns = null;
		private IAssetRetirement m_vestedRetire = null;
		private IAssetBusiness m_businessWorth = null;

        public CAssetCollection(CAppBase appData, string xmlSchema, string xmlContent)
			: base( appData.LoanData , appData, xmlSchema, xmlContent )
		{
		}

        public ISubcollection GetSubcollection( bool bInclusiveGroup, E_AssetGroupT group )
		{
			return base.GetSubcollectionCore( bInclusiveGroup, group );
		}

		protected override bool IsTypeOfSpecialRecord( Enum recordT )
		{
			return CAssetFields.IsTypeOfSpecialRecord( (E_AssetT) recordT );
		}
		
		
		protected override ICollectionItemBase2 CreateRegularRawRecord()
		{
			var asset = CAssetRegular.Create( m_appData, m_ds, this );
            asset.IsEmptyCreated = true;
            return asset;
		}
		
	
	
		override protected ArrayList SpecialRecords
		{
			get
			{				
				ArrayList list = new ArrayList( 5 );
				IAsset rec = GetCashDeposit1( false );
				if( null != rec )
					list.Add( rec );
				rec = GetCashDeposit2( false );
				if( null != rec )
					list.Add( rec );
				rec = GetLifeInsurance( false );
				if( null != rec )
					list.Add( rec );
				rec = GetRetirement( false );
				if( null != rec )
					list.Add( rec );
				rec = this.GetBusinessWorth( false );
				if( null != rec )
					list.Add( rec );
				return list;
			}
		}
		
		override protected ICollectionItemBase2 Reconstruct( int iRow )
		{
			if( iRow < 0 )
				throw new CBaseException(ErrorMessages.Generic, "Invalid iRow value for xml record.");

			E_AssetT type;
			try
			{
				type = (E_AssetT) int.Parse( m_ds.Tables[0].Rows[iRow]["AssetT"].ToString() );
			}
			catch
			{
				return CAssetRegular.Recontruct( m_appData, m_ds, this, iRow );
			}
			switch( type )
			{
				case E_AssetT.CashDeposit:
					return CAssetCashDeposit.Reconstruct( m_appData, m_ds, this, iRow );
				case E_AssetT.LifeInsurance:
					return CAssetLifeIns.Reconstruct( m_appData, m_ds, this, iRow );
				case E_AssetT.Business:
					return CAssetBusiness.Reconstruct( m_appData, m_ds, this, iRow );
				case E_AssetT.Retirement:
					return CAssetRetirement.Reconstruct( m_appData, m_ds, this, iRow );
				default:
					return CAssetRegular.Recontruct( m_appData, m_ds, this, iRow );
			}
			
		}

		/// <summary>
		/// Returns nulls if it doesn't exist and bForceCreate = false
		/// </summary>
		public IAssetCashDeposit GetCashDeposit1( bool bForceCreate )
		{
			if( null == m_cashDep1 && bForceCreate )
				m_cashDep1	= CAssetCashDeposit.CreateDeposit1( m_appData, m_ds, this );
			return m_cashDep1;
		}

		/// <summary>
		/// Returns nulls if it doesn't exist and bForceCreate = false
		/// </summary>
		public IAssetCashDeposit GetCashDeposit2( bool bForceCreate )
		{
			if( null == m_cashDep2 && bForceCreate )
				m_cashDep2	= CAssetCashDeposit.CreateDeposit2( m_appData, m_ds, this );
			return m_cashDep2;
		}

		/// <summary>
		/// Returns nulls if it doesn't exist and bForceCreate = false
		/// </summary>
		public IAssetLifeInsurance GetLifeInsurance( bool bForceCreate )
		{
			if( null == m_lifeIns && bForceCreate )
			{
				m_lifeIns = CAssetLifeIns.Create( m_appData, m_ds, this );
			}
			return m_lifeIns;
		}

		/// <summary>
		/// Return nulls if it doesn't exist and bForceCreate = false
		/// </summary>
		public IAssetBusiness GetBusinessWorth( bool bForceCreate )
		{
			if( null == m_businessWorth && bForceCreate )
			{
				m_businessWorth = CAssetBusiness.Create( m_appData, m_ds, this );
			}
			return m_businessWorth;
		}

		/// <summary>
		/// Return nulls if it doesn't exist and bForceCreate = false
		/// </summary>
		public IAssetRetirement GetRetirement( bool bForceCreate )
		{
			//Clear all content except for RecordId, Type, Owner type, and order position
			if( null == m_vestedRetire && bForceCreate )
			{
				m_vestedRetire = CAssetRetirement.Create( m_appData, m_ds, this );
			}
			return m_vestedRetire;
		}

		public DataView SortedView
		{
			get
			{
				DataTable table = m_ds.Tables[0];
				DataView view = table.DefaultView;
                // Filter out business type, cash deposit, life insurance, retirement.
                view.RowFilter = SortedViewRowFilter;
				view.Sort = "OrderRankValue";
				return view;
			}
		}

		override protected E_ExistingRecordFaith DetermineRawRecordDestiny(ICollectionItemBase2 record )
		{
			var rec = (IAsset) record;
			switch( rec.AssetT )
			{
				case E_AssetT.CashDeposit:
					var depositRec = (IAssetCashDeposit) rec;
					switch( depositRec.AssetCashDepositT )
					{
						case E_AssetCashDepositT.CashDeposit1:
							if( null == m_cashDep1 )
							{
								m_cashDep1 = depositRec;
								return E_ExistingRecordFaith.E_SpecialTaken;
							}
							return E_ExistingRecordFaith.E_SpecialInvalid;
						case E_AssetCashDepositT.CashDeposit2:
							if( null == m_cashDep2 )
							{
								m_cashDep2 = depositRec;
								return E_ExistingRecordFaith.E_SpecialTaken;
							}
							return E_ExistingRecordFaith.E_SpecialInvalid;
					}
					return E_ExistingRecordFaith.E_SpecialInvalid;
				case E_AssetT.Business:
					if( null == m_businessWorth )
					{
						m_businessWorth = rec as IAssetBusiness;
						return E_ExistingRecordFaith.E_SpecialTaken;
					}
					return E_ExistingRecordFaith.E_SpecialInvalid;
				case E_AssetT.LifeInsurance:
					if( null == m_lifeIns )
					{
						m_lifeIns = rec as IAssetLifeInsurance;
						return E_ExistingRecordFaith.E_SpecialTaken;
					}
					return E_ExistingRecordFaith.E_SpecialInvalid;
				case E_AssetT.Retirement:
					if( null == m_vestedRetire )
					{
						m_vestedRetire = rec as IAssetRetirement;
						return E_ExistingRecordFaith.E_SpecialTaken;
					}
					return E_ExistingRecordFaith.E_SpecialInvalid;
				default:
					return E_ExistingRecordFaith.E_RegularOK;
			}
		}

        /// <summary>
        /// Update CAppData with the new values
        /// </summary>
        override public void Flush()
        {
            ExecuteDeathRowRecords();

            decimal AsstLiqTot = 0;
            decimal AsstNonReSolidTot = 0;

            var subcoll = this.GetSubcollection(true, E_AssetGroupT.All);
            foreach (var item in subcoll)
            {
                var rec = (IAsset)item;
                rec.PrepareToFlush();

                // remove the skip negative assets from opm 65062
                if (rec.IsLiquidAsset)
                {
                    if (rec.AssetT == E_AssetT.CashDeposit)
                    {
                        // If it's migrated, then we need to always exclude the cash deposit.
                        if (!LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.m_loanData.sLoanVersionT, LoanVersionT.V7_ExcludeCashDeposit_Opm236245) &&
                            !this.m_loanData.BrokerDB.ExcludeCashDepositFromAssetTotals)
                        {
                            AsstLiqTot += rec.Val;
                        }
                    }
                    else
                    {
                        AsstLiqTot += rec.Val;
                    }
                }
                else
                {
                    AsstNonReSolidTot += rec.Val;
                }
            }

            // The following lines will prepare the app data object ready to be saved
            // and all cached calculated values are updated to be used in ratio calculations etc...
            m_appData.aAsstLiqTot_ = AsstLiqTot;
            m_appData.aAsstNonReSolidTot_ = AsstNonReSolidTot;

            m_appData.aAssetXmlContent = m_ds.GetXml();
            base.Flush();
        }

        override protected void DetachSpecialRecords()
		{
			m_businessWorth = null;
			m_cashDep1 = null;
			m_cashDep2 = null;
			m_lifeIns = null;
			m_vestedRetire = null;
		}

		new public IAssetRegular GetRegularRecordAt( int pos )
		{
			return base.GetRegularRecordAt( pos ) as IAssetRegular;

		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="recordId"></param>
		/// <returns></returns>
		new public IAssetRegular GetRegRecordOf( Guid recordId )
		{
			return base.GetRegRecordOf( recordId ) as IAssetRegular;
		}
		/// <summary>
		/// Do not use this, use foreach on GetSubcollection() instead.
		/// </summary>
		/// <param name="i"></param>
		/// <returns></returns>
		new public IAssetSpecial GetSpecialRecordAt( int i )
		{
			return base.GetSpecialRecordAt( i ) as IAssetSpecial;
		}

		new public IAssetRegular AddRegularRecord()
		{
			var asset = base.AddRegularRecord() as IAssetRegular;

            asset.IsEmptyCreated = true;

            return asset;
		}

		/// <summary>
		/// This method can throw if cannot add
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		protected override ICollectionItemBase2 AddSpecialRecord( Enum type )
		{
			E_AssetT t = (E_AssetT) type;
			switch( t )
			{
				case E_AssetT.LifeInsurance:
					if( null == m_lifeIns )
						return GetLifeInsurance( true );
					break;
				case E_AssetT.Retirement:
					if( null == m_vestedRetire )
						return GetRetirement( true );
					break;
				case E_AssetT.Business:
					if( null == m_businessWorth )
						return GetBusinessWorth( true );
					break;
				case E_AssetT.CashDeposit:
					if( null == m_cashDep1 )
						return GetCashDeposit1( true );
					if( null == m_cashDep1 )
						return GetCashDeposit2( true );
					break;
			}
			throw new CBaseException(ErrorMessages.Generic, "Cannot add any more special asset record of the requested type = " + t.ToString());
		}
		
		/// <summary>
		/// Avoid using this method if possible, it's intended for data object use only.
		/// </summary>
		/// <returns></returns>
		public IAsset AddRecord( E_AssetT assetT )
		{		
			return (IAsset) base.AddRecord( assetT );
		}
		

		new public IAssetRegular AddRegularRecordAt( int pos )
		{			
			var asset = (IAssetRegular) base.AddRegularRecordAt( pos );
            asset.IsEmptyCreated = true;

            return asset;
		}



        public ISubcollection AutomobileCollection 
        {
            get 
            { 
                return this.GetSubcollection(true, E_AssetGroupT.Auto); 
            }
        }
        /// <summary>
        /// Return true if there are more than 3 auto record in Asset. This is useful for 1003 printing. 5/24/2004 dd
        /// </summary>
        public bool HasExtraAutomobiles 
        {
            get 
            {
                var subCollection = this.GetSubcollection(true, E_AssetGroupT.Auto);
                return subCollection.Count > 3;
            }
        }


        public string AutomobileWorthTotal_rep 
        {
            get 
            {
                decimal total = Sum(AutomobileCollection);
                return m_loanData.m_convertLos.ToMoneyString(total, FormatDirection.ToRep);
            }
        }
        /// <summary>
        /// Return a collection of OtherIlliquidAsset type. Util for 1003 printing.
        /// </summary>
        public ISubcollection OtherAssetCollection 
        {
            get 
            {
                return this.GetSubcollection(true, E_AssetGroupT.OtherIlliquidAsset); 
            }
        }

        /// <summary>
        /// Return true if there are more than 3 OtherIlliquidAsset type. Use in 1003 printing.
        /// </summary>
        public bool HasExtraOtherAssets 
        {
            get 
            {
                return OtherAssetCollection.Count > 3;
            }
        }


        public string OtherAssetTotal_rep 
        {
            get 
            {
                decimal total = Sum(OtherAssetCollection);
                return m_loanData.m_convertLos.ToMoneyString(total, FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// Return list of stocks and bonds
        /// </summary>
        public ISubcollection StockCollection 
        {
            get 
            {
                return this.GetSubcollection(true, E_AssetGroupT.Stocks | E_AssetGroupT.Bonds);
            }
        }
        /// <summary>
        /// Return true if there are more than 3 stocks/bonds in asset list.
        /// </summary>
        public bool HasExtraStocks 
        {
            get 
            {
                return StockCollection.Count > 3;
            }
        }


        /// <summary>
        /// Return total value of all stocks/bonds.
        /// </summary>
        public string StockTotal_rep 
        {
            get 
            {
                decimal total = Sum(StockCollection);
                return m_loanData.m_convertLos.ToMoneyString(total, FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// Gets a collection of all the "regular" assets within the current instance. 
        /// </summary>
        public IEnumerable<IAssetRegular> RegularAssets
        {
            get
            {
                int regularCount = this.CountRegular;
                for (int i = 0; i < regularCount; ++i)
                {
                    yield return this.GetRegularRecordAt(i);
                }
            }
        }

        /// <summary>
        /// Gets a collection of assets to consider for VOA/VOD orders.
        /// </summary>
        public IEnumerable<IAssetRegular> AssetsForVerification => this.RegularAssets.Where(a => a.IsLiquidAsset);

        /// <summary>
        /// Return list of Checking/Saving/Gift Funds/Other Liquid Assets type.
        /// </summary>
        public ISubcollection NormalAssetCollection 
        {
            get 
            {
                return GetSubcollection(true, 
                    E_AssetGroupT.Checking | 
                    E_AssetGroupT.Savings | 
                    E_AssetGroupT.GiftFunds | 
                    E_AssetGroupT.OtherLiquidAsset | 
                    E_AssetGroupT.GiftEquity | 
                    E_AssetGroupT.BridgeLoanNotDposited | 
                    E_AssetGroupT.CertificateOfDeposit | 
                    E_AssetGroupT.TrustFunds | 
                    E_AssetGroupT.SecuredBorrowedFundsNotDeposit | 
                    E_AssetGroupT.MoneyMarketFund |
                    //OPM 60497: Pending net sale proceeds count as a normal asset
                    //(and should be included in the bank account section of the 1003)
                    E_AssetGroupT.PendingNetSaleProceedsFromRealEstateAssets | 
                    E_AssetGroupT.MutualFunds);
            }
        }

        public bool HasMoreThan4NormalAssets 
        {
            get 
            {
                return NormalAssetCollection.Count > 4; 
            }
        }

        /// <summary>
        /// Get first 4 checking/saving/gift funds/other liquid asset record to print on 1003 pg 2.
        /// </summary>
        public ISubcollection First4NormalAssets 
        {
            get 
            {
                var mainList = NormalAssetCollection;
                if (mainList.Count > 4) 
                {
                    // 5/24/2004 dd - Create an array of record with maximum 4 entries.
                    var list = new ICollectionItemBase[4];
                    for (int i = 0; i < 4; i++) 
                    {
                        list[i] = mainList.GetRecordAt(i);
                    }
                    return CXmlRecordSubcollection.Create(list);

                } 
                else 
                {
                    // 5/24/2004 dd - Less than 4 check/saving account, just return the list.
                    return mainList;
                }
            }
        }


        /// <summary>
        /// Return extra checking/saving/gift funds/other liquid asset record to print on 1003 pg 4 (additional page). 
        /// </summary>
        public ISubcollection ExtraNormalAssets 
        {
            get 
            {
                var mainList = NormalAssetCollection;
                if (mainList.Count > 4) 
                {
                    var list = new ICollectionItemBase[mainList.Count - 4];
                    for (int i = 0; i < mainList.Count - 4; i++) 
                    {
                        list[i] = mainList.GetRecordAt(i + 4);
                    }
                    return CXmlRecordSubcollection.Create(list);
                } 
                else 
                {
                    return CXmlRecordSubcollection.Create(new ICollectionItemBase[0]); 
                }
            }
        }
        private decimal Sum(ISubcollection collection) 
        {
            decimal total = 0.0M;
            int count = collection.Count;
            for (int i = 0; i < count; i++) 
            {
                try 
                {
                    var field = (IAsset) collection.GetRecordAt(i);
                    total += field.Val;
                } 
                catch {}
            }
            return total;
        }

        #region JsonHandler

        /// <summary>
        /// Return then json of assets of an applicant.
        /// </summary>
        /// <returns>An dictionary of assets of an applicant.</returns>
        public Dictionary<string, object> ToInputModel()
        {
            Dictionary<string, object> inputModelDict = new Dictionary<string, object>();

            inputModelDict.Add("cashDeposit1", this.GetCashDeposit1(true).ToInputFieldModel());
            inputModelDict.Add("cashDeposit2", this.GetCashDeposit2(true).ToInputFieldModel());
            inputModelDict.Add("businessWorth", this.GetBusinessWorth(true).ToInputFieldModel());
            inputModelDict.Add("lifeInsurance", this.GetLifeInsurance(true).ToInputFieldModel());
            inputModelDict.Add("retirement", this.GetRetirement(true).ToInputFieldModel());

            var regularAssets = m_sortedList.GetValueList().Cast<IAssetRegular>().Where(x => !x.IsOnDeathRow).Select(x =>x.ToInputFieldModel());

            inputModelDict.Add("regulars", regularAssets);

            var regAssetTemplate = ((IAssetRegular)this.CreateRegularRawRecord()).ToInputFieldModel();
            regAssetTemplate.Remove("id"); // Remove id to avoid conflict when creating 2 or 3 asset from template.

            inputModelDict.Add("regularTemplate", regAssetTemplate);
            
            return inputModelDict;
        }

        /// <summary>
        /// Update asset collection from an input field model.
        /// </summary>
        /// <param name="inputModelDict">Input field model to update assets.</param> 
        public void BindFromObjectModel(Dictionary<string, object> inputModelDict)
        {
            if (inputModelDict.ContainsKey("cashDeposit1"))
            {
                this.GetCashDeposit1(true).BindFromObjectModel(SerializationHelper.JsonNetDeserialize<SimpleObjectInputFieldModel>(inputModelDict[@"cashDeposit1"].ToString()));
            }

            if (inputModelDict.ContainsKey("cashDeposit2"))
            {
                this.GetCashDeposit2(true).BindFromObjectModel(SerializationHelper.JsonNetDeserialize<SimpleObjectInputFieldModel>(inputModelDict[@"cashDeposit2"].ToString()));
            }

            if (inputModelDict.ContainsKey("businessWorth"))
            {
                this.GetBusinessWorth(true).BindFromObjectModel(SerializationHelper.JsonNetDeserialize<SimpleObjectInputFieldModel>(inputModelDict[@"businessWorth"].ToString()));
            }

            if (inputModelDict.ContainsKey("lifeInsurance"))
            {
                this.GetLifeInsurance(true).BindFromObjectModel(SerializationHelper.JsonNetDeserialize<SimpleObjectInputFieldModel>(inputModelDict[@"lifeInsurance"].ToString()));
            }

            if (inputModelDict.ContainsKey("retirement"))
            {
                this.GetRetirement(true).BindFromObjectModel(SerializationHelper.JsonNetDeserialize<SimpleObjectInputFieldModel>(inputModelDict[@"retirement"].ToString()));
            }

            if (inputModelDict.ContainsKey("regulars"))
            {
                var regularInputList = SerializationHelper.JsonNetDeserialize<List<SimpleObjectInputFieldModel>>(inputModelDict[@"regulars"].ToString());
                if (regularInputList != null)
                {
                    var assets = m_sortedList.GetValueList().Cast<IAssetRegular>().ToList();
                    assets.ForEach(x => x.IsOnDeathRow = true);
                    int currentPosition = 0;

                    foreach (SimpleObjectInputFieldModel regularInput in regularInputList)
                    {
                        IAssetRegular regAsset = null;
                        Guid assetGuid = Guid.Empty;
                        if (regularInput.ContainsKey("id") && regularInput["id"] != null)
                        {
                            assetGuid = new Guid(regularInput["id"].Value);
                            regAsset = assets.Find(x => x.RecordId == assetGuid);
                        }

                        if (regAsset == null)
                        {
                            regAsset = this.AddRegularRecordAt(currentPosition);
                            assets = m_sortedList.GetValueList().Cast<IAssetRegular>().ToList();
                            if (assetGuid != Guid.Empty)
                            {
                                regAsset.ChangeRecordId(assetGuid);
                            }
                        }
                        else
                        {
                            regAsset.IsOnDeathRow = false;
                            if (assets[currentPosition] != regAsset)
                            {
                                this.MoveRegularRecordTo(currentPosition, regAsset);
                                assets = m_sortedList.GetValueList().Cast<IAssetRegular>().ToList();
                            }
                        }

                        currentPosition++;
                        regAsset.BindFromObjectModel(regularInput);
                    }

                    this.Flush();
                }
            }
        }

        #endregion

        /// <summary>
        /// Calculate zipcode in regular field of Asset Collection.
        /// </summary>
        /// <param name="assetCollection"></param>
        public static void CalculateZipCode(Dictionary<string, object> assetCollection)
        {
            var regulars = SerializationHelper.JsonNetDeserialize<List<SimpleObjectInputFieldModel>>(assetCollection["regulars"].ToString());
            foreach (var r in regulars)
            {
                r.CalculateZipCode();
            }
            assetCollection["regulars"] = regulars;
        }
    }
}