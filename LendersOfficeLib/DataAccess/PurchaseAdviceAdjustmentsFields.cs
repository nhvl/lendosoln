﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Globalization;

namespace DataAccess
{
    [XmlType]
    public sealed class CPurchaseAdviceAdjustmentsFields
    {
        [XmlElement]
        public string AdjDesc { get; set; }
        [XmlElement]
        public string AdjPc_rep { get; set; }
        [XmlElement]
        public string AdjAmt_rep { get; set; }
        [XmlElement]
        public bool IsSRP { get; set; }
        [XmlElement]
        public int LastColumnUpdated { get; set; }
        [XmlElement]
        public int RowNum { get; set; }

        public decimal AdjPc
        {
            get
            {
                if (string.IsNullOrEmpty(AdjPc_rep))
                {
                    return 0;
                }
                else
                {
                    return ToRate(AdjPc_rep);
                }
            }
        }

        public decimal AdjAmt
        {
            get
            {
                if (string.IsNullOrEmpty(AdjAmt_rep))
                {
                    return 0;
                }
                else
                {
                    try
                    {
                        return ToMoney(AdjAmt_rep);
                    }
                    catch (FormatException)
                    {
                        CBaseException exc = new CBaseException("Adjustment amount is not a valid decimal", "Adjustment amount is not a valid decimal");
                        exc.IsEmailDeveloper = false;
                        throw exc;
                    }
                }
            }
        }

        // Taken from LosConvert - TODO find a way to not have to copy this
        private decimal ToRate(string str)
        {
            if (string.IsNullOrEmpty(str))
                return 0.0M;

            decimal ret;

            if (!decimal.TryParse(SanitizedDecimalString(str), out ret))
            {
                ret = 0.0M;
            }
            return ret;

        }

        /// <summary>
        /// This function will remove any character that are not 0-9, '.', or ','
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private string SanitizedDecimalString(string str)
        {
            if (string.IsNullOrEmpty(str))
                return string.Empty;

            char[] buffer = new char[str.Length];
            int index = 0;
            foreach (char ch in str)
            {
                if (char.IsDigit(ch) || ch == '.' || ch == ',' || ch == '(' || ch == ')' || ch == '-')
                {
                    buffer[index++] = ch;
                }
            }
            return new string(buffer, 0, index);
        }

        // Taken from LosConvert - TODO find a way to not have to copy this
        private decimal ToMoney(string str)
        {
            if (null == str || str.Length == 0)
                return 0;
            return Decimal.Parse(str, NumberStyles.Currency);
        }
    }
}
