﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Common;
using System.Xml;
using LendersOffice.Security;

namespace DataAccess
{
    public class BrokerRateLockPriceTable
    {
        public RateRow BasePrice { get; private set; }
        public RateRow TotalHiddenAdjustments { get; private set; }
        public RateRow BrokerBasePrice { get; private set; }
        public RateRow TotalVisibleAdjustments { get; private set; }
        public RateRow BrokerFinalPrice { get; private set; }
        public RateRow OriginatorCompAdjustments { get; private set; }
        public RateRow OriginatorPrice { get; private set; }
        public LosConvert m_losConvert;

        public BrokerRateLockPriceTable(
                        List<PricingAdjustment> adjustments
            , E_sBrokerLockPriceRowLockedT lockType
            , decimal rate
            , decimal fee
            , decimal margin
            , decimal teaserRate
            , LosConvert losConvert
            , decimal originatorComp
            )
        {
            m_losConvert = losConvert;
            BuildTable(adjustments, lockType, rate, fee, margin, teaserRate, originatorComp);
        }

        public BrokerRateLockPriceTable(
            List<PricingAdjustment> adjustments
            , E_sBrokerLockPriceRowLockedT lockType
            , decimal rate
            , decimal fee
            , decimal margin
            , decimal teaserRate
            , LosConvert losConvert
            //, decimal originatorComp
            )
        {
            m_losConvert = losConvert;
            BuildTable(adjustments, lockType, rate, fee, margin, teaserRate, 0);
        }

        private void BuildTable(
            List<PricingAdjustment> adjustments
            , E_sBrokerLockPriceRowLockedT lockType
            , decimal rate
            , decimal fee
            , decimal margin
            , decimal teaserRate
            , decimal originatorComp            
            )
        {
            // Set up adjustment counts
            TotalHiddenAdjustments = new RateRow();
            TotalVisibleAdjustments = new RateRow();

            foreach (PricingAdjustment adj in adjustments)
            {
                if (adj.IsHidden)
                {
                    SumUp(TotalHiddenAdjustments, adj);
                }
                else
                {
                    SumUp(TotalVisibleAdjustments, adj);
                }
            }

            // Set up originator comp
            OriginatorCompAdjustments = new RateRow() { Rate = 0, Margin = 0, Fee = decimal.Round(originatorComp,3), TeaserRate = 0 };

            // Do-Not Add                               Add
            // ----------                               ----------
            // Base               Base                  Base                Base             
            // Tot. Hidden        Total Hidden Adj.     Tot. Hidden         Total Hidden Adj.
            // Broker Base        Originator Base       Broker Base         Originator Base  
            // Tot. Visible       Total Visible Adj.    Tot. Visible        Total Visible Adj
            // Broker Final       *Orignator Price      Broker Final        *Final Price 
            // OComp Adjustments  OComp Adj.                  
            // Originator Price   Final Price                  

            if (lockType == E_sBrokerLockPriceRowLockedT.BrokerFinal)
            {
                // We were passed final broker price--back calculate the rest
                BrokerFinalPrice = new RateRow() { Rate = rate, Margin = margin, Fee = fee, TeaserRate = teaserRate };
                BrokerBasePrice = BrokerFinalPrice - TotalVisibleAdjustments;
                BasePrice = BrokerBasePrice - TotalHiddenAdjustments;
                OriginatorPrice = BrokerFinalPrice + OriginatorCompAdjustments;
            }
            else if (lockType == E_sBrokerLockPriceRowLockedT.OriginatorPrice)
            {
                // We were passed the originator price--back calculate the rest
                OriginatorPrice = new RateRow() { Rate = rate, Margin = margin, Fee = fee, TeaserRate = teaserRate };
                BrokerFinalPrice = OriginatorPrice - OriginatorCompAdjustments;
                BrokerBasePrice = BrokerFinalPrice - TotalVisibleAdjustments;
                BasePrice = BrokerBasePrice - TotalHiddenAdjustments;
            }
            else if (lockType == E_sBrokerLockPriceRowLockedT.BrokerBase)
            {
                // We were given the broker base price--back calculate the rest
                BrokerBasePrice = new RateRow() { Rate = rate, Margin = margin, Fee = fee, TeaserRate = teaserRate };
                BrokerFinalPrice = BrokerBasePrice + TotalVisibleAdjustments;
                OriginatorPrice = BrokerFinalPrice + OriginatorCompAdjustments;
                BasePrice = BrokerBasePrice - TotalHiddenAdjustments;

            }
            else if (lockType == E_sBrokerLockPriceRowLockedT.Base)
            {
                // We were given base price--back calculate the rest
                BasePrice = new RateRow() { Rate = rate, Margin = margin, Fee = fee, TeaserRate = teaserRate };
                BrokerBasePrice = BasePrice + TotalHiddenAdjustments;
                BrokerFinalPrice = BrokerBasePrice + TotalVisibleAdjustments;
                OriginatorPrice = BrokerFinalPrice + OriginatorCompAdjustments;
            }
            else
            {
                throw new UnhandledEnumException(lockType);
            }
        }

        private void SumUp(RateRow rate, PricingAdjustment adjustment)
        {
            rate.Rate += m_losConvert.ToRate(adjustment.Rate);
            rate.Fee += m_losConvert.ToRate(adjustment.Fee);
            rate.Margin += m_losConvert.ToRate(adjustment.Margin);
            rate.TeaserRate += m_losConvert.ToRate(adjustment.TeaserRate);
        }

    }

    public class InvestorRateLockPriceTable
    {
        public RateRow BasePrice { get; private set; }
        public RateRow TotalAdjustments { get; private set; }
        public RateRow TotalNonSRPAdjustments { get; private set; }
        public RateRow AdjustedPrice { get; private set; }
        public LosConvert m_losConvert;

        public InvestorRateLockPriceTable(
            List<PricingAdjustment> adjustments
            , E_sInvestorLockPriceRowLockedT lockType
            , decimal rate
            , decimal fee
            , decimal margin
            , decimal teaserRate
            , LosConvert losConvert
            )
        {
            m_losConvert = losConvert;

            // Set up adjustment counts
            TotalAdjustments = new RateRow();
            TotalNonSRPAdjustments = new RateRow();

            foreach (PricingAdjustment adj in adjustments)
            {
                SumUp(TotalAdjustments, adj);
                if (!adj.IsSRPAdjustment)
                    SumUp(TotalNonSRPAdjustments, adj);
            }

            if (lockType == E_sInvestorLockPriceRowLockedT.Adjusted)
            {
                AdjustedPrice = new RateRow() { Rate = rate, Margin = margin, Fee = fee, TeaserRate = teaserRate };
                BasePrice = AdjustedPrice - TotalAdjustments;
            }
            else if (lockType == E_sInvestorLockPriceRowLockedT.Base)
            {
                BasePrice = new RateRow() { Rate = rate, Margin = margin, Fee = fee, TeaserRate = teaserRate };
                AdjustedPrice = BasePrice + TotalAdjustments;
            }
            else
            {
                throw new UnhandledEnumException(lockType);
            }
        }

        private void SumUp(RateRow rate, PricingAdjustment adjustment)
        {
            rate.Rate += m_losConvert.ToRate(adjustment.Rate);
            rate.Fee += m_losConvert.ToRate(adjustment.Fee);
            rate.Margin += m_losConvert.ToRate(adjustment.Margin);
            rate.TeaserRate += m_losConvert.ToRate(adjustment.TeaserRate);
        }

    }

    //POD
    public class RateRow
    {
        public decimal Rate = 0;
        public decimal Fee = 0;
        public decimal Margin = 0;
        public decimal TeaserRate = 0;

        public string Price
        {
            get { return string.Format("{0:N3}%", 100 - Fee); }
        }

        public string AdjPrice
        {
            get { return string.Format("{0:N3}%", Fee * -1); }
        }

        public static RateRow operator +(RateRow firstRow, RateRow secondRow)
        {
            RateRow ret = new RateRow();
            ret.Rate = firstRow.Rate + secondRow.Rate;
            ret.Fee = firstRow.Fee + secondRow.Fee;
            ret.Margin = firstRow.Margin + secondRow.Margin;
            ret.TeaserRate = firstRow.TeaserRate + secondRow.TeaserRate;
            return ret;
        }
        public static RateRow operator -(RateRow firstRow, RateRow secondRow)
        {
            RateRow ret = new RateRow();
            ret.Rate = firstRow.Rate - secondRow.Rate;
            ret.Fee = firstRow.Fee - secondRow.Fee;
            ret.Margin = firstRow.Margin - secondRow.Margin;
            ret.TeaserRate = firstRow.TeaserRate - secondRow.TeaserRate;
            return ret;
        }
    }
}