using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Data.Common;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using LendersOffice.Common;
using LendersOffice.UI;
using System.Collections.Generic;

namespace DataAccess
{
	public class CReFields : CXmlRecordBase2, IRealEstateOwned
    {
        private CAppBase m_parent;
        internal static IRealEstateOwned Create(CAppBase parent, DataSet ds, IReCollection reColl)
		{
			return new CReFields( parent, ds, reColl );
		}

        internal static IRealEstateOwned Reconstruct(CAppBase parent, DataSet ds, IRecordCollection recordList, int iRow)
		{
			return new CReFields( parent, ds, recordList, iRow );
		}

        internal static IRealEstateOwned Reconstruct(CAppBase parent, DataSet ds, IRecordCollection recordList, Guid id)
		{
			return new CReFields( parent, ds, recordList, id );
		}
        public override void PrepareToFlush()
        {
            if (this.IsEmptyCreated && (string.IsNullOrEmpty(this.Addr) == false || string.IsNullOrEmpty(this.City) == false ||
                string.IsNullOrEmpty(this.State) == false || string.IsNullOrEmpty(this.Zip) == false ||
                this.Val > 0 || string.IsNullOrEmpty(this.Type) == false))
            {
                this.IsEmptyCreated = false;
            }
            
        }

        // DO NOT USE!  CREATED JUST TO ENABLE SOME QUICK UNIT TESTING OF THE SHIM
        public CReFields(DataSet data, LosConvert converter, Guid recordId)
            : base(data, converter, recordId)
        {

        }

        /// <summary>
        /// Create new record
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="ds"></param>
        private CReFields(CAppBase parent, DataSet ds, IReCollection reColl)
			: base( parent, ds, reColl, "ReXmlContent" )
		{
			OccR = 75; 
			ReOwnerT = E_ReOwnerT.Borrower;
            m_parent = parent;
		}

		/// <summary>
		/// Reconstruct
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="ds"></param>
		/// <param name="recordList"></param>
		/// <param name="iRow"></param>
        private CReFields(CAppBase parent, DataSet ds, IRecordCollection recordList, int iRow)
			: base( parent, ds, recordList, iRow, "ReXmlContent" )
		{
            m_parent = parent;
		}

		/// <summary>
		/// Reconstruct
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="ds"></param>
		/// <param name="recordList"></param>
		/// <param name="id"></param>
        private CReFields(CAppBase parent, DataSet ds, IRecordCollection recordList, Guid id) 
            : base(parent, ds, recordList, id, "ReXmlContent") 
        {
            m_parent = parent;
        }

        [System.Xml.Serialization.XmlIgnore]
        public bool IsForceCalcNetRentalIVisible
        {
            get { return LendersOffice.CalculatedFields.RealProperty.IsForceCalcNetRentalIVisible(this.Stat); }
        }

        /// <summary>
        /// This is visible to user and considered in calculation when it's a residence (blank) or Pending Sale.
        /// </summary>
        public bool IsForceCalcNetRentalI
        {
            get
            {
                if (IsSubjectProp)
                {
                    return false;
                }

                return GetBool( "IsForceCalcNetRentalI" );
            }
            set { SetBool( "IsForceCalcNetRentalI", value ); }
        }
        
		public string Addr
		{
            get { return GetDescString("Addr"); }
            set { SetDescString("Addr", value); }
		}

		public string City
		{
			get { return GetDescString("City"); }
			set { SetDescString("City", value); }
		}

		public string State
		{
			get { return GetState("State"); }
			set { SetState("State", value); }
		}

		public string Zip
		{
			get { return GetZipCode("Zip"); }
			set { SetZipCode("Zip", value); }
		}

		/// <summary>
		/// This would be what subcollection of records will be based on.
		/// </summary>
        [LqbInputModelAttribute(invalid: true)]
        override public Enum KeyType
		{
			get { return StatT; }
			set { StatT = (E_ReoStatusT) value; }
		}

        /// <summary>
        /// Gets a value indicating whether the the status has been changed.
        /// NOTE: THIS DOES NOT GET RESET ON FLUSH!!
        /// </summary>
        /// <remarks>
        /// This is currently a dumb check and will be flagged if there has
        /// has been a state change over the object's lifetime. For example,
        /// a change from status A => B => A would still have this flag set.
        /// </remarks>
        public bool IsStatusUpdated { get; private set; }

		public E_ReoStatusT StatT
		{
			get
			{
                return LendersOffice.CalculatedFields.RealProperty.StatusFromCode(this.Stat);
			}
            set
            {
                this.Stat = LendersOffice.CalculatedFields.RealProperty.StatusCode(value);
            }
        }
        [LqbInputModelAttribute(invalid: true)]
        public string Stat
		{
			get
			{
				string s = GetDescString("Stat");
				switch( s.ToUpper() )
				{
					case "S":
					case "PS":
					case "R":
						break;
					default:
						s = "";
						break;
				}
				return s;
			}
            set
            {
                string s;
                string val = value.TrimWhitespaceAndBOM();
                switch (val.ToUpper())
                {
                    case "S":
                    case "PS":
                    case "R":
                        s = value;
                        break;
                    default:
                        s = "";
                        break;
                }

                this.IsStatusUpdated = this.IsStatusUpdated || s != this.Stat;

                SetDescString("Stat", s);
            }
        }
        [LqbInputModelAttribute(invalid: true)]
		public string Type
		{
			get { return GetDescString("Type"); }
			set { SetDescString("Type", value);	}
		}

        public E_ReoTypeT TypeT
        {
            get
            {
                return LendersOffice.CalculatedFields.RealProperty.TypeFromCode(this.Type);
            }
            set
            {
                this.Type = LendersOffice.CalculatedFields.RealProperty.TypeCode(value);
            }
        }
		public bool IsSubjectProp
		{
			get{ return GetBool( "IsSubjectProp" ); }
			set{ SetBool( "IsSubjectProp", value ); }
        }
        public bool IsPrimaryResidence
        {
            get { return GetBool("IsPrimaryResidence"); }
            set { SetBool("IsPrimaryResidence", value); }
        }

        [LqbInputModelAttribute(invalid: true)]
        public int OccR
		{
			get
			{
				if( E_ReoStatusT.Rental == StatT )
					return GetCount( "OccR" );
				return 0;
			}
			set
			{
				SetCount( "OccR", value );
			}
		}

        [System.Xml.Serialization.XmlIgnore]
        public bool OccR_repReadOnly
        {
            get { return LendersOffice.CalculatedFields.RealProperty.OccR_repReadOnly(this.StatT); }
        }

        [LqbInputModelAttribute(type:InputFieldType.Number)]
		public string OccR_rep
		{
			get { return m_convertLos.ToCountString( OccR ); }
			set { OccR = m_convertLos.ToCount( value ); }
		}
		public decimal Val
		{
			get { return GetMoney("Val"); }
			set { SetMoney( "Val", value ); }
		}
        [LqbInputModelAttribute(invalid: true)]
        public string Val_rep
		{
			get { return m_convertLos.ToMoneyString(Val, FormatDirection.ToRep); }
			set { Val = m_convertLos.ToMoney(value); }
		}

		public decimal MAmt
		{
			get { return GetMoney( "MAmt" ); }
			set { SetMoney( "MAmt", value ); }
		}
        [LqbInputModelAttribute(invalid: true)]
        public string MAmt_rep
		{
			get { return m_convertLos.ToMoneyString(MAmt, FormatDirection.ToRep ); }
			set { MAmt = m_convertLos.ToMoney(value ); }
		}

        /// <summary>Gets the difference of market value and mortgage balance for a real estate owned property.</summary>
        /// <value>Net market value of the real estate owned property in decimal form.</value>
        public decimal NetVal
        {
            get
            {
                var marketValue = LqbGrammar.DataTypes.Money.Create(Val).Value;
                var mortgageAmount = LqbGrammar.DataTypes.Money.Create(MAmt).Value;
                return Convert.ToDecimal(LendersOffice.CalculatedFields.RealProperty.NetValue(marketValue, mortgageAmount).Value.Value);
            }
        }

        /// <summary>Gets the difference of market value and mortgage balance for a real estate owned property, in string representation.</summary>
        /// <value>Net market value of the real estate owned property in string representation.</value>
        [LqbInputModelAttribute(invalid: true)]
        public string NetValue_rep
        {
            get { return m_convertLos.ToMoneyString(NetVal, FormatDirection.ToRep); }
        }

		public decimal GrossRentI
		{
			get
			{
				if( E_ReoStatusT.Rental == StatT )
					return GetMoney( "GrossRentI" ); 
				return 0;
			}
			set { SetMoney( "GrossRentI", value ); }
		}
        [LqbInputModelAttribute(invalid: true)]
		public string GrossRentI_rep
		{
			get { return m_convertLos.ToMoneyString(GrossRentI, FormatDirection.ToRep ); }
			set { GrossRentI = m_convertLos.ToMoney(value ); }
		}

		public decimal MPmt
		{
			get { return GetMoney( "MPmt" ); }
			set { SetMoney( "MPmt", value ); }
		}
        [LqbInputModelAttribute(invalid: true)]
        public string MPmt_rep
		{
			get { return m_convertLos.ToMoneyString(MPmt, FormatDirection.ToRep ); }
			set { MPmt = m_convertLos.ToMoney(value ); }
		}

		public decimal HExp
		{
			get { return GetMoney( "HExp" ); }
			set { SetMoney( "HExp", value ); }
		}
        [LqbInputModelAttribute(invalid: true)]
        public string HExp_rep
		{
			get { return m_convertLos.ToMoneyString(HExp, FormatDirection.ToRep ); }
			set { HExp = m_convertLos.ToMoney(value); }
		}
        public bool NetRentILckd
        {
            // 4/12/2011 dd - Since we add this attribute later per case 25661 we going to default empty value as false.
            get { return GetBool("NetRentILckd", false); }
            set { SetBool("NetRentILckd", value); }

        }
		public decimal NetRentI
		{
			get 
            {
                if (NetRentILckd)
                {
                    return GetMoney("NetRentI");
                }
                else
                {
                    bool bCalc;
                    switch (Stat)
                    {
                        case "R": bCalc = true; break;
                        case "":
                        case "PS": bCalc = IsForceCalcNetRentalI; break;
                        default: bCalc = false; break;
                    }
                    if (bCalc)
                        return ((decimal)OccR * GrossRentI) / 100 - HExp - MPmt;
                    else
                        return 0;
                }
            }
            set { SetMoney("NetRentI", value); }
		}
        [LqbInputModelAttribute(invalid: true)]
		public string NetRentI_rep
		{
			get { return m_convertLos.ToMoneyString(NetRentI, FormatDirection.ToRep ); }
            set { NetRentI = m_convertLos.ToMoney(value); }
		}

		public E_ReOwnerT ReOwnerT
		{
            get { return (E_ReOwnerT) GetEnum("ReOwnerT", E_ReOwnerT.Borrower); }
            set { SetEnum("ReOwnerT", value); }
		}
        public int H4HNumOfJointOwners
        {
            get
            {
                int value = GetCount("H4HNumOfJointOwners");
                if (value <= 1)
                {
                    if (ReOwnerT == E_ReOwnerT.Joint)
                    {
                        return 2; // 12/8/2009 dd - Default to 2 when it is a joint acocunt.
                    }
                    else
                    {
                        return 1;
                    }
                }
                return value;
            }
            set { SetCount("H4HNumOfJointOwners", value); }
        }
        [LqbInputModelAttribute(invalid: true)]
        public string H4HNumOfJointOwners_rep
        {
            get { return m_convertLos.ToCountString(H4HNumOfJointOwners); }
            set { H4HNumOfJointOwners = m_convertLos.ToCount(value); }
        }
        public bool IsEmptyCreated
        {
            get { return GetBool("IsEmptyCreated"); }
            set { SetBool("IsEmptyCreated", value); }
        }

        [System.Xml.Serialization.XmlIgnore]
        public List<LiaDisplay> LinkedLia
        {
            get { return LiaDisplay.GetSubcollection(m_parent, RecordId); }
        }
        public class LiaDisplay
        {
            public string LiabilityRecordId
            {
                get { return liabilityRecordID; }
            }
            public string ComNm
            {
                get { return cNameHidden; }
            }
            public decimal Bal
            {
                get { return cBalHidden; }
            }
            public decimal Pmt
            {
                get { return cPmtHidden; }
            }
            private string liabilityRecordID;
            private string cNameHidden;
            private decimal cBalHidden;
            private decimal cPmtHidden;
            public static List<LiaDisplay> GetSubcollection(CAppBase dataApp, Guid ReRecordId)
            {
                ILiaCollection coll = dataApp.aLiaCollection;
                var subcoll = coll.GetSubcollection(true, E_DebtGroupT.Mortgage);
                var subList = new List<LiaDisplay>();
                var count = 0;
                foreach (ILiabilityRegular liaField in subcoll)
                {
                    if (liaField.MatchedReRecordId != ReRecordId) continue;
                    if (count >= 3) break;
                    var liaD = CreateLiaDisplay(liaField.RecordId.ToString(), liaField.ComNm, liaField.Bal, liaField.Pmt);
                    subList.Add(liaD);
                    count++;
                }
                return subList;
            }
            public static LiaDisplay CreateLiaDisplay(string id, string name, decimal bal, decimal pmt)
            {
                var lia = new LiaDisplay()
                {
                    liabilityRecordID = id,
                    cNameHidden = name,
                    cBalHidden = bal,
                    cPmtHidden = pmt
                };
                return lia;
            }
        }
    };
}