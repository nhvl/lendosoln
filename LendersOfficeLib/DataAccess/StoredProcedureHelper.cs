using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Adapter;
using LendersOffice.Common;

namespace DataAccess
{
    /// <summary>
    /// Adding this class here due to similarity to SqlReaderExtensions.
    /// </summary>
    public static class SqlParameterExtensions
    {
        public static string AsSimpleFormatString(this SqlParameter p)
        {
            return string.Format(
                "Name: '{0}', Value: '{1}'",
                p.ParameterName, // {0}
                (
                p.Value == DBNull.Value
                ? "DBNULL"
                : p.Value.ToString())); // {1}
        }
    }

    public static class IDataRecordExtensions
    {
        public static Nullable<T> AsNullableStruct<T>(this IDataRecord reader, string columnName) where T : struct
        {
            if (reader[columnName] == DBNull.Value)
            {
                return null;
            }
            else
            {
                return (T)reader[columnName];
            }
        }

        public static T AsObject<T>(this IDataRecord reader, string columnName) where T : class
        {
            if (reader[columnName] == DBNull.Value)
            {
                return null;
            }
            else
            {
                return (T)reader[columnName];
            }
        }
        public static DateTime? AsNullableDateTime(this IDataRecord reader, string columnName)
        {
            if (reader[columnName] == DBNull.Value)
            {
                return new DateTime?();
            }
            else
            {
                return (DateTime)reader[columnName];
            }
        }

        public static Guid? AsNullableGuid(this IDataRecord reader, string columnName)
        {
            if (reader[columnName] == DBNull.Value)
            {
                return new Guid?();
            }
            else
            {
                return (Guid)reader[columnName];
            }
        }


        public static int? AsNullableInt(this IDataRecord reader, string columnName)
        {
            if (reader[columnName] == DBNull.Value)
            {
                return new int?();
            }
            else
            {
                return (int)reader[columnName];
            }
        }

        public static bool? AsNullableBool(this IDataRecord reader, string columnName)
        {
            if (reader[columnName] == DBNull.Value)
            {
                return new bool?();
            }
            else
            {
                return (bool)reader[columnName];
            }
        }

        public static string AsNullableString(this IDataRecord reader, string columnName)
        {
            if (reader[columnName] == DBNull.Value)
            {
                return null;
            }
            else
            {
                return (string)reader[columnName];
            }
        }

        public static byte[] AsNullableByteArray(this IDataRecord reader, string columnName)
        {
            if (reader[columnName] == DBNull.Value)
            {
                return null;
            }
            else
            {
                return (byte[])reader[columnName];
            }
        }

        public static string SafeString(this IDataRecord reader, string columnName)
        {
            object obj = reader[columnName];

            if (obj == DBNull.Value)
            {
                return string.Empty;
            }
            else
            {
                return Convert.ToString(obj).TrimWhitespaceAndBOM();
            }
        }
        public static DateTime? SafeDateTimeNullable(this IDataRecord reader, string columnName)
        {
            object obj = reader[columnName];

            DateTime? result = null;
            if (obj != DBNull.Value)
            {
                DateTime dt = Convert.ToDateTime(obj);

                if (dt > new DateTime(1900, 1, 1))
                {
                    result = dt;
                }
            }
            return result;
        }
        public static DateTime SafeDateTime(this IDataRecord reader, string columnName)
        {
            object obj = reader[columnName];

            DateTime result = DateTime.MinValue;
            if (obj != DBNull.Value)
            {
                DateTime dt = Convert.ToDateTime(obj);

                if (dt > new DateTime(1900, 1, 1))
                {
                    result = dt;
                }
            }
            return result;
        }
        public static decimal SafeDecimal(this IDataRecord reader, string columnName)
        {
            object obj = reader[columnName];

            decimal result = 0;

            if (obj != DBNull.Value)
            {
                result = Convert.ToDecimal(obj);
            }
            return result;
        }
        public static int SafeInt(this IDataRecord reader, string columnName)
        {
            object obj = reader[columnName];

            int result = 0;

            if (obj != DBNull.Value)
            {
                result = Convert.ToInt32(obj);
            }
            return result;
        }
        public static bool SafeBool(this IDataRecord reader, string columnName)
        {
            object obj = reader[columnName];
            bool result = false;
            if (obj != DBNull.Value)
            {
                result = Convert.ToBoolean(obj);
            }
            return result;
        }
        public static Guid SafeGuid(this IDataRecord reader, string columnName)
        {
            object obj = reader[columnName];
            Guid result = Guid.Empty;
            if (obj != DBNull.Value)
            {
                result = (Guid)obj;
            }
            return result;
        }

        public static int? GetNullableInt(this IDataRecord reader, string columnName)
        {
            object obj = reader[columnName];

            if (obj == DBNull.Value)
            {
                return null;
            }

            return Convert.ToInt32(obj);
        }
        public static long? GetNullableLong(this IDataRecord reader, string columnName)
        {
            object obj = reader[columnName];

            if (obj == DBNull.Value)
            {
                return null;
            }

            return Convert.ToInt64(obj);
        }

        /// <summary>
        /// Gets a nullable enum value with <see cref="int"/> underlying value from a nullable database row.
        /// </summary>
        /// <typeparam name="TEnum">The Enum type to return. If not an enum type or the underlying type is not <see cref="int"/>, a runtime exception will be thrown.</typeparam>
        /// <param name="record">The record to read the value from.</param>
        /// <param name="columnName">The column name in the record to get the value of.</param>
        /// <param name="skipDefinedCheck">
        /// Whether to check <see cref="Enum.IsDefined(Type, object)"/> on the value. 
        /// Setting this to true may be useful for <see cref="FlagsAttribute"/> enums, 
        /// which may have valid values for which IsDefined() returns false. 
        /// Alternative validity checks may have to be implemented if this is true.
        /// </param>
        /// <returns>A nullable Enum value parsed from the database.</returns>
        public static TEnum? GetNullableIntEnum<TEnum>(this IDataRecord record, string columnName, bool skipDefinedCheck = false) where TEnum : struct
        {
            if (!typeof(TEnum).IsEnum)
            {
                throw new InvalidOperationException("Type of TEnum must be an Enum type. Type was " + typeof(TEnum));
            }

            int? intValue = GetNullableInt(record, columnName);
            if (intValue.HasValue && (skipDefinedCheck || Enum.IsDefined(typeof(TEnum), intValue)))
            {
                return (TEnum)(object)intValue.Value;
            }

            return null;
        }

        /// <summary>
        /// Converts the current record into a dictionary of column name to value.
        /// </summary>
        /// <param name="record">The record to pull the column data from.</param>
        /// <returns>A case-insensitive dictionary mapping column name to value.</returns>
        public static Dictionary<string, object> ToDictionary(this IDataRecord record)
        {
            int count = record.FieldCount;
            var fieldLookup = new Dictionary<string, object>(count, StringComparer.OrdinalIgnoreCase);
            for (int i = 0; i < count; ++i)
            {
                // 2017/01 tj 449956:
                // I'm skipping a corner case that SqlDataRecord["field"] and SqlDataRecord["Field"] can
                // have different values, but that's hard to emulate with only one dictionary. In fact,
                // SELECT field ... will result in SqlDataRecord["Field"] returning the same value, and
                // SELECT field, Field ... will result in two different columns.
                // If this turns into an issue in the future, we can build a custom data type to represent
                // this "mostly case-insensitive" dictionary, but efficiency is the focus for now.
                if (!fieldLookup.ContainsKey(record.GetName(i)))
                {
                    fieldLookup.Add(record.GetName(i), record.GetValue(i));
                }
            }

            return fieldLookup;
        }
    }

    public static class IDataReaderExtensions
    {
        public static IReadOnlyList<T> Select<T>(this IDataReader reader, Func<IDataRecord, T> selector)
        {
            var Ts = new List<T>();

            while (reader.Read())
            {
                Ts.Add(selector(reader));
            }

            return Ts;
        }
    }

    public class StoredProcedureHelper
    {
        #region ExecuteNonQuery methods

        public static int ExecuteNonQuery(DataSrc dataSrc, string procedureName, int nRetry)
        {
            return ExecuteNonQuery(dataSrc, procedureName, true, nRetry, (SqlParameter[])null);
        }

        public static int ExecuteNonQuery(DataSrc dataSrc, string procedureName, int nRetry, IEnumerable<SqlParameter> parameters)
        {
            if (parameters == null)
            {
                return ExecuteNonQuery(dataSrc, procedureName, true, nRetry, (SqlParameter[])null);
            }
            else
            {
                return ExecuteNonQuery(dataSrc, procedureName, true, nRetry, (SqlParameter[])parameters.ToArray());
            }
        }

        /// <summary>
        /// DO NOT USE this method. Any legacy code calling this method need to migrate to newer methods or be remove.
        /// </summary>
        /// <param name="procedureName"></param>
        /// <param name="nRetry"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static int ExecuteNonQuery_OBSOLETE_DO_NOT_USE(string procedureName, int nRetry, params SqlParameter[] parameters)
        {
            return ExecuteNonQuery(DataSrc.LOShare, procedureName, true, nRetry, parameters);
        }

        public static int ExecuteNonQuery(DataSrc dataSrc, string procedureName, int nRetry, params SqlParameter[] parameters)
        {
            return ExecuteNonQuery(dataSrc, procedureName, true, nRetry, parameters);
        }

        public static int ExecuteNonQuery(DataSrc dataSrc, string procedureName, bool bSendEmail, int nRetry, params SqlParameter[] parameters)
        {
            Exception exc = null;
            int nExec = nRetry + 1;
            for (int i = 0; i < nExec; ++i)
            {
                using (DbConnection conn = GetConnection(dataSrc, false))
                {
                    try
                    {
                        return ExecuteNonQueryHelper(conn, procedureName, parameters);
                    }
                    catch (Exception e)
                    {
                        exc = e;
                        Tools.LogWarning(string.Format("Failed to executing store procedure {0}, exec count ={1}, out of {2} total # of tries.",
                            procedureName, i.ToString(), nRetry.ToString()), exc);
                        if ((i + 1) < nExec) // not the last time.
                        {
                            Tools.SleepAndWakeupRandomlyWithin(1000, 4500);
                        }
                    }
                }
            }

            LogDetailException(dataSrc.ToString(), "ExecuteNonQuery", procedureName, nRetry, exc, bSendEmail, parameters);
            throw exc;
        }

        public static int ExecuteNonQuery(Guid brokerId, string procedureName, int nRetry, IEnumerable<SqlParameter> parameters)
        {
            DbConnectionInfo connectionInfo = DbConnectionInfo.GetConnectionInfo(brokerId);

            return ExecuteNonQuery(connectionInfo, procedureName, nRetry, parameters);
        }

        public static int ExecuteNonQuery(DbConnectionInfo connectionInfo, string procedureName, int nRetry, IEnumerable<SqlParameter> parameters)
        {
            Exception exc = null;
            int nExec = nRetry + 1;
            for (int i = 0; i < nExec; ++i)
            {
                using (DbConnection conn = connectionInfo.GetConnection())
                {
                    try
                    {
                        return ExecuteNonQueryHelper(conn, procedureName, parameters);
                    }
                    catch (Exception e)
                    {
                        exc = e;
                        Tools.LogWarning(string.Format("Failed to executing store procedure {0}, exec count ={1}, out of {2} total # of tries.",
                            procedureName, i.ToString(), nRetry.ToString()), exc);
                        if ((i + 1) < nExec) // not the last time.
                        {
                            Tools.SleepAndWakeupRandomlyWithin(1000, 4500);
                        }
                    }
                }
            }

            LogDetailException(connectionInfo.FriendlyDisplay, "ExecuteNonQuery", procedureName, nRetry, exc, true, parameters);
            throw exc;
        }


        /// <summary>
        /// You probably don't want to use this outside this class unless you're doing something unusual, like a migration for an odd connection.
        /// </summary>
        /// <param name="procedureName"></param>
        /// <param name="nRetry"></param>
        /// <param name="parameters"></param>
        /// <returns>number of rows affected</returns>
        public static int ExecuteNonQueryHelper(DbConnection conn, string procedureName, IEnumerable<SqlParameter> parameters)
        {
            DbCommand command = conn.CreateCommand();
            command.CommandText = procedureName;
            command.CommandType = CommandType.StoredProcedure;
            
            if (parameters != null)
            {
                foreach (SqlParameter p in parameters)
                {
                    command.Parameters.Add((SqlParameter)((ICloneable)p).Clone());
                }
            }

            conn.OpenWithRetry();

            try
            {
                int ret = command.ExecuteNonQuery();

                if (parameters != null)
                {
                    // 9/7/2006 dd - Since we are cloning the SqlParameters, we need to set the value of the new SqlParamter to the original argument.
                    foreach (SqlParameter p in parameters)
                    {
                        p.Value = command.Parameters[p.ParameterName].Value;
                    }
                }
                return ret;
            }
            finally
            {
                command.Parameters.Clear();
            }

        }
        #endregion

        #region ExecuteScalar methods

        public static object ExecuteScalar(DataSrc dataSrc, string procedureName, params SqlParameter[] parameters)
        {
            using (DbConnection conn = GetConnection(dataSrc, false))
            {
                return ExecuteScalarImpl(dataSrc.ToString(), conn, procedureName, parameters);
            }
        }

        public static object ExecuteScalar(Guid brokerId, string procedureName, IEnumerable<SqlParameter> parameters)
        {
            DbConnectionInfo connectionInfo = DbConnectionInfo.GetConnectionInfo(brokerId);
            return ExecuteScalar(connectionInfo, procedureName, parameters);
        }

        public static object ExecuteScalar(DbConnectionInfo connectionInfo, string procedureName, IEnumerable<SqlParameter> parameters)
        {
            using (DbConnection conn = connectionInfo.GetConnection())
            {
                return ExecuteScalarImpl(connectionInfo.FriendlyDisplay, conn, procedureName, parameters);
            }

        }

        private static object ExecuteScalarImpl(string dataSourceDescription, DbConnection conn, string procedureName, IEnumerable<SqlParameter> parameters)
        {
            var command = conn.CreateCommand();
            command.CommandText = procedureName;
            command.CommandType = CommandType.StoredProcedure;

            if (parameters != null)
            {
                foreach (SqlParameter p in parameters)
                {
                    command.Parameters.Add(p);
                }
            }

            conn.OpenWithRetry();

            try
            {
                return command.ExecuteScalar();
            }
            catch (SqlException exc)
            {
                LogDetailException(dataSourceDescription, "ExecuteScalar", procedureName, 1, exc, true, parameters);

                throw;
            }
        }
        #endregion

        #region ExecuteReader methods



        /// <summary>
        /// DON'T forget to surround Disposable objects such as DbDataReader with using blocks.
        /// </summary>
        public static DbDataReader ExecuteReader(DataSrc dataSrc, string procedureName)
        {
            return ExecuteReader(dataSrc, procedureName, null, (SqlParameter[])null);
        }

        public static IReadOnlyList<T> ExecuteReaderAndGetResults<T>(Func<IDataRecord, T> selector, DbConnectionInfo connInfo, string procedureName, IEnumerable<SqlParameter> parameters)
        {
            using (var reader = ExecuteReader(connInfo, procedureName, parameters))
            {
                return reader.Select<T>(selector);
            }
        }

        public static IReadOnlyList<T> ExecuteReaderAndGetResults<T>(Func<IDataRecord, T> selector, Guid brokerId, string procedureName, IEnumerable<SqlParameter> parameters)
        {
            using (var reader = ExecuteReader(brokerId, procedureName, parameters))
            {
                return reader.Select<T>(selector);
            }
        }

        public static IReadOnlyList<IReadOnlyList<IReadOnlyDictionary<string, object>>> ExecuteReaderIntoResultListDictionaries(Guid brokerId, string procedureName, IEnumerable<SqlParameter> parameters)
        {
            var resultSets = new List<IReadOnlyList<IReadOnlyDictionary<string, object>>>();
            using (var reader = ExecuteReader(brokerId, procedureName, parameters))
            {
                do
                {
                    resultSets.Add(reader.Select(IDataRecordExtensions.ToDictionary));
                }
                while (reader.NextResult());
            }

            return resultSets;
        }

        public static DbDataReader ExecuteReader_OBSOLETE_DO_NOT_USE(string procedureName, IEnumerable<SqlParameter> parameters)
        {
            return ExecuteReader(DataSrc.LOShareROnly, procedureName, null, parameters);
        }

        public static DbDataReader ExecuteReader(DataSrc dataSrc, string procedureName, IEnumerable<SqlParameter> parameters)
        {
            return ExecuteReader(dataSrc, procedureName, null, parameters.ToArray());
        }
        public static DbDataReader ExecuteReaderAndLogPrinting(Guid brokerId, string procedureName, IEnumerable<SqlParameter> parameters)
        {
            DbConnectionInfo connectionInfo = DbConnectionInfo.GetConnectionInfo(brokerId);

            string dataSourceName = connectionInfo.FriendlyDisplay + " (BrokerId: " + brokerId + ")";
            DbConnection conn = DbAccessUtils.GetReadonlyConnection(brokerId);
            return ExecuteReaderImpl(dataSourceName, conn, procedureName, null, parameters, true);
        }

        public static DbDataReader ExecuteReader(DataSrc dataSrc, string procedureName, params SqlParameter[] parameters)
        {
            return ExecuteReader(dataSrc, procedureName, null, parameters);
        }

        public static DbDataReader ExecuteReader(Guid brokerId, string procedureName, IEnumerable<SqlParameter> parameters)
        {
            DbConnectionInfo connectionInfo = DbConnectionInfo.GetConnectionInfo(brokerId);

            string dataSourceName = connectionInfo.FriendlyDisplay + " (BrokerId: " + brokerId + ")";
            DbConnection conn = DbAccessUtils.GetReadonlyConnection(brokerId);
            return ExecuteReaderImpl(dataSourceName, conn, procedureName, null, parameters, false);
        }

        public static DbDataReader ExecuteReader(DbConnectionInfo connectionInfo, string procedureName, IEnumerable<SqlParameter> parameters)
        {
            DbConnection conn = connectionInfo.GetReadOnlyConnection();
            return ExecuteReaderImpl(connectionInfo.FriendlyDisplay, conn, procedureName, null, parameters, false);
        }
        public static DbDataReader ExecuteReader(DbConnectionInfo connectionInfo, string procedureName, int? customTimeoutInSeconds, IEnumerable<SqlParameter> parameters)
        {
            DbConnection conn = connectionInfo.GetReadOnlyConnection();
            return ExecuteReaderImpl(connectionInfo.FriendlyDisplay, conn, procedureName, customTimeoutInSeconds, parameters, false);
        }
		public static DbDataReader ExecuteReader(DataSrc dataSrc, string procedureName, int? customTimeoutInSeconds, IEnumerable<SqlParameter> parameters)
		{
			DbConnection conn = GetConnection(dataSrc, false);

			return ExecuteReaderImpl(dataSrc.ToString(), conn, procedureName, customTimeoutInSeconds, parameters, false);
		}
		public static DbDataReader ExecuteReaderDuringBootPhase(DataSrc dataSrc, string procedureName, int? customTimeoutInSeconds, IEnumerable<SqlParameter> parameters)
		{
			DbConnection conn = GetConnection(dataSrc, true);

			return ExecuteReaderImpl(dataSrc.ToString(), conn, procedureName, customTimeoutInSeconds, parameters, false);
		}

		/// <summary>
		/// Justin Jia Add this.
		/// I add this so that the loan export tool can make query with differnet time out setting by broker id.
		/// </summary>
		/// <param name="brokerId">The broker id to locate database.</param>
		/// <param name="procedureName">The stored procedure name.</param>
		/// <param name="customTimeoutInSeconds">The time out setting measured by second.</param>
		/// <param name="parameters">The parameters for query.</param>
		/// <returns>The sql data reader object is returned.</returns>
		public static DbDataReader ExecuteReader(Guid brokerId, string procedureName, int? customTimeoutInSeconds, IEnumerable<SqlParameter> parameters)
        {
            DbConnectionInfo connectionInfo = DbConnectionInfo.GetConnectionInfo(brokerId);

            string dataSourceName = connectionInfo.FriendlyDisplay + " (BrokerId: " + brokerId + ")";
            DbConnection conn = DbAccessUtils.GetReadonlyConnection(brokerId);
            return ExecuteReaderImpl(dataSourceName, conn, procedureName, customTimeoutInSeconds, parameters, false);
        }

        private static DbDataReader ExecuteReaderImpl(string dataSourceName, DbConnection conn, string procedureName, int? customTimeoutInSeconds, IEnumerable<SqlParameter> parameters, bool logPrintMessages)
        {
            DbCommand command = conn.CreateCommand();
            command.CommandText = procedureName;
            command.CommandType = CommandType.StoredProcedure;

            //30 is default so imagine we should not go lower
            if (customTimeoutInSeconds.HasValue && customTimeoutInSeconds.Value > 30)
            {
                command.CommandTimeout = customTimeoutInSeconds.Value;
            }

            if (parameters != null)
            {
                foreach (SqlParameter p in parameters)
                {
                    command.Parameters.Add(p);
                }
            }

            conn.OpenWithRetry();

            try
            {
                return command.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception exc)
            {
                conn.Close();
                LogDetailException(dataSourceName, "ExecuteReader", procedureName, 1, exc, true, parameters);
                throw;
            }
        }

        public static DataTable ExecuteDataTable(DataSrc dataSrc, string procedureName, params SqlParameter[] parameters)
        {
            DataSet ds = ExecuteDataSet(dataSrc, procedureName, parameters);
            return ds.Tables[0];
        }

        public static DataTable ExecuteDataTable(Guid brokerId, string procedureName, IEnumerable<SqlParameter> parameters)
        {
            DbConnectionInfo dbConnectionInfo = DbConnectionInfo.GetConnectionInfo(brokerId);
            using (DbConnection conn = dbConnectionInfo.GetReadOnlyConnection())
            {
                DataSet ds = ExecuteDataSetImpl(dbConnectionInfo.FriendlyDisplay, conn, procedureName, parameters);
                return ds.Tables[0];
            }
        }

        public static DataSet ExecuteDataSet(Guid brokerId, string procedureName, params SqlParameter[] parameters)
        {
            DbConnectionInfo dbConnectionInfo = DbConnectionInfo.GetConnectionInfo(brokerId);

            using (DbConnection conn = dbConnectionInfo.GetReadOnlyConnection())
            {
                return ExecuteDataSetImpl(dbConnectionInfo.FriendlyDisplay, conn, procedureName, parameters);
            }

        }

        public static DataSet ExecuteDataSet(DataSrc dataSrc, string procedureName, params SqlParameter[] parameters)
        {
            using (DbConnection conn = GetConnection(dataSrc, false))
            {
                return ExecuteDataSetImpl(dataSrc.ToString(), conn, procedureName, parameters);
            }
        }

        private static DataSet ExecuteDataSetImpl(string dataSourceName, DbConnection conn, string procedureName, IEnumerable<SqlParameter> parameters)
        {
            DataSet dataset = new DataSet();
            DbCommand command = conn.CreateCommand();
            command.CommandText = procedureName;
            command.CommandType = CommandType.StoredProcedure;

            if (parameters != null)
            {
                foreach (SqlParameter p in parameters)
                {
                    command.Parameters.Add(p);
                }
            }

            conn.OpenWithRetry();

            try
            {

                using (DbDataAdapter sda = DbAccessUtils.GetDbProvider().CreateDataAdapter())
                {
                    sda.SelectCommand = command;
                    sda.Fill(dataset);
                    return dataset;
                }
            }
            catch (SqlException exc)
            {
                LogDetailException(dataSourceName, "ExecuteDataset", procedureName, 1, exc, true, parameters);
                throw;
            }
        }
        #endregion

        #region ExecuteReaderWithRetries

        public static DbDataReader ExecuteReaderWithRetries(DataSrc dataSrc, string procedureName, int nRetries, params SqlParameter[] parameters)
        {

            Exception lastException = null;

            int nExec = nRetries + 1;

            for (int i = 0; i < nExec; i++)
            {
                try
                {
                    return ExecuteReaderWithRetriesHelper(dataSrc, procedureName, parameters);
                }
                catch (Exception exc)
                {
                    lastException = exc;
                }
                Tools.SleepAndWakeupRandomlyWithin(1000, 4500);
            }

            SqlException sqlException = lastException as SqlException;
            if (null != sqlException)
            {
                LogDetailException(dataSrc.ToString(), "ExecuteReaderWithRetries", procedureName, nRetries, sqlException, true, parameters);
            }

            throw lastException;
        }

        private static DbConnection GetConnection(DataSrc dataSrc, bool isBootPhase)
        {
            return DbAccessUtils.GetConnection(dataSrc);
        }

        public static DbDataReader ExecuteReaderWithRetriesHelper(DataSrc dataSrc, string procedureName, params SqlParameter[] parameters)
        {
            DbConnection conn = GetConnection(dataSrc, false);

            DbCommand command = conn.CreateCommand();
            command.CommandText = procedureName;
            command.CommandType = CommandType.StoredProcedure;

            if (parameters != null)
            {
                foreach (SqlParameter p in parameters)
                {
                    command.Parameters.Add((SqlParameter)((ICloneable)p).Clone());
                }
            }

            conn.OpenWithRetry();

            try
            {
                return command.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (SqlException)
            {
                conn.Close();
                throw;
            }
            catch (Exception)
            {
                conn.Close();
                throw;
            }

        }

        #endregion

        private static void LogDetailException_old(string dataSourceName, string sqlMethod, string procedureName, int numberOfRetries, Exception exc, bool bSendEmail, IEnumerable<SqlParameter> parameters)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Failed to executing store procedure {0}, after {1} tries.", procedureName, numberOfRetries);
            sb.Append(Environment.NewLine);
            sb.AppendFormat("Data Source={0}", dataSourceName);
            sb.Append(Environment.NewLine);
            sb.AppendFormat("<!---- Stored Procedure Debug Info ({0}) ---->", sqlMethod).Append(Environment.NewLine);
            sb.AppendFormat("Procedure name = {0}{1}", procedureName, Environment.NewLine);
            if (parameters != null)
            {
                foreach (SqlParameter p in parameters)
                {
                    sb.AppendFormat("  {0} = [{1}]{2}", p.ParameterName, p.Value, Environment.NewLine);
                }
            }
            sb.Append("<!----- End Stored Procedure Debug Info ---->").Append(Environment.NewLine);

            if (exc is SqlException)
            {
                SqlException sqlExc = (SqlException)exc;
                sb.Append("SqlException.Number = " + sqlExc.Number).Append(Environment.NewLine);

                CBaseException baseException = new GenericSqlException(sb.ToString(), sqlExc);

                if (bSendEmail)
                {
                    Tools.LogErrorWithCriticalTracking(baseException);
                }
                else
                {
                    Tools.LogError(baseException);
                }
            }
            else
            {

                Exception mainException = new Exception(sb.ToString(), exc);

                if (bSendEmail)
                {
                    Tools.LogErrorWithCriticalTracking(mainException);
                }
                else
                {
                    Tools.LogError(mainException);
                }
            }
        }

        /// <summary>
        /// A new version made by Justin Jia.
        /// It allows user to directly execute the log's stored procedure.
        /// </summary>
        /// <param name="dataSourceName"></param>
        /// <param name="sqlMethod"></param>
        /// <param name="procedureName"></param>
        /// <param name="numberOfRetries"></param>
        /// <param name="exc"></param>
        /// <param name="bSendEmail"></param>
        /// <param name="parameters"></param>
        private static void LogDetailException(string dataSourceName, string sqlMethod, string procedureName, int numberOfRetries, Exception exc, bool bSendEmail, IEnumerable<SqlParameter> parameters)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Failed to executing store procedure " + procedureName + ", after " + numberOfRetries + " tries.");
            sb.AppendLine("Data Source=" + dataSourceName);
            sb.AppendLine("<!---- Executable Stored Procedure Debug Info (" + sqlMethod + ") ---->");
            sb.AppendLine("Exec " + procedureName);
            if (parameters != null)
            {
                bool isNotFirstParameter = false;
                foreach (SqlParameter p in parameters)
                {
                    if (isNotFirstParameter)
                    {
                        sb.AppendLine(",");
                    }
                    else
                    {
                        isNotFirstParameter = true;
                    }

                    sb.Append("  @" + p.ParameterName.TrimStart('@') + " = " + GetSqlStringValue(p) + GetSqlComment(p));
                }
            }

            sb.AppendLine();
            sb.AppendLine("<!----- End Stored Procedure Debug Info ---->");

            if (exc is SqlException)
            {
                SqlException sqlExc = (SqlException)exc;
                sb.Append("SqlException.Number = " + sqlExc.Number).Append(Environment.NewLine);

                CBaseException baseException = new GenericSqlException(sb.ToString(), sqlExc);

                if (bSendEmail)
                {
                    Tools.LogErrorWithCriticalTracking(baseException);
                }
                else
                {
                    Tools.LogError(baseException);
                }
            }
            else
            {

                Exception mainException = new Exception(sb.ToString(), exc);

                if (bSendEmail)
                {
                    Tools.LogErrorWithCriticalTracking(mainException);
                }
                else
                {
                    Tools.LogError(mainException);
                }
            }
        }

        /// <summary>
        /// Gets a SQL string of the value of <paramref name="parameter"/>.
        /// </summary>
        /// <param name="parameter">The parameter to convert to a SQL string.</param>
        /// <returns>A string representing the parameter's value in SQL.</returns>
        private static string GetSqlStringValue(SqlParameter parameter)
        {
            if (parameter.Value == null || parameter.Value == DBNull.Value)
            {
                return "NULL";
            }

            switch (parameter.DbType)
            {
                case DbType.Int16:
                    return GetSqlLiteralString<Int16>(parameter);
                case DbType.Int32:
                    return GetSqlLiteralString<Int32>(parameter);
                case DbType.Int64:
                    return GetSqlLiteralString<Int64>(parameter);
                case DbType.UInt16:
                    return GetSqlLiteralString<UInt16>(parameter);
                case DbType.UInt32:
                    return GetSqlLiteralString<UInt32>(parameter);
                case DbType.UInt64:
                    return GetSqlLiteralString<UInt64>(parameter);
                case DbType.Byte:
                    return GetSqlLiteralString<Byte>(parameter);
                case DbType.SByte:
                    return GetSqlLiteralString<SByte>(parameter);
                case DbType.Binary:
                    return "0x" + BitConverter.ToString((byte[])parameter.Value).Replace("-", string.Empty); // From http://stackoverflow.com/a/1064129/2946652
                default:
                    return "'" + parameter.Value.ToString().Replace("'", "''") + "'";
            }
        }

        /// <summary>
        /// Gets the best representation we have of a the SQL string representation
        /// of the value for a numeric value.  Useful for values that can be declared
        /// as literals.  Necessary to cover enum values, which would otherwise show
        /// their string value rather than numeric underlying value.
        /// </summary>
        /// <typeparam name="TStruct">The type of the struct that we're converting to.</typeparam>
        /// <param name="parameter">The SQL parameter to read our value from.</param>
        /// <returns>The string literal of the value of <paramref name="parameter"/>.</returns>
        private static string GetSqlLiteralString<TStruct>(SqlParameter parameter) where TStruct : struct
        {
            return ((TStruct)parameter.Value).ToString();
        }

        /// <summary>
        /// Extracts a comment of the parameter's information to be used for debugging.
        /// </summary>
        /// <param name="parameter">The SQL parameter to read a value from.</param>
        /// <returns>A commented out snippet of information.</returns>
        /// <example>An enum's named value, rather than just the numeric.</example>
        private static string GetSqlComment(SqlParameter parameter)
        {
            if (parameter.Value is Enum)
            {
                return "/*" + parameter.Value.ToString() + "*/";
            }
            else
            {
                return string.Empty;
            }
        }
    }
}