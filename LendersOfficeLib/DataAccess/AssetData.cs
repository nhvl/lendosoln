using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.UI;

namespace DataAccess
{
	public class CAssetAllAppsData : CPageData
	{

		private static CSelectStatementProvider s_selectProvider;
		static CAssetAllAppsData()
		{
			StringList list = new StringList();

            list.Add("aAsstLiqTot" );
            list.Add("aAssetCollection" );
            list.Add("aAsstNonReSolidTot" );
            list.Add("aAsstValTot" );
            list.Add("aBNm" );
            list.Add("aCNm" );
            list.Add("aReTotVal" );
            list.Add("sAsstLValTot");
            list.Add("aReCollection");

			s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }

        public CAssetAllAppsData( Guid fileId ) : base( fileId , "AssetAllAppsWorksheet" , s_selectProvider )
        {
        }
        protected override bool m_enforceAccessControl
        {
            get
            {
                return false;
            }
        }
	}

	public class CAssetRecordData : CPageData
	{

		private static CSelectStatementProvider s_selectProvider;
		static CAssetRecordData()
		{
			StringList list = new StringList();
            list.Add("aAsstLiaCompletedNotJointly");
            list.Add( "aAssetXmlContent" );
            list.Add( "aAssetCollection");
            list.Add( "aAsstLiqTot" );
            list.Add( "aAsstValTot" );
            list.Add( "aBAddr" );
            list.Add( "aBCity" );
            list.Add( "aBNm" );
            list.Add( "aBSsn" );
            list.Add( "aBState" );
            list.Add( "aBZip" );
            list.Add( "aCNm" );
            list.Add( "aCSsn" );
            list.Add( "aReTotVal" );
            list.Add("sfGetAgentOfRole");
            list.Add( "sLenderNumVerif" );
            list.Add( "sLNm" );
            list.Add( "sOpenedD" );
            list.Add( "sPreparerXmlContent" );
            list.Add("sLoanVersionT");
            list.Add("sLienToIncludeCashDepositInTridDisclosures");

			s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }

        public CAssetRecordData( Guid fileId) : base( fileId , "AssetRecord" , s_selectProvider )
        {
        }

	}

}