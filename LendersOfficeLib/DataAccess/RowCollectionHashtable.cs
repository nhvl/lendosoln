

using System.Collections;
using System.Data;
using System;

namespace DataAccess
{
	
	public class CRowHashtable
	{
		private  DataRowCollection m_coll;
		private  Hashtable m_hashtable;		
		public CRowHashtable( DataRowCollection coll, string keyName )
		{
			m_coll = coll;

			int count = coll.Count;
			m_hashtable = new Hashtable( count );
			foreach( DataRow row in coll )
			{
				m_hashtable.Add( row[keyName], row );
			}
		}

		/// <summary>
		/// TODO: DO NOT CHECK THIS METHOD IN, IT WOULD CAUSE TOO MUCH PROBLEM WITH PERFORMANCE
		/// </summary>
		/// <param name="coll"></param>
		/// <param name="keyName"></param>
		/// <param name="isTakeFirstDuplicateOnly"> if true, then ignore the rest of the duplicated rows</param>
        //public CRowHashtable( DataRowCollection coll, string keyName, bool isTakeFirstDuplicateOnly )
        //{
        //    Hashtable keys = new Hashtable( coll.Count );
        //    ArrayList dups = new ArrayList( coll.Count );
        //    foreach( DataRow row in coll )
        //    {
        //        object key = row[keyName];
        //        if( keys.ContainsKey( key ) )
        //            dups.Add(  row );
        //        else
        //            keys[ key ] = null;
        //    }

        //    foreach( DataRow row in dups )
        //    {
        //        coll.Remove( row );
        //    }

        //    m_coll = coll;
            
        //    int count = coll.Count;
        //    m_hashtable = new Hashtable( count );
        //    foreach( DataRow row in coll )
        //    {
        //        object key =  row[keyName];
        //        if( isTakeFirstDuplicateOnly && m_hashtable.ContainsKey( key ) )
        //            continue;					
        //        m_hashtable.Add( key, row );
        //    }
        //}

        //public CRowHashtable( DataRowCollection coll, string keyName, object nullKeySubstitute )
        //{
        //    m_coll = coll;

        //    int count = coll.Count;
        //    m_hashtable = new Hashtable( count );
        //    foreach( DataRow row in coll )
        //    {
        //        object key = row[keyName]; 
        //        if( DBNull.Value == key )
        //            key = nullKeySubstitute;
        //        m_hashtable.Add( key, row );
        //    }
        //}

		public DataRow GetRowByKey( object key )
		{
			return (DataRow) m_hashtable[key];
		}

		public bool HasRowOfKey( object key )
		{
			return m_hashtable.ContainsKey( key );
		}

		public DataRow GetRowByIndex( int index )
		{
			return m_coll[index];
		}

		/// <summary>
		/// Avoid using collection directly as much as possible; don't do a search
		/// on the key, use GetRow() instead
		/// </summary>
        //public DataRowCollection Collection
        //{
        //    get {	return m_coll; }
        //}

		public int Count
		{
			get { return m_coll.Count;}
		}

	}
}