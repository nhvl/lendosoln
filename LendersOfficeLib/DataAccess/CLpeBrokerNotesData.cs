using System;
using System.Collections;
using System.Data;

namespace DataAccess
{

	public class CLpeBrokerNotesData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CLpeBrokerNotesData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("sLpeNotesFromBrokerToUnderwriterNew");
            list.Add("sLpeNotesFromBrokerToUnderwriterHistory");
            list.Add("sEstCloseD");
            list.Add("sAgentCollection");
            list.Add("sEmployeeLenderAccExecEmail");
            list.Add("sEmployeeUnderwriterEmail");
            list.Add("sEmployeeProcessorEmail");
            list.Add("sEmployeeLockDeskEmail");
            list.Add("sIsIncomeAssetsNonRequiredFhaStreamline");
            list.Add("sProdRLckdDays");
            list.Add("sProdRLckdExpiredDLabel");

	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CLpeBrokerNotesData(Guid fileId) : base(fileId, "CLpeBrokerNotesData", s_selectProvider)
		{
		}
        protected override bool m_enforceAccessControl
        {
            get { return false; }
        }

	}
}
