using System;
using System.Collections;
using System.Data;

namespace DataAccess
{

	public class CLOImportLiabilityData : CFullAccessPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CLOImportLiabilityData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
	        list.Add("sfImportLiabilitiesFromCreditReport");
			list.Add("sfImportPublicRecordsFromCreditReport");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CLOImportLiabilityData(Guid fileId) : base(fileId, "CLOImportLiabilityData", s_selectProvider)
		{
		}
	}
}
