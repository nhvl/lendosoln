using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
	public class CTILData : CPageData
	{

		private static CSelectStatementProvider s_selectProvider;
		static CTILData()
		{
			StringList list = new StringList();

            list.Add("sfGetAgentOfRole");
            list.Add( "sApr" );
            list.Add( "sAprIncludedCc" );
            list.Add( "sAprIncludesReqDeposit" );
            list.Add( "sAssumeLT" );
            list.Add( "sAsteriskEstimate" );
            list.Add( "sBiweeklyPmt" );
            list.Add( "sBuydwnMon1" );
            list.Add( "sBuydwnMon2" );
            list.Add( "sBuydwnMon3" );
            list.Add( "sBuydwnMon4" );
            list.Add( "sBuydwnMon5" );
            list.Add( "sBuydwnR1" );
            list.Add( "sBuydwnR2" );
            list.Add( "sBuydwnR3" );
            list.Add( "sBuydwnR4" );
            list.Add( "sBuydwnR5" );
            list.Add( "sDue" );
            list.Add( "sFilingF" );
            list.Add( "sFinalLAmt" );
            list.Add( "sFinancedAmt" );
            list.Add( "sFinCharge" );
            list.Add( "sFinMethDesc" );
            list.Add( "sFinMethT" );
            list.Add( "sGradPmtR" );
            list.Add( "sGradPmtYrs" );
            list.Add( "sHasDemandFeature" );
            list.Add( "sHasVarRFeature" );
            list.Add( "sIfPurchFloodInsFrCreditor" );
            list.Add( "sIfPurchInsFrCreditor" );
            list.Add( "sIfPurchPropInsFrCreditor" );
            list.Add( "sInsFrCreditorAmt" );
            list.Add( "sInsReqDesc" );
            list.Add( "sIOnlyMon" );
            list.Add( "sIsPropertyBeingSoldByCreditor");
            list.Add( "sLateChargeBaseDesc" );
            list.Add( "sLateChargePc" );
            list.Add( "sLateDays" );
            list.Add( "sNoteIR" );
            list.Add( "sOnlyLatePmtEstimate" );
            list.Add( "sOpenedD" );
            list.Add( "sPmtAdjCapMon" );
            list.Add( "sPmtAdjCapR" );
            list.Add( "sPmtAdjMaxBalPc" );
            list.Add( "sPmtAdjRecastPeriodMon" );
            list.Add( "sPmtAdjRecastStop" );
            list.Add( "sPpmtAmt" );
            list.Add( "sPpmtMon" );
            list.Add( "sPpmtOneAmt" );
            list.Add( "sPpmtOneMon" );
            list.Add( "sPpmtStartMon" );
            list.Add( "sPreparerXmlContent" );
            list.Add( "sPrepmtPenaltyT" );
            list.Add( "sPrepmtRefundT" );
            list.Add( "sProMIns" );
            list.Add( "sProMIns2" );
            list.Add( "sProMIns2Mon" );
            list.Add( "sProMInsBaseAmt" );
            list.Add( "sProMInsCancelLtv" );
            list.Add( "sProMInsMidptCancel" );
            list.Add( "sProMInsMon" );
            list.Add( "sProMInsR" );
            list.Add( "sProMInsR2" );
            list.Add( "sProMInsT" );
            list.Add( "sRAdj1stCapMon" );
            list.Add( "sRAdj1stCapR" );
            list.Add( "sRAdjCapMon" );
            list.Add( "sRAdjCapR" );
            list.Add( "sRAdjFloorR" );
            list.Add( "sRAdjIndexR" );
            list.Add( "sRAdjLifeCapR" );
            list.Add( "sRAdjMarginR" );
            list.Add( "sRAdjRoundT" );
            list.Add( "sRAdjRoundToR" );
            list.Add( "sRAdjWorstIndex" );
            list.Add( "sReqCreditDisabilityIns" );
            list.Add( "sReqCreditLifeIns" );
            list.Add( "sReqFloodIns" );
            list.Add( "sReqPropIns" );
            list.Add( "sSchedBal1" );
            list.Add( "sSchedBal10" );
            list.Add( "sSchedBal2" );
            list.Add( "sSchedBal3" );
            list.Add( "sSchedBal4" );
            list.Add( "sSchedBal5" );
            list.Add( "sSchedBal6" );
            list.Add( "sSchedBal7" );
            list.Add( "sSchedBal8" );
            list.Add( "sSchedBal9" );
            list.Add("sSchedDueD1Lckd");
            list.Add( "sSchedDueD1" );
            list.Add( "sSchedDueD10" );
            list.Add( "sSchedDueD2" );
            list.Add( "sSchedDueD3" );
            list.Add( "sSchedDueD4" );
            list.Add( "sSchedDueD5" );
            list.Add( "sSchedDueD6" );
            list.Add( "sSchedDueD7" );
            list.Add( "sSchedDueD8" );
            list.Add( "sSchedDueD9" );
            list.Add( "sSchedIR1" );
            list.Add( "sSchedIR10" );
            list.Add( "sSchedIR2" );
            list.Add( "sSchedIR3" );
            list.Add( "sSchedIR4" );
            list.Add( "sSchedIR5" );
            list.Add( "sSchedIR6" );
            list.Add( "sSchedIR7" );
            list.Add( "sSchedIR8" );
            list.Add( "sSchedIR9" );
            list.Add( "sSchedPmt1" );
            list.Add( "sSchedPmt10" );
            list.Add( "sSchedPmt2" );
            list.Add( "sSchedPmt3" );
            list.Add( "sSchedPmt4" );
            list.Add( "sSchedPmt5" );
            list.Add( "sSchedPmt6" );
            list.Add( "sSchedPmt7" );
            list.Add( "sSchedPmt8" );
            list.Add( "sSchedPmt9" );
            list.Add( "sSchedPmtNum1" );
            list.Add( "sSchedPmtNum10" );
            list.Add( "sSchedPmtNum2" );
            list.Add( "sSchedPmtNum3" );
            list.Add( "sSchedPmtNum4" );
            list.Add( "sSchedPmtNum5" );
            list.Add( "sSchedPmtNum6" );
            list.Add( "sSchedPmtNum7" );
            list.Add( "sSchedPmtNum8" );
            list.Add( "sSchedPmtNum9" );
            list.Add( "sSchedPmtNumTot" );
            list.Add( "sSchedPmtTot" );
            list.Add( "sSecurityCurrentOwn" );
            list.Add( "sSecurityPurch" );
            list.Add( "sSpFullAddr" );
            list.Add( "sTerm" );
            list.Add( "sTilInitialDisclosureD" );
            list.Add( "sTilRedisclosureD" );
            list.Add( "sVarRNotes" );
            list.Add("sIsRateLocked");
			list.Add( "sIsOptionArm" );
			list.Add( "sOptionArmMinPayPeriod" );
			list.Add( "sOptionArmMinPayIsIOOnly" );
			list.Add( "sOptionArmIntroductoryPeriod" );
			list.Add( "sOptionArmInitialFixMinPmtPeriod" );
			list.Add( "sOptionArmMinPayOptionT" );
			list.Add( "sOptionArmPmtDiscount" );
			list.Add( "sOptionArmNoteIRDiscount" );
			list.Add( "sIsFullAmortAfterRecast" );
			list.Add( "sIsOptionArmTeaserR" );
			list.Add( "sDtiUsingMaxBalPc" );
            list.Add( "sProMInsCancelMinPmts" );
            list.Add("sRedisclosureMethodT");
            list.Add("sTilRedisclosuresReceivedD");
            list.Add("sPrepmtPeriodMonths");
            list.Add("sSoftPrepmtPeriodMonths");
            list.Add("sIsConvertibleMortgage");
            list.Add("sInitAPR");
            list.Add("sLastDiscAPR");
            list.Add("sIsUseManualApr");
            list.Add("sArmIndexNameVstr");
			s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

		}

        public CTILData( Guid fileId ) : base( fileId , "TIL" , s_selectProvider )
        {
        }

	}

}