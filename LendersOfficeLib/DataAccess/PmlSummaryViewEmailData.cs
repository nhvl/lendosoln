using System;
using System.Collections;
using System.Data;

namespace DataAccess
{

	public class CPmlSummaryViewEmailData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CPmlSummaryViewEmailData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
	        list.Add("sfGetAgentOfRole");
            list.Add("sEmployeeLenderAccExec");
            list.Add("sPriceGroup");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CPmlSummaryViewEmailData(Guid fileId) : base(fileId, "CPmlSummaryViewEmailData", s_selectProvider)
		{
		}
	}
}
