﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Newtonsoft.Json;

    /// <summary>
    /// Represents the settlement service providers that are available for fees.
    /// </summary>
    /// <remarks>See OPM 225610 for more details.</remarks>
    public class AvailableSettlementServiceProviders
    {
        /// <summary>
        /// Map from fee type id to all of the associated settlement service
        /// providers.
        /// </summary>
        [JsonProperty]
        private Dictionary<Guid, List<SettlementServiceProvider>> providersByFeeTypeId;

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="AvailableSettlementServiceProviders"/> class.
        /// </summary>
        public AvailableSettlementServiceProviders()
        {
            this.providersByFeeTypeId = new Dictionary<Guid, List<SettlementServiceProvider>>();
        }

        /// <summary>
        /// Adds the given fee type.
        /// </summary>
        /// <param name="feeTypeId">The fee type id to add.</param>
        public void AddFeeType(Guid feeTypeId)
        {
            if (!this.providersByFeeTypeId.ContainsKey(feeTypeId))
            {
                this.providersByFeeTypeId.Add(
                    feeTypeId,
                    new List<SettlementServiceProvider>());
            }
        }

        /// <summary>
        /// Removes the SSP with the given ID from this collection.
        /// </summary>
        /// <param name="providerId">The id of the SSP.</param>
        /// <returns>True if removed, false otherwise.</returns>
        public bool DeleteProvider(Guid providerId)
        {
            int totalCount = 0;
            foreach (var pair in this.providersByFeeTypeId)
            {
                totalCount += pair.Value.RemoveAll((ssp) => ssp.Id == providerId);
            }

            return totalCount != 0;
        }

        /// <summary>
        /// Retrieves the SSP with the given ID.
        /// </summary>
        /// <param name="providerid">The id of the SSP.</param>
        /// <returns>Returns the found SSP or null if not found.</returns>
        public SettlementServiceProvider RetrieveProvider(Guid providerid)
        {
            return this.providersByFeeTypeId.SelectMany((pair) => pair.Value).FirstOrDefault((ssp) => ssp.Id == providerid);
        }

        /// <summary>
        /// Adds a provider for a fee type id.
        /// </summary>
        /// <param name="feeTypeId">The fee type id.</param>
        /// <param name="provider">The provider to associate with the given fee type id.</param>
        public void AddProviderForFeeType(Guid feeTypeId, SettlementServiceProvider provider)
        {
            if (provider == null)
            {
                throw new ArgumentNullException("provider");
            }

            this.providersByFeeTypeId[feeTypeId].Add(provider);
        }

        /// <summary>
        /// Adds providers for a fee type id.
        /// </summary>
        /// <param name="feeTypeId">The fee type id.</param>
        /// <param name="providers">The providers to associate with the given fee type id.</param>
        public void AddProvidersForFeeType(Guid feeTypeId, IEnumerable<SettlementServiceProvider> providers)
        {
            if (providers == null)
            {
                throw new ArgumentNullException("providers");
            }

            this.providersByFeeTypeId[feeTypeId].AddRange(providers);
        }

        /// <summary>
        /// Gets all of the tracked fee type ids.
        /// </summary>
        /// <returns>The tracked fee type ids.</returns>
        public IEnumerable<Guid> GetFeeTypeIds()
        {
            return this.providersByFeeTypeId.Keys;
        }

        /// <summary>
        /// Checks if the fee type id is part of this provider collection.
        /// </summary>
        /// <param name="feeTypeId">The fee type id to check.</param>
        /// <returns>True if the fee type id is in the collection. False otherwise.</returns>
        public bool IsFeeTypeAvailableInProviderList(Guid feeTypeId)
        {
            return this.providersByFeeTypeId.ContainsKey(feeTypeId);
        }

        /// <summary>
        /// Gets the settlement service providers associated with the given fee type id.
        /// </summary>
        /// <param name="feeTypeId">The fee type id.</param>
        /// <returns>The settlement service providers associated with the given fee type id.</returns>
        public IEnumerable<SettlementServiceProvider> GetProvidersForFeeTypeId(Guid feeTypeId)
        {
            if (!this.providersByFeeTypeId.ContainsKey(feeTypeId))
            {
                return new List<SettlementServiceProvider>();
            }

            return this.providersByFeeTypeId[feeTypeId];
        }

        /// <summary>
        /// Returns all of the settlement service providers associated with any
        /// of the tracked fee types.
        /// </summary>
        /// <returns>
        /// All of the settlement service providers associated with any of the
        /// tracked fee types.
        /// </returns>
        public IEnumerable<SettlementServiceProvider> GetAllProviders()
        {
            return this.providersByFeeTypeId.Values.SelectMany(x => x);
        }

        /// <summary>
        /// Indicates whether every tracked fee type is associated with at least
        /// one settlement service provider.
        /// </summary>
        /// <returns>
        /// True if every tracked fee type has at least one provider associated
        /// with it.
        /// </returns>
        public bool HasAtLeastOneServiceProviderPerFeeType()
        {
            return this.providersByFeeTypeId.Values.All(l => l.Count > 0);
        }

        /// <summary>
        /// Determines whether an identical provider is already associated with the given fee type.
        /// </summary>
        /// <param name="feeTypeId">The fee type id.</param>
        /// <param name="provider">The settlement service provider.</param>
        /// <returns>
        /// True if there is an identical provider associated with the fee type. Otherwise, false.
        /// </returns>
        public bool IsProviderAssociatedWithFeeType(Guid feeTypeId, SettlementServiceProvider provider)
        {
            if (provider == null)
            {
                throw new ArgumentNullException("provider");
            }
            else if (!this.providersByFeeTypeId.ContainsKey(feeTypeId))
            {
                return false;
            }

            return this.providersByFeeTypeId[feeTypeId].Any(p => p.IsEqual(provider));
        }

        /// <summary>
        /// Updates the fee types that providers can be associated with. Any fee
        /// type ids that are not currently tracked will be added. Any fee
        /// type ids that are currently tracked but not in feeTypeIds will be
        /// removed, in addition to any service providers that were associated
        /// with those fee type ids.
        /// </summary>
        /// <param name="feeTypeIdsToTrack">The fee type ids to track.</param>
        public void OnlyTrackFeeTypeIds(IEnumerable<Guid> feeTypeIdsToTrack)
        {
            this.AddMissingFeeTypeIds(feeTypeIdsToTrack);

            this.RemoveUnneededFeeTypeIds(feeTypeIdsToTrack);
        }

        /// <summary>
        /// Validates these settlement service providers against the old list.
        /// </summary>
        /// <param name="errorMessage">Any errors during validation.</param>
        /// <returns>True if validated, false otherwise.</returns>
        public bool ValidateSSPs(out string errorMessage)
        {
            string emptyFieldError = "Empty value for field {0}.";
            foreach (var ssp in this.GetAllProviders())
            {
                if (string.IsNullOrEmpty(ssp.CompanyName))
                {
                    errorMessage = string.Format(emptyFieldError, "Company Name");
                    return false;
                }

                if (string.IsNullOrEmpty(ssp.StreetAddress))
                {
                    errorMessage = string.Format(emptyFieldError, "Street Address");
                    return false;
                }

                if (string.IsNullOrEmpty(ssp.City))
                {
                    errorMessage = string.Format(emptyFieldError, "City");
                    return false;
                }

                if (string.IsNullOrEmpty(ssp.State))
                {
                    errorMessage = string.Format(emptyFieldError, "State");
                    return false;
                }

                if (string.IsNullOrEmpty(ssp.Zip))
                {
                    errorMessage = string.Format(emptyFieldError, "Zip");
                    return false;
                }

                if (string.IsNullOrEmpty(ssp.Phone))
                {
                    errorMessage = string.Format(emptyFieldError, "Phone");
                    return false;
                }
            }

            errorMessage = string.Empty;
            return true;
        }

        /// <summary>
        /// Adds fee type ids that were not already being tracked.
        /// </summary>
        /// <param name="feeTypeIdsToTrack">The fee type ids to track.</param>
        private void AddMissingFeeTypeIds(IEnumerable<Guid> feeTypeIdsToTrack)
        {
            var feeTypeIdsToAdd = feeTypeIdsToTrack
                .Where(id => !this.providersByFeeTypeId.ContainsKey(id))
                .ToList();

            foreach (var feeTypeId in feeTypeIdsToAdd)
            {
                this.AddFeeType(feeTypeId);
            }
        }

        /// <summary>
        /// Removes any fee type ids and associated service providers that no
        /// longer need to be tracked.
        /// </summary>
        /// <param name="feeTypeIdsToTrack">The fee type ids that need to be tracked.</param>
        private void RemoveUnneededFeeTypeIds(IEnumerable<Guid> feeTypeIdsToTrack)
        {
            var feeTypeIdsToRemove = this.providersByFeeTypeId.Keys
                .Except(feeTypeIdsToTrack)
                .ToList();

            foreach (var typeIdToRemove in feeTypeIdsToRemove)
            {
                this.providersByFeeTypeId.Remove(typeIdToRemove);
            }
        }
    }
}
