
using LendersOffice.Common;
/// Author: David Dao
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataAccess
{
    public class FannieMaeArmPlanTable
    {
        // 11/15/2007 dd - Data is pull from https://www.efanniemae.com/sf/technology/support/integration/integguide/resources/
        // 06/28/2016 mf - Data is pulled from case 238055.  This list was
        // brought up to date to what is currently offered.
        private static readonly IEnumerable<ArmPlan> s_fannieArmPlanTable =
            ArmPlan.GetArmPlanCodes();

        public static string GetPlanCode(string description) 
        {
            foreach ( ArmPlan plan in s_fannieArmPlanTable)
            {
                if (String.Equals(plan.Description, description, StringComparison.OrdinalIgnoreCase))
                {
                    return plan.Code;
                }
            }    
             
            return description; // Could not find predefine description. Return as-is.
        }

        public static string GetDescription(string planCode) 
        {
            foreach (ArmPlan plan in s_fannieArmPlanTable)
            {
                if (String.Equals(plan.Code, planCode, StringComparison.OrdinalIgnoreCase))
                {
                    return plan.Description;
                }
            }
            
            return planCode;
        }

        public static void BindToComboBox(MeridianLink.CommonControls.ComboBox cb) 
        {
            foreach (var plan in s_fannieArmPlanTable.Where(plan => plan.IsActive))
            {
                cb.Items.Add(plan.Description);
            }
        }

        public static string CalculatePlanCode(
            E_sLT sLT,                  // Loan Type
            int sRAdj1stCapMon,         // 1st Change/Initial FR Int Per
            decimal sRAdjCapR,          // Annual Cap/Subseq Change Limit
            E_sArmIndexT sArmIndexT,    // Index
            int sRAdjCapMon,            // Subseq IR Adj
            decimal sRAdj1stCapR,       // First Change Limit
            decimal sRAdjLifeCapR,      // Life IR Cap
            bool sIsConvertibleMortgage // Convert
            )
        {
            return ArmPlan.CalculatePlanCode(
                    sLT,
                    sRAdj1stCapMon,
                    sRAdjCapR,
                    sArmIndexT,
                    sRAdjCapMon,
                    sRAdj1stCapR,
                    sRAdjLifeCapR,
                    sIsConvertibleMortgage,
                    s_fannieArmPlanTable);
        }
    }
}
