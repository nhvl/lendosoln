﻿using System;

namespace DataAccess
{
    public class CGetLoanProgramsToRunLpeData : CPageData
    {
        private static CSelectStatementProvider s_selectProvider;

        static CGetLoanProgramsToRunLpeData()
        {
            StringList list = new StringList();

            list.Add("sfGetLoanProgramsToRunLpe");
            list.Add("sProdLpePriceGroupId");
            list.Add("sInvestorLockLpePriceGroupId");
            list.Add("sfValidateForRunningPricing");
            list.Add("sfTransformDataToPml");
            list.Add("sfGetLoanProgramsToRunInternalPricing");
            list.Add("sOriginatorCompensationPaymentSourceT");
            list.Add("sIsQualifiedForOriginatorCompensation");
            list.Add("sOriginatorCompensationLenderFeeOptionT");
            list.Add("sfApplyCompensationFromCurrentOriginator");
            list.Add("sHasOriginatorCompensationPlan");
            list.Add("sAllAppsHasCreditReportOnFile");
            list.Add("sfRecalculateClosingFeeConditions");
            list.Add("sBranchChannelT");
            list.Add("sLpTemplateId");
            list.Add("sfGetLoanProgramsToRunForHistoricalPricing");

            s_selectProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection);

        }
        public CGetLoanProgramsToRunLpeData(Guid sLId)
            : base(sLId, "CGetLoanProgramsToRunLpeData", s_selectProvider)
        {
        }
    }
}
