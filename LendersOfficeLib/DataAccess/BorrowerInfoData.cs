using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.UI;

namespace DataAccess
{
	public class CBorrowerInfoData : CPageData
	{
		private static CSelectStatementProvider s_selectProvider;
		static CBorrowerInfoData()
		{
			StringList list = new StringList();

            list.Add("aAppNm" );
            list.Add("aBAddr" );
            list.Add("aBAddrMail" );
            list.Add("aBAddrMailUsePresentAddr" );
            list.Add("aBAddrMailSourceT");
            list.Add("aBAddrT" );
            list.Add("aBAddrYrs" );
            list.Add("aBAge" );
            list.Add("aBBusPhone" );
            list.Add("aBCellPhone" );
            list.Add("aBCity" );
            list.Add("aBCityMail" );
            list.Add("aBDependAges" );
            list.Add("aBDependNum" );
            list.Add("aBDob" );
            list.Add("aBEmail" );
            list.Add("aBEmpCollection" );
            list.Add("aBFirstNm" );
            list.Add("aBHPhone" );
            list.Add("aBLastNm" );
            list.Add("aBTypeT");
            list.Add("aBMaritalStatT" );
            list.Add("aBMidNm" );
            list.Add("aBNm" );
            list.Add("aBPrev1Addr" );
            list.Add("aBPrev1AddrT" );
            list.Add("aBPrev1AddrYrs" );
            list.Add("aBPrev1City" );
            list.Add("aBPrev1State" );
            list.Add("aBPrev1Zip" );
            list.Add("aBPrev2Addr" );
            list.Add("aBPrev2AddrT" );
            list.Add("aBPrev2AddrYrs" );
            list.Add("aBPrev2City" );
            list.Add("aBPrev2State" );
            list.Add("aBPrev2Zip" );
            list.Add("aBSchoolYrs" );
            list.Add("aBSsn" );
            list.Add("aBState" );
            list.Add("aBStateMail" );
            list.Add("aBSuffix" );
            list.Add("aBZip" );
            list.Add("aBZipMail" );
            list.Add("aCAddr" );
            list.Add("aCAddrMail" );
            list.Add("aCAddrMailUsePresentAddr" );
            list.Add("aCAddrMailSourceT");
            list.Add("aCAddrT" );
            list.Add("aCAddrYrs" );
            list.Add("aCAge" );
            list.Add("aCBusPhone" );
            list.Add("aCCellPhone" );
            list.Add("aCCity" );
            list.Add("aCCityMail" );
            list.Add("aCDependAges" );
            list.Add("aCDependNum" );
            list.Add("aCDob" );
            list.Add("aCEmail" );
            list.Add("aCEmpCollection" );
            list.Add("aCFirstNm" );
            list.Add("aCHPhone" );
            list.Add("aCLastNm" );
            list.Add("aCTypeT");
            list.Add("aCMaritalStatT" );
            list.Add("aCMidNm" );
            list.Add("aCNm" );
            list.Add("aCPrev1Addr" );
            list.Add("aCPrev1AddrT" );
            list.Add("aCPrev1AddrYrs" );
            list.Add("aCPrev1City" );
            list.Add("aCPrev1State" );
            list.Add("aCPrev1Zip" );
            list.Add("aCPrev2Addr" );
            list.Add("aCPrev2AddrT" );
            list.Add("aCPrev2AddrYrs" );
            list.Add("aCPrev2City" );
            list.Add("aCPrev2State" );
            list.Add("aCPrev2Zip" );
            list.Add("aCSchoolYrs" );
            list.Add("aCSsn" );
            list.Add("aCState" );
            list.Add("aCStateMail" );
            list.Add("aCSuffix" );
            list.Add("aCZip" );
            list.Add("aCZipMail" );
            list.Add("aIsPrimary" );
            list.Add("aOccT");
            list.Add("aVorCollection" );
            list.Add("sfGetAgentOfRole");
            list.Add("sLenderNumVerif" );
            list.Add("sOpenedD" );
            list.Add("sPreparerXmlContent" );
            list.Add("sSpAddr" );
            list.Add("sSpCity" );
            list.Add("sSpState" );
            list.Add("sSpZip" );
            list.Add("aCEmplrBusPhoneLckd");
            list.Add("aBEmplrBusPhoneLckd");
            list.Add("aBBusPhone");
            list.Add("aCBusPhone");
            list.Add("aBFax");
            list.Add("aCFax");
            list.Add("aLiaCollection");
            list.Add("aCrOd");
            list.Add("sLpqLoanNumber");
            list.Add("aLqiCrOd");

            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

		}

        public CBorrowerInfoData( Guid fileId ) : base( fileId , "BorrowerInfo" , s_selectProvider )
        {
        }

	}
}