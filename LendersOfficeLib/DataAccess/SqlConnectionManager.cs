namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using CommonProjectLib.Database;
    using System.Data.Common;
    using System.Data.SqlClient;

    public sealed class SqlConnectionManager : ISqlConnectionManager
    {

        private DbConnectionInfo m_connectionInfo = null;

        private SqlConnectionManager(Guid brokerId)
        {
            m_connectionInfo = DbConnectionInfo.GetConnectionInfo(brokerId);
        }

        private SqlConnectionManager(DbConnectionInfo connInfo)
        {
            m_connectionInfo = connInfo;
        }

        #region ISqlConnectionManager Members

        public DbConnection GetConnection()
        {
            return m_connectionInfo.GetConnection();
        }

        public string ConnectionName
        {
            get
            {
                return m_connectionInfo.FriendlyDisplay;
            }

        }

        public ISqlConnectionManager GetDefaultConnectionManager()
        {
            return new SqlConnectionManager(DbConnectionInfo.DefaultConnectionInfo);
        }

        public static SqlConnectionManager Get(Guid brokerId)
        {
            return new SqlConnectionManager(brokerId);
        }

        public static SqlConnectionManager Get(DbConnectionInfo connInfo)
        {
            return new SqlConnectionManager(connInfo);
        }

        #endregion
    }
}
