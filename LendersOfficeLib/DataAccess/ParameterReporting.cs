﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Linq;
    using System.Web;
    using ConfigSystem;
    using DataAccess.PathDispatch;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.ManualInvestorProductExpiration;
    using LendersOffice.ObjLib.Extensions;
    using LendersOffice.ObjLib.Regions;
    using LendersOffice.QueryProcessor;
    using LendersOffice.Reports;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes.PathDispatch;

    public class ParameterReporting
    {
        private const string SP_FETCH_ALL_CATEGORIES = "TASK_Categories_FetchAll";
                                                                    
        /// <summary>
        /// Should be initialize by the construnctor and not touched again for writing again.
        /// DO not manipulate this dictionary. I want to create a read only dictionary class for this eventually.
        /// </summary>
        private static readonly LqbSingleThreadInitializeLazy<IDictionary<string,SecurityParameter.Parameter>> x_commonSystemFields;

        #region Properties
        private  Dictionary<string, SecurityParameter.Parameter> m_parameters;
        /// <summary>
        /// much slower than Parameters, due to sorting.
        /// </summary>
        public IEnumerable<SecurityParameter.Parameter> ParametersSorted
        {
            get
            {
                return m_parameters.Values.OrderBy(p => p.CategoryName).ThenBy(p => p.FriendlyName);
            }
        }
        /// <summary>
        /// faster than ParametersSorted.
        /// </summary>
        public IEnumerable<SecurityParameter.Parameter> Parameters
        {
            get
            {
                return m_parameters.Values;
            }
        }

        private Guid m_brokerId;

        /// <summary>
        /// Tries to get the parameter with the given value. 
        /// </summary>
        /// <param name="id">The parameter id to lookup.</param>
        /// <param name="p">The parameter that will be returned.</param>
        /// <returns>True if parameter is found false otherwise.</returns>
        public bool TryGetParameter(string id, out SecurityParameter.Parameter p)
        {
            p = this[id];
            return p != null;
        }

        public SecurityParameter.Parameter this[string id]
        {
            get
            {
                SecurityParameter.Parameter param;
                m_parameters.TryGetValue(id, out param);
                if( param == null ) 
                {
                    if (id.Equals("csLpProductType", StringComparison.OrdinalIgnoreCase) || 
                        id.Equals("csLpInvestorNm", StringComparison.OrdinalIgnoreCase))
                    {
                        if (LoadInvestorValues())
                        {
                            m_parameters.TryGetValue(id, out param);
                        }
                    }
                    else if (id.Equals("csBranchNm", StringComparison.OrdinalIgnoreCase))
                    {
                        LoadBranchList();
                        m_parameters.TryGetValue(id, out param);
                    }
                    else if (id.Equals("csLeadSrcId", StringComparison.OrdinalIgnoreCase))
                    {
                        LoadLeadSources();
                        m_parameters.TryGetValue(id, out param);
                    }
                    else if (id.Equals("csPmlCompanyTierId", StringComparison.OrdinalIgnoreCase))
                    {
                        LoadPmlCompanyTiers();
                        m_parameters.TryGetValue(id, out param);
                    }
                    else if (id.Equals("csRegion", StringComparison.OrdinalIgnoreCase))
                    {
                        LoadRegions();
                        m_parameters.TryGetValue(id, out param);
                    }
                }
                return param;
            }
        }

        private List<string> x_categories = null;
        public List<string> Categories
        {
            get
            {
                if (x_categories == null)
                {
                    x_categories = BuildCategoryList();
                }
                return x_categories;
            }
        }
        #endregion

        #region Constructors


        static ParameterReporting()
        {
            x_commonSystemFields = new LqbSingleThreadInitializeLazy<IDictionary<string, SecurityParameter.Parameter>>(
                ParameterReporting.BuildSharedFieldDictionary);
        }

        //in case we want to cache.. since we already cache the common system fields dont think we need to do this.
        public static ParameterReporting RetrieveByBrokerId(Guid brokerId)
        {
            if (HttpContext.Current == null)
            {
                return new ParameterReporting(brokerId);
            }

            string key = "PRA-" + brokerId;
            if (HttpContext.Current.Items.Contains(key))
            {
                return (ParameterReporting)HttpContext.Current.Items[key];
            }

            ParameterReporting p = new ParameterReporting(brokerId);
            HttpContext.Current.Items.Add(key, p);
            return p;
        }

        public static ParameterReporting RetrieveSystemParameters()
        {
            return RetrieveByBrokerId(Guid.Empty);
        }

        public ParameterReporting(Guid brokerId)
        {
            //perform shallow copy from shared fields this is thread safe since we are only reading the values
            //enumerating is thread safe? Need to check
            m_parameters = new Dictionary<string, SecurityParameter.Parameter>(x_commonSystemFields.Value, StringComparer.OrdinalIgnoreCase);

            // Load up the lender's group information and any custom fields.
            this.LoadCustomBrokerFields(brokerId);

            // OPM 457365 - Loads parameters for pathable objects.
            this.LoadLoanFilePaths(brokerId);

            m_brokerId = brokerId;
        }

        #endregion

        #region Public Methods
        public void UpdateCustomList(ICollection<CustomVariable> newCustomFields)
        {
            UpdateCustomList(newCustomFields, new Dictionary<string, SecurityParameter.E_FieldSource>());
        }

        public void UpdateCustomList(ICollection<CustomVariable> newCustomFields, Dictionary<string, SecurityParameter.E_FieldSource> fieldSources )
        {

            foreach (var newField in newCustomFields)
            {
                SecurityParameter.Parameter param = new SecurityParameter.Parameter();
                param.CategoryName = newField.Scope == CustomVariable.E_Scope.System ? "System Triggers" : "Lender Triggers";
                param.Scope = newField.Scope;
                param.FriendlyName = newField.Name;
                param.Id = newField.Name; // The custom variable Id is not relevant here; use Name
                param.Type = SecurityParameter.SecurityParameterType.Custom;
                m_parameters[param.Id] = param;
                if (fieldSources.ContainsKey(newField.Name))
                {
                    param.Source = fieldSources[newField.Name];
                }
                else
                {
                    param.Source = SecurityParameter.E_FieldSource.CompositeField;
                }
            }

            if (newCustomFields.Count == 0)
            {
                x_categories = BuildCategoryList();
            }

        }

        public SecurityParameter.Parameter GetSecurityParameterWithValues(string fieldName)
        {
            if (fieldName.StartsWith(DataPath.PathHeader, StringComparison.OrdinalIgnoreCase))
            {
                return GetSecurityParameterWithValuesByPath(fieldName);
            }
            else
            {
                return GetSecurityParameterWithValuesByFieldName(fieldName);
            }
        }
        #endregion

        #region Private Methods

        Dictionary<string, HashSet<string>> customVariables = new Dictionary<string, HashSet<string>>(StringComparer.OrdinalIgnoreCase);
        Dictionary<string, SecurityParameter.E_FieldSource> m_fieldStatus = new Dictionary<string, SecurityParameter.E_FieldSource>();

        /// <summary>
        /// For the closing cost auomation we are making branch a field to be used in the condition. This field will only be used in that UI.
        /// So it will be loaded on demand.  OPM 54598
        /// </summary>
        private void LoadBranchList()
        {
            //May want to change this to use a different store procedure since we only need the name 
            var branches = BranchDB.GetBranchObjects(m_brokerId);

            SecurityParameter.EnumeratedParmeter p = new SecurityParameter.EnumeratedParmeter();
            p.BaseType = SecurityParameter.EnumParamType.String;
            p.CategoryName = "misc";
            p.EnumMapping = new List<SecurityParameter.EnumMapping>();
            p.FriendlyName = "Branch";
            p.Id = "csBranchNm";
            p.Source = SecurityParameter.E_FieldSource.CustomReport;
            p.Type = SecurityParameter.SecurityParameterType.Enum;


            HashSet<string> branchNames = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            foreach (BranchDB branch in branches)
            {
                if (branchNames.Add(branch.Name)) // just in case want to make sure are not adding duplicates
                {
                    p.EnumMapping.Add(new SecurityParameter.EnumMapping() { FriendlyValue = branch.Name, RepValue = branch.BranchID.ToString("N") });
                }
            }

            m_parameters.Add(p.Id, p);
        }

        /// <summary>
        /// Load the non-deleted lead sources for the broker.
        /// </summary>
        /// <remarks>
        /// Loaded on demand because this was added for the Fee Service.
        /// opm 172380 gf
        /// </remarks>
        private void LoadLeadSources()
        {
            var enumeratedParameter = new SecurityParameter.EnumeratedParmeter();
            enumeratedParameter.BaseType = SecurityParameter.EnumParamType.Int;
            enumeratedParameter.CategoryName = "misc";
            enumeratedParameter.EnumMapping = new List<SecurityParameter.EnumMapping>();
            enumeratedParameter.FriendlyName = "Lead Source";
            enumeratedParameter.Id = "csLeadSrcId";
            enumeratedParameter.Source = SecurityParameter.E_FieldSource.SpecialField;
            enumeratedParameter.Type = SecurityParameter.SecurityParameterType.Enum;

            // Default value is 0, this isn't configured at the lender level.
            enumeratedParameter.EnumMapping.Add(new SecurityParameter.EnumMapping()
            {
                FriendlyValue = ConstApp.SystemDefualtBlankLeadSourceName + ": " + ConstApp.SystemDefaultBlankLeadSourceId.ToString("D"),
                RepValue = ConstApp.SystemDefaultBlankLeadSourceId.ToString("D")
            });

            var broker = BrokerDB.RetrieveById(m_brokerId);
            var leadSources = broker.LeadSources;
            foreach (var leadSource in leadSources)
            {
                // The UI tries to enforce that the name is required, but if any
                // blank names make it into the lead source list we do not want
                // to include them here. Blank lead sources are misleading because
                // they will display an ID value in the config page which does not 
                // match the data-layer value of 0 for blank. This would be especially
                // problematic in the fee service because we supply the "No selection"
                // option for blanks by default. 5/7/2014 gf
                if (!leadSource.Deleted && leadSource.Name != null && !string.IsNullOrEmpty(leadSource.Name.TrimWhitespaceAndBOM()))
                {
                    var leadSourceIdRep = leadSource.Id.ToString("D");
                    enumeratedParameter.EnumMapping.Add(new SecurityParameter.EnumMapping()
                    {
                        FriendlyValue = leadSource.Name + ": " + leadSourceIdRep,
                        RepValue = leadSourceIdRep
                    });
                }
            }
            m_parameters.Add(enumeratedParameter.Id, enumeratedParameter);
        }

        /// <summary>
        /// Load the non-deleted originating company tiers for the broker.
        /// </summary>
        /// <remarks>
        /// Loaded on demand because this was added for the Fee Service.
        /// opm 190542 je - cloned from LoadLeadSources()
        /// </remarks>
        private void LoadPmlCompanyTiers()
        {
            var enumeratedParameter = new SecurityParameter.EnumeratedParmeter();
            enumeratedParameter.BaseType = SecurityParameter.EnumParamType.Int;
            enumeratedParameter.CategoryName = "misc";
            enumeratedParameter.EnumMapping = new List<SecurityParameter.EnumMapping>();
            enumeratedParameter.FriendlyName = "Originating Company Tier";
            enumeratedParameter.Id = "csPmlCompanyTierId";
            enumeratedParameter.Source = SecurityParameter.E_FieldSource.SpecialField;
            enumeratedParameter.Type = SecurityParameter.SecurityParameterType.Enum;

            // Default value is 0, this isn't configured at the lender level.
            enumeratedParameter.EnumMapping.Add(new SecurityParameter.EnumMapping()
            {
                FriendlyValue = ConstApp.SystemDefualtBlankPmlCompanyTierName + ": " + ConstApp.SystemDefaultBlankPmlCompanyTierId.ToString("D"),
                RepValue = ConstApp.SystemDefaultBlankPmlCompanyTierId.ToString("D")
            });

            var broker = BrokerDB.RetrieveById(m_brokerId);
            var pmlCompanyTiers = broker.PmlCompanyTiers;
            foreach (var pmlCompanyTier in pmlCompanyTiers)
            {
                // The UI tries to enforce that the name is required, but if any
                // blank names make it into the originating company tier list we do not want
                // to include them here. Blank originating company tiers are misleading because
                // they will display an ID value in the config page which does not 
                // match the data-layer value of 0 for blank. This would be especially
                // problematic in the fee service because we supply the "No selection"
                // option for blanks by default. 10/29/2014 je
                if (!pmlCompanyTier.Deleted && pmlCompanyTier.Name != null && !string.IsNullOrEmpty(pmlCompanyTier.Name.TrimWhitespaceAndBOM()))
                {
                    var pmlCompanyTierIdRep = pmlCompanyTier.Id.ToString("D");
                    enumeratedParameter.EnumMapping.Add(new SecurityParameter.EnumMapping()
                    {
                        FriendlyValue = pmlCompanyTier.Name + ": " + pmlCompanyTierIdRep,
                        RepValue = pmlCompanyTierIdRep
                    });
                }
            }
            m_parameters.Add(enumeratedParameter.Id, enumeratedParameter);
        }

        /// <summary>
        /// Load Regions for the broker.
        /// </summary>
        /// <remarks>
        /// OPM 449923.
        /// We will make use of nested parameters to implement the regions parameter list.
        /// The top layer will be a list of region sets, with the sub layer being the list of regions.
        /// </remarks>
        private void LoadRegions()
        {
            var regionSets = RegionSet.GetAllRegionSets(m_brokerId);

            SecurityParameter.NestedEnumeratedParmeter p = new SecurityParameter.NestedEnumeratedParmeter();
            p.BaseType = SecurityParameter.EnumParamType.Int;
            p.CategoryName = "misc";
            p.EnumMapping = new List<SecurityParameter.EnumMapping>();
            p.SubParameters = new List<SecurityParameter.EnumeratedParmeter>();
            p.FriendlyName = "Region";
            p.Id = "csRegion";
            p.Source = SecurityParameter.E_FieldSource.SpecialField;
            p.Type = SecurityParameter.SecurityParameterType.Enum;

            foreach (RegionSet regionSet in regionSets.Values)
            {
                // Add enum mapping for this region set.
                p.EnumMapping.Add(new SecurityParameter.EnumMapping() { FriendlyValue = regionSet.Name, RepValue = regionSet.Id.ToString() });

                // Create subset parameter for region set.
                SecurityParameter.EnumeratedParmeter sub = new SecurityParameter.EnumeratedParmeter();
                sub.BaseType = SecurityParameter.EnumParamType.Int;
                sub.CategoryName = "misc";
                sub.EnumMapping = new List<SecurityParameter.EnumMapping>();
                sub.FriendlyName = regionSet.Name;
                sub.Id = regionSet.Id.ToString();
                sub.Source = SecurityParameter.E_FieldSource.SpecialField;
                sub.Type = SecurityParameter.SecurityParameterType.Enum;

                foreach (Region region in regionSet.Regions.Values)
                {
                    // Add enum mapping for region.
                    sub.EnumMapping.Add(new SecurityParameter.EnumMapping() { FriendlyValue = region.Name, RepValue = region.Id.ToString() });
                }

                p.SubParameters.Add(sub);
            }

            m_parameters.Add(p.Id, p);
        }

        private bool LoadInvestorValues()
        {
            if (this.m_brokerId == Guid.Empty)
            {
                return false;
            }
            var broker = BrokerDB.RetrieveById(m_brokerId);
            List<string> list = InvestorProductManager.RetrieveInvestors(broker);

            SecurityParameter.EnumeratedParmeter p = new SecurityParameter.EnumeratedParmeter();
            p.BaseType = SecurityParameter.EnumParamType.String;
            p.CategoryName = "misc";
            p.EnumMapping = new List<SecurityParameter.EnumMapping>();
            p.FriendlyName = "Investor Name";
            p.Id = "csLpInvestorNm";
            p.Source = SecurityParameter.E_FieldSource.CustomReport;
            p.Type = SecurityParameter.SecurityParameterType.Enum;

            foreach (string item in list)
            {
                p.EnumMapping.Add(new SecurityParameter.EnumMapping() { FriendlyValue = item, RepValue = item });
            }

            m_parameters.Add(p.Id, p);
            list = InvestorProductManager.RetrieveProductTypes(m_brokerId); 

            p = new SecurityParameter.EnumeratedParmeter();
            p.BaseType = SecurityParameter.EnumParamType.String;
            p.CategoryName = "misc";
            p.EnumMapping = new List<SecurityParameter.EnumMapping>();
            p.FriendlyName = "Loan Product Type";
            p.Id = "csLpProductType";
            p.Source = SecurityParameter.E_FieldSource.CustomReport;
            p.Type = SecurityParameter.SecurityParameterType.Enum;

            foreach (string item in list)
            {
                p.EnumMapping.Add(new SecurityParameter.EnumMapping() { FriendlyValue = item, RepValue = item });
            }

            m_parameters.Add(p.Id, p);

            return true;
        }

        private void LoadDefaultGroups()
        {
            // 11/2/2010 dd - When edit System Configuration we return a list of hard code
            // User Group, Branch Group and OC Group.

            #region Populate Default User Group
            SecurityParameter.EnumeratedParmeter param = new SecurityParameter.EnumeratedParmeter()
            {
                CategoryName = "Groups",
                FriendlyName = "Employee Group",
                Id = "UserGroup",
                Type = SecurityParameter.SecurityParameterType.Enum,
                BaseType = SecurityParameter.EnumParamType.String,
                Source = SecurityParameter.E_FieldSource.UserGroup
            };
            param.EnumMapping = new List<SecurityParameter.EnumMapping>();
            param.EnumMapping.Add(new SecurityParameter.EnumMapping() { FriendlyValue = "Delete Loan", RepValue = "Delete Loan"});
            param.EnumMapping.Add(new SecurityParameter.EnumMapping() { FriendlyValue = "DO_NOT_USE", RepValue = "DO_NOT_USE" });
            m_parameters.Add(param.Id, param);
            #endregion

        }
        public const string LEAD_SOURCE_ENUM_FIELD_ID = "sLeadSrcIdEnum";
        public const string LEAD_SOURCE_ENUM_REMAP_ID = "sLeadSrcId";

        public const string STATUS_PROGRESS_ENUM_FIELD_ID = "sStatusProgressT";
        public const string STATUS_PROGRESS_ENUM_REMAP_ID = "sStatusT";

        public const string GenerateDocsPackageTypeFieldId = "GenerateDocs_PackageType";

        public const string GenerateDocsVendorPackageIdPrefix = "GenerateDocs_VendorPackageId_";
        public const string GenerateDocsVendorPackageIdFieldId = "GenerateDocs_VendorPackageId";
        
        private void LoadCustomBrokerFields(Guid brokerId)
        {
            if (brokerId == ConstAppDavid.SystemBrokerGuid || brokerId == Guid.Empty)
            {
                LoadDefaultGroups();
                return;
            }

            BrokerDB db = BrokerDB.RetrieveById(brokerId);

            SecurityParameter.EnumeratedParmeter leadSourceParam = new SecurityParameter.EnumeratedParmeter()
            {
                CategoryName = string.Empty,
                FriendlyName = "Lead Source Enumeration",
                Id = LEAD_SOURCE_ENUM_FIELD_ID,
                Scope = CustomVariable.E_Scope.Lender,
                Source = SecurityParameter.E_FieldSource.LoanOrApp, 
                Type = SecurityParameter.SecurityParameterType.Enum,
                BaseType = SecurityParameter.EnumParamType.Int
            };

            this.m_parameters.Add(LEAD_SOURCE_ENUM_FIELD_ID, leadSourceParam);

            leadSourceParam.EnumMapping = new List<SecurityParameter.EnumMapping>();
            foreach (LeadSource source in db.LeadSources)
            {
                string desc = source.Name;

                if (source.Deleted)
                {
                    desc = string.Concat(desc, " (Deleted)");
                }

                leadSourceParam.EnumMapping.Add(new SecurityParameter.EnumMapping()
                {
                    FriendlyValue = desc,
                    RepValue = source.Id.ToString()
                });
            }


            // Branch Groups
            IEnumerable<Group> groupList = GroupDB.GetAllGroups(brokerId, GroupType.Branch);

            if (groupList.Count() > 0)
            {
                SecurityParameter.EnumeratedParmeter param = new SecurityParameter.EnumeratedParmeter()
                {
                    CategoryName = "Groups",
                    FriendlyName = "Branch Group",
                    Id = "BranchGroup",
                    Type = SecurityParameter.SecurityParameterType.Enum,
                    BaseType = SecurityParameter.EnumParamType.String
                };

                param.EnumMapping = new List<SecurityParameter.EnumMapping>(groupList.Count());

                foreach (var group in groupList)
                {
                    param.EnumMapping.Add(
                        new SecurityParameter.EnumMapping()
                        {
                            FriendlyValue = group.GroupName,
                            RepValue = group.GroupName
                        });
                }
                m_parameters.Add(param.Id, param);

            }

            // Originating Company Groups

            groupList = GroupDB.GetAllGroups(brokerId, GroupType.PmlBroker);
            if (groupList.Count() > 0)
            {
                SecurityParameter.EnumeratedParmeter param = new SecurityParameter.EnumeratedParmeter()
                {
                    CategoryName = "Groups",
                    FriendlyName = "Originating Company Group",
                    Id = "OCGroup",
                    Type = SecurityParameter.SecurityParameterType.Enum,
                    BaseType = SecurityParameter.EnumParamType.String
                };
                param.Source = SecurityParameter.E_FieldSource.UserGroup;
                param.EnumMapping = new List<SecurityParameter.EnumMapping>(groupList.Count());

                foreach (var group in groupList)
                {
                    param.EnumMapping.Add(
                        new SecurityParameter.EnumMapping()
                        {
                            FriendlyValue = group.GroupName,
                            RepValue = group.GroupName
                        });
                }

                m_parameters.Add(param.Id, param);
                
            }

            // Employee Groups

            groupList = GroupDB.GetAllGroups(brokerId, GroupType.Employee);
            if (groupList.Count() > 0)
            {
                SecurityParameter.EnumeratedParmeter param = new SecurityParameter.EnumeratedParmeter()
                {
                    CategoryName = "Groups",
                    FriendlyName = "Employee Group",
                    Id = "UserGroup",
                    Type = SecurityParameter.SecurityParameterType.Enum,
                    BaseType = SecurityParameter.EnumParamType.String
                };

                param.Source = SecurityParameter.E_FieldSource.UserGroup;
                param.EnumMapping = new List<SecurityParameter.EnumMapping>(groupList.Count());

                foreach (var group in groupList)
                {
                    param.EnumMapping.Add(
                        new SecurityParameter.EnumMapping()
                        {
                            FriendlyValue = group.GroupName,
                            RepValue = group.GroupName
                        });
                }
                m_parameters.Add(param.Id, param);
            }

            // Vendors
            List<DocumentVendorBrokerSettings> vendorSettings = DocumentVendorBrokerSettings.ListAllForBroker(brokerId);

            foreach (DocumentVendorBrokerSettings settings in vendorSettings)
            {
                if (settings.VendorId == Guid.Empty)
                {
                    continue;
                }

                VendorConfig config = VendorConfig.Retrieve(settings.VendorId);

                // OPM 474716 - Parameter IDs need to be unique, but vendor names and service link are not.
                // We need to check for uniqueness, and make the IDs unique if they are not.
                string friendlyIdName;
                string serviceLinkName = System.Text.RegularExpressions.Regex.Replace(settings.ServicesLinkNm, "[^a-zA-Z0-9]", string.Empty);
                string vendorName = System.Text.RegularExpressions.Regex.Replace(config.VendorName, "[^a-zA-Z0-9]", string.Empty);
                if (!m_parameters.ContainsKey($"{GenerateDocsVendorPackageIdPrefix}{serviceLinkName}"))
                {
                    friendlyIdName = serviceLinkName;
                }
                else if (!m_parameters.ContainsKey($"{GenerateDocsVendorPackageIdPrefix}{vendorName}"))
                {
                    friendlyIdName = vendorName;
                }
                else
                {
                    string IdLastFour = config.VendorId.ToString("N").Substring(28);
                    friendlyIdName = $"{serviceLinkName}_{IdLastFour}";
                }

                SecurityParameter.EnumeratedParmeter param = new SecurityParameter.EnumeratedParmeter()
                {
                    CategoryName = "Generate Docs",
                    FriendlyName = $"{friendlyIdName}PackageId",
                    Id = $"{GenerateDocsVendorPackageIdPrefix}{friendlyIdName}",
                    Type = SecurityParameter.SecurityParameterType.Enum,
                    BaseType = SecurityParameter.EnumParamType.String,
                    EnumMapping = config.GetPackageDataForBroker(brokerId).Values
                        .Select(package => new SecurityParameter.EnumMapping() { FriendlyValue = package.Name, RepValue = $"{config.VendorId}_{package.Name}" })
                        .ToList(),
                    Source = SecurityParameter.E_FieldSource.SpecialField
                };

                m_parameters.Add(param.Id, param);
            }
        }

        /// <summary>
        /// Generates security parameters for <see cref="IPathResolvable"/> memebers of the loan file.
        /// </summary>
        /// <remarks>
        /// Only generates parameters for those <see cref="IPathResolvable"/> members with a schema.
        /// </remarks>
        /// <param name="brokerId">The broker ID.</param>
        private void LoadLoanFilePaths(Guid brokerId)
        {
            IEnumerable<PathableFieldInfo> lffPathableFields = PathSchemaManager.GetSchemaByType(typeof(CPageData)).GetFields().Values.Where(
                f => f.ParameterType == SecurityParameter.SecurityParameterType.PathableRecord
                || f.ParameterType == SecurityParameter.SecurityParameterType.PathableCollection);

            foreach (PathableFieldInfo fieldInfo in lffPathableFields)
            {
                SecurityParameter.Parameter parameter = CreatePathableRecordParameter(brokerId, fieldInfo, m_parameters);
                parameter.FriendlyName = DataPath.PathHeader + parameter.FriendlyName;  // Affix path header to friendly name for easy identification from UI.
            }
        }

        private SecurityParameter.Parameter CreatePathableRecordParameter(Guid brokerId, PathableFieldInfo fieldInfo, Dictionary<string, SecurityParameter.Parameter> parameterSet)
        {
            SecurityParameter.Parameter parameter;
            if (fieldInfo.ParameterType == SecurityParameter.SecurityParameterType.PathableRecord)
            {
                SecurityParameter.PathableRecordParameter recordParameter = new SecurityParameter.PathableRecordParameter();

                IPathableRecordSchema schema = PathSchemaManager.GetSchemaByType(fieldInfo.FieldType);
                foreach (PathableFieldInfo childFieldInfo in schema.GetFields().Values)
                {
                    CreatePathableRecordParameter(brokerId, childFieldInfo, recordParameter.FieldParameters);
                }

                parameter = recordParameter;
            }
            else if (fieldInfo.ParameterType == SecurityParameter.SecurityParameterType.PathableCollection)
            {
                SecurityParameter.PathableCollectionParameter collectionParameter = new SecurityParameter.PathableCollectionParameter();

                IPathableCollectionSchema schema = PathSchemaManager.GetSchemaByType(fieldInfo.FieldType) as IPathableCollectionSchema;
                collectionParameter.Subsets = schema.GetSubsets();
                collectionParameter.Records = schema.GetRecordTypes(brokerId);

                foreach (PathableFieldInfo childFieldInfo in schema.GetFields().Values)
                {
                    CreatePathableRecordParameter(brokerId, childFieldInfo, collectionParameter.FieldParameters);
                }

                parameter = collectionParameter;
            }
            else if (fieldInfo.ParameterType == SecurityParameter.SecurityParameterType.Enum)
            {
                SecurityParameter.EnumeratedParmeter eparam = new SecurityParameter.EnumeratedParmeter();
                eparam.BaseType = SecurityParameter.EnumParamType.Int;
                eparam.EnumMapping = ((PathableEnumFieldInfo)fieldInfo).EnumMapping;
                parameter = eparam;
            }
            else
            {
                parameter = new SecurityParameter.Parameter();
            }

            parameter.Type = fieldInfo.ParameterType;
            parameter.CategoryName = "";
            parameter.Id = fieldInfo.Name;
            parameter.FriendlyName = fieldInfo.Name;
            parameter.Source = SecurityParameter.E_FieldSource.LoanOrApp;
            parameterSet.Add(parameter.Id, parameter);

            return parameter;
        }

        private static IDictionary<string,SecurityParameter.Parameter> BuildSharedFieldDictionary()
        {
            IDictionary<string, SecurityParameter.Parameter> fullParameterList;

            #region Constant Parameter Reporting Values
            // Get a list of condition categories for condition-related parameters
            var conditionCategories = new HashSet<string>();
            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, SP_FETCH_ALL_CATEGORIES, null))
                {
                    while (reader.Read())
                    {
                        conditionCategories.Add((string)reader["category"]);
                    }
                }
            }
            var conditionCategoriesMapping = (from string category in conditionCategories
                                              select new SecurityParameter.EnumMapping()
                                              {
                                                  FriendlyValue = category,
                                                  RepValue = category
                                              }).ToList();

            SecurityParameter.Parameter[] specialVars = new SecurityParameter.Parameter[] {
                         new SecurityParameter.Parameter() {
                    CategoryName = "",
                    Id = "IsValid",
                    FriendlyName = "Is Valid",
                    Type = SecurityParameter.SecurityParameterType.Boolean,
                    Source = SecurityParameter.E_FieldSource.SpecialField
                },
                new SecurityParameter.Parameter() {
                    CategoryName = "",
                    Id = "IsTemplate",
                    FriendlyName = "Is Template",
                    Type = SecurityParameter.SecurityParameterType.Boolean,
                    Source = SecurityParameter.E_FieldSource.SpecialField
                },
                new SecurityParameter.Parameter() {
                    CategoryName = "",
                    Id = "IsAssigned",
                    FriendlyName = "Is Assigned",
                    Type = SecurityParameter.SecurityParameterType.Boolean,
                    Source = SecurityParameter.E_FieldSource.SpecialField
                },
                new SecurityParameter.Parameter() {
                    CategoryName = "",
                    Id = "PriceMyLoan",
                    FriendlyName = "Have PML?",
                    Type = SecurityParameter.SecurityParameterType.Boolean,
                    Source = SecurityParameter.E_FieldSource.SpecialField
                },
                new SecurityParameter.Parameter()
                {
                    CategoryName = "",
                    Id = "IsSameBranchAsLoanFile",
                    FriendlyName = "User Same Branch As Loan?",
                    Type = SecurityParameter.SecurityParameterType.Boolean,
                    Source = SecurityParameter.E_FieldSource.SpecialField
                },
                new SecurityParameter.Parameter() {
                    CategoryName = "",
                    Id = "IsBranchAllowLOEditLoanUntilUW",
                    FriendlyName = "Branch Allow LO Edit Until UW",
                    Type = SecurityParameter.SecurityParameterType.Boolean,
                    Source = SecurityParameter.E_FieldSource.SpecialField
                },

                new SecurityParameter.Parameter() {
                    CategoryName = "",
                    Id = "IsPmlAssigned",
                    FriendlyName = "Is PML Assigned?",
                    Type = SecurityParameter.SecurityParameterType.Boolean,
                    Source = SecurityParameter.E_FieldSource.SpecialField
                },
                new SecurityParameter.Parameter() {
                    CategoryName = "",
                    Id = "IsPmlExternalAssigned",
                    FriendlyName = "Is PML External Assigned?",
                    Type = SecurityParameter.SecurityParameterType.Boolean,
                    Source = SecurityParameter.E_FieldSource.SpecialField
                },
                new SecurityParameter.Parameter() {
                    CategoryName = "",
                    Id = "HaveUnderwriter",
                    FriendlyName = "Have Underwriter?",
                    Type = SecurityParameter.SecurityParameterType.Boolean,
                    Source = SecurityParameter.E_FieldSource.SpecialField
                },
                new SecurityParameter.Parameter() {
                    CategoryName = "",
                    Id = "HaveProcessor",
                    FriendlyName = "Have Processor?",
                    Type = SecurityParameter.SecurityParameterType.Boolean,
                    Source = SecurityParameter.E_FieldSource.SpecialField
                },
                new SecurityParameter.Parameter() {
                    CategoryName = "",
                    Id = "CanWriteNonAssignedLoan",
                    FriendlyName = "Has Permission To Write Non-Asigned Loan?",
                    Type = SecurityParameter.SecurityParameterType.Boolean,
                    Source = SecurityParameter.E_FieldSource.SpecialField
                },
                new SecurityParameter.Parameter() {
                    CategoryName = "",
                    Id = "EditAssignedProcessor",
                    FriendlyName = "Has Permission To Write Assigned Processor Loan?",
                    Type = SecurityParameter.SecurityParameterType.Boolean,
                    Source = SecurityParameter.E_FieldSource.SpecialField
                },
                new SecurityParameter.Parameter() {
                    CategoryName = "",
                    Id = "EditAssignedUnderwriter",
                    FriendlyName = "Has Permission To Write Assigned Underwriter Loan?",
                    Type = SecurityParameter.SecurityParameterType.Boolean,
                    Source = SecurityParameter.E_FieldSource.SpecialField
                },
                new SecurityParameter.Parameter() {
                    CategoryName = "",
                    Id = "sLpTemplateIdNonEmpty",
                    FriendlyName = "Has Registered Loan Program?",
                    Type = SecurityParameter.SecurityParameterType.Boolean,
                    Source = SecurityParameter.E_FieldSource.SpecialField
                },
                new SecurityParameter.Parameter() {
                    CategoryName = "",
                    Id = "AccessClosedLoan",
                    FriendlyName = "Has Permission Access Closed Loan?",
                    Type = SecurityParameter.SecurityParameterType.Boolean,
                    Source = SecurityParameter.E_FieldSource.SpecialField
                },
                new SecurityParameter.Parameter() {
                    CategoryName = "",
                    Id = "LimitTemplateToBranch",
                    FriendlyName = "Limit Template To Branch?",
                    Type = SecurityParameter.SecurityParameterType.Boolean,
                    Source = SecurityParameter.E_FieldSource.SpecialField
                },
                new SecurityParameter.EnumeratedParmeter() {
                    CategoryName = "",
                    Id = "Lien",
                    FriendlyName = "Lien",
                    Type = SecurityParameter.SecurityParameterType.Enum,
                    EnumMapping = new List<SecurityParameter.EnumMapping>() { new SecurityParameter.EnumMapping() { FriendlyValue ="First", RepValue="0"},
                        new SecurityParameter.EnumMapping() { FriendlyValue ="Second", RepValue="1"},
                        new SecurityParameter.EnumMapping() { FriendlyValue ="Other", RepValue="2"}
                    },
                    Source = SecurityParameter.E_FieldSource.SpecialField
                },
                new SecurityParameter.Parameter() {
                    CategoryName = "",
                    Id = "AlwaysTrue",
                    FriendlyName ="Always True",
                    Type = SecurityParameter.SecurityParameterType.Boolean ,
                    Source = SecurityParameter.E_FieldSource.SpecialField
                },
                new SecurityParameter.EnumeratedParmeter() {
                    CategoryName = "",
                    Id = "Scope",
                    FriendlyName = "Scope",
                    Type = SecurityParameter.SecurityParameterType.Enum,
                    EnumMapping = new List<SecurityParameter.EnumMapping>() { new SecurityParameter.EnumMapping() { FriendlyValue ="In Scope", RepValue="1"},
new SecurityParameter.EnumMapping() { FriendlyValue="Non-Duty", RepValue="2"}},
                    Source = SecurityParameter.E_FieldSource.SpecialField
                },
                new SecurityParameter.EnumeratedParmeter() {
                    CategoryName = "",
                    Id = "ActAsRateLocked",
                    FriendlyName = "Act As Rate Locked",
                    Type = SecurityParameter.SecurityParameterType.Enum,
                    EnumMapping = new List<SecurityParameter.EnumMapping>() { new SecurityParameter.EnumMapping() { FriendlyValue ="NotAffect", RepValue="0"},
                        new SecurityParameter.EnumMapping() { FriendlyValue ="AllowRead", RepValue="1"},
                        new SecurityParameter.EnumMapping() { FriendlyValue ="AllowWrite", RepValue="2"}
                    },
                    Source = SecurityParameter.E_FieldSource.SpecialField
                },
                new SecurityParameter.EnumeratedParmeter() { // Section 2.2.1.1 of Task Backend spec
                    CategoryName = "Condition",
                    Id = "DoesConditionExist",
                    FriendlyName = "Condition exists with category",
                    Type = SecurityParameter.SecurityParameterType.Enum,
                    BaseType = SecurityParameter.EnumParamType.String,
                    EnumMapping = conditionCategoriesMapping,
                    Source = SecurityParameter.E_FieldSource.Condition
                },
                new SecurityParameter.EnumeratedParmeter() { // Section 2.2.1.1 of Task Backend spec
                    CategoryName = "Condition",
                    Id = "AreAllConditionsClosed",
                    FriendlyName = "All conditions with category are cleared",
                    Type = SecurityParameter.SecurityParameterType.Enum,
                    BaseType = SecurityParameter.EnumParamType.String,
                    EnumMapping = conditionCategoriesMapping,
                    Source = SecurityParameter.E_FieldSource.Condition
                },
                new SecurityParameter.Parameter(){
                    CategoryName = "",
                    Id = "sSpAddrTBD",
                    FriendlyName = "Property TBD",
                    Type = SecurityParameter.SecurityParameterType.Boolean,
                    Source = SecurityParameter.E_FieldSource.LoanOrApp
                },
                new SecurityParameter.Parameter() {
                    CategoryName = "Dates",
                    Id = "sDateTriggered",
                    FriendlyName = "Date Triggered",
                    Type = SecurityParameter.SecurityParameterType.Date,
                    Source = SecurityParameter.E_FieldSource.SpecialField
                },
                new SecurityParameter.EnumeratedParmeter() { // 5/23/2014 gf - opm 129490
                    CategoryName = "Condition",
                    Id = "DoesActiveConditionExist",
                    FriendlyName = "Active condition exists with category",
                    Type = SecurityParameter.SecurityParameterType.Enum,
                    BaseType = SecurityParameter.EnumParamType.String,
                    EnumMapping = conditionCategoriesMapping,
                    Source = SecurityParameter.E_FieldSource.Condition
                },
                new SecurityParameter.Parameter() { // 04/04/2017 mf - opm 450477
                    CategoryName = "",
                    Id = "OrderCredit_CreditReportProtocol",
                    FriendlyName = "Credit Report Protocol",
                    Type = SecurityParameter.SecurityParameterType.Count,
                    Source = SecurityParameter.E_FieldSource.SpecialField
                },
                new SecurityParameter.EnumeratedParmeter() {
                    CategoryName = "Generate Docs",
                    Id = GenerateDocsPackageTypeFieldId,
                    FriendlyName = "Package Type",
                    Type = SecurityParameter.SecurityParameterType.Enum,
                    EnumMapping = Enum.GetValues(typeof(VendorConfig.PackageType))
                        .Cast<VendorConfig.PackageType>()
                        .Select(pt => new SecurityParameter.EnumMapping() { FriendlyValue = pt.ToString(), RepValue = pt.ToString("d") })
                        .ToList(),
                    Source = SecurityParameter.E_FieldSource.SpecialField
                },
            };

            fullParameterList = specialVars.ToDictionary(p => p.Id, StringComparer.OrdinalIgnoreCase);
            #endregion

            #region load custom report fields
            // We want to still have one place where we add fields to reporting engine,
            // So we use the LoanReporting engine to get our field information.
            LoanReporting lR = new LoanReporting();
            foreach (Field field in lR.Fields.GetAllFields())
            {
                if (field.Domain == "Calendar Dates") continue; // For now, we do not use the calculated dates.

                if (field.Type == Field.E_FieldType.Enu)
                {
                    SecurityParameter.EnumeratedParmeter param = new SecurityParameter.EnumeratedParmeter();
                    param.CategoryName = field.Domain;
                    param.FriendlyName = field.Name;
                    param.Id = field.Id;
                    param.Type = SecurityParameter.SecurityParameterType.Enum;
                    param.BaseType = SecurityParameter.EnumParamType.Int;
                    param.Source = SecurityParameter.E_FieldSource.CustomReport;
                    param.EnumMapping = new List<SecurityParameter.EnumMapping>();
                    foreach (string sVal in field.Mapping)
                    {
                        string sKey = field.Mapping.ByVal[sVal];
                        param.EnumMapping.Add(new SecurityParameter.EnumMapping()
                        {
                            FriendlyValue = sVal,
                            RepValue = sKey
                        });
                    }
                    fullParameterList.Add(param.Id, param);
                }
                else if (false == field.Id.Equals("sSpState", StringComparison.OrdinalIgnoreCase))
                {
                    SecurityParameter.Parameter param = new SecurityParameter.Parameter();
                    param.CategoryName = field.Domain;
                    param.FriendlyName = field.Name;
                    param.Id = field.Id;
                    param.Type = GetParameterTypeFromReportingType(field.Kind, field.Type);
                    param.Source = SecurityParameter.E_FieldSource.CustomReport;
                    fullParameterList.Add(param.Id, param);
                }
                else
                {
                    List<SecurityParameter.EnumMapping> mappigns = new List<SecurityParameter.EnumMapping>();
                    foreach (string state in StateInfo.Instance.GetAllStates())
                    {
                        mappigns.Add(new SecurityParameter.EnumMapping() { FriendlyValue = state, RepValue = state });
                    }

                    mappigns.Add(new SecurityParameter.EnumMapping() { FriendlyValue = "Empty String", RepValue = "" });

                    SecurityParameter.EnumeratedParmeter param = new SecurityParameter.EnumeratedParmeter();
                    param.CategoryName = field.Domain;
                    param.FriendlyName = field.Name;
                    param.Id = field.Id;
                    param.Type = SecurityParameter.SecurityParameterType.Enum;
                    param.Source = SecurityParameter.E_FieldSource.CustomReport;
                    param.BaseType = SecurityParameter.EnumParamType.String;
                    param.EnumMapping = mappigns;
                    fullParameterList.Add(param.Id, param);
                }
            }
            #endregion

            #region load roles

            SecurityParameter.EnumeratedParmeter roleParam = new SecurityParameter.EnumeratedParmeter()
            {
                CategoryName = "Role",
                FriendlyName = "Role",
                Id = "Role",
                HasNotInOption = true,
                Type = SecurityParameter.SecurityParameterType.Enum,
                BaseType = SecurityParameter.EnumParamType.Int,
                Source = SecurityParameter.E_FieldSource.SpecialField
            };

            roleParam.EnumMapping = new List<SecurityParameter.EnumMapping>();
            foreach (E_RoleT role in Enum.GetValues(typeof(E_RoleT)))
            {
                roleParam.EnumMapping.Add(
                    new SecurityParameter.EnumMapping()
                    {
                        FriendlyValue = Role.GetRoleDescription(role),
                        RepValue = role.ToString("D")
                    });
            }

            fullParameterList.Add(roleParam.Id, roleParam);

            #endregion

            #region load all page data fields
            foreach (PageDataFieldInfo field in PageDataUtilities.GetAllFieldsFromPageData())
            {
                if (fullParameterList.ContainsKey(field.Id))
                {
                    continue;
                }

                if (field.FieldType == E_PageDataFieldType.Array)
                {
                    // dd 6/6/2016 - We do not support this type of fields in custom reporting.
                    continue;
                }
                SecurityParameter.SecurityParameterType fieldType = GetParameterTypeFromPageDataUtilityType(field.FieldType);

                SecurityParameter.Parameter parameter;

                if (fieldType == SecurityParameter.SecurityParameterType.Enum)
                {
                    if (field.EnumeratedType == null)
                    {
                        throw CBaseException.GenericException("Invalid enum " + field.Id);
                    }
                    SecurityParameter.EnumeratedParmeter eparam = new SecurityParameter.EnumeratedParmeter();
                    eparam.BaseType = SecurityParameter.EnumParamType.Int;
                    parameter = eparam;

                    Array values = Enum.GetValues(field.EnumeratedType);
                    string[] names = Enum.GetNames(field.EnumeratedType);

                    List<SecurityParameter.EnumMapping> mapping = new List<SecurityParameter.EnumMapping>(names.Length);
                    int iteration = 0;
                    foreach (int i in values) //what if someone uses values that are not int?
                    {
                        mapping.Add(new SecurityParameter.EnumMapping()
                        {
                            FriendlyValue = names[iteration],
                            RepValue = i.ToString()
                        });
                        iteration++;
                    }
                    eparam.EnumMapping = mapping;
                }
                else
                {
                    parameter = new SecurityParameter.Parameter();
                }

                parameter.CategoryName = "";
                parameter.Id = field.Id;
                parameter.FriendlyName = field.Id;
                parameter.Type = fieldType;
                parameter.Source = SecurityParameter.E_FieldSource.LoanOrApp;
                fullParameterList.Add(parameter.Id, parameter);
            }

            #endregion

            return fullParameterList;

        }


        private static SecurityParameter.SecurityParameterType GetParameterTypeFromPageDataUtilityType(E_PageDataFieldType fieldType)
        {
            switch (fieldType)
            {
                 case E_PageDataFieldType.Unknown:
                    throw new UnhandledEnumException(fieldType);
                 case E_PageDataFieldType.String:
                     return SecurityParameter.SecurityParameterType.String;
                 case E_PageDataFieldType.Money:
                     return SecurityParameter.SecurityParameterType.Money;
                 case E_PageDataFieldType.Percent:
                     return SecurityParameter.SecurityParameterType.Rate;
                 case E_PageDataFieldType.Ssn:
                     return SecurityParameter.SecurityParameterType.String;
                 case E_PageDataFieldType.Phone:
                     return SecurityParameter.SecurityParameterType.String;
                 case E_PageDataFieldType.Integer:
                     return SecurityParameter.SecurityParameterType.Count;
                 case E_PageDataFieldType.Bool:
                     return SecurityParameter.SecurityParameterType.Boolean;
                 case E_PageDataFieldType.Enum:
                     return SecurityParameter.SecurityParameterType.Enum;
                 case E_PageDataFieldType.DateTime:
                     return SecurityParameter.SecurityParameterType.Date;
                 case E_PageDataFieldType.Guid:
                     return SecurityParameter.SecurityParameterType.String;
                 case E_PageDataFieldType.Decimal:
                     return SecurityParameter.SecurityParameterType.String;
                case E_PageDataFieldType.SensitiveString:
                     return SecurityParameter.SecurityParameterType.String;
                case E_PageDataFieldType.Array: // 6/6/2016 - dd - Not support in this context.
                default:
                    throw new UnhandledEnumException(fieldType);
             }
        }

        private static SecurityParameter.SecurityParameterType GetParameterTypeFromReportingType(Field.E_ClassType classType, Field.E_FieldType fieldType)
        {
            switch (fieldType)
            {
                case Field.E_FieldType.Boo:
                    return SecurityParameter.SecurityParameterType.Boolean;
                case Field.E_FieldType.Int:
                case Field.E_FieldType.Dbl:
                    return classType == Field.E_ClassType.Cnt ? SecurityParameter.SecurityParameterType.Count : SecurityParameter.SecurityParameterType.String;
                case Field.E_FieldType.Str:
                    return SecurityParameter.SecurityParameterType.String;
                case Field.E_FieldType.Dec:
                    return classType == Field.E_ClassType.Csh ? SecurityParameter.SecurityParameterType.Money : SecurityParameter.SecurityParameterType.Rate;
                case Field.E_FieldType.Enu:
                    return SecurityParameter.SecurityParameterType.Enum;
                case Field.E_FieldType.Dtm:
                    return SecurityParameter.SecurityParameterType.Date;
                case Field.E_FieldType.Invalid:
                case Field.E_FieldType.Obj:
                    Tools.LogBug("Cannot classify Reporting object.");
                    return SecurityParameter.SecurityParameterType.String;
                default:
                    throw new UnhandledEnumException(fieldType);
            }
        }

        private List<string> BuildCategoryList()
        {
            List<string> ret = new List<string>();
            string categoryName;
            foreach (SecurityParameter.Parameter parameter in ParametersSorted)
            {
                categoryName = parameter.CategoryName;
                if (ret.Contains(categoryName) == false)
                    ret.Add(categoryName);
            }
            return ret;
        }

        private SecurityParameter.Parameter GetSecurityParameterWithValuesByFieldName(string fieldName)
        {
            SecurityParameter.Parameter systemParameter;
            if (!m_parameters.TryGetValue(fieldName, out systemParameter))
            {
                return null;
            }

            return systemParameter;
        }

        private SecurityParameter.Parameter GetSecurityParameterWithValuesByPath(string fieldName)
        {
            DataPath path = DataPath.Create(fieldName);
            SecurityParameter.Parameter currentParameter = null;

            // Set initial parameter.
            if (!m_parameters.TryGetValue(path.Head.Name, out currentParameter))
            {
                return null;    // Path is bad. Maybe throw.
            }

            foreach (IDataPathElement element in path.Tail.PathList)
            {
                if (element is DataPathBasicElement)
                {
                    // Only Record parameters have pathable elements. So check that parameter is a record parameter, and that the next element
                    // in the path corresponds to a field belonging to that parameter.
                    SecurityParameter.PathableRecordParameter recordParameter = currentParameter as SecurityParameter.PathableRecordParameter;
                    if (recordParameter == null || !recordParameter.FieldParameters.TryGetValue(element.Name, out currentParameter))
                    {
                        return null;    // Path is bad. Maybe throw.
                    }
                }
                else if (element is DataPathCollectionElement)
                {
                    // Only Record parameters have pathable elements. So check that parameter is a record parameter, and that the next element
                    // in the path corresponds to a field belonging to that parameter.
                    SecurityParameter.PathableRecordParameter recordParameter = currentParameter as SecurityParameter.PathableRecordParameter;
                    if (recordParameter == null || !recordParameter.FieldParameters.TryGetValue(element.Name, out currentParameter))
                    {
                        return null;    // Path is bad. Maybe throw.
                    }

                    // Only Collection parameters correspond to Collection data path nodes. So check that parameter is a collection parameter.
                    SecurityParameter.PathableCollectionParameter collectionParameter = currentParameter as SecurityParameter.PathableCollectionParameter;
                    if (collectionParameter == null)
                    {
                        return null;    // Path is bad. Maybe throw.
                    }
                }
                else if (element is DataPathSelectionElement)
                {
                    // Only Collection parameters can be indexed by a selection parameter. So check that parameter is a collection parameter.
                    SecurityParameter.PathableCollectionParameter collectionParameter = currentParameter as SecurityParameter.PathableCollectionParameter;
                    if (collectionParameter == null)
                    {
                        return null;    // Path is bad. Maybe throw.
                    }

                    // Verify selection index.
                    if (string.Equals(element.Name, DataPath.WholeCollectionIdentifier, StringComparison.OrdinalIgnoreCase))
                    {
                        // Do nothing. No extra verification needed in this case.
                    }
                    else if (element.Name.StartsWith(DataPath.SubsetMarker, StringComparison.OrdinalIgnoreCase))
                    {
                        // Verify subset name.
                        if (!collectionParameter.Subsets.ContainsKey(element.Name.Substring(DataPath.SubsetMarker.Length)))
                        {
                            return null;    // Path is bad. Maybe throw.
                        }
                    }
                    else
                    {
                        // Verify record ID.
                        PathableRecordTypeInfo recordInfo;
                        if (!collectionParameter.Records.TryGetValue(element.Name, out recordInfo))
                        {
                            return null;    // Path is bad. Maybe throw.
                        }
                    }
                }
            }

            return currentParameter; // The last parameter we run into should be the one we want.
        }
        #endregion
    }

    public static class SecurityParameter
    {
        public enum SecurityParameterType
        {
            Rate,
            Money,
            Count,
            String,
            Date,
            Boolean,
            Enum,
            Custom,
            PathableRecord,
            PathableCollection
        }

        public enum EnumParamType
        {
            Int,
            String
        }

        public enum E_FieldSource
        {
            /// <summary>
            /// These fields are in custom reports
            /// </summary>
            CustomReport = 0,
            /// <summary>
            /// These fields are located in the tables and not in custom reports
            /// </summary>
            LoanOrApp = 1,
            /// <summary>
            /// Used for custom variables from workflow config, these fields are composed of loanapp fields / custom report fields
            /// </summary>
            CompositeField = 2, 
            /// <summary>
            /// These are nonexistent fields that are not in workflow but the engine knows how to handle
            /// </summary>
            SpecialField = 3,
            /// <summary>
            /// These fields are user groups
            /// </summary>
            UserGroup = 4,
            /// <summary>
            /// These fields are the condition-related parameters
            /// </summary>
            Condition = 5,
        }


        public class Parameter : IComparable<Parameter>
        {
            public string Id { get; set; }
            public string FriendlyName { get; set; }
            public string CategoryName { get; set; }
            public SecurityParameterType Type { get; set; }
            public CustomVariable.E_Scope Scope { get; set; }
            public E_FieldSource Source { get; set; }

            public int CompareTo(Parameter param)
            {
                // Sort by category, then by Friendly Name
                return (CategoryName == param.CategoryName)
                    ? FriendlyName.CompareTo(param.FriendlyName)
                    : CategoryName.CompareTo(param.CategoryName);
            }

        }

        public class EnumeratedParmeter : Parameter
        {
            public EnumParamType BaseType { get; set; }
            public List<EnumMapping> EnumMapping { get; set; }
            public bool HasNotInOption { get; set; }
        }

        public class EnumMapping
        {
            public String RepValue { get; set; }
            public String FriendlyValue { get; set; }
        }

        /// <summary>
        /// A parameter that contains a list of nested parameters.
        /// </summary>
        /// <remarks>
        /// Currently implemented to use only enumerated parameters, as that
        /// was all that was needed for OPM 449923. I leave it to other devs
        /// to genericise as needed. - je
        /// </remarks>
        public class NestedEnumeratedParmeter : EnumeratedParmeter
        {
            public List<EnumeratedParmeter> SubParameters { get; set; }
        }

        /// <summary>
        /// Parameter to use for <see cref="IPathResolvable"/> objects that are part
        /// of a collection not a collection (ie. that have their own specific fieldId).
        /// </summary>
        /// <remarks>
        /// Not sure if any of these exists right now, apart from <see cref="CPageData"/>.
        /// </remarks>
        public class PathableRecordParameter : Parameter
        {
            public Dictionary<string, Parameter> FieldParameters { get; set; } = new Dictionary<string, Parameter>(StringComparer.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Parameter to use for <see cref="IPathResolvable"/> collections that are
        /// made up of multiple <see cref="IPathResolvable"/> records of the same type
        /// (and trys with the same set of field parameters).
        /// </summary>
        public class PathableCollectionParameter : PathableRecordParameter
        {
            public IReadOnlyDictionary<string, PathableSubsetInfo> Subsets { get; set; }
            public IReadOnlyDictionary<string, PathableRecordTypeInfo> Records { get; set; }
        }
    }
}