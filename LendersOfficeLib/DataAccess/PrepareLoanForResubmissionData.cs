using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
    public class CReasonForNotAllowingResubmissionData : CPageData
    {
        private static CSelectStatementProvider s_selectProvider;
	    
        static CReasonForNotAllowingResubmissionData()
        {
            StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("sReasonForNotAllowingResubmission");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
        public CReasonForNotAllowingResubmissionData(Guid fileId) : base(fileId, "CReasonForNotAllowingResubmissionData", s_selectProvider)
        {
        }
    }

	public class CPrepareLoanForResubmissionData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CPrepareLoanForResubmissionData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
	        list.Add("sLienPosT");
            list.Add("sStatusT");
            list.Add("sStatusLckd");
            list.Add("sLpTemplateId");
            list.Add("sLpTemplateNm");
            list.Add("sLpTemplateNmSubmitted");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CPrepareLoanForResubmissionData(Guid fileId) : base(fileId, "CPrepareLoanForResubmissionData", s_selectProvider)
		{
		}
	}
}
