using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
	public class CLoanTemplateData : CPageData
	{
		private static CSelectStatementProvider s_selectProvider;
		static CLoanTemplateData()
		{
			StringList list = new StringList();

            list.Add("sfApplyLoanProductTemplate");
            list.Add("sNoteIR");
            list.Add("sLOrigFPc");
            list.Add("sRAdjMarginR");
            list.Add("sStipulations");

			s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

		}

        public CLoanTemplateData( Guid fileId ) : base( fileId , "LoanTemplateData" , s_selectProvider )
        {
        }

	}



}