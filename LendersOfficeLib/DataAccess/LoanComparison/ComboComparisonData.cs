﻿namespace DataAccess.LoanComparison
{
    using System;

    /// <summary>
    /// A container class for the correct combo comparison data.
    /// </summary>
    public class ComboComparisonData
    {
        /// <summary>
        /// Gets or sets the first dti.
        /// </summary>
        /// <value> The first dti.</value>
        public decimal FirstDti { get; set; }

        /// <summary>
        /// Gets or sets the second dti.
        /// </summary>
        /// <value> The second dti.</value>
        public decimal SecondDti { get; set; }

        /// <summary>
        /// Gets or sets the sum of the first and second dti.
        /// </summary>
        /// <remarks>
        /// This is a string currently because it is a pass-through value
        /// from an underlying RateOption rather than a number that is
        /// being recalculated. Clean this up when we normalize conversions
        /// between strings and values.
        /// </remarks>
        /// <value> The total dti.</value>
        public string TotalDti { get; set; }

        /// <summary>
        /// Gets or sets the total principal and interest.
        /// </summary>
        /// <value> The total principal and interest.</value>
        public decimal TotalPI { get; set; }

        /// <summary>
        /// Gets or sets the total monthly payment amount.
        /// </summary>
        /// <value> The total monthly payment.</value>
        public decimal TotalMonthlyPayment { get; set; }

        /// <summary>
        /// Gets or sets the toal amount saved from re-registering a loan.
        /// </summary>
        /// <value> The amount saved.</value>
        public decimal PitiSavings { get; set; }

        /// <summary>
        /// Gets or sets the number of months necessary to break even.
        /// </summary>
        /// <value> The number of recoup months.</value>
        public decimal RecoupMonths
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the total closing costs.
        /// </summary>
        /// <value>The total closing costs.</value>
        public decimal TotalClosingCosts
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the total cash to close.
        /// </summary>
        /// <remarks>
        /// This is a string currently because it is a pass-through value
        /// from an underlying RateOption rather than a number that is
        /// being recalculated. Clean this up when we normalize conversions
        /// between strings and values.
        /// </remarks>
        /// <value>The total cash to close.</value>
        public string TotalCashToClose { get; set; }

        /// <summary>
        /// Gets or sets the total estimated reserves dollar amount.
        /// </summary>
        /// <remarks>
        /// This is a string currently because it is a pass-through value
        /// from an underlying RateOption rather than a number that is
        /// being recalculated. Clean this up when we normalize conversions
        /// between strings and values.
        /// </remarks>
        /// <value>The total estimated reserves dollar amonunt.</value>
        public string TotalEstimatedReservesDollars { get; set; }

        /// <summary>
        /// Gets or sets the total number of estimated reserves months.
        /// </summary>
        /// <remarks>
        /// This is a string currently because it is a pass-through value
        /// from an underlying RateOption rather than a number that is
        /// being recalculated. Clean this up when we normalize conversions
        /// between strings and values.
        /// </remarks>
        /// <value>The total number of estimated reserves months.</value>
        public string TotalEstimatedReservesMonths { get; set; }
    }
}
