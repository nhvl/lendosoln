﻿namespace DataAccess.LoanComparison
{
    using System.Collections.Generic;

    /// <summary>
    /// A class for retrieving the correct data for combo scenarios.
    /// </summary>
    public class LoanComparisonComboCorrections
    {
        /// <summary>
        /// A static method that calculates the combo scenario data for the Loan Comparison report.  
        /// </summary>
        /// <param name="convert">An LosConvert object.</param>
        /// <param name="dataLoan">The loan file.</param>
        /// <param name="first">The first lien result item.</param>
        /// <param name="second">The second lien result item.</param>
        /// <returns>A combo data object containing the correct data for combo scenarios.</returns>
        public static ComboComparisonData CorrectComboData(LosConvert convert, CPageData dataLoan, LoanComparisonXml.PinStateLienResultItem first, LoanComparisonXml.PinStateLienResultItem second)
        {
            // If either of the components are null, none of these calculations will result
            // in anything but garbage, so why even bother?
            if (first?.RateOption == null || second?.RateOption == null)
            {
                return null;
            }

            var comboData = new ComboComparisonData();
            var firstMonthlyPayment = convert.ToMoney(first.RateOption.FirstPmtAmt_rep);
            var secondMonthlyPayment = convert.ToMoney(second.RateOption.FirstPmtAmt_rep);

            comboData.TotalPI = Tools.SumMoney(new List<decimal>() { firstMonthlyPayment, secondMonthlyPayment });

            var monthlyPayment = Tools.SumMoney(
                new List<string>()
                {
                    first.RateOption.sProRealETx,
                    first.RateOption.sProHazIns,
                    first.RateOption.sProMIns,
                    first.RateOption.sProHoAssocDues,
                    first.RateOption.sProOHExp
                });

            comboData.TotalMonthlyPayment = Tools.SumMoney(new List<decimal>() { comboData.TotalPI, monthlyPayment });

            comboData.FirstDti = LoanComparisonComboCorrections.AdjustDti(convert.ToRate(first.RateOption.BottomRatio_rep), dataLoan);
            comboData.SecondDti = 100 * (secondMonthlyPayment / dataLoan.sLTotI);
            comboData.TotalDti = second.RateOption.BottomRatio_rep;

            comboData.TotalClosingCosts = Tools.SumMoney(new List<string>() { first.RateOption.ClosingCost, second.RateOption.ClosingCost });
            comboData.TotalCashToClose = second.RateOption.CashToClose;
            comboData.TotalEstimatedReservesDollars = second.RateOption.EstimatedReservesDollars;
            comboData.TotalEstimatedReservesMonths = second.RateOption.Reserves;

            var currentPiti = convert.ToMoney(first.RateOption.CurrentPITIPayment);
            comboData.PitiSavings = Tools.MoneyDifference(currentPiti, comboData.TotalMonthlyPayment);

            if (comboData.PitiSavings < 0)
            {
                comboData.RecoupMonths = -1;
            }
            else if (comboData.TotalMonthlyPayment <= 0)
            {
                comboData.RecoupMonths = 0;
            }
            else
            {
                if (comboData.TotalMonthlyPayment == 0)
                {
                    comboData.RecoupMonths = 0;
                }
                else
                {
                    var breakEvenMonths = Tools.AlwaysRoundUp(comboData.TotalClosingCosts / comboData.PitiSavings, 1);
                    comboData.RecoupMonths = breakEvenMonths > dataLoan.sDue ? -1 : breakEvenMonths;
                }
            }

            return comboData;
        }

        /// <summary>
        /// Adjusts the DTI by removing the second monthly payment saved to the loan, to avoid adding
        /// two monthly payment amounts.
        /// </summary>
        /// <param name="bottomRatio">The original DTI value.</param>
        /// <param name="dataLoan">The loan file.</param>
        /// <returns>Returns the DTI minus the percentage from sTransmPro2ndMPmt.  Returns -1 if the DTI is invalid.</returns>
        public static decimal AdjustDti(decimal bottomRatio, CPageData dataLoan)
        {
            if (bottomRatio == 0 || dataLoan.sLTotI == 0)
            {
                return -1;
            }

            return bottomRatio - (100 * (dataLoan.sTransmPro2ndMPmt / dataLoan.sLTotI));
        }
    }
}