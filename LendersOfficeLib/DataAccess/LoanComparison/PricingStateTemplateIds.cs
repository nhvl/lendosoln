﻿namespace DataAccess.LoanComparison
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public sealed class PricingStateTemplateIds
    {
        [XmlElement]
        public Guid Id { get; set; }
        [XmlArray("TemplateIds")]
        [XmlArrayItem("TemplateId", typeof(Guid))]
        public List<Guid> TemplateIds { get; set; }

        public PricingStateTemplateIds()
        {
            TemplateIds = new List<Guid>();
        }
    }
}
