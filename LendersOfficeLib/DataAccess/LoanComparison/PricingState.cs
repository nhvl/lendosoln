﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOfficeApp.los.RatePrice;
using System.Xml.Serialization;
using System.Xml;
using System.Xml.Schema;

namespace DataAccess.LoanComparison
{
    /// <summary>
    /// Represents the property information from the loan file used in files.
    /// </summary> 
    public sealed class PricingState : IXmlSerializable
    {
        #region fields

        public int FriendlyId { get; set; }
        public Guid Id { get; private set; }

        public bool sIsRenovationLoan { get; private set; }
        public string sTotalRenovationCosts_rep { get; private set; }
        public E_sLPurposeT sLPurposeTPe { get; private set; }
        public string sProdCashoutAmt_rep { get; private set; }
        public string sApprValPe_rep { get; private set; }
        public string sHouseValPe_rep { get; private set; }
        public string sAsIsAppraisedValuePeval_rep { get; private set; }
        public string sDownPmtPcPe_rep { get; private set; }
        public string sEquityPe_rep { get; private set; }
        public string sLtvRPe_rep { get; private set; }
        public string sLAmtCalcPe_rep { get; private set; }
        public bool sHas2ndFinPe { get; private set; }
        public string sLtvROtherFinPe_rep { get; set; }
        public string sProOFinBalPe_rep { get; private set; }
        public string sCltvRPe_rep { get; private set; }
        public string sSpAddr { get; private set; }
        public string sSpCity { get; private set; }
        public string sSpZip { get; private set; }
        public string sSpStatePe { get; private set; }
        public string sSpCounty { get; private set; }
        public E_sProdSpT sProdSpT { get; private set; }
        public E_sProdSpStructureT sProdSpStructureT { get; private set; }
        public bool sHasNonOccupantCoborrowerPe { get; private set; }
        public bool sProdImpound { get; private set; }
        public bool sIsIOnlyPe { get; private set; }
        public E_sProd3rdPartyUwResultT sProd3rdPartyUwResultT { get; private set; }
        public bool sProdIsDuRefiPlus { get; private set; }
        public string sProdRLckdDays_rep { get; private set; }
        public E_sProdCalcEntryT sProdCalcEntryT { get; private set; }
        public bool sProdIsSpInRuralArea { get; private set; }
        public string sProRealETxPe_rep { get; private set; }
        public E_SellerCreditT sSellerCreditT { get; private set; }
        public E_sOccT sOccTPe { get; private set; }
        public string sProHazInsPe_rep { get; private set; }
        public string sProHoAssocDuesPe_rep { get; private set; }
        public string sProOHExpDesc { get; private set; }
        public string sProOHExpPe_rep { get; private set; }
        public string sPriorSalesD_rep { get; private set; }
        public bool sIsCreditQualifying { get; private set; }
        public E_sProdDocT sProdDocT { get; private set; }
        public E_sLienPosT sLienPosT { get; private set; }
        public string sNumFinancedProperties_rep { get; private set; }
        public E_sProdConvMIOptionT sProdConvMIOptionT { get; private set; }
        public bool sProdIsUFMIPFFFinanced { get; private set; }
        public bool sProdOverrideUFMIPFF { get; private set; }
        public string sProdFhaUfmip_rep { get; private set; }
        public string sProdVaFundingFee_rep { get; private set; }
        public string sProdUSDAGuaranteeFee_rep { get; private set; }

        public string sCountyRtcPe_rep { get; private set; }
        public string sRecFPe_rep { get; private set; }
        public string sTitleInsF_rep { get; private set; }
        public string sOwnerTitleInsF_rep { get; private set; }

        public string SelectedNoteRate { get; private set; }

        public LosConvert convert;
        public LosConvert Convert
        {
            get
            {
                if(convert == null)
                {
                    convert = new LosConvert();
                }

                return convert;
            }
        }

        public string GetNoteRate(E_sLienPosT sLienPosT)
        {
            if ( sLienPosT == E_sLienPosT.First)
            {
                return SelectedNoteRate;
            }
            else
            {
                return secondLoanSelectedNoteRate;
            }
        }


        public Guid GetLoanTemplateId(E_sLienPosT sLienPosT)
        {
            if (sLienPosT == E_sLienPosT.First)
            {
                return lLpTemplateId;
            }
            else 
            {
                if (secondLoanlLpTemplateId.HasValue)
                {
                    return secondLoanlLpTemplateId.Value;
                }
                else
                {
                    return Guid.Empty;
                }
            }
        }

        public bool IsRateMerge { get; private set; }

        public E_sLT lLt { get; set; }
        public Guid lLpTemplateId { get; private set; } // for non rate merge

        public string lLpTemplateNm { get; private set; }

        public E_sOriginatorCompensationPaymentSourceT sOriginatorCompensationPaymentSourceT { get; private set; }

        public E_sConvSplitMIRT sConvSplitMIRT { get; private set; }

        public E_sSubFinT sSubFinT { get; private set; }

        public string sSubFinToPropertyValue_rep { get; private set; }
        public string sSubFinPe_rep { get; private set; }
        public string sHCLTVRPe_rep { get; private set; }

        // OPM 244915.  We have to track if these values were pulled from the loan
        // or came from fee service at the time of the pin.  Fee service should 
        // override existing values if we know the existing came from a fee service.
        // Nullable because we cannot safely assume either direction for existing pins.
        public bool? sProRealETxLocked { get; private set; }
        public bool? sProHazInsLocked { get; private set; }
        public bool? sProHoAssocDuesLocked { get; private set; }
        public bool? sProOHExpLocked { get; private set; }

        public bool IsForRateMonitor { get; set; }

        public RateMergeGroupKeyParameters firstLoanRateMergeParameters;

        public Guid? secondLoanlLpTemplateId { get; private set; }
        public string secondLoanlLpTemplateNm { get; private set; }
        public RateMergeGroupKeyParameters secondLoanParameters { get; private set; }
        public string secondLoanSelectedNoteRate { get; private set; }

        public string secondLoanMonthlyPayment_rep { get; private set; }

        public bool sIsStudentLoanCashoutRefi { get; set; }

        public int Version { get; private set; }
        
        public bool IsComboScenario
        {
            get
            {
                return this.secondLoanlLpTemplateId.HasValue;
            }
        }

        public decimal SecondLoanMonthlyPayment
        {
            get
            {
                if (!this.IsComboScenario)
                {
                    return 0;
                }

                return this.Convert.ToDecimal(this.secondLoanMonthlyPayment_rep);
            }
        }


        public string CombinedLoanAmount
        {
            get
            {
                if (this.Version == 0)
                {
                    return "N/A";
                }

                decimal slamt = this.Convert.ToDecimal(this.sLAmtCalcPe_rep);
                decimal otherFinancing = this.Convert.ToDecimal(this.sProOFinBalPe_rep);
                return this.Convert.ToMoneyString(Tools.SumMoney(new decimal[] { slamt, otherFinancing }), FormatDirection.ToRep);
            }
        }

        public string CombinedHelocLoanAmount
        {
            get
            {
                if (this.Version == 0 || this.sSubFinT != E_sSubFinT.Heloc)
                {
                    return "N/A";
                }

                decimal slamt = this.Convert.ToDecimal(this.sLAmtCalcPe_rep);
                decimal helocLineAmount = this.Convert.ToDecimal(this.sSubFinPe_rep);
                return this.Convert.ToMoneyString(Tools.SumMoney(new decimal[] { slamt, helocLineAmount }), FormatDirection.ToRep);
            }
        }

        #endregion

        public PricingState()
        {
            Id = Guid.NewGuid();
            IsForRateMonitor = false;
            Version = 2;
        }

        public RateMergeGroupKey GetMergeKey(E_sLienPosT sLienPosT)
        {
            if (!IsRateMerge)
            {
                return null;
            }

            if (sLienPosT == E_sLienPosT.First)
            {
                return new RateMergeGroupKey(firstLoanRateMergeParameters);
            }
            else if (IsComboScenario)
            {
                return new RateMergeGroupKey(secondLoanParameters);
            }

            return null;
        }

        internal void ApplyLoanInformation(CPageBase pageData)
        {
            sIsRenovationLoan = pageData.sIsRenovationLoan;
            sTotalRenovationCosts_rep = pageData.sTotalRenovationCosts_rep;
            sLPurposeTPe = pageData.sLPurposeTPe;
            sProdCashoutAmt_rep = pageData.sProdCashoutAmt_rep;
            sApprValPe_rep = pageData.sApprValPe_rep;
            sHouseValPe_rep = pageData.sHouseValPe_rep;
            sAsIsAppraisedValuePeval_rep = pageData.sAsIsAppraisedValuePeval_rep;
            sDownPmtPcPe_rep = pageData.sDownPmtPcPe_rep;
            sEquityPe_rep = pageData.sEquityPe_rep;
            sLtvRPe_rep = pageData.sLtvRPe_rep;
            sLAmtCalcPe_rep = pageData.sLAmtCalcPe_rep;
            sHas2ndFinPe = pageData.sHas2ndFinPe;
            sLtvROtherFinPe_rep = pageData.sLtvROtherFinPe_rep;
            sProOFinBalPe_rep = pageData.sProOFinBalPe_rep;
            sCltvRPe_rep = pageData.sCltvRPe_rep;
            sSpAddr = pageData.sSpAddr;
            sSpZip = pageData.sSpZip;
            sSpStatePe = pageData.sSpStatePe;
            sSpCounty = pageData.sSpCounty;
            sSpCity = pageData.sSpCity;
            sProdSpT = pageData.sProdSpT;
            sProdSpStructureT = pageData.sProdSpStructureT;
            sHasNonOccupantCoborrowerPe = pageData.sHasNonOccupantCoborrowerPe;
            sProdImpound = pageData.sProdImpound;
            sIsIOnlyPe = pageData.sIsIOnlyPe;
            sProd3rdPartyUwResultT = pageData.sProd3rdPartyUwResultT;
            sProdIsDuRefiPlus = pageData.sProdIsDuRefiPlus;
            sProdRLckdDays_rep = pageData.sProdRLckdDays_rep;
            sProdCalcEntryT = pageData.sProdCalcEntryT;
            sProdIsSpInRuralArea = pageData.sProdIsSpInRuralArea;
            sProRealETxPe_rep = pageData.sProRealETx_rep;
            sSellerCreditT = pageData.sSellerCreditT;
            sOccTPe = pageData.sOccTPe;
            sProHazInsPe_rep = pageData.sProHazIns_rep;
            sProHoAssocDuesPe_rep = pageData.sProHoAssocDuesPe_rep;
            sProOHExpDesc = pageData.sProOHExpDesc;
            sProOHExpPe_rep = pageData.sProOHExpPe_rep;
            sIsCreditQualifying = pageData.sIsCreditQualifying;
            sPriorSalesD_rep = pageData.sPriorSalesD_rep;
            sProdDocT = pageData.sProdDocT;
            sLienPosT = pageData.sLienPosT;
            sNumFinancedProperties_rep = pageData.sNumFinancedProperties_rep;
            sProdConvMIOptionT = pageData.sProdConvMIOptionT;
            sProdIsUFMIPFFFinanced = pageData.sProdIsUFMIPFFFinanced;
            sProdOverrideUFMIPFF = pageData.sProdOverrideUFMIPFF;
            sProdFhaUfmip_rep = pageData.sProdFhaUfmip_rep;
            sProdVaFundingFee_rep = pageData.sProdVaFundingFee_rep;
            sProdUSDAGuaranteeFee_rep = pageData.sProdUSDAGuaranteeFee_rep;

            sOwnerTitleInsF_rep = pageData.sOwnerTitleInsF_rep;
            sRecFPe_rep = pageData.sRecFPe_rep;
            sTitleInsF_rep = pageData.sTitleInsF_rep;
            sCountyRtcPe_rep = pageData.sCountyRtcPe_rep;
            sOriginatorCompensationPaymentSourceT = pageData.sOriginatorCompensationPaymentSourceT;
            sConvSplitMIRT = pageData.sConvSplitMIRT;
            this.sSubFinT = pageData.sSubFinT;

            this.sSubFinToPropertyValue_rep = pageData.sSubFinToPropertyValue_rep;
            this.sSubFinPe_rep = pageData.sSubFinPe_rep;
            this.sHCLTVRPe_rep = pageData.sHCLTVRPe_rep;

            this.sIsStudentLoanCashoutRefi = pageData.sIsStudentLoanCashoutRefi;

        }

        internal void ApplyInternalStateData(bool _sProRealETxLocked, bool _sProHazInsLocked, bool _sProHoAssocDuesLocked, bool _sProOHExpLocked)
        {
            sProRealETxLocked = _sProRealETxLocked;
            sProHazInsLocked = _sProHazInsLocked;
            sProHoAssocDuesLocked = _sProHoAssocDuesLocked;
            sProOHExpLocked = _sProOHExpLocked;
        }

        private RateMergeGroupKeyParameters GetRateMergeParametersFrom(ILoanProgramTemplate product)
        {
            RateMergeGroupKeyParameters parameters = new RateMergeGroupKeyParameters()
            {
                lTerm_rep = product.lTerm_rep,
                lDue_rep = product.lDue_rep,
                lFinMethT = product.lFinMethT,
                lBuydwnR1_rep = product.lBuydwnR1_rep,
                lBuydwnR2_rep = product.lBuydwnR2_rep,
                lBuydwnR3_rep = product.lBuydwnR3_rep,
                lBuydwnR4_rep = product.lBuydwnR4_rep,
                lBuydwnR5_rep = product.lBuydwnR5_rep,
                lArmIndexNameVstr = product.lArmIndexNameVstr,
                lLpProductType = product.lLpProductType,
                lRAdj1stCapMon_rep = product.lRadj1stCapMon_rep,
                lRAdj1stCapR_rep = product.lRadj1stCapR_rep,
                lRAdjCapR_rep = product.lRAdjCapR_rep,
                lRAdjLifeCapR_rep = product.lRAdjLifeCapR_rep,
                lRAdjCapMon_rep = product.lRAdjCapMon_rep,
                lPrepmtPeriod_rep = product.lPrepmtPeriod_rep,
                lHelocRepay_rep = product.lHelocRepay_rep,
                lHelocDraw_rep = product.lHelocDraw_rep
            };

            return parameters;
        }

        internal void ApplyLoanProductTemplate(bool isRateMerge, ILoanProgramTemplate firstLoanProduct, string firstLoanNoteRate, ILoanProgramTemplate secondLoanProduct, string secondLoanNoteRate, string secondLoanMonthlyPayment)
        {
            IsRateMerge = isRateMerge;
            SelectedNoteRate = firstLoanNoteRate;
            lLt = firstLoanProduct.lLT;
            lLpTemplateId = firstLoanProduct.lLpTemplateId;
            lLpTemplateNm = firstLoanProduct.lLpTemplateNm;
            this.firstLoanRateMergeParameters = this.GetRateMergeParametersFrom(firstLoanProduct);

            if (secondLoanProduct != null && !string.IsNullOrEmpty(secondLoanNoteRate))
            {
                this.secondLoanlLpTemplateId = secondLoanProduct.lLpTemplateId;
                this.secondLoanlLpTemplateNm = secondLoanProduct.lLpTemplateNm;
                this.secondLoanSelectedNoteRate = secondLoanNoteRate;
                this.secondLoanParameters = this.GetRateMergeParametersFrom(secondLoanProduct);
                this.secondLoanMonthlyPayment_rep = secondLoanMonthlyPayment;
            }
        }

        private int GetIntAttribute(XmlReader reader, string id)
        {
            string attr = reader.GetAttribute(id);
            if (string.IsNullOrEmpty(attr))
            {
                return 0;
            }
            else
            {
                return int.Parse(attr);
            }
        }

        private string GetStringAttribute(XmlReader reader, string id)
        {
            return reader.GetAttribute(id) ?? "";
        }

        private Guid GetGuidAttribute(XmlReader reader, string id)
        {
            string attr = reader.GetAttribute(id);
            if (string.IsNullOrEmpty(attr))
            {
                return Guid.Empty;
            }
            return new Guid(attr);
        }

        private bool GetBoolAttribute(XmlReader reader, string id)
        {
            string attr = reader.GetAttribute(id);
            if (string.IsNullOrEmpty(attr))
            {
                return false;
            }
            return bool.Parse(attr);
        }

        private bool? GetBoolAttributeWithNoDefault(XmlReader reader, string id)
        {
            string attr = reader.GetAttribute(id);
            if (string.IsNullOrEmpty(attr))
            {
                return null;
            }
            return bool.Parse(attr);
        }

        private void WriteSafeAttribute(XmlWriter writer, string id, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                value = "";
            }
            writer.WriteAttributeString(id, value);
        }


        #region IXmlSerializable Members

        public void ReadXml(XmlReader reader)
        {
            FriendlyId = GetIntAttribute(reader, "FriendlyId");
            Id = GetGuidAttribute(reader, "Id");
            sIsRenovationLoan = GetBoolAttribute(reader, "sIsRenovationLoan");
            sTotalRenovationCosts_rep = GetStringAttribute(reader, "sTotalRenovationCosts_rep");
            sLPurposeTPe = (E_sLPurposeT)GetIntAttribute(reader, "sLPurposeTPe");
            sProdCashoutAmt_rep = GetStringAttribute(reader, "sProdCashoutAmt_rep");
            sApprValPe_rep = GetStringAttribute(reader, "sApprValPe_rep");
            sHouseValPe_rep = GetStringAttribute(reader, "sHouseValPe_rep");
            sAsIsAppraisedValuePeval_rep = GetStringAttribute(reader, "sAsIsAppraisedValuePeval_rep");
            sDownPmtPcPe_rep = GetStringAttribute(reader, "sDownPmtPcPe_rep");
            sEquityPe_rep = GetStringAttribute(reader, "sEquityPe_rep");
            sLtvRPe_rep = GetStringAttribute(reader, "sLtvRPe_rep");
            sLAmtCalcPe_rep = GetStringAttribute(reader, "sLAmtCalcPe_rep");
            sHas2ndFinPe = GetBoolAttribute(reader, "sHas2ndFinPe");
            sLtvROtherFinPe_rep = GetStringAttribute(reader, "sLtvROtherFinPe_rep");
            sProOFinBalPe_rep = GetStringAttribute(reader, "sProOFinBalPe_rep");
            sCltvRPe_rep = GetStringAttribute(reader, "sCltvRPe_rep");
            sSpAddr = GetStringAttribute(reader, "sSpAddr");
            sSpCity = GetStringAttribute(reader, "sSpCity");
            sSpZip = GetStringAttribute(reader, "sSpZip");
            sSpStatePe = GetStringAttribute(reader, "sSpStatePe");
            sSpCounty = GetStringAttribute(reader, "sSpCounty");
            sProdSpT = (E_sProdSpT)GetIntAttribute(reader, "sProdSpT");
            sProdSpStructureT = (E_sProdSpStructureT)GetIntAttribute(reader, "sProdSpStructureT");
            sHasNonOccupantCoborrowerPe = GetBoolAttribute(reader, "sHasNonOccupantCoborrowerPe");
            sProdImpound = GetBoolAttribute(reader, "sProdImpound");
            sIsIOnlyPe = GetBoolAttribute(reader, "sIsIOnlyPe");
            sProd3rdPartyUwResultT = (E_sProd3rdPartyUwResultT)GetIntAttribute(reader, "sProd3rdPartyUwResultT");
            sProdIsDuRefiPlus = GetBoolAttribute(reader, "sProdIsDuRefiPlus");
            sProdRLckdDays_rep = GetStringAttribute(reader, "sProdRLckdDays_rep");
            sProdCalcEntryT = (E_sProdCalcEntryT)GetIntAttribute(reader, "sProdCalcEntryT");
            sProdIsSpInRuralArea = GetBoolAttribute(reader, "sProdIsSpInRuralArea");
            sProRealETxPe_rep = GetStringAttribute(reader, "sProRealETxPe_rep");
            sSellerCreditT = (E_SellerCreditT)GetIntAttribute(reader, "sSellerCreditT");
            sOccTPe = (E_sOccT)GetIntAttribute(reader, "sOccTPe");
            sProHazInsPe_rep = GetStringAttribute(reader, "sProHazInsPe_rep");
            sProHoAssocDuesPe_rep = GetStringAttribute(reader, "sProHoAssocDuesPe_rep");
            sProOHExpDesc = GetStringAttribute(reader, "sProOHExpDesc");
            sProOHExpPe_rep = GetStringAttribute(reader, "sProOHExpPe_rep");
            sPriorSalesD_rep = GetStringAttribute(reader, "sPriorSalesD_rep");
            sIsCreditQualifying = GetBoolAttribute(reader, "sIsCreditQualifying");
            sProdDocT = (E_sProdDocT)GetIntAttribute(reader, "sProdDocT");
            sLienPosT = (E_sLienPosT)GetIntAttribute(reader, "sLienPosT");
            sNumFinancedProperties_rep = GetStringAttribute(reader, "sNumFinancedProperties_rep");
            sProdConvMIOptionT = (E_sProdConvMIOptionT)GetIntAttribute(reader, "sProdConvMIOptionT");
            sProdIsUFMIPFFFinanced = GetBoolAttribute(reader, "sProdIsUFMIPFFFinanced");
            sProdOverrideUFMIPFF = GetBoolAttribute(reader, "sProdOverrideUFMIPFF");
            sProdFhaUfmip_rep = GetStringAttribute(reader, "sProdFhaUfmip_rep");
            sProdVaFundingFee_rep = GetStringAttribute(reader, "sProdVaFundingFee_rep");
            sProdUSDAGuaranteeFee_rep = GetStringAttribute(reader, "sProdUSDAGuaranteeFee_rep");

            SelectedNoteRate = GetStringAttribute(reader, "SelectedNoteRate");
            IsRateMerge = GetBoolAttribute(reader, "IsRateMerge");
          
            lLpTemplateId = GetGuidAttribute(reader, "lLpTemplateId");
            lLt = (E_sLT)GetIntAttribute(reader, "lLt");
            lLpTemplateNm = GetStringAttribute(reader, "lLpTemplateNm");

            sOwnerTitleInsF_rep = GetStringAttribute(reader, "sOwnerTitleInsF_rep");
            sRecFPe_rep = GetStringAttribute(reader, "sRecFPe_rep");
            sTitleInsF_rep = GetStringAttribute(reader, "sTitleInsF_rep");
            sCountyRtcPe_rep = GetStringAttribute(reader, "sCountyRtcPe_rep");

            IsForRateMonitor = GetBoolAttribute(reader, "IsForRateMonitor");
            sOriginatorCompensationPaymentSourceT = (E_sOriginatorCompensationPaymentSourceT)GetIntAttribute(reader, "sOriginatorCompensationPaymentSourceT");

            // GetIntAttribute() will return 0 (E_sConvSplitMIRT.Blank) if the attribute does not exist.
            sConvSplitMIRT = (E_sConvSplitMIRT)GetIntAttribute(reader, "sConvSplitMIRT");

            sProRealETxLocked = GetBoolAttributeWithNoDefault(reader, "sProRealETxLocked");
            sProHazInsLocked = GetBoolAttributeWithNoDefault(reader, "sProHazInsLocked");
            sProHoAssocDuesLocked = GetBoolAttributeWithNoDefault(reader, "sProHoAssocDuesLocked");
            sProOHExpLocked = GetBoolAttributeWithNoDefault(reader, "sProOHExpLocked");
            firstLoanRateMergeParameters = this.ReadXmlRateMergeParameters(reader);

            this.sSubFinT = (E_sSubFinT)this.GetIntAttribute(reader, "sSubFinT");
            this.sSubFinToPropertyValue_rep = this.GetStringAttribute(reader, "sSubFinToPropertyValue_rep");
            this.sSubFinPe_rep = this.GetStringAttribute(reader, "sSubFinPe_rep");
            this.sHCLTVRPe_rep = this.GetStringAttribute(reader, "sHCLTVRPe_rep");
            this.Version = this.GetIntAttribute(reader, "Version");
            this.sIsStudentLoanCashoutRefi = this.GetBoolAttribute(reader, "sIsStudentLoanCashoutRefi");

            bool isEmpty = reader.IsEmptyElement;
            reader.ReadStartElement(); //consume <PricingState>  

            if (!isEmpty)
            {
                //we should be on second loan info in this case
                this.secondLoanlLpTemplateId = this.GetGuidAttribute(reader, "lLpTemplateId");
                this.secondLoanlLpTemplateNm = this.GetStringAttribute(reader, "lLpTemplateNm");
                this.secondLoanParameters = this.ReadXmlRateMergeParameters(reader);
                this.secondLoanSelectedNoteRate = this.GetStringAttribute(reader, "SelectedNoteRate");
                this.secondLoanMonthlyPayment_rep = this.GetStringAttribute(reader, "MonthlyPayment");
                reader.ReadStartElement(); //consume <SecondLoanInfo/>
                reader.ReadEndElement(); //consume </PricingState>
            }
        }

        private void WriteRateMergeParameters(XmlWriter writer, RateMergeGroupKeyParameters parameters)
        {
            WriteSafeAttribute(writer, "lTerm_rep", parameters.lTerm_rep);
            WriteSafeAttribute(writer, "lDue_rep", parameters.lDue_rep);
            WriteSafeAttribute(writer, "lRadj1stCapMon_rep", parameters.lRAdj1stCapMon_rep);
            WriteSafeAttribute(writer, "lRadj1stCapR_rep", parameters.lRAdj1stCapR_rep);
            WriteSafeAttribute(writer, "lRAdjCapMon_rep", parameters.lRAdjCapMon_rep);
            WriteSafeAttribute(writer, "lRAdjCapR_rep", parameters.lRAdjCapR_rep);
            WriteSafeAttribute(writer, "lRAdjLifeCapR_rep", parameters.lRAdjLifeCapR_rep);
            WriteSafeAttribute(writer, "lArmIndexNameVstr", parameters.lArmIndexNameVstr);
            WriteSafeAttribute(writer, "lBuydwnR1_rep", parameters.lBuydwnR1_rep);
            WriteSafeAttribute(writer, "lBuydwnR2_rep", parameters.lBuydwnR2_rep);
            WriteSafeAttribute(writer, "lBuydwnR3_rep", parameters.lBuydwnR3_rep);
            WriteSafeAttribute(writer, "lBuydwnR4_rep", parameters.lBuydwnR4_rep);
            WriteSafeAttribute(writer, "lBuydwnR5_rep", parameters.lBuydwnR5_rep);
            WriteSafeAttribute(writer, "lFinMethT", parameters.lFinMethT.ToString("d"));
            WriteSafeAttribute(writer, "lLpProductType", parameters.lLpProductType);
            WriteSafeAttribute(writer, "lPrepmtPeriod_rep", parameters.lPrepmtPeriod_rep);
            WriteSafeAttribute(writer, "lHelocDraw_rep", parameters.lHelocDraw_rep);
            WriteSafeAttribute(writer, "lHelocRepay_rep", parameters.lHelocRepay_rep);
        }

        public RateMergeGroupKeyParameters ReadXmlRateMergeParameters(XmlReader reader)
        {
            RateMergeGroupKeyParameters parameters = new RateMergeGroupKeyParameters();
            parameters.lTerm_rep = GetStringAttribute(reader, "lTerm_rep");
            parameters.lDue_rep = GetStringAttribute(reader, "lDue_rep");
            parameters.lRAdj1stCapMon_rep = GetStringAttribute(reader, "lRadj1stCapMon_rep");
            parameters.lRAdj1stCapR_rep = GetStringAttribute(reader, "lRadj1stCapR_rep");
            parameters.lRAdjCapR_rep = GetStringAttribute(reader, "lRAdjCapR_rep");
            parameters.lRAdjLifeCapR_rep = GetStringAttribute(reader, "lRAdjLifeCapR_rep");
            parameters.lRAdjCapMon_rep = GetStringAttribute(reader, "lRAdjCapMon_rep");
            parameters.lArmIndexNameVstr = GetStringAttribute(reader, "lArmIndexNameVstr");
            parameters.lBuydwnR1_rep = GetStringAttribute(reader, "lBuydwnR1_rep");
            parameters.lBuydwnR2_rep = GetStringAttribute(reader, "lBuydwnR2_rep");
            parameters.lBuydwnR3_rep = GetStringAttribute(reader, "lBuydwnR3_rep");
            parameters.lBuydwnR4_rep = GetStringAttribute(reader, "lBuydwnR4_rep");
            parameters.lBuydwnR5_rep = GetStringAttribute(reader, "lBuydwnR5_rep");
            parameters.lFinMethT = (E_sFinMethT)GetIntAttribute(reader, "lFinMethT");
            parameters.lLpProductType = GetStringAttribute(reader, "lLpProductType");
            parameters.lPrepmtPeriod_rep = GetStringAttribute(reader, "lPrepmtPeriod_rep");
            parameters.lHelocDraw_rep = GetStringAttribute(reader, "lHelocDraw_rep");
            parameters.lHelocRepay_rep = GetStringAttribute(reader, "lHelocDraw_rep");
            return parameters;
        }

        public bool LoanDataMatches(PricingState other)
        {
            bool matches = true;
            matches = matches && sIsRenovationLoan == other.sIsRenovationLoan;
            matches = matches && sTotalRenovationCosts_rep == other.sTotalRenovationCosts_rep;
            matches = matches && sLPurposeTPe == other.sLPurposeTPe;
            matches = matches && sProdCashoutAmt_rep == other.sProdCashoutAmt_rep;
            matches = matches && sApprValPe_rep == other.sApprValPe_rep;
            matches = matches && sHouseValPe_rep == other.sHouseValPe_rep;
            matches = matches && sAsIsAppraisedValuePeval_rep == other.sAsIsAppraisedValuePeval_rep;
            matches = matches && sDownPmtPcPe_rep == other.sDownPmtPcPe_rep;
            matches = matches && sEquityPe_rep == other.sEquityPe_rep;
            matches = matches && sLtvRPe_rep == other.sLtvRPe_rep;
            matches = matches && sLAmtCalcPe_rep == other.sLAmtCalcPe_rep;
            matches = matches && sHas2ndFinPe == other.sHas2ndFinPe;
            matches = matches && sLtvROtherFinPe_rep == other.sLtvROtherFinPe_rep;
            matches = matches && sProOFinBalPe_rep == other.sProOFinBalPe_rep;
            matches = matches && sCltvRPe_rep == other.sCltvRPe_rep;
            matches = matches && sSpAddr == other.sSpAddr;
            matches = matches && sSpZip == other.sSpZip;
            matches = matches && sSpStatePe == other.sSpStatePe;
            matches = matches && sSpCounty == other.sSpCounty;
            matches = matches && sSpCity == other.sSpCity;
            matches = matches && sProdSpT == other.sProdSpT;
            matches = matches && sProdSpStructureT == other.sProdSpStructureT;
            matches = matches && sHasNonOccupantCoborrowerPe == other.sHasNonOccupantCoborrowerPe;
            matches = matches && sProdImpound == other.sProdImpound;
            matches = matches && sIsIOnlyPe == other.sIsIOnlyPe;
            matches = matches && sProd3rdPartyUwResultT == other.sProd3rdPartyUwResultT;
            matches = matches && sProdIsDuRefiPlus == other.sProdIsDuRefiPlus;
            matches = matches && sProdRLckdDays_rep == other.sProdRLckdDays_rep;
            matches = matches && sProdIsSpInRuralArea == other.sProdIsSpInRuralArea;
            //matches = matches && sProRealETxPe_rep == other.sProRealETxPe_rep;
            matches = matches && sSellerCreditT == other.sSellerCreditT;
            matches = matches && sOccTPe == other.sOccTPe;
            //matches = matches && sProHazInsPe_rep == other.sProHazInsPe_rep;
            //matches = matches && sProHoAssocDuesPe_rep == other.sProHoAssocDuesPe_rep;
            //matches = matches && sProOHExpDesc == other.sProOHExpDesc;
            //matches = matches && sProOHExpPe_rep == other.sProOHExpPe_rep;
            matches = matches && sIsCreditQualifying == other.sIsCreditQualifying;
            matches = matches && sPriorSalesD_rep == other.sPriorSalesD_rep;
            matches = matches && sProdDocT == other.sProdDocT;
            matches = matches && sLienPosT == other.sLienPosT;
            matches = matches && sNumFinancedProperties_rep == other.sNumFinancedProperties_rep;
            matches = matches && sProdConvMIOptionT == other.sProdConvMIOptionT;
            matches = matches && sProdIsUFMIPFFFinanced == other.sProdIsUFMIPFFFinanced;
            matches = matches && sProdOverrideUFMIPFF == other.sProdOverrideUFMIPFF;
            matches = matches && sProdFhaUfmip_rep == other.sProdFhaUfmip_rep;
            matches = matches && sProdVaFundingFee_rep == other.sProdVaFundingFee_rep;
            matches = matches && sProdUSDAGuaranteeFee_rep == other.sProdUSDAGuaranteeFee_rep;
            //matches = matches && sTitleInsF_rep == other.sTitleInsF_rep;
            //matches = matches && sRecFPe_rep == other.sRecFPe_rep;
            //matches = matches && sCountyRtcPe_rep == other.sCountyRtcPe_rep;
            //matches = matches && sOwnerTitleInsF_rep == other.sOwnerTitleInsF_rep;
            matches = matches && sOriginatorCompensationPaymentSourceT == other.sOriginatorCompensationPaymentSourceT;
            matches = matches && sConvSplitMIRT == other.sConvSplitMIRT;
            matches = matches && sSubFinT == other.sSubFinT;
            matches = matches && sIsStudentLoanCashoutRefi == other.sIsStudentLoanCashoutRefi;
            matches = matches && this.sSubFinToPropertyValue_rep == other.sSubFinToPropertyValue_rep;

            return matches;
        }

        public void WriteXml(XmlWriter writer)
        {
            WriteSafeAttribute(writer, "FriendlyId", FriendlyId.ToString());
            WriteSafeAttribute(writer, "Id", Id.ToString());

            WriteSafeAttribute(writer, "sIsRenovationLoan", sIsRenovationLoan.ToString());
            WriteSafeAttribute(writer, "sTotalRenovationCosts_rep", sTotalRenovationCosts_rep);
            WriteSafeAttribute(writer, "sLPurposeTPe", sLPurposeTPe.ToString("d"));
            WriteSafeAttribute(writer, "sProdCashoutAmt_rep", sProdCashoutAmt_rep);
            WriteSafeAttribute(writer, "sApprValPe_rep", sApprValPe_rep);
            WriteSafeAttribute(writer, "sHouseValPe_rep", sHouseValPe_rep);
            WriteSafeAttribute(writer, "sAsIsAppraisedValuePeval_rep", sAsIsAppraisedValuePeval_rep);
            WriteSafeAttribute(writer, "sDownPmtPcPe_rep", sDownPmtPcPe_rep);
            WriteSafeAttribute(writer, "sEquityPe_rep", sEquityPe_rep);
            WriteSafeAttribute(writer, "sLtvRPe_rep", sLtvRPe_rep);
            WriteSafeAttribute(writer, "sLAmtCalcPe_rep", sLAmtCalcPe_rep);
            WriteSafeAttribute(writer, "sHas2ndFinPe", sHas2ndFinPe.ToString());
            WriteSafeAttribute(writer, "sLtvROtherFinPe_rep", sLtvROtherFinPe_rep);
            WriteSafeAttribute(writer, "sProOFinBalPe_rep", sProOFinBalPe_rep);
            WriteSafeAttribute(writer, "sCltvRPe_rep", sCltvRPe_rep);
            WriteSafeAttribute(writer, "sSpAddr", sSpAddr);
            WriteSafeAttribute(writer, "sSpZip", sSpZip);
            WriteSafeAttribute(writer, "sSpStatePe", sSpStatePe);
            WriteSafeAttribute(writer, "sSpCounty", sSpCounty);
            WriteSafeAttribute(writer, "sSpCity", sSpCity);
            WriteSafeAttribute(writer, "sProdSpT", sProdSpT.ToString("d"));
            WriteSafeAttribute(writer, "sProdSpStructureT", sProdSpStructureT.ToString("d"));
            WriteSafeAttribute(writer, "sHasNonOccupantCoborrowerPe", sHasNonOccupantCoborrowerPe.ToString());
            WriteSafeAttribute(writer, "sProdImpound", sProdImpound.ToString());
            WriteSafeAttribute(writer, "sIsIOnlyPe", sIsIOnlyPe.ToString());
            WriteSafeAttribute(writer, "sProd3rdPartyUwResultT", sProd3rdPartyUwResultT.ToString("d"));
            WriteSafeAttribute(writer, "sProdIsDuRefiPlus", sProdIsDuRefiPlus.ToString());
            WriteSafeAttribute(writer, "sProdRLckdDays_rep", sProdRLckdDays_rep);
            WriteSafeAttribute(writer, "sProdCalcEntryT", sProdCalcEntryT.ToString("d"));
            WriteSafeAttribute(writer, "sProdIsSpInRuralArea", sProdIsSpInRuralArea.ToString());
            WriteSafeAttribute(writer, "sProRealETxPe_rep", sProRealETxPe_rep);
            WriteSafeAttribute(writer, "sSellerCreditT", sSellerCreditT.ToString("d"));
            WriteSafeAttribute(writer, "sOccTPe", sOccTPe.ToString("d"));
            WriteSafeAttribute(writer, "sProHazInsPe_rep", sProHazInsPe_rep);
            WriteSafeAttribute(writer, "sProHoAssocDuesPe_rep", sProHoAssocDuesPe_rep);
            WriteSafeAttribute(writer, "sProOHExpDesc", sProOHExpDesc);
            WriteSafeAttribute(writer, "sProOHExpPe_rep", sProOHExpPe_rep);
            WriteSafeAttribute(writer, "sPriorSalesD_rep", sPriorSalesD_rep);
            WriteSafeAttribute(writer, "sIsCreditQualifying", sIsCreditQualifying.ToString());
            WriteSafeAttribute(writer, "sProdDocT", sProdDocT.ToString("d"));
            WriteSafeAttribute(writer, "sLienPosT", sLienPosT.ToString("d"));
            WriteSafeAttribute(writer, "sNumFinancedProperties_rep", sNumFinancedProperties_rep);
            WriteSafeAttribute(writer, "sProdConvMIOptionT", sProdConvMIOptionT.ToString("d"));
            WriteSafeAttribute(writer, "sProdIsUFMIPFFFinanced", sProdIsUFMIPFFFinanced.ToString());
            WriteSafeAttribute(writer, "sProdOverrideUFMIPFF", sProdOverrideUFMIPFF.ToString());
            WriteSafeAttribute(writer, "sProdFhaUfmip_rep", sProdFhaUfmip_rep);
            WriteSafeAttribute(writer, "sProdVaFundingFee_rep", sProdVaFundingFee_rep);
            WriteSafeAttribute(writer, "sProdUSDAGuaranteeFee_rep", sProdUSDAGuaranteeFee_rep);

            WriteSafeAttribute(writer, "IsRateMerge", IsRateMerge.ToString());
           
            WriteSafeAttribute(writer, "lLpTemplateId", lLpTemplateId.ToString());
            WriteSafeAttribute(writer, "lLT", lLt.ToString("d"));
            WriteSafeAttribute(writer, "SelectedNoteRate", SelectedNoteRate);
            WriteSafeAttribute(writer, "lLpTemplateNm", lLpTemplateNm);

            WriteSafeAttribute(writer, "sTitleInsF_rep", sTitleInsF_rep);
            WriteSafeAttribute(writer, "sRecFPe_rep", sRecFPe_rep);
            WriteSafeAttribute(writer, "sCountyRtcPe_rep", sCountyRtcPe_rep);
            WriteSafeAttribute(writer, "sOwnerTitleInsF_rep", sOwnerTitleInsF_rep);

            WriteSafeAttribute(writer, "IsForRateMonitor", IsForRateMonitor.ToString());
            WriteSafeAttribute(writer, "sOriginatorCompensationPaymentSourceT", sOriginatorCompensationPaymentSourceT.ToString("d"));
            WriteSafeAttribute(writer, "sConvSplitMIRT", sConvSplitMIRT.ToString("d"));

            WriteSafeAttribute(writer, "sProRealETxLocked", sProRealETxLocked.ToString());
            WriteSafeAttribute(writer, "sProHazInsLocked", sProHazInsLocked.ToString());
            WriteSafeAttribute(writer, "sProHoAssocDuesLocked", sProHoAssocDuesLocked.ToString());
            WriteSafeAttribute(writer, "sProOHExpLocked", sProOHExpLocked.ToString());
            WriteSafeAttribute(writer, "sSubFinT", this.sSubFinT.ToString("d"));
            WriteSafeAttribute(writer, "sSubFinToPropertyValue_rep", this.sSubFinToPropertyValue_rep);
            WriteSafeAttribute(writer, "sSubFinPe_rep", this.sSubFinPe_rep);
            WriteSafeAttribute(writer, "sHCLTVRPe_rep", this.sHCLTVRPe_rep);
            WriteSafeAttribute(writer, "Version", this.Version.ToString());

            WriteRateMergeParameters(writer, firstLoanRateMergeParameters);
            WriteSafeAttribute(writer, "sIsStudentLoanCashoutRefi", this.sIsStudentLoanCashoutRefi.ToString());

            if (this.IsComboScenario)
            {
                writer.WriteStartElement("SecondLoan");
                WriteRateMergeParameters(writer, secondLoanParameters);
                WriteSafeAttribute(writer, "lLpTemplateId", this.secondLoanlLpTemplateId.ToString());
                WriteSafeAttribute(writer, "lLpTemplateNm", secondLoanlLpTemplateNm);
                WriteSafeAttribute(writer, "SelectedNoteRate", secondLoanSelectedNoteRate);
                WriteSafeAttribute(writer, "MonthlyPayment", secondLoanMonthlyPayment_rep);
                writer.WriteEndElement();
            }
        }


        public XmlSchema GetSchema()
        {
            return null;
        }

        #endregion



    }
}
