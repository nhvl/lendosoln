﻿namespace DataAccess.LoanComparison
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using LendersOffice.Admin;
    using LendersOffice.Constants;
    using LendersOffice.LoanSearch;
    using LendersOffice.ObjLib.LoanSearch.Basic;
    using LendersOffice.ObjLib.Security;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    public class DuplicatePMLSubmissionException : CBaseException
    {
        public IEnumerable<DuplicateResult> Duplicates
        {
            get;
            private set;
        }
        public DuplicatePMLSubmissionException(IEnumerable<DuplicateResult> duplicates)
            : base(
                "Attempt made to register / lock duplicate loan(s): " + string.Join(", ", duplicates.Select(d => d.LoanName).ToArray())
              , "Attempt made to register / lock duplicate loan(s): " + string.Join(", ", duplicates.Select(d => d.LoanName).ToArray())
            ) 
        {
            IsEmailDeveloper = false;
            Duplicates = duplicates;
        }
    }

    public class DuplicateResult
    {
        public Guid LoanID
        {
            get;
            private set;
        }
        public string LoanName
        {
            get;
            private set;
        }
        public DuplicateResult(Guid loanID, string loanName)
        {
            LoanID = loanID;
            LoanName = loanName;
        }
    }
    public class DuplicateFinder
    {
        static private readonly string[][] wlist = {
            new string[] {"John","Homeowner","999-40-5000"},
            new string[] {"Mary","Homeowner","500-22-2000"},
            new string[] {"Andy","America","999-60-3333"},
            new string[] {"Amy","America","500-60-2222"},
            new string[] {"Patrick","Purchaser","999-12-1234"},
            new string[] {"Lorraine","Purchaser","999-56-5678"},
            new string[] {"Suzi","Builder","999-60-6666"},
            new string[] {"Alice","Firstimer","991-91-9991"},
            new string[] {"Penny","Public","999-60-3000"},
            new string[] {"Dad","Firstimer","999-00-8881"},
            new string[] {"Mom","Firstimer","999-00-8882"},
            new string[] {"Ross","Blemished","000-88-9999"},
            new string[] {"Wanna","House","000-11-2222"},
            new string[] {"Needa","House","999-44-5555"},
            new string[] {"Elizabeth","Spender","000-66-7777"},
            new string[] {"Ken","Customer","500-50-7000"},
            new string[] {"Homer","Loanseeker","999-60-1111"},
            new string[] {"Mort","Gage","000-00-0002"},
            new string[] {"Ann","Gage","000-00-0003"},
            new string[] {"Sonny","Graves","001-01-0021"},
            new string[] {"Loco","Motion","999-47-8801"},
            new string[] {"Pitt","Rock","666-00-6666"},
            new string[] {"Ron","Tintin","999-72-5641"},
            new string[] {"Lucky","Knowscore","999-42-2345"},
            new string[] {"Donna","Testcase","000-00-0020"},
            new string[] {"David","Testcase","000-00-0002"},
            new string[] {"Marisol","Testcase","000-00-0001"}
        };

        const int SECONDS_BEFORE_DUPLICATE_RESULTS_EXPIRE = 3;

        #region ( private methods )
        static string GetLastDuplicateResultsKey(Guid loanID)
        {
            return "LastDuplicateResults_" + loanID;
        }
        static string GetLastTimeDuplicateCheckRanKey(Guid loanID)
        {
            return "LastTimeDuplicateCheckRan_" + loanID;
        }
        static string GetSkipLoanSubmissionCheckKey(Guid loanID)
        {
            return "SkipPMLDuplicateLoanSubmissionCheck_" + loanID;
        }
        

        static bool GetSkipLoanSubmissionCheck(Guid loanID)
        {
            var context = System.Web.HttpContext.Current;
            if (context != null)
            {
                if(context.Items.Contains(GetSkipLoanSubmissionCheckKey(loanID)))
                {
                    return (bool)context.Items[GetSkipLoanSubmissionCheckKey(loanID)];
                }
            }
            return false;
        }
        
        /// <summary>
        /// Get's last results found on this request for this loan, if it hasn't been more than SECONDS_BEFORE_DUPLICATE_RESULTS_EXPIRE.
        /// 
        /// This was tested by calling one of the public methods twice, and putting a breakpoint on the return of the cached results.
        /// (Can't have a unit test for that.)
        /// </summary>
        static IEnumerable<DuplicateResult> GetLastDuplicateResults(Guid loanID)
        {
            var context = System.Web.HttpContext.Current;
            if (null != context)
            {
                DateTime? lastTimeDuplicateCheckRan = null;
                if (context.Items.Contains(GetLastTimeDuplicateCheckRanKey(loanID)))
                {
                    lastTimeDuplicateCheckRan = (DateTime?)context.Items[GetLastTimeDuplicateCheckRanKey(loanID)];
                }
                if (lastTimeDuplicateCheckRan != null)
                {
                    if (DateTime.Now - lastTimeDuplicateCheckRan.Value < TimeSpan.FromSeconds(SECONDS_BEFORE_DUPLICATE_RESULTS_EXPIRE))
                    {
                        if (context.Items.Contains(GetLastDuplicateResultsKey(loanID)))
                        { 
                            return (List<DuplicateResult>)context.Items[GetLastDuplicateResultsKey(loanID)];
                        }
                    }
                }
            }
            return null;
        }
        static void SetLastDuplicateResults(Guid loanID, List<DuplicateResult> value)
        {
            var context = System.Web.HttpContext.Current;
            if (null != context)
            {
                context.Items[GetLastDuplicateResultsKey(loanID)] = value;
                context.Items[GetLastTimeDuplicateCheckRanKey(loanID)] = DateTime.Now;
            }
        }

        private static bool StreetNumberAndApartmentsMatch(CommonLib.Address addr1, CommonLib.Address addr2)
        {
            return (
                    string.Compare(addr1.AptNum, addr2.AptNum, true) == 0
                && string.Compare(addr1.StreetNumber, addr2.StreetNumber, true) == 0
                );
        }

        /// <summary>
        /// Parses data from reader and fills a dictionary of loan IDs to loan details.
        /// </summary>
        /// <param name="records">A list of dictionaries representing data records from the DB.</param>
        /// <returns>A dictionary of loan IDs to loan details.</returns>
        private static Dictionary<Guid, LoanDetails> ParseDuplicateLoanDetailsPer105725(IEnumerable<Dictionary<string, object>> records)
        {
            Dictionary<Guid, LoanDetails> loansById = new Dictionary<Guid, LoanDetails>();

            foreach (Dictionary<string, object> record in records)
            {
                LoanDetails details;
                Guid loanId = (Guid)record["sLId"];
                if (!loansById.TryGetValue(loanId, out details))
                {
                    DuplicateLoanDetailsPer105725 detailsPer105725 = new DuplicateLoanDetailsPer105725();
                    detailsPer105725.sLNm = record["sLNm"] as string;
                    detailsPer105725.sSpAddr = record["sSpAddr"] as string;
                    detailsPer105725.sLId = (Guid)record["sLId"];
                    detailsPer105725.sStatusT = (E_sStatusT)record["sStatusT"];
                    detailsPer105725.sBranchChannelT = (E_BranchChannelT)record["sBranchChannelT"];
                    detailsPer105725.Applicants = new List<ApplicantDetails>();
                    loansById.Add(loanId, detailsPer105725);

                    details = detailsPer105725;
                }

                ApplicantDetails appDetails = LoanSearchUtilities.ParseApplicantDetails(record);

                details.Applicants.Add(appDetails);
            }

            return loansById;
        }

        /// <summary>
        /// Parses data from reader and fills a dictionary of loan IDs to loan details.
        /// </summary>
        /// <param name="records">A list of dictionaries representing data records from the DB.</param>
        /// <returns>A dictionary of loan IDs to loan details.</returns>
        private static Dictionary<Guid, LoanDetails> ParsePmlDuplicateLoanDetails(IEnumerable<Dictionary<string, object>> records)
        {
            Dictionary<Guid, LoanDetails> loansById = new Dictionary<Guid, LoanDetails>();

            foreach (Dictionary<string, object> record in records)
            {
                LoanDetails details;
                Guid loanId = (Guid)record["sLId"];
                if (!loansById.TryGetValue(loanId, out details))
                {
                    PmlDuplicateLoanDetails pmlDetails = new PmlDuplicateLoanDetails();
                    pmlDetails.sLId = (Guid)record["sLId"];
                    pmlDetails.sLNm = record["sLNm"] as string;
                    pmlDetails.sStatusD = record["sStatusD"] as DateTime?;
                    pmlDetails.sStatusT = (E_sStatusT)record["sStatusT"];
                    pmlDetails.sNumOfActiveConditionOnly = record["sNumOfActiveConditionOnly"] as int?;
                    pmlDetails.sNumOfActiveDueTodayConditionOnly = record["sNumOfActiveDueTodayConditionOnly"] as int?;
                    pmlDetails.sNumOfActivePastDueConditionOnly = record["sNumOfActivePastDueConditionOnly"] as int?;
                    pmlDetails.sNumOfActiveTasksOnly = record["sNumOfActiveTasksOnly"] as int?;
                    pmlDetails.sNumOfActiveDueTodayTasksOnly = record["sNumOfActiveDueTodayTasksOnly"] as int?;
                    pmlDetails.sNumOfActivePastDueTasksOnly = record["sNumOfActivePastDueTasksOnly"] as int?;
                    pmlDetails.sLAmtCalc = record["sLAmtCalc"] as decimal?;
                    pmlDetails.sApprovalCertPdfLastSavedD = record["sApprovalCertPdfLastSavedD"] as DateTime?;
                    pmlDetails.sSuspenseNoticePdfLastSavedD = record["sSuspenseNoticePdfLastSavedD"] as DateTime?;
                    pmlDetails.sRateLockConfirmationPdfLastSavedD = record["sRateLockConfirmationPdfLastSavedD"] as DateTime?;
                    pmlDetails.sRLckdExpiredD = record["sRLckdExpiredD"] as DateTime?;
                    pmlDetails.sRateLockStatusT = (E_sRateLockStatusT)record["sRateLockStatusT"];
                    pmlDetails.sRateLockStatusT_rep = record["sRateLockStatusT_rep"] as string;
                    pmlDetails.sEmployeeLoanRepName = record["sEmployeeLoanRepName"] as string;
                    pmlDetails.sEmployeeBrokerProcessorName = record["sEmployeeBrokerProcessorName"] as string;
                    pmlDetails.sEmployeeExternalSecondaryName = record["sEmployeeExternalSecondaryName"] as string;
                    pmlDetails.sEmployeeExternalPostCloserName = record["sEmployeeExternalPostCloserName"] as string;
                    pmlDetails.aBLastNm = record["loan_aBLastNm"] as string;
                    pmlDetails.aBFirstNm = record["loan_aBFirstNm"] as string;
                    pmlDetails.LoanStatus = record["LoanStatus"] as string;
                    pmlDetails.sSpAddr = record["sSpAddr"] as string;
                    pmlDetails.InScope = record["InScope"] as int?;
                    pmlDetails.CanRead = record["CanRead"] as int?;
                    pmlDetails.Applicants = new List<ApplicantDetails>();
                    loansById.Add(loanId, pmlDetails);

                    details = pmlDetails;
                }

                ApplicantDetails appDetails = LoanSearchUtilities.ParseApplicantDetails(record);

                details.Applicants.Add(appDetails);
            }

            return loansById;
        }
        #endregion

        #region ( public )

        /// <summary>
        /// Set this to true per request IF AND ONLY IF you want to skip the duplicate loan submission check on that request.
        /// </summary>
        public static bool SetSkipLoanSubmissionCheck(Guid loanID, bool value)
        {
            var context = System.Web.HttpContext.Current;
            if (context != null)
            {
                context.Items[GetSkipLoanSubmissionCheckKey(loanID)] = value;
            }
            return false;
        }

        /// <summary>
        /// If it returns empty.  (*Note this should be set based on user permissions and only if the user desired it.)
        ///     ( Like if the lock desk wants to manually lock the loan. )
        /// If ConstStage.DisablePMLDuplicateSubmissionBlocker, it returns empty.
        /// If BrokerDB.DisablePMLDuplicateSubmissionBlocker, it returns empty.
        /// 
        /// Finds the names of the loans that are duplicates of the input loan per 105723, namely:
        ///  - at least one borrower matches ssn  (provided not empty)
        ///  - at least one borrower matches last name. (provided not empty / null.)
        ///  - loans that are in locked or lock requested status
        /// excluding loans not in archived status.
        /// loan purpose must match
        ///  - subject property's "match" - (street number, county, state, zip, not city as it could be misspelled.)
        ///  - lien type (first or second) match.
        ///  - valid loans.
        ///  - not templates.
        ///  - at the same lender.
        /// </summary>
        public static IEnumerable<DuplicateResult> FindDuplicateLoansPer105723(Guid loanID, Guid brokerID, string sSpAddr)
        {
            return FindDuplicateLoansPer105723Impl(loanID, brokerID, sSpAddr, null, null, null, null, null);
        }

        public static IEnumerable<DuplicateResult> FindDuplicateLoansPer105723(Guid loanID, Guid brokerID, PricingState pricingState)
        {
            return FindDuplicateLoansPer105723Impl(loanID, brokerID, pricingState.sSpAddr, pricingState.sSpStatePe,
                pricingState.sSpCounty, pricingState.sSpZip, pricingState.sLienPosT, pricingState.sLPurposeTPe);
        }

        public static IEnumerable<DuplicateResult> FindDuplicateLoansPer105723Impl(Guid loanID, Guid brokerID, string addr,
            string state, string county, string zip, E_sLienPosT? lienPositionType,  E_sLPurposeT? loanPurposeType)
        {
            var duplicateLoans = new List<DuplicateResult>();

            // Allow for this method to be disabled at multiple levels.
            if (    GetSkipLoanSubmissionCheck(loanID) // defaults to false
                || ConstStage.DisablePMLDuplicateSubmissionBlocker 
                || BrokerDB.RetrieveById(brokerID).DisablePMLDuplicateSubmissionBlocker
                )
            {
                return duplicateLoans;
            }

            // prevent running this expensive operation if we're on the same request and loan and it's not been too long.
            var lastResults = GetLastDuplicateResults(loanID);
            if (lastResults != null) 
            {
                return lastResults;
            }

            List<ApplicantDetails> applicantDetails = GetApplicants(brokerID, loanID);

            //If all borrowers are in the whitelist, they will be exempted from checking duplicates.
            bool isExempt = exemptByWhiteListHardcode(applicantDetails);
//            Tools.LogInfo("isExempt:" + isExempt);
            if (isExempt)
            {
                return duplicateLoans;
            }

            var parameters = new List<SqlParameter>() { new SqlParameter("@slid", loanID) };
            
            if (state != null) { parameters.Add(new SqlParameter("@loanState", state)); }
            if (county != null) { parameters.Add(new SqlParameter("@loanCounty", county)); }
            if (zip != null) { parameters.Add(new SqlParameter("@loanZip", zip)); }
            if (lienPositionType != null) { parameters.Add(new SqlParameter("@loanLienPosT", lienPositionType.Value)); }
            if (loanPurposeType != null) { parameters.Add(new SqlParameter("@loanPurposeT", loanPurposeType.Value)); }

            List<Dictionary<string, object>> records = new List<Dictionary<string, object>>();
            using (var reader = StoredProcedureHelper.ExecuteReader(brokerID, "FindPossibleDuplicatesPer105723", parameters))
            {
                while (reader.Read())
                {
                    records.Add(reader.ToDictionary());
                }
            }

            Dictionary<Guid, LoanDetails> loansById;
            loansById = ParseDuplicateLoanDetailsPer105725(records);

            List<ApplicantCriteria> criteria = new List<ApplicantCriteria>();

            foreach (ApplicantDetails applicantDetail in applicantDetails)
            {
                criteria.Add(new ApplicantCriteria() { FirstName = string.Empty, LastName = applicantDetail.aBLastNm, SSN = applicantDetail.aBSsn });
                criteria.Add(new ApplicantCriteria() { FirstName = string.Empty, LastName = applicantDetail.aCLastNm, SSN = applicantDetail.aCSsn });
            }

            foreach (DuplicateLoanDetailsPer105725 loanDetails in LoanSearchUtilities.FilterLoansByAppData(criteria, loansById))
            {
                if (IsLoanStatusBeforeFundingStage(loanDetails.sStatusT, loanDetails.sBranchChannelT)
                    && CommonLib.Address.AreEquivalentByComparator(addr, loanDetails.sSpAddr, StreetNumberAndApartmentsMatch))
                {
                    duplicateLoans.Add(new DuplicateResult(loanDetails.sLId, loanDetails.sLNm));
                }
            }

            SetLastDuplicateResults(loanID, duplicateLoans);
            return duplicateLoans;
        }

        public static IEnumerable<Dictionary<string, object>> FindDuplicateLoansForPmlPipeline(
            AbstractUserPrincipal principal,
            string firstNm,
            string lastNm,
            string ssn,
            E_PortalMode PortalMode = E_PortalMode.Blank,
            Dictionary<E_sCorrespondentProcessT, HashSet<E_sStatusT>> sStatusTDictionary = null,
            HashSet<E_sStatusT> sStatusTFilters = null)
        {
            LendingQBSearchFilter searchFilter;
            {
                searchFilter = new LendingQBSearchFilter
                {
                    aBFirstNm = firstNm,
                    aBLastNm = lastNm,
                    IsDuplicateSearch = true,
                    LoanAssignedToT = 0, // Assigned to Anyone
                    sStatusT = -1, // Any status
                    sStatusDateT = -1, // All dates
                    PortalMode = PortalMode,
                    sStatusTDictionary = sStatusTDictionary,
                    sStatusTFilters = sStatusTFilters
                };
            }

            DataSet ds = LendingQBPmlSearch.Search(principal, searchFilter, 100);
            DataTable dt = ds.Tables[0];

            List<Dictionary<string, object>> records = dt.Rows.Cast<DataRow>().Select(row => dt.Columns.Cast<DataColumn>()
                .ToDictionary(col => col.ColumnName, col => row[col.ColumnName])).ToList();

            Dictionary<Guid, LoanDetails> loansById;
            loansById = ParsePmlDuplicateLoanDetails(records);

            ApplicantCriteria[] criteria = new ApplicantCriteria[] {
                new ApplicantCriteria() { FirstName = firstNm, LastName = lastNm, SSN = ssn }
            };

            List<Dictionary<string, object>> duplicateLoans = new List<Dictionary<string, object>>();
            foreach (PmlDuplicateLoanDetails loanDetails in LoanSearchUtilities.FilterLoansByAppData(criteria, loansById))
            {
                duplicateLoans.Add(loanDetails.ToDictionary());
            }

            return duplicateLoans;
        }

        /// <summary>
        /// Verifies that the given loan status occurs earlier in the order than either the Funded
        /// or Purchased status, depending on the branch channel.
        /// </summary>
        /// <param name="status">The loan status to check.</param>
        /// <param name="channel">The branch channel.</param>
        private static bool IsLoanStatusBeforeFundingStage(E_sStatusT status, E_BranchChannelT channel)
        {
            if (status == E_sStatusT.Loan_Archived || status == E_sStatusT.Loan_Withdrawn ||
                status == E_sStatusT.Loan_Canceled || status == E_sStatusT.Loan_Rejected)
            {
                return false;
            }

            E_sStatusT targetStatus;
            if (channel == E_BranchChannelT.Retail || channel == E_BranchChannelT.Wholesale || channel == E_BranchChannelT.Broker)
            {
                targetStatus = E_sStatusT.Loan_Funded;
            }
            else
            {
                targetStatus = E_sStatusT.Loan_Purchased;
            }

            foreach (var orderedStatus in CPageBase.s_sStatusT_Order)
            {
                if (orderedStatus == targetStatus)
                {
                    break;
                }

                if (orderedStatus == status)
                {
                    return true;
                }
            }

            return false;
        }

        private static List<ApplicantDetails> GetApplicants(Guid brokerId, Guid loanID)
        {
            List<ApplicantDetails> applicants = new List<ApplicantDetails>();

            SqlParameter[] parameters = { new SqlParameter("@slid", loanID) };
            List<Dictionary<string, object>> records = new List<Dictionary<string, object>>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "FindBorrowersForWhiteList", parameters))
            {
                while (reader.Read())
                {
                    records.Add(reader.ToDictionary());
                }
            }

            foreach (Dictionary<string, object> record in records)
            {
                ApplicantDetails details = LoanSearchUtilities.ParseApplicantDetails(record);
                applicants.Add(details);
            }

            return applicants;
        }

        //if all borrowers are in the whitelist, return true; else return false;
        private static bool exemptByWhiteListHardcode(IEnumerable<ApplicantDetails> applicantDetails)
        {
            foreach (ApplicantDetails applicant in applicantDetails)
            {
                if ((!string.IsNullOrWhiteSpace(applicant.aBFirstNm) || !string.IsNullOrWhiteSpace(applicant.aBLastNm) || !string.IsNullOrEmpty(applicant.aBSsn))
                    && !ExistInWhiteList(wlist, applicant.aBFirstNm, applicant.aBLastNm, applicant.aBSsn))
                {
                    return false;
                }

                if ((!string.IsNullOrWhiteSpace(applicant.aCFirstNm) || !string.IsNullOrWhiteSpace(applicant.aCLastNm) || !string.IsNullOrEmpty(applicant.aCSsn))
                    && !ExistInWhiteList(wlist, applicant.aCFirstNm, applicant.aCLastNm, applicant.aCSsn))
                {
                    return false;
                }
            }

            return true;
        }

        private static bool ExistInWhiteList(string[][] tlist, string pFirstNm, string pLastNm, string pSsn)
        {
            bool isFound = false;

            for (int i = 0; i < tlist.Length; i++)
            {
                if (pFirstNm == tlist[i][0]
                    && pLastNm == tlist[i][1]
                    && pSsn == tlist[i][2])
                {
                    isFound = true;
                    break;
                }
            }

            return isFound;
        }

        public static bool ExistDuplicateLoansPer105723(Guid loanID, Guid brokerID, string sSpAddr)
        {
            return FindDuplicateLoansPer105723(loanID, brokerID, sSpAddr).Count() > 0;
        }
        public static bool ExistDuplicateLoansPer105723(Guid loanID, Guid brokerID, PricingState pricingState)
        {
            return FindDuplicateLoansPer105723(loanID, brokerID, pricingState).Count() > 0;
        }

        
        #endregion        
    }
}
