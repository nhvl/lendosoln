﻿namespace DataAccess.LoanComparison
{
    using LendersOffice.ObjLib.LoanSearch.Basic;

    /// <summary>
    /// Details about a loan file, used by DuplicateFinder for finding duplicates per OPM 105723.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "I want the property names to match their LFF names.")]
    public class DuplicateLoanDetailsPer105725 : LoanDetails
    {
        /// <summary>
        /// Gets or sets the branch channel.
        /// </summary>
        /// <value>The branch channel.</value>
        public E_BranchChannelT sBranchChannelT { get; set; }
    }
}
