﻿namespace LendersOffice.ObjLib.ComparisonReport
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Xml.Linq;

    public class ConfirmationUrlGenerator
    {
        private List<Tuple<string, string>> queryStrings;

        private void Append(string key, string value)
        {
            queryStrings.Add(Tuple.Create(key, value));
        }

        private void Append(string key, XElement parentElement, string id)
        {
            this.Append(key, parentElement, id, null);
        }

        private void Append(string key, XElement parentElement, string id, Func<string, string> transform)
        {
            string value = this.Get(parentElement, id);
            if (transform != null)
            {
                value = transform(value);
            }
            this.Append(key, value);
        }

        private void Append(string key, bool value, string truthyValue, string falslyValue)
        {
            string d = value ? truthyValue : falslyValue;
            this.Append(key, d);
        }

        public string GenerateUrl(XDocument comparisonXml, Guid pinId, bool isLock, string confirmationPath)
        {
            string pinIdRep = pinId.ToString("N");
            this.queryStrings = new List<Tuple<string, string>>();

            XElement pinStateResult = comparisonXml.Descendants("LoanProgram").FirstOrDefault(p => p.Attribute("PinId").Value == pinIdRep);

            if (pinStateResult == null)
            {
                return null;
            }

            this.Append("pinid", pinId.ToString("N"));
            this.Append("requestratelock", isLock, "t", "f");
            this.HandleGeneral(comparisonXml);
            this.HandleResultData(pinStateResult);

            StringBuilder sb = new StringBuilder();
            sb.Append(confirmationPath);
            sb.Append("?");

            foreach (var queryString in this.queryStrings)
            {
                sb.Append($"{queryString.Item1}={queryString.Item2}&");
            }

            sb.Remove(sb.Length - 1, 1);
            return sb.ToString();
        }

        private void HandleGeneral(XDocument comparisonXml)
        {
            string loanId = comparisonXml.Element("LoanComparison").Element("LoanInfo").Element("sLId").Value;
            string debugResultStartTicks = comparisonXml.Element("LoanComparison").Element("Report").Element("GenerationTimeTicks").Value;

            this.Append("loanid", loanId);
            this.Append("debugResultStartTicks", debugResultStartTicks);
        }

        private void HandleResultData(XElement resultElement)
        {
            bool isComboScenario = resultElement.Element("IsComboScenario").Value == "True";

            this.Append("Has2ndLoan", isComboScenario.ToString());

            if (isComboScenario)
            {
                this.HandleComboScenario(resultElement);
            }
            else
            {
                this.HandleSingleScenario(resultElement);
            }
        }

        private void HandleSingleScenario(XElement resultElement)
        {
            this.Append("productId", resultElement, "lLpTemplateId");
            this.Append("rate", resultElement, "sNoteIR");
            this.Append("point", resultElement, "Point");
            this.Append("payment", resultElement, "sProThisMPmt");
            this.Append("rawId", resultElement, "RateOptionId");
            this.Append("blockSubmit", resultElement, "IsBlockedRateLockSubmission", p => p == "True" ? "1" : "0");
            this.Append("blockSubmitMsg", resultElement, "RateLockSubmissionUserWarningMessage", p => HttpUtility.UrlEncode(p));
            this.Append("checksum",  resultElement, "CheckSum");
            this.Append("apr", resultElement, "sApr");
            this.Append("margin", resultElement, "Margin");
            this.Append("bottom", resultElement, "DTI");
            this.Append("productName", resultElement, "LpTemplateNm");
            this.Append("teaser", resultElement, "TeaserRate");
            this.Append("qrate", resultElement, "QRate");
            this.Append("qualPayment", resultElement, "QualPmt");
            this.Append("version", resultElement, "Version");
            this.Append("totalCashToClose", resultElement, "TotalCashClose");
            this.Append("parId", resultElement, "ParRate");
        }

        private void HandleComboScenario(XElement resultElement)
        {
            this.Append("productid", resultElement, "lLpTemplateIdSecond");
            this.Append("rate", resultElement, "sNoteIRSecond");
            this.Append("point", resultElement, "PointSecond");
            this.Append("payment", resultElement, "sProOFinPmt");
            this.Append("apr", resultElement, "sAprSecond");
            this.Append("margin", resultElement, "MarginSecond");
            this.Append("bottom", resultElement, "DTISecond");
            this.Append("productName", resultElement, "LpTemplateNmSecond");
            this.Append("teaserrate", resultElement, "TeaserRateSecond");
            this.Append("qrate" , resultElement, "QRateSecond");
            this.Append("qualPmt", resultElement, "QualPmtSecond");
            this.Append("RawId", resultElement, "RateOptionIdSecond");
            this.Append("Version", resultElement, "VersionSecond");
            this.Append("uniqueChecksum", resultElement, "CheckSumSecond");
            this.Append("blocksubmit", resultElement, "IsBlockedRateLockSubmissionSecond", p => p == "True" ? "1" : "0");
            this.Append("blockSubmitMsg", resultElement, "RateLockSubmissionUserWarningMessageSecond", p => HttpUtility.UrlEncode(p));
            this.Append("totalcashclose", resultElement, "TotalCashCloseSecond");
            this.Append("1stProductId", resultElement, "lLpTemplateId");
            this.Append("1stRate", resultElement, "sNoteIR");
            this.Append("1stPoint", resultElement, "Point");
            this.Append("1stMPmt", resultElement, "sProThisMPmt");
            this.Append("1stRawId", resultElement, "RateOptionId");
            this.Append("1stBlockSubmit", resultElement, "IsBlockedRateLockSubmission", p => p == "True" ? "1" : "0");
            this.Append("1stBlockSubmitMsg", resultElement, "RateLockSubmissionUserWarningMessage", p=> HttpUtility.UrlEncode(p));
            this.Append("1stUniqueChecksum", resultElement, "CheckSum");
        }

        private string Get(XElement el, string name)
        {
            return el.Element(name).Value;
        }

    }
}
