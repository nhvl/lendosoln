﻿namespace DataAccess.LoanComparison
{
    using System;
    using System.Collections.Generic;
    using LendersOffice.ObjLib.LoanSearch.Basic;

    /// <summary>
    /// Details about a loan file, used by DuplicateFinder for finding duplicates for the PML pipeline.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "I want the property names to match their LFF names.")]
    public class PmlDuplicateLoanDetails : LoanDetails
    {
        /// <summary>
        /// Gets or sets the number of active conditions.
        /// </summary>
        public int? sNumOfActiveConditionOnly { get; set; }

        /// <summary>
        /// Gets or sets the number of active conditions due today.
        /// </summary>
        public int? sNumOfActiveDueTodayConditionOnly { get; set; }

        /// <summary>
        /// Gets or sets the number of active conditions past due.
        /// </summary>
        public int? sNumOfActivePastDueConditionOnly { get; set; }

        /// <summary>
        /// Gets or sets the number of active tasks.
        /// </summary>
        public int? sNumOfActiveTasksOnly { get; set; }

        /// <summary>
        /// Gets or sets the number of active tasks due today.
        /// </summary>
        public int? sNumOfActiveDueTodayTasksOnly { get; set; }

        /// <summary>
        /// Gets or sets the number of active tasks past due.
        /// </summary>
        public int? sNumOfActivePastDueTasksOnly { get; set; }

        /// <summary>
        /// Gets or sets the approval cert PDF last saved date.
        /// </summary>
        public DateTime? sApprovalCertPdfLastSavedD { get; set; }

        /// <summary>
        /// Gets or sets the suspense notice PDF last saved date.
        /// </summary>
        public DateTime? sSuspenseNoticePdfLastSavedD { get; set; }

        /// <summary>
        /// Gets or sets the rate lock confirmation PDF last saved date.
        /// </summary>
        public DateTime? sRateLockConfirmationPdfLastSavedD { get; set; }

        /// <summary>
        /// Gets or sets the rate locked expired date.
        /// </summary>
        public DateTime? sRLckdExpiredD { get; set; }

        /// <summary>
        /// Gets or sets the rate lock status type.
        /// </summary>
        public E_sRateLockStatusT sRateLockStatusT { get; set; }

        /// <summary>
        /// Gets or sets the rate lock status type rep.
        /// </summary>
        public string sRateLockStatusT_rep { get; set; }

        /// <summary>
        /// Gets or sets the employee loan rep name.
        /// </summary>
        public string sEmployeeLoanRepName { get; set; }

        /// <summary>
        /// Gets or sets the employee broker processor name.
        /// </summary>
        public string sEmployeeBrokerProcessorName { get; set; }

        /// <summary>
        /// Gets or sets the employee external secondary name.
        /// </summary>
        public string sEmployeeExternalSecondaryName { get; set; }

        /// <summary>
        /// Gets or sets the employee external post closer name.
        /// </summary>
        public string sEmployeeExternalPostCloserName { get; set; }

        /// <summary>
        /// Gets or sets the primary applicant last name.
        /// </summary>
        public string aBLastNm { get; set; }

        /// <summary>
        /// Gets or sets the primary applicant first name.
        /// </summary>
        public string aBFirstNm { get; set; }

        /// <summary>
        /// Gets or sets the loan amount calculation.
        /// </summary>
        public decimal? sLAmtCalc { get; set; }

        /// <summary>
        /// Gets or sets the loan status.
        /// </summary>
        public string LoanStatus { get; set; }

        /// <summary>
        /// Gets or sets the is loan in scope.
        /// </summary>
        public int? InScope { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can read the loan.
        /// </summary>
        public int? CanRead { get; set; }

        /// <summary>
        /// Exports this object to a Dictionary of property names mapped to property values.
        /// </summary>
        /// <returns>A Dictionary of property names as strings mapped to property values as objects.</returns>
        public Dictionary<string, object> ToDictionary()
        {
            Dictionary<string, object> record = new Dictionary<string, object>();

            record.Add(nameof(this.sLId), this.sLId);
            record.Add(nameof(this.sLNm), this.sLNm);
            record.Add(nameof(this.sStatusD), this.sStatusD);
            record.Add(nameof(this.sStatusT), this.sStatusT);
            record.Add(nameof(this.sNumOfActiveConditionOnly), this.sNumOfActiveConditionOnly);
            record.Add(nameof(this.sNumOfActiveDueTodayConditionOnly), this.sNumOfActiveDueTodayConditionOnly);
            record.Add(nameof(this.sNumOfActivePastDueConditionOnly), this.sNumOfActivePastDueConditionOnly);
            record.Add(nameof(this.sNumOfActiveTasksOnly), this.sNumOfActiveTasksOnly);
            record.Add(nameof(this.sNumOfActiveDueTodayTasksOnly), this.sNumOfActiveDueTodayTasksOnly);
            record.Add(nameof(this.sNumOfActivePastDueTasksOnly), this.sNumOfActivePastDueTasksOnly);
            record.Add(nameof(this.sApprovalCertPdfLastSavedD), this.sApprovalCertPdfLastSavedD);
            record.Add(nameof(this.sSuspenseNoticePdfLastSavedD), this.sSuspenseNoticePdfLastSavedD);
            record.Add(nameof(this.sRateLockConfirmationPdfLastSavedD), this.sRateLockConfirmationPdfLastSavedD);
            record.Add(nameof(this.sRLckdExpiredD), this.sRLckdExpiredD);
            record.Add(nameof(this.sRateLockStatusT), this.sRateLockStatusT);
            record.Add(nameof(this.sRateLockStatusT_rep), this.sRateLockStatusT_rep);
            record.Add(nameof(this.sEmployeeLoanRepName), this.sEmployeeLoanRepName);
            record.Add(nameof(this.sEmployeeBrokerProcessorName), this.sEmployeeBrokerProcessorName);
            record.Add(nameof(this.sEmployeeExternalSecondaryName), this.sEmployeeExternalSecondaryName);
            record.Add(nameof(this.sEmployeeExternalPostCloserName), this.sEmployeeExternalPostCloserName);
            record.Add(nameof(this.aBLastNm), this.aBLastNm);
            record.Add(nameof(this.aBFirstNm), this.aBFirstNm);
            record.Add(nameof(this.sLAmtCalc), this.sLAmtCalc);
            record.Add(nameof(this.LoanStatus), this.LoanStatus);
            record.Add(nameof(this.sSpAddr), this.sSpAddr);
            record.Add(nameof(this.InScope), this.InScope);
            record.Add(nameof(this.CanRead), this.CanRead);

            return record;
        }
    }
}
