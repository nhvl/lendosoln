﻿namespace DataAccess.LoanComparison
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;
    using System.Xml.Serialization;
    using System.Xml.Schema;
    using LendersOffice.DistributeUnderwriting;
    using LendersOfficeApp.los.RatePrice;
    using LendersOffice.Admin;

    public sealed class PricingStates : IXmlSerializable
    {
        private int m_MaxCount = 0;
        private Dictionary<Guid, PricingState> m_pricingStates;
        private E_sLPurposeT? m_sLPurposeT;
        private E_sLPurposeT[] m_RefiTypes = new E_sLPurposeT[] {
            E_sLPurposeT.FhaStreamlinedRefinance,
            E_sLPurposeT.Refin,
            E_sLPurposeT.RefinCashout,
            E_sLPurposeT.VaIrrrl
        };
        
        public IEnumerable<PricingState> States
        {
            get { return m_pricingStates.Values ; }
        }

        public PricingState GetState(Guid id)
        {
            PricingState state;
            m_pricingStates.TryGetValue(id, out state);
            return state;
        }

        public int Count { get { return m_pricingStates.Count; } }

        public PricingStates(E_sLPurposeT sLPurposeT)
        {
            m_pricingStates = new Dictionary<Guid, PricingState>();
            m_sLPurposeT = sLPurposeT;
        }

        public PricingStates()
        {
            m_pricingStates = new Dictionary<Guid, PricingState>();
        }

        public void DeleteAll()
        {
            m_pricingStates.Clear();
        }

        public void Delete(Guid id)
        {
            if (m_pricingStates.ContainsKey(id))
            {
                m_pricingStates.Remove(id);
            }
        }

        internal Guid Add(CPageBase data, ILoanProgramTemplate productData, bool isRateMergeEnabled, string noteRate, bool sProRealETxLocked, bool sProHazInsLocked, bool sProHoAssocDuesLocked, bool sProOHExpLocked, ILoanProgramTemplate secondLoanProduct, string secondSelectedRate, string secondLoanMonthlyPayment_rep)
        {
            PricingState state = new PricingState();
            state.ApplyLoanProductTemplate(isRateMergeEnabled, productData, noteRate, secondLoanProduct, secondSelectedRate, secondLoanMonthlyPayment_rep);
            state.ApplyLoanInformation(data);
            state.ApplyInternalStateData(sProRealETxLocked, sProHazInsLocked, sProHoAssocDuesLocked, sProOHExpLocked);
            m_pricingStates.Add(state.Id, state);
            m_MaxCount++;
            state.FriendlyId = m_MaxCount;
            return state.Id;
        }
        
        public void AddExistingState(Guid id, PricingState state)
        {
            m_pricingStates.Add(id, state);
        }
        public LoanProgramByInvestorSet GetLoan2ndLoanQualifyingProgramsToRunLpeForState(Guid priceGroupId, BrokerDB broker, Guid stateId)
        {
            Dictionary<RateMergeGroupKey, LoanProgramByInvestorSet> programSetByRateMergeKey = new Dictionary<RateMergeGroupKey, LoanProgramByInvestorSet>();
            Dictionary<Guid, RateMergeGroupKey> programIdsAndKey = new Dictionary<Guid, RateMergeGroupKey>();

            foreach (TemplateRowData row in TemplateRowData.List(broker.GetLatestReleaseInfo().DataSrcForSnapshot, broker, priceGroupId))
            {
                LoanProgramByInvestorSet set;

                if (!programSetByRateMergeKey.TryGetValue(row.Key, out set))
                {
                    set = new LoanProgramByInvestorSet();
                    programSetByRateMergeKey.Add(row.Key, set);
                }
                set.Add(row.lLpTemplateId, row.lLpTemplateNm, row.productCode);
                programIdsAndKey.Add(row.lLpTemplateId, row.Key);
            }


            PricingStateTemplateIds pricingstateTemplateIds = new PricingStateTemplateIds();

            RateMergeGroupKey key;

            if (programIdsAndKey.TryGetValue(this.GetState(stateId).secondLoanlLpTemplateId.Value, out key))
            {
                return programSetByRateMergeKey[key];
            }
            else
            {
                //not sure what to do here.. Did not find the program anymore.
                Tools.LogInfo("Failed to find the 2nd Lien Program for broker: " + broker.BrokerID.ToString() + " and priceGroup: " + priceGroupId.ToString());
                return new LoanProgramByInvestorSet();
            }
        }

        public IDictionary<Guid, LoanProgramByInvestorSet> GetLoan1stLoanQualifyingProgramsToRunLpe(Guid priceGroupId, BrokerDB broker)
        {
            Dictionary<Guid, LoanProgramByInvestorSet> investorsetsByStateId = new Dictionary<Guid, LoanProgramByInvestorSet>();
            Dictionary<RateMergeGroupKey, LoanProgramByInvestorSet> programSetByRateMergeKey = new Dictionary<RateMergeGroupKey, LoanProgramByInvestorSet>();
            Dictionary<Guid, RateMergeGroupKey> programIdsAndKey = new Dictionary<Guid, RateMergeGroupKey>();
            
            //keep a Id to Key dictionary and a Key to Set Dictionary
            // we now need to run more than one program because of QM.
            // The Loan Comparison XML now needs the par rate and for the par rate we need the entire merge group.

            #region populate cache

            foreach (TemplateRowData row in TemplateRowData.List(broker.GetLatestReleaseInfo().DataSrcForSnapshot, broker, priceGroupId))
            {
                LoanProgramByInvestorSet set;

                if (!programSetByRateMergeKey.TryGetValue(row.Key, out set))
                {
                    set = new LoanProgramByInvestorSet();
                    programSetByRateMergeKey.Add(row.Key, set);
                }
                set.Add(row.lLpTemplateId, row.lLpTemplateNm, row.productCode);
                programIdsAndKey.Add(row.lLpTemplateId, row.Key);

            }
            #endregion

            foreach (var state in m_pricingStates.Values)
            {
                PricingStateTemplateIds pricingstateTemplateIds = new PricingStateTemplateIds();

                RateMergeGroupKey key;

                if (programIdsAndKey.TryGetValue(state.lLpTemplateId, out key))
                {
                    investorsetsByStateId.Add(state.Id, programSetByRateMergeKey[key]);
                }
                else
                {
                    //not sure what to do here.. didnt find the program anymore.
                    investorsetsByStateId.Add(state.Id, new LoanProgramByInvestorSet());
                }
             
            }
            return investorsetsByStateId;
        }


        #region IXmlSerializable Members

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            reader.MoveToContent();
            m_MaxCount = int.Parse(reader.GetAttribute("MaxCount"));
            reader.ReadStartElement("PricingStates");


            while (reader.NodeType == XmlNodeType.Element && reader.Name == "PricingState")
            {
                PricingState state = new PricingState();
                state.ReadXml(reader);

                //per dw if the purpose of the state is purchase but the loan is refi dont load it
                if (m_sLPurposeT.HasValue && state.sLPurposeTPe == E_sLPurposeT.Purchase && m_RefiTypes.Contains(m_sLPurposeT.Value))
                {
                    //ignore
                }
                //per dw if the purpose is refi of the state and the loan is purchase ignore it
                else if (m_sLPurposeT.HasValue && m_RefiTypes.Contains(state.sLPurposeTPe) && m_sLPurposeT == E_sLPurposeT.Purchase)
                {
                    //ignore
                }
                else
                {
                    m_pricingStates.Add(state.Id, state);
                }
            }
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("PricingStates");
            writer.WriteAttributeString("MaxCount", m_MaxCount.ToString());
            foreach (PricingState state in m_pricingStates.Values)
            {
                writer.WriteStartElement("PricingState");
                state.WriteXml(writer);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }

        #endregion
    }
}
