﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using LendersOffice.DistributeUnderwriting;
using LendersOffice.Security;
using LendersOfficeApp.los.RatePrice;
using LendersOffice.Admin;
using System.Xml.Linq;
using LendersOffice.Constants;
using System.Text.RegularExpressions;
using LendersOffice.ConfigSystem;
using LendersOffice.Common;
using LendersOffice.ConfigSystem.Operations;
using LendersOffice.ObjLib.ComparisonReport;

namespace DataAccess.LoanComparison
{
    public sealed class LoanComparisonXml
    {
        // Class holding info needed to process fee descriptions
        private class FeeDescriptionItem
        {
            public FeeDescriptionItem()
            {
                FeeDescriptions = new HashSet<string>();
            }

            public string Hudline { get; set; }
            public E_IntegratedDisclosureSectionT LoanSection { get; set; }
            public HashSet<string> FeeDescriptions { get; set; }
        }

        private class PinStateResultItem
        {
            public PricingState State { get; set; }
            public PinStateLienResultItem First { get; set; }
            public PinStateLienResultItem Second { get; set; }
            public ComboComparisonData ComboData { get; set; }

            public bool HasError
            {
                get
                {
                    if (First.RateOption == null || First.Xml == null)
                    {
                        return true;
                    }

                    if (State.IsComboScenario)
                    {
                        if (Second.RateOption == null || Second.Xml == null)
                        {
                            return true;
                        }
                    }

                    return false;
                }
            }

            public string GetErrorMessage()
            {
                if (this.HasError)
                {
                    return "The pin state is no longer available.";
                }


                List<PinStateLienResultItem> items = new List<PinStateLienResultItem>(2);
                items.Add(this.First);

                if (this.State.IsComboScenario)
                {
                    items.Add(this.Second);
                }

                foreach (var item in items)
                {
                    if (item.ResultItem.IsErrorMessage)
                    {
                        return item.ResultItem.ErrorMessage;
                    }

                    if (item.RateOption.IsDisqualified)
                    {
                        return item.RateOption.DisqualReason;
                    }
                }

                return null;
            }
        }

        public class PinStateLienResultItem
        {
            public UnderwritingResultItem ResultItem { get; set; }
            public CApplicantRateOption RateOption { get; set; }
            public CApplicantPriceXml Xml { get; set; }
            public decimal MergeGroupParRate { get; set; }
        }

        private static LosConvert m_losConvert = new LosConvert();

        private static string CombineFields(params Tuple<string,string>[] labelsAndFields)
        {
            StringBuilder data = new StringBuilder();
            foreach (var field in labelsAndFields)
            {
                if (string.IsNullOrEmpty(field.Item2))
                {
                    continue;
                }

                if (data.Length != 0)
                {
                    data.Append(", ");
                }
                
                data.Append(field.Item1);
                if (field.Item1.Length > 0)
                {
                    data.Append(" ");
                }
                data.Append(field.Item2);
            }
            return data.ToString();
        }
        private static PinStateResultItem GetPinStateResultItem(LosConvert convert, CPageData loanData, PricingState state, UnderwritingResultItem firstResultItem, UnderwritingResultItem secondResultItem)
        {
            PinStateResultItem resultItem = new PinStateResultItem();
            resultItem.State = state;
            resultItem.First = Create(loanData, state.sOriginatorCompensationPaymentSourceT, state.lLpTemplateId, state.SelectedNoteRate, firstResultItem);

            if (resultItem.State.IsComboScenario)
            {
                resultItem.Second = Create(loanData, state.sOriginatorCompensationPaymentSourceT, state.secondLoanlLpTemplateId.Value, state.secondLoanSelectedNoteRate, secondResultItem);
                resultItem.ComboData = LoanComparisonComboCorrections.CorrectComboData(convert, loanData, resultItem.First, resultItem.Second);
            }

            return resultItem;
        }

        private static PinStateLienResultItem Create(CPageData loanData, E_sOriginatorCompensationPaymentSourceT paymentSource,  Guid lLpTemplateId, string noteRate, UnderwritingResultItem item)
        {
            PinStateLienResultItem lienResult = new PinStateLienResultItem();
            lienResult.MergeGroupParRate = 0;
            lienResult.RateOption = null;
            lienResult.ResultItem = item;

            lienResult.Xml = item.GetEligibleLoanProgramList().Where(p => p.lLpTemplateId == lLpTemplateId).FirstOrDefault();

            if (lienResult.Xml != null)
            {
                lienResult.RateOption = lienResult.Xml.ApplicantRateOptions.Where(p => p.Rate_rep == noteRate).FirstOrDefault();
                lienResult.MergeGroupParRate = GetParRate(paymentSource, loanData, item, lLpTemplateId);
            }

            return lienResult;
        }

        /// <summary>
        /// Need to move this to one spot. If you update it you have to go update it in a few other places as well. Please search.
        /// </summary>
        /// <param name="dataLoan"></param>
        /// <param name="item"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        private static decimal GetParRate(E_sOriginatorCompensationPaymentSourceT paymentSource, CPageData dataLoan, UnderwritingResultItem item, Guid productId)
        {
            E_OriginatorCompPointSetting pointSetting = E_OriginatorCompPointSetting.UsePoint;

            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            if (dataLoan.sIsQualifiedForOriginatorCompensation)
            {
                if (dataLoan.BrokerDB.IsUsePriceIncludingCompensationInPricingResult && paymentSource == E_sOriginatorCompensationPaymentSourceT.LenderPaid &&
                    dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees)
                {
                    pointSetting = E_OriginatorCompPointSetting.UsePointWithOriginatorComp;
                }
            }
            dataLoan.CalcModeT = E_CalcModeT.LendersOfficePageEditing;

            RateMergeBucket rateMergeBucket = new RateMergeBucket(dataLoan.BrokerDB.IsShowRateOptionsWorseThanLowerRate);
            RateMergeGroupKey key = null;
            foreach( CApplicantPriceXml priceXml in item.GetEligibleLoanProgramList())
            {
                if (priceXml.lLpTemplateId == productId)
                {
                    key = new RateMergeGroupKey(priceXml);
                }

                if (priceXml.Status == E_EvalStatus.Eval_Eligible)
                {
                    rateMergeBucket.Add(priceXml);
                }
            }

            if (key == null)
            {
                return 0;
            }

            RateMergeGroup g = rateMergeBucket.GetGroup(key);
            if (g == null)
            {
                return 0;
            }
            return g.GetQMParRate(pointSetting);
        }


        public static string DeletePinFromCert(Guid newResultId, string xml, Guid pinId)
        {

            XDocument existingDoc = XDocument.Parse(xml);
            XDocument newDoc = new XDocument(new XDeclaration("1.0", "utf-8", "yes"), new XElement("LoanComparison"));

            XElement existingRoot = existingDoc.Element("LoanComparison");
            XElement newDocRoot = newDoc.Element("LoanComparison");

            //copy loan info
            newDocRoot.Add(new XElement(existingRoot.Element("LoanInfo")));
            newDocRoot.Add(new XElement(existingRoot.Element("Borrowers")));
            newDocRoot.Add(new XElement(existingRoot.Element("App1003Interviewer")));

            newDocRoot.Add(new XElement(existingRoot.Element("HasHeloc")));
            newDocRoot.Add(new XElement(existingRoot.Element("ShowSecondLienInfo")));
            newDocRoot.Add(new XElement(existingRoot.Element("SecondLienLabel")));
            newDocRoot.Add(new XElement(existingRoot.Element("ShowUpfrontMipRow")));

            // remove pin from results
            XElement results = new XElement("Results");
            newDocRoot.Add(results);
            XElement group = null;
            int count = 0;
            int pageCount = 0;
            foreach (XElement loanProgram in existingRoot.Element("Results").Descendants("LoanProgram"))
            {
                if (loanProgram.Attribute("PinId").Value == pinId.ToString("N"))
                {
                    continue;
                }
                if (count % 3 == 0)
                {
                    pageCount++;
                    group = new XElement("Group", new XAttribute("Page", pageCount));
                    results.Add(group);
                }

                group.Add(new XElement(loanProgram));
                count++;

            }

            // remove pin from addendum
            XElement addendum = new XElement("Addendum");
            newDocRoot.Add(addendum);
            group = null;
            count = 0;
            foreach (XElement loanProgram in existingRoot.Element("Addendum").Descendants("LoanProgram"))
            {
                if (loanProgram.Attribute("PinId").Value == pinId.ToString("N"))
                {
                    continue;
                }
                if (count % 3 == 0)
                {
                    pageCount++; // Note: pageCount continues from results
                    group = new XElement("Group", new XAttribute("Page", pageCount));
                    addendum.Add(group);
                }

                group.Add(new XElement(loanProgram));
                count++;

            }

            newDocRoot.Add(new XElement(existingRoot.Element("Report")));
            newDocRoot.Element("Report").Element("TotalPageCount").Value = pageCount.ToString();
            newDocRoot.Element("Report").Element("ResultId").Value = newResultId.ToString("N");
            return newDoc.ToString();
        }

        public static bool HasIneligiblePins(string xml)
        {
            XDocument doc = XDocument.Parse(xml);
            XElement root = doc.Element("LoanComparison");
            XElement IneligiblePinsCount = root.Element("Report").Element("IneligiblePinsCount");
            return IneligiblePinsCount != null && int.Parse(IneligiblePinsCount.Value) > 0;
        }

        public static string RemoveIneligiblePins(Guid newResultId, string xml, out Guid[] removedPinIds)
        {
            List<Guid> pinIds = new List<Guid>();
            XDocument existingDoc = XDocument.Parse(xml);
            XElement existingRoot = existingDoc.Element("LoanComparison");

            XDocument newDoc = new XDocument(new XDeclaration("1.0", "utf-8", "yes"), new XElement("LoanComparison"));            
            XElement newDocRoot = newDoc.Element("LoanComparison");

            //copy loan info
            newDocRoot.Add(new XElement(existingRoot.Element("LoanInfo")));
            newDocRoot.Add(new XElement(existingRoot.Element("Borrowers")));
            newDocRoot.Add(new XElement(existingRoot.Element("App1003Interviewer")));

            // remove pin from results
            XElement results = new XElement("Results");
            newDocRoot.Add(results);
            XElement group = null;
            int count = 0;
            int pageCount = 0;
            foreach (XElement loanProgram in existingRoot.Element("Results").Descendants("LoanProgram"))
            {
                if (loanProgram.Element("ErrorMessage") != null)
                {
                    pinIds.Add(new Guid(loanProgram.Attribute("PinId").Value));
                    continue;
                }
                if (count % 3 == 0)
                {
                    pageCount++;
                    group = new XElement("Group", new XAttribute("Page", pageCount));
                    results.Add(group);
                }

                group.Add(new XElement(loanProgram));
                count++;
            }

            // remove pin from addendum
            XElement addendum = new XElement("Addendum");
            newDocRoot.Add(addendum);
            group = null;
            count = 0;
            foreach (XElement loanProgram in existingRoot.Element("Addendum").Descendants("LoanProgram"))
            {
                if (loanProgram.Element("ErrorMessage") != null)
                {
                    // Note: PinId was added to list on Results pass. Do not need to add PinId a second time.
                    continue;
                }
                if (count % 3 == 0)
                {
                    pageCount++; // Note: pageCount continues from results
                    group = new XElement("Group", new XAttribute("Page", pageCount));
                    addendum.Add(group);
                }

                group.Add(new XElement(loanProgram));
                count++;

            }

            newDocRoot.Add(new XElement(existingRoot.Element("Report")));
            newDocRoot.Element("Report").Element("TotalPageCount").Value = pageCount.ToString();
            newDocRoot.Element("Report").Element("IneligiblePinsCount").Value = "0";
            newDocRoot.Element("Report").Element("ResultId").Value = newResultId.ToString("N");

            removedPinIds = pinIds.ToArray();
            return newDoc.ToString();
        }

        private static int MaxSize(params string[] items)
        {
            int max = -1;

            foreach (var str in items)
            {
                if (max < str.Length)
                {
                    max = str.Length;
                }
            }
            return max;
        }

        private static string ValueIf(bool show, string value)
        {
            if (show)
            {
                return value;
            }

            return "--";
        }

        private static string GetDTI(CApplicantRateOption rateOption, decimal sLTotI)
        {
            string dti_rep = rateOption.BottomRatio_rep + "%";
             decimal dti;
            if (false == decimal.TryParse(rateOption.BottomRatio_rep, out dti) || dti < 0 || sLTotI == 0)
            {
                dti_rep = "N/A";
            }

            return dti_rep;
        }

        private static string SumMoney(LosConvert convert, string amount1_rep, string amount2_rep)
        {
            decimal amount1 = convert.ToDecimal(amount1_rep);
            decimal amount2 = convert.ToDecimal(amount2_rep);

            return convert.ToMoneyString(Tools.SumMoney(new decimal[] { amount1, amount2 }), FormatDirection.ToRep);
        }

        private static string SumPercent(LosConvert convert, string amount1_rep, string amount2_rep)
        {
            decimal amount1 = convert.ToRate(amount1_rep);
            decimal amount2 = convert.ToRate(amount2_rep);

            return convert.ToRateString(amount1 + amount2);
        }

        private static string AdjustDti(LosConvert convert, string dti, decimal sTransmPro2ndMPmt, decimal sLTotI)
        {
            var parsedDti = convert.ToRate(dti);

            if(parsedDti == 0 || sLTotI == 0)
            {
                return "N/A";
            }

            return convert.ToRateString(parsedDti - (100 * (sTransmPro2ndMPmt/sLTotI)));
        }

        private static string CalculateDti(LosConvert convert, string monthlyPayment, decimal totalIncome)
        {
            decimal payment = convert.ToDecimal(monthlyPayment);

            var returnDti = "N/A";

            if(payment != 0 && totalIncome != 0)
            {
                var dtiRate = payment / totalIncome;

                if (dtiRate >= 0)
                {
                    returnDti = convert.ToRateString(100 * (payment / totalIncome));
                }
            }

            return returnDti;
        }

        private static string DtiToRep(LosConvert convert, decimal dti)
        {
            return dti < 0 ? "N/A" : convert.ToRateString(dti);
        }

        private static string RecoupMonthsToRep(LosConvert convert, decimal recoupMonths)
        {
            return recoupMonths < 0 ? "N/A" : recoupMonths.ToString();
        }

        private static void WritePinStateInfo(XmlWriter writer, PricingState state, bool showSecondLienInfo)
        {
            writer.WriteAttributeString("PinId", state.Id.ToString("N"));
            writer.WriteElementString("PinFriendlyId", state.FriendlyId.ToString("000"));
            writer.WriteElementString("lLpTemplateId", state.lLpTemplateId.ToString("N"));
            writer.WriteElementString("LpTemplateNm", state.lLpTemplateNm);
            writer.WriteElementString("PinId", state.Id.ToString("N"));
            writer.WriteElementString("IsComboScenario", state.IsComboScenario.ToString());
            writer.WriteElementString("sSpAddr", state.sSpAddr);
            writer.WriteElementString("sSpCity", state.sSpCity + ", ");
            writer.WriteElementString("sSpCounty", state.sSpCounty);
            writer.WriteElementString("sSpStatePe", state.sSpStatePe);
            writer.WriteElementString("sSpZip", state.sSpZip);
            writer.WriteElementString("sHouseValPe", state.sHouseValPe_rep);
            writer.WriteElementString("sDownPmtPcPe", state.sDownPmtPcPe_rep);
            writer.WriteElementString("sEquityPe", state.sEquityPe_rep);
            writer.WriteElementString("sProdRLckdDays", state.sProdRLckdDays_rep);

            writer.WriteElementString("sLtvROtherFinPe", ValueIf(state.IsComboScenario,state.sLtvROtherFinPe_rep));
            writer.WriteElementString("sProOFinBalPe", ValueIf(state.IsComboScenario, state.sProOFinBalPe_rep));
            writer.WriteElementString("sSubFinToPropertyValue", ValueIf(state.IsComboScenario && state.sSubFinT == E_sSubFinT.Heloc, state.sSubFinToPropertyValue_rep));
            writer.WriteElementString("sSubFinPe", ValueIf(state.IsComboScenario && state.sSubFinT == E_sSubFinT.Heloc, state.sSubFinPe_rep));

            writer.WriteElementString("CombinedLoanAmount", state.CombinedLoanAmount);

            if(state.sSubFinT == E_sSubFinT.Heloc)
            {
                writer.WriteElementString("CombinedHelocLoanAmount", state.CombinedHelocLoanAmount);
            }

            writer.WriteElementString("sProdSpStructureT", Tools.GetStructureTypeDescription(state.sProdSpStructureT));
            writer.WriteElementString("sProdSpT", Tools.GetsProdSpTDescription(state.sProdSpT) + ", ");
            writer.WriteElementString("sSubFinT", Tools.GetSubFinTDescription(state.sSubFinT));
            writer.WriteElementString("sNoteIR", state.SelectedNoteRate + "%");
            writer.WriteElementString("sProdImpound", state.sProdImpound ? "Yes" : "No");



            RateMergeGroup firstGroup = new RateMergeGroup(state.GetMergeKey(E_sLienPosT.First));
            RateMergeGroup secondGroup = null;

            writer.WriteElementString("RateMergeName", firstGroup.Name);

            string rateMergeNameSecond = "--";
            string lLpTemplateIdSecond = "--";
            string sNoteIRSecond = "--";
            string lLpTemplateNmSecond = "--";
            if (state.IsComboScenario)
            {
                secondGroup = new RateMergeGroup(state.GetMergeKey(E_sLienPosT.Second));
                rateMergeNameSecond = secondGroup.Name;
                lLpTemplateIdSecond = state.secondLoanlLpTemplateId.Value.ToString("N");
                lLpTemplateNmSecond = state.secondLoanlLpTemplateNm;
                sNoteIRSecond = state.secondLoanSelectedNoteRate + "%";
            }


            writer.WriteElementString("RateMergeNameSecond", rateMergeNameSecond);
            writer.WriteElementString("lLpTemplateIdSecond", lLpTemplateIdSecond);
            writer.WriteElementString("sNoteIRSecond", sNoteIRSecond);
            writer.WriteElementString("LpTemplateNmSecond", lLpTemplateNmSecond);
        }

        private static int FeeDescriptionComparator(KeyValuePair<string, FeeDescriptionItem> a, KeyValuePair<string, FeeDescriptionItem> b)
        {
            int loanSecCompareVal = a.Value.LoanSection.CompareTo(b.Value.LoanSection);
            if (loanSecCompareVal == 0)
            {
                return int.Parse(a.Key).CompareTo(int.Parse(b.Key));
            }
            else
            {
                return loanSecCompareVal;
            }
        }

        public static void Generate(XmlWriter writer, AbstractUserPrincipal principal, Guid loanid, Guid statePriceCacheId, Guid resultId, PricingStates states, Dictionary<Guid, UnderwritingResultItem> underwritingResultByJobId, ComparisonRunData runData)
        {
            LosConvert convert = new LosConvert();
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanid, typeof(LoanComparisonXml));
            dataLoan.AllowLoadWhileQP2Sandboxed = true;
            dataLoan.InitLoad();
            BrokerDB brokerdb = principal.BrokerDB;
            string logoHtml = "";
            bool showTextInstead = false;
            string logoText = brokerdb.Name;
            Regex emailRegex = new Regex(ConstAppDavid.EmailRegexValidator);

            LoanValueEvaluator evaluator = new LoanValueEvaluator(brokerdb.BrokerID, loanid, new WorkflowOperation[] { WorkflowOperations.RequestRateLockRequiresManualLock, WorkflowOperations.RequestRateLockCausesLock });
            evaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(PrincipalFactory.CurrentPrincipal));

            bool canRegister = LendingQBExecutingEngine.CanPerform(WorkflowOperations.RegisterLoan, evaluator);
            string cantRegisterReason = "Registering is not permitted for this loan file. " +   LendingQBExecutingEngine.GetUserFriendlyMessage(WorkflowOperations.RegisterLoan, evaluator);

            bool canLock = LendingQBExecutingEngine.CanPerform(WorkflowOperations.RequestRateLockRequiresManualLock, evaluator)
                || LendingQBExecutingEngine.CanPerform(WorkflowOperations.RequestRateLockCausesLock, evaluator);

            string manLockMsg = LendingQBExecutingEngine.GetUserFriendlyMessage(WorkflowOperations.RequestRateLockRequiresManualLock, evaluator);
            string autoLockMsg = LendingQBExecutingEngine.GetUserFriendlyMessage(WorkflowOperations.RequestRateLockCausesLock, evaluator);
            string canLockReason = "Locking is not permitted for this loan file. " + manLockMsg + (string.IsNullOrEmpty(manLockMsg) ? "" :". ") + autoLockMsg ;

            if (canRegister || canLock)
            {
                if (E_sLoanFileT.QuickPricer2Sandboxed == dataLoan.sLoanFileT)
                {
                    canLock = false;
                    canRegister = false;

                    if (principal.Type == "P")
                    {
                        if (principal.BrokerDB.IsAllowExternalUserToCreateNewLoan)
                        {
                            cantRegisterReason = LendersOffice.Common.JsMessages.LPE_CannotSubmitInQP2ModeInPML;
                        }
                        else
                        {
                            cantRegisterReason = LendersOffice.Common.JsMessages.LPE_CannotSubmitInQP2ModeInPMLAndCantCreateLoan;
                        }
                    }
                    else
                    {
                        cantRegisterReason = LendersOffice.Common.JsMessages.LPE_CannotSubmitInQP2Mode;
                    }
                    canLockReason = cantRegisterReason;
                }
                else if (!principal.HasPermission(Permission.CanSubmitWithoutCreditReport) && !dataLoan.sAllAppsHasCreditReportOnFile)
                {
                    canLock = false;
                    canRegister = false;

                    cantRegisterReason = "You do not have permission to submit loans without a credit report.";
                    canLockReason = cantRegisterReason;
                }
            }
            
            BranchDB branch = new BranchDB(dataLoan.sBranchId, dataLoan.sBrokerId);
            branch.Retrieve();
            Dictionary<string, string> emailsOnFile = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

            bool isBorrowerPaidOriginatorCompAndIncludedInFees = false;
            bool isLenderPaidOriginatorCompAndAdditionToFees = false;

            #region logo settings
            if (branch.IsDisplayNmModified)
            {
                logoText = branch.DisplayNm;
                showTextInstead = true;
            }

            if (!showTextInstead)
            {
                logoHtml = "LogoPL.aspx?id=" + brokerdb.PmlSiteID.ToString("N");
            }
            #endregion

            writer.WriteStartElement("LoanComparison");
            #region <LoanInfo>
            writer.WriteStartElement("LoanInfo");
            writer.WriteElementString("IsTRID2015", dataLoan.sDisclosureRegulationT.ToString("D"));
            writer.WriteElementString("sLPurposeTDesc", dataLoan.sLPurposeTDesc);
            writer.WriteElementString("sLNm", dataLoan.sLNm);
            writer.WriteElementString("EmbeddedLogo", logoHtml);
            writer.WriteElementString("LogoText", logoText);
            writer.WriteElementString("ShowTextInstead", showTextInstead.ToString());
            writer.WriteElementString("sLId", loanid.ToString("N"));
            writer.WriteElementString("IsLead", Tools.IsStatusLead(dataLoan.sStatusT).ToString());
            writer.WriteElementString("HorizonOfBorrowerInterestInMonths", dataLoan.sHorizonOfBorrowerInterestInMonths.ToString());
            writer.WriteEndElement();
            #endregion

            #region <Borrowers>
            writer.WriteStartElement("Borrowers");
            for (int i = 0; i < dataLoan.nApps; i++)
            {
                CAppData appData = dataLoan.GetAppData(i);
                writer.WriteStartElement("Borrower");
                writer.WriteElementString("ContactInfo", CombineFields(
                    Tuple.Create("", appData.aBNm),
                    Tuple.Create("Tel:", appData.aBHPhone),
                    Tuple.Create("Email:", appData.aBEmail)
                    ));
                if (!emailsOnFile.ContainsKey(appData.aBEmail.TrimWhitespaceAndBOM()) && emailRegex.IsMatch(appData.aBEmail.TrimWhitespaceAndBOM()))
                {
                    emailsOnFile.Add(appData.aBEmail.TrimWhitespaceAndBOM(), appData.aBNm);
                }
                writer.WriteEndElement();
                if (appData.aBHasSpouse)
                {
                    writer.WriteStartElement("Borrower");
                    writer.WriteElementString("ContactInfo", CombineFields(
                      Tuple.Create("", appData.aCNm),
                      Tuple.Create("Tel:", appData.aCHPhone),
                      Tuple.Create("Email:", appData.aCEmail)
                      ));
                    writer.WriteEndElement();
                    if (!emailsOnFile.ContainsKey(appData.aBEmail.TrimWhitespaceAndBOM()) && emailRegex.IsMatch(appData.aCEmail.TrimWhitespaceAndBOM()))
                    {
                        emailsOnFile.Add(appData.aBEmail.TrimWhitespaceAndBOM(), appData.aCNm);
                    }
                }

            }

            

            writer.WriteEndElement();
            #endregion

            #region <EmailsOnFile>
            writer.WriteStartElement("EmailsOnFile");

            foreach (KeyValuePair<string, string> item in emailsOnFile)
            {
                writer.WriteStartElement("Entry");
                writer.WriteAttributeString("Name", item.Value);
                writer.WriteAttributeString("Email", item.Key);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
            #endregion

            #region App1003Interviewer
            IPreparerFields app1003Interviewer = dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            writer.WriteStartElement("App1003Interviewer");
            writer.WriteElementString("Identity", CombineFields(
                Tuple.Create("", app1003Interviewer.PreparerName),
                Tuple.Create("LIC#", app1003Interviewer.LicenseNumOfAgent),
                Tuple.Create("NMLS#", app1003Interviewer.LoanOriginatorIdentifier)));
            writer.WriteElementString("Name", app1003Interviewer.PreparerName);
            writer.WriteElementString("AgentLicenseNumber", app1003Interviewer.LicenseNumOfAgent);
            writer.WriteElementString("LoanOriginatorIdentifier", app1003Interviewer.LoanOriginatorIdentifier);
            writer.WriteElementString("Address", app1003Interviewer.StreetAddrSingleLine);
            writer.WriteElementString("ContactInfo", CombineFields(
                Tuple.Create("Tel:", app1003Interviewer.Phone),
                Tuple.Create("Email:", app1003Interviewer.EmailAddr)));
            writer.WriteElementString("Phone", app1003Interviewer.Phone);
            writer.WriteElementString("EmailAddr", app1003Interviewer.EmailAddr);
            writer.WriteEndElement(); // </App1003Interviewer>
            #endregion

            bool showCloseEnd = false;
            bool showHeloc = false;
            bool showSecondLienInfo = false;

            foreach (PricingState state in states.States)
            {
                switch (state.sSubFinT)
                {
                    case E_sSubFinT.CloseEnd:
                        showCloseEnd = true;
                        break;
                    case E_sSubFinT.Heloc:
                        showHeloc = true;
                        break;
                    default:
                        throw new UnhandledEnumException(state.sSubFinT);
                }

                if (state.IsComboScenario)
                {
                    showSecondLienInfo = true;
                }
            }

            string secondLienLabel; 

            if (showCloseEnd && showHeloc)
            {
                secondLienLabel = "2nd Financing / Initial Draw";
            }
            else if (showCloseEnd)
            {
                secondLienLabel = "2nd Financing";
             
            }
            else 
            {
                secondLienLabel = "Initial Draw";
            }

            writer.WriteElementString("HasHeloc", showHeloc.ToString());
            writer.WriteElementString("ShowSecondLienInfo", showSecondLienInfo ? "Yes" : "No");

            var isLead = Tools.IsStatusLead(dataLoan.sStatusT);
            var lcrHideEstimatedDTI = CheckSectionVisibility(brokerdb.LcrHideEstimatedDTI, isLead);
            var lcrHide1stLienLifetimeFinanceCharge = CheckSectionVisibility(brokerdb.LcrHide1stLienLifetimeFinanceCharge, isLead);
            var lcrHide2ndLienLifetimeFinanceCharge = CheckSectionVisibility(brokerdb.LcrHide2ndLienLifetimeFinanceCharge, isLead);
            var lcrHideEstimatedReserves = CheckSectionVisibility(brokerdb.LcrHideEstimatedReserves, isLead);
            var lcrHideHorizon = CheckSectionVisibility(brokerdb.LcrHideHorizon, isLead);

            writer.WriteElementString("LcrHideHorizon", lcrHideHorizon.ToString());
            writer.WriteElementString("LcrHideEstimatedDTI", lcrHideEstimatedDTI.ToString());
            writer.WriteElementString("LcrHide1stLienLifetimeFinanceCharge", (lcrHide1stLienLifetimeFinanceCharge && showSecondLienInfo).ToString());
            writer.WriteElementString("LcrHide2ndLienLifetimeFinanceCharge", (lcrHide2ndLienLifetimeFinanceCharge && showSecondLienInfo).ToString());

            writer.WriteElementString("LcrHideEstimatedReserves", (lcrHideEstimatedReserves && showSecondLienInfo).ToString());
            writer.WriteElementString("SecondLienLabel", secondLienLabel);

            #region <results>



            List<PinStateResultItem> okayResults = new List<PinStateResultItem>();
            List<PinStateResultItem> errorResults = new List<PinStateResultItem>();

            #region figure out the order

            foreach (var request in runData.Requests)
            {
                PinStateResultItem resultItem = new PinStateResultItem()
                {
                    State = states.GetState(request.StateId)
                };

                UnderwritingResultItem firstResultItem;
                if (!underwritingResultByJobId.TryGetValue(request.FirstLoanRequestId, out firstResultItem))
                {
                    errorResults.Add(resultItem);
                    continue;
                }

                UnderwritingResultItem secondResultItem = null;

                if (request.HasSecondLoanInfo)
                {
                    if (!request.SecondLoanRequestId.HasValue || !underwritingResultByJobId.TryGetValue(request.SecondLoanRequestId.Value, out secondResultItem))
                    {
                        errorResults.Add(resultItem);
                        continue;
                    }
                }

                resultItem = GetPinStateResultItem(convert, dataLoan, resultItem.State, firstResultItem, secondResultItem);

                if (resultItem.HasError)
                {
                    errorResults.Add(resultItem);
                }
                else
                {
                    okayResults.Add(resultItem);
                }
            }
            #endregion

            int count = 0;
            int pageCount = 0;
            int ineligibleCount = 0;

            // Datastructure to hold needed info for fee descriptions.
            Dictionary<string, FeeDescriptionItem> feeDescriptionsFirst = new Dictionary<string, FeeDescriptionItem>();
            Dictionary<string, FeeDescriptionItem> feeDescriptionsSecond = new Dictionary<string, FeeDescriptionItem>();

            bool showUpfrontMipRow = false;

            writer.WriteStartElement("Results");
            foreach (PinStateResultItem resultItem in okayResults.Union(errorResults))
            {
                if (count % 3 == 0)
                {
                    pageCount += 1;
                    if (count != 0)
                    {
                        writer.WriteEndElement();
                    }
                    writer.WriteStartElement("Group");
                    writer.WriteAttributeString("Page", pageCount.ToString());
                }

                writer.WriteStartElement("LoanProgram");

                PricingState state = resultItem.State;
                dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                if (dataLoan.sIsQualifiedForOriginatorCompensation)
                {
                    isBorrowerPaidOriginatorCompAndIncludedInFees = state.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid
                        && dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.IncludedInLenderFees;
                    isLenderPaidOriginatorCompAndAdditionToFees = state.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid &&
                        dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees;
                }
                else
                {
                    isBorrowerPaidOriginatorCompAndIncludedInFees = false;
                    isLenderPaidOriginatorCompAndAdditionToFees = false;
                }
                dataLoan.CalcModeT = E_CalcModeT.LendersOfficePageEditing;

                WritePinStateInfo(writer, state, showSecondLienInfo);

                string errorMessage = resultItem.GetErrorMessage();

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    writer.WriteElementString("ErrorMessage", errorMessage);
                    writer.WriteEndElement(); //</LoanProgram>
                    count++;
                    ineligibleCount++;
                    continue;
                }

                writer.WriteElementString("Point_debug_IsUsePriceIncludingCompensationInPricingResult", brokerdb.IsUsePriceIncludingCompensationInPricingResult.ToString());
                writer.WriteElementString("Point_debug_isLenderPaidOriginatorCompAndAdditionToFees", isLenderPaidOriginatorCompAndAdditionToFees.ToString());
                writer.WriteElementString("Point_debug_isBorrowerPaidOriginatorCompAndIncludedInFees", isBorrowerPaidOriginatorCompAndIncludedInFees.ToString());

                string pointRep = GetPointRep(brokerdb, isLenderPaidOriginatorCompAndAdditionToFees, resultItem.First.RateOption);
                writer.WriteElementString("Point_debug_Point_rep", pointRep);
                writer.WriteElementString("Point", pointRep);

                writer.WriteElementString("ParRate", resultItem.First.MergeGroupParRate.ToString());
                writer.WriteElementString("LpTemplateNm", resultItem.First.RateOption.LpTemplateNm);

                writer.WriteElementString("sFfUfmipFinanced", resultItem.First.RateOption.sFfUfmipFinanced);
                showUpfrontMipRow = showUpfrontMipRow || !string.Equals(resultItem.First.RateOption.sFfUfmipFinanced, "$0.00");

                writer.WriteElementString("sLtvRPe", state.sLtvRPe_rep);
                writer.WriteElementString("sLAmtCalcPe", resultItem.First.RateOption.sLAmtCalcPe);
                writer.WriteElementString("sProThisMPmt", "$" + resultItem.First.RateOption.FirstPmtAmt_rep);
                writer.WriteElementString("sProMIns", resultItem.First.RateOption.sProMIns);
                writer.WriteElementString("sApr", resultItem.First.RateOption.Apr_rep + "%");
                writer.WriteElementString("sFinalLAmt", resultItem.First.RateOption.sFinalLAmt);
                writer.WriteElementString("CurrentPITIPayment", resultItem.First.RateOption.CurrentPITIPayment);
                writer.WriteElementString("sProRealETx", resultItem.First.RateOption.sProRealETx);
                writer.WriteElementString("sProHazIns", resultItem.First.RateOption.sProHazIns);
                writer.WriteElementString("sProOHExp", resultItem.First.RateOption.sProOHExp);
                writer.WriteElementString("sProHoAssocDues", resultItem.First.RateOption.sProHoAssocDues);
                writer.WriteElementString("sProdIsUFMIPFFFinanced", state.sProdIsUFMIPFFFinanced ? "Yes" : "No");

                writer.WriteElementString("DTI", state.IsComboScenario ? DtiToRep(convert, resultItem.ComboData.FirstDti) : resultItem.First.RateOption.BottomRatio_rep + "%");
                writer.WriteElementString("TotalCashClose", state.IsComboScenario ? resultItem.ComboData.TotalCashToClose : resultItem.First.RateOption.CashToClose);
                writer.WriteElementString("sFinCharge", resultItem.First.RateOption.sFinCharge);
                writer.WriteElementString("ClosingCost", resultItem.First.RateOption.ClosingCost);
                writer.WriteElementString("ClosingCostRowCount", resultItem.First.RateOption.ClosingCostBreakdown.Count.ToString());
                writer.WriteElementString("PrepaidCharges", resultItem.First.RateOption.PrepaidCharges);
                writer.WriteElementString("NonPrepaidCharges", resultItem.First.RateOption.NonPrepaidCharges);
                writer.WriteElementString("NoteRate", resultItem.First.RateOption.Rate_rep);
                writer.WriteElementString("EstimatedReservesDollars", state.IsComboScenario ? resultItem.ComboData.TotalEstimatedReservesDollars : resultItem.First.RateOption.EstimatedReservesDollars);
                writer.WriteElementString("EstimatedReservesMonths", state.IsComboScenario ? resultItem.ComboData.TotalEstimatedReservesMonths : resultItem.First.RateOption.Reserves);
                writer.WriteElementString("PrincipalPaidOverHorizon", resultItem.First.RateOption.PrincipalPaidOverHorizon);
                writer.WriteElementString("FinanceChargesPaidOverHorizon", resultItem.First.RateOption.FinanceChargesPaidOverHorizon);
                writer.WriteElementString("RemainingBalanceAfterHorizon", resultItem.First.RateOption.RemainingBalanceAfterHorizon);
                writer.WriteElementString("SavingsVersusCurrentLoan", resultItem.First.RateOption.SavingsVersusCurrentLoan); // This doesn't make sense in a combo scenario...
                writer.WriteElementString("PitiaSavings", state.IsComboScenario ? convert.ToMoneyString(resultItem.ComboData.PitiSavings, FormatDirection.ToRep) : resultItem.First.RateOption.PitiaSavings);
                writer.WriteElementString("RecoupClosingCostsMonths", state.IsComboScenario ? RecoupMonthsToRep(convert, resultItem.ComboData.RecoupMonths) : resultItem.First.RateOption.BreakEvenPoint);
                writer.WriteElementString("Margin", resultItem.First.RateOption.Margin_rep);
                writer.WriteElementString("TeaserRate", resultItem.First.RateOption.TeaserRate_rep);
                writer.WriteElementString("QRate", resultItem.First.RateOption.QRate_rep);
                writer.WriteElementString("QualPmt", resultItem.First.RateOption.QualPmt_rep);
                writer.WriteElementString("RateOptionId", resultItem.First.RateOption.RateOptionId);

                writer.WriteElementString("Point_debug_PointIncludingOriginatorComp_rep", resultItem.First.RateOption.PointIncludingOriginatorComp_rep);
                writer.WriteElementString("Point_debug_PointLessOriginatorComp_rep", resultItem.First.RateOption.PointLessOriginatorComp_rep);

                writer.WriteElementString("CheckSum", resultItem.First.Xml.UniqueChecksum);
                writer.WriteElementString("Version", resultItem.First.Xml.Version);

                string sAprSecond = "--";
                string sProOFinPmt = "--";
                string principalPaidOverHorizonSecond = "--";
                string financeChargesPaidOverHorizon = "--";
                string remainingBalanceAfterHorizon = "--";
                bool areRatesExpired = resultItem.First.Xml.AreRatesExpired;

                SortClosingCost(dataLoan, resultItem.First.RateOption);

                bool hasTitleQuote = ManageFees(writer, feeDescriptionsFirst, resultItem.First.RateOption, E_sLienPosT.First);
                var lpTemplateNmSecond = "--";
                var closingCostSecond = "--";
                var sFinChargeSecond = "--";

                if (state.IsComboScenario && !resultItem.HasError)
                {
                    lpTemplateNmSecond = resultItem.Second.RateOption.LpTemplateNm;
                    sAprSecond = resultItem.Second.RateOption.Apr_rep + "%";
                    sProOFinPmt = "$" + resultItem.Second.RateOption.FirstPmtAmt_rep;
                    principalPaidOverHorizonSecond = resultItem.Second.RateOption.PrincipalPaidOverHorizon;
                    financeChargesPaidOverHorizon = resultItem.Second.RateOption.FinanceChargesPaidOverHorizon;
                    remainingBalanceAfterHorizon = resultItem.Second.RateOption.RemainingBalanceAfterHorizon;
                    SortClosingCost(dataLoan, resultItem.Second.RateOption);
                    bool hasTitleInSecond = ManageFees(writer, feeDescriptionsSecond, resultItem.Second.RateOption, E_sLienPosT.Second);
                    hasTitleQuote = hasTitleQuote || hasTitleInSecond;
                    areRatesExpired = areRatesExpired || resultItem.Second.Xml.AreRatesExpired;

                    writer.WriteElementString("PrepaidChargesSecond", resultItem.Second.RateOption.PrepaidCharges);
                    writer.WriteElementString("NonPrepaidChargesSecond", resultItem.Second.RateOption.NonPrepaidCharges);

                    string pointRepSecond = GetPointRep(brokerdb, isLenderPaidOriginatorCompAndAdditionToFees, resultItem.Second.RateOption);
                    writer.WriteElementString("PointSecond", pointRepSecond);
                    writer.WriteElementString("MarginSecond", resultItem.Second.RateOption.Margin_rep);
                    writer.WriteElementString("TeaserRateSecond", resultItem.Second.RateOption.TeaserRate_rep);

                    writer.WriteElementString("DTISecond", DtiToRep(convert, resultItem.ComboData.SecondDti));

                    writer.WriteElementString("CheckSumSecond", resultItem.Second.Xml.UniqueChecksum);
                    writer.WriteElementString("QRateSecond", resultItem.Second.RateOption.QRate_rep);
                    writer.WriteElementString("QualPmtSecond", resultItem.Second.RateOption.QualPmt_rep);
                    writer.WriteElementString("RateOptionIdSecond", resultItem.Second.RateOption.RateOptionId);
                    writer.WriteElementString("IsBlockedRateLockSubmissionSecond", resultItem.Second.Xml.IsBlockedRateLockSubmission.ToString());
                    writer.WriteElementString("RateLockSubmissionUserWarningMessageSecond", resultItem.Second.Xml.RateLockSubmissionUserWarningMessage);
                    sFinChargeSecond =  string.IsNullOrEmpty(resultItem.Second.RateOption.sFinCharge) ? "--" : resultItem.Second.RateOption.sFinCharge;
                    closingCostSecond = resultItem.Second.RateOption.ClosingCost;
                    writer.WriteElementString("ClosingCostRowCountSecond", resultItem.Second.RateOption.ClosingCostBreakdown.Count.ToString());
                    writer.WriteElementString("VersionSecond", resultItem.Second.Xml.Version);
                    writer.WriteElementString("TotalCashCloseSecond", resultItem.Second.RateOption.CashToClose);
                }

                writer.WriteElementString("sFinChargeSecond", sFinChargeSecond);
                writer.WriteElementString("totalDti", (state.IsComboScenario ? resultItem.ComboData.TotalDti : resultItem.First.RateOption.BottomRatio_rep) + "%");
                writer.WriteElementString("TotalPI", state.IsComboScenario ? convert.ToMoneyString(resultItem.ComboData.TotalPI, FormatDirection.ToRep) : ("$" + resultItem.First.RateOption.FirstPmtAmt_rep)) ;
                writer.WriteElementString("sMonthlyPmt", state.IsComboScenario ? convert.ToMoneyString(resultItem.ComboData.TotalMonthlyPayment, FormatDirection.ToRep) : resultItem.First.RateOption.sMonthlyPmtPe);

                writer.WriteElementString("ClosingCostSecond", closingCostSecond);
                writer.WriteElementString("CombinedClosingCost", state.IsComboScenario ? convert.ToMoneyString(resultItem.ComboData.TotalClosingCosts, FormatDirection.ToRep) : resultItem.First.RateOption.ClosingCost);
                writer.WriteElementString("sAprSecond", sAprSecond);
                writer.WriteElementString("sProOFinPmt",sProOFinPmt);
                writer.WriteElementString("LpTemplateNmSecond", lpTemplateNmSecond);
                writer.WriteElementString("PrincipalPaidOverHorizonSecond", principalPaidOverHorizonSecond);
                writer.WriteElementString("FinanceChargesPaidOverHorizonSecond", financeChargesPaidOverHorizon);
                writer.WriteElementString("RemainingBalanceAfterHorizonSecond", remainingBalanceAfterHorizon);
                ////writer.WriteElementString("SavingsVersusCurrentLoanSecond", resultItem?.Second?.RateOption?.SavingsVersusCurrentLoan ?? "--"); // This doesn't make sense either

                writer.WriteElementString("HasTitleQuote", hasTitleQuote ? "true" : "false");

                var sProdRLckdExpiredD = dataLoan.CalculateRateLockExpirationDate(convert, CDateTime.Create(DateTime.Now), int.Parse(state.sProdRLckdDays_rep));
                writer.WriteElementString("sProdRLckdExpiredD", sProdRLckdExpiredD.DateTimeForComputation.ToShortDateString());

                if (areRatesExpired)
                {
                    writer.WriteElementString("RatesAreExpired", "True");
                }

                writer.WriteElementString("RateLockSubmissionUserWarningMessage", resultItem.First.Xml.RateLockSubmissionUserWarningMessage);
                writer.WriteElementString("IsBlockedRateLockSubmission", resultItem.First.Xml.IsBlockedRateLockSubmission.ToString());

                if (resultItem.First.RateOption.IsDisqualified)
                {
                    writer.WriteElementString("AlertLink", "disqualified");
                    writer.WriteElementString("AlertLinkText", resultItem.First.RateOption.DisqualReason);
                }
                else if (resultItem.First.RateOption.IsBlockedRateLockSubmission)
                {
                    if (dataLoan.lpeRunModeT == E_LpeRunModeT.Full)
                    {
                        if (canRegister)
                        {
                            writer.WriteElementString("RegisterLinkText", "register");
                        }
                        else
                        {
                            writer.WriteElementString("AlertLink", showSecondLienInfo ? "register" : "register?");
                            writer.WriteElementString("AlertLinkText", cantRegisterReason);
                        }
                    }
                    else if (dataLoan.lpeRunModeT == E_LpeRunModeT.OriginalProductOnly_Direct)
                    {
                        if (principal.IsRateLockedAtSubmission)
                        {
                            writer.WriteElementString("AlertLink", "lock unavailable");
                            writer.WriteElementString("AlertLinkText", "Lock Unavailable");
                        }
                    }
                    //dont do anything for not allowed
                }
                else
                {
                    if (dataLoan.lpeRunModeT == E_LpeRunModeT.Full)
                    {
                        if (principal.IsRateLockedAtSubmission == false)
                        {
                            if (canRegister)
                            {
                                writer.WriteElementString("RegisterLinkText", "register");
                            }
                            else
                            {
                                writer.WriteElementString("AlertLink", showSecondLienInfo ? "register" : "register?");
                                writer.WriteElementString("AlertLinkText", cantRegisterReason);
                            }

                        }
                        else if (canLock)
                        {
                            writer.WriteElementString("RegisterLinkText", "register");
                            writer.WriteElementString("LockLinkText", "lock rate");
                        }
                        else
                        {
                            if (canRegister)
                            {
                                writer.WriteElementString("RegisterLinkText", "register");
                            }
                            else
                            {
                                writer.WriteElementString("RegisterLinkText", showSecondLienInfo ? "register" : "register?");
                                writer.WriteElementString("RegisterLinkDisableReason", cantRegisterReason);
                            }

                            if (!showSecondLienInfo)
                            {
                                writer.WriteElementString("AlertLink", "lock rate?");
                                writer.WriteElementString("AlertLinkText", canLockReason);
                            }
                        }
                    }
                    else if (dataLoan.lpeRunModeT == E_LpeRunModeT.OriginalProductOnly_Direct)
                    {
                        if (principal.IsRateLockedAtSubmission)
                        {
                            if (canLock)
                            {
                                writer.WriteElementString("LockLinkText", "lock rate");
                            }
                            else if(!showSecondLienInfo)
                            {
                                writer.WriteElementString("AlertLink", "lock rate?");
                                writer.WriteElementString("AlertLinkText", canLockReason);
                            }
                        }
                    }
                }



                count++;
                writer.WriteEndElement();
            }
            writer.WriteEndElement(); //group
            writer.WriteEndElement(); //results

            writer.WriteElementString("ShowUpfrontMipRow", showUpfrontMipRow.ToString());

            #endregion

            #region <addendum>
            count = 0;

            writer.WriteStartElement("Addendum");

            int firstValid2ndProgramIndex = -1;
            foreach (PinStateResultItem resultItem in okayResults.Union(errorResults))
            {
                if (count % 3 == 0)
                {
                    pageCount += 1; // Note: pageCount continues from results
                    if (count != 0)
                    {
                        if(firstValid2ndProgramIndex != -1)
                        {
                            var index = firstValid2ndProgramIndex % 3 == 0 ? 3 : firstValid2ndProgramIndex;
                            writer.WriteElementString("firstValid2ndIndex", index.ToString());
                        }

                        writer.WriteEndElement();
                    }
                    writer.WriteStartElement("Group");
                    writer.WriteAttributeString("Page", pageCount.ToString());
                    firstValid2ndProgramIndex = -1;
                }

                writer.WriteStartElement("LoanProgram");

                PricingState state = resultItem.State;

                dataLoan.CalcModeT = E_CalcModeT.LendersOfficePageEditing;

                writer.WriteAttributeString("PinId", state.Id.ToString("N"));
                writer.WriteElementString("PinFriendlyId", state.FriendlyId.ToString("000"));
                writer.WriteElementString("lLpTemplateId", state.lLpTemplateId.ToString("N"));
                writer.WriteElementString("PinId", state.Id.ToString("N"));

                RateMergeGroup g = new RateMergeGroup(state.GetMergeKey(E_sLienPosT.First));


                var lpTemplateNmSecond = "--";
                var rateMergeNameSecond = "--";
                string lLpTemplateIdSecond = "--";
                if (state.IsComboScenario)
                {
                    var secondGroup = new RateMergeGroup(state.GetMergeKey(E_sLienPosT.Second));
                    rateMergeNameSecond = secondGroup.Name;
                    if(!resultItem.HasError)
                    {
                        lpTemplateNmSecond = resultItem.Second.RateOption.LpTemplateNm; 
                    }
                    lLpTemplateIdSecond = state.secondLoanlLpTemplateId.Value.ToString("N");
                    writer.WriteElementString("CheckSumSecond", resultItem.Second.Xml.UniqueChecksum);
                    writer.WriteElementString("sProThisMPmt", "$" + dataLoan.sProThisMPmt_rep);
                    writer.WriteElementString("Point", "$" + dataLoan.sProThisMPmt_rep);
                    writer.WriteElementString("sNoteIR", dataLoan.sNoteIR + "%");

                }

                writer.WriteElementString("LpTemplateNmSecond", lpTemplateNmSecond);
                writer.WriteElementString("RateMergeNameSecond", rateMergeNameSecond);
                writer.WriteElementString("lLpTemplateIdSecond", lLpTemplateIdSecond);

                if (resultItem.First == null || resultItem.First.ResultItem == null || resultItem.First.RateOption == null)
                {
                    // this means we didnt find any programs to run
                    // or the rate option is not found

                    writer.WriteElementString("ErrorMessage", "The pin state is no longer available.");
                    writer.WriteElementString("RateMergeName", g.Name);
                    writer.WriteElementString("LpTemplateNm", state.lLpTemplateNm);
                    writer.WriteEndElement();
                    count++;
                    continue;
                }
                else if (resultItem.First.ResultItem.IsErrorMessage)
                {
                    writer.WriteElementString("ErrorMessage", resultItem.First.ResultItem.ErrorMessage);
                    writer.WriteElementString("LpTemplateNm", state.lLpTemplateNm);
                    writer.WriteEndElement();
                    count++;
                    continue;
                }
                else if (resultItem.First.RateOption.IsDisqualified)
                {
                    writer.WriteElementString("ErrorMessage", resultItem.First.RateOption.DisqualReason);
                    writer.WriteElementString("LpTemplateNm", state.lLpTemplateNm);
                    writer.WriteEndElement();
                    count++;
                    continue;
                }

                writer.WriteElementString("RateMergeName", g.Name);

                CApplicantRateOption rateOption = resultItem.First.RateOption;
                CApplicantPriceXml priceXml = resultItem.First.Xml;

                writer.WriteElementString("LpTemplateNm", rateOption.LpTemplateNm);
                writer.WriteElementString("CheckSum", priceXml.UniqueChecksum);
                writer.WriteElementString("Version", priceXml.Version);

                // Convert feeDescriptions to ordered list (only need to iterate through it now).
                // Note: Done outside loop for efficiency. No need to redo this for every loan program.
                List<KeyValuePair<string, FeeDescriptionItem>> orderedFeeDiscriptionsFirst = feeDescriptionsFirst.ToList();
                List<KeyValuePair<string, FeeDescriptionItem>> orderedFeeDiscriptionsSecond = feeDescriptionsSecond?.ToList();

                // OPM 106889 - Comparison of RESPA Fees
                WriteFeeAddendum(writer, rateOption, orderedFeeDiscriptionsFirst, state.sLienPosT, dataLoan.sDisclosureRegulationT);

                var secondResult = resultItem.Second;
                if(secondResult != null && secondResult.RateOption != null)
                {
                    if(firstValid2ndProgramIndex == -1)
                    {
                        firstValid2ndProgramIndex = count + 1;
                    }

                    WriteFeeAddendum(writer, secondResult.RateOption, orderedFeeDiscriptionsSecond, E_sLienPosT.Second, dataLoan.sDisclosureRegulationT);
                    
                }

                var combinedClosingCost = resultItem.First.RateOption.ClosingCost;
                if(state.IsComboScenario && !resultItem.HasError)
                {
                    combinedClosingCost = LoanComparisonXml.SumMoney(m_losConvert, resultItem.First.RateOption.ClosingCost, resultItem.Second.RateOption.ClosingCost);
                }

                writer.WriteElementString("CombinedClosingCost", combinedClosingCost);

                count++;
                writer.WriteEndElement();
            }

            if (firstValid2ndProgramIndex != -1)
            {
                var index = firstValid2ndProgramIndex % 3 == 0 ? 3 : firstValid2ndProgramIndex;
                writer.WriteElementString("firstValid2ndIndex", index.ToString()); ;
            }

            writer.WriteEndElement(); //group
            writer.WriteEndElement(); //addendum
            #endregion

            #region <report>
            writer.WriteStartElement("Report");
            writer.WriteElementString("GenerationTime", Tools.GetDateTimeNowString());
            writer.WriteElementString("TotalPageCount", pageCount.ToString());
            writer.WriteElementString("RequestId", statePriceCacheId.ToString("N"));
            writer.WriteElementString("ResultId", resultId.ToString("N"));
            writer.WriteElementString("GenerationTimeTicks", DateTime.Now.Ticks.ToString());
            writer.WriteElementString("IneligiblePinsCount", ineligibleCount.ToString());
            writer.WriteEndElement();
            #endregion

            writer.WriteEndElement(); //loan comparison

        }

        private static void WriteFeeAddendum(XmlWriter writer, CApplicantRateOption rateOption, List<KeyValuePair<string, FeeDescriptionItem>> orderedFeeDiscriptions, E_sLienPosT sLienPosT, E_sDisclosureRegulationT sDisclosureRegulationT)
        {
            if (orderedFeeDiscriptions == null)
            {
                return;
            }
            string suffix = "";
            string respaFeeItemElement = "RespaFeeItem";

            if (sLienPosT == E_sLienPosT.Second)
            {
                respaFeeItemElement = "RespaFeeItemSecond";
                suffix = "Second";
            }

            if (sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                // Sort by loan section then by hudline.
                orderedFeeDiscriptions.Sort(FeeDescriptionComparator);
            }
            else
            {
                orderedFeeDiscriptions.Sort((a, b) => int.Parse(a.Key).CompareTo(int.Parse(b.Key))); // sort by hudline
            }

            // With new closing cost set, can have multiple RespaFeeItems with the same HUD line.
            Dictionary<string, List<RespaFeeItem>> fees = new Dictionary<string, List<RespaFeeItem>>();
            foreach (RespaFeeItem rfi in rateOption.ClosingCostBreakdown)
            {
                if (fees.ContainsKey(rfi.HudLine))
                {
                    fees[rfi.HudLine].Add(rfi);
                }
                else
                {
                    List<RespaFeeItem> newList = new List<RespaFeeItem>();
                    newList.Add(rfi);
                    fees.Add(rfi.HudLine, newList);
                }
            }
            foreach (KeyValuePair<string, FeeDescriptionItem> item in orderedFeeDiscriptions)
            {
                foreach (string description in item.Value.FeeDescriptions)
                {
                    writer.WriteStartElement(respaFeeItemElement);
                    writer.WriteElementString("HudLine", item.Key); // NOTE: included in case we need it later

                    writer.WriteElementString("Description", description);

                    // Find RespaFeeItems with the same HUD line and see which one matches the description.
                    if (fees.ContainsKey(item.Key))
                    {
                        bool found = false;
                        foreach (RespaFeeItem rfi in fees[item.Key])
                        {
                            if (GetAddendumFeeDescription(rfi).Equals(description, StringComparison.OrdinalIgnoreCase))
                            {
                                writer.WriteElementString("Fee", m_losConvert.ToMoneyString(rfi.Fee, FormatDirection.ToRep));
                                found = true;
                                break;
                            }
                        }

                        if (found == false)
                        {
                            writer.WriteElementString("Fee", "--");
                        }
                    }
                    else // If no matching fee, add fee value of $0.00
                    {
                        writer.WriteElementString("Fee", "--");
                    }

                    // NOTE: Not including Source, since it's not needed for addendum and 
                    //writer.WriteElementString("Source", "");
                    writer.WriteEndElement();
                }
            }

            
                writer.WriteElementString($"PrepaidCharges{suffix}", rateOption.PrepaidCharges);
                writer.WriteElementString($"NonPrepaidCharges{suffix}", rateOption.NonPrepaidCharges);
                writer.WriteElementString($"ClosingCost{suffix}", rateOption.ClosingCost);
         
        }

        private static bool ManageFees(XmlWriter writer, Dictionary<string, FeeDescriptionItem> feeDescriptions, CApplicantRateOption rateOption, E_sLienPosT sLienPosT)
        {
            string elementType = "RespaFeeItem";
            bool hasTitleQuote = false;

            if (sLienPosT == E_sLienPosT.Second)
            {
                elementType = "RespaFeeItemSecond";
            }

            foreach (RespaFeeItem item in rateOption.ClosingCostBreakdown)
            {
                writer.WriteStartElement(elementType);
                writer.WriteElementString("HudLine", item.HudLine);
                writer.WriteElementString("Description", item.Description);
                writer.WriteElementString("Fee", m_losConvert.ToMoneyString(item.Fee, FormatDirection.ToRep));
                writer.WriteElementString("Source", item.DescriptionSource);
                writer.WriteElementString("LoanSec", Tools.GetIntegratedDisclosureString(item.DisclosureSectionT));
                writer.WriteEndElement();

                if (item.Source == E_ClosingFeeSource.FirstAmerican)
                {
                    hasTitleQuote = true;
                }

                // OPM 106889 - Build list of fee descriptions here, to be used in addendum.
                // Line/Description pairs, but can have more than one description per line.
                string description = GetAddendumFeeDescription(item);
                if (feeDescriptions.ContainsKey(item.HudLine))
                {
                    FeeDescriptionItem descriptionItem = feeDescriptions[item.HudLine];
                    if (!descriptionItem.FeeDescriptions.Contains(description))
                    {
                        descriptionItem.FeeDescriptions.Add(description);
                    }
                }
                else
                {
                    FeeDescriptionItem descriptionItem = new FeeDescriptionItem();
                    descriptionItem.FeeDescriptions.Add(description);
                    descriptionItem.Hudline = item.HudLine;
                    descriptionItem.LoanSection = item.DisclosureSectionT;
                    feeDescriptions.Add(item.HudLine, descriptionItem);
                }
            }

            return hasTitleQuote;
        }

        private static string GetPointRep(BrokerDB brokerdb, bool isLenderPaidOriginatorCompAndAdditionToFees, CApplicantRateOption rateOption)
        {
            string pointRep;
            if (brokerdb.IsUsePriceIncludingCompensationInPricingResult && isLenderPaidOriginatorCompAndAdditionToFees)
            {
                // OPM 64253
                pointRep = rateOption.PointIncludingOriginatorComp_rep;
            }
            else
            {
                pointRep = rateOption.Point_rep;
            }

            return pointRep;
        }

        private static void SortClosingCost(CPageData dataLoan, CApplicantRateOption rateOption)
        {
            /* If you want to change how the closing cost breakdown is rendered please also see look for the same
             * comment(s) in GetResultsNewUI.ascx, GetResultsNewUI.ascx.cs, LoanProductResultControlNewUI.cs, LoanComparisonXml.cs.
             */
            // This generates the breakdown on the loan comparison report.
            if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                // Sort by Loan Section then by Hudline
                rateOption.ClosingCostBreakdown.Sort(delegate (RespaFeeItem a, RespaFeeItem b)
                {
                    int loanSecCompareVal = a.DisclosureSectionT.CompareTo(b.DisclosureSectionT);
                    if (loanSecCompareVal == 0)
                    {
                        return int.Parse(a.HudLine).CompareTo(int.Parse(b.HudLine));
                    }
                    else
                    {
                        return loanSecCompareVal;
                    }
                });
            }
            else
            {
                rateOption.ClosingCostBreakdown.Sort((a, b) => int.Parse(a.HudLine).CompareTo(int.Parse(b.HudLine))); // sort closing cost breakdown by hudline
            }
        }

        private static string GetAddendumFeeDescription(RespaFeeItem item)
        {
            // If description is blank, return placeholder string
            if (string.IsNullOrEmpty(item.Description.TrimWhitespaceAndBOM()))
            {
                string template = "Unspecified GFE Box {0} Fee";
                switch (item.GfeSectionT)
                {
                    case E_GfeSectionT.B1:
                        return string.Format(template, "A1");
                    case E_GfeSectionT.NotApplicable:
                    case E_GfeSectionT.LeaveBlank:
                        return "Unspecified Fee";
                    default:
                        return string.Format(template, item.GfeSectionT.ToString());
                }
            }

            // If not blank, return trimmed description
            return item.Description.TrimWhitespaceAndBOM();
        }

        public static bool CheckSectionVisibility(EditorStatus editorStatus, bool isLead)
        {
            return editorStatus == EditorStatus.Both || (isLead ? editorStatus == EditorStatus.Lead : editorStatus == EditorStatus.Loan);
        }
    }
}
