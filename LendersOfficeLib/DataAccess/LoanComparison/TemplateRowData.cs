﻿namespace DataAccess.LoanComparison
{
    using LendersOfficeApp.los.RatePrice;
    using System;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Collections.Generic;
    using System.Xml.Linq;
    using LendersOffice.Admin;
    using LendersOffice.RatePrice.FileBasedPricing;

    public class TemplateRowData
    {
        public RateMergeGroupKey Key { get; private set; }
        public Guid lLpTemplateId { get; private set; }
        public string lLpTemplateNm { get; private set; }
        public string productCode { get; private set; }
        public string lLpInvestorNm { get; private set; }

        private TemplateRowData(ILoanProgramTemplate item)
        {
            lLpTemplateId = item.lLpTemplateId;
            lLpTemplateNm = item.lLpTemplateNm;
            productCode = item.ProductCode;
            lLpInvestorNm = item.lLpInvestorNm;

            RateMergeGroupKeyParameters parameters = new RateMergeGroupKeyParameters()
            {
                lTerm_rep = item.lTerm_rep,
                lDue_rep = item.lDue_rep,
                lFinMethT = item.lFinMethT,
                lBuydwnR1_rep = item.lBuydwnR1_rep,
                lBuydwnR2_rep = item.lBuydwnR2_rep,
                lBuydwnR3_rep = item.lBuydwnR3_rep,
                lBuydwnR4_rep = item.lBuydwnR4_rep,
                lBuydwnR5_rep = item.lBuydwnR5_rep,
                lArmIndexNameVstr = item.lArmIndexNameVstr,
                lLpProductType = item.lLpProductType,
                lRAdj1stCapMon_rep = item.lRadj1stCapMon_rep,
                lRAdj1stCapR_rep = item.lRadj1stCapR_rep,
                lRAdjCapR_rep = item.lRAdjCapR_rep,
                lRAdjLifeCapR_rep = item.lRAdjLifeCapR_rep,
                lRAdjCapMon_rep = item.lRAdjCapMon_rep,
                lPrepmtPeriod_rep = (item.lHardPrepmtPeriodMonths + item.lSoftPrepmtPeriodMonths).ToString()
            };

            Key = new RateMergeGroupKey(parameters);
        }

        private TemplateRowData(DbDataReader reader)
        {
            lLpTemplateId = (Guid)reader["lLpTemplateId"];
            lLpTemplateNm = (string)reader["lLpTemplateNm"];
            productCode = (string)reader["ProductCode"];
            lLpInvestorNm = (string)reader["lLpInvestorNm"];

            RateMergeGroupKeyParameters parameters = new RateMergeGroupKeyParameters()
            {
                lTerm_rep = reader["lTerm"].ToString(),
                lDue_rep = reader["lDue"].ToString(),
                lFinMethT = (E_sFinMethT)((int)reader["lFinMethT"]),
                lBuydwnR1_rep = reader["lBuydwnR1"].ToString(),
                lBuydwnR2_rep = reader["lBuydwnR2"].ToString(),
                lBuydwnR3_rep = reader["lBuydwnR3"].ToString(),
                lBuydwnR4_rep = reader["lBuydwnR4"].ToString(),
                lBuydwnR5_rep = reader["lBuydwnR5"].ToString(),
                lArmIndexNameVstr = reader["lArmIndexNameVstr"].ToString(),
                lLpProductType = reader["lLpProductType"].ToString(),
                lRAdj1stCapMon_rep = reader["lRAdj1stCapMon"].ToString(),
                lRAdj1stCapR_rep = reader["lRAdj1stCapR"].ToString(),
                lRAdjCapR_rep = reader["lRAdjCapR"].ToString(),
                lRAdjLifeCapR_rep = reader["lRAdjLifeCapR"].ToString(),
                lRAdjCapMon_rep = reader["lRAdjCapMon"].ToString(),
                lPrepmtPeriod_rep = reader["lPrepmtPeriod"].ToString()
            };

            Key = new RateMergeGroupKey(parameters);
        }

        private static List<TemplateRowData> GetProgramsFromActualPricingBroker(BrokerDB broker, IEnumerable<Guid> programIds)
        {
            var snapshot = broker.RetrieveLatestManualImport();

            List<TemplateRowData> list = new List<TemplateRowData>();

            foreach (var lLpTemplateId in programIds)
            {
                FileBasedLoanProgramTemplate item = null;
                if (snapshot.TryGetLoanProgramTemplate(lLpTemplateId, out item))
                {
                    if (item.BrokerId != broker.ActualPricingBrokerId)
                    {
                        continue;
                    }
                    list.Add(new TemplateRowData(item));
                }
            }

            return list;
        }

        public static List<TemplateRowData> GetPrograms(DataSrc lpeDataSrc, BrokerDB broker, IEnumerable<Guid> programIds)
        {
            if (broker.ActualPricingBrokerId != Guid.Empty)
            {
                return GetProgramsFromActualPricingBroker(broker, programIds);
            }
            else
            {
                return GetPrograms(lpeDataSrc, broker.BrokerID, programIds);
            }
        }

        private static List<TemplateRowData> GetPrograms(DataSrc lpeDataSrc, Guid brokerId, IEnumerable<Guid> programIds)
        {
            XElement programIdXml = new XElement("r");
            foreach (Guid programId in programIds)
            {
                programIdXml.Add(new XElement("id", programId.ToString()));
            }

            SqlParameter[] parameters = new SqlParameter[] {
                    new SqlParameter("@BrokerId", brokerId),
                    new SqlParameter("@Ids", programIdXml.ToString(SaveOptions.DisableFormatting))
            };

            List<TemplateRowData> list = new List<TemplateRowData>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(lpeDataSrc, "ListLoanProgramsForLoanComparisonByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    Guid lLpTemplateId = (Guid)reader["lLpTemplateId"];
                    list.Add(new TemplateRowData(reader));
                }
            }

            return list;
        }


        public static IEnumerable<TemplateRowData> List(DataSrc lpeDataSrc, BrokerDB broker, Guid lpePriceGroupId)
        {
            List<Guid> lLpTemplateIdHashSet = new List<Guid>();

            // 4/4/2015 dd - First get all active program belong to price group.
            SqlParameter[] parameters = {
                                            new SqlParameter("@LpePriceGroupId", lpePriceGroupId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(broker.BrokerID, "LPE_PRICE_GROUP_ListLpTemplateIdByPriceGroupId", parameters))
            {
                while (reader.Read())
                {
                    lLpTemplateIdHashSet.Add((Guid)reader["lLpTemplateId"]);
                }
            }


            return GetPrograms(lpeDataSrc, broker, lLpTemplateIdHashSet);
        }

    }

}
