namespace DataAccess
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using DataAccess;
    using System.Linq;
    using LendersOffice.Common;
    using LendersOffice.UI;

    public class CLiaCollection : CXmlRecordCollection, ILiaCollection
    {
        public const string SortedViewRowFilter = "DebtT NOT IN ('0', '2', '7')";
        private ILiabilityAlimony m_alimony = null;
        private ILiabilityChildSupport childSupport = null;
        private ILiabilityJobExpense m_jobRelated1 = null;
		private ILiabilityJobExpense m_jobRelated2 = null;

        public CLiaCollection(CAppBase appData, string xmlContent)
			: base( appData.LoanData , appData, E_DataSetT.LiabilityXml, xmlContent )
		{
		}

		
		protected override bool IsTypeOfSpecialRecord( Enum recordT )
		{
			return CLiaFields.IsTypeOfSpecialRecord( (E_DebtT) recordT );
		}
		
		
		protected override ICollectionItemBase2 CreateRegularRawRecord()
		{
			return CLiaRegular.Create( m_appData, m_ds, this );
		}
		

		override protected ArrayList SpecialRecords
		{
			get
			{
				ArrayList list = new ArrayList( 4 );
				ILiability rec = GetAlimony( false );
				if( null != rec )
					list.Add( rec );
				rec = GetJobRelated1( false );
				if( null != rec )
					list.Add( rec );
				rec = GetJobRelated2( false );
				if( null != rec )
					list.Add( rec );

                rec = this.GetChildSupport(false);
                if (rec != null)
                {
                    list.Add(rec);
                }

				return list;
			}
		}

		override protected ICollectionItemBase2 Reconstruct( int iRow )
		{
			if( iRow < 0 )
				throw new CBaseException(ErrorMessages.Generic, "Invalid iRow value for xml record.");

			E_DebtT type;
			try
			{
				type = (E_DebtT) int.Parse( m_ds.Tables[0].Rows[iRow]["DebtT"].ToString() );
			}
			catch
			{
				var reg = CLiaRegular.Reconstruct( m_appData, m_ds, this, iRow );
				reg.DebtT = E_DebtRegularT.Other;
				return reg;
			}

			switch( type )
			{
				case E_DebtT.JobRelatedExpense:
					return CLiaJobExpense.Reconstruct( m_appData, m_ds, this, iRow );
				case E_DebtT.Alimony:
					return CLiaAlimony.Reconstruct( m_appData, m_ds, this, iRow );
                case E_DebtT.ChildSupport:
                    return CLiaChildSupport.Reconstruct(m_appData, m_ds, this, iRow);
				default:
					return CLiaRegular.Reconstruct( m_appData, m_ds, this, iRow );
			}
		}

		
		/// <summary>
		/// Returns nulls if it doesn't exist and bForceCreate = false
		/// </summary>
		public ILiabilityAlimony GetAlimony( bool bForceCreate )
		{
			if( null == m_alimony && bForceCreate )
			{
				m_alimony = CLiaAlimony.Create( m_appData, m_ds, this );
			}
			return m_alimony;
        }

        /// <summary>
        /// Returns nulls if it doesn't exist and forceCreate = false
        /// </summary>
        public ILiabilityChildSupport GetChildSupport(bool forceCreate)
        {
            if (this.childSupport == null && forceCreate)
            {
                this.childSupport = CLiaChildSupport.Create(m_appData, m_ds, this);
            }

            return this.childSupport;
        }

        /// <summary>
        /// Returns nulls if it doesn't exist and bForceCreate = false
        /// </summary>
        public ILiabilityJobExpense GetJobRelated1( bool bForceCreate )
		{
			if( null == m_jobRelated1 && bForceCreate )
			{
				m_jobRelated1 = CLiaJobExpense.CreateJobExpense1( m_appData, m_ds, this );
			}
			return m_jobRelated1;
		}

		/// <summary>
		/// Return nulls if it doesn't exist and bForceCreate = false
		/// </summary>
		public ILiabilityJobExpense GetJobRelated2( bool bForceCreate )
		{
			if( null == m_jobRelated2 && bForceCreate )
			{
				m_jobRelated2 = CLiaJobExpense.CreateJobExpense2( m_appData, m_ds, this );
			}
			return m_jobRelated2;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns>returns null if all slots are full</returns>
		public ILiabilityJobExpense CreateNewJobExpenseAtFirstAvailSlot()
		{
			ILiabilityJobExpense jobExp = GetJobRelated1( false );
			if( null == jobExp )
				return GetJobRelated1( true );

			jobExp = GetJobRelated2( false );
			if( null == jobExp )
				return GetJobRelated2( true );

			return null;
		}

		public DataView SortedView
		{
			get
			{
				DataTable table = m_ds.Tables[0];
				DataView view = table.DefaultView;

                // Not alimony, job expense, or child support
                view.RowFilter = SortedViewRowFilter;
				view.Sort = "OrderRankValue";
				return view;
			}
		}

		override protected E_ExistingRecordFaith DetermineRawRecordDestiny(ICollectionItemBase2 record )
		{
			var rec = (ILiability) record;
			switch( rec.DebtT )
			{
				case E_DebtT.Alimony:
					if( null != m_alimony )
						return E_ExistingRecordFaith.E_SpecialInvalid;
					m_alimony = rec as ILiabilityAlimony;
					return E_ExistingRecordFaith.E_SpecialTaken;
					
				case E_DebtT.JobRelatedExpense:
				{
					ILiabilityJobExpense jobExpenseRec = (ILiabilityJobExpense) rec;
					switch( jobExpenseRec.DebtJobExpenseT )
					{
						case E_DebtJobExpenseT.JobExpense1:
							if( null != m_jobRelated1 )
								return E_ExistingRecordFaith.E_SpecialInvalid;
							m_jobRelated1 = jobExpenseRec;
							return E_ExistingRecordFaith.E_SpecialTaken;

						case E_DebtJobExpenseT.JobExpense2:
							if( null != m_jobRelated2 )
								return E_ExistingRecordFaith.E_SpecialInvalid;
							m_jobRelated2 = jobExpenseRec;
							return E_ExistingRecordFaith.E_SpecialTaken;	
					}
					return E_ExistingRecordFaith.E_SpecialInvalid;
				}

                case E_DebtT.ChildSupport:
                    if (this.childSupport != null)
                    {
                        return E_ExistingRecordFaith.E_SpecialInvalid;
                    }

                    this.childSupport = rec as ILiabilityChildSupport;
                    return E_ExistingRecordFaith.E_SpecialTaken;

				default:
					return E_ExistingRecordFaith.E_RegularOK;
			}
		}
		
		/// <summary>
		/// Update CAppData with the new values
		/// </summary>
		override public void Flush()
		{
			ExecuteDeathRowRecords();

			decimal totBal = 0; //aLiaBalTot;
			decimal totMon = 0; //aLiaMonTot;
			decimal totPdOff = 0; // aLiaPdOffTot;

			int count = CountRegular;
			for( int i = 0; i < count; ++i )
			{
				ILiabilityRegular rec = GetRegularRecordAt( i );
				
				rec.PrepareToFlush();

                // OPM 143988: DT - Don't include liabilities marked as excluded or that are
                //                  associated with sold REO records in the balance totals. At 
                //                  some point, force liabilities associated with sold REOs to
                //                  be excluded, and only check the exclusion field here.
                bool isAssociatedWithSoldReo = false;

                if (rec.MatchedReRecordId != Guid.Empty) {
                    isAssociatedWithSoldReo = m_appData.aReCollection.GetRegRecordOf(rec.MatchedReRecordId).StatT == E_ReoStatusT.Sale;
                }

                if (!isAssociatedWithSoldReo && !rec.ExcFromUnderwriting)
                {
                    totBal += rec.Bal;

                    if (rec.WillBePdOff && rec.PayoffTiming == E_Timing.At_Closing)
                    {
                        totPdOff += rec.PayoffAmt;
                    }
                }

                if (!rec.NotUsedInRatio)
                    totMon += rec.Pmt;
                
			}

			count = CountSpecial;
			for( int i = 0; i < count; ++i )
			{
				var rec = GetSpecialRecordAt( i );
				
				rec.PrepareToFlush();

				//totBal += rec.Bal; The special records have 0 balance.
				//if( rec.WillBePdOff )
				//	totPdOff += rec.Bal;

				if( !rec.NotUsedInRatio )
					totMon += rec.Pmt;
			}

            // OPM 174375.  Need to sneak in a special liablity that is the
            // non-primary non-rental retained propery to conform to DU.
            decimal totRetainedNegCf = (m_appData.aBRetainedNegCf + m_appData.aCRetainedNegCf);

            // The following lines will prepare the app data object ready to be saved
            // and all cached calculated values are updated to be used in ratio calculations etc...
            m_appData.aLiaBalTot_ = totBal ;
            m_appData.aLiaMonTot_ = totMon + totRetainedNegCf;
			m_appData.aLiaPdOffTot_ = totPdOff;

			m_appData.aLiaXmlContent = m_ds.GetXml();
            base.Flush();
		}

		override protected void DetachSpecialRecords()
		{
			m_alimony = null;
			m_jobRelated1 = null;
			m_jobRelated2 = null;
            this.childSupport = null;
		}

		public ISubcollection GetSubcollection( bool bInclusiveGroup, E_DebtGroupT group )
		{
			return base.GetSubcollectionCore( bInclusiveGroup, group );
		}

		new public ILiabilityRegular GetRegularRecordAt( int pos )
		{
			return base.GetRegularRecordAt( pos ) as ILiabilityRegular;

		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="recordId"></param>
		/// <returns></returns>
		new public ILiabilityRegular GetRegRecordOf( Guid recordId )
		{
			return base.GetRegRecordOf( recordId ) as ILiabilityRegular;
		}
		/// <summary>
		/// Do not use this, use foreach on GetSubcollection() instead.
		/// </summary>
		/// <param name="i"></param>
		/// <returns></returns>
		new public ILiabilitySpecial GetSpecialRecordAt( int i )
		{
			return (ILiabilitySpecial) base.GetSpecialRecordAt( i );
		}

		new public ILiabilityRegular AddRegularRecord()
		{
			return base.AddRegularRecord() as ILiabilityRegular;
		}

		/// <summary>
		/// This method can throw if cannot add
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		protected override ICollectionItemBase2 AddSpecialRecord( Enum type )
		{
			E_DebtT t = (E_DebtT) type;
			switch( t )
			{
				case E_DebtT.Alimony:
					if( null == m_alimony )
						return GetAlimony( true );
					break;
				case E_DebtT.JobRelatedExpense:
					if( null == m_jobRelated1 )
						return this.GetJobRelated1( true );
					if( null == m_jobRelated2 )
						return this.GetJobRelated2( true );
					break;
                case E_DebtT.ChildSupport:
                    if (this.childSupport == null)
                    {
                        return this.GetChildSupport(true);
                    }
                    break;
			}
			throw new CBaseException(ErrorMessages.Generic, "Cannot add any more special liability record of the requested type = " + t.ToString());
		}
	

		new public ILiabilityRegular AddRegularRecordAt( int pos )
		{			
			return base.AddRegularRecordAt( pos ) as ILiabilityRegular;
		}

        private ISubcollection NormalLiabilityCollection 
        {
            get 
            {
                return GetSubcollection(true, E_DebtGroupT.Regular);
            }
        }
        public bool HasMoreThan7NormalLiablities 
        {
            get 
            {
                return NormalLiabilityCollection.Count > 7;
            }
        }
        /// <summary>
        /// Get first 7 regular liabilities to print on 1003 pg 2.
        /// </summary>
        public ISubcollection First7NormalLiabilities 
        {
            get 
            {
                var mainList = NormalLiabilityCollection;
                if (mainList.Count > 7) 
                {
                    // 5/24/2004 dd - Create an array of record with maximum 7 entries
                    var list = new ICollectionItemBase[7];
                    for (int i = 0; i < 7; i++) 
                    {
                        list[i] = mainList.GetRecordAt(i);
                    }
                    return CXmlRecordSubcollection.Create(list);
                } 
                else 
                {
                    return mainList;
                }
            }
        }

        /// <summary>
        /// Return extra normal liabilities to print on 1003 pg 4 (additional page).
        /// </summary>
        public ISubcollection ExtraNormalLiabilities 
        {
            get 
            {
                var mainList = NormalLiabilityCollection;
                if (mainList.Count > 7) 
                {
                    var list = new ICollectionItemBase[mainList.Count - 7];
                    for (int i = 0; i < mainList.Count - 7; i++) 
                    {
                        list[i] = mainList.GetRecordAt(i + 7);
                    }
                    return CXmlRecordSubcollection.Create(list);

                } 
                else 
                {
                    return CXmlRecordSubcollection.Create(new ICollectionItemBase[0]);
                }
            }
        }

        /// <summary>
        /// Return extra normal liabilities to print on 1003 pg 5 (additional page) for the new 1003
        /// The new 1003 only printout 6 liabilities in normal page.
        /// </summary>
        public ISubcollection ExtraNormalLiabilitiesInNew1003 
        {
            get 
            {
                var mainList = NormalLiabilityCollection;
                if (mainList.Count > 6) 
                {
                    var list = new ICollectionItemBase[mainList.Count - 6];
                    for (int i = 0; i < mainList.Count - 6; i++) 
                    {
                        list[i] = mainList.GetRecordAt(i + 6);
                    }
                    return CXmlRecordSubcollection.Create(list);

                } 
                else 
                {
                    return CXmlRecordSubcollection.Create(new ICollectionItemBase[0]);
                }
            }
        }

        /// <summary>
        /// Return then json of liaRegulars of an applicant.
        /// </summary>
        /// <returns>An dictionary of liaRegulars of an applicant.</returns>
        public Dictionary<string, object> ToInputModel()
        {
            Dictionary<string, object> inputModelDict = new Dictionary<string, object>();

            inputModelDict.Add("Alimony", this.GetAlimony(true).ToInputFieldModel());
            inputModelDict.Add("ChildSupport", this.GetChildSupport(true).ToInputFieldModel());
            inputModelDict.Add("JobRelated1", this.GetJobRelated1(true).ToInputFieldModel());
            inputModelDict.Add("JobRelated2", this.GetJobRelated2(true).ToInputFieldModel());

            var regularLias= m_sortedList.GetValueList().Cast<ILiabilityRegular>().Where(x => !x.IsOnDeathRow).Select(x => x.ToInputFieldModel());

            inputModelDict.Add("regulars", regularLias);

            var regLiaTemplate = ((ILiabilityRegular)this.CreateRegularRawRecord()).ToInputFieldModel();
            regLiaTemplate.Remove("id");
            inputModelDict.Add("regularTemplate", regLiaTemplate);

            return inputModelDict;
        }

        /// <summary>
        /// Update liability collection from an input field model.
        /// </summary>
        /// <param name="inputModelDict">Input field model to update liabilities.</param> 
        public void BindFromObjectModel(Dictionary<string, object> inputModelDict)
        {
            if (inputModelDict.ContainsKey("Alimony"))
            {
                this.GetAlimony(true).BindFromObjectModel(SerializationHelper.JsonNetDeserialize<SimpleObjectInputFieldModel>(inputModelDict[@"Alimony"].ToString()));
            }

            if (inputModelDict.ContainsKey("ChildSupport"))
            {
                this.GetChildSupport(true).BindFromObjectModel(SerializationHelper.JsonNetDeserialize<SimpleObjectInputFieldModel>(inputModelDict[@"ChildSupport"].ToString()));
            }

            if (inputModelDict.ContainsKey("JobRelated1"))
            {
                this.GetJobRelated1(true).BindFromObjectModel(SerializationHelper.JsonNetDeserialize<SimpleObjectInputFieldModel>(inputModelDict[@"JobRelated1"].ToString()));
            }

            if (inputModelDict.ContainsKey("JobRelated2"))
            {
                this.GetJobRelated2(true).BindFromObjectModel(SerializationHelper.JsonNetDeserialize<SimpleObjectInputFieldModel>(inputModelDict[@"JobRelated2"].ToString()));
            }

            if (inputModelDict.ContainsKey("regulars"))
            {
                var regularInputList = SerializationHelper.JsonNetDeserialize<List<SimpleObjectInputFieldModel>>(inputModelDict[@"regulars"].ToString());
                if (regularInputList != null)
                {
                    var liaRegulars = m_sortedList.GetValueList().Cast<ILiabilityRegular>().ToList();
                    liaRegulars.ForEach(x => x.IsOnDeathRow = true);
                    int currentPosition = 0;

                    foreach (SimpleObjectInputFieldModel regularInput in regularInputList)
                    {
                        ILiabilityRegular regLia = null;
                        Guid liaGuid = Guid.Empty;
                        if (regularInput.ContainsKey("id") && regularInput["id"] != null)
                        {
                            liaGuid = new Guid(regularInput["id"].Value);
                            regLia = liaRegulars.Find(x => x.RecordId == liaGuid);
                        }

                        if (regLia == null)
                        {
                            regLia = this.AddRegularRecordAt(currentPosition);
                            liaRegulars = m_sortedList.GetValueList().Cast<ILiabilityRegular>().ToList();
                            if (liaGuid != Guid.Empty)
                            {
                                regLia.ChangeRecordId(liaGuid);
                            }
                        }
                        else
                        {
                            regLia.IsOnDeathRow = false;
                            if (liaRegulars[currentPosition] != regLia)
                            {
                                this.MoveRegularRecordTo(currentPosition, regLia);
                                liaRegulars = m_sortedList.GetValueList().Cast<ILiabilityRegular>().ToList();
                            }
                        }

                        currentPosition++;
                        regLia.BindFromObjectModel(regularInput);
                    }

                    this.Flush();
                }
            }
        }
	}
}