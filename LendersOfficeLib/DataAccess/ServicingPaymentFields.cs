﻿using System;
using System.Xml.Serialization;
using System.Globalization;

namespace DataAccess
{
    [XmlType]
    public sealed class CServicingPaymentFields
    {
        public CServicingPaymentFields()
        {
            this.Id = Guid.NewGuid();
        }

        [XmlElement]
        public Guid? Id { get; set; }
        [XmlElement]
        public string ServicingTransactionT { get; set; }
        [XmlElement]
        public string DueD_rep { get; set; }
        [XmlElement]
        public string DueAmt_rep { get; set; }
        /// <summary>
        /// In the UI as the Payment Credited Date.
        /// </summary>
        [XmlElement]
        public string PaymentD_rep { get; set; }
        [XmlElement]
        public string PaymentAmt_rep { get; set; }
        [XmlElement]
        public string Principal_rep { get; set; }
        [XmlElement]
        public string Interest_rep { get; set; }
        [XmlElement]
        public string Escrow_rep { get; set; }
        [XmlElement]
        public string Other_rep { get; set; }
        [XmlElement]
        public string LateFee_rep { get; set; }
        [XmlElement]
        public string Notes { get; set; }
        [XmlElement]
        public int LastColumnUpdated { get; set; }
        [XmlElement]
        public int RowNum { get; set; }
        [XmlElement]
        public string PaymentReceivedDOverride_rep { get; set; }
        [XmlElement]
        public bool PaymentReceivedDLckd { get; set; }
        
        string m_payee = ""; // OPM 108162
        [XmlElement]
        public string Payee
        {
            get { return m_payee; }
            set { m_payee = value; }
        }

        [XmlElement]
        public Guid PaymentStatementDocumentId { get; set; }

        [XmlElement]
        public string TransactionD_rep { get; set; }

        public DateTime TransactionD
        {
            get
            {
                if(string.IsNullOrEmpty(TransactionD_rep))
                {
                    return DateTime.MinValue;
                }
                else
                {
                    try
                    {
                        return DateTime.Parse(TransactionD_rep);
                    }
                    catch (FormatException)
                    {
                        CBaseException exc = new CBaseException("Transaction Date is not a valid date.", "Transaction Received Date is not a valid date.");
                        exc.IsEmailDeveloper = false;
                        throw exc;
                    }
                }
            }
        }

        public string PaymentReceivedD_rep
        {
            get
            {
                if (PaymentReceivedDLckd)
                {
                    return PaymentReceivedDOverride_rep;
                }
                else
                {
                    return PaymentD_rep;
                }
            }
        }

        public DateTime PaymentReceivedD
        {
            get
            {
                if(string.IsNullOrEmpty(PaymentReceivedD_rep))
                {
                    return DateTime.MinValue;
                }
                else
                {
                    try
                    {
                        return DateTime.Parse(PaymentReceivedD_rep);
                    }
                    catch (FormatException)
                    {
                        CBaseException exc = new CBaseException("Payment Received Date is not a valid date.", "Payment Received Date is not a valid date.");
                        exc.IsEmailDeveloper = false;
                        throw exc;
                    }
                }
            }
        }

        public DateTime DueD
        {
            get 
            {
                if (string.IsNullOrEmpty(DueD_rep))
                {
                    return DateTime.MinValue;
                }
                else
                {
                    try
                    {
                        return DateTime.Parse(DueD_rep);
                    }
                    catch (FormatException)
                    {
                        CBaseException exc = new CBaseException("Due Date is not a valid date.", "Due Date is not a valid date.");
                        exc.IsEmailDeveloper = false;
                        throw exc;
                    }
                }
            }
        }

        public decimal DueAmt
        {
            get
            {
                return SafeParseMoney("Due Amount", DueAmt_rep);
            }
        }

        /// <summary>
        /// In the UI as the Payment Credited Date.
        /// </summary>
        public DateTime PaymentD
        {
            get
            {
                if (string.IsNullOrEmpty(PaymentD_rep))
                {
                    return DateTime.MinValue;
                }
                else
                {
                    try
                    {
                        return DateTime.Parse(PaymentD_rep);
                    }
                    catch (FormatException)
                    {
                        CBaseException exc = new CBaseException("Payment Date is not a valid date.", "Payment is not a valid date.");

                        exc.IsEmailDeveloper = false;
                        throw exc;
                    }
                }
            }
        }

        public decimal PaymentAmt
        {
            get
            {
                return SafeParseMoney("Payment Amount", PaymentAmt_rep);
            }
        }

        public decimal Principal
        {
            get
            {
                return SafeParseMoney("Principal", Principal_rep);
            }
        }

        public decimal Interest
        {
            get
            {
                return SafeParseMoney("Interest", Interest_rep);
            }
        }

        public decimal Escrow
        {
            get
            {
                return SafeParseMoney("Escrow", Escrow_rep);
            }
        }

        public decimal Other
        {
            get
            {
                return SafeParseMoney("'Other'", Other_rep);
            }
        }

        public decimal LateFee
        {
            get
            {
                return SafeParseMoney("Late Fee", LateFee_rep);
            }
        }
        private decimal SafeParseMoney(string fieldName, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return 0;
            }

            try
            {
                return ToMoney(value);
            }
            catch (FormatException)
            {
                CBaseException exc = new CBaseException(fieldName + " is not a valid decimal.", fieldName + " is not a valid decimal.");

                exc.IsEmailDeveloper = false;
                throw exc;
            }
        }
        // Taken from LosConvert - TODO find a way to not have to copy this
        private decimal ToMoney(string str)
        {
            if (null == str || str.Length == 0)
                return 0;
            return Decimal.Parse(str, NumberStyles.Currency);
        }
        
    }
}
