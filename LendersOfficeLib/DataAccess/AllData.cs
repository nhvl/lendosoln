using System;
using System.Collections;
using System.Data;
using System.Text;

namespace DataAccess
{

	/// <summary>
	/// Data object for all data in lendingqb.
	/// </summary>
	public class CAllData : CPageData
	{
		private static CSelectStatementProvider s_selectProvider;
		static CAllData()
		{
			s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputNothing_NeedAllFields, null );
		}

		public CAllData( Guid fileId ) : base( fileId , "AllData" , s_selectProvider )
		{
		}

	}
}
