﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using LendersOffice.UI;

namespace DataAccess.Trust
{
    [XmlRoot] //105887: Implement IEquatable on all of these classes
    public class TrustCollection : IEquatable<TrustCollection>
    {
        public List<Trustee> Trustees = new List<Trustee>();
        public List<Trustor> Trustors = new List<Trustor>();

        public class Trustee : IEquatable<Trustee>
        {
            [XmlAttribute]
            public string Name;

            [XmlAttribute]
            public string Phone;
            
            public Address Address = new Address();

            [XmlAttribute]
            public bool CanSignCertOfTrust;

            [XmlAttribute]
            public bool CanSignLoanDocs;

            public string SignatureVerbiage;

            public bool IsEmpty()
            {
                return string.IsNullOrEmpty(Name) &&
                       string.IsNullOrEmpty(Phone) &&
                       string.IsNullOrEmpty(SignatureVerbiage) &&
                       CanSignCertOfTrust == default(bool) &&
                       CanSignLoanDocs == default(bool) &&
                       Address.IsEmpty();
            }

            #region IEquatable<Trustee> Members
            public override bool Equals(object obj)
            {
                return this.Equals(obj as Trustee);
            }

            public bool Equals(Trustee other)
            {
                if (other == null) return false;

                return this.Name == other.Name &&
                       this.Phone == other.Phone &&
                       this.Address == other.Address &&
                       this.CanSignCertOfTrust == other.CanSignCertOfTrust &&
                       this.CanSignLoanDocs == other.CanSignLoanDocs &&
                       this.SignatureVerbiage == other.SignatureVerbiage;
            }

            public override int GetHashCode()
            {
                unchecked // Overflow is fine, just wrap
                {
                    int hash = 17;
                    int filler = 2; //Just a prime number that will fill in when there's a null - 29 + 2 == 31, another prime
                    hash = hash * 29 + (Name == null ? filler : Name.GetHashCode());
                    hash = hash * 29 + (Phone == null ? filler : Phone.GetHashCode());
                    hash = hash * 29 + (Address == null ? filler : Address.GetHashCode());
                    hash = hash * 29 + CanSignCertOfTrust.GetHashCode();
                    hash = hash * 29 + CanSignLoanDocs.GetHashCode();
                    hash = hash * 29 + (SignatureVerbiage == null ? filler : SignatureVerbiage.GetHashCode());

                    return hash;
                }
            }

            #endregion
        }

        public class Trustor : IEquatable<Trustor>
        {
            [XmlAttribute]
            public string Name;

            public bool IsEmpty()
            {
                return string.IsNullOrEmpty(Name);
            }

            #region IEquatable<Trustor> Members
            public override bool Equals(object obj)
            {
                return this.Equals(obj as Trustor);
            }

            public bool Equals(Trustor other)
            {
                if (other == null) return false;
                return this.Name == other.Name;
            }

            public override int GetHashCode()
            {
                return Name == null ? 0 : Name.GetHashCode();
            }

            #endregion
        }

        public class Address : IEquatable<Address>
        {
            [XmlAttribute]
            public string StreetAddress { get; set; }

            [XmlAttribute]
            public string City { get; set; }

            [LqbInputModelAttribute(name: "Zip")]
            [XmlAttribute]
            public string PostalCode { get; set; }

            [XmlAttribute]
            public string State { get; set; }

            public bool IsEmpty()
            {
                return string.IsNullOrEmpty(StreetAddress) &&
                       string.IsNullOrEmpty(City) &&
                       string.IsNullOrEmpty(PostalCode) &&
                       string.IsNullOrEmpty(State);

            }

            #region IEquatable<Address> Members

            public override bool Equals(object obj)
            {
                return this.Equals(obj as Address);
            }

            public bool Equals(Address other)
            {
                if (other == null) return false;

                return this.StreetAddress == other.StreetAddress &&
                       this.City == other.City &&
                       this.PostalCode == other.PostalCode &&
                       this.State == other.State;
            }

            public override int GetHashCode()
            {
                unchecked // Overflow is fine, just wrap
                {
                    int hash = 17;
                    int filler = 2; //Just a prime number that will fill in when there's a null; 29 + 2 == 31, another prime
                    hash = hash * 29 + (StreetAddress == null ? filler : StreetAddress.GetHashCode());
                    hash = hash * 29 + (City == null ? filler : City.GetHashCode());
                    hash = hash * 29 + (PostalCode == null ? filler : PostalCode.GetHashCode());
                    hash = hash * 29 + (State == null ? filler : State.GetHashCode());
                    return hash;
                }
            }

            #endregion
        }

        public TrustCollection()
        {
        }

        public string Serialize()
        {
            XmlSerializer serializer = new XmlSerializer(this.GetType());
            string ret;

            using (var writer = new StringWriter())
            {
                serializer.Serialize(writer, this);
                ret = writer.ToString();
            }

            return ret;
        }

        public static TrustCollection Deserialize(string input)
        {
            XmlSerializer deserializer = new XmlSerializer(typeof(TrustCollection));
            using (var sr = new StringReader(input))
            {
                return (TrustCollection)deserializer.Deserialize(sr);
            }
        }

        #region IEquatable<TrustCollection> Members

        public override bool Equals(object obj)
        {
            return this.Equals(obj as TrustCollection);
        }

        public bool Equals(TrustCollection other)
        {
            if (other == null) return false;

            if (this.Trustees.SequenceEqual(other.Trustees) &&
               this.Trustors.SequenceEqual(other.Trustors)) return true;

            //105887: special case: occasionally we'll have a trust collection with 1 default trustor & trustee,
            //which should be equivalent to blank (0 trustor/trustees).

            if (this.IsEmpty() && other.IsEmpty()) return true;

            return false;
        }

        public bool IsEmpty()
        {
            if (this.Trustees.Count == 0 && this.Trustors.Count == 0) return true;
            if (this.Trustees.Count == 1 && this.Trustors.Count == 1)
            {
                return this.Trustees.First().IsEmpty() && this.Trustors.First().IsEmpty();
            }
            
            return false;
        }

        public override int GetHashCode()
        {
            //For compatibility with .Equals; I'm pretty sure null hashes into 0 so try not to collide with that.
            if (this.IsEmpty()) return 1;

            unchecked // Overflow is fine, just wrap
            {
                int hash = 17;
                int filler = 2; //Just a prime number that will fill in when there's a null; 29 + 2 == 31, another prime
                hash = hash * 29 + (Trustees == null ? filler : Trustees.GetHashCode());
                hash = hash * 29 + (Trustors == null ? filler : Trustors.GetHashCode());
                return hash;
            }
        }

        #endregion

    }
}