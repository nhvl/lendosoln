﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Globalization;

namespace DataAccess
{
    [XmlType]
    public sealed class CPurchaseAdviceFeesFields
    {
        [XmlElement]
        public string FeeDesc { get; set; }
        [XmlElement]
        public string FeeAmt_rep { get; set; }
        [XmlElement]
        public int RowNum { get; set; }

        public decimal FeeAmt
        {
            get
            {
                if (string.IsNullOrEmpty(FeeAmt_rep))
                {
                    return 0;
                }
                else
                {
                    try
                    {
                        return ToMoney(FeeAmt_rep);
                    }
                    catch (FormatException)
                    {
                        CBaseException exc = new CBaseException("Fee Amount [" + FeeAmt_rep + "] is not a valid decimal", "Fee Amount [" + FeeAmt_rep + "] is not a valid decimal");
                        exc.IsEmailDeveloper = false;
                        throw exc;
                    }
                }
            }
        }

        // Taken from LosConvert - TODO find a way to not have to copy this
        private decimal ToMoney(string str)
        {
            if (null == str || str.Length == 0)
                return 0;
            return Decimal.Parse(str, NumberStyles.Currency);
        }
    }
}
