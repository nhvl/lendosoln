/// Author: David Dao

using System;
using System.Data;

namespace DataAccess
{
    public class CAgentFieldsForImport : CAgentFields
    {
        protected override void SetString(string fieldName, string value) 
        {
            if (IsValid) 
            {
                if (null == value)
                    value = "";

                string oldValue = CurrentRow[fieldName].ToString();
                string uniqueFieldName = GetUniqueFieldName(fieldName);

                if (this.Parent.IsRequireFieldSetForXmlRecord(uniqueFieldName, oldValue, value)) 
                {
                    CurrentRow[fieldName] = value;
                }

            }
        }
        public CAgentFieldsForImport( CPageBase parent, DataSet ds, int iRow ) : base( parent, ds, iRow)
        {
        }

        public CAgentFieldsForImport(CPageBase parent, DataSet ds, Guid id) : base(parent, ds, id) 
        {
        }

    }
}
