﻿namespace DataAccess
{
    using System.Runtime.Serialization;

    /// <summary>
    /// VOE data for employment records.
    /// </summary>
    [DataContract]
    public class EmploymentRecordVOEData
    {
        /// <summary>
        /// Gets or sets the employee id for VOE.
        /// </summary>
        /// <value>The employee id for VOE.</value>
        [DataMember(Name = "i")]
        public string EmployeeIdVoe { get; set; }

        /// <summary>
        /// Gets or sets the employer code for VOE.
        /// </summary>
        /// <value>The employer code for VOE.</value>
        [DataMember(Name = "c")]
        public string EmployerCodeVoe { get; set; }
    }
}
