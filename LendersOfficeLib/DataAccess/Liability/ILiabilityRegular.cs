﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Interface that represents a generic liability.
    /// </summary>
    public interface ILiabilityRegular : ILiability
    {
        /// <summary>
        /// Gets or sets the account name -- the name of the debtor.
        /// </summary>
        /// <value>The name of the debtor.</value>
        string AccNm { get; set; }

        /// <summary>
        /// Gets or sets the account number as a sensitive value.
        /// </summary>
        /// <value>The account number as a sensitive value.</value>
        Sensitive<string> AccNum { get; set; }

        /// <summary>
        /// Gets or sets the name of a person to whom attention is called.
        /// </summary>
        /// <value>The name of a person to whom attention is called.</value>
        string Attention { get; set; }

        /// <summary>
        /// Gets or sets the year and make of an automobile when this liability is an auto loan.
        /// </summary>
        /// <value>The year and make of an automobile when this liability is an auto loan.</value>
        string AutoYearMake { get; set; }

        /// <summary>
        /// Gets or sets the balance for the liability.
        /// </summary>
        /// <value>The balance for the liability.</value>
        decimal Bal { get; set; }

        /// <summary>
        /// Gets or sets a string representation of the balance for the liability.
        /// </summary>
        /// <value>A string representation of the balance for the liability.</value>
        string Bal_rep { get; set; }

        /// <summary>
        /// Gets or sets the address of the company that is the listed creditor for this liability.
        /// </summary>
        /// <value>The address of the company that is the listed creditor for this liability.</value>
        string ComAddr { get; set; }

        /// <summary>
        /// Gets or sets the city of the company that is the listed creditor for this liability.
        /// </summary>
        /// <value>The city of the company that is the listed creditor for this liability.</value>
        string ComCity { get; set; }

        /// <summary>
        /// Gets or sets the fax number of the company that is the listed creditor for this liability.
        /// </summary>
        /// <value>The fax number of the company that is the listed creditor for this liability.</value>
        string ComFax { get; set; }

        /// <summary>
        /// Gets or sets the name of the company that is the listed creditor for this liability.
        /// </summary>
        /// <value>The name of the company that is the listed creditor for this liability.</value>
        string ComNm { get; set; }

        /// <summary>
        /// Gets or sets the phone number of the company that is the listed creditor for this liability.
        /// </summary>
        /// <value>The phone number of the company that is the listed creditor for this liability.</value>
        string ComPhone { get; set; }

        /// <summary>
        /// Gets or sets the state of the company that is the listed creditor for this liability.
        /// </summary>
        /// <value>The state of the company that is the listed creditor for this liability.</value>
        string ComState { get; set; }

        /// <summary>
        /// Gets or sets the zipcode of the company that is the listed creditor for this liability.
        /// </summary>
        /// <value>The zipcode of the company that is the listed creditor for this liability.</value>
        string ComZip { get; set; }

        /// <summary>
        /// Gets or sets the debt type.
        /// </summary>
        /// <value>The debt type.</value>
        new E_DebtRegularT DebtT { get; set; }

        /// <summary>
        /// Gets the string representation of the debt type.
        /// </summary>
        /// <value>The string representation of the debt type.</value>
        string DebtT_rep { get; }

        /// <summary>
        /// Gets or sets the description of the liability.
        /// </summary>
        /// <value>The description of the liability.</value>
        string Desc { get; set; }

        /// <summary>
        /// Gets or sets the string representation for the amount due.
        /// </summary>
        /// <value>The string representation for the amount due.</value>
        string Due_rep { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this liability should be excluded from the underwriting.
        /// </summary>
        /// <value>A value indicating whether this liability should be excluded from the underwriting.</value>
        bool ExcFromUnderwriting { get; set; }

        /// <summary>
        /// Gets or sets the fully indexed PITI payment.
        /// </summary>
        /// <value>The fully indexed PITI payment.</value>
        decimal FullyIndexedPITIPayment { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this liability was included in a bankruptcy.
        /// </summary>
        /// <value>A value indicating whether this liability was included in a bankruptcy.</value>
        bool IncInBankruptcy { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this liability was included in a foreclosure.
        /// </summary>
        /// <value>A value indicating whether this liability was included in a foreclosure.</value>
        bool IncInForeclosure { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this liability was included in a repossession.
        /// </summary>
        /// <value>A value indicating whether this liability was included in a repossession.</value>
        bool IncInReposession { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this liability represents an auto loan.
        /// </summary>
        /// <value>A value indicating whether this liability represents an auto loan.</value>
        bool IsForAuto { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this liability is an FHA insured mortgage.
        /// </summary>
        /// <value>A value indicating whether this liability is an FHA insured mortgage.</value>
        bool IsMortFHAInsured { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this liability is a piggy-back loan.
        /// </summary>
        /// <value>A value indicating whether this liability is a piggy-back loan.</value>
        bool IsPiggyBack { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether there is an attachment referenced with further information about this liability.
        /// </summary>
        /// <value>A value indicating whether there is an attachment referenced with further information about this liability.</value>
        bool IsSeeAttachment { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this liability is the first mortgage for the subject property.
        /// </summary>
        /// <value>A value indicating whether this liability is the first mortgage for the subject property.</value>
        bool IsSubjectProperty1stMortgage { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this liability is a mortgage for the subject property.
        /// </summary>
        /// <value>A value indicating whether this liability is a mortgage for the subject property.</value>
        bool IsSubjectPropertyMortgage { get; set; }

        /// <summary>
        /// Gets or sets the string representation of the 30 day late indicator.
        /// </summary>
        /// <value>The string representation of the 30 day late indicator.</value>
        string Late30_rep { get; set; }

        /// <summary>
        /// Gets or sets the string representation of the 60 day late indicator.
        /// </summary>
        /// <value>The string representation of the 60 day late indicator.</value>
        string Late60_rep { get; set; }

        /// <summary>
        /// Gets or sets the string representation of the 90+ day late indicator.
        /// </summary>
        /// <value>The string representation of the 90+ day late indicator.</value>
        string Late90Plus_rep { get; set; }

        /// <summary>
        /// Gets or sets the matched real estate record identifier.
        /// </summary>
        /// <value>The matched real estate record identifier.</value>
        Guid MatchedReRecordId { get; set; }

        /// <summary>
        /// Gets or sets the original debt amount.
        /// </summary>
        /// <value>The original debt amount.</value>
        decimal OrigDebtAmt { get; set; }

        /// <summary>
        /// Gets or sets the string representation of the original debt amount.
        /// </summary>
        /// <value>The string representation of the original debt amount.</value>
        string OrigDebtAmt_rep { get; set; }

        /// <summary>
        /// Gets or sets the string representation of the original term.
        /// </summary>
        /// <value>The string representation of the original term.</value>
        string OrigTerm_rep { get; set; }

        /// <summary>
        /// Gets or sets the amount required to payoff the debt.
        /// </summary>
        /// <value>The amount required to payoff the debt.</value>
        decimal PayoffAmt { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether there is an edit lock on the amount required to payoff the debt.
        /// </summary>
        /// <value>A value indicating whether there is an edit lock on the amount required to payoff the debt.</value>
        bool PayoffAmtLckd { get; set; }

        /// <summary>
        /// Gets or sets the string representation of the amount required to payoff the debt.
        /// </summary>
        /// <value>The string representation of the amount required to payoff the debt.</value>
        string PayoffAmt_rep { get; set; }

        /// <summary>
        /// Gets or sets an indicator of when the debt is expected to be paid off relative to the loan's closing date.
        /// </summary>
        /// <value>An indicator of when the debt is expected to be paid off relative to the loan's closing date.</value>
        E_Timing PayoffTiming { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether there is an edit lock on the payoff timing indicator.
        /// </summary>
        /// <value>A value indicating whether there is an edit lock on the payoff timing indicator.</value>
        bool PayoffTimingLckd { get; set; }

        /// <summary>
        /// Gets or sets the date when the liability was prepared.
        /// </summary>
        /// <value>The date when the liability was prepared.</value>
        CDateTime PrepD { get; set; }

        /// <summary>
        /// Gets or sets the status for the debt's reconciliation.
        /// </summary>
        /// <value>The status for the debt's reconciliation.</value>
        E_LiabilityReconcileStatusT ReconcileStatusT { get; set; }

        /// <summary>
        /// Gets or sets rate.
        /// </summary>
        decimal R { get; set; }

        /// <summary>
        /// Gets or sets the string representation for the R value.
        /// </summary>
        /// <value>The string representation for the R value.</value>
        string R_rep { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this liability should be used in the DTI calculation.
        /// </summary>
        /// <value>A value indicating whether this liability should be used in the DTI calculation.</value>
        bool UsedInRatio { get; set; }

        /// <summary>
        /// Gets or sets the verification date for this liability.
        /// </summary>
        /// <value>The verification date for this liability.</value>
        CDateTime VerifExpD { get; set; }

        /// <summary>
        /// Gets or sets the string representation of the verification date for this liability.
        /// </summary>
        /// <value>The string representation of the verification date for this liability.</value>
        string VerifExpD_rep { get; set; }

        /// <summary>
        /// Gets a value indicating whether there is a signature associated with the debt verification.
        /// </summary>
        /// <value>A value indicating whether there is a signature associated with the debt verification.</value>
        bool VerifHasSignature { get; }

        /// <summary>
        /// Gets or sets the date when the debt verification was received.
        /// </summary>
        /// <value>The date when the debt verification was received.</value>
        CDateTime VerifRecvD { get; set; }

        /// <summary>
        /// Gets or sets the string representation for the date when the debt verification was received.
        /// </summary>
        /// <value>The string representation for the date when the debt verification was received.</value>
        string VerifRecvD_rep { get; set; }

        /// <summary>
        /// Gets or sets the date when the debt verification was re-ordered.
        /// </summary>
        /// <value>The date when the debt verification was re-ordered.</value>
        CDateTime VerifReorderedD { get; set; }

        /// <summary>
        /// Gets or sets the string representation for the date when the debt verification was re-ordered.
        /// </summary>
        /// <value>The string representation for the date when the debt verification was re-ordered.</value>
        string VerifReorderedD_rep { get; set; }

        /// <summary>
        /// Gets or sets the date when the debt verification request was sent.
        /// </summary>
        /// <value>The date when the debt verification request was sent.</value>
        CDateTime VerifSentD { get; set; }

        /// <summary>
        /// Gets or sets the string representation for the date when the debt verification request was sent.
        /// </summary>
        /// <value>The string representation for the date when the debt verification request was sent.</value>
        string VerifSentD_rep { get; set; }

        /// <summary>
        /// Gets the identifier for the image that holds the signature associated with the debt verification.
        /// </summary>
        /// <value>The identifier for the image that holds the signature associated with the debt verification.</value>
        string VerifSignatureImgId { get; }

        /// <summary>
        /// Gets the identifier for the employee who issued the debt verification.
        /// </summary>
        /// <value>The identifier for the employee who issued the debt verification.</value>
        Guid VerifSigningEmployeeId { get; }

        /// <summary>
        /// Attach a signature image to this liability.
        /// </summary>
        /// <param name="employeeId">The identifier for the employee who's signature is contained in the image.</param>
        /// <param name="imgId">The identifier for the image.</param>
        void ApplySignature(Guid employeeId, string imgId);

        /// <summary>
        /// Remove the signature image from this liability.
        /// </summary>
        void ClearSignature();

        /// <summary>
        /// Create an audit item for a field change.
        /// </summary>
        /// <param name="userName">The name of the user making the field change.</param>
        /// <param name="loginName">The login of the user account that made the field change.</param>
        /// <param name="field">The name of the field that has changed.</param>
        /// <param name="value">The new value for the field that has changed.</param>
        void FieldUpdateAudit(string userName, string loginName, string field, string value);

        /// <summary>
        /// Perform a matching operation on the input account number and creditor name.
        /// </summary>
        /// <param name="accNumber">The account number for the liability that is checked for a match.</param>
        /// <param name="creditorName">The creditor name for the liability that is checked for a match.</param>
        /// <returns>True if the tradelines match, false otherwise.</returns>
        bool IsSameTradeline(string accNumber, string creditorName);

        /// <summary>
        /// Create an audit record that indicates that a liability record is created via user input.
        /// </summary>
        /// <param name="userName">The name of the user making creating the liability record.</param>
        /// <param name="loginName">The login of the user account that created the liability record.</param>
        void LiabilityCreationAudit(string userName, string loginName);

        /// <summary>
        /// Gets a calculated count of the remaining months to pay off a liability, automatically calculating a value for revolving "(R)" and liabilities excluded for underwriting.
        /// This value is intended to be appropriate for the Loan Product Advisor export, but may be appropriate for other uses.
        /// </summary>
        /// <returns>The liability remaining months for LPA.</returns>
        string GetRemainingMonthsForLpa();

        /// <summary>
        /// Assign the record identifier for this liability.
        /// </summary>
        /// <param name="s">The record identifier for this liability.</param>
        void SetRecordId(string s);
    }
}