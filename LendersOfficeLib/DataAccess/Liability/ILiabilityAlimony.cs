﻿namespace DataAccess
{
    /// <summary>
    /// Interface for a liability that represents an alimony payment.
    /// </summary>
    public interface ILiabilityAlimony : ILiabilitySpecial
    {
        /// <summary>
        /// Gets or sets the name of the person to whom is paid the alimony.
        /// </summary>
        /// <value>The name of the person to whom is paid the alimony.</value>
        string OwedTo { get; set; }
    }
}