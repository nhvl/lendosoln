﻿namespace DataAccess
{
    /// <summary>
    /// Interface for a liability that represents a job expense.
    /// </summary>
    public interface ILiabilityJobExpense : ILiabilitySpecial
    {
        /// <summary>
        /// Gets or sets the type of job expense.
        /// </summary>
        /// <value>The type of job expense.</value>
        E_DebtJobExpenseT DebtJobExpenseT { get; set; }

        /// <summary>
        /// Gets or sets a string description of the job expense.
        /// </summary>
        /// <value>A string description of the job expense.</value>
        string ExpenseDesc { get; set; }
    }
}