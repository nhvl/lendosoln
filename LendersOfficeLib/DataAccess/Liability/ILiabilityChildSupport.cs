﻿namespace DataAccess
{
    /// <summary>
    /// Interface for a liability that represents a child support payment.
    /// </summary>
    public interface ILiabilityChildSupport : ILiabilitySpecial
    {
        /// <summary>
        /// Gets or sets the name of the person to whom is paid the child support.
        /// </summary>
        /// <value>The name of the person to whom is paid the child support.</value>
        string OwedTo { get; set; }
    }
}