﻿namespace DataAccess
{
    /// <summary>
    /// Interface for special liabilities.
    /// </summary>
    public interface ILiabilitySpecial : ILiability
    {
        /// <summary>
        /// Gets the debt type.
        /// </summary>
        /// <value>The debt type.</value>
        new E_DebtSpecialT DebtT { get; }
    }
}