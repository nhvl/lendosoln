﻿namespace DataAccess
{
    using System;
    using LendersOffice.UI;

    /// <summary>
    /// Interface for any liability.
    /// </summary>
    public interface ILiability : ICollectionItemBase2
    {
        /// <summary>
        /// Gets the type of debt for the liability.
        /// </summary>
        /// <value>The type of debt for the liability.</value>
        E_DebtT DebtT { get; }

        /// <summary>
        /// Gets or sets a value indicating whether h4h is retirement.
        /// </summary>
        /// <value>A value indicting whether h4h is retirement.</value>
        bool H4HIsRetirement { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this liability should not be used in calculating DTI.
        /// </summary>
        /// <value>A value indicating whether this liability should not be used in calculating DTI.</value>
        bool NotUsedInRatio { get; set; }

        /// <summary>
        /// Gets or sets the owner type for this liability.
        /// </summary>
        /// <value>The owner type for this liability.</value>
        E_LiaOwnerT OwnerT { get; set; }

        /// <summary>
        /// Gets the string representation of the owner type for this liability.
        /// </summary>
        /// <value>The string representation of the owner type for this liability.</value>
        string OwnerT_rep { get; }

        /// <summary>
        /// Gets or sets the audit trail XML.
        /// </summary>
        /// <value>The audit trail XML.</value>
        string PmlAuditTrailXmlContent { get; set; }

        /// <summary>
        /// Gets or sets the payment value.
        /// </summary>
        /// <value>The payment value.</value>
        decimal Pmt { get; set; }

        /// <summary>
        /// Gets the string representation for remaining payment for form 1003.
        /// </summary>
        /// <value>The string representation for remaining payment for form 1003.</value>
        string PmtRemainMons_rep1003 { get; }

        /// <summary>
        /// Gets or sets the string representation of the payment value.
        /// </summary>
        /// <value>The string representation of the payment value.</value>
        string Pmt_rep { get; set; }

        /// <summary>
        /// Gets or sets the string representation for the payment amount for Point.
        /// </summary>
        /// <value>The string representation for the payment amount for Point.</value>
        string Pmt_repPoint { get; set; }

        /// <summary>
        /// Gets the string representation for the number of payments that remain.
        /// </summary>
        /// <value>The string representation for the number of payments that remain.</value>
        string RemainMons_raw { get; }

        /// <summary>
        /// Gets or sets another string representation for the number of payments that remain.
        /// </summary>
        /// <value>Another string representation for the number of payments that remain.</value>
        string RemainMons_rep { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the liability is expected to be paid off.
        /// </summary>
        /// <value>A value indicating whether the liability is expected to be paid off.</value>
        bool WillBePdOff { get; set; }

        /// <summary>
        /// Take the input object model and parse it into the liability.
        /// </summary>
        /// <param name="objInputFieldModel">The input object model.</param>
        void BindFromObjectModel(SimpleObjectInputFieldModel objInputFieldModel);

        /// <summary>
        /// Transfer the data from this liability to an input model.
        /// </summary>
        /// <returns>The input model that holds the data from this liability.</returns>
        SimpleObjectInputFieldModel ToInputFieldModel();
    }
}