using System;
using System.Collections;
using System.Data;

namespace DataAccess
{

	public class CLOSubmitFirstLoanData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CLOSubmitFirstLoanData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
            DataAccessUtils.AddCertificateFields(list);
            list.Add("sfTransformLoPropertyTypeToPml");
            list.Add("aBNm");
            list.Add("sEmployeeLenderAccExec");
            list.Add("sfSubmitFromPML");
            list.Add("aBEquifaxScore");  
            list.Add("aBExperianScore");  
            list.Add("aBNm");
            list.Add("aBTransUnionScore");  
            list.Add("aCEquifaxScore"); 
            list.Add("aCExperianScore");  
            list.Add("aCNm");
            list.Add("aCSsn");
            list.Add("aCTransUnionScore");  
            list.Add("sCltvR");  
            list.Add("sCreditScoreType1");
            list.Add("sCreditScoreType2");
            list.Add("sCreditScoreType3");
            list.Add("sFinMethT");
            list.Add("sIsIOnly");
            list.Add("sIsSelfEmployed");
            list.Add("sLAmtCalc");
            list.Add("sLPurposeT");  
            list.Add("sLiaMonLTot");  
            list.Add("sLienPosT");
            list.Add("sLtvR");
            list.Add("sOccT");
            list.Add("sProdCashoutAmt");
            list.Add("sProdCondoStories");
            list.Add("sProdDocT");
            list.Add("sProdImpound");
            list.Add("sProdPpmtPenaltyMon");
            list.Add("sProdRLckdDays");
            list.Add("sProdSpT");
            list.Add("sSpState");
            list.Add("sStatusT");
            list.Add("sUnitsNum");
            list.Add("sQualIR");
            list.Add("sNoteIR");
            list.Add("sTerm");
            list.Add("sLT");
            list.Add("sPreparerXmlContent");
            list.Add("sfGetAgentOfRole");
            list.Add("sEmployeeLenderAccExec");
            list.Add(nameof(CPageBase.SwapFor2ndLienQualifying));
            list.Add(nameof(CPageBase.MakeThisInto2ndLienForQualifying));
            list.Add(nameof(CPageBase.sClosingCostSetJsonContent));
            list.Add(nameof(CPageBase.sFeeServiceApplicationHistoryXmlContent));
            list.Add("sProdIsCreditScoresAltered");
            list.Add("sLNm");
            list.Add("sProOHExp");
            list.Add("sProRealETx");
            list.Add("sTransmOMonPmt");
            list.Add("sPmlCertXmlContent");
            list.Add("sfApplyLoanProductTemplate");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CLOSubmitFirstLoanData(Guid fileId) : base(fileId, "CLOSubmitFirstLoanData", s_selectProvider)
		{
		}
	}
    public class CVerifyFirstLoanProgram : CPageData 
    {
        private static CSelectStatementProvider s_selectProvider;
	    
        static CVerifyFirstLoanProgram()
        {
            StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("sLNm");
            list.Add("sLOrigFPcSubmitted");
            list.Add("sLpTemplateId");
            list.Add("sLpTemplateNm");
            list.Add("sLoanProductIdentifier");
            list.Add("sNoteIRSubmitted");
            list.Add("sProOFinPmtPe");
            list.Add("sProThisMPmt");
            list.Add("sQualBottomR");
            list.Add("sQualTopR");
            list.Add("sSubFinIR");
            list.Add("sSubFinMb");
            list.Add("sfRunLpe");
            list.Add("sRAdjMarginR");
            list.Add("sLpIsArmMarginDisplayed");
            #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
        public CVerifyFirstLoanProgram(Guid fileId) : base(fileId, "CVerifyFirstLoanProgram", s_selectProvider)
        {
        }
    }
}
