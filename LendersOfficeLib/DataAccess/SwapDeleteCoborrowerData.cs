using System;
using System.Collections;
using System.Data;

namespace DataAccess
{

	public class CSwapCoborrowerData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CSwapCoborrowerData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("afSwapMarriedBorAndCobor");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CSwapCoborrowerData(Guid fileId) : base(fileId, "CSwapCoborrowerData", s_selectProvider)
		{
		}
	}

    public class CDeleteCoborrowerData : CPageData
    {
        private static CSelectStatementProvider s_selectProvider;
	    
        static CDeleteCoborrowerData()
        {
            StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("afDelMarriedCobor");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
        public CDeleteCoborrowerData(Guid fileId) : base(fileId, "CDeleteCoborrowerData", s_selectProvider)
        {
        }
    }
}
