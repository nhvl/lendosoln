using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Data.Common;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.IO;

namespace DataAccess
{
	public class CVorFields : CXmlRecordBaseOld, IVerificationOfRent
	{
        private CAppBase m_parent;

        protected Action<DataSet> m_onflush;

        public void SetFlushFunction(Action<DataSet> onFlush)
        {
            m_onflush = onFlush;
        }

		// Pass -1 for rowApp to add new row.
		public CVorFields( CAppBase parent, DataSet ds, int iRow )
			: base( parent, ds, iRow, "VorXmlContent" )
		{
			m_parent = parent;
		}
		public CVorFields( CAppBase parent, DataSet ds, Guid id) 
			: base (parent, ds, id, "VorXmlContent") 
		{
            // If id is one of VOR then set VerifT = 
            if (id == new Guid("11111111-1111-1111-1111-111111111111") ||
                id == new Guid("22222222-2222-2222-2222-222222222222") ||
                id == new Guid("33333333-3333-3333-3333-333333333333") ||
                id == new Guid("44444444-4444-4444-4444-444444444444") ||
                id == new Guid("55555555-5555-5555-5555-555555555555") ||
                id == new Guid("66666666-6666-6666-6666-666666666666")) 
            {
                // These special GUID are for Verification of Rental
                VerifT = E_VorT.Rental;
            }

			m_parent = parent;
		}

        // DO NOT USE!  CREATED JUST TO ENABLE SOME QUICK UNIT TESTING OF THE SHIM
        public CVorFields(DataSet data, LosConvert converter)
            : base(data, converter)
        {

        }

        override public void Update()
		{
			base.Update();

            m_parent.aVorCollection.Update();

            if (m_onflush != null)
            {
                m_onflush(m_ds);
            }
		}

		public string LandlordCreditorName
		{
			get { return GetDescString( "LandlordCreditorName" ); }
			set { SetDescString( "LandlordCreditorName", value ); }
		}

		public string Description
		{
			get { return GetDescString("Description"); }
			set { SetDescString( "Description", value ); }
		}

		
		public string AddressFor
		{
			get { return GetDescString( "AddressFor" ); }
			set { SetDescString( "AddressFor", value ); }
		}
		public string CityFor
		{
			get { return GetDescString( "CityFor" ); }
			set { SetDescString( "CityFor", value ); }
		}

		public string StateFor
		{
			get { return GetDescString( "StateFor" ); }
			set { SetDescString( "StateFor", value ); }
		}

		public string ZipFor
		{
			get { return GetDescString( "ZipFor" ); }
			set { SetDescString( "ZipFor", value ); }
		}

        public string AddressFor_SingleLine
        {
            get { return Tools.FormatSingleLineAddress(AddressFor, CityFor, StateFor, ZipFor); }
        }
//-----------
		public string AddressTo
		{
			get { return GetDescString( "AddressTo" ); }
			set { SetDescString( "AddressTo", value ); }
		}
		public string CityTo
		{
			get { return GetDescString( "CityTo" ); }
			set { SetDescString( "CityTo", value ); }
		}

		public string StateTo
		{
			get { return GetDescString( "StateTo" ); }
			set { SetDescString( "StateTo", value ); }
		}

		public string ZipTo
		{
			get { return GetDescString( "ZipTo" ); }
			set { SetDescString( "ZipTo", value ); }
		}
        public string PhoneTo 
        {
            get { return GetDescString("PhoneTo"); }
            set { SetDescString("PhoneTo", value); }
        }
        public string FaxTo 
        {
            get { return GetDescString("FaxTo"); }
            set { SetDescString("FaxTo", value); }
        }
//-----------

		public string AccountName
		{
			get { return GetDescString( "AccountName" ); }
			set { SetDescString( "AccountName", value ); }
		}

		public CDateTime VerifSentD
		{
			get { return GetDateTime( "VerifSentD" ); }
			set { SetDateTime( "VerifSentD", value ); }
		}
		public string VerifSentD_rep
		{
			get { return VerifSentD.ToString(m_convertLos); }
			set { VerifSentD = CDateTime.Create(value, m_convertLos); }
		}

		public CDateTime VerifRecvD
		{
			get { return GetDateTime( "VerifRecvD" ); }
			set { SetDateTime( "VerifRecvD", value ); }
		}
		public string VerifRecvD_rep
		{
			get { return VerifRecvD.ToString(m_convertLos); }
			set { VerifRecvD = CDateTime.Create(value, m_convertLos); }
		}

		public CDateTime VerifExpD
		{
			get { return GetDateTime( "VerifExpD" ); }
			set { SetDateTime( "VerifExpD", value ); }
		}
		public string VerifExpD_rep
		{
			get { return VerifExpD.ToString(m_convertLos); }
			set { VerifExpD = CDateTime.Create(value, m_convertLos); }
		}

		public CDateTime VerifReorderedD
		{
			get { return GetDateTime( "VerifReorderedD" ); }
			set { SetDateTime( "VerifReorderedD", value ); }
		}
		public string VerifReorderedD_rep
		{
			get { return VerifReorderedD.ToString(m_convertLos); }
			set { VerifReorderedD = CDateTime.Create(value, m_convertLos); }
		}


		public string OtherTDesc
		{
			get { return this.GetDescString( "OtherTDesc" ); }
			set { SetDescString( "OtherTDesc", value ); }
		}

		public E_VorT VerifT
		{
			get { return (E_VorT) GetEnum("VerifT", E_VorT.Other ); }
			set { SetEnum( "VerifT", value); }
		}
        public override Enum KeyType 
        {
            get { return VerifT; }
            set { VerifT = (E_VorT) value; }
        }

		public string Attention
		{
			get { return GetDescString( "Attention" ); }
			set { SetDescString( "Attention", value ); }
		}

		public CDateTime PrepD
		{
			get { return GetDateTime( "PrepD" ); }
			set { SetDateTime( "PrepD", value ) ; }
		}
	
		public bool IsSeeAttachment
		{
			get { return GetBool( "IsSeeAttachment" ); }
			set { SetBool( "IsSeeAttachment", value ); }
		}

        public Guid VerifSigningEmployeeId
        {
            get
            {
                return GetGuid("VerifSigningEmployeeId");
            }
            private set
            {
                SetGuid("VerifSigningEmployeeId", value);
            }
        }

        public string VerifSignatureImgId
        {
            get
            {
                return GetString("VerifSignatureImgId");
            }
            private set
            {
                SetString("VerifSignatureImgId", value);
            }
        }

        public bool VerifHasSignature
        {
            get
            {
                return false == string.IsNullOrEmpty(VerifSignatureImgId);
            }
        }

        public void ClearSignature()
        {
            VerifSignatureImgId = "";
            VerifSigningEmployeeId = Guid.Empty;
        }

        public void ApplySignature(Guid employeeId, string imgId)
        {
            VerifSignatureImgId = imgId;
            VerifSigningEmployeeId = employeeId;
        }
	};
}