﻿// <copyright file="DownPaymentGiftContractResolver.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Antonio Valencia
//  Date:   3/18/2015 11:32:14 AM
// </summary>
namespace DataAccess
{
    using System.Reflection;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    /// <summary>
    /// A contract resolver for <see cref="DownPaymentGift"/>. 
    /// </summary>
    public class DownPaymentGiftContractResolver : DefaultContractResolver
    {
        /// <summary>
        /// Indicating whether the DownPaymentGift class is being serialize to the DB instead of the UI.
        /// </summary>
        private bool isForStorage; 

        /// <summary>
        /// Initializes a new instance of the <see cref="DownPaymentGiftContractResolver" /> class.
        /// </summary>
        /// <param name="isForStorage">A value indicating whether the serialization is for storage.</param>
        public DownPaymentGiftContractResolver(bool isForStorage)
        {
            this.isForStorage = isForStorage;
        }
        
        /// <summary>
        /// Creates a property for Serialization. Based on the settings it will serialize some properties and not the others.
        /// </summary>
        /// <param name="member">The type member that will be serialized.</param>
        /// <param name="memberSerialization">The details for serialization.</param>
        /// <returns>A JsonProperty used by Newton to serialize.</returns>
        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            JsonProperty property = base.CreateProperty(member, memberSerialization);

            if (property.DeclaringType == typeof(DownPaymentGift) && member.Name == "Amount")
            {
                property.ShouldSerialize = instance =>
                {
                    return isForStorage;
                };
            }

            if (property.DeclaringType == typeof(DownPaymentGift) && member.Name == "Amount_rep")
            {
                property.ShouldSerialize = instance =>
                {
                    return !isForStorage;
                };
            }

            return property;
        }
    }
}
