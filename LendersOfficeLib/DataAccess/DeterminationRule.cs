﻿// <summary>
// <copyright file="DeterminationRule.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Eduardo Michel
//  Date:   9/17/2015
// </summary>

namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Xml.Serialization;

    /// <summary>
    /// A DeterminationRule contains the conditions and message details to determine when a message should be applied
    /// on the Pml certificate.
    /// </summary>
    public class DeterminationRule
    {
        /// <summary>
        /// Gets or sets The Id of the rule.
        /// </summary>
        /// <value>The Id of the rule.</value>
        [XmlElement("id")]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets A List of messages that will be displayed if the rule applies to a loan.
        /// </summary>
        /// <value>The Id of the rule.</value>
        [XmlArray("message")]
        [XmlArrayItem("text", typeof(DeterminationMessage))]
        public List<DeterminationMessage> Messages { get; set; }

        /// <summary>
        /// Gets or sets A List of conditions that the loan must pass before the messages are applied to the Pml certificate.
        /// </summary>
        /// <value>A List of conditions that the loan must pass before the messages are applied to the Pml certificate.</value>
        [XmlElement("condition")]
        public DeterminationCondition Conditions { get; set; }

        /// <summary>
        /// Returns a bool signifying whether the Determination Rule should apply itself to the loan's Pml certificate.
        /// </summary>
        /// <param name="isRateLockRequested">Is the Rate Lock Requested.</param>
        /// <param name="branchChannelT">The loan's branch channel.</param>
        /// <param name="statusT">The loan's status.</param>
        /// <returns>Returns whether the rule should apply.</returns>
        public bool ShouldApply(bool isRateLockRequested, E_BranchChannelT branchChannelT, E_sStatusT statusT)
        {
            int submissionT = isRateLockRequested ? 1 : 0;
            return this.ShouldApply(submissionT, branchChannelT, statusT);
        }

        /// <summary>
        /// Returns a bool signifying whether the Determination Rule should apply itself to the loan's Pml certificate.
        /// </summary>
        /// <param name="submissionT">Is the Rate Lock Requested? 0 for false, 1 for true.</param>
        /// <param name="branchChannelT">The loan's branch channel.</param>
        /// <param name="statusT">The loan's status.</param>
        /// <returns>Returns whether the rule should apply.</returns>
        public bool ShouldApply(int submissionT, E_BranchChannelT branchChannelT, E_sStatusT statusT)
        {
            return
                this.Conditions.SubmissionType.Exists(p => p.Contains(submissionT))
                && this.Conditions.Channel.Exists(p => p.Contains((int)branchChannelT))
                && this.Conditions.LoanStatus.Exists(p => p.Contains((int)statusT));
        }
    }
}
