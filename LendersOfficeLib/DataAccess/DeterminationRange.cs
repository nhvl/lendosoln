﻿// <summary>
// <copyright file="DeterminationRange.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Eduardo Michel
//  Date:   9/17/2015
// </summary>

namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Xml.Serialization;

    /// <summary>
    /// This class contain's the start and end values for the range, and can determine whether value falls within it.
    /// </summary>
    public class DeterminationRange
    {
        /// <summary>
        /// Gets or sets The start of the range.
        /// </summary>
        /// <value>The start of the range.</value>
        [XmlElement("start")]
        public int Start { get; set; }

        /// <summary>
        /// Gets or sets The end of the range.
        /// </summary>
        /// <value>The end of the range.</value>
        [XmlElement("end")]
        public int? End { get; set; }

        /// <summary>
        /// Checks whether the given item falls within the range.
        /// </summary>
        /// <param name="item">The item that will be checked.</param>
        /// <returns>Whether the item is within the range.</returns>
        public bool Contains(int item)
        {
            if (this.End.HasValue)
            {
                return item >= this.Start && item <= this.End.Value;
            }

            return item == this.Start;
        }
    }
}
