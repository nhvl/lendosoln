﻿//-----------------------------------------------------------------------
// <summary>
//      Convenience class for accessing GFE custom fee line template data from DB.
// </summary>
// <copyright file="FeeType.cs" company="MeridianLink">
//      Copyright (c) MeridianLink. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Convenience class for accessing GFE custom fee line template data from DB.
    /// </summary>
    public class FeeType
    {
        /// <summary>
        /// Stored procedure name for fetching FeeType data.
        /// </summary>
        private const string SpFetch = "FEE_TYPE_Fetch";

        /// <summary>
        /// Stored procedure name for fetching FeeType data based on BrokerId, LineNumber, and Description.
        /// </summary>
        private const string SpFetchByDescription = "FEE_TYPE_FetchByDescription";

        /// <summary>
        /// Stored procedure name for fetching FeeType data based on BrokerId and LineNumber.
        /// </summary>
        private const string SpFetchAll = "FEE_TYPE_FetchAllByBrokerIdAndLineNumber";

        /// <summary>
        /// Stored procedure name for creating new FeeType data.
        /// </summary>
        private const string SpCreate = "FEE_TYPE_Create";

        /// <summary>
        /// Stored procedure name for updating FeeType data.
        /// </summary>
        private const string SpUpdate = "FEE_TYPE_Update";

        /// <summary>
        /// Stored procedure name for deleting FeeType data.
        /// </summary>
        private const string SpDelete = "FEE_TYPE_Delete";

        /// <summary>
        /// True if this FeeType is new and has not yet been saved to DB.
        /// </summary>
        private bool isNew = false;
        
        /// <summary>
        /// Initializes a new instance of the FeeType class. Creates empty FeeType.
        /// </summary>
        internal FeeType()
        {
        }

        /// <summary>
        /// Initializes a new instance of the FeeType class.
        /// </summary>
        /// <param name="brokerId">Broker Id.</param>
        internal FeeType(Guid brokerId)
        {
            this.isNew = true;
            this.BrokerId = brokerId;
        }

        /// <summary>
        /// Initializes a new instance of the FeeType class. Used for constructing FeeType from DB.
        /// </summary>
        /// <param name="reader">DbDataReader object for FEE_TYPE DB data.</param>
        internal FeeType(DbDataReader reader)
        {
            this.isNew = false;
            this.FeeTypeId = (Guid)reader["FeeTypeId"];
            this.BrokerId = (Guid)reader["BrokerId"];
            this.Description = (string)reader["Description"];
            this.GfeLineNumber = (string)reader["GfeLineNumber"];
            this.GfeSection = (E_GfeSectionT)reader["GfeSection"];
            this.Props = (int)reader["Props"];
        }

        /// <summary>
        /// Gets the Fee Type ID.
        /// </summary>
        /// <value>Primary key used in DB.</value>
        public Guid FeeTypeId { get; private set; }

        /// <summary>
        /// Gets Broker ID.
        /// </summary>
        /// <value>Broker ID.</value>
        public Guid BrokerId { get; private set; }

        /// <summary>
        /// Gets or sets Custom Fee Description.
        /// </summary>
        /// <value>Custom Fee Description.</value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets GFE section/line number.
        /// Used string type to keep flexible for possible later expansion.
        /// </summary>
        /// <value>GFE section/line number.</value>
        public string GfeLineNumber { get; set; }

        /// <summary>
        /// Gets or sets GFE Box.
        /// </summary>
        /// <value>GFE Box. 10CharMinPad.</value>
        public E_GfeSectionT GfeSection { get; set; }

        /// <summary>
        /// Gets or sets GFE Fee line properties.
        /// </summary>
        /// <value>GFE Fee Line properties.</value>
        public int Props { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether APR bit is checked.
        /// </summary>
        /// <value>APR bit in Props.</value>
        public bool Apr
        {
            get { return LosConvert.GfeItemProps_Apr(this.Props); }
            set { this.Props = LosConvert.GfeItemProps_UpdateApr(this.Props, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether FhaAllow bit is checked.
        /// </summary>
        /// <value>FhaAllow bit in Props.</value>
        public bool FhaAllow
        {
            get { return LosConvert.GfeItemProps_FhaAllow(this.Props); }
            set { this.Props = LosConvert.GfeItemProps_UpdateFhaAllow(this.Props, value); }
        }

        /// <summary>
        /// Gets a value indicating whether this FeeType is new and has not yet been saved to DB.
        /// </summary>
        /// <value>True if FeeType is new and has not been saved.</value>
        public bool IsNew
        {
            get
            {
                return this.isNew;
            }
        }

        /// <summary>
        /// Creates a new FeeType.
        /// </summary>
        /// <param name="brokerId">ID for broker this FeeType belongs to.</param>
        /// <returns>A new FeeType.</returns>
        public static FeeType Create(Guid brokerId)
        {
            return new FeeType(brokerId);
        }

        /// <summary>
        /// Creates an empty FeeType (no members prefilled).
        /// </summary>
        /// <returns>Empty FeeType.</returns>
        public static FeeType CreateEmpty()
        {
            return new FeeType();
        }

        /// <summary>
        /// Retrieve FeeType by Fee Type ID.
        /// </summary>
        /// <param name="brokerId">Broker id of the fee type.</param>
        /// <param name="feeTypeId">Fee Type ID.</param>
        /// <returns>The FeeType specified by the ID, or null if it is not found.</returns>
        public static FeeType RetrieveById(Guid brokerId, Guid feeTypeId)
        {
            SqlParameter[] parameters = 
                                        {
                                            new SqlParameter("@FeeTypeId", feeTypeId)
                                        };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, SpFetch, parameters))
            {
                if (reader.Read())
                {
                    return new FeeType(reader);
                }
            }

            return null;
        }

        /// <summary>
        /// Retrieve all FeeTypes for a Broker.
        /// </summary>
        /// <param name="brokerId">Broker ID.</param>
        /// <returns>List of FeeTypes.</returns>
        public static List<FeeType> RetrieveAllByBrokerId(Guid brokerId)
        {
            return RetrieveAllByBrokerIdAndLineNumber(brokerId, string.Empty);
        }

        /// <summary>
        /// Retrieve all FeeTypes for a Broker for a certain section/line number.
        /// </summary>
        /// <param name="brokerId">Broker ID.</param>
        /// <param name="lineNumber">Line Number.</param>
        /// <returns>List of FeeTypes.</returns>
        public static List<FeeType> RetrieveAllByBrokerIdAndLineNumber(Guid brokerId, string lineNumber)
        {
            List<FeeType> feeTypes = new List<FeeType>();

            SqlParameter[] parameters = 
                {
                    new SqlParameter("@BrokerId", brokerId), 
                    new SqlParameter("@GfeLineNumber", lineNumber)
                };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, SpFetchAll, parameters))
            {
                while (reader.Read())
                {
                    feeTypes.Add(new FeeType(reader));
                }
            }

            return feeTypes;
        }

        /// <summary>
        /// Retrieve first FeeTypes matching Broker ID, section/line number, and description.
        /// </summary>
        /// <param name="brokerId">Broker ID.</param>
        /// <param name="lineNumber">GFE Section or Line Number.</param>
        /// <param name="description">Fee Type Description.</param>
        /// <returns>List of FeeTypes.</returns>
        public static FeeType RetrieveByDescription(Guid brokerId, string lineNumber, string description)
        {
            SqlParameter[] parameters = 
                {
                    new SqlParameter("@BrokerId", brokerId),
                    new SqlParameter("@GfeLineNumber", lineNumber),
                    new SqlParameter("Description", description)
                };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, SpFetchByDescription, parameters))
            {
                while (reader.Read())
                {
                    return new FeeType(reader);
                }
            }

            return null;
        }

        /// <summary>
        /// Save a collection of FeeTypes to DB.
        /// </summary>
        /// <param name="feeTypes">IEnumerable collection of FeeTypes.</param>
        public static void Save(IEnumerable<FeeType> feeTypes)
        {
            foreach (FeeType feeType in feeTypes)
            {
                feeType.Save();
            }
        }

        /// <summary>
        /// Save and/or Delete collections of FeeTypes.
        /// </summary>
        /// <param name="brokerId">Broker id of this FeeTypes.</param>
        /// <param name="feeTypesToSave">IEnumerable collection of FeeTypes to save.</param>
        /// <param name="idsToDelete">IEnumerable collection of FeeTypes to delete.</param>
        public static void SaveAndDelete(Guid brokerId, IEnumerable<FeeType> feeTypesToSave, IEnumerable<Guid> idsToDelete)
        {
            // Delete
            Delete(brokerId, idsToDelete);

            // Save
            Save(feeTypesToSave);
        }

        /// <summary>
        /// Save or update FeeType.
        /// </summary>
        public void Save()
        {
            SqlParameter[] parameters = this.GetSaveParameters();

            if (this.IsNew)
            {
                // Save
                parameters[0].Direction = System.Data.ParameterDirection.Output;    // Set FeeType to output

                StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, SpCreate, 3, parameters);

                this.FeeTypeId = (Guid)parameters[0].Value;
            }
            else
            {
                // Update
                StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, SpUpdate, 3, parameters);
            }
        }

        /// <summary>
        /// Delete a collection of FeeTypes.
        /// </summary>
        /// <param name="brokerId">Broker ID of the fee type.</param>
        /// <param name="feeTypeIds">IEnumerable Collection of fee type ids.</param>
        private static void Delete(Guid brokerId, IEnumerable<Guid> feeTypeIds)
        {
            foreach (Guid id in feeTypeIds)
            {
                Delete(brokerId, id);
            }
        }

        /// <summary>
        /// Delete Fee Type from DB.
        /// </summary>
        /// <param name="brokerId">Broker ID of the fee type.</param>
        /// <param name="feeTypeId">Fee Type ID.</param>
        private static void Delete(Guid brokerId, Guid feeTypeId)
        {
            SqlParameter[] parrameters =
            {
                new SqlParameter("@FeeTypeId", feeTypeId)
            };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, SpDelete, 3, parrameters);
        }

        /// <summary>
        /// Helper function to get collection of SqlParameters for creating/updating FeeTypes.
        /// </summary>
        /// <returns>Array of SQL Parameters. <seealso cref="SqlParameter"/></returns>
        private SqlParameter[] GetSaveParameters()
        {
            SqlParameter[] sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@FeeTypeId", this.FeeTypeId),
                new SqlParameter("@BrokerId", this.BrokerId),
                new SqlParameter("@Description", this.Description),
                new SqlParameter("@GfeLineNumber", this.GfeLineNumber),
                new SqlParameter("@GfeSection", this.GfeSection),
                new SqlParameter("@Props", this.Props)
            };

            return sqlParameters;
        }
    }
}
