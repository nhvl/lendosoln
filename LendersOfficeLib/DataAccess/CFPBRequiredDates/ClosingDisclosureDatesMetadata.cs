﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using LendersOffice.Common.SerializationTypes;

    /// <summary>
    /// Provides a wrapper for the metadata required
    /// to construct a closing disclosure date.
    /// </summary>
    public class ClosingDisclosureDatesMetadata : CfpbRequiredDatesMetadata
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ClosingDisclosureDatesMetadata"/> class.
        /// </summary>
        /// <param name="getArchiveById">
        /// The archive retrieval function.
        /// </param>
        public ClosingDisclosureDatesMetadata(Func<Guid, IClosingCostArchive> getArchiveById)
            : this(getArchiveById, consumerMetadataByConsumerId: null, loanRescindableType: LoanRescindableT.Blank, alwaysRequireAllBorrowersToReceiveClosingDisclosure: false)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClosingDisclosureDatesMetadata"/> class.
        /// </summary>
        /// <param name="getArchiveById">
        /// The archive retrieval function.
        /// </param>
        /// <param name="consumerMetadataByConsumerId">
        /// The consumer metadata for borrower-level disclosure tracking.
        /// </param>
        /// <param name="loanRescindableType">
        /// The rescindable type of the loan file.
        /// </param>
        /// <param name="alwaysRequireAllBorrowersToReceiveClosingDisclosure">
        /// A value indicating whether all borrowers must receive the CD.
        /// </param>
        public ClosingDisclosureDatesMetadata(
            Func<Guid, IClosingCostArchive> getArchiveById,
            IReadOnlyList<BorrowerDisclosureMetadata> consumerMetadataByConsumerId,
            LoanRescindableT loanRescindableType,
            bool alwaysRequireAllBorrowersToReceiveClosingDisclosure) : base(getArchiveById, consumerMetadataByConsumerId, loanRescindableType)
        {
            this.AlwaysRequireAllBorrowersToReceiveClosingDisclosure = alwaysRequireAllBorrowersToReceiveClosingDisclosure;
        }

        /// <summary>
        /// Gets a value indicating whether all borrowers must receive the CD.
        /// </summary>
        public bool AlwaysRequireAllBorrowersToReceiveClosingDisclosure { get; private set; }
    }
}
