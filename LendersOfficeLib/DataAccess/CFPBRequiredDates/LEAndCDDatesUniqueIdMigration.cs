﻿// <copyright file="LEAndCDDatesUniqueIdMigration.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//     Author: Eric Mallare
//     Date:   10/8/2015
// </summary>
namespace DataAccess
{
    using System;
    using LendersOffice.Constants;
    using LendersOffice.Security;

    /// <summary>
    /// Adds Unique Ids to the Loan Estimate and Closing Disclosure Dates.
    /// </summary>
    public class LEAndCDDatesUniqueIdMigration
    {
        /// <summary>
        /// Adds the Unique Ids to the Dates.
        /// </summary>
        /// <param name="loanid">The loan to migrate.</param>
        public static void AddUniqueIds(Guid loanid)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanid, typeof(LEAndCDDatesUniqueIdMigration));
            dataLoan.ByPassFieldSecurityCheck = true;
            dataLoan.AllowSaveWhileQP2Sandboxed = true;
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            if (dataLoan.sBrokerId != PrincipalFactory.CurrentPrincipal.BrokerId)
            {
                throw new AccessDenied();
            }

            LoanEstimateDatesInfo loanEstimateDatesInfo = dataLoan.sLoanEstimateDatesInfo;
            ClosingDisclosureDatesInfo closingDiscDatesInfo = dataLoan.sClosingDisclosureDatesInfo;

            bool isDirty = false;
            foreach (LoanEstimateDates loanEstDate in loanEstimateDatesInfo.LoanEstimateDatesList)
            {
                if (loanEstDate.UniqueId == Guid.Empty)
                {
                    loanEstDate.UniqueId = Guid.NewGuid();
                    isDirty = true;
                }
            }

            foreach (ClosingDisclosureDates closingDiscDate in closingDiscDatesInfo.ClosingDisclosureDatesList)
            {
                if (closingDiscDate.UniqueId == Guid.Empty)
                {
                    closingDiscDate.UniqueId = Guid.NewGuid();
                    isDirty = true;
                }
            }

            if (isDirty)
            {
                dataLoan.SaveLEandCDDates();
                dataLoan.Save();
            }
        }
    }
}
