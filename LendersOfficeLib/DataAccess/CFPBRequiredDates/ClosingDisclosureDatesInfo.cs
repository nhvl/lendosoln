﻿// <copyright file="ClosingDisclosureDatesInfo.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   2/10/2015 
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;

    using DataAccess.PathDispatch;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// A class holding all ClosingDisclosureDates.
    /// </summary>
    [DataContract]
    public class ClosingDisclosureDatesInfo : IPathResolvable
    {
        /// <summary>
        /// Corresponds to the version that will be released in August 2017.
        /// </summary>
        /// <remarks>
        /// In this version, the records will have DisclosedApr, DisclosedAprLckd, DocVendorApr, and LqbApr.
        /// If you add a new version, make sure to update GetMostRecentVersion().
        /// </remarks>
        public const int Version201708 = 201708;

        /// <summary>
        /// A list of all ClosingDisclosureDates.
        /// </summary>
        /// <remarks>
        /// Do not change the underlying type here. If you absolutely must and
        /// can verify that the change won't break serialization or other areas,
        /// then make sure to update the casts in AddDates and DeleteDate.
        /// </remarks>
        [DataMember]
        private List<ClosingDisclosureDates> closingDisclosureDatesList;

        /// <summary>
        /// The date that represents the loan's closing date.
        /// </summary>
        private DateTime? loanClosingDate;

        /// <summary>
        /// The version of the collection.
        /// </summary>
        /// <remarks>
        /// The goal of the version is to enable special handling in validation code.
        /// Sometimes, you want to handle old collections that may not have had certain
        /// values differently. This originally came up in case 242144.
        /// </remarks>
        [DataMember]
        private int version;

        /// <summary>
        /// Prevents a default instance of the <see cref="ClosingDisclosureDatesInfo"/> class from being created.
        /// </summary>
        /// <remarks>
        /// The new instance will use the most recent version.
        /// </remarks>
        private ClosingDisclosureDatesInfo()
        {
            this.closingDisclosureDatesList = new List<ClosingDisclosureDates>();
            this.version = GetMostRecentVersion();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClosingDisclosureDatesInfo"/> class.
        /// </summary>
        /// <param name="closingDisclosureDatesList">A list of closing disclosure dates.</param>
        /// <param name="version">The version of the collection.</param>
        /// <param name="closingDate">The loan closing date. Can be excluded if a closing date does not yet exist.</param>
        private ClosingDisclosureDatesInfo(List<ClosingDisclosureDates> closingDisclosureDatesList, int version, DateTime? closingDate = null)
        {
            this.closingDisclosureDatesList = closingDisclosureDatesList;
            this.version = version; 

            if (this.loanClosingDate != null)
            {
                this.loanClosingDate = closingDate;
            }
        }

        /// <summary>
        /// Gets or sets the date that represents the loan's closing date.
        /// </summary>
        /// <value>The date that represents the loan's closing date.</value>
        public DateTime? LoanClosingDate
        {
            get
            {
                return this.loanClosingDate;
            }

            set
            {
                this.loanClosingDate = value;
                foreach (var d in this.ClosingDisclosureDatesList)
                {
                    d.LoanClosingDate = value;
                }
            }
        }

        /// <summary>
        /// Gets the list of all ClosingDisclosureDates.
        /// </summary>
        /// <value>The list of all ClosingDisclosureDates.</value>
        public IReadOnlyList<ClosingDisclosureDates> ClosingDisclosureDatesList
        {
            get
            {
                if (this.closingDisclosureDatesList == null)
                {
                    this.closingDisclosureDatesList = new List<ClosingDisclosureDates>();
                }

                return this.closingDisclosureDatesList;
            }
        }

        /// <summary>
        /// Gets the ClosingDisclosureDates marked as Initial.
        /// </summary>
        /// <value>The ClosingDisclosureDates marked as Initial.</value>
        public ClosingDisclosureDates InitialClosingDisclosure
        {
            get
            {
                foreach (ClosingDisclosureDates dates in this.ClosingDisclosureDatesList)
                {
                    if (dates.IsInitial)
                    {
                        return dates;
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the ClosingDisclosureDates marked as Preview.
        /// </summary>
        /// <value>The ClosingDisclosureDates marked as Preview.</value>
        public ClosingDisclosureDates PreviewClosingDisclosure
        {
            get
            {
                foreach (ClosingDisclosureDates dates in this.ClosingDisclosureDatesList)
                {
                    if (dates.IsPreview)
                    {
                        return dates;
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the ClosingDisclosureDates marked as Final.
        /// </summary>
        /// <value>The ClosingDisclosureDated marked as Final.</value>
        public ClosingDisclosureDates FinalClosingDisclosure
        {
            get
            {
                foreach (ClosingDisclosureDates dates in this.ClosingDisclosureDatesList)
                {
                    if (dates.IsFinal)
                    {
                        return dates;
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the version.
        /// </summary>
        /// <value>The version.</value>
        public int Version
        {
            get { return this.version; }
        }

        /// <summary>
        /// Creates an empty <see cref="ClosingDisclosureDatesInfo"/> instance.
        /// </summary>
        /// <returns>The <see cref="ClosingDisclosureDatesInfo"/> instance.</returns>
        public static ClosingDisclosureDatesInfo Create()
        {
            return new ClosingDisclosureDatesInfo();
        }

        /// <summary>
        /// Creates a <see cref="ClosingDisclosureDatesInfo"/> instance from json.
        /// </summary>
        /// <param name="json">The serialized json.</param>
        /// <param name="metadata">The metadata for the closing disclosure dates.</param>
        /// <returns>The <see cref="ClosingDisclosureDatesInfo"/> instance.</returns>
        public static ClosingDisclosureDatesInfo FromJson(string json, ClosingDisclosureDatesMetadata metadata)
        {
            var cds = ObsoleteSerializationHelper.JsonDeserialize<ClosingDisclosureDatesInfo>(json);

            if (cds != null)
            {
                foreach (var cd in cds.ClosingDisclosureDatesList)
                {
                    cd.SetMetadata(metadata);
                }
            }

            return cds;
        }

        /// <summary>
        /// Gets the most recent version.
        /// </summary>
        /// <returns>The most recent version.</returns>
        public static int GetMostRecentVersion() => Version201708;

        /// <summary>
        /// Adds a ClosingDisclosureDates object to the list.
        /// </summary>
        /// <param name="dates">The dates to add.</param>
        public void AddDates(ClosingDisclosureDates dates)
        {
            dates.LoanClosingDate = this.LoanClosingDate;
            ((List<ClosingDisclosureDates>)this.ClosingDisclosureDatesList).Add(dates);
        }

        /// <summary>
        /// Deletes a ClosingDisclosureDates from the list if possible.
        /// </summary>
        /// <param name="date">The date to delete.</param>
        /// <param name="errors">Any errors encountered while deleting.</param>
        /// <returns>True if deleted, false otherwise.</returns>
        public bool DeleteDate(ClosingDisclosureDates date, out string errors)
        {
            errors = string.Empty;
            if (date == null)
            {
                errors = "Date parameter is null.";
                return false;
            }

            if (!this.ValidateDeletedDate(date, out errors))
            {
                return false;
            }

            if (!((List<ClosingDisclosureDates>)this.ClosingDisclosureDatesList).Remove(date))
            {
                errors = "Unable to find and remove the specified date.";
                return false;
            }

            return true;
        }

        /// <summary>
        /// Equality for two ClosingDisclosureDatesInfo objects.
        /// </summary>
        /// <param name="obj">The other object to compare to.</param>
        /// <returns>True if ClosingDisclosureDatesList is equal.</returns>
        public bool IsEqual(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            ClosingDisclosureDatesInfo info = obj as ClosingDisclosureDatesInfo;
            if (info != null)
            {
                if (this.ClosingDisclosureDatesList.Count != info.ClosingDisclosureDatesList.Count)
                {
                    return false;
                }

                List<ClosingDisclosureDates> thisOrdered = this.ClosingDisclosureDatesList.OrderBy(o => o.ToString()).ToList();
                List<ClosingDisclosureDates> infoOrder = info.ClosingDisclosureDatesList.OrderBy(o => o.ToString()).ToList();

                for (int i = 0; i < thisOrdered.Count; i++)
                {
                    if (!thisOrdered[i].IsEqual(infoOrder[i]))
                    {
                        return false;
                    }
                }

                return true;
            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, "Unable to compare ClosingDisclosureDatesInfo");
            }
        }

        /// <summary>
        /// Returns a duplicate of this closing dates info object.
        /// </summary>
        /// <returns>A duplicate of this object.</returns>
        public ClosingDisclosureDatesInfo DuplicatePreservingIds()
        {
            List<ClosingDisclosureDates> duplicateDates = new List<ClosingDisclosureDates>();
            foreach (var currentDate in this.ClosingDisclosureDatesList)
            {
                duplicateDates.Add(currentDate.DuplicatePreservingId());
            }

            return new ClosingDisclosureDatesInfo(duplicateDates, this.version, this.loanClosingDate);
        }

        /// <summary>
        /// Validates this closing disclosure dates to an old list.
        /// </summary>
        /// <param name="oldCdDates">The old closing disclosure dates to validate against.</param>
        /// <param name="isInitialModifiedDates">Any dates that had their IsInitial property modified manually.</param>
        /// <param name="validArchives">The valid archives.</param>
        /// <param name="calculateInitialClosingDisclosure">Whether the intial property is calculated.</param>
        /// <param name="errorMessage">Any errors during validation.</param>
        /// <returns>True if validated, false otherwise.</returns>
        public bool ValidateDatesToOldList(ClosingDisclosureDatesInfo oldCdDates, HashSet<Guid> isInitialModifiedDates, Dictionary<Guid, ClosingCostArchive> validArchives, bool calculateInitialClosingDisclosure, out string errorMessage)
        {
            if (oldCdDates == null)
            {
                throw new ArgumentNullException(nameof(oldCdDates));
            }

            Dictionary<Guid, ClosingDisclosureDates> changedDates = this.ClosingDisclosureDatesList.ToDictionary((date) => date.UniqueId);
            Dictionary<Guid, ClosingDisclosureDates> oldDates = oldCdDates.ClosingDisclosureDatesList.ToDictionary((date) => date.UniqueId);

            ClosingDisclosureDates defaultChecker = ClosingDisclosureDates.Create(metadata: null);
            foreach (var changedDatePair in changedDates)
            {
                Guid id = changedDatePair.Key;
                ClosingDisclosureDates date = changedDatePair.Value;

                ClosingDisclosureDates oldDate = null;
                if (!oldDates.TryGetValue(id, out oldDate))
                {
                    if (!date.IsPostClosing)
                    {
                        if (defaultChecker.IsDisclosurePostClosingDueToCureForToleranceViolation != date.IsDisclosurePostClosingDueToCureForToleranceViolation ||
                            defaultChecker.IsDisclosurePostClosingDueToNonNumericalClericalError != date.IsDisclosurePostClosingDueToNonNumericalClericalError ||
                            defaultChecker.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller != date.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller ||
                            defaultChecker.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower != date.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower ||
                            defaultChecker.PostConsummationKnowledgeOfEventDate != date.PostConsummationKnowledgeOfEventDate ||
                            defaultChecker.PostConsummationRedisclosureReasonDate != date.PostConsummationRedisclosureReasonDate)
                        {
                            errorMessage = $"Cannot modify Post-Closing properties if the date is not marked as Post-Closing. Date id: {date.UniqueId}.";
                            return false;
                        }
                    }
                }
                else
                {
                    if (!oldDate.IsManual && date.CreatedDate != oldDate.CreatedDate)
                    {
                        errorMessage = "Created Date cannot be modified for system generated records.";
                        return false;
                    }

                    if (!oldDate.IsManual && date.ArchiveId != oldDate.ArchiveId)
                    {
                        errorMessage = "Associated archive cannot be modified for system generated records.";
                        return false;
                    }

                    if (oldDate.DisableManualArchiveAssociation && date.ArchiveId != oldDate.ArchiveId)
                    {
                        errorMessage = $"Associated archive id cannot be modified. Date id: {date.UniqueId}.";
                        return false;
                    }

                    if (!date.IsPostClosing)
                    {
                        if (oldDate.IsDisclosurePostClosingDueToCureForToleranceViolation != date.IsDisclosurePostClosingDueToCureForToleranceViolation ||
                            oldDate.IsDisclosurePostClosingDueToNonNumericalClericalError != date.IsDisclosurePostClosingDueToNonNumericalClericalError ||
                            oldDate.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller != date.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller ||
                            oldDate.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower != date.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower ||
                            oldDate.PostConsummationKnowledgeOfEventDate != date.PostConsummationKnowledgeOfEventDate ||
                            oldDate.PostConsummationRedisclosureReasonDate != date.PostConsummationRedisclosureReasonDate)
                        {
                            errorMessage = $"Cannot modify Post-Closing properties if the date is not marked as Post-Closing. Date id: {date.UniqueId}.";
                            return false;
                        }
                    }
                }

                ClosingCostArchive chosenArchive;
                if (date.ArchiveId != Guid.Empty && !validArchives.TryGetValue(date.ArchiveId, out chosenArchive))
                {
                    errorMessage = $"{date.ArchiveId} is not a valid archive. Date id: {date.UniqueId}.";
                    return false;
                }

                if (calculateInitialClosingDisclosure)
                {
                    if (!date.IsCandidateInitial && isInitialModifiedDates.Contains(date.UniqueId))
                    {
                        errorMessage = $"The Initial property for this record is automatically calculated and cannot be modified. Date id: {date.UniqueId}.";
                        return false;
                    }
                }
            }

            // This validates deleted dates.
            foreach (var deletedDatePair in oldDates.Where((pair) => !changedDates.ContainsKey(pair.Key)))
            {
                ClosingDisclosureDates deletedDate = deletedDatePair.Value;
                if (!this.ValidateDeletedDate(deletedDate, out errorMessage))
                {
                    return false;
                }
            }

            errorMessage = string.Empty;
            return true;
        }

        /// <summary>
        /// Calculates the initial closing disclosure.
        /// </summary>
        /// <param name="calculateInitialClosingDisclosure">Whether the initial closing disclosure can be calculated.</param>
        /// <param name="closingCostArchives">The closing cost archives on file.</param>
        public void CalculateInitialClosingDisclosure(bool calculateInitialClosingDisclosure, IEnumerable<ClosingCostArchive> closingCostArchives)
        {
            if (!calculateInitialClosingDisclosure)
            {
                return;
            }

            var validDisclosures = new List<ClosingDisclosureDates>();
            foreach (var disclosure in this.ClosingDisclosureDatesList)
            {
                if ((disclosure.ArchiveId == Guid.Empty ||
                    closingCostArchives.Any(archive => disclosure.ArchiveId == archive.Id && archive.Status == ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed))
                    && disclosure.IssuedDate.CompareTo(DateTime.MinValue) > 0)
                {
                    validDisclosures.Add(disclosure);
                }
            }

            validDisclosures = validDisclosures.OrderBy(d => d.IssuedDate).ToList();

            if (!validDisclosures.Any())
            {
                return;
            }

            var earliestDisclosureDate = validDisclosures.First().IssuedDate;
            var earliestDisclosuresCreatedOnSameDate = validDisclosures.Where(d => d.IssuedDate.Date == earliestDisclosureDate.Date);
            if (earliestDisclosuresCreatedOnSameDate.Count() == 1)
            {
                var initialDisclosure = earliestDisclosuresCreatedOnSameDate.First();
                this.SetInitialDisclosure(initialDisclosure.UniqueId, resetCandidates: true);
                return;
            }
            else if (validDisclosures.Any(d => d.ArchiveId == Guid.Empty))
            {
                var candidateInitialIds = earliestDisclosuresCreatedOnSameDate.Select(d => d.UniqueId);
                this.SetCandidateInitialDisclosures(candidateInitialIds);
                return;
            }

            ClosingDisclosureDates earliestDisclosure = null;
            ClosingCostArchive earliestDisclosureArchive = null;
            foreach (var disclosure in validDisclosures)
            {
                var associatedArchive = closingCostArchives.SingleOrDefault(a => a.Id == disclosure.ArchiveId);

                if (associatedArchive == null)
                {
                    if (earliestDisclosure == null || earliestDisclosure.IssuedDate.CompareTo(earliestDisclosure) < 0)
                    {
                        earliestDisclosure = disclosure;
                    }

                    continue;
                }

                DateTime archiveDate, earliestArchiveDate;
                bool archiveDateParsed = DateTime.TryParse(associatedArchive.DateArchived, out archiveDate);
                bool archiveDateIsPriorToEarliestDisclosure = earliestDisclosure == null || archiveDate.CompareTo(earliestDisclosure.IssuedDate) < 0;
                bool archiveDateIsPriorToEarliestArchive = earliestDisclosureArchive != null
                    && DateTime.TryParse(earliestDisclosureArchive.DateArchived, out earliestArchiveDate)
                    && archiveDate.CompareTo(earliestArchiveDate) < 0;

                if (archiveDateParsed && (archiveDateIsPriorToEarliestDisclosure || archiveDateIsPriorToEarliestArchive))
                {
                    earliestDisclosure = disclosure;
                    earliestDisclosureArchive = associatedArchive;
                }

                if (!archiveDateParsed || disclosure.IssuedDate.Date != archiveDate.Date)
                {
                    var candidateInitialIds = earliestDisclosuresCreatedOnSameDate.Select(d => d.UniqueId);
                    this.SetCandidateInitialDisclosures(candidateInitialIds);
                    return;
                }
            }

            if (earliestDisclosureArchive != null && validDisclosures.Count(disclosure => disclosure.ArchiveId == earliestDisclosureArchive.Id) == 1)
            {
                this.SetInitialDisclosure(earliestDisclosure.UniqueId, resetCandidates: true);
                return;
            }

            var candidateDisclosures = validDisclosures.Where(d => d.ArchiveId == earliestDisclosureArchive.Id);
            this.SetCandidateInitialDisclosures(candidateDisclosures.Select(d => d.UniqueId));
        }

        /// <summary>
        /// Sets the given disclosure to be initial.
        /// </summary>
        /// <param name="initialId">The unique ID for the initial disclosure.</param>
        /// <param name="resetCandidates">Should only be true if this is being called by a system
        /// calculation that has found a single unambiguous initial disclosure.</param>
        public void SetInitialDisclosure(Guid initialId, bool resetCandidates = false)
        {
            foreach (var disclosure in this.closingDisclosureDatesList)
            {
                disclosure.IsInitial = disclosure.UniqueId == initialId;

                if (resetCandidates)
                {
                    disclosure.IsCandidateInitial = false;
                }
            }
        }

        /// <summary>
        /// Sets the given disclosures to be candidate initials, indicating that they can be selected
        /// as initial because an unambiguous initial disclosure cannot be found.
        /// </summary>
        /// <param name="candidateIds">The unique IDs for candidate initial disclosures.</param>
        public void SetCandidateInitialDisclosures(IEnumerable<Guid> candidateIds)
        {
            foreach (var disclosure in this.closingDisclosureDatesList)
            {
                disclosure.IsCandidateInitial = candidateIds.Contains(disclosure.UniqueId);
            }
        }

        /// <summary>
        /// Implements IPathResolvable.GetElement.
        /// </summary>
        /// <param name="element">The element to get.</param>
        /// <returns>The gotten element.</returns>
        object IPathResolvable.GetElement(IDataPathElement element)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Checks if a date can be deleted.
        /// </summary>
        /// <param name="date">The date to check.</param>
        /// <param name="errors">Reason why it can't be deleted.</param>
        /// <returns>True if can be deleted, false otherwise.</returns>
        private bool ValidateDeletedDate(ClosingDisclosureDates date, out string errors)
        {
            if (!date.IsManual)
            {
                errors = $"This date is system generated and cannot be deleted. Date id: {date.UniqueId}.";
                return false;
            }

            if (date.ArchiveId != Guid.Empty)
            {
                errors = $"Cannot delete a date that has an associated archive. Date id: {date.UniqueId}.";
                return false;
            }

            if (date.IsInitial)
            {
                errors = $"Cannot delete a date marked as Initial. Date id: {date.UniqueId}.";
                return false;
            }

            if (date.DisableManualArchiveAssociation)
            {
                errors = $"Cannot delete this date. Date id: {date.UniqueId}.";
                return false;
            }

            if (date.IsPreview)
            {
                errors = $"Cannot delete a date marked as Preview. Date id: {date.UniqueId}.";
                return false;
            }

            if (date.IsFinal)
            {
                errors = $"Cannot delete a date marked as Final. Date id: {date.UniqueId}.";
                return false;
            }

            errors = string.Empty;
            return true;
        }

        /// <summary>
        /// Assigns the most recent version on serialization.
        /// </summary>
        /// <param name="context">The streaming context.</param>
        [OnSerializing]
        private void EnsureMostRecentVersion(StreamingContext context)
        {
            this.version = GetMostRecentVersion();
        }
    }
}
