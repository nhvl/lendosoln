﻿namespace DataAccess
{
    using System;
    using System.Runtime.Serialization;
    using LendersOffice.Common;

    /// <summary>
    /// Provides a wrapper for borrower-level disclosure dates.
    /// </summary>
    [DataContract]
    public class BorrowerDisclosureDates : IEquatable<BorrowerDisclosureDates>
    {
        /// <summary>
        /// The name of the consumer at the time the system
        /// began tracking borrower-level dates for that consumer.
        /// </summary>
        [DataMember]
        private string consumerName;

        /// <summary>
        /// Represents whether the disclosure dates are
        /// excluded from disclosure calculations.
        /// </summary>
        [DataMember]
        private bool excludeFromCalculations;

        /// <summary>
        /// Represents the issued date for the disclosures.
        /// </summary>
        [DataMember]
        private string issuedDate;

        /// <summary>
        /// Represents the delivery method for the disclosures.
        /// </summary>
        [DataMember]
        private int deliveryMethod = (int)E_DeliveryMethodT.LeaveEmpty;

        /// <summary>
        /// Represents the received date for the disclosures.
        /// </summary>
        [DataMember]
        private string receivedDate;

        /// <summary>
        /// Represents the signed date for the disclosures.
        /// </summary>
        [DataMember]
        private string signedDate;

        /// <summary>
        /// The default converter for storage.
        /// </summary>
        private LosConvert defaultConvert;
        
        /// <summary>
        /// Gets or sets the name of the consumer at the time the 
        /// system began tracking borrower-level dates for that consumer.
        /// </summary>
        public string ConsumerName
        {
            get { return this.consumerName; }
            set { this.consumerName = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the dates
        /// in this instance are excluded from regulatory-level
        /// disclosure calculations.
        /// </summary>
        public bool ExcludeFromCalculations
        {
            get { return this.excludeFromCalculations; }
            set { this.excludeFromCalculations = value; }
        }

        /// <summary>
        /// Gets or sets the issued date for the disclosure.
        /// </summary>
        public DateTime? IssuedDate
        {
            get { return CfpbRequiredDateUtils.StringToNullableDate(this.issuedDate); }
            set { this.issuedDate = CfpbRequiredDateUtils.NullableDateToString(value); }
        }

        /// <summary>
        /// Gets or sets the string representation of the issued date.
        /// </summary>
        public string IssuedDate_rep
        {
            get { return CfpbRequiredDateUtils.DateToString(this.IssuedDate, this.DefaultConvert); }
            set { this.IssuedDate = CfpbRequiredDateUtils.ToDateTime(value, this.DefaultConvert); }
        }

        /// <summary>
        /// Gets or sets the delivery method for the disclosure.
        /// </summary>
        public E_DeliveryMethodT DeliveryMethod
        {
            get { return (E_DeliveryMethodT)this.deliveryMethod; }
            set { this.deliveryMethod = (int)value; }
        }

        /// <summary>
        /// Gets or sets the delivery method in string format.
        /// </summary>
        /// <value>
        /// The delivery method in string format.
        /// </value>
        public string DeliveryMethod_rep
        {
            get
            {
                return this.DeliveryMethod.GetDescription();
            }

            set
            {
                E_DeliveryMethodT parsedValue;
                if (string.IsNullOrEmpty(value))
                {
                    this.DeliveryMethod = E_DeliveryMethodT.LeaveEmpty;
                }
                else if (value.TryParseDefine(out parsedValue, ignoreCase: true))
                {
                    this.DeliveryMethod = parsedValue;
                }
            }
        }

        /// <summary>
        /// Gets or sets the received date for the disclosure.
        /// </summary>
        public DateTime? ReceivedDate
        {
            get { return CfpbRequiredDateUtils.StringToNullableDate(this.receivedDate); }
            set { this.receivedDate = CfpbRequiredDateUtils.NullableDateToString(value); }
        }

        /// <summary>
        /// Gets or sets the string representation of the received date.
        /// </summary>
        public string ReceivedDate_rep
        {
            get { return CfpbRequiredDateUtils.DateToString(this.ReceivedDate, this.DefaultConvert); }
            set { this.ReceivedDate = CfpbRequiredDateUtils.ToDateTime(value, this.DefaultConvert); }
        }

        /// <summary>
        /// Gets or sets the signed date for the disclosure.
        /// </summary>
        public DateTime? SignedDate
        {
            get { return CfpbRequiredDateUtils.StringToNullableDate(this.signedDate); }
            set { this.signedDate = CfpbRequiredDateUtils.NullableDateToString(value); }
        }

        /// <summary>
        /// Gets or sets the string representation of the signed date.
        /// </summary>
        public string SignedDate_rep
        {
            get { return CfpbRequiredDateUtils.DateToString(this.SignedDate, this.DefaultConvert); }
            set { this.SignedDate = CfpbRequiredDateUtils.ToDateTime(value, this.DefaultConvert); }
        }

        /// <summary>
        /// Gets the default converter for storage.
        /// </summary>
        /// <value>The default converter for storage.</value>
        private LosConvert DefaultConvert
        {
            get
            {
                if (this.defaultConvert == null)
                {
                    this.defaultConvert = new LosConvert();
                }

                return this.defaultConvert;
            }
        }

        /// <summary>
        /// Determines whether this instance equals another instance.
        /// </summary>
        /// <param name="other">
        /// The other instance to check.
        /// </param>
        /// <returns>
        /// True, if the two instances are equal. False otherwise.
        /// </returns>
        public bool Equals(BorrowerDisclosureDates other)
        {
            if (other == null)
            {
                return false;
            }

            if (object.ReferenceEquals(this, other))
            {
                return true;
            }

            return string.Equals(this.ConsumerName, other.ConsumerName, StringComparison.Ordinal) &&
                this.ExcludeFromCalculations == other.ExcludeFromCalculations &&
                this.IssuedDate == other.IssuedDate &&
                this.DeliveryMethod == other.DeliveryMethod &&
                this.ReceivedDate == other.ReceivedDate &&
                this.SignedDate == other.SignedDate;
        }
    }
}
