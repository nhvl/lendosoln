﻿// <copyright file="LoanEstimateDatesInfo.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   2/10/2015 
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;

    using DataAccess.PathDispatch;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// Class to hold all LoanEstimateDates.
    /// </summary>
    [DataContract]
    public class LoanEstimateDatesInfo : IPathResolvable
    {
        /// <summary>
        /// Corresponds to the version that will be released in August 2017.
        /// </summary>
        /// <remarks>
        /// In this version, the records will have DisclosedApr, DisclosedAprLckd, DocVendorApr, and LqbApr.
        /// If you add a new version, make sure to update GetMostRecentVersion().
        /// </remarks>
        public const int Version201708 = 201708;

        /// <summary>
        /// List of all LoanEstimateDates.
        /// </summary>
        /// <remarks>
        /// Do not change the underlying type here. If you absolutely must and
        /// can verify that the change won't break serialization or other areas,
        /// then make sure to update the casts in AddDates and DeleteDate.
        /// </remarks>
        [DataMember]
        private List<LoanEstimateDates> loanEstimateDatesList;

        /// <summary>
        /// The version of the collection.
        /// </summary>
        /// <remarks>
        /// The goal of the version is to enable special handling in validation code.
        /// Sometimes, you want to handle old collections that may not have had certain
        /// values differently. This originally came up in case 242144.
        /// </remarks>
        [DataMember]
        private int version;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanEstimateDatesInfo"/> class.
        /// </summary>
        /// <param name="version">Optional. The version of the collection. If not provided, will default to the most recent version.</param>
        private LoanEstimateDatesInfo(int? version = null)
        {
            this.loanEstimateDatesList = new List<LoanEstimateDates>();

            if (version.HasValue)
            {
                this.version = version.Value;
            }
            else
            {
                this.version = GetMostRecentVersion();
            }
        }

        /// <summary>
        /// Gets the list of all LoanEstimateDates.
        /// </summary>
        /// <value>The list of all LoanEstimateDates.</value>
        public IReadOnlyList<LoanEstimateDates> LoanEstimateDatesList
        {
            get
            {
                if (this.loanEstimateDatesList == null)
                {
                    this.loanEstimateDatesList = new List<LoanEstimateDates>();
                }

                return this.loanEstimateDatesList;
            }
        }

        /// <summary>
        /// Gets the LoanEstimateDates marked as Initial.
        /// </summary>
        /// <value>The LoanEstimateDate marked as Initial.</value>
        public LoanEstimateDates InitialLoanEstimate
        {
            get
            {
                return this.LoanEstimateDatesList.FirstOrDefault(dates => dates.IsInitial);
            }
        }

        /// <summary>
        /// Gets the LoanEstimateDates with the most recent issued date following the LoanEstimateDates
        /// marked as initial.
        /// </summary>
        /// <value>The LoanEstimateDates with the most recent issued date following the Initial.</value>
        public LoanEstimateDates LastRevisedLoanEstimate
        {
            get
            {
                LoanEstimateDates initial = this.InitialLoanEstimate;
                if (initial == null)
                {
                    return null;
                }

                DateTime initialIssuedD = initial.IssuedDate;
                LoanEstimateDates lastRevised = null;
                foreach (LoanEstimateDates dates in this.loanEstimateDatesList)
                {
                    if (!dates.IsInitial)
                    {
                        if (lastRevised == null && initialIssuedD <= dates.IssuedDate)
                        {
                            lastRevised = dates;
                        }
                        else if (lastRevised != null && lastRevised.IssuedDate <= dates.IssuedDate)
                        {
                            lastRevised = dates;
                        }
                        else
                        {
                            continue;
                        }
                    }
                }

                return lastRevised;
            }
        }

        /// <summary>
        /// Gets the version.
        /// </summary>
        /// <value>The version.</value>
        public int Version
        {
            get { return this.version; }
        }

        /// <summary>
        /// Creates a <see cref="LoanEstimateDatesInfo"/> instance from json.
        /// </summary>
        /// <param name="json">The serialized json.</param>
        /// <param name="metadata">The metadata for the loan estimate dates.</param>
        /// <returns>The <see cref="LoanEstimateDatesInfo"/> instance.</returns>
        public static LoanEstimateDatesInfo FromJson(string json, LoanEstimateDatesMetadata metadata)
        {
            var leDates = ObsoleteSerializationHelper.JsonDeserialize<LoanEstimateDatesInfo>(json);

            if (leDates != null)
            {
                foreach (var le in leDates.LoanEstimateDatesList)
                {
                    le.SetMetadata(metadata);
                }
            }

            return leDates;
        }

        /// <summary>
        /// Creates an empty <see cref="LoanEstimateDatesInfo"/> instance.
        /// </summary>
        /// <returns>The <see cref="LoanEstimateDatesInfo"/> instance.</returns>
        public static LoanEstimateDatesInfo Create()
        {
            return new LoanEstimateDatesInfo();
        }

        /// <summary>
        /// Gets the most recent version.
        /// </summary>
        /// <returns>The most recent version.</returns>
        public static int GetMostRecentVersion() => Version201708;

        /// <summary>
        /// Adds a LoanEstimateDates object to the list.
        /// </summary>
        /// <param name="dates">The dates to add.</param>
        public void AddDates(LoanEstimateDates dates)
        {
            ((List<LoanEstimateDates>)this.LoanEstimateDatesList).Add(dates);
        }

        /// <summary>
        /// Deletes the LoanEstimateDates object from the list.
        /// </summary>
        /// <param name="date">The date to delete.</param>
        /// <param name="errors">Any errors encountered during deletion.</param>
        /// <returns>True if successfully deleted, false otherwise.</returns>
        public bool DeleteDate(LoanEstimateDates date, out string errors)
        {
            errors = string.Empty;
            if (date == null)
            {
                errors = "Date parameter is null.";
                return false;
            }

            if (!this.ValidateDeletedDate(date, out errors))
            {
                return false;
            }

            if (!((List<LoanEstimateDates>)this.LoanEstimateDatesList).Remove(date))
            {
                errors = "Unable to find and remove the specified date.";
                return false;
            }

            return true;
        }

        /// <summary>
        /// Duplicates this loan estimate dates info object.
        /// </summary>
        /// <returns>The duplicate loan estimate dates.</returns>
        public LoanEstimateDatesInfo DuplicatePreservingIds()
        {
            var newDates = new LoanEstimateDatesInfo(this.version);
            foreach (var oldDate in this.LoanEstimateDatesList)
            {
                newDates.AddDates(oldDate.DuplicatePreservingId());
            }

            return newDates;
        }

        /// <summary>
        /// Validates this loan estimate dates against an old loan estimate dates.
        /// </summary>
        /// <param name="oldLeDates">The old loan estimate dates to check against.</param>
        /// <param name="isInitialModifiedDates">A set of ids for dates that have had their IsInitial property modified.</param>
        /// <param name="validArchives">The valid archives that can be chosen.</param>
        /// <param name="calculateInitialLoanEstimate">Whether or not the intial loan estimate dates is calculated.</param>
        /// <param name="errorMessage">Any error messages encountered.</param>
        /// <returns>True if this new list if valid. False otherwise.</returns>
        public bool ValidateDatesToOldList(LoanEstimateDatesInfo oldLeDates, HashSet<Guid> isInitialModifiedDates, Dictionary<Guid, ClosingCostArchive> validArchives, bool calculateInitialLoanEstimate, out string errorMessage)
        {
            if (oldLeDates == null)
            {
                throw new ArgumentNullException(nameof(oldLeDates));
            }

            Dictionary<Guid, LoanEstimateDates> changedDates = this.LoanEstimateDatesList.ToDictionary((date) => date.UniqueId);
            Dictionary<Guid, LoanEstimateDates> oldDates = oldLeDates.LoanEstimateDatesList.ToDictionary((date) => date.UniqueId);

            foreach (var changedDatePair in changedDates)
            {
                Guid id = changedDatePair.Key;
                LoanEstimateDates date = changedDatePair.Value;

                LoanEstimateDates oldDate = null;
                if (oldDates.TryGetValue(id, out oldDate))
                {
                    if (!oldDate.IsManual && date.CreatedDate != oldDate.CreatedDate)
                    {
                        errorMessage = "Created Date cannot be modified for system generated records.";
                        return false;
                    }

                    if (!oldDate.IsManual && date.ArchiveId != oldDate.ArchiveId)
                    {
                        errorMessage = "Associated archive cannot be modified for system generated records.";
                        return false;
                    }

                    if (oldDate.DisableManualArchiveAssociation && date.ArchiveId != oldDate.ArchiveId)
                    {
                        errorMessage = $"Associated archive id cannot be modified. Date id: {date.UniqueId}.";
                        return false;
                    }
                }

                ClosingCostArchive chosenArchive;
                if (date.ArchiveId != Guid.Empty && !validArchives.TryGetValue(date.ArchiveId, out chosenArchive))
                {
                    errorMessage = $"{date.ArchiveId} is not a valid archive. Date id: {date.UniqueId}.";
                    return false;
                }

                if (calculateInitialLoanEstimate)
                {
                    if (!date.IsCandidateInitial && isInitialModifiedDates.Contains(date.UniqueId))
                    {
                        errorMessage = $"The Initial property for is automatically calculated and cannot be modified. Date id: {date.UniqueId}.";
                        return false;
                    }
                }
            }

            // This validates deleted dates.
            foreach (var deletedDatePair in oldDates.Where((pair) => !changedDates.ContainsKey(pair.Key)))
            {
                LoanEstimateDates deletedDate = deletedDatePair.Value;
                if (!this.ValidateDeletedDate(deletedDate, out errorMessage))
                {
                    return false;
                }
            }

            errorMessage = string.Empty;
            return true;
        }

        /// <summary>
        /// Calculates the initial loan estimate.
        /// </summary>
        /// <param name="calculateInitialLoanEstimate">The field sCalculateInitialLoanEstimate.</param>
        /// <param name="closingCostArchives">The closing cost archives on the file.</param>
        public void CalculateInitialLoanEstimate(bool calculateInitialLoanEstimate, IEnumerable<ClosingCostArchive> closingCostArchives)
        {
            if (!calculateInitialLoanEstimate)
            {
                return;
            }

            var validDisclosures = new List<LoanEstimateDates>();
            foreach (var disclosure in this.LoanEstimateDatesList)
            {
                if ((disclosure.ArchiveId == Guid.Empty || 
                    closingCostArchives.Any(archive => disclosure.ArchiveId == archive.Id && archive.Status == ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed))
                    && disclosure.IssuedDate.CompareTo(DateTime.MinValue) > 0)
                {
                    validDisclosures.Add(disclosure);
                }
            }

            validDisclosures = validDisclosures.OrderBy(d => d.IssuedDate).ToList();

            if (!validDisclosures.Any())
            {
                return;
            }

            var earliestDisclosureDate = validDisclosures.First().IssuedDate;
            var earliestDisclosuresCreatedOnSameDate = validDisclosures.Where(d => d.IssuedDate.Date == earliestDisclosureDate.Date);
            if (earliestDisclosuresCreatedOnSameDate.Count() == 1)
            {
                var initialDisclosure = earliestDisclosuresCreatedOnSameDate.First();
                this.SetInitialDisclosure(initialDisclosure.UniqueId, resetCandidates: true);
                return;
            }
            else if (validDisclosures.Any(d => d.ArchiveId == Guid.Empty))
            {
                var candidateInitialIds = earliestDisclosuresCreatedOnSameDate.Select(d => d.UniqueId);
                this.SetCandidateInitialDisclosures(candidateInitialIds);
                return;
            }

            LoanEstimateDates earliestDisclosure = null;
            ClosingCostArchive earliestDisclosureArchive = null;
            foreach (var disclosure in validDisclosures)
            {
                var associatedArchive = closingCostArchives.SingleOrDefault(a => a.Id == disclosure.ArchiveId);

                if (associatedArchive == null)
                {
                    if (earliestDisclosure == null || earliestDisclosure.IssuedDate.CompareTo(earliestDisclosure) < 0)
                    {
                        earliestDisclosure = disclosure;
                    }

                    continue;
                }

                DateTime archiveDate, earliestArchiveDate;
                bool archiveDateParsed = DateTime.TryParse(associatedArchive.DateArchived, out archiveDate);
                bool archiveDateIsPriorToEarliestDisclosure = earliestDisclosure == null || archiveDate.CompareTo(earliestDisclosure.IssuedDate) < 0;
                bool archiveDateIsPriorToEarliestArchive = earliestDisclosureArchive != null
                    && DateTime.TryParse(earliestDisclosureArchive.DateArchived, out earliestArchiveDate)
                    && archiveDate.CompareTo(earliestArchiveDate) < 0;

                if (archiveDateParsed && (archiveDateIsPriorToEarliestDisclosure || archiveDateIsPriorToEarliestArchive))
                {
                    earliestDisclosure = disclosure;
                    earliestDisclosureArchive = associatedArchive;
                }

                if (!archiveDateParsed || disclosure.IssuedDate.Date != archiveDate.Date)
                {
                    var candidateInitialIds = earliestDisclosuresCreatedOnSameDate.Select(d => d.UniqueId);
                    this.SetCandidateInitialDisclosures(candidateInitialIds);
                    return;
                }
            }

            if (earliestDisclosureArchive != null && validDisclosures.Count(disclosure => disclosure.ArchiveId == earliestDisclosureArchive.Id) == 1)
            {
                this.SetInitialDisclosure(earliestDisclosure.UniqueId, resetCandidates: true);
                return;
            }

            var candidateDisclosures = validDisclosures.Where(d => d.ArchiveId == earliestDisclosureArchive.Id);
            this.SetCandidateInitialDisclosures(candidateDisclosures.Select(d => d.UniqueId));
            return; 
        }

        /// <summary>
        /// Equates two LoanEstimateDatesInfo objects.
        /// </summary>
        /// <param name="obj">The other object to compare to.</param>
        /// <returns>True if LoanEstimateDatesList is equal, false otherwise.</returns>
        public bool IsEqual(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            LoanEstimateDatesInfo info = obj as LoanEstimateDatesInfo;
            if (info != null)
            {
                if (this.LoanEstimateDatesList.Count != info.LoanEstimateDatesList.Count)
                {
                    return false;
                }

                List<LoanEstimateDates> thisOrdered = this.LoanEstimateDatesList.OrderBy(o => o.ToString()).ToList();
                List<LoanEstimateDates> infoOrder = info.LoanEstimateDatesList.OrderBy(o => o.ToString()).ToList();

                return !thisOrdered.Where((t, i) => !t.IsEqual(infoOrder[i])).Any();
            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, "Unable to compare LoanEstimateDatesInfo.");
            }
        }

        /// <summary>
        /// Sets the given disclosure to be initial.
        /// </summary>
        /// <param name="initialId">The unique ID for the initial disclosure.</param>
        /// <param name="resetCandidates">Should only be true if this is being called by a system
        /// calculation that has found a single unambiguous initial disclosure.</param>
        public void SetInitialDisclosure(Guid initialId, bool resetCandidates = false)
        {
            foreach (var disclosure in this.loanEstimateDatesList)
            {
                disclosure.IsInitial = disclosure.UniqueId == initialId;

                if (resetCandidates)
                {
                    disclosure.IsCandidateInitial = false;
                }
            }
        }

        /// <summary>
        /// Sets the given disclosures to be candidate initials, indicating that they can be selected
        /// as initial because an unambiguous initial disclosure cannot be found.
        /// </summary>
        /// <param name="candidateIds">The unique IDs for candidate initial disclosures.</param>
        public void SetCandidateInitialDisclosures(IEnumerable<Guid> candidateIds)
        {
            foreach (var disclosure in this.loanEstimateDatesList)
            {
                disclosure.IsCandidateInitial = candidateIds.Contains(disclosure.UniqueId);
            }
        }

        /// <summary>
        /// Implements IPathResolvable.GetElement.
        /// </summary>
        /// <param name="element">The element to get.</param>
        /// <returns>The gotten element.</returns>
        object IPathResolvable.GetElement(IDataPathElement element)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Validates the date for deletion.
        /// </summary>
        /// <param name="date">The date to check.</param>
        /// <param name="errors">Any errors encountered.</param>
        /// <returns>Returns true if this date can be deleted. False otherwise.</returns>
        private bool ValidateDeletedDate(LoanEstimateDates date, out string errors)
        {
            if (!date.IsManual)
            {
                errors = $"This date is system generated and cannot be deleted. Date id: {date.UniqueId}.";
                return false;
            }

            if (date.ArchiveId != Guid.Empty)
            {
                errors = $"Cannot delete a date that has an associated archive. Date id: {date.UniqueId}.";
                return false;
            }

            if (date.IsInitial)
            {
                errors = $"Cannot delete a date marked as Initial. Date id: {date.UniqueId}.";
                return false;
            }

            if (date.DisableManualArchiveAssociation)
            {
                errors = $"Cannot delete this date. Date id: {date.UniqueId}.";
                return false;
            }

            errors = string.Empty;
            return true;
        }

        /// <summary>
        /// Assigns the most recent version on serialization.
        /// </summary>
        /// <param name="context">The streaming context.</param>
        [OnSerializing]
        private void EnsureMostRecentVersion(StreamingContext context)
        {
            this.version = GetMostRecentVersion();
        }
    }
}
