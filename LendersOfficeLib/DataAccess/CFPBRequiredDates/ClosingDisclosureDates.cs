﻿// <copyright file="ClosingDisclosureDates.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   2/10/2015
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Runtime.Serialization;

    using DataAccess.PathDispatch;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.UI;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// The class holding info for closing packages.
    /// NOTE: If you are adding more fields to this, please also update the DisclosureCenter.aspx page so that the new field actually gets saved when the General page gets saved.
    /// </summary>
    /// <remarks>
    /// When adding new fields to this object make sure to update LoanFileFieldValidator::ValidateClosingDisclosureDates, especially for fields that could be null on deserialization.
    /// </remarks>
    [DataContract]
    public class ClosingDisclosureDates : IPathResolvable
    {
        /// <summary>
        /// The identifier for this dates object.
        /// </summary>
        [DataMember]
        private Guid uniqueId;

        /// <summary>
        /// The created date for the closing package.
        /// </summary>
        [DataMember]
        private string createdDate;

        /// <summary>
        /// The issued date for the closing package.
        /// </summary>
        [DataMember]
        private string issuedDate;

        /// <summary>
        /// If the issued date is locked to user input.
        /// </summary>
        [DataMember]
        private bool issuedDateLckd;

        /// <summary>
        /// The delivery method for the closing package.
        /// </summary>
        [DataMember]
        private int deliveryMethod;

        /// <summary>
        /// If the delivery method is locked to user input.
        /// </summary>
        [DataMember]
        private bool deliveryMethodLckd;

        /// <summary>
        /// The received date for the closing package.
        /// </summary>
        [DataMember]
        private string receivedDate;

        /// <summary>
        /// If the received date is locked to user input.
        /// </summary>
        [DataMember]
        private bool receivedDateLckd;

        /// <summary>
        /// If the closing package is the initial one.
        /// </summary>
        [DataMember]
        private bool isInitial;

        /// <summary>
        /// If the Initial is calculated, but the algorithm cannot determine the initial exactly,
        /// the user must choose between the narrowed-down candidates. This should be true for a
        /// candidate initial disclosure.
        /// </summary>
        [DataMember]
        private bool isCandidateInitial;

        /// <summary>
        /// If the closing package is a preview.
        /// </summary>
        [DataMember]
        private bool isPreview;

        /// <summary>
        /// If the closing package is the final one.
        /// </summary>
        [DataMember]
        private bool isFinal;

        /// <summary>
        /// The loan product for compliance validation.
        /// </summary>
        [DataMember]
        private string lastDisclosedTRIDLoanProductDescription;

        /// <summary>
        /// If the disclosure package was manually entered.
        /// </summary>
        [DataMember]
        private bool isManual;

        /// <summary>
        /// The ID for the associated UCD document.
        /// </summary>
        [DataMember]
        private Guid? ucdDocument;

        /// <summary>
        /// The archive date.
        /// </summary>
        [DataMember]
        private string archiveDate;

        /// <summary>
        /// The Id of the closing cost archive used.
        /// </summary>
        [DataMember]
        private Guid? archiveId;

        /// <summary>
        /// The default converter for storage.
        /// </summary>
        private LosConvert defaultConvert;

        /// <summary>
        /// The Transaction ID resulting from doc generation.
        /// </summary>
        [DataMember]
        private string transactionId;

        /// <summary>
        /// The <see cref="LendersOffice.Integration.DocumentVendor.VendorConfig.VendorId"/> of the document vendor used to place the order.
        /// </summary>
        [DataMember]
        private Guid? vendorId;

        /// <summary>
        /// The document code returned by the document vendor.
        /// </summary>
        [DataMember]
        private string docCode;

        /// <summary>
        /// Indicates whether the user should be able to update the associated archive.
        /// </summary>
        [DataMember]
        private bool disableManualArchiveAssociation;

        /// <summary>
        /// Indicates whether the closing disclosure was issued after the closing
        /// date due to a numerical change in amount paid by the borrower.
        /// </summary>
        [DataMember]
        private bool isDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower;

        /// <summary>
        /// Indicates whether the closing disclosure was issued after the closing
        /// date due to a numerical change in amount paid by the seller.
        /// </summary>
        [DataMember]
        private bool isDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller;

        /// <summary>
        /// Indicates whether the closing disclosure was issued after the closing
        /// date due to a non-numerical clerical error.
        /// </summary>
        [DataMember]
        private bool isDisclosurePostClosingDueToNonNumericalClericalError;

        /// <summary>
        /// Indicates whether the closing disclosure was issued after the closing
        /// date due to a cure for a tolerance violation.
        /// </summary>
        [DataMember]
        private bool isDisclosurePostClosingDueToCureForToleranceViolation;

        /// <summary>
        /// The date on which the event prompting the post-consummation redisclosure occurred.
        /// </summary>
        [DataMember]
        private string postConsummationRedisclosureReasonDate;

        /// <summary>
        /// The date on which the creditor received knwoledge of the event prompting
        /// the post-consummation redisclosure.
        /// </summary>
        [DataMember]
        private string postConsummationKnowledgeOfEventDate;

        /// <summary>
        /// Indicates whether the disclosed APR is locked to allow manual data entry.
        /// </summary>
        [DataMember]
        private bool disclosedAprLckd;

        /// <summary>
        /// The disclosed APR associated with the disclosure.
        /// </summary>
        [DataMember]
        private decimal? disclosedApr;

        /// <summary>
        /// The APR that LQB calculated for the disclosure.
        /// </summary>
        /// <remarks>
        /// This field is needed for display in the UI, but I'm not sure of a decent
        /// way of doing that without making it a DataMember. So it will be stored too.
        /// gf opm 242144.
        /// </remarks>
        [DataMember]
        private decimal? lqbApr;

        /// <summary>
        /// The APR that is returned by the document vendor.
        /// </summary>
        /// <remarks>
        /// We will maintain the precision returned by the document vendor but display the value
        /// rounded to 3 decimal places like the rest of the APR values.
        /// </remarks>
        [DataMember]
        private decimal? docVendorApr;

        /// <summary>
        /// The date the CD was signed.
        /// </summary>
        [DataMember]
        private string signedDate;

        /// <summary>
        /// If the signed date is locked to user input.
        /// </summary>
        [DataMember]
        private bool signedDateLckd;

        /// <summary>
        /// The borrower-level disclosure dates by consumer ID.
        /// </summary>
        [DataMember]
        private List<KeyValuePair<Guid, BorrowerDisclosureDates>> disclosureDatesByConsumerId;

        /// <summary>
        /// The mapping between consumer IDs and consumer types.
        /// </summary>
        private Dictionary<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, E_aTypeT> consumerTypesByConsumerId;

        /// <summary>
        /// Whether all borrowers are required to receive the CD for determining
        /// the received and signed dates.
        /// </summary>
        private bool alwaysRequireAllBorrowersToReceiveClosingDisclosure;

        /// <summary>
        /// The rescindable type of the loan file.
        /// </summary>
        private LoanRescindableT loanRescindableType;

        /// <summary>
        /// A function to retrieve an archive by an archive id.
        /// </summary>
        private Func<Guid, IClosingCostArchive> getArchiveById;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClosingDisclosureDates" /> class.
        /// </summary>
        /// <param name="metadata">The metadata for the construction of the class.</param>
        private ClosingDisclosureDates(ClosingDisclosureDatesMetadata metadata)
        {
            this.uniqueId = Guid.NewGuid();
            this.getArchiveById = metadata?.GetArchiveById;
            this.alwaysRequireAllBorrowersToReceiveClosingDisclosure = metadata?.AlwaysRequireAllBorrowersToReceiveClosingDisclosure ?? false;
            this.loanRescindableType = metadata?.LoanRescindableType ?? LoanRescindableT.Blank;

            this.disclosureDatesByConsumerId = new List<KeyValuePair<Guid, BorrowerDisclosureDates>>();
            this.consumerTypesByConsumerId = new Dictionary<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, E_aTypeT>();

            if (metadata?.ConsumerMetadataByConsumerId != null)
            {
                foreach (var consumerMetadata in metadata.ConsumerMetadataByConsumerId)
                {
                    this.consumerTypesByConsumerId.Add(consumerMetadata.ConsumerId, consumerMetadata.ConsumerType);

                    this.disclosureDatesByConsumerId.Add(new KeyValuePair<Guid, BorrowerDisclosureDates>(
                        consumerMetadata.ConsumerId.Value, // Use the Guid backing field
                        new BorrowerDisclosureDates() { ConsumerName = consumerMetadata.ConsumerName }));
                }
            }
        }

        /// <summary>
        /// Gets the borrower-level disclosure dates by consumer ID.
        /// </summary>
        public ReadOnlyCollection<KeyValuePair<Guid, BorrowerDisclosureDates>> DisclosureDatesByConsumerId => this.disclosureDatesByConsumerId.AsReadOnly();

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier for this.</value>
        public Guid UniqueId
        {
            get
            {
                return this.uniqueId;
            }

            set
            {
                this.uniqueId = value;
            }
        }

        /// <summary>
        /// Gets or sets the closing date of the loan file.
        /// </summary>
        /// <value>The closing date of the loan file.</value>
        public DateTime? LoanClosingDate { get; set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        /// <value>The created date for the closing package.</value>
        public DateTime CreatedDate
        {
            get
            {
                return CfpbRequiredDateUtils.ToDate(this.createdDate, this.DefaultConvert);
            }

            set
            {
                this.createdDate = CfpbRequiredDateUtils.DateToString(value, this.DefaultConvert);
            }
        }

        /// <summary>
        /// Gets or sets the issued date.
        /// </summary>
        /// <value>The issued date for the closing package.</value>
        public DateTime IssuedDate
        {
            get
            {
                if (this.IssuedDateLckd)
                {
                    return CfpbRequiredDateUtils.ToDate(this.issuedDate, this.DefaultConvert);
                }

                var candidate = CfpbRequiredDateUtils.CalculateEarliestDisclosureDatesCandidate(
                    this.IsManual,
                    this.consumerTypesByConsumerId,
                    this.disclosureDatesByConsumerId,
                    date => date.IssuedDate);

                return candidate?.IssuedDate ?? DateTime.MinValue;
            }

            set
            {
                this.issuedDate = CfpbRequiredDateUtils.DateToString(value, this.DefaultConvert);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the issued date is locked.
        /// </summary>
        public bool IssuedDateLckd
        {
            get { return this.issuedDateLckd || this.disclosureDatesByConsumerId.Count == 0; }
            set { this.issuedDateLckd = value; }
        }
        
        /// <summary>
        /// Gets or sets the string representation of the issued date.
        /// </summary>
        public string IssuedDate_rep
        {
            get { return CfpbRequiredDateUtils.DateToString(this.IssuedDate, this.DefaultConvert); }
            set { this.IssuedDate = CfpbRequiredDateUtils.ToDateTime(value, this.DefaultConvert); }
        }

        /// <summary>
        /// Gets or sets the delivery method.
        /// </summary>
        /// <value>The delivery method for the closing package.</value>
        public E_DeliveryMethodT DeliveryMethod
        {
            get
            {
                if (this.DeliveryMethodLckd)
                {
                    return (E_DeliveryMethodT)this.deliveryMethod;
                }

                var candidate = CfpbRequiredDateUtils.CalculateEarliestDisclosureDatesCandidate(
                    this.IsManual,
                    this.consumerTypesByConsumerId,
                    this.disclosureDatesByConsumerId,
                    date => date.IssuedDate);

                return candidate?.DeliveryMethod ?? E_DeliveryMethodT.LeaveEmpty;
            }

            set
            {
                this.deliveryMethod = (int)value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the delivery method is locked.
        /// </summary>
        public bool DeliveryMethodLckd
        {
            get { return this.deliveryMethodLckd || this.disclosureDatesByConsumerId.Count == 0; }
            set { this.deliveryMethodLckd = value; }
        }

        /// <summary>
        /// Gets or sets the delivery method in string format.
        /// </summary>
        /// <value>
        /// The delivery method in string format.
        /// </value>
        public string DeliveryMethod_rep
        {
            get
            {
                return this.DeliveryMethod.GetDescription();
            }

            set
            {
                E_DeliveryMethodT parsedValue;
                if (string.IsNullOrEmpty(value))
                {
                    this.DeliveryMethod = E_DeliveryMethodT.LeaveEmpty;
                }
                else if (value.TryParseDefine(out parsedValue, ignoreCase: true))
                {
                    this.DeliveryMethod = parsedValue;
                }
            }
        }

        /// <summary>
        /// Gets or sets the received date.
        /// </summary>
        /// <value>The received date for the closing package.</value>
        public DateTime ReceivedDate
        {
            get
            {
                if (this.ReceivedDateLckd)
                {
                    return CfpbRequiredDateUtils.ToDate(this.receivedDate, this.DefaultConvert);
                }

                var candidate = CfpbRequiredDateUtils.CalculateEarliestClosingDisclosureCandidate(
                    this.IsManual,
                    this.loanRescindableType,
                    this.alwaysRequireAllBorrowersToReceiveClosingDisclosure,
                    this.consumerTypesByConsumerId,
                    this.disclosureDatesByConsumerId,
                    date => date.ReceivedDate);

                return candidate?.ReceivedDate ?? DateTime.MinValue;
            }

            set
            {
                this.receivedDate = CfpbRequiredDateUtils.DateToString(value, this.DefaultConvert);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the received date is locked.
        /// </summary>
        public bool ReceivedDateLckd
        {
            get { return this.receivedDateLckd || this.disclosureDatesByConsumerId.Count == 0; }
            set { this.receivedDateLckd = value; }
        }

        /// <summary>
        /// Gets or sets the string representation of the received date.
        /// </summary>
        public string ReceivedDate_rep
        {
            get { return CfpbRequiredDateUtils.DateToString(this.ReceivedDate, this.DefaultConvert); }
            set { this.ReceivedDate = CfpbRequiredDateUtils.ToDateTime(value, this.DefaultConvert); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the closing package is the initial one.
        /// </summary>
        /// <value>If the closing package is the initial one.</value>
        public bool IsInitial
        {
            get
            {
                return this.isInitial;
            }

            set
            {
                this.isInitial = value;
            }
        }

        /// <summary>
        /// Gets or sets the string representation of the <see cref="IsInitial"/> boolean.
        /// </summary>
        public string IsInitial_rep
        {
            get { return this.IsInitial.ToString(); }
            set { this.IsInitial = CfpbRequiredDateUtils.ToBool(value, defaultValue: this.IsInitial); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this could potentially be the Initial disclosure.
        /// </summary>
        /// <value>Indicates whether this could potentially be the Initial disclosure.</value>
        public bool IsCandidateInitial
        {
            get
            {
                return this.isCandidateInitial;
            }

            set
            {
                this.isCandidateInitial = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the closing package is the preview.
        /// </summary>
        /// <value>If the closing package is the preview.</value>
        public bool IsPreview
        {
            get
            {
                return this.isPreview;
            }

            set
            {
                this.isPreview = value;
            }
        }

        /// <summary>
        /// Gets or sets the string representation of the <see cref="IsPreview"/> boolean.
        /// </summary>
        public string IsPreview_rep
        {
            get { return this.IsPreview.ToString(); }
            set { this.IsPreview = CfpbRequiredDateUtils.ToBool(value, defaultValue: this.IsPreview); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the closing package is final.
        /// </summary>
        /// <value>If the closing package is final.</value>
        public bool IsFinal
        {
            get
            {
                return this.isFinal;
            }

            set
            {
                this.isFinal = value;
            }
        }

        /// <summary>
        /// Gets or sets the string representation of the <see cref="IsFinal"/> boolean.
        /// </summary>
        public string IsFinal_rep
        {
            get { return this.IsFinal.ToString(); }
            set { this.IsFinal = CfpbRequiredDateUtils.ToBool(value, defaultValue: this.IsFinal); }
        }

        /// <summary>
        /// Gets or sets the last disclosed TRID loan product description.
        /// </summary>
        /// <value>The name of the loan product.</value>
        public string LastDisclosedTRIDLoanProductDescription
        {
            get
            {
                return this.lastDisclosedTRIDLoanProductDescription;
            }

            set
            {
                this.lastDisclosedTRIDLoanProductDescription = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the disclosure package was manually set.
        /// </summary>
        /// <value>Indicates if the disclosure package was manually set.</value>
        public bool IsManual
        {
            get
            {
                return this.isManual;
            }

            set
            {
                this.isManual = value;
            }
        }

        /// <summary>
        /// Gets or sets the ID of the associated document.
        /// </summary>
        /// <value>The ID of the associated document.</value>
        public Guid UcdDocument
        {
            get
            {
                return this.ucdDocument.HasValue ? this.ucdDocument.Value : Guid.Empty;
            }

            set
            {
                this.ucdDocument = value;
            }
        }

        /// <summary>
        /// Gets or sets the archive date.
        /// </summary>
        /// <value>The archive date.</value>
        public DateTime ArchiveDate
        {
            get
            {
                return CfpbRequiredDateUtils.ToDateTime(this.archiveDate, this.DefaultConvert);
            }

            set
            {
                this.archiveDate = CfpbRequiredDateUtils.DateTimeToString(value, this.DefaultConvert);
            }
        }

        /// <summary>
        /// Gets or sets the Id of the closing cost archive used.
        /// </summary>
        /// <value>The Id of the closing cost archive used.</value>
        [LqbInputModel(type: InputFieldType.DropDownList)]
        public Guid ArchiveId
        {
            get
            {
                return this.archiveId.HasValue ? this.archiveId.Value : Guid.Empty;
            }

            set
            {
                this.archiveId = value;
            }
        }

        /// <summary>
        /// Gets or sets the Transaction Id from when the docs were generated.
        /// </summary>
        /// <value>The Transaction Id.</value>
        public string TransactionId
        {
            get
            {
                return this.transactionId;
            }

            set
            {
                this.transactionId = value;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="LendersOffice.Integration.DocumentVendor.VendorConfig.VendorId"/> of the document vendor used to place the order.
        /// </summary>
        /// <value>The <see cref="LendersOffice.Integration.DocumentVendor.VendorConfig.VendorId"/> of the document vendor used to place the order.</value>
        public Guid? VendorId
        {
            get { return this.vendorId; }
            set { this.vendorId = value; }
        }

        /// <summary>
        /// Gets or sets the document code returned by the document vendor.
        /// </summary>
        /// <value>The document code returned by the document vendor.</value>
        public string DocCode
        {
            get { return this.docCode; }
            set { this.docCode = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user should be able to modify the archive id.
        /// </summary>
        /// <value>Indicates whether the user should be able to modify the archive id.</value>
        public bool DisableManualArchiveAssociation
        {
            get
            {
                return this.disableManualArchiveAssociation;
            }

            set
            {
                this.disableManualArchiveAssociation = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the disclosed APR is locked to allow manual data entry.
        /// </summary>
        /// <value>A value indicating whether the disclosed APR is locked to allow manual data entry.</value>
        public bool DisclosedAprLckd
        {
            get
            {
                return this.disclosedAprLckd;
            }

            set
            {
                this.disclosedAprLckd = value;
            }
        }

        /// <summary>
        /// Gets or sets the disclosed APR associated with the disclosure.
        /// </summary>
        /// <value>The disclosed APR associated with the disclosure.</value>
        public decimal? DisclosedApr
        {
            get
            {
                if (this.DisclosedAprLckd)
                {
                    return this.disclosedApr;
                }

                if (this.DocVendorApr.HasValue)
                {
                    return this.DocVendorApr;
                }

                return this.LqbApr;
            }

            set
            {
                this.disclosedApr = value;
            }
        }

        /// <summary>
        /// Gets or sets the string representation of the disclosed APR associated with the disclosure.
        /// </summary>
        /// <value>The string representation of the disclosed APR associated with the disclosure.</value>
        public string DisclosedApr_rep
        {
            get { return this.DefaultConvert.ToRateString(this.DisclosedApr); }
            set { this.DisclosedApr = this.DefaultConvert.ToRate(value); }
        }

        /// <summary>
        /// Gets the APR that LQB calculated for the disclosure.
        /// </summary>
        /// <value>The APR that LQB calculated for the disclosure.</value>
        public decimal? LqbApr
        {
            get
            {
                if (this.ArchiveId == Guid.Empty)
                {
                    return null;
                }

                var archive = this.getArchiveById(this.ArchiveId);
                return archive?.Apr;
            }
        }

        /// <summary>
        /// Gets the string representation of the APR that LQB calculated for the disclosure.
        /// </summary>
        /// <value>The APR that LQB calculated for the disclosure.</value>
        public string LqbApr_rep
        {
            get { return this.DefaultConvert.ToRateString(this.LqbApr); }
        }

        /// <summary>
        /// Gets or sets the APR that is returned by the document vendor.
        /// </summary>
        /// <remarks>
        /// We will maintain the precision returned by the document vendor but display the value
        /// rounded to 3 decimal places like the rest of the APR values.
        /// </remarks>
        /// <value>The APR that is returned by the document vendor.</value>
        public decimal? DocVendorApr
        {
            get
            {
                return this.docVendorApr;
            }

            set
            {
                this.docVendorApr = value;
            }
        }

        /// <summary>
        /// Gets or sets the string representation of the APR that is returned by the document vendor.
        /// </summary>
        /// <value>The string representation of the APR that is returned by the document vendor.</value>
        public string DocVendorApr_rep
        {
            get { return this.DefaultConvert.ToRateString(this.DocVendorApr); }
            set { this.DocVendorApr = this.DefaultConvert.ToRate(value); }
        }

        /// <summary>
        /// Gets or sets the date that the CD was signed.
        /// </summary>
        /// <value>The date that the CD was signed. DateTime.MinValue if not set.</value>
        public DateTime SignedDate
        {
            get
            {
                if (this.SignedDateLckd)
                {
                    return CfpbRequiredDateUtils.ToDate(this.signedDate, this.DefaultConvert);
                }

                var candidate = CfpbRequiredDateUtils.CalculateEarliestClosingDisclosureCandidate(
                    this.IsManual,
                    this.loanRescindableType,
                    this.alwaysRequireAllBorrowersToReceiveClosingDisclosure,
                    this.consumerTypesByConsumerId,
                    this.disclosureDatesByConsumerId,
                    date => date.SignedDate);

                return candidate?.SignedDate ?? DateTime.MinValue;
            }

            set
            {
                this.signedDate = CfpbRequiredDateUtils.DateToString(value, this.DefaultConvert);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the signed date is locked.
        /// </summary>
        public bool SignedDateLckd
        {
            get { return this.signedDateLckd || this.disclosureDatesByConsumerId.Count == 0; }
            set { this.signedDateLckd = value; }
        }
        
        /// <summary>
        /// Gets or sets the string representation of the signed date.
        /// </summary>
        public string SignedDate_rep
        {
            get { return CfpbRequiredDateUtils.DateToString(this.SignedDate, this.DefaultConvert); }
            set { this.SignedDate = CfpbRequiredDateUtils.ToDateTime(value, this.DefaultConvert); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the closing disclosure was issued after the loan's
        /// closing date. This is a calculated property and cannot be set.
        /// </summary>
        /// <value>
        /// Whether the closing disclosure was issued after the loan's
        /// closing date.
        /// </value>
        [DataMember]
        public bool IsPostClosing
        {
            get
            {
                if (!this.LoanClosingDate.HasValue)
                {
                    return false;
                }

                return this.IssuedDate.Date > this.LoanClosingDate.Value.Date;
            }

            // Needed for serialization :(.
            set
            {
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the closing disclosure was issued after the closing
        /// date due to a numerical change in amount paid by the borrower.
        /// </summary>
        /// <value>
        /// Whether the closing disclosure was issued after the closing
        /// date due to a numerical change in amount paid by the borrower.
        /// </value>
        public bool IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower
        {
            get
            {
                return this.isDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower;
            }

            set
            {
                this.isDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower = value;
            }
        }

        /// <summary>
        /// Gets or sets the string representation of the <see cref="IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower"/> field.
        /// </summary>
        public string IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower_rep
        {
            get { return this.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower.ToString(); }
            set { this.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower = CfpbRequiredDateUtils.ToBool(value, defaultValue: this.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the closing disclosure was issued after the closing
        /// date due to a numerical change in amount paid by the seller.
        /// </summary>
        /// <value>
        /// Whether the closing disclosure was issued after the closing
        /// date due to a numerical change in amount paid by the seller.
        /// </value>
        public bool IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller
        {
            get
            {
                return this.isDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller;
            }

            set
            {
                this.isDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller = value;
            }
        }

        /// <summary>
        /// Gets or sets the string representation of the <see cref="IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller"/> field.
        /// </summary>
        public string IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller_rep
        {
            get { return this.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller.ToString(); }
            set { this.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller = CfpbRequiredDateUtils.ToBool(value, defaultValue: this.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the closing disclosure was issued after the closing
        /// date due to a non-numerical clerical error.
        /// </summary>
        /// <value>
        /// Whether the closing disclosure was issued after the closing
        /// date due to a non-numerical clerical error.
        /// </value>
        public bool IsDisclosurePostClosingDueToNonNumericalClericalError
        {
            get
            {
                return this.isDisclosurePostClosingDueToNonNumericalClericalError;
            }

            set
            {
                this.isDisclosurePostClosingDueToNonNumericalClericalError = value;
            }
        }

        /// <summary>
        /// Gets or sets the string representation of the <see cref="IsDisclosurePostClosingDueToNonNumericalClericalError"/> field.
        /// </summary>
        public string IsDisclosurePostClosingDueToNonNumericalClericalError_rep
        {
            get { return this.IsDisclosurePostClosingDueToNonNumericalClericalError.ToString(); }
            set { this.IsDisclosurePostClosingDueToNonNumericalClericalError = CfpbRequiredDateUtils.ToBool(value, defaultValue: this.IsDisclosurePostClosingDueToNonNumericalClericalError); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the closing disclosure was issued after the closing
        /// date due to a cure for a tolerance violation.
        /// </summary>
        /// <value>
        /// Whether the closing disclosure was issued after the closing
        /// date due to a cure for a tolerance violation.
        /// </value>
        public bool IsDisclosurePostClosingDueToCureForToleranceViolation
        {
            get
            {
                return this.isDisclosurePostClosingDueToCureForToleranceViolation;
            }

            set
            {
                this.isDisclosurePostClosingDueToCureForToleranceViolation = value;
            }
        }

        /// <summary>
        /// Gets or sets the string representation of the <see cref="IsDisclosurePostClosingDueToCureForToleranceViolation_rep"/> field.
        /// </summary>
        public string IsDisclosurePostClosingDueToCureForToleranceViolation_rep
        {
            get { return this.IsDisclosurePostClosingDueToCureForToleranceViolation.ToString(); }
            set { this.IsDisclosurePostClosingDueToCureForToleranceViolation = CfpbRequiredDateUtils.ToBool(value, defaultValue: this.IsDisclosurePostClosingDueToCureForToleranceViolation); }
        }

        /// <summary>
        /// Gets or sets the post-consummation redisclosure reason date.
        /// </summary>
        /// <value>The date for a post-consummation redisclosure.</value>
        public DateTime PostConsummationRedisclosureReasonDate
        {
            get
            {
                return CfpbRequiredDateUtils.ToDate(this.postConsummationRedisclosureReasonDate, this.DefaultConvert);
            }

            set
            {
                this.postConsummationRedisclosureReasonDate = CfpbRequiredDateUtils.DateToString(value, this.DefaultConvert);
            }
        }

        /// <summary>
        /// Gets or sets the string representation of the post consummation redisclosure reason date.
        /// </summary>
        public string PostConsummationRedisclosureReasonDate_rep
        {
            get { return CfpbRequiredDateUtils.DateToString(this.PostConsummationRedisclosureReasonDate, this.DefaultConvert); }
            set { this.PostConsummationRedisclosureReasonDate = CfpbRequiredDateUtils.ToDateTime(value, this.DefaultConvert); }
        }

        /// <summary>
        /// Gets or sets the post-consummation knowledge-of-event date.
        /// </summary>
        /// <value>The date on which the creditor knew of an event requiring post-consummation redisclosure.</value>
        public DateTime PostConsummationKnowledgeOfEventDate
        {
            get
            {
                return CfpbRequiredDateUtils.ToDate(this.postConsummationKnowledgeOfEventDate, this.DefaultConvert);
            }

            set
            {
                this.postConsummationKnowledgeOfEventDate = CfpbRequiredDateUtils.DateToString(value, this.DefaultConvert);
            }
        }

        /// <summary>
        /// Gets or sets the string representation of the post consummation knowledge of event date.
        /// </summary>
        public string PostConsummationKnowledgeOfEventDate_rep
        {
            get { return CfpbRequiredDateUtils.DateToString(this.PostConsummationKnowledgeOfEventDate, this.DefaultConvert); }
            set { this.PostConsummationKnowledgeOfEventDate = CfpbRequiredDateUtils.ToDateTime(value, this.DefaultConvert); }
        }

        /// <summary>
        /// Gets the default converter for storage.
        /// </summary>
        /// <value>The default converter for storage.</value>
        private LosConvert DefaultConvert
        {
            get
            {
                if (this.defaultConvert == null)
                {
                    this.defaultConvert = new LosConvert();
                }

                return this.defaultConvert;
            }
        }

        /// <summary>
        /// Creates a <see cref="ClosingDisclosureDates"/> instance with the given archive
        /// retrieval function.
        /// </summary>
        /// <param name="metadata">The metadata for the construction of the class.</param>
        /// <returns>The <see cref="ClosingDisclosureDates"/> instance.</returns>
        public static ClosingDisclosureDates Create(ClosingDisclosureDatesMetadata metadata)
        {
            return new ClosingDisclosureDates(metadata);
        }

        /// <summary>
        /// Sets metadata for existing dates.
        /// </summary>
        /// <param name="metadata">
        /// The new metadata.
        /// </param>
        public void SetMetadata(ClosingDisclosureDatesMetadata metadata)
        {
            this.getArchiveById = metadata?.GetArchiveById;
            this.loanRescindableType = metadata?.LoanRescindableType ?? this.loanRescindableType;
            this.alwaysRequireAllBorrowersToReceiveClosingDisclosure = metadata?.AlwaysRequireAllBorrowersToReceiveClosingDisclosure 
                ?? this.alwaysRequireAllBorrowersToReceiveClosingDisclosure;

            if (metadata?.ConsumerMetadataByConsumerId != null)
            {
                foreach (var updatedConsumerMetadata in metadata.ConsumerMetadataByConsumerId)
                {
                    this.consumerTypesByConsumerId[updatedConsumerMetadata.ConsumerId] = updatedConsumerMetadata.ConsumerType;
                }
            }
        }

        /// <summary>
        /// Updates the consumer IDs stored for the dates.
        /// </summary>
        /// <param name="oldConsumerIdToNewConsumerIdMappings">
        /// The mappings between old consumer IDs and new consumer IDs.
        /// </param>
        public void UpdateConsumerIds(Dictionary<Guid, Guid> oldConsumerIdToNewConsumerIdMappings)
        {
            var newDisclosureDatesList = new List<KeyValuePair<Guid, BorrowerDisclosureDates>>(this.disclosureDatesByConsumerId.Count);

            foreach (var disclosureDatePair in this.disclosureDatesByConsumerId)
            {
                var updatedKey = oldConsumerIdToNewConsumerIdMappings[disclosureDatePair.Key];
                newDisclosureDatesList.Add(new KeyValuePair<Guid, BorrowerDisclosureDates>(updatedKey, disclosureDatePair.Value));
            }

            this.disclosureDatesByConsumerId = newDisclosureDatesList;
        }

        /// <summary>
        /// Creates a string for this object. Guarantees order for the properties.
        /// </summary>
        /// <returns>String representation of the object.</returns>
        public override string ToString()
        {
            string comp = "ClosingDisclosureDates_"
                + this.createdDate + "|"
                + this.deliveryMethod + "|"
                + this.receivedDate + "|"
                + this.archiveDate + "|"
                + this.isInitial + "|"
                + this.isPreview + "|"
                + this.isFinal + "|"
                + this.lastDisclosedTRIDLoanProductDescription + "|"
                + this.isManual + "|"
                + this.disableManualArchiveAssociation + "|"
                + this.IsPostClosing + "|"
                + this.isDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower + "|"
                + this.isDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller + "|"
                + this.isDisclosurePostClosingDueToNonNumericalClericalError + "|"
                + this.isDisclosurePostClosingDueToCureForToleranceViolation + "|"
                + this.postConsummationRedisclosureReasonDate + "|"
                + this.postConsummationKnowledgeOfEventDate + "|"
                + this.disclosedAprLckd + "|"
                + this.disclosedApr + "|"
                + this.docVendorApr + "|"
                + this.issuedDate + "|"
                + this.issuedDateLckd + "|"
                + this.deliveryMethodLckd + "|"
                + this.receivedDateLckd + "|"
                + this.signedDateLckd + "|"
                + this.loanRescindableType + "|"
                + this.disclosureDatesByConsumerId.Select(pair =>
                     pair.Key + "|" +
                     pair.Value.ConsumerName + "|" +
                     pair.Value.ExcludeFromCalculations + "|" +
                     pair.Value.IssuedDate + "|" +
                     pair.Value.DeliveryMethod + "|" +
                     pair.Value.ReceivedDate + "|" +
                     pair.Value.SignedDate) + "|"
                + this.consumerTypesByConsumerId.Select(pair => pair.Key + "|" + pair.Value) + "|"
                + this.alwaysRequireAllBorrowersToReceiveClosingDisclosure;

            return comp;
        }

        /// <summary>
        /// Equality for ClosingDisclosureDates. Compares all the fields for equality.
        /// </summary>
        /// <param name="obj">The date to check against.</param>
        /// <returns>True if all properties equal, false otherwise.</returns>
        public bool IsEqual(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            ClosingDisclosureDates otherDate = obj as ClosingDisclosureDates;
            if (otherDate != null)
            {
                if (this.CreatedDate.CompareTo(otherDate.CreatedDate) != 0)
                {
                    return false;
                }
                else if (this.IssuedDate.CompareTo(otherDate.IssuedDate) != 0)
                {
                    return false;
                }
                else if (this.DeliveryMethod != otherDate.DeliveryMethod)
                {
                    return false;
                }
                else if (this.IsFinal != otherDate.IsFinal)
                {
                    return false;
                }
                else if (this.IsInitial != otherDate.IsInitial)
                {
                    return false;
                }
                else if (!string.Equals(this.LastDisclosedTRIDLoanProductDescription, otherDate.LastDisclosedTRIDLoanProductDescription))
                {
                    return false;
                }
                else if (this.IsManual != otherDate.IsManual)
                {
                    return false;
                }
                else if (this.IsPreview != otherDate.IsPreview)
                {
                    return false;
                }
                else if (this.ReceivedDate.CompareTo(otherDate.ReceivedDate) != 0)
                {
                    return false;
                }
                else if (this.ArchiveId != otherDate.ArchiveId)
                {
                    return false;
                }
                else if (this.DisableManualArchiveAssociation != otherDate.DisableManualArchiveAssociation)
                {
                    return false;
                }
                else if (this.IsPostClosing != otherDate.IsPostClosing)
                {
                    return false;
                }
                else if (this.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower !=
                    otherDate.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower)
                {
                    return false;
                }
                else if (this.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller !=
                    otherDate.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller)
                {
                    return false;
                }
                else if (this.IsDisclosurePostClosingDueToNonNumericalClericalError !=
                    otherDate.IsDisclosurePostClosingDueToNonNumericalClericalError)
                {
                    return false;
                }
                else if (this.IsDisclosurePostClosingDueToCureForToleranceViolation !=
                    otherDate.IsDisclosurePostClosingDueToCureForToleranceViolation)
                {
                    return false;
                }
                else if (this.PostConsummationRedisclosureReasonDate.CompareTo(otherDate.PostConsummationRedisclosureReasonDate) != 0)
                {
                    return false;
                }
                else if (this.PostConsummationKnowledgeOfEventDate.CompareTo(otherDate.PostConsummationKnowledgeOfEventDate) != 0)
                {
                    return false;
                }
                else if (this.DisclosedAprLckd != otherDate.DisclosedAprLckd)
                {
                    return false;
                }
                else if (this.DisclosedApr != otherDate.DisclosedApr)
                {
                    return false;
                }
                else if (this.LqbApr != otherDate.LqbApr)
                {
                    return false;
                }
                else if (this.DocVendorApr != otherDate.DocVendorApr)
                {
                    return false;
                }
                else if (this.IssuedDateLckd != otherDate.IssuedDateLckd)
                {
                    return false;
                }
                else if (this.DeliveryMethodLckd != otherDate.DeliveryMethodLckd)
                {
                    return false;
                }
                else if (this.ReceivedDateLckd != otherDate.ReceivedDateLckd)
                {
                    return false;
                }
                else if (this.SignedDateLckd != otherDate.SignedDateLckd)
                {
                    return false;
                }
                else if (this.loanRescindableType != otherDate.loanRescindableType)
                {
                    return false;
                }
                else if (!CfpbRequiredDateUtils.AreBorrowerLevelDatesEqual(this.DisclosureDatesByConsumerId, otherDate.DisclosureDatesByConsumerId))
                {
                    return false;
                }
                else if (!CfpbRequiredDateUtils.AreConsumerTypeMappingsEqual(this.consumerTypesByConsumerId, otherDate.consumerTypesByConsumerId))
                {
                    return false;
                }
                else if (this.alwaysRequireAllBorrowersToReceiveClosingDisclosure != otherDate.alwaysRequireAllBorrowersToReceiveClosingDisclosure)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, "Unable to compare ClosingDisclosureDates.");
            }
        }

        /// <summary>
        /// Duplicates the current instance, maintaining the same UniqueId.
        /// </summary>
        /// <returns>The duplicate instance.</returns>
        public ClosingDisclosureDates DuplicatePreservingId()
        {
            var metadata = new ClosingDisclosureDatesMetadata(this.getArchiveById);
            return this.Duplicate(shouldGenerateNewId: false, metadata: metadata);
        }

        /// <summary>
        /// Duplicates the current instance, assigning a new UniqueId.
        /// </summary>
        /// <param name="duplicateGetArchiveById">The archive retrieval function for the duplicate.</param>
        /// <returns>The dupclicate instance.</returns>
        public ClosingDisclosureDates DuplicateAssigningNewId(Func<Guid, IClosingCostArchive> duplicateGetArchiveById)
        {
            var metadata = new ClosingDisclosureDatesMetadata(duplicateGetArchiveById);
            return this.Duplicate(shouldGenerateNewId: true, metadata: metadata);
        }

        /// <summary>
        /// Implements IPathResolvable.GetElement.
        /// </summary>
        /// <param name="element">The element to get.</param>
        /// <returns>The gotten element.</returns>
        object IPathResolvable.GetElement(IDataPathElement element)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Duplicate this closing disclosure dates.
        /// </summary>
        /// <param name="shouldGenerateNewId">Whether the duplicate date should have a new id or not.</param>
        /// <param name="metadata">The metadata used to construct the class.</param>
        /// <returns>A duplicate of this closing disclosure dates.</returns>
        private ClosingDisclosureDates Duplicate(bool shouldGenerateNewId, ClosingDisclosureDatesMetadata metadata)
        {
            ClosingDisclosureDates dupe = new ClosingDisclosureDates(metadata)
            {
                createdDate = this.createdDate,
                issuedDate = this.issuedDate,
                deliveryMethod = this.deliveryMethod,
                receivedDate = this.receivedDate,
                isInitial = this.isInitial,
                isPreview = this.isPreview,
                isFinal = this.isFinal,
                lastDisclosedTRIDLoanProductDescription = this.lastDisclosedTRIDLoanProductDescription,
                isManual = this.isManual,
                archiveDate = this.archiveDate,
                archiveId = this.archiveId,
                transactionId = this.transactionId,
                vendorId = this.vendorId,
                docCode = this.docCode,
                isDisclosurePostClosingDueToCureForToleranceViolation = this.isDisclosurePostClosingDueToCureForToleranceViolation,
                isDisclosurePostClosingDueToNonNumericalClericalError = this.isDisclosurePostClosingDueToNonNumericalClericalError,
                isDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower = this.isDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower,
                isDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller = this.isDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller,
                postConsummationRedisclosureReasonDate = this.postConsummationRedisclosureReasonDate,
                postConsummationKnowledgeOfEventDate = this.postConsummationKnowledgeOfEventDate,
                disclosedAprLckd = this.disclosedAprLckd,
                disclosedApr = this.disclosedApr,
                docVendorApr = this.docVendorApr,
                lqbApr = this.lqbApr,
                consumerTypesByConsumerId = new Dictionary<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, E_aTypeT>(this.consumerTypesByConsumerId),
                disclosureDatesByConsumerId = CfpbRequiredDateUtils.DeepCopyDisclosureDatesByConsumerId(this.disclosureDatesByConsumerId),
                alwaysRequireAllBorrowersToReceiveClosingDisclosure = this.alwaysRequireAllBorrowersToReceiveClosingDisclosure,
                issuedDateLckd = this.issuedDateLckd,
                deliveryMethodLckd = this.deliveryMethodLckd,
                receivedDateLckd = this.receivedDateLckd,
                signedDate = this.signedDate,
                signedDateLckd = this.signedDateLckd,
                loanRescindableType = this.loanRescindableType
            };

            if (!shouldGenerateNewId)
            {
                dupe.UniqueId = this.UniqueId;
            }

            return dupe;
        }

        /// <summary>
        /// Ensures that the serialized calculated fields have the most up-to-date value.
        /// </summary>
        /// <param name="context">The streaming context.</param>
        [OnSerializing]
        private void RefreshCalculatedFields(StreamingContext context)
        {
            this.disclosedApr = this.DisclosedApr;
            this.lqbApr = this.LqbApr;
            this.issuedDate = CfpbRequiredDateUtils.DateToString(this.IssuedDate, this.DefaultConvert);
            this.deliveryMethod = (int)this.DeliveryMethod;
            this.receivedDate = CfpbRequiredDateUtils.DateToString(this.ReceivedDate, this.DefaultConvert);
            this.signedDate = CfpbRequiredDateUtils.DateToString(this.SignedDate, this.DefaultConvert);
        }

        /// <summary>
        /// Handles the post-deserialization event.
        /// </summary>
        /// <param name="context">The streaming context.</param>
        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            if (this.issuedDate == null)
            {
                this.issuedDate = this.createdDate;
            }

            if (this.disclosureDatesByConsumerId == null)
            {
                this.disclosureDatesByConsumerId = new List<KeyValuePair<Guid, BorrowerDisclosureDates>>();
            }

            if (this.consumerTypesByConsumerId == null)
            {
                this.consumerTypesByConsumerId = new Dictionary<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, E_aTypeT>();
            }
        }
    }
}
