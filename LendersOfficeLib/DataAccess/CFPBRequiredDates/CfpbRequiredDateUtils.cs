﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides utilities for CFPB required dates.
    /// </summary>
    public static class CfpbRequiredDateUtils
    {
        /// <summary>
        /// Determines whether the borrower-level disclosure dates are equal between
        /// two instances.
        /// </summary>
        /// <param name="firstInstanceDisclosureDatesByConsumerId">
        /// The disclosure dates by consumer ID for the first instance.
        /// </param>
        /// <param name="secondInstanceDisclosureDatesByConsumerId">
        /// The disclosure dates by consumer ID for the second instance.
        /// </param>
        /// <returns>
        /// True if the borrower-level dates are equal, false otherwise.
        /// </returns>
        public static bool AreBorrowerLevelDatesEqual(
            ReadOnlyCollection<KeyValuePair<Guid, BorrowerDisclosureDates>> firstInstanceDisclosureDatesByConsumerId,
            ReadOnlyCollection<KeyValuePair<Guid, BorrowerDisclosureDates>> secondInstanceDisclosureDatesByConsumerId)
        {
            if (firstInstanceDisclosureDatesByConsumerId.Count != secondInstanceDisclosureDatesByConsumerId.Count)
            {
                return false;
            }

            if (firstInstanceDisclosureDatesByConsumerId.Count == 0 && secondInstanceDisclosureDatesByConsumerId.Count == 0)
            {
                return true;
            }

            var secondInstanceDisclosureDatesByConsumerIdDict = secondInstanceDisclosureDatesByConsumerId.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            foreach (var disclosureDatePair in firstInstanceDisclosureDatesByConsumerId)
            {
                BorrowerDisclosureDates otherDates;
                if (!secondInstanceDisclosureDatesByConsumerIdDict.TryGetValue(disclosureDatePair.Key, out otherDates))
                {
                    return false;
                }

                if (!disclosureDatePair.Value.Equals(otherDates))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Determines whether two dictionaries mapping consumer types to
        /// consumer IDs are equal.
        /// </summary>
        /// <param name="firstInstanceConsumerTypesByConsumerId">
        /// The consumer types by consumer ID for the first instance.
        /// </param>
        /// <param name="secondInstanceConsumerTypesByConsumerId">
        /// The consumer types by consumer ID for the second instance.
        /// </param>
        /// <returns>
        /// True if the mappings are equal, false otherwise.
        /// </returns>
        public static bool AreConsumerTypeMappingsEqual(
            Dictionary<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, E_aTypeT> firstInstanceConsumerTypesByConsumerId,
            Dictionary<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, E_aTypeT> secondInstanceConsumerTypesByConsumerId)
        {
            if (firstInstanceConsumerTypesByConsumerId.Count != secondInstanceConsumerTypesByConsumerId.Count)
            {
                return false;
            }

            if (firstInstanceConsumerTypesByConsumerId.Count == 0 && secondInstanceConsumerTypesByConsumerId.Count == 0)
            {
                return true;
            }

            foreach (var consumerMappingPair in firstInstanceConsumerTypesByConsumerId)
            {
                E_aTypeT secondInstanceValue;
                if (!secondInstanceConsumerTypesByConsumerId.TryGetValue(consumerMappingPair.Key, out secondInstanceValue))
                {
                    return false;
                }

                if (consumerMappingPair.Value != secondInstanceValue)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Converts a nullable date to its string representation.
        /// </summary>
        /// <param name="value">
        /// The nullable date.
        /// </param>
        /// <returns>
        /// The string representation.
        /// </returns>
        public static string NullableDateToString(DateTime? value)
        {
            if (value == null)
            {
                return null;
            }

            var date = CDateTime.Create(value);
            if (date.IsValid)
            {
                return date.ToString();
            }

            return null;
        }

        /// <summary>
        /// Converts a string to its nullable date representation.
        /// </summary>
        /// <param name="value">
        /// The string.
        /// </param>
        /// <returns>
        /// The nullable date representation.
        /// </returns>
        public static DateTime? StringToNullableDate(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            var date = CDateTime.Create(value, FormatTarget.Webform);
            if (date.IsValid)
            {
                return date.DateTimeForComputationWithTime;
            }

            return null;
        }

        /// <summary>
        /// Converts the DateTime to a string.
        /// </summary>
        /// <param name="dateToConvert">The DateTime to convert.</param>
        /// <param name="converter">A conversion object.</param>
        /// <returns>The string value.</returns>
        public static string DateToString(DateTime dateToConvert, LosConvert converter)
        {
            return converter.ToDateTimeString(dateToConvert);
        }

        /// <summary>
        /// Converts the DateTime to a string.
        /// </summary>
        /// <param name="dateToConvert">The DateTime to convert.</param>
        /// <param name="converter">A conversion object.</param>
        /// <returns>The string value.</returns>
        public static string DateToString(DateTime? dateToConvert, LosConvert converter)
        {
            if (dateToConvert.HasValue)
            {
                return DateToString(dateToConvert.Value, converter);
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Converts a string to DateTime using default converter.
        /// </summary>
        /// <param name="stringValue">The string to convert.</param>
        /// <param name="converter">A conversion object.</param>
        /// <returns>DateTime if converted, MinValue if not.</returns>
        public static DateTime ToDate(string stringValue, LosConvert converter)
        {
            CDateTime date = CDateTime.Create(stringValue, converter);
            if (date.IsValid)
            {
                return date.DateTimeForComputation;
            }
            else
            {
                return DateTime.MinValue;
            }
        }

        /// <summary>
        /// Converts the DateTime to a string.
        /// </summary>
        /// <param name="dateToConvert">The DateTime to convert.</param>
        /// <param name="converter">A conversion object.</param>
        /// <returns>The string value.</returns>
        public static string DateTimeToString(DateTime dateToConvert, LosConvert converter)
        {
            return converter.ToDateTimeString(dateToConvert, displayTime: true);
        }

        /// <summary>
        /// Converts a string to DateTime using default converter.
        /// </summary>
        /// <param name="stringValue">The string to convert.</param>
        /// <param name="converter">A conversion object.</param>
        /// <returns>DateTime if converted, MinValue if not.</returns>
        public static DateTime ToDateTime(string stringValue, LosConvert converter)
        {
            CDateTime date = CDateTime.Create(stringValue, converter);
            if (date.IsValid)
            {
                return date.DateTimeForComputationWithTime;
            }
            else
            {
                return DateTime.MinValue;
            }
        }

        /// <summary>
        /// Converts a string value to a boolean.
        /// </summary>
        /// <param name="boolString">A boolean string value.</param>
        /// <param name="defaultValue">A default value to use if the string cannot be parsed.</param>
        /// <returns>A boolean value.</returns>
        public static bool ToBool(string boolString, bool defaultValue)
        {
            bool parsedValue;
            if (bool.TryParse(boolString, out parsedValue))
            {
                return parsedValue;
            }
            else
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// Calculates the earliest loan estimate borrower-level disclosure
        /// dates candidate using values from the specific property getter.
        /// </summary>
        /// <param name="disclosureRowWasManuallyAdded">
        /// True if the disclosure row for the associated
        /// dates was manually added, false otherwise.
        /// </param>
        /// <param name="consumerTypesByConsumerId">
        /// The mapping from consumer ID to consumer type.
        /// </param>
        /// <param name="disclosureDatesByConsumerId">
        /// The mapping between consumer ID and disclosure dates.
        /// </param>
        /// <param name="propertyGetter">
        /// Function that, given a borrower disclosure date wrapper,
        /// returns a property to compute a minimum.
        /// </param>
        /// <returns>
        /// The disclosure dates with the earliest date 
        /// or null if the value could not be calculated.
        /// </returns>
        public static BorrowerDisclosureDates CalculateEarliestDisclosureDatesCandidate(
            bool disclosureRowWasManuallyAdded,
            Dictionary<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, E_aTypeT> consumerTypesByConsumerId,
            List<KeyValuePair<Guid, BorrowerDisclosureDates>> disclosureDatesByConsumerId,
            Func<BorrowerDisclosureDates, DateTime?> propertyGetter)
        {
            var validBorrowerTypes = new[] { E_aTypeT.Individual, E_aTypeT.CoSigner };
            var validBorrowersDatePairs = disclosureDatesByConsumerId
                .Where(datePair => DatePairIsValidForCalculation(datePair, disclosureRowWasManuallyAdded, consumerTypesByConsumerId, propertyGetter, validBorrowerTypes))
                .Select(datePair => datePair.Value);

            if (validBorrowersDatePairs.Any())
            {
                return validBorrowersDatePairs.MinBy(date => propertyGetter(date).Value);
            }

            return null;
        }

        /// <summary>
        /// Calculates the borrower-level closing disclosure dates based
        /// on the specified criteria.
        /// </summary>
        /// <param name="disclosureRowWasManuallyAdded">
        /// True if the disclosure row for the associated
        /// dates was manually added, false otherwise.
        /// </param>
        /// <param name="loanRescindableType">
        /// The rescindable type of the loan file.
        /// </param>
        /// <param name="alwaysRequireAllBorrowersToReceiveClosingDisclosure">
        /// Whether all borrowers are required to receive the CD for determining
        /// the received and signed dates.
        /// </param>
        /// <param name="consumerTypesByConsumerId">
        /// The mapping from consumer ID to consumer type.
        /// </param>
        /// <param name="disclosureDatesByConsumerId">
        /// The mapping between consumer ID and disclosure dates.
        /// </param>
        /// <param name="propertyGetter">
        /// Function that, given a borrower disclosure date wrapper,
        /// returns a property to compute a minimum.
        /// </param>
        /// <returns>
        /// The calculated borrower-level closing disclosure dates.
        /// </returns>
        public static BorrowerDisclosureDates CalculateEarliestClosingDisclosureCandidate(
            bool disclosureRowWasManuallyAdded,
            LoanRescindableT loanRescindableType,
            bool alwaysRequireAllBorrowersToReceiveClosingDisclosure,
            Dictionary<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, E_aTypeT> consumerTypesByConsumerId,
            List<KeyValuePair<Guid, BorrowerDisclosureDates>> disclosureDatesByConsumerId,
            Func<BorrowerDisclosureDates, DateTime?> propertyGetter)
        {
            var requireAllDatesSpecified = alwaysRequireAllBorrowersToReceiveClosingDisclosure || loanRescindableType != LoanRescindableT.NotRescindable;
            if (requireAllDatesSpecified)
            {
                // If any valid borrower or non-valid borrower has a blank date and is not excluded, return blank.
                if (disclosureDatesByConsumerId.Any(datePair => !propertyGetter(datePair.Value).HasValue && 
                    !(disclosureRowWasManuallyAdded && datePair.Value.ExcludeFromCalculations)))
                {
                    return null;
                }
            }

            var validBorrowerTypes = new[] { E_aTypeT.Individual, E_aTypeT.CoSigner };
            var validBorrowersDatePairs = disclosureDatesByConsumerId
                .Where(datePair =>
                {
                    return ClosingDisclosureDatePairIsValidForCalculation(
                        datePair,
                        disclosureRowWasManuallyAdded,
                        consumerTypesByConsumerId,
                        propertyGetter,
                        requireAllDatesSpecified,
                        validBorrowerTypes);
                })
                .Select(datePair => datePair.Value);

            if (requireAllDatesSpecified)
            {
                return validBorrowersDatePairs.MaxBy(date => propertyGetter(date).Value);
            }
            else
            {
                return validBorrowersDatePairs.MinBy(date => propertyGetter(date).Value);
            }
        }

        /// <summary>
        /// Deep copies a list mapping consumer IDs to disclosure dates.
        /// </summary>
        /// <param name="source">
        /// The source dictionary.
        /// </param>
        /// <returns>
        /// The deep copy result.
        /// </returns>
        public static List<KeyValuePair<Guid, BorrowerDisclosureDates>> DeepCopyDisclosureDatesByConsumerId(
            IReadOnlyCollection<KeyValuePair<Guid, BorrowerDisclosureDates>> source)
        {
            return source.Select(disclosureDatesPair =>
            {
                var consumerId = disclosureDatesPair.Key;
                var duplicateBorrowerDates = new BorrowerDisclosureDates()
                {
                    ConsumerName = disclosureDatesPair.Value.ConsumerName,
                    ExcludeFromCalculations = disclosureDatesPair.Value.ExcludeFromCalculations,
                    IssuedDate = disclosureDatesPair.Value.IssuedDate,
                    DeliveryMethod = disclosureDatesPair.Value.DeliveryMethod,
                    ReceivedDate = disclosureDatesPair.Value.ReceivedDate,
                    SignedDate = disclosureDatesPair.Value.SignedDate
                };

                return new KeyValuePair<Guid, BorrowerDisclosureDates>(consumerId, duplicateBorrowerDates);
            }).ToList();
        }

        /// <summary>
        /// Determines whether a date pair is valid for an earliest candidate calculation.
        /// </summary>
        /// <param name="datePair">
        /// The consumer ID - disclosure dates pair.
        /// </param>
        /// <param name="disclosureRowWasManuallyAdded">
        /// True if the disclosure row for the associated
        /// dates was manually added, false otherwise.
        /// </param>
        /// <param name="consumerTypesByConsumerId">
        /// The mapping from consumer ID to consumer type.
        /// </param>
        /// <param name="propertyGetter">
        /// Function that, given a borrower disclosure date wrapper,
        /// returns a property to compute a minimum.
        /// </param>
        /// <param name="validBorrowerTypes">
        /// The list of valid borrower types for the calculation.
        /// </param>
        /// <returns>
        /// True if the date pair is valid, false otherwise.
        /// </returns>
        private static bool DatePairIsValidForCalculation(
            KeyValuePair<Guid, BorrowerDisclosureDates> datePair,
            bool disclosureRowWasManuallyAdded,
            Dictionary<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, E_aTypeT> consumerTypesByConsumerId,
            Func<BorrowerDisclosureDates, DateTime?> propertyGetter,
            E_aTypeT[] validBorrowerTypes)
        {
            if (disclosureRowWasManuallyAdded && datePair.Value.ExcludeFromCalculations)
            {
                return false;
            }

            if (!propertyGetter(datePair.Value).HasValue)
            {
                return false;
            }

            if (disclosureRowWasManuallyAdded)
            {
                // Assume that borrowers added to the LE or CD row are "valid" 
                // borrowers whose dates will contribute to the calculations.
                return true;
            }

            var key = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(datePair.Key);

            E_aTypeT borrowerType;
            if (!consumerTypesByConsumerId.TryGetValue(key, out borrowerType))
            {
                return false;
            }

            return borrowerType.EqualsOneOf(validBorrowerTypes);
        }

        /// <summary>
        /// Determines whether a closing disclosure date pair is valid for an earliest candidate calculation.
        /// </summary>
        /// <param name="datePair">
        /// The consumer ID - disclosure dates pair.
        /// </param>
        /// <param name="disclosureRowWasManuallyAdded">
        /// True if the disclosure row for the associated
        /// dates was manually added, false otherwise.
        /// </param>
        /// <param name="consumerTypesByConsumerId">
        /// The mapping from consumer ID to consumer type.
        /// </param>
        /// <param name="propertyGetter">
        /// Function that, given a borrower disclosure date wrapper,
        /// returns a property to compute a minimum.
        /// </param>
        /// <param name="requireAllDatesSpecified">
        /// True if values for all dates must be specified, false otherwise.
        /// </param>
        /// <param name="validBorrowerTypes">
        /// The list of valid borrower types for the calculation.
        /// </param>
        /// <returns>
        /// True if the date pair is valid, false otherwise.
        /// </returns>
        private static bool ClosingDisclosureDatePairIsValidForCalculation(
            KeyValuePair<Guid, BorrowerDisclosureDates> datePair,
            bool disclosureRowWasManuallyAdded,
            Dictionary<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, E_aTypeT> consumerTypesByConsumerId,
            Func<BorrowerDisclosureDates, DateTime?> propertyGetter,
            bool requireAllDatesSpecified,
            E_aTypeT[] validBorrowerTypes)
        {
            if (disclosureRowWasManuallyAdded && datePair.Value.ExcludeFromCalculations)
            {
                return false;
            }

            if (!propertyGetter(datePair.Value).HasValue)
            {
                return false;
            }

            if (requireAllDatesSpecified || disclosureRowWasManuallyAdded)
            {
                // When all dates are required to be specified,
                // valid type is ignored in the calculation. Also
                // assume that borrowers added to the LE or CD row 
                // are "valid" borrowers whose dates will contribute 
                // to the calculations.
                return true;
            }

            var key = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(datePair.Key);

            E_aTypeT borrowerType;
            if (!consumerTypesByConsumerId.TryGetValue(key, out borrowerType))
            {
                return false;
            }

            return borrowerType.EqualsOneOf(validBorrowerTypes);
        }
    }
}
