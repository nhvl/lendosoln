﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using LendersOffice.Common.SerializationTypes;

    /// <summary>
    /// Provides a wrapper for the metadata required
    /// to construct a CFPB required date.
    /// </summary>
    public abstract class CfpbRequiredDatesMetadata
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CfpbRequiredDatesMetadata"/> class.
        /// </summary>
        /// <param name="getArchiveById">
        /// The archive retrieval function.
        /// </param>
        /// <param name="consumerMetadataByConsumerId">
        /// The consumer metadata for borrower-level disclosure tracking.
        /// </param>
        /// <param name="loanRescindableType">
        /// The rescindable type of the loan file.
        /// </param>
        protected CfpbRequiredDatesMetadata(
            Func<Guid, IClosingCostArchive> getArchiveById, 
            IReadOnlyList<BorrowerDisclosureMetadata> consumerMetadataByConsumerId, 
            LoanRescindableT loanRescindableType)
        {
            this.GetArchiveById = getArchiveById;
            this.ConsumerMetadataByConsumerId = consumerMetadataByConsumerId;
            this.LoanRescindableType = loanRescindableType;
        }

        /// <summary>
        /// Gets the archive retrieval function.
        /// </summary>
        public Func<Guid, IClosingCostArchive> GetArchiveById { get; private set; }

        /// <summary>
        /// Gets the consumer metadata for borrower-level disclosure tracking.
        /// </summary>
        public IReadOnlyList<BorrowerDisclosureMetadata> ConsumerMetadataByConsumerId { get; private set; }

        /// <summary>
        /// Gets the rescindable type of the loan file.
        /// </summary>
        public LoanRescindableT LoanRescindableType { get; private set; }
    }
}