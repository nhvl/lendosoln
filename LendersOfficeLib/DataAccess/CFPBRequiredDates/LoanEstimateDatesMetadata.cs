﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using LendersOffice.Common.SerializationTypes;

    /// <summary>
    /// Provides a wrapper for the metadata required
    /// to construct a loan estimate date.
    /// </summary>
    public class LoanEstimateDatesMetadata : CfpbRequiredDatesMetadata
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoanEstimateDatesMetadata"/> class.
        /// </summary>
        /// <param name="getArchiveById">
        /// The archive retrieval function.
        /// </param>
        public LoanEstimateDatesMetadata(Func<Guid, IClosingCostArchive> getArchiveById)
            : this(getArchiveById, consumerMetadataByConsumerId: null, loanRescindableType: LoanRescindableT.Blank)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanEstimateDatesMetadata"/> class.
        /// </summary>
        /// <param name="getArchiveById">
        /// The archive retrieval function.
        /// </param>
        /// <param name="consumerMetadataByConsumerId">
        /// The consumer metadata for borrower-level disclosure tracking.
        /// </param>
        /// <param name="loanRescindableType">
        /// The rescindable type of the loan file.
        /// </param>
        public LoanEstimateDatesMetadata(
            Func<Guid, IClosingCostArchive> getArchiveById,
            IReadOnlyList<BorrowerDisclosureMetadata> consumerMetadataByConsumerId,
            LoanRescindableT loanRescindableType) : base(getArchiveById, consumerMetadataByConsumerId, loanRescindableType)
        {
        }
    }
}
