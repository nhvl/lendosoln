﻿namespace DataAccess
{
    using System;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides a wrapper for datapoints related to borrower-level disclosures.
    /// </summary>
    public class BorrowerDisclosureMetadata
    {
        /// <summary>
        /// Gets or sets the ID of the consumer.
        /// </summary>
        public DataObjectIdentifier<DataObjectKind.Consumer, Guid> ConsumerId { get; set; }

        /// <summary>
        /// Gets or sets the name of the consumer.
        /// </summary>
        public string ConsumerName { get; set; }

        /// <summary>
        /// Gets or sets the consumer's type.
        /// </summary>
        public E_aTypeT ConsumerType { get; set; }
    }
}
