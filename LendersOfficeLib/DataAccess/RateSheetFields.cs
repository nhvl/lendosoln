using System;
using System.Data;
using LendersOfficeApp.los.RatePrice;

namespace DataAccess
{
    public enum E_RateSheetKeywordT 
    {
        Normal = 0,
        Lock = 1,
        Prepay = 2,
        IO = 3
    }

    /// <summary>
    /// See Case 2095: Enhance engine to support lockdays and prepay values in ratesheet.
    /// Sample import rate:
    /// LOCK,0,15
    /// PP,30,45
    /// 7%,-2,0
    /// 6.875%,-1.5,0
    /// 6.75%,-1,0
    /// 6.625%,-.5,0
    /// 6.5%,0,0

    /// LOCK,16,30
    /// PP,60,75
    /// 7%,-2,0
    /// 6.875%,-1.5,0
    /// 6.75%,-1,0
    /// 6.625%,-.5,0
    /// 6.5%,0,0
    /// </summary>
    public class CRateSheetFields : CXmlRecordBaseOld, IRateItem
	{
		private CLoanProductBase m_parent;

		// Pass -1 for rowApp to add new row.
		public CRateSheetFields( CLoanProductBase parent, DataSet ds, Guid recordId )
			: base( parent, ds, recordId, "RateSheetXmlContent" )
		{
			m_parent = parent;
		}

		public CRateSheetFields( CLoanProductBase parent, DataSet ds, int recordIndex )
			: base( parent, ds, recordIndex, "RateSheetXmlContent" )
		{
			m_parent = parent;
		}

		override public void Update()
		{
			base.Update();

			m_parent.Set_lRateSheetDataSetWithoutDeltas( m_ds ); //ThienChange
		}
		
        /// <summary>
        /// Detect if this entry is LOCK, PP, IO special keyword or just regular rate entry.
        /// </summary>
        public bool IsSpecialKeyword 
        {
            get 
            {
                string str = this.GetDescString("Rate");
                return str == "LOCK" || str == "PP" || str == "IO" ;
            }
        }

        /// <summary>
        /// Should only invoke this property when IsSpecialKeyword == true
        /// </summary>
        public E_RateSheetKeywordT RateSheetKeyword 
        {
            get 
            {
                switch (this.GetDescString("Rate")) 
                {
                    case "LOCK": return E_RateSheetKeywordT.Lock;
                    case "PP": return E_RateSheetKeywordT.Prepay;
                    case "IO": return E_RateSheetKeywordT.IO;
                    default:
                        throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Unsupport [" + this.GetDescString("Rate") + "] for RateSheetKeyword");
                }
            }
        }

        /// <summary>
        /// Should only invoke this property when IsSpecialKeyword == true
        /// </summary>
        public int LowerLimit 
        {
            get { return this.GetCount("Point"); }
        }

        /// <summary>
        /// Should only invoke this property when IsSpecialKeyword == true
        /// </summary>
        public int UpperLimit 
        {
            get { return GetCount("Margin"); }
        }

		public decimal Rate
		{
			get { return GetRate( "Rate" );} 
			set { SetRate( "Rate", value ); }
		}
		
		public string Rate_rep
		{
			get 
            {
                if (IsSpecialKeyword) 
                {
                    return GetDescString("Rate"); 
                } 
                else 
                {
                    return string.Format("{0:N3}%", this.Rate);
                }
            }
			set { Rate = m_convertLos.ToRate(value); }
		}

		public decimal Point
		{
			get { return this.GetRate( "Point" ); } 
			set { SetRate( "Point", value ); }
		}
		
		public string Point_rep
		{
			get 
            { 
                if (IsSpecialKeyword) 
                {
                    return GetDescString("Point"); 
                } 
                else 
                {
                    return string.Format("{0:N3}%", this.Point);
                }
            }
			set { Point = m_convertLos.ToRate(value); }
		}
		
		public decimal Margin
		{
			get { return GetRate( "Margin" );} 
			set { SetRate( "Margin", value ); }
		}
		
		public string Margin_rep
		{
			get 
            {
                if (IsSpecialKeyword) 
                {
                
                    return GetDescString("Margin"); 
                } 
                else 
                {
                    return string.Format("{0:N3}%", this.Margin);
                }
            }
			set { Margin = m_convertLos.ToRate(value); }
		}

		public decimal QRateBase
		{
			get { return GetRate( "QRateBase" );} 
			set { SetRate( "QRateBase", value ); }
		}
		
		public string QRateBase_rep
		{
			get 
			{
				if (IsSpecialKeyword) 
				{
					return GetDescString("QRateBase"); 
				} 
				else 
				{
					return string.Format("{0:N3}%", this.QRateBase);
				}
			}
		}

		public decimal TeaserRate
		{
			get { return this.GetRate( "TeaserRate" );} 
			set { SetRate( "TeaserRate", value ); }
		}
		
		public string TeaserRate_rep
		{
			get 
			{
				if (IsSpecialKeyword) 
				{
					return GetDescString("TeaserRate"); 
				} 
				else 
				{
					return string.Format("{0:N3}%", this.TeaserRate);
				}
			}
		}
        public override Enum KeyType 
        {
            get { return E_XmlRecordBaseKeyType.Undefined; }
            set {}
        }


	}


	public class CRateSheetFieldsReadOnly : CRateSheetFields
	{
		// Pass -1 for rowApp to add new row.
		public CRateSheetFieldsReadOnly( CLoanProductBase parent, DataSet ds, Guid recordId )
			: base( parent, ds, recordId )
		{}

		public CRateSheetFieldsReadOnly( CLoanProductBase parent, DataSet ds, int recordIndex )
			: base( parent, ds, recordIndex )
		{}

		override public void Update()
		{
			throw new CBaseException( LendersOffice.Common.ErrorMessages.RateOptionUpdateError,
				"CRateSheetFieldsReadOnly does not support Update() method. Rateoption modification fails." );
		}
	}
}