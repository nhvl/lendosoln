﻿namespace DataAccess
{
    using System;
    using System.Collections;
    using System.Data;
    using DataAccess;

    public class CPmlLoanSummaryData : CPmlPageData
    {
        private static CSelectStatementProvider s_selectProvider;

        static CPmlLoanSummaryData()
        {
            StringList list = new StringList();

            #region Target Fields
            list.Add("sLNm");
            list.Add("aBNm");
            list.Add("sLAmtCalc");
            list.Add("sLtvR");
            list.Add("sCltvR");
            list.Add("sCreditScoreType1");
            list.Add("sCreditScoreType2");
            list.Add("sQualTopR");
            list.Add("sQualBottomR");
            list.Add("sHcltvR");
            #endregion

            s_selectProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection);

        }

        public CPmlLoanSummaryData(Guid fileId)
            : base(fileId, "CPmlLoanSummaryData", s_selectProvider)
        {
        }
    }

    // Hack
    public class CPmlLoanSummary_TransformData : CPmlPageData
    {
        private static CSelectStatementProvider s_selectProvider;

        private Hashtable m_modifiedFieldsHash = new Hashtable(20);

        static CPmlLoanSummary_TransformData()
        {
            StringList list = new StringList();

            #region Target Fields
            list.Add("sLNm");
            list.Add("aBNm");
            list.Add("sLAmtCalc");
            list.Add("sLtvR");
            list.Add("sCltvR");
            list.Add("sCreditScoreType1");
            list.Add("sCreditScoreType2");
            list.Add("sQualTopR");
            list.Add("sQualBottomR");
            list.Add("sHcltvR");
            list.Add("sfInitNewLoanForPml");
            list.Add("sfTransformDataToPml");
            list.Add("sfListModifyPmlData");
            list.Add("sHasNonOccupantCoborrower");
            list.Add("sHasNonOccupantCoborrowerPe");
            list.Add("sfRecalculateClosingFeeConditions");
            #endregion

            s_selectProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection);

        }

        public CPmlLoanSummary_TransformData(Guid fileId)
            : base(fileId, "CPmlLoanSummary_TransformData", s_selectProvider)
        {
        }
        protected override bool m_enforceAccessControl
        {
            get { return false; }
        }

    }

}
