using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Data.Common;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOffice.UI;
using LqbGrammar.DataTypes;

namespace DataAccess
{
	public abstract class CAssetSpecial : CAssetFields, IAssetSpecial
    {
		/// <summary>
		/// Create new record
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="ds"></param>
		/// <param name="assetColl"></param>
		protected CAssetSpecial( CAppBase parent, DataSet ds, CAssetCollection assetColl, E_AssetSpecialT assetT )
			: base( parent, ds, assetColl )
		{
			AssetT = assetT;
		}

		
		/// <summary>
		/// Reconstruct record from existing data
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="ds"></param>
		/// <param name="assetColl"></param>
		protected CAssetSpecial( CAppBase parent, DataSet ds, CAssetCollection assetColl, int iRow, E_AssetSpecialT assetT )
			: base( parent, ds, assetColl, iRow )
		{
			AssetT = assetT;
		}

		/// <summary>
		/// Reconstruct record from existing data
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="ds"></param>
		/// <param name="assetColl"></param>
		protected CAssetSpecial( CAppBase parent, DataSet ds, CAssetCollection assetColl, Guid id, E_AssetSpecialT assetT )
			: base( parent, ds, assetColl, id )
		{
			AssetT = assetT;
		}

        // DO NOT USE!  CREATED JUST TO ENABLE SOME QUICK UNIT TESTING OF THE SHIM
        protected CAssetSpecial(DataSet data, LosConvert converter, Guid recordId)
            : base(data, converter, recordId)
        {

        }

        new public E_AssetSpecialT AssetT
		{
			get { return ( E_AssetSpecialT ) base.AssetT; }
			set { SetAssetT( (E_AssetT) value ); }
		}
		override public bool IsRegularType
		{
			get { return false; }
		}

        public override SimpleObjectInputFieldModel ToInputFieldModel()
        {
            return base.ToInputFieldModel();
        }

        public override void BindFromObjectModel(SimpleObjectInputFieldModel objInputFieldModel)
        {
            base.BindFromObjectModel(objInputFieldModel);
        }
	}

    public class CAssetCashDeposit : CAssetSpecial, IAssetCashDeposit
    {
		internal static IAssetCashDeposit CreateDeposit1( CAppBase parent, DataSet ds, CAssetCollection assetColl )
		{
			return new CAssetCashDeposit( parent, ds, assetColl,E_AssetCashDepositT.CashDeposit1 );
			
		}
		internal static IAssetCashDeposit CreateDeposit2( CAppBase parent, DataSet ds, CAssetCollection assetColl )
		{
			return new CAssetCashDeposit( parent, ds, assetColl,E_AssetCashDepositT.CashDeposit2 );
			
		}

		internal static IAssetCashDeposit Reconstruct( CAppBase parent, DataSet ds, CAssetCollection assetColl, int iRow )
		{
			return new CAssetCashDeposit( parent, ds, assetColl, iRow );
		}

        // DO NOT USE!  CREATED JUST TO ENABLE SOME QUICK UNIT TESTING OF THE SHIM
        public CAssetCashDeposit(DataSet data, LosConvert converter, Guid recordId)
            : base(data, converter, recordId)
        {
        }

        /// <summary>
        /// Create new record
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="ds"></param>
        private CAssetCashDeposit( CAppBase parent, DataSet ds, CAssetCollection assetColl, E_AssetCashDepositT depositT )
			: base( parent, ds, assetColl, E_AssetSpecialT.CashDeposit )
		{
			AssetCashDepositT = depositT;
		}


		// To create new, call static method Create() instead
		private CAssetCashDeposit( CAppBase parent, DataSet ds, CAssetCollection assetColl, int iRow )
			: base( parent, ds, assetColl, iRow, E_AssetSpecialT.CashDeposit )
		{
			DataRowCollection rows = ds.Tables[0].Rows;
			string thisRawVal = rows[iRow]["AssetCashDepositT"].ToString();
			if( thisRawVal != "" )
				return; // nothing special needs to be done, AssetCashDepositT should work fine.

			
			// Figure out what to Deposit type to give to record that didn't have deposit type stored.
			// This is the situation where it's read for the first time since we restructure the xml list code.

			int nRow = rows.Count;
			bool bFoundDeposit1 = false;
			bool bFoundDeposit2 = false;
			for( int i = 0; i < nRow; ++i )
			{
				if( i == iRow )
					continue;

				DataRow row = rows[i];
				E_AssetT assetT = (E_AssetT) int.Parse( row["AssetT"].ToString() );
				if( E_AssetT.CashDeposit == assetT )
				{
					string rawDepositT = row["AssetCashDepositT"].ToString();
					if( "" != rawDepositT  )
					{
						E_AssetCashDepositT depositT = (E_AssetCashDepositT) int.Parse( rawDepositT );
						switch( depositT )
						{
							case E_AssetCashDepositT.CashDeposit1:
								bFoundDeposit1 = true; break;
							case E_AssetCashDepositT.CashDeposit2: 
								bFoundDeposit2 = true; break;
						}
					}
				}
			}		
			if( !bFoundDeposit1 )
				AssetCashDepositT = E_AssetCashDepositT.CashDeposit1;
			else if( !bFoundDeposit2 )
				AssetCashDepositT = E_AssetCashDepositT.CashDeposit2;
			else
				IsOnDeathRow = true;
		}

		/// <summary>
		/// Valid only if AssetT is CashDeposit
		/// </summary>
		public E_AssetCashDepositT AssetCashDepositT
		{
			get
			{			
				return (E_AssetCashDepositT) GetEnum( "AssetCashDepositT", E_AssetCashDepositT.CashDeposit1 );
			}
			set{ SetEnum( "AssetCashDepositT", value );	}
		}

	}
	public class CAssetLifeIns : CAssetSpecial, IAssetLifeInsurance
    {
		static internal IAssetLifeInsurance Create( CAppBase parent, DataSet ds, CAssetCollection assetColl )
		{
			return new CAssetLifeIns( parent, ds, assetColl );
		}
		static internal IAssetLifeInsurance Reconstruct( CAppBase parent, DataSet ds, CAssetCollection assetColl, int iRow )
		{
			return new CAssetLifeIns( parent, ds, assetColl, iRow );
		}

        // DO NOT USE!  CREATED JUST TO ENABLE SOME QUICK UNIT TESTING OF THE SHIM
        public CAssetLifeIns(DataSet data, LosConvert converter, Guid recordId)
            : base(data, converter, recordId)
        {
        }

        /// <summary>
        /// Create new record
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="ds"></param>
        /// <param name="assetColl"></param>
        private CAssetLifeIns( CAppBase parent, DataSet ds, CAssetCollection assetColl )
			: base( parent, ds, assetColl, E_AssetSpecialT.LifeInsurance )
		{
		}

		
		/// <summary>
		/// Reconstruct record from existing data
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="ds"></param>
		/// <param name="assetColl"></param>
		private CAssetLifeIns( CAppBase parent, DataSet ds, CAssetCollection assetColl, int iRow )
			: base( parent, ds, assetColl, iRow,  E_AssetSpecialT.LifeInsurance )
		{
		}

		/// <summary>
		/// Reconstruct record from existing data
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="ds"></param>
		/// <param name="assetColl"></param>
		private CAssetLifeIns( CAppBase parent, DataSet ds, CAssetCollection assetColl, Guid id )
			: base( parent, ds, assetColl, id,  E_AssetSpecialT.LifeInsurance )
		{
		}
		public decimal FaceVal
		{
			get	{	return GetMoney( "FaceVal" );	}
			set {	SetMoney( "FaceVal", value ); }
		}

		public string FaceVal_rep
		{
			get { return m_convertLos.ToMoneyString(FaceVal, FormatDirection.ToRep); }
			set { FaceVal = m_convertLos.ToMoney(value); }
		}

        public override SimpleObjectInputFieldModel ToInputFieldModel()
        {
            SimpleObjectInputFieldModel objInputFieldModel = base.ToInputFieldModel();

            objInputFieldModel.Add("FaceVal", new InputFieldModel(this.FaceVal.ToString(), InputFieldType.Money));
            return objInputFieldModel;
        }

        public override void BindFromObjectModel(SimpleObjectInputFieldModel objInputFieldModel)
        {
            base.BindFromObjectModel(objInputFieldModel);
            
            if (objInputFieldModel == null)
            {
                return;
            }

            decimal tempDec;
            if (decimal.TryParse(objInputFieldModel.ValueFromModel("FaceVal"), out tempDec))
            {
                this.FaceVal = tempDec;
            }
        }
	}

	public class CAssetBusiness : CAssetSpecial, IAssetBusiness
	{
		static internal IAssetBusiness Create( CAppBase parent, DataSet ds, CAssetCollection assetColl )
		{
			return new CAssetBusiness( parent, ds, assetColl );
		}

		static internal IAssetBusiness Reconstruct( CAppBase parent, DataSet ds, CAssetCollection assetColl, int iRow )
		{
			return new CAssetBusiness( parent, ds, assetColl, iRow );
		}

        // DO NOT USE!  CREATED JUST TO ENABLE SOME QUICK UNIT TESTING OF THE SHIM
        public CAssetBusiness(DataSet data, LosConvert converter, Guid recordId)
            : base(data, converter, recordId)
        {
        }

        /// <summary>
        /// Create new record
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="ds"></param>
        /// <param name="assetColl"></param>
        private CAssetBusiness( CAppBase parent, DataSet ds, CAssetCollection assetColl )
			: base( parent, ds, assetColl, E_AssetSpecialT.Business )
		{
		}

		/// <summary>
		/// Reconstruct
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="ds"></param>
		/// <param name="assetColl"></param>
		/// <param name="iRow"></param>
		private CAssetBusiness( CAppBase parent, DataSet ds, CAssetCollection assetColl, int iRow )
			: base( parent, ds, assetColl, iRow, E_AssetSpecialT.Business )
		{
		}
	}


	public class CAssetRetirement : CAssetSpecial, IAssetRetirement
    {
		static internal IAssetRetirement Create( CAppBase parent, DataSet ds, CAssetCollection assetColl )
		{
			return new CAssetRetirement( parent, ds, assetColl );
		}

		static internal IAssetRetirement Reconstruct( CAppBase parent, DataSet ds, CAssetCollection assetColl, int iRow )
		{
			return new CAssetRetirement( parent, ds, assetColl, iRow );
		}
		/// <summary>
		/// Create new record
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="ds"></param>
		/// <param name="assetColl"></param>
		private CAssetRetirement( CAppBase parent, DataSet ds, CAssetCollection assetColl )
			: base( parent, ds, assetColl, E_AssetSpecialT.Retirement )
		{

		}
		/// <summary>
		/// Reconstruct
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="ds"></param>
		/// <param name="assetColl"></param>
		/// <param name="iRow"></param>
		private CAssetRetirement( CAppBase parent, DataSet ds, CAssetCollection assetColl, int iRow )
			: base( parent, ds, assetColl, iRow, E_AssetSpecialT.Retirement )
		{

		}

        // DO NOT USE!  CREATED JUST TO ENABLE SOME QUICK UNIT TESTING OF THE SHIM
        public CAssetRetirement(DataSet data, LosConvert converter, Guid recordId)
            : base(data, converter, recordId)
        {
        }

        public override bool H4HIsRetirement
        {
            get { return true; }
            set {}
        }

	}

	public class CAssetRegular : CAssetFields, IAssetRegular
    {
        static internal IAssetRegular Create(CAppBase parent, DataSet ds, CAssetCollection assetColl)
		{
			return new CAssetRegular( parent, ds, assetColl );
		}

        static internal IAssetRegular Recontruct(CAppBase parent, DataSet ds, CAssetCollection assetColl, int iRow)
		{
			return new CAssetRegular( parent, ds, assetColl, iRow );			
		}

        static internal IAssetRegular Recontruct(CAppBase parent, DataSet ds, CAssetCollection assetColl, Guid id)
		{
			return new CAssetRegular( parent, ds, assetColl, id );
		}

        // DO NOT USE!  CREATED JUST TO ENABLE SOME QUICK UNIT TESTING OF THE SHIM
        public CAssetRegular(DataSet data, LosConvert converter, Guid recordId)
            : base(data, converter, recordId)
        {
        }

		/// <summary>
		/// Create new record
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="ds"></param>
		/// <param name="assetColl"></param>
        private CAssetRegular(CAppBase parent, DataSet ds, CAssetCollection assetColl)
			: base( parent, ds, assetColl )
		{
			AssetT = E_AssetRegularT.Checking;
            AccNm = parent.aBNm; // Default account name with borrower name.
		}


        /// <summary>
        /// Reconstruct record from existing data
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="ds"></param>
        /// <param name="assetColl"></param>
        /// <param name="iRow"></param>
        private CAssetRegular(CAppBase parent, DataSet ds, CAssetCollection assetColl, int iRow)
			: base( parent, ds, assetColl, iRow )
		{
		}

        /// <summary>
        /// Construct record from existing data
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="ds"></param>
        /// <param name="assetColl"></param>
        /// <param name="id"></param>
        private CAssetRegular(CAppBase parent, DataSet ds, CAssetCollection assetColl, Guid id)
			: base( parent, ds, assetColl, id )
		{
		}

		/// <summary>
		/// Returns regular types, a subset of E_AssetT
		/// </summary>
		new public E_AssetRegularT AssetT
		{
			get { return (E_AssetRegularT) base.AssetT; }
			set { SetAssetT( (E_AssetT) value ); }
		}
		public string AssetT_rep
		{
			get { return DisplayStringForAssetT( AssetT, OtherTypeDesc ); }
		}
		public string Attention
		{
			get { return GetDescString( "Attention" ); }
			set { SetDescString( "Attention", value ); }
		}

        public E_GiftFundSourceT GiftSource
        {
            get
            {
                if (AssetT != E_AssetRegularT.GiftFunds)
                {

                    return E_GiftFundSourceT.Blank;
                }
                return (E_GiftFundSourceT) GetCount("GiftSource");
            }
            set
            {
                SetCount("GiftSource", (int)value);
            }
        }

		public CDateTime PrepD
		{
			get { return GetDateTime( "PrepD" ); }
			set { SetDateTime( "PrepD", value ) ; }
		}
		public string PrepD_rep
		{
			get { return PrepD.ToString(m_convertLos); }
			set { PrepD = CDateTime.Create(value, m_convertLos); }
		}
	
		public bool IsSeeAttachment
		{
			get { return GetBool( "IsSeeAttachment", true ); }
			set { SetBool( "IsSeeAttachment", value ); }
		}

        public Guid VerifSigningEmployeeId
        {
            get
            {
                return GetGuid("VerifSigningEmployeeId");
            }
            private set
            {
                SetGuid("VerifSigningEmployeeId", value);
            }
        }

        public string VerifSignatureImgId
        {
            get
            {
                return GetString("VerifSignatureImgId");
            }
            private set
            {
                SetString("VerifSignatureImgId", value);
            }
        }

        public bool VerifHasSignature
        {
            get
            {
                return false == string.IsNullOrEmpty(VerifSignatureImgId);
            }
        }

        public void ClearSignature()
        {
            VerifSignatureImgId = "";
            VerifSigningEmployeeId = Guid.Empty;
        }

        public void ApplySignature(Guid employeeId, string imgId)
        {
            VerifSignatureImgId = imgId;
            VerifSigningEmployeeId = employeeId;
        }
		
		static public string DisplayStringForAssetT( E_AssetRegularT assetT, string otherDesc )
		{
            return LendersOffice.CalculatedFields.Asset.DisplayStringForAssetT(
                assetT,
                otherDesc);
        }

		public override string SortValueFor( string fieldName )
		{
			switch( fieldName )
			{
				case "AssetT": return DisplayStringForAssetT( AssetT, OtherTypeDesc );
				default: return base.SortValueFor( fieldName );
			}
		}

		public CDateTime VerifExpD
		{
			get { return GetDateTime( "VerifExpD" ); }
			set { SetDateTime( "VerifExpD", value ); }
		}

		public string VerifExpD_rep
		{
			get { return VerifExpD.ToString(m_convertLos); }
			set { VerifExpD = CDateTime.Create(value, m_convertLos); }
		}

		public CDateTime VerifRecvD
		{
			get { return GetDateTime( "VerifRecvD" ); }
			set { SetDateTime( "VerifRecvD", value ); }
		}

		public string VerifRecvD_rep
		{
			get { return VerifRecvD.ToString(m_convertLos); }
			set { VerifRecvD = CDateTime.Create(value, m_convertLos); }
		}


		public CDateTime VerifSentD
		{
			get { return GetDateTime( "VerifSentD" ); }
			set { SetDateTime( "VerifSentD", value ); }
		}
		public string VerifSentD_rep
		{
			get { return VerifSentD.ToString(m_convertLos); }
			set { VerifSentD = CDateTime.Create(value, m_convertLos); }
		}

		public CDateTime VerifReorderedD
		{
			get { return GetDateTime( "VerifReorderedD" ); }
			set { SetDateTime( "VerifReorderedD", value ); }
		}
		public string VerifReorderedD_rep
		{
			get { return VerifReorderedD.ToString( m_convertLos ); }
			set { VerifReorderedD = CDateTime.Create(value, m_convertLos ); }
		}

		public string StAddr
		{
			get { return GetDescString( "StAddr" );}
			set { SetDescString( "StAddr", value ); }
		}

		public string City
		{
			get { return GetDescString("City" );}
			set { SetDescString( "City", value ); }

		}

		public string State
		{
			get { return GetState("State"); }
			set { SetState( "State", value ); }

		}

		public string Zip
		{
			get { return GetZipCode( "Zip" ); }
			set { SetZipCode( "Zip", value ); }
		}
		public Sensitive<string> AccNum
		{
            get { return GetDescString("AccNum"); }
            set { SetDescString("AccNum", value); }
        }


        public string AccNm
		{
			get { return GetDescString( "AccNm" );}
			set { SetDescString( "AccNm", value ); }
		}

		public string DepartmentName
		{
			get { return GetDescString( "DepartmentName" ); }
			set { SetDescString( "DepartmentName", value ); }
		}

		public string ComNm
		{
			get { return GetDescString( "ComNm" ); }
			set { SetDescString( "ComNm", value ); }
		}

		
		public string OtherTypeDesc
		{
			get { return GetDescString( "OtherTypeDesc" ); }
			set { SetDescString( "OtherTypeDesc", value ); }
		}


		override public void PrepareToFlush()
		{
			string desc = Desc;
			if( "" == desc )
				Desc = ComNm;

            if (this.IsEmptyCreated && (  this.Val > 0 || string.IsNullOrEmpty(this.Desc) == false || string.IsNullOrEmpty(this.ComNm) == false
                || string.IsNullOrEmpty(this.AccNum.Value) == false))
            {
                this.IsEmptyCreated = false;
            }
		}
		override public bool IsRegularType
		{
			get { return true; }
		}

        #region InputModelParse

        public override SimpleObjectInputFieldModel ToInputFieldModel()
        {
            SimpleObjectInputFieldModel objInputFieldModel = base.ToInputFieldModel();

            objInputFieldModel.Add("id", new InputFieldModel(RecordId.ToString(), InputFieldType.Unknown, readOnly: true));

            bool isAutoOrOther= (this.AssetT == E_AssetRegularT.Auto || this.AssetT == E_AssetRegularT.OtherIlliquidAsset || this.AssetT == E_AssetRegularT.OtherPurchaseCredit);
            bool isOtherAsset = (this.AssetT == E_AssetRegularT.OtherIlliquidAsset || this.AssetT == E_AssetRegularT.OtherLiquidAsset || this.AssetT == E_AssetRegularT.OtherPurchaseCredit);

            objInputFieldModel.Add("VerifExpD", new InputFieldModel(this.VerifRecvD, readOnly: isAutoOrOther));
            objInputFieldModel.Add("VerifRecvD", new InputFieldModel(this.VerifRecvD, readOnly: isAutoOrOther));
            objInputFieldModel.Add("VerifSentD", new InputFieldModel(this.VerifSentD, readOnly: isAutoOrOther));
            objInputFieldModel.Add("VerifReorderedD", new InputFieldModel(this.VerifReorderedD, readOnly: isAutoOrOther));

            objInputFieldModel.Add("AccNum", new InputFieldModel(this.AccNum, readOnly: isAutoOrOther));
            objInputFieldModel.Add("StAddr", new InputFieldModel(this.StAddr, readOnly: isAutoOrOther));
            objInputFieldModel.Add("City", new InputFieldModel(this.City, readOnly: isAutoOrOther));
            objInputFieldModel.Add("Zip", new InputFieldModel(this.Zip, readOnly: isAutoOrOther, customInputType: InputFieldType.Zipcode));
            objInputFieldModel.Add("AccNm", new InputFieldModel(this.AccNm, readOnly: isAutoOrOther));
            objInputFieldModel.Add("ComNm", new InputFieldModel(this.ComNm, readOnly: isAutoOrOther));
            objInputFieldModel.Add("OtherTypeDesc", new InputFieldModel(this.OtherTypeDesc, readOnly: !isOtherAsset));
            objInputFieldModel.Add("DepartmentName", new InputFieldModel(this.DepartmentName, readOnly: isAutoOrOther));

            objInputFieldModel.Add("State", new InputFieldModel(this.State, InputFieldType.DropDownList, options: EnumUtilities.GetValuesFromField("state", PrincipalFactory.CurrentPrincipal), readOnly: isAutoOrOther));
            objInputFieldModel.Add("AssetT", new InputFieldModel(this.AssetT));
            objInputFieldModel.Add("OwnerT", new InputFieldModel(this.OwnerT));
            objInputFieldModel.Add("GiftSource", new InputFieldModel(this.GiftSource, hidden: (this.AssetT != E_AssetRegularT.GiftFunds)));
            objInputFieldModel.Add("OrderRankValue", new InputFieldModel(this.OrderRankValue));

            return objInputFieldModel;
        }

        public override void BindFromObjectModel(SimpleObjectInputFieldModel objInputFieldModel)
        {
            base.BindFromObjectModel(objInputFieldModel);

            if (objInputFieldModel == null)
            {
                return;
            }

            this.AccNum = objInputFieldModel.ValueFromModel("AccNum");
            this.StAddr = objInputFieldModel.ValueFromModel("StAddr");
            this.City = objInputFieldModel.ValueFromModel("City");
            this.Zip = objInputFieldModel.ValueFromModel("Zip");
            this.AccNm = objInputFieldModel.ValueFromModel("AccNm");
            this.ComNm = objInputFieldModel.ValueFromModel("ComNm");
            this.OtherTypeDesc = objInputFieldModel.ValueFromModel("OtherTypeDesc");
            this.DepartmentName = objInputFieldModel.ValueFromModel("DepartmentName");
            this.State = objInputFieldModel.ValueFromModel("State");

            DateTime tempDate;
            if (DateTime.TryParse(objInputFieldModel.ValueFromModel("VerifExpD"), out tempDate))
            {
                this.VerifExpD = CDateTime.Create(tempDate);
            }

            if (DateTime.TryParse(objInputFieldModel.ValueFromModel("VerifRecvD"), out tempDate))
            {
                this.VerifRecvD = CDateTime.Create(tempDate);
            }

            if (DateTime.TryParse(objInputFieldModel.ValueFromModel("VerifSentD"), out tempDate))
            {
                this.VerifSentD = CDateTime.Create(tempDate);
            }

            if (DateTime.TryParse(objInputFieldModel.ValueFromModel("VerifReorderedD"), out tempDate))
            {
                this.VerifReorderedD = CDateTime.Create(tempDate);
            }

            int tempInt;
            if (int.TryParse(objInputFieldModel.ValueFromModel("AssetT"), out tempInt))
            {
                this.AssetT = (E_AssetRegularT)tempInt;
            }

            if (int.TryParse(objInputFieldModel.ValueFromModel("GiftSource"), out tempInt))
            {
                this.GiftSource = (E_GiftFundSourceT)tempInt;
            }

            if (int.TryParse(objInputFieldModel.ValueFromModel("OwnerT"), out tempInt))
            {
                this.OwnerT = (E_AssetOwnerT)tempInt;
            }
        }

        #endregion
    }

	abstract public class CAssetFields : CXmlRecordBase2, IAsset
    {
		/// <summary>
		/// Create new record
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="ds"></param>
        protected CAssetFields(CAppBase parent, DataSet ds, CAssetCollection assetColl)
			: base( parent, ds, assetColl, "AssetXmlContent" )
		{
			OwnerT = E_AssetOwnerT.Borrower;
		}

        // DO NOT USE!  CREATED JUST TO ENABLE SOME QUICK UNIT TESTING OF THE SHIM
        protected CAssetFields(DataSet data, LosConvert converter, Guid recordId)
            : base(data, converter, recordId)
        {

        }

        // To create new, call static method Create() instead
        protected CAssetFields(CAppBase parent, DataSet ds, CAssetCollection assetColl, int iRow)
			: base( parent, ds, assetColl, iRow, "AssetXmlContent" )
		{
		}
        protected CAssetFields(CAppBase parent, DataSet ds, CAssetCollection assetColl, Guid id)
			: base( parent, ds, assetColl, id, "AssetXmlContent" )
		{
		}
		public static bool IsTypeOfSpecialRecord( E_AssetT assetT )
		{
			switch( (E_AssetT) assetT )
			{
				case E_AssetT.CashDeposit:
				case E_AssetT.LifeInsurance:
				case E_AssetT.Retirement:
				case E_AssetT.Business:
					return true;
				default:
					return false;
			}
		}
        public int H4HNumOfJointOwners
        {
            get 
            { 
                int value = GetCount("H4HNumOfJointOwners");
                if (value <= 1)
                {
                    if (OwnerT == E_AssetOwnerT.Joint)
                    {
                        return 2; // 12/8/2009 dd - Default to 2 when it is a joint acocunt.
                    }
                    else
                    {
                        return 1;
                    }
                }
                return value;
            }
            set { SetCount("H4HNumOfJointOwners", value); }
        }
        public string H4HNumOfJointOwners_rep
        {
            get { return m_convertLos.ToCountString(H4HNumOfJointOwners); }
            set { H4HNumOfJointOwners = m_convertLos.ToCount(value); }
        }
        public virtual bool H4HIsRetirement
        {
            get { return GetBool("H4HIsRetirement", false); }
            set { SetBool("H4HIsRetirement", value); }
        }
		public decimal Val
		{
			get	{ return GetMoney( "Val" );	}
			set { SetMoney( "Val", value ); }
		}

		public string Val_rep
		{
			get { return m_convertLos.ToMoneyString(Val, FormatDirection.ToRep ); }
			set { Val = m_convertLos.ToMoney(value); }
		}

        public string PhoneNumber
        {
            get
            {
                return this.GetPhoneNum("PhoneNumber");
            }
            set
            {
                this.SetPhoneNum("PhoneNumber", value);
            }
        }

		public string Desc
		{
			get { return GetDescString( "Desc" ); }
			set { SetDescString( "Desc", value ); }
		}

		public E_AssetOwnerT OwnerT
		{
			get { return (E_AssetOwnerT) GetEnum("OwnerT", E_AssetOwnerT.Borrower); }
			set { SetEnum("OwnerT", value); }
		}
		public string OwnerT_rep
		{
			get { return DisplayStringForOwnerT( OwnerT ); }
		}
		static public string DisplayStringForOwnerT( E_AssetOwnerT ownerT )
		{
			switch( ownerT )
			{
				case E_AssetOwnerT.Borrower:	return "B";
				case E_AssetOwnerT.CoBorrower:	return "C";
				case E_AssetOwnerT.Joint:		return "J";
				default:						return "?";
			}
		}

		public bool IsLiquidAsset
		{
			get 
			{
				switch( AssetT )
				{
					case E_AssetT.Bonds:
					case E_AssetT.Checking:
					case E_AssetT.GiftFunds:
                    case E_AssetT.GiftEquity:
					case E_AssetT.LifeInsurance:
					case E_AssetT.Savings:
					case E_AssetT.Stocks:
					case E_AssetT.CashDeposit:
					case E_AssetT.OtherLiquidAsset:
                    case E_AssetT.PendingNetSaleProceedsFromRealEstateAssets:
                    case E_AssetT.BridgeLoanNotDeposited:
                    case E_AssetT.CertificateOfDeposit:
                    case E_AssetT.MoneyMarketFund:
                    case E_AssetT.MutualFunds:
                    case E_AssetT.SecuredBorrowedFundsNotDeposit:
                    case E_AssetT.TrustFunds:
						return true;
					default:
						return false;
				}
			}
		}
			
		public E_AssetT AssetT
		{
			get { return (E_AssetT) GetEnum("AssetT", E_AssetT.Checking); }
		}

		abstract public bool IsRegularType
		{
			get;
		}
		protected void SetAssetT( E_AssetT assetT )
		{
			SetEnum("AssetT", assetT);
		}

		/// <summary>
		/// This would be what subcollection of records will be based on.
		/// </summary>
		override public Enum KeyType
		{
			get { return AssetT; }
			set { SetAssetT( (E_AssetT) value ); }
		}

		override public void PrepareToFlush()
		{
			// no-op
		}
        public bool IsEmptyCreated
        {
            // 1/5/2012 dd - Add this attribute to indicate if the method AddRegularRecordAt or
            // AddRegularRecord are called and then Save without setting data.
            get { return GetBool("IsEmptyCreated"); }
            set { SetBool("IsEmptyCreated", value); }
        }

        #region JsonHandler

        /// <summary>
        /// Return then json input model of an asset.
        /// </summary>
        /// <returns>An Input field type of the asset.</returns>
        public virtual SimpleObjectInputFieldModel ToInputFieldModel()
        {
            SimpleObjectInputFieldModel objInputFieldModel = new SimpleObjectInputFieldModel();
            objInputFieldModel.Add("Val", new InputFieldModel(Val, customInputType : InputFieldType.Money));
            objInputFieldModel.Add("Desc", new InputFieldModel(Desc));
            return objInputFieldModel;
        }

        /// <summary>
        /// Update asset from an input field model.
        /// </summary>
        /// <param name="objInputFieldModel">Input field model to update asset.</param> 
        public virtual void BindFromObjectModel(SimpleObjectInputFieldModel objInputFieldModel)
        {
            if (objInputFieldModel == null)
            {
                return;
            }

            decimal tempDec;
            if (decimal.TryParse(objInputFieldModel.ValueFromModel("Val"), out tempDec))
            {
                this.Val = tempDec;
            }
            this.Desc = objInputFieldModel.ValueFromModel("Desc");
        }

        #endregion

        /// <summary>
        /// Determines whether the asset is credited at closing.  If credit at closing,
        /// assets will not be included in the asset totals.  Instead, they'll be added
        /// to the adjustments.
        /// </summary>
        public bool IsCreditAtClosing
        {
            get
            {
                return false;
            }
        }

	}
}