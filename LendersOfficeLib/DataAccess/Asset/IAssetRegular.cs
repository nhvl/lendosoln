﻿namespace DataAccess
{
    using System;
    using LendersOffice.UI;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Interface representing a generic, regular, asset.
    /// </summary>
    public interface IAssetRegular : IAsset
    {
        /// <summary>
        /// Gets or sets the account number for the asset.
        /// </summary>
        /// <value>The account number for the asset.</value>
        string AccNm { get; set; }

        /// <summary>
        /// Gets or sets the account number for the asset as a sensitive data type.
        /// </summary>
        /// <value>The account number for the asset as a sensitive data type.</value>
        Sensitive<string> AccNum { get; set; }

        /// <summary>
        /// Gets or sets the type of the asset.
        /// </summary>
        /// <value>The type of the asset.</value>
        new E_AssetRegularT AssetT { get; set; }

        /// <summary>
        /// Gets the string representation of the type of the asset.
        /// </summary>
        /// <value>The string representation of the type of the asset.</value>
        string AssetT_rep { get; }

        /// <summary>
        /// Gets or sets the name of the person who should be notified in refernce to this asset.
        /// </summary>
        /// <value>The person who should be notified in refernce to this asset.</value>
        string Attention { get; set; }

        /// <summary>
        /// Gets or sets the city in which the company associated with the asset is located.
        /// </summary>
        /// <value>The city in which the company associated with the asset is located.</value>
        string City { get; set; }

        /// <summary>
        /// Gets or sets the name of the company associated with the asset.
        /// </summary>
        /// <value>The name of the company associated with the asset.</value>
        string ComNm { get; set; }

        /// <summary>
        /// Gets or sets the name of the responsible department for the company associated with the asset.
        /// </summary>
        /// <value>The name of the responsible department for the company associated with the asset.</value>
        string DepartmentName { get; set; }

        /// <summary>
        /// Gets or sets the source of the asset when the asset was a gift.
        /// </summary>
        /// <value>The source of the asset when the asset was a gift.</value>
        E_GiftFundSourceT GiftSource { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether there is an attachment containing information about this asset.
        /// </summary>
        /// <value>A value indicating whether there is an attachment containing information about this asset.</value>
        bool IsSeeAttachment { get; set; }

        /// <summary>
        /// Gets or sets the description of the asset type when the asset type is Other.
        /// </summary>
        /// <value>The description of the asset type when the asset type is Other.</value>
        string OtherTypeDesc { get; set; }

        /// <summary>
        /// Gets or sets the date when the documenation for this asset was prepared.
        /// </summary>
        /// <value>The date when the documenation for this asset was prepared.</value>
        CDateTime PrepD { get; set; }

        /// <summary>
        /// Gets or sets the string representation of the date when the documenation for this asset was prepared.
        /// </summary>
        /// <value>The string representation of the date when the documenation for this asset was prepared.</value>
        string PrepD_rep { get; set; }

        /// <summary>
        /// Gets or sets the street address in which the company associated with the asset is located.
        /// </summary>
        /// <value>The street address in which the company associated with the asset is located.</value>
        string StAddr { get; set; }

        /// <summary>
        /// Gets or sets the state in which the company associated with the asset is located.
        /// </summary>
        /// <value>The state in which the company associated with the asset is located.</value>
        string State { get; set; }

        /// <summary>
        /// Gets or sets the date when this asset was verified.
        /// </summary>
        /// <value>The date when this asset was verified.</value>
        CDateTime VerifExpD { get; set; }

        /// <summary>
        /// Gets or sets the string representation of the date when this asset was verified.
        /// </summary>
        /// <value>The the string representation of the date when this asset was verified.</value>
        string VerifExpD_rep { get; set; }

        /// <summary>
        /// Gets a value indicating whether there is a signature on the verification of this asset.
        /// </summary>
        /// <value>A value indicating whether there is a signature on the verification of this asset.</value>
        bool VerifHasSignature { get; }

        /// <summary>
        /// Gets or sets the date when the verification for this asset was received.
        /// </summary>
        /// <value>The date when the verification for this asset was received.</value>
        CDateTime VerifRecvD { get; set; }

        /// <summary>
        /// Gets or sets the string representation of the date when the verification for this asset was received.
        /// </summary>
        /// <value>The string representation of the date when the verification for this asset was received.</value>
        string VerifRecvD_rep { get; set; }

        /// <summary>
        /// Gets or sets the date when the verification was reordered.
        /// </summary>
        /// <value>The date when the verification was reordered.</value>
        CDateTime VerifReorderedD { get; set; }

        /// <summary>
        /// Gets or sets the string representation of the date when the verification was reordered.
        /// </summary>
        /// <value>The string representation of the date when the verification was reordered.</value>
        string VerifReorderedD_rep { get; set; }

        /// <summary>
        /// Gets or sets the date when the verification request was sent.
        /// </summary>
        /// <value>The date when the verification request was sent.</value>
        CDateTime VerifSentD { get; set; }

        /// <summary>
        /// Gets or sets the string representation of the date when the verification request was sent.
        /// </summary>
        /// <value>The string representation of the date when the verification request was sent.</value>
        string VerifSentD_rep { get; set; }

        /// <summary>
        /// Gets the identifier for the image that holds the verification signature.
        /// </summary>
        /// <value>The identifier for the image that holds the verification signature.</value>
        string VerifSignatureImgId { get; }

        /// <summary>
        /// Gets the identifier for the employee that signed the verification.
        /// </summary>
        /// <value>The identifier for the employee that signed the verification.</value>
        Guid VerifSigningEmployeeId { get; }

        /// <summary>
        /// Gets or sets the zipcode in which the company associated with the asset is located.
        /// </summary>
        /// <value>The zipcode in which the company associated with the asset is located.</value>
        string Zip { get; set; }

        /// <summary>
        /// Attach a verification signature to this asset.
        /// </summary>
        /// <param name="employeeId">The identifier for the employee that signed the verification.</param>
        /// <param name="imgId">The identifier for the image containing the verification signature.</param>
        void ApplySignature(Guid employeeId, string imgId);

        /// <summary>
        /// Remove the verification signature from this asset.
        /// </summary>
        void ClearSignature();
    }
}