﻿namespace DataAccess
{
    /// <summary>
    /// Interface for an asset that is a life insurance policy.
    /// </summary>
    public interface IAssetLifeInsurance : IAssetSpecial
    {
        /// <summary>
        /// Gets or sets the face value of the insurance policy.
        /// </summary>
        /// <value>The face value of the insurance policy.</value>
        decimal FaceVal { get; set; }

        /// <summary>
        /// Gets or sets the string representation of the face value of the insurance policy.
        /// </summary>
        /// <value>The string representation of the face value of the insurance policy.</value>
        string FaceVal_rep { get; set; }
    }
}