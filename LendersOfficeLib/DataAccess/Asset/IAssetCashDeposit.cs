﻿namespace DataAccess
{
    /// <summary>
    /// Interface for a cash deposit asset.
    /// </summary>
    public interface IAssetCashDeposit : IAssetSpecial
    {
        /// <summary>
        /// Gets or sets the type of the cash deposit.
        /// </summary>
        /// <value>The type of the cash deposit.</value>
        E_AssetCashDepositT AssetCashDepositT { get; set; }
    }
}