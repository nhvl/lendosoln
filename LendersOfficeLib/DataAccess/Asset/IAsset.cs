﻿namespace DataAccess
{
    using System;
    using LendersOffice.UI;

    /// <summary>
    /// Interface for any asset.
    /// </summary>
    public interface IAsset : ICollectionItemBase2
    {
        /// <summary>
        /// Gets the type of asset.
        /// </summary>
        /// <value>The type of asset.</value>
        E_AssetT AssetT { get; }

        /// <summary>
        /// Gets or sets the text description of the asset.
        /// </summary>
        /// <value>The text description of the asset.</value>
        string Desc { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the asset has just been created and is still empty of data.
        /// </summary>
        /// <value>A value indicating whether the asset has just been created and is still empty of data.</value>
        bool IsEmptyCreated { get; set; }

        /// <summary>
        /// Gets a value indicating whether the asset can be quickly monetized.
        /// </summary>
        /// <value>A value indicating whether the asset can be quickly monetized.</value>
        bool IsLiquidAsset { get; }

        /// <summary>
        /// Gets a value indicating whether the asset is a generic, regular, type.
        /// </summary>
        /// <value>A value indicating whether the asset is a generic, regular, type.</value>
        bool IsRegularType { get; }

        /// <summary>
        /// Gets or sets the owner type of the asset.
        /// </summary>
        /// <value>The owner type of the asset.</value>
        E_AssetOwnerT OwnerT { get; set; }

        /// <summary>
        /// Gets the string representation of the owner type of the asset.
        /// </summary>
        /// <value>The string representation of the owner type of the asset.</value>
        string OwnerT_rep { get; }

        /// <summary>
        /// Gets or sets the phone number that can be used to verify the asset.
        /// </summary>
        /// <value>The phone number that can be used to verify the asset.</value>
        string PhoneNumber { get; set; }

        /// <summary>
        /// Gets or sets the value of the asset.
        /// </summary>
        /// <value>The value of the asset.</value>
        decimal Val { get; set; }

        /// <summary>
        /// Gets or sets the string representation of the value of the asset.
        /// </summary>
        /// <value>The string representation of the value of the asset.</value>
        string Val_rep { get; set; }

        /// <summary>
        /// Gets a value indicating whether the asset is credited at closing.  If credit at closing,
        /// assets will not be included in the asset totals.  Instead, they'll be added
        /// to the adjustments.
        /// </summary>
        bool IsCreditAtClosing { get; }

        /// <summary>
        /// Populate the asset's data from an input object model.
        /// </summary>
        /// <param name="objInputFieldModel">The input object model.</param>
        void BindFromObjectModel(SimpleObjectInputFieldModel objInputFieldModel);

        /// <summary>
        /// Gather the asset's data into an object model.
        /// </summary>
        /// <returns>An object model containing the asset's data.</returns>
        SimpleObjectInputFieldModel ToInputFieldModel();
    }
}