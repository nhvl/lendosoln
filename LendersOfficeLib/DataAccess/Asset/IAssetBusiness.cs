﻿namespace DataAccess
{
    /// <summary>
    /// Interface representing an asset that is an owned business.
    /// </summary>
    public interface IAssetBusiness : IAssetSpecial
    {
    }
}
