﻿namespace DataAccess
{
    /// <summary>
    /// Interface for an asset that is a retirement account.
    /// </summary>
    public interface IAssetRetirement : IAssetSpecial
    {
    }
}