﻿namespace DataAccess
{
    /// <summary>
    /// Interface for a special (not regular) asset.
    /// </summary>
    public interface IAssetSpecial : IAsset
    {
        /// <summary>
        /// Gets or sets the type of the special asset.
        /// </summary>
        /// <value>The type of the special asset.</value>
        new E_AssetSpecialT AssetT { get; set; }
    }
}