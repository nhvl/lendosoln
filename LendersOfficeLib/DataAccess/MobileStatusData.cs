﻿using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
    public class MobileStatusWithSecurityByPassData : CPageData
    {
        private static CSelectStatementProvider s_selectProvider;

        static MobileStatusWithSecurityByPassData()
        {
            StringList list = new StringList();

            #region Target Fields
            list.Add("sRLckdD");
            list.Add("sRLckdExpiredD");
            list.Add("sLeadD");
            list.Add("sOpenedD");
            list.Add("sPreQualD");
            list.Add("sSubmitD");
            list.Add("sProcessingD");
            list.Add("sPreApprovD");
            list.Add("sEstCloseD");
            list.Add("sUnderwritingD");
            list.Add("sApprovD");
            list.Add("sFinalUnderwritingD");
            list.Add("sClearToCloseD");
            list.Add("sDocsD");
            list.Add("sDocsBackD");
            list.Add("sFundingConditionsD");
            list.Add("sFundD");
            list.Add("sRecordedD");
            list.Add("sFinalDocsD");
            list.Add("sClosedD");
            list.Add("sOnHoldD");
            list.Add("sCanceledD");
            list.Add("sRejectD");
            list.Add("sSuspendedD");
            list.Add("sShippedToInvestorD");
            list.Add("sLPurchaseD");
            list.Add("sGoodByLetterD");
            list.Add("sServicingStartD");
            list.Add("sU1LStatD");
            list.Add("sU2LStatD");
            list.Add("sU3LStatD");
            list.Add("sU4LStatD");
            list.Add("sU1LStatDesc");
            list.Add("sU2LStatDesc");
            list.Add("sU3LStatDesc");
            list.Add("sU4LStatDesc");
            list.Add("sPrelimRprtOd");
            list.Add("sApprRprtOd");
            list.Add("sAppSubmittedD");
            list.Add("sGfeInitialDisclosureD");
            list.Add("sGfeRedisclosureD");
            list.Add("sTilInitialDisclosureD");
            list.Add("sTilRedisclosureD");
            list.Add("sApprRprtExpD");
            list.Add("sIncomeDocExpD");
            list.Add("sCrExpD");
            list.Add("sAssetExpD");
            list.Add("sBondDocExpD");
            #endregion

            s_selectProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection);

        }

        protected override bool m_enforceAccessControl
        {
            get
            {
                return false;
            }
        }

        public MobileStatusWithSecurityByPassData(Guid fileId): base(fileId, "MobileStatusWithSecurityByPassData", s_selectProvider)
        {
        }
    }
}