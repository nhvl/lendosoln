using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
	public class CPmlTransformData : CPageData
	{
		private static CSelectStatementProvider s_selectProvider;
	    
		static CPmlTransformData()
		{
			StringList list = new StringList();
	        
	        #region Target Fields

			list.Add( "sStatusT"             );
			list.Add( "sLNm"                 );
			list.Add( "sPrimBorrowerFullNm"  );
			list.Add( "sfTransformDataToPml" );
			list.Add( "sPmlSubmitStatusT"    );
            list.Add("aIsCreditReportOnFile");

			#endregion
	        
			s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields , list.Collection );

		}        
	        
		public CPmlTransformData( Guid fileId )
		: base( fileId , "CPmlTransformData" , s_selectProvider )
		{
		}

	}

}
