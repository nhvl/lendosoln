﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using LendersOffice.Constants;

namespace DataAccess
{
    /// <summary>
    /// Contains a set of commonly used functions to perform data validation on user input. 
    /// If you add a method here, add test to UserDataValidationTest class in the LendOSolnTest. 
    /// </summary>
    public static class UserDataValidation
    {

        /// <summary>
        /// This is not that great, if people want other emails to work we will need to change it.
        /// </summary>
        private static readonly Regex m_emailPattern = new Regex(ConstApp.EmailValidationExpression, RegexOptions.Compiled);



        /// <summary>
        /// Checks to see if the given string is a valid email address.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static bool IsValidEmailAddress(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                return false; 
            }

            return m_emailPattern.IsMatch(email);
        }

        /// <summary>
        /// Checks to see if the given string is a valid SSN.
        /// </summary>
        /// <param name="ssn"></param>
        /// <returns></returns>
        public static bool IsValidSSN(string ssn)
        {
            return !string.IsNullOrEmpty(ssn) && ssn.Length == 11;
        }
    }
}
