namespace DataAccess
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using System.Text.RegularExpressions;

    /// <summary>
    /// Provides FIPS Codes for state/county pairs and a list of counties for a given state. 
    /// This Class implements the Singleton pattern and is Thread Safe.
    /// av 1-15-08
    /// </summary>
    public sealed class StateInfo
	{
		public static readonly StateInfo Instance = new StateInfo();

        /// <summary>
        /// The country code for USA.
        /// </summary>
        public const string USACountryCode = "US";

        /// <summary>
        /// The country name for USA.
        /// </summary>
        public const string USACountryName = "United States of America";
        
        private Regex removeFilter;
        private Regex spaceFilter;

        /// <summary>
        /// Filters out apostrophes, periods and hyphens in a given string
        /// </summary>
        /// <param name="input">The string to filter</param>
        /// <returns>The input without any apostrophes, periods or hyphens.</returns>
        private string filterCharacters(string input)
        {
            //Everywhere the regex matches in the string, replace with the empty string
            string temp = removeFilter.Replace(input, "", -1);
            //Same except with spaces
            return spaceFilter.Replace(temp, " ", -1);
        }

        private Dictionary<string, SortedDictionary<string, int>> m_States;

        public ReadOnlyDictionary<string, SortedDictionary<string, int>> StatesCountiesAndFipsMap
        {
            get
            {
                return new ReadOnlyDictionary<string, SortedDictionary<string, int>>(m_States);
            }
        }

        /// <summary>
        /// Set of states that should not be included in list returned by GetStatesAndCounties.
        /// </summary>
        private HashSet<string> excludedStates = new HashSet<string>()
        {
            "AA",
            "AE",
            "AP"
        };

        private ReadOnlyDictionary<string, SortedDictionary<string, int>> m_StatesExcludingArmy;
        public ReadOnlyDictionary<string, SortedDictionary<string, int>> StatesCountiesAndFipsMapExcludingArmy
        {
            get
            {
                if (m_StatesExcludingArmy == null)
                {
                    m_StatesExcludingArmy = new ReadOnlyDictionary<string, SortedDictionary<string, int>>(m_States.Where(p => !excludedStates.Contains(p.Key)).ToDictionary(kvp => kvp.Key, kvp => kvp.Value));
                }

                return m_StatesExcludingArmy;
            }
        }

        //One instance is enough. 
        private StateInfo()
		{
            //Initialize our regexen and have them be compiled.
            removeFilter = new Regex(@"['.]+", RegexOptions.Compiled);
            spaceFilter = new Regex(@"[-]+", RegexOptions.Compiled);
			Update();
		}

		/// <summary>
		/// This method should only run once on creation. It goes to the database and fetches all the counties/states along with their fips code. 
		/// </summary>
		private void Update() 
		{
            //Dictionary is [state two-letter abbrev] -> <[county full name] : [fips code]>
            m_States = new Dictionary<string, SortedDictionary<string, int>>(StringComparer.CurrentCultureIgnoreCase);
			using( DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "GetFIPSCodes") )
			{
				while( reader.Read() )
				{
                    //Get the current state's two letter abbreviation
                    string stateAbbrev = reader["State"].ToString();

					SortedDictionary<string,int> counties;
                    //Try and get the current state's counties from the States dictionary
					if ( ! m_States.TryGetValue(stateAbbrev, out counties ) )
					{
                        //First time we've seen this state, so make an entry for it
                        counties = new SortedDictionary<string, int>(StringComparer.CurrentCultureIgnoreCase);
						m_States.Add( stateAbbrev, counties ); 
					}
			 
                    //Get the county name, and filter it so everything is on the same level
                    string county = filterCharacters(reader["County"].ToString());

                    //Do we already have this county in there? (we shouldn't, btw)
					if ( ! counties.ContainsKey( county ))  
					{
                        //Then add the county's FIPS code.
						counties.Add( county, System.Convert.ToInt32(reader["FipsCountyCode"])); 
					}
				}
			}
		}

        /// <summary>
        /// Normalizes the input similar to how we normalize counties.
        /// </summary>
        /// <param name="input">The input to normalize.</param>
        /// <returns>Normalized string.</returns>
        public string Sanitize(string input)
        {
            return this.filterCharacters(input);
        }

		/// <summary>
		/// Fetches the fips code for the given state/county. At the time of writing this comment the 
		/// database contained 0s in the fips column for some places that did not have fips code such as AP state or other non states. 
		/// A 0 will be returned for anything invalid or missing. 
		/// </summary>
		/// <param name="State">The two letter abbreviation of the state.</param>
		/// <param name="County">The full name of the county</param>
		/// <returns>FipsCode (int) or 0 if no code was found</returns>
        public int GetFipsCodeFor( string state, string county ) 
        {
            state = filterCharacters(state);
            county = filterCharacters(county);
            
            SortedDictionary<string, int> counties; 
	        if ( m_States.TryGetValue( state , out counties) )
	        {
                int fipsCode; 
		        if ( counties.TryGetValue(county,out fipsCode )) 
		        {
                    return fipsCode;
		        }
                if (county.ToLower().Contains("st"))
                {
                    if (counties.TryGetValue(Tools.ReplaceWord(county, "ST", true, "SAINT"), out fipsCode))
                    {
                        return fipsCode;
                    }
                }
                if (county.ToLower().Contains("ste"))
                {
                    if (counties.TryGetValue(Tools.ReplaceWord(county, "STE", true, "SAINTE"), out fipsCode))
                    {
                        return fipsCode;
                    }
                }
	        }
	        return 0;
        }

        /// <summary>
        /// Gets the FIPS county code based on the given county & state.
        /// </summary>
        /// <param name="state">The two letter abbreviation of the state, e.g. CA.</param>
        /// <param name="county">The full county name, e.g. Orange.</param>
        /// <returns>The FIPS code for the county.</returns>
        public string GetFIPSCodeForCounty(string state, string county)
        {
            string countyCode = this.GetFipsCodeFor(state, county).ToString();

            return countyCode.Length > 3 ? countyCode.Substring(countyCode.Length - 3, 3) : countyCode;
        }

        /// <summary>
        /// Gets the FIPS state code based on the given county & state.
        /// </summary>
        /// <param name="state">The two letter abbreviation of the state, e.g. CA.</param>
        /// <param name="county">The full county name, e.g. Orange.</param>
        /// <returns>The FIPS code for the state.</returns>
        public string GetFIPSCodeForState(string state, string county)
        {
            string stateCode = this.GetFipsCodeFor(state, county).ToString();

            if (stateCode.Length > 3)
            {
                stateCode = stateCode.Substring(0, stateCode.Length - 3);

                if (stateCode.Length == 1)
                {
                    stateCode = "0" + stateCode;
                }
            }

            return stateCode;
        }

		/// <summary>
		/// Returns an ArrayList containing all the counties in the given state. Note this works with non states also! 
		/// </summary>
        /// <remarks> DONT USE ANYMORE USE</remarks>
		/// <param name="State">The two letter abbreviation of the state.</param>
		/// <returns>A sorted ArrayList with strings representing the counties or empty if the state does not exist.</returns>
		public ArrayList GetCountiesFor( string state ) 
		{
            //Even though the remark says don't use any more, this still gets called 
            //(I marked it obsolete and the build broke :( )
            //So, filter it!
            state = filterCharacters(state);
            SortedDictionary<string, int> counties; 
            if (m_States.TryGetValue(state, out counties))
			{
                return new ArrayList(counties.Keys);
			}
            return new ArrayList();
		}

        public IEnumerable<string> GetAllStates()
        {
            return m_States.Keys;
        }

        /// <summary>
        /// Returns an IList with strings containing all the counties in the given state. Note this works with non states also! 
        /// Returns empty string if its false.
        /// </summary>
        /// <param name="state">The state to look up</param>
        public IList<string> GetCountiesIn(string state)
        {
            state = filterCharacters(state);
            SortedDictionary<string, int> counties;
            if (m_States.TryGetValue(state, out counties))
            {
                return new List<string>(counties.Keys);
            }
            return new List<string>();
        }

        /// <summary>
        /// Returns the first matching county that is in the specified zipcode and state.
        /// A zipcode can be in multiple counties.
        /// </summary>
        /// <param name="state">Two letter abbreviation of the state.</param>
        /// <param name="zip">The five digit zipcode.</param>
        /// <returns></returns>
        public string GetFirstCountyByStateAndZip(string state, string zip)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@Zipcode", zip)
                                        };
            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "GetCountyCityStateByZip", parameters ))
            {
                while ( reader.Read())
                {
                    if ( reader["State"].ToString().Equals(state, StringComparison.OrdinalIgnoreCase))
                    {
                        return reader["County"].ToString();
                    }
                }
            }
            return null;
        }

		/// <summary>
		/// Gets the code for the given state abbreviation. It fetches the counties but looks at the first one only.  
		/// The first two digits belong to the state. The number is 5 digitios so fipscode / 1000 = statecode. 
		/// </summary>
        /// <param name="state">The state in question ( abbreviation ) </param>
		/// <returns>The code for the state</returns>
		public int StateCode( string state ) 
		{
            state = filterCharacters(state);
			int stateCode = -1;
            SortedDictionary<string,int> counties; 
			if ( m_States.TryGetValue(state, out counties)) 
			{
               var i = counties.Values.GetEnumerator();
               if (i.MoveNext())
               {
					stateCode = i.Current / 1000; 
               }
			}
			return stateCode;
		}

        /// <summary>
        /// Map of state abbreviation to state name. It might be better to add a state name column to the associated database table at some point.
        /// </summary>
        public static readonly Dictionary<string, string> StatesAndTerritories = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
        {
            {"AA", "Armed Forces Americas"},
            {"AE", "Armed Forces Europe"},
            {"AK", "Alaska"},
            {"AL", "Alabama"},
            {"AP", "Armed Forces Pacific"},
            {"AR", "Arkansas"},
            {"AS", "American Samoa"},
            {"AZ", "Arizona"},
            {"CA", "California"},
            {"CO", "Colorado"},
            {"CT", "Connecticut"},
            {"DC", "District of Columbia"},
            {"DE", "Delaware"},
            {"FL", "Florida"},
            {"FM", "Federated States Of Micronesia"},
            {"GA", "Georgia"},
            {"GU", "Guam"},
            {"HI", "Hawaii"},
            {"IA", "Iowa"},
            {"ID", "Idaho"},
            {"IL", "Illinois"},
            {"IN", "Indiana"},
            {"KS", "Kansas"},
            {"KY", "Kentucky"},
            {"LA", "Louisiana"},
            {"MA", "Massachusetts"},
            {"MD", "Maryland"},
            {"ME", "Maine"},
            {"MH", "Marshall Islands"},
            {"MI", "Michigan"},
            {"MN", "Minnesota"},
            {"MO", "Missouri"},
            {"MP", "Northern Mariana Islands"},
            {"MS", "Mississippi"},
            {"MT", "Montana"},
            {"NC", "North Carolina"},
            {"ND", "North Dakota"},
            {"NE", "Nebraska"},
            {"NH", "New Hampshire"},
            {"NJ", "New Jersey"},
            {"NM", "New Mexico"},
            {"NV", "Nevada"},
            {"NY", "New York"},
            {"OH", "Ohio"},
            {"OK", "Oklahoma"},
            {"OR", "Oregon"},
            {"PA", "Pennsylvania"},
            {"PR", "Puerto Rico"},
            {"PW", "Palau"},
            {"RI", "Rhode Island"},
            {"SC", "South Carolina"},
            {"SD", "South Dakota"},
            {"TN", "Tennessee"},
            {"TX", "Texas"},
            {"UT", "Utah"},
            {"VA", "Virginia"},
            {"VI", "Virgin Islands"},
            {"VT", "Vermont"},
            {"WA", "Washington"},
            {"WI", "Wisconsin"},
            {"WV", "West Virginia"},
            {"WY", "Wyoming"}
        };

        /// <summary>
        /// Get the full name of the state or territory from the abbreviation. E.G. CA returns California.
        /// </summary>
        /// <param name="abbreviation">The abbreviation for the state or territory.</param>
        /// <returns>The full name of the state or territory. Returns empty string if the given abbreviation is blank/null, or the state cannot be found.</returns>
        public string GetStateName(string abbreviation)
        {
            if (string.IsNullOrEmpty(abbreviation))
            {
                return string.Empty;
            }

            abbreviation = this.filterCharacters(abbreviation);
            string stateName = string.Empty;

            return StatesAndTerritories.TryGetValue(abbreviation, out stateName) ? stateName : string.Empty;
        }
	}
}
