using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
	public class CLeadConvertLoanData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CLeadConvertLoanData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
	        list.Add("sLeadSrcDesc");
            list.Add("aBFirstNm");
            list.Add("aBMidNm");
            list.Add("aBLastNm");
            list.Add("aBSuffix");
            list.Add("aBDob");
            list.Add("aBAge");
            list.Add("aBSsn");
            list.Add("aBMaritalStatT");
            list.Add("aBAddr");
            list.Add("aBCity");
            list.Add("aBState");
            list.Add("aBZip");
            list.Add("aBEmail");
            list.Add("aBHPhone");
            list.Add("aBBusPhone");
            list.Add("aBCellPhone");
            list.Add("aBFax");
            list.Add("aBBaseI");
            list.Add("aCFirstNm");
            list.Add("aCMidNm");
            list.Add("aCLastNm");
            list.Add("aCSuffix");
            list.Add("aCDob");
            list.Add("aCAge");
            list.Add("aCSsn");
            list.Add("aCMaritalStatT");
            list.Add("aCAddr");
            list.Add("aCCity");
            list.Add("aCState");
            list.Add("aCZip");
            list.Add("aCEmail");
            list.Add("aCHPhone");
            list.Add("aCBusPhone");
            list.Add("aCCellPhone");
            list.Add("aCFax");
            list.Add("aCBaseI");
            list.Add("sLT");
            list.Add("sLPurposeT");
            list.Add("sLienPosT");
            list.Add("sLpTemplateNm");
            list.Add("sPurchPrice");
            list.Add("sApprVal");
            list.Add("sDownPmtPc");
            list.Add("sEquityCalc");
            list.Add("sLAmtLckd");
            list.Add("sLAmtCalc");
            list.Add("sNoteIR");
            list.Add("sTerm");
            list.Add("sDue");
            list.Add("sSpAddr");
            list.Add("sSpCity");
            list.Add("sSpState");
            list.Add("sSpZip");
            list.Add("sTrNotes");
            list.Add("afAcceptNewLiaCollection");
            list.Add("sLeadD");

            list.Add("sEmployeeLoanRepId");
            list.Add("sEmployeeLoanRepName");
            list.Add("sEmployeeLoanRepPhone");

            list.Add("sEmployeeProcessorId");
            list.Add("sEmployeeProcessorName");
            list.Add("sEmployeeLoanOpenerId");
            list.Add("sEmployeeLoanOpenerName");

            list.Add("sEmployeeCallCenterAgentId");
            list.Add("sEmployeeCallCenterAgentName");
            list.Add("sEmployeeRealEstateAgentId");
            list.Add("sEmployeeRealEstateAgentName");
            list.Add("sEmployeeRealEstateAgentEmail");
            list.Add("sEmployeeRealEstateAgentPhone");
            list.Add("sEmployeeLenderAccExecId");
            list.Add("sEmployeeLenderAccExecName");
            list.Add("sEmployeeLenderAccExecEmail");
            list.Add("sEmployeeLenderAccExecPhone");
            list.Add("sEmployeeLockDeskId");
            list.Add("sEmployeeLockDeskName");
            list.Add("sEmployeeLockDeskEmail");
            list.Add("sEmployeeLockDeskPhone");
            list.Add("sEmployeeUnderwriterId");
            list.Add("sEmployeeUnderwriterName");
            list.Add("sEmployeeUnderwriterEmail");
            list.Add("sEmployeeUnderwriterPhone");
            list.Add("sEmployeeManagerId");
            list.Add("sEmployeeManagerName");
            list.Add("sEmployeeManagerEmail");
            list.Add("sEmployeeManagerPhone");
            list.Add("sPreparerDataSet");
            list.Add("aBExperianScore");
            list.Add("aBExperianCreatedD");
            list.Add("aBExperianFactors");
            list.Add("aCExperianScore");
            list.Add("aCExperianCreatedD");
            list.Add("aBTransUnionScore");
            list.Add("aBTransUnionCreatedD");
            list.Add("aBTransUnionFactors");
            list.Add("aCTransUnionScore");
            list.Add("aCTransUnionCreatedD");
            list.Add("aCTransUnionFactors");
            list.Add("aBEquifaxScore");
            list.Add("aBEquifaxCreatedD");
            list.Add("aBEquifaxFactors");
            list.Add("aCEquifaxScore");
            list.Add("aCEquifaxCreatedD");
            list.Add("aCEquifaxFactors");
            list.Add("sExperianModelName");
            list.Add("sExperianScoreFrom");
            list.Add("sExperianScoreTo");
            list.Add("sEquifaxModelName");
            list.Add("sEquifaxScoreFrom");
            list.Add("sEquifaxScoreTo");
            list.Add("sTransUnionModelName");
            list.Add("sTransUnionScoreFrom");
            list.Add("sTransUnionScoreTo");
            list.Add("sLNm");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
        protected override bool m_enforceAccessControl
        {
            get { return false; }
        }
		public CLeadConvertLoanData(Guid fileId) : base(fileId, "CLeadConvertLoanData", s_selectProvider)
		{
		}
	}
}
