using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;
using LendersOffice.Constants;
using LendersOffice.HttpModule;

namespace DataAccess
{
    /// <summary>
    /// This is an helper class that data return from stored procedure and fill in dataset.
    /// </summary>
    public class DataSetHelper 
	{
        public static void Fill(DataSet ds, Guid brokerId, string procedureName, IEnumerable<SqlParameter> parameters)
        {
            var connInfo = DbConnectionInfo.GetConnectionInfo(brokerId);
            Fill_Private(ds, connInfo, DataSrc.LOShare, procedureName, 0, parameters);
        }

        public static void Fill(DataSet ds, DbConnectionInfo connInfo, string procedureName, IEnumerable<SqlParameter> parameters)
        {
            Fill_Private(ds, connInfo, DataSrc.LOShare, procedureName, 0, parameters);
        }

        public static void Fill(DataSet ds, DataSrc dataSrc, string procedureName, IEnumerable<SqlParameter> parameters) 
		{
			Fill_Private( ds, null, dataSrc, procedureName, 0, parameters );
		}

		private static void Fill_Private(DataSet ds, DbConnectionInfo connInfo, DataSrc dataSrc, string procedureName, int nRetries, IEnumerable<SqlParameter> parameters) 
		{
            int nExec = nRetries + 1;
            Exception lastException = null;

            for (int i = 0; i < nExec; i++) 
            {
                try 
                {
                    try 
                    {
                        DbConnection conn = null;

                        if (connInfo == null)
                        {
                            conn = DbAccessUtils.GetConnection(dataSrc);
                        }
                        else
                        {
                            conn = connInfo.GetConnection();
                        }

                        var command = conn.CreateCommand();
                        command.CommandText = procedureName;
                        command.CommandType = CommandType.StoredProcedure;

                        var dataAdapter = DbAccessUtils.GetDbProvider().CreateDataAdapter();
                        dataAdapter.SelectCommand = command;

                        if (parameters != null) 
                        {
                            foreach (SqlParameter p in parameters) 
                            {
                                dataAdapter.SelectCommand.Parameters.Add((SqlParameter) ((ICloneable) p).Clone());
                            }
                        }
                        dataAdapter.Fill(ds);
                        // Q: Why connection does not close?
                        // A: SqlDataAdapter.Fill will leave the state of connection the same before and after Fill() invoke.
                        //    Therefore, in this scenario SqlDataAdapter.Fill will close the connection when done. dd 5/6/2003

                        return;
                    } 
                    catch (SqlException exc) 
                    {
                        lastException = exc;

                    }

                } 
                catch (Exception exc) 
                {
                    lastException = exc;
                }
                Tools.SleepAndWakeupRandomlyWithin( 1000, 4500 );
            }

            SqlException sqlException = lastException as SqlException;

            if (null != sqlException) 
            {
                StringBuilder sb = new StringBuilder();

                sb.Append( Environment.NewLine );
                sb.Append( "<!---- Stored Procedure Debug Info (Fill) ---->").Append( Environment.NewLine );
                sb.AppendFormat( "Procedure name = {0}{1}" , procedureName , Environment.NewLine );

                if( parameters != null )
                {
                    foreach( SqlParameter p in parameters )
                    {
                        sb.AppendFormat("  {0} = {1}{2}" , p.ParameterName , p.Value , Environment.NewLine );
                    }
                }

                sb.Append( "<!----- End Stored Procedure Debug Info ---->" ).Append( Environment.NewLine );

                // Construct wrapping exception and send it to the
                // calling code.  We first write it out to the dev
                // team via email.

                CBaseException baseException = new GenericSqlException(sb.ToString(), sqlException);
                Tools.LogErrorWithCriticalTracking(baseException);
            }

            throw lastException;
		}

        public static void FillWithRetries(DataSet ds, DataSrc dataSrc, string procedureName, int nRetries, IEnumerable<SqlParameter> parameters) 
        {
            Fill_Private(ds, null, dataSrc, procedureName, nRetries, parameters);
        }
    }
}