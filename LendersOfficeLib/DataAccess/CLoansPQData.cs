﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccess
{
    public class CLoansPQData : CPageData
    {
        private static CSelectStatementProvider _selectProvider;

        static CLoansPQData()
        {
            StringList list = new StringList();
            list.Add("sInvestorLockRLckdDays");
            list.Add("sInvestorLockRLckdD");
            list.Add("sAssessorsParcelId");
            list.Add("sBrokerId");
            list.Add("sBranchId");
            list.Add("sfTransformLoPropertyTypeToPml");
            list.Add("aAddr");
            list.Add("aAddrT");
            list.Add("aAddrYrs");
            list.Add("aAssetCollection");
            list.Add("aBBaseI");
            list.Add("aBBonusesI");
            list.Add("aBDividendI");
            list.Add("aBIsValidNameSsn");
            list.Add("aBNetRentI1003");
            list.Add("aBOvertimeI");
            list.Add("aBTotOI");
            list.Add("aBusPhone");
            list.Add("aCellPhone");
            list.Add("aCIsValidNameSsn");
            list.Add("aCity");
            list.Add("aDecAlimony");
            list.Add("aDecBankrupt");
            list.Add("aDecBorrowing");
            list.Add("aDecCitizen");
            list.Add("aDecDelinquent");
            list.Add("aDecEndorser");
            list.Add("aDecForeclosure");
            list.Add("aDecJudgment");
            list.Add("aDecLawsuit");
            list.Add("aDecObligated");
            list.Add("aDecOcc");
            list.Add("aDecPastOwnedPropT");
            list.Add("aDecPastOwnedPropTitleT");
            list.Add("aDecPastOwnership");
            list.Add("aDecResidency");
            list.Add("aDependAges");
            list.Add("aDependNum");
            list.Add("aDob");
            list.Add("aEmail");
            list.Add("aEmpCollection");
            list.Add("aFax");
            list.Add("aFirstNm");
            list.Add("aHPhone");
            list.Add("aLastNm");
            list.Add("aMidNm");
            list.Add("aPrev1Addr");
            list.Add("aPrev1City");
            list.Add("aPrev1State");
            list.Add("aPrev2AddrT");
            list.Add("aReCollection");
            list.Add("aSsn");
            list.Add("aState");
            list.Add("aZip");

            list.Add("sGfeLockPeriodBeforeSettlement");
            list.Add("sGfeCanRateIncrease");
            list.Add("sPrepmtPenaltyT");
            list.Add("sGfeRateLockPeriod");
            list.Add("sGfeCanRateIncrease");

            list.Add("aBaseI");
            list.Add("aBonusesI");
            list.Add("aDividendI");
            list.Add("aNetRentI1003");
            list.Add("aOvertimeI");
            list.Add("aTotOI");

            list.Add("aAddrMail");
            list.Add("aAddrMailUsePresentAddr");
            list.Add("aAddrMailSourceT");
            list.Add("aCityMail");
            list.Add("aMaritalStatT");
            list.Add("aPrev1AddrT");
            list.Add("aPrev1AddrYrs");
            list.Add("aPrev1Zip");
            list.Add("aStateMail");
            list.Add("aSuffix");
            list.Add("aZipMail");
            list.Add("aPresHazIns");
            list.Add("aPresHoAssocDues");
            list.Add("aPresMIns");
            list.Add("aPresOFin");
            list.Add("aPresOHExp");
            list.Add("aPresRealETx");
            list.Add("aPresRent");
            list.Add("sLNm");

            list.Add("aSchoolYrs");
            list.Add("nApps");
            list.Add("sBuildingStatusT");
            list.Add("sLotImprovC");
            list.Add("sLotVal");
            list.Add("sProHazInsMb");
            list.Add("sProHazInsR");
            list.Add("sProHoAssocDues");
            list.Add("sProMInsMb");
            list.Add("sProMInsR");
            list.Add("sProOHExp");
            list.Add("sProRealETxMb");
            list.Add("sProRealETxR");
            list.Add("sSpAcqYr");
            list.Add("sSpAddr");
            list.Add("sSpCity");
            list.Add("sSpCounty");
            list.Add("sSpImprovC");
            list.Add("sSpImprovDesc");
            list.Add("sSpImprovTimeFrameT");
            list.Add("sSpOrigC");
            list.Add("sSpState");
            list.Add("sSpZip");
            list.Add("sUnitsNum");
            list.Add("sYrBuilt");

            list.Add("sAgentDataSet");
            list.Add("sAltCost");
            list.Add("sBuydown");
            list.Add("sBuydownContributorT");
            list.Add("sDue");
            list.Add("sDwnPmtSrc");
            list.Add("sDwnPmtSrcExplain");
            list.Add("sEquityCalc");
            list.Add("sEstateHeldT");
            list.Add("sFfUfmip1003");
            list.Add("sFHALenderIdCode");
            list.Add("sFHASalesConcessions");
            list.Add("sFHASponsorAgentIdCode");
            list.Add("sFinMethT");
            list.Add("sFredAffordProgId");
            list.Add("sFreddieArmIndexT");
            list.Add("sGseRefPurposeT");
            list.Add("sGseSpT");
            list.Add("sLDiscnt1003");
            list.Add("sLienPosT");
            list.Add("sLPurposeT");
            list.Add("sLT");
            list.Add("sLTODesc");
            list.Add("sNoteIR");
            list.Add("sOCredit1Amt");
            list.Add("sOCredit1Desc");
            list.Add("sOCredit1Lckd");
            list.Add("sOCredit2Amt");
            list.Add("sOCredit2Desc");
            list.Add("sOCredit2Lckd");
            list.Add("sOCredit3Amt");
            list.Add("sOCredit3Desc");
            list.Add("sOCredit4Amt");
            list.Add("sOCredit4Desc");
            list.Add("sPmtAdjCapMon");
            list.Add("sPmtAdjCapR");
            list.Add("sPmtAdjMaxBalPc");
            list.Add("sProdCashoutAmt");
            list.Add("sPurchPrice");
            list.Add("sRAdj1stCapMon");
            list.Add("sRefPdOffAmt1003");
            list.Add("sSpMarketVal");
            list.Add("sTerm");
            list.Add("sTotCcPbs");
            list.Add("sTotEstCcNoDiscnt1003");
            list.Add("sTotEstPp1003");
            list.Add("aIntrvwrMethodT");
            list.Add("sSpOrigc");
            list.Add("sIsConstructionLoan");
            list.Add("sIsRefinancing");
            list.Add("sRAdj1stCapR");
            list.Add("sBuydwnR1");
            list.Add("sLotAcqYr");
            list.Add("sLotOrigC");
            list.Add("sLotImprovC");
            list.Add("sHmdaCountyCode");
            list.Add("sHmdaReportAsHoepaLoan");
            list.Add("sHmdaMsaNum");
            list.Add("sHmdaStateCode");
            list.Add("sFHARefinanceTypeDesc");
            list.Add("sFHAHousingActSection");

            list.Add("sApprovD");
            list.Add("sApr");
            list.Add("sFinMethodPrintAsOther");
            list.Add("sFinMethPrintAsOtherDesc");
            list.Add("sRejectD");
            list.Add("sRLckdD");
            list.Add("sRLckdDays");
            list.Add("sStatusT");
            list.Add("sSubmitD");

            list.Add("aGender");
            list.Add("aHispanicT");
            list.Add("aNoFurnish");
            list.Add("aRaceT");

            list.Add("aIsAmericanIndian");
            list.Add("aIsAsian");
            list.Add("aIsBlack");
            list.Add("aIsPacificIslander");
            list.Add("aIsWhite");

            list.Add("aAppId");
            list.Add("aCommisionI");
            list.Add("aNetRentI1003Lckd");
            list.Add("aPres1stM");
            list.Add("s1stMtgOrigLAmt");
            list.Add("sAgentCollection");
            list.Add("sAgentDataSetClear");
            list.Add("sConcurSubFin");
            list.Add("sFfUfmip1003Lckd");
            list.Add("sFfUfmipFinanced");
            list.Add("sIsOFinNew");
            list.Add("sLandCost");
            list.Add("sLDiscnt1003Lckd");
            list.Add("sONewFinBal");
            list.Add("sRefPdOffAmt1003Lckd");
            list.Add("sTotCcPbsLocked");
            list.Add("sTotEstPp1003Lckd");

            list.Add("aLiaCollection");
            list.Add("aOtherIncomeList");

            list.Add("aEmplrBusPhoneLckd");
            list.Add("aManner");
            list.Add("aOccT");

            list.Add("sRAdjCapMon");
            list.Add("sRAdjCapR");
            list.Add("sTotEstCc1003Lckd");

            list.Add("sAgencyCaseNum");
            list.Add("sFreddieConstructionT");
            list.Add("sFreddieTransactionId");
            list.Add("sFredieReservesAmt");
            list.Add("sVaProUtilityPmt");
            list.Add("sSpProjClassT");
            list.Add("sLAmtLckd");
            list.Add("sLAmtCalc");

            list.Add("aTitleNm1");
            list.Add("aTitleNm2");
            list.Add("sProHazIns");
            list.Add("sProMIns");
            list.Add("sProRealETx");
            list.Add("sfGetAgentOfRole");
            list.Add("sProdDocT");

            list.Add("sFinalLAmt");
            list.Add("sSubFinIR");
            list.Add("sSubFinMb");
            list.Add("sUfCashPd");


            list.Add("sLenderCaseNum");
            list.Add("sProFirstMPmt");
            list.Add("sProSecondMPmt");

            list.Add("sLotLien");
            list.Add("sSpLien");

            list.Add("sFinMethDesc");

            list.Add("afDelMarriedCobor");

            list.Add("sSpLegalDesc");
            list.Add("aLiaMonTot");

            list.Add("s800U1F");
            list.Add("s800U1FCode");
            list.Add("s800U1FDesc");
            list.Add("s800U1FProps");
            list.Add("s800U2F");
            list.Add("s800U2FCode");
            list.Add("s800U2FDesc");
            list.Add("s800U2FProps");
            list.Add("s800U3F");
            list.Add("s800U3FCode");
            list.Add("s800U3FDesc");
            list.Add("s800U3FProps");
            list.Add("s800U4F");
            list.Add("s800U4FCode");
            list.Add("s800U4FDesc");
            list.Add("s800U4FProps");
            list.Add("s800U5F");
            list.Add("s800U5FCode");
            list.Add("s800U5FDesc");
            list.Add("s800U5FProps");
            list.Add("sApprF");
            list.Add("sApprFPaid");
            list.Add("sCrF");
            list.Add("sCrFPaid");
            list.Add("sCrFProps");
            list.Add("sInspectF");
            list.Add("sInspectFProps");
            list.Add("sLDiscntFMb");
            list.Add("sLDiscntPc");
            list.Add("sLDiscntProps");
            list.Add("sLOrigFMb");
            list.Add("sLOrigFPc");
            list.Add("sLOrigFProps");
            list.Add("sMBrokFMb");
            list.Add("sMBrokFPc");
            list.Add("sMBrokFProps");
            list.Add("sProcF");
            list.Add("sProcFPaid");
            list.Add("sProcFProps");
            list.Add("sTxServF");
            list.Add("sTxServFProps");
            list.Add("sUwF");
            list.Add("sUwFProps");
            list.Add("sWireF");
            list.Add("sWireFProps");


            list.Add("s1006ProHExp");
            list.Add("s1006ProHExpDesc");
            list.Add("s1006RsrvMon");
            list.Add("s1006RsrvProps");
            list.Add("s1007ProHExp");
            list.Add("s1007ProHExpDesc");
            list.Add("s1007RsrvMon");
            list.Add("s1007RsrvProps");
            list.Add("s900U1Pia");
            list.Add("s900U1PiaCode");
            list.Add("s900U1PiaDesc");
            list.Add("s900U1PiaProps");
            list.Add("s904Pia");
            list.Add("s904PiaDesc");
            list.Add("s904PiaProps");
            list.Add("sAggregateAdjRsrv");
            list.Add("sAggregateAdjRsrvLckd");
            list.Add("sAggregateAdjRsrvProps");
            list.Add("sApprFProps");
            list.Add("sAttorneyF");
            list.Add("sAttorneyFProps");
            list.Add("sCountyRtcBaseT");
            list.Add("sCountyRtcDesc");
            list.Add("sCountyRtcMb");
            list.Add("sCountyRtcPc");
            list.Add("sCountyRtcProps");
            list.Add("sDocPrepF");
            list.Add("sDocPrepFProps");
            list.Add("sEscrowF");
            list.Add("sEscrowFProps");
            list.Add("sEscrowFTable");
            list.Add("sFloodInsRsrvMon");
            list.Add("sFloodInsRsrvProps");
            list.Add("sHazInsPiaMon");
            list.Add("sHazInsPiaProps");
            list.Add("sHazInsRsrvMon");
            list.Add("sHazInsRsrvProps");
            list.Add("sIPerDay");
            list.Add("sGfeNoteIRAvailTillD");
            list.Add("sIPerDayLckd");
            list.Add("sIPiaDy");
            list.Add("sIPiaProps");
            list.Add("sMInsRsrvMon");
            list.Add("sMInsRsrvProps");
            list.Add("sMipPiaProps");
            list.Add("sNotaryF");
            list.Add("sNotaryFProps");
            list.Add("sPestInspectF");
            list.Add("sPestInspectFProps");
            list.Add("sProFloodIns");
            list.Add("sProHazInsT");
            list.Add("sProMInsT");
            list.Add("sProRealETxT");
            list.Add("sProSchoolTx");
            list.Add("sRealETxRsrvMon");
            list.Add("sRealETxRsrvProps");
            list.Add("sRecBaseT");
            list.Add("sRecFDesc");
            list.Add("sRecFMb");
            list.Add("sRecF");
            list.Add("sRecFPc");
            list.Add("sRecFProps");
            list.Add("sRecDeed");
            list.Add("sRecMortgage");
            list.Add("sRecRelease");
            list.Add("sRecFLckd");
            list.Add("sSchoolTxRsrvMon");
            list.Add("sSchoolTxRsrvProps");
            list.Add("sStateRtcBaseT");
            list.Add("sStateRtcDesc");
            list.Add("sStateRtcMb");
            list.Add("sStateRtcPc");
            list.Add("sStateRtcProps");
            list.Add("sTitleInsF");
            list.Add("sTitleInsFProps");
            list.Add("sTitleInsFTable");
            list.Add("sU1GovRtcBaseT");
            list.Add("sU1GovRtcCode");
            list.Add("sU1GovRtcDesc");
            list.Add("sU1GovRtcMb");
            list.Add("sU1GovRtcPc");
            list.Add("sU1GovRtcProps");
            list.Add("sU1Sc");


            list.Add("sU1ScCode");
            list.Add("sU1ScDesc");
            list.Add("sU1ScProps");
            list.Add("sU1Tc");
            list.Add("sU1TcCode");
            list.Add("sU1TcDesc");
            list.Add("sU1TcProps");
            list.Add("sU2GovRtcBaseT");
            list.Add("sU2GovRtcCode");
            list.Add("sU2GovRtcDesc");
            list.Add("sU2GovRtcMb");
            list.Add("sU2GovRtcPc");
            list.Add("sU2GovRtcProps");
            list.Add("sU2Sc");
            list.Add("sU2ScCode");
            list.Add("sU2ScDesc");
            list.Add("sU2ScProps");
            list.Add("sU2Tc");
            list.Add("sU2TcCode");
            list.Add("sU2TcDesc");
            list.Add("sU2TcProps");
            list.Add("sU3GovRtcBaseT");
            list.Add("sU3GovRtcCode");
            list.Add("sU3GovRtcDesc");
            list.Add("sU3GovRtcMb");
            list.Add("sU3GovRtcPc");
            list.Add("sU3GovRtcProps");
            list.Add("sU3Sc");
            list.Add("sU3ScCode");
            list.Add("sU3ScDesc");
            list.Add("sU3ScProps");
            list.Add("sU3Tc");
            list.Add("sU3TcCode");
            list.Add("sU3TcDesc");
            list.Add("sU3TcProps");
            list.Add("sU4Sc");
            list.Add("sU4ScCode");
            list.Add("sU4ScDesc");
            list.Add("sU4ScProps");
            list.Add("sU4Tc");
            list.Add("sU4TcCode");
            list.Add("sU4TcDesc");
            list.Add("sU4TcProps");
            list.Add("sVaFfProps");

            list.Add("sU5Sc");
            list.Add("sU5ScCode");
            list.Add("sU5ScDesc");
            list.Add("sU5ScProps");


            list.Add("sBrokComp1");
            list.Add("sBrokComp1Desc");
            list.Add("sBrokComp1Lckd");
            list.Add("sBrokComp1Pc");
            list.Add("sBrokComp2");
            list.Add("sBrokComp2Desc");
            list.Add("sApprVal");
            list.Add("sRAdjFloorR");
            list.Add("sRAdjMarginR");
            list.Add("sRAdjLifeCapR");
            list.Add("sProMIns");
            list.Add("sProMInsLckd");
            list.Add("sApr");
            list.Add("sFloodCertificationFProps");

            list.Add("aAddrTotalMonths");
            list.Add("aPrev1AddrTotalMonths");
            list.Add("aSchoolTotalMonths");
            list.Add("sFloodCertificationF");
            list.Add("sFloodCertificationFPaid");
            list.Add("sMBrokF");
            list.Add("sOwnerTitleInsF");
            list.Add("sOwnerTitleInsProps");
            list.Add("sProOHExpLckd");
            list.Add("sTxServFPaid");

            list.Add("sDocsD");
            list.Add("sOpenedD");
            list.Add("GfeTilPrepareDate");
            list.Add("sSchedDueD1");
            list.Add("sSchedDueD1Lckd");
            list.Add("sEstCloseD");
            list.Add("sDaysInYr");
            list.Add("sGfeRateLockPeriod");
            list.Add("sGfeLockPeriodBeforeSettlement");

            list.Add("sPreparerXmlContent");
            list.Add("sMonthlyPmt");
            list.Add("sProThisMPmt");
            list.Add("sProThisMPmt");
            list.Add("sMiCompanyNmT");
            list.Add("sLpTemplateNm");

            list.Add("sCustomField1Notes");
            list.Add("sCustomField2Notes");
            list.Add("sCustomField3Notes");
            list.Add("sCustomField4Notes");
            list.Add("sCustomField5Notes");
            list.Add("sCustomField6Notes");
            list.Add("sCustomField7Notes");
            list.Add("sCustomField8Notes");
            list.Add("sCustomField9Notes");
            list.Add("sCustomField10Notes");
            list.Add("sCustomField11Notes");
            list.Add("sCustomField12Notes");
            list.Add("sCustomField13Notes");
            list.Add("sCustomField14Notes");
            list.Add("sCustomField15Notes");
            list.Add("sCustomField16Notes");
            list.Add("sCustomField17Notes");
            list.Add("sCustomField18Notes");
            list.Add("sCustomField19Notes");
            list.Add("sCustomField20Notes");
            list.Add("aSecondHighestScore");
            list.Add("aHighestScore");
            list.Add("sSubFin");
            list.Add("sFundD");

            list.Add("sSpValuationMethodT");
            list.Add("sApprRprtRd");
            list.Add("sHazInsRsrvMonLckd"); // start opm 180983
            list.Add("sMInsRsrvMonLckd");
            list.Add("sRealETxRsrvMonLckd");
            list.Add("sSchoolTxRsrvMonLckd");
            list.Add("sFloodInsRsrvMonLckd");
            list.Add("s1006RsrvMonLckd");
            list.Add("s1007RsrvMonLckd");
            list.Add("sU3RsrvMonLckd");
            list.Add("sU4RsrvMonLckd"); // end opm 180983

            list.Add("sfAssignBranch");

            list.Add("sHmdaCensusTract"); // opm 280155

            list.Add(nameof(CAppData.aBInterviewMethodT));
            list.Add(nameof(CAppData.aCInterviewMethodT));

            list.Add(nameof(CPageData.sRemain1stMPmt));
            list.Add(nameof(CPageData.sRemain1stMBal));
            list.Add(nameof(CPageData.sRAdjIndexR));
            list.Add(nameof(CPageData.sRLifeCapR));
            list.Add(nameof(CPageData.sRLckdExpiredD));
            list.Add(nameof(CPageData.sGfeMaxPpmtPenaltyAmt));
            list.Add(nameof(CPageData.sGfeHavePpmtPenalty));
            list.Add(nameof(CPageData.sSpValuationEffectiveD));
            list.Add(nameof(CPageData.sProdSpT));
            list.Add(nameof(CPageData.sProdIsCondoStoriesHighRise));
            list.Add(nameof(CPageData.sCreditLineAmt));
            list.Add(nameof(CPageData.sProdIsCondotel));
            list.Add(nameof(CPageData.sIsLineOfCredit));
            list.Add(nameof(CPageData.sInitialRateTermExpirationDate));

            _selectProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection);

        }

        public CLoansPQData(Guid fileId)
            : base(fileId, "CLoansPQData", _selectProvider)
        {
        }


        public void SetsRAdjLifeCapR(string ceiling, string fundingFee)
        {
            decimal cel;
            decimal fee;
            if (Decimal.TryParse(ceiling, out cel) && Decimal.TryParse(fundingFee, out fee))
            {
                decimal tempSRAdjLifeCapR = (cel - fee);
                if (tempSRAdjLifeCapR > 0)
                {
                    sRAdjLifeCapR_rep = tempSRAdjLifeCapR.ToString();
                }
            }
        }

    }
}