/// Author: David Dao

using System;
using System.Data;
using System.IO;
using System.Text;
using System.Collections;
using LendersOffice.Common;

namespace DataAccess
{
	public class CPublicRecordCollection : CXmlRecordCollectionNoSpecialType, IPublicRecordCollection
    {
        public CPublicRecordCollection(CAppBase appData, string xmlSchema, string xmlContent) :
            base (appData.LoanData, appData, xmlSchema, xmlContent)
		{
		}


        protected override ICollectionItemBase2 CreateRegularRawRecord() 
        {
            return CPublicRecordFields.Create(m_appData, m_ds, this);
        }

        protected override ICollectionItemBase2 Reconstruct(int iRow ) 
        {
            if (iRow < 0) 
                throw new CBaseException(ErrorMessages.Generic, "Invalid iRow value for xml record.");
            return CPublicRecordFields.Reconstruct(m_appData, m_ds, this, iRow);
        }
        protected override E_ExistingRecordFaith DetermineRawRecordDestiny(ICollectionItemBase2 record )
        {
            return E_ExistingRecordFaith.E_RegularOK;
        }

        public override void Flush() 
        {
            ExecuteDeathRowRecords();

            int count = this.CountRegular;
            for (int i = 0; i < count; i++) 
            {
                var fields = GetRegularRecordAt(i);
                fields.PrepareToFlush();
            }
            m_appData.aPublicRecordXmlContent = m_ds.GetXml();

        }
        new public IPublicRecord GetRegularRecordAt( int pos )
        {
            // Use direct cast because we know base holds instances of CPublicRecordFields 
            // so cast from CXmlRecordBase2 to IPublicRecord will work and we want to make the 
            // strongest statement possible here.
            return (IPublicRecord)base.GetRegularRecordAt( pos );
        }
        new public IPublicRecord GetRegRecordOf( Guid recordId )
        {
            return (IPublicRecord)base.GetRegRecordOf( recordId);
        }
        new public IPublicRecord AddRegularRecord()
        {
            return (IPublicRecord)base.AddRegularRecord();
        }
        new public IPublicRecord AddRegularRecordAt( int pos )
        {			
            return (IPublicRecord)base.AddRegularRecordAt( pos);
        }

        public ISubcollection GetAllItems()
        {
            var list = new ICollectionItemBase[this.CountRegular];
            for (int i = 0; i < this.CountRegular; ++i)
            {
                list[i] = this.GetRegularRecordAt(i);
            }

            return CXmlRecordSubcollection.Create(list);
        }
    }
}
