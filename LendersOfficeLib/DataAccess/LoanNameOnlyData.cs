using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
	public class CLoanNameOnlyData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CLoanNameOnlyData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
	        list.Add("sLNm");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CLoanNameOnlyData(Guid fileId) : base(fileId, "CLoanNameOnlyData", s_selectProvider)
		{
		}
	}
}
