﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Common;

namespace DataAccess
{
    public class AggregateEscrowItem
    {
        private string m_mon;
        private int m_monInt;
        private decimal m_pmtFromEscrow;
        private decimal m_pmtToEscrow;
        private string m_desc = "";
        private decimal m_bal;
        private CPageBase m_dataLoan;

        public AggregateEscrowItem(CPageBase dataLoan, int month)
        {
            m_dataLoan = dataLoan;
            m_monInt = month;

            if (month == 1) m_mon = "January";
            else if (month == 2) m_mon = "February";
            else if (month == 3) m_mon = "March";
            else if (month == 4) m_mon = "April";
            else if (month == 5) m_mon = "May";
            else if (month == 6) m_mon = "June";
            else if (month == 7) m_mon = "July";
            else if (month == 8) m_mon = "August";
            else if (month == 9) m_mon = "September";
            else if (month == 10) m_mon = "October";
            else if (month == 11) m_mon = "November";
            else if (month == 12) m_mon = "December";
            else m_mon = "";
        }

        public string Mon
        {
            get { return m_mon; }
        }
        public int MonInt
        {
            get { return m_monInt; }
        }
        public decimal PmtFromEscrow
        {
            get { return m_pmtFromEscrow; }
            set { m_pmtFromEscrow = value; }
        }
        public string PmtFromEscrow_rep
        {
            get
            {
                try { return m_dataLoan.m_convertLos.ToMoneyString(PmtFromEscrow, FormatDirection.ToRep); }
                catch { return ""; }
            }
        }
        public decimal PmtToEscrow
        {
            get { return m_pmtToEscrow; }
            set { m_pmtToEscrow = value; }
        }
        public string PmtToEscrow_rep
        {
            get
            {
                if (m_dataLoan != null)
                {
                    return m_dataLoan.m_convertLos.ToMoneyString(this.PmtToEscrow, FormatDirection.ToRep);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Desc
        {
            get { return m_desc; }
            set { m_desc = value; }
        }
        public decimal Bal
        {
            get { return m_bal; }
            set { m_bal = value; }
        }
        public string Bal_rep
        {
            get
            {
                try { return m_dataLoan.m_convertLos.ToMoneyString(Bal, FormatDirection.ToRep); }
                catch { return ""; }
            }
        }
    }

}
