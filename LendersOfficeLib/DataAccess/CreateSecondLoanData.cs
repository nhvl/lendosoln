using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
	public class CCreateSecondLoanData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CCreateSecondLoanData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("sfSpinOffLoanForSubsequent2ndLienLoan");
            list.Add("sLienPosT");
            list.Add("sLinkedLoanInfo");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CCreateSecondLoanData(Guid fileId) : base(fileId, "CCreateSecondLoanData", s_selectProvider)
		{
		}
	}
}
