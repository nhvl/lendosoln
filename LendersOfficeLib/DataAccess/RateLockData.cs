using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
    public class CSaveRateLockData : CPageData 
    {
        private static CSelectStatementProvider s_selectProvider;
	    
        static CSaveRateLockData()
        {
            StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("sRLckdN");
            list.Add("sRLckdExpiredN");
            list.Add("sSubmitN");
            list.Add("sApprovD");
            list.Add("sApprovN");
            list.Add("sNoteIR");
            list.Add("sBrokComp1Pc");
            list.Add("sRAdjMarginR");
            list.Add("sTerm");
            list.Add("sDue");
            list.Add("sFinMethT");
            list.Add("sTrNotes");
            list.Add("sRLckdDays");
            list.Add("sPpmtPenaltyMon");
            list.Add("sSubmitD");
			list.Add("sQualIR");
			list.Add("sOptionArmTeaserR");
			list.Add("sIsOptionArm");
			list.Add("sLpTemplateNm");
            list.Add("sLpTemplateId");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }  
        public CSaveRateLockData(Guid fileId) : base(fileId, "CSaveRateLockData", s_selectProvider)
        {
        }

    }
    public class CPerformRateLockData : CPageData 
    {
        private static CSelectStatementProvider s_selectProvider;
	    
        static CPerformRateLockData()
        {
            StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("aBNm");
            list.Add("sNoteIR");
            list.Add("sBrokComp1Pc");
            list.Add("sRAdjMarginR");
            list.Add("sTerm");
            list.Add("sDue");
            list.Add("sFinMethT");
            list.Add("sIsRateLocked");
            list.Add("sRLckdDays");
            list.Add("sRLckdD");
            list.Add("sRLckdExpiredD");
            list.Add("sApprovD");
            list.Add("sRateLockHistoryXmlContent");
            list.Add("sEmployeeLoanRep");
            list.Add("sfLockRate");
            list.Add("sStatusT");
            list.Add("sReasonForNotAllowingResubmission");
            list.Add("sPpmtPenaltyMon");
			list.Add("sIsOptionArm");
			list.Add("sOptionArmTeaserR");
			list.Add("sQualIR");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
        public CPerformRateLockData(Guid fileId) : base(fileId, "CPerformRateLockData", s_selectProvider)
        {
        }

    }
    public class CPerformBreakRateLockData : CPageData 
    {
        private static CSelectStatementProvider s_selectProvider;
	    
        static CPerformBreakRateLockData()
        {
            StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("sIsRateLocked");
            list.Add("sRLckdD");
            list.Add("sRLckdExpiredD");
            list.Add("sRateLockHistoryXmlContent");
            list.Add("sApprovD");
            list.Add("sfBreakRateLock");
            list.Add("sStatusT");
            list.Add("sfRemoveRequestedRate");
            list.Add("sNoteIRSubmitted");
            list.Add("sLOrigFPcSubmitted");
            list.Add("sRAdjMarginRSubmitted");
            list.Add("sQualIRSubmitted");
            list.Add("sOptionArmTeaserRSubmitted");
            list.Add("sIsOptionArmSubmitted");
            list.Add("sTermSubmitted");
            list.Add("sDueSubmitted");
            list.Add("sFinMethTSubmitted");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
        public CPerformBreakRateLockData(Guid fileId) : base(fileId, "CPerformBreakRateLockData", s_selectProvider)
        {
        }

    }

    public class CPerformExtendRateLockData : CPageData 
    {
        private static CSelectStatementProvider s_selectProvider;

        static CPerformExtendRateLockData() 
        {
            StringList list = new StringList();

            #region Target Fields
            list.Add("sRLckdExpiredD");
            list.Add("sfExtendRateLock");
            list.Add("sRLckdDays");
            list.Add("sRateLockHistoryXmlContent");
            list.Add("sBrokComp1Pc");
            #endregion
            s_selectProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection);

        }
        public CPerformExtendRateLockData(Guid fileId) : base(fileId, "CPerformExtendRateLockData", s_selectProvider)
        {
        }

    }

}
