﻿// <copyright file="E_ClosingCostViewT.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   11/19/2014 4:12:53 PM 
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// An enum for different mode of closing cost section view.
    /// </summary>
    public enum E_ClosingCostViewT
    {
        /// <summary>
        /// Leave Blank.
        /// </summary>
        LeaveBlank = 0,

        /// <summary>
        /// View use in loan editor. Each section is by HUD line number (i.e 800, 900, etc...).
        /// </summary>
        LoanHud1,

        /// <summary>
        /// View use in loan editor. Each section is correspondent to GFE 2015.
        /// </summary>
        LoanClosingCost,

        /// <summary>
        /// View use in lender type editor. Each section is by HUD line number (i.e 800, 900, etc...).
        /// </summary>
        LenderTypeHud1,

        /// <summary>
        /// View use in lender type editor. Each section is correspondent to GFE 2015.
        /// </summary>
        LenderTypeEstimate,

        /// <summary>
        /// View used for seller responsible fees. Missing Reserves.
        /// </summary>
        SellerResponsibleLoanHud1,

        /// <summary>
        /// View used for seller responsible fees for Closing Disclosure. Missing Section F and G.
        /// </summary>
        SellerResponsibleLenderTypeEstimate,

        /// <summary>
        /// View used for both Borrower and Seller Responsible fees.  Contains all sections.  Section C fees with Did_Shop == false are moved into Section B.
        /// </summary>
        CombinedLenderTypeEstimate 
    }
}