﻿// <copyright file="SellerClosingCostFormula.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   7/9/2015
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;

    /// <summary>
    /// Class representing a seller responsible closing cost formula.
    /// </summary>
    public class SellerClosingCostFormula : LoanClosingCostFormula
    {
        /// <summary>
        /// Clones the given object.
        /// </summary>
        /// <returns>A seller formula without an associated closing cost fee. Please attach to a closing cost fee before use.</returns>
        public override object Clone()
        {
            SellerClosingCostFormula formula = (SellerClosingCostFormula)this.MemberwiseClone();
            formula.ParentClosingCostFee = null;
            return formula;
        }

        /// <summary>
        /// Ensure some values are properly set before serialization/deserialization.
        /// </summary>
        internal override void EnsureCorrectness()
        {
            base.EnsureCorrectness();
            
            // 12/30/2014 dd - Before serialization we need to make sure that when fee has dataloan object associate then
            // it must save away savedPercentTotalAmount.
            if (this.ParentClosingCostSet != null && this.ParentClosingCostSet.HasDataLoanAssociate)
            {
                this.SavedPercentTotalAmount = this.ParentClosingCostSet.GetPercentBaseAmount(this.PercentBaseT);
            }
        }
    }
}
