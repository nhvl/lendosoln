﻿// <copyright file="E_ClosingCostFeeMismoFeeT.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   12/29/2014 3:39:03 PM 
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// An enum base on MISMO RESPA Fee Type.
    /// </summary>
    public enum E_ClosingCostFeeMismoFeeT
    {
        /// <summary>
        /// Leave Blank.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// 203K Discount On Repairs.
        /// </summary>
        _203KDiscountOnRepairs = 1,

        /// <summary>
        /// 203K Permits.
        /// </summary>
        _203KPermits = 2,

        /// <summary>
        /// 203K Architectural And Engineering Fee.
        /// </summary>
        _203KArchitecturalAndEngineeringFee = 3,

        /// <summary>
        /// 203K Inspection Fee.
        /// </summary>
        _203KInspectionFee = 4,

        /// <summary>
        /// 203K Supplemental Origination Fee.
        /// </summary>
        _203KSupplementalOriginationFee = 5,

        /// <summary>
        /// 203K Consultant Fee.
        /// </summary>
        _203KConsultantFee = 6,

        /// <summary>
        /// 203K Title Update.
        /// </summary>
        _203KTitleUpdate = 7,

        /// <summary>
        /// Abstract Or Title Search Fee.
        /// </summary>
        AbstractOrTitleSearchFee = 8,

        /// <summary>
        /// Amortization Fee.
        /// </summary>
        AmortizationFee = 9,

        /// <summary>
        /// Fee paid to broker or lender to receive loan application for mortgage financing.
        /// </summary>
        ApplicationFee = 10,

        /// <summary>
        /// Appraisal Fee.
        /// </summary>
        AppraisalFee = 11,

        /// <summary>
        /// Assignment Fee.
        /// </summary>
        AssignmentFee = 12,

        /// <summary>
        /// Recording Fee For Assignment.
        /// </summary>
        AssignmentRecordingFee = 13,

        /// <summary>
        /// Assumption Fee.
        /// </summary>
        AssumptionFee = 14,

        /// <summary>
        /// Attorney Fee.
        /// </summary>
        AttorneyFee = 15,

        /// <summary>
        /// Bond Review Fee.
        /// </summary>
        BondReviewFee = 16,

        /// <summary>
        /// Tax Stamp For County Deed.
        /// </summary>
        CountyDeedTaxStampFee = 17,

        /// <summary>
        /// Tax Stamp For County Mortgage.
        /// </summary>
        CountyMortgageTaxStampFee = 18,

        /// <summary>
        /// CLO Access Fee.
        /// </summary>
        CLOAccessFee = 19,

        /// <summary>
        /// Commitment Fee.
        /// </summary>
        CommitmentFee = 20,

        /// <summary>
        /// Copy Fax Fee.
        /// </summary>
        CopyFaxFee = 21,

        /// <summary>
        /// Courier Fee.
        /// </summary>
        CourierFee = 22,

        /// <summary>
        /// Credit Report Fee.
        /// </summary>
        CreditReportFee = 23,

        /// <summary>
        /// Recording Fee For Deed.
        /// </summary>
        DeedRecordingFee = 24,

        /// <summary>
        /// Document Preparation Fee.
        /// </summary>
        DocumentPreparationFee = 25,

        /// <summary>
        /// Documentary Stamp Fee.
        /// </summary>
        DocumentaryStampFee = 26,

        /// <summary>
        /// Escrow Waiver Fee.
        /// </summary>
        EscrowWaiverFee = 27,

        /// <summary>
        /// Flood Certification.
        /// </summary>
        FloodCertification = 28,

        /// <summary>
        /// General Counsel Fee.
        /// </summary>
        GeneralCounselFee = 29,

        /// <summary>
        /// Home Inspection Fee.
        /// </summary>
        InspectionFee = 30,

        /// <summary>
        /// Loan Discount Points.
        /// </summary>
        LoanDiscountPoints = 31,

        /// <summary>
        /// Loan Origination Fee.
        /// </summary>
        LoanOriginationFee = 32,

        /// <summary>
        /// Modification Fee.
        /// </summary>
        ModificationFee = 33,

        /// <summary>
        /// Mortgage Broker Fee.
        /// </summary>
        MortgageBrokerFee = 34,

        /// <summary>
        /// Recording Fee For Mortgage.
        /// </summary>
        MortgageRecordingFee = 35,

        /// <summary>
        /// Municipal Lien Certificate Fee.
        /// </summary>
        MunicipalLienCertificateFee = 36,

        /// <summary>
        /// Recording Fee For Municipal Lien Certificate.
        /// </summary>
        MunicipalLienCertificateRecordingFee = 37,

        /// <summary>
        /// New Loan Administration Fee.
        /// </summary>
        NewLoanAdministrationFee = 38,

        /// <summary>
        /// Notary Fee.
        /// </summary>
        NotaryFee = 39,

        /// <summary>
        /// Other Fee.
        /// </summary>
        Other = 40,

        /// <summary>
        /// Pest Inspection Fee.
        /// </summary>
        PestInspectionFee = 41,

        /// <summary>
        /// Processing Fee.
        /// </summary>
        ProcessingFee = 42,

        /// <summary>
        /// Redraw Fee.
        /// </summary>
        RedrawFee = 43,

        /// <summary>
        /// Real Estate Commission Buyers Broker.
        /// </summary>
        RealEstateCommission = 44,

        /// <summary>
        /// Fee for inspection of the subject property after completion of construction, repairs or improvements.
        /// </summary>
        ReinspectionFee = 45,

        /// <summary>
        /// Recording Fee For Release.
        /// </summary>
        ReleaseRecordingFee = 46,

        /// <summary>
        /// USDA Rural Development Guarantee Fee.
        /// </summary>
        RuralHousingFee = 47,

        /// <summary>
        /// Settlement Fee.
        /// </summary>
        SettlementOrClosingFee = 48,

        /// <summary>
        /// Tax Stamp For State Deed.
        /// </summary>
        StateDeedTaxStampFee = 49,

        /// <summary>
        /// Tax Stamp For State Mortgage.
        /// </summary>
        StateMortgageTaxStampFee = 50,

        /// <summary>
        /// Survey Fee.
        /// </summary>
        SurveyFee = 51,

        /// <summary>
        /// Tax Related Service Fee.
        /// </summary>
        TaxRelatedServiceFee = 52,

        /// <summary>
        /// Title Examination Fee.
        /// </summary>
        TitleExaminationFee = 53,

        /// <summary>
        /// Title Insurance Binder Fee.
        /// </summary>
        TitleInsuranceBinderFee = 54,

        /// <summary>
        /// Title Insurance Fee.
        /// </summary>
        TitleInsuranceFee = 55,

        /// <summary>
        /// Underwriting Fee.
        /// </summary>
        UnderwritingFee = 56,

        /// <summary>
        /// Monitoring or Proof of Claim Fee charged as a result of a bankruptcy filing by the Borrower.
        /// </summary>
        BankruptcyMonitoringFee = 57,

        /// <summary>
        /// Fee paid to a state or local housing agency to participate in a special lending program.
        /// </summary>
        BondFee = 58,

        /// <summary>
        /// Title Closing Protection Letter Fee
        /// Fee charged for a statement or document issued by title insurance underwriters that sets forth an underwriter's responsibility for negligence, fraud and errors in closings performed by the underwriter's agents and approved attorneys.
        /// </summary>
        ClosingProtectionLetterFee = 59,

        /// <summary>
        /// Fee charged for reviewing an appraisal report and performing some basic research on value without going out to the field to view the property or the comparable properties included in the appraisal report.
        /// </summary>
        AppraisalDeskReviewFee = 60,

        /// <summary>
        /// Fee for transmitting or receiving the loan documents through email or by some other electronic means.
        /// </summary>
        ElectronicDocumentDeliveryFee = 61,

        /// <summary>
        /// Fee to set up and service the escrow account for the loan.
        /// </summary>
        EscrowServiceFee = 62,

        /// <summary>
        /// Fee charged for performing an appraisal review in the field – actually inspecting the property and comparable properties.
        /// </summary>
        AppraisalFieldReviewFee = 63,

        /// <summary>
        /// Fee charged to prepare and submit to the Home Owner’s, Condominium, or Co-Op Association Certification for the property securing the loan.
        /// </summary>
        CertificationFee = 64,

        /// <summary>
        /// Fee charged for registering/transferring the loan onto the Mortgage Electronic Registration System.
        /// </summary>
        MERSRegistrationFee = 65,

        /// <summary>
        /// In refinances, fee paid to prior lender to prepare and provide a bookkeeping statement of how much is owed on the loan being refinanced.
        /// </summary>
        PayoffRequestFee = 66,

        /// <summary>
        /// Fee charged for the services of a signing agent who, as an accommodation of the borrower, closes the loan at a specific time or at a location requested by the borrower.
        /// </summary>
        SigningAgentFee = 67,

        /// <summary>
        /// Fee paid to a lien holder with an existing lien on the property to subordinate its lien position to the Lender in the current loan.
        /// </summary>
        SubordinationFee = 68,

        /// <summary>
        /// Fee charged for obtaining a title endorsement (for which the original lender and/or borrower are the beneficiaries) that relates to the closing of the mortgage loan.
        /// </summary>
        TitleEndorsementFee = 69,

        /// <summary>
        /// Fee for wiring funds in connection with the loan.
        /// </summary>
        WireTransferFee = 70,

        /// <summary>
        /// Originator compensation.
        /// </summary>
        LoanOriginatorCompensation = 71,

        /// <summary>
        /// Fee paid to title company for issuance of lender's title insurance policy that indemnifies the lender in the event that clear ownership of property is challenged by the discovery of faults in the title.
        /// </summary>
        TitleLendersCoveragePremium = 72,

        /// <summary>
        /// Fee paid to title company for issuance of owner's title insurance policy that indemnifies the owner of real estate in the event that his or her clear ownership of property is challenged by the discovery of faults in the title. This coverage is optional.
        /// </summary>
        TitleOwnersCoveragePremium = 73,

        /// <summary>
        /// Tax Stamp For City Deed.
        /// </summary>
        CityDeedTaxStampFee = 74,

        /// <summary>
        /// Tax Stamp For City Mortgage.
        /// </summary>
        CityMortgageTaxStampFee = 75,

        /// <summary>
        /// Appraisal Management Company Fee.
        /// </summary>
        AppraisalManagementCompanyFee = 76,

        /// <summary>
        /// Asbestos Inspection Fee.
        /// </summary>
        AsbestosInspectionFee = 77,

        /// <summary>
        /// Automated Underwriting Fee.
        /// </summary>
        AutomatedUnderwritingFee = 78,

        /// <summary>
        /// A.V.M. Fee.
        /// </summary>
        AVMFee = 79,

        /// <summary>
        /// Condominium Association Dues.
        /// </summary>
        CondominiumAssociationDues = 80,

        /// <summary>
        /// Condominium Association Special Assessment.
        /// </summary>
        CondominiumAssociationSpecialAssessment = 81,

        /// <summary>
        /// Cooperative Association Dues.
        /// </summary>
        CooperativeAssociationDues = 82,

        /// <summary>
        /// Cooperative Association Special Assessment.
        /// </summary>
        CooperativeAssociationSpecialAssessment = 83,

        /// <summary>
        /// Credit Disability Insurance Premium.
        /// </summary>
        CreditDisabilityInsurancePremium = 84,

        /// <summary>
        /// Credit Life Insurance Premium.
        /// </summary>
        CreditLifeInsurancePremium = 85,

        /// <summary>
        /// Credit Property Insurance Premium.
        /// </summary>
        CreditPropertyInsurancePremium = 86,

        /// <summary>
        /// Credit Unemployment Insurance Premium.
        /// </summary>
        CreditUnemploymentInsurancePremium = 87,

        /// <summary>
        /// Debt Cancellation Insurance Premium.
        /// </summary>
        DebtCancellationInsurancePremium = 88,

        /// <summary>
        /// Deed Preparation Fee.
        /// </summary>
        DeedPreparationFee = 89,

        /// <summary>
        /// Disaster Inspection Fee.
        /// </summary>
        DisasterInspectionFee = 90,

        /// <summary>
        /// Dry Wall Inspection Fee.
        /// </summary>
        DryWallInspectionFee = 91,

        /// <summary>
        /// Electrical Inspection Fee.
        /// </summary>
        ElectricalInspectionFee = 92,

        /// <summary>
        /// Environmental Inspection Fee.
        /// </summary>
        EnvironmentalInspectionFee = 93,

        /// <summary>
        /// Filing Fee.
        /// </summary>
        FilingFee = 94,

        /// <summary>
        /// Foundation Inspection Fee.
        /// </summary>
        FoundationInspectionFee = 95,

        /// <summary>
        /// Heating Cooling Inspection Fee.
        /// </summary>
        HeatingCoolingInspectionFee = 96,

        /// <summary>
        /// High Cost Mortgage Counseling Fee.
        /// </summary>
        HighCostMortgageCounselingFee = 97,

        /// <summary>
        /// Homeowners Association Dues.
        /// </summary>
        HomeownersAssociationDues = 98,

        /// <summary>
        /// Homeowners Association Special Assessment.
        /// </summary>
        HomeownersAssociationSpecialAssessment = 99,

        /// <summary>
        /// Home Warranty Fee.
        /// </summary>
        HomeWarrantyFee = 100,

        /// <summary>
        /// Lead Inspection Fee.
        /// </summary>
        LeadInspectionFee = 101,

        /// <summary>
        /// Lenders Attorney Fee.
        /// </summary>
        LendersAttorneyFee = 102,

        /// <summary>
        /// Loan Level Price Adjustment.
        /// </summary>
        LoanLevelPriceAdjustment = 103,

        /// <summary>
        /// Manual Underwriting Fee.
        /// </summary>
        ManualUnderwritingFee = 104,

        /// <summary>
        /// Manufactured Housing Inspection Fee.
        /// </summary>
        ManufacturedHousingInspectionFee = 105,

        /// <summary>
        /// MI Upfront Premium.
        /// </summary>
        MIUpfrontPremium = 106,

        /// <summary>
        /// Mold Inspection Fee.
        /// </summary>
        MoldInspectionFee = 107,

        /// <summary>
        /// Mortgage Surcharge County Or Parish.
        /// </summary>
        MortgageSurchargeCountyOrParish = 108,

        /// <summary>
        /// Mortgage Surcharge Municipal.
        /// </summary>
        MortgageSurchargeMunicipal = 109,

        /// <summary>
        /// Mortgage Surcharge State.
        /// </summary>
        MortgageSurchargeState = 110,

        /// <summary>
        /// Plumbing Inspection Fee.
        /// </summary>
        PlumbingInspectionFee = 111,

        /// <summary>
        /// Power Of Attorney Preparation Fee.
        /// </summary>
        PowerOfAttorneyPreparationFee = 112,

        /// <summary>
        /// Power Of Attorney Recording Fee.
        /// </summary>
        PowerOfAttorneyRecordingFee = 113,

        /// <summary>
        /// Preclosing Verification Control Fee.
        /// </summary>
        PreclosingVerificationControlFee = 114,

        /// <summary>
        /// Property Inspection Waiver Fee.
        /// </summary>
        PropertyInspectionWaiverFee = 115,

        /// <summary>
        /// Property Tax Status Research Fee.
        /// </summary>
        PropertyTaxStatusResearchFee = 116,

        /// <summary>
        /// Radon Inspection Fee.
        /// </summary>
        RadonInspectionFee = 117,

        /// <summary>
        /// Rate Lock Fee.
        /// </summary>
        RateLockFee = 118,

        /// <summary>
        /// Reconveyance Fee.
        /// </summary>
        ReconveyanceFee = 119,

        /// <summary>
        /// Recording Fee For Subordination.
        /// </summary>
        RecordingFeeForSubordination = 120,

        /// <summary>
        /// Recording Fee Total.
        /// </summary>
        RecordingFeeTotal = 121,

        /// <summary>
        /// Repairs Fee.
        /// </summary>
        RepairsFee = 122,

        /// <summary>
        /// Roof Inspection Fee.
        /// </summary>
        RoofInspectionFee = 123,

        /// <summary>
        /// Septic Inspection Fee.
        /// </summary>
        SepticInspectionFee = 124,

        /// <summary>
        /// Smoke Detector Inspection Fee.
        /// </summary>
        SmokeDetectorInspectionFee = 125,

        /// <summary>
        /// State Title Insurance Fee.
        /// </summary>
        StateTitleInsuranceFee = 126,

        /// <summary>
        /// Structural Inspection Fee.
        /// </summary>
        StructuralInspectionFee = 127,

        /// <summary>
        /// Temporary Buydown Administration Fee.
        /// </summary>
        TemporaryBuydownAdministrationFee = 128,

        /// <summary>
        /// Temporary Buydown Points.
        /// </summary>
        TemporaryBuydownPoints = 129,

        /// <summary>
        /// Title Certification Fee.
        /// </summary>
        TitleCertificationFee = 130,

        /// <summary>
        /// Title Closing Fee.
        /// </summary>
        TitleClosingFee = 131,

        /// <summary>
        /// Title Document Preparation Fee.
        /// </summary>
        TitleDocumentPreparationFee = 132,

        /// <summary>
        /// Title Final Policy Short Form Fee.
        /// </summary>
        TitleFinalPolicyShortFormFee = 133,

        /// <summary>
        /// Title Notary Fee.
        /// </summary>
        TitleNotaryFee = 134,

        /// <summary>
        /// Title Services Sales Tax.
        /// </summary>
        TitleServicesSalesTax = 135,

        /// <summary>
        /// Title Underwriting Issue Resolution Fee.
        /// </summary>
        TitleUnderwritingIssueResolutionFee = 136,

        /// <summary>
        /// Transfer Tax Total.
        /// </summary>
        TransferTaxTotal = 137,

        /// <summary>
        /// Verification Of Assets Fee.
        /// </summary>
        VerificationOfAssetsFee = 138,

        /// <summary>
        /// Verification Of Employment Fee.
        /// </summary>
        VerificationOfEmploymentFee = 139,

        /// <summary>
        /// Verification Of Income Fee.
        /// </summary>
        VerificationOfIncomeFee = 140,

        /// <summary>
        /// Verification Of Residency Status Fee.
        /// </summary>
        VerificationOfResidencyStatusFee = 141,

        /// <summary>
        /// Verification Of Taxpayer IdentificationFee.
        /// </summary>
        VerificationOfTaxpayerIdentificationFee = 142,

        /// <summary>
        /// Verification Of Tax Return Fee.
        /// </summary>
        VerificationOfTaxReturnFee = 143,

        /// <summary>
        /// Water Testing Fee.
        /// </summary>
        WaterTestingFee = 144,

        /// <summary>
        /// Well Inspection Fee.
        /// </summary>
        WellInspectionFee = 145,

        /// <summary>
        /// VA Funding Fee.
        /// </summary>
        VAFundingFee = 146,

        /// <summary>
        /// MI Initial Premium.
        /// </summary>
        MIInitialPremium = 147,

        /// <summary>
        /// Chosen Interest Rate Credit Or Charge Total.
        /// </summary>
        ChosenInterestRateCreditOrChargeTotal = 148,

        /// <summary>
        /// Our Origination Charge Total.
        /// </summary>
        OurOriginationChargeTotal = 149,

        /// <summary>
        /// Title Services Fee Total.
        /// </summary>
        TitleServicesFeeTotal = 150,

        /// <summary>
        /// Real Estate Commission Sellers Broker.
        /// </summary>
        RealEstateCommissionSellersBroker = 151,
    }
}