﻿// <copyright file="CombinedClosingCostSet.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   7/9/2015
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.Security;

    /// <summary>
    /// Set for the combined closing disclosure. Derives from BorrowerSet without an associated data loan so it pulls directly from the data loan.
    /// </summary>
    public class CombinedClosingCostSet : BorrowerClosingCostSet
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CombinedClosingCostSet" /> class.
        /// </summary>
        /// <param name="json">The json to deserialize.</param>
        /// <param name="converter">The converter to use.</param>
        public CombinedClosingCostSet(string json, LosConvert converter)
            : base(json, converter)
        { 
        }

        /// <summary>
        /// Gets or sets a value indicating what type of closing cost set this is.
        /// </summary>
        /// <value>A value indicating what type of closing cost set this is.</value>
        public override E_sClosingCostSetType SetType
        {
            get
            {
                return E_sClosingCostSetType.BorrowerAndSellerFees;
            }

            protected set
            {
            }
        }

        /// <summary>
        /// Gets a value indicating whether an actual data loan object is associate with this closing cost set.
        /// </summary>
        /// <value>A value indicating whether an actual data loan object is associate with this closing cost set.</value>
        public override bool HasDataLoanAssociate
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the data loan object.
        /// </summary>
        internal override CPageBase DataLoan
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this set skips creating automatic fees and checks on enumeration.
        /// </summary>
        /// <value>Indicates whether this set skips creating automatic fees and checks on enumeration.</value>
        internal override bool SkipInitAndEnumerable
        {
            get
            {
                return true;
            }

            set
            {
            }
        }

        /// <summary>
        /// Adds a new fee to the set.  If it already exists, it's still added as another copy.  This method also bypasses the check to ensure that only fees from a set containing
        /// the same ClosingCostSetType are added together.  DO NOT USE THIS WHEN SAVING!  THIS IS ONLY FOR READ-ONLY PURPOSES.
        /// </summary>
        /// <param name="fee">The fee to be added to the set.</param>
        public void AddExistingFeeWithDuplicates(BorrowerClosingCostFee fee)
        {
            if (fee == null)
            {
                throw new ArgumentNullException("fee");
            }

            fee.SetClosingCostSet(this);

            this.ClearFeeLookupCache();
            this.ClosingCostFeeList.Add(fee);
        }

        /// <summary>
        /// Returns a compressed version of a view type, where payments with the same paid by values are combined into one.  This is set is not be used for editing fees, it is for
        /// read-only purposes.
        /// </summary>
        /// <param name="viewT">The type of view to be returned.</param>
        /// <param name="optionalCollapseBorrFin">Optional parameter indicating if the borrower financed payments should be collapsed with the borrower paid payments.</param>
        /// <returns>A compressed version of a view.</returns>
        public IEnumerable<BorrowerClosingCostFeeSection> GetCompressedView(E_ClosingCostViewT viewT, bool optionalCollapseBorrFin = false)
        {
            List<Func<LoanClosingCostFeePayment, bool>> paidByChecks = new List<Func<LoanClosingCostFeePayment, bool>>();
            paidByChecks.Add(p => p.PaidByT == E_ClosingCostFeePaymentPaidByT.Seller);
            if (optionalCollapseBorrFin)
            {
                paidByChecks.Add(p => p.PaidByT == E_ClosingCostFeePaymentPaidByT.Borrower || p.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance);
            }
            else
            {
                paidByChecks.Add(p => p.PaidByT == E_ClosingCostFeePaymentPaidByT.Borrower);
                paidByChecks.Add(p => p.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance);
            }

            IEnumerable<BorrowerClosingCostFeeSection> compressedView = this.GetViewForSerialization(viewT);
            foreach (BorrowerClosingCostFeeSection section in compressedView)
            {
                foreach (BorrowerClosingCostFee fee in section.FilteredClosingCostFeeList)
                {
                    List<BorrowerClosingCostFeePayment> removedPayments = new List<BorrowerClosingCostFeePayment>();
                    foreach (Func<LoanClosingCostFeePayment, bool> paidByCheck in paidByChecks)
                    {
                        Func<LoanClosingCostFeePayment, bool> rightAtClosingPred = p => paidByCheck(p) && p.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.AtClosing;

                        var rightAtClosingPayments = fee.Payments.Where(rightAtClosingPred);
                        if (rightAtClosingPayments.Count() > 1)
                        {
                            foreach (BorrowerClosingCostFeePayment payment in rightAtClosingPayments)
                            {
                                if (payment == rightAtClosingPayments.First() && !paidByCheck(payment))
                                {
                                    continue;
                                }
                                else
                                {
                                    rightAtClosingPayments.First().Amount += payment.Amount;
                                    removedPayments.Add(payment);
                                }
                            }
                        }

                        Func<LoanClosingCostFeePayment, bool> beforeClosingPred = p => paidByCheck(p) && p.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing;
                        var beforeClosingPayments = fee.Payments.Where(beforeClosingPred);
                        if (beforeClosingPayments.Count() > 1)
                        {
                            foreach (BorrowerClosingCostFeePayment payment in beforeClosingPayments)
                            {
                                if (payment == beforeClosingPayments.First())
                                {
                                    continue;
                                }
                                else
                                {
                                    beforeClosingPayments.First().Amount += payment.Amount;
                                    removedPayments.Add(payment);
                                }
                            }
                        }
                    }

                    Func<LoanClosingCostFeePayment, bool> closingOtherPred = p => (
                        p.PaidByT == E_ClosingCostFeePaymentPaidByT.Other || p.PaidByT == E_ClosingCostFeePaymentPaidByT.Broker || 
                        p.PaidByT == E_ClosingCostFeePaymentPaidByT.Lender || p.PaidByT == E_ClosingCostFeePaymentPaidByT.LeaveBlank);

                    var closingOtherPayments = fee.Payments.Where(closingOtherPred);
                    if (closingOtherPayments.Count() > 1)
                    {
                        foreach (BorrowerClosingCostFeePayment payment in closingOtherPayments)
                        {
                            if (payment == closingOtherPayments.First())
                            {
                                continue;
                            }
                            else
                            {
                                closingOtherPayments.First().Amount += payment.Amount;
                                removedPayments.Add(payment);
                            }
                        }
                    }

                    foreach (BorrowerClosingCostFeePayment payment in removedPayments)
                    {
                        fee.RemovePayment(payment);
                    }
                }
            }

            return compressedView;
        }

        /// <summary>
        /// Gets a list of appropriate fee sections by view type. DO NOT USE THIS FOR SERIALIZATION/DESERIALIZATION.
        /// </summary>
        /// <param name="viewType">The view to construct the sections on.</param>
        /// <returns>The list of fee sections.</returns>
        public override IEnumerable<BaseClosingCostFeeSection> GetViewBase(E_ClosingCostViewT viewType)
        {
            if (viewType != E_ClosingCostViewT.CombinedLenderTypeEstimate)
            {
                throw new ArgumentException("CombinedClosingCostSet does not take view type: " + Enum.GetName(typeof(E_ClosingCostViewT), viewType));
            }

            List<BaseClosingCostFeeSection> list = new List<BaseClosingCostFeeSection>();

            Func<BaseClosingCostFee, bool> sectionFilter;
            if (this.HasDataLoanAssociate)
            {
                sectionFilter = bFee => ClosingCostSetUtils.PrepsSecGMIFilter((BorrowerClosingCostFee)bFee) &&
                                        ClosingCostSetUtils.OrigCompFilter((BorrowerClosingCostFee)bFee, this.DataLoan.sOriginatorCompensationPaymentSourceT, this.DataLoan.sDisclosureRegulationT, this.DataLoan.sGfeIsTPOTransaction);
            }
            else if (this.HasClosingCostArchiveAssociate)
            {
                sectionFilter = bfee => ClosingCostSetUtils.PrepsSecGMIFilter((BorrowerClosingCostFee)bfee) &&
                                        ClosingCostSetUtils.OrigCompFilterWithArchive((BorrowerClosingCostFee)bfee, this.ClosingCostArchive);
            }
            else
            {
                sectionFilter = bFee => ClosingCostSetUtils.PrepsSecGMIFilter((BorrowerClosingCostFee)bFee);
            }

            switch (viewType)
            {
                case E_ClosingCostViewT.CombinedLenderTypeEstimate:
                    BorrowerClosingCostFee defaultSectionAFee = new BorrowerClosingCostFee()
                    {
                        HudLine = 800,
                        IsTitleFee = false,
                        IsOptional = false,
                        GfeSectionT = E_GfeSectionT.B1,
                        IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionA,
                        IsApr = true,
                        IsFhaAllowable = true,
                        Beneficiary = E_AgentRoleT.Lender,
                        IsThirdParty = false,
                        IsAffiliate = false,
                        CanShop = false,
                    };
                    defaultSectionAFee.SetClosingCostSet(this);

                    BorrowerClosingCostFee defaultSectionBFee = new BorrowerClosingCostFee()
                    {
                        HudLine = 800,
                        IsTitleFee = false,
                        IsOptional = false,
                        GfeSectionT = E_GfeSectionT.B3,
                        IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionB,
                        IsApr = true,
                        IsFhaAllowable = true,
                        Beneficiary = E_AgentRoleT.Other,
                        IsThirdParty = false,
                        IsAffiliate = false,
                        CanShop = false,
                    };
                    defaultSectionBFee.SetClosingCostSet(this);

                    BorrowerClosingCostFee defaultSectionCFee = new BorrowerClosingCostFee()
                    {
                        HudLine = 1300,
                        IsTitleFee = false,
                        IsOptional = false,
                        GfeSectionT = E_GfeSectionT.B6,
                        IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionC,
                        IsApr = true,
                        IsFhaAllowable = true,
                        Beneficiary = E_AgentRoleT.Other,
                        IsThirdParty = false,
                        IsAffiliate = false,
                        CanShop = true,
                    };
                    defaultSectionCFee.SetClosingCostSet(this);

                    BorrowerClosingCostFee defaultSectionEFee = new BorrowerClosingCostFee()
                    {
                        HudLine = 1200,
                        IsTitleFee = false,
                        IsOptional = false,
                        GfeSectionT = E_GfeSectionT.B8,
                        IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionE,
                        IsApr = false,
                        IsFhaAllowable = true,
                        Beneficiary = E_AgentRoleT.Other,
                        IsThirdParty = false,
                        IsAffiliate = false,
                        CanShop = false,
                    };
                    defaultSectionEFee.SetClosingCostSet(this);

                    BorrowerClosingCostFee defaultSectionFFee = new BorrowerClosingCostFee()
                    {
                        HudLine = 900,
                        IsTitleFee = false,
                        IsOptional = false,
                        GfeSectionT = E_GfeSectionT.B11,
                        IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionF,
                        IsApr = false,
                        IsFhaAllowable = true,
                        Beneficiary = E_AgentRoleT.Lender,
                        IsThirdParty = false,
                        IsAffiliate = false,
                        CanShop = true,
                    };
                    defaultSectionFFee.SetClosingCostSet(this);

                    BorrowerClosingCostFee defaultSectionGFee = new BorrowerClosingCostFee();
                    defaultSectionGFee.IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionG;
                    defaultSectionGFee.Beneficiary = E_AgentRoleT.Lender;
                    defaultSectionGFee.ClosingCostFeeTypeId = DefaultSystemClosingCostFee.DefaultSectionGFeeTypeId;
                    defaultSectionGFee.SetClosingCostSet(this);

                    BorrowerClosingCostFee defaultSectionHFee = new BorrowerClosingCostFee()
                    {
                        HudLine = 1300,
                        IsTitleFee = false,
                        IsOptional = true,
                        GfeSectionT = E_GfeSectionT.B6,
                        IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionH,
                        IsApr = false,
                        IsFhaAllowable = true,
                        Beneficiary = E_AgentRoleT.Other,
                        IsThirdParty = false,
                        IsAffiliate = false,
                        CanShop = true,
                    };
                    defaultSectionHFee.SetClosingCostSet(this);

                    list.Add(new BorrowerClosingCostFeeSection(viewType, this, sectionFilter, ClosingCostSetUtils.SectionAOriginationCharges, o => o.CheckIfFeeIsInSection(ClosingCostSetUtils.SectionAFilter), defaultSectionAFee, ClosingCostSetUtils.Start800Hudline, ClosingCostSetUtils.End1300Hudline, E_IntegratedDisclosureSectionT.SectionA));
                    list.Add(new BorrowerClosingCostFeeSection(viewType, this, sectionFilter, "B - Services You Did Not Shop For", o => o.CheckIfFeeIsInSection(ClosingCostSetUtils.SectionBFilter) || (o.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionC && o.DidShop == false), defaultSectionBFee, ClosingCostSetUtils.Start800Hudline, ClosingCostSetUtils.End1300Hudline, E_IntegratedDisclosureSectionT.SectionB));
                    list.Add(new BorrowerClosingCostFeeSection(viewType, this, sectionFilter, "C - Services You Did Shop For", o => o.CheckIfFeeIsInSection(ClosingCostSetUtils.SectionCFilter) && o.DidShop, defaultSectionCFee, ClosingCostSetUtils.Start800Hudline, ClosingCostSetUtils.End1300Hudline, E_IntegratedDisclosureSectionT.SectionC));
                    list.Add(new BorrowerClosingCostFeeSection(viewType, this, sectionFilter, ClosingCostSetUtils.SectionETaxes, o => o.CheckIfFeeIsInSection(ClosingCostSetUtils.SectionEFilter), defaultSectionEFee, ClosingCostSetUtils.Start800Hudline, ClosingCostSetUtils.End1300Hudline, E_IntegratedDisclosureSectionT.SectionE));
                    list.Add(new BorrowerClosingCostFeeSection(viewType, this, sectionFilter, ClosingCostSetUtils.SectionFPrepaids, o => o.CheckIfFeeIsInSection(ClosingCostSetUtils.SectionFFilter), defaultSectionFFee, ClosingCostSetUtils.Start800Hudline, ClosingCostSetUtils.End1300Hudline, E_IntegratedDisclosureSectionT.SectionF));
                    list.Add(new BorrowerClosingCostFeeSection(viewType, this, sectionFilter, ClosingCostSetUtils.SectionGInitialEscrow, o => o.CheckIfFeeIsInSection(ClosingCostSetUtils.SectionGFilter), defaultSectionGFee, ClosingCostSetUtils.Start800Hudline, ClosingCostSetUtils.End1300Hudline, E_IntegratedDisclosureSectionT.SectionG));
                    list.Add(new BorrowerClosingCostFeeSection(viewType, this, sectionFilter, ClosingCostSetUtils.SectionHOther, o => o.CheckIfFeeIsInSection(ClosingCostSetUtils.SectionHFilter), defaultSectionHFee, ClosingCostSetUtils.Start800Hudline, ClosingCostSetUtils.End1300Hudline, E_IntegratedDisclosureSectionT.SectionH));

                    break;
                case E_ClosingCostViewT.LeaveBlank:
                case E_ClosingCostViewT.LenderTypeEstimate:
                case E_ClosingCostViewT.LenderTypeHud1:
                case E_ClosingCostViewT.LoanClosingCost:
                case E_ClosingCostViewT.LoanHud1:
                case E_ClosingCostViewT.SellerResponsibleLenderTypeEstimate:
                case E_ClosingCostViewT.SellerResponsibleLoanHud1:
                default:
                    throw new UnhandledEnumException(viewType);
            }

            return list;
        }
    }
}
