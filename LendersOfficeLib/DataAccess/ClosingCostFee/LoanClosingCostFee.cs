﻿// <copyright file="LoanClosingCostFee.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   7/9/2015
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// The base class for all closing cost fees that exist inside of a loan. DO NOT SERIALIZE/DESERIALIZE DIRECTLY INTO THIS CLASS.
    /// </summary>
    [DataContract]
    [KnownType(typeof(SellerClosingCostFee))]
    [KnownType(typeof(BorrowerClosingCostFee))]
    public abstract class LoanClosingCostFee : BaseClosingCostFee
    {
        /* NOTE: If deriving from this class, please make sure the child class has two properties, Formula and PaymentList that return
         * the appropriate Formula and Payment type. 
         * See SellerClosingCostFee and BorrowerClosingCostFee for examples
         */

        /// <summary>
        /// The provider who provided the service and will ultimately receive the payment for it.
        /// </summary>
        private E_AgentRoleT beneficiaryImpl;

        /// <summary>
        /// Beneficiary (Paid To) free form description.
        /// </summary>
        private string beneficiaryDescriptionImpl;

        /// <summary>
        /// The responsible party for the fee.
        /// </summary>
        private E_GfeResponsiblePartyT responsiblePartyInternal;

        /// <summary>
        /// Gets the fee's parent closing cost set. Only a LoanClosingCostSet can be associated with a LoanClosingCostFee.
        /// </summary>
        /// <value>The parent closing cost set.</value>
        public LoanClosingCostSet ParentClosingCostSet
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether it is a third party.
        /// </summary>
        /// <value>Whether is is a third party.</value>
        public override bool IsThirdParty
        {
            get
            {
                // Return automated value if possible.
                // Fee must have BeneficiaryAgentId set and dataloan accessible in order for automation to work.
                if (!this.DisableBeneficiaryAutomation &&
                    this.BeneficiaryAgentId != Guid.Empty &&
                    this.ParentClosingCostSet != null &&
                    this.ParentClosingCostSet.HasDataLoanAssociate)
                {
                    // If TP & AFF aren't set to manual and if loan is non-correspondent,
                    // set true if this agent is NOT the lender or originator.
                    CPageBase dataLoan = this.ParentClosingCostSet.DataLoan;
                    if (!dataLoan.sIsManuallySetThirdPartyAffiliateProps &&
                        !(dataLoan.sBranchChannelT == E_BranchChannelT.Correspondent || dataLoan.sBranchChannelT == E_BranchChannelT.Blank))
                    {
                        // If agent is found, set value based on agent fields
                        CAgentFields officialContact = this.ParentClosingCostSet.DataLoan.GetAgentFields(this.BeneficiaryAgentId);
                        if (!officialContact.IsNewRecord)
                        {
                            this.IsThirdPartyImpl = !(officialContact.IsLender || officialContact.IsOriginator);
                        }

                        // TODO: THROW IF AGENT NOT FOUND (officialContact.IsNewRecord == true)?
                    }
                }

                // Returns manually set value if fee cannot be automated.
                return this.IsThirdPartyImpl;
            }

            set
            {
                this.IsThirdPartyImpl = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether provider is affiliate with lender.
        /// </summary>
        /// <value>Whether provider is affiliate with lender.</value>
        public override bool IsAffiliate
        {
            get
            {
                if (this.IsThirdParty == false)
                {
                    this.IsAffiliateImpl = false;

                    return this.IsAffiliateImpl;
                }

                // Return automated value if possible.
                // Fee must have BeneficiaryAgentId set and dataloan accessible in order for automation to work.
                if (!this.DisableBeneficiaryAutomation &&
                    this.BeneficiaryAgentId != Guid.Empty &&
                    this.ParentClosingCostSet != null &&
                    this.ParentClosingCostSet.HasDataLoanAssociate)
                {
                    // If TP & AFF aren't set to manual and if loan is non-correspondent,
                    // set true if this agent is an affiliate of lender or originator.
                    CPageBase dataLoan = this.ParentClosingCostSet.DataLoan;
                    if (!dataLoan.sIsManuallySetThirdPartyAffiliateProps &&
                        !(dataLoan.sBranchChannelT == E_BranchChannelT.Correspondent || dataLoan.sBranchChannelT == E_BranchChannelT.Blank))
                    {
                        // If agent is found, set value based on agent fields
                        CAgentFields officialContact = this.ParentClosingCostSet.DataLoan.GetAgentFields(this.BeneficiaryAgentId);
                        if (!officialContact.IsNewRecord)
                        {
                            this.IsAffiliateImpl = officialContact.IsLenderAffiliate || officialContact.IsOriginatorAffiliate;
                        }

                        // TODO: THROW IF AGENT NOT FOUND (officialContact.IsNewRecord == true)?
                    }
                }

                // Returns manually set value if fee cannot be automated.
                return this.IsAffiliateImpl;
            }

            set
            {
                base.IsAffiliate = value;
            }
        }

        /// <summary>
        /// Gets or sets the provider who provided the service and will ultimately receive the payment for it.
        /// </summary>
        /// <value>The provide who provided the service and will ultimately receive the payment for it.</value>
        public override E_AgentRoleT Beneficiary
        {
            get
            {
                // Return automated value if possible.
                // Fee must have BeneficiaryAgentId set and dataloan accessible in order for automation to work.
                if (!this.DisableBeneficiaryAutomation &&
                    this.BeneficiaryAgentIdImpl != Guid.Empty &&
                    this.ParentClosingCostSet != null &&
                    this.ParentClosingCostSet.HasDataLoanAssociate)
                {
                    // If agent is found, set value based on agent fields
                    CAgentFields officialContact = this.ParentClosingCostSet.DataLoan.GetAgentFields(this.BeneficiaryAgentIdImpl);
                    if (!officialContact.IsNewRecord)
                    {
                        this.beneficiaryImpl = officialContact.AgentRoleT;
                    }

                    // TODO: THROW IF AGENT NOT FOUND (officialContact.IsNewRecord == true)?
                }

                // Returns manually set value if fee cannot be automated.
                return this.beneficiaryImpl;
            }

            set
            {
                // Only do stuff if the value has actually changed.
                if (value == this.beneficiaryImpl)
                {
                    return;
                }

                this.beneficiaryImpl = value;

                // Clear out beneficiary agent id and other fields that could affect automation.
                // Fields will be reset by automation.
                this.ClearBeneficiary();

                // re-enable automation if it was disabled by migration.
                this.DisableBeneficiaryAutomation = false;
            }
        }

        /// <summary>
        /// Gets the human readable description of Beneficiary enumeration.
        /// </summary>
        /// <value>Beneficiary human readable description.</value>
        public string BeneficiaryType
        {
            get
            {
                return LendersOffice.Rolodex.RolodexDB.GetTypeDescription(this.Beneficiary);
            }
        }

        /// <summary>
        /// Gets the agent fields of the provider who provided the service and will ultimately receive the payment for it.
        /// </summary>
        /// <value>The provide who provided the service and will ultimately receive the payment for it.  May return null.</value>
        public CAgentFields BeneficiaryAgent
        {
            get
            {
                if (!this.DisableBeneficiaryAutomation && this.BeneficiaryAgentIdImpl != Guid.Empty
                    && this.ParentClosingCostSet != null && this.ParentClosingCostSet.HasClosingCostArchiveAssociate)
                {
                    // ideally the data set should be cached on the archive object.
                    var ds = this.ParentClosingCostSet.ClosingCostArchive.GetCachedAgentDataSet();
                    var agent = new CAgentFields(this.ParentClosingCostSet.DataLoan, ds, this.BeneficiaryAgentIdImpl);
                    if (!agent.IsNewRecord)
                    {
                        return agent;
                    }
                }

                // Return automated value if possible.
                // Fee must have BeneficiaryAgentId set and dataloan accessible in order for automation to work.
                if (!this.DisableBeneficiaryAutomation && this.BeneficiaryAgentIdImpl != Guid.Empty &&
                    this.ParentClosingCostSet != null && this.ParentClosingCostSet.HasDataLoanAssociate)
                {
                    // If agent is found, set value based on agent fields
                    CAgentFields officialContact = this.ParentClosingCostSet.DataLoan.GetAgentFields(this.BeneficiaryAgentIdImpl);
                    if (!officialContact.IsNewRecord)
                    {
                        return officialContact;
                    }
                }

                // Returns manually set value if fee cannot be automated.
                return null;
            }
        }
        
        /// <summary>
        /// Gets a value indicating whether this fee is one of the special prepaid fees that flow from the housing expenses.
        /// </summary>
        /// <value>The value indicating whether this fee is one of the special prepaid fees that flow from the housing expenses.</value>
        public bool IsPrepaidFromExpensesFee
        {
            get
            {
                return ClosingCostSetUtils.ConstantPrepaidEscrowFeeList.Contains(this.ClosingCostFeeTypeId);
            }
        }

        /// <summary>
        /// Gets or sets the original description of the fee. The original is come from lender level settings.
        /// </summary>
        /// <value>The original description of the fee.</value>
        [DataMember(Name = "org_desc")]
        public string OriginalDescription
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether a borrower did shop for provider.
        /// </summary>
        /// <value>Whether a borrower did shop for provider.</value>
        [DataMember(Name = "did_shop")]
        public bool DidShop
        {
            get
            {
                if (this.CanShop == false)
                {
                    this.DidShopImpl = false;
                }

                return this.DidShopImpl;
            }

            set
            {
                this.DidShopImpl = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not beneficiary automation is disabled.
        /// </summary>
        /// <value>Whether or not beneficiary automation is disabled.</value>
        [DataMember(Name = "disable_bene_auto")]
        public bool DisableBeneficiaryAutomation
        {
            get;
            set;
        }

        /// <summary>
        /// Gets beneficiary (Paid To) free form description.
        /// </summary>
        /// <value>Beneficiary (Paid To) free form description.</value>
        [DataMember(Name = "bene_desc")]
        public string BeneficiaryDescription
        {
            get
            {
                // Return automated value if possible.
                // Fee must have BeneficiaryAgentId set and dataloan accessible in order for automation to work.
                if (!this.DisableBeneficiaryAutomation &&
                    this.BeneficiaryAgentId != Guid.Empty &&
                    this.ParentClosingCostSet != null &&
                    this.ParentClosingCostSet.HasDataLoanAssociate)
                {
                    // If agent is found, set value based on agent fields
                    CAgentFields officialContact = this.ParentClosingCostSet.DataLoan.GetAgentFields(this.BeneficiaryAgentId);
                    if (!officialContact.IsNewRecord)
                    {
                        this.beneficiaryDescriptionImpl = officialContact.CompanyName;
                    }

                    // TODO: THROW IF AGENT NOT FOUND (officialContact.IsNewRecord == true)?
                }

                if (string.IsNullOrEmpty(this.beneficiaryDescriptionImpl))
                {
                    this.beneficiaryDescriptionImpl = string.Empty;
                }

                // Returns manually set value if fee cannot be automated.
                return this.beneficiaryDescriptionImpl;
            }

            internal set
            {
                this.beneficiaryDescriptionImpl = value;
            }
        }

        /// <summary>
        /// Gets or sets the official contact id for this provider. If id is Guid.Empty the use Beneficiary as type.
        /// </summary>
        /// <value>The official contact id for this provider.</value>
        [DataMember(Name = "bene_id")]
        public Guid BeneficiaryAgentId
        {
            get
            {
                // Get beneficiary agent id from agent role if id has not been set.
                // Fee must have dataloan accessible in order for automation to work.
                if (!this.DisableBeneficiaryAutomation &&
                    this.BeneficiaryAgentIdImpl == Guid.Empty &&
                    this.ParentClosingCostSet != null &&
                    this.ParentClosingCostSet.HasDataLoanAssociate)
                {
                    // If agent is found, set value based on agent fields
                    CAgentFields officialContact = this.ParentClosingCostSet.DataLoan.GetAgentOfRoleForFee(this.beneficiaryImpl);
                    if (!officialContact.IsNewRecord)
                    {
                        this.BeneficiaryAgentIdImpl = officialContact.RecordId;
                    }

                    // Note: Agent might not be found, in which case BeneficiaryAgentId would still be Guid.Empty. DO NOT THROW IN THIS CASE.
                }

                // Returns manually set value if fee cannot be automated.
                return this.BeneficiaryAgentIdImpl;
            }

            set
            {
                // Only do stuff if the value has actually changed.
                if (value == this.BeneficiaryAgentIdImpl)
                {
                    return;
                }

                this.BeneficiaryAgentIdImpl = value;

                // re-enable automation if it was disabled by migration.
                this.DisableBeneficiaryAutomation = false;
            }
        }

        /// <summary>
        /// Gets or sets the responsible party for the charge. Is fee typically a responsibility of the Buyer or Seller.
        /// </summary>
        /// <value>The responsible party for the charge.</value>
        [DataMember(Name = "responsible")]
        public virtual E_GfeResponsiblePartyT GfeResponsiblePartyT
        {
            get
            {
                if (this.responsiblePartyInternal != E_GfeResponsiblePartyT.LeaveBlank)
                {
                    return this.responsiblePartyInternal;
                }

                if (this.Payments != null && this.Payments.Count(p => p != null) > 0)
                {
                    return this.Payments.Where(p => p != null).FirstOrDefault().ResponsiblePartyT;
                }

                return this.responsiblePartyInternal;
            }

            set
            {
                this.responsiblePartyInternal = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether fee is bona fide.
        /// </summary>
        /// <value>Whether fee is bona fide.</value>
        [DataMember(Name = "is_bf")]
        public bool IsBonaFide
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the total amount.
        /// </summary>
        /// <value>The total amount.</value>
        public decimal TotalAmount
        {
            get
            {
                LoanClosingCostFormula closingCostFormula = this.GetFormula();
                return closingCostFormula.TotalAmount;
            }
        }

        /// <summary>
        /// Gets the total amount paid by the borrower.
        /// </summary>
        /// <value>The total borrower amount.</value>
        public decimal BorrowerAmount
        {
            get
            {
                var borrowerAmounts = new List<decimal>();
                foreach (LoanClosingCostFeePayment payment in this.Payments)
                {
                    if (payment.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance || payment.PaidByT == E_ClosingCostFeePaymentPaidByT.Borrower)
                    {
                        borrowerAmounts.Add(payment.Amount);
                    }
                }

                return Tools.SumMoney(borrowerAmounts);
            }
        }

        /// <summary>
        /// Gets the string representation of the total amount.
        /// </summary>
        /// <value>String representation of the total amount.</value>
        [DataMember(Name = "total")]
        public string TotalAmount_rep
        {
            get
            {
                LoanClosingCostFormula closingCostFormula = this.GetFormula();
                return closingCostFormula.TotalAmount_rep;
            }

            private set
            {
                // 11/15/2014 dd - This is required so we can exposed the total amount through JSON serialization.
            }
        }

        /// <summary>
        /// Gets or sets the percent.
        /// </summary>
        /// <value>The percent.</value>
        public decimal Percent
        {
            get
            {
                LoanClosingCostFormula closingCostFormula = this.GetFormula();
                return closingCostFormula.Percent;
            }

            set
            {
                LoanClosingCostFormula closingCostFormula = this.GetFormula();
                closingCostFormula.Percent = value;
            }
        }

        /// <summary>
        /// Gets or sets the percent.
        /// </summary>
        /// <value>The percent.</value>
        public string Percent_rep
        {
            get
            {
                LoanClosingCostFormula closingCostFormula = this.GetFormula();
                return closingCostFormula.Percent_rep;
            }

            set
            {
                LoanClosingCostFormula closingCostFormula = this.GetFormula();
                closingCostFormula.Percent_rep = value;
            }
        }

        /// <summary>
        /// Gets or sets the percent base type.
        /// </summary>
        /// <value>The percent base type.</value>
        public E_PercentBaseT PercentBaseT
        {
            get
            {
                LoanClosingCostFormula closingCostFormula = this.GetFormula();
                return closingCostFormula.PercentBaseT;
            }

            set
            {
                LoanClosingCostFormula closingCostFormula = this.GetFormula();
                closingCostFormula.PercentBaseT = value;
            }
        }

        /// <summary>
        /// Gets or sets the minimum base amount.
        /// </summary>
        /// <value>The minimum base amount.</value>
        public decimal BaseAmount
        {
            get
            {
                LoanClosingCostFormula closingCostFormula = this.GetFormula();
                return closingCostFormula.BaseAmount;
            }

            set
            {
                LoanClosingCostFormula closingCostFormula = this.GetFormula();
                closingCostFormula.BaseAmount = value;
            }
        }

        /// <summary>
        /// Gets or sets the minimum base amount.
        /// </summary>
        /// <value>The minimum base amount.</value>
        public string BaseAmount_rep
        {
            get
            {
                LoanClosingCostFormula closingCostFormula = this.GetFormula();
                return closingCostFormula.BaseAmount_rep;
            }

            set
            {
                LoanClosingCostFormula closingCostFormula = this.GetFormula();
                closingCostFormula.BaseAmount_rep = value;
            }
        }

        /// <summary>
        /// Gets or sets the number of periods.
        /// </summary>
        /// <value>The number of periods.</value>
        public int NumberOfPeriods
        {
            get
            {
                LoanClosingCostFormula closingCostFormula = this.GetFormula();
                return closingCostFormula.NumberOfPeriods;
            }

            set
            {
                LoanClosingCostFormula closingCostFormula = this.GetFormula();
                closingCostFormula.NumberOfPeriods = value;
            }
        }

        /// <summary>
        /// Gets or sets the number of periods.
        /// </summary>
        /// <value>The number of periods.</value>
        public string NumberOfPeriods_rep
        {
            get
            {
                LoanClosingCostFormula closingCostFormula = this.GetFormula();
                return closingCostFormula.NumberOfPeriods_rep;
            }

            set
            {
                LoanClosingCostFormula closingCostFormula = this.GetFormula();
                closingCostFormula.NumberOfPeriods_rep = value;
            }
        }
        
        /// <summary>
        /// Gets or sets the saved percent total amount used when no loan file associated.
        /// </summary>
        /// <value>The Stored percent total amount of this formula.</value>
        public decimal PercentTotalAmount
        {
            get
            {
                LoanClosingCostFormula closingCostFormula = this.GetFormula();
                return closingCostFormula.PercentTotalAmount;
            }

            set
            {
                LoanClosingCostFormula closingCostFormula = this.GetFormula();
                closingCostFormula.PercentTotalAmount = value;
            }
        }

        /// <summary>
        /// Gets a list of payments.
        /// </summary>
        /// <value>A list of payments.</value>
        public IEnumerable<LoanClosingCostFeePayment> Payments
        {
            get
            {
                return this.GetPaymentList(null);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this custom fee is empty.
        /// </summary>
        /// <value>A value indicating whether this custom fee is empty.</value>
        public bool IsFeeEmpty
        {
            get
            {
                if (this.LegacyGfeFieldT == E_LegacyGfeFieldT.Undefined && string.IsNullOrEmpty(this.Description) && this.TotalAmount == 0)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets or sets the RESPA FEE MISMO type mapping.
        /// </summary>
        /// <value>The RESPA FEE MISMO type mapping.</value>
        public override E_ClosingCostFeeMismoFeeT MismoFeeT
        {
            get
            {
                bool isUSDALoan = this.ParentClosingCostSet != null && this.ParentClosingCostSet.HasDataLoanAssociate && this.ParentClosingCostSet.DataLoan.sLT == E_sLT.UsdaRural;

                E_ClosingCostFeeMismoFeeT? mismoFeeType = ClosingCostSetUtils.GetMismoFeeType(this.SourceFeeTypeId ?? this.ClosingCostFeeTypeId, isUSDALoan);
                if (mismoFeeType.HasValue)
                {
                    this.InternalMismoFeeT = mismoFeeType.Value;
                }

                return this.InternalMismoFeeT;
            }

            set
            {
                this.InternalMismoFeeT = value;
            }
        }

        /// <summary>
        /// Gets or sets the total amount value on deserialization, before any auto calculations happen.
        /// </summary>
        /// <value>The total amount value on deserialization.</value>
        internal decimal TotalAmountOnDeserialization
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the payment list for this fee. All payments are LoanClosingCostFeePayments.
        /// </summary>
        /// <value>The payment list for this fee.</value>
        internal List<LoanClosingCostFeePayment> ChildPaymentList
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the child loan formula. This casts BaseClosingCostFee's ChildFormula as a LoanClosingCostFormula. USE THIS IF YOU CAN FOR THIS CLASS AND ALL OF ITS CHILD CLASSES.
        /// </summary>
        /// <value>The child loan formula.</value>
        internal LoanClosingCostFormula ChildLoanFormula
        {
            get
            {
                return (LoanClosingCostFormula)this.ChildFormula;
            }

            set
            {
                this.ChildFormula = value;
            }
        }

        /// <summary>
        /// Gets or sets the official contact id for this provider. If id is Guid.Empty the use Beneficiary as type.
        /// </summary>
        /// <value>The official contact id for this provider. If id is Guid.Empty the use Beneficiary as type.</value>
        protected Guid BeneficiaryAgentIdImpl
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the conversion format.
        /// </summary>
        /// <value>The conversion format for this fee.</value>
        protected LosConvert LosConvert
        {
            get
            {
                if (this.ParentClosingCostSet != null)
                {
                    return this.ParentClosingCostSet.LosConvert;
                }

                return new LosConvert();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a borrower did shop for provider.
        /// </summary>
        /// <value>Whether a borrower did shop for provider.</value>
        protected bool DidShopImpl
        {
            get;
            set;
        }
        
        /// <summary>
        /// Gets or sets a value indicating whether it is a third party.
        /// </summary>
        /// <value>Whether is is a third party.</value>
        protected bool IsThirdPartyImpl
        {
            get;
            set;
        }

        /// <summary>
        /// Checks if this fee can be removed.
        /// </summary>
        /// <param name="message">The error message.</param>
        /// <returns>True if it can, false otherwise.</returns>
        public override bool ValidateRemoveFee(out string message)
        {
            Guid feeTypeId = this.ClosingCostFeeTypeId;
            if (feeTypeId == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId ||
                feeTypeId == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumUpfrontFeeTypeId ||
                feeTypeId == DefaultSystemClosingCostFee.Hud900VAFundingFeeTypeId ||
                feeTypeId == DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId ||
                (feeTypeId == DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId && this.TotalAmount != 0) ||
                ClosingCostSetUtils.ConstantInitialEscrowLegacyFeeList.Contains(feeTypeId) ||
                ClosingCostSetUtils.ConstantPrepaidEscrowFeeList.Contains(feeTypeId) ||
                feeTypeId == DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId)
            {
                message = "Fee " + this.Description + " cannot be removed.";
                return false;
            }
            else
            {
                message = "Fee " + this.Description + " can be removed.";
                return true;
            }
        }

        /// <summary>
        /// Removes a payment from the list of payments.
        /// </summary>
        /// <param name="payment">The payment to be removed.</param>
        public void RemovePayment(LoanClosingCostFeePayment payment)
        {
            this.ChildPaymentList.Remove(payment);
        }

        /// <summary>
        /// Clear out the existing list of payments.
        /// </summary>
        public void ClearPayments()
        {
            this.ChildPaymentList = new List<LoanClosingCostFeePayment>();
        }

        /// <summary>
        /// Resets payments for $0 fees.
        /// </summary>
        public void ResetPayments()
        {
            this.ClearPayments();
            this.GetPaymentsWithCachedTotal(0);
            this.Payments.First().Id = ClosingCostSetUtils.CombineGuids(this.UniqueId, ClosingCostSetUtils.SaltGuid1);
        }

        /// <summary>
        /// Gets the list of payments with a pre-computed total. This is especially useful when the total is not needed 
        /// or the total has already been computed once before. 
        /// </summary>
        /// <param name="totalAmount">The total amount of the fee.</param>
        /// <returns>A list of payments.</returns>
        public IEnumerable<LoanClosingCostFeePayment> GetPaymentsWithCachedTotal(decimal totalAmount)
        {
            return this.GetPaymentList(totalAmount);
        }

        /// <summary>
        /// Add the payment to this fee.
        /// </summary>
        /// <param name="payment">The payment.</param>
        public void AddPayment(LoanClosingCostFeePayment payment)
        {
            if (payment == null)
            {
                return;
            }

            payment.SetParent(this);
            payment.EnsureCorrectness();

            this.GetPaymentList(null).Add(payment);
        }

        /// <summary>
        /// Set the beneficiary description manually. Should only in the migration between old GFE and new GFE.
        /// </summary>
        /// <param name="agentRole">Try to match with agent role.</param>
        /// <param name="description">Manual description.</param>
        public void SetBeneficiaryDescription(E_AgentRoleT agentRole, string description)
        {
            if (description == this.BeneficiaryDescription)
            {
                return; // NO-OP
            }

            // Migration of some form has happened. Disable automation so we don't clobber migrated values,
            // and set fields manually.
            this.DisableBeneficiaryAutomation = true;
            this.ClearBeneficiary();
            this.beneficiaryImpl = agentRole;
            this.beneficiaryDescriptionImpl = description;
        }

        /// <summary>
        /// Clears associated beneficiary agent id and beneficiary description for this fee.
        /// </summary>
        /// <remarks>Does not affect the value of beneficiary agent role, is third party, or is affiliate.</remarks>
        public void ClearBeneficiary()
        {
            // Note: Use private members instead of properties in order to avoid automation code.
            this.BeneficiaryAgentIdImpl = Guid.Empty;
            this.beneficiaryDescriptionImpl = string.Empty;
        }

        /// <summary>
        /// Converts this closing cost fee to a fee setup fee.
        /// </summary>
        /// <returns>The fee setup fee made from this closing cost fee.</returns>
        public FeeSetupClosingCostFee ConvertToFeeSetupFee()
        {
            FeeSetupClosingCostFee convertedFee = new FeeSetupClosingCostFee();
            convertedFee.IsAffiliate = this.IsAffiliate;
            convertedFee.IsApr = this.IsApr;
            convertedFee.Beneficiary = this.Beneficiary;
            convertedFee.CanShop = this.CanShop;
            convertedFee.ClosingCostFeeTypeId = this.ClosingCostFeeTypeId;
            convertedFee.Description = this.Description;
            convertedFee.Dflp = this.Dflp;
            convertedFee.GfeSectionT = this.GfeSectionT;
            convertedFee.HudLine = this.HudLine;
            convertedFee.IntegratedDisclosureSectionT = this.IntegratedDisclosureSectionT;
            convertedFee.IsFhaAllowable = this.IsFhaAllowable;
            convertedFee.IsOptional = this.IsOptional;
            convertedFee.IsThirdParty = this.IsThirdParty;
            convertedFee.IsTitleFee = this.IsTitleFee;
            convertedFee.IsVaAllowable = this.IsVaAllowable;
            convertedFee.LegacyGfeFieldT = this.LegacyGfeFieldT;
            convertedFee.MismoFeeT = this.MismoFeeT;
            convertedFee.NewFeeIndex = this.NewFeeIndex;
            convertedFee.FormulaT = this.FormulaT;

            return convertedFee;
        }

        /// <summary>
        /// Checks if the fee is part of the fee section. 
        /// </summary>
        /// <param name="optionalPredicate">An additional check to the IsFeeEmpty check.</param>
        /// <returns>False if fee is empty or the optional predicate returns false. True otherwise.</returns>
        public bool CheckIfFeeIsInSection(Func<LoanClosingCostFee, bool> optionalPredicate)
        {
            if (this.IsFeeEmpty)
            {
                return false;
            }

            if (optionalPredicate != null)
            {
                return optionalPredicate(this);
            }

            return true;
        }

        /// <summary>
        /// Gets the TRID section based on the type of disclosure that is being produced.
        /// </summary>
        /// <param name="usingClosingDisclosure">Indicates whether a CD is being produced.</param>
        /// <returns>The corresponding TRID section type.</returns>
        public E_IntegratedDisclosureSectionT GetTRIDSectionBasedOnDisclosure(bool usingClosingDisclosure)
        {
            if (usingClosingDisclosure && !this.DidShop && this.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionC)
            {
                return E_IntegratedDisclosureSectionT.SectionB;
            }

            return this.IntegratedDisclosureSectionT;
        }

        /// <summary>
        /// Returns the value specified by the element.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns>The value specified by the element.</returns>
        public override object GetElement(IDataPathElement element)
        {
            if (element is DataPathCollectionElement && string.Equals(element.Name, "Payment", StringComparison.OrdinalIgnoreCase))
            {
                return new LoanClosingCostFeePaymentCollection(this);
            }

            return base.GetElement(element);
        }

        /// <summary>
        /// Set the closing costs set to this formula.
        /// </summary>
        /// <param name="o">Closing costs set.</param>
        internal override void SetClosingCostSet(BaseClosingCostSet o)
        {
            this.ParentClosingCostSet = (LoanClosingCostSet)o;
        }

        /// <summary>
        /// Enforces some rules on the formula object.
        /// </summary>
        internal override void EnsureFormula()
        {
            if (this.ChildLoanFormula == null)
            {
                this.ChildLoanFormula = this.CreateNewFormula();
            }

            E_ClosingCostFeeFormulaT? type = ClosingCostSetUtils.GetFormulaType(this.IntegratedDisclosureSectionT, this.LegacyGfeFieldT, this.SourceFeeTypeId ?? this.ClosingCostFeeTypeId);
            if (type.HasValue)
            {
                this.ChildLoanFormula.FormulaT = type.Value;
            }

            // 12/30/2014 dd - Always update the formula object with latest closing cost set object.
            this.ChildLoanFormula.SetClosingCostFee(this);

            this.ChildLoanFormula.EnsureCorrectness();
        }

        /// <summary>
        /// Since JSON serialization/deserialization set to private member variables directly which bypass some logic in the getter / setter.
        /// This function is a centralize of the logics on the absolute fact or guarantee of this object.
        /// </summary>
        internal override void EnsureCorrectness()
        {
            base.EnsureCorrectness();

            foreach (LoanClosingCostFeePayment payment in this.Payments)
            {
                payment.EnsureCorrectness();
            }
        }

        /// <summary>
        /// Gets the proposed housing expense of certain type. Return null if housing expense not found.
        /// </summary>
        /// <param name="housingExpenseT">The housing expense type.</param>
        /// <returns>Housing Expense.</returns>
        internal BaseHousingExpense GetHousingExpense(E_HousingExpenseTypeT housingExpenseT)
        {
            if (this.ParentClosingCostSet != null && this.ParentClosingCostSet.HasDataLoanAssociate)
            {
                return this.ParentClosingCostSet.DataLoan.sHousingExpenses.GetExpense(housingExpenseT);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the unassigned expense associated with a line number. Returns null if not found.
        /// </summary>
        /// <param name="customExpenseLineNumberT">The custom housing expense line number.</param>
        /// <returns>Housing Expense.</returns>
        internal BaseHousingExpense GetHousingExpense(E_CustomExpenseLineNumberT customExpenseLineNumberT)
        {
            if (this.ParentClosingCostSet != null && this.ParentClosingCostSet.HasDataLoanAssociate)
            {
                return this.ParentClosingCostSet.DataLoan.sHousingExpenses.GetUnassignedExp(customExpenseLineNumberT);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the internal list of payment. Will instantiate a new object if it is not exists. By passing the total amount
        /// there will be a performance boost.
        /// </summary>
        /// <param name="cacheAmount">The amount to use when populating the payments. If its null it will fetch it.</param>
        /// <returns>A list of payments.</returns>
        protected virtual List<LoanClosingCostFeePayment> GetPaymentList(decimal? cacheAmount)
        {
            decimal totalAmount;

            if (cacheAmount.HasValue)
            {
                totalAmount = cacheAmount.Value;
            }
            else
            {
                totalAmount = this.TotalAmount;
            }

            if (this.ChildPaymentList == null)
            {
                // Make a new list of payments.
                this.ClearPayments();
            }

            // 1/6/2015 dd - Always require at least one payment.
            if (this.ChildPaymentList.Count == 0)
            {
                // Create new appropriate payment.
                LoanClosingCostFeePayment payment = this.CreateNewPayment();

                payment.GfeClosingCostFeePaymentTimingT = E_GfeClosingCostFeePaymentTimingT.AtClosing;
                payment.Amount = totalAmount;
                payment.IsSystemGenerated = false;

                this.ChildPaymentList.Add(payment);
            }
            else if (this.ChildPaymentList.Count == 1)
            {
                // 2/12/2015 dd - Make sure the only payment is not system generate.
                LoanClosingCostFeePayment payment = this.ChildPaymentList.First();
                payment.IsSystemGenerated = false;
                payment.Amount = totalAmount;
            }
            else if (this.ChildPaymentList.Count > 1)
            {
                // 1/16/2015 dd - When there are more than one payment record, then we need to ensure the following:
                //      a) Sum of all payment == TotalAmount
                //      b) There MUST be a System Generate Payment.
                //      c) System Generate payment must be at the end of the list.
                //      d) System Generate payment can be zero.
                decimal manualPaymentTotal = 0;
                decimal currentSystemPaymentTotal = 0;
                int systemPaymentCount = 0;
                LoanClosingCostFeePayment oldSystemPayment = null;

                foreach (LoanClosingCostFeePayment o in this.ChildPaymentList)
                {
                    if (o.IsSystemGenerated == false)
                    {
                        manualPaymentTotal += o.Amount;
                    }
                    else
                    {
                        currentSystemPaymentTotal += o.Amount;
                        systemPaymentCount++;
                        oldSystemPayment = o;
                    }
                }

                bool isRegenerateList = false;
                if (systemPaymentCount != 1)
                {
                    isRegenerateList = true;
                }
                else if (systemPaymentCount == 1)
                {
                    if (this.ChildPaymentList[this.ChildPaymentList.Count - 1].IsSystemGenerated == false)
                    {
                        // System generate payment is not at the end.
                        isRegenerateList = true;
                    }
                }

                decimal newSystemPaymentTotal = totalAmount - manualPaymentTotal;

                if (newSystemPaymentTotal != currentSystemPaymentTotal)
                {
                    isRegenerateList = true;
                }

                if (isRegenerateList)
                {
                    List<LoanClosingCostFeePayment> newList = new List<LoanClosingCostFeePayment>();
                    foreach (var o in this.ChildPaymentList)
                    {
                        if (o.IsSystemGenerated == false)
                        {
                            newList.Add(o);
                        }
                    }

                    LoanClosingCostFeePayment systemPayment = this.CreateNewPayment();
                    systemPayment.PaidByT = E_ClosingCostFeePaymentPaidByT.Borrower;
                    systemPayment.GfeClosingCostFeePaymentTimingT = E_GfeClosingCostFeePaymentTimingT.AtClosing;
                    systemPayment.IsSystemGenerated = true;
                    systemPayment.Amount = newSystemPaymentTotal;
                    systemPayment.PaymentDate_rep = string.Empty;
                    systemPayment.SetParent(this);
                    if (oldSystemPayment != null)
                    {
                        // We strip out all the system-generated payments, so reusing the ID
                        // from one of the old ones for the new one should result in the new
                        // payments being as unique as the list we started with.
                        systemPayment.Id = oldSystemPayment.Id;
                    }

                    newList.Add(systemPayment);
                    this.ChildPaymentList = newList;
                }
            }

            // 6/2/2015 BB - This ensures that each payment uses the same LosConvert as the parent fee so that the output format on export is the same.
            foreach (LoanClosingCostFeePayment payment in this.ChildPaymentList)
            {
                payment.SetParent(this);
            }

            return this.ChildPaymentList;
        }

        /// <summary>
        /// Creates a new formula. Creates the child class's appropriate formula.
        /// </summary>
        /// <returns>A new formula.</returns>
        protected abstract LoanClosingCostFormula CreateNewFormula();

        /// <summary>
        /// Creates a new payment. Creates the child class's appropriate payment.
        /// </summary>
        /// <returns>A new payment.</returns>
        protected abstract LoanClosingCostFeePayment CreateNewPayment();

        /// <summary>
        /// Gets the formula for this closing cost fee.
        /// </summary>
        /// <returns>The loan closing cost formula for the fee.</returns>
        protected LoanClosingCostFormula GetFormula()
        {
            this.EnsureFormula();
            return this.ChildLoanFormula;
        }

        /// <summary>
        /// Clears the parent set's fee lookup cache.
        /// </summary>
        protected override void ClearParentSetLookupCache()
        {
            if (this.ParentClosingCostSet != null)
            {
                this.ParentClosingCostSet.ClearFeeLookupCache(false);
            }
        }

        /// <summary>
        /// Perform setup before object is serialize.
        /// </summary>
        /// <param name="context">Serialized context.</param>
        [OnSerializing]
        private void SetValuesOnSerializing(StreamingContext context)
        {
            var ftype = this.FormulaT;
            this.EnsureFormula();

            if (ftype != this.FormulaT)
            {
                Tools.LogError("Changed ftype " + ftype.ToString() + " to " + this.FormulaT.ToString());
            }

            // 3/25/2015 dd - Assign new id when id or type id is empty.
            if (this.UniqueId == Guid.Empty)
            {
                this.UniqueId = Guid.NewGuid();
            }

            if (this.ClosingCostFeeTypeId == Guid.Empty)
            {
                this.ClosingCostFeeTypeId = Guid.NewGuid();
            }

            if (string.IsNullOrEmpty(this.BeneficiaryDescription))
            {
                this.BeneficiaryDescription = string.Empty;
            }

            foreach (LoanClosingCostFeePayment payment in this.GetPaymentList(null))
            {
                payment.SetParent(this);
            }
        }

        /// <summary>
        /// Perform initialization after object is deserialized.
        /// </summary>
        /// <param name="context">Serialized context.</param>
        [OnDeserialized]
        private void InitializeValueAfterDeserialized(StreamingContext context)
        {
            if (string.IsNullOrEmpty(this.BeneficiaryDescription))
            {
                this.BeneficiaryDescription = string.Empty;
            }

            if (string.IsNullOrEmpty(this.HudLine_rep))
            {
                this.HudLine_rep = string.Empty;
            }

            this.TotalAmountOnDeserialization = this.TotalAmount;
        }
    }
}
