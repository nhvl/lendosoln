﻿// <copyright file="BorrowerClosingCostFeeSection.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   7/21/2015
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;

    /// <summary>
    /// Class representing a borrower responsible fee section.
    /// </summary>
    [DataContract]
    public class BorrowerClosingCostFeeSection : BaseClosingCostFeeSection
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BorrowerClosingCostFeeSection" /> class.
        /// </summary>
        /// <param name="closingCostViewT">The view type.</param>
        /// <param name="closingCostSet">The parent set to pull fees from.</param>
        /// <param name="setFilter">The filter for the master set.</param>
        /// <param name="sectionName">The section name.</param>
        /// <param name="predicate">The predicate used to filter fees.</param>
        /// <param name="feeTemplate">The fee template for this section.</param>
        /// <param name="hudLineStart">The first hudline number for this section.</param>
        /// <param name="hudLineEnd">The last hudline number for this section.</param>
        /// <param name="sectionT">The disclosure section type for this section.</param>
        public BorrowerClosingCostFeeSection(E_ClosingCostViewT closingCostViewT, BorrowerClosingCostSet closingCostSet, Func<BaseClosingCostFee, bool> setFilter, string sectionName, Func<LoanClosingCostFee, bool> predicate, BorrowerClosingCostFee feeTemplate, int hudLineStart, int hudLineEnd, E_IntegratedDisclosureSectionT sectionT)
            : base(closingCostViewT, closingCostSet, setFilter, sectionName, o => predicate((BorrowerClosingCostFee)o), feeTemplate, hudLineStart, hudLineEnd, sectionT) 
        {
        }

        /// <summary>
        /// Gets the sum of all the fees in this section that are Borrower Paid / Financed.
        /// </summary>
        /// <value>The sum of all the fees in this section that are Borrower Paid / Financed.</value>
        [DataMember(Name = "TotalAmountBorrPaid")]
        public string TotalAmountBorrPaid_rep
        {
            get
            {
                Func<LoanClosingCostFeePayment, bool> paymentPred = p => p.PaidByT == E_ClosingCostFeePaymentPaidByT.Borrower || p.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance;
                decimal totalAmountBorrPaid = this.SetAsBorrowerSet.Sum(o => this.Predicate(o), paymentPred);

                //// 6/3/2015 BB - This ensures that the output format matches that of the parent closing cost set. For example if this is called by a MISMO exporter and therefore needs MISMO format.
                if (this.SetAsBorrowerSet != null && this.SetAsBorrowerSet.LosConvert != null)
                {
                    return this.SetAsBorrowerSet.LosConvert.ToMoneyString(totalAmountBorrPaid, FormatDirection.ToRep);
                }
                else
                {
                    LosConvert losConvert = new LosConvert();

                    return losConvert.ToMoneyString(totalAmountBorrPaid, FormatDirection.ToRep);
                }
            }

            private set
            {
                // Needed by the serializer.
            }
        }

        /// <summary>
        /// Gets the sum of all the fees in this section.
        /// </summary>
        /// <value>The sum of all the fees in this section.</value>
        [DataMember(Name = "TotalAmount")]
        public string TotalAmount_rep
        {
            get
            {
                decimal total = Tools.SumMoney(this.FilteredClosingCostFeeList.Select(f => ((BorrowerClosingCostFee)f).TotalAmount));

                if (this.SetAsBorrowerSet != null && this.SetAsBorrowerSet.LosConvert != null)
                {
                    return this.SetAsBorrowerSet.LosConvert.ToMoneyString(total, FormatDirection.ToRep);
                }
                else
                {
                    LosConvert losConvert = new LosConvert();

                    return losConvert.ToMoneyString(total, FormatDirection.ToRep);
                }
            }

            private set
            {
            }
        }

        /// <summary>
        /// Gets or sets the section's fee list as BorrowerClosingCostFee. USE THIS FOR SERIALIZATION PLEASE.
        /// </summary>
        /// <value>The section's fee list as BorrowerClosingCostFee.</value>
        [DataMember]
        private IEnumerable<BorrowerClosingCostFee> ClosingCostFeeList
        {
            get
            {
                List<BorrowerClosingCostFee> list = new List<BorrowerClosingCostFee>();

                foreach (BorrowerClosingCostFee fee in this.FilteredClosingCostFeeList)
                {
                    list.Add(fee);
                }

                return list;
            }

            set
            {
                this.FilteredClosingCostFeeList = (IEnumerable<BaseClosingCostFee>)value;
            }
        }

        /// <summary>
        /// Gets or sets the section's fee template as a BorrowerClosingCostFee. USE THIS FOR SERIALIZATION PLEASE.
        /// </summary>
        /// <value>The fee template as a BorrowerClosingCostFee.</value>
        [DataMember]
        private BorrowerClosingCostFee FeeTemplate
        {
            get
            {
                return (BorrowerClosingCostFee)this.SectionFeeTemplate;
            }

            set
            {
                this.SectionFeeTemplate = value;
            }
        }

        /// <summary>
        /// Gets the associated closing cost set as a Borrower set.
        /// </summary>
        /// <value>The associated closing cost set as a borrower set.</value>
        private BorrowerClosingCostSet SetAsBorrowerSet
        {
            get
            {
                return (BorrowerClosingCostSet)this.ClosingCostSet;
            }
        }
    }
}
