﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;

    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Used to reserve closing cost fee type ID and HUD line for a dynamic fee (a fee whose type is not in the broker fee setup) added to a loan.
    /// </summary>
    public class DynamicClosingCostFeeHistoryItem
    {
        /// <summary>
        /// A value indicating whether this is a new object not yet saved to DB.
        /// </summary>
        private bool isNew;

        /// <summary>
        /// Initializes a new instance of the <see cref="DynamicClosingCostFeeHistoryItem" /> class.
        /// </summary>
        /// <param name="brokerId">Broker ID.</param>
        /// <param name="loanId">Loan ID of the loan this fee belongs to.</param>
        /// <param name="description">Fee Description.</param>
        /// <param name="closingCostFeeTypeId">Fee type ID assigned to this fee.</param>
        /// <param name="hudLine">HUD line number assigned to this fee.</param>
        public DynamicClosingCostFeeHistoryItem(Guid brokerId, Guid loanId, string description, Guid closingCostFeeTypeId, int hudLine)
        {
            this.isNew = true;
            this.Id = -1;
            this.BrokerId = brokerId;
            this.LoanId = loanId;
            this.Description = description;
            this.ClosingCostFeeTypeId = closingCostFeeTypeId;
            this.HudLine = hudLine;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DynamicClosingCostFeeHistoryItem" /> class.
        /// </summary>
        /// <param name="reader">An IDataReader to read from.</param>
        public DynamicClosingCostFeeHistoryItem(IDataReader reader)
        {
            this.isNew = false;
            this.Id = (int)reader["FeeHistoryId"];
            this.BrokerId = (Guid)reader["BrokerId"];
            this.LoanId = (Guid)reader["LoanId"];
            this.Description = (string)reader["Description"];
            this.ClosingCostFeeTypeId = (Guid)reader["ClosingCostFeeTypeId"];
            this.HudLine = (int)reader["HudLine"];
        }

        /// <summary>
        /// Gets the fee history item ID.
        /// </summary>
        /// <value>Fee history item ID.</value>
        public int Id { get; private set; }

        /// <summary>
        /// Gets the broker ID for this fee history item.
        /// </summary>
        /// <value>Broker ID.</value>
        public Guid BrokerId { get; private set; }

        /// <summary>
        /// Gets the loan ID of the loan this fee belongs to.
        /// </summary>
        /// <value>The loan ID of the loan this fee belongs to.</value>
        public Guid LoanId { get; private set; }

        /// <summary>
        /// Gets the fee description.
        /// </summary>
        /// <value>Fee description.</value>
        /// <remarks>
        /// Because ClosingCostFeeTypeId is a random Guid, LoanId and Description are
        /// used as the primary key values for this object as stored on the DB.
        /// </remarks>
        public string Description { get; private set; }

        /// <summary>
        /// Gets the closing cost fee type ID assigned to this dynamic fee.
        /// </summary>
        /// <value>Closing cost fee type ID.</value>
        public Guid ClosingCostFeeTypeId { get; private set; }

        /// <summary>
        /// Gets or sets the HUD line assigned to this fee.
        /// </summary>
        /// <value>HUD Line as Int.</value>
        public int HudLine { get; set; }

        /// <summary>
        /// Saves this history item to db.
        /// </summary>
        /// <param name="conn">SQL Connection.</param>
        /// <param name="trans">SQL Transaction.</param>
        public void Save(DbConnection conn, DbTransaction trans)
        {
            SqlParameter idOut = new SqlParameter("@FeeHistoryId", SqlDbType.Int);
            idOut.Direction = ParameterDirection.Output;

            List<SqlParameter> regionParameters = new List<SqlParameter>()
            {
                new SqlParameter("@BrokerId", this.BrokerId),
                new SqlParameter("@HudLine", this.HudLine)
            };

            StoredProcedureName spName;

            if (this.isNew)
            {
                regionParameters.Add(new SqlParameter("@LoanId", this.LoanId));
                regionParameters.Add(new SqlParameter("@Description", Tools.TruncateString(this.Description, 200)));
                regionParameters.Add(new SqlParameter("@ClosingCostFeeTypeId", this.ClosingCostFeeTypeId));
                regionParameters.Add(idOut);
                spName = StoredProcedureName.Create("DYNAMIC_CLOSING_COST_FEE_HISTORY_Create").Value;
            }
            else
            {
                regionParameters.Add(new SqlParameter("@FeeHistoryId", this.Id));
                spName = StoredProcedureName.Create("DYNAMIC_CLOSING_COST_FEE_HISTORY_Update").Value;
            }

            IStoredProcedureDriver driver = GenericLocator<IStoredProcedureDriverFactory>.Factory.Create(TimeoutInSeconds.Thirty);
            driver.ExecuteNonQuery(conn, trans, spName, regionParameters);

            if (this.Id == -1)
            {
                this.Id = (int)idOut.Value;
            }
        }
    }
}
