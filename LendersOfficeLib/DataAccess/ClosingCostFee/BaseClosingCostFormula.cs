﻿// <copyright file="BaseClosingCostFormula.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   7/9/2015
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;

    /// <summary>
    /// Absolute base class for all closing cost formulas. DO NOT SERIALIZE/DESERIALIZE DIRECTLY INTO THIS CLASS.
    /// </summary>
    [DataContract]
    [KnownType(typeof(FeeSetupClosingCostFormula))]
    [KnownType(typeof(LoanClosingCostFormula))]
    public abstract class BaseClosingCostFormula : ICloneable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BaseClosingCostFormula" /> class.
        /// </summary>
        public BaseClosingCostFormula()
        {
            this.FormulaT = E_ClosingCostFeeFormulaT.Full;
        }

        /// <summary>
        /// Gets or sets the type of this formula. Mainly use to render formula.
        /// </summary>
        /// <value>The type of this formula.</value>
        public virtual E_ClosingCostFeeFormulaT FormulaT
        {
            get
            {
                return this.InternalFormulaT;
            }

            set
            {
                this.InternalFormulaT = value;
            }
        }

        /// <summary>
        /// Gets or sets type of this formula.
        /// </summary>
        /// <value>The internal formulate type.</value>
        [DataMember(Name = "t")]
        protected E_ClosingCostFeeFormulaT InternalFormulaT
        {
            get;
            set;
        }

        /// <summary>
        /// Clones the given object.
        /// </summary>
        /// <returns>A closing cost fee with the exact values except ID.</returns>
        public abstract object Clone();
    }
}
