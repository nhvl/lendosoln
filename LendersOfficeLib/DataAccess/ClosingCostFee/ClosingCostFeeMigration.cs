﻿// <copyright file="ClosingCostFeeMigration.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//     Author: David Dao
//     Date:   3/24/2015
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.Constants;
    using LendersOffice.Security;

    /// <summary>
    /// Perform migration.
    /// </summary>
    /// <remarks>
    /// The option to use a single save was added in commit 6351b1c4, and there
    /// is a ScheduleExecutable method that was added in that commit to verify
    /// these changes. If we need to make changes to the migration and want to
    /// compare the before/after results it may be useful to reference that routine.
    /// </remarks>
    public class ClosingCostFeeMigration
    {
        /// <summary>
        /// The specific field ids used by custom fees on the old GFE.
        /// </summary>
        private static readonly HashSet<string> LegacyCustomFeeFieldIds = new HashSet<string>()
        {
            // 800
            "s800U1FDesc", "s800U1F", "s800U1FGfeSection", "s800U1FProps", "s800U1FPaidTo",
            "s800U2FDesc", "s800U2F", "s800U2FGfeSection", "s800U2FProps", "s800U2FPaidTo",
            "s800U3FDesc", "s800U3F", "s800U3FGfeSection", "s800U3FProps", "s800U3FPaidTo",
            "s800U4FDesc", "s800U4F", "s800U4FGfeSection", "s800U4FProps", "s800U4FPaidTo",
            "s800U5FDesc", "s800U5F", "s800U5FGfeSection", "s800U5FProps", "s800U5FPaidTo",

            // 900
            "s904PiaDesc", "s904Pia", "s904PiaGfeSection", "s904PiaProps",
            "s900U1PiaCode", "s900U1PiaDesc", "s900U1Pia", "s900U1PiaGfeSection", "s900U1PiaProps",
            
            // 1100
            "sU1TcDesc", "sU1Tc", "sU1TcGfeSection", "sU1TcProps", "sU1TcPaidTo",
            "sU2TcDesc", "sU2Tc", "sU2TcGfeSection", "sU2TcProps", "sU2TcPaidTo",
            "sU3TcDesc", "sU3Tc", "sU3TcGfeSection", "sU3TcProps", "sU3TcPaidTo",
            "sU4TcDesc", "sU4Tc", "sU4TcGfeSection", "sU4TcProps", "sU4TcPaidTo",

            // 1200
            "sU1GovRtcDesc", "sU1GovRtcPc", "sU1GovRtcBaseT", "sU1GovRtcMb", "sU1GovRtcGfeSection", "sU1GovRtcProps", "sU1GovRtcPaidTo",
            "sU2GovRtcDesc", "sU2GovRtcPc", "sU2GovRtcBaseT", "sU2GovRtcMb", "sU2GovRtcGfeSection", "sU2GovRtcProps", "sU2GovRtcPaidTo",
            "sU3GovRtcDesc", "sU3GovRtcPc", "sU3GovRtcBaseT", "sU3GovRtcMb", "sU3GovRtcGfeSection", "sU3GovRtcProps", "sU3GovRtcPaidTo",
            
            // 1300
            "sU1ScDesc", "sU1Sc", "sU1ScGfeSection", "sU1ScProps", "sU1ScPaidTo",
            "sU2ScDesc", "sU2Sc", "sU2ScGfeSection", "sU2ScProps", "sU2ScPaidTo",
            "sU3ScDesc", "sU3Sc", "sU3ScGfeSection", "sU3ScProps", "sU3ScPaidTo",
            "sU4ScDesc", "sU4Sc", "sU4ScGfeSection", "sU4ScProps", "sU4ScPaidTo",
            "sU5ScDesc", "sU5Sc", "sU5ScGfeSection", "sU5ScProps", "sU5ScPaidTo",
        };

        /// <summary>
        /// The specific field ids we want to migrate.
        /// </summary>
        private static IEnumerable<string> closingCostFieldIdsToMigrate;

        /// <summary>
        /// All of the loan file field dependencies required to run the migration.
        /// </summary>
        private static IEnumerable<string> dependencyList;
        
        /// <summary>
        /// Initializes static members of the <see cref="ClosingCostFeeMigration" /> class.
        /// </summary>
        static ClosingCostFeeMigration()
        {
            var idsToMigrate = new HashSet<string>
            {
                "sLOrigFPc", "sLOrigFMb", "sLOrigFProps",
                "sGfeDiscountPointFProps",
                "sApprF", "sApprFProps", "sApprFPaid", "sApprFPaidTo",
                "sCrF", "sCrFProps", "sCrFPaid", "sCrFPaidTo",
                "sTxServF", "sTxServFProps", "sTxServFPaidTo",
                "sFloodCertificationF", "sFloodCertificationFProps", "sFloodCertificationFPaidTo",
                "sMBrokFPc", "sMBrokFBaseT", "sMBrokFMb", "sMBrokFProps",
                "sInspectF", "sInspectFProps", "sInspectFPaidTo",
                "sProcF", "sProcFProps", "sProcFPaid", "sProcFPaidTo",
                "sUwF", "sUwFProps", "sUwFPaidTo",
                "sWireF", "sWireFProps", "sWireFPaidTo",

                // 900
                "sIPiaProps",
                "sMipPiaProps", "sMipPiaPaidTo",
                "sHazInsPiaProps", "sHazInsPiaPaidTo",
                "sVaFfProps", "sVaFfPaidTo",

                // 1000
                "sHazInsRsrvProps",
                "sMInsRsrvProps",
                "sRealETxRsrvProps",
                "sSchoolTxRsrvProps",
                "sFloodInsRsrvProps",
                "sAggregateAdjRsrvProps",

                // 1100
                "sEscrowF", "sEscrowFGfeSection", "sEscrowFProps", "sEscrowFTable",
                "sOwnerTitleInsF", "sOwnerTitleInsProps", "sOwnerTitleInsPaidTo",
                "sTitleInsF", "sTitleInsFGfeSection", "sTitleInsFProps", "sTitleInsFTable",
                "sDocPrepF", "sDocPrepFGfeSection", "sDocPrepFProps", "sDocPrepFPaidTo",
                "sNotaryF", "sNotaryFGfeSection", "sNotaryFProps", "sNotaryFPaidTo",
                "sAttorneyF", "sAttorneyFGfeSection", "sAttorneyFProps", "sAttorneyFPaidTo",

                // 1200
                "sRecDeed",
                "sRecMortgage",
                "sRecRelease",
                "sRecFProps", "sRecFDesc",
                "sCountyRtcPc", "sCountyRtcBaseT", "sCountyRtcMb", "sCountyRtcProps", "sCountyRtcDesc",
                "sStateRtcPc", "sStateRtcBaseT", "sStateRtcMb", "sStateRtcProps", "sStateRtcDesc",

                // 1300
                "sPestInspectF", "sPestInspectFProps", "sPestInspectPaidTo",

                "sSellerSettlementChargesU01FHudline",
                "sSellerSettlementChargesU02FHudline",
                "sSellerSettlementChargesU03FHudline",
                "sSellerSettlementChargesU04FHudline",
                "sSellerSettlementChargesU05FHudline",
                "sSellerSettlementChargesU06FHudline",
                "sSellerSettlementChargesU07FHudline",
                "sSellerSettlementChargesU08FHudline",
                "sSellerSettlementChargesU09FHudline",
                "sSellerSettlementChargesU10FHudline",

                "sSellerSettlementChargesU01F",
                "sSellerSettlementChargesU02F",
                "sSellerSettlementChargesU03F",
                "sSellerSettlementChargesU04F",
                "sSellerSettlementChargesU05F",
                "sSellerSettlementChargesU06F",
                "sSellerSettlementChargesU07F",
                "sSellerSettlementChargesU08F",
                "sSellerSettlementChargesU09F",
                "sSellerSettlementChargesU10F",
            };

            idsToMigrate.UnionWith(LegacyCustomFeeFieldIds);
            closingCostFieldIdsToMigrate = idsToMigrate;

            var housingExpenseMigrationDependencies = CPageData.GetCPageBaseAndCAppDataDependencyList(
                typeof(HousingExpensesMigration));

            var otherDependencies = new List<string>()
            {
                // Misc
                "sClosingCostFeeVersionT",
                "sRecFLckd",
                "sRecF",
                "sSellerResponsibleClosingCostSet",
                "sClosingCostArchive",

                // Archive migration dependencies.
                "sfArchiveGFE",
                "sClosingCostFeeVersionT",
                "sfArchiveClosingCosts",
                "sClosingCostSet",
                "sClosingCostCoCArchive"
            };

            var allDependencies = new HashSet<string>(closingCostFieldIdsToMigrate);
            allDependencies.UnionWith(housingExpenseMigrationDependencies);
            allDependencies.UnionWith(otherDependencies);

            dependencyList = allDependencies;
        }

        /// <summary>
        /// Perform migration by loan id.
        /// </summary>
        /// <param name="principal">Principal that use to perform migration.</param>
        /// <param name="loanId">Loan id to migrate.</param>
        /// <param name="version">Migrate to version.</param>
        public static void Migrate(AbstractUserPrincipal principal, Guid loanId, E_sClosingCostFeeVersionT version)
        {
            Migrate(principal, loanId, version, false);
        }

        /// <summary>
        /// Perform migration by loan id.
        /// </summary>
        /// <param name="principal">Principal that use to perform migration.</param>
        /// <param name="loanId">Loan id to migrate.</param>
        /// <param name="version">Migrate to version.</param>
        /// <param name="legacyOnly">If true, only migrates if loan fee version is Legacy. Prevents remigration (and possible data loss).</param>
        public static void Migrate(AbstractUserPrincipal principal, Guid loanId, E_sClosingCostFeeVersionT version, bool legacyOnly)
        {
            try
            {
                if (version == E_sClosingCostFeeVersionT.Legacy)
                {
                    throw new CBaseException("Cannot migrate to Legacy.", "Cannot migrate to legacy");
                }

                Stopwatch sw = Stopwatch.StartNew();

                CPageData dataLoan = new NotEnforceAccessControlPageData(loanId, dependencyList);
                dataLoan.ByPassFieldSecurityCheck = true;
                dataLoan.AllowSaveWhileQP2Sandboxed = true;
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                Migrate(principal, dataLoan, version, legacyOnly);

                // 3/24/2015 dd - Perform additional cleanup.
                dataLoan.Save();

                Tools.LogInfo("ClosingCostFeeMigration", "Migrate loanid=" + loanId + " in " + sw.ElapsedMilliseconds + "ms.");
            }
            catch (CBaseException exc) 
            {
                // Probably have to allow catch all.  We want to do this for any exception.
                Tools.LogErrorWithCriticalTracking("Closing Cost migration failed: " + loanId.ToString(), exc);

                // Nothing new commited to DB.  Re-throw with special error message.
                throw new CBaseException("An unexpected error occurred that prevented fee migration. Please contact support or retry at a later time.", exc);
            }
        }

        /// <summary>
        /// Perform migration by loan id without saving. USE WITH CAUTION: returned loan
        /// does not enforce access control.
        /// </summary>
        /// <param name="principal">Principal that use to perform migration.</param>
        /// <param name="loanId">Loan id to migrate.</param>
        /// <param name="version">Migrate to version.</param>
        /// <param name="additionalLoanDependencies">
        /// The dependencies that will need to be accessed after the loan is returned.
        /// </param>
        /// <returns>
        /// Migrated loan object. **The returned object will NOT enforce access control**.
        /// </returns>
        public static NotEnforceAccessControlPageData MigrateWithoutSaving(
            AbstractUserPrincipal principal,
            Guid loanId,
            E_sClosingCostFeeVersionT version,
            IEnumerable<string> additionalLoanDependencies)
        {
            try
            {
                if (version == E_sClosingCostFeeVersionT.Legacy)
                {
                    throw new CBaseException("Cannot migrate to Legacy.", "Cannot migrate to legacy");
                }

                var dataLoan = new NotEnforceAccessControlPageData(
                    loanId,
                    dependencyList.Union(additionalLoanDependencies));

                dataLoan.ByPassFieldSecurityCheck = true;
                dataLoan.AllowSaveWhileQP2Sandboxed = true;
                dataLoan.InitLoad();

                bool onlyMigrateFileIfLegacy = true;
                Migrate(principal, dataLoan, version, onlyMigrateFileIfLegacy);

                return dataLoan;
            }
            catch (CBaseException exc)
            {
                // Probably have to allow catch all.  We want to do this for any exception.
                Tools.LogErrorWithCriticalTracking("Closing Cost migration failed: " + loanId.ToString(), exc);

                // Nothing new commited to DB.  Re-throw with special error message.
                throw new CBaseException("An unexpected error occurred that prevented fee migration. Please contact support or retry at a later time.", exc);
            }
        }

        /// <summary>
        /// Perform migration on the given loan.
        /// </summary>
        /// <param name="principal">Principal that use to perform migration.</param>
        /// <param name="dataLoan">The loan to migrate.</param>
        /// <param name="version">Migrate to version.</param>
        /// <param name="legacyOnly">If true, only migrates if loan fee version is Legacy. Prevents remigration (and possible data loss).</param>
        private static void Migrate(AbstractUserPrincipal principal, CPageData dataLoan, E_sClosingCostFeeVersionT version, bool legacyOnly)
        {
            // Increase timeout so the thread doesn't abort due to a request timeout and put the loan in a bad state.
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Server.ScriptTimeout = 600; // 10 minutes
            }

            if (version == E_sClosingCostFeeVersionT.Legacy)
            {
                throw new CBaseException("Cannot migrate to Legacy.", "Cannot migrate to legacy");
            }

            if (principal.UserId != new Guid("11111111-1111-1111-1111-111111111111") && principal.LoginNm != "tasksystem")
            {
                if (dataLoan.sBrokerId != principal.BrokerId)
                {
                    throw new AccessDenied();
                }
            }

            // Prevent migrating downward/re-migration
            if (legacyOnly && dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy)
            {
                // Done. No work to do.
                Tools.LogInfo("ClosingCostFeeMigration", "Loan not migrated because it has already been migrated. loanid=" + dataLoan.sLId + ".");
                return;
            }

            List<KeyValuePair<string, string>> stringValueList = new List<KeyValuePair<string, string>>();

            CAppData dataApp = dataLoan.GetAppData(0);

            // Loan will throw an error if you attempt to set sClosingCostFeeVersionT to Legacy if sDisclosureRegulationT == TRID2015.
            // We need to bypass that behavior.
            bool old_sDisclosureRegulationTLckd = dataLoan.sDisclosureRegulationTLckd;
            E_sDisclosureRegulationT old_sDisclosureRegulationT = dataLoan.sDisclosureRegulationT;

            dataLoan.sDisclosureRegulationTLckd = true;
            dataLoan.sDisclosureRegulationT = E_sDisclosureRegulationT.Blank;

            dataLoan.sClosingCostFeeVersionT = E_sClosingCostFeeVersionT.Legacy;

            bool recFLckd = dataLoan.sRecFLckd;
            string recF = dataLoan.sRecF_rep;

            foreach (var fieldName in closingCostFieldIdsToMigrate)
            {
                string value = PageDataUtilities.GetValue(dataLoan, dataApp, fieldName);

                if (fieldName == "sRecDeed" && recFLckd)
                {
                    // 3/24/2015 dd - When 1202 mortgage fee is locked then when migrate populate full amount to sRecDeed.
                    value = recF;
                }
                else if (fieldName == "sRecMortgage" && recFLckd)
                {
                    value = "$0.00";
                }
                else if (fieldName == "sRecRelease" && recFLckd)
                {
                    value = "$0.00";
                }

                if (value == "Yes")
                {
                    value = "True";
                }
                else if (value == "No")
                {
                    value = "False";
                }

                stringValueList.Add(new KeyValuePair<string, string>(fieldName, value));
            }

            if (recFLckd)
            {
                dataLoan.sRecFLckd = false;
            }

            string compPc = dataLoan.sGfeOriginatorCompFPc_rep;
            E_PercentBaseT compPcT = dataLoan.sGfeOriginatorCompFBaseT;
            string compMb = dataLoan.sGfeOriginatorCompFMb_rep;
            int props = dataLoan.sGfeOriginatorCompFProps;

            if (dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid)
            {
                compPc = dataLoan.sOriginatorCompensationBorrPaidPc_rep;
                compPcT = dataLoan.sOriginatorCompensationBorrPaidBaseT;
                compMb = dataLoan.sOriginatorCompensationBorrPaidMb_rep;
            }

            string lastDisclosedGFEArchiveD_rep = dataLoan.sLastDisclosedGFEArchiveD_rep;

            dataLoan.sClosingCostFeeVersionT = version;

            // 3/24/2015 dd - Clear existing closing cost.
            dataLoan.sClosingCostSet.ClearFeeList();

            // Copy old field values into new datalayer.
            HousingExpensesMigration.MigrateExpenses(dataLoan);

            dataLoan.sClosingCostSet.IsMigration = true;

            Func<BaseClosingCostFee, bool> borrowerSetFilter = borrowerFee => ClosingCostSetUtils.OrigCompDiscountPrepsSecGMIFilter(
                                                                                (BorrowerClosingCostFee)borrowerFee,
                                                                                dataLoan.sOriginatorCompensationPaymentSourceT,
                                                                                dataLoan.sDisclosureRegulationT,
                                                                                dataLoan.sGfeIsTPOTransaction);

            // Disable beneficiary migration on fees created during initialization (ie. before we could set IsMigrated)
            foreach (BorrowerClosingCostFee fee in dataLoan.sClosingCostSet.GetFees(borrowerSetFilter))
            {
                fee.DisableBeneficiaryAutomation = true;
                fee.ClearBeneficiary();
            }

            foreach (var kvp in stringValueList)
            {
                if (kvp.Key == "sRecFProps")
                {
                    BorrowerClosingCostFee deed = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.sRecDeed, dataLoan.sBrokerId, true);
                    BorrowerClosingCostFee mortgage = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.sRecMortgage, dataLoan.sBrokerId, true);
                    BorrowerClosingCostFee release = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.sRecRelease, dataLoan.sBrokerId, true);

                    int i;
                    if (int.TryParse(kvp.Value, out i) == false)
                    {
                        throw new FieldInvalidValueException(kvp.Key, kvp.Value);
                    }

                    LosConvert.SetFeeWithLegacyFeeProps(deed, i);
                    LosConvert.SetFeeWithLegacyFeeProps(mortgage, i);
                    LosConvert.SetFeeWithLegacyFeeProps(release, i);
                }
                else if (kvp.Key == "sRecFDesc")
                {
                    BorrowerClosingCostFee deed = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.sRecDeed, dataLoan.sBrokerId, true);
                    BorrowerClosingCostFee mortgage = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.sRecMortgage, dataLoan.sBrokerId, true);
                    BorrowerClosingCostFee release = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.sRecRelease, dataLoan.sBrokerId, true);

                    deed.SetBeneficiaryDescription(E_AgentRoleT.Other, kvp.Value);
                    mortgage.SetBeneficiaryDescription(E_AgentRoleT.Other, kvp.Value);
                    release.SetBeneficiaryDescription(E_AgentRoleT.Other, kvp.Value);
                }
                else if (LegacyCustomFeeFieldIds.Contains(kvp.Key))
                {
                    // OPM 221272 - Don't add custom fees via legacy field mapping. We need something more complex than that, so just continue for now.
                    continue;
                }
                else
                {
                    PageDataUtilities.SetValue(dataLoan, dataApp, kvp.Key, kvp.Value);
                }
            }

            // OPM 221727 - Add legacy custom fees to migrated loan.
            Dictionary<string, string> customFeeValues = stringValueList.ToDictionary(k => k.Key, v => v.Value);

            // 800
            MigrateCustomFee(dataLoan, 813, customFeeValues["s800U1FDesc"], null, null, customFeeValues["s800U1F"], customFeeValues["s800U1FGfeSection"], customFeeValues["s800U1FProps"], customFeeValues["s800U1FPaidTo"]);
            MigrateCustomFee(dataLoan, 814, customFeeValues["s800U2FDesc"], null, null, customFeeValues["s800U2F"], customFeeValues["s800U2FGfeSection"], customFeeValues["s800U2FProps"], customFeeValues["s800U2FPaidTo"]);
            MigrateCustomFee(dataLoan, 815, customFeeValues["s800U3FDesc"], null, null, customFeeValues["s800U3F"], customFeeValues["s800U3FGfeSection"], customFeeValues["s800U3FProps"], customFeeValues["s800U3FPaidTo"]);
            MigrateCustomFee(dataLoan, 816, customFeeValues["s800U4FDesc"], null, null, customFeeValues["s800U4F"], customFeeValues["s800U4FGfeSection"], customFeeValues["s800U4FProps"], customFeeValues["s800U4FPaidTo"]);
            MigrateCustomFee(dataLoan, 817, customFeeValues["s800U5FDesc"], null, null, customFeeValues["s800U5F"], customFeeValues["s800U5FGfeSection"], customFeeValues["s800U5FProps"], customFeeValues["s800U5FPaidTo"]);

            // 900
            MigrateCustomFee(dataLoan, 904, customFeeValues["s904PiaDesc"], null, null, customFeeValues["s904Pia"], customFeeValues["s904PiaGfeSection"], customFeeValues["s904PiaProps"], null);
            MigrateCustomFee(dataLoan, 906, customFeeValues["s900U1PiaDesc"], null, null, customFeeValues["s900U1Pia"], customFeeValues["s900U1PiaGfeSection"], customFeeValues["s900U1PiaProps"], null);

            // 1100
            MigrateCustomFee(dataLoan, 1112, customFeeValues["sU1TcDesc"], null, null, customFeeValues["sU1Tc"], customFeeValues["sU1TcGfeSection"], customFeeValues["sU1TcProps"], customFeeValues["sU1TcPaidTo"]);
            MigrateCustomFee(dataLoan, 1113, customFeeValues["sU2TcDesc"], null, null, customFeeValues["sU2Tc"], customFeeValues["sU2TcGfeSection"], customFeeValues["sU2TcProps"], customFeeValues["sU2TcPaidTo"]);
            MigrateCustomFee(dataLoan, 1114, customFeeValues["sU3TcDesc"], null, null, customFeeValues["sU3Tc"], customFeeValues["sU3TcGfeSection"], customFeeValues["sU3TcProps"], customFeeValues["sU3TcPaidTo"]);
            MigrateCustomFee(dataLoan, 1115, customFeeValues["sU4TcDesc"], null, null, customFeeValues["sU4Tc"], customFeeValues["sU4TcGfeSection"], customFeeValues["sU4TcProps"], customFeeValues["sU4TcPaidTo"]);

            // 1200
            MigrateCustomFee(dataLoan, 1206, customFeeValues["sU1GovRtcDesc"], customFeeValues["sU1GovRtcPc"], customFeeValues["sU1GovRtcBaseT"], customFeeValues["sU1GovRtcMb"], customFeeValues["sU1GovRtcGfeSection"], customFeeValues["sU1GovRtcProps"], customFeeValues["sU1GovRtcPaidTo"]);
            MigrateCustomFee(dataLoan, 1207, customFeeValues["sU2GovRtcDesc"], customFeeValues["sU2GovRtcPc"], customFeeValues["sU2GovRtcBaseT"], customFeeValues["sU2GovRtcMb"], customFeeValues["sU2GovRtcGfeSection"], customFeeValues["sU2GovRtcProps"], customFeeValues["sU2GovRtcPaidTo"]);
            MigrateCustomFee(dataLoan, 1208, customFeeValues["sU3GovRtcDesc"], customFeeValues["sU3GovRtcPc"], customFeeValues["sU3GovRtcBaseT"], customFeeValues["sU3GovRtcMb"], customFeeValues["sU3GovRtcGfeSection"], customFeeValues["sU3GovRtcProps"], customFeeValues["sU3GovRtcPaidTo"]);

            // 1300
            MigrateCustomFee(dataLoan, 1303, customFeeValues["sU1ScDesc"], null, null, customFeeValues["sU1Sc"], customFeeValues["sU1ScGfeSection"], customFeeValues["sU1ScProps"], customFeeValues["sU1ScPaidTo"]);
            MigrateCustomFee(dataLoan, 1304, customFeeValues["sU2ScDesc"], null, null, customFeeValues["sU2Sc"], customFeeValues["sU2ScGfeSection"], customFeeValues["sU2ScProps"], customFeeValues["sU2ScPaidTo"]);
            MigrateCustomFee(dataLoan, 1305, customFeeValues["sU3ScDesc"], null, null, customFeeValues["sU3Sc"], customFeeValues["sU3ScGfeSection"], customFeeValues["sU3ScProps"], customFeeValues["sU3ScPaidTo"]);
            MigrateCustomFee(dataLoan, 1306, customFeeValues["sU4ScDesc"], null, null, customFeeValues["sU4Sc"], customFeeValues["sU4ScGfeSection"], customFeeValues["sU4ScProps"], customFeeValues["sU4ScPaidTo"]);
            MigrateCustomFee(dataLoan, 1307, customFeeValues["sU5ScDesc"], null, null, customFeeValues["sU5Sc"], customFeeValues["sU5ScGfeSection"], customFeeValues["sU5ScProps"], customFeeValues["sU5ScPaidTo"]);

            BorrowerClosingCostFee originatorCompFee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.sGfeOriginatorCompF, dataLoan.sBrokerId, true);
            originatorCompFee.DisableBeneficiaryAutomation = true;
            originatorCompFee.ClearBeneficiary();
            originatorCompFee.Percent_rep = compPc;
            originatorCompFee.PercentBaseT = compPcT;
            originatorCompFee.BaseAmount_rep = compMb;
            dataLoan.sGfeOriginatorCompFProps = props;

            // 4/17/2015 AV - 210403 Closing Cost Fee Payment: Add property/UI for buyer/seller responsible
            var sellerCharges = new[] 
                {
                    new { HudLine = dataLoan.sSellerSettlementChargesU01FHudline_rep, amount = dataLoan.sSellerSettlementChargesU01F },
                    new { HudLine = dataLoan.sSellerSettlementChargesU02FHudline_rep, amount = dataLoan.sSellerSettlementChargesU02F },
                    new { HudLine = dataLoan.sSellerSettlementChargesU03FHudline_rep, amount = dataLoan.sSellerSettlementChargesU03F },
                    new { HudLine = dataLoan.sSellerSettlementChargesU04FHudline_rep, amount = dataLoan.sSellerSettlementChargesU04F },
                    new { HudLine = dataLoan.sSellerSettlementChargesU05FHudline_rep, amount = dataLoan.sSellerSettlementChargesU05F },
                    new { HudLine = dataLoan.sSellerSettlementChargesU06FHudline_rep, amount = dataLoan.sSellerSettlementChargesU06F },
                    new { HudLine = dataLoan.sSellerSettlementChargesU07FHudline_rep, amount = dataLoan.sSellerSettlementChargesU07F },
                    new { HudLine = dataLoan.sSellerSettlementChargesU08FHudline_rep, amount = dataLoan.sSellerSettlementChargesU08F },
                    new { HudLine = dataLoan.sSellerSettlementChargesU09FHudline_rep, amount = dataLoan.sSellerSettlementChargesU09F },
                    new { HudLine = dataLoan.sSellerSettlementChargesU10FHudline_rep, amount = dataLoan.sSellerSettlementChargesU10F },
                };

            Func<BaseClosingCostFee, bool> feeFilter = fee => ClosingCostSetUtils.OrigCompDiscountPrepsSecGMIFilter(
                                                                (BorrowerClosingCostFee)fee, 
                                                                dataLoan.sOriginatorCompensationPaymentSourceT, 
                                                                dataLoan.sDisclosureRegulationT, 
                                                                dataLoan.sGfeIsTPOTransaction);
            
            ILookup<int, BorrowerClosingCostFee> feeByHudLine = dataLoan.sClosingCostSet.GetFees(feeFilter).Cast<BorrowerClosingCostFee>().ToLookup(p => p.HudLine);

            SellerClosingCostSet sellerFees = dataLoan.sSellerResponsibleClosingCostSet;
            sellerFees.ClearFeeList();

            foreach (var charge in sellerCharges)
            {
                if (charge.amount == 0)
                {
                    continue;
                }

                int hudline;

                if (int.TryParse(charge.HudLine, out hudline))
                {
                    if (feeByHudLine.Contains(hudline))
                    {
                        BorrowerClosingCostFee fee = feeByHudLine[hudline].First();
                        SellerClosingCostFee clonedFee = fee.ConvertToSellerFee();
                        clonedFee.SetClosingCostSet(sellerFees);
                        clonedFee.ClearPayments();
                        clonedFee.FormulaT = E_ClosingCostFeeFormulaT.MinBaseOnly;
                        clonedFee.BaseAmount = charge.amount;
                        SellerClosingCostFeePayment payment = (SellerClosingCostFeePayment)clonedFee.Payments.First();
                        payment.PaidByT = E_ClosingCostFeePaymentPaidByT.Seller;
                        payment.GfeClosingCostFeePaymentTimingT = E_GfeClosingCostFeePaymentTimingT.AtClosing;
                        payment.ResponsiblePartyT = E_GfeResponsiblePartyT.Seller;
                        sellerFees.AddOrUpdate(clonedFee);
                    }
                }
            }

            // 05/01/2015. mf. OPM 212101. Migrate out existing GFE Archives.
            if (dataLoan.GFEArchives.Count() != 0)
            {
                List<ClosingCostArchive> migratedArchives = new List<ClosingCostArchive>();

                // Load new instance of this loan in legacy.  We will apply this old-style archive to it.
                // Have to be in legacy mode for old archive values to set correctly.
                CPageData dataLoanArchiveMigraterLegacy = new NotEnforceAccessControlPageData(dataLoan.sLId, dependencyList);

                dataLoan.AllowLoadWhileQP2Sandboxed = true;
                dataLoan.ByPassFieldSecurityCheck = true;

                dataLoanArchiveMigraterLegacy.InitLoad();

                dataLoanArchiveMigraterLegacy.sDisclosureRegulationTLckd = true;
                dataLoanArchiveMigraterLegacy.sDisclosureRegulationT = E_sDisclosureRegulationT.Blank;
                dataLoanArchiveMigraterLegacy.sClosingCostFeeVersionT = E_sClosingCostFeeVersionT.Legacy;

                var dataAppArchiveMigraterLegacy = dataLoanArchiveMigraterLegacy.GetAppData(0);

                List<ClosingCostCoCArchive> newCoCList = new List<ClosingCostCoCArchive>();

                foreach (var archive in dataLoanArchiveMigraterLegacy.GFEArchives)
                {
                    dataLoanArchiveMigraterLegacy.ApplyGFEArchive(archive);

                    // Load in instance in non-legacy, so that updating the fee properties updates the fee set.
                    CPageData dataLoanArchiveMigrater = new NotEnforceAccessControlPageData(dataLoan.sLId, dependencyList);
                    dataLoan.AllowLoadWhileQP2Sandboxed = true;
                    dataLoan.ByPassFieldSecurityCheck = true;
                    dataLoanArchiveMigrater.InitLoad();

                    dataLoanArchiveMigrater.sDisclosureRegulationTLckd = true;
                    dataLoanArchiveMigrater.sDisclosureRegulationT = E_sDisclosureRegulationT.Blank;
                    dataLoanArchiveMigrater.sClosingCostFeeVersionT = E_sClosingCostFeeVersionT.Legacy;

                    dataLoanArchiveMigrater.ApplyGFEArchive(archive); // For other things, like loan amt.
                    dataLoanArchiveMigrater.sClosingCostFeeVersionT = version;

                    var dataAppArchiveMigrater = dataLoanArchiveMigrater.GetAppData(0);

                    // Apply the migration field-set to the in-memory instance from the archive data.
                    dataLoanArchiveMigrater.sClosingCostSet.IsMigration = true;
                    foreach (var kvp in stringValueList)
                    {
                        var value = PageDataUtilities.GetValue(dataLoanArchiveMigraterLegacy, dataAppArchiveMigraterLegacy, kvp.Key);

                        // GetValue() seems to produce bools in a format SetValue() cannot take.
                        if (value == "Yes")
                        {
                            value = "true";
                        }
                        else if (value == "No")
                        {
                            value = "false";
                        }

                        PageDataUtilities.SetValue(dataLoanArchiveMigrater, dataAppArchiveMigrater, kvp.Key, value);
                    }

                    // In-memory housing expense migration.
                    HousingExpensesMigration.MigrateExpenses(dataLoanArchiveMigrater);

                    // OPM  217508. CoC history is core loan file data and should be preserved to the fullest extent possible.
                    // Duplicate it.
                    var correspondingCoc = dataLoanArchiveMigraterLegacy.CoCArchives.FirstOrDefault(coc => coc.DateArchived == archive.DateArchived);
                    if (correspondingCoc != null)
                    {
                        var fees = new List<LendersOffice.Common.SerializationTypes.ClosingCostCoCArchive.Fee>();

                        Action<string, string> addIfExists = (amt, reason) =>
                        {
                            if (!string.IsNullOrEmpty(amt)
                                || !string.IsNullOrEmpty(reason))
                            {
                                if (!(amt == "$0.00" && reason == string.Empty))
                                {
                                    fees.Add(new ClosingCostCoCArchive.Fee()
                                    {
                                        Value = dataLoanArchiveMigraterLegacy.m_convertLos.ToDecimal(amt),
                                        Description = reason
                                    });
                                }
                            }
                        };

                        addIfExists(correspondingCoc.sCircumstanceChange1Amount, correspondingCoc.sCircumstanceChange1Reason);
                        addIfExists(correspondingCoc.sCircumstanceChange2Amount, correspondingCoc.sCircumstanceChange2Reason);
                        addIfExists(correspondingCoc.sCircumstanceChange3Amount, correspondingCoc.sCircumstanceChange3Reason);
                        addIfExists(correspondingCoc.sCircumstanceChange4Amount, correspondingCoc.sCircumstanceChange4Reason);
                        addIfExists(correspondingCoc.sCircumstanceChange5Amount, correspondingCoc.sCircumstanceChange5Reason);
                        addIfExists(correspondingCoc.sCircumstanceChange6Amount, correspondingCoc.sCircumstanceChange6Reason);
                        addIfExists(correspondingCoc.sCircumstanceChange7Amount, correspondingCoc.sCircumstanceChange7Reason);
                        addIfExists(correspondingCoc.sCircumstanceChange8Amount, correspondingCoc.sCircumstanceChange8Reason);

                        var newCoc = new ClosingCostCoCArchive()
                        {
                            DateArchived = correspondingCoc.DateArchived,
                            CircumstanceChangeD = correspondingCoc.sCircumstanceChangeD,
                            RedisclosureD = correspondingCoc.sGfeRedisclosureD,
                            CircumstanceChangeExplanation = correspondingCoc.sCircumstanceChangeExplanation,
                            Fees = fees
                        };
                        
                        newCoCList.Add(newCoc);
                    }

                    // We have to do this so we can get the json updated without a save.
                    dataLoanArchiveMigrater.FlushClosingCostSetForMigration();

                    // Default to disclosed status. That will maintain the system
                    // behavior from before statuses were added to archives.
                    ClosingCostArchive newArchive = new ClosingCostArchive(
                        archive.DateArchived,
                        ClosingCostArchive.E_ClosingCostArchiveType.Gfe2015,
                        ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed,
                        ClosingCostArchive.E_ClosingCostArchiveSource.Migration);
                    newArchive.ExtractFromLoan(dataLoanArchiveMigrater);

                    migratedArchives.Add(newArchive);
                }

                dataLoan.UpdateClosingCostArchiveForMigration(migratedArchives, newCoCList);

                // Now that archives have been migrated, migrate archive date to ID.
                if (!string.IsNullOrEmpty(lastDisclosedGFEArchiveD_rep))
                {
                    ClosingCostArchive archive = dataLoan.sClosingCostArchive.FirstOrDefault(p => p.DateArchived == lastDisclosedGFEArchiveD_rep
                        && p.ClosingCostArchiveType != ClosingCostArchive.E_ClosingCostArchiveType.ClosingDisclosure);

                    // Just let it have no Id value if sLastDisclosedGFEArchiveD is for legacy (GFE2010) archive (bad value for bad loan). Will fix itself when next archive created.
                    dataLoan.sLastDisclosedGFEArchiveId = archive == null ? Guid.Empty : archive.Id;
                }
            }

            // Reset old sDisclosureRegulationT values.
            dataLoan.sDisclosureRegulationTLckd = old_sDisclosureRegulationTLckd;
            dataLoan.sDisclosureRegulationT = old_sDisclosureRegulationT;
        }

        /// <summary>
        /// Migrates custom fee by selecting the appropriate fee type based of description and copying over the given values to the new fee.
        /// </summary>
        /// <param name="dataLoan">The data loan.</param>
        /// <param name="hudLine">Fee Hudline.</param>
        /// <param name="description">Fee description.</param>
        /// <param name="percent">Fee amount percentage value.</param>
        /// <param name="percentBaseT">Fee amount percent base.</param>
        /// <param name="baseAmount">Fee amount flat dollar amount.</param>
        /// <param name="gfeSectionT">Fee GFE section.</param>
        /// <param name="props">Fee properties.</param>
        /// <param name="paidTo">Who the fee is paid to.</param>
        private static void MigrateCustomFee(CPageData dataLoan, int hudLine, string description, string percent, string percentBaseT, string baseAmount, string gfeSectionT, string props, string paidTo)
        {
            BorrowerClosingCostFee fee = null;

            // If custom fee type with same description exists in the fee setup, and that fee type does not already exist in the loan, create fee from that fee type.
            Func<BaseClosingCostFee, bool> feeTypeFilter = f => f.IsSystemLegacyFee == false &&
                                                                f.HudLine / 100 == hudLine / 100 && 
                                                                f.Description.Equals(description, StringComparison.OrdinalIgnoreCase);
            FeeSetupClosingCostFee feeType = (FeeSetupClosingCostFee)dataLoan.sBrokerUnlinkedClosingCostSet.GetFees(feeTypeFilter).FirstOrDefault();
            
            if (feeType != null && dataLoan.sClosingCostSet.FindFeeByTypeId(feeType.ClosingCostFeeTypeId) == null)
            {
                fee = feeType.ConvertToBorrowerClosingCostFee();
                fee.OriginalDescription = feeType.Description;
            }
            else
            {
                // If no matching fee type found, or a fee of that fee type has already been added, use legacy custom fee
                switch (hudLine)
                {
                    case 813:
                        fee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.s800U1F, dataLoan.sBrokerId, true);
                        break;
                    case 814:
                        fee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.s800U2F, dataLoan.sBrokerId, true);
                        break;
                    case 815:
                        fee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.s800U3F, dataLoan.sBrokerId, true);
                        break;
                    case 816:
                        fee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.s800U4F, dataLoan.sBrokerId, true);
                        break;
                    case 817:
                        fee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.s800U5F, dataLoan.sBrokerId, true);
                        break;

                    case 904:
                        fee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.s904Pia, dataLoan.sBrokerId, true);
                        break;
                    case 906:
                        fee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.s900U1Pia, dataLoan.sBrokerId, true);
                        break;

                    case 1112:
                        fee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.sU1Tc, dataLoan.sBrokerId, true);
                        break;
                    case 1113:
                        fee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.sU2Tc, dataLoan.sBrokerId, true);
                        break;
                    case 1114:
                        fee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.sU3Tc, dataLoan.sBrokerId, true);
                        break;
                    case 1115:
                        fee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.sU4Tc, dataLoan.sBrokerId, true);
                        break;

                    case 1206:
                        fee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.sU1GovRtc, dataLoan.sBrokerId, true);
                        break;
                    case 1207:
                        fee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.sU2GovRtc, dataLoan.sBrokerId, true);
                        break;
                    case 1208:
                        fee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.sU3GovRtc, dataLoan.sBrokerId, true);
                        break;

                    case 1303:
                        fee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.sU1Sc, dataLoan.sBrokerId, true);
                        break;
                    case 1304:
                        fee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.sU2Sc, dataLoan.sBrokerId, true);
                        break;
                    case 1305:
                        fee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.sU3Sc, dataLoan.sBrokerId, true);
                        break;
                    case 1306:
                        fee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.sU4Sc, dataLoan.sBrokerId, true);
                        break;
                    case 1307:
                        fee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.sU5Sc, dataLoan.sBrokerId, true);
                        break;
                    default:
                        throw new CBaseException(ErrorMessages.Generic, "Closing Cost Fee Migration encountered a bad HUD line: " + hudLine); 
                }
            }

            // Set values.
            fee.HudLine = hudLine;
            fee.FormulaT = E_ClosingCostFeeFormulaT.Full;

            if (!string.IsNullOrEmpty(description))
            {
                fee.Description = description;
            }

            if (!string.IsNullOrEmpty(percent))
            {
                fee.Percent_rep = percent;
            }

            if (!string.IsNullOrEmpty(percentBaseT))
            {
                fee.PercentBaseT = (E_PercentBaseT)dataLoan.m_convertLos.ToCount(percentBaseT);
            }

            if (!string.IsNullOrEmpty(baseAmount))
            {
                fee.BaseAmount_rep = baseAmount;
            }

            if (!string.IsNullOrEmpty(gfeSectionT))
            {
                fee.GfeSectionT = (E_GfeSectionT)dataLoan.m_convertLos.ToCount(gfeSectionT);
            }

            if (!string.IsNullOrEmpty(props))
            {
                LosConvert.SetFeeWithLegacyFeeProps(fee, dataLoan.m_convertLos.ToCount(props));
            }

            if (!string.IsNullOrEmpty(paidTo))
            {
                fee.SetBeneficiaryDescription(E_AgentRoleT.Other, paidTo);
            }

            // Save fee.
            dataLoan.sClosingCostSet.AddOrUpdate(fee);
        }
    }
}
