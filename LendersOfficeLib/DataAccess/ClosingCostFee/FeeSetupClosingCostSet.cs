﻿// <copyright file="FeeSetupClosingCostSet.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   6/22/2015
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LendersOffice.Common;

    /// <summary>
    /// The class representing the broker level closing cost fee setup.
    /// </summary>
    public class FeeSetupClosingCostSet : BaseClosingCostSet
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FeeSetupClosingCostSet" /> class from json.
        /// </summary>
        /// <param name="json">Json to deserialize.</param>
        public FeeSetupClosingCostSet(string json)
            : base(json)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FeeSetupClosingCostSet" /> class from json.
        /// </summary>
        /// <param name="json">Json to deserialize.</param>
        /// <param name="isForclear">Whether this set is for updating/clearing another set.</param>
        public FeeSetupClosingCostSet(string json, bool isForclear)
            : base(json, isForclear)
        {
        }

        /// <summary>
        /// Gets a value indicating what set type this is. This will always be type LenderFeeSetup.
        /// </summary>
        /// <value>A value indicating what set type this is. This will always be type LenderFeeSetup.</value>
        public override E_sClosingCostSetType SetType
        {
            get
            {
                return E_sClosingCostSetType.LenderFeeSetup;
            }

            protected set 
            { 
            }
        }

        /// <summary>
        /// Gets the import mapping a <see cref="BaseClosingCostFee.ClosingCostFeeTypeId"/> for each MISMO 3.4 type.<para/>
        /// This value can and will be <see langword="null"/> if no value has been set.
        /// </summary>
        public IReadOnlyDictionary<Mismo3Specification.Version4Schema.FeeBase, Guid> Mismo34ImportMappings { get; private set; }

        /// <summary>
        /// Sets <see cref="Mismo34ImportMappings"/> and validates the input to ensure every value is mapped to a valid fee type.
        /// </summary>
        /// <param name="mappings">The import mappings.</param>
        /// <exception cref="CBaseException"><paramref name="mappings"/> was not a valid set of mappings.</exception>
        public void SetMismoImportMappings(IReadOnlyDictionary<Mismo3Specification.Version4Schema.FeeBase, Guid> mappings)
        {
            string errorMessage = this.ValidateMismoImportMappings(mappings);
            if (errorMessage != null)
            {
                throw new CBaseException(errorMessage, "Unable to set MISMO import mappings.");
            }

            this.Mismo34ImportMappings = mappings;
        }

        /// <summary>
        /// Get a list of appropriate fee sections by view type.
        /// </summary>
        /// <param name="viewType">View Type.</param>
        /// <returns>A list of appropriate fee sections.</returns>
        public override IEnumerable<BaseClosingCostFeeSection> GetViewBase(E_ClosingCostViewT viewType)
        {
            if (viewType != E_ClosingCostViewT.LenderTypeHud1 && viewType != E_ClosingCostViewT.LenderTypeEstimate)
            {
                throw new ArgumentException("FeeSetupClosingCostSet does not take view type: " + Enum.GetName(typeof(E_ClosingCostViewT), viewType));
            }

            List<BaseClosingCostFeeSection> list = new List<BaseClosingCostFeeSection>();

            switch (viewType)
            {
                case E_ClosingCostViewT.LenderTypeHud1:
                    FeeSetupClosingCostFee default800Fee = new FeeSetupClosingCostFee()
                    {
                        Description = "800 Template Fee",
                        HudLine = 800,
                        IsTitleFee = false,
                        IsOptional = false,
                        GfeSectionT = E_GfeSectionT.B1,
                        IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionA,
                        IsApr = true,
                        IsFhaAllowable = true,
                        Beneficiary = E_AgentRoleT.Lender,
                        IsThirdParty = false,
                        IsAffiliate = false,
                        CanShop = false,
                    };

                    FeeSetupClosingCostFee default900Fee = new FeeSetupClosingCostFee()
                    {
                        Description = "900 Template Fee",
                        HudLine = 900,
                        IsTitleFee = false,
                        IsOptional = false,
                        GfeSectionT = E_GfeSectionT.B11,
                        IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionF,
                        IsApr = false,
                        IsFhaAllowable = true,
                        Beneficiary = E_AgentRoleT.Lender,
                        IsThirdParty = false,
                        IsAffiliate = false,
                        CanShop = true,
                    };

                    FeeSetupClosingCostFee default1000Fee = new FeeSetupClosingCostFee()
                    {
                        Description = "1000 Template Fee",
                        GfeSectionT = E_GfeSectionT.B1,
                        Beneficiary = E_AgentRoleT.Lender,
                        IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionA,
                    };

                    FeeSetupClosingCostFee default1100Fee = new FeeSetupClosingCostFee()
                    {
                        Description = "1100 Template Fee",
                        HudLine = 1100,
                        IsTitleFee = true,
                        IsOptional = false,
                        GfeSectionT = E_GfeSectionT.B4,
                        IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionC,
                        IsApr = false,
                        IsFhaAllowable = true,
                        Beneficiary = E_AgentRoleT.Other,
                        IsThirdParty = false,
                        IsAffiliate = false,
                        CanShop = true,
                    };

                    FeeSetupClosingCostFee default1200Fee = new FeeSetupClosingCostFee()
                    {
                        Description = "1200 Template Fee",
                        HudLine = 1200,
                        IsTitleFee = false,
                        IsOptional = false,
                        GfeSectionT = E_GfeSectionT.B8,
                        IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionE,
                        IsApr = false,
                        IsFhaAllowable = true,
                        Beneficiary = E_AgentRoleT.Other,
                        IsThirdParty = false,
                        IsAffiliate = false,
                        CanShop = false,
                    };

                    FeeSetupClosingCostFee default1300Fee = new FeeSetupClosingCostFee()
                    {
                        Description = "1300 Template Fee",
                        HudLine = 1300,
                        IsTitleFee = false,
                        IsOptional = false,
                        GfeSectionT = E_GfeSectionT.B6,
                        IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionC,
                        IsApr = true,
                        IsFhaAllowable = true,
                        Beneficiary = E_AgentRoleT.Other,
                        IsThirdParty = false,
                        IsAffiliate = false,
                        CanShop = true,
                    };
                    list.Add(new FeeSetupClosingCostFeeSection(viewType, this, null, ClosingCostSetUtils.Hud800ItemsPayable, ClosingCostSetUtils.Section800Filter, default800Fee, ClosingCostSetUtils.Start800Hudline, ClosingCostSetUtils.End800Hudline, E_IntegratedDisclosureSectionT.LeaveBlank));
                    list.Add(new FeeSetupClosingCostFeeSection(viewType, this, null, ClosingCostSetUtils.Hud900ItemsRequiredByLenderToBePaid, ClosingCostSetUtils.Section900Filter, default900Fee, ClosingCostSetUtils.Start900Hudline, ClosingCostSetUtils.End900Hudline, E_IntegratedDisclosureSectionT.LeaveBlank));
                    list.Add(new FeeSetupClosingCostFeeSection(viewType, this, null, ClosingCostSetUtils.Hud1000ReservesDeposited, ClosingCostSetUtils.Section1000Filter, default1000Fee, ClosingCostSetUtils.Start1000Hudline, ClosingCostSetUtils.End1000Hudline, E_IntegratedDisclosureSectionT.LeaveBlank));
                    list.Add(new FeeSetupClosingCostFeeSection(viewType, this, null, ClosingCostSetUtils.Hud1100Title, ClosingCostSetUtils.Section1100Filter, default1100Fee, ClosingCostSetUtils.Start1100Hudline, ClosingCostSetUtils.End1100Hudline, E_IntegratedDisclosureSectionT.LeaveBlank));
                    list.Add(new FeeSetupClosingCostFeeSection(viewType, this, null, ClosingCostSetUtils.Hud1200Government, ClosingCostSetUtils.Section1200Filter, default1200Fee, ClosingCostSetUtils.Start1200Hudline, ClosingCostSetUtils.End1200Hudline, E_IntegratedDisclosureSectionT.LeaveBlank));
                    list.Add(new FeeSetupClosingCostFeeSection(viewType, this, null, ClosingCostSetUtils.Hud1300AdditionalSettlementCharge, ClosingCostSetUtils.Section1300Filter, default1300Fee, ClosingCostSetUtils.Start1300Hudline, ClosingCostSetUtils.End1300Hudline, E_IntegratedDisclosureSectionT.LeaveBlank));
                    break;
                case E_ClosingCostViewT.LenderTypeEstimate:
                    {
                        FeeSetupClosingCostFee defaultSectionAFee = new FeeSetupClosingCostFee()
                        {
                            Description = "Section A Template Fee",
                            HudLine = 800,
                            IsTitleFee = false,
                            IsOptional = false,
                            GfeSectionT = E_GfeSectionT.B1,
                            IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionA,
                            IsApr = true,
                            IsFhaAllowable = true,
                            Beneficiary = E_AgentRoleT.Lender,
                            IsThirdParty = false,
                            IsAffiliate = false,
                            CanShop = false,
                        };

                        FeeSetupClosingCostFee defaultSectionBFee = new FeeSetupClosingCostFee()
                        {
                            Description = "Section B Template Fee",
                            HudLine = 800,
                            IsTitleFee = false,
                            IsOptional = false,
                            GfeSectionT = E_GfeSectionT.B3,
                            IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionB,
                            IsApr = true,
                            IsFhaAllowable = true,
                            Beneficiary = E_AgentRoleT.Other,
                            IsThirdParty = false,
                            IsAffiliate = false,
                            CanShop = false,
                        };

                        FeeSetupClosingCostFee defaultSectionCFee = new FeeSetupClosingCostFee()
                        {
                            Description = "Section C Template Fee",
                            HudLine = 1300,
                            IsTitleFee = false,
                            IsOptional = false,
                            GfeSectionT = E_GfeSectionT.B6,
                            IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionC,
                            IsApr = true,
                            IsFhaAllowable = true,
                            Beneficiary = E_AgentRoleT.Other,
                            IsThirdParty = false,
                            IsAffiliate = false,
                            CanShop = true,
                        };

                        FeeSetupClosingCostFee defaultSectionEFee = new FeeSetupClosingCostFee()
                        {
                            Description = "Section E Template Fee",
                            HudLine = 1200,
                            IsTitleFee = false,
                            IsOptional = false,
                            GfeSectionT = E_GfeSectionT.B8,
                            IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionE,
                            IsApr = false,
                            IsFhaAllowable = true,
                            Beneficiary = E_AgentRoleT.Other,
                            IsThirdParty = false,
                            IsAffiliate = false,
                            CanShop = false,
                        };

                        FeeSetupClosingCostFee defaultSectionFFee = new FeeSetupClosingCostFee()
                        {
                            Description = "Section F Template Fee",
                            HudLine = 900,
                            IsTitleFee = false,
                            IsOptional = false,
                            GfeSectionT = E_GfeSectionT.B11,
                            IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionF,
                            IsApr = false,
                            IsFhaAllowable = true,
                            Beneficiary = E_AgentRoleT.Lender,
                            IsThirdParty = false,
                            IsAffiliate = false,
                            CanShop = true,
                        };

                        FeeSetupClosingCostFee defaultSectionGFee = new FeeSetupClosingCostFee();
                        defaultSectionGFee.Description = "Section G Template Fee";
                        defaultSectionGFee.IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionG;
                        defaultSectionGFee.ClosingCostFeeTypeId = DefaultSystemClosingCostFee.DefaultSectionGFeeTypeId;
                        defaultSectionGFee.Beneficiary = E_AgentRoleT.Lender;

                        FeeSetupClosingCostFee defaultSectionHFee = new FeeSetupClosingCostFee()
                        {
                            Description = "Section H Template Fee",
                            HudLine = 1300,
                            IsTitleFee = false,
                            IsOptional = true,
                            GfeSectionT = E_GfeSectionT.B6,
                            IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionH,
                            IsApr = false,
                            IsFhaAllowable = true,
                            Beneficiary = E_AgentRoleT.Other,
                            IsThirdParty = false,
                            IsAffiliate = false,
                            CanShop = true,
                        };

                        list.Add(new FeeSetupClosingCostFeeSection(viewType, this, null, ClosingCostSetUtils.SectionAOriginationCharges, ClosingCostSetUtils.SectionAFilter, defaultSectionAFee, ClosingCostSetUtils.Start800Hudline, ClosingCostSetUtils.End1300Hudline, E_IntegratedDisclosureSectionT.SectionA));
                        list.Add(new FeeSetupClosingCostFeeSection(viewType, this, null, ClosingCostSetUtils.SectionBServiceYouCannotShopFor, ClosingCostSetUtils.SectionBFilter, defaultSectionBFee, ClosingCostSetUtils.Start800Hudline, ClosingCostSetUtils.End1300Hudline, E_IntegratedDisclosureSectionT.SectionB));
                        list.Add(new FeeSetupClosingCostFeeSection(viewType, this, null, ClosingCostSetUtils.SectionCServiceYouCanShopFor, ClosingCostSetUtils.SectionCFilter, defaultSectionCFee, ClosingCostSetUtils.Start800Hudline, ClosingCostSetUtils.End1300Hudline, E_IntegratedDisclosureSectionT.SectionC));
                        list.Add(new FeeSetupClosingCostFeeSection(viewType, this, null, ClosingCostSetUtils.SectionETaxes, ClosingCostSetUtils.SectionEFilter, defaultSectionEFee, ClosingCostSetUtils.Start800Hudline, ClosingCostSetUtils.End1300Hudline, E_IntegratedDisclosureSectionT.SectionE));
                        list.Add(new FeeSetupClosingCostFeeSection(viewType, this, null, ClosingCostSetUtils.SectionFPrepaids, ClosingCostSetUtils.SectionFFilter, defaultSectionFFee, ClosingCostSetUtils.Start800Hudline, ClosingCostSetUtils.End1300Hudline, E_IntegratedDisclosureSectionT.SectionF));
                        list.Add(new FeeSetupClosingCostFeeSection(viewType, this, null, ClosingCostSetUtils.SectionGInitialEscrow, ClosingCostSetUtils.SectionGFilter, defaultSectionGFee, ClosingCostSetUtils.Start800Hudline, ClosingCostSetUtils.End1300Hudline, E_IntegratedDisclosureSectionT.SectionG));
                        list.Add(new FeeSetupClosingCostFeeSection(viewType, this, null, ClosingCostSetUtils.SectionHOther, ClosingCostSetUtils.SectionHFilter, defaultSectionHFee, ClosingCostSetUtils.Start800Hudline, ClosingCostSetUtils.End1300Hudline, E_IntegratedDisclosureSectionT.SectionH));
                    }

                    break;
                case E_ClosingCostViewT.LeaveBlank:
                case E_ClosingCostViewT.CombinedLenderTypeEstimate:
                case E_ClosingCostViewT.LoanClosingCost:
                case E_ClosingCostViewT.LoanHud1:
                case E_ClosingCostViewT.SellerResponsibleLenderTypeEstimate:
                case E_ClosingCostViewT.SellerResponsibleLoanHud1:
                default:
                    throw new UnhandledEnumException(viewType);
            }

            return list;
        }

        /// <summary>
        /// Gets a list of fee setup fee sections for serialization. USE THS FOR SERIALIZATION.
        /// </summary>
        /// <param name="viewType">The view type to construct the sections for.</param>
        /// <returns>The list of fee setup fee sections.</returns>
        public IEnumerable<FeeSetupClosingCostFeeSection> GetViewForSerialization(E_ClosingCostViewT viewType)
        {
            IEnumerable<BaseClosingCostFeeSection> sectionList = this.GetViewBase(viewType);

            List<FeeSetupClosingCostFeeSection> convertedList = new List<FeeSetupClosingCostFeeSection>();

            foreach (FeeSetupClosingCostFeeSection section in sectionList)
            {
                convertedList.Add(section);
            }

            return convertedList;
        }

        /// <summary>
        /// Serializes the underlying fee list to JSON. Specifically serializes as a list of FeeSetupClosingCostFees.
        /// </summary>
        /// <returns>Json representation of the fee setup closing cost fee list.</returns>
        public override string ToJson()
        {
            if (this.Mismo34ImportMappings != null)
            {
                string errorMessage = this.ValidateMismoImportMappings(this.Mismo34ImportMappings);
                if (errorMessage != null)
                {
                    throw new CBaseException(errorMessage, "Unable to serialize MISMO import mappings due to inconsistency.");
                }
            }

            var storage = new FeeSetupClosingCostSetStorage
            {
                Fees = this.ClosingCostFeeList.Cast<FeeSetupClosingCostFee>().ToList(),
                Mismo34ImportMappings = this.Mismo34ImportMappings?.ToDictionary(kvp => kvp.Key, kvp => kvp.Value),
            };

            return ObsoleteSerializationHelper.JsonSerializeAndSanitize(storage);
        }
        
        /// <summary>
        /// Gets the first fee found based on legacy GFE type. Does not make a new fee.
        /// </summary>
        /// <param name="type">The legacy GFE field type.</param>
        /// <param name="brokerId">Not used for this implementation.</param>
        /// <param name="addFeeToSet">Parameter not used.</param>
        /// <returns>The matching fee if found, null otherwise.</returns>
        public override BaseClosingCostFee GetFirstFeeByLegacyType(E_LegacyGfeFieldT type, Guid brokerId, bool addFeeToSet)
        {
            BaseClosingCostFee firstcheck = this.CheckForCustomFee(type, brokerId, addFeeToSet);

            if (firstcheck != null)
            {
                return firstcheck;
            }

            Guid feeTypeId = Guid.Empty;
            if (type == E_LegacyGfeFieldT.sHazInsRsrv)
            {
                feeTypeId = DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId;
            }
            else if (type == E_LegacyGfeFieldT.sMInsRsrv)
            {
                feeTypeId = DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId;
            }
            else if (type == E_LegacyGfeFieldT.sRealETxRsrv)
            {
                feeTypeId = DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId;
            }
            else if (type == E_LegacyGfeFieldT.sSchoolTxRsrv)
            {
                feeTypeId = DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId;
            }
            else if (type == E_LegacyGfeFieldT.sFloodInsRsrv)
            {
                feeTypeId = DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId;
            }
            else if (type == E_LegacyGfeFieldT.s1006Rsrv)
            {
                feeTypeId = DefaultSystemClosingCostFee.Hud1000Custom1FeeTypeId;
            }
            else if (type == E_LegacyGfeFieldT.s1007Rsrv)
            {
                feeTypeId = DefaultSystemClosingCostFee.Hud1000Custom2FeeTypeId;
            }
            else if (type == E_LegacyGfeFieldT.sU3Rsrv)
            {
                feeTypeId = DefaultSystemClosingCostFee.Hud1000Custom3FeeTypeId;
            }
            else if (type == E_LegacyGfeFieldT.sU4Rsrv)
            {
                feeTypeId = DefaultSystemClosingCostFee.Hud1000Custom4FeeTypeId;
            }
            else if (type == E_LegacyGfeFieldT.sAggregateAdjRsrv)
            {
                feeTypeId = DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId;
            }
            else if (type == E_LegacyGfeFieldT.sGfeOriginatorComp_BorrowerPaid || type == E_LegacyGfeFieldT.sGfeOriginatorCompF || type == E_LegacyGfeFieldT.sGfeOriginatorCompF_Manual)
            {
                feeTypeId = DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId;
            }

            foreach (FeeSetupClosingCostFee fee in this.ClosingCostFeeList)
            {
                if (fee.LegacyGfeFieldT == type || fee.ClosingCostFeeTypeId == feeTypeId)
                {
                    return fee;
                }
            }

            // If no fee found in fee setup, then return null.
            return null;
        }

        /// <summary>
        /// Creates an empty FeeSetupClosingCostSet.
        /// </summary>
        /// <returns>The empty FeeSetupClosingCostSet.</returns>
        protected override BaseClosingCostSet CreateEmptySet()
        {
            return new FeeSetupClosingCostSet(string.Empty);
        }

        /// <summary>
        /// Creates a new empty closing cost set that does not automatically create fees and enumerates through ALL fees.
        /// </summary>
        /// <returns>The empty set.</returns>
        protected override BaseClosingCostSet CreateEmptySetForClearing()
        {
            return new FeeSetupClosingCostSet(string.Empty, true);
        }

        /// <summary>
        /// Deserialize the json into a list of FeeSetupClosingCostFees. 
        /// </summary>
        /// <param name="json">Json content to deserialize.</param>
        protected override void Initialize(string json)
        {
            if (string.IsNullOrEmpty(json))
            {
                this.ClosingCostFeeList = new List<BaseClosingCostFee>();
            }
            else
            {
                List<FeeSetupClosingCostFee> list;
                if (json.StartsWith("{"))
                {
                    FeeSetupClosingCostSetStorage storageObject = ObsoleteSerializationHelper.JsonDeserialize<FeeSetupClosingCostSetStorage>(json);
                    list = storageObject.Fees;
                    this.Mismo34ImportMappings = storageObject.Mismo34ImportMappings == null ? null : new System.Collections.ObjectModel.ReadOnlyDictionary<Mismo3Specification.Version4Schema.FeeBase, Guid>(storageObject.Mismo34ImportMappings);
                }
                else
                {
                    list = ObsoleteSerializationHelper.JsonDeserialize<List<FeeSetupClosingCostFee>>(json);
                    this.Mismo34ImportMappings = null; // it already is, but let's be explicit
                }

                this.ClosingCostFeeList = new List<BaseClosingCostFee>();

                foreach (FeeSetupClosingCostFee o in list)
                {
                    if (!o.IsValidFee())
                    {
                        continue;
                    }

                    // Correct the default system fees incase they have been warped due to constant changes.
                    if (DefaultSystemClosingCostFee.IsDefaultFee(o.ClosingCostFeeTypeId))
                    {
                        // Correct the formulas.
                        E_ClosingCostFeeFormulaT? type = ClosingCostSetUtils.GetFormulaType(o.IntegratedDisclosureSectionT, o.LegacyGfeFieldT, o.ClosingCostFeeTypeId);
                        if (type.HasValue)
                        {
                            o.FormulaT = type.Value;
                        }
                        else
                        {
                            // All default system fees at the Fee Setup level have a formula of Full if they are not assigned a specific formula.
                            o.FormulaT = E_ClosingCostFeeFormulaT.Full;
                        }
                    }

                    this.ClosingCostFeeList.Add(o);
                }
            }

            this.ClearFeeLookupCache();
        }

        /// <summary>
        /// Validates the mappings specified for <see cref="Mismo34ImportMappings"/>, returning an error message.
        /// </summary>
        /// <param name="mappings">The mappings.</param>
        /// <returns>The error message if there were errors; otherwise, <see langword="null"/>.</returns>
        /// <exception cref="CBaseException"><paramref name="mappings"/> contained unexpected values for <see cref="Mismo3Specification.Version4Schema.FeeBase"/>.</exception>
        private string ValidateMismoImportMappings(IReadOnlyDictionary<Mismo3Specification.Version4Schema.FeeBase, Guid> mappings)
        {
            if (mappings == this.Mismo34ImportMappings)
            {
                return null; // both null or same reference
            }
            else if (mappings == null)
            {
                throw CBaseException.GenericException("Expected non-null mappings to be specified.");
            }

            var unknownMismoTypes = new List<Mismo3Specification.Version4Schema.FeeBase>();
            var invalidFeeTypeMappings = new List<Mismo3Specification.Version4Schema.FeeBase>();
            IReadOnlyDictionary<Mismo3Specification.Version4Schema.FeeBase, Guid> defaultMappings = DefaultSystemClosingCostFee.FeeTemplate.DefaultMismoImportFeeTypeMappings;
            foreach (KeyValuePair<Mismo3Specification.Version4Schema.FeeBase, Guid> mapping in mappings)
            {
                if (!defaultMappings.ContainsKey(mapping.Key))
                {
                    unknownMismoTypes.Add(mapping.Key);
                }
                else if (this.FindFeeByTypeId(mapping.Value) == null)
                {
                    invalidFeeTypeMappings.Add(mapping.Key);
                }
            }

            if (mappings.Count < defaultMappings.Count)
            {
                invalidFeeTypeMappings.AddRange(defaultMappings.Keys.Except(mappings.Keys));
            }

            if (unknownMismoTypes.Any())
            {
                // If we get undefined/unexpected enum values, this is a true developer error, and there's no recovery for the user
                throw CBaseException.GenericException("Unknown values for E_ClosingCostFeeMismoFeeT: " + string.Join(", ", unknownMismoTypes));
            }
            else if (invalidFeeTypeMappings.Any())
            {
                return "Invalid MISMO import mapping for: " + string.Join(", ", invalidFeeTypeMappings.Select(e => Tools.GetFriendlyMismoFeeType(e)));
            }

            return null;
        }

        /// <summary>
        /// A simple storage class for serialization of the data of <see cref="FeeSetupClosingCostSet"/>.
        /// </summary>
        /// <remarks>
        /// This class exists to support extra data beyond the list of fees.
        /// </remarks>
        [System.Runtime.Serialization.DataContract]
        private class FeeSetupClosingCostSetStorage
        {
            /// <summary>
            /// Gets or sets the list of fees to store.
            /// </summary>
            [System.Runtime.Serialization.DataMember]
            public List<FeeSetupClosingCostFee> Fees { get; set; }

            /// <summary>
            /// Gets or sets the MISMO 3.4 import mappings, stored here since they reference the values of <see cref="Fees"/>.
            /// </summary>
            [System.Runtime.Serialization.DataMember]
            public Dictionary<Mismo3Specification.Version4Schema.FeeBase, Guid> Mismo34ImportMappings { get; set; }
        }
    }
}
