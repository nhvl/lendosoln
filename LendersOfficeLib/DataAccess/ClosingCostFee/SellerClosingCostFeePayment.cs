﻿// <copyright file="SellerClosingCostFeePayment.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   7/9/2015
// </summary>
namespace DataAccess
{
    using System;
    using System.Linq;
    using System.Runtime.Serialization;
    using LendersOffice.Common;

    /// <summary>
    /// Class representing a seller responsible payment.
    /// </summary>
    [DataContract]
    public class SellerClosingCostFeePayment : LoanClosingCostFeePayment
    {
        /// <summary>
        /// The payment version number (as an enum).
        /// </summary>
        [DataMember(Name = "version")]
        private SellerClosingCostFeePaymentVersion? version;

        /// <summary>
        /// Initializes a new instance of the <see cref="SellerClosingCostFeePayment" /> class.
        /// </summary>
        public SellerClosingCostFeePayment()
        {
            this.InternalPaidByT = E_ClosingCostFeePaymentPaidByT.Seller;
            this.InternalResponsiblePartyT = E_GfeResponsiblePartyT.Seller;
            this.version = CurrentVerison;
        }

        /// <summary>
        /// Gets or sets the paid by party of this payment. All payments for seller responsible paid by the seller.
        /// </summary>
        /// <value>The paid by party of this payment.</value>
        public override E_ClosingCostFeePaymentPaidByT PaidByT
        {
            get
            {
                if (this.Version == SellerClosingCostFeePaymentVersion.V0 && this.InternalPaidByT != E_ClosingCostFeePaymentPaidByT.Seller)
                {
                    this.InternalPaidByT = E_ClosingCostFeePaymentPaidByT.Seller;
                }

                return this.InternalPaidByT;
            }

            set
            {
                // OPM 473064 - je - Only Seller, Lender, and Broker are valid options.
                if (!value.EqualsOneOf(E_ClosingCostFeePaymentPaidByT.Seller, E_ClosingCostFeePaymentPaidByT.Lender, E_ClosingCostFeePaymentPaidByT.Broker))
                {
                    throw new CBaseException("Seller fee payment \"Paid By\" property must be set to either Seller, Lender, or Broker", "Seller fee payment \"Paid By\" property must be set to either Seller, Lender, or Broker");
                }

                this.InternalPaidByT = value;
            }
        }

        /// <summary>
        /// Gets or sets the type of party responsible for the payment.
        /// </summary>
        /// <value>The type of party responsible for the payment.</value>
        public override E_GfeResponsiblePartyT ResponsiblePartyT
        {
            get
            {
                // OPM 473064 - je - Only Seller, Lender, and Broker are valid options.
                switch (this.PaidByT)
                {
                    case E_ClosingCostFeePaymentPaidByT.Seller:
                        return E_GfeResponsiblePartyT.Seller;
                    case E_ClosingCostFeePaymentPaidByT.Lender:
                        return E_GfeResponsiblePartyT.Lender;
                    case E_ClosingCostFeePaymentPaidByT.Broker:
                        return E_GfeResponsiblePartyT.Broker;
                    default:
                        throw new UnhandledEnumException(this.PaidByT, "Seller fee payment \"Paid By\" property must be set to either Seller, Lender, or Broker");
                }
            }

            set
            {
                // OPM 473064 - je - This does nothing right now.
            }
        }

        /// <summary>
        /// Gets the current payment version number.
        /// </summary>
        /// <remarks>
        /// Initialized using linq, so we don't have manually update latest version each time.
        /// </remarks>
        private static SellerClosingCostFeePaymentVersion CurrentVerison { get; } = Enum.GetValues(typeof(SellerClosingCostFeePaymentVersion)).Cast<SellerClosingCostFeePaymentVersion>().Max();

        /// <summary>
        /// Gets the payment version number (as an enum).
        /// </summary>
        private SellerClosingCostFeePaymentVersion Version => this.version.Value;

        /// <summary>
        /// Gets a copy of the payment object.
        /// </summary>
        /// <returns>A copy of the payment object.</returns>
        public override object Clone()
        {
            SellerClosingCostFeePayment payment = (SellerClosingCostFeePayment)this.MemberwiseClone();
            payment.SetParent(null);
            return payment;
        }

        /// <summary>
        /// Since JSON serialization/deserialization set to private member variables directly which bypass some logic in the getter / setter.
        /// This function is a centralize of the logics on the absolute fact or guarantee of this object.
        /// </summary>
        internal override void EnsureCorrectness()
        {
            if (!this.PaidByT.EqualsOneOf(E_ClosingCostFeePaymentPaidByT.Seller, E_ClosingCostFeePaymentPaidByT.Lender, E_ClosingCostFeePaymentPaidByT.Broker))
            {
                this.PaidByT = E_ClosingCostFeePaymentPaidByT.Seller;
            }

            this.InternalPaidByT = this.PaidByT;
            this.InternalResponsiblePartyT = this.ResponsiblePartyT;
        }

        /// <summary>
        /// Resets the paid by value to the default value.
        /// </summary>
        protected override void ResetInternalPaidByT()
        {
            this.InternalPaidByT = E_ClosingCostFeePaymentPaidByT.Seller;
        }

        /// <summary>
        /// Sets default values on deserialization.
        /// </summary>
        /// <param name="context">The streaming context.</param>
        [OnDeserialized]
        private void SellerPayment_OnDeserialized(StreamingContext context)
        {
            if (!this.version.HasValue)
            {
                this.version = SellerClosingCostFeePaymentVersion.V0;
            }

            this.EnsureCorrectness();
        }

        /// <summary>
        /// Assigns the most recent version on serialization.
        /// </summary>
        /// <param name="context">The streaming context.</param>
        [OnSerializing]
        private void EnsureMostRecentVersion(StreamingContext context)
        {
            this.version = CurrentVerison;
        }
    }
}
