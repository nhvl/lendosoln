﻿// <copyright file="BaseClosingCostSet.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   7/9/2015
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using DataAccess.PathDispatch;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// The absolute base class for all Closing Cost Sets. DO NOT SERIALIZE OR DESERIALIZE INTO THIS CLASS.
    /// </summary>
    public abstract class BaseClosingCostSet : IPathResolvable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BaseClosingCostSet" /> class from json.
        /// </summary>
        /// <param name="json">Json to deserialize.</param>
        public BaseClosingCostSet(string json)
        {
            this.Initialize(json);
        }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="BaseClosingCostSet" /> class from json.
        /// </summary>
        /// <param name="json">The json to deserialize.</param>
        /// <param name="skipInitEnumerable">Whether this set is for updating/clearing another set.</param>
        public BaseClosingCostSet(string json, bool skipInitEnumerable)
        {
            this.SkipInitAndEnumerable = skipInitEnumerable;
            this.Initialize(json);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseClosingCostSet" /> class from json.
        /// </summary>
        protected BaseClosingCostSet()
        { 
        }

        /// <summary>
        /// Gets or sets a value indicating what type of closing cost set this is.
        /// </summary>
        /// <value>A value indicating what type of closing cost set this is.</value>
        public abstract E_sClosingCostSetType SetType
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this set skips creating automatic fees and checks on enumeration.
        /// </summary>
        /// <value>Indicates whether this set skips creating automatic fees and checks on enumeration.</value>
        internal virtual bool SkipInitAndEnumerable
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the underlying closing cost fee list. DO NOT SERIALIZE/DESERIALIZE THIS.
        /// </summary>
        /// <value>The underlying closing cost fee list.</value>
        protected List<BaseClosingCostFee> ClosingCostFeeList
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a fee lookup cache to improve performance.
        /// </summary>
        /// <value>A fee lookup cache to improve performance.</value>
        protected ILookup<int, BaseClosingCostFee> FeeLookupCache
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a dictionary of fees keyed by ID.
        /// </summary>
        /// <value>A dictionary of fees keyed by ID.</value>
        protected ILookup<Guid, BaseClosingCostFee> FeeLookupCache_Id
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a dictionary of fees keyed by type ID.
        /// </summary>
        /// <value>A dictionary of fees keyed by type ID.</value>
        protected ILookup<Guid, BaseClosingCostFee> FeeLookupCache_TypeId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets a list of Fees that pass the given filter. The filter must return true if the fee is to be included.
        /// </summary>
        /// <param name="filter">The filter to check against.</param>
        /// <returns>The list of Fees that pass the filter.</returns>
        public IEnumerable<BaseClosingCostFee> GetFees(Func<BaseClosingCostFee, bool> filter)
        {
            if (filter == null)
            {
                return this.ClosingCostFeeList.Select(f => f);
            }
            else
            {
                List<BaseClosingCostFee> filteredFees = new List<BaseClosingCostFee>();

                foreach (BaseClosingCostFee fee in this.ClosingCostFeeList)
                {
                    if (filter(fee))
                    {
                        filteredFees.Add(fee);
                    }
                }

                return filteredFees;
            }
        }

        /// <summary>
        /// Serialize closing cost fee set to JSON. This converts the list to appropriate fee type before serialization to ensure things go right.
        /// </summary>
        /// <returns>Json representation of closing cost fee list.</returns>
        public abstract string ToJson();

        /// <summary>
        /// Add or update closing cost fee to the set. If fee.Id is Guid.Empty or not existed in the set then fee will get add. Otherwise it will be replace with existing one.
        /// </summary>
        /// <param name="fee">A closing cost fee to be add or update.</param>
        public virtual void AddOrUpdate(BaseClosingCostFee fee)
        {
            if (fee == null)
            {
                throw new ArgumentNullException("fee");
            }

            fee.SetClosingCostSet(this);
            fee.NewFeeIndex = null;

            // All Fees must have a non-empty UniqueId if they are going to be added to a Set.
            if (fee.UniqueId == Guid.Empty)
            {
                fee.UniqueId = Guid.NewGuid();
            }

            // For some reason, the fee has an empty Fee Type Id. This should not happen often. Maybe for adding to FeeSetup Set.
            // If it does, assign it a new Fee Type Id.
            if (fee.ClosingCostFeeTypeId == Guid.Empty)
            {
                this.ClearFeeLookupCache(true);

                fee.ClosingCostFeeTypeId = Guid.NewGuid();
                fee.NewFeeIndex = this.ClosingCostFeeList.Count;
                this.ClosingCostFeeList.Add(fee);
            }
            else
            {
                this.ClearFeeLookupCache(false);

                bool isFound = false;
                for (int i = 0; i < this.ClosingCostFeeList.Count; i++)
                {
                    BaseClosingCostFee o = this.ClosingCostFeeList[i];

                    // Look for a fee existance based on Fee Type Id.
                    if (o.ClosingCostFeeTypeId == fee.ClosingCostFeeTypeId)
                    {
                        this.SetDflp(fee, o);

                        this.ClosingCostFeeList[i] = fee;
                        isFound = true;

                        break;
                    }
                }

                if (isFound == false)
                {
                    fee.NewFeeIndex = this.ClosingCostFeeList.Count;
                    this.ClosingCostFeeList.Add(fee);
                    this.ClearFeeLookupCache(true);
                }
            }

            fee.EnsureCorrectness();
        }

        /// <summary>
        /// Replace the current closing cost set with the new closing cost set.
        /// </summary>
        /// <param name="closingCostSet">Closing cost set to be replace with.</param>
        /// <param name="repSetFilter">The replacement set's filter.</param>
        public virtual void UpdateWith(BaseClosingCostSet closingCostSet, Func<BaseClosingCostFee, bool> repSetFilter)
        {
            this.UpdateWith(closingCostSet, repSetFilter, this.ClosingCostFeeList);
        }

        /// <summary>
        /// Replace the current closing cost set with the fees from the section list.
        /// </summary>
        /// <param name="sectionList">The section list to be replace with.</param>
        public void UpdateWith(IEnumerable<BaseClosingCostFeeSection> sectionList)
        {
            if (sectionList == null)
            {
                // 1/6/2015 dd - Do nothing when empty closing cost set is pass in. Probably error in usage.
                throw new ArgumentNullException("sectionList");
            }

            BaseClosingCostSet set = this.CreateEmptySetForClearing();

            foreach (var feeSection in sectionList)
            {
                foreach (var fee in feeSection.FilteredClosingCostFeeList)
                {
                    fee.SetClosingCostSet(set);
                    set.AddOrUpdate(fee);
                    this.ClearFeeLookupCache();
                }
            }

            this.UpdateWith(set, null);
        }

        /// <summary>
        /// Remove closing cost fee by id from the set.
        /// </summary>
        /// <param name="fee">A closing cost fee to be removed.</param>
        public void Remove(BaseClosingCostFee fee)
        {
            if (fee == null)
            {
                throw new ArgumentNullException("fee");
            }

            foreach (var o in this.ClosingCostFeeList)
            {
                if (o.ClosingCostFeeTypeId == fee.ClosingCostFeeTypeId)
                {
                    this.ClosingCostFeeList.Remove(o);
                    this.ClearFeeLookupCache();
                    return; // 11/5/2014 dd - Assume there is only one unique id.
                }
            }
        }

        /// <summary>
        /// Gets the first fee in the list the match the LendingQB legacy field type.
        /// </summary>
        /// <param name="type">LendingQB legacy field type.</param>
        /// <param name="brokerId">Broker Id to find lender settings when could not find in list.</param>
        /// <param name="addFeeToSet">Determines whether the fee should be added if it does not exist.</param>
        /// <returns>The first fee with legacy field type.</returns>
        public abstract BaseClosingCostFee GetFirstFeeByLegacyType(E_LegacyGfeFieldT type, Guid brokerId, bool addFeeToSet);

        /// <summary>
        /// Get a list of appropriate fee sections by view type. DO NOT USE THIS FOR SERIALIZATION/DESERIALIZATION.
        /// </summary>
        /// <param name="viewType">The view type. Warning, certain closing cost sets only accept certain view types.</param>
        /// <returns>A list of fee BaseClosingCostFeeSections.</returns>
        public abstract IEnumerable<BaseClosingCostFeeSection> GetViewBase(E_ClosingCostViewT viewType);

        /// <summary>
        /// Get the section by name.
        /// </summary>
        /// <param name="viewType">View Type.</param>
        /// <param name="sectionName">Name of the section to search.</param>
        /// <returns>A closing cost fee section object. Return null if not found.</returns>
        public BaseClosingCostFeeSection GetSection(E_ClosingCostViewT viewType, string sectionName)
        {
            foreach (var section in this.GetViewBase(viewType))
            {
                if (section.SectionName.Equals(sectionName, StringComparison.OrdinalIgnoreCase))
                {
                    return section;
                }
            }

            return null;
        }

        /// <summary>
        /// Get the section by section type.
        /// </summary>
        /// <param name="viewType">The view type.</param>
        /// <param name="sectionType">The section type.</param>
        /// <returns>The closing cost fee section with the matching section type, or null if no such exists.</returns>
        public BaseClosingCostFeeSection GetSection(E_ClosingCostViewT viewType, E_IntegratedDisclosureSectionT sectionType)
        {
            try
            {
                return this.GetViewBase(viewType).FirstOrDefault(s => s.SectionType == sectionType);
            }
            catch (ArgumentNullException)
            {
                return null;
            }
        }

        /// <summary>
        /// Gets all the fees that match the given HUD section.
        /// </summary>
        /// <param name="hudSection">The HUD line number.</param>
        /// <returns>Matching fees.</returns>
        public IEnumerable<BaseClosingCostFee> FindFeeByHudline(int hudSection)
        {
            if (this.FeeLookupCache == null)
            {
                this.FeeLookupCache = this.ClosingCostFeeList.ToLookup(p => p.HudLine);
            }

            return this.FeeLookupCache[hudSection];
        }

        /// <summary>
        /// Clears ALL fees. BE CAREFUL WITH THIS.
        /// </summary>
        public void ClearFeeList()
        {
            this.ClosingCostFeeList.Clear();
        }

        /// <summary>
        /// Gets the fee with the given type ID.
        /// </summary>
        /// <param name="typeId">The fee type ID.</param>
        /// <returns>First fee that matches fee or null if no fees match.</returns>
        public BaseClosingCostFee FindFeeByTypeId(Guid typeId)
        {
            if (this.FeeLookupCache_TypeId == null)
            {
                this.FeeLookupCache_TypeId = this.ClosingCostFeeList.ToLookup(f => f.ClosingCostFeeTypeId);
            }

            if (!this.FeeLookupCache_TypeId.Contains(typeId))
            {
                return null;
            }

            IEnumerable<BaseClosingCostFee> matchingFees = this.FeeLookupCache_TypeId[typeId];
            if (matchingFees.Count() > 1)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("FindFeeByTypeId found more than 1 fee with the same Type Id. Returning first.");

                foreach (BaseClosingCostFee match in matchingFees)
                {
                    sb.AppendLine("\tFee Description: " + match.Description + "\tFee Type Id: " + match.ClosingCostFeeTypeId);
                }

                Tools.LogWarning(sb.ToString());
            }

            return matchingFees.First();
        }
        
        /// <summary>
        /// Validates the new closing cost set.
        /// </summary>
        /// <param name="newSet">The new Set to validate.</param>
        /// <param name="oldSetFilter">The filter for the old set.</param>
        /// <param name="message">Error messages if any.</param>
        /// <param name="args">The information needed to validate the new fees.</param>
        /// <returns>True if the set has valid modifications, false otherwise.</returns>
        public bool ValidateNewSet(BaseClosingCostSet newSet, Func<BaseClosingCostFee, bool> oldSetFilter, out string message, ValidationParamContainer args)
        {
            HashSet<Guid> feesInBothSets = new HashSet<Guid>();
            foreach (BaseClosingCostFee oldFee in this.GetFees(oldSetFilter))
            {
                BaseClosingCostFee matchingNewFee = newSet.FindFeeByTypeId(oldFee.ClosingCostFeeTypeId);
                if (matchingNewFee == null)
                {
                    // No matching new fee. This means that the fee was removed from the old set. Check if we can remove it.
                    if (!oldFee.ValidateRemoveFee(out message))
                    {
                        return false;
                    }
                }
                else
                {
                    // The fee has been modified. Validate it.
                    // Also keep track of it so we can tell what fees are new.
                    feesInBothSets.Add(oldFee.ClosingCostFeeTypeId);

                    if (!oldFee.ValidateFeeModifcation(matchingNewFee, out message, args))
                    {
                        return false;
                    }
                }
            }

            foreach (BaseClosingCostFee newFee in newSet.GetFees(oldSetFilter))
            {
                if (!feesInBothSets.Contains(newFee.ClosingCostFeeTypeId))
                {
                    if (!BaseClosingCostFee.ValidateGFESection(newFee, out message))
                    {
                        return false;
                    }
                }
            }

            message = "Set has been validated.";
            return true;
        }

        /// <summary>
        /// Returns the value specified by the element.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns>The value specified by the element.</returns>
        public object GetElement(IDataPathElement element)
        {
            if (!(element is DataPathSelectionElement))
            {
                throw new ArgumentException("BorrowerClosingCostSet can only handle DataPathSelectionElements, but passed element of type " + element.GetType());
            }

            var selectorElement = (DataPathSelectionElement)element;

            Guid closingCostTypeId;

            if (Guid.TryParse(selectorElement.Name, out closingCostTypeId))
            {
                var fee = this.FindFeeByTypeId(closingCostTypeId);

                if (fee == null)
                {
                    throw new ArgumentException("Could not find fee of type ID " + closingCostTypeId);
                }

                return fee;
            }
            else
            {
                throw new ArgumentException("Could not parse closing cost fee argument to a valid GUID");
            }
        }

        /// <summary>
        /// Clear the Fee Lookup Cache so that it can be recreated on the next call to FindFeeByHudline. Clears everything.
        /// </summary>
        internal void ClearFeeLookupCache()
        {
            this.ClearFeeLookupCache(true);
        }

        /// <summary>
        /// Clear the Fee Lookup Cache so that it can be recreated on the next call to FindFeeByHudline.
        /// </summary>
        /// <param name="clearIdLookups">Whether or not to clear the id based lookups.</param>
        internal void ClearFeeLookupCache(bool clearIdLookups)
        {
            this.FeeLookupCache = null;

            if (clearIdLookups)
            {
                this.FeeLookupCache_Id = null;
                this.FeeLookupCache_TypeId = null;
            }
        }

        /// <summary>
        /// Replace the current closing cost set with the new closing cost set.
        /// </summary>
        /// <param name="closingCostSet">The replacement set.</param>
        /// <param name="repSetfilter">The replacement set filter.</param>
        /// <param name="existingFees">The fees to consider for removal. Should be the list of fees that show up on the UI.</param>
        protected void UpdateWith(BaseClosingCostSet closingCostSet, Func<BaseClosingCostFee, bool> repSetfilter, IEnumerable<BaseClosingCostFee> existingFees)
        {
            if (closingCostSet == null)
            {
                // 1/6/2015 dd - Do nothing when empty closing cost set is pass in. Probably error in usage.
                throw new ArgumentNullException("Parameter closingCostSet is null");
            }

            HashSet<Guid> existingIds = new HashSet<Guid>();

            foreach (var fee in existingFees)
            {
                existingIds.Add(fee.ClosingCostFeeTypeId);
            }

            foreach (var fee in closingCostSet.GetFees(repSetfilter))
            {
                this.AddOrUpdate(fee);
                if (existingIds.Contains(fee.ClosingCostFeeTypeId))
                {
                    existingIds.Remove(fee.ClosingCostFeeTypeId);
                }
            }

            // 1/6/2015 dd - Delete fees that do not show up in the new closing cost set.
            for (int i = this.ClosingCostFeeList.Count - 1; i >= 0; i--)
            {
                var fee = this.ClosingCostFeeList[i];
                if (existingIds.Contains(fee.ClosingCostFeeTypeId))
                {
                    this.ClosingCostFeeList.RemoveAt(i);
                    this.ClearFeeLookupCache();
                }
            }
        }

        /// <summary>
        /// Sets the Dflp property when adding a fee to the list. Only really used for LoanClosingCostFees.
        /// </summary>
        /// <param name="fee">The fee to modify.</param>
        /// <param name="sourceFee">The source fee to copy from.</param>
        protected virtual void SetDflp(BaseClosingCostFee fee, BaseClosingCostFee sourceFee)
        {
            // Do nothing.
            return;
        }

        /// <summary>
        /// Creates a new empty closing cost set.
        /// </summary>
        /// <returns>The empty closing cost set.</returns>
        protected abstract BaseClosingCostSet CreateEmptySet();

        /// <summary>
        /// Creates a new empty closing cost set that does not automatically create fees and enumerates through ALL fees.
        /// </summary>
        /// <returns>The empty set.</returns>
        protected abstract BaseClosingCostSet CreateEmptySetForClearing();

        /// <summary>
        /// Gets the custom fee by legacy gfe field type.
        /// </summary>
        /// <param name="type">The legacy gfe field.</param>
        /// <param name="brokerId">The broker id for pulling from broker fee setup.</param>
        /// <param name="addFeeToSet">Whether to add to the set if it doesn't exist already.</param>
        /// <returns>The custom fee or null if it is not found/made.</returns>
        protected BaseClosingCostFee CheckForCustomFee(E_LegacyGfeFieldT type, Guid brokerId, bool addFeeToSet)
        {
            if (type == E_LegacyGfeFieldT.s800U1F)
            {
                return this.GetCustomFee(813, DefaultSystemClosingCostFee.Hud800Custom1FeeTypeId, addFeeToSet);
            }
            else if (type == E_LegacyGfeFieldT.s800U2F)
            {
                return this.GetCustomFee(814, DefaultSystemClosingCostFee.Hud800Custom2FeeTypeId, addFeeToSet);
            }
            else if (type == E_LegacyGfeFieldT.s800U3F)
            {
                return this.GetCustomFee(815, DefaultSystemClosingCostFee.Hud800Custom3FeeTypeId, addFeeToSet);
            }
            else if (type == E_LegacyGfeFieldT.s800U4F)
            {
                return this.GetCustomFee(816, DefaultSystemClosingCostFee.Hud800Custom4FeeTypeId, addFeeToSet);
            }
            else if (type == E_LegacyGfeFieldT.s800U5F)
            {
                return this.GetCustomFee(817, DefaultSystemClosingCostFee.Hud800Custom5FeeTypeId, addFeeToSet);
            }
            else if (type == E_LegacyGfeFieldT.s904Pia)
            {
                return this.GetCustomFee(904, DefaultSystemClosingCostFee.Hud900Custom1FeeTypeId, addFeeToSet);
            }
            else if (type == E_LegacyGfeFieldT.s900U1Pia)
            {
                return this.GetCustomFee(906, DefaultSystemClosingCostFee.Hud900Custom2FeeTypeId, addFeeToSet);
            }
            else if (type == E_LegacyGfeFieldT.sU1Tc)
            {
                return this.GetCustomFee(1112, DefaultSystemClosingCostFee.Hud1100Custom1FeeTypeId, addFeeToSet);
            }
            else if (type == E_LegacyGfeFieldT.sU2Tc)
            {
                return this.GetCustomFee(1113, DefaultSystemClosingCostFee.Hud1100Custom2FeeTypeId, addFeeToSet);
            }
            else if (type == E_LegacyGfeFieldT.sU3Tc)
            {
                return this.GetCustomFee(1114, DefaultSystemClosingCostFee.Hud1100Custom3FeeTypeId, addFeeToSet);
            }
            else if (type == E_LegacyGfeFieldT.sU4Tc)
            {
                return this.GetCustomFee(1115, DefaultSystemClosingCostFee.Hud1100Custom4FeeTypeId, addFeeToSet);
            }
            else if (type == E_LegacyGfeFieldT.sU1GovRtc)
            {
                return this.GetCustomFee(1206, DefaultSystemClosingCostFee.Hud1200Custom1FeeTypeId, addFeeToSet);
            }
            else if (type == E_LegacyGfeFieldT.sU2GovRtc)
            {
                return this.GetCustomFee(1207, DefaultSystemClosingCostFee.Hud1200Custom2FeeTypeId, addFeeToSet);
            }
            else if (type == E_LegacyGfeFieldT.sU3GovRtc)
            {
                return this.GetCustomFee(1208, DefaultSystemClosingCostFee.Hud1200Custom3FeeTypeId, addFeeToSet);
            }
            else if (type == E_LegacyGfeFieldT.sU1Sc)
            {
                return this.GetCustomFee(1303, DefaultSystemClosingCostFee.Hud1300Custom1FeeTypeId, addFeeToSet);
            }
            else if (type == E_LegacyGfeFieldT.sU2Sc)
            {
                return this.GetCustomFee(1304, DefaultSystemClosingCostFee.Hud1300Custom2FeeTypeId, addFeeToSet);
            }
            else if (type == E_LegacyGfeFieldT.sU3Sc)
            {
                return this.GetCustomFee(1305, DefaultSystemClosingCostFee.Hud1300Custom3FeeTypeId, addFeeToSet);
            }
            else if (type == E_LegacyGfeFieldT.sU4Sc)
            {
                return this.GetCustomFee(1306, DefaultSystemClosingCostFee.Hud1300Custom4FeeTypeId, addFeeToSet);
            }
            else if (type == E_LegacyGfeFieldT.sU5Sc)
            {
                return this.GetCustomFee(1307, DefaultSystemClosingCostFee.Hud1300Custom5FeeTypeId, addFeeToSet);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Deserialize the closing cost fee list. The JSON should get deserialized into the EXACT child class or things could go bad.
        /// </summary>
        /// <param name="json">Json content to deserialize.</param>
        protected abstract void Initialize(string json);

        /// <summary>
        /// Gets the first custom fee in the set.
        /// </summary>
        /// <param name="hudSection">The section to look at.</param>
        /// <param name="closingCostFeeTypeId">The fee's closingCostFeeTypeId.</param>
        /// <param name="addFee">Whether the fee should be added to the set. Not used for this particular implementation.</param>
        /// <returns>The fee if found, null if not found.</returns>
        protected virtual BaseClosingCostFee GetCustomFee(int hudSection, Guid closingCostFeeTypeId, bool addFee)
        {
            IEnumerable<BaseClosingCostFee> matchingFees = this.FindFeeByHudline(hudSection);
            int count = matchingFees.Count();

            if (count > 0)
            {
                if (count > 1)
                {
                    Tools.LogError("GetCustomFee found 2 matching fees. Don't think this should happen " + hudSection + " type " + closingCostFeeTypeId);
                }

                return matchingFees.First();
            }

            // If no fee found in fee setup, then return null;
            return null;
        }
    }
}
