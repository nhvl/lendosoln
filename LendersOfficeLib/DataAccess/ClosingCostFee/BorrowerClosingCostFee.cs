﻿// <copyright file="BorrowerClosingCostFee.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   7/21/2015
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using LendersOffice.Migration;

    /// <summary>
    /// Class representing a borrower responsible closing cost fee.
    /// </summary>
    [DataContract]
    public class BorrowerClosingCostFee : LoanClosingCostFee
    {
        /// <summary>
        /// Gets or sets a value indicating whether the originator compensation fee should skip the TRID check for it's amount.
        /// </summary>
        /// <value>Whether the originator compensation fee should skip the TRID check for it's amount.</value>
        public bool OrigCompSkipTRIDCheck
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the provider who provided the service and will ultimately receive the payment for it.
        /// </summary>
        /// <value>The provide who provided the service and will ultimately receive the payment for it.</value>
        public override E_AgentRoleT Beneficiary
        {
            get
            {
                if (this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId)
                {
                    if (this.ParentClosingCostSet != null && this.ParentClosingCostSet.HasDataLoanAssociate)
                    {
                        if (this.ParentClosingCostSet.DataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID &&
                            this.ParentClosingCostSet.DataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)
                        {
                            return E_AgentRoleT.Broker;
                        }
                    }
                }

                return base.Beneficiary;
            }

            set
            {
                base.Beneficiary = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not this fee is to pay for title services.
        /// </summary>
        /// <value>Whether this fee is to pay for title services.</value>
        public override bool IsTitleFee
        {
            get
            {
                if (this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId)
                {
                    if (this.ParentClosingCostSet != null && this.ParentClosingCostSet.HasDataLoanAssociate &&
                        this.ParentClosingCostSet.DataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID &&
                        this.ParentClosingCostSet.DataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)
                    {
                        return false;
                    }
                }

                return base.IsTitleFee;
            }

            set
            {
                base.IsTitleFee = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether it is a third party.
        /// </summary>
        /// <value>Whether is is a third party.</value>
        public override bool IsThirdParty
        {
            get
            {
                if (this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId)
                {
                    if (this.ParentClosingCostSet != null && this.ParentClosingCostSet.HasDataLoanAssociate &&
                        this.ParentClosingCostSet.DataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID &&
                        this.ParentClosingCostSet.DataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)
                    {
                        return false;
                    }
                }

                return base.IsThirdParty;
            }

            set
            {
                base.IsThirdParty = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a borrower can shop for provider.
        /// </summary>
        /// <value>Whether a borrower can shop for provider.</value>
        public override bool CanShop
        {
            get
            {
                if (this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId)
                {
                    if (this.ParentClosingCostSet != null && this.ParentClosingCostSet.HasDataLoanAssociate &&
                        this.ParentClosingCostSet.DataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID &&
                        this.ParentClosingCostSet.DataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)
                    {
                        return false;
                    }
                }

                return base.CanShop;
            }

            set
            {
                base.CanShop = value;
            }
        }

        /// <summary>
        /// Gets or sets the line number on the HUD-1 that the fee appears on.
        /// Multiples of 100 indicate that the fee should appear in the corresponding section but do not specify a particular line.
        /// </summary>
        /// <value>The line number on the HUD-1 that the fee appears on.</value>
        public override int HudLine
        {
            get
            {
                if (this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000WindStormInsuranceFeeTypeId ||
                    this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000CondoHo6InsuranceFeeTypeId ||
                    this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000HomeOwnerAssociationDuesFeeTypeId ||
                    this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000GroundRentFeeTypeId ||
                    this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000OtherTax1FeeTypeId ||
                    this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000OtherTax2FeeTypeId ||
                    this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000OtherTax3FeeTypeId ||
                    this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000OtherTax4FeeTypeId)
                {
                    string lineNum = this.GetHudlineFromExpenseOrFeeSetup(this.ClosingCostFeeTypeId);

                    // Not null, thus a hudline was obtained from the expenses or the fee setup. Assign this as the latest hudline.
                    if (!string.IsNullOrEmpty(lineNum))
                    {
                        if (this.InternalHudline != lineNum)
                        {
                            this.InternalHudline = lineNum;
                            this.ClearParentSetLookupCache();
                        }
                    }

                    return ClosingCostSetUtils.SafeInt(this.InternalHudline); 
                }
                else
                {
                    return base.HudLine;
                }
            }

            set
            {
                base.HudLine = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the QM warning flag should be shown for this payment on QM Point and Fees page.
        /// </summary>
        /// <value>Whether the QM warning flag should be displayed on QM pages.</value>
        [DataMember(Name = "qm_warning")]
        public bool IsShowQmWarning 
        { 
            get; 
            set; 
        }

        /// <summary>
        /// Gets or sets who is choosing the provider for a particular service.
        /// </summary>
        /// <value>Who is choosing the provider for a particular service.</value>
        [DataMember(Name = "prov_choice")]
        public E_GfeProviderChoiceT GfeProviderChoiceT
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the provider chosen by the lender when the borrower can shop for the service.
        /// The provider must be set up in the official contact list.
        /// </summary>
        /// <value>The provider chosen by the lender when the borrower can shop for the service.</value>
        [DataMember(Name = "prov")]
        public E_AgentRoleT ProviderChosenByLender 
        { 
            get;
            set;
        }

        /// <summary>
        /// Gets a value indicating whether this fee is one of the special ones that pulls base value directly from loan.
        /// </summary>
        /// <value>True if this field pulls from loan file as a base value.</value>
        public bool IsConstantBaseAmountFromFieldId
        {
            get
            {
                BorrowerClosingCostFormula closingCostFormula = (BorrowerClosingCostFormula)this.GetFormula();
                return closingCostFormula.IsConstantBaseAmountFromFieldId;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this fee should be included in the Total QM Fee Amount calculation.
        /// </summary>
        /// <value>Whether this fee should be included in the Total QM Fee Amount calculation.</value>
        /// <remarks>
        /// Closing Cost Fee is included in the QM calculations if the fee is APR and the Fee is not paid to a
        /// Third Party, or if the Fee is paid to a Third Party Affiliate (i.e. any payment made to the lender
        /// or its affiliates). NOTE: Some fees require isAPR == true to be included in QM.
        /// </remarks>
        [DataMember(Name = "is_qm")]
        public bool IsIncludedInQm
        {
            get
            {
                // Exclude certain legacy fees (see spec for OPM 143132)
                if (this.LegacyGfeFieldT == E_LegacyGfeFieldT.sIPia || this.LegacyGfeFieldT == E_LegacyGfeFieldT.sMipPia || 
                    this.LegacyGfeFieldT == E_LegacyGfeFieldT.sUpfrontMipPia || this.LegacyGfeFieldT == E_LegacyGfeFieldT.sVaFf || 
                    this.LegacyGfeFieldT == E_LegacyGfeFieldT.sLDiscnt ||
                    this.LegacyGfeFieldT == E_LegacyGfeFieldT.sGfeOriginatorCompF ||
                    this.LegacyGfeFieldT == E_LegacyGfeFieldT.sGfeOriginatorCompF_Manual ||
                    this.LegacyGfeFieldT == E_LegacyGfeFieldT.sGfeOriginatorComp_BorrowerPaid)
                {
                    return false;
                }

                // Per spec for OPM 143132 and OPM 203073
                if (this.IsApr)
                {
                    if (this.IsThirdParty == false || this.IsAffiliate)
                    {
                        return true;
                    }
                }
                else if (this.QmRequiresApr == false)
                {
                    if (this.IsThirdParty && this.IsAffiliate)
                    {
                        return true;
                    }
                }

                return false;
            }

            private set
            {
                // 2/17/2015 je - This is required so we can exposed through JSON serialization.
            }
        }

        /// <summary>
        /// Gets the borrower paid or financed portion of the fee total amount that is applicable toward calculating QM Status.
        /// </summary>
        /// <value>Amount to be included in QM Calculation.</value>
        public decimal QmAmount
        {
            get 
            { 
                return this.GetQmAmount(false); 
            }
        }

        /// <summary>
        /// Gets the borrower paid or financed portion of the fee total amount that is applicable toward calculating QM Status.
        /// </summary>
        /// <value>Amount to be included in QM Calculation.</value>
        [DataMember(Name = "qm_amount")]
        public string QmAmount_rep
        {
            get 
            { 
                return this.LosConvert.ToMoneyString(this.QmAmount, FormatDirection.ToRep); 
            }
            
            private set 
            { 
            }
        }

        /// <summary>
        /// Gets the borrower financed portion of the fee total amount that is applicable toward calculating QM Status.
        /// </summary>
        /// <value>Fee QM Amount financed by borrower.</value>
        public decimal FinancedQmAmount
        {
            get 
            { 
                return this.GetQmAmount(true); 
            }
        }

        /// <summary>
        /// Gets or sets the legacy LendingQB GFE closing cost dollar amount field that corresponds to the fee. Does some calculation for Orig Comp Fee.
        /// </summary>
        /// <value>The legacy LendingQB GFE closing cost dollar amount field that corresponds to the fee.</value>
        public override E_LegacyGfeFieldT LegacyGfeFieldT
        {
            get
            {
                if (this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId)
                {
                    if (this.ParentClosingCostSet != null && this.ParentClosingCostSet.HasDataLoanAssociate)
                    {
                        E_sOriginatorCompensationPaymentSourceT originatorCompensationPaymentSourceT;
                        E_sDisclosureRegulationT disclosureRegulationT;
                        bool gfeIsTPOTransaction;
                        E_BranchChannelT branchChannelT;

                        if (this.ParentClosingCostSet.HasDataLoanAssociate)
                        {
                            originatorCompensationPaymentSourceT = this.ParentClosingCostSet.DataLoan.sOriginatorCompensationPaymentSourceT;
                            disclosureRegulationT = this.ParentClosingCostSet.DataLoan.sDisclosureRegulationT;
                            gfeIsTPOTransaction = this.ParentClosingCostSet.DataLoan.sGfeIsTPOTransaction;
                            branchChannelT = this.ParentClosingCostSet.DataLoan.sBranchChannelT;
                        }
                        else if (this.ParentClosingCostSet.HasClosingCostArchiveAssociate)
                        {
                            originatorCompensationPaymentSourceT = (E_sOriginatorCompensationPaymentSourceT)this.ParentClosingCostSet.ClosingCostArchive.GetCountValue("sOriginatorCompensationPaymentSourceT");
                            disclosureRegulationT = (E_sDisclosureRegulationT)this.ParentClosingCostSet.ClosingCostArchive.GetCountValue("sDisclosureRegulationT");
                            gfeIsTPOTransaction = this.ParentClosingCostSet.ClosingCostArchive.GetBitValue("sGfeIsTPOTransaction");
                            branchChannelT = (E_BranchChannelT)this.ParentClosingCostSet.ClosingCostArchive.GetCountValue("sBranchChannelT");
                        }
                        else
                        {
                            return base.LegacyGfeFieldT;
                        }

                        if (originatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid)
                        {
                            this.InternalLegacyGfeFieldT = E_LegacyGfeFieldT.sGfeOriginatorComp_BorrowerPaid;
                        }
                        else if (originatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)
                        {
                            bool skipTridCheck = false;
                            if (this.OrigCompSkipTRIDCheck)
                            {
                                skipTridCheck = true;
                            }
                            
                            if (disclosureRegulationT != E_sDisclosureRegulationT.TRID || skipTridCheck == true)
                            {
                                if (gfeIsTPOTransaction)
                                {
                                    if (branchChannelT == E_BranchChannelT.Broker ||
                                        branchChannelT == E_BranchChannelT.Correspondent)
                                    {
                                        this.InternalLegacyGfeFieldT = E_LegacyGfeFieldT.sGfeOriginatorCompF_Manual;
                                    }
                                    else
                                    {
                                        this.InternalLegacyGfeFieldT = E_LegacyGfeFieldT.sGfeOriginatorCompF;
                                    }
                                }
                                else
                                {
                                    this.InternalLegacyGfeFieldT = E_LegacyGfeFieldT.sGfeOriginatorCompF;
                                }
                            }
                        }
                    }
                }

                return base.LegacyGfeFieldT;
            }

            set
            {
                base.LegacyGfeFieldT = value;
            }
        }

        /// <summary>
        /// Gets the borrower financed portion of the fee total amount that is applicable toward calculating QM Status.
        /// </summary>
        /// <value>Fee QM Amount financed by borrower.</value>
        public string FinancedQmAmount_rep
        {
            get 
            { 
                return this.LosConvert.ToMoneyString(this.FinancedQmAmount, FormatDirection.ToRep); 
            }
        }

        /// <summary>
        /// Gets a value indicating whether this fee requires isAPR == true in order to be included in QM.
        /// </summary>
        /// <value>Whether this fee requires isAPR == true in order to be included in QM.</value>
        private bool QmRequiresApr
        {
            get
            {
                // Per Spec for OPM 143132:

                // All fees in section 1100 and 1300 should not require APR.
                int hudSection = this.HudLine / 100;
                if (hudSection == 11 || hudSection == 13)
                {
                    return false;
                }

                // All non-tax items in section 1000 should not require APR.
                // NOTE: We should be able to assume that any fees in section 1000 paid to an affiliate are not taxes.
                // So, QM Amount for taxes should be $0.00 unless client mistakenly marks AFF checkbox on tax item (client error).
                if (hudSection >= 10 /*TODO: && NOT A TAX*/)
                {
                    return false;
                }

                // Specific Legacy fees should not require APR.
                if (this.LegacyGfeFieldT == E_LegacyGfeFieldT.sApprF || this.LegacyGfeFieldT == E_LegacyGfeFieldT.sCrF || 
                    this.LegacyGfeFieldT == E_LegacyGfeFieldT.sFloodCertificationF)
                {
                    return false;
                }

                // OPM 244350, 5/31/2016, ML
                if (ClosingCostSetUtils.MismoFeeTypesNotRequiringApr.Contains(this.MismoFeeT))
                {
                    return false;
                }

                if (this.IsTitleFee)
                {
                    return false;
                }

                // TODO: DEFINE RULES FOR NEW SECTIONS (A,B,C,etc.)
                // Per Doug: For now, just continue to use the existing sections. There is not a clear mapping
                // between the requirements in the regulation and the new sections, so we will need to rely on
                // the HUD-1 sections until we have time to develop a clearly better solution. (OPM 198065)

                // All other fees require APR.
                return true;
            }
        }

        /// <summary>
        /// Gets or sets the formula that will be use to compute the total amount. This ensures proper serialization.
        /// </summary>
        /// <value>The formula that will be use to compute the total amount.</value>
        [DataMember(Name = "f")]
        private BorrowerClosingCostFormula Formula
        {
            get
            {
                return (BorrowerClosingCostFormula)this.ChildLoanFormula;
            }

            set
            {
                this.ChildLoanFormula = value;
            }
        }

        /// <summary>
        /// Gets or sets a list of payments associate with the closing cost. This ensures proper serialization. The getter will be a different list than in the base class.
        /// Direct adds/removes from this list will not be reflected in the payment list stored in the base class. So use ChildPaymentList for that.
        /// </summary>
        /// <value>A list of all the payments associate with the closing cost.</value>
        [DataMember(Name = "pmts")]
        private List<BorrowerClosingCostFeePayment> PaymentList
        {
            get
            {
                List<BorrowerClosingCostFeePayment> payments = new List<BorrowerClosingCostFeePayment>();

                foreach (BorrowerClosingCostFeePayment payment in this.ChildPaymentList)
                {
                    payments.Add(payment);
                }

                return payments;
            }

            set
            {
                if (this.ChildPaymentList == null)
                {
                    this.ChildPaymentList = new List<LoanClosingCostFeePayment>();
                }

                if (value != null)
                {
                    foreach (BorrowerClosingCostFeePayment payment in value)
                    {
                        this.ChildPaymentList.Add(payment);
                    }
                }
                else
                {
                    this.ChildPaymentList = new List<LoanClosingCostFeePayment>();
                }
            }
        }

        /// <summary>
        /// Gets the total fee amount included in the APR.
        /// </summary>
        /// <param name="loanVersion">The loan version.</param>
        /// <returns>The total fee amount included in the APR.</returns>
        public decimal AprAmt(LoanVersionT loanVersion)
        {
            var aprPaymentAmounts = new List<decimal>();
            if (this.IsApr)
            {
                foreach (LoanClosingCostFeePayment payment in this.Payments)
                {
                    if (LoanClosingCostFeePayment.AprFeePaymentIsAprPredicate(payment, loanVersion))
                    {
                        aprPaymentAmounts.Add(payment.Amount);
                    }
                }
            }

            return Tools.SumMoney(aprPaymentAmounts);
        }

        /// <summary>
        /// Converts this BorrowerClosingCostFee to a SellerClosingCostFee. Hopefully temporary.
        /// </summary>
        /// <returns>The new SellerClosingCostFee.</returns>
        public SellerClosingCostFee ConvertToSellerFee()
        {
            SellerClosingCostFee convertedFee = new SellerClosingCostFee();

            convertedFee.BaseAmount = this.BaseAmount;
            convertedFee.Beneficiary = this.Beneficiary;
            convertedFee.BeneficiaryDescription = this.BeneficiaryDescription;
            convertedFee.BeneficiaryAgentId = this.BeneficiaryAgentId;
            convertedFee.CanShop = this.CanShop;
            convertedFee.ClosingCostFeeTypeId = this.ClosingCostFeeTypeId;
            convertedFee.Description = this.Description;
            convertedFee.Dflp = this.Dflp;
            convertedFee.DidShop = this.DidShop;
            convertedFee.DisableBeneficiaryAutomation = this.DisableBeneficiaryAutomation;
            convertedFee.FormulaT = this.FormulaT;
            convertedFee.GfeResponsiblePartyT = this.GfeResponsiblePartyT;
            convertedFee.GfeSectionT = this.GfeSectionT;
            convertedFee.HudLine = this.HudLine;
            convertedFee.IntegratedDisclosureSectionT = this.IntegratedDisclosureSectionT;
            convertedFee.IsAffiliate = this.IsAffiliate;
            convertedFee.IsApr = this.IsApr;
            convertedFee.IsBonaFide = this.IsBonaFide;
            convertedFee.IsFhaAllowable = this.IsFhaAllowable;
            convertedFee.IsOptional = this.IsOptional;
            convertedFee.IsThirdParty = this.IsThirdParty;
            convertedFee.IsTitleFee = this.IsTitleFee;
            convertedFee.IsVaAllowable = this.IsVaAllowable;
            convertedFee.LegacyGfeFieldT = this.LegacyGfeFieldT;
            convertedFee.MismoFeeT = this.MismoFeeT;
            convertedFee.NewFeeIndex = this.NewFeeIndex;
            convertedFee.NumberOfPeriods = this.NumberOfPeriods;
            convertedFee.OriginalDescription = this.OriginalDescription;
            convertedFee.Percent = this.Percent;
            convertedFee.PercentBaseT = this.PercentBaseT;
            convertedFee.PercentTotalAmount = this.PercentTotalAmount;

            convertedFee.ClearPayments();
            foreach (BorrowerClosingCostFeePayment payment in this.Payments)
            {
                SellerClosingCostFeePayment newPayment = new SellerClosingCostFeePayment();
                newPayment.Amount = payment.Amount;
                newPayment.Entity = payment.Entity;
                newPayment.GfeClosingCostFeePaymentTimingT = payment.GfeClosingCostFeePaymentTimingT;
                newPayment.Id = payment.Id;
                newPayment.IsMade = payment.IsMade;
                newPayment.PaidByT = payment.PaidByT;
                newPayment.PaymentDate = payment.PaymentDate;
                newPayment.IsSystemGenerated = payment.IsSystemGenerated;
                newPayment.ResponsiblePartyT = payment.ResponsiblePartyT;
                newPayment.SetParent(convertedFee);
                convertedFee.ChildPaymentList.Add(newPayment);
            }

            return convertedFee;
        }

        /// <summary>
        /// Clones the borrower closing cost fee.
        /// </summary>
        /// <returns>The clone of this borrower closing cost fee.</returns>
        public override object Clone()
        {
            BorrowerClosingCostFormula formular = (BorrowerClosingCostFormula)this.Formula.Clone();
            BorrowerClosingCostFee clonedFee = (BorrowerClosingCostFee)this.MemberwiseClone();
            clonedFee.UniqueId = Guid.NewGuid();
            clonedFee.ParentClosingCostSet = null;
            clonedFee.ChildPaymentList = new List<LoanClosingCostFeePayment>();
            clonedFee.Formula = formular;
            clonedFee.Formula.SetClosingCostFee(clonedFee);

            foreach (BorrowerClosingCostFeePayment payment in this.ChildPaymentList)
            {
                BorrowerClosingCostFeePayment clonedP = (BorrowerClosingCostFeePayment)payment.Clone();
                clonedP.SetParent(clonedFee);
                clonedFee.ChildPaymentList.Add(clonedP);
            }

            return clonedFee;
        }

        /// <summary>
        /// Validates the Borrower Fee for read-only changes. 
        /// </summary>
        /// <param name="newBaseFee">The new Fee to check.</param>
        /// <param name="message">The error message, if any.</param>
        /// <param name="args">The info needed to validate this fee.</param>
        /// <returns>True if the new Fee is valid, false otherwise.</returns>
        public override bool ValidateFeeModifcation(BaseClosingCostFee newBaseFee, out string message, ValidationParamContainer args)
        {
            if (newBaseFee == null)
            {
                message = "New Fee is null.";
                return false;
            }

            BorrowerClosingCostFee newFee = (BorrowerClosingCostFee)newBaseFee;

            if (newFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId)
            {
                foreach (BorrowerClosingCostFeePayment payment in newFee.ChildPaymentList)
                {
                    if (!payment.ValidateBorrowerFeePayment(out message))
                    {
                        return false;
                    }
                }
            }

            if (this.ClosingCostFeeTypeId != newFee.ClosingCostFeeTypeId)
            {
                message = "Comparing two different fees.";
                return false;
            }

            if (args.IsRequireFeesFromDropDown.HasValue)
            {
                bool requireFeesFromDropDown = args.IsRequireFeesFromDropDown.Value;
                if (!this.SourceFeeTypeId.HasValue && this.InternalHudline != newFee.InternalHudline &&
                    ((this.IsSystemLegacyFee && !this.IsEditableSystemFee) || requireFeesFromDropDown))
                {
                    message = "Hudline for fee " + this.Description + " cannot be changed. It is read only.";
                    return false;
                }

                if (this.IsApr != newFee.IsApr && requireFeesFromDropDown)
                {
                    message = "APR checkbox for fee " + this.Description + " cannot be changed. It is read only.";
                    return false;
                }

                if (this.IsFhaAllowable != newFee.IsFhaAllowable && requireFeesFromDropDown)
                {
                    message = "FHA checkbox for fee " + this.Description + " cannot be changed. It is read only.";
                    return false;
                }

                if (this.CanShop != newFee.CanShop && requireFeesFromDropDown)
                {
                    message = "Can Shop checkbox for fee " + this.Description + " cannot be changed. It is read only.";
                    return false;
                }

                if (this.IsTitleFee != newFee.IsTitleFee && requireFeesFromDropDown)
                {
                    message = "Title checkbox for fee " + this.Description + " cannot be changed. It is read only.";
                    return false;
                }

                if (this.IsOptional != newFee.IsOptional && (requireFeesFromDropDown || this.InternalIntegratedDisclosureSectionT != E_IntegratedDisclosureSectionT.SectionH))
                {
                    message = "Optional checkbox for fee " + this.Description + " cannot be changed. It is read only.";
                    return false;
                }
            }

            if (args.AllowCloserWrite.HasValue)
            {
                if (this.Dflp != newFee.Dflp && args.AllowCloserWrite.Value != true)
                {
                    message = "DFLP checkbox for fee " + this.Description + " cannot be changed. It is read only.";
                    return false;
                }
            }

            if (!base.ValidateFeeModifcation(newBaseFee, out message, args))
            {
                return false;
            }

            // It passed all those checks. It should be validated by now.
            message = "Fee " + this.Description + " is valid.";
            return true;
        }

        /// <summary>
        /// Gets the housing expense on a specific line number. Can return an Assigned expense. 
        /// </summary>
        /// <param name="customExpenseLineNumberT">The custom expense line number.</param>
        /// <returns>The housing expense on the line number or null if it does not exist.</returns>
        internal BaseHousingExpense GetHousingExpenseOnLineNum(E_CustomExpenseLineNumberT customExpenseLineNumberT)
        {
            if (this.ParentClosingCostSet != null && this.ParentClosingCostSet.HasDataLoanAssociate)
            {
                return this.ParentClosingCostSet.DataLoan.sHousingExpenses.GetExpenseByLineNum(customExpenseLineNumberT);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the payment list for this borrower fee. Special handling for the MI and VA fees.
        /// </summary>
        /// <param name="cacheAmount">The cache amount for performance.</param>
        /// <returns>The list of payments associated with this fee.</returns>
        protected override List<LoanClosingCostFeePayment> GetPaymentList(decimal? cacheAmount)
        {
            if (this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumUpfrontFeeTypeId ||
                this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId ||
                this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900VAFundingFeeTypeId)
            {
                bool paymentsAreBeingReset = this.TotalAmount == 0 && this.ChildPaymentList != null && this.ChildPaymentList.Count > 0;
                if (paymentsAreBeingReset)
                {
                    // A total amount of zero for these fees means that the fee is not in the closing cost set. 
                    // We should also zero out the payments for these fees.
                    this.ClearPayments();
                }

                // We have the data loan so we can make the adjustments.
                // With no data loan, we can't really do anything, so just return whatever we got. Don't let it go through the normal payment stuff though. 
                if (this.ParentClosingCostSet != null && this.ParentClosingCostSet.HasDataLoanAssociate)
                {
                    if (this.ParentClosingCostSet.DataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy &&
                        this.ParentClosingCostSet.DataLoan.sFfUfmip1003 > 0)
                    {
                        bool isFeeAmountZero = false;
                        if (cacheAmount.HasValue)
                        {
                            isFeeAmountZero = cacheAmount.Value == 0;
                        }
                        else
                        {
                            isFeeAmountZero = this.TotalAmount == 0;
                        }

                        if (this.ChildPaymentList == null || this.ChildPaymentList.Count != 2)
                        {
                            // Don't have the correct number of fees. Reset it.
                            this.ClearPayments();

                            BorrowerClosingCostFeePayment payment1 = new BorrowerClosingCostFeePayment()
                            {
                                GfeClosingCostFeePaymentTimingT = E_GfeClosingCostFeePaymentTimingT.AtClosing,
                                IsSystemGenerated = false,
                                PaidByT = E_ClosingCostFeePaymentPaidByT.Borrower,
                                Id = ClosingCostSetUtils.CombineGuids(this.UniqueId, ClosingCostSetUtils.SaltGuid1),
                            };

                            BorrowerClosingCostFeePayment payment2 = new BorrowerClosingCostFeePayment()
                            {
                                GfeClosingCostFeePaymentTimingT = E_GfeClosingCostFeePaymentTimingT.AtClosing,
                                IsSystemGenerated = false,
                                PaidByT = E_ClosingCostFeePaymentPaidByT.Borrower,
                                Id = ClosingCostSetUtils.CombineGuids(this.UniqueId, ClosingCostSetUtils.SaltGuid2),
                            };

                            this.ChildPaymentList.Add(payment1);
                            this.ChildPaymentList.Add(payment2);
                        }

                        BorrowerClosingCostFeePayment mipFinancedPayment = (BorrowerClosingCostFeePayment)this.ChildPaymentList[0];
                        mipFinancedPayment.PaidByT = E_ClosingCostFeePaymentPaidByT.BorrowerFinance;
                        mipFinancedPayment.GfeClosingCostFeePaymentTimingT = E_GfeClosingCostFeePaymentTimingT.AtClosing;
                        mipFinancedPayment.Amount = isFeeAmountZero ? 0 : this.ParentClosingCostSet.DataLoan.sFfUfmipFinanced;
                        mipFinancedPayment.IsSystemGenerated = false;
                        mipFinancedPayment.IsMIPFinancedPayment = true;

                        BorrowerClosingCostFeePayment cashPdPayment = (BorrowerClosingCostFeePayment)this.ChildPaymentList[1];
                        cashPdPayment.Amount = isFeeAmountZero ? 0 : this.ParentClosingCostSet.DataLoan.sUfCashPd;
                        cashPdPayment.IsSystemGenerated = false;
                        cashPdPayment.IsCashPdPayment = true;
                        if (cashPdPayment.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance)
                        {
                            cashPdPayment.PaidByT = E_ClosingCostFeePaymentPaidByT.Borrower;
                        }

                        mipFinancedPayment.EnsureCorrectness();
                        cashPdPayment.EnsureCorrectness();
                    }
                    else
                    {
                        // We didn't meet the conditions to do the specific payments BUT we can still do the normal payment processing.
                        var paymentsList = base.GetPaymentList(cacheAmount);

                        if (paymentsAreBeingReset)
                        {
                            paymentsList.First().Id = ClosingCostSetUtils.CombineGuids(this.UniqueId, ClosingCostSetUtils.SaltGuid1);
                        }

                        return paymentsList;
                    }
                }
                else
                {
                    // Even though we just want to let this go, the fee wants at least one payment so we'll go make one if we have to.
                    if (this.ChildPaymentList == null || this.ChildPaymentList.Count == 0)
                    {
                        this.ClearPayments();
                        BorrowerClosingCostFeePayment payment = new BorrowerClosingCostFeePayment();
                        payment.GfeClosingCostFeePaymentTimingT = E_GfeClosingCostFeePaymentTimingT.AtClosing;
                        payment.Amount = cacheAmount.HasValue ? cacheAmount.Value : this.TotalAmount;
                        payment.IsSystemGenerated = false;
                        
                        payment.EnsureCorrectness();
                        this.ChildPaymentList.Add(payment);
                    }
                }

                foreach (LoanClosingCostFeePayment payment in this.ChildPaymentList)
                {
                    // It's possible that the Fee was loaded up but was not linked to its child Payments. The base function does this, so we need to copy it too.
                    payment.SetParent(this);
                }

                return this.ChildPaymentList;
            }

            return base.GetPaymentList(cacheAmount);
        }

        /// <summary>
        /// Creates a new BorrowerClosingCostFormula.
        /// </summary>
        /// <returns>The new BorrowerClosingCostFormula.</returns>
        protected override LoanClosingCostFormula CreateNewFormula()
        {
            return new BorrowerClosingCostFormula();
        }

        /// <summary>
        /// Creates a new BorrowerClosingCostFeePayment.
        /// </summary>
        /// <returns>The new BorrowerClosingCostFeePayment.</returns>
        protected override LoanClosingCostFeePayment CreateNewPayment()
        {
            return new BorrowerClosingCostFeePayment();
        }

        /// <summary>
        /// For expenses that can have custom line numbers. Gets the line number from the loan file.
        /// </summary>
        /// <param name="feeTypeId">The closing cost fee type id.</param>
        /// <returns>Null if expense cannot have custom line number or has line number of "Any". The line number otherwise.</returns>
        private string GetHudlineFromExpenseOrFeeSetup(Guid feeTypeId)
        {
            if (this.ParentClosingCostSet != null && this.ParentClosingCostSet.HasDataLoanAssociate)
            {
                BaseHousingExpense retrievedExp = null;
                if (this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000WindStormInsuranceFeeTypeId)
                {
                    retrievedExp = this.GetHousingExpense(E_HousingExpenseTypeT.WindstormInsurance);
                }
                else if (this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000CondoHo6InsuranceFeeTypeId)
                {
                    retrievedExp = this.GetHousingExpense(E_HousingExpenseTypeT.CondoHO6Insurance);
                }
                else if (this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000HomeOwnerAssociationDuesFeeTypeId)
                {
                    retrievedExp = this.GetHousingExpense(E_HousingExpenseTypeT.HomeownersAsscDues);
                }
                else if (this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000GroundRentFeeTypeId)
                {
                    retrievedExp = this.GetHousingExpense(E_HousingExpenseTypeT.GroundRent);
                }
                else if (this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000OtherTax1FeeTypeId)
                {
                    retrievedExp = this.GetHousingExpense(E_HousingExpenseTypeT.OtherTaxes1);
                }
                else if (this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000OtherTax2FeeTypeId)
                {
                    retrievedExp = this.GetHousingExpense(E_HousingExpenseTypeT.OtherTaxes2);
                }
                else if (this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000OtherTax3FeeTypeId)
                {
                    retrievedExp = this.GetHousingExpense(E_HousingExpenseTypeT.OtherTaxes3);
                }
                else if (this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000OtherTax4FeeTypeId)
                {
                    retrievedExp = this.GetHousingExpense(E_HousingExpenseTypeT.OtherTaxes4);
                }
                else
                {
                    return null;
                }

                if (retrievedExp != null)
                {
                    if (retrievedExp.CustomExpenseLineNum == E_CustomExpenseLineNumberT.None)
                    {
                        return null;
                    }
                    else if (retrievedExp.CustomExpenseLineNum == E_CustomExpenseLineNumberT.Any)
                    {
                        // The expense is set to 'Any' and we have an associated parent Set. Should try to retrieve from FeeSetup.
                        if (this.ParentClosingCostSet.BrokerClosingCostSet != null)
                        {
                            var fee = this.ParentClosingCostSet.BrokerClosingCostSet.FindFeeByTypeId(this.ClosingCostFeeTypeId);
                            return fee == null ? null : fee.HudLine_rep;
                        }
                    }
                    else
                    {
                        return retrievedExp.LineNumAsString;
                    }
                }
            }
            
            // If no data loan or expenses to grab from, just use the defaults.
            return null;
        }

        /// <summary>
        /// Gets the borrower paid or financed portion of the fee total amount that is applicable toward calculating QM Status.
        /// </summary>
        /// <param name="financed">If true amount returned is the amount of the fee amount that applies to the Financed QM Fee Amount.</param>
        /// <returns>Amount to be included in QM Calculation.</returns>
        /// <remarks>
        /// If Fee is included in the Total QM Fee Amount calculation (<code>IsIncludedInQm</code>) then QM Amount is the sum of all
        /// Closing Cost Fee Payments made or financed by the borrower.
        /// </remarks>
        private decimal GetQmAmount(bool financed)
        {
            if (this.IsIncludedInQm == false)
            {
                return 0;
            }

            decimal sum = 0;

            foreach (var payment in this.Payments)
            {
                if (financed)
                {
                    // NOTE: For both the following conditions and IsIncludedInQm to be true
                    // IsAffiliate must also be true (hence why it's not explicitly checked for).
                    if (payment.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance &&
                        this.IsApr == false && this.QmRequiresApr == false)
                    {
                        sum += payment.Amount;
                    }
                }
                else
                {
                    // If IsApr is true, payer must be the borrower for payer amount to be counted in QM Amount.
                    // If IsApr is false, we don't care who the payer is.
                    if (this.IsApr == false || payment.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance ||
                        payment.PaidByT == E_ClosingCostFeePaymentPaidByT.Borrower)
                    {
                        sum += payment.Amount;
                    }
                }
            }

            return sum;
        }
    }
}
