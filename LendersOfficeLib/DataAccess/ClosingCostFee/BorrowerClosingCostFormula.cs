﻿// <copyright file="BorrowerClosingCostFormula.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   7/21/2015
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;

    /// <summary>
    /// Class representing a borrower responsible closing cost formula.
    /// </summary>
    [DataContract]
    public class BorrowerClosingCostFormula : LoanClosingCostFormula
    {
        /// <summary>
        /// A list of formula type that base amount must pull from data loan layer.
        /// </summary>
        private static readonly E_ClosingCostFeeFormulaT[] ConstantBaseAmountFromFieldIdList = 
        {
            E_ClosingCostFeeFormulaT.MortgageInsurancePremiumRecurring,
            E_ClosingCostFeeFormulaT.MortgageInsurancePremiumUpfront,
            E_ClosingCostFeeFormulaT.VAFundingFee,
            E_ClosingCostFeeFormulaT.sIPia,
            E_ClosingCostFeeFormulaT.sLDiscnt,
            E_ClosingCostFeeFormulaT.sOriginatorCompensationTotalAmount
        };

        /// <summary>
        /// Gets or sets the type of formula. Also ensures the correct formula type.
        /// </summary>
        /// <value>The type of formula.</value>
        public override E_ClosingCostFeeFormulaT FormulaT
        {
            get
            {
                if (this.ParentClosingCostFee != null && this.ParentClosingCostFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId &&
                    this.ParentClosingCostSet != null && this.ParentClosingCostSet.HasDataLoanAssociate)
                {
                    E_sOriginatorCompensationPaymentSourceT originatorCompensationPaymentSourceT;
                    E_sDisclosureRegulationT disclosureRegulationT;
                    bool gfeIsTPOTransaction;
                    E_BranchChannelT branchChannelT;

                    if (this.ParentClosingCostSet.HasDataLoanAssociate)
                    {
                        originatorCompensationPaymentSourceT = this.ParentClosingCostSet.DataLoan.sOriginatorCompensationPaymentSourceT;
                        disclosureRegulationT = this.ParentClosingCostSet.DataLoan.sDisclosureRegulationT;
                        gfeIsTPOTransaction = this.ParentClosingCostSet.DataLoan.sGfeIsTPOTransaction;
                        branchChannelT = this.ParentClosingCostSet.DataLoan.sBranchChannelT;
                    }
                    else if (this.ParentClosingCostSet.HasClosingCostArchiveAssociate)
                    {
                        originatorCompensationPaymentSourceT = (E_sOriginatorCompensationPaymentSourceT)this.ParentClosingCostSet.ClosingCostArchive.GetCountValue("sOriginatorCompensationPaymentSourceT");
                        disclosureRegulationT = (E_sDisclosureRegulationT)this.ParentClosingCostSet.ClosingCostArchive.GetCountValue("sDisclosureRegulationT");
                        gfeIsTPOTransaction = this.ParentClosingCostSet.ClosingCostArchive.GetBitValue("sGfeIsTPOTransaction");
                        branchChannelT = (E_BranchChannelT)this.ParentClosingCostSet.ClosingCostArchive.GetCountValue("sBranchChannelT");
                    }
                    else
                    {
                        return base.FormulaT;
                    }

                    if (originatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid)
                    {
                        this.InternalFormulaT = E_ClosingCostFeeFormulaT.Full;
                        return this.InternalFormulaT;
                    }
                    else if (originatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)
                    {
                        bool skipTridCheck = false;
                        if (this.ParentFeeAsBorrower != null && this.ParentFeeAsBorrower.OrigCompSkipTRIDCheck)
                        {
                            skipTridCheck = true;
                        }

                        if (disclosureRegulationT != E_sDisclosureRegulationT.TRID || skipTridCheck == true)
                        {
                            if (gfeIsTPOTransaction)
                            {
                                if (branchChannelT == E_BranchChannelT.Broker || branchChannelT == E_BranchChannelT.Correspondent)
                                {
                                    this.InternalFormulaT = E_ClosingCostFeeFormulaT.Full;
                                    return this.InternalFormulaT;
                                }
                                else
                                {
                                    this.InternalFormulaT = E_ClosingCostFeeFormulaT.sOriginatorCompensationTotalAmount;
                                    return this.InternalFormulaT;
                                }
                            }
                            else
                            {
                                this.InternalFormulaT = E_ClosingCostFeeFormulaT.sOriginatorCompensationTotalAmount;
                                return this.InternalFormulaT;
                            }
                        }
                    }
                }

                return base.FormulaT;
            }

            set
            {
                base.FormulaT = value;
            }
        }

        /// <summary>
        /// Gets or sets the percent amount.
        /// </summary>
        /// <value>The percent amount.</value>
        public override decimal Percent
        {
            get
            {
                if (this.FormulaT == E_ClosingCostFeeFormulaT.InitialEscrowPaymentAtClosing || this.FormulaT == E_ClosingCostFeeFormulaT.PrepaidAmountForExpenses)
                {
                    this.InternalPercent = "0.000%";
                }

                return base.Percent;
            }

            set
            {
                base.Percent = value;
            }
        }

        /// <summary>
        /// Gets or sets the minimum base amount.
        /// </summary>
        /// <value>The minimum base amount.</value>
        public override decimal BaseAmount
        {
            get
            {
                if (this.ParentClosingCostFee != null && this.ParentClosingCostSet != null && this.ParentClosingCostSet.HasDataLoanAssociate)
                {
                    decimal value = 0;

                    if (this.FormulaT == E_ClosingCostFeeFormulaT.InitialEscrowPaymentAtClosing)
                    {
                        if (this.ParentClosingCostFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId)
                        {
                            // For possible infinite loop.
                            var res = this.GetValueByFieldId("sAggregateAdjRsrv");
                            if (res.HasValue)
                            {
                                value = res.Value;
                            }
                        }
                        else if (this.ParentClosingCostFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId)
                        {
                            // Should always have value.
                            var res = this.GetValueByFieldId("sMInsRsrv");
                            if (res.HasValue)
                            {
                                value = res.Value;
                            }
                        }
                        else
                        {
                            BaseHousingExpense housingExpense = this.GetExpense(ref value);
                            if (housingExpense != null)
                            {
                                if (housingExpense.IsEscrowedAtClosing == E_TriState.Yes)
                                {
                                    value = housingExpense.ReserveAmt;
                                }
                            }
                        }

                        this.InternalBaseAmount = this.LosConvertForJson.ToMoneyString(value, FormatDirection.ToRep);
                    }
                    else if (this.FormulaT == E_ClosingCostFeeFormulaT.PrepaidAmountForExpenses)
                    {
                        BaseHousingExpense housingExpense = this.GetExpense(ref value);
                        if (housingExpense != null)
                        {
                            if (housingExpense.IsPrepaid)
                            {
                                value = housingExpense.PrepaidAmt;
                            }
                        }

                        this.InternalBaseAmount = this.LosConvertForJson.ToMoneyString(value, FormatDirection.ToRep);
                    }
                    else if (ConstantBaseAmountFromFieldIdList.Contains(this.FormulaT))
                    {
                        // Should always have a value since only the Aggregate Adjustment can return null.
                        var res = this.GetValueByFieldId(this.InternalBaseAmountFieldId);
                        if (res.HasValue)
                        {
                            value = res.Value;
                        }
                        
                        if (this.FormulaT == E_ClosingCostFeeFormulaT.sLDiscnt && value < 0)
                        {
                            // 3/16/2015 dd - Only include value if it greater than zero for discount point.
                            value = 0;
                        }

                        // 2/16/2015 dd - Save the value so it can be use without data loan.
                        if (this.FormulaT == E_ClosingCostFeeFormulaT.sIPia)
                        {
                            // OPM 217956. The field it is based on uses the long format decimal.
                            this.InternalBaseAmount = this.LosConvertForJson.ToMoneyString6DecimalDigits(value, FormatDirection.ToRep);
                        }
                        else
                        {
                            this.InternalBaseAmount = this.LosConvertForJson.ToMoneyString(value, FormatDirection.ToRep);
                        }
                    }
                }
                
                return base.BaseAmount;
            }

            set
            {
                base.BaseAmount = value;
            }
        }

        /// <summary>
        /// Gets or sets the number of periods.
        /// </summary>
        /// <value>The number of periods.</value>
        public override int NumberOfPeriods
        {
            get
            {
                if (this.FormulaT == E_ClosingCostFeeFormulaT.InitialEscrowPaymentAtClosing || this.FormulaT == E_ClosingCostFeeFormulaT.PrepaidAmountForExpenses)
                {
                    return 1;
                }
                else if (this.FormulaT == E_ClosingCostFeeFormulaT.sIPia)
                {
                    if (this.ParentClosingCostSet != null && this.ParentClosingCostSet.HasDataLoanAssociate)
                    {
                        int value = this.GetIntValueByFieldId(this.InternalNumberOfPeriodsFieldId);

                        // 2/16/2015 dd - Save the value so it can be use without data loan.
                        this.InternalNumberOfPeriods = this.LosConvertForJson.ToCountString(value);
                    }
                }

                return base.NumberOfPeriods;
            }

            set
            {
                base.NumberOfPeriods = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this fee is one of the special ones that pulls base value directly from loan.
        /// </summary>
        /// <value>True if this field pulls from loan file as a base value.</value>
        public bool IsConstantBaseAmountFromFieldId
        {
            get 
            { 
                return ConstantBaseAmountFromFieldIdList.Contains(this.FormulaT); 
            }
        }

        /// <summary>
        /// Gets or sets the loan field id that store the internal base amount. 
        /// </summary>
        /// <value>The loan field id that store the internal base amount.</value>
        [DataMember(Name = "base_fid")]
        protected string InternalBaseAmountFieldId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the loan field id that store the internal number of periods.
        /// </summary>
        /// <value>The loan field id that store the internal number of periods.</value>
        [DataMember(Name = "period_fid")]
        protected string InternalNumberOfPeriodsFieldId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the parent fee as a borrower fee.
        /// </summary>
        /// <value>The parent fee as a borrower fee.</value>
        private BorrowerClosingCostFee ParentFeeAsBorrower
        {
            get
            {
                return this.ParentClosingCostFee as BorrowerClosingCostFee;
            }
        }

        /// <summary>
        /// Clones the given object.
        /// </summary>
        /// <returns>A borrower formula without an associated closing cost fee. Please attach to a closing cost fee before use.</returns>
        public override object Clone()
        {
            BorrowerClosingCostFormula formula = (BorrowerClosingCostFormula)this.MemberwiseClone();
            formula.ParentClosingCostFee = null;
            return formula;
        }

        /// <summary>
        /// Ensure some values are properly set before serialization/deserialization.
        /// </summary>
        internal override void EnsureCorrectness()
        {
            base.EnsureCorrectness();

            // 12/30/2014 dd - Before serialization we need to make sure that when fee has dataloan object associate then
            // it must save away savedPercentTotalAmount.
            if (this.ParentClosingCostSet != null && this.ParentClosingCostSet.HasDataLoanAssociate)
            {
                if (this.FormulaT == E_ClosingCostFeeFormulaT.MortgageInsurancePremiumRecurring ||
                    this.FormulaT == E_ClosingCostFeeFormulaT.MortgageInsurancePremiumUpfront ||
                    this.FormulaT == E_ClosingCostFeeFormulaT.VAFundingFee)
                {
                    // Should always have a value since only the Aggregate Adjustment can return null.
                    var res = this.GetValueByFieldId(this.InternalBaseAmountFieldId);
                    if (res.HasValue)
                    {
                        this.BaseAmount = res.Value;
                    }
                }
                else if (this.FormulaT == E_ClosingCostFeeFormulaT.InitialEscrowPaymentAtClosing || this.FormulaT == E_ClosingCostFeeFormulaT.PrepaidAmountForExpenses)
                {
                    this.SavedPercentTotalAmount = 0;
                }
                else
                {
                    this.SavedPercentTotalAmount = this.ParentClosingCostSet.GetPercentBaseAmount(this.PercentBaseT);
                }
            }
        }

        /// <summary>
        /// Checks if, for this formula's formula type, all the other fields are correct.
        /// </summary>
        protected override void EnsureFormulaType()
        {
            if (this.FormulaT == E_ClosingCostFeeFormulaT.sLDiscnt)
            {
                this.InternalBaseAmountFieldId = "sLDiscnt";
                this.InternalNumberOfPeriodsFieldId = string.Empty;
            }
            else if (this.FormulaT == E_ClosingCostFeeFormulaT.MortgageInsurancePremiumRecurring)
            {
                this.InternalBaseAmountFieldId = "sMipPia";
                this.InternalNumberOfPeriodsFieldId = string.Empty;
            }
            else if (this.FormulaT == E_ClosingCostFeeFormulaT.MortgageInsurancePremiumUpfront)
            {
                this.InternalBaseAmountFieldId = "sMipPiaUpfront";
                this.InternalNumberOfPeriodsFieldId = string.Empty;
            }
            else if (this.FormulaT == E_ClosingCostFeeFormulaT.VAFundingFee)
            {
                this.InternalBaseAmountFieldId = "sVaFf";
                this.InternalNumberOfPeriodsFieldId = string.Empty;
            }
            else if (this.FormulaT == E_ClosingCostFeeFormulaT.sIPia)
            {
                this.InternalPercent = string.Empty;
                this.InternalBaseAmountFieldId = "sIPerDay";
                this.InternalNumberOfPeriodsFieldId = "sIPiaDy";
            }
            else if (this.FormulaT == E_ClosingCostFeeFormulaT.sOriginatorCompensationTotalAmount)
            {
                this.InternalPercent = string.Empty;
                this.InternalBaseAmountFieldId = "sOriginatorCompensationTotalAmount";
                this.InternalNumberOfPeriodsFieldId = string.Empty;
                this.InternalNumberOfPeriods = "1";
            }
            else if (this.FormulaT == E_ClosingCostFeeFormulaT.InitialEscrowPaymentAtClosing || this.FormulaT == E_ClosingCostFeeFormulaT.PrepaidAmountForExpenses)
            {
                this.InternalPercent = string.Empty;
                this.InternalBaseAmountFieldId = string.Empty;
                this.InternalNumberOfPeriodsFieldId = string.Empty;
                this.InternalNumberOfPeriods = "1";
            }
            else if (this.FormulaT == E_ClosingCostFeeFormulaT.PercentOnly)
            {
                this.InternalBaseAmount = string.Empty;
                this.InternalBaseAmountFieldId = string.Empty;
                this.InternalNumberOfPeriodsFieldId = string.Empty;
            }
            else if (this.FormulaT == E_ClosingCostFeeFormulaT.MinBaseOnly)
            {
                this.InternalPercent = string.Empty;
                this.InternalBaseAmountFieldId = string.Empty;
                this.InternalNumberOfPeriodsFieldId = string.Empty;
            }
        }

        /// <summary>
        /// Gets the housing expense based on this formula's associated fee's fee type Id.
        /// </summary>
        /// <param name="value">A value to set. Will only get set to 0 in this function.</param>
        /// <returns>The housing expense associated with the fee's fee type Id.</returns>
        private BaseHousingExpense GetExpense(ref decimal value)
        {
            E_HousingExpenseTypeT? housingExpenseTypeT = ClosingCostSetUtils.GetHousingExpenseTypeTFromFeeId(this.ParentClosingCostFee.ClosingCostFeeTypeId);
            if (!housingExpenseTypeT.HasValue)
            {
                return null;
            }

            BaseHousingExpense housingExpense = null;
            if (housingExpenseTypeT.Value != E_HousingExpenseTypeT.Unassigned)
            {
                housingExpense = this.ParentFeeAsBorrower.GetHousingExpense(housingExpenseTypeT.Value);
            }
            else
            {
                E_CustomExpenseLineNumberT? customExpenseLineNumberT = ClosingCostSetUtils.GetLegacyCustomExpenseLineNumFromFeeTypeId(this.ParentClosingCostFee.ClosingCostFeeTypeId);
                if (!customExpenseLineNumberT.HasValue)
                {
                    return null;
                }

                if (customExpenseLineNumberT.Value != E_CustomExpenseLineNumberT.None)
                {
                    housingExpense = this.ParentFeeAsBorrower.GetHousingExpense(customExpenseLineNumberT.Value);

                    // Check if the Unassigned expense is actually used. If not, then 0 out the value.
                    BaseHousingExpense trueExpense = this.ParentFeeAsBorrower.GetHousingExpenseOnLineNum(customExpenseLineNumberT.Value);
                    if (trueExpense != null && housingExpense != null && trueExpense != housingExpense)
                    {
                        value = 0;
                        housingExpense = null;
                    }
                }
            }

            return housingExpense;
        }

        /// <summary>
        /// Gets the value from loan object by field id.
        /// </summary>
        /// <param name="fieldId">Field id of the loan object.</param>
        /// <returns>Value from the loan object.</returns>
        private decimal? GetValueByFieldId(string fieldId)
        {
            // 2/12/2015 dd - When add new field id here make sure to update the DependOns attribute in sClosingCostDataSet
            if (this.ParentClosingCostSet != null && this.ParentClosingCostSet.HasDataLoanAssociate)
            {
                var dataLoan = this.ParentClosingCostSet.DataLoan;
                
                bool isUseCache = dataLoan.IsUseFeeBaseCache();
               
                if (isUseCache)
                {
                    var cacheResult = dataLoan.GetFeeBaseCache(fieldId);
                    if (cacheResult.HasValue)
                    {
                        return cacheResult.Value;
                    }
                }

                decimal result;
                if (fieldId == "sMipPia")
                {
                    result = dataLoan.sRecurringMipPia;
                }
                else if (fieldId == "sMipPiaUpfront")
                {
                    result = dataLoan.sUpfrontMipPia;
                }
                else if (fieldId == "sVaFf")
                {
                    result = dataLoan.sVaFf;
                }
                else if (fieldId == "sProHazInsMb")
                {
                    result = dataLoan.sProHazInsMb;
                }
                else if (fieldId == "sProHazInsR")
                {
                    result = dataLoan.sProHazInsR;
                }
                else if (fieldId == "sIPerDay")
                {
                    result = dataLoan.sIPerDay;
                }
                else if (fieldId == "sProHazIns")
                {
                    result = dataLoan.sProHazIns;
                }
                else if (fieldId == "sProMIns")
                {
                    result = dataLoan.sProMIns;
                }
                else if (fieldId == "sProRealETxMb")
                {
                    result = dataLoan.sProRealETxMb;
                }
                else if (fieldId == "sProSchoolTx")
                {
                    result = dataLoan.sProSchoolTx;
                }
                else if (fieldId == "sProFloodIns")
                {
                    result = dataLoan.sProFloodIns;
                }
                else if (fieldId == "sAggregateAdjRsrv")
                {
                    var res = dataLoan.GetsAggregateAdjRsrv();
                    if (res.HasValue)
                    {
                        result = res.Value;
                    }
                    else
                    {
                        if (isUseCache)
                        {
                            dataLoan.SetFeeBaseCache(fieldId, 0);
                        }

                        return null;
                    }
                }
                else if (fieldId == "s1006ProHExp")
                {
                    result = dataLoan.s1006ProHExp;
                }
                else if (fieldId == "s1007ProHExp")
                {
                    result = dataLoan.s1007ProHExp;
                }
                else if (fieldId == "sProRealETxR")
                {
                    result = dataLoan.sProRealETxR;
                }
                else if (fieldId == "sLDiscnt")
                {
                    result = dataLoan.sGfeDiscountPointF;
                }
                else if (fieldId == "sMInsRsrv")
                {
                    result = dataLoan.sMInsRsrv;
                }
                else if (fieldId == "sOriginatorCompensationTotalAmount")
                {
                    // The logic here is meant to only return the value from the
                    // loan when the fee would be included in the enumerator/UI.
                    // If the comp fee is going to be filtered out, then we want
                    // to return 0 for the amount. This logic is needed to ensure
                    // operations which do not go through the enumerator (such as
                    // Sum) do not incorrectly include the comp. This will also
                    // remove the issue where we may write a non-zero amount to
                    // the json that we cannot filter out unless we have access
                    // to a loan or a closing cost archive in the closing cost set.
                    // gf 10/6/15
                    bool skipTridCheck = false;
                    if (this.ParentFeeAsBorrower != null && this.ParentFeeAsBorrower.OrigCompSkipTRIDCheck)
                    {
                        skipTridCheck = true;
                    }
                    
                    if (dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid &&
                        (dataLoan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID || skipTridCheck) &&
                        dataLoan.sGfeIsTPOTransaction &&
                        dataLoan.sBranchChannelT != E_BranchChannelT.Broker &&
                        dataLoan.sBranchChannelT != E_BranchChannelT.Correspondent)
                    {
                        result = dataLoan.sOriginatorCompensationTotalAmount;
                    }
                    else
                    {
                        result = 0;
                    }
                }
                else
                {
                    throw CBaseException.GenericException("Unhandle field id=[" + fieldId + "]");
                }

                if (isUseCache)
                {
                    dataLoan.SetFeeBaseCache(fieldId, result);
                }

                return result;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the value from loan object by field id.
        /// </summary>
        /// <param name="fieldId">Field id of the loan object.</param>
        /// <returns>Integer value from the loan object.</returns>
        private int GetIntValueByFieldId(string fieldId)
        {
            if (this.ParentClosingCostSet != null && this.ParentClosingCostSet.HasDataLoanAssociate)
            {
                if (fieldId == "sHazInsPiaMon")
                {
                    return this.ParentClosingCostSet.DataLoan.sHazInsPiaMon;
                }
                else if (fieldId == "sIPiaDy")
                {
                    return this.ParentClosingCostSet.DataLoan.sIPiaDy;
                }
                else if (fieldId == "sHazInsRsrvMon")
                {
                    return this.ParentClosingCostSet.DataLoan.sHazInsRsrvMon;
                }
                else if (fieldId == "sMInsRsrvMon")
                {
                    return this.ParentClosingCostSet.DataLoan.sMInsRsrvMon;
                }
                else if (fieldId == "sRealETxRsrvMon")
                {
                    return this.ParentClosingCostSet.DataLoan.sRealETxRsrvMon;
                }
                else if (fieldId == "sSchoolTxRsrvMon")
                {
                    return this.ParentClosingCostSet.DataLoan.sSchoolTxRsrvMon;
                }
                else if (fieldId == "sFloodInsRsrvMon")
                {
                    return this.ParentClosingCostSet.DataLoan.sFloodInsRsrvMon;
                }
                else if (fieldId == "s1006RsrvMon")
                {
                    return this.ParentClosingCostSet.DataLoan.s1006RsrvMon;
                }
                else if (fieldId == "s1007RsrvMon")
                {
                    return this.ParentClosingCostSet.DataLoan.s1007RsrvMon;
                }
                else
                {
                    throw CBaseException.GenericException("Unhandle field id=[" + fieldId + "]");
                }
            }
            else
            {
                return 0;
            }
        }
    }
}
