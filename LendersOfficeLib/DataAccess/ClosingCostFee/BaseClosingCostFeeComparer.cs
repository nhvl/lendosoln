﻿// <copyright file="BaseClosingCostFeeComparer.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Eric Mallare
//  Date:   7/9/2015
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// A closing cost fee comparer used to rank fees. It supports both HUD line and loan estimate ranking.
    /// It will always show existing fees first and new fees second based on the order added.
    /// </summary>
    public class BaseClosingCostFeeComparer : IComparer<BaseClosingCostFee>
    {
        /// <summary>
        /// A value indicating what type of view to compare for.
        /// </summary>
        private E_ClosingCostViewT closingCostView;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseClosingCostFeeComparer" /> class.
        /// </summary>
        /// <param name="closingCostView">The type of view the comparer is for.</param>
        public BaseClosingCostFeeComparer(E_ClosingCostViewT closingCostView)
        {
            this.closingCostView = closingCostView;
        }

        /// <summary>
        /// Gets a number indicating fee1 relation to fee2 for display purposes.
        /// </summary>
        /// <param name="fee1">First closing cost fee.</param>
        /// <param name="fee2">Second closing cost fee.</param>
        /// <returns>Returns -1 if x appears before y 0 if they are the same rank 1 if x appears after y.</returns>
        public int Compare(BaseClosingCostFee fee1, BaseClosingCostFee fee2)
        {
            if (fee1 == null)
            {
                throw new NullReferenceException("fee1");
            }

            if (fee2 == null)
            {
                throw new NullReferenceException("fee2");
            }

            bool fee1IsNew = fee1.NewFeeIndex.HasValue;
            bool fee2IsNew = fee2.NewFeeIndex.HasValue;

            if (fee1IsNew && fee2IsNew)
            {
                return fee1.NewFeeIndex.Value.CompareTo(fee2.NewFeeIndex.Value);
            }

            if (fee1IsNew == fee2IsNew)
            {
                switch (this.closingCostView)
                {
                    case E_ClosingCostViewT.LoanHud1:
                    case E_ClosingCostViewT.SellerResponsibleLoanHud1:
                    case E_ClosingCostViewT.LenderTypeHud1:
                        int hudLineComparisonResult = fee1.HudLine.CompareTo(fee2.HudLine);
                        if (hudLineComparisonResult != 0)
                        {
                            return hudLineComparisonResult;
                        }
                        else
                        {
                            return fee1.Description.CompareTo(fee2.Description);
                        }

                    case E_ClosingCostViewT.LoanClosingCost:
                    case E_ClosingCostViewT.LenderTypeEstimate:
                    case E_ClosingCostViewT.SellerResponsibleLenderTypeEstimate:
                    case E_ClosingCostViewT.CombinedLenderTypeEstimate:

                        bool fee1is802 = fee1.HudLine == 802;
                        bool fee2is802 = fee2.HudLine == 802;

                        bool fee1IsAggregateAdj = fee1.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId;
                        bool fee1IsSectionG = fee1.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionG;
                        bool fee2IsAggregateAdj = fee2.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId;
                        bool fee2IsSectionG = fee2.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionG;

                        string fee1desc = fee1.IsTitleFee ? "Title" + fee1.Description : fee1.Description;
                        string fee2desc = fee2.IsTitleFee ? "Title" + fee2.Description : fee2.Description;

                        if (fee1is802 && !fee2is802)
                        {
                            return -1;
                        }
                        else if (fee2is802 && !fee1is802)
                        {
                            return 1;
                        }

                        if (fee1IsAggregateAdj && fee2IsSectionG)
                        {
                            return 1;
                        }
                        else if (fee1IsSectionG && fee2IsAggregateAdj)
                        {
                            return -1;
                        }

                        return fee1desc.ToLower().CompareTo(fee2desc.ToLower());
                    case E_ClosingCostViewT.LeaveBlank:
                        return 0;
                    default:
                        throw new UnhandledEnumException(this.closingCostView);
                }
            }

            return fee1IsNew ? 1 : -1;
        }
    }
}
