﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using Adapter;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Used to keep a history of closing cost fee type IDs and HUD line values assigned to dynamic fees (a fee whose type is not in the broker fee setup) added to a loan.
    /// Implemented as a collection of <see cref="DynamicClosingCostFeeHistoryItem" /> objects.
    /// </summary>
    public class DynamicClosingCostFeeHistory
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="DynamicClosingCostFeeHistory" /> class from being created.
        /// </summary>
        private DynamicClosingCostFeeHistory()
        {
        }

        /// <summary>
        /// Gets BrokerId.
        /// </summary>
        /// <value>A Broker ID.</value>
        public Guid BrokerId { get; private set; }

        /// <summary>
        /// Gets loan ID.
        /// </summary>
        /// <value>A Loan ID.</value>
        public Guid LoanId { get; private set; }

        /// <summary>
        /// Gets of sets a dictionary mapping fee descriptions to dynamic closing cost fee history items.
        /// </summary>
        /// <value>A dictionary mapping fee descriptions to dynamic closing cost fee history items.</value>
        public Dictionary<string, DynamicClosingCostFeeHistoryItem> Items { get; private set; } = new Dictionary<string, DynamicClosingCostFeeHistoryItem>();

        /// <summary>
        /// Retrieve dynamic fee history by loan ID.
        /// </summary>
        /// <param name="brokerId">Broker ID.</param>
        /// <param name="loanId">Loan ID for loan to retrieve history from.</param>
        /// <returns>A <see cref="DynamicClosingCostFeeHistoryItem" /> object.</returns>
        public static DynamicClosingCostFeeHistory Retrieve(Guid brokerId, Guid loanId)
        {
            DynamicClosingCostFeeHistory history = new DynamicClosingCostFeeHistory();
            history.BrokerId = brokerId;
            history.LoanId = loanId;

            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@LoanId", loanId)
            };

            IStoredProcedureDriver driver = GenericLocator<IStoredProcedureDriverFactory>.Factory.Create(TimeoutInSeconds.Thirty);

            using (var connection = DbAccessUtils.GetConnection(brokerId))
            using (var reader = driver.ExecuteReader(connection, null, StoredProcedureName.Create("DYNAMIC_CLOSING_COST_FEE_HISTORY_RetrieveAll").Value, parameters))
            {
                while (reader.Read())
                {
                    DynamicClosingCostFeeHistoryItem item = new DynamicClosingCostFeeHistoryItem(reader);
                    history.Items.Add(item.Description, item);
                }
            }

            return history;
        }

        /// <summary>
        /// Saves Items to DB.
        /// </summary>
        public void Save()
        {
            using (var conn = DbAccessUtils.GetConnection(this.BrokerId))
            {
                conn.OpenWithRetry();
                using (var trans = conn.BeginTransaction())
                {
                    try
                    {
                        foreach (DynamicClosingCostFeeHistoryItem item in this.Items.Values)
                        {
                            item.Save(conn, trans);
                        }

                        trans.Commit();
                    }
                    catch (Exception e)
                    {
                        Tools.LogError(e);
                        trans.Rollback();
                        throw;
                    }
                }
            }
        }
    }
}
