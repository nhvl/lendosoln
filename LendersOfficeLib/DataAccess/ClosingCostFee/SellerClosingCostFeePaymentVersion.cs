﻿namespace DataAccess
{
    /// <summary>
    /// Version number for <see cref="SellerClosingCostFeePayment"/> .
    /// </summary>
    public enum SellerClosingCostFeePaymentVersion
    {
        V0 = 0,
        V1_NonBorrowerResponsible = 1
    }
}
