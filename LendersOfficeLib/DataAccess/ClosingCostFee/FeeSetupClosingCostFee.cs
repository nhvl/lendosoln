﻿// <copyright file="FeeSetupClosingCostFee.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   6/22/2015 
// </summary>
namespace DataAccess
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// The class representing a broker level closing cost fee.
    /// </summary>
    [DataContract]
    public class FeeSetupClosingCostFee : BaseClosingCostFee
    {
        /// <summary>
        /// Gets or sets the formula that will be use to compute the total amount.
        /// </summary>
        /// <value>The formula that will be use to compute the total amount.</value>
        [DataMember(Name = "f")]
        private FeeSetupClosingCostFormula Formula
        {
            get
            {
                return (FeeSetupClosingCostFormula)this.ChildFormula;
            }

            set
            {
                this.ChildFormula = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the fee is a template fee type.
        /// </summary>
        [DataMember(Name = "isTemplate")]
        private bool IsTemplate
        {
            get { return DefaultSystemClosingCostFee.FeeTemplate.IsFeeTemplateSystemFeeType(this.ClosingCostFeeTypeId); }
            set { } // For serialization only
        }

        /// <summary>
        /// Checks if this fee can be removed.
        /// </summary>
        /// <param name="message">The error message.</param>
        /// <returns>True if it can, false otherwise.</returns>
        public override bool ValidateRemoveFee(out string message)
        {
            if (this.IsSystemLegacyFee)
            {
                message = "Fee " + this.Description + " cannot be removed.";
                return false;
            }
            else
            {
                message = "Fee " + this.Description + " can be removed.";
                return true;
            }
        }

        /// <summary>
        /// Converts the fee to a borrower fee.
        /// </summary>
        /// <returns>The converted borrower fee.</returns>
        public BorrowerClosingCostFee ConvertToBorrowerClosingCostFee()
        {
            BorrowerClosingCostFee convertedFee = new BorrowerClosingCostFee();
            this.InitializeLoanFee(convertedFee);
            return convertedFee;
        }

        /// <summary>
        /// Clones this FeeSetupClosingCostFee.
        /// </summary>
        /// <returns>A closing cost fee with the exact values except ID.</returns>
        public override object Clone()
        {
            FeeSetupClosingCostFormula formular = (FeeSetupClosingCostFormula)this.Formula.Clone();
            FeeSetupClosingCostFee clonedFee = (FeeSetupClosingCostFee)this.MemberwiseClone();
            clonedFee.ChildFormula = formular;
            clonedFee.UniqueId = Guid.NewGuid();

            return clonedFee;
        }

        /// <summary>
        /// Checks if the modifications to this FeeSetup Fee are valid. Extra parameters not used.
        /// </summary>
        /// <param name="newBaseFee">The new Fee to check.</param>
        /// <param name="message">The error message if any.</param>
        /// <param name="args">Info needed to validate the fee. Not needed for FeeSetup fees.</param>
        /// <returns>True if the Fee has valid changes, false otherwise.</returns>
        public override bool ValidateFeeModifcation(BaseClosingCostFee newBaseFee, out string message, ValidationParamContainer args)
        {
            if (newBaseFee == null)
            {
                message = "New Fee is null.";
                return false;
            }

            FeeSetupClosingCostFee newFee = (FeeSetupClosingCostFee)newBaseFee;

            if (this.ClosingCostFeeTypeId != newFee.ClosingCostFeeTypeId)
            {
                message = "Comparing two different fees.";
                return false;
            }

            bool isOldSystem = this.IsSystemLegacyFee;
            bool isTemplate = this.IsTemplate;
            if (this.HudLine != newFee.HudLine && isOldSystem && !this.IsEditableSystemFee)
            {
                message = "Hudline for fee " + this.Description + " cannot be changed. It is read only.";
                return false;
            }

            if (this.IsTitleFee != newFee.IsTitleFee && isOldSystem)
            {
                message = "Title checkbox for fee " + this.Description + " cannot be changed, it is read only.";
                return false;
            }

            if (this.InternalIntegratedDisclosureSectionT != newFee.InternalIntegratedDisclosureSectionT && !this.CanChangeSection)
            {
                message = "Loan Estimate Section for fee " + this.Description + " cannot be changed, it is read only.";
                return false;
            }

            if (this.InternalIsOptional != newFee.InternalIsOptional && this.InternalIntegratedDisclosureSectionT != E_IntegratedDisclosureSectionT.SectionH)
            {
                message = "Optional checkbox for fee " + this.Description + " cannot be changed, it is read only.";
                return false;
            }

            if (this.Description != newFee.Description && (isOldSystem || isTemplate))
            {
                message = "Description for fee " + this.Description + " cannot be changed, it is read only.";
                return false;
            }

            if (this.MismoFeeT != newFee.MismoFeeT && isTemplate)
            {
                message = "MISMO Type for fee " + this.Description + " cannot be changed, it is read only.";
                return false;
            }

            if (!base.ValidateFeeModifcation(newBaseFee, out message, args))
            {
                return false;
            }

            message = "Fee " + this.Description + " is valid.";
            return true;
        }

        /// <summary>
        /// Enforces some rules on the formula object.
        /// </summary>
        internal override void EnsureFormula()
        {
            if (this.ChildFormula == null)
            {
                this.ChildFormula = new FeeSetupClosingCostFormula();
            }

            E_ClosingCostFeeFormulaT? type = ClosingCostSetUtils.GetFormulaType(this.IntegratedDisclosureSectionT, this.LegacyGfeFieldT, this.ClosingCostFeeTypeId);
            if (type.HasValue)
            {
                this.ChildFormula.FormulaT = type.Value;
            }
        }

        /// <summary>
        /// Initializes the LoanClosingCostFee with this fee setup fee's values.
        /// </summary>
        /// <param name="convertedFee">The fee to initialize.</param>
        internal void InitializeLoanFee(LoanClosingCostFee convertedFee)
        {
            convertedFee.IsAffiliate = this.IsAffiliate;
            convertedFee.IsApr = this.IsApr;
            convertedFee.Beneficiary = this.Beneficiary;
            convertedFee.CanShop = this.CanShop;
            convertedFee.ClosingCostFeeTypeId = this.ClosingCostFeeTypeId;
            convertedFee.Description = this.Description;
            convertedFee.OriginalDescription = this.Description;
            convertedFee.Dflp = this.Dflp;
            convertedFee.GfeSectionT = this.GfeSectionT;
            convertedFee.HudLine = this.HudLine;
            convertedFee.IntegratedDisclosureSectionT = this.IntegratedDisclosureSectionT;
            convertedFee.IsFhaAllowable = this.IsFhaAllowable;
            convertedFee.IsOptional = this.IsOptional;
            convertedFee.IsThirdParty = this.IsThirdParty;
            convertedFee.IsTitleFee = this.IsTitleFee;
            convertedFee.IsVaAllowable = this.IsVaAllowable;
            convertedFee.LegacyGfeFieldT = this.LegacyGfeFieldT;
            convertedFee.MismoFeeT = this.MismoFeeT;
            convertedFee.NewFeeIndex = this.NewFeeIndex;
            convertedFee.FormulaT = this.FormulaT;
            
            // Initialize the payments with a 0 dollar amount. Even if it did use the normal GetPayments method
            // it would still output 0 since this fee is not yet associated with a set or loan.
            var payments = convertedFee.GetPaymentsWithCachedTotal(0);
        }
    }
}
