﻿// <copyright file="DefaultSystemClosingCostFee.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   12/16/2014 2:45:03 PM 
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Contains a list of default GFE Fee Type.
    /// </summary>
    public static class DefaultSystemClosingCostFee
    {
        // 12/16/2014 dd - These Guid are generate randomly. Do not modify them because it is store in the database.

        /// <summary>
        /// Unique Identifier for Section G default fee.
        /// </summary>
        public static readonly Guid DefaultSectionGFeeTypeId = new Guid("b8f38c9a-c174-46ed-9374-87cbc5b8af45");

        /// <summary>
        /// Unique Identifier for Loan Origination Fee.
        /// </summary>
        public static readonly Guid Hud800LoanOriginationFeeTypeId = new Guid("D6A73356-D798-4D7B-9605-591BBEA1E75F");

        /// <summary>
        /// Unique Identifier for Originator Compensation.
        /// </summary>
        public static readonly Guid Hud800OriginatorCompensationFeeTypeId = new Guid("F17C6573-E1BB-447d-BA89-31183BE07BE8");

        /// <summary>
        /// Unique Identifier for Discount Point.
        /// </summary>
        public static readonly Guid Hud800DiscountPointsFeeTypeId = new Guid("B9731D64-C7E8-4248-A619-CABA8EED6E11");

        /// <summary>
        /// Unique Identifier for Credit Or Charge Fee.
        /// </summary>
        public static readonly Guid Hud800CreditOrChargeFeeTypeId = new Guid("B1A743F7-BC94-45BE-9AEC-FD25871B22D5");

        /// <summary>
        /// Unique Identifier for Appraisal Fee.
        /// </summary>
        public static readonly Guid Hud800AppraisalFeeFeeTypeId = new Guid("FD119846-8304-4C3B-952C-3AF21DFD87BF");

        /// <summary>
        /// Unique Identifier for Credit Report Fee.
        /// </summary>
        public static readonly Guid Hud800CreditReportFeeTypeId = new Guid("8FBDD06F-8656-4A7B-B1C1-126FBA5A11CA");

        /// <summary>
        /// Unique identifier for Tax Service Fee.
        /// </summary>
        public static readonly Guid Hud800TaxServiceFeeTypeId = new Guid("311DC100-5907-49B5-9FE3-CEB6A67FD590");

        /// <summary>
        /// Unique identifier for Flood Certification Fee.
        /// </summary>
        public static readonly Guid Hud800FloodCertificationFeeTypeId = new Guid("B2CCD41B-9EA0-402E-B2EF-2A61C60359C8");

        /// <summary>
        /// Unique identifier for Mortgage Broker Fee.
        /// </summary>
        public static readonly Guid Hud800MortgageBrokerFeeTypeId = new Guid("9C902D0B-2BAD-4BCD-9767-811A70F9A83E");

        /// <summary>
        /// Unique identifier for Lender's Inspection Fee.
        /// </summary>
        public static readonly Guid Hud800LenderInspectionFeeTypeId = new Guid("A915C560-766B-452A-98B7-20F21230D94F");

        /// <summary>
        /// Unique identifier for Processing Fee.
        /// </summary>
        public static readonly Guid Hud800ProcessingFeeTypeId = new Guid("0D06C792-CAEA-48BC-B603-3BAD14195D6A");

        /// <summary>
        /// Unique identifier for Underwriting Fee.
        /// </summary>
        public static readonly Guid Hud800UnderwritingFeeTypeId = new Guid("15CD7EE2-58B5-45BB-ADC5-4F2EA6AA240E");

        /// <summary>
        /// Unique identifier for Wire Transfer Fee.
        /// </summary>
        public static readonly Guid Hud800WireTransferFeeTypeId = new Guid("03B66EE0-6DA0-40D9-9D0F-B4E55EA42266");

        /// <summary>
        /// Unique identifier for 800 Custom #1 Fee.
        /// </summary>
        public static readonly Guid Hud800Custom1FeeTypeId = new Guid("DB79CBAA-5AC5-4682-AFF9-2E130973FEEC");

        /// <summary>
        /// Unique identifier for 800 Custom #2 Fee.
        /// </summary>
        public static readonly Guid Hud800Custom2FeeTypeId = new Guid("C320D734-B97B-4029-BEB1-3C0B28D0B5FD");

        /// <summary>
        /// Unique identifier for 800 Custom #3 Fee.
        /// </summary>
        public static readonly Guid Hud800Custom3FeeTypeId = new Guid("D12877BB-4595-42A3-A2D9-99A899AC54CA");

        /// <summary>
        /// Unique identifier for 800 Custom #4 Fee.
        /// </summary>
        public static readonly Guid Hud800Custom4FeeTypeId = new Guid("3F09DF45-9FBB-4AFB-94C0-DEBF696D31E5");

        /// <summary>
        /// Unique identifier for 800 Custom #5 Fee.
        /// </summary>
        public static readonly Guid Hud800Custom5FeeTypeId = new Guid("E69CAC4F-E899-4613-B3F1-A08EAD3D43BE");

        /// <summary>
        /// Unique identifier for Daily Interest Fee.
        /// </summary>
        public static readonly Guid Hud900DailyInterestFeeTypeId = new Guid("6C674D8E-CC58-4B27-A7AD-B67E53FE478F");

        /// <summary>
        /// Unique identifier for Mortgage Insurance Premium Fee. For recurring premium prepaid at closing.
        /// </summary>
        public static readonly Guid Hud900MortgageInsurancePremiumRecurringFeeTypeId = new Guid("DF136238-25B1-45F0-862D-88E96372B30E");

        /// <summary>
        /// Unique identifier for Mortgage Insurance Premium Fee. For premium paid upfront.
        /// </summary>
        public static readonly Guid Hud900MortgageInsurancePremiumUpfrontFeeTypeId = new Guid("a0f2ad78-4991-4925-bb4d-7363e6095a20");

        /// <summary>
        /// Unique identifier for Hazard Insurance Fee.
        /// </summary>
        public static readonly Guid Hud900HazardInsuranceFeeTypeId = new Guid("E58FDF74-6CDC-4F85-951A-46E70BD746AD");

        /// <summary>
        /// Unique identifier for Flood Insurance prepaid fee.
        /// </summary>
        public static readonly Guid Hud900FloodInsuranceFeeTypeId = new Guid("d40d4a2c-32c2-4ba6-89d9-e45fd3235d7a");

        /// <summary>
        /// Unique identifier for Windstorm Insurance prepaid fee.
        /// </summary>
        public static readonly Guid Hud900WindstormInsuranceFeeTypeId = new Guid("95896456-2666-497f-8434-5c9810144a24");

        /// <summary>
        /// Unique identifier for Condo HO-6 Insurance prepaid fee.
        /// </summary>
        public static readonly Guid Hud900CondoInsuranceFeeTypeId = new Guid("7ced067a-9904-48a4-b304-f5f006786a5d");

        /// <summary>
        /// Unique identifier for Property Tax prepaid fee.
        /// </summary>
        public static readonly Guid Hud900PropertyTaxFeeTypeId = new Guid("cf15756f-e10c-45d7-ac7b-f3023ee3c2b3");

        /// <summary>
        /// Unique identifier for School Tax prepaid fee.
        /// </summary>
        public static readonly Guid Hud900SchoolTaxFeeTypeId = new Guid("d6ba1884-23ae-47f2-814a-17cc4d68a1af");

        /// <summary>
        /// Unique identifier for Other Tax 1 prepaid fee.
        /// </summary>
        public static readonly Guid Hud900OtherTax1FeeTypeId = new Guid("9adb8458-0362-4f2a-a526-8cce89acc89b");

        /// <summary>
        /// Unique identifier for Other Tax 2 prepaid fee.
        /// </summary>
        public static readonly Guid Hud900OtherTax2FeeTypeId = new Guid("928b671a-7290-4067-b37a-ea235c4b8b1a");

        /// <summary>
        /// Unique identifier for Other Tax 3 prepaid fee.
        /// </summary>
        public static readonly Guid Hud900OtherTax3FeeTypeId = new Guid("9596cada-a47e-42a5-af3f-0161bbca3bbc");

        /// <summary>
        /// Unique identifier for Other Tax 4 prepaid fee.
        /// </summary>
        public static readonly Guid Hud900OtherTax4FeeTypeId = new Guid("cec28294-c0ae-4f57-b01b-10464b2e8d5a");

        /// <summary>
        /// Unique identifier for HOA Dues prepaid fee.
        /// </summary>
        public static readonly Guid Hud900HOADuesFeeTypeId = new Guid("36952a1e-db5d-4702-9f61-719ddcb6c761");

        /// <summary>
        /// Unique identifier for Ground Rent prepaid fee.
        /// </summary>
        public static readonly Guid Hud900GroundRentFeeTypeId = new Guid("675d14fd-6cad-498c-9691-e064506d7862");

        /// <summary>
        /// Unique identifier for Line 1008 prepaid fee.
        /// </summary>
        public static readonly Guid Hud900OtherExp1FeeTypeId = new Guid("d74842e9-c692-4c02-aaa4-78d3bdbccadb");
        
        /// <summary>
        /// Unique identifier for Line 1009 prepaid fee.
        /// </summary>
        public static readonly Guid Hud900OtherExp2FeeTypeId = new Guid("244105da-619f-4bd3-8be1-ce53871a1084");
        
        /// <summary>
        /// Unique identifier for Line 1010  prepaid fee.
        /// </summary>
        public static readonly Guid Hud900OtherExp3FeeTypeId = new Guid("df86eff6-1020-4802-96b6-5466990074b4");

        /// <summary>
        /// Unique identifier for Line 1011 prepaid fee.
        /// </summary>
        public static readonly Guid Hud900OtherExp4FeeTypeId = new Guid("6fb90676-10eb-435d-943c-4e5cfd7cc04d");

        /// <summary>
        /// Unique identifier for 900 Custom #1 Fee.
        /// </summary>
        public static readonly Guid Hud900Custom1FeeTypeId = new Guid("1190981F-2F07-4858-94D1-B5BCFD008984");

        /// <summary>
        /// Unique identifier for VA Funding Fee.
        /// </summary>
        public static readonly Guid Hud900VAFundingFeeTypeId = new Guid("4470FFB9-5079-45CB-8419-4D56BAD41A67");

        /// <summary>
        /// Unique identifier for 900 Custom #2 Fee.
        /// </summary>
        public static readonly Guid Hud900Custom2FeeTypeId = new Guid("6E90D70C-7965-40A7-8AF6-8F5A6CCF605C");

        /// <summary>
        /// Unique identifier for Hazard Insurance Reserve Fee.
        /// </summary>
        public static readonly Guid Hud1000HazardInsuranceReserveFeeTypeId = new Guid("9FA9D4C5-D874-4330-BF48-0F1CA3AC5D44");

        /// <summary>
        /// Unique identifier for Mortgage Insurance Reserve Fee.
        /// </summary>
        public static readonly Guid Hud1000MortgageInsuranceReserveFeeTypeId = new Guid("E2222EAE-306B-4EB7-BA15-588B624C1E7F");

        /// <summary>
        /// Unique identifier for Tax Reserve Fee.
        /// </summary>
        public static readonly Guid Hud1000TaxReserveFeeTypeId = new Guid("E6BB4617-3FCD-484F-8CD1-75123B185967");

        /// <summary>
        /// Unique identifier for School Taxes Fee.
        /// </summary>
        public static readonly Guid Hud1000SchoolTaxFeeTypeId = new Guid("E1C8A80E-96DE-43F6-A1CB-9C1AA2EEACDA");

        /// <summary>
        /// Unique identifier for Flood Insurance Reserve Fee.
        /// </summary>
        public static readonly Guid Hud1000FloodInsuranceReserveFeeTypeId = new Guid("949D28CA-81DA-47B3-9C8C-5673C1377752");

        /// <summary>
        /// Unique identifier for Aggregate Adjustment Fee.
        /// </summary>
        public static readonly Guid Hud1000AggregateAdjustmentFeeTypeId = new Guid("0764C50B-D0D4-4341-A9CA-3CC5547ABD73");

        /// <summary>
        /// Unique identifier for 1000 Custom #1 Fee.
        /// </summary>
        public static readonly Guid Hud1000Custom1FeeTypeId = new Guid("923CCF68-761E-4985-B01D-AB29679FAB81");

        /// <summary>
        /// Unique identifier for 1000 Custom #2 Fee.
        /// </summary>
        public static readonly Guid Hud1000Custom2FeeTypeId = new Guid("EBFB4BCA-1ACE-41E1-96B6-A10A4E3B2E4E");

        /// <summary>
        /// Unique identifier for 1000 Custom #3 Fee.
        /// </summary>
        public static readonly Guid Hud1000Custom3FeeTypeId = new Guid("326D80EA-C2D8-11E4-BBEC-8B362BCBFA65");

        /// <summary>
        /// Unique identifier for 1000 Custom #4 Fee.
        /// </summary>
        public static readonly Guid Hud1000Custom4FeeTypeId = new Guid("49645D14-C2D8-11E4-8222-8B362BCBFA65");

        /// <summary>
        /// Unique identifier for 1000 Wind Storm Insurance Fee.
        /// </summary>
        public static readonly Guid Hud1000WindStormInsuranceFeeTypeId = new Guid("BC1E77EE-0478-4B6E-8E14-613E2AEB18D2");

        /// <summary>
        /// Unique identifier for 1000 Condo HO-6 Insurance Fee.
        /// </summary>
        public static readonly Guid Hud1000CondoHo6InsuranceFeeTypeId = new Guid("7274A033-6075-4E56-B8F9-6A1E85883CE6");

        /// <summary>
        /// Unique identifier for 1000 Homeowner Association Dues Fee.
        /// </summary>
        public static readonly Guid Hud1000HomeOwnerAssociationDuesFeeTypeId = new Guid("592F9A81-911B-4366-851D-7BC0746A6B25");

        /// <summary>
        /// Unique identifier for 1000 Ground Rent Fee.
        /// </summary>
        public static readonly Guid Hud1000GroundRentFeeTypeId = new Guid("03007991-79B1-4985-981A-0935679F821A");

        /// <summary>
        /// Unique identifier for 1000 Custom Other Tax #1 Fee.
        /// </summary>
        public static readonly Guid Hud1000OtherTax1FeeTypeId = new Guid("6D44A542-8A69-4281-93B3-40A36E1569CC");

        /// <summary>
        /// Unique identifier for 1000 Custom Other Tax #2 Fee.
        /// </summary>
        public static readonly Guid Hud1000OtherTax2FeeTypeId = new Guid("3B7D1FE9-A266-448E-8FF7-6C0E69798712");

        /// <summary>
        /// Unique identifier for 1000 Custom Other Tax #3 Fee.
        /// </summary>
        public static readonly Guid Hud1000OtherTax3FeeTypeId = new Guid("EF05BBE9-12A5-4DA7-AB4A-2BB7012F989F");

        /// <summary>
        /// Unique identifier for 1000 Custom Other Tax #4 Fee.
        /// </summary>
        public static readonly Guid Hud1000OtherTax4FeeTypeId = new Guid("A750CA72-B771-45D9-BE80-FC6694E34157");

        /// <summary>
        /// Unique identifier for Closing/Escrow Fee.
        /// </summary>
        public static readonly Guid Hud1100ClosingEscrowFeeTypeId = new Guid("E5CB0019-9184-4FED-9A63-E0DF9BCBF0BF");

        /// <summary>
        /// Unique identifier for Owner Title Insurance Fee.
        /// </summary>
        public static readonly Guid Hud1100OwnerTitleInsuranceFeeTypeId = new Guid("BE1DD8F4-AE64-4C96-9954-F965B3897616");

        /// <summary>
        /// Unique identifier for Lender Title Insurance Fee.
        /// </summary>
        public static readonly Guid Hud1100LenderTitleInsuranceFeeTypeId = new Guid("A9044301-1E74-4DF3-B2F9-67716091A240");

        /// <summary>
        /// Unique identifier for Doc Preparation Fee.
        /// </summary>
        public static readonly Guid Hud1100DocPreparationFeeTypeId = new Guid("F13530B8-B1F9-4FAC-80EE-2D6BE556600C");

        /// <summary>
        /// Unique identifier for Notary Fee.
        /// </summary>
        public static readonly Guid Hud1100NotaryFeeTypeId = new Guid("CE912D3D-46C3-466F-BFDF-7AB83375937A");

        /// <summary>
        /// Unique identifier for Attorney Fee.
        /// </summary>
        public static readonly Guid Hud1100AttorneyFeeTypeId = new Guid("2CB19C70-DFF8-4161-8B54-67AB8CE59EA5");

        /// <summary>
        /// Unique identifier for 1100 Custom #1 Fee.
        /// </summary>
        public static readonly Guid Hud1100Custom1FeeTypeId = new Guid("677D8C68-A827-4B67-A852-74D02595D732");

        /// <summary>
        /// Unique identifier for 1100 Custom #2 Fee.
        /// </summary>
        public static readonly Guid Hud1100Custom2FeeTypeId = new Guid("63456171-5424-47AF-9B52-C0AD430D1A6B");

        /// <summary>
        /// Unique identifier for 1100 Custom #3 Fee.
        /// </summary>
        public static readonly Guid Hud1100Custom3FeeTypeId = new Guid("93246E3E-5850-4628-863A-19F8F5F1C692");

        /// <summary>
        /// Unique identifier for 1100 Custom #4 Fee.
        /// </summary>
        public static readonly Guid Hud1100Custom4FeeTypeId = new Guid("67C462B2-E7DD-448B-AF01-8241C5375306");

        /// <summary>
        /// Unique identifier for Recording Fee.
        /// </summary>
        public static readonly Guid Hud1200RecordingFeeTypeId = new Guid("4FB8E955-70C9-4889-A1D2-72A3B3CCE380");

        /// <summary>
        /// Unique identifier for Deed Fee.
        /// </summary>
        public static readonly Guid Hud1200DeedFeeTypeId = new Guid("8F000241-2213-4F9C-828E-B5AD777077A5");

        /// <summary>
        /// Unique identifier for Mortgage Fee.
        /// </summary>
        public static readonly Guid Hud1200MortgageFeeTypeId = new Guid("0BDD1404-B4D0-4E36-B37B-A6F03B814B1C");

        /// <summary>
        /// Unique identifier for Release Fee.
        /// </summary>
        public static readonly Guid Hud1200ReleaseFeeTypeId = new Guid("65FE36BA-FB41-4B80-ADC2-A121DCBCFEEC");

        /// <summary>
        /// Unique identifier for County tax stamps.
        /// </summary>
        public static readonly Guid Hud1200CountyTaxStampsFeeTypeId = new Guid("EEEBB532-8702-4837-84B0-D45A393CFF1E");

        /// <summary>
        /// Unique identifier for State tax/tamps.
        /// </summary>
        public static readonly Guid Hud1200StateTaxStampsFeeTypeId = new Guid("67F6A9A6-4214-4663-BAFA-4E0A6F88D82C");

        /// <summary>
        /// Unique identifier for 1200 Custom #1 Fee.
        /// </summary>
        public static readonly Guid Hud1200Custom1FeeTypeId = new Guid("B1EEAB2D-4007-4E45-90E4-083603376CB0");

        /// <summary>
        /// Unique identifier for 1200 Custom #2 Fee.
        /// </summary>
        public static readonly Guid Hud1200Custom2FeeTypeId = new Guid("CACAE482-9FC6-486D-9288-011D1F9E3737");

        /// <summary>
        /// Unique identifier for 1200 Custom #3 Fee.
        /// </summary>
        public static readonly Guid Hud1200Custom3FeeTypeId = new Guid("FAD237DA-AAD7-47C8-8BA0-DE2478BF24DD");

        /// <summary>
        /// Unique identifier for Pest Inspection Fee.
        /// </summary>
        public static readonly Guid Hud1300PestInspectionFeeTypeId = new Guid("B03A436C-F5DC-4373-91E2-2D5A5BB73668");

        /// <summary>
        /// Unique identifier for 1300 Custom #1 Fee.
        /// </summary>
        public static readonly Guid Hud1300Custom1FeeTypeId = new Guid("6FB5424D-C07B-4B28-B2EF-ED3EF768DAB0");

        /// <summary>
        /// Unique identifier for 1300 Custom #2 Fee.
        /// </summary>
        public static readonly Guid Hud1300Custom2FeeTypeId = new Guid("BA439D5C-F52F-4E77-9821-FF6A5A4DBEAE");

        /// <summary>
        /// Unique identifier for 1300 Custom #3 Fee.
        /// </summary>
        public static readonly Guid Hud1300Custom3FeeTypeId = new Guid("86C9445A-D86C-4D5A-BBF0-A5A5D83C8617");

        /// <summary>
        /// Unique identifier for 1300 Custom #4 Fee.
        /// </summary>
        public static readonly Guid Hud1300Custom4FeeTypeId = new Guid("23F3CB65-A1ED-4883-9B59-B39F5A3352F3");

        /// <summary>
        /// Unique identifier for 1300 Custom #5 Fee.
        /// </summary>
        public static readonly Guid Hud1300Custom5FeeTypeId = new Guid("FFD9253D-3CFE-4AF3-AAA3-0337FD63985C");

        /// <summary>
        /// Set of legacy fee type ids. Should contain all the fee type ids of the fees in <see cref="legacyFeeList" />.
        /// </summary>
        private static readonly HashSet<Guid> LegacyFeeTypeList = new HashSet<Guid>()
        {
            Hud800LoanOriginationFeeTypeId,
            Hud800OriginatorCompensationFeeTypeId,
            Hud800DiscountPointsFeeTypeId,
            Hud800AppraisalFeeFeeTypeId,
            Hud800CreditReportFeeTypeId,
            Hud800TaxServiceFeeTypeId,
            Hud800FloodCertificationFeeTypeId,
            Hud800MortgageBrokerFeeTypeId,
            Hud800LenderInspectionFeeTypeId,
            Hud800ProcessingFeeTypeId,
            Hud800UnderwritingFeeTypeId,
            Hud800WireTransferFeeTypeId,
            Hud900DailyInterestFeeTypeId,
            Hud900MortgageInsurancePremiumRecurringFeeTypeId,
            Hud900MortgageInsurancePremiumUpfrontFeeTypeId,
            Hud900HazardInsuranceFeeTypeId,
            Hud900VAFundingFeeTypeId,
            Hud900PropertyTaxFeeTypeId,
            Hud900SchoolTaxFeeTypeId,
            Hud900FloodInsuranceFeeTypeId,
            Hud900WindstormInsuranceFeeTypeId,
            Hud900CondoInsuranceFeeTypeId,
            Hud900HOADuesFeeTypeId,
            Hud900GroundRentFeeTypeId,
            Hud900OtherTax1FeeTypeId,
            Hud900OtherTax2FeeTypeId,
            Hud900OtherTax3FeeTypeId,
            Hud900OtherTax4FeeTypeId,
            Hud1000HazardInsuranceReserveFeeTypeId,
            Hud1000MortgageInsuranceReserveFeeTypeId,
            Hud1000TaxReserveFeeTypeId,
            Hud1000SchoolTaxFeeTypeId,
            Hud1000FloodInsuranceReserveFeeTypeId,
            Hud1000AggregateAdjustmentFeeTypeId,
            Hud1000WindStormInsuranceFeeTypeId,
            Hud1000CondoHo6InsuranceFeeTypeId,
            Hud1000HomeOwnerAssociationDuesFeeTypeId,
            Hud1000GroundRentFeeTypeId,
            Hud1000OtherTax1FeeTypeId,
            Hud1000OtherTax2FeeTypeId,
            Hud1000OtherTax3FeeTypeId,
            Hud1000OtherTax4FeeTypeId,
            Hud1100ClosingEscrowFeeTypeId,
            Hud1100OwnerTitleInsuranceFeeTypeId,
            Hud1100LenderTitleInsuranceFeeTypeId,
            Hud1100DocPreparationFeeTypeId,
            Hud1100NotaryFeeTypeId,
            Hud1100AttorneyFeeTypeId,
            Hud1200DeedFeeTypeId,
            Hud1200MortgageFeeTypeId,
            Hud1200ReleaseFeeTypeId,
            Hud1200CountyTaxStampsFeeTypeId,
            Hud1200StateTaxStampsFeeTypeId,
            Hud1300PestInspectionFeeTypeId
        };

        /// <summary>
        /// Private list of legacy GFE fee.
        /// </summary>
        private static List<FeeSetupClosingCostFee> legacyFeeList = new List<FeeSetupClosingCostFee>();

        /// <summary>
        /// Initializes static members of the <see cref="DefaultSystemClosingCostFee" /> class.
        /// </summary>
        static DefaultSystemClosingCostFee()
        {
            legacyFeeList = new List<FeeSetupClosingCostFee>() 
            {
                // 800 - ITEMS PAYABLE IN CONNECTION WITH LOAN.
                Create(Hud800LoanOriginationFeeTypeId, 801, false, false, "Loan origination fee", E_GfeSectionT.B1, E_IntegratedDisclosureSectionT.SectionA, E_AgentRoleT.Lender, true, true, E_LegacyGfeFieldT.sLOrigF),
                Create(Hud800OriginatorCompensationFeeTypeId, 801, false, false, "Originator compensation", E_GfeSectionT.B1, E_IntegratedDisclosureSectionT.SectionA, E_AgentRoleT.Broker, true, true, E_LegacyGfeFieldT.sGfeOriginatorCompF),
                Create(Hud800DiscountPointsFeeTypeId, 802, false, false, "Discount points", E_GfeSectionT.B2, E_IntegratedDisclosureSectionT.SectionA, E_AgentRoleT.Lender, true, true, E_LegacyGfeFieldT.sLDiscnt),
                Create(Hud800AppraisalFeeFeeTypeId, 804, false, false, "Appraisal fee", E_GfeSectionT.B3, E_IntegratedDisclosureSectionT.SectionB, E_AgentRoleT.Appraiser, false, true, E_LegacyGfeFieldT.sApprF),
                Create(Hud800CreditReportFeeTypeId, 805, false, false, "Credit report", E_GfeSectionT.B3, E_IntegratedDisclosureSectionT.SectionB, E_AgentRoleT.CreditReport, false, true, E_LegacyGfeFieldT.sCrF),
                Create(Hud800TaxServiceFeeTypeId, 806, false, false, "Tax service fee", E_GfeSectionT.B3, E_IntegratedDisclosureSectionT.SectionB, E_AgentRoleT.Lender, true, false, E_LegacyGfeFieldT.sTxServF),
                Create(Hud800FloodCertificationFeeTypeId, 807, false, false, "Flood certification", E_GfeSectionT.B3, E_IntegratedDisclosureSectionT.SectionB, E_AgentRoleT.Lender, false, true, E_LegacyGfeFieldT.sFloodCertificationF),
                Create(Hud800MortgageBrokerFeeTypeId, 808, false, false, "Mortgage broker fee", E_GfeSectionT.B1, E_IntegratedDisclosureSectionT.SectionA, E_AgentRoleT.Broker, true, true, E_LegacyGfeFieldT.sMBrokF),
                Create(Hud800LenderInspectionFeeTypeId, 809, false, false, "Lender's inspection fee", E_GfeSectionT.B1, E_IntegratedDisclosureSectionT.SectionA, E_AgentRoleT.Lender, true, true, E_LegacyGfeFieldT.sInspectF, E_ClosingCostFeeMismoFeeT.InspectionFee),
                Create(Hud800ProcessingFeeTypeId, 810, false, false, "Processing fee", E_GfeSectionT.B1, E_IntegratedDisclosureSectionT.SectionA, E_AgentRoleT.Lender, true, true, E_LegacyGfeFieldT.sProcF),
                Create(Hud800UnderwritingFeeTypeId, 811, false, false, "Underwriting fee", E_GfeSectionT.B1, E_IntegratedDisclosureSectionT.SectionA, E_AgentRoleT.Lender, true, true, E_LegacyGfeFieldT.sUwF),
                Create(Hud800WireTransferFeeTypeId, 812, false, false, "Wire transfer", E_GfeSectionT.B1, E_IntegratedDisclosureSectionT.SectionA, E_AgentRoleT.Lender, true, true, E_LegacyGfeFieldT.sWireF),
                ////Create(Hud800Custom1FeeTypeId, 813, false, false, string.Empty, E_GfeSectionT.B1, E_IntegratedDisclosureSectionT.SectionA, E_AgentRoleT.Lender, true, true, E_LegacyGfeFieldT.s800U1F, E_ClosingCostFeeMismoFeeT.Undefined),
                ////Create(Hud800Custom2FeeTypeId, 814, false, false, string.Empty, E_GfeSectionT.B1, E_IntegratedDisclosureSectionT.SectionA, E_AgentRoleT.Lender, true, true, E_LegacyGfeFieldT.s800U2F, E_ClosingCostFeeMismoFeeT.Undefined),
                ////Create(Hud800Custom3FeeTypeId, 815, false, false, string.Empty, E_GfeSectionT.B1, E_IntegratedDisclosureSectionT.SectionA, E_AgentRoleT.Lender, true, true, E_LegacyGfeFieldT.s800U3F, E_ClosingCostFeeMismoFeeT.Undefined),
                ////Create(Hud800Custom4FeeTypeId, 816, false, false, string.Empty, E_GfeSectionT.B1, E_IntegratedDisclosureSectionT.SectionA, E_AgentRoleT.Lender, true, true, E_LegacyGfeFieldT.s800U4F, E_ClosingCostFeeMismoFeeT.Undefined),
                ////Create(Hud800Custom5FeeTypeId, 817, false, false, string.Empty, E_GfeSectionT.B1, E_IntegratedDisclosureSectionT.SectionA, E_AgentRoleT.Lender, true, true, E_LegacyGfeFieldT.s800U5F, E_ClosingCostFeeMismoFeeT.Undefined),

                // 900 - ITEMS REQUIRED BY LENDER TO BE PAID IN ADVANCE.
                Create(Hud900DailyInterestFeeTypeId, 901, false, false, "Per-diem interest", E_GfeSectionT.B10, E_IntegratedDisclosureSectionT.SectionF, E_AgentRoleT.Lender, true, true, E_LegacyGfeFieldT.sIPia),
                Create(Hud900MortgageInsurancePremiumRecurringFeeTypeId, 902, false, false, "Prepaid Mortgage Insurance Premium", E_GfeSectionT.B3, E_IntegratedDisclosureSectionT.SectionF, E_AgentRoleT.Lender, true, true, E_LegacyGfeFieldT.sMipPia),
                Create(Hud900MortgageInsurancePremiumUpfrontFeeTypeId, 902, false, false, "Upfront Mortgage Insurance Premium", E_GfeSectionT.B3, E_IntegratedDisclosureSectionT.SectionB, E_AgentRoleT.Lender, true, true, E_LegacyGfeFieldT.sUpfrontMipPia),
                Create(Hud900HazardInsuranceFeeTypeId, 903, false, false, "Hazard insurance", E_GfeSectionT.B11, E_IntegratedDisclosureSectionT.SectionF, E_AgentRoleT.HazardInsurance, false, true, E_LegacyGfeFieldT.sHazInsPia),
                Create(Hud900VAFundingFeeTypeId, 905, false, false, "VA Funding Fee", E_GfeSectionT.B3, E_IntegratedDisclosureSectionT.SectionB, E_AgentRoleT.Lender, true, true, E_LegacyGfeFieldT.sVaFf),
                Create(Hud900PropertyTaxFeeTypeId, 907, false, false, "Property Taxes", E_GfeSectionT.B11, E_IntegratedDisclosureSectionT.SectionF, E_AgentRoleT.Lender, false, true, E_LegacyGfeFieldT.Undefined),
                Create(Hud900SchoolTaxFeeTypeId, 908, false, false, "School Taxes", E_GfeSectionT.B11, E_IntegratedDisclosureSectionT.SectionF, E_AgentRoleT.Lender, false, true, E_LegacyGfeFieldT.Undefined),
                Create(Hud900FloodInsuranceFeeTypeId, 909, false, false, "Flood Insurance", E_GfeSectionT.B11, E_IntegratedDisclosureSectionT.SectionF, E_AgentRoleT.Lender, false, true, E_LegacyGfeFieldT.Undefined),
                Create(Hud900WindstormInsuranceFeeTypeId, 914, false, false, "Windstorm Insurance", E_GfeSectionT.B11, E_IntegratedDisclosureSectionT.SectionF, E_AgentRoleT.Lender, false, true, E_LegacyGfeFieldT.Undefined),
                Create(Hud900CondoInsuranceFeeTypeId, 915, false, false, "Condo HO-6 Insurance", E_GfeSectionT.B11, E_IntegratedDisclosureSectionT.SectionF, E_AgentRoleT.Lender, false, true, E_LegacyGfeFieldT.Undefined),
                Create(Hud900HOADuesFeeTypeId, 916, false, false, "Homeowner's Association Dues", E_GfeSectionT.B11, E_IntegratedDisclosureSectionT.SectionF, E_AgentRoleT.Lender, false, true, E_LegacyGfeFieldT.Undefined),
                Create(Hud900GroundRentFeeTypeId, 917, false, false, "Ground Rent", E_GfeSectionT.B11, E_IntegratedDisclosureSectionT.SectionF, E_AgentRoleT.Lender, false, true, E_LegacyGfeFieldT.Undefined),
                Create(Hud900OtherTax1FeeTypeId, 918, false, false, "Other Tax Expense 1", E_GfeSectionT.B11, E_IntegratedDisclosureSectionT.SectionF, E_AgentRoleT.Lender, false, true, E_LegacyGfeFieldT.Undefined),
                Create(Hud900OtherTax2FeeTypeId, 919, false, false, "Other Tax Expense 2", E_GfeSectionT.B11, E_IntegratedDisclosureSectionT.SectionF, E_AgentRoleT.Lender, false, true, E_LegacyGfeFieldT.Undefined),
                Create(Hud900OtherTax3FeeTypeId, 920, false, false, "Other Tax Expense 3", E_GfeSectionT.B11, E_IntegratedDisclosureSectionT.SectionF, E_AgentRoleT.Lender, false, true, E_LegacyGfeFieldT.Undefined),
                Create(Hud900OtherTax4FeeTypeId, 921, false, false, "Other Tax Expense 4", E_GfeSectionT.B11, E_IntegratedDisclosureSectionT.SectionF, E_AgentRoleT.Lender, false, true, E_LegacyGfeFieldT.Undefined),

                // 1000 - RESERVES DEPOSITED WITH LENDER.
                Create(Hud1000HazardInsuranceReserveFeeTypeId, 1002, false, false, "Hazard insurance reserves", E_GfeSectionT.B9, E_IntegratedDisclosureSectionT.SectionG, E_AgentRoleT.Lender, false, true, E_LegacyGfeFieldT.sHazInsRsrv),
                Create(Hud1000MortgageInsuranceReserveFeeTypeId, 1003, false, false, "Mortgage insurance reserves", E_GfeSectionT.B9, E_IntegratedDisclosureSectionT.SectionG, E_AgentRoleT.Lender, true, true, E_LegacyGfeFieldT.sMInsRsrv),
                Create(Hud1000TaxReserveFeeTypeId, 1004, false, false, "Real estate tax reserves", E_GfeSectionT.B9, E_IntegratedDisclosureSectionT.SectionG, E_AgentRoleT.Lender, false, true, E_LegacyGfeFieldT.sRealETxRsrv),
                Create(Hud1000SchoolTaxFeeTypeId, 1005, false, false, "School tax reserves", E_GfeSectionT.B9, E_IntegratedDisclosureSectionT.SectionG, E_AgentRoleT.Lender, false, true, E_LegacyGfeFieldT.sSchoolTxRsrv),
                Create(Hud1000FloodInsuranceReserveFeeTypeId, 1006, false, false, "Flood insurance reserves", E_GfeSectionT.B9, E_IntegratedDisclosureSectionT.SectionG, E_AgentRoleT.Lender, false, true, E_LegacyGfeFieldT.sFloodInsRsrv),
                Create(Hud1000AggregateAdjustmentFeeTypeId, 1007, false, false, "Aggregate adjustment", E_GfeSectionT.B9, E_IntegratedDisclosureSectionT.SectionG, E_AgentRoleT.Lender, false, true, E_LegacyGfeFieldT.sAggregateAdjRsrv),
                Create(Hud1000WindStormInsuranceFeeTypeId, 1012, false, false, "Windstorm Insurance Reserves", E_GfeSectionT.B9, E_IntegratedDisclosureSectionT.SectionG, E_AgentRoleT.Lender, false, true, E_LegacyGfeFieldT.Undefined),
                Create(Hud1000CondoHo6InsuranceFeeTypeId, 1013, false, false, "Condo HO-6 Insurance Reserves", E_GfeSectionT.B9, E_IntegratedDisclosureSectionT.SectionG, E_AgentRoleT.Lender, false, true, E_LegacyGfeFieldT.Undefined),
                Create(Hud1000HomeOwnerAssociationDuesFeeTypeId, 1014, false, false, "Homeowner's Assocation Dues Reserves", E_GfeSectionT.B9, E_IntegratedDisclosureSectionT.SectionG, E_AgentRoleT.Lender, false, true, E_LegacyGfeFieldT.Undefined),
                Create(Hud1000GroundRentFeeTypeId, 1015, false, false, "Ground Rent Reserves", E_GfeSectionT.B9, E_IntegratedDisclosureSectionT.SectionG, E_AgentRoleT.Lender, false, true, E_LegacyGfeFieldT.Undefined),
                Create(Hud1000OtherTax1FeeTypeId, 1016, false, false, "Other Tax Expense 1 Reserves", E_GfeSectionT.B9, E_IntegratedDisclosureSectionT.SectionG, E_AgentRoleT.Lender, false, true, E_LegacyGfeFieldT.Undefined),
                Create(Hud1000OtherTax2FeeTypeId, 1017, false, false, "Other Tax Expense 2 Reserves", E_GfeSectionT.B9, E_IntegratedDisclosureSectionT.SectionG, E_AgentRoleT.Lender, false, true, E_LegacyGfeFieldT.Undefined),
                Create(Hud1000OtherTax3FeeTypeId, 1018, false, false, "Other Tax Expense 3 Reserves", E_GfeSectionT.B9, E_IntegratedDisclosureSectionT.SectionG, E_AgentRoleT.Lender, false, true, E_LegacyGfeFieldT.Undefined),
                Create(Hud1000OtherTax4FeeTypeId, 1019, false, false, "Other Tax Expense 4 Reserves", E_GfeSectionT.B9, E_IntegratedDisclosureSectionT.SectionG, E_AgentRoleT.Lender, false, true, E_LegacyGfeFieldT.Undefined),

                // 1100 - TITLE CHARGES
                Create(Hud1100ClosingEscrowFeeTypeId, 1102, true, false, "Closing/Escrow fee", E_GfeSectionT.B4, E_IntegratedDisclosureSectionT.SectionC, E_AgentRoleT.Title, false, true, E_LegacyGfeFieldT.sEscrowF),
                Create(Hud1100OwnerTitleInsuranceFeeTypeId, 1103, true, true, "Owner's title insurance", E_GfeSectionT.B5, E_IntegratedDisclosureSectionT.SectionH, E_AgentRoleT.Title, false, true, E_LegacyGfeFieldT.sOwnerTitleInsF),
                Create(Hud1100LenderTitleInsuranceFeeTypeId, 1104, true, false, "Lender's title insurance", E_GfeSectionT.B4, E_IntegratedDisclosureSectionT.SectionC, E_AgentRoleT.Title, false, true, E_LegacyGfeFieldT.sTitleInsF),
                Create(Hud1100DocPreparationFeeTypeId, 1109, true, false, "Document preparation fee", E_GfeSectionT.B4, E_IntegratedDisclosureSectionT.SectionC, E_AgentRoleT.Title, false, true, E_LegacyGfeFieldT.sDocPrepF),
                Create(Hud1100NotaryFeeTypeId, 1110, true, false, "Notary fees", E_GfeSectionT.B4, E_IntegratedDisclosureSectionT.SectionC, E_AgentRoleT.Title, false, true, E_LegacyGfeFieldT.sNotaryF),
                Create(Hud1100AttorneyFeeTypeId, 1111, true, false, "Attorney fees", E_GfeSectionT.B4, E_IntegratedDisclosureSectionT.SectionC, E_AgentRoleT.Title, false, true, E_LegacyGfeFieldT.sAttorneyF),
                ////Create(Hud1100Custom1FeeTypeId, 1112, true, false, string.Empty, E_GfeSectionT.B4, E_IntegratedDisclosureSectionT.SectionC, E_AgentRoleT.Title, false, true, E_LegacyGfeFieldT.sU1Tc, E_ClosingCostFeeMismoFeeT.Undefined),
                ////Create(Hud1100Custom2FeeTypeId, 1113, true, false, string.Empty, E_GfeSectionT.B4, E_IntegratedDisclosureSectionT.SectionC, E_AgentRoleT.Title, false, true, E_LegacyGfeFieldT.sU2Tc, E_ClosingCostFeeMismoFeeT.Undefined),
                ////Create(Hud1100Custom3FeeTypeId, 1114, true, false, string.Empty, E_GfeSectionT.B4, E_IntegratedDisclosureSectionT.SectionC, E_AgentRoleT.Title, false, true, E_LegacyGfeFieldT.sU3Tc, E_ClosingCostFeeMismoFeeT.Undefined),
                ////Create(Hud1100Custom4FeeTypeId, 1115, true, false, string.Empty, E_GfeSectionT.B4, E_IntegratedDisclosureSectionT.SectionC, E_AgentRoleT.Title, false, true, E_LegacyGfeFieldT.sU4Tc, E_ClosingCostFeeMismoFeeT.Undefined),

                // 1200 - GOVERNMENT RECORDING & TRANSFER CHARGES.
                Create(Hud1200DeedFeeTypeId, 1202, false, false, "Deed recording fee", E_GfeSectionT.B7, E_IntegratedDisclosureSectionT.SectionE, E_AgentRoleT.Other, false, true, E_LegacyGfeFieldT.sRecDeed),
                Create(Hud1200MortgageFeeTypeId, 1202, false, false, "Mortgage recording fee", E_GfeSectionT.B7, E_IntegratedDisclosureSectionT.SectionE, E_AgentRoleT.Other, false, true, E_LegacyGfeFieldT.sRecMortgage),
                Create(Hud1200ReleaseFeeTypeId, 1202, false, false, "Release recording fee", E_GfeSectionT.B7, E_IntegratedDisclosureSectionT.SectionE, E_AgentRoleT.Other, false, true, E_LegacyGfeFieldT.sRecRelease),
                Create(Hud1200CountyTaxStampsFeeTypeId, 1204, false, false, "County tax stamps", E_GfeSectionT.B8, E_IntegratedDisclosureSectionT.SectionE, E_AgentRoleT.Other, false, true, E_LegacyGfeFieldT.sCountyRtc),
                Create(Hud1200StateTaxStampsFeeTypeId, 1205, false, false, "State tax stamps", E_GfeSectionT.B8, E_IntegratedDisclosureSectionT.SectionE, E_AgentRoleT.Other, false, true, E_LegacyGfeFieldT.sStateRtc),
                ////Create(Hud1200Custom1FeeTypeId, 1206, false, false, string.Empty, E_GfeSectionT.B8, E_IntegratedDisclosureSectionT.SectionE, E_AgentRoleT.Other, false, true, E_LegacyGfeFieldT.sU1GovRtc, E_ClosingCostFeeMismoFeeT.Undefined),
                ////Create(Hud1200Custom2FeeTypeId, 1207, false, false, string.Empty, E_GfeSectionT.B8, E_IntegratedDisclosureSectionT.SectionE, E_AgentRoleT.Other, false, true, E_LegacyGfeFieldT.sU2GovRtc, E_ClosingCostFeeMismoFeeT.Undefined),
                ////Create(Hud1200Custom3FeeTypeId, 1208, false, false, string.Empty, E_GfeSectionT.B8, E_IntegratedDisclosureSectionT.SectionE, E_AgentRoleT.Other, false, true, E_LegacyGfeFieldT.sU3GovRtc, E_ClosingCostFeeMismoFeeT.Undefined),

                // 1300 - ADDITIONAL SETTLEMENT CHARGES.
                Create(Hud1300PestInspectionFeeTypeId, 1302, false, false, "Pest inspection", E_GfeSectionT.B6, E_IntegratedDisclosureSectionT.SectionC, E_AgentRoleT.PestInspection, false, true, E_LegacyGfeFieldT.sPestInspectF),
                ////Create(Hud1300Custom1FeeTypeId, 1303, false, false, string.Empty, E_GfeSectionT.B6, E_IntegratedDisclosureSectionT.SectionC, E_AgentRoleT.Other, false, true, E_LegacyGfeFieldT.sU1Sc, E_ClosingCostFeeMismoFeeT.Undefined),
                ////Create(Hud1300Custom2FeeTypeId, 1304, false, false, string.Empty, E_GfeSectionT.B6, E_IntegratedDisclosureSectionT.SectionC, E_AgentRoleT.Other, false, true, E_LegacyGfeFieldT.sU2Sc, E_ClosingCostFeeMismoFeeT.Undefined),
                ////Create(Hud1300Custom3FeeTypeId, 1305, false, false, string.Empty, E_GfeSectionT.B6, E_IntegratedDisclosureSectionT.SectionC, E_AgentRoleT.Other, false, true, E_LegacyGfeFieldT.sU3Sc, E_ClosingCostFeeMismoFeeT.Undefined),
                ////Create(Hud1300Custom4FeeTypeId, 1306, false, false, string.Empty, E_GfeSectionT.B6, E_IntegratedDisclosureSectionT.SectionC, E_AgentRoleT.Other, false, true, E_LegacyGfeFieldT.sU4Sc, E_ClosingCostFeeMismoFeeT.Undefined),
                ////Create(Hud1300Custom5FeeTypeId, 1307, false, false, string.Empty, E_GfeSectionT.B6, E_IntegratedDisclosureSectionT.SectionC, E_AgentRoleT.Other, false, true, E_LegacyGfeFieldT.sU5Sc, E_ClosingCostFeeMismoFeeT.Undefined),
            };
        }

        /// <summary>
        /// Gets a list of legacy GFE fee.
        /// </summary>
        /// <value>A list of legacy GFE fee.</value>
        public static IEnumerable<FeeSetupClosingCostFee> LegacyFeeList
        {
            get { return legacyFeeList; }
        }

        /// <summary>
        /// Check to see if the fee id is part of the legacy fee list.
        /// </summary>
        /// <param name="feeId">The fee id to check.</param>
        /// <returns>True if fee is part of legacy fee list.</returns>
        public static bool IsDefaultFee(Guid feeId)
        {
            if (feeId == Guid.Empty)
            {
                return false;
            }

            return DefaultSystemClosingCostFee.LegacyFeeTypeList.Contains(feeId);
        }

        /// <summary>
        /// Create a closing cost fee.
        /// </summary>
        /// <param name="feeTypeId">The fee type Id of the Fee.</param>
        /// <param name="hudLine">Line number on HUD-1.</param>
        /// <param name="isTitleFee">Whether or not a fee is considered a title fee on the Integrated Disclosures.</param>
        /// <param name="isOptional">Whether or not a fee should be marked as optional on the Integrated Disclosures.</param>
        /// <param name="description">Description of fee.</param>
        /// <param name="gfeSectionT">Block that the Fee appears on the GFE.</param>
        /// <param name="disclosureSectionT">The block that fee appears on the new GFE.</param>
        /// <param name="paidTo">Paid To Contact.</param>
        /// <param name="isApr">Is Fee APR.</param>
        /// <param name="isFhaAllowable">Is Fee allowed to be charged to the borrower under FHA rules.</param>
        /// <param name="legacyGfeField">Legacy LendingQB Fee Closing.</param>
        /// <param name="defaultMismoFeeType">A default value for the MISMO fee type if the type is user-modifiable.</param>
        /// <returns>A closing cost fee.</returns>
        public static FeeSetupClosingCostFee Create(
            Guid feeTypeId, 
            int hudLine, 
            bool isTitleFee, 
            bool isOptional, 
            string description, 
            E_GfeSectionT gfeSectionT, 
            E_IntegratedDisclosureSectionT disclosureSectionT, 
            E_AgentRoleT paidTo, 
            bool isApr, 
            bool isFhaAllowable, 
            E_LegacyGfeFieldT legacyGfeField,
            E_ClosingCostFeeMismoFeeT? defaultMismoFeeType = null) 
        {
            FeeSetupClosingCostFee fee = new FeeSetupClosingCostFee();

            // Explicitly set the UniqueIds for the Fees to Guid.Empty to differentiate them from all other Fees in the system. 
            fee.UniqueId = Guid.Empty;
            fee.ClosingCostFeeTypeId = feeTypeId;
            fee.HudLine = hudLine;
            fee.IsTitleFee = isTitleFee;
            fee.IsOptional = isOptional;
            fee.Description = description;
            fee.GfeSectionT = gfeSectionT;
            fee.IntegratedDisclosureSectionT = disclosureSectionT;
            fee.Beneficiary = paidTo;
            fee.IsApr = isApr;
            fee.IsFhaAllowable = isFhaAllowable;
            fee.LegacyGfeFieldT = legacyGfeField;

            E_ClosingCostFeeMismoFeeT? mismoFeeType = ClosingCostSetUtils.GetMismoFeeType(feeTypeId, false);
            if (mismoFeeType.HasValue)
            {
                fee.MismoFeeT = mismoFeeType.Value;
            }
            else if (defaultMismoFeeType.HasValue)
            {
                fee.MismoFeeT = defaultMismoFeeType.Value;
            }
            else
            {
                fee.MismoFeeT = E_ClosingCostFeeMismoFeeT.Undefined;
            }

            return fee;
        }

        /// <summary>
        /// The set of system-defined fee template <see cref="FeeSetupClosingCostFee"/> items.  This is initially for MISMO 3.4 import.
        /// </summary>
        public static class FeeTemplate
        {
            /// <summary>
            /// Unique Identifier for Section A fee template.
            /// </summary>
            public static readonly Guid FeeTemplateSectionAFeeTypeId = new Guid("2A7DC23C-0AB1-4688-A050-964C3530CD8B");

            /// <summary>
            /// Unique Identifier for Section B fee template.
            /// </summary>
            public static readonly Guid FeeTemplateSectionBFeeTypeId = new Guid("038B993E-66B1-495F-BA43-EECC68ED5392");

            /// <summary>
            /// Unique Identifier for Section C fee template.
            /// </summary>
            public static readonly Guid FeeTemplateSectionCFeeTypeId = new Guid("2325ACCB-F60E-4808-94B4-B3C348DE1FDF");

            /// <summary>
            /// Unique Identifier for Section E fee template.
            /// </summary>
            public static readonly Guid FeeTemplateSectionEFeeTypeId = new Guid("DF7D025B-4BF2-4021-9427-102B8335C5F7");

            /// <summary>
            /// Unique Identifier for Section F fee template.
            /// </summary>
            public static readonly Guid FeeTemplateSectionFFeeTypeId = new Guid("47CEC965-6402-424D-85E8-AADBF28DA880");

            /// <summary>
            /// Unique Identifier for Section H fee template.
            /// </summary>
            public static readonly Guid FeeTemplateSectionHFeeTypeId = new Guid("A720B7EC-41F5-448E-B005-75EB28BDE1B6");

            /// <summary>
            /// A map of <see cref="BaseClosingCostFee.ClosingCostFeeTypeId"/> to <see cref="BaseClosingCostFee.IntegratedDisclosureSectionT"/>.
            /// </summary>
            private static readonly IReadOnlyDictionary<Guid, E_IntegratedDisclosureSectionT> IntegratedDisclosureSectionByFeeType = new Dictionary<Guid, E_IntegratedDisclosureSectionT>
            {
                { FeeTemplateSectionAFeeTypeId, E_IntegratedDisclosureSectionT.SectionA },
                { FeeTemplateSectionBFeeTypeId, E_IntegratedDisclosureSectionT.SectionB },
                { FeeTemplateSectionCFeeTypeId, E_IntegratedDisclosureSectionT.SectionC },
                { FeeTemplateSectionEFeeTypeId, E_IntegratedDisclosureSectionT.SectionE },
                { FeeTemplateSectionFFeeTypeId, E_IntegratedDisclosureSectionT.SectionF },
                { FeeTemplateSectionHFeeTypeId, E_IntegratedDisclosureSectionT.SectionH },
            };

            /// <summary>
            /// A map of <see cref="E_ClosingCostFeeMismoFeeT"/> to <see cref="BaseClosingCostFee.ClosingCostFeeTypeId"/>.
            /// </summary>
            private static readonly Dictionary<Mismo3Specification.Version4Schema.FeeBase, Guid> DefaultImportMismoToFeeTypeMappings = new Dictionary<Mismo3Specification.Version4Schema.FeeBase, Guid>
            {
                { Mismo3Specification.Version4Schema.FeeBase.Item203KConsultantFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.Item203KDiscountOnRepairs, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.Item203KInspectionFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.Item203KPermits, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.Item203KSupplementalOriginationFee, FeeTemplateSectionAFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.Item203KTitleUpdate, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.ApplicationFee, FeeTemplateSectionAFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.AppraisalDeskReviewFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.AppraisalFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.AppraisalFieldReviewFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.AppraisalManagementCompanyFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.AsbestosInspectionFee, FeeTemplateSectionHFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.AssignmentPreparationFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.AssumptionFee, FeeTemplateSectionAFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.AttorneyFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.AutomatedUnderwritingFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.AVMFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.BankruptcyMonitoringFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.BondFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.BondReviewFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.CertificationFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.CommitmentFee, FeeTemplateSectionAFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.CondominiumAssociationDues, FeeTemplateSectionHFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.CondominiumAssociationSpecialAssessment, FeeTemplateSectionHFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.CooperativeAssociationDues, FeeTemplateSectionHFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.CooperativeAssociationSpecialAssessment, FeeTemplateSectionHFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.CopyOrFaxFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.CourierFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.CreditDisabilityInsurancePremium, FeeTemplateSectionHFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.CreditLifeInsurancePremium, FeeTemplateSectionHFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.CreditPropertyInsurancePremium, FeeTemplateSectionHFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.CreditReportFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.CreditUnemploymentInsurancePremium, FeeTemplateSectionHFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.DebtCancellationInsurancePremium, FeeTemplateSectionHFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.DebtSuspensionInsurancePremium, FeeTemplateSectionHFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.DeedPreparationFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.DisasterInspectionFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.DiscountOnRepairsFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.DocumentaryStampFee, FeeTemplateSectionEFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.DocumentPreparationFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.DownPaymentProtectionFee, FeeTemplateSectionHFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.DryWallInspectionFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.ElectricalInspectionFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.ElectronicDocumentDeliveryFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.EnvironmentalInspectionFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.EscrowHoldbackFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.EscrowServiceFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.EscrowWaiverFee, FeeTemplateSectionAFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.FilingFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.FloodCertification, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.FoundationInspectionFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.HeatingCoolingInspectionFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.HighCostMortgageCounselingFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.HomeInspectionFee, FeeTemplateSectionHFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.HomeownersAssociationDues, FeeTemplateSectionHFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.HomeownersAssociationServiceFee, FeeTemplateSectionHFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.HomeownersAssociationSpecialAssessment, FeeTemplateSectionHFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.HomeWarrantyFee, FeeTemplateSectionHFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.LeadInspectionFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.LendersAttorneyFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.LoanOriginationFee, FeeTemplateSectionAFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.LoanOriginatorCompensation, FeeTemplateSectionAFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.ManualUnderwritingFee, FeeTemplateSectionAFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.ManufacturedHousingInspectionFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.ManufacturedHousingProcessingFee, FeeTemplateSectionAFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.MERSRegistrationFee, FeeTemplateSectionAFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.MIInitialPremium, FeeTemplateSectionFFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.MIUpfrontPremium, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.ModificationFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.MoldInspectionFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.MortgageBrokerFee, FeeTemplateSectionAFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.MortgageSurchargeCountyOrParish, FeeTemplateSectionEFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.MortgageSurchargeMunicipal, FeeTemplateSectionEFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.MortgageSurchargeState, FeeTemplateSectionEFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.MortgageTaxCreditServiceFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.MultipleLoansClosingFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.MunicipalLienCertificateFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.NotaryFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.Other, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.PayoffRequestFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.PestInspectionFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.PlumbingInspectionFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.PowerOfAttorneyPreparationFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.PowerOfAttorneyRecordingFee, FeeTemplateSectionEFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.PreclosingVerificationControlFee, FeeTemplateSectionAFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.ProcessingFee, FeeTemplateSectionAFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.ProgramGuaranteeFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.PropertyInspectionWaiverFee, FeeTemplateSectionAFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.RadonInspectionFee, FeeTemplateSectionHFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.RateLockFee, FeeTemplateSectionAFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.RealEstateCommissionBuyersBroker, FeeTemplateSectionHFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.RealEstateCommissionSellersBroker, FeeTemplateSectionHFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.ReconveyanceFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.ReconveyanceTrackingFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.RecordingFeeForAssignment, FeeTemplateSectionEFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.RecordingFeeForDeed, FeeTemplateSectionEFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.RecordingFeeForMortgage, FeeTemplateSectionEFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.RecordingFeeForMunicipalLienCertificate, FeeTemplateSectionEFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.RecordingFeeForOtherDocument, FeeTemplateSectionEFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.RecordingFeeForRelease, FeeTemplateSectionEFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.RecordingFeeForSubordination, FeeTemplateSectionEFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.RecordingFeeTotal, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.RecordingServiceFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.RedrawFee, FeeTemplateSectionAFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.ReinspectionFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.RenovationConsultantFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.RepairsFee, FeeTemplateSectionHFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.RoofInspectionFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.SepticInspectionFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.SettlementFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.SigningAgentFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.SmokeDetectorInspectionFee, FeeTemplateSectionHFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.StateTitleInsuranceFee, FeeTemplateSectionEFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.StructuralInspectionFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.SubordinationFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.SurveyFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.TaxServiceFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.TaxStampForCityDeed, FeeTemplateSectionEFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.TaxStampForCityMortgage, FeeTemplateSectionEFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.TaxStampForCountyDeed, FeeTemplateSectionEFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.TaxStampForCountyMortgage, FeeTemplateSectionEFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.TaxStampForStateDeed, FeeTemplateSectionEFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.TaxStampForStateMortgage, FeeTemplateSectionEFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.TaxStatusResearchFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.TemporaryBuydownAdministrationFee, FeeTemplateSectionAFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.TemporaryBuydownPoints, FeeTemplateSectionAFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.TitleAbstractFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.TitleBorrowerClosingProtectionLetterFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.TitleCertificationFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.TitleClosingCoordinationFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.TitleClosingFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.TitleClosingProtectionLetterFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.TitleDocumentPreparationFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.TitleEndorsementFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.TitleExaminationFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.TitleFinalPolicyShortFormFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.TitleInsuranceBinderFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.TitleInsuranceFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.TitleLendersCoveragePremium, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.TitleNotaryFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.TitleOwnersCoveragePremium, FeeTemplateSectionHFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.TitleServicesFeeTotal, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.TitleServicesSalesTax, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.TitleUnderwritingIssueResolutionFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.TransferTaxTotal, FeeTemplateSectionEFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.UnderwritingFee, FeeTemplateSectionAFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.USDARuralDevelopmentGuaranteeFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.VAFundingFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.VerificationOfAssetsFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.VerificationOfEmploymentFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.VerificationOfIncomeFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.VerificationOfResidencyStatusFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.VerificationOfTaxpayerIdentificationFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.VerificationOfTaxReturnFee, FeeTemplateSectionBFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.WaterTestingFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.WellInspectionFee, FeeTemplateSectionCFeeTypeId },
                { Mismo3Specification.Version4Schema.FeeBase.WireTransferFee, FeeTemplateSectionCFeeTypeId },
            };

            /// <summary>
            /// Gets the default set of mappings for MISMO fee types to lender fee types.
            /// </summary>
            public static IReadOnlyDictionary<Mismo3Specification.Version4Schema.FeeBase, Guid> DefaultMismoImportFeeTypeMappings
            {
                get { return new System.Collections.ObjectModel.ReadOnlyDictionary<Mismo3Specification.Version4Schema.FeeBase, Guid>(DefaultImportMismoToFeeTypeMappings); }
            }

            /// <summary>
            /// Gets the list of system-defined template fees.
            /// </summary>
            public static FeeSetupClosingCostFee[] SystemFeeList
            {
                get
                {
                    return new[]
                    {
                        new FeeSetupClosingCostFee
                        {
                            ClosingCostFeeTypeId = FeeTemplateSectionAFeeTypeId,
                            HudLine = 800,
                            IsTitleFee = false,
                            IsOptional = false,
                            Description = "Section A Fee Template",
                            GfeSectionT = E_GfeSectionT.B1,
                            IntegratedDisclosureSectionT = GetIntegratedDisclosureSection(FeeTemplateSectionAFeeTypeId),
                            IsApr = true,
                            IsFhaAllowable = true,
                            Beneficiary = E_AgentRoleT.Lender,
                            Dflp = false,
                            IsThirdParty = false,
                            IsAffiliate = false,
                            CanShop = false,
                            MismoFeeT = GetMismoFeeType(FeeTemplateSectionAFeeTypeId),
                        },
                        new FeeSetupClosingCostFee
                        {
                            ClosingCostFeeTypeId = FeeTemplateSectionBFeeTypeId,
                            HudLine = 800,
                            IsTitleFee = false,
                            IsOptional = false,
                            Description = "Section B Fee Template",
                            GfeSectionT = E_GfeSectionT.B3,
                            IntegratedDisclosureSectionT = GetIntegratedDisclosureSection(FeeTemplateSectionBFeeTypeId),
                            IsApr = true,
                            IsFhaAllowable = true,
                            Beneficiary = E_AgentRoleT.Other,
                            Dflp = false,
                            IsThirdParty = true,
                            IsAffiliate = false,
                            CanShop = false,
                            MismoFeeT = GetMismoFeeType(FeeTemplateSectionBFeeTypeId),
                        },
                        new FeeSetupClosingCostFee
                        {
                            ClosingCostFeeTypeId = FeeTemplateSectionCFeeTypeId,
                            HudLine = 1300,
                            IsTitleFee = true,
                            IsOptional = false,
                            Description = "Section C Fee Template",
                            GfeSectionT = E_GfeSectionT.B6,
                            IntegratedDisclosureSectionT = GetIntegratedDisclosureSection(FeeTemplateSectionCFeeTypeId),
                            IsApr = false,
                            IsFhaAllowable = true,
                            Beneficiary = E_AgentRoleT.Other,
                            Dflp = false,
                            IsThirdParty = true,
                            IsAffiliate = false,
                            CanShop = true,
                            MismoFeeT = GetMismoFeeType(FeeTemplateSectionCFeeTypeId),
                        },
                        new FeeSetupClosingCostFee
                        {
                            ClosingCostFeeTypeId = FeeTemplateSectionEFeeTypeId,
                            HudLine = 1200,
                            IsTitleFee = false,
                            IsOptional = false,
                            Description = "Section E Fee Template",
                            GfeSectionT = E_GfeSectionT.B8,
                            IntegratedDisclosureSectionT = GetIntegratedDisclosureSection(FeeTemplateSectionEFeeTypeId),
                            IsApr = false,
                            IsFhaAllowable = true,
                            Beneficiary = E_AgentRoleT.Other,
                            Dflp = false,
                            IsThirdParty = true,
                            IsAffiliate = false,
                            CanShop = false,
                            MismoFeeT = GetMismoFeeType(FeeTemplateSectionEFeeTypeId),
                        },
                        new FeeSetupClosingCostFee
                        {
                            ClosingCostFeeTypeId = FeeTemplateSectionFFeeTypeId,
                            HudLine = 900,
                            IsTitleFee = false,
                            IsOptional = false,
                            Description = "Section F Fee Template",
                            GfeSectionT = E_GfeSectionT.B11,
                            IntegratedDisclosureSectionT = GetIntegratedDisclosureSection(FeeTemplateSectionFFeeTypeId),
                            IsApr = false,
                            IsFhaAllowable = true,
                            Beneficiary = E_AgentRoleT.Other,
                            Dflp = false,
                            IsThirdParty = true,
                            IsAffiliate = false,
                            CanShop = false,
                            MismoFeeT = GetMismoFeeType(FeeTemplateSectionFFeeTypeId),
                        },
                        new FeeSetupClosingCostFee
                        {
                            ClosingCostFeeTypeId = FeeTemplateSectionHFeeTypeId,
                            HudLine = 1300,
                            IsTitleFee = false,
                            IsOptional = false,
                            Description = "Section H Fee Template",
                            GfeSectionT = E_GfeSectionT.B6,
                            IntegratedDisclosureSectionT = GetIntegratedDisclosureSection(FeeTemplateSectionHFeeTypeId),
                            IsApr = false,
                            IsFhaAllowable = true,
                            Beneficiary = E_AgentRoleT.Other,
                            Dflp = false,
                            IsThirdParty = true,
                            IsAffiliate = false,
                            CanShop = true,
                            MismoFeeT = GetMismoFeeType(FeeTemplateSectionHFeeTypeId),
                        },
                    };
                }
            }

            /// <summary>
            /// Gets a value indicating if the fee type is for a system fee template.
            /// </summary>
            /// <param name="feeTypeId">The <see cref="BaseClosingCostFee.ClosingCostFeeTypeId"/> for the fee.</param>
            /// <returns><see langword="true"/> if the fee type is a system template fee type; <see langword="false"/> otherwise.</returns>
            public static bool IsFeeTemplateSystemFeeType(Guid feeTypeId)
            {
                return IntegratedDisclosureSectionByFeeType.ContainsKey(feeTypeId);
            }

            /// <summary>
            /// Gets the integrated disclosure section for a system-defined fee template.
            /// </summary>
            /// <param name="feeTypeId">The <see cref="BaseClosingCostFee.ClosingCostFeeTypeId"/> for the fee.</param>
            /// <returns>The integrated disclosure section for the fee.</returns>
            /// <exception cref="KeyNotFoundException"><paramref name="feeTypeId"/> does not refer to a system-defined fee template.</exception>
            public static E_IntegratedDisclosureSectionT GetIntegratedDisclosureSection(Guid feeTypeId)
            {
                return IntegratedDisclosureSectionByFeeType[feeTypeId];
            }

            /// <summary>
            /// Gets the MISMO type for a system-defined fee template.
            /// </summary>
            /// <param name="feeTypeId">The <see cref="BaseClosingCostFee.ClosingCostFeeTypeId"/> for the fee.</param>
            /// <returns>The MISMO type for the fee.</returns>
            /// <exception cref="KeyNotFoundException"><paramref name="feeTypeId"/> does not refer to a system-defined fee template.</exception>
            public static E_ClosingCostFeeMismoFeeT GetMismoFeeType(Guid feeTypeId)
            {
                if (IntegratedDisclosureSectionByFeeType.ContainsKey(feeTypeId))
                {
                    return E_ClosingCostFeeMismoFeeT.Undefined;
                }

                throw new KeyNotFoundException(); // Copying the behavior of GetIntegratedDisclosureSection, even though this isn't a dictionary yet
            }
        }
    }
}