﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess.PathDispatch;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// Wrapper class to provide an object that LoanClosingCostFee can delegate
    /// selecting a particular payment when locating data using paths.
    /// </summary>
    public class LoanClosingCostFeePaymentCollection : IPathResolvable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoanClosingCostFeePaymentCollection"/> class.
        /// </summary>
        /// <param name="fee">The fee to wrap.</param>
        public LoanClosingCostFeePaymentCollection(LoanClosingCostFee fee)
        {
            this.Fee = fee;
        }

        /// <summary>
        /// Gets the type of the payments in this collection.
        /// </summary>
        public Type PaymentType
        {
            get
            {
                if (this.Fee is BorrowerClosingCostFee)
                {
                    return typeof(BorrowerClosingCostFeePayment);
                }
                else if (this.Fee is SellerClosingCostFee)
                {
                    return typeof(SellerClosingCostFeePayment);
                }

                throw new UnhandledCaseException(this.Fee.GetType().ToString());
            }
        }

        /// <summary>
        /// Gets the <see cref="LoanClosingCostFee"/> instance being wrapped.
        /// </summary>
        internal LoanClosingCostFee Fee { get; private set; }

        /// <summary>
        /// Returns an enumerable of all payments in the collection.
        /// </summary>
        /// <returns>An enumerable of all payments in the collection.</returns>
        public IEnumerable<LoanClosingCostFeePayment> GetPayments()
        {
            return this.Fee.Payments;
        }

        /// <summary>
        /// Returns the value specified by the element.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns>The value specified by the element.</returns>
        public object GetElement(IDataPathElement element)
        {
            if (!(element is DataPathSelectionElement))
            {
                throw new ArgumentException("LoanClosingCostFeePaymentCollection can only handle DataPathSelectionElements, but passed element of type " + element.GetType());
            }

            var selectorElement = (DataPathSelectionElement)element;

            int paymentIndex;

            if (int.TryParse(selectorElement.Name, out paymentIndex))
            {
                return this.Fee.Payments.Skip(paymentIndex).First();
            }
            else
            {
                throw new ArgumentException("Could not parse payment argument to a valid index");
            }
        }
    }
}
