﻿// <copyright file="LoanClosingCostFormula.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   7/9/2015
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;

    /// <summary>
    /// The base class for all closing cost formulas that belong in a loan. DO NOT SERIALIZE/DESERIALIZE THIS CLASS DIRECTLY.
    /// </summary>
    [DataContract]
    [KnownType(typeof(SellerClosingCostFormula))]
    [KnownType(typeof(BorrowerClosingCostFormula))]
    public abstract class LoanClosingCostFormula : BaseClosingCostFormula
    {
        /// <summary>
        /// The internal conversion that use to serialize money, percent as string in JSON. Use WebForm as default.
        /// </summary>
        private LosConvert internalLosConvert = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanClosingCostFormula" /> class.
        /// </summary>
        public LoanClosingCostFormula()
        {
            this.Initialization();
            this.NumberOfPeriods = 1;
        }

        /// <summary>
        /// Gets or sets the type of formula. Also ensures the correct formula type.
        /// </summary>
        /// <value>The type of formula.</value>
        public override E_ClosingCostFeeFormulaT FormulaT
        {
            get
            {
                if (this.ParentClosingCostFee != null && this.ParentClosingCostFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId &&
                    this.ParentClosingCostSet != null && this.ParentClosingCostSet.HasDataLoanAssociate)
                {
                    if (this.ParentClosingCostSet.DataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)
                    {
                        base.FormulaT = E_ClosingCostFeeFormulaT.sOriginatorCompensationTotalAmount;
                    }
                    else
                    {
                        base.FormulaT = E_ClosingCostFeeFormulaT.Full;
                    }
                }

                return base.FormulaT;
            }

            set
            {
                base.FormulaT = value;

                this.EnsureFormulaType();
            }
        }

        /// <summary>
        /// Gets or sets the percent amount.
        /// </summary>
        /// <value>The percent amount.</value>
        public virtual decimal Percent
        {
            get
            {
                return this.LosConvertForJson.ToRate(this.InternalPercent);
            }

            set
            {
                this.InternalPercent = this.LosConvertForJson.ToRateString(value);
            }
        }

        /// <summary>
        /// Gets or sets the display of percent amount.
        /// </summary>
        /// <value>The display of percent amount.</value>
        public string Percent_rep
        {
            get
            {
                return this.LosConvert.ToRateString(this.Percent);
            }

            set
            {
                this.Percent = this.LosConvert.ToRate(value);
            }
        }

        /// <summary>
        /// Gets or sets the percent base amount type.
        /// </summary>
        /// <value>The percent base amount type.</value>
        public virtual E_PercentBaseT PercentBaseT
        {
            get
            {
                if (this.FormulaT == E_ClosingCostFeeFormulaT.sLOrigFPc)
                {
                    if (this.ParentClosingCostSet != null && this.ParentClosingCostSet.HasDataLoanAssociate)
                    {
                        this.InternalPercentBaseT = this.ParentClosingCostSet.GetPercentBaseTForLoanOriginationFee();
                    }
                }

                return this.InternalPercentBaseT;
            }

            set
            {
                this.InternalPercentBaseT = value;
            }
        }

        /// <summary>
        /// Gets or sets the minimum base amount.
        /// </summary>
        /// <value>The minimum base amount.</value>
        public virtual decimal BaseAmount
        {
            get
            {
                return this.LosConvertForJson.ToMoney(this.InternalBaseAmount);
            }

            set
            {
                this.InternalBaseAmount = this.LosConvertForJson.ToMoneyString(value, FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// Gets or sets the display of minimum base amount.
        /// </summary>
        /// <value>The display of minimum base amount.</value>
        public string BaseAmount_rep
        {
            get
            {
                return this.LosConvert.ToMoneyString(this.BaseAmount, FormatDirection.ToRep);
            }

            set
            {
                this.BaseAmount = this.LosConvert.ToMoney(value);
            }
        }

        /// <summary>
        /// Gets or sets the number of periods.
        /// </summary>
        /// <value>The number of periods.</value>
        public virtual int NumberOfPeriods
        {
            get
            {
                return this.LosConvertForJson.ToCount(this.InternalNumberOfPeriods);
            }

            set
            {
                this.InternalNumberOfPeriods = this.LosConvertForJson.ToCountString(value);
            }
        }

        /// <summary>
        /// Gets or sets the string representation of the number of periods.
        /// </summary>
        /// <value>The string representation of the number of periods.</value>
        public string NumberOfPeriods_rep
        {
            get
            {
                return this.LosConvert.ToCountString(this.NumberOfPeriods);
            }

            set
            {
                this.NumberOfPeriods = this.LosConvert.ToCount(value);
            }
        }

        /// <summary>
        /// Gets or sets the amount that multiply with percent.
        /// </summary>
        /// <value>The amount that multiply with percent.</value>
        public virtual decimal PercentTotalAmount
        {
            get
            {
                if (this.ParentClosingCostSet != null && this.ParentClosingCostSet.HasDataLoanAssociate)
                {
                    this.SavedPercentTotalAmount = this.ParentClosingCostSet.GetPercentBaseAmount(this.PercentBaseT);
                }

                return this.SavedPercentTotalAmount;
            }

            set
            {
                this.SavedPercentTotalAmount = value;
            }
        }

        /// <summary>
        /// Gets the display representation of the amount that multiply with percent.
        /// </summary>
        /// <value>The display representation of the amount that multiply with percent.</value>
        public string PercentTotalAmount_rep
        {
            get
            {
                return this.LosConvert.ToMoneyString(this.PercentTotalAmount, FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// Gets the total amount base on (Percent * PercentTotalAmount + BaseAmount) * NumberOfPeriods.
        /// </summary>
        /// <value>The total amount base on (Percent * PercentTotalAmount + BaseAmount) * NumberOfPeriods.</value>
        public decimal TotalAmount
        {
            get
            {
                // 10/31/2014 dd - The formula is (Percent * PercentTotalAmount + BaseAmount) * NumberOfPeriods.
                // To optimize the calculation, I check for zero befure perform multiply.
                int periods = this.NumberOfPeriods;
                if (periods == 0)
                {
                    return 0;
                }

                decimal percentAmount = 0;
                decimal percent = this.Percent;
                if (percent != 0)
                {
                    percentAmount = (percent * this.PercentTotalAmount) / 100;
                }

                // OPM 247069 - Rounding should happen in decimal field NOT in rep.
                // NOTE: Rounding here prevents issues resulting from the incorrect rounding of sIPia in methods that sum together multiple fees.
                decimal unroundedAmount = (percentAmount + this.BaseAmount) * periods;
                MidpointRounding roundingMode = this.FormulaT == E_ClosingCostFeeFormulaT.sIPia ? MidpointRounding.AwayFromZero : MidpointRounding.ToEven;

                return Math.Round(unroundedAmount, 2, roundingMode);
            }
        }

        /// <summary>
        /// Gets the display representation of the total amount.
        /// </summary>
        /// <value>The display representation of the total amount.</value>
        public string TotalAmount_rep
        {
            get
            {
                return this.LosConvert.ToMoneyString(this.TotalAmount, FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// Gets or sets the conversion tool for going to and from JSON. Different than what is used for display purposes. 
        /// </summary>
        /// <value>The conversion for going to and from JSON.</value>
        protected LosConvert LosConvertForJson
        {
            get
            {
                if (this.internalLosConvert == null)
                {
                    this.internalLosConvert = new LosConvert();
                }

                return this.internalLosConvert;
            }

            set
            {
                this.internalLosConvert = value;
            }
        }

        /// <summary>
        /// Gets or sets the parent closing cost fee. The parent closing cost fee must be a LoanClosingCostFee.
        /// </summary>
        /// <value>The parent closing cost fee.</value>
        protected LoanClosingCostFee ParentClosingCostFee
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the parent closing cost set of the parent closing cost fee or null if the fee is null.
        /// </summary>
        /// <value>The parent closing cost set of the parent closing cost fee or null if the fee is null.</value>
        protected LoanClosingCostSet ParentClosingCostSet
        {
            get
            {
                if (this.ParentClosingCostFee != null)
                {
                    return this.ParentClosingCostFee.ParentClosingCostSet;
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Gets or sets the internal amount that will be use to multiple with percent. DO NOT use this member directly.
        /// It must go through property PercentTotalAmount.
        /// </summary>
        /// <value>The internal amount that will be use to multiple with percent.</value>
        protected decimal SavedPercentTotalAmount
        {
            get
            {
                return this.LosConvertForJson.ToMoney(this.InternalPercentTotalAmount);
            }

            set
            {
                this.InternalPercentTotalAmount = this.LosConvertForJson.ToMoneyString(value, FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// Gets or sets the internal amount that will be use to multiple with percent. DO NOT use this member directly.
        /// It must go through property PercentTotalAmount.
        /// </summary>
        /// <value>The internal amount that will be use to multiple with percent.</value>
        [DataMember(Name = "pb")]
        protected string InternalPercentTotalAmount
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the minimum base amount. This value is stored as string that have friendly display format $x,xxx.xx. This value
        /// can display directly to Java script UI.
        /// </summary>
        /// <value>The internal base amount.</value>
        [DataMember(Name = "base")]
        protected string InternalBaseAmount
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the percent amount. This value is stored as string that have friendly display. This value can
        /// display directly to Java script UI.
        /// </summary>
        /// <value>The internal percent.</value>
        [DataMember(Name = "p")]
        protected string InternalPercent
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the number of periods. This value is stored as string that have friendly display.
        /// </summary>
        /// <value>The internal number of periods.</value>
        [DataMember(Name = "period")]
        protected string InternalNumberOfPeriods
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the percent type.
        /// </summary>
        /// <value>The internal percent base type.</value>
        [DataMember(Name = "pt")]
        protected E_PercentBaseT InternalPercentBaseT
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the conversion format.
        /// </summary>
        /// <value>The base conversion format.</value>
        protected virtual LosConvert LosConvert
        {
            get
            {
                if (this.ParentClosingCostSet != null)
                {
                    return this.ParentClosingCostSet.LosConvert;
                }
                else
                {
                    return this.LosConvertForJson;
                }
            }
        }

        /// <summary>
        /// Set the closing cost fee to this formula.
        /// </summary>
        /// <param name="o">Closing cost fee.</param>
        internal void SetClosingCostFee(LoanClosingCostFee o)
        {
            this.ParentClosingCostFee = o;
        }

        /// <summary>
        /// Perform initialization after object is deserialized.
        /// </summary>
        /// <param name="context">Serialized context.</param>
        [OnDeserialized]
        internal void OnDeserialized(StreamingContext context)
        {
            this.Initialization();
        }

        /// <summary>
        /// Perform initialization.
        /// </summary>
        internal void Initialization()
        {
            this.EnsureCorrectness();
        }

        /// <summary>
        /// Since JSON serialization/deserialization set to private member variables directly which bypass some logic in the getter / setter.
        /// This function is a centralize of the logics on the absolute fact or guarantee of this object.
        /// </summary>
        internal virtual void EnsureCorrectness()
        {
            if (this.internalLosConvert == null)
            {
                this.internalLosConvert = new LosConvert();
            }

            this.EnsureFormulaType();
        }

        /// <summary>
        /// Checks if, for this formula's formula type, all the other fields are correct.
        /// </summary>
        protected virtual void EnsureFormulaType()
        {
            if (this.FormulaT == E_ClosingCostFeeFormulaT.PercentOnly)
            {
                this.InternalBaseAmount = string.Empty;
            }
            else if (this.FormulaT == E_ClosingCostFeeFormulaT.MinBaseOnly)
            {
                this.InternalPercent = string.Empty;
            }
        }

        /// <summary>
        /// Make sure everything is correct before serializing.
        /// </summary>
        /// <param name="context">Serialized context.</param>
        [OnSerializing]
        private void SetValuesOnSerializing(StreamingContext context)
        {
            this.EnsureCorrectness();
        }
    }
}
