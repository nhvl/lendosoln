﻿// <copyright file="FeesForContact.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Scott Kibler
//    Date:   7/30/2015
// </summary>
namespace DataAccess
{
    using System.Collections.Generic;

    /// <summary>
    /// Container class for Fees assigned to a particular contact.  Does not enforce the assignment.
    /// </summary>
    public class FeesForContact
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FeesForContact" /> class.
        /// </summary>
        public FeesForContact()
        {
            this.Fees = new List<BorrowerClosingCostFee>();
        }

        /// <summary>
        /// Gets or sets the list of fees tied to a particular contact.  You have to make sure the fees are all assigned to that contact.
        /// </summary>
        /// <value>The list of fees tied to the particular contact.</value>
        public List<BorrowerClosingCostFee> Fees { get; set; }

        /// <summary>
        /// Gets or sets the contact assigned to the list of fees.
        /// </summary>
        /// <value>The contact assigned tot he list of fees.</value>
        public CAgentFields Contact { get; set; }
    }
}
