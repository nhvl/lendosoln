﻿// <copyright file="ClosingCostSetUtils.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   6/22/2015
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    
    /// <summary>
    /// Class containing static methods and constants for use in all closing cost classes.
    /// </summary>
    public static class ClosingCostSetUtils
    {
        /// <summary>
        /// Static section name for HUD 800 section.
        /// </summary>
        public const string Hud800ItemsPayable = "800 - Items Payable In Connection With Loan";

        /// <summary>
        /// Static section name for HUD 900 section.
        /// </summary>
        public const string Hud900ItemsRequiredByLenderToBePaid = "900 - Items Required By Lender To Be Paid In Advance";

        /// <summary>
        /// Static section name for HUD 1000 section.
        /// </summary>
        public const string Hud1000ReservesDeposited = "1000 - Reserves Deposited With Lender";

        /// <summary>
        /// Static section name for HUD 1100 section.
        /// </summary>
        public const string Hud1100Title = "1100 - Title Charges";

        /// <summary>
        /// Static section name for HUD 1200 section.
        /// </summary>
        public const string Hud1200Government = "1200 - Government Recording And Transfer Charges";

        /// <summary>
        /// Static section name for HUD 1300 section.
        /// </summary>
        public const string Hud1300AdditionalSettlementCharge = "1300 - Additional Settlement Charges";

        /// <summary>
        /// Static section name for TRID Section A.
        /// </summary>
        public const string SectionAOriginationCharges = "A - Origination Charges";

        /// <summary>
        /// Static section name for TRID Section B.
        /// </summary>
        public const string SectionBServiceYouCannotShopFor = "B - Services You Cannot Shop For";

        /// <summary>
        /// Static section name for Section B for combined estimate.
        /// </summary>
        public const string SectionBForCombinedLenderTypeEstimate = "B - Services You Did Not Shop For";

        /// <summary>
        /// Static section name for TRID Section C.
        /// </summary>
        public const string SectionCServiceYouCanShopFor = "C - Services You Can Shop For";

        /// <summary>
        /// Static section name for Section C for combined estimate.
        /// </summary>
        public const string SectionCForCombinedLenderTypeEstimate = "C - Services You Did Shop For";

        /// <summary>
        /// Static section name for TRID Sections B or C.
        /// </summary>
        public const string SectionBorC = "B/C - Third Party Services";

        /// <summary>
        /// Static section name for TRID Section E.
        /// </summary>
        public const string SectionETaxes = "E - Taxes And Other Government Fees";

        /// <summary>
        /// Static section name for TRID Section F.
        /// </summary>
        public const string SectionFPrepaids = "F - Prepaids";

        /// <summary>
        /// Static section name for TRID Section G.
        /// </summary>
        public const string SectionGInitialEscrow = "G - Initial Escrow Payment At Closing";

        /// <summary>
        /// Static section name for TRID Section H.
        /// </summary>
        public const string SectionHOther = "H - Other";

        /// <summary>
        /// The first acceptable hudline for section 800.
        /// </summary>
        public const int Start800Hudline = 800;

        /// <summary>
        /// The last acceptable hudline for section 800.
        /// </summary>
        public const int End800Hudline = 899;

        /// <summary>
        /// The first acceptable hudline for section 900.
        /// </summary>
        public const int Start900Hudline = 900;

        /// <summary>
        /// The last acceptable hudline for section 900.
        /// </summary>
        public const int End900Hudline = 999;

        /// <summary>
        /// The first acceptable hudline for section 1000.
        /// </summary>
        public const int Start1000Hudline = 1000;

        /// <summary>
        /// The end acceptable hudline for section 1000.
        /// </summary>
        public const int End1000Hudline = 1099;

        /// <summary>
        /// The first acceptable hudline for section 1100.
        /// </summary>
        public const int Start1100Hudline = 1100;

        /// <summary>
        /// The end acceptable hudline for section 1100.
        /// </summary>
        public const int End1100Hudline = 1199;

        /// <summary>
        /// The first acceptable hudline for section 1200.
        /// </summary>
        public const int Start1200Hudline = 1200;

        /// <summary>
        /// The last acceptable hudline for section 1200.
        /// </summary>
        public const int End1200Hudline = 1299;

        /// <summary>
        /// The first acceptable hudline for section 1300.
        /// </summary>
        public const int Start1300Hudline = 1300;

        /// <summary>
        /// The end acceptable hudline for section 1300.
        /// </summary>
        public const int End1300Hudline = 1399;

        /// <summary>
        /// A static readonly list of fee type ids for the prepaid fees for section 900.
        /// </summary>
        public static readonly ReadOnlyCollection<Guid> ConstantPrepaidEscrowFeeList = new ReadOnlyCollection<Guid>(new Guid[]  
        {
            DefaultSystemClosingCostFee.Hud900HazardInsuranceFeeTypeId,
            DefaultSystemClosingCostFee.Hud900CondoInsuranceFeeTypeId,
            DefaultSystemClosingCostFee.Hud900WindstormInsuranceFeeTypeId,
            DefaultSystemClosingCostFee.Hud900FloodInsuranceFeeTypeId,
            DefaultSystemClosingCostFee.Hud900PropertyTaxFeeTypeId,
            DefaultSystemClosingCostFee.Hud900SchoolTaxFeeTypeId,
            DefaultSystemClosingCostFee.Hud900OtherTax1FeeTypeId,
            DefaultSystemClosingCostFee.Hud900OtherTax2FeeTypeId,
            DefaultSystemClosingCostFee.Hud900OtherTax3FeeTypeId,
            DefaultSystemClosingCostFee.Hud900OtherTax4FeeTypeId,
            DefaultSystemClosingCostFee.Hud900OtherExp1FeeTypeId,
            DefaultSystemClosingCostFee.Hud900OtherExp2FeeTypeId,
            DefaultSystemClosingCostFee.Hud900OtherExp3FeeTypeId,
            DefaultSystemClosingCostFee.Hud900OtherExp4FeeTypeId,
            DefaultSystemClosingCostFee.Hud900HOADuesFeeTypeId,
            DefaultSystemClosingCostFee.Hud900GroundRentFeeTypeId
        });

        /// <summary>
        /// A static readonly list of fee type for the initial escrow payment section.
        /// </summary>
        public static readonly ReadOnlyCollection<Guid> ConstantInitialEscrowLegacyFeeList = new ReadOnlyCollection<Guid>(new Guid[] 
        {
            DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId,
            DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId,
            DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId,
            DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId,
            DefaultSystemClosingCostFee.Hud1000WindStormInsuranceFeeTypeId,
            DefaultSystemClosingCostFee.Hud1000CondoHo6InsuranceFeeTypeId,
            DefaultSystemClosingCostFee.Hud1000OtherTax1FeeTypeId,
            DefaultSystemClosingCostFee.Hud1000OtherTax2FeeTypeId,
            DefaultSystemClosingCostFee.Hud1000OtherTax3FeeTypeId,
            DefaultSystemClosingCostFee.Hud1000OtherTax4FeeTypeId,
            DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId,
            DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId,
            DefaultSystemClosingCostFee.Hud1000Custom1FeeTypeId,
            DefaultSystemClosingCostFee.Hud1000Custom2FeeTypeId,
            DefaultSystemClosingCostFee.Hud1000Custom3FeeTypeId,
            DefaultSystemClosingCostFee.Hud1000Custom4FeeTypeId,
            DefaultSystemClosingCostFee.Hud1000GroundRentFeeTypeId,
            DefaultSystemClosingCostFee.Hud1000HomeOwnerAssociationDuesFeeTypeId
        });

        /// <summary>
        /// A static readonly set of MISMO closing cost fee types that do not require
        /// APR and are thus not included in QM.
        /// </summary>
        public static readonly HashSet<E_ClosingCostFeeMismoFeeT> MismoFeeTypesNotRequiringApr = new HashSet<E_ClosingCostFeeMismoFeeT>()
        {
            E_ClosingCostFeeMismoFeeT.AppraisalDeskReviewFee,
            E_ClosingCostFeeMismoFeeT.AppraisalFee,
            E_ClosingCostFeeMismoFeeT.AppraisalFieldReviewFee,
            E_ClosingCostFeeMismoFeeT.AppraisalManagementCompanyFee,
            E_ClosingCostFeeMismoFeeT.AsbestosInspectionFee,
            E_ClosingCostFeeMismoFeeT.AVMFee,
            E_ClosingCostFeeMismoFeeT.CreditReportFee,
            E_ClosingCostFeeMismoFeeT.DeedPreparationFee,
            E_ClosingCostFeeMismoFeeT.DisasterInspectionFee,
            E_ClosingCostFeeMismoFeeT.DocumentPreparationFee,
            E_ClosingCostFeeMismoFeeT.DryWallInspectionFee,
            E_ClosingCostFeeMismoFeeT.ElectricalInspectionFee,
            E_ClosingCostFeeMismoFeeT.EnvironmentalInspectionFee,
            E_ClosingCostFeeMismoFeeT.FloodCertification,
            E_ClosingCostFeeMismoFeeT.FoundationInspectionFee,
            E_ClosingCostFeeMismoFeeT.HeatingCoolingInspectionFee,
            E_ClosingCostFeeMismoFeeT.LeadInspectionFee,
            E_ClosingCostFeeMismoFeeT.ManufacturedHousingInspectionFee,
            E_ClosingCostFeeMismoFeeT.MoldInspectionFee,
            E_ClosingCostFeeMismoFeeT.NotaryFee,
            E_ClosingCostFeeMismoFeeT.PestInspectionFee,
            E_ClosingCostFeeMismoFeeT.PlumbingInspectionFee,
            E_ClosingCostFeeMismoFeeT.RadonInspectionFee,
            E_ClosingCostFeeMismoFeeT.RoofInspectionFee,
            E_ClosingCostFeeMismoFeeT.SepticInspectionFee,
            E_ClosingCostFeeMismoFeeT.SmokeDetectorInspectionFee,
            E_ClosingCostFeeMismoFeeT.StructuralInspectionFee,
            E_ClosingCostFeeMismoFeeT.WellInspectionFee
        };

        /// <summary>
        /// Set of HUD line numbers that allow multiple fees per HUD line.
        /// </summary>
        public static readonly HashSet<int> HudLinesAllowingMultipleFees = new HashSet<int>()
        {
            800, 900, 1000, 1100, 1200, 1300, 801, 802, 902, 1202
        };

        /// <summary>
        /// Constant Guid used to generate a predictable Guid from another Guid.
        /// </summary>
        public static readonly Guid SaltGuid1 = new Guid("1933666f-c1ba-41de-b0d0-da248217a0e7");

        /// <summary>
        /// Constant Guid used to generate a predictable Guid from another Guid
        /// when the 1st salt has already been used.
        /// </summary>
        public static readonly Guid SaltGuid2 = new Guid("7d9cb834-2d16-4665-8808-185195c57266");

        /// <summary>
        /// Combines two Guids to make a new Guid different from either.
        /// </summary>
        /// <param name="baseGuid">The base, unique Guid.</param>
        /// <param name="saltGuid">The salt, non-unique Guid.</param>
        /// <returns>A new Guid that is almost certainly different from Guids produced using other techniques.</returns>
        public static Guid CombineGuids(Guid baseGuid, Guid saltGuid)
        {
            byte[] baseBytes = baseGuid.ToByteArray();
            byte[] saltBytes = saltGuid.ToByteArray();
            byte[] combinedBytes = new byte[baseBytes.Length];

            for (int i = 0; i < baseBytes.Length; ++i)
            {
                unchecked
                {
                    combinedBytes[i] = (byte)(baseBytes[i] + (17 * saltBytes[i]));
                }
            }

            return new Guid(combinedBytes);
        }

        /// <summary>
        /// Default filter for 800 section.
        /// </summary>
        /// <param name="fee">The fee to check.</param>
        /// <returns>True if matched filter, false otherwise.</returns>
        public static bool Section800Filter(BaseClosingCostFee fee)
        {
            return fee.HudLine >= 800 && fee.HudLine < 900;
        }

        /// <summary>
        /// Default filter for 900 section.
        /// </summary>
        /// <param name="fee">The fee to check.</param>
        /// <returns>True if matched filter, false otherwise.</returns>
        public static bool Section900Filter(BaseClosingCostFee fee)
        {
            return fee.HudLine >= 900 && fee.HudLine < 1000;
        }

        /// <summary>
        /// Default filter for 1000 section.
        /// </summary>
        /// <param name="fee">The fee to check.</param>
        /// <returns>True if matched filter, false otherwise.</returns>
        public static bool Section1000Filter(BaseClosingCostFee fee)
        {
            return fee.HudLine >= 1000 && fee.HudLine < 1100;
        }

        /// <summary>
        /// Default filter for 1100 section.
        /// </summary>
        /// <param name="fee">The fee to check.</param>
        /// <returns>True if matched filter, false otherwise.</returns>
        public static bool Section1100Filter(BaseClosingCostFee fee)
        {
            return fee.HudLine >= 1100 && fee.HudLine < 1200;
        }

        /// <summary>
        /// Default filter for 1200 section.
        /// </summary>
        /// <param name="fee">The fee to check.</param>
        /// <returns>True if matched filter, false otherwise.</returns>
        public static bool Section1200Filter(BaseClosingCostFee fee)
        {
            return fee.HudLine >= 1200 && fee.HudLine < 1300;
        }

        /// <summary>
        /// Default filter for 1300 section.
        /// </summary>
        /// <param name="fee">The fee to check.</param>
        /// <returns>True if matched filter, false otherwise.</returns>
        public static bool Section1300Filter(BaseClosingCostFee fee)
        {
            return fee.HudLine >= 1300 && fee.HudLine < 1400;
        }

        /// <summary>
        /// The default filter for section A.
        /// </summary>
        /// <param name="fee">The fee to check.</param>
        /// <returns>True if the fee is in section A, false otherwise.</returns>
        public static bool SectionAFilter(BaseClosingCostFee fee)
        {
            return fee.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionA;
        }

        /// <summary>
        /// The default filter for section B.
        /// </summary>
        /// <param name="fee">The fee to check.</param>
        /// <returns>True if the fee is in section B, false otherwise.</returns>
        public static bool SectionBFilter(BaseClosingCostFee fee)
        {
            return fee.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionB;
        }

        /// <summary>
        /// The default filter for section C.
        /// </summary>
        /// <param name="fee">The fee to check.</param>
        /// <returns>True if the fee is in section C, false otherwise.</returns>
        public static bool SectionCFilter(BaseClosingCostFee fee)
        {
            return fee.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionC;
        }

        /// <summary>
        /// The default filter for section D.
        /// </summary>
        /// <param name="fee">The fee to check.</param>
        /// <returns>True if the fee is in section D, false otherwise.</returns>
        public static bool SectionDFilter(BaseClosingCostFee fee)
        {
            return fee.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionD;
        }

        /// <summary>
        /// The default filter for section E.
        /// </summary>
        /// <param name="fee">The fee to check.</param>
        /// <returns>True if the fee is in section E, false otherwise.</returns>
        public static bool SectionEFilter(BaseClosingCostFee fee)
        {
            return fee.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionE;
        }

        /// <summary>
        /// The default filter for section F.
        /// </summary>
        /// <param name="fee">The fee to check.</param>
        /// <returns>True if the fee is in section F, false otherwise.</returns>
        public static bool SectionFFilter(BaseClosingCostFee fee)
        {
            return fee.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionF;
        }

        /// <summary>
        /// The default filter for section G.
        /// </summary>
        /// <param name="fee">The fee to check.</param>
        /// <returns>True if the fee is in section G, false otherwise.</returns>
        public static bool SectionGFilter(BaseClosingCostFee fee)
        {
            return fee.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionG;
        }

        /// <summary>
        /// The default filter for section H.
        /// </summary>
        /// <param name="fee">The fee to check.</param>
        /// <returns>True if the fee is in section H, false otherwise.</returns>
        public static bool SectionHFilter(BaseClosingCostFee fee)
        {
            return fee.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionH;
        }

        /// <summary>
        /// Filters out the Prepaids, MI, and Section G Fees. Checks if the TotalAmount is non zero.
        /// </summary>
        /// <param name="borrowerFee">The fee to check.</param>
        /// <returns>True if the fee passes the filter or if it doesn't need to be checked. False otherwise.</returns>
        public static bool PrepsSecGMIFilter(BorrowerClosingCostFee borrowerFee)
        {
            if (borrowerFee.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionG ||
                borrowerFee.FormulaT == E_ClosingCostFeeFormulaT.PrepaidAmountForExpenses ||
                borrowerFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900VAFundingFeeTypeId ||
                borrowerFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId ||
                borrowerFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumUpfrontFeeTypeId)
            {
                return borrowerFee.TotalAmount != 0;
            }
            else
            {
                // This fee does not get a special filter. Return true.
                return true;
            }
        }

        /// <summary>
        /// Filters the Discount Points Fee. Filtered out if TotalAmount is zero.
        /// </summary>
        /// <param name="borrowerFee">The Fee to check.</param>
        /// <returns>True if the discount points fee has a non-zero total amount or if the fee is not the discount points fee. False otherwise.</returns>
        public static bool DiscountPointsFilter(BorrowerClosingCostFee borrowerFee)
        {
            if (borrowerFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId)
            {
                return borrowerFee.TotalAmount != 0;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Checks the Originator Compensation Fee. Essentially replaces HandleOriginatorCompensation (at least the filter aspect of it). 
        /// </summary>
        /// <param name="borrowerFee">The Fee to check.</param>
        /// <param name="originatorCompensationPaymentSourceT">The originator compensation payment source.</param>
        /// <param name="disclosureRegulationT">The disclosure regulation type.</param>
        /// <param name="gfeIsTPOTransaction">The loan file field.</param>
        /// <returns>Return true if not the originator compensation fee or if the checks pass. False otherwise.</returns>
        public static bool OrigCompFilter(
            BorrowerClosingCostFee borrowerFee,
            E_sOriginatorCompensationPaymentSourceT originatorCompensationPaymentSourceT,
            E_sDisclosureRegulationT disclosureRegulationT,
            bool gfeIsTPOTransaction)
        {
            if (borrowerFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId)
            {
                if (originatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid)
                {
                    return true;
                }
                else if (originatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)
                {
                    if (disclosureRegulationT != E_sDisclosureRegulationT.TRID)
                    {
                        if (gfeIsTPOTransaction)
                        {
                            return true;
                        }
                    }
                }
             
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Filters the Originator Compensation fees using data loan values stored in an archive.
        /// </summary>
        /// <param name="borrowerFee">The Borrower Fee to check.</param>
        /// <param name="archive">The archive to pull values from.</param>
        /// <returns>True if the fee is to be included, false otherwise.</returns>
        public static bool OrigCompFilterWithArchive(BorrowerClosingCostFee borrowerFee, ClosingCostArchive archive)
        {
            var originatorCompensationSourceT = (E_sOriginatorCompensationPaymentSourceT)archive.GetCountValue("sOriginatorCompensationPaymentSourceT");
            var disclosureRegulationT = (E_sDisclosureRegulationT)archive.GetCountValue("sDisclosureRegulationT");
            var gfeIsTPOTransaction = archive.GetBitValue("sGfeIsTPOTransaction");

            if (borrowerFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId)
            {
                if (originatorCompensationSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid)
                {
                    return true;
                }
                else if (originatorCompensationSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)
                {
                    if (disclosureRegulationT != E_sDisclosureRegulationT.TRID 
                        || (disclosureRegulationT == E_sDisclosureRegulationT.TRID && archive.ClosingCostArchiveType == ClosingCostArchive.E_ClosingCostArchiveType.ClosingDisclosure))
                    {
                        if (gfeIsTPOTransaction)
                        {
                            return true;
                        }
                    }
                }

                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Combination of filters meant to filter out the Borrower Closing Cost Set. WILL CHECK ORIGINATOR COMPENSATION FEE.
        /// </summary>
        /// <param name="borrowerFee">The Fee to check.</param>
        /// <param name="originatorCompensationPaymentSourceT">The originator compensation payment source.</param>
        /// <param name="disclosureRegulationT">The disclosure regulation type.</param>
        /// <param name="gfeIsTPOTransaction">The loan file field.</param>
        /// <returns>True if all 3 filters pass. False otherwise.</returns>
        public static bool OrigCompDiscountPrepsSecGMIFilter(
            BorrowerClosingCostFee borrowerFee,
            E_sOriginatorCompensationPaymentSourceT originatorCompensationPaymentSourceT,
            E_sDisclosureRegulationT disclosureRegulationT,
            bool gfeIsTPOTransaction)
        {
            return PrepsSecGMIFilter(borrowerFee) &&
                   DiscountPointsFilter(borrowerFee) &&
                   OrigCompFilter(borrowerFee, originatorCompensationPaymentSourceT, disclosureRegulationT, gfeIsTPOTransaction);
        }

        /// <summary>
        /// Combination of filters meant to filter out the Borrower Closing Cost Set. Filters the Originator Comp Fee based on the values stored in the archive.
        /// </summary>
        /// <param name="borrowerFee">The Fee to check.</param>
        /// <param name="archive">The archive to pull values from for the Originator Comp Fee.</param>
        /// <returns>True if the Fee passes all filters, false otherwise.</returns>
        public static bool OrigCompDiscountPrepsSecGMIFilter(BorrowerClosingCostFee borrowerFee, ClosingCostArchive archive)
        {
            return PrepsSecGMIFilter(borrowerFee) &&
                   DiscountPointsFilter(borrowerFee) &&
                   OrigCompFilterWithArchive(borrowerFee, archive);
        }

        /// <summary>
        /// Filter for Borrower Set enumeration. EXCLUDES THE ORIGINATOR COMPENSATION FEE.
        /// </summary>
        /// <param name="borrowerFee">The Fee to check.</param>
        /// <returns>Returns true if all filters are met. False otherwise.</returns>
        public static bool ExOrigCompDiscountPrepsSecGMIFilter(BorrowerClosingCostFee borrowerFee)
        {
            return borrowerFee.ClosingCostFeeTypeId != DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId &&
                   PrepsSecGMIFilter(borrowerFee) &&
                   DiscountPointsFilter(borrowerFee);
        }

        /// <summary>
        /// Will give you the correct filter for the Borrower set based on its status and the input parameters. USE THIS IF YOU DON'T KNOW WHAT TO GET.
        /// </summary>
        /// <param name="borrowerSet">The Borrower Set.</param>
        /// <param name="includeOrigCompFeeWithoutLoan">
        /// Whether or not you want to include the Orig Comp Fee if the Set does not have an associated loan.
        /// If this is true and the Set has an archive, it will filter out the Set based on the archived values.
        /// </param>
        /// <param name="originatorCompensationPaymentSourceT">The loan file field sOriginatorCompensationPaymentSourceT. Used only if Set has data loan associate.</param>
        /// <param name="disclosureRegulationT">The loan file field sDisclosureRegulationT. Used only if the Set has data loan associate.</param>
        /// <param name="gfeIsTPOTransaction">The loan file field sGfeIsTPOTransaction. Used only if the Set has data loan associate.</param>
        /// <returns>The correct filter for the Borrower set.</returns>
        public static Func<BaseClosingCostFee, bool> GetOrigCompDiscountPrepsSecGMIFilter(
            BorrowerClosingCostSet borrowerSet,
            bool includeOrigCompFeeWithoutLoan,
            E_sOriginatorCompensationPaymentSourceT? originatorCompensationPaymentSourceT,
            E_sDisclosureRegulationT? disclosureRegulationT,
            bool? gfeIsTPOTransaction)
        {
            if (borrowerSet.HasDataLoanAssociate &&
                originatorCompensationPaymentSourceT.HasValue &&
                disclosureRegulationT.HasValue &&
                gfeIsTPOTransaction.HasValue)
            {
                return fee => OrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostFee)fee, originatorCompensationPaymentSourceT.Value, disclosureRegulationT.Value, gfeIsTPOTransaction.Value);
            }
            else if (includeOrigCompFeeWithoutLoan)
            {
                if (borrowerSet.HasClosingCostArchiveAssociate)
                {
                    return fee => OrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostFee)fee, borrowerSet.ClosingCostArchive);
                }
                else
                {
                    return fee => PrepsSecGMIFilter((BorrowerClosingCostFee)fee) && DiscountPointsFilter((BorrowerClosingCostFee)fee);
                }
            }
            else
            {
                return fee => ExOrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostFee)fee);
            }
        }

        /// <summary>
        /// Filter for Borrower Set. Excludes any Fees with null Description and IntegratedDisclosureSection of 'LeaveBlank'.
        /// </summary>
        /// <param name="borrowerFee">The Borrower Fee to check.</param>
        /// <returns>True if a valid Borrower Fee, false otherwise.</returns>
        public static bool ValidDiscSecDescriptionFilter(BorrowerClosingCostFee borrowerFee)
        {
            return borrowerFee.Description != null && borrowerFee.IntegratedDisclosureSectionT != E_IntegratedDisclosureSectionT.LeaveBlank;
        }

        /// <summary>
        /// Returns the formula type based on the fee properties.
        /// </summary>
        /// <param name="integratedDisclosureSectionT">The disclosure section.</param>
        /// <param name="legacyGfeFieldT">The legacy GFE field.</param>
        /// <param name="closingCostFeeTypeId">The fee's closing cost fee type id.</param>
        /// <returns>The formula type if it is enforced for this fee. Null if not.</returns>
        public static E_ClosingCostFeeFormulaT? GetFormulaType(E_IntegratedDisclosureSectionT integratedDisclosureSectionT, E_LegacyGfeFieldT legacyGfeFieldT, Guid closingCostFeeTypeId)
        {
            // TODO: If a base class is find for the fees, move this over to there.
            if (integratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionG)
            {
                return E_ClosingCostFeeFormulaT.InitialEscrowPaymentAtClosing;
            }
            else if (legacyGfeFieldT == E_LegacyGfeFieldT.sMipPia)
            {
                return E_ClosingCostFeeFormulaT.MortgageInsurancePremiumRecurring;
            }
            else if (legacyGfeFieldT == E_LegacyGfeFieldT.sUpfrontMipPia)
            {
                return E_ClosingCostFeeFormulaT.MortgageInsurancePremiumUpfront;
            }
            else if (legacyGfeFieldT == E_LegacyGfeFieldT.sVaFf)
            {
                return E_ClosingCostFeeFormulaT.VAFundingFee;
            }
            else if (legacyGfeFieldT == E_LegacyGfeFieldT.sHazInsPia)
            {
                return E_ClosingCostFeeFormulaT.PrepaidAmountForExpenses;
            }
            else if (legacyGfeFieldT == E_LegacyGfeFieldT.sIPia)
            {
                return E_ClosingCostFeeFormulaT.sIPia;
            }
            else if (legacyGfeFieldT == E_LegacyGfeFieldT.sLDiscnt)
            {
                return E_ClosingCostFeeFormulaT.sLDiscnt;
            }
            else if (legacyGfeFieldT == E_LegacyGfeFieldT.sLOrigF)
            {
                return E_ClosingCostFeeFormulaT.Full;
            }
            else if (legacyGfeFieldT == E_LegacyGfeFieldT.sGfeOriginatorCompF)
            {
                return E_ClosingCostFeeFormulaT.sOriginatorCompensationTotalAmount;
            }
            else if (ClosingCostSetUtils.ConstantPrepaidEscrowFeeList.Contains(closingCostFeeTypeId))
            {
                return E_ClosingCostFeeFormulaT.PrepaidAmountForExpenses;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// For most system fees, will return the appropriate Mismo Fee Type.
        /// </summary>
        /// <param name="feeTypeId">The Fee Type Id.</param>
        /// <param name="isUSDALoan">Is the Loan Type for the loan this fee belongs to a USDA loan. False if fee not associated with a loan.</param>
        /// <returns>Null if does not have a hard-coded mismo fee type, the mismo fee type otherwise.</returns>
        public static E_ClosingCostFeeMismoFeeT? GetMismoFeeType(Guid feeTypeId, bool isUSDALoan)
        {
            if (feeTypeId == DefaultSystemClosingCostFee.Hud800LoanOriginationFeeTypeId)
            {
                return E_ClosingCostFeeMismoFeeT.LoanOriginationFee;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId)
            {
                return E_ClosingCostFeeMismoFeeT.LoanOriginatorCompensation;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId)
            {
                return E_ClosingCostFeeMismoFeeT.LoanDiscountPoints;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud800AppraisalFeeFeeTypeId)
            {
                return E_ClosingCostFeeMismoFeeT.AppraisalFee;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud800CreditReportFeeTypeId)
            {
                return E_ClosingCostFeeMismoFeeT.CreditReportFee;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud800TaxServiceFeeTypeId)
            {
                return E_ClosingCostFeeMismoFeeT.TaxRelatedServiceFee;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud800FloodCertificationFeeTypeId)
            {
                return E_ClosingCostFeeMismoFeeT.FloodCertification;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud800MortgageBrokerFeeTypeId)
            {
                return E_ClosingCostFeeMismoFeeT.MortgageBrokerFee;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud800ProcessingFeeTypeId)
            {
                return E_ClosingCostFeeMismoFeeT.ProcessingFee;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud800UnderwritingFeeTypeId)
            {
                return E_ClosingCostFeeMismoFeeT.UnderwritingFee;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud800WireTransferFeeTypeId)
            {
                return E_ClosingCostFeeMismoFeeT.WireTransferFee;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud1100ClosingEscrowFeeTypeId)
            {
                return E_ClosingCostFeeMismoFeeT.EscrowServiceFee;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud1100DocPreparationFeeTypeId)
            {
                return E_ClosingCostFeeMismoFeeT.DocumentPreparationFee;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud1100LenderTitleInsuranceFeeTypeId)
            {
                return E_ClosingCostFeeMismoFeeT.TitleLendersCoveragePremium;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud1100OwnerTitleInsuranceFeeTypeId)
            {
                return E_ClosingCostFeeMismoFeeT.TitleOwnersCoveragePremium;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud1100NotaryFeeTypeId)
            {
                return E_ClosingCostFeeMismoFeeT.NotaryFee;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud1100AttorneyFeeTypeId)
            {
                return E_ClosingCostFeeMismoFeeT.AttorneyFee;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud1200DeedFeeTypeId)
            {
                return E_ClosingCostFeeMismoFeeT.DeedRecordingFee;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud1200MortgageFeeTypeId)
            {
                return E_ClosingCostFeeMismoFeeT.MortgageRecordingFee;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud1200ReleaseFeeTypeId)
            {
                return E_ClosingCostFeeMismoFeeT.ReleaseRecordingFee;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud1200CountyTaxStampsFeeTypeId)
            {
                return E_ClosingCostFeeMismoFeeT.CountyMortgageTaxStampFee;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud1200StateTaxStampsFeeTypeId)
            {
                return E_ClosingCostFeeMismoFeeT.StateMortgageTaxStampFee;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud1300PestInspectionFeeTypeId)
            {
                return E_ClosingCostFeeMismoFeeT.PestInspectionFee;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud900VAFundingFeeTypeId)
            {
                return E_ClosingCostFeeMismoFeeT.VAFundingFee;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud900HOADuesFeeTypeId)
            {
                return E_ClosingCostFeeMismoFeeT.HomeownersAssociationDues;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId)
            {
                return E_ClosingCostFeeMismoFeeT.MIInitialPremium;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumUpfrontFeeTypeId)
            {
                if (isUSDALoan)
                {
                    return E_ClosingCostFeeMismoFeeT.RuralHousingFee;
                }
                else
                {
                    return E_ClosingCostFeeMismoFeeT.MIUpfrontPremium;
                }
            }
            else if (DefaultSystemClosingCostFee.FeeTemplate.IsFeeTemplateSystemFeeType(feeTypeId))
            {
                return DefaultSystemClosingCostFee.FeeTemplate.GetMismoFeeType(feeTypeId);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Determines what the GFE Section for this fee type should be.
        /// </summary>
        /// <param name="feeTypeId">The fee's fee type id.</param>
        /// <returns>Null if the fee type does not have exactly ONE GFE section it can be in. The GFE Section otherwise.</returns>
        public static E_GfeSectionT? GetGfeSectionType(Guid feeTypeId)
        {
            if (feeTypeId == DefaultSystemClosingCostFee.Hud800LoanOriginationFeeTypeId ||
                feeTypeId == DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId ||
                feeTypeId == DefaultSystemClosingCostFee.Hud800MortgageBrokerFeeTypeId ||
                feeTypeId == DefaultSystemClosingCostFee.Hud800LenderInspectionFeeTypeId ||
                feeTypeId == DefaultSystemClosingCostFee.Hud800ProcessingFeeTypeId ||
                feeTypeId == DefaultSystemClosingCostFee.Hud800UnderwritingFeeTypeId ||
                feeTypeId == DefaultSystemClosingCostFee.Hud800WireTransferFeeTypeId)
            {
                return E_GfeSectionT.B1;    
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId)
            {
                return E_GfeSectionT.B2;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud800AppraisalFeeFeeTypeId ||
                    feeTypeId == DefaultSystemClosingCostFee.Hud800CreditReportFeeTypeId ||
                    feeTypeId == DefaultSystemClosingCostFee.Hud800FloodCertificationFeeTypeId ||
                    feeTypeId == DefaultSystemClosingCostFee.Hud800TaxServiceFeeTypeId ||
                    feeTypeId == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId ||
                    feeTypeId == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumUpfrontFeeTypeId ||
                    feeTypeId == DefaultSystemClosingCostFee.Hud900VAFundingFeeTypeId)
            {
                return E_GfeSectionT.B3;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud1100LenderTitleInsuranceFeeTypeId)
            {
                return E_GfeSectionT.B4;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud1100OwnerTitleInsuranceFeeTypeId)
            {
                return E_GfeSectionT.B5;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud1300PestInspectionFeeTypeId)
            {
                return E_GfeSectionT.B6;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud1200DeedFeeTypeId ||
                    feeTypeId == DefaultSystemClosingCostFee.Hud1200MortgageFeeTypeId ||
                    feeTypeId == DefaultSystemClosingCostFee.Hud1200ReleaseFeeTypeId)
            {
                return E_GfeSectionT.B7;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud1200CountyTaxStampsFeeTypeId ||
                    feeTypeId == DefaultSystemClosingCostFee.Hud1200StateTaxStampsFeeTypeId)
            {
                return E_GfeSectionT.B8;
            }
            else if (ClosingCostSetUtils.ConstantInitialEscrowLegacyFeeList.Contains(feeTypeId))
            {
                return E_GfeSectionT.B9;
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId)
            {
                return E_GfeSectionT.B10;
            }
            else if (ClosingCostSetUtils.ConstantPrepaidEscrowFeeList.Contains(feeTypeId))
            {
                return E_GfeSectionT.B11;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the set HUD line for a fee if it cannot change.
        /// </summary>
        /// <param name="closingCostFeeTypeId">The fee id to get the HUD line for.</param>
        /// <returns>The HUD line for the fee if needed, null otherwise.</returns>
        public static string GetPredeterminedHudline(Guid closingCostFeeTypeId)
        {
            // Another candidate to put into a base class when it gets made.
            if (closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId)
            {
                return "1002";
            }
            else if (closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId)
            {
                return "1003";
            }
            else if (closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId)
            {
                return "1004";
            }
            else if (closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId)
            {
                return "1005";
            }
            else if (closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId)
            {
                return "1006";
            }
            else if (closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId)
            {
                return "1007";
            }
            else if (closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000Custom1FeeTypeId)
            {
                return "1008";
            }
            else if (closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000Custom2FeeTypeId)
            {
                return "1009";
            }
            else if (closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000Custom3FeeTypeId)
            {
                return "1010";
            }
            else if (closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000Custom4FeeTypeId)
            {
                return "1011";
            }
            else if (closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900OtherExp1FeeTypeId)
            {
                return "910";
            }
            else if (closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900OtherExp2FeeTypeId)
            {
                return "911";
            }
            else if (closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900OtherExp3FeeTypeId)
            {
                return "912";
            }
            else if (closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900OtherExp4FeeTypeId)
            {
                return "913";
            }
            else if (closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId)
            {
                return "802";
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Returns the section a fee must always belong in.
        /// </summary>
        /// <param name="closingCostFeeTypeId">The fee's closing cost fee type id.</param>
        /// <returns>The section if it needs to belong to one. Null otherwise.</returns>
        public static E_IntegratedDisclosureSectionT? DetermineConstantSectionForFee(Guid closingCostFeeTypeId)
        {
            if (closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId)
            {
                return E_IntegratedDisclosureSectionT.SectionA;
            }
            else if (ClosingCostSetUtils.ConstantPrepaidEscrowFeeList.Contains(closingCostFeeTypeId) ||
                     closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId ||
                     closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId)
            {
                return E_IntegratedDisclosureSectionT.SectionF;
            }
            else if (ClosingCostSetUtils.ConstantInitialEscrowLegacyFeeList.Contains(closingCostFeeTypeId))
            {
                return E_IntegratedDisclosureSectionT.SectionG;
            }
            else if (closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900VAFundingFeeTypeId ||
                     closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumUpfrontFeeTypeId)
            {
                return E_IntegratedDisclosureSectionT.SectionB;
            }
            else if (DefaultSystemClosingCostFee.FeeTemplate.IsFeeTemplateSystemFeeType(closingCostFeeTypeId))
            {
                return DefaultSystemClosingCostFee.FeeTemplate.GetIntegratedDisclosureSection(closingCostFeeTypeId);
            }
            else
            {
                return null;
            }
        }
        
        /// <summary>
        /// Checks if a fee is an editable system fee.
        /// </summary>
        /// <param name="isSystemLegacyFee">If the fee is a system fee.</param>
        /// <param name="formulaT">The fee's formula type.</param>
        /// <param name="closingCostFeeTypeId">The fee's closing cost fee type id.</param>
        /// <returns>True if the fee is editable, false if not.</returns>
        public static bool IsEditableSystemFee(bool isSystemLegacyFee, E_ClosingCostFeeFormulaT formulaT, Guid closingCostFeeTypeId)
        {
            // TODO: May also go into a base class.
            if (!isSystemLegacyFee)
            {
                // Not a system fee. It's editable so return true.
                return true;
            }

            if (closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900PropertyTaxFeeTypeId ||
                closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900SchoolTaxFeeTypeId ||
                closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900FloodInsuranceFeeTypeId ||
                closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900WindstormInsuranceFeeTypeId ||
                closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900CondoInsuranceFeeTypeId ||
                closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900HOADuesFeeTypeId ||
                closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900GroundRentFeeTypeId ||
                closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900OtherTax1FeeTypeId ||
                closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900OtherTax2FeeTypeId ||
                closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900OtherTax3FeeTypeId ||
                closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900OtherTax4FeeTypeId ||
                closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000WindStormInsuranceFeeTypeId ||
                closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000CondoHo6InsuranceFeeTypeId ||
                closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000HomeOwnerAssociationDuesFeeTypeId ||
                closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000GroundRentFeeTypeId ||
                closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000OtherTax1FeeTypeId ||
                closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000OtherTax2FeeTypeId ||
                closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000OtherTax3FeeTypeId ||
                closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000OtherTax4FeeTypeId)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Checks if a fee is a system fee that can have its MISMO type modified.
        /// </summary>
        /// <param name="isSystemLegacyFee">If the fee is a system fee.</param>
        /// <param name="closingCostFeeTypeId">The fee's closing cost fee type id.</param>
        /// <returns>True if the fee can have its MISMO type modified, false if not.</returns>
        public static bool CanModifySystemFeeMismoType(bool isSystemLegacyFee, Guid closingCostFeeTypeId)
        {
            // Fees that are not system legacy or fee templates can have their MISMO types changed.
            return closingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800LenderInspectionFeeTypeId
                || (!isSystemLegacyFee && !DefaultSystemClosingCostFee.FeeTemplate.IsFeeTemplateSystemFeeType(closingCostFeeTypeId));
        }

        /// <summary>
        /// Checks if the fee is a system legacy fee based on the parameters.
        /// </summary>
        /// <param name="integratedDisclosureSectionT">The fee's disclosure section.</param>
        /// <param name="closingCostFeeTypeId">The fee's closing cost fee type id.</param>
        /// <returns>True if the fee is a system fee, false otherwise.</returns>
        public static bool IsSystemLegacyFee(E_IntegratedDisclosureSectionT integratedDisclosureSectionT, Guid closingCostFeeTypeId)
        {
            // TODO: What fee gets counted as a 'system' fee in this function doesn't seem complete. Or the name of the property itself is wrong.
            if (DefaultSystemClosingCostFee.IsDefaultFee(closingCostFeeTypeId) ||
                ConstantPrepaidEscrowFeeList.Contains(closingCostFeeTypeId) ||
                ConstantInitialEscrowLegacyFeeList.Contains(closingCostFeeTypeId))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Safely convert string to integer value.
        /// </summary>
        /// <param name="val">String to convert.</param>
        /// <returns>Integer representation. Return 0 for empty string.</returns>
        public static int SafeInt(string val)
        {
            if (string.IsNullOrEmpty(val))
            {
                return 0;
            }

            int i = 0;

            if (int.TryParse(val, out i) == false)
            {
                return 0;
            }

            return i;
        }

        /// <summary>
        /// Gets the housing expense type from the fee's fee type Id.
        /// </summary>
        /// <param name="typeId">The fee's fee type id.</param>
        /// <returns>The expense type if the fee is associated with an expense, null otherwise.</returns>
        public static E_HousingExpenseTypeT? GetHousingExpenseTypeTFromFeeId(Guid typeId)
        {
            if (typeId == DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId || typeId == DefaultSystemClosingCostFee.Hud900HazardInsuranceFeeTypeId)
            {
                return E_HousingExpenseTypeT.HazardInsurance;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId || typeId == DefaultSystemClosingCostFee.Hud900FloodInsuranceFeeTypeId)
            {
                return E_HousingExpenseTypeT.FloodInsurance;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId || typeId == DefaultSystemClosingCostFee.Hud900PropertyTaxFeeTypeId)
            {
                return E_HousingExpenseTypeT.RealEstateTaxes;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId || typeId == DefaultSystemClosingCostFee.Hud900SchoolTaxFeeTypeId)
            {
                return E_HousingExpenseTypeT.SchoolTaxes;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000WindStormInsuranceFeeTypeId || typeId == DefaultSystemClosingCostFee.Hud900WindstormInsuranceFeeTypeId)
            {
                return E_HousingExpenseTypeT.WindstormInsurance;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000CondoHo6InsuranceFeeTypeId || typeId == DefaultSystemClosingCostFee.Hud900CondoInsuranceFeeTypeId)
            {
                return E_HousingExpenseTypeT.CondoHO6Insurance;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000HomeOwnerAssociationDuesFeeTypeId || typeId == DefaultSystemClosingCostFee.Hud900HOADuesFeeTypeId)
            {
                return E_HousingExpenseTypeT.HomeownersAsscDues;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000GroundRentFeeTypeId || typeId == DefaultSystemClosingCostFee.Hud900GroundRentFeeTypeId)
            {
                return E_HousingExpenseTypeT.GroundRent;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000OtherTax1FeeTypeId || typeId == DefaultSystemClosingCostFee.Hud900OtherTax1FeeTypeId)
            {
                return E_HousingExpenseTypeT.OtherTaxes1;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000OtherTax2FeeTypeId || typeId == DefaultSystemClosingCostFee.Hud900OtherTax2FeeTypeId)
            {
                return E_HousingExpenseTypeT.OtherTaxes2;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000OtherTax3FeeTypeId || typeId == DefaultSystemClosingCostFee.Hud900OtherTax3FeeTypeId)
            {
                return E_HousingExpenseTypeT.OtherTaxes3;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000OtherTax4FeeTypeId || typeId == DefaultSystemClosingCostFee.Hud900OtherTax4FeeTypeId)
            {
                return E_HousingExpenseTypeT.OtherTaxes4;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000Custom1FeeTypeId || typeId == DefaultSystemClosingCostFee.Hud900OtherExp1FeeTypeId ||
                typeId == DefaultSystemClosingCostFee.Hud1000Custom2FeeTypeId || typeId == DefaultSystemClosingCostFee.Hud900OtherExp2FeeTypeId ||
                typeId == DefaultSystemClosingCostFee.Hud1000Custom3FeeTypeId || typeId == DefaultSystemClosingCostFee.Hud900OtherExp3FeeTypeId ||
                typeId == DefaultSystemClosingCostFee.Hud1000Custom4FeeTypeId || typeId == DefaultSystemClosingCostFee.Hud900OtherExp4FeeTypeId)
            {
                return E_HousingExpenseTypeT.Unassigned;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the legacy custom expense line number based on the fee's fee type id.
        /// </summary>
        /// <param name="typeId">The fee's fee type id.</param>
        /// <returns>The custom expense line number if it associated with the fee, null otherwise.</returns>
        public static E_CustomExpenseLineNumberT? GetLegacyCustomExpenseLineNumFromFeeTypeId(Guid typeId)
        {
            if (typeId == DefaultSystemClosingCostFee.Hud1000Custom1FeeTypeId || typeId == DefaultSystemClosingCostFee.Hud900OtherExp1FeeTypeId)
            {
                return E_CustomExpenseLineNumberT.Line1008;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000Custom2FeeTypeId || typeId == DefaultSystemClosingCostFee.Hud900OtherExp2FeeTypeId)
            {
                return E_CustomExpenseLineNumberT.Line1009;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000Custom3FeeTypeId || typeId == DefaultSystemClosingCostFee.Hud900OtherExp3FeeTypeId)
            {
                return E_CustomExpenseLineNumberT.Line1010;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000Custom4FeeTypeId || typeId == DefaultSystemClosingCostFee.Hud900OtherExp4FeeTypeId)
            {
                return E_CustomExpenseLineNumberT.Line1011;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 8/21/2015 Budi - Case 222479
        /// Gets the feeTypeId associated with housingExpenseType.
        /// </summary>
        /// <param name="expense">Base Housing Expense.</param>
        /// <returns>Null if fee type id not found. The feeTypeId otherwise.</returns>
        public static Guid Get1000FeeTypeIdFromAssignedHousingExpense(BaseHousingExpense expense)
        {
            if (expense.HousingExpenseType == E_HousingExpenseTypeT.CondoHO6Insurance)
            {
                return DefaultSystemClosingCostFee.Hud1000CondoHo6InsuranceFeeTypeId;
            }
            else if (expense.HousingExpenseType == E_HousingExpenseTypeT.WindstormInsurance)
            {
                return DefaultSystemClosingCostFee.Hud1000WindStormInsuranceFeeTypeId;
            }
            else if (expense.HousingExpenseType == E_HousingExpenseTypeT.HazardInsurance)
            {
                return DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId;
            }
            else if (expense.HousingExpenseType == E_HousingExpenseTypeT.FloodInsurance)
            {
                return DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId;
            }
            else if (expense.HousingExpenseType == E_HousingExpenseTypeT.RealEstateTaxes)
            {
                return DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId;
            }
            else if (expense.HousingExpenseType == E_HousingExpenseTypeT.SchoolTaxes)
            {
                return DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId;
            }
            else if (expense.HousingExpenseType == E_HousingExpenseTypeT.OtherTaxes1)
            {
                return DefaultSystemClosingCostFee.Hud1000OtherTax1FeeTypeId;
            }
            else if (expense.HousingExpenseType == E_HousingExpenseTypeT.OtherTaxes2)
            {
                return DefaultSystemClosingCostFee.Hud1000OtherTax2FeeTypeId;
            }
            else if (expense.HousingExpenseType == E_HousingExpenseTypeT.OtherTaxes3)
            {
                return DefaultSystemClosingCostFee.Hud1000OtherTax3FeeTypeId;
            }
            else if (expense.HousingExpenseType == E_HousingExpenseTypeT.OtherTaxes4)
            {
                return DefaultSystemClosingCostFee.Hud1000OtherTax4FeeTypeId;
            }
            else if (expense.HousingExpenseType == E_HousingExpenseTypeT.HomeownersAsscDues)
            {
                return DefaultSystemClosingCostFee.Hud1000HomeOwnerAssociationDuesFeeTypeId;
            }
            else if (expense.HousingExpenseType == E_HousingExpenseTypeT.GroundRent)
            {
                return DefaultSystemClosingCostFee.Hud1000GroundRentFeeTypeId;
            }
            else
            {
                return Guid.Empty;
            }
        }

        /// <summary>
        /// Maps E_IntegratedDisclosureSectionT value to its string representation.
        /// </summary>
        /// <param name="section">E_IntegratedDisclosureSectionT value.</param>
        /// <returns>String representation of E_IntegratedDisclosureSectionT value.</returns>
        public static string MapIntegratedDisclosureSectionToString(E_IntegratedDisclosureSectionT section)
        {
            switch (section)
            {
                case E_IntegratedDisclosureSectionT.LeaveBlank:
                    return string.Empty;
                case E_IntegratedDisclosureSectionT.SectionA:
                    return "A";
                case E_IntegratedDisclosureSectionT.SectionB:
                    return "B";
                case E_IntegratedDisclosureSectionT.SectionC:
                case E_IntegratedDisclosureSectionT.SectionBorC:
                    return "C";
                case E_IntegratedDisclosureSectionT.SectionD:
                    return "D";
                case E_IntegratedDisclosureSectionT.SectionE:
                    return "E";
                case E_IntegratedDisclosureSectionT.SectionF:
                    return "F";
                case E_IntegratedDisclosureSectionT.SectionG:
                    return "G";
                case E_IntegratedDisclosureSectionT.SectionH:
                    return "H";
                default:
                    throw new UnhandledEnumException(section, "Unexpected IntegratedDisclosureSectionT.");
            }
        }
    }
}
