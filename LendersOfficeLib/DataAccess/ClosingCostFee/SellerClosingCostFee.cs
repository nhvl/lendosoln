﻿// <copyright file="SellerClosingCostFee.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   7/9/2015
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using DataAccess;

    /// <summary>
    /// Class representing a seller responsible closing cost fee.
    /// </summary>
    [DataContract]
    public class SellerClosingCostFee : LoanClosingCostFee
    {
        /// <summary>
        /// Gets or sets the reponsible party type.
        /// </summary>
        public override E_GfeResponsiblePartyT GfeResponsiblePartyT
        {
            get
            {
                if (this.Payments != null && this.Payments.Count(p => p != null) > 0)
                {
                    return this.Payments.Where(p => p != null).FirstOrDefault().ResponsiblePartyT;
                }

                return E_GfeResponsiblePartyT.Seller;
            }

            set
            {
            }
        }

        /// <summary>
        /// Gets or sets the formula that will be use to compute the total amount. This ensures proper serialization.
        /// </summary>
        /// <value>The formula that will be use to compute the total amount.</value>
        [DataMember(Name = "f")]
        private SellerClosingCostFormula Formula
        {
            get
            {
                return (SellerClosingCostFormula)this.ChildLoanFormula;
            }

            set
            {
                this.ChildLoanFormula = value;
            }
        }

        /// <summary>
        /// Gets or sets a list of payments associate with the closing cost. This ensures proper serialization. The getter will be a different list than in the base class.
        /// Direct adds/removes from this list will not be reflected in the payment list stored in the base class. So use ChildPaymentList for that.
        /// </summary>
        /// <value>A list of all the payments associate with the closing cost.</value>
        [DataMember(Name = "pmts")]
        private List<SellerClosingCostFeePayment> PaymentList
        {
            get
            {
                List<SellerClosingCostFeePayment> payments = new List<SellerClosingCostFeePayment>();

                foreach (SellerClosingCostFeePayment payment in this.ChildPaymentList)
                {
                    payments.Add(payment);
                }

                return payments;
            }

            set
            {
                if (this.ChildPaymentList == null)
                {
                    this.ChildPaymentList = new List<LoanClosingCostFeePayment>();
                }

                if (value != null)
                {
                    foreach (SellerClosingCostFeePayment payment in value)
                    {
                        this.ChildPaymentList.Add(payment);
                    }
                }
                else
                {
                    this.ChildPaymentList = new List<LoanClosingCostFeePayment>();
                }
            }
        }

        /// <summary>
        /// Clones the seller closing cost fee.
        /// </summary>
        /// <returns>The clone of this seller closing cost fee.</returns>
        public override object Clone()
        {
            SellerClosingCostFormula formular = (SellerClosingCostFormula)this.Formula.Clone();
            SellerClosingCostFee clonedFee = (SellerClosingCostFee)this.MemberwiseClone();
            clonedFee.UniqueId = Guid.NewGuid();
            clonedFee.ParentClosingCostSet = null;
            clonedFee.ChildPaymentList = new List<LoanClosingCostFeePayment>();
            clonedFee.Formula = formular;
            clonedFee.Formula.SetClosingCostFee(clonedFee);

            foreach (SellerClosingCostFeePayment payment in this.ChildPaymentList)
            {
                SellerClosingCostFeePayment clonedP = (SellerClosingCostFeePayment)payment.Clone();
                clonedP.SetParent(clonedFee);
                clonedFee.ChildPaymentList.Add(clonedP);
            }

            return clonedFee;
        }

        /// <summary>
        /// Converts this seller fee to a BorrowerClosingCostFeeClosingCostFee. Most likely temporary.
        /// </summary>
        /// <returns>The seller fee converted to a ClosingCostFee.</returns>
        public BorrowerClosingCostFee ConvertToBorrowerClosingCostFee()
        {
            BorrowerClosingCostFee convertedFee = new BorrowerClosingCostFee();

            convertedFee.BaseAmount = this.BaseAmount;
            convertedFee.Beneficiary = this.Beneficiary;
            convertedFee.BeneficiaryDescription = this.BeneficiaryDescription;
            convertedFee.BeneficiaryAgentId = this.BeneficiaryAgentId;
            convertedFee.CanShop = this.CanShop;
            convertedFee.ClosingCostFeeTypeId = this.ClosingCostFeeTypeId;
            convertedFee.Description = this.Description;
            convertedFee.Dflp = this.Dflp;
            convertedFee.DidShop = this.DidShop;
            convertedFee.DisableBeneficiaryAutomation = this.DisableBeneficiaryAutomation;
            convertedFee.FormulaT = this.FormulaT;
            convertedFee.GfeResponsiblePartyT = this.GfeResponsiblePartyT;
            convertedFee.GfeSectionT = this.GfeSectionT;
            convertedFee.HudLine = this.HudLine;
            convertedFee.IntegratedDisclosureSectionT = this.IntegratedDisclosureSectionT;
            convertedFee.IsAffiliate = this.IsAffiliate;
            convertedFee.IsApr = this.IsApr;
            convertedFee.IsBonaFide = this.IsBonaFide;
            convertedFee.IsFhaAllowable = this.IsFhaAllowable;
            convertedFee.IsOptional = this.IsOptional;
            convertedFee.IsThirdParty = this.IsThirdParty;
            convertedFee.IsTitleFee = this.IsTitleFee;
            convertedFee.IsVaAllowable = this.IsVaAllowable;
            convertedFee.LegacyGfeFieldT = this.LegacyGfeFieldT;
            convertedFee.MismoFeeT = this.MismoFeeT;
            convertedFee.NewFeeIndex = this.NewFeeIndex;
            convertedFee.NumberOfPeriods = this.NumberOfPeriods;
            convertedFee.OriginalDescription = this.OriginalDescription;
            convertedFee.Percent = this.Percent;
            convertedFee.PercentBaseT = this.PercentBaseT;
            convertedFee.PercentTotalAmount = this.PercentTotalAmount;
            convertedFee.UniqueId = this.UniqueId;

            convertedFee.ClearPayments();
            foreach (SellerClosingCostFeePayment payment in this.ChildPaymentList)
            {
                BorrowerClosingCostFeePayment newPayment = new BorrowerClosingCostFeePayment();
                newPayment.Amount = payment.Amount;
                newPayment.Entity = payment.Entity;
                newPayment.GfeClosingCostFeePaymentTimingT = payment.GfeClosingCostFeePaymentTimingT;
                newPayment.Id = payment.Id;
                newPayment.IsMade = payment.IsMade;
                newPayment.PaidByT = payment.PaidByT;
                newPayment.PaymentDate = payment.PaymentDate;
                newPayment.IsSystemGenerated = payment.IsSystemGenerated;
                ////convertedFee.AddPayment(newPayment);
                newPayment.ResponsiblePartyT = payment.ResponsiblePartyT;
                newPayment.SetParent(convertedFee);
                convertedFee.ChildPaymentList.Add(newPayment);
            }

            return convertedFee;
        }

        /// <summary>
        /// Validates the Seller Fee for read-only changes. 
        /// </summary>
        /// <param name="newBaseFee">The new Fee to check.</param>
        /// <param name="message">The error message, if any.</param>
        /// <param name="args">Info needed to validate this fee.</param>
        /// <returns>True if the new Fee is valid, false otherwise.</returns>
        public override bool ValidateFeeModifcation(BaseClosingCostFee newBaseFee, out string message, ValidationParamContainer args)
        {
            if (newBaseFee == null)
            {
                message = "New Fee is null.";
                return false;
            }

            SellerClosingCostFee newFee = (SellerClosingCostFee)newBaseFee;

            if (this.ClosingCostFeeTypeId != newFee.ClosingCostFeeTypeId)
            {
                message = "Comparing two different fees.";
                return false;
            }

            if (args.IsRequireFeesFromDropDown.HasValue)
            {
                bool requireFeesFromDropDown = args.IsRequireFeesFromDropDown.Value;
                if (!this.SourceFeeTypeId.HasValue && this.InternalHudline != newFee.InternalHudline &&
                    (this.IsSystemLegacyFee || requireFeesFromDropDown))
                {
                    message = "Hudline for fee " + this.Description + " cannot be changed. It is read only.";
                    return false;
                }

                if (this.IsApr != newFee.IsApr && requireFeesFromDropDown)
                {
                    message = "APR checkbox for fee " + this.Description + " cannot be changed. It is read only.";
                    return false;
                }

                if (this.IsFhaAllowable != newFee.IsFhaAllowable && requireFeesFromDropDown)
                {
                    message = "FHA checkbox for fee " + this.Description + " cannot be changed. It is read only.";
                    return false;
                }

                if (this.CanShop != newFee.CanShop && requireFeesFromDropDown)
                {
                    message = "Can Shop checkbox for fee " + this.Description + " cannot be changed. It is read only.";
                    return false;
                }

                if (this.IsTitleFee != newFee.IsTitleFee && requireFeesFromDropDown)
                {
                    message = "Title checkbox for fee " + this.Description + " cannot be changed. It is read only.";
                    return false;
                }

                if (this.IsOptional != newFee.IsOptional && (requireFeesFromDropDown || this.InternalIntegratedDisclosureSectionT != E_IntegratedDisclosureSectionT.SectionH))
                {
                    message = "Optional checkbox for fee " + this.Description + " cannot be changed. It is read only.";
                    return false;
                }
            }

            if (!base.ValidateFeeModifcation(newBaseFee, out message, args))
            {
                return false;
            }

            // It passed all those checks. It should be validated by now.
            message = "Fee " + this.Description + " is valid.";
            return true;
        }        

        /// <summary>
        /// Ensures that the formula object for this fee is correct. Does not automatically correct the formula type.
        /// </summary>
        internal override void EnsureFormula()
        {
            bool wasNewFormula = false;
            if (this.ChildLoanFormula == null)
            {
                this.ChildLoanFormula = this.CreateNewFormula();
                wasNewFormula = true;
            }

            // 12/30/2014 dd - Always update the formula object with latest closing cost set object.
            this.ChildLoanFormula.SetClosingCostFee(this);

            if (wasNewFormula)
            {
                this.ChildLoanFormula.EnsureCorrectness();
            }
        }

        /// <summary>
        /// Creates a new SellerClosingCostFormula.
        /// </summary>
        /// <returns>The new SellerClosingCostFormula.</returns>
        protected override LoanClosingCostFormula CreateNewFormula()
        {
            return new SellerClosingCostFormula();
        }

        /// <summary>
        /// Creates a new SellerClosingCostFeePayment.
        /// </summary>
        /// <returns>The new SellerClosingCostFeePayment.</returns>
        protected override LoanClosingCostFeePayment CreateNewPayment()
        {
            return new SellerClosingCostFeePayment();
        }
    }
}
