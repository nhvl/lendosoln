﻿// <copyright file="SellerClosingCostFeeSection.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   6/22/2015
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;

    /// <summary>
    /// Class representing a seller responsible fee section.
    /// </summary>
    [DataContract]
    public class SellerClosingCostFeeSection : BaseClosingCostFeeSection
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SellerClosingCostFeeSection" /> class.
        /// </summary>
        /// <param name="closingCostViewT">The view type.</param>
        /// <param name="closingCostSet">The parent set to pull fees from.</param>
        /// <param name="setFitler">The filter for master closing cost set.</param>
        /// <param name="sectionName">The section name.</param>
        /// <param name="predicate">The predicate used to filter fees.</param>
        /// <param name="feeTemplate">The fee template for this section.</param>
        /// <param name="hudLineStart">The first hudline number for this section.</param>
        /// <param name="hudLineEnd">The last hudline number for this section.</param>
        /// <param name="sectionT">The disclosure section type for this section.</param>
        public SellerClosingCostFeeSection(E_ClosingCostViewT closingCostViewT, SellerClosingCostSet closingCostSet, Func<BaseClosingCostFee, bool> setFitler, string sectionName, Func<LoanClosingCostFee, bool> predicate, SellerClosingCostFee feeTemplate, int hudLineStart, int hudLineEnd, E_IntegratedDisclosureSectionT sectionT)
            : base(closingCostViewT, closingCostSet, setFitler, sectionName, o => predicate((SellerClosingCostFee)o), feeTemplate, hudLineStart, hudLineEnd, sectionT) 
        {
        }

        /// <summary>
        /// Gets the sum of all the fees in this section.
        /// </summary>
        /// <value>The sum of all the fees in this section.</value>
        [DataMember(Name = "TotalAmount")]
        public string TotalAmount_rep
        {
            get
            {
                decimal total = Tools.SumMoney(this.FilteredClosingCostFeeList.Select(f => ((SellerClosingCostFee)f).TotalAmount));

                if (this.SetAsSellerSet != null && this.SetAsSellerSet.LosConvert != null)
                {
                    return this.SetAsSellerSet.LosConvert.ToMoneyString(total, FormatDirection.ToRep);
                }
                else
                {
                    LosConvert losConvert = new LosConvert();

                    return losConvert.ToMoneyString(total, FormatDirection.ToRep);
                }
            }

            private set
            {
            }
        }

        /// <summary>
        /// Gets or sets the section's fee list as SellerClosingCostFees. USE THIS FOR SERIALIZATION PLEASE.
        /// </summary>
        /// <value>The section's fee list as SellerClosingCostFees.</value>
        [DataMember]
        private IEnumerable<SellerClosingCostFee> ClosingCostFeeList
        {
            get
            {
                List<SellerClosingCostFee> list = new List<SellerClosingCostFee>();

                foreach (SellerClosingCostFee fee in this.FilteredClosingCostFeeList)
                {
                    list.Add(fee);
                }

                return list;
            }

            set
            {
                this.FilteredClosingCostFeeList = (IEnumerable<BaseClosingCostFee>)value;
            }
        }

        /// <summary>
        /// Gets or sets the section's fee template as a SellerClosingCostFee. USE THIS FOR SERIALIZATION PLEASE.
        /// </summary>
        /// <value>The fee template as a SellerClosingCostFee.</value>
        [DataMember]
        private SellerClosingCostFee FeeTemplate
        {
            get
            {
                return (SellerClosingCostFee)this.SectionFeeTemplate;
            }

            set
            {
                this.SectionFeeTemplate = value;
            }
        }

        /// <summary>
        /// Gets the associated closing cost set as a Seller set.
        /// </summary>
        /// <value>The associated closing cost set as a borrower set.</value>
        private SellerClosingCostSet SetAsSellerSet
        {
            get
            {
                return (SellerClosingCostSet)this.ClosingCostSet;
            }
        }
    }
}
