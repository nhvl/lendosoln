﻿// <copyright file="FeeSetupClosingCostFormula.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   7/9/2015
// </summary>
namespace DataAccess
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Class representing a broker level closing cost formula.
    /// </summary>
    public class FeeSetupClosingCostFormula : BaseClosingCostFormula
    {
        /// <summary>
        /// Clones the given object.
        /// </summary>
        /// <returns>A closing cost fee with the exact values except ID.</returns>
        public override object Clone()
        {
            FeeSetupClosingCostFormula formula = (FeeSetupClosingCostFormula)this.MemberwiseClone();
            return formula;
        }
    }
}
