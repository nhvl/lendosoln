﻿// <copyright file="BaseClosingCostFee.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   7/9/2015
// </summary>
namespace DataAccess
{
    using System;
    using System.Runtime.Serialization;
    using DataAccess.PathDispatch;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// The absolute base class for all closing cost fees. DO NOT SERIALIZE/DESERIALIZE DIRECTLY INTO THIS CLASS.
    /// </summary>
    [DataContract]
    [KnownType(typeof(FeeSetupClosingCostFee))]
    [KnownType(typeof(LoanClosingCostFee))]
    public abstract class BaseClosingCostFee : ICloneable, IPathResolvable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BaseClosingCostFee" /> class.
        /// </summary>
        public BaseClosingCostFee()
        {
            this.UniqueId = Guid.NewGuid();
        }

        /// <summary>
        /// Gets or sets the line number on the HUD-1 that the fee appears on.
        /// Multiples of 100 indicate that the fee should appear in the corresponding section but do not specify a particular line.
        /// </summary>
        /// <value>The line number on the HUD-1 that the fee appears on.</value>
        public virtual int HudLine
        {
            get
            {
                bool clearCache = false;
                string predeterminedHudLine = ClosingCostSetUtils.GetPredeterminedHudline(this.ClosingCostFeeTypeId);
                if (predeterminedHudLine != null)
                {
                    clearCache = predeterminedHudLine != this.InternalHudline;
                    this.InternalHudline = predeterminedHudLine;
                }

                if (string.IsNullOrEmpty(this.InternalHudline))
                {
                    if (this.LegacyGfeFieldT == E_LegacyGfeFieldT.s800U1F)
                    {
                        clearCache = this.InternalHudline != "813";
                        this.InternalHudline = "813";
                    }
                    else if (this.LegacyGfeFieldT == E_LegacyGfeFieldT.s800U2F)
                    {
                        clearCache = this.InternalHudline != "814";
                        this.InternalHudline = "814";
                    }
                    else if (this.LegacyGfeFieldT == E_LegacyGfeFieldT.s800U3F)
                    {
                        clearCache = this.InternalHudline != "815";
                        this.InternalHudline = "815";
                    }
                    else if (this.LegacyGfeFieldT == E_LegacyGfeFieldT.s800U4F)
                    {
                        clearCache = this.InternalHudline != "816";
                        this.InternalHudline = "816";
                    }
                    else if (this.LegacyGfeFieldT == E_LegacyGfeFieldT.s800U5F)
                    {
                        clearCache = this.InternalHudline != "817";
                        this.InternalHudline = "817";
                    }
                }

                if (clearCache)
                {
                    this.ClearParentSetLookupCache();
                }

                return ClosingCostSetUtils.SafeInt(this.InternalHudline);
            }

            set
            {
                this.InternalHudline = value.ToString();
                this.ClearParentSetLookupCache();
            }
        }

        /// <summary>
        /// Gets or sets the line number on the HUD-1 that the fee appears on.
        /// </summary>
        /// <value>The line number on the HUD-1 that the fee appears on.</value>
        public string HudLine_rep
        {
            get
            {
                return this.HudLine.ToString();
            }

            set
            {
                this.HudLine = ClosingCostSetUtils.SafeInt(value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not this fee is optional.
        /// </summary>
        /// <value>Whether this fee is optional.</value>
        [DataMember(Name = "is_optional")]
        public bool IsOptional
        {
            get
            {
                if (this.IntegratedDisclosureSectionT != E_IntegratedDisclosureSectionT.SectionH)
                {
                    this.InternalIsOptional = false;
                }

                return this.InternalIsOptional;
            }

            set
            {
                this.InternalIsOptional = value;
            }
        }

        /// <summary>
        /// Gets or sets the block that fee appears on the new GFE.
        /// </summary>
        /// <value>The block that the fee appears on the new GFE.</value>
        [DataMember(Name = "disc_sect")]
        public E_IntegratedDisclosureSectionT IntegratedDisclosureSectionT
        {
            get
            {
                E_IntegratedDisclosureSectionT? section = ClosingCostSetUtils.DetermineConstantSectionForFee(this.SourceFeeTypeId ?? this.ClosingCostFeeTypeId);
                if (section.HasValue)
                {
                    return section.Value;
                }
                else
                {
                    return this.InternalIntegratedDisclosureSectionT;
                }
            }

            set
            {
                this.InternalIntegratedDisclosureSectionT = value;
            }
        }

        /// <summary>
        /// Gets or sets a unique Id for the fee. Do not use this to compare a fee to anything. Use <see cref="ClosingCostFeeTypeId"/>.
        /// </summary>
        /// <value>Unique id of the fee.</value>
        [DataMember(Name = "id")]
        public Guid UniqueId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the ClosingCostFeeTypeId of the fee type this fee is based on.
        /// </summary>
        /// <remarks>
        /// If SourceFeeTypeId is not null then this fee can be considered a sub fee.
        /// </remarks>
        /// <value>Source fee type ID.</value>
        [DataMember(Name = "sourceid")]
        public Guid? SourceFeeTypeId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets id of the closing cost fee type in lender account.
        /// </summary>
        /// <value>Id of the closing cost fee type in lender account.</value>
        [DataMember(Name = "typeid")]
        public Guid ClosingCostFeeTypeId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets description of the fee.
        /// </summary>
        /// <value>Description of the fee.</value>
        [DataMember(Name = "desc")]
        public virtual string Description
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the legacy LendingQB GFE closing cost dollar amount field that corresponds to the fee.
        /// </summary>
        /// <value>The legacy LendingQB GFE closing cost dollar amount field that corresponds to the fee.</value>
        [DataMember(Name = "legacy")]
        public virtual E_LegacyGfeFieldT LegacyGfeFieldT
        {
            get
            {
                return this.InternalLegacyGfeFieldT;
            }

            set
            {
                this.InternalLegacyGfeFieldT = value;
            }
        }

        /// <summary>
        /// Gets or sets the RESPA FEE MISMO type mapping.
        /// </summary>
        /// <value>The RESPA FEE MISMO type mapping.</value>
        [DataMember(Name = "mismo")]
        public virtual E_ClosingCostFeeMismoFeeT MismoFeeT
        {
            get
            {
                E_ClosingCostFeeMismoFeeT? mismoFeeType = ClosingCostSetUtils.GetMismoFeeType(this.SourceFeeTypeId ?? this.ClosingCostFeeTypeId, false);
                if (mismoFeeType.HasValue)
                {
                    this.InternalMismoFeeT = mismoFeeType.Value;
                }

                return this.InternalMismoFeeT;
            }

            set
            {
                this.InternalMismoFeeT = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the fee is considered a "Prepaid Finance Charge" (AKA APR fee) under Regulation Z.
        /// </summary>
        /// <value>Whether the fee is considered a "Prepaid Finance Charge" (AKA APR fee) under Regulation Z.</value>
        [DataMember(Name = "apr")]
        public bool IsApr
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not this fee can be charged to the borrower for an FHA loan.
        /// </summary>
        /// <value>Whether or not this fee can be charged to the borrower for an FHA loan.</value>
        [DataMember(Name = "fha")]
        public bool IsFhaAllowable
        {
            get;
            set;
        }
        
        /// <summary>
        /// Gets or sets a value indicating whether or not this fee can be charge to the borrower for a VA loan.
        /// </summary>
        /// <value>Whether or not this fee can be charged to the VA loan.</value>
        [DataMember(Name = "va")]
        public bool IsVaAllowable
        {
            get;
            set;
        }

        /// <summary>
        /// Gets an enum that indicates what GFE Sections a fee can be in. Used to render UI. Only for System Fees.
        /// </summary>
        /// <value>Indicates what GFE Sections a fee can be in.</value>
        [DataMember(Name = "gfeGrps")]
        public E_GfeSectionTGroups GfeSectionGroup
        {
            get
            {
                E_GfeSectionT? calcSec = ClosingCostSetUtils.GetGfeSectionType(this.SourceFeeTypeId ?? this.ClosingCostFeeTypeId);
                if (calcSec.HasValue)
                {
                    E_GfeSectionT sectionValue = calcSec.Value;
                    if (sectionValue == E_GfeSectionT.B1)
                    {
                        return E_GfeSectionTGroups.B1Only;
                    }
                    else if (sectionValue == E_GfeSectionT.B2)
                    {
                        return E_GfeSectionTGroups.B2Only;
                    }
                    else if (sectionValue == E_GfeSectionT.B3)
                    {
                        return E_GfeSectionTGroups.B3Only;
                    }
                    else if (sectionValue == E_GfeSectionT.B4)
                    {
                        return E_GfeSectionTGroups.B4Only;
                    }
                    else if (sectionValue == E_GfeSectionT.B5)
                    {
                        return E_GfeSectionTGroups.B5Only;
                    }
                    else if (sectionValue == E_GfeSectionT.B6)
                    {
                        return E_GfeSectionTGroups.B6Only;
                    }
                    else if (sectionValue == E_GfeSectionT.B7)
                    {
                        return E_GfeSectionTGroups.B7Only;
                    }
                    else if (sectionValue == E_GfeSectionT.B8)
                    {
                        return E_GfeSectionTGroups.B8Only;
                    }
                    else if (sectionValue == E_GfeSectionT.B9)
                    {
                        return E_GfeSectionTGroups.B9Only;
                    }
                    else if (sectionValue == E_GfeSectionT.B10)
                    {
                        return E_GfeSectionTGroups.B10Only;
                    }
                    else if (sectionValue == E_GfeSectionT.B11)
                    {
                        return E_GfeSectionTGroups.B11Only;
                    }
                    else
                    {
                        return E_GfeSectionTGroups.None;
                    }
                }
                else
                {
                    if (this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1100ClosingEscrowFeeTypeId ||
                        this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1100DocPreparationFeeTypeId ||
                        this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1100NotaryFeeTypeId ||
                        this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1100AttorneyFeeTypeId)
                    {
                        return E_GfeSectionTGroups.B4andB6;
                    }
                }

                return E_GfeSectionTGroups.None;
            }

            private set
            {
            }
        }

        /// <summary>
        /// Gets or sets the block that the fee appears in on the GFE.
        /// </summary>
        /// <value>The block that the fee appears in on the GFE.</value>
        [DataMember(Name = "section")]
        public E_GfeSectionT GfeSectionT
        {
            get
            {
                E_GfeSectionT? calcSec = ClosingCostSetUtils.GetGfeSectionType(this.SourceFeeTypeId ?? this.ClosingCostFeeTypeId);
                if (calcSec.HasValue)
                {
                    this.InternalGfeSectionT = calcSec.Value;
                }

                return this.InternalGfeSectionT;
            }

            set
            {
                this.InternalGfeSectionT = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not this fee is to pay for title services.
        /// </summary>
        /// <value>Whether this fee is to pay for title services.</value>
        [DataMember(Name = "is_title")]
        public virtual bool IsTitleFee
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether it is a third party.
        /// </summary>
        /// <value>Whether is is a third party.</value>
        [DataMember(Name = "tp")]
        public virtual bool IsThirdParty
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether a borrower can shop for provider.
        /// </summary>
        /// <value>Whether a borrower can shop for provider.</value>
        [DataMember(Name = "can_shop")]
        public virtual bool CanShop
        {
            get
            {
                if (this.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionB)
                {
                    this.CanShopImpl = false;
                }
                else if (this.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionC)
                {
                    this.CanShopImpl = true;
                }

                return this.CanShopImpl;
            }

            set
            {
                this.CanShopImpl = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the loan is deduced from loan proceeds.
        /// </summary>
        /// <value>Whether the loan is deducted from loan proceeds.</value>
        [DataMember(Name = "dflp")]
        public bool Dflp
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether provider is affiliate with lender.
        /// </summary>
        /// <value>Whether provider is affiliate with lender.</value>
        [DataMember(Name = "aff")]
        public virtual bool IsAffiliate
        {
            get
            {
                if (this.IsThirdParty == false)
                {
                    this.IsAffiliateImpl = false;
                }

                return this.IsAffiliateImpl;
            }

            set
            {
                this.IsAffiliateImpl = value;
            }
        }

        /// <summary>
        /// Gets or sets the provider who provided the service and will ultimately receive the payment for it.
        /// </summary>
        /// <value>The provide who provided the service and will ultimately receive the payment for it.</value>
        [DataMember(Name = "bene")]
        public virtual E_AgentRoleT Beneficiary
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the index of this fee when it is added to a new closing cost set.
        /// </summary>
        /// <value>The new index of the fee in a closing cost set.</value>
        public int? NewFeeIndex
        {
            get;
            set;
        }

        /// <summary>
        /// Gets a value indicating whether this fee is part of the default legacy fee list we setup.
        /// </summary>
        /// <value>Whether this fee is part of the default legacy fee list.</value>
        [DataMember(Name = "is_system")]
        public bool IsSystemLegacyFee
        {
            get
            {
                return ClosingCostSetUtils.IsSystemLegacyFee(this.IntegratedDisclosureSectionT, this.ClosingCostFeeTypeId);
            }

            private set
            {
                // 12/17/2014 dd - This is required so we can exposed through JSON serialization.
            }
        }

        /// <summary>
        /// Gets a value indicating whether the fee can change loan estimate section.
        /// </summary>
        /// <value>A value indicating whether the fee can change loan estimate section.</value>
        [DataMember(Name = "changeSect")]
        public bool CanChangeSection
        {
            get
            {
                E_IntegratedDisclosureSectionT? section = ClosingCostSetUtils.DetermineConstantSectionForFee(this.SourceFeeTypeId ?? this.ClosingCostFeeTypeId);
                if (section.HasValue)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            private set
            {
            }
        }

        /// <summary>
        /// Gets a value indicating whether the fee is a system fee that is editable. 
        /// </summary>
        /// <value>A value indicating if the fee is a system fee that is editable.</value>
        [DataMember(Name = "editableSystem")]
        public bool IsEditableSystemFee
        {
            get
            {
                return ClosingCostSetUtils.IsEditableSystemFee(this.IsSystemLegacyFee, this.FormulaT, this.ClosingCostFeeTypeId);
            }

            private set
            {
            }
        }

        /// <summary>
        /// Gets a value indicating whether this fee can be manually added.
        /// </summary>
        /// <value>Whether this fee can be manually added.</value>
        public bool CanManuallyAddToEditor
        {
            get
            {
                if (ClosingCostSetUtils.ConstantPrepaidEscrowFeeList.Contains(this.ClosingCostFeeTypeId) ||
                   this.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionG ||
                   this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId ||
                   this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumUpfrontFeeTypeId ||
                   this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900VAFundingFeeTypeId ||
                   this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId ||
                   this.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId
                   || DefaultSystemClosingCostFee.FeeTemplate.IsFeeTemplateSystemFeeType(this.ClosingCostFeeTypeId))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether the fee is a system fee that can have its
        /// MISMO fee type modified. 
        /// </summary>
        /// <value>
        /// True if the fee is a system fee that can have its MISMO fee type modified,
        /// false otherwise.
        /// </value>
        [DataMember(Name = "modifiableMismoType")]
        public bool CanModifySystemFeeMismoType
        {
            get
            {
                return ClosingCostSetUtils.CanModifySystemFeeMismoType(this.IsSystemLegacyFee, this.ClosingCostFeeTypeId);
            }

            private set
            {
            }
        }

        /// <summary>
        /// Gets or sets the type of this formula. Mainly use to render formula.
        /// </summary>
        /// <value>The type of this formula.</value>
        public E_ClosingCostFeeFormulaT FormulaT
        {
            get
            {
                BaseClosingCostFormula closingCostFormula = this.GetFormula();
                return closingCostFormula.FormulaT;
            }

            set
            {
                BaseClosingCostFormula closingCostFormula = this.GetFormula();
                closingCostFormula.FormulaT = value;
            }
        }
        
        /// <summary>
        /// Gets or sets the string hudline for storage.
        /// </summary>
        /// <value>The string hudline for storage.</value>
        [DataMember(Name = "hudline")]
        protected string InternalHudline
        {
            get;
            set;
        }
        
        /// <summary>
        /// Gets or sets the legacy LendingQB GFE closing cost dollar amount field that corresponds to the fee.
        /// </summary>
        /// <value>The legacy LendingQB GFE closing cost dollar amount field that corresponds to the fee.</value>
        protected E_LegacyGfeFieldT InternalLegacyGfeFieldT
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the fee's GFE Section.
        /// </summary>
        /// <value>The fee's GFE Section.</value>
        protected E_GfeSectionT InternalGfeSectionT
        {
            get;
            set;
        }
        
        /// <summary>
        /// Gets or sets this fee's child formula. DO NOT SERIALIZE/DESERIALIZE THIS DIRECTLY.
        /// </summary>
        /// <value>The fee's child formula.</value>
        protected BaseClosingCostFormula ChildFormula
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the fee can be shopped for.
        /// </summary>
        /// <value>Value indicating whether the fee can be shopped for.</value>
        protected bool CanShopImpl
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not this fee is optional.
        /// </summary>
        /// <value>Whether or not this fee is optional.</value>
        protected bool InternalIsOptional
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the block that the fee appears on the new GFE.
        /// </summary>
        /// <value>The block that the fee appears on the new GFE.</value>
        protected E_IntegratedDisclosureSectionT InternalIntegratedDisclosureSectionT
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether provider is affiliate with lender.
        /// </summary>
        /// <value>Whether provider is affiliate with lender.</value>
        protected bool IsAffiliateImpl
        {
            get;
            set;
        }
        
        /// <summary>
        /// Gets or sets the RESPA FEE MISMO type mapping.
        /// </summary>
        /// <value>The RESPA FEE MISMO type mapping.</value>
        protected E_ClosingCostFeeMismoFeeT InternalMismoFeeT
        {
            get;
            set;
        }

        /// <summary>
        /// Checks to see if the fee is a valid fee. 
        /// </summary>
        /// <returns>Whether the fee is valid.</returns>
        public bool IsValidFee()
        {
            return this.HudLine != 0;
        }

        /// <summary>
        /// Checks if this fee can be removed.
        /// </summary>
        /// <param name="message">The error message.</param>
        /// <returns>True if it can, false otherwise.</returns>
        public abstract bool ValidateRemoveFee(out string message);

        /// <summary>
        /// Clones the fee.
        /// </summary>
        /// <returns>The cloned fee.</returns>
        public abstract object Clone();

        /// <summary>
        /// Checks if this fee can be modified. It is up to the child classes to make a public 'ValidateFee' method with the proper parameters.
        /// </summary>
        /// <param name="newBaseFee">The fee to check.</param>
        /// <param name="message">Error message if any.</param>
        /// <param name="args">The information needed to validate the new fee.</param>
        /// <returns>True if the fee has valid modifications, false otherwise.</returns>
        public virtual bool ValidateFeeModifcation(BaseClosingCostFee newBaseFee, out string message, ValidationParamContainer args)
        {
            return BaseClosingCostFee.ValidateGFESection(newBaseFee, out message);
        }

        /// <summary>
        /// Returns the value specified by the element.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns>The value specified by the element.</returns>
        public virtual object GetElement(IDataPathElement element)
        {
            return PathResolver.GetStringUsingReflection(this, element);
        }

        /// <summary>
        /// Validates the Fee's GFESection.
        /// </summary>
        /// <param name="newBaseFee">The fee to validate.</param>
        /// <param name="message">Output message.</param>
        /// <returns>True if validated, false otherwise.</returns>
        internal static bool ValidateGFESection(BaseClosingCostFee newBaseFee, out string message)
        {
            // Check the GFE Section to make sure that it has been assigned the right data.
            Guid feeTypeId = newBaseFee.ClosingCostFeeTypeId;
            E_GfeSectionT savedSection = newBaseFee.InternalGfeSectionT;

            if (feeTypeId == DefaultSystemClosingCostFee.Hud1100ClosingEscrowFeeTypeId ||
                feeTypeId == DefaultSystemClosingCostFee.Hud1100DocPreparationFeeTypeId ||
                feeTypeId == DefaultSystemClosingCostFee.Hud1100NotaryFeeTypeId ||
                feeTypeId == DefaultSystemClosingCostFee.Hud1100AttorneyFeeTypeId)
            {
                if (savedSection != E_GfeSectionT.B4 && savedSection != E_GfeSectionT.B6)
                {
                    message = "Fee " + newBaseFee.Description + " must be in Section B4 or B6.";
                    return false;
                }
            }
            else if (!newBaseFee.IsSystemLegacyFee && !newBaseFee.SourceFeeTypeId.HasValue)
            {
                // Custom Fees must be in certain sections depending on their hudline number.
                if (newBaseFee.HudLine >= ClosingCostSetUtils.Start800Hudline && newBaseFee.HudLine <= ClosingCostSetUtils.End800Hudline)
                {
                    if (savedSection != E_GfeSectionT.B1 && savedSection != E_GfeSectionT.B3 && savedSection != E_GfeSectionT.NotApplicable)
                    {
                        message = "Fee " + newBaseFee.Description + " must be in Section A1, B3, or N/A.";
                        return false;
                    }
                }
                else if (newBaseFee.HudLine >= ClosingCostSetUtils.Start900Hudline && newBaseFee.HudLine <= ClosingCostSetUtils.End900Hudline)
                {
                    if (savedSection != E_GfeSectionT.B3 && savedSection != E_GfeSectionT.B11 && savedSection != E_GfeSectionT.NotApplicable)
                    {
                        message = "Fee " + newBaseFee.Description + " must be in Section B3, B11, or N/A.";
                        return false;
                    }
                }
                else if (newBaseFee.HudLine >= ClosingCostSetUtils.Start1000Hudline && newBaseFee.HudLine <= ClosingCostSetUtils.End1000Hudline)
                {
                    if (savedSection != E_GfeSectionT.B9)
                    {
                        message = "Fee " + newBaseFee.Description + " must be in Section B9.";
                        return false;
                    }
                }
                else if ((newBaseFee.HudLine >= ClosingCostSetUtils.Start1100Hudline && newBaseFee.HudLine <= ClosingCostSetUtils.End1100Hudline) ||
                         (newBaseFee.HudLine >= ClosingCostSetUtils.Start1300Hudline && newBaseFee.HudLine <= ClosingCostSetUtils.End1300Hudline))
                {
                    if (savedSection != E_GfeSectionT.B4 && savedSection != E_GfeSectionT.B6 && savedSection != E_GfeSectionT.NotApplicable)
                    {
                        message = "Fee " + newBaseFee.Description + " must be in Section B4, B6 or N/A.";
                        return false;
                    }
                }
                else if (newBaseFee.HudLine >= ClosingCostSetUtils.Start1200Hudline && newBaseFee.HudLine <= ClosingCostSetUtils.End1200Hudline)
                {
                    if (savedSection != E_GfeSectionT.B7 && savedSection != E_GfeSectionT.B8)
                    {
                        message = "Fee " + newBaseFee.Description + " must be in Section B7 or B8.";
                        return false;
                    }
                }
            }

            message = "Fee GFE Section validated.";
            return true;
        }

        /// <summary>
        /// Ensures that the formula for this fee contains the correct values.
        /// </summary>
        internal abstract void EnsureFormula();

        /// <summary>
        /// Ensures that this fee contains the correct values.
        /// </summary>
        internal virtual void EnsureCorrectness()
        {
            this.EnsureFormula();
        }

        /// <summary>
        /// Sets the closing cost set. This doesn't do anything for this base class. Only for the LoanClosingCostFee class.
        /// </summary>
        /// <param name="o">The closing cost set to associate this fee to.</param>
        internal virtual void SetClosingCostSet(BaseClosingCostSet o)
        {
            // Do nothing since base does not have an associated closing cost set.
        }

        /// <summary>
        /// Clear's the parent set's lookup cache.
        /// </summary>
        protected virtual void ClearParentSetLookupCache()
        {
            // For base, do nothing since no Parent Set.
        }
        
        /// <summary>
        /// Gets the internal closing cost formula. Will instantiate a new object if it is not exists.
        /// </summary>
        /// <returns>A current instance of the closing cost formula.</returns>
        private BaseClosingCostFormula GetFormula()
        {
            this.EnsureFormula();
            return this.ChildFormula;
        }

        /// <summary>
        /// Perform initialization after object is deserialized.
        /// </summary>
        /// <param name="context">Serialized context.</param>
        [OnDeserialized]
        private void InitializeValueAfterDeserialized_Base(StreamingContext context)
        {
            if (string.IsNullOrEmpty(this.Description))
            {
                this.Description = string.Empty;
            }
        }
    }
}
