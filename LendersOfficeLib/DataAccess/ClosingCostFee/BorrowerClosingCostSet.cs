﻿// <copyright file="BorrowerClosingCostSet.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   7/9/2015
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.Security;

    /// <summary>
    /// The class representing a Borrower-Responsible closing cost set. 
    /// </summary>
    public class BorrowerClosingCostSet : LoanClosingCostSet
    {
        /// <summary>
        /// The standalone lender comp fee.
        /// </summary>
        private BorrowerClosingCostFee standaloneLenderOrigCompFee;

        /// <summary>
        /// Initializes a new instance of the <see cref="BorrowerClosingCostSet" /> class.
        /// </summary>
        /// <param name="json">The json to deserialize.</param>
        public BorrowerClosingCostSet(string json)
            : base(json)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BorrowerClosingCostSet" /> class.
        /// </summary>
        /// <param name="json">The json to deserialize.</param>
        /// <param name="skipInitEnumerable">Whether this set is for updating/clearing another set.</param>
        public BorrowerClosingCostSet(string json, bool skipInitEnumerable)
            : base(json, skipInitEnumerable)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BorrowerClosingCostSet" /> class.
        /// </summary>
        /// <param name="closingCostArchive">The archive to get the JSON from.</param>
        public BorrowerClosingCostSet(ClosingCostArchive closingCostArchive)
            : this(closingCostArchive.GetValue("sClosingCostSetJsonContent"))
        {
            this.ClosingCostArchive = closingCostArchive;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BorrowerClosingCostSet" /> class.
        /// </summary>
        /// <param name="closingCostArchive">The archive to get the JSON from.</param>
        /// <param name="convertor">The converter to use.</param>
        public BorrowerClosingCostSet(ClosingCostArchive closingCostArchive, LosConvert convertor)
            : this(closingCostArchive.GetValue("sClosingCostSetJsonContent"), convertor)
        {
            this.ClosingCostArchive = closingCostArchive;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BorrowerClosingCostSet" /> class.
        /// </summary>
        /// <param name="json">The json to deserialize.</param>
        /// <param name="convertor">The converter to use.</param>
        public BorrowerClosingCostSet(string json, LosConvert convertor)
            : base(json, convertor)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BorrowerClosingCostSet" /> class.
        /// </summary>
        /// <param name="loan">The data loan to associate with.</param>
        /// <param name="json">The json to deserialize.</param>
        public BorrowerClosingCostSet(CPageBase loan, string json)
            : base(loan, json)
        {
        }

        /// <summary>
        /// Gets or sets a value indicating what type of closing cost set this is.
        /// </summary>
        /// <value>A value indicating what type of closing cost set this is.</value>
        public override E_sClosingCostSetType SetType
        {
            get
            {
                return E_sClosingCostSetType.Fees;
            }

            protected set
            {
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the closing cost set is currently being migrated.
        /// </summary>
        /// <value>A value indicating whether the closing cost set is currently being migrated.</value>
        public bool IsMigration 
        { 
            get; 
            set; 
        }

        /// <summary>
        /// Gets the standalone originator compensation fee regardless of the checks put in place. Please don't use this unless absolutely necessary.
        /// </summary>
        /// <value>Gets the standalone originator compensation fee regardless of the checks.</value>
        public BorrowerClosingCostFee StandaloneOrigCompFeeRaw
        {
            get
            {
                return this.standaloneLenderOrigCompFee;
            }
        }

        /// <summary>
        /// Gets or sets the standalone lender paid Originator Compensation fee. Be very careful when using this.
        /// </summary>
        /// <value>
        /// If there is an associated data loan or archive, if it is in TRID mode and originator compensation is lender paid, then
        /// it will return the standalone fee. Otherwise, it will return null.
        /// </value>
        public BorrowerClosingCostFee StandaloneLenderOrigCompFee
        {
            get
            {
                E_sOriginatorCompensationPaymentSourceT paymentSource;
                E_sDisclosureRegulationT regulationT;
                bool gfeIsTpoTransaction;

                if (this.HasDataLoanAssociate)
                {
                    paymentSource = this.DataLoan.sOriginatorCompensationPaymentSourceT;
                    regulationT = this.DataLoan.sDisclosureRegulationT;
                    gfeIsTpoTransaction = this.DataLoan.sGfeIsTPOTransaction;
                }
                else if (this.HasClosingCostArchiveAssociate)
                {
                    paymentSource = (E_sOriginatorCompensationPaymentSourceT)this.ClosingCostArchive.GetCountValue("sOriginatorCompensationPaymentSourceT");
                    regulationT = (E_sDisclosureRegulationT)this.ClosingCostArchive.GetCountValue("sOriginatorCompensationPaymentSourceT");
                    gfeIsTpoTransaction = this.ClosingCostArchive.GetBitValue("sGfeIsTPOTransaction");
                }
                else
                {
                    return null;
                }

                if (paymentSource == E_sOriginatorCompensationPaymentSourceT.LenderPaid && regulationT == E_sDisclosureRegulationT.TRID &&
                    gfeIsTpoTransaction)
                {
                    if (this.standaloneLenderOrigCompFee != null)
                    {
                        this.standaloneLenderOrigCompFee.OrigCompSkipTRIDCheck = true;
                    }

                    return this.standaloneLenderOrigCompFee;
                }
                else
                {
                    return null;
                }
            }

            set
            {
                this.standaloneLenderOrigCompFee = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the set should reset system generated fees using the Fee Setup when they are removed from the set.
        /// </summary>
        /// <value>A value indicating whether the set should reset system generated fees using the Fee Setup when they are removed from the set.</value>
        private bool IsResetSystemFees
        {
            get;
            set;
        }

        /// <summary>
        /// Replace the current closing cost set with the new closing cost set.
        /// </summary>
        /// <param name="closingCostSet">Closing cost set to be replace with.</param>
        /// <param name="repSetFilter">The replacement set's filter.</param>
        public override void UpdateWith(BaseClosingCostSet closingCostSet, Func<BaseClosingCostFee, bool> repSetFilter)
        {
            Func<BaseClosingCostFee, bool> existingFeesFilter;
            if (this.HasDataLoanAssociate)
            {
                existingFeesFilter = fee => ClosingCostSetUtils.PrepsSecGMIFilter((BorrowerClosingCostFee)fee) &&
                                            ClosingCostSetUtils.OrigCompFilter(
                                             (BorrowerClosingCostFee)fee,
                                             this.DataLoan.sOriginatorCompensationPaymentSourceT,
                                             this.DataLoan.sDisclosureRegulationT,
                                             this.DataLoan.sGfeIsTPOTransaction);
            }
            else
            {
                existingFeesFilter = fee => fee.ClosingCostFeeTypeId != DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId &&
                                            ClosingCostSetUtils.PrepsSecGMIFilter((BorrowerClosingCostFee)fee);
            }

            this.UpdateWith(closingCostSet, repSetFilter, this.GetFees(existingFeesFilter));
        }

        /// <summary>
        /// Gets the first fee in the list the match the LendingQB legacy field type.
        /// If no fee is found then it will attempt to create new fee from lender settings.
        /// If still no fee from lender settings then a completely blank fee object will create.
        /// </summary>
        /// <param name="type">LendingQB legacy field type.</param>
        /// <param name="brokerId">Broker Id to find lender settings when could not find in list.</param>
        /// <param name="addFeeToSet">Determines whether the fee should be added if it does not exist.</param>
        /// <returns>The first fee with legacy field type.</returns>
        public override BaseClosingCostFee GetFirstFeeByLegacyType(E_LegacyGfeFieldT type, Guid brokerId, bool addFeeToSet)
        {
            BaseClosingCostFee firstcheck = this.CheckForCustomFee(type, brokerId, addFeeToSet);
            if (firstcheck != null)
            {
                return firstcheck;
            }

            Guid feeTypeId = Guid.Empty;

            if (type == E_LegacyGfeFieldT.sHazInsRsrv)
            {
                feeTypeId = DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId;
            }
            else if (type == E_LegacyGfeFieldT.sMInsRsrv)
            {
                feeTypeId = DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId;
            }
            else if (type == E_LegacyGfeFieldT.sRealETxRsrv)
            {
                feeTypeId = DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId;
            }
            else if (type == E_LegacyGfeFieldT.sSchoolTxRsrv)
            {
                feeTypeId = DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId;
            }
            else if (type == E_LegacyGfeFieldT.sFloodInsRsrv)
            {
                feeTypeId = DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId;
            }
            else if (type == E_LegacyGfeFieldT.s1006Rsrv)
            {
                feeTypeId = this.GetClosingCostFeeTypeForLineNum(E_CustomExpenseLineNumberT.Line1008);
                if (feeTypeId == Guid.Empty)
                {
                    feeTypeId = DefaultSystemClosingCostFee.Hud1000Custom1FeeTypeId;
                }
            }
            else if (type == E_LegacyGfeFieldT.s1007Rsrv)
            {
                feeTypeId = this.GetClosingCostFeeTypeForLineNum(E_CustomExpenseLineNumberT.Line1009);
                if (feeTypeId == Guid.Empty)
                {
                    feeTypeId = DefaultSystemClosingCostFee.Hud1000Custom2FeeTypeId;
                }
            }
            else if (type == E_LegacyGfeFieldT.sU3Rsrv)
            {
                feeTypeId = this.GetClosingCostFeeTypeForLineNum(E_CustomExpenseLineNumberT.Line1010);
                if (feeTypeId == Guid.Empty)
                {
                    feeTypeId = DefaultSystemClosingCostFee.Hud1000Custom3FeeTypeId;
                }
            }
            else if (type == E_LegacyGfeFieldT.sU4Rsrv)
            {
                feeTypeId = this.GetClosingCostFeeTypeForLineNum(E_CustomExpenseLineNumberT.Line1011);
                if (feeTypeId == Guid.Empty)
                {
                    feeTypeId = DefaultSystemClosingCostFee.Hud1000Custom4FeeTypeId;
                }
            }
            else if (type == E_LegacyGfeFieldT.sAggregateAdjRsrv)
            {
                feeTypeId = DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId;
            }
            else if (type == E_LegacyGfeFieldT.sGfeOriginatorComp_BorrowerPaid || type == E_LegacyGfeFieldT.sGfeOriginatorCompF || 
                     type == E_LegacyGfeFieldT.sGfeOriginatorCompF_Manual)
            {
                feeTypeId = DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId;
            }

            // Handle the originator compensation fee if it is in TRID and Lender Paid.
            if (feeTypeId == DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId &&
                ((this.HasDataLoanAssociate &&
                 this.DataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid &&
                 this.DataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID &&
                 this.DataLoan.sGfeIsTPOTransaction) ||
                (this.HasClosingCostArchiveAssociate &&
                 this.ClosingCostArchive.GetCountValue("sOriginatorCompensationPaymentSourceT") == (int)E_sOriginatorCompensationPaymentSourceT.LenderPaid &&
                 this.ClosingCostArchive.GetCountValue("sDisclosureRegulationT") == (int)E_sDisclosureRegulationT.TRID &&
                 this.ClosingCostArchive.GetBitValue("sGfeIsTPOTransaction"))))
            {
                // At this point, the standalone orig comp fee should be populated if the JSON had it to begin with.
                // So return it at this point. If it is null, we'll want to make it.
                if (this.StandaloneLenderOrigCompFee != null)
                {
                    return this.StandaloneLenderOrigCompFee;
                }

                string message = string.Empty;
                if (this.HasDataLoanAssociate)
                {
                    message = $"Standalone LP OC fee missing. Pulled from Data Loan";
                }
                else if (this.HasClosingCostArchiveAssociate)
                {
                    message = $"Standalone LP OC fee missing. Pulled from archive. Archive Id: {this.ClosingCostArchive.Id} Archive Date: {this.ClosingCostArchive.DateArchived} Archive Type: {this.ClosingCostArchive.ClosingCostArchiveType}.";
                }
                else
                {
                    message = $"Standalone LP OC fee missing. Not made from anything!!";
                }

                Tools.LogWarning(message);

                var misplacedOcFee = this.GetFees((fee) => fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId);
                if (misplacedOcFee.Any())
                {
                    return misplacedOcFee.First();
                }

                if (brokerId != Guid.Empty)
                {
                    if (this.BrokerClosingCostSet == null)
                    {
                        BrokerDB broker = BrokerDB.RetrieveById(brokerId);
                        this.BrokerClosingCostSet = broker.GetUnlinkedClosingCostSet();
                    }

                    FeeSetupClosingCostFee brokerOrigCompFee = (FeeSetupClosingCostFee)this.BrokerClosingCostSet.GetFees(fee => fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId).FirstOrDefault();
                    BorrowerClosingCostFee newOrigCompFee = null;
                    if (brokerOrigCompFee != null)
                    {
                        newOrigCompFee = brokerOrigCompFee.ConvertToBorrowerClosingCostFee();
                        newOrigCompFee.ClearBeneficiary();
                        newOrigCompFee.SetClosingCostSet(this);
                    }
                    else
                    {
                        newOrigCompFee = new BorrowerClosingCostFee();
                        newOrigCompFee.SetClosingCostSet(this);
                        newOrigCompFee.ClosingCostFeeTypeId = DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId;
                        newOrigCompFee.HudLine = 801;
                        newOrigCompFee.Description = "Originator compensation";
                        newOrigCompFee.LegacyGfeFieldT = E_LegacyGfeFieldT.sGfeOriginatorCompF;
                        newOrigCompFee.IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionA;
                        newOrigCompFee.Beneficiary = E_AgentRoleT.Broker;
                        newOrigCompFee.DisableBeneficiaryAutomation = this.IsMigration;
                        newOrigCompFee.IsApr = true;
                        newOrigCompFee.GfeSectionT = E_GfeSectionT.B1;
                        newOrigCompFee.MismoFeeT = E_ClosingCostFeeMismoFeeT.LoanOriginatorCompensation;

                        Tools.LogWarning("Adding blank fee with possibly incomplete information for legacy fee type " + type);
                    }

                    if (addFeeToSet)
                    {
                        this.StandaloneLenderOrigCompFee = newOrigCompFee;
                        return this.StandaloneLenderOrigCompFee;
                    }

                    return newOrigCompFee;
                }
            }

            BorrowerClosingCostFee foundFee = null;
            foreach (BorrowerClosingCostFee fee in this.ClosingCostFeeList)
            {
                if (fee.LegacyGfeFieldT == type || fee.ClosingCostFeeTypeId == feeTypeId)
                {
                    foundFee = fee;
                    break;
                }
            }

            if (foundFee != null)
            {
                // Return found fee if it is NOT a sub fee.
                if (!foundFee.SourceFeeTypeId.HasValue)
                {
                    return foundFee;
                }

                // OPM 457222 - If this is a subfee and this is a "get" operation
                // (ie. addFeeToSet == false) then return a fake sub fee.
                if (!addFeeToSet)
                {
                    return this.CreateDummyFeeFromSubFees(foundFee.SourceFeeTypeId.Value);
                }

                // If this is a "set" operation (ie. addFeeToSet == true)
                // then ignore found subfee. It will be overwritten in later code.
            }

            var brokerFee = this.GetBrokerFee(brokerId, type, addFeeToSet);
            if (brokerFee == null)
            {
                // 1/8/2015 dd - If no default fee from lender settings then create blank.
                BorrowerClosingCostFee blankFee = new BorrowerClosingCostFee();
                blankFee.SetClosingCostSet(this);
                blankFee.DisableBeneficiaryAutomation = this.IsMigration;
                blankFee.LegacyGfeFieldT = type;
                blankFee.ClosingCostFeeTypeId = feeTypeId;

                if (type == E_LegacyGfeFieldT.s1006Rsrv || type == E_LegacyGfeFieldT.s1007Rsrv ||
                    type == E_LegacyGfeFieldT.sU3Rsrv || type == E_LegacyGfeFieldT.sU4Rsrv)
                {
                    // These are the same defaults as are used in the initialize method for section 1000,
                    // excluding some of the fields which are set above.
                    blankFee.IsApr = false;
                    blankFee.GfeSectionT = E_GfeSectionT.B9;
                    blankFee.IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionG;
                    blankFee.Description = this.GetDescription(feeTypeId) + " Reserves";
                    blankFee.OriginalDescription = this.GetDefaultDescription(feeTypeId) + " Reserves";
                    blankFee.FormulaT = E_ClosingCostFeeFormulaT.InitialEscrowPaymentAtClosing;
                    blankFee.Payments.First().PaidByT = E_ClosingCostFeePaymentPaidByT.Borrower;
                    blankFee.Beneficiary = E_AgentRoleT.Lender;
                    blankFee.IsTitleFee = false;
                    blankFee.IsOptional = false;
                    blankFee.IsFhaAllowable = true;
                }
                else
                {
                    Tools.LogWarning("Adding blank fee with possibly incomplete information for legacy fee type " + type);
                }

                if (addFeeToSet)
                {
                    this.AddOrUpdate(blankFee);
                }

                return blankFee;
            }
            else
            {
                return brokerFee;
            }
        }

        /// <summary>
        /// Serializes the underlying fee list to JSON. This specifically serializes a list of BorrowerClosingCostFees.
        /// </summary>
        /// <returns>The json string.</returns>
        public override string ToJson()
        {
            return this.ToJson(false);
        }

        /// <summary>
        /// Serializes the underlying fee list to JSON. This specifically serializes a list of BorrowerClosingCostFees.
        /// </summary>
        /// <param name="includeOrigCompFee">Whether we include the orig comp fee or not.</param>
        /// <returns>The Json string.</returns>
        public string ToJson(bool includeOrigCompFee)
        {
            List<BorrowerClosingCostFee> list = new List<BorrowerClosingCostFee>();

            bool hasOrigComp = false;
            foreach (BorrowerClosingCostFee fee in this.ClosingCostFeeList)
            {
                // 3/23/2015 dd - Prune out all zero custom fee at the loan level.
                if (fee.LegacyGfeFieldT == E_LegacyGfeFieldT.Undefined && string.IsNullOrEmpty(fee.Description) && fee.TotalAmount == 0)
                {
                    continue;
                }

                // Correct the TP and AFF checkboxes
                var thirdPartyNotUsed = fee.IsThirdParty;
                var affiliateNotUsed = fee.IsAffiliate;

                // If any of these fees, then ensure that the total amount is up to date before serializing.
                // Can do this by simply calling total amount, which should update all the parts.
                if (ClosingCostSetUtils.ConstantInitialEscrowLegacyFeeList.Contains(fee.ClosingCostFeeTypeId) ||
                    ClosingCostSetUtils.ConstantPrepaidEscrowFeeList.Contains(fee.ClosingCostFeeTypeId))
                {
                    // The hudline needs to be completely updated before the JSON is saved.
                    if (fee.ClosingCostFeeTypeId != DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId &&
                        fee.ClosingCostFeeTypeId != DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId)
                    {
                        var hudNotUsed = fee.HudLine;
                    }

                    var updatedFeeAmount = fee.TotalAmount;
                    var oldFeeAmount = fee.TotalAmountOnDeserialization;
                    if ((updatedFeeAmount != 0 && oldFeeAmount == 0) || 
                        (updatedFeeAmount == 0 && oldFeeAmount != 0))
                    {
                        // We want to update this fee if the amount went from zero to non zero.
                        // Both scenerios are checked in order to keep the JSON as up to date as possible.
                        if (this.BrokerClosingCostSet != null && this.IsResetSystemFees)
                        {
                            FeeSetupClosingCostFee feeSetupFee = (FeeSetupClosingCostFee)this.BrokerClosingCostSet.FindFeeByTypeId(fee.ClosingCostFeeTypeId);
                            if (feeSetupFee != null)
                            {
                                fee.IsTitleFee = feeSetupFee.IsTitleFee;
                                fee.IsOptional = feeSetupFee.IsOptional;
                                fee.Description = feeSetupFee.Description;
                                fee.OriginalDescription = feeSetupFee.Description;
                                fee.GfeSectionT = feeSetupFee.GfeSectionT;
                                fee.IntegratedDisclosureSectionT = feeSetupFee.IntegratedDisclosureSectionT;
                                fee.IsApr = feeSetupFee.IsApr;
                                fee.IsFhaAllowable = feeSetupFee.IsFhaAllowable;
                                fee.Beneficiary = feeSetupFee.Beneficiary;
                                fee.Dflp = feeSetupFee.Dflp;
                                fee.IsThirdParty = feeSetupFee.IsThirdParty;
                                fee.IsAffiliate = feeSetupFee.IsAffiliate;
                                fee.CanShop = feeSetupFee.CanShop;
                                fee.MismoFeeT = feeSetupFee.MismoFeeT;
                                fee.IsVaAllowable = feeSetupFee.IsVaAllowable;
                            }
                        }
                    }

                    // Clear payments when these fees have been effectively 'removed' from the set. Prevents constantly changing payment ids.
                    if (updatedFeeAmount == 0 && oldFeeAmount != 0)
                    {
                        fee.ResetPayments();
                    }
                }

                if (fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId)
                {
                    if (includeOrigCompFee)
                    {
                        if (this.HasDataLoanAssociate && this.DataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
                        {
                            fee.OrigCompSkipTRIDCheck = true;
                        }

                        hasOrigComp = true;
                        list.Add(fee);
                        continue;
                    }
                    else
                    {
                        if (this.HasDataLoanAssociate &&
                            this.DataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID &&
                            this.DataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid &&
                            this.DataLoan.sGfeIsTPOTransaction)
                        {
                            continue;
                        }
                    }
                }
                
                list.Add(fee);
            }

            if (includeOrigCompFee && hasOrigComp == false && this.standaloneLenderOrigCompFee != null)
            {
                this.standaloneLenderOrigCompFee.OrigCompSkipTRIDCheck = true;
                list.Add(this.standaloneLenderOrigCompFee);
            }
            else if (hasOrigComp == false && this.StandaloneLenderOrigCompFee != null)
            {
                list.Add(this.StandaloneLenderOrigCompFee);
            }

            return ObsoleteSerializationHelper.JsonSerializeAndSanitize(list);
        }

        /// <summary>
        /// Returns JSON of list of fees after the hidden ones have been pruned out. Specifically serializes a list of BorrowerClosingCostFees.
        /// </summary>
        /// <returns>JSON of the list of fees.</returns>
        public string ToJsonPruneHiddenFees()
        {
            List<BorrowerClosingCostFee> list = new List<BorrowerClosingCostFee>();

            Func<BaseClosingCostFee, bool> shownInUIFilter;

            // ejm opm 365504 - Remove the discount point filter
            if (this.HasDataLoanAssociate)
            {
                shownInUIFilter = fee => ClosingCostSetUtils.PrepsSecGMIFilter((BorrowerClosingCostFee)fee) &&
                                         ClosingCostSetUtils.OrigCompFilter((BorrowerClosingCostFee)fee, this.DataLoan.sOriginatorCompensationPaymentSourceT, this.DataLoan.sDisclosureRegulationT, this.DataLoan.sGfeIsTPOTransaction);
            }
            else
            {
                shownInUIFilter = fee => ClosingCostSetUtils.PrepsSecGMIFilter((BorrowerClosingCostFee)fee);
            }

            bool hasBorrowerPaidOCFee = false;
            IEnumerable<BaseClosingCostFee> shownInUI = this.GetFees(shownInUIFilter);
            foreach (BorrowerClosingCostFee fee in shownInUI)
            {
                // 3/23/2015 dd - Prune out all zero custom fee at the loan level.
                if (fee.LegacyGfeFieldT == E_LegacyGfeFieldT.Undefined && string.IsNullOrEmpty(fee.Description) && fee.TotalAmount == 0)
                {
                    continue;
                }

                // OPM 214180 - Remove lender paid originator compensation from fees when loan is TRID 2015
                if (fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId)
                {
                    if (this.HasDataLoanAssociate &&
                        this.DataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID &&
                        this.DataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)
                    {
                        continue;
                    }

                    hasBorrowerPaidOCFee = true;
                }

                list.Add(fee);
            }

            if (!hasBorrowerPaidOCFee)
            {
                // Make sure to port over the standalone OC fee.
                var standaloneOcFee = this.StandaloneLenderOrigCompFee;
                if (standaloneOcFee != null)
                {
                    list.Add(standaloneOcFee);
                }
            }

            return ObsoleteSerializationHelper.JsonSerialize(list);
        }

        /// <summary>
        /// Gets a list of appropriate fee sections by view type. DO NOT USE THIS FOR SERIALIZATION/DESERIALIZATION.
        /// </summary>
        /// <param name="viewType">The view to construct the sections on.</param>
        /// <returns>The list of fee sections.</returns>
        public override IEnumerable<BaseClosingCostFeeSection> GetViewBase(E_ClosingCostViewT viewType)
        {
            if (viewType != E_ClosingCostViewT.LoanHud1 && viewType != E_ClosingCostViewT.LoanClosingCost)
            {
                throw new ArgumentException("BorrowerClosingCostSet does not take view type: " + Enum.GetName(typeof(E_ClosingCostViewT), viewType));
            }
        
            List<BaseClosingCostFeeSection> list = new List<BaseClosingCostFeeSection>();

            Func<BaseClosingCostFee, bool> sectionFilter;
            if (this.HasDataLoanAssociate)
            {
                sectionFilter = bFee => ClosingCostSetUtils.PrepsSecGMIFilter((BorrowerClosingCostFee)bFee) &&
                                        ClosingCostSetUtils.OrigCompFilter((BorrowerClosingCostFee)bFee, this.DataLoan.sOriginatorCompensationPaymentSourceT, this.DataLoan.sDisclosureRegulationT, this.DataLoan.sGfeIsTPOTransaction);
            }
            else if (this.HasClosingCostArchiveAssociate)
            {
                sectionFilter = bfee => ClosingCostSetUtils.PrepsSecGMIFilter((BorrowerClosingCostFee)bfee) &&
                                        ClosingCostSetUtils.OrigCompFilterWithArchive((BorrowerClosingCostFee)bfee, this.ClosingCostArchive);
            }
            else
            {
                sectionFilter = bFee => ClosingCostSetUtils.PrepsSecGMIFilter((BorrowerClosingCostFee)bFee);
            }
            
            switch (viewType)
            {
                case E_ClosingCostViewT.LoanHud1:
                    BorrowerClosingCostFee default800Fee = new BorrowerClosingCostFee()
                    {
                        HudLine = 800,
                        IsTitleFee = false,
                        IsOptional = false,
                        GfeSectionT = E_GfeSectionT.B1,
                        IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionA,
                        IsApr = true,
                        IsFhaAllowable = true,
                        Beneficiary = E_AgentRoleT.Lender,
                        IsThirdParty = false,
                        IsAffiliate = false,
                        CanShop = false,
                    };
                    default800Fee.SetClosingCostSet(this);

                    BorrowerClosingCostFee default900Fee = new BorrowerClosingCostFee()
                    {
                        HudLine = 900,
                        IsTitleFee = false,
                        IsOptional = false,
                        GfeSectionT = E_GfeSectionT.B11,
                        IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionF,
                        IsApr = false,
                        IsFhaAllowable = true,
                        Beneficiary = E_AgentRoleT.Lender,
                        IsThirdParty = false,
                        IsAffiliate = false,
                        CanShop = true,
                    };
                    default900Fee.SetClosingCostSet(this);

                    BorrowerClosingCostFee default1000Fee = new BorrowerClosingCostFee()
                    {
                        GfeSectionT = E_GfeSectionT.B1,
                        Beneficiary = E_AgentRoleT.Lender,
                        IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionA,
                    };
                    default1000Fee.SetClosingCostSet(this);

                    BorrowerClosingCostFee default1100Fee = new BorrowerClosingCostFee()
                    {
                        HudLine = 1100,
                        IsTitleFee = true,
                        IsOptional = false,
                        GfeSectionT = E_GfeSectionT.B4,
                        IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionC,
                        IsApr = false,
                        IsFhaAllowable = true,
                        Beneficiary = E_AgentRoleT.Other,
                        IsThirdParty = false,
                        IsAffiliate = false,
                        CanShop = true,
                    };
                    default1100Fee.SetClosingCostSet(this);

                    BorrowerClosingCostFee default1200Fee = new BorrowerClosingCostFee()
                    {
                        HudLine = 1200,
                        IsTitleFee = false,
                        IsOptional = false,
                        GfeSectionT = E_GfeSectionT.B8,
                        IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionE,
                        IsApr = false,
                        IsFhaAllowable = true,
                        Beneficiary = E_AgentRoleT.Other,
                        IsThirdParty = false,
                        IsAffiliate = false,
                        CanShop = false,
                    };
                    default1200Fee.SetClosingCostSet(this);

                    BorrowerClosingCostFee default1300Fee = new BorrowerClosingCostFee()
                    {
                        HudLine = 1300,
                        IsTitleFee = false,
                        IsOptional = false,
                        GfeSectionT = E_GfeSectionT.B6,
                        IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionC,
                        IsApr = true,
                        IsFhaAllowable = true,
                        Beneficiary = E_AgentRoleT.Other,
                        IsThirdParty = false,
                        IsAffiliate = false,
                        CanShop = true,
                    };
                    default1300Fee.SetClosingCostSet(this);

                    list.Add(new BorrowerClosingCostFeeSection(viewType, this, sectionFilter, ClosingCostSetUtils.Hud800ItemsPayable, o => o.CheckIfFeeIsInSection(ClosingCostSetUtils.Section800Filter), default800Fee, ClosingCostSetUtils.Start800Hudline, ClosingCostSetUtils.End800Hudline, E_IntegratedDisclosureSectionT.LeaveBlank));
                    list.Add(new BorrowerClosingCostFeeSection(viewType, this, sectionFilter, ClosingCostSetUtils.Hud900ItemsRequiredByLenderToBePaid, o => o.CheckIfFeeIsInSection(ClosingCostSetUtils.Section900Filter), default900Fee, ClosingCostSetUtils.Start900Hudline, ClosingCostSetUtils.End900Hudline, E_IntegratedDisclosureSectionT.LeaveBlank));
                    list.Add(new BorrowerClosingCostFeeSection(viewType, this, sectionFilter, ClosingCostSetUtils.Hud1000ReservesDeposited, o => o.CheckIfFeeIsInSection(ClosingCostSetUtils.Section1000Filter), default1000Fee, ClosingCostSetUtils.Start1000Hudline, ClosingCostSetUtils.End1000Hudline, E_IntegratedDisclosureSectionT.LeaveBlank));
                    list.Add(new BorrowerClosingCostFeeSection(viewType, this, sectionFilter, ClosingCostSetUtils.Hud1100Title, o => o.CheckIfFeeIsInSection(ClosingCostSetUtils.Section1100Filter), default1100Fee, ClosingCostSetUtils.Start1100Hudline, ClosingCostSetUtils.End1100Hudline, E_IntegratedDisclosureSectionT.LeaveBlank));
                    list.Add(new BorrowerClosingCostFeeSection(viewType, this, sectionFilter, ClosingCostSetUtils.Hud1200Government, o => o.CheckIfFeeIsInSection(ClosingCostSetUtils.Section1200Filter), default1200Fee, ClosingCostSetUtils.Start1200Hudline, ClosingCostSetUtils.End1200Hudline, E_IntegratedDisclosureSectionT.LeaveBlank));
                    list.Add(new BorrowerClosingCostFeeSection(viewType, this, sectionFilter, ClosingCostSetUtils.Hud1300AdditionalSettlementCharge, o => o.CheckIfFeeIsInSection(ClosingCostSetUtils.Section1300Filter), default1300Fee, ClosingCostSetUtils.Start1300Hudline, ClosingCostSetUtils.End1300Hudline, E_IntegratedDisclosureSectionT.LeaveBlank));
                    break;
                case E_ClosingCostViewT.LoanClosingCost:
                    BorrowerClosingCostFee defaultSectionAFee = new BorrowerClosingCostFee()
                    {
                        HudLine = 800,
                        IsTitleFee = false,
                        IsOptional = false,
                        GfeSectionT = E_GfeSectionT.B1,
                        IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionA,
                        IsApr = true,
                        IsFhaAllowable = true,
                        Beneficiary = E_AgentRoleT.Lender,
                        IsThirdParty = false,
                        IsAffiliate = false,
                        CanShop = false,
                    };
                    defaultSectionAFee.SetClosingCostSet(this);

                    BorrowerClosingCostFee defaultSectionBFee = new BorrowerClosingCostFee()
                    {
                        HudLine = 800,
                        IsTitleFee = false,
                        IsOptional = false,
                        GfeSectionT = E_GfeSectionT.B3,
                        IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionB,
                        IsApr = true,
                        IsFhaAllowable = true,
                        Beneficiary = E_AgentRoleT.Other,
                        IsThirdParty = false,
                        IsAffiliate = false,
                        CanShop = false,
                    };
                    defaultSectionBFee.SetClosingCostSet(this);

                    BorrowerClosingCostFee defaultSectionCFee = new BorrowerClosingCostFee()
                    {
                        HudLine = 1300,
                        IsTitleFee = false,
                        IsOptional = false,
                        GfeSectionT = E_GfeSectionT.B6,
                        IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionC,
                        IsApr = true,
                        IsFhaAllowable = true,
                        Beneficiary = E_AgentRoleT.Other,
                        IsThirdParty = false,
                        IsAffiliate = false,
                        CanShop = true,
                    };
                    defaultSectionCFee.SetClosingCostSet(this);

                    BorrowerClosingCostFee defaultSectionEFee = new BorrowerClosingCostFee()
                    {
                        HudLine = 1200,
                        IsTitleFee = false,
                        IsOptional = false,
                        GfeSectionT = E_GfeSectionT.B8,
                        IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionE,
                        IsApr = false,
                        IsFhaAllowable = true,
                        Beneficiary = E_AgentRoleT.Other,
                        IsThirdParty = false,
                        IsAffiliate = false,
                        CanShop = false,
                    };
                    defaultSectionEFee.SetClosingCostSet(this);

                    BorrowerClosingCostFee defaultSectionFFee = new BorrowerClosingCostFee()
                    {
                        HudLine = 900,
                        IsTitleFee = false,
                        IsOptional = false,
                        GfeSectionT = E_GfeSectionT.B11,
                        IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionF,
                        IsApr = false,
                        IsFhaAllowable = true,
                        Beneficiary = E_AgentRoleT.Lender,
                        IsThirdParty = false,
                        IsAffiliate = false,
                        CanShop = true,
                    };
                    defaultSectionFFee.SetClosingCostSet(this);

                    BorrowerClosingCostFee defaultSectionGFee = new BorrowerClosingCostFee();
                    defaultSectionGFee.IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionG;
                    defaultSectionGFee.Beneficiary = E_AgentRoleT.Lender;
                    defaultSectionGFee.ClosingCostFeeTypeId = DefaultSystemClosingCostFee.DefaultSectionGFeeTypeId;
                    defaultSectionGFee.SetClosingCostSet(this);

                    BorrowerClosingCostFee defaultSectionHFee = new BorrowerClosingCostFee()
                    {
                        HudLine = 1300,
                        IsTitleFee = false,
                        IsOptional = true,
                        GfeSectionT = E_GfeSectionT.B6,
                        IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionH,
                        IsApr = false,
                        IsFhaAllowable = true,
                        Beneficiary = E_AgentRoleT.Other,
                        IsThirdParty = false,
                        IsAffiliate = false,
                        CanShop = true,
                    };
                    defaultSectionHFee.SetClosingCostSet(this);

                    list.Add(new BorrowerClosingCostFeeSection(viewType, this, sectionFilter, ClosingCostSetUtils.SectionAOriginationCharges, o => o.CheckIfFeeIsInSection(ClosingCostSetUtils.SectionAFilter), defaultSectionAFee, ClosingCostSetUtils.Start800Hudline, ClosingCostSetUtils.End1300Hudline, E_IntegratedDisclosureSectionT.SectionA));
                    list.Add(new BorrowerClosingCostFeeSection(viewType, this, sectionFilter,  ClosingCostSetUtils.SectionBServiceYouCannotShopFor, o => o.CheckIfFeeIsInSection(ClosingCostSetUtils.SectionBFilter), defaultSectionBFee, ClosingCostSetUtils.Start800Hudline, ClosingCostSetUtils.End1300Hudline, E_IntegratedDisclosureSectionT.SectionB));
                    list.Add(new BorrowerClosingCostFeeSection(viewType, this, sectionFilter, ClosingCostSetUtils.SectionCServiceYouCanShopFor, o => o.CheckIfFeeIsInSection(ClosingCostSetUtils.SectionCFilter), defaultSectionCFee, ClosingCostSetUtils.Start800Hudline, ClosingCostSetUtils.End1300Hudline, E_IntegratedDisclosureSectionT.SectionC));
                    list.Add(new BorrowerClosingCostFeeSection(viewType, this, sectionFilter, ClosingCostSetUtils.SectionETaxes, o => o.CheckIfFeeIsInSection(ClosingCostSetUtils.SectionEFilter), defaultSectionEFee, ClosingCostSetUtils.Start800Hudline, ClosingCostSetUtils.End1300Hudline, E_IntegratedDisclosureSectionT.SectionE));
                    list.Add(new BorrowerClosingCostFeeSection(viewType, this, sectionFilter, ClosingCostSetUtils.SectionFPrepaids, o => o.CheckIfFeeIsInSection(ClosingCostSetUtils.SectionFFilter), defaultSectionFFee, ClosingCostSetUtils.Start800Hudline, ClosingCostSetUtils.End1300Hudline, E_IntegratedDisclosureSectionT.SectionF));
                    list.Add(new BorrowerClosingCostFeeSection(viewType, this, sectionFilter, ClosingCostSetUtils.SectionGInitialEscrow, o => o.CheckIfFeeIsInSection(ClosingCostSetUtils.SectionGFilter), defaultSectionGFee, ClosingCostSetUtils.Start800Hudline, ClosingCostSetUtils.End1300Hudline, E_IntegratedDisclosureSectionT.SectionG));
                    list.Add(new BorrowerClosingCostFeeSection(viewType, this, sectionFilter, ClosingCostSetUtils.SectionHOther, o => o.CheckIfFeeIsInSection(ClosingCostSetUtils.SectionHFilter), defaultSectionHFee, ClosingCostSetUtils.Start800Hudline, ClosingCostSetUtils.End1300Hudline, E_IntegratedDisclosureSectionT.SectionH));

                    break;
                case E_ClosingCostViewT.CombinedLenderTypeEstimate:
                case E_ClosingCostViewT.LeaveBlank:
                case E_ClosingCostViewT.LenderTypeEstimate:
                case E_ClosingCostViewT.LenderTypeHud1:
                case E_ClosingCostViewT.SellerResponsibleLenderTypeEstimate:
                case E_ClosingCostViewT.SellerResponsibleLoanHud1:
                default:
                    throw new UnhandledEnumException(viewType);
            }

            return list;
        }

        /// <summary>
        /// Gets the list of fee sections for serialization. This specifically returns a list of borrower sections. THIS IS THE ONE YOU WANT FOR SERIALIZATION.
        /// </summary>
        /// <param name="viewType">The view type to build the sections on.</param>
        /// <returns>The list of seller fee sections.</returns>
        public IEnumerable<BorrowerClosingCostFeeSection> GetViewForSerialization(E_ClosingCostViewT viewType)
        {
            return this.GetViewBase(viewType).Cast<BorrowerClosingCostFeeSection>();
        }

        /// <summary>
        /// Returns the discount fee, even if hidden by enumerators.
        /// </summary>
        /// <returns>The first discount based fee, null if none exists.</returns>
        public BorrowerClosingCostFee GetRawDiscount()
        {
            // The enumerators can filter out and modify the discount fee, which is desired for
            // most purposes. However, CoC needs its raw value, so directly pulling it here.
            // Rather than pile on more enumerators for very specific purposes, pulling here.  
            return (BorrowerClosingCostFee)this.ClosingCostFeeList.FirstOrDefault(fee => fee.FormulaT == E_ClosingCostFeeFormulaT.sLDiscnt);
        }

        /// <summary>
        /// Runs Initialization on the closing cost set again, recreating automated fees that may have been deleted.
        /// </summary>
        public void Reinitialize()
        {
            this.Initialize(this.ToJson(true));
        }

        /// <summary>
        /// Gets the closing cost fee type id of the fee that is associated with the expense that is on the line number.
        /// </summary>
        /// <param name="lineNum">The line number the fee is on.</param>
        /// <returns>Guid.Empty if not found, the fee type id otherwise.</returns>
        internal Guid GetClosingCostFeeTypeForLineNum(E_CustomExpenseLineNumberT lineNum)
        {
            BaseHousingExpense exp = null;

            if (this.DataLoan != null)
            {
                exp = this.DataLoan.sHousingExpenses.GetExpenseByLineNum(lineNum);
            }

            if (exp == null)
            {
                return Guid.Empty;
            }
            else
            {
                if (exp.HousingExpenseType == E_HousingExpenseTypeT.CondoHO6Insurance)
                {
                    return DefaultSystemClosingCostFee.Hud1000CondoHo6InsuranceFeeTypeId;
                }
                else if (exp.HousingExpenseType == E_HousingExpenseTypeT.WindstormInsurance)
                {
                    return DefaultSystemClosingCostFee.Hud1000WindStormInsuranceFeeTypeId;
                }
                else if (exp.HousingExpenseType == E_HousingExpenseTypeT.OtherTaxes1)
                {
                    return DefaultSystemClosingCostFee.Hud1000OtherTax1FeeTypeId;
                }
                else if (exp.HousingExpenseType == E_HousingExpenseTypeT.OtherTaxes2)
                {
                    return DefaultSystemClosingCostFee.Hud1000OtherTax2FeeTypeId;
                }
                else if (exp.HousingExpenseType == E_HousingExpenseTypeT.OtherTaxes3)
                {
                    return DefaultSystemClosingCostFee.Hud1000OtherTax3FeeTypeId;
                }
                else if (exp.HousingExpenseType == E_HousingExpenseTypeT.OtherTaxes4)
                {
                    return DefaultSystemClosingCostFee.Hud1000OtherTax4FeeTypeId;
                }
                else if (exp.HousingExpenseType == E_HousingExpenseTypeT.HomeownersAsscDues)
                {
                    return DefaultSystemClosingCostFee.Hud1000HomeOwnerAssociationDuesFeeTypeId;
                }
                else if (exp.HousingExpenseType == E_HousingExpenseTypeT.GroundRent)
                {
                    return DefaultSystemClosingCostFee.Hud1000GroundRentFeeTypeId;
                }
                else if (exp.HousingExpenseType == E_HousingExpenseTypeT.Unassigned && exp.CustomExpenseLineNum == E_CustomExpenseLineNumberT.Line1008)
                {
                    return DefaultSystemClosingCostFee.Hud1000Custom1FeeTypeId;
                }
                else if (exp.HousingExpenseType == E_HousingExpenseTypeT.Unassigned && exp.CustomExpenseLineNum == E_CustomExpenseLineNumberT.Line1009)
                {
                    return DefaultSystemClosingCostFee.Hud1000Custom2FeeTypeId;
                }
                else if (exp.HousingExpenseType == E_HousingExpenseTypeT.Unassigned && exp.CustomExpenseLineNum == E_CustomExpenseLineNumberT.Line1010)
                {
                    return DefaultSystemClosingCostFee.Hud1000Custom3FeeTypeId;
                }
                else if (exp.HousingExpenseType == E_HousingExpenseTypeT.Unassigned && exp.CustomExpenseLineNum == E_CustomExpenseLineNumberT.Line1011)
                {
                    return DefaultSystemClosingCostFee.Hud1000Custom4FeeTypeId;
                }
                else
                {
                    return Guid.Empty;
                }
            }
        }

        /// <summary>
        /// Creates a new blank fee. Creates the set's specific fee type.
        /// </summary>
        /// <returns>Creates a new blank fee.</returns>
        protected override LoanClosingCostFee CreateNewFee()
        {
            return new BorrowerClosingCostFee();
        }

        /// <summary>
        /// Deserializes the JSON and sets up the underlying fee list. This specifically deserializes to a borrower closing cost fee list.
        /// </summary>
        /// <param name="json">The JSON to deserialize.</param>
        protected override void Initialize(string json)
        {
            bool hasDiscountFee = false;
            bool hasOriginatorCompensationFee = false;
            bool hasPrepaidInterest = false;
            bool hasMIVAFee = false;
            bool hasMIPremiumRecurring = false;
            bool hasMIPremiumUpfront = false;

            HashSet<Guid> existedInitialEscrowFee = new HashSet<Guid>();
            HashSet<Guid> existingPrepaidEscrowFees = new HashSet<Guid>();

            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            BrokerDB brokerDb = null;

            if (this.DataLoan != null)
            {
                // 5/16/2015 dd - Retrieve brokerid from dataloan first.
                brokerDb = this.DataLoan.BrokerDB;
            }
            else if (principal != null)
            {
                if (principal.BrokerId != Guid.Empty)
                {
                    brokerDb = principal.BrokerDB;
                }
            }

            if (brokerDb != null)
            {
                this.IsResetSystemFees = brokerDb.IsResetSystemFeesOnRemoval;
                this.BrokerClosingCostSet = brokerDb.GetUnlinkedClosingCostSet();
            }

            if (string.IsNullOrEmpty(json))
            {
                this.ClosingCostFeeList = new List<BaseClosingCostFee>();
            }
            else
            {
                List<BorrowerClosingCostFee> list = ObsoleteSerializationHelper.JsonDeserializeAndSanitize<List<BorrowerClosingCostFee>>(json);
                this.ClosingCostFeeList = new List<BaseClosingCostFee>();

                foreach (BorrowerClosingCostFee o in list)
                {
                    if (!o.IsValidFee())
                    {
                        continue;
                    }

                    decimal totalAmtInJson = o.TotalAmountOnDeserialization;
                    o.SetClosingCostSet(this);

                    // Update the TP and AFF properties. Can be done just by calling the properties.
                    var thirdPartyNotUsed = o.IsThirdParty;
                    var affiliateNotUsed = o.IsAffiliate;

                    if (o.LegacyGfeFieldT == E_LegacyGfeFieldT.sLDiscnt)
                    {
                        hasDiscountFee = true;
                        this.ClosingCostFeeList.Add(o);
                    }
                    else if (o.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800LoanOriginationFeeTypeId)
                    {
                        if (this.HasDataLoanAssociate &&
                            (this.DataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.ClosingCostFee2015 || this.DataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.LegacyButMigrated) &&
                            o.ChildLoanFormula.FormulaT != E_ClosingCostFeeFormulaT.Full)
                        {
                            o.FormulaT = E_ClosingCostFeeFormulaT.Full;
                            o.PercentBaseT = this.GetPercentBaseTForLoanOriginationFee();
                        }

                        this.ClosingCostFeeList.Add(o);
                    }
                    else if (o.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId)
                    {
                        // OPM 475044 - This is run in order to fix the loans that are in a bad state
                        if (this.HasDataLoanAssociate &&
                            this.DataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID &&
                            this.DataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid)
                        {
                            foreach (var payment in o.Payments)
                            {
                                payment.ResponsiblePartyT = payment.ResponsiblePartyT;
                            }
                        }

                        // OPM 214180 - Exclude lender paid originator compensation fee if loan is TRID 2015
                        // Note: fee still added as failsafe if no loan associated.
                        if (this.HasDataLoanAssociate &&
                            this.DataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID &&
                            this.DataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid &&
                            this.DataLoan.sGfeIsTPOTransaction)
                        {
                            this.standaloneLenderOrigCompFee = o;
                            hasOriginatorCompensationFee = true;
                            continue;
                        }

                        if (this.HasClosingCostArchiveAssociate &&
                            this.ClosingCostArchive.GetCountValue("sDisclosureRegulationT") == (int)E_sDisclosureRegulationT.TRID &&
                            this.ClosingCostArchive.GetCountValue("sOriginatorCompensationPaymentSourceT") == (int)E_sOriginatorCompensationPaymentSourceT.LenderPaid &&
                            this.ClosingCostArchive.GetBitValue("sGfeIsTPOTransaction"))
                        {
                            this.standaloneLenderOrigCompFee = o;
                            hasOriginatorCompensationFee = true;
                            continue;
                        }

                        if (hasOriginatorCompensationFee == false)
                        {
                            // 4/3/2015 dd - Only add one originator compensation fee.
                            hasOriginatorCompensationFee = true;

                            this.ClosingCostFeeList.Add(o);
                        }
                    }
                    else if (o.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionG && ClosingCostSetUtils.ConstantInitialEscrowLegacyFeeList.Contains(o.ClosingCostFeeTypeId))
                    {
                        // 3/30/2015 dd - For Reserves section only include each fee one and it must be in
                        // the constant initial escrow fee list.
                        if (existedInitialEscrowFee.Contains(o.ClosingCostFeeTypeId))
                        {
                            // 3/30/2015 dd - The fee already in the list. Skip the duplicate one.
                            continue;
                        }

                        // For section G, we want the description to be what the expense description is + "Reserves" except for aggregate adjustment fee
                        string addition = string.Empty;
                        if (o.ClosingCostFeeTypeId != DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId)
                        {
                            addition = " Reserves";

                            // Update the hudline. Can be done by calling the property.
                            // Can't do the updating for the Aggregate adjustment at the initialize stage due to infinite loop possiblity using the legacy calculations.
                            // We'd need a new way to calculate in legacy mode in order to fix this problem.
                            // Or to move the entire calculation inside the closing cost set. 
                            var hudNotUsed = o.HudLine;
                            var updatedTotalAmt = o.TotalAmount;
                            if (updatedTotalAmt == 0)
                            {
                                o.ResetPayments();
                            }

                            if (updatedTotalAmt != 0 && totalAmtInJson == 0)
                            {
                                // The JSON indicates that the fee was not in the set before this initialize but now it should be since total amount is non zero. We need to update the beneficiary.
                                if (this.BrokerClosingCostSet != null)
                                {
                                    FeeSetupClosingCostFee feeSetupFee = (FeeSetupClosingCostFee)this.BrokerClosingCostSet.GetFees(fsFee => fsFee.ClosingCostFeeTypeId == o.ClosingCostFeeTypeId).FirstOrDefault();
                                    if (feeSetupFee != null)
                                    {
                                        o.Beneficiary = feeSetupFee.Beneficiary;
                                    }
                                }
                            }
                        }

                        o.Description = this.GetDescription(o.ClosingCostFeeTypeId) + addition;
                        if (string.IsNullOrEmpty(o.OriginalDescription))
                        {
                            o.OriginalDescription = this.GetDefaultDescription(o.ClosingCostFeeTypeId) + addition;
                        }

                        o.GfeSectionT = E_GfeSectionT.B9;

                        existedInitialEscrowFee.Add(o.ClosingCostFeeTypeId);

                        this.ClosingCostFeeList.Add(o);
                    }
                    else if (o.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionF && ClosingCostSetUtils.ConstantPrepaidEscrowFeeList.Contains(o.ClosingCostFeeTypeId))
                    {
                        if (existingPrepaidEscrowFees.Contains(o.ClosingCostFeeTypeId))
                        {
                            continue;
                        }

                        // For the prepaids that pull from the expenses, enforce that they use the expense description.
                        // Also enforce their formula type and gfe section.
                        o.FormulaT = E_ClosingCostFeeFormulaT.PrepaidAmountForExpenses;
                        o.Description = this.GetDescription(o.ClosingCostFeeTypeId);
                        if (string.IsNullOrEmpty(o.OriginalDescription))
                        {
                            o.OriginalDescription = this.GetDefaultDescription(o.ClosingCostFeeTypeId);
                        }

                        o.GfeSectionT = E_GfeSectionT.B11;

                        // Clear out payments if fee has total amount of 0.
                        var updatedTotalAmt = o.TotalAmount;
                        if (updatedTotalAmt == 0)
                        {
                            o.ResetPayments();
                        }

                        if (updatedTotalAmt != 0 && totalAmtInJson == 0)
                        {
                            // The JSON indicates that the fee was not in the set before this initialize but now it should be since total amount is non zero. We need to update the beneficiary.
                            if (this.BrokerClosingCostSet != null)
                            {
                                FeeSetupClosingCostFee feeSetupFee = (FeeSetupClosingCostFee)this.BrokerClosingCostSet.GetFees(fsFee => fsFee.ClosingCostFeeTypeId == o.ClosingCostFeeTypeId).FirstOrDefault();
                                if (feeSetupFee != null)
                                {
                                    o.Beneficiary = feeSetupFee.Beneficiary;
                                }
                            }
                        }

                        existingPrepaidEscrowFees.Add(o.ClosingCostFeeTypeId);
                        this.ClosingCostFeeList.Add(o);
                    }
                    else if (o.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId)
                    {
                        hasPrepaidInterest = true;
                        this.ClosingCostFeeList.Add(o);
                    }
                    else if (o.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900VAFundingFeeTypeId)
                    {
                        o.IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionB;
                        hasMIVAFee = true;
                        this.ClosingCostFeeList.Add(o);
                    }
                    else if (o.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId)
                    {
                        o.IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionF;
                        hasMIPremiumRecurring = true;
                        this.ClosingCostFeeList.Add(o);
                    }
                    else if (o.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumUpfrontFeeTypeId)
                    {
                        o.IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionB;
                        hasMIPremiumUpfront = true;
                        this.ClosingCostFeeList.Add(o);
                    }
                    else
                    {
                        this.ClosingCostFeeList.Add(o);
                    }
                }
            }

            this.ClearFeeLookupCache();

            // This set is for updating another set. Don't need to make automatic fees.
            if (this.SkipInitAndEnumerable)
            {
                return;
            }

            // 3/16/2015 dd - If no discount point fee is create then add one.
            if (hasDiscountFee == false)
            {
                BorrowerClosingCostFee fee = null;

                // Don't think this will ever fail.
                if (brokerDb != null)
                {
                    FeeSetupClosingCostFee brokerFee = (FeeSetupClosingCostFee)this.BrokerClosingCostSet.GetFees(o => o.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId).FirstOrDefault();
                    if (brokerFee != null)
                    {
                        BorrowerClosingCostFee convertedFee = brokerFee.ConvertToBorrowerClosingCostFee();
                        this.PrepFee(convertedFee);
                        fee = convertedFee;
                    }
                }

                if (fee == null)
                {
                    fee = new BorrowerClosingCostFee();
                    fee.SetClosingCostSet(this);
                    fee.FormulaT = E_ClosingCostFeeFormulaT.sLDiscnt;
                    fee.HudLine = 802;
                    fee.Description = "Discount Point";
                    fee.LegacyGfeFieldT = E_LegacyGfeFieldT.sLDiscnt;
                    fee.IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionA;
                    fee.ClosingCostFeeTypeId = DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId;
                    fee.Beneficiary = E_AgentRoleT.Lender;
                    fee.DisableBeneficiaryAutomation = this.IsMigration;
                    fee.IsApr = true;
                    fee.IsFhaAllowable = true;
                    fee.MismoFeeT = E_ClosingCostFeeMismoFeeT.LoanDiscountPoints;
                    fee.GfeSectionT = E_GfeSectionT.B2;
                }

                fee.SetClosingCostSet(this);

                if (string.IsNullOrEmpty(fee.OriginalDescription))
                {
                    fee.OriginalDescription = fee.Description;
                }

                this.ClosingCostFeeList.Add(fee);
            }

            // OPM 214180 - Don't add lender paid originator compensation fee if loan is TRID 2015
            // Note: fee still added as failsafe if no loan associated.
            if (hasOriginatorCompensationFee == false)
            {
                BorrowerClosingCostFee fee = null;

                if (brokerDb != null)
                {
                    FeeSetupClosingCostFee brokerDefaultFee = (FeeSetupClosingCostFee)this.BrokerClosingCostSet.GetFees(o => o.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId).FirstOrDefault();
                    if (brokerDefaultFee != null)
                    {
                        BorrowerClosingCostFee convertedFee = brokerDefaultFee.ConvertToBorrowerClosingCostFee();
                        this.PrepFee(convertedFee);
                        fee = convertedFee;
                    }
                }

                // If the lender config is somehow missing this fee, provide default.
                if (fee == null)
                {
                    fee = new BorrowerClosingCostFee();
                    fee.SetClosingCostSet(this);
                    fee.ClosingCostFeeTypeId = DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId;
                    fee.HudLine = 801;
                    fee.Description = "Originator compensation";
                    fee.LegacyGfeFieldT = E_LegacyGfeFieldT.sGfeOriginatorCompF;
                    fee.IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionA;
                    fee.Beneficiary = E_AgentRoleT.Broker;
                    fee.DisableBeneficiaryAutomation = this.IsMigration;
                    fee.IsApr = true;
                    fee.GfeSectionT = E_GfeSectionT.B1;
                    fee.MismoFeeT = E_ClosingCostFeeMismoFeeT.LoanOriginatorCompensation;
                }

                var isPlanSetFromLoanData = this.HasDataLoanAssociate && this.DataLoan.sHasOriginatorCompensationPlan;

                // Closing cost archives did not previously have this indicator.
                // To prevent overwriting data for a closing cost archive on a 
                // loan that did have a compensation plan, we default to true.
                var isPlanSetFromClosingCostData = this.HasClosingCostArchiveAssociate && this.ClosingCostArchive.GetBitValue("sHasOriginatorCompensationPlan", true);

                if (brokerDb != null &&
                    brokerDb.DefaultBorrPaidOrigCompSourceToTotalLoanAmount &&
                    !isPlanSetFromLoanData &&
                    !isPlanSetFromClosingCostData)
                {
                    fee.PercentBaseT = E_PercentBaseT.TotalLoanAmount;
                }

                fee.SetClosingCostSet(this);
                if (string.IsNullOrEmpty(fee.OriginalDescription))
                {
                    fee.OriginalDescription = fee.Description;
                }

                if (this.HasDataLoanAssociate &&
                    this.DataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID &&
                    this.DataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid &&
                    this.DataLoan.sGfeIsTPOTransaction)
                {
                    this.standaloneLenderOrigCompFee = fee;
                }
                else if (this.HasClosingCostArchiveAssociate &&
                    this.ClosingCostArchive.GetCountValue("sDisclosureRegulationT") == (int)E_sDisclosureRegulationT.TRID &&
                    this.ClosingCostArchive.GetCountValue("sOriginatorCompensationPaymentSourceT") == (int)E_sOriginatorCompensationPaymentSourceT.LenderPaid &&
                    this.ClosingCostArchive.GetBitValue("sGfeIsTPOTransaction"))
                {
                    this.standaloneLenderOrigCompFee = fee;
                }
                else
                {
                    this.ClosingCostFeeList.Add(fee);
                }
            }

            foreach (Guid feeTypeId in ClosingCostSetUtils.ConstantPrepaidEscrowFeeList)
            {
                if (existingPrepaidEscrowFees.Contains(feeTypeId))
                {
                    continue;
                }

                BorrowerClosingCostFee fee = null;

                if (brokerDb != null)
                {
                    FeeSetupClosingCostFee brokerFee = (FeeSetupClosingCostFee)this.BrokerClosingCostSet.GetFees(o => o.ClosingCostFeeTypeId == feeTypeId).FirstOrDefault();
                    if (brokerFee != null)
                    {
                        BorrowerClosingCostFee convertedFee = brokerFee.ConvertToBorrowerClosingCostFee();
                        this.PrepFee(convertedFee);
                        fee = convertedFee;
                    }
                }

                if (fee == null)
                {
                    fee = new BorrowerClosingCostFee();
                    fee.SetClosingCostSet(this);
                    fee.ClosingCostFeeTypeId = feeTypeId;
                    fee.HudLine = 900;
                    fee.IsApr = false;
                    fee.IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionF;
                    fee.Description = this.GetDescription(fee.ClosingCostFeeTypeId);
                    fee.OriginalDescription = this.GetDefaultDescription(fee.ClosingCostFeeTypeId);
                    fee.FormulaT = E_ClosingCostFeeFormulaT.PrepaidAmountForExpenses;
                    fee.IsTitleFee = false;
                    fee.IsOptional = false;
                    fee.Beneficiary = E_AgentRoleT.Other;
                    fee.IsApr = false;
                    fee.IsFhaAllowable = true;
                }
                else
                {
                    fee.SetClosingCostSet(this);
                    fee.Description = this.GetDescription(fee.ClosingCostFeeTypeId);
                    if (string.IsNullOrEmpty(fee.OriginalDescription))
                    {
                        fee.OriginalDescription = this.GetDefaultDescription(fee.ClosingCostFeeTypeId);
                    }
                }

                fee.GfeSectionT = E_GfeSectionT.B11;
                this.ClosingCostFeeList.Add(fee);
            }

            // 3/18/2015 dd - For all missing initial escrow fee, we need to add to list.
            foreach (Guid feeTypeId in ClosingCostSetUtils.ConstantInitialEscrowLegacyFeeList)
            {
                if (existedInitialEscrowFee.Contains(feeTypeId))
                {
                    continue;
                }

                BorrowerClosingCostFee fee = null;
                string addition = string.Empty;
                if (feeTypeId != DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId)
                {
                    addition = " Reserves";
                }

                // Should never fail.
                if (brokerDb != null)
                {
                    FeeSetupClosingCostFee brokerFee = (FeeSetupClosingCostFee)this.BrokerClosingCostSet.GetFees(o => o.ClosingCostFeeTypeId == feeTypeId).FirstOrDefault();
                    if (brokerFee != null)
                    {
                        BorrowerClosingCostFee convertedFee = brokerFee.ConvertToBorrowerClosingCostFee();
                        this.PrepFee(convertedFee);
                        fee = convertedFee;
                    }
                }

                if (fee == null)
                {
                    fee = new BorrowerClosingCostFee();
                    fee.SetClosingCostSet(this);
                    fee.ClosingCostFeeTypeId = feeTypeId;
                    fee.LegacyGfeFieldT = E_LegacyGfeFieldT.Undefined;
                    fee.IsApr = false;
                    fee.GfeSectionT = E_GfeSectionT.B9;
                    fee.IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionG;
                    fee.Description = this.GetDescription(fee.ClosingCostFeeTypeId) + addition;
                    fee.OriginalDescription = this.GetDefaultDescription(fee.ClosingCostFeeTypeId) + addition;
                    fee.FormulaT = E_ClosingCostFeeFormulaT.InitialEscrowPaymentAtClosing;
                    
                    // Initialize the first (system) payment as paid by borrower. Use a 0 total amount since all we need to do
                    // is set the paid by value.
                    fee.GetPaymentsWithCachedTotal(0).First().PaidByT = E_ClosingCostFeePaymentPaidByT.Borrower;
                    fee.Beneficiary = E_AgentRoleT.Lender;
                    fee.DisableBeneficiaryAutomation = this.IsMigration;
                    fee.ClosingCostFeeTypeId = feeTypeId;
                    fee.IsTitleFee = false;
                    fee.IsOptional = false;
                    fee.IsFhaAllowable = true;
                }
                else
                {
                    fee.SetClosingCostSet(this);
                    fee.Description = this.GetDescription(feeTypeId) + addition;
                    if (string.IsNullOrEmpty(fee.OriginalDescription))
                    {
                        fee.OriginalDescription = this.GetDefaultDescription(fee.ClosingCostFeeTypeId) + addition;
                    }
                }

                this.ClosingCostFeeList.Add(fee);
            }

            // OPM 213908 - Automatically create Prepaid Interest fee if sIPia != $0.00
            // OPM 241783 - Always create the prepaid interest fee if it doesn't exist,
            // regardless of the value being non-zero. The old behavior was causing issues
            // when the value of sIPia was 0 when the set was initialized but was changed
            // afterward.
            if (!hasPrepaidInterest)
            {
                BorrowerClosingCostFee fee = null;

                if (brokerDb != null)
                {
                    FeeSetupClosingCostFee brokerFee = (FeeSetupClosingCostFee)this.BrokerClosingCostSet.GetFees(o => o.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId).FirstOrDefault();
                    if (brokerFee != null)
                    {
                        BorrowerClosingCostFee convertedFee = brokerFee.ConvertToBorrowerClosingCostFee();
                        this.PrepFee(convertedFee);
                        fee = convertedFee;
                    }
                }

                if (fee == null)
                {
                    fee = new BorrowerClosingCostFee();
                    fee.SetClosingCostSet(this);
                    fee.ClosingCostFeeTypeId = DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId;
                    fee.HudLine = 901;
                    fee.Description = "Per-diem interest";
                    fee.LegacyGfeFieldT = E_LegacyGfeFieldT.sIPia;
                    fee.IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionF;
                    fee.Beneficiary = E_AgentRoleT.Lender;
                    fee.DisableBeneficiaryAutomation = this.IsMigration;
                    fee.IsApr = true;
                    fee.IsFhaAllowable = true;
                    fee.MismoFeeT = E_ClosingCostFeeMismoFeeT.Undefined;
                    fee.GfeSectionT = E_GfeSectionT.B10;
                    fee.FormulaT = E_ClosingCostFeeFormulaT.sIPia;
                    fee.IsTitleFee = false;
                    fee.IsOptional = false;

                    // Initialize the payments.
                    fee.GetPaymentsWithCachedTotal(0);
                }

                if (string.IsNullOrEmpty(fee.OriginalDescription))
                {
                    fee.OriginalDescription = fee.Description;
                }

                fee.SetClosingCostSet(this);
                this.ClosingCostFeeList.Add(fee);
            }

            if (!hasMIVAFee)
            {
                BorrowerClosingCostFee fee = null;

                if (brokerDb != null)
                {
                    FeeSetupClosingCostFee brokerFee = (FeeSetupClosingCostFee)this.BrokerClosingCostSet.GetFees(o => o.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900VAFundingFeeTypeId).FirstOrDefault();
                    if (brokerFee != null)
                    {
                        BorrowerClosingCostFee convertedFee = brokerFee.ConvertToBorrowerClosingCostFee();
                        this.PrepFee(convertedFee);
                        fee = convertedFee;
                    }
                }

                if (fee == null)
                {
                    fee = new BorrowerClosingCostFee();
                    fee.SetClosingCostSet(this);
                    fee.ClosingCostFeeTypeId = DefaultSystemClosingCostFee.Hud900VAFundingFeeTypeId;
                    fee.HudLine = 905;
                    fee.IsTitleFee = false;
                    fee.IsOptional = false;
                    fee.Description = "VA Funding Fee";
                    fee.GfeSectionT = E_GfeSectionT.B3;
                    fee.IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionB;
                    fee.Beneficiary = E_AgentRoleT.Lender;
                    fee.IsApr = true;
                    fee.IsFhaAllowable = true;
                    fee.LegacyGfeFieldT = E_LegacyGfeFieldT.sVaFf;
                    fee.MismoFeeT = E_ClosingCostFeeMismoFeeT.Undefined;
                    fee.FormulaT = E_ClosingCostFeeFormulaT.VAFundingFee;
                }

                if (string.IsNullOrEmpty(fee.OriginalDescription))
                {
                    fee.OriginalDescription = fee.Description;
                }

                fee.SetClosingCostSet(this);
                this.ClosingCostFeeList.Add(fee);
            }

            if (!hasMIPremiumRecurring)
            {
                BorrowerClosingCostFee fee = null;

                if (brokerDb != null)
                {
                    FeeSetupClosingCostFee brokerFee = (FeeSetupClosingCostFee)this.BrokerClosingCostSet.GetFees(o => o.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId).FirstOrDefault();
                    if (brokerFee != null)
                    {
                        BorrowerClosingCostFee convertedFee = brokerFee.ConvertToBorrowerClosingCostFee();
                        this.PrepFee(convertedFee);
                        fee = convertedFee;
                    }
                }

                if (fee == null)
                {
                    fee = new BorrowerClosingCostFee();
                    fee.SetClosingCostSet(this);
                    fee.ClosingCostFeeTypeId = DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId;
                    fee.HudLine = 902;
                    fee.IsTitleFee = false;
                    fee.IsOptional = false;
                    fee.Description = "Mortgage Insurance Premium";
                    fee.GfeSectionT = E_GfeSectionT.B3;
                    fee.IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionF;
                    fee.Beneficiary = E_AgentRoleT.Lender;
                    fee.IsApr = true;
                    fee.IsFhaAllowable = true;
                    fee.LegacyGfeFieldT = E_LegacyGfeFieldT.sMipPia;
                    fee.MismoFeeT = E_ClosingCostFeeMismoFeeT.Undefined;
                    fee.FormulaT = E_ClosingCostFeeFormulaT.MortgageInsurancePremiumRecurring;
                }

                if (string.IsNullOrEmpty(fee.OriginalDescription))
                {
                    fee.OriginalDescription = fee.Description;
                }

                fee.SetClosingCostSet(this);
                this.ClosingCostFeeList.Add(fee);
            }

            if (!hasMIPremiumUpfront)
            {
                BorrowerClosingCostFee fee = null;

                if (brokerDb != null)
                {
                    FeeSetupClosingCostFee brokerFee = (FeeSetupClosingCostFee)this.BrokerClosingCostSet.GetFees(o => o.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumUpfrontFeeTypeId).FirstOrDefault();
                    if (brokerFee != null)
                    {
                        BorrowerClosingCostFee convertedFee = brokerFee.ConvertToBorrowerClosingCostFee();
                        this.PrepFee(convertedFee);
                        fee = convertedFee;
                    }
                }

                if (fee == null)
                {
                    fee = new BorrowerClosingCostFee();
                    fee.SetClosingCostSet(this);
                    fee.ClosingCostFeeTypeId = DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumUpfrontFeeTypeId;
                    fee.HudLine = 902;
                    fee.IsTitleFee = false;
                    fee.IsOptional = false;
                    fee.Description = "Mortgage Insurance Premium";
                    fee.GfeSectionT = E_GfeSectionT.B3;
                    fee.IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionB;
                    fee.Beneficiary = E_AgentRoleT.Lender;
                    fee.IsApr = true;
                    fee.IsFhaAllowable = true;
                    fee.LegacyGfeFieldT = E_LegacyGfeFieldT.sUpfrontMipPia;
                    fee.MismoFeeT = E_ClosingCostFeeMismoFeeT.Undefined;
                    fee.FormulaT = E_ClosingCostFeeFormulaT.MortgageInsurancePremiumUpfront;
                }

                if (string.IsNullOrEmpty(fee.OriginalDescription))
                {
                    fee.OriginalDescription = fee.Description;
                }

                fee.SetClosingCostSet(this);
                this.ClosingCostFeeList.Add(fee);
            }

            this.ClearFeeLookupCache();
        }

        /// <summary>
        /// Creates a new custom borrower fee and initializes it.
        /// </summary>
        /// <param name="hudLine">The hudline for the new fee.</param>
        /// <param name="closingCostFeeTypeId">The fee type id.</param>
        /// <param name="section">The section that contains the section template.</param>
        /// <returns>The created seller closing cost fee.</returns>
        protected override LoanClosingCostFee PrepCustomFee(int hudLine, Guid closingCostFeeTypeId, BaseClosingCostFeeSection section)
        {
            BorrowerClosingCostFee blankFee = new BorrowerClosingCostFee();
            blankFee.SetClosingCostSet(this);
            blankFee.HudLine = hudLine;
            blankFee.LegacyGfeFieldT = E_LegacyGfeFieldT.Undefined;
            blankFee.ClosingCostFeeTypeId = closingCostFeeTypeId;
            blankFee.OriginalDescription = this.GetCustomFeeOriginalDescription(closingCostFeeTypeId);
            blankFee.IsTitleFee = section.SectionFeeTemplate.IsTitleFee;
            blankFee.IsOptional = section.SectionFeeTemplate.IsOptional;
            blankFee.GfeSectionT = section.SectionFeeTemplate.GfeSectionT;
            blankFee.IntegratedDisclosureSectionT = section.SectionFeeTemplate.IntegratedDisclosureSectionT;
            blankFee.IsApr = section.SectionFeeTemplate.IsApr;
            blankFee.IsFhaAllowable = section.SectionFeeTemplate.IsFhaAllowable;
            blankFee.Beneficiary = section.SectionFeeTemplate.Beneficiary;
            blankFee.IsThirdParty = section.SectionFeeTemplate.IsThirdParty;
            blankFee.IsAffiliate = section.SectionFeeTemplate.IsAffiliate;
            blankFee.CanShop = section.SectionFeeTemplate.CanShop;
            blankFee.SetClosingCostSet(this);

            // Provide a default empty description for custom fees. Otherwise,
            // they may get filtered out of the closing cost breakdown but
            // still be applied to the file upon submission.
            // opm 221848 gf 8/15/2015
            blankFee.Description = string.Empty;

            return blankFee;
        }

        /// <summary>
        /// Creates an empty BorrowerClosingCostSet.
        /// </summary>
        /// <returns>The empty BorrowerClosingCostSet.</returns>
        protected override BaseClosingCostSet CreateEmptySet()
        {
            return new BorrowerClosingCostSet(string.Empty);
        }

        /// <summary>
        /// Creates a new empty closing cost set that does not automatically create fees and enumerates through ALL fees.
        /// </summary>
        /// <returns>The empty set.</returns>
        protected override BaseClosingCostSet CreateEmptySetForClearing()
        {
            return new BorrowerClosingCostSet(string.Empty, true);
        }

        /// <summary>
        /// Gets a list of fee sections in the HUD view.
        /// </summary>
        /// <returns>The list of fee sections in the HUD view.</returns>
        protected override IEnumerable<BaseClosingCostFeeSection> GetHudView()
        {
            return this.GetViewBase(E_ClosingCostViewT.LoanHud1);
        }

        /// <summary>
        /// Gets the description for the 1000 fee type.
        /// </summary>
        /// <param name="value">1000 fee type id.</param>
        /// <returns>A fee description.</returns>
        private string GetDescription(Guid value)
        {
            if (value == DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId)
            {
                return "Aggregate adjustment";
            }
            else if (value == DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId)
            {
                return "Mortgage insurance";
            }

            if (this.DataLoan == null)
            {
                return this.GetDefaultDescription(value);
            }

            if (value == DefaultSystemClosingCostFee.Hud900HazardInsuranceFeeTypeId || value == DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId)
            {
                return this.DataLoan.sHazardExpense.ExpenseDescription;
            }
            else if (value == DefaultSystemClosingCostFee.Hud900FloodInsuranceFeeTypeId || value == DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId)
            {
                return this.DataLoan.sFloodExpense.ExpenseDescription;
            }
            else if (value == DefaultSystemClosingCostFee.Hud900GroundRentFeeTypeId || value == DefaultSystemClosingCostFee.Hud1000GroundRentFeeTypeId)
            {
                return this.DataLoan.sGroundRentExpense.ExpenseDescription;
            }
            else if (value == DefaultSystemClosingCostFee.Hud900HOADuesFeeTypeId || value == DefaultSystemClosingCostFee.Hud1000HomeOwnerAssociationDuesFeeTypeId)
            {
                return this.DataLoan.sHOADuesExpense.ExpenseDescription;
            }
            else if (value == DefaultSystemClosingCostFee.Hud900OtherExp1FeeTypeId || value == DefaultSystemClosingCostFee.Hud1000Custom1FeeTypeId)
            {
                return this.DataLoan.sUnassigned1008Expense.ExpenseDescription;
            }
            else if (value == DefaultSystemClosingCostFee.Hud900OtherExp2FeeTypeId || value == DefaultSystemClosingCostFee.Hud1000Custom2FeeTypeId)
            {
                return this.DataLoan.sUnassigned1009Expense.ExpenseDescription;
            }
            else if (value == DefaultSystemClosingCostFee.Hud900OtherExp3FeeTypeId || value == DefaultSystemClosingCostFee.Hud1000Custom3FeeTypeId)
            {
                return this.DataLoan.sUnassigned1010Expense.ExpenseDescription;
            }
            else if (value == DefaultSystemClosingCostFee.Hud900OtherExp4FeeTypeId || value == DefaultSystemClosingCostFee.Hud1000Custom4FeeTypeId)
            {
                return this.DataLoan.sUnassigned1011Expense.ExpenseDescription;
            }
            else if (value == DefaultSystemClosingCostFee.Hud900OtherTax1FeeTypeId || value == DefaultSystemClosingCostFee.Hud1000OtherTax1FeeTypeId)
            {
                return this.DataLoan.sOtherTax1Expense.ExpenseDescription;
            }
            else if (value == DefaultSystemClosingCostFee.Hud900OtherTax2FeeTypeId || value == DefaultSystemClosingCostFee.Hud1000OtherTax2FeeTypeId)
            {
                return this.DataLoan.sOtherTax2Expense.ExpenseDescription;
            }
            else if (value == DefaultSystemClosingCostFee.Hud900OtherTax3FeeTypeId || value == DefaultSystemClosingCostFee.Hud1000OtherTax3FeeTypeId)
            {
                return this.DataLoan.sOtherTax3Expense.ExpenseDescription;
            }
            else if (value == DefaultSystemClosingCostFee.Hud900OtherTax4FeeTypeId || value == DefaultSystemClosingCostFee.Hud1000OtherTax4FeeTypeId)
            {
                return this.DataLoan.sOtherTax4Expense.ExpenseDescription;
            }
            else if (value == DefaultSystemClosingCostFee.Hud900PropertyTaxFeeTypeId || value == DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId)
            {
                return this.DataLoan.sRealEstateTaxExpense.ExpenseDescription;
            }
            else if (value == DefaultSystemClosingCostFee.Hud900SchoolTaxFeeTypeId || value == DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId)
            {
                return this.DataLoan.sSchoolTaxExpense.ExpenseDescription;
            }
            else if (value == DefaultSystemClosingCostFee.Hud900WindstormInsuranceFeeTypeId || value == DefaultSystemClosingCostFee.Hud1000WindStormInsuranceFeeTypeId)
            {
                return this.DataLoan.sWindstormExpense.ExpenseDescription;
            }
            else if (value == DefaultSystemClosingCostFee.Hud900CondoInsuranceFeeTypeId || value == DefaultSystemClosingCostFee.Hud1000CondoHo6InsuranceFeeTypeId)
            {
                return this.DataLoan.sCondoHO6Expense.ExpenseDescription;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the description for the 1000 fee type.
        /// </summary>
        /// <param name="value">1000 fee type id.</param>
        /// <returns>A fee description.</returns>
        private string GetDefaultDescription(Guid value)
        {
            if (value == DefaultSystemClosingCostFee.Hud900HazardInsuranceFeeTypeId || value == DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId)
            {
                return HousingExpenses.GetDefaultExpenseDescription(E_HousingExpenseTypeT.HazardInsurance, E_CustomExpenseLineNumberT.None);
            }
            else if (value == DefaultSystemClosingCostFee.Hud900FloodInsuranceFeeTypeId || value == DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId)
            {
                return HousingExpenses.GetDefaultExpenseDescription(E_HousingExpenseTypeT.FloodInsurance, E_CustomExpenseLineNumberT.None);
            }
            else if (value == DefaultSystemClosingCostFee.Hud900GroundRentFeeTypeId || value == DefaultSystemClosingCostFee.Hud1000GroundRentFeeTypeId)
            {
                return HousingExpenses.GetDefaultExpenseDescription(E_HousingExpenseTypeT.GroundRent, E_CustomExpenseLineNumberT.None);
            }
            else if (value == DefaultSystemClosingCostFee.Hud900HOADuesFeeTypeId || value == DefaultSystemClosingCostFee.Hud1000HomeOwnerAssociationDuesFeeTypeId)
            {
                return HousingExpenses.GetDefaultExpenseDescription(E_HousingExpenseTypeT.HomeownersAsscDues, E_CustomExpenseLineNumberT.None);
            }
            else if (value == DefaultSystemClosingCostFee.Hud900OtherExp1FeeTypeId || value == DefaultSystemClosingCostFee.Hud1000Custom1FeeTypeId)
            {
                return HousingExpenses.GetDefaultExpenseDescription(E_HousingExpenseTypeT.Unassigned, E_CustomExpenseLineNumberT.Line1008);
            }
            else if (value == DefaultSystemClosingCostFee.Hud900OtherExp2FeeTypeId || value == DefaultSystemClosingCostFee.Hud1000Custom2FeeTypeId)
            {
                return HousingExpenses.GetDefaultExpenseDescription(E_HousingExpenseTypeT.Unassigned, E_CustomExpenseLineNumberT.Line1009);
            }
            else if (value == DefaultSystemClosingCostFee.Hud900OtherExp3FeeTypeId || value == DefaultSystemClosingCostFee.Hud1000Custom3FeeTypeId)
            {
                return HousingExpenses.GetDefaultExpenseDescription(E_HousingExpenseTypeT.Unassigned, E_CustomExpenseLineNumberT.Line1010);
            }
            else if (value == DefaultSystemClosingCostFee.Hud900OtherExp4FeeTypeId || value == DefaultSystemClosingCostFee.Hud1000Custom4FeeTypeId)
            {
                return HousingExpenses.GetDefaultExpenseDescription(E_HousingExpenseTypeT.Unassigned, E_CustomExpenseLineNumberT.Line1011);
            }
            else if (value == DefaultSystemClosingCostFee.Hud900OtherTax1FeeTypeId || value == DefaultSystemClosingCostFee.Hud1000OtherTax1FeeTypeId)
            {
                return HousingExpenses.GetDefaultExpenseDescription(E_HousingExpenseTypeT.OtherTaxes1, E_CustomExpenseLineNumberT.None);
            }
            else if (value == DefaultSystemClosingCostFee.Hud900OtherTax2FeeTypeId || value == DefaultSystemClosingCostFee.Hud1000OtherTax2FeeTypeId)
            {
                return HousingExpenses.GetDefaultExpenseDescription(E_HousingExpenseTypeT.OtherTaxes2, E_CustomExpenseLineNumberT.None);
            }
            else if (value == DefaultSystemClosingCostFee.Hud900OtherTax3FeeTypeId || value == DefaultSystemClosingCostFee.Hud1000OtherTax3FeeTypeId)
            {
                return HousingExpenses.GetDefaultExpenseDescription(E_HousingExpenseTypeT.OtherTaxes3, E_CustomExpenseLineNumberT.None);
            }
            else if (value == DefaultSystemClosingCostFee.Hud900OtherTax4FeeTypeId || value == DefaultSystemClosingCostFee.Hud1000OtherTax4FeeTypeId)
            {
                return HousingExpenses.GetDefaultExpenseDescription(E_HousingExpenseTypeT.OtherTaxes4, E_CustomExpenseLineNumberT.None);
            }
            else if (value == DefaultSystemClosingCostFee.Hud900PropertyTaxFeeTypeId || value == DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId)
            {
                return HousingExpenses.GetDefaultExpenseDescription(E_HousingExpenseTypeT.RealEstateTaxes, E_CustomExpenseLineNumberT.None);
            }
            else if (value == DefaultSystemClosingCostFee.Hud900SchoolTaxFeeTypeId || value == DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId)
            {
                return HousingExpenses.GetDefaultExpenseDescription(E_HousingExpenseTypeT.SchoolTaxes, E_CustomExpenseLineNumberT.None);
            }
            else if (value == DefaultSystemClosingCostFee.Hud900WindstormInsuranceFeeTypeId || value == DefaultSystemClosingCostFee.Hud1000WindStormInsuranceFeeTypeId)
            {
                return HousingExpenses.GetDefaultExpenseDescription(E_HousingExpenseTypeT.WindstormInsurance, E_CustomExpenseLineNumberT.None);
            }
            else if (value == DefaultSystemClosingCostFee.Hud900CondoInsuranceFeeTypeId || value == DefaultSystemClosingCostFee.Hud1000CondoHo6InsuranceFeeTypeId)
            {
                return HousingExpenses.GetDefaultExpenseDescription(E_HousingExpenseTypeT.CondoHO6Insurance, E_CustomExpenseLineNumberT.None);
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Disable beneficiary automation and clear beneficiary.
        /// </summary>
        /// <param name="fee">The fee to prep.</param>
        private void PrepFee(BorrowerClosingCostFee fee)
        {
            if (fee != null)
            {
                fee.DisableBeneficiaryAutomation = this.IsMigration;
                fee.ClearBeneficiary();
            }
        }
    }
}
