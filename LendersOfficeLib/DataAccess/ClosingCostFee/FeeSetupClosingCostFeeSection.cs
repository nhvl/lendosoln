﻿// <copyright file="FeeSetupClosingCostFeeSection.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   7/9/2015
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Class representing the broker level closing cost fee section.
    /// </summary>
    [DataContract]
    public class FeeSetupClosingCostFeeSection : BaseClosingCostFeeSection
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FeeSetupClosingCostFeeSection" /> class.
        /// </summary>
        /// <param name="closingCostViewT">The view type.</param>
        /// <param name="closingCostSet">The parent set to pull fees from.</param>
        /// <param name="setfilter">The filter for the master set.</param>
        /// <param name="sectionName">The section name.</param>
        /// <param name="predicate">The predicate used to filter fees.</param>
        /// <param name="feeTemplate">The fee template for this section.</param>
        /// <param name="hudLineStart">The first hudline number for this section.</param>
        /// <param name="hudLineEnd">The last hudline number for this section.</param>
        /// <param name="sectionT">The disclosure section type for this section.</param>
        public FeeSetupClosingCostFeeSection(E_ClosingCostViewT closingCostViewT, FeeSetupClosingCostSet closingCostSet, Func<BaseClosingCostFee, bool> setfilter, string sectionName, Func<BaseClosingCostFee, bool> predicate, FeeSetupClosingCostFee feeTemplate, int hudLineStart, int hudLineEnd, E_IntegratedDisclosureSectionT sectionT)
            : base(closingCostViewT, closingCostSet, setfilter, sectionName, predicate, feeTemplate, hudLineStart, hudLineEnd, sectionT)
        {
        }

        /// <summary>
        /// Gets or sets the section's fee list as FeeSetupClosingCostFee. USE THIS FOR SERIALIZATION PLEASE.
        /// </summary>
        /// <value>The section's fee list as FeeSetupClosingCostFee.</value>
        [DataMember]
        private IEnumerable<FeeSetupClosingCostFee> ClosingCostFeeList
        {
            get
            {
                List<FeeSetupClosingCostFee> list = new List<FeeSetupClosingCostFee>();

                foreach (FeeSetupClosingCostFee fee in this.FilteredClosingCostFeeList)
                {
                    list.Add(fee);
                }

                return list;
            }

            set
            {
                this.FilteredClosingCostFeeList = (IEnumerable<BaseClosingCostFee>)value;
            }
        }

        /// <summary>
        /// Gets or sets a list of fees in this fee setup section as Borrower Fees.
        /// </summary>
        /// <value>The list of fees in this fee setup section as Borrower Fees.</value>
        [DataMember]
        private IEnumerable<BorrowerClosingCostFee> FeeListAsBorrowerFees
        {
            get
            {
                List<BorrowerClosingCostFee> list = new List<BorrowerClosingCostFee>();

                foreach (FeeSetupClosingCostFee fee in this.FilteredClosingCostFeeList)
                {
                    BorrowerClosingCostFee convertedFee = fee.ConvertToBorrowerClosingCostFee();
                    list.Add(convertedFee);
                }

                return list;
            }

            set
            {
            }
        }

        /// <summary>
        /// Gets or sets a list of fees in this fee setup fee as Seller Fees.
        /// </summary>
        /// <value>The list of fees in this fee setup section as Seller Fees.</value>
        [DataMember]
        private IEnumerable<SellerClosingCostFee> FeeListAsSellerFees
        {
            get
            {
                List<SellerClosingCostFee> list = new List<SellerClosingCostFee>();

                foreach (FeeSetupClosingCostFee fee in this.FilteredClosingCostFeeList)
                {
                    SellerClosingCostFee convertedFee = new SellerClosingCostFee();
                    fee.InitializeLoanFee(convertedFee);
                    list.Add(convertedFee);
                }

                return list;
            }

            set
            {
            }
        }

        /// <summary>
        /// Gets or sets the section's fee template as a FeeSetupClosingCostFee. USE THIS FOR SERIALIZATION PLEASE.
        /// </summary>
        /// <value>The fee template as a FeeSetupClosingCostFee.</value>
        [DataMember]
        private FeeSetupClosingCostFee FeeTemplate
        {
            get
            {
                return (FeeSetupClosingCostFee)this.SectionFeeTemplate;
            }

            set
            {
                this.SectionFeeTemplate = value;
            }
        }
    }
}
