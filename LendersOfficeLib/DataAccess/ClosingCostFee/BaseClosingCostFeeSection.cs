﻿// <copyright file="BaseClosingCostFeeSection.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   6/22/2015
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Runtime.Serialization;

    /// <summary>
    /// Base class for all closing cost fee sections.
    /// </summary>
    [DataContract]
    [KnownType(typeof(FeeSetupClosingCostFeeSection))]
    [KnownType(typeof(SellerClosingCostFeeSection))]
    public abstract class BaseClosingCostFeeSection
    {
        /// <summary>
        /// An array of allowable custom hud lines for Legacy w/migrated support.
        /// </summary>
        public static readonly ReadOnlyCollection<int> AllowableCustomFeesForLegacyButMigrated = new ReadOnlyCollection<int>(new int[]
        { 
            813, 814, 815, 816, 817,
            904, 906,
            1112, 1113, 1114, 1115,
            1206, 1207, 1208,
            1303, 1304, 1305, 1306, 1307
        });

        /// <summary>
        /// A dictionary of max custom fees per <code>hud</code> line section.
        /// </summary>
        public static readonly IReadOnlyDictionary<int, int> MaxCustomCountPerSection = new ReadOnlyDictionary<int, int>(
            AllowableCustomFeesForLegacyButMigrated.GroupBy(hudLine => hudLine - (hudLine % 100)).ToDictionary(hudLineSection => hudLineSection.Key, hudLineSection => hudLineSection.Count()));

        /// <summary>
        /// A dictionary of custom fee count by <code>hud</code> section.
        /// </summary>
        private Dictionary<int, int> customFeeCountByHud = new Dictionary<int, int>();

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseClosingCostFeeSection" /> class.
        /// </summary>
        /// <param name="closingCostViewT">The type of view the object is in.</param>
        /// <param name="closingCostSet">A master closing cost set.</param>
        /// <param name="setFilter">The filter for the master closing cost set.</param>
        /// <param name="sectionName">Name of this section.</param>
        /// <param name="predicate">A function to test each fee include in this section.</param>
        /// <param name="feeTemplate">A fee template.</param>
        /// <param name="hudLineStart">The first correct HUD line number.</param>
        /// <param name="hudLineEnd">The last correct HUD line number.</param>
        /// <param name="sectionT">The integrated disclosure section type.</param>
        protected BaseClosingCostFeeSection(E_ClosingCostViewT closingCostViewT, BaseClosingCostSet closingCostSet, Func<BaseClosingCostFee, bool> setFilter, string sectionName, Func<BaseClosingCostFee, bool> predicate, BaseClosingCostFee feeTemplate, int hudLineStart, int hudLineEnd, E_IntegratedDisclosureSectionT sectionT)
        {
            if (closingCostSet == null)
            {
                throw new ArgumentNullException("closingCostSet cannot be null.");
            }

            if (string.IsNullOrEmpty(sectionName))
            {
                throw new ArgumentNullException("sectionName cannot be null or empty.");
            }

            if (predicate == null)
            {
                throw new ArgumentNullException("predicate cannot be null.");
            }

            this.InternalClosingCostFeeListFromSerialization = null;
            this.Predicate = predicate;
            this.ClosingCostSet = closingCostSet;
            this.SectionName = sectionName;
            this.SectionFeeTemplate = feeTemplate;
            this.HudLineEnd = hudLineEnd;
            this.HudLineStart = hudLineStart;
            this.ClosingCostViewT = closingCostViewT;
            this.SectionType = sectionT;
            this.ClosingCostSetFilter = setFilter;
        }

        /// <summary>
        /// Gets the first valid HUD line number of this section.
        /// </summary>
        /// <value>The first valid HUD line number for this section.</value>
        [DataMember]
        public int HudLineStart
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the last valid HUD line number for this section.
        /// </summary>
        /// <value>The last valid HUD line number for this section.</value>
        [DataMember]
        public int HudLineEnd
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the name of this section.
        /// </summary>
        /// <value>The name of this section.</value>
        [DataMember]
        public string SectionName
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the integrated disclosure section type.
        /// </summary>
        /// <value>The integrated disclosure section type.</value>
        [DataMember]
        public E_IntegratedDisclosureSectionT SectionType
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the section type in rep form.
        /// </summary>
        /// <value>
        /// The section type in rep form.
        /// </value>
        [DataMember]
        public string SectionTypeRep
        {
            get { return this.SectionType.ToString(); }
            private set { }
        }

        /// <summary>
        /// Gets or sets a default fee template. This is only use in UI on when add new record.
        /// </summary>
        /// <value>A default fee template.</value>
        public BaseClosingCostFee SectionFeeTemplate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the fees should be sorted.
        /// </summary>
        /// <value>Whether the view should be sorted.</value>
        public bool EnableSorting
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a list of closing cost fee in this section.
        /// </summary>
        /// <value>A list of closing cost fee in this section.</value>
        public IEnumerable<BaseClosingCostFee> FilteredClosingCostFeeList
        {
            get
            {
                if (this.InternalClosingCostFeeListFromSerialization != null)
                {
                    return this.InternalClosingCostFeeListFromSerialization;
                }

                Dictionary<int, int> customCount = new Dictionary<int, int>();

                List<BaseClosingCostFee> list = new List<BaseClosingCostFee>();

                if (this.ClosingCostSet != null)
                {
                    foreach (var fee in this.ClosingCostSet.GetFees(this.ClosingCostSetFilter))
                    {
                        if (this.IsExclusionSet)
                        {
                            int key = this.GetCustomFeeKey(fee);

                            if (key > 0)
                            {
                                if (this.HasRoomForCustomFee(key, fee.HudLine) == false)
                                {
                                    continue;
                                }
                            }
                        }

                        if (this.ExcludeFeeSet != null && this.ExcludeFeeSet.Contains(fee.ClosingCostFeeTypeId))
                        {
                            continue;
                        }

                        if (this.Predicate(fee))
                        {
                            // Don't include the fee if the exclusion filter returns true.
                            if (this.ExclusionFilter != null && this.ExclusionFilter(fee))
                            {
                                continue;
                            }
                            else
                            {
                                list.Add(fee);
                            }
                        }
                    }
                }

                list.Sort(new BaseClosingCostFeeComparer(this.ClosingCostViewT));
                return list;
            }

            protected set
            {
                this.InternalClosingCostFeeListFromSerialization = value;
            }
        }

        /// <summary>
        /// Gets the view the section is in. This is used by the sort function.
        /// </summary>
        /// <value>The view the section is in.</value>
        public E_ClosingCostViewT ClosingCostViewT
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets another function to test if a fee should be included in the section. Used for determining what can get added to editors.
        /// </summary>
        /// <value>Another function to test if a fee should be included in the section.</value>
        public Func<BaseClosingCostFee, bool> ExclusionFilter
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the custom fee count by hud.
        /// </summary>
        /// <value>The custom fee count by hud.</value>
        protected Dictionary<int, int> CustomFeeCountByHud
        {
            get
            {
                return this.customFeeCountByHud;
            }
        }

        /// <summary>
        /// Gets or sets a set of allowable custom hudlines.
        /// </summary>
        /// <value>A set of allowable custom hudlines.</value>
        protected HashSet<int> AllowableCustomHudFee
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a list of LQB System Fee Type ids to ignore.
        /// </summary>
        /// <value>A list of fee type ids.</value>
        protected HashSet<Guid> ExcludeFeeSet
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a function to test each fee include in this section.
        /// </summary>
        /// <value>A function to test if a fee should be included in this section.</value>
        protected Func<BaseClosingCostFee, bool> Predicate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the master closing cost set.
        /// </summary>
        /// <value>The master closing cost set.</value>
        protected BaseClosingCostSet ClosingCostSet
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a function that will filter out fees for <see cref="ClosingCostSet" />.
        /// </summary>
        /// <value>A function to filter out fees.</value>
        protected Func<BaseClosingCostFee, bool> ClosingCostSetFilter
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a closing cost fee list that get set through JSON deserialization. 
        /// Do not use this variable directly.
        /// </summary>
        /// <value>A closing cost fee list that gets set through JSON deserialization.</value>
        protected IEnumerable<BaseClosingCostFee> InternalClosingCostFeeListFromSerialization
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this set is being used to determine available fees.
        /// </summary>
        /// <value>A value indicating whether this set is being used to determine fees to add to the editor.</value>
        protected bool IsExclusionSet
        {
            get;
            set;
        }

        /// <summary>
        /// Add a closing cost fee to this section.
        /// </summary>
        /// <param name="fee">A closing cost fee.</param>
        public void Add(BaseClosingCostFee fee)
        {
            if (fee == null)
            {
                return;
            }

            if (this.Predicate(fee) == false)
            {
                throw new CBaseException("Fee is not belong to this section.", "Fee is not belong to this section");
            }

            this.ClosingCostSet.AddOrUpdate(fee);
        }

        /// <summary>
        /// Filter out a list of fee from the source. Excludes based on ClosingCostFeeTypeId.
        /// </summary>
        /// <param name="excludeFeeList">A list of fee to exclude.</param>
        /// <param name="feeVersionT">A current fee version.</param>
        /// <param name="optionalFilter">An optional filter. If it evaluates to true, the fee will be added to the exclude set.</param>
        public void SetExcludeFeeList(IEnumerable<BaseClosingCostFee> excludeFeeList, E_sClosingCostFeeVersionT feeVersionT, Func<BaseClosingCostFee, bool> optionalFilter)
        {
            this.IsExclusionSet = true;
            this.ExclusionFilter = optionalFilter;

            if (excludeFeeList == null)
            {
                return;
            }

            if (this.ExcludeFeeSet == null)
            {
                this.ExcludeFeeSet = new HashSet<Guid>();
            }

            if (feeVersionT == E_sClosingCostFeeVersionT.LegacyButMigrated)
            {
                if (this.AllowableCustomHudFee == null)
                {
                    this.InitializeAllowableCustomFeeSet();
                }
            }

            foreach (var o in excludeFeeList)
            {
                if (feeVersionT == E_sClosingCostFeeVersionT.LegacyButMigrated)
                {
                    int key = this.GetCustomFeeKey(o);

                    if (key != -1)
                    {
                        int count = 0;

                        if (this.customFeeCountByHud.TryGetValue(key, out count))
                        {
                            this.customFeeCountByHud[key] = count + 1;
                        }
                        else
                        {
                            this.customFeeCountByHud[key] = 1;
                        }

                        this.AllowableCustomHudFee.Remove(o.HudLine);
                    }
                }

                this.ExcludeFeeSet.Add(o.ClosingCostFeeTypeId);
            }
        }

        /// <summary>
        /// Test whether or not there is still room in the section to add a custom fee with the given hud line.
        /// </summary>
        /// <param name="key">Section key for the fee being tested.</param>
        /// <param name="hudLine">HudLine for the fee being tested.</param>
        /// <returns>A value indicating whether or not there is still room to add a custom fee to a section.</returns>
        public bool HasRoomForCustomFee(int key, int hudLine)
        {
            int count;

            if (hudLine == 800 || hudLine == 900 || hudLine == 1100 || hudLine == 1200 || hudLine == 1300
                || (this.AllowableCustomHudFee != null && this.AllowableCustomHudFee.Contains(hudLine)))
            {
                if (this.customFeeCountByHud != null)
                {
                    if (this.customFeeCountByHud.TryGetValue(key, out count))
                    {
                        if (count >= MaxCustomCountPerSection[key])
                        {
                            return false;
                        }
                    }
                }
            }
            else if (this.AllowableCustomHudFee != null && this.AllowableCustomHudFee.Contains(hudLine) == false)
            {
                // 4/1/2015 dd - Skip hudline that is not in allowable custom fee.
                return false;
            }

            return true;
        }

        /// <summary>
        /// Gets the <code>hud</code> key section for the custom fee. Return -1 if fee is not custom fee.
        /// </summary>
        /// <param name="fee">The fee to get <code>hud</code> key section.</param>
        /// <returns>The <code>hud</code> key section.</returns>
        private int GetCustomFeeKey(BaseClosingCostFee fee)
        {
            int key = -1;

            if (fee.LegacyGfeFieldT == E_LegacyGfeFieldT.Undefined)
            {
                if (fee.HudLine >= 800 && fee.HudLine < 900)
                {
                    key = 800;
                }
                else if (fee.HudLine >= 900 && fee.HudLine < 1000)
                {
                    key = 900;
                }
                else if (fee.HudLine >= 1100 && fee.HudLine < 1200)
                {
                    key = 1100;
                }
                else if (fee.HudLine >= 1200 && fee.HudLine < 1300)
                {
                    key = 1200;
                }
                else if (fee.HudLine >= 1300 && fee.HudLine < 1400)
                {
                    key = 1300;
                }
            }

            return key;
        }

        /// <summary>
        /// Initialize a set of hard code allowable custom fee.
        /// </summary>
        private void InitializeAllowableCustomFeeSet()
        {
            this.AllowableCustomHudFee = new HashSet<int>(AllowableCustomFeesForLegacyButMigrated);
        }
    }
}
