﻿// <copyright file="ValidationParamContainer.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   7/9/2015
// </summary>
namespace DataAccess
{
    /// <summary>
    /// Contains the parameters needed to validate the fee. Not all properties are needed for all fee types.
    /// </summary>
    public class ValidationParamContainer
    {
        /// <summary>
        /// Gets or sets the value IsRequireFeesFromDropDown.
        /// </summary>
        /// <value>The loan file field sIsRequireFeesFromDropDown.</value>
        public bool? IsRequireFeesFromDropDown
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the value IsManuallySetThirdPartyAffiliateProps.
        /// </summary>
        /// <value>The loan file field sIsManuallySetThirdPartyAffiliateProps.</value>
        public bool? IsManuallySetThirdPartyAffiliateProps
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the value AllowCloserWrite.
        /// </summary>
        /// <value>The user permission AllowCloserWrite.</value>
        public bool? AllowCloserWrite
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the value BranchChannelT.
        /// </summary>
        /// <value>The loan file field sBranchChannelT.</value>
        public E_BranchChannelT? BranchChannelT
        {
            get;
            set;
        }
    }
}
