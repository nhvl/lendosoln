﻿// <copyright file="BorrowerClosingCostFeePayment.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   7/27/2015
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;

    /// <summary>
    /// Class representing a borrower responsible payment.
    /// </summary>
    [DataContract]
    public class BorrowerClosingCostFeePayment : LoanClosingCostFeePayment
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BorrowerClosingCostFeePayment" /> class.
        /// </summary>
        public BorrowerClosingCostFeePayment()
        {
            this.InternalPaidByT = E_ClosingCostFeePaymentPaidByT.Borrower;
            this.InternalResponsiblePartyT = E_GfeResponsiblePartyT.Buyer;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this is the special UFMIP/VA payment with sFfUfmipFinanced amount.
        /// </summary>
        /// <value>A value indicating if this is the special UFMIP/VA payment with sFfufmipFinanced amount.</value>
        [DataMember(Name = "mipFinancedPmt")]
        public bool IsMIPFinancedPayment
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this is the special UFMIP/VA payment with the sUfCashPd amount.
        /// </summary>
        /// <value>A value indicating if this is the speical UFMIP/VA payment with the sUfCashPd amount.</value>
        [DataMember(Name = "cashPdPmt")]
        public bool IsCashPdPayment
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the paid by party of this payment. All payments for seller responsible paid by the seller.
        /// </summary>
        /// <value>The paid by party of this payment.</value>
        public override E_ClosingCostFeePaymentPaidByT PaidByT
        {
            get
            {
                // OPM 214180 - calculate paid by for originator compensation fee if loan is TRID 2015
                // OPM 476009 - calculate paid by for originator compensation fee if possible
                if (this.ParentClosingCostFee != null && 
                    this.ParentClosingCostFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId &&
                    this.ParentClosingCostFee.ParentClosingCostSet != null &&
                    this.ParentClosingCostFee.ParentClosingCostSet.HasDataLoanAssociate)
                {
                    switch (this.ParentClosingCostFee.ParentClosingCostSet.DataLoan.sOriginatorCompensationPaymentSourceT)
                    {
                        case E_sOriginatorCompensationPaymentSourceT.SourceNotSpecified:
                            this.InternalPaidByT = E_ClosingCostFeePaymentPaidByT.LeaveBlank;
                            break;
                        case E_sOriginatorCompensationPaymentSourceT.BorrowerPaid:
                            this.InternalPaidByT = E_ClosingCostFeePaymentPaidByT.Borrower;
                            break;
                        case E_sOriginatorCompensationPaymentSourceT.LenderPaid:
                            this.InternalPaidByT = E_ClosingCostFeePaymentPaidByT.Lender;
                            break;
                        default:
                            throw new UnhandledEnumException(this.ParentClosingCostFee.ParentClosingCostSet.DataLoan.sOriginatorCompensationPaymentSourceT);
                    }
                }

                if (this.InternalPaidByT == E_ClosingCostFeePaymentPaidByT.LeaveBlank || this.InternalPaidByT == E_ClosingCostFeePaymentPaidByT.Other)
                {
                    this.InternalPaidByT = E_ClosingCostFeePaymentPaidByT.Borrower;
                }

                if (this.ParentClosingCostFee != null && this.ParentClosingCostFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId &&
                    this.InternalPaidByT == E_ClosingCostFeePaymentPaidByT.Lender)
                {
                    this.InternalPaidByT = E_ClosingCostFeePaymentPaidByT.Borrower;
                }

                return this.InternalPaidByT;
            }

            set
            {
                if (value == E_ClosingCostFeePaymentPaidByT.LeaveBlank)
                {
                    throw new CBaseException("Paid By property cannot be set to Blank", "Paid By property cannot be set to Blank");
                }
                else
                {
                    this.InternalPaidByT = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the type of party responsible for the payment.
        /// </summary>
        /// <value>The type of party responsible for the payment.</value>
        public override E_GfeResponsiblePartyT ResponsiblePartyT
        {
            get
            {
                if (this.ParentClosingCostFee != null && this.ParentClosingCostFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId)
                {
                    if (this.ParentClosingCostFee.ParentClosingCostSet != null &&
                        this.ParentClosingCostFee.ParentClosingCostSet.HasDataLoanAssociate)
                    {
                        if (this.ParentClosingCostFee.ParentClosingCostSet.DataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)
                        {
                            this.InternalResponsiblePartyT = E_GfeResponsiblePartyT.Lender;
                        }
                        else if (this.ParentClosingCostFee.ParentClosingCostSet.DataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid)
                        {
                            this.InternalResponsiblePartyT = E_GfeResponsiblePartyT.Buyer;
                        }
                    }
                }

                if (this.InternalResponsiblePartyT == E_GfeResponsiblePartyT.LeaveBlank)
                {
                    this.InternalResponsiblePartyT = E_GfeResponsiblePartyT.Buyer;
                }

                return this.InternalResponsiblePartyT;
            }

            set
            {
                if (value == E_GfeResponsiblePartyT.LeaveBlank)
                {
                    throw new CBaseException("Responsible Party property cannot be set to Blank", "Responsible Party property cannot be set to Blank");
                }
                else
                {
                    this.InternalResponsiblePartyT = value;
                }
            }
        }

        /// <summary>
        /// Gets a copy of the payment object.
        /// </summary>
        /// <returns>A copy of the payment object.</returns>
        public override object Clone()
        {
            BorrowerClosingCostFeePayment payment = (BorrowerClosingCostFeePayment)this.MemberwiseClone();
            payment.SetParent(null);
            return payment;
        }

        /// <summary>
        /// Validates the Borrower Fee Payment. Currently only used for Discount Points Fee Payments.
        /// </summary>
        /// <param name="message">The output message.</param>
        /// <returns>True if the payment is valid, false otherwise.</returns>
        internal bool ValidateBorrowerFeePayment(out string message)
        {
            if (this.ParentClosingCostFee != null && 
                this.ParentClosingCostFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId &&
                this.InternalPaidByT == E_ClosingCostFeePaymentPaidByT.Lender)
            {
                message = "Paid By property for Discount Point Fee cannot be set to Lender.";
                return false;
            }

            message = "Borrower payment validated.";
            return true;
        }

        /// <summary>
        /// Since JSON serialization/deserialization set to private member variables directly which bypass some logic in the getter / setter.
        /// This function is a centralize of the logics on the absolute fact or guarantee of this object.
        /// </summary>
        internal override void EnsureCorrectness()
        {
            base.EnsureCorrectness();

            if (this.InternalPaidByT == E_ClosingCostFeePaymentPaidByT.Other || this.InternalPaidByT == E_ClosingCostFeePaymentPaidByT.LeaveBlank)
            {
                this.InternalPaidByT = E_ClosingCostFeePaymentPaidByT.Borrower;
            }

            var temp = this.ResponsiblePartyT;
        }

        /// <summary>
        /// Resets the paid by value to the default value.
        /// </summary>
        protected override void ResetInternalPaidByT()
        {
            this.InternalPaidByT = E_ClosingCostFeePaymentPaidByT.Borrower;
        }
    }
}
