﻿// <copyright file="LoanClosingCostFeePayment.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   7/9/2015
// </summary>
namespace DataAccess
{
    using System;
    using System.Runtime.Serialization;
    using DataAccess.PathDispatch;
    using LendersOffice.Migration;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// Base class for closing cost fee payments.
    /// </summary>
    [DataContract]
    [KnownType(typeof(SellerClosingCostFeePayment))]
    [KnownType(typeof(BorrowerClosingCostFeePayment))]
    public abstract class LoanClosingCostFeePayment : ICloneable, IPathResolvable
    {
        /// <summary>
        /// A predicate to determine whether or not a payment should be included in the total of APR fees,
        /// assuming that the fee the payment is part of has IsApr == true.
        /// </summary>
        public static readonly Func<LoanClosingCostFeePayment, LoanVersionT, bool> AprFeePaymentIsAprPredicate = (payment, version) =>
            (payment.PaidByT == E_ClosingCostFeePaymentPaidByT.Borrower 
                || payment.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance)
            && (!LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(version, LoanVersionT.V21_DontTreatNegativeFeePaymentAmountsAsFinanceCharges)
                || payment.Amount >= 0);

        /// <summary>
        /// When the payment is/was/will be made.
        /// </summary>
        private string internalPaymentDate;

        /// <summary>
        /// DO NOT USE DIRECTLY.  Only for use by <see cref="Id"/>.
        /// Needed because the loans on production may not have a valid Guid for their value.
        /// </summary>
        [DataMember(Name = "id")]
        private string stringId;

        /// <summary>
        /// DO NOT USE DIRECTLY.  Only for use by <see cref="Id"/>.
        /// A Guid version of the id; always set this with <see cref="stringId"/>.
        /// </summary>
        private Guid id;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanClosingCostFeePayment" /> class.
        /// </summary>
        public LoanClosingCostFeePayment()
        {
            this.Id = Guid.NewGuid();
            this.InternalLosConvert = null;
            this.InternalGfeClosingCostFeePaymentTimingT = E_GfeClosingCostFeePaymentTimingT.AtClosing;
            this.ParentClosingCostFee = null;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the payment record is auto calculate by the system.
        /// </summary>
        /// <value>Whether the payment record is auto calculate by the system.</value>
        [DataMember(Name = "is_system")]
        public bool IsSystemGenerated
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the entity this payment refers to.
        /// </summary>
        /// <value>The entity this payment refers to.</value>
        [DataMember(Name = "ent")]
        public E_AgentRoleT Entity
        {
            get;
            set;
        }

        /// <summary>
        /// Gets a value indicating whether the payment is financed by borrower.
        /// </summary>
        /// <value>Whether the payment is financed by borrower.</value>
        public bool IsFinanced
        {
            get
            {
                return this.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not this payment has actually been made yet.
        /// </summary>
        /// <value>Whether or not this payment has actually been made yet.</value>
        [DataMember(Name = "made")]
        public bool IsMade
        {
            get;
            set;
        }

        /// <summary>
        /// Gets a unique id of the payment. Be careful with the setter.
        /// </summary>
        /// <value>Unique id of the payment.</value>
        public Guid Id
        {
            get
            {
                return this.id;
            }

            internal set
            {
                this.id = value;
                this.stringId = value.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the dollar amount transferred.
        /// </summary>
        /// <value>The dollar amount transferred.</value>
        public decimal Amount
        {
            get
            {
                return this.ToMoney(this.InternalAmount);
            }

            set
            {
                this.InternalAmount = this.ToMoneyString(value);
            }
        }

        /// <summary>
        /// Gets or sets the dollar amount transferred.
        /// </summary>
        /// <value>The dollar amount transferred.</value>
        public string Amount_rep
        {
            get
            {
                return this.LosConvert.ToMoneyString(this.Amount, FormatDirection.ToRep);
            }

            set
            {
                this.Amount = this.LosConvert.ToMoney(value);
            }
        }

        /// <summary>
        /// Gets or sets the paid by party of this payment.
        /// </summary>
        /// <value>The paid by party of this payment.</value>
        public virtual E_ClosingCostFeePaymentPaidByT PaidByT
        {
            get
            {
                return this.InternalPaidByT;
            }

            set
            {
                this.InternalPaidByT = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether or not the payment should count towards the total of APR fees.
        ///     Used by DoD instead of the IsIncludedInApr value.
        /// </summary>
        /// <value>Whether the APR checkbox for the fee is checked.</value>
        public bool IsAPRRaw
        {
            get { return this.ParentClosingCostFee.IsApr; }
        }

        /// <summary>
        /// Gets or sets when the payment was made relative to closing.
        /// </summary>
        /// <value>When the payment was made relative to closing.</value>
        public E_GfeClosingCostFeePaymentTimingT GfeClosingCostFeePaymentTimingT
        {
            get
            {
                if (this.InternalGfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.LeaveBlank)
                {
                    this.InternalGfeClosingCostFeePaymentTimingT = E_GfeClosingCostFeePaymentTimingT.AtClosing;
                }

                return this.InternalGfeClosingCostFeePaymentTimingT;
            }

            set
            {
                if (value == E_GfeClosingCostFeePaymentTimingT.LeaveBlank)
                {
                    throw new CBaseException("Payable property cannot be set to Blank", "Payable property cannot be set to Blank");
                }
                else
                {
                    this.InternalGfeClosingCostFeePaymentTimingT = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the type of party that is responsible for this payment.
        /// </summary>
        /// <value>The type of party that is responsible for this payment.</value>
        public virtual E_GfeResponsiblePartyT ResponsiblePartyT
        {
            get
            {
                return this.InternalResponsiblePartyT;
            }

            set
            {
                this.InternalResponsiblePartyT = value;
            }
        }

        /// <summary>
        /// Gets or sets when the payment is/was/will be made.
        /// </summary>
        /// <value>When the payment is/was/will be made.</value>
        public DateTime PaymentDate
        {
            get
            {
                return this.ToDate(this.InternalPaymentDate);
            }

            set
            {
                this.InternalPaymentDate = this.ToDateString(value);
            }
        }

        /// <summary>
        /// Gets or sets when the payment is/was/will be made.
        /// </summary>
        /// <value>When the payment is/was/will be made.</value>
        public string PaymentDate_rep
        {
            get
            {
                return this.ToDateRep(this.PaymentDate);
            }

            set
            {
                this.PaymentDate = this.FromDateRep(value);
            }
        }

        /// <summary>
        /// Gets or sets the parent closing cost fee. Must be a LoanClosingCostFee.
        /// </summary>
        /// <value>The parent closing cost fee.</value>
        protected LoanClosingCostFee ParentClosingCostFee
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the internal conversion that use to serialize money, percent as string in JSON. Use WebForm as default.
        /// </summary>
        /// <value>The internal conversion used for serialization.</value>
        protected LosConvert InternalLosConvert
        {
            get;
            set;
        }
        
        /// <summary>
        /// Gets or sets the internal dollar amount transferred.
        /// </summary>
        /// <value>The internal dollar amount transferred.</value>
        [DataMember(Name = "amt")]
        protected string InternalAmount
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the internal paid by value.
        /// </summary>
        /// <value>The internal paid by value.</value>
        [DataMember(Name = "paid_by")]
        protected E_ClosingCostFeePaymentPaidByT InternalPaidByT
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the internal payment timing value.
        /// </summary>
        /// <value>The internal payment timing value.</value>
        [DataMember(Name = "pmt_at")]
        protected E_GfeClosingCostFeePaymentTimingT InternalGfeClosingCostFeePaymentTimingT
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the type of party that is responsible for this payment.
        /// </summary>
        /// <value>The type of party that is responsible for this payment.</value>
        [DataMember(Name = "responsible")]
        protected E_GfeResponsiblePartyT InternalResponsiblePartyT
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets when the payment is/was/will be made.
        /// </summary>
        /// <value>When the payment is/was/will be made.</value>
        [DataMember(Name = "pmt_dt")]
        protected virtual string InternalPaymentDate
        {
            get
            {
                if (this.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.AtClosing &&
                    this.ParentClosingCostFee != null &&
                    this.ParentClosingCostFee.ParentClosingCostSet != null &&
                    this.ParentClosingCostFee.ParentClosingCostSet.DataLoan != null)
                {
                    return this.ParentClosingCostFee.ParentClosingCostSet.DataLoan.sConsummationD_rep;
                }

                return this.internalPaymentDate;
            }

            set
            {
                this.internalPaymentDate = value;
            }
        }

        /// <summary>
        /// Gets the conversion format.
        /// </summary>
        /// <value>The conversion format.</value>
        protected virtual LosConvert LosConvert
        {
            get
            {
                if (this.ParentClosingCostFee != null)
                {
                    if (this.ParentClosingCostFee.ParentClosingCostSet != null)
                    {
                        return this.ParentClosingCostFee.ParentClosingCostSet.LosConvert;
                    }
                }

                if (this.InternalLosConvert == null)
                {
                    this.InternalLosConvert = new LosConvert();
                }

                return this.InternalLosConvert;
            }
        }

        /// <summary>
        /// Gets a value indicating whether or not the payment should count towards the total of APR fees.
        /// </summary>
        /// <param name="loanVersion">The loan version.</param>
        /// <returns>True if the payment should count towards the total of APR fees. Otherwise, false.</returns>
        public bool IsIncludedInApr(LoanVersionT loanVersion)
        {
            return this.ParentClosingCostFee.IsApr && AprFeePaymentIsAprPredicate(this, loanVersion);
        }

        /// <summary>
        /// Gets the paid by party of this payment, with the additional logic that if the fee is lender-paid
        /// and the lender-paid fee disclosure location is on the closing documents, we return Borrower
        /// instead of Lender. This is meant to be used to generate Initial/Loan Estimate documents.
        /// </summary>
        /// <param name="creditDiscloseLocation">The document that will disclose the lender credit.</param>
        /// <param name="isClosingPackage">Indicates whether the package being generated is for closing.</param>
        /// <returns>The Paid By type, adjusted if necessary.</returns>
        public E_ClosingCostFeePaymentPaidByT GetPaidByType(E_LenderCreditDiscloseLocationT creditDiscloseLocation, bool isClosingPackage)
        {
            if (creditDiscloseLocation == E_LenderCreditDiscloseLocationT.ClosingHud1
                && this.PaidByT == E_ClosingCostFeePaymentPaidByT.Lender && !isClosingPackage)
            {
                return E_ClosingCostFeePaymentPaidByT.Borrower;
            }

            return this.PaidByT;
        }

        /// <summary>
        /// Gets a copy of the payment object.
        /// </summary>
        /// <returns>A copy of the payment object.</returns>
        public abstract object Clone();

        /// <summary>
        /// Returns the value specified by the element.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns>The value specified by the element.</returns>
        public object GetElement(IDataPathElement element)
        {
            return PathResolver.GetStringUsingReflection(this, element);
        }
        
        /// <summary>
        /// Set the parent fee of this payment.
        /// </summary>
        /// <param name="parentFee">Parent fee.</param>
        internal void SetParent(LoanClosingCostFee parentFee)
        {
            this.ParentClosingCostFee = parentFee;
        }

        /// <summary>
        /// Since JSON serialization/deserialization set to private member variables directly which bypass some logic in the getter / setter.
        /// This function is a centralize of the logics on the absolute fact or guarantee of this object.
        /// </summary>
        internal virtual void EnsureCorrectness()
        {
            if (this.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance)
            {
                this.GfeClosingCostFeePaymentTimingT = E_GfeClosingCostFeePaymentTimingT.AtClosing;
            }
        }

        /// <summary>
        /// Perform initialization after object is deserialized.
        /// </summary>
        /// <param name="context">Serialized context.</param>
        [OnDeserialized]
        internal void OnDeserialized(StreamingContext context)
        {
            this.Initialization();
        }

        /// <summary>
        /// Perform initialization.
        /// </summary>
        internal void Initialization()
        {
            // 2/3/2015 dd - Set internalAmount, and internalPaymentDate to empty string when it is null for easier handling on client script.
            if (string.IsNullOrEmpty(this.InternalAmount))
            {
                this.InternalAmount = "$0.00";
            }

            if (this.InternalPaymentDate == null)
            {
                this.InternalPaymentDate = string.Empty;
            }

            if (this.Id == Guid.Empty)
            {
                Guid result;
                this.Id = Guid.TryParse(this.stringId, out result) && result != Guid.Empty ? result : Guid.NewGuid();
            }

            // If this gets mysteriously gets set to -1 (probably from the split option), then reset it back to the default for that particular payment.
            if ((int)this.InternalPaidByT < 0)
            {
                this.ResetInternalPaidByT();
            }
        }

        /// <summary>
        /// Resets the paid by value to the default value.
        /// </summary>
        protected abstract void ResetInternalPaidByT();

        /// <summary>
        /// Perform setup before object is serialize.
        /// </summary>
        /// <param name="context">Serialized context.</param>
        [OnSerializing]
        private void SetValuesOnSerializing(StreamingContext context)
        {
            if (this.Id == Guid.Empty)
            {
                Guid result;
                this.Id = Guid.TryParse(this.stringId, out result) && result != Guid.Empty ? result : Guid.NewGuid();
            }
        }

        /// <summary>
        /// Convert string money to decimal value.
        /// </summary>
        /// <param name="str">Money in string format.</param>
        /// <returns>Decimal value.</returns>
        private decimal ToMoney(string str)
        {
            if (this.InternalLosConvert == null)
            {
                this.InternalLosConvert = new LosConvert();
            }

            return this.InternalLosConvert.ToMoney(str);
        }

        /// <summary>
        /// Convert decimal value to string format.
        /// </summary>
        /// <param name="value">Decimal value.</param>
        /// <returns>String value.</returns>
        private string ToMoneyString(decimal value)
        {
            if (this.InternalLosConvert == null)
            {
                this.InternalLosConvert = new LosConvert();
            }

            return this.InternalLosConvert.ToMoneyString(value, FormatDirection.ToRep);
        }

        /// <summary>
        /// Convert string in format <code>MM/dd/yyyy</code> to DateTime object.
        /// </summary>
        /// <param name="str">Date in string format.</param>
        /// <returns>DateTime object.</returns>
        private DateTime ToDate(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return DateTime.MinValue;
            }

            DateTime dt;

            if (DateTime.TryParse(str, out dt) == false)
            {
                return DateTime.MinValue;
            }

            return dt;
        }

        /// <summary>
        /// Convert date time format to string <code>MM/dd/yyyy</code>.
        /// </summary>
        /// <param name="dt">Date time object.</param>
        /// <returns>Date in string format.</returns>
        private string ToDateString(DateTime dt)
        {
            if (dt == DateTime.MinValue)
            {
                return string.Empty;
            }

            return dt.ToString("MM/dd/yyyy");
        }

        /// <summary>
        /// Convert string to date time object.
        /// </summary>
        /// <param name="str">Date represent as string.</param>
        /// <returns>A date time object.</returns>
        private DateTime FromDateRep(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return DateTime.MinValue;
            }

            CDateTime dt = CDateTime.Create(str, this.LosConvert);

            return dt.DateTimeForComputation;
        }

        /// <summary>
        /// Convert a date time object to string format.
        /// </summary>
        /// <param name="dt">A date time object.</param>
        /// <returns>A string rep.</returns>
        private string ToDateRep(DateTime dt)
        {
            CDateTime datetime = CDateTime.Create(dt);

            return datetime.ToString(this.LosConvert);
        }
    }
}
