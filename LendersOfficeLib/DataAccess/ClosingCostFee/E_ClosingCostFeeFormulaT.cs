﻿// <copyright file="E_ClosingCostFeeFormulaT.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   12/10/2014 2:49:06 PM 
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// An enum for different view of the follow.
    /// </summary>
    public enum E_ClosingCostFeeFormulaT
    {
        /// <summary>
        /// Leave Blank.
        /// </summary>
        LeaveBlank = 0,

        /// <summary>
        /// Percent * PercentBase + MinBase.
        /// </summary>
        Full = 1,

        /// <summary>
        /// Percent * PercentBase.
        /// </summary>
        PercentOnly = 2,

        /// <summary>
        /// MinBase only.
        /// </summary>
        MinBaseOnly = 3,

        /// <summary>
        /// For Mortgage Insurance Premium with recurring payments.
        /// </summary>
        MortgageInsurancePremiumRecurring = 4,

        /// <summary>
        /// Always use the read-only field <code>sVaFf</code> from the data layer.
        /// </summary>
        VAFundingFee = 5,

        /// <summary>
        /// Formula for the 903 hazard insurance. Use field from the data layer.
        /// </summary>
        sHazInsPia = 6,

        /// <summary>
        /// For Per-Diem interest.
        /// </summary>
        sIPia = 7,

        /// <summary>
        /// For loan origination fee.
        /// </summary>
        sLOrigFPc = 8,

        /// <summary>
        /// Reserve - Hazard Insurance.
        /// </summary>
        sHazInsRsrv = 9,

        /// <summary>
        /// Reserve - Mortgage Insurance.
        /// </summary>
        sMInsRsrv = 10,

        /// <summary>
        /// Reserve - Property Tax.
        /// </summary>
        sRealETxRsrv = 11,

        /// <summary>
        /// Reserve - School Tax.
        /// </summary>
        sSchoolTxRsrv = 12,

        /// <summary>
        /// Reserve - Flood Insurance.
        /// </summary>
        sFloodInsRsrv = 13,

        /// <summary>
        /// Reserve - Aggregate Adjustment.
        /// </summary>
        sAggregateAdjRsrv = 14,

        /// <summary>
        /// Reserve - Custom 1.
        /// </summary>
        s1006Rsrv = 15,

        /// <summary>
        /// Reserve - Custom 2.
        /// </summary>
        s1007Rsrv = 16,

        /// <summary>
        /// Discount Point.
        /// </summary>
        sLDiscnt = 17,

        /// <summary>
        /// Section G - Initial Escrow Payment At Closing. This formula type will pull from propose housing expense.
        /// </summary>
        InitialEscrowPaymentAtClosing = 18,

        /// <summary>
        /// Originator Compensation.
        /// </summary>
        sOriginatorCompensationTotalAmount = 19,

        /// <summary>
        /// Section F - Prepaid expenses. Will pull prepaid amount from expenses.
        /// </summary>
        PrepaidAmountForExpenses = 20,

        /// <summary>
        /// For Mortgage Insurance Premium paid upfront.
        /// </summary>
        MortgageInsurancePremiumUpfront = 21
    }
}