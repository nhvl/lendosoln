﻿// <copyright file="LoanClosingCostSet.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   7/9/2015
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.Security;

    /// <summary>
    /// The base class for all closing cost sets that belong in a loan. DO NOT SERIALIZE/DESERIALIZE INTO THIS CLASS DIRECTLY.
    /// </summary>
    public abstract class LoanClosingCostSet : BaseClosingCostSet
    {
        /// <summary>
        /// The format convertor to use when accessing data.
        /// </summary>
        private LosConvert losConvert = null;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="LoanClosingCostSet" /> class from json.
        /// </summary>
        /// <param name="json">Json to deserialize.</param>
        public LoanClosingCostSet(string json)
            : base(json)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanClosingCostSet" /> class from json.
        /// </summary>
        /// <param name="json">Json to deserialize.</param>
        /// <param name="isForClearing">Whether this set is for updating/clearing another set.</param>
        public LoanClosingCostSet(string json, bool isForClearing)
            : base(json, isForClearing)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanClosingCostSet" /> class.
        /// </summary>
        /// <param name="loan">The set's data loan object.</param>
        /// <param name="json">Json to deserialize.</param>
        public LoanClosingCostSet(CPageBase loan, string json)
        {
            this.DataLoan = loan;
            this.Initialize(json);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanClosingCostSet" /> class from json. 
        /// </summary>
        /// <param name="json">Json to deserialize.</param>
        /// <param name="convertor">The <see cref="LosConvert" /> object to use when accessing data.</param>
        public LoanClosingCostSet(string json, LosConvert convertor)
        {
            this.losConvert = convertor;
            this.Initialize(json);
        }
        
        /// <summary>
        /// Gets the conversion object for this closing cost set.
        /// </summary>
        /// <value>The conversion object for this closing cost set.</value>
        public LosConvert LosConvert
        {
            get
            {
                if (this.DataLoan != null)
                {
                    return this.DataLoan.m_convertLos;
                }
                else if (this.losConvert != null)
                {
                    return this.losConvert;
                }
                else
                {
                    return new LosConvert();
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether an actual data loan object is associate with this closing cost set.
        /// </summary>
        /// <value>A value indicating whether an actual data loan object is associate with this closing cost set.</value>
        public virtual bool HasDataLoanAssociate
        {
            get 
            { 
                return this.DataLoan != null; 
            }
        }

        /// <summary>
        /// Gets a value indicating whether an actual closing cost archive is associate with this closing cost set.
        /// </summary>
        /// <value>A value indicating whether an actual closing cost archive is associate with this closing cost set.</value>
        public bool HasClosingCostArchiveAssociate
        {
            get { return this.ClosingCostArchive != null; }
        }

        /// <summary>
        /// Gets or sets the closing cost archive object.  It can be null.
        /// </summary>
        /// <value>The closing cost archive associated with the set, if there is one.</value>
        public ClosingCostArchive ClosingCostArchive
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets the data loan object.
        /// </summary>
        internal virtual CPageBase DataLoan
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the broker level closing cost set. Cached for performance reasons.
        /// </summary>
        /// <value>The broker level closing cost set.</value>
        internal FeeSetupClosingCostSet BrokerClosingCostSet
        {
            get;
            set;
        }

        /// <summary>
        /// Clears associated beneficiary agent id and beneficiary description for any fees associated with a given agent.
        /// </summary>
        /// <param name="beneficiaryAgentId">The agent Id to be cleared from any associated fees.</param>
        /// <remarks>Does not affect the value of beneficiary agent role, is third party, or is affiliate for the affected fees.</remarks>
        public void ClearBeneficiary(Guid beneficiaryAgentId)
        {
            foreach (LoanClosingCostFee fee in this.ClosingCostFeeList)
            {
                if (fee.BeneficiaryAgentId == beneficiaryAgentId)
                {
                    fee.ClearBeneficiary();
                }
            }
        }

        /// <summary>
        /// Sum of all the fees that meets set of criteria.
        /// </summary>
        /// <param name="feePredicate">The criteria for the fee.</param>
        /// <param name="paymentPredicate">The criteria for the payment.</param>
        /// <returns>The total amount of fee.</returns>
        public decimal Sum(Func<LoanClosingCostFee, bool> feePredicate, Func<LoanClosingCostFeePayment, bool> paymentPredicate)
        {
            List<decimal> list = new List<decimal>();

            foreach (LoanClosingCostFee fee in this.ClosingCostFeeList)
            {
                if (!feePredicate(fee))
                {
                    continue;
                }

                foreach (LoanClosingCostFeePayment payment in fee.Payments)
                {
                    if (paymentPredicate(payment))
                    {
                        list.Add(payment.Amount);
                    }
                }
            }

            return this.SumMoney(list);
        }

        /// <summary>
        /// Sum of all the fees that meets set of criteria.
        /// </summary>
        /// <param name="predicate">The criteria.</param>
        /// <returns>The total amount of fee.</returns>
        public decimal Sum(Func<LoanClosingCostFee, bool> predicate)
        {
            List<decimal> list = new List<decimal>();

            foreach (LoanClosingCostFee fee in this.ClosingCostFeeList)
            {
                if (predicate(fee))
                {
                    list.Add(fee.TotalAmount);
                }
            }

            return this.SumMoney(list);
        }

        /// <summary>
        /// Add or update closing cost fee to the set. If fee.Id is Guid.Empty or not existed in the set then fee will get add. Otherwise it will be replace with existing one.
        /// </summary>
        /// <param name="fee">A closing cost fee to be add or update.</param>
        public override void AddOrUpdate(BaseClosingCostFee fee)
        {
            if (fee.SourceFeeTypeId.HasValue)
            {
                // Delete standard fee if adding subfee.
                this.ClosingCostFeeList.RemoveAll(f => f.ClosingCostFeeTypeId == fee.SourceFeeTypeId);

                // Update fee history.
                // Note: This needs to happen here or reassignment won't work correctly.
                if (this.HasDataLoanAssociate)
                {
                    this.DataLoan.UpdateDynamicClosingCostFeeHistory(fee);
                }
            }
            else
            {
                // Delete subfees if adding a standard fee.
                this.ClosingCostFeeList.RemoveAll(f => f.SourceFeeTypeId == fee.ClosingCostFeeTypeId);

                // Check for subfees with same HUD line, and reassign their HUD lines.
                if (!ClosingCostSetUtils.HudLinesAllowingMultipleFees.Contains(fee.HudLine) && this.HasDataLoanAssociate)
                {
                    foreach (LoanClosingCostFee subfee in this.FindFeeByHudline(fee.HudLine).Where(f => f.SourceFeeTypeId.HasValue))
                    {
                        LendersOffice.ObjLib.TitleProvider.TitleService.ReassignHudLine(this.DataLoan, this, subfee);
                    }
                }
            }

            this.ClearFeeLookupCache();

            base.AddOrUpdate(fee);
        }

        /// <summary>
        /// Gets the percent base amount from the associated loan object if it exists.
        /// </summary>
        /// <param name="percentBaseT">Percent base type.</param>
        /// <returns>Percent base amount from loan object. 0 if the data loan object doesn't exist.</returns>
        internal decimal GetPercentBaseAmount(E_PercentBaseT percentBaseT)
        {
            if (this.DataLoan == null)
            {
                return 0;
            }

            switch (percentBaseT)
            {
                case E_PercentBaseT.LoanAmount: // Loan amount
                    return this.DataLoan.sLAmtCalc;
                case E_PercentBaseT.SalesPrice: // sPurchPrice
                    return this.DataLoan.sPurchPrice;
                case E_PercentBaseT.AppraisalValue: // Appraisal value
                    return this.DataLoan.sApprVal != 0 ? this.DataLoan.sApprVal : this.DataLoan.sPurchPrice; // OPM 126087. Appraised Value Should Never Return Zero for the Purpose of Closing Cost Calculations.
                case E_PercentBaseT.TotalLoanAmount:
                    return this.DataLoan.sFinalLAmt;
                case E_PercentBaseT.OriginalCost:
                    return 100000;
                case E_PercentBaseT.AverageOutstandingBalance:
                    return 100000;
                case E_PercentBaseT.AllYSP:
                    return 100000;
                case E_PercentBaseT.DecliningRenewalsMonthly:
                    return 100000;
                case E_PercentBaseT.DecliningRenewalsAnnually:
                    return 100000;
                default:
                    throw new UnhandledEnumException(percentBaseT);
            }
        }

        /// <summary>
        /// Gets the value of percent base for loan origination fee. If loan type is VA then use total loan amount, otherwise loan amount.
        /// </summary>
        /// <returns>Percent base type.</returns>
        internal E_PercentBaseT GetPercentBaseTForLoanOriginationFee()
        {
            if (this.DataLoan == null)
            {
                return E_PercentBaseT.LoanAmount;
            }

            if (this.DataLoan.sLT == E_sLT.VA)
            {
                return E_PercentBaseT.TotalLoanAmount;
            }
            else
            {
                return E_PercentBaseT.LoanAmount;
            }
        }

        /// <summary>
        /// For GetFirstFeeByLegacyType. Gets the fee from the fee setup.
        /// </summary>
        /// <param name="brokerId">The broker id for the fee setup.</param>
        /// <param name="type">The legacy field type.</param>
        /// <param name="addFeeToSet">Whether or not we add the fee to the set.</param>
        /// <returns>The fee that was made from the fee setup fee.</returns>
        internal LoanClosingCostFee GetBrokerFee(Guid brokerId, E_LegacyGfeFieldT type, bool addFeeToSet)
        {
            // 1/8/2015 dd - Get the default fee from lender settings.
            if (brokerId != Guid.Empty)
            {
                if (this.BrokerClosingCostSet == null)
                {
                    BrokerDB broker = BrokerDB.RetrieveById(brokerId);
                    this.BrokerClosingCostSet = broker.GetUnlinkedClosingCostSet();
                }

                FeeSetupClosingCostFee brokerFee = (FeeSetupClosingCostFee)this.BrokerClosingCostSet.GetFirstFeeByLegacyType(type, Guid.Empty, false);

                LoanClosingCostFee fee = null;
                if (brokerFee != null)
                {
                    LoanClosingCostFee convertedFee = this.CreateNewFee();
                    brokerFee.InitializeLoanFee(convertedFee);
                    
                    convertedFee.ClearBeneficiary();
                    fee = convertedFee;
                }

                if (fee != null)
                {
                    fee.OriginalDescription = fee.Description;
                    fee.SetClosingCostSet(this);
                    if (addFeeToSet)
                    {
                        this.AddOrUpdate(fee);
                    }

                    return fee;
                }
            }

            return null;
        }
        
        /// <summary>
        /// Gets the fee from the fee setup.
        /// </summary>
        /// <param name="brokerId">The broker id for the fee setup.</param>
        /// <param name="typeId">The fee type ID.</param>
        /// <param name="addFeeToSet">Whether or not we add the fee to the set.</param>
        /// <returns>The fee that was made from the fee setup fee.</returns>
        internal LoanClosingCostFee GetBrokerFee(Guid brokerId, Guid typeId, bool addFeeToSet)
        {
            if (brokerId == Guid.Empty)
            {
                return null;
            }

            if (this.BrokerClosingCostSet == null)
            {
                BrokerDB broker = BrokerDB.RetrieveById(brokerId);
                this.BrokerClosingCostSet = broker.GetUnlinkedClosingCostSet();
            }

            FeeSetupClosingCostFee brokerFee = (FeeSetupClosingCostFee)this.BrokerClosingCostSet.FindFeeByTypeId(typeId);

            LoanClosingCostFee fee = null;
            if (brokerFee != null)
            {
                LoanClosingCostFee convertedFee = this.CreateNewFee();
                brokerFee.InitializeLoanFee(convertedFee);

                convertedFee.ClearBeneficiary();
                fee = convertedFee;
            }

            if (fee != null)
            {
                fee.OriginalDescription = fee.Description;
                fee.SetClosingCostSet(this);
                if (addFeeToSet)
                {
                    this.AddOrUpdate(fee);
                }
            }

            return fee;
        }

        /// <summary>
        /// Creates a fake fee cloned from the subfee with the highest amount,
        /// but with amount set to the sum of all subfees of the same type.
        /// </summary>
        /// <remarks>For OPM 457222.</remarks>
        /// <param name="closingCostFeeTypeId">Source fee type ID used to search for subfees.</param>
        /// <returns>
        /// A fake fee with amount set to the sum of alls subfees and props taken from 
        /// the subfee with the largest amount value.
        /// </returns>
        protected LoanClosingCostFee CreateDummyFeeFromSubFees(Guid closingCostFeeTypeId)
        {
            if (closingCostFeeTypeId == Guid.Empty)
            {
                return null;
            }

            IEnumerable<LoanClosingCostFee> subFees = this.ClosingCostFeeList.Where(f => f.SourceFeeTypeId == closingCostFeeTypeId)
                        .Cast<LoanClosingCostFee>()
                        .OrderBy(f => f.TotalAmount)
                        .ThenBy(f => f.Description);

            if (!subFees.Any())
            {
                return null;
            }

            LoanClosingCostFee fee = (LoanClosingCostFee)subFees.First().Clone();
            fee.FormulaT = E_ClosingCostFeeFormulaT.MinBaseOnly;
            fee.BaseAmount = subFees.Sum(f => f.TotalAmount);

            return fee;
        }

        /// <summary>
        /// Creates a new blank fee. Creates the set's specific fee type.
        /// </summary>
        /// <returns>Creates a new blank fee.</returns>
        protected abstract LoanClosingCostFee CreateNewFee();

        /// <summary>
        /// Sets the Dflp value for the destination fee. Requires a principal and AllowCloserWrite permissions.
        /// </summary>
        /// <param name="fee">The fee to set the Dflp for.</param>
        /// <param name="sourceFee">The source fee.</param>
        protected override void SetDflp(BaseClosingCostFee fee, BaseClosingCostFee sourceFee)
        {
            // Skip all checks if the data isn't changing (needed for testing without principals...)
            if (fee.Dflp == sourceFee.Dflp)
            {
                return;
            }

            if (PrincipalFactory.CurrentPrincipal == null)
            {
                throw new NullReferenceException("The current principal is null when setting the ClosingCostSet.");
            }
            else if (!PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowCloserWrite))
            {
                fee.Dflp = sourceFee.Dflp;
            }
        }

        /// <summary>
        /// Gets the HUD view of this closing cost set.
        /// </summary>
        /// <returns>The list of closing cost sections in HUD view.</returns>
        protected abstract IEnumerable<BaseClosingCostFeeSection> GetHudView();

        /// <summary>
        /// Gets the custom fee by hud section.
        /// </summary>
        /// <param name="hudLine">The hud line number.</param>
        /// <param name="closingCostFeeTypeId">The fee type id of the custom fee.</param>
        /// <param name="addFee">Whether the fee should be added to the closing cost set.</param>
        /// <returns>The custom fee or a replacement fee if it is not found.</returns>
        protected override BaseClosingCostFee GetCustomFee(int hudLine, Guid closingCostFeeTypeId, bool addFee)
        {
            IEnumerable<BaseClosingCostFee> matchingFees = this.FindFeeByHudline(hudLine);
            int count = matchingFees.Count();

            if (count > 0)
            {
                if (count > 1)
                {
                    Tools.LogError("GetCustomFee found 2 matching fees, don't think this should happen " + hudLine + " type " + closingCostFeeTypeId + " addFee " + addFee);
                }

                return matchingFees.First();
            }

            // We do not have this custom fee currently.  Before we fall back to
            // returning a blank fee, check if we can show a generic fee
            // on this line, per OPM 213691.
            LoanClosingCostFee replacementFee = this.GetReplacementCustomFee(hudLine);
            if (replacementFee != null)
            {
                return replacementFee;
            }

            var section = this.GetHudView().First(p => p.HudLineStart <= hudLine && p.HudLineEnd >= hudLine);

            LoanClosingCostFee blankFee = this.PrepCustomFee(hudLine, closingCostFeeTypeId, section);

            if (addFee)
            {
                this.AddOrUpdate(blankFee);
            }

            return blankFee;
        }

        /// <summary>
        /// Creates a custom fee based on the fee template from the section.
        /// </summary>
        /// <param name="hudLine">The new fee's hudline.</param>
        /// <param name="closingCostFeeTypeId">The new fee's closing cost fee type id.</param>
        /// <param name="section">The fee section that contains the new fee's template.</param>
        /// <returns>The new custom fee.</returns>
        protected abstract LoanClosingCostFee PrepCustomFee(int hudLine, Guid closingCostFeeTypeId, BaseClosingCostFeeSection section);

        /// <summary>
        /// Gets the original description for the custom fee types.
        /// </summary>
        /// <param name="typeId">Custom fee type id.</param>
        /// <returns>A fee description.</returns>
        protected string GetCustomFeeOriginalDescription(Guid typeId)
        {
            if (typeId == DefaultSystemClosingCostFee.Hud800Custom1FeeTypeId)
            {
                return "Misc. section 800 fee 1";
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud800Custom2FeeTypeId)
            {
                return "Misc. section 800 fee 2";
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud800Custom3FeeTypeId)
            {
                return "Misc. section 800 fee 3";
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud800Custom4FeeTypeId)
            {
                return "Misc. section 800 fee 4";
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud800Custom5FeeTypeId)
            {
                return "Misc. section 800 fee 5";
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud900Custom1FeeTypeId)
            {
                return "Misc. section 900 fee 1";
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud900Custom2FeeTypeId)
            {
                return "Misc. section 900 fee 2";
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1100Custom1FeeTypeId)
            {
                return "Misc. section 1100 fee 1";
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1100Custom2FeeTypeId)
            {
                return "Misc. section 1100 fee 2";
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1100Custom3FeeTypeId)
            {
                return "Misc. section 1100 fee 3";
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1100Custom4FeeTypeId)
            {
                return "Misc. section 1100 fee 4";
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1200Custom1FeeTypeId)
            {
                return "Misc. section 1200 fee 1";
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1200Custom2FeeTypeId)
            {
                return "Misc. section 1200 fee 2";
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1200Custom3FeeTypeId)
            {
                return "Misc. section 1200 fee 3";
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1300Custom1FeeTypeId)
            {
                return "Misc. section 1300 fee 1";
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1300Custom2FeeTypeId)
            {
                return "Misc. section 1300 fee 2";
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1300Custom3FeeTypeId)
            {
                return "Misc. section 1300 fee 3";
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1300Custom4FeeTypeId)
            {
                return "Misc. section 1300 fee 4";
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1300Custom5FeeTypeId)
            {
                return "Misc. section 1300 fee 5";
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Return a generic fee that would fit in this custom line if we have one, per the logic in case 213691.
        /// </summary>
        /// <param name="hudLine">The line number of the fee.</param>
        /// <returns>A corresponding closing cost fee if it exists, otherwise null.</returns>
        private LoanClosingCostFee GetReplacementCustomFee(int hudLine)
        {
            // Generic section lines are 800, 900, 1100, 1200, 1300
            int hudGenericSection = (int)Math.Floor(hudLine / 100d) * 100;

            int[] legacyCustomLines;

            if (hudGenericSection == 800)
            {
                legacyCustomLines = new int[] { 813, 814, 815, 816, 817 };
            }
            else if (hudGenericSection == 900)
            {
                legacyCustomLines = new int[] { 904, 906 };
            }
            else if (hudGenericSection == 1100)
            {
                legacyCustomLines = new int[] { 1112, 1113, 1114, 1115 };
            }
            else if (hudGenericSection == 1200)
            {
                legacyCustomLines = new int[] { 1206, 1207, 1208 };
            }
            else if (hudGenericSection == 1300)
            {
                legacyCustomLines = new int[] { 1303, 1304, 1305, 1306, 1307 };
            }
            else
            {
                return null;
            }

            // Cycle through fees, if the one being requested fits in a spot, return it.
            // Note that we just the use the existing order in the list and if there are
            // more generic fees than can fit in their custom section, they are omitted.
            int legacyIndex = 0;
            foreach (LoanClosingCostFee genericFee in this.ClosingCostFeeList.Where(p => p.HudLine == hudGenericSection))
            {
                // We want to fill in empty fees.
                while (legacyIndex < legacyCustomLines.Count())
                {
                    int currentLine = legacyCustomLines[legacyIndex++];
                    if (this.ClosingCostFeeList.Any(p => p.HudLine == currentLine))
                    {
                        // Occupied line.  Cannot use.
                    }
                    else
                    {
                        if (hudLine == currentLine)
                        {
                            // Line we seek has a fee that maps to it.
                            return genericFee;
                        }
                        else
                        {
                            // This is a valid place for this fee, but it is not
                            // the one we are looking for.  Moving along to next fee.
                            break;
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Sum of a list of money value.
        /// </summary>
        /// <param name="args">List of money.</param>
        /// <returns>The total.</returns>
        private decimal SumMoney(IEnumerable<decimal> args)
        {
            decimal total = 0;
            foreach (decimal a in args)
            {
                total += Math.Round(a, 2);
            }

            return total;
        }
    }
}
