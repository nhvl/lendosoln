﻿// <copyright file="SellerClosingCostSet.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   7/9/2015
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using LendersOffice.Common;
    using LendersOffice.Security;

    /// <summary>
    /// The class representing a Seller-Responsible closing cost set. 
    /// </summary>
    public class SellerClosingCostSet : LoanClosingCostSet
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SellerClosingCostSet" /> class.
        /// </summary>
        /// <param name="json">The json to deserialize.</param>
        public SellerClosingCostSet(string json)
            : base(json)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SellerClosingCostSet" /> class.
        /// </summary>
        /// <param name="loan">The loan to associate with this closing cost set.</param>
        /// <param name="json">The json to deserialize.</param>
        public SellerClosingCostSet(CPageBase loan, string json)
            : base(loan, json)
        { 
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SellerClosingCostSet" /> class.
        /// </summary>
        /// <param name="json">The loan to associate with this closing cost set.</param>
        /// <param name="isForClearing">Whether this set is for updating/clearing another set.</param>
        public SellerClosingCostSet(string json, bool isForClearing)
            : base(json, isForClearing)
        { 
        }

        /// <summary>
        /// Gets or sets a value indicating what type of closing cost set this is.
        /// </summary>
        /// <value>A value indicating what type of closing cost set this is.</value>
        public override E_sClosingCostSetType SetType
        {
            get
            {
                return E_sClosingCostSetType.SellerResponsibleFees;
            }

            protected set
            {
            }
        }

        /// <summary>
        /// Gets the first fee in the list the match the LendingQB legacy field type.
        /// If no fee is found then it will attempt to create new fee from lender settings.
        /// If still no fee from lender settings then a completely blank fee object will create.
        /// </summary>
        /// <param name="type">LendingQB legacy field type.</param>
        /// <param name="brokerId">Broker Id to find lender settings when could not find in list.</param>
        /// <param name="addFeeToSet">Determines whether the fee should be added if it does not exist.</param>
        /// <returns>The first fee with legacy field type.</returns>
        public override BaseClosingCostFee GetFirstFeeByLegacyType(E_LegacyGfeFieldT type, Guid brokerId, bool addFeeToSet)
        {
            BaseClosingCostFee firstcheck = this.CheckForCustomFee(type, brokerId, addFeeToSet);

            if (firstcheck != null)
            {
                return firstcheck;
            }

            Guid feeTypeId = Guid.Empty;

            if (type == E_LegacyGfeFieldT.sGfeOriginatorComp_BorrowerPaid || type == E_LegacyGfeFieldT.sGfeOriginatorCompF || 
                type == E_LegacyGfeFieldT.sGfeOriginatorCompF_Manual)
            {
                feeTypeId = DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId;
            }

            SellerClosingCostFee foundFee = null;
            foreach (SellerClosingCostFee fee in this.ClosingCostFeeList)
            {
                if (fee.LegacyGfeFieldT == type || fee.ClosingCostFeeTypeId == feeTypeId)
                {
                    foundFee = fee;
                    break;
                }
            }

            if (foundFee != null)
            {
                // Return found fee if it is NOT a sub fee.
                if (!foundFee.SourceFeeTypeId.HasValue)
                {
                    return foundFee;
                }

                // OPM 457222 - If this is a subfee and this is a "get" operation
                // (ie. addFeeToSet == false) then return a fake sub fee.
                if (!addFeeToSet)
                {
                    return this.CreateDummyFeeFromSubFees(foundFee.SourceFeeTypeId.Value);   
                }

                // If this is a "set" operation (ie. addFeeToSet == true)
                // then ignore found subfee. It will be overwritten in later code.
            }

            var brokerFee = this.GetBrokerFee(brokerId, type, addFeeToSet);
            if (brokerFee == null)
            {
                // 1/8/2015 dd - If no default fee from lender settings then create blank.
                SellerClosingCostFee blankFee = new SellerClosingCostFee();
                blankFee.SetClosingCostSet(this);
                blankFee.LegacyGfeFieldT = type;
                blankFee.ClosingCostFeeTypeId = feeTypeId;

                Tools.LogWarning("Adding blank fee with possibly incomplete information for legacy fee type " + type);

                if (addFeeToSet)
                {
                    this.AddOrUpdate(blankFee);
                }

                return blankFee;
            }
            else
            {
                return brokerFee;
            }
        }

        /// <summary>
        /// Serializes the underlying fee list to JSON. This specifically serializes a list of SellerClosingCostFees.
        /// </summary>
        /// <returns>The json string.</returns>
        public override string ToJson()
        {
            List<SellerClosingCostFee> list = new List<SellerClosingCostFee>();

            foreach (SellerClosingCostFee fee in this.ClosingCostFeeList)
            {
                // Correct the TP and AFF checkboxes
                var thirdPartyNotUsed = fee.IsThirdParty;
                var affiliateNotUsed = fee.IsAffiliate;

                if (fee.LegacyGfeFieldT == E_LegacyGfeFieldT.Undefined && string.IsNullOrEmpty(fee.Description) && fee.TotalAmount == 0)
                {
                    continue;
                }

                // OPM 214180 - Remove lender paid originator compensation from fees when loan is TRID 2015
                if (fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId 
                    && this.HasDataLoanAssociate 
                    && this.DataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID 
                    && this.DataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)
                {
                    continue;
                }

                list.Add(fee);
            }

            return ObsoleteSerializationHelper.JsonSerializeAndSanitize(list);
        }

        /// <summary>
        /// Gets a list of appropriate fee sections by view type. DO NOT USE THIS FOR SERIALIZATION/DESERIALIZATION.
        /// </summary>
        /// <param name="viewType">The view to construct the sections on.</param>
        /// <returns>The list of fee sections.</returns>
        public override IEnumerable<BaseClosingCostFeeSection> GetViewBase(E_ClosingCostViewT viewType)
        {
            if (viewType != E_ClosingCostViewT.SellerResponsibleLenderTypeEstimate && viewType != E_ClosingCostViewT.SellerResponsibleLoanHud1)
            {
                throw new ArgumentException("SellerClosingCostSet does not take view type: " + Enum.GetName(typeof(E_ClosingCostViewT), viewType));
            }

            List<BaseClosingCostFeeSection> list = new List<BaseClosingCostFeeSection>();

            switch (viewType)
            {
                case E_ClosingCostViewT.SellerResponsibleLoanHud1:
                    SellerClosingCostFee default800Fee = new SellerClosingCostFee()
                    {
                        HudLine = 800,
                        IsTitleFee = false,
                        IsOptional = false,
                        GfeSectionT = E_GfeSectionT.B1,
                        IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionA,
                        IsApr = true,
                        IsFhaAllowable = true,
                        Beneficiary = E_AgentRoleT.Lender,
                        IsThirdParty = false,
                        IsAffiliate = false,
                        CanShop = false,
                    };
                    default800Fee.SetClosingCostSet(this);

                    SellerClosingCostFee default1100Fee = new SellerClosingCostFee()
                    {
                        HudLine = 1100,
                        IsTitleFee = true,
                        IsOptional = false,
                        GfeSectionT = E_GfeSectionT.B4,
                        IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionC,
                        IsApr = false,
                        IsFhaAllowable = true,
                        Beneficiary = E_AgentRoleT.Other,
                        IsThirdParty = false,
                        IsAffiliate = false,
                        CanShop = true
                    };
                    default1100Fee.SetClosingCostSet(this); 

                    SellerClosingCostFee default1200Fee = new SellerClosingCostFee()
                    {
                        HudLine = 1200,
                        IsTitleFee = false,
                        IsOptional = false,
                        GfeSectionT = E_GfeSectionT.B8,
                        IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionE,
                        IsApr = false,
                        IsFhaAllowable = true,
                        Beneficiary = E_AgentRoleT.Other,
                        IsThirdParty = false,
                        IsAffiliate = false,
                        CanShop = false
                    };
                    default1200Fee.SetClosingCostSet(this);

                    SellerClosingCostFee default1300Fee = new SellerClosingCostFee()
                    {
                        HudLine = 1300,
                        IsTitleFee = false,
                        IsOptional = false,
                        GfeSectionT = E_GfeSectionT.B6,
                        IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionC,
                        IsApr = true,
                        IsFhaAllowable = true,
                        Beneficiary = E_AgentRoleT.Other,
                        IsThirdParty = false,
                        IsAffiliate = false,
                        CanShop = true
                    };
                    default1300Fee.SetClosingCostSet(this);

                    list.Add(new SellerClosingCostFeeSection(viewType, this, null, ClosingCostSetUtils.Hud800ItemsPayable, o => o.CheckIfFeeIsInSection(ClosingCostSetUtils.Section800Filter), default800Fee, ClosingCostSetUtils.Start800Hudline, ClosingCostSetUtils.End800Hudline, E_IntegratedDisclosureSectionT.LeaveBlank));
                    list.Add(new SellerClosingCostFeeSection(viewType, this, null, ClosingCostSetUtils.Hud1100Title, o => o.CheckIfFeeIsInSection(ClosingCostSetUtils.Section1100Filter), default1100Fee, ClosingCostSetUtils.Start1100Hudline, ClosingCostSetUtils.End1100Hudline, E_IntegratedDisclosureSectionT.LeaveBlank));
                    list.Add(new SellerClosingCostFeeSection(viewType, this, null, ClosingCostSetUtils.Hud1200Government, o => o.CheckIfFeeIsInSection(ClosingCostSetUtils.Section1200Filter), default1200Fee, ClosingCostSetUtils.Start1200Hudline, ClosingCostSetUtils.End1200Hudline, E_IntegratedDisclosureSectionT.LeaveBlank));
                    list.Add(new SellerClosingCostFeeSection(viewType, this, null, ClosingCostSetUtils.Hud1300AdditionalSettlementCharge, o => o.CheckIfFeeIsInSection(ClosingCostSetUtils.Section1300Filter), default1300Fee, ClosingCostSetUtils.Start1300Hudline, ClosingCostSetUtils.End1300Hudline, E_IntegratedDisclosureSectionT.LeaveBlank));
                    break;
                case E_ClosingCostViewT.SellerResponsibleLenderTypeEstimate:
                    {
                        SellerClosingCostFee defaultSectionAFee = new SellerClosingCostFee()
                        {
                            HudLine = 800,
                            IsTitleFee = false,
                            IsOptional = false,
                            GfeSectionT = E_GfeSectionT.B1,
                            IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionA,
                            IsApr = true,
                            IsFhaAllowable = true,
                            Beneficiary = E_AgentRoleT.Lender,
                            IsThirdParty = false,
                            IsAffiliate = false,
                            CanShop = false
                        };
                        defaultSectionAFee.SetClosingCostSet(this);

                        SellerClosingCostFee defaultSectionBFee = new SellerClosingCostFee()
                        {
                            HudLine = 800,
                            IsTitleFee = false,
                            IsOptional = false,
                            GfeSectionT = E_GfeSectionT.B3,
                            IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionB,
                            IsApr = true,
                            IsFhaAllowable = true,
                            Beneficiary = E_AgentRoleT.Other,
                            IsThirdParty = false,
                            IsAffiliate = false,
                            CanShop = false
                        };
                        defaultSectionBFee.SetClosingCostSet(this);

                        SellerClosingCostFee defaultSectionCFee = new SellerClosingCostFee()
                        {
                            HudLine = 1300,
                            IsTitleFee = false,
                            IsOptional = false,
                            GfeSectionT = E_GfeSectionT.B6,
                            IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionC,
                            IsApr = true,
                            IsFhaAllowable = true,
                            Beneficiary = E_AgentRoleT.Other,
                            IsThirdParty = false,
                            IsAffiliate = false,
                            CanShop = true
                        };
                        defaultSectionCFee.SetClosingCostSet(this);

                        SellerClosingCostFee defaultSectionEFee = new SellerClosingCostFee()
                        {
                            HudLine = 1200,
                            IsTitleFee = false,
                            IsOptional = false,
                            GfeSectionT = E_GfeSectionT.B8,
                            IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionE,
                            IsApr = false,
                            IsFhaAllowable = true,
                            Beneficiary = E_AgentRoleT.Other,
                            IsThirdParty = false,
                            IsAffiliate = false,
                            CanShop = false
                        };
                        defaultSectionEFee.SetClosingCostSet(this);

                        SellerClosingCostFee defaultSectionHFee = new SellerClosingCostFee()
                        {
                            HudLine = 1300,
                            IsTitleFee = false,
                            IsOptional = true,
                            GfeSectionT = E_GfeSectionT.B6,
                            IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionH,
                            IsApr = false,
                            IsFhaAllowable = true,
                            Beneficiary = E_AgentRoleT.Other,
                            IsThirdParty = false,
                            IsAffiliate = false,
                            CanShop = true
                        };
                        defaultSectionHFee.SetClosingCostSet(this);

                        list.Add(new SellerClosingCostFeeSection(viewType, this, null, ClosingCostSetUtils.SectionAOriginationCharges, o => o.CheckIfFeeIsInSection(ClosingCostSetUtils.SectionAFilter), defaultSectionAFee, ClosingCostSetUtils.Start800Hudline, ClosingCostSetUtils.End1300Hudline, E_IntegratedDisclosureSectionT.SectionA));
                        list.Add(new SellerClosingCostFeeSection(viewType, this, null, ClosingCostSetUtils.SectionBServiceYouCannotShopFor, o => o.CheckIfFeeIsInSection(ClosingCostSetUtils.SectionBFilter), defaultSectionBFee, ClosingCostSetUtils.Start800Hudline, ClosingCostSetUtils.End1300Hudline, E_IntegratedDisclosureSectionT.SectionB));
                        list.Add(new SellerClosingCostFeeSection(viewType, this, null, ClosingCostSetUtils.SectionCServiceYouCanShopFor, o => o.CheckIfFeeIsInSection(ClosingCostSetUtils.SectionCFilter), defaultSectionCFee, ClosingCostSetUtils.Start800Hudline, ClosingCostSetUtils.End1300Hudline, E_IntegratedDisclosureSectionT.SectionC));
                        list.Add(new SellerClosingCostFeeSection(viewType, this, null, ClosingCostSetUtils.SectionETaxes, o => o.CheckIfFeeIsInSection(ClosingCostSetUtils.SectionEFilter), defaultSectionEFee, ClosingCostSetUtils.Start800Hudline, ClosingCostSetUtils.End1300Hudline, E_IntegratedDisclosureSectionT.SectionE));
                        list.Add(new SellerClosingCostFeeSection(viewType, this, null, ClosingCostSetUtils.SectionHOther, o => o.CheckIfFeeIsInSection(ClosingCostSetUtils.SectionHFilter), defaultSectionHFee, ClosingCostSetUtils.Start800Hudline, ClosingCostSetUtils.End1300Hudline, E_IntegratedDisclosureSectionT.SectionH));
                    }

                    break;
                case E_ClosingCostViewT.CombinedLenderTypeEstimate:
                case E_ClosingCostViewT.LeaveBlank:
                case E_ClosingCostViewT.LenderTypeEstimate:
                case E_ClosingCostViewT.LenderTypeHud1:
                case E_ClosingCostViewT.LoanClosingCost:
                case E_ClosingCostViewT.LoanHud1:
                default:
                    throw new UnhandledEnumException(viewType);
            }

            return list;
        }

        /// <summary>
        /// Gets the list of fee sections for serialization. This specifically returns a list of seller sections. THIS IS THE ONE YOU WANT FOR SERIALIZATION.
        /// </summary>
        /// <param name="viewType">The view type to build the sections on.</param>
        /// <returns>The list of seller fee sections.</returns>
        public IEnumerable<SellerClosingCostFeeSection> GetViewForSerialization(E_ClosingCostViewT viewType)
        {
            return this.GetViewBase(viewType).Cast<SellerClosingCostFeeSection>();
        }

        /// <summary>
        /// Creates a new blank fee. Creates the set's specific fee type.
        /// </summary>
        /// <returns>Creates a new blank fee.</returns>
        protected override LoanClosingCostFee CreateNewFee()
        {
            return new SellerClosingCostFee();
        }

        /// <summary>
        /// Deserializes the JSON and sets up the underlying fee list. This specifically deserializes to a seller closing cost fee list.
        /// </summary>
        /// <param name="json">The JSON to deserialize.</param>
        protected override void Initialize(string json)
        {
            bool hasOriginatorCompensationFee = false;
            
            if (string.IsNullOrEmpty(json))
            {
                this.ClosingCostFeeList = new List<BaseClosingCostFee>();
            }
            else
            {
                List<SellerClosingCostFee> list = ObsoleteSerializationHelper.JsonDeserializeAndSanitize<List<SellerClosingCostFee>>(json);
                this.ClosingCostFeeList = new List<BaseClosingCostFee>();

                foreach (SellerClosingCostFee o in list)
                {
                    if (!o.IsValidFee())
                    {
                        continue;
                    }

                    o.SetClosingCostSet(this);

                    // Correct the TP and AFF checkboxes
                    var thirdPartyNotUsed = o.IsThirdParty;
                    var affiliateNotUsed = o.IsAffiliate;

                    if (o.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId)
                    {
                        // OPM 214180 - Exclude lender paid originator compensation fee if loan is TRID 2015
                        // Note: fee still added as failsafe if no loan associated.
                        if (this.HasDataLoanAssociate && 
                            this.DataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID &&
                            this.DataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)
                        {
                            continue;
                        }
                        
                        if (this.HasDataLoanAssociate && this.DataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)
                        {
                            o.LegacyGfeFieldT = E_LegacyGfeFieldT.sGfeOriginatorCompF;
                        }
                        else if (this.HasDataLoanAssociate && this.DataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid)
                        {
                            o.LegacyGfeFieldT = E_LegacyGfeFieldT.sGfeOriginatorComp_BorrowerPaid;
                        }

                        if (hasOriginatorCompensationFee == false)
                        {
                            // 4/3/2015 dd - Only add one originator compensation fee.
                            hasOriginatorCompensationFee = true;

                            this.ClosingCostFeeList.Add(o);
                        }
                    }
                    else
                    {
                        this.ClosingCostFeeList.Add(o);
                    }
                }
            }

            this.ClearFeeLookupCache();
        }

        /// <summary>
        /// Creates a new custom seller fee and initializes it.
        /// </summary>
        /// <param name="hudLine">The hudline for the new fee.</param>
        /// <param name="closingCostFeeTypeId">The fee type id.</param>
        /// <param name="section">The section that contains the section template.</param>
        /// <returns>The created seller closing cost fee.</returns>
        protected override LoanClosingCostFee PrepCustomFee(int hudLine, Guid closingCostFeeTypeId, BaseClosingCostFeeSection section)
        {
            SellerClosingCostFee blankFee = new SellerClosingCostFee();
            blankFee.SetClosingCostSet(this);
            blankFee.HudLine = hudLine;
            blankFee.LegacyGfeFieldT = E_LegacyGfeFieldT.Undefined;
            blankFee.ClosingCostFeeTypeId = closingCostFeeTypeId;
            blankFee.OriginalDescription = this.GetCustomFeeOriginalDescription(closingCostFeeTypeId);
            blankFee.IsTitleFee = section.SectionFeeTemplate.IsTitleFee;
            blankFee.IsOptional = section.SectionFeeTemplate.IsOptional;
            blankFee.GfeSectionT = section.SectionFeeTemplate.GfeSectionT;
            blankFee.IntegratedDisclosureSectionT = section.SectionFeeTemplate.IntegratedDisclosureSectionT;
            blankFee.IsApr = section.SectionFeeTemplate.IsApr;
            blankFee.IsFhaAllowable = section.SectionFeeTemplate.IsFhaAllowable;
            blankFee.Beneficiary = section.SectionFeeTemplate.Beneficiary;
            blankFee.IsThirdParty = section.SectionFeeTemplate.IsThirdParty;
            blankFee.IsAffiliate = section.SectionFeeTemplate.IsAffiliate;
            blankFee.CanShop = section.SectionFeeTemplate.CanShop;
            blankFee.SetClosingCostSet(this);

            return blankFee;
        }

        /// <summary>
        /// Gets a list of fee sections in the HUD view.
        /// </summary>
        /// <returns>The list of fee sections in the HUD view.</returns>
        protected override IEnumerable<BaseClosingCostFeeSection> GetHudView()
        {
            return this.GetViewBase(E_ClosingCostViewT.SellerResponsibleLoanHud1);
        }

        /// <summary>
        /// Creates an empty SellerClosingCostSet.
        /// </summary>
        /// <returns>The empty SellerClosingCostSet.</returns>
        protected override BaseClosingCostSet CreateEmptySet()
        {
            return new SellerClosingCostSet(string.Empty);
        }

        /// <summary>
        /// Creates a new empty closing cost set that does not automatically create fees and enumerates through ALL fees.
        /// </summary>
        /// <returns>The empty set.</returns>
        protected override BaseClosingCostSet CreateEmptySetForClearing()
        {
            return new SellerClosingCostSet(string.Empty, true);
        }
    }
}
