﻿// <copyright file="E_ClosingCostFeePaymentPaidByT.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   1/6/2015 12:17:38 AM 
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// An enum for paid by party in closing cost fee payment.
    /// </summary>
    public enum E_ClosingCostFeePaymentPaidByT
    {
        /// <summary>
        /// Leave Blank.
        /// </summary>
        LeaveBlank = 0,
 
        /// <summary>
        /// Borrower Paid.
        /// </summary>
        Borrower = 1,

        /// <summary>
        /// Seller Paid.
        /// </summary>
        Seller = 2,

        /// <summary>
        /// Borrower Finance.
        /// </summary>
        BorrowerFinance = 3,

        /// <summary>
        /// Legacy option to match with previous GFE. Lender Paid.
        /// </summary>
        Lender = 4,

        /// <summary>
        /// Legacy option to match with previous GFE. Broker Paid.
        /// </summary>
        Broker = 5,

        /// <summary>
        /// Other Paid.
        /// </summary>
        Other = 6
    }
}