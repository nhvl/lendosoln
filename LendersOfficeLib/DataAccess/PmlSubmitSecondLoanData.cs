using System;
using System.Collections;
using System.Data;
using DataAccess;

namespace DataAccess
{
	public class CSubmitSecondLoanData : CPmlPageData 
	{
		private static CSelectStatementProvider s_selectProvider;

		/// <summary>
		/// Override access control checking.
		/// </summary>

		protected override bool m_enforceAccessControl
		{
			// 12/14/2004 kb - Skip security checking when we
			// import files.  We need this from now on.

			get
			{
				return false;
			}
		}

		static CSubmitSecondLoanData()
		{
			StringList list = new StringList();
	        
	        #region Target Fields
            DataAccessUtils.AddCertificateFields(list);

            list.Add("sIsLineOfCredit");
            list.Add("sBrokComp1Desc");
            list.Add("sBrokComp1Lckd");
            list.Add("sBrokComp1Pc");
            list.Add("sDueSubmitted");
            list.Add("sFinMethTSubmitted");
            list.Add("sFirstLienProductId");
            list.Add("sLOrigFPcSubmitted");
            list.Add("sNoteIRSubmitted");
            list.Add("sLpeNotesFromBrokerToUnderwriterHistory" );
            list.Add("sPmlCertXmlContent");
            list.Add("sPmlSubmitStatusT");
            list.Add("sRAdjMarginR");
            list.Add("sRAdjMarginRSubmitted");
            list.Add("sStatusLckd");
            list.Add("sSubmitD");
            list.Add("sTermSubmitted");
            list.Add("sTrNotes" );
            list.Add("sfApplyLoanProductTemplate");
            list.Add("sfSpinOffLoanForSubsequent2ndLienLoan");
			list.Add("sQualIR");
			list.Add("sIsQualRateIOnly");
			list.Add("sOptionArmTeaserR");
			list.Add("sFinMethT");
			list.Add("sFinMethDesc");
			list.Add("sQualIRSubmitted");
			list.Add("sOptionArmTeaserRSubmitted");
			list.Add("sIsOptionArmSubmitted");
            list.Add("sSubFinT");

            // 12/16/2013 AV - 145628 Suppress copying the custom app ID when creating combo 2nds LendingQB
            list.Add("sCustomField1Notes");
            list.Add("sCustomField2Notes");
            list.Add("sCustomField3Notes");
            list.Add("sCustomField4Notes");
            list.Add("sCustomField5Notes");
            list.Add("sCustomField6Notes");
            list.Add("sCustomField7Notes");
            list.Add("sCustomField8Notes");
            list.Add("sCustomField9Notes");
            list.Add("sCustomField10Notes");
            list.Add("sCustomField11Notes");
            list.Add("sCustomField12Notes");
            list.Add("sCustomField13Notes");
            list.Add("sCustomField14Notes");
            list.Add("sCustomField15Notes");
            list.Add("sCustomField16Notes");
            list.Add("sCustomField17Notes");
            list.Add("sCustomField18Notes");
            list.Add("sCustomField19Notes");
            list.Add("sCustomField20Notes");

            list.Add("sBranchChannelT");
            list.Add("sCorrespondentProcessT");
            list.Add("sClosingCostFeeVersionT");
            list.Add("sClosingCostSet");
            list.Add("sClosingCostArchiveXmlContent");
            list.Add("sfClear_sGfeInitialDisclosureD");
            list.Add("sLoanEstimateDatesInfo");
            list.Add("sClosingDisclosureDatesInfo");
            list.Add("sTilGfeOd");
            list.Add("sTilGfeDueD");
            list.Add("sTilGfeRd");

            // OPM 232352
            list.Add("sAppSubmittedD");
            list.Add("sAppSubmittedDLckd");
            list.Add("sPreProcessingD");
            list.Add("sDocumentCheckD");
            list.Add("sDocumentCheckFailedD");
            list.Add("sPreUnderwritingD");
            list.Add("sConditionReviewD");
            list.Add("sPreDocQCD");
            list.Add("sDocsOrderedD");
            list.Add("sDocsDrawnD");
            list.Add("sDocsD");
            list.Add("sSubmittedForPurchaseReviewD");
            list.Add("sInPurchaseReviewD");
            list.Add("sPrePurchaseConditionsD");
            list.Add("sSubmittedForFinalPurchaseD");
            list.Add("sSubmittedForFinalPurchaseN");
            list.Add("sInFinalPurchaseReviewD");
            list.Add("sClearToPurchaseD");
            list.Add("sPurchasedD");
            list.Add("sReadyForSaleD");
            list.Add("sSuspendedByInvestorD");
            list.Add("sCondSentToInvestorD");
            list.Add("sAdditionalCondSentD");
            list.Add("sCounterOfferD");
            list.Add("sWithdrawnD");
            list.Add("sArchivedD");
            list.Add("sCaseAssignmentD");
            list.Add("sDisclosuresMailedD");
            list.Add("sPrelimRprtDueD");
            list.Add("sU1DocStatDueD");
            list.Add("sU2DocStatDueD");
            list.Add("sU3DocStatDueD");
            list.Add("sU4DocStatDueD");
            list.Add("sU5DocStatDueD");
            list.Add("sNoteIR");
            list.Add("sTerm");
            list.Add("sQMParR");
            list.Add("sChangeOfCircumstanceXmlContent");
            #endregion

            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

		}        
	        
		public CSubmitSecondLoanData(Guid fileId) : base(fileId, "CSubmitSecondLoanData", s_selectProvider)
		{
		}
	}

	public class CCreateSubfinancingLoanInSwapData : CPageData 
	{
		private static CSelectStatementProvider s_selectProvider;

		/// <summary>
		/// Override access control checking.
		/// </summary>

		protected override bool m_enforceAccessControl
		{
			// 12/14/2004 kb - Skip security checking when we
			// import files.  We need this from now on.

			get
			{
				return false;
			}
		}

		static CCreateSubfinancingLoanInSwapData()
		{
			StringList list = new StringList();
	        
	        #region Target Fields
            DataAccessUtils.AddCertificateFields(list);

            list.Add("sIsLineOfCredit");
            list.Add("sBrokComp1Desc" );
            list.Add("sBrokComp1Lckd" );
            list.Add("sBrokComp1Pc" );
            list.Add("sDueSubmitted" );
            list.Add("sSubFinT");
            list.Add("sFinMethTSubmitted" );
            list.Add("sFirstLienProductId" );
            list.Add("sLOrigFPcSubmitted" );
            list.Add("sLpeNotesFromBrokerToUnderwriterHistory");
            list.Add("sNoteIRSubmitted" );
            list.Add("sPmlCertXmlContent");
            list.Add("sPmlSubmitStatusT" );
            list.Add("sProHazInsMb" );
            list.Add("sProHazInsR" );
            list.Add("sProMInsMb" );
            list.Add("sProMInsR" );
            list.Add("sProRealETxMb" );
            list.Add("sProRealETxR" );
            list.Add("sRAdjMarginR" );
            list.Add("sRAdjMarginRSubmitted" );
            list.Add("sStatusLckd" );
            list.Add("sSubmitD" );
            list.Add("sTermSubmitted" );
            list.Add("sTrNotes");
            list.Add("sfSpinOffLoanForSubsequent2ndLienLoan" );
            list.Add("aProdBCitizenT");
            list.Add("sLpeNotesFromBrokerToUnderwriterHistory");
			list.Add("sQualIR");
			list.Add("sIsQualRateIOnly");
			list.Add("sOptionArmTeaserR");
			list.Add("sFinMethT");
			list.Add("sFinMethDesc");
			list.Add("sQualIRSubmitted");
			list.Add("sOptionArmTeaserRSubmitted");
			list.Add("sIsOptionArmSubmitted");

            // 12/16/2013 AV - 145628 Suppress copying the custom app ID when creating combo 2nds LendingQB
            list.Add("sCustomField1Notes");
            list.Add("sCustomField2Notes");
            list.Add("sCustomField3Notes");
            list.Add("sCustomField4Notes");
            list.Add("sCustomField5Notes");
            list.Add("sCustomField6Notes");
            list.Add("sCustomField7Notes");
            list.Add("sCustomField8Notes");
            list.Add("sCustomField9Notes");
            list.Add("sCustomField10Notes");
            list.Add("sCustomField11Notes");
            list.Add("sCustomField12Notes");
            list.Add("sCustomField13Notes");
            list.Add("sCustomField14Notes");
            list.Add("sCustomField15Notes");
            list.Add("sCustomField16Notes");
            list.Add("sCustomField17Notes");
            list.Add("sCustomField18Notes");
            list.Add("sCustomField19Notes");
            list.Add("sCustomField20Notes");

            list.Add("sBranchChannelT");
            list.Add("sCorrespondentProcessT");
            list.Add("sClosingCostSet");
            list.Add("sClosingCostArchiveXmlContent");
            list.Add("sfClear_sGfeInitialDisclosureD");
            list.Add("sLoanEstimateDatesInfo");
            list.Add("sClosingDisclosureDatesInfo");
            list.Add("sTilGfeOd");
            list.Add("sTilGfeDueD");
            list.Add("sTilGfeRd");

            // OPM 232352
            list.Add("sAppSubmittedD");
            list.Add("sAppSubmittedDLckd");
            list.Add("sPreProcessingD");
            list.Add("sDocumentCheckD");
            list.Add("sDocumentCheckFailedD");
            list.Add("sPreUnderwritingD");
            list.Add("sConditionReviewD");
            list.Add("sPreDocQCD");
            list.Add("sDocsOrderedD");
            list.Add("sDocsDrawnD");
            list.Add("sDocsD");
            list.Add("sSubmittedForPurchaseReviewD");
            list.Add("sInPurchaseReviewD");
            list.Add("sPrePurchaseConditionsD");
            list.Add("sSubmittedForFinalPurchaseD");
            list.Add("sSubmittedForFinalPurchaseN");
            list.Add("sInFinalPurchaseReviewD");
            list.Add("sClearToPurchaseD");
            list.Add("sPurchasedD");
            list.Add("sReadyForSaleD");
            list.Add("sSuspendedByInvestorD");
            list.Add("sCondSentToInvestorD");
            list.Add("sAdditionalCondSentD");
            list.Add("sCounterOfferD");
            list.Add("sWithdrawnD");
            list.Add("sArchivedD");
            list.Add("sCaseAssignmentD");
            list.Add("sDisclosuresMailedD");
            list.Add("sPrelimRprtDueD");
            list.Add("sU1DocStatDueD");
            list.Add("sU2DocStatDueD");
            list.Add("sU3DocStatDueD");
            list.Add("sU4DocStatDueD");
            list.Add("sU5DocStatDueD");
            list.Add("sNoteIR");
            list.Add("sTerm");
            list.Add("sQMParR");
            list.Add("sChangeOfCircumstanceXmlContent");
            #endregion

            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

		}        
	        
		public CCreateSubfinancingLoanInSwapData(Guid fileId) : base(fileId, "CCreateSubfinancingLoanInSwapData", s_selectProvider)
		{
		}
	}
}
