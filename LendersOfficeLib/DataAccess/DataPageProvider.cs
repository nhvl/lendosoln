﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Admin;
using System.Text.RegularExpressions;
using LendersOffice.Constants;
using LendersOffice.ObjLib.PriceGroups;
using LendersOffice.ObjLib.LockPolicies;


namespace DataAccess
{
    public interface IPmlSummaryDataConsumer
    {
        string UsersEmailAddress { set; get; }
        string CorporatesEmailAddress { set; get; }
        string OfficialLoanOfficersEmailAddress { set; get; }
        string OfficialLoanOfficersName { set; get; }
        string OfficialBrokerProcessorsEmailAddress { get; set; }
        string OfficialBrokerProcessorsName { get; set; }
        string OfficialProcessorsEmailAddress { get; set; }
        string OfficialProcessorsName { get; set; }
        string OfficialUnderwritersEmailAddress { get; set; }
        string OfficialUnderwritersName { get; set; }
        string UsersFullName { set; get; }
        string CorpateLabel { get; set; }
        string AccountExecutivesEmailAddress { set; get; }
        string AccountExecutivesName { set; get; }
        string OfficialExternalSecondaryEmailAddress { get; set; }
        string OfficialExternalSecondaryName { get; set; }
        string OfficialExternalPostCloserEmailAddress { get; set; }
        string OfficialExternalPostCloserName { get; set; }
        Guid LoanID { get; }
        Guid UsersEmployeeID { get;  }
        Guid BrokerID { get; } 
        bool IsSendToOther { get; set; }
        string OtherEmail { get; set; }
        bool IsRateReLock { get; }

    }

    public static class DataPageProvider
    {
        private static Regex emailRegex = new Regex(ConstApp.EmailValidationExpression);

        private static Regex nameAndEmail = new Regex(@"(^""(?<name>.*?)""\s<(?<email>" + ConstApp.EmailValidationExpression + ")>$)|^(?<email>" + ConstApp.EmailValidationExpression + ")$", RegexOptions.ExplicitCapture);
        private static bool IsValidEmail(string email)
        {
            return emailRegex.IsMatch(email); 
        }

        private static bool IsValidEmail(string input, out string name, out string email)
        {
            name = "";
            email = "";

            Match match = nameAndEmail.Match(input);
            if (!match.Success)
            {
                return false;
            }

            email = match.Groups["email"].Value;
            if (match.Groups["name"].Success)
            {
                name = match.Groups["name"].Value;
            }
            return true;
        }
        public static void Bind(IPmlSummaryDataConsumer data)
        {                                          
            EmployeeDB  empDb = new EmployeeDB( data.UsersEmployeeID, data.BrokerID );
            BrokerDB brokerdb = BrokerDB.RetrieveById( data.BrokerID );
            CPageData dataLoan = new CPmlSummaryViewEmailData(data.LoanID);
            
            EventNotificationRulesView rView = new EventNotificationRulesView(data.BrokerID); // OPM 8833
            string corporateEmail = rView.GetFromAddressString(dataLoan.sLId);
            string email;
            string name; 
            empDb.Retrieve(); 
            dataLoan.InitLoad();
            CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            data.AccountExecutivesEmailAddress = IsValidEmail(dataLoan.sEmployeeLenderAccExecEmail) ? dataLoan.sEmployeeLenderAccExecEmail : "";
            data.AccountExecutivesName = dataLoan.sEmployeeLenderAccExec.FullName;

            if (data.IsRateReLock)
            {
                if (!Guid.Equals(dataLoan.sPriceGroup.ID, Guid.Empty))
                {
                    var lockPolicyId = dataLoan.sPriceGroup.LockPolicyID;

                    if (lockPolicyId.HasValue && !Guid.Equals(lockPolicyId.Value, Guid.Empty))
                    {
                        LockPolicy policy = LockPolicy.Retrieve(PrincipalFactory.CurrentPrincipal.BrokerId, lockPolicyId.Value);
                        if (policy.IsLockDeskEmailDefaultFromForRateLockConfirmation)
                        {
                            data.IsSendToOther = true;

                            if (!Guid.Equals(policy.DefaultLockDeskIDOrInheritedID, Guid.Empty))
                            {
                                EmployeeDB empDB = new EmployeeDB(policy.DefaultLockDeskIDOrInheritedID, PrincipalFactory.CurrentPrincipal.BrokerId);
                                if (empDB.Retrieve())
                                {
                                    data.OtherEmail = empDB.Email;
                                }
                            }
                        }
                    }
                }
            }

            if (IsValidEmail(corporateEmail,out name, out email))
            {
                data.CorpateLabel = name;
                data.CorporatesEmailAddress = email; 
            }
            data.UsersFullName = empDb.FullName; 
            data.UsersEmailAddress = empDb.Email ;
            data.OfficialLoanOfficersEmailAddress = IsValidEmail(agent.EmailAddr) ? agent.EmailAddr : "";
            data.OfficialLoanOfficersName = agent.AgentName;
            
            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.BrokerProcessor, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            data.OfficialBrokerProcessorsEmailAddress = IsValidEmail(agent.EmailAddr) ? agent.EmailAddr : "";
            data.OfficialBrokerProcessorsName = agent.AgentName;

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Processor, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            data.OfficialProcessorsEmailAddress = IsValidEmail(agent.EmailAddr) ? agent.EmailAddr : "";
            data.OfficialProcessorsName = agent.AgentName;

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Underwriter, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            data.OfficialUnderwritersEmailAddress = IsValidEmail(agent.EmailAddr) ? agent.EmailAddr : "";
            data.OfficialUnderwritersName = agent.AgentName;

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.ExternalSecondary, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            data.OfficialExternalSecondaryEmailAddress = IsValidEmail(agent.EmailAddr) ? agent.EmailAddr : "";
            data.OfficialExternalSecondaryName = agent.AgentName;

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.ExternalPostCloser, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            data.OfficialExternalPostCloserEmailAddress = IsValidEmail(agent.EmailAddr) ? agent.EmailAddr : "";
            data.OfficialExternalPostCloserName = agent.AgentName;
        }
    }
}
