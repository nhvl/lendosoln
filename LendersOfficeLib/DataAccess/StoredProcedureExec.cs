namespace DataAccess
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Text;
    using Adapter;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;

    public class CStoredProcedureExec : IDisposable
    {
        private DbConnection m_conn = null;
        private DbTransaction m_tx = null;
        private DataSrc m_dataSrc;
        private DbConnectionInfo m_connectionInfo = null;

        public String CurrentConnectionState
        {
            // Check the associated connection state.

            get
            {
                // We are really checking the transaction instance to see if it will
                // endure a commit or rollback call.

                if (m_tx != null && m_tx.Connection != null)
                {
                    return m_tx.Connection.State.ToString();
                }

                return "Null";
            }
        }

        public CStoredProcedureExec(Guid brokerId)
        {
            if (brokerId == Guid.Empty)
            {
                throw new ArgumentException("BrokerId cannot be empty.");
            }

            this.Initialize(DataSrc.LOShare, DbConnectionInfo.GetConnectionInfo(brokerId));
        }

        public CStoredProcedureExec(DataSrc dataSrc)
        {
            this.Initialize(dataSrc, null);
        }

        public CStoredProcedureExec(DbConnectionInfo connectionInfo)
        {
            this.Initialize(DataSrc.LOShare, connectionInfo);
        }

        private void Initialize(DataSrc dataSrc, DbConnectionInfo connectionInfo)
        {
            this.m_connectionInfo = connectionInfo;
            this.m_dataSrc = dataSrc;
        }

        private DbConnection CreateConnection()
        {
            DbConnection connection = null;

            if (this.m_connectionInfo != null)
            {
                connection = m_connectionInfo.GetConnection();
            }
            else
            {
                connection = DbAccessUtils.GetConnection(m_dataSrc);
            }

            return connection;
        }

        public string DataSrcDescription
        {
            get
            {
                if (this.m_connectionInfo != null)
                {
                    return this.m_connectionInfo.FriendlyDisplay;
                }
                else
                {
                    return this.m_dataSrc.ToString();
                }
            }
        }

        public void BeginTransactionForWrite()
        {
            this.m_conn = CreateConnection();
            this.m_conn.OpenWithRetry();
            // Changing from Serializable to RepeatableRead 10/15/04
            this.m_tx = m_conn.BeginTransaction(IsolationLevel.RepeatableRead);
        }

        /// <summary>
        /// Use this with care. Make sure BeginTransactionForWrite() is considered first. 
        /// Check with Thinh if you plan to use this for new code.
        /// </summary>
        public void BeginTransactionForWriteSerializable()
        {
            this.m_conn = CreateConnection();
            this.m_conn.OpenWithRetry();
            this.m_tx = m_conn.BeginTransaction(IsolationLevel.Serializable);
        }

        public void CommitTransaction()
        {
            if (null != this.m_tx)
            {
                this.m_tx.Commit();
                this.m_tx = null;
            }
        }

        public void RollbackTransaction()
        {
            if (null != this.m_tx)
            {
                this.m_tx.Rollback();
                this.m_tx = null;
            }
        }

        public int ExecuteNonQuery(string procedureName, int nRetry, bool bSendOnError, params DbParameter[] parameters)
        {
            return this.ExecuteNonQueryWithCustomTimeout(procedureName, nRetry, 0, bSendOnError, parameters);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="procedureName"></param>
        /// <param name="nRetry"></param>
        /// <param name="nTimeoutSeconds">Passing &lt;= 0 to accept defaults which is less than 30 seconds </param>
        /// <param name="bSendOnError"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public int ExecuteNonQueryWithCustomTimeout(string procedureName, int nRetry, int nTimeoutSeconds, bool bSendOnError, IEnumerable<DbParameter> parameters)
        {
            // Initialize new command using the given parameters and
            // our embedded transaction.

            var command = this.m_conn.CreateCommand();
            command.CommandText = procedureName;
            command.Transaction = m_tx;
            command.CommandType = CommandType.StoredProcedure;

            if (nTimeoutSeconds > 0)
            {
                command.CommandTimeout = nTimeoutSeconds; // NOTE: passing 0 means waiting for ever, we don't allow that.
            }

            if (parameters != null)
            {
                foreach (DbParameter p in parameters)
                {
                    command.Parameters.Add(p);
                }
            }

            // We attempt retry + 1 executions to get this command to
            // work.  If we exceed the execution count, we throw the
            // last offending exception for logging.

            try
            {
                int i = 0;

                while (i++ <= nRetry)
                {
                    // Attempt this stored procedure invocation.  On
                    // failure, we pass over the return statement and
                    // log the error and try again.

                    try
                    {
                        return command.ExecuteNonQuery();
                    }
                    catch (DbException exc)
                    {
                        //dont bother retrying this wont work
                        if (exc.Message.Contains("Violation of UNIQUE KEY"))
                        {
                            throw;
                        }

                        Tools.LogWarning
                            (String.Format
                            ("Failed to executing stored procedure {0} , attempt #{1} ({2})."
                            , procedureName
                            , i
                            , exc.Message
                            )
                            , exc
                            );

                        if (i <= nRetry)
                        {
                            Tools.SleepAndWakeupRandomlyWithin(1000, 4500);
                        }
                        else
                        {
                            // We have exceeded our retries.  Log this
                            // error and move on.
                            throw;
                        }
                    }
                }

                // We should have never reached this point -- the
                // exception catch should always be executed, otherwise
                // we would have returned.

                throw new CBaseException
                    (ErrorMessages.Generic,
                    "Failed to execute stored procedure, but didn't catch errors !?!"
                    );
            }
            catch (System.Data.SqlClient.SqlException exc)
            {

                RollbackTransaction();

                StringBuilder sb = new StringBuilder();
                sb.Append(Environment.NewLine);
                sb.Append("<!---- Stored Procedure Debug Info (ExecuteNonQuery) ---->").Append(Environment.NewLine);
                sb.AppendFormat("DataSrc = {0}{1}", DataSrcDescription, Environment.NewLine);
                sb.AppendFormat("Procedure name = {0}{1}", procedureName, Environment.NewLine);
                if (parameters != null)
                {
                    foreach (DbParameter p in parameters)
                    {
                        sb.AppendFormat("  {0} = {1}{2}", p.ParameterName, p.Value, Environment.NewLine);
                    }
                }
                sb.Append("<!----- End Stored Procedure Debug Info ---->").Append(Environment.NewLine);

                CBaseException baseException = new GenericSqlException(sb.ToString(), exc);
                if (bSendOnError)
                    Tools.LogErrorWithCriticalTracking(baseException);
                else
                    Tools.LogError(baseException);

                throw;
            }
        }

        public int ExecuteNonQuery(string procedureName, int nRetry, params DbParameter[] parameters)
        {
            // Delegate to method and assume we don't send on error.
            return this.ExecuteNonQuery(procedureName, nRetry, false, parameters);
        }

        public int ExecuteNonQuery(string procedureName, bool bSendOnError, params DbParameter[] parameters)
        {
            // Delegate to method that retries execution of failure.
            // Zero retries implies one attempt.
            return this.ExecuteNonQuery(procedureName, 0, bSendOnError, parameters);
        }

        public int ExecuteNonQuery(string procedureName, params DbParameter[] parameters)
        {
            // Delegate to method that retries execution of failure.
            // Zero retries implies one attempt.  No emails sent.
            return this.ExecuteNonQuery(procedureName, 0, false, parameters);
        }

        public int ExecuteNonQuery(string procedureName, int nRetry, bool bSendOnError, ArrayList arrayList)
        {
            return this.ExecuteNonQueryWithCustomTimeout(procedureName, nRetry, 0, bSendOnError, arrayList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="procedureName"></param>
        /// <param name="nRetry"></param>
        /// <param name="nTimeoutSeconds">passing <= 0 to accept default which is less than 30 seconds</param>
        /// <param name="bSendOnError"></param>
        /// <param name="arrayList"></param>
        /// <returns></returns>
        public int ExecuteNonQueryWithCustomTimeout(string procedureName, int nRetry, int nTimeoutSeconds, bool bSendOnError, ArrayList arrayList)
        {
            // Create the array on the fly and execute the non-query.
            // Note that a null array list produces a parameter array
            // of size zero.

            DbParameter[] parameters = new DbParameter[arrayList != null ? arrayList.Count : 0];
            for (int i = 0; i < parameters.Length; ++i)
            {
                parameters[i] = arrayList[i] as DbParameter;
            }
            return this.ExecuteNonQueryWithCustomTimeout(procedureName, nRetry, nTimeoutSeconds, bSendOnError, parameters);
        }

        public int ExecuteNonQuery(string procedureName, int nRetry, ArrayList arrayList)
        {
            // Delegate to method and opt out of sending email.

            return this.ExecuteNonQuery(procedureName, nRetry, false, arrayList);
        }

        public int ExecuteNonQuery(string procedureName, ArrayList arrayList)
        {
            // Delegate to method that retries execution of failure.
            // Zero retries implies one attempt.  No emails sent.
            return this.ExecuteNonQuery(procedureName, 0, false, arrayList);
        }

        public DbDataReader ExecuteReader(string procedureName, params DbParameter[] parameters)
        {
            return ExecuteReader(procedureName, (IEnumerable<DbParameter>)parameters);
        }

        public DbDataReader ExecuteReader(string procedureName, IEnumerable<DbParameter> parameters)
        {
            var command = this.m_conn.CreateCommand();
            command.CommandText = procedureName;
            command.Transaction = this.m_tx;

            command.CommandType = CommandType.StoredProcedure;

            if (parameters != null)
            {
                foreach (DbParameter p in parameters)
                {
                    command.Parameters.Add(p);
                }
            }

            try
            {
                var reader = command.ExecuteReader();

                return reader;
            }
            catch (System.Data.SqlClient.SqlException exc)
            {
                this.RollbackTransaction();

                StringBuilder sb = new StringBuilder();
                sb.Append(Environment.NewLine);
                sb.Append("<!---- Sql Text Debug Info (ExecuteReader) ---->").Append(Environment.NewLine);
                sb.AppendFormat("DataSrc = {0}{1}", DataSrcDescription, Environment.NewLine);
                sb.AppendFormat("Procedure = {0}{1}", procedureName, Environment.NewLine);
                sb.Append("<!----- End Sql Text Debug Info ---->").Append(Environment.NewLine);

                CBaseException baseException = new GenericSqlException(sb.ToString(), exc);
                Tools.LogErrorWithCriticalTracking(baseException);

                throw;
            }
        }

        public int Update(DataSet dSet, DbCommand updateCommand, string srcTable)
        {
            // Update a data set using this shared transaction object.

            try
            {
                // Use an adapter to apply the command to our named table.
                var da = DbAccessUtils.GetDbProvider().CreateDataAdapter();

                updateCommand.Connection = m_conn;
                updateCommand.Transaction = m_tx;

                da.UpdateCommand = updateCommand;

                var factory = LqbGrammar.GenericLocator<LqbGrammar.Drivers.ISqlDriverFactory>.Factory;
                var driver = factory.Create(TimeoutInSeconds.Default);

                var tableName = DBTableName.Create(srcTable);
                if (!tableName.HasValue)
                {
                    throw new LqbGrammar.Exceptions.DeveloperException(ErrorMessage.SystemError);
                }

                var modifiedRowCount = driver.UpdateDataSet(da, dSet, tableName);
                return modifiedRowCount.Value;
            }
            catch (DbException exc)
            {
                // Trap error and publish to the log.

                StringBuilder sb = new StringBuilder();

                this.RollbackTransaction();

                sb.Append(Environment.NewLine);
                sb.Append("<!---- Stored Procedure Debug Info (Fill) ---->").Append(Environment.NewLine);
                sb.AppendFormat("DataSrc = {0}{1}", DataSrcDescription, Environment.NewLine);
                sb.AppendFormat("Update text = {0}{1}", updateCommand.CommandText, Environment.NewLine);

                if (updateCommand.Parameters != null)
                {
                    foreach (DbParameter p in updateCommand.Parameters)
                    {
                        sb.AppendFormat("  {0} = {1}{2}", p.ParameterName, p.Value, Environment.NewLine);
                    }
                }

                sb.Append("<!----- End Stored Procedure Debug Info ---->").Append(Environment.NewLine);

                // Construct wrapping exception and send it to the
                // calling code.  We first write it out to the dev
                // team via email.
                CBaseException baseException = new CBaseException(sb.ToString(), exc);
                Tools.LogErrorWithCriticalTracking(baseException);

                throw;
            }
        }

        public int Fill(DataSet dSet, string procedureName, params DbParameter[] parameters)
        {
            return this.Fill(dSet, procedureName, null, parameters);
        }

        public int Fill(DataSet dSet, string procedureName, string tableName, params DbParameter[] parameters)
        {
            // Fill the valid dataset with the result of the stored
            // procedure.  We fill using an adapter given the passed
            // in state of the set (i.e., we don't clear it first).

            try
            {
                // Execute fill using the connection and transaction
                // given.  If connection is not opened, then expect
                // to have it open it and then close it.
                Int32 n = 0;
                DbCommand selectCommand = this.m_conn.CreateCommand();
                selectCommand.CommandText = procedureName;
                selectCommand.CommandType = CommandType.StoredProcedure;
                selectCommand.Transaction = this.m_tx;

                DbDataAdapter da = DbAccessUtils.GetDbProvider().CreateDataAdapter();
                da.SelectCommand = selectCommand;

                if (parameters != null)
                {
                    foreach (DbParameter p in parameters)
                    {
                        da.SelectCommand.Parameters.Add(p);
                    }
                }


                if (!string.IsNullOrWhiteSpace(tableName))
                {
                    n = da.Fill(dSet, tableName);
                }
                else
                {
                    n = da.Fill(dSet);
                }

                return n;
            }
            catch (System.Data.SqlClient.SqlException exc)
            {
                // Trap error and publish to the log.
                StringBuilder sb = new StringBuilder();

                RollbackTransaction();

                sb.Append(Environment.NewLine);
                sb.Append("<!---- Stored Procedure Debug Info (Fill) ---->").Append(Environment.NewLine);
                sb.AppendFormat("DataSrc = {0}{1}", DataSrcDescription, Environment.NewLine);
                sb.AppendFormat("Procedure name = {0}{1}", procedureName, Environment.NewLine);

                if (parameters != null)
                {
                    foreach (DbParameter p in parameters)
                    {
                        sb.AppendFormat("  {0} = {1}{2}", p.ParameterName, p.Value, Environment.NewLine);
                    }
                }

                sb.Append("<!----- End Stored Procedure Debug Info ---->").Append(Environment.NewLine);

                // Construct wrapping exception and send it to the
                // calling code.  We first write it out to the dev
                // team via email.

                CBaseException baseException = new GenericSqlException(sb.ToString(), exc);
                Tools.LogErrorWithCriticalTracking(baseException);
                throw;
            }
        }

        public void Dispose()
        {
            if (null != this.m_conn)
            {
                this.m_conn.Close();
                this.m_conn = null;
            }
        }
    }
}