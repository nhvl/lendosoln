﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess.FeeService;
using System.Xml.Serialization;
using System.Xml;
using DataAccess;
using System.IO;

namespace DataAccess.FeeService
{
    public class FeeServiceConditionSet : IXmlSerializable
    {
        private HashSet<AbstractFeeServiceCondition> Conditions { get; set; }
        private static XmlSerializer x_xmlSerializer = new XmlSerializer(typeof(FeeServiceConditionSet));

        public FeeServiceConditionSet()
        {
            Conditions = new HashSet<AbstractFeeServiceCondition>();
        }

        public FeeServiceConditionSet(IEnumerable<AbstractFeeServiceCondition> conditions)
        {
            Conditions = new HashSet<AbstractFeeServiceCondition>(conditions);
        }

        /// <summary>
        /// Evaluate all of the conditions in the set based on the loan / loan program data.
        /// Note that this will ALWAYS return true if there are no conditions in the set.
        /// </summary>
        /// <param name="data"></param>
        /// <returns>True if all conditions evaluate to true or if there are no conditions. Otherwise, false.</returns>
        public bool Evaluate(FeeServiceMatchingTemplateData data)
        {
            bool allConditionsPass = true;
            foreach (var condition in Conditions)
            {
                allConditionsPass &= condition.Evaluate(data);

                if (!allConditionsPass) break;
            }
            return allConditionsPass;
        }

        public void Add(AbstractFeeServiceCondition condition)
        {
            Conditions.Add(condition);
        }

        public IEnumerable<AbstractFeeServiceCondition> GetAll()
        {
            return Conditions;
        }

        #region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            bool isEmptyElement = reader.IsEmptyElement;
            reader.ReadStartElement("FeeServiceConditionSet");
            if (isEmptyElement)
            {
                return;
            }

            while (reader.NodeType == XmlNodeType.Element && reader.Name == "Condition")
            {
                var conditionType = (E_FeeServiceConditionT)Enum.Parse(typeof(E_FeeServiceConditionT),
                    reader.GetAttribute("ConditionType"));
                AbstractFeeServiceCondition condition;
                switch (conditionType)
                {
                    case E_FeeServiceConditionT.Enumerated:
                        condition = new FeeServiceEnumeratedCondition();
                        condition.ReadXml(reader);
                        break;
                    case E_FeeServiceConditionT.Range:
                        condition = new FeeServiceRangeCondition();
                        condition.ReadXml(reader);
                        break;
                    case E_FeeServiceConditionT.Nested:
                        condition = new FeeServiceNestedCondition();
                        condition.ReadXml(reader);
                        break;
                    default:
                        throw new UnhandledEnumException(conditionType);
                }
                Conditions.Add(condition);
            }
            reader.ReadEndElement();
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            foreach (var condition in Conditions)
            {
                writer.WriteStartElement("Condition");
                condition.WriteXml(writer);
                writer.WriteEndElement();
            }
        }

        #endregion

        public static FeeServiceConditionSet FromXml(string xml)
        {
            if (string.IsNullOrEmpty(xml))
            {
                return new FeeServiceConditionSet();
            }

            FeeServiceConditionSet conditionSet;
            using (var memoryStream = new MemoryStream(ASCIIEncoding.ASCII.GetBytes(xml)))
            {
                memoryStream.Position = 0;
                conditionSet = (FeeServiceConditionSet)x_xmlSerializer.Deserialize(memoryStream);
            }

            return conditionSet;
        }

        public static bool EvaluateSerializedConditions(string serializedConditionSet, FeeServiceMatchingTemplateData data)
        {
            var conditionSet = FeeServiceConditionSet.FromXml(serializedConditionSet);
            return conditionSet.Evaluate(data);
        }

        public override string ToString()
        {
            int i = 0;
            StringBuilder sb = new StringBuilder();

            // Order the conditions by field id such that equivalent condition sets have the same rep.
            foreach (AbstractFeeServiceCondition cond in Conditions.OrderBy(condition => condition.FieldId))
            {
                sb.Append(cond.ToString());
                if (++i != Conditions.Count)
                {
                    sb.Append(" and ");
                }
            }

            return sb.ToString();
        }
    }
}
