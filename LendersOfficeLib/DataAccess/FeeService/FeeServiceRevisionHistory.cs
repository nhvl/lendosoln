﻿// <copyright file="FeeServiceRevisionHistory.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Geoffrey Feltman
//    Date:   09/09/2013 
// </summary>
namespace DataAccess.FeeService
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using System.Web;

    /// <summary>
    /// Used to retrieve emumerations of FeeServiceRevision objects sorted in descending order by date uploaded, such that the first item in the enumeration is the most recently uploaded.
    /// </summary>
    /// <remarks>Meant to be used with UI only. The FeeServiceRevision objects in this object's collections do not contain fee template data.</remarks>
    public class FeeServiceRevisionHistory
    {
        /// <summary>
        /// Stored procedure for retrieving all revisions for listing in the Fee Service UI page.
        /// </summary>
        private static readonly string SpRetrieveAllForUi = "FEE_SERVICE_RetrieveRevisionsForListing";

        /// <summary>
        /// Broker Id.
        /// </summary>
        private Guid brokerId;

        /// <summary>
        /// List of production revisions.
        /// </summary>
        /// <remarks>FeeServiceRevision objects in this collection do not contain fee template data.</remarks>
        private List<FeeServiceRevision> prodRevisions;

        /// <summary>
        /// List of test revisions.
        /// </summary>
        /// <remarks>FeeServiceRevision objects in this collection do not contain fee template data.</remarks>
        private List<FeeServiceRevision> testRevisions;

        /// <summary>
        /// Initializes a new instance of the<see cref="FeeServiceRevisionHistory" /> class.
        /// </summary>
        /// <param name="brokerId">Broker Id.</param>
        private FeeServiceRevisionHistory(Guid brokerId)
        {
            this.brokerId = brokerId;
            this.prodRevisions = new List<FeeServiceRevision>();
            this.testRevisions = new List<FeeServiceRevision>();

            SqlParameter[] parameters =
                {
                    new SqlParameter("@BrokerId", brokerId)
                };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, SpRetrieveAllForUi, parameters))
            {
                while (reader.Read())
                {
                    FeeServiceRevision revision = new FeeServiceRevision(reader, isForListing: true);

                    if (revision.IsTest)
                    {
                        this.testRevisions.Add(revision);
                    }
                    else
                    {
                        this.prodRevisions.Add(revision);
                    }
                }
            }
        }

        /// <summary>
        /// Gets revisions for the broker ordered in descending order of upload date.
        /// </summary>
        /// <value>An enumerable of FeeServiceRevision objects meant for production loans.</value>
        /// <remarks>FeeServiceRevision objects in this collection do not contain fee template data.</remarks>
        public IEnumerable<FeeServiceRevision> FeeServiceRevisions
        {
            get { return this.prodRevisions.Where(rev => !string.IsNullOrEmpty(rev.FileDbKey)).OrderByDescending(rev => rev.UploadedD); }
        }

        /// <summary>
        /// Gets revisions for the broker ordered in descending order of upload date. Includeds Title Vendor Updates (ie. revisions with blank templates).
        /// </summary>
        /// <value>An enumerable of FeeServiceRevision objects meant for production loans. Includeds Title Vendor Updates (ie. revisions with blank templates).</value>
        /// <remarks>FeeServiceRevision objects in this collection do not contain fee template data.</remarks>
        public IEnumerable<FeeServiceRevision> FeeServiceRevisionsIncludingTitleVendorUpdates
        {
            get { return this.prodRevisions.OrderByDescending(rev => rev.UploadedD); }
        }

        /// <summary>
        /// Gets most recently uploaded revision. Returns null if no revisions have been uploaded.
        /// </summary>
        /// <value>The last uploaded FeeServiceRevision for production loans.</value>
        /// <remarks>FeeServiceRevision objects in this collection do not contain fee template data.</remarks>
        public FeeServiceRevision CurrentFeeServiceRevision
        {
            get { return this.prodRevisions.Where(rev => !string.IsNullOrEmpty(rev.FileDbKey)).OrderByDescending(rev => rev.UploadedD).FirstOrDefault(); }
        }

        /// <summary>
        /// Gets test revisions for the broker ordered in descending order of upload date.
        /// </summary>
        /// <remarks>We do not need to filter out Title Vendor Updates here because vendor updates are always marked production.</remarks>
        /// <value>An enumerable of FeeServiceRevision objects meant for test loans.</value>
        /// <remarks>FeeServiceRevision objects in this collection do not contain fee template data.</remarks>
        public IEnumerable<FeeServiceRevision> FeeServiceTestRevisions
        {
            get { return this.testRevisions.OrderByDescending(rev => rev.UploadedD); }
        }

        /// <summary>
        /// Gets most recently uploaded test revision. Returns null if no revisions have been uploaded.
        /// </summary>
        /// <remarks>We do not need to filter out Title Vendor Updates here because vendor updates are always marked production.</remarks>
        /// <value>The last uploaded FeeServiceRevision for test loans.</value>
        /// <remarks>FeeServiceRevision objects in this collection do not contain fee template data.</remarks>
        public FeeServiceRevision CurrentFeeServiceTestRevision
        {
            get { return this.testRevisions.OrderByDescending(rev => rev.UploadedD).FirstOrDefault(); }
        }

        /// <summary>
        /// Load the revision history for a broker.
        /// </summary>
        /// <param name="brokerId">Borker ID.</param>
        /// <returns>FeeServiceRevisionHistory Object.</returns>
        public static FeeServiceRevisionHistory Retrieve(Guid brokerId)
        {
            return new FeeServiceRevisionHistory(brokerId);
        }
    }
}
