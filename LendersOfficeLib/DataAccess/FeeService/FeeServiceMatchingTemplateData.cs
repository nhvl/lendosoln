﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.FeeService
{
    public class FeeServiceMatchingTemplateData : DataAccess.ClosingCostAutomation.CCTemplateSystem.CCcMatchingTemplateData
    {
        public Guid sBrokerId { get; set; }
        public decimal sLAmt { get; set; }
        public decimal sApprVal { get; set; }
        public decimal sPurchPrice { get; set; }
        public decimal sFinalLAmt { get; set; }
        public bool sProdImpound { get; set; }
        public E_sLenderFeeBuyoutRequestedT sLenderFeeBuyoutRequestedT { get; set; }
        public int sLeadSrcId { get; set; }
        public E_sCorrespondentProcessT sCorrespondentProcessT { get; set; }
        public int sPmlCompanyTierId { get; set; }
        public E_sDisclosureRegulationT sDisclosureRegulationT { get; set; }
        public int sSpCountyFips { get; set; }
        public bool sIsNewConstruction { get; set; }
    }
}
