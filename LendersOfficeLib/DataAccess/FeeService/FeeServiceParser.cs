﻿namespace DataAccess.FeeService
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web;
    using ClosedXML.Excel;
    using CommonProjectLib.Common.Lib;
    using DataAccess.ClosingCostAutomation;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Common.TextImport;
    using LendersOffice.ObjLib.TitleProvider;
    using LendersOffice.Rolodex;

    /// <summary>
    /// Class to parse fee service file into individual fee temlpates.
    /// </summary>
    public class FeeServiceParser
    {
        // These are 1-based, as that's what the ClosedXML stuff uses.
        private const int FIELD_VALUE_COLUMN = 1;
        private const int FIELD_ID_COLUMN = 3;
        private const int CONDITION_COLUMN = 6;
        private const int RULE_TYPE_COLUMN = 7;
        private const int ID_COLUMN = 8;
        private const int FEE_PROPERTY_COLUMN = 9;

        private const string ruleTypeColumnAltName = "Is new rule?";

        private static readonly string[] OLD_FEETABLE_COLUMN_NAMES = new string[] {
                                                                        "Fee Service Field Value Setting",
                                                                        "HUD Line Affected",
                                                                        "LQB Field Name",
                                                                        "Valid Value Entry Options",
                                                                        "Description of Field",
                                                                        "Condition"
                                                                    };

        private static readonly string[] NEW_FEETABLE_COLUMN_NAMES = new string[] {
                                                                        "Fee service property value setting",
                                                                        "HUD line affected",
                                                                        "Fee description",
                                                                        "Valid value entry options",
                                                                        "Description of fee property",
                                                                        "Condition",
                                                                        "Rule Type",
                                                                        "Unique identifier",
                                                                        "Fee property"
                                                                    };

        private static Regex x_enumeratedConditionRegex = new Regex(@"^([a-zA-Z0-9_ /]*)=(.*)$");
        private static Regex x_rangeConditionRegex = new Regex(@"^([$a-zA-Z0-9,. ]*)<=([a-zA-Z0-9_ ]*)<=([$a-zA-Z0-9,. ]*)$");

        private ParameterReporting m_parameterReporting;
        private Dictionary<HashSet<AbstractFeeServiceCondition>, FeeServiceTemplate> m_templatesByConditionSet;
        private Dictionary<int, HashSet<AbstractFeeServiceCondition>> m_conditionSetsByRow;
        private Dictionary<string, Tuple<IEnumerable<AbstractFeeServiceCondition>, List<AbstractParsingError>>> m_parsedConditions;
        private Dictionary<string, List<int>> m_parseErrors;
        private HashSet<int> m_rowsWithConditionParsingError;
        private DelimitedListParser m_csvParser;

        public static ReadOnlyCollection<SecurityParameter.EnumMapping> BOOLEAN_ENUM_MAPPING =
            new List<SecurityParameter.EnumMapping>()
            {
                new SecurityParameter.EnumMapping()
                {
                    RepValue = bool.TrueString,
                    FriendlyValue = "Yes"
                },
                new SecurityParameter.EnumMapping()
                {
                    RepValue = bool.FalseString,
                    FriendlyValue = "No"
                }
            }.AsReadOnly();

        private Guid m_brokerId;
        private AvailableAutomationFields m_automationFields;
        private LosConvert m_losConvert;
        public FeeServiceParser(Guid brokerId)
        {
            m_brokerId = brokerId;
            m_automationFields = new AvailableAutomationFields(true, m_brokerId);
            m_parameterReporting = new ParameterReporting(brokerId);
            m_templatesByConditionSet = new Dictionary<HashSet<AbstractFeeServiceCondition>, FeeServiceTemplate>
                (HashSet<AbstractFeeServiceCondition>.CreateSetComparer());
            m_conditionSetsByRow = new Dictionary<int, HashSet<AbstractFeeServiceCondition>>();
            m_parsedConditions = new Dictionary<string, Tuple<IEnumerable<AbstractFeeServiceCondition>, List<AbstractParsingError>>>();
            m_parseErrors = new Dictionary<string, List<int>>();
            m_rowsWithConditionParsingError = new HashSet<int>();
            m_csvParser = new DelimitedListParser();
            m_losConvert = new LosConvert();
        }

        /// <summary>
        /// Parse a .xlsx file into fee templates.
        /// </summary>
        /// <param name="filePath">Path to file.</param>
        /// <param name="parseErrors">Lookup of line numbers grouped by error message.</param>
        /// <returns>Enumerable of fee service templates. Null if there were errors with the .xlsx file format.</returns>
        public IEnumerable<FeeServiceTemplate> ParseFile(string filePath, out Dictionary<string, List<int>> parseErrors)
        {
            // Increase timeout so the thread doesn't abort due to a request timeout.
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Server.ScriptTimeout = 1200; // 20 minutes - Slighty longer than it takes our client with largest file to upload.
            }

            Stopwatch sw = Stopwatch.StartNew();

            int numErrors = 0;
            m_templatesByConditionSet.Clear();
            m_conditionSetsByRow.Clear();
            m_parsedConditions.Clear();
            m_parseErrors.Clear();
            m_rowsWithConditionParsingError.Clear();

            var extension = Path.GetExtension(filePath);
            if (string.IsNullOrEmpty(extension) || !extension.Equals(".xlsx"))
            {
                AddParseError(new InvalidFileExtensionError(), 1);
                parseErrors = m_parseErrors;
                return null;
            }

            AbstractParsingError fileError;
            var rows = ParseExcelFile(filePath, out fileError);
            if (fileError != null)
            {
                AddParseError(fileError, 1);
                parseErrors = m_parseErrors;
                return null;
            }

            // OPM 220727 - Cache Broker Closing cost set and Title Provider list for easy HUD line lookup.
            var broker = BrokerDB.RetrieveById(m_brokerId);
            FeeSetupClosingCostSet cachedBrokerFeeSetup = broker.GetUnlinkedClosingCostSet();
            List<BrokerVendorAssociation> cachedVendorAssociationList = TitleProvider.GetAssociations(m_brokerId);

            var cachedSettlementServiceProvidersById = new Lazy<Dictionary<string, RolodexDB>>(() =>
                RolodexDB.ListRolodexEntriesBySystemIdWithPermissionBypass(this.m_brokerId, restrictToSettlementServiceProviders: true));

            int lineNumber;
            foreach (var row in rows)
            {
                lineNumber = row.LineNumber;

                var fieldId = row.FieldId;
                var fieldValue = row.FieldValue;

                if (string.IsNullOrEmpty(fieldValue))
                    continue;

                var unparsedConditions = row.Condition;
                var parseErrorsForRow = new List<AbstractParsingError>();

                // OPM 198239 - Check if rule is new rule
                FeeServiceRuleType ruleType;
                ruleType = ParseRuleType(row, parseErrorsForRow);

                int hudLine = 0;
                string parsedValue = null;
                Guid parsedTypeId = Guid.Empty;
                E_FeeServiceFeePropertyT parsedFeeProperty = E_FeeServiceFeePropertyT.None;

                // Only validate row values if there were no previous errors.
                // No point trying to validate if we couldn't deturmine rule format.
                if (parseErrorsForRow.Count() == 0)
                {
                    switch (ruleType)
                    {
                        case FeeServiceRuleType.Legacy:
                            if (!AvailableFeeServiceFields.IsValidFieldId(fieldId))
                            {
                                parseErrorsForRow.Add(new InvalidFieldIdError(fieldId));
                            }
                            else if (!AvailableFeeServiceFields.IsValidValueForField(fieldId, fieldValue, out parsedValue))
                            {
                                parseErrorsForRow.Add(new InvalidFieldValueError(fieldId));
                            }

                            parsedTypeId = AvailableFeeServiceFields.GetFeeTypeIdFromFieldId(fieldId);
                            parsedFeeProperty = AvailableFeeServiceFields.GetFeePropertyFromFieldId(fieldId);
                            hudLine = AvailableFeeServiceFields.GetHudLineFromFieldId(fieldId);
                            break;
                        case FeeServiceRuleType.ClosingCost:
                            // Verify TypeId and property value for new rule.
                            if (!AvailableFeeServiceFields.IsValidFeeTypeId(row.Id, cachedBrokerFeeSetup, out parsedTypeId))
                            {
                                parseErrorsForRow.Add(new InvalidFeeTypeIdError(fieldId));
                            }
                            else if (!AvailableFeeServiceFields.IsValidFeeProperty(parsedTypeId, row.FeeProperty, cachedBrokerFeeSetup, out parsedFeeProperty))
                            {
                                parseErrorsForRow.Add(new InvalidFeePropertyError(fieldId, row.FeeProperty));
                            }
                            else if (!AvailableFeeServiceFields.IsValidValueForFeeProperty(parsedFeeProperty, fieldValue, out parsedValue))
                            {
                                parseErrorsForRow.Add(new InvalidFieldValueError(fieldId));
                            }
                            else if (parsedFeeProperty == E_FeeServiceFeePropertyT.SettlementServiceProvider &&
                                !cachedSettlementServiceProvidersById.Value.ContainsKey(parsedValue))
                            {
                                parseErrorsForRow.Add(new InvalidSettlementServiceProviderError());
                            }

                            hudLine = AvailableFeeServiceFields.GetHudLineFromTypeId(parsedTypeId, cachedBrokerFeeSetup);
                            break;
                        case FeeServiceRuleType.TitleSource:
                            // Add error if broker does not allow title source rules
                            if (!broker.EnableEnhancedTitleQuotes)
                            {
                                parseErrorsForRow.Add(new InvalidRuleTypeValueError(row.FieldId));
                                break;
                            }

                            // Verify vendor ID.
                            if (AvailableFeeServiceFields.IsValidTitleVendorId(row.Id, cachedVendorAssociationList))
                            {
                                parsedValue = row.Id.TrimWhitespaceAndBOM();
                            }
                            else
                            {
                                parseErrorsForRow.Add(new InvalidTitleVendorIdError(fieldId)); 
                            }
                            
                            break;
                        default:
                            throw new UnhandledEnumException(ruleType);
                    }
                }

                List<AbstractParsingError> conditionParsingErrors;
                var parsedConditions = ParseConditions(unparsedConditions, out conditionParsingErrors);
                parseErrorsForRow.AddRange(conditionParsingErrors);
                numErrors += parseErrorsForRow.Count;

                foreach (var error in parseErrorsForRow)
                {
                    AddParseError(error, lineNumber);

                    if (error is ConditionParsingError || error is ConditionFieldValueError)
                    {
                        m_rowsWithConditionParsingError.Add(lineNumber);
                    }
                }

                var conditionSet = new HashSet<AbstractFeeServiceCondition>(parsedConditions);
                if (m_templatesByConditionSet.ContainsKey(conditionSet))
                {
                    var templateWithSameConditions = m_templatesByConditionSet[conditionSet];
                    // If adding the field doesn't succeed, then we have already added the field 
                    // for this exact condition set. The condition logic check should catch this
                    // error. We don't need to worry about it here.
                    templateWithSameConditions.AddField(fieldId, parsedValue, ruleType, parsedTypeId, parsedFeeProperty, lineNumber);
                }
                else
                {
                    var template = new FeeServiceTemplate(fieldId, parsedValue, ruleType, parsedTypeId, parsedFeeProperty, lineNumber, conditionSet);
                    m_templatesByConditionSet.Add(conditionSet, template);
                }

                m_conditionSetsByRow.Add(lineNumber, conditionSet);

                if (conditionParsingErrors.Count == 0)
                {
                    foreach (RowInfo rowCompare in rows)
                    {
                        int lineRowCompare = rowCompare.LineNumber;
                        // Don't compare row to itself or unparsed rows.
                        if (lineNumber == lineRowCompare)
                            break;
                        // Don't show overlapping condition errors for rows with 
                        // improperly parsed (and thus skipped) conditions.
                        // Also, do not compare rows that have no field value,
                        // as these will not have any conditions (blank values
                        // are skipped).
                        if (m_rowsWithConditionParsingError.Contains(lineRowCompare) || string.IsNullOrEmpty(rowCompare.FieldValue))
                            continue;

                        bool checkMutualExclusion = false;
                        bool isHudlineConflict = false;

                        FeeServiceRuleType ruleTypeCompare = ParseRuleType(rowCompare, null);

                        // If both rules are old rules or title rules, just check feildIds
                        if ((ruleType == FeeServiceRuleType.Legacy && ruleTypeCompare == FeeServiceRuleType.Legacy)
                                    || (ruleType == FeeServiceRuleType.TitleSource && ruleTypeCompare == FeeServiceRuleType.TitleSource))
                        {
                            if (fieldId == rowCompare.FieldId)
                            {
                                checkMutualExclusion = true;
                            }
                        }
                        // If neither rule is a title rule (both new, one old/one new), check type id, property, and hud line for match.
                        else if (ruleType != FeeServiceRuleType.TitleSource && ruleTypeCompare != FeeServiceRuleType.TitleSource)
                        {
                            // Get parsed values for compare row
                            E_FeeServiceFeePropertyT parsedFeePropertyRowCompare;
                            Guid parsedTypeIdRowCompare;
                            int hudLineRowCompare = 0;

                            if (ruleTypeCompare == FeeServiceRuleType.ClosingCost)
                            {
                                parsedFeePropertyRowCompare = Tools.MapStringToFeeServiceFeePropertyT(rowCompare.FeeProperty);
                                parsedTypeIdRowCompare = m_losConvert.ToGuid(rowCompare.Id);
                                hudLineRowCompare = AvailableFeeServiceFields.GetHudLineFromTypeId(parsedTypeIdRowCompare, cachedBrokerFeeSetup);
                            }
                            else // ruleTypeCompare == legacy
                            {
                                parsedTypeIdRowCompare = AvailableFeeServiceFields.GetFeeTypeIdFromFieldId(rowCompare.FieldId);
                                parsedFeePropertyRowCompare = AvailableFeeServiceFields.GetFeePropertyFromFieldId(rowCompare.FieldId);
                                hudLineRowCompare = AvailableFeeServiceFields.GetHudLineFromFieldId(rowCompare.FieldId);
                            }

                            // Make sure typeIds are valid for both rows
                            if (parsedTypeId != Guid.Empty && parsedTypeIdRowCompare != Guid.Empty)
                            {
                                // Check if both rows have the same type ID
                                if (parsedTypeId == parsedTypeIdRowCompare)
                                {
                                    // Check if property types are valid for both rows
                                    if (parsedFeeProperty != E_FeeServiceFeePropertyT.None && parsedFeePropertyRowCompare != E_FeeServiceFeePropertyT.None)
                                    {
                                        // If both rows have the same property type too then they are the same field
                                        // and need to be checked for mutual exclusivity.
                                        // If either row has the remove property then it conflicts with all other properties
                                        // and must also be checked for mutual exclusivity.
                                        if (parsedFeeProperty == parsedFeePropertyRowCompare ||
                                            parsedFeeProperty == E_FeeServiceFeePropertyT.Remove || parsedFeePropertyRowCompare == E_FeeServiceFeePropertyT.Remove)
                                        {
                                            checkMutualExclusion = true;
                                        }
                                    }
                                }
                                // If type ID doesn't match, check if HUD line numbers match.
                                else if (hudLine != 0 && !ClosingCostSetUtils.HudLinesAllowingMultipleFees.Contains(hudLine) && hudLine == hudLineRowCompare)
                                {
                                    checkMutualExclusion = true;
                                    isHudlineConflict = true;
                                }
                            }
                        }

                        if (checkMutualExclusion && !ConditionsMutuallyExclusive(lineNumber, lineRowCompare))
                        {
                            AbstractParsingError overlapError;
                            if (isHudlineConflict)
                            {
                                overlapError = new OverlappingConditionHudLineError(fieldId);
                            }
                            else
                            {
                                overlapError = new OverlappingConditionError(fieldId);
                            }

                            AddParseError(overlapError, lineRowCompare);
                            AddParseError(overlapError, lineNumber);
                            numErrors += 2;
                        }
                    }
                }
            }

            parseErrors = m_parseErrors;

            sw.Stop();
            Tools.LogInfo("FeeServiceParser.ParseFile. Total rows parsed: " + rows.Count() + ". Num errors:" + numErrors + ". run time: " + sw.Elapsed);
            return m_templatesByConditionSet.Values;
        }

        private FeeServiceRuleType ParseRuleType(RowInfo row, List<AbstractParsingError> parseErrorsForRow)
        {
            FeeServiceRuleType ruleType;
            string TrimmedRuleTypeVal = row.RuleType.TrimWhitespaceAndBOM().ToUpper();
            if (!Enum.TryParse<FeeServiceRuleType>(TrimmedRuleTypeVal, true, out ruleType) || !Enum.IsDefined(typeof(FeeServiceRuleType), ruleType))
            {
                if (string.IsNullOrEmpty(TrimmedRuleTypeVal) || TrimmedRuleTypeVal.Equals("NO", StringComparison.OrdinalIgnoreCase))
                {
                    return FeeServiceRuleType.Legacy;
                }
                else if (TrimmedRuleTypeVal.Equals("YES", StringComparison.OrdinalIgnoreCase))
                {
                    return FeeServiceRuleType.ClosingCost;
                }
                else
                {
                    // NOTE: Adding the parsing error is optional because we don't want to add the same error
                    // multiple times (once when the row is parced and again during the conflict check).
                    if (parseErrorsForRow != null)
                    {
                        parseErrorsForRow.Add(new InvalidRuleTypeValueError(row.FieldId));
                    }

                    // Rule Type is an invalid value. Return legacy by default (it doesn't matter, the file won't be uploaded).
                    return FeeServiceRuleType.Legacy;
                }
            }

            return ruleType;
        }

        private IEnumerable<RowInfo> ParseExcelFile(string filepath, out AbstractParsingError error)
        {
            error = null;
            var rows = new List<RowInfo>();
            var table = new DataTable();

            var workbook = new XLWorkbook(filepath, XLEventTracking.Disabled);
            var worksheet = workbook.Worksheet(1);

            var headerRow = worksheet.FirstRow();
            for (int i = 0; i < NEW_FEETABLE_COLUMN_NAMES.Length; i++)
            {
                var suppliedColumnHeader = headerRow.Cell(i + 1).Value.ToString();

                if ((i < OLD_FEETABLE_COLUMN_NAMES.Length
                        && !suppliedColumnHeader.Equals(OLD_FEETABLE_COLUMN_NAMES[i], StringComparison.OrdinalIgnoreCase)
                        && !suppliedColumnHeader.Equals(NEW_FEETABLE_COLUMN_NAMES[i], StringComparison.OrdinalIgnoreCase))
                    || (i >= OLD_FEETABLE_COLUMN_NAMES.Length
                        && !suppliedColumnHeader.Equals("", StringComparison.OrdinalIgnoreCase)
                        && !suppliedColumnHeader.Equals(NEW_FEETABLE_COLUMN_NAMES[i], StringComparison.OrdinalIgnoreCase)
                        && (i != RULE_TYPE_COLUMN - 1 || !suppliedColumnHeader.Equals(ruleTypeColumnAltName, StringComparison.OrdinalIgnoreCase))))
                {
                    error = new InvalidFileHeaderError(NEW_FEETABLE_COLUMN_NAMES);
                    return null;
                }
            }

            var lastRowUsed = worksheet.LastCellUsed().Address.RowNumber;
            var bodyRow = headerRow.RowBelow();
            while (bodyRow.FirstCell().Address.RowNumber <= lastRowUsed)
            {
                var fieldId = bodyRow.Cell(FIELD_ID_COLUMN).GetString();

                // If the number is formatted as a percent, the value will be
                // its actual base 10 value. eg 5% => .05. This is not the way
                // we expect rates to be represented in our system, so multiply
                // percent values by 100. 
                // Always using GetFormattedString() may do away with decimal
                // places that were entered but are not in the formatted value
                // displayed in Excel.
                // If they entered a % without formatting it as a percentage in
                // excel, then the value will be parsed as we expect.
                // TODO: look into more correct way of doing this.
                var valueCell = bodyRow.Cell(FIELD_VALUE_COLUMN);
                var isFormattedPercent = false;
                if (valueCell.DataType == XLCellValues.Number)
                {
                    // These formatters indicate that the value displayed in Excel
                    // will be the actual value multiplied by 100.
                    var numberFormat = valueCell.Style.NumberFormat;
                    var isPredefinedPercentFormat = numberFormat.NumberFormatId == 9
                        || numberFormat.NumberFormatId == 10;
                    var isCustomPercentFormat = !string.IsNullOrEmpty(numberFormat.Format)
                        && (numberFormat.Format.IndexOf("p", StringComparison.OrdinalIgnoreCase) >= 0 || numberFormat.Format.Contains("%"));
                    isFormattedPercent = isPredefinedPercentFormat || isCustomPercentFormat;
                }
                var value = isFormattedPercent ? (Convert.ToDecimal(valueCell.GetDouble()) * 100).ToString() : valueCell.GetString();

                var condition = bodyRow.Cell(CONDITION_COLUMN).GetString();
                var ruleType = bodyRow.Cell(RULE_TYPE_COLUMN).GetString();
                var id = bodyRow.Cell(ID_COLUMN).GetString();
                var feeProperty = bodyRow.Cell(FEE_PROPERTY_COLUMN).GetString();

                rows.Add(new RowInfo(fieldId, value, condition, ruleType, id, feeProperty, bodyRow.FirstCell().Address.RowNumber));
                bodyRow = bodyRow.RowBelow();
            }

            return rows;
        }

        private void AddParseError(AbstractParsingError error, int lineNumber)
        {
            List<int> lineNumbers;
            string errorString = error.ToString();
            if (m_parseErrors.TryGetValue(errorString, out lineNumbers))
            {
                lineNumbers.Add(lineNumber);
            }
            else
            {
                m_parseErrors.Add(errorString, new List<int>() { lineNumber });
            }
        }

        /// <summary>
        /// Finds if two rows have mutually exclusive condition sets.
        /// Assumes m_conditionSetsByRow has been created for these rows.
        /// </summary>
        /// <exception cref="System.Collections.Generic.KeyNotFoundException"><see cref="m_conditionSetsByRow"/> did not contain one of the specified rows.</exception>
        private bool ConditionsMutuallyExclusive(int lineNum1, int lineNum2)
        {
            // je - 10/08/2015 - OPM 228220 - reduce complexity to O(n+m) instead of O(n*m)
            HashSet<AbstractFeeServiceCondition> conditionSet1 = m_conditionSetsByRow[lineNum1];
            Dictionary<string, AbstractFeeServiceCondition> conditionSet2ByFieldId = m_conditionSetsByRow[lineNum2].ToDictionary(k => k.FieldId);   // O(m_conditionSetsByRow[lineNum2].Count)

            foreach (AbstractFeeServiceCondition cond1 in conditionSet1)   // O(m_conditionSetsByRow[lineNum1].Count)
            {
                AbstractFeeServiceCondition cond2;
                if (conditionSet2ByFieldId.TryGetValue(cond1.FieldId, out cond2) && AreMutuallyExclusive(cond1, cond2))
                {
                        return true; //This condition gives witness to the condition sets being mutually exclusive
                }
            }
            return false; //all the overlapping condition fields also had overlapping condition values
        }

        /// <summary>
        /// Checks if two AbstractFeeServiceCondition objects are mutually exclusive.
        /// </summary>
        /// <exception cref="System.InvalidCastException">The type of <param name="condition1"/> is not equal to the type of <param name="condition2"/>.</exception>
        private bool AreMutuallyExclusive(AbstractFeeServiceCondition condition1, AbstractFeeServiceCondition condition2)
        {
            if (condition1.GetType() == typeof(FeeServiceEnumeratedCondition))
            {
                var item1 = (FeeServiceEnumeratedCondition)condition1;
                var item2 = (FeeServiceEnumeratedCondition)condition2;

                // OPM 228220 - Values are HastSets, so make use of Contains() rather than looping through both sets. O(item1.Values.Count)
                foreach (string value1 in item1.Values)
                {
                    if (item2.Values.Contains(value1))   // O(1)
                    {
                        return false;
                    }
                }

                return true;
            }
            else if (condition1.GetType() == typeof(FeeServiceRangeCondition))
            {
                var item1 = (FeeServiceRangeCondition)condition1;
                var item2 = (FeeServiceRangeCondition)condition2;

                return item1.LowerBound > item2.UpperBound || item1.UpperBound < item2.LowerBound;
            }
            else // if (condition1.GetType() == typeof(FeeServiceNestedCondition))
            {
                var item1 = (FeeServiceNestedCondition)condition1;
                var item2 = (FeeServiceNestedCondition)condition2;

                // OPM 449923 - Last minute hack fix. All region rules in the same file must use the same
                // region set. We cannot guarantee mutual exclusion otherwise (because sets are user edited).
                if (item1.FieldId == "csRegion"
                    && item1.NestedConditions.Keys.First() != item2.NestedConditions.Keys.First())
                {
                    return false;
                }

                // OPM 449923 - Check that nested conditions with matching keys are mutually exclusive.
                foreach (var kvp1 in item1.NestedConditions)
                {
                    AbstractFeeServiceCondition cond2;
                    if (item2.NestedConditions.TryGetValue(kvp1.Key, out cond2) && !AreMutuallyExclusive(kvp1.Value, cond2)) 
                    {
                        return false;
                    }
                }

                return true;
            }
        }

        private bool ValidFieldId(string fieldId)
        {
            return true;
        }

        private bool ValidFieldValue(string fieldId, string fieldValue)
        {
            return true;
        }

        /// <summary>
        /// Parse condition string into AbstractFeeServiceConditions.
        /// </summary>
        /// <remarks>
        /// If the conditions string is empty or null, it will return an 
        /// empty enumerable and will not consider this an error.
        /// </remarks>
        /// <param name="unparsedConditions">The condtition string.</param>
        /// <param name="errors">Errors in the condition string.</param>
        /// <returns>Enumerable of AbstractFeeServiceConditions.</returns>
        private IEnumerable<AbstractFeeServiceCondition> ParseConditions(string unparsedConditions,
            out List<AbstractParsingError> errors)
        {
            // OPM 228220 - Take advantage of the fact that condition strings are repeated often.
            if (!m_parsedConditions.ContainsKey(unparsedConditions))
            {
                List<AbstractParsingError> err = null;
                var parsedConditions = ParseConditionsImpl(unparsedConditions, out err);
                m_parsedConditions.Add(unparsedConditions, new Tuple<IEnumerable<AbstractFeeServiceCondition>,List<AbstractParsingError>>(parsedConditions, err));
            }

            var cache = m_parsedConditions[unparsedConditions];
            errors = cache.Item2;

            return cache.Item1;
        }

        /// <summary>
        /// Actual implementation of ParseConditions
        /// </summary>
        /// <param name="unparsedConditions">The condtition string.</param>
        /// <param name="errors">Errors in the condition string.</param>
        /// <returns>Enumerable of AbstractFeeServiceConditions.</returns>
        private IEnumerable<AbstractFeeServiceCondition> ParseConditionsImpl(string unparsedConditions,
            out List<AbstractParsingError> errors)
        {
            errors = new List<AbstractParsingError>();
            var parsedConditions = new List<AbstractFeeServiceCondition>();

            if (string.IsNullOrEmpty(unparsedConditions)) return parsedConditions;

            // OPM 233918 - Cannot split string on " and " alone as condition values may contain the word "and".
            // Remove open paren of 1st condition and close paren of last condition, and split on ") and (".
            if (unparsedConditions[0] != '(' || unparsedConditions[unparsedConditions.Length - 1] != ')')
            {
                errors.Add(new ConditionParsingError(E_ConditionParseErrorT.InvalidFormat));
                return parsedConditions;
            }

            var splitConditions = unparsedConditions.Substring(1, unparsedConditions.Length - 2).Split(new string[] { ") and (" }, StringSplitOptions.None);

            foreach (var unparsedCondition in splitConditions.Select(condition => condition.TrimWhitespaceAndBOM()))
            {
                AbstractParsingError error = null;
                AbstractFeeServiceCondition parsedCondition = null;

                Match match = x_enumeratedConditionRegex.Match(unparsedCondition);
                if (match.Success)
                {
                    parsedCondition = ParseEnumeratedCondition(match, out error);
                }
                else if (x_rangeConditionRegex.IsMatch(unparsedCondition))
                {
                    match = x_rangeConditionRegex.Match(unparsedCondition);
                    if (match.Success)
                    {
                        parsedCondition = ParseRangeCondition(match, out error);
                    }
                }
                else
                {
                    error = new ConditionParsingError(E_ConditionParseErrorT.InvalidFormat);
                }

                if (parsedCondition != null)
                {
                    var isConditionFieldAlreadyUsed = parsedConditions
                        .Any(condition => condition.FieldId == parsedCondition.FieldId);
                    if (isConditionFieldAlreadyUsed)
                    {
                        errors.Add(new ConditionParsingError(E_ConditionParseErrorT.InvalidFormat));
                    }
                    else
                    {
                        parsedConditions.Add(parsedCondition);
                    }
                }
                else
                {
                    if (error == null)
                    {
                        throw new CBaseException(ErrorMessages.Generic,
                            "No error provided for unparsed condition.");
                    }
                    errors.Add(error);
                }
            }

            return parsedConditions;
        }

        private AbstractFeeServiceCondition ParseRangeCondition(Match match, out AbstractParsingError error)
        {
            error = null;

            var lowerBoundRep = match.Groups[1].Value.TrimWhitespaceAndBOM().Replace("$", "").Replace(",", "");
            var upperBoundRep = match.Groups[3].Value.TrimWhitespaceAndBOM().Replace("$", "").Replace(",", "");
            var friendlyName = match.Groups[2].Value.TrimWhitespaceAndBOM();

            var field = m_automationFields.GetAutomationFieldByName(friendlyName);
            if (field == null)
            {
                error = new ConditionParsingError(E_ConditionParseErrorT.InvalidFieldName);
                return null;
            }
            else if (field.Type != AutomationFieldType.Range)
            {
                error = new ConditionParsingError(E_ConditionParseErrorT.UnexpectedConditionType);
                return null;
            }

            decimal lowerBound;
            decimal upperBound;
            var validLowerBound = decimal.TryParse(lowerBoundRep, out lowerBound);
            var validUpperBound = decimal.TryParse(upperBoundRep, out upperBound);
            if (!validLowerBound || !validUpperBound)
            {
                var invalidValues = new List<string>();
                if (!validLowerBound) invalidValues.Add(match.Groups[1].Value.TrimWhitespaceAndBOM());
                if (!validUpperBound) invalidValues.Add(match.Groups[3].Value.TrimWhitespaceAndBOM());
                error = new ConditionFieldValueError(field.Name, invalidValues);
                return null;
            }
            else if (upperBound < lowerBound)
            {
                error = new ConditionParsingError(E_ConditionParseErrorT.InvalidRange);
                return null;
            }

            return new FeeServiceRangeCondition(field.FieldId, lowerBound, upperBound);
        }

        private AbstractFeeServiceCondition ParseEnumeratedCondition(Match match, out AbstractParsingError error)
        {
            error = null;

            var friendlyName = match.Groups[1].Value.TrimWhitespaceAndBOM();
            var friendlyValues = match.Groups[2].Value.TrimWhitespaceAndBOM();

            var field = m_automationFields.GetAutomationFieldByName(friendlyName);
            if (field == null)
            {
                error = new ConditionParsingError(E_ConditionParseErrorT.InvalidFieldName);
                return null;
            }
            else if (field.Type != AutomationFieldType.Enumeration
                && field.Type != AutomationFieldType.BrokerDefinedEnumeration
                && field.Type != AutomationFieldType.NestedEnumeration)
            {
                error = new ConditionParsingError(E_ConditionParseErrorT.UnexpectedConditionType);
                return null;
            }

            string setId = string.Empty;
            IList<SecurityParameter.EnumMapping> enumMapForField;
            var securityParameter = m_parameterReporting[field.FieldId];
            if (securityParameter is SecurityParameter.NestedEnumeratedParmeter)
            {
                // Parse setname from values.
                int i = friendlyValues.IndexOf(':');
                if (i == -1)
                {
                    error = new ConditionParsingError(E_ConditionParseErrorT.InvalidFormat);
                    return null;
                }

                string setName = friendlyValues.Substring(0, i).TrimWhitespaceAndBOM();
                friendlyValues = friendlyValues.Substring(i + 1).TrimWhitespaceAndBOM();

                if (string.IsNullOrWhiteSpace(setName) || string.IsNullOrWhiteSpace(friendlyValues))
                {
                    error = new ConditionParsingError(E_ConditionParseErrorT.InvalidFormat);
                    return null;
                }

                // Get set ID.
                SecurityParameter.NestedEnumeratedParmeter param = (SecurityParameter.NestedEnumeratedParmeter)securityParameter;
                setId = param.EnumMapping.FirstOrDefault(em => em.FriendlyValue == setName)?.RepValue;

                if (string.IsNullOrWhiteSpace(setId))
                {
                    error = new ConditionFieldValueError(field.Name, new string[] { setName });
                    return null;
                }

                // Retrieve enumerations from subset.
                SecurityParameter.EnumeratedParmeter nestedParam = param.SubParameters.FirstOrDefault(p => p.Id == setId);
                if (nestedParam == null)
                {
                    error = new ConditionFieldValueError(field.Name, new string[] { setName });
                    return null;
                }

                enumMapForField = nestedParam.EnumMapping;
            }
            else if (securityParameter is SecurityParameter.EnumeratedParmeter)
            {
                enumMapForField = ((SecurityParameter.EnumeratedParmeter)securityParameter).EnumMapping;
            }
            else if (securityParameter.Type == SecurityParameter.SecurityParameterType.Boolean)
            {
                enumMapForField = BOOLEAN_ENUM_MAPPING;
            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, "Unhandled automation field type.");
            }

            var values = new List<string>();
            foreach (string value in m_csvParser.Eat(friendlyValues))
            {
                if (field.Type == AutomationFieldType.BrokerDefinedEnumeration)
                {
                    // Since the friendly value of broker defined enumerations can
                    // change, we only want to parse the ID. The friendly value is only
                    // present in the file for the benefit of the uploading user.
                    // We expect the format {Friendly Value} : {ID}
                    var indexOfLastColon = value.LastIndexOf(':');
                    if (indexOfLastColon != -1 && value.Length > indexOfLastColon + 1)
                    {
                        values.Add(value.Substring(indexOfLastColon + 1).TrimWhitespaceAndBOM());
                    }
                    else
                    {
                        // This will trigger invalid value error below.
                        values.Add(value);
                    }
                }
                else
                {
                    values.Add(value);
                }
            }

            Dictionary<string, SecurityParameter.EnumMapping> enumMapDict;
            if (field.Type == AutomationFieldType.BrokerDefinedEnumeration)
            {
                enumMapDict = enumMapForField.ToDictionary(enumMap => enumMap.RepValue);
            }
            else
            {
                enumMapDict = enumMapForField.ToDictionary(enumMap => enumMap.FriendlyValue);
            }

            var invalidValues = values.Where(value => !enumMapDict.ContainsKey(value));
            if (invalidValues.Any())
            {
                error = new ConditionFieldValueError(field.Name, invalidValues);
                return null;
            }

            var validSystemValues = values.Select(value => enumMapDict[value].RepValue);

            FeeServiceEnumeratedCondition enumCond = new FeeServiceEnumeratedCondition(field.FieldId, new HashSet<string>(validSystemValues));
            if (securityParameter is SecurityParameter.NestedEnumeratedParmeter)
            {
                Dictionary<string, AbstractFeeServiceCondition> condMap = new Dictionary<string, AbstractFeeServiceCondition>();

                condMap.Add(setId, enumCond);

                return new FeeServiceNestedCondition(field.FieldId, condMap);
            }

            return enumCond;
        }

        private struct RowInfo
        {
            public readonly string FieldId;
            public readonly string FieldValue;
            public readonly string Condition;
            public readonly int LineNumber;

            // OPM 198239 - Update Fee Service for CFPB
            public readonly string RuleType;
            public readonly string Id;
            public readonly string FeeProperty;

            public RowInfo(string fieldId, string value, string condition, string ruleType, string id, string feeProperty, int lineNumber)
            {
                FieldId = fieldId;
                FieldValue = value;
                Condition = condition;
                LineNumber = lineNumber;
                RuleType = ruleType;
                Id = id;
                FeeProperty = feeProperty;
            }
        }

    }

    public enum E_ConditionParseErrorT
    {
        InvalidFieldName,
        InvalidFieldValue,
        InvalidRange,
        InvalidFormat,
        UnexpectedConditionType
    }

    public abstract class AbstractParsingError
    {
        public abstract override string ToString();
    }

    public class InvalidFileExtensionError : AbstractParsingError
    {
        public override string ToString()
        {
            return "Unexpected file format. Please upload a .xlsx file.";
        }
    }

    public class InvalidFileHeaderError : AbstractParsingError
    {
        private string[] ExpectedHeaders;
        public InvalidFileHeaderError(string[] expectedHeaders)
        {
            ExpectedHeaders = expectedHeaders;
        }
        public override string ToString()
        {
            return string.Format("Unexpected header format. Please include the "
                + "following columns as a header: {0}.", string.Join(", ", ExpectedHeaders));
        }
    }

    public class InvalidFieldIdError : AbstractParsingError
    {
        private string FieldId { get; set; }
        public InvalidFieldIdError(string fieldId)
        {
            FieldId = fieldId;
        }

        public override string ToString()
        {
            return string.Format("{0} is non-existent field. Please use the "
                + "condition generator to obtain a list of fields and attached "
                + "conditions for each HUD line.", FieldId);
        }
    }

    public class InvalidFieldValueError : AbstractParsingError
    {
        private string FieldId { get; set; }
        public InvalidFieldValueError(string fieldId)
        {
            FieldId = fieldId;
        }

        public override string ToString()
        {
            return string.Format("Invalid field value setting for {0}.", FieldId);
        }
    }

    public class InvalidSettlementServiceProviderError: AbstractParsingError
    {
        public override string ToString()
        {
            return "System ID for the selected settlement service provider is not valid. Please update the system ID for the desired settlement service provider.";
        }
    }

    public class InvalidIsNewRuleValueError : AbstractParsingError
    {
        private string FieldId { get; set; }
        public InvalidIsNewRuleValueError(string fieldId)
        {
            FieldId = fieldId;
        }

        public override string ToString()
        {
            return string.Format("Invalid value for Is New Rule? column for {0}. Value must be Yes, No, or blank.", FieldId);
        }
    }

    public class InvalidRuleTypeValueError : AbstractParsingError
    {
        private string FieldId { get; set; }
        public InvalidRuleTypeValueError(string fieldId)
        {
            FieldId = fieldId;
        }

        public override string ToString()
        {
            return string.Format("Invalid value for Rule Type column for {0}. Value must be Legacy, ClosingCost, or TitleSource.", FieldId);
        }
    }

    public class InvalidFeeTypeIdError : AbstractParsingError
    {
        private string FieldId { get; set; }
        public InvalidFeeTypeIdError(string fieldId)
        {
            FieldId = fieldId;
        }

        public override string ToString()
        {
            return string.Format("The unique identifier provided for the field {0} does not match the ID of any known fee type. Please use the "
                + "condition generator to obtain a list of fees and attached conditions for each HUD line.", FieldId);
        }
    }

    public class InvalidTitleVendorIdError : AbstractParsingError
    {
        private string FieldId { get; set; }
        public InvalidTitleVendorIdError(string fieldId)
        {
            FieldId = fieldId;
        }

        public override string ToString()
        {
            return string.Format("The unique identifier provided for the field {0} does not match the ID of any known title vendor. Please use the "
                + "condition generator to obtain a list of vendors and attached conditions.", FieldId);
        }
    }

    public class InvalidFeePropertyError : AbstractParsingError
    {
        private string FieldId { get; set; }
        private string FeeProperty{ get; set; }
        public InvalidFeePropertyError(string fieldId, string feeProperty)
        {
            FieldId = fieldId;
            FeeProperty = feeProperty;
        }

        public override string ToString()
        {
            return string.Format("{0} is a non-existent fee property for the fee {1}. Please use the "
                + "condition generator to obtain a list of fields and attached "
                + "conditions for each HUD line.", FeeProperty, FieldId);
        }
    }

    public class OverlappingConditionError : AbstractParsingError
    {
        private string FieldId { get; set; }
        public OverlappingConditionError(string fieldId)
        {
            FieldId = fieldId;
        }
        public override string ToString()
        {
            return string.Format("Overlapping condition setting detected for {0}. "
                + "Each LendingQB field setting must be mutually exclusive.", FieldId);
        }
    }

    public class OverlappingConditionHudLineError : AbstractParsingError
    {
        private string FieldId { get; set; }
        public OverlappingConditionHudLineError(string fieldId)
        {
            FieldId = fieldId;
        }
        public override string ToString()
        {
            return string.Format("Overlapping condition setting detected for {0}. "
                + "Each fee HUD line must be mutually exclusive.", FieldId);
        }
    }

    public class ConditionFieldValueError : AbstractParsingError
    {
        private string FieldId { get; set; }
        private IEnumerable<string> InvalidValues;
        private const string USE_GENERATOR = "Please use the condition generator.";

        public ConditionFieldValueError(string fieldId, IEnumerable<string> invalidValues)
        {
            FieldId = fieldId;
            InvalidValues = invalidValues;
        }

        public override string ToString()
        {
            var multiError = InvalidValues.Count() > 1;
            return string.Format("Invalid {0} encountered in condition. Field: {1}. {2}: {3}. {4}",
                multiError ? "values" : "value", FieldId, multiError ? "Values" : "Value", 
                string.Join(", ", InvalidValues.ToArray()), USE_GENERATOR);
        }
    }

    // If we want to provide more detailed error information,
    // this should probably be divided up into several classes.
    public class ConditionParsingError : AbstractParsingError
    {
        private E_ConditionParseErrorT ErrorType { get; set; }
        private const string USE_GENERATOR = "Please use the condition generator.";

        public ConditionParsingError(E_ConditionParseErrorT errorType)
        {
            ErrorType = errorType;
        }

        public override string ToString()
        {
            switch (ErrorType)
            {
                case E_ConditionParseErrorT.InvalidFormat:
                case E_ConditionParseErrorT.UnexpectedConditionType:
                    return "Condition is in unreadable format. " + USE_GENERATOR;
                case E_ConditionParseErrorT.InvalidFieldName:
                    return "Condition uses unknown field. " + USE_GENERATOR;
                case E_ConditionParseErrorT.InvalidRange:
                    return "Invalid range encountered in condition. " + USE_GENERATOR;
                case E_ConditionParseErrorT.InvalidFieldValue:
                    return "Invalid field value encountered in condition. " + USE_GENERATOR;
                default:
                    throw new UnhandledEnumException(ErrorType);
            }
        }
    }
}
