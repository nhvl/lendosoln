﻿namespace DataAccess.FeeService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;
    using System.Xml.Schema;

    using CommonProjectLib.Caching;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.Regions;

    /// <summary>
    /// Condition where a field values is required to be in a nested subset of a set of values.
    /// </summary>
    /// <remarks>
    /// Due to time constraints I did not flesh out the FeeServiceParser enough to make full use of this
    /// class, beyond what was necessary for OPM 449923. This means that at the moment the the parser will
    /// only create FeeServiceNestedCondition Objects with only 1 item selected in the root set and with
    /// only one nested layer (ie. one selected RegionSet with multiple selected regions). - je - 05/23/17.
    /// </remarks>
    public class FeeServiceNestedCondition : AbstractFeeServiceCondition
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FeeServiceNestedCondition" /> class.
        /// </summary>
        public FeeServiceNestedCondition()
        {
            this.FieldId = string.Empty;
            this.NestedConditions = new Dictionary<string, AbstractFeeServiceCondition>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FeeServiceNestedCondition" /> class, instatiating it's members with
        /// the given values.
        /// </summary>
        /// <param name="fieldId">Condition Field ID.</param>
        /// <param name="conditions">Dictionary of nested conditions.</param>
        public FeeServiceNestedCondition(string fieldId, Dictionary<string, AbstractFeeServiceCondition> conditions)
        {
            this.FieldId = fieldId;
            this.NestedConditions = conditions;
        }

        /// <summary>
        /// Gets a dictionary of nested condition.
        /// </summary>
        /// <value>A dictionary of nested condition.</value>
        public Dictionary<string, AbstractFeeServiceCondition> NestedConditions { get; private set; }

        /// <summary>
        /// Evaluate this condition to see if it's true.
        /// </summary>
        /// <param name="data">Fee service template matching data.</param>
        /// <returns>True, if at least one of the nested conditions returns true.</returns>
        public override bool Evaluate(FeeServiceMatchingTemplateData data)
        {
            switch (this.FieldId)
            {
                case "csRegion":
                    string cacheKey = $"AllRegionSets_{data.sBrokerId}";
                    var regionSets = MemoryCacheUtilities.GetOrAddExisting(
                        cacheKey,
                        () => RegionSet.GetAllRegionSets(data.sBrokerId),
                        DateTimeOffset.Now.AddSeconds(ConstStage.RegionSetCacheSeconds));

                    foreach (string key in this.NestedConditions.Keys)
                    {
                        int setKey = int.Parse(key);
                        RegionSet regionSet = regionSets[setKey];

                        FeeServiceEnumeratedCondition nestedCond = (FeeServiceEnumeratedCondition)this.NestedConditions[key];
                        foreach (string value in nestedCond.Values)
                        {
                            int regionKey = int.Parse(value);
                            if (regionSet.Regions[regionKey].FipsCountyCodes.Contains(data.sSpCountyFips))
                            {
                                return true;
                            }
                        }
                    }

                    return false;
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Unhandled field id=[" + FieldId + "] in fee service condition.");
            }
        }

        /// <summary>
        /// Checks if this object is equal to another object.
        /// </summary>
        /// <param name="obj">The object to compare againse.</param>
        /// <returns>True, if the two objects are equal. False otherwise.</returns>
        public override bool Equals(object obj)
        {
            // Null check.
            if (obj == null)
            {
                return false;
            }

            // Check type equal.
            var otherCondition = obj as FeeServiceNestedCondition;
            if (otherCondition == null)
            {
                return false;
            }

            // Check field ID equal.
            if (this.FieldId != otherCondition.FieldId)
            {
                return false;
            }

            // Check values are the same length.
            if (this.NestedConditions.Count != otherCondition.NestedConditions.Count)
            {
                return false;
            }

            foreach (var kvp in this.NestedConditions)
            {
                // Check all values exist is both set.
                AbstractFeeServiceCondition condition;
                if (!otherCondition.NestedConditions.TryGetValue(kvp.Key, out condition))
                {
                    return false;
                }

                // Check that nested condition are the same.
                if (!kvp.Value.Equals(condition))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Gets this objects hash code.
        /// </summary>
        /// <returns>Integer hash code.</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 31;
                hash = (hash * 37) + FieldId.GetHashCode();
                foreach (var kvp in this.NestedConditions)
                {
                    hash = (hash * 37) + kvp.Key.GetHashCode();
                    hash = (hash * 37) + kvp.Value.GetHashCode();
                }

                return hash;
            }
        }

        /// <summary>
        /// Gets a string representation of this object.
        /// </summary>
        /// <returns>String representation of this object.</returns>
        public override string ToString()
        {
            // Join values as semicolon separated list (of possibly comma separated values).
            var valueCsv = string.Join(";", this.NestedConditions.OrderBy(kvp => kvp.Key).Select(kvp => kvp.Key + ": " + kvp.Value.ToString()).ToArray());
            return string.Format("({0}={1})", this.FieldId, valueCsv);
        }

        /// <summary>
        /// Gets the XML schema used for validation.
        /// </summary>
        /// <returns>Null. There is no schema for this object.</returns>
        public override XmlSchema GetSchema()
        {
            return null;
        }

        /// <summary>
        /// Used to deserialize this object from XML.
        /// </summary>
        /// <param name="reader">An xml reader.</param>
        public override void ReadXml(XmlReader reader)
        {
            this.FieldId = reader.GetAttribute("FieldId");

            bool isEmptyElement = reader.IsEmptyElement;
            if (reader.IsEmptyElement)
            {
                throw new CBaseException(ErrorMessages.Generic, "A nested condition should never exist without nested conditions.");
            }

            reader.ReadStartElement("Condition");

            // Read nested condition elements.
            while (reader.NodeType == XmlNodeType.Element && reader.Name == "Condition")
            {
                string key = reader.GetAttribute("key");

                var conditionType = (E_FeeServiceConditionT)Enum.Parse(typeof(E_FeeServiceConditionT), reader.GetAttribute("ConditionType"));
                AbstractFeeServiceCondition condition;
                switch (conditionType)
                {
                    case E_FeeServiceConditionT.Enumerated:
                        condition = new FeeServiceEnumeratedCondition();
                        condition.ReadXml(reader);
                        break;
                    case E_FeeServiceConditionT.Range:
                        condition = new FeeServiceRangeCondition();
                        condition.ReadXml(reader);
                        break;
                    case E_FeeServiceConditionT.Nested:
                        condition = new FeeServiceNestedCondition();
                        condition.ReadXml(reader);
                        break;
                    default:
                        throw new UnhandledEnumException(conditionType);
                }

                this.NestedConditions.Add(key, condition);
            }

            reader.ReadEndElement();
        }

        /// <summary>
        /// Used to serialize this object to XML.
        /// </summary>
        /// <param name="writer">An XML writer.</param>
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("ConditionType", E_FeeServiceConditionT.Nested.ToString("d"));
            writer.WriteAttributeString("FieldId", this.FieldId);
            foreach (var kvp in this.NestedConditions)
            {
                writer.WriteStartElement("Condition");
                writer.WriteAttributeString("key", kvp.Key);
                kvp.Value.WriteXml(writer);
                writer.WriteEndElement();
            }
        }
    }
}
