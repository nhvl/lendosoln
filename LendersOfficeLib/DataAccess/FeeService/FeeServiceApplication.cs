﻿namespace DataAccess.FeeService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Audit;
    using LendersOffice.ObjLib.Audit;
    using LendersOffice.ObjLib.TitleProvider;
    using LendersOffice.ObjLib.Escrow;
    using LendersOffice.Rolodex;
    using LendersOffice.Security;
    using System.Xml.Linq;
    using DataAccess.LoanComparison;

    public static class FeeServiceApplication
    {
        public static FeeServiceRevision GetCurrentRevision(Guid BrokerId, bool IsTest)
        {
            if (IsTest)
            {
                return FeeServiceRevision.RetrieveLatestTestRevision(BrokerId);
            }
            else
            {
                return FeeServiceRevision.RetrieveLatestRevision(BrokerId);
            }
        }

        public static FeeServiceRevision GetRevision(Guid BrokerId, int feeServiceRev)
        {
            return FeeServiceRevision.RetrieveRevision(BrokerId, feeServiceRev);
        }

        public static FeeServiceResult SetFees(CPageData dataLoan, ILoanProgramTemplate loanProd, FeeServiceRevision revision, PricingQuotes previousQuote) 
        {
            if (revision == null)
            {
                return new FeeServiceResult() { isValidFees = false }; // Cannot compute when no fees are valid.
            }

            bool originalBypass = dataLoan.ByPassFieldSecurityCheck;
            dataLoan.ByPassFieldSecurityCheck = true;  // Must be allowed to set through secured fields.


            var feeServiceHistory = new ClosingCostApplicationHistory(dataLoan.sFeeServiceApplicationHistoryXmlContent);
            bool priorSubmission = !feeServiceHistory.IsBlank;

            if ( revision.Id.HasValue )
                feeServiceHistory.RevisionId = revision.Id.Value;

            var matchingData = new FeeServiceMatchingTemplateData()
            {
                sBrokerId = dataLoan.sBrokerId,
                sLPurposeT = dataLoan.sLPurposeT,
                sFinMethT = loanProd.lFinMethT,
                sLienPosT = dataLoan.sLienPosT,
                aOccT = dataLoan.GetAppData(0).aOccT,
                sSpState = dataLoan.sSpState,
                sLT = loanProd.lLT,
                sBranchChannelT = dataLoan.sBranchChannelT,
                lLpProductType = loanProd.lLpProductType,
                lLpInvestorNm = loanProd.lLpInvestorNm,
                sBranchId = dataLoan.sBranchId,
                sProdSpT = dataLoan.sProdSpT,
                sApprVal = dataLoan.sApprVal == 0 ? dataLoan.sPurchPrice : dataLoan.sApprVal,
                sFinalLAmt = dataLoan.sFinalLAmt,
                sLAmt = dataLoan.sLAmtCalc,
                sPurchPrice = dataLoan.sPurchPrice,
                sProdImpound = dataLoan.sProdImpound,
                sLenderFeeBuyoutRequestedT = dataLoan.sLenderFeeBuyoutRequestedT,
                sLeadSrcId = dataLoan.sLeadSrcId,
                sCorrespondentProcessT = dataLoan.sCorrespondentProcessT,
                sPmlCompanyTierId = dataLoan.sPmlCompanyTierId,
                sDisclosureRegulationT = dataLoan.sDisclosureRegulationT,
                sSpCountyFips = dataLoan.sSpCountyFips,
                sIsNewConstruction = dataLoan.sIsNewConstruction
            };

            var applicableTemplates = revision.GetApplicableFeeTemplates(matchingData);

            bool pbLogEnabled = false; // SAEs will want this for debugging.  Eventually need to maybe have it on CERT or somewhere similar.
            bool writeDebugLog = pbLogEnabled || ConstSite.EnableFilebasedCmp;
            StringBuilder debug_log = null;
            if (writeDebugLog)
            {
                debug_log = new StringBuilder();
                debug_log.AppendLine("<FeeService> " + dataLoan.sLpTemplateNm);
                debug_log.AppendLine( Environment.NewLine + "Parameters:");
                debug_log.AppendLine("sLPurposeT = " + dataLoan.sLPurposeT);
                debug_log.AppendLine("sFinMethT = " + loanProd.lFinMethT);
                debug_log.AppendLine("sLienPosT = " + dataLoan.sLienPosT);
                debug_log.AppendLine("aOccT = " + dataLoan.GetAppData(0).aOccT);
                debug_log.AppendLine("sSpState = " + dataLoan.sSpState);
                debug_log.AppendLine("sLT = " + loanProd.lLT);
                debug_log.AppendLine("sBranchChannelT = " + dataLoan.sBranchChannelT);
                debug_log.AppendLine("lLpProductType = " + loanProd.lLpProductType);
                debug_log.AppendLine("lLpInvestorNm = " + loanProd.lLpInvestorNm);
                debug_log.AppendLine("sBranchId = " + dataLoan.sBranchId);
                debug_log.AppendLine("sProdSpT = " + dataLoan.sProdSpT);
                debug_log.AppendLine("sApprVal = " + dataLoan.sApprVal);
                debug_log.AppendLine("sFinalLAmt = " + dataLoan.sFinalLAmt);
                debug_log.AppendLine("sLAmt = " + dataLoan.sLAmtCalc);
                debug_log.AppendLine("sPurchPrice = " + dataLoan.sPurchPrice);
                debug_log.AppendLine("sProdImpound = " + dataLoan.sProdImpound);
                debug_log.AppendLine("sLenderFeeBuyoutRequestedT = " + dataLoan.sLenderFeeBuyoutRequestedT);
                debug_log.AppendLine("sLeadSrcId = " + dataLoan.sLeadSrcId);
                debug_log.AppendLine(Environment.NewLine + "Assignments:" );
                debug_log.AppendLine("sCorrespondentProcessT = " + dataLoan.sCorrespondentProcessT);
                debug_log.AppendLine("sPmlCompanyTierId = " + dataLoan.sPmlCompanyTierId);
                debug_log.AppendLine("sDisclosureRegulationT = " + dataLoan.sDisclosureRegulationT);
                debug_log.AppendLine("sSpCountyFips = " + dataLoan.sSpCountyFips);
                debug_log.AppendLine("sIsNewConstruction = " + dataLoan.sIsNewConstruction);
            }

            Dictionary<string, string> specialFields = new Dictionary<string, string>();
            HashSet<string> protectedFields = GetProtectedFields(dataLoan);

            Dictionary<string, E_ClosingFeeSource> feeSourceMap = new Dictionary<string, E_ClosingFeeSource>();

            // OPM 220727 - Keep track of fees being set by fee service.
            HashSet<Guid> setFeeTypes = new HashSet<Guid>();

            // OPM 227804 - Audit Fee Service
            Dictionary<string, FeeServiceApplicationAuditItem.RuleAppliedAuditDetails> auditDetails = new Dictionary<string, FeeServiceApplicationAuditItem.RuleAppliedAuditDetails>();

            bool updateEscrowAccount = false;
            bool alwaysSet801, alwaysSet802;
            bool lenderCreditFromRateLock = dataLoan.sLenderCreditCalculationMethodT == E_sLenderCreditCalculationMethodT.CalculateFromFrontEndRateLock;

            DetermineCreditCostApplication(applicableTemplates, out alwaysSet801, out alwaysSet802, ref lenderCreditFromRateLock);

            if (alwaysSet801)
            {
                // OPM 179391/175025
                dataLoan.sLOrigFMb = 0;
                dataLoan.sLOrigFPc = 0;
            }

            if (alwaysSet802)
            {
                // OPM 179391/175025
                dataLoan.sLDiscntFMb = 0;
                dataLoan.sLDiscntPc = 0;
            }

            // OPM 244915. Pre-load pin-locks for better perf than checking per-fee.
            var housingLocks = GetHousingLocks(dataLoan.CurrentPricingState);

            // OPM 474540 - Moving retrieval of dataLoan.sAvailableSettlementServiceProviders outside of for loop
            // to avoid provider.EstimatedCostAmountPopulated getting set to true before title fee values are set.
            var availableSettlementServiceProviders = new Lazy<AvailableSettlementServiceProviders> (() => dataLoan.sAvailableSettlementServiceProviders);

            var cachedContactEntriesBySystemId = new Lazy<Dictionary<string, RolodexDB>>(() => 
                RolodexDB.ListRolodexEntriesBySystemIdWithPermissionBypass(dataLoan.sBrokerId, restrictToSettlementServiceProviders: true));

            Dictionary<int, QuoteRequestOptions> requestOptionsByVendorId = new Dictionary<int, QuoteRequestOptions>();
            Dictionary<E_HousingExpenseTypeT, decimal> previousExpenseTotal = new Dictionary<E_HousingExpenseTypeT, decimal>();
            foreach (var template in applicableTemplates)
            {
                string settingXml = template.GetSerializedConditions();

                foreach (var templateField in template.GetTemplateFields())
                {
                    // OPM 198239 - For unmigrated legacy loans, skip new rules.
                    if (dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy && templateField.RuleType != FeeServiceRuleType.Legacy)
                    {
                        throw new FeeServiceException("Fee Service attempted to apply new format rules to an unmigrated loan.",
                            "Fee service attempted to apply new format rules to a legacy loan. LoanId: " + dataLoan.sLId);
                    }

                    // OPM 243540 - Ignore TitleSource rules when broker bit is off.
                    if (templateField.RuleType == FeeServiceRuleType.TitleSource && !dataLoan.BrokerDB.EnableEnhancedTitleQuotes)
                    {
                        Tools.LogWarning($"Fee Service attempted to apply a TitleSource rule but Enhanced Title Fees are not enabled. FieldId:[{templateField.FieldId}].");
                        continue;
                    }

                    // For TitleSource rules, add vender and request options to the dictionary,
                    // for use later when we order title quotes.
                    if (templateField.RuleType == FeeServiceRuleType.TitleSource)
                    {
                        SetTitleSourceRequestOptions(templateField, dataLoan, requestOptionsByVendorId);
                        continue;
                    }

                    if (!AllowFieldToBeSet(templateField, feeServiceHistory.FeeSetHistory, protectedFields, matchingData, alwaysSet801, alwaysSet802, lenderCreditFromRateLock, dataLoan, previousExpenseTotal, housingLocks))
                    {
                        // Cannot write this one.
                        if (writeDebugLog) debug_log.AppendLine("DO NOT SET: " + templateField.FieldId + " = " + templateField.Value);
                        continue;
                    }

                    bool updateHistory;
                    bool fieldRequiresEscrowAccountUpdate;
                    try
                    {
                        if (writeDebugLog)
                        {
                            debug_log.AppendLine("SET: " + templateField.FieldId + " = " + templateField.Value);
                        }

                        switch (templateField.RuleType)
                        {
                            case FeeServiceRuleType.Legacy:
                                CheckIfHudLineSetInSameRun_Legacy(templateField, dataLoan, setFeeTypes);   // OPM 220727 - Throws if different fee with same hudline set in same run of fee service.
                                SetValue(templateField.FieldId, templateField.Value, dataLoan, specialFields, out updateHistory, out fieldRequiresEscrowAccountUpdate);
                                LogSettingLegacyFee(templateField, template.ToString(), feeSourceMap, auditDetails);
                                break;
                            case FeeServiceRuleType.ClosingCost:
                                fieldRequiresEscrowAccountUpdate = false;
                                SetClosingCostFee(templateField, dataLoan, setFeeTypes, feeServiceHistory.FeeSetHistory, matchingData, template.ToString(), feeSourceMap, auditDetails, availableSettlementServiceProviders, cachedContactEntriesBySystemId, out updateHistory);
                                break;
                            default:
                                // NOTE: This throws for TitleSource rules, since those should not be handled here.
                                throw new UnhandledEnumException(templateField.RuleType);
                        }
                    }
                    catch (Exception exc)
                    {
                        // Log More detail.  If a field is supposed to be set and we cannot set it, pricing will be inaccurate.
                        Tools.LogError("Fee Service: Unable to set field: " + templateField.FieldId + "::" + templateField.Value, exc);

                        if (exc.GetType() == typeof(FeeServiceException))
                        {
                            throw;
                        }
                        else
                        {
                            throw new FeeServiceException(exc);
                        }
                    }

                    if (updateHistory)
                    {
                        // Update the history to reflect new way this field got set.
                        UpdateFeeHistory(feeServiceHistory.FeeSetHistory, templateField, settingXml);

                        // OPM 220727 - Add fee to the set of affected fee types.
                        if (templateField.TypeId != Guid.Empty)
                        {
                            setFeeTypes.Add(templateField.TypeId);
                        }
                    }

                    updateEscrowAccount |= fieldRequiresEscrowAccountUpdate;
                }
            }

            if (writeDebugLog)
            {
                debug_log.AppendLine(Environment.NewLine + "Special Fields:");
                foreach (var field in specialFields)
                {
                    debug_log.AppendLine(field.Key + " = " + field.Value);
                }
            }

            string debugInfo = string.Empty;
            if (writeDebugLog)
            {
                debugInfo = debug_log.ToString();
                if (!ConstSite.PricingCompactLogEnabled)
                {
                    debugInfo += Environment.NewLine + feeServiceHistory.ToXml();
                }
            }

            if (pbLogEnabled)
            {
                Tools.LogInfo(debugInfo);
            }

            int revisionId = -1;

            if (revision.Id.HasValue)
            {
                revisionId = revision.Id.Value;
            }

            FeeServiceResult result = new FeeServiceResult(revisionId, specialFields, dataLoan, feeSourceMap, debugInfo);

            SetCompositeFields(dataLoan, specialFields);

            if (updateEscrowAccount)
            {
                // 8/20/2014 gf - Set the initial escrow account to its in-memory
                // value. Evaluating the fee templates will only set the value in
                // memory. Setting the property will update the XML and allow the
                // data to persist.
                dataLoan.sInitialEscrowAcc = dataLoan.sInitialEscrowAcc;
            }

            // It is intended that this might overwrite some of the fields from Fee service.
            if (!dataLoan.sHasInitialDisclosure && dataLoan.BrokerDB.IsTitleInterfaceEnabled(dataLoan.sBranchChannelT, dataLoan.sLPurposeT))
            {
                if(!dataLoan.BrokerDB.EnableEnhancedTitleQuotes)
                {
                    requestOptionsByVendorId.Clear();

                    if (dataLoan.BrokerDB.ClosingCostTitleVendorId.HasValue)
                    {
                        QuoteRequestOptions options = new QuoteRequestOptions()
                        {
                            RequestTitle = true,
                            RequestClosing = !dataLoan.BrokerDB.TitleInterfaceTitleRecordingOnly(dataLoan.sSpState),
                            RequestRecording = true
                        };

                        requestOptionsByVendorId.Add(dataLoan.BrokerDB.ClosingCostTitleVendorId.Value, options);
                    }
                }

                PricingQuotes quoteResult = previousQuote;
                if (quoteResult == null && requestOptionsByVendorId.Count > 0)
                {
                    quoteResult = TitleProvider.GetQuotesForPricing(dataLoan.sBrokerId, dataLoan.GetTitleQuoteRequestData(), requestOptionsByVendorId);
                }

                IReadOnlyCollection<TitleFeeQuoteApplicationRecord> appliedFees = SetTitle(dataLoan, quoteResult, feeSourceMap, auditDetails, availableSettlementServiceProviders);
                if (appliedFees.Any())
                {
                    result.AuditItems.Add(new TitleQuoteImportAuditItem(PrincipalFactory.CurrentPrincipal, appliedFees));
                }

                result.quoteResult = quoteResult;
            }

            // To save changes to sAvailableSettlementServiceProviders, we need to
            // store the result back to the loan so the new serialized string can be stored
            // to the loan file. See SettlementServiceProviderListService.aspx for an example
            // of how SSPs are stored to the loan.
            if (availableSettlementServiceProviders.IsValueCreated)
            {
                dataLoan.sAvailableSettlementServiceProviders = availableSettlementServiceProviders.Value;
            }

            // OPM 227804 - Audit Fee Service.
            if (revision != null) // It should not be null if we reached this point.
            {
                AbstractAuditItem auditItem = new FeeServiceApplicationAuditItem(PrincipalFactory.CurrentPrincipal, revision.IsTest, priorSubmission, dataLoan.sClosingCostAutomationUpdateT, auditDetails.Values);
                result.AuditItems.Add(auditItem);
            }

            dataLoan.sFeeServiceApplicationHistoryXmlContent = feeServiceHistory.ToXml();
            
            dataLoan.ByPassFieldSecurityCheck = originalBypass;

            return result;
        }

        private static void LogSetting_SourceOnly(string feeLine, E_ClosingFeeSource source, Dictionary<string, E_ClosingFeeSource> feeSourceMap)
        {
            if (string.IsNullOrEmpty(feeLine) == false)
            {
                feeSourceMap[feeLine] = source;
            }
        }

        private static void LogSetting_AuditOnly(string key, string fieldId, Guid FeeId, string hudLine, string value, string conditions, Dictionary<string, FeeServiceApplicationAuditItem.RuleAppliedAuditDetails> auditDetails)
        {
            var details = new FeeServiceApplicationAuditItem.RuleAppliedAuditDetails(value, hudLine, fieldId, conditions, FeeId);
            auditDetails[key] = details;
        }

        private static void LogSettingImpl(string key, string fieldId, Guid FeeId, string hudLine, string value, string conditions, E_ClosingFeeSource source, Dictionary<string, E_ClosingFeeSource> feeSourceMap, Dictionary<string, FeeServiceApplicationAuditItem.RuleAppliedAuditDetails> auditDetails)
        {
            LogSetting_SourceOnly(hudLine, source, feeSourceMap); // Log fee source.
            LogSetting_AuditOnly(key, fieldId, FeeId, hudLine, value, conditions, auditDetails); // Audit fee service rule.
        }

        private static void LogSettingLegacyFee(FeeServiceTemplate.TemplateField templateField, string conditions, Dictionary<string, E_ClosingFeeSource> feeSourceMap, Dictionary<string, FeeServiceApplicationAuditItem.RuleAppliedAuditDetails> auditDetails)
        {
            int lineNum = AvailableFeeServiceFields.GetHudLineFromFieldId(templateField.FieldId);
            LogSettingImpl(templateField.Key, templateField.FieldId, Guid.Empty, lineNum == 0 ? string.Empty : lineNum.ToString(), templateField.Value, conditions, E_ClosingFeeSource.FeeService, feeSourceMap, auditDetails);
        }

        private static void LogSettingClosingCostFee(FeeServiceTemplate.TemplateField templateField, BaseClosingCostFee fee, string conditions, Dictionary<string, E_ClosingFeeSource> feeSourceMap, Dictionary<string, FeeServiceApplicationAuditItem.RuleAppliedAuditDetails> auditDetails, bool foundSettlementServiceProvider)
        {
            var setValue = templateField.Value;
            if (templateField.FeeProperty == E_FeeServiceFeePropertyT.SettlementServiceProvider && !foundSettlementServiceProvider)
            {
                setValue = ErrorMessages.FeeService.MissingSettlementServiceProvider(templateField.Value);
            }

            LogSettingImpl(templateField.Key, templateField.FieldId, templateField.TypeId, fee.HudLine_rep, setValue, conditions, E_ClosingFeeSource.FeeService, feeSourceMap, auditDetails);
        }

        private static void LogSettingTitleQuote_Legacy(string FieldId, string value, Dictionary<string, E_ClosingFeeSource> feeSourceMap, Dictionary<string, FeeServiceApplicationAuditItem.RuleAppliedAuditDetails> auditDetails)
        {
            int lineNum = AvailableFeeServiceFields.GetHudLineFromFieldId(FieldId);
            LogSettingImpl(FieldId, FieldId, Guid.Empty, lineNum == 0 ? string.Empty : lineNum.ToString(), value, "Value set by title quote", E_ClosingFeeSource.FirstAmerican, feeSourceMap, auditDetails);
        }

        private static void LogSettingTitleQuote(LoanClosingCostFee fee, Dictionary<string, E_ClosingFeeSource> feeSourceMap, Dictionary<string, FeeServiceApplicationAuditItem.RuleAppliedAuditDetails> auditDetails)
        {
            string key = fee.ClosingCostFeeTypeId.ToString() + ":" + E_FeeServiceFeePropertyT.Amount.ToString();
            LogSettingImpl(key, fee.OriginalDescription + ": Affiliate", fee.ClosingCostFeeTypeId, fee.HudLine_rep, fee.BaseAmount_rep, "Value set by title quote", E_ClosingFeeSource.FirstAmerican, feeSourceMap, auditDetails);
        }

        private static void LogSettingHousingExpense(CPageData dataLoan, BaseHousingExpense expense, FeeServiceTemplate.TemplateField templateField, string conditions, Dictionary<string, E_ClosingFeeSource> feeSourceMap, Dictionary<string, FeeServiceApplicationAuditItem.RuleAppliedAuditDetails> auditDetails)
        {
            if (templateField.TypeId == Guid.Empty)
            {
                return; // Can't do anything with an empty Guid.
            }

            // Get Hud Line number for section 900 Fee.
            Guid Sect900TypeId = GetHousingExpense900LineId(templateField.TypeId);
            string hud900 = GetHudLineFromFeeType(Sect900TypeId, dataLoan);

            // Get Hud Line number for section 1000 Fee.
            string hud1000 = expense.LineNumAsString;
            if (string.IsNullOrEmpty(hud1000))
            {
                hud1000 = GetHudLineFromFeeType(templateField.TypeId, dataLoan);
            }

            // Log source for both hud lines.
            LogSetting_SourceOnly(hud900, E_ClosingFeeSource.FeeService, feeSourceMap);
            LogSetting_SourceOnly(hud1000, E_ClosingFeeSource.FeeService, feeSourceMap);

            // Create Audit.
            LogSetting_AuditOnly(templateField.Key, templateField.FieldId, templateField.TypeId, "Housing Expense", templateField.Value, conditions, auditDetails);

            string hudLines = hud900 + "/" + hud1000;
        }

        public static string GetHudLineFromFeeType(Guid typeId, CPageData dataLoan)
        {
            if (typeId == Guid.Empty)
            {
                return string.Empty; // Can't do anything with an empty Guid.
            }

            // Get Fee from Fee Setup
            FeeSetupClosingCostFee feeType = (FeeSetupClosingCostFee)dataLoan.sBrokerUnlinkedClosingCostSet.FindFeeByTypeId(typeId);

            // Log if could not find fee type (should not be possible as housing expeses are default fee types).
            if (feeType == null)
            {
                Tools.LogWarning("Fee Service, SetHousingExpenses failed to find a fee in the Fee Setup with ID: " + typeId);
                return string.Empty;
            }

            return feeType.HudLine_rep;
        }

        // OPM 220727 - If a fee with same hud line was set by the current run of fee service (the one setting this fee) then throw an error.
        private static void CheckIfHudLineSetInSameRun_Legacy(FeeServiceTemplate.TemplateField templateField, CPageData dataLoan, HashSet<Guid> setFeeTypes)
        {
            // Note: If TypeId is Guid.Empty then this fieldId does not set an actual fee, so we can skip this check.
            if (templateField.TypeId != Guid.Empty)
            {
                // If the fee already exists in the fee set, then we don't nee to add it and can skip this check.
                BorrowerClosingCostFee fee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.FindFeeByTypeId(templateField.TypeId);
                if (fee == null)
                {
                    int hudLine = AvailableFeeServiceFields.GetHudLineFromFieldId(templateField.FieldId); // Don't need to check fee setup because legacy fees all have fixed hudlines.
                    if (hudLine != 0 && !ClosingCostSetUtils.HudLinesAllowingMultipleFees.Contains(hudLine))
                    {
                        IEnumerable<BaseClosingCostFee> matchingFees = dataLoan.sClosingCostSet.FindFeeByHudline(hudLine);
                        if (matchingFees.Count() == 1)
                        {
                            BaseClosingCostFee match = matchingFees.First();

                            if (setFeeTypes.Contains(match.ClosingCostFeeTypeId))
                            {
                                string msg = "Fee Service is attempting to add a fee with the same HUD Line number as an existing fee added earlier in the same run of fee service. " +
                                    "New Fee: " + templateField.FieldId + ". Existing Fee: " + match.ClosingCostFeeTypeId;
                                throw new FeeServiceException("Two fees with HUD line " + hudLine.ToString() + " are applied in the same fee service run.", msg);
                            }
                        }
                        else if (matchingFees.Count() > 1)
                        {
                            throw FeeServiceException.MultipleMatchingFeesException(templateField.FieldId, hudLine.ToString(), null, matchingFees);
                        }
                    }
                }
            }
        }

        private static Dictionary<E_HousingExpenseTypeT, bool> GetHousingLocks(PricingState pricingState)
        {
            // OPM 244915. Preload the housing locks from a pinstate.
            // True->explicit lock, False->explicit unlock.
            // Missing -> not set
            var locks = new Dictionary<E_HousingExpenseTypeT, bool>();

            if (pricingState != null)
            {
                if (pricingState.sProHazInsLocked.HasValue)
                {
                    locks.Add(E_HousingExpenseTypeT.HazardInsurance, pricingState.sProHazInsLocked.Value);
                }

                if (pricingState.sProRealETxLocked.HasValue)
                {
                    locks.Add(E_HousingExpenseTypeT.RealEstateTaxes, pricingState.sProRealETxLocked.Value);
                }

                if (pricingState.sProHoAssocDuesLocked.HasValue)
                {
                    locks.Add(E_HousingExpenseTypeT.HomeownersAsscDues, pricingState.sProHoAssocDuesLocked.Value);
                }

                if (pricingState.sProOHExpLocked.HasValue)
                {
                    locks.Add(E_HousingExpenseTypeT.SchoolTaxes, pricingState.sProOHExpLocked.Value);
                    locks.Add(E_HousingExpenseTypeT.OtherTaxes1, pricingState.sProOHExpLocked.Value);
                    locks.Add(E_HousingExpenseTypeT.OtherTaxes2, pricingState.sProOHExpLocked.Value);
                    locks.Add(E_HousingExpenseTypeT.OtherTaxes3, pricingState.sProOHExpLocked.Value);
                    locks.Add(E_HousingExpenseTypeT.OtherTaxes4, pricingState.sProOHExpLocked.Value);
                }
            }
                return locks;
        }

        private static void DetermineCreditCostApplication(IEnumerable<FeeServiceTemplate> applicableTemplates, out bool alwaysSet801, out bool alwaysSet802, ref bool lenderCreditFromRateLock)
        {
            // OPM 179391. Peek ahead and see if 801 or 802 should bypass logic.
            // Need to know this before 801/802 can be processed.
            
            // Engine Defaults:
            E_cPricingEngineCostT costTo = E_cPricingEngineCostT._802CreditOrCharge;
            E_cPricingEngineCreditT creditTo = E_cPricingEngineCreditT.None;

            // OPM 209852 - Check if sLenderCreditCalculationMethodT is set and what it's set to.
            bool calcMethodSet = false;
            E_sLenderCreditCalculationMethodT calcMethod = E_sLenderCreditCalculationMethodT.SetManually;

            foreach (var template in applicableTemplates)
            {
                foreach (var templateField in template.GetTemplateFields())
                {
                    string fieldId = templateField.FieldId;
                    if (fieldId == "sPricingEngineCostT" )
                    {
                        costTo = (E_cPricingEngineCostT) int.Parse(templateField.Value);
                    }

                    else if (fieldId == "sPricingEngineCreditT" )
                    {
                        creditTo = (E_cPricingEngineCreditT)int.Parse(templateField.Value);
                    }

                    else if (fieldId == "sLenderCreditCalculationMethodT")
                    {
                        calcMethodSet = true;
                        calcMethod = (E_sLenderCreditCalculationMethodT)int.Parse(templateField.Value);
                    }
                }
            }

            // OPM 209852 - Determine if lender credits will pull from the front end rate lock after fee service is applied.
            // Use value set in loan file (passed in as reference) if sLenderCreditCalculationMethodT not set by fee service.
            lenderCreditFromRateLock = calcMethodSet ? calcMethod == E_sLenderCreditCalculationMethodT.CalculateFromFrontEndRateLock : lenderCreditFromRateLock;

            // OPM 209852 - If sLenderCreditCalculationMethodT is set from rate lock, ignore sPricingEngineCostT and sPricingEngineCreditT.
            if (lenderCreditFromRateLock)
            {
                // Reset costTo/creditTo to defaults
                costTo = E_cPricingEngineCostT._802CreditOrCharge;
                creditTo = E_cPricingEngineCreditT.None;
            }

            alwaysSet801 = costTo == E_cPricingEngineCostT._801LoanOriginationFee || creditTo == E_cPricingEngineCreditT._801LoanOriginationFee;
            alwaysSet802 = costTo == E_cPricingEngineCostT._802CreditOrCharge || creditTo == E_cPricingEngineCreditT._802CreditOrCharge;
        }

        private static void SetTitleSourceRequestOptions(FeeServiceTemplate.TemplateField templateField, CPageData dataLoan, Dictionary<int, QuoteRequestOptions> requestOptionsByVendorId)
        {
            int vendorId;
            if (!int.TryParse(templateField.Value, out vendorId))
            {
                throw new FeeServiceException($"Fee Service could not order a title quote because the vendor ID is invalid. vendorId=[{templateField.Value}].");
            }

            QuoteRequestOptions options;
            if (!requestOptionsByVendorId.TryGetValue(vendorId, out options))
            {
                options = new QuoteRequestOptions();
                requestOptionsByVendorId.Add(vendorId, options);
            }

            switch (templateField.FieldId.ToLower())
            {
                case "titlefeesource":
                    options.RequestTitle = true;
                    break;
                case "closingfeesource":
                    options.RequestClosing = true;
                    break;
                case "recordingfeesource":
                    options.RequestRecording = true;
                    break;
                default:
                    throw new FeeServiceException(new UnhandledCaseException(templateField.FieldId));
            }
        }

        private static IReadOnlyCollection<TitleFeeQuoteApplicationRecord> SetTitle(
            CPageData dataLoan,
            PricingQuotes pricingQuotes,
            Dictionary<string, E_ClosingFeeSource> feeSourceMap,
            Dictionary<string, FeeServiceApplicationAuditItem.RuleAppliedAuditDetails> auditDetails, 
            Lazy<AvailableSettlementServiceProviders> availableSettlementServiceProviders)
        {
            // If the fee service is set to use a title provider quote, 
            // we need to use the numbers we get from that title provider for the fees within that quote.
            // We would only use the fee service values if the quote fails.

            List<TitleFeeQuoteApplicationRecord> appliedFees = new List<TitleFeeQuoteApplicationRecord>();

            if (pricingQuotes == null || pricingQuotes.HasError)
            {
                return appliedFees;
            }
            
            foreach (QuoteResultData quoteResult in pricingQuotes.Quotes)
            {
                appliedFees.AddRange(ApplyTitleQuote(dataLoan, quoteResult, feeSourceMap, auditDetails));
            }

            // OPM 473635 - Update Settlement Service Provider List.
            foreach (TitleFeeQuoteApplicationRecord fee in appliedFees)
            {
                if (fee.FeeTypeId == Guid.Empty)
                {
                    continue;
                }

                foreach (SettlementServiceProvider provider in availableSettlementServiceProviders.Value.GetProvidersForFeeTypeId(fee.FeeTypeId))
                {
                    if (provider.IsSystemGenerated)
                    {
                        provider.EstimatedCostAmountPopulated = false;
                    }
                }
            }

            return appliedFees;
        }

        public static IReadOnlyCollection<TitleFeeQuoteApplicationRecord> ApplyTitleQuote(
            CPageData loanData,
            QuoteResultData results,
            Dictionary<string, E_ClosingFeeSource> feeSourceMap,
            Dictionary<string, FeeServiceApplicationAuditItem.RuleAppliedAuditDetails> auditDetails)
        {
            return TitleService.ApplyQuote(
                loanData,
                results,
                borrowerFee => LogSettingTitleQuote(borrowerFee, feeSourceMap, auditDetails),
                (fieldId, value) => LogSettingTitleQuote_Legacy(fieldId, value, feeSourceMap, auditDetails));
        }

        // Update the history to reflect new way this field got set.
        private static void UpdateFeeHistory(List<FeeConditionHit> history, FeeServiceTemplate.TemplateField templateField, string settingCondition)
        {
            // Disassociate this field from its current condition
            foreach (var condition in history)
            {
                condition.FieldIds.RemoveAll(new Predicate<string>(GetConditionCheckPredicate(templateField)));
            }

            history.RemoveAll(p => p.FieldIds.Count == 0);

            // Associate it with this condition.
            var conditionList = history.FirstOrDefault(p => p.SetCondition == settingCondition);
            if (conditionList == null)
            {
                conditionList = new FeeConditionHit() { SetCondition = settingCondition, FieldIds = new List<string>() };
                history.Add(conditionList);
            }
            conditionList.FieldIds.Add(templateField.Key);
        }

        private static List<FeeConditionHit> GetFeeServiceHistory(CPageData dataLoan)
        {
            return (dataLoan.sFeeServiceApplicationHistoryXmlContent != string.Empty) ?
                (List<FeeConditionHit>)SerializationHelper.XmlDeserialize(dataLoan.sFeeServiceApplicationHistoryXmlContent, typeof(List<FeeConditionHit>))
                : new List<FeeConditionHit>();
        }

        private static string SetFeeServiceHistory(CPageData dataLoan, List<FeeConditionHit> feeServiceHistory)
        {
            
            string historyXml = SerializationHelper.XmlSerialize(feeServiceHistory);
            dataLoan.sFeeServiceApplicationHistoryXmlContent = historyXml;
            return historyXml;
        }

        private static BorrowerClosingCostFeePayment GetClosingCostFeePayment(BorrowerClosingCostFee fee)
        {
            // Remove split payments
            if (fee.Payments.Count() > 1)
            {
                fee.ClearPayments();
            }

            return (BorrowerClosingCostFeePayment)fee.Payments.First();
        }

        private static BaseHousingExpense GetHousingExpenseForFeeService(CPageData dataLoan, Guid typeId)
        {
            if (typeId == DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId)
            {
                return dataLoan.sHazardExpense;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId)
            {
                return dataLoan.sFloodExpense;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000WindStormInsuranceFeeTypeId)
            {
                return dataLoan.sWindstormExpense;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000CondoHo6InsuranceFeeTypeId)
            {
                return dataLoan.sCondoHO6Expense;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId)
            {
                return dataLoan.sRealEstateTaxExpense;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId)
            {
                return dataLoan.sSchoolTaxExpense;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000OtherTax1FeeTypeId)
            {
                return dataLoan.sOtherTax1Expense;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000OtherTax2FeeTypeId)
            {
                return dataLoan.sOtherTax2Expense;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000OtherTax3FeeTypeId)
            {
                return dataLoan.sOtherTax3Expense;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000OtherTax4FeeTypeId)
            {
                return dataLoan.sOtherTax4Expense;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000HomeOwnerAssociationDuesFeeTypeId)
            {
                return dataLoan.sHOADuesExpense;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000GroundRentFeeTypeId)
            {
                return dataLoan.sGroundRentExpense;
            }
            else
            {
                throw new FeeServiceException("Fee service encountered an unexpected housing expense type.", "Unexpected housing expense type.");
            }
        }

        private static Guid GetHousingExpense900LineId(Guid typeId)
        {
            if (typeId == DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId)
            {
                return DefaultSystemClosingCostFee.Hud900HazardInsuranceFeeTypeId;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId)
            {
                return DefaultSystemClosingCostFee.Hud900FloodInsuranceFeeTypeId;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000WindStormInsuranceFeeTypeId)
            {
                return DefaultSystemClosingCostFee.Hud900WindstormInsuranceFeeTypeId;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000CondoHo6InsuranceFeeTypeId)
            {
                return DefaultSystemClosingCostFee.Hud900CondoInsuranceFeeTypeId;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId)
            {
                return DefaultSystemClosingCostFee.Hud900PropertyTaxFeeTypeId;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId)
            {
                return DefaultSystemClosingCostFee.Hud900SchoolTaxFeeTypeId;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000OtherTax1FeeTypeId)
            {
                return DefaultSystemClosingCostFee.Hud900OtherTax1FeeTypeId;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000OtherTax2FeeTypeId)
            {
                return DefaultSystemClosingCostFee.Hud900OtherTax2FeeTypeId;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000OtherTax3FeeTypeId)
            {
                return DefaultSystemClosingCostFee.Hud900OtherTax3FeeTypeId;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000OtherTax4FeeTypeId)
            {
                return DefaultSystemClosingCostFee.Hud900OtherTax4FeeTypeId;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000HomeOwnerAssociationDuesFeeTypeId)
            {
                return DefaultSystemClosingCostFee.Hud900HOADuesFeeTypeId;
            }
            else if (typeId == DefaultSystemClosingCostFee.Hud1000GroundRentFeeTypeId)
            {
                return DefaultSystemClosingCostFee.Hud900GroundRentFeeTypeId;
            }
            else
            {
                throw new FeeServiceException("Fee service encountered an unexpected housing expense type.", "Unexpected housing expense type.");
            }
        }

        private static void SetHousingExpenses(FeeServiceTemplate.TemplateField templateField, CPageData dataLoan, string conditions, Dictionary<string, E_ClosingFeeSource> feeSourceMap,
            Dictionary<string, FeeServiceApplicationAuditItem.RuleAppliedAuditDetails> auditDetails, out bool updateHistory)
        {
            BaseHousingExpense expense;
            expense = GetHousingExpenseForFeeService(dataLoan, templateField.TypeId);

            // expense should have a value at this point
            // Set Fee Property
            switch (templateField.FeeProperty)
            {
                case E_FeeServiceFeePropertyT.Description:
                    expense.ExpenseDescription = templateField.Value;
                    break;
                case E_FeeServiceFeePropertyT.Percent:
                    expense.AnnualAmtCalcBasePerc_rep = templateField.Value;
                    break;
                case E_FeeServiceFeePropertyT.BaseValue:
                    expense.AnnualAmtCalcBaseType = (E_PercentBaseT)dataLoan.m_convertLos.ToCount(templateField.Value);
                    break;
                case E_FeeServiceFeePropertyT.Amount:
                    expense.MonthlyAmtFixedAmt_rep = templateField.Value;
                    break;
                case E_FeeServiceFeePropertyT.TaxType:
                    expense.TaxType = (E_TaxTableTaxT)dataLoan.m_convertLos.ToCount(templateField.Value);
                    break;
                case E_FeeServiceFeePropertyT.CalculationSource:
                    expense.AnnualAmtCalcType = (E_AnnualAmtCalcTypeT)dataLoan.m_convertLos.ToCount(templateField.Value);
                    break;
                case E_FeeServiceFeePropertyT.Prepaid:
                    expense.IsPrepaid = templateField.Value == "True";
                    break;
                case E_FeeServiceFeePropertyT.Escrow:
                    expense.IsEscrowedAtClosing = templateField.Value == "True" ? E_TriState.Yes : E_TriState.No;
                    break;
                case E_FeeServiceFeePropertyT.PrepaidMonths:
                    expense.PrepaidMonths_rep = templateField.Value;
                    break;
                case E_FeeServiceFeePropertyT.ReserveCushion:
                    expense.DisbursementScheduleMonths[0] = dataLoan.m_convertLos.ToCount(templateField.Value);
                    dataLoan.ClearInitialEscrowAccCache();
                    break;
                case E_FeeServiceFeePropertyT.ReserveMonthsLocked:
                    expense.ReserveMonthsLckd = templateField.Value == "True";
                    break;
                case E_FeeServiceFeePropertyT.ReserveMonths:
                    expense.ReserveMonths_rep = templateField.Value;
                    break;
                case E_FeeServiceFeePropertyT.PaymentRepeatInterval:
                    expense.DisbursementRepInterval = (E_DisbursementRepIntervalT)dataLoan.m_convertLos.ToCount(templateField.Value);
                    break;
                case E_FeeServiceFeePropertyT.DisbursementSchedule:
                    var disbursementSchedule = ObsoleteSerializationHelper.JavascriptJsonDeserializer<EscrowItemDisbursementSchedule>(templateField.Value);

                    if (disbursementSchedule == null)
                    {
                        throw new FeeServiceException("Fee Service could not successfully deserialize Disbursement Schedule.");
                    }
                    else if (!disbursementSchedule.Validate())  // Probably don't need validation. Parser should have taken care of that.
                    {
                        throw new FeeServiceException("Housing expense disbursement schedule failed to validate and could not be set.", "Cannot set escrow item disbursement schedule to invalid value.");
                    }

                    expense.DisbursementScheduleMonths[1] = disbursementSchedule.Jan;
                    expense.DisbursementScheduleMonths[2] = disbursementSchedule.Feb;
                    expense.DisbursementScheduleMonths[3] = disbursementSchedule.Mar;
                    expense.DisbursementScheduleMonths[4] = disbursementSchedule.Apr;
                    expense.DisbursementScheduleMonths[5] = disbursementSchedule.May;
                    expense.DisbursementScheduleMonths[6] = disbursementSchedule.Jun;
                    expense.DisbursementScheduleMonths[7] = disbursementSchedule.Jul;
                    expense.DisbursementScheduleMonths[8] = disbursementSchedule.Aug;
                    expense.DisbursementScheduleMonths[9] = disbursementSchedule.Sep;
                    expense.DisbursementScheduleMonths[10] = disbursementSchedule.Oct;
                    expense.DisbursementScheduleMonths[11] = disbursementSchedule.Nov;
                    expense.DisbursementScheduleMonths[12] = disbursementSchedule.Dec;

                    dataLoan.ClearInitialEscrowAccCache();

                    break;
                default:
                    throw new FeeServiceException("Fee service encountered an unexpected property type.", "Unexpected property type.");
            }

            updateHistory = true;

            // Log source and audit
            LogSettingHousingExpense(dataLoan, expense, templateField, conditions, feeSourceMap, auditDetails);
        }

        private static void SetClosingCostFee(
            FeeServiceTemplate.TemplateField templateField, 
            CPageData dataLoan, 
            HashSet<Guid> setFeeTypes,
            List<FeeConditionHit> feeSetHistory, 
            FeeServiceMatchingTemplateData matchingData,
            string conditions, 
            Dictionary<string, E_ClosingFeeSource> feeSourceMap, 
            Dictionary<string, FeeServiceApplicationAuditItem.RuleAppliedAuditDetails> auditDetails,
            Lazy<AvailableSettlementServiceProviders> availableSettlementServiceProviders,
            Lazy<Dictionary<string, RolodexDB>> cachedContactEntriesBySystemId,
            out bool updateHistory)
        {
            updateHistory = false;

            // Handle housing expenses' monthly amount separately
            if (AvailableFeeServiceFields.HousingExpensesFeeTypeIds.Contains(templateField.TypeId))
            {
                // These fees can't be set if Housing Expenses haven't been migrated, so log and ignore.
                if (dataLoan.sIsHousingExpenseMigrated == false)
                {
                    string msg = "Fee Service is attempting to set the following housing expense, but housing expenses have not yet been migrated on the loan: " + templateField.FieldId;
                    throw new FeeServiceException("Fee service attempted to set a housing expense using new format rules, but housing expenses have not been migrated yet.", msg);
                }

                //// Don't set Mortgage Insurance Amount fields through FeeService (this should technically never hit)
                //if (templateField.TypeId == DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId)
                //{
                //    string msg = "Fee Service is attempting to set the Mortgage Insurance amount, which is not supported: " + templateField.FieldId;
                //    throw new FeeServiceException(ErrorMessages.FeeServiceError, msg);
                //}

                SetHousingExpenses(templateField, dataLoan, conditions, feeSourceMap, auditDetails, out updateHistory);
                return;
            }

            // Find matching broker feeType
            FeeSetupClosingCostFee feeType = (FeeSetupClosingCostFee) dataLoan.sBrokerUnlinkedClosingCostSet.FindFeeByTypeId(templateField.TypeId);

            // If we cannot find a matching fee type, log error and return
            if (feeType == null)
            {
                // Cannot write this one.
                string msg = "The fee service is attempting to set the following fee type, which is not available: " + templateField.FieldId;
                throw new FeeServiceException("Fee service cannot find a matching fee type in the fee setup.", msg);
            }

            // Find matching fee in dataloan fee set
            BorrowerClosingCostFee fee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.FindFeeByTypeId(templateField.TypeId);

            // if fee not found and property is not remove, use default fee type.
            if (fee == null && templateField.FeeProperty != E_FeeServiceFeePropertyT.Remove)
            {
                fee = feeType.ConvertToBorrowerClosingCostFee();
                fee.OriginalDescription = feeType.Description;

                // OPM 220727 - If a fee with the same HUD Line already exists check if we can overwrite it.
                if (!ClosingCostSetUtils.HudLinesAllowingMultipleFees.Contains(fee.HudLine))
                {
                    IEnumerable<BaseClosingCostFee> matchingFees = dataLoan.sClosingCostSet.FindFeeByHudline(fee.HudLine);
                    if (matchingFees.Count() == 1)
                    {
                        BaseClosingCostFee match = matchingFees.First();

                        // OPM 220727 - If a fee with same hud line was set by the current run of fee service (the one setting this fee) then throw an error.
                        if (setFeeTypes.Contains(match.ClosingCostFeeTypeId))
                        {
                            string msg = "Fee Service is attempting to add a fee with the same HUD Line number as an existing fee added earlier in the same run of fee service. " +
                                "New Fee: " + templateField.FieldId + ". Existing Fee: " + match.ClosingCostFeeTypeId;
                            throw new FeeServiceException("Two fees with HUD line " + fee.HudLine_rep + " are applied in the same fee service run.", msg);
                        }

                        // Check if matching fee was set by fee service.
                        string removeKey = match.ClosingCostFeeTypeId.ToString() + ":" + E_FeeServiceFeePropertyT.Remove.ToString();
                        var previousSettingCondition = feeSetHistory.FirstOrDefault(p => p.FieldIds.Any(s => s.StartsWith(match.ClosingCostFeeTypeId.ToString()) && s != removeKey));

                        // If conditions have changed since matching fee was set, remove matching fee so we can add the new fee.
                        // OPM 227987 - Check if loan is set to stomp all fees.
                        if (dataLoan.sClosingCostAutomationUpdateT == E_sClosingCostAutomationUpdateT.UpdateUnconditionally ||  CheckConditionsChanged(previousSettingCondition, matchingData))
                        {
                            dataLoan.sClosingCostSet.Remove(match);
                        }
                        else // If conditions have changed, do not overwright.
                        {
                            // Treat as though AllowFieldToBeSet returned false. Do not update history. Do not modify loan file.
                            return;
                        }
                    }
                    else if (matchingFees.Count() > 1)
                    {
                        throw FeeServiceException.MultipleMatchingFeesException(templateField.FieldId, fee.HudLine_rep, fee.Description, matchingFees);
                    }
                }
                
                // For LegacyButMigrated loans, only add custom fee if we haven't already taken up
                // max number of custom fees for that section.
                if (dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.LegacyButMigrated &&
                templateField.RuleType == FeeServiceRuleType.ClosingCost && feeType.LegacyGfeFieldT == E_LegacyGfeFieldT.Undefined)
                {
                    string sectionName;
                    switch (feeType.HudLine / 100)
                    {
                        case 8:
                            sectionName = ClosingCostSetUtils.Hud800ItemsPayable;
                            break;
                        case 9:
                            sectionName = ClosingCostSetUtils.Hud900ItemsRequiredByLenderToBePaid;
                            break;
                        case 10:
                            // Don't add custom fees for the 1000 lines.
                            //sectionName = ClosingCostSet.Hud1000ReservesDeposited;
                            return;
                        case 11:
                            sectionName = ClosingCostSetUtils.Hud1100Title;
                            break;
                        case 12:
                            sectionName = ClosingCostSetUtils.Hud1200Government;
                            break;
                        case 13:
                            sectionName = ClosingCostSetUtils.Hud1300AdditionalSettlementCharge;
                            break;
                        default:
                            throw new FeeServiceException(" Fee service can't determine if there is enough room to add a new custom fee in a LegacyButMigrated loan because the HUD line on the fee is invalid.",
                                "Unexpected Line Number: " + feeType.HudLine);
                    }

                    int hudSection = (feeType.HudLine / 100) * 100;
                    BorrowerClosingCostFeeSection section = (BorrowerClosingCostFeeSection)dataLoan.sClosingCostSet.GetSection(E_ClosingCostViewT.LoanHud1, sectionName);
                    section.SetExcludeFeeList(section.FilteredClosingCostFeeList, dataLoan.sClosingCostFeeVersionT, null);

                    if (section.HasRoomForCustomFee(hudSection, fee.HudLine) == false)
                    {
                        string usrMsg = "Fee Service is attempting to add more than the max number of allowable custom " + hudSection + " section fees to a LegacyButMigrated loan.";
                        string devMsg = "Fee Service is attempting to add more than the max number of allowable custom fee types to a LegacyButMigrated loan: " + templateField.FieldId;

                        FeeServiceException exp = new FeeServiceException(usrMsg, devMsg);
                        exp.IsEmailDeveloper = false;
                        throw exp;
                    }
                }
            }

            var foundSettlementServiceProvider = false;

            // Set Fee Property
            switch (templateField.FeeProperty)
            {
                case E_FeeServiceFeePropertyT.Description:
                    fee.Description = templateField.Value;
                    break;
                //case E_FeeServiceFeePropertyT.GfeBox:
                //    fee.GfeSectionT = (E_GfeSectionT)dataLoan.m_convertLos.ToCount(templateField.Value);
                //    break;
                case E_FeeServiceFeePropertyT.Apr:
                    fee.IsApr = templateField.Value == "True";
                    break;
                case E_FeeServiceFeePropertyT.Fha:
                    fee.IsFhaAllowable = templateField.Value == "True";
                    break;
                case E_FeeServiceFeePropertyT.PaidTo:
                    fee.Beneficiary = (E_AgentRoleT)dataLoan.m_convertLos.ToCount(templateField.Value);
                    break;
                case E_FeeServiceFeePropertyT.Dflp:
                    fee.Dflp = templateField.Value == "True";
                    break;
                case E_FeeServiceFeePropertyT.ThirdParty:
                    fee.IsThirdParty = templateField.Value == "True";
                    break;
                case E_FeeServiceFeePropertyT.Affiliate:
                    fee.IsAffiliate = templateField.Value == "True";
                    break;
                case E_FeeServiceFeePropertyT.CanShop:
                    fee.CanShop = templateField.Value == "True";
                    break;
                case E_FeeServiceFeePropertyT.Percent:
                    fee.FormulaT = E_ClosingCostFeeFormulaT.Full;
                    fee.Percent_rep = templateField.Value;
                    break;
                case E_FeeServiceFeePropertyT.BaseValue:
                    fee.FormulaT = E_ClosingCostFeeFormulaT.Full;
                    fee.PercentBaseT = (E_PercentBaseT)dataLoan.m_convertLos.ToCount(templateField.Value);
                    break;
                case E_FeeServiceFeePropertyT.Amount:
                    fee.FormulaT = E_ClosingCostFeeFormulaT.Full;
                    fee.BaseAmount_rep = templateField.Value;
                    break;
                case E_FeeServiceFeePropertyT.PaidBy:
                    GetClosingCostFeePayment(fee).PaidByT = (E_ClosingCostFeePaymentPaidByT)dataLoan.m_convertLos.ToCount(templateField.Value);
                    break;
                case E_FeeServiceFeePropertyT.Payable:
                    GetClosingCostFeePayment(fee).GfeClosingCostFeePaymentTimingT = (E_GfeClosingCostFeePaymentTimingT)dataLoan.m_convertLos.ToCount(templateField.Value);
                    break;
                case E_FeeServiceFeePropertyT.SettlementServiceProvider:
                    foundSettlementServiceProvider = AddSettlementServiceProvider(dataLoan, availableSettlementServiceProviders, cachedContactEntriesBySystemId.Value, templateField);
                    break;
                case E_FeeServiceFeePropertyT.Remove:
                    // only need to do something if the fee exists
                    if (templateField.Value == "True" && fee != null)
                    {
                        dataLoan.sClosingCostSet.Remove(fee);
                    }
                    break;
                default:
                    throw new FeeServiceException("Fee service encountered an unexpected property type.", "Unexpected property type.");
            }

            if (fee != null && templateField.FeeProperty != E_FeeServiceFeePropertyT.Remove)
            {
                dataLoan.sClosingCostSet.AddOrUpdate(fee);
            }

            updateHistory = true;

            BaseClosingCostFee AuditFeeLine = fee == null ? (BaseClosingCostFee) feeType : (BaseClosingCostFee) fee;
            LogSettingClosingCostFee(templateField, AuditFeeLine, conditions, feeSourceMap, auditDetails, foundSettlementServiceProvider);
        }

        private static void SetValue( string fieldId, string setValue, CPageData dataLoan, Dictionary<string,string> specialFields, out bool updateHistory, out bool updateEscrowAccount)
        {
            updateHistory = true;
            updateEscrowAccount = false;

            // Use giant switch statment because using reflection has poor performance.
            switch (fieldId)
            {
                case "sLOrigFPc": dataLoan.sLOrigFPc_rep = setValue; specialFields.Add(fieldId, setValue); break;
                case "sLOrigFMb": dataLoan.sLOrigFMb_rep = setValue; specialFields.Add(fieldId, setValue); break;
                case "sLOrigFProps_PdByT": dataLoan.sLOrigFProps_PdByT_rep = setValue; break;
                case "sLOrigFProps_Apr": dataLoan.sLOrigFProps_Apr = (setValue == "True"); break;
                case "sLOrigFProps_BF": dataLoan.sLOrigFProps_BF = (setValue == "True"); break;
                case "sLOrigFProps_FhaAllow": dataLoan.sLOrigFProps_FhaAllow = (setValue == "True"); break;
                case "sSettlementLOrigFProps_Dflp": dataLoan.sSettlementLOrigFProps_Dflp = (setValue == "True"); break;
                case "sLOrigFProps_ToBroker": dataLoan.sLOrigFProps_ToBroker = (setValue == "True"); break;
                case "sLDiscntPc": dataLoan.sLDiscntPc_rep = setValue; specialFields.Add(fieldId, setValue); break;
                case "sLDiscntBaseT": dataLoan.sLDiscntBaseT = (E_PercentBaseT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sLDiscntFMb": dataLoan.sLDiscntFMb_rep = setValue; break;
                case "sGfeDiscountPointFProps_PdByT": dataLoan.sGfeDiscountPointFProps_PdByT_rep = setValue; break;
                case "sGfeLenderCreditFProps_Apr": dataLoan.sGfeLenderCreditFProps_Apr = (setValue == "True"); break;
                case "sGfeLenderCreditFProps_FhaAllow": dataLoan.sGfeLenderCreditFProps_FhaAllow = (setValue == "True"); break;
                case "sGfeDiscountPointFProps_Apr": dataLoan.sGfeDiscountPointFProps_Apr = (setValue == "True"); break;
                case "sGfeDiscountPointFProps_BF": dataLoan.sGfeDiscountPointFProps_BF = (setValue == "True"); break;
                case "sGfeDiscountPointFProps_FhaAllow": dataLoan.sGfeDiscountPointFProps_FhaAllow = (setValue == "True"); break;
                case "sSettlementDiscountPointFProps_Dflp": dataLoan.sSettlementDiscountPointFProps_Dflp = (setValue == "True"); break;
                case "sApprF": dataLoan.sApprF_rep = setValue; break;
                case "sApprFProps_PdByT": dataLoan.sApprFProps_PdByT_rep = setValue; break;
                case "sApprFProps_Apr": dataLoan.sApprFProps_Apr = (setValue == "True"); break;
                case "sApprFProps_FhaAllow": dataLoan.sApprFProps_FhaAllow = (setValue == "True"); break;
                case "sApprFProps_Poc": dataLoan.sApprFProps_Poc = (setValue == "True"); break;
                case "sApprFPaid": dataLoan.sApprFPaid = (setValue == "True"); break;
                case "sSettlementApprFProps_Dflp": dataLoan.sSettlementApprFProps_Dflp = (setValue == "True"); break;
                case "sApprFProps_ToBroker": dataLoan.sApprFProps_ToBroker = (setValue == "True"); break;
                case "sApprFPaidTo": dataLoan.sApprFPaidTo = setValue; break;
                case "sCrF": dataLoan.sCrF_rep = setValue; break;
                case "sCrFProps_PdByT": dataLoan.sCrFProps_PdByT_rep = setValue; break;
                case "sCrFProps_Apr": dataLoan.sCrFProps_Apr = (setValue == "True"); break;
                case "sCrFProps_FhaAllow": dataLoan.sCrFProps_FhaAllow = (setValue == "True"); break;
                case "sCrFProps_Poc": dataLoan.sCrFProps_Poc = (setValue == "True"); break;
                case "sCrFPaid": dataLoan.sCrFPaid = (setValue == "True"); break;
                case "sSettlementCrFProps_Dflp": dataLoan.sSettlementCrFProps_Dflp = (setValue == "True"); break;
                case "sCrFProps_ToBroker": dataLoan.sCrFProps_ToBroker = (setValue == "True"); break;
                case "sCrFPaidTo": dataLoan.sCrFPaidTo = setValue; break;
                case "sTxServF": dataLoan.sTxServF_rep = setValue; break;
                case "sTxServFProps_PdByT": dataLoan.sTxServFProps_PdByT_rep = setValue; break;
                case "sTxServFProps_Apr": dataLoan.sTxServFProps_Apr = (setValue == "True"); break;
                case "sTxServFProps_FhaAllow": dataLoan.sTxServFProps_FhaAllow = (setValue == "True"); break;
                case "sTxServFProps_Poc": dataLoan.sTxServFProps_Poc = (setValue == "True"); break;
                case "sSettlementTxServFProps_Dflp": dataLoan.sSettlementTxServFProps_Dflp = (setValue == "True"); break;
                case "sTxServFProps_ToBroker": dataLoan.sTxServFProps_ToBroker = (setValue == "True"); break;
                case "sTxServFPaidTo": dataLoan.sTxServFPaidTo = setValue; break;
                case "sFloodCertificationF": dataLoan.sFloodCertificationF_rep = setValue; break;
                case "sFloodCertificationDeterminationT": dataLoan.sFloodCertificationDeterminationT = (E_FloodCertificationDeterminationT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sFloodCertificationFProps_PdByT": dataLoan.sFloodCertificationFProps_PdByT_rep = setValue; break;
                case "sFloodCertificationFProps_Apr": dataLoan.sFloodCertificationFProps_Apr = (setValue == "True"); break;
                case "sFloodCertificationFProps_FhaAllow": dataLoan.sFloodCertificationFProps_FhaAllow = (setValue == "True"); break;
                case "sFloodCertificationFProps_Poc": dataLoan.sFloodCertificationFProps_Poc = (setValue == "True"); break;
                case "sSettlementFloodCertificationFProps_Dflp": dataLoan.sSettlementFloodCertificationFProps_Dflp = (setValue == "True"); break;
                case "sFloodCertificationFProps_ToBroker": dataLoan.sFloodCertificationFProps_ToBroker = (setValue == "True"); break;
                case "sFloodCertificationFPaidTo": dataLoan.sFloodCertificationFPaidTo = setValue; break;
                case "sMBrokFPc": dataLoan.sMBrokFPc_rep = setValue; break;
                case "sMBrokFBaseT": dataLoan.sMBrokFBaseT = (E_PercentBaseT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sMBrokFMb": dataLoan.sMBrokFMb_rep = setValue; break;
                case "sMBrokFProps_PdByT": dataLoan.sMBrokFProps_PdByT_rep = setValue; break;
                case "sMBrokFProps_Apr": dataLoan.sMBrokFProps_Apr = (setValue == "True"); break;
                case "sMBrokFProps_FhaAllow": dataLoan.sMBrokFProps_FhaAllow = (setValue == "True"); break;
                case "sMBrokFProps_Poc": dataLoan.sMBrokFProps_Poc = (setValue == "True"); break;
                case "sSettlementMBrokFProps_Dflp": dataLoan.sSettlementMBrokFProps_Dflp = (setValue == "True"); break;
                case "sMBrokFProps_ToBroker": dataLoan.sMBrokFProps_ToBroker = (setValue == "True"); break;
                case "sInspectF": dataLoan.sInspectF_rep = setValue; break;
                case "sInspectFProps_PdByT": dataLoan.sInspectFProps_PdByT_rep = setValue; break;
                case "sInspectFProps_Apr": dataLoan.sInspectFProps_Apr = (setValue == "True"); break;
                case "sInspectFProps_FhaAllow": dataLoan.sInspectFProps_FhaAllow = (setValue == "True"); break;
                case "sInspectFProps_Poc": dataLoan.sInspectFProps_Poc = (setValue == "True"); break;
                case "sSettlementInspectFProps_Dflp": dataLoan.sSettlementInspectFProps_Dflp = (setValue == "True"); break;
                case "sInspectFProps_ToBroker": dataLoan.sInspectFProps_ToBroker = (setValue == "True"); break;
                case "sInspectFPaidTo": dataLoan.sInspectFPaidTo = setValue; break;
                case "sProcF": dataLoan.sProcF_rep = setValue; break;
                case "sProcFProps_PdByT": dataLoan.sProcFProps_PdByT_rep = setValue; break;
                case "sProcFProps_Apr": dataLoan.sProcFProps_Apr = (setValue == "True"); break;
                case "sProcFProps_FhaAllow": dataLoan.sProcFProps_FhaAllow = (setValue == "True"); break;
                case "sProcFProps_Poc": dataLoan.sProcFProps_Poc = (setValue == "True"); break;
                case "sProcFPaid": dataLoan.sProcFPaid = (setValue == "True"); break;
                case "sSettlementProcFProps_Dflp": dataLoan.sSettlementProcFProps_Dflp = (setValue == "True"); break;
                case "sProcFProps_ToBroker": dataLoan.sProcFProps_ToBroker = (setValue == "True"); break;
                case "sProcFPaidTo": dataLoan.sProcFPaidTo = setValue; break;
                case "sUwF": dataLoan.sUwF_rep = setValue; break;
                case "sUwFProps_PdByT": dataLoan.sUwFProps_PdByT_rep = setValue; break;
                case "sUwFProps_Apr": dataLoan.sUwFProps_Apr = (setValue == "True"); break;
                case "sUwFProps_FhaAllow": dataLoan.sUwFProps_FhaAllow = (setValue == "True"); break;
                case "sUwFProps_Poc": dataLoan.sUwFProps_Poc = (setValue == "True"); break;
                case "sSettlementUwFProps_Dflp": dataLoan.sSettlementUwFProps_Dflp = (setValue == "True"); break;
                case "sUwFProps_ToBroker": dataLoan.sUwFProps_ToBroker = (setValue == "True"); break;
                case "sUwFPaidTo": dataLoan.sUwFPaidTo = setValue; break;
                case "sWireF": dataLoan.sWireF_rep = setValue; break;
                case "sWireFProps_PdByT": dataLoan.sWireFProps_PdByT_rep = setValue; break;
                case "sWireFProps_Apr": dataLoan.sWireFProps_Apr = (setValue == "True"); break;
                case "sWireFProps_FhaAllow": dataLoan.sWireFProps_FhaAllow = (setValue == "True"); break;
                case "sWireFProps_Poc": dataLoan.sWireFProps_Poc = (setValue == "True"); break;
                case "sSettlementWireFProps_Dflp": dataLoan.sSettlementWireFProps_Dflp = (setValue == "True"); break;
                case "sWireFProps_ToBroker": dataLoan.sWireFProps_ToBroker = (setValue == "True"); break;
                case "sWireFPaidTo": dataLoan.sWireFPaidTo = setValue; break;
                case "s800U1FDesc": dataLoan.s800U1FDesc = setValue; break;
                case "s800U1F": dataLoan.s800U1F_rep = setValue; break;
                case "s800U1FProps_PdByT": dataLoan.s800U1FProps_PdByT_rep = setValue; break;
                case "s800U1FProps_Apr": dataLoan.s800U1FProps_Apr = (setValue == "True"); break;
                case "s800U1FProps_FhaAllow": dataLoan.s800U1FProps_FhaAllow = (setValue == "True"); break;
                case "s800U1FProps_Poc": dataLoan.s800U1FProps_Poc = (setValue == "True"); break;
                case "sSettlement800U1FProps_Dflp": dataLoan.sSettlement800U1FProps_Dflp = (setValue == "True"); break;
                case "s800U1FProps_ToBroker": dataLoan.s800U1FProps_ToBroker = (setValue == "True"); break;
                case "s800U1FPaidTo": dataLoan.s800U1FPaidTo = setValue; break;
                case "s800U1FGfeSection": dataLoan.s800U1FGfeSection = (E_GfeSectionT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "s800U2FDesc": dataLoan.s800U2FDesc = setValue; break;
                case "s800U2F": dataLoan.s800U2F_rep = setValue; break;
                case "s800U2FProps_PdByT": dataLoan.s800U2FProps_PdByT_rep = setValue; break;
                case "s800U2FProps_Apr": dataLoan.s800U2FProps_Apr = (setValue == "True"); break;
                case "s800U2FProps_FhaAllow": dataLoan.s800U2FProps_FhaAllow = (setValue == "True"); break;
                case "s800U2FProps_Poc": dataLoan.s800U2FProps_Poc = (setValue == "True"); break;
                case "sSettlement800U2FProps_Dflp": dataLoan.sSettlement800U2FProps_Dflp = (setValue == "True"); break;
                case "s800U2FProps_ToBroker": dataLoan.s800U2FProps_ToBroker = (setValue == "True"); break;
                case "s800U2FPaidTo": dataLoan.s800U2FPaidTo = setValue; break;
                case "s800U2FGfeSection": dataLoan.s800U2FGfeSection = (E_GfeSectionT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "s800U3FDesc": dataLoan.s800U3FDesc = setValue; break;
                case "s800U3F": dataLoan.s800U3F_rep = setValue; break;
                case "s800U3FProps_PdByT": dataLoan.s800U3FProps_PdByT_rep = setValue; break;
                case "s800U3FProps_Apr": dataLoan.s800U3FProps_Apr = (setValue == "True"); break;
                case "s800U3FProps_FhaAllow": dataLoan.s800U3FProps_FhaAllow = (setValue == "True"); break;
                case "s800U3FProps_Poc": dataLoan.s800U3FProps_Poc = (setValue == "True"); break;
                case "sSettlement800U3FProps_Dflp": dataLoan.sSettlement800U3FProps_Dflp = (setValue == "True"); break;
                case "s800U3FProps_ToBroker": dataLoan.s800U3FProps_ToBroker = (setValue == "True"); break;
                case "s800U3FPaidTo": dataLoan.s800U3FPaidTo = setValue; break;
                case "s800U3FGfeSection": dataLoan.s800U3FGfeSection = (E_GfeSectionT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "s800U4FDesc": dataLoan.s800U4FDesc = setValue; break;
                case "s800U4F": dataLoan.s800U4F_rep = setValue; break;
                case "s800U4FProps_PdByT": dataLoan.s800U4FProps_PdByT_rep = setValue; break;
                case "s800U4FProps_Apr": dataLoan.s800U4FProps_Apr = (setValue == "True"); break;
                case "s800U4FProps_FhaAllow": dataLoan.s800U4FProps_FhaAllow = (setValue == "True"); break;
                case "s800U4FProps_Poc": dataLoan.s800U4FProps_Poc = (setValue == "True"); break;
                case "sSettlement800U4FProps_Dflp": dataLoan.sSettlement800U4FProps_Dflp = (setValue == "True"); break;
                case "s800U4FProps_ToBroker": dataLoan.s800U4FProps_ToBroker = (setValue == "True"); break;
                case "s800U4FPaidTo": dataLoan.s800U4FPaidTo = setValue; break;
                case "s800U4FGfeSection": dataLoan.s800U4FGfeSection = (E_GfeSectionT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "s800U5FDesc": dataLoan.s800U5FDesc = setValue; break;
                case "s800U5F": dataLoan.s800U5F_rep = setValue; break;
                case "s800U5FProps_PdByT": dataLoan.s800U5FProps_PdByT_rep = setValue; break;
                case "s800U5FProps_Apr": dataLoan.s800U5FProps_Apr = (setValue == "True"); break;
                case "s800U5FProps_FhaAllow": dataLoan.s800U5FProps_FhaAllow = (setValue == "True"); break;
                case "s800U5FProps_Poc": dataLoan.s800U5FProps_Poc = (setValue == "True"); break;
                case "sSettlement800U5FProps_Dflp": dataLoan.sSettlement800U5FProps_Dflp = (setValue == "True"); break;
                case "s800U5FProps_ToBroker": dataLoan.s800U5FProps_ToBroker = (setValue == "True"); break;
                case "s800U5FPaidTo": dataLoan.s800U5FPaidTo = setValue; break;
                case "s800U5FGfeSection": dataLoan.s800U5FGfeSection = (E_GfeSectionT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sIPiaDy": dataLoan.sIPiaDy_rep = setValue; break;
                case "sIPiaProps_PdByT": dataLoan.sIPiaProps_PdByT_rep = setValue; break;
                case "sIPiaProps_Apr": dataLoan.sIPiaProps_Apr = (setValue == "True"); break;
                case "sIPiaProps_FhaAllow": dataLoan.sIPiaProps_FhaAllow = (setValue == "True"); break;
                case "sIPiaProps_Poc": dataLoan.sIPiaProps_Poc = (setValue == "True"); break;
                case "sSettlementIPiaProps_Dflp": dataLoan.sSettlementIPiaProps_Dflp = (setValue == "True"); break;
                case "sMipPiaProps_PdByT": dataLoan.sMipPiaProps_PdByT_rep = setValue; break;
                case "sMipPiaProps_Apr": dataLoan.sMipPiaProps_Apr = (setValue == "True"); break;
                case "sMipPiaProps_FhaAllow": dataLoan.sMipPiaProps_FhaAllow = (setValue == "True"); break;
                case "sMipPiaProps_Poc": dataLoan.sMipPiaProps_Poc = (setValue == "True"); break;
                case "sMipPiaProps_Dflp": dataLoan.sMipPiaProps_Dflp = (setValue == "True"); break;
                case "sMipPiaPaidTo": dataLoan.sMipPiaPaidTo = setValue; break;
                case "sProHazInsR": dataLoan.sProHazInsR_rep = setValue; break;
                case "sProHazInsT": dataLoan.sProHazInsT = (E_PercentBaseT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sProHazInsMb": dataLoan.sProHazInsMb_rep = setValue; break;
                case "sHazInsPiaMon": dataLoan.sHazInsPiaMon_rep = setValue; break;
                case "sHazInsPiaProps_PdByT": dataLoan.sHazInsPiaProps_PdByT_rep = setValue; break;
                case "sHazInsPiaProps_Apr": dataLoan.sHazInsPiaProps_Apr = (setValue == "True"); break;
                case "sHazInsPiaProps_FhaAllow": dataLoan.sHazInsPiaProps_FhaAllow = (setValue == "True"); break;
                case "sHazInsPiaProps_Poc": dataLoan.sHazInsPiaProps_Poc = (setValue == "True"); break;
                case "sSettlementHazInsPiaProps_Dflp": dataLoan.sSettlementHazInsPiaProps_Dflp = (setValue == "True"); break;
                case "sHazInsPiaPaidTo": dataLoan.sHazInsPiaPaidTo = setValue; break;
                case "s904PiaDesc": dataLoan.s904PiaDesc = setValue; break;
                case "s904Pia": dataLoan.s904Pia_rep = setValue; break;
                case "s904PiaProps_PdByT": dataLoan.s904PiaProps_PdByT_rep = setValue; break;
                case "s904PiaProps_Apr": dataLoan.s904PiaProps_Apr = (setValue == "True"); break;
                case "s904PiaProps_FhaAllow": dataLoan.s904PiaProps_FhaAllow = (setValue == "True"); break;
                case "s904PiaProps_Poc": dataLoan.s904PiaProps_Poc = (setValue == "True"); break;
                case "sSettlement904PiaProps_Dflp": dataLoan.sSettlement904PiaProps_Dflp = (setValue == "True"); break;
                case "s904PiaGfeSection": dataLoan.s904PiaGfeSection = (E_GfeSectionT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sVaFfProps_PdByT": dataLoan.sVaFfProps_PdByT_rep = setValue; break;
                case "sVaFfProps_Apr": dataLoan.sVaFfProps_Apr = (setValue == "True"); break;
                case "sVaFfProps_FhaAllow": dataLoan.sVaFfProps_FhaAllow = (setValue == "True"); break;
                case "sVaFfProps_Poc": dataLoan.sVaFfProps_Poc = (setValue == "True"); break;
                case "sVaFfProps_Dflp": dataLoan.sVaFfProps_Dflp = (setValue == "True"); break;
                case "sVaFfPaidTo": dataLoan.sVaFfPaidTo = setValue; break;
                case "s900U1PiaDesc": dataLoan.s900U1PiaDesc = setValue; break;
                case "s900U1Pia": dataLoan.s900U1Pia_rep = setValue; break;
                case "s900U1PiaProps_PdByT": dataLoan.s900U1PiaProps_PdByT_rep = setValue; break;
                case "s900U1PiaProps_Apr": dataLoan.s900U1PiaProps_Apr = (setValue == "True"); break;
                case "s900U1PiaProps_FhaAllow": dataLoan.s900U1PiaProps_FhaAllow = (setValue == "True"); break;
                case "s900U1PiaProps_Poc": dataLoan.s900U1PiaProps_Poc = (setValue == "True"); break;
                case "sSettlement900U1PiaProps_Dflp": dataLoan.sSettlement900U1PiaProps_Dflp = (setValue == "True"); break;
                case "s900U1PiaGfeSection": dataLoan.s900U1PiaGfeSection = (E_GfeSectionT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sHazInsRsrvMon": dataLoan.sHazInsRsrvMonLckd = true; dataLoan.sHazInsRsrvMon_rep = setValue; break;
                case "sHazInsRsrvProps_PdByT": dataLoan.sHazInsRsrvProps_PdByT_rep = setValue; break;
                case "sHazInsRsrvProps_Apr": dataLoan.sHazInsRsrvProps_Apr = (setValue == "True"); break;
                case "sHazInsRsrvProps_FhaAllow": dataLoan.sHazInsRsrvProps_FhaAllow = (setValue == "True"); break;
                case "sHazInsRsrvProps_Poc": dataLoan.sHazInsRsrvProps_Poc = (setValue == "True"); break;
                case "sHazInsRsrvEscrowedTri": dataLoan.sHazInsRsrvEscrowedTri = (E_TriState)int.Parse(setValue); break;
                case "sSettlementHazInsRsrvProps_Dflp": dataLoan.sSettlementHazInsRsrvProps_Dflp = (setValue == "True"); break;
                case "sMInsRsrvMon": dataLoan.sMInsRsrvMonLckd = true; dataLoan.sMInsRsrvMon_rep = setValue; break;
                case "sMInsRsrvProps_PdByT": dataLoan.sMInsRsrvProps_PdByT_rep = setValue; break;
                case "sMInsRsrvProps_Apr": dataLoan.sMInsRsrvProps_Apr = (setValue == "True"); break;
                case "sMInsRsrvProps_FhaAllow": dataLoan.sMInsRsrvProps_FhaAllow = (setValue == "True"); break;
                case "sMInsRsrvProps_Poc": dataLoan.sMInsRsrvProps_Poc = (setValue == "True"); break;
                case "sMIPaymentRepeat": dataLoan.sMIPaymentRepeat = (E_DisbursementRepIntervalT)int.Parse(setValue); break;
                case "sSettlementMInsRsrvProps_Dflp": dataLoan.sSettlementMInsRsrvProps_Dflp = (setValue == "True"); break;
                case "sProRealETxR": dataLoan.sProRealETxR_rep = setValue; break;
                case "sProRealETxT": dataLoan.sProRealETxT = (E_PercentBaseT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sProRealETxMb": dataLoan.sProRealETxMb_rep = setValue; break;
                case "sRealETxRsrvMon": dataLoan.sRealETxRsrvMonLckd = true; dataLoan.sRealETxRsrvMon_rep = setValue; break;
                case "sRealETxRsrvProps_PdByT": dataLoan.sRealETxRsrvProps_PdByT_rep = setValue; break;
                case "sRealETxRsrvProps_Apr": dataLoan.sRealETxRsrvProps_Apr = (setValue == "True"); break;
                case "sRealETxRsrvProps_FhaAllow": dataLoan.sRealETxRsrvProps_FhaAllow = (setValue == "True"); break;
                case "sRealETxRsrvProps_Poc": dataLoan.sRealETxRsrvProps_Poc = (setValue == "True"); break;
                case "sSettlementRealETxRsrvProps_Dflp": dataLoan.sSettlementRealETxRsrvProps_Dflp = (setValue == "True"); break;
                case "sRealETxRsrvEscrowedTri": dataLoan.sRealETxRsrvEscrowedTri = (E_TriState)int.Parse(setValue); break;
                case "sSchoolTxRsrvMon": dataLoan.sSchoolTxRsrvMonLckd = true; dataLoan.sSchoolTxRsrvMon_rep = setValue; break;
                case "sProSchoolTx": dataLoan.sProSchoolTx_rep = setValue; break;
                case "sSchoolTxRsrvProps_PdByT": dataLoan.sSchoolTxRsrvProps_PdByT_rep = setValue; break;
                case "sSchoolTxRsrvProps_Apr": dataLoan.sSchoolTxRsrvProps_Apr = (setValue == "True"); break;
                case "sSchoolTxRsrvProps_FhaAllow": dataLoan.sSchoolTxRsrvProps_FhaAllow = (setValue == "True"); break;
                case "sSchoolTxRsrvProps_Poc": dataLoan.sSchoolTxRsrvProps_Poc = (setValue == "True"); break;
                case "sSettlementSchoolTxRsrvProps_Dflp": dataLoan.sSettlementSchoolTxRsrvProps_Dflp = (setValue == "True"); break;
                case "sFloodInsRsrvMon": dataLoan.sFloodInsRsrvMonLckd = true; dataLoan.sFloodInsRsrvMon_rep = setValue; break;
                case "sProFloodIns": dataLoan.sProFloodIns_rep = setValue; break;
                case "sFloodInsRsrvProps_PdByT": dataLoan.sFloodInsRsrvProps_PdByT_rep = setValue; break;
                case "sFloodInsRsrvProps_Apr": dataLoan.sFloodInsRsrvProps_Apr = (setValue == "True"); break;
                case "sFloodInsRsrvProps_FhaAllow": dataLoan.sFloodInsRsrvProps_FhaAllow = (setValue == "True"); break;
                case "sFloodInsRsrvProps_Poc": dataLoan.sFloodInsRsrvProps_Poc = (setValue == "True"); break;
                case "sFloodInsRsrvEscrowedTri": dataLoan.sFloodInsRsrvEscrowedTri = (E_TriState)int.Parse(setValue); break;
                case "sSettlementFloodInsRsrvProps_Dflp": dataLoan.sSettlementFloodInsRsrvProps_Dflp = (setValue == "True"); break;
                case "sAggregateAdjRsrvProps_PdByT": dataLoan.sAggregateAdjRsrvProps_PdByT_rep = setValue; break;
                case "sAggregateAdjRsrvProps_Apr": dataLoan.sAggregateAdjRsrvProps_Apr = (setValue == "True"); break;
                case "sAggregateAdjRsrvProps_FhaAllow": dataLoan.sAggregateAdjRsrvProps_FhaAllow = (setValue == "True"); break;
                case "sAggregateAdjRsrvProps_Poc": dataLoan.sAggregateAdjRsrvProps_Poc = (setValue == "True"); break;
                case "sSettlementAggregateAdjRsrvProps_Dflp": dataLoan.sSettlementAggregateAdjRsrvProps_Dflp = (setValue == "True"); break;
                case "s1006ProHExpDesc": dataLoan.s1006ProHExpDesc = setValue; break;
                case "s1006RsrvMon": dataLoan.s1006RsrvMonLckd = true; dataLoan.s1006RsrvMon_rep = setValue; break;
                case "s1006ProHExp": dataLoan.s1006ProHExp_rep = setValue; break;
                case "s1006RsrvProps_PdByT": dataLoan.s1006RsrvProps_PdByT_rep = setValue; break;
                case "s1006RsrvProps_Apr": dataLoan.s1006RsrvProps_Apr = (setValue == "True"); break;
                case "s1006RsrvProps_FhaAllow": dataLoan.s1006RsrvProps_FhaAllow = (setValue == "True"); break;
                case "s1006RsrvProps_Poc": dataLoan.s1006RsrvProps_Poc = (setValue == "True"); break;
                case "s1006RsrvEscrowedTri": dataLoan.s1006RsrvEscrowedTri = (E_TriState)int.Parse(setValue); break;
                case "sSettlement1008RsrvProps_Dflp": dataLoan.sSettlement1008RsrvProps_Dflp = (setValue == "True"); break;
                case "s1007ProHExpDesc": dataLoan.s1007ProHExpDesc = setValue; break;
                case "s1007RsrvMon": dataLoan.s1007RsrvMonLckd = true; dataLoan.s1007RsrvMon_rep = setValue; break;
                case "s1007ProHExp": dataLoan.s1007ProHExp_rep = setValue; break;
                case "s1007RsrvProps_PdByT": dataLoan.s1007RsrvProps_PdByT_rep = setValue; break;
                case "s1007RsrvProps_Apr": dataLoan.s1007RsrvProps_Apr = (setValue == "True"); break;
                case "s1007RsrvProps_FhaAllow": dataLoan.s1007RsrvProps_FhaAllow = (setValue == "True"); break;
                case "s1007RsrvProps_Poc": dataLoan.s1007RsrvProps_Poc = (setValue == "True"); break;
                case "s1007RsrvEscrowedTri": dataLoan.s1007RsrvEscrowedTri = (E_TriState)int.Parse(setValue); break;
                case "sSettlement1009RsrvProps_Dflp": dataLoan.sSettlement1009RsrvProps_Dflp = (setValue == "True"); break;
                case "sU3RsrvDesc": dataLoan.sU3RsrvDesc = setValue; break;
                case "sU3RsrvMon": dataLoan.sU3RsrvMonLckd = true; dataLoan.sU3RsrvMon_rep = setValue; break;
                case "sProU3Rsrv": dataLoan.sProU3Rsrv_rep = setValue; break;
                case "sU3RsrvProps_PdByT": dataLoan.sU3RsrvProps_PdByT_rep = setValue; break;
                case "sU3RsrvProps_Apr": dataLoan.sU3RsrvProps_Apr = (setValue == "True"); break;
                case "sU3RsrvProps_FhaAllow": dataLoan.sU3RsrvProps_FhaAllow = (setValue == "True"); break;
                case "sU3RsrvProps_Poc": dataLoan.sU3RsrvProps_Poc = (setValue == "True"); break;
                case "sU3RsrvEscrowedTri": dataLoan.sU3RsrvEscrowedTri = (E_TriState)int.Parse(setValue); break;
                case "sSettlementU3RsrvProps_Dflp": dataLoan.sSettlementU3RsrvProps_Dflp = (setValue == "True"); break;
                case "sU4RsrvDesc": dataLoan.sU4RsrvDesc = setValue; break; 
                case "sU4RsrvMon": dataLoan.sU4RsrvMonLckd = true; dataLoan.sU4RsrvMon_rep = setValue; break;
                case "sProU4Rsrv": dataLoan.sProU4Rsrv_rep = setValue; break;
                case "sU4RsrvProps_PdByT": dataLoan.sU4RsrvProps_PdByT_rep = setValue; break;
                case "sU4RsrvProps_Apr": dataLoan.sU4RsrvProps_Apr = (setValue == "True"); break;
                case "sU4RsrvProps_FhaAllow": dataLoan.sU4RsrvProps_FhaAllow = (setValue == "True"); break;
                case "sU4RsrvProps_Poc": dataLoan.sU4RsrvProps_Poc = (setValue == "True"); break;
                case "sU4RsrvEscrowedTri": dataLoan.sU4RsrvEscrowedTri = (E_TriState)int.Parse(setValue); break;
                case "sSettlementU4RsrvProps_Dflp": dataLoan.sSettlementU4RsrvProps_Dflp = (setValue == "True"); break;
                case "sEscrowFProps_PdByT": dataLoan.sEscrowFProps_PdByT_rep = setValue; break;
                case "sEscrowFProps_Apr": dataLoan.sEscrowFProps_Apr = (setValue == "True"); break;
                case "sEscrowFProps_FhaAllow": dataLoan.sEscrowFProps_FhaAllow = (setValue == "True"); break;
                case "sEscrowFProps_Poc": dataLoan.sEscrowFProps_Poc = (setValue == "True"); break;
                case "sSettlementEscrowFProps_Dflp": dataLoan.sSettlementEscrowFProps_Dflp = (setValue == "True"); break;
                case "sEscrowFProps_ToBroker": dataLoan.sEscrowFProps_ToBroker = (setValue == "True"); break;
                case "sEscrowFTable": dataLoan.sEscrowFTable = setValue; break;
                case "sEscrowFGfeSection": dataLoan.sEscrowFGfeSection = (E_GfeSectionT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sOwnerTitleInsProps_PdByT": dataLoan.sOwnerTitleInsProps_PdByT_rep = setValue; break;
                case "sOwnerTitleInsProps_Apr": dataLoan.sOwnerTitleInsProps_Apr = (setValue == "True"); break;
                case "sOwnerTitleInsProps_FhaAllow": dataLoan.sOwnerTitleInsProps_FhaAllow = (setValue == "True"); break;
                case "sOwnerTitleInsProps_Poc": dataLoan.sOwnerTitleInsProps_Poc = (setValue == "True"); break;
                case "sSettlementOwnerTitleInsFProps_Dflp": dataLoan.sSettlementOwnerTitleInsFProps_Dflp = (setValue == "True"); break;
                case "sOwnerTitleInsProps_ToBroker": dataLoan.sOwnerTitleInsProps_ToBroker = (setValue == "True"); break;
                case "sOwnerTitleInsPaidTo": dataLoan.sOwnerTitleInsPaidTo = setValue; break;
                case "sTitleInsFProps_PdByT": dataLoan.sTitleInsFProps_PdByT_rep = setValue; break;
                case "sTitleInsFProps_Apr": dataLoan.sTitleInsFProps_Apr = (setValue == "True"); break;
                case "sTitleInsFProps_FhaAllow": dataLoan.sTitleInsFProps_FhaAllow = (setValue == "True"); break;
                case "sTitleInsFProps_Poc": dataLoan.sTitleInsFProps_Poc = (setValue == "True"); break;
                case "sSettlementTitleInsFProps_Dflp": dataLoan.sSettlementTitleInsFProps_Dflp = (setValue == "True"); break;
                case "sTitleInsFProps_ToBroker": dataLoan.sTitleInsFProps_ToBroker = (setValue == "True"); break;
                case "sTitleInsFTable": dataLoan.sTitleInsFTable = setValue; break;
                case "sDocPrepF": dataLoan.sDocPrepF_rep = setValue; break;
                case "sDocPrepFProps_PdByT": dataLoan.sDocPrepFProps_PdByT_rep = setValue; break;
                case "sDocPrepFProps_Apr": dataLoan.sDocPrepFProps_Apr = (setValue == "True"); break;
                case "sDocPrepFProps_FhaAllow": dataLoan.sDocPrepFProps_FhaAllow = (setValue == "True"); break;
                case "sDocPrepFProps_Poc": dataLoan.sDocPrepFProps_Poc = (setValue == "True"); break;
                case "sSettlementDocPrepFProps_Dflp": dataLoan.sSettlementDocPrepFProps_Dflp = (setValue == "True"); break;
                case "sDocPrepFProps_ToBroker": dataLoan.sDocPrepFProps_ToBroker = (setValue == "True"); break;
                case "sDocPrepFPaidTo": dataLoan.sDocPrepFPaidTo = setValue; break;
                case "sDocPrepFGfeSection": dataLoan.sDocPrepFGfeSection = (E_GfeSectionT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sNotaryF": dataLoan.sNotaryF_rep = setValue; break;
                case "sNotaryFProps_PdByT": dataLoan.sNotaryFProps_PdByT_rep = setValue; break;
                case "sNotaryFProps_Apr": dataLoan.sNotaryFProps_Apr = (setValue == "True"); break;
                case "sNotaryFProps_FhaAllow": dataLoan.sNotaryFProps_FhaAllow = (setValue == "True"); break;
                case "sNotaryFProps_Poc": dataLoan.sNotaryFProps_Poc = (setValue == "True"); break;
                case "sSettlementNotaryFProps_Dflp": dataLoan.sSettlementNotaryFProps_Dflp = (setValue == "True"); break;
                case "sNotaryFProps_ToBroker": dataLoan.sNotaryFProps_ToBroker = (setValue == "True"); break;
                case "sNotaryFPaidTo": dataLoan.sNotaryFPaidTo = setValue; break;
                case "sNotaryFGfeSection": dataLoan.sNotaryFGfeSection = (E_GfeSectionT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sAttorneyF": dataLoan.sAttorneyF_rep = setValue; break;
                case "sAttorneyFProps_PdByT": dataLoan.sAttorneyFProps_PdByT_rep = setValue; break;
                case "sAttorneyFProps_Apr": dataLoan.sAttorneyFProps_Apr = (setValue == "True"); break;
                case "sAttorneyFProps_FhaAllow": dataLoan.sAttorneyFProps_FhaAllow = (setValue == "True"); break;
                case "sAttorneyFProps_Poc": dataLoan.sAttorneyFProps_Poc = (setValue == "True"); break;
                case "sSettlementAttorneyFProps_Dflp": dataLoan.sSettlementAttorneyFProps_Dflp = (setValue == "True"); break;
                case "sAttorneyFProps_ToBroker": dataLoan.sAttorneyFProps_ToBroker = (setValue == "True"); break;
                case "sAttorneyFPaidTo": dataLoan.sAttorneyFPaidTo = setValue; break;
                case "sAttorneyFGfeSection": dataLoan.sAttorneyFGfeSection = (E_GfeSectionT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sU1TcDesc": dataLoan.sU1TcDesc = setValue; break;
                case "sU1Tc": dataLoan.sU1Tc_rep = setValue; break;
                case "sU1TcProps_PdByT": dataLoan.sU1TcProps_PdByT_rep = setValue; break;
                case "sU1TcProps_Apr": dataLoan.sU1TcProps_Apr = (setValue == "True"); break;
                case "sU1TcProps_FhaAllow": dataLoan.sU1TcProps_FhaAllow = (setValue == "True"); break;
                case "sU1TcProps_Poc": dataLoan.sU1TcProps_Poc = (setValue == "True"); break;
                case "sSettlementU1TcProps_Dflp": dataLoan.sSettlementU1TcProps_Dflp = (setValue == "True"); break;
                case "sU1TcProps_ToBroker": dataLoan.sU1TcProps_ToBroker = (setValue == "True"); break;
                case "sU1TcPaidTo": dataLoan.sU1TcPaidTo = setValue; break;
                case "sU1TcGfeSection": dataLoan.sU1TcGfeSection = (E_GfeSectionT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sU2TcDesc": dataLoan.sU2TcDesc = setValue; break;
                case "sU2Tc": dataLoan.sU2Tc_rep = setValue; break;
                case "sU2TcProps_PdByT": dataLoan.sU2TcProps_PdByT_rep = setValue; break;
                case "sU2TcProps_Apr": dataLoan.sU2TcProps_Apr = (setValue == "True"); break;
                case "sU2TcProps_FhaAllow": dataLoan.sU2TcProps_FhaAllow = (setValue == "True"); break;
                case "sU2TcProps_Poc": dataLoan.sU2TcProps_Poc = (setValue == "True"); break;
                case "sSettlementU2TcProps_Dflp": dataLoan.sSettlementU2TcProps_Dflp = (setValue == "True"); break;
                case "sU2TcProps_ToBroker": dataLoan.sU2TcProps_ToBroker = (setValue == "True"); break;
                case "sU2TcPaidTo": dataLoan.sU2TcPaidTo = setValue; break;
                case "sU2TcGfeSection": dataLoan.sU2TcGfeSection = (E_GfeSectionT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sU3TcDesc": dataLoan.sU3TcDesc = setValue; break;
                case "sU3Tc": dataLoan.sU3Tc_rep = setValue; break;
                case "sU3TcProps_PdByT": dataLoan.sU3TcProps_PdByT_rep = setValue; break;
                case "sU3TcProps_Apr": dataLoan.sU3TcProps_Apr = (setValue == "True"); break;
                case "sU3TcProps_FhaAllow": dataLoan.sU3TcProps_FhaAllow = (setValue == "True"); break;
                case "sU3TcProps_Poc": dataLoan.sU3TcProps_Poc = (setValue == "True"); break;
                case "sSettlementU3TcProps_Dflp": dataLoan.sSettlementU3TcProps_Dflp = (setValue == "True"); break;
                case "sU3TcProps_ToBroker": dataLoan.sU3TcProps_ToBroker = (setValue == "True"); break;
                case "sU3TcPaidTo": dataLoan.sU3TcPaidTo = setValue; break;
                case "sU3TcGfeSection": dataLoan.sU3TcGfeSection = (E_GfeSectionT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sU4TcDesc": dataLoan.sU4TcDesc = setValue; break;
                case "sU4Tc": dataLoan.sU4Tc_rep = setValue; break;
                case "sU4TcProps_PdByT": dataLoan.sU4TcProps_PdByT_rep = setValue; break;
                case "sU4TcProps_Apr": dataLoan.sU4TcProps_Apr = (setValue == "True"); break;
                case "sU4TcProps_FhaAllow": dataLoan.sU4TcProps_FhaAllow = (setValue == "True"); break;
                case "sU4TcProps_Poc": dataLoan.sU4TcProps_Poc = (setValue == "True"); break;
                case "sSettlementU4TcProps_Dflp": dataLoan.sSettlementU4TcProps_Dflp = (setValue == "True"); break;
                case "sU4TcProps_ToBroker": dataLoan.sU4TcProps_ToBroker = (setValue == "True"); break;
                case "sU4TcPaidTo": dataLoan.sU4TcPaidTo = setValue; break;
                case "sU4TcGfeSection": dataLoan.sU4TcGfeSection = (E_GfeSectionT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sRecFPc": dataLoan.sRecFPc_rep = setValue; break;
                case "sRecBaseT": dataLoan.sRecBaseT = (E_PercentBaseT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sRecFMb": dataLoan.sRecFMb_rep = setValue; break;
                case "sRecFLckd": dataLoan.sRecFLckd = (setValue == "True"); break;
                case "sRecDeed": dataLoan.sRecDeed_rep = setValue; break;
                case "sRecMortgage": dataLoan.sRecMortgage_rep = setValue; break;
                case "sRecRelease": dataLoan.sRecRelease_rep = setValue; break;
                case "sRecFProps_PdByT": dataLoan.sRecFProps_PdByT_rep = setValue; break;
                case "sRecFProps_Apr": dataLoan.sRecFProps_Apr = (setValue == "True"); break;
                case "sRecFProps_FhaAllow": dataLoan.sRecFProps_FhaAllow = (setValue == "True"); break;
                case "sRecFProps_Poc": dataLoan.sRecFProps_Poc = (setValue == "True"); break;
                case "sSettlementRecFProps_Dflp": dataLoan.sSettlementRecFProps_Dflp = (setValue == "True"); break;
                case "sRecFDesc": dataLoan.sRecFDesc = setValue; break;
                case "sCountyRtcPc": dataLoan.sCountyRtcPc_rep = setValue; break;
                case "sCountyRtcBaseT": dataLoan.sCountyRtcBaseT = (E_PercentBaseT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sCountyRtcMb": dataLoan.sCountyRtcMb_rep = setValue; break;
                case "sCountyRtcProps_PdByT": dataLoan.sCountyRtcProps_PdByT_rep = setValue; break;
                case "sCountyRtcProps_Apr": dataLoan.sCountyRtcProps_Apr = (setValue == "True"); break;
                case "sCountyRtcProps_FhaAllow": dataLoan.sCountyRtcProps_FhaAllow = (setValue == "True"); break;
                case "sCountyRtcProps_Poc": dataLoan.sCountyRtcProps_Poc = (setValue == "True"); break;
                case "sSettlementCountyRtcProps_Dflp": dataLoan.sSettlementCountyRtcProps_Dflp = (setValue == "True"); break;
                case "sCountyRtcDesc": dataLoan.sCountyRtcDesc = setValue; break;
                case "sStateRtcPc": dataLoan.sStateRtcPc_rep = setValue; break;
                case "sStateRtcBaseT": dataLoan.sStateRtcBaseT = (E_PercentBaseT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sStateRtcMb": dataLoan.sStateRtcMb_rep = setValue; break;
                case "sStateRtcProps_PdByT": dataLoan.sStateRtcProps_PdByT_rep = setValue; break;
                case "sStateRtcProps_Apr": dataLoan.sStateRtcProps_Apr = (setValue == "True"); break;
                case "sStateRtcProps_FhaAllow": dataLoan.sStateRtcProps_FhaAllow = (setValue == "True"); break;
                case "sStateRtcProps_Poc": dataLoan.sStateRtcProps_Poc = (setValue == "True"); break;
                case "sSettlementStateRtcProps_Dflp": dataLoan.sSettlementStateRtcProps_Dflp = (setValue == "True"); break;
                case "sStateRtcDesc": dataLoan.sStateRtcDesc = setValue; break;
                case "sU1GovRtcDesc": dataLoan.sU1GovRtcDesc = setValue; break;
                case "sU1GovRtcPc": dataLoan.sU1GovRtcPc_rep = setValue; break;
                case "sU1GovRtcBaseT": dataLoan.sU1GovRtcBaseT = (E_PercentBaseT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sU1GovRtcMb": dataLoan.sU1GovRtcMb_rep = setValue; break;
                case "sU1GovRtcGfeSection": dataLoan.sU1GovRtcGfeSection = (E_GfeSectionT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sU1GovRtcProps_PdByT": dataLoan.sU1GovRtcProps_PdByT_rep = setValue; break;
                case "sU1GovRtcProps_Apr": dataLoan.sU1GovRtcProps_Apr = (setValue == "True"); break;
                case "sU1GovRtcProps_FhaAllow": dataLoan.sU1GovRtcProps_FhaAllow = (setValue == "True"); break;
                case "sU1GovRtcProps_Poc": dataLoan.sU1GovRtcProps_Poc = (setValue == "True"); break;
                case "sSettlementU1GovRtcProps_Dflp": dataLoan.sSettlementU1GovRtcProps_Dflp = (setValue == "True"); break;
                case "sU1GovRtcPaidTo": dataLoan.sU1GovRtcPaidTo = setValue; break;
                case "sU2GovRtcDesc": dataLoan.sU2GovRtcDesc = setValue; break;
                case "sU2GovRtcPc": dataLoan.sU2GovRtcPc_rep = setValue; break;
                case "sU2GovRtcBaseT": dataLoan.sU2GovRtcBaseT = (E_PercentBaseT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sU2GovRtcMb": dataLoan.sU2GovRtcMb_rep = setValue; break;
                case "sU2GovRtcGfeSection": dataLoan.sU2GovRtcGfeSection = (E_GfeSectionT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sU2GovRtcProps_PdByT": dataLoan.sU2GovRtcProps_PdByT_rep = setValue; break;
                case "sU2GovRtcProps_Apr": dataLoan.sU2GovRtcProps_Apr = (setValue == "True"); break;
                case "sU2GovRtcProps_FhaAllow": dataLoan.sU2GovRtcProps_FhaAllow = (setValue == "True"); break;
                case "sU2GovRtcProps_Poc": dataLoan.sU2GovRtcProps_Poc = (setValue == "True"); break;
                case "sSettlementU2GovRtcProps_Dflp": dataLoan.sSettlementU2GovRtcProps_Dflp = (setValue == "True"); break;
                case "sU2GovRtcPaidTo": dataLoan.sU2GovRtcPaidTo = setValue; break;
                case "sU3GovRtcDesc": dataLoan.sU3GovRtcDesc = setValue; break;
                case "sU3GovRtcPc": dataLoan.sU3GovRtcPc_rep = setValue; break;
                case "sU3GovRtcBaseT": dataLoan.sU3GovRtcBaseT = (E_PercentBaseT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sU3GovRtcMb": dataLoan.sU3GovRtcMb_rep = setValue; break;
                case "sU3GovRtcGfeSection": dataLoan.sU3GovRtcGfeSection = (E_GfeSectionT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sU3GovRtcProps_PdByT": dataLoan.sU3GovRtcProps_PdByT_rep = setValue; break;
                case "sU3GovRtcProps_Apr": dataLoan.sU3GovRtcProps_Apr = (setValue == "True"); break;
                case "sU3GovRtcProps_FhaAllow": dataLoan.sU3GovRtcProps_FhaAllow = (setValue == "True"); break;
                case "sU3GovRtcProps_Poc": dataLoan.sU3GovRtcProps_Poc = (setValue == "True"); break;
                case "sSettlementU3GovRtcProps_Dflp": dataLoan.sSettlementU3GovRtcProps_Dflp = (setValue == "True"); break;
                case "sU3GovRtcPaidTo": dataLoan.sU3GovRtcPaidTo = setValue; break;
                case "sPestInspectF": dataLoan.sPestInspectF_rep = setValue; break;
                case "sPestInspectFProps_PdByT": dataLoan.sPestInspectFProps_PdByT_rep = setValue; break;
                case "sPestInspectFProps_Apr": dataLoan.sPestInspectFProps_Apr = (setValue == "True"); break;
                case "sPestInspectFProps_FhaAllow": dataLoan.sPestInspectFProps_FhaAllow = (setValue == "True"); break;
                case "sPestInspectFProps_Poc": dataLoan.sPestInspectFProps_Poc = (setValue == "True"); break;
                case "sSettlementPestInspectFProps_Dflp": dataLoan.sSettlementPestInspectFProps_Dflp = (setValue == "True"); break;
                case "sPestInspectFProps_ToBroker": dataLoan.sPestInspectFProps_ToBroker = (setValue == "True"); break;
                case "sPestInspectPaidTo": dataLoan.sPestInspectPaidTo = setValue; break;
                case "sU1ScDesc": dataLoan.sU1ScDesc = setValue; break;
                case "sU1Sc": dataLoan.sU1Sc_rep = setValue; break;
                case "sU1ScProps_PdByT": dataLoan.sU1ScProps_PdByT_rep = setValue; break;
                case "sU1ScProps_Apr": dataLoan.sU1ScProps_Apr = (setValue == "True"); break;
                case "sU1ScProps_FhaAllow": dataLoan.sU1ScProps_FhaAllow = (setValue == "True"); break;
                case "sU1ScProps_Poc": dataLoan.sU1ScProps_Poc = (setValue == "True"); break;
                case "sSettlementU1ScProps_Dflp": dataLoan.sSettlementU1ScProps_Dflp = (setValue == "True"); break;
                case "sU1ScProps_ToBroker": dataLoan.sU1ScProps_ToBroker = (setValue == "True"); break;
                case "sU1ScPaidTo": dataLoan.sU1ScPaidTo = setValue; break;
                case "sU1ScGfeSection": dataLoan.sU1ScGfeSection = (E_GfeSectionT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sU2ScDesc": dataLoan.sU2ScDesc = setValue; break;
                case "sU2Sc": dataLoan.sU2Sc_rep = setValue; break;
                case "sU2ScProps_PdByT": dataLoan.sU2ScProps_PdByT_rep = setValue; break;
                case "sU2ScProps_Apr": dataLoan.sU2ScProps_Apr = (setValue == "True"); break;
                case "sU2ScProps_FhaAllow": dataLoan.sU2ScProps_FhaAllow = (setValue == "True"); break;
                case "sU2ScProps_Poc": dataLoan.sU2ScProps_Poc = (setValue == "True"); break;
                case "sSettlementU2ScProps_Dflp": dataLoan.sSettlementU2ScProps_Dflp = (setValue == "True"); break;
                case "sU2ScProps_ToBroker": dataLoan.sU2ScProps_ToBroker = (setValue == "True"); break;
                case "sU2ScPaidTo": dataLoan.sU2ScPaidTo = setValue; break;
                case "sU2ScGfeSection": dataLoan.sU2ScGfeSection = (E_GfeSectionT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sU3ScDesc": dataLoan.sU3ScDesc = setValue; break;
                case "sU3Sc": dataLoan.sU3Sc_rep = setValue; break;
                case "sU3ScProps_PdByT": dataLoan.sU3ScProps_PdByT_rep = setValue; break;
                case "sU3ScProps_Apr": dataLoan.sU3ScProps_Apr = (setValue == "True"); break;
                case "sU3ScProps_FhaAllow": dataLoan.sU3ScProps_FhaAllow = (setValue == "True"); break;
                case "sU3ScProps_Poc": dataLoan.sU3ScProps_Poc = (setValue == "True"); break;
                case "sSettlementU3ScProps_Dflp": dataLoan.sSettlementU3ScProps_Dflp = (setValue == "True"); break;
                case "sU3ScProps_ToBroker": dataLoan.sU3ScProps_ToBroker = (setValue == "True"); break;
                case "sU3ScPaidTo": dataLoan.sU3ScPaidTo = setValue; break;
                case "sU3ScGfeSection": dataLoan.sU3ScGfeSection = (E_GfeSectionT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sU4ScDesc": dataLoan.sU4ScDesc = setValue; break;
                case "sU4Sc": dataLoan.sU4Sc_rep = setValue; break;
                case "sU4ScProps_PdByT": dataLoan.sU4ScProps_PdByT_rep = setValue; break;
                case "sU4ScProps_Apr": dataLoan.sU4ScProps_Apr = (setValue == "True"); break;
                case "sU4ScProps_FhaAllow": dataLoan.sU4ScProps_FhaAllow = (setValue == "True"); break;
                case "sU4ScProps_Poc": dataLoan.sU4ScProps_Poc = (setValue == "True"); break;
                case "sSettlementU4ScProps_Dflp": dataLoan.sSettlementU4ScProps_Dflp = (setValue == "True"); break;
                case "sU4ScProps_ToBroker": dataLoan.sU4ScProps_ToBroker = (setValue == "True"); break;
                case "sU4ScPaidTo": dataLoan.sU4ScPaidTo = setValue; break;
                case "sU4ScGfeSection": dataLoan.sU4ScGfeSection = (E_GfeSectionT) dataLoan.m_convertLos.ToCount(setValue); break;
                case "sU5ScDesc": dataLoan.sU5ScDesc = setValue; break;
                case "sU5Sc": dataLoan.sU5Sc_rep = setValue; break;
                case "sU5ScProps_PdByT": dataLoan.sU5ScProps_PdByT_rep = setValue; break;
                case "sU5ScProps_Apr": dataLoan.sU5ScProps_Apr = (setValue == "True"); break;
                case "sU5ScProps_FhaAllow": dataLoan.sU5ScProps_FhaAllow = (setValue == "True"); break;
                case "sU5ScProps_Poc": dataLoan.sU5ScProps_Poc = (setValue == "True"); break;
                case "sSettlementU5ScProps_Dflp": dataLoan.sSettlementU5ScProps_Dflp = (setValue == "True"); break;
                case "sU5ScProps_ToBroker": dataLoan.sU5ScProps_ToBroker = (setValue == "True"); break;
                case "sU5ScPaidTo": dataLoan.sU5ScPaidTo = setValue; break;
                case "sU5ScGfeSection": dataLoan.sU5ScGfeSection = (E_GfeSectionT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sDaysInYr": dataLoan.sDaysInYr_rep = setValue; break;

                // OPM 198239 - Lender Credits Fields
                case "sLenderCreditCalculationMethodT": dataLoan.sLenderCreditCalculationMethodT = (E_sLenderCreditCalculationMethodT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sGfeCreditLenderPaidItemT": dataLoan.sGfeCreditLenderPaidItemT = (E_CreditLenderPaidItemT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sLenderCustomCredit1DiscloseLocationT": dataLoan.sLenderCustomCredit1DiscloseLocationT = (E_LenderCreditDiscloseLocationT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sLenderCustomCredit1Description": dataLoan.sLenderCustomCredit1Description = setValue; break;
                case "sLenderCustomCredit1Amount": dataLoan.sLenderCustomCredit1Amount_rep = setValue; break;
                case "sLenderCustomCredit2DiscloseLocationT": dataLoan.sLenderCustomCredit2DiscloseLocationT = (E_LenderCreditDiscloseLocationT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sLenderCustomCredit2Description": dataLoan.sLenderCustomCredit2Description = setValue; break;
                case "sLenderCustomCredit2Amount": dataLoan.sLenderCustomCredit2Amount_rep = setValue; break;
                //case "sToleranceCureCalculationT": dataLoan.sToleranceCureCalculationT = (E_sToleranceCureCalculationT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sLenderCreditMaxT": dataLoan.sLenderCreditMaxT = (E_sLenderCreditMaxT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sLenderPaidFeeDiscloseLocationT": dataLoan.sLenderPaidFeeDiscloseLocationT = (E_LenderCreditDiscloseLocationT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sLenderGeneralCreditDiscloseLocationT": dataLoan.sLenderGeneralCreditDiscloseLocationT = (E_LenderCreditDiscloseLocationT)dataLoan.m_convertLos.ToCount(setValue); break;

                // OPM 217028 - Aggregate Escrow Calculation Fields
                case "sAggEscrowCalcModeT": dataLoan.sAggEscrowCalcModeT = (E_AggregateEscrowCalculationModeT)dataLoan.m_convertLos.ToCount(setValue); break;
                case "sCustomaryEscrowImpoundsCalcMinT": dataLoan.sCustomaryEscrowImpoundsCalcMinT = (E_CustomaryEscrowImpoundsCalcMinT)dataLoan.m_convertLos.ToCount(setValue); break;

                #region QM FIELDS
                // OPM 145229 - QM FIELDS
                case "sLOrigFProps_PaidToThirdParty": dataLoan.sLOrigFProps_PaidToThirdParty = (setValue == "True"); break;
                case "sLOrigFProps_ThisPartyIsAffiliate": dataLoan.sLOrigFProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sGfeDiscountPointFProps_PaidToThirdParty": dataLoan.sGfeDiscountPointFProps_PaidToThirdParty = (setValue == "True"); break;
                case "sGfeDiscountPointFProps_ThisPartyIsAffiliate": dataLoan.sGfeDiscountPointFProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sApprFProps_PaidToThirdParty": dataLoan.sApprFProps_PaidToThirdParty = (setValue == "True"); break;
                case "sApprFProps_ThisPartyIsAffiliate": dataLoan.sApprFProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sCrFProps_PaidToThirdParty": dataLoan.sCrFProps_PaidToThirdParty = (setValue == "True"); break;
                case "sCrFProps_ThisPartyIsAffiliate": dataLoan.sCrFProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sTxServFProps_PaidToThirdParty": dataLoan.sTxServFProps_PaidToThirdParty = (setValue == "True"); break;
                case "sTxServFProps_ThisPartyIsAffiliate": dataLoan.sTxServFProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sFloodCertificationFProps_PaidToThirdParty": dataLoan.sFloodCertificationFProps_PaidToThirdParty = (setValue == "True"); break;
                case "sFloodCertificationFProps_ThisPartyIsAffiliate": dataLoan.sFloodCertificationFProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sMBrokFProps_PaidToThirdParty": dataLoan.sMBrokFProps_PaidToThirdParty = (setValue == "True"); break;
                case "sMBrokFProps_ThisPartyIsAffiliate": dataLoan.sMBrokFProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sInspectFProps_PaidToThirdParty": dataLoan.sInspectFProps_PaidToThirdParty = (setValue == "True"); break;
                case "sInspectFProps_ThisPartyIsAffiliate": dataLoan.sInspectFProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sProcFProps_PaidToThirdParty": dataLoan.sProcFProps_PaidToThirdParty = (setValue == "True"); break;
                case "sProcFProps_ThisPartyIsAffiliate": dataLoan.sProcFProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sUwFProps_PaidToThirdParty": dataLoan.sUwFProps_PaidToThirdParty = (setValue == "True"); break;
                case "sUwFProps_ThisPartyIsAffiliate": dataLoan.sUwFProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sWireFProps_PaidToThirdParty": dataLoan.sWireFProps_PaidToThirdParty = (setValue == "True"); break;
                case "sWireFProps_ThisPartyIsAffiliate": dataLoan.sWireFProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "s800U1FProps_PaidToThirdParty": dataLoan.s800U1FProps_PaidToThirdParty = (setValue == "True"); break;
                case "s800U1FProps_ThisPartyIsAffiliate": dataLoan.s800U1FProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "s800U2FProps_PaidToThirdParty": dataLoan.s800U2FProps_PaidToThirdParty = (setValue == "True"); break;
                case "s800U2FProps_ThisPartyIsAffiliate": dataLoan.s800U2FProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "s800U3FProps_PaidToThirdParty": dataLoan.s800U3FProps_PaidToThirdParty = (setValue == "True"); break;
                case "s800U3FProps_ThisPartyIsAffiliate": dataLoan.s800U3FProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "s800U4FProps_PaidToThirdParty": dataLoan.s800U4FProps_PaidToThirdParty = (setValue == "True"); break;
                case "s800U4FProps_ThisPartyIsAffiliate": dataLoan.s800U4FProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "s800U5FProps_PaidToThirdParty": dataLoan.s800U5FProps_PaidToThirdParty = (setValue == "True"); break;
                case "s800U5FProps_ThisPartyIsAffiliate": dataLoan.s800U5FProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sIPiaProps_PaidToThirdParty": dataLoan.sIPiaProps_PaidToThirdParty = (setValue == "True"); break;
                case "sIPiaProps_ThisPartyIsAffiliate": dataLoan.sIPiaProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sMipPiaProps_PaidToThirdParty": dataLoan.sMipPiaProps_PaidToThirdParty = (setValue == "True"); break;
                case "sMipPiaProps_ThisPartyIsAffiliate": dataLoan.sMipPiaProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sHazInsPiaProps_PaidToThirdParty": dataLoan.sHazInsPiaProps_PaidToThirdParty = (setValue == "True"); break;
                case "sHazInsPiaProps_ThisPartyIsAffiliate": dataLoan.sHazInsPiaProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "s904PiaProps_PaidToThirdParty": dataLoan.s904PiaProps_PaidToThirdParty = (setValue == "True"); break;
                case "s904PiaProps_ThisPartyIsAffiliate": dataLoan.s904PiaProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sVaFfProps_PaidToThirdParty": dataLoan.sVaFfProps_PaidToThirdParty = (setValue == "True"); break;
                case "sVaFfProps_ThisPartyIsAffiliate": dataLoan.sVaFfProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "s900U1PiaProps_PaidToThirdParty": dataLoan.s900U1PiaProps_PaidToThirdParty = (setValue == "True"); break;
                case "s900U1PiaProps_ThisPartyIsAffiliate": dataLoan.s900U1PiaProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sHazInsRsrvProps_PaidToThirdParty": dataLoan.sHazInsRsrvProps_PaidToThirdParty = (setValue == "True"); break;
                case "sHazInsRsrvProps_ThisPartyIsAffiliate": dataLoan.sHazInsRsrvProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sMInsRsrvProps_PaidToThirdParty": dataLoan.sMInsRsrvProps_PaidToThirdParty = (setValue == "True"); break;
                case "sMInsRsrvProps_ThisPartyIsAffiliate": dataLoan.sMInsRsrvProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sMInsRsrvEscrowedTri": dataLoan.sMInsRsrvEscrowedTri = (E_TriState)int.Parse(setValue); break;
                case "sRealETxRsrvProps_PaidToThirdParty": dataLoan.sRealETxRsrvProps_PaidToThirdParty = (setValue == "True"); break;
                case "sRealETxRsrvProps_ThisPartyIsAffiliate": dataLoan.sRealETxRsrvProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sSchoolTxRsrvProps_PaidToThirdParty": dataLoan.sSchoolTxRsrvProps_PaidToThirdParty = (setValue == "True"); break;
                case "sSchoolTxRsrvProps_ThisPartyIsAffiliate": dataLoan.sSchoolTxRsrvProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sSchoolTxRsrvEscrowedTri": dataLoan.sSchoolTxRsrvEscrowedTri = (E_TriState)int.Parse(setValue); break;
                case "sFloodInsRsrvProps_PaidToThirdParty": dataLoan.sFloodInsRsrvProps_PaidToThirdParty = (setValue == "True"); break;
                case "sFloodInsRsrvProps_ThisPartyIsAffiliate": dataLoan.sFloodInsRsrvProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sAggregateAdjRsrvProps_PaidToThirdParty": dataLoan.sAggregateAdjRsrvProps_PaidToThirdParty = (setValue == "True"); break;
                case "sAggregateAdjRsrvProps_ThisPartyIsAffiliate": dataLoan.sAggregateAdjRsrvProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "s1006RsrvProps_PaidToThirdParty": dataLoan.s1006RsrvProps_PaidToThirdParty = (setValue == "True"); break;
                case "s1006RsrvProps_ThisPartyIsAffiliate": dataLoan.s1006RsrvProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "s1007RsrvProps_PaidToThirdParty": dataLoan.s1007RsrvProps_PaidToThirdParty = (setValue == "True"); break;
                case "s1007RsrvProps_ThisPartyIsAffiliate": dataLoan.s1007RsrvProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sU3RsrvProps_PaidToThirdParty": dataLoan.sU3RsrvProps_PaidToThirdParty = (setValue == "True"); break;
                case "sU3RsrvProps_ThisPartyIsAffiliate": dataLoan.sU3RsrvProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sU4RsrvProps_PaidToThirdParty": dataLoan.sU4RsrvProps_PaidToThirdParty = (setValue == "True"); break;
                case "sU4RsrvProps_ThisPartyIsAffiliate": dataLoan.sU4RsrvProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sEscrowFProps_PaidToThirdParty": dataLoan.sEscrowFProps_PaidToThirdParty = (setValue == "True"); break;
                case "sEscrowFProps_ThisPartyIsAffiliate": dataLoan.sEscrowFProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sOwnerTitleInsProps_PaidToThirdParty": dataLoan.sOwnerTitleInsProps_PaidToThirdParty = (setValue == "True"); break;
                case "sOwnerTitleInsProps_ThisPartyIsAffiliate": dataLoan.sOwnerTitleInsProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sTitleInsFProps_PaidToThirdParty": dataLoan.sTitleInsFProps_PaidToThirdParty = (setValue == "True"); break;
                case "sTitleInsFProps_ThisPartyIsAffiliate": dataLoan.sTitleInsFProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sDocPrepFProps_PaidToThirdParty": dataLoan.sDocPrepFProps_PaidToThirdParty = (setValue == "True"); break;
                case "sDocPrepFProps_ThisPartyIsAffiliate": dataLoan.sDocPrepFProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sNotaryFProps_PaidToThirdParty": dataLoan.sNotaryFProps_PaidToThirdParty = (setValue == "True"); break;
                case "sNotaryFProps_ThisPartyIsAffiliate": dataLoan.sNotaryFProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sAttorneyFProps_PaidToThirdParty": dataLoan.sAttorneyFProps_PaidToThirdParty = (setValue == "True"); break;
                case "sAttorneyFProps_ThisPartyIsAffiliate": dataLoan.sAttorneyFProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sU1TcProps_PaidToThirdParty": dataLoan.sU1TcProps_PaidToThirdParty = (setValue == "True"); break;
                case "sU1TcProps_ThisPartyIsAffiliate": dataLoan.sU1TcProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sU2TcProps_PaidToThirdParty": dataLoan.sU2TcProps_PaidToThirdParty = (setValue == "True"); break;
                case "sU2TcProps_ThisPartyIsAffiliate": dataLoan.sU2TcProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sU3TcProps_PaidToThirdParty": dataLoan.sU3TcProps_PaidToThirdParty = (setValue == "True"); break;
                case "sU3TcProps_ThisPartyIsAffiliate": dataLoan.sU3TcProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sU4TcProps_PaidToThirdParty": dataLoan.sU4TcProps_PaidToThirdParty = (setValue == "True"); break;
                case "sU4TcProps_ThisPartyIsAffiliate": dataLoan.sU4TcProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sRecFProps_PaidToThirdParty": dataLoan.sRecFProps_PaidToThirdParty = (setValue == "True"); break;
                case "sRecFProps_ThisPartyIsAffiliate": dataLoan.sRecFProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sCountyRtcProps_PaidToThirdParty": dataLoan.sCountyRtcProps_PaidToThirdParty = (setValue == "True"); break;
                case "sCountyRtcProps_ThisPartyIsAffiliate": dataLoan.sCountyRtcProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sStateRtcProps_PaidToThirdParty": dataLoan.sStateRtcProps_PaidToThirdParty = (setValue == "True"); break;
                case "sStateRtcProps_ThisPartyIsAffiliate": dataLoan.sStateRtcProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sU1GovRtcProps_PaidToThirdParty": dataLoan.sU1GovRtcProps_PaidToThirdParty = (setValue == "True"); break;
                case "sU1GovRtcProps_ThisPartyIsAffiliate": dataLoan.sU1GovRtcProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sU2GovRtcProps_PaidToThirdParty": dataLoan.sU2GovRtcProps_PaidToThirdParty = (setValue == "True"); break;
                case "sU2GovRtcProps_ThisPartyIsAffiliate": dataLoan.sU2GovRtcProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sU3GovRtcProps_PaidToThirdParty": dataLoan.sU3GovRtcProps_PaidToThirdParty = (setValue == "True"); break;
                case "sU3GovRtcProps_ThisPartyIsAffiliate": dataLoan.sU3GovRtcProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sPestInspectFProps_PaidToThirdParty": dataLoan.sPestInspectFProps_PaidToThirdParty = (setValue == "True"); break;
                case "sPestInspectFProps_ThisPartyIsAffiliate": dataLoan.sPestInspectFProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sU1ScProps_PaidToThirdParty": dataLoan.sU1ScProps_PaidToThirdParty = (setValue == "True"); break;
                case "sU1ScProps_ThisPartyIsAffiliate": dataLoan.sU1ScProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sU2ScProps_PaidToThirdParty": dataLoan.sU2ScProps_PaidToThirdParty = (setValue == "True"); break;
                case "sU2ScProps_ThisPartyIsAffiliate": dataLoan.sU2ScProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sU3ScProps_PaidToThirdParty": dataLoan.sU3ScProps_PaidToThirdParty = (setValue == "True"); break;
                case "sU3ScProps_ThisPartyIsAffiliate": dataLoan.sU3ScProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sU4ScProps_PaidToThirdParty": dataLoan.sU4ScProps_PaidToThirdParty = (setValue == "True"); break;
                case "sU4ScProps_ThisPartyIsAffiliate": dataLoan.sU4ScProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                case "sU5ScProps_PaidToThirdParty": dataLoan.sU5ScProps_PaidToThirdParty = (setValue == "True"); break;
                case "sU5ScProps_ThisPartyIsAffiliate": dataLoan.sU5ScProps_ThisPartyIsAffiliate = (setValue == "True"); break;
                #endregion

                case "sHazInsRsrvEscrowCushion": dataLoan.SetEscrowItemCushion(E_EscrowItemT.HazardInsurance, setValue, false); updateEscrowAccount = true; break;
                case "sMInsRsrvEscrowCushion": dataLoan.SetEscrowItemCushion(E_EscrowItemT.MortgageInsurance, setValue, false); updateEscrowAccount = true; break;
                case "sRealETxRsrvEscrowCushion": dataLoan.SetEscrowItemCushion(E_EscrowItemT.RealEstateTax, setValue, false); updateEscrowAccount = true; break;
                case "sSchoolTxRsrvEscrowCushion": dataLoan.SetEscrowItemCushion(E_EscrowItemT.SchoolTax, setValue, false); updateEscrowAccount = true; break;
                case "sFloodInsRsrvEscrowCushion": dataLoan.SetEscrowItemCushion(E_EscrowItemT.FloodInsurance, setValue, false); updateEscrowAccount = true; break;
                case "s1006RsrvEscrowCushion": dataLoan.SetEscrowItemCushion(E_EscrowItemT.UserDefine1, setValue, false); updateEscrowAccount = true; break;
                case "s1007RsrvEscrowCushion": dataLoan.SetEscrowItemCushion(E_EscrowItemT.UserDefine2, setValue, false); updateEscrowAccount = true; break;
                case "sU3RsrvEscrowCushion": dataLoan.SetEscrowItemCushion(E_EscrowItemT.UserDefine3, setValue, false); updateEscrowAccount = true; break;
                case "sU4RsrvEscrowCushion": dataLoan.SetEscrowItemCushion(E_EscrowItemT.UserDefine4, setValue, false); updateEscrowAccount = true; break;

                case "sHazInsRsrvEscrowDisbursementSchedule": dataLoan.SetEscrowItemDisbursementSchedule(E_EscrowItemT.HazardInsurance, setValue, false); updateEscrowAccount = true; break;
                case "sMInsRsrvEscrowDisbursementSchedule": dataLoan.SetEscrowItemDisbursementSchedule(E_EscrowItemT.MortgageInsurance, setValue, false); updateEscrowAccount = true; break;
                case "sRealETxRsrvEscrowDisbursementSchedule": dataLoan.SetEscrowItemDisbursementSchedule(E_EscrowItemT.RealEstateTax, setValue, false); updateEscrowAccount = true; break;
                case "sSchoolTxRsrvEscrowDisbursementSchedule": dataLoan.SetEscrowItemDisbursementSchedule(E_EscrowItemT.SchoolTax, setValue, false); updateEscrowAccount = true; break;
                case "sFloodInsRsrvEscrowDisbursementSchedule": dataLoan.SetEscrowItemDisbursementSchedule(E_EscrowItemT.FloodInsurance, setValue, false); updateEscrowAccount = true; break;
                case "s1006RsrvEscrowDisbursementSchedule": dataLoan.SetEscrowItemDisbursementSchedule(E_EscrowItemT.UserDefine1, setValue, false); updateEscrowAccount = true; break;
                case "s1007RsrvEscrowDisbursementSchedule": dataLoan.SetEscrowItemDisbursementSchedule(E_EscrowItemT.UserDefine2, setValue, false); updateEscrowAccount = true; break;
                case "sU3RsrvEscrowDisbursementSchedule": dataLoan.SetEscrowItemDisbursementSchedule(E_EscrowItemT.UserDefine3, setValue, false); updateEscrowAccount = true; break;
                case "sU4RsrvEscrowDisbursementSchedule": dataLoan.SetEscrowItemDisbursementSchedule(E_EscrowItemT.UserDefine4, setValue, false); updateEscrowAccount = true; break;

                // SPECIALS:

                // These ones are part of the run, not loan-file specific
                case "sPricingEngineCostT": specialFields.Add(fieldId, setValue); updateHistory = false; break;
                case "sPricingEngineCreditT": specialFields.Add(fieldId, setValue); updateHistory = false; break;
                case "sPricingEngineLimitCreditToT": specialFields.Add(fieldId, setValue); updateHistory = false; break;
                
                // These ones are used for loan data via a calculation.
                case "sEscrowFPc":
                case "sEscrowFBaseT":
                case "sEscrowFMb":
                case "sOwnerTitleInsFPc":
                case "sOwnerTitleInsFBaseT":
                case "sOwnerTitleInsFMb":
                case "sTitleInsFPc":
                case "sTitleInsFBaseT":
                case "sTitleInsFMb":
                    specialFields.Add(fieldId, setValue);
                    break;

                default:
                    throw new NotFoundException("Fee service encountered an unexpected field id.", "Unknown fee service field: " + fieldId);
            }
        }

        /// <summary>
        /// Adds the settlement service provider to the specified <paramref name="dataLoan"/> using
        /// the system ID from the specified <paramref name="templateField"/>.
        /// </summary>
        /// <param name="dataLoan">
        /// The loan to add the settlement service provider.
        /// </param>
        /// <param name="cachedContactEntriesBySystemId">
        /// The broker's settlement service provider contacts by system ID.
        /// </param>
        /// <param name="templateField">
        /// The template field containing the settlement service provider data.
        /// </param>
        /// <returns>
        /// True if the settlement service provider was added to the loan file,
        /// false if the settlement service provider was not found due to being disabled or deleted.
        /// </returns>
        private static bool AddSettlementServiceProvider(CPageData dataLoan, Lazy<AvailableSettlementServiceProviders> availableSettlementServiceProviders, Dictionary<string, RolodexDB> cachedContactEntriesBySystemId, FeeServiceTemplate.TemplateField templateField)
        {
            RolodexDB contact = null;
            if (!cachedContactEntriesBySystemId.TryGetValue(templateField.Value, out contact))
            {
                return false;
            }

            if (!availableSettlementServiceProviders.Value.IsFeeTypeAvailableInProviderList(templateField.TypeId))
            {
                availableSettlementServiceProviders.Value.AddFeeType(templateField.TypeId);
            }

            var provider = new SettlementServiceProvider(contact);
            provider.IsSystemGenerated = true;
            availableSettlementServiceProviders.Value.AddProviderForFeeType(templateField.TypeId, provider);

            return true;
        }

        private static HashSet<string> GetProtectedFields(CPageData dataLoan)
        {
            HashSet<string> protectedList = new HashSet<string>();

            // OPM 244915. For pricing state pricing of old-style rules,
            // if we know these values were set via fee service, we can 
            // still clobber existing value with new fee service value.
            // Otherwise, use non-0 as the indicator if we keep it.
            if ((dataLoan.CurrentPricingState?.sProRealETxLocked) ?? dataLoan.sProRealETx != 0)
            {
                protectedList.Add("sProRealETxR");
                protectedList.Add("sProRealETxT");
                protectedList.Add("sProRealETxMb");
            }

            if ((dataLoan.CurrentPricingState?.sProHazInsLocked) ?? dataLoan.sProHazIns != 0)
            {
                protectedList.Add("sProHazInsR");
                protectedList.Add("sProHazInsT");
                protectedList.Add("sProHazInsMb");
            }
            
            return protectedList;
        }

        private static bool AllowFieldToBeSet(FeeServiceTemplate.TemplateField templateField, List<FeeConditionHit> feeSetHistory, HashSet<string> protectedList, FeeServiceMatchingTemplateData matchingData, bool alwaysSet801, bool alwaysSet802, bool lenderCreditFromRateLock,
                                              CPageData dataLoan, Dictionary<E_HousingExpenseTypeT, decimal> previousExpenseTotal, Dictionary<E_HousingExpenseTypeT,bool> housingLocks)
        {
            string fieldKey = templateField.Key;

            if (protectedList.Contains(fieldKey))
                return false; // The loan file should not take this one.

            // OPM 209852 - Do not set sPricingEngineCostT, sPricingEngineCreditT, or sPricingEngineLimitCreditToT when sLenderCreditCalculationMethodT == Calculate from Front-end Rate Lock
            if (lenderCreditFromRateLock && (fieldKey == "sPricingEngineCostT" || fieldKey == "sPricingEngineCreditT" || fieldKey == "sPricingEngineLimitCreditToT"))
            {
                return false;
            }

            // Per 179391. 801/802 sometimes go.
            int lineNum = AvailableFeeServiceFields.GetHudLineFromFieldId(fieldKey);
            if ((lineNum == 802 && alwaysSet802) || (lineNum == 801 && alwaysSet801))
            {
                return true;
            }

            // OPM 227987 - Check if loan is set to stomp all fees.
            if (dataLoan.sClosingCostAutomationUpdateT == E_sClosingCostAutomationUpdateT.UpdateUnconditionally)
            {
                return true;
            }

            // If the TemplateField is any of these properties, then we have to check if the expense's previous total is non-zero. If it is, then don't stomp on the old values.
            if (AvailableFeeServiceFields.HousingExpensesFeeTypeIds.Contains(templateField.TypeId) &&
                (templateField.FeeProperty == E_FeeServiceFeePropertyT.Percent || templateField.FeeProperty == E_FeeServiceFeePropertyT.BaseValue || templateField.FeeProperty == E_FeeServiceFeePropertyT.Amount))
            {
                BaseHousingExpense expense = GetHousingExpenseForFeeService(dataLoan, templateField.TypeId);

                // If we come across a template field where the associated expense's previous total hasn't been loaded in yet, then load it in.
                // The previous monthly total should not have changed at this point since only these 3 properties can change it.
                if (!previousExpenseTotal.ContainsKey(expense.HousingExpenseType))
                {
                    previousExpenseTotal.Add(expense.HousingExpenseType, expense.GetMonthlyAmtTotal());
                }

                bool explicitLock;
                if (housingLocks.TryGetValue(expense.HousingExpenseType, out explicitLock))
                {
                    // If this is a pin run and it was pinned using loan data for this expense,
                    // we cannot allow the fee service to stomp on it per OPM 244915.
                    return !explicitLock;
                }

                // Check if the previous total is non-zero. Don't stomp if it is.
                if (previousExpenseTotal[expense.HousingExpenseType] != 0)
                {
                    return false;
                }
            }

            // The one that made this value set.
            var previousSettingCondition = feeSetHistory.FirstOrDefault(cond => cond.FieldIds.Any(GetConditionCheckPredicate(templateField)));
            return CheckConditionsChanged(previousSettingCondition, matchingData);
        }

        // OPM 457137 - Removing a fee should count as "setting" that fee's fields to blank, and setting a fee should
        // count as setting a removed field to a non-blank value. So, for closing cost rules, we need to check if
        // field has ever been either removed or set.
        private static Func<string, bool> GetConditionCheckPredicate(FeeServiceTemplate.TemplateField templateField)
        {
            Func<string, bool> predicate;
            if (templateField.RuleType == FeeServiceRuleType.ClosingCost)
            {
                if (templateField.FeeProperty == E_FeeServiceFeePropertyT.Remove)
                {
                    predicate = id => id == templateField.Key || id.StartsWith(templateField.TypeId.ToString());
                }
                else
                {
                    // Search for fee removal and self.
                    string removeKey = templateField.TypeId.ToString() + ":" + E_FeeServiceFeePropertyT.Remove.ToString();
                    predicate = id => id == templateField.Key || id == removeKey;
                }
            }
            else
            {
                predicate = id => id == templateField.Key;
            }

            return predicate;
        }

        private static bool CheckConditionsChanged(FeeConditionHit previousSettingCondition, FeeServiceMatchingTemplateData matchingData)
        {
            // Condition has never been run. Go ahead and stomp.
            if (previousSettingCondition == null)
            {
                return true;
            }

            string previousCondition = previousSettingCondition.SetCondition;

            // This field was set by this ID in the past, but was applied unconditionally.
            // Fee Service should only set this field once once, and block any further attempts to set.
            if (previousCondition == "<FeeServiceConditionSet />"
                || previousCondition == "<?xml version=\"1.0\"?>" + Environment.NewLine + "<FeeServiceConditionSet />")
            {
                return false;
            }

            // This field was set by this ID in the past, the only way we can allow a clobber
            // is if the condition that set it is no longer true.
            if (FeeServiceConditionSet.EvaluateSerializedConditions(previousCondition, matchingData))
            {
                // The last condition that set this field is still true.  Use loan file value.
                return false;
            }
            else
            {
                // Previous setting condition no longer true.  Clobber, but notify.
                return true;
            }
        }

        private static void SetCompositeField( Action<decimal> setValue, CPageData dataLoan, Dictionary<string, string> specialFields, string percentFieldNm, string baseTypeFieldNm, string flatAmountFieldNm)
        {
            // PER OPM 143369 We set these dangerous defaults and use them if a fee service rule does not set them.
            decimal percent = 0.000m;
            E_PercentBaseT baseType = E_PercentBaseT.LoanAmount;
            decimal flatAmount = 0m;

            bool setField = false;
            if (specialFields.Keys.Contains(percentFieldNm))
            {
                setField = true;
                percent = dataLoan.m_convertLos.ToRate(specialFields[percentFieldNm]);
            }
            if (specialFields.Keys.Contains( baseTypeFieldNm) )
            {
                setField = true;
                baseType = (E_PercentBaseT)dataLoan.m_convertLos.ToCount(specialFields[baseTypeFieldNm]);
            }

            if (specialFields.Keys.Contains( flatAmountFieldNm))
            {
                setField = true;
                flatAmount = dataLoan.m_convertLos.ToMoney(specialFields[flatAmountFieldNm]);
            }

            if (setField)
            {
                setValue.Invoke(dataLoan.CalculateTotalAmountForCC(percent, baseType, flatAmount));
            }
            
        }

        private static void SetCompositeFields(CPageData dataLoan, Dictionary<string, string> specialFields)
        {
            // These are a few special fields where three values on a CC template
            // computes to one value on the loan file.

            SetCompositeField(value => dataLoan.sTitleInsF = value, dataLoan, specialFields, "sTitleInsFPc", "sTitleInsFBaseT", "sTitleInsFMb");
            SetCompositeField(value => dataLoan.sOwnerTitleInsF = value, dataLoan, specialFields, "sOwnerTitleInsFPc", "sOwnerTitleInsFBaseT", "sOwnerTitleInsFMb");
            SetCompositeField(value => dataLoan.sEscrowF = value, dataLoan, specialFields, "sEscrowFPc", "sEscrowFBaseT", "sEscrowFMb");
        }

        public static void RecalculateConditions(CPageData dataLoan)
        {
            bool originalBypass = dataLoan.ByPassFieldSecurityCheck;
            dataLoan.ByPassFieldSecurityCheck = true;

            try
            {
                FeeServiceRevision feeService = GetCurrentRevision(dataLoan.sBrokerId, dataLoan.sTestLoanFileEnvironment.EnableFeeServiceTest);
                if (feeService == null) return; // Should not be possible unless there is some way to clear fee service fees in the future.

                ClosingCostApplicationHistory feeServiceHistory = new ClosingCostApplicationHistory(dataLoan.sFeeServiceApplicationHistoryXmlContent);
                RecalculateConditionsImpl(dataLoan, feeService, feeServiceHistory);
            }
            finally
            {
                dataLoan.ByPassFieldSecurityCheck = originalBypass;
            }
        }        

        private static void RecalculateConditionsImpl(CPageData dataLoan, FeeServiceRevision feeService, ClosingCostApplicationHistory feeHistory )
        {
            var matchingData = new FeeServiceMatchingTemplateData()
            {
                sBrokerId = dataLoan.sBrokerId,
                sLPurposeT = dataLoan.sLPurposeT,
                sFinMethT = dataLoan.sFinMethT,
                sLienPosT = dataLoan.sLienPosT,
                aOccT = dataLoan.GetAppData(0).aOccT,
                sSpState = dataLoan.sSpState,
                sLT = dataLoan.sLT,
                sBranchChannelT = dataLoan.sBranchChannelT,
                lLpProductType = dataLoan.sLpProductType,
                lLpInvestorNm = dataLoan.sLpInvestorNm,
                sBranchId = dataLoan.sBranchId,
                sProdSpT = dataLoan.sProdSpT,
                sApprVal = dataLoan.sApprVal == 0 ? dataLoan.sPurchPrice : dataLoan.sApprVal,
                sFinalLAmt = dataLoan.sFinalLAmt,
                sLAmt = dataLoan.sLAmtCalc,
                sPurchPrice = dataLoan.sPurchPrice,
                sProdImpound = dataLoan.sProdImpound,
                sLenderFeeBuyoutRequestedT = dataLoan.sLenderFeeBuyoutRequestedT,
                sLeadSrcId = dataLoan.sLeadSrcId,
                sCorrespondentProcessT = dataLoan.sCorrespondentProcessT,
                sPmlCompanyTierId = dataLoan.sPmlCompanyTierId,
                sDisclosureRegulationT = dataLoan.sDisclosureRegulationT,
                sSpCountyFips = dataLoan.sSpCountyFips,
                sIsNewConstruction = dataLoan.sIsNewConstruction
            };

            var applicableTemplates = feeService.GetApplicableFeeTemplates(matchingData);

            // These guys have no line number, but we use value from application, or default.  Not part of history.
            string[] noHistoryFields = { "sPricingEngineCostT", "sPricingEngineCreditT", "sPricingEngineLimitCreditToT" };

            // For each fee that would have been set by this run, 
            // we set the history condition to that one. ( but leave value alone ).
            foreach (var template in applicableTemplates)
            {
                string settingXml = template.GetSerializedConditions();

                foreach (var templateField in template.GetTemplateFields())
                {
                    
                    if (noHistoryFields.Contains(templateField.FieldId) == false )
                        UpdateFeeHistory(feeHistory.FeeSetHistory, templateField, settingXml);
                }
            }

            if (feeService.Id.HasValue)
                feeHistory.RevisionId = feeService.Id.Value;

            dataLoan.sFeeServiceApplicationHistoryXmlContent = feeHistory.ToXml();
        }

        public static bool AreAllConditionsTrue(CPageData dataLoan)
        {
            bool originalBypass = dataLoan.ByPassFieldSecurityCheck;
            dataLoan.ByPassFieldSecurityCheck = true;

            try
            {
                
                var matchingData = new FeeServiceMatchingTemplateData()
                {
                    sBrokerId = dataLoan.sBrokerId,
                    sLPurposeT = dataLoan.sLPurposeT,
                    sFinMethT = dataLoan.sFinMethT,
                    sLienPosT = dataLoan.sLienPosT,
                    aOccT = dataLoan.GetAppData(0).aOccT,
                    sSpState = dataLoan.sSpState,
                    sLT = dataLoan.sLT,
                    sBranchChannelT = dataLoan.sBranchChannelT,
                    lLpProductType = dataLoan.sLpProductType,
                    lLpInvestorNm = dataLoan.sLpInvestorNm,
                    sBranchId = dataLoan.sBranchId,
                    sProdSpT = dataLoan.sProdSpT,
                    sApprVal = dataLoan.sApprVal == 0 ? dataLoan.sPurchPrice : dataLoan.sApprVal,
                    sFinalLAmt = dataLoan.sFinalLAmt,
                    sLAmt = dataLoan.sLAmtCalc,
                    sPurchPrice = dataLoan.sPurchPrice,
                    sProdImpound = dataLoan.sProdImpound,
                    sLenderFeeBuyoutRequestedT = dataLoan.sLenderFeeBuyoutRequestedT,
                    sLeadSrcId = dataLoan.sLeadSrcId,
                    sCorrespondentProcessT = dataLoan.sCorrespondentProcessT,
                    sPmlCompanyTierId = dataLoan.sPmlCompanyTierId,
                    sDisclosureRegulationT = dataLoan.sDisclosureRegulationT,
                    sSpCountyFips = dataLoan.sSpCountyFips,
                    sIsNewConstruction = dataLoan.sIsNewConstruction
                };

                FeeServiceRevision feeService = GetCurrentRevision(dataLoan.sBrokerId, dataLoan.sTestLoanFileEnvironment.EnableFeeServiceTest);
                if (feeService == null) return true; // Should not be possible unless there is some way to clear fee service fees in the future.
                var applicableTemplates = feeService.GetApplicableFeeTemplates(matchingData);

                var feeServiceHistory = new ClosingCostApplicationHistory(dataLoan.sFeeServiceApplicationHistoryXmlContent);

                foreach (var conditionHit in feeServiceHistory.FeeSetHistory)
                {
                    if (FeeServiceConditionSet.EvaluateSerializedConditions(conditionHit.SetCondition, matchingData) == false)
                        return false; // This old condition was true
                }

                return true;
            }
            finally
            {
                dataLoan.ByPassFieldSecurityCheck = originalBypass;
            }
        }


        // POD : A condition and the fields it set.
        public class FeeConditionHit
        {
            public string SetCondition;
            public List<string> FieldIds;
        }

        //POD: 
        public class FeeServiceResult
        {
            // Notice there are defaults for this in case no rule ever sets it.
            //public int sDaysInYear = 360;
            public E_cPricingEngineCostT PricingEngineCostT = E_cPricingEngineCostT._802CreditOrCharge;
            public E_cPricingEngineCreditT PricingEngineCreditT = E_cPricingEngineCreditT.None;
            public E_cPricingEngineLimitCreditToT PricingEngineLimitCreditToT = E_cPricingEngineLimitCreditToT.OriginationCharges;
            public decimal? sLOrigFMb;
            public decimal? sLDiscntPc;
            public decimal? sLDiscntFMb;
            public decimal? sLOrigFPc;
            public PricingQuotes quoteResult = null;
            public bool isValidFees = true;
            public bool SetField = false;
            public Dictionary<string, E_ClosingFeeSource> AffectedLines;
            public readonly List<AbstractAuditItem> AuditItems = new List<AbstractAuditItem>();
            public string DebugInfo = string.Empty;

            /// <summary>
            /// Gets the revision id of the fee service.
            /// </summary>
            public int RevisionId { get; private set; }

            public FeeServiceResult(int revisionId, Dictionary<string, string> settingFields, CPageData dataLoan, Dictionary<string,E_ClosingFeeSource> affectedLines, string debugInfo=null)
            {
                this.RevisionId = revisionId;
                AffectedLines = affectedLines;
                foreach (var field in settingFields)
                {
                    if (field.Key == "sPricingEngineCostT") PricingEngineCostT = (E_cPricingEngineCostT)dataLoan.m_convertLos.ToCount(field.Value);
                    if (field.Key == "sPricingEngineCreditT") PricingEngineCreditT = (E_cPricingEngineCreditT)dataLoan.m_convertLos.ToCount(field.Value);
                    if (field.Key == "sPricingEngineLimitCreditToT") PricingEngineLimitCreditToT = (E_cPricingEngineLimitCreditToT)dataLoan.m_convertLos.ToCount(field.Value);

                    if (field.Key == "sLOrigFMb") sLOrigFMb = dataLoan.m_convertLos.ToMoney(field.Value);
                    if (field.Key == "sLDiscntPc") sLDiscntPc = dataLoan.m_convertLos.ToRate(field.Value);
                    if (field.Key == "sLDiscntFMb") sLDiscntFMb = dataLoan.m_convertLos.ToMoney(field.Value);
                    if (field.Key == "sLOrigFPc") sLOrigFPc = dataLoan.m_convertLos.ToRate(field.Value);
                }

                if (sLOrigFMb.HasValue == false) sLOrigFMb = dataLoan.sLOrigFMb;
                if (sLDiscntPc.HasValue == false) sLDiscntPc = dataLoan.sLDiscntPc;
                if (sLDiscntFMb.HasValue == false) sLDiscntFMb = dataLoan.sLDiscntFMb;
                if (sLOrigFPc.HasValue == false) sLOrigFPc = dataLoan.sLOrigFPc;

                DebugInfo = string.IsNullOrEmpty(debugInfo) ? string.Empty : debugInfo;
            }

            public FeeServiceResult() { }
        }


        public class ClosingCostApplicationHistory
        {
            public bool IsBlank
            {
                get
                {
                    return (RevisionId.HasValue == false && FeeSetHistory.Count == 0);
                }
            }
            public int? RevisionId
            {
                get;
                set;
            }

            public List<FeeConditionHit> FeeSetHistory
            {
                get;
                set;
            }

            // Try to check if this history was set by a mode the lender no longer uses.
            public bool IsCorrectFormat( bool IsFeeService )
            {
                if ( IsBlank ) return true; // Blank is the universal format!

                if ( IsFeeService && FeeSetHistory.Any(p => p.FieldIds.Contains("sCcTemplateId") ) )
                {
                    return false; // Fee service does not set templates!
                }

                if (IsFeeService == false && FeeSetHistory.Any(p => !p.FieldIds.Contains("sCcTemplateId")))
                {
                    return false; // AutoCC does not set fee-by-fee!
                }

                return true;
            }

            public ClosingCostApplicationHistory()
            {
                Reset();
            }

            public ClosingCostApplicationHistory(string xml)
            {
                if (!string.IsNullOrEmpty(xml))
                {
                    try
                    {
                        ParseXml(xml);
                    }
                    catch (System.InvalidOperationException)
                    {
                        // This can happen when loading an old version.
                        Reset();
                    }
                }
                else
                {
                    // No history. We are new.
                    Reset();
                }
            }

            private void Reset()
            {
                RevisionId = null;
                FeeSetHistory = new List<FeeConditionHit>();
            }

            public string ToXml()
            {
                if (IsBlank) return "";

                XDocument doc = new XDocument(
                    new XElement("costHistory",
                        new XAttribute("version", RevisionId.ToString()),
                        from FeeConditionHit feeSet in FeeSetHistory
                        select new XElement("feeSet",
                            new XAttribute("condition", feeSet.SetCondition),
                            new XElement("fields",
                            from string fieldId in feeSet.FieldIds
                            select new XElement("field",
                                new XAttribute("id", fieldId)
                            )))));

                return doc.ToString();
            }


            private void ParseXml(string xml)
            {
                var doc = XDocument.Parse(xml);

                RevisionId = (from revision in doc.Elements("costHistory")
                              select int.Parse(revision.Attribute("version").Value)).First();

                FeeSetHistory = new List<FeeConditionHit>(
                            from feeSet in doc.Descendants("feeSet")
                            select new FeeConditionHit()
                            {
                                SetCondition = feeSet.Attribute("condition").Value,
                                FieldIds =
                                new List<string>(
                                    from fields in feeSet.Descendants("field")
                                    select fields.Attribute("id").Value)
                            });
            }

        }

    public class FeeSummary
    {
        public string LineNum;
        public string Desc;
        public string Amount;
    }
    public class FeeChangeSummary : FeeSummary
    {
        public string NewAmount;
        public bool IsChange
        {
            get { return Amount != NewAmount; }
        }
    }

    public class FeeChangeCalculator
    {
        HashSet<FeeSummary> m_baseFees = new HashSet<FeeSummary>();
        HashSet<FeeSummary> m_newFees = new HashSet<FeeSummary>();

        public void AddBaseFee(FeeSummary feeLine)
        {
            m_baseFees.Add(feeLine);
        }

        public void AddNewFee(FeeSummary feeLine)
        {
            m_newFees.Add(feeLine);
        }

        public List<FeeChangeSummary> ChangeList
        {
            get
            {
                List<FeeChangeSummary> ret = new List<FeeChangeSummary>();

                // Merge the lists.
                foreach (FeeSummary currentFee in m_newFees)
                {
                    FeeChangeSummary summary = new FeeChangeSummary() { NewAmount = currentFee.Amount, Desc = currentFee.Desc, LineNum = currentFee.LineNum };
                    ret.Add(summary);

                    FeeSummary previousFee = m_baseFees.FirstOrDefault(oldFee => oldFee.Desc == currentFee.Desc && oldFee.LineNum == currentFee.LineNum);
                    summary.Amount = (previousFee != null) ? previousFee.Amount : "$0.00";
                }

                return ret;
            }
        }
    }

    /// <summary>
    /// Special exception class for fee service.
    /// Really just here to help identify whish exception are thrown by fee service.
    /// </summary>
    private class FeeServiceException : CBaseException
    {
        public static FeeServiceException MultipleMatchingFeesException(string fieldId, string hudLine, string description, IEnumerable<BaseClosingCostFee> matchingFees)
        {
            // If we have more than 1 matching fee then the borrower's set is bad. Log and throw error.
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Fee Service is attempting to add a fee with the same HUD Line number as multiple existing fee.");
            sb.AppendLine("Field ID:" + fieldId);
            sb.AppendLine("HUD Line: " + hudLine);
            if (!string.IsNullOrEmpty(description))
            {
                sb.AppendLine("Fee Description: " + description);
            }
            sb.AppendLine("Existing Fee(s):");

            foreach (BorrowerClosingCostFee match in matchingFees)
            {
                sb.AppendLine("\tFee Description: " + match.Description + "\tFee Id: " + match.ClosingCostFeeTypeId);
            }

            return new FeeServiceException("Fee service cannot add fees because there are two or more fees with HUD line " + hudLine + " already on the loan.", sb.ToString());
        }

        public FeeServiceException(string developerMessage) : base(ErrorMessages.FeeService.GenericError, developerMessage) { }
        public FeeServiceException(Exception innerException) : base(ErrorMessages.FeeService.GenericError, innerException) { }

        public FeeServiceException(string userMessage, string developerMessage) : base(userMessage, developerMessage) { }
        public FeeServiceException(string userMessage, Exception innerException) : base(userMessage, innerException) { }
    }
    }
}
