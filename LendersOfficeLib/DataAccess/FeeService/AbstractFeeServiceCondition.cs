﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml.Schema;
using System.Xml;

namespace DataAccess.FeeService
{
    public abstract class AbstractFeeServiceCondition : IXmlSerializable
    {
        public string FieldId { get; protected set; }
        public abstract bool Evaluate(FeeServiceMatchingTemplateData data);
        public abstract override bool Equals(object obj);
        public abstract override int GetHashCode();
        public abstract override string ToString();

        #region IXmlSerializable Members
        public abstract XmlSchema GetSchema();
        public abstract void ReadXml(XmlReader reader);
        public abstract void WriteXml(XmlWriter writer);
        #endregion
    }
}
