﻿namespace DataAccess.FeeService
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Xml.Linq;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Escrow;
    using LendersOffice.ObjLib.TitleProvider;
    using LendersOffice.Security;

    public class FeeServiceFieldInfo
    {
        public readonly string HUDLine;
        public readonly string LQBFieldName;
        public readonly string ValidValueEntryOptions;
        public readonly string DescriptionOfField;

        public FeeServiceFieldInfo(string hudLine, string lqbFieldName, string validValues, string desc)
        {
            HUDLine = hudLine;
            LQBFieldName = lqbFieldName;
            ValidValueEntryOptions = validValues;
            DescriptionOfField = desc;
        }
    }

    /// <summary>
    /// Class to provide info about fee service fields. This may eventually 
    /// hold field descriptions and the other text necessary for the fee service
    /// files.
    /// </summary>
    public class AvailableFeeServiceFields
    {
        private const string EMBEDDED_RESOURCE_PATH = "LendersOffice.DataAccess.FeeService.FeeServiceFields.xml.config";
        private static Dictionary<string, FeeServiceFieldInfo> x_availableFieldsById;
        private static LosConvert m_losConvert;

        internal static readonly HashSet<Guid> HousingExpensesFeeTypeIds = new HashSet<Guid>()
        {
            DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId,
            DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId,
            DefaultSystemClosingCostFee.Hud1000WindStormInsuranceFeeTypeId,
            DefaultSystemClosingCostFee.Hud1000CondoHo6InsuranceFeeTypeId,
            DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId,
            DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId,
            DefaultSystemClosingCostFee.Hud1000OtherTax1FeeTypeId,
            DefaultSystemClosingCostFee.Hud1000OtherTax2FeeTypeId,
            DefaultSystemClosingCostFee.Hud1000OtherTax3FeeTypeId,
            DefaultSystemClosingCostFee.Hud1000OtherTax4FeeTypeId,
            DefaultSystemClosingCostFee.Hud1000HomeOwnerAssociationDuesFeeTypeId,
            DefaultSystemClosingCostFee.Hud1000GroundRentFeeTypeId
        };

        internal static readonly HashSet<int> HudLinesAllowingMultipleFees = new HashSet<int>()
        {
            800, 900, 1000, 1100, 1200, 1300, 801, 802, 902, 1202
        };

        private static XDocument GetXmlDocument()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            using (StreamReader stream = new StreamReader(assembly.GetManifestResourceStream(EMBEDDED_RESOURCE_PATH)))
            {
                return XDocument.Load(stream);
            }
        }

        static AvailableFeeServiceFields()
        {
            m_losConvert = new LosConvert();

            x_availableFieldsById = new Dictionary<string, FeeServiceFieldInfo>();
            var xmlDoc = GetXmlDocument();

            foreach (var fieldElement in xmlDoc.Descendants("FeeServiceField"))
            {
                var hudLine = fieldElement.Attribute("HUDLine").Value;
                var lqbFieldName = fieldElement.Attribute("LQBFieldName").Value;
                var validValues = fieldElement.Attribute("ValidValueEntryOptions").Value;
                var desc = fieldElement.Attribute("DescriptionOfField").Value;
                var feeServiceField = new FeeServiceFieldInfo(hudLine,
                        lqbFieldName,
                        validValues,
                        desc);
                x_availableFieldsById.Add(lqbFieldName, feeServiceField);
            }
        }

        #region Calc Option Fields
        // Fields that can be set via the fee service but are not stored on the loan.
        private static HashSet<string> x_calcOptionFieldIds = new HashSet<string>()
        {
            "sDaysInYr",
            "sPricingEngineCostT",
            "sPricingEngineCreditT",
            "sPricingEngineLimitCreditToT",
            "sEscrowFPc",
            "sEscrowFBaseT",
            "sEscrowFMb",
            "sOwnerTitleInsFPc",
            "sOwnerTitleInsFBaseT",
            "sOwnerTitleInsFMb",
            "sTitleInsFPc",
            "sTitleInsFBaseT",
            "sTitleInsFMb"
        };
        #endregion

        private static HashSet<string> escrowDisbursementScheduleFields = new HashSet<string>()
        {
            "sHazInsRsrvEscrowDisbursementSchedule",
            "sMInsRsrvEscrowDisbursementSchedule",
            "sRealETxRsrvEscrowDisbursementSchedule",
            "sSchoolTxRsrvEscrowDisbursementSchedule",
            "sFloodInsRsrvEscrowDisbursementSchedule",
            "s1006RsrvEscrowDisbursementSchedule",
            "s1007RsrvEscrowDisbursementSchedule",
            "sU3RsrvEscrowDisbursementSchedule",
            "sU4RsrvEscrowDisbursementSchedule"
        };

        private static HashSet<string> escrowCushionFields = new HashSet<string>()
        {
            "sHazInsRsrvEscrowCushion",
            "sMInsRsrvEscrowCushion",
            "sRealETxRsrvEscrowCushion",
            "sSchoolTxRsrvEscrowCushion",
            "sFloodInsRsrvEscrowCushion",
            "s1006RsrvEscrowCushion",
            "s1007RsrvEscrowCushion",
            "sU3RsrvEscrowCushion",
            "sU4RsrvEscrowCushion"
        };

        /// <summary>
        /// Determine if the field can be set by the fee service. 
        /// </summary>
        /// <param name="fieldId">The field id of the field.</param>
        /// <returns>True if it can be set. Otherwise, false.</returns>
        public static bool IsValidFieldId(string fieldId)
        {
            return x_availableFieldsById.ContainsKey(fieldId);
        }

        private static bool IsCalcOptionField(string fieldId)
        {
            return x_calcOptionFieldIds.Contains(fieldId);
        }

        /// <summary>
        /// Returns the Closing Cost Fee Type ID corresponding to the Default System Closing Cost Fee
        /// that represents the lagacy GFE Field affected by the given Fee Service Field ID.
        /// </summary>
        /// <param name="fieldId">A legacy fee service field ID</param>
        /// <returns>Guid representing a FeeTypeID, or Guid.Empty if the fieldId does not map to one
        /// of the Default System Closing Cost Fees</returns>
        public static Guid GetFeeTypeIdFromFieldId(string fieldId)
        {
            if (s_fieldIdToFeeTypeIdMap.ContainsKey(fieldId))
            {
                return s_fieldIdToFeeTypeIdMap[fieldId];
            }

            return Guid.Empty;
        }

        /// <summary>
        /// Returns the Closing Cost Fee Property that represents the lagacy GFE Field affected by
        /// the given Fee Service Field ID.
        /// </summary>
        /// <param name="fieldId">A legacy fee service field ID</param>
        /// <returns>Enum value representing the fee property to be changed.</returns>
        public static E_FeeServiceFeePropertyT GetFeePropertyFromFieldId(string fieldId)
        {
            if (s_fieldIdToFeeTypePropertyMap.ContainsKey(fieldId))
            {
                return s_fieldIdToFeeTypePropertyMap[fieldId];
            }

            return E_FeeServiceFeePropertyT.None;
        }

        /// <summary>
        /// Returns the Hud Line that represents the legacy GFE Field affected by
        /// the given Fee Service Field ID.
        /// </summary>
        /// <param name="fieldId">A legacy fee service field ID</param>
        /// <returns>Int representing the HUD line number, or 0 if fieldId does not have a HUD line number (eg. calc options).</returns>
        public static int GetHudLineFromFieldId(string fieldId)
        {
            if (s_fieldLineMap.ContainsKey(fieldId))
            {
                return s_fieldLineMap[fieldId];
            }

            return 0;
        }

        /// <summary>
        /// Returns the Hud Line for the fee represented by the passed in type id
        /// </summary>
        /// <param name="fieldId">A legacy fee service field ID</param>
        /// <returns>Int representing the HUD line number, or 0 if fieldId does not have a HUD line number (eg. calc options).</returns>
        public static int GetHudLineFromTypeId(Guid typeId, FeeSetupClosingCostSet cachedBrokerFeeSetup)
        {
            if(HousingExpensesFeeTypeIds.Contains(typeId))
            {
                return 0;
            }
            
            FeeSetupClosingCostFee fee = (FeeSetupClosingCostFee)cachedBrokerFeeSetup.FindFeeByTypeId(typeId);
            if (fee == null)
            {
                return 0;
            }

            return fee.HudLine;
        }

        /// <summary>
        /// Determine if the fee type id is valid. 
        /// </summary>
        /// <param name="typeId">The fee type id of the field.</param>
        /// <param name="parsedTypeId">The parsed value.</param>
        /// <returns>True if typeId is valid. Otherwise, false.</returns>
        public static bool IsValidFeeTypeId(string typeId, FeeSetupClosingCostSet cachedBrokerFeeSetup, out Guid parsedTypeId)
        {
            parsedTypeId = Guid.Empty;

            // Check Inputs
            if (string.IsNullOrEmpty(typeId) || string.IsNullOrEmpty(typeId.TrimWhitespaceAndBOM()))
            {
                return false;
            }

            // Parse TypeId
            Guid gTypeId = m_losConvert.ToGuid(typeId.TrimWhitespaceAndBOM());

            if (gTypeId == Guid.Empty)
            {
                return false;
            }

            parsedTypeId = gTypeId;
            
            // Search broker fee type setup for fee type id
            return cachedBrokerFeeSetup.FindFeeByTypeId(gTypeId) != null;
        }

        /// <summary>
        /// Determine if the title vendor ID is valid. 
        /// </summary>
        /// <param name="vendorId">The vendor ID.</param>
        /// <param name="cachedVendorAssociationList">Cached list of Broker Vendor Associations.</param>
        /// <returns>True if vendorId is valid. Otherwise, false.</returns>
        public static bool IsValidTitleVendorId(string vendorId, List<BrokerVendorAssociation> cachedVendorAssociationList)
        {
            // Check Inputs
            if (string.IsNullOrWhiteSpace(vendorId))
            {
                return false;
            }

            // Parse vendorId
            int parsedVendorId;
            if(!int.TryParse(vendorId.Trim(), out parsedVendorId))
            {
                return false;
            }

            // Search broker fee type setup for fee type id
            return cachedVendorAssociationList.Any(v => v.VendorId == parsedVendorId);
        }

        /// <summary>
        /// Determine if the given fee property is valid. 
        /// </summary>
        /// <param name="feeProperty">The fee property.</param>
        /// <param name="parsedFeeProperty">The parsed value.</param>
        /// <returns>True if feeProperty is valid. Otherwise, false.</returns>
        public static bool IsValidFeeProperty(Guid feeTypeId, string feeProperty, FeeSetupClosingCostSet cachedBrokerFeeSetup, out E_FeeServiceFeePropertyT parsedFeeProperty)
        {
            bool isHousingExpense = HousingExpensesFeeTypeIds.Contains(feeTypeId);
            parsedFeeProperty = Tools.MapStringToFeeServiceFeePropertyT(feeProperty);

            switch (parsedFeeProperty)
            {
                case E_FeeServiceFeePropertyT.Description:
                case E_FeeServiceFeePropertyT.Percent:
                case E_FeeServiceFeePropertyT.BaseValue:
                case E_FeeServiceFeePropertyT.Amount:
                    return true;
                case E_FeeServiceFeePropertyT.SettlementServiceProvider:
                    var feeDisclosureSection = cachedBrokerFeeSetup.FindFeeByTypeId(feeTypeId).IntegratedDisclosureSectionT;
                    return feeDisclosureSection == E_IntegratedDisclosureSectionT.SectionC;
                //case E_FeeServiceFeePropertyT.GfeBox:
                case E_FeeServiceFeePropertyT.Apr:
                case E_FeeServiceFeePropertyT.Fha:
                case E_FeeServiceFeePropertyT.PaidTo:
                case E_FeeServiceFeePropertyT.ThirdParty:
                case E_FeeServiceFeePropertyT.Affiliate:
                case E_FeeServiceFeePropertyT.CanShop:
                case E_FeeServiceFeePropertyT.PaidBy:
                case E_FeeServiceFeePropertyT.Payable:
                case E_FeeServiceFeePropertyT.Remove:
                case E_FeeServiceFeePropertyT.Dflp:
                    return !isHousingExpense;
                case E_FeeServiceFeePropertyT.TaxType:
                case E_FeeServiceFeePropertyT.CalculationSource:
                case E_FeeServiceFeePropertyT.Prepaid:
                case E_FeeServiceFeePropertyT.Escrow:
                case E_FeeServiceFeePropertyT.PrepaidMonths:
                case E_FeeServiceFeePropertyT.ReserveCushion:
                case E_FeeServiceFeePropertyT.ReserveMonthsLocked:
                case E_FeeServiceFeePropertyT.ReserveMonths:
                case E_FeeServiceFeePropertyT.PaymentRepeatInterval:
                case E_FeeServiceFeePropertyT.DisbursementSchedule:
                    return isHousingExpense;
                case E_FeeServiceFeePropertyT.None:
                    return false;
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Unexpected property type.");
            }
        }

        /// <summary>
        /// Determine if the supplied value is valid for the fee property.
        /// </summary>
        /// <param name="feeProperty">Fee Property Type.</param>
        /// <param name="inValue">The value to test.</param>
        /// <param name="parsedValue">The parsed value.</param>
        /// <returns>True if it is a valid value. Otherwise, false.</returns>
        public static bool IsValidValueForFeeProperty(E_FeeServiceFeePropertyT feeProperty, string inValue, out string parsedValue)
        {
            switch (feeProperty)
            {
                case E_FeeServiceFeePropertyT.Description:
                    parsedValue = inValue;
                    return true;
                case E_FeeServiceFeePropertyT.SettlementServiceProvider:
                    return IsValidSettlementServiceProviderValue(inValue, out parsedValue);
                case E_FeeServiceFeePropertyT.Apr:
                case E_FeeServiceFeePropertyT.Fha:
                case E_FeeServiceFeePropertyT.ThirdParty:
                case E_FeeServiceFeePropertyT.Affiliate:
                case E_FeeServiceFeePropertyT.CanShop:
                case E_FeeServiceFeePropertyT.Remove:
                case E_FeeServiceFeePropertyT.Dflp:
                case E_FeeServiceFeePropertyT.Prepaid:
                case E_FeeServiceFeePropertyT.Escrow:
                case E_FeeServiceFeePropertyT.ReserveMonthsLocked:
                    return IsBool(inValue, out parsedValue);
                case E_FeeServiceFeePropertyT.Percent:
                    return IsPercent(inValue, out parsedValue);
                case E_FeeServiceFeePropertyT.Amount:
                    return IsMoney(inValue, out parsedValue);
                case E_FeeServiceFeePropertyT.PrepaidMonths:
                case E_FeeServiceFeePropertyT.ReserveMonths:
                    return IsInteger(inValue, out parsedValue);
                case E_FeeServiceFeePropertyT.ReserveCushion:
                    return IsValidEscrowCushionValue(inValue, out parsedValue);
                //case E_FeeServiceFeePropertyT.GfeBox:
                //    return IsValidGfeBoxPropertyValue(inValue, out parsedValue);
                case E_FeeServiceFeePropertyT.PaidTo:
                    return IsValidPaidToPropertyValue(inValue, out parsedValue);
                case E_FeeServiceFeePropertyT.BaseValue:
                    return IsValidBaseValuePropertyValue(inValue, out parsedValue);
                case E_FeeServiceFeePropertyT.PaidBy:
                    return IsValidPaidByPropertyValue(inValue, out parsedValue);
                case E_FeeServiceFeePropertyT.Payable:
                    return IsValidPayablePropertyValue(inValue, out parsedValue);
                case E_FeeServiceFeePropertyT.TaxType:
                    return IsValidTaxTypePropertyValue(inValue, out parsedValue);
                case E_FeeServiceFeePropertyT.CalculationSource:
                    return IsValidCalculationSourcePropertyValue(inValue, out parsedValue);
                case E_FeeServiceFeePropertyT.PaymentRepeatInterval:
                    return IsValidPaymentRepeatIntervalPropertyValue(inValue, out parsedValue);
                case E_FeeServiceFeePropertyT.DisbursementSchedule:
                    return IsValidEscrowDisbursementScheduleValue(inValue, out parsedValue);
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Unexpected property type.");
            }
        }

        /// <summary>
        /// Get the information associated with a field based on its id.
        /// </summary>
        /// <param name="fieldId"></param>
        /// <returns>The FeeServiceField for the given id. If field id is invalid, null.</returns>
        public static FeeServiceFieldInfo GetField(string fieldId)
        {
            if (!IsValidFieldId(fieldId))
            {
                return null;
            }

            return x_availableFieldsById[fieldId];
        }

        /// <summary>
        /// Get the information associated with all fields in a GFE line.
        /// </summary>
        /// <param name="fieldId"></param>
        /// <returns>Array of FeeServiceFieldInfo for the given line number. If line number is invalid, returns empty array.</returns>
        public static FeeServiceFieldInfo[] GetFieldsByLineNumber(string lineNumber)
        {
            List<FeeServiceFieldInfo> info = new List<FeeServiceFieldInfo>();
            var xmlDoc = GetXmlDocument();
            foreach (var fieldElement in (from el in xmlDoc.Descendants("FeeServiceField") where (string)el.Attribute("HUDLine") == lineNumber select el))
                info.Add(x_availableFieldsById[fieldElement.Attribute("LQBFieldName").Value]);

            return info.ToArray();
        }

        /// <summary>
        /// Determine if the supplied value is valid for the field.
        /// </summary>
        /// <param name="fieldId">The field id of the field.</param>
        /// <param name="value">The value to test.</param>
        /// <param name="outValue">The parsed value.</param>
        /// <returns>True if it is a valid value. Otherwise, false.</returns>
        public static bool IsValidValueForField(string fieldId, string inValue, out string parsedValue)
        {
            parsedValue = null;

            if (!IsValidFieldId(fieldId) || string.IsNullOrEmpty(inValue))
            {
                return false;
            }

            if (IsCalcOptionField(fieldId))
            {
                return IsValidValueForCalcOptionField(fieldId, inValue, out parsedValue);
            }
            else if (IsPaidByField(fieldId))
            {
                return IsValidPaidByFieldValue(inValue, out parsedValue);
            }
            else if (IsEscrowCushionField(fieldId))
            {
                return IsValidEscrowCushionValue(inValue, out parsedValue);
            }
            else if (IsEscrowDisbursementScheduleField(fieldId))
            {
                return IsValidEscrowDisbursementScheduleValue(inValue, out parsedValue);
            }

            E_PageDataFieldType fieldType;
            if (!PageDataUtilities.GetFieldType(fieldId, out fieldType))
            {
                throw new CBaseException(ErrorMessages.Generic, "Could not determine type of field using PageDataUtilities.");
            }

            switch (fieldType)
            {
                case E_PageDataFieldType.String:
                    parsedValue = inValue;
                    return true;
                case E_PageDataFieldType.Bool: return IsBool(inValue, out parsedValue);
                case E_PageDataFieldType.Integer: return IsInteger(inValue, out parsedValue);
                case E_PageDataFieldType.Percent: return IsPercent(inValue, out parsedValue);
                case E_PageDataFieldType.Money: return IsMoney(inValue, out parsedValue);
                case E_PageDataFieldType.Enum:
                    Type enumType;
                    if (!PageDataUtilities.GetFieldEnumType(fieldId, out enumType))
                    {
                        throw new CBaseException(ErrorMessages.Generic, "Unable to determine type of enum using PageDataUtilities.");
                    }

                    return IsValidEnumForFieldAndType(fieldId, inValue, enumType, out parsedValue);
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Unexpected field type.");
            }
        }

        private static bool IsPaidByField(string fieldId)
        {
            return fieldId.TrimWhitespaceAndBOM().EndsWith("PdByT");
        }

        private static bool IsEscrowCushionField(string fieldId)
        {
            return escrowCushionFields.Contains(fieldId);
        }

        private static bool IsEscrowDisbursementScheduleField(string fieldId)
        {
            return escrowDisbursementScheduleFields.Contains(fieldId);
        }

        private static bool IsValidPaidByFieldValue(string inValue, out string parsedValue)
        {
            parsedValue = null;
            switch (inValue.TrimWhitespaceAndBOM().ToUpper())
            {
                case "BORR PD":
                    parsedValue = LosConvert.BORR_PAID_OUTOFPOCKET.ToString();
                    return true;
                case "BORR FIN":
                    parsedValue = LosConvert.BORR_PAID_FINANCED.ToString();
                    return true;
                case "SELLER":
                    parsedValue = LosConvert.SELLER_PAID.ToString();
                    return true;
                case "LENDER":
                    parsedValue = LosConvert.LENDER_PAID.ToString();
                    return true;
                case "BROKER":
                    parsedValue = LosConvert.BROKER_PAID.ToString();
                    return true;
                default:
                    return false;
            }
        }

        private static bool IsValidEscrowCushionValue(string inValue, out string parsedValue)
        {
            parsedValue = null;

            if (string.IsNullOrEmpty(inValue))
            {
                return false;
            }

            if (IsInteger(inValue, out parsedValue))
            {
                int val = int.Parse(inValue);
                return val >= 0 && val <= 2;
            }
            else
            {
                return false;
            }
        }

        private static bool IsValidEscrowDisbursementScheduleValue(string inValue, out string parsedValue)
        {
            parsedValue = null;

            if (string.IsNullOrEmpty(inValue))
            {
                return false;
            }

            EscrowItemDisbursementSchedule disbursementSchedule = null;
            try
            {
                disbursementSchedule = ObsoleteSerializationHelper.JavascriptJsonDeserializer<EscrowItemDisbursementSchedule>(inValue);
            }
            catch (ArgumentException)
            {
                return false;
            }
            catch (InvalidOperationException)
            {
                return false;
            }

            if (disbursementSchedule != null && disbursementSchedule.Validate())
            {
                parsedValue = inValue;
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsValidEnumForFieldAndType(string fieldId, string inValue, Type enumType, out string parsedValue)
        {
            parsedValue = null;
            string trimmedUpperInVal = inValue.TrimWhitespaceAndBOM().ToUpper();

            if (enumType.Equals(typeof(E_PercentBaseT)))
            {
                return IsValidPercentBaseTValue(fieldId, inValue, out parsedValue);
                
            }
            else if (enumType.Equals(typeof(E_FloodCertificationDeterminationT)))
            {
                return IsValidFloodCertDeterminationTValue(inValue, out parsedValue);
            }
            else if (enumType.Equals(typeof(E_TriState)))
            {
                return IsValidTriState(inValue, out parsedValue);
            }
            else if (enumType.Equals(typeof(E_GfeSectionT)))
            {
                return IsValidGfeSectionTValue(fieldId, inValue, out parsedValue);
            }
            else if (enumType.Equals(typeof(E_sLenderCreditCalculationMethodT)))
            {
                return IsValidLenderCreditCalculationMethodTValue(inValue, out parsedValue);
            }
            else if (enumType.Equals(typeof(E_CreditLenderPaidItemT)))
            {
                return IsValidCreditLenderPaidItemTValue(inValue, out parsedValue);
            }
            else if (enumType.Equals(typeof(E_LenderCreditDiscloseLocationT)))
            {
                return IsValidLenderCreditDiscloseLocationTValue(inValue, out parsedValue);
            }
            //else if (enumType.Equals(typeof(E_sToleranceCureCalculationT)))
            //{
            //    return IsValidToleranceCureCalculationTValue(inValue, out parsedValue);
            //}
            else if (enumType.Equals(typeof(E_sLenderCreditMaxT)))
            {
                return IsValidLenderCreditMaxTValue(inValue, out parsedValue);
            }
            else if (enumType.Equals(typeof(E_DisbursementRepIntervalT)))
            {
                return IsValidPaymentRepeatIntervalPropertyValue(inValue, out parsedValue);
            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic,
                    "Trying to parse value of unimplemented enum type in fee service.");
            }
        }

        private static bool IsValidPercentBaseTValue(string fieldId, string inValue, out string parsedValue)
        {
            parsedValue = null;
            string trimmedUpperInVal = inValue.TrimWhitespaceAndBOM().ToUpper();

            bool onlyAllowLoanAmountOptions = fieldId.TrimWhitespaceAndBOM().Equals("sLDiscntBaseT", StringComparison.InvariantCultureIgnoreCase)
                || fieldId.TrimWhitespaceAndBOM().Equals("sMBrokFBaseT", StringComparison.InvariantCultureIgnoreCase);

            bool allowOriginalCosts = fieldId.TrimWhitespaceAndBOM().Equals("sProRealETxT", StringComparison.InvariantCultureIgnoreCase);

            switch (trimmedUpperInVal)
            {
                case "LOAN AMOUNT":
                    parsedValue = E_PercentBaseT.LoanAmount.ToString("d");
                    return true;
                case "TOTAL LOAN AMOUNT":
                    parsedValue = E_PercentBaseT.TotalLoanAmount.ToString("d");
                    return true;
                case "PURCHASE PRICE":
                    if (onlyAllowLoanAmountOptions)
                    {
                        return false;
                    }
                    parsedValue = E_PercentBaseT.SalesPrice.ToString("d");
                    return true;
                case "APPRAISED VALUE":
                case "APPRAISAL VALUE":
                    if (onlyAllowLoanAmountOptions)
                    {
                        return false;
                    }
                    parsedValue = E_PercentBaseT.AppraisalValue.ToString("d");
                    return true;
                case "ORIGINAL COST":
                    if (!allowOriginalCosts)
                    {
                        return false;
                    }
                    parsedValue = E_PercentBaseT.OriginalCost.ToString("d");
                    return true;
                default:
                    return false;
            }
        }

        private static bool IsValidFloodCertDeterminationTValue(string inValue, out string parsedValue)
        {
            parsedValue = null;
            string trimmedUpperInVal = inValue.TrimWhitespaceAndBOM().ToUpper();

            switch (trimmedUpperInVal)
            {
                case "SINGLE CHARGE OR LIFE OF LOAN":
                    parsedValue = E_FloodCertificationDeterminationT.SingleChargeOrLifeOfLoan.ToString("d");
                    return true;
                case "INITIAL FEE":
                    parsedValue = E_FloodCertificationDeterminationT.InitialFee.ToString("d");
                    return true;
                default:
                    return false;
            }
        }

        private static bool IsValidTriState(string inValue, out string parsedValue)
        {
            parsedValue = null;
            string trimmedUpperVal = inValue.TrimWhitespaceAndBOM().ToUpper();
            switch (trimmedUpperVal)
            {
                case "YES":
                    parsedValue = E_TriState.Yes.ToString("d");
                    return true;
                case "NO":
                    parsedValue = E_TriState.No.ToString("d");
                    return true;
                default:
                    return false;
            }
        }

        private static bool IsB4AndB6DisabledIn1100And1300OfGfe
        {
            get
            {
                var principal = PrincipalFactory.CurrentPrincipal;
                if (principal == null)
                {
                    return false;
                }
                var brokerdb = principal.BrokerDB;
                if (brokerdb == null)
                {
                    return false;
                }
                return brokerdb.IsB4AndB6DisabledIn1100And1300OfGfe;
            }
        }
        
        private static bool IsValidGfeSectionTValue(string fieldId, string inValue, out string parsedValue)
        {
            
            parsedValue = null;
            var trimmedUpperInVal = inValue.TrimWhitespaceAndBOM().ToUpper();
            switch (fieldId)
            {
                case "s800U1FGfeSection":
                case "s800U2FGfeSection":
                case "s800U3FGfeSection":
                case "s800U4FGfeSection":
                case "s800U5FGfeSection":
                    // A1, B3, or N/A
                    switch (trimmedUpperInVal)
                    {
                        case "A1":
                            parsedValue = E_GfeSectionT.B1.ToString("d");
                            return true;
                        case "B3":
                            parsedValue = E_GfeSectionT.B3.ToString("d");
                            return true;
                        case "N/A":
                            parsedValue = E_GfeSectionT.NotApplicable.ToString("d");
                            return true;
                        default:
                            return false;
                    }
                case "sEscrowFGfeSection":
                case "sTitleInsFGfeSection":
                case "sDocPrepFGfeSection":
                case "sNotaryFGfeSection":
                case "sAttorneyFGfeSection":
                    // B4, B6
                    switch(trimmedUpperInVal)
                    {
                        case "B4":
                            parsedValue = E_GfeSectionT.B4.ToString("d");
                            return true;
                        case "B6":
                            if (IsB4AndB6DisabledIn1100And1300OfGfe)
                            {
                                return false;
                            }
                            parsedValue = E_GfeSectionT.B6.ToString("d");
                            return true;
                        default:
                            return false;
                    }
                case "sU1TcGfeSection":
                case "sU2TcGfeSection":
                case "sU3TcGfeSection":
                case "sU4TcGfeSection":
                    // B4, B6, N/A
                    switch (trimmedUpperInVal)
                    {
                        case "B4":
                            parsedValue = E_GfeSectionT.B4.ToString("d");
                            return true;
                        case "B6":
                            if (IsB4AndB6DisabledIn1100And1300OfGfe)
                            {
                                return false;
                            }
                            parsedValue = E_GfeSectionT.B6.ToString("d");
                            return true;
                        case "N/A":
                            parsedValue = E_GfeSectionT.NotApplicable.ToString("d");
                            return true;
                        default:
                            return false;
                    }
                case "sU1GovRtcGfeSection":
                case "sU2GovRtcGfeSection":
                case "sU3GovRtcGfeSection":
                    // B7, or B8
                    switch (trimmedUpperInVal)
                    {
                        case "B7":
                            parsedValue = E_GfeSectionT.B7.ToString("d");
                            return true;
                        case "B8":
                            parsedValue = E_GfeSectionT.B8.ToString("d");
                            return true;
                        default:
                            return false;
                    }
                case "sU1ScGfeSection":
                case "sU2ScGfeSection":
                case "sU3ScGfeSection":
                case "sU4ScGfeSection":
                case "sU5ScGfeSection":
                    // B4, B6, N/A
                    switch (trimmedUpperInVal)
                    {
                        case "B4":
                            if (IsB4AndB6DisabledIn1100And1300OfGfe)
                            {
                                return false;
                            }
                            parsedValue = E_GfeSectionT.B4.ToString("d");
                            return true;
                        case "B6":
                            parsedValue = E_GfeSectionT.B6.ToString("d");
                            return true;
                        case "N/A":
                            parsedValue = E_GfeSectionT.NotApplicable.ToString("d");
                            return true;
                        default:
                            return false;
                    }
                case "s904PiaGfeSection":
                case "s900U1PiaGfeSection":
                    // B3, B11, N/A
                    switch(trimmedUpperInVal)
                    {
                        case "B3":
                            parsedValue = E_GfeSectionT.B3.ToString("d");
                            return true;
                        case "B11":
                            parsedValue = E_GfeSectionT.B11.ToString("d");
                            return true;
                        case "N/A":
                            parsedValue = E_GfeSectionT.NotApplicable.ToString("d");
                            return true;
                        default:
                            return false;
                    }
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Unexpected GFE Section field id.");
            }
        }

        private static bool IsValidValueForCalcOptionField(string fieldId, string inValue, out string parsedValue)
        {
            parsedValue = null;
            if (!IsCalcOptionField(fieldId))
            {
                return false;
            }

            var trimmedUpperInValue = inValue.TrimWhitespaceAndBOM().ToUpper();
            switch (fieldId.TrimWhitespaceAndBOM())
            {
                case "sDaysInYr":
                    switch (trimmedUpperInValue)
                    {
                        case "360":
                        case "365":
                        case "366":
                            parsedValue = trimmedUpperInValue;
                            return true;
                        default:
                            return false;
                    }
                case "sPricingEngineCostT":
                    switch (trimmedUpperInValue)
                    {
                        case "801":
                            parsedValue = E_cPricingEngineCostT._801LoanOriginationFee.ToString("d");
                            return true;
                        case "802":
                            parsedValue = E_cPricingEngineCostT._802CreditOrCharge.ToString("d");
                            return true;
                        case "NONE":
                            parsedValue = E_cPricingEngineCostT.None.ToString("d");
                            return true;
                        default:
                            return false;
                    }
                case "sPricingEngineCreditT":
                    switch (trimmedUpperInValue)
                    {
                        case "801":
                            parsedValue = E_cPricingEngineCreditT._801LoanOriginationFee.ToString("d");
                            return true;
                        case "802":
                            parsedValue = E_cPricingEngineCreditT._802CreditOrCharge.ToString("d");
                            return true;
                        case "NONE":
                            parsedValue = E_cPricingEngineCreditT.None.ToString("d");
                            return true;
                        default:
                            return false;
                    }
                case "sPricingEngineLimitCreditToT":
                    switch (trimmedUpperInValue)
                    {
                        case "ESTIMATED CLOSING COST":
                        case "ESTIMATED CLOSING COSTS":
                            parsedValue = E_cPricingEngineLimitCreditToT.EstimatedClosingCosts.ToString("d");
                            return true;
                        case "ORIGINATION CHARGES (BOX A1)":
                            parsedValue = E_cPricingEngineLimitCreditToT.OriginationCharges.ToString("d");
                            return true;
                        case "NO LIMIT":
                            parsedValue = E_cPricingEngineLimitCreditToT.NoLimit.ToString("d");
                            return true;
                        default:
                            return false;
                    }
                case "sEscrowFPc":
                case "sOwnerTitleInsFPc":
                case "sTitleInsFPc":
                    return IsPercent(inValue, out parsedValue);
                case "sEscrowFBaseT":
                case "sOwnerTitleInsFBaseT":
                case "sTitleInsFBaseT":
                    return IsValidEnumForFieldAndType(fieldId, inValue, typeof(E_PercentBaseT), out parsedValue);
                case "sEscrowFMb":
                case "sOwnerTitleInsFMb":
                case "sTitleInsFMb":
                    return IsMoney(inValue, out parsedValue);
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Unhandled Calc Option field");
            }
        }

        private static bool IsValidLenderCreditCalculationMethodTValue(string inValue, out string parsedValue)
        {
            parsedValue = null;
            string trimmedUpperVal = inValue.TrimWhitespaceAndBOM().ToUpper();
            switch (trimmedUpperVal)
            {
                case "SET MANUALLY":
                    parsedValue = E_sLenderCreditCalculationMethodT.SetManually.ToString("d");
                    return true;
                case "CALCULATE FROM FRONT-END RATE LOCK":
                    parsedValue = E_sLenderCreditCalculationMethodT.CalculateFromFrontEndRateLock.ToString("d");
                    return true;
                default:
                    return false;
            }
        }

        private static bool IsValidCreditLenderPaidItemTValue(string inValue, out string parsedValue)
        {
            parsedValue = null;
            string trimmedUpperVal = inValue.TrimWhitespaceAndBOM().ToUpper();
            switch (trimmedUpperVal)
            {
                case "ALL LENDER PAID ITEMS":
                    parsedValue = E_CreditLenderPaidItemT.AllLenderPaidItems.ToString("d");
                    return true;
                case "ORIGINATOR COMPENSATION ONLY":
                    parsedValue = E_CreditLenderPaidItemT.OriginatorCompensationOnly.ToString("d");
                    return true;
                case "NONE":
                    parsedValue = E_CreditLenderPaidItemT.None.ToString("d");
                    return true;
                default:
                    return false;
            }
        }

        private static bool IsValidLenderCreditDiscloseLocationTValue(string inValue, out string parsedValue)
        {
            parsedValue = null;
            string trimmedUpperVal = inValue.TrimWhitespaceAndBOM().ToUpper();
            switch (trimmedUpperVal)
            {
                case "INITIAL/GFE":
                    parsedValue = E_LenderCreditDiscloseLocationT.InitialGfe.ToString("d");
                    return true;
                case "CLOSING/HUD-1":
                    parsedValue = E_LenderCreditDiscloseLocationT.ClosingHud1.ToString("d");
                    return true;
                default:
                    return false;
            }
        }

        //private static bool IsValidToleranceCureCalculationTValue(string inValue, out string parsedValue)
        //{
        //    parsedValue = null;
        //    string trimmedUpperVal = inValue.TrimWhitespaceAndBOM().ToUpper();
        //    switch (trimmedUpperVal)
        //    {
        //        case "INCLUDE":
        //            parsedValue = E_sToleranceCureCalculationT.Include.ToString("d");
        //            return true;
        //        case "EXCLUDE":
        //            parsedValue = E_sToleranceCureCalculationT.Exclude.ToString("d");
        //            return true;
        //        default:
        //            return false;
        //    }
        //}

        private static bool IsValidLenderCreditMaxTValue(string inValue, out string parsedValue)
        {
            parsedValue = null;
            string trimmedUpperVal = inValue.TrimWhitespaceAndBOM().ToUpper();
            switch (trimmedUpperVal)
            {
                case "NO CREDIT":
                    parsedValue = E_sLenderCreditMaxT.NoCredit.ToString("d");
                    return true;
                case "ORIGINATION CHARGES":
                    parsedValue = E_sLenderCreditMaxT.OriginationCharges.ToString("d");
                    return true;
                case "TOTAL CLOSING COSTS":
                    parsedValue = E_sLenderCreditMaxT.TotalClosingCosts.ToString("d");
                    return true;
                case "NO LIMIT":
                    parsedValue = E_sLenderCreditMaxT.NoLimit.ToString("d");
                    return true;
                case "MANUAL":
                    parsedValue = E_sLenderCreditMaxT.Manual.ToString("d");
                    return true;
                default:
                    return false;
            }
        }

        //private static bool IsValidGfeBoxPropertyValue(string inValue, out string parsedValue)
        //{
        //    parsedValue = null;
        //    var trimmedUpperInVal = inValue.TrimWhitespaceAndBOM().ToUpper();
        //    switch (trimmedUpperInVal)
        //    {
        //        case "A1":
        //            parsedValue = E_GfeSectionT.B1.ToString("d");
        //            return true;
        //        case "A2":
        //            parsedValue = E_GfeSectionT.B2.ToString("d");
        //            return true;
        //        case "B3":
        //            parsedValue = E_GfeSectionT.B3.ToString("d");
        //            return true;
        //        case "B4":
        //            parsedValue = E_GfeSectionT.B4.ToString("d");
        //            return true;
        //        case "B5":
        //            parsedValue = E_GfeSectionT.B5.ToString("d");
        //            return true;
        //        case "B6":
        //            parsedValue = E_GfeSectionT.B6.ToString("d");
        //            return true;
        //        case "B7":
        //            parsedValue = E_GfeSectionT.B7.ToString("d");
        //            return true;
        //        case "B8":
        //            parsedValue = E_GfeSectionT.B8.ToString("d");
        //            return true;
        //        case "B9":
        //            parsedValue = E_GfeSectionT.B9.ToString("d");
        //            return true;
        //        case "B10":
        //            parsedValue = E_GfeSectionT.B10.ToString("d");
        //            return true;
        //        case "B11":
        //            parsedValue = E_GfeSectionT.B11.ToString("d");
        //            return true;
        //        case "N/A":
        //            parsedValue = E_GfeSectionT.NotApplicable.ToString("d");
        //            return true;
        //        default:
        //            return false;
        //    }
        //}

        private static bool IsValidPaidToPropertyValue(string inValue, out string parsedValue)
        {
            parsedValue = null;
            var trimmedUpperInVal = inValue.TrimWhitespaceAndBOM().ToUpper();
            switch (trimmedUpperInVal)
            {
                case "APPRAISER":
                    parsedValue = E_AgentRoleT.Appraiser.ToString("D");
                    return true;
                case "BANK":
                    parsedValue = E_AgentRoleT.Bank.ToString("D");
                    return true;
                case "BROKER":
                    parsedValue = E_AgentRoleT.Broker.ToString("D");
                    return true;
                case "BROKER REP":
                    parsedValue = E_AgentRoleT.BrokerRep.ToString("D");
                    return true;
                case "BUILDER":
                    parsedValue = E_AgentRoleT.Builder.ToString("D");
                    return true;
                case "BUYER AGENT":
                    parsedValue = E_AgentRoleT.BuyerAgent.ToString("D");
                    return true;
                case"BUYER ATTORNEY":
                    parsedValue = E_AgentRoleT.BuyerAttorney.ToString("D");
                    return true;
                case "CALL CENTER AGENT":
                    parsedValue = E_AgentRoleT.CallCenterAgent.ToString("D");
                    return true;
                case "CLOSING AGENT":
                    parsedValue = E_AgentRoleT.ClosingAgent.ToString("D");
                    return true;
                case "CREDIT REPORT":
                    parsedValue = E_AgentRoleT.CreditReport.ToString("D");
                    return true;
                case "CREDIT REPORT AGENCY #2":
                    parsedValue = E_AgentRoleT.CreditReportAgency2.ToString("D");
                    return true;
                case "CREDIT REPORT AGENCY #3":
                    parsedValue = E_AgentRoleT.CreditReportAgency3.ToString("D");
                    return true;
                case "ECOA":
                    parsedValue = E_AgentRoleT.ECOA.ToString("D");
                    return true;
                case "ESCROW":
                    parsedValue = E_AgentRoleT.Escrow.ToString("D");
                    return true;
                case "FAIR HOUSING LENDING":
                    parsedValue = E_AgentRoleT.FairHousingLending.ToString("D");
                    return true;
                case "FLOOD PROVIDER":
                    parsedValue = E_AgentRoleT.FloodProvider.ToString("D");
                    return true;
                case "HOMEOWNER INSURANCE":
                    parsedValue = E_AgentRoleT.HazardInsurance.ToString("D");
                    return true;
                case "HOMEOWNER INSURANCE (OBSOLETE)":
                    parsedValue = E_AgentRoleT.HomeOwnerInsurance.ToString("D");
                    return true;
                case "HOMEOWNER ASSOCIATION":
                    parsedValue = E_AgentRoleT.HomeOwnerAssociation.ToString("D");
                    return true;
                case "INVESTOR":
                    parsedValue = E_AgentRoleT.Investor.ToString("D");
                    return true;
                case "LENDER":
                    parsedValue = E_AgentRoleT.Lender.ToString("D");
                    return true;
                case "LISTING AGENT":
                    parsedValue = E_AgentRoleT.ListingAgent.ToString("D");
                    return true;
                case "LOAN OFFICER":
                    parsedValue = E_AgentRoleT.LoanOfficer.ToString("D");
                    return true;
                case "LOAN OPENER":
                    parsedValue = E_AgentRoleT.LoanOpener.ToString("D");
                    return true;
                case "MANAGER":
                    parsedValue = E_AgentRoleT.Manager.ToString("D");
                    return true;
                case "MARKETING LEAD":
                    parsedValue = E_AgentRoleT.MarketingLead.ToString("D");
                    return true;
                case "MORTGAGE INSURANCE":
                    parsedValue = E_AgentRoleT.MortgageInsurance.ToString("D");
                    return true;
                case "MORTGAGEE":
                    parsedValue = E_AgentRoleT.Mortgagee.ToString("D");
                    return true;
                case "OTHER":
                    parsedValue = E_AgentRoleT.Other.ToString("D");
                    return true;
                case "PROCESSOR":
                    parsedValue = E_AgentRoleT.Processor.ToString("D");
                    return true;
                case "REALTOR":
                    parsedValue = E_AgentRoleT.Realtor.ToString("D");
                    return true;
                case "SELLER":
                    parsedValue = E_AgentRoleT.Seller.ToString("D");
                    return true;
                case "SELLER ATTORNEY":
                    parsedValue = E_AgentRoleT.SellerAttorney.ToString("D");
                    return true;
                case "SELLING AGENT":
                    parsedValue = E_AgentRoleT.SellingAgent.ToString("D");
                    return true;
                case "SERVICING":
                    parsedValue = E_AgentRoleT.Servicing.ToString("D");
                    return true;
                case "SURVEYOR":
                    parsedValue = E_AgentRoleT.Surveyor.ToString("D");
                    return true;
                case "TITLE":
                    parsedValue = E_AgentRoleT.Title.ToString("D");
                    return true;
                case "UNDERWRITER":
                    parsedValue = E_AgentRoleT.Underwriter.ToString("D");
                    return true;
                case "HOME INSPECTION":
                    parsedValue = E_AgentRoleT.HomeInspection.ToString("D");
                    return true;
                case "SHIPPER":
                    parsedValue = E_AgentRoleT.Shipper.ToString("D");
                    return true;
                case "PROCESSOR (EXTERNAL)":
                    parsedValue = E_AgentRoleT.BrokerProcessor.ToString("D");
                    return true;
                case "TRUSTEE":
                    parsedValue = E_AgentRoleT.Trustee.ToString("D");
                    return true;
                case "FUNDER":
                    parsedValue = E_AgentRoleT.Funder.ToString("D");
                    return true;
                case "POST-CLOSER":
                    parsedValue = E_AgentRoleT.PostCloser.ToString("D");
                    return true;
                case "INSURING":
                    parsedValue = E_AgentRoleT.Insuring.ToString("D");
                    return true;
                case "COLLATERAL AGENT":
                    parsedValue = E_AgentRoleT.CollateralAgent.ToString("D");
                    return true;
                case "DOC DRAWER":
                    parsedValue = E_AgentRoleT.DocDrawer.ToString("D");
                    return true;
                case "PROPERTY MANAGEMENT":
                    parsedValue = E_AgentRoleT.PropertyManagement.ToString("D");
                    return true;
                case "TITLE UNDERWRITER":
                    parsedValue = E_AgentRoleT.TitleUnderwriter.ToString("D");
                    return true;
                case "CREDIT AUDITOR":
                    parsedValue = E_AgentRoleT.CreditAuditor.ToString("D");
                    return true;
                case "DISCLOSURE DESK":
                    parsedValue = E_AgentRoleT.DisclosureDesk.ToString("D");
                    return true;
                case "JUNIOR PROCESSOR":
                    parsedValue = E_AgentRoleT.JuniorProcessor.ToString("D");
                    return true;
                case "JUNIOR UNDERWRITER":
                    parsedValue = E_AgentRoleT.JuniorUnderwriter.ToString("D");
                    return true;
                case "LEGAL AUDITOR":
                    parsedValue = E_AgentRoleT.LegalAuditor.ToString("D");
                    return true;
                case "LOAN OFFICER ASSISTANT":
                    parsedValue = E_AgentRoleT.LoanOfficerAssistant.ToString("D");
                    return true;
                case "PURCHASER":
                    parsedValue = E_AgentRoleT.Purchaser.ToString("D");
                    return true;
                case "QC COMPLIANCE":
                    parsedValue = E_AgentRoleT.QCCompliance.ToString("D");
                    return true;
                case "SECONDARY":
                    parsedValue = E_AgentRoleT.Secondary.ToString("D");
                    return true;
                case "PEST INSPECTION":
                    parsedValue = E_AgentRoleT.PestInspection.ToString("D");
                    return true;
                case "SUBSERVICER":
                    parsedValue = E_AgentRoleT.Subservicer.ToString("D");
                    return true;
                case "SECONDARY (EXTERNAL)":
                    parsedValue = E_AgentRoleT.ExternalSecondary.ToString("D");
                    return true;
                case "POST-CLOSER (EXTERNAL)":
                    parsedValue = E_AgentRoleT.ExternalPostCloser.ToString("D");
                    return true;
                case"LOAN PURCHASE PAYEE":
                    parsedValue = E_AgentRoleT.LoanPurchasePayee.ToString("D");
                    return true;
                case "APPRAISAL MANAGEMENT COMPANY":
                    parsedValue = E_AgentRoleT.AppraisalManagementCompany.ToString("D"); 
                    return true;
                case "REFERRAL":
                    parsedValue = E_AgentRoleT.Referral.ToString("D");
                    return true;
                default:
                    return false;
            }
        }

        private static bool IsValidBaseValuePropertyValue(string inValue, out string parsedValue)
        {
            parsedValue = null;
            string trimmedUpperInVal = inValue.TrimWhitespaceAndBOM().ToUpper();

            switch (trimmedUpperInVal)
            {
                case "LOAN AMOUNT":
                    parsedValue = E_PercentBaseT.LoanAmount.ToString("d");
                    return true;
                case "PURCHASE PRICE":
                    parsedValue = E_PercentBaseT.SalesPrice.ToString("d");
                    return true;
                case "APPRAISAL VALUE":
                    parsedValue = E_PercentBaseT.AppraisalValue.ToString("d");
                    return true;
                case "TOTAL LOAN AMOUNT":
                    parsedValue = E_PercentBaseT.TotalLoanAmount.ToString("d");
                    return true;
                default:
                    return false;
            }
        }

        private static bool IsValidPaidByPropertyValue(string inValue, out string parsedValue)
        {
            parsedValue = null;
            string trimmedUpperInVal = inValue.TrimWhitespaceAndBOM().ToUpper();

            switch (trimmedUpperInVal)
            {
                case "BORR PD":
                    parsedValue = "1";
                    return true;
                case "SELLER":
                    parsedValue = "2";
                    return true;
                case "BORR FIN":
                    parsedValue = "3";
                    return true;
                case "LENDER":
                    parsedValue = "4";
                    return true;
                case "BROKER":
                    parsedValue = "5";
                    return true;
                case "OTHER":
                    parsedValue = "6";
                    return true;
                default:
                    return false;
            }
        }

        private static bool IsValidTaxTypePropertyValue(string inValue, out string parsedValue)
        {
            parsedValue = null;
            string trimmedUpperInVal = inValue.TrimWhitespaceAndBOM().ToUpper();

            switch (trimmedUpperInVal)
            {
                case "BLANK":
                    parsedValue = E_TaxTableTaxT.LeaveBlank.ToString("D");
                    return true;
                case "BOROUGH":
                    parsedValue = E_TaxTableTaxT.Borough.ToString("D");
                    return true;
                case "CITY":
                    parsedValue = E_TaxTableTaxT.City.ToString("D");
                    return true;
                case "COUNTY":
                    parsedValue = E_TaxTableTaxT.County.ToString("D");
                    return true;
                case "FIRE DISTRICT":
                    parsedValue = E_TaxTableTaxT.FireDist.ToString("D");
                    return true;
                case "LOCAL IMPROVEMENT DISTRICT":
                    parsedValue = E_TaxTableTaxT.LocalImprovementDist.ToString("D");
                    return true;
                case "MISCELLANEOUS":
                    parsedValue = E_TaxTableTaxT.Miscellaneous.ToString("D");
                    return true;
                case "MUNICIPAL UTILITY DISTRICT":
                    parsedValue = E_TaxTableTaxT.MunicipalUtilDist.ToString("D");
                    return true;
                case "SCHOOL":
                    parsedValue = E_TaxTableTaxT.School.ToString("D");
                    return true;
                case "SPECIAL ASSESSMENT DISTRICT":
                    parsedValue = E_TaxTableTaxT.SpecialAssessmentDist.ToString("D");
                    return true;
                case "TOWN":
                    parsedValue = E_TaxTableTaxT.Town.ToString("D");
                    return true;
                case "TOWNSHIP":
                    parsedValue = E_TaxTableTaxT.Township.ToString("D");
                    return true;
                case "UTILITY":
                    parsedValue = E_TaxTableTaxT.Utility.ToString("D");
                    return true;
                case "VILLAGE":
                    parsedValue = E_TaxTableTaxT.Village.ToString("D");
                    return true;
                case "WASTE FEE DISTRICT":
                    parsedValue = E_TaxTableTaxT.WasteFeeDist.ToString("D");
                    return true;
                case "WATER/IRRIGATION":
                    parsedValue = E_TaxTableTaxT.Water_Irrigation.ToString("D");
                    return true;
                case "Water/Sewer":
                    parsedValue = E_TaxTableTaxT.Water_Sewer.ToString("D");
                    return true;
                default:
                    return false;
            }
        }

        private static bool IsValidCalculationSourcePropertyValue(string inValue, out string parsedValue)
        {
            parsedValue = null;
            string trimmedUpperInVal = inValue.TrimWhitespaceAndBOM().ToUpper();

            switch (trimmedUpperInVal)
            {
                case "CALCULATOR":
                    parsedValue = E_AnnualAmtCalcTypeT.LoanValues.ToString("D");
                    return true;
                case "DISBURSEMENTS":
                    parsedValue = E_AnnualAmtCalcTypeT.Disbursements.ToString("D");
                    return true;
                default:
                    return false;
            }
        }

        private static bool IsValidPaymentRepeatIntervalPropertyValue(string inValue, out string parsedValue)
        {
            parsedValue = null;
            string trimmedUpperInVal = inValue.TrimWhitespaceAndBOM().ToUpper();

            switch (trimmedUpperInVal)
            {
                case "ANNUAL":
                    parsedValue = E_DisbursementRepIntervalT.Annual.ToString("D");
                    return true;
                case "MONTHLY":
                    parsedValue = E_DisbursementRepIntervalT.Monthly.ToString("D");
                    return true;
                case "ANNUALLY IN CLOSING MONTH":
                    parsedValue = E_DisbursementRepIntervalT.AnnuallyInClosingMonth.ToString("D");
                    return true;
                default:
                    return false;
            }
        }

        private static bool IsValidPayablePropertyValue(string inValue, out string parsedValue)
        {
            parsedValue = null;
            string trimmedUpperInVal = inValue.TrimWhitespaceAndBOM().ToUpper();

            switch (trimmedUpperInVal)
            {
                case "AT CLOSING":
                    parsedValue = "1";
                    return true;
                case "OUTSIDE OF CLOSING":
                    parsedValue = "2";
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Determines whether the specified <paramref name="inValue"/> is a
        /// valid settlement service provider value, setting <paramref name="parsedValue"/>
        /// to the provider's system ID if so.
        /// </summary>
        /// <param name="inValue">
        /// The input string to parse.
        /// </param>
        /// <param name="parsedValue">
        /// The parsed system ID of the provider or null if the value could not be parsed.
        /// </param>
        /// <remarks>
        /// The expected input format for an entry is "system ID : provider name".
        /// </remarks>
        /// <returns>
        /// True if the input value is a valid settlement service provider value,
        /// false otherwise.
        /// </returns>
        private static bool IsValidSettlementServiceProviderValue(string inValue, out string parsedValue)
        {
            parsedValue = null;

            if (string.IsNullOrWhiteSpace(inValue))
            {
                return false;
            }

            // The Split overload accepting a StringSplitOptions will only filter out
            // the empty string. We need to make sure we filter out both empty and whitespace.
            var parts = inValue.Split(':').Where(part => !string.IsNullOrWhiteSpace(part));
            if (parts.Count() != 2)
            {
                return false;
            }

            parsedValue = parts.First().Trim();
            return true;
        }

        private static bool IsBool(string inValue)
        {
            string parsedValue;
            return IsBool(inValue, out parsedValue);
        }

        private static bool IsBool(string inValue, out string parsedValue)
        {
            parsedValue = null;
            string trimmedUpperInVal = inValue.TrimWhitespaceAndBOM().ToUpper();

            switch (trimmedUpperInVal)
            {
                case "YES":
                    parsedValue = true.ToString();
                    return true;
                case "NO":
                    parsedValue = false.ToString();
                    return true;
                default:
                    return false;
            }
        }

        private static bool IsMoney(string inValue, out string parsedValue)
        {
            parsedValue = null;
            decimal moneyVal;
            if (Decimal.TryParse(inValue, NumberStyles.Currency, CultureInfo.CurrentCulture, out moneyVal))
            {
                parsedValue = moneyVal.ToString();
                return true;
            }

            return false;
        }

        private static bool IsPercent(string inValue, out string parsedValue)
        {
            parsedValue = null;
            return IsDecimal(inValue.Replace("%", ""), out parsedValue);
        }

        private static bool IsDecimal(string inValue, out string parsedValue)
        {
            parsedValue = null;
            decimal parsedDecimal;
            if (!decimal.TryParse(inValue, out parsedDecimal))
            {
                return false;
            }
            parsedValue = parsedDecimal.ToString();
            return true;
        }

        private static bool IsInteger(string inValue, out string parsedValue)
        {
            parsedValue = null;
            int parsedInt;
            if (!int.TryParse(inValue, out parsedInt))
            {
                return false;
            }
            parsedValue = parsedInt.ToString();
            return true;
        }

        // "A Dictionary can support multiple readers concurrently, as long as the collection is not modified." -MSDN
        private static readonly Dictionary<string, Guid> s_fieldIdToFeeTypeIdMap
            = new Dictionary<string, Guid>
        {
            { "sLOrigFPc", DefaultSystemClosingCostFee.Hud800LoanOriginationFeeTypeId },
            { "sLOrigFMb", DefaultSystemClosingCostFee.Hud800LoanOriginationFeeTypeId },
            { "sLOrigFProps_PdByT", DefaultSystemClosingCostFee.Hud800LoanOriginationFeeTypeId },
            { "sLOrigFProps_Apr", DefaultSystemClosingCostFee.Hud800LoanOriginationFeeTypeId },
            { "sLOrigFProps_BF", DefaultSystemClosingCostFee.Hud800LoanOriginationFeeTypeId },
            { "sLOrigFProps_FhaAllow", DefaultSystemClosingCostFee.Hud800LoanOriginationFeeTypeId },
            { "sSettlementLOrigFProps_Dflp", DefaultSystemClosingCostFee.Hud800LoanOriginationFeeTypeId },
            { "sLOrigFProps_PaidToThirdParty",DefaultSystemClosingCostFee.Hud800LoanOriginationFeeTypeId },
            { "sLOrigFProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud800LoanOriginationFeeTypeId },
            { "sLOrigFProps_ToBroker", DefaultSystemClosingCostFee.Hud800LoanOriginationFeeTypeId },
            { "sLDiscntPc", DefaultSystemClosingCostFee.Hud800CreditOrChargeFeeTypeId },
            { "sLDiscntBaseT", DefaultSystemClosingCostFee.Hud800CreditOrChargeFeeTypeId },
            { "sLDiscntFMb", DefaultSystemClosingCostFee.Hud800CreditOrChargeFeeTypeId },
            { "sGfeDiscountPointFProps_PdByT", DefaultSystemClosingCostFee.Hud800CreditOrChargeFeeTypeId },
            { "sGfeLenderCreditFProps_Apr", DefaultSystemClosingCostFee.Hud800CreditOrChargeFeeTypeId },
            { "sGfeLenderCreditFProps_FhaAllow", DefaultSystemClosingCostFee.Hud800CreditOrChargeFeeTypeId },
            { "sGfeDiscountPointFProps_Apr", DefaultSystemClosingCostFee.Hud800CreditOrChargeFeeTypeId },
            { "sGfeDiscountPointFProps_BF", DefaultSystemClosingCostFee.Hud800CreditOrChargeFeeTypeId },
            { "sGfeDiscountPointFProps_FhaAllow", DefaultSystemClosingCostFee.Hud800CreditOrChargeFeeTypeId },
            { "sSettlementDiscountPointFProps_Dflp", DefaultSystemClosingCostFee.Hud800CreditOrChargeFeeTypeId },
            { "sGfeDiscountPointFProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud800CreditOrChargeFeeTypeId },
            { "sGfeDiscountPointFProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud800CreditOrChargeFeeTypeId },
            { "sApprF", DefaultSystemClosingCostFee.Hud800AppraisalFeeFeeTypeId },
            { "sApprFProps_PdByT", DefaultSystemClosingCostFee.Hud800AppraisalFeeFeeTypeId },
            { "sApprFProps_Apr", DefaultSystemClosingCostFee.Hud800AppraisalFeeFeeTypeId },
            { "sApprFProps_FhaAllow", DefaultSystemClosingCostFee.Hud800AppraisalFeeFeeTypeId },
            { "sApprFProps_Poc", DefaultSystemClosingCostFee.Hud800AppraisalFeeFeeTypeId },
            { "sApprFPaid", DefaultSystemClosingCostFee.Hud800AppraisalFeeFeeTypeId },
            { "sSettlementApprFProps_Dflp", DefaultSystemClosingCostFee.Hud800AppraisalFeeFeeTypeId },
            { "sApprFProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud800AppraisalFeeFeeTypeId },
            { "sApprFProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud800AppraisalFeeFeeTypeId },
            { "sApprFProps_ToBroker", DefaultSystemClosingCostFee.Hud800AppraisalFeeFeeTypeId },
            { "sApprFPaidTo", DefaultSystemClosingCostFee.Hud800AppraisalFeeFeeTypeId },
            { "sCrF", DefaultSystemClosingCostFee.Hud800CreditReportFeeTypeId },
            { "sCrFProps_PdByT", DefaultSystemClosingCostFee.Hud800CreditReportFeeTypeId },
            { "sCrFProps_Apr", DefaultSystemClosingCostFee.Hud800CreditReportFeeTypeId },
            { "sCrFProps_FhaAllow", DefaultSystemClosingCostFee.Hud800CreditReportFeeTypeId },
            { "sCrFProps_Poc", DefaultSystemClosingCostFee.Hud800CreditReportFeeTypeId },
            { "sCrFPaid", DefaultSystemClosingCostFee.Hud800CreditReportFeeTypeId },
            { "sSettlementCrFProps_Dflp", DefaultSystemClosingCostFee.Hud800CreditReportFeeTypeId },
            { "sCrFProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud800CreditReportFeeTypeId },
            { "sCrFProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud800CreditReportFeeTypeId },
            { "sCrFProps_ToBroker", DefaultSystemClosingCostFee.Hud800CreditReportFeeTypeId },
            { "sCrFPaidTo", DefaultSystemClosingCostFee.Hud800CreditReportFeeTypeId },
            { "sTxServF", DefaultSystemClosingCostFee.Hud800TaxServiceFeeTypeId },
            { "sTxServFProps_PdByT", DefaultSystemClosingCostFee.Hud800TaxServiceFeeTypeId },
            { "sTxServFProps_Apr", DefaultSystemClosingCostFee.Hud800TaxServiceFeeTypeId },
            { "sTxServFProps_FhaAllow", DefaultSystemClosingCostFee.Hud800TaxServiceFeeTypeId },
            { "sTxServFProps_Poc", DefaultSystemClosingCostFee.Hud800TaxServiceFeeTypeId },
            { "sSettlementTxServFProps_Dflp", DefaultSystemClosingCostFee.Hud800TaxServiceFeeTypeId },
            { "sTxServFProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud800TaxServiceFeeTypeId },
            { "sTxServFProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud800TaxServiceFeeTypeId },
            { "sTxServFProps_ToBroker", DefaultSystemClosingCostFee.Hud800TaxServiceFeeTypeId },
            { "sTxServFPaidTo", DefaultSystemClosingCostFee.Hud800TaxServiceFeeTypeId },
            { "sFloodCertificationF", DefaultSystemClosingCostFee.Hud800FloodCertificationFeeTypeId },
            { "sFloodCertificationDeterminationT", DefaultSystemClosingCostFee.Hud800FloodCertificationFeeTypeId },
            { "sFloodCertificationFProps_PdByT", DefaultSystemClosingCostFee.Hud800FloodCertificationFeeTypeId },
            { "sFloodCertificationFProps_Apr", DefaultSystemClosingCostFee.Hud800FloodCertificationFeeTypeId },
            { "sFloodCertificationFProps_FhaAllow", DefaultSystemClosingCostFee.Hud800FloodCertificationFeeTypeId },
            { "sFloodCertificationFProps_Poc", DefaultSystemClosingCostFee.Hud800FloodCertificationFeeTypeId },
            { "sSettlementFloodCertificationFProps_Dflp", DefaultSystemClosingCostFee.Hud800FloodCertificationFeeTypeId },
            { "sFloodCertificationFProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud800FloodCertificationFeeTypeId },
            { "sFloodCertificationFProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud800FloodCertificationFeeTypeId },
            { "sFloodCertificationFProps_ToBroker", DefaultSystemClosingCostFee.Hud800FloodCertificationFeeTypeId },
            { "sFloodCertificationFPaidTo", DefaultSystemClosingCostFee.Hud800FloodCertificationFeeTypeId },
            { "sMBrokFPc", DefaultSystemClosingCostFee.Hud800MortgageBrokerFeeTypeId },
            { "sMBrokFBaseT", DefaultSystemClosingCostFee.Hud800MortgageBrokerFeeTypeId },
            { "sMBrokFMb", DefaultSystemClosingCostFee.Hud800MortgageBrokerFeeTypeId },
            { "sMBrokFProps_PdByT", DefaultSystemClosingCostFee.Hud800MortgageBrokerFeeTypeId },
            { "sMBrokFProps_Apr", DefaultSystemClosingCostFee.Hud800MortgageBrokerFeeTypeId },
            { "sMBrokFProps_FhaAllow", DefaultSystemClosingCostFee.Hud800MortgageBrokerFeeTypeId },
            { "sMBrokFProps_Poc", DefaultSystemClosingCostFee.Hud800MortgageBrokerFeeTypeId },
            { "sSettlementMBrokFProps_Dflp", DefaultSystemClosingCostFee.Hud800MortgageBrokerFeeTypeId },
            { "sMBrokFProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud800MortgageBrokerFeeTypeId },
            { "sMBrokFProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud800MortgageBrokerFeeTypeId },
            { "sMBrokFProps_ToBroker", DefaultSystemClosingCostFee.Hud800MortgageBrokerFeeTypeId },
            { "sInspectF", DefaultSystemClosingCostFee.Hud800LenderInspectionFeeTypeId },
            { "sInspectFProps_PdByT", DefaultSystemClosingCostFee.Hud800LenderInspectionFeeTypeId },
            { "sInspectFProps_Apr", DefaultSystemClosingCostFee.Hud800LenderInspectionFeeTypeId },
            { "sInspectFProps_FhaAllow", DefaultSystemClosingCostFee.Hud800LenderInspectionFeeTypeId },
            { "sInspectFProps_Poc", DefaultSystemClosingCostFee.Hud800LenderInspectionFeeTypeId },
            { "sSettlementInspectFProps_Dflp", DefaultSystemClosingCostFee.Hud800LenderInspectionFeeTypeId },
            { "sInspectFProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud800LenderInspectionFeeTypeId },
            { "sInspectFProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud800LenderInspectionFeeTypeId },
            { "sInspectFProps_ToBroker", DefaultSystemClosingCostFee.Hud800LenderInspectionFeeTypeId },
            { "sInspectFPaidTo", DefaultSystemClosingCostFee.Hud800LenderInspectionFeeTypeId },
            { "sProcF", DefaultSystemClosingCostFee.Hud800ProcessingFeeTypeId },
            { "sProcFProps_PdByT", DefaultSystemClosingCostFee.Hud800ProcessingFeeTypeId },
            { "sProcFProps_Apr", DefaultSystemClosingCostFee.Hud800ProcessingFeeTypeId },
            { "sProcFProps_FhaAllow", DefaultSystemClosingCostFee.Hud800ProcessingFeeTypeId },
            { "sProcFProps_Poc", DefaultSystemClosingCostFee.Hud800ProcessingFeeTypeId },
            { "sProcFPaid", DefaultSystemClosingCostFee.Hud800ProcessingFeeTypeId },
            { "sSettlementProcFProps_Dflp", DefaultSystemClosingCostFee.Hud800ProcessingFeeTypeId },
            { "sProcFProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud800ProcessingFeeTypeId },
            { "sProcFProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud800ProcessingFeeTypeId },
            { "sProcFProps_ToBroker", DefaultSystemClosingCostFee.Hud800ProcessingFeeTypeId },
            { "sProcFPaidTo", DefaultSystemClosingCostFee.Hud800ProcessingFeeTypeId },
            { "sUwF", DefaultSystemClosingCostFee.Hud800UnderwritingFeeTypeId },
            { "sUwFProps_PdByT", DefaultSystemClosingCostFee.Hud800UnderwritingFeeTypeId },
            { "sUwFProps_Apr", DefaultSystemClosingCostFee.Hud800UnderwritingFeeTypeId },
            { "sUwFProps_FhaAllow", DefaultSystemClosingCostFee.Hud800UnderwritingFeeTypeId },
            { "sUwFProps_Poc", DefaultSystemClosingCostFee.Hud800UnderwritingFeeTypeId },
            { "sSettlementUwFProps_Dflp", DefaultSystemClosingCostFee.Hud800UnderwritingFeeTypeId },
            { "sUwFProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud800UnderwritingFeeTypeId },
            { "sUwFProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud800UnderwritingFeeTypeId },
            { "sUwFProps_ToBroker", DefaultSystemClosingCostFee.Hud800UnderwritingFeeTypeId },
            { "sUwFPaidTo", DefaultSystemClosingCostFee.Hud800UnderwritingFeeTypeId },
            { "sWireF", DefaultSystemClosingCostFee.Hud800WireTransferFeeTypeId },
            { "sWireFProps_PdByT", DefaultSystemClosingCostFee.Hud800WireTransferFeeTypeId },
            { "sWireFProps_Apr", DefaultSystemClosingCostFee.Hud800WireTransferFeeTypeId },
            { "sWireFProps_FhaAllow", DefaultSystemClosingCostFee.Hud800WireTransferFeeTypeId },
            { "sWireFProps_Poc", DefaultSystemClosingCostFee.Hud800WireTransferFeeTypeId },
            { "sSettlementWireFProps_Dflp", DefaultSystemClosingCostFee.Hud800WireTransferFeeTypeId },
            { "sWireFProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud800WireTransferFeeTypeId },
            { "sWireFProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud800WireTransferFeeTypeId },
            { "sWireFProps_ToBroker", DefaultSystemClosingCostFee.Hud800WireTransferFeeTypeId },
            { "sWireFPaidTo", DefaultSystemClosingCostFee.Hud800WireTransferFeeTypeId },
            { "s800U1FDesc", DefaultSystemClosingCostFee.Hud800Custom1FeeTypeId },
            { "s800U1F", DefaultSystemClosingCostFee.Hud800Custom1FeeTypeId },
            { "s800U1FProps_PdByT", DefaultSystemClosingCostFee.Hud800Custom1FeeTypeId },
            { "s800U1FProps_Apr", DefaultSystemClosingCostFee.Hud800Custom1FeeTypeId },
            { "s800U1FProps_FhaAllow", DefaultSystemClosingCostFee.Hud800Custom1FeeTypeId },
            { "s800U1FProps_Poc", DefaultSystemClosingCostFee.Hud800Custom1FeeTypeId },
            { "sSettlement800U1FProps_Dflp", DefaultSystemClosingCostFee.Hud800Custom1FeeTypeId },
            { "s800U1FProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud800Custom1FeeTypeId },
            { "s800U1FProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud800Custom1FeeTypeId },
            { "s800U1FProps_ToBroker", DefaultSystemClosingCostFee.Hud800Custom1FeeTypeId },
            { "s800U1FPaidTo", DefaultSystemClosingCostFee.Hud800Custom1FeeTypeId },
            { "s800U1FGfeSection", DefaultSystemClosingCostFee.Hud800Custom1FeeTypeId },
            { "s800U2FDesc", DefaultSystemClosingCostFee.Hud800Custom2FeeTypeId },
            { "s800U2F", DefaultSystemClosingCostFee.Hud800Custom2FeeTypeId },
            { "s800U2FProps_PdByT", DefaultSystemClosingCostFee.Hud800Custom2FeeTypeId },
            { "s800U2FProps_Apr", DefaultSystemClosingCostFee.Hud800Custom2FeeTypeId },
            { "s800U2FProps_FhaAllow", DefaultSystemClosingCostFee.Hud800Custom2FeeTypeId },
            { "s800U2FProps_Poc", DefaultSystemClosingCostFee.Hud800Custom2FeeTypeId },
            { "sSettlement800U2FProps_Dflp", DefaultSystemClosingCostFee.Hud800Custom2FeeTypeId },
            { "s800U2FProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud800Custom2FeeTypeId },
            { "s800U2FProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud800Custom2FeeTypeId },
            { "s800U2FProps_ToBroker", DefaultSystemClosingCostFee.Hud800Custom2FeeTypeId },
            { "s800U2FPaidTo", DefaultSystemClosingCostFee.Hud800Custom2FeeTypeId },
            { "s800U2FGfeSection", DefaultSystemClosingCostFee.Hud800Custom2FeeTypeId },
            { "s800U3FDesc", DefaultSystemClosingCostFee.Hud800Custom3FeeTypeId },
            { "s800U3F", DefaultSystemClosingCostFee.Hud800Custom3FeeTypeId },
            { "s800U3FProps_PdByT", DefaultSystemClosingCostFee.Hud800Custom3FeeTypeId },
            { "s800U3FProps_Apr", DefaultSystemClosingCostFee.Hud800Custom3FeeTypeId },
            { "s800U3FProps_FhaAllow", DefaultSystemClosingCostFee.Hud800Custom3FeeTypeId },
            { "s800U3FProps_Poc", DefaultSystemClosingCostFee.Hud800Custom3FeeTypeId },
            { "sSettlement800U3FProps_Dflp", DefaultSystemClosingCostFee.Hud800Custom3FeeTypeId },
            { "s800U3FProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud800Custom3FeeTypeId },
            { "s800U3FProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud800Custom3FeeTypeId },
            { "s800U3FProps_ToBroker", DefaultSystemClosingCostFee.Hud800Custom3FeeTypeId },
            { "s800U3FPaidTo", DefaultSystemClosingCostFee.Hud800Custom3FeeTypeId },
            { "s800U3FGfeSection", DefaultSystemClosingCostFee.Hud800Custom3FeeTypeId },
            { "s800U4FDesc", DefaultSystemClosingCostFee.Hud800Custom4FeeTypeId },
            { "s800U4F", DefaultSystemClosingCostFee.Hud800Custom4FeeTypeId },
            { "s800U4FProps_PdByT", DefaultSystemClosingCostFee.Hud800Custom4FeeTypeId },
            { "s800U4FProps_Apr", DefaultSystemClosingCostFee.Hud800Custom4FeeTypeId },
            { "s800U4FProps_FhaAllow", DefaultSystemClosingCostFee.Hud800Custom4FeeTypeId },
            { "s800U4FProps_Poc", DefaultSystemClosingCostFee.Hud800Custom4FeeTypeId },
            { "sSettlement800U4FProps_Dflp", DefaultSystemClosingCostFee.Hud800Custom4FeeTypeId },
            { "s800U4FProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud800Custom4FeeTypeId },
            { "s800U4FProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud800Custom4FeeTypeId },
            { "s800U4FProps_ToBroker", DefaultSystemClosingCostFee.Hud800Custom4FeeTypeId },
            { "s800U4FPaidTo", DefaultSystemClosingCostFee.Hud800Custom4FeeTypeId },
            { "s800U4FGfeSection", DefaultSystemClosingCostFee.Hud800Custom4FeeTypeId },
            { "s800U5FDesc", DefaultSystemClosingCostFee.Hud800Custom5FeeTypeId },
            { "s800U5F", DefaultSystemClosingCostFee.Hud800Custom5FeeTypeId },
            { "s800U5FProps_PdByT", DefaultSystemClosingCostFee.Hud800Custom5FeeTypeId },
            { "s800U5FProps_Apr", DefaultSystemClosingCostFee.Hud800Custom5FeeTypeId },
            { "s800U5FProps_FhaAllow", DefaultSystemClosingCostFee.Hud800Custom5FeeTypeId },
            { "s800U5FProps_Poc", DefaultSystemClosingCostFee.Hud800Custom5FeeTypeId },
            { "sSettlement800U5FProps_Dflp", DefaultSystemClosingCostFee.Hud800Custom5FeeTypeId },
            { "s800U5FProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud800Custom5FeeTypeId },
            { "s800U5FProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud800Custom5FeeTypeId },
            { "s800U5FProps_ToBroker", DefaultSystemClosingCostFee.Hud800Custom5FeeTypeId },
            { "s800U5FPaidTo", DefaultSystemClosingCostFee.Hud800Custom5FeeTypeId },
            { "s800U5FGfeSection", DefaultSystemClosingCostFee.Hud800Custom5FeeTypeId },
            { "sIPiaDy", DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId },
            { "sIPiaProps_PdByT", DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId },
            { "sIPiaProps_Apr", DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId },
            { "sIPiaProps_FhaAllow", DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId },
            { "sIPiaProps_Poc", DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId },
            { "sSettlementIPiaProps_Dflp", DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId },
            { "sIPiaProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId },
            { "sIPiaProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId },
            { "sMipPiaProps_PdByT", DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId },
            { "sMipPiaProps_Apr", DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId },
            { "sMipPiaProps_FhaAllow", DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId },
            { "sMipPiaProps_Poc", DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId },
            { "sMipPiaProps_Dflp", DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId },
            { "sMipPiaProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId },
            { "sMipPiaProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId },
            { "sMipPiaPaidTo", DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId },
            { "sProHazInsR", DefaultSystemClosingCostFee.Hud900HazardInsuranceFeeTypeId },
            { "sProHazInsT", DefaultSystemClosingCostFee.Hud900HazardInsuranceFeeTypeId },
            { "sProHazInsMb", DefaultSystemClosingCostFee.Hud900HazardInsuranceFeeTypeId },
            { "sHazInsPiaMon", DefaultSystemClosingCostFee.Hud900HazardInsuranceFeeTypeId },
            { "sHazInsRsrvEscrowedTri", DefaultSystemClosingCostFee.Hud900HazardInsuranceFeeTypeId },
            { "sHazInsPiaProps_PdByT", DefaultSystemClosingCostFee.Hud900HazardInsuranceFeeTypeId },
            { "sHazInsPiaProps_Apr", DefaultSystemClosingCostFee.Hud900HazardInsuranceFeeTypeId },
            { "sHazInsPiaProps_FhaAllow", DefaultSystemClosingCostFee.Hud900HazardInsuranceFeeTypeId },
            { "sHazInsPiaProps_Poc", DefaultSystemClosingCostFee.Hud900HazardInsuranceFeeTypeId },
            { "sSettlementHazInsPiaProps_Dflp", DefaultSystemClosingCostFee.Hud900HazardInsuranceFeeTypeId },
            { "sHazInsPiaProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud900HazardInsuranceFeeTypeId },
            { "sHazInsPiaProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud900HazardInsuranceFeeTypeId },
            { "sHazInsPiaPaidTo", DefaultSystemClosingCostFee.Hud900HazardInsuranceFeeTypeId },
            { "s904PiaDesc", DefaultSystemClosingCostFee.Hud900Custom1FeeTypeId },
            { "s904Pia", DefaultSystemClosingCostFee.Hud900Custom1FeeTypeId },
            { "s904PiaProps_PdByT", DefaultSystemClosingCostFee.Hud900Custom1FeeTypeId },
            { "s904PiaProps_Apr", DefaultSystemClosingCostFee.Hud900Custom1FeeTypeId },
            { "s904PiaProps_FhaAllow", DefaultSystemClosingCostFee.Hud900Custom1FeeTypeId },
            { "s904PiaProps_Poc", DefaultSystemClosingCostFee.Hud900Custom1FeeTypeId },
            { "sSettlement904PiaProps_Dflp", DefaultSystemClosingCostFee.Hud900Custom1FeeTypeId },
            { "s904PiaProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud900Custom1FeeTypeId },
            { "s904PiaProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud900Custom1FeeTypeId },
            { "s904PiaGfeSection", DefaultSystemClosingCostFee.Hud900Custom1FeeTypeId },
            { "sVaFfProps_PdByT", DefaultSystemClosingCostFee.Hud900VAFundingFeeTypeId },
            { "sVaFfProps_Apr", DefaultSystemClosingCostFee.Hud900VAFundingFeeTypeId },
            { "sVaFfProps_FhaAllow", DefaultSystemClosingCostFee.Hud900VAFundingFeeTypeId },
            { "sVaFfProps_Poc", DefaultSystemClosingCostFee.Hud900VAFundingFeeTypeId },
            { "sVaFfProps_Dflp", DefaultSystemClosingCostFee.Hud900VAFundingFeeTypeId },
            { "sVaFfProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud900VAFundingFeeTypeId },
            { "sVaFfProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud900VAFundingFeeTypeId },
            { "sVaFfPaidTo", DefaultSystemClosingCostFee.Hud900VAFundingFeeTypeId },
            { "s900U1PiaDesc", DefaultSystemClosingCostFee.Hud900Custom2FeeTypeId },
            { "s900U1Pia", DefaultSystemClosingCostFee.Hud900Custom2FeeTypeId },
            { "s900U1PiaProps_PdByT", DefaultSystemClosingCostFee.Hud900Custom2FeeTypeId },
            { "s900U1PiaProps_Apr", DefaultSystemClosingCostFee.Hud900Custom2FeeTypeId },
            { "s900U1PiaProps_FhaAllow", DefaultSystemClosingCostFee.Hud900Custom2FeeTypeId },
            { "s900U1PiaProps_Poc", DefaultSystemClosingCostFee.Hud900Custom2FeeTypeId },
            { "sSettlement900U1PiaProps_Dflp", DefaultSystemClosingCostFee.Hud900Custom2FeeTypeId },
            { "s900U1PiaProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud900Custom2FeeTypeId },
            { "s900U1PiaProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud900Custom2FeeTypeId },
            { "s900U1PiaGfeSection", DefaultSystemClosingCostFee.Hud900Custom2FeeTypeId },
            { "sHazInsRsrvMon", DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId },
            { "sHazInsRsrvProps_PdByT", DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId },
            { "sHazInsRsrvProps_Apr", DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId },
            { "sHazInsRsrvProps_FhaAllow", DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId },
            { "sHazInsRsrvProps_Poc", DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId },
            { "sSettlementHazInsRsrvProps_Dflp", DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId },
            { "sHazInsRsrvProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId },
            { "sHazInsRsrvProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId },
            { "sHazInsRsrvEscrowCushion", DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId },
            { "sHazInsRsrvEscrowDisbursementSchedule", DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId },
            { "sMInsRsrvMon", DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId },
            { "sMInsRsrvProps_PdByT", DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId },
            { "sMInsRsrvProps_Apr", DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId },
            { "sMInsRsrvProps_FhaAllow", DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId },
            { "sMInsRsrvProps_Poc", DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId },
            { "sMInsRsrvEscrowedTri" , DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId },
            { "sSettlementMInsRsrvProps_Dflp", DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId },
            { "sMInsRsrvProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId },
            { "sMInsRsrvProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId },
            { "sMInsRsrvEscrowCushion", DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId },
            { "sMInsRsrvEscrowDisbursementSchedule", DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId },
            { "sProRealETxR", DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId },
            { "sProRealETxT", DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId },
            { "sProRealETxMb", DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId },
            { "sRealETxRsrvMon", DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId },
            { "sRealETxRsrvProps_PdByT", DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId },
            { "sRealETxRsrvProps_Apr", DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId },
            { "sRealETxRsrvProps_FhaAllow", DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId },
            { "sRealETxRsrvProps_Poc", DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId },
            { "sRealETxRsrvEscrowedTri" , DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId },
            { "sSettlementRealETxRsrvProps_Dflp", DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId },
            { "sRealETxRsrvProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId },
            { "sRealETxRsrvProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId },
            { "sRealETxRsrvEscrowCushion", DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId },
            { "sRealETxRsrvEscrowDisbursementSchedule", DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId },
            { "sSchoolTxRsrvMon", DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId },
            { "sProSchoolTx", DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId },
            { "sSchoolTxRsrvProps_PdByT", DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId },
            { "sSchoolTxRsrvProps_Apr", DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId },
            { "sSchoolTxRsrvProps_FhaAllow", DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId },
            { "sSchoolTxRsrvProps_Poc", DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId },
            { "sSchoolTxRsrvEscrowedTri" , DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId },
            { "sSettlementSchoolTxRsrvProps_Dflp", DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId },
            { "sSchoolTxRsrvProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId },
            { "sSchoolTxRsrvProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId },
            { "sSchoolTxRsrvEscrowCushion", DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId },
            { "sSchoolTxRsrvEscrowDisbursementSchedule", DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId },
            { "sFloodInsRsrvMon", DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId },
            { "sProFloodIns", DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId },
            { "sFloodInsRsrvProps_PdByT", DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId },
            { "sFloodInsRsrvProps_Apr", DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId },
            { "sFloodInsRsrvProps_FhaAllow", DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId },
            { "sFloodInsRsrvProps_Poc", DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId },
            { "sFloodInsRsrvEscrowedTri" , DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId },
            { "sSettlementFloodInsRsrvProps_Dflp", DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId },
            { "sFloodInsRsrvProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId },
            { "sFloodInsRsrvProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId },
            { "sFloodInsRsrvEscrowCushion", DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId },
            { "sFloodInsRsrvEscrowDisbursementSchedule", DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId },
            { "sAggregateAdjRsrvProps_PdByT", DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId },
            { "sAggregateAdjRsrvProps_Apr", DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId },
            { "sAggregateAdjRsrvProps_FhaAllow", DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId },
            { "sAggregateAdjRsrvProps_Poc", DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId },
            { "sSettlementAggregateAdjRsrvProps_Dflp", DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId },
            { "sAggregateAdjRsrvProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId },
            { "sAggregateAdjRsrvProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId },
            { "s1006ProHExpDesc", DefaultSystemClosingCostFee.Hud1000Custom1FeeTypeId },
            { "s1006RsrvMon", DefaultSystemClosingCostFee.Hud1000Custom1FeeTypeId },
            { "s1006ProHExp", DefaultSystemClosingCostFee.Hud1000Custom1FeeTypeId },
            { "s1006RsrvProps_PdByT", DefaultSystemClosingCostFee.Hud1000Custom1FeeTypeId} ,
            { "s1006RsrvProps_Apr", DefaultSystemClosingCostFee.Hud1000Custom1FeeTypeId },
            { "s1006RsrvProps_FhaAllow", DefaultSystemClosingCostFee.Hud1000Custom1FeeTypeId },
            { "s1006RsrvProps_Poc", DefaultSystemClosingCostFee.Hud1000Custom1FeeTypeId },
            { "s1006RsrvEscrowedTri", DefaultSystemClosingCostFee.Hud1000Custom1FeeTypeId },
            { "sSettlement1008RsrvProps_Dflp", DefaultSystemClosingCostFee.Hud1000Custom1FeeTypeId },
            { "s1006RsrvProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1000Custom1FeeTypeId },
            { "s1006RsrvProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1000Custom1FeeTypeId },
            { "s1006RsrvEscrowCushion", DefaultSystemClosingCostFee.Hud1000Custom1FeeTypeId },
            { "s1006RsrvEscrowDisbursementSchedule", DefaultSystemClosingCostFee.Hud1000Custom1FeeTypeId },
            { "s1007ProHExpDesc", DefaultSystemClosingCostFee.Hud1000Custom2FeeTypeId },
            { "s1007RsrvMon", DefaultSystemClosingCostFee.Hud1000Custom2FeeTypeId },
            { "s1007ProHExp", DefaultSystemClosingCostFee.Hud1000Custom2FeeTypeId },
            { "s1007RsrvProps_PdByT", DefaultSystemClosingCostFee.Hud1000Custom2FeeTypeId },
            { "s1007RsrvProps_Apr", DefaultSystemClosingCostFee.Hud1000Custom2FeeTypeId },
            { "s1007RsrvProps_FhaAllow", DefaultSystemClosingCostFee.Hud1000Custom2FeeTypeId },
            { "s1007RsrvProps_Poc", DefaultSystemClosingCostFee.Hud1000Custom2FeeTypeId },
            { "s1007RsrvEscrowedTri", DefaultSystemClosingCostFee.Hud1000Custom2FeeTypeId },
            { "sSettlement1009RsrvProps_Dflp", DefaultSystemClosingCostFee.Hud1000Custom2FeeTypeId },
            { "s1007RsrvProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1000Custom2FeeTypeId },
            { "s1007RsrvProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1000Custom2FeeTypeId },
            { "s1007RsrvEscrowCushion", DefaultSystemClosingCostFee.Hud1000Custom2FeeTypeId },
            { "s1007RsrvEscrowDisbursementSchedule", DefaultSystemClosingCostFee.Hud1000Custom2FeeTypeId },
            { "sU3RsrvDesc", DefaultSystemClosingCostFee.Hud1000Custom3FeeTypeId },
            { "sU3RsrvMon", DefaultSystemClosingCostFee.Hud1000Custom3FeeTypeId },
            { "sProU3Rsrv", DefaultSystemClosingCostFee.Hud1000Custom3FeeTypeId },
            { "sU3RsrvEscrowedTri", DefaultSystemClosingCostFee.Hud1000Custom3FeeTypeId },
            { "sU3RsrvProps_PdByT", DefaultSystemClosingCostFee.Hud1000Custom3FeeTypeId },
            { "sU3RsrvProps_Apr", DefaultSystemClosingCostFee.Hud1000Custom3FeeTypeId },
            { "sU3RsrvProps_FhaAllow", DefaultSystemClosingCostFee.Hud1000Custom3FeeTypeId },
            { "sU3RsrvProps_Poc", DefaultSystemClosingCostFee.Hud1000Custom3FeeTypeId },
            { "sSettlementU3RsrvProps_Dflp", DefaultSystemClosingCostFee.Hud1000Custom3FeeTypeId },
            { "sU3RsrvProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1000Custom3FeeTypeId },
            { "sU3RsrvProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1000Custom3FeeTypeId },
            { "sU3RsrvEscrowCushion", DefaultSystemClosingCostFee.Hud1000Custom3FeeTypeId },
            { "sU3RsrvEscrowDisbursementSchedule", DefaultSystemClosingCostFee.Hud1000Custom3FeeTypeId },
            { "sU4RsrvDesc", DefaultSystemClosingCostFee.Hud1000Custom4FeeTypeId },
            { "sU4RsrvMon", DefaultSystemClosingCostFee.Hud1000Custom4FeeTypeId },
            { "sProU4Rsrv", DefaultSystemClosingCostFee.Hud1000Custom4FeeTypeId },
            { "sU4RsrvEscrowedTri" , DefaultSystemClosingCostFee.Hud1000Custom4FeeTypeId },
            { "sU4RsrvProps_PdByT", DefaultSystemClosingCostFee.Hud1000Custom4FeeTypeId },
            { "sU4RsrvProps_Apr", DefaultSystemClosingCostFee.Hud1000Custom4FeeTypeId },
            { "sU4RsrvProps_FhaAllow", DefaultSystemClosingCostFee.Hud1000Custom4FeeTypeId },
            { "sU4RsrvProps_Poc", DefaultSystemClosingCostFee.Hud1000Custom4FeeTypeId },
            { "sSettlementU4RsrvProps_Dflp", DefaultSystemClosingCostFee.Hud1000Custom4FeeTypeId },
            { "sU4RsrvProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1000Custom4FeeTypeId },
            { "sU4RsrvProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1000Custom4FeeTypeId },
            { "sU4RsrvEscrowCushion", DefaultSystemClosingCostFee.Hud1000Custom4FeeTypeId },
            { "sU4RsrvEscrowDisbursementSchedule", DefaultSystemClosingCostFee.Hud1000Custom4FeeTypeId },
            { "sEscrowFPc", DefaultSystemClosingCostFee.Hud1100ClosingEscrowFeeTypeId },
            { "sEscrowFBaseT", DefaultSystemClosingCostFee.Hud1100ClosingEscrowFeeTypeId },
            { "sEscrowFMb", DefaultSystemClosingCostFee.Hud1100ClosingEscrowFeeTypeId },
            { "sEscrowFProps_PdByT", DefaultSystemClosingCostFee.Hud1100ClosingEscrowFeeTypeId },
            { "sEscrowFProps_Apr", DefaultSystemClosingCostFee.Hud1100ClosingEscrowFeeTypeId },
            { "sEscrowFProps_FhaAllow", DefaultSystemClosingCostFee.Hud1100ClosingEscrowFeeTypeId },
            { "sEscrowFProps_Poc", DefaultSystemClosingCostFee.Hud1100ClosingEscrowFeeTypeId },
            { "sSettlementEscrowFProps_Dflp", DefaultSystemClosingCostFee.Hud1100ClosingEscrowFeeTypeId },
            { "sEscrowFProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1100ClosingEscrowFeeTypeId },
            { "sEscrowFProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1100ClosingEscrowFeeTypeId },
            { "sEscrowFProps_ToBroker", DefaultSystemClosingCostFee.Hud1100ClosingEscrowFeeTypeId },
            { "sEscrowFTable", DefaultSystemClosingCostFee.Hud1100ClosingEscrowFeeTypeId },
            { "sEscrowFGfeSection", DefaultSystemClosingCostFee.Hud1100ClosingEscrowFeeTypeId },
            { "sOwnerTitleInsFPc", DefaultSystemClosingCostFee.Hud1100OwnerTitleInsuranceFeeTypeId },
            { "sOwnerTitleInsFBaseT", DefaultSystemClosingCostFee.Hud1100OwnerTitleInsuranceFeeTypeId },
            { "sOwnerTitleInsFMb", DefaultSystemClosingCostFee.Hud1100OwnerTitleInsuranceFeeTypeId },
            { "sOwnerTitleInsProps_PdByT", DefaultSystemClosingCostFee.Hud1100OwnerTitleInsuranceFeeTypeId },
            { "sOwnerTitleInsProps_Apr", DefaultSystemClosingCostFee.Hud1100OwnerTitleInsuranceFeeTypeId },
            { "sOwnerTitleInsProps_FhaAllow", DefaultSystemClosingCostFee.Hud1100OwnerTitleInsuranceFeeTypeId },
            { "sOwnerTitleInsProps_Poc", DefaultSystemClosingCostFee.Hud1100OwnerTitleInsuranceFeeTypeId },
            { "sSettlementOwnerTitleInsFProps_Dflp", DefaultSystemClosingCostFee.Hud1100OwnerTitleInsuranceFeeTypeId },
            { "sOwnerTitleInsProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1100OwnerTitleInsuranceFeeTypeId },
            { "sOwnerTitleInsProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1100OwnerTitleInsuranceFeeTypeId },
            { "sOwnerTitleInsProps_ToBroker", DefaultSystemClosingCostFee.Hud1100OwnerTitleInsuranceFeeTypeId },
            { "sOwnerTitleInsPaidTo", DefaultSystemClosingCostFee.Hud1100OwnerTitleInsuranceFeeTypeId },
            { "sTitleInsFPc", DefaultSystemClosingCostFee.Hud1100LenderTitleInsuranceFeeTypeId },
            { "sTitleInsFBaseT", DefaultSystemClosingCostFee.Hud1100LenderTitleInsuranceFeeTypeId },
            { "sTitleInsFMb", DefaultSystemClosingCostFee.Hud1100LenderTitleInsuranceFeeTypeId },
            { "sTitleInsFProps_PdByT", DefaultSystemClosingCostFee.Hud1100LenderTitleInsuranceFeeTypeId },
            { "sTitleInsFProps_Apr", DefaultSystemClosingCostFee.Hud1100LenderTitleInsuranceFeeTypeId },
            { "sTitleInsFProps_FhaAllow", DefaultSystemClosingCostFee.Hud1100LenderTitleInsuranceFeeTypeId },
            { "sTitleInsFProps_Poc", DefaultSystemClosingCostFee.Hud1100LenderTitleInsuranceFeeTypeId },
            { "sSettlementTitleInsFProps_Dflp", DefaultSystemClosingCostFee.Hud1100LenderTitleInsuranceFeeTypeId },
            { "sTitleInsFProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1100LenderTitleInsuranceFeeTypeId },
            { "sTitleInsFProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1100LenderTitleInsuranceFeeTypeId },
            { "sTitleInsFProps_ToBroker", DefaultSystemClosingCostFee.Hud1100LenderTitleInsuranceFeeTypeId },
            { "sTitleInsFTable", DefaultSystemClosingCostFee.Hud1100LenderTitleInsuranceFeeTypeId },
            { "sDocPrepF", DefaultSystemClosingCostFee.Hud1100DocPreparationFeeTypeId },
            { "sDocPrepFProps_PdByT", DefaultSystemClosingCostFee.Hud1100DocPreparationFeeTypeId },
            { "sDocPrepFProps_Apr", DefaultSystemClosingCostFee.Hud1100DocPreparationFeeTypeId },
            { "sDocPrepFProps_FhaAllow", DefaultSystemClosingCostFee.Hud1100DocPreparationFeeTypeId },
            { "sDocPrepFProps_Poc", DefaultSystemClosingCostFee.Hud1100DocPreparationFeeTypeId },
            { "sSettlementDocPrepFProps_Dflp", DefaultSystemClosingCostFee.Hud1100DocPreparationFeeTypeId },
            { "sDocPrepFProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1100DocPreparationFeeTypeId },
            { "sDocPrepFProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1100DocPreparationFeeTypeId },
            { "sDocPrepFProps_ToBroker", DefaultSystemClosingCostFee.Hud1100DocPreparationFeeTypeId },
            { "sDocPrepFPaidTo", DefaultSystemClosingCostFee.Hud1100DocPreparationFeeTypeId },
            { "sDocPrepFGfeSection", DefaultSystemClosingCostFee.Hud1100DocPreparationFeeTypeId },
            { "sNotaryF", DefaultSystemClosingCostFee.Hud1100NotaryFeeTypeId },
            { "sNotaryFProps_PdByT", DefaultSystemClosingCostFee.Hud1100NotaryFeeTypeId },
            { "sNotaryFProps_Apr", DefaultSystemClosingCostFee.Hud1100NotaryFeeTypeId },
            { "sNotaryFProps_FhaAllow", DefaultSystemClosingCostFee.Hud1100NotaryFeeTypeId },
            { "sNotaryFProps_Poc", DefaultSystemClosingCostFee.Hud1100NotaryFeeTypeId },
            { "sSettlementNotaryFProps_Dflp", DefaultSystemClosingCostFee.Hud1100NotaryFeeTypeId },
            { "sNotaryFProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1100NotaryFeeTypeId },
            { "sNotaryFProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1100NotaryFeeTypeId },
            { "sNotaryFProps_ToBroker", DefaultSystemClosingCostFee.Hud1100NotaryFeeTypeId },
            { "sNotaryFPaidTo", DefaultSystemClosingCostFee.Hud1100NotaryFeeTypeId },
            { "sNotaryFGfeSection", DefaultSystemClosingCostFee.Hud1100NotaryFeeTypeId },
            { "sAttorneyF", DefaultSystemClosingCostFee.Hud1100AttorneyFeeTypeId },
            { "sAttorneyFProps_PdByT", DefaultSystemClosingCostFee.Hud1100AttorneyFeeTypeId },
            { "sAttorneyFProps_Apr", DefaultSystemClosingCostFee.Hud1100AttorneyFeeTypeId },
            { "sAttorneyFProps_FhaAllow", DefaultSystemClosingCostFee.Hud1100AttorneyFeeTypeId },
            { "sAttorneyFProps_Poc", DefaultSystemClosingCostFee.Hud1100AttorneyFeeTypeId },
            { "sSettlementAttorneyFProps_Dflp", DefaultSystemClosingCostFee.Hud1100AttorneyFeeTypeId },
            { "sAttorneyFProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1100AttorneyFeeTypeId },
            { "sAttorneyFProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1100AttorneyFeeTypeId },
            { "sAttorneyFProps_ToBroker", DefaultSystemClosingCostFee.Hud1100AttorneyFeeTypeId },
            { "sAttorneyFPaidTo", DefaultSystemClosingCostFee.Hud1100AttorneyFeeTypeId },
            { "sAttorneyFGfeSection", DefaultSystemClosingCostFee.Hud1100AttorneyFeeTypeId },
            { "sU1TcDesc", DefaultSystemClosingCostFee.Hud1100Custom1FeeTypeId },
            { "sU1Tc", DefaultSystemClosingCostFee.Hud1100Custom1FeeTypeId },
            { "sU1TcProps_PdByT", DefaultSystemClosingCostFee.Hud1100Custom1FeeTypeId },
            { "sU1TcProps_Apr", DefaultSystemClosingCostFee.Hud1100Custom1FeeTypeId },
            { "sU1TcProps_FhaAllow", DefaultSystemClosingCostFee.Hud1100Custom1FeeTypeId },
            { "sU1TcProps_Poc", DefaultSystemClosingCostFee.Hud1100Custom1FeeTypeId },
            { "sSettlementU1TcProps_Dflp", DefaultSystemClosingCostFee.Hud1100Custom1FeeTypeId },
            { "sU1TcProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1100Custom1FeeTypeId },
            { "sU1TcProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1100Custom1FeeTypeId },
            { "sU1TcProps_ToBroker", DefaultSystemClosingCostFee.Hud1100Custom1FeeTypeId },
            { "sU1TcPaidTo", DefaultSystemClosingCostFee.Hud1100Custom1FeeTypeId },
            { "sU1TcGfeSection", DefaultSystemClosingCostFee.Hud1100Custom1FeeTypeId },
            { "sU2TcDesc", DefaultSystemClosingCostFee.Hud1100Custom2FeeTypeId },
            { "sU2Tc", DefaultSystemClosingCostFee.Hud1100Custom2FeeTypeId },
            { "sU2TcProps_PdByT", DefaultSystemClosingCostFee.Hud1100Custom2FeeTypeId },
            { "sU2TcProps_Apr", DefaultSystemClosingCostFee.Hud1100Custom2FeeTypeId },
            { "sU2TcProps_FhaAllow", DefaultSystemClosingCostFee.Hud1100Custom2FeeTypeId },
            { "sU2TcProps_Poc", DefaultSystemClosingCostFee.Hud1100Custom2FeeTypeId },
            { "sSettlementU2TcProps_Dflp", DefaultSystemClosingCostFee.Hud1100Custom2FeeTypeId },
            { "sU2TcProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1100Custom2FeeTypeId },
            { "sU2TcProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1100Custom2FeeTypeId },
            { "sU2TcProps_ToBroker", DefaultSystemClosingCostFee.Hud1100Custom2FeeTypeId },
            { "sU2TcPaidTo", DefaultSystemClosingCostFee.Hud1100Custom2FeeTypeId },
            { "sU2TcGfeSection", DefaultSystemClosingCostFee.Hud1100Custom2FeeTypeId },
            { "sU3TcDesc", DefaultSystemClosingCostFee.Hud1100Custom3FeeTypeId },
            { "sU3Tc", DefaultSystemClosingCostFee.Hud1100Custom3FeeTypeId },
            { "sU3TcProps_PdByT", DefaultSystemClosingCostFee.Hud1100Custom3FeeTypeId },
            { "sU3TcProps_Apr", DefaultSystemClosingCostFee.Hud1100Custom3FeeTypeId },
            { "sU3TcProps_FhaAllow", DefaultSystemClosingCostFee.Hud1100Custom3FeeTypeId },
            { "sU3TcProps_Poc", DefaultSystemClosingCostFee.Hud1100Custom3FeeTypeId },
            { "sSettlementU3TcProps_Dflp", DefaultSystemClosingCostFee.Hud1100Custom3FeeTypeId },
            { "sU3TcProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1100Custom3FeeTypeId },
            { "sU3TcProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1100Custom3FeeTypeId },
            { "sU3TcProps_ToBroker", DefaultSystemClosingCostFee.Hud1100Custom3FeeTypeId },
            { "sU3TcPaidTo", DefaultSystemClosingCostFee.Hud1100Custom3FeeTypeId },
            { "sU3TcGfeSection", DefaultSystemClosingCostFee.Hud1100Custom3FeeTypeId },
            { "sU4TcDesc", DefaultSystemClosingCostFee.Hud1100Custom4FeeTypeId },
            { "sU4Tc", DefaultSystemClosingCostFee.Hud1100Custom4FeeTypeId },
            { "sU4TcProps_PdByT", DefaultSystemClosingCostFee.Hud1100Custom4FeeTypeId },
            { "sU4TcProps_Apr", DefaultSystemClosingCostFee.Hud1100Custom4FeeTypeId },
            { "sU4TcProps_FhaAllow", DefaultSystemClosingCostFee.Hud1100Custom4FeeTypeId },
            { "sU4TcProps_Poc", DefaultSystemClosingCostFee.Hud1100Custom4FeeTypeId },
            { "sSettlementU4TcProps_Dflp", DefaultSystemClosingCostFee.Hud1100Custom4FeeTypeId },
            { "sU4TcProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1100Custom4FeeTypeId },
            { "sU4TcProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1100Custom4FeeTypeId },
            { "sU4TcProps_ToBroker", DefaultSystemClosingCostFee.Hud1100Custom4FeeTypeId },
            { "sU4TcPaidTo", DefaultSystemClosingCostFee.Hud1100Custom4FeeTypeId },
            { "sU4TcGfeSection", DefaultSystemClosingCostFee.Hud1100Custom4FeeTypeId },
            { "sRecFPc", DefaultSystemClosingCostFee.Hud1200RecordingFeeTypeId },
            { "sRecBaseT", DefaultSystemClosingCostFee.Hud1200RecordingFeeTypeId },
            { "sRecFMb", DefaultSystemClosingCostFee.Hud1200RecordingFeeTypeId },
            { "sRecFLckd", DefaultSystemClosingCostFee.Hud1200RecordingFeeTypeId },
            { "sRecDeed", DefaultSystemClosingCostFee.Hud1200DeedFeeTypeId },
            { "sRecMortgage", DefaultSystemClosingCostFee.Hud1200MortgageFeeTypeId },
            { "sRecRelease", DefaultSystemClosingCostFee.Hud1200ReleaseFeeTypeId },
            { "sRecFProps_PdByT", DefaultSystemClosingCostFee.Hud1200RecordingFeeTypeId },
            { "sRecFProps_Apr", DefaultSystemClosingCostFee.Hud1200RecordingFeeTypeId },
            { "sRecFProps_FhaAllow", DefaultSystemClosingCostFee.Hud1200RecordingFeeTypeId },
            { "sRecFProps_Poc", DefaultSystemClosingCostFee.Hud1200RecordingFeeTypeId },
            { "sSettlementRecFProps_Dflp", DefaultSystemClosingCostFee.Hud1200RecordingFeeTypeId },
            { "sRecFProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1200RecordingFeeTypeId },
            { "sRecFProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1200RecordingFeeTypeId },
            { "sRecFDesc", DefaultSystemClosingCostFee.Hud1200RecordingFeeTypeId },
            { "sCountyRtcPc", DefaultSystemClosingCostFee.Hud1200CountyTaxStampsFeeTypeId },
            { "sCountyRtcBaseT", DefaultSystemClosingCostFee.Hud1200CountyTaxStampsFeeTypeId },
            { "sCountyRtcMb", DefaultSystemClosingCostFee.Hud1200CountyTaxStampsFeeTypeId },
            { "sCountyRtcProps_PdByT", DefaultSystemClosingCostFee.Hud1200CountyTaxStampsFeeTypeId },
            { "sCountyRtcProps_Apr", DefaultSystemClosingCostFee.Hud1200CountyTaxStampsFeeTypeId },
            { "sCountyRtcProps_FhaAllow", DefaultSystemClosingCostFee.Hud1200CountyTaxStampsFeeTypeId },
            { "sCountyRtcProps_Poc", DefaultSystemClosingCostFee.Hud1200CountyTaxStampsFeeTypeId },
            { "sSettlementCountyRtcProps_Dflp", DefaultSystemClosingCostFee.Hud1200CountyTaxStampsFeeTypeId },
            { "sCountyRtcProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1200CountyTaxStampsFeeTypeId },
            { "sCountyRtcProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1200CountyTaxStampsFeeTypeId },
            { "sCountyRtcDesc", DefaultSystemClosingCostFee.Hud1200CountyTaxStampsFeeTypeId },
            { "sStateRtcPc", DefaultSystemClosingCostFee.Hud1200StateTaxStampsFeeTypeId },
            { "sStateRtcBaseT", DefaultSystemClosingCostFee.Hud1200StateTaxStampsFeeTypeId },
            { "sStateRtcMb", DefaultSystemClosingCostFee.Hud1200StateTaxStampsFeeTypeId },
            { "sStateRtcProps_PdByT", DefaultSystemClosingCostFee.Hud1200StateTaxStampsFeeTypeId },
            { "sStateRtcProps_Apr", DefaultSystemClosingCostFee.Hud1200StateTaxStampsFeeTypeId },
            { "sStateRtcProps_FhaAllow", DefaultSystemClosingCostFee.Hud1200StateTaxStampsFeeTypeId },
            { "sStateRtcProps_Poc", DefaultSystemClosingCostFee.Hud1200StateTaxStampsFeeTypeId },
            { "sSettlementStateRtcProps_Dflp", DefaultSystemClosingCostFee.Hud1200StateTaxStampsFeeTypeId },
            { "sStateRtcProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1200StateTaxStampsFeeTypeId },
            { "sStateRtcProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1200StateTaxStampsFeeTypeId },
            { "sStateRtcDesc", DefaultSystemClosingCostFee.Hud1200StateTaxStampsFeeTypeId },
            { "sU1GovRtcDesc", DefaultSystemClosingCostFee.Hud1200Custom1FeeTypeId },
            { "sU1GovRtcPc", DefaultSystemClosingCostFee.Hud1200Custom1FeeTypeId },
            { "sU1GovRtcBaseT", DefaultSystemClosingCostFee.Hud1200Custom1FeeTypeId },
            { "sU1GovRtcMb", DefaultSystemClosingCostFee.Hud1200Custom1FeeTypeId },
            { "sU1GovRtcGfeSection", DefaultSystemClosingCostFee.Hud1200Custom1FeeTypeId },
            { "sU1GovRtcProps_PdByT", DefaultSystemClosingCostFee.Hud1200Custom1FeeTypeId },
            { "sU1GovRtcProps_Apr", DefaultSystemClosingCostFee.Hud1200Custom1FeeTypeId },
            { "sU1GovRtcProps_FhaAllow", DefaultSystemClosingCostFee.Hud1200Custom1FeeTypeId },
            { "sU1GovRtcProps_Poc", DefaultSystemClosingCostFee.Hud1200Custom1FeeTypeId },
            { "sSettlementU1GovRtcProps_Dflp", DefaultSystemClosingCostFee.Hud1200Custom1FeeTypeId },
            { "sU1GovRtcProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1200Custom1FeeTypeId },
            { "sU1GovRtcProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1200Custom1FeeTypeId },
            { "sU1GovRtcPaidTo", DefaultSystemClosingCostFee.Hud1200Custom1FeeTypeId },
            { "sU2GovRtcDesc", DefaultSystemClosingCostFee.Hud1200Custom2FeeTypeId },
            { "sU2GovRtcPc", DefaultSystemClosingCostFee.Hud1200Custom2FeeTypeId },
            { "sU2GovRtcBaseT", DefaultSystemClosingCostFee.Hud1200Custom2FeeTypeId },
            { "sU2GovRtcMb", DefaultSystemClosingCostFee.Hud1200Custom2FeeTypeId },
            { "sU2GovRtcGfeSection", DefaultSystemClosingCostFee.Hud1200Custom2FeeTypeId },
            { "sU2GovRtcProps_PdByT", DefaultSystemClosingCostFee.Hud1200Custom2FeeTypeId },
            { "sU2GovRtcProps_Apr", DefaultSystemClosingCostFee.Hud1200Custom2FeeTypeId },
            { "sU2GovRtcProps_FhaAllow", DefaultSystemClosingCostFee.Hud1200Custom2FeeTypeId },
            { "sU2GovRtcProps_Poc", DefaultSystemClosingCostFee.Hud1200Custom2FeeTypeId },
            { "sSettlementU2GovRtcProps_Dflp", DefaultSystemClosingCostFee.Hud1200Custom2FeeTypeId },
            { "sU2GovRtcProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1200Custom2FeeTypeId },
            { "sU2GovRtcProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1200Custom2FeeTypeId },
            { "sU2GovRtcPaidTo", DefaultSystemClosingCostFee.Hud1200Custom2FeeTypeId },
            { "sU3GovRtcDesc", DefaultSystemClosingCostFee.Hud1200Custom3FeeTypeId },
            { "sU3GovRtcPc", DefaultSystemClosingCostFee.Hud1200Custom3FeeTypeId },
            { "sU3GovRtcBaseT", DefaultSystemClosingCostFee.Hud1200Custom3FeeTypeId },
            { "sU3GovRtcMb", DefaultSystemClosingCostFee.Hud1200Custom3FeeTypeId },
            { "sU3GovRtcGfeSection", DefaultSystemClosingCostFee.Hud1200Custom3FeeTypeId },
            { "sU3GovRtcProps_PdByT", DefaultSystemClosingCostFee.Hud1200Custom3FeeTypeId },
            { "sU3GovRtcProps_Apr", DefaultSystemClosingCostFee.Hud1200Custom3FeeTypeId },
            { "sU3GovRtcProps_FhaAllow", DefaultSystemClosingCostFee.Hud1200Custom3FeeTypeId },
            { "sU3GovRtcProps_Poc", DefaultSystemClosingCostFee.Hud1200Custom3FeeTypeId },
            { "sSettlementU3GovRtcProps_Dflp", DefaultSystemClosingCostFee.Hud1200Custom3FeeTypeId },
            { "sU3GovRtcProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1200Custom3FeeTypeId },
            { "sU3GovRtcProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1200Custom3FeeTypeId },
            { "sU3GovRtcPaidTo", DefaultSystemClosingCostFee.Hud1200Custom3FeeTypeId },
            { "sPestInspectF", DefaultSystemClosingCostFee.Hud1300PestInspectionFeeTypeId },
            { "sPestInspectFProps_PdByT", DefaultSystemClosingCostFee.Hud1300PestInspectionFeeTypeId },
            { "sPestInspectFProps_Apr", DefaultSystemClosingCostFee.Hud1300PestInspectionFeeTypeId },
            { "sPestInspectFProps_FhaAllow", DefaultSystemClosingCostFee.Hud1300PestInspectionFeeTypeId },
            { "sPestInspectFProps_Poc", DefaultSystemClosingCostFee.Hud1300PestInspectionFeeTypeId },
            { "sSettlementPestInspectFProps_Dflp", DefaultSystemClosingCostFee.Hud1300PestInspectionFeeTypeId },
            { "sPestInspectFProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1300PestInspectionFeeTypeId },
            { "sPestInspectFProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1300PestInspectionFeeTypeId },
            { "sPestInspectFProps_ToBroker", DefaultSystemClosingCostFee.Hud1300PestInspectionFeeTypeId },
            { "sPestInspectPaidTo", DefaultSystemClosingCostFee.Hud1300PestInspectionFeeTypeId },
            { "sU1ScDesc", DefaultSystemClosingCostFee.Hud1300Custom1FeeTypeId },
            { "sU1Sc", DefaultSystemClosingCostFee.Hud1300Custom1FeeTypeId },
            { "sU1ScProps_PdByT", DefaultSystemClosingCostFee.Hud1300Custom1FeeTypeId },
            { "sU1ScProps_Apr", DefaultSystemClosingCostFee.Hud1300Custom1FeeTypeId },
            { "sU1ScProps_FhaAllow", DefaultSystemClosingCostFee.Hud1300Custom1FeeTypeId },
            { "sU1ScProps_Poc", DefaultSystemClosingCostFee.Hud1300Custom1FeeTypeId },
            { "sSettlementU1ScProps_Dflp", DefaultSystemClosingCostFee.Hud1300Custom1FeeTypeId },
            { "sU1ScProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1300Custom1FeeTypeId },
            { "sU1ScProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1300Custom1FeeTypeId },
            { "sU1ScProps_ToBroker", DefaultSystemClosingCostFee.Hud1300Custom1FeeTypeId },
            { "sU1ScPaidTo", DefaultSystemClosingCostFee.Hud1300Custom1FeeTypeId },
            { "sU1ScGfeSection", DefaultSystemClosingCostFee.Hud1300Custom1FeeTypeId },
            { "sU2ScDesc", DefaultSystemClosingCostFee.Hud1300Custom2FeeTypeId },
            { "sU2Sc", DefaultSystemClosingCostFee.Hud1300Custom2FeeTypeId },
            { "sU2ScProps_PdByT", DefaultSystemClosingCostFee.Hud1300Custom2FeeTypeId },
            { "sU2ScProps_Apr", DefaultSystemClosingCostFee.Hud1300Custom2FeeTypeId },
            { "sU2ScProps_FhaAllow", DefaultSystemClosingCostFee.Hud1300Custom2FeeTypeId },
            { "sU2ScProps_Poc", DefaultSystemClosingCostFee.Hud1300Custom2FeeTypeId },
            { "sSettlementU2ScProps_Dflp", DefaultSystemClosingCostFee.Hud1300Custom2FeeTypeId },
            { "sU2ScProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1300Custom2FeeTypeId },
            { "sU2ScProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1300Custom2FeeTypeId },
            { "sU2ScProps_ToBroker", DefaultSystemClosingCostFee.Hud1300Custom2FeeTypeId },
            { "sU2ScPaidTo", DefaultSystemClosingCostFee.Hud1300Custom2FeeTypeId },
            { "sU2ScGfeSection", DefaultSystemClosingCostFee.Hud1300Custom2FeeTypeId },
            { "sU3ScDesc", DefaultSystemClosingCostFee.Hud1300Custom3FeeTypeId },
            { "sU3Sc", DefaultSystemClosingCostFee.Hud1300Custom3FeeTypeId },
            { "sU3ScProps_PdByT", DefaultSystemClosingCostFee.Hud1300Custom3FeeTypeId },
            { "sU3ScProps_Apr", DefaultSystemClosingCostFee.Hud1300Custom3FeeTypeId },
            { "sU3ScProps_FhaAllow", DefaultSystemClosingCostFee.Hud1300Custom3FeeTypeId },
            { "sU3ScProps_Poc", DefaultSystemClosingCostFee.Hud1300Custom3FeeTypeId },
            { "sSettlementU3ScProps_Dflp", DefaultSystemClosingCostFee.Hud1300Custom3FeeTypeId },
            { "sU3ScProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1300Custom3FeeTypeId },
            { "sU3ScProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1300Custom3FeeTypeId },
            { "sU3ScProps_ToBroker", DefaultSystemClosingCostFee.Hud1300Custom3FeeTypeId },
            { "sU3ScPaidTo", DefaultSystemClosingCostFee.Hud1300Custom3FeeTypeId },
            { "sU3ScGfeSection", DefaultSystemClosingCostFee.Hud1300Custom3FeeTypeId },
            { "sU4ScDesc", DefaultSystemClosingCostFee.Hud1300Custom4FeeTypeId },
            { "sU4Sc", DefaultSystemClosingCostFee.Hud1300Custom4FeeTypeId },
            { "sU4ScProps_PdByT", DefaultSystemClosingCostFee.Hud1300Custom4FeeTypeId },
            { "sU4ScProps_Apr", DefaultSystemClosingCostFee.Hud1300Custom4FeeTypeId },
            { "sU4ScProps_FhaAllow", DefaultSystemClosingCostFee.Hud1300Custom4FeeTypeId },
            { "sU4ScProps_Poc", DefaultSystemClosingCostFee.Hud1300Custom4FeeTypeId },
            { "sSettlementU4ScProps_Dflp", DefaultSystemClosingCostFee.Hud1300Custom4FeeTypeId },
            { "sU4ScProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1300Custom4FeeTypeId },
            { "sU4ScProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1300Custom4FeeTypeId },
            { "sU4ScProps_ToBroker", DefaultSystemClosingCostFee.Hud1300Custom4FeeTypeId },
            { "sU4ScPaidTo", DefaultSystemClosingCostFee.Hud1300Custom4FeeTypeId },
            { "sU4ScGfeSection", DefaultSystemClosingCostFee.Hud1300Custom4FeeTypeId },
            { "sU5ScDesc", DefaultSystemClosingCostFee.Hud1300Custom5FeeTypeId },
            { "sU5Sc", DefaultSystemClosingCostFee.Hud1300Custom5FeeTypeId },
            { "sU5ScProps_PdByT", DefaultSystemClosingCostFee.Hud1300Custom5FeeTypeId },
            { "sU5ScProps_Apr", DefaultSystemClosingCostFee.Hud1300Custom5FeeTypeId },
            { "sU5ScProps_FhaAllow", DefaultSystemClosingCostFee.Hud1300Custom5FeeTypeId },
            { "sU5ScProps_Poc", DefaultSystemClosingCostFee.Hud1300Custom5FeeTypeId },
            { "sSettlementU5ScProps_Dflp", DefaultSystemClosingCostFee.Hud1300Custom5FeeTypeId },
            { "sU5ScProps_PaidToThirdParty", DefaultSystemClosingCostFee.Hud1300Custom5FeeTypeId },
            { "sU5ScProps_ThisPartyIsAffiliate", DefaultSystemClosingCostFee.Hud1300Custom5FeeTypeId },
            { "sU5ScProps_ToBroker", DefaultSystemClosingCostFee.Hud1300Custom5FeeTypeId },
            { "sU5ScPaidTo", DefaultSystemClosingCostFee.Hud1300Custom5FeeTypeId },
            { "sU5ScGfeSection", DefaultSystemClosingCostFee.Hud1300Custom5FeeTypeId }
        };

        // "A Dictionary can support multiple readers concurrently, as long as the collection is not modified." -MSDN
        private static readonly Dictionary<string, E_FeeServiceFeePropertyT> s_fieldIdToFeeTypePropertyMap
            = new Dictionary<string, E_FeeServiceFeePropertyT>
        {
            { "sLOrigFPc", E_FeeServiceFeePropertyT.Percent } ,
            { "sLOrigFMb", E_FeeServiceFeePropertyT.Amount } ,
            { "sLOrigFProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sLOrigFProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sLOrigFProps_BF", E_FeeServiceFeePropertyT.None } ,
            { "sLOrigFProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sSettlementLOrigFProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sLOrigFProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sLOrigFProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sLOrigFProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "sLDiscntPc", E_FeeServiceFeePropertyT.Percent } ,
            { "sLDiscntBaseT", E_FeeServiceFeePropertyT.BaseValue } ,
            { "sLDiscntFMb", E_FeeServiceFeePropertyT.Amount } ,
            { "sGfeDiscountPointFProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sGfeLenderCreditFProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sGfeLenderCreditFProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sGfeDiscountPointFProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sGfeDiscountPointFProps_BF", E_FeeServiceFeePropertyT.None } ,
            { "sGfeDiscountPointFProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sSettlementDiscountPointFProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sGfeDiscountPointFProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sGfeDiscountPointFProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sApprF", E_FeeServiceFeePropertyT.Amount } ,
            { "sApprFProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sApprFProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sApprFProps_FhaAllow", E_FeeServiceFeePropertyT.Fha} ,
            { "sApprFProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sApprFPaid", E_FeeServiceFeePropertyT.None } ,
            { "sSettlementApprFProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sApprFProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sApprFProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sApprFProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "sApprFPaidTo", E_FeeServiceFeePropertyT.None } ,
            { "sCrF", E_FeeServiceFeePropertyT.Amount } ,
            { "sCrFProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sCrFProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sCrFProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sCrFProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sCrFPaid", E_FeeServiceFeePropertyT.None } ,
            { "sSettlementCrFProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sCrFProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sCrFProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sCrFProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "sCrFPaidTo", E_FeeServiceFeePropertyT.None } ,
            { "sTxServF", E_FeeServiceFeePropertyT.Amount } ,
            { "sTxServFProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sTxServFProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sTxServFProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sTxServFProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementTxServFProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sTxServFProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sTxServFProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sTxServFProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "sTxServFPaidTo", E_FeeServiceFeePropertyT.None } ,
            { "sFloodCertificationF", E_FeeServiceFeePropertyT.Amount } ,
            { "sFloodCertificationDeterminationT", E_FeeServiceFeePropertyT.None } ,
            { "sFloodCertificationFProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sFloodCertificationFProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sFloodCertificationFProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sFloodCertificationFProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementFloodCertificationFProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sFloodCertificationFProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sFloodCertificationFProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sFloodCertificationFProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "sFloodCertificationFPaidTo", E_FeeServiceFeePropertyT.None } ,
            { "sMBrokFPc", E_FeeServiceFeePropertyT.Percent } ,
            { "sMBrokFBaseT", E_FeeServiceFeePropertyT.BaseValue } ,
            { "sMBrokFMb", E_FeeServiceFeePropertyT.Amount } ,
            { "sMBrokFProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sMBrokFProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sMBrokFProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sMBrokFProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementMBrokFProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sMBrokFProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sMBrokFProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sMBrokFProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "sInspectF", E_FeeServiceFeePropertyT.Amount } ,
            { "sInspectFProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sInspectFProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sInspectFProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sInspectFProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementInspectFProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sInspectFProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sInspectFProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sInspectFProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "sInspectFPaidTo", E_FeeServiceFeePropertyT.None } ,
            { "sProcF", E_FeeServiceFeePropertyT.Amount } ,
            { "sProcFProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sProcFProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sProcFProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sProcFProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sProcFPaid", E_FeeServiceFeePropertyT.None } ,
            { "sSettlementProcFProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sProcFProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sProcFProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sProcFProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "sProcFPaidTo", E_FeeServiceFeePropertyT.None } ,
            { "sUwF", E_FeeServiceFeePropertyT.Amount } ,
            { "sUwFProps_PdByT", E_FeeServiceFeePropertyT.PaidBy  } ,
            { "sUwFProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sUwFProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sUwFProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementUwFProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sUwFProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sUwFProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sUwFProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "sUwFPaidTo", E_FeeServiceFeePropertyT.None } ,
            { "sWireF", E_FeeServiceFeePropertyT.Amount } ,
            { "sWireFProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sWireFProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sWireFProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sWireFProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementWireFProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sWireFProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sWireFProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sWireFProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "sWireFPaidTo", E_FeeServiceFeePropertyT.None } ,
            { "s800U1FDesc", E_FeeServiceFeePropertyT.Description } ,
            { "s800U1F", E_FeeServiceFeePropertyT.Amount } ,
            { "s800U1FProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "s800U1FProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "s800U1FProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "s800U1FProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlement800U1FProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "s800U1FProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "s800U1FProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "s800U1FProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "s800U1FPaidTo", E_FeeServiceFeePropertyT.None } ,
            ////{ "s800U1FGfeSection", E_FeeServiceFeePropertyT.GfeBox } ,
            { "s800U1FGfeSection", E_FeeServiceFeePropertyT.None } ,
            { "s800U2FDesc", E_FeeServiceFeePropertyT.Description } ,
            { "s800U2F", E_FeeServiceFeePropertyT.Amount } ,
            { "s800U2FProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "s800U2FProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "s800U2FProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "s800U2FProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlement800U2FProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "s800U2FProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "s800U2FProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "s800U2FProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "s800U2FPaidTo", E_FeeServiceFeePropertyT.None } ,
            ////{ "s800U2FGfeSection", E_FeeServiceFeePropertyT.GfeBox } ,
            { "s800U2FGfeSection", E_FeeServiceFeePropertyT.None } ,
            { "s800U3FDesc", E_FeeServiceFeePropertyT.Description } ,
            { "s800U3F", E_FeeServiceFeePropertyT.Amount } ,
            { "s800U3FProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "s800U3FProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "s800U3FProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "s800U3FProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlement800U3FProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "s800U3FProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "s800U3FProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "s800U3FProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "s800U3FPaidTo", E_FeeServiceFeePropertyT.None } ,
            ////{ "s800U3FGfeSection", E_FeeServiceFeePropertyT.GfeBox } ,
            { "s800U3FGfeSection", E_FeeServiceFeePropertyT.None } ,
            { "s800U4FDesc", E_FeeServiceFeePropertyT.Description } ,
            { "s800U4F", E_FeeServiceFeePropertyT.Amount } ,
            { "s800U4FProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "s800U4FProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "s800U4FProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "s800U4FProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlement800U4FProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "s800U4FProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "s800U4FProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "s800U4FProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo  } ,
            { "s800U4FPaidTo", E_FeeServiceFeePropertyT.None } ,
            ////{ "s800U4FGfeSection", E_FeeServiceFeePropertyT.GfeBox } ,
            { "s800U4FGfeSection", E_FeeServiceFeePropertyT.None } ,
            { "s800U5FDesc", E_FeeServiceFeePropertyT.Description } ,
            { "s800U5F", E_FeeServiceFeePropertyT.Amount } ,
            { "s800U5FProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "s800U5FProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "s800U5FProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "s800U5FProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlement800U5FProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "s800U5FProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "s800U5FProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "s800U5FProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "s800U5FPaidTo", E_FeeServiceFeePropertyT.None } ,
            ////{ "s800U5FGfeSection", E_FeeServiceFeePropertyT.GfeBox } ,
            { "s800U5FGfeSection", E_FeeServiceFeePropertyT.None } ,
            { "sIPiaDy", E_FeeServiceFeePropertyT.None } ,
            { "sIPiaProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sIPiaProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sIPiaProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sIPiaProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementIPiaProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sIPiaProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sIPiaProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sMipPiaProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sMipPiaProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sMipPiaProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sMipPiaProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sMipPiaProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sMipPiaProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sMipPiaProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sMipPiaPaidTo", E_FeeServiceFeePropertyT.PaidTo } ,
            { "sProHazInsR", E_FeeServiceFeePropertyT.Percent } ,
            { "sProHazInsT", E_FeeServiceFeePropertyT.BaseValue } ,
            { "sProHazInsMb", E_FeeServiceFeePropertyT.Amount } ,
            { "sHazInsPiaMon", E_FeeServiceFeePropertyT.PrepaidMonths } ,
            { "sHazInsRsrvEscrowedTri", E_FeeServiceFeePropertyT.Escrow },
            { "sHazInsPiaProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sHazInsPiaProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sHazInsPiaProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sHazInsPiaProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementHazInsPiaProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sHazInsPiaProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sHazInsPiaProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sHazInsPiaPaidTo", E_FeeServiceFeePropertyT.None } ,
            { "s904PiaDesc", E_FeeServiceFeePropertyT.Description } ,
            { "s904Pia", E_FeeServiceFeePropertyT.Amount } ,
            { "s904PiaProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "s904PiaProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "s904PiaProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "s904PiaProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlement904PiaProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "s904PiaProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "s904PiaProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            ////{ "s904PiaGfeSection", E_FeeServiceFeePropertyT.GfeBox } ,
            { "s904PiaGfeSection", E_FeeServiceFeePropertyT.None } ,
            { "sVaFfProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sVaFfProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sVaFfProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sVaFfProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sVaFfProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sVaFfProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sVaFfProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sVaFfPaidTo", E_FeeServiceFeePropertyT.None } ,
            { "s900U1PiaDesc", E_FeeServiceFeePropertyT.Description } ,
            { "s900U1Pia", E_FeeServiceFeePropertyT.Amount } ,
            { "s900U1PiaProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "s900U1PiaProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "s900U1PiaProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "s900U1PiaProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlement900U1PiaProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "s900U1PiaProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "s900U1PiaProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            ////{ "s900U1PiaGfeSection", E_FeeServiceFeePropertyT.GfeBox } ,
            { "s900U1PiaGfeSection", E_FeeServiceFeePropertyT.None } ,
            { "sHazInsRsrvMon", E_FeeServiceFeePropertyT.ReserveMonths } ,
            { "sHazInsRsrvProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sHazInsRsrvProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sHazInsRsrvProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sHazInsRsrvProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementHazInsRsrvProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sHazInsRsrvProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sHazInsRsrvProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sHazInsRsrvEscrowCushion", E_FeeServiceFeePropertyT.ReserveCushion } ,
            { "sHazInsRsrvEscrowDisbursementSchedule", E_FeeServiceFeePropertyT.DisbursementSchedule } ,
            { "sMInsRsrvMon", E_FeeServiceFeePropertyT.ReserveMonths } ,
            { "sMInsRsrvProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sMInsRsrvProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sMInsRsrvProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sMInsRsrvProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sMInsRsrvEscrowedTri" , E_FeeServiceFeePropertyT.Escrow },
            { "sSettlementMInsRsrvProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sMInsRsrvProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sMInsRsrvProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sMInsRsrvEscrowCushion", E_FeeServiceFeePropertyT.ReserveCushion } ,
            { "sMInsRsrvEscrowDisbursementSchedule", E_FeeServiceFeePropertyT.DisbursementSchedule } ,
            { "sProRealETxR", E_FeeServiceFeePropertyT.Percent } ,
            { "sProRealETxT", E_FeeServiceFeePropertyT.BaseValue } ,
            { "sProRealETxMb", E_FeeServiceFeePropertyT.Amount } ,
            { "sRealETxRsrvMon", E_FeeServiceFeePropertyT.ReserveMonths } ,
            { "sRealETxRsrvProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sRealETxRsrvProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sRealETxRsrvProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sRealETxRsrvProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sRealETxRsrvEscrowedTri" , E_FeeServiceFeePropertyT.Escrow } ,
            { "sSettlementRealETxRsrvProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sRealETxRsrvProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sRealETxRsrvProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sRealETxRsrvEscrowCushion", E_FeeServiceFeePropertyT.ReserveCushion } ,
            { "sRealETxRsrvEscrowDisbursementSchedule", E_FeeServiceFeePropertyT.DisbursementSchedule } ,
            { "sSchoolTxRsrvMon", E_FeeServiceFeePropertyT.ReserveMonths } ,
            { "sProSchoolTx", E_FeeServiceFeePropertyT.Amount } ,
            { "sSchoolTxRsrvProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sSchoolTxRsrvProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sSchoolTxRsrvProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sSchoolTxRsrvProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSchoolTxRsrvEscrowedTri" , E_FeeServiceFeePropertyT.Escrow } ,
            { "sSettlementSchoolTxRsrvProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sSchoolTxRsrvProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sSchoolTxRsrvProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sSchoolTxRsrvEscrowCushion", E_FeeServiceFeePropertyT.ReserveCushion } ,
            { "sSchoolTxRsrvEscrowDisbursementSchedule", E_FeeServiceFeePropertyT.DisbursementSchedule } ,
            { "sFloodInsRsrvMon", E_FeeServiceFeePropertyT.ReserveMonths } ,
            { "sProFloodIns", E_FeeServiceFeePropertyT.Amount } ,
            { "sFloodInsRsrvProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sFloodInsRsrvProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sFloodInsRsrvProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sFloodInsRsrvProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sFloodInsRsrvEscrowedTri" , E_FeeServiceFeePropertyT.Escrow },
            { "sSettlementFloodInsRsrvProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sFloodInsRsrvProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sFloodInsRsrvProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sFloodInsRsrvEscrowCushion", E_FeeServiceFeePropertyT.ReserveCushion } ,
            { "sFloodInsRsrvEscrowDisbursementSchedule", E_FeeServiceFeePropertyT.DisbursementSchedule } ,
            { "sAggregateAdjRsrvProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sAggregateAdjRsrvProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sAggregateAdjRsrvProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sAggregateAdjRsrvProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementAggregateAdjRsrvProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sAggregateAdjRsrvProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sAggregateAdjRsrvProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "s1006ProHExpDesc", E_FeeServiceFeePropertyT.Description } ,
            { "s1006RsrvMon", E_FeeServiceFeePropertyT.ReserveMonths } ,
            { "s1006ProHExp", E_FeeServiceFeePropertyT.Amount } ,
            { "s1006RsrvProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "s1006RsrvProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "s1006RsrvProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "s1006RsrvProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "s1006RsrvEscrowedTri", E_FeeServiceFeePropertyT.Escrow },
            { "sSettlement1008RsrvProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "s1006RsrvProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "s1006RsrvProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "s1006RsrvEscrowCushion", E_FeeServiceFeePropertyT.ReserveCushion } ,
            { "s1006RsrvEscrowDisbursementSchedule", E_FeeServiceFeePropertyT.DisbursementSchedule } ,
            { "s1007ProHExpDesc", E_FeeServiceFeePropertyT.Description } ,
            { "s1007RsrvMon", E_FeeServiceFeePropertyT.ReserveMonths } ,
            { "s1007ProHExp", E_FeeServiceFeePropertyT.Amount } ,
            { "s1007RsrvProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "s1007RsrvProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "s1007RsrvProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "s1007RsrvProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "s1007RsrvEscrowedTri", E_FeeServiceFeePropertyT.Escrow },
            { "sSettlement1009RsrvProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "s1007RsrvProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "s1007RsrvProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "s1007RsrvEscrowCushion", E_FeeServiceFeePropertyT.ReserveCushion } ,
            { "s1007RsrvEscrowDisbursementSchedule", E_FeeServiceFeePropertyT.DisbursementSchedule } ,
            { "sU3RsrvDesc", E_FeeServiceFeePropertyT.Description } ,
            { "sU3RsrvMon", E_FeeServiceFeePropertyT.ReserveMonths } ,
            { "sProU3Rsrv", E_FeeServiceFeePropertyT.Amount } ,
            { "sU3RsrvEscrowedTri", E_FeeServiceFeePropertyT.Escrow },
            { "sU3RsrvProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sU3RsrvProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sU3RsrvProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sU3RsrvProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementU3RsrvProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sU3RsrvProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sU3RsrvProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sU3RsrvEscrowCushion", E_FeeServiceFeePropertyT.ReserveCushion } ,
            { "sU3RsrvEscrowDisbursementSchedule", E_FeeServiceFeePropertyT.DisbursementSchedule } ,
            { "sU4RsrvDesc", E_FeeServiceFeePropertyT.Description } ,
            { "sU4RsrvMon", E_FeeServiceFeePropertyT.ReserveMonths } ,
            { "sProU4Rsrv", E_FeeServiceFeePropertyT.Amount } ,
            { "sU4RsrvEscrowedTri" , E_FeeServiceFeePropertyT.Escrow },
            { "sU4RsrvProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sU4RsrvProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sU4RsrvProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sU4RsrvProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementU4RsrvProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sU4RsrvProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sU4RsrvProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sU4RsrvEscrowCushion", E_FeeServiceFeePropertyT.ReserveCushion } ,
            { "sU4RsrvEscrowDisbursementSchedule", E_FeeServiceFeePropertyT.DisbursementSchedule } ,
            { "sEscrowFPc", E_FeeServiceFeePropertyT.Percent } ,
            { "sEscrowFBaseT", E_FeeServiceFeePropertyT.BaseValue } ,
            { "sEscrowFMb", E_FeeServiceFeePropertyT.Amount } ,
            { "sEscrowFProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sEscrowFProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sEscrowFProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sEscrowFProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementEscrowFProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sEscrowFProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sEscrowFProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sEscrowFProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "sEscrowFTable", E_FeeServiceFeePropertyT.Description } ,
            ////{ "sEscrowFGfeSection", E_FeeServiceFeePropertyT.GfeBox } ,
            { "sEscrowFGfeSection", E_FeeServiceFeePropertyT.None } ,
            { "sOwnerTitleInsFPc", E_FeeServiceFeePropertyT.Percent } ,
            { "sOwnerTitleInsFBaseT", E_FeeServiceFeePropertyT.BaseValue } ,
            { "sOwnerTitleInsFMb", E_FeeServiceFeePropertyT.Amount } ,
            { "sOwnerTitleInsProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sOwnerTitleInsProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sOwnerTitleInsProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sOwnerTitleInsProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementOwnerTitleInsFProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sOwnerTitleInsProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sOwnerTitleInsProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sOwnerTitleInsProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "sOwnerTitleInsPaidTo", E_FeeServiceFeePropertyT.None } ,
            { "sTitleInsFPc", E_FeeServiceFeePropertyT.Percent } ,
            { "sTitleInsFBaseT", E_FeeServiceFeePropertyT.BaseValue } ,
            { "sTitleInsFMb", E_FeeServiceFeePropertyT.Amount } ,
            { "sTitleInsFProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sTitleInsFProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sTitleInsFProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sTitleInsFProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementTitleInsFProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sTitleInsFProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sTitleInsFProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sTitleInsFProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "sTitleInsFTable", E_FeeServiceFeePropertyT.Description } ,
            { "sDocPrepF", E_FeeServiceFeePropertyT.Amount } ,
            { "sDocPrepFProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sDocPrepFProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sDocPrepFProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sDocPrepFProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementDocPrepFProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sDocPrepFProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sDocPrepFProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sDocPrepFProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "sDocPrepFPaidTo", E_FeeServiceFeePropertyT.None } ,
            ////{ "sDocPrepFGfeSection", E_FeeServiceFeePropertyT.GfeBox } ,
            { "sDocPrepFGfeSection", E_FeeServiceFeePropertyT.None } ,
            { "sNotaryF", E_FeeServiceFeePropertyT.Amount } ,
            { "sNotaryFProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sNotaryFProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sNotaryFProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sNotaryFProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementNotaryFProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sNotaryFProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sNotaryFProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sNotaryFProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "sNotaryFPaidTo", E_FeeServiceFeePropertyT.None } ,
            ////{ "sNotaryFGfeSection", E_FeeServiceFeePropertyT.GfeBox } ,
            { "sNotaryFGfeSection", E_FeeServiceFeePropertyT.None } ,
            { "sAttorneyF", E_FeeServiceFeePropertyT.Amount } ,
            { "sAttorneyFProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sAttorneyFProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sAttorneyFProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sAttorneyFProps_Poc", E_FeeServiceFeePropertyT.Fha } ,
            { "sSettlementAttorneyFProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sAttorneyFProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sAttorneyFProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sAttorneyFProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "sAttorneyFPaidTo", E_FeeServiceFeePropertyT.None } ,
            ////{ "sAttorneyFGfeSection", E_FeeServiceFeePropertyT.GfeBox } ,
            { "sAttorneyFGfeSection", E_FeeServiceFeePropertyT.None } ,
            { "sU1TcDesc", E_FeeServiceFeePropertyT.Description } ,
            { "sU1Tc", E_FeeServiceFeePropertyT.Amount } ,
            { "sU1TcProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sU1TcProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sU1TcProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sU1TcProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementU1TcProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sU1TcProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sU1TcProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sU1TcProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "sU1TcPaidTo", E_FeeServiceFeePropertyT.None } ,
            ////{ "sU1TcGfeSection", E_FeeServiceFeePropertyT.GfeBox } ,
            { "sU1TcGfeSection", E_FeeServiceFeePropertyT.None } ,
            { "sU2TcDesc", E_FeeServiceFeePropertyT.Description } ,
            { "sU2Tc", E_FeeServiceFeePropertyT.Amount } ,
            { "sU2TcProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sU2TcProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sU2TcProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sU2TcProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementU2TcProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sU2TcProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sU2TcProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sU2TcProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "sU2TcPaidTo", E_FeeServiceFeePropertyT.None } ,
            ////{ "sU2TcGfeSection", E_FeeServiceFeePropertyT.GfeBox } ,
            { "sU2TcGfeSection", E_FeeServiceFeePropertyT.None } ,
            { "sU3TcDesc", E_FeeServiceFeePropertyT.Description } ,
            { "sU3Tc", E_FeeServiceFeePropertyT.Amount } ,
            { "sU3TcProps_PdByT", E_FeeServiceFeePropertyT.PaidBy} ,
            { "sU3TcProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sU3TcProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sU3TcProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementU3TcProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sU3TcProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sU3TcProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sU3TcProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "sU3TcPaidTo", E_FeeServiceFeePropertyT.None } ,
            ////{ "sU3TcGfeSection", E_FeeServiceFeePropertyT.GfeBox } ,
            { "sU3TcGfeSection", E_FeeServiceFeePropertyT.None } ,
            { "sU4TcDesc", E_FeeServiceFeePropertyT.Description } ,
            { "sU4Tc", E_FeeServiceFeePropertyT.Amount } ,
            { "sU4TcProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sU4TcProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sU4TcProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sU4TcProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementU4TcProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sU4TcProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sU4TcProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sU4TcProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "sU4TcPaidTo", E_FeeServiceFeePropertyT.None } ,
            ////{ "sU4TcGfeSection", E_FeeServiceFeePropertyT.GfeBox } ,
            { "sU4TcGfeSection", E_FeeServiceFeePropertyT.None } ,
            { "sRecFPc", E_FeeServiceFeePropertyT.Percent } ,
            { "sRecBaseT", E_FeeServiceFeePropertyT.BaseValue } ,
            { "sRecFMb", E_FeeServiceFeePropertyT.Amount } ,
            { "sRecFLckd", E_FeeServiceFeePropertyT.None } ,
            { "sRecDeed", E_FeeServiceFeePropertyT.Amount } ,
            { "sRecMortgage", E_FeeServiceFeePropertyT.Amount } ,
            { "sRecRelease", E_FeeServiceFeePropertyT.Amount } ,
            { "sRecFProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sRecFProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sRecFProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sRecFProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementRecFProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sRecFProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sRecFProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sRecFDesc", E_FeeServiceFeePropertyT.Description } ,
            { "sCountyRtcPc", E_FeeServiceFeePropertyT.Percent } ,
            { "sCountyRtcBaseT", E_FeeServiceFeePropertyT.BaseValue } ,
            { "sCountyRtcMb", E_FeeServiceFeePropertyT.Amount } ,
            { "sCountyRtcProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sCountyRtcProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sCountyRtcProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sCountyRtcProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementCountyRtcProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sCountyRtcProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sCountyRtcProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sCountyRtcDesc", E_FeeServiceFeePropertyT.Description } ,
            { "sStateRtcPc", E_FeeServiceFeePropertyT.Percent } ,
            { "sStateRtcBaseT", E_FeeServiceFeePropertyT.BaseValue } ,
            { "sStateRtcMb", E_FeeServiceFeePropertyT.Amount } ,
            { "sStateRtcProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sStateRtcProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sStateRtcProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sStateRtcProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementStateRtcProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sStateRtcProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sStateRtcProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sStateRtcDesc", E_FeeServiceFeePropertyT.Description } ,
            { "sU1GovRtcDesc", E_FeeServiceFeePropertyT.Description } ,
            { "sU1GovRtcPc", E_FeeServiceFeePropertyT.Percent } ,
            { "sU1GovRtcBaseT", E_FeeServiceFeePropertyT.BaseValue } ,
            { "sU1GovRtcMb", E_FeeServiceFeePropertyT.Amount } ,
            ////{ "sU1GovRtcGfeSection", E_FeeServiceFeePropertyT.GfeBox } ,
            { "sU1GovRtcGfeSection", E_FeeServiceFeePropertyT.None } ,
            { "sU1GovRtcProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sU1GovRtcProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sU1GovRtcProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sU1GovRtcProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementU1GovRtcProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sU1GovRtcProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sU1GovRtcProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sU1GovRtcPaidTo", E_FeeServiceFeePropertyT.None } ,
            { "sU2GovRtcDesc", E_FeeServiceFeePropertyT.Description } ,
            { "sU2GovRtcPc", E_FeeServiceFeePropertyT.Percent } ,
            { "sU2GovRtcBaseT", E_FeeServiceFeePropertyT.BaseValue } ,
            { "sU2GovRtcMb", E_FeeServiceFeePropertyT.Amount } ,
            ////{ "sU2GovRtcGfeSection", E_FeeServiceFeePropertyT.GfeBox } ,
            { "sU2GovRtcGfeSection", E_FeeServiceFeePropertyT.None } ,
            { "sU2GovRtcProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sU2GovRtcProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sU2GovRtcProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sU2GovRtcProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementU2GovRtcProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sU2GovRtcProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sU2GovRtcProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sU2GovRtcPaidTo", E_FeeServiceFeePropertyT.None } ,
            { "sU3GovRtcDesc", E_FeeServiceFeePropertyT.Description } ,
            { "sU3GovRtcPc", E_FeeServiceFeePropertyT.Percent } ,
            { "sU3GovRtcBaseT", E_FeeServiceFeePropertyT.BaseValue } ,
            { "sU3GovRtcMb", E_FeeServiceFeePropertyT.Amount } ,
            ////{ "sU3GovRtcGfeSection", E_FeeServiceFeePropertyT.GfeBox } ,
            { "sU3GovRtcGfeSection", E_FeeServiceFeePropertyT.None } ,
            { "sU3GovRtcProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sU3GovRtcProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sU3GovRtcProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sU3GovRtcProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementU3GovRtcProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sU3GovRtcProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sU3GovRtcProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sU3GovRtcPaidTo", E_FeeServiceFeePropertyT.None } ,
            { "sPestInspectF", E_FeeServiceFeePropertyT.Amount } ,
            { "sPestInspectFProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sPestInspectFProps_Apr", E_FeeServiceFeePropertyT.Apr} ,
            { "sPestInspectFProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sPestInspectFProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementPestInspectFProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sPestInspectFProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sPestInspectFProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sPestInspectFProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "sPestInspectPaidTo", E_FeeServiceFeePropertyT.None } ,
            { "sU1ScDesc", E_FeeServiceFeePropertyT.Description } ,
            { "sU1Sc", E_FeeServiceFeePropertyT.Amount } ,
            { "sU1ScProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sU1ScProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sU1ScProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sU1ScProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementU1ScProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sU1ScProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sU1ScProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sU1ScProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "sU1ScPaidTo", E_FeeServiceFeePropertyT.None } ,
            ////{ "sU1ScGfeSection", E_FeeServiceFeePropertyT.GfeBox } ,
            { "sU1ScGfeSection", E_FeeServiceFeePropertyT.None } ,
            { "sU2ScDesc", E_FeeServiceFeePropertyT.Description } ,
            { "sU2Sc", E_FeeServiceFeePropertyT.Amount } ,
            { "sU2ScProps_PdByT", E_FeeServiceFeePropertyT.PaidTo } ,
            { "sU2ScProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sU2ScProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sU2ScProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementU2ScProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sU2ScProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sU2ScProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sU2ScProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "sU2ScPaidTo", E_FeeServiceFeePropertyT.None } ,
            ////{ "sU2ScGfeSection", E_FeeServiceFeePropertyT.GfeBox } ,
            { "sU2ScGfeSection", E_FeeServiceFeePropertyT.None } ,
            { "sU3ScDesc", E_FeeServiceFeePropertyT.Description } ,
            { "sU3Sc", E_FeeServiceFeePropertyT.Amount } ,
            { "sU3ScProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sU3ScProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sU3ScProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sU3ScProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementU3ScProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sU3ScProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sU3ScProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sU3ScProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "sU3ScPaidTo", E_FeeServiceFeePropertyT.None } ,
            ////{ "sU3ScGfeSection", E_FeeServiceFeePropertyT.GfeBox } ,
            { "sU3ScGfeSection", E_FeeServiceFeePropertyT.None } ,
            { "sU4ScDesc", E_FeeServiceFeePropertyT.Description } ,
            { "sU4Sc", E_FeeServiceFeePropertyT.Amount } ,
            { "sU4ScProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sU4ScProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sU4ScProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sU4ScProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementU4ScProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sU4ScProps_PaidToThirdParty", E_FeeServiceFeePropertyT.ThirdParty } ,
            { "sU4ScProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sU4ScProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "sU4ScPaidTo", E_FeeServiceFeePropertyT.None } ,
            ////{ "sU4ScGfeSection", E_FeeServiceFeePropertyT.GfeBox } ,
            { "sU4ScGfeSection", E_FeeServiceFeePropertyT.None } ,
            { "sU5ScDesc", E_FeeServiceFeePropertyT.Description } ,
            { "sU5Sc", E_FeeServiceFeePropertyT.Amount } ,
            { "sU5ScProps_PdByT", E_FeeServiceFeePropertyT.PaidBy } ,
            { "sU5ScProps_Apr", E_FeeServiceFeePropertyT.Apr } ,
            { "sU5ScProps_FhaAllow", E_FeeServiceFeePropertyT.Fha } ,
            { "sU5ScProps_Poc", E_FeeServiceFeePropertyT.Payable } ,
            { "sSettlementU5ScProps_Dflp", E_FeeServiceFeePropertyT.Dflp } ,
            { "sU5ScProps_PaidToThirdParty", E_FeeServiceFeePropertyT.PaidTo } ,
            { "sU5ScProps_ThisPartyIsAffiliate", E_FeeServiceFeePropertyT.Affiliate } ,
            { "sU5ScProps_ToBroker", E_FeeServiceFeePropertyT.PaidTo } ,
            { "sU5ScPaidTo", E_FeeServiceFeePropertyT.None } ,
            ////{ "sU5ScGfeSection", E_FeeServiceFeePropertyT.GfeBox } 
            { "sU5ScGfeSection", E_FeeServiceFeePropertyT.None } 
        };

        // "A Dictionary can support multiple readers concurrently, as long as the collection is not modified." -MSDN
        private static readonly Dictionary<string, int> s_fieldLineMap
            = new Dictionary<string, int>
        {
            { "sLOrigFPc", 801 } ,
            { "sLOrigFMb", 801 } ,
            { "sLOrigFProps_PdByT", 801 } ,
            { "sLOrigFProps_Apr", 801 } ,
            { "sLOrigFProps_BF", 801 } ,
            { "sLOrigFProps_FhaAllow", 801 } ,
            { "sSettlementLOrigFProps_Dflp", 801 } ,
            { "sLOrigFProps_PaidToThirdParty", 801 } ,
            { "sLOrigFProps_ThisPartyIsAffiliate", 801 } ,
            { "sLOrigFProps_ToBroker", 801 } ,
            { "sLDiscntPc", 802 } ,
            { "sLDiscntBaseT", 802 } ,
            { "sLDiscntFMb", 802 } ,
            { "sGfeDiscountPointFProps_PdByT", 802 } ,
            { "sGfeLenderCreditFProps_Apr", 802 } ,
            { "sGfeLenderCreditFProps_FhaAllow", 802 } ,
            { "sGfeDiscountPointFProps_Apr", 802 } ,
            { "sGfeDiscountPointFProps_BF", 802 } ,
            { "sGfeDiscountPointFProps_FhaAllow", 802 } ,
            { "sSettlementDiscountPointFProps_Dflp", 802 } ,
            { "sGfeDiscountPointFProps_PaidToThirdParty", 802 } ,
            { "sGfeDiscountPointFProps_ThisPartyIsAffiliate", 802 } ,
            { "sApprF", 804 } ,
            { "sApprFProps_PdByT", 804 } ,
            { "sApprFProps_Apr", 804 } ,
            { "sApprFProps_FhaAllow", 804 } ,
            { "sApprFProps_Poc", 804 } ,
            { "sApprFPaid", 804 } ,
            { "sSettlementApprFProps_Dflp", 804 } ,
            { "sApprFProps_PaidToThirdParty", 804 } ,
            { "sApprFProps_ThisPartyIsAffiliate", 804 } ,
            { "sApprFProps_ToBroker", 804 } ,
            { "sApprFPaidTo", 804 } ,
            { "sCrF", 805 } ,
            { "sCrFProps_PdByT", 805 } ,
            { "sCrFProps_Apr", 805 } ,
            { "sCrFProps_FhaAllow", 805 } ,
            { "sCrFProps_Poc", 805 } ,
            { "sCrFPaid", 805 } ,
            { "sSettlementCrFProps_Dflp", 805 } ,
            { "sCrFProps_PaidToThirdParty", 805 } ,
            { "sCrFProps_ThisPartyIsAffiliate", 805 } ,
            { "sCrFProps_ToBroker", 805 } ,
            { "sCrFPaidTo", 805 } ,
            { "sTxServF", 806 } ,
            { "sTxServFProps_PdByT", 806 } ,
            { "sTxServFProps_Apr", 806 } ,
            { "sTxServFProps_FhaAllow", 806 } ,
            { "sTxServFProps_Poc", 806 } ,
            { "sSettlementTxServFProps_Dflp", 806 } ,
            { "sTxServFProps_PaidToThirdParty", 806 } ,
            { "sTxServFProps_ThisPartyIsAffiliate", 806 } ,
            { "sTxServFProps_ToBroker", 806 } ,
            { "sTxServFPaidTo", 806 } ,
            { "sFloodCertificationF", 807 } ,
            { "sFloodCertificationDeterminationT", 807 } ,
            { "sFloodCertificationFProps_PdByT", 807 } ,
            { "sFloodCertificationFProps_Apr", 807 } ,
            { "sFloodCertificationFProps_FhaAllow", 807 } ,
            { "sFloodCertificationFProps_Poc", 807 } ,
            { "sSettlementFloodCertificationFProps_Dflp", 807 } ,
            { "sFloodCertificationFProps_PaidToThirdParty", 807 } ,
            { "sFloodCertificationFProps_ThisPartyIsAffiliate", 807 } ,
            { "sFloodCertificationFProps_ToBroker", 807 } ,
            { "sFloodCertificationFPaidTo", 807 } ,
            { "sMBrokFPc", 808 } ,
            { "sMBrokFBaseT", 808 } ,
            { "sMBrokFMb", 808 } ,
            { "sMBrokFProps_PdByT", 808 } ,
            { "sMBrokFProps_Apr", 808 } ,
            { "sMBrokFProps_FhaAllow", 808 } ,
            { "sMBrokFProps_Poc", 808 } ,
            { "sSettlementMBrokFProps_Dflp", 808 } ,
            { "sMBrokFProps_PaidToThirdParty", 808 } ,
            { "sMBrokFProps_ThisPartyIsAffiliate", 808 } ,
            { "sMBrokFProps_ToBroker", 808 } ,
            { "sInspectF", 809 } ,
            { "sInspectFProps_PdByT", 809 } ,
            { "sInspectFProps_Apr", 809 } ,
            { "sInspectFProps_FhaAllow", 809 } ,
            { "sInspectFProps_Poc", 809 } ,
            { "sSettlementInspectFProps_Dflp", 809 } ,
            { "sInspectFProps_PaidToThirdParty", 809 } ,
            { "sInspectFProps_ThisPartyIsAffiliate", 809 } ,
            { "sInspectFProps_ToBroker", 809 } ,
            { "sInspectFPaidTo", 809 } ,
            { "sProcF", 810 } ,
            { "sProcFProps_PdByT", 810 } ,
            { "sProcFProps_Apr", 810 } ,
            { "sProcFProps_FhaAllow", 810 } ,
            { "sProcFProps_Poc", 810 } ,
            { "sProcFPaid", 810 } ,
            { "sSettlementProcFProps_Dflp", 810 } ,
            { "sProcFProps_PaidToThirdParty", 810 } ,
            { "sProcFProps_ThisPartyIsAffiliate", 810 } ,
            { "sProcFProps_ToBroker", 810 } ,
            { "sProcFPaidTo", 810 } ,
            { "sUwF", 811 } ,
            { "sUwFProps_PdByT", 811 } ,
            { "sUwFProps_Apr", 811 } ,
            { "sUwFProps_FhaAllow", 811 } ,
            { "sUwFProps_Poc", 811 } ,
            { "sSettlementUwFProps_Dflp", 811 } ,
            { "sUwFProps_PaidToThirdParty", 811 } ,
            { "sUwFProps_ThisPartyIsAffiliate", 811 } ,
            { "sUwFProps_ToBroker", 811 } ,
            { "sUwFPaidTo", 811 } ,
            { "sWireF", 812 } ,
            { "sWireFProps_PdByT", 812 } ,
            { "sWireFProps_Apr", 812 } ,
            { "sWireFProps_FhaAllow", 812 } ,
            { "sWireFProps_Poc", 812 } ,
            { "sSettlementWireFProps_Dflp", 812 } ,
            { "sWireFProps_PaidToThirdParty", 812 } ,
            { "sWireFProps_ThisPartyIsAffiliate", 812 } ,
            { "sWireFProps_ToBroker", 812 } ,
            { "sWireFPaidTo", 812 } ,
            { "s800U1FDesc", 813 } ,
            { "s800U1F", 813 } ,
            { "s800U1FProps_PdByT", 813 } ,
            { "s800U1FProps_Apr", 813 } ,
            { "s800U1FProps_FhaAllow", 813 } ,
            { "s800U1FProps_Poc", 813 } ,
            { "sSettlement800U1FProps_Dflp", 813 } ,
            { "s800U1FProps_PaidToThirdParty", 813 } ,
            { "s800U1FProps_ThisPartyIsAffiliate", 813 } ,
            { "s800U1FProps_ToBroker", 813 } ,
            { "s800U1FPaidTo", 813 } ,
            { "s800U1FGfeSection", 813 } ,
            { "s800U2FDesc", 814 } ,
            { "s800U2F", 814 } ,
            { "s800U2FProps_PdByT", 814 } ,
            { "s800U2FProps_Apr", 814 } ,
            { "s800U2FProps_FhaAllow", 814 } ,
            { "s800U2FProps_Poc", 814 } ,
            { "sSettlement800U2FProps_Dflp", 814 } ,
            { "s800U2FProps_PaidToThirdParty", 814 } ,
            { "s800U2FProps_ThisPartyIsAffiliate", 814 } ,
            { "s800U2FProps_ToBroker", 814 } ,
            { "s800U2FPaidTo", 814 } ,
            { "s800U2FGfeSection", 814 } ,
            { "s800U3FDesc", 815 } ,
            { "s800U3F", 815 } ,
            { "s800U3FProps_PdByT", 815 } ,
            { "s800U3FProps_Apr", 815 } ,
            { "s800U3FProps_FhaAllow", 815 } ,
            { "s800U3FProps_Poc", 815 } ,
            { "sSettlement800U3FProps_Dflp", 815 } ,
            { "s800U3FProps_PaidToThirdParty", 815 } ,
            { "s800U3FProps_ThisPartyIsAffiliate", 815 } ,
            { "s800U3FProps_ToBroker", 815 } ,
            { "s800U3FPaidTo", 815 } ,
            { "s800U3FGfeSection", 815 } ,
            { "s800U4FDesc", 816 } ,
            { "s800U4F", 816 } ,
            { "s800U4FProps_PdByT", 816 } ,
            { "s800U4FProps_Apr", 816 } ,
            { "s800U4FProps_FhaAllow", 816 } ,
            { "s800U4FProps_Poc", 816 } ,
            { "sSettlement800U4FProps_Dflp", 816 } ,
            { "s800U4FProps_PaidToThirdParty", 816 } ,
            { "s800U4FProps_ThisPartyIsAffiliate", 816 } ,
            { "s800U4FProps_ToBroker", 816 } ,
            { "s800U4FPaidTo", 816 } ,
            { "s800U4FGfeSection", 816 } ,
            { "s800U5FDesc", 817 } ,
            { "s800U5F", 817 } ,
            { "s800U5FProps_PdByT", 817 } ,
            { "s800U5FProps_Apr", 817 } ,
            { "s800U5FProps_FhaAllow", 817 } ,
            { "s800U5FProps_Poc", 817 } ,
            { "sSettlement800U5FProps_Dflp", 817 } ,
            { "s800U5FProps_PaidToThirdParty", 817 } ,
            { "s800U5FProps_ThisPartyIsAffiliate", 817 } ,
            { "s800U5FProps_ToBroker", 817 } ,
            { "s800U5FPaidTo", 817 } ,
            { "s800U5FGfeSection", 817 } ,
            { "sIPiaDy", 901 } ,
            { "sIPiaProps_PdByT", 901 } ,
            { "sIPiaProps_Apr", 901 } ,
            { "sIPiaProps_FhaAllow", 901 } ,
            { "sIPiaProps_Poc", 901 } ,
            { "sSettlementIPiaProps_Dflp", 901 } ,
            { "sIPiaProps_PaidToThirdParty", 901 } ,
            { "sIPiaProps_ThisPartyIsAffiliate", 901 } ,
            { "sMipPiaProps_PdByT", 902 } ,
            { "sMipPiaProps_Apr", 902 } ,
            { "sMipPiaProps_FhaAllow", 902 } ,
            { "sMipPiaProps_Poc", 902 } ,
            { "sMipPiaProps_Dflp", 902 } ,
            { "sMipPiaProps_PaidToThirdParty", 902 } ,
            { "sMipPiaProps_ThisPartyIsAffiliate", 902 } ,
            { "sMipPiaPaidTo", 902 } ,
            { "sProHazInsR", 903 } ,
            { "sProHazInsT", 903 } ,
            { "sProHazInsMb", 903 } ,
            { "sHazInsPiaMon", 903 } ,
            { "sHazInsRsrvEscrowedTri", 903 },
            { "sHazInsPiaProps_PdByT", 903 } ,
            { "sHazInsPiaProps_Apr", 903 } ,
            { "sHazInsPiaProps_FhaAllow", 903 } ,
            { "sHazInsPiaProps_Poc", 903 } ,
            { "sSettlementHazInsPiaProps_Dflp", 903 } ,
            { "sHazInsPiaProps_PaidToThirdParty", 903 } ,
            { "sHazInsPiaProps_ThisPartyIsAffiliate", 903 } ,
            { "sHazInsPiaPaidTo", 903 } ,
            { "s904PiaDesc", 904 } ,
            { "s904Pia", 904 } ,
            { "s904PiaProps_PdByT", 904 } ,
            { "s904PiaProps_Apr", 904 } ,
            { "s904PiaProps_FhaAllow", 904 } ,
            { "s904PiaProps_Poc", 904 } ,
            { "sSettlement904PiaProps_Dflp", 904 } ,
            { "s904PiaProps_PaidToThirdParty", 904 } ,
            { "s904PiaProps_ThisPartyIsAffiliate", 904 } ,
            { "s904PiaGfeSection", 904 } ,
            { "sVaFfProps_PdByT", 905 } ,
            { "sVaFfProps_Apr", 905 } ,
            { "sVaFfProps_FhaAllow", 905 } ,
            { "sVaFfProps_Poc", 905 } ,
            { "sVaFfProps_Dflp", 905 } ,
            { "sVaFfProps_PaidToThirdParty", 905 } ,
            { "sVaFfProps_ThisPartyIsAffiliate", 905 } ,
            { "sVaFfPaidTo", 905 } ,
            { "s900U1PiaDesc", 906 } ,
            { "s900U1Pia", 906 } ,
            { "s900U1PiaProps_PdByT", 906 } ,
            { "s900U1PiaProps_Apr", 906 } ,
            { "s900U1PiaProps_FhaAllow", 906 } ,
            { "s900U1PiaProps_Poc", 906 } ,
            { "sSettlement900U1PiaProps_Dflp", 906 } ,
            { "s900U1PiaProps_PaidToThirdParty", 906 } ,
            { "s900U1PiaProps_ThisPartyIsAffiliate", 906 } ,
            { "s900U1PiaGfeSection", 906 } ,
            { "sHazInsRsrvMon", 1002 } ,
            { "sHazInsRsrvProps_PdByT", 1002 } ,
            { "sHazInsRsrvProps_Apr", 1002 } ,
            { "sHazInsRsrvProps_FhaAllow", 1002 } ,
            { "sHazInsRsrvProps_Poc", 1002 } ,
            { "sSettlementHazInsRsrvProps_Dflp", 1002 } ,
            { "sHazInsRsrvProps_PaidToThirdParty", 1002 } ,
            { "sHazInsRsrvProps_ThisPartyIsAffiliate", 1002 } ,
            { "sHazInsRsrvEscrowCushion", 1002 } ,
            { "sHazInsRsrvEscrowDisbursementSchedule", 1002 } ,
            { "sMInsRsrvMon", 1003 } ,
            { "sMInsRsrvProps_PdByT", 1003 } ,
            { "sMInsRsrvProps_Apr", 1003 } ,
            { "sMInsRsrvProps_FhaAllow", 1003 } ,
            { "sMInsRsrvProps_Poc", 1003 } ,
            { "sMInsRsrvEscrowedTri" , 1003 },
            { "sSettlementMInsRsrvProps_Dflp", 1003 } ,
            { "sMInsRsrvProps_PaidToThirdParty", 1003 } ,
            { "sMInsRsrvProps_ThisPartyIsAffiliate", 1003 } ,
            { "sMInsRsrvEscrowCushion", 1003 } ,
            { "sMInsRsrvEscrowDisbursementSchedule", 1003 } ,
            { "sProRealETxR", 1004 } ,
            { "sProRealETxT", 1004 } ,
            { "sProRealETxMb", 1004 } ,
            { "sRealETxRsrvMon", 1004 } ,
            { "sRealETxRsrvProps_PdByT", 1004 } ,
            { "sRealETxRsrvProps_Apr", 1004 } ,
            { "sRealETxRsrvProps_FhaAllow", 1004 } ,
            { "sRealETxRsrvProps_Poc", 1004 } ,
            { "sRealETxRsrvEscrowedTri" , 1004 } ,
            { "sSettlementRealETxRsrvProps_Dflp", 1004 } ,
            { "sRealETxRsrvProps_PaidToThirdParty", 1004 } ,
            { "sRealETxRsrvProps_ThisPartyIsAffiliate", 1004 } ,
            { "sRealETxRsrvEscrowCushion", 1004 } ,
            { "sRealETxRsrvEscrowDisbursementSchedule", 1004 } ,
            { "sSchoolTxRsrvMon", 1005 } ,
            { "sProSchoolTx", 1005 } ,
            { "sSchoolTxRsrvProps_PdByT", 1005 } ,
            { "sSchoolTxRsrvProps_Apr", 1005 } ,
            { "sSchoolTxRsrvProps_FhaAllow", 1005 } ,
            { "sSchoolTxRsrvProps_Poc", 1005 } ,
            { "sSchoolTxRsrvEscrowedTri" , 1005 } ,
            { "sSettlementSchoolTxRsrvProps_Dflp", 1005 } ,
            { "sSchoolTxRsrvProps_PaidToThirdParty", 1005 } ,
            { "sSchoolTxRsrvProps_ThisPartyIsAffiliate", 1005 } ,
            { "sSchoolTxRsrvEscrowCushion", 1005 } ,
            { "sSchoolTxRsrvEscrowDisbursementSchedule", 1005 } ,
            { "sFloodInsRsrvMon", 1006 } ,
            { "sProFloodIns", 1006 } ,
            { "sFloodInsRsrvProps_PdByT", 1006 } ,
            { "sFloodInsRsrvProps_Apr", 1006 } ,
            { "sFloodInsRsrvProps_FhaAllow", 1006 } ,
            { "sFloodInsRsrvProps_Poc", 1006 } ,
            { "sFloodInsRsrvEscrowedTri" , 1006 },
            { "sSettlementFloodInsRsrvProps_Dflp", 1006 } ,
            { "sFloodInsRsrvProps_PaidToThirdParty", 1006 } ,
            { "sFloodInsRsrvProps_ThisPartyIsAffiliate", 1006 } ,
            { "sFloodInsRsrvEscrowCushion", 1006 } ,
            { "sFloodInsRsrvEscrowDisbursementSchedule", 1006 } ,
            { "sAggregateAdjRsrvProps_PdByT", 1007 } ,
            { "sAggregateAdjRsrvProps_Apr", 1007 } ,
            { "sAggregateAdjRsrvProps_FhaAllow", 1007 } ,
            { "sAggregateAdjRsrvProps_Poc", 1007 } ,
            { "sSettlementAggregateAdjRsrvProps_Dflp", 1007 } ,
            { "sAggregateAdjRsrvProps_PaidToThirdParty", 1007 } ,
            { "sAggregateAdjRsrvProps_ThisPartyIsAffiliate", 1007 } ,
            { "s1006ProHExpDesc", 1008 } ,
            { "s1006RsrvMon", 1008 } ,
            { "s1006ProHExp", 1008 } ,
            { "s1006RsrvProps_PdByT", 1008 } ,
            { "s1006RsrvProps_Apr", 1008 } ,
            { "s1006RsrvProps_FhaAllow", 1008 } ,
            { "s1006RsrvProps_Poc", 1008 } ,
            { "s1006RsrvEscrowedTri", 1008 },
            { "sSettlement1008RsrvProps_Dflp", 1008 } ,
            { "s1006RsrvProps_PaidToThirdParty", 1008 } ,
            { "s1006RsrvProps_ThisPartyIsAffiliate", 1008 } ,
            { "s1006RsrvEscrowCushion", 1008 } ,
            { "s1006RsrvEscrowDisbursementSchedule", 1008 } ,
            { "s1007ProHExpDesc", 1009 } ,
            { "s1007RsrvMon", 1009 } ,
            { "s1007ProHExp", 1009 } ,
            { "s1007RsrvProps_PdByT", 1009 } ,
            { "s1007RsrvProps_Apr", 1009 } ,
            { "s1007RsrvProps_FhaAllow", 1009 } ,
            { "s1007RsrvProps_Poc", 1009 } ,
            { "s1007RsrvEscrowedTri", 1009 },
            { "sSettlement1009RsrvProps_Dflp", 1009 } ,
            { "s1007RsrvProps_PaidToThirdParty", 1009 } ,
            { "s1007RsrvProps_ThisPartyIsAffiliate", 1009 } ,
            { "s1007RsrvEscrowCushion", 1009 } ,
            { "s1007RsrvEscrowDisbursementSchedule", 1009 } ,
            { "sU3RsrvDesc", 1010 } ,
            { "sU3RsrvMon", 1010 } ,
            { "sProU3Rsrv", 1010 } ,
            { "sU3RsrvEscrowedTri", 1010},
            { "sU3RsrvProps_PdByT", 1010 } ,
            { "sU3RsrvProps_Apr", 1010 } ,
            { "sU3RsrvProps_FhaAllow", 1010 } ,
            { "sU3RsrvProps_Poc", 1010 } ,
            { "sSettlementU3RsrvProps_Dflp", 1010 } ,
            { "sU3RsrvProps_PaidToThirdParty", 1010 } ,
            { "sU3RsrvProps_ThisPartyIsAffiliate", 1010 } ,
            { "sU3RsrvEscrowCushion", 1010 } ,
            { "sU3RsrvEscrowDisbursementSchedule", 1010 } ,
            { "sU4RsrvDesc", 1011 } ,
            { "sU4RsrvMon", 1011 } ,
            { "sProU4Rsrv", 1011 } ,
            { "sU4RsrvEscrowedTri" , 1011 },
            { "sU4RsrvProps_PdByT", 1011 } ,
            { "sU4RsrvProps_Apr", 1011 } ,
            { "sU4RsrvProps_FhaAllow", 1011 } ,
            { "sU4RsrvProps_Poc", 1011 } ,
            { "sSettlementU4RsrvProps_Dflp", 1011 } ,
            { "sU4RsrvProps_PaidToThirdParty", 1011 } ,
            { "sU4RsrvProps_ThisPartyIsAffiliate", 1011 } ,
            { "sU4RsrvEscrowCushion", 1011 } ,
            { "sU4RsrvEscrowDisbursementSchedule", 1011 } ,
            { "sEscrowFPc", 1102 } ,
            { "sEscrowFBaseT", 1102 } ,
            { "sEscrowFMb", 1102 } ,
            { "sEscrowFProps_PdByT", 1102 } ,
            { "sEscrowFProps_Apr", 1102 } ,
            { "sEscrowFProps_FhaAllow", 1102 } ,
            { "sEscrowFProps_Poc", 1102 } ,
            { "sSettlementEscrowFProps_Dflp", 1102 } ,
            { "sEscrowFProps_PaidToThirdParty", 1102 } ,
            { "sEscrowFProps_ThisPartyIsAffiliate", 1102 } ,
            { "sEscrowFProps_ToBroker", 1102 } ,
            { "sEscrowFTable", 1102 } ,
            { "sEscrowFGfeSection", 1102 } ,
            { "sOwnerTitleInsFPc", 1103 } ,
            { "sOwnerTitleInsFBaseT", 1103 } ,
            { "sOwnerTitleInsFMb", 1103 } ,
            { "sOwnerTitleInsProps_PdByT", 1103 } ,
            { "sOwnerTitleInsProps_Apr", 1103 } ,
            { "sOwnerTitleInsProps_FhaAllow", 1103 } ,
            { "sOwnerTitleInsProps_Poc", 1103 } ,
            { "sSettlementOwnerTitleInsFProps_Dflp", 1103 } ,
            { "sOwnerTitleInsProps_PaidToThirdParty", 1103 } ,
            { "sOwnerTitleInsProps_ThisPartyIsAffiliate", 1103 } ,
            { "sOwnerTitleInsProps_ToBroker", 1103 } ,
            { "sOwnerTitleInsPaidTo", 1103 } ,
            { "sTitleInsFPc", 1104 } ,
            { "sTitleInsFBaseT", 1104 } ,
            { "sTitleInsFMb", 1104 } ,
            { "sTitleInsFProps_PdByT", 1104 } ,
            { "sTitleInsFProps_Apr", 1104 } ,
            { "sTitleInsFProps_FhaAllow", 1104 } ,
            { "sTitleInsFProps_Poc", 1104 } ,
            { "sSettlementTitleInsFProps_Dflp", 1104 } ,
            { "sTitleInsFProps_PaidToThirdParty", 1104 } ,
            { "sTitleInsFProps_ThisPartyIsAffiliate", 1104 } ,
            { "sTitleInsFProps_ToBroker", 1104 } ,
            { "sTitleInsFTable", 1104 } ,
            { "sDocPrepF", 1109 } ,
            { "sDocPrepFProps_PdByT", 1109 } ,
            { "sDocPrepFProps_Apr", 1109 } ,
            { "sDocPrepFProps_FhaAllow", 1109 } ,
            { "sDocPrepFProps_Poc", 1109 } ,
            { "sSettlementDocPrepFProps_Dflp", 1109 } ,
            { "sDocPrepFProps_PaidToThirdParty", 1109 } ,
            { "sDocPrepFProps_ThisPartyIsAffiliate", 1109 } ,
            { "sDocPrepFProps_ToBroker", 1109 } ,
            { "sDocPrepFPaidTo", 1109 } ,
            { "sDocPrepFGfeSection", 1109 } ,
            { "sNotaryF", 1110 } ,
            { "sNotaryFProps_PdByT", 1110 } ,
            { "sNotaryFProps_Apr", 1110 } ,
            { "sNotaryFProps_FhaAllow", 1110 } ,
            { "sNotaryFProps_Poc", 1110 } ,
            { "sSettlementNotaryFProps_Dflp", 1110 } ,
            { "sNotaryFProps_PaidToThirdParty", 1110 } ,
            { "sNotaryFProps_ThisPartyIsAffiliate", 1110 } ,
            { "sNotaryFProps_ToBroker", 1110 } ,
            { "sNotaryFPaidTo", 1110 } ,
            { "sNotaryFGfeSection", 1110 } ,
            { "sAttorneyF", 1111 } ,
            { "sAttorneyFProps_PdByT", 1111 } ,
            { "sAttorneyFProps_Apr", 1111 } ,
            { "sAttorneyFProps_FhaAllow", 1111 } ,
            { "sAttorneyFProps_Poc", 1111 } ,
            { "sSettlementAttorneyFProps_Dflp", 1111 } ,
            { "sAttorneyFProps_PaidToThirdParty", 1111 } ,
            { "sAttorneyFProps_ThisPartyIsAffiliate", 1111 } ,
            { "sAttorneyFProps_ToBroker", 1111 } ,
            { "sAttorneyFPaidTo", 1111 } ,
            { "sAttorneyFGfeSection", 1111 } ,
            { "sU1TcDesc", 1112 } ,
            { "sU1Tc", 1112 } ,
            { "sU1TcProps_PdByT", 1112 } ,
            { "sU1TcProps_Apr", 1112 } ,
            { "sU1TcProps_FhaAllow", 1112 } ,
            { "sU1TcProps_Poc", 1112 } ,
            { "sSettlementU1TcProps_Dflp", 1112 } ,
            { "sU1TcProps_PaidToThirdParty", 1112 } ,
            { "sU1TcProps_ThisPartyIsAffiliate", 1112 } ,
            { "sU1TcProps_ToBroker", 1112 } ,
            { "sU1TcPaidTo", 1112 } ,
            { "sU1TcGfeSection", 1112 } ,
            { "sU2TcDesc", 1113 } ,
            { "sU2Tc", 1113 } ,
            { "sU2TcProps_PdByT", 1113 } ,
            { "sU2TcProps_Apr", 1113 } ,
            { "sU2TcProps_FhaAllow", 1113 } ,
            { "sU2TcProps_Poc", 1113 } ,
            { "sSettlementU2TcProps_Dflp", 1113 } ,
            { "sU2TcProps_PaidToThirdParty", 1113 } ,
            { "sU2TcProps_ThisPartyIsAffiliate", 1113 } ,
            { "sU2TcProps_ToBroker", 1113 } ,
            { "sU2TcPaidTo", 1113 } ,
            { "sU2TcGfeSection", 1113 } ,
            { "sU3TcDesc", 1114 } ,
            { "sU3Tc", 1114 } ,
            { "sU3TcProps_PdByT", 1114 } ,
            { "sU3TcProps_Apr", 1114 } ,
            { "sU3TcProps_FhaAllow", 1114 } ,
            { "sU3TcProps_Poc", 1114 } ,
            { "sSettlementU3TcProps_Dflp", 1114 } ,
            { "sU3TcProps_PaidToThirdParty", 1114 } ,
            { "sU3TcProps_ThisPartyIsAffiliate", 1114 } ,
            { "sU3TcProps_ToBroker", 1114 } ,
            { "sU3TcPaidTo", 1114 } ,
            { "sU3TcGfeSection", 1114 } ,
            { "sU4TcDesc", 1115 } ,
            { "sU4Tc", 1115 } ,
            { "sU4TcProps_PdByT", 1115 } ,
            { "sU4TcProps_Apr", 1115 } ,
            { "sU4TcProps_FhaAllow", 1115 } ,
            { "sU4TcProps_Poc", 1115 } ,
            { "sSettlementU4TcProps_Dflp", 1115 } ,
            { "sU4TcProps_PaidToThirdParty", 1115 } ,
            { "sU4TcProps_ThisPartyIsAffiliate", 1115 } ,
            { "sU4TcProps_ToBroker", 1115 } ,
            { "sU4TcPaidTo", 1115 } ,
            { "sU4TcGfeSection", 1115 } ,
            { "sRecFPc", 1201 } ,
            { "sRecBaseT", 1201 } ,
            { "sRecFMb", 1201 } ,
            { "sRecFLckd", 1201 } ,
            { "sRecDeed", 1201 } ,
            { "sRecMortgage", 1201 } ,
            { "sRecRelease", 1201 } ,
            { "sRecFProps_PdByT", 1201 } ,
            { "sRecFProps_Apr", 1201 } ,
            { "sRecFProps_FhaAllow", 1201 } ,
            { "sRecFProps_Poc", 1201 } ,
            { "sSettlementRecFProps_Dflp", 1201 } ,
            { "sRecFProps_PaidToThirdParty", 1201 } ,
            { "sRecFProps_ThisPartyIsAffiliate", 1201 } ,
            { "sRecFDesc", 1201 } ,
            { "sCountyRtcPc", 1204 } ,
            { "sCountyRtcBaseT", 1204 } ,
            { "sCountyRtcMb", 1204 } ,
            { "sCountyRtcProps_PdByT", 1204 } ,
            { "sCountyRtcProps_Apr", 1204 } ,
            { "sCountyRtcProps_FhaAllow", 1204 } ,
            { "sCountyRtcProps_Poc", 1204 } ,
            { "sSettlementCountyRtcProps_Dflp", 1204 } ,
            { "sCountyRtcProps_PaidToThirdParty", 1204 } ,
            { "sCountyRtcProps_ThisPartyIsAffiliate", 1204 } ,
            { "sCountyRtcDesc", 1204 } ,
            { "sStateRtcPc", 1205 } ,
            { "sStateRtcBaseT", 1205 } ,
            { "sStateRtcMb", 1205 } ,
            { "sStateRtcProps_PdByT", 1205 } ,
            { "sStateRtcProps_Apr", 1205 } ,
            { "sStateRtcProps_FhaAllow", 1205 } ,
            { "sStateRtcProps_Poc", 1205 } ,
            { "sSettlementStateRtcProps_Dflp", 1205 } ,
            { "sStateRtcProps_PaidToThirdParty", 1205 } ,
            { "sStateRtcProps_ThisPartyIsAffiliate", 1205 } ,
            { "sStateRtcDesc", 1205 } ,
            { "sU1GovRtcDesc", 1206 } ,
            { "sU1GovRtcPc", 1206 } ,
            { "sU1GovRtcBaseT", 1206 } ,
            { "sU1GovRtcMb", 1206 } ,
            { "sU1GovRtcGfeSection", 1206 } ,
            { "sU1GovRtcProps_PdByT", 1206 } ,
            { "sU1GovRtcProps_Apr", 1206 } ,
            { "sU1GovRtcProps_FhaAllow", 1206 } ,
            { "sU1GovRtcProps_Poc", 1206 } ,
            { "sSettlementU1GovRtcProps_Dflp", 1206 } ,
            { "sU1GovRtcProps_PaidToThirdParty", 1206 } ,
            { "sU1GovRtcProps_ThisPartyIsAffiliate", 1206 } ,
            { "sU1GovRtcPaidTo", 1206 } ,
            { "sU2GovRtcDesc", 1207 } ,
            { "sU2GovRtcPc", 1207 } ,
            { "sU2GovRtcBaseT", 1207 } ,
            { "sU2GovRtcMb", 1207 } ,
            { "sU2GovRtcGfeSection", 1207 } ,
            { "sU2GovRtcProps_PdByT", 1207 } ,
            { "sU2GovRtcProps_Apr", 1207 } ,
            { "sU2GovRtcProps_FhaAllow", 1207 } ,
            { "sU2GovRtcProps_Poc", 1207 } ,
            { "sSettlementU2GovRtcProps_Dflp", 1207 } ,
            { "sU2GovRtcProps_PaidToThirdParty", 1207 } ,
            { "sU2GovRtcProps_ThisPartyIsAffiliate", 1207 } ,
            { "sU2GovRtcPaidTo", 1207 } ,
            { "sU3GovRtcDesc", 1208 } ,
            { "sU3GovRtcPc", 1208 } ,
            { "sU3GovRtcBaseT", 1208 } ,
            { "sU3GovRtcMb", 1208 } ,
            { "sU3GovRtcGfeSection", 1208 } ,
            { "sU3GovRtcProps_PdByT", 1208 } ,
            { "sU3GovRtcProps_Apr", 1208 } ,
            { "sU3GovRtcProps_FhaAllow", 1208 } ,
            { "sU3GovRtcProps_Poc", 1208 } ,
            { "sSettlementU3GovRtcProps_Dflp", 1208 } ,
            { "sU3GovRtcProps_PaidToThirdParty", 1208 } ,
            { "sU3GovRtcProps_ThisPartyIsAffiliate", 1208 } ,
            { "sU3GovRtcPaidTo", 1208 } ,
            { "sPestInspectF", 1302 } ,
            { "sPestInspectFProps_PdByT", 1302 } ,
            { "sPestInspectFProps_Apr", 1302 } ,
            { "sPestInspectFProps_FhaAllow", 1302 } ,
            { "sPestInspectFProps_Poc", 1302 } ,
            { "sSettlementPestInspectFProps_Dflp", 1302 } ,
            { "sPestInspectFProps_PaidToThirdParty", 1302 } ,
            { "sPestInspectFProps_ThisPartyIsAffiliate", 1302 } ,
            { "sPestInspectFProps_ToBroker", 1302 } ,
            { "sPestInspectPaidTo", 1302 } ,
            { "sU1ScDesc", 1303 } ,
            { "sU1Sc", 1303 } ,
            { "sU1ScProps_PdByT", 1303 } ,
            { "sU1ScProps_Apr", 1303 } ,
            { "sU1ScProps_FhaAllow", 1303 } ,
            { "sU1ScProps_Poc", 1303 } ,
            { "sSettlementU1ScProps_Dflp", 1303 } ,
            { "sU1ScProps_PaidToThirdParty", 1303 } ,
            { "sU1ScProps_ThisPartyIsAffiliate", 1303 } ,
            { "sU1ScProps_ToBroker", 1303 } ,
            { "sU1ScPaidTo", 1303 } ,
            { "sU1ScGfeSection", 1303 } ,
            { "sU2ScDesc", 1304 } ,
            { "sU2Sc", 1304 } ,
            { "sU2ScProps_PdByT", 1304 } ,
            { "sU2ScProps_Apr", 1304 } ,
            { "sU2ScProps_FhaAllow", 1304 } ,
            { "sU2ScProps_Poc", 1304 } ,
            { "sSettlementU2ScProps_Dflp", 1304 } ,
            { "sU2ScProps_PaidToThirdParty", 1304 } ,
            { "sU2ScProps_ThisPartyIsAffiliate", 1304 } ,
            { "sU2ScProps_ToBroker", 1304 } ,
            { "sU2ScPaidTo", 1304 } ,
            { "sU2ScGfeSection", 1304 } ,
            { "sU3ScDesc", 1305 } ,
            { "sU3Sc", 1305 } ,
            { "sU3ScProps_PdByT", 1305 } ,
            { "sU3ScProps_Apr", 1305 } ,
            { "sU3ScProps_FhaAllow", 1305 } ,
            { "sU3ScProps_Poc", 1305 } ,
            { "sSettlementU3ScProps_Dflp", 1305 } ,
            { "sU3ScProps_PaidToThirdParty", 1305 } ,
            { "sU3ScProps_ThisPartyIsAffiliate", 1305 } ,
            { "sU3ScProps_ToBroker", 1305 } ,
            { "sU3ScPaidTo", 1305 } ,
            { "sU3ScGfeSection", 1305 } ,
            { "sU4ScDesc", 1306 } ,
            { "sU4Sc", 1306 } ,
            { "sU4ScProps_PdByT", 1306 } ,
            { "sU4ScProps_Apr", 1306 } ,
            { "sU4ScProps_FhaAllow", 1306 } ,
            { "sU4ScProps_Poc", 1306 } ,
            { "sSettlementU4ScProps_Dflp", 1306 } ,
            { "sU4ScProps_PaidToThirdParty", 1306 } ,
            { "sU4ScProps_ThisPartyIsAffiliate", 1306 } ,
            { "sU4ScProps_ToBroker", 1306 } ,
            { "sU4ScPaidTo", 1306 } ,
            { "sU4ScGfeSection", 1306 } ,
            { "sU5ScDesc", 1307 } ,
            { "sU5Sc", 1307 } ,
            { "sU5ScProps_PdByT", 1307 } ,
            { "sU5ScProps_Apr", 1307 } ,
            { "sU5ScProps_FhaAllow", 1307 } ,
            { "sU5ScProps_Poc", 1307 } ,
            { "sSettlementU5ScProps_Dflp", 1307 } ,
            { "sU5ScProps_PaidToThirdParty", 1307 } ,
            { "sU5ScProps_ThisPartyIsAffiliate", 1307 } ,
            { "sU5ScProps_ToBroker", 1307 } ,
            { "sU5ScPaidTo", 1307 } ,
            { "sU5ScGfeSection", 1307 } 
        };
    }
}
