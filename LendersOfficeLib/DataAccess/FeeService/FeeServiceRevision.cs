﻿// <copyright file="FeeServiceRevision.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Jhairo Erazo
//    Date:   10/12/2016
// </summary>
namespace DataAccess.FeeService
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Xml.Serialization;
    using LendersOffice.Security;

    /// <summary>
    /// Represents a user uploaded Fee Service template revision.
    /// </summary>
    public class FeeServiceRevision
    {
        /// <summary>
        /// Stored procedure for retrieving only the latest production revision.
        /// </summary>
        private static readonly string SpRetrieveLatestProd = "FEE_SERVICE_RetrieveLatestProductionRevision";

        /// <summary>
        /// Stored procedure for retrieving only the latest test revision.
        /// </summary>
        private static readonly string SpRetrieveLatestTest = "FEE_SERVICE_RetrieveLatestTestRevision";

        /// <summary>
        /// Revision ID.
        /// </summary>
        private int? id;

        /// <summary>
        /// Upload Date.
        /// </summary>
        private DateTime uploadedD;

        /// <summary>
        /// The Broker Id.
        /// </summary>
        private Guid brokerId;

        /// <summary>
        /// ID of user who uploaded this revision.
        /// </summary>
        private Guid uploadedByUserId;

        /// <summary>
        /// Name of user who uploaded this revision.
        /// </summary>
        private string uploadedByUserName;

        /// <summary>
        /// Revision Comments.
        /// </summary>
        private string comments;

        /// <summary>
        /// FileDB key.
        /// </summary>
        private string fileDbKey;

        /// <summary>
        /// Is this a test revision?.
        /// </summary>
        private bool isTest;

        /// <summary>
        /// Fee Service Templates.
        /// </summary>
        private IEnumerable<FeeServiceTemplate> feeTemplates = Enumerable.Empty<FeeServiceTemplate>();

        /// <summary>
        /// Initializes a new instance of the<see cref="FeeServiceRevision" /> class.
        /// </summary>
        /// <param name="reader">SQL data reader.</param>
        /// <param name="isForListing">Is this revision used only in a UI listing?.</param>
        internal FeeServiceRevision(DbDataReader reader, bool isForListing)
        {
            this.id = (int)reader["Id"];
            this.FeeTemplateXmlContent = isForListing ? string.Empty : (string)reader["FeeTemplateXmlContent"];
            this.brokerId = (Guid)reader["BrokerId"];
            this.comments = (string)reader["Comments"];
            this.fileDbKey = (string)reader["FileDbKey"];
            this.uploadedByUserId = (Guid)reader["UploadedByUserId"];
            this.uploadedByUserName = reader["UploadedByUserName"] == DBNull.Value ? string.Empty : (string)reader["UploadedByUserName"];
            this.uploadedD = Convert.ToDateTime(reader["UploadedD"]);
            this.isTest = (bool)reader["IsTest"];
        }

        /// <summary>
        /// Initializes a new instance of the<see cref="FeeServiceRevision" /> class.
        /// </summary>
        /// <param name="principal">Abstract User Principal.</param>
        /// <param name="feeTemplates">Fee service templates.</param>
        /// <param name="comments">Revision comments.</param>
        /// <param name="fileDbKey">FileDB key.</param>
        /// <param name="isTest">Is this a test revision?.</param>
        internal FeeServiceRevision(AbstractUserPrincipal principal, IEnumerable<FeeServiceTemplate> feeTemplates, string comments, string fileDbKey, bool isTest)
        {
            this.brokerId = principal.BrokerId;
            this.comments = comments;
            this.fileDbKey = fileDbKey;
            this.uploadedByUserId = principal.UserId;
            this.uploadedByUserName = principal.FirstName + " " + principal.LastName;
            this.uploadedD = DateTime.Now;
            this.isTest = isTest;

            if (feeTemplates != null)
            {
                this.feeTemplates = feeTemplates;
            }
        }

        /// <summary>
        /// Gets all fee service templates.
        /// </summary>
        /// <value>An enumerable of FeeServiceTemplates.</value>
        public IEnumerable<FeeServiceTemplate> FeeTemplates
        {
            get
            {
                return this.feeTemplates;
            }
        }

        /// <summary>
        /// Gets the Revision ID.
        /// </summary>
        /// <value>Revision ID.</value>
        public int? Id
        {
            get { return this.id; }
        }

        /// <summary>
        /// Gets the Uploaded Date.
        /// </summary>
        /// <value>Uploaded Date.</value>
        public DateTime UploadedD
        {
            get { return this.uploadedD; }
        }

        /// <summary>
        /// Gets the uploaded date in string format.
        /// </summary>
        /// <value>Uploaded date as string.</value>
        public string UploadedD_rep
        {
            get { return this.uploadedD.ToString("M/d/yyyy h:mm tt"); }
        }

        /// <summary>
        /// Gets uploaded name of the user that uploaded this revision.
        /// </summary>
        /// <value>Name of user that uploaded this revision.</value>
        public string UploadedByUserName
        {
            get { return this.uploadedByUserName; }
        }

        /// <summary>
        /// Gets the revision comments.
        /// </summary>
        /// <value>Revision comments.</value>
        public string Comments
        {
            get { return this.comments; }
        }

        /// <summary>
        /// Gets the FileDB key.
        /// </summary>
        /// <value>FileDB key.</value>
        public string FileDbKey
        {
            get { return this.fileDbKey; }
        }

        /// <summary>
        /// Gets a value indicating whether if this is a test revision.
        /// </summary>
        /// <value>Whether this is a test revision.</value>
        public bool IsTest
        {
            get { return this.isTest; }
        }

        /// <summary>
        /// Gets or sets Fee Service Templates as an XML string.
        /// </summary>
        /// <value>An XML string.</value>
        private string FeeTemplateXmlContent
        {
            get
            {
                using (var ms = new MemoryStream())
                {
                    var xs = new XmlSerializer(typeof(List<FeeServiceTemplate>));
                    xs.Serialize(ms, this.feeTemplates.ToList());
                    return Encoding.ASCII.GetString(ms.ToArray());
                }
            }

            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    this.feeTemplates = Enumerable.Empty<FeeServiceTemplate>();
                    return;
                }

                using (var ms = new MemoryStream(ASCIIEncoding.ASCII.GetBytes(value)))
                {
                    var xs = new XmlSerializer(typeof(List<FeeServiceTemplate>));
                    ms.Position = 0;
                    try
                    {
                        this.feeTemplates = (List<FeeServiceTemplate>)xs.Deserialize(ms);
                    }
                    catch (InvalidOperationException)
                    {
                        Tools.LogError("FeeService: Failed to deserialize fee service templates.");
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Retrieves the latest production revision.
        /// </summary>
        /// <param name="brokerId">Broker ID.</param>
        /// <returns>The latest production FeeServiceRevision.</returns>
        public static FeeServiceRevision RetrieveLatestRevision(Guid brokerId)
        {
            return RetrieveRevisionCached(brokerId, SpRetrieveLatestProd);
        }

        /// <summary>
        /// Retrieves the latest test revision.
        /// </summary>
        /// <param name="brokerId">Broker ID.</param>
        /// <returns>The latest test FeeServiceRevision.</returns>
        public static FeeServiceRevision RetrieveLatestTestRevision(Guid brokerId)
        {
            return RetrieveRevisionCached(brokerId, SpRetrieveLatestTest);
        }

        /// <summary>
        /// Retrieves the particular revision.
        /// </summary>
        /// <param name="brokerId">Broker ID.</param>
        /// <param name="rev">Particular revision.</param>
        /// <returns>The FeeServiceRevision.</returns>
        public static FeeServiceRevision RetrieveRevision(Guid brokerId, int rev) 
        {
            return RetrieveRevisionCached(brokerId, "FEE_SERVICE_RetrieveRevision", rev);
        }

        /// <summary>
        /// Save the revision. Always creates a new revision.
        /// </summary>
        public void Save()
        {
            SqlParameter[] parameters =
                                        {
                                            new SqlParameter("@BrokerId", this.brokerId),
                                            new SqlParameter("@Comments", this.comments),
                                            new SqlParameter("@FeeTemplateXmlContent", this.FeeTemplateXmlContent),
                                            new SqlParameter("@FileDbKey", this.fileDbKey),
                                            new SqlParameter("@UploadedByUserId", this.uploadedByUserId),
                                            new SqlParameter("@UploadedD", this.uploadedD),
                                            new SqlParameter("@IsTest", this.isTest)
                                        };

            object newId = StoredProcedureHelper.ExecuteScalar(this.brokerId, "FEE_SERVICE_SaveRevision", parameters);

            this.id = (int)newId;
        }

        /// <summary>
        /// Get all of the fee templates that match the loan and loan program data.
        /// </summary>
        /// <param name="data">The loan and loan program data.</param>
        /// <returns>Enumerable of the applicable templates.</returns>
        public IEnumerable<FeeServiceTemplate> GetApplicableFeeTemplates(FeeServiceMatchingTemplateData data)
        {
            return this.FeeTemplates.Where(template => template.EvaluateConditions(data));
        }

        /// <summary>
        /// Retrieves a revision from cache, or from DB is revision not found in cache.
        /// </summary>
        /// <param name="brokerId">Broker ID.</param>
        /// <param name="spName">Stored procedure name.</param>
        /// <param name="historicalRevision">Historical Revision for retrieving. Retrive the lastest revision if historicalRevision == null.</param>
        /// <returns>A fee service revision.</returns>
        private static FeeServiceRevision RetrieveRevisionCached(Guid brokerId, string spName, int? historicalRevision = null)
        {
            string key = string.Concat("FeeServiceRevision_", brokerId.ToString(), "_", spName);

            if (historicalRevision.HasValue)
            {
                key = key + "_" + historicalRevision.Value.ToString();
            }

            if (HttpContext.Current != null && HttpContext.Current.Items.Contains(key))
            {
                return (FeeServiceRevision)HttpContext.Current.Items[key];
            }

            List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@BrokerId", brokerId),                    
                };

            if (historicalRevision.HasValue)
            {
                parameters.Add(new SqlParameter("@Id", historicalRevision.Value));
            }

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, spName, parameters))
            {
                if (reader.Read())
                {
                    FeeServiceRevision revision = new FeeServiceRevision(reader, false);

                    if (HttpContext.Current != null)
                    {
                        HttpContext.Current.Items.Add(key, revision);
                    }

                    return revision;
                }
            }

            return null;
        }
    }
}
