﻿namespace DataAccess.FeeService
{
    /// <summary>
    /// Fee Service Rule Type.
    /// </summary>
    public enum FeeServiceRuleType
    {
        /// <summary>
        /// For Legacy/"Old" Fee Service rules that rely on
        /// field ID to set the appropriate field.
        /// </summary>
        Legacy = 0,

        /// <summary>
        /// For ClosingCost/"New" Fee Service rules that rely
        /// on closing cost fee type ID to set the appropriate fee.
        /// </summary>
        ClosingCost = 1,

        /// <summary>
        /// For Title Source rules use the set the title providers
        /// used to generate title quote/policy and set title fees.
        /// </summary>
        TitleSource = 2
    }
}
