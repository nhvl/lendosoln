﻿// <copyright file="FeeServiceUtilities.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Jhairo Erazo
//    Date:   10/11/2016
// </summary>
namespace DataAccess.FeeService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using LendersOffice.Common;
    using LendersOffice.ObjLib.TitleProvider;
    using LendersOffice.Security;

    /// <summary>
    /// Utilities Class for Fee Service.
    /// </summary>
    public static class FeeServiceUtilities
    {
        /// <summary>
        /// Upload a new revision.
        /// </summary>
        /// <param name="principal">The uploading user.</param>
        /// <param name="filepath">Path to uploaded file.</param>
        /// <param name="comments">Comments for the revision.</param>
        /// <param name="isTest">Is this a test revision?.</param>
        /// <param name="errors">Lookup of line numbers grouped by error.</param>
        /// <returns>True if file was valid and upload succeeded. Otherwise, false.</returns>
        public static bool UploadRevision(AbstractUserPrincipal principal, string filepath, string comments, bool isTest, out Dictionary<string, List<int>> errors)
        {
            var parser = new FeeServiceParser(principal.BrokerId);

            var feeTemplates = parser.ParseFile(filepath, out errors);
            if (errors != null && errors.Any())
            {
                return false;
            }

            var fileDbKey = string.Format("fee_service_{0}", Guid.NewGuid());
            FileDBTools.WriteFile(E_FileDB.Normal, fileDbKey, filepath);

            var revision = new FeeServiceRevision(principal, feeTemplates, comments, fileDbKey, isTest);
            revision.Save();

            return true;
        }

        /// <summary>
        /// Creates a blank revision used to record changes in title vendor id.
        /// </summary>
        /// <param name="principal">The uploading user.</param>
        /// <param name="titleVendorId">Title vendor ID.</param>
        /// <returns>Always returns true if method succeeds without error.</returns>
        public static bool RecordTitleVendorChange(AbstractUserPrincipal principal, int titleVendorId)
        {
            // Get Title Vendor Name.
            string vendorName = "Fee Service Settings";
            if (titleVendorId != -1)
            {
                vendorName = TitleProvider.GetProvider(titleVendorId).Name;
            }

            string comments = "Obtain Escrow/Title/Recording/Transfer Tax From changed to " + vendorName;

            var revision = new FeeServiceRevision(principal, null, comments, string.Empty, false);
            revision.Save();

            return true;
        }

        /// <summary>
        /// Release the latest test revision to production.
        /// </summary>
        /// <param name="principal">The uploading user.</param>
        /// <returns>True if upload succeeded. Otherwise, false.</returns>
        /// <remarks>Should only be called with revision history that includes test revisions.</remarks>
        public static bool ReleaseTestToProduction(AbstractUserPrincipal principal)
        {
            // Get current test revision.
            FeeServiceRevision currentTestRevision = FeeServiceRevision.RetrieveLatestTestRevision(principal.BrokerId);

            if (currentTestRevision == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "ReleaseTestToProduction must be called from a FeeServiceRevisionHistory object that includes test revisions.");
            }

            // Copy test Fee Service file to prod.
            string tempFilePath = FileDBTools.CreateCopy(E_FileDB.Normal, currentTestRevision.FileDbKey);

            var fileDbKey = string.Format("fee_service_{0}", Guid.NewGuid());
            FileDBTools.WriteFile(E_FileDB.Normal, fileDbKey, tempFilePath);

            // Clone revision. Set IsTest = false.
            var revision = new FeeServiceRevision(principal, currentTestRevision.FeeTemplates, currentTestRevision.Comments, fileDbKey, false);
            revision.Save();

            return true;
        }
    }
}
