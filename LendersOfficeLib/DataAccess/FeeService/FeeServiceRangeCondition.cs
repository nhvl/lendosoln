﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using LendersOffice.Common;
using System.Xml.Schema;
using System.Xml;

namespace DataAccess.FeeService
{
    /// <summary>
    /// Condition in which a field value is required to be within a range.
    /// </summary>
    public class FeeServiceRangeCondition: AbstractFeeServiceCondition
    {
        // Bounds are rounded to the nearest dollar. Left type as decimal
        // so support for cents can be re-added easily.
        private decimal m_lowerBound;
        public decimal LowerBound
        {
            get { return Math.Round(m_lowerBound, MidpointRounding.AwayFromZero); }
            private set { m_lowerBound = Math.Round(value, MidpointRounding.AwayFromZero); }
        }

        private decimal m_upperBound;
        public decimal UpperBound 
        {
            get { return Math.Round(m_upperBound, MidpointRounding.AwayFromZero); }
            private set { m_upperBound = Math.Round(value, MidpointRounding.AwayFromZero); }
        }

        public FeeServiceRangeCondition()
        {
            FieldId = "";
        }

        public FeeServiceRangeCondition(string fieldId, decimal lowerBound, decimal upperBound)
        {
            FieldId = fieldId;
            LowerBound = lowerBound;
            UpperBound = upperBound;
        }

        /// <summary>
        /// Determine if the loan data satisfies this condition.
        /// </summary>
        /// <param name="data">The loan data to test.</param>
        /// <returns>True if the value of the field is within the range (inclusive).</returns>
        public override bool Evaluate(FeeServiceMatchingTemplateData data)
        {
            decimal toCompare;
            switch (FieldId)
            {
                case "sLAmt": toCompare = data.sLAmt; break;
                case "sApprVal": toCompare = data.sApprVal; break;
                case "sPurchPrice": toCompare = data.sPurchPrice; break;
                case "sFinalLAmt": toCompare = data.sFinalLAmt; break;
                default: throw new CBaseException(ErrorMessages.Generic, "Unhandled field id in fee service condition.");
            }

            toCompare = Tools.AlwaysRoundUp(toCompare, 0);
            return toCompare >= LowerBound && toCompare <= UpperBound;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var otherCondition = obj as FeeServiceRangeCondition;
            if (otherCondition == null) return false;

            return FieldId == otherCondition.FieldId && LowerBound == otherCondition.LowerBound
                && UpperBound == otherCondition.UpperBound;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 31;
                hash = hash * 37 + FieldId.GetHashCode();
                hash = hash * 37 + LowerBound.GetHashCode();
                hash = hash * 37 + UpperBound.GetHashCode();
                return hash;
            }
        }

        public override string ToString()
        {
            return string.Format("({0}<={1}<={2})", LowerBound.ToString("N0"), 
                FieldId, UpperBound.ToString("N0"));
        }

        #region IXmlSerializable Members
        // Basic Format:
        // <Condition ConditionType="" FieldId="" LowerBound="" UpperBound="" />
        public override XmlSchema GetSchema()
        {
            return null;
        }
        
        public override void ReadXml(XmlReader reader)
        {
            FieldId = reader.GetAttribute("FieldId");
            LowerBound = decimal.Parse(reader.GetAttribute("LowerBound"));
            UpperBound = decimal.Parse(reader.GetAttribute("UpperBound"));
            reader.ReadStartElement("Condition");
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("ConditionType", E_FeeServiceConditionT.Range.ToString("d"));
            writer.WriteAttributeString("FieldId", FieldId);
            writer.WriteAttributeString("LowerBound", LowerBound.ToString());
            writer.WriteAttributeString("UpperBound", UpperBound.ToString());
        }
        #endregion
    }
}
