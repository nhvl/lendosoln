﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using LendersOffice.Common;
using System.Xml;
using System.Xml.Schema;

namespace DataAccess.FeeService
{
    /// <summary>
    /// Condition in which a field value is required to be in a set of values.
    /// </summary>
    public class FeeServiceEnumeratedCondition: AbstractFeeServiceCondition
    {
        public HashSet<string> Values { get; private set; }

        public FeeServiceEnumeratedCondition()
        {
            FieldId = "";
            Values = new HashSet<string>();
        }

        public FeeServiceEnumeratedCondition(string fieldId, HashSet<string> values)
        {
            FieldId = fieldId;
            Values = values;
        }

        /// <summary>
        /// Determine if the loan data satisfies this condition.
        /// </summary>
        /// <param name="data">The loan data to test.</param>
        /// <returns>True if the field value is included in the value set.</returns>
        public override bool Evaluate(FeeServiceMatchingTemplateData data)
        {
            switch (FieldId)
            {
                case "sLPurposeT": return Values.Contains(data.sLPurposeT.ToString("d"));
                case "sFinMethT": return Values.Contains(data.sFinMethT.ToString("d"));
                case "sLienPosT": return Values.Contains(data.sLienPosT.ToString("d"));
                case "aOccT": return Values.Contains(data.aOccT.ToString("d"));
                case "sSpState": return Values.Contains(data.sSpState);
                case "sLT": return Values.Contains(data.sLT.ToString("d"));
                case "sBranchChannelT": return Values.Contains(data.sBranchChannelT.ToString("d"));
                case "csLpProductType": return Values.Contains(data.lLpProductType);
                case "csLpInvestorNm": return Values.Contains(data.lLpInvestorNm);
                case "csBranchNm": return Values.Contains(data.sBranchId.ToString().Replace("-", ""));
                case "sProdSpT": return Values.Contains(data.sProdSpT.ToString("d"));
                case "sProdImpound": return Values.Contains(data.sProdImpound.ToString());
                case "sLenderFeeBuyoutRequestedT": return Values.Contains(data.sLenderFeeBuyoutRequestedT.ToString("d"));
                case "sLenderFeeBuyoutRequested":
                    // 10/3/2014 dd - Related to OPM 186233 - Handle the old keyword sLenderFeeBuyoutRequested while waiting for migration
                    // to new enum key.
                    string valueToCompare = string.Empty;
                    if (data.sLenderFeeBuyoutRequestedT == E_sLenderFeeBuyoutRequestedT.Yes)
                    {
                        valueToCompare = "True";
                    }
                    else if (data.sLenderFeeBuyoutRequestedT == E_sLenderFeeBuyoutRequestedT.No)
                    {
                        valueToCompare = "False";
                    }
                    return Values.Contains(valueToCompare);
                case "csLeadSrcId": return Values.Contains(data.sLeadSrcId.ToString("d"));
                case "sCorrespondentProcessT": return Values.Contains(data.sCorrespondentProcessT.ToString("d"));
                case "csPmlCompanyTierId": return Values.Contains(data.sPmlCompanyTierId.ToString("d"));
                case "sDisclosureRegulationT": return Values.Contains(data.sDisclosureRegulationT.ToString("d"));
                case "sIsNewConstruction": return Values.Contains(data.sIsNewConstruction.ToString());
                default: throw new CBaseException(ErrorMessages.Generic, "Unhandled field id=[" + FieldId + "] in fee service condition.");
            }
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var otherCondition = obj as FeeServiceEnumeratedCondition;
            if (otherCondition == null) return false;

            return FieldId == otherCondition.FieldId && Values.SetEquals(otherCondition.Values);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 31;
                hash = hash * 37 + FieldId.GetHashCode();
                foreach (var value in Values)
                {
                    hash = hash * 37 + value.GetHashCode();
                }
                return hash;
            }
        }

        public override string ToString()
        {
            // Sort the field values such that equivalent enumerated conditions have the same rep.
            var valueCsv = string.Join(",", Values.OrderBy(field => field).ToArray());
            return string.Format("({0}={1})", FieldId, valueCsv);
        }

        #region IXmlSerializable Members
        // <EnumeratedCondition FieldId="">
        //   <Value></Value>
        //   <Value></Value>
        //   ...
        // </EnumeratedCondition>
        public override XmlSchema GetSchema()
        {
            return null;
        }

        public override void ReadXml(XmlReader reader)
        {
            FieldId = reader.GetAttribute("FieldId");

            bool isEmptyElement = reader.IsEmptyElement;
            if (reader.IsEmptyElement)
            {
                throw new CBaseException(ErrorMessages.Generic, "An enumerated condition should never exist without values.");
            }

            reader.ReadStartElement("Condition");
            while (reader.NodeType == XmlNodeType.Element && reader.Name == "Value")
            {
                Values.Add(reader.ReadElementString("Value"));
            }
            reader.ReadEndElement();
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("ConditionType", E_FeeServiceConditionT.Enumerated.ToString("d"));
            writer.WriteAttributeString("FieldId", FieldId);
            foreach (var value in Values)
            {
                writer.WriteElementString("Value", value);
            }
        }
        #endregion
    }
}
