﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Xml.Serialization;
using System.Xml.Schema;
using System.Xml;
using CommonProjectLib.Common.Lib;
using System.IO;

namespace DataAccess.FeeService
{
    public class FeeServiceTemplate: IXmlSerializable
    {
        private Dictionary<string, TemplateField> TemplateFieldsByKey { get; set; }
        private static XmlSerializer x_conditionSetSerializer = new XmlSerializer(typeof(FeeServiceConditionSet));
        public FeeServiceConditionSet Conditions { get; set; }

    public FeeServiceTemplate()
        {
            TemplateFieldsByKey = new Dictionary<string, TemplateField>();
            Conditions = new FeeServiceConditionSet();
        }

        public FeeServiceTemplate(string fieldId, string value, FeeServiceRuleType ruleType, Guid typeId, E_FeeServiceFeePropertyT feeProperty, int lineNumber, HashSet<AbstractFeeServiceCondition> conditions)
        {
            TemplateField tf = new TemplateField(fieldId, value, ruleType, typeId, feeProperty, lineNumber);

            TemplateFieldsByKey = new Dictionary<string, TemplateField>();
            TemplateFieldsByKey.Add(tf.Key, tf);
            Conditions = new FeeServiceConditionSet(conditions);
        }

        /// <summary>
        /// Adds a field and value to the template with a line number. 
        /// </summary>
        /// <param name="fieldId">The field id to set.</param>
        /// <param name="value">The value to set for the field.</param>
        /// <param name="lineNumber">The line number from the service file.</param>
        /// <returns>True if the field was added to the template. Otherwise, false.</returns>
        public bool AddField(string fieldId, string value, FeeServiceRuleType ruleType, Guid typeId, E_FeeServiceFeePropertyT feeProperty, int lineNumber)
        {
            TemplateField tf = new TemplateField(fieldId, value, ruleType, typeId, feeProperty, lineNumber);

            if (TemplateFieldsByKey.ContainsKey(tf.Key))
            {
                return false;
            }

            TemplateFieldsByKey.Add(tf.Key, tf);
            return true;
        }

        /// <summary>
        /// Get the fields and values that need to be set when the conditions are satisfied.
        /// </summary>
        /// <returns>Enumerable of TemplateFields, which contain field ids and values to set.</returns>
        public IEnumerable<TemplateField> GetTemplateFields()
        {
            return TemplateFieldsByKey.Values;
        }

        /// <summary>
        /// Evaluate all of the conditions associated with this fee template.
        /// Note: returns true if there aren't any conditions associated with the template.
        /// </summary>
        /// <param name="data">The loan and loan program data to compare against.</param>
        /// <returns>True if all conditions are satisfied or there are no conditions. Otherwise, false.</returns>
        public bool EvaluateConditions(FeeServiceMatchingTemplateData data)
        {
            return Conditions.Evaluate(data);
        }

        public override string ToString()
        {
            return Conditions.ToString();
        }

        public string GetSerializedConditions()
        {
            string xml;
            using (var memoryStream = new MemoryStream())
            {
                x_conditionSetSerializer.Serialize(memoryStream, Conditions);
                xml = Encoding.ASCII.GetString(memoryStream.ToArray()); 
            }

            return xml;
        }

        #region IXmlSerializable Members
        // Basic Format:
        // <FeeServiceTemplate>
        //   <FieldsAndValuesToSet>
        //     <Set FieldId="" Value="">
        //     ...
        //   </FieldsAndValuesToSet>
        //   <FeeServiceConditionSet>
        //     <Condition />
        //     <Condition />
        //   </FeeServiceConditionSet>
        // </FeeServiceTemplate>
        // NOTE: See classes that implement IFeeServiceCondition for more info on Condition format.
        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            reader.ReadStartElement("FeeServiceTemplate");
            reader.ReadStartElement("FieldsAndValuesToSet");
            while (reader.NodeType == XmlNodeType.Element && reader.Name == "Set")
            {
                var fieldId = reader.GetAttribute("FieldId");
                var value = reader.GetAttribute("Value");
                var lineNumber = int.Parse(reader.GetAttribute("LineNumber"));

                // OPM 198239 - Extend Fee Service to handle CFPB fees.
                // NOTE - These attributes can be null in older docs.
                string typeId = reader.GetAttribute("TypeId");
                string feeProperty = reader.GetAttribute("FeeProperty");

                
                Guid gTypeId = string.IsNullOrEmpty(typeId) ? Guid.Empty : new Guid(typeId);
                E_FeeServiceFeePropertyT eFeeProperty = string.IsNullOrEmpty(feeProperty) ? E_FeeServiceFeePropertyT.None : (E_FeeServiceFeePropertyT) int.Parse(feeProperty);

                // OPM 243540 - Add rules to set title providers. Replace IsNewRule bool
                // with a RuleType enum, to allow better support for different rule types.
                string ruleType = reader.GetAttribute("RuleType");
                FeeServiceRuleType eRuleType;
                if (string.IsNullOrEmpty(ruleType))
                {
                    string isNewRule = reader.GetAttribute("IsNewRule");
                    if(string.IsNullOrEmpty(isNewRule) || !string.Equals(isNewRule, "Y", StringComparison.OrdinalIgnoreCase))
                    {
                        eRuleType = FeeServiceRuleType.Legacy;
                    }
                    else
                    {
                        eRuleType = FeeServiceRuleType.ClosingCost;
                    }
                }
                else
                {
                    eRuleType = (FeeServiceRuleType)int.Parse(ruleType);
                }

                TemplateField tf = new TemplateField(fieldId, value, eRuleType, gTypeId, eFeeProperty, lineNumber);
                TemplateFieldsByKey.Add(tf.Key, tf);
                reader.ReadStartElement();
            }
            reader.ReadEndElement();
            // 10/8/2013 gf - if this was serialized before the addition of the ConditionSet class
            // we will need to parse it as we did before. Otherwise, it can parse itself.
            if (reader.NodeType == XmlNodeType.Element && reader.Name == "Condition")
            {
                while (reader.NodeType == XmlNodeType.Element && reader.Name == "Condition")
                {
                    var conditionType = (E_FeeServiceConditionT)Enum.Parse(typeof(E_FeeServiceConditionT),
                        reader.GetAttribute("ConditionType"));
                    AbstractFeeServiceCondition condition;
                    switch (conditionType)
                    {
                        case E_FeeServiceConditionT.Enumerated:
                            condition = new FeeServiceEnumeratedCondition();
                            condition.ReadXml(reader);
                            break;
                        case E_FeeServiceConditionT.Range:
                            condition = new FeeServiceRangeCondition();
                            condition.ReadXml(reader);
                            break;
                        case E_FeeServiceConditionT.Nested:
                            condition = new FeeServiceNestedCondition();
                            condition.ReadXml(reader);
                            break;
                        default:
                            throw new UnhandledEnumException(conditionType);
                    }
                    Conditions.Add(condition);
                }
            }
            else if (reader.NodeType == XmlNodeType.Element && reader.Name == "FeeServiceConditionSet")
            {
                Conditions.ReadXml(reader);
            }
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("FieldsAndValuesToSet");
            foreach (var key in TemplateFieldsByKey.Keys)
            {
                writer.WriteStartElement("Set");
                writer.WriteAttributeString("FieldId", TemplateFieldsByKey[key].FieldId);
                writer.WriteAttributeString("Value", TemplateFieldsByKey[key].Value);
                writer.WriteAttributeString("LineNumber", TemplateFieldsByKey[key].LineNumber.ToString());

                // OPM 198239 - Extend Fee Service to handle CFPB fees
                writer.WriteAttributeString("RuleType", TemplateFieldsByKey[key].RuleType.ToString("d"));
                writer.WriteAttributeString("TypeId", TemplateFieldsByKey[key].TypeId.ToString());
                writer.WriteAttributeString("FeeProperty", TemplateFieldsByKey[key].FeeProperty.ToString("D"));

                writer.WriteEndElement();
            }
            writer.WriteEndElement();
            writer.WriteStartElement("FeeServiceConditionSet");
            Conditions.WriteXml(writer);
            writer.WriteEndElement();
        }
        #endregion

        public struct TemplateField
        {
            public readonly string FieldId;
            public readonly string Value;
            public readonly int LineNumber;

            // OPM 198239 - Update Fee Service for CFPB
            public readonly FeeServiceRuleType RuleType;
            public readonly Guid TypeId;
            public readonly E_FeeServiceFeePropertyT FeeProperty;

            // OPM 198239 - For new rules, using fieldId is not sufficient for uniqeness.
            public string Key
            {
                get { return RuleType == FeeServiceRuleType.ClosingCost ? TypeId.ToString() + ":" + FeeProperty.ToString() : FieldId; }
            }

            public TemplateField(string fieldId, string value, FeeServiceRuleType ruleType, Guid typeId, E_FeeServiceFeePropertyT feeProperty, int lineNumber)
            {
                FieldId = fieldId;
                Value = value;
                LineNumber = lineNumber;

                RuleType = ruleType;
                TypeId = typeId;
                FeeProperty = feeProperty;
            }
        }
    }
}
