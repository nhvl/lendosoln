using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Collections;
using System.Globalization;
using System.Linq;
using LendersOffice.Common;
using LendersOffice.UI;

namespace DataAccess
{
	
	public class CReCollection : CXmlRecordCollectionNoSpecialType, IReCollection
    {
		private CReCollection( CAppBase appData, string xmlSchema, string xmlContent )
			: base( appData.LoanData , appData, xmlSchema, xmlContent )
		{
		}

        public static IReCollection Create(CAppBase appData, string xmlSchema, string xmlContent)
        {
            return new CReCollection(appData, xmlSchema, xmlContent);
        }

		public ISubcollection GetSubcollection( bool bInclusiveGroup, E_ReoGroupT group )
		{
			return base.GetSubcollectionCore( bInclusiveGroup, group );
		}

		

		protected override ICollectionItemBase2 CreateRegularRawRecord()
		{
			var reField = CReFields.Create( m_appData, m_ds, this );
            reField.IsEmptyCreated = true;
            return reField;
		}
		

		override protected ICollectionItemBase2 Reconstruct( int iRow )
		{
			if( iRow < 0 )
				throw new CBaseException(ErrorMessages.Generic, "Invalid iRow value for xml record.");
			return CReFields.Reconstruct( m_appData, m_ds, this, iRow );
		}

        [LqbInputModelAttribute(invalid: true)]
		public DataView SortedView
		{
			get
			{
				DataTable table = m_ds.Tables[0];

				DataView view = table.DefaultView;
				view.Sort = "OrderRankValue";
				return view;
			}
		}

		override protected E_ExistingRecordFaith DetermineRawRecordDestiny(ICollectionItemBase2 record )
		{
			return E_ExistingRecordFaith.E_RegularOK;
		}
		
		/// <summary>
		/// Update CAppData with the new values
		/// </summary>
        override public void Flush()
        {
            ExecuteDeathRowRecords();

            var subcoll = this.GetSubcollection(true, E_ReoGroupT.All);
            foreach (var item in subcoll)
            {
                var fields = (IRealEstateOwned)item;
                fields.PrepareToFlush();
            }

            var aggData = LendersOffice.CalculatedFields.RealProperty.CalculateAggregateData(subcoll.Cast<IRealEstateOwned>(), m_appData.aOccT == E_aOccT.PrimaryResidence);

            m_appData.aReTotVal_ = (decimal)aggData.ReTotVal;
            m_appData.aReTotMAmt_ = (decimal)aggData.ReTotMAmt;
            m_appData.aReTotGrossRentI_ = (decimal)aggData.ReTotGrossRentI;
            m_appData.aReTotMPmt_ = (decimal)aggData.ReTotMPmt;
            m_appData.aReTotHExp_ = (decimal)aggData.ReTotHExp;
            m_appData.aReTotNetRentI_ = (decimal)aggData.ReNetRentI;
            m_appData.aBRetainedNegCf_ = (decimal)aggData.ReBRetainedRentI < 0 ? -(decimal)aggData.ReBRetainedRentI : 0;
            m_appData.aCRetainedNegCf_ = (decimal)aggData.ReCRetainedRentI < 0 ? -(decimal)aggData.ReCRetainedRentI : 0;

            if ((decimal)aggData.ReBNetRentI + (decimal)aggData.ReCNetRentI > 0)
            {
                m_appData.aBNetRentI_ = (decimal)aggData.ReBNetRentI;
                m_appData.aCNetRentI_ = (decimal)aggData.ReCNetRentI;
                m_appData.aBNetNegCf_ = 0;
                m_appData.aCNetNegCf_ = 0;
            }
            else
            {
                m_appData.aBNetRentI_ = 0;
                m_appData.aCNetRentI_ = 0;
                m_appData.aBNetNegCf_ = -(decimal)aggData.ReBNetRentI;
                m_appData.aCNetNegCf_ = -(decimal)aggData.ReCNetRentI;
            }

            m_appData.aReXmlContent = m_ds.GetXml();
            base.Flush();
        }

		/// <summary>
		/// Determine if this application requires extra 1003 page 4 for REO
		/// Return true if has more than 3 REO
		/// </summary>
		public bool HasExtraREO
		{
			get { return CountRegular > 3; }
		}

		new public IRealEstateOwned GetRegularRecordAt( int pos )
		{
			return (IRealEstateOwned) base.GetRegularRecordAt( pos );

		}
		//[Obsolete("This might cause problem and create code complexity, must add new record and save it when users click *add*", false )]
		new public IRealEstateOwned EnsureRegularRecordOf( Guid recordId )
		{
			return (IRealEstateOwned) base.EnsureRegularRecordOf( recordId );
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="recordId"></param>
		/// <returns></returns>
		new public IRealEstateOwned GetRegRecordOf( Guid recordId )
		{
			return (IRealEstateOwned) base.GetRegRecordOf( recordId );
		}

		/// <summary>
		/// Do not use this, use foreach on GetSubcollection() instead.
		/// </summary>
		/// <param name="i"></param>
		/// <returns></returns>
		new public IRealEstateOwned GetSpecialRecordAt( int i )
		{
			return (IRealEstateOwned) base.GetSpecialRecordAt( i );
		}

		new public IRealEstateOwned AddRegularRecord()
		{
			return (IRealEstateOwned) base.AddRegularRecord();
		}

		
		/// <summary>
		/// Add new record, let collection object figure out special or regular
		/// and add the right one accordingly.
		/// </summary>
		/// <returns></returns>
		public IRealEstateOwned AddRecord( E_ReoStatusT reT )
		{		
			return (IRealEstateOwned) base.AddRecord( reT );
		}
		

		new public IRealEstateOwned AddRegularRecordAt( int pos )
		{			
			return (IRealEstateOwned) base.AddRegularRecordAt( pos );
		}

        /// <summary>
        /// Return a template. This will be used in input model reflection only.
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public IRealEstateOwned RegularTemplate
        {
            get
            {
                return (IRealEstateOwned)this.CreateRegularRawRecord();
            }
        }
        /// <summary>
        /// Return a Reo list. This will be used in input model reflection only.
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public List<object> Regulars
        {
            get
            {
                return this.m_sortedList.GetValueList().Cast<object>().ToList();
            }
            set
            {
                if (null == value)
                {
                    return;
                }
                else
                {
                    var regulars = m_sortedList.GetValueList().Cast<IRealEstateOwned>().ToList();
                    regulars.ForEach(x => x.IsOnDeathRow = true);
                    int index = 0;
                    value.Cast<ICollectionItemBase2>().ToList().ForEach(x => this.InsertRegularRecordTo(index++, x));
                    this.Flush();
                }
            }
        }

        /// <summary>
        /// Return a Rental Properties. This will be used in input model reflection only.
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public IReoProperty RentalProperties
        {
            get { return new ReoProperty(true, Regulars); }
        }

        /// <summary>
        /// Return a Retained Properties. This will be used in input model reflection only.
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public IReoProperty RetainedProperties
        {
            get { return new ReoProperty(false, Regulars); }
        }

        /// <summary>
        /// The class contains items for Rental Properties and Retained Properties. This will be used in input model reflection only.
        /// </summary>
        public class ReoProperty : IReoProperty
        {
            public decimal GrossRentI
            {
                get { return grossRent; }
            }
            public decimal MPmt
            {
                get { return mPmt; }
            }
            public decimal HExp
            {
                get { return hExp; }
            }
            public decimal NetRentI
            {
                get { return netRentI; }
            }
            private decimal grossRent;
            private decimal mPmt;
            private decimal hExp;
            private decimal netRentI;
            public ReoProperty(bool isRental, List<object> Regulars)
            {
                grossRent = 0;
                mPmt = 0;
                hExp = 0;
                netRentI = 0;
                foreach (var item in Regulars)
                {
                    var r = (IRealEstateOwned)item;
                    if (isRental)
                    {
                        if (r.StatT == E_ReoStatusT.Rental)
                        {
                            grossRent += r.GrossRentI;
                            mPmt += r.MPmt;
                            hExp += r.HExp;
                            netRentI += r.NetRentI;
                        }
                    }
                    else
                    {
                        if (r.StatT == E_ReoStatusT.Residence)
                        {
                            grossRent += r.GrossRentI;
                            mPmt += r.MPmt;
                            hExp += r.HExp;
                            netRentI += r.NetRentI;
                        }
                    }
                }
            }
        }
	}
}