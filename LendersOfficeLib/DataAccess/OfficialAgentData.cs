using System;

namespace DataAccess
{
    /// <summary>
    /// 3/16/2005 kb - We need to access a loan's people data before the
    /// loan is finalized, during initial creation.  As a general rule,
    /// all creation access should be outside the access control
    /// boundaries.
    /// </summary>

    public class COfficialAgentData : CPeopleData
    {
        /// <summary>
        /// 3/16/2005 kb - We need to access a loan's people data
        /// before the loan is finalized, during initial creation.
        /// As a general rule, all creation access should be outside
        /// the access control boundaries.
        /// </summary>

        protected override bool m_enforceAccessControl
        {
            get { return false; }
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public COfficialAgentData( Guid loanId ) : base( loanId )
        {
        }

    }
}
