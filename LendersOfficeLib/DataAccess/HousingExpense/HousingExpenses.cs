// <summary>
//    Author: Eric Mallare
//    Date:   12/22/2014 
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using LendersOffice.Common;
    using LendersOffice.Migration;

    /// <summary>
    /// Class that allows interactions with Housing Expenses.
    /// </summary>
    public class HousingExpenses
    {
        /// <summary>
        /// Mapping between an expense type and the equivalent field.
        /// </summary>
        public static readonly Dictionary<E_HousingExpenseTypeCalculatedT, string> ExpenseToFieldMap = new Dictionary<E_HousingExpenseTypeCalculatedT, string>()
        {
            { E_HousingExpenseTypeCalculatedT.CondoHO6Insurance, "sCondoHO6Expense" },
            { E_HousingExpenseTypeCalculatedT.FloodInsurance, "sFloodExpense" },
            { E_HousingExpenseTypeCalculatedT.GroundRent, "sGroundRentExpense" },
            { E_HousingExpenseTypeCalculatedT.HazardInsurance, "sHazardExpense" },
            { E_HousingExpenseTypeCalculatedT.HomeownersAsscDues, "sHOADuesExpense" },
            { E_HousingExpenseTypeCalculatedT.Line1008Exp, "sLine1008Expense" },
            { E_HousingExpenseTypeCalculatedT.Line1009Exp, "sLine1009Expense" },
            { E_HousingExpenseTypeCalculatedT.Line1010Exp, "sLine1010Expense" },
            { E_HousingExpenseTypeCalculatedT.Line1011Exp, "sLine1011Expense" },
            { E_HousingExpenseTypeCalculatedT.OtherTaxes1, "sOtherTax1Expense" },
            { E_HousingExpenseTypeCalculatedT.OtherTaxes2, "sOtherTax2Expense" },
            { E_HousingExpenseTypeCalculatedT.OtherTaxes3, "sOtherTax3Expense" },
            { E_HousingExpenseTypeCalculatedT.OtherTaxes4, "sOtherTax4Expense" },
            { E_HousingExpenseTypeCalculatedT.RealEstateTaxes, "sRealEstateTaxExpense" },
            { E_HousingExpenseTypeCalculatedT.SchoolTaxes, "sSchoolTaxExpense" },
            { E_HousingExpenseTypeCalculatedT.WindstormInsurance, "sWindstormExpense" }
        };

        /// <summary>
        /// The expenses that weren't delegated to a line number in the legacy system.
        /// </summary>
        public static readonly E_HousingExpenseTypeT[] NonCustomExpenses = 
        {
            E_HousingExpenseTypeT.FloodInsurance,
            E_HousingExpenseTypeT.GroundRent,
            E_HousingExpenseTypeT.HazardInsurance,
            E_HousingExpenseTypeT.HomeownersAsscDues,
            E_HousingExpenseTypeT.SchoolTaxes,
            E_HousingExpenseTypeT.RealEstateTaxes
        };

        /// <summary>
        /// The Housing Expense Struct that holds all expenses.
        /// </summary>
        private HousingExpContainer expensesContainer;

        /// <summary>
        /// The associated loan for all these expenses.
        /// </summary>
        private CPageBase dataLoan;

        /// <summary>
        /// Initializes a new instance of the <see cref="HousingExpenses" /> class.
        /// For every expense in the JSON, it will link the loan data to that expense.
        /// For every Actual Disbursement in the JSON, it will link it to the parent expense.
        /// </summary>
        /// <param name="json">The json to deserialize.</param>
        /// <param name="dataLoan">The loan file to associate with.</param>
        public HousingExpenses(string json, CPageBase dataLoan)
        {
            this.dataLoan = dataLoan;

            if (string.IsNullOrEmpty(json))
            {
                this.expensesContainer = new HousingExpContainer();
            }
            else
            {
                this.expensesContainer = ObsoleteSerializationHelper.JsonDeserialize<HousingExpContainer>(json);
                this.expensesContainer.DataLoan = this.dataLoan;

                foreach (BaseHousingExpense assignedExp in this.expensesContainer.AssignedExpenses.Values)
                {
                    assignedExp.DataLoan = this.dataLoan;

                    foreach (SingleDisbursement assignedDisb in assignedExp.ActualDisbursements.Values)
                    {
                        assignedDisb.ParentExpense = assignedExp;
                    }
                }

                foreach (BaseHousingExpense unassignedExp in this.expensesContainer.UnassignedCustomExpenses.Values)
                {
                    unassignedExp.DataLoan = this.dataLoan;

                    foreach (SingleDisbursement unassignedDisb in unassignedExp.ActualDisbursements.Values)
                    {
                        unassignedDisb.ParentExpense = unassignedExp;
                    }
                }
            }
        }

        /// <summary>
        /// Gets a collection of expenses that have been assigned a type.
        /// </summary>
        /// <value>The collection of assigned expenses.</value>
        public ICollection<BaseHousingExpense> AssignedExpenses
        {
            get
            {
                return this.ExpContainer.AssignedExpenses.Values;
            }
        }

        /// <summary>
        /// Gets a collection of expenses that have not been assigned a type.
        /// </summary>
        /// <value>The collection of unassigned expenses.</value>
        public ICollection<BaseHousingExpense> UnassignedExpenses
        {
            get
            {
                return this.ExpContainer.UnassignedCustomExpenses.Values;
            }
        }

        /// <summary>
        /// Gets all the housing expenses that should be used. 
        /// </summary>
        /// <value>A list of expenses that should be used.</value>
        public List<BaseHousingExpense> ExpensesToUse
        {
            get
            {
                List<BaseHousingExpense> list = new List<BaseHousingExpense>();
                List<E_CustomExpenseLineNumberT> remainingLines = new List<E_CustomExpenseLineNumberT>
                { 
                    E_CustomExpenseLineNumberT.Line1008, 
                    E_CustomExpenseLineNumberT.Line1009, 
                    E_CustomExpenseLineNumberT.Line1010,
                    E_CustomExpenseLineNumberT.Line1011
                };

                foreach (BaseHousingExpense exp in this.AssignedExpenses)
                {
                    list.Add(exp);
                    if (remainingLines.Contains(exp.CustomExpenseLineNum))
                    {
                        remainingLines.Remove(exp.CustomExpenseLineNum);
                    }
                }

                foreach (E_CustomExpenseLineNumberT remaining in remainingLines)
                {
                    BaseHousingExpense exp = this.GetUnassignedExp(remaining);
                    if (exp != null)
                    {
                        if (this.dataLoan != null &&
                            LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.dataLoan.sLoanVersionT, LoanVersionT.V10_HideOtherHousingExpenses_Opm235669) &&
                            exp.AnnualAmt == 0)
                        {
                            // This Other Housing expense has a $0 annual amount. There's no need to use it.
                            continue;
                        }

                        list.Add(this.GetUnassignedExp(remaining));
                    }
                }

                return list;
            }
        }

        /// <summary>
        /// Gets the custom expenses in use.
        /// </summary>
        /// <value>The custom expenses in use.</value>
        public List<BaseHousingExpense> CustomExpensesInUse
        {
            get
            {
                List<BaseHousingExpense> list = this.ExpensesToUse;
                return list.FindAll(o => !NonCustomExpenses.Contains(o.HousingExpenseType));
            }
        }

        /// <summary>
        /// Gets or sets the mortgage insurance escrow schedule.
        /// </summary>
        /// <value>The mortgage insurance escrow schedule.</value>
        public int[] MIEscrowSchedule
        {
            get
            {
                return this.ExpContainer.MIEscrowSchedule;
            }

            set
            {
                this.ExpContainer.MIEscrowSchedule = value;
            }
        }

        /// <summary>
        /// Gets or sets the mortgage insurance payment repetition interval.
        /// </summary>
        /// <value>The MI payment repetition interval.</value>
        public E_DisbursementRepIntervalT MIPaymentRepeat
        {
            get
            {
                return this.ExpContainer.MIPaymentRepeat;
            }

            set
            {
                this.expensesContainer.MIPaymentRepeat = value;
            }
        }

        /// <summary>
        /// Gets the housing expense struct. Initializes it if it is null.
        /// </summary>
        /// <value>The housing expense struct.</value>
        private HousingExpContainer ExpContainer
        {
            get
            {
                if (this.expensesContainer == null)
                {
                    this.expensesContainer = new HousingExpContainer();
                }

                return this.expensesContainer;
            }
        }

        /// <summary>
        /// Gets the default description for an expense type.
        /// </summary>
        /// <param name="type">The type of expense.</param>
        /// <param name="lineNum">The line number of the expense.</param>
        /// <returns>The default description.</returns>
        public static string GetDefaultExpenseDescription(E_HousingExpenseTypeT type, E_CustomExpenseLineNumberT lineNum)
        {
            switch (type)
            {
                case E_HousingExpenseTypeT.HazardInsurance:
                    return "Hazard Insurance";
                case E_HousingExpenseTypeT.FloodInsurance:
                    return "Flood Insurance";
                case E_HousingExpenseTypeT.RealEstateTaxes:
                    return "Property Taxes";
                case E_HousingExpenseTypeT.SchoolTaxes:
                    return "School Taxes";
                case E_HousingExpenseTypeT.CondoHO6Insurance:
                    return "Condo HO-6 Insurance";
                case E_HousingExpenseTypeT.GroundRent:
                    return "Ground Rent";
                case E_HousingExpenseTypeT.HomeownersAsscDues:
                    return "Homeowner's Association Dues";
                case E_HousingExpenseTypeT.OtherTaxes1:
                    return "Other Tax Expense 1";
                case E_HousingExpenseTypeT.OtherTaxes2:
                    return "Other Tax Expense 2";
                case E_HousingExpenseTypeT.OtherTaxes3:
                    return "Other Tax Expense 3";
                case E_HousingExpenseTypeT.OtherTaxes4:
                    return "Other Tax Expense 4";
                case E_HousingExpenseTypeT.WindstormInsurance:
                    return "Windstorm Insurance";
                case E_HousingExpenseTypeT.Unassigned:
                    if (lineNum == E_CustomExpenseLineNumberT.Line1008)
                    {
                        return "Line 1008 Housing Expense";
                    }
                    else if (lineNum == E_CustomExpenseLineNumberT.Line1009)
                    {
                        return "Line 1009 Housing Expense";
                    }
                    else if (lineNum == E_CustomExpenseLineNumberT.Line1010)
                    {
                        return "Line 1010 Housing Expense";
                    }
                    else if (lineNum == E_CustomExpenseLineNumberT.Line1011)
                    {
                        return "Line 1011 Housing Expense";
                    }
                    else
                    {
                        return string.Empty;
                    }

                default:
                    throw new UnhandledEnumException(type);
            }
        }

        /// <summary>
        /// Calculates the housing expense type based on the passed in expensed type and line number. This is so that the weird Line10XX expenses get a specific type.
        /// </summary>
        /// <param name="expType">The expense's Housing Expense Type.</param>
        /// <param name="lineNum">The expense's Line Number. Only gets factored in for Unassigned Expenses.</param>
        /// <returns>The calculated type.</returns>
        public static E_HousingExpenseTypeCalculatedT GetCalculatedExpenseType(E_HousingExpenseTypeT expType, E_CustomExpenseLineNumberT lineNum)
        {
            switch (expType)
            {
                case E_HousingExpenseTypeT.CondoHO6Insurance:
                    return E_HousingExpenseTypeCalculatedT.CondoHO6Insurance;
                case E_HousingExpenseTypeT.FloodInsurance:
                    return E_HousingExpenseTypeCalculatedT.FloodInsurance;
                case E_HousingExpenseTypeT.GroundRent:
                    return E_HousingExpenseTypeCalculatedT.GroundRent;
                case E_HousingExpenseTypeT.HazardInsurance:
                    return E_HousingExpenseTypeCalculatedT.HazardInsurance;
                case E_HousingExpenseTypeT.HomeownersAsscDues:
                    return E_HousingExpenseTypeCalculatedT.HomeownersAsscDues;
                case E_HousingExpenseTypeT.OtherTaxes1:
                    return E_HousingExpenseTypeCalculatedT.OtherTaxes1;
                case E_HousingExpenseTypeT.OtherTaxes2:
                    return E_HousingExpenseTypeCalculatedT.OtherTaxes2;
                case E_HousingExpenseTypeT.OtherTaxes3:
                    return E_HousingExpenseTypeCalculatedT.OtherTaxes3;
                case E_HousingExpenseTypeT.OtherTaxes4:
                    return E_HousingExpenseTypeCalculatedT.OtherTaxes4;
                case E_HousingExpenseTypeT.RealEstateTaxes:
                    return E_HousingExpenseTypeCalculatedT.RealEstateTaxes;
                case E_HousingExpenseTypeT.SchoolTaxes:
                    return E_HousingExpenseTypeCalculatedT.SchoolTaxes;
                case E_HousingExpenseTypeT.WindstormInsurance:
                    return E_HousingExpenseTypeCalculatedT.WindstormInsurance;
                case E_HousingExpenseTypeT.Unassigned:
                    {
                        if (lineNum == E_CustomExpenseLineNumberT.Line1008)
                        {
                            return E_HousingExpenseTypeCalculatedT.Line1008Exp;
                        }
                        else if (lineNum == E_CustomExpenseLineNumberT.Line1009)
                        {
                            return E_HousingExpenseTypeCalculatedT.Line1009Exp;
                        }
                        else if (lineNum == E_CustomExpenseLineNumberT.Line1010)
                        {
                            return E_HousingExpenseTypeCalculatedT.Line1010Exp;
                        }
                        else if (lineNum == E_CustomExpenseLineNumberT.Line1011)
                        {
                            return E_HousingExpenseTypeCalculatedT.Line1011Exp;
                        }
                        else
                        {
                            throw new UnhandledEnumException(lineNum, "This case is not acceptable for Unassigned Housing Expenses");
                        }
                    }

                default:
                    throw new UnhandledEnumException(expType);
            }
        }

        /// <summary>
        /// Serializes the Housing Expense to JSON.
        /// </summary>
        /// <returns>The json string to save.</returns>
        public string ToJson()
        {
            return ObsoleteSerializationHelper.JsonSerialize(this.ExpContainer);
        }

        /// <summary>
        /// Check if the expense type exists. Only checks assigned expenses.
        /// </summary>
        /// <param name="type">The type to check for.</param>
        /// <returns>True if it exists, false if not.</returns>
        public bool DoesExpenseExist(E_HousingExpenseTypeT type)
        {
            return this.ExpContainer.AssignedExpenses.ContainsKey(type);
        }

        /// <summary>
        /// Checks if the expense exists for a specified line number. Checks assigned, then unassigned.
        /// </summary>
        /// <param name="lineNum">The line number to look at.</param>
        /// <returns>True if it exists, false if it doesn't.</returns>
        public bool DoesExpenseExistLineNum(E_CustomExpenseLineNumberT lineNum)
        {
            if (this.GetExpenseByLineNum(lineNum) == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Add an expense if it doesn't exist. Replaces an existing one if it does.
        /// This will also link the expense with this objects associated loan.
        /// </summary>
        /// <param name="expense">The expense to add.</param>
        public void AddOrUpdateExpense(BaseHousingExpense expense)
        {
            expense.DataLoan = this.dataLoan;

            if (expense.HousingExpenseType == E_HousingExpenseTypeT.Unassigned)
            {
                this.ExpContainer.UnassignedCustomExpenses[expense.CustomExpenseLineNum] = expense;
            }
            else
            {
                this.ExpContainer.AssignedExpenses[expense.HousingExpenseType] = expense;
            }
        }

        /// <summary>
        /// Remove the expense of the specified type. Will only remove Assigned expenses.
        /// </summary>
        /// <param name="type">The type of expense to remove.</param>
        /// <returns>True if the expense was removed, false if the expense doesn't exist.</returns>
        public bool RemoveExpense(E_HousingExpenseTypeT type)
        {
            if (this.DoesExpenseExist(type))
            {
                this.ExpContainer.AssignedExpenses.Remove(type);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the expense of a certain type. Only checks assigned expenses.
        /// </summary>
        /// <param name="type">The type of expense to get.</param>
        /// <returns>The expense if it exists, null if it doesn't.</returns>
        public BaseHousingExpense GetExpense(E_HousingExpenseTypeT type)
        {
            if (this.DoesExpenseExist(type))
            {
                return this.ExpContainer.AssignedExpenses[type];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Return the expense of the specified line number. Checks assigned then unassigned.
        /// </summary>
        /// <param name="lineNum">The line number to get.</param>
        /// <returns>The expense if it exists, null it it doesn't.</returns>
        public BaseHousingExpense GetExpenseByLineNum(E_CustomExpenseLineNumberT lineNum)
        {
            foreach (BaseHousingExpense assignedExp in this.ExpContainer.AssignedExpenses.Values)
            {
                if (assignedExp.CustomExpenseLineNum == lineNum)
                {
                    return assignedExp;
                }
            }

            if (this.ExpContainer.UnassignedCustomExpenses.ContainsKey(lineNum))
            {
                return this.ExpContainer.UnassignedCustomExpenses[lineNum];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the unassigned expense with the line number.
        /// </summary>
        /// <param name="lineNum">The line number of the expense.</param>
        /// <returns>The expense or null if not found.</returns>
        public BaseHousingExpense GetUnassignedExp(E_CustomExpenseLineNumberT lineNum)
        {
            if (this.ExpContainer.UnassignedCustomExpenses.ContainsKey(lineNum))
            {
                return this.ExpContainer.UnassignedCustomExpenses[lineNum];
            }

            return null;
        }

        /// <summary>
        /// Gets the expense associated with a prepaid fee type from the 900 section.
        /// </summary>
        /// <param name="feeTypeId">The fee's closing cost fee type id.</param>
        /// <returns>Null if the expense is not found or if the fee is not a prepaid. The expense otherwise.</returns>
        public BaseHousingExpense GetExpenseFrom900Or1000FeeTypeId(Guid feeTypeId)
        {
            if (feeTypeId == DefaultSystemClosingCostFee.Hud900CondoInsuranceFeeTypeId ||
                feeTypeId == DefaultSystemClosingCostFee.Hud1000CondoHo6InsuranceFeeTypeId)
            {
                return this.GetExpense(E_HousingExpenseTypeT.CondoHO6Insurance);
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud900WindstormInsuranceFeeTypeId ||
                feeTypeId == DefaultSystemClosingCostFee.Hud1000WindStormInsuranceFeeTypeId)
            {
                return this.GetExpense(E_HousingExpenseTypeT.WindstormInsurance);
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud900HazardInsuranceFeeTypeId ||
                feeTypeId == DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId)
            {
                return this.GetExpense(E_HousingExpenseTypeT.HazardInsurance);
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud900FloodInsuranceFeeTypeId ||
                feeTypeId == DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId)
            {
                return this.GetExpense(E_HousingExpenseTypeT.FloodInsurance);
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud900PropertyTaxFeeTypeId ||
                feeTypeId == DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId)
            {
                return this.GetExpense(E_HousingExpenseTypeT.RealEstateTaxes);
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud900SchoolTaxFeeTypeId ||
                feeTypeId == DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId)
            {
                return this.GetExpense(E_HousingExpenseTypeT.SchoolTaxes);
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud900OtherTax1FeeTypeId ||
                feeTypeId == DefaultSystemClosingCostFee.Hud1000OtherTax1FeeTypeId)
            {
                return this.GetExpense(E_HousingExpenseTypeT.OtherTaxes1);
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud900OtherTax2FeeTypeId ||
                feeTypeId == DefaultSystemClosingCostFee.Hud1000OtherTax2FeeTypeId)
            {
                return this.GetExpense(E_HousingExpenseTypeT.OtherTaxes2);
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud900OtherTax3FeeTypeId ||
                feeTypeId == DefaultSystemClosingCostFee.Hud1000OtherTax3FeeTypeId)
            {
                return this.GetExpense(E_HousingExpenseTypeT.OtherTaxes3);
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud900OtherTax4FeeTypeId ||
                feeTypeId == DefaultSystemClosingCostFee.Hud1000OtherTax4FeeTypeId)
            {
                return this.GetExpense(E_HousingExpenseTypeT.OtherTaxes4);
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud900HOADuesFeeTypeId ||
                feeTypeId == DefaultSystemClosingCostFee.Hud1000HomeOwnerAssociationDuesFeeTypeId)
            {
                return this.GetExpense(E_HousingExpenseTypeT.HomeownersAsscDues);
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud900GroundRentFeeTypeId ||
                feeTypeId == DefaultSystemClosingCostFee.Hud1000GroundRentFeeTypeId)
            {
                return this.GetExpense(E_HousingExpenseTypeT.GroundRent);
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud900OtherExp1FeeTypeId ||
                feeTypeId == DefaultSystemClosingCostFee.Hud1000Custom1FeeTypeId)
            {
                return this.GetUnassignedExp(E_CustomExpenseLineNumberT.Line1008);
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud900OtherExp2FeeTypeId ||
                feeTypeId == DefaultSystemClosingCostFee.Hud1000Custom2FeeTypeId)
            {
                return this.GetUnassignedExp(E_CustomExpenseLineNumberT.Line1009);
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud900OtherExp3FeeTypeId ||
                feeTypeId == DefaultSystemClosingCostFee.Hud1000Custom3FeeTypeId)
            {
                return this.GetUnassignedExp(E_CustomExpenseLineNumberT.Line1010);
            }
            else if (feeTypeId == DefaultSystemClosingCostFee.Hud900OtherExp4FeeTypeId ||
                feeTypeId == DefaultSystemClosingCostFee.Hud1000Custom4FeeTypeId)
            {
                return this.GetUnassignedExp(E_CustomExpenseLineNumberT.Line1011);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        ///  Checks if any expenses are set to 'Any' custom line num.
        /// </summary>
        /// <returns>True if yes, false if not.</returns>
        public bool IsAnyExpenseSetToAny()
        {
            return this.AssignedExpenses.Any(o => o.CustomExpenseLineNum == E_CustomExpenseLineNumberT.Any);
        }

        /// <summary>
        /// Remove all the Housing Expenses.
        /// </summary>
        internal void ClearExpenses()
        {
            this.ExpContainer.ClearExpenses();
        }
    }
}
