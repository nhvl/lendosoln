﻿// <copyright file="HousingExpensesMigration.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   3/24/2015
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using LendersOffice.Constants;
    using LendersOffice.Security;

    /// <summary>
    /// Migration for new Housing Expenses.
    /// </summary>
    public class HousingExpensesMigration
    {
        /// <summary>
        /// Migrate new housing expenses in-memory.  We do not want to save this instance.
        /// </summary>
        /// <param name="dataLoan">The loan instance.</param>
        public static void MigrateExpenses(CPageData dataLoan)
        {
            MigrateExpenses(dataLoan, false);
        }

        /// <summary>
        /// Migrate new housing expenses.  Loan will be saved.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        public static void MigrateExpenses(Guid loanId)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(HousingExpensesMigration));
            dataLoan.ByPassFieldSecurityCheck = true;
            dataLoan.AllowSaveWhileQP2Sandboxed = true;
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            MigrateExpenses(dataLoan, true);
        }

        /// <summary>
        /// Migrate new housing expenses.
        /// </summary>
        /// <param name="dataLoan">The loan itself.</param>
        /// <param name="shouldSave">Should we save or not.</param>
        private static void MigrateExpenses(CPageData dataLoan, bool shouldSave)
        {
            var principal = PrincipalFactory.CurrentPrincipal;

            if (principal.UserId != new Guid("11111111-1111-1111-1111-111111111111") && principal.LoginNm != "tasksystem")
            {
                if (dataLoan.sBrokerId != principal.BrokerId)
                {
                    throw new AccessDenied();
                }
            }

            if (dataLoan.sIsHousingExpenseMigrated)
            {
                dataLoan.sHousingExpenses.ClearExpenses();
            }

            // Ensure values are grabbed from DB
            dataLoan.sIsHousingExpenseMigrated = false;

            // Populate MI schedule
            for (int i = 0; i < 13; i++)
            {
                dataLoan.sHousingExpenses.MIEscrowSchedule[i] = dataLoan.sInitialEscrowAcc[i, 2];
            }

            // Ground Rent 
            GroundRentExpense groundExp = dataLoan.sGroundRentExpense as GroundRentExpense;
            groundExp.FHAMonthlyAmtFixedAmt = dataLoan.sFHAProGroundRent;
            groundExp.VAMonthlyAmtFixedAmt = dataLoan.sSpLeaseAnnualGroundRent;
            groundExp.DisbursementRepInterval = E_DisbursementRepIntervalT.Monthly;
            groundExp.AnnualAmtCalcType = E_AnnualAmtCalcTypeT.LoanValues;

            // HOA
            BaseHousingExpense hoaExp = dataLoan.sHOADuesExpense;
            hoaExp.DisbursementRepInterval = E_DisbursementRepIntervalT.Monthly;
            hoaExp.MonthlyAmtFixedAmt = dataLoan.sProHoAssocDues;
            hoaExp.AnnualAmtCalcType = E_AnnualAmtCalcTypeT.LoanValues;

            // School
            BaseHousingExpense schoolExp = dataLoan.sSchoolTaxExpense;
            schoolExp.TaxType = dataLoan.sTaxTableSchoolT;
            schoolExp.MonthlyAmtFixedAmt = dataLoan.sProSchoolTx;
            schoolExp.IsEscrowedAtClosing = dataLoan.sSchoolTxRsrvEscrowedTri;
            schoolExp.ReserveMonths = dataLoan.sSchoolTxRsrvMon;
            schoolExp.ReserveMonthsLckd = dataLoan.sSchoolTxRsrvMonLckd;
            schoolExp.DisbursementRepInterval = E_DisbursementRepIntervalT.Annual;
            schoolExp.AnnualAmtCalcType = E_AnnualAmtCalcTypeT.LoanValues;
            for (int i = 0; i < 13; i++)
            {
                schoolExp.DisbursementScheduleMonths[i] = dataLoan.sInitialEscrowAcc[i, 4];
            }

            // Real Estate
            BaseHousingExpense realExp = dataLoan.sRealEstateTaxExpense;
            realExp.TaxType = dataLoan.sTaxTableRealETxT;
            realExp.AnnualAmtCalcBaseType = dataLoan.sProRealETxT;
            realExp.AnnualAmtCalcBasePerc = dataLoan.sProRealETxR;
            realExp.MonthlyAmtFixedAmt = dataLoan.sProRealETxMb;
            realExp.IsEscrowedAtClosing = dataLoan.sRealETxRsrvEscrowedTri;
            realExp.ReserveMonths = dataLoan.sRealETxRsrvMon;
            realExp.ReserveMonthsLckd = dataLoan.sRealETxRsrvMonLckd;
            realExp.DisbursementRepInterval = E_DisbursementRepIntervalT.Annual;
            realExp.AnnualAmtCalcType = E_AnnualAmtCalcTypeT.LoanValues;
            for (int i = 0; i < 13; i++)
            {
                realExp.DisbursementScheduleMonths[i] = dataLoan.sInitialEscrowAcc[i, 0];
            }

            // Flood
            BaseHousingExpense floodExp = dataLoan.sFloodExpense;
            floodExp.MonthlyAmtFixedAmt = dataLoan.sProFloodIns;
            floodExp.IsEscrowedAtClosing = dataLoan.sFloodInsRsrvEscrowedTri;
            floodExp.ReserveMonths = dataLoan.sFloodInsRsrvMon;
            floodExp.ReserveMonthsLckd = dataLoan.sFloodInsRsrvMonLckd;
            floodExp.PolicyActivationD_rep = dataLoan.sFloodInsPolicyActivationD_rep;
            floodExp.AnnualAmtCalcType = E_AnnualAmtCalcTypeT.LoanValues;
            floodExp.DisbursementRepInterval = E_DisbursementRepIntervalT.Annual;
            for (int i = 0; i < 13; i++)
            {
                floodExp.DisbursementScheduleMonths[i] = dataLoan.sInitialEscrowAcc[i, 3];
            }

            // Hazard
            BaseHousingExpense hazardExp = dataLoan.sHazardExpense;
            hazardExp.AnnualAmtCalcBaseType = dataLoan.sProHazInsT;
            hazardExp.AnnualAmtCalcBasePerc = dataLoan.sProHazInsR;
            hazardExp.MonthlyAmtFixedAmt = dataLoan.sProHazInsMb;
            hazardExp.PrepaidMonths = dataLoan.sHazInsPiaMon;
            if (dataLoan.sHazInsPiaMon > 0)
            {
                hazardExp.IsPrepaid = true;
            }

            hazardExp.IsEscrowedAtClosing = dataLoan.sHazInsRsrvEscrowedTri;
            hazardExp.ReserveMonths = dataLoan.sHazInsRsrvMon;
            hazardExp.ReserveMonthsLckd = dataLoan.sHazInsRsrvMonLckd;
            hazardExp.PolicyActivationD_rep = dataLoan.sHazInsPolicyActivationD_rep;
            hazardExp.AnnualAmtCalcType = E_AnnualAmtCalcTypeT.LoanValues;
            hazardExp.DisbursementRepInterval = E_DisbursementRepIntervalT.Annual;
            for (int i = 0; i < 13; i++)
            {
                hazardExp.DisbursementScheduleMonths[i] = dataLoan.sInitialEscrowAcc[i, 1];
            }

            HashSet<E_CustomExpenseLineNumberT> remainingLineNums = new HashSet<E_CustomExpenseLineNumberT>() 
                                                                    { 
                                                                        E_CustomExpenseLineNumberT.Line1008,
                                                                        E_CustomExpenseLineNumberT.Line1009,
                                                                        E_CustomExpenseLineNumberT.Line1010,
                                                                        E_CustomExpenseLineNumberT.Line1011
                                                                    };

            // Wind
            CustomExpense windExp = dataLoan.sWindstormExpense as CustomExpense;
            windExp.PolicyActivationD_rep = dataLoan.sWindInsPolicyActivationD_rep;
            if (dataLoan.sWindInsSettlementChargeT != E_sInsSettlementChargeT.None)
            {
                switch (dataLoan.sWindInsSettlementChargeT)
                {
                    case E_sInsSettlementChargeT.Line1008:
                        if (remainingLineNums.Contains(E_CustomExpenseLineNumberT.Line1008))
                        {
                            Set1008Expense(windExp, dataLoan);
                            remainingLineNums.Remove(E_CustomExpenseLineNumberT.Line1008);
                        }

                        break;
                    case E_sInsSettlementChargeT.Line1009:
                        if (remainingLineNums.Contains(E_CustomExpenseLineNumberT.Line1009))
                        {
                            Set1009Expense(windExp, dataLoan);
                            remainingLineNums.Remove(E_CustomExpenseLineNumberT.Line1009);
                        }

                        break;
                    case E_sInsSettlementChargeT.Line1010:
                        if (remainingLineNums.Contains(E_CustomExpenseLineNumberT.Line1010))
                        {
                            Set1010Expense(windExp, dataLoan);
                            remainingLineNums.Remove(E_CustomExpenseLineNumberT.Line1010);
                        }

                        break;
                    case E_sInsSettlementChargeT.Line1011:
                        if (remainingLineNums.Contains(E_CustomExpenseLineNumberT.Line1011))
                        {
                            Set1011Expense(windExp, dataLoan);
                            remainingLineNums.Remove(E_CustomExpenseLineNumberT.Line1011);
                        }

                        break;
                    case E_sInsSettlementChargeT.None:
                        break;
                    default:
                        throw new UnhandledEnumException(dataLoan.sWindInsSettlementChargeT);
                }
            }

            // Condo
            CustomExpense condoExp = dataLoan.sCondoHO6Expense as CustomExpense;
            condoExp.PolicyActivationD_rep = dataLoan.sCondoHO6InsPolicyActivationD_rep;
            if (dataLoan.sCondoHO6InsSettlementChargeT != E_sInsSettlementChargeT.None)
            {
                switch (dataLoan.sCondoHO6InsSettlementChargeT)
                {
                    case E_sInsSettlementChargeT.Line1008:
                        if (remainingLineNums.Contains(E_CustomExpenseLineNumberT.Line1008))
                        {
                            Set1008Expense(condoExp, dataLoan);
                            remainingLineNums.Remove(E_CustomExpenseLineNumberT.Line1008);
                        }

                        break;
                    case E_sInsSettlementChargeT.Line1009:
                        if (remainingLineNums.Contains(E_CustomExpenseLineNumberT.Line1009))
                        {
                            Set1009Expense(condoExp, dataLoan);
                            remainingLineNums.Remove(E_CustomExpenseLineNumberT.Line1009);
                        }

                        break;
                    case E_sInsSettlementChargeT.Line1010:
                        if (remainingLineNums.Contains(E_CustomExpenseLineNumberT.Line1010))
                        {
                            Set1010Expense(condoExp, dataLoan);
                            remainingLineNums.Remove(E_CustomExpenseLineNumberT.Line1010);
                        }

                        break;
                    case E_sInsSettlementChargeT.Line1011:
                        if (remainingLineNums.Contains(E_CustomExpenseLineNumberT.Line1011))
                        {
                            Set1011Expense(condoExp, dataLoan);
                            remainingLineNums.Remove(E_CustomExpenseLineNumberT.Line1011);
                        }

                        break;
                    case E_sInsSettlementChargeT.None:
                        break;
                    default:
                        throw new UnhandledEnumException(dataLoan.sCondoHO6InsSettlementChargeT);
                }
            }

            // Tax 1
            CustomExpense tax1Exp = dataLoan.sOtherTax1Expense as CustomExpense;
            tax1Exp.TaxType = dataLoan.sTaxTable1008T;
            if (dataLoan.sTaxTable1008DescT != E_TableSettlementDescT.NoInfo)
            {
                switch (dataLoan.sTaxTable1008DescT)
                {
                    case E_TableSettlementDescT.Use1008DescT:
                        if (remainingLineNums.Contains(E_CustomExpenseLineNumberT.Line1008))
                        {
                            Set1008Expense(tax1Exp, dataLoan);
                            remainingLineNums.Remove(E_CustomExpenseLineNumberT.Line1008);
                        }

                        break;
                    case E_TableSettlementDescT.Use1009DescT:
                        if (remainingLineNums.Contains(E_CustomExpenseLineNumberT.Line1009))
                        {
                            Set1009Expense(tax1Exp, dataLoan);
                            remainingLineNums.Remove(E_CustomExpenseLineNumberT.Line1009);
                        }

                        break;
                    case E_TableSettlementDescT.Use1010DescT:
                        if (remainingLineNums.Contains(E_CustomExpenseLineNumberT.Line1010))
                        {
                            Set1010Expense(tax1Exp, dataLoan);
                            remainingLineNums.Remove(E_CustomExpenseLineNumberT.Line1010);
                        }

                        break;
                    case E_TableSettlementDescT.Use1011DescT:
                        if (remainingLineNums.Contains(E_CustomExpenseLineNumberT.Line1011))
                        {
                            Set1011Expense(tax1Exp, dataLoan);
                            remainingLineNums.Remove(E_CustomExpenseLineNumberT.Line1011);
                        }

                        break;
                    case E_TableSettlementDescT.NoInfo:
                        break;
                    default:
                        throw new UnhandledEnumException(dataLoan.sTaxTable1008DescT);
                }
            }

            // Tax 2
            CustomExpense tax2Exp = dataLoan.sOtherTax2Expense as CustomExpense;
            tax2Exp.TaxType = dataLoan.sTaxTable1009T;
            if (dataLoan.sTaxTable1009DescT != E_TableSettlementDescT.NoInfo)
            {
                switch (dataLoan.sTaxTable1009DescT)
                {
                    case E_TableSettlementDescT.Use1008DescT:
                        if (remainingLineNums.Contains(E_CustomExpenseLineNumberT.Line1008))
                        {
                            Set1008Expense(tax2Exp, dataLoan);
                            remainingLineNums.Remove(E_CustomExpenseLineNumberT.Line1008);
                        }

                        break;
                    case E_TableSettlementDescT.Use1009DescT:
                        if (remainingLineNums.Contains(E_CustomExpenseLineNumberT.Line1009))
                        {
                            Set1009Expense(tax2Exp, dataLoan);
                            remainingLineNums.Remove(E_CustomExpenseLineNumberT.Line1009);
                        }

                        break;
                    case E_TableSettlementDescT.Use1010DescT:
                        if (remainingLineNums.Contains(E_CustomExpenseLineNumberT.Line1010))
                        {
                            Set1010Expense(tax2Exp, dataLoan);
                            remainingLineNums.Remove(E_CustomExpenseLineNumberT.Line1010);
                        }

                        break;
                    case E_TableSettlementDescT.Use1011DescT:
                        if (remainingLineNums.Contains(E_CustomExpenseLineNumberT.Line1011))
                        {
                            Set1011Expense(tax2Exp, dataLoan);
                            remainingLineNums.Remove(E_CustomExpenseLineNumberT.Line1011);
                        }

                        break;
                    case E_TableSettlementDescT.NoInfo:
                        break;
                    default:
                        throw new UnhandledEnumException(dataLoan.sTaxTable1009DescT);
                }
            }

            // Tax 3
            CustomExpense tax3Exp = dataLoan.sOtherTax3Expense as CustomExpense;
            tax3Exp.TaxType = dataLoan.sTaxTableU3T;
            if (dataLoan.sTaxTableU3DescT != E_TableSettlementDescT.NoInfo)
            {
                switch (dataLoan.sTaxTableU3DescT)
                {
                    case E_TableSettlementDescT.Use1008DescT:
                        if (remainingLineNums.Contains(E_CustomExpenseLineNumberT.Line1008))
                        {
                            Set1008Expense(tax3Exp, dataLoan);
                            remainingLineNums.Remove(E_CustomExpenseLineNumberT.Line1008);
                        }

                        break;
                    case E_TableSettlementDescT.Use1009DescT:
                        if (remainingLineNums.Contains(E_CustomExpenseLineNumberT.Line1009))
                        {
                            Set1009Expense(tax3Exp, dataLoan);
                            remainingLineNums.Remove(E_CustomExpenseLineNumberT.Line1009);
                        }

                        break;
                    case E_TableSettlementDescT.Use1010DescT:
                        if (remainingLineNums.Contains(E_CustomExpenseLineNumberT.Line1010))
                        {
                            Set1010Expense(tax3Exp, dataLoan);
                            remainingLineNums.Remove(E_CustomExpenseLineNumberT.Line1010);
                        }

                        break;
                    case E_TableSettlementDescT.Use1011DescT:
                        if (remainingLineNums.Contains(E_CustomExpenseLineNumberT.Line1011))
                        {
                            Set1011Expense(tax3Exp, dataLoan);
                            remainingLineNums.Remove(E_CustomExpenseLineNumberT.Line1011);
                        }

                        break;
                    case E_TableSettlementDescT.NoInfo:
                        break;
                    default:
                        throw new UnhandledEnumException(dataLoan.sTaxTableU3DescT);
                }
            }

            // Tax 4
            CustomExpense tax4Exp = dataLoan.sOtherTax4Expense as CustomExpense;
            tax4Exp.TaxType = dataLoan.sTaxTableU4T;
            if (dataLoan.sTaxTableU4DescT != E_TableSettlementDescT.NoInfo)
            {
                switch (dataLoan.sTaxTableU4DescT)
                {
                    case E_TableSettlementDescT.Use1008DescT:
                        if (remainingLineNums.Contains(E_CustomExpenseLineNumberT.Line1008))
                        {
                            Set1008Expense(tax4Exp, dataLoan);
                            remainingLineNums.Remove(E_CustomExpenseLineNumberT.Line1008);
                        }

                        break;
                    case E_TableSettlementDescT.Use1009DescT:
                        if (remainingLineNums.Contains(E_CustomExpenseLineNumberT.Line1009))
                        {
                            Set1009Expense(tax4Exp, dataLoan);
                            remainingLineNums.Remove(E_CustomExpenseLineNumberT.Line1009);
                        }

                        break;
                    case E_TableSettlementDescT.Use1010DescT:
                        if (remainingLineNums.Contains(E_CustomExpenseLineNumberT.Line1010))
                        {
                            Set1010Expense(tax4Exp, dataLoan);
                            remainingLineNums.Remove(E_CustomExpenseLineNumberT.Line1010);
                        }

                        break;
                    case E_TableSettlementDescT.Use1011DescT:
                        if (remainingLineNums.Contains(E_CustomExpenseLineNumberT.Line1011))
                        {
                            Set1011Expense(tax4Exp, dataLoan);
                            remainingLineNums.Remove(E_CustomExpenseLineNumberT.Line1011);
                        }

                        break;
                    case E_TableSettlementDescT.NoInfo:
                        break;
                    default:
                        throw new UnhandledEnumException(dataLoan.sTaxTableU4DescT);
                }
            }

            // Unassigned Expenses
            foreach (E_CustomExpenseLineNumberT lineNum in remainingLineNums)
            {
                switch (lineNum)
                {
                    case E_CustomExpenseLineNumberT.Line1008:
                        CustomExpense line1008exp = dataLoan.sLine1008Expense as CustomExpense;
                        Set1008Expense(line1008exp, dataLoan);
                        break;
                    case E_CustomExpenseLineNumberT.Line1009:
                        CustomExpense line1009exp = dataLoan.sLine1009Expense as CustomExpense;
                        Set1009Expense(line1009exp, dataLoan);
                        break;
                    case E_CustomExpenseLineNumberT.Line1010:
                        CustomExpense line1010exp = dataLoan.sLine1010Expense as CustomExpense;
                        Set1010Expense(line1010exp, dataLoan);
                        break;
                    case E_CustomExpenseLineNumberT.Line1011:
                        CustomExpense line1011exp = dataLoan.sLine1011Expense as CustomExpense;
                        Set1011Expense(line1011exp, dataLoan);
                        break;
                    default:
                        throw new UnhandledEnumException(lineNum);
                }
            }

            dataLoan.sIsHousingExpenseMigrated = true;

            if (shouldSave)
            {
                dataLoan.Save();
            }
        }

        /// <summary>
        /// Populate the expense on line 1008.
        /// </summary>
        /// <param name="expense">The expense to populate.</param>
        /// <param name="dataLoan">The parent data loan.</param>
        private static void Set1008Expense(CustomExpense expense, CPageData dataLoan)
        {
            expense.SetCustomLineNumber(E_CustomExpenseLineNumberT.Line1008);
            expense.MonthlyAmtFixedAmt = dataLoan.s1006ProHExp;
            expense.IsEscrowedAtClosing = dataLoan.s1006RsrvEscrowedTri;
            expense.ReserveMonths = dataLoan.s1006RsrvMon;
            expense.ReserveMonthsLckd = dataLoan.s1006RsrvMonLckd;

            expense.AnnualAmtCalcType = E_AnnualAmtCalcTypeT.LoanValues;
            expense.DisbursementRepInterval = E_DisbursementRepIntervalT.Annual;
            for (int i = 0; i < 13; i++)
            {
                expense.DisbursementScheduleMonths[i] = dataLoan.sInitialEscrowAcc[i, 5];
            }
        }

        /// <summary>
        /// Populate the expense on line 1009.
        /// </summary>
        /// <param name="expense">The expense to populate.</param>
        /// <param name="dataLoan">The parent data loan.</param>
        private static void Set1009Expense(CustomExpense expense, CPageData dataLoan)
        {
            expense.SetCustomLineNumber(E_CustomExpenseLineNumberT.Line1009);
            expense.MonthlyAmtFixedAmt = dataLoan.s1007ProHExp;
            expense.IsEscrowedAtClosing = dataLoan.s1007RsrvEscrowedTri;
            expense.ReserveMonths = dataLoan.s1007RsrvMon;
            expense.ReserveMonthsLckd = dataLoan.s1007RsrvMonLckd;

            expense.AnnualAmtCalcType = E_AnnualAmtCalcTypeT.LoanValues;
            expense.DisbursementRepInterval = E_DisbursementRepIntervalT.Annual;
            for (int i = 0; i < 13; i++)
            {
                expense.DisbursementScheduleMonths[i] = dataLoan.sInitialEscrowAcc[i, 6];
            }
        }

        /// <summary>
        /// Populate the expense on line 1010.
        /// </summary>
        /// <param name="expense">The expense to populate.</param>
        /// <param name="dataLoan">The parent data loan.</param>
        private static void Set1010Expense(CustomExpense expense, CPageData dataLoan)
        {
            expense.SetCustomLineNumber(E_CustomExpenseLineNumberT.Line1010);
            expense.MonthlyAmtFixedAmt = dataLoan.sProU3Rsrv;
            expense.IsEscrowedAtClosing = dataLoan.sU3RsrvEscrowedTri;
            expense.ReserveMonths = dataLoan.sU3RsrvMon;
            expense.ReserveMonthsLckd = dataLoan.sU3RsrvMonLckd;

            expense.AnnualAmtCalcType = E_AnnualAmtCalcTypeT.LoanValues;
            expense.DisbursementRepInterval = E_DisbursementRepIntervalT.Annual;
            for (int i = 0; i < 13; i++)
            {
                expense.DisbursementScheduleMonths[i] = dataLoan.sInitialEscrowAcc[i, 7];
            }
        }

        /// <summary>
        /// Populate the expense on line 1011.
        /// </summary>
        /// <param name="expense">The expense to populate.</param>
        /// <param name="dataLoan">The parent data loan.</param>
        private static void Set1011Expense(CustomExpense expense, CPageData dataLoan)
        {
            expense.SetCustomLineNumber(E_CustomExpenseLineNumberT.Line1011);
            expense.MonthlyAmtFixedAmt = dataLoan.sProU4Rsrv;
            expense.IsEscrowedAtClosing = dataLoan.sU4RsrvEscrowedTri;
            expense.ReserveMonths = dataLoan.sU4RsrvMon;
            expense.ReserveMonthsLckd = dataLoan.sU4RsrvMonLckd;

            expense.AnnualAmtCalcType = E_AnnualAmtCalcTypeT.LoanValues;
            expense.DisbursementRepInterval = E_DisbursementRepIntervalT.Annual;
            for (int i = 0; i < 13; i++)
            {
                expense.DisbursementScheduleMonths[i] = dataLoan.sInitialEscrowAcc[i, 8];
            }
        }
    }
}
