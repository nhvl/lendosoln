// <copyright file="SingleDisbursement.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   12/22/2014 
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using LendersOffice.Common;

    /// <summary>
    /// A disbursement of a housing expense is a payment that is required to be made.
    /// </summary>
    [DataContract]
    public class SingleDisbursement
    {
        /// <summary>
        /// The amount that is due/was disbursed.
        /// </summary>
        [DataMember(Name = "dAmt")]
        private decimal disbursementAmt;

        /// <summary>
        /// Initializes a new instance of the <see cref="SingleDisbursement" /> class.
        /// </summary>
        public SingleDisbursement()
        {
            this.DisbId = Guid.NewGuid();
        }

        /// <summary>
        /// Gets or sets the parent HousingExpense.
        /// </summary>
        /// <value>The parent housing expense.</value>
        public BaseHousingExpense ParentExpense
        {
            private get;
            set;
        }

        /// <summary>
        /// Gets the unique ID for this Guid.
        /// </summary>
        /// <value>The unique ID for the Guid.</value>
        [DataMember(Name = "dId")]
        public Guid DisbId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the paid date type.
        /// </summary>
        /// <value>The date paid relative to closing.</value>
        [DataMember(Name = "dPaidDT")]
        public E_DisbPaidDateType DisbPaidDateType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Disbursement Type.
        /// </summary>
        /// <value>
        /// Whether the disbursement is actual or projected.
        /// </value>
        [DataMember(Name = "dType")]
        public E_DisbursementTypeT DisbursementType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Due Date.
        /// </summary>
        /// <value>
        /// The date that the disbursement needs/needed to be made.
        /// </value>
        [DataMember(Name = "dDueD")]
        public DateTime DueDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the string rep of DueDate.
        /// </summary>
        /// <value>String representation of the property.</value>
        public string DueDate_rep
        {
            get
            {
                return this.ParentExpense.Converter.ToDateTimeString(this.DueDate);
            }

            set
            {
                this.DueDate = this.ToDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the Disbursement Amount.
        /// </summary>
        /// <value>
        /// If the disbursement amount is locked, the parent expense calculation type is Disbursement or if
        /// the disbursement is an Actual type, then return the stored value.
        /// Else calculate using parent's monthly amount total (since this is a projected disbursement).
        /// </value>
        public decimal DisbursementAmt
        {
            get
            {
                if (this.DisbursementAmtLckd || this.ParentExpense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements
                 || this.DisbursementType == E_DisbursementTypeT.Actual)
                {
                    return this.disbursementAmt;
                }

                decimal calculatedDisbursementAmount;
                if (this.EscAccMonth == -1)
                {
                    calculatedDisbursementAmount = this.ParentExpense.MonthlyAmtTotal;
                }
                else
                {
                    calculatedDisbursementAmount = this.ParentExpense.MonthlyAmtTotal * this.ParentExpense.DisbursementScheduleMonths[this.EscAccMonth];
                }

                // Disbursements represent actual payments (cash or check), and so they need to be even cents.
                // Round to the nearest cent.
                return Math.Round(calculatedDisbursementAmount, 2, MidpointRounding.AwayFromZero);
            }

            set
            {
                this.disbursementAmt = value;
            }
        }

        /// <summary>
        /// Gets or sets the string rep of Disbursement Amount.
        /// </summary>
        /// <value>String representation of the property.</value>
        public string DisbursementAmt_rep
        {
            get
            {
                return this.ParentExpense.Converter.ToMoneyStringRoundAwayFromZero(this.DisbursementAmt, FormatDirection.ToRep);
            }

            set
            {
                this.DisbursementAmt = this.ParentExpense.Converter.ToMoney(value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the Disbursement Amount was manually entered.
        /// </summary>
        /// <value>
        /// Whether or not the disbursement amount is manually entered or calculated.
        /// </value>
        [DataMember(Name = "dAmtL")]
        public bool DisbursementAmtLckd
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the covered months.
        /// </summary>
        /// <value>The number of months of payments that this disbursement covers.</value>
        [DataMember(Name = "dCovMon")]
        public int CoveredMonths
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the string representation of covered months.
        /// </summary>
        /// <value>The string representation of covered months.</value>
        public string CoveredMonths_rep
        {
            get
            {
                return this.ParentExpense.Converter.ToCountString(this.CoveredMonths);
            }

            set
            {
                this.CoveredMonths = this.ParentExpense.Converter.ToCount(value);
            }
        }

        /// <summary>
        /// Gets or sets the Paid By value.
        /// </summary>
        /// <value>
        /// The entity that paid the disbursement.
        /// </value>
        [DataMember(Name = "dPaidBy")]
        public E_DisbursementPaidByT PaidBy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the disbursement was made from escrow impounds account.
        /// </summary>
        /// <value>
        /// Whether or not the disbursement was made from the escrow impounds account.
        /// </value>
        [DataMember(Name = "dIsPaidE")]
        public bool IsPaidFromEscrowAcc
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Paid Date.
        /// </summary>
        /// <value>
        /// The date on which the disbursement was made.
        /// </value>
        [DataMember(Name = "dPaidD")]
        public DateTime PaidDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the string rep of PaidDate.
        /// </summary>
        /// <value>String representation of the property.</value>
        public string PaidDate_rep
        {
            get
            {
                return this.ParentExpense.Converter.ToDateTimeString(this.PaidDate);
            }

            set
            {
                this.PaidDate = this.ToDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the Billing Period Start Date.
        /// </summary>
        /// <value>
        /// The start date of the billing period that the disbursement covers.
        /// </value>
        [DataMember(Name = "dSD")]
        public DateTime BillingPeriodStartD
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the string rep of BillingPeriodStartD.
        /// </summary>
        /// <value>String representation of the property.</value>
        public string BillingPeriodStartD_rep
        {
            get
            {
                return this.ParentExpense.Converter.ToDateTimeString(this.BillingPeriodStartD);
            }

            set
            {
                this.BillingPeriodStartD = this.ToDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the Billing Period End Date.
        /// </summary>
        /// <value>
        /// The end date of the billing period that the disbursement covers.
        /// </value>
        [DataMember(Name = "dED")]
        public DateTime BillingPeriodEndD
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the string rep of BillingPeriodEndD.
        /// </summary>
        /// <value>String representation of the property.</value>
        public string BillingPeriodEndD_rep
        {
            get
            {
                return this.ParentExpense.Converter.ToDateTimeString(this.BillingPeriodEndD);
            }

            set
            {
                this.BillingPeriodEndD = this.ToDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the month in the initial escrow account table for this disbursement.
        /// </summary>
        /// <value>The value in the initial escrow account table. Only used for Projected type.</value>
        public int EscAccMonth
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the annual amount calculation type of the associated expense.
        /// </summary>
        /// <value>
        /// The annual amount calculation type of the associated expense.
        /// </value>
        public E_AnnualAmtCalcTypeT ExpenseCalcType
        {
            get
            {
                return this.ParentExpense.AnnualAmtCalcType;
            }
        }

        /// <summary>
        /// Gets the DateTime of the input string.
        /// </summary>
        /// <param name="date">The string to parse.</param>
        /// <returns>Returns DateTime.MinValue if invalid, the DateTime if successful.</returns>
        private DateTime ToDate(string date)
        {
            if (string.IsNullOrEmpty(date))
            {
                return DateTime.MinValue;
            }

            CDateTime output = CDateTime.Create(date, this.ParentExpense.Converter);
            if (output.IsValid)
            {
                return output.DateTimeForComputation;
            }

            return DateTime.MinValue;
        }
    }
}
