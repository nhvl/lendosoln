// <copyright file="GroundRentExpense.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   12/22/2014 
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using LendersOffice.Common;

    /// <summary>
    /// Class for Ground Rent Expenses.
    /// </summary>
    [DataContract]
    public class GroundRentExpense : BaseHousingExpense
    {
        /// <summary>
        /// The monthly fixed amount if the loan is an FHA loan.
        /// </summary>
        [DataMember(Name = "grFHATot")]
        private decimal fhamonthlyFixed;

        /// <summary>
        /// The monthly fixed amount if the loan is an VA loan.
        /// </summary>
        [DataMember(Name = "grVATot")]
        private decimal vamonthlyFixed;

        /// <summary>
        /// Initializes a new instance of the <see cref="GroundRentExpense" /> class.
        /// </summary>
        public GroundRentExpense()
        {
            this.HousingExpenseType = E_HousingExpenseTypeT.GroundRent;
            this.DisbursementScheduleMonths = this.DefaultDisbSchedule;
            this.DisbursementRepInterval = E_DisbursementRepIntervalT.Monthly;
            this.CommonInitialize();
            this.CustomExpenseLineNum = E_CustomExpenseLineNumberT.None;
        }

        /// <summary>
        /// Gets or sets the disbursement monthly schedule.
        /// </summary>
        /// <value>The disbursement monthly schedule.</value>
        public override int[] DefaultDisbSchedule
        {
            get
            {
                int[] defaultSched = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
                return defaultSched;
            }
        }

        /// <summary>
        /// Gets or sets the FHA Monthly Fixed Amount.
        /// </summary>
        /// <value>The FHA Monthly Fixed Amount.</value>
        public decimal FHAMonthlyAmtFixedAmt
        {
            get
            {
                return this.fhamonthlyFixed;
            }

            set
            {
                this.fhamonthlyFixed = value;
            }
        }

        /// <summary>
        /// Gets or sets the string representation of FHA Monthly Fixed Amount.
        /// </summary>
        /// <value>String representation of the property.</value>
        public string FHAMonthlyAmtFixedAmt_rep
        {
            get
            {
                return this.Converter.ToMoneyString(this.FHAMonthlyAmtFixedAmt, FormatDirection.ToRep);
            }

            set
            {
                this.FHAMonthlyAmtFixedAmt = this.Converter.ToMoney(value);
            }
        }

        /// <summary>
        /// Gets or sets the VA Monthly Fixed Amount.
        /// </summary>
        /// <value>The VA Monthly Fixed Amount.</value>
        public decimal VAMonthlyAmtFixedAmt
        {
            get
            {
                return this.vamonthlyFixed;
            }

            set
            {
                this.vamonthlyFixed = value;
            }
        }

        /// <summary>
        /// Gets or sets the string representation of the VA Monthly Fixed Amount.
        /// </summary>
        /// <value>The string representation of the field.</value>
        public string VAMonthlyAmtFixedAmt_rep
        {
            get
            {
                return this.Converter.ToMoneyString(this.VAMonthlyAmtFixedAmt, FormatDirection.ToRep);
            }

            set
            {
                this.VAMonthlyAmtFixedAmt = this.Converter.ToMoney(value);
            }
        }
        
        /// <summary>
        /// Gets or sets the monthly fixed amount depending on loan type.
        /// </summary>
        /// <value>The monthly fixed amount depending on loan type.</value>
        public override decimal MonthlyAmtFixedAmt
        {
            get
            {
                if (this.DataLoan == null)
                {
                    return base.MonthlyAmtFixedAmt;
                }

                if (this.DataLoan.sLT == E_sLT.FHA)
                {
                    return this.FHAMonthlyAmtFixedAmt;
                }
                else if (this.DataLoan.sLT == E_sLT.VA)
                {
                    return this.VAMonthlyAmtFixedAmt;
                }
                else
                {
                    return base.MonthlyAmtFixedAmt;
                }
            }

            set
            {
                // This is for the deserializer. Assign the GroundExpense's
                // stored MonthlyAmtFixedAmt to the base. 
                if (this.DataLoan == null)
                {
                    base.MonthlyAmtFixedAmt = value;
                    return;
                }

                if (this.DataLoan.sLT == E_sLT.FHA)
                {
                    this.FHAMonthlyAmtFixedAmt = value;
                }
                else if (this.DataLoan.sLT == E_sLT.VA)
                {
                    this.VAMonthlyAmtFixedAmt = value;
                }
                else
                {
                    base.MonthlyAmtFixedAmt = value;
                }
            }
        }

        /// <summary>
        /// Gets the expense's default description.
        /// </summary>
        /// <value>The default description.</value>
        public override string ExpensePrefix
        {
            get
            {
                return "GROUND";
            }
        }

        /// <summary>
        /// Gets or sets the line number for the custom expense.
        /// </summary>
        /// <value>The line number for the custom expense.</value>
        public override E_CustomExpenseLineNumberT CustomExpenseLineNum
        {
            get
            {
                if (this.IsEscrowedAtClosing == E_TriState.Yes && base.CustomExpenseLineNum == E_CustomExpenseLineNumberT.None)
                {
                    base.CustomExpenseLineNum = E_CustomExpenseLineNumberT.Any;
                }

                return base.CustomExpenseLineNum;
            }

            protected set
            {
                base.CustomExpenseLineNum = value;
            }
        }

        /// <summary>
        /// Sets the line number for the custom expense.
        /// </summary>
        /// <param name="lineNum">The line number to set the expense to.</param>
        public override void SetCustomLineNumber(E_CustomExpenseLineNumberT lineNum)
        {
            this.CustomExpenseLineNum = lineNum;
        }
    }
}
