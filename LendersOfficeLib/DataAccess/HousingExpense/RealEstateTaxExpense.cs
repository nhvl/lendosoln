// <copyright file="RealEstateTaxExpense.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   12/22/2014 
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using LendersOffice.Common;

    /// <summary>
    /// Class for Real Estate Tax Expenses.
    /// </summary>
    public class RealEstateTaxExpense : BaseHousingExpense
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RealEstateTaxExpense" /> class.
        /// </summary>
        public RealEstateTaxExpense()
        {
            this.HousingExpenseType = E_HousingExpenseTypeT.RealEstateTaxes;
            this.DisbursementScheduleMonths = this.DefaultDisbSchedule;
            this.CommonInitialize();
        }

        /// <summary>
        /// Gets the expense's default description.
        /// </summary>
        /// <value>The default description.</value>
        public override string ExpensePrefix
        {
            get
            {
                return "REAL";
            }
        }

        /// <summary>
        /// Gets the default escrow schedule for this expense.
        /// </summary>
        /// <value>The default escrow schedule.</value>
        public override int[] DefaultDisbSchedule
        {
            get
            {
                int[] defaultSched = { 2, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 6, 0 };
                return defaultSched;
            }
        }

        /// <summary>
        /// Gets the line number as a string.
        /// </summary>
        /// <value>The line number as a string.</value>
        public override string LineNumAsString
        {
            get
            {
                return "1004";
            }
        }
    }
}
