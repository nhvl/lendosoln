// <copyright file="FloodExpense.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   12/22/2014 
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using LendersOffice.Common;

    /// <summary>
    /// Class for Flood Insurance Expenses.
    /// </summary>
    public class FloodExpense : BaseHousingExpense
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FloodExpense" /> class.
        /// </summary>
        public FloodExpense()
        {
            this.HousingExpenseType = E_HousingExpenseTypeT.FloodInsurance;
            this.DisbursementScheduleMonths = this.DefaultDisbSchedule;
            this.CommonInitialize();
        }

        /// <summary>
        /// Gets the expense's default description.
        /// </summary>
        /// <value>The default description.</value>
        public override string ExpensePrefix
        {
            get
            {
                return "FLOOD";
            }
        }

        /// <summary>
        /// Gets the default escrow schedule for this expense.
        /// </summary>
        /// <value>The default escrow schedule.</value>
        public override int[] DefaultDisbSchedule
        {
            get
            {
                int[] defaultSched = { 2, 0, 0, 0, 0, 0, 0, 0, 12, 0, 0, 0, 0 };
                return defaultSched;
            }
        }

        /// <summary>
        /// Gets the line number as a string.
        /// </summary>
        /// <value>The line number as a string.</value>
        public override string LineNumAsString
        {
            get
            {
                return "1006";
            }
        }
    }
}
