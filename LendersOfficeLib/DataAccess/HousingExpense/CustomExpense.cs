﻿// <copyright file="CustomExpense.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   12/22/2014 
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using LendersOffice.Common;

    /// <summary>
    /// Class for Custom Expenses.
    /// </summary>
    public class CustomExpense : BaseHousingExpense
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomExpense" /> class.
        /// </summary>
        public CustomExpense()
        {
            this.HousingExpenseType = E_HousingExpenseTypeT.Unassigned;
            this.CustomExpenseLineNum = E_CustomExpenseLineNumberT.None;
            this.InitialSetup();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomExpense" /> class.
        /// </summary>
        /// <param name="lineNum">The line number the expense belongs on.</param>
        public CustomExpense(E_CustomExpenseLineNumberT lineNum)
        {
            this.HousingExpenseType = E_HousingExpenseTypeT.Unassigned; 
            this.CustomExpenseLineNum = lineNum;
            this.InitialSetup();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomExpense" /> class.
        /// </summary>
        /// <param name="type">The type of custom expense.</param>
        public CustomExpense(E_HousingExpenseTypeT type)
        {
            this.HousingExpenseType = type;
            this.CustomExpenseLineNum = E_CustomExpenseLineNumberT.None;
            this.InitialSetup();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomExpense" /> class.
        /// </summary>
        /// <param name="type">The type of custom expense.</param>
        /// <param name="lineNum">The line number the expense belongs on.</param>
        public CustomExpense(E_HousingExpenseTypeT type, E_CustomExpenseLineNumberT lineNum)
        {
            this.HousingExpenseType = type; 
            this.CustomExpenseLineNum = lineNum;
            this.InitialSetup();
        }

        /// <summary>
        /// Gets the expense's default prefix.
        /// </summary>
        /// <value>The default description.</value>
        public override string ExpensePrefix
        {
            get
            {
                if (this.HousingExpenseType == E_HousingExpenseTypeT.WindstormInsurance)
                {
                    return "WIND";
                }
                else if (this.HousingExpenseType == E_HousingExpenseTypeT.CondoHO6Insurance)
                {
                    return "CONDO";
                }
                else if (this.HousingExpenseType == E_HousingExpenseTypeT.OtherTaxes1)
                {
                    return "TAX1";
                }
                else if (this.HousingExpenseType == E_HousingExpenseTypeT.OtherTaxes2)
                {
                    return "TAX2";
                }
                else if (this.HousingExpenseType == E_HousingExpenseTypeT.OtherTaxes3)
                {
                    return "TAX3";
                }
                else if (this.HousingExpenseType == E_HousingExpenseTypeT.OtherTaxes4)
                {
                    return "TAX4";
                }
                else if (this.HousingExpenseType == E_HousingExpenseTypeT.Unassigned)
                {
                    switch (this.CustomExpenseLineNum)
                    {
                        case E_CustomExpenseLineNumberT.Line1008:
                            return "L1008";
                        case E_CustomExpenseLineNumberT.Line1009:
                            return "L1009";
                        case E_CustomExpenseLineNumberT.Line1010:
                            return "L1010";
                        case E_CustomExpenseLineNumberT.Line1011:
                            return "L1011";
                        case E_CustomExpenseLineNumberT.None:
                        case E_CustomExpenseLineNumberT.Any:
                            return string.Empty;
                        default:
                            throw new UnhandledEnumException(this.CustomExpenseLineNum);
                    }
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Gets the default escrow schedule for this expense.
        /// </summary>
        /// <value>The default escrow schedule.</value>
        public override int[] DefaultDisbSchedule
        {
            get
            {
                int[] defaultSched = { 2, 0, 0, 0, 0, 0, 12, 0, 0, 0, 0, 0, 0 };
                return defaultSched;
            }
        }

        /// <summary>
        /// Gets or sets the line number for the custom expense.
        /// </summary>
        /// <value>The line number for the custom expense.</value>
        public override E_CustomExpenseLineNumberT CustomExpenseLineNum
        {
            get
            {
                if (this.HousingExpenseType != E_HousingExpenseTypeT.Unassigned && 
                    this.IsEscrowedAtClosing == E_TriState.Yes && base.CustomExpenseLineNum == E_CustomExpenseLineNumberT.None)
                {
                    base.CustomExpenseLineNum = E_CustomExpenseLineNumberT.Any;
                }

                return base.CustomExpenseLineNum;
            }

            protected set
            {
                base.CustomExpenseLineNum = value;
            }
        }

        /// <summary>
        /// Sets the line number for the custom expense.
        /// </summary>
        /// <param name="lineNum">The line number to set the expense to.</param>
        public override void SetCustomLineNumber(E_CustomExpenseLineNumberT lineNum)
        {
            if (this.HousingExpenseType != E_HousingExpenseTypeT.Unassigned)
            {
                this.CustomExpenseLineNum = lineNum;
            }
        }

        /// <summary>
        /// Initial setup common to all CustomExpense constructors.
        /// </summary>
        private void InitialSetup()
        {
            this.DisbursementScheduleMonths = this.DefaultDisbSchedule;
            this.CommonInitialize();
        }
    }
}
