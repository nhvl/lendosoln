// <copyright file="SchoolTaxExpense.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   12/22/2014 
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using LendersOffice.Common;

    /// <summary>
    /// Class for School Tax Expenses.
    /// </summary>
    public class SchoolTaxExpense : BaseHousingExpense
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SchoolTaxExpense" /> class.
        /// </summary>
        public SchoolTaxExpense()
        {
            this.HousingExpenseType = E_HousingExpenseTypeT.SchoolTaxes;
            this.DisbursementScheduleMonths = this.DefaultDisbSchedule;
            this.CommonInitialize();
        }

        /// <summary>
        /// Gets the expense's default description.
        /// </summary>
        /// <value>The default description.</value>
        public override string ExpensePrefix
        {
            get
            {
                return "SCHOOL";
            }
        }

        /// <summary>
        /// Gets the default escrow schedule for this expense.
        /// </summary>
        /// <value>The default escrow schedule.</value>
        public override int[] DefaultDisbSchedule
        {
            get
            {
                int[] defaultSched = { 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 0, 0 };
                return defaultSched;
            }
        }

        /// <summary>
        /// Gets the line number as a string.
        /// </summary>
        /// <value>The line number as a string.</value>
        public override string LineNumAsString
        {
            get
            {
                return "1005";
            }
        }
    }
}
