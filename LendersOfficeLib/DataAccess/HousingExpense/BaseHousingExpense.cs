// <copyright file="BaseHousingExpense.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   12/22/2014 
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using LendersOffice.Common;

    /// <summary>
    /// Information about a Housing Expense, including disbursements made for it.
    /// Make sure to add to a HousingExpenses object before use. This requirement may not be needed
    /// in the future when all functionality gets ported to these classes.
    /// </summary>
    [DataContract]
    [KnownType(typeof(HazardExpense))]
    [KnownType(typeof(FloodExpense))]
    [KnownType(typeof(RealEstateTaxExpense))]
    [KnownType(typeof(SchoolTaxExpense))]
    [KnownType(typeof(HOADuesExpense))]
    [KnownType(typeof(GroundRentExpense))]
    [KnownType(typeof(CustomExpense))]
    public abstract class BaseHousingExpense
    {
        /// <summary>
        /// The converter to use.
        /// </summary>
        private LosConvert losConverter;

        /// <summary>
        /// The disbursement schedule in months.
        /// </summary>
        [DataMember(Name = "hdisbMonSched")]
        private int[] disbursementScheduleMonths = new int[13];

        /// <summary>
        /// The reserve months.
        /// </summary>
        [DataMember(Name = "hResMon")]
        private int reserveMonths;

        /// <summary>
        /// Dictionary of Disbursements that have been manually entered.
        /// </summary>
        [DataMember(Name = "hDisbList")]
        private Dictionary<Guid, SingleDisbursement> actualDisbursementList;

        /// <summary>
        /// Description for this expense.
        /// </summary>
        [DataMember(Name = "hExpDesc")]
        private string expenseDesc;

        /// <summary>
        /// The number of whole months worth of monthly payments necessary to pay the prepaid housing expense amount due at closing.
        /// </summary>
        [DataMember(Name = "hPrePMnth")]
        private int prepaidMonths;

        /// <summary>
        /// The line number for the custom expense.
        /// </summary>
        [DataMember(Name = "hCusLineNum")]
        private E_CustomExpenseLineNumberT customExpenseLinNum;

        /// <summary>
        /// Gets a value indicating whether this expense is a tax expense.
        /// </summary>
        /// <value>True if the expense is a tax expense. Otherwise, false.</value>
        public bool IsTax
        {
            get
            {
                switch (this.HousingExpenseType)
                {
                    case E_HousingExpenseTypeT.OtherTaxes1:
                    case E_HousingExpenseTypeT.OtherTaxes2:
                    case E_HousingExpenseTypeT.OtherTaxes3:
                    case E_HousingExpenseTypeT.OtherTaxes4:
                    case E_HousingExpenseTypeT.RealEstateTaxes:
                    case E_HousingExpenseTypeT.SchoolTaxes:
                        return true;
                    case E_HousingExpenseTypeT.CondoHO6Insurance:
                    case E_HousingExpenseTypeT.FloodInsurance:
                    case E_HousingExpenseTypeT.GroundRent:
                    case E_HousingExpenseTypeT.HazardInsurance:
                    case E_HousingExpenseTypeT.HomeownersAsscDues:
                    case E_HousingExpenseTypeT.Unassigned:
                    case E_HousingExpenseTypeT.WindstormInsurance:
                        return false;
                    default:
                        throw new UnhandledEnumException(this.HousingExpenseType);
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether this expense is an insurance expense.
        /// </summary>
        /// <value>True if the expense is an insurance expense. Otherwise, false.</value>
        public bool IsInsurance
        {
            get
            {
                switch (this.HousingExpenseType)
                {
                    case E_HousingExpenseTypeT.CondoHO6Insurance:
                    case E_HousingExpenseTypeT.FloodInsurance:
                    case E_HousingExpenseTypeT.HazardInsurance:
                    case E_HousingExpenseTypeT.WindstormInsurance:
                        return true;
                    case E_HousingExpenseTypeT.GroundRent:
                    case E_HousingExpenseTypeT.HomeownersAsscDues:
                    case E_HousingExpenseTypeT.OtherTaxes1:
                    case E_HousingExpenseTypeT.OtherTaxes2:
                    case E_HousingExpenseTypeT.OtherTaxes3:
                    case E_HousingExpenseTypeT.OtherTaxes4:
                    case E_HousingExpenseTypeT.RealEstateTaxes:
                    case E_HousingExpenseTypeT.SchoolTaxes:
                    case E_HousingExpenseTypeT.Unassigned:
                        return false;
                    default:
                        throw new UnhandledEnumException(this.HousingExpenseType);
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether the expense can have a custom line num.
        /// </summary>
        /// <value>Value indicating whether or not the expense can have a custom line num.</value>
        public bool CanHaveCustomLineNum
        {
            get
            {
                switch (this.HousingExpenseType)
                {
                    case E_HousingExpenseTypeT.HazardInsurance:
                    case E_HousingExpenseTypeT.FloodInsurance:
                    case E_HousingExpenseTypeT.SchoolTaxes:
                    case E_HousingExpenseTypeT.RealEstateTaxes:
                    case E_HousingExpenseTypeT.Unassigned:
                        return false;
                    case E_HousingExpenseTypeT.CondoHO6Insurance:
                    case E_HousingExpenseTypeT.GroundRent:
                    case E_HousingExpenseTypeT.HomeownersAsscDues:
                    case E_HousingExpenseTypeT.OtherTaxes1:
                    case E_HousingExpenseTypeT.OtherTaxes2:
                    case E_HousingExpenseTypeT.OtherTaxes3:
                    case E_HousingExpenseTypeT.OtherTaxes4:
                    case E_HousingExpenseTypeT.WindstormInsurance:
                        return true;
                    default:
                        throw new UnhandledEnumException(this.HousingExpenseType);
                }
            }
        }

        /// <summary>
        /// Gets the expense's default description.
        /// </summary>
        /// <value>The default description.</value>
        public string DefaultExpenseDescription
        {
            get
            {
                return HousingExpenses.GetDefaultExpenseDescription(this.HousingExpenseType, this.CustomExpenseLineNum);
            }
        }

        /// <summary>
        /// Gets the expense's prefix.
        /// </summary>
        /// <value>The expense's prefix.</value>
        public virtual string ExpensePrefix
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the expense's description.
        /// </summary>
        /// <value>The expense's description.</value>
        public string ExpenseDescription
        {
            get
            {
                return this.expenseDesc;
            }

            set
            {
                this.expenseDesc = value;
            }
        }

        /// <summary>
        /// Gets a label for the expense. Either the assigned description or the default if the description is empty.
        /// </summary>
        /// <value>Label for the expense.</value>
        public string DescriptionOrDefault
        {
            get
            {
                if (string.IsNullOrEmpty(this.ExpenseDescription))
                {
                    return this.DefaultExpenseDescription;
                }
                else
                {
                    return this.ExpenseDescription;
                }
            }
        }

        /// <summary>
        /// Gets a calculated housing expense type based on <see cref="HousingExpenseType" /> and <see cref="CustomExpenseLineNum" />.
        /// Mostly used so that the Line10XX expenses get a specific type.
        /// </summary>
        /// <value>The calculated housing expense type.</value>
        public E_HousingExpenseTypeCalculatedT CalculatedHousingExpenseType
        {
            get
            {
                return HousingExpenses.GetCalculatedExpenseType(this.HousingExpenseType, this.CustomExpenseLineNum);
            }
        }

        /// <summary>
        /// Gets or sets the Housing Expense type.
        /// </summary>
        /// <value>
        /// The classification of the housing expense object. Only one object is allowed per type.
        /// </value>
        [DataMember(Name = "hExpT")]
        public E_HousingExpenseTypeT HousingExpenseType
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets or sets the Tax Type.
        /// </summary>
        /// <value>
        /// The classification of the taxing authority that is levying the tax or the classification of the tax itself.
        /// </value>
        [DataMember(Name = "hTaxT")]
        public E_TaxTableTaxT TaxType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Annual Amount calculation type.
        /// </summary>
        /// <value>
        /// The method being used to calculate the monthly housing expense.
        /// </value>
        [DataMember(Name = "hAnnAmtCalcT")]
        public E_AnnualAmtCalcTypeT AnnualAmtCalcType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the Annual Amount. Only counts non-prepaid disbursements.
        /// </summary>
        /// <value>
        /// If Disbursement calculation mode, use ActualDisbursementsInOneYear.
        /// If LoanValues mode, use ProjectedDisbursements.
        /// Sum all disbursement amounts, then multiply by 12 if disbursement interval is Monthly.
        /// </value>
        public decimal AnnualAmt
        {
            get
            {
                decimal total = 0;
                if (this.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
                {
                    foreach (SingleDisbursement sdb in this.ActualDisbursementsInOneYear)
                    {
                        total += sdb.DisbursementAmt;
                    }
                }
                else
                {
                    foreach (SingleDisbursement sdb in this.ProjectedDisbursements.FindAll(o => o.DueDate >= this.GetStartDateWindow()))
                    {
                        total += sdb.DisbursementAmt;
                    }
                }

                return total;
            }
        }

        /// <summary>
        /// Gets the string representation of Annual Amount.
        /// </summary>
        /// <value>String representation of the property.</value>
        public string AnnualAmt_rep
        {
            get
            {
                return this.Converter.ToMoneyStringRoundAwayFromZero(this.AnnualAmt, FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// Gets the Monthly Amount Total.
        /// </summary>
        /// <value>
        /// If Disbursement mode, then Annual Amount / 12.
        /// If Loan Values, then is the calculated value using the AnnualAmount... fields.
        /// </value>
        public decimal MonthlyAmtTotal
        {
            get
            {
                if (this.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
                {
                    return this.AnnualAmt / 12;
                }
                else if (this.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.LoanValues)
                {
                    return this.GetMonthlyAmtTotal();
                }
                else
                {
                    throw new UnhandledEnumException(this.AnnualAmtCalcType);
                }
            }
        }

        /// <summary>
        /// Gets the string representation of Monthly Amount Total.
        /// </summary>
        /// <value>String representation of the property.</value>
        public string MonthlyAmtTotal_rep
        {
            get
            {
                return this.Converter.ToMoneyStringRoundAwayFromZero(this.MonthlyAmtTotal, FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// Gets the annual amount calculated with respect to the aggregate escrow calculation method divided by 12.
        /// </summary>
        /// <value>The annual amount calculated with respect to the aggregate escrow calculation method divided by 12.</value>
        public decimal MonthlyAmtServicing
        {
            get
            {
                if (this.DataLoan != null && this.DataLoan.sAggEscrowCalcModeT == E_AggregateEscrowCalculationModeT.Legacy)
                {
                    if (this.IsEscrowedAtClosing == E_TriState.Yes)
                    {
                        return this.MonthlyAmtTotal;
                    }
                    else
                    {
                        return 0;
                    }
                }

                List<SingleDisbursement> temp1;
                List<SingleDisbursement> temp2;
                decimal total = this.AggregateAnnualTotal(out temp1, out temp2);
                return total / 12;
            }
        }

        /// <summary>
        /// Gets the string representation of Monthly Amount Servicing.
        /// </summary>
        /// <value>The string representation of the field.</value>
        public string MonthlyAmtServicing_rep
        {
            get
            {
                return this.Converter.ToMoneyStringRoundAwayFromZero(this.MonthlyAmtServicing, FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// Gets or sets the Annual Amount calculation percent.
        /// </summary>
        /// <value>
        /// If the Annual Amount is calculated as a percentage, what is the value of the percentage.
        /// </value>
        [DataMember(Name = "hAnnAmtCalcP")]
        public decimal AnnualAmtCalcBasePerc
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the string representation of Annual Amount Calculation Percent.
        /// </summary>
        /// <value>String representation of the property.</value>
        public string AnnualAmtCalcBasePerc_rep
        {
            get
            {
                return this.Converter.ToRateString(this.AnnualAmtCalcBasePerc);
            }

            set
            {
                this.AnnualAmtCalcBasePerc = this.Converter.ToRate(value);
            }
        }

        /// <summary>
        /// Gets or sets the Annual Amount calculation base type.
        /// </summary>
        /// <value>
        /// If the Annual Amount is calculated as a percentage, what value should be used as the base.
        /// </value>
        [DataMember(Name = "hAnnAmtCalcBaseT")]
        public E_PercentBaseT AnnualAmtCalcBaseType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the Annual Amount calculation base amount.
        /// </summary>
        /// <value>
        /// If the Annual Amount is calculated as a percentage, what amount is the percentage a percent of.
        /// </value>
        public virtual decimal AnnualAmtCalcBaseAmt
        {
            get
            {
                return this.GetPercentBaseAmount(this.AnnualAmtCalcBaseType);
            }
        }

        /// <summary>
        /// Gets the string representation of Annual Amount Calculation Base Amount.
        /// </summary>
        /// <value>String representation of the property.</value>
        public string AnnualAmtCalcBaseAmt_rep
        {
            get
            {
                return this.Converter.ToMoneyStringRoundAwayFromZero(this.AnnualAmtCalcBaseAmt, FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// Gets or sets the Monthly Amount fixed amount.
        /// </summary>
        /// <value>
        /// A dollar amount to add to the monthly amount of the housing expense calculated from an annual percentage.
        /// </value>
        [DataMember(Name = "hMnthAmtF")]
        public virtual decimal MonthlyAmtFixedAmt
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the string representation of Monthly Amount Fixed.
        /// </summary>
        /// <value>String representation of the property.</value>
        public virtual string MonthlyAmtFixedAmt_rep
        {
            get
            {
                return this.Converter.ToMoneyStringRoundAwayFromZero(this.MonthlyAmtFixedAmt, FormatDirection.ToRep);
            }

            set
            {
                this.MonthlyAmtFixedAmt = this.Converter.ToMoney(value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the expense is prepaid or not.
        /// </summary>
        /// <value>If the expense is prepaid or not.</value>
        [DataMember(Name = "hIsPrep")]
        public bool IsPrepaid
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Prepaid Months value.
        /// </summary>
        /// <value>
        /// The number of whole months worth of monthly payments necessary to pay the prepaid housing expense amount due at closing.
        /// </value>
        public int PrepaidMonths
        {
            get
            {
                if (!this.IsPrepaid)
                {
                    return 0;
                }
                else
                {
                    if (this.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
                    {
                        int prepMon = 0;
                        foreach (SingleDisbursement sdb in this.ActualDisbursementsList)
                        {
                            if (sdb.PaidBy == E_DisbursementPaidByT.Borrower && sdb.DisbPaidDateType == E_DisbPaidDateType.AtClosing)
                            {
                                prepMon += sdb.CoveredMonths;
                            }
                        }

                        this.prepaidMonths = prepMon;
                    }
                }

                return this.prepaidMonths;
            }

            set
            {
                this.prepaidMonths = value;
            }
        }

        /// <summary>
        /// Gets or sets the string representation of PrepaidMonths.
        /// </summary>
        /// <value>String representation of the property.</value>
        public string PrepaidMonths_rep
        {
            get
            {
                return this.Converter.ToCountString(this.PrepaidMonths);
            }

            set
            {
                this.PrepaidMonths = this.Converter.ToCount(value);
            }
        }

        /// <summary>
        /// Gets the Prepaid Amount.
        /// </summary>
        /// <value>
        /// The amount of the housing expense to be prepaid at closing.
        /// </value>
        public virtual decimal PrepaidAmt
        {
            get
            {
                if (this.IsPrepaid)
                {
                    if (this.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.LoanValues)
                    {
                        return this.PrepaidMonths * this.MonthlyAmtTotal;
                    }
                    else
                    {
                        decimal amount = 0;
                        foreach (SingleDisbursement sdb in this.ActualDisbursementsList)
                        {
                            if (sdb.PaidBy == E_DisbursementPaidByT.Borrower && sdb.DisbPaidDateType == E_DisbPaidDateType.AtClosing)
                            {
                                amount += sdb.DisbursementAmt;
                            }
                        }

                        return amount;
                    }
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Gets the string rep of Prepaid Amount.
        /// </summary>
        /// <value>String representation of the property.</value>
        public string PrepaidAmt_rep
        {
            get
            {
                return this.Converter.ToMoneyStringRoundAwayFromZero(this.PrepaidAmt, FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the expense was escrowed at closing.
        /// </summary>
        /// <value>
        /// Whether or not the housing expense is to be included in the mandatory escrow impounds account.
        /// </value>
        [DataMember(Name = "hEscClosing")]
        public E_TriState IsEscrowedAtClosing
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Reserves Months value.
        /// If the field is locked, returns the stored value. Otherwise, it returns the calculated value.
        /// </summary>
        /// <value>
        /// The whole number of monthly payments of the housing expense that need to be collected at closing under 
        /// singe-item accounting to ensure that the escrow impounds account is adequately funded.
        /// </value>
        public int ReserveMonths
        {
            get
            {
                if (this.ReserveMonthsLckd)
                {
                    return this.reserveMonths;
                }
                else
                {
                    return this.GetReserveMonths();
                }
            }

            set
            {
                this.reserveMonths = value;
            }
        }

        /// <summary>
        /// Gets the manually-set reserve months amount. 
        /// </summary>
        /// <value>The reserve months amount that has been manually set.</value>
        public int ReserveMonthsManual
        {
            get
            {
                return this.reserveMonths;
            }
        }

        /// <summary>
        /// Gets or sets the string rep of Reserves Months.
        /// </summary>
        /// <value>The string representation of the property.</value>
        public string ReserveMonths_rep
        {
            get
            {
                return this.Converter.ToCountString(this.ReserveMonths);
            }

            set
            {
                this.ReserveMonths = this.Converter.ToCount(value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the reserves months was set manually.
        /// </summary>
        /// <value>
        /// Whether or not the numbers of months of reserves is being set manually.
        /// </value>
        [DataMember(Name = "hResMnthLckd")]
        public bool ReserveMonthsLckd
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the Reserves Amount.
        /// </summary>
        /// <value>
        /// The total dollar amount being reserved under single-item accounting for the housing expense.
        /// </value>
        public virtual decimal ReserveAmt
        {
            get
            {
                if (this.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
                {
                    return this.ReserveMonths * Math.Round(this.MonthlyAmtServicing, 2, MidpointRounding.AwayFromZero);
                }
                else
                {
                    return this.ReserveMonths * Math.Round(this.MonthlyAmtTotal, 2, MidpointRounding.AwayFromZero);
                }
            }
        }

        /// <summary>
        /// Gets the string rep of Reserves Amount.
        /// </summary>
        /// <value>String representation of the property.</value>
        public string ReserveAmt_rep
        {
            get
            {
                return this.Converter.ToMoneyStringRoundAwayFromZero(this.ReserveAmt, FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// Gets or sets the Policy Activation Date.
        /// </summary>
        /// <value>
        /// The date that the insurance policy first goes into effect.
        /// </value>
        [DataMember(Name = "hPolicyActD")]
        public DateTime PolicyActivationD
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the string rep of PolicyActivationD.
        /// </summary>
        /// <value>String representation of the property.</value>
        public string PolicyActivationD_rep
        {
            get
            {
                return this.Converter.ToDateTimeString(this.PolicyActivationD);
            }

            set
            {
                this.PolicyActivationD = this.ToDate(value);
            }
        }

        /// <summary>
        /// Gets or sets the Paid To value.
        /// </summary>
        /// <value>
        /// The entity to which the disbursement needs to be made.
        /// </value>
        [DataMember(Name = "hPaidTo")]
        public E_AgentRoleT PaidTo
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Disbursement Repetition Interval.
        /// </summary>
        /// <value>
        /// When generating new disbursements from existing disbursements, the interval of time to be added to the 
        /// old disbursement date to get the new disbursement date.
        /// </value>
        [DataMember(Name = "hDisbRepI")]
        public E_DisbursementRepIntervalT DisbursementRepInterval
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the line number for the custom expense.
        /// </summary>
        /// <value>The line number for the custom expense.</value>
        public virtual E_CustomExpenseLineNumberT CustomExpenseLineNum
        {
            get
            {
                if (!this.CanHaveCustomLineNum && this.HousingExpenseType != E_HousingExpenseTypeT.Unassigned)
                {
                    this.customExpenseLinNum = E_CustomExpenseLineNumberT.None;
                }

                return this.customExpenseLinNum;
            }

            protected set
            {
                if (!this.CanHaveCustomLineNum && this.HousingExpenseType != E_HousingExpenseTypeT.Unassigned)
                {
                    // No-op. These expenses can't have their line number changed.
                }
                else
                {
                    this.customExpenseLinNum = value;
                }
            }
        }

        /// <summary>
        /// Gets the line number as a string.
        /// </summary>
        /// <value>The line number as a string.</value>
        public virtual string LineNumAsString
        {
            get
            {
                if (this.CustomExpenseLineNum == E_CustomExpenseLineNumberT.Line1008)
                {
                    return "1008";
                }
                else if (this.CustomExpenseLineNum == E_CustomExpenseLineNumberT.Line1009)
                {
                    return "1009";
                }
                else if (this.CustomExpenseLineNum == E_CustomExpenseLineNumberT.Line1010)
                {
                    return "1010";
                }
                else if (this.CustomExpenseLineNum == E_CustomExpenseLineNumberT.Line1011)
                {
                    return "1011";
                }
                else
                {
                    return string.Empty;
                }
            }
        }
        
        /// <summary>
        /// Gets a list containing the Actual Disbursements.
        /// </summary>
        /// <value>A list of Actual Disbursements.</value>
        public List<SingleDisbursement> ActualDisbursementsList
        {
            get
            {
                return this.ActualDisbursements.Values.ToList();
            }
        }

        /// <summary>
        /// Gets a list of Actual Disbursements that fall in the initial year.
        /// </summary>
        /// <value>The list of Actual Disbursements that fall in the initial year.</value>
        public List<SingleDisbursement> ActualDisbursementsInOneYear
        {
            get
            {
                int[] output;
                return this.GetActualDisbInYear(out output);
            }
        }

        /// <summary>
        /// Gets a list of projected disbursements for the initial year.
        /// </summary>
        /// <value>
        /// The list of projected disbursements for the initial year.
        /// </value>
        public List<SingleDisbursement> ProjectedDisbursements
        {
            get
            {
                return this.PopulateProjectedDisbs();
            }
        }

        /// <summary>
        /// Gets the default escrow schedule for this expense.
        /// </summary>
        /// <value>The default escrow schedule.</value>
        public virtual int[] DefaultDisbSchedule
        {
            get
            {
                int[] defaultSched = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                return defaultSched;
            }
        }

        /// <summary>
        /// Gets the maximum escrow cushion for this expense, in months.
        /// </summary>
        /// <value>The escrow cushion for this expense.</value>
        public int CushionMonths
        {
            get
            {
                return this.DisbursementScheduleMonths[0];
            }
        }

        /// <summary>
        /// Gets the string representation of the maximum escrow cushion for this expense.
        /// </summary>
        /// <value>The string representation of the escrow cushion.</value>
        public string CushionMonths_rep
        {
            get
            {
                return this.Converter.ToCountString(this.CushionMonths);
            }
        }

        /// <summary>
        /// Gets or sets the disbursement monthly schedule.
        /// </summary>
        /// <value>The disbursement monthly schedule.</value>
        public int[] DisbursementScheduleMonths
        {
            get
            {
                if (this.disbursementScheduleMonths == null || this.disbursementScheduleMonths.Length != 13)
                {
                    this.disbursementScheduleMonths = this.DefaultDisbSchedule;
                }

                if (this.DisbursementRepInterval == E_DisbursementRepIntervalT.Monthly)
                {
                    int[] oneSched = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
                    oneSched.CopyTo(this.disbursementScheduleMonths, 1);
                }
                else if (this.DisbursementRepInterval == E_DisbursementRepIntervalT.AnnuallyInClosingMonth)
                {
                    int[] scheduleWithAllPaymentsInClosingMonth = new int[12];
                    int closingMonth = this.DataLoan.sClosingDateMonth;
                    scheduleWithAllPaymentsInClosingMonth[closingMonth - 1] = 12;
                    scheduleWithAllPaymentsInClosingMonth.CopyTo(this.disbursementScheduleMonths, 1);
                }

                return this.disbursementScheduleMonths;
            }

            set
            {
                if (value.Length < 13)
                {
                    int[] sched = new int[13];
                    value.CopyTo(sched, 0);
                    this.disbursementScheduleMonths = sched;
                }
                else
                {
                    this.disbursementScheduleMonths = value;
                }
            }
        }

        /// <summary>
        /// Gets the first disbursement date.
        /// </summary>
        /// <value>The first disbursement date.</value>
        public DateTime FirstDisbDate
        {
            get
            {
                SingleDisbursement first = this.FirstDisbursement;

                return first == null ? DateTime.MaxValue : first.DueDate;
            }
        }

        /// <summary>
        /// Gets the string representation of the first disbursement date.
        /// </summary>
        /// <value>The string representation of the field.</value>
        public string FirstDisbDate_rep
        {
            get
            {
                return this.Converter.ToDateTimeString(this.FirstDisbDate);
            }
        }

        /// <summary>
        /// Gets the first disbursement associated with the housing expense.
        /// </summary>
        /// <value>The first disbursement.</value>
        public SingleDisbursement FirstDisbursement
        {
            get
            {
                ICollection<SingleDisbursement> disbList;
                if (this.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.LoanValues)
                {
                    disbList = this.ProjectedDisbursements;
                }
                else
                {
                    disbList = this.ActualDisbursementsList;
                }

                SingleDisbursement first = new SingleDisbursement();
                first.DueDate = DateTime.MaxValue;

                foreach (SingleDisbursement disb in disbList)
                {
                    CDateTime date = CDateTime.Create(disb.DueDate);
                    if (date.IsValid && disb.DueDate < first.DueDate)
                    {
                        first = disb;
                    }
                }

                return first.DueDate < DateTime.MaxValue ? first : null;
            }
        }

        /// <summary>
        /// Gets the converter to use.
        /// </summary>
        /// <value>The converter to use.</value>
        internal LosConvert Converter
        {
            get
            {
                if (this.losConverter == null)
                {
                    this.losConverter = this.DataLoan.m_convertLos;
                }

                return this.losConverter;
            }
        }

        /// <summary>
        /// Gets or sets the loan file associated with this expense.
        /// </summary>
        /// <value>The loan file associated with this expense.</value>
        internal CPageBase DataLoan
        {
            get;
            set;
        }
        
        /// <summary>
        /// Gets a dictionary of disbursements that have been made for this expense.
        /// </summary>
        /// <value>
        /// A dictionary with Guid keys of manually entered disbursements.
        /// </value>
        internal Dictionary<Guid, SingleDisbursement> ActualDisbursements
        {
            get
            {
                if (this.actualDisbursementList == null)
                {
                    this.actualDisbursementList = new Dictionary<Guid, SingleDisbursement>();
                }

                return this.actualDisbursementList;
            }
        }

        /// <summary>
        /// Clears the disbursements from the expense.
        /// </summary>
        public void ClearDisbursements()
        {
            this.ActualDisbursements.Clear();
        }

        /// <summary>
        /// Sets the line number. This is only for custom expenses.
        /// </summary>
        /// <param name="lineNum">The line number to set the expense to.</param>
        public virtual void SetCustomLineNumber(E_CustomExpenseLineNumberT lineNum)
        {
            return;
        }

        /// <summary>
        /// Gets the closing cost fee type id of the fee associated with the expense, within the given integrated disclosure section.
        /// Section F ($$prepaids$$) corresponds to HUD 900. Section G (initial escrows) corresponds to HUD 1000.
        /// </summary>
        /// <param name="integratedDisclosureSection">The integrated disclosure section containing the fee associated with the expense.</param>
        /// <returns>The <see cref="DefaultSystemClosingCostFee" /> type id, or Guid.Empty if not found.</returns>
        public Guid GetClosingCostFee900Or1000Type(E_IntegratedDisclosureSectionT integratedDisclosureSection)
        {
            bool escrows = false;

            if (integratedDisclosureSection == E_IntegratedDisclosureSectionT.SectionG)
            {
                escrows = true;
            }
            else if (integratedDisclosureSection != E_IntegratedDisclosureSectionT.SectionF)
            {
                return Guid.Empty;
            }

            if (this.HousingExpenseType == E_HousingExpenseTypeT.CondoHO6Insurance)
            {
                return escrows ? DefaultSystemClosingCostFee.Hud1000CondoHo6InsuranceFeeTypeId : DefaultSystemClosingCostFee.Hud900CondoInsuranceFeeTypeId;
            }
            else if (this.HousingExpenseType == E_HousingExpenseTypeT.FloodInsurance)
            {
                return escrows ? DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId : DefaultSystemClosingCostFee.Hud900FloodInsuranceFeeTypeId;
            }
            else if (this.HousingExpenseType == E_HousingExpenseTypeT.GroundRent)
            {
                return escrows ? DefaultSystemClosingCostFee.Hud1000GroundRentFeeTypeId : DefaultSystemClosingCostFee.Hud900GroundRentFeeTypeId;
            }
            else if (this.HousingExpenseType == E_HousingExpenseTypeT.HazardInsurance)
            {
                return escrows ? DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId : DefaultSystemClosingCostFee.Hud900HazardInsuranceFeeTypeId;
            }
            else if (this.HousingExpenseType == E_HousingExpenseTypeT.HomeownersAsscDues)
            {
                return escrows ? DefaultSystemClosingCostFee.Hud1000HomeOwnerAssociationDuesFeeTypeId : DefaultSystemClosingCostFee.Hud900HOADuesFeeTypeId;
            }
            else if (this.HousingExpenseType == E_HousingExpenseTypeT.OtherTaxes1)
            {
                return escrows ? DefaultSystemClosingCostFee.Hud1000OtherTax1FeeTypeId : DefaultSystemClosingCostFee.Hud900OtherTax1FeeTypeId;
            }
            else if (this.HousingExpenseType == E_HousingExpenseTypeT.OtherTaxes2)
            {
                return escrows ? DefaultSystemClosingCostFee.Hud1000OtherTax2FeeTypeId : DefaultSystemClosingCostFee.Hud900OtherTax2FeeTypeId;
            }
            else if (this.HousingExpenseType == E_HousingExpenseTypeT.OtherTaxes3)
            {
                return escrows ? DefaultSystemClosingCostFee.Hud1000OtherTax3FeeTypeId : DefaultSystemClosingCostFee.Hud900OtherTax3FeeTypeId;
            }
            else if (this.HousingExpenseType == E_HousingExpenseTypeT.OtherTaxes4)
            {
                return escrows ? DefaultSystemClosingCostFee.Hud1000OtherTax4FeeTypeId : DefaultSystemClosingCostFee.Hud900OtherTax4FeeTypeId;
            }
            else if (this.HousingExpenseType == E_HousingExpenseTypeT.RealEstateTaxes)
            {
                return escrows ? DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId : DefaultSystemClosingCostFee.Hud900PropertyTaxFeeTypeId;
            }
            else if (this.HousingExpenseType == E_HousingExpenseTypeT.SchoolTaxes)
            {
                return escrows ? DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId : DefaultSystemClosingCostFee.Hud900SchoolTaxFeeTypeId;
            }
            else if (this.HousingExpenseType == E_HousingExpenseTypeT.Unassigned)
            {
                switch (this.CustomExpenseLineNum)
                {
                    case E_CustomExpenseLineNumberT.Any:
                        return Guid.Empty;
                    case E_CustomExpenseLineNumberT.Line1008:
                        return escrows ? DefaultSystemClosingCostFee.Hud1000Custom1FeeTypeId : DefaultSystemClosingCostFee.Hud900OtherExp1FeeTypeId;
                    case E_CustomExpenseLineNumberT.Line1009:
                        return escrows ? DefaultSystemClosingCostFee.Hud1000Custom2FeeTypeId : DefaultSystemClosingCostFee.Hud900OtherExp2FeeTypeId;
                    case E_CustomExpenseLineNumberT.Line1010:
                        return escrows ? DefaultSystemClosingCostFee.Hud1000Custom3FeeTypeId : DefaultSystemClosingCostFee.Hud900OtherExp3FeeTypeId;
                    case E_CustomExpenseLineNumberT.Line1011:
                        return escrows ? DefaultSystemClosingCostFee.Hud1000Custom4FeeTypeId : DefaultSystemClosingCostFee.Hud900OtherExp4FeeTypeId;
                    case E_CustomExpenseLineNumberT.None:
                        return Guid.Empty;
                    default:
                        throw new UnhandledEnumException(this.CustomExpenseLineNum);
                }
            }
            else if (this.HousingExpenseType == E_HousingExpenseTypeT.WindstormInsurance)
            {
                return escrows ? DefaultSystemClosingCostFee.Hud1000WindStormInsuranceFeeTypeId : DefaultSystemClosingCostFee.Hud900WindstormInsuranceFeeTypeId;
            }

            return Guid.Empty;
        }

        /// <summary>
        /// Gets the closing cost fee associated with the expense, within the given integrated disclosure section.
        /// Section F ($$prepaids$$) corresponds to HUD 900. Section G (initial escrows) corresponds to HUD 1000.
        /// </summary>
        /// <param name="integratedDisclosureSection">The integrated disclosure section containing the fee associated with the expense.</param>
        /// <returns>The closing cost fee associated with the expense, or null if there is no such fee.</returns>
        public BorrowerClosingCostFee GetClosingCostFee(E_IntegratedDisclosureSectionT integratedDisclosureSection)
        {
            if (this.DataLoan == null)
            {
                return null;
            }

            Guid feeTypeID = this.GetClosingCostFee900Or1000Type(integratedDisclosureSection);
            Func<BaseClosingCostFee, bool> feeFilter;
            if (this.DataLoan.sClosingCostSet.HasDataLoanAssociate)
            {
                feeFilter = fee => ClosingCostSetUtils.OrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostFee)fee, this.DataLoan.sOriginatorCompensationPaymentSourceT, this.DataLoan.sDisclosureRegulationT, this.DataLoan.sGfeIsTPOTransaction) &&
                                   fee.ClosingCostFeeTypeId == feeTypeID;
            }
            else
            {
                feeFilter = fee => ClosingCostSetUtils.ExOrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostFee)fee) &&
                                   fee.ClosingCostFeeTypeId == feeTypeID;
            }

            return feeTypeID == Guid.Empty ? null : (BorrowerClosingCostFee)this.DataLoan.sClosingCostSet.GetFees(feeFilter).FirstOrDefault();
        }

        /// <summary>
        /// Adds a SingleDisbursement to the actual disbursement list.
        /// Links this expense to the Disbursement for calculation purposes.
        /// </summary>
        /// <param name="disb">The disbursement to add.</param>
        /// <returns>True if added, false if not.</returns>
        public bool AddDisbursement(SingleDisbursement disb)
        {
            if (disb.DisbursementType != E_DisbursementTypeT.Actual)
            {
                return false;
            }

            disb.ParentExpense = this;
            Guid disbursementId = disb.DisbId;
            this.ActualDisbursements.Add(disbursementId, disb);
            return true;
        }

        /// <summary>
        /// Removes an actual disbursement by Id.
        /// </summary>
        /// <param name="id">The disbursement's id.</param>
        /// <returns>True if removed, false if not found.</returns>
        public bool RemoveDisbursement(Guid id)
        {
            if (this.ActualDisbursements.ContainsKey(id))
            {
                this.ActualDisbursements.Remove(id);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets the SingleDisbursement by unique Id.
        /// </summary>
        /// <param name="id">The Id of the disbursement.</param>
        /// <returns>The disbursement if found, null if not.</returns>
        public SingleDisbursement GetDisbursementById(Guid id)
        {
            if (this.ActualDisbursements.ContainsKey(id))
            {
                return this.ActualDisbursements[id];
            }

            return null;
        }

        /// <summary>
        /// Calculates the start date for projected disbursements.
        /// </summary>
        /// <returns>Returns 1st scheduled date if it exists, opened date + 3 months if not.</returns>
        public DateTime GetStartDateWindow()
        {
            DateTime startDateWindow;
            if (!this.DataLoan.sSchedDueD1.IsValid)
            {
                var oldMonth = this.DataLoan.sOpenedD.DateTimeForComputation.Month;
                var year = this.DataLoan.sOpenedD.DateTimeForComputation.Year;
                var newMonth = (oldMonth + 3) % 12;

                if (newMonth == 0)
                {
                    newMonth = 12;
                }
                else if (newMonth < 4)
                {
                    year++;
                }

                startDateWindow = new DateTime(year, newMonth, 1);
            }
            else
            {
                startDateWindow = this.DataLoan.sSchedDueD1.DateTimeForComputation;
            }

            return startDateWindow;
        }

        /// <summary>
        /// Calculates the PBC, annual aggregate total and the monthly servicing (to prevent doubling up on calculation).
        /// </summary>
        /// <param name="disbursementMonthlyTotal">Will be populated with the total disbursement amount per month for the expense.</param>
        /// <param name="annualTotal">The aggregate annual total.</param>
        /// <param name="monthlyTotal">The monthly amount based on the aggregate annual total AKA the monthly servicing amount.</param>
        /// <returns>The PBC value.</returns>
        public decimal CalcPBC_Annual_Monthly(ref decimal[] disbursementMonthlyTotal, out decimal annualTotal, out decimal monthlyTotal)
        {
            List<SingleDisbursement> escImpDisbs;
            List<SingleDisbursement> nonEscImpDisbs;

            decimal paidAtClosing = 0;
            annualTotal = this.AggregateAnnualTotal(out escImpDisbs, out nonEscImpDisbs);
            monthlyTotal = annualTotal / 12;

            if (monthlyTotal == 0)
            {
                return 0;
            }

            foreach (SingleDisbursement disb in escImpDisbs)
            {
                int month = disb.DueDate.Month;
                disbursementMonthlyTotal[month] += disb.DisbursementAmt;
            }

            if (this.DataLoan.sAggEscrowCalcModeT == E_AggregateEscrowCalculationModeT.Customary)
            {
                foreach (SingleDisbursement disb in nonEscImpDisbs)
                {
                    int month = disb.DueDate.Month;
                    disbursementMonthlyTotal[month] += disb.DisbursementAmt;
                    paidAtClosing += disb.DisbursementAmt;
                }
            }

            return paidAtClosing;
        }

        /// <summary>
        /// Returns just the adjusted recommended reserve months.
        /// </summary>
        /// <returns>The adjusted recommended reserve months.</returns>
        public int CalcNewRecRsrvMon()
        {
            int temp1;
            decimal[] temp2;
            decimal temp3;
            decimal customaryDeductionFromAggregate;
            return this.CalcNewRecRsrvMon(this.DisbursementScheduleMonths[0], out temp1, out temp2, out temp3, out customaryDeductionFromAggregate);
        }

        /// <summary>
        /// Calculates the recommended reserve months.
        /// </summary>
        /// <param name="cushionMon">The cushion value.</param>
        /// <param name="unadjustedRsrvMon">The unadjusted recommended month.</param>
        /// <param name="monDisbTotal">A list of disbursement totals by month.</param>
        /// <param name="expMonTot">The expense's monthly total.</param>
        /// <param name="customaryDeductionFromAggregate">The deduction from aggregate adjustment for certain customary modes.</param>
        /// <returns>The adjusted recommended reserve month value.</returns>
        public int CalcNewRecRsrvMon(decimal cushionMon, out int unadjustedRsrvMon, out decimal[] monDisbTotal, out decimal expMonTot, out decimal customaryDeductionFromAggregate)
        {
            // The deduction from aggregate adjustment due to Customary/Customary2 mode with EscrowImpoundsCalcMinT Cushion_AdjustAggregate or Zero_AdjustAggregate
            // We bump up the reserves at closing but offset the aggregate adjustment to keep the cash collected at closing the same.
            customaryDeductionFromAggregate = 0;

            // If we're in Legacy calculations, then use the Legacy calculations for calculating recommended reserve months.
            if (this.DataLoan != null && this.DataLoan.sAggEscrowCalcModeT == E_AggregateEscrowCalculationModeT.Legacy)
            {
                monDisbTotal = new decimal[13];
                expMonTot = 0;

                if (this.DataLoan.sSchedDueD1.IsValid && this.MonthlyAmtTotal != 0)
                {
                    int initialMonth = this.DataLoan.sSchedDueD1.DateTimeForComputation.Month - 1;
                    int result = 0;
                    int runningBalance = 0;

                    for (int offset = 0; offset < 12; offset++)
                    {
                        int currentMonth = ((initialMonth + offset) % 12) + 1;
                        runningBalance += (-1 * this.DisbursementScheduleMonths[currentMonth]) + 1;
                        if (runningBalance < result)
                        {
                            result = runningBalance;
                        }
                    }

                    var calcMonth = (-1 * result) + this.DisbursementScheduleMonths[0];
                    unadjustedRsrvMon = calcMonth;

                    return calcMonth;
                }
                else
                {
                    unadjustedRsrvMon = 0;
                    return 0;
                }
            }

            // Start at index 1.
            decimal[] disbursementMonthlyTotal = new decimal[13];

            // Total amount paid at closing.
            decimal paidAtClosing = 0;

            // The aggregate annual total.
            decimal annualTotal = 0;

            // The monthly servicing amount. Recalculated to prevent recalculating a more expensive calculation.
            decimal monthlyTotal = 0;

            // Step 1: Gather disbursements and calculate annual and monthly and PBC.
            paidAtClosing = this.CalcPBC_Annual_Monthly(ref disbursementMonthlyTotal, out annualTotal, out monthlyTotal);
            if (monthlyTotal == 0)
            {
                unadjustedRsrvMon = 0;
                monDisbTotal = disbursementMonthlyTotal;
                expMonTot = monthlyTotal;
                return 0;
            }

            // Step 2 and 3: Normalize and Single-Item Analysis
            decimal singleItemRunningAmt = 0;
            decimal minSingleItem = decimal.MaxValue;

            DateTime startDate = this.GetStartDateWindow();
            DateTime endDate = startDate.AddYears(1).AddDays(-1);

            DateTime trackDate = startDate;
            while (trackDate <= endDate)
            {
                int mon = trackDate.Month;

                decimal normalizedMonth = disbursementMonthlyTotal[mon] / monthlyTotal;

                // Single-Item Analysis
                singleItemRunningAmt = singleItemRunningAmt + 1 - normalizedMonth;

                if (singleItemRunningAmt <= minSingleItem)
                {
                    minSingleItem = singleItemRunningAmt;
                }

                trackDate = trackDate.AddMonths(1);
            }

            decimal calcRsrvMon = -minSingleItem + cushionMon;

            unadjustedRsrvMon = (int)Math.Round(calcRsrvMon, MidpointRounding.AwayFromZero);

            if (this.DataLoan.sAggEscrowCalcModeT == E_AggregateEscrowCalculationModeT.Customary || this.DataLoan.sAggEscrowCalcModeT == E_AggregateEscrowCalculationModeT.Customary2)
            {
                decimal min = 0;
                if (this.DataLoan.sCustomaryEscrowImpoundsCalcMinT == E_CustomaryEscrowImpoundsCalcMinT.Cushion
                    || this.DataLoan.sCustomaryEscrowImpoundsCalcMinT == E_CustomaryEscrowImpoundsCalcMinT.Cushion_AdjustAggregate)
                {
                    min = cushionMon;
                }

                decimal pbcMonths = paidAtClosing / monthlyTotal;
                calcRsrvMon -= pbcMonths;

                if (this.DataLoan.sCustomaryEscrowImpoundsCalcMinT != E_CustomaryEscrowImpoundsCalcMinT.NoMin
                    && (calcRsrvMon < min))
                {
                    ////When we increase the reserves in Cushion_AdjustAggregate or Zero_AdjustAggregate, we want to decrease the aggregate adjustment accordingly.
                    if (this.DataLoan.sCustomaryEscrowImpoundsCalcMinT == E_CustomaryEscrowImpoundsCalcMinT.Cushion_AdjustAggregate
                        || this.DataLoan.sCustomaryEscrowImpoundsCalcMinT == E_CustomaryEscrowImpoundsCalcMinT.Zero_AdjustAggregate)
                    {
                        customaryDeductionFromAggregate = Math.Round(monthlyTotal, 2, MidpointRounding.AwayFromZero) * ((int)Math.Round(calcRsrvMon, MidpointRounding.AwayFromZero) - min);
                    }

                    calcRsrvMon = min;
                }
            }

            expMonTot = monthlyTotal;
            monDisbTotal = disbursementMonthlyTotal;
            return (int)Math.Round(calcRsrvMon, MidpointRounding.AwayFromZero);
        }

        /// <summary>
        /// Gets the Monthly Amount Total if it is manually entered. Don't use this unless you know what you're doing.
        /// </summary>
        /// <returns>Returns the monthly amount total.</returns>
        public virtual decimal GetMonthlyAmtTotal()
        {
            return this.MonthlyAmtFixedAmt + (this.AnnualAmtCalcBasePerc * this.AnnualAmtCalcBaseAmt / 1200.00M);
        }
        
        /// <summary>
        /// Gets the disbursement total depending on Escrow Impounds Calculation type.
        /// </summary>
        /// <param name="escImpDisbs">The disbursements paid by Escrow/Impounds.</param>
        /// <param name="nonEscImpDisbs">The other disbursements.</param>
        /// <returns>The total of the disbursement amounts.</returns>
        internal decimal AggregateAnnualTotal(out List<SingleDisbursement> escImpDisbs, out List<SingleDisbursement> nonEscImpDisbs)
        {
            decimal total = 0;
            List<SingleDisbursement> escImp = new List<SingleDisbursement>();
            List<SingleDisbursement> nonEscImp = new List<SingleDisbursement>();

            if (this.IsEscrowedAtClosing != E_TriState.Yes)
            {
                escImpDisbs = escImp;
                nonEscImpDisbs = nonEscImp;
                return 0;
            }

            ICollection<SingleDisbursement> allD = null;
            if (this.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.LoanValues)
            {
                allD = this.ProjectedDisbursements.FindAll(o => o.DueDate >= this.GetStartDateWindow());
            }
            else
            {
                allD = this.ActualDisbursementsInOneYear;
            }

            E_AggregateEscrowCalculationModeT method = this.DataLoan.sAggEscrowCalcModeT;
            foreach (SingleDisbursement disb in allD)
            {
                if (method == E_AggregateEscrowCalculationModeT.Regulatory)
                {
                    if (disb.PaidBy == E_DisbursementPaidByT.EscrowImpounds)
                    {
                        total += disb.DisbursementAmt;
                    }
                }
                else
                {
                    total += disb.DisbursementAmt;
                }

                if (disb.PaidBy == E_DisbursementPaidByT.EscrowImpounds)
                {
                    escImp.Add(disb);
                }
                else
                {
                    nonEscImp.Add(disb);
                }
            }

            escImpDisbs = escImp;
            nonEscImpDisbs = nonEscImp;
            return total;
        }

        /// <summary>
        /// Gets the reserve months depending on type of expense. If not applicable, return 0.
        /// </summary>
        /// <returns>The calculated reserve months.</returns>
        protected virtual int GetReserveMonths()
        {
            if (this.IsEscrowedAtClosing == E_TriState.Yes && this.DataLoan != null && this.DataLoan.sSchedDueD1.IsValid)
            {
                return this.CalcNewRecRsrvMon();
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Populates the projected disbursements list.
        /// </summary>
        /// <returns>The projected disbursements list.</returns>
        protected virtual List<SingleDisbursement> PopulateProjectedDisbs()
        {
            DateTime startDateWindow = this.GetStartDateWindow();
            List<SingleDisbursement> projected = new List<SingleDisbursement>();
            
            DateTime prepaidDate = DateTime.MinValue;

            for (int i = 1; i <= 12; i++)
            {
                int escVal = this.DisbursementScheduleMonths[i];

                if (escVal != 0)
                {
                    if (this.IsPrepaid)
                    {
                        DateTime tempDate;
                        if (i < startDateWindow.Month)
                        {
                            tempDate = new DateTime(startDateWindow.Year, i, 1);
                        }
                        else
                        {
                            tempDate = new DateTime(startDateWindow.Year - 1, i, 1);
                        }

                        if (tempDate >= prepaidDate && tempDate < startDateWindow)
                        {
                            prepaidDate = tempDate;
                        }
                    }

                    SingleDisbursement sdb = new SingleDisbursement();
                    sdb.ParentExpense = this;
                    sdb.EscAccMonth = i;
                    sdb.DisbursementType = E_DisbursementTypeT.Projected;
                    sdb.CoveredMonths = escVal;
                    sdb.DisbPaidDateType = E_DisbPaidDateType.AfterClosing;

                    // Have to hit new year to get to right month
                    if (i < startDateWindow.Month
                        || (i == startDateWindow.Month && startDateWindow.Day > 1))
                    {
                        sdb.DueDate = new DateTime(startDateWindow.Year + 1, i, 1);
                    }
                    else
                    {
                        sdb.DueDate = new DateTime(startDateWindow.Year, i, 1);
                    }

                    sdb.DisbursementAmtLckd = false;

                    if (this.IsEscrowedAtClosing == E_TriState.Yes)
                    {
                        sdb.PaidBy = E_DisbursementPaidByT.EscrowImpounds;
                        sdb.IsPaidFromEscrowAcc = true;
                    }
                    else
                    {
                        sdb.PaidBy = E_DisbursementPaidByT.Borrower;
                        sdb.IsPaidFromEscrowAcc = false;
                    }

                    projected.Add(sdb);
                }
            }

            if (this.IsPrepaid && prepaidDate != DateTime.MinValue)
            {
                SingleDisbursement prepaidDisb = new SingleDisbursement();
                prepaidDisb.ParentExpense = this;
                prepaidDisb.DisbPaidDateType = E_DisbPaidDateType.AtClosing;
                prepaidDisb.DisbursementAmtLckd = true;
                prepaidDisb.DisbursementAmt = this.MonthlyAmtTotal * this.PrepaidMonths;
                prepaidDisb.CoveredMonths = this.PrepaidMonths;
                prepaidDisb.PaidBy = E_DisbursementPaidByT.Borrower;
                prepaidDisb.DueDate = prepaidDate;

                projected.Add(prepaidDisb);
            }

            return projected;
        }

        /// <summary>
        /// Initializes the properties common to all housing expenses.
        /// </summary>
        protected void CommonInitialize()
        {
            this.IsEscrowedAtClosing = E_TriState.No;
            this.IsPrepaid = false;
            this.ExpenseDescription = this.DefaultExpenseDescription;
        }

        /// <summary>
        /// Gets the DateTime of the input string.
        /// </summary>
        /// <param name="date">The string to parse.</param>
        /// <returns>Returns DateTime.MinValue if invalid, the DateTime if successful.</returns>
        private DateTime ToDate(string date)
        {
            if (string.IsNullOrEmpty(date))
            {
                return DateTime.MinValue;
            }

            CDateTime output = CDateTime.Create(date, this.Converter);
            if (output.IsValid)
            {
                return output.DateTimeForComputation;
            }

            return DateTime.MinValue;
        }

        /// <summary>
        /// Gets a list of disbursements that fall in the initial year.
        /// </summary>
        /// <param name="takenMonths">The months which have disbursements on them. Should only use index 1-12.</param>
        /// <returns>The list of disbursements.</returns>
        private List<SingleDisbursement> GetActualDisbInYear(out int[] takenMonths)
        {
            int[] usedMonths = new int[13];
            List<SingleDisbursement> annualActDisb = new List<SingleDisbursement>();
            DateTime startDate = this.GetStartDateWindow();
            DateTime endDate = startDate.AddYears(1).AddDays(-1);

            foreach (SingleDisbursement actual in this.ActualDisbursements.Values)
            {
                CDateTime dueDate = CDateTime.Create(actual.DueDate);
                if (dueDate.IsValid)
                {
                    if (dueDate.DateTimeForComputation >= startDate && dueDate.DateTimeForComputation <= endDate)
                    {
                        annualActDisb.Add(actual);
                        usedMonths[dueDate.DateTimeForComputation.Month]++;
                    }
                }
            }

            takenMonths = usedMonths;
            return annualActDisb;
        }

        /// <summary>
        /// Gets the percent base amount from loan object.
        /// </summary>
        /// <param name="percentBaseT">Percent base type.</param>
        /// <returns>Percent base amount from loan object.</returns>
        private decimal GetPercentBaseAmount(E_PercentBaseT percentBaseT)
        {
            if (this.DataLoan == null)
            {
                return 0;
            }

            switch (percentBaseT)
            {
                case E_PercentBaseT.LoanAmount: // Loan amount
                    return this.DataLoan.sLAmtCalc;
                case E_PercentBaseT.SalesPrice: // sPurchPrice
                    return this.DataLoan.sPurchPrice;
                case E_PercentBaseT.AppraisalValue: // Appraisal value
                    return this.DataLoan.sApprVal != 0 ? this.DataLoan.sApprVal : this.DataLoan.sPurchPrice; // OPM 126087. Appraised Value Should Never Return Zero for the Purpose of Closing Cost Calculations.
                case E_PercentBaseT.TotalLoanAmount:
                    return this.DataLoan.sFinalLAmt;
                case E_PercentBaseT.OriginalCost:
                    if (this.DataLoan.sIsConstructionLoan)
                    {
                        return this.DataLoan.sLotOrigC;
                    }
                    else if (this.DataLoan.sIsRefinancing)
                    {
                        return this.DataLoan.sSpOrigC;
                    }
                    else
                    {
                        return 0;
                    }

                case E_PercentBaseT.AverageOutstandingBalance:
                    return 100000;
                case E_PercentBaseT.AllYSP:
                    return 100000;
                case E_PercentBaseT.DecliningRenewalsMonthly:
                    return 100000;
                case E_PercentBaseT.DecliningRenewalsAnnually:
                    return 100000;
                default:
                    throw new UnhandledEnumException(percentBaseT);
            }
        }
    }
}
