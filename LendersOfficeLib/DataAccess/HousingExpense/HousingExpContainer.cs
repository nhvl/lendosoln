﻿// <copyright file="HousingExpContainer.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   12/22/2014 
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using LendersOffice.Common;

    /// <summary>
    /// Class containing the housing expenses.
    /// </summary>
    [DataContract]
    public class HousingExpContainer
    {
        /// <summary>
        /// A dictionary of housing expenses that contains expenses with properly assigned types.
        /// Key is the HousingExpenseType.
        /// </summary>
        [DataMember(Name = "aHExp")]
        private Dictionary<E_HousingExpenseTypeT, BaseHousingExpense> assignedExpenses;

        /// <summary>
        /// A dictionary of housing expenses that contains custom expenses that do not have an assigned type.
        /// Key is the Custom Expense Line Number.
        /// Should only be used for old loans.
        /// New loans after new page is up will not be able to make expenses with no specific type.
        /// </summary>
        [DataMember(Name = "uHExp")]
        private Dictionary<E_CustomExpenseLineNumberT, BaseHousingExpense> unassignedCustomExpenses;

        /// <summary>
        /// The schedule for mortgage insurance.
        /// </summary>
        [DataMember(Name = "miSched")]
        private int[] mortIEscrowSchedule = new int[13];

        /// <summary>
        /// The upfront MI payment repetition interval.
        /// </summary>
        [DataMember(Name = "miRep")]
        private E_DisbursementRepIntervalT mortIPaymentRepeat;

        /// <summary>
        /// Initializes a new instance of the <see cref="HousingExpContainer" /> class.
        /// </summary>
        public HousingExpContainer()
        {
            this.assignedExpenses = new Dictionary<E_HousingExpenseTypeT, BaseHousingExpense>();
            this.unassignedCustomExpenses = new Dictionary<E_CustomExpenseLineNumberT, BaseHousingExpense>();
            this.MIEscrowSchedule = this.DefaultDisbSchedule;
        }

        /// <summary>
        /// Gets the dictionary of expenses with assigned housing expense types.
        /// </summary>
        /// <value>The assigned expenses dictionary.</value>
        public Dictionary<E_HousingExpenseTypeT, BaseHousingExpense> AssignedExpenses
        {
            get
            {
                if (this.assignedExpenses == null)
                {
                    this.assignedExpenses = new Dictionary<E_HousingExpenseTypeT, BaseHousingExpense>();
                }

                return this.assignedExpenses;
            }
        }

        /// <summary>
        /// Gets the dictionary of expenses without assigned housing expense types.
        /// </summary>
        /// <value>The unassigned expenses dictionary.</value>
        public Dictionary<E_CustomExpenseLineNumberT, BaseHousingExpense> UnassignedCustomExpenses
        {
            get
            {
                if (this.unassignedCustomExpenses == null)
                {
                    this.unassignedCustomExpenses = new Dictionary<E_CustomExpenseLineNumberT, BaseHousingExpense>();
                }

                return this.unassignedCustomExpenses;
            }
        }

        /// <summary>
        /// Gets the default escrow schedule for mortgage insurance.
        /// </summary>
        /// <value>The default escrow schedule.</value>
        public virtual int[] DefaultDisbSchedule
        {
            get
            {
                int[] defaultSched = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
                return defaultSched;
            }
        }

        /// <summary>
        /// Gets or sets the MI payment repetition interval.
        /// </summary>
        /// <value>The MI repetition interval.</value>
        public E_DisbursementRepIntervalT MIPaymentRepeat
        {
            get
            {
                return this.mortIPaymentRepeat;
            }

            set
            {
                this.mortIPaymentRepeat = value;
            }
        }

        /// <summary>
        /// Gets or sets the escrow schedule for mortgage insurance.
        /// </summary>
        /// <value>The escrow schedule for mortgage insurance.</value>
        public int[] MIEscrowSchedule
        {
            get
            {
                // Want to correct the schedule before messing around with it.
                if (this.mortIEscrowSchedule == null || this.mortIEscrowSchedule.Length != 13)
                {
                    this.mortIEscrowSchedule = this.DefaultDisbSchedule;
                }

                if (this.MIPaymentRepeat == E_DisbursementRepIntervalT.Monthly)
                {
                    int[] oneSched = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
                    oneSched.CopyTo(this.mortIEscrowSchedule, 1);
                }
                else if (this.MIPaymentRepeat == E_DisbursementRepIntervalT.AnnuallyInClosingMonth && this.DataLoan != null)
                {
                    int[] scheduleWithAllPaymentsInClosingMonth = new int[12];
                    int closingMonth = this.DataLoan.sClosingDateMonth;
                    scheduleWithAllPaymentsInClosingMonth[closingMonth - 1] = 12;
                    scheduleWithAllPaymentsInClosingMonth.CopyTo(this.mortIEscrowSchedule, 1);
                }

                return this.mortIEscrowSchedule;
            }

            set
            {
                if (value.Length < 13)
                {
                    int[] sched = new int[13];
                    value.CopyTo(sched, 0);
                    this.mortIEscrowSchedule = sched;
                }
                else
                {
                    this.mortIEscrowSchedule = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the data loan.
        /// </summary>
        /// <value>The associated data loan.</value>
        internal CPageBase DataLoan
        {
            get;
            set;
        }

        /// <summary>
        /// Remove all the Housing Expenses.
        /// </summary>
        internal void ClearExpenses()
        {
            this.AssignedExpenses.Clear();
            this.UnassignedCustomExpenses.Clear();
        }
    }
}
