// <copyright file="HOADuesExpense.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   12/22/2014 
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using LendersOffice.Common;

    /// <summary>
    /// Class for HOA Dues Expenses.
    /// </summary>
    public class HOADuesExpense : BaseHousingExpense
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HOADuesExpense" /> class.
        /// </summary>
        public HOADuesExpense()
        {
            this.HousingExpenseType = E_HousingExpenseTypeT.HomeownersAsscDues;
            this.DisbursementScheduleMonths = this.DefaultDisbSchedule;
            this.DisbursementRepInterval = E_DisbursementRepIntervalT.Monthly;
            this.CommonInitialize();
            this.CustomExpenseLineNum = E_CustomExpenseLineNumberT.None;
        }

        /// <summary>
        /// Gets the expense's default description.
        /// </summary>
        /// <value>The default description.</value>
        public override string ExpensePrefix
        {
            get
            {
                return "HOA";
            }
        }

        /// <summary>
        /// Gets or sets the disbursement monthly schedule.
        /// </summary>
        /// <value>The disbursement monthly schedule.</value>
        public override int[] DefaultDisbSchedule
        {
            get
            {
                int[] defaultSched = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
                return defaultSched;
            }
        }

        /// <summary>
        /// Gets or sets the line number for the custom expense.
        /// </summary>
        /// <value>The line number for the custom expense.</value>
        public override E_CustomExpenseLineNumberT CustomExpenseLineNum
        {
            get
            {
                if (this.IsEscrowedAtClosing == E_TriState.Yes && base.CustomExpenseLineNum == E_CustomExpenseLineNumberT.None)
                {
                    base.CustomExpenseLineNum = E_CustomExpenseLineNumberT.Any;
                }

                return base.CustomExpenseLineNum;
            }

            protected set
            {
                base.CustomExpenseLineNum = value;
            }
        }

        /// <summary>
        /// Sets the line number for the custom expense.
        /// </summary>
        /// <param name="lineNum">The line number to set the expense to.</param>
        public override void SetCustomLineNumber(E_CustomExpenseLineNumberT lineNum)
        {
            this.CustomExpenseLineNum = lineNum;
        }
    }
}
