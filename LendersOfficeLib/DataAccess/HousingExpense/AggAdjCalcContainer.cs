﻿// <copyright file="AggAdjCalcContainer.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   6/18/2015
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Class containing all data per expense to calculate the aggregate adjustment.
    /// </summary>
    public class AggAdjCalcContainer
    {
        /// <summary>
        /// Gets or sets a value indicating whether the represented object is null.
        /// </summary>
        /// <value>Whether the represented object is null.</value>
        public bool IsNull
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the represented object's escrowed state.
        /// </summary>
        /// <value>The represented object's escrowed state.</value>
        public E_TriState IsEscrowed
        {
            get;
            set;
        }
        
        /// <summary>
        /// Gets or sets the represented object's disbursement schedule.
        /// </summary>
        /// <value>The represented object's disbursement schedule.</value>
        public int[] DisbursementScheduleMonths
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the represented object's per month disbursement amounts.
        /// </summary>
        /// <value>The represented object's per month disbursement amounts.</value>
        public decimal[] MonthlyDisbursementAmt
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the represented object's reserve months.
        /// </summary>
        /// <value>The represented object's reserve months.</value>
        public int ReserveMonths
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the represented object's monthly total.
        /// </summary>
        /// <value>The represented object's monthly total.</value>
        public decimal MonthlyTotal
        {
            get;
            set;
        }
    }
}
