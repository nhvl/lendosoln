// <copyright file="HazardExpense.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   12/22/2014 
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using LendersOffice.Common;

    /// <summary>
    /// Class for Hazard Insurance Expenses.
    /// </summary>
    public class HazardExpense : BaseHousingExpense
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HazardExpense" /> class.
        /// </summary>
        public HazardExpense()
        {
            this.HousingExpenseType = E_HousingExpenseTypeT.HazardInsurance;
            this.DisbursementScheduleMonths = this.DefaultDisbSchedule;
            this.CommonInitialize();
        }

        /// <summary>
        /// Gets the expense's default description.
        /// </summary>
        /// <value>The default description.</value>
        public override string ExpensePrefix
        {
            get
            {
                return "HAZ";
            }
        }

        /// <summary>
        /// Gets or sets the string representation of Monthly Amount Fixed.
        /// </summary>
        /// <value>String representation of the property.</value>
        public override string MonthlyAmtFixedAmt_rep
        {
            get
            {
                return this.Converter.ToMoneyString4DecimalDigits(this.MonthlyAmtFixedAmt, FormatDirection.ToRep);
            }

            set
            {
                base.MonthlyAmtFixedAmt_rep = value;
            }
        }

        /// <summary>
        /// Gets the default escrow schedule for this expense.
        /// </summary>
        /// <value>The default escrow schedule.</value>
        public override int[] DefaultDisbSchedule
        {
            get
            {
                int[] defaultSched = { 2, 0, 0, 0, 0, 0, 12, 0, 0, 0, 0, 0, 0 };
                return defaultSched;
            }
        }

        /// <summary>
        /// Gets the line number as a string.
        /// </summary>
        /// <value>The line number as a string.</value>
        public override string LineNumAsString
        {
            get
            {
                return "1002";
            }
        }
    }
}