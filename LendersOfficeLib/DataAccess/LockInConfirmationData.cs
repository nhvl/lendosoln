using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
	public class CLockInConfirmationData : CPageData
	{
		private static CSelectStatementProvider s_selectProvider;

        static CLockInConfirmationData() 
        {
            StringList list = new StringList();

            list.Add("sfGetAgentOfRole");
            list.Add("sDue");
            list.Add("sLAmtCalc");
            list.Add("sLDiscnt");
            list.Add("sLDiscntFMb");
            list.Add("sLDiscntPc");
            list.Add("sLOrigF");
            list.Add("sLOrigFMb");
            list.Add("sLOrigFPc");
            list.Add("sLT");
            list.Add("sLpTemplateNm");
            list.Add("sNoteIR");
            list.Add("sRLckdExpiredD");
            list.Add("sPreparerXmlContent");
            list.Add("sTerm");
            list.Add("sIsRateLocked");
            list.Add("sLpTemplateId");
            list.Add("sLPurposeT");


            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }

		public CLockInConfirmationData(Guid fileId) : base(fileId, "LockInConfirmationData", s_selectProvider)
		{
		}
	}
}
