﻿using System;

namespace DataAccess
{
    public class CQuickPricerLoanPoolData : CPageData
    {
        private static CSelectStatementProvider s_selectProvider;
        static CQuickPricerLoanPoolData()
        {
            StringList list = new StringList();
            #region Target Fields
            list.Add("aLiaCollection");
            list.Add("aAssetCollection");
            list.Add("aReCollection");
            list.Add("aBEmpCollection");
            list.Add("aCEmpCollection");
            list.Add("sAgentCollection");
            #endregion
            s_selectProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection);

        }

        public CQuickPricerLoanPoolData(Guid sLId)
            : base(sLId, "QuickPricerLoanPool", s_selectProvider)
        {
        }
        protected override bool m_enforceAccessControl
        {
            get
            {
                // This will allow QuickPricerLoanPoolManager to recycle any loan.
                return false;
            }
        }
    }
}