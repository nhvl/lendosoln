﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{
    class CGetLoanProgramsToRunLpeDataBypassAccessControl : CGetLoanProgramsToRunLpeData
    {
        public CGetLoanProgramsToRunLpeDataBypassAccessControl(Guid sLId)
            : base(sLId)
        {
        }

        protected override bool m_enforceAccessControl
        {
            get
            {
                return false;
            }
        }
    }
}
