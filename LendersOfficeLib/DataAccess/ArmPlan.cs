﻿// <copyright file="ArmPlan.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Matthew Flynn
//    Date:   7/19/2016
// </summary>
namespace DataAccess
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// <para>
    /// This is a simple POD class that represents an ARM plan,
    /// containing the code and description, as well as the data
    /// description of the plan's requirements.
    /// </para><para>
    /// Initally created for case 238055.
    /// </para>
    /// </summary>
    public class ArmPlan
    {
        /// <summary>
        /// Gets the description of this plan.
        /// </summary>
        /// <value>The description of this plan.</value>
        public string Description { get; private set; }

        /// <summary>
        /// Gets the code of this plan.
        /// </summary>
        /// <value>The code of this plan.</value>
        public string Code { get; private set; }
        
        /// <summary>
        /// Gets or sets a value indicating whether this is an active plan or not.
        /// </summary>
        /// <remarks>If a plan is not active it will not be evaluated or added
        /// in the UI, but it will be allowed for existing plan codes.
        /// </remarks>
        /// <value>Whether this is an active plan or not.</value>
        public bool IsActive { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this is a generic plan or not.
        /// </summary>
        /// <remarks>Being a generic plan means it should be evaluated after specific
        /// plans, but before the fallback plans.
        /// </remarks>
        /// <value>Whether this is a generic plan or not.</value>
        private bool IsGeneric { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this is a fall back plan or not.
        /// </summary>
        /// <remarks>Being a fall back plan, means it should be evaluated subsequently
        /// to the other plans. Typically these are catch-all plans.</remarks>
        /// <value>Whether this is a fall back plan or not.</value>
        private bool IsFallBack { get; set; }

        /// <summary>
        /// Gets or sets the Loan Type required by this plan.
        /// </summary>
        /// <value>The Loan Type required by this plan.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing element.")]
        private E_sLT? sLT { get; set; }

        /// <summary>
        /// Gets or sets the minimum Initial Fixed Rate Interest Period required by this plan.
        /// </summary>
        /// <value>The minimum Initial Fixed Rate Interest Period required by this plan.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing element.")]
        private int? sRAdj1stCapMonMin { get; set; }

        /// <summary>
        /// Gets or sets the maximum Initial Fixed Rate Interest Period required by this plan.
        /// </summary>
        /// <value>The maximum Initial Fixed Rate Interest Period required by this plan.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing element.")]
        private int? sRAdj1stCapMonMax { get; set; }

        /// <summary>
        /// Gets or sets the ARM Index required by this plan.
        /// </summary>
        /// <value>The ARM Index required by this plan.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing element.")]
        private E_sArmIndexT? sArmIndexT { get; set; }

        /// <summary>
        /// Gets or sets the Subsequent Change Limit required by this plan.
        /// </summary>
        /// <value>The Subsequent Change Limit required by this plan.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing element.")]
        private decimal? sRAdjCapR { get; set; }

        /// <summary>
        /// Gets or sets the Subsequent Interest Rate Adjustment Period required by this plan.
        /// </summary>
        /// <value>The Subsequent Interest Rate Adjustment Period required by this plan.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing element.")]
        private int? sRAdjCapMon { get; set; }

        /// <summary>
        /// Gets or sets the First Change Limit required by this plan.
        /// </summary>
        /// <value>The First Change Limit required by this plan.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing element.")]
        private decimal? sRAdj1stCapR { get; set; }

        /// <summary>
        /// Gets or sets the Life Interest Rate Cap required by this plan.
        /// </summary>
        /// <value>The Life Interest Rate Cap required by this plan.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing element.")]
        private decimal? sRAdjLifeCapR { get; set; }

        /// <summary>
        /// Gets or sets the Convertibility required by this plan.
        /// </summary>
        /// <value>The Convertibility required by this plan.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing element.")]
        private bool? sIsConvertibleMortgage { get; set; }

        /// <summary>
        /// List all applicable ARM Plans.
        /// </summary>
        /// <returns>All applicable ARM Plans.</returns>
        public static List<ArmPlan> GetArmPlanCodes()
        {
            List<ArmPlan> planCodes = new List<ArmPlan>();

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "FM GENERIC, 6 MONTH",
                    Code = "GEN06",
                    sLT = E_sLT.Conventional,
                    sRAdj1stCapMonMin = 4,
                    sRAdj1stCapMonMax = 8,
                    IsGeneric = true
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "FM GENERIC, 1 YR, 1% ANNUAL Cap",
                    Code = "GEN1A",
                    sLT = E_sLT.Conventional,
                    sRAdj1stCapMonMin = 9,
                    sRAdj1stCapMonMax = 18,
                    sRAdjCapR = 1m,
                    IsGeneric = true
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "FM GENERIC, 1 YR, 2% ANNUAL Cap",
                    Code = "GEN1B",
                    sLT = E_sLT.Conventional,
                    sRAdj1stCapMonMin = 9,
                    sRAdj1stCapMonMax = 18,
                    sRAdjCapR = 2m,
                    IsGeneric = true
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "FM GENERIC, 3 YR",
                    Code = "GEN3",
                    sLT = E_sLT.Conventional,
                    sRAdj1stCapMonMin = 19,
                    sRAdj1stCapMonMax = 42,
                    IsGeneric = true
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "FM GENERIC, 5 YR",
                    Code = "GEN5",
                    sLT = E_sLT.Conventional,
                    sRAdj1stCapMonMin = 43,
                    sRAdj1stCapMonMax = 66,
                    IsGeneric = true
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "FM GENERIC, 7 YR",
                    Code = "GEN7",
                    sLT = E_sLT.Conventional,
                    sRAdj1stCapMonMin = 67,
                    sRAdj1stCapMonMax = 90,
                    IsGeneric = true
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "FM GENERIC, 10 YR",
                    Code = "GEN10",
                    sLT = E_sLT.Conventional,
                    sRAdj1stCapMonMin = 91,
                    sRAdj1stCapMonMax = 150,
                    IsGeneric = true
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "760 - 6 MO COST OF FUNDS 1&5 Caps",
                    Code = "760",
                    sLT = E_sLT.Conventional,
                    sArmIndexT = E_sArmIndexT.EleventhDistrictCOF,
                    sRAdj1stCapMonMin = 4,
                    sRAdj1stCapMonMax = 8,
                    sRAdjCapMon = 6,
                    sRAdj1stCapR = 1m,
                    sRAdjCapR = 1m,
                    sRAdjLifeCapR = 5,
                    sIsConvertibleMortgage = false
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "761 - 6 MO COST OF FUNDS,CONVERTIBLE 1&5 Caps",
                    Code = "761",
                    sLT = E_sLT.Conventional,
                    sArmIndexT = E_sArmIndexT.EleventhDistrictCOF,
                    sRAdj1stCapMonMin = 4,
                    sRAdj1stCapMonMax = 8,
                    sRAdjCapMon = 6,
                    sRAdj1stCapR = 1m,
                    sRAdjCapR = 1m,
                    sRAdjLifeCapR = 5,
                    sIsConvertibleMortgage = true
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "57 - 1 YR TREASURY,CONVERTIBLE 2&6 Caps",
                    Code = "57",
                    sLT = E_sLT.Conventional,
                    sArmIndexT = E_sArmIndexT.WeeklyAvgCMT,
                    sRAdj1stCapMonMin = 0,
                    sRAdj1stCapMonMax = 18,
                    sRAdjCapMon = 12,
                    sRAdj1stCapR = 2m,
                    sRAdjCapR = 2m,
                    sRAdjLifeCapR = 6,
                    sIsConvertibleMortgage = true
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "681 - 1 YR COST OF FUNDS 2&5 Caps",
                    Code = "681",
                    sLT = E_sLT.Conventional,
                    sArmIndexT = E_sArmIndexT.EleventhDistrictCOF,
                    sRAdj1stCapMonMin = 0,
                    sRAdj1stCapMonMax = 18,
                    sRAdjCapMon = 12,
                    sRAdj1stCapR = 2m,
                    sRAdjCapR = 2m,
                    sRAdjLifeCapR = 5,
                    sIsConvertibleMortgage = false
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "682 - 1 YR COST OF FUNDS,CONVERTIBLE 2&5 Caps",
                    Code = "682",
                    sLT = E_sLT.Conventional,
                    sArmIndexT = E_sArmIndexT.EleventhDistrictCOF,
                    sRAdj1stCapMonMin = 0,
                    sRAdj1stCapMonMax = 18,
                    sRAdjCapMon = 12,
                    sRAdj1stCapR = 2m,
                    sRAdjCapR = 2m,
                    sRAdjLifeCapR = 5,
                    sIsConvertibleMortgage = true
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "710 - 1 YR TREASURY 1&6 Caps",
                    Code = "710",
                    sLT = E_sLT.Conventional,
                    sArmIndexT = E_sArmIndexT.WeeklyAvgCMT,
                    sRAdj1stCapMonMin = 0,
                    sRAdj1stCapMonMax = 18,
                    sRAdjCapMon = 12,
                    sRAdj1stCapR = 1m,
                    sRAdjCapR = 1m,
                    sRAdjLifeCapR = 6,
                    sIsConvertibleMortgage = false
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "720 - 1 YR TREASURY 2&6 Caps",
                    Code = "720",
                    sLT = E_sLT.Conventional,
                    sArmIndexT = E_sArmIndexT.WeeklyAvgCMT,
                    sRAdj1stCapMonMin = 0,
                    sRAdj1stCapMonMax = 18,
                    sRAdjCapMon = 12,
                    sRAdj1stCapR = 2m,
                    sRAdjCapR = 2m,
                    sRAdjLifeCapR = 6,
                    sIsConvertibleMortgage = false
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "721 - 1 YR TREASURY,CONVERTIBLE 2&6 Caps",
                    Code = "721",
                    sLT = E_sLT.Conventional,
                    sArmIndexT = E_sArmIndexT.WeeklyAvgCMT,
                    sRAdj1stCapMonMin = 0,
                    sRAdj1stCapMonMax = 18,
                    sRAdjCapMon = 12,
                    sRAdj1stCapR = 2m,
                    sRAdjCapR = 2m,
                    sRAdjLifeCapR = 6,
                    sIsConvertibleMortgage = true
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "861 - 1 YR TREASURY,CONVERTIBLE 1&6 Caps",
                    Code = "861",
                    sLT = E_sLT.Conventional,
                    sArmIndexT = E_sArmIndexT.WeeklyAvgCMT,
                    sRAdj1stCapMonMin = 0,
                    sRAdj1stCapMonMax = 18,
                    sRAdjCapMon = 12,
                    sRAdj1stCapR = 1m,
                    sRAdjCapR = 1m,
                    sRAdjLifeCapR = 6,
                    sIsConvertibleMortgage = true
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "651 - 3/1 YR TREASURY 2&6 Caps",
                    Code = "651",
                    sLT = E_sLT.Conventional,
                    sArmIndexT = E_sArmIndexT.WeeklyAvgCMT,
                    sRAdj1stCapMonMin = 19,
                    sRAdj1stCapMonMax = 42,
                    sRAdjCapMon = 12,
                    sRAdj1stCapR = 2m,
                    sRAdjCapR = 2m,
                    sRAdjLifeCapR = 6,
                    sIsConvertibleMortgage = false
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "652 - 3/1 YR TREASURY,CONVERTIBLE 2&6 Caps",
                    Code = "652",
                    sLT = E_sLT.Conventional,
                    sArmIndexT = E_sArmIndexT.WeeklyAvgCMT,
                    sRAdj1stCapMonMin = 19,
                    sRAdj1stCapMonMax = 42,
                    sRAdjCapMon = 12,
                    sRAdj1stCapR = 2m,
                    sRAdjCapR = 2m,
                    sRAdjLifeCapR = 6,
                    sIsConvertibleMortgage = true
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "649 - 3/3 YR TREASURY 2&6 Caps",
                    Code = "649",
                    sLT = E_sLT.Conventional,
                    sArmIndexT = E_sArmIndexT.WeeklyAvgCMT,
                    sRAdj1stCapMonMin = 30,
                    sRAdj1stCapMonMax = 42,
                    sRAdjCapMon = 36,
                    sRAdj1stCapR = 2m,
                    sRAdjCapR = 2m,
                    sRAdjLifeCapR = 6,
                    sIsConvertibleMortgage = false
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "650 - 3/3 YR TREASURY,CONVERTIBLE 2&6 Caps",
                    Code = "650",
                    sLT = E_sLT.Conventional,
                    sArmIndexT = E_sArmIndexT.WeeklyAvgCMT,
                    sRAdj1stCapMonMin = 30,
                    sRAdj1stCapMonMax = 42,
                    sRAdjCapMon = 36,
                    sRAdj1stCapR = 2m,
                    sRAdjCapR = 2m,
                    sRAdjLifeCapR = 6,
                    sIsConvertibleMortgage = true
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "660 - 5/1 YR TREASURY 2&5 Caps",
                    Code = "660",
                    sLT = E_sLT.Conventional,
                    sArmIndexT = E_sArmIndexT.WeeklyAvgCMT,
                    sRAdj1stCapMonMin = 43,
                    sRAdj1stCapMonMax = 66,
                    sRAdjCapMon = 12,
                    sRAdj1stCapR = 2m,
                    sRAdjCapR = 2m,
                    sRAdjLifeCapR = 5,
                    sIsConvertibleMortgage = false
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "661 - 5/1 YR TREASURY,CONVERTIBLE 2&5 Caps",
                    Code = "661",
                    sLT = E_sLT.Conventional,
                    sArmIndexT = E_sArmIndexT.WeeklyAvgCMT,
                    sRAdj1stCapMonMin = 43,
                    sRAdj1stCapMonMax = 66,
                    sRAdjCapMon = 12,
                    sRAdj1stCapR = 2m,
                    sRAdjCapR = 2m,
                    sRAdjLifeCapR = 5,
                    sIsConvertibleMortgage = true
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "3252 - 5/1 LIBOR 2&5 Caps",
                    Code = "3252",
                    sLT = E_sLT.Conventional,
                    sArmIndexT = E_sArmIndexT.WallStreetJournalLIBOR,
                    sRAdj1stCapMonMin = 54,
                    sRAdj1stCapMonMax = 62,
                    sRAdjCapMon = 12,
                    sRAdj1stCapR = 5m,
                    sRAdjCapR = 2m,
                    sRAdjLifeCapR = 5,
                    sIsConvertibleMortgage = false
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "750 - 7/1 YR TREASURY 2&5 Caps",
                    Code = "750",
                    sLT = E_sLT.Conventional,
                    sArmIndexT = E_sArmIndexT.WeeklyAvgCMT,
                    sRAdj1stCapMonMin = 67,
                    sRAdj1stCapMonMax = 90,
                    sRAdjCapMon = 12,
                    sRAdj1stCapR = 5m,
                    sRAdjCapR = 2m,
                    sRAdjLifeCapR = 5,
                    sIsConvertibleMortgage = false
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "751 - 7/1 YR TREASURY,CONVERTIBLE 2&5 Caps",
                    Code = "751",
                    sLT = E_sLT.Conventional,
                    sArmIndexT = E_sArmIndexT.WeeklyAvgCMT,
                    sRAdj1stCapMonMin = 67,
                    sRAdj1stCapMonMax = 90,
                    sRAdjCapMon = 12,
                    sRAdj1stCapR = 5m,
                    sRAdjCapR = 2m,
                    sRAdjLifeCapR = 5,
                    sIsConvertibleMortgage = true
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "1423 - 10/1 YR TREASURY 2&5 Caps",
                    Code = "1423",
                    sLT = E_sLT.Conventional,
                    sArmIndexT = E_sArmIndexT.WeeklyAvgCMT,
                    sRAdj1stCapMonMin = 91,
                    sRAdj1stCapMonMax = 150,
                    sRAdjCapMon = 12,
                    sRAdj1stCapR = 5m,
                    sRAdjCapR = 2m,
                    sRAdjLifeCapR = 5,
                    sIsConvertibleMortgage = false
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "1437 - 10/1 YR TREASURY,CONVERTIBLE 2&5 Caps",
                    Code = "1437",
                    sLT = E_sLT.Conventional,
                    sArmIndexT = E_sArmIndexT.WeeklyAvgCMT,
                    sRAdj1stCapMonMin = 91,
                    sRAdj1stCapMonMax = 150,
                    sRAdjCapMon = 12,
                    sRAdj1stCapR = 5m,
                    sRAdjCapR = 2m,
                    sRAdjLifeCapR = 5,
                    sIsConvertibleMortgage = true
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "251 - FHA ARM: 1 YR TREASURY 1&5 Caps",
                    Code = "251",
                    sLT = E_sLT.FHA,
                    sArmIndexT = E_sArmIndexT.WeeklyAvgCMT,
                    sRAdj1stCapMonMin = 0,
                    sRAdj1stCapMonMax = 18,
                    sRAdjCapMon = 12,
                    sRAdj1stCapR = 1m,
                    sRAdjCapR = 1m,
                    sRAdjLifeCapR = 5,
                    sIsConvertibleMortgage = false
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "LENDER ARM PLAN",
                    Code = "!FNMA",
                    IsGeneric = true,
                    IsFallBack = true
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "FHA HYBRID ARM",
                    Code = "FHAHY",
                    sLT = E_sLT.FHA,
                    IsGeneric = true
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "VA HYBRID ARM",
                    Code = "VAARM",
                    sLT = E_sLT.VA,
                    sRAdj1stCapMonMin = 19,
                    sRAdj1stCapMonMax = 150,
                    IsGeneric = true
                });

            planCodes.Add(
                new ArmPlan()
                {
                    Description = "VA 1 YR ARM",
                    Code = "VA1YR",
                    sLT = E_sLT.VA,
                    sRAdj1stCapMonMin = 0,
                    sRAdj1stCapMonMax = 18,
                    IsGeneric = true
                });

            planCodes.ForEach(plan => plan.IsActive = true);

            // Legacy plans.
            // These are plan codes that we once offered, but no longer. 
            // Still need them so we can map the description of old codes,
            // but they are not used in evaluation.
            planCodes.Add(new ArmPlan() { Description = "FM GENERIC, NEGATIVE AMORTIZATION", Code = "NGAM", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "346- 6 MO TREASURY  1&5 Caps", Code = "346", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "746- 6 MO TREASURY  1&6 Caps", Code = "746", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "1030- 6 MO CERTIFICATE OF DEPOSIT,CONVERTIBLE  1&6 Caps", Code = "1030", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "1031- 6 MO CERTIFICATE OF DEPOSIT  1&6 Caps", Code = "1031", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "1443- 6 MO WSJ LIBOR  1&5 Caps", Code = "1443", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "1444- 6 MO WSJ LIBOR,CONVERTIBLE   1&5 Caps", Code = "1444", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "1445- 6 MO WSJ LIBOR  1&6 Caps", Code = "1445", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "1446- 6 MO WSJ LIBOR,CONVERTIBLE   1&6 Caps", Code = "1446", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "383- 1 YR TREASURY   1&4 Caps", Code = "383", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "421- 1 YR TREASURY   2&4 Caps", Code = "421", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "510- 1 YR TREASURY   1&5 Caps", Code = "510", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "511- 1 YR TREASURY,CONVERTIBLE  1&5 Caps", Code = "511", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "520- 1 YR TREASURY   2&5 Caps", Code = "520", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "521- 1 YR TREASURY,CONVERTIBLE  2&5 Caps", Code = "521", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "711- 1 YR TREASURY,CONVERTIBLE  1&6 Caps", Code = "711", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "714- 1 YR COST OF FUNDS  1.5&5 Caps", Code = "714", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "722- 1 YR COST OF FUNDS  1&5 Caps", Code = "722", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "776- 1 YR TREASURY,CONVERTIBLE  1&4 Caps", Code = "776", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "791- 1 YR TREASURY,CONVERTIBLE  2&4 Caps", Code = "791", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "841- 1 YR TREASURY,CONVERTIBLE  1&4 Caps", Code = "841", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "851- 1 YR TREASURY,CONVERTIBLE  1&5 Caps", Code = "851", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "1029- 5 YR TWO-STEP", Code = "1029", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "975- 7 YR TWO-STEP", Code = "975", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "3270 – 3/1 LIBOR, 2/2/6 caps, 3 year Interest Only", Code = "3270", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "3271 – 3/1 TREASURY, 2/2/6 caps, 3 year Interest Only", Code = "3271", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "3513 – 3/1 TREASURY, 2/2/6 caps, 10 year Interest Only", Code = "3513", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "3514 – 3/1 LIBOR, 2/2/6 caps, 10 year Interest Only", Code = "3514", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "3223 – 5/1 LIBOR, 2/2/5 caps, 5 year Interest Only", Code = "3223", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "3226 – 5/1 TREASURY, 2/2/5 caps, 5 year Interest Only", Code = "3226", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "3515 – 5/1 TREASURY, 2/2/5 caps, 10 year Interest Only", Code = "3515", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "3516 – 5/1 LIBOR, 2/2/5 caps, 10 year Interest Only", Code = "3516", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "3502 – 5/1 LIBOR, 5/2/5 caps, 5 year Interest Only", Code = "3502", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "3503 – 5/1 TREASURY, 5/2/5 caps, 5 year Interest Only", Code = "3503", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "3504 – 5/1 TREASURY, 5/2/5 caps, 10 year Interest Only", Code = "3504", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "3505 – 5/1 LIBOR, 5/2/5 caps, 10 year Interest Only", Code = "3505", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "3224 – 7/1, LIBOR 5/2/5 caps, 7 year Interest Only", Code = "3224", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "3227 – 7/1, TREASURY 5/2/5 caps, 7 year Interest Only", Code = "3227", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "3517 – 7/1 TREASURY, 5/2/5 caps, 10 year Interest Only", Code = "3517", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "3518 – 7/1 LIBOR, 5/2/5 caps, 10 year Interest Only", Code = "3518", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "3225 – 10/1 LIBOR, 5/2/5 caps, 10 year Interest Only", Code = "3225", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "3228 – 10/1 TREASURY, 5/2/5 caps, 10 year Interest Only", Code = "3228", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "2236 – 3/1 TREASURY, 2/2/6 caps, 40 year term", Code = "2236", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "2238 – 5/1 TREASURY, 2/2/6 caps, 40 year term", Code = "2238", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "2699 – 5/1 TREASURY, 5/2/5 caps, 40 year term", Code = "2699", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "2704 – 7/1 TREASURY, 5/2/5 caps, 40 year term", Code = "2704", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "2703 – 10/1 TREASURY, 5/2/5 caps, 40 year term", Code = "2703", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "3130 – 3/1 LIBOR, 2/2/6 caps, 40 year term", Code = "3130", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "3557 – 5/1 LIBOR, 2/2/6 caps, 40 year term", Code = "3557", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "3128 – 5/1 LIBOR, 5/2/5 caps, 40 year term", Code = "3128", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "3558 – 7/1 LIBOR, 5/2/5 caps, 40 year term", Code = "3558", IsActive = false });
            planCodes.Add(new ArmPlan() { Description = "3559 – 10/1 LIBOR, 5/2/5 caps, 40 year term", Code = "3559", IsActive = false });

            return planCodes;
        }

        /// <summary>
        /// Determines the ARM plan code that best fits the provided loan data
        /// from the provided list of plan codes.
        /// </summary>
        /// <param name="sLT">Loan Type.</param>
        /// <param name="sRAdj1stCapMon">Initial Fixed Rate Interest Period.</param>
        /// <param name="sRAdjCapR">Subsequent Change Limit.</param>
        /// <param name="sArmIndexT">ARM Index.</param>
        /// <param name="sRAdjCapMon">Subsequent Interest Rate Adjustment Period.</param>
        /// <param name="sRAdj1stCapR">First Change Limit.</param>
        /// <param name="sRAdjLifeCapR">Life Interest Rate Cap.</param>
        /// <param name="sIsConvertibleMortgage">Convertibility of the mortgage.</param>
        /// <param name="planList">List of possible plan codes.</param>
        /// <returns>The plan code of the matching ARM plan.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Match existing elements.")]
        public static string CalculatePlanCode(
            E_sLT sLT,                                       
            int sRAdj1stCapMon,         
            decimal sRAdjCapR,          
            E_sArmIndexT sArmIndexT,    
            int sRAdjCapMon,            
            decimal sRAdj1stCapR,       
            decimal sRAdjLifeCapR,      
            bool sIsConvertibleMortgage,
            IEnumerable<ArmPlan> planList)
        {
            // Non-generic plans, then generic plans, then fall throughs.
            foreach (ArmPlan armPlan in planList
                .Where(plan => plan.IsActive)
                .OrderBy(plan => plan.IsGeneric)
                .ThenBy(plan => plan.IsFallBack))
            {
                if (armPlan.IsMatch(
                    sLT,
                    sRAdj1stCapMon,
                    sRAdjCapR,
                    sArmIndexT,
                    sRAdjCapMon,
                    sRAdj1stCapR,
                    sRAdjLifeCapR,
                    sIsConvertibleMortgage))
                {
                    return armPlan.Code;
                }
            }

            // There is a generic ARM plan that has no conditions on it.
            // That one should capture all ARM loans that do match any,
            // unless something went wrong.
            throw new LendersOffice.Common.GenericUserErrorMessageException("Cannot calculate ARM plan"
                + $" code from data: sLT={sLT}, sRAdj1stCapMon={sRAdj1stCapMon},"
                + $" sRAdjCapR={sRAdjCapR}, sArmIndexT={sArmIndexT}, sRAdjCapMon="
                + $"{sRAdjCapMon}, sRAdj1stCapR={sRAdj1stCapR}, sRAdjLifeCapR="
                + $"{sRAdjLifeCapR}, sIsConvertibleMortgage={sIsConvertibleMortgage}.");
        }

        /// <summary>
        /// Determines if the loan data provided qualifies for this ARM plan.
        /// </summary>
        /// <param name="sLT">Loan Type.</param>
        /// <param name="sRAdj1stCapMon">Initial Fixed Rate Interest Period.</param>
        /// <param name="sRAdjCapR">Subsequent Change Limit.</param>
        /// <param name="sArmIndexT">ARM Index.</param>
        /// <param name="sRAdjCapMon">Subsequent Interest Rate Adjustment Period.</param>
        /// <param name="sRAdj1stCapR">First Change Limit.</param>
        /// <param name="sRAdjLifeCapR">Life Interest Rate Cap.</param>
        /// <param name="sIsConvertibleMortgage">Convertibility of the mortgage.</param>
        /// <returns>True if this loan data qualifies for this ARM plan.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Match existing elements.")]
        private bool IsMatch(
            E_sLT sLT,                                      
            int sRAdj1stCapMon,        
            decimal sRAdjCapR,         
            E_sArmIndexT sArmIndexT,   
            int sRAdjCapMon,           
            decimal sRAdj1stCapR,      
            decimal sRAdjLifeCapR,     
            bool sIsConvertibleMortgage)
        {
            return this.IsActive  
                && (!this.sLT.HasValue || this.sLT == sLT)
                && (!this.sRAdj1stCapMonMin.HasValue || this.sRAdj1stCapMonMin <= sRAdj1stCapMon)
                && (!this.sRAdj1stCapMonMax.HasValue || this.sRAdj1stCapMonMax >= sRAdj1stCapMon)
                && (!this.sArmIndexT.HasValue || this.sArmIndexT == sArmIndexT)
                && (!this.sRAdjCapR.HasValue || this.sRAdjCapR == sRAdjCapR)
                && (!this.sRAdjCapMon.HasValue || this.sRAdjCapMon == sRAdjCapMon)
                && (!this.sRAdj1stCapR.HasValue || this.sRAdj1stCapR == sRAdj1stCapR)
                && (!this.sRAdjLifeCapR.HasValue || this.sRAdjLifeCapR == sRAdjLifeCapR)
                && (!this.sIsConvertibleMortgage.HasValue || this.sIsConvertibleMortgage == sIsConvertibleMortgage);
        }
    }
}
