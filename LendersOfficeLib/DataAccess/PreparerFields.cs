namespace DataAccess
{
    using System;
    using System.Data;
    using System.Reflection;
    using DataAccess.PathDispatch;
    using LendersOffice.Common;
    using LendersOffice.Migration;
    using LendersOffice.UI;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;

    public class CPreparerFields : CXmlRecordBaseOld, IPreparerFields, IPathResolvable
    {
        #region Fields
        //This should be refactored and pulled into the base class along with VorFields' version of it,
        //but I don't know this code well enough to do that without potentially breaking things.
        // -- No, we will *not* be doing that -- DT 8/22/17
        protected Action<IPreparerFields> m_onUpdate;

        private static readonly CPreparerFields s_empty = new CPreparerFields();

        private CPageBase m_parent;

        private bool agentTypeSet = false;

        private CAgentFields x_agentFields = null;
        #endregion Fields

        #region Constructors
        /// <summary>
        ///  This constructor will instantiate an object that IsValid() will return false,
        ///  use it for making static Empty object.
        /// </summary>
        protected CPreparerFields() : base()
        {
        }

        public CPreparerFields(CPageBase parent, DataSet ds, E_PreparerFormT form)
            : this(parent, ds, -1, null)
        {
            this.PreparerFormT = form;
        }

        // Pass -1 for rowApp to add new row.
        public CPreparerFields(CPageBase parent, DataSet ds, int iRow, Action<IPreparerFields> notifyOfChange)
            : base(parent, ds, iRow, "PreparerXmlContent")
        {
            m_parent = parent;
            this.m_onUpdate = notifyOfChange;
            if (!HadInitialCheck)
            {
                if (GetDescString("PreparerName") != "")
                {
                    NameLocked = true;
                }

                if (GetDescString("LicenseNum") != "")
                {
                    ChumsIdLocked = true;
                }

                HadInitialCheck = true;
            }

            this.SetDefaultAgent();
        }
        #endregion Constructors

        #region Properties
        public static CPreparerFields Empty
        {
            get { return s_empty; }
        }

        public bool IsEmpty
        {
            get
            {
                return
                    PreparerName == "" &&
                    CompanyName == "" &&
                    StreetAddr == "" &&
                    City == "" &&
                    State == "" &&
                    Zip == "" &&
                    County == string.Empty &&
                    Phone == "" &&
                    CellPhone == "" &&
                    PagerNum == "" &&
                    EmailAddr == "" &&
                    PrepareDate_rep == "" &&
                    LicenseNumOfAgent == "" &&
                    true;
            }
        }

        public E_PreparerFormT PreparerFormT
        {
            get
            {
                if (!IsValid)
                    return E_PreparerFormT.None;

                return (E_PreparerFormT)GetEnum("PreparerFormT", E_PreparerFormT.None);
            }
            set
            {
                SetEnum("PreparerFormT", value);

                // 17May2017 DT - Doing this here because we set the PreparerFormT after the constructor
                // completes when creating a new record.
                this.SetDefaultAgent();
            }
        }

        public E_AgentRoleT AgentRoleT
        {
            get { return (E_AgentRoleT)GetEnum("AgentRoleT", E_AgentRoleT.Other); }
            set
            {
                SetEnum("AgentRoleT", value);
                this.agentTypeSet = true;
            }
        }

        public Guid AgentId
        {
            get
            {
                if (IsLocked)
                {
                    return GetGuid("AgentId");
                }
                else
                {
                    return AgentFields.RecordId;
                }
            }
            set { SetGuid("AgentId", value); }
        }

        public bool IsLocked
        {
            get
            {
                if (!IsValid)
                {
                    return true; // 5/19/2009 dd - If CPreparer is invalid then always return manual override
                }
                // 5/19/2009 dd - I just introduce this boolean. Therefore any existing CPreparerFields without this property will default to ManualOverride.
                return GetBool("IsLocked", true);
            }
            set { SetBool("IsLocked", value); }
        }

        public bool NameLocked
        {
            get
            {
                return GetBool("NameLocked", false);
            }
            set
            {
                if (!NameLocked)
                {
                    SetBool("NameLocked", value);
                }
            }
        }

        public bool ChumsIdLocked
        {
            get
            {
                return GetBool("LicenseNumOfAgentLocked", false);
            }
            set
            {
                if (!ChumsIdLocked)
                {
                    SetBool("LicenseNumOfAgentLocked", value);
                }
            }
        }

        public override Enum KeyType
        {
            get { return PreparerFormT; }
            set { PreparerFormT = (E_PreparerFormT)value; }
        }

        public bool HadInitialCheck
        {
            get { return GetBool("HadInitialCheck", false); }
            set { SetBool("HadInitialCheck", value); }
        }

        public string PreparerName
        {
            get
            {
                // FIXME - These forms currently have bits other than the IsLocked bit to determine
                // their override behavior, and will need a migration to set right. Deal with this special
                // case here in the meantime.
                if (IsSpecialFhaUwPreparer(this.PreparerFormT))
                {
                    if (this.NameLocked)
                    {
                        return GetDescString("PreparerName");
                    }
                    else
                    {
                        return AgentFields.AgentName;
                    }
                }

                if (IsLocked)
                {
                    return GetDescString("PreparerName");
                }
                else
                {
                    return AgentFields.AgentName;
                }

            }
            set { SetDescString("PreparerName", value); }
        }

        public string CompanyName
        {
            get
            {
                if (IsLocked)
                {
                    return GetDescString("CompanyName");
                }
                else
                {
                    return AgentFields.CompanyName;
                }
            }
            set { SetDescString("CompanyName", value); }
        }

        public string DepartmentName
        {
            get
            {
                if (IsLocked)
                {
                    return GetDescString("DepartmentName");
                }
                else
                {
                    return AgentFields.DepartmentName;
                }
            }
            set { SetDescString("DepartmentName", value); }
        }

        public string Title
        {
            get
            {
                if (IsLocked)
                {
                    return GetDescString("Title");
                }
                else
                {
                    return AgentFields.AgentRoleDescription;
                }
            }
            set { SetDescString("Title", value); }
        }

        public string TaxId
        {
            get
            {
                if (IsLocked)
                {
                    // m_parent will be null if the ReturnEmptyObject option was specified.
                    if (this.m_parent == null
                        || LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.m_parent.sLoanVersionT, LoanVersionT.V18_ConsolidateTaxIdFields))
                    {
                        EmployerTaxIdentificationNumber? ein = EmployerTaxIdentificationNumber.Create(GetDescString("TaxId"));
                        return this.m_convertLos.ToEmployerTaxIdentificationNumberString(ein);
                    }
                    else
                    {
                        return GetDescString("TaxId");
                    }
                }
                else
                {
                    // For loan versions >= 18 this will be valid and the value will
                    // be formatted by LosConvert.
                    return AgentFields.TaxId;
                }

            }
            set
            {
                // m_parent will be null if the ReturnEmptyObject option was specified.
                if (this.m_parent == null
                    || LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.m_parent.sLoanVersionT, LoanVersionT.V18_ConsolidateTaxIdFields))
                {
                    EmployerTaxIdentificationNumber? ein = EmployerTaxIdentificationNumber.Create(value);

                    if (!ein.HasValue && !string.IsNullOrEmpty(value))
                    {
                        throw new InvalidTaxIdValueException("TaxId");
                    }

                    // For files on the new version, we will only set valid values and we will store them without the dash.
                    SetDescString("TaxId", ein.ToString());
                }
                else
                {
                    SetDescString("TaxId", value);
                }
            }
        }

        public bool TaxIdValid
        {
            get
            {
                return string.IsNullOrEmpty(this.TaxId) || EmployerTaxIdentificationNumber.Create(this.TaxId).HasValue;
            }
        }

        public string LoanOriginatorIdentifier
        {
            get
            {
                if (IsLocked)
                {
                    return GetString("LoanOriginatorIdentifier");
                }
                else
                {
                    return AgentFields.LoanOriginatorIdentifier;
                }
            }
            set { SetString("LoanOriginatorIdentifier", value); }
        }

        public string CompanyLoanOriginatorIdentifier
        {
            get
            {
                if (IsLocked)
                {
                    return GetString("CompanyLoanOriginatorIdentifier");
                }
                else
                {
                    return AgentFields.CompanyLoanOriginatorIdentifier;
                }
            }
            set { SetString("CompanyLoanOriginatorIdentifier", value); }
        }

        public string LicenseNumOfAgent
        {
            get
            {
                // FIXME - The code for these forms previously stored the CHUMS ID in the LicenseNum column.
                // Leave this here until we can do a migration to move existing data to the ChumsId column.
                if (IsSpecialFhaUwPreparer(this.PreparerFormT))
                {
                    if (this.ChumsIdLocked)
                    {
                        return GetDescString("LicenseNum");
                    }
                    else
                    {
                        return AgentFields.ChumsId;
                    }
                }


                if (IsLocked)
                {
                    return GetDescString("LicenseNum");
                }
                else
                {
                    return AgentFields.LicenseNumOfAgent;
                }
            }
            set { SetDescString("LicenseNum", value); }
        }

        public bool LicenseNumOfAgentIsExpired
        {
            get
            {
                if (IsLocked)
                {
                    // Per OPM 201194, Manual means not expired.
                    return false;
                }
                else
                {
                    return AgentFields.LicenseNumOfAgentIsExpired;
                }
            }
        }

        public string LicenseNumOfCompany
        {
            get
            {
                if (IsLocked)
                {
                    return GetDescString("LicenseNumOfCompany");
                }
                else
                {
                    return AgentFields.LicenseNumOfCompany;
                }
            }
            set { SetDescString("LicenseNumOfCompany", value); }
        }

        public bool LicenseNumOfCompanyIsExpired
        {
            get
            {
                if (IsLocked)
                {
                    // Per OPM 201194, Manual means not expired.
                    return false;
                }
                else
                {
                    return AgentFields.LicenseNumOfCompanyIsExpired;
                }
            }
        }

        public string ChumsId
        {
            get
            {
                // FIXME - The code for these forms previously stored the CHUMS ID in the LicenseNum column.
                // Leave this here until we can do a migration to move existing data to the ChumsId column.
                if (IsSpecialFhaUwPreparer(this.PreparerFormT))
                {
                    if (this.ChumsIdLocked)
                    {
                        return GetDescString("LicenseNum");
                    }
                    else
                    {
                        return AgentFields.ChumsId;
                    }
                }

                if (this.IsLocked)
                {
                    return GetDescString("ChumsId");
                }
                else
                {
                    return AgentFields.ChumsId;
                }
            }
            set
            {
                // FIXME - Again, we need to put this in the wrong place until we can do a migration
                // to put this all in the right place.
                if (this.ChumsIdLocked && IsSpecialFhaUwPreparer(this.PreparerFormT))
                {
                    SetDescString("LicenseNum", value);
                }
                else
                {
                    SetDescString("ChumsId", value);
                }
            }
        }

        public string CaseNum
        {
            get
            {
                if (IsLocked)
                {
                    return GetDescString("CaseNum");
                }
                else
                {
                    return AgentFields.CaseNum;
                }
            }
            set { SetDescString("CaseNum", value); }
        }

        public string StreetAddr
        {
            get
            {
                if (IsLocked)
                {
                    return GetDescString("StreetAddr");
                }
                else
                {
                    return AgentFields.StreetAddr;
                }
            }
            set { SetDescString("StreetAddr", value); }
        }

        public string City
        {
            get
            {
                if (IsLocked)
                {
                    return GetDescString("City");
                }
                else
                {
                    return AgentFields.City;
                }

            }
            set { SetDescString("City", value); }
        }

        public string County
        {
            get
            {
                if (this.IsLocked)
                {
                    return GetDescString("County");
                }
                else
                {
                    return AgentFields.County;
                }
            }

            set
            {
                SetDescString("County", value);
            }
        }

        public string State
        {
            get
            {
                if (IsLocked)
                {
                    return GetDescString("State");
                }
                else
                {
                    return AgentFields.State;
                }
            }
            set { SetDescString("State", value); }
        }

        public string Zip
        {
            get
            {
                if (IsLocked)
                {
                    return GetDescString("Zip");
                }
                else
                {
                    return AgentFields.Zip;
                }
            }
            set { SetDescString("Zip", value); }
        }

        public string CityStateZip
        {
            get
            {
                if (!IsValid)
                    return "";

                return Tools.CombineCityStateZip(City, State, Zip);
            }
        }

        public string StreetAddrMultiLines
        {
            get { return Tools.FormatAddress(StreetAddr, City, State, Zip); }
        }

        public string StreetAddrSingleLine
        {
            get { return Tools.FormatSingleLineAddress(StreetAddr, City, State, Zip); }
        }

        [LqbInputModel(type: InputFieldType.Phone)]
        public string Phone
        {
            get
            {
                if (IsLocked)
                {
                    return GetPhoneNum("Phone");
                }
                else
                {
                    return AgentFields.Phone;
                }
            }
            set { SetPhoneNum("Phone", value); }
        }

        [LqbInputModel(type: InputFieldType.Phone)]
        public string CellPhone
        {
            get
            {
                if (IsLocked)
                {
                    return GetPhoneNum("CellPhone");
                }
                else
                {
                    return AgentFields.CellPhone;
                }
            }
            set { SetPhoneNum("CellPhone", value); }
        }

        public string PagerNum
        {
            get
            {
                if (IsLocked)
                {
                    return GetPhoneNum("PagerNum");
                }
                else
                {
                    return AgentFields.PagerNum;
                }
            }
            set { SetPhoneNum("PagerNum", value); }
        }

        [LqbInputModel(type: InputFieldType.Phone)]
        public string FaxNum
        {
            get
            {
                if (IsLocked)
                {
                    return GetPhoneNum("FaxNum");
                }
                else
                {
                    return AgentFields.FaxNum;
                }
            }
            set { SetPhoneNum("FaxNum", value); }
        }

        [LqbInputModel(type: InputFieldType.Phone)]
        public string PhoneOfCompany
        {
            get
            {
                if (IsLocked)
                {
                    return GetPhoneNum("PhoneOfCompany");
                }
                else
                {
                    return AgentFields.PhoneOfCompany;
                }
            }
            set { SetPhoneNum("PhoneOfCompany", value); }
        }

        [LqbInputModel(type: InputFieldType.Phone)]
        public string FaxOfCompany
        {
            get
            {
                if (IsLocked)
                {
                    return GetPhoneNum("FaxOfCompany");
                }
                else
                {
                    return AgentFields.FaxOfCompany;
                }
            }
            set { SetPhoneNum("FaxOfCompany", value); }
        }

        public string EmailAddr
        {
            get
            {
                if (IsLocked)
                {
                    return GetDescString("EmailAddr");
                }
                else
                {
                    return AgentFields.EmailAddr;
                }
            }
            set { SetDescString("EmailAddr", value); }
        }

        public CDateTime PrepareDate
        {
            get
            {
                if (IsValid)
                {
                    if (PreparerFormT == E_PreparerFormT.Gfe || PreparerFormT == E_PreparerFormT.Til)
                    {
                        if (m_parent != null && m_parent.BrokerDB.IsRemovePreparedDates)
                        {
                            return CDateTime.Create(DateTime.Now);
                        }
                    }
                }
                return GetDateTime("PrepareDate");

            }
            set { SetDateTime("PrepareDate", value); }
        }

        [LqbInputModel(invalid: true)]
        public string PrepareDate_rep
        {
            get { return PrepareDate.ToString(m_convertLos); }
            set { PrepareDate = CDateTime.Create(value, m_convertLos); }
        }

        internal DataSet PreparerDataSet
        {
            get { return this.m_parent.sPreparerDataSet; }
        }

        private CAgentFields AgentFields
        {
            get
            {
                if (IsLocked)
                {
                    // FIXME - These forms currently have special override behavior that will require a migration to get rid of.
                    if (!IsSpecialFhaUwPreparer(this.PreparerFormT))
                    {
                        throw new CBaseException(ErrorMessages.Generic, "Programming Error: Pulling Agent Fields when IsManualOverride is true.");
                    }
                }

                if (null == x_agentFields)
                {
                    x_agentFields = ((CPageBase)Parent).GetAgentOfRole(AgentRoleT, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                }
                return x_agentFields;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds the record to the DataSet if it is new, and runs the function set by the
        /// SetFlushFunction method. Note that if you have a new record and you call this
        /// twice, it will throw. The recommended work-around is to refactor the code base
        /// so that we can throw this class away. If that is not feasible, your alternative
        /// is to get a new instance of the object immediately after calling Update() the
        /// first time. You can call Update() on that instance with impunity since it is
        /// not considered new.
        /// </summary>
        override public void Update()
        {
            if (IsValid)
            {
                base.Update();
                m_parent.sPreparerDataSet = m_ds;
                if (m_onUpdate != null)
                {
                    m_onUpdate(this);
                }
            }
            else
            {
                Tools.LogBug("Programming Error, CPreparerFields.Update() is called on an invalid CPreparerFields object.");
            }
        }

        public void SyncFromAgent(CAgentFields agent)
        {
            CaseNum = agent.CaseNum;
            CellPhone = agent.CellPhone;
            CompanyName = agent.CompanyName;
            DepartmentName = agent.DepartmentName;
            EmailAddr = agent.EmailAddr;
            FaxNum = agent.FaxNum;
            LicenseNumOfAgent = agent.LicenseNumOfAgent;
            LicenseNumOfCompany = agent.LicenseNumOfCompany;
            PagerNum = agent.PagerNum;
            Phone = agent.Phone;
            PreparerName = agent.AgentName;
            Title = agent.AgentRoleDescription;
            StreetAddr = agent.StreetAddr;
            City = agent.City;
            State = agent.State;
            Zip = agent.Zip;
            County = agent.County;
            PhoneOfCompany = agent.PhoneOfCompany;
            FaxOfCompany = agent.FaxOfCompany;
            LoanOriginatorIdentifier = agent.LoanOriginatorIdentifier;
            CompanyLoanOriginatorIdentifier = agent.CompanyLoanOriginatorIdentifier;
            AgentId = agent.RecordId;
        }

        public void SyncToAgent(CAgentFields agent)
        {
            IsLocked = true;
            agent.CaseNum = CaseNum;
            agent.CellPhone = CellPhone;
            agent.CompanyName = CompanyName;
            agent.DepartmentName = DepartmentName;
            agent.EmailAddr = EmailAddr;
            agent.FaxNum = FaxNum;
            agent.LicenseNumOfAgent = LicenseNumOfAgent;
            agent.LicenseNumOfCompany = LicenseNumOfCompany;
            agent.PagerNum = PagerNum;
            agent.Phone = Phone;
            agent.AgentName = PreparerName;
            agent.StreetAddr = StreetAddr;
            agent.City = City;
            agent.State = State;
            agent.Zip = Zip;
            agent.County = County;
            agent.PhoneOfCompany = PhoneOfCompany;
            agent.FaxOfCompany = FaxOfCompany;
            agent.LoanOriginatorIdentifier = LoanOriginatorIdentifier;
            agent.CompanyLoanOriginatorIdentifier = CompanyLoanOriginatorIdentifier;
            IsLocked = false;
        }

        public void CopyInfoFrom(IPreparerFields other)
        {
            AgentRoleT = other.AgentRoleT;
            CaseNum = other.CaseNum;
            CellPhone = other.CellPhone;
            City = other.City;
            CompanyLoanOriginatorIdentifier = other.CompanyLoanOriginatorIdentifier;
            CompanyName = other.CompanyName;
            DepartmentName = other.DepartmentName;
            EmailAddr = other.EmailAddr;
            FaxOfCompany = other.FaxOfCompany;
            FaxNum = other.FaxNum;
            IsLocked = other.IsLocked;
            LicenseNumOfAgent = other.LicenseNumOfAgent;
            LicenseNumOfCompany = other.LicenseNumOfCompany;
            LoanOriginatorIdentifier = other.LoanOriginatorIdentifier;
            PagerNum = other.PagerNum;
            Phone = other.Phone;
            PhoneOfCompany = other.PhoneOfCompany;
            PrepareDate_rep = other.PrepareDate_rep;
            PreparerName = other.PreparerName;
            State = other.State;
            StreetAddr = other.StreetAddr;
            TaxId = other.TaxId;
            Title = other.Title;
            Zip = other.Zip;
            County = other.County;
            AgentId = other.AgentId;
        }

        public void overrideNameLocked(bool val)
        {
            SetBool("NameLocked", val);
        }

        public void overrideLicenseLocked(bool val)
        {
            SetBool("LicenseNumOfAgentLocked", val);
        }

        public DataSet GetCopyOfBaseData()
        {
            return m_ds.Copy();
        }

        /// <summary>
        /// Indicates whether or not the preparer type is one of the special FHA preparers,
        /// who do weird things due to historical reasons.
        /// </summary>
        /// <remarks>This special behavior needs to die.</remarks>
        /// <param name="preparerType">The preparer type to check</param>
        /// <returns>True if the preparer type is one the the special FHA ones, false otherwise.</returns>
        private static bool IsSpecialFhaUwPreparer(E_PreparerFormT preparerType)
        {
            switch (preparerType)
            {
                case E_PreparerFormT.FHA203kWorksheetUnderwriter:
                case E_PreparerFormT.FHA92900LtUnderwriter:
                case E_PreparerFormT.FHAAddendumUnderwriter:
                case E_PreparerFormT.FHAAnalysisAppraisalUnderwriter:
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Sets the default agent for some preparer form types.
        /// </summary>
        private void SetDefaultAgent()
        {
            if (!this.agentTypeSet && IsSpecialFhaUwPreparer(this.PreparerFormT))
            {
                this.AgentRoleT = E_AgentRoleT.Underwriter;
                this.agentTypeSet = true;
            }
        }

        /// <summary>
        /// Returns the value specified by the element.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns>The value specified by the element.</returns>
        public object GetElement(IDataPathElement element)
        {
            if (!(element is DataPathBasicElement))
            {
                throw new ArgumentException("Preparer can only handle basic elements");
            }

            string fieldName = element.Name;

            PropertyInfo fieldInfo = this.GetType().GetProperty(fieldName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance);

            if (fieldInfo == null)
            {
                throw new ArgumentException("Field not found in Preparer: " + fieldName);
            }

            if (string.Equals(fieldName, nameof(PrepareDate), StringComparison.OrdinalIgnoreCase))
            {
                return this.PrepareDate_rep;
            }
            if (fieldInfo.PropertyType == typeof(string))
            {
                return this.GetDescString(fieldName);
            }
            else if (fieldInfo.PropertyType.IsEnum)
            {
                return GetEnum(fieldName, E_AgentRoleT.Other);
            }
            else if (fieldInfo.PropertyType == typeof(bool))
            {
                return GetBool(fieldName);
            }
            else
            {
                throw new ArgumentException("Found field in Preparer, but couldn't figure out what to do with the type: " + fieldName);
            }
        }
        #endregion Methods
    }
}