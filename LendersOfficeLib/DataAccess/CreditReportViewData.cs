using System;
using System.Collections;
using System.Data;

namespace DataAccess
{

	public class CCreditReportViewData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CCreditReportViewData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
	        list.Add("CreditReportView");
            list.Add("LqiCreditReportView");
            #endregion

            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CCreditReportViewData(Guid fileId) : base(fileId, "CCreditReportViewData", s_selectProvider)
		{
		}
	}
}
