using System.Data.Common;
using System.Data.SqlClient;
using System;
using System.Data;
using LendersOffice.Common;

namespace DataAccess
{
    public class CEmployeeFields
    {
        public static CEmployeeFields Empty
        {
            // 7/16/2013 dd - Return an new instance of CEmployeeFields instead of static variable.
            // The reason is I am introduce a way to set EmployeeId, therefore a static variable
            // is no longer safe.
            get { return new CEmployeeFields(null); }
        }
        private IDataContainer m_row;

        /// <summary>
        /// Please note: when modifying this enumeration, make sure to 
        /// notify support so that they can update the sample folder 
        /// import spreadsheet for the edocs importer. The importer 
        /// will handle the rest automatically. See OPM 119251 for 
        /// more details.
        /// </summary>
        public enum E_Role
        {
            Undefined = 0,
            LoanRep = 1,
            Manager = 2,
            Processor = 3,
            CallCenterAgent = 4,
            RealEstateAgent = 5,
            Underwriter = 6,
            Administrator = 7,
            Funder = 8,
            LoanOpener = 9,
            LenderAccountExec = 10,
            LockDesk = 11,
            Shipper = 12, // OPM 44150
            Closer = 13, 
            Accountant = 14,
            Consumer = 15, // Consumer Portal project
            BrokerProcessor = 16, //new work flow  av 7 7 10  
            PostCloser = 17, // OPM 108148 v gf
            Insuring = 18,
            CollateralAgent = 19,
            DocDrawer = 20, // OPM 108148 ^ gf
            CreditAuditor = 21, // 11/22/2013 gf - opm 145015 start
            DisclosureDesk = 22,
            JuniorProcessor = 23,
            JuniorUnderwriter = 24,
            LegalAuditor = 25,
            LoanOfficerAssistant = 26,
            Purchaser = 27,
            QCCompliance = 28,
            Secondary = 29,
            Servicing = 30, // 11/22/2013 gf - opm 145015 end
            ExternalSecondary = 31,
            ExternalPostCloser = 32
        }

        public CEmployeeFields(IDataContainer dataRow)
        {
            m_row = dataRow;
            IsDirty = false;
        }

        public static Guid s_ManagerRoleId
        {
            get { return new Guid("F544DF6A-5C55-4568-9579-97D74ED0F752"); }
        }
        public static Guid s_LoanRepRoleId
        {
            get { return new Guid("6DFC9B0E-11E3-48C1-B410-A1B0B5363B69"); }
        }
        public static Guid s_UnderwriterRoleId
        {
            get { return new Guid("E0534F45-2AE1-4A8C-952E-00520F445819"); }
        }
        public static Guid s_CallCenterAgentRoleId
        {
            get { return new Guid("5B02D42C-1F89-4BEC-8A29-63872837FC1D"); }
        }
        public static Guid s_ProcessorRoleId
        {
            get { return new Guid("89572CFA-A3EE-4513-9B25-71A212AD5D09"); }
        }
        public static Guid s_RealEstateAgentRoleId
        {
            get { return new Guid("74ED15E7-C06D-4EAB-9CB0-A64AB412FBE5"); }
        }
        public static Guid s_AdministratorRoleId
        {
            get { return new Guid("86C86CF3-FEB6-400F-80DC-123363A79605"); }
        }
        public static Guid s_FunderRoleId
        {
            get { return new Guid("9658E876-A3EC-4B3E-B081-F0F955719820"); }
        }
        public static Guid s_LenderAccountExecRoleId
        {
            get { return new Guid("4FAA1C9E-3188-4D82-9734-E4991F6AD50C"); }
        }
        public static Guid s_LockDeskRoleId
        {
            get { return new Guid("9067077D-6BD3-4E70-99C2-4A4AAF7811D7"); }
        }
        public static Guid s_LoanOpenerId
        {
            get { return new Guid("1764DFFD-EBF9-4375-ACCD-1B822B39FA63"); }
        }
        public static Guid s_ShipperId
        {
            get { return new Guid("27424fdc-d6c8-432c-860a-4eca064f924f"); }
        }

        public static Guid s_CloserId
        {
            get { return new Guid("270A7689-EEED-49F9-B438-69AC322EA834"); }
        }

        public static Guid s_BrokerProcessorId
        {
            get { return new Guid("B3785253-D9E8-4a3f-B0EC-D02D6FCF8538"); }
        }

        public static Guid s_AccountantId
        {
            get { return new Guid("F98C17F5-27AD-4097-ACFB-88F6A754ACF3"); }
        }

        // OPM 50974
        public static Guid s_ConsumerId
        {
            get { return new Guid("552EBA07-EA55-404F-8C00-DC696EAA04BB"); }
        }

        public static Guid s_PostCloserId
        {
            get { return new Guid("A24E962E-301E-4436-BCA1-37E5D2227A30"); }
        }
        
        public static Guid s_InsuringId
        {
            get { return new Guid("D8DC85B9-629E-4488-95AB-5DEC71FD8018"); }
        }

        public static Guid s_CollateralAgentId
        {
            get { return new Guid("E34352C6-45A8-4239-B2D1-218748C8B120"); }
        }

        public static Guid s_DocDrawerId
        {
            get { return new Guid("74097291-502A-4F50-9018-11A6A302B12E"); }
        }

        public static Guid s_CreditAuditorId
        {
            get { return new Guid("6C83465C-D3E9-4950-918B-331DE262B4A5"); }
        }

        public static Guid s_DisclosureDeskId
        {
            get { return new Guid("1386D205-F5FB-435E-AFA8-730CDDEB824E"); }
        }

        public static Guid s_JuniorProcessorId
        {
            get { return new Guid("4A03CF0F-D2E8-4201-B161-A5BC9E87FDE3"); }
        }

        public static Guid s_JuniorUnderwriterId
        {
            get { return new Guid("AD4661DF-4DCA-4FE4-A60D-85424FB937FE"); }
        }

        public static Guid s_LegalAuditorId
        {
            get { return new Guid("A9604616-6C20-4D0F-BE97-89913CF11AE3"); }
        }

        public static Guid s_LoanOfficerAssistantId
        {
            get { return new Guid("E4949811-34DC-4050-ADF5-78A06990E24E"); }
        }

        public static Guid s_PurchaserId
        {
            get { return new Guid("763F27C8-EAC4-45FF-AD65-15E27BDC41F1"); }
        }

        public static Guid s_QCComplianceId
        {
            get { return new Guid("270A9A82-F572-4810-8B8D-3D8D0EEDC65F"); }
        }

        public static Guid s_SecondaryId
        {
            get { return new Guid("1F51B219-5F29-4729-BAD4-AB2ACC670F24"); }
        }

        public static Guid s_ServicingId
        {
            get { return new Guid("C5058D4E-BBC2-425B-B4A3-73B8CD66D896"); }
        }

        public static Guid s_ExternalSecondaryId
        {
            get { return new Guid("274981E4-8C18-4204-B97B-2F537922CCD9"); }
        }

        public static Guid s_ExternalPostCloserId
        {
            get { return new Guid("89811C63-6A28-429A-BB45-3152449D854E"); }
        }

        public bool IsValid
        {
            get { return null != m_row; }
        }

        public static Guid GetRoleId(E_Role role)
        {
            switch (role)
            {
                
                case E_Role.LoanRep:
                    return CEmployeeFields.s_LoanRepRoleId;
                case E_Role.Manager:
                    return CEmployeeFields.s_ManagerRoleId;
                case E_Role.Processor:
                    return CEmployeeFields.s_ProcessorRoleId;
                case E_Role.CallCenterAgent:
                    return CEmployeeFields.s_CallCenterAgentRoleId;
                case E_Role.RealEstateAgent:
                    return CEmployeeFields.s_RealEstateAgentRoleId;
                case E_Role.Underwriter:
                    return CEmployeeFields.s_UnderwriterRoleId;
                case E_Role.Administrator:
                    return CEmployeeFields.s_AdministratorRoleId;
                case E_Role.Funder:
                    return CEmployeeFields.s_FunderRoleId;
                case E_Role.LoanOpener:
                    return CEmployeeFields.s_LoanOpenerId;
                case E_Role.LenderAccountExec:
                    return CEmployeeFields.s_LenderAccountExecRoleId;
                case E_Role.LockDesk:
                    return CEmployeeFields.s_LockDeskRoleId;
                case E_Role.Shipper:
                    return CEmployeeFields.s_ShipperId;
                case E_Role.Closer:
                    return CEmployeeFields.s_CloserId;                                        
                case E_Role.Accountant:
                    return CEmployeeFields.s_AccountantId;
                case E_Role.Consumer:
                    return CEmployeeFields.s_ConsumerId;
                case E_Role.BrokerProcessor:
                    return CEmployeeFields.s_BrokerProcessorId;
                case E_Role.PostCloser:
                    return CEmployeeFields.s_PostCloserId;
                case E_Role.Insuring:
                    return CEmployeeFields.s_InsuringId;
                case E_Role.CollateralAgent:
                    return CEmployeeFields.s_CollateralAgentId;
                case E_Role.DocDrawer:
                    return CEmployeeFields.s_DocDrawerId;
                case E_Role.CreditAuditor:
                    return CEmployeeFields.s_CreditAuditorId ;
            
                case E_Role.DisclosureDesk:
                    return CEmployeeFields.s_DisclosureDeskId;
               
                case E_Role.JuniorProcessor:
                    return CEmployeeFields.s_JuniorProcessorId;
          
                case E_Role.JuniorUnderwriter:
                    return CEmployeeFields.s_JuniorUnderwriterId;
          
                case E_Role.LegalAuditor:
                    return CEmployeeFields.s_LegalAuditorId;
         
                case E_Role.LoanOfficerAssistant:
                    return CEmployeeFields.s_LoanOfficerAssistantId;
 
                case E_Role.Purchaser:
                    return CEmployeeFields.s_PurchaserId;
           
                case E_Role.QCCompliance:
                    return CEmployeeFields.s_QCComplianceId;
   
                case E_Role.Secondary:
                    return CEmployeeFields.s_SecondaryId;
          
                case E_Role.Servicing:
                    return CEmployeeFields.s_ServicingId;

                case E_Role.ExternalSecondary:
                    return CEmployeeFields.s_ExternalSecondaryId;

                case E_Role.ExternalPostCloser:
                    return CEmployeeFields.s_ExternalPostCloserId;

                case E_Role.Undefined:
                default:
                    throw new UnhandledEnumException(role);
            }
        }

        public static bool IsPmlRole(E_Role role)
        {
            return role == E_Role.BrokerProcessor ||
                role == E_Role.ExternalPostCloser ||
                role == E_Role.ExternalSecondary;
        }

        public E_Role Role
        {
            get
            {
                if (!IsValid)
                    return E_Role.Undefined;

                if (RoleId == s_LoanRepRoleId)
                    return E_Role.LoanRep;
                else if (RoleId == s_ManagerRoleId)
                    return E_Role.Manager;
                else if (RoleId == s_CallCenterAgentRoleId)
                    return E_Role.CallCenterAgent;
                else if (RoleId == s_ProcessorRoleId)
                    return E_Role.Processor;
                else if (RoleId == s_RealEstateAgentRoleId)
                    return E_Role.RealEstateAgent;
                else if (RoleId == s_AdministratorRoleId)
                    return E_Role.Administrator;
                else if (RoleId == s_FunderRoleId)
                    return E_Role.Funder;
                else if (RoleId == s_UnderwriterRoleId)
                    return E_Role.Underwriter;
                else if (RoleId == s_LenderAccountExecRoleId)
                    return E_Role.LenderAccountExec;
                else if (RoleId == s_LockDeskRoleId)
                    return E_Role.LockDesk;
                else if (RoleId == s_LoanOpenerId)
                    return E_Role.LoanOpener;
                else if (RoleId == s_ShipperId)
                    return E_Role.Shipper;
                else if (RoleId == s_CloserId)
                    return E_Role.Closer;
                else if (RoleId == s_AccountantId)
                    return E_Role.Accountant;
                else if (RoleId == s_BrokerProcessorId)
                    return E_Role.BrokerProcessor;
                else if (RoleId == s_PostCloserId)
                    return E_Role.PostCloser;
                else if (RoleId == s_InsuringId)
                    return E_Role.Insuring;
                else if (RoleId == s_CollateralAgentId)
                    return E_Role.CollateralAgent;
                else if (RoleId == s_DocDrawerId)
                    return E_Role.DocDrawer;
                else if (RoleId == s_CreditAuditorId)
                    return E_Role.CreditAuditor;
                else if (RoleId == s_DisclosureDeskId)
                    return E_Role.DisclosureDesk;
                else if (RoleId == s_JuniorProcessorId)
                    return E_Role.JuniorProcessor;
                else if (RoleId == s_JuniorUnderwriterId)
                    return E_Role.JuniorUnderwriter;
                else if (RoleId == s_LegalAuditorId)
                    return E_Role.LegalAuditor;
                else if (RoleId == s_LoanOfficerAssistantId)
                    return E_Role.LoanOfficerAssistant;
                else if (RoleId == s_PurchaserId)
                    return E_Role.Purchaser;
                else if (RoleId == s_QCComplianceId)
                    return E_Role.QCCompliance;
                else if (RoleId == s_SecondaryId)
                    return E_Role.Secondary;
                else if (RoleId == s_ServicingId)
                    return E_Role.Servicing;
                else if (RoleId == s_ExternalSecondaryId)
                    return E_Role.ExternalSecondary;
                else if (RoleId == s_ExternalPostCloserId)
                    return E_Role.ExternalPostCloser;
                throw new System.Exception("Unhandled role id assigned to a loan file");
            }
        }

        // Adding for consumer portal project
        //public static E_Role GetRoleById(Guid roleId)
        //{
        //    if (roleId == s_LoanRepRoleId)
        //        return E_Role.LoanRep;
        //    else if (roleId == s_ManagerRoleId)
        //        return E_Role.Manager;
        //    else if (roleId == s_CallCenterAgentRoleId)
        //        return E_Role.CallCenterAgent;
        //    else if (roleId == s_ProcessorRoleId)
        //        return E_Role.Processor;
        //    else if (roleId == s_RealEstateAgentRoleId)
        //        return E_Role.RealEstateAgent;
        //    else if (roleId == s_AdministratorRoleId)
        //        return E_Role.Administrator;
        //    else if (roleId == s_FunderRoleId)
        //        return E_Role.Funder;
        //    else if (roleId == s_UnderwriterRoleId)
        //        return E_Role.Underwriter;
        //    else if (roleId == s_LenderAccountExecRoleId)
        //        return E_Role.LenderAccountExec;
        //    else if (roleId == s_LockDeskRoleId)
        //        return E_Role.LockDesk;
        //    else if (roleId == s_LoanOpenerId)
        //        return E_Role.LoanOpener;
        //    else if (roleId == s_ShipperId)
        //        return E_Role.Shipper;
        //    else if (roleId == s_CloserId)
        //        return E_Role.Closer;
        //    else if (roleId == s_AccountantId)
        //        return E_Role.Accountant;
        //    else if (roleId == s_ConsumerId)
        //        return E_Role.Consumer;
        //    else if (roleId == s_BrokerProcessorId)
        //        return E_Role.BrokerProcessor;
        //    else if (roleId == s_PostCloserId)
        //        return E_Role.PostCloser;
        //    else if (roleId == s_InsuringId)
        //        return E_Role.Insuring;
        //    else if (roleId == s_CollateralAgentId)
        //        return E_Role.CollateralAgent;
        //    else if (roleId == s_DocDrawerId)
        //        return E_Role.DocDrawer;
        //    else
        //        return E_Role.Undefined;
        //}

        private string GetString(string fieldName)
        {
            if (!IsValid)
                return string.Empty;

            if (IsDirty == true)
            {
                return string.Empty;
            }
            if (m_row[fieldName] == DBNull.Value)
            {
                return string.Empty;
            }
            else
            {
                return m_row[fieldName].ToString();
            }
        }
        private Guid GetGuid(string fieldName)
        {
            if (!IsValid)
                return Guid.Empty;

            if (IsDirty == true)
            {
                return Guid.Empty;
            }
            return new Guid(m_row[fieldName].ToString());
        }

        private Nullable<DateTime> GetNullableDateTime(string fieldName)
        {
            if (!IsValid)
                return null;

            if (IsDirty == true)
            {
                return null;
            }
            if (m_row[fieldName] == DBNull.Value)
            {
                return null;
            }
            else
            {
                return (DateTime)m_row[fieldName];
            }
        }

        public Guid RoleId
        {
            get { return GetGuid("RoleId"); }
        }
        public Guid EmployeeId
        {
            get 
            {
                if (IsDirty)
                {
                    return m_tempNewEmployeeId;
                }
                else
                {
                    return GetGuid("EmployeeId");
                }
            }
        }

        public string PmlBrokerCompanyId
        {
            get { return GetString("PmlBrokerCompanyId"); }
        }

        public string FirstName
        {
            get { return GetString("UserFirstNm"); }
        }

        public string LastName
        {
            get { return GetString("UserLastNm"); }
        }

        public string Phone
        {
            get { return GetString("Phone"); }
        }

        public string Fax
        {
            get { return GetString("Fax"); }
        }

        public string Email
        {
            get { return GetString("Email"); }
        }

        public string PmlUserBrokerNm
        {
            get { return GetString("PmlBrokerName"); }
        }

        // Note, change this method will force us to re-run data migration for
        // loan_file_cache db table.
        public static string ComposeFullName(string firstNm, string lastNm)
        {
            string full = firstNm.TrimWhitespaceAndBOM() + " " + lastNm.TrimWhitespaceAndBOM();
            return full.TrimWhitespaceAndBOM();
        }

        public string FullName
        {
            get
            {
                return ComposeFullName(FirstName, LastName);
            }
        }

        public Nullable<DateTime> EmployeeStartD
        {
            get
            {
                return GetNullableDateTime("EmployeeStartD");
            }
        }
        public Nullable<DateTime> EmployeeTerminationD
        {
            get
            {
                return GetNullableDateTime("EmployeeTerminationD");
            }
        }

        public bool IsDirty { get; private set; }

        private Guid m_tempNewEmployeeId = Guid.Empty;
        public void SetNewEmployeeId(Guid employeeId)
        {
            IsDirty = true;
            m_tempNewEmployeeId = employeeId;
        }
    }
}

