using System;
using System.Data;
using System.IO;
using System.Text;
using LendersOffice.Constants;
using LqbGrammar.DataTypes;

namespace DataAccess
{
    public class CDataRow
	{
		private DataRow m_row;
		public CDataRow( DataRow row )
		{
			RecycleFor( row );
		}
		/// <summary>
		/// Use RecycleFor() with this constructor
		/// </summary>
		public  CDataRow() {}

		public void RecycleFor( DataRow row )
		{
			m_row = row;
		}

		public static Guid GetGuidField( DataRow rawRow, string columnName )
		{
			return new Guid( rawRow[columnName].ToString() ); 
		}
		public Guid GetGuidField( string columnName )
		{
			return new Guid( m_row[columnName].ToString() );
		}
		public Guid GetGuidField( string columnName, Guid nullSubstitute )
		{
			object rawObj = m_row[columnName];
			if( DBNull.Value == rawObj )
				return nullSubstitute;
			return new Guid( rawObj.ToString() );
		}
		public bool GetBoolField( string columnName )
		{
			return bool.Parse( m_row[columnName].ToString() );
		}
		public void SetBoolField( string columnName, bool newVal )
		{
			m_row[columnName] = newVal;
		}

		public E_TriState GetTriStateField( string columnName )
		{
			return (E_TriState) (Byte) (m_row[columnName]);
		}
		public void SetTriStateField( string columnName, E_TriState newVal )
		{
			m_row[columnName] = newVal;
		}
	}
	public class TempFileUtils
	{
        // NOTE: This code has been copied to LqbGrammar.Utils.TempFileUtils.  Please use those methods instead.
        //       Eventually this class should be eliminated in favor of the new location.  I'm not doing so now
        //       because the new class is part of experimental code related to using AWS as a replacement for FileDB.
		public static string Name2Path(string sTempFileName)
		{
			return Path.Combine(ConstApp.TempFolder, sTempFileName) ;
		}
		public static string NewTempFilePath()
		{
			return Name2Path(Guid.NewGuid().ToString()) ;
		}

        /// <summary>
        /// Get a temporary file location.
        /// 
        /// This method return a semantic type of LocalFilePath instead of string. Eventually we should replace call to NewTempFilePath with this method.
        /// </summary>
        /// <returns>A temporary file location.</returns>
        public static LocalFilePath NewTempFile()
        {
            string tempFile = Name2Path(Guid.NewGuid().ToString());

            LocalFilePath? obj = LocalFilePath.Create(tempFile);
            if (obj == null)
            {
                throw CBaseException.GenericException("TempFile path is not a valid path."); // 11/14/2016 - dd - Should never get here.
            }

            return obj.Value;
        }

	}

    public class TimerHelper 
    {
        private long m_startTicks;
        private long m_endTicks;

        public void start() 
        {
            m_startTicks = System.DateTime.Now.Ticks;
        }
        public void stop() 
        {
            m_endTicks = System.DateTime.Now.Ticks;
        }

        public string DurationString 
        {
            get 
            {
                TimeSpan ts = new TimeSpan(m_endTicks - m_startTicks);
                return GetDurationString(ts);

            }
        }

        private string GetDurationString(TimeSpan ts) 
        {
            StringBuilder sb = new StringBuilder();

            int hours = ts.Hours;
            int minutes = ts.Minutes;
            int seconds = ts.Seconds;
            int ms = ts.Milliseconds;

            sb.Append("[");
            if (hours > 0)
                sb.AppendFormat("{0} hrs, ", hours);
            if (minutes > 0)
                sb.AppendFormat("{0} minutes, ", minutes);
            if (seconds > 0)
                sb.AppendFormat("{0} seconds, ", seconds);
            sb.AppendFormat("{0} ms]", ms);

            return sb.ToString();

        }

    }
	
	public class SQLCommaStringBuilder
	{
		public string SQL
		{
			get { return m_sSQL ; }
		}
		public void Reset()
		{
			m_sSQL = "" ;
		}
		public void Add(string s)
		{
			if ("" == m_sSQL)
				m_sSQL = s ;
			else
				m_sSQL += ", " + s ;
		}

		private string m_sSQL = "" ;
	}
	public interface ISQLStringBuilder
	{
		void Reset() ;
		void AddString(string sField, string sValue) ;
		void AddNonString(string sField, string sValue) ;
		void AddObject(string sField, string sValue) ;
	}
	public class SQLInsertStringBuilder : ISQLStringBuilder
	{
		public override string ToString()
		{
			if ("" == m_oFields.SQL)
				return "" ;
			else
				return "(" + m_oFields.SQL + ") VALUES(" + m_oValues.SQL + ")" ;
		}
		public void Reset()
		{
			m_oFields.Reset() ;
			m_oValues.Reset() ;
		}
		public void AddString(string sField, string sValue)
		{
			m_oFields.Add(sField) ;
			m_oValues.Add(DbAccessUtils.SQLString(sValue)) ;
		}
		public void AddNonString(string sField, string sValue)
		{
			m_oFields.Add(sField) ;
			m_oValues.Add(sValue) ;
		}
		public void AddNonString(string sField, int nValue)
		{
			m_oFields.Add(sField) ;
			m_oValues.Add(nValue.ToString()) ;
		}
		public void AddNonString(string sField, double dblValue)
		{
			m_oFields.Add(sField) ;
			m_oValues.Add(dblValue.ToString()) ;
		}
		public void AddObject(string sField, string sValue)
		{
			m_oFields.Add(sField) ;

			if (sValue.Length > 0)
			{
				m_oValues.Add(DbAccessUtils.SQLString(sValue)) ;
			}
			else
			{
				m_oValues.Add("null") ;
			}
		}

		private SQLCommaStringBuilder m_oFields = new SQLCommaStringBuilder() ;
		private SQLCommaStringBuilder m_oValues = new SQLCommaStringBuilder() ;
	}
	public class SQLUpdateStringBuilder : ISQLStringBuilder
	{
		public override string ToString()
		{
			if ("" == m_oFields.SQL)
				return "" ;
			else
				return "SET " + m_oFields.SQL ;
		}
		public void Reset()
		{
			m_oFields.Reset() ;
		}
		public void AddString(string sField, string sValue)
		{
			m_oFields.Add(sField + "=" + DbAccessUtils.SQLString(sValue)) ;
		}
		public void AddNonString(string sField, string sValue)
		{
			m_oFields.Add(sField + "=" + sValue) ;
		}
		public void AddNonString(string sField, int nValue)
		{
			m_oFields.Add(sField + "=" + nValue.ToString()) ;
		}
		public void AddNonString(string sField, double dblValue)
		{
			m_oFields.Add(sField + "=" + dblValue.ToString()) ;
		}
		public void AddObject(string sField, string sValue)
		{
			if (sValue.Length > 0)
			{
				m_oFields.Add(sField + "=" + DbAccessUtils.SQLString(sValue)) ;
			}
			else
			{
				m_oFields.Add(sField + "=" + "null") ;
			}
		}

		private SQLCommaStringBuilder m_oFields = new SQLCommaStringBuilder() ;
	}
}