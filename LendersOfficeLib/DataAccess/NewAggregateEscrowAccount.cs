﻿// <copyright file="NewAggregateEscrowAccount.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   6/15/2015 
// </summary>
namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Class for NewAggregateEscrowAccount.
    /// </summary>
    public class NewAggregateEscrowAccount : AggregateEscrowAccount
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NewAggregateEscrowAccount" /> class.
        /// </summary>
        /// <param name="dataLoan">The data loan to calculate from.</param>
        public NewAggregateEscrowAccount(CPageBase dataLoan)
        {
            this.m_dataLoan = dataLoan;
            this.m_initialDeposit = 0.0M;
            this.m_aggregateEscrowAdjustment = 0.0M;
            this.m_aggregateItems = new AggregateEscrowItem[12];

            if (this.m_dataLoan != null && this.m_dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy && this.m_dataLoan.sIsHousingExpenseMigrated &&
               this.m_dataLoan.sAggEscrowCalcModeT != E_AggregateEscrowCalculationModeT.Legacy && this.m_dataLoan.sSchedDueD1.IsValid)
            {
                this.m_initialDeposit = this.m_dataLoan.sGfeInitialImpoundDeposit;

                /* The aggregate adjustment reserve must be calculated outside this class due to sGfeInitialImpoundDeposit needing the
                 * aggregate adjustment in its calculation.
                 */
                this.m_aggregateEscrowAdjustment = this.m_dataLoan.sAggregateAdjRsrv;

                this.PopulateItems();
            }
            else
            {
                // return empty table.
                for (int i = 0; i < 12; i++)
                {
                    this.m_aggregateItems[i] = new AggregateEscrowItem(dataLoan, -1);
                }

                return;
            }
        }

        /// <summary>
        /// Populates the aggregate escrow items array. Done separately to prevent infinite loops.
        /// </summary>
        protected void PopulateItems()
        {
            List<BaseHousingExpense> expenses = this.m_dataLoan.sHousingExpenses.ExpensesToUse;

            // Start at index 1.
            decimal[] aggTotalPerMonth = new decimal[13];

            // Initialize the aggregate item array.
            DateTime startDate = this.m_dataLoan.sSchedDueD1.DateTimeForComputation;
            DateTime endDate = startDate.AddYears(1).AddDays(-1);
            DateTime track = startDate;
            int index = 0;

            while (track <= endDate)
            {
                int month = track.Month;

                if (this.m_aggregateItems[index] == null)
                {
                    this.m_aggregateItems[index] = new AggregateEscrowItem(this.m_dataLoan, month);
                }

                index++;
                track = track.AddMonths(1);
            }

            for (int expNum = 0; expNum < expenses.Count; expNum++)
            {
                if (expenses[expNum] == null || expenses[expNum].IsEscrowedAtClosing != E_TriState.Yes)
                {
                    continue;
                }

                decimal monthlyTotal = 0;
                decimal[] monthlyDisbursementAmt = new decimal[13];
                decimal annualUnused = 0;

                expenses[expNum].CalcPBC_Annual_Monthly(ref monthlyDisbursementAmt, out annualUnused, out monthlyTotal);
                
                // monthlyTotal is the monthly servicing amount. I didn't want to call the property since it would recalculate. 
                monthlyTotal = Math.Round(monthlyTotal, 2, MidpointRounding.AwayFromZero);

                DateTime trackDate = startDate;
                int aggItemIndex = 0;
                while (trackDate <= endDate)
                {
                    int month = trackDate.Month;
                    
                    this.m_aggregateItems[aggItemIndex].PmtToEscrow += monthlyTotal;

                    if (monthlyDisbursementAmt[month] != 0)
                    {
                        this.m_aggregateItems[aggItemIndex].PmtFromEscrow += monthlyDisbursementAmt[month];
                        this.m_aggregateItems[aggItemIndex].Desc += expenses[expNum].ExpenseDescription + Environment.NewLine;
                    }

                    aggItemIndex++;
                    trackDate = trackDate.AddMonths(1);
                }
            }

            int[] mortInsSched = this.m_dataLoan.sHousingExpenses.MIEscrowSchedule;
            int startMonth = this.m_dataLoan.sSchedDueD1.DateTimeForComputation.Month;
            for (int i = 0; i < this.m_aggregateItems.Length; i++)
            {
                AggregateEscrowItem item = this.m_aggregateItems[i];
                
                // Add MI
                item.PmtToEscrow += this.m_dataLoan.sProMIns;

                decimal pmtFrom = this.m_dataLoan.sProMIns * mortInsSched[item.MonInt];
                if (pmtFrom != 0)
                {
                    item.PmtFromEscrow += pmtFrom;
                    item.Desc += "Mortgage Insurance" + Environment.NewLine;
                }
                
                // Set account balance. 
                if (i == 0)
                {
                    item.Bal = this.InitialDeposit + item.PmtToEscrow - item.PmtFromEscrow;
                }
                else
                {
                    AggregateEscrowItem prevItem = this.m_aggregateItems[i - 1];
                    item.Bal = prevItem.Bal + item.PmtToEscrow - item.PmtFromEscrow;
                }
            }
        }
    }
}
