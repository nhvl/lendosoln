﻿namespace DataAccess.TempFile
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// A class that encapsulates result information from downloading a temporary file.
    /// </summary>
    [Serializable]
    public class TempFileDownloadResult
    {
        /// <summary>
        /// Gets or sets the array of bytes downloaded.
        /// </summary>
        /// <value>The byte array read during the download.</value>
        public byte[] Bytes { get; set; }

        /// <summary>
        /// Gets or sets the number of bytes downloaded.
        /// </summary>
        /// <value>The number of bytes read during the download.</value>
        public int NumBytesRead { get; set; }

        /// <summary>
        /// Gets or sets a list of errors that occurred during the download, or empty if it succeeded.
        /// </summary>
        /// <value>The list of errors that occurred during the download, or empty if the download succeeded.</value>
        public List<string> Errors { get; set; } = new List<string>();
    }
}
