﻿namespace DataAccess.TempFile
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using LqbGrammar.Drivers.SecurityEventLogging;

    /// <summary>
    /// Can be used to manage the CRUD operations of files in the temp directory. <para></para>
    /// Note that a user will only have access to the files they themselves created through this, or if we explicitly granted them access. <para></para>
    /// Non LoAuth production servers will not allow any of these operations. <para></para>
    /// A user can lose access to a file they created if: <para></para>
    /// 1) They lose access to the operation itself (e.g. via stage config variable.) <para></para>
    /// 2) The server is reset (which would kill the memory cache.) <para></para>
    /// 3) Their cached permission expired (controlled by stage variable SlidingMinutesToKeepUsersAccessToTemporaryFile). <para></para>
    /// </summary>
    public class TempFileManager
    {
        // tdsk: consider joining the access manager with this.
        // tdsk: add some internal alert(email) if user tries to export/import upload/download with wrong login.

        /// <summary>
        /// Creates the file with the specified number of bytes, and allows the user access.<para></para>
        /// </summary>
        /// <param name="principal">The principal on whose behalf this operation is running.</param>
        /// <param name="numBytes">The number of bytes to allocate for the file.</param>
        /// <returns>A result with information such as the created file name or the list of errors.</returns>
        public static TempFileCreationResult CreateFile(AbstractUserPrincipal principal, long numBytes)
        {
            var result = new TempFileCreationResult();
            if (!UserCanCreate(principal))
            {
                result.Errors.Add("You are not permitted to create files.");
                return result;
            }

            if (numBytes > ConstStage.AllowedBytesPerFileInTempForSpecialUsers)
            {
                result.Errors.Add($"file would exceed maximum allowed file size of {ConstStage.AllowedBytesPerFileInTempForSpecialUsers}.");
                return result;
            }

            var fullFileName = TempFileUtils.NewTempFile().Value;

            using (var fw = File.OpenWrite(fullFileName))
            {
                fw.SetLength(numBytes);
            }

            var fileNameNoPath = Path.GetFileName(fullFileName);
            result.CreatedFileName = fileNameNoPath;
            TempFileAccessManager.AssertThatCurrentUserCreatedFile(principal, fileNameNoPath);

            return result;
        }

        /// <summary>
        /// Writes a chunk of bytes to the specified file in the temp folder if the user created the file, <para></para>
        /// starting at the specified offset relative to the beginning of the file.
        /// </summary>
        /// <param name="principal">The principal on whose behalf this operation is running.</param>
        /// <param name="fileNameInTempFolder">The name of the file relative to the temp folder.</param>
        /// <param name="offset">The offset relative to the beginning of the file to write to.</param>
        /// <param name="bytes">The bytes to write.</param>
        /// <returns>The result which contains any errors.</returns>
        public static TempFileUploadResult WriteChunkToFile(AbstractUserPrincipal principal, string fileNameInTempFolder, long offset, byte[] bytes)
        {
            var result = new TempFileUploadResult();

            if (!UserCanOverWrite(principal))
            {
                result.Errors.Add("You are not permitted to overwrite files.");
                return result;
            }

            if (!TempFileAccessManager.DoesFileBelongToCurrentUser(principal, fileNameInTempFolder))
            {
                result.Errors.Add("You did not create the file so you can not upload to it.");
                return result;
            }

            if (bytes.Length > ConstStage.AllowedBytesPerReadWriteOverSoap)
            {
                result.Errors.Add($"You cannot upload more than {ConstStage.AllowedBytesPerReadWriteOverSoap} at a time.");
                return result;
            }

            if (offset < 0)
            {
                result.Errors.Add($"offset must be > 0 but was {offset}");
                return result;
            }

            var fullFileName = TempFileUtils.Name2Path(fileNameInTempFolder);
            var fileInfo = new FileInfo(fullFileName);
            if (offset >= fileInfo.Length)
            {
                result.Errors.Add($"offset {offset} is past the file length of {fileInfo.Length}.");
                return result;
            }

            using (var fw = File.Open(fullFileName, FileMode.OpenOrCreate, FileAccess.Write))
            {
                fw.Seek(offset, SeekOrigin.Begin);
                fw.Write(bytes, 0, bytes.Length);
            }

            return result;
        }

        /// <summary>
        /// Reads a chunk of bytes from the specified file in the temp folder if the user created the file, <para></para>
        /// starting at the specified offset relative to the beginning of the file.
        /// </summary>
        /// <param name="principal">The principal on whose behalf this operation is running.</param>
        /// <param name="fileNameInTempFolder">The name of the file relative to the temp folder.</param>
        /// <param name="offset">The offset relative to the beginning of the file to start reading from.</param>
        /// <param name="maxNumberOfBytes">The maximum number of bytes to read.</param>
        /// <returns>The result which contains any errors, as well as the chunk read.</returns>
        public static TempFileDownloadResult GetChunkFromFile(AbstractUserPrincipal principal, string fileNameInTempFolder, long offset, int maxNumberOfBytes)
        {
            var result = new TempFileDownloadResult();

            if (!UserCanDownload(principal))
            {
                result.Errors.Add("You are not permitted to download files.");
                return result;
            }

            if (!TempFileAccessManager.DoesFileBelongToCurrentUser(principal, fileNameInTempFolder))
            {
                result.Errors.Add("You did not create the file so you can not download from it.");
                return result;
            }

            if (maxNumberOfBytes > ConstStage.AllowedBytesPerReadWriteOverSoap)
            {
                result.Errors.Add($"You cannot download more than {ConstStage.AllowedBytesPerReadWriteOverSoap} at a time.");
                return result;
            }

            if (maxNumberOfBytes <= 0)
            {
                result.Errors.Add($"maxNumberOfBytes must be > 0 but was {maxNumberOfBytes}");
                return result;
            }

            if (offset < 0)
            {
                result.Errors.Add($"offset must be > 0 but was {offset}");
                return result;
            }

            var fullFileName = TempFileUtils.Name2Path(fileNameInTempFolder);

            var fileInfo = new FileInfo(fullFileName);
            if (offset >= fileInfo.Length)
            {
                result.Errors.Add($"offset {offset} is past the file length of {fileInfo.Length}.");
                return result;
            }

            using (var fs = File.Open(fullFileName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                fs.Seek(offset, SeekOrigin.Begin);
                var bytes = new byte[maxNumberOfBytes];

                var numBytesRead = fs.Read(bytes, 0, maxNumberOfBytes);
                result.NumBytesRead = numBytesRead;
                if (maxNumberOfBytes != numBytesRead)
                {
                    var curtailedBytes = new byte[result.NumBytesRead];
                    Array.Copy(bytes, curtailedBytes, numBytesRead);
                    result.Bytes = curtailedBytes;
                }
                else
                {
                    result.Bytes = bytes;
                }
            }

            return result;
        }

        /// <summary>
        /// Determines if the user is allowed to use temp files depending on the stage, server, and user type, login name and ip.
        /// </summary>
        /// <param name="principal">The principal on whose behalf this operation is running.</param>
        /// <param name="requiredLoginNamesOfBUsers">A B user must have one of these login names to do the thing.</param>
        /// <param name="requiredIpAddressesOfBUsers">A B user must have one of these ip addresses to do the thing.</param>
        /// <returns>Whether or not hte user is allowed to do the thing.</returns>
        private static bool UserIsAllowed(AbstractUserPrincipal principal, IEnumerable<string> requiredLoginNamesOfBUsers, IEnumerable<string> requiredIpAddressesOfBUsers)
        {
            if (requiredLoginNamesOfBUsers == null)
            {
                throw new ArgumentNullException(nameof(requiredLoginNamesOfBUsers));
            }

            if (requiredIpAddressesOfBUsers == null)
            {
                throw new ArgumentNullException(nameof(requiredIpAddressesOfBUsers));
            }
            
            if (principal == null)
            {
                return false;
            }

            var requestIsLoauth = Tools.RequestIsOnLoauth() ?? false;
            if (ConstAppDavid.CurrentServerLocation == ServerLocation.Production
                && !requestIsLoauth)
            {
                return false;
            }

            if (principal is InternalUserPrincipal || principal is SystemUserPrincipal)
            {
                return true;
            }

            if (HttpContext.Current == null)
            {
                return false;
            }

            if (principal is BrokerUserPrincipal 
                && principal.ApplicationType == E_ApplicationT.LendersOffice
                && requiredLoginNamesOfBUsers.Contains(principal.LoginNm, StringComparer.OrdinalIgnoreCase)
                && requiredIpAddressesOfBUsers.Contains(RequestHelper.ClientIP))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Whether or not the user can create a file in the temp directory.
        /// </summary>
        /// <param name="principal">The principal on whose behalf this operation is running.</param>
        /// <returns>True iff the user can create a file in the temp directory.</returns>
        private static bool UserCanCreate(AbstractUserPrincipal principal)
        {
            return UserIsAllowed(principal, ConstStage.LoginNamesRequiredOfBUsersForBrokerReplicateServiceMethods, ConstStage.IpsRequiredOfBUsersForBrokerReplicateServiceMethods);
        }

        /// <summary>
        /// Whether or not the user can overwrite a file in the temp directory, which is true only if they created it and they can overwrite temp files.
        /// </summary>
        /// <param name="principal">The principal on whose behalf this operation is running.</param>
        /// <returns>True iff the user can overwrite a file in the temp directory.</returns>
        private static bool UserCanOverWrite(AbstractUserPrincipal principal)
        {
            return UserIsAllowed(principal, ConstStage.LoginNamesRequiredOfBUsersForBrokerReplicateServiceMethods, ConstStage.IpsRequiredOfBUsersForBrokerReplicateServiceMethods)
                && ConstAppDavid.CurrentServerLocation != ServerLocation.Production;
        }

        /// <summary>
        /// Whether or not the user can download a file in the temp directory, which is true only if they created it and they can download temp files.
        /// </summary>
        /// <param name="principal">The principal on whose behalf this operation is running.</param>
        /// <returns>True iff the user can download a file in the temp directory.</returns>
        private static bool UserCanDownload(AbstractUserPrincipal principal)
        {
            return UserIsAllowed(principal, ConstStage.LoginNamesRequiredOfBUsersForBrokerReplicateServiceMethods, ConstStage.IpsRequiredOfBUsersForBrokerReplicateServiceMethods);
        }

        /// <summary>
        /// Whether or not the user can delete a file in the temp directory, which is true only if they created it and they can delete temp files.
        /// </summary>
        /// <param name="principal">The principal on whose behalf this operation is running.</param>
        /// <returns>True iff the user can delete a file in the temp directory.</returns>
        private static bool UserCanDelete(AbstractUserPrincipal principal)
        {
            return UserIsAllowed(principal, ConstStage.LoginNamesRequiredOfBUsersForBrokerReplicateServiceMethods, ConstStage.IpsRequiredOfBUsersForBrokerReplicateServiceMethods)
                 && ConstAppDavid.CurrentServerLocation != ServerLocation.Production;
        }
    }
}
