﻿namespace DataAccess.TempFile
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// A class that encapsulates result information from uploading a temporary file.
    /// </summary>
    [Serializable]
    public class TempFileUploadResult
    {
        /// <summary>
        /// Gets or sets a list of errors that occurred during the upload, or empty if it succeeded.
        /// </summary>
        /// <value>The list of errors that occurred during the upload, or empty if the upload succeeded.</value>
        public List<string> Errors { get; set; } = new List<string>();
    }
}
