﻿namespace DataAccess.TempFile
{
    using System;
    using System.IO;
    using System.Runtime.Caching;
    using LendersOffice.Constants;
    using LendersOffice.Security;

    /// <summary>
    /// The goal of this class is to prevent users from reading from or writing to files that they themselves did not create. <para></para>
    /// You probably only want to add/check for 'B' users, but this will also add the access for any user type.
    /// Note: the file must exist in the temp directory to get it's existence.
    /// </summary>
    public class TempFileAccessManager
    {
        /// <summary>
        /// The cache of users access to temporary files.
        /// </summary>
        private static readonly MemoryCache AccessCache = new MemoryCache("TempFileAccessCache");

        /// <summary>
        /// This is only public to allow calling it when the file was generated automatically.  Otherwise use TempFileManager. <para></para>
        /// Marks the file as accessible to the user.
        /// </summary>
        /// <param name="principal">The principal of the user for which we are running the operation.</param>
        /// <param name="fileNameInTempFolder">The fileName relative to the temp directory.</param>
        public static void AssertThatCurrentUserCreatedFile(AbstractUserPrincipal principal, string fileNameInTempFolder)
        {
            var usersFilesKey = GetAccessKeyForCurrentUserAndFile(principal, fileNameInTempFolder);
            AccessCache.AddOrGetExisting(usersFilesKey, true, new CacheItemPolicy() { SlidingExpiration = TimeSpan.FromMinutes(ConstStage.SlidingMinutesToKeepUsersAccessToTemporaryFile) });
        }

        /// <summary>
        /// Returns true if we marked the file as created by the user.
        /// </summary>
        /// <param name="principal">The principal of the user for which we are running the operation.</param>
        /// <param name="fileNameInTempFolder">The fileName relative to the temp directory.</param>
        /// <returns>Whether or not the file was marked as accessible to the user.</returns>
        public static bool DoesFileBelongToCurrentUser(AbstractUserPrincipal principal, string fileNameInTempFolder)
        {
            var usersFilesKey = GetAccessKeyForCurrentUserAndFile(principal, fileNameInTempFolder);
            return AccessCache.Contains(usersFilesKey);
        }        

        /// <summary>
        /// Gets the key to determine whether or no the user can access the file.
        /// </summary>
        /// <param name="principal">The principal of the user for which we are running the operation.</param>
        /// <param name="fileNameInTempFolder">The fileName relative to the temp directory.</param>
        /// <returns>The key for checking the users acess to the file.</returns>
        private static string GetAccessKeyForCurrentUserAndFile(AbstractUserPrincipal principal, string fileNameInTempFolder)
        {
            var fullFileName = TempFileUtils.Name2Path(fileNameInTempFolder);
            var fi = new FileInfo(fullFileName);
            if (!fi.Exists)
            {
                throw new ArgumentException($"file {fileNameInTempFolder} does not exist in the temp directory.", nameof(fileNameInTempFolder));
            }

            var keyFileName = new Uri(ConstApp.TempFolder + Path.DirectorySeparatorChar).MakeRelativeUri(new Uri(fullFileName)).ToString();
            
            var usersFilesKey = principal.UserId.ToString("N") + "_" + keyFileName;
            return usersFilesKey;
        }
    }
}
