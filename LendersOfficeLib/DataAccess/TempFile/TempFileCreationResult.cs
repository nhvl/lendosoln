﻿namespace DataAccess.TempFile
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// A class that encapsulates result information from creating a temporary file.
    /// </summary>
    [Serializable]
    public class TempFileCreationResult
    {
        /// <summary>
        /// Gets or sets the name of the file (not the path) created in the temporary folder on the server.
        /// </summary>
        /// <value>The name fo the file created on the server in the temporary directory.</value>
        public string CreatedFileName { get; set; }

        /// <summary>
        /// Gets or sets a list of errors that occurred during the creation, or empty if it succeeded.
        /// </summary>
        /// <value>The list of errors that occurred during the creation, or empty if the creation succeeded.</value>
        public List<string> Errors { get; set; } = new List<string>();
    }
}
