﻿namespace DataAccess
{
    using System;

    public interface IPreparerFields
    {
        // Data

        string CaseNum { get; set; }
        string CellPhone { get; set; }
        string ChumsId { get; set; }
        string City { get; set; }
        string CityStateZip { get; }
        string CompanyLoanOriginatorIdentifier { get; set; }
        string CompanyName { get; set; }
        string County { get; set; }
        string DepartmentName { get; set; }
        string EmailAddr { get; set; }
        string FaxNum { get; set; }
        string FaxOfCompany { get; set; }
        string LicenseNumOfAgent { get; set; }
        bool LicenseNumOfAgentIsExpired { get; }
        string LicenseNumOfCompany { get; set; }
        bool LicenseNumOfCompanyIsExpired { get; }
        string LoanOriginatorIdentifier { get; set; }
        string PagerNum { get; set; }
        string Phone { get; set; }
        string PhoneOfCompany { get; set; }
        string PreparerName { get; set; }
        string State { get; set; }
        string StreetAddr { get; set; }
        string StreetAddrMultiLines { get; }
        string StreetAddrSingleLine { get; }
        string TaxId { get; set; }
        string Title { get; set; }
        string Zip { get; set; }

        CDateTime PrepareDate { get; set; }
        string PrepareDate_rep { get; set; }

        // State management

        Guid AgentId { get; set; }
        E_AgentRoleT AgentRoleT { get; set; }
        bool ChumsIdLocked { get; set; }
        bool HadInitialCheck { get; set; }
        bool IsEmpty { get; }
        bool IsLocked { get; set; }
        bool NameLocked { get; set; }
        E_PreparerFormT PreparerFormT { get; }
        bool TaxIdValid { get; }

        bool IsNewRecord { get; }
        bool IsValid { get; }

        void SyncFromAgent(CAgentFields agent);
        void SyncToAgent(CAgentFields agent);
        void overrideLicenseLocked(bool val);
        void overrideNameLocked(bool val);
        void Update();
    }
}