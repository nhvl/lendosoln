﻿using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
    public class TaskConditionExporterData : CPageData
    {
        private static CSelectStatementProvider s_selectProvider;
	    
        static TaskConditionExporterData()
        {
            StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("sLId");
            list.Add("sfGetCondRecordCount");
            list.Add("sfGetCondFields");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }

        public TaskConditionExporterData(Guid fileId) : base(fileId, "TaskConditionExporterData", s_selectProvider)
        {
        }
    }
}