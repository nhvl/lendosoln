using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.UI;

namespace DataAccess
{
    /// <summary>
    /// Use this class to assign employee to role for loan file.
    /// </summary>

	public class CPeopleData : CPageData
	{
		private static CSelectStatementProvider s_selectProvider;
		static CPeopleData()
		{
			StringList list = new StringList();

            list.Add( "sPreparerXmlContent" );
            list.Add( "sStatusT" );
            list.Add("sLAmtCalc");
            list.Add("sGrossProfit");
            list.Add("sSpState");
            list.Add("sfGetAgentOfRole");
            list.Add("sBranchId");
            list.Add("sfAssignBranch");
            list.Add("sClosingCostFeeVersionT");
			s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

		}

        public CPeopleData( Guid fileId ) : base( fileId , "AllPeopleTab" , s_selectProvider )
        {
        }

	}

    /// <summary>
    /// Use this class to assign employee to role for loan file.
    /// </summary>
    public class CEmployeeData : CPageData
    {
		private static CSelectStatementProvider s_selectProvider;

		protected override Boolean m_enforceAccessControl
		{
			// 8/25/2005 kb - Again, we seem to need to bypass access
			// control when setting up employee assignment fixups.

			get
			{
				return false;
			}
		}

		static CEmployeeData()
		{
			StringList list = new StringList();

            list.Add( "sBranchId" );
            list.Add( "sEmployeeCallCenterAgent" );
            list.Add( "sEmployeeLoanOpener" );
            list.Add( "sEmployeeLoanRep" );
            list.Add( "sEmployeeManager" );
            list.Add( "sEmployeeProcessor" );
            list.Add( "sEmployeeRealEstateAgent" );
			list.Add( "sEmployeeLenderAccExec" );
			list.Add( "sEmployeeLockDesk" );
			list.Add( "sEmployeeUnderwriter" );
            list.Add( "sEmployeeShipper");
            list.Add( "sEmployeeFunder");
            list.Add( "sEmployeePostCloser");
            list.Add( "sEmployeeInsuring");
            list.Add( "sEmployeeCollateralAgent");
            list.Add( "sEmployeeDocDrawer");
            list.Add( "sEmployeeCreditAuditor");
            list.Add( "sEmployeeDisclosureDesk");
            list.Add( "sEmployeeJuniorProcessor");
            list.Add( "sEmployeeJuniorUnderwriter");
            list.Add( "sEmployeeLegalAuditor");
            list.Add( "sEmployeeLoanOfficerAssistant");
            list.Add( "sEmployeePurchaser");
            list.Add( "sEmployeeQCCompliance");
            list.Add( "sEmployeeSecondary");
            list.Add( "sEmployeeServicing");
            list.Add( "sEmployeeExternalSecondary");
            list.Add( "sEmployeeExternalPostCloser");
			list.Add( "sStatusLckd" );
            list.Add( "sStatusT" );
            list.Add( "sOpenedD" );
            list.Add( "sPreQualD" );
            list.Add( "sPreApprovD" );
            list.Add( "sSubmitD" );
            list.Add( "sApprovD" );
            list.Add( "sDocsD" );
            list.Add( "sFundD" );
            list.Add( "sOnHoldD" );
            list.Add( "sSuspendedD" );
            list.Add( "sCanceledD" );
            list.Add( "sRejectD" );
            list.Add( "sClosedD" );
            list.Add( "sLeadD" );
            list.Add( "sUnderwritingD" );
            list.Add( "sLeadCanceledD" );
            list.Add( "sLeadDeclinedD" );
            list.Add( "sRecordedD" );
			list.Add( "sLNm" );
			list.Add( "sPrimBorrowerFullNm" );
            list.Add("sAgentCollection");
            list.Add("sShippedToInvestorD");
            list.Add("sSpStatePe");
            list.Add("sfRecalculateTpoValue");
            list.Add("sLockPolicy");
            list.Add("sBranchChannelT");
            list.Add("sCorrespondentProcessT");
            list.Add("sfAssignBranch");
            list.Add("sAgentXmlContent");

			s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

		}

        public CEmployeeData( Guid fileId ) : base( fileId , "CEmployeeData" , s_selectProvider )
        {
        }

    }


    public class CEmployeeDataWithAccessControl : CPageData
    {
        private static CSelectStatementProvider s_selectProvider;

        protected override Boolean m_enforceAccessControl
        {
            get
            {
                return true;
            }
        }

        static CEmployeeDataWithAccessControl()
        {
            StringList list = new StringList();

            list.Add("sBranchId");
            list.Add("sEmployeeCallCenterAgent");
            list.Add("sEmployeeLoanOpener");
            list.Add("sEmployeeLoanRep");
            list.Add("sEmployeeManager");
            list.Add("sEmployeeProcessor");
            list.Add("sEmployeeRealEstateAgent");
            list.Add("sEmployeeLenderAccExec");
            list.Add("sEmployeeLockDesk");
            list.Add("sEmployeeUnderwriter");
            list.Add("sEmployeeShipper");
            list.Add("sEmployeeFunder");
            list.Add("sEmployeePostCloser");
            list.Add("sEmployeeInsuring");
            list.Add("sEmployeeCollateralAgent");
            list.Add("sEmployeeDocDrawer");
            list.Add("sEmployeeCreditAuditor");
            list.Add("sEmployeeDisclosureDesk");
            list.Add("sEmployeeJuniorProcessor");
            list.Add("sEmployeeJuniorUnderwriter");
            list.Add("sEmployeeLegalAuditor");
            list.Add("sEmployeeLoanOfficerAssistant");
            list.Add("sEmployeePurchaser");
            list.Add("sEmployeeQCCompliance");
            list.Add("sEmployeeSecondary");
            list.Add("sEmployeeServicing");
            list.Add("sEmployeeExternalSecondary");
            list.Add("sEmployeeExternalPostCloser");
            list.Add("sStatusLckd");
            list.Add("sStatusT");
            list.Add("sBranchChannelT");
            list.Add("sOpenedD");
            list.Add("sPreQualD");
            list.Add("sPreApprovD");
            list.Add("sSubmitD");
            list.Add("sApprovD");
            list.Add("sDocsD");
            list.Add("sFundD");
            list.Add("sOnHoldD");
            list.Add("sSuspendedD");
            list.Add("sCanceledD");
            list.Add("sRejectD");
            list.Add("sClosedD");
            list.Add("sLeadD");
            list.Add("sUnderwritingD");
            list.Add("sLeadCanceledD");
            list.Add("sLeadDeclinedD");
            list.Add("sLeadOtherD");
            list.Add("sRecordedD");
            list.Add("sLNm");
            list.Add("sPrimBorrowerFullNm");
            list.Add("sAgentCollection");
            list.Add("sShippedToInvestorD");
            list.Add("sSpStatePe");
            list.Add("sfRecalculateTpoValue");
            list.Add("sCorrespondentProcessT");
            list.Add("sfAssignBranch");

            s_selectProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection);

        }

        public CEmployeeDataWithAccessControl(Guid fileId)
            : base(fileId, "CEmployeeDataWithAccessControl", s_selectProvider)
        {
        }

    }
	
}