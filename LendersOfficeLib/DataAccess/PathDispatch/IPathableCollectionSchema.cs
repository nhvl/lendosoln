﻿namespace DataAccess.PathDispatch
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Interface for Schemas that hold path dispath information for <see cref="IPathResolvable" /> collections of <see cref="IPathResolvable" /> records.
    /// This interface should be used by any pathable object that would be represented by both a <see cref="DataPathCollectionElement"/> and a
    /// <see cref="DataPathSelectionElement"/> in the path.
    /// </summary>
    public interface IPathableCollectionSchema : IPathableRecordSchema
    {
        /// <summary>
        /// Gets the type of the <see cref="IPathResolvable"/> collection represented by this schema.
        /// </summary>
        /// <value>The type of the <see cref="IPathResolvable"/> collection represented by this schema.</value>
        Type CollectionType { get; }

        /// <summary>
        /// Gets a function that ruturns all records in this collection.
        /// </summary>
        /// <value>A function that returns all records in this collection.</value>
        Func<IPathResolvable, IEnumerable<IPathResolvable>> GetAllRecordsFunc { get; }

        /// <summary>
        /// Gets a function that returns the record at the given int index in the collection.
        /// </summary>
        /// <value>A function that returns the record at the given index in the collection.</value>
        /// <remarks>
        /// Collections should be indexable either by ID or by index. NOT BOTH.
        /// Only use if GetRecordTypes returns the Empty Set.
        /// </remarks>
        Func<IPathResolvable, int, IPathResolvable> GetAtIndexFunc { get; }

        /// <summary>
        /// Gets a function that takes in a record from the collection and returns its ID.
        /// </summary>
        /// <value>A function that takes in a record from the collection and returns its ID.</value>
        Func<IPathResolvable, string> RecordIdGetter { get; }

        /// <summary>
        /// Returns an enumerable of pathable record types. May vary by broker.
        /// </summary>
        /// <param name="brokerId">The Broker ID.</param>
        /// <returns>An enumerable of <see cref="PathableRecordTypeInfo"/> objects.</returns>
        /// <remarks>
        /// Collections should be indexable either by ID or by index. NOT BOTH.
        /// Only use if GetAtIndex is null.
        /// </remarks>
        IReadOnlyDictionary<string, PathableRecordTypeInfo> GetRecordTypes(Guid brokerId);

        /// <summary>
        /// Returns a default record for the given record type from the given broker.
        /// </summary>
        /// <param name="brokerId">The broker ID.</param>
        /// <param name="typeId">Record type ID as string.</param>
        /// <param name="collection">Pathable collection represented by this schema.</param>
        /// <returns>An <see cref="IPathResolvable"/> representing a default record of the given type.</returns>
        IPathResolvable GetDefaultRecord(Guid brokerId, string typeId, IPathResolvable collection);

        /// <summary>
        /// Returns an enumerable of pathable subset info.
        /// </summary>
        /// <returns>An enumerable of <see cref="PathableSubsetInfo"/> objects.</returns>
        IReadOnlyDictionary<string, PathableSubsetInfo> GetSubsets();
    }
}
