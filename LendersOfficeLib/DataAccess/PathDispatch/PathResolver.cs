﻿namespace DataAccess.PathDispatch
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    using LendersOffice.Common;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// A static class to hold methods for handling path dispatch.
    /// </summary>
    public static class PathResolver
    {
        /// <summary>
        /// Gets the set of dependency fields for the given path.
        /// </summary>
        /// <param name="pathString">Data path in string form.</param>
        /// <returns>The dependency field set for the given path.</returns>
        public static IEnumerable<string> GetPathDependencyFields(string pathString)
        {
            HashSet<string> dependencies = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

            // Get schema for head and add it's dependencies.
            DataPath path = DataPath.Create(pathString);
            IPathableRecordSchema loanSchema = PathSchemaManager.GetSchemaByType(typeof(CPageData));

            PathableFieldInfo pathFieldInfo;
            if (!loanSchema.GetFields().TryGetValue(path.Head.Name, out pathFieldInfo))
            {
                throw new CBaseException(ErrorMessages.Generic, $"Path is invalid.");
            }

            IPathableRecordSchema fieldSchema = PathSchemaManager.GetSchemaByType(pathFieldInfo.FieldType);
            foreach (string dependency in fieldSchema.LoanFileDependencies)
            {
                dependencies.Add(dependency);
            }

            return dependencies;
        }

        /// <summary>
        /// Attempts to get the data in the location specified by the path using
        /// the schema, retrieved based on target type.
        /// </summary>
        /// <param name="brokerId">Broker ID, used for data retrieval.</param>
        /// <param name="target">The root object to apply the path against.</param>
        /// <param name="path">The path to the desired data.</param>
        /// <returns>The data specified by the path.</returns>
        public static IEnumerable<object> GetUsingSchema(Guid brokerId, IPathResolvable target, DataPath path)
        {
            return GetUsingSchema(brokerId, new IPathResolvable[] { target }, path);
        }

        /// <summary>
        /// Attempts to get the data in the location specified by the element
        /// from the target via reflection. This will get any public string
        /// property, or the _rep version of a non-string property.
        /// WARNING: this is potentially a huge security risk.
        /// </summary>
        /// <param name="target">The target from which to request data.</param>
        /// <param name="element">The location of the data being requested.</param>
        /// <returns>The data that was requested.</returns>
        /// <remarks>WE SHOULD EVENTUALLY GET RID OF THIS METHOD. WE DON'T WANT TO USE REFLECTION AT RUNTIME.</remarks>
        public static string GetStringUsingReflection(object target, IDataPathElement element)
        {
            if (!(element is DataPathBasicElement))
            {
                throw new NotImplementedException("Path element type " + element.GetType() + " not supported yet.");
            }

            string fieldName = element.Name;

            PropertyInfo fieldInfo = target.GetType().GetProperty(fieldName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance);

            if (fieldInfo == null)
            {
                throw new ArgumentException("Field '" + fieldName + "' not found in " + target.GetType());
            }

            if (fieldInfo.PropertyType == typeof(string))
            {
                return (string)fieldInfo.GetValue(target);
            }
            else if (fieldInfo.PropertyType == typeof(bool))
            {
                return fieldInfo.GetValue(target).ToString();
            }
            else if (fieldInfo.PropertyType == typeof(Enum))
            {
                return ((Enum)fieldInfo.GetValue(target)).ToString("d");    // Returns int value as string.
            }
            else
            {
                fieldInfo = target.GetType().GetProperty(fieldName + "_rep", BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance);

                if (fieldInfo == null)
                {
                    throw new ArgumentException("Field '" + fieldName + "' found, but '" + fieldName + "_rep' not found in " + target.GetType());
                }

                return (string)fieldInfo.GetValue(target);
            }
        }

        /// <summary>
        /// Attempts to get the data in the location specified by the path using
        /// the target as the root.
        /// </summary>
        /// <param name="target">The root object to apply the path against.</param>
        /// <param name="path">The path to the desired data.</param>
        /// <returns>The data located at the path.</returns>
        public static object Get(IPathResolvable target, DataPath path)
        {
            return GetPathDynamically(target, path);
        }

        /// <summary>
        /// Attempts to get the data in the location specified by the path using
        /// the target as the root.
        /// </summary>
        /// <param name="target">The root object to apply the path against.</param>
        /// <param name="path">The path to the desired data.</param>
        /// <returns>The data located at the path.</returns>
        private static object GetPathDynamically(object target, DataPath path)
        {
            object next;

            if (target is IPathResolvable)
            {
                next = GetElement((IPathResolvable)target, path.Head);
            }
            else
            {
                throw new InvalidCastException("Cannot convert type '" + target.GetType() + "' to a type we can pass to GetElement.");
            }

            if (path.Length > 1)
            {
                return GetPathDynamically(next, path.Tail);
            }
            else
            {
                return next;
            }
        }

        /// <summary>
        /// Attempts to get the data in the location specified by the element
        /// from the target.
        /// Overload for implementers of the IPathResolvable interface.
        /// </summary>
        /// <param name="target">The target from which to request data.</param>
        /// <param name="element">The location of the data being requested.</param>
        /// <returns>The data that was requested.</returns>
        private static object GetElement(IPathResolvable target, IDataPathElement element)
        {
            return target.GetElement(element);
        }

        /// <summary>
        /// Attempts to get the data in the location specified by the path using
        /// the schema, retrieved based on target type.
        /// </summary>
        /// <param name="brokerId">Broker ID, used for data retrieval.</param>
        /// <param name="targets">A set of objects to apply the path too.</param>
        /// <param name="path">The path to the desired data.</param>
        /// <returns>The data specified by the path.</returns>
        private static IEnumerable<object> GetUsingSchema(Guid brokerId, IEnumerable<object> targets, DataPath path)
        {
            List<object> results = new List<object>();

            foreach (IPathResolvable target in targets)
            {
                results.AddRange(ResolveElementUsingSchema(brokerId, target, path.Head));
            }

            if (path.Length > 1)
            {
                if (!(results.FirstOrDefault() is IPathResolvable))
                {
                    throw new CBaseException(ErrorMessages.Generic, "Path is invalid.");
                }

                return GetUsingSchema(brokerId, results, path.Tail);
            }

            return results;
        }

        /// <summary>
        /// Helper used to resolve single path element for single target.
        /// </summary>
        /// <param name="brokerId">Broker ID, used for data retrieval.</param>
        /// <param name="target">The object to apply the path elemet to.</param>
        /// <param name="element">The path element.</param>
        /// <returns>The data specified by the path.</returns>
        private static IEnumerable<object> ResolveElementUsingSchema(Guid brokerId, IPathResolvable target, IDataPathElement element)
        {
            List<object> results = new List<object>();

            IPathableRecordSchema schema = PathSchemaManager.GetSchemaByType(target.GetType());
            if (schema == null)
            {
                throw new CBaseException(ErrorMessages.Generic, $"Schema could not be found for Path node=[{element.ToString()}]");
            }

            if (element is DataPathBasicElement || element is DataPathCollectionElement)
            {
                // Just get next field in the chain.
                PathableFieldInfo fieldInfo;
                if (!schema.GetFields().TryGetValue(element.Name, out fieldInfo))
                {
                    throw new CBaseException(ErrorMessages.Generic, $"Path is invalid.");
                }

                results.Add(fieldInfo.Getter(target));
            }
            else if (element is DataPathSelectionElement)
            {
                IPathableCollectionSchema collectionSchema = schema as IPathableCollectionSchema;
                if (collectionSchema == null)
                {
                    throw new CBaseException(ErrorMessages.Generic, "Path is invalid.");
                }

                if (string.Equals(element.Name, DataPath.WholeCollectionIdentifier, StringComparison.OrdinalIgnoreCase))
                {
                    results.AddRange(collectionSchema.GetAllRecordsFunc(target));
                }
                else if (element.Name.StartsWith(DataPath.SubsetMarker, StringComparison.OrdinalIgnoreCase))
                {
                    PathableSubsetInfo subsetInfo;
                    string subsetName = element.Name.Substring(DataPath.SubsetMarker.Length);
                    if (!collectionSchema.GetSubsets().TryGetValue(subsetName, out subsetInfo))
                    {
                        throw new CBaseException(ErrorMessages.Generic, "Path is invalid.");
                    }

                    results.AddRange(collectionSchema.GetAllRecordsFunc(target).Where(rec => subsetInfo.Filter(rec)));
                }
                else
                {
                    PathableRecordTypeInfo recordTypeInfo;
                    IReadOnlyDictionary<string, PathableRecordTypeInfo> recordTypes = collectionSchema.GetRecordTypes(brokerId);
                    if (recordTypes.Any())
                    {
                        // Treat selector as ID.
                        if (!recordTypes.TryGetValue(element.Name, out recordTypeInfo))
                        {
                            throw new CBaseException(ErrorMessages.Generic, "Path is invalid.");
                        }

                        results.AddRange(
                            collectionSchema.GetAllRecordsFunc(target).Where(
                            rec => string.Equals(
                                collectionSchema.RecordIdGetter(rec),
                                recordTypeInfo.TypeId,
                                StringComparison.OrdinalIgnoreCase)));
                    }
                    else
                    {
                        // Treat as index
                        int index;
                        if (!int.TryParse(element.Name, out index) || collectionSchema.GetAtIndexFunc == null)
                        {
                            throw new CBaseException(ErrorMessages.Generic, "Path is invalid.");
                        }

                        results.Add(collectionSchema.GetAtIndexFunc(target, index));
                    }
                }
            }

            return results;
        }
    }
}
