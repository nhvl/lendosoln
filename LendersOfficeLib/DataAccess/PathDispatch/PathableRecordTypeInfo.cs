﻿namespace DataAccess.PathDispatch
{
    /// <summary>
    /// Holds information about inividual record types for pathable objects.
    /// Used in UI and path creation.
    /// </summary>
    public class PathableRecordTypeInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PathableRecordTypeInfo"/> class.
        /// </summary>
        /// <param name="typeId">Record Type ID.</param>
        /// <param name="friendlyDescription">Record type friendly description.</param>
        public PathableRecordTypeInfo(string typeId, string friendlyDescription)
        {
            this.TypeId = typeId;
            this.FriendlyDescription = friendlyDescription;
        }

        /// <summary>
        /// Gets the ID used to recognize type, as string.
        /// </summary>
        /// <remarks>
        /// This is used in path name.
        /// </remarks>
        /// <value>The ID used to recognize type, as string.</value>
        public string TypeId { get; }

        /// <summary>
        /// Gets the friendly, human readable, description used to identify type in UI.
        /// </summary>
        /// <remarks>
        /// This is used in UI.
        /// </remarks>
        /// <value>The friendly, human readable, description used to identify type in UI.</value>
        public string FriendlyDescription { get; }
    }
}
