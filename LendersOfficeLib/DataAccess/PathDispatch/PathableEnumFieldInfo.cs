﻿namespace DataAccess.PathDispatch
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Pathable field info for enum fields.
    /// </summary>
    public class PathableEnumFieldInfo : PathableFieldInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PathableEnumFieldInfo"/> class.
        /// </summary>
        /// <param name="name">Field name.</param>
        /// <param name="fieldType">Field object type.</param>
        /// <param name="getter">Field getter.</param>
        public PathableEnumFieldInfo(string name, Type fieldType, Func<IPathResolvable, object> getter)
            : base(name, fieldType, SecurityParameter.SecurityParameterType.Enum, getter)
        {
            this.EnumMapping = new List<SecurityParameter.EnumMapping>();
        }

        /// <summary>
        /// Gets a list of enum mappings.
        /// </summary>
        /// <value>List of enum mappings.</value>
        public List<SecurityParameter.EnumMapping> EnumMapping { get; }
    }
}
