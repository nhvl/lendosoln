﻿namespace DataAccess.PathDispatch
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    /// Simple <see cref="IPathableRecordSchema"/> class that uses reflection to retrieve
    /// a dictionary of Pathable fields from objects of type T.
    /// </summary>
    /// <typeparam name="T">They Type this schema is for.</typeparam>
    public abstract class PathableRecordSchema<T> : IPathableRecordSchema
    {
        /// <summary>
        /// Dictionary of field info.
        /// </summary>
        /// <remarks>
        /// Retrieved lazily when needed.
        /// NOTE: For generic types, each veriation of type T is considered a separate type, which means that their static members
        /// are not shared. For example, PathableRecordSchema&lt;BorrowerClosingCostSet&gt; and PathableRecordSchema&lt;SellerClosingCostSet&gt; are considered
        /// two separate types and so each have their own separate instance of the fieldInfos member.
        /// </remarks>
        private static Lazy<Dictionary<string, PathableFieldInfo>> fieldInfos = new Lazy<Dictionary<string, PathableFieldInfo>>(() => GetFieldInfo());

        /// <summary>
        /// Gets the type of the <see cref="IPathResolvable"/> records contained by the collection.
        /// </summary>
        /// <value>The type of the <see cref="IPathResolvable"/> records contained by the collection.</value>
        public Type RecordType
        {
            get
            {
                return typeof(T);
            }
        }

        /// <summary>
        /// Gets the set of loan file dependencies needed to resolve this record from the loan file.
        /// </summary>
        /// <value>The set of loan file dependencies needed to resolve this record from the loan file.</value>
        public abstract IEnumerable<string> LoanFileDependencies { get; }

        /// <summary>
        /// Gets a Dictionary of field info.
        /// </summary>
        /// <value>A Dictionary of field info.</value>
        /// <remarks>
        /// Exposed as protected to allow fieldInfos to be modified by inheriting classes as necessary.
        /// NOTE: Any modification should only be done in the static constructor to avoid possible issues.
        /// </remarks>
        protected static Dictionary<string, PathableFieldInfo> FieldInfos
        {
            get
            {
                return fieldInfos.Value;
            }
        }

        /// <summary>
        /// Returns an enumerable of pathable field info.
        /// </summary>
        /// <returns>An enumerable of <see cref="PathableFieldInfo"/> objects.</returns>
        public virtual IReadOnlyDictionary<string, PathableFieldInfo> GetFields()
        {
            return fieldInfos.Value;
        }

        /// <summary>
        /// Generates the dictionary of field info for the passes in Type T.
        /// </summary>
        /// <returns>A <see cref="Dictionary{string, PathableFieldInfo}"/> generated using recflection on generic type T.</returns>
        private static Dictionary<string, PathableFieldInfo> GetFieldInfo()
        {
            Dictionary<string, PathableFieldInfo> info = new Dictionary<string, PathableFieldInfo>(StringComparer.OrdinalIgnoreCase);

            foreach (PropertyInfo prop in typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public))
            {
                // Skip reps. We don't want the same fee showing up twice.
                if (prop.Name.EndsWith("_rep"))
                {
                    continue;
                }

                PathableFieldInfo fieldInfo;
                SecurityParameter.SecurityParameterType parameterType;

                if (prop.PropertyType.IsEnum)
                {
                    MethodInfo getter = prop.GetGetMethod();
                    PathableEnumFieldInfo enumInfo = new PathableEnumFieldInfo(prop.Name, prop.PropertyType, rec => getter.Invoke(rec, null));

                    IEnumerable<int> values = Enum.GetValues(prop.PropertyType).Cast<int>();
                    string[] names = Enum.GetNames(prop.PropertyType);

                    List<SecurityParameter.EnumMapping> mapping = new List<SecurityParameter.EnumMapping>(names.Length);
                    int iteration = 0;
                    foreach (int i in values)
                    {
                        enumInfo.EnumMapping.Add(new SecurityParameter.EnumMapping()
                        {
                            FriendlyValue = names[iteration],
                            RepValue = i.ToString()
                        });
                        iteration++;
                    }

                    fieldInfo = enumInfo;
                }
                else
                {
                    // Defaults based on type.
                    if (prop.PropertyType == typeof(string))
                    {
                        parameterType = SecurityParameter.SecurityParameterType.String;
                    }
                    else if (prop.PropertyType == typeof(bool))
                    {
                        parameterType = SecurityParameter.SecurityParameterType.Boolean;
                    }
                    else if (prop.PropertyType == typeof(int) || prop.PropertyType == typeof(long))
                    {
                        parameterType = SecurityParameter.SecurityParameterType.Count;
                    }
                    else if (prop.PropertyType == typeof(decimal) || prop.PropertyType == typeof(double))
                    {
                        parameterType = SecurityParameter.SecurityParameterType.Money;
                    }
                    else if (prop.PropertyType == typeof(CDateTime) || prop.PropertyType == typeof(DateTime))
                    {
                        parameterType = SecurityParameter.SecurityParameterType.Date;
                    }
                    else if (typeof(IPathResolvable).IsAssignableFrom(prop.PropertyType))
                    {
                        IPathableRecordSchema schema = PathSchemaManager.GetSchemaByType(prop.PropertyType);

                        // If schema is not found, just ignore.
                        if (schema == null)
                        {
                            continue;
                        }

                        // Set parameter type based on schema type.
                        if (schema is IPathableCollectionSchema)
                        {
                            parameterType = SecurityParameter.SecurityParameterType.PathableCollection;
                        }
                        else
                        {
                            parameterType = SecurityParameter.SecurityParameterType.PathableRecord;
                        }                        
                    }
                    else
                    {
                        continue;   // Unknown object type. Just ignore.
                    }

                    MethodInfo getter = prop.GetGetMethod();
                    fieldInfo = new PathableFieldInfo(prop.Name, prop.PropertyType, parameterType, rec => getter.Invoke(rec, null));
                }

                info.Add(prop.Name, fieldInfo);
            }

            return info;
        }
    }
}
