﻿namespace DataAccess.PathDispatch
{
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// Provides an interface for setting fields.
    /// </summary>
    public interface IPathSettable
    {
        /// <summary>
        /// Sets the value indicated by the path to the given value.
        /// </summary>
        /// <param name="path">The path to the field.</param>
        /// <param name="value">The field value to set.</param>
        void SetElement(IDataPathElement path, object value);
    }
}
