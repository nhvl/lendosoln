﻿namespace DataAccess.PathDispatch.Schema
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Path schema for the borrower closing cost set.
    /// </summary>
    public class BorrowerClosingCostSetPathSchema : LoanClosingCostSetPathSchema<BorrowerClosingCostSet, BorrowerClosingCostFee>
    {
        /// <summary>
        /// Static array of loan file dependencies.
        /// </summary>
        private static string[] loanFileDependencies = new string[] { "sClosingCostSet" };

        /// <summary>
        /// Gets the set of loan file dependencies needed to resolve this record from the loan file.
        /// </summary>
        /// <value>The set of loan file dependencies needed to resolve this record from the loan file.</value>
        public override IEnumerable<string> LoanFileDependencies
        {
            get
            {
                return loanFileDependencies;
            }
        }

        /// <summary>
        /// Gets a function that returns all records in this collection.
        /// </summary>
        /// <value>A function that returns all records in this collection.</value>
        public override Func<IPathResolvable, IEnumerable<IPathResolvable>> GetAllRecordsFunc
        {
            get
            {
                return set => ((BorrowerClosingCostSet)set).GetFees(fee => FeeFilter((BorrowerClosingCostFee)fee));
            }
        }

        /// <summary>
        /// Used to filter "hidden" fees from being returned in call to GetAllRecordsFunc.
        /// </summary>
        /// <param name="fee">Fee to test against filter.</param>
        /// <returns>True, if the fee passes through the filter, false otherwise.</returns>
        private static bool FeeFilter(BorrowerClosingCostFee fee)
        {
            if (fee.ParentClosingCostSet != null && fee.ParentClosingCostSet.HasDataLoanAssociate)
            {
                return ClosingCostSetUtils.PrepsSecGMIFilter(fee) &&
                    ClosingCostSetUtils.OrigCompFilter(
                        fee,
                        fee.ParentClosingCostSet.DataLoan.sOriginatorCompensationPaymentSourceT,
                        fee.ParentClosingCostSet.DataLoan.sDisclosureRegulationT,
                        fee.ParentClosingCostSet.DataLoan.sGfeIsTPOTransaction);
            }
            else if (fee.ParentClosingCostSet != null && fee.ParentClosingCostSet.HasClosingCostArchiveAssociate)
            {
                return ClosingCostSetUtils.PrepsSecGMIFilter(fee) &&
                    ClosingCostSetUtils.OrigCompFilterWithArchive(fee, fee.ParentClosingCostSet.ClosingCostArchive);
            }
            else
            {
                return ClosingCostSetUtils.PrepsSecGMIFilter(fee);
            }
        }
    }
}
