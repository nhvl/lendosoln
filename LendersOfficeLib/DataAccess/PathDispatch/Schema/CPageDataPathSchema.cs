﻿namespace DataAccess.PathDispatch.Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Path schema for CPageData.
    /// </summary>
    public class CPageDataPathSchema : IPathableRecordSchema
    {
        /// <summary>
        /// Gets the type of the <see cref="IPathResolvable"/> records contained by the collection.
        /// </summary>
        /// <value>The type of the <see cref="IPathResolvable"/> records contained by the collection.</value>
        public Type RecordType
        {
            get
            {
                return typeof(CPageData);
            }
        }

        /// <summary>
        /// Gets the set of loan file dependencies needed to resolve this record from the loan file.
        /// </summary>
        /// <value>The set of loan file dependencies needed to resolve this record from the loan file.</value>
        public IEnumerable<string> LoanFileDependencies
        {
            get
            {
                return Enumerable.Empty<string>();
            }
        }

        /// <summary>
        /// Returns an enumerable of pathable field info.
        /// </summary>
        /// <returns>An enumerable of <see cref="PathableFieldInfo"/> objects.</returns>
        public IReadOnlyDictionary<string, PathableFieldInfo> GetFields()
        {
            Dictionary<string, PathableFieldInfo> fields = new Dictionary<string, PathableFieldInfo>(StringComparer.OrdinalIgnoreCase);
            fields.Add("borrowerclosingcostset", new PathableFieldInfo("borrowerclosingcostset", typeof(BorrowerClosingCostSet), SecurityParameter.SecurityParameterType.PathableCollection, data => ((CPageData)data).sClosingCostSet));
            fields.Add("sellerclosingcostset", new PathableFieldInfo("sellerclosingcostset", typeof(SellerClosingCostSet), SecurityParameter.SecurityParameterType.PathableCollection, data => ((CPageData)data).sSellerResponsibleClosingCostSet));
            fields.Add("sClosingDisclosureDatesInfo", new PathableFieldInfo("sClosingDisclosureDatesInfo", typeof(ClosingDisclosureDatesInfo), SecurityParameter.SecurityParameterType.PathableCollection, data => ((CPageData)data).sClosingDisclosureDatesInfo));
            fields.Add("sLoanEstimateDatesInfo", new PathableFieldInfo("sLoanEstimateDatesInfo", typeof(LoanEstimateDatesInfo), SecurityParameter.SecurityParameterType.PathableCollection, data => ((CPageData)data).sLoanEstimateDatesInfo));
            return fields;
        }
    }
}
