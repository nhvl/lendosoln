﻿namespace DataAccess.PathDispatch.Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using DataAccess.PathDispatch;
    using LendersOffice.Admin;

    /// <summary>
    /// Abstract path schema for the loan closing cost fee payment collection.
    /// </summary>
    public class LoanClosingCostFeePaymentCollectionPathSchema : PathableRecordSchema<LoanClosingCostFeePayment>, IPathableCollectionSchema
    {
        /// <summary>
        /// Static array of loan file dependencies.
        /// </summary>
        private static string[] loanFileDependencies = new string[] { "sClosingCostSet", "sSellerResponsibleClosingCostSet" };

        /// <summary>
        /// Gets the type of the <see cref="IPathResolvable"/> collection represented by this schema.
        /// </summary>
        /// <value>The type of the <see cref="IPathResolvable"/> collection represented by this schema.</value>
        public Type CollectionType
        {
            get
            {
                return typeof(LoanClosingCostFeePaymentCollection);
            }
        }

        /// <summary>
        /// Gets a function that returns all records in this collection.
        /// </summary>
        /// <value>A function that returns all records in this collection.</value>
        public virtual Func<IPathResolvable, IEnumerable<IPathResolvable>> GetAllRecordsFunc
        {
            get
            {
                return collection => ((LoanClosingCostFeePaymentCollection)collection).GetPayments();
            }
        }

        /// <summary>
        /// Gets a function that returns the record at the given int index in the collection.
        /// </summary>
        /// <value>A function that returns the record at the given index in the collection.</value>
        /// <remarks>
        /// Collections should be indexable either by ID or by index. NOT BOTH.
        /// Only use if GetRecordTypes returns the Empty Set.
        /// </remarks>
        public Func<IPathResolvable, int, IPathResolvable> GetAtIndexFunc
        {
            get
            {
                return (collection, index) => this.GetAllRecordsFunc(collection).ElementAt(index);
            }
        }

        /// <summary>
        /// Gets a function that takes in a record from the collection and returns its ID.
        /// </summary>
        /// <value>A function that takes in a record from the collection and returns its ID.</value>
        public Func<IPathResolvable, string> RecordIdGetter
        {
            get
            {
                return payment => ((LoanClosingCostFeePayment)payment).Id.ToString();
            }
        }

        /// <summary>
        /// Gets the set of loan file dependencies needed to resolve this record from the loan file.
        /// </summary>
        /// <value>The set of loan file dependencies needed to resolve this record from the loan file.</value>
        public override IEnumerable<string> LoanFileDependencies
        {
            get
            {
                return loanFileDependencies;
            }
        }

        /// <summary>
        /// Returns an enumerable of pathable record types. May vary by broker.
        /// </summary>
        /// <param name="brokerId">The Broker ID.</param>
        /// <returns>An enumerable of <see cref="PathableRecordTypeInfo"/> objects.</returns>
        public IReadOnlyDictionary<string, PathableRecordTypeInfo> GetRecordTypes(Guid brokerId)
        {
            return new Dictionary<string, PathableRecordTypeInfo>(StringComparer.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Returns a default record for the given record type from the given broker.
        /// </summary>
        /// <param name="brokerId">The broker ID.</param>
        /// <param name="typeId">Record type ID as string.</param>
        /// <param name="collection">Pathable collection represented by this schema.</param>
        /// <returns>An <see cref="IPathResolvable"/> representing a default record of the given type.</returns>
        public IPathResolvable GetDefaultRecord(Guid brokerId, string typeId, IPathResolvable collection)
        {
            LoanClosingCostFeePaymentCollection paymentCollection = (LoanClosingCostFeePaymentCollection)collection;
            LoanClosingCostFeePayment payment = (LoanClosingCostFeePayment)Activator.CreateInstance(paymentCollection.PaymentType);
            payment.SetParent(paymentCollection.Fee);

            return payment;               
        }

        /// <summary>
        /// Returns an enumerable of pathable subset info.
        /// </summary>
        /// <returns>An enumerable of <see cref="PathableSubsetInfo"/> objects.</returns>
        public IReadOnlyDictionary<string, PathableSubsetInfo> GetSubsets()
        {
            return new Dictionary<string, PathableSubsetInfo>();
        }
    }
}
