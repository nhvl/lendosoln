﻿namespace DataAccess.PathDispatch.Schema
{
    using System.Collections.Generic;

    /// <summary>
    /// Path schema for the borrower closing cost set.
    /// </summary>
    public class SellerClosingCostSetPathSchema : LoanClosingCostSetPathSchema<SellerClosingCostSet, SellerClosingCostFee>
    {
        /// <summary>
        /// Static array of loan file dependencies.
        /// </summary>
        private static string[] loanFileDependencies = new string[] { "sSellerResponsibleClosingCostSet" };

        /// <summary>
        /// Gets the set of loan file dependencies needed to resolve this record from the loan file.
        /// </summary>
        /// <value>The set of loan file dependencies needed to resolve this record from the loan file.</value>
        public override IEnumerable<string> LoanFileDependencies
        {
            get
            {
                return loanFileDependencies;
            }
        }
    }
}
