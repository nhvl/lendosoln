﻿namespace DataAccess.PathDispatch.Schema
{
    using System;
    using System.Collections.Generic;

    using DataAccess.PathDispatch;
    using LendersOffice.Admin;

    /// <summary>
    /// Abstract path schema for the loan closing cost sets.
    /// </summary>
    /// <typeparam name="TSet">Closing cost set type.</typeparam>
    /// <typeparam name="TFee">Closing cost fee type.</typeparam>
    public abstract class LoanClosingCostSetPathSchema<TSet, TFee> : PathableRecordSchema<TFee>, IPathableCollectionSchema
        where TSet : LoanClosingCostSet
        where TFee : LoanClosingCostFee
    {
        /// <summary>
        /// Static array of subset info.
        /// </summary>
        private static Lazy<Dictionary<string, PathableSubsetInfo>> subsetInfo = new Lazy<Dictionary<string, PathableSubsetInfo>>(() =>
        {
            Dictionary<string, PathableSubsetInfo> ret = new Dictionary<string, PathableSubsetInfo>(StringComparer.OrdinalIgnoreCase);
            ret.Add("SectionA", new PathableSubsetInfo("SectionA", fee => ((TFee)fee).IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionA));
            ret.Add("SectionB", new PathableSubsetInfo("SectionB", fee => ((TFee)fee).IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionB));
            ret.Add("SectionC", new PathableSubsetInfo("SectionC", fee => ((TFee)fee).IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionC));
            ret.Add("SectionD", new PathableSubsetInfo("SectionD", fee => ((TFee)fee).IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionD));
            ret.Add("SectionE", new PathableSubsetInfo("SectionE", fee => ((TFee)fee).IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionE));
            ret.Add("SectionF", new PathableSubsetInfo("SectionF", fee => ((TFee)fee).IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionF));
            ret.Add("SectionG", new PathableSubsetInfo("SectionG", fee => ((TFee)fee).IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionG));
            ret.Add("SectionH", new PathableSubsetInfo("SectionH", fee => ((TFee)fee).IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionH));

            ret.Add("800", new PathableSubsetInfo("800", fee => ((TFee)fee).HudLine / 100 == 8));
            ret.Add("900", new PathableSubsetInfo("900", fee => ((TFee)fee).HudLine / 100 == 9));
            ret.Add("1000", new PathableSubsetInfo("1000", fee => ((TFee)fee).HudLine / 100 == 10));
            ret.Add("1100", new PathableSubsetInfo("1100", fee => ((TFee)fee).HudLine / 100 == 11));
            ret.Add("1200", new PathableSubsetInfo("1200", fee => ((TFee)fee).HudLine / 100 == 12));
            ret.Add("1300", new PathableSubsetInfo("1300", fee => ((TFee)fee).HudLine / 100 == 13));

            return ret;
        });

        /// <summary>
        /// Initializes static members of the <see cref="LoanClosingCostSetPathSchema" /> class.
        /// Used to modify the static FieldInfos dictionary.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage(
            category: "Microsoft.StyleCop.CSharp.DocumentationRules",
            checkId: "SA1642:ConstructorSummaryDocumentationMustBeginWithStandardText",
            Justification = "StyleCop occasionally does not recognize the summary text for static constructors, requiring restarting VS to fix.")]
        static LoanClosingCostSetPathSchema()
        {
            FieldInfos.Add("Payments", new PathableFieldInfo("Payments", typeof(LoanClosingCostFeePaymentCollection), SecurityParameter.SecurityParameterType.PathableCollection, fee => new LoanClosingCostFeePaymentCollection((TFee)fee)));
        }

        /// <summary>
        /// Gets the type of the <see cref="IPathResolvable"/> collection represented by this schema.
        /// </summary>
        /// <value>The type of the <see cref="IPathResolvable"/> collection represented by this schema.</value>
        public Type CollectionType
        {
            get
            {
                return typeof(TSet);
            }
        }

        /// <summary>
        /// Gets a function that returns all records in this collection.
        /// </summary>
        /// <value>A function that returns all records in this collection.</value>
        public virtual Func<IPathResolvable, IEnumerable<IPathResolvable>> GetAllRecordsFunc
        {
            get
            {
                return set => ((TSet)set).GetFees(null);
            }
        }

        /// <summary>
        /// Gets a function that returns the record at the given int index in the collection.
        /// </summary>
        /// <value>A function that returns the record at the given index in the collection.</value>
        /// <remarks>
        /// Collections should be indexable either by ID or by index. NOT BOTH.
        /// Only use if GetRecordTypes returns the Empty Set.
        /// </remarks>
        public Func<IPathResolvable, int, IPathResolvable> GetAtIndexFunc
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Gets a function that takes in a record from the collection and returns its ID.
        /// </summary>
        /// <value>A function that takes in a record from the collection and returns its ID.</value>
        public Func<IPathResolvable, string> RecordIdGetter
        {
            get
            {
                return fee => ((TFee)fee).ClosingCostFeeTypeId.ToString();
            }
        }

        /// <summary>
        /// Returns an enumerable of pathable record types. May vary by broker.
        /// </summary>
        /// <param name="brokerId">The Broker ID.</param>
        /// <returns>An enumerable of <see cref="PathableRecordTypeInfo"/> objects.</returns>
        public IReadOnlyDictionary<string, PathableRecordTypeInfo> GetRecordTypes(Guid brokerId)
        {
            Dictionary<string, PathableRecordTypeInfo> recordTypes = new Dictionary<string, PathableRecordTypeInfo>(StringComparer.OrdinalIgnoreCase);

            // Don't load if broker ID is empty guid. Likely a unit test.
            // NOTE: System Broker still has default fees in it's Fee Setup Closing Cost Set.
            if (brokerId == Guid.Empty)
            {
                return recordTypes;
            }

            // Get all Broker Fee Types
            BrokerDB broker = BrokerDB.RetrieveById(brokerId);
            IEnumerable<BaseClosingCostFee> feeList = broker.GetUnlinkedClosingCostSet().GetFees(null);

            foreach (FeeSetupClosingCostFee fee in feeList)
            {
                recordTypes.Add(fee.ClosingCostFeeTypeId.ToString(), new PathableRecordTypeInfo(fee.ClosingCostFeeTypeId.ToString(), fee.Description));
            }

            return recordTypes;
        }

        /// <summary>
        /// Returns a default record for the given record type from the given broker.
        /// </summary>
        /// <param name="brokerId">The broker ID.</param>
        /// <param name="typeId">Record type ID as string.</param>
        /// <param name="collection">Pathable collection represented by this schema.</param>
        /// <returns>An <see cref="IPathResolvable"/> representing a default record of the given type.</returns>
        public IPathResolvable GetDefaultRecord(Guid brokerId, string typeId, IPathResolvable collection)
        {
            Guid typeGuid;
            if (!Guid.TryParse(typeId, out typeGuid))
            {
                return null;
            }

            return ((LoanClosingCostSet)collection).GetBrokerFee(brokerId, typeGuid, false);
        }

        /// <summary>
        /// Returns an enumerable of pathable subset info.
        /// </summary>
        /// <returns>An enumerable of <see cref="PathableSubsetInfo"/> objects.</returns>
        public IReadOnlyDictionary<string, PathableSubsetInfo> GetSubsets()
        {
            return subsetInfo.Value;
        }
    }
}
