﻿namespace DataAccess.PathDispatch
{
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// Provides an interface for resolving path elements into a value.
    /// </summary>
    public interface IPathResolvable
    {
        /// <summary>
        /// Returns the value specified by the element.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns>The value specified by the element.</returns>
        object GetElement(IDataPathElement element);
    }

    /// <summary>
    /// Provides an interface for providing support to expand to immediate children.
    /// </summary>
    public interface IPathResolvableExpandable : IPathResolvable
    {
        /// <summary>
        /// Gets the children of this instance supporting read/get operations.
        /// </summary>
        /// <returns>The set of readable child elements.</returns>
        System.Collections.Generic.IEnumerable<DataPathBasicElement> GetReadableChildren();
    }
}
