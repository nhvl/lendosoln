﻿namespace DataAccess.PathDispatch
{
    using System.Collections.Generic;
    using LqbGrammar.DataTypes.PathDispatch;

    /// <summary>
    /// Provides path access to collection elements.
    /// </summary>
    public interface IPathResolvableCollection : IPathResolvable
    {
        /// <summary>
        /// Gets all of the keys for the collection.
        /// </summary>
        /// <returns>All of the keys for the collection.</returns>
        IEnumerable<DataPathSelectionElement> GetAllKeys();
    }
}
