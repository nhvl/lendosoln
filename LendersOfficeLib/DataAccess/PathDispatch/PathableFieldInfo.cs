﻿namespace DataAccess.PathDispatch
{
    using System;

    /// <summary>
    /// Holds information on pathable fields (properties) in a pathable object.
    /// Used to create parameters in workflow UI.
    /// </summary>
    public class PathableFieldInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PathableFieldInfo"/> class.
        /// </summary>
        /// <param name="name">Field name.</param>
        /// <param name="fieldType">Field object type.</param>
        /// <param name="parameterType">Field property info.</param>
        /// <param name="getter">Field getter.</param>
        public PathableFieldInfo(string name, Type fieldType, SecurityParameter.SecurityParameterType parameterType, Func<IPathResolvable, object> getter)
        {
            this.Name = name;
            this.FieldType = fieldType;
            this.ParameterType = parameterType;
            this.Getter = getter;
        }

        /// <summary>
        /// Gets the field name.
        /// </summary>
        /// <value>The field name.</value>
        public string Name { get; }

        /// <summary>
        /// Gets the field's object type.
        /// </summary>
        /// <value>The field object type.</value>
        public Type FieldType { get; }

        /// <summary>
        /// Gets the parameter type.
        /// Used in Workflow UI.
        /// </summary>
        /// <value>The parameter type.</value>
        public SecurityParameter.SecurityParameterType ParameterType { get; }

        /// <summary>
        /// Gets property getter.
        /// </summary>
        /// <value>Property getter.</value>
        public Func<IPathResolvable, object> Getter { get; }
    }
}
