﻿namespace DataAccess.PathDispatch
{
    using System;
    using System.Collections.Generic;

    using DataAccess.PathDispatch.Schema;
    using LendersOffice.Common;

    /// <summary>
    /// Path Schema Manager.
    /// </summary>
    public static class PathSchemaManager
    {
        /// <summary>
        /// HashSet of dependencies for all schema.
        /// </summary>
        private static HashSet<string> dependencies = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

        /// <summary>
        /// Initializes static members of the <see cref="PathSchemaManager"/> class.
        /// </summary>
        static PathSchemaManager()
        {
            RegisterSchema(new CPageDataPathSchema());
            RegisterSchema(new BorrowerClosingCostSetPathSchema());
            RegisterSchema(new SellerClosingCostSetPathSchema());
            RegisterSchema(new LoanClosingCostFeePaymentCollectionPathSchema());
            RegisterSchema(new ClosingDisclosureDatesInfoPathSchema());
            RegisterSchema(new LoanEstimateDatesInfoPathSchema());
        }

        /// <summary>
        /// Gets a dictionary of schema by type.
        /// </summary>
        /// <value>A dictionary of schema by type.</value>
        private static Dictionary<Type, IPathableRecordSchema> Schemas { get; } = new Dictionary<Type, IPathableRecordSchema>();

        /// <summary>
        /// Checks if the given field name is included in the dependency list for one or more schema.
        /// </summary>
        /// <param name="fieldName">Field name to check.</param>
        /// <returns>True, if fieldName appears in the dependency list of one or more schema.</returns>
        public static bool IsSchemaDependency(string fieldName)
        {
            return dependencies.Contains(fieldName);
        }

        /// <summary>
        /// Retrieves Schema by type. Returns base type schema if type schema not found.
        /// </summary>
        /// <param name="t">Type to get schema for.</param>
        /// <returns><see cref="IPathableRecordSchema"/> for given type or for the nearest base type it inherits from.</returns>
        public static IPathableRecordSchema GetSchemaByType(Type t)
        {
            while (t != null)
            {
                IPathableRecordSchema schema;
                if (Schemas.TryGetValue(t, out schema))
                {
                    return schema;
                }

                t = t.BaseType;
            }

            return null; // Or throw.
        }

        /// <summary>
        /// Adds a schema to the Schema dictionary.
        /// </summary>
        /// <param name="schema">Schema to add.</param>
        private static void RegisterSchema(IPathableRecordSchema schema)
        {
            Schemas.Add(schema.RecordType, schema);
            dependencies.UnionWith(schema.LoanFileDependencies);

            // Register collection schema to both record and collection type.
            if (schema is IPathableCollectionSchema)
            {
                Schemas.Add(((IPathableCollectionSchema)schema).CollectionType, schema);
            }
        }
    }
}
