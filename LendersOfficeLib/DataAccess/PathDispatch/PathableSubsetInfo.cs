﻿namespace DataAccess.PathDispatch
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Used to define subsets in pathable collections.
    /// </summary>
    public class PathableSubsetInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PathableSubsetInfo"/> class.
        /// </summary>
        /// <param name="name">Subset name.</param>
        /// <param name="filter">Predicate used to filter elements in subset.</param>
        public PathableSubsetInfo(string name, Func<IPathResolvable, bool> filter)
        {
            this.Name = name;
            this.Filter = filter;
        }

        /// <summary>
        /// Gets the subset Name. Used to identify subset in Path.
        /// </summary>
        /// <value>The subset Name.</value>
        public string Name { get; }

        /////// <summary>
        /////// Gets the friendly name for the subset. Used in UI.
        /////// </summary>
        /////// <value>The friendly name for the subset. Used in UI.</value>
        ////public string FriendlyName { get; }

        /// <summary>
        /// Gets the predicate used to filter elements in T that belong to the subset.
        /// </summary>
        /// <value>The predicate used to filter elements in T that belong to the subset.</value>
        public Func<IPathResolvable, bool> Filter { get;  }
    }
}
