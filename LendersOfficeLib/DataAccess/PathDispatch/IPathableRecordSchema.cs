﻿namespace DataAccess.PathDispatch
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Interface for Schemas that hold path dispath information for <see cref="IPathResolvable" /> records.
    /// This interface should be used by any pathable object that would be represented by a <see cref="DataPathBasicElement"/> in the path.
    /// </summary>
    public interface IPathableRecordSchema
    {
        /// <summary>
        /// Gets the type of the <see cref="IPathResolvable"/> records contained by the collection.
        /// </summary>
        /// <value>The type of the <see cref="IPathResolvable"/> records contained by the collection.</value>
        Type RecordType { get; }

        /// <summary>
        /// Gets the set of loan file dependencies needed to resolve this record from the loan file.
        /// </summary>
        /// <value>The set of loan file dependencies needed to resolve this record from the loan file.</value>
        IEnumerable<string> LoanFileDependencies { get; }

        /// <summary>
        /// Returns an enumerable of pathable field info.
        /// </summary>
        /// <returns>An enumerable of <see cref="PathableFieldInfo"/> objects.</returns>
        IReadOnlyDictionary<string, PathableFieldInfo> GetFields();
    }
}
