namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using DataAccess.PathDispatch;
    using LendersOffice.Admin;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Migration;
    using LendersOffice.Security;
    using LendersOffice.UI;
    using LendersOffice.Rolodex;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;

    public class CAgentFields : CXmlRecordBaseOld, IPathResolvable
    {
        public new const string TableName = "AgentXmlContent";
        private static CAgentFields s_empty;
        public static CAgentFields Empty
        {
            get { return s_empty; }
        }

        static CAgentFields()
        {
            s_empty = new CAgentFields();
        }

        private static readonly HashSet<string> FIELD_FROM_BROKER_CONTACT = new HashSet<string>(new string[]
        {
            "AgentName"

            ,"CellPhone"
            ,"City"
            ,"CompanyName"
            ,"DepartmentName"
            ,"EmailAddr"
            ,"FaxNum"

            ,"LicenseNumOfAgent"
            ,"LicenseNumOfCompany"
            ,"PagerNum"
            ,"Phone"
            ,"State"
            ,"StreetAddr"
            ,"Zip"
            ,"County"
            ,"PhoneOfCompany"
            ,"FaxOfCompany"
            ,"IsNotifyWhenLoanStatusChange"

            ,"BranchName"
            ,"PayToBankName"
            ,"PayToBankCityState"
            ,"PayToABANumber"
            ,"PayToAccountNumber"
            ,"PayToAccountName"
            ,"FurtherCreditToAccountNumber"
            ,"FurtherCreditToAccountName"
            ,"CompanyId"
        });

        private bool m_doCopyOfCommision = false; // if this is true, then we copy over the commission fields from the broker too.
        private bool m_NewLicenseSelected
        {
            get { return !GetDescString("LicenseNumForCompany").Equals(LicenseNumOfCompany) && !LicenseNumOfCompany.Equals(string.Empty)  && m_selectedLicense != null;}
        }

        private LicenseInfo m_selectedLicense
        {
            get
            {
                foreach (LicenseInfo licenseInfo in CompanyLicenseInfoList)
                {
                    if (licenseInfo.License.Equals(LicenseNumOfCompany) && licenseInfo.State.Equals(m_parent.sSpState))
                    {
                        return licenseInfo;
                    }
                }

                return null;
            }
        }

        private static readonly IEnumerable<string> COMMISION_FIELDS_FROM_BROKER = new string[] {
            "CommissionMinBase",
            "CommissionPointOfLoanAmount",
            "CommissionPointOfGrossProfit"
        };

        private CPageBase m_parent;

        /// <summary>
        /// Initializes a new instance of the <see cref="CAgentFields"/> class.
        /// This instance will return false, use it for making a static empty object.
        /// </summary>
        protected CAgentFields() : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CAgentFields"/> class.
        /// </summary>
        /// <param name="parent">The parent loan file.</param>
        /// <param name="ds">The agent dataset.</param>
        /// <param name="iRow">The relevant datarow number. Pass in -1 to add a new contact.</param>
        public CAgentFields( CPageBase parent, DataSet ds, int iRow )
            : base(parent, ds, iRow, TableName)
        {
            Initialize(parent, ds);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CAgentFields"/> class.
        /// </summary>
        /// <param name="parent">The parent loan file.</param>
        /// <param name="ds">The agent dataset.</param>
        /// <param name="id">The record ID.</param>
        public CAgentFields(CPageBase parent, DataSet ds, Guid id)
            : base(parent, ds, id, TableName)
        {
            Initialize(parent, ds);
        }

        /// <summary>
        /// Initializes a new CAgentFields object.
        /// </summary>
        /// <param name="parent">The parent loan.</param>
        /// <param name="ds">The agent dataset.</param>
        private void Initialize(CPageBase parent, DataSet ds)
        {
            m_parent = parent;
            string check = GetDescString("LicenseNumForCompany");

            if (!this.IsNewRecord)
            {
                this.CurrentRow.AcceptChanges();
            }
        }

        override public void Update()
        {
            Update(true);
        }

        public void Update(AbstractUserPrincipal principal)
        {
            this.Update(true, principal);
        }

        public void Update(bool updateAffiliateValuesFromLoan, AbstractUserPrincipal principal = null)
        {
            if (IsValid)
            {
                base.Update();
                LendingLicenseXmlContent = LicenseInfoList.ToString(); // Save the lending license xml contact
                CompanyLendingLicenseXmlContent = CompanyLicenseInfoList.ToString();

                if (IsNewRecord && updateAffiliateValuesFromLoan)
                {
                    m_parent.UpdateAffiliateOfficialContactValues(this);
                }

                this.RecordAuditOnCreateOrEdit(principal);
                m_parent.sAgentDataSet = m_ds;

                if (IsApplyToFormsUponSave)
                {
                    m_parent.CopyToFormsPreparer(this);
                }
            }
            else
            {
                Tools.LogError("Programming Error, CAgentFields.Update() is called on an invalid CAgentFields object.");
            }
        }

        public void Delete(AbstractUserPrincipal principal = null)
        {
            if (this.CurrentRow.RowState == DataRowState.Deleted ||
                this.CurrentRow.RowState == DataRowState.Detached)
            {
                return;
            }

            this.m_parent.RecordAuditOnSave(AgentRecordChangeAuditHelper.CreateAuditEvent(this.CurrentRow, this.m_parent.sSpState, AgentRecordChangeType.DeleteRecord, principal));
            this.CurrentRow.Delete();
            this.CurrentRow.AcceptChanges();
        }

        /// <summary>
        /// Records information to the audit history about the agent being created or edited.
        /// </summary>
        private void RecordAuditOnCreateOrEdit(AbstractUserPrincipal principal = null)
        {
            var changeType = this.IsNewRecord ? AgentRecordChangeType.CreateRecord : AgentRecordChangeType.EditRecord;
            this.m_parent.RecordAuditOnSave(AgentRecordChangeAuditHelper.CreateAuditEvent(this.CurrentRow, this.m_parent.sSpState, changeType, principal));
            this.CurrentRow.AcceptChanges();
        }

        private bool m_isApplyToForms = false;
        public bool IsApplyToFormsUponSave
        {
            get { return m_isApplyToForms; }
            set { m_isApplyToForms = value; }
        }

        public E_AgentSourceT AgentSourceT // opm 137883
        {
            get { return (E_AgentSourceT)GetEnum("AgentSourceT", E_AgentSourceT.NotYetDefined); }
            set { SetCount("AgentSourceT", (int)value); }
        }

        public Guid BrokerLevelAgentID
        {
            get { return GetGuid("BrokerLevelAgentID"); }
            set { SetGuid("BrokerLevelAgentID", value); }
        }

        public E_AgentRoleT AgentRoleT
        {
            get
            {
                if (!IsValid)
                {
                    return E_AgentRoleT.Other;
                }

                return (E_AgentRoleT) GetEnum("AgentRoleT", E_AgentRoleT.Other);
            }
            set { SetEnum("AgentRoleT", value); }
        }

        /// <summary>
        /// Gets the description of AgentRoleT, regardless of type.
        /// </summary>
        public string AgentRoleTDesc
        {
            get
            {
                return RolodexDB.GetTypeDescription(AgentRoleT);
            }
        }

        /// <summary>
        /// Gets the description of AgentRoleT if not Other. If Other, return OtherAgentRoleTDesc if not blank, otherwise fall back on "Other."
        /// </summary>
        public string AgentRoleDescription
        {
            get
            {
                if (AgentRoleT == E_AgentRoleT.Other)
                {
                    if (string.IsNullOrEmpty(OtherAgentRoleTDesc))
                    {
                        return "Other";
                    }
                    else
                    {
                        return OtherAgentRoleTDesc;
                    }
                }
                else
                {
                    return AgentRoleTDesc;
                }
            }
        }

        public override Enum KeyType
        {
            get { return AgentRoleT; }
            set { AgentRoleT = (E_AgentRoleT) value; }
        }

		public string AgentName
		{
			get { return GetDescString( "AgentName" ); }
			set { SetDescString( "AgentName", value ); }
		}

        public string TaxId
        {
            get
            {
                // m_parent will be null if the ReturnEmptyObject option was specified.
                if (this.m_parent == null
                    || LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.m_parent.sLoanVersionT, LoanVersionT.V18_ConsolidateTaxIdFields))
                {
                    EmployerTaxIdentificationNumber? ein = EmployerTaxIdentificationNumber.Create(GetDescString("TaxId"));
                    return this.m_convertLos.ToEmployerTaxIdentificationNumberString(ein);
                }
                else
                {
                    return GetDescString("TaxId");
                }
            }
            set
            {
                // m_parent will be null if the ReturnEmptyObject option was specified.
                if (this.m_parent == null
                    || LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.m_parent.sLoanVersionT, LoanVersionT.V18_ConsolidateTaxIdFields))
                {
                    EmployerTaxIdentificationNumber? ein = EmployerTaxIdentificationNumber.Create(value);

                    if (!ein.HasValue && !string.IsNullOrEmpty(value))
                    {
                        throw new InvalidTaxIdValueException("TaxId");
                    }

                    // For files on the new version, we will only set valid values and we will store them without the dash.
                    SetDescString("TaxId", ein.ToString());
                }
                else
                {
                    SetDescString("TaxId", value);
                }
            }
        }

        public bool TaxIdValid
        {
            get
            {
                return string.IsNullOrEmpty(this.TaxId) || EmployerTaxIdentificationNumber.Create(this.TaxId).HasValue;
            }
        }

        public string ChumsId
        {
            get { return GetDescString("ChumsId"); }
            set { SetDescString("ChumsId", value); }
        }

        public Guid EmployeeId
        {
            get { return GetGuid("EmployeeId"); }
            set { SetGuid("EmployeeId", value); }
        }

		public string LicenseNumOfAgent
		{
			get
            {

                if (LicenseInfoList.Count == 0 || OverrideLicenses)
                {
                    // 6/18/2009 dd - no lending licencse list then return old value store in xml.
                    return GetDescString("LicenseNum");
                }

                if (m_parent.sSpState != string.Empty)
                {
                    return GetLendingLicenseByState(m_parent.sSpState, LicenseInfoList);
                }

                return string.Empty;
            }
			set { SetDescString( "LicenseNum", value ); }
		}

        public bool LicenseNumOfAgentIsExpired
        {
            get
            {
                if (LicenseInfoList.Count == 0)
                {
                    return false;
                }

                string _LicenseNumOfAgent = this.LicenseNumOfAgent;
                if (_LicenseNumOfAgent.Length == 0)
                {
                    return false;
                }

                if (m_parent.sSpState != string.Empty)
                {
                    var license = CAgentFields.GetLendingLicenseObjectByState(m_parent.sSpState, LicenseInfoList);
                    return IsLicenseExpired(license);
                }

                return IsLicenseExpired(_LicenseNumOfAgent, LicenseInfoList);
            }
        }

		public string LicenseNumOfCompany
		{
			get
            {
                if (CompanyLicenseInfoList.Count == 0 || OverrideLicenses)
                {
                    return GetDescString("LicenseNumForCompany");
                }

                if (m_parent.sSpState != string.Empty)
                {
                    return GetLendingLicenseByState(m_parent.sSpState, CompanyLicenseInfoList);
                }

                return string.Empty;
            }
			set { SetDescString( "LicenseNumForCompany", value ); }
		}

        public bool LicenseNumOfCompanyIsExpired
        {
            get
            {
                if (CompanyLicenseInfoList.Count == 0)
                {
                    return false;
                }

                string _LicenseNumOfCompany = this.LicenseNumOfCompany;
                if (_LicenseNumOfCompany.Length == 0)
                {
                    return false;
                }

                return IsLicenseExpired(LicenseNumOfCompany, CompanyLicenseInfoList);
            }
        }

		public string DepartmentName
		{
			get { return GetDescString( "DepartmentName" ); }
			set { SetDescString( "DepartmentName", value ); }
		}

        protected override void SetString(string fieldName, string newString)
        {
            if (!IsValid)
            {
                return;
            }

            // we want to automatically set the agent source type to "manually" enterred if it has changed.
            if (AgentSourceT != E_AgentSourceT.ManuallyEntered)
            {
                if (FIELD_FROM_BROKER_CONTACT.Contains(fieldName)
                    || (m_doCopyOfCommision && COMMISION_FIELDS_FROM_BROKER.Contains(fieldName)))
                {
                    var oldString = GetString(fieldName);
                    if (newString.TrimWhitespaceAndBOM() != oldString.TrimWhitespaceAndBOM())
                    {
                        AgentSourceT = E_AgentSourceT.ManuallyEntered;
                    }
                }
            }

            base.SetString(fieldName, newString);
        }

        /// <summary>
        /// Populates the <see cref="CAgentFields"/> instance from a <see cref="RolodexDB"/> entry.
        /// </summary>
        /// <param name="rolodexID">The ID of the relevant rolodex entry.</param>
        /// <param name="brokerUserPrincipal">The broker user.</param>
        /// <remarks>
        /// Commented out fields appear not to be transferred over when "picking from contact",
        /// ignore them when validating.
        /// </remarks>
        public void PopulateFromRolodex(Guid rolodexID, BrokerUserPrincipal brokerUserPrincipal)
        {
            var rolodex = new RolodexDB(rolodexID, m_parent.sBrokerId);
            rolodex.Retrieve();

            this.AgentName = rolodex.Name;

            this.CellPhone = rolodex.AlternatePhone;
            this.City = rolodex.Address.City;
            this.CompanyName = rolodex.CompanyName;
            this.DepartmentName = rolodex.DepartmentName;
            this.EmailAddr = rolodex.Email;
            this.FaxNum = rolodex.Fax;

            this.LicenseNumOfAgent = rolodex.AgentLicenseNumber;
            this.LicenseNumOfCompany = rolodex.CompanyLicenseNumber;
            this.PagerNum = rolodex.Pager;
            this.Phone = rolodex.Phone;
            this.State = rolodex.Address.State;
            this.StreetAddr = rolodex.Address.StreetAddress;
            this.Zip = rolodex.Address.Zipcode;
            this.County = rolodex.Address.County;

            this.PhoneOfCompany = rolodex.PhoneOfCompany;
            this.FaxOfCompany = rolodex.FaxOfCompany;
            this.IsNotifyWhenLoanStatusChange = rolodex.IsNotifyWhenLoanStatusChange;

            // av opm 48610  Agents record: Hide the commission information unless user has Finance read permission
            // Just in case the user was JUST given the permission (while looking at the page) don't try to save
            // the values if the strings are not there there. If one of them is there the rest should be.
            if (brokerUserPrincipal.HasPermission(Permission.AllowAccountantRead))
            {
                m_doCopyOfCommision = CommonFunctions.GetEmployeeRoleWithCommissionInLoanFile(this.EmployeeId, m_parent.sLId) == E_AgentRoleT.Other;
                if (m_doCopyOfCommision)
                {
                    this.CommissionMinBase = rolodex.CommissionMinBase;
                    this.CommissionPointOfLoanAmount = rolodex.CommissionPointOfLoanAmount;
                    this.CommissionPointOfGrossProfit = rolodex.CommissionPointOfGrossProfit;
                }
                else
                {
                    //else it retains the old values
                    this.CommissionMinBase = 0;
                    this.CommissionPointOfLoanAmount = 0;
                    this.CommissionPointOfGrossProfit = 0;
                }
            }

            // OPM 109299
            this.BranchName = rolodex.BranchName;
            this.PayToBankName = rolodex.PayToBankName;
            this.PayToBankCityState = rolodex.PayToBankCityState;
            this.PayToABANumber = rolodex.PayToABANumber;
            this.PayToAccountNumber = rolodex.PayToAccountNumber;
            this.PayToAccountName = rolodex.PayToAccountName;
            this.FurtherCreditToAccountNumber = rolodex.FurtherCreditToAccountNumber;
            this.FurtherCreditToAccountName = rolodex.FurtherCreditToAccountName;

            // OPM 215190
            this.BrokerLevelAgentID = rolodex.ID;

            this.CompanyId = rolodex.CompanyID;
        }


		/// <summary>
		/// Case or file number
		/// </summary>
		public string CaseNum
		{
			get { return GetDescString("CaseNum"); }
			set { SetDescString("CaseNum", value); }
		}

		public string CompanyName
		{
			get
            {
                if (m_NewLicenseSelected &&  m_selectedLicense.DisplayName != string.Empty)
                {
                    return m_selectedLicense.DisplayName;
                }

                return GetDescString("CompanyName");
            }
			set { SetDescString("CompanyName", value); }
		}

		public string StreetAddr
		{
			get
            {
                if (m_NewLicenseSelected && m_selectedLicense.Street != string.Empty)
                {
                    return m_selectedLicense.Street;
                }

                return GetDescString("StreetAddr");
            }
			set { SetDescString("StreetAddr", value); }
		}

		public string City
		{
			get
            {
                if (m_NewLicenseSelected && m_selectedLicense.City != string.Empty)
                {
                    return m_selectedLicense.City;
                }

                return GetDescString("City");
            }
			set { SetDescString("City", value); }
		}

		public string State
		{
			get
            {
                if (m_NewLicenseSelected && m_selectedLicense.AddrState != string.Empty)
                {
                    return m_selectedLicense.AddrState;
                }

                return GetDescString("State");
            }
			set { SetDescString("State", value); }
		}

		public string Zip
		{
			get
            {
                if (m_NewLicenseSelected && m_selectedLicense.Zip != string.Empty)
                {
                    return m_selectedLicense.Zip;
                }

                string zip = GetDescString("Zip");
                if (!string.IsNullOrEmpty(zip) && zip.Length > 5)
                {
                    zip = zip.Substring(0, 5);
                }

                return zip;
            }
			set { SetDescString("Zip", value); }
		}

        public string County
        {
            get { return GetDescString("County"); }
            set { SetDescString("County", value); }
        }

        public string StreetAddr_SingleLine
        {
            get { return Tools.FormatSingleLineAddress(StreetAddr, City, State, Zip); }
        }

        public string StreetAddr_MultiLine
        {
            get { return Tools.FormatAddress(StreetAddr, City, State, Zip); }
        }

		public string OtherAgentRoleTDesc
		{
			get { return GetDescString("OtherAgentRoleTDesc"); }
			set { SetDescString("OtherAgentRoleTDesc", value); }
		}

        [LqbInputModel(type: InputFieldType.Phone)]
		public string Phone
        {
			get { return GetPhoneNum("Phone"); }
			set { SetPhoneNum("Phone", value); }
		}

        [LqbInputModel(type: InputFieldType.Phone)]
		public string CellPhone
        {
			get { return GetPhoneNum("CellPhone"); }
			set { SetPhoneNum("CellPhone", value); }
		}
		public string PagerNum
		{
			get { return GetPhoneNum("PagerNum"); }
			set { SetPhoneNum("PagerNum", value); }
		}

        [LqbInputModel(type: InputFieldType.Phone)]
        public string FaxNum
		{
			get { return GetPhoneNum("FaxNum"); }
			set { SetPhoneNum("FaxNum", value); }
		}

        public string EmployeeIDInCompany
        {
            get { return GetDescString("EmployeeIDInCompany"); }
            set { SetDescString("EmployeeIDInCompany", value); }
        }

        public string CompanyId
        {
            get { return this.GetDescString("CompanyId"); }
            set { this.SetDescString("CompanyId", value); }
        }

        [LqbInputModel(required:true)]
		public string EmailAddr
		{
			get { return GetDescString("EmailAddr"); }
			set { SetDescString("EmailAddr", value); }
		}

        [LqbInputModel(type:InputFieldType.TextArea)]
		public string Notes
		{
			get { return GetDescString("Notes"); }
			set { SetDescString("Notes", value); }
		}

		public string CityStateZip
		{
			get
			{
                if (!IsValid)
                {
                    return string.Empty;
                }

				return Tools.CombineCityStateZip(City, State, Zip);
			}
		}

        public decimal CommissionMinBase
		{
			get{ return GetMoney("CommissionMinBase"); }
			set{ SetMoney("CommissionMinBase", value); }
		}

        [LqbInputModelAttribute(invalid: true)]
        public string CommissionMinBase_rep
		{
			get{ return m_convertLos.ToMoneyString(CommissionMinBase, FormatDirection.ToRep); }
			set{ CommissionMinBase = m_convertLos.ToMoney(value); }
		}

        [LqbInputModel(type:InputFieldType.Percent)]
        public decimal CommissionPointOfLoanAmount
		{
			get{ return GetRate("CommissionPointOfLoanAmount"); }
			set{ SetRate("CommissionPointOfLoanAmount", value); }
		}

        [LqbInputModelAttribute(invalid: true)]
        public string CommissionPointOfLoanAmount_rep
		{
			get{ return m_convertLos.ToRateString(CommissionPointOfLoanAmount); }
			set{ CommissionPointOfLoanAmount = m_convertLos.ToRate(value); }
		}

        [LqbInputModel(type: InputFieldType.Percent)]
        public decimal CommissionPointOfGrossProfit
		{
			get{ return GetRate("CommissionPointOfGrossProfit"); }
			set{ SetRate("CommissionPointOfGrossProfit", value); }
		}

        [LqbInputModelAttribute(invalid: true)]
        public string CommissionPointOfGrossProfit_rep
		{
			get{ return m_convertLos.ToRateString(CommissionPointOfGrossProfit); }
			set{ CommissionPointOfGrossProfit = m_convertLos.ToRate(value); }
		}

        public bool IsUseOriginatorCompensationAmount
        {
            get
            {
                // 3/10/2011 dd - OPM 63885 - If Compensation= LenderPaid and it is check to include compensation to commission
                // then do not use the commission from official agent of Loan Officer
                return (AgentRoleT == E_AgentRoleT.LoanOfficer && m_parent.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid
                    && m_parent.sIsIncludeOriginatorCompensationToCommission);
            }
        }

		public decimal Commission
		{
			get
			{
                // 3/10/2011 dd - OPM 63885 - If Compensation= LenderPaid and it is check to include compensation to commission
                // then do not use the commission from official agent of Loan Officer
                if (IsUseOriginatorCompensationAmount)
                {
                    return m_parent.sOriginatorCompensationTotalAmount;
                }
                else
                {
                    return CommissionMinBase + (CommissionPointOfLoanAmount * m_parent.sLAmtCalc) / 100 +
                        (CommissionPointOfGrossProfit * m_parent.sGrossProfit) / 100;
                }
			}
		}

        [LqbInputModelAttribute(invalid: true)]        
        public string Commission_rep
		{
			get
			{
				return m_convertLos.ToMoneyString(Commission, FormatDirection.ToRep);
			}
		}

        [LqbInputModelAttribute(invalid: true)]
        public bool IsCommissionPaid
		{
            get { return GetBool("IsCommissionPaid"); }
            set { SetBool("IsCommissionPaid", value); }
		}

        [LqbInputModelAttribute(invalid: true)]
        public decimal CommissionPaid
        {
            get
            {
                return IsCommissionPaid ? Commission : 0;
            }
        }

        [LqbInputModelAttribute(invalid: true)]
        public string CommissionPaid_rep
        {
            get { return m_convertLos.ToMoneyString(CommissionPaid, FormatDirection.ToRep); }
        }

        [LqbInputModelAttribute(invalid: true)]
        public string PaymentNotes
        {
            get { return GetDescString("PaymentNotes"); }
            set { SetDescString("PaymentNotes", value); }
        }

		/// <summary>
		/// Investor specific.
		/// </summary>
		public CDateTime InvestorSoldDate
		{
			get{ return GetDateTime("InvestorSoldDate"); }
			set{ SetDateTime("InvestorSoldDate", value); }
		}

        public string InvestorSoldDate_rep
        {
            get { return InvestorSoldDate.ToString(m_convertLos); }
            set { InvestorSoldDate = CDateTime.Create(value, m_convertLos); }
        }

		/// <summary>
		/// Investor specific
		/// </summary>
		public decimal InvestorBasisPoints
		{
			get{ return GetRate("InvestorBasisPoints"); }
			set{ SetRate("InvestorBasisPoints", value); }
		}

		public string InvestorBasisPoints_rep
		{
			get{ return m_convertLos.ToRateString(InvestorBasisPoints); }
			set{ InvestorBasisPoints = m_convertLos.ToRate(value); }
		}

		public bool IsListedInGFEProviderForm
		{
			get { return GetBool("IsListedInGFEProviderForm"); }
			set { SetBool("IsListedInGFEProviderForm", value); }
		}

		public bool IsLenderAssociation
		{
			get { return GetBool("IsLenderAssociation"); }
			set { SetBool("IsLenderAssociation", value); }
		}

		public bool IsLenderAffiliate
		{
			get { return GetBool("IsLenderAffiliate"); }
			set { SetBool("IsLenderAffiliate", value); }
		}

		public bool IsLenderRelative
		{
			get { return GetBool("IsLenderRelative"); }
			set { SetBool("IsLenderRelative", value); }
		}

		public bool HasLenderRelationship
		{
			get { return GetBool("HasLenderRelationship"); }
			set { SetBool("HasLenderRelationship", value); }
		}

		public bool HasLenderAccountLast12Months
		{
			get { return GetBool("HasLenderAccountLast12Months"); }
			set { SetBool("HasLenderAccountLast12Months", value); }
		}

		public bool IsUsedRepeatlyByLenderLast12Months
		{
			get { return GetBool("IsUsedRepeatlyByLenderLast12Months"); }
			set { SetBool("IsUsedRepeatlyByLenderLast12Months", value); }
		}

		public string ProviderItemNumber
		{
			get { return GetDescString("ProviderItemNumber"); }
			set { SetDescString("ProviderItemNumber", value.TrimWhitespaceAndBOM()); }
		}

        [LqbInputModel(type: InputFieldType.Phone)]
		public string PhoneOfCompany
        {
			get
            {
                if (m_NewLicenseSelected && m_selectedLicense.Phone != string.Empty)
                {
                    return m_selectedLicense.Phone;
                }

                return GetPhoneNum("PhoneOfCompany");
            }
			set { SetPhoneNum("PhoneOfCompany", value); }
		}

        [LqbInputModel(type: InputFieldType.Phone)]
		public string FaxOfCompany
        {
			get
            {
                if (m_NewLicenseSelected)
                {
                    return m_selectedLicense.Fax;
                }

                return GetPhoneNum("FaxOfCompany");
            }
			set { SetPhoneNum("FaxOfCompany", value); }
		}

        public bool IsNotifyWhenLoanStatusChange
        {
            get { return GetBool("IsNotifyWhenLoanStatusChange"); }
            set { SetBool("IsNotifyWhenLoanStatusChange", value); }
        }

        public string LoanOriginatorIdentifier
        {
            get { return GetString("LoanOriginatorIdentifier"); }
            set { SetString("LoanOriginatorIdentifier", value); }
        }

        public string CompanyLoanOriginatorIdentifier
        {
            get { return GetString("CompanyLoanOriginatorIdentifier"); }
            set { SetString("CompanyLoanOriginatorIdentifier", value); }
        }

        public string BranchName
        {
            get { return GetString("BranchName"); }
            set { SetString("BranchName", value); }
        }

        public string PayToBankName
        {
            get { return GetString("PayToBankName"); }
            set { SetString("PayToBankName", value); }
        }

        [LqbInputModel(type:InputFieldType.String)]
        public string PayToBankCityState
        {
            get { return GetString("PayToBankCityState"); }
            set { SetString("PayToBankCityState", value); }
        }

        public Sensitive<string> PayToABANumber
        {
            get { return GetString("PayToABANumber"); }
            set { SetString("PayToABANumber", value.Value); }
        }

        public Sensitive<string> PayToAccountNumber
        {
            get { return GetString("PayToAccountNumber"); }
            set { SetString("PayToAccountNumber", value.Value); }
        }

        public string PayToAccountName
        {
            get { return GetString("PayToAccountName"); }
            set { SetString("PayToAccountName", value); }
        }

        public Sensitive<string> FurtherCreditToAccountNumber
        {
            get { return GetString("FurtherCreditToAccountNumber"); }
            set { SetString("FurtherCreditToAccountNumber", value.Value); }
        }

        public string FurtherCreditToAccountName
        {
            get { return GetString("FurtherCreditToAccountName"); }
            set { SetString("FurtherCreditToAccountName", value); }
        }

        private string LendingLicenseXmlContent
        {
            get { return GetString("LendingLicenseXmlContent"); }
            set { SetString("LendingLicenseXmlContent", value); }
        }

        public void SetRawLicenseXmlContent(string xml)
        {
            LendingLicenseXmlContent = xml;
            m_licenseInfoList = null;
        }

        public bool IsLender
        {
            get { return GetBool("IsLender"); }
            set { SetBool("IsLender", value); }
        }

        public bool IsOriginator
        {
            get { return GetBool("IsOriginator"); }
            set { SetBool("IsOriginator", value); }
        }

        public bool IsOriginatorAffiliate
        {
            get { return GetBool("IsOriginatorAffiliate"); }
            set { SetBool("IsOriginatorAffiliate", value); }
        }

        public bool OverrideLicenses
        {
            get { return GetBool("OverrideLicenses"); }
            set { SetBool("OverrideLicenses", value); }
        }

        private LicenseInfoList m_licenseInfoList = null;

        public LicenseInfoList LicenseInfoList
        {
            get
            {
                if (null == m_licenseInfoList)
                {
                    m_licenseInfoList = LicenseInfoList.ToObject(LendingLicenseXmlContent);
                }

                return m_licenseInfoList;
            }
            set
            {
                m_licenseInfoList = value;
            }
        }

        private string CompanyLendingLicenseXmlContent
        {
            get { return GetString("CompanyLendingLicenseXmlContent"); }
            set { SetString("CompanyLendingLicenseXmlContent", value); }
        }

        private LicenseInfoList m_companyLicenseInfoList = null;
        public LicenseInfoList CompanyLicenseInfoList
        {
            get
            {
                if (null == m_companyLicenseInfoList)
                {
                    m_companyLicenseInfoList = LicenseInfoList.ToObject(CompanyLendingLicenseXmlContent);
                }

                return m_companyLicenseInfoList;
            }
            set
            {
                m_companyLicenseInfoList = value;
            }
        }

        public void SetRawCompanyLicenseXmlContent(string xml)
        {
            CompanyLendingLicenseXmlContent = xml;
            m_companyLicenseInfoList = null;
        }

        public static LicenseInfo GetLendingLicenseObjectByState(string state, LicenseInfoList lendingLicenseList)
        {
            if (string.IsNullOrEmpty(state) || lendingLicenseList == null || lendingLicenseList.Count == 0)
            {
                return null;
            }

            LicenseInfo license = null;

            if (lendingLicenseList.Count == 1)
            {
                LicenseInfo o = (LicenseInfo)lendingLicenseList.List[0];
                // 6/15/2009 dd - We will return license if there is only 1 license existed and state does not specify. This is for backward compatibility support.
                if (o.State == string.Empty || o.State.ToUpper() == state)
                {
                    license = o;
                }
            }
            else
            {
                LicenseInfo mostCurrentLicense = null;
                foreach (LicenseInfo o in lendingLicenseList)
                {
                    if (o.State.ToUpper() == state)
                    {
                        // 6/15/2009 dd - Only apply license that does not expire.
                        if (mostCurrentLicense == null)
                        {
                            mostCurrentLicense = o;
                        }
                        else if (mostCurrentLicense.ExpirationDate.CompareTo(o.ExpirationDate) < 0)
                        {
                            mostCurrentLicense = o; // 6/15/2009 dd - Replace the license with one that has farther expiration date.
                        }
                    }
                }

                license = mostCurrentLicense ?? license;
            }

            return license;
        }

        public static string GetLendingLicenseByState(string state, LicenseInfoList lendingLicenseList)
        {
            if (string.IsNullOrEmpty(state) || lendingLicenseList == null || lendingLicenseList.Count == 0)
            {
                return string.Empty;
            }

            string license = string.Empty;

            if (lendingLicenseList.Count == 1)
            {
                LicenseInfo o = (LicenseInfo)lendingLicenseList.List[0];
                // 6/15/2009 dd - We will return license if there is only 1 license existed and state does not specify. This is for backward compatibility support.
                if (o.State == string.Empty || o.State.ToUpper() == state)
                {
                    license = o.License;
                }
            }
            else
            {
                LicenseInfo mostCurrentLicense = null;
                foreach (LicenseInfo o in lendingLicenseList)
                {
                    if (o.State.ToUpper() == state)
                    {
                        // 6/15/2009 dd - Only apply license that does not expire.
                        if (mostCurrentLicense == null)
                        {
                            mostCurrentLicense = o;
                        }
                        else if (mostCurrentLicense.ExpirationDate.CompareTo(o.ExpirationDate) < 0)
                        {
                            mostCurrentLicense = o; // 6/15/2009 dd - Replace the license with one that has farther expiration date.
                        }
                    }
                }

                if (mostCurrentLicense != null)
                {
                    license = mostCurrentLicense.License;
                }
            }

            return license;
        }

        public static bool IsLicenseExpired(LicenseInfo license)
        {
            if (license == null)
            {
                return false;
            }

            return license.ExpirationDate.CompareTo(DateTime.Now) <= 0;
        }


        private static bool IsLicenseExpired(string LicenseNum, LicenseInfoList lendingLicenseList)
        {
            if (string.IsNullOrEmpty(LicenseNum))
            {
                return false;
            }

            foreach (LicenseInfo o in lendingLicenseList)
            {
                if (o.License == LicenseNum)
                {
                    return (o.ExpirationDate.CompareTo(DateTime.Now) <= 0);
                }
            }


            // OPM 246115. 07.11.2016 mf.  Case 222483 added ability to override
            // licenses.  Getting here means that the license list exists, but
            // does not contain this number.  With no expiration date to look up,
            // we have to assume this license is nonexpired otherwise the feature
            // is useless.

            return false;
        }

        public void CopyFromAgent(CAgentFields sourceAgent)
        {
            DataRow source = sourceAgent.CurrentRow;
            DataRow destination = CurrentRow;

            foreach (DataColumn column in source.Table.Columns)
            {
                if (column.ColumnName == RecordIdColumnName || column.ColumnName == "AgentRoleT" || column.ColumnName == "OtherAgentRoleTDesc")
                {
                    continue;
                }

                destination[column.ColumnName] = source[column.ColumnName];
            }
        }

        public object GetElement(IDataPathElement element)
        {
            return PathResolver.GetStringUsingReflection(this, element);
        }
    }
}