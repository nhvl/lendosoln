using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
	public class CDuSubmissionImportData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CDuSubmissionImportData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("aBNm");
            list.Add("aCNm");
            list.Add("aBSsn");
            list.Add("aCSsn");
            list.Add("sLT");
            list.Add("sSpAddr");
            list.Add("sSpCity");
            list.Add("sSpState");
            list.Add("sSpZip");
            list.Add("sLienPosT");
            list.Add("sDuCaseId");
	        list.Add("sfSaveXisDuUnderwriteResponse");
            list.Add("sProd3rdPartyUwResultT");
            list.Add("sAusFindingsPull");
            list.Add("aCreditReportId");
            list.Add("aCreditReportIdLckd");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CDuSubmissionImportData(Guid fileId) : base(fileId, "CDuSubmissionImportData", s_selectProvider)
		{
		}
	}
}
