﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Describes an employment record and employment history.
    /// </summary>
    public interface IEmploymentRecord : ICollectionItemBase2
    {
        /// <summary>
        /// Gets or sets the VOE employee Id.
        /// </summary>
        string EmployeeIdVoe { get; set; }

        /// <summary>
        /// Gets or sets the VOE employer code.
        /// </summary>
        string EmployerCodeVoe { get; set; }

        /// <summary>
        /// Gets or sets the EmployerName.
        /// </summary>
        string EmplrNm { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the record is self
        /// employment.
        /// </summary>
        bool IsSelfEmplmt { get; set; }

        /// <summary>
        /// Gets or sets the employer street address.
        /// </summary>
        string EmplrAddr { get; set; }

        /// <summary>
        /// Gets or sets the employer city.
        /// </summary>
        string EmplrCity { get; set; }

        /// <summary>
        /// Gets or sets the employer zip code.
        /// </summary>
        string EmplrZip { get; set; }

        /// <summary>
        /// Gets or sets the employer phone.
        /// </summary>
        string EmplrBusPhone { get; set; }

        /// <summary>
        /// Gets or sets the employer fax.
        /// </summary>
        string EmplrFax { get; set; }

        /// <summary>
        /// Gets or sets the job title.
        /// </summary>
        string JobTitle { get; set; }

        /// <summary>
        /// Gets or sets the verification re-order date.
        /// </summary>
        CDateTime VerifReorderedD { get; set; }
        
        /// <summary>
        /// Gets or sets the verification re-order date.
        /// </summary>
        string VerifReorderedD_rep { get; set; }

        /// <summary>
        /// Gets or sets the verification expiration date.
        /// </summary>
        CDateTime VerifExpD { get; set; }

        /// <summary>
        /// Gets or sets the verification expiration date.
        /// </summary>
        string VerifExpD_rep { get; set; }

        /// <summary>
        /// Gets or sets the verification received date.
        /// </summary>
        CDateTime VerifRecvD { get; set; }

        /// <summary>
        /// Gets or sets the verification received date.
        /// </summary>
        string VerifRecvD_rep { get; set; }

        /// <summary>
        /// Gets or sets the verification sent date.
        /// </summary>
        CDateTime VerifSentD { get; set; }

        /// <summary>
        /// Gets or sets the verification sent date.
        /// </summary>
        string VerifSentD_rep { get; set; }

        /// <summary>
        /// Gets or sets the employer state.
        /// </summary>
        string EmplrState { get; set; }

        /// <summary>
        /// Gets or sets the employment status.
        /// </summary>
        /// <remarks>
        /// While we can indicate Current or Previous, what is really being 
        /// indicated is the distinction between primary and non-primary
        /// employment.  One can have more than one job at a time, but only 
        /// one is considered primary employment.
        /// </remarks>
        E_EmplmtStat EmplmtStat { get; set; }

        /// <summary>
        /// Gets or sets the Attention entity in the VoE.
        /// </summary>
        string Attention { get; set; }

        /// <summary>
        /// Gets or sets the prepared date.
        /// </summary>
        CDateTime PrepD { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the see attachment text
        /// should be indicated in the verification of employment form.
        /// </summary>
        bool IsSeeAttachment { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the job is current.
        /// </summary>
        bool IsCurrent { get; set; }

        /// <summary>
        /// Gets a value indicating whether this is the primary 
        /// employment.
        /// </summary>
        bool IsPrimaryEmp { get; }

        /// <summary>
        /// Gets the employment length in years.
        /// </summary>
        int EmplmtLenInYrs { get; }

        /// <summary>
        /// Gets the employment length in years.
        /// </summary>
        string EmplmtLenInYrs_rep { get; }

        /// <summary>
        /// Gets the number of months beyond the most recent completed full 
        /// year of employment.
        /// </summary>
        int EmplmtLenInMonths { get; }

        /// <summary>
        /// Gets the number of months beyond the most recent completed full 
        /// year of employment. 
        /// </summary>
        string EmplmtLenInMonths_rep { get; }

        /// <summary>
        /// Gets the employee id of verification signing employee.
        /// </summary>
        Guid VerifSigningEmployeeId { get; }

        /// <summary>
        /// Gets image id of the verification signature.
        /// </summary>
        string VerifSignatureImgId { get; }

        /// <summary>
        /// Gets a value indicating whether the verification has an 
        /// associated signature.
        /// </summary>
        bool VerifHasSignature { get; }

        /// <summary>
        /// Gets a list of the associated VOE data.
        /// </summary>
        List<EmploymentRecordVOEData> EmploymentRecordVOEData { get; }

        /// <summary>
        /// Gets or sets a value indicating whether the borrower has a special relationship with the Employer.
        /// </summary>
        bool IsSpecialBorrowerEmployerRelationship { get; set; }

        /// <summary>
        /// Gets or sets a value determining whether the Ownership share is Blank, 0 - 25%, 25% or more.
        /// </summary>
        SelfOwnershipShare SelfOwnershipShareT { get; set; }

        /// <summary>
        /// Clears the signature.
        /// </summary>
        void ClearSignature();

        /// <summary>
        /// Applies the signature.
        /// </summary>
        /// <param name="employeeId">The employee id.</param>
        /// <param name="imgId">The image id.</param>
        void ApplySignature(Guid employeeId, string imgId);
    }
}
