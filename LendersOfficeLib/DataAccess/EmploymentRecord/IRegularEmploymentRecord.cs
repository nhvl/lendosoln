﻿namespace DataAccess
{
    /// <summary>
    /// A regular (non-primary) employment record.
    /// </summary>
    public interface IRegularEmploymentRecord : IEmploymentRecord
    {
        /// <summary>
        /// Gets or sets the monthly income description.
        /// </summary>
        string MonI_rep { get; set; }

        /// <summary>
        /// Gets or sets the employment start date.
        /// </summary>
        /// <remarks>
        /// It should be noted that regular employment records contain 
        /// EmplmtStartD/EndD,  while primary employment records use 
        /// EmpltStartD (no second 'm').
        /// </remarks>
        string EmplmtStartD_rep { get; set; }

        /// <summary>
        /// Gets or sets the employment end date.
        /// </summary>
        /// <remarks>
        /// It should be noted that regular employment records contain 
        /// EmplmtStartD/EndD,  while primary employment records use 
        /// EmpltStartD (no second 'm').
        /// </remarks>
        string EmplmtEndD_rep { get; set; }
    }
}
