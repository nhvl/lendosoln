﻿namespace DataAccess
{
    /// <summary>
    /// A primary employment record.
    /// </summary>
    public interface IPrimaryEmploymentRecord : IEmploymentRecord
    {
        /// <summary>
        /// Gets or sets the years on the job.
        /// </summary>
        string EmplmtLen_rep { get; set; }

        /// <summary>
        /// Gets or sets the employment start date.
        /// </summary>
        /// <remarks>
        /// It should be noted that regular employment records contain 
        /// EmplmtStartD/EndD,  while primary employment records use 
        /// EmpltStartD (no second 'm').
        /// </remarks>
        string EmpltStartD_rep { get; set; }

        /// <summary>
        /// Gets the years in profession.
        /// </summary>
        decimal ProfLen { get; }

        /// <summary>
        /// Gets or sets the profession start date.
        /// </summary>
        string ProfStartD_rep { get; set; }

        /// <summary>
        /// Gets or sets the years in profession.
        /// </summary>
        string ProfLen_rep { get; set; }

        /// <summary>
        /// Gets or sets the profession length in total months.
        /// </summary>
        string ProfLenTotalMonths { get; set; }

        /// <summary>
        /// Gets profession length in years, rounded down.
        /// <example>If profession length is 3.78 (years), this value will be 
        /// 3 (years).</example>
        /// </summary>
        string ProfLenYears { get; }

        /// <summary>
        /// Gets profession length in months, after removing the year component.
        /// <example>If profession length is 3.78 (years), this value will be 9 
        /// (months; == 0.78 years).</example>
        /// </summary>
        string ProfLenRemainderMonths { get; }

        /// <summary>
        /// Gets or sets the employment length in total months.
        /// </summary>
        string EmplmtLenTotalMonths { get; set; }

        /// <summary>
        /// Sets the employment start date.
        /// </summary>
        /// <param name="date">The employement start date.</param>
        void SetEmploymentStartDate(string date);

        /// <summary>
        /// Sets the profession start date.
        /// </summary>
        /// <param name="date">The profession start date.</param>
        void SetProfessionStartDate(string date);
    }
}
