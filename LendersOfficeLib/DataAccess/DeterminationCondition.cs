﻿// <summary>
// <copyright file="DeterminationCondition.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Eduardo Michel
//  Date:   9/17/2015
// </summary>

namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Xml.Serialization;

    /// <summary>
    /// The Determination Condition holds a three lists.  Each list contains ranges to determine 
    /// whether a loan will apply the Determination Rule's message or not.
    /// </summary>
    [Serializable]
    public class DeterminationCondition
    {
        /// <summary>
        /// Gets or sets The list of ranges to check against the loan's Branch Channel.
        /// </summary>
        /// <value>The list of ranges to check against the loan's Branch Channel.</value>
        [XmlArray("channel")]
        [XmlArrayItem("range", typeof(DeterminationRange))]
        public List<DeterminationRange> Channel { get; set; }

        /// <summary>
        /// Gets or sets The list of ranges to check against the loan's Loan Status.
        /// </summary>
        /// <value>The list of ranges to check against the loan's Loan Status.</value>
        [XmlArray("loanstatus")]
        [XmlArrayItem("range", typeof(DeterminationRange))]
        public List<DeterminationRange> LoanStatus { get; set; }

        /// <summary>
        /// Gets or sets The list of ranges to check against the loan's submission type (Registered or Lock Requested).
        /// </summary>
        /// <value>The list of ranges to check against the loan's submission type (Registered or Lock Requested).</value>
        [XmlArray("submissiontype")]
        [XmlArrayItem("range", typeof(DeterminationRange))]
        public List<DeterminationRange> SubmissionType { get; set; }
    }
}
