
using System;
using System.Data;
using System.IO;
using System.Text;
using System.Collections;
using System.Globalization;
using System.Linq;
using LendersOffice.Common;
using System.Collections.Generic;
using LendersOffice.UI;

namespace DataAccess
{
	public class CBEmpCollection : CEmpCollection
	{
		public CBEmpCollection( CAppBase appData, string xmlSchema, string xmlContent )
			: base( appData, xmlSchema, xmlContent )
		{
		}



		/// <summary>
		/// Update CAppBase with the new values
		/// </summary>
		override public void Flush()
		{
			ExecuteDeathRowRecords();

			var subcoll = GetSubcollection( true, E_EmpGroupT.All );
			foreach(IEmploymentRecord rec in subcoll )
			{			
				rec.PrepareToFlush();
			}

			m_appData.aBEmplmtXmlContent = m_ds.GetXml();
            base.Flush();
		}

		public override bool IsEmplrBusPhoneLckd
		{
			get{ return m_appData.aBEmplrBusPhoneLckd; }
		}

        public override string PersonBusPhone
        {
            get{ return m_appData.aBBusPhone; }
        }
	}

	public class CCEmpCollection : CEmpCollection
	{
		public CCEmpCollection( CAppBase appData, string xmlSchema, string xmlContent )
			: base( appData, xmlSchema, xmlContent )
		{
		}


		/// <summary>
		/// Update CAppBase with the new values
		/// </summary>
		override public void Flush()
		{
			ExecuteDeathRowRecords();

			var subcoll = GetSubcollection( true, E_EmpGroupT.All );
			foreach(IEmploymentRecord rec in subcoll )
			{			
				rec.PrepareToFlush();
			}

			m_appData.aCEmplmtXmlContent = m_ds.GetXml();
            base.Flush();
		}

		public override bool IsEmplrBusPhoneLckd
		{
			get{ return m_appData.aCEmplrBusPhoneLckd; }
		}

        public override string PersonBusPhone
        {
            get{ return m_appData.aCBusPhone; }
        }
	}

	public abstract class CEmpCollection : CXmlRecordCollection, IEmpCollection
	{
		private IPrimaryEmploymentRecord m_primary = null;

		protected CEmpCollection( CAppBase appData, string xmlSchema, string xmlContent )
			: base( appData.LoanData , appData, xmlSchema, xmlContent )
		{
		}

		
		public abstract bool IsEmplrBusPhoneLckd { get; }
        public abstract string PersonBusPhone { get; }


		public ISubcollection GetSubcollection( bool bInclusiveGroup, E_EmpGroupT group )
		{
			return base.GetSubcollectionCore( bInclusiveGroup, group );
		}

		protected override bool IsTypeOfSpecialRecord( Enum statT )
		{

			return CEmpRec.IsTypeOfSpecialRecord( (E_EmplmtStat) statT );
		}
		


		override protected ArrayList SpecialRecords
		{
			get
			{
				ArrayList list = new ArrayList( 1 );
                IEmploymentRecord rec = GetPrimaryEmp( false );
				if( null != rec )
					list.Add( rec );
				return list;
			}
		}

        [LqbInputModelAttribute(invalid : true)]
		public DataView SortedView
		{
			get
			{
				DataTable table = m_ds.Tables[0];
				DataView view = table.DefaultView;
				view.RowFilter = "EmplmtStat NOT IN ('0')"; // Only previous employment recoreds
				view.Sort = "OrderRankValue";
				return view;
			}
		}

		override protected E_ExistingRecordFaith DetermineRawRecordDestiny(ICollectionItemBase2 record )
		{
            IEmploymentRecord rec = record as IEmploymentRecord;
			switch( rec.EmplmtStat )
			{
				case E_EmplmtStat.Current:
					if( null != m_primary )
						return E_ExistingRecordFaith.E_SpecialInvalid;
					m_primary = rec as IPrimaryEmploymentRecord;
					return E_ExistingRecordFaith.E_SpecialTaken;
				default:
					return E_ExistingRecordFaith.E_RegularOK;
			}
		}
		
		override protected void DetachSpecialRecords()
		{
			m_primary = null;
		}


		new public IRegularEmploymentRecord GetRegularRecordAt( int pos )
		{
			return (IRegularEmploymentRecord) base.GetRegularRecordAt( pos );
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="recordId"></param>
		/// <returns></returns>
		new public IRegularEmploymentRecord GetRegRecordOf( Guid recordId )
		{
			return base.GetRegRecordOf( recordId ) as IRegularEmploymentRecord;
		}
		/// <summary>
		/// Do not use this, use foreach on GetSubcollection() instead.
		/// </summary>
		/// <param name="i"></param>
		/// <returns></returns>
		new public IEmploymentRecord GetSpecialRecordAt( int i )
		{
			return (IEmploymentRecord) base.GetSpecialRecordAt( i );
		}

		new public IRegularEmploymentRecord AddRegularRecord()
		{
			return base.AddRegularRecord() as IRegularEmploymentRecord;
		}

		/// <summary>
		/// This method can throw if cannot add
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		protected override ICollectionItemBase2 AddSpecialRecord( Enum type )
		{
			E_EmplmtStat t = (E_EmplmtStat) type;
			switch( t )
			{
				case E_EmplmtStat.Current:
					if( null == m_primary )
						return GetPrimaryEmp( true );
					break;
				default:
					throw new CBaseException(ErrorMessages.Generic, "The requested record type is not a special type of employment record. The requested type = " + t.ToString());

			}
            throw new CBaseException(ErrorMessages.Generic, "Cannot add any more special liability record of the requested type = " + t.ToString());

		}

		/// <summary>
		/// Add new record, let collection object figure out special or regular
		/// and add the right one accordingly.
		/// </summary>
		/// <returns></returns>
		/*
		public CEmpRec AddRecord( E_EmplmtStat empStatT )
		{		
			return (CEmpRec) base.AddRecord( empStatT );
		}
		*/
		

		new public IRegularEmploymentRecord AddRegularRecordAt( int pos )
		{			
			return base.AddRegularRecordAt( pos ) as IRegularEmploymentRecord;
		}

	
		protected override ICollectionItemBase2 CreateRegularRawRecord()
		{
			return CEmpRegRec.Create( m_appData, m_ds, this );
		}
		

		override protected ICollectionItemBase2 Reconstruct( int iRow )
		{
			if( iRow < 0 )
                throw new CBaseException(ErrorMessages.Generic, "Invalid iRow value for xml record.");

			E_EmplmtStat type;
			try
			{
				type = (E_EmplmtStat) int.Parse( m_ds.Tables[0].Rows[iRow]["EmplmtStat"].ToString() );
			}
			catch
			{
				return CEmpPrimaryRec.Reconstruct( m_appData, m_ds, this, iRow );
			}

			 
			switch( type )
			{
				case E_EmplmtStat.Current:	return CEmpPrimaryRec.Reconstruct(m_appData, m_ds, this, iRow );
				case E_EmplmtStat.Previous: return CEmpRegRec.Reconstruct( m_appData, m_ds, this, iRow );
				
				default:
					Tools.LogBug( "CEmpCollection cannot handle a E_EmplmtStat type. Convert it to Previous type" );
					return CEmpRegRec.Reconstruct( m_appData, m_ds, this, iRow );
				

			}
		}

		/// <summary>
		/// Returns nulls if it doesn't exist and bForceCreate = false
		/// </summary>
		public IPrimaryEmploymentRecord GetPrimaryEmp( bool bForceCreate )
		{
			if( null == m_primary && bForceCreate )
			{
				m_primary = CEmpPrimaryRec.Create( m_appData, m_ds, this );
			}
			return m_primary;
		}

        public bool HasMoreThan2PreviousEmployments 
        {
            get 
            {
                return CountRegular > 2;
            }
        }

        [LqbInputModelAttribute(invalid: true)]
        public ISubcollection ExtraPreviousEmployments 
        {
            get 
            {
                var mainList = GetSubcollection(true, E_EmpGroupT.Regular);
                if (mainList.Count > 2) 
                {
                    var list = new ICollectionItemBase[mainList.Count - 2];
                    for (int i = 0; i < mainList.Count - 2; i++) 
                    {
                        list[i] = mainList.GetRecordAt(i + 2);
                    }
                    return CXmlRecordSubcollection.Create(list);
                } 
                else 
                {
                    return CXmlRecordSubcollection.Create(new ICollectionItemBase[0]);
                }
            }
        }


        // 10/28/09 mf. OPM 41248 
        public bool IsEmploymentHistoryValid
        {
            get
            {
                foreach (IEmploymentRecord employmentRecord in GetSubcollection( true, E_EmpGroupT.All ))
                {
                    DateTime endDate;
                    DateTime startDate;

                    if (employmentRecord is IRegularEmploymentRecord)
                    {
                        IRegularEmploymentRecord pastEmployment = (IRegularEmploymentRecord) employmentRecord;

                        if (!DateTime.TryParse( pastEmployment.EmplmtStartD_rep, out startDate ) )
                        {
                            return false;
                        }

                        if (!pastEmployment.IsCurrent)
                        {
                            if (!DateTime.TryParse(pastEmployment.EmplmtEndD_rep, out endDate))
                            {
                                return false;
                            }

                            if (startDate > endDate)
                            {
                                return false;
                            }
                        }
                        
                    }
                    else if (employmentRecord is IPrimaryEmploymentRecord)
                    {
                        IPrimaryEmploymentRecord currentEmployment = (IPrimaryEmploymentRecord) employmentRecord;

                        // UI does not allow entering negative, but it would mess up keyword evaluation,
                        // and import might pull in bad data.
                        if (currentEmployment.EmplmtLenInYrs < 0 || currentEmployment.EmplmtLenInMonths < 0)
                        {
                            return false;
                        }
                        if (currentEmployment.ProfLen < 0)
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
        }

        // 10/28/09 mf. OPM 41248 
        public bool ValidateEmploymentHistoryDates( StringBuilder userMessage, string borrowerName )
        {
            bool isValid = true;
            foreach (IEmploymentRecord employmentRecord in GetSubcollection( true, E_EmpGroupT.All ))
            {
                DateTime endDate;
                DateTime startDate;

                if (employmentRecord is IRegularEmploymentRecord)
                {
                    IRegularEmploymentRecord pastEmployment = (IRegularEmploymentRecord) employmentRecord;
                    string employer = pastEmployment.EmplrNm != string.Empty ? 
                        pastEmployment.EmplrNm : "employer " + pastEmployment.RowPos.ToString();

                    if (!DateTime.TryParse( pastEmployment.EmplmtStartD_rep, out startDate ) ||
                         !DateTime.TryParse( pastEmployment.EmplmtEndD_rep, out endDate ))
                    {
                        userMessage.AppendLine( borrowerName + ": Could not determine employment term for " + employer + ". Please check employment dates." );
                        isValid = false;
                        continue;
                    }

                    if (startDate > endDate)
                    {
                        userMessage.AppendLine( borrowerName + ": Invalid employment term for " + employer + ". Start date of " + startDate.ToShortDateString() + " occurs after end date of " + endDate.ToShortDateString() + "." );
                        isValid = false;
                        continue;
                    }
                }
                else if (employmentRecord is IPrimaryEmploymentRecord)
                {
                    IPrimaryEmploymentRecord currentEmployment = (IPrimaryEmploymentRecord) employmentRecord;
                    string employer = currentEmployment.EmplrNm != string.Empty ?
                        currentEmployment.EmplrNm : "Primary employer";

                    // UI does not allow entering negative, but it would mess up keyword evaluation,
                    // and import might pull in bad data.
                    if (currentEmployment.EmplmtLenInYrs < 0 || currentEmployment.EmplmtLenInMonths < 0)
                    {
                        userMessage.AppendLine( borrowerName + ": Could not determine employment term for " + employer + " Please check years on Job." );
                        isValid = false;
                        continue;
                    }
                    if (currentEmployment.ProfLen < 0 )
                    {
                        userMessage.AppendLine( borrowerName + ": Could not determine years in profession term for " + employer + "." );
                        isValid = false;
                        continue;
                    }
                }
            }
            return isValid;
        }

        public int GetMaxEmploymentGapWithin( int months )
        {
            // Returns the length of the longest continuous employment gap (in months, rounding down)
            // within the period specified in the parameter.

            // 10.24.09 mf. 
            // 1. Collect all jobs' start and end dates.
            // 2. Flatten all overlapping jobs to a single timeline
            // 3. Use this timeline to determine the largest gaps.

            DateTime now = DateTime.Now;
            DateTime intervalStart = now.AddMonths( 0 - months );
            const int iStart = 0;
            const int iEnd = 1;

            var jobEntries = GetSubcollection( true, E_EmpGroupT.All );
            ArrayList jobs = new ArrayList( jobEntries.Count );
            ArrayList entriesInInterval = new ArrayList();

            // 1. Build the list of employment entries from the employment history
            foreach (IEmploymentRecord employmentRecord in jobEntries)
            {
                DateTime endDate;
                DateTime startDate;

                if (employmentRecord is IRegularEmploymentRecord)
                {
                    // This is a standard employment record that can contain start and end dates.

                    IRegularEmploymentRecord pastEmployment = (IRegularEmploymentRecord) employmentRecord;
                    if ( !DateTime.TryParse( pastEmployment.EmplmtStartD_rep, out startDate ) )
                    {
                        continue; // Could not parse start date
                    }

                    if (pastEmployment.IsCurrent) 
                    {
                        // OPM 48903 - To support multiple jobs, jobs in employment history can be marked as 
                        // current.  We treat them like primary employment.
                        endDate = now;
                    }
                    else
                    {
                        if (!DateTime.TryParse(pastEmployment.EmplmtEndD_rep, out endDate))
                        {
                            continue; // Could not parse end date
                        }
                    }

                    // If a job's end date falls in the period of interest, we need it.
                    // Note that this job may have started before the period of interest.  Thats fine.
                    if ( endDate >= intervalStart )
                    {
                        jobs.Add(new DateTime[]{ startDate, endDate } );
                    }
                }
                else if ( employmentRecord is IPrimaryEmploymentRecord)
                {
                    // This is a primary record that only contains years on the job.
                    // We use the years on the job to determine start date.
                    IPrimaryEmploymentRecord currentEmployment = (IPrimaryEmploymentRecord) employmentRecord;
                    startDate = now.AddMonths( 0 - (currentEmployment.EmplmtLenInMonths + (currentEmployment.EmplmtLenInYrs * 12)) );
                    endDate = now;
                    
                    jobs.Add( new DateTime[] { startDate, endDate } );
                }
            }

            if (jobs.Count == 0) return months;  // Was never employed

            // 2. Merge all records into one flat timeline.
            // Basically, we don't care about dates that are contained by
            // a different job.
            List<DateTime> finalList = new List<DateTime>();
            foreach (DateTime[] job in jobs)
            {
                bool useStart = true;
                bool useFinish = true;

                // Determine if start or end date is in the middle of another existing job.
                foreach (DateTime[] compareJob in jobs)
                {
                    if (job == compareJob) continue;

                    if (job[iStart] > compareJob[iStart] && job[iStart] < compareJob[iEnd] )
                    {
                        useStart = false;
                        // This start is in the middle of another job.
                    }
                    if (job[iEnd] > compareJob[iStart] && job[iEnd] < compareJob[iEnd])
                    {
                        useFinish = false;
                        // This end is in the middle of another job.
                    }
                }

                useStart = useStart && !finalList.Contains(job[iStart]);
                useFinish = useFinish && !finalList.Contains(job[iEnd]);

                if (useStart) finalList.Add( job[iStart] );
                if (useFinish) finalList.Add( job[iEnd] );
            }

            // Here we have the flattened list.
            // If data was valid, sorting by date should
            // produce the single employment timeline.
            finalList.Sort();

            if (finalList.Count > 0 && finalList[0] > intervalStart)
            {
                // This borrower was unemployed at the start of the interval, simulate an ended job to create the gap.
                // This is a bit of a hack.
                finalList.Insert(0, intervalStart);
                finalList.Insert(0, intervalStart - TimeSpan.FromDays(1) );
            }
            
            bool isStart = true;
            DateTime previousEnd = DateTime.MinValue;
            TimeSpan largestGap = TimeSpan.Zero;
            DateTime largestGapBegins = DateTime.MinValue;
            
            foreach (DateTime dateOfInterest in finalList)
            {
                if (isStart && previousEnd != DateTime.MinValue)
                {
                    // A new job starts.  Measure this gap
                    TimeSpan gap = dateOfInterest - previousEnd;
                    if (gap > largestGap)
                    {
                        largestGap = gap;
                        largestGapBegins = previousEnd;
                    }
                }

                if (!isStart)
                    previousEnd = dateOfInterest; // This is an end.
                isStart = !isStart;
            }

            DateTime largestGapEnds = largestGapBegins + largestGap;

            // Note this rounds down the months.
            return ( 12 * (largestGapEnds.Year - largestGapBegins.Year) )
                + largestGapEnds.Month - largestGapBegins.Month;
        }

        /// <summary>
        /// Return a primary employment. This will be used in input model reflection only. Should not use this in your code.
        /// </summary>
        public IPrimaryEmploymentRecord PrimaryEmployment{
            get 
            {
                return GetPrimaryEmp(true);
            }
            set 
            {
                this.m_primary = value;
            }
        }

        /// <summary>
        /// Return a primary employment. This will be used in input model reflection only. Should not use this in your code.
        /// </summary>
        public IRegularEmploymentRecord RegularEmploymentTemplate
        {
            get
            {
                return (IRegularEmploymentRecord)this.CreateRegularRawRecord();
            }
        }

        /// <summary>
        /// Return a primary employment. This will be used in input model reflection only. Should not use this in your code.
        /// </summary>
        public List<object> RegularEmployments
        {
            get
            {
                return this.m_sortedList.GetValueList().Cast<object>().ToList();
            }
            set
            {
                if (value == null)
                {
                    return;
                }
                else
                {
                    var regulars = m_sortedList.GetValueList().Cast<IRegularEmploymentRecord>().ToList();
                    regulars.ForEach(x => x.IsOnDeathRow = true);
                    this.Flush();
                    int index = 0;
                    value.Cast<IRegularEmploymentRecord>().ToList().ForEach(x => this.InsertRegularRecordTo(index++, x));
                }
            }
        }

	}
}