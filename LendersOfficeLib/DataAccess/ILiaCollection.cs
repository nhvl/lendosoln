﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;

    /// <summary>
    /// Interface extracted from CLiaCollection.
    /// </summary>
    public interface ILiaCollection : IRecordCollection
    {
        /// <summary>
        /// Gets the extra normal liabilities to print on 1003 pg 4 (additional page).
        /// </summary>
        ISubcollection ExtraNormalLiabilities { get; }

        /// <summary>
        /// Gets the extra normal liabilities to print on 1003 pg 5 (additional page) for the new 1003
        /// The new 1003 only printout 6 liabilities in normal page.
        /// </summary>
        ISubcollection ExtraNormalLiabilitiesInNew1003 { get; }

        /// <summary>
        /// Gets the first 7 regular liabilities to print on 1003 pg 2.
        /// </summary>
        ISubcollection First7NormalLiabilities { get; }

        /// <summary>
        /// Gets a value indicating whether there are more than 7 regular liabilities.
        /// </summary>
        bool HasMoreThan7NormalLiablities { get; }

        /// <summary>
        /// Gets a DataView of the liabilities excluding alimony, job expenses, and child support. Ordered by order rank value.
        /// </summary>
        DataView SortedView { get; }

        /// <summary>
        /// Adds a regular liability to the collection.
        /// </summary>
        /// <returns>The regular liability that was added.</returns>
        new ILiabilityRegular AddRegularRecord();

        /// <summary>
        /// Adds a regular liability at the specified index.
        /// </summary>
        /// <param name="pos">The index.</param>
        /// <returns>The liability that was added.</returns>
        new ILiabilityRegular AddRegularRecordAt(int pos);

        /// <summary>
        /// Update liability collection from an input field model.
        /// </summary>
        /// <param name="inputModelDict">Input field model to update liabilities.</param> 
        void BindFromObjectModel(Dictionary<string, object> inputModelDict);

        /// <summary>
        /// Attempts to create a job expense at the first available slot.
        /// </summary>
        /// <returns>The created job expense. Null if no slots were available.</returns>
        ILiabilityJobExpense CreateNewJobExpenseAtFirstAvailSlot();

        /// <summary>
        /// Gets the alimony liability.
        /// </summary>
        /// <param name="forceCreate">
        /// A value indicating whether the liability should be created if it doesn't already exist.
        /// </param>
        /// <returns>The alimony liability if it exists or was force-created. Null otherwise.</returns>
        ILiabilityAlimony GetAlimony(bool forceCreate);

        /// <summary>
        /// Gets the child support liability.
        /// </summary>
        /// <param name="forceCreate">
        /// A value indicating whether the liability should be created if it doesn't already exist.
        /// </param>
        /// <returns>The child support liability if it exists or was force-created. Null otherwise.</returns>
        ILiabilityChildSupport GetChildSupport(bool forceCreate);

        /// <summary>
        /// Gets the job expense #1 liability.
        /// </summary>
        /// <param name="forceCreate">
        /// A value indicating whether the liability should be created if it doesn't already exist.
        /// </param>
        /// <returns>The job expense #1 if it exists or was force-created. Null otherwise.</returns>
        ILiabilityJobExpense GetJobRelated1(bool forceCreate);

        /// <summary>
        /// Gets the job expense #2 liability.
        /// </summary>
        /// <param name="forceCreate">
        /// A value indicating whether the liability should be created if it doesn't already exist.
        /// </param>
        /// <returns>The job expense #2 if it exists or was force-created. Null otherwise.</returns>
        ILiabilityJobExpense GetJobRelated2(bool forceCreate);

        /// <summary>
        /// Gets a regular liability by id.
        /// </summary>
        /// <param name="recordId">The record id.</param>
        /// <returns>The liability with the specified id.</returns>
        new ILiabilityRegular GetRegRecordOf(Guid recordId);

        /// <summary>
        /// Gets the regular liability at the specified index.
        /// </summary>
        /// <param name="pos">The index.</param>
        /// <returns>The regular liability at the index.</returns>
        new ILiabilityRegular GetRegularRecordAt(int pos);

        /// <summary>
        /// Gets the special liability at the specified index.
        /// </summary>
        /// <param name="i">The index.</param>
        /// <returns>The special liability at the index.</returns>
        new ILiabilitySpecial GetSpecialRecordAt(int i);

        /// <summary>
        /// Gets a subset of the liabilities.
        /// </summary>
        /// <param name="inclusiveGroup">
        /// A value indicating whether the specified groups should be included or excluded.
        /// </param>
        /// <param name="group">The liability groups to either include or exclude.</param>
        /// <returns>The specified subset of the liabilities.</returns>
        ISubcollection GetSubcollection(bool inclusiveGroup, E_DebtGroupT group);

        /// <summary>
        /// Gets a dictionary representation of the liabilities and their input models.
        /// </summary>
        /// <returns>An dictionary containing the input models.</returns>
        Dictionary<string, object> ToInputModel();
    }
}