using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Data.Common;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.IO;

namespace DataAccess
{
	public class CCondFieldsObsolete : CXmlRecordBaseOld
	{
		private CPageBase m_parent;

		// Pass -1 for rowApp to add new row.
		public CCondFieldsObsolete( CPageBase parent, DataSet ds, int iRow )
			: base( parent, ds, iRow, "CondXmlContent" )
		{
			m_parent = parent;
		}

        public CCondFieldsObsolete (CPageBase parent, DataSet ds, Guid recordId) 
            : base (parent, ds, recordId, "CondXmlContent")
        {
            m_parent = parent;
        }
        public CCondFieldsObsolete(CPageBase parent, DataSet ds, Guid recordId, string tableName)
			: base( parent, ds, recordId, tableName )
		{
			m_parent = parent;
		}

		override public void Update()
		{
			base.Update();

			m_parent.sCondDataSet = m_ds;
		}

        public override Enum KeyType 
        {
            get { return E_XmlRecordBaseKeyType.Undefined; }
            set {}
        }

		
		public string CondDesc
		{
			get { return GetDescString( "CondDesc" ); }
			set { SetDescString( "CondDesc", value ); }
		}

		public string PriorToEventDesc
		{
			get { return GetDescString( "PriorToEventDesc" ); }
			set { SetDescString( "PriorToEventDesc", value ); }
		}
		public bool IsDone
		{
			get { return this.GetBool( "IsDone" ); }
			set { SetBool( "IsDone", value ); }
		}
		public CDateTime DoneDate
		{
			get { return GetDateTime( "DoneDate" ); }
			set { SetDateTime( "DoneDate", value ); }
		}
		public string DoneDate_rep
		{
			get { return DoneDate.ToString(m_convertLos); }
			set { DoneDate = CDateTime.Create(value, m_convertLos); }
		}

		public bool IsRequired
		{
			get { return GetBool( "IsRequired" ); }
			set { SetBool( "IsRequired", value ); }
		}
	}
}