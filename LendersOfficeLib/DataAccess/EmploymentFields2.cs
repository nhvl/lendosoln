using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Data.Common;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using LendersOffice.Common;
using System.Linq;
using System.Globalization;

namespace DataAccess
{
    public class CEmpRegRec : CEmpRec, IRegularEmploymentRecord
    {
        static internal IRegularEmploymentRecord Create(CAppBase parent, DataSet ds, IRecordCollection recordList)
        {
            return new CEmpRegRec(parent, ds, recordList);
        }

        static internal IRegularEmploymentRecord Reconstruct(CAppBase parent, DataSet ds, IRecordCollection recordList, int iRow)
        {
            return new CEmpRegRec(parent, ds, recordList, iRow);
        }

        static internal IRegularEmploymentRecord Reconstruct(CAppBase parent, DataSet ds, IRecordCollection recordList, Guid id)
        {
            return new CEmpRegRec(parent, ds, recordList, id);
        }


        // DO NOT USE!  CREATED JUST TO ENABLE SOME QUICK UNIT TESTING OF THE SHIM
        public CEmpRegRec(DataSet data, LosConvert converter, Guid recordId)
            : base(data, converter, recordId)
        {

        }

        /// <summary>
        /// Create new
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="ds"></param>
        /// <param name="recordList"></param>
        private CEmpRegRec(CAppBase parent, DataSet ds, IRecordCollection recordList)
            : base(parent, ds, recordList)
        {
            EmplmtStat = E_EmplmtStat.Previous; // Must be this, reinforce it.
        }

        /// <summary>
        /// Reconstruct
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="ds"></param>
        /// <param name="recordList"></param>
        /// <param name="iRow"></param>
        private CEmpRegRec(CAppBase parent, DataSet ds, IRecordCollection recordList, int iRow)
            : base(parent, ds, recordList, iRow)
        {
            EmplmtStat = E_EmplmtStat.Previous; // Must be this, reinforce it.
        }

        /// <summary>
        /// Reconstruct
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="ds"></param>
        /// <param name="recordList"></param>
        /// <param name="id"></param>
        private CEmpRegRec(CAppBase parent, DataSet ds, IRecordCollection recordList, Guid id)
            : base(parent, ds, recordList, id)
        {
            EmplmtStat = E_EmplmtStat.Previous; // Must be this, reinforce it.
        }

        public override bool IsEmpty
        {
            get
            {
                decimal monthlyIncome;
                return base.IsEmpty
                    && (string.IsNullOrEmpty(this.MonI_rep) 
                        || (decimal.TryParse(this.MonI_rep, NumberStyles.Currency, NumberFormatInfo.CurrentInfo, out monthlyIncome) && monthlyIncome == 0))
                    && string.IsNullOrEmpty(this.EmplmtStartD_rep)
                    && string.IsNullOrEmpty(this.EmplmtEndD_rep)
                    && !this.IsCurrent;
            }
        }

        public string MonI_rep
        {
            get { return GetMoneyVarChar_rep("MonI"); }
            set { SetMoneyVarChar_rep("MonI", value); }
        }

        public string EmplmtStartD_rep
        {
            get { return GetDatetimeVarChar_rep("EmplmtStartD"); }
            set
            {
                // 10/22/2007 dd - The datetime format from FNMA will be CCYYMMDD format. When import we need to convert this
                // to mm/dd/yyyy format in LendingQB. The format conversion only happen for FNMA. We do allow this field to be free form text.
                if (m_convertLos.FormatTargetCurrent == FormatTarget.FannieMaeMornetPlus)
                {
                    try
                    {
                        DateTime dt = DateTime.ParseExact(value, "yyyyMMdd", new System.Globalization.CultureInfo(1033));

                        SetDatetimeVarChar_rep("EmplmtStartD", dt.ToString("MM/dd/yyyy"));
                    }
                    catch
                    {
                        SetDatetimeVarChar_rep("EmplmtStartD", value);
                    }

                }
                else
                {
                    SetDatetimeVarChar_rep("EmplmtStartD", value);
                }
            }
        }

        public string EmplmtEndD_rep
        {
            get
            {
                if (IsCurrent)
                {
                    if (m_convertLos.FormatTargetCurrent == FormatTarget.FannieMaeMornetPlus ||
                        m_convertLos.FormatTargetCurrent == FormatTarget.MismoClosing ||
                        m_convertLos.FormatTargetCurrent == FormatTarget.XsltExport)
                    {
                        // 4/6/2010 dd - Do not return "PRESENT" value when export.
                        return string.Empty;
                    }
                    else
                    {
                        return "PRESENT";
                    }
                }
                else
                {
                    return GetDatetimeVarChar_rep("EmplmtEndD");
                }
            }
            set
            {
                if (IsCurrent)
                {
                    return;
                }
                // 10/22/2007 dd - The datetime format from FNMA will be CCYYMMDD format. When import we need to convert this
                // to mm/dd/yyyy format in LendingQB. The format conversion only happen for FNMA. We do allow this field to be free form text.
                if (m_convertLos.FormatTargetCurrent == FormatTarget.FannieMaeMornetPlus)
                {
                    try
                    {
                        DateTime dt = DateTime.ParseExact(value, "yyyyMMdd", new System.Globalization.CultureInfo(1033));

                        SetDatetimeVarChar_rep("EmplmtEndD", dt.ToString("MM/dd/yyyy"));
                    }
                    catch
                    {
                        SetDatetimeVarChar_rep("EmplmtEndD", value);
                    }

                }
                else
                {
                    SetDatetimeVarChar_rep("EmplmtEndD", value);
                }
            }
        }
        public override bool IsCurrent
        {
            get { return GetBool("IsCurrent"); }
            set { SetBool("IsCurrent", value); }
        }

        override public bool IsPrimaryEmp
        {
            get { return false; }
        }

        override public int EmplmtLenInYrs
        {
            get
            {
                YearMonthParser ym = new YearMonthParser();
                ym.Parse(EmplmtStartD_rep, EmplmtEndD_rep);
                return ym.NumberOfYears;
            }
        }
        override public string EmplmtLenInYrs_rep
        {
            get
            {
                try { return m_convertLos.ToCountString(EmplmtLenInYrs); }
                catch { return ""; }
            }
        }

        override public int EmplmtLenInMonths
        {
            get
            {
                YearMonthParser ym = new YearMonthParser();
                ym.Parse(EmplmtStartD_rep, EmplmtEndD_rep);
                return ym.NumberOfMonths;
            }
        }
        override public string EmplmtLenInMonths_rep
        {
            get
            {
                try { return m_convertLos.ToCountString(EmplmtLenInMonths); }
                catch { return ""; }
            }
        }
    }

    public class CEmpPrimaryRec : CEmpRec, IPrimaryEmploymentRecord
    {
        static internal IPrimaryEmploymentRecord Create(CAppBase parent, DataSet ds, IEmpCollection recordList)
        {
            return new CEmpPrimaryRec(parent, ds, recordList);
        }
        static internal IPrimaryEmploymentRecord Reconstruct(CAppBase parent, DataSet ds, IEmpCollection recordList, int iRow)
        {
            return new CEmpPrimaryRec(parent, ds, recordList, iRow);
        }

        static internal IPrimaryEmploymentRecord Reconstruct(CAppBase parent, DataSet ds, IEmpCollection recordList, Guid id)
        {
            return new CEmpPrimaryRec(parent, ds, recordList, id);
        }

        private IEmpCollection m_coll;

        // DO NOT USE!  CREATED JUST TO ENABLE SOME QUICK UNIT TESTING OF THE SHIM
        public CEmpPrimaryRec(DataSet data, LosConvert converter, Guid recordId, IEmpCollection collection)
            : base(data, converter, recordId)
        {
            this.m_coll = collection;
        }

        /// <summary>
        /// Create new
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="ds"></param>
        /// <param name="recordList"></param>
        private CEmpPrimaryRec(CAppBase parent, DataSet ds, IEmpCollection recordList)
            : base(parent, ds, recordList)
        {
            EmplmtStat = E_EmplmtStat.Current;
            m_coll = recordList;
        }

        /// <summary>
        /// Reconstruct by row pos
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="ds"></param>
        /// <param name="recordList"></param>
        /// <param name="iRow"></param>
        private CEmpPrimaryRec(CAppBase parent, DataSet ds, IEmpCollection recordList, int iRow)
            : base(parent, ds, recordList, iRow)
        {
            EmplmtStat = E_EmplmtStat.Current;
            m_coll = recordList;
        }

        /// <summary>
        /// Reconstruct by record id
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="ds"></param>
        /// <param name="recordList"></param>
        /// <param name="id"></param>
        private CEmpPrimaryRec(CAppBase parent, DataSet ds, IEmpCollection recordList, Guid id)
            : base(parent, ds, recordList, id)
        {
            EmplmtStat = E_EmplmtStat.Current;
            m_coll = recordList;
        }

        public override bool IsEmpty
        {
            get
            {
                return base.IsEmpty
                    && string.IsNullOrEmpty(this.EmplmtLen_rep)
                    && string.IsNullOrEmpty(this.EmpltStartD_rep)
                    && string.IsNullOrEmpty(this.ProfStartD_rep)
                    && string.IsNullOrEmpty(this.ProfLen_rep);
            }
        }

        public string EmplmtLen_rep
        {
            get { return GetDescString("EmplmtLen"); }
            set { SetDescString("EmplmtLen", value); }
        }
        public string EmpltStartD_rep
        {
            get { return GetDatetimeVarChar_rep("EmpltStartD"); }
            set { SetDatetimeVarChar_rep("EmpltStartD", value); }
        }

        public decimal ProfLen
        {
            get
            {
                string rep = ProfLen_rep;
                if (rep.Length == 0)
                    return 0;
                try
                {
                    return decimal.Parse(rep);
                }
                catch
                {
                    Tools.LogError("Cannot convert " + rep + " to decimal in CEmpRec's ProfLen property get method.");
                    return 0;
                }
            }
        }
        public string ProfStartD_rep
        {
            get { return GetDatetimeVarChar_rep("ProfStartD"); }
            set { SetDatetimeVarChar_rep("ProfStartD", value); }
        }

        public string ProfLen_rep
        {
            get { return GetDescString("ProfLen"); }
            set { SetDescString("ProfLen", value); }
        }

        public string ProfLenTotalMonths
        {
            get { return m_convertLos.ConvertYearToMonth(ProfLen_rep); }
            set { ProfLen_rep = m_convertLos.ConvertMonthToYear(value); }
        }

        /// <summary>
        /// Profession length in years, rounded down.<para />
        /// <example>If profession length is 3.78 (years), this value will be 3 (years).</example>
        /// </summary>
        public string ProfLenYears
        {
            get { return m_convertLos.ToCountString((int)Math.Truncate(ProfLen)); }
        }

        /// <summary>
        /// Profession length in months, after removing the year component.<para />
        /// <example>If profession length is 3.78 (years), this value will be 9 (months; == 0.78 years).</example>
        /// </summary>
        public string ProfLenRemainderMonths
        {
            get
            {
                decimal remainderYears = ProfLen % 1;
                return m_convertLos.ConvertYearToMonth(remainderYears.ToString());
            }
        }

        public string EmplmtLenTotalMonths
        {
            get { return m_convertLos.ConvertYearToMonth(EmplmtLen_rep); }
            set { EmplmtLen_rep = m_convertLos.ConvertMonthToYear(value); }
        }

        override public bool IsPrimaryEmp
        {
            get { return true; }
        }

        override public int EmplmtLenInYrs
        {
            get
            {
                YearMonthParser ym = new YearMonthParser();
                ym.Parse(EmplmtLen_rep);
                return ym.NumberOfYears;
            }
        }
        override public string EmplmtLenInYrs_rep
        {
            get
            {
                try { return this.m_convertLos.ToCountString(EmplmtLenInYrs); }
                catch { return ""; }
            }
        }

        /// <summary>
        /// The number of months beyond the most recent completed full year of employment 
        /// </summary>
        override public int EmplmtLenInMonths
        {
            get
            {
                YearMonthParser ym = new YearMonthParser();
                ym.Parse(EmplmtLen_rep);
                return ym.NumberOfMonths;
            }
        }

        /// <summary>
        /// The number of months beyond the most recent completed full year of employment 
        /// </summary>
        override public string EmplmtLenInMonths_rep
        {
            get
            {
                try { return this.m_convertLos.ToCountString(EmplmtLenInMonths); }
                catch { return ""; }
            }
        }

        override public string EmplrBusPhone
        {
            get
            {
                if (m_coll.IsEmplrBusPhoneLckd)
                    return GetPhoneNum("EmplrBusPhone");

                return m_coll.PersonBusPhone;
            }

            set { SetPhoneNum("EmplrBusPhone", value); }
        }

        public bool EmplrBusPhoneLckd
        {
            get
            {
                return this.m_coll.IsEmplrBusPhoneLckd;
            }
        }

        public override bool IsCurrent
        {
            get { return true; }
            set { }
        }

        public void SetEmploymentStartDate(string date)
        {
            EmpltStartD_rep = date;
            EmplmtLen_rep = CalculateYearsSince(GetDateTime("EmpltStartD"));
        }

        public void SetProfessionStartDate(string date)
        {
            ProfStartD_rep = date;
            ProfLen_rep = CalculateYearsSince(GetDateTime("ProfStartD"));
        }

        //keep this in sync with C:\LendOSoln\LendersOfficeApp\newlos\BorrowerEmploymentFrame.ascx.cs
        //Should move that code in instead. Fields are bad.
        private string CalculateYearsSince(CDateTime startD)
        {
            if (!startD.IsValid)
            {
                return string.Empty;
            }

            double years = DateTime.Today.Year - startD.DateTimeForComputation.Year;
            double months = DateTime.Today.Month - startD.DateTimeForComputation.Month;
            int days = DateTime.Today.Day - startD.DateTimeForComputation.Day;

            double avgDaysInMonth = 30.4;
            months += days / avgDaysInMonth;
            years += months / 12;
            return years.ToString("N");

        }
    }

    public abstract class CEmpRec : CXmlRecordBase2, IEmploymentRecord
    {
        protected CAppBase m_parent;
        private List<EmploymentRecordVOEData> voeData = null;

        // DO NOT USE!  CREATED JUST TO ENABLE SOME QUICK UNIT TESTING OF THE SHIM
        public CEmpRec(DataSet data, LosConvert converter, Guid recordId)
            : base(data, converter, recordId)
        {

        }

        /// <summary>
        /// Create new row/record
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="ds"></param>
        /// <param name="recordList"></param>
        /// <param name="iRow"></param>
        protected CEmpRec(CAppBase parent, DataSet ds, IRecordCollection recordList)
            : base(parent, ds, recordList, "EmplmtXmlContent")
        {
            m_parent = parent;
            IsSeeAttachment = true;
        }

        /// <summary>
        /// Reconstruct
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="ds"></param>
        /// <param name="recordList"></param>
        /// <param name="iRow"></param>
        protected CEmpRec(CAppBase parent, DataSet ds, IRecordCollection recordList, int iRow)
            : base(parent, ds, recordList, iRow, "EmplmtXmlContent")
        //CBase parent, DataSet ds, IRecordCollection recordList, int iRow, string tableName
        {
            m_parent = parent;
        }

        /// <summary>
        /// Reconstruct
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="ds"></param>
        /// <param name="recordList"></param>
        /// <param name="id"></param>
        protected CEmpRec(CAppBase parent, DataSet ds, IRecordCollection recordList, Guid id)
            : base(parent, ds, recordList, id, "EmplmtXmlContent")
        {
            m_parent = parent;
        }

        static public bool IsTypeOfSpecialRecord(E_EmplmtStat empT)
        {
            return (E_EmplmtStat.Current == empT);
        }

        public override Enum KeyType
        {
            get { return EmplmtStat; }
            set { EmplmtStat = (E_EmplmtStat)value; }
        }

        public virtual bool IsEmpty
        {
            get
            {
                return string.IsNullOrEmpty(this.EmployeeIdVoe)
                    && string.IsNullOrEmpty(this.EmployerCodeVoe)
                    && string.IsNullOrEmpty(this.EmplrNm)
                    && !this.IsSelfEmplmt
                    && string.IsNullOrEmpty(this.EmplrAddr)
                    && string.IsNullOrEmpty(this.EmplrCity)
                    && string.IsNullOrEmpty(this.EmplrZip)
                    // Check the raw string here, otherwise it may pull from the loan/app data.
                    && string.IsNullOrEmpty(this.GetPhoneNum("EmplrBusPhone"))
                    && string.IsNullOrEmpty(this.EmplrFax)
                    && string.IsNullOrEmpty(this.JobTitle)
                    && !this.VerifReorderedD.IsValid
                    && !this.VerifExpD.IsValid
                    && !this.VerifRecvD.IsValid
                    && !this.VerifSentD.IsValid
                    && string.IsNullOrEmpty(this.EmplrState)
                    && string.IsNullOrEmpty(this.Attention)
                    && !this.PrepD.IsValid
                    && this.IsSeeAttachment // The property IsSeeAttachment defaults to true.
                    && this.VerifSigningEmployeeId == Guid.Empty
                    && string.IsNullOrEmpty(this.VerifSignatureImgId)
                    && !this.EmploymentRecordVOEData.Any()
                    && !this.IsSpecialBorrowerEmployerRelationship
                    && this.SelfOwnershipShareT == SelfOwnershipShare.Blank;
            }
        }

        public List<EmploymentRecordVOEData> EmploymentRecordVOEData
        {
            get
            {
                if (this.voeData == null)
                {
                    var dataXml = this.GetString("EmploymentRecordVOEDataJsonContent");
                    if (string.IsNullOrEmpty(dataXml))
                    {
                        this.voeData = new List<EmploymentRecordVOEData>();
                    }
                    else
                    {
                        this.voeData = SerializationHelper.JsonNetDeserialize<List<EmploymentRecordVOEData>>(dataXml);
                    }
                }

                return this.voeData;
            }
        }

        public string EmployeeIdVoe
        {
            get
            {
                // Done like this to make it easier to contain more than a single record in the future.
                if (this.EmploymentRecordVOEData.Count >= 1)
                {
                    return this.EmploymentRecordVOEData[0].EmployeeIdVoe;
                }

                return string.Empty;
            }

            set
            {
                if (this.EmploymentRecordVOEData.Count < 1)
                {
                    this.EmploymentRecordVOEData.Add(new EmploymentRecordVOEData());    
                }

                this.EmploymentRecordVOEData[0].EmployeeIdVoe = value;
            }
        }

        public string EmployerCodeVoe
        {
            get
            {
                // Done like this to make it easier to contain more than a single record in the future.
                if (this.EmploymentRecordVOEData.Count >= 1)
                {
                    return this.EmploymentRecordVOEData[0].EmployerCodeVoe;
                }

                return string.Empty;
            }

            set
            {
                if (this.EmploymentRecordVOEData.Count < 1)
                {
                    this.EmploymentRecordVOEData.Add(new DataAccess.EmploymentRecordVOEData());
                }

                this.EmploymentRecordVOEData[0].EmployerCodeVoe = value;
            }
        }

        public string EmplrNm
        {
            get { return GetDescString("EmplrNm"); }
            set { SetDescString("EmplrNm", value); }
        }
        public bool IsSelfEmplmt
        {
            get { return GetBool("IsSelfEmplmt"); }
            set { SetBool("IsSelfEmplmt", value); }
        }

        public string EmplrAddr
        {
            get { return GetDescString("EmplrAddr"); }
            set { SetDescString("EmplrAddr", value); }
        }

        public string EmplrCity
        {
            get { return GetDescString("EmplrCity"); }
            set { SetDescString("EmplrCity", value); }
        }

        public string EmplrZip
        {
            get { return GetZipCode("EmplrZip"); }
            set { SetZipCode("EmplrZip", value); }
        }

        virtual public string EmplrBusPhone
        {
            get { return GetPhoneNum("EmplrBusPhone"); }
            set { base.SetPhoneNum("EmplrBusPhone", value); }
        }

        public string EmplrFax
        {
            get { return GetPhoneNum("EmplrFax"); }
            set { SetPhoneNum("EmplrFax", value); }
        }

        public string JobTitle
        {
            get { return GetDescString("JobTitle"); }
            set { SetDescString("JobTitle", value); }
        }

        public CDateTime VerifReorderedD
        {
            get { return GetDateTime("VerifReorderedD"); }
            set { SetDateTime("VerifReorderedD", value); }
        }
        public string VerifReorderedD_rep
        {
            get { return VerifReorderedD.ToString(m_convertLos); }
            set { VerifReorderedD = CDateTime.Create(value, m_convertLos); }
        }

        public CDateTime VerifExpD
        {
            get { return GetDateTime("VerifExpD"); }
            set { SetDateTime("VerifExpD", value); }
        }

        public string VerifExpD_rep
        {
            get { return VerifExpD.ToString(m_convertLos); }
            set { VerifExpD = CDateTime.Create(value, m_convertLos); }
        }

        public CDateTime VerifRecvD
        {
            get { return GetDateTime("VerifRecvD"); }
            set { SetDateTime("VerifRecvD", value); }
        }
        public string VerifRecvD_rep
        {
            get { return VerifRecvD.ToString(m_convertLos); }
            set { VerifRecvD = CDateTime.Create(value, m_convertLos); }
        }
        public CDateTime VerifSentD
        {
            get { return GetDateTime("VerifSentD"); }
            set { SetDateTime("VerifSentD", value); }
        }
        public string VerifSentD_rep
        {
            get { return VerifSentD.ToString(m_convertLos); }
            set { VerifSentD = CDateTime.Create(value, m_convertLos); }
        }

        public string EmplrState
        {
            get { return GetState("EmplrState"); }
            set { SetState("EmplrState", value); }
        }

        public E_EmplmtStat EmplmtStat
        {
            get { return (E_EmplmtStat)GetEnum("EmplmtStat", E_EmplmtStat.Current); }
            set { SetEnum("EmplmtStat", value); }
        }

        public string Attention
        {
            get { return GetDescString("Attention"); }
            set { SetDescString("Attention", value); }
        }

        public CDateTime PrepD
        {
            get { return GetDateTime("PrepD"); }
            set { SetDateTime("PrepD", value); }
        }

        public bool IsSeeAttachment
        {
            get { return GetBool("IsSeeAttachment", true); }
            set { SetBool("IsSeeAttachment", value); }
        }
        public override void PrepareToFlush()
        {
            if (this.voeData != null)
            {
                this.SetString("EmploymentRecordVOEDataJsonContent", SerializationHelper.JsonNetSerialize(this.voeData));
            }
        }
        abstract public bool IsCurrent
        {
            get;
            set;
        }
        abstract public bool IsPrimaryEmp
        {
            get;
        }
        abstract public int EmplmtLenInYrs
        {
            get;
        }
        abstract public string EmplmtLenInYrs_rep
        {
            get;
        }

        /// <summary>
        /// The number of months beyond the most recent completed full year of employment 
        /// </summary>
        abstract public int EmplmtLenInMonths
        {
            get;
        }
        /// <summary>
        /// The number of months beyond the most recent completed full year of employment 
        /// </summary>
        abstract public string EmplmtLenInMonths_rep
        {
            get;
        }

        public Guid VerifSigningEmployeeId
        {
            get
            {
                return GetGuid("VerifSigningEmployeeId");
            }
            private set
            {
                SetGuid("VerifSigningEmployeeId", value);
            }
        }

        public string VerifSignatureImgId
        {
            get
            {
                return GetString("VerifSignatureImgId");
            }
            private set
            {
                SetString("VerifSignatureImgId", value);
            }
        }

        public bool VerifHasSignature
        {
            get
            {
                return false == string.IsNullOrEmpty(VerifSignatureImgId);
            }
        }

        public void ClearSignature()
        {
            VerifSignatureImgId = "";
            VerifSigningEmployeeId = Guid.Empty;
        }

        public void ApplySignature(Guid employeeId, string imgId)
        {
            VerifSignatureImgId = imgId;
            VerifSigningEmployeeId = employeeId;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the borrower has a special relationship with a party involved in the transaction.
        /// </summary>
        public bool IsSpecialBorrowerEmployerRelationship
        {
            get
            {
                if (this.IsSelfEmplmt)
                {
                    return GetBool("IsSpecialBorrowerEmployerRelationship");
                }

                return false;
            }
            set { SetBool("IsSpecialBorrowerEmployerRelationship", value); }
        }

        /// <summary>
        /// Gets or sets a value determining whether the value has an ownership share of null, 0 < 25%, or 25%+.
        /// </summary>
        public SelfOwnershipShare SelfOwnershipShareT
        {
            get
            {
                if (this.IsSelfEmplmt)
                {
                    return (SelfOwnershipShare)GetEnum("SelfOwnershipShare", SelfOwnershipShare.Blank);
                }

                return SelfOwnershipShare.Blank;
            }
            set { SetEnum("SelfOwnershipShare", value); }
        }
    }

    /// <summary>
    /// This class is used to store Verbal Verification of Employment (VOE) Data.  Is it separate from written VOEs.
    /// </summary>
    public class VerbalVerificationOfEmploymentRecord
    {
        public Guid Id { get; set; }
        public string BorrowerName { get; set; }
        public string EmployerName { get; set; }

        public string EmployerStreet { get; set; }
        public string EmployerCity { get; set; }
        public string EmployerState { get; set; }
        public string EmployerZip { get; set; }

        [JsonIgnore]
        public string EmployerAddress
        {
            get
            {
                return EmployerStreet + ", " + EmployerCity + ", " + EmployerState + ", " + EmployerZip;
            }
        }

        public string EmployerPhone { get; set; }
        public string ThirdPartySource { get; set; }
        public string EmploymentStatus { get; set; }
        public string BorrowerTitle { get; set; }
        public string EmployerContactName { get; set; }
        public string EmployerContactTitle { get; set; }

        public string PreparedBy { get; set; }
        public string PreparedByTitle { get; set; }

        [JsonIgnore]
        public CDateTime DateOfHire { get; set; }

        public string DateOfHire_rep
        {
            get
            {
                return losConvert.ToDateTimeString(DateOfHire);
            }

            set
            {
                DateOfHire = CDateTime.Create(value, losConvert);
            }
        }

        [JsonIgnore]
        public CDateTime DateOfTermination { get; set; }

        public string DateOfTermination_rep
        {
            get
            {
                return losConvert.ToDateTimeString(DateOfTermination);
            }

            set
            {
                DateOfTermination = CDateTime.Create(value, losConvert);
            }
        }

        [JsonIgnore]
        public CDateTime DateOfCall { get; set; }

        public string DateOfCall_rep
        {
            get
            {
                return losConvert.ToDateTimeString(DateOfCall);
            }

            set
            {
                DateOfCall = CDateTime.Create(value, losConvert);
            }
        }

        private LosConvert losConvert;
        public VerbalVerificationOfEmploymentRecord()
        {
            losConvert = new LosConvert();
            Id = Guid.NewGuid();
        }


        public bool IsSpecialBorrowerEmployeeRelationship { get; set; }
        public int SelfOwnershipShare { get; set; }
    }
}