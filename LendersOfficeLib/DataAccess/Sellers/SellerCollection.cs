using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using LendersOffice.UI;

namespace DataAccess.Sellers
{
    [XmlRoot] //This is designed to mimic DataAccess.Trust.TrustCollection
    public class SellerCollection : IEquatable<SellerCollection>
    {
        public SellerCollection()
        {

        }

        public List<Seller> ListOfSellers = new List<Seller>();

        public string Serialize()
        {
            XmlSerializer serializer = new XmlSerializer(this.GetType());
            string ret;
            using (var writer = new StringWriter())
            {
                serializer.Serialize(writer, this);
                ret = writer.ToString();
            }
            return ret;
        }

        public static SellerCollection Deserialize(string input)
        {
            XmlSerializer deserializer = new XmlSerializer(typeof(SellerCollection));
            using (var sr = new StringReader(input))
            {
                return (SellerCollection)deserializer.Deserialize(sr);
            }
        }

        #region IEquatable<SellerCollection> Members

        public override bool Equals(object obj)
        {
            return this.Equals(obj as SellerCollection);
        }

        public bool Equals(SellerCollection other)
        {
            if (other == null) return false;

            if (this.ListOfSellers.SequenceEqual(other.ListOfSellers)) return true;

            if (this.IsEmpty() && other.IsEmpty()) return true;

            return false;
        }

        public bool IsEmpty()
        {
            if (this.ListOfSellers == null || this.ListOfSellers.Count == 0) return true;
            foreach (var seller in this.ListOfSellers)
            {
                if (!seller.IsEmpty())
                    return false;
            }
            return true;
        }

        public override int GetHashCode()
        {
            //For compatibility with .Equals; I'm pretty sure null hashes into 0 so try not to collide with that.
            if (this.IsEmpty()) return 1;

            unchecked // Overflow is fine, just wrap
            {
                int hash = 17;
                int filler = 2; //Just a prime number that will fill in when there's a null; 29 + 2 == 31, another prime
                hash = hash * 29 + (ListOfSellers == null ? filler : ListOfSellers.GetHashCode());
                return hash;
            }
        }

        #endregion

        public void PopulateFromAgents(List<CAgentFields> agentSellers)
        {

            ListOfSellers = new List<Seller>();
            if (agentSellers.Count < 1 || (agentSellers.Count == 1 && agentSellers.First() == CAgentFields.Empty))
            {
                //There were no Agents of Role Seller, so we don't have anything to populate.
            }
            else
            {
                foreach (var agentSeller in agentSellers)
                {
                    ListOfSellers.Add(new Seller()
                    {
                        Name = agentSeller.AgentName,
                        AgentId = agentSeller.RecordId,
                        Address = new DataAccess.Trust.TrustCollection.Address()
                        {
                            StreetAddress = agentSeller.StreetAddr,
                            City = agentSeller.City,
                            State = agentSeller.State,
                            PostalCode = agentSeller.Zip
                        }
                    });
                }
            }
        }
    }

    public class Seller : IEquatable<Seller>
    {
        [XmlAttribute]
        public string Name {get; set;}

        public DataAccess.Trust.TrustCollection.Address Address { get; set; }

        [XmlAttribute]
        public E_SellerType Type { get; set; }

        private E_SellerEntityType entityType;

        [XmlAttribute]
        public E_SellerEntityType EntityType
        {
            get { return Type == E_SellerType.Individual ? E_SellerEntityType.Undefined : entityType; }
            set { entityType = value; }
        }

        [XmlAttribute]
        public bool EntityTypeReadOnly
        {
            get
            {
                return Type == E_SellerType.Individual;
            }
        }
        
        [XmlAttribute]
        public Guid AgentId { get; set; }

        public bool IsEmpty()
        {
            return string.IsNullOrEmpty(Name) &&
                Address.IsEmpty() &&
                EntityType == E_SellerEntityType.Undefined;
        }

        #region IEquatable<Seller> Members
        public override bool Equals(object obj)
        {
            return this.Equals(obj as Seller);
        }

        public bool Equals(Seller other)
        {
            if (other == null) return false;

            return this.Name == other.Name &&
                   this.Address == other.Address &&
                   this.Type == other.Type &&
                   this.EntityType == other.EntityType;
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 17;
                int filler = 2; //Just a prime number that will fill in when there's a null - 29 + 2 == 31, another prime
                hash = hash * 29 + (Name == null ? filler : Name.GetHashCode());
                hash = hash * 29 + (Address == null ? filler : Address.GetHashCode());
                hash = hash * 29 + Type.GetHashCode();
                hash = hash * 29 + EntityType.GetHashCode();

                return hash;
            }
        }

         #region JsonHandler

        /// <summary>
        /// Return then json input model of an object.
        /// </summary>
        /// <returns>An Input field type of the current seller node.</returns>
        public SimpleObjectInputFieldModel ToInputFieldModel()
        {
            SimpleObjectInputFieldModel objInputFieldModel = new SimpleObjectInputFieldModel();

            objInputFieldModel.Add("Name",ModelFromData(Name, InputFieldType.String));

            objInputFieldModel.Add("AgentId",ModelFromData(AgentId.ToString(), InputFieldType.String));

            var stateModel = ModelFromData(Type.ToString(), InputFieldType.DropDownList);
            stateModel.Options = EnumUtilities.GetValuesFromType(Type.GetType());
            objInputFieldModel.Add("Type", stateModel);

            var entityTypeeModel = ModelFromData(EntityType.ToString(), InputFieldType.DropDownList);
            entityTypeeModel.Options = EnumUtilities.GetValuesFromType(EntityType.GetType());
            objInputFieldModel.Add("EntityType", entityTypeeModel);

            return objInputFieldModel;
        }

        public Seller() 
        {
            this.Address = new Trust.TrustCollection.Address();
        }

        /// <summary>
        /// Update other income instance from an input field model.
        /// </summary>
        /// <param name="objInputFieldModel">Input field model to update other income.</param> 
        public Seller(SimpleObjectInputFieldModel objInputFieldModel): base()
        {
            Name = DataFromModel(objInputFieldModel, "Name");
            
            AgentId = new Guid(DataFromModel(objInputFieldModel, "AgentId"));

            int tempInt;

            if (int.TryParse(DataFromModel(objInputFieldModel, "Type"), out tempInt))
            {
                Type = (E_SellerType)tempInt;
            }

            if (int.TryParse(DataFromModel(objInputFieldModel, "EntityType"), out tempInt))
            {
                EntityType = (E_SellerEntityType)tempInt;
            }
        }

        private static InputFieldModel ModelFromData(string value, InputFieldType type)
        {
            var model = new InputFieldModel();
            model.Value = value;
            model.Type = type;
            return model;
        }

        private static InputFieldModel ModelFromData(string value, InputFieldType type, bool readOnly)
        {
            var model = ModelFromData(value, type);
            model.IsReadOnly = readOnly;
            return model;
        }

        private static string DataFromModel(SimpleObjectInputFieldModel objInputFieldModel, string field)
        {
           if (objInputFieldModel.ContainsKey(field))
           {
               return objInputFieldModel[field].Value;
           }
           return string.Empty;
        }
        #endregion

        #endregion
    }
}