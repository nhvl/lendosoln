﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{

    /// <summary>
    /// POCO object to store fields needed by pricing
    /// </summary>
    public sealed class QMPricingFields
    {
        public decimal sQMAveragePrimeOfferR { get; set; }

        public decimal sQMQualR { get; set; }

        public decimal sQMQualBottom { get; set; }

        public decimal sQMTotFeeAmount { get; set; }

        public decimal sQMTotFinFeeAmount { get; set; }

        public decimal sQMExclDiscntPntNonCalc { get; set; }

        public bool sQMExclDiscntPntLck { get; set; }

        public decimal sQMNonExcludableDiscountPointsPc { get; set; }

        public decimal sQMGfeDiscountPointF { get; set; }

        public decimal sLDiscntBaseAmt { get; set; }

        public decimal sQMFhaUfmipAtClosing { get; set; }

        public decimal sQMMaxPrePmntPenalty { get; set; }

        public decimal sGfeOriginatorCompF { get; set; }

        public decimal sQMGfeDiscountPointFPc { get; set; }

        public int sGfeDiscountPointFProps { get; set; }

        public string sQMExcessUpfrontMIPCalcDesc { get; set; }

        public decimal sQMLAmt { get; set; }

        public string sQMLAmtCalcDesc { get; set; }

        public decimal sQMExcessUpfrontMIP { get; set; }

        public decimal sQMMaxPointAndFeesAllowedAmt { get; set; }

        public bool sQMLoanDoesNotHaveNegativeAmort { get; set; }

        public bool sQMLoanDoesNotHaveBalloonFeature { get; set; }
        
        public bool sQMLoanDoesNotHaveAmortTermOver30Yr { get; set; }
        
        public bool sQMLoanHasDtiLessThan43Pc { get; set; }

        public E_sLT sLT { get; set; }

        public decimal sProMInsR { get; set; }

        public E_sLienPosT sLienPosT { get; set; }

        public bool sIsConformingLAmt { get; set; }

        // OPM 184815 - Note: sQMIsEligibleByLoanPurchaseAgency is set based on
        // broker bit BrokerDB.AssumePmlQmEligibleForGse NOT on LFF_QM bit of same name.
        public bool sQMIsEligibleByLoanPurchaseAgency { get; set; }
    }

    /// <summary>
    /// QMI implementation for pricing. Sadly these calculations depend on the par rate of the merge group. 
    /// We wont know the QM status until after pricing is complete.
    /// </summary>
    public sealed class QMStatusCalculator
    {
        /// <summary>
        /// Copied from base.cs
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        private decimal SumMoney(params decimal[] args)
        {
            decimal total = 0;
            foreach (decimal a in args)
            {
                total += Math.Round(a, 2);
            }
            return total;
        }
        private bool m_ignoreBFCheckbox = false;
        private LosConvert m_convert; 

        /// <summary>
        /// The one dynamic field.
        /// </summary>
        private decimal sQMParR { get; set; }

        public string sQMParR_rep { get { return m_convert.ToRateString(sQMParR); } }


        private decimal sApr { get; set; }
        public string sApr_rep { get { return m_convert.ToRateString(sApr); } }

        private QMPricingFields Fields { get; set; }


        public QMStatusCalculator(decimal parRate, string apr_rep, QMPricingFields fields, LosConvert convert, decimal rate)
        {
            this.sQMParR = parRate;
            this.Fields = fields;
            this.m_convert = convert;
            this.sApr = m_convert.ToRate(apr_rep);
            this.m_ignoreBFCheckbox = rate >= parRate;
        }

        public string sQMAprRSpreadCalcDesc
        {
            get
            {
                return string.Format("= {0} APR - {1} APOR", sApr_rep, sQMAveragePrimeOfferR_rep);
            }
        }

        private decimal sQMAprRSpread
        {
            get
            {
                return sApr - sQMAveragePrimeOfferR;
            }
        }

        public string sQMAprRSpread_rep
        {
            get { return m_convert.ToRateString(sQMAprRSpread); }
        }

        #region loan data fields

        public decimal sQMGfeDiscountPointFPc { get { return Fields.sQMGfeDiscountPointFPc; } }

        public int sGfeDiscountPointFProps { get { return Fields.sGfeDiscountPointFProps; } }

        public string sQMExcessUpfrontMIPCalcDesc { get { return Fields.sQMExcessUpfrontMIPCalcDesc; } }

        public decimal sQMLAmt { get { return Fields.sQMLAmt; } }

        public string sQMLAmtCalcDesc { get { return Fields.sQMLAmtCalcDesc; } }

        public decimal sQMExcessUpfrontMIP { get { return Fields.sQMExcessUpfrontMIP; } }

        public decimal sQMMaxPointAndFeesAllowedAmt { get { return Fields.sQMMaxPointAndFeesAllowedAmt; } }

        public string sQMMaxPointAndFeesAllowedAmt_rep { get { return m_convert.ToMoneyString(sQMMaxPointAndFeesAllowedAmt, FormatDirection.ToRep); } }

        public bool sQMLoanDoesNotHaveNegativeAmort { get { return Fields.sQMLoanDoesNotHaveNegativeAmort; } }

        public bool sQMLoanDoesNotHaveBalloonFeature { get { return Fields.sQMLoanDoesNotHaveBalloonFeature; } }

        public bool sQMLoanDoesNotHaveAmortTermOver30Yr { get { return Fields.sQMLoanDoesNotHaveAmortTermOver30Yr; } }

        public bool sQMLoanHasDtiLessThan43Pc { get { return Fields.sQMLoanHasDtiLessThan43Pc; } }

        public decimal sQMAveragePrimeOfferR { get { return Fields.sQMAveragePrimeOfferR; } }

        public decimal sQMQualR { get { return Fields.sQMQualR; } }

        public decimal sQMQualBottom { get { return Fields.sQMQualBottom; } }

        public decimal sQMTotFeeAmount { get { return Fields.sQMTotFeeAmount; } }

        public string sQMTotFeeAmount_rep { get { return m_convert.ToMoneyString(Fields.sQMTotFeeAmount, FormatDirection.ToRep); } }

        public decimal sQMTotFinFeeAmount { get { return Fields.sQMTotFinFeeAmount; } }

        public decimal sQMExclDiscntPntNonCalc { get { return Fields.sQMExclDiscntPntNonCalc; } }

        public bool sQMExclDiscntPntLck { get { return Fields.sQMExclDiscntPntLck; } }

        public decimal sQMNonExcludableDiscountPointsPc { get { return Fields.sQMNonExcludableDiscountPointsPc; } }

        public decimal sQMGfeDiscountPointF { get { return Fields.sQMGfeDiscountPointF; } }

        public decimal sLDiscntBaseAmt { get { return Fields.sLDiscntBaseAmt; } }

        public decimal sQMFhaUfmipAtClosing { get { return Fields.sQMFhaUfmipAtClosing; } }

        public decimal sQMMaxPrePmntPenalty { get { return Fields.sQMMaxPrePmntPenalty; } }

        public string sQMMaxPrePmntPenalty_rep { get { return m_convert.ToMoneyString(sQMMaxPrePmntPenalty, FormatDirection.ToRep); } }


        public decimal sGfeOriginatorCompF { get { return Fields.sGfeOriginatorCompF; } }

        public string sGfeOriginatorCompF_rep { get { return m_convert.ToMoneyString(sGfeOriginatorCompF, FormatDirection.ToRep); } }

        public string sQMAveragePrimeOfferR_rep { get { return m_convert.ToRateString(sQMAveragePrimeOfferR); } }

        public bool sQMIsEligibleByLoanPurchaseAgency { get { return Fields.sQMIsEligibleByLoanPurchaseAgency; } }  // OPM 184815

        #endregion 


        #region calculated fields


        public string sQMLAmt_rep
        {
            get { return m_convert.ToMoneyString(sQMLAmt, FormatDirection.ToRep); }
        }



        public string sQMParRSpreadCalcDesc
        {
            get { return string.Format("= {0} Par rate - {1} APOR", sQMParR_rep, sQMAveragePrimeOfferR_rep); } 
        }


        public decimal sQMParRSpread
        {
            get
            {
                return sQMParR - Fields.sQMAveragePrimeOfferR;
            }
        }

        public string sQMParRSpread_rep
        {
            get { return m_convert.ToRateString(sQMParRSpread); }
        }

        public string sQMExcessUpfrontMIP_rep { get { return m_convert.ToMoneyString(Fields.sQMExcessUpfrontMIP, FormatDirection.ToRep); } }

        public decimal sQMExclDiscntPnt
        {
            get
            {
                if (Fields.sQMExclDiscntPntLck)
                {
                    return Fields.sQMExclDiscntPntNonCalc;
                }

                if (LosConvert.GfeItemProps_BF(Fields.sGfeDiscountPointFProps) && !m_ignoreBFCheckbox)
                {
                    return CPageBase.GetExcludableDiscountPointsPc(Fields.sQMAveragePrimeOfferR, sQMParR);
                }

                return 0;
            }
        
        }

        public decimal sQMExcessDiscntFPc
        {
            get
            {
                return Math.Max(Fields.sQMGfeDiscountPointFPc - Fields.sQMNonExcludableDiscountPointsPc - sQMExclDiscntPnt, 0) + Fields.sQMNonExcludableDiscountPointsPc;
            }
        }

        public string sQMExcessDiscntFPc_rep
        {
            get { return m_convert.ToRateString(sQMExcessDiscntFPc); }
        }


        public string sQMExcessDiscntFCalcDesc
        {
            get
            {
                string msg = "{0} Discount points - {1} Excludable discount points";

                if (sQMNonExcludableDiscountPointsPc != 0)
                {
                    msg = "(" + msg + ")\n+ {2} Non-excludable discount points";
                    msg = string.Format(msg, m_convert.ToRateString(sQMGfeDiscountPointFPc - sQMNonExcludableDiscountPointsPc),
                                        m_convert.ToRateString(sQMExclDiscntPnt), m_convert.ToRateString(sQMNonExcludableDiscountPointsPc));
                }
                else
                {
                    msg = string.Format(msg, m_convert.ToRateString(Fields.sQMGfeDiscountPointFPc), m_convert.ToRateString(sQMExclDiscntPnt));

                    if (sQMGfeDiscountPointFPc > 0 && sQMExcessDiscntFPc == 0)
                    {
                        msg += " (entire amount excluded)";
                    }
                }

                return msg;
            }
        }

        public decimal sQMExcessDiscntF
        {
            get { return Fields.sLDiscntBaseAmt * sQMExcessDiscntFPc / 100; }
        }

        public string sQMExcessDiscntF_rep
        {
            get { return m_convert.ToMoneyString(sQMExcessDiscntF, FormatDirection.ToRep); }
        }

        public decimal sQMTotFeeAmt
        {
            get
            {
                return SumMoney(Fields.sQMTotFeeAmount, Fields.sQMExcessUpfrontMIP, Fields.sQMMaxPrePmntPenalty, sQMExcessDiscntF, Fields.sGfeOriginatorCompF);
            }
        }

        public string sQMTotFeeAmt_rep
        {
            get { return m_convert.ToMoneyString(sQMTotFeeAmt, FormatDirection.ToRep); }
        }

        public bool sQMLoanPassesPointAndFeesTest
        {
            get 
            {
                return Fields.sQMMaxPointAndFeesAllowedAmt >= sQMTotFeeAmt;
            }
        }


        public E_sQMStatusT GetQMStatus()
        {

            bool[] checks = new bool[] { 
                sQMLoanPassesPointAndFeesTest,
                Fields.sQMLoanDoesNotHaveNegativeAmort,
                Fields.sQMLoanDoesNotHaveBalloonFeature,
                Fields.sQMLoanDoesNotHaveAmortTermOver30Yr
            };

            if (checks.Any(p => p == false))
            {
                return E_sQMStatusT.Ineligible;
            }

            // OPM 184815 - If Broker bit BrokerDB.AssumePmlQmEligibleForGse enabled, evalueate QM as though
            // sQMIsEligibleByLoanPurchaseAgency is true (ie. Ignore sQMLoanHasDtiLessThan43Pc).
            if (!Fields.sQMLoanHasDtiLessThan43Pc && !Fields.sQMIsEligibleByLoanPurchaseAgency)
            {
                return E_sQMStatusT.Ineligible;
            }

            return E_sQMStatusT.ProvisionallyEligible;
        }

        public E_HighPricedMortgageT GetHPMLStatus()
        {
            if (sQMAveragePrimeOfferR <= 0 || sApr <= 0)
                return E_HighPricedMortgageT.Unknown;

            E_sQMStatusT sQMStatusT = GetQMStatus();

            if (sQMStatusT == E_sQMStatusT.Eligible || sQMStatusT == E_sQMStatusT.ProvisionallyEligible)
            {
                if (Fields.sLT == E_sLT.FHA)
                {
                    if (sApr - sQMAveragePrimeOfferR > 1.150M + Fields.sProMInsR)
                        return E_HighPricedMortgageT.HigherPricedQm;
                    else
                        return E_HighPricedMortgageT.None;
                }
                else
                {
                    if (Fields.sLienPosT == E_sLienPosT.First)
                    {
                        if (sApr - sQMAveragePrimeOfferR > 1.500M)
                            return E_HighPricedMortgageT.HigherPricedQm;
                        else
                            return E_HighPricedMortgageT.None;
                    }
                    else
                    {
                        if (sApr - sQMAveragePrimeOfferR > 3.500M)
                            return E_HighPricedMortgageT.HigherPricedQm;
                        else
                            return E_HighPricedMortgageT.None;
                    }
                }
            }
            else
            {
                if (Fields.sLienPosT == E_sLienPosT.First && Fields.sIsConformingLAmt)
                {
                    if (sApr - sQMAveragePrimeOfferR > 1.500M)
                        return E_HighPricedMortgageT.HPML;
                    else
                        return E_HighPricedMortgageT.None;
                }
                else if (Fields.sLienPosT == E_sLienPosT.First && !Fields.sIsConformingLAmt)
                {
                    if (sApr - sQMAveragePrimeOfferR > 2.500M)
                        return E_HighPricedMortgageT.HPML;
                    else
                        return E_HighPricedMortgageT.None;
                }
                else
                {
                    if (sApr - sQMAveragePrimeOfferR > 3.500M)
                        return E_HighPricedMortgageT.HPML;
                    else
                        return E_HighPricedMortgageT.None;
                }
            }
        }

        #endregion

    }

}
