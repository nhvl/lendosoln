using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.UI;

namespace DataAccess
{
	public class CMigrationTempData : CPageData
	{

		private static CSelectStatementProvider s_selectProvider;
		private static string s_pageNm;
		static CMigrationTempData() 
		{
				
		}

		public static void AcceptNewFieldTarget( StringList targetedFields, string pageName )
		{
			s_pageNm = pageName;
			s_selectProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, targetedFields.Collection);

		}
		public CMigrationTempData(Guid fileId) : base(fileId, s_pageNm, s_selectProvider) 
		{
		}
	}
}