﻿namespace DataAccess
{
    using System;

    using LendersOffice.Constants;

    /// <summary>
    /// Stub class to be fleshed out in the future.
    /// </summary>
    public class TestLoanFileEnvironment
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="TestLoanFileEnvironment" /> class from being created.
        /// </summary>
        private TestLoanFileEnvironment()
        {
        }

        /// <summary>
        /// Gets the TestLoanFileEnvironment Id.
        /// </summary>
        /// <value>The TestLoanFileEnvironment Id.</value>
        public Guid Id { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not Fee Service Testing is enabled.
        /// </summary>
        /// <value>Whether or not Fee Service Testing is enabled.</value>
        public bool EnableFeeServiceTest { get; set; }

        /// <summary>
        /// Stub getter. Only works with hardcoded Fee Service Id.
        /// Will be fleshed out once we figure out how these objects will be stored/represented.
        /// </summary>
        /// <param name="testLoanFileEnvironmentId">The Id used to retrieve the test loan file environment.</param>
        /// <returns>A TestLoanFileEnvironment Object.</returns>
        public static TestLoanFileEnvironment GetTestLoanFileEnvironment(Guid testLoanFileEnvironmentId)
        {
            TestLoanFileEnvironment ret = new TestLoanFileEnvironment();
            if (testLoanFileEnvironmentId == ConstApp.TestFeeServiceEnvironmentId)
            {
                ret.Id = testLoanFileEnvironmentId;
                ret.EnableFeeServiceTest = true;
            }

            return ret;
        }
    }
}
