﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.ClosingCostAutomation
{

    public class ClosingCostAuditConflict
    {
        private List<Guid> m_templateIds; 

        public string FieldId { get; private set; }
        public string Value { get; private set; }


        public ClosingCostAuditConflict(string fieldId, string value)
        {
            FieldId = fieldId;
            Value = value;
            m_templateIds = new List<Guid>();
        }

        public void AddTemplate(Guid id)
        {
            m_templateIds.Add(id);
        }
    }

    public sealed class ClosingCostAutomationAuditor
    {
        
        private TemplateCondition[] m_conditions; 

        public ClosingCostAutomationAuditor(BrokerAutomationFields fields)
        {
            m_conditions = fields.GetAllTemplateConditions().ToArray();

        }

        public IEnumerable<TemplateCondition> GetAllTemplates()
        {
            return m_conditions;
        }


    }
}
