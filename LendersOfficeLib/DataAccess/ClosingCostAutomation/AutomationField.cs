﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.ClosingCostAutomation
{

    public enum AutomationFieldType
    {
        Enumeration,
        Range,

        /// <summary>
        /// An enumerated field where the values can be changed by the broker.
        /// Values in fee service file are expected to be of format 
        /// {Friendly Value} : {ID}. For example, Call-in : 2
        /// </summary>
        BrokerDefinedEnumeration,

        /// <summary>
        /// An enumerated field where the values at the top layer are each linked
        /// to a sub set of selectable values. 
        /// Values in fee service file are expected to be of format 
        /// {Superset Value} : {Subset Values}. For example, RegionSetA : Region1, Region2,...
        /// </summary>
        NestedEnumeration
    }

    /// <summary>
    /// POCO object used to represent supported closing cost fields.
    /// </summary>
    public class AutomationField
    {
        public string FieldId { get; internal set; }
        public string Name { get; internal set; }
        public AutomationFieldType Type { get; internal set; }
    }

    /// <summary>
    /// POCO object used to keep track of the selected values for a given field
    /// </summary>
    public sealed class AutomationFieldValue
    {
        public string Rep { get; set; }
        public string Value { get; set; }
        public bool IsSelected { get; set; }
    }

    /// <summary>
    /// POCO object used to specifyu which fields are in use by the page
    /// </summary>
    public sealed class SelectedAutomationField : AutomationField
    {
        public IEnumerable<AutomationFieldValue> Values { get; set; }
    }

    /// <summary>
    /// The bare
    /// </summary>
    public sealed class BareboneSelectedAutomationValues
    {
        public string FieldId { get; set; }
        public string[] Values { get; set; }
    }

    public class CCTemplateDetails
    {
        public string Name { get;  set; }
        public Guid Id { get;  set; }
        public E_GfeVersion Version { get; set; }
        public int? CCSystemId { get; set; }

        public string TemplateType
        {
            get
            {
                //todo why the version?
                return CCSystemId.HasValue ? "Automated" : "Manual - GFE 2010";  
            }
        }
    }
}
