﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.Xml.Schema;

namespace DataAccess.ClosingCostAutomation
{
    internal sealed class SystemRuleXml : IXmlSerializable
    {
        /// <summary>
        /// Keep a list of all fields used in the individual template rules 
        /// </summary>
        private HashSet<String> m_sortedAutomationFields;

        /// <summary>
        /// A mapping between template id and its rules
        /// </summary>
        private Dictionary<Guid, TemplateCondition> m_templateRules;

        /// <summary>
        /// Creates a new SystemRuleXml 
        /// </summary>
        internal SystemRuleXml()
        {
            m_sortedAutomationFields = new HashSet<string>();
            m_templateRules = new Dictionary<Guid, TemplateCondition>();
        }

        internal bool HasTemplate(Guid id)
        {
            return m_templateRules.ContainsKey(id);
        }

        internal void DeleteTemplate(Guid id)
        {
            m_templateRules.Remove(id);
        }

        internal void UpdateTemplateId(Guid srcId, Guid dstId)
        {
            TemplateCondition condition;
            if (m_templateRules.TryGetValue(srcId, out condition))
            {
                m_templateRules.Remove(srcId);
                condition.TemplateId = dstId;
                m_templateRules.Add(dstId, condition);
            }
        }

        internal IEnumerable<string> GetUsedFieldIds()
        {
            foreach (string id in m_sortedAutomationFields)
            {
                yield return id;
            }
        }

        internal IEnumerable<TemplateCondition> GetAllTemplates()
        {
            return m_templateRules.Values; 
        }

        /// <summary>
        /// Resets the fields used in the system xml. Since the individual templates are listed in 
        /// the order specified here the order matters. When this method is called each template is checked
        /// ensuring that the fields in its rules are in the passed in set.
        /// </summary>
        /// <param name="fields"></param>
        internal void SetAutomationFieldsWithListingOrder(IEnumerable<string> fields)
        {
            m_sortedAutomationFields.Clear();
            m_sortedAutomationFields.UnionWith(fields);

            foreach (TemplateCondition template in m_templateRules.Values)
            {
                template.EnsureRuleFieldsAreIn(m_sortedAutomationFields);
            }

            if (m_sortedAutomationFields.Count == 0)
            {
                m_templateRules.Clear();
            }
        }

        public bool HasScenarios()
        {
            return m_sortedAutomationFields.Count > 0 && m_templateRules.Count > 0;
        }


        internal TemplateCondition GetTemplateCondition(Guid id)
        {
            TemplateCondition condition;

            if (false == m_templateRules.TryGetValue(id, out condition))
            {
                condition = new TemplateCondition();
                condition.TemplateId = id;
                m_templateRules.Add(id, condition);
            }

            return condition;
        }

        internal void SetTemplateCondition(Guid id, IEnumerable<BareboneSelectedAutomationValues> condition)
        {
            var item = GetTemplateCondition(id);
            item.SetConditionTo(condition);
        }


        #region IXmlSerializable Members


        public XmlSchema GetSchema()
        {
            return null;
        }

        /// <summary>
        /// The minimal XML. 
        /// <SystemRuleXml>
        /// <Fields></Fields>
        /// <Conditions></Conditions>
        /// </SystemRuleXml>
        /// </summary>
        /// <param name="reader"></param>
        public void ReadXml(XmlReader reader)
        {
            reader.MoveToContent();
            reader.ReadStartElement("SystemRuleXml");
            bool isFieldsEmpty = reader.IsEmptyElement;
            reader.ReadStartElement("Fields");

            while (!isFieldsEmpty && reader.NodeType == XmlNodeType.Element)
            {
                string field = reader.ReadElementString("Field");
                m_sortedAutomationFields.Add(field);
            }

            if (!isFieldsEmpty)
            {
                reader.ReadEndElement(); //consume Fields
            }
            bool isConditionEmpty = reader.IsEmptyElement;
            reader.ReadStartElement("ConditionTemplates");

            while (!isConditionEmpty && reader.NodeType == XmlNodeType.Element)
            {
                TemplateCondition condition = new TemplateCondition();
                condition.ReadXml(reader);
                m_templateRules.Add(condition.TemplateId, condition);
            }
            if (!isConditionEmpty)
            {
                reader.ReadEndElement(); //consume conditions
            }
            reader.ReadEndElement(); // consume root

        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("SystemRuleXml");
            writer.WriteStartElement("Fields");
            foreach (string field in m_sortedAutomationFields)
            {
                writer.WriteElementString("Field", field);
            }
            writer.WriteEndElement();
            writer.WriteStartElement("ConditionTemplates");
            foreach (var template in m_templateRules.Values)
            {
                writer.WriteStartElement("ConditionTemplate");
                template.WriteXml(writer);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        #endregion
    }
}
