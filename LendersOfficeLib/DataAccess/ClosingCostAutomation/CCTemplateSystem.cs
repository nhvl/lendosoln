﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml.Schema;
using System.Xml;
using System.IO;
using LendersOffice.Security;
using LendersOffice.Common;
using LendersOffice.ObjLib.TitleProvider;

namespace DataAccess.ClosingCostAutomation
{
    /// <summary>
    /// Represents a template system. Has all loading methods. 
    /// </summary>
    public sealed class CCTemplateSystem
    {
        public class CCcMatchingTemplateData
        {
            public Guid sLId { get; set; }
            public E_sLPurposeT sLPurposeT { get; set; }
            public E_sFinMethT sFinMethT { get; set; }
            public E_sLienPosT sLienPosT { get; set; }
            public E_aOccT aOccT { get; set; }
            public string sSpState { get; set; }
            public E_sLT sLT { get; set; }
            public E_BranchChannelT sBranchChannelT { get; set; }
            public string lLpProductType { get; set; }
            public string lLpInvestorNm { get; set; }
            public Guid sBranchId { get; set; }
            public E_sProdSpT sProdSpT { get; set; }
        }

        private QuoteRequestData m_loanData; 
        private Dictionary<Guid, CCcTemplateData> m_templateCache; 
        private string m_ruleXml;
        private List<CCcTemplateInfo> m_templates; 
        private BrokerAutomationFields m_fieldCache;
        private bool m_ApplyClosingCost;
        private PricingQuotes m_quotes; 

        public int Id { get; private set; }
        public Guid BrokerId { get; private set; }
        public string Description { get; private set; }
        public DateTime? DraftOpenedD { get; private set; }
        public Guid DraftOpenedByUserId { get; private set; }
        public DateTime? ReleasedD { get; private set; }
        public Guid? ReleasedByUserId { get; private set; }
        public string ReleasedByUserFullName { get; private set; }
        public string DraftOpenedByUserNm { get; private set; }

        public BrokerAutomationFields TemplateConditions
        {
            get
            {

                SystemRuleXml xml = new SystemRuleXml();
                if (!string.IsNullOrEmpty(m_ruleXml))
                {
                    using (StringReader sr = new StringReader(m_ruleXml))
                    using (XmlReader reader = XmlReader.Create(sr))
                    {
                        xml.ReadXml(reader);
                    }
                }

                return new BrokerAutomationFields(BrokerId, xml, Id);
            }
        }


        public IEnumerable<CCcTemplateInfo> Templates
        {
            get { return m_templates; }
        }

        internal CCTemplateSystem(DbDataReader reader, QuoteRequestData data)
        {
            Id = (int)reader["Id"];
            BrokerId = (Guid)reader["Brokerid"];
            Description = (String)reader["Description"];
            DraftOpenedD = (DateTime)reader["DraftOpenedD"];
            DraftOpenedByUserId = (Guid)reader["DraftOpenedByUserId"];

            if (reader["ReleasedD"] != DBNull.Value)
            {
                ReleasedD = (DateTime)reader["ReleasedD"];
                ReleasedByUserId = (Guid)reader["ReleasedByUserId"];
            }
            m_ruleXml = (string)reader["RuleXml"];

            //todo throw exception when code tries to read these fields but they are not loaded?
            if (reader.NextResult())
            {
                reader.Read();
                ReleasedByUserFullName = (string)reader["Name"];

                reader.NextResult();
                m_templates = new List<CCcTemplateInfo>();

                while (reader.Read())
                {
                    m_templates.Add(new CCcTemplateInfo(reader));
                }

                if (reader.NextResult())
                {
                    reader.Read();
                    DraftOpenedByUserNm = (string)reader["Name"];
                }
            }
            m_loanData = data;
            if (data != null)
            {
                m_templateCache = new Dictionary<Guid, CCcTemplateData>();

                Dictionary<int, QuoteRequestOptions> requestOptionsByVendorId = new Dictionary<int, QuoteRequestOptions>();
                if (PrincipalFactory.CurrentPrincipal.BrokerDB.ClosingCostTitleVendorId.HasValue)
                {
                    QuoteRequestOptions options = new QuoteRequestOptions()
                    {
                        RequestTitle = true,
                        RequestClosing = !PrincipalFactory.CurrentPrincipal.BrokerDB.TitleInterfaceTitleRecordingOnly(data.sSpState),
                        RequestRecording = true
                    };

                    requestOptionsByVendorId.Add(PrincipalFactory.CurrentPrincipal.BrokerDB.ClosingCostTitleVendorId.Value, options);
                }

                m_quotes = TitleProvider.GetQuotesForPricing(PrincipalFactory.CurrentPrincipal.BrokerId,data, requestOptionsByVendorId);
                m_ApplyClosingCost = true;
                try
                {
                    m_fieldCache = TemplateConditions;
                }
                catch (NullReferenceException e)
                {
                    m_fieldCache = null;
                    Tools.LogError(e);
                }
            }
            else
            {
                m_ApplyClosingCost = false;
                m_fieldCache = null;
                m_quotes = null;
                m_templateCache = new Dictionary<Guid, CCcTemplateData>();
            }
        }

        public bool Release(AbstractUserPrincipal principal)
        {

            var rules = TemplateConditions;


            var names = (from a in Templates
                         select new { a.Name, a.Id }).ToDictionary(p => p.Id, y => y.Name);

            if (!rules.PerformAudit(names))
            {
                return false;
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", BrokerId),
                                            new SqlParameter("@DraftId", Id),
                                            new SqlParameter("@UserId",  principal.UserId )
                                        };

            int rowCount = (int)StoredProcedureHelper.ExecuteScalar(BrokerId, "CC_TEMPLATE_ReleaseDraft", parameters);

            if (rowCount != 1)
            {
                throw CBaseException.GenericException("Could not release draft.");
            }

            return true;
        }

        internal string GetConditionForTemplate(Guid templateId)
        {
            foreach (TemplateCondition condition in m_fieldCache.GetAllTemplateConditions())
            {
                if (condition.TemplateId == templateId)
                {
                    return condition.ToXml();
                }
            }
            
            return null;
        }

        internal CCcTemplateData GetMatchingTemplate(CCcMatchingTemplateData data, out TemplateCondition hitCondition)
        {
            hitCondition = null;

            if (m_fieldCache == null)
            {
                return null;
            }


            bool hit = true;

            foreach (TemplateCondition condition in m_fieldCache.GetAllTemplateConditions())
            {
                hit = EvaluateCondition(condition, data);

                if (hit)
                {
                    hitCondition = condition;

                    if (!m_templateCache.ContainsKey(condition.TemplateId))
                    {
                        m_templateCache.Add(condition.TemplateId,
                            CCcTemplateData.RetrieveClosingCostTemplateById(PrincipalFactory.CurrentPrincipal, condition.TemplateId));
                    }

                    CCcTemplateData template = m_templateCache[condition.TemplateId];

                    if (m_ApplyClosingCost && m_quotes != null && !m_quotes.HasError)
                    {
                        foreach (QuoteResultData quoteResult in m_quotes.Quotes)
                        {
                            template.SetPricingQuotes(quoteResult);
                        }
                    }
                    else
                    {
                        template.SetPricingQuotes(null); //clear out 
                    }
                    return template;
                }
            }

            return null;
        }

        internal bool EvaluateCondition(TemplateCondition condition, CCcMatchingTemplateData data)
        {
            bool hit = true;
            foreach (string field in condition.GetFieldIds())
            {
                HashSet<string> values = condition.GetFieldValuesUsed(field);
                switch (field)
                {
                    case "sLPurposeT":
                        hit &= values.Contains(data.sLPurposeT.ToString("d"));
                        break;
                    case "sFinMethT":
                        hit &= values.Contains(data.sFinMethT.ToString("d"));
                        break;
                    case "sLienPosT":
                        hit &= values.Contains(data.sLienPosT.ToString("d"));
                        break;
                    case "aOccT":
                        hit &= values.Contains(data.aOccT.ToString("d"));
                        break;
                    case "sSpState":
                        hit &= values.Contains(data.sSpState);
                        break;
                    case "sLT":
                        hit &= values.Contains(data.sLT.ToString("d"));
                        break;
                    case "sBranchChannelT":
                        hit &= values.Contains(data.sBranchChannelT.ToString("d"));
                        break;
                    case "csLpProductType":
                        hit &= values.Contains(data.lLpProductType);
                        break;
                    case "csLpInvestorNm":
                        hit &= values.Contains(data.lLpInvestorNm);
                        break;
                    case "csBranchNm":
                        hit &= values.Contains(data.sBranchId.ToString().Replace("-", ""));
                        break;
                    case "sProdSpT":
                        hit &= values.Contains(data.sProdSpT.ToString("d"));
                        break;
                    default:
                        throw new CBaseException(ErrorMessages.Generic, "Unhandled field field");
                }

                if (!hit)
                {
                    break;
                }
            }

            return hit;
        }

    }






}
