﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using LendersOffice.Security;
using LendersOffice.ObjLib.TitleProvider;

namespace DataAccess.ClosingCostAutomation
{
    /// <summary>
    /// Provides storage functions for the automation closing cost system
    /// </summary>
    public sealed class ClosingCostAutomationStorage
    {
        private Guid m_brokerId;
        private Guid m_userId;

        public ClosingCostAutomationStorage(Guid brokerId, Guid userId)
        {
            m_userId = userId;
            m_brokerId = brokerId;
        }

        public ClosingCostAutomationStorage(AbstractUserPrincipal principal) 
            : this(principal.BrokerId, principal.UserId)
        {

        }

        public bool HasManualTemplates()
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", m_brokerId)
                                        };
            return (int)StoredProcedureHelper.ExecuteScalar(m_brokerId, "CC_TEMPLATE_ManualTemplateCount", parameters) > 0;
        }

        public CCTemplateSystem GetReleaseVersion()
        {
            return GetReleaseVersionImpl(false, null);
        }
        public CCTemplateSystem GetReleaseVersionAndTemplatesForPricing(QuoteRequestData data)
        {
            return GetReleaseVersionImpl(true, data);
        }
        public CCTemplateSystem GetReleaseVersionAndTemplates()
        {
            return GetReleaseVersionImpl(true, null);
        }

        private CCTemplateSystem GetReleaseVersionImpl(bool pullTemplates, QuoteRequestData data)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", m_brokerId),
                                            new SqlParameter("@IncludeIndividualTemplateInfo", pullTemplates)
                                        };
            CCTemplateSystem system = null;
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(m_brokerId, "CC_TEMPLATE_GetReleaseVersion", parameters))
            {
                if (reader.Read())
                {
                    system = new CCTemplateSystem(reader, data);
                }
            }
            return system;
        }

        public bool HasDraft()
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", m_brokerId)
                                        };
            return (int)StoredProcedureHelper.ExecuteScalar(m_brokerId, "CC_TEMPLATE_DraftCount", parameters) > 0;
        }

        public CCTemplateSystem GetDraftAndTemplates()
        {
            CCTemplateSystem system = null;

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", m_brokerId)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(m_brokerId, "CC_TEMPLATE_GetDraftAndTemplates", parameters))
            {
                if (reader.Read())
                {
                    system = new CCTemplateSystem(reader, null);
                }
            }
            return system;
        }

        public void DeleteDraft()
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", m_brokerId)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(m_brokerId, "CC_TEMPLATE_DeleteDraft", 1, parameters);
        }


        public void CreateEmptyDraft(string description)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", m_brokerId),
                                            new SqlParameter("@UserId", m_userId),
                                            new SqlParameter("@Description", description)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(m_brokerId, "CC_TEMPLATE_CreateEmptyDraft", 1, parameters);
        }

        private int CreateDraftFromImpl(DbDataReader reader)
        {
            List<Guid> srcGuids = new List<Guid>(), dstGuids = new List<Guid>();
            int newId;
            string xml; 
            while (reader.Read())
            {
                srcGuids.Add((Guid)reader["cCcTemplateId"]);
            }

                reader.NextResult();

                while (reader.Read())
                {
                    dstGuids.Add((Guid)reader["cCcTemplateId"]);
                }

                reader.NextResult();
                reader.Read();


                newId = (int)reader["NewId"];
                xml = (string)reader["RuleXml"];
            

            if (string.IsNullOrEmpty(xml))
            {
                return newId; //nothing to do
            }

            SystemRuleXml rules = new SystemRuleXml();
            using (StringReader sr = new StringReader(xml))
            using (XmlReader xmlreader = XmlReader.Create(sr))
            {
                rules.ReadXml(xmlreader);
            }

            for (int x = 0; x < srcGuids.Count; x++)
            {
                rules.UpdateTemplateId(srcGuids[x], dstGuids[x]);
            }

            StringBuilder sb = new StringBuilder();
            using( XmlWriter writer = XmlWriter.Create(sb))
            {
                rules.WriteXml(writer);
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", m_brokerId),
                                            new SqlParameter("@DraftId", newId),
                                            new SqlParameter("@RuleXml", sb.ToString())
                                        };

            StoredProcedureHelper.ExecuteNonQuery(m_brokerId, "CC_TEMPLATE_UpdateXml",3,parameters);

            return newId;
        }


        /// <summary>
        /// Copies the given ccsystem template system as a new draft. If there is a draft it is deleted. 
        /// </summary>
        /// <param name="ccTemplateSystemId">The Id of the system to copy</param>
        public int CreateDraftFrom(string description, int ccTemplateSystemId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@srcSystemId", ccTemplateSystemId),
                                              new SqlParameter("@UserId", m_userId),
                                              new SqlParameter("@BrokerId", m_brokerId),
                                              new SqlParameter("@Description", description)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(m_brokerId, "CC_TEMPLATE_CreateDraftFromExistingSystem",parameters))
            {
                return CreateDraftFromImpl(reader);
            }
        }

        public int CreateDraftFromCurrentRelease(string description)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", m_userId),
                                             new SqlParameter("@BrokerId", m_brokerId),
                                             new SqlParameter("@Description", description)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(m_brokerId, "CC_TEMPLATE_CreateDraftFromExistingSystem",parameters))
            {
                return CreateDraftFromImpl(reader);

            }
        }

        /// <summary>
        /// Creates a new CC Template System using the manual templates. 
        /// </summary>
        /// <param name="createdByUserId"></param>
        public int CreateDraftFromManualTemplates(string description)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", m_userId),
                                              new SqlParameter("@BrokerId", m_brokerId),
                                              new SqlParameter("@Description", description)
                                        };
            return (int)StoredProcedureHelper.ExecuteScalar(m_brokerId, "CC_TEMPLATE_CreateDraftFromManualTemplates", parameters);
        }

        public IEnumerable<AuditEntry> GetAuditEvents()
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", m_brokerId)
                                        };
            List<AuditEntry> auditEntries = new List<AuditEntry>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(m_brokerId, "CC_TEMPLATE_GetAuditHistory",parameters))
            {
                while (reader.Read())
                {
                    auditEntries.Add(new AuditEntry(reader));
                }
            }

            return auditEntries;
        }

        public IEnumerable<CCTemplateSystemListingInfo> GetPreviousReleasesInfo()
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", m_brokerId)
                                        };

            List<CCTemplateSystemListingInfo> templates = new List<CCTemplateSystemListingInfo>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(m_brokerId, "CC_TEMPLATE_GetPreviousReleasesListingInfo", parameters))
            {
                while (reader.Read())
                {
                    templates.Add(new CCTemplateSystemListingInfo(reader));
                }
            }

            return templates;
        }

        public BrokerAutomationFields GetAutomationConditionForDraft(int expectedDraftId)
        {
            BrokerAutomationFields fields = GetAutomationConditionForDraft();

            if (fields.DraftVersion != expectedDraftId)
            {
                throw new CBaseException("Draft version has changed.", "Draft version changed.");
            }

            return fields;
        }

        public BrokerAutomationFields GetAutomationConditionForDraft()
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", m_brokerId)
                                        };
            int currentDraftId = -1;

            string xml = null;
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(m_brokerId, "CC_TEMPLATE_GetDraftRuleXml", parameters))
            {
                
                if (reader.Read())
                {
                    currentDraftId = (int)reader["Id"];
                    xml = (string)reader["RuleXml"];
                }
            }

            if (xml == null)
            {
                throw new CBaseException("No Draft found.", String.Format("No Draft Found. BrokerId: {0} UserId: {1}", m_brokerId, m_userId) );
            }

            SystemRuleXml ruleXml = new SystemRuleXml();

            if (xml.Length != 0)
            {
                using (StringReader sr = new StringReader(xml))
                using (XmlReader xmlr = XmlReader.Create(sr))
                {
                    ruleXml.ReadXml(xmlr);
                }
            }

            BrokerAutomationFields br = new BrokerAutomationFields(m_brokerId, ruleXml, currentDraftId);
            return br;
        }

        /// <summary>
        /// Returns true if the save was successful
        /// </summary>
        /// <param name="m_brokerId"></param>
        /// <param name="expectedDraftId"></param>
        /// <param name="rule"></param>
        /// <returns></returns>
        public bool SaveDraftAutomationConditions(BrokerAutomationFields automationCondition)
        {

            StringBuilder sb = new StringBuilder();
            using (XmlWriter writer = XmlWriter.Create(sb))
            {
                automationCondition.WorkingSystemRuleXml.WriteXml(writer);
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", m_brokerId),
              new SqlParameter("@Id", automationCondition.DraftVersion),
              new SqlParameter("@Xml", sb.ToString())
                                        };
            int result = (int)StoredProcedureHelper.ExecuteScalar(m_brokerId, "CC_TEMPLATE_SaveDraftXML", parameters);

            return result > 0;

        }

        public IEnumerable<CCTemplateDetails> GetAllTemplateDetails()
        {
            List<CCTemplateDetails> details = new List<CCTemplateDetails>();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", m_brokerId)
                                        };

            using(DbDataReader reader = StoredProcedureHelper.ExecuteReader(m_brokerId, "CC_TEMPLATE_GetTemplateDetails", parameters))
            {
                while (reader.Read())
                {
                    int? systemId = null;

                    if( reader["CCTemplateSystemId"] != DBNull.Value )
                    {
                        systemId = (int) reader["CCTemplateSystemId"];
                    }

                    var ccTemplate = new CCTemplateDetails {CCSystemId = systemId};
                    var broker = LendersOffice.Admin.BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);

                    //if automated closing cost templates are disabled, don't return the automated cc templates
                    if (broker.AutomatedClosingCost || ccTemplate.TemplateType != "Automated")
                    {
                        details.Add(new CCTemplateDetails
                        {
                            CCSystemId = systemId,
                            Name = (string)reader["cCcTemplateNm"],
                            Version = (E_GfeVersion)reader["GfeVersion"],
                            Id = (Guid)reader["cCcTemplateId"]
                        });
                    }
                }
            }

            return details.OrderBy(p => p.TemplateType).ThenBy(p => p.Name);
        }

        public bool DeleteAutomatedDraftTemplate(Guid templateId, int draftId)
        {
            BrokerAutomationFields fields = GetAutomationConditionForDraft(draftId);

            if (fields.DeleteTemplate(templateId))
            {
                SaveDraftAutomationConditions(fields);                
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", m_brokerId),
                                            new SqlParameter("@TemplateId", templateId)
                                        };

            return (int)StoredProcedureHelper.ExecuteScalar(m_brokerId, "CC_TEMPLATE_DeleteFromDraft", parameters) ==1;
        }
    }
}
