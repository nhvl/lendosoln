﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;

namespace DataAccess.ClosingCostAutomation
{
    public class AuditEntry
    {
        public string Description { get; private set; }
        public DateTime ReleasedD { get; private set; }
        public DateTime DraftOpenedD { get; private set; }
        public string DraftUserNm { get; private set; }
        public string ReleaseUserNm { get; private set; }

        internal AuditEntry(DbDataReader reader)
        {
            Description = (String)reader["Description"];
            DraftOpenedD = (DateTime)reader["DraftOpenedD"];
            ReleasedD = (DateTime)reader["ReleasedD"];
            DraftUserNm = (string)reader["DraftUser"];
            ReleaseUserNm = (string)reader["ReleaseUser"];
        }


    }
}
