﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.Xml.Schema;
using System.IO;

namespace DataAccess.ClosingCostAutomation
{
    /// <summary>
    /// A template condition is made up of a template id corresponding toa  closing cost template. 
    /// A list of fields and the valeus a field can be for the template to apply.
    /// </summary>
    [XmlRoot(ElementName = "ConditionTemplate")]
    public sealed class TemplateCondition : IXmlSerializable
    {
        private Dictionary<string, HashSet<string>> m_condition;
        internal Guid TemplateId { get; set; }
        public bool IsNew { get; private set; }
        private static XmlSerializer x_xmlSerializer = new XmlSerializer(typeof(TemplateCondition));

        internal TemplateCondition()
        {
            m_condition = new Dictionary<string, HashSet<string>>(StringComparer.OrdinalIgnoreCase);
            IsNew = true;
        }

        /// <summary>
        /// Returns the field ids that make up this condition template.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetFieldIds()
        {
            return m_condition.Keys;
        }

        public int ConditionCount
        {
            get { return m_condition.Count; }
        }

        /// <summary>
        /// Removes any conditions whose fields are not in passed in fields collection.
        /// </summary>
        /// <param name="fieldSet"></param>
        internal void EnsureRuleFieldsAreIn(HashSet<string> fieldSet)
        {
            if (fieldSet.Count() == 0)
            {
                m_condition.Clear();
            }

            foreach (string field in m_condition.Keys.ToArray())
            {
                if (false == fieldSet.Contains(field))
                {
                    m_condition.Remove(field);
                }
            }
        }


        /// <summary>
        /// Returns the values that apply to this template condition.
        /// </summary>
        /// <param name="fieldid"></param>
        /// <returns></returns>
        public HashSet<string> GetFieldValuesUsed(string fieldid)
        {
            HashSet<string> values = null;
            m_condition.TryGetValue(fieldid, out values);
            return values;
        }


        public IEnumerable<KeyValuePair<string, HashSet<string>>> GetAllConditions()
        {
            return m_condition;
        }

        /// <summary>
        /// Rebuilds the condition for the template. 
        /// </summary>
        /// <param name="fieldAndValues">A collection of fields and the values those fields can be </param>
        internal void SetConditionTo(IEnumerable<BareboneSelectedAutomationValues> fieldAndValues)
        {
            m_condition.Clear();
            foreach (var fieldValue in fieldAndValues)
            {
                m_condition.Add(fieldValue.FieldId, new HashSet<string>(fieldValue.Values));
            }
        }


        #region IXmlSerializable Members

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            IsNew = false;
            TemplateId = new Guid(reader.GetAttribute("TemplateId"));
            bool isEmptyElement = reader.IsEmptyElement;
            reader.ReadStartElement("ConditionTemplate"); //read wrapper
            if (isEmptyElement)
            {
                return; 
            }
            while (reader.NodeType == XmlNodeType.Element && reader.Name == "Field")
            {
                string id = reader.GetAttribute("Id");
                reader.ReadStartElement("Field");
                HashSet<string> values = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
                while (reader.NodeType == XmlNodeType.Element)
                {
                    values.Add(reader.ReadElementString("Value"));
                }
                m_condition.Add(id, values);
                reader.ReadEndElement();
            }
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("TemplateId", TemplateId.ToString());
            foreach (string field in m_condition.Keys)
            {
                writer.WriteStartElement("Field");
                writer.WriteAttributeString("Id", field);

                if (m_condition[field].Count == 0)
                {
                    throw CBaseException.GenericException("Dev error sould not save field but no values.");
                }
                foreach (string value in m_condition[field])
                {
                    writer.WriteStartElement("Value");
                    writer.WriteString(value);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
        }

        #endregion

        public string ToXml()
        {
            string xml;
            using (var memoryStream = new MemoryStream())
            {
                x_xmlSerializer.Serialize(memoryStream, this);
                xml = Encoding.ASCII.GetString(memoryStream.ToArray());
            }

            return xml;
        }

        public static TemplateCondition FromXml(string xml)
        {
            if (string.IsNullOrEmpty(xml))
            {
                return new TemplateCondition();
            }

            TemplateCondition condition;
            using (var memoryStream = new MemoryStream(ASCIIEncoding.ASCII.GetBytes(xml)))
            {
                memoryStream.Position = 0;
                condition = (TemplateCondition)x_xmlSerializer.Deserialize(memoryStream);
            }
            return condition;
        }
    }
}
