﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;

namespace DataAccess.ClosingCostAutomation
{
    /// <summary>
    /// Class with listing details about the template system.
    /// </summary>
    public sealed class CCTemplateSystemListingInfo
    {
        public int Id { get; private set; }
        public string Description { get; private set; }
        public DateTime? ReleasedD { get; private set; }
        public DateTime DraftOpenedD { get; private set; }


        internal CCTemplateSystemListingInfo(DbDataReader reader)
        {
            Id = (int)reader["Id"];
            Description = (String)reader["Description"];
            DraftOpenedD = (DateTime)reader["DraftOpenedD"];

            if (reader["ReleasedD"] != DBNull.Value)
            {
                ReleasedD = (DateTime)reader["ReleasedD"];
            }
        }

        public string Listing
        {
            get
            {
                return string.Format("{0} ({1} - {2})", Description, DraftOpenedD, ReleasedD);
            }
        }
    }
}
