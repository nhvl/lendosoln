﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess.ClosingCostAutomation;
using DataAccess;

namespace DataAccess.ClosingCostAutomation
{
    public class TermplateSelector
    {
        private SystemRuleXml m_xml;

        internal TermplateSelector(SystemRuleXml xml)
        {
            m_xml = xml;
        }

        public Guid GetTemplateIdFor(CPageData pageData)
        {
            Dictionary<string, string> cachedValues = GetUsedFieldValues(pageData);

            foreach (var template in m_xml.GetAllTemplates())
            {
                if (IsMatchingTemplate(cachedValues, template))
                {
                    return template.TemplateId;
                }
            }

            return Guid.Empty;
        }

        private bool IsMatchingTemplate(Dictionary<string, string> data, TemplateCondition condition)
        {
            foreach (var field in condition.GetFieldIds())
            {
                var values = condition.GetFieldValuesUsed(field);
                if (!values.Contains(data[field]))
                {
                    return false;
                }
            }
            return true;
        }

        private Dictionary<string, string> GetUsedFieldValues(CPageData data)
        {
            Dictionary<string,string> items = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

            foreach (var field in m_xml.GetUsedFieldIds())
            {
                var temp = field;
                if (temp.StartsWith("c"))
                {
                    temp = field.Substring(1);
                }

                string value = PageDataUtilities.GetValue(data, data.GetAppData(0), temp);
                items.Add(field, value);
            }

            return items;
        }
    }
}
