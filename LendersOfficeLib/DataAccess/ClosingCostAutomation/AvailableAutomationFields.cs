﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using LendersOffice.Admin;

namespace DataAccess.ClosingCostAutomation
{
    /* The classes here can most likely be combined with the stuff in parameter reporting
     * Im not doing so to save time, since Ill most likely have to make changes. The biggest reason
     * is that i need some of the data for the UI and the templates dont store all possible field values
     * just selected.
     * */

    public class AvailableAutomationFields
    {
        /// <summary>
        /// Dont remove fields from this unless you are certain no system rules use them. All methods in this 
        /// clasee are case insensitve.
        /// </summary>
        private static AutomationField[] x_availableFields = new AutomationField[] 
        {
            //WHEN ADDING NEW AUTOMATION FIELD, ALSO UPDATE CPageBase.PopulateMatchingCCTemplate
            new AutomationField() { FieldId = "sLT", Name = "Loan Type", Type = AutomationFieldType.Enumeration },
            new AutomationField() { FieldId = "sLPurposeT", Name = "Loan Purpose", Type = AutomationFieldType.Enumeration },
            new AutomationField() { FieldId = "sFinMethT", Name = "Amortization Type", Type = AutomationFieldType.Enumeration },
            new AutomationField() { FieldId = "sLienPosT", Name = "Lien Position", Type = AutomationFieldType.Enumeration },
            new AutomationField() { FieldId = "aOccT", Name = "Subject Property Purpose", Type = AutomationFieldType.Enumeration },
            new AutomationField() { FieldId = "sSpState", Name = "Subject Property State", Type = AutomationFieldType.Enumeration },
            new AutomationField() { FieldId = "csLpProductType", Name = "Loan Product Type", Type = AutomationFieldType.Enumeration },
            new AutomationField() { FieldId = "csLpInvestorNm", Name = "Investor Name", Type = AutomationFieldType.Enumeration },
            new AutomationField() { FieldId = "csBranchNm", Name = "Branch", Type = AutomationFieldType.Enumeration }, 
            new AutomationField() { FieldId = "sBranchChannelT", Name ="Loan Channel", Type = AutomationFieldType.Enumeration },
            new AutomationField() { FieldId = "sProdSpT", Name = "Property Type", Type = AutomationFieldType.Enumeration } //OPM 120166
            //missing loan channel todo
        };

        /// <summary>
        /// These will be available for the fee service, but not cc templates.
        /// </summary>
        private static AutomationField[] x_feeServiceFields = new AutomationField[]
        {
            new AutomationField() { FieldId = "sLAmt", Name = "Loan Amount", Type = AutomationFieldType.Range },
            new AutomationField() { FieldId = "sApprVal", Name = "Appraised Value", Type = AutomationFieldType.Range },
            new AutomationField() { FieldId = "sPurchPrice", Name = "Purchase Price", Type = AutomationFieldType.Range },
            new AutomationField() { FieldId = "sFinalLAmt", Name = "Total Loan Amount", Type = AutomationFieldType.Range },
            new AutomationField() { FieldId = "sProdImpound", Name = "Escrow/Impound", Type = AutomationFieldType.Enumeration },
            new AutomationField() { FieldId = "sLenderFeeBuyoutRequestedT", Name="Lender Fee Buyout Requested", Type = AutomationFieldType.Enumeration },
            new AutomationField() { FieldId = "csLeadSrcId", Name="Lead Source", Type = AutomationFieldType.BrokerDefinedEnumeration },
            new AutomationField() { FieldId = "sCorrespondentProcessT", Name="Correspondent Process Type", Type = AutomationFieldType.Enumeration },
            new AutomationField() { FieldId = "csPmlCompanyTierId", Name="Originating Company Tier", Type = AutomationFieldType.BrokerDefinedEnumeration },
            new AutomationField() { FieldId = "sDisclosureRegulationT", Name="Disclosure Regulation Type", Type = AutomationFieldType.Enumeration },
            new AutomationField() { FieldId = "csRegion", Name = "Region", Type = AutomationFieldType.NestedEnumeration },
            new AutomationField() { FieldId = "sIsNewConstruction", Name="New Construction", Type = AutomationFieldType.Enumeration },
        };

        /// <summary>
        /// Create without the expanded fee service fields.
        /// </summary>
        public AvailableAutomationFields()
        {
        }

        public AvailableAutomationFields(bool includeFeeServiceFields, Guid brokerId)
        {
            BrokerDB broker = BrokerDB.RetrieveById(brokerId);

            m_includeFeeServiceFields = includeFeeServiceFields;
            m_includeLenderBuyOut = includeFeeServiceFields && broker.EnableLenderFeeBuyout;
            m_includeDisclosureRegulation = includeFeeServiceFields && broker.IsEnableNewFeeService;
        }

        /// <summary>
        /// Includes lender buyout field when getting the list of available fields. Controlled by temp bit.
        /// </summary>
        private bool m_includeLenderBuyOut;

        /// <summary>
        /// Includes disclosure regulation field when getting the list of available fields. Controlled by temp bit.
        /// </summary>
        private bool m_includeDisclosureRegulation;

        /// <summary>
        /// Used for lookup caching.
        /// </summary>
        private Dictionary<string, AutomationField> m_mapping = null;

        /// <summary>
        /// Map from friendly name -> AutomationField.
        /// </summary>
        private Dictionary<string, AutomationField> m_automationFieldsByName = null;

        /// <summary>
        /// Indicates whether to include fee service specific fields.
        /// </summary>
        private bool m_includeFeeServiceFields = false;

        /// <summary>
        /// Returns an enumerable set of available fields.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<AutomationField> GetFields()
        {
            foreach (AutomationField field in x_availableFields)
            {
                yield return field;
            }

            if (m_includeFeeServiceFields)
            {
                foreach (var field in x_feeServiceFields)
                {
                    if (field.FieldId.Equals("sLenderFeeBuyoutRequestedT", StringComparison.OrdinalIgnoreCase) && !m_includeLenderBuyOut)
                    {
                        continue; 
                    }

                    if (field.FieldId.Equals("sDisclosureRegulationT", StringComparison.OrdinalIgnoreCase) && !m_includeDisclosureRegulation)
                    {
                        continue;
                    }

                    yield return field;
                }
            }
        }

        /// <summary>
        /// Returns the rep (friendly name) of the auttomation field given the id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public AutomationField GetRepOfaAutomationField(string id)
        {
            if (m_mapping == null)
            {
                m_mapping = GetFields().ToDictionary(p => p.FieldId, StringComparer.OrdinalIgnoreCase);
            }

            AutomationField rep;

            if (m_mapping.TryGetValue(id, out rep))
            {
                return rep;
            }

            throw CBaseException.GenericException("Missing field from automation list, this should not happen.");
        }

         /// <summary>
         /// Get an automation field by it's friendly name.
         /// </summary>
         /// <param name="name">Field name.</param>
         /// <returns>Automation field if found. Otherwise, null.</returns>
        public AutomationField GetAutomationFieldByName(string name)
        {
            if (m_automationFieldsByName == null)
            {
                m_automationFieldsByName = GetFields().ToDictionary(p => p.Name, StringComparer.OrdinalIgnoreCase);
            }

            AutomationField field = null;
            m_automationFieldsByName.TryGetValue(name, out field);
            return field;
        }
    }
}
