﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonProjectLib.Common.Lib;
using LendersOffice.Common;
using System.Data;
using LendersOffice.Security;

namespace DataAccess.ClosingCostAutomation
{
    public sealed class BrokerAutomationFields
    {
        private AvailableAutomationFields m_availableAutomationFields;
        private SystemRuleXml m_brokerRuleXml;
        private ParameterReporting m_parameterReporting;
        private Dictionary<string, Guid> m_templateByScenario;
        private Dictionary<string, List<Guid>> m_conflicts ;
        private List<string[]> m_missingScenarios;
        private bool m_ranAudit;
        private Dictionary<string, SecurityParameter.EnumeratedParmeter> m_cachedFieldValues;
        private Dictionary<string, Dictionary<string, string>> m_fieldValues;
        private Dictionary<Guid, string> m_draftTemplateNames;
        private bool m_auditSucessful;
        private List<string> m_templateNamesWithNoScenarios;

        public bool CanPerformAudit
        {
            get;
            private set;
        }

        /// <summary>
        /// These fields are in the xml but do not show up in any condition.
        /// </summary>
        private List<string> m_UnusedFieldsInXml;
        private List<string> m_usedFieldsInXml; 

        public int DraftVersion { get; private set; }

        internal SystemRuleXml WorkingSystemRuleXml 
        { 
            get { return m_brokerRuleXml; } 
        }

        internal BrokerAutomationFields(Guid brokerId, SystemRuleXml ruleXml, int draftVersion)
        {
            m_availableAutomationFields = new AvailableAutomationFields();
            m_parameterReporting = new ParameterReporting(brokerId);
            m_cachedFieldValues = new Dictionary<string, SecurityParameter.EnumeratedParmeter>(StringComparer.OrdinalIgnoreCase);
            m_brokerRuleXml = ruleXml;
            DraftVersion = draftVersion;
            m_ranAudit = false;
            m_templateByScenario = new Dictionary<string, Guid>(StringComparer.OrdinalIgnoreCase);
            m_conflicts = new Dictionary<string, List<Guid>>(StringComparer.OrdinalIgnoreCase);
            m_missingScenarios = new List<string[]>();
            m_fieldValues = new Dictionary<string, Dictionary<string, string>>();
            CanPerformAudit = ruleXml != null && ruleXml.HasScenarios(); 
        }

        /// <summary>
        /// Returns the list of autoamtion fields used by the broker.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<AutomationField> GetAutomationFieldsUsed()
        {
            foreach (var fieldId in m_brokerRuleXml.GetUsedFieldIds())
            {
                yield return m_availableAutomationFields.GetRepOfaAutomationField(fieldId);
            }
        }
        #region audit stuff... should be in its own class. running out of time though

        /// <summary>
        /// Expensive process. Basically generates all possible combinations for each template 
        /// and all values used for each variable to perform audit. 
        /// Required Conditions: Every template for the system has a scenario 
        ///                      There are no conflicts for a given scenario 
        ///                      Every field is used in at least one template.
        /// </summary>
        /// <param name="masterTemplateList"></param>
        public bool PerformAudit(Dictionary<Guid, string> masterTemplateList)
        {
            if (m_ranAudit)
            {
                return m_auditSucessful;//nothing left to do
            }

            m_draftTemplateNames = masterTemplateList;

            using (PerformanceStopwatch.Start("BrokerAutomationField.RunAudit"))
            {
                //todo what happens if one of the fields is not used at all ? Need to remove it. 
                m_templateByScenario = new Dictionary<string, Guid>();
                m_missingScenarios = new List<string[]>();
                m_UnusedFieldsInXml = new List<string>();
                m_templateNamesWithNoScenarios = new List<string>();
                m_usedFieldsInXml = new List<string>();

                Dictionary<string, HashSet<string>> valueMappings = new Dictionary<string, HashSet<string>>(StringComparer.OrdinalIgnoreCase);

                foreach (string field in m_brokerRuleXml.GetUsedFieldIds())
                {
                    valueMappings.Add(field, new HashSet<string>(StringComparer.OrdinalIgnoreCase));
                }
                List<TemplateCondition> allTemplateConditions = new List<TemplateCondition>(GetAllTemplateConditions());
                if (valueMappings.Count == 0 || allTemplateConditions.Count == 0)
                {
                    m_auditSucessful = false;
                    m_ranAudit = true;
                    return false;
                }

                //since we dont want to generate every possible combination we find all the values the user used in the templates. 
                //if the user doesnt use the values in ANY of the templates we ignore them. 
                foreach (TemplateCondition condition in allTemplateConditions)
                {
                    //Should not happen basically, there are templates in the xml that are not part of the draft
                    if (masterTemplateList.ContainsKey(condition.TemplateId) == false)
                    {
                        throw new AccessDenied();
                    }

                    foreach (var item in condition.GetAllConditions())
                    {
                        valueMappings[item.Key].UnionWith(item.Value);
                    }
                }


                //keep track of the unused fields
                foreach (AutomationField field in GetAutomationFieldsUsed())
                {
                    if (valueMappings[field.FieldId].Count == 0)
                    {
                        m_UnusedFieldsInXml.Add(field.FieldId);
                    }
                    else
                    {
                        m_usedFieldsInXml.Add(field.FieldId);
                    }
                }

                //remove the unuse field. We will conduct the audit without it 
                foreach (string fieldId in m_UnusedFieldsInXml)
                {
                    valueMappings.Remove(fieldId);
                }

                //generate all the combination of values for each template  and track them. 
                //we need this set for 2 reasons to cache the result AND to find collissions between templates
                foreach (TemplateCondition condition in allTemplateConditions)
                {
                    HashSet<string>[] valuesUsedByCondition = new HashSet<string>[valueMappings.Count];
                    int i = 0;
                    foreach (var mapping in valueMappings)
                    {
                        //since a condition xml may have a missing field when All is selected need to include all values for given field. 
                        //in the end none of the values used to generate the combinations can be 0
                        valuesUsedByCondition[i] = condition.GetFieldValuesUsed(mapping.Key) ?? mapping.Value;
                        i++;
                    }

                    GenerateCombinations(valuesUsedByCondition, p => TrackScenarioForCollisions(p, condition.TemplateId));
                }

                //finally we need to generate the combinations for the used values and keep track of any that didnt show up in our list.
                GenerateCombinations(valueMappings.Values.ToArray(), p => VerifyScenarioExist(p));


                var xmlTemplateIds = from a in allTemplateConditions
                                     select a.TemplateId;

                HashSet<Guid> knownTemplates = new HashSet<Guid>(xmlTemplateIds);
                foreach (var templateInfo in m_draftTemplateNames)
                {
                    if (!knownTemplates.Contains(templateInfo.Key))
                    {
                        m_templateNamesWithNoScenarios.Add(templateInfo.Value);
                    }
                }
            }

            m_ranAudit = true;
            m_auditSucessful = m_conflicts.Count == 0 && m_UnusedFieldsInXml.Count == 0 && m_templateNamesWithNoScenarios.Count == 0;
            return m_auditSucessful;
        }

        /// <summary>
        /// Returns the list of fields the xml has but were not use in the templates.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetUnusedFields()
        {
            return from a in m_UnusedFieldsInXml
                   select GetFriendlyName(a);
        }

        /// <summary>
        /// Generates a table representing the scenarios a template covers. This only shows templates with 
        /// scenarios/conditions.
        /// </summary>
        /// <returns></returns>
        public DataTable GenerateSummaryTable()
        {
            if (!m_ranAudit)
            {
                throw CBaseException.GenericException("Dev error: need to call audit first.");
            }
            
            DataTable table = new DataTable();
            DataColumn header = table.Columns.Add("TemplateName", typeof(string));
            header.Caption = "Closing Cost Template Name";

            foreach (string field in m_usedFieldsInXml)
            {
                DataColumn column = table.Columns.Add(field, typeof(string));
                column.Caption = GetFriendlyName(field);
            }

            foreach (var template in m_brokerRuleXml.GetAllTemplates())
            {
                DataRow row = table.NewRow();
                row["TemplateName"] = m_draftTemplateNames[template.TemplateId];

                foreach (string field in m_usedFieldsInXml)
                {
                    var valuesForField  = template.GetFieldValuesUsed(field);
                    if( valuesForField == null )
                    {
                        row[field] = "All";
                        continue;                       
                    }

                    var names = (from a in valuesForField
                                 select GetFriendlyValueDesc(field, a) ).
                                 ToArray();

                    row[field] = String.Join(", ", names);
                }
                table.Rows.Add(row);
            }

            return table;
        }

        /// <summary>
        /// Generates a table with each scenario that has a conflict and the conflicting template names
        /// </summary>
        /// <returns></returns>
        public DataTable GenerateConflictTable()
        {
            if (!m_ranAudit)
            {
                throw CBaseException.GenericException("Dev error: need to call audit first.");
            }

            DataTable table = new DataTable();


            foreach (string field in m_usedFieldsInXml )
            {
                var column = table.Columns.Add(field, typeof(string));
                column.Caption = GetFriendlyName(field);
            }

            var conflict = table.Columns.Add("ConflictingTemplates", typeof(string));
            conflict.Caption = "Conflicting Templates";

            foreach (var entry in m_conflicts)
            {
                string[] scenario = entry.Key.Split('|');
                DataRow row = table.NewRow();

                for (int i = 0; i < table.Columns.Count - 1; i++)
                {
                    row[i] = GetFriendlyValueDesc(table.Columns[i].ColumnName, scenario[i]);
                }

                string names = "\"" + String.Join("\", \"", (from a in entry.Value select m_draftTemplateNames[a]).ToArray()) + "\"";
                row["ConflictingTemplates"] = names;
                table.Rows.Add(row);

            }

            return table;
        }

        /// <summary>
        /// Generates a table with the scenarios that are not covered by any template.
        /// </summary>
        /// <returns></returns>
        public DataTable GenerateMissingTable()
        {
            if (!m_ranAudit)
            {
                throw CBaseException.GenericException("Dev error: need to call audit first.");
            }

            DataTable table = new DataTable();

            foreach (string field in m_usedFieldsInXml)
            {
                var column = table.Columns.Add(field, typeof(string));
                column.Caption = GetFriendlyName(field);
            }


            foreach (string[] scenario in m_missingScenarios)
            {
                DataRow row = table.NewRow();

                for (int i = 0; i < scenario.Length; i++)
                {
                    string value = scenario[i];
                    row[i] = GetFriendlyValueDesc(table.Columns[i].ColumnName,value);
                }
                table.Rows.Add(row);

            }

            return table;
        }

        /// <summary>
        /// Returns the list of templates that do not have a condition.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GenerateListOfTemplatesWithNoConditions()
        {
            if (!m_ranAudit)
            {
                throw CBaseException.GenericException("Dev error: need to call audit first.");
            }

            return m_templateNamesWithNoScenarios;
        }

        private void VerifyScenarioExist(string[] scenario)
        {
            string scenarioRep = String.Join("|", scenario);

            if (!m_templateByScenario.ContainsKey(scenarioRep))
            {
                string[] dst = new string[scenario.Length];
                Array.Copy(scenario, dst, scenario.Length);
                m_missingScenarios.Add(dst);
            }
        }

        private void TrackScenarioForCollisions(string[] scenario, Guid templateId)
        {
            Guid template;
            string scenarioRep = String.Join("|", scenario);
            if ( m_templateByScenario.TryGetValue(scenarioRep, out template))
            {
                List<Guid> conflictTemplateIds;
                if (false == m_conflicts.TryGetValue(scenarioRep, out conflictTemplateIds))
                {
                    conflictTemplateIds = new List<Guid>();
                    m_conflicts.Add(scenarioRep, conflictTemplateIds);
                    conflictTemplateIds.Add(template);
                }
                conflictTemplateIds.Add(templateId); 
            }
            else
            {
                m_templateByScenario.Add(scenarioRep, templateId);
            }
        }

        private void GenerateCombinations(IEnumerable<string>[] items, Action<string[]> p)
        {
            string[] workingSet = new string[items.Length];
            Recurse(workingSet, 0, items, p);
        }

        private void Recurse<TList>(string[] selected, int index, IEnumerable<TList> remaining, Action<string[]> action) where TList : IEnumerable<string>
        {
            IEnumerable<string> nextList = remaining.FirstOrDefault();
            if (nextList == null)
            {
                action(selected);
            }
            else
            {
                foreach (string i in nextList)
                {
                    selected[index] = i;
                    Recurse(selected, index + 1, remaining.Skip(1), action);
                }
            }
        }

        #endregion

        public int GetNumberOfAutomationFieldsUsed()
        {
            return m_brokerRuleXml.GetUsedFieldIds().Count();
        }

        public IEnumerable<AutomationField> GetAutomationFieldsUnused()
        {
            Dictionary<string, AutomationField> fields = m_availableAutomationFields.GetFields().ToDictionary(p => p.FieldId,
                StringComparer.OrdinalIgnoreCase);

            foreach (var fieldId in m_brokerRuleXml.GetUsedFieldIds())
            {
                fields.Remove(fieldId);
            }

            return fields.Values;
        }

        public IEnumerable<SelectedAutomationField> GetConditionsFor(Guid templateConditionId)
        {
            TemplateCondition condition = m_brokerRuleXml.GetTemplateCondition(templateConditionId);

            foreach (AutomationField automationField in GetAutomationFieldsUsed())
            {
                SelectedAutomationField selectedAutomationField = new SelectedAutomationField()
                {
                    FieldId = automationField.FieldId,
                    Name = automationField.Name,
                    Values = GetValues(automationField.FieldId, condition)
                };

                yield return selectedAutomationField;
            }
        }

        public bool DeleteTemplate(Guid templateId)
        {
            if (m_brokerRuleXml.HasTemplate(templateId))
            {
                m_brokerRuleXml.DeleteTemplate(templateId);
                return true;
            }

            return false;
        }

        public string GetDescriptionFor(Guid conditionId)
        {
            TemplateCondition condition = m_brokerRuleXml.GetTemplateCondition(conditionId);
            if (condition.IsNew || condition.ConditionCount == 0)
            {
                return string.Empty;
            }

            StringBuilder sb = new StringBuilder();
            string fieldSeperator = " and ";
            string valueSeperator = " or ";
            foreach (string field in condition.GetFieldIds())
            {
                sb.Append(m_availableAutomationFields.GetRepOfaAutomationField(field).Name);
                sb.Append(" (");

                foreach (string valueId in condition.GetFieldValuesUsed(field))
                {
                    string valueDesc = GetFriendlyValueDesc(field, valueId); 
                    sb.Append(valueDesc);
                    sb.Append(valueSeperator);
                }

                sb.Length = sb.Length - valueSeperator.Length; //remove the last or
                sb.Append(")");
                sb.Append(fieldSeperator);
            }
            sb.Length = sb.Length - fieldSeperator.Length; //remove the last and

            return sb.ToString();
        }


        public void SetCondition(Guid id, IEnumerable<BareboneSelectedAutomationValues> condition)
        {
            m_brokerRuleXml.SetTemplateCondition(id, condition);
        }

        public void SetAutomationFieldsWithListingOrder(IEnumerable<string> fields)
        {
            m_brokerRuleXml.SetAutomationFieldsWithListingOrder(fields);
        }

        public IEnumerable<TemplateCondition> GetAllTemplateConditions()
        {
            return m_brokerRuleXml.GetAllTemplates();
        }

        

        /// <summary>
        ///  Returns the values for the specified field. It also specifies which apply to the template.
        /// </summary>
        /// <param name="fieldId"></param>
        /// <param name="condition"></param>
        /// <returns></returns>
        private AutomationFieldValue[] GetValues(string fieldId, TemplateCondition condition)
        {
            SecurityParameter.EnumeratedParmeter enumMapping = (SecurityParameter.EnumeratedParmeter)m_parameterReporting[fieldId];
            AutomationFieldValue[] values = new AutomationFieldValue[enumMapping.EnumMapping.Count];
            HashSet<string> selectedValues = condition.GetFieldValuesUsed(fieldId) ?? new HashSet<string>();

            for (int x = 0; x < values.Length; x++)
            {
                values[x] = new AutomationFieldValue()
                {
                    Rep = enumMapping.EnumMapping[x].FriendlyValue,
                    Value = enumMapping.EnumMapping[x].RepValue,
                    IsSelected = selectedValues.Contains(enumMapping.EnumMapping[x].RepValue)
                };
            }

            return values;
        }


        
        private string GetFriendlyName(string id)
        {
            SecurityParameter.EnumeratedParmeter parameter;

            if (m_cachedFieldValues.TryGetValue(id, out parameter))
            {
                return parameter.FriendlyName;
            }

            SecurityParameter.EnumeratedParmeter enumMapping = (SecurityParameter.EnumeratedParmeter)m_parameterReporting[id];
            m_cachedFieldValues.Add(id, enumMapping);

            Dictionary<string, string> values = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            foreach (var entry in enumMapping.EnumMapping)
            {
                values.Add(entry.RepValue, entry.FriendlyValue);
            }
            m_fieldValues.Add(id, values);
            return enumMapping.FriendlyName;
        }

        private Dictionary<string, string> GetValuesForField(string fieldId)
        {
            if (!m_fieldValues.ContainsKey(fieldId))
            {
                GetFriendlyName(fieldId);
            }
            return m_fieldValues[fieldId];
        }

        /// <summary>
        /// TRIES to get the friendly description of the specified value. If the specified value does  not exist returns empty string.
        /// </summary>
        /// <param name="fieldId"></param>
        /// <param name="valueId"></param>
        /// <returns></returns>
        private string GetFriendlyValueDesc(string fieldId, string valueId)
        {
            var valueDictionary = GetValuesForField(fieldId);
            string data;
            valueDictionary.TryGetValue(valueId, out data);
            return data ?? valueId;
        }
    }
}
