/// Author: David Dao

using System;

namespace DataAccess.LoanValidation
{
    public enum E_LoanValidationResultT 
    {
        OK,
        Error,
        Warning
    }

    public class LoanValidationResult 
    {
        private E_LoanValidationResultT m_result;
        private string m_message;
        public LoanValidationResult(E_LoanValidationResultT result, string message) 
        {
            m_result = result;
            m_message = message;
        }

        public E_LoanValidationResultT Result 
        {
            get { return m_result; }
        }

        public string Message 
        {
            get { return m_message; }
        }

    }
}
