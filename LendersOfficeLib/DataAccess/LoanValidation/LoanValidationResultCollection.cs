/// Author: David Dao

using System;
using System.Collections;

namespace DataAccess.LoanValidation
{
	public class LoanValidationResultCollection
	{
        private ArrayList m_okayList = new ArrayList();
        private ArrayList m_warningList = new ArrayList();
        private ArrayList m_errorList = new ArrayList();

		public LoanValidationResultCollection()
		{
		}

        public int NormalCount 
        {
            get { return m_okayList.Count; }
        }
        public int WarningCount 
        {
            get { return m_warningList.Count; }
        }
        public int ErrorCount 
        {
            get { return m_errorList.Count; }
        }
        public void Add(LoanValidationResult result) 
        {
            switch (result.Result) 
            {
                case E_LoanValidationResultT.OK:
                    m_okayList.Add(result);
                    break;
                case E_LoanValidationResultT.Warning:
                    m_warningList.Add(result);
                    break;
                case E_LoanValidationResultT.Error:
                    m_errorList.Add(result);
                    break;
                default:
                    Tools.LogBug("Unhandle LoanValidationResult=" + result.Result);
                    break;
            }
        }

        public LoanValidationResult[] ErrorList 
        {
            get { return (LoanValidationResult[]) m_errorList.ToArray(typeof(LoanValidationResult)); }
        }

        public LoanValidationResult[] WarningList 
        {
            get { return (LoanValidationResult[]) m_warningList.ToArray(typeof(LoanValidationResult)); }
        }

        public LoanValidationResult[] NormalList 
        {
            get { return (LoanValidationResult[]) m_okayList.ToArray(typeof(LoanValidationResult)); }
        }
	}
}
