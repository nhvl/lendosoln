/// Author: David Dao

using System;
using DataAccess;

namespace DataAccess.LoanValidation
{
	public class PmlRateLockPeriodValidationRule : ILoanValidationRule
	{
        public LoanValidationResult Validate(CPageBase dataLoan) 
        {
            string s = dataLoan.sProdRLckdDays_rep;

            try 
            {
                if (s == "" || s == "0")
                    return new LoanValidationResult(E_LoanValidationResultT.Error, "Missing rate lock period.");

                int _v = int.Parse(s);
                if (_v <= 0)
                    return new LoanValidationResult(E_LoanValidationResultT.Error, "Rate lock period must be greater than 0.");

                return new LoanValidationResult(E_LoanValidationResultT.OK, "");
            } 
            catch 
            {
                return new LoanValidationResult(E_LoanValidationResultT.Error, "Rate lock period is invalid.");

            }

        }
	}
}
