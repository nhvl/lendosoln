/// Author: David Dao

using System;
using System.Collections;
using DataAccess;

namespace DataAccess.LoanValidation
{
    public class PmlBorrowerNameSsnValidationRule : ILoanValidationRule
    {
        private bool UserCanRunPricingWithoutCreditReport { get; }

        public PmlBorrowerNameSsnValidationRule(bool userCanRunPricingWithoutCreditReport)
        {
            this.UserCanRunPricingWithoutCreditReport = userCanRunPricingWithoutCreditReport;
        }

        public LoanValidationResult Validate(CPageBase dataLoan) 
        {
            string[] missingFields = new string[3];
            int index = 0;

            CAppBase dataApp = dataLoan.GetAppData(0);

            if (dataApp.aBFirstNm == "") 
            {
                missingFields[index++] = "first name";
            }

            if (dataApp.aBLastNm == "") 
            {
                missingFields[index++] = "last name";
            }

            if (!this.UserCanRunPricingWithoutCreditReport && dataApp.aBSsn == "") 
            {
                missingFields[index++] = "SSN";
            }

            if (index > 0) 
            {
                string errorMessage = string.Format("Missing applicant {0}.", string.Join(", ", missingFields, 0, index));

                return new LoanValidationResult(E_LoanValidationResultT.Error, errorMessage);
            } 
            else 
            {
                return new LoanValidationResult(E_LoanValidationResultT.OK, "");
            }
        }

    }


}
