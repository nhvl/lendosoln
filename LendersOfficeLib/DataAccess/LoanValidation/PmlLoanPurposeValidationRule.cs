/// Author: David Dao

using System;
using DataAccess;

namespace DataAccess.LoanValidation
{
	public class PmlLoanPurposeValidationRule : ILoanValidationRule
	{
        public LoanValidationResult Validate(CPageBase dataLoan) 
        {
            switch (dataLoan.sLPurposeTPe) 
            {
                case E_sLPurposeT.Other:
                    return new LoanValidationResult(E_LoanValidationResultT.Error, "Loan purpose other is not supported by the pricing engine.");
                case E_sLPurposeT.Purchase:
                case E_sLPurposeT.Refin:
                case E_sLPurposeT.RefinCashout:
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.VaIrrrl:
                case E_sLPurposeT.Construct:
                case E_sLPurposeT.ConstructPerm:
                case E_sLPurposeT.HomeEquity: // 9/9/2013 dd - OPM 136658
                    return new LoanValidationResult(E_LoanValidationResultT.OK, "");
                default:
                    Tools.LogBug("Unhandle sLPurposeTPe in PmlLoanPurposeValidationRule. Value=" + dataLoan.sLPurposeTPe);
                    return new LoanValidationResult(E_LoanValidationResultT.Error, "Loan purpose is not supported by the pricing engine.");

            }

        }
	}
}
