/// Author: David Dao

using System;
using DataAccess;

namespace DataAccess.LoanValidation
{
	public class PmlPropertyStateValidationRule : ILoanValidationRule
	{
        public LoanValidationResult Validate(CPageBase dataLoan) 
        {
            if (dataLoan.sSpStatePe == "") 
            {
                return new LoanValidationResult(E_LoanValidationResultT.Error, "Missing property state.");
            } 
            else 
            {
                return new LoanValidationResult(E_LoanValidationResultT.OK, "");
            }
        }
	}
}
