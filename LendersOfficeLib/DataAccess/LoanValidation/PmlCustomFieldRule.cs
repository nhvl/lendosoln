﻿// <copyright file="PmlCustomFieldRule.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: EduardoM
//    Date:   8/12/2014
// </summary>

namespace DataAccess.LoanValidation
{
    using System;
    using DataAccess;
    using LendersOffice.CustomPmlFields;

    /// <summary>
    /// This class is a rule that checks whether all the PML custom fields are filled out.
    /// </summary>
    public class PmlCustomFieldRule : ILoanValidationRule
    {
        /// <summary>
        /// Checks whether the PML custom fields have been filled correctly.
        /// </summary>
        /// <param name="dataLoan">The data from the loan.</param>
        /// <returns>Returns an "OK" result if the fields are filled out, else it returns an "Error" result.</returns>
        public LoanValidationResult Validate(CPageBase dataLoan)
        {
            var list = dataLoan.BrokerDB.CustomPmlFieldList;
            string errorList = string.Empty;

            for (int i = 1; i <= list.Fields.Count; i++)
            {
                CustomPmlField field = list.Get(i);
                if (field.IsValid && field.DisplayFieldForLoanPurpose(dataLoan.sLPurposeT))
                {
                    if (dataLoan.GetCustomPMLFieldRep(i, false).Equals(string.Empty))
                    {
                        if (!errorList.Equals(string.Empty))
                        {
                            errorList += ", " + field.Description;
                        }
                        else
                        {
                            errorList = field.Description;
                        }
                    }
                }
            }

            if (errorList.Equals(string.Empty))
            {
                return new LoanValidationResult(E_LoanValidationResultT.OK, string.Empty);
            }
            else
            {
                return new LoanValidationResult(E_LoanValidationResultT.Error, "Missing custom PML field(s): " + errorList);
            }
        }
    }
}
