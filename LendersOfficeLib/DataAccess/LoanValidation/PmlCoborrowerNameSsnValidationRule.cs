/// Author: David Dao

using System;
using System.Collections;
using DataAccess;

namespace DataAccess.LoanValidation
{
	public class PmlCoborrowerNameSsnValidationRule : ILoanValidationRule
	{
        private bool UserCanRunPricingWithoutCreditReport { get; }

        public PmlCoborrowerNameSsnValidationRule(bool userCanRunPricingWithoutCreditReport)
        {
            this.UserCanRunPricingWithoutCreditReport = userCanRunPricingWithoutCreditReport;
        }

        public LoanValidationResult Validate(CPageBase dataLoan) 
        {
            CAppBase dataApp = dataLoan.GetAppData(0);

            if (dataApp.aBHasSpouse) 
            {
                string[] missingFields = new string[3];
                int index = 0;

                if (dataApp.aCFirstNm == "") 
                {
                    missingFields[index++] = "first name";
                }

                if (dataApp.aCLastNm == "") 
                {
                    missingFields[index++] = "last name";
                }

                if (!this.UserCanRunPricingWithoutCreditReport && dataApp.aCSsn == "") 
                {
                    missingFields[index++] = "SSN";
                }

                if (index > 0) 
                {
                    string errorMessage = string.Format("Missing co-applicant {0}.", string.Join(", ", missingFields, 0, index));

                    return new LoanValidationResult(E_LoanValidationResultT.Error, errorMessage);
                } 

                 
            }

            return new LoanValidationResult(E_LoanValidationResultT.OK, "");
        }
	}
}
