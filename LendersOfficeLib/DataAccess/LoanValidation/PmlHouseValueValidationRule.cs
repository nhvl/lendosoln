/// Author: David Dao

using System;
using DataAccess;

namespace DataAccess.LoanValidation
{
	public class PmlHouseValueValidationRule : ILoanValidationRule
	{
        public LoanValidationResult Validate(CPageBase dataLoan) 
        {
            if (dataLoan.sHouseValPe_rep == "" || dataLoan.sHouseValPe_rep =="$0.00") 
            {
                return new LoanValidationResult(E_LoanValidationResultT.Error, "Missing sales price/house value.");
            }
            else 
            {
                return new LoanValidationResult(E_LoanValidationResultT.OK, "");
            }
        }
	}
}
