/// Author: David Dao

using System;
using DataAccess;

namespace DataAccess.LoanValidation
{
	public class PmlProdMIOptionTValidationRule : ILoanValidationRule
	{
        public LoanValidationResult Validate(CPageBase dataLoan) 
        {
            decimal sLtvRPe = 0;
            try 
            {
                sLtvRPe = dataLoan.sLtvRPe;
            } 
            catch {}

            // 10/25/2013 dd - It is acceptable for HELOC to have LTV = 0
            if (dataLoan.sIsLineOfCredit == false && sLtvRPe <= 0) 
            {
                return new LoanValidationResult(E_LoanValidationResultT.Error, "LTV is not a valid value.");
            }
            // 01/24/12. Per OPM 109393, People can choose No MI for LTV > 80%.
            //else if (sLtvRPe > 80 && dataLoan.sProdMIOptionT == E_sProdMIOptionT.LeaveBlank) 
            //{

            //    LendersOfficeApp.los.admin.EmployeeDB empDb = LendersOfficeApp.los.admin.EmployeeDB.RetrieveById(dataLoan.BrokerDB.BrokerID, LendersOffice.Security.PrincipalFactory.CurrentPrincipal.EmployeeId);
            //    if (dataLoan.BrokerDB.IsNewPmlUIEnabled || empDb.IsNewPmlUIEnabled)
            //    {
            //       // This situation OK in new PML.
            //    }
            //    else
            //        return new LoanValidationResult(E_LoanValidationResultT.Error, "Mortgage Insurance cannot be blank for LTV > 80%.");
            //}
            return new LoanValidationResult(E_LoanValidationResultT.OK, "");
        }
	}
}
