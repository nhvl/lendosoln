/// Author: David Dao

using System;
using DataAccess;

namespace DataAccess.LoanValidation
{
	public interface ILoanValidationRule
	{
        LoanValidationResult Validate(CPageBase dataLoan);
	}
}
