/// Author: David Dao

using System;
using DataAccess;
namespace DataAccess.LoanValidation
{
	public class PmlCashoutAmountValidationRule : ILoanValidationRule
	{
		public LoanValidationResult Validate(CPageBase dataLoan)
		{
            if (dataLoan.sLPurposeTPe == E_sLPurposeT.RefinCashout || dataLoan.sLPurposeTPe == E_sLPurposeT.HomeEquity) 
            {
                try 
                {
                    if (dataLoan.sProdCashoutAmt == 0 && (!dataLoan.sIsStandAlone2ndLien && !dataLoan.sIsLineOfCredit)) 
                    {
                        return new LoanValidationResult(E_LoanValidationResultT.Error, "Cashout amount cannot be $0.00 for a Refi-Cashout.");
                    } 
                    else if (dataLoan.sProdCashoutAmt < 0) 
                    {
                        return new LoanValidationResult(E_LoanValidationResultT.Error, "Cashout amount cannot be negative.");
                    }
                } 
                catch 
                {
                    return new LoanValidationResult(E_LoanValidationResultT.Error, "Cashout amount is required for Refi-Cashout.");
                }

            }

            return new LoanValidationResult(E_LoanValidationResultT.OK, "");
		}
	}
}
