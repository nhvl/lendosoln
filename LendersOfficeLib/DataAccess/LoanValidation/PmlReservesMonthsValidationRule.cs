/// Author: David Dao

using System;
using DataAccess;

namespace DataAccess.LoanValidation
{
	public class PmlReservesMonthsValidationRule : ILoanValidationRule
	{
        public LoanValidationResult Validate(CPageBase dataLoan) 
        {
            // 6/19/2013 dd - if Broker is using PML 2.0 then reserve will get auto calculate. Therefore this validation is not applicable.
            if (dataLoan.BrokerDB.IsNewPmlUIEnabled)
            {
                return new LoanValidationResult(E_LoanValidationResultT.OK, "");
            }
            else
            {
                // 06/06/07 mf OPM 15866.  Now allow blank Reserves if no-asset doc type.
                if (dataLoan.sProdAvailReserveMonths_rep == "-1" &&
                     !(dataLoan.sProdDocT == E_sProdDocT.NINA || dataLoan.sProdDocT == E_sProdDocT.NINANE || dataLoan.sProdDocT == E_sProdDocT.VINA) &&
                     !dataLoan.sIsIncomeAssetsNonRequiredFhaStreamline)
                {
                    return new LoanValidationResult(E_LoanValidationResultT.Error, "Reserves Available cannot be blank unless Doc Type is no-asset.");
                }
                else
                {
                    return new LoanValidationResult(E_LoanValidationResultT.OK, "");
                }
            }
        }
	}
}
