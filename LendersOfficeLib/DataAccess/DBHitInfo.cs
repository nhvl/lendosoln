﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;

namespace DataAccess
{
    public class DBHitInfo
    {
        public bool Succeeded;
        public string ReasonFailed;
        public DBHitInfo(bool succeeded, string reasonFailed)
        {
            Succeeded = succeeded;
            ReasonFailed = reasonFailed;
        }

        public static DBHitInfo CreateFromRowsAffectedWhenExpectingOne(
            int numRowsAffected, 
            string usrMessageIfNoRowsAffected,
            string procName, IEnumerable<SqlParameter> sqlParameters
            )
        {
            if (numRowsAffected == 1)
            {
                return new DBHitInfo(true, null);
            }
            else
            {
                if (numRowsAffected == 0)
                {
                    Tools.LogError(procName + " updated no rows with the following parameters: "
                        + SqlParamSerializer.ToString(sqlParameters));
                    return new DBHitInfo(false, usrMessageIfNoRowsAffected);
                }
                else // numRowsAffected > 1
                {
                    Tools.LogErrorWithCriticalTracking(procName + " updated too many rows (" + numRowsAffected
                        + ") with the following parameters: " + SqlParamSerializer.ToString(sqlParameters));
                    return new DBHitInfo(true, null);
                }
            }
        }
    }
}
